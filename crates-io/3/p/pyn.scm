(define-module (crates-io #{3}# p pyn) #:use-module (crates-io))

(define-public crate-pyn-0.1.0 (c (n "pyn") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "0l5h8f07jzdia7bp5zjr71iv9zdqvwi90lwvdlfz60ry88x716n0")))

(define-public crate-pyn-0.2.0 (c (n "pyn") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "1zb88bplr1h4mmkca4szijga9k8bg90b7krki6wgf59r1h7920dd")))

(define-public crate-pyn-0.2.1 (c (n "pyn") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.45") (d #t) (k 0)))) (h "0gyl4ykhq2qa4p7xk053pymrgyv158616vyn2iarznscn8baqzl6")))

(define-public crate-pyn-0.2.2 (c (n "pyn") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1in6zyqi80gnp4i0i4qx2ibwyk9jbhhrlilni92z1j3ha6lg1lc5")))

(define-public crate-pyn-0.3.0 (c (n "pyn") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0ma28lw93mqd46rzph8p12sl9phdaqqi377qkdgyqf93ih577rfp")))

(define-public crate-pyn-0.3.1 (c (n "pyn") (v "0.3.1") (d (list (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0216kgshhwap8ckld4cg9l5l07fnqijb8v3sksd8dinn57lqx5qs")))

(define-public crate-pyn-0.4.0 (c (n "pyn") (v "0.4.0") (d (list (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1v7swx6k34dzd2s6s8wdl6h7wa1lll78rcmkb8xmifv1y5vh2x30")))

(define-public crate-pyn-0.4.1 (c (n "pyn") (v "0.4.1") (d (list (d (n "dialoguer") (r "^0.8") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "02ckp4svj6fb5n46xyfypiyjw9nrw081iciqbx8gcbq50282m8h2")))

(define-public crate-pyn-0.5.0 (c (n "pyn") (v "0.5.0") (d (list (d (n "dialoguer") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13agb5pncr2nzwhjnra5r5r7p7xqdz8m8wzlq5f2w923hbfgh725")))

(define-public crate-pyn-0.6.0 (c (n "pyn") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5.4") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ifxkwana6kssbzvy7n6agfn56h9n0jdmzh0q0bpsmfiqpdj6rmy")))

