(define-module (crates-io #{3}# p phd) #:use-module (crates-io))

(define-public crate-phd-0.1.0 (c (n "phd") (v "0.1.0") (d (list (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "gophermap") (r "^0.1.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1l0n4r4hhp2vikqbl8b08d54xyk89jcrzy6amd8csh27qg7g8cxk")))

(define-public crate-phd-0.1.1 (c (n "phd") (v "0.1.1") (d (list (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "gophermap") (r "^0.1.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0irfzvjn7wchiy1zajqd7645w5zhx8dj926xhbdmqing9lzninqj")))

(define-public crate-phd-0.1.2 (c (n "phd") (v "0.1.2") (d (list (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "gophermap") (r "^0.1.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0b2hblgy7ngbdz0mv7lbx8dvajkryl37yyfzln5ym189xbc4sid4")))

(define-public crate-phd-0.1.3 (c (n "phd") (v "0.1.3") (d (list (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "gophermap") (r "^0.1.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "11q09s2j8rgwgk36agy4pdycw2qdg8a49lbsh3rgpqnr93f6n2ss")))

(define-public crate-phd-0.1.4 (c (n "phd") (v "0.1.4") (d (list (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "gophermap") (r "^0.1.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0ab10h5siln0c74balqrz3593b9vglfxna9w1309zr7b7f2d49as")))

(define-public crate-phd-0.1.5 (c (n "phd") (v "0.1.5") (d (list (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "gophermap") (r "^0.1.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0mw03ywg9a645c6v19faw6fvma491bawcmap6hg3zms4f79y3svj")))

(define-public crate-phd-0.1.6 (c (n "phd") (v "0.1.6") (d (list (d (n "alphanumeric-sort") (r "^1.0.11") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "gophermap") (r "^0.1.2") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "114wxydp6asyqhiqvkiczsrsx3jz069bbl7f95v7f7fhn25njhyy")))

(define-public crate-phd-0.1.7 (c (n "phd") (v "0.1.7") (d (list (d (n "alphanumeric-sort") (r "^1.0.11") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "gophermap") (r "^0.1.2") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "008d1cvmbwb2rq3bywd7ndi18ngdxw97z12jp3vk59z53f5s7bwm")))

(define-public crate-phd-0.1.8 (c (n "phd") (v "0.1.8") (d (list (d (n "alphanumeric-sort") (r "^1.0.11") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "gophermap") (r "^0.1.2") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1d87fprddnfkkmsjykmiaj5fnk0x0zrk453067z3x2dqk8rvwrfz")))

(define-public crate-phd-0.1.9 (c (n "phd") (v "0.1.9") (d (list (d (n "alphanumeric-sort") (r "^1.0.11") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "gophermap") (r "^0.1.2") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "15r0m4ymp30z1ky9j067rqwnchzlwipmnsm1q4gmc7dcsmqv010c")))

(define-public crate-phd-0.1.10 (c (n "phd") (v "0.1.10") (d (list (d (n "alphanumeric-sort") (r "^1.0.11") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "19pbmn0scckjqn8i6ah0brqa60f6bjwy9w5x8f964drjmhfzcbjz")))

(define-public crate-phd-0.1.11 (c (n "phd") (v "0.1.11") (d (list (d (n "alphanumeric-sort") (r "^1.0.11") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1pnpz4rc4wk5say1wbrdvja48x0gxaycz17nqwblapdwqakbm0bj")))

(define-public crate-phd-0.1.12 (c (n "phd") (v "0.1.12") (d (list (d (n "alphanumeric-sort") (r "^1.0.11") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "161qhspqjbrxgxjq5mfsbml923h9gaifjjp72j1gd34295xbkfll")))

(define-public crate-phd-0.1.13 (c (n "phd") (v "0.1.13") (d (list (d (n "alphanumeric-sort") (r "^1.0.11") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "1s81bd00j4id2l03g21f7kv965p9rs8a4d4jbv3q9rnq9y5rgjpq")))

(define-public crate-phd-0.1.14 (c (n "phd") (v "0.1.14") (d (list (d (n "alphanumeric-sort") (r "^1.0.11") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "0cq1x7vi4gdina16ihvfk7z5yqbh053pb4mv508fcxdmvd3qafvq")))

(define-public crate-phd-0.1.15 (c (n "phd") (v "0.1.15") (d (list (d (n "alphanumeric-sort") (r "^1.4") (d #t) (k 0)) (d (n "content_inspector") (r "^0.2.4") (d #t) (k 0)) (d (n "shell-escape") (r "^0.1.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.7.1") (d #t) (k 0)))) (h "06ydkcn20pl44h1g2821silh8bz7r2n6q2b3nr047rhl1h2y92yy")))

