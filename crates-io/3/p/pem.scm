(define-module (crates-io #{3}# p pem) #:use-module (crates-io))

(define-public crate-pem-0.1.0 (c (n "pem") (v "0.1.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1fr11l453qv1rvgriy7qgg5ppl3ccpn7qcdywp96dq066skbcx8g")))

(define-public crate-pem-0.2.0 (c (n "pem") (v "0.2.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0i1lpxqpav73hdhfn36k7wz6kcgqkf15nm2rikkrc2l1jd9cjzis")))

(define-public crate-pem-0.3.0 (c (n "pem") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1hzdd41w1d6fp2miblva41d39fz0n9bd0axl9vhiy0l1vbw77rci")))

(define-public crate-pem-0.4.0 (c (n "pem") (v "0.4.0") (d (list (d (n "base64") (r "^0.5.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "09mzz2xkbqknmsqmxhjkpw5n6x8j8mfplrhprbswjpkbxfscz3kb")))

(define-public crate-pem-0.4.1 (c (n "pem") (v "0.4.1") (d (list (d (n "base64") (r "^0.6.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1094arpzh0w0g7wihkxida0qr8q09cdilh7pg9sczn32m6w9lxmb")))

(define-public crate-pem-0.5.0 (c (n "pem") (v "0.5.0") (d (list (d (n "base64") (r "^0.8.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1n3lm99dh19w35ha9jia0g444ln4i8xz643mrka9mgivqryzxclp")))

(define-public crate-pem-0.5.1 (c (n "pem") (v "0.5.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ladid05di7w2rrb2fabdyjvqh8ddgm9j55lwck78fgqdgbdmqrk")))

(define-public crate-pem-0.6.0 (c (n "pem") (v "0.6.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1lmk5sxyas70sqs1hav2nkbpai692nccl6fls666hqcdasbf0apx")))

(define-public crate-pem-0.6.1 (c (n "pem") (v "0.6.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0npl6sw25vwc97h1bgyxk3k1rna5c8k48lsiarqvzpfzfd04gsrr")))

(define-public crate-pem-0.7.0 (c (n "pem") (v "0.7.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "14wpql0znpxrg6bq6lmp9kvbs9v24l0zzqqf3yj5d9spqxh1fn51")))

(define-public crate-pem-0.8.0 (c (n "pem") (v "0.8.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1cyzpgab3k5csb2dvncs14rdv7jbywgln6ybdr5h771xrlgfswwk")))

(define-public crate-pem-0.8.1 (c (n "pem") (v "0.8.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "criterion") (r "^0.2.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jzvvxpn9wnxf053p75m7xlvj34rxk1rrlzg9887ggzrknkqwsar")))

(define-public crate-pem-0.8.2 (c (n "pem") (v "0.8.2") (d (list (d (n "base64") (r ">=0.13.0, <0.14.0") (d #t) (k 0)) (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "once_cell") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2.0.0") (f (quote ("std"))) (k 0)))) (h "1iphvl566j93gggbqdq6pz3afr0yx38mj8x8dkci6gc63z821hpl")))

(define-public crate-pem-0.8.3 (c (n "pem") (v "0.8.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)))) (h "1sqkzp87j6s79sjxk4n913gcmalzb2fdc75l832d0j7a3z9cnmpx")))

(define-public crate-pem-1.0.0 (c (n "pem") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)))) (h "03sw5k1wmvzvni3dbvd1j0jx3xpk1f5cghiv7n4jm4rkabgp68rz")))

(define-public crate-pem-1.0.1 (c (n "pem") (v "1.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (f (quote ("std"))) (k 0)))) (h "06ngsmxglj1h2wrpzsisvyjj89izakcrr9igjiijml44vdh3hrq6")))

(define-public crate-pem-1.0.2 (c (n "pem") (v "1.0.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)))) (h "0iqrvfnm71x9pvff39d5ajwn3gc9glxlv4d4h22max7342db18z9")))

(define-public crate-pem-1.1.0 (c (n "pem") (v "1.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1m3gb5rj3w3lgx5dzjq91nfib9zchljkdd7kqj7384m2l4qlkih3")))

(define-public crate-pem-1.1.1 (c (n "pem") (v "1.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1f184b7vs5kgwglfsy9adqqy7625jsq8jj1lsxah9abn78kmr0x8")))

(define-public crate-pem-2.0.0 (c (n "pem") (v "2.0.0") (d (list (d (n "base64") (r "^0.21.0") (f (quote ("alloc"))) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "08qikv3khmcq7hivj1wlpzkdh24fxvz1s6lpkm8pi2i9lxb7ls1i") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "base64/std" "serde?/std") ("serde" "dep:serde")))) (r "1.67.0")))

(define-public crate-pem-2.0.1 (c (n "pem") (v "2.0.1") (d (list (d (n "base64") (r "^0.21.0") (f (quote ("alloc"))) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "06j4vmzkfg5jh9ykc5bdvydishqkbb4sf64fa528wg6zbi0zw4vb") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "base64/std" "serde?/std") ("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-pem-3.0.0 (c (n "pem") (v "3.0.0") (d (list (d (n "base64") (r "^0.21.0") (f (quote ("alloc"))) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09d1y5a8ik9j3qvbqi0g752rpp7agjgpk2bdgxvms4z2mdj43jb4") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "base64/std" "serde?/std") ("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-pem-3.0.1 (c (n "pem") (v "3.0.1") (d (list (d (n "base64") (r "^0.21.0") (f (quote ("alloc"))) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0q595la77ysarwmi909bch4ahqmm3xsfnjn3c2nlq2y3pypjfcgd") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "base64/std" "serde?/std") ("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-pem-3.0.2 (c (n "pem") (v "3.0.2") (d (list (d (n "base64") (r "^0.21.0") (f (quote ("alloc"))) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "08sr305rhn9svnw7bvr65p95rfn9xv3z4md0a7b54fvw5f8x4qri") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "base64/std" "serde?/std") ("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-pem-3.0.3 (c (n "pem") (v "3.0.3") (d (list (d (n "base64") (r "^0.21.0") (f (quote ("alloc"))) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0463ya67xrxaqn4qs9iz7rsx4parcasd78pd9fv7yd1m81wwr3qv") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "base64/std" "serde?/std") ("serde" "dep:serde")))) (r "1.60.0")))

(define-public crate-pem-3.0.4 (c (n "pem") (v "3.0.4") (d (list (d (n "base64") (r "^0.22.0") (f (quote ("alloc"))) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^1") (f (quote ("std"))) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1blgcn17wc41yxdzrxlsir5m6ds8r13ijmpsqda6lwwhwmjr6icf") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "base64/std" "serde?/std") ("serde" "dep:serde")))) (r "1.60.0")))

