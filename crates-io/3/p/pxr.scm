(define-module (crates-io #{3}# p pxr) #:use-module (crates-io))

(define-public crate-pxr-0.1.0 (c (n "pxr") (v "0.1.0") (d (list (d (n "pxr_sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "020hws58mrss9lrwhm9x6gk3c7iz4pba760n778r1kxkdm1i9sqj") (f (quote (("default")))) (r "1.70")))

(define-public crate-pxr-0.1.1 (c (n "pxr") (v "0.1.1") (d (list (d (n "pxr_sys") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cj5vkmg6d0hhani1pr84qnrdf1xskn7c04ikd3d49icas3mrlrc") (f (quote (("vendored" "pxr_sys/vendored") ("default" "vendored")))) (r "1.70")))

(define-public crate-pxr-0.1.2 (c (n "pxr") (v "0.1.2") (d (list (d (n "pxr_sys") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f1ndyv425ylg0lk3jgzjmmlwb15l2d1x2ir65q5j2ri9c9k0jxc") (f (quote (("vendored" "pxr_sys/vendored") ("default" "vendored")))) (r "1.70")))

(define-public crate-pxr-0.2.0 (c (n "pxr") (v "0.2.0") (d (list (d (n "pxr_sys") (r "^0.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sx9h5frhwccdr90d21cir2pxm0h03fdixlszqd36ynbh9a2ssbs") (f (quote (("vendored" "pxr_sys/vendored") ("default" "vendored")))) (r "1.70")))

