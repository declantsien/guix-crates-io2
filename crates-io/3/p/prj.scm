(define-module (crates-io #{3}# p prj) #:use-module (crates-io))

(define-public crate-prj-0.1.0 (c (n "prj") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "crossterm") (r "^0.14.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.1") (d #t) (k 0)) (d (n "git2") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "term_size") (r "^1.0.0-beta1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "14xs7hvn4ig0hnc2ghh410az9276d8f6yhma064mnqwsy9nmw6il")))

