(define-module (crates-io #{3}# p pow) #:use-module (crates-io))

(define-public crate-pow-0.1.0 (c (n "pow") (v "0.1.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "07gzwdymyw25pvw4zw45fk76kd2g6hjcab2caflb483kvzvw4lbf")))

(define-public crate-pow-0.1.1 (c (n "pow") (v "0.1.1") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0i1033rx5nlqfql1k6p6643lw55y5xmq48sz4fc1y4qg07gv3aqy")))

(define-public crate-pow-0.1.2 (c (n "pow") (v "0.1.2") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0fcgmxhglq1gxflvvxk403xa8zqfkx7m46ygqc77r7xx9x9wlqbd")))

(define-public crate-pow-0.1.3 (c (n "pow") (v "0.1.3") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1xvsf30j1i8kalslabxdqfd1ijlgn290brk5rq9iz5dsx9jbsqr7")))

(define-public crate-pow-0.1.4 (c (n "pow") (v "0.1.4") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0jkkv38jii4k7nazdlhkx33452nrpvzda50nqp664m4gk90xcscd")))

(define-public crate-pow-0.1.5 (c (n "pow") (v "0.1.5") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "05zx7iq58nv2knja90zibgijampg5jzcrw4k2czzadfz2bj57cr6")))

(define-public crate-pow-0.2.0 (c (n "pow") (v "0.2.0") (d (list (d (n "bincode") (r "^1.1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0d315raapnx1rp6hnc2prv1m7jmk2iq7gxklih8hz9bdvwnlkh4y")))

