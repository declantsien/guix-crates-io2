(define-module (crates-io #{3}# p psm) #:use-module (crates-io))

(define-public crate-psm-0.1.0 (c (n "psm") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "13crk9s295v9bhf9nygmqkn35aq67axfic5ybpdhh23rir9c7683")))

(define-public crate-psm-0.1.1 (c (n "psm") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "049chzpal644zdc5w6ma7mngfp8fbv4ry7r0rx1m8ik05bjqlc1c")))

(define-public crate-psm-0.1.2 (c (n "psm") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1xzya8y763ndjr6xssla94qqpyj6kxlvz91k3qr68arlczqkag5z")))

(define-public crate-psm-0.1.3 (c (n "psm") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15ga3whv6q6j0fg4mv6l2zcvswarfkzgj2b74f6pbkha82rvqdsy")))

(define-public crate-psm-0.1.4 (c (n "psm") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1g34hkirgfs91rh27aznvnmv9hahxvabx0j2amx3i82zj8vz682p")))

(define-public crate-psm-0.1.5 (c (n "psm") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "14z1dr9dpa15rxd83whfvvnp6dn766f06i4nqjnxyxb2wbz9wqsl")))

(define-public crate-psm-0.1.6 (c (n "psm") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1q1hdbnp2j3zz1vhzp1xhds6ynan3mg5bhjlhfy5m1sg8n5wckxi")))

(define-public crate-psm-0.1.7 (c (n "psm") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0hmcpf7l57ynr0s6g9w9m8378rgc5aywlwaa0yv2kdsyan4phvsq")))

(define-public crate-psm-0.1.8 (c (n "psm") (v "0.1.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "19rsrrs3i8nn2g14j8wkx8ppa2ymp8s32rxp7f4mi92247mcz7k5")))

(define-public crate-psm-0.1.9 (c (n "psm") (v "0.1.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "074grhybyn19aijy0lmrl0isfsa6xrys4mphlw66rh20qjbwvq45")))

(define-public crate-psm-0.1.10 (c (n "psm") (v "0.1.10") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1kr9sal8g9zil4ch8ra0ry96d5cl15xslk1p0wnqk1504ib3hb89")))

(define-public crate-psm-0.1.11 (c (n "psm") (v "0.1.11") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1igjsa60cbm2rnxc955p4pzx1a3m871qdfg6pfxnsii8cmpm7q4n")))

(define-public crate-psm-0.1.12 (c (n "psm") (v "0.1.12") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0fh8wyrsqf89756r7864bi50qmjqcd9h2rfjrxm7b43j87jlkgrs")))

(define-public crate-psm-0.1.13 (c (n "psm") (v "0.1.13") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "1h4hjhwwkmghsgjws1i7c5bcvsrpwh9iv8p4jxmmfpm8niwh5zr1")))

(define-public crate-psm-0.1.14 (c (n "psm") (v "0.1.14") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "078r0j28g1i4carycwil03faxcq9vlm2jqyi0xrs6a04ikx3gkhl")))

(define-public crate-psm-0.1.15 (c (n "psm") (v "0.1.15") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "0n6k05xhash97vkzly229l562rq90i4wwra0h0fr8p9v2rhyw5q6")))

(define-public crate-psm-0.1.16 (c (n "psm") (v "0.1.16") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "0scfnwhc0l6dv1dvsnxrfqrjdbxj59qy9f8w0vy56irc73s6y4yd")))

(define-public crate-psm-0.1.17 (c (n "psm") (v "0.1.17") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "0vvyn6n0acbr14fb53b7bqbf67nvn7q8iiffhkhrck3wvnjhzjkf")))

(define-public crate-psm-0.1.18 (c (n "psm") (v "0.1.18") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "0nhidmrhmdnkfphcyf9n4vdx9cq5i5hddly5sc0frk462wwp44w7")))

(define-public crate-psm-0.1.19 (c (n "psm") (v "0.1.19") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "1wnxmyim41ajrw6jz7lcxp1fj7qh70ja4m8kb0x57ygv32m8kkdc")))

(define-public crate-psm-0.1.20 (c (n "psm") (v "0.1.20") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "09c15ds079q1py8f6hqk0sq8jv7xngh4ryy4b22r48msxykd0ipl")))

(define-public crate-psm-0.1.21 (c (n "psm") (v "0.1.21") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "0x78nj5wxkxwijd2gvv2ycq06443b2y1ih4j46kk6c2flg6zg1sp")))

