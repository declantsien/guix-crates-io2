(define-module (crates-io #{3}# p pia) #:use-module (crates-io))

(define-public crate-pia-0.1.0 (c (n "pia") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1cw1n0i1lnyyip5iph1ylrfw8bj5l72iikn2fkx758w92rakvif1")))

(define-public crate-pia-0.2.0 (c (n "pia") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (o #t) (d #t) (k 0)))) (h "0g34dfcwzy7vaqpqcy0xv4p4x93n08yywxsff9xyjcv0aqzv2mwq")))

