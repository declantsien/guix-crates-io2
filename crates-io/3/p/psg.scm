(define-module (crates-io #{3}# p psg) #:use-module (crates-io))

(define-public crate-psg-1.0.0 (c (n "psg") (v "1.0.0") (h "0bg49bbzi6zw1pp78w38zyh96di4rww7rdvwm607cs8mkdygbbbi")))

(define-public crate-psg-1.0.1 (c (n "psg") (v "1.0.1") (h "0a7jpyd0zr2jgdvxbqbxsq4gb4bn3pcpjmy72yc9fpjmi65v9h7p")))

