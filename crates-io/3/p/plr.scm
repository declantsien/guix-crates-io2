(define-module (crates-io #{3}# p plr) #:use-module (crates-io))

(define-public crate-plr-0.1.0 (c (n "plr") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)))) (h "1pp65q6yq71xq03gd1ac05xibqc8h0g7scjjg608637akqwnxgxc")))

(define-public crate-plr-0.1.1 (c (n "plr") (v "0.1.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)))) (h "158rin8m1ny71zsfkd2468lhzxy6xf50k3cjb36ksy3c34lyag8a")))

(define-public crate-plr-0.1.2 (c (n "plr") (v "0.1.2") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)))) (h "01dwdg0hwil7pr9gb1ipcj64ai399kv34l0fx1ln0qwvaqlaq062")))

(define-public crate-plr-0.1.3 (c (n "plr") (v "0.1.3") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)))) (h "0867q5i72b0dafp7dskif06fc64pkzcyb74zgw7wmmn1bg6vmm7p")))

(define-public crate-plr-0.1.4 (c (n "plr") (v "0.1.4") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)))) (h "0dfv6xcpqd5an3hhsdwx5smqjbvzkbccy98g504z1n8acawkdm3p")))

(define-public crate-plr-0.1.5 (c (n "plr") (v "0.1.5") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "18ijnfnldnw5fjxys156m41aar1pq9wwciwq0cmmpalbq52byk8g")))

(define-public crate-plr-0.1.6 (c (n "plr") (v "0.1.6") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1cznnl2fqrzh301yg5q4nwdr8ga6w7ca6j7skydxw4gbpwxdfdrn")))

(define-public crate-plr-0.2.0 (c (n "plr") (v "0.2.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rug") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 2)))) (h "0yh7j7i5aqkmhwfr2w7p7kyybia44ppxzbfw50ngf2494wk7k4ai")))

(define-public crate-plr-0.3.0 (c (n "plr") (v "0.3.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rug") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 2)))) (h "14qf8b1vmv51v9hxppi5kzwmqlqbag2f104zv2b3vvnlxx4hzhcy")))

(define-public crate-plr-0.3.1 (c (n "plr") (v "0.3.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rug") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "superslice") (r "^1.0.0") (d #t) (k 2)))) (h "0a8wnz354if0vlykv4pisj31y8m1a8kn0a3pnj36j3fswffrrixy")))

