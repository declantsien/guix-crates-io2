(define-module (crates-io #{3}# p pwr) #:use-module (crates-io))

(define-public crate-pwr-0.1.0 (c (n "pwr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08v5dim1qrr0h2f7dvyc2rv8c2fv6mpr7v8f4l9fz4llykyka6hf")))

(define-public crate-pwr-0.2.0 (c (n "pwr") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.32") (d #t) (k 0)))) (h "18nzxvl2qiiapdpld57xgqlmqy65qjx4z65kq31jsxx61c1kvis2")))

(define-public crate-pwr-0.2.1 (c (n "pwr") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.32") (d #t) (k 0)))) (h "1mszqzqchqvfg24785yi2gkjv1ww75hsxfwjfi0fqx2b9cppgd7k")))

(define-public crate-pwr-0.2.2 (c (n "pwr") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uom") (r "^0.32") (d #t) (k 0)))) (h "0p4k29ahbnfy2gk5kn08nwbpq5hzsx6i2w5d11dg5d7cypvp72k5")))

