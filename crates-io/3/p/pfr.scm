(define-module (crates-io #{3}# p pfr) #:use-module (crates-io))

(define-public crate-pfr-0.1.0 (c (n "pfr") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1bksxczmz3nisfrjn9w7963s3i6dk8krlcypvs8n8isljmw9nbkp")))

(define-public crate-pfr-0.1.1 (c (n "pfr") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0ds19749k35761bwkkdm8mn32wma17ixz0ib8p3ab5qgb74qm206")))

(define-public crate-pfr-0.2.0 (c (n "pfr") (v "0.2.0") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "062gk6qxyqkwc69x5jack1rna3q9wicwsy77l83n3vypmgsshd7p")))

(define-public crate-pfr-0.2.1 (c (n "pfr") (v "0.2.1") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.33") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1fd0r8yz94g4h4hbq6qrvgjz470qas2d5wyc12r63nfw4b1nhvxc")))

