(define-module (crates-io #{3}# p ptr) #:use-module (crates-io))

(define-public crate-ptr-0.1.0 (c (n "ptr") (v "0.1.0") (h "0m1r3agpybszznnsz3aylhb4sdnp2cc1a06178m1fs4q3wcdlqv1")))

(define-public crate-ptr-0.1.1 (c (n "ptr") (v "0.1.1") (h "0imh7hm0k3wvdw6fma1rqmm7vblskz1pgmjh89rn471j49z080wz")))

(define-public crate-ptr-0.1.2 (c (n "ptr") (v "0.1.2") (h "1kbxf8kqy6g4dlnsj3aj2njf6h51vgzx8170335ybmgsjrmcin6m")))

(define-public crate-ptr-0.1.3 (c (n "ptr") (v "0.1.3") (h "1dvxj09yf82siampc3f1kxv7s5y8vvh3g7rgcaws3vrjv4wp5yw1")))

(define-public crate-ptr-0.1.4 (c (n "ptr") (v "0.1.4") (h "1whznl8w7i1qybrjl0ipz0sb44fcfj6lv5d7m2c5aj8jyxv8ajcb")))

(define-public crate-ptr-0.2.0 (c (n "ptr") (v "0.2.0") (h "0ja7l8ajjmj4l1zag3k5ffka84aa07vr25mandd8r3kkvajmybm1")))

(define-public crate-ptr-0.2.1 (c (n "ptr") (v "0.2.1") (h "0mah3i9gism1v96cg6h4ch7w8hw0jqz9xhlaq1yfqmgak468qwl1")))

(define-public crate-ptr-0.2.2 (c (n "ptr") (v "0.2.2") (h "1y6q0hdqgj27v9jaq23jdj3r8472ncrgq8sacfrp5pxay79a0qhy")))

(define-public crate-ptr-0.2.3 (c (n "ptr") (v "0.2.3") (h "1kl7glrqxxmkn2nxslwrs5n4lpz5w90czx92m81afi2awj9q483n")))

