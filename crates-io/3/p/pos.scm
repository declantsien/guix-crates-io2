(define-module (crates-io #{3}# p pos) #:use-module (crates-io))

(define-public crate-pos-0.1.0 (c (n "pos") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "01ax0z6fmy3dra7bi76hihgwrskah8daq8c4hs1yhh9qrmgbnh8j")))

(define-public crate-pos-0.1.1 (c (n "pos") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "02ycgvbylblrch55ny8vckdl942nsfbkfwx4k9zw1rsb39wqhxb8")))

