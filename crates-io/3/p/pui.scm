(define-module (crates-io #{3}# p pui) #:use-module (crates-io))

(define-public crate-pui-0.1.0 (c (n "pui") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0cfr1c82kmcm0dfmcc0w4kjphkqh8ral7wb7dqx30xvig81pyyzz") (f (quote (("test") ("std") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-pui-0.1.1 (c (n "pui") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1pfiji1nn11lca3hxcl8rl3z65vlcqph9cxal0fry3iz6vckp4r8") (f (quote (("test") ("std" "atomic") ("nightly") ("default" "std") ("atomic") ("alloc"))))))

(define-public crate-pui-0.2.0 (c (n "pui") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0q33wx3wlrmzypn1hwvna74b5dqjamfi2m01g1yww8ip63j3vzvg") (f (quote (("test") ("std" "atomic") ("nightly") ("default" "std") ("atomic") ("alloc"))))))

(define-public crate-pui-0.3.0 (c (n "pui") (v "0.3.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0sbzjl8i1dhw1hq9q0wn1js9x8mb590dxhip7xd1l9n80lyzq8lr") (f (quote (("test") ("std" "atomic") ("nightly") ("default" "std") ("atomic") ("alloc"))))))

(define-public crate-pui-0.3.1 (c (n "pui") (v "0.3.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1vjgnd3lrsngqmi438sjqq6b8lhsn2c1lzd8ykjv8vvm66rppnah") (f (quote (("test") ("std" "crossbeam-utils") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-pui-0.4.0 (c (n "pui") (v "0.4.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0661nar0lpd20x1ml8d0q8jdz6l7w4lirgb4mfwpkw2nv8w2zkbz") (f (quote (("test") ("std" "crossbeam-utils") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-pui-0.4.1 (c (n "pui") (v "0.4.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "02rlim5jhc4m8i61jxnds4hicizs47x2pa4ly14ckr1awwxakwgy") (f (quote (("test") ("std" "crossbeam-utils") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-pui-0.5.0 (c (n "pui") (v "0.5.0") (d (list (d (n "pui-arena") (r "^0.5") (f (quote ("pui" "slotmap" "slab" "scoped"))) (d #t) (k 0)) (d (n "pui-cell") (r "^0.5") (d #t) (k 0)) (d (n "pui-core") (r "^0.5.1") (d #t) (k 0)) (d (n "pui-vec") (r "^0.5") (f (quote ("pui"))) (d #t) (k 0)))) (h "02fwsnnd81s235712hqz42j0i8waxjwmm18imal915jnl9j4rcsd") (f (quote (("std"))))))

(define-public crate-pui-0.5.1 (c (n "pui") (v "0.5.1") (d (list (d (n "pui-arena") (r "^0.5.1") (f (quote ("pui" "slotmap" "slab" "scoped"))) (o #t) (k 0)) (d (n "pui-cell") (r "^0.5.1") (d #t) (k 0)) (d (n "pui-core") (r "^0.5.2") (k 0)) (d (n "pui-vec") (r "^0.5.1") (f (quote ("pui"))) (o #t) (k 0)))) (h "10qsgs9f3smgh0harhbx63dfj01pf0znc1mz6gbiknki7fadbkkl") (f (quote (("std" "pui-core/std" "alloc") ("default" "std") ("alloc" "pui-arena" "pui-vec"))))))

