(define-module (crates-io #{3}# p pfg) #:use-module (crates-io))

(define-public crate-pfg-0.1.0 (c (n "pfg") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rss") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "15pybagj06pywf81js2w3d4ldqb5a40dn7fw9rrjss19d9i3qz26")))

(define-public crate-pfg-0.2.0 (c (n "pfg") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rss") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1js1xh6lb2x1yiivqama7q37lil43h5qxfk4w9bc36bmkxr7h1j6")))

(define-public crate-pfg-0.3.0 (c (n "pfg") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "rss") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "093danlgiaz0g2dnpkamn6v8phj7nwvy253c1jlcd21dbxhdxdsi")))

