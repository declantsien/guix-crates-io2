(define-module (crates-io #{3}# p px4) #:use-module (crates-io))

(define-public crate-px4-0.1.0 (c (n "px4") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "px4_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1pfjf18qszml1r8pkykql0jyh9lg14l1ij1mcv4hd65zmbf5nyhg")))

(define-public crate-px4-0.2.0 (c (n "px4") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "px4_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1y3wdh5pfm4l9z4llspvm19ps4l33h9z89hnb06f6iblf9ilkr6i")))

(define-public crate-px4-0.2.1 (c (n "px4") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "px4_macros") (r "^0.2.1") (d #t) (k 0)))) (h "0fvvcrhvkgl3z4c7py93jw7m5sary1wshdldgfdcl5g6gvyfqz0g")))

(define-public crate-px4-0.2.2 (c (n "px4") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "px4_macros") (r "^0.2.1") (d #t) (k 0)))) (h "00m9wvv4fx7r9xninan4004y6y4z9ym6z75y7ivwi1mmyx77bvxq")))

(define-public crate-px4-0.2.3 (c (n "px4") (v "0.2.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "px4_macros") (r "^0.2.1") (d #t) (k 0)))) (h "14hql7v5jb7kzss3lpw00r663xhglajsvrsj9rgci1rhys76sc6j")))

(define-public crate-px4-0.2.4 (c (n "px4") (v "0.2.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "px4_macros") (r "^0.2.4") (d #t) (k 0)))) (h "04j197qk4k2rpa5vx48kcqjy3mb5qarc3n2azbb7p43gja6dfc4x")))

