(define-module (crates-io #{3}# p psu) #:use-module (crates-io))

(define-public crate-psu-1.0.0 (c (n "psu") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "argh") (r "^0.1.10") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-serial") (r "^5.4.4") (d #t) (k 0)))) (h "1dcll0npwawd0mrafimwk2yz82852nk9f7s0vxbbpr2r4mq2pwhb")))

