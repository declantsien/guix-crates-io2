(define-module (crates-io #{3}# p pho) #:use-module (crates-io))

(define-public crate-pho-1.0.0 (c (n "pho") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive" "env" "unicode" "string" "wrap_help"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (d #t) (k 0)))) (h "0gwvyikizal2k3hg4l69li4ci56dyvxmmm6mnnp4ypb3xcaar152")))

(define-public crate-pho-1.0.1 (c (n "pho") (v "1.0.1") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive" "env" "unicode" "string" "wrap_help"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (d #t) (k 0)))) (h "0l5v0lyspkjz3a4yz46j1phlv0hn2m8c8spq86zpji09sfn0q7kb")))

(define-public crate-pho-1.0.2 (c (n "pho") (v "1.0.2") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive" "env" "unicode" "string" "wrap_help"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (d #t) (k 0)))) (h "0rgxyrbyvlg7187bn6qpb9bdl1phl05bckjfa5c3j8hxkxp6czxv")))

(define-public crate-pho-1.0.3 (c (n "pho") (v "1.0.3") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive" "env" "unicode" "string" "wrap_help"))) (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (f (quote ("gg-alloc"))) (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "0ggg289n6avyw0mjah1v8dpjd0jmg9k3br2j866pbr2kaf3m9fi2")))

(define-public crate-pho-1.0.4 (c (n "pho") (v "1.0.4") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive" "env" "unicode" "string" "wrap_help"))) (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (f (quote ("gg-alloc"))) (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "0hfwd0c8vsl957x361nrjs1674wy6820isgk5nv1slnqai0zg1pc")))

(define-public crate-pho-1.0.5 (c (n "pho") (v "1.0.5") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive" "env" "unicode" "string" "wrap_help"))) (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.37") (f (quote ("gg-alloc"))) (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "1d3rvlaic7fvkczcydv3gr8hdihhcwj1vvvzbmd61rbkp6i3rwfx")))

