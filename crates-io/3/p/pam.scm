(define-module (crates-io #{3}# p pam) #:use-module (crates-io))

(define-public crate-pam-0.0.1 (c (n "pam") (v "0.0.1") (h "1y5pp3hbf8j827bj39c3bgb187dr6dnnd8bkll6psqsbv5dq99ga")))

(define-public crate-pam-0.0.2 (c (n "pam") (v "0.0.2") (h "1pzqy4axyqspngmyjkwbnma1g0nzkx4y4418ll4h3l4v9qdhbbmg") (y #t)))

(define-public crate-pam-0.7.0 (c (n "pam") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pam-sys") (r "^0.5") (d #t) (k 0)) (d (n "rpassword") (r "^2.0") (d #t) (k 2)) (d (n "users") (r "^0.8") (d #t) (k 0)))) (h "15rhp57pdb54lcx37vymcimimpd1ma90lhm10iq08710kjaxqazs")))

(define-public crate-pam-0.8.0 (c (n "pam") (v "0.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (d #t) (k 0)) (d (n "pam-macros") (r "=0.0.3") (d #t) (k 0)) (d (n "pam-sys") (r "^1.0.0-alpha5") (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 2)) (d (n "users") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1fl5w8ankwx8fz3l75mr3w5s68nwjfss7mppv2av5v83472m7dca") (f (quote (("module") ("functions") ("default" "client") ("client" "users"))))))

