(define-module (crates-io #{3}# p pki) #:use-module (crates-io))

(define-public crate-pki-0.1.0 (c (n "pki") (v "0.1.0") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1k92zr75ah7d9bccv6k4f4ra3d6izxm7hcihrny4x34kpymxrv26") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-pki-0.1.1 (c (n "pki") (v "0.1.1") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wrmyspq0nkcbq48amndnps9q216anqcb5yqi9xaq8gx4b1ghcm6") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-pki-0.1.2 (c (n "pki") (v "0.1.2") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0n91jkzmfidz3vx10c2sqbcxn3mvjx1vr5isy8cyrn47rmbakdd0") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-pki-0.2.0 (c (n "pki") (v "0.2.0") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0f61pgmixi0l5g857w5fdc837mhxm2yq5jr2n3sjqcn4y2l7a30h") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-pki-0.2.1 (c (n "pki") (v "0.2.1") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "07rzpc61qhyxw947d55m0n4xzyrwh6f2c9zmsivg7vpvhxqdn916") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

(define-public crate-pki-0.2.2 (c (n "pki") (v "0.2.2") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 2)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9") (d #t) (k 0)) (d (n "rustls") (r "^0.23") (f (quote ("std" "tls12" "ring"))) (k 2)) (d (n "rustls-pki-types") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zs71vzryy0rk6p6pn57hxipddnrni5658jrkwplsvv8hk49gxzz") (f (quote (("vendored-openssl" "openssl/vendored") ("default"))))))

