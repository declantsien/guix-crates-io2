(define-module (crates-io #{3}# p ppg) #:use-module (crates-io))

(define-public crate-ppg-0.1.0 (c (n "ppg") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "06c91r4cw4q4h22yliaqw7bymlswdkkrfjfwp3gl27ww0b0n12kb")))

(define-public crate-ppg-0.2.0 (c (n "ppg") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1sifr510pyrm5mw1kl4pr35lq3nxxvzk4cgcy3aqv976rv1k2fbl")))

(define-public crate-ppg-0.3.0 (c (n "ppg") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0fk7jd5vg70jqxwrabmp6p9nwy6ydg42q77qp29r4lp1bwmlxf8i")))

(define-public crate-ppg-0.4.0 (c (n "ppg") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1r8va8rnnzn2n8say7vn8w95cjmkwv2cw2c2zy1vr1f9xncxi672")))

