(define-module (crates-io #{3}# p pds) #:use-module (crates-io))

(define-public crate-pds-0.1.0 (c (n "pds") (v "0.1.0") (h "1ji0cxwfxbzs9b2dsfwzg4yyl7fxwgnlaxf0h7lbabd5qxc3amf4")))

(define-public crate-pds-0.2.0 (c (n "pds") (v "0.2.0") (d (list (d (n "extended-primitives") (r "^0.3.1") (d #t) (k 0)) (d (n "fasthash") (r "^0.4") (d #t) (k 0)))) (h "1kd6mydhw8bxyqxwsk11q5apd6ryxpnvcla4x1d266mpvddmgp7h")))

