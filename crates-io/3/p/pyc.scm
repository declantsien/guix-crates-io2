(define-module (crates-io #{3}# p pyc) #:use-module (crates-io))

(define-public crate-pyc-0.1.0 (c (n "pyc") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "python_object") (r "^0.1.0") (d #t) (k 0)))) (h "14py7kpiq9nnnkdrw3gf61gxbxdjrqpjcv0yzl30kw5zi7gxz46g")))

