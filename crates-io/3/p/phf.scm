(define-module (crates-io #{3}# p phf) #:use-module (crates-io))

(define-public crate-phf-0.0.1 (c (n "phf") (v "0.0.1") (d (list (d (n "phf_mac") (r "^0.0.1") (d #t) (k 0)) (d (n "phf_shared") (r "^0.0.1") (d #t) (k 0)))) (h "0gqzz17gnnp1bbzrmyizv9y2gqxa2lqc5v8gm55rjlf3achm4fyb")))

(define-public crate-phf-0.1.0 (c (n "phf") (v "0.1.0") (d (list (d (n "phf_mac") (r "= 0.1.0") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.1.0") (d #t) (k 0)))) (h "0l3bn7w503051gfsppj025yxdc30spj7dhznx62b09bjxcvaw24y")))

(define-public crate-phf-0.2.0 (c (n "phf") (v "0.2.0") (d (list (d (n "phf_mac") (r "= 0.2.0") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.2.0") (d #t) (k 0)))) (h "1l5s9c49ia5b7lnqfwn723w8vp6xc0hhdk26qj0pgq2x4gqrdg3j")))

(define-public crate-phf-0.3.0 (c (n "phf") (v "0.3.0") (d (list (d (n "phf_mac") (r "= 0.3.0") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.3.0") (d #t) (k 0)))) (h "0mn11khq2jnmrzlrfyazcfkdvj61fkadsnh239l48cjgyl4hqvy5")))

(define-public crate-phf-0.4.0 (c (n "phf") (v "0.4.0") (d (list (d (n "phf_mac") (r "= 0.4.0") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.4.0") (d #t) (k 0)))) (h "1dc8i66ifrn4g2kxn91kwvfyzl9z7ngnx08iznn90a5bnaarb68y")))

(define-public crate-phf-0.4.1 (c (n "phf") (v "0.4.1") (d (list (d (n "phf_mac") (r "= 0.4.1") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.4.1") (d #t) (k 0)))) (h "170f3fajrn27hi3si6i3p629kizbz0pchq8bk925vlpcm68rp8hl")))

(define-public crate-phf-0.4.2 (c (n "phf") (v "0.4.2") (d (list (d (n "phf_mac") (r "= 0.4.2") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.4.2") (d #t) (k 0)))) (h "0cf35i1ymp5bw2wpacjg5v5m6wf7xl92n621hdqsa0cm295bz3h4")))

(define-public crate-phf-0.4.3 (c (n "phf") (v "0.4.3") (d (list (d (n "phf_mac") (r "= 0.4.3") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.4.3") (d #t) (k 0)))) (h "1alqph0vgzlkzxv91abimd430lva7dhp76i8clhc2siq75l6zmay")))

(define-public crate-phf-0.4.4 (c (n "phf") (v "0.4.4") (d (list (d (n "phf_mac") (r "= 0.4.4") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.4.4") (d #t) (k 0)))) (h "0fjq2nwqjqgas6ykr465lf8pbg31gpz5is7yal0hpvhwpazxxzpd")))

(define-public crate-phf-0.4.5 (c (n "phf") (v "0.4.5") (d (list (d (n "phf_mac") (r "= 0.4.5") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.4.5") (d #t) (k 0)))) (h "163vr2c853lm3yv2l7jdnypp2a27k7bq6c3sz6z8k8gkr0dz8zqy")))

(define-public crate-phf-0.4.6 (c (n "phf") (v "0.4.6") (d (list (d (n "phf_mac") (r "= 0.4.6") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.4.6") (d #t) (k 0)))) (h "1ld5cf0vvs9dcvprib9rr0bz1n149shzg5zdxamvrc6qjn5ibk9k")))

(define-public crate-phf-0.4.8 (c (n "phf") (v "0.4.8") (d (list (d (n "phf_mac") (r "= 0.4.8") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.4.8") (d #t) (k 0)))) (h "0cnsdg8snwybz5nix8q6g49jbndqi771bmfwvdxinpq6208acnji")))

(define-public crate-phf-0.4.9 (c (n "phf") (v "0.4.9") (d (list (d (n "phf_mac") (r "= 0.4.9") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.4.9") (d #t) (k 0)))) (h "07d3npsz7jdddalwssir3jjbwwrwl9945yqxaj4ccy10754mddl1")))

(define-public crate-phf-0.5.0 (c (n "phf") (v "0.5.0") (d (list (d (n "phf_mac") (r "= 0.5.0") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.5.0") (d #t) (k 0)))) (h "1qg59fygf8rkrxbya6s8phqbxapqwz1g4hxw6lxsxj755i456f9h")))

(define-public crate-phf-0.6.0 (c (n "phf") (v "0.6.0") (d (list (d (n "phf_macros") (r "= 0.6.0") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.6.0") (d #t) (k 0)))) (h "1nb8yihavifmkb8d9v1mkg687w9pwsa3vikhy0dymkayx4c4svrk")))

(define-public crate-phf-0.6.1 (c (n "phf") (v "0.6.1") (d (list (d (n "phf_macros") (r "= 0.6.1") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.6.1") (d #t) (k 0)))) (h "1fmdr35v76vx3jl0byl8svbzk5k47clf6vblzd8k89dk6j42qri5")))

(define-public crate-phf-0.6.2 (c (n "phf") (v "0.6.2") (d (list (d (n "phf_macros") (r "= 0.6.2") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.6.2") (d #t) (k 0)))) (h "06dph9p2r6aiv9zi9v4hlr3pi15bzwv7gzpp7qq4hz8l2x6s0mjk") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.6.3 (c (n "phf") (v "0.6.3") (d (list (d (n "phf_macros") (r "= 0.6.3") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.6.3") (d #t) (k 0)))) (h "07390dlvmvk7mgia0mcbdv90vj4zwj8122w8mnh52xdga516gg06") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.6.4 (c (n "phf") (v "0.6.4") (d (list (d (n "phf_macros") (r "= 0.6.4") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.6.4") (d #t) (k 0)))) (h "0h9w1dkgcxqf5sv2ab3ivfvlqsg2m0s50xw7m1q6p7sw2wp5f3y9") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.6.5 (c (n "phf") (v "0.6.5") (d (list (d (n "phf_macros") (r "= 0.6.5") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.6.5") (d #t) (k 0)))) (h "1l8yzh16wc91cl7qjh3c6hvy8i79fhp3xy2jlsi6syajxq78jz3y") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.6.6 (c (n "phf") (v "0.6.6") (d (list (d (n "phf_macros") (r "= 0.6.6") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.6.6") (d #t) (k 0)))) (h "03hfkd7ihflzs7qvahs3dnc5hwfjmm0ycnxc1hpz39gmgmbmxizn") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.6.7 (c (n "phf") (v "0.6.7") (d (list (d (n "phf_macros") (r "= 0.6.7") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.6.7") (d #t) (k 0)))) (h "15mp9q2g1jm4lpxrggj9lrc52v9j0r1ipf3b78r4rjp9ypy795y0") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.6.8 (c (n "phf") (v "0.6.8") (d (list (d (n "phf_macros") (r "= 0.6.8") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.6.8") (d #t) (k 0)))) (h "04f2829qaz9m0n8b220mmbqx5gvqvdxa3r4a759ckhy55zrs33rd") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.6.9 (c (n "phf") (v "0.6.9") (d (list (d (n "phf_macros") (r "= 0.6.9") (d #t) (k 2)) (d (n "phf_shared") (r "= 0.6.9") (d #t) (k 0)))) (h "1ry2gsn44dh2qrh9yvnmz4hb1lxyqbhdbm56k0yffw8sdw9rlv4j") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.6.10 (c (n "phf") (v "0.6.10") (d (list (d (n "phf_shared") (r "= 0.6.10") (d #t) (k 0)))) (h "0pflkzqjjx41ljfl533yshg7xp3ln5h3iva0is83a5h9xdyh4s17")))

(define-public crate-phf-0.6.11 (c (n "phf") (v "0.6.11") (d (list (d (n "phf_shared") (r "= 0.6.11") (d #t) (k 0)))) (h "12b26zbawm0p9bgbm13x39f12b11hha4x93b3g48v17l0pdsnrln")))

(define-public crate-phf-0.6.12 (c (n "phf") (v "0.6.12") (d (list (d (n "phf_shared") (r "= 0.6.12") (d #t) (k 0)))) (h "1qwwdk8534394vbpcsghjy1hykrgp0n18z0h9phdm6pki0gcpvqn")))

(define-public crate-phf-0.6.13 (c (n "phf") (v "0.6.13") (d (list (d (n "phf_shared") (r "= 0.6.13") (d #t) (k 0)))) (h "1qhnnamcv5si0z9591dxq3lgrfmysci0zjsgwrljmx196s8d3k50")))

(define-public crate-phf-0.6.14 (c (n "phf") (v "0.6.14") (d (list (d (n "phf_shared") (r "= 0.6.14") (d #t) (k 0)))) (h "0qpnrprzrc13411iqc6w9g0hqhjmx7r1z98jyyzqdv7frczzl0wv")))

(define-public crate-phf-0.6.15 (c (n "phf") (v "0.6.15") (d (list (d (n "phf_shared") (r "= 0.6.15") (d #t) (k 0)))) (h "1kmckwld94qrbn4nh7zslj7f4x0jskdnzn4mlpngn33i68m1fpfl")))

(define-public crate-phf-0.6.17 (c (n "phf") (v "0.6.17") (d (list (d (n "phf_shared") (r "= 0.6.17") (d #t) (k 0)))) (h "0ldr574phasml7mqnl3x16xjlcsq8i2hrkjm82xad1vr1w72x4ix")))

(define-public crate-phf-0.6.18 (c (n "phf") (v "0.6.18") (d (list (d (n "phf_shared") (r "= 0.6.18") (d #t) (k 0)))) (h "1krqdd4ws7hhmj51mm0k9dg9jaj6gzk5822ig2bmx2n8bpz8yqiv")))

(define-public crate-phf-0.6.19 (c (n "phf") (v "0.6.19") (d (list (d (n "phf_shared") (r "= 0.6.19") (d #t) (k 0)))) (h "0pn6hqxqv893kwy05ingniljxinrfi13d72bx0rp1liicdl6s96k")))

(define-public crate-phf-0.7.0 (c (n "phf") (v "0.7.0") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.0") (d #t) (k 0)))) (h "11g4h2n4s7i9w2f2hzav4pfb2n65n6fk5s90swnky3b01calriq9")))

(define-public crate-phf-0.7.1 (c (n "phf") (v "0.7.1") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.1") (d #t) (k 0)))) (h "0jdlvsr7ardqbiqvarzbpfl3psvmm2dza1aznhlgl8licj6ym2pd")))

(define-public crate-phf-0.7.2 (c (n "phf") (v "0.7.2") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.2") (d #t) (k 0)))) (h "021b4iwi3zkaxhkjp1wiza6nl9pd9biqy23r0gdzbs3j5rvdwp7g")))

(define-public crate-phf-0.7.3 (c (n "phf") (v "0.7.3") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.3") (d #t) (k 0)))) (h "19jh07q6vf9wz1vrgsj9fd13ri52wlrn57z6js4322nbmsh4m29x")))

(define-public crate-phf-0.7.4 (c (n "phf") (v "0.7.4") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.4") (d #t) (k 0)))) (h "1n0q2fi3najxxm1jg34k35l0yv53nn4pfjldnidm7i2sgfq3paar")))

(define-public crate-phf-0.7.5 (c (n "phf") (v "0.7.5") (d (list (d (n "debug-builders") (r "^0.1") (d #t) (k 0)) (d (n "phf_shared") (r "= 0.7.5") (d #t) (k 0)))) (h "1p6sy06cjf9qz4f9y0hhfla1zmh7nzni5cqdq9db6gq8s9c636zy")))

(define-public crate-phf-0.7.6 (c (n "phf") (v "0.7.6") (d (list (d (n "phf_shared") (r "= 0.7.6") (d #t) (k 0)))) (h "19p8nwhzr1y8vxr36hi3y3zaxgypygjjjcd03pssgfh2al13zckj") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.7 (c (n "phf") (v "0.7.7") (d (list (d (n "phf_shared") (r "= 0.7.7") (d #t) (k 0)))) (h "07nc7sjvhajmm1pkvk1rjfz1x8ka2xl24h8llz83ingwv7953ldd") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.8 (c (n "phf") (v "0.7.8") (d (list (d (n "phf_shared") (r "= 0.7.8") (d #t) (k 0)))) (h "1djd9rw6c5cyi8dxdh6hl3k97hspr6q4pjs5l44xz58b1yd6sdxj") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.9 (c (n "phf") (v "0.7.9") (d (list (d (n "phf_shared") (r "= 0.7.9") (d #t) (k 0)))) (h "16rbyffvynzjw3gscafj98ckhm5dbjjjrxsyx9r4v3vc0l8fmmys") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.10 (c (n "phf") (v "0.7.10") (d (list (d (n "phf_shared") (r "= 0.7.10") (d #t) (k 0)))) (h "1f4j6836v0yxsbmmzb1fpxrm3rryamzczcwqxz3v7fldj3g8kjnj") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.11 (c (n "phf") (v "0.7.11") (d (list (d (n "phf_shared") (r "= 0.7.11") (d #t) (k 0)))) (h "10v02db3894kas2q8f4lxg1g30r4bfvminwr1p9xx4a7v05rfskg") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.12 (c (n "phf") (v "0.7.12") (d (list (d (n "phf_shared") (r "= 0.7.12") (d #t) (k 0)))) (h "1j4da2yf5kra1qp8g3d4kcas4054j4n42p7v5595y9v3bdqgmmp5") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.13 (c (n "phf") (v "0.7.13") (d (list (d (n "phf_shared") (r "= 0.7.13") (d #t) (k 0)))) (h "11wzx636xi51paqnq0ivzmn6qkrifc60wbjq864gfb94d2gpybhc") (f (quote (("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.14 (c (n "phf") (v "0.7.14") (d (list (d (n "phf_shared") (r "= 0.7.14") (d #t) (k 0)))) (h "09ph2bpkclnlzh1748fawqdj3z0qpxd3c2786asskd70y92rsza4") (f (quote (("unicase" "phf_shared/unicase") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.15 (c (n "phf") (v "0.7.15") (d (list (d (n "phf_shared") (r "= 0.7.15") (d #t) (k 0)))) (h "0i1yb8yag8f80grzi71ygls3mh66i83v669mgpnwp9krw58nk28p") (f (quote (("unicase" "phf_shared/unicase") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.16 (c (n "phf") (v "0.7.16") (d (list (d (n "phf_shared") (r "= 0.7.16") (d #t) (k 0)))) (h "06kab9n2l1gjy67cxfs4kfij0ydmb2ifllwinssh2k72dn97bj2j") (f (quote (("unicase" "phf_shared/unicase") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.17 (c (n "phf") (v "0.7.17") (d (list (d (n "phf_shared") (r "= 0.7.17") (d #t) (k 0)))) (h "1vxrmnsfgp7h1z1ffsqkjvdpq2nfxmmx0gnkg8fi523ff2mm6a7h") (f (quote (("unicase" "phf_shared/unicase") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.18 (c (n "phf") (v "0.7.18") (d (list (d (n "phf_shared") (r "= 0.7.18") (d #t) (k 0)))) (h "142bw2h2mswwd8cms5q8nfn89z7iwdhipw3wznk9123jy16xj7cp") (f (quote (("unicase" "phf_shared/unicase") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.19 (c (n "phf") (v "0.7.19") (d (list (d (n "phf_shared") (r "= 0.7.19") (d #t) (k 0)))) (h "1dbl7bklp04bkf88y50rp5nknlc9sk0j70sw1c8r5r8k3d8l3jwm") (f (quote (("unicase" "phf_shared/unicase") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.20 (c (n "phf") (v "0.7.20") (d (list (d (n "phf_shared") (r "= 0.7.20") (d #t) (k 0)))) (h "00ljk677ddsxj89gf2ajqdq4kvqwpj83yw3mgdm88pxvawhgnshc") (f (quote (("unicase" "phf_shared/unicase") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.21 (c (n "phf") (v "0.7.21") (d (list (d (n "phf_shared") (r "= 0.7.21") (d #t) (k 0)))) (h "1z22pr7r3sjn188nxbv21hi2z1wsjici2866v0afwa0g5515ccnb") (f (quote (("unicase" "phf_shared/unicase") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.22 (c (n "phf") (v "0.7.22") (d (list (d (n "phf_shared") (r "= 0.7.22") (d #t) (k 0)))) (h "1wk2mnsi9f9ws9gx4bqr3gkqzfdwdpsmal9297h4i5ssqx2a4dvx") (f (quote (("unicase" "phf_shared/unicase") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.23 (c (n "phf") (v "0.7.23") (d (list (d (n "phf_shared") (r "= 0.7.23") (d #t) (k 0)))) (h "19mr9nwiwajpln2swv403g0cjqd21ixcfll8171z8hmj4airvhnf") (f (quote (("unicase" "phf_shared/unicase") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.7.24 (c (n "phf") (v "0.7.24") (d (list (d (n "phf_macros") (r "^0.7.24") (o #t) (d #t) (k 0)) (d (n "phf_shared") (r "^0.7.24") (d #t) (k 0)))) (h "066xwv4dr6056a9adlkarwp4n94kbpwngbmd47ngm3cfbyw49nmk") (f (quote (("unicase" "phf_shared/unicase") ("macros" "phf_macros") ("core" "phf_shared/core"))))))

(define-public crate-phf-0.8.0 (c (n "phf") (v "0.8.0") (d (list (d (n "phf_macros") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "phf_shared") (r "^0.8.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (o #t) (d #t) (k 0)))) (h "04pyv8bzqvw69rd5dynd5nb85py1hf7wa4ixyhrvdz1l5qin3yrx") (f (quote (("unicase" "phf_shared/unicase") ("std" "phf_shared/std") ("macros" "phf_macros" "proc-macro-hack") ("default" "std"))))))

(define-public crate-phf-0.9.0 (c (n "phf") (v "0.9.0") (d (list (d (n "phf_macros") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "phf_shared") (r "^0.9.0") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (o #t) (d #t) (k 0)))) (h "0dsgl8gqaia5lwg9sjlg4ccihn5dija2drg0gi2sjz1samkqpb5j") (f (quote (("unicase" "phf_shared/unicase") ("uncased" "phf_shared/uncased") ("std" "phf_shared/std") ("macros" "phf_macros" "proc-macro-hack") ("default" "std"))))))

(define-public crate-phf-0.9.1 (c (n "phf") (v "0.9.1") (d (list (d (n "phf_macros") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "phf_shared") (r "^0.9.0") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (o #t) (d #t) (k 0)))) (h "0sk9arhiv4hkg3hbmbxnpjvr0bffhyz7kpb2pn86mn6ia4c566xj") (f (quote (("unicase" "phf_shared/unicase") ("uncased" "phf_shared/uncased") ("std" "phf_shared/std") ("macros" "phf_macros" "proc-macro-hack") ("default" "std")))) (y #t)))

(define-public crate-phf-0.10.0 (c (n "phf") (v "0.10.0") (d (list (d (n "phf_macros") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "phf_shared") (r "^0.10.0") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (o #t) (d #t) (k 0)))) (h "17s0k0z1rjczk37cpbmn718h7dkqci1kk9w2spbmjjwc06qkvz5r") (f (quote (("unicase" "phf_shared/unicase") ("uncased" "phf_shared/uncased") ("std" "phf_shared/std") ("macros" "phf_macros" "proc-macro-hack") ("default" "std"))))))

(define-public crate-phf-0.10.1 (c (n "phf") (v "0.10.1") (d (list (d (n "phf_macros") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "phf_shared") (r "^0.10.0") (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0naj8n5nasv5hj5ldlva3cl6y3sv7zp3kfgqylhbrg55v3mg3fzs") (f (quote (("unicase" "phf_shared/unicase") ("uncased" "phf_shared/uncased") ("std" "phf_shared/std") ("macros" "phf_macros" "proc-macro-hack") ("default" "std"))))))

(define-public crate-phf-0.11.0 (c (n "phf") (v "0.11.0") (d (list (d (n "phf_macros") (r "^0.11.0") (o #t) (d #t) (k 0)) (d (n "phf_shared") (r "^0.11.0") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0cc1ysij04ywnf35gqwiw7lw2bz37kkbwgdxh7c7q7lddjagl927") (f (quote (("uncased" "phf_shared/uncased") ("std" "phf_shared/std") ("macros" "phf_macros") ("default" "std")))) (s 2) (e (quote (("unicase" "phf_macros?/unicase" "phf_shared/unicase")))) (r "1.60")))

(define-public crate-phf-0.11.1 (c (n "phf") (v "0.11.1") (d (list (d (n "phf_macros") (r "^0.11.1") (o #t) (d #t) (k 0)) (d (n "phf_shared") (r "^0.11.1") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1354fbpb52cp9gs5mlkaygc5qhdx6r07rfv3xy482m4kvqsnb34j") (f (quote (("uncased" "phf_shared/uncased") ("std" "phf_shared/std") ("macros" "phf_macros") ("default" "std")))) (s 2) (e (quote (("unicase" "phf_macros?/unicase" "phf_shared/unicase")))) (r "1.60")))

(define-public crate-phf-0.11.2 (c (n "phf") (v "0.11.2") (d (list (d (n "phf_macros") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "phf_shared") (r "^0.11.2") (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1p03rsw66l7naqhpgr1a34r9yzi1gv9jh16g3fsk6wrwyfwdiqmd") (f (quote (("uncased" "phf_shared/uncased") ("std" "phf_shared/std") ("macros" "phf_macros") ("default" "std")))) (s 2) (e (quote (("unicase" "phf_macros?/unicase" "phf_shared/unicase")))) (r "1.60")))

