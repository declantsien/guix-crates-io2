(define-module (crates-io #{3}# p pcp) #:use-module (crates-io))

(define-public crate-pcp-0.1.0 (c (n "pcp") (v "0.1.0") (d (list (d (n "clap") (r "^1.5.2") (d #t) (k 0)) (d (n "ctrlc") (r "^1.0.0") (f (quote ("nightly"))) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.7") (d #t) (k 0)) (d (n "quickersort") (r "^1.0.0") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.6") (d #t) (k 0)) (d (n "tabwriter") (r "^0.1.24") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "0mqjr52kpl90qjbz4rycsc74l8qyj7cd582mqlzd709z5lvnrkxr")))

(define-public crate-pcp-0.2.0 (c (n "pcp") (v "0.2.0") (d (list (d (n "clap") (r "^1.5.2") (d #t) (k 0)) (d (n "ctrlc") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.7") (d #t) (k 0)) (d (n "quickersort") (r "^1.0.0") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1.6") (d #t) (k 0)) (d (n "tabwriter") (r "^0.1.24") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "0sypm0grr5b3m1q1vg5rq657b5frsr0p5x6aracwi816m18lhs22")))

