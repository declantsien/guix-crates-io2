(define-module (crates-io #{3}# p pty) #:use-module (crates-io))

(define-public crate-pty-0.1.0 (c (n "pty") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0icd8ga4ybars1andv22akbgl0z82si97cmxdqrgbg5csb59z4a6")))

(define-public crate-pty-0.1.1 (c (n "pty") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1a21q7q2ifcg4j1k0gj81cj2v3c2xz3f2rb45sjdfsr1jqb9ha24")))

(define-public crate-pty-0.1.2 (c (n "pty") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "08m1yv70whirq4k3cc536072hmy61iqhq5xmhn39sgcbfq7nx4na")))

(define-public crate-pty-0.1.3 (c (n "pty") (v "0.1.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1mkrb7ckradbrn74fm8rl45banwpmpas7dnsj6d8x71fi94d8999")))

(define-public crate-pty-0.1.4 (c (n "pty") (v "0.1.4") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0g1bs15fy56vqdv86lj4dfw5pbvmzb853clqxdkh2s8r3bpkm388")))

(define-public crate-pty-0.1.5 (c (n "pty") (v "0.1.5") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)))) (h "0xi3vvg15vgn00sspq0djgrj458xkjjhgj5bw9ng8lvy93qa33dc")))

(define-public crate-pty-0.1.6 (c (n "pty") (v "0.1.6") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.4") (d #t) (k 0)))) (h "1ga7la7zpw1brwkifw7k88bspjavp9iq7vwmqnpjbxjv8655id9g") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-pty-0.2.0 (c (n "pty") (v "0.2.0") (d (list (d (n "chan") (r "^0.1.18") (d #t) (k 0)) (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1i76j2k992b6sbq2fzxrpr7ljshgn55fp41cb27n3q60rddyqxn2") (f (quote (("unstable") ("travis" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default") ("debug"))))))

(define-public crate-pty-0.2.1 (c (n "pty") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bi5hg7b9q743sdda0spnbfq50qnf0sqy5scf202kid40xskr5g7") (f (quote (("unstable") ("travis" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default") ("debug"))))))

(define-public crate-pty-0.2.2 (c (n "pty") (v "0.2.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.18") (d #t) (k 0)))) (h "1s2ybkzn195nhdnl0izm44r1czbg7sc3xpy5wjs1x636b4jks3zm") (f (quote (("unstable") ("travis" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default") ("debug"))))))

