(define-module (crates-io #{3}# p pew) #:use-module (crates-io))

(define-public crate-pew-0.1.0 (c (n "pew") (v "0.1.0") (h "0y38xl79ij45fmqkcq5ffxwqrzm8vxk775z6c140xhjm548wg2vq")))

(define-public crate-pew-0.1.1 (c (n "pew") (v "0.1.1") (h "1jx4b523b60av60dvh4x151ja31znkmrbif3iac7yrrvsz1q0xsf")))

(define-public crate-pew-0.1.2 (c (n "pew") (v "0.1.2") (h "0vsarf8a3gq3bx0lbcwndvdh64pv3r3l7cmwgfsma5c0av4qw711")))

(define-public crate-pew-0.2.0 (c (n "pew") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 2)))) (h "0rq48mpr402hlnml5wdx6jcvnfmcv5d7iq69khb2vx97cby1dnww")))

(define-public crate-pew-0.2.1 (c (n "pew") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 2)))) (h "0p6rjj7k4f5xhnpnxfn9p6glc2d545vvf0jiav1mi36nbs1n6mqm")))

(define-public crate-pew-0.2.2 (c (n "pew") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 2)))) (h "0mla446xx6726hfjpk384fxlbmga30p96d5ahybs8w644alnxljb")))

(define-public crate-pew-0.2.3 (c (n "pew") (v "0.2.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "rand") (r "^0.4.0") (d #t) (k 2)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "01cavbdfzazv0pjmk8arjbvs58jn1pl9vqzfykrza1phfmhvkmkh")))

