(define-module (crates-io #{3}# p pdu) #:use-module (crates-io))

(define-public crate-pdu-1.0.0-beta1 (c (n "pdu") (v "1.0.0-beta1") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.7") (d #t) (k 2)) (d (n "roxmltree") (r "~0.7") (d #t) (k 2)))) (h "08sr4awqm22ql5ickpa9sqsxmwjmxznzfihsb70yv4ax6hcibh1q") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.0.0-beta2 (c (n "pdu") (v "1.0.0-beta2") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.7") (d #t) (k 2)) (d (n "roxmltree") (r "~0.7") (d #t) (k 2)))) (h "10z7q1wvhz2na5lv8r72r8p5wm22jq7mgm07hgj5l3zc7f6sdmy7") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.0.0-beta3 (c (n "pdu") (v "1.0.0-beta3") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.7") (d #t) (k 2)) (d (n "roxmltree") (r "~0.9") (d #t) (k 2)))) (h "0zyf6m8dwmqlrrgkbz82f16p6gbwzq65rv329iwca8gkzl0cv20r") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.0.0 (c (n "pdu") (v "1.0.0") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.7") (d #t) (k 2)) (d (n "roxmltree") (r "~0.9") (d #t) (k 2)))) (h "09yc66gdfa7h3blwf72kycxzwk3lxx9ifm2mc4i4qhcs7v3ik3q5") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.0.1 (c (n "pdu") (v "1.0.1") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.7") (d #t) (k 2)) (d (n "roxmltree") (r "~0.9") (d #t) (k 2)))) (h "1mr0h98kzb12fmwcg3sbzg0rdjcidskav853xisrc6rjxk5rc1z7") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.1.0 (c (n "pdu") (v "1.1.0") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.8") (d #t) (k 2)) (d (n "roxmltree") (r "~0.14") (f (quote ("std"))) (d #t) (k 2)))) (h "1ppd8x7866931cz03w9kx3s2i4djjsiznm9wf207in0zfbhzcmr2") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.2.0 (c (n "pdu") (v "1.2.0") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.8") (d #t) (k 2)) (d (n "roxmltree") (r "~0.14") (f (quote ("std"))) (d #t) (k 2)))) (h "0fimsvzy1msfw5hp4xzir4szxkqi6ard62h90ilvlaisp0hyraaj") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.3.0 (c (n "pdu") (v "1.3.0") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.8") (d #t) (k 2)) (d (n "roxmltree") (r "~0.14") (f (quote ("std"))) (d #t) (k 2)))) (h "06vja6a3rb43gbciyl80d5qmy695vwfbi0wyysh3n53wpkwq4cmc") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.3.1 (c (n "pdu") (v "1.3.1") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.8") (d #t) (k 2)) (d (n "roxmltree") (r "~0.14") (f (quote ("std"))) (d #t) (k 2)))) (h "067zrp6j7x6myfxzsrcy0jsnfzi109pw83pl95gnqq80p4wsqqma") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.4.0 (c (n "pdu") (v "1.4.0") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.8") (d #t) (k 2)) (d (n "roxmltree") (r "~0.14") (f (quote ("std"))) (d #t) (k 2)))) (h "11zmq907gja3d3s9fdlarnz5wb9wbj6ri2l4nkhq8h2j111mq3a5") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.4.1 (c (n "pdu") (v "1.4.1") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.8") (d #t) (k 2)) (d (n "roxmltree") (r "~0.14") (f (quote ("std"))) (d #t) (k 2)))) (h "0la5pb62cxsvkjdgn2qnj6g4ic6x56a36qgxigmfan06piw1j2f0") (f (quote (("std") ("default" "std"))))))

(define-public crate-pdu-1.4.2 (c (n "pdu") (v "1.4.2") (d (list (d (n "base16") (r "~0.2") (d #t) (k 2)) (d (n "pcap") (r "~0.9") (d #t) (k 2)) (d (n "roxmltree") (r "~0.14") (f (quote ("std"))) (d #t) (k 2)))) (h "1kpl69y0mlqlfrsqy3qz714pxp6b8mxl9zfcjnfvp7bpzgsslmrr") (f (quote (("std") ("default" "std"))))))

