(define-module (crates-io #{3}# p psi) #:use-module (crates-io))

(define-public crate-psi-0.1.0 (c (n "psi") (v "0.1.0") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.1") (d #t) (k 2)))) (h "0dqx09fbi6mqsifik6hllj5lqr1pm1k2i444v5gw5978qh0a9x2v")))

(define-public crate-psi-0.1.1 (c (n "psi") (v "0.1.1") (d (list (d (n "epoll") (r "^4.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.1") (d #t) (k 2)))) (h "130v1sm5g9g11kin4zw9lpn8a08pa2pm8f6gmc3l4jqmagjy3qjg")))

(define-public crate-psi-0.1.2 (c (n "psi") (v "0.1.2") (d (list (d (n "epoll") (r "^4.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.1") (d #t) (k 2)))) (h "06j0zaa8x6qyk34lqs87lccczkf8nngzazmvlmjikchx5dgvccbi") (f (quote (("monitor" "epoll") ("default" "monitor"))))))

