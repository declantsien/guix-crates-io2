(define-module (crates-io #{3}# p pk2) #:use-module (crates-io))

(define-public crate-pk2-0.1.0 (c (n "pk2") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)))) (h "06sk8w3v6qycvxhhwhikqv7b10hc3nxq415plyznzxphmhp354fq") (f (quote (("euc-kr" "encoding_rs") ("default" "euc-kr"))))))

(define-public crate-pk2-0.2.0 (c (n "pk2") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.2") (d #t) (k 2)) (d (n "byteorder") (r "^1.5") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0hx94w7524ly10hccs6fqx40g586cf59hfhkmbgadd9vry1jz6w2") (f (quote (("euc-kr" "encoding_rs") ("default" "euc-kr"))))))

