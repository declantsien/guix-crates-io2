(define-module (crates-io #{3}# p ppp) #:use-module (crates-io))

(define-public crate-ppp-1.0.0 (c (n "ppp") (v "1.0.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1vixwv0imxh64fybld2wpa7ikhb0kq3vww4n3fj66pl4q4pcny6k")))

(define-public crate-ppp-1.0.1 (c (n "ppp") (v "1.0.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1wssg7v0qga228ry1b0xgqn4krjziipydiwjcdpsr20rsl2wang8")))

(define-public crate-ppp-1.1.0 (c (n "ppp") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "05l1y7raab3mkw3zc3n69jz76kqw3fmzvrqfyw18p21a71ish0pi")))

(define-public crate-ppp-1.2.0 (c (n "ppp") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "03cmsfxqsfd30nnh09blfpp9csd5cy7v9vzz8sl54yqjrgmjazwz")))

(define-public crate-ppp-1.2.1 (c (n "ppp") (v "1.2.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0cyjvacp11hga4p4h139h1lwjp4lwc579mm53l2wqrz27zqllfax")))

(define-public crate-ppp-1.2.2 (c (n "ppp") (v "1.2.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0ws484b99qbz9w13yj3awd9nxdn5a7f53y5yhccg393q1pw14z5w")))

(define-public crate-ppp-2.0.0 (c (n "ppp") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.4") (f (quote ("criterion" "flamegraph" "protobuf"))) (d #t) (t "cfg(unix)") (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "09mwd6rwamb8vbiib6ccchplpfcj9a52kff9dsj9f0bnlgd536vh")))

(define-public crate-ppp-2.1.0 (c (n "ppp") (v "2.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.6") (f (quote ("criterion" "flamegraph" "protobuf"))) (d #t) (t "cfg(unix)") (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lz6pb2r945zhxclhqf4b342xdgz74fj8ckwfbywhgkwjlibj554") (f (quote (("default"))))))

(define-public crate-ppp-2.2.0 (c (n "ppp") (v "2.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.6") (f (quote ("criterion" "flamegraph" "protobuf"))) (d #t) (t "cfg(unix)") (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lghvclpylqjz0rr4hjnpr83xr9kpjyii5dg2j74fg3lvpbh3nc2") (f (quote (("default"))))))

