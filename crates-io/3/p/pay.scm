(define-module (crates-io #{3}# p pay) #:use-module (crates-io))

(define-public crate-pay-0.0.1-alpha.1 (c (n "pay") (v "0.0.1-alpha.1") (d (list (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 0)))) (h "0bp967rbqchd3788vajfac8kid3dam6g1rzkkhs1rfldw8cnsi1n")))

