(define-module (crates-io #{3}# p psw) #:use-module (crates-io))

(define-public crate-psw-0.1.0 (c (n "psw") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)))) (h "0734x98bc4l7874fqn7nsd5b1d4l208fmpz3mc9jsm60dxckih8s")))

(define-public crate-psw-0.1.1 (c (n "psw") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)))) (h "0pjcjaw2vfgl34zqj6cj2xh62925i866bhylzwkdf89vy241x5cb")))

(define-public crate-psw-0.1.2 (c (n "psw") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)))) (h "1nbvnssqh94s313jrrqc8mc8az495ik52jg21xp6p6xqs66kvgr3")))

(define-public crate-psw-0.2.0 (c (n "psw") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.2") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.0") (d #t) (k 0)))) (h "1dz0kz51ld6a67aiwwrw0pairmj6057d5v62v49knvg6bbcfbfap")))

