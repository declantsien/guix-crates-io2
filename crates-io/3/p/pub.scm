(define-module (crates-io #{3}# p pub) #:use-module (crates-io))

(define-public crate-pub-0.1.0 (c (n "pub") (v "0.1.0") (h "158zcyycv3fg1c60fnz2rz8swiq8bsa6s6yg2kgx9cr9aqp4wvqy")))

(define-public crate-pub-0.0.0 (c (n "pub") (v "0.0.0") (h "0yjvzabd7cnrb8mwgz4w2j0w9p8japifjq2kih9s33sw56zxxdz2")))

(define-public crate-pub-0.0.1 (c (n "pub") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vh3s5h9pz4ir8qwfgacjdrpwqbs7vna4qkw50qgwk3kcx41ax96")))

(define-public crate-pub-0.0.1-aarch64 (c (n "pub") (v "0.0.1-aarch64") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0b79m07cxjv8b0kk9djg5g8lp70w47n1s7p7f278an85xkqifk4b")))

(define-public crate-pub-0.1.1 (c (n "pub") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "1dp4f69dc40jq1h5l0c68z0dq2j05fdjk38p37b4dvs2ms5xi523") (y #t)))

(define-public crate-pub-0.1.2 (c (n "pub") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "169yr6q2xzgd17yj41r0cdflxcbm7v7niyjy5i6axywppj11avya") (y #t)))

(define-public crate-pub-0.1.3 (c (n "pub") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1i3n8rnwbximcidbi7by2zr7wrmbbclwlwlihanxddjdwc1wb1q9") (y #t)))

(define-public crate-pub-0.1.4 (c (n "pub") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1gyv19vz5rskzzx01qv8mcjdzcqcqdp8hq1f5b1zrw3k61l7a9i3")))

(define-public crate-pub-0.1.5 (c (n "pub") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "04ax6bmc4f3r8ki2ywl5mf53d65an7mnm2wf2jqbqhpg1yp9z9fr")))

(define-public crate-pub-0.1.6 (c (n "pub") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1hcnsihqi8djlxi61fvz0xlpp7kh5bhl6n9hg8p2q8aaw40jizzw")))

(define-public crate-pub-0.1.7 (c (n "pub") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "0vcb71a5cfg8bbfwh8mdpmx86hxbjz9pm6dhchg0gwgnx1kgli2y")))

(define-public crate-pub-0.1.8 (c (n "pub") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1l7lnw2hmw8ix4a7w7176rb7hmchq6m6r13cj4pkqncxgpfaz4xv")))

(define-public crate-pub-0.1.9 (c (n "pub") (v "0.1.9") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1hwm42fqyaxyckwkzm7qja5ccip6rs6j29s2x038ljsnnxird0x0")))

(define-public crate-pub-0.1.10 (c (n "pub") (v "0.1.10") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1ln2ip5irch3z0nmhi61m0bjr850h9fxhgc6xzzclqivjyyfn44w")))

(define-public crate-pub-0.1.11 (c (n "pub") (v "0.1.11") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1z7y1sfv0105v0i6iami4d435wzr96p9wc93bsh661z2wdra3sbj")))

(define-public crate-pub-0.1.12 (c (n "pub") (v "0.1.12") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "0nfiznzv6haagpx26m3iq427dnl7nkbnm28y6hxablvdsi2ybcjd")))

(define-public crate-pub-0.1.13 (c (n "pub") (v "0.1.13") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1xypn3nvxlxzgv7j8x93ggg22dbkaki8k126qy7hbm06qk9qb6fk")))

(define-public crate-pub-0.1.14 (c (n "pub") (v "0.1.14") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "0prikql41i5vplb1a7y57ymp8smvh9igww0ijlh73g400kmkc2xz")))

(define-public crate-pub-0.1.15 (c (n "pub") (v "0.1.15") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "068xpqyfgsdps0hrllc7s6b23k29ghac81v7zcl7vai1b98q78l8")))

(define-public crate-pub-0.1.16 (c (n "pub") (v "0.1.16") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "159li29h3iki1n2hhw9a7j5489933jwwqmk2vyx4vsf2a4qn5yq9")))

(define-public crate-pub-0.1.17 (c (n "pub") (v "0.1.17") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1lvwdmf1p2908v505976arng8whzi03bjfr2zl34qlgismiwc6v3")))

(define-public crate-pub-0.1.18 (c (n "pub") (v "0.1.18") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "0sysr5qj2b88mn6w77lqf6hx0z599a4vii6xpyx7x2wv6sl7x5a4")))

(define-public crate-pub-0.1.19 (c (n "pub") (v "0.1.19") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1s1nawzi4iahc9kvnz2icfahfh5y9zkcddblb3hgdywk9wnyqisk")))

(define-public crate-pub-0.2.0 (c (n "pub") (v "0.2.0") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1pv47bkilgga2x89wa7i6vwd0pmrfwvcq9bzz7xji6a4p29s47mr")))

(define-public crate-pub-0.2.1 (c (n "pub") (v "0.2.1") (d (list (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1zw14swgvw3fxrig6033pam6fyywyyf7m4wkhvv9yllhxms1d9r7")))

(define-public crate-pub-0.2.2 (c (n "pub") (v "0.2.2") (d (list (d (n "self_update") (r "^0.39") (f (quote ("compression-flate2" "compression-zip-deflate"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "1k34y4wpk728zb7v5q0fx7n4v8292mamrqp01hyc07293m834xm8")))

(define-public crate-pub-0.2.9 (c (n "pub") (v "0.2.9") (d (list (d (n "self_update") (r "^0.39") (f (quote ("compression-flate2" "compression-zip-deflate"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "0y21h2xari4n4fl6nri1i34djn1szlj58ymm9sc8d7gyb7yh33kn")))

(define-public crate-pub-0.3.3 (c (n "pub") (v "0.3.3") (d (list (d (n "self_update") (r "^0.39") (f (quote ("compression-flate2" "compression-zip-deflate"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "0g1w6n0mzzwqf5vbk8jc9ikc4bw53dd0mrhliiqqiq1w5vaznq2w")))

(define-public crate-pub-0.3.4 (c (n "pub") (v "0.3.4") (d (list (d (n "self_update") (r "^0.39") (f (quote ("compression-flate2" "compression-zip-deflate"))) (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "ureq") (r "^2.9") (d #t) (k 1)) (d (n "xz2") (r "^0.1") (d #t) (k 1)))) (h "156hlj4xyi4z5v26jbjf60diiknxsaya1chz7rzp1q8afiz96mq0")))

