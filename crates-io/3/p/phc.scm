(define-module (crates-io #{3}# p phc) #:use-module (crates-io))

(define-public crate-phc-0.1.0 (c (n "phc") (v "0.1.0") (h "0by1jmd89vd40fxv8wz2zn34waxw5naljhqv4iwhhzbrwbjy63ps")))

(define-public crate-phc-0.2.0 (c (n "phc") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.3") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)))) (h "006yhscgha1nqwfj9ryxlwq8blr3zdknaxwnxd9lc008003jl92l")))

