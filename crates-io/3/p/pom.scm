(define-module (crates-io #{3}# p pom) #:use-module (crates-io))

(define-public crate-pom-0.1.0 (c (n "pom") (v "0.1.0") (h "0nwwhlgdslz0a60lqchrx13klyiwmvv07aq2s4lqkp7ms5ra6bc5")))

(define-public crate-pom-0.2.0 (c (n "pom") (v "0.2.0") (h "1bpri3dngfrw6aq7d5ld7isj4d2r9aipqwal2n74cfmla0nardgn")))

(define-public crate-pom-0.3.0 (c (n "pom") (v "0.3.0") (h "0is8gj3vh2aj65cv4j7qp2m5swzb1mah9jyd1517pc4663ym8hwv")))

(define-public crate-pom-0.4.0 (c (n "pom") (v "0.4.0") (h "1ggasvjvpf1780vnwqzsfk882m23n5syffwrcg7ji6w7f345srg4")))

(define-public crate-pom-0.5.0 (c (n "pom") (v "0.5.0") (h "1hkhgk5sfxb44cm30fs6sazx1ga52hh0wy60asz6rgf328xfgm2r")))

(define-public crate-pom-0.6.0 (c (n "pom") (v "0.6.0") (h "10g9my0b3mny05ph3inxlj954y03xkhxyj2n4dvy64a2ypbw1f3h")))

(define-public crate-pom-0.6.1 (c (n "pom") (v "0.6.1") (h "131hjicvnr4g72dsws8jqp7slqg4x9gz0ri27liqz8vnnv5xr078")))

(define-public crate-pom-0.7.0 (c (n "pom") (v "0.7.0") (h "1w7r4g8wpa35sk54zxzja4j4c0g1gkfpxsqzs8znqk6663sxlfcc")))

(define-public crate-pom-0.8.0 (c (n "pom") (v "0.8.0") (h "1pmngaqg2qrc1bxxajygl6iny3h9i7ffnkn614pql3xrcqh2anyg")))

(define-public crate-pom-0.8.1 (c (n "pom") (v "0.8.1") (h "0x1zjmxbq8fw816a2k677ppqxkx6fvfjgg5jp14bzzbijhxxhyr8")))

(define-public crate-pom-0.9.0 (c (n "pom") (v "0.9.0") (h "1r0dx6hn7h79p9nw45cljd5bida1ygk3f3h71cslr81mrm65k2x7")))

(define-public crate-pom-2.0.0-alpha (c (n "pom") (v "2.0.0-alpha") (h "05kycd1f6l2bmfv1222kmmrfl636089svqk14dlz4b4iqk2d34r0")))

(define-public crate-pom-1.0.0 (c (n "pom") (v "1.0.0") (h "17l3y7r4kvwpi27xjqhc1z36lfmbwxgp10mpjh1jl3k5fzwy1xyc")))

(define-public crate-pom-2.0.0-beta (c (n "pom") (v "2.0.0-beta") (h "1p59jhq72qwqqk31gf8sy931qm5lxx9s77b6z3y09mi6jscxc0bj")))

(define-public crate-pom-1.0.1 (c (n "pom") (v "1.0.1") (h "0safvymjy4i5570axv13378nahflklf1rcpyzgqgwalg7hdvly46")))

(define-public crate-pom-1.1.0 (c (n "pom") (v "1.1.0") (h "1ri8bhdivrz7fp31dyj6ckba74sh0sqsrpbziq4rmk6dgrcwxxk0")))

(define-public crate-pom-2.0.0 (c (n "pom") (v "2.0.0") (h "1nix5v2gi8gjs01nnsdyk37fgh3zda4jh1dmp2hhacwcaq0dcp9b")))

(define-public crate-pom-2.0.1 (c (n "pom") (v "2.0.1") (h "084128sz8376rn8h8097w9qc841xmyrs2qjf47jmdw7ga2kyyjqx")))

(define-public crate-pom-3.0.0 (c (n "pom") (v "3.0.0") (h "0jns24x5l8cn47398aqhfpw5p5hmy6rhfv4500iwdjn9szn35wm7")))

(define-public crate-pom-3.0.1 (c (n "pom") (v "3.0.1") (h "0s8mmna00fqy4j0r1h1g702k1gwsxiwqs4j0jl6r2q0bzwf619by")))

(define-public crate-pom-3.0.2 (c (n "pom") (v "3.0.2") (h "01icgl34af781n0amb2grhgisd9ly269sx52jwa4wv7pg0pmwakb")))

(define-public crate-pom-3.0.3 (c (n "pom") (v "3.0.3") (h "05lhlrj1w7ikc21blchfqhaq0p6cch1fs71h5gk39hf0s9sxavcm")))

(define-public crate-pom-3.1.0 (c (n "pom") (v "3.1.0") (h "0jwryasyrihd0dn58745f6q2zj1b06ghvvk3dg197nhj5kszfp7g")))

(define-public crate-pom-3.2.0 (c (n "pom") (v "3.2.0") (h "1v14c2p1irblagnljkw4n0f1w5r8mbybzycz0j1f5y79h0kikqh7")))

(define-public crate-pom-3.3.0 (c (n "pom") (v "3.3.0") (d (list (d (n "bstr") (r "^1.1.0") (d #t) (k 0)))) (h "1r6c6m12fqs5nwqara8a92nahnjf2099an1lfpkmim0hzsjp6baw") (f (quote (("utf8") ("default" "utf8"))))))

(define-public crate-pom-3.4.0 (c (n "pom") (v "3.4.0") (d (list (d (n "bstr") (r "^1.1.0") (d #t) (k 0)))) (h "0jvlsxzzwwmfng3sk0drig8x36klbabfh10b5m9sshz9hs7jv5vc") (f (quote (("utf8") ("trace") ("default" "utf8"))))))

