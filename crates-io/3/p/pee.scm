(define-module (crates-io #{3}# p pee) #:use-module (crates-io))

(define-public crate-pee-0.1.1 (c (n "pee") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "063hs2i14ss241hm9hvkxdj9wgscqd3rzcsh14hlz0j5lxp0z2f4")))

(define-public crate-pee-0.1.2 (c (n "pee") (v "0.1.2") (d (list (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0swggrfr11rm6ldlw9f2izl3pacqif6ap8shqqiv32fffwrzf0rj")))

