(define-module (crates-io #{3}# p pbl) #:use-module (crates-io))

(define-public crate-pbl-0.1.0 (c (n "pbl") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ramhorns") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "1sqdsq9c7zysx10hfcvmhxckxsbxsvyqlhb7ry9iwh8pw10jxq9w")))

(define-public crate-pbl-0.1.1 (c (n "pbl") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ramhorns") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "0rdricy42wxy7vhrx5w7bl7hif46gcslxqp67l0dd4id74dy0l0x")))

(define-public crate-pbl-0.1.2 (c (n "pbl") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ramhorns") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "0q4bf7w1pvzr9wh0kp99f8gc4cmnyjm3aj5nybf8p82nq50bbimg")))

(define-public crate-pbl-0.1.3 (c (n "pbl") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "ramhorns") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.124") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "02pm4jdifqdw7kbyffkyq5zid69mhdmjg7kpm1ik09vjkf378fhz")))

(define-public crate-pbl-0.2.1 (c (n "pbl") (v "0.2.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "mustache") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "13rg5pk8wq5kblinc002x8cwhvbdf0d18f071f5f3vg935lx2nxf")))

