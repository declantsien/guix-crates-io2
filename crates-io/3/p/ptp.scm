(define-module (crates-io #{3}# p ptp) #:use-module (crates-io))

(define-public crate-ptp-0.1.0 (c (n "ptp") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libusb") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "0275gbj2s2h78f015r0hp1zk8936rwbgnd8p2wrq1klc9lkwcqjm")))

(define-public crate-ptp-0.1.1 (c (n "ptp") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libusb") (r "^0.2.2") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)))) (h "1m4vjiq2ip67a76a11rx4d3mi47msjg033cf52byj1j9vz1yspjk")))

(define-public crate-ptp-0.2.0 (c (n "ptp") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libusb") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1y8jrzjhlisyc1fqh4i808491znkxvpvhzg7y210j9g2cmbbkmaf")))

(define-public crate-ptp-0.2.2 (c (n "ptp") (v "0.2.2") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libusb") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1.30") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "152sbnvippxq0x1rbd3w0ncypp275ijac8fqn1ja2jygl6d0dy5w")))

(define-public crate-ptp-0.3.0 (c (n "ptp") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libusb") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0dc3hgqhw8yww09dd3dsiskk6mwlizcjwajvxk6sfliws3x3cgaa")))

(define-public crate-ptp-0.3.1 (c (n "ptp") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libusb") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1k0gdvmm5sk1ssfdfcznkcjbs1q4dqbiir0dafvybwm9h501mnwp")))

(define-public crate-ptp-0.4.0 (c (n "ptp") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libusb") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "12lhia2hl5az9nkc9n9jfb63sn08y90l2smpk77s757id9938aqx")))

(define-public crate-ptp-0.4.1 (c (n "ptp") (v "0.4.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libusb") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1610xhx5x6h6z05zzkdiknbpnlihpdbgbsk0m22b7ic9dkhlxim0")))

(define-public crate-ptp-0.5.0 (c (n "ptp") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "16qv25qsvm6wqp0zl319q3fzxgbm3h9grfv8npcc04griig85mbn")))

