(define-module (crates-io #{3}# p pbf) #:use-module (crates-io))

(define-public crate-pbf-0.1.0 (c (n "pbf") (v "0.1.0") (h "0z7szkzadwxlly6fjn295if9xq0q2pg25s6m8y1a7gs9y7pyapbx") (f (quote (("std") ("default" "std"))))))

(define-public crate-pbf-0.1.1 (c (n "pbf") (v "0.1.1") (h "05qj28lz7n2m3cv1pz3qppmff5fhwdzxjjnk23c9mgg2b67cpwji") (f (quote (("std") ("default" "std"))))))

(define-public crate-pbf-0.1.2 (c (n "pbf") (v "0.1.2") (h "1giav9qny82rzwynxx32gg60agh89n4s4ilajqagrw2i424w9lq4") (f (quote (("std") ("default" "std"))))))

(define-public crate-pbf-0.1.3 (c (n "pbf") (v "0.1.3") (h "1hgiwg4b0hr647rvs6j6s1q7jgfzm1wm9n3dw5nwf8xxzdjhbgr8") (f (quote (("std") ("default" "std"))))))

(define-public crate-pbf-0.1.4 (c (n "pbf") (v "0.1.4") (h "0zmgw5ds7f6kam39gsgmi7x40y3jhmxcnicxvw7mr81cz0jfrhcr") (f (quote (("std") ("default" "std"))))))

(define-public crate-pbf-0.1.5 (c (n "pbf") (v "0.1.5") (h "15mmfjqj0cvqfrsdf9bqmqw6681acnhry2vwdhz27p805l3akjfz") (f (quote (("std") ("default" "std"))))))

