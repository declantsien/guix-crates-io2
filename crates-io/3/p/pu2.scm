(define-module (crates-io #{3}# p pu2) #:use-module (crates-io))

(define-public crate-pu2-0.0.0 (c (n "pu2") (v "0.0.0") (h "0m9d55kqgszbckkl3skjzzyqc7clzmscbfbia2ayf5sv99l1a22i") (y #t)))

(define-public crate-pu2-0.0.1 (c (n "pu2") (v "0.0.1") (h "0pprkq648bfa0zm5y7hrlmvx9bj8sqrrpnj66wxg7hnyas4llsn4") (y #t)))

(define-public crate-pu2-0.5.0 (c (n "pu2") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "105g9kyvza38idn9fmfxj4bpcp175if56w1kkb39d5jmk4afycif") (y #t)))

(define-public crate-pu2-0.5.1 (c (n "pu2") (v "0.5.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "1m22ql1wp72b25pp184v9akgkhh30rz1d9wx9zi7ng8y2sd4ksjc") (y #t)))

