(define-module (crates-io #{3}# p pod) #:use-module (crates-io))

(define-public crate-pod-0.0.1 (c (n "pod") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.0") (d #t) (k 0)) (d (n "resize-slice") (r "^0.0") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "1h69pfr322lc1clq87c5nxiw7nrizkzb5fqidszxfkfrpjr9qv8w") (f (quote (("unstable" "resize-slice/unstable"))))))

(define-public crate-pod-0.0.2 (c (n "pod") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.1") (d #t) (k 0)) (d (n "resize-slice") (r "^0.0") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "18cm6jhfs8b5qknz6p1jd5i87768iapxnjw6y24r6hpjfl1ng377") (f (quote (("unstable" "resize-slice/unstable"))))))

(define-public crate-pod-0.0.3 (c (n "pod") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.1") (d #t) (k 0)) (d (n "resize-slice") (r "^0.0") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "1vjkcy16fcdbvspf8fdd8al318kby0x209c97zb4pmp5xikjmaax") (f (quote (("unstable" "resize-slice/unstable"))))))

(define-public crate-pod-0.1.0 (c (n "pod") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.1") (d #t) (k 0)) (d (n "resize-slice") (r "^0.0") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "1czh9dfakpwqq7n8x2cysmf1zbcvdyi22vg0cqbwhspniz6rs7bw") (f (quote (("unstable" "resize-slice/unstable"))))))

(define-public crate-pod-0.1.1 (c (n "pod") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.1") (d #t) (k 0)) (d (n "resize-slice") (r "^0.0") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "0h8kf72w0zmrndk2wy575jxir9myf8mlqnx1x938wsz39914w889") (f (quote (("unstable" "resize-slice/unstable"))))))

(define-public crate-pod-0.2.0 (c (n "pod") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.2") (d #t) (k 0)) (d (n "packed") (r "^0.2") (d #t) (k 0)) (d (n "resize-slice") (r "^0.0") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "10ngiw86v4zl5n6mm0gdbad9qzz3qjhvrf9l07iapnlk32vazlkl") (f (quote (("unstable" "resize-slice/unstable" "packed/unstable"))))))

(define-public crate-pod-0.3.0 (c (n "pod") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.3") (d #t) (k 0)) (d (n "packed") (r "^0.3") (d #t) (k 0)) (d (n "resize-slice") (r "^0.0") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "167mxmpbr2bqc4kx9znpd8rj87n5llw3vb4ris39bjbyqw1pywcw") (f (quote (("unstable" "resize-slice/unstable" "packed/unstable"))))))

(define-public crate-pod-0.3.1 (c (n "pod") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "nue-io") (r "^0.3") (d #t) (k 0)) (d (n "packed") (r "^0.3") (d #t) (k 0)) (d (n "resize-slice") (r "^0.1") (d #t) (k 0)) (d (n "uninitialized") (r "^0.0") (d #t) (k 0)))) (h "1h8wli0bvbfhqvx8wwb6n3ccypswpbz3ib03lsv87y9bnbarrqjv") (f (quote (("unstable" "resize-slice/unstable" "packed/unstable"))))))

(define-public crate-pod-0.4.0 (c (n "pod") (v "0.4.0") (d (list (d (n "packed") (r "^0.4.0") (d #t) (k 0)) (d (n "read_exact") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "uninitialized") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "0bhr2xk32qinbwqdzbzh9nvngjkhkxb8574v4q6iaz28709nh24y") (f (quote (("default" "read_exact" "uninitialized"))))))

(define-public crate-pod-0.4.1 (c (n "pod") (v "0.4.1") (d (list (d (n "packed") (r "^0.4.0") (d #t) (k 0)) (d (n "read_exact") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "uninitialized") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "0ql82xrw2n80b0h2nxf9445ig5am1nk7l8ml6ahjk51x33m9z88n") (f (quote (("default" "read_exact" "uninitialized"))))))

(define-public crate-pod-0.5.0 (c (n "pod") (v "0.5.0") (d (list (d (n "packed") (r "^0.4.0") (d #t) (k 0)) (d (n "read_exact") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "uninitialized") (r "^0.0.2") (o #t) (d #t) (k 0)))) (h "1dqqcfms8zf3hzhyy1q0frbf636vg0508q5f49fdn6ivmz4w3c88") (f (quote (("default" "read_exact" "uninitialized"))))))

