(define-module (crates-io #{3}# p pbr) #:use-module (crates-io))

(define-public crate-pbr-0.0.1 (c (n "pbr") (v "0.0.1") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "01sqcf7x14ip95nh9i1a1yvh3pk9ra0bq4za29ny5pfcn9xi6gsq")))

(define-public crate-pbr-0.0.2 (c (n "pbr") (v "0.0.2") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1y8wvaanjxavsf7qcgz0ymhwnrqparygpylza153gxc2dcgqndx3")))

(define-public crate-pbr-0.0.3 (c (n "pbr") (v "0.0.3") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "00klznzx73b28mbp6g2gcl7bcbhf16qg1kiapjkmpk7vgs5divqn")))

(define-public crate-pbr-0.0.4 (c (n "pbr") (v "0.0.4") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1aramzk5zwipnsbrrwq3ycxvjfrqhy75sas7wdjgmwx9y89y9ncj")))

(define-public crate-pbr-0.0.5 (c (n "pbr") (v "0.0.5") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1qkrymyp2ckyspq0nrvdy11c2j95956kah2mgcg2jj7s10zh828l")))

(define-public crate-pbr-0.1.0 (c (n "pbr") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0j28nq4p5jmzycm8b3s357ywhf7zqlpca9j97zjcvwqfivdmmqd5")))

(define-public crate-pbr-0.1.1 (c (n "pbr") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0jbcx9rp44x1izh53ky1v8fww7mdbqsnrb8qrgl0jpmy9y744gvg")))

(define-public crate-pbr-0.1.2 (c (n "pbr") (v "0.1.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "12m4vgryr31xbplr03v8gbavfy9w2cpkklwpadddn1chycj5lraa")))

(define-public crate-pbr-0.2.0 (c (n "pbr") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "15lwqxm89n28gwbzi6z9792xy7cphp1z0w4bp5sx2878s8mx07nf")))

(define-public crate-pbr-0.2.1 (c (n "pbr") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1gk1ijn23847xicrgjds80apd0sdm0d1kcldks4ahlxxwiq7fcjg")))

(define-public crate-pbr-0.3.0-rc1 (c (n "pbr") (v "0.3.0-rc1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1h4d7r9whk3dvql7yh4x4p8q824jd4qzqbvy718azl4rmj41iql8")))

(define-public crate-pbr-0.3.0-rc2 (c (n "pbr") (v "0.3.0-rc2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1iw32w0vfnx49sin8gf7nnb9ghdzlz2kn7rfxpl165xq1bvw7a88")))

(define-public crate-pbr-0.3.0 (c (n "pbr") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0xw2jdvi84xvzqk0bgqhj6i8kqim61aqfh4wgjlw49vx7p45l50q")))

(define-public crate-pbr-0.3.1 (c (n "pbr") (v "0.3.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "07x4gnsryigcbr4m89g5vmsgx4krbmrp4r8a8dkxyldhg9pm2dwz")))

(define-public crate-pbr-1.0.0-alpha.1 (c (n "pbr") (v "1.0.0-alpha.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1mhv9v2gynr156nw4vzcpdwl53y0541cckjahdf8asvl627cngvd")))

(define-public crate-pbr-1.0.0-alpha.2 (c (n "pbr") (v "1.0.0-alpha.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "16w0ddn3fyxwahxzdb2pbmnyad84w6hyryyag3ap4hxcnypn6a2v")))

(define-public crate-pbr-1.0.0-alpha.3 (c (n "pbr") (v "1.0.0-alpha.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "03nf0vfrylzz013vphicdzb3wm2lr4il77qx381z1wq57s13811c")))

(define-public crate-pbr-1.0.0 (c (n "pbr") (v "1.0.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0fnj6rvw6ir463kxi3np2m3qnsalv8zyf3sx3jxm9i5nxfpy6j70")))

(define-public crate-pbr-1.0.1 (c (n "pbr") (v "1.0.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "termion") (r "^1.4") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "01v9mmc29wzffa776fgxah5vnlbld5gi8kcrpn91kn38mf837dyy")))

(define-public crate-pbr-1.0.2 (c (n "pbr") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "termion") (r "^1.4") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "processenv" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "10ydp58gp79gd8xs6w780758rih255w3f5z5g7i3xh3himqyn0s4")))

(define-public crate-pbr-1.0.3 (c (n "pbr") (v "1.0.3") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "processenv" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0m6jxy6f1rkhljk6zk0z9bdvr12nh8296ndqs1zx1klb3lykwcvl")))

(define-public crate-pbr-1.0.4 (c (n "pbr") (v "1.0.4") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "processenv" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "18nfd2qpf88qhz7w3s0dlg4pd74v4axcn7zb0djaw03wgzc52mzz")))

(define-public crate-pbr-1.1.0 (c (n "pbr") (v "1.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "processenv" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0pf3gxhq6gd9qakyjrn1729q4n5nsp9c1d5kl514dvpsgss76zvi")))

(define-public crate-pbr-1.1.1 (c (n "pbr") (v "1.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("wincon" "processenv" "winbase"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "056mqvw168ziig1dgl2kq4vmkamv6gk3hv1x9696r6ynl3gjfn7d")))

