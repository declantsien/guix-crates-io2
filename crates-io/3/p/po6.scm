(define-module (crates-io #{3}# p po6) #:use-module (crates-io))

(define-public crate-po6-0.1.0 (c (n "po6") (v "0.1.0") (h "12mrykg1v3ccq9f6whizzw9kb5zwj42722vwh0l9wb0ywizq9qan")))

(define-public crate-po6-0.1.1 (c (n "po6") (v "0.1.1") (h "106jvvrp77kdbnig2dblprbgvvvxq5155rj8k1r5s7l1ik6g7cm3")))

(define-public crate-po6-0.1.2 (c (n "po6") (v "0.1.2") (h "14g6arvmhqnw3sk8aqb3rk0vamkbqlgrjsj2h01b6r7pbbk3s204")))

(define-public crate-po6-0.1.3 (c (n "po6") (v "0.1.3") (h "0xn5fzmpm9png2w0rxk38jy60nk1wsxyqz7z0wkrpa3gxxp7i3aj")))

(define-public crate-po6-0.1.4 (c (n "po6") (v "0.1.4") (h "0vsm19ddsx7nbrdybhqbpxdhg508x4ydcf1hmzgvrj5lhs1kdh5n")))

(define-public crate-po6-0.1.5 (c (n "po6") (v "0.1.5") (h "1svqhpmaz7h9hzfskgg238hb9mkfi8d4aba1587rwzr10yn0cyz0")))

(define-public crate-po6-0.1.6 (c (n "po6") (v "0.1.6") (h "03apc6hb9r8bcf1v26wynmyk6043zvk2nvkqw1dqcgfkryqfrncr")))

(define-public crate-po6-0.1.7 (c (n "po6") (v "0.1.7") (h "0j0ysmpcypgz2cc3li1infykr7mrwb39cpz7mw70ky569539qkcm")))

