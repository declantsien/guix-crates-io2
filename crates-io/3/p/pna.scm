(define-module (crates-io #{3}# p pna) #:use-module (crates-io))

(define-public crate-pna-0.0.0-alpha.0 (c (n "pna") (v "0.0.0-alpha.0") (d (list (d (n "libpna") (r "^0.0.0-alpha.0") (d #t) (k 0)) (d (n "portable-network-archive") (r "^0.0.0-alpha.0") (o #t) (d #t) (k 0)))) (h "13ahf6sm6c15anxb8adnz6y2dykqlcypixpnvxa9vi07wa6bzjd9") (f (quote (("cli" "portable-network-archive")))) (y #t)))

(define-public crate-pna-0.0.0-alpha.1 (c (n "pna") (v "0.0.0-alpha.1") (d (list (d (n "libpna") (r "^0.0.0-alpha.1") (d #t) (k 0)) (d (n "portable-network-archive") (r "^0.0.0-alpha.1") (o #t) (d #t) (k 0)))) (h "1x08z59hlppp3gi7q8zkrjbfic3y8plrc51p1w379an7qc2r6rba") (f (quote (("cli" "portable-network-archive")))) (y #t)))

(define-public crate-pna-0.0.0 (c (n "pna") (v "0.0.0") (d (list (d (n "libpna") (r "=0.0.0") (d #t) (k 0)) (d (n "portable-network-archive") (r "=0.0.0") (o #t) (d #t) (k 0)))) (h "12vci1v20dx6q7maaaxfzmsxhwl85w0a7vag6mb9pvqfscwwsyai") (f (quote (("cli" "portable-network-archive"))))))

(define-public crate-pna-0.0.1 (c (n "pna") (v "0.0.1") (d (list (d (n "libpna") (r "=0.0.1") (d #t) (k 0)) (d (n "portable-network-archive") (r "=0.0.1") (o #t) (d #t) (k 0)))) (h "1hcaj8xbbc3x25mw8jydplrgsw9qmkrcafk61pfsk803qhyhfq84") (f (quote (("cli" "portable-network-archive"))))))

(define-public crate-pna-0.1.0 (c (n "pna") (v "0.1.0") (d (list (d (n "libpna") (r "=0.1.0") (d #t) (k 0)) (d (n "portable-network-archive") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "0jam6d2zh84csalfqxa0rv5nwi3gs0kb6v1i4rzva87haziq6gi0") (f (quote (("cli" "portable-network-archive"))))))

(define-public crate-pna-0.2.0 (c (n "pna") (v "0.2.0") (d (list (d (n "libpna") (r "=0.2.0") (d #t) (k 0)) (d (n "portable-network-archive") (r "=0.2.0") (o #t) (d #t) (k 0)))) (h "1q95v82w671s4c9zzih61c4ss3vaa49lr5gf0n28q99abl8pqj9p") (f (quote (("cli" "portable-network-archive"))))))

(define-public crate-pna-0.3.0 (c (n "pna") (v "0.3.0") (d (list (d (n "libpna") (r "=0.3.0") (d #t) (k 0)) (d (n "portable-network-archive") (r "=0.3.0") (o #t) (d #t) (k 0)))) (h "1k7z1vwv3q30yhw5hi6vnv4isjd2ypcgcy125w7phrhjxci5x1v7") (f (quote (("cli" "portable-network-archive"))))))

(define-public crate-pna-0.3.1 (c (n "pna") (v "0.3.1") (d (list (d (n "libpna") (r "^0.3.0") (d #t) (k 0)) (d (n "portable-network-archive") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "07488wq3grwbg5bjm7b7b0pywcciw64vlnf5f7vr8fr6v1wd1z6p") (f (quote (("cli" "portable-network-archive"))))))

(define-public crate-pna-0.4.0 (c (n "pna") (v "0.4.0") (d (list (d (n "libpna") (r "^0.4.0") (d #t) (k 0)) (d (n "portable-network-archive") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0ya24gz5dpc9q42291vgbypirzwb6dqa9khmyasnv12vjnh8wzvp") (f (quote (("cli" "portable-network-archive"))))))

(define-public crate-pna-0.5.0 (c (n "pna") (v "0.5.0") (d (list (d (n "libpna") (r "^0.5.0") (d #t) (k 0)) (d (n "portable-network-archive") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1l2djk5zia9slnqc1845wgdddq73nj5jr12x55v1q1nqycq4iq1s") (f (quote (("cli" "portable-network-archive"))))))

(define-public crate-pna-0.6.0 (c (n "pna") (v "0.6.0") (d (list (d (n "libpna") (r "^0.6.0") (d #t) (k 0)))) (h "03ar2bbg6sra5v7gwj6kccm05c60p57zjgvjmli10yqzc1grbg4b")))

(define-public crate-pna-0.7.0 (c (n "pna") (v "0.7.0") (d (list (d (n "libpna") (r "^0.7.0") (d #t) (k 0)))) (h "1x99vwl6xan0zwn8i40dign38qnj2zabzq98rqm4pr3mfydgr6fs")))

(define-public crate-pna-0.8.0 (c (n "pna") (v "0.8.0") (d (list (d (n "libpna") (r "^0.8.0") (d #t) (k 0)))) (h "1zhvlqm5za765zdxna82p47wq6zfki50iqqn09lxj4ys4innik73")))

(define-public crate-pna-0.8.1 (c (n "pna") (v "0.8.1") (d (list (d (n "libpna") (r "^0.8.0") (d #t) (k 0)))) (h "0xn9ad9pxi47fxv0i5w3gabc8lv99lgmljdp68y3pnm62ki72kij")))

(define-public crate-pna-0.9.0 (c (n "pna") (v "0.9.0") (d (list (d (n "libpna") (r "^0.9.0") (d #t) (k 0)))) (h "1v4270yqi9f6mqjbjakxibw8pp1mx08d7brqh0nczmwhmfhkps76")))

(define-public crate-pna-0.10.0 (c (n "pna") (v "0.10.0") (d (list (d (n "libpna") (r "^0.10.0") (d #t) (k 0)))) (h "03v9w1ddbk3a3m963n7y8jminvnw1rvg2jv3dy61psp9bg87gcax")))

(define-public crate-pna-0.11.0 (c (n "pna") (v "0.11.0") (d (list (d (n "libpna") (r "^0.11.0") (d #t) (k 0)))) (h "070n9ylsa2449z5xa3wg1gjm3x129pynj36z6fjssw12gk98c1kn")))

