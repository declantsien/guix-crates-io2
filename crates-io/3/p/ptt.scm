(define-module (crates-io #{3}# p ptt) #:use-module (crates-io))

(define-public crate-ptt-0.1.0 (c (n "ptt") (v "0.1.0") (h "0jg3cqlzs7sir4gyfb5m83gssz9hy8j2cq6y6i21k0lf5yn4m22g")))

(define-public crate-ptt-0.2.0 (c (n "ptt") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "crypto-hash") (r "^0.3.4") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)))) (h "06cvwzmq6mxpg3w60iiqa130qvgm192fgdy359plwsyzkqjqhnw6")))

(define-public crate-ptt-0.3.0 (c (n "ptt") (v "0.3.0") (d (list (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)))) (h "0paapd1fg0dwk759nk70v12gi425gz26s7qc1nnzvahlf764vads")))

(define-public crate-ptt-0.5.0 (c (n "ptt") (v "0.5.0") (d (list (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)))) (h "1bl684qw3m0wnnkk59mjc86l7b94xvsxqi1z6wbn4rzpjj2f7dm9")))

(define-public crate-ptt-0.6.0 (c (n "ptt") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)))) (h "0n0pzq7a65nh3cj9fx0qpdppp1sq0hlbwvzfy0wflbp00g1sgcdf")))

(define-public crate-ptt-0.7.0 (c (n "ptt") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)))) (h "0v2q4fdh8ld5kam3ka96826xwjw8wvrphajqjncvp3m4y96hmqr0")))

