(define-module (crates-io #{3}# p pwd) #:use-module (crates-io))

(define-public crate-pwd-0.1.0 (c (n "pwd") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1iclqkgq1pp792vriq75dm4jx2d9zyhlvlvfn7kmd3wnavcl437v")))

(define-public crate-pwd-1.0.0 (c (n "pwd") (v "1.0.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fq3jx613ikijglr7q5iyzhy9d945jn6xswjhd4kqx6z1awdiqp5")))

(define-public crate-pwd-1.1.0 (c (n "pwd") (v "1.1.0") (d (list (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ac9a2k2irvkma98ihd3sppkajrlsknyli6bvq2bgxpidcs17k58")))

(define-public crate-pwd-1.2.0 (c (n "pwd") (v "1.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xas1yymz8gab3fqriy5lcjlfw4afry1gzq9pqg4ghwmxhz340wx")))

(define-public crate-pwd-1.3.0 (c (n "pwd") (v "1.3.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1986a618ginghg9w485vvbncs1yi9rqiw990r92f2276xj5jvlsx")))

(define-public crate-pwd-1.3.1 (c (n "pwd") (v "1.3.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yxhkkkqi1ckln37vdz6gc16aykw88h02548rafi153mhl207jpr")))

(define-public crate-pwd-1.4.0 (c (n "pwd") (v "1.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18p4j95sqqcxn3fbm6gbi7klxp8n40xmcjqy9vz1ww5rg461rivj")))

