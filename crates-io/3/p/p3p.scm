(define-module (crates-io #{3}# p p3p) #:use-module (crates-io))

(define-public crate-p3p-0.1.0 (c (n "p3p") (v "0.1.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.18.1") (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)))) (h "03dwklqcpr44jghrnixq64xr18kx4bp9njcj0nvls7kwvkbdd4v3")))

