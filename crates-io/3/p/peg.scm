(define-module (crates-io #{3}# p peg) #:use-module (crates-io))

(define-public crate-peg-0.1.0 (c (n "peg") (v "0.1.0") (h "1xyfj8maphxlz98zxbv2vhjc0rhjimhdcng0w9akv7mm0kfaafd4")))

(define-public crate-peg-0.1.1 (c (n "peg") (v "0.1.1") (h "0608az73d9pyjda5r8jc3w30bp37bzhr6mkihz0q7ghywykmvj6r")))

(define-public crate-peg-0.1.2 (c (n "peg") (v "0.1.2") (h "07s28f9ibpf40qsw8zyzbi7k56b2srm3fbmidqryixqk0g10hbj6")))

(define-public crate-peg-0.1.3 (c (n "peg") (v "0.1.3") (h "0hvbc83wjs5zq2gblhfppmvcx82sckcd1wn7196bybi7hjfz5bwl")))

(define-public crate-peg-0.1.4 (c (n "peg") (v "0.1.4") (h "1rwpm1g4lh325a6hkw7bw5ll4z1gsswy6bmxlp5klj9dkz5vw2gz")))

(define-public crate-peg-0.1.5 (c (n "peg") (v "0.1.5") (h "0ckaw982nn5d7jbfjjyyaycmswlfgrlvf20hxayf8nx9waw3js9r")))

(define-public crate-peg-0.1.6 (c (n "peg") (v "0.1.6") (h "0s301s61ykg04i979fk4gjdli6sr0l9inffwj7f78410qnbbpal2")))

(define-public crate-peg-0.1.7 (c (n "peg") (v "0.1.7") (h "13vgwkdsq7axwinm7qd191ia151171jryz925acnhb7l6mvx69ax")))

(define-public crate-peg-0.1.8 (c (n "peg") (v "0.1.8") (h "0aj8iil3gg4cxgn2b79sxn060s855rf8qi1808qx79w3svvlbak1")))

(define-public crate-peg-0.1.9 (c (n "peg") (v "0.1.9") (h "0q44vra9m2ga6j42rqry12ybqr5mv271h2l1g43nb47sklgs2lji")))

(define-public crate-peg-0.2.0 (c (n "peg") (v "0.2.0") (h "0s0f833s60sab192p0w443ib78l8fbpysvql8n4s0ji4az7cq6nn")))

(define-public crate-peg-0.2.1 (c (n "peg") (v "0.2.1") (h "1j4p5fgsbibmwkqmsfzwfixxz8fhvgfnwg6b3msdi1py17qgl7rr")))

(define-public crate-peg-0.2.2 (c (n "peg") (v "0.2.2") (h "14pkknfyx73r6fpl0jdzgmf2x985201s3dj89hp75cplwz3zlfz9")))

(define-public crate-peg-0.2.3 (c (n "peg") (v "0.2.3") (h "0641bl9gd7b09446r9l8qlvd998r9jhrmadszzgyri9l9c397ya1")))

(define-public crate-peg-0.3.0 (c (n "peg") (v "0.3.0") (h "1c7gankkb2jr9qlhr1sj0dhf8c8i8ars7lh105ya29rdr7hn7a24") (f (quote (("trace"))))))

(define-public crate-peg-0.3.1 (c (n "peg") (v "0.3.1") (h "1si15gwmgbk3l4i2snd74xp08hy2gmrc65a52dhngrkbxbxr5d1k") (f (quote (("trace"))))))

(define-public crate-peg-0.3.2 (c (n "peg") (v "0.3.2") (h "0ixiqrmw1xq0bkd7ykfikkbmg3a5dmazhz3s54l4dw244dv8b7pa") (f (quote (("trace"))))))

(define-public crate-peg-0.3.3 (c (n "peg") (v "0.3.3") (h "167845hhx2ab28scb2ah1k9ld6mf71rdisjybgr43s1kqg70bazi") (f (quote (("trace"))))))

(define-public crate-peg-0.3.4 (c (n "peg") (v "0.3.4") (h "0v204vk1cl7palqfznasv71275zd07r92a6gz2lvalbn7dyjx50w") (f (quote (("trace"))))))

(define-public crate-peg-0.3.5 (c (n "peg") (v "0.3.5") (h "0gvyd2i0njw0b194pzyc7sn5aw2w1nryffg3xyzzah9zb1wm4n02") (f (quote (("trace"))))))

(define-public crate-peg-0.3.6 (c (n "peg") (v "0.3.6") (h "1lknr3d6ga268n6nasj5l42na5b4028wvmqz9wndqfzmzb8h9qx9") (f (quote (("trace"))))))

(define-public crate-peg-0.3.7 (c (n "peg") (v "0.3.7") (h "0s52h81byzv4byj4yd2794rdkq1daxczvaj825f2iihrzxj8bwkv") (f (quote (("trace"))))))

(define-public crate-peg-0.3.8 (c (n "peg") (v "0.3.8") (h "1vmym9hcy9l9j88gwr0i0rc214cs1dc72ssjgl099x351ff0x43l") (f (quote (("trace"))))))

(define-public crate-peg-0.3.9 (c (n "peg") (v "0.3.9") (h "1wida81bqaz30vqd3avdabfcmybjzy90wvb0vris2z4jqr9wygc6") (f (quote (("trace"))))))

(define-public crate-peg-0.3.10 (c (n "peg") (v "0.3.10") (h "07vhdpbb1hrkl17mji3pw2sypy2q98nz9mawppb1m43wmscb6q3p") (f (quote (("trace"))))))

(define-public crate-peg-0.3.11 (c (n "peg") (v "0.3.11") (h "0xkp9l8c3fb497fbarc6w1w27fkhixcq9bcmjw6p0iz4lr1yhprm") (f (quote (("trace"))))))

(define-public crate-peg-0.3.12 (c (n "peg") (v "0.3.12") (h "1gi6myx8sjgbhpydr70d8y67zff85klz8r1hq0xy80q38d4wdfj9") (f (quote (("trace"))))))

(define-public crate-peg-0.3.13 (c (n "peg") (v "0.3.13") (h "1xclfhj4iv0n7wv5g4fdx4s1ymmfprwalrn4pbd4ni326w8wnqfd") (f (quote (("trace"))))))

(define-public crate-peg-0.3.14 (c (n "peg") (v "0.3.14") (h "1nnlpr79c35c0l6c08bphb8dszqvbrjjwhpnxdydwv9wb5iv4pib") (f (quote (("trace"))))))

(define-public crate-peg-0.3.15 (c (n "peg") (v "0.3.15") (h "044mjn0wc47cl5l3vif1vr72j7gvj8f43lliij2h7lmzgyxsiy7m") (f (quote (("trace"))))))

(define-public crate-peg-0.3.16 (c (n "peg") (v "0.3.16") (h "00zwjq77wx6lrndia19yfviykjvx19pk4p94cjg5yrg3sylf9gpp") (f (quote (("trace"))))))

(define-public crate-peg-0.3.17 (c (n "peg") (v "0.3.17") (h "1ax0s5kbc4wa75f16hmplyam0n841qzqxljy49r1znm66110ycf4") (f (quote (("trace"))))))

(define-public crate-peg-0.3.18 (c (n "peg") (v "0.3.18") (h "12zc8ybdnfghcizc4r6kdmcqq380f4m53q5bj1azj1dnv16059vy") (f (quote (("trace"))))))

(define-public crate-peg-0.4.0 (c (n "peg") (v "0.4.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "0yh1p1b869y1dpjjc53f7mwcw4733mwskfv5r9f3vgqd4jlb1xxj") (f (quote (("trace"))))))

(define-public crate-peg-0.5.0 (c (n "peg") (v "0.5.0") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "06yp7iav5905ak2cc1arkc2ji2qfq6hi04aiyhpn87w0x7r03xzi") (f (quote (("trace"))))))

(define-public crate-peg-0.5.1 (c (n "peg") (v "0.5.1") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "1mkinx91gyrnx6g2blfrkq788m6qkdl5s2p4s5yhm4058wxv5maz") (f (quote (("trace"))))))

(define-public crate-peg-0.5.2 (c (n "peg") (v "0.5.2") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "1hmkaw8apxpwsxbzxz4wbamn6hf9fakiir232h4z33c8qk5i6pm8") (f (quote (("trace"))))))

(define-public crate-peg-0.5.3 (c (n "peg") (v "0.5.3") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "1pn1jnzkpz6gn62gqd2w83hxbjmfn2kviwalcxvas4chrvd3vh39") (f (quote (("trace"))))))

(define-public crate-peg-0.5.4 (c (n "peg") (v "0.5.4") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "1yy4fzj1281dqxcj4l9f2xzkicwl6lkd9s93ybhayi17lk5p991n") (f (quote (("trace"))))))

(define-public crate-peg-0.5.7 (c (n "peg") (v "0.5.7") (d (list (d (n "quote") (r "^0.3") (d #t) (k 0)))) (h "11az3bs3ngvfip920xfr0zwblfkyg6cjgz1v9hmfsdnqw7fi5ps0") (f (quote (("trace"))))))

(define-public crate-peg-0.6.0 (c (n "peg") (v "0.6.0") (d (list (d (n "peg-macros") (r "= 0.6.0") (d #t) (k 0)) (d (n "peg-runtime") (r "= 0.6.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1m7c4mnsfammv030gjgzdp6vh3pva5kj9gl3hkvpdyphscvlsmn2") (f (quote (("trace" "peg-macros/trace"))))))

(define-public crate-peg-0.6.1 (c (n "peg") (v "0.6.1") (d (list (d (n "peg-macros") (r "= 0.6.1") (d #t) (k 0)) (d (n "peg-runtime") (r "= 0.6.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1nyhds9rp26npry6q4mz4z6jsdip5gw9532gbnx55kdwlz8vb996") (f (quote (("trace" "peg-macros/trace"))))))

(define-public crate-peg-0.6.2 (c (n "peg") (v "0.6.2") (d (list (d (n "peg-macros") (r "= 0.6.2") (d #t) (k 0)) (d (n "peg-runtime") (r "= 0.6.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15rfp12dgsynplphp443zfw47m2d5snvdm6a25gz48dv2if8fxch") (f (quote (("trace" "peg-macros/trace"))))))

(define-public crate-peg-0.6.3 (c (n "peg") (v "0.6.3") (d (list (d (n "peg-macros") (r "=0.6.3") (d #t) (k 0)) (d (n "peg-runtime") (r "=0.6.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0rqkllpmcsda51wkhghyrp0wcg77wg12lzivqdx1fbr75246fxlz") (f (quote (("trace" "peg-macros/trace"))))))

(define-public crate-peg-0.7.0 (c (n "peg") (v "0.7.0") (d (list (d (n "peg-macros") (r "=0.7.0") (d #t) (k 0)) (d (n "peg-runtime") (r "=0.7.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0shn3npql9xp6avjwzg6y83bxg4ksbxmcsammbvj7xalx90vih07") (f (quote (("trace" "peg-macros/trace"))))))

(define-public crate-peg-0.8.0 (c (n "peg") (v "0.8.0") (d (list (d (n "peg-macros") (r "=0.8.0") (d #t) (k 0)) (d (n "peg-runtime") (r "=0.8.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0b6dgycdclxfxdp6ag9px99mjj04s7k3vs9pijrz66l14vl8ywmg") (f (quote (("trace" "peg-macros/trace"))))))

(define-public crate-peg-0.8.1 (c (n "peg") (v "0.8.1") (d (list (d (n "peg-macros") (r "=0.8.1") (d #t) (k 0)) (d (n "peg-runtime") (r "=0.8.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0m45a2ccab38ak8b8cbwri1bfhil6hc92jby12yfxarvvjpjqzx0") (f (quote (("trace" "peg-macros/trace"))))))

(define-public crate-peg-0.8.2 (c (n "peg") (v "0.8.2") (d (list (d (n "peg-macros") (r "=0.8.2") (d #t) (k 0)) (d (n "peg-runtime") (r "=0.8.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "0qbgchzj0ajpgmasbxk65lqx9fwvxda21k6pifzqmhqrsavwl2s0") (f (quote (("unstable" "peg-runtime/unstable") ("trace" "peg-macros/trace") ("std" "peg-runtime/std") ("default" "std")))) (r "1.68.0")))

(define-public crate-peg-0.8.3 (c (n "peg") (v "0.8.3") (d (list (d (n "peg-macros") (r "=0.8.3") (d #t) (k 0)) (d (n "peg-runtime") (r "=0.8.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)) (d (n "version_check") (r "^0.9") (d #t) (k 2)))) (h "1rbi7rkmdpvdqc5v58b4wfzh7y1y9hqzkxpgyz5i82bpml95sqla") (f (quote (("unstable" "peg-runtime/unstable") ("trace" "peg-macros/trace") ("std" "peg-runtime/std") ("default" "std")))) (r "1.68.0")))

