(define-module (crates-io #{3}# p pws) #:use-module (crates-io))

(define-public crate-pws-0.0.0 (c (n "pws") (v "0.0.0") (h "13a60abh9xdyvyjf1chxayz60s7njwbm1jdsmwjn6w056w49q979") (y #t)))

(define-public crate-pws-0.1.0 (c (n "pws") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "1blykw6v45fj9xbd5hfim5c2c7dbfxh6cjm90n8i669mw1832qqq") (f (quote (("pong") ("default" "pong"))))))

(define-public crate-pws-0.1.1 (c (n "pws") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-tungstenite") (r "^0.21") (f (quote ("native-tls"))) (d #t) (k 0)) (d (n "url") (r "^2.4") (d #t) (k 0)))) (h "0ahnrbqkdcg7j3byw3scyis7fmvjg6ad4nqqamg4fpbs4vx77d02") (f (quote (("pong") ("default" "pong"))))))

