(define-module (crates-io #{3}# p pfx) #:use-module (crates-io))

(define-public crate-pfx-0.1.0 (c (n "pfx") (v "0.1.0") (h "08l040skjyjipm09nb1liyl28nz4vlaghb7q6v6dvh881pymbd7y") (r "1.77.0")))

(define-public crate-pfx-0.2.0 (c (n "pfx") (v "0.2.0") (h "0pdmwynxzzw8c7dqwdb15s2fs0hw6w5a5zgxr90y8ijabvzw4mg4") (r "1.77.0")))

(define-public crate-pfx-0.3.0 (c (n "pfx") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1x0hizjwp80jx8qhk3pgvq774fjc6iicl7p2rsirab6pdqgik043") (r "1.77.0")))

(define-public crate-pfx-0.3.1 (c (n "pfx") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1i7fayq36f4irmiyx9wcbrk1lxqb78f61jni18a0i8lb63kgb8g9") (r "1.77.0")))

(define-public crate-pfx-0.4.0 (c (n "pfx") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1fl6f7zh4zdq88wcjh2rvv6q9ws1drmxl5x361nc7i8sq8xi1pc8") (r "1.77.0")))

(define-public crate-pfx-0.4.1 (c (n "pfx") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0bnddqnzhgkg05hy3qmm6swfn43p7q9z1yn54jbd6bq4dqzl32cf") (r "1.77.0")))

