(define-module (crates-io #{3}# p pkg) #:use-module (crates-io))

(define-public crate-pkg-0.1.0 (c (n "pkg") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1crikqb2jskday6y4h8m8ni35srdka81nc7rna2297kg2irw77jn") (f (quote (("nightly" "lazy_static/nightly"))))))

(define-public crate-pkg-0.1.1 (c (n "pkg") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1akv61fhs4bhz1kpnwdy7yiim5qsqnqmksjpy3sp18rwnvsm8n0y") (f (quote (("nightly" "lazy_static/nightly"))))))

(define-public crate-pkg-1.0.0 (c (n "pkg") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "01mkad4g2lsmyvkap6g0mkqm2998a97zxjmla8p9xzlr71drawxq") (f (quote (("nightly" "lazy_static/nightly"))))))

(define-public crate-pkg-1.1.0 (c (n "pkg") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0wh3i1rrfzhyg0sxx4f1il5cjkqalpi3wahx8h8x316761gqldsm") (f (quote (("nightly" "lazy_static/nightly") ("build"))))))

(define-public crate-pkg-2.0.0 (c (n "pkg") (v "2.0.0") (d (list (d (n "lazy_static") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "pkg_impl") (r "^2.0.0-alpha.1") (d #t) (k 0)))) (h "0nmzkv0xsii1ad7j98xzi8ymihxrjih12g9i0zna2hd7j62r480l") (f (quote (("build"))))))

(define-public crate-pkg-2.0.1 (c (n "pkg") (v "2.0.1") (d (list (d (n "lazy_static") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "pkg_impl") (r "^2.0.0") (d #t) (k 0)))) (h "1v6rcv7mkzynhnli4zaln35jvpywlwscncmv3ajg7j1srmwsb45m") (f (quote (("build"))))))

(define-public crate-pkg-3.0.0 (c (n "pkg") (v "3.0.0") (d (list (d (n "pkg-macros") (r "^3.0.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "19fw9897fvdfgi6axk5aaaf7s832rhx4g33m0xa5b04bxd5pi5ip") (f (quote (("git" "build" "pkg-macros/git") ("default") ("build" "pkg-macros/build"))))))

