(define-module (crates-io #{3}# p pst) #:use-module (crates-io))

(define-public crate-pst-0.1.0 (c (n "pst") (v "0.1.0") (d (list (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zklw3hjl043h4pkgyq37clbmlcwqyxwizmhl5zkqjndk1kzrrni")))

(define-public crate-pst-0.2.0 (c (n "pst") (v "0.2.0") (d (list (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0biwcdmwhs9i4g90g4pvmbcqcrkl7kb5zp10pz2ravdhahq02b2q")))

