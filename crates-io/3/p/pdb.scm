(define-module (crates-io #{3}# p pdb) #:use-module (crates-io))

(define-public crate-pdb-0.1.0 (c (n "pdb") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)))) (h "02rvl5lkqcqkrr49xv940k5ivx1gkl9r6pdqyqzzqvmrmc27i75w")))

(define-public crate-pdb-0.1.1 (c (n "pdb") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)))) (h "0l9bhshm204f7fgbm66dfwyd3ym423p7qv91xn3s5qcvqx9crhxv")))

(define-public crate-pdb-0.1.2 (c (n "pdb") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)))) (h "0plqw0jqbfxgvi7j0ysajl6m9svbabqvyp97zzk96gffz404bhq4")))

(define-public crate-pdb-0.1.3 (c (n "pdb") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)))) (h "028y4wlcw06xk5fiy7b3hla7c4vvv8sqv4cz13s7jad3rbf8pa0b")))

(define-public crate-pdb-0.1.4 (c (n "pdb") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)))) (h "1hrbca856qgsxpx7ldqrl06z5b86bn8d3clincnrdsgnphi5xvsk")))

(define-public crate-pdb-0.1.5 (c (n "pdb") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "0qdx240qnz9zlm09qqj0cgpr07773394c53cwjf8463p8q7vvaid")))

(define-public crate-pdb-0.1.6 (c (n "pdb") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "033gilr3rn1wlvjp0hdy1ww78cgjzi49h56pzcpkhqyqvyg0xp2d")))

(define-public crate-pdb-0.2.0 (c (n "pdb") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "189n0k0b86cxzljq3xlzxa05hswz1ngaa8fmd52n2x51sjhf0mhm")))

(define-public crate-pdb-0.2.1 (c (n "pdb") (v "0.2.1") (d (list (d (n "fallible-iterator") (r "^0.1.3") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "scroll") (r "^0.9.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "1674qp4ajkj62s5gbyxdqj3lf83pz0sll9js8h3xymcpga6dcgq4")))

(define-public crate-pdb-0.2.2 (c (n "pdb") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.1.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2.15") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 2)) (d (n "scroll") (r "^0.9.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (d #t) (k 0)))) (h "1nfz62picr3sh97masdxw8yi71dir5ph8lkx286f1rbiwd012pv8")))

(define-public crate-pdb-0.3.0 (c (n "pdb") (v "0.3.0") (d (list (d (n "fallible-iterator") (r "^0.1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 2)) (d (n "scroll") (r "^0.9.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7.2") (d #t) (k 0)))) (h "0dp5mm560hgydcii2pzldvsq9y489rys3mhffqrss5qx9b0sa7k0")))

(define-public crate-pdb-0.4.0 (c (n "pdb") (v "0.4.0") (d (list (d (n "fallible-iterator") (r "^0.1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 2)) (d (n "scroll") (r "^0.9.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7.2") (d #t) (k 0)))) (h "1wp77h3lyh9w9zakx7759dgdwjh1pg9r29xa8af3cq9yj14l23nj")))

(define-public crate-pdb-0.5.0 (c (n "pdb") (v "0.5.0") (d (list (d (n "fallible-iterator") (r "^0.1.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2.18") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 2)) (d (n "scroll") (r "^0.9.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.7.2") (d #t) (k 0)))) (h "0ab9mdbdjzc0n9n3p9snb5jgrl77i8dbanrhn42bv7fwcxq7pdg6")))

(define-public crate-pdb-0.6.0 (c (n "pdb") (v "0.6.0") (d (list (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 2)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "1r2j27gahhw7d0k6j9fab9pncxqx9wcwbz6nsdi2kdc8xbyfasxn")))

(define-public crate-pdb-0.7.0 (c (n "pdb") (v "0.7.0") (d (list (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.10") (d #t) (k 2)) (d (n "scroll") (r "^0.10.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "1app5bdmy323hzjv92ysa55nfyvmjzan5gmgwmyld8daxiid3x0k")))

(define-public crate-pdb-0.8.0 (c (n "pdb") (v "0.8.0") (d (list (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 2)) (d (n "scroll") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (d #t) (k 0)))) (h "0qs8lxx3ly029c77ip2mhlf0s9fmcbzlmaq0khkydar354whl142")))

