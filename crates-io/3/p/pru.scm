(define-module (crates-io #{3}# p pru) #:use-module (crates-io))

(define-public crate-pru-0.1.0 (c (n "pru") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 1)) (d (n "crossterm") (r "^0.19") (d #t) (k 0)) (d (n "execute") (r "^0.2") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tui") (r "^0.15") (f (quote ("crossterm"))) (k 0)))) (h "0i72pif16kwwiw13jafbh5zq9ckvgml7lqicmccs3x24nzwd937q")))

