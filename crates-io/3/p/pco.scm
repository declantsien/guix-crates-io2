(define-module (crates-io #{3}# p pco) #:use-module (crates-io))

(define-public crate-pco-0.0.0-alpha.0 (c (n "pco") (v "0.0.0-alpha.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "05g95cp13vac71snz02kiim31cfnwhq9gh2sc9j0cs3v4vw37nfm")))

(define-public crate-pco-0.0.0-alpha.1 (c (n "pco") (v "0.0.0-alpha.1") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1k5hkaclfg3q29zz6qncf9ccn2d5hddca2lrwdk861zmav62wfr5")))

(define-public crate-pco-0.0.0-alpha.2 (c (n "pco") (v "0.0.0-alpha.2") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0xq60zdfacwqw6grnlv2wmgqr8xbz8cmhqijjzfnjmvn90cln5pj")))

(define-public crate-pco-0.0.0-alpha.3 (c (n "pco") (v "0.0.0-alpha.3") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1wld0x5vpb46kkh5s4xpxpjd75gkxb0sawikhj8ncxjdma53y5z4")))

(define-public crate-pco-0.0.0 (c (n "pco") (v "0.0.0") (d (list (d (n "better_io") (r "^0.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "156d7gilm4m6fh95kjmiw05fqx1f9y7xxqamjglzb1phmhpid7hq") (y #t)))

(define-public crate-pco-0.1.0 (c (n "pco") (v "0.1.0") (d (list (d (n "better_io") (r "^0.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0hir1gcn3yvvag81c0jl2k85algnyaa6ahhxmqqqr96lb984v6l2")))

(define-public crate-pco-0.1.1 (c (n "pco") (v "0.1.1") (d (list (d (n "better_io") (r "^0.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0azah05yy4hsy40x1w27ba9fnpdgbyi6g1l4cgv4ai2w1j66l9ac")))

(define-public crate-pco-0.1.2 (c (n "pco") (v "0.1.2") (d (list (d (n "better_io") (r "^0.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0j6a015gd215hakba98bn54zxchrhkkb0vzqkawf18dwxajaw8bg")))

(define-public crate-pco-0.1.3 (c (n "pco") (v "0.1.3") (d (list (d (n "better_io") (r "^0.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "07phav26mlwihg8hb64b4iz6ap9lkn8c62cajh064fnjvayqwhg6")))

(define-public crate-pco-0.2.0 (c (n "pco") (v "0.2.0") (d (list (d (n "better_io") (r "^0.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "052k63rlvsnzxjjmbpsqz9dkr0jhz7ygc6qqsxv5c7abiaaqyv00") (r "1.73.0")))

(define-public crate-pco-0.2.1 (c (n "pco") (v "0.2.1") (d (list (d (n "better_io") (r "^0.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1m3pbh9n0bsndlfjhcqbfk8wwrk2y7n5w92z93vkl5lfh1wc6xa4") (r "1.73.0")))

(define-public crate-pco-0.2.2 (c (n "pco") (v "0.2.2") (d (list (d (n "better_io") (r "^0.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "14imyligfbcfqvv1qb17n55ywhpalrvp95ldjv3vzgcdj8w93iay") (r "1.73.0")))

(define-public crate-pco-0.2.3 (c (n "pco") (v "0.2.3") (d (list (d (n "better_io") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1hrwn5ggsanjp5bxp7kfgbkvv7kyyfz9j9nzdh8jnwpapp7hpjqr") (r "1.73.0")))

(define-public crate-pco-0.2.4 (c (n "pco") (v "0.2.4") (d (list (d (n "better_io") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1ip4g5k3yzl213yps3j1ws2i53x5smh6m2zgh0p0wcbv293qbms3") (y #t) (r "1.73.0")))

(define-public crate-pco-0.2.5 (c (n "pco") (v "0.2.5") (d (list (d (n "better_io") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.19.2") (f (quote ("full"))) (d #t) (k 2)))) (h "15qq6m54nx8gnha5dkxjjr8ia8bz17q0hzy517ddd2iirhjfwblz") (r "1.73.0")))

