(define-module (crates-io #{3}# p pmv) #:use-module (crates-io))

(define-public crate-pmv-0.1.0 (c (n "pmv") (v "0.1.0") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "atty") (r "~0.2") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (f (quote ("color" "wrap_help"))) (d #t) (k 0)))) (h "10fm4ydigdbq7lbniqmas1kairrbgnfyyqpbv40qd503xisj8dcy")))

(define-public crate-pmv-0.1.1 (c (n "pmv") (v "0.1.1") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "atty") (r "~0.2") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (f (quote ("color" "wrap_help"))) (d #t) (k 0)))) (h "0b93xzy35slwzscwqwyyfj9kys0lqmf6g2kjzp2lvb36n8k8i47p")))

(define-public crate-pmv-0.4.0 (c (n "pmv") (v "0.4.0") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "atty") (r "~0.2") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (f (quote ("color" "wrap_help"))) (d #t) (k 0)))) (h "0j9x66n1rxgrs885ci3grrq5v0dar7kbxpmcld2b215adm91xaya")))

(define-public crate-pmv-0.4.1 (c (n "pmv") (v "0.4.1") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "atty") (r "~0.2") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (f (quote ("color" "wrap_help"))) (d #t) (k 0)) (d (n "function_name") (r "^0.2.0") (d #t) (k 2)))) (h "1di0yzxb3w23l9x7nr3zhcg6yv52k0jgjjaaz8m57yqcg0ifqm9h")))

(define-public crate-pmv-0.4.2 (c (n "pmv") (v "0.4.2") (d (list (d (n "ansi_term") (r "~0.12.1") (d #t) (k 0)) (d (n "atty") (r "~0.2") (d #t) (k 0)) (d (n "clap") (r "^3.2.5") (f (quote ("cargo" "color" "deprecated" "wrap_help"))) (d #t) (k 0)) (d (n "function_name") (r "~0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "10qx485hybsy4yd633vxlxz7zlf8ifx4w5wbfmbdrf6xpz5pn6nq")))

