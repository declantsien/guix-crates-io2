(define-module (crates-io #{3}# p pig) #:use-module (crates-io))

(define-public crate-pig-0.0.1 (c (n "pig") (v "0.0.1") (d (list (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "quicli") (r "^0.3") (d #t) (k 0)))) (h "10ild87rdzh13g1aymrw42k7gb2vjs79gwc4mbcx21sf3ihcc0q9")))

(define-public crate-pig-0.0.2 (c (n "pig") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "quicli") (r "^0.3") (d #t) (k 0)))) (h "1i2sbvn8sq2qdhfigvzg0h1ms8jzyy75mvshywrdq48l2npmjgad")))

(define-public crate-pig-0.0.3 (c (n "pig") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "quicli") (r "^0.3") (d #t) (k 0)))) (h "0ji98glnmgnvk2vmlkslg15slk2yspxlgwzyykd37l4j9iwa53yf")))

(define-public crate-pig-0.0.4 (c (n "pig") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "quicli") (r "^0.3") (d #t) (k 0)))) (h "1iy178ivsxr5hwwjjwqq7h6qmjgp3irjc1a9ym9bqar4774ja5yl")))

