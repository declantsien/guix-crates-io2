(define-module (crates-io #{3}# p pgm) #:use-module (crates-io))

(define-public crate-pgm-0.0.2 (c (n "pgm") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pgw") (r "^0.1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0vcp72c4cvz9cpd712xnxxmydm824g0fwmi1js9j5lcb2fprmhsd")))

(define-public crate-pgm-0.0.3 (c (n "pgm") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pgw") (r "^0.1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0sp3rnhyxk4c0vi44hhc9z6csm644201w3dlmy5v6h3mils81kx1")))

(define-public crate-pgm-0.0.4 (c (n "pgm") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pgw") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0km3kjbv1l0wf9rz42wy2v5j8k4myga3fqp2g84bkcmw8pqla3bj")))

(define-public crate-pgm-0.0.5 (c (n "pgm") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pgw") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "084c9a40rzib03m0a6lkxjphi4f3jzb6hj4jg39p54d3rihgsmkj")))

(define-public crate-pgm-0.0.6 (c (n "pgm") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pgw") (r "^0.2.2") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0fxn941cbn4050qs12xaf9k9dx7j9mgz7640pf693xn1jam5b15y")))

(define-public crate-pgm-0.0.7 (c (n "pgm") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pgw") (r "^0.2.4") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0whdi886lipm5j23sq6xrjn140nvghk7jchppgb2agzjzxxpwmmw")))

(define-public crate-pgm-0.0.8 (c (n "pgm") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "pgw") (r "^0.2.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0jamma0y9v0c2c78fqzfvaz7pkkbladmc3w58g6qdih2m73da9qh")))

