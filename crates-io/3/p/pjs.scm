(define-module (crates-io #{3}# p pjs) #:use-module (crates-io))

(define-public crate-pjs-0.1.0 (c (n "pjs") (v "0.1.0") (h "1ipqf5phhl80l7irx9dfp388jz3wvjz8ldjc626mmd8csza4kkpr")))

(define-public crate-pjs-0.1.1 (c (n "pjs") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ikz7fgdg65lchq8rp0zigp6wx1ssrblkzba5ihwzs5rcrfbb8jd")))

(define-public crate-pjs-0.1.2 (c (n "pjs") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01halsq2wajn0vfqzi29fzg9xb29ic3zfyp0mc6akxmq6vxn4yrr")))

(define-public crate-pjs-0.1.3 (c (n "pjs") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s4629y1lhdgbw09bvbav2q1yhlr714bric8pa220vqcdpra9km0")))

