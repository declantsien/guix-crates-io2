(define-module (crates-io #{3}# p pwg) #:use-module (crates-io))

(define-public crate-pwg-0.1.0 (c (n "pwg") (v "0.1.0") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0d2f05br68j0i4hzf2x1wsh2h8kas7lm13lcsrxbhwj4pc8zcw7x")))

(define-public crate-pwg-0.1.1 (c (n "pwg") (v "0.1.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0x40jsyd01ipymarg24zwrm41lff168mhqdakdz90c9jiriidc38")))

