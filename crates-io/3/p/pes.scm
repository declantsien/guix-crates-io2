(define-module (crates-io #{3}# p pes) #:use-module (crates-io))

(define-public crate-pes-0.1.0 (c (n "pes") (v "0.1.0") (h "026n2wppj89xxi24ixwxzlm80pgj5jcpb9drkpaj5b8v4s5lw5yc")))

(define-public crate-pes-0.2.0 (c (n "pes") (v "0.2.0") (h "1cfr8gx6qh5p9bqz5wc9im3993baf3fzh8fxl790wnnb1djpw7nb")))

