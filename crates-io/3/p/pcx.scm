(define-module (crates-io #{3}# p pcx) #:use-module (crates-io))

(define-public crate-pcx-0.1.0 (c (n "pcx") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "03yv7c7qyf88z3az61pb0qqxjicqb3q6ngy9mi8hbw63fshkcfrp")))

(define-public crate-pcx-0.1.1 (c (n "pcx") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0crc6a5z64c81vgwh8zdl26qsa0kx041zcigdfhwcsg7nckgrk05")))

(define-public crate-pcx-0.1.2 (c (n "pcx") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "image") (r "^0.10.4") (f (quote ("png_codec"))) (k 2)) (d (n "walkdir") (r "^1.0.3") (d #t) (k 2)))) (h "00bi8j5gbjk8m8p0zkfc1i31ycd44x615kbs8xvndhamyyamvadf")))

(define-public crate-pcx-0.2.0 (c (n "pcx") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "image") (r "^0.10.4") (f (quote ("png_codec"))) (k 2)) (d (n "walkdir") (r "^1.0.3") (d #t) (k 2)))) (h "1qa0g5kcl6p0pwp64zi0rgjlvndr6zjpp3jl1636k9c9a63bvwva")))

(define-public crate-pcx-0.2.1 (c (n "pcx") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (f (quote ("png_codec"))) (k 2)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 2)))) (h "1zm80kxzydjk2s04s4h2y4f4vhfj114pn713x9zdyq7cjcwki40j")))

(define-public crate-pcx-0.2.2 (c (n "pcx") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (f (quote ("png_codec"))) (k 2)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 2)))) (h "13v978jahshp2vxbl2nxdx62zsi98li2j28a0xh097x0rdxlzhav")))

(define-public crate-pcx-0.2.3 (c (n "pcx") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.2.6") (d #t) (k 0)) (d (n "image") (r "^0.20.0") (f (quote ("png_codec"))) (k 2)) (d (n "walkdir") (r "^2.2.5") (d #t) (k 2)))) (h "06j8hwy8kl3ijfnz313kh86pc8g8hrgkky33g275fxipjxib000v")))

