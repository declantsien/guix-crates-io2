(define-module (crates-io #{3}# p pkz) #:use-module (crates-io))

(define-public crate-pkz-0.1.0 (c (n "pkz") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "async_zip") (r "^0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "05ddml681zwvsxg5yq68g8yq9s0n8yl2mh1kp3m5m1cwvxl3vif0")))

