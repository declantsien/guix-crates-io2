(define-module (crates-io #{3}# p paw) #:use-module (crates-io))

(define-public crate-paw-0.0.0 (c (n "paw") (v "0.0.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "clap_flags") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "188lyk5wd8n39aqpbky030fh9i8wciba46s9kh3k239v1gzl875q")))

(define-public crate-paw-1.0.0 (c (n "paw") (v "1.0.0") (d (list (d (n "paw-attributes") (r "^1.0.0") (d #t) (k 0)) (d (n "paw-raw") (r "^1.0.0") (d #t) (k 0)) (d (n "paw-structopt") (r "^1.0.0") (d #t) (k 2)) (d (n "runtime") (r "^0.3.0-alpha.2") (d #t) (k 2)) (d (n "structopt") (r "^0.2.15") (d #t) (k 2)))) (h "1sc481y42rb08hmww525m4539ppl8k0w14kwxp13vg2dasdzrh09")))

