(define-module (crates-io #{3}# p pfy) #:use-module (crates-io))

(define-public crate-pfy-0.1.0 (c (n "pfy") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "personify") (r "^0.1") (d #t) (k 0)))) (h "07dnfd0gd6khi9d7hnd5ik1qrrz59bq9nkmi39zhpkqjx8bk41z9") (f (quote (("persona-unstable" "personify/unstable"))))))

