(define-module (crates-io #{3}# p pxl) #:use-module (crates-io))

(define-public crate-pxl-0.0.0 (c (n "pxl") (v "0.0.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)))) (h "19y9yxklfafz8gmqj2hxji01y4bs8mz83l0ymlfhs7jb7c3lhrr0")))

(define-public crate-pxl-0.0.1 (c (n "pxl") (v "0.0.1") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)))) (h "1qbakh19pi1sc9hkfal1wb81sqbb96xlml6v4dwmwy5c2rmkilax")))

(define-public crate-pxl-0.0.2 (c (n "pxl") (v "0.0.2") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)))) (h "14lvz00dnc5mkcikgb3cvxkbpydgja91ddhpcxv90lrf9p85pb3b")))

(define-public crate-pxl-0.0.3 (c (n "pxl") (v "0.0.3") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)))) (h "0ha00hchs88iqhmsr2ny6av3g556m8nycvq7n89bm3kg53z3idsn")))

(define-public crate-pxl-0.0.4 (c (n "pxl") (v "0.0.4") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)))) (h "0dx2vvxq0hz9zlhry9khdyi56bl7r4rm5krfhgf9fxychzdjb519")))

(define-public crate-pxl-0.0.5 (c (n "pxl") (v "0.0.5") (d (list (d (n "cpal") (r "^0.8.1") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)))) (h "0f0n7vqj54sr6y7z5382p0zkamnwbsyif0x52k1fpwfiz1js8gnj")))

(define-public crate-pxl-0.0.6 (c (n "pxl") (v "0.0.6") (d (list (d (n "cpal") (r "^0.8.1") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)))) (h "1nbqz10ydry1vvgcxqz4sp4fh45avgw08pv6wv9dkd04yj9awd41")))

(define-public crate-pxl-0.0.7 (c (n "pxl") (v "0.0.7") (d (list (d (n "cpal") (r "^0.8.1") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)))) (h "172q1an5bf5bbkm3g44pjf3dd4yynsrgk6ncayxcy0gqzggzjpd5")))

(define-public crate-pxl-0.0.8 (c (n "pxl") (v "0.0.8") (d (list (d (n "cpal") (r "^0.8.1") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)))) (h "1qpsflm529pyym92ascsx4vvyc1dhdi7ls9bwdiipc3nrhrz61fp")))

(define-public crate-pxl-0.0.9 (c (n "pxl") (v "0.0.9") (d (list (d (n "cpal") (r "^0.8.1") (d #t) (k 0)) (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glutin") (r "^0.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)))) (h "0shwdsjsdw8c21yrdr7794plgynhybpwidcm8qy57yqr0c07a1wx")))

