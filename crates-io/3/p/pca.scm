(define-module (crates-io #{3}# p pca) #:use-module (crates-io))

(define-public crate-pca-0.1.0 (c (n "pca") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-static"))) (d #t) (k 0)))) (h "1ibhmrz5rf43s06kdi6a451ylfklqzz78irwbq5h8l010np4q4is")))

(define-public crate-pca-0.1.1 (c (n "pca") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-static"))) (d #t) (k 0)))) (h "1057rp70lyp6xc2pzkhf94dy2jvw8yhgxf1s4b52wz2kvnhl5jbj")))

(define-public crate-pca-0.1.2 (c (n "pca") (v "0.1.2") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-static"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0x6b575xwp138m3hqipxafb6w0qfmk5vfrihanply52r6b577az7")))

(define-public crate-pca-0.1.3 (c (n "pca") (v "0.1.3") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-static"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsvd") (r "^0.1.2") (d #t) (k 0)))) (h "0gc3vcnnpn5z371428smh0abfz809j7qiqnlnf562y2q7smhvqfh")))

(define-public crate-pca-0.1.4 (c (n "pca") (v "0.1.4") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (f (quote ("openblas-static"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "rsvd") (r "^0.1.2") (d #t) (k 0)))) (h "1b55yaq5k8l3la2s1slj8anfdhb65k0bdpkqz8afip1jjhi4qwdh")))

