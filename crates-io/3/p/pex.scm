(define-module (crates-io #{3}# p pex) #:use-module (crates-io))

(define-public crate-pex-0.0.0 (c (n "pex") (v "0.0.0") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "07wf7qlasnyfs39zr42mm0mwdj7zdrjb46035j6nbwm9gq7rxm5p") (f (quote (("default"))))))

(define-public crate-pex-0.0.1 (c (n "pex") (v "0.0.1") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0pphr4crq6sp4kn62i1cdnb8v4w1nbc5f4zwns0875frn00sl146") (f (quote (("default"))))))

(define-public crate-pex-0.0.2 (c (n "pex") (v "0.0.2") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "04i728jhrpj1gb3rjvvrgmkvbvp025b7fvs2nhhii67sm1zbc1vz") (f (quote (("default"))))))

(define-public crate-pex-0.0.3 (c (n "pex") (v "0.0.3") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1b0shg69474d1y71ycplak3iyajwylck8g9h7hixpdj014hkykjr") (f (quote (("default"))))))

(define-public crate-pex-0.0.4 (c (n "pex") (v "0.0.4") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "16zbw0bqljw5ryyhbi7xhxm41y449xvagzr8h54071zz9rn2c5zp") (f (quote (("default"))))))

(define-public crate-pex-0.0.5 (c (n "pex") (v "0.0.5") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0giwrnziji641wysjb1y9acdw0lsa99rxpq9ifm25wb9g4h81b1h") (f (quote (("default"))))))

(define-public crate-pex-0.0.6 (c (n "pex") (v "0.0.6") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0168l7y5qa6sw6dyrgs5b8jg9hrzyz25xndk6fljbs0q0pid0k8k") (f (quote (("default"))))))

(define-public crate-pex-0.0.7 (c (n "pex") (v "0.0.7") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1yvzqkr9crpjywpid1k6hb8jpxfqfp56dfb44hkagv7cwl97qgnb") (f (quote (("default"))))))

(define-public crate-pex-0.0.8 (c (n "pex") (v "0.0.8") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0dssq922p1bs8w5bdn4zlcx2ncl0787alm6d3wbcakdycq7fa43r") (f (quote (("default"))))))

(define-public crate-pex-0.0.9 (c (n "pex") (v "0.0.9") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1srgm0xivfgchgaw45xa3rmg2w1za7ayp8z1v23rk7qmnxa7pr13") (f (quote (("default"))))))

(define-public crate-pex-0.0.10 (c (n "pex") (v "0.0.10") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "093mh0qxhn6pl7l4cri12ykwg6z8v080h6rl4c18nblbr8d4x4l5") (f (quote (("default"))))))

(define-public crate-pex-0.0.11 (c (n "pex") (v "0.0.11") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1jfvnfm9dm17v3qd8j5c0lqpdzv2f8gib6ixjnvy68r7ia17mbxk") (f (quote (("default"))))))

(define-public crate-pex-0.0.12 (c (n "pex") (v "0.0.12") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0lvphb0mnrrpx7r7xjmyp8401l7ixnvizc56dmay3srghk5z5asl") (f (quote (("default"))))))

(define-public crate-pex-0.1.0 (c (n "pex") (v "0.1.0") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "18kd3jpy305z89bc2l2p216a6j6pnw7x886sm15p480k4z39xwal") (f (quote (("default"))))))

(define-public crate-pex-0.1.1 (c (n "pex") (v "0.1.1") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0hz2jcr8j0hik2079lmz67646syqjj4bs0aprfsmm6xnbsfvwr21") (f (quote (("default"))))))

(define-public crate-pex-0.1.2 (c (n "pex") (v "0.1.2") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "07f1bxsvw5blxk8rsck2lndzfcwsjlfg95mmrf4b8srq1dsqplmf") (f (quote (("default"))))))

(define-public crate-pex-0.1.3 (c (n "pex") (v "0.1.3") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "12hvv8siqgbr22kbnhh3p49n3g22s4w9jignn6h8xfz0hxhz7a4i") (f (quote (("default"))))))

(define-public crate-pex-0.1.4 (c (n "pex") (v "0.1.4") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1jfsswry93q99s55h4cf71q8accamdcd4cz2z6xzfz76nbgzx8kf") (f (quote (("default"))))))

(define-public crate-pex-0.1.5 (c (n "pex") (v "0.1.5") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "16hjaqfbcakf6gp9mwvf2z5ypyfzzp595w1dp6p8kzgdndkn9j2s") (f (quote (("default"))))))

(define-public crate-pex-0.1.6 (c (n "pex") (v "0.1.6") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "00d2l863hyv3zw06085aw126vkhszagc8zhngaxcz9114x59jwph") (f (quote (("default"))))))

(define-public crate-pex-0.1.7 (c (n "pex") (v "0.1.7") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "06q6zbqfzpqb4g37hqhsg0jkilzqrcswh3q2lj0p8b21kwvqp4ws") (f (quote (("default"))))))

(define-public crate-pex-0.1.8 (c (n "pex") (v "0.1.8") (d (list (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1gk62yqdxpjgaqx6kxann0kbyx9079mjh5d7waxxpi6grrisp6k3") (f (quote (("default"))))))

(define-public crate-pex-0.1.9 (c (n "pex") (v "0.1.9") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0ikkagaqfmg29fzy457s0qbsf9bl79ij94hd54cm5qfiv5k1yrvk") (f (quote (("default"))))))

(define-public crate-pex-0.1.10 (c (n "pex") (v "0.1.10") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1g2xyq9yx08gn79scqm6mmlcd4wl1j775sb3l0z333av49vwvwq5") (f (quote (("default"))))))

(define-public crate-pex-0.1.11 (c (n "pex") (v "0.1.11") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1ac12mn9zi63lxmwa3idpffs7z3yzbwh2zlvqdalwfk4rd7gba6y") (f (quote (("default"))))))

(define-public crate-pex-0.1.12 (c (n "pex") (v "0.1.12") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "1n3y1cwcym578s7axkmz6xwxxkxa7lsswm6bhswai7gc1v6f0pgc") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.1.13 (c (n "pex") (v "0.1.13") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "12n32v0x04zgyifk8krx05zldh0klfsc166wcdgvmx48k8qbd796") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.1.14 (c (n "pex") (v "0.1.14") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0161vndww6z6r2354sz5v22iymqv7cbv340ajymhmpmc1a9hr1ds") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.1.15 (c (n "pex") (v "0.1.15") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0dbvzlnv58ql1jwdabpnk1vc9ghrkpp245rc3z3nj8zckyq9ax5j") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.1.16 (c (n "pex") (v "0.1.16") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "176r7dlinsj6kk9hmz1w52kpqw2s4r7qwmlb6nbn38051lcf1m18") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.1.17 (c (n "pex") (v "0.1.17") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "regex-automata") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0hrv36i49c7q9q340f4j308ib20sqvhshzzcz1hc42bi38v350jz") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.1.18 (c (n "pex") (v "0.1.18") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0hjkm7a033bmkpv8qxy5wrhjcmsl6rz04pv38qvsljg0h121sax1") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.2.0 (c (n "pex") (v "0.2.0") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "ucd-trie") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0lr9dgngr8kwamp268i691kxd2vbgb2d0m0qsvsqs20xs3d6dl68") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.2.1 (c (n "pex") (v "0.2.1") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)))) (h "0ywivzy2y02q606p66x1fadgbq5zj3g6yk1b80hq2zxalj8nxhr9") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.2.2 (c (n "pex") (v "0.2.2") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)))) (h "1y62f48zq0xailpscg1zam961rxf4dv37ifzq057ha30wvyli4yw") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.2.3 (c (n "pex") (v "0.2.3") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)))) (h "1rbf3a2ss95cp0qiscam3glr8sa36l1j8rkwmawr0azhllcj664s") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-pex-0.2.4 (c (n "pex") (v "0.2.4") (d (list (d (n "regex") (r "^1.8.1") (o #t) (d #t) (k 0)))) (h "14w4502pwdnfvrkdnnn46kxvd9w26ij0nf6006sk17d41bpikbs4") (f (quote (("default" "alloc") ("alloc"))))))

