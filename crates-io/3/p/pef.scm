(define-module (crates-io #{3}# p pef) #:use-module (crates-io))

(define-public crate-pef-0.1.0 (c (n "pef") (v "0.1.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "0alw7z3bwdfjh6r2zjkq2vmp1rj4ynwdsbga7s9cz8wzzdynlb22")))

(define-public crate-pef-0.1.1 (c (n "pef") (v "0.1.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "0kf7cxh8mv64qzwcs8yf7wkg3dfaackaxyglsxwla64fhldm6yan")))

(define-public crate-pef-0.1.2 (c (n "pef") (v "0.1.2") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "0jylx3w30jmcps9xw85inv56miinycwll3228dj0i9qin9w84iy0")))

(define-public crate-pef-0.1.3 (c (n "pef") (v "0.1.3") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 2)))) (h "03bndllz5if3z3him0ivvqwg27bi1pgnaarpd6giyr22qr9dcmin")))

