(define-module (crates-io #{3}# p psa) #:use-module (crates-io))

(define-public crate-psa-0.1.0 (c (n "psa") (v "0.1.0") (d (list (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "maplit") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "09bp8jr46cb4nqjvcs8gdwbbbr88qlh4m7n4xc7nqpm8gnyyl7d1")))

(define-public crate-psa-0.1.1 (c (n "psa") (v "0.1.1") (d (list (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "maplit") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0rhjbvl37gjpckqp2d22ar3bh16zlpykxzzbvjvmbfzs86vkbzmk")))

