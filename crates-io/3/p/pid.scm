(define-module (crates-io #{3}# p pid) #:use-module (crates-io))

(define-public crate-pid-1.0.0 (c (n "pid") (v "1.0.0") (h "0hp7cc40fya40f0jpxdmjh6d5m8x2j6ry6fk3nb8ymnwqi40c2ym")))

(define-public crate-pid-1.1.0 (c (n "pid") (v "1.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1yjxm3716fnklkl3glcli29bi5ab2mh8k87cb0ijfawlr29mn63h")))

(define-public crate-pid-1.1.1 (c (n "pid") (v "1.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1l2lg7cf8c6lcyrb8i5vglrqwjy2zlv9gixniqhx1sqicpng8992")))

(define-public crate-pid-1.2.1 (c (n "pid") (v "1.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ngbfyqs6dgbc98ggwkp42dqpdvmwas21f4ff7q0slrjjzbmz61j")))

(define-public crate-pid-1.3.0 (c (n "pid") (v "1.3.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1n334dvvcylv8s38vzp5vklmmqzgyabl3f3i1j6ix2x58z3zk2gr")))

(define-public crate-pid-2.0.0 (c (n "pid") (v "2.0.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "0gy5gyi1c3wa6s3825ajbs32myska17hx9nzygka7955smq6v95y")))

(define-public crate-pid-2.1.0 (c (n "pid") (v "2.1.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0knyqn4w1l74nykvd29drjypkn736ix1fcfhpzjz1w9c6hm3c8jj")))

(define-public crate-pid-2.2.0 (c (n "pid") (v "2.2.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "13x3ndfppk5hqi9y2073kg4w6q5z7jczahnwwml1nqc3zlnljjm9") (y #t)))

(define-public crate-pid-3.0.0 (c (n "pid") (v "3.0.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "02j8nnrkxcv462dafw6bcracjcfwn3jjb2c9p5lxkhqf5ws685yq")))

(define-public crate-pid-4.0.0 (c (n "pid") (v "4.0.0") (d (list (d (n "num-traits") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "0djfldyd29r9n7vahv1cc7qadpzlkphbz5fklczmxkanjzpk3jfp")))

