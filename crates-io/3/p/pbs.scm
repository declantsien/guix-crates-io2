(define-module (crates-io #{3}# p pbs) #:use-module (crates-io))

(define-public crate-pbs-0.0.1 (c (n "pbs") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 0)) (d (n "bindgen") (r "^0.61") (d #t) (k 1)))) (h "0ng74kngyg74yamgnilhfyzc2w51slcpbfl1v4qk3lcfkb4k977y")))

(define-public crate-pbs-0.0.2 (c (n "pbs") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 0)) (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_c") (r "^0.0.1") (d #t) (k 0)))) (h "1yspvxsia0b4syazjn64v7xvjvqyfgk0grzp9lhnypa40vmx1c6l")))

(define-public crate-pbs-0.0.3 (c (n "pbs") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 0)) (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_c") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1cg2abadnnbkwmrhbwbmzm2yxv2kqzglhn1rxgd03r3panxwqbvr")))

(define-public crate-pbs-0.0.4 (c (n "pbs") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_c") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pbs-sys") (r "^0.0.2") (f (quote ("static"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ik1lp4h8hv42bdl81y301sn20nfq4m6s30x6bq6hkszc68q67by")))

(define-public crate-pbs-0.0.5 (c (n "pbs") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_c") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pbs-sys") (r "^0.0.2") (f (quote ("static"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0z2r3x49vvqdv3xsqz70c4wgzx1ic13f15j4c6fc8wgdhrqnizr8")))

(define-public crate-pbs-0.0.6 (c (n "pbs") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linked_list_c") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pbs-sys") (r "^0.0.2") (f (quote ("static"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ad63727vs1kr5q7rm8w0kr7pzvdm96piycmqi8kzzxr03qq2dl7")))

