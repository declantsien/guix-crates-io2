(define-module (crates-io #{3}# p pls) #:use-module (crates-io))

(define-public crate-pls-0.1.0 (c (n "pls") (v "0.1.0") (d (list (d (n "rust-ini") (r "^0.10") (d #t) (k 0)))) (h "1nrpcvhsqhs5z1j6slfs56vgd8dy4ginmiah0bkhljbzbp9mxbdm")))

(define-public crate-pls-0.2.0 (c (n "pls") (v "0.2.0") (d (list (d (n "rust-ini") (r "^0.10") (d #t) (k 0)))) (h "1lb50kpq6vrad650wzv6dfn7snajc7mv028i9392p6ysn7408nsh")))

(define-public crate-pls-0.2.1 (c (n "pls") (v "0.2.1") (d (list (d (n "rust-ini") (r "^0.10") (d #t) (k 0)))) (h "1akx79j4vibd75ppr2mav0r7wawpmw4wfvlfz19frrh7rcbxrrga")))

(define-public crate-pls-0.2.2 (c (n "pls") (v "0.2.2") (d (list (d (n "rust-ini") (r "^0.10") (d #t) (k 0)))) (h "0i9bbyycq70rj0g976aazjy1rq4ykgd4sjcgcm1px2i8gvybg91z")))

