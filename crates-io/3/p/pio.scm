(define-module (crates-io #{3}# p pio) #:use-module (crates-io))

(define-public crate-pio-0.0.1 (c (n "pio") (v "0.0.1") (h "0vy0zda7dsgkpvc1hnk0dxxbw943chnh5n960z2jjjw9d3zihp4j")))

(define-public crate-pio-0.1.0 (c (n "pio") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)))) (h "1h5byywk6gp0x1zlzjsbbrkc93fxm41ka83nbgfzhchyjmw4jfsr")))

(define-public crate-pio-0.2.0 (c (n "pio") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)))) (h "18n080g81y00j1qsmd4gcbn4lcih0s1mwzydi5jpwla2vbm45mhg")))

(define-public crate-pio-0.2.1 (c (n "pio") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "num_enum") (r "^0.5") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "test-generator") (r "^0.3.0") (d #t) (k 2)))) (h "1qvq03nbx6vjix7spr5fcxcbxw39flm1y72kxl1g728gnna9dq3n")))

