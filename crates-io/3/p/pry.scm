(define-module (crates-io #{3}# p pry) #:use-module (crates-io))

(define-public crate-pry-0.1.0 (c (n "pry") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "read_pipe") (r "^0.1.2") (d #t) (k 0)) (d (n "unrar") (r "^0.4.4") (d #t) (k 0)))) (h "1f62mwznzb4fv7j07idvzy7rl1wk09fa8q8fw1ia4s3mzq7lbnyz")))

