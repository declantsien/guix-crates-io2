(define-module (crates-io #{3}# p pbj) #:use-module (crates-io))

(define-public crate-pbj-0.2.0 (c (n "pbj") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colog") (r "^1.3.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0nl0jgsy1w395jz1mycfnvqp164ngiwmri6iys7npd6rrvrs8zn6")))

(define-public crate-pbj-0.2.1 (c (n "pbj") (v "0.2.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colog") (r "^1.3.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0bbb5s6c2svb6l1m8fw3am3f9p1jg4s0a9bdv8w1nx0i7zyjjcml") (y #t)))

(define-public crate-pbj-0.2.2 (c (n "pbj") (v "0.2.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colog") (r "^1.3.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0gwyn8av78hg127wzr1mb73i8hagc0jw99csfdx8npb33w3r2jz3") (y #t)))

(define-public crate-pbj-0.2.3 (c (n "pbj") (v "0.2.3") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colog") (r "^1.3.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0ky5950fkinkm9y3qn4ax7ys2rvh43xvxy82c8xf5gqxrwfx5c0j") (y #t)))

(define-public crate-pbj-0.2.4 (c (n "pbj") (v "0.2.4") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colog") (r "^1.3.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "00gajxwi9jcjs3i98dskblxnjnwqzgkyrz5qx2y8hhymrg1zn1kx") (y #t)))

(define-public crate-pbj-0.2.5 (c (n "pbj") (v "0.2.5") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colog") (r "^1.3.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "14bx78jan8jldg02whvazr2v5nn6ym615w14zm4rl98h465md45h")))

(define-public crate-pbj-0.2.6 (c (n "pbj") (v "0.2.6") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colog") (r "^1.3.0") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1q18kbqnvd62jvz00vdwb8csjnv3rysg1a2094ivrn80vjsg0z60")))

