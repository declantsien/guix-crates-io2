(define-module (crates-io #{3}# p psd) #:use-module (crates-io))

(define-public crate-psd-0.1.0 (c (n "psd") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1yxb7wp985pca159mjcmip5y9glq0nk1y7wd8xwsgnil6p5n3grz")))

(define-public crate-psd-0.1.1 (c (n "psd") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0mw8h1nj9nn1f6dbzbwpam4whf6jcfbzwvvjihif3l0ajn4b1cpl")))

(define-public crate-psd-0.1.2 (c (n "psd") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1x46xg8j8y1p8jw1a0pfyyysn699i02gnhi7xn17rs8qcaw9mqrp")))

(define-public crate-psd-0.1.3 (c (n "psd") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "14qc17q51kvzi0yr1qvcdsj2g1z12gbi81z5hb9b4bgi58nnf40v")))

(define-public crate-psd-0.1.4 (c (n "psd") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1v2hvia790p21px1gyl4xzp6cps4i4ysgwm6qh1169hkpz32nphx")))

(define-public crate-psd-0.1.5 (c (n "psd") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0akqyc2g2zaymp4vmvdrpv135cs3a3wk5ps38pn0drb9r71ac0q6")))

(define-public crate-psd-0.1.6 (c (n "psd") (v "0.1.6") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1dna3sxg83c7nc4nw9ldn46fayh5a8xlfzzm0sd8kh65gva2zy5x")))

(define-public crate-psd-0.1.7 (c (n "psd") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0fihdnsryx1yp43fj7j02hbha5rvffc7kj6rg7a9xa8kyiax7ncr")))

(define-public crate-psd-0.1.8 (c (n "psd") (v "0.1.8") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0c09mxkcj4zb2lixqmp4jqgryxhkgj7y7ygdigy8lqyxhhzg9wbm")))

(define-public crate-psd-0.1.9 (c (n "psd") (v "0.1.9") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "139abiix31dqfnchqpwlkr5kr35ikgscn9ynddi3hhj0cwqkqf8f")))

(define-public crate-psd-0.2.0 (c (n "psd") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1klhia64c3i0zfvy5d0qa5h07mzqwzg2ly156ql1xwhbi39j6iak")))

(define-public crate-psd-0.3.0 (c (n "psd") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11814r7j3awa87b2b16xskjrnlm23zm0w2f12vg540ndpy40z2b6")))

(define-public crate-psd-0.3.1 (c (n "psd") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z0q6m384p59rv8z5hq750j47jf9jd64v719qk1qhg5355jr62vc")))

(define-public crate-psd-0.3.2 (c (n "psd") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ny9hi07x04n414l2rkwihdmbxswvrm29bp0pl6rgswlxc5c29im")))

(define-public crate-psd-0.3.3 (c (n "psd") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16cjzzy2i03cj5ik39v3h2y03bs6ryi4xd19j29z04ai8a42c97l")))

(define-public crate-psd-0.3.4 (c (n "psd") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "112rwippd8i1sjmrd7na41b5gwfav9z2m2afiv4mzjfdb7pg85hk")))

(define-public crate-psd-0.3.5 (c (n "psd") (v "0.3.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "15civjshvdvm2m4a5yjmnyn0yylz4crs0cdg3f8mvmpzrywgj9cs")))

