(define-module (crates-io #{3}# p pap) #:use-module (crates-io))

(define-public crate-pap-0.1.1 (c (n "pap") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "strsim") (r "^0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1hf21cqm9sgc1yfd19qy9kamxbbh5yc7laq9ms05f4lfl9j6bwql")))

