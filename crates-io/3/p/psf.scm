(define-module (crates-io #{3}# p psf) #:use-module (crates-io))

(define-public crate-psf-0.1.0 (c (n "psf") (v "0.1.0") (d (list (d (n "flate2") (r "1.0.*") (o #t) (d #t) (k 0)))) (h "1505mhph9cjshi9rflf94zxybcrz83d45qaric4g4fdn1wdsws3n") (f (quote (("unzip" "flate2") ("default" "unzip"))))))

(define-public crate-psf-0.2.0 (c (n "psf") (v "0.2.0") (d (list (d (n "flate2") (r "1.0.*") (o #t) (d #t) (k 0)))) (h "1ic2bj8icifd5drv48dhrn8ckrvj072jm083rr3bkkvzkaqaqh8b") (f (quote (("unzip" "flate2") ("default" "unzip"))))))

