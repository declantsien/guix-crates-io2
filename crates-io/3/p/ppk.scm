(define-module (crates-io #{3}# p ppk) #:use-module (crates-io))

(define-public crate-ppk-0.1.0 (c (n "ppk") (v "0.1.0") (d (list (d (n "netstat2") (r "^0.9.1") (d #t) (k 0)) (d (n "psutil") (r "^3.2.2") (d #t) (k 0)))) (h "11sn3lgv9wfj45xicmkgczhkgvxn20gi29msy437vj558ipg3wwz")))

(define-public crate-ppk-0.1.1 (c (n "ppk") (v "0.1.1") (d (list (d (n "netstat2") (r "^0.9.1") (d #t) (k 0)) (d (n "psutil") (r "^3.2.2") (d #t) (k 0)))) (h "0z1jcrxb4a6l9k9zp6pk4lhs97mg9dw7vjplwllrdyar54cwfg0b")))

