(define-module (crates-io #{3}# p pkr) #:use-module (crates-io))

(define-public crate-pkr-0.1.0 (c (n "pkr") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1i3dcc8v48n5s4d090j24436wipb3isq9qkn7v97r5m0qkm84yqq")))

(define-public crate-pkr-0.1.1 (c (n "pkr") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "12kxxy1aa41661gf0yfgnz47lrfc4fgld961i4k9qpghv0aswzl0")))

