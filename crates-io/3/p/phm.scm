(define-module (crates-io #{3}# p phm) #:use-module (crates-io))

(define-public crate-phm-0.0.1 (c (n "phm") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "phm-icd") (r "^0.0.1") (d #t) (k 0)) (d (n "postcard") (r "^0.7.3") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "0llvgdfir23ylln667mfd9sd24sx4fjj1zy636nc1qg20y7psqr0")))

(define-public crate-phm-0.0.2 (c (n "phm") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.6") (d #t) (k 0)) (d (n "heapless") (r "^0.7.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "phm-icd") (r "^0.0.2") (d #t) (k 0)) (d (n "postcard") (r "^0.7.3") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serialport") (r "^4.0.1") (d #t) (k 0)))) (h "17g8mqac27gams41qsxp5ksjz67rwhwbh54czxhwi44zjvpc2i59")))

