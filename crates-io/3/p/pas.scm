(define-module (crates-io #{3}# p pas) #:use-module (crates-io))

(define-public crate-pas-0.1.0-beta.0 (c (n "pas") (v "0.1.0-beta.0") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "12rp5ci14madvjsn4bh3dcvcmm1ibyhbln7xsikr5qj2h333mxpi")))

(define-public crate-pas-0.1.0 (c (n "pas") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (f (quote ("derive"))) (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)))) (h "0d5g4xg9jnx9wa86impxi0r2xmvrqh57ss91da6cs3grjfrgwarl")))

