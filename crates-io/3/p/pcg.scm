(define-module (crates-io #{3}# p pcg) #:use-module (crates-io))

(define-public crate-pcg-0.1.0 (c (n "pcg") (v "0.1.0") (h "0w46vk5798hx6rv2c7x6n3r6kfv2hl86rcvfaw201ncszipdkdlb")))

(define-public crate-pcg-0.1.1 (c (n "pcg") (v "0.1.1") (h "1aa1q3l9xj65aaski61ibv63hzvv6g1llp6nd8zxrff7idb9rnav")))

(define-public crate-pcg-1.0.0 (c (n "pcg") (v "1.0.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.2.1") (d #t) (k 0)))) (h "0ymngxx0a5knh6mlkkqrh1l5q9dhhirs299vh2r672nwzlchh2rd")))

(define-public crate-pcg-1.0.1 (c (n "pcg") (v "1.0.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.3.0") (d #t) (k 0)))) (h "03cqnscl3np047509n8ck9wcfgg5jvs39ws7yq35jhh4653rfap9")))

(define-public crate-pcg-2.0.0 (c (n "pcg") (v "2.0.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "rand_core") (r "^0.3.0") (d #t) (k 0)))) (h "0cxn32s2j8l1fh68al5pyrizjsnsb9n9cn2831hghpcr8gycvsk8")))

(define-public crate-pcg-3.0.0 (c (n "pcg") (v "3.0.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0i5xym3hbjmi6wmr1b2yawarajhxb5wyxfm5rwk2kd6g556xqdy7") (f (quote (("serialize" "serde" "serde_derive") ("default"))))))

(define-public crate-pcg-3.0.1 (c (n "pcg") (v "3.0.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "16nlzqdfnp40pbffdwb9272yyjfkr0d5b6cfcl8cwvs51wgxzggx") (f (quote (("serialize" "serde" "serde_derive") ("default"))))))

(define-public crate-pcg-4.0.0 (c (n "pcg") (v "4.0.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1z3b1m049sscqq7q132hh52vfxm70wzmh0j5a8mn3338yb4sqfgp") (f (quote (("std") ("default" "std"))))))

(define-public crate-pcg-4.0.1 (c (n "pcg") (v "4.0.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "087aqyrag31ris1cc30av1qypfaihbr2qca266v6851ccqr2cqnf") (f (quote (("std") ("default" "std"))))))

(define-public crate-pcg-4.1.0 (c (n "pcg") (v "4.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0rgqkk39a73kz07wzx6xvk8z5i55wiz86585c069lag0s7bc0dax") (f (quote (("std") ("default" "std"))))))

