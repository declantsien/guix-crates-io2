(define-module (crates-io #{3}# p pcb) #:use-module (crates-io))

(define-public crate-pcb-0.1.0 (c (n "pcb") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "llvm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.1.0") (d #t) (k 0)))) (h "0ip4g7gj85hwkha50i125icw4876hhx4gfn5s7yj88xdnxyghnnj")))

(define-public crate-pcb-0.1.1 (c (n "pcb") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)) (d (n "llvm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "typed-arena") (r "^1.1.0") (d #t) (k 0)))) (h "12whg92iw0mhfz8z4hfdza8xjpj02xxl80ms2zmip6b2qgxm68yy")))

(define-public crate-pcb-0.1.3 (c (n "pcb") (v "0.1.3") (d (list (d (n "pcb-core") (r "^0.1.3") (d #t) (k 0)))) (h "06x78806n6d4fmhd5vypcw0jfvg8mqnwiss4k7gb15pi9rmd94j1")))

(define-public crate-pcb-0.2.0 (c (n "pcb") (v "0.2.0") (d (list (d (n "pcb-core") (r "^0.2.0") (d #t) (k 0)))) (h "1dyxvk0bsnjj18359i9v5c2g4z23vv2mc228yzb1mxkgvxpa8kg1")))

