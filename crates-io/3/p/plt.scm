(define-module (crates-io #{3}# p plt) #:use-module (crates-io))

(define-public crate-plt-0.1.0 (c (n "plt") (v "0.1.0") (d (list (d (n "draw") (r "^0.1") (d #t) (k 0) (p "plt-draw")) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m5c68k395h7nrzdq064yasv7qpazxdjl3hwqvwlq2c1g7cl9pmh") (y #t)))

(define-public crate-plt-0.2.0 (c (n "plt") (v "0.2.0") (d (list (d (n "draw") (r "^0.2.0") (d #t) (k 0) (p "plt-draw")) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kp95aqm9h6pwvjr2kiiwfd691ygwg55hnxq955i9sz66fvcm90y")))

(define-public crate-plt-0.2.1 (c (n "plt") (v "0.2.1") (d (list (d (n "draw") (r "^0.2.0") (d #t) (k 0) (p "plt-draw")) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01086n3bfya8l9rvy030kj956nk346ypj6g2fcxnkh4prxli9vcy")))

(define-public crate-plt-0.3.0 (c (n "plt") (v "0.3.0") (d (list (d (n "draw") (r "^0.3.0") (d #t) (k 0) (p "plt-draw")) (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07jz9qwdvdwjj8hcszpvc6lfnvb03j47si1d5hsg2m3fh3sb5kbk")))

(define-public crate-plt-0.3.1 (c (n "plt") (v "0.3.1") (d (list (d (n "draw") (r "^0.3.0") (d #t) (k 0) (p "plt-draw")) (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04iqr4nda8q197129aqc6s2dx42189zkq6dh5f438qyiz8fn1r8n")))

(define-public crate-plt-0.4.0 (c (n "plt") (v "0.4.0") (d (list (d (n "draw") (r "^0.4.0") (d #t) (k 0) (p "plt-draw")) (d (n "draw-cairo") (r "^0.1.0") (o #t) (k 0) (p "plt-cairo")) (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wi06f8lybjmfjln2rcfjbmv695hqy83lwjjgvfyi62zr6dxnc3m") (f (quote (("default" "cairo" "png" "svg")))) (s 2) (e (quote (("svg" "draw-cairo?/svg") ("png" "draw-cairo?/png") ("cairo" "dep:draw-cairo"))))))

(define-public crate-plt-0.4.1 (c (n "plt") (v "0.4.1") (d (list (d (n "draw") (r "^0.4.0") (d #t) (k 0) (p "plt-draw")) (d (n "draw-cairo") (r "^0.1.0") (o #t) (k 0) (p "plt-cairo")) (d (n "dyn-clone") (r "^1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g286zl4l5m46zv39r5zx56m2q5pkk3vvd64mrrxq72v8mc8pbbl") (f (quote (("default" "cairo" "png" "svg")))) (s 2) (e (quote (("svg" "draw-cairo?/svg") ("png" "draw-cairo?/png") ("cairo" "dep:draw-cairo"))))))

