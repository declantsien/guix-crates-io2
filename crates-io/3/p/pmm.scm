(define-module (crates-io #{3}# p pmm) #:use-module (crates-io))

(define-public crate-pmm-0.1.3 (c (n "pmm") (v "0.1.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "123rsg8d63cbz7wbx4gqqkbahy56sxcvswl2bvmlyb4ljdxza27b") (y #t)))

(define-public crate-pmm-0.1.4 (c (n "pmm") (v "0.1.4") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1k8g6fkqh9c0wba07z5i26im2930vhydyg93alq7gxj9zdihxiii") (y #t)))

