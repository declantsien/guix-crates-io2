(define-module (crates-io #{3}# p pvt) #:use-module (crates-io))

(define-public crate-pvt-0.1.0 (c (n "pvt") (v "0.1.0") (d (list (d (n "shape-core") (r "^0.1.14") (d #t) (k 0)))) (h "1i8ybrccccccjhwqqalf9rvn6878yzxf1kfxhl1qbdp9l09i69f1") (f (quote (("default"))))))

(define-public crate-pvt-0.0.0 (c (n "pvt") (v "0.0.0") (d (list (d (n "shape-core") (r "^0.1.14") (d #t) (k 0)))) (h "08flcy21lvkfihhwwabllzkdnjqmf0a3nf4136ihl7zz8nz31gg7") (f (quote (("default")))) (y #t)))

