(define-module (crates-io #{3}# p pug) #:use-module (crates-io))

(define-public crate-pug-0.1.0 (c (n "pug") (v "0.1.0") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1dsannrl0ci7c0ls1kz9wj3vh6659nvcq2qgncqbfb9iv6czvg4y") (y #t)))

(define-public crate-pug-0.1.1 (c (n "pug") (v "0.1.1") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1fxbfc9nxkwav27syppnbxz06x89riy87iziv1qh8c7f4ylh219g")))

(define-public crate-pug-0.1.2 (c (n "pug") (v "0.1.2") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "172h3sxx71yd0161mn5b2xiknf30qppdxarsff76422bhiv4wnmk")))

(define-public crate-pug-0.1.3 (c (n "pug") (v "0.1.3") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "11l5dgpgv4avpwljvvlhi83wkigcx9wg67bdc7mqlhicijrdjgmq")))

(define-public crate-pug-0.1.4 (c (n "pug") (v "0.1.4") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "171cani2dcljpdz7q95ycaaqkvd0ywpab786sfjjdvrpihf9w3cx")))

(define-public crate-pug-0.1.5 (c (n "pug") (v "0.1.5") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "024ggk9vw3d17sscdwba19g8z5fmgvfxdmgv9dc8rk9z1hpfsvfi")))

(define-public crate-pug-0.1.6 (c (n "pug") (v "0.1.6") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0i4acc4wf6fqdbz0hmaarnnyrkhk44aib4qax0prmvsgpbqxskfm")))

(define-public crate-pug-0.1.7 (c (n "pug") (v "0.1.7") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "01ncxf41s0b6zwag4w4zvjpgrqbcgwvacljcp2bq3m9h0bb78zzp")))

(define-public crate-pug-0.1.8 (c (n "pug") (v "0.1.8") (d (list (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0dm65br0pjwx2fgl8qavnnv2d93wqs1fsps1mvrvw98yfrjy2rhm")))

(define-public crate-pug-0.1.9 (c (n "pug") (v "0.1.9") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0sxr46rqb5cxnw15w6kjgrp5vx7z87nckx9v1y4r24vlr4amqx44")))

(define-public crate-pug-0.1.10 (c (n "pug") (v "0.1.10") (d (list (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1f766zh0xalrajnnmq3ya0k5hascrjy0qwd6pxcsnf9zyhq5bvav")))

(define-public crate-pug-0.2.0 (c (n "pug") (v "0.2.0") (d (list (d (n "pest") (r "^2.5") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0bkpwk2hck6f53h7s9n3gwjliff6sxj8r3l4s34kspjq5xk4yxif")))

