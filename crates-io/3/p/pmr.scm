(define-module (crates-io #{3}# p pmr) #:use-module (crates-io))

(define-public crate-pmr-0.1.0 (c (n "pmr") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "open") (r "^3.2.0") (d #t) (k 0)) (d (n "question") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1zrn3myq16zpzxq120yl8clhzscsmbc9qih278hcqxhsrfi1zar5")))

