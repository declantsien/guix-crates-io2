(define-module (crates-io #{3}# p pbo) #:use-module (crates-io))

(define-public crate-pbo-0.1.0 (c (n "pbo") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "03k0py87if2ljsirnvsi9skwhwc6d4zj5ss8kq1qzv2nznx4sv4d")))

(define-public crate-pbo-0.1.1 (c (n "pbo") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0shm34bbjp7rbfkl4qmqycbza1ljlgfiinrmgzp7azf4ar024sld")))

(define-public crate-pbo-0.1.2 (c (n "pbo") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "07gsfhjs8axxhpcagc6aijdwkvhn3chij5slvgiq6h2mnq40m86l")))

(define-public crate-pbo-0.1.3 (c (n "pbo") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "107qzx4cf7472hij86p5s9i0ajsqxhk1ka8h2sy762yphbq7lhrl")))

