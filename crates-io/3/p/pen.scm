(define-module (crates-io #{3}# p pen) #:use-module (crates-io))

(define-public crate-pen-0.1.0 (c (n "pen") (v "0.1.0") (d (list (d (n "formdata") (r "^0.12.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "lazycell") (r "^0.5.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.5") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "1q2li3wr2aflwdjrjfzzp9c0yrm63hyrl83cdrx7rlzkxndfapsv")))

