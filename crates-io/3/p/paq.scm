(define-module (crates-io #{3}# p paq) #:use-module (crates-io))

(define-public crate-paq-0.1.0 (c (n "paq") (v "0.1.0") (h "0y8zczdgyzafqpfwhd2msgihmj98q98smm2qni68y8wdhgc3f4kc") (y #t)))

(define-public crate-paq-0.2.0 (c (n "paq") (v "0.2.0") (d (list (d (n "rs_merkle") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "04gs5xlpcsq44gii4ng3ja4k5q7fbsxq1sw37jgnxxaai0dc9pqb") (y #t)))

(define-public crate-paq-0.3.0 (c (n "paq") (v "0.3.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "rs_merkle") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1kjfkr8i4n5ng3ra20a4ix3bcfhydv086hrf5pjv4r9an415mm84") (y #t)))

(define-public crate-paq-0.3.1 (c (n "paq") (v "0.3.1") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "rs_merkle") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0jp427hwlyijc66hngynbcz9m9hckmbjx8vzjybvcawa3xvbcf9i") (y #t)))

(define-public crate-paq-0.3.2 (c (n "paq") (v "0.3.2") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "rs_merkle") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0g7kxijjxp9fpcn4qij3j7bqrx09g4l0w4l0bsa2mj8h8jlv8gfv")))

(define-public crate-paq-0.4.0 (c (n "paq") (v "0.4.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "rs_merkle") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0n09kxl6z8wra250g391f5yw6f9nzynwviyvzzaa3alhglx96wcl")))

(define-public crate-paq-0.5.0 (c (n "paq") (v "0.5.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "rs_merkle") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "075q23xnnh3pq6viq55srhm382nrys062w2qy2vb99ji1qwzp9d6")))

(define-public crate-paq-0.5.1 (c (n "paq") (v "0.5.1") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "rs_merkle") (r "^1.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0nr5mw7c17905zywn380h8776ckmc0xxih3jm776x3ckihqjh0g4")))

(define-public crate-paq-1.0.0 (c (n "paq") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("cargo" "unstable-styles"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1irs6scgh9m8j8jy5kpyy2ayd70ywfq0paybxajhh563dr6bmsy0") (f (quote (("test-cleanup") ("default" "test-cleanup"))))))

(define-public crate-paq-1.0.1 (c (n "paq") (v "1.0.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("cargo" "unstable-styles"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1z3pyw5kv9nxcwj39dab1ccwb1pfpcic9f8qi6jy51xhqh0h8waw") (f (quote (("test-cleanup") ("default" "test-cleanup"))))))

