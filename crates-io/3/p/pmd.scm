(define-module (crates-io #{3}# p pmd) #:use-module (crates-io))

(define-public crate-pmd-0.0.1 (c (n "pmd") (v "0.0.1") (d (list (d (n "axum") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rust-embed") (r "^6.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)) (d (n "tui") (r "^0.19") (f (quote ("termion"))) (d #t) (k 0)) (d (n "tui-tree-widget") (r "^0.11.0") (d #t) (k 0)))) (h "04qz2mmkh3fnzxrk6jh5yjqsmaqrsppn8bkl0m7y224kxvmfj7v4")))

