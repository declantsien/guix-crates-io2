(define-module (crates-io #{3}# p pkt) #:use-module (crates-io))

(define-public crate-pkt-0.1.1 (c (n "pkt") (v "0.1.1") (h "07wlq2w83nskd81vcfqwxx59r6ff64hqly6jvbjgv4sssyn7v1si")))

(define-public crate-pkt-0.1.2 (c (n "pkt") (v "0.1.2") (h "0zgjz7arskcg1s2ifds06ghqfjrw8jwa1r1wjww4l9y5c3az65ni")))

(define-public crate-pkt-0.1.3 (c (n "pkt") (v "0.1.3") (h "19xfbi7mv1ldbq7dlqdbdlhg85h3b93mx5k7sr8zwiksm5gj4fsq")))

(define-public crate-pkt-0.1.4 (c (n "pkt") (v "0.1.4") (h "1dm1x9gvxczlf8k1ibg7f0fmqcaf35cwy1wlg9h5n0c80km6qqhk")))

(define-public crate-pkt-0.1.5 (c (n "pkt") (v "0.1.5") (h "0rpm1vyzkljm9m8aihk3s5zyl637gvmjcddm33cpb2jyrcfcqmzg")))

(define-public crate-pkt-0.1.6 (c (n "pkt") (v "0.1.6") (h "0fd38xgac0y90n6c542i1n2dfwvzzvz57vdrvsh98vzag3fawy65")))

(define-public crate-pkt-0.1.7 (c (n "pkt") (v "0.1.7") (h "0p3zbwbmsa4y3prcg1bb74k1x6p38rys3nkbv3nh284k6pdjbmjg")))

(define-public crate-pkt-0.1.8 (c (n "pkt") (v "0.1.8") (h "1sr23y2dm71cfvg6d5zpaiy025s073j0yky57gnqq35zgj7z9lqf")))

(define-public crate-pkt-0.1.9 (c (n "pkt") (v "0.1.9") (h "0x6bv3iq428cc6qf229250lp4lpck0j7v2aslg0mjn4spq8311ly")))

(define-public crate-pkt-0.1.10 (c (n "pkt") (v "0.1.10") (h "1f1rgis0nc0qs89jl44x829ia3mznc1xhxk2svlxdp4mkqc2j9r9")))

(define-public crate-pkt-0.1.11 (c (n "pkt") (v "0.1.11") (h "0lblghsr7hszf18cqmqvxgq0jax9hf6i8ld156xi8l647ic5gv6v")))

(define-public crate-pkt-0.2.0 (c (n "pkt") (v "0.2.0") (h "1k5jj0c7rdlv8c2nmj756pwsqy7n41sk78x39rwjs6x3f3smpg5m")))

