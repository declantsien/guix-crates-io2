(define-module (crates-io #{3}# p pri) #:use-module (crates-io))

(define-public crate-pri-0.1.0 (c (n "pri") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1xl9ylayspmsp21b6461927bs6h6356k4zszzyyvzk90wbjxxa9g")))

(define-public crate-pri-0.2.0 (c (n "pri") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0iz0pd2sw6ls8xmwyxg6lja5k7ay71grb0lwnvhfvacbs06wi0v9")))

