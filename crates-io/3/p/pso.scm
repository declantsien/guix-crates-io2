(define-module (crates-io #{3}# p pso) #:use-module (crates-io))

(define-public crate-pso-0.1.0 (c (n "pso") (v "0.1.0") (d (list (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0n7ylim6sym1w65xpbqkp8m43lq044xzk3mc1206jsyhbyw3k7jd")))

(define-public crate-pso-0.1.1 (c (n "pso") (v "0.1.1") (d (list (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "120bhydin0f4zmjr42mi1f1d2hlrh87i5l734ihrxdib446cl4j8")))

(define-public crate-pso-0.1.2 (c (n "pso") (v "0.1.2") (d (list (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "0drnq2dy9yqkdcks66kckrwg5mpymqwjmkr49yrnwbqzn8cx6das")))

(define-public crate-pso-0.1.3 (c (n "pso") (v "0.1.3") (d (list (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "02vg79m6arhw3iq2baycdg89i88x2gydga22qhdw208f3vkmmwbq")))

(define-public crate-pso-0.2.0 (c (n "pso") (v "0.2.0") (d (list (d (n "colour") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "1a3vnr413x4mdl14h2p08j8xspy3bd1bwkgcwg2i6cdlki2r9a5y")))

