(define-module (crates-io #{3}# p poi) #:use-module (crates-io))

(define-public crate-poi-0.1.0 (c (n "poi") (v "0.1.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "13igcnb6ds9sx5pzf1s5aiq8vm19dgblzzz7i47zdrzai71bzqn3")))

(define-public crate-poi-0.2.0 (c (n "poi") (v "0.2.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1axj9cxqkpvi790hpckwci0ga4p4p7r81y9qh46hqzbwyfzn2vwc")))

(define-public crate-poi-0.3.0 (c (n "poi") (v "0.3.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "0i19nb1sw37b88cq8yqy934hcm6wxdh5y3m0hfv99mz8yam1chpx")))

(define-public crate-poi-0.4.0 (c (n "poi") (v "0.4.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1ilysyfgz2vdxik56c30bp8raxn5a0j7ps82rx1g60cfirn0bs16")))

(define-public crate-poi-0.5.0 (c (n "poi") (v "0.5.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "07ng0lqw2cf9jisskyhs605szm0l8spvl2fcddkinv75fhigff38")))

(define-public crate-poi-0.6.0 (c (n "poi") (v "0.6.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "18siivchsni73s6dcpnshi8rbqq066p0lxv9g3zr1l6na78fqm5z")))

(define-public crate-poi-0.7.0 (c (n "poi") (v "0.7.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1c25aqxbp9whw7y2k7dslxczj1lwwp1pq43jvr2lgvzxjwm53h3n")))

(define-public crate-poi-0.7.2 (c (n "poi") (v "0.7.2") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1955ms8l0x60flhm3sl7wjq3zy00fpalnd00c89yly48d2xazjck")))

(define-public crate-poi-0.8.0 (c (n "poi") (v "0.8.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1qa9n8fb5rny5jzykabc5va0478a0jss23cy0anp5zqlb3klcga1")))

(define-public crate-poi-0.9.0 (c (n "poi") (v "0.9.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1zbcpdlqybv047pxp5d1y2a2rhb9bjgdzxp917126r3g6a3qnxs4")))

(define-public crate-poi-0.10.0 (c (n "poi") (v "0.10.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1w86xr7ciivg4q7v6j4mcrl18fmwzwljn0y0pa06lwpk99783b4m")))

(define-public crate-poi-0.10.1 (c (n "poi") (v "0.10.1") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1kp8kzg3psq66jxr8h4d5k9rgfgavw86alc4c3qpq49883kayasn")))

(define-public crate-poi-0.11.0 (c (n "poi") (v "0.11.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1vy17d82hg26jqc1sw5prfwvqnqypzkiqxr1hkbx8mpv8i2y1sm7")))

(define-public crate-poi-0.12.0 (c (n "poi") (v "0.12.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "0wysgk0wg6gkb9j11dl87221mzy51j7rlv64175sjb30yk4dg7zx")))

(define-public crate-poi-0.13.0 (c (n "poi") (v "0.13.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "10bv9w2fr5g9n7g0iijfk2q0bcir70l6ijhz1k2mf0yj65xpp07z")))

(define-public crate-poi-0.14.0 (c (n "poi") (v "0.14.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "06cxvgdxzsvyz76d72s36j5904cxxp7ima9vdh7w0n057kfjsrc7")))

(define-public crate-poi-0.15.0 (c (n "poi") (v "0.15.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "085990y1qq5v6h4k5d4w0rkdy6cx1amln4ks490z67m43dg0agma")))

(define-public crate-poi-0.16.0 (c (n "poi") (v "0.16.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)))) (h "1rvlnmad5mh0bis667f23f0w13fsiick4lw3p6fzwf7kx0javvar")))

(define-public crate-poi-0.17.0 (c (n "poi") (v "0.17.0") (d (list (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "11ywvjlmgx98d8n6jd0cnf6q6jxn45ymf6rnb2p97xjps0bjcd6z")))

(define-public crate-poi-0.18.0 (c (n "poi") (v "0.18.0") (d (list (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "1rjas9ij4k2lw1sicx35dpbv3plkjy7vx98cgf3yj8i0hiicvpr0")))

(define-public crate-poi-0.19.0 (c (n "poi") (v "0.19.0") (d (list (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "1zwnv4vz8akwxr46qh1h51ypm5i1cqxygym0snb8ma7zx09nj6z5")))

(define-public crate-poi-0.20.0 (c (n "poi") (v "0.20.0") (d (list (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "0995643rg9yhp4qbpm5zhcjzr4l336npch9di4nlpcxa4s71fwir")))

(define-public crate-poi-0.21.0 (c (n "poi") (v "0.21.0") (d (list (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "piston_meta") (r "^2.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "1r4ky2f3kfk8j1fhiifjpiprc6dw9v97k5ac344w1vcdsjcdmbvk")))

(define-public crate-poi-0.22.0 (c (n "poi") (v "0.22.0") (d (list (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "piston_meta") (r "^2.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "1n4h78xm3x3qkxgbmdfqw4gaii01nrkshb7xjhya9li618aps4b2")))

(define-public crate-poi-0.22.1 (c (n "poi") (v "0.22.1") (d (list (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "piston_meta") (r "^2.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "1pz76c7bnd23ry506ncbf9rb5malac6i18lkigh1914przb4gpd4")))

(define-public crate-poi-0.23.0 (c (n "poi") (v "0.23.0") (d (list (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "piston_meta") (r "^2.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "09cz78v51nln7kn16q45w5nv05l92ndv8l992rg1n31vv2rmgbi4")))

(define-public crate-poi-0.24.0 (c (n "poi") (v "0.24.0") (d (list (d (n "levenshtein") (r "^1.0.4") (d #t) (k 2)) (d (n "piston_meta") (r "^2.0.0") (d #t) (k 0)) (d (n "read_token") (r "^1.0.0") (d #t) (k 2)))) (h "116bmf19hzkp7lfd3wj9cn3i5r7crv5v7vrdah2gwqy2p3vhfggr")))

