(define-module (crates-io #{3}# p ps2) #:use-module (crates-io))

(define-public crate-ps2-0.0.0 (c (n "ps2") (v "0.0.0") (h "1vzl7y7mxcribjrzkssd6rq2z9acf1haj370l9m5gny4yjr0z406")))

(define-public crate-ps2-0.1.0 (c (n "ps2") (v "0.1.0") (d (list (d (n "bitflags") (r ">=1.2.1, <2.0.0") (d #t) (k 0)) (d (n "x86_64") (r ">=0.12.2, <0.13.0") (d #t) (k 0)))) (h "0ac00wxbcb1ccb1g6zhd9qqg9mcmxxby395vsmx833hf52nml4k0")))

(define-public crate-ps2-0.1.1 (c (n "ps2") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "x86_64") (r "^0.12.3") (d #t) (k 0)))) (h "1jzxychidd644jgv03z1z3r09iyr609slnhdi7idmwcvi1750n8k")))

(define-public crate-ps2-0.1.2 (c (n "ps2") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.3") (d #t) (k 0)))) (h "1fzgkl4bcwz5kd0l8hgsxvam6k3r5ryr49zy3n8zjhlxzmmr33xr")))

(define-public crate-ps2-0.2.0 (c (n "ps2") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.3") (d #t) (k 0)))) (h "1l57dsc71d3vl3grqcpva67qidpfw9r8rzhy44xa5rk22fxh7vyz")))

