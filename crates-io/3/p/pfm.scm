(define-module (crates-io #{3}# p pfm) #:use-module (crates-io))

(define-public crate-pfm-0.0.1 (c (n "pfm") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.9") (d #t) (k 0)))) (h "1vlx6v4i3sgnkz1pfzyhbmn96w0wx3y4fy98p9nka65c8wcw3g8x") (y #t)))

(define-public crate-pfm-0.0.2 (c (n "pfm") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.10") (d #t) (k 0)))) (h "0mi94mm1kvgj3jz0z02ij0nhjx1rmpb3n80964ixs3yi8nw0fhb1") (y #t)))

(define-public crate-pfm-0.0.3 (c (n "pfm") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.11") (d #t) (k 0)))) (h "02iw5s162pahb0pmcbd100xckdaps3kzm8isc1xny2l9by1ipdbf")))

(define-public crate-pfm-0.0.4 (c (n "pfm") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.12") (d #t) (k 0)))) (h "1d3r1x28pi1sq6s8sfacbp1f11875k1n8czcx2bksqy1mj5bvvrn")))

(define-public crate-pfm-0.0.5 (c (n "pfm") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.13") (d #t) (k 0)))) (h "1f7hgfs3zpqz9sj4dakn8rs23bsyzsi4jiw52rclh3gac30fnvz7")))

(define-public crate-pfm-0.0.6 (c (n "pfm") (v "0.0.6") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.13") (d #t) (k 0)))) (h "1zvbgrszmzzv6sxxi5h569nvqsa3ffa0pv7hh91pczrmx2jkmjsf")))

(define-public crate-pfm-0.0.7 (c (n "pfm") (v "0.0.7") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.13") (d #t) (k 0)))) (h "0vkayg19p65cs6fxgc940qdjbj5c3m2dvb7jy7vqad8dybk8m2qa")))

(define-public crate-pfm-0.0.8 (c (n "pfm") (v "0.0.8") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.13") (d #t) (k 0)))) (h "0capkzwnlaijczys88qxzh7625dchfngdqgg9p67lvigj09xnw6j")))

(define-public crate-pfm-0.0.9 (c (n "pfm") (v "0.0.9") (d (list (d (n "libc") (r "^0.2.94") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.13") (d #t) (k 0)))) (h "10qhf8fg9v1lm2qv40b9afwl992ggj4l9nvapznqcs1a6awyrqqx")))

(define-public crate-pfm-0.1.0-beta.1 (c (n "pfm") (v "0.1.0-beta.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.13") (d #t) (k 0)))) (h "1ci5yi971p9rgmwghrbdzmjlfh869q29q0pikack4352bk4hmxv8")))

(define-public crate-pfm-0.1.0-beta.2 (c (n "pfm") (v "0.1.0-beta.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.14") (d #t) (k 0)))) (h "1w8dv62mf2qrmp816csd7hnw9rjnb3w7lv3wsvcmlnig8y4z2bhh")))

(define-public crate-pfm-0.1.0-beta.3 (c (n "pfm") (v "0.1.0-beta.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.15-libpfm4.533633ad") (d #t) (k 0)))) (h "0frsgwrqk1280bvl6g9rwm4ckyrqbs6lw6ghmdz9r5zqf6bmh0fi")))

(define-public crate-pfm-0.1.0 (c (n "pfm") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^4.0") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.16-libpfm4.13.0") (d #t) (k 0)))) (h "0n173189kkdvy69z6gj4f09q5sx1g08sfw4w6vqriv5vjkmgb644")))

(define-public crate-pfm-0.1.1 (c (n "pfm") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^4.0") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.17-libpfm4.13.0") (d #t) (k 0)))) (h "0h7rljdja3nisgil8jm8gn0jimy4fwg4r394aihdsf5r9xhmjv12")))

(define-public crate-pfm-0.1.2 (c (n "pfm") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "perf-event-open-sys") (r "^4.0") (d #t) (k 0)) (d (n "pfm-sys") (r "^0.0.18") (d #t) (k 0)))) (h "0g8qkdigyzrr7y6wlgw97c5ki7dp02k310s2ngwi0aznw40zyz3a")))

