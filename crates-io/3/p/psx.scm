(define-module (crates-io #{3}# p psx) #:use-module (crates-io))

(define-public crate-psx-0.1.0 (c (n "psx") (v "0.1.0") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (k 2)))) (h "0b3z5bm169sby7kmxs1bgds17n7sl1k2qpq62xd9ia4kk0jcwi1p") (f (quote (("min_panic") ("loadable_exe") ("custom_oom") ("NA_region") ("J_region") ("EU_region")))) (y #t)))

(define-public crate-psx-0.1.1 (c (n "psx") (v "0.1.1") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 2)) (d (n "num") (r "^0.4.0") (k 2)))) (h "1ylq213clbx4x94vl0q0g7mwjm548i8axkg093ycqzmgmspbqdp2") (f (quote (("min_panic") ("loadable_exe") ("custom_oom") ("NA_region") ("J_region") ("EU_region")))) (y #t)))

(define-public crate-psx-0.1.3 (c (n "psx") (v "0.1.3") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.9.1") (f (quote ("const_mut_refs"))) (o #t) (k 0)) (d (n "num") (r "^0.4.0") (k 2)))) (h "0l93jxigyl8mqw8s5frl3gcx62xgdrr2wpbnjciwcpyh3421q71w") (f (quote (("nightlier") ("min_panic") ("loadable_exe") ("custom_oom") ("NA_region") ("J_region") ("EU_region")))) (y #t) (s 2) (e (quote (("heap" "dep:linked_list_allocator"))))))

(define-public crate-psx-0.1.2 (c (n "psx") (v "0.1.2") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.9.1") (f (quote ("const_mut_refs"))) (o #t) (k 0)) (d (n "num") (r "^0.4.0") (k 2)))) (h "00p91p6v49gkk8anpic37ysi5xhkhir2vwnr1p30ibch2qr9sa13") (f (quote (("nightlier") ("min_panic") ("loadable_exe") ("custom_oom") ("NA_region") ("J_region") ("EU_region")))) (y #t) (s 2) (e (quote (("heap" "dep:linked_list_allocator"))))))

(define-public crate-psx-0.1.4 (c (n "psx") (v "0.1.4") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.9.1") (f (quote ("const_mut_refs"))) (o #t) (k 0)) (d (n "num") (r "^0.4.0") (k 2)))) (h "007zl5mfdgqq6g19d91p63hkcnb5rdn2ahqbs73z7qhppv1rh03k") (f (quote (("nightlier") ("min_panic") ("loadable_exe") ("custom_oom") ("NA_region") ("J_region") ("EU_region")))) (y #t) (s 2) (e (quote (("heap" "dep:linked_list_allocator"))))))

(define-public crate-psx-0.1.5 (c (n "psx") (v "0.1.5") (d (list (d (n "aligned") (r "^0.4.0") (d #t) (k 2)) (d (n "const-random") (r "^0.1.13") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.9.1") (f (quote ("const_mut_refs"))) (o #t) (k 0)) (d (n "num") (r "^0.4.0") (k 2)))) (h "1bkyb0h7bqj4rnmcriyrccipnjm85bd077ac98ifzyymggd4v38z") (f (quote (("nightlier") ("min_panic") ("loadable_exe") ("custom_oom") ("NA_region") ("J_region") ("EU_region")))) (s 2) (e (quote (("heap" "dep:linked_list_allocator"))))))

(define-public crate-psx-0.1.6 (c (n "psx") (v "0.1.6") (d (list (d (n "aligned") (r "^0.4.0") (d #t) (k 2)) (d (n "const-random") (r "^0.1.13") (d #t) (k 2)) (d (n "linked_list_allocator") (r "^0.9.1") (f (quote ("const_mut_refs"))) (o #t) (k 0)) (d (n "num") (r "^0.4.0") (k 2)))) (h "161i9r05w2hc7x0fg7gmwah9y3fflcsaxa6zak6rhxq21i54y827") (f (quote (("nightlier") ("min_panic") ("loadable_exe") ("custom_oom") ("NA_region") ("J_region") ("EU_region")))) (s 2) (e (quote (("heap" "dep:linked_list_allocator"))))))

