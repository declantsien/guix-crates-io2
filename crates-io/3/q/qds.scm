(define-module (crates-io #{3}# q qds) #:use-module (crates-io))

(define-public crate-qds-0.1.0 (c (n "qds") (v "0.1.0") (h "0jnhrcl7k0aw916b2f29p04lqasscxhzfag0ak92z0i9ch0p0bq2")))

(define-public crate-qds-0.1.1 (c (n "qds") (v "0.1.1") (h "15lknjm34g975bkfirv2hzgba6616mc8dx32g1kl77909nwhwxr9")))

