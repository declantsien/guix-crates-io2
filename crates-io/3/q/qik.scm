(define-module (crates-io #{3}# q qik) #:use-module (crates-io))

(define-public crate-qik-1.0.1 (c (n "qik") (v "1.0.1") (d (list (d (n "serial") (r "^0.3.4") (d #t) (k 0)))) (h "17hgm7avl62khhzj5pdl1ynyr730iaw8fgd9d8vaj1qxwdgc5fcg")))

(define-public crate-qik-1.1.1 (c (n "qik") (v "1.1.1") (d (list (d (n "serial") (r "^0.3.4") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.4.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1qhlcjgwkr2hd6lmxkqabsknkgv1rg7dc41cfny8j8r64dibczwi")))

(define-public crate-qik-1.1.2 (c (n "qik") (v "1.1.2") (d (list (d (n "serial") (r "^0.3.4") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.4.3") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "02sq4ah6k8qmps8fjc1v6x573y5y2n728lddd529pzmcixpy1s9z")))

(define-public crate-qik-1.2.0 (c (n "qik") (v "1.2.0") (d (list (d (n "serial") (r "^0.3.4") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.4.3") (d #t) (k 0)))) (h "00yyvqa4aqhdxbjc8pdd4pnb896bd5sv45grlw8vp9d44hrfrspi") (y #t)))

(define-public crate-qik-1.2.1 (c (n "qik") (v "1.2.1") (d (list (d (n "serial") (r "^0.3.4") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.4.3") (d #t) (k 0)))) (h "1yhiy0hp84l5rfv7qvckldhpprn8vdj2a99fmk00j4bjj33idx4x")))

(define-public crate-qik-1.2.2 (c (n "qik") (v "1.2.2") (d (list (d (n "serial") (r "^0.3.4") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.4.3") (d #t) (k 0)))) (h "199rsywr6i2kcz42qkjj9y59paj261wrac43a8gspqnvkc8db3vz")))

(define-public crate-qik-1.2.3 (c (n "qik") (v "1.2.3") (d (list (d (n "serial") (r "^0.3.4") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.4.3") (d #t) (k 0)))) (h "1sw8c3mpy0sr2qsglz0npnmjhh2k1k7zxf7hjcbyz452mfk2b14z")))

