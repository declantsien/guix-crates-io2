(define-module (crates-io #{3}# q qst) #:use-module (crates-io))

(define-public crate-qst-1.0.0 (c (n "qst") (v "1.0.0") (h "16dacnsswdbm134svh3sgr3vaf6pj80xzsbhms2qag69scwp9jzv")))

(define-public crate-qst-1.0.1 (c (n "qst") (v "1.0.1") (h "1j5f48j2wzfdn266w45qqfwij8w42pa67v9qgqi6552zczgyjgqz")))

(define-public crate-qst-1.0.2 (c (n "qst") (v "1.0.2") (h "114h69g1x1xkwp2r4dxpyccn8fav9598q24rqhmpsi1vws2i8ihy")))

(define-public crate-qst-1.0.3 (c (n "qst") (v "1.0.3") (h "0qpf7ljkv0qgk1dvqpfwdzwkvs1gjwkrwzj31486zkhy10k185mh")))

