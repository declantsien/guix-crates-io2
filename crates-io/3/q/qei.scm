(define-module (crates-io #{3}# q qei) #:use-module (crates-io))

(define-public crate-qei-1.0.0 (c (n "qei") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "01gk659qg7qmhbijn5zrmc719q2vifd0jjdwgnfhmkpq14kfs0nv")))

(define-public crate-qei-1.0.1 (c (n "qei") (v "1.0.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "1m3grw1hqhnjp7jshsidmdppi0hpipwbmfphl48x6ddb73acin5i")))

(define-public crate-qei-1.0.2 (c (n "qei") (v "1.0.2") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)))) (h "0g4g4g8lh65cvcp7hph20mcg2iskdjk37pcvpg8mis07zf20yvjh")))

