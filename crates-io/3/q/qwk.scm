(define-module (crates-io #{3}# q qwk) #:use-module (crates-io))

(define-public crate-qwk-0.1.0 (c (n "qwk") (v "0.1.0") (d (list (d (n "clap") (r "^2.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quackngo") (r "^0.1") (d #t) (k 0)))) (h "05z4swj2x52yl9935d44wvm2yw7xhwg7wladcgs92h6b81lj5iik")))

(define-public crate-qwk-0.1.1 (c (n "qwk") (v "0.1.1") (d (list (d (n "clap") (r "^2.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quackngo") (r "^0.1") (d #t) (k 0)))) (h "101bpxj5qrk5sl7ha26knd6zj2344kqqaqnmpn010adaxl3l0lc5")))

(define-public crate-qwk-0.1.2 (c (n "qwk") (v "0.1.2") (d (list (d (n "clap") (r "^2.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quackngo") (r "^0.1") (d #t) (k 0)))) (h "1602v9hyw2kq149lhlfm9fbzh6k4wmg7899k5szyjwrfpsnkcbms")))

(define-public crate-qwk-0.1.3 (c (n "qwk") (v "0.1.3") (d (list (d (n "clap") (r "^2.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "quackngo") (r "^0.1") (d #t) (k 0)))) (h "0cs6xxp8k9jjsklnf8cb83273lfhc9kgvmc5438h45kg5c4gawpm")))

