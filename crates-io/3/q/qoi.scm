(define-module (crates-io #{3}# q qoi) #:use-module (crates-io))

(define-public crate-qoi-0.0.1 (c (n "qoi") (v "0.0.1") (h "1048hv1d5i9j4chcy0ffc0drdb69qadcyzl48d70gil3dk0h56x6") (y #t)))

(define-public crate-qoi-0.1.0 (c (n "qoi") (v "0.1.0") (h "1qg0sgg8r51wwsda81j5f5fxm97hi7bam92wfy5c1xw7ngpjznqc") (y #t)))

(define-public crate-qoi-0.2.0 (c (n "qoi") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0xamjbh28dy2jln70jq88k1jfkgm19jrziwj7y8yqs3n5hmgd2qy") (y #t)))

(define-public crate-qoi-0.3.0 (c (n "qoi") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "00zyrg1vk25yszjfmmawag0mpnnhif96yf499h4z0m8v4m8w7fv8") (y #t)))

(define-public crate-qoi-0.3.1 (c (n "qoi") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "15iqgmx33by3fvmi4c22nn7vl47hac5fca22qczd6b0nc9ap4k12") (y #t)))

(define-public crate-qoi-0.4.0 (c (n "qoi") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytemuck") (r "^1.7") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 2)))) (h "06bddh78ba05bfnmavqkacj4wck3k7ps34ac9h4lmzlxz8i9c991") (f (quote (("std") ("reference") ("default" "std") ("alloc")))) (r "1.51.0")))

(define-public crate-qoi-0.4.1 (c (n "qoi") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytemuck") (r "^1.12") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "walkdir") (r "^2.3") (d #t) (k 2)))) (h "00c0wkb112annn2wl72ixyd78mf56p4lxkhlmsggx65l3v3n8vbz") (f (quote (("std") ("reference") ("default" "std") ("alloc")))) (r "1.61.0")))

