(define-module (crates-io #{3}# q qoo) #:use-module (crates-io))

(define-public crate-qoo-0.1.0 (c (n "qoo") (v "0.1.0") (h "0k5hfv3dvd3qcfza4agyfrz8whcxwcjljqx4qy6k2asyyf8d5nyl")))

(define-public crate-qoo-0.1.1 (c (n "qoo") (v "0.1.1") (h "0jrx66i9isj76855m146qilc2xab68912fpkhqlimqwda6zg9wzb")))

(define-public crate-qoo-0.1.2 (c (n "qoo") (v "0.1.2") (h "0bqpnh6ymsvsyqgp870f2ga8pp3sv56f9k7nm0vw2012jwdcgx7x")))

(define-public crate-qoo-0.1.3 (c (n "qoo") (v "0.1.3") (h "1176jflqgp6jb38zsd90sm1q7d656jawcv30qa0wmzy7xwpkwifq")))

