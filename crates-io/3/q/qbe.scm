(define-module (crates-io #{3}# q qbe) #:use-module (crates-io))

(define-public crate-qbe-0.0.1 (c (n "qbe") (v "0.0.1") (h "1qkr204brsya05ld9x1d4knqb1748z33wfwhx2hh5gyqzy01mm7j")))

(define-public crate-qbe-0.1.0 (c (n "qbe") (v "0.1.0") (h "1nvi77a8zzkq35wymcrbwbn4m92kxiqq6xiq6an7jbwwckym738g")))

(define-public crate-qbe-1.0.0 (c (n "qbe") (v "1.0.0") (h "1ps7a24p26nmis0dazqv79mg1i1r0w61211i7dw44kqyy2aaigya")))

(define-public crate-qbe-2.0.0 (c (n "qbe") (v "2.0.0") (h "155kf4x0q8z97mrm3sikdcdlvk1b9kzn1j1ywj8kml57p5gr8wgd")))

(define-public crate-qbe-2.1.0 (c (n "qbe") (v "2.1.0") (h "18gj49jm5hfwc8vvyvi874s32v5aq30a3r74fy8y35k0k1vgngjc")))

