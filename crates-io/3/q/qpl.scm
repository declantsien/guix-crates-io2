(define-module (crates-io #{3}# q qpl) #:use-module (crates-io))

(define-public crate-qpl-0.1.0 (c (n "qpl") (v "0.1.0") (h "0dm7xmb81my2qx5iqiwrw8wps1x6k7v8xmflxcssvv2r9gybsk9q")))

(define-public crate-qpl-0.0.1 (c (n "qpl") (v "0.0.1") (h "12wfx2nzb9z85adfbikrbq6aiwfyqpqn3cy7r7zvhqcp3jjg8lvk")))

(define-public crate-qpl-0.0.1+0 (c (n "qpl") (v "0.0.1+0") (h "045m52w65za2hr1qk4iz4k4azklql4zb4sz68b5ch855262hhh2d")))

(define-public crate-qpl-0.0.1+1 (c (n "qpl") (v "0.0.1+1") (h "161m6a83jmkvpahgn1nff5xcpa89ssqg7nxvh4gz6kdcms2v10la")))

(define-public crate-qpl-0.0.1+2 (c (n "qpl") (v "0.0.1+2") (h "02fz9aqf45hcyxf8z311rrbz6rkngdgd7rqgy21h1660swiwmsk4")))

(define-public crate-qpl-0.0.1+3 (c (n "qpl") (v "0.0.1+3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "15nyiqx1dhb7n6jy0jyay2gmsqsc1csnx3r35j7s3w1jdlmpjr9s")))

(define-public crate-qpl-0.0.1+4 (c (n "qpl") (v "0.0.1+4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0j1rqdk07awycf2liwg2za5i1642hzigj9q2frxigzqgdqdkcyyx")))

(define-public crate-qpl-0.0.1+5 (c (n "qpl") (v "0.0.1+5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1bpclbdzd1cz2dim6cc0z63qy11yg508wcq3xy429hybs53lad6a")))

(define-public crate-qpl-0.0.1+6 (c (n "qpl") (v "0.0.1+6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1iiha2gxfqng7mlaca9is4gnrjil0qgy5vjj1c5963yxhpdmsycm")))

(define-public crate-qpl-0.0.1+7 (c (n "qpl") (v "0.0.1+7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0qlbcm8lj7ym5l7mm9y2p1nb1snp5bc4ml4xvb3gb12cc6irbd2x")))

(define-public crate-qpl-0.0.1+78 (c (n "qpl") (v "0.0.1+78") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0wbfsxc4hcg0pmk3mzr0z8rkx0620aszj2jf5ajn8i96yw2164z2")))

(define-public crate-qpl-0.0.1+8 (c (n "qpl") (v "0.0.1+8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0s1mwxl3kawgsi38g076dr3m06i9mm70m8i9jwx7ziqhjybizsng")))

(define-public crate-qpl-0.0.1+9 (c (n "qpl") (v "0.0.1+9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "12m02h6358byjqw9k5gz9kn6qk8hjnv3lmkc7ghybgmjh28859rq")))

(define-public crate-qpl-0.0.2 (c (n "qpl") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1qqh787nqm5zda6x9ygpixmrqmjfyl3im5f0sf6pcwn7j017m3kv")))

(define-public crate-qpl-0.0.3 (c (n "qpl") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11-dl") (r "^2.20.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0fkw0nxamj9firp1bgjxmx6dsx47bp8h1c7frhhs4dkr0lkkbrzj")))

(define-public crate-qpl-0.2.0 (c (n "qpl") (v "0.2.0") (d (list (d (n "ash") (r "^0.37.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11") (r "^2.20.0") (f (quote ("xlib" "glx"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1sfs51rb4aasgzgm407gwmbs968bkmnfyvlzc04drqzi0qbwlhha")))

(define-public crate-qpl-0.2.1 (c (n "qpl") (v "0.2.1") (d (list (d (n "ash") (r "^0.37.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("everything"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11") (r "^2.20.0") (f (quote ("xlib" "glx"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0smbkffjpp939ypfkkcmdfk0n44s33mzk7w66lpl2qw51pq42iyr")))

