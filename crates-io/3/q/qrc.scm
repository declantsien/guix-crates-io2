(define-module (crates-io #{3}# q qrc) #:use-module (crates-io))

(define-public crate-qrc-0.0.1 (c (n "qrc") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (f (quote ("svg"))) (k 0)))) (h "1w60kzcd63z9xk05hm27qin1hxy6fza6zx0f2wpjy62v0kv45ap7") (f (quote (("default")))) (r "1.66.1")))

(define-public crate-qrc-0.0.2 (c (n "qrc") (v "0.0.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (f (quote ("svg"))) (k 0)))) (h "1l20pviwipxqjs7hw4ifm481xjb90z1iy0ls64032w6099p6dzdq") (f (quote (("default")))) (r "1.66.1")))

(define-public crate-qrc-0.0.3 (c (n "qrc") (v "0.0.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "qrcode") (r "^0.13.0") (f (quote ("svg"))) (k 0)))) (h "0ds2vmfk6vfi5gfdiza4jmcvr7xfck3i7z9fvxbna3w1zny9nbrq") (f (quote (("default")))) (r "1.71.1")))

(define-public crate-qrc-0.0.4 (c (n "qrc") (v "0.0.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "qrcode") (r "^0.13.0") (f (quote ("svg"))) (k 0)))) (h "05wz552xlbyijyq51m4jm9hlcvmc37027siaanfirv8jjgvx4xdw") (f (quote (("default")))) (r "1.71.1")))

(define-public crate-qrc-0.0.5 (c (n "qrc") (v "0.0.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "qrcode") (r "^0.13.0") (f (quote ("svg"))) (k 0)))) (h "0kjk1ky8zjd790yqacx7h8xyyc05j6zbqgp1qbz2iylqvkk4xxmp") (f (quote (("default")))) (r "1.71.1")))

