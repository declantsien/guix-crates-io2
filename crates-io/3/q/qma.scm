(define-module (crates-io #{3}# q qma) #:use-module (crates-io))

(define-public crate-qma-0.1.0 (c (n "qma") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1s8ww1ac44q95nkc1mjy3ck2jh0865q88g5igr9xcz8xc2wfcgzn")))

(define-public crate-qma-0.1.1 (c (n "qma") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1f4r5fc6xljipkzzpygq41x0dr4h75nkmw0773838i4l0ka3cpzk")))

