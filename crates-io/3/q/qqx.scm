(define-module (crates-io #{3}# q qqx) #:use-module (crates-io))

(define-public crate-qqx-0.0.0 (c (n "qqx") (v "0.0.0") (h "1cnnnkap4kvh2zdi97smksy57cry0gwdn5f92jjd7q40m1rj7xg0")))

(define-public crate-qqx-0.0.2 (c (n "qqx") (v "0.0.2") (d (list (d (n "ctor") (r "^0.1.20") (d #t) (k 0)) (d (n "glium") (r "^0.30.1") (d #t) (k 0)) (d (n "qqx-macro") (r "^0.0.2") (d #t) (k 0)))) (h "106i2w77h15ycb25wmc9dsbccyyjxhy83aa29qf2b9gk6gz527dl")))

(define-public crate-qqx-0.0.3 (c (n "qqx") (v "0.0.3") (d (list (d (n "ctor") (r "^0.1.20") (d #t) (k 0)) (d (n "glium") (r "^0.30.1") (d #t) (k 0)) (d (n "qqx-macro") (r "^0.0.3") (d #t) (k 0)))) (h "1pn78r7v9pj3bvyabr6985na0amwv6270c3rawmw943zp0r98xih")))

