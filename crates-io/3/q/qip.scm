(define-module (crates-io #{3}# q qip) #:use-module (crates-io))

(define-public crate-qip-0.1.0 (c (n "qip") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "05f3fvjpsiyf5y3n9nb4xhacbqd255f48ynkyrhdr30pzkn9n3w2")))

(define-public crate-qip-0.1.1 (c (n "qip") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0ydv737pars2xj1axaxpalf5zf4rv1vl5p8pmxv7a62kraanvn9w")))

(define-public crate-qip-0.1.2 (c (n "qip") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "00rkrnl0w8qil6z6l0b0fag93h6nrvwp2kr3gdc1wgibpg9jz7zs")))

(define-public crate-qip-0.1.3 (c (n "qip") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "17dazzp4ldqxysdwgwiihrp102d45dcgpmd1pgyi40lc41zbhxnq")))

(define-public crate-qip-0.1.5 (c (n "qip") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1112h0632asbfiy4cd4vazd9dwlgd9x77ak9f0vw5yln8s6w19dd")))

(define-public crate-qip-0.1.6 (c (n "qip") (v "0.1.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "16g2600wrrvk1rz2yxbgw6rzvrbhr2pcmmyhajrddjpl5g6drapq")))

(define-public crate-qip-0.1.7 (c (n "qip") (v "0.1.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1g58ll5mh0q45vyhp6qzgkw089d54g0njzrqki47qf73r3wldrgp")))

(define-public crate-qip-0.1.8 (c (n "qip") (v "0.1.8") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1chf77z0viz2zq5h1zjpd5biywwsa6nv9qjpikh70hvdvv2kdc42")))

(define-public crate-qip-0.2.0 (c (n "qip") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1qbajkskacc9w35bcc3r95z852yl54gsi25rc4lhd4qhs2lc8gp2")))

(define-public crate-qip-0.2.1 (c (n "qip") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1rzkjpng98zfz1lqcc0l28r5cc94cadxp4h4y5grzsgab06h66vk")))

(define-public crate-qip-0.2.2 (c (n "qip") (v "0.2.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "015q149fvwydddklv7fzq42x4mz4lpnmybb0hhz90nisvk6x6ygy")))

(define-public crate-qip-0.2.3 (c (n "qip") (v "0.2.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "11qalq3k481zcpn8llgrr6yzwsnvfna6kl5am7r2s63q12pskhcd")))

(define-public crate-qip-0.2.4 (c (n "qip") (v "0.2.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1lxbi4cyr7z0h5mdd09v4zc4w7vmnvlly08bsahj50a19a0xbgda")))

(define-public crate-qip-0.2.5 (c (n "qip") (v "0.2.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0zb60kd21mq6n7z7cfzfv6ni3f28gi0mgjlz04vd2f817hhi9r9m")))

(define-public crate-qip-0.3.0 (c (n "qip") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1lmkg0an7qkflafgfzgy0zgvl9r7hydfh4mgxyxmcabip5z37ffh")))

(define-public crate-qip-0.3.1 (c (n "qip") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0d9zccigyn92yi9pilapam88ivb5ac6kjm6kmhd4bvlx9rpwx3dj")))

(define-public crate-qip-0.4.0 (c (n "qip") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0lgvwbmyh6fsvpg3w26xyrh60lkxmbbbydb9xm325sxh9yv82yl6")))

(define-public crate-qip-0.5.0 (c (n "qip") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1mvarr8n30md28pa5p4p89jmkskp096ajp87qmrn11gx0mkff2m1")))

(define-public crate-qip-0.6.1 (c (n "qip") (v "0.6.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1lcgrbaqd21kjgr9yll3p0xnzhrwhrvlhbzk51g3arrmvz8axmry")))

(define-public crate-qip-0.7.0 (c (n "qip") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0cmxw297z9r6pwwmlpbxwx94819cm5hwij4kzgplsycmzd2k5rdf")))

(define-public crate-qip-0.7.1 (c (n "qip") (v "0.7.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "06124zllb4x04d33b1dwh0d8svzvl6lmb4my6blpd4daidmvmxq8")))

(define-public crate-qip-0.7.2 (c (n "qip") (v "0.7.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "08gznzvrgyhbksx88kxlgyk5s0y59lhrmgcvphzxn35rmpxbzbwc")))

(define-public crate-qip-0.7.3 (c (n "qip") (v "0.7.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "18zaz4zxnpyb66f7scsclbkgrb6y0338gsh75c3hf627zh9648zi")))

(define-public crate-qip-0.7.4 (c (n "qip") (v "0.7.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0gnly0lgp3wpdg5iiil72fmsni6mvqv700fvyar75wckdszxvjxv")))

(define-public crate-qip-0.7.5 (c (n "qip") (v "0.7.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0crc6xxy5fi9wblhv8j637q0fwih1al00lhylkxglprplmfziiqa")))

(define-public crate-qip-0.7.6 (c (n "qip") (v "0.7.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "085rld6qvnf5wsyz75mfz9bmg4zagcwhvk6gad6df8gixw2hwwl6")))

(define-public crate-qip-0.7.7 (c (n "qip") (v "0.7.7") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1x29x75xqbhqbd6z1mj26jq39azh69v9wgy9rgd09xid58pi5x3f")))

(define-public crate-qip-0.8.0 (c (n "qip") (v "0.8.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1ypcr1038zkdw2502qh6bkb3xvyhhmpw3sra2n7snfjmaic0lz36")))

(define-public crate-qip-0.9.0 (c (n "qip") (v "0.9.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "08wyzr81hm7kjzb8rqimawfsxjhzs3751pcmj8cbv0j3d1g1qc57")))

(define-public crate-qip-0.9.1 (c (n "qip") (v "0.9.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1dzb8hilkp6x4131nm983814fcjihmqwm2r8nskin0g3ssw89p6r")))

(define-public crate-qip-0.9.2 (c (n "qip") (v "0.9.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0gl83i2a44wjxhvykzwyah5nlx6s4w2790pxxrx4ns4sbxglr9ap")))

(define-public crate-qip-0.9.3 (c (n "qip") (v "0.9.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "16b5jfwh3ym9lwz7fjj1wdw587y9msk3jszfylgzl9phs08rfhmy")))

(define-public crate-qip-0.10.0 (c (n "qip") (v "0.10.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0qc506aaafpba9014yr18admsjvmgyivi87h9kky0n44v3pa1mdh")))

(define-public crate-qip-0.10.1 (c (n "qip") (v "0.10.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1ig49sa0v675s1yklbfxskx9xva2ijg9mkqb4qqj96wwb3xq13md")))

(define-public crate-qip-0.10.2 (c (n "qip") (v "0.10.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1r1y1dpn30nrnid9wgzw1sc29in1cllxmb3xyq6xjf3gh3ma4f3b")))

(define-public crate-qip-0.10.3 (c (n "qip") (v "0.10.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0phbphgdj8n5qm6lqnfrvba1a791w8wqj4vpzcxmghpr69dpf05y")))

(define-public crate-qip-0.10.4 (c (n "qip") (v "0.10.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "1v23sm47n6a2hn8xk6s8qjv9pk8dbvc6yjfqg56h0k9xkk1iqjan")))

(define-public crate-qip-0.11.0 (c (n "qip") (v "0.11.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "0jsgm3fqv8g7f2a05bzs2amsg3d4g4qv84k3cp0spci9qkipr96n")))

(define-public crate-qip-0.11.2 (c (n "qip") (v "0.11.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 0)))) (h "07rvl47bxm27w6161aj2b844v7zqh4j11sc66n010w6qd2kxmrmw")))

(define-public crate-qip-0.12.0 (c (n "qip") (v "0.12.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0b4zb72zd0ibncfncnmc5g0r6pky3n9p3c9fk0ndcifjxpwgq3cy") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-qip-0.12.1 (c (n "qip") (v "0.12.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1qlbsqp18qqk8zdbnmvbrcvwrcrr1w7sgl74gqr6sl5m832hyvm5") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-qip-0.13.0 (c (n "qip") (v "0.13.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0kqljscikbvii0b59v2p7l7a0wxnsa11cxj86izpw7jq9dfa3pbg") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-qip-0.13.1 (c (n "qip") (v "0.13.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "16sgyl41dqd63f0qdb6lxjzba9n63808pzq7d2q0hcc56793r59a") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-qip-0.14.0 (c (n "qip") (v "0.14.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "0zby59hi38807jaway1r50kacfh25cnhfgjmk1ckg77xgl9l3zjm") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-qip-0.15.0 (c (n "qip") (v "0.15.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "03mv286jpwxpna9hhqcbgfpqqf3n16swwamyp2xw62bg68vhc9zk") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-qip-0.15.1 (c (n "qip") (v "0.15.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1flhaymyfv02aayhjswmpj9w66r66l32ak4462wq11yv8m6gmaw2") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-qip-0.15.2 (c (n "qip") (v "0.15.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "066v1pcjwwrh3rbfzkabkjv2a2h20fkxrgcs513m8ild4fnplb6d") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-qip-0.15.3 (c (n "qip") (v "0.15.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1zjcijy7079cq9ribg5k40iqk51m7h2b7zppx6rnkd39agp73d6r") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-qip-1.0.0-beta.0 (c (n "qip") (v "1.0.0-beta.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "qip-macros") (r "^1.0.0-beta.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "1245mpp9n7sxwwiw106kgkqz8v1dn9fa2whn3znnx3afcxdfkayj") (f (quote (("parallel" "rayon") ("optimization") ("macros" "qip-macros") ("default" "parallel") ("boolean_circuits" "macros"))))))

(define-public crate-qip-1.0.0-beta.1 (c (n "qip") (v "1.0.0-beta.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "qip-macros") (r "^1.0.0-beta.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "0k3m64660a3xnrls89fkankvgcsr8gyifvqmwn0h3kvanydam323") (f (quote (("parallel" "rayon") ("optimization") ("macros" "qip-macros") ("default" "parallel") ("boolean_circuits" "macros"))))))

(define-public crate-qip-1.0.0 (c (n "qip") (v "1.0.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "qip-macros") (r "^1.0.0-beta.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "1n5slnmr3l75h9n2n09823n108j2f7ky319nzp0bhgkqjf8j4qjm") (f (quote (("parallel" "rayon") ("macros" "qip-macros") ("default" "parallel") ("boolean_circuits" "macros"))))))

(define-public crate-qip-1.0.1 (c (n "qip") (v "1.0.1") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "qip-macros") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.6") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "104j0l3wk8pqsaqgslcba5ysavydji1cs3lfbc5wg0x72pmzz2by") (f (quote (("parallel" "rayon") ("macros" "qip-macros") ("default" "parallel") ("boolean_circuits" "macros"))))))

(define-public crate-qip-1.1.0 (c (n "qip") (v "1.1.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "qip-iterators") (r "^1.0.0") (d #t) (k 0)) (d (n "qip-macros") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "07ani3xrr2girgg7mv2a6w1xj1bn9gvsr9apn96cwsyaj188fn77") (f (quote (("parallel" "rayon" "qip-iterators/parallel") ("macros" "qip-macros") ("default" "parallel") ("boolean_circuits" "macros"))))))

(define-public crate-qip-1.2.0 (c (n "qip") (v "1.2.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "qip-iterators") (r "^1.1.0") (d #t) (k 0)) (d (n "qip-macros") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "1rnywfnkk8a2whpbg429fzjdmscwzs7s1012lwg9zxs1dpch7xqp") (f (quote (("parallel" "rayon" "qip-iterators/parallel") ("macros" "qip-macros") ("default" "parallel") ("boolean_circuits" "macros"))))))

(define-public crate-qip-1.4.0 (c (n "qip") (v "1.4.0") (d (list (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-rational") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "qip-iterators") (r "^1.4.0") (d #t) (k 0)) (d (n "qip-macros") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "0xbaxfqa300qlc146pv3yl09npay5sv2189y2almxy7mw61bsfn7") (f (quote (("parallel" "rayon" "qip-iterators/parallel") ("macros" "qip-macros") ("default" "parallel") ("boolean_circuits" "macros"))))))

