(define-module (crates-io #{3}# q qcd) #:use-module (crates-io))

(define-public crate-qcd-0.1.0 (c (n "qcd") (v "0.1.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "simple-home-dir") (r "^0.1.4") (d #t) (k 0)))) (h "0jghxipcdk7lc5mlpskh46wprbwavlw3zr3405yiagnngljicnv5")))

(define-public crate-qcd-0.1.1 (c (n "qcd") (v "0.1.1") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serial_test") (r "^2.0") (d #t) (k 2)) (d (n "simple-home-dir") (r "^0.1.4") (d #t) (k 0)))) (h "0cnn1bqppil3vy2lk67j269gswv8jdjaga1lwhv5n3ckfqvv4n1i")))

