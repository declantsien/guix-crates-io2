(define-module (crates-io #{3}# q qli) #:use-module (crates-io))

(define-public crate-qli-0.1.0 (c (n "qli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0n389xiip9cqi87girbxfsjcb60yi0myimggxndi2ax4742rsmpv")))

