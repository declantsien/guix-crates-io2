(define-module (crates-io #{3}# q qsc) #:use-module (crates-io))

(define-public crate-qsc-0.1.0 (c (n "qsc") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qscan") (r "^0.3.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "0imqb7g02bys40nk1d212kdy4xngvnls0knhgz0xngvfy0y9ddjd")))

(define-public crate-qsc-0.2.0 (c (n "qsc") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qscan") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1fbyhk2axf349gnv9xwcnyabwyplmzg19l6ckyc2c91js9hh34w5")))

(define-public crate-qsc-0.2.1 (c (n "qsc") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qscan") (r "^0.4.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "0k7ly4xvznfcd0m1q90xxf9k5ih2wdl5bva95846w3md82zjqm0k")))

(define-public crate-qsc-0.3.0 (c (n "qsc") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qscan") (r "^0.5.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "00d19mrbbj02cgc43sqv3siaqwqr03xhbjxklxm2iyi4ffpqyix6")))

(define-public crate-qsc-0.4.0 (c (n "qsc") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "qscan") (r "^0.6.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "0r30jvzirm034i7ksx175n6cjnkk2skiv68d1smwxp6g0pzrr02j")))

(define-public crate-qsc-0.4.1 (c (n "qsc") (v "0.4.1") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "debugoff") (r "^0.1.1") (f (quote ("obfuscate"))) (o #t) (d #t) (k 0)) (d (n "qscan") (r "^0.6.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1d5w22mfr3h2l9p5zwny9a568vjdlw98hha62lkrjkah601n8bn1") (f (quote (("dbgoff" "debugoff"))))))

(define-public crate-qsc-0.4.2 (c (n "qsc") (v "0.4.2") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "debugoff") (r "^0.2.1") (f (quote ("obfuscate" "syscallobf"))) (o #t) (d #t) (k 0)) (d (n "qscan") (r "^0.6.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "134yxgz0m6y1wxbw4c2cd4vmp3x21gk60nzmm4ajnpvvp80mpq0h") (f (quote (("dbgoff" "debugoff"))))))

(define-public crate-qsc-0.4.3 (c (n "qsc") (v "0.4.3") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "debugoff") (r "^0.2.2") (f (quote ("obfuscate" "syscallobf"))) (o #t) (d #t) (k 0)) (d (n "qscan") (r "^0.6.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 0)))) (h "1kh184vfhzjrla209jdkwsw5sfjhxigyjggah9hhrrngwbijph0p") (f (quote (("dbgoff" "debugoff"))))))

