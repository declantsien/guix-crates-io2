(define-module (crates-io #{3}# q qol) #:use-module (crates-io))

(define-public crate-qol-1.0.0 (c (n "qol") (v "1.0.0") (h "1ma820jhwvllf15ds6s76d5mcl1rd62i6idyrwi6y19gi21am5r7")))

(define-public crate-qol-2.0.0 (c (n "qol") (v "2.0.0") (h "0la9qzgg5dynbawfxyyydj9yxg582k0npbifyb96zqdbdwdghj76")))

