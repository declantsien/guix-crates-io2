(define-module (crates-io #{3}# q qed) #:use-module (crates-io))

(define-public crate-qed-0.0.0 (c (n "qed") (v "0.0.0") (h "05k67c448lsbj58ysn9g9bqviklgx4351lyrs964n0fik3rza9hq")))

(define-public crate-qed-1.0.0 (c (n "qed") (v "1.0.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0ndlkcpsbyq1zimn56d23va7dc70h5wir917akfj8x1cm8jdkfsg")))

(define-public crate-qed-1.1.0 (c (n "qed") (v "1.1.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1202kih084jkrxgk8qw0amn7h55k1j4ppj4xxbdmv69c6czbybdn")))

(define-public crate-qed-1.2.0 (c (n "qed") (v "1.2.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1mvayylpx1cy12kgcglzx2iavb029cib3gc4ai353xjr1jvd3vlc")))

(define-public crate-qed-1.3.0 (c (n "qed") (v "1.3.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0xyn0g0r53rn1sqkh2f5i713z7hsx0vjhkfd1zx70xkqr4f9y2wl")))

(define-public crate-qed-1.3.1 (c (n "qed") (v "1.3.1") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "10xqy4a8xxgzxjy6m8qrxnvwa2b0zjiysdybfh99x4dadhzlbrlc")))

(define-public crate-qed-1.4.0 (c (n "qed") (v "1.4.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "0zplky4x9fy2cpynfm9gqmfs15icrk4xv74l49z1w3lnjkrapl4h")))

(define-public crate-qed-1.5.0 (c (n "qed") (v "1.5.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1049i66700jjwgy323wyb4kz9qdqvsmnkxlr0adgybgb0w9q8dg7")))

(define-public crate-qed-1.6.0 (c (n "qed") (v "1.6.0") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1h5bx0mnmcmjaz1izzgp9alfpm97m8hgsrxzsak1l2ysav9dxhcz") (r "1.64.0")))

(define-public crate-qed-1.6.1 (c (n "qed") (v "1.6.1") (d (list (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "08zb8i9r61n87jnipy35nss9fwafnmjgpf258frdifdhl566nrmz") (r "1.64.0")))

