(define-module (crates-io #{3}# q qcv) #:use-module (crates-io))

(define-public crate-qcv-0.1.0 (c (n "qcv") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (f (quote ("debug-embed" "compression"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "1zvrxwa75p5fgiam4ihgrmzgdkifcnyz76cmxm1y2dk1rb51mfwm")))

(define-public crate-qcv-0.2.0 (c (n "qcv") (v "0.2.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (f (quote ("debug-embed" "compression"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "1gy09rnc20zkgzdqadnvf2m3bsvac143jnbhv6dni284a3wwhd98")))

(define-public crate-qcv-0.2.2 (c (n "qcv") (v "0.2.2") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rust-embed") (r "^5.5.1") (f (quote ("debug-embed" "compression"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.56") (d #t) (k 0)))) (h "14qzhwks2qpmc6kjaik7f8ylsahw6mkxcg6mraxj5ydyxi835fva")))

