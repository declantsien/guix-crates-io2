(define-module (crates-io #{3}# q qml) #:use-module (crates-io))

(define-public crate-qml-0.0.1 (c (n "qml") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "046r8sd5wgxmqzh0gh0vf0168c71s3l4m59x06c2wv1758nrpa4v")))

(define-public crate-qml-0.0.2 (c (n "qml") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0i9q290dkkc44hn7796f85nvnwc9jkq3s8fiy8vxxa33vh5syx84")))

(define-public crate-qml-0.0.3 (c (n "qml") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0l6nyp0b5c017bnrfggj12gjvkck0hxkmlizwrl0bfcwq3hw9p2q")))

(define-public crate-qml-0.0.4 (c (n "qml") (v "0.0.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0p8z90ij3kfvzk19xbiqlw8rqjp43lqmrvpq2nlaclnsydsp0sms")))

(define-public crate-qml-0.0.5 (c (n "qml") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1n9qp2bqjw5bjdgixrcl2hbaw24i2mma2vbha8yplvp8hiivkjnx")))

(define-public crate-qml-0.0.6 (c (n "qml") (v "0.0.6") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xx8ilvywbl8mir8k7bss6s5fq5rxfdaaqpdm69yapsxjwdvhz9k")))

(define-public crate-qml-0.0.7 (c (n "qml") (v "0.0.7") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "022p9dygjbl5dhxmzjl3cjhj6v1sh8kwqh8zwl4pi4qmmrizad8h")))

(define-public crate-qml-0.0.8 (c (n "qml") (v "0.0.8") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ynaddjw872bs9nxljkwjh88qqf86xj7d8qjgiq9jbb79lifm3rf")))

(define-public crate-qml-0.0.9 (c (n "qml") (v "0.0.9") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1inbzzd7rg5fvypl927hmy0qdc899mbjp7x5b0wmk6869ril2nsy")))

