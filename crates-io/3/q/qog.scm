(define-module (crates-io #{3}# q qog) #:use-module (crates-io))

(define-public crate-qog-0.1.0 (c (n "qog") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1iz56vv88ji9zfv0z79h6c4p072z464nph7b6s9s911z128x59xp")))

(define-public crate-qog-0.1.1 (c (n "qog") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1hv9m0mixvzvpy6d0ki07cpw7qgcdwf2i113xl3wyhpygp78gqfa")))

(define-public crate-qog-0.2.0 (c (n "qog") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0wy0zybwaxzakq2vrz6615armn4yj51j703q8dgjjp04lx0zrnwf")))

(define-public crate-qog-0.2.1 (c (n "qog") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1p3bjjby1rfkzz5mypx6ihczjjn7spvkmdylgbydb033fp76pbq5")))

(define-public crate-qog-0.3.0 (c (n "qog") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1cyiw9xjy4zbrfchxm8ymgkscyfri511j40rb814csprdy8sb4yj")))

