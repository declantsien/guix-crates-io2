(define-module (crates-io #{3}# q qit) #:use-module (crates-io))

(define-public crate-Qit-0.1.0 (c (n "Qit") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "04pv10c3c7pdb3n2wfz42czmzkphf6gja3kc13639429606sa0dc")))

(define-public crate-Qit-0.1.2 (c (n "Qit") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nsv4m5hlj57f5rj1zi29zjdpw6ycykkkkyfpcll9x5n5gpyx1mx")))

(define-public crate-Qit-0.1.3 (c (n "Qit") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0n6wrrb2bn94d4kqfn6rrkgphbs41ksbm8b78vxqqsrwnhxwf503")))

(define-public crate-Qit-0.1.4 (c (n "Qit") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11i3mrs86qvf46a8pl8j5xlh0qm1b33836b3j4bnpb525ak4w13x")))

