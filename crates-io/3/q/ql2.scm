(define-module (crates-io #{3}# q ql2) #:use-module (crates-io))

(define-public crate-ql2-1.0.0 (c (n "ql2") (v "1.0.0") (d (list (d (n "protobuf") (r "^1.0.18") (d #t) (k 0)))) (h "1ql7w9arzmgb37z42fhfav0w7hbzyidxfmrbxxjgf450n5xzl0vl")))

(define-public crate-ql2-1.0.1 (c (n "ql2") (v "1.0.1") (d (list (d (n "protobuf") (r "^1.0.18") (d #t) (k 0)))) (h "1i99m16qapxx510r9sb8i3n2gcpgwq20l2kgyp07y8d5m4m5adif")))

(define-public crate-ql2-1.1.0-alpha.1 (c (n "ql2") (v "1.1.0-alpha.1") (d (list (d (n "protobuf") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0nih9vc29m1va9s8aijgcbg4z41gnxqclxqw92y8x0b6ndxdd5wx")))

(define-public crate-ql2-1.0.2 (c (n "ql2") (v "1.0.2") (d (list (d (n "protobuf") (r "^1.1") (d #t) (k 0)))) (h "04flhmn2awlvfw3wbxbkxi9ni0s9dy87vixsqga2dvngpqnlkrg7")))

(define-public crate-ql2-1.1.0 (c (n "ql2") (v "1.1.0") (d (list (d (n "protobuf") (r "^1.1") (d #t) (k 0)))) (h "0k7xzhlc89p3bi8m0ay8cysyq1ynsdmvp0cc4lp9fx7aciwk46j9")))

(define-public crate-ql2-1.1.1 (c (n "ql2") (v "1.1.1") (d (list (d (n "protobuf") (r "^1.2") (d #t) (k 0)))) (h "055byksgajf5ijkq6lm7smhxf5jwm6f55yy5rbas0giijwjvl7cn")))

(define-public crate-ql2-1.1.2 (c (n "ql2") (v "1.1.2") (d (list (d (n "protobuf") (r ">= 1.4, < 1.6") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1vj91jpi8rgv1qnccp7nxd906li8i282iv5ai4z6iib2fl3s4ik7")))

(define-public crate-ql2-2.0.0 (c (n "ql2") (v "2.0.0") (d (list (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)))) (h "1qj87a5lbr09894z5vr3ghil835miswrs5bgrmgmd2h1xnqah785")))

(define-public crate-ql2-2.0.1 (c (n "ql2") (v "2.0.1") (d (list (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)))) (h "1jvqkm7cxb3wyplmp5z5g7iamf4clli98cmkxf7w3x0z11nc4a7l")))

(define-public crate-ql2-2.0.2 (c (n "ql2") (v "2.0.2") (d (list (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)))) (h "0nbmcv3s0irl258w7lx16k8jxafj8j719indhyah5jxxa54cx6f9")))

(define-public crate-ql2-2.0.3 (c (n "ql2") (v "2.0.3") (d (list (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)))) (h "1b953cdr2i1qc7xj1vfsi041hr7s8mn1vq4raznbxjsifxnb15zy")))

(define-public crate-ql2-2.0.4 (c (n "ql2") (v "2.0.4") (d (list (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)))) (h "0dqbk4w7xy4j33z7q5jajvphbl64c3syfws1i2675ph9qxdjg37j")))

(define-public crate-ql2-2.1.0 (c (n "ql2") (v "2.1.0") (d (list (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)))) (h "0y5aqrh1mxsaf3wvn3lmw2rqkvi72drqx6vz0p41qwpji1izabx7")))

(define-public crate-ql2-2.1.1 (c (n "ql2") (v "2.1.1") (d (list (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "066ys7paaxy70px2sfldnxdcvprcs38vlvrbnjc5b3kwmnjl0s0i")))

