(define-module (crates-io #{3}# q qrl) #:use-module (crates-io))

(define-public crate-qrl-0.1.0 (c (n "qrl") (v "0.1.0") (d (list (d (n "ash") (r "^0.37.0") (d #t) (k 0)) (d (n "gl") (r "^0.14.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.134") (d #t) (k 0)) (d (n "qmu") (r "^0.1.0") (d #t) (k 0)) (d (n "qpl") (r "^0.2.0") (d #t) (k 0)))) (h "1wbp1g1dj52mw2x5rdki6xfdwl0czba7fakqhwqp8921hgqvydzv")))

