(define-module (crates-io #{3}# q qht) #:use-module (crates-io))

(define-public crate-qht-0.1.0 (c (n "qht") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)) (d (n "rust-dense-bitset") (r "^0.1.1") (d #t) (k 0)))) (h "05fvhi8nkz08417qxiklwmywazkisr5r64iva981d0isjn3vpp81")))

