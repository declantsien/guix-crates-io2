(define-module (crates-io #{3}# m mec) #:use-module (crates-io))

(define-public crate-mec-0.1.0 (c (n "mec") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "run_shell") (r "^0.1.13") (d #t) (k 0)))) (h "0dy2ij1ba8im1g8bb0qfjs6cmshf83nnk2snabshcry7967rzkz0")))

(define-public crate-mec-0.2.0 (c (n "mec") (v "0.2.0") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jdy8wi9pwlrkk40rcx3b0xfl25jam13d6fghbxvyyim69xp9x84")))

(define-public crate-mec-0.2.1 (c (n "mec") (v "0.2.1") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0nqak29iqrklcm50jrpybqd56nl0rjnlbbps2qvhvw6qmpgyyakg")))

(define-public crate-mec-0.2.2 (c (n "mec") (v "0.2.2") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nc1pq4qhy55rd0w576n9hdvpc8b60jqss9xpi3nkg72gsq1jnpc")))

(define-public crate-mec-0.2.3 (c (n "mec") (v "0.2.3") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nqbhi60d0pakxr0717rlj3p1h0d1403j0mqf1wr14pbdqqvifi4")))

(define-public crate-mec-0.2.4 (c (n "mec") (v "0.2.4") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0n2xdq3z0lq1qr1lcar4v7yzsddjs3yq71qan4nqsyvhjm9ddzn7")))

(define-public crate-mec-0.3.0 (c (n "mec") (v "0.3.0") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1040802zsfcya4nykff5xhpw81qjwc9dkglayk7sgpdmk004vpyb")))

(define-public crate-mec-0.3.1 (c (n "mec") (v "0.3.1") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11cvjfrfah4jirksjbqi12xnk38jdd6ddirhkbydg4zhvfwbp9lc")))

(define-public crate-mec-0.3.2 (c (n "mec") (v "0.3.2") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "13lhqypzhy2db6cb5a3nsxra7bf5zalm7bhf0873hnccm89215rr")))

(define-public crate-mec-0.3.3 (c (n "mec") (v "0.3.3") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02264qv0wm7bxl1x7bp60mkv2gifx04rbaa26jrm4ra9s5m7y1ig")))

(define-public crate-mec-0.3.4 (c (n "mec") (v "0.3.4") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0x8gyhi6yi3r4v6m65n6l2m999l2yinyg6vh62yhlxh6rxdlawl2")))

(define-public crate-mec-0.3.5 (c (n "mec") (v "0.3.5") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01j3a741p2z294nn9w7y2gawjjzi6kdy4s6y50qa24k48fwcfbl2")))

(define-public crate-mec-0.3.6 (c (n "mec") (v "0.3.6") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0q9hph5b5irs12laas0bqv3x28lxw510gbzxvjx8jmclcxnz3iiw")))

(define-public crate-mec-0.4.0 (c (n "mec") (v "0.4.0") (d (list (d (n "askama") (r "^0.12.1") (d #t) (k 0)) (d (n "bpaf") (r "^0.9.11") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "083gz3qb2lb84zwkbcy65zvmrw7qmaq0pb742j814cipyjlia46l")))

