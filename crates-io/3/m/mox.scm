(define-module (crates-io #{3}# m mox) #:use-module (crates-io))

(define-public crate-mox-0.1.0 (c (n "mox") (v "0.1.0") (h "1rcgx17ds3vjmxqaq08h19qbl27s63a2f471ml0r1kf1baqzh53b")))

(define-public crate-mox-0.2.0 (c (n "mox") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^0.4.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "snax") (r "^0.3.0") (d #t) (k 0)))) (h "0ljkw9yvz3d7hknizav1fpyk6rg13s00jfaw41hlxjksq2pzy3wr")))

(define-public crate-mox-0.10.0 (c (n "mox") (v "0.10.0") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 2)) (d (n "mox-impl") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro-nested") (r "^0.1.3") (d #t) (k 0)) (d (n "topo") (r "^0.11.0") (d #t) (k 0)))) (h "0fn04wzj5vjqmyd4fndh8fa4mmwdapslqvlkwb9l3hlkkgbnxdxz")))

(define-public crate-mox-0.11.0 (c (n "mox") (v "0.11.0") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 2)) (d (n "mox-impl") (r "^0.11.0") (d #t) (k 0)) (d (n "topo") (r "^0.13.1") (d #t) (k 0)))) (h "1a441acizlwrrs1ri9cp3zasmdjl1b71hzyhrc0gmvfkyxsagz6b")))

(define-public crate-mox-0.12.0 (c (n "mox") (v "0.12.0") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)) (d (n "syn-rsx") (r "^0.8.0-beta.2") (d #t) (k 0)))) (h "0iyp7d5af40cysy0h5xwbnmfx7addjwbg1qnwykkg0sjhgz00f88")))

