(define-module (crates-io #{3}# m mci) #:use-module (crates-io))

(define-public crate-mci-0.1.0 (c (n "mci") (v "0.1.0") (d (list (d (n "bit_field") (r "~0.10") (d #t) (k 0)) (d (n "embedded-error") (r "^0.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "0rqzvg8nksrgz235qhwd66b2nb04c17sfzyv9cgcm7q9ch5946kl") (f (quote (("sdio") ("mmc") ("default" "embedded-hal/unproven"))))))

