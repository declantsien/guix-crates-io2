(define-module (crates-io #{3}# m mdn) #:use-module (crates-io))

(define-public crate-mdn-0.1.2 (c (n "mdn") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "pager") (r "^0.15.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "surf") (r "^2.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1.1") (d #t) (k 0)))) (h "1g2bfqhfjngsq60j6sb9q07q01l714nyyh2jaykbnim44w27k3j2")))

(define-public crate-mdn-0.1.3 (c (n "mdn") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "async-std") (r "^1.6.5") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "bat") (r "^0.16.0") (d #t) (k 0)) (d (n "html2text") (r "^0.2.1") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "surf") (r "^2.0.0") (d #t) (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1.1") (d #t) (k 0)))) (h "1ivs9w7p83z8lakhqv81qvzzf743z5xj9kz30lss5gnx5ddxjb00")))

