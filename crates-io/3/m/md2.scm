(define-module (crates-io #{3}# m md2) #:use-module (crates-io))

(define-public crate-md2-0.1.0 (c (n "md2") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "0q0r08xfzs520m3a7mckdx7xcgfirnzb1vvdwyk3bhmkyl6xfch9")))

(define-public crate-md2-0.1.1 (c (n "md2") (v "0.1.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "1xlmyfjj3zlq77qlc0b2ylm460ys641nggn35b3a82h8x2dc8y3b")))

(define-public crate-md2-0.1.2 (c (n "md2") (v "0.1.2") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "1014v5ids1r79ryw4dz3za9ajida93ivj55v6ppny6rqwfg13pgx")))

(define-public crate-md2-0.2.0 (c (n "md2") (v "0.2.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4.0") (d #t) (k 2)) (d (n "digest") (r "^0.5.0") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1fzy5hprbnln7g3hvjsn0i4w2qwkr65a6hwilk40j7qaikwn3bj7")))

(define-public crate-md2-0.2.1 (c (n "md2") (v "0.2.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4") (d #t) (k 2)) (d (n "digest") (r "^0.5") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "17wr7cb9d5rvyidq5wlaqxpjdyaf55n6zcq94c3n0ly50hriyaff")))

(define-public crate-md2-0.3.0 (c (n "md2") (v "0.3.0") (d (list (d (n "block-buffer") (r "^0.2") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "15ql1chnhqjn9f83y5hkpmnxalf3585rgn2yzld8zlkc5fnrxpg2")))

(define-public crate-md2-0.7.0 (c (n "md2") (v "0.7.0") (d (list (d (n "block-buffer") (r "^0.3") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)))) (h "0gimzxzr2gy8bv2zzpnlf2h8qq29aracnw90mj7lvwqfiv261b1d")))

(define-public crate-md2-0.8.0 (c (n "md2") (v "0.8.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1s8n46969ka7m6g7pwq1rb1rylz4qalx23i80pagy18bbx5vfc7i") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-md2-0.9.0 (c (n "md2") (v "0.9.0") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0y2zv5w0fx4mb2nj2020jxfwmqlkkx22p5hx8n4gzsxd2fv3plmz") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-md2-0.10.0 (c (n "md2") (v "0.10.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1fxclpf49mcrdnx0kqq3dhqcbybw7705p5s472gzi5s1ga0x10kr") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-md2-0.10.1 (c (n "md2") (v "0.10.1") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "00s2wvnsvg3698japilfn145dfv91ln2jnywxwjbvcgd6gas4j48") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-md2-0.10.2 (c (n "md2") (v "0.10.2") (d (list (d (n "digest") (r "^0.10.4") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "07rxqh3hskzygpv4r20wcq1c5vq0z698sa02s7cgix2zs8z0ykqz") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std"))))))

