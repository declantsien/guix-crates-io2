(define-module (crates-io #{3}# m mbn) #:use-module (crates-io))

(define-public crate-mbn-0.1.0 (c (n "mbn") (v "0.1.0") (d (list (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "0phxyx6ayrbz0l2waarxl4b7vqaz2048z7blkjcwwvrykpqq2hag")))

(define-public crate-mbn-0.2.0 (c (n "mbn") (v "0.2.0") (d (list (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "19010h6sb33r1spxw03z7f8pjj7as1qhwkix77ahm33bmq1ww843")))

(define-public crate-mbn-0.3.0 (c (n "mbn") (v "0.3.0") (d (list (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "0kixg1qysgs3va67cncr7yvxqpyr8jchhairkxv1f28brd9kr323")))

(define-public crate-mbn-0.3.1 (c (n "mbn") (v "0.3.1") (d (list (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "11cks944yz9nj64wk800ygqv73p4mfvndd1d1jkm7z90h3vs591j")))

(define-public crate-mbn-0.3.2 (c (n "mbn") (v "0.3.2") (d (list (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "1vyf6jwpbi8pz8rw80l4f0brby7q501ynxj0rc3vdxrcjbq9k1j4")))

(define-public crate-mbn-1.0.0 (c (n "mbn") (v "1.0.0") (d (list (d (n "elf") (r "^0.7") (d #t) (k 0)))) (h "0625zhiy79gzg1z0jj60dplm9awvaq4ldfax8n691x57hix525pv")))

