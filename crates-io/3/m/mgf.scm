(define-module (crates-io #{3}# m mgf) #:use-module (crates-io))

(define-public crate-mgf-0.1.0 (c (n "mgf") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)))) (h "029w3y93a9rkm1pmmn3pgnqcl00x8a9chp9zdjcf3597lgf574a1")))

(define-public crate-mgf-0.2.0 (c (n "mgf") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)))) (h "1l37flyp7hzyi6qlhzbfbrpyr3034zik06cr3qsnk3zkfzgy1jpg")))

(define-public crate-mgf-0.2.1 (c (n "mgf") (v "0.2.1") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)))) (h "1kg06v3lkngiib3gw435k1n5qil03vyj1wyx4688jgw2kqv4pnaa")))

(define-public crate-mgf-0.2.2 (c (n "mgf") (v "0.2.2") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)))) (h "1sjd2mq2xbz6vsq6c3afr4wwlbx52mxina698g9l13r5d6zdc7zb")))

(define-public crate-mgf-0.3.0 (c (n "mgf") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)))) (h "1dqfpvky2yh577m9abb27vggg4n245yvg7700xbsxmvz3dw8agzb")))

(define-public crate-mgf-0.3.1 (c (n "mgf") (v "0.3.1") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)))) (h "16zz3y6v7l813g5737v6haxmab3bsdg3z0gyc7z1wagn5bdm59nc")))

(define-public crate-mgf-0.3.2 (c (n "mgf") (v "0.3.2") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)))) (h "1l61ssg5fgr8nfgp0arss9j02d8yhyxfn347k75yzpvbbb2js5hw")))

(define-public crate-mgf-1.0.0 (c (n "mgf") (v "1.0.0") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)))) (h "1h4wr69sdpdsjr48gpjl36v3v2rnxx80qa18vbafda3x4zk5d5x1")))

(define-public crate-mgf-1.1.0 (c (n "mgf") (v "1.1.0") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)))) (h "08nd62ryrjydh8kxbjmzf8m17s4fl8r5bdwpd348raf8mv0w9ybp") (y #t)))

(define-public crate-mgf-1.1.1 (c (n "mgf") (v "1.1.1") (d (list (d (n "cgmath") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.5.0") (d #t) (k 0)))) (h "1gnr3rb5c3wd1ramhf9qwvqc7i7v8ychfga4rg0jacvmlrgx74jb")))

(define-public crate-mgf-1.2.0 (c (n "mgf") (v "1.2.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0sn17gcnr30swadsfqrrg8qf6mz8arlgnga292bm1m8dr0xhlhij")))

(define-public crate-mgf-1.2.1 (c (n "mgf") (v "1.2.1") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0mcgmiixvxp2w4vymvpbxmgqig449ckwh1f2rfmqnwaj93xjncjs")))

(define-public crate-mgf-1.2.2 (c (n "mgf") (v "1.2.2") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1k7ca661c5348w9x2zkys00l2gchm2fzrxd5p5i0sjhjgpskwq9s")))

(define-public crate-mgf-1.2.3 (c (n "mgf") (v "1.2.3") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0c8x81dqrag12kxz9nvwc1b2qn8ycy3a7yyd62mhhcbal7dwwwqb")))

(define-public crate-mgf-1.2.4 (c (n "mgf") (v "1.2.4") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "cgmath") (r "^0.17") (f (quote ("serde"))) (d #t) (t "cfg(feature = \"serde\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1582m2i1c5n6c52wrnhqcyvylylny2ny1x2bnrsvagwwiy4dqw0g")))

(define-public crate-mgf-1.2.5 (c (n "mgf") (v "1.2.5") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "cgmath") (r "^0.17") (f (quote ("serde"))) (d #t) (t "cfg(feature = \"serde\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "10dwgwrrdk96p93vsnx7kiv6qmq2k7131cz83bxakrg8n1gj10md")))

(define-public crate-mgf-1.3.0 (c (n "mgf") (v "1.3.0") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "cgmath") (r "^0.17") (f (quote ("serde"))) (d #t) (t "cfg(feature = \"serde\")") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "11pckscjip97zxwa0md62kqss52k0x5q256p4l7m0512022qk8ai")))

(define-public crate-mgf-1.3.2 (c (n "mgf") (v "1.3.2") (d (list (d (n "cgmath") (r "^0.17") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0) (p "serde")) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1glganwnls0lng4y40fglzyd9a19z3k5lvjw0bhl8jbnyq17lis3") (f (quote (("serde" "serde_crate" "cgmath/serde"))))))

(define-public crate-mgf-1.4.0 (c (n "mgf") (v "1.4.0") (d (list (d (n "cgmath") (r "^0.17") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0mqw1sdgbq8b27vbwq7dmwblq2a3dw7nqx8yfgkx56p8ld41vflr")))

