(define-module (crates-io #{3}# m mpw) #:use-module (crates-io))

(define-public crate-mpw-0.1.0 (c (n "mpw") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1bl2l59qi5bszrcszj3vr430iflh41qmnmiawlcj11ljv187wlaf")))

(define-public crate-mpw-0.2.0 (c (n "mpw") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.4.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sha2") (r "^0.6.0") (d #t) (k 0)))) (h "0n21gyss6l0slps3g9ysw4w7g2h073whsd1mx56r60n168i1r6g8")))

(define-public crate-mpw-0.2.1 (c (n "mpw") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.4.2") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "sha2") (r "^0.6.0") (d #t) (k 0)))) (h "1iw4jf1r6k6j9yb7ysnsfldj7lw586gcnbr1yqliv6w7cv35ah6i")))

