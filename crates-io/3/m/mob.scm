(define-module (crates-io #{3}# m mob) #:use-module (crates-io))

(define-public crate-mob-0.1.1 (c (n "mob") (v "0.1.1") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)))) (h "09h00yrihk7crmq93rlz38iwpnbg674bnyhhrwcy9bm4z003sr1y")))

(define-public crate-mob-0.1.2 (c (n "mob") (v "0.1.2") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)))) (h "1iz39kryjdw3gvgazryb36dlh5y93f86vf7a61s4765a1wijf63b")))

(define-public crate-mob-0.1.3 (c (n "mob") (v "0.1.3") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)))) (h "1g7nk79mpgmzyk334z8vcfm4pmlajzzb544910sck99lmddls5xy")))

(define-public crate-mob-0.4.0 (c (n "mob") (v "0.4.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "mob_server") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)))) (h "04x2djnk4x54qx4fqqkfdxkffy7kcp5fg3i59w4cyxa4imag9886")))

(define-public crate-mob-0.4.1 (c (n "mob") (v "0.4.1") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "mob_server") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)))) (h "00scif1126z07scn6y12kn8b3rr7mh1z24zpvczd3bjzg2bqs591")))

(define-public crate-mob-0.4.3 (c (n "mob") (v "0.4.3") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "mob_server") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "termion") (r "^1.3") (d #t) (k 0)))) (h "09h0a3dsrhls5hw0wj3fnqzc0p119nb36ndia148k7cr2s2lbv54")))

