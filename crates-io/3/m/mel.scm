(define-module (crates-io #{3}# m mel) #:use-module (crates-io))

(define-public crate-mel-0.1.0 (c (n "mel") (v "0.1.0") (d (list (d (n "apodize") (r "^0.2.0") (d #t) (k 0)) (d (n "hertz") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.4.10") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.3.1") (f (quote ("rblas"))) (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "rblas") (r "^0.0.13") (d #t) (k 0)))) (h "1939gz70fdqv6ywdn4pid94z3mgbbq10nhv5xnj7frhdlys1ah5p")))

(define-public crate-mel-0.2.0 (c (n "mel") (v "0.2.0") (d (list (d (n "apodize") (r "^0.2.0") (d #t) (k 0)) (d (n "hertz") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.4.10") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.3.1") (f (quote ("rblas"))) (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "rblas") (r "^0.0.13") (d #t) (k 0)))) (h "00gkzy4raq8wbxpddpgw7580gl0fklrwd7byncw6zwgr3vgcqqwj")))

(define-public crate-mel-0.2.1 (c (n "mel") (v "0.2.1") (d (list (d (n "apodize") (r "^0.2.0") (d #t) (k 0)) (d (n "hertz") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.4.10") (d #t) (k 0)) (d (n "nalgebra") (r "^0.5.1") (d #t) (k 0)) (d (n "ndarray") (r "^0.3.1") (f (quote ("rblas"))) (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "rblas") (r "^0.0.13") (d #t) (k 0)))) (h "0wjrr16qyrrw5a1sddxcafdpnxphzgyigaflnsimnvk1gk2072nx")))

(define-public crate-mel-0.3.0 (c (n "mel") (v "0.3.0") (d (list (d (n "apodize") (r "^0.2.0") (d #t) (k 0)) (d (n "approx") (r "^0.1.1") (d #t) (k 2)) (d (n "hertz") (r "^0.2.0") (d #t) (k 0)) (d (n "itertools-num") (r "^0.1.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13.0") (d #t) (k 2)) (d (n "num") (r "^0.1.40") (d #t) (k 0)))) (h "08w2zz6812xf1bn8gvv9q2i4fz9wbyvisrj6ykn34pxhh8nl4rbk")))

