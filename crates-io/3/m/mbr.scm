(define-module (crates-io #{3}# m mbr) #:use-module (crates-io))

(define-public crate-mbr-0.1.0 (c (n "mbr") (v "0.1.0") (h "1hyiawghxnhb650q1pg2lghk3bxpsk1m2bmwr2zazs1hlsrzi61y")))

(define-public crate-mbr-0.2.0 (c (n "mbr") (v "0.2.0") (h "07c0bdy1a20cl76bmaiyafmqrc5lra75n54kcva8dhskpgn2sw4j")))

(define-public crate-mbr-0.2.1 (c (n "mbr") (v "0.2.1") (h "1ap5l02qs609dz4g50301x2bmvmyym64ygayc0wmpjvd7nhg2hnl")))

(define-public crate-mbr-0.2.2 (c (n "mbr") (v "0.2.2") (h "0cgj6cm2x2y41vmdkvyvvay9ac4d1m2qiaggf7mriziidkk6g5hs")))

