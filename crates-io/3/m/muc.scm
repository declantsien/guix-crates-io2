(define-module (crates-io #{3}# m muc) #:use-module (crates-io))

(define-public crate-muc-0.1.0 (c (n "muc") (v "0.1.0") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1zdr1qffxmrlqaghzmrg5d6czpnghfq3hnlzknfhnmlz9195x8nf")))

(define-public crate-muc-0.2.0 (c (n "muc") (v "0.2.0") (d (list (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "0pss8j1hg90cfyw23cnh95q4v9j42fa623x6vhz3an3fwhkvyvgp")))

(define-public crate-muc-0.2.1 (c (n "muc") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "net2") (r "^0.2") (d #t) (k 0)))) (h "1lc9yskgwmdav5apwq967n69jrsd4yyk20g01s7z6zrrkij0npzc")))

