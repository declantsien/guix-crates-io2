(define-module (crates-io #{3}# m mdb) #:use-module (crates-io))

(define-public crate-mdb-0.0.0 (c (n "mdb") (v "0.0.0") (h "0w9dcbf1dgzfkvg52nxa5v44kgli1yb3v56g7nv0j76lw4msbl3h")))

(define-public crate-mdb-0.0.1 (c (n "mdb") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "memmap2") (r "^0.3.1") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.4.4") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rkyv") (r "^0.7") (f (quote ("default" "tinyvec_alloc"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tinyvec") (r "^1.3.1") (f (quote ("alloc" "serde"))) (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lv6b8irvxzbgax76pmx1cgjbshz46vcps9xlmbndnqc40gk631w")))

