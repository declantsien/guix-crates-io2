(define-module (crates-io #{3}# m mtk) #:use-module (crates-io))

(define-public crate-mtk-0.1.0 (c (n "mtk") (v "0.1.0") (d (list (d (n "rsap") (r "^0.3.0") (d #t) (k 0)))) (h "1aqj50zf6drd4vv8vr9wdghw7sy3jraq22n050gshg8mbd1n1p7d") (y #t)))

(define-public crate-mtk-0.1.1 (c (n "mtk") (v "0.1.1") (h "1rl2dvxssqalix4n3g65vv0jh1x45jf7dvhyzz7ipndw9h1ci6pa") (y #t)))

(define-public crate-mtk-0.1.2 (c (n "mtk") (v "0.1.2") (h "0cgg452671zyj1vzvfd0f9k7rrqj6jj7ch5j4nk114g44qkk5fyn") (y #t)))

(define-public crate-mtk-0.1.3 (c (n "mtk") (v "0.1.3") (d (list (d (n "msgbox") (r "^0.6.1") (d #t) (k 0)))) (h "0131paxmyw1lj0n6niliq3sxq5svxkmccq2df6wndb8fjg1ins6n") (y #t)))

(define-public crate-mtk-0.1.4 (c (n "mtk") (v "0.1.4") (h "0bpw0sbm79fz3nd4vfdrsbkcq1fsr2w1w4xg8dlq2p3d73ayns2q") (y #t)))

(define-public crate-mtk-0.1.5 (c (n "mtk") (v "0.1.5") (h "17xmmlg4nmihmj647vcz504i4xn7l4ay82pb4pimw2as71g9221h") (y #t)))

(define-public crate-mtk-0.1.6 (c (n "mtk") (v "0.1.6") (h "07sgqzj7pcx2zfr8n8gbqmm8gpcyhp3g9wzzhwxvjr8z264ylafc") (y #t)))

(define-public crate-mtk-0.1.7 (c (n "mtk") (v "0.1.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1bmh0frvz1xk8qff8r290dvdx8529b908b6556jii2w2dyz9ihrp") (y #t)))

(define-public crate-mtk-0.1.8 (c (n "mtk") (v "0.1.8") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "01bf19sk76cxyl19z8qq1i76qrhnmlj2ql7pgg4jls3arl8vywd7") (y #t)))

(define-public crate-mtk-0.1.9 (c (n "mtk") (v "0.1.9") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1sghawf0yl12l4c7xpnriblv7swwdw7vyjms5732q54qf6ilxi09") (y #t)))

(define-public crate-mtk-0.2.0 (c (n "mtk") (v "0.2.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0xnbk927szzvihzidjhjg83a7s3c0hr3j7whv84585f9gb9y3by9") (y #t)))

(define-public crate-mtk-0.2.1 (c (n "mtk") (v "0.2.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1bi4vn8hxl4xsnzxqx95gwxwz9cnw8gcikm9dqx0hz5h4yglf3x6") (y #t)))

(define-public crate-mtk-0.2.2 (c (n "mtk") (v "0.2.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1cphfwnmibkvqfjk3akas5k21m1zq2zjc42zjzvmv7mwhf3dszz8") (y #t)))

(define-public crate-mtk-0.2.3 (c (n "mtk") (v "0.2.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1gvxjy6glax2lhr3knnpvp791zd58akd0mizlc38ag6ckmwvmr9s") (y #t)))

(define-public crate-mtk-0.2.4 (c (n "mtk") (v "0.2.4") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1ah5ydyyqypk326iak10a184a0y77xcwjhi1538m8kcyz9c8r27q") (y #t)))

(define-public crate-mtk-0.2.5 (c (n "mtk") (v "0.2.5") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1ah3lfbiiqlqgy9cpwsfsq37d9ssyaiwnbwdkl4rywa4wr3dfxq9") (y #t)))

(define-public crate-mtk-0.2.6 (c (n "mtk") (v "0.2.6") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ethnum") (r "^1.0.3") (d #t) (k 0)))) (h "0d4vn2g96i5gsvxbgqmhmp8l98dfn22yjvsl04qql7k5655m4qbr") (y #t)))

(define-public crate-mtk-0.2.7 (c (n "mtk") (v "0.2.7") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ethnum") (r "^1.0.3") (d #t) (k 0)))) (h "1fpsgsa3np3lhl59a8rp5bqvs7i3jvc8a725fzm3klpjc5wscmpg") (y #t)))

(define-public crate-mtk-0.2.8 (c (n "mtk") (v "0.2.8") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1f9z6dmhxi14zqq4l54413z00k2bkn7sbx7yha71qjjqwvxwhhvg") (y #t)))

(define-public crate-mtk-0.2.9 (c (n "mtk") (v "0.2.9") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "099zq89n7n8xzya9n0bdxlly47h3a0bcjgvm8gwzz7ynn7y873sn") (y #t)))

(define-public crate-mtk-0.3.0 (c (n "mtk") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0y0389jgy5hkqxmqpj1scjgindcrkan7spdb20lk7qnbb5i9h1pp") (y #t)))

(define-public crate-mtk-0.3.1 (c (n "mtk") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1mbjg5083q5hb1xr1il8nb3p0i1j0mw6sm0yzwldhzy5qkqvj13q") (y #t)))

(define-public crate-mtk-0.3.2 (c (n "mtk") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0hfibgyp8ybz7b8p8gxgii78f09k7qqqmbsphpymswyc1pr4i39q") (y #t)))

(define-public crate-mtk-0.3.3 (c (n "mtk") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "png") (r "^0.17.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0w1hy76bl9qqhgrwn4q806ph6ajfyx1dvg6fg4sflvz5x4fvv0xk") (y #t)))

(define-public crate-mtk-0.3.4 (c (n "mtk") (v "0.3.4") (h "1b7fwnqljnx77fwni0fj0rd6san090hlpf96ih8zzw24azhdwi4i") (y #t)))

(define-public crate-mtk-0.3.5 (c (n "mtk") (v "0.3.5") (h "1bwxim12h89w6ps99mqrv6brmzxsk5yzxdmpwlyy5qvfhgr9sjl6") (y #t)))

