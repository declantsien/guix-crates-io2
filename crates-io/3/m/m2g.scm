(define-module (crates-io #{3}# m m2g) #:use-module (crates-io))

(define-public crate-m2g-0.1.0 (c (n "m2g") (v "0.1.0") (d (list (d (n "roxmltree") (r "^0.15.1") (d #t) (k 0)))) (h "0cnkmf9fml8ipgm8k55x1w6l3jabgpp1fj65p9cb4x8xacq346dy")))

(define-public crate-m2g-0.1.1 (c (n "m2g") (v "0.1.1") (d (list (d (n "roxmltree") (r "^0.15.1") (d #t) (k 0)))) (h "0y0nmf56apbis6bd66k4xhmv5ysimpp4il7anjg5icd4ha4bx8db")))

