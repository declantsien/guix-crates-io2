(define-module (crates-io #{3}# m moq) #:use-module (crates-io))

(define-public crate-moq-0.1.0 (c (n "moq") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "moq_derive") (r "=0.1.0") (d #t) (k 0)))) (h "09pqgg3ir11inpxwwc9j27h0688rl0kwaqzvzfs8yhdskwvzcfb7")))

(define-public crate-moq-0.2.0 (c (n "moq") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "moq_derive") (r "=0.2.0") (d #t) (k 0)))) (h "06zcp132mbi67cv9y7djc6sdzx6sni07k7la6kgbmscv2xnzcsdi")))

(define-public crate-moq-0.3.0 (c (n "moq") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "moq_derive") (r "=0.3.0") (d #t) (k 0)) (d (n "moq_lambda") (r "=0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1241262bml1vl2q14nmg4qqz4xszzq6r91fnd76wnb6rslr9x2ar")))

(define-public crate-moq-0.3.1 (c (n "moq") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "moq_derive") (r "=0.3.1") (d #t) (k 0)) (d (n "moq_lambda") (r "=0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0mjblrh7sxsmkk9dvzg23ij3qrm9n0571l3f8j9xjhgic7rm03jl")))

(define-public crate-moq-0.4.0 (c (n "moq") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "moq_derive") (r "=0.3.1") (d #t) (k 0)) (d (n "moq_lambda") (r "=0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "11pmqqwv7lisa3xvxmhapyj8m053lkb40kz8brrmlzjl6i9ib6bl")))

