(define-module (crates-io #{3}# m mck) #:use-module (crates-io))

(define-public crate-mck-0.1.0-alpha.1 (c (n "mck") (v "0.1.0-alpha.1") (d (list (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "0zvrnmp74n3x7bc815rm1p30vqkw0nj3wxw1sg5naiif4nq663fi") (r "1.70")))

(define-public crate-mck-0.1.0-alpha.2 (c (n "mck") (v "0.1.0-alpha.2") (d (list (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "1rgpr0dwv8lb880mkhqazsjm4wpn1f2mn048p88khgbkinfd7lqv") (r "1.70")))

(define-public crate-mck-0.1.0-alpha.3 (c (n "mck") (v "0.1.0-alpha.3") (d (list (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "04da3ghhyy1ffc13llv9z80732sgxb1ccjx0ic25z2725qkb27nb") (r "1.70")))

(define-public crate-mck-0.1.0 (c (n "mck") (v "0.1.0") (d (list (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "17daxpzignz3g03n562qxg6fa7jrd1jcyvbwsdh0pcbzmzlw1nvx") (r "1.70")))

(define-public crate-mck-0.2.0-alpha.1 (c (n "mck") (v "0.2.0-alpha.1") (d (list (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "1kfisz4avs4ps78ksldvicjrf7byrfjr2ppcfw5jpbvnl63764d4") (r "1.75")))

(define-public crate-mck-0.2.0-alpha.2 (c (n "mck") (v "0.2.0-alpha.2") (d (list (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "1q18z8hsmngpzl6nz7nz7i0n9xrxibls37c9hz98z56mfsw9b0sq") (r "1.75")))

(define-public crate-mck-0.2.0 (c (n "mck") (v "0.2.0") (d (list (d (n "seq-macro") (r "^0.3.5") (d #t) (k 0)))) (h "1ix3qbr53m6s6c9z7byg997p7bqj7xyagihk0zyzw3607lhklwm9") (r "1.75")))

