(define-module (crates-io #{3}# m mrq) #:use-module (crates-io))

(define-public crate-mrq-0.1.0 (c (n "mrq") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "rustls") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "tiny_http") (r "^0.6") (d #t) (k 2)) (d (n "webpki") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.16") (o #t) (d #t) (k 0)))) (h "1swcd7i2yap9g8m0yqh8wrcsyr2mx9dsvcqg9hli86svxl5hhmpq") (f (quote (("https" "rustls" "webpki-roots" "webpki") ("default" "https"))))))

