(define-module (crates-io #{3}# m mvn) #:use-module (crates-io))

(define-public crate-mvn-0.1.0 (c (n "mvn") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pubgrub") (r "^0.2.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23.0-alpha2") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p8y4hcgv2rp7gspnyz2ps3km1zbznfjhwnvisdc0bg01sklahhm")))

(define-public crate-mvn-0.2.0 (c (n "mvn") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pubgrub") (r "^0.2.1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xcommon") (r "^0.3.0") (d #t) (k 0)))) (h "0lbqpg4mlsfvgw96lrqhq1yxbxxq2mswl5w2dq0w0r04gk4ib802")))

