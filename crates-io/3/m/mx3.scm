(define-module (crates-io #{3}# m mx3) #:use-module (crates-io))

(define-public crate-mx3-0.1.0 (c (n "mx3") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1xjpkl7pc49vcl16jbyrpx8l039zhw5g775shmd27sjah8y4g2kz")))

(define-public crate-mx3-0.2.0 (c (n "mx3") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1hbdq2j9xyp1zkccn1a2qq3i787gbvrlibrh113g1pxq4lbkm5p9")))

(define-public crate-mx3-1.0.0 (c (n "mx3") (v "1.0.0") (d (list (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0w1njrmrqkffhfia8b6dv8ix0ywj1nvigvy4g8sk18l0vzq2m2fw") (f (quote (("hasher") ("default"))))))

(define-public crate-mx3-1.0.1 (c (n "mx3") (v "1.0.1") (d (list (d (n "rand") (r "^0.8") (k 2)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "06gn034kigrhjl2grfza4hmkmi83s6002xkmmrxxxv6pqqnbz09y") (f (quote (("hasher") ("default"))))))

