(define-module (crates-io #{3}# m mmo) #:use-module (crates-io))

(define-public crate-mmo-0.0.1-pre (c (n "mmo") (v "0.0.1-pre") (d (list (d (n "actix-web") (r "^4.0.0-beta.8") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0p97zmidqymijb6vl34dxh2ylp2khw7ikdjl5gz195pgqcc1czir")))

(define-public crate-mmo-0.0.2 (c (n "mmo") (v "0.0.2") (d (list (d (n "mmo-core") (r "^0.0.2") (d #t) (k 0)))) (h "1hap6z5rgj2nw1wza7df9snhrffaixgzpv3a49cr2nbgaci8zgbi")))

