(define-module (crates-io #{3}# m mcp) #:use-module (crates-io))

(define-public crate-mcp-0.1.0 (c (n "mcp") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)))) (h "1h186mjlpzzrh5z99jygjd9l93f8rjyasiv1043465d39gvqlwbb")))

(define-public crate-mcp-0.1.1 (c (n "mcp") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.1") (d #t) (k 0)))) (h "1y3823ddjxlbd1lb9sbimgwk3nzxp7dmi3vl822a1wbxxsfxmyzc")))

