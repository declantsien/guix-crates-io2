(define-module (crates-io #{3}# m mlg) #:use-module (crates-io))

(define-public crate-mlg-0.1.0 (c (n "mlg") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xlfyq6m5wjgn0sz40l32ri2z57lwhzfnppx7mhf3yzdjpzxs383")))

(define-public crate-mlg-0.1.1 (c (n "mlg") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.74") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.29") (d #t) (k 2)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02m9fa3q9x3z7zvzpb0d8aiq00vm79ld5h68lh2p5n3giff5p1pr")))

