(define-module (crates-io #{3}# m mrg) #:use-module (crates-io))

(define-public crate-mrg-0.1.0 (c (n "mrg") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "endianness") (r "^0.2.0") (d #t) (k 0)))) (h "0w7f064w64ddladwibg7ybdpn2f3kfhk37wgahvrh93fybm17rvk")))

(define-public crate-mrg-0.2.0 (c (n "mrg") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1781r8irbz7ggh1av6wy3wxrcdfaff7pk5bkzzrlb7lxs4i08cb3")))

(define-public crate-mrg-0.2.1 (c (n "mrg") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)))) (h "0ywil0fbgnj7w0j7jphk1kxxz6nsp3gzphbyxcg5rmc1vgxa8k1f")))

(define-public crate-mrg-0.2.2 (c (n "mrg") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)))) (h "0zxh07sf3paq2f5vgga5q357047d88hj8y2ay9jhqddbqh3hc8ji")))

