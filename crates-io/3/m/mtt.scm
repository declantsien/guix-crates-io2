(define-module (crates-io #{3}# m mtt) #:use-module (crates-io))

(define-public crate-mtt-0.1.0 (c (n "mtt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "165234n4mnvks0qjkpjbd0nsx6z7wigklvshhhqqbmx5yw4k2z3x")))

