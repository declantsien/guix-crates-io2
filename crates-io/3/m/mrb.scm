(define-module (crates-io #{3}# m mrb) #:use-module (crates-io))

(define-public crate-mrb-0.1.0 (c (n "mrb") (v "0.1.0") (d (list (d (n "mrb-sys") (r ">=0.1.0, <0.2.0") (d #t) (k 0)))) (h "1wc63qhnslg0czrlfaf2zqcmj3365bfs1w0zxx4qlncygnj4sfwn")))

(define-public crate-mrb-0.1.1 (c (n "mrb") (v "0.1.1") (d (list (d (n "mrb-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0wd5m7r7ggz98f6p51ksj5l4jzlq19p9wh391wrrfarz3ghyfrxa")))

(define-public crate-mrb-0.1.2 (c (n "mrb") (v "0.1.2") (d (list (d (n "mrb-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0j2r57gw47i96ixa5xa5kgwma7wmzqw2mdkla3q6fdrdbm42drs5")))

