(define-module (crates-io #{3}# m mog) #:use-module (crates-io))

(define-public crate-mog-0.0.0 (c (n "mog") (v "0.0.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes" "tokio02"))) (d #t) (k 0)) (d (n "git-url-parse") (r "^0.3") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "hostname") (r "^0.3") (d #t) (k 0)) (d (n "mlmd") (r "^0.1") (d #t) (k 0)) (d (n "slack-hook2") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0s01r5rjwwqdq3a5v7qm7wqskhib9v9zsqyz880x75xcfipyff3a")))

