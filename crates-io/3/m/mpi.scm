(define-module (crates-io #{3}# m mpi) #:use-module (crates-io))

(define-public crate-mpi-0.1.7 (c (n "mpi") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.15.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.13") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0ajpxr2i2nyn13mbk5zy7krnmwfzardd00sr226drh85zvjhxhmk")))

(define-public crate-mpi-0.1.8 (c (n "mpi") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.15.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.13") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "17pggi08fxpk2fsskg698qbfgcahi3asi9hlglvdg3zfx8042r2f")))

(define-public crate-mpi-0.1.9 (c (n "mpi") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.15.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.16") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0vk78r5j9sv9w98kx8yb7svwki36h2pwmrshbi5a7a43npnrycig")))

(define-public crate-mpi-0.1.10 (c (n "mpi") (v "0.1.10") (d (list (d (n "bindgen") (r "^0.15.0") (d #t) (k 1)) (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "16gi65x46s67jxkzgqjbg2ny6h9xdsi8cwvh28ymhkl7pynln8p1")))

(define-public crate-mpi-0.2.0 (c (n "mpi") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.15") (d #t) (k 1)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xf2ng8llwf68infp4r03pkw9vrdh072hsnag1ijhq557zdyk1wz")))

(define-public crate-mpi-0.5.0 (c (n "mpi") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "build-probe-mpi") (r "^0.1") (d #t) (k 1)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libffi") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1ghfz2i4xa8q09asrkgdcgkvsk6avc9l08csykiy2nssbmb59m8x")))

(define-public crate-mpi-0.5.1 (c (n "mpi") (v "0.5.1") (d (list (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "libffi") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "mpi-sys") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "0l165nd057xb7shj1ay7npwvanvdslqz3fiig3haz0gxk1bhbic8") (f (quote (("user-operations" "libffi") ("default" "user-operations"))))))

(define-public crate-mpi-0.5.2 (c (n "mpi") (v "0.5.2") (d (list (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "libffi") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "mpi-sys") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1kkkh02kpdinxf6wm54x2dpswck5lqasm2djvq3pz29v498arxy1") (f (quote (("user-operations" "libffi") ("default" "user-operations"))))))

(define-public crate-mpi-0.5.3 (c (n "mpi") (v "0.5.3") (d (list (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "libffi") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "mpi-sys") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "04bidzl501ghnfbmjqlgrqj4f88fdqbilq0kdp4bxbwldh1qw8id") (f (quote (("user-operations" "libffi") ("default" "user-operations"))))))

(define-public crate-mpi-0.5.4 (c (n "mpi") (v "0.5.4") (d (list (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "libffi") (r "^0.6.3") (o #t) (d #t) (k 0)) (d (n "mpi-sys") (r "^0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "1fqadwvmhjwpyrc2nvi4wglr2nnsr5b3qwmnnmnb6kljhnqy4ikl") (f (quote (("user-operations" "libffi") ("default" "user-operations"))))))

(define-public crate-mpi-0.6.0 (c (n "mpi") (v "0.6.0") (d (list (d (n "build-probe-mpi") (r "^0.1.2") (d #t) (k 1)) (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "libffi") (r "^3.0.0") (o #t) (d #t) (k 0)) (d (n "memoffset") (r "^0.6") (d #t) (k 0)) (d (n "mpi-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "mpi-sys") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.4") (d #t) (k 0)) (d (n "smallvec") (r "^1.0.0") (d #t) (k 0)))) (h "16hyqz5g99dr30hmfrl0p2b7ia2k6j3h95zz5dklcxj4y6dmb0z8") (f (quote (("user-operations" "libffi") ("derive" "mpi-derive") ("default" "user-operations")))) (r "1.54")))

(define-public crate-mpi-0.7.0 (c (n "mpi") (v "0.7.0") (d (list (d (n "build-probe-mpi") (r "^0.1.3") (d #t) (k 1)) (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "libffi") (r "^3.2.0") (o #t) (d #t) (k 0)) (d (n "memoffset") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "memoffset") (r "^0.9") (d #t) (k 2)) (d (n "mpi-derive") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "mpi-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "smallvec") (r "^1.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0yi6py5mpcxw05j9jkdmrk0c7zm9s1n362dmrcd2k28y09mh9zzy") (f (quote (("user-operations" "libffi") ("derive" "mpi-derive" "memoffset") ("default" "user-operations") ("complex" "num-complex")))) (r "1.65")))

(define-public crate-mpi-0.8.0 (c (n "mpi") (v "0.8.0") (d (list (d (n "build-probe-mpi") (r "^0.1.4") (d #t) (k 1)) (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "libffi") (r "^3.2.0") (o #t) (d #t) (k 0)) (d (n "memoffset") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "memoffset") (r "^0.9") (d #t) (k 2)) (d (n "mpi-derive") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "mpi-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1k0iwfdiii9aj97lzrllhqsw7c0ignwnjsjnzic13j72pnj64xv7") (f (quote (("user-operations" "libffi") ("derive" "mpi-derive" "memoffset") ("default" "user-operations")))) (s 2) (e (quote (("complex" "dep:num-complex")))) (r "1.70")))

