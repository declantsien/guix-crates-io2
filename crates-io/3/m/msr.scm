(define-module (crates-io #{3}# m msr) #:use-module (crates-io))

(define-public crate-msr-0.0.0 (c (n "msr") (v "0.0.0") (h "06hli2dqahr440b8glvi75gfqhaq5db87538b8lz97vqf0ylrdgc")))

(define-public crate-msr-0.1.0 (c (n "msr") (v "0.1.0") (h "0c80x16j2swgrif24mfhxdpcg9m75rym7p06frkbwm0dcpiia4lv")))

(define-public crate-msr-0.2.0 (c (n "msr") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1m4biaxvi302hjfci624acxsys8krr35pa1zcn80fqaldma0i9ya") (f (quote (("default" "serde"))))))

(define-public crate-msr-0.3.2 (c (n "msr") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "msr-core") (r "=0.3.2") (d #t) (k 0)) (d (n "msr-plugin") (r "=0.3.2") (o #t) (d #t) (k 0)) (d (n "msr-plugin") (r "=0.3.2") (d #t) (k 2)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)))) (h "04wa3jl6ddcldkk9jmddjhxq39x1y4mca59yvjxx95c26ia80j0r") (f (quote (("plugin" "msr-plugin") ("default"))))))

(define-public crate-msr-0.3.4 (c (n "msr") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "msr-core") (r "=0.3.4") (d #t) (k 0)) (d (n "msr-plugin") (r "=0.3.4") (o #t) (d #t) (k 0)) (d (n "msr-plugin") (r "=0.3.4") (d #t) (k 2)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)))) (h "17sxa9wv2dn1p98mkzs00qj4sirj8470x9bjcd5nh48g57vn6wvw") (f (quote (("plugin" "msr-plugin") ("default"))))))

(define-public crate-msr-0.3.6 (c (n "msr") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 2)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "msr-core") (r "=0.3.6") (d #t) (k 0)) (d (n "msr-plugin") (r "=0.3.6") (o #t) (d #t) (k 0)) (d (n "msr-plugin") (r "=0.3.6") (d #t) (k 2)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0zr07lzxf8h8fk18nj38nl2z5qjaczmnkg4cxk5cx2x3ay2lq8rb") (f (quote (("plugin" "msr-plugin") ("default"))))))

(define-public crate-msr-0.3.7 (c (n "msr") (v "0.3.7") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.19") (d #t) (k 2)) (d (n "msr-core") (r "=0.3.6") (d #t) (k 0)) (d (n "msr-plugin") (r "=0.3.6") (o #t) (d #t) (k 0)) (d (n "msr-plugin") (r "=0.3.6") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0xrvwmzlxaqxvyd8w6x9fq786lrs73bsdprj3yym504kiipiq0yb") (f (quote (("plugin" "msr-plugin") ("default")))) (r "1.71")))

