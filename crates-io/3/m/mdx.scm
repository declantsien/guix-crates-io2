(define-module (crates-io #{3}# m mdx) #:use-module (crates-io))

(define-public crate-mdx-0.0.1 (c (n "mdx") (v "0.0.1") (h "06q95cqapsilhh3c20mfnnqv1h8k7pm7j3drh79lfw050dmjmcr3")))

(define-public crate-mdx-0.0.2 (c (n "mdx") (v "0.0.2") (h "1xqj9b1ad7gqaqi04r18d1fgkc5r261kp3177al9fpg4dvb8cd7d")))

(define-public crate-mdx-0.0.3 (c (n "mdx") (v "0.0.3") (d (list (d (n "color-eyre") (r "^0.5.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "datatest") (r "^0.6.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.1.2") (d #t) (k 0)) (d (n "owo-colors") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 2)))) (h "0dirfkrqjc7vdg0x0lybvsf6alpr6giqn7w9a4g0zk61jjn04c6k")))

(define-public crate-mdx-0.0.4 (c (n "mdx") (v "0.0.4") (d (list (d (n "color-eyre") (r "^0.5.10") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "datatest") (r "^0.6.3") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "nom-supreme") (r "^0.4.1") (d #t) (k 0)) (d (n "owo-colors") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 2)))) (h "1i0vvqkh7yqifwcfxap2507g4q4yd348wfs6krr0iszxb9j5l65z")))

