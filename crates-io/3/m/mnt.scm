(define-module (crates-io #{3}# m mnt) #:use-module (crates-io))

(define-public crate-mnt-0.2.0 (c (n "mnt") (v "0.2.0") (h "0pn2ssj8kqr8zv47hqyahvs14w8qnaj8ng4ls77pldwzjh230idf")))

(define-public crate-mnt-0.3.0 (c (n "mnt") (v "0.3.0") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1v0115q48m3zm8hazbcva5q3bgmph8k1844g6s8slrvp87ms1nj3")))

(define-public crate-mnt-0.3.1 (c (n "mnt") (v "0.3.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "1ssr2pzn50sirpx20agrhimlkf7i4qjpxyng2s7p612v1arfp1qm")))

