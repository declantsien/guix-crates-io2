(define-module (crates-io #{3}# m m3d) #:use-module (crates-io))

(define-public crate-m3d-0.0.1 (c (n "m3d") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (d #t) (k 0)))) (h "0iqmc79ik38f5rqlrd150gpyangblmr3vmv804inayn1annyddny")))

