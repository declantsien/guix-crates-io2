(define-module (crates-io #{3}# m msq) #:use-module (crates-io))

(define-public crate-msq-0.1.0 (c (n "msq") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "193d8pf2jj7901j2naqv0fpq5rid6wd3q332r76k2a8gn2q09s0f")))

(define-public crate-msq-0.1.1 (c (n "msq") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f4yfpv3hn4qmh73gmc27k5js9inbcsk000dq044rvp7r053hiwc")))

(define-public crate-msq-0.2.0 (c (n "msq") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (o #t) (d #t) (k 0)))) (h "1r2314ki5x96vjlchv95jlfkibi7ichdwrpzgq21sj7g6f9vzs7r") (f (quote (("non-async") ("default" "async" "non-async") ("async" "tokio"))))))

(define-public crate-msq-0.2.1 (c (n "msq") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "rt" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)))) (h "021hzc75c8777n7p1hk83b0m829jqrmf0xlr7vqbhma09r06ggcv") (f (quote (("non-async") ("default" "async" "non-async") ("async" "tokio"))))))

