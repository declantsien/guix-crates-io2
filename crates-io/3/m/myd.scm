(define-module (crates-io #{3}# m myd) #:use-module (crates-io))

(define-public crate-myd-0.1.0 (c (n "myd") (v "0.1.0") (d (list (d (n "cargo_metadata") (r "^0.15.3") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1bgr7wmlybp7avlxpify1k5690rixg930hvjh4zx31sz1fdkhfa3")))

(define-public crate-myd-0.1.1 (c (n "myd") (v "0.1.1") (d (list (d (n "cargo_metadata") (r "^0.15.3") (d #t) (k 0)) (d (n "ego-tree") (r "^0.6.2") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.54") (d #t) (k 0)) (d (n "syn") (r "^2.0.12") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0gb6980xjvrgsnfn2d5am06kp8m15lh07204xc6jzp3q4ry3jq52")))

