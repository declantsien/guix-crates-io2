(define-module (crates-io #{3}# m mhv) #:use-module (crates-io))

(define-public crate-mhv-0.1.2 (c (n "mhv") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)))) (h "07sipc7hcb6lymbyvr1l6ffm1szriwwhx02z9x4rj8pzp4ki0wrp") (y #t)))

(define-public crate-mhv-0.1.3 (c (n "mhv") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)))) (h "14x268vrilbv9n42xvkb5cwdv4rq6c4yhfsb03jhqvcd5x9gcvr9") (y #t)))

(define-public crate-mhv-2.0.0 (c (n "mhv") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)))) (h "1a4dv6176pgvi0p7z9w3gw7gqpiw7sgnv8d1r4s4ki5dqypnrli0") (y #t)))

(define-public crate-mhv-0.2.1 (c (n "mhv") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)))) (h "0piy094vczf8h53f538nff8vnh95mgiwlpc834gbq1gsyh8k2iyn") (y #t)))

(define-public crate-mhv-0.2.2 (c (n "mhv") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)))) (h "1z4alj1hmjcnj3hcr9kwwgxn900gwiccrjlisy2mfn50wr8sccva") (y #t)))

(define-public crate-mhv-0.3.0 (c (n "mhv") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)))) (h "0agf2g1jva6p0fspjwcl3kvqfblg9k02ib2999birb5lsh01mv4c") (y #t)))

(define-public crate-mhv-0.3.1 (c (n "mhv") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)))) (h "0xrsijnwfr9ydc4daywsn9rahis2liac4id3fppzk5a759wq2gd0") (y #t)))

(define-public crate-mhv-0.3.2 (c (n "mhv") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)))) (h "0lfb3x27ccj75knk5kla7wjyhxd3shvacv32v46041zklpkazr7r")))

(define-public crate-mhv-0.3.3 (c (n "mhv") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)))) (h "1qn2iwn74h1y9ln7y2n4xlg1fa1v2npv8iwx6c2pc5gjv707ikr0")))

(define-public crate-mhv-0.3.4 (c (n "mhv") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 2)))) (h "05c6rqy072i9f840cazfamh5p597hicg8d198f3p78kc7dg7yf0p")))

