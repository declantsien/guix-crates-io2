(define-module (crates-io #{3}# m mrt) #:use-module (crates-io))

(define-public crate-mrt-0.1.0 (c (n "mrt") (v "0.1.0") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "0d38rd443rijyf8y2sqipnb394w2zygiigcs84d6x8ym9nfcajgy")))

(define-public crate-mrt-0.1.1 (c (n "mrt") (v "0.1.1") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "0i3qych9yyk4qh7accmlwqrm1z4pbjx6sblhj6vqczpwrfqlsqxs")))

(define-public crate-mrt-0.1.2 (c (n "mrt") (v "0.1.2") (d (list (d (n "nom") (r "^4.1.1") (d #t) (k 0)))) (h "1ljszdvixsbvag6vqjjaiwwfqr4xx7bvrh3m7wgjlw90l4jdk256")))

(define-public crate-mrt-0.2.0 (c (n "mrt") (v "0.2.0") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "1ci5laqicyf417mi06z22gxi9clw7181ndjpz0405c0xmvdr5jv5")))

