(define-module (crates-io #{3}# m mcm) #:use-module (crates-io))

(define-public crate-mcm-0.1.0 (c (n "mcm") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.16") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1p18vpvgi5b4syks6693blmc4yndap47criym8v3pczm9gxn4a4f") (y #t)))

(define-public crate-mcm-0.1.1 (c (n "mcm") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.16") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0mywlf4v3vqbx67v6arpf3zspa8isp5hbbwfd1gypz8ijhv0k675") (y #t)))

(define-public crate-mcm-0.1.2 (c (n "mcm") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.16") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0j2lp7r2k4bxc5ws70prss0i765sgi6gh2a5cpqzfap1ijzxqcip") (y #t)))

(define-public crate-mcm-0.1.3 (c (n "mcm") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.16") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "1md9d6nh39n3khqggjkps1xznw253jgyxgpidvrchy2kpdvh5qkj")))

