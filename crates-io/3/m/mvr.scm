(define-module (crates-io #{3}# m mvr) #:use-module (crates-io))

(define-public crate-mvr-0.2.0 (c (n "mvr") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "1lamaf654jyf3543jkydvrmj8w4npijzh676rd026a4n4m3wrmb8")))

(define-public crate-mvr-0.2.1 (c (n "mvr") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "regex") (r "^1.0.5") (d #t) (k 0)))) (h "17sr2jrmd52vb1bq0ji7855x0kyb7r8fr7z0j9iij4lrjh2m7iak")))

