(define-module (crates-io #{3}# m mtl) #:use-module (crates-io))

(define-public crate-mtl-0.1.0 (c (n "mtl") (v "0.1.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "05c46i47zyq0rjxi184s7gnb9w8q70qj3y60gy5bb71myr55sbsc")))

(define-public crate-mtl-0.1.1 (c (n "mtl") (v "0.1.1") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0pbc1l7hkp0p85hci146b3hswiqc1h292gz8wxlbkf8grnpsiq8l")))

(define-public crate-mtl-0.1.2 (c (n "mtl") (v "0.1.2") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0m9skskirv53dw1fw3ha2gg90brc7yfydzpsmrbffqpp5gmy3bz5")))

(define-public crate-mtl-0.1.3 (c (n "mtl") (v "0.1.3") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0qqbcy11yd86z6qg09iy7a2gkfpkn0l9pm6c6pb7xry8bqsrnq4n")))

(define-public crate-mtl-0.1.4 (c (n "mtl") (v "0.1.4") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0alds6xbl3j4b6yk8h65h2zkl5jhhj2834h7xlwzh7440kis1jva")))

(define-public crate-mtl-0.1.5 (c (n "mtl") (v "0.1.5") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0c107qmwxc174z0cqy07xy38p3ih3c4n0qf581vm4b476g29sxzl")))

