(define-module (crates-io #{3}# m mdo) #:use-module (crates-io))

(define-public crate-mdo-0.2.0 (c (n "mdo") (v "0.2.0") (h "0dca4hjhm61qsr9ys13nm4f8jya7wj2320xc6133kqnw0s8c8i9f")))

(define-public crate-mdo-0.3.0 (c (n "mdo") (v "0.3.0") (h "1h01hh4xp10d9ag5d06ph32zzvc2z9vmxhwbd6r7rdax79wijcpm")))

