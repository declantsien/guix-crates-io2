(define-module (crates-io #{3}# m md4) #:use-module (crates-io))

(define-public crate-md4-0.1.0 (c (n "md4") (v "0.1.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.1") (d #t) (k 2)) (d (n "digest") (r "^0.1") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "fake-simd") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "12rr2z275q8zzhdvg1k9jswcpqhaa2lkf51hlg0jbxq77rbscs8q")))

(define-public crate-md4-0.2.0 (c (n "md4") (v "0.2.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.1") (d #t) (k 2)) (d (n "digest") (r "^0.2") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "fake-simd") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "1mkn4lj6bvmcw130wxvirmam8nksinykkmapwfqq9z76krihknwf")))

(define-public crate-md4-0.3.0 (c (n "md4") (v "0.3.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2") (d #t) (k 2)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "fake-simd") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0a1hjhljavraml3vb7gwh8pm9mh3b7a9sn1160rm4r5vwzsdnlj5")))

(define-public crate-md4-0.3.1 (c (n "md4") (v "0.3.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2") (d #t) (k 2)) (d (n "digest") (r "^0.3") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.1") (d #t) (k 0)) (d (n "fake-simd") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "0hh2vmmcwj5vq351zjcb1pzayxmmz4380fcijcn36nvk9n2pdf6b")))

(define-public crate-md4-0.4.0 (c (n "md4") (v "0.4.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "fake-simd") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "04fbzcc7ncsc1x07d4c5dai9fiqs158qvc0r91r8j2h68fj1kqmm")))

(define-public crate-md4-0.4.1 (c (n "md4") (v "0.4.1") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.3") (d #t) (k 2)) (d (n "digest") (r "^0.4") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.2") (d #t) (k 0)) (d (n "fake-simd") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.6") (d #t) (k 0)))) (h "1ynypmlj71s4yqsnzid2iji93yslpjlz1a9ax5hhiq2iydp8kk6x")))

(define-public crate-md4-0.5.0 (c (n "md4") (v "0.5.0") (d (list (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.4.0") (d #t) (k 2)) (d (n "digest") (r "^0.5.0") (d #t) (k 0)) (d (n "digest-buffer") (r "^0.3.0") (d #t) (k 0)) (d (n "fake-simd") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.7") (d #t) (k 0)))) (h "1h0vhl3ypig7m4s6db6nkcscbj2hpzpyd74njz7nxs73cr09z9y9")))

(define-public crate-md4-0.6.0 (c (n "md4") (v "0.6.0") (d (list (d (n "block-buffer") (r "^0.2") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.5") (d #t) (k 2)) (d (n "digest") (r "^0.6") (d #t) (k 0)) (d (n "fake-simd") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.8") (d #t) (k 0)))) (h "1q21r4lcmx0knwrf22c2i37g0w47z119hkk08xqkq1wqa37ck36c")))

(define-public crate-md4-0.7.0 (c (n "md4") (v "0.7.0") (d (list (d (n "block-buffer") (r "^0.3") (d #t) (k 0)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "digest") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.7.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "fake-simd") (r "^0.1") (d #t) (k 0)))) (h "0v3rdkvxk6yv4v2w0gn8mlnkwjaz591d6bcggg9ykrkvnswgscip")))

(define-public crate-md4-0.8.0 (c (n "md4") (v "0.8.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "fake-simd") (r "^0.1") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "18frpyag0dx0zfdg7m6ysqsd1y2rf4g7vblwfvd7mararxjhq0x4") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-md4-0.9.0 (c (n "md4") (v "0.9.0") (d (list (d (n "block-buffer") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1qh93dnvsky757qbpci0msvzajcr6slcj79a4rhabrhyc0qpj6nx") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-md4-0.10.0 (c (n "md4") (v "0.10.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1a9z8iw8sm6zp2v3av3bjcvi3yv8zp7gjbqql7xpac6zqbfj7iv4") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-md4-0.10.1 (c (n "md4") (v "0.10.1") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "0jqhyypz9v6yaah0n85dsy9s0l27jfql5jr898rcp89z18jgb9ck") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-md4-0.10.2 (c (n "md4") (v "0.10.2") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "1nixkyx1zsn8jkvhzgwqlh8z3yvlw4jr5539pzxfbp1l6lvar9bx") (f (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std"))))))

