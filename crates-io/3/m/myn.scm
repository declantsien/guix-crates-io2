(define-module (crates-io #{3}# m myn) #:use-module (crates-io))

(define-public crate-myn-0.1.0 (c (n "myn") (v "0.1.0") (h "0ljfldw20bsvqinn11ah6zkm15xh6b5j02qgpcrsw1c0gdqayl7r")))

(define-public crate-myn-0.2.0 (c (n "myn") (v "0.2.0") (d (list (d (n "proc_macro") (r "^1") (d #t) (k 2) (p "proc-macro2")))) (h "01zc3n7hifwkbksy3j5x4y3prhm51rd09sa2gl1h9cf9xrsjc6sk") (r "1.58.0")))

(define-public crate-myn-0.2.1 (c (n "myn") (v "0.2.1") (d (list (d (n "proc_macro") (r "^1") (d #t) (k 2) (p "proc-macro2")))) (h "0fp6dph06dphz3rfr2yiw92bcki4nydyf3hn2rkbqpqamvfpmssi") (r "1.58.0")))

