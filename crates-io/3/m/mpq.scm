(define-module (crates-io #{3}# m mpq) #:use-module (crates-io))

(define-public crate-mpq-0.1.0 (c (n "mpq") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0lq8b0fzi9kmb9qaa9dkf1xnlyllkg2nri5n3xx9f8nzr1x4mplx")))

(define-public crate-mpq-0.2.0 (c (n "mpq") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "bzip2") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1r4h82ky899545kqf1kbppi3yd1k7bzklcg60v8jyp1siaz6hppm")))

(define-public crate-mpq-0.2.1 (c (n "mpq") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "bzip2") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0qsyvbq0cxnmcllcllr38c3y5f4pbx2229caxn9zc1yccgkvy2ph")))

(define-public crate-mpq-0.3.0 (c (n "mpq") (v "0.3.0") (d (list (d (n "adler32") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "bzip2") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "1n9gjaqaygh2hlyirnw3j5lk92jdqhr49akp8wvv57q7xr17qm5q")))

(define-public crate-mpq-0.4.0 (c (n "mpq") (v "0.4.0") (d (list (d (n "adler32") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "bzip2") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)))) (h "0jmba4nb9kmhkf9w0dgy26a43ww34s54bd5jwhvqgxqwipn3yqg7")))

(define-public crate-mpq-0.5.0 (c (n "mpq") (v "0.5.0") (d (list (d (n "adler32") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "bzip2") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "implode") (r "^0.1") (d #t) (k 0)))) (h "1qgxl5awimx7mx3mjhv9g0pbdx4f48ac28nvnibk8cdrhyrqh577")))

(define-public crate-mpq-0.6.0 (c (n "mpq") (v "0.6.0") (d (list (d (n "adler32") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "bzip2") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "implode") (r "^0.1") (d #t) (k 0)))) (h "17a63m3hsw6q91klrfiwf36mkrhy7a024n52wx87z0z335xrhqs9")))

(define-public crate-mpq-0.7.0 (c (n "mpq") (v "0.7.0") (d (list (d (n "adler32") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "bzip2") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "implode") (r "^0.1") (d #t) (k 0)))) (h "0ngmngvcsxbf8zl4sq289zm83bg2p2a4rvzih3fxis38jipzz66x")))

(define-public crate-mpq-0.8.0 (c (n "mpq") (v "0.8.0") (d (list (d (n "adler32") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "bzip2-rs") (r "^0.1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.27") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "implode") (r "^0.1") (d #t) (k 0)))) (h "1rr9qyqrgz0hk52h8flgymv9srvsifh2v70nv919wp5jhk7x1y3j")))

