(define-module (crates-io #{3}# m miv) #:use-module (crates-io))

(define-public crate-miv-0.1.0 (c (n "miv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "11indx9ia9wwbaq759spjgpinfyx7lvz26nf2d2zhafw6abhlg5a")))

(define-public crate-miv-0.1.1 (c (n "miv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0dx09ssa33adsqv8iyla1rwyx1jj7q0izp3lg81si4w71pd6kwhk")))

