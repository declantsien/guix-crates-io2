(define-module (crates-io #{3}# m my3) #:use-module (crates-io))

(define-public crate-my3-0.1.0 (c (n "my3") (v "0.1.0") (d (list (d (n "my2") (r "^0.1.0") (d #t) (k 0)))) (h "1wpybks2h1flnpqwn3c3028mn714lf468r3ya1shj0yg80mxqhkw")))

(define-public crate-my3-0.1.1 (c (n "my3") (v "0.1.1") (d (list (d (n "my2") (r "^0.1.1") (d #t) (k 0)))) (h "00qmis85wnm1szpwfh8lxqk81al44qgxj1km2xgzzhwyx9j4rlvk")))

(define-public crate-my3-0.1.2 (c (n "my3") (v "0.1.2") (d (list (d (n "my2") (r "^0.1.2") (d #t) (k 0)))) (h "05ymsj4b6pzg9mi4iramb2rw6wlkgzsbmrnlcd4nj0yw5k9j6s58")))

(define-public crate-my3-0.1.3 (c (n "my3") (v "0.1.3") (d (list (d (n "my2") (r "^0.1.3") (d #t) (k 0)))) (h "1vmy3pigxmsprmpnvd7wxqzq6qldc6s756744kr9wykdppkagdpq")))

(define-public crate-my3-0.1.4 (c (n "my3") (v "0.1.4") (d (list (d (n "my2") (r "^0.1.4") (d #t) (k 0)))) (h "005mv6g2plwkp0hqhg8acp4kqb24cmzk3112mz7kdwjbzrfarshb")))

(define-public crate-my3-0.1.5 (c (n "my3") (v "0.1.5") (d (list (d (n "my2") (r "^0.1.5") (d #t) (k 0)))) (h "0922mr9jg83dbcw5fcbj0rhblv2hk7dp4q0pql0rv488hikjrx5i")))

