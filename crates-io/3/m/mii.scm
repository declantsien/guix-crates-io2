(define-module (crates-io #{3}# m mii) #:use-module (crates-io))

(define-public crate-mii-0.1.0 (c (n "mii") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "08937x5gfr713y53x0i2hdyhlvaj3qzbv32cnxdk1r8zmg7p6ghr")))

(define-public crate-mii-0.1.1 (c (n "mii") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1ssfbinnphgmxy3ks7br9kf2sk8mxinis3s1bx1is8z09g1gxh45")))

