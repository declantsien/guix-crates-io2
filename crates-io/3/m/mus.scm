(define-module (crates-io #{3}# m mus) #:use-module (crates-io))

(define-public crate-mus-0.2.0 (c (n "mus") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "mpd") (r "^0.1.0") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "02pa1cx40nafcajvdvvwfq2jhykcy88hpv95b9f6c5acvynqc5b5")))

