(define-module (crates-io #{3}# m mut) #:use-module (crates-io))

(define-public crate-mut-0.1.0 (c (n "mut") (v "0.1.0") (h "0wnrx835flk7isjhslwxpgcw2qkl9zapwv1n06my13lmq8jamz1z") (y #t)))

(define-public crate-mut-0.1.1 (c (n "mut") (v "0.1.1") (h "1mxq5ncsk2p95py60xz6jgl2fl3yf5y7hn1677njx0k1g552kkgv") (y #t)))

(define-public crate-mut-0.1.3 (c (n "mut") (v "0.1.3") (h "1h15pp5xgih43517qghn2lkrcznsjagifm2i89fqkqfxq36b6arq") (y #t)))

(define-public crate-mut-0.1.4 (c (n "mut") (v "0.1.4") (h "07zqnmcj2i0j13irrqy32h06453j03ryzmm4nfc308wkj6wvpzz8")))

