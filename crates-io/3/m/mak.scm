(define-module (crates-io #{3}# m mak) #:use-module (crates-io))

(define-public crate-mak-0.1.0 (c (n "mak") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.7") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1lkrzlx519h0ll1vq0lfr1cadldp4g41m5j5rrnnpdjp0a8yj6mx")))

