(define-module (crates-io #{3}# m mhs) #:use-module (crates-io))

(define-public crate-mhs-0.1.8 (c (n "mhs") (v "0.1.8") (h "0s3ha27bqaa2kqgrd8qa0ciw6hj6nvaz8j22y2jysm03fhf27vxi")))

(define-public crate-mhs-0.1.9 (c (n "mhs") (v "0.1.9") (h "0l1ljqz3d0sdxkz4ifm86sxhfl93lffrp0pkxy223fm12nmnrky4")))

(define-public crate-mhs-0.1.10 (c (n "mhs") (v "0.1.10") (h "1gwiwz9g1zm1yix3fw0pzbgv0fbhhvf4qa7n9ka5yd9f8pxlqk8w")))

(define-public crate-mhs-0.1.13 (c (n "mhs") (v "0.1.13") (h "1j56x61vy88nj65fn2cm3ysvvjm0l0457qgw33d9x55g15zis8xy")))

(define-public crate-mhs-0.1.14 (c (n "mhs") (v "0.1.14") (h "1g771nvz5dx4mm7r5p6109pfb33f29gig2b0j6lmgcaqp1zbqb9w")))

(define-public crate-mhs-0.1.17 (c (n "mhs") (v "0.1.17") (h "0s7ajy11zrzb6m1vasxxdhaxp02167m8ks03zjbsfbjx49znpnl0")))

(define-public crate-mhs-0.1.19 (c (n "mhs") (v "0.1.19") (h "14xi90qabyaxfmchh9r7hd6ck2iw0avf15sbyhq5crg5hkv237pq")))

(define-public crate-mhs-0.1.20 (c (n "mhs") (v "0.1.20") (h "0hkhd8safzml1vx5abgdpfgzyajamwa2dc4wf013mhc7pzc7yvj2")))

(define-public crate-mhs-0.1.23 (c (n "mhs") (v "0.1.23") (h "0v18bhlsis8z6cxywym6s6c4bggr4kh1hpamnkgpca9cv5538b8m")))

(define-public crate-mhs-0.1.24 (c (n "mhs") (v "0.1.24") (h "0f61pmvap6074pbg2c47h0d625ij5j1palyfikxrpr8fy7d668mg")))

(define-public crate-mhs-0.2.0 (c (n "mhs") (v "0.2.0") (h "1qx1mvp3fcjibynk1cl66bqlj9hvl95jmh1mclic18ifcbw1w5xk")))

(define-public crate-mhs-0.2.3 (c (n "mhs") (v "0.2.3") (h "1d4pkr2j0x5x8vyp69zgb2cbrxxv386m9n55m62a0151scd4xa0m")))

(define-public crate-mhs-0.3.0 (c (n "mhs") (v "0.3.0") (h "1qb3n1sk01qknshvd4pli2mxsx2rjbas62wagsbgqxbxk9g6y8fm")))

(define-public crate-mhs-0.3.1 (c (n "mhs") (v "0.3.1") (h "1i5djix4x0n506sf692jfvkkcckba8ndgrgqc0pkfqysnacby373")))

(define-public crate-mhs-0.3.2 (c (n "mhs") (v "0.3.2") (h "0kr36p6zj1z6d9czbzmxic0qm57djbp3i7z492jbbcq45nzmg181")))

(define-public crate-mhs-0.3.9 (c (n "mhs") (v "0.3.9") (h "0ix59id4gywcj7d3z6hz43594dkilyzl1x50996spqgmmm4fs49b")))

(define-public crate-mhs-0.3.12 (c (n "mhs") (v "0.3.12") (h "09dc3al2kdc672mhiqmxrvr7gbqxwcmibxl3kmyjj43qfjw7dsr3")))

(define-public crate-mhs-0.4.0 (c (n "mhs") (v "0.4.0") (h "1534q6dw1lsr678gjxn3xm4smfn34mcb8figwy2xa536h7rs9hwk")))

