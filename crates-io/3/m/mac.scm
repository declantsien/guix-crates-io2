(define-module (crates-io #{3}# m mac) #:use-module (crates-io))

(define-public crate-mac-0.0.1 (c (n "mac") (v "0.0.1") (h "1bs2viq2v5c6mssfl0f3b4v69xd5bnv2rfsj7cpq51a9vcwpaanh")))

(define-public crate-mac-0.0.2 (c (n "mac") (v "0.0.2") (h "0pjdw2iibqlwjyzma35a5x5ckk7g3j6sa7azws8vbnqd1n6b078v")))

(define-public crate-mac-0.1.0 (c (n "mac") (v "0.1.0") (h "183dbkwy1p9an4wwx3h656sl16r9gl8nbdv59bjq0a9r61r2gh71")))

(define-public crate-mac-0.1.1 (c (n "mac") (v "0.1.1") (h "194vc7vrshqff72rl56f9xgb0cazyl4jda7qsv31m5l6xx7hq7n4")))

