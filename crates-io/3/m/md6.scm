(define-module (crates-io #{3}# m md6) #:use-module (crates-io))

(define-public crate-md6-1.0.0 (c (n "md6") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f7n02382m6kdwsqqzbjkslfs1rivi59q0yc588z8088khmgmdi6")))

(define-public crate-md6-1.0.1 (c (n "md6") (v "1.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zl0sb6si0l22dqr9rd30y3v8my75rbrl6z3lf93mkkvh8nmbk9v")))

(define-public crate-md6-1.1.0 (c (n "md6") (v "1.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0phyj9zl15bb9784xjm7zzla8145aqk241bhj80msaxv944zgqpm")))

(define-public crate-md6-1.1.1 (c (n "md6") (v "1.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w22pvkprsf89sai7lml54qxgmw9w0bjwbg8pamg6532bpcznfac")))

(define-public crate-md6-2.0.0 (c (n "md6") (v "2.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pg4l8k98dwi1gzig69s2qpm98sh5h2sb9h5zb9yr744hik85ral")))

(define-public crate-md6-2.0.1 (c (n "md6") (v "2.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cgvcc7zdq5clj5zf6ncfn3c3jc3clil6j2ls2q6k03hzw9g3fi1")))

(define-public crate-md6-2.0.2 (c (n "md6") (v "2.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k8js6r78s29zhijx4vvf64wz084chy0i70695gj945qa2lzqwdg")))

(define-public crate-md6-2.0.3 (c (n "md6") (v "2.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vxq0z53hn8bba1vffhi55phqbzcm8lzgqwywibi4pblqiw66hpd")))

