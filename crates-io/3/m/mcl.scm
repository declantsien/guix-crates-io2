(define-module (crates-io #{3}# m mcl) #:use-module (crates-io))

(define-public crate-mcl-0.1.0-alpha (c (n "mcl") (v "0.1.0-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bqvfrx5mpqq1is9hqcskfkxmbdr29q8gqfmkd7ynclsqik15y60")))

(define-public crate-mcl-0.2.1-alpha (c (n "mcl") (v "0.2.1-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q64z8ns98zl1ycm1ahjkg9mv8yfxdl3i7qsp97zkdqyqizfvla1")))

(define-public crate-mcl-0.2.2-alpha (c (n "mcl") (v "0.2.2-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07h0nxn6axbb5h290711jc58133n18xdcgszbanaibhm40b1ll6v") (f (quote (("serde_lib" "serde") ("default" "serde_lib"))))))

(define-public crate-mcl-0.2.3-alpha (c (n "mcl") (v "0.2.3-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mcl_derive") (r "^0.2.3-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1a86hnhy8dr2d232vqawxq6qd01psa5ckgsk7104qjbwnp3ihlwl") (f (quote (("serde_lib" "serde") ("default" "serde_lib"))))))

(define-public crate-mcl-0.3.0-alpha (c (n "mcl") (v "0.3.0-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mcl_derive") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1pgyn2ldp6j9zvqijw3n1lhdhcqyr14w0jgk1pzwfcrdjc0sm0h1") (f (quote (("serde_lib" "serde") ("default" "serde_lib"))))))

(define-public crate-mcl-0.4.0-alpha (c (n "mcl") (v "0.4.0-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mcl_derive") (r "^0.4.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03n5brj83n9y3hzd9263k61f8z5njvfa8fzzwm783gzh4ylhsm5r") (f (quote (("serde_lib" "serde") ("default" "serde_lib"))))))

(define-public crate-mcl-0.5.0-alpha (c (n "mcl") (v "0.5.0-alpha") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mcl_derive") (r "^0.5.0-alpha") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1lf2azypjfgchd38mnji4ljwqqvlwhfy8hnglbsg9k4xrynqdvz2") (f (quote (("serde_lib" "serde") ("default" "serde_lib"))))))

