(define-module (crates-io #{3}# m mew) #:use-module (crates-io))

(define-public crate-mew-0.0.0 (c (n "mew") (v "0.0.0") (h "0mhiywm6jhw1kz0iawpanfr24r6nq9zga5xah2rka5pfb4a7p4nj")))

(define-public crate-mew-0.0.1 (c (n "mew") (v "0.0.1") (h "0van951mmhi84mgf444ww8qkg1x223m3fnkvfjbwapaqys515qzj")))

