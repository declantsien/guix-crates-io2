(define-module (crates-io #{3}# m mkr) #:use-module (crates-io))

(define-public crate-mkr-0.1.0 (c (n "mkr") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mackerel_client") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.145") (d #t) (k 0)))) (h "12md7ngs23hnjamp9zlsvllh72mvxgb4im57pdy496l7mcdhgqcp")))

