(define-module (crates-io #{3}# m mji) #:use-module (crates-io))

(define-public crate-mji-0.1.0 (c (n "mji") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1d3jl065f8lzyak49iwyvfk3ibnibf8w9mq5ind83y9vmf5qm942")))

