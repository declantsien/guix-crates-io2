(define-module (crates-io #{3}# m my2) #:use-module (crates-io))

(define-public crate-my2-0.1.0 (c (n "my2") (v "0.1.0") (d (list (d (n "my1") (r "^0.1.0") (d #t) (k 0)))) (h "053iwzmgj3lsg5ljdjhaq2gw9ahcl22wsi6kp431v9abxwy929ws")))

(define-public crate-my2-0.1.1 (c (n "my2") (v "0.1.1") (d (list (d (n "my1") (r "^0.1.1") (d #t) (k 0)))) (h "00xy711ln38xwzkvpr2a7n8g22x9v5bqx7hic763fgh3czkkjsy5")))

(define-public crate-my2-0.1.2 (c (n "my2") (v "0.1.2") (d (list (d (n "my1") (r "^0.1.2") (d #t) (k 0)))) (h "0dcv5pc1mvsw7rzrkmki5zsvwbzbvrxi3gl7qv8h0k3pcmwajhcd")))

(define-public crate-my2-0.1.3 (c (n "my2") (v "0.1.3") (d (list (d (n "my1") (r "^0.1.3") (d #t) (k 0)))) (h "1ivbzjs7lpwcp55bv2lpv9nxnzclq96rc0s8gcqwajdfrm6i6hmi")))

(define-public crate-my2-0.1.4 (c (n "my2") (v "0.1.4") (d (list (d (n "my1") (r "^0.1.4") (d #t) (k 0)))) (h "11663v5h4vcg8z8cgwlvlidygz4pqsbdw6vr1jq517292h7ba7my")))

(define-public crate-my2-0.1.5 (c (n "my2") (v "0.1.5") (d (list (d (n "my1") (r "^0.1.5") (d #t) (k 0)))) (h "1hfpc26icj7mnymlrs39gv17mykrpfrlmbkicabchmbbwk53vy1p")))

