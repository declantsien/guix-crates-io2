(define-module (crates-io #{3}# m msh) #:use-module (crates-io))

(define-public crate-msh-0.1.0 (c (n "msh") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "execute") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "088vg24r20m1c5l42qqdhw5ff6rwk35w4js2h5lz8zw4zfn1hrdc")))

(define-public crate-msh-0.1.1 (c (n "msh") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "execute") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.151") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)))) (h "1cryzsjj5f7fnzg2q6lkrqz43a5yvgysw5ccs9mwbw5f5xd3j12h")))

