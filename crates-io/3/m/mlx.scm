(define-module (crates-io #{3}# m mlx) #:use-module (crates-io))

(define-public crate-mlx-0.1.0 (c (n "mlx") (v "0.1.0") (d (list (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0ikz8gdgw94ns1r3aqc3nvmsjahkd7f21in2cqb5k6awxzax8g06")))

(define-public crate-mlx-0.1.1 (c (n "mlx") (v "0.1.1") (d (list (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0jiskkfz7jjwcd7lgx3f28shwbfzb0d7dw6jfbm7rbl3hvwbs631")))

(define-public crate-mlx-0.1.2 (c (n "mlx") (v "0.1.2") (d (list (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "04yj60vysivgzblw1w1blc6zny8sdn1c5ln9vmqqpzggkgh572hn")))

