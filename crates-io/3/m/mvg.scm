(define-module (crates-io #{3}# m mvg) #:use-module (crates-io))

(define-public crate-mvg-0.1.0 (c (n "mvg") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d5md5f2xzar4pgrg30y7d3rr0q7zii4nin9qisni8g50yzjllq6")))

(define-public crate-mvg-0.1.1 (c (n "mvg") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1shdlcvksanzjzjpgpfq21vckndbc452vzj3rwfjggmrb9vdd19m")))

(define-public crate-mvg-0.1.2 (c (n "mvg") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "13qjk5ld85cwdrx0nbg9anhgmvkcg0s6m3ln8iphsrwmhzslx7w1")))

