(define-module (crates-io #{3}# m mdl) #:use-module (crates-io))

(define-public crate-mdl-0.1.0 (c (n "mdl") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)))) (h "0jxbbv1nym8i55m40k1mwpljc5yih5wkhpxzii71pc9fgw6h48fx")))

(define-public crate-mdl-0.1.1 (c (n "mdl") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)))) (h "0v4akfz7gq683pmvpwg87k6byr1nlanwgrpjg9j0dlvwr9bdi7y0")))

(define-public crate-mdl-0.1.2 (c (n "mdl") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)))) (h "0xw9qgg0q4rpmsbsdf3ny0n6ghs68pcz176qhlbbjm1vg3f6wkab")))

(define-public crate-mdl-0.1.3 (c (n "mdl") (v "0.1.3") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)))) (h "0m64jbgaf30hqw99k6v1vpqrd3sfjj7jih5pmdgb59ng0q404nlw")))

(define-public crate-mdl-1.0.0 (c (n "mdl") (v "1.0.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)))) (h "0g06pby2bjpxx7sv51p3q8a2ns1jc9mm8k6jr68wbqvynhps8znx")))

(define-public crate-mdl-1.0.1 (c (n "mdl") (v "1.0.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)))) (h "1k5v1k6mcj00vw36l155hys67sf93qk513f0yr5gzsavhp1v6xr1")))

(define-public crate-mdl-1.0.2 (c (n "mdl") (v "1.0.2") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)))) (h "0llqvrl5x4lrd8rwcnjm4x4mq518zl29l3c9w1qbd37lcld35zyk")))

(define-public crate-mdl-1.0.3 (c (n "mdl") (v "1.0.3") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)))) (h "1c4f2dpi4jsv5qwm8ymfrkxly9brq9vmak21vp1r8k3p7zk2d4m9")))

(define-public crate-mdl-1.0.4 (c (n "mdl") (v "1.0.4") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.79") (d #t) (k 0)))) (h "0xcvfg7ijf43hgp85vz022jjzsdk6lxi0krz1b9isx3gi8gniaxr")))

(define-public crate-mdl-1.0.5 (c (n "mdl") (v "1.0.5") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "lmdb") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.79") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mavphw0ynhggm310rbldzifcrc507jax1nzwrmj4cz1xv7vw24l")))

