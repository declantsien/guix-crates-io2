(define-module (crates-io #{3}# m mms) #:use-module (crates-io))

(define-public crate-mms-0.0.0 (c (n "mms") (v "0.0.0") (h "1qz5gha52h8ikwhlyzgy1jf8xjdazy1mmd4d55s5lxrd1vrdv5sn") (y #t)))

(define-public crate-mms-0.0.1 (c (n "mms") (v "0.0.1") (h "0q6fb4a0cdn4wbfjs6x16p63l3428mhb10p2yv2jy2sinibcx406")))

