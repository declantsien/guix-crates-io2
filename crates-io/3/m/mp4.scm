(define-module (crates-io #{3}# m mp4) #:use-module (crates-io))

(define-public crate-mp4-0.1.0 (c (n "mp4") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "17qri532ywc8kz5sdmb4gcvyly4r5pq0jwilwsrn9sh8g9f6nn75")))

(define-public crate-mp4-0.2.0 (c (n "mp4") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0wk3b9h1fajc984ryjnl75barm440vl5skbb6hza3sn6000l8xbv")))

(define-public crate-mp4-0.3.0 (c (n "mp4") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "079232wx47njxrwqb92lzlkjs2gw39rjkzk8m3bwgnpr1hwghw2w")))

(define-public crate-mp4-0.4.0 (c (n "mp4") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "098h2y6xkf82m1fyyqqvnf9ymql2h86x9x1wgzbcqqqw59vk7rfr")))

(define-public crate-mp4-0.4.1 (c (n "mp4") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1zgzv7qmg4g2qvax3vcwasv8b6av4g7r8j58nbqxz98r39fd7lbf")))

(define-public crate-mp4-0.4.2 (c (n "mp4") (v "0.4.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1ffyargljymnh78ixakb8rjwmjcb6wzm9y3nfvzzs7xb9pqyik6m")))

(define-public crate-mp4-0.4.3 (c (n "mp4") (v "0.4.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "104mkmqmmhnahdwmql6g7za1wy7a38cvi4j40y7y6i67xh8r97a0")))

(define-public crate-mp4-0.5.0 (c (n "mp4") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g1cnkm8rvv23g39wbz10y3pv8nkfp72h9rkwj0xc65xkqcci976")))

(define-public crate-mp4-0.5.1 (c (n "mp4") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00div507v3d6hyqkxzslcvd3brrkgmgz40pxwh3yxm4pwgyr9qix")))

(define-public crate-mp4-0.5.2 (c (n "mp4") (v "0.5.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1swag086r9mlj3rfh41b5fyq1fp9jf7dy50jzdvy76r2asc4rv22")))

(define-public crate-mp4-0.6.0 (c (n "mp4") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1irlaxl4c18a4l5jpsz82xs9bl55c9lj9x4nkv19ypvhk0j7gl6r")))

(define-public crate-mp4-0.7.0 (c (n "mp4") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wc509s3ippqxjrhi43zzgcwbxbwdqq0cnhzrpvr0sjdjb5a02d5")))

(define-public crate-mp4-0.7.1 (c (n "mp4") (v "0.7.1") (d (list (d (n "byteorder") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "bytes") (r ">=0.5.0, <0.6.0") (d #t) (k 0)) (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "num-rational") (r ">=0.3.0, <0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "1zdgj17rb29q18x725f96md7zpa7m0giwzc77fdgwz9mgnzw38rk")))

(define-public crate-mp4-0.7.2 (c (n "mp4") (v "0.7.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m8gp9774p3svhdcr36kk43nicl11jiyprh52g171j1phs8i1j99")))

(define-public crate-mp4-0.8.0 (c (n "mp4") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bhqrkyv7155ncjxq7x8phfpi34gi7r8h3kk7hq924v2s4kvwfql")))

(define-public crate-mp4-0.8.1 (c (n "mp4") (v "0.8.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11ivsaari457cr0190fmxxlh1ngvv8dnjqpka8iip37xm36b17cr")))

(define-public crate-mp4-0.8.2 (c (n "mp4") (v "0.8.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03mm52l13wsrwxrz7xms8aqwdxdhkl1040c6w1biqi96mfln55rn")))

(define-public crate-mp4-0.8.3 (c (n "mp4") (v "0.8.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14zh9dh6hwzmbchl7yh8wv84r0fniq1jcbz9x2hqq699h6l1issi")))

(define-public crate-mp4-0.9.0 (c (n "mp4") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h5yy36a3g9s15ppx4fyy65yvcsllb2ashv976aqscdri16hsrl5")))

(define-public crate-mp4-0.9.1 (c (n "mp4") (v "0.9.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l1z8k11wn2k58rys018faplzjiykns9qdygz7aq7dpvm3nwd3ji")))

(define-public crate-mp4-0.9.2 (c (n "mp4") (v "0.9.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12rwsjj5fn5h198ih0ig08vz34rfjlnf8rw3d0i5jwbaay88f1kq")))

(define-public crate-mp4-0.10.0 (c (n "mp4") (v "0.10.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0zxh8izi56yhliaf36hkd3r24bz1c4320ihx572rywx5k8dvvafw")))

(define-public crate-mp4-0.11.0 (c (n "mp4") (v "0.11.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05plkz2m3gig63lddqjfn7rjgjm4bwsdwjv0c66zdim1llbnlkcn")))

(define-public crate-mp4-0.12.0 (c (n "mp4") (v "0.12.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s0h5i440xalbpk3fxvl17hr35sjqdwwrqhq5yvl62763ln2sgrv")))

(define-public crate-mp4-0.13.0 (c (n "mp4") (v "0.13.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k96fhbhqrsnqy0sd0gqmginx8z7vhnhl43mm19birshlb5li4sh")))

(define-public crate-mp4-0.14.0 (c (n "mp4") (v "0.14.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "num-rational") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hx5aaihw3ij1sq9i51sqk8sqjnw2h1j4l735954jpnmbr6q7vy9")))

