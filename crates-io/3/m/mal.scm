(define-module (crates-io #{3}# m mal) #:use-module (crates-io))

(define-public crate-mal-0.1.0 (c (n "mal") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0g66p12rbpmv76i31fad82s0kfgm7c2h260lssa7ia92sf5wzxzh")))

(define-public crate-mal-0.2.0 (c (n "mal") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1zfvf0svsj45ypn4p64p9z1d655kii90c11984icalx9bfd8bihy")))

(define-public crate-mal-0.2.1 (c (n "mal") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0hygcyqv64vnhy36455c7lg28pi77sd7gfzridr3w680mq91aayq")))

(define-public crate-mal-0.3.0 (c (n "mal") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1pfji8xfjsabxynfaac5ba951pfc6ydzxpmxy1ra727y39pg1hj0") (f (quote (("manga-list") ("default" "anime-list" "manga-list") ("anime-list"))))))

(define-public crate-mal-0.4.0 (c (n "mal") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1kmywm3136hcik1bkhq7wrqh6yv7naaywngbhgc7h5hapf5z83qb") (f (quote (("manga-list") ("default" "anime-list" "manga-list") ("anime-list"))))))

(define-public crate-mal-0.4.1 (c (n "mal") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "10i0xqndg6g39jws6jljcacx1nd10rb53r8ycl30dba4zrcrmd2r") (f (quote (("manga-list") ("default" "anime-list" "manga-list") ("anime-list"))))))

(define-public crate-mal-0.5.0 (c (n "mal") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0c1rvb3wqdj7k88xlsf8phh99f1nyxddp3pknyvkby87gvpxny6h") (f (quote (("manga-list") ("default" "anime-list" "manga-list") ("anime-list"))))))

(define-public crate-mal-0.6.0 (c (n "mal") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0rin76vqkx0qkpksjbp6izxx9b7ys1pvx22gzmxim5fqk16h6mvg") (f (quote (("manga") ("default" "anime" "manga") ("anime"))))))

(define-public crate-mal-0.7.0 (c (n "mal") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "0fm0f266kb2r54v5gnnmdiph44hznzjhs1z19cixnjsjzay0zls2") (f (quote (("manga") ("default" "anime" "manga") ("anime"))))))

(define-public crate-mal-0.8.0 (c (n "mal") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "1lcaqvx5nksng40j73vydxs55w9ak1c14av504g1dfwhscj4mdf4") (f (quote (("manga") ("default" "anime" "manga") ("anime"))))))

(define-public crate-mal-0.8.1 (c (n "mal") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "minidom") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)))) (h "13sh2khryams1d4s5rqqbyazi1c5kfd3l7msslbwvxh4wja2nkaf") (f (quote (("manga") ("default" "anime" "manga") ("anime"))))))

