(define-module (crates-io #{3}# m mbp) #:use-module (crates-io))

(define-public crate-mbp-1.0.0 (c (n "mbp") (v "1.0.0") (h "16k3gimi9dqvfih7ba5sviq6q9749f6jkqir1klcfh0vnf8hzmmb")))

(define-public crate-mbp-1.1.0 (c (n "mbp") (v "1.1.0") (d (list (d (n "rust_sfp") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1a0yaff2qairqijhr99x54cy8kvv0gazrv0gdpq0xdxr9d0l0vr7") (f (quote (("net" "rust_sfp") ("all" "net"))))))

(define-public crate-mbp-1.2.0 (c (n "mbp") (v "1.2.0") (d (list (d (n "rust_sfp") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1hh1yjjf0dpm44aqk3h1gxhf5b5hznirrr4rqz669qg9k1glfbv1") (f (quote (("net" "rust_sfp") ("all" "net"))))))

(define-public crate-mbp-1.3.0 (c (n "mbp") (v "1.3.0") (d (list (d (n "rust_sfp") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0m8sm6v58c9g51xc8h89y8j4r2g1gzvy052nar3k1b6dvnq3g938") (f (quote (("net" "rust_sfp") ("all" "net"))))))

(define-public crate-mbp-2.0.0 (c (n "mbp") (v "2.0.0") (d (list (d (n "rust_sfp") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1pc2b61p95f72l18834cz7012v7pdvbkh7gkhdpnx36c433mi0hp") (f (quote (("net" "rust_sfp") ("all" "net"))))))

