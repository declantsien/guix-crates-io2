(define-module (crates-io #{3}# m mpv) #:use-module (crates-io))

(define-public crate-mpv-0.1.0 (c (n "mpv") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "sdl2") (r "^0.16") (d #t) (k 2)) (d (n "sdl2-sys") (r "^0.8") (d #t) (k 2)))) (h "16c7x3pj431d7dlm0kr93fyfcb7nh6b46v6934mavr3d6q09ajn3")))

(define-public crate-mpv-0.2.0 (c (n "mpv") (v "0.2.0") (d (list (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "sdl2") (r "^0.16") (d #t) (k 2)) (d (n "sdl2-sys") (r "^0.8") (d #t) (k 2)))) (h "0cywm52xkiml6jyx21kbqv1i1jiz7vpwbx5ndx9qzgdn1pmsxim6")))

(define-public crate-mpv-0.2.1 (c (n "mpv") (v "0.2.1") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.20") (d #t) (k 2)) (d (n "sdl2-sys") (r "^0.19") (d #t) (k 2)))) (h "1bq5ninysbj4rrqp09xj9h09lcin71cdi29cfk5mbwkg0mb0lr5l")))

(define-public crate-mpv-0.2.2 (c (n "mpv") (v "0.2.2") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.20") (d #t) (k 2)) (d (n "sdl2-sys") (r "^0.19") (d #t) (k 2)))) (h "0p6qp7lp0xdx7n6a53n7bx2nwj6zmcb8gqmnk3ag042ygy6nvdb9")))

(define-public crate-mpv-0.2.3 (c (n "mpv") (v "0.2.3") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.27") (d #t) (k 2)) (d (n "sdl2-sys") (r "^0.27") (d #t) (k 2)))) (h "10kg4c4n7i9hcfaxy7i1mym5mmcfyyq598dqm9mfzfsm8sagsmwy")))

