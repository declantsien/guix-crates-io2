(define-module (crates-io #{3}# m mvt) #:use-module (crates-io))

(define-public crate-mvt-0.1.1 (c (n "mvt") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "1li79rfw7hp5lfhanh8zdwazb8va8yv8mfh2628ymww49l3xawza") (y #t)))

(define-public crate-mvt-0.1.2 (c (n "mvt") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "1hjvcanjsi8gd70lryrdrv8q5qcp578nl6hbca4m8zyh5wcsxaxy")))

(define-public crate-mvt-0.2.0 (c (n "mvt") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "018byzsybdw4yyvrvni82k6rpn9pn84va6bhvq9zmic47lgcq3r3")))

(define-public crate-mvt-0.3.0 (c (n "mvt") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "1b9h23nijkma26y1kwpn5fv5sq9y7sb9wsnvachv8acznaa483lm")))

(define-public crate-mvt-0.4.0 (c (n "mvt") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "00yr3cgwq603v737gfs9z1ibxd66hpw0m0j3ss5p9m54kxhyjlhh")))

(define-public crate-mvt-0.5.0 (c (n "mvt") (v "0.5.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "1w3i47j7yb08dyhiwqacwya0nih0fs7m8bkxyjkqdrhiphbr2ffs")))

(define-public crate-mvt-0.5.1 (c (n "mvt") (v "0.5.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "07w308kmmj0g3gpy6kdhkl72h09i3xk89jx4638mcymyv5547mnn")))

(define-public crate-mvt-0.5.2 (c (n "mvt") (v "0.5.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.0") (d #t) (k 0)))) (h "18p8dxy62m24v8knqqh5gn7aj7pysa8l0d2gl84rsd34jnbjscr0")))

(define-public crate-mvt-0.5.3 (c (n "mvt") (v "0.5.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^2.8") (d #t) (k 0)))) (h "0p7fiwxq7nap31ckn3k48y0nryzad9vcr9wf9p7jfs8bgwdv0dyv")))

(define-public crate-mvt-0.5.4 (c (n "mvt") (v "0.5.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "~2.17") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.17") (o #t) (d #t) (k 0)))) (h "1z3rdhwi6fqnwq8jdyqnzg3gh92mznfbby0y78kknj2mfbqlajsl") (f (quote (("update" "protobuf-codegen-pure"))))))

(define-public crate-mvt-0.6.0 (c (n "mvt") (v "0.6.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "~2.17") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.17") (o #t) (d #t) (k 0)))) (h "05vxflwfg8fwshz8lv1zldnk7nsb7rbgn8xcwigca1nzs7piryh3") (f (quote (("update" "protobuf-codegen-pure"))))))

(define-public crate-mvt-0.7.0 (c (n "mvt") (v "0.7.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pointy") (r "^0.2") (d #t) (k 0)) (d (n "protobuf") (r "~2.17") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.17") (o #t) (d #t) (k 0)))) (h "135c1mm27kv2cdjynaajq7vcvmd52bh0qpj0a3bycc97bf4h6x0k") (f (quote (("update" "protobuf-codegen-pure"))))))

(define-public crate-mvt-0.8.0 (c (n "mvt") (v "0.8.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pointy") (r "^0.3") (d #t) (k 0)) (d (n "protobuf") (r "~3.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "00jbg37ph39x6b4r3ashrk4n0my6j9z66ws8glw3idnbvkzq8hj6") (f (quote (("update" "protobuf-codegen")))) (y #t)))

(define-public crate-mvt-0.8.1 (c (n "mvt") (v "0.8.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pointy") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "~3.2") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1il76i5lvyh9gcrnfp9pjdyjy43shbbw5ma7yzzv688z4x5qcnsn") (f (quote (("update" "protobuf-codegen"))))))

(define-public crate-mvt-0.9.0 (c (n "mvt") (v "0.9.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pointy") (r "^0.5") (d #t) (k 0)) (d (n "protobuf") (r "~3.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k83g37i15nyir1qx9axkl68vyvijy2vvfj4wpl6agl459qg1bpi") (f (quote (("update" "protobuf-codegen"))))))

(define-public crate-mvt-0.9.1 (c (n "mvt") (v "0.9.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pointy") (r "^0.5") (d #t) (k 0)) (d (n "protobuf") (r "~3.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k0dr1896agy9c7ph64ay4rcafv39i3xby8srnvwv6ifdlzbyjh1") (f (quote (("update" "protobuf-codegen"))))))

(define-public crate-mvt-0.9.2 (c (n "mvt") (v "0.9.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pointy") (r "^0.6") (d #t) (k 0)) (d (n "protobuf") (r "~3.3") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^3.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02a8mjzl9396azmh7pfvg2cav57izj54pmilixanvj0f0ygpi16y") (f (quote (("update" "protobuf-codegen"))))))

