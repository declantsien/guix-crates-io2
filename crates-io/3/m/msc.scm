(define-module (crates-io #{3}# m msc) #:use-module (crates-io))

(define-public crate-msc-0.1.0 (c (n "msc") (v "0.1.0") (h "12a30yhrgb3yd7b7lg8zf8vm4i3raq317dqv9vj0pk5jqgdq13al")))

(define-public crate-msc-0.5.0 (c (n "msc") (v "0.5.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "197bvp4kk4j6sl13fy3kn9rikc9vppf75a9iq41b53cg8g40rhpc")))

(define-public crate-msc-0.5.1 (c (n "msc") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1rkv4912606bl22kjyvai3gwn765mkav870g0yy2d905dn5vnzcs")))

(define-public crate-msc-0.5.2 (c (n "msc") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "16mi5b0ysx1r71drf6h3knprndw77p0yz9xvdp88cfzl8f4miq9q")))

(define-public crate-msc-0.5.3 (c (n "msc") (v "0.5.3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1xmcnwg82higrvnh7fdwypmh8iis3w56fs2psq0nva22mn9qb3rb")))

(define-public crate-msc-0.5.4 (c (n "msc") (v "0.5.4") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "00ss71mskj74c6cab6xhdwx3wcls127fxski0h578hhinnn65j0p")))

