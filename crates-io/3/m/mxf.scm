(define-module (crates-io #{3}# m mxf) #:use-module (crates-io))

(define-public crate-mxf-0.1.0 (c (n "mxf") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "03sakbf9ny0jfzpcg78kvc515jkxaljq7jaqakzp50cxi1gwzydf")))

(define-public crate-mxf-0.1.1 (c (n "mxf") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0s9ka7y9z1dh6k5l14frmh65qmz5abwc3vr54p8a6wp64m809h8x")))

(define-public crate-mxf-0.1.2 (c (n "mxf") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "01vas048cn2mzjb6a7bncdw430q81lkj34w7fd4m1nb0w7fn0pzj")))

