(define-module (crates-io #{3}# m mda) #:use-module (crates-io))

(define-public crate-mda-0.1.0 (c (n "mda") (v "0.1.0") (d (list (d (n "charset") (r "^0.1") (d #t) (k 0)) (d (n "gethostname") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0zs2ggbl9kakh0lz2sa2nsj3diid3byj72z6600ipdz07cd23w97")))

(define-public crate-mda-0.1.1 (c (n "mda") (v "0.1.1") (d (list (d (n "charset") (r "^0.1") (d #t) (k 0)) (d (n "gethostname") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0i86gcvy6hzzypphma4qa62ysbyp8cs913rha3b2pj4zwfix6cw8")))

