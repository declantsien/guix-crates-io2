(define-module (crates-io #{3}# m mom) #:use-module (crates-io))

(define-public crate-mom-0.1.0-alpha (c (n "mom") (v "0.1.0-alpha") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16gqygv9b69dy8zdwpshdqlx4414qlzdlzfvdbjnbqg7fnzpbazd")))

