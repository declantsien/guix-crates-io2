(define-module (crates-io #{3}# m msp) #:use-module (crates-io))

(define-public crate-msp-0.1.0 (c (n "msp") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cla61pvqxqz9b5jb8833pkkj63lbiixv955z83rygd6jq6wscb8") (y #t)))

(define-public crate-msp-0.1.1 (c (n "msp") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hx9zmbdx5l3c7r5cp5fnh2gvrfm8ng0h5b7920xm6b5ayyh2kik") (y #t)))

(define-public crate-msp-0.1.2 (c (n "msp") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1iy85svh14h4l843vbjyzlcd5xh4hc8abw8l5iazbwqn788f945x")))

