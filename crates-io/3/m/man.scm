(define-module (crates-io #{3}# m man) #:use-module (crates-io))

(define-public crate-man-0.0.0 (c (n "man") (v "0.0.0") (h "0xi8rj570imn4wpchl6zpyj5v0xh24xdpl74vx67c8s15411k0ir")))

(define-public crate-man-0.1.0 (c (n "man") (v "0.1.0") (d (list (d (n "roff") (r "^0.1.0") (d #t) (k 0)))) (h "1js9lyhgs16pkxba46q5z00mnmsjlzi0fvs1q00khx38gjck93rw")))

(define-public crate-man-0.1.1 (c (n "man") (v "0.1.1") (d (list (d (n "roff") (r "^0.1.0") (d #t) (k 0)))) (h "0hm9ynqngpcvqkah3qzki3ks333gpp6gfwm0z7pcmjx34gbb3fyc")))

(define-public crate-man-0.2.0 (c (n "man") (v "0.2.0") (d (list (d (n "roff") (r "^0.1.0") (d #t) (k 0)))) (h "1ahjkf2cvi7gwh7nf974bn46v04y1rb7z6v2xl2vyig1z1nh4dxw")))

(define-public crate-man-0.3.0 (c (n "man") (v "0.3.0") (d (list (d (n "roff") (r "^0.1.0") (d #t) (k 0)))) (h "1jd103brl70sh1hxm2w3n6z3pzazrznsl45cn53h3a47a5wzmxgb")))

