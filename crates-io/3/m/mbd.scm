(define-module (crates-io #{3}# m mbd) #:use-module (crates-io))

(define-public crate-mbd-0.0.1 (c (n "mbd") (v "0.0.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.23") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0ipl9xh2h9g1jdx9chbg2dzvvi52djyh3m0jv8cq9v2r25s3l3f5") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-mbd-0.0.4 (c (n "mbd") (v "0.0.4") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.23") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1b2phag48prgc31fgglxjnfmippb8sjhd73kn9gqlj48bb7sl1vn") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-mbd-0.1.0 (c (n "mbd") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.23") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0cbicjyv8bd1vq2x7k0naiaaaphjvrh48gqn2qhh4wivvywp2xiy") (f (quote (("default" "console_error_panic_hook"))))))

(define-public crate-mbd-0.1.1 (c (n "mbd") (v "0.1.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.73") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.23") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "001l916skshbai5adsjv3l0cr64qnj66hfigl3nfkvw74nbw1v8k") (f (quote (("default" "console_error_panic_hook"))))))

