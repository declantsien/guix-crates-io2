(define-module (crates-io #{3}# m mnl) #:use-module (crates-io))

(define-public crate-mnl-0.1.0 (c (n "mnl") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mnl-sys") (r "^0.1") (d #t) (k 0)))) (h "1xxqz3jz4x5j1hm3k72q8qinn5qk1dcmwci7c7fr0w3i6d5mdpwx") (f (quote (("mnl-1-0-4" "mnl-sys/mnl-1-0-4"))))))

(define-public crate-mnl-0.2.0 (c (n "mnl") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mnl-sys") (r "^0.2") (d #t) (k 0)))) (h "09p26grjxi425wm359iv8x5fqc55gy2bjiz8ww9d3b6v905bbvz6") (f (quote (("mnl-1-0-4" "mnl-sys/mnl-1-0-4"))))))

(define-public crate-mnl-0.2.1 (c (n "mnl") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "mnl-sys") (r "^0.2") (d #t) (k 0)))) (h "1apsh23ckcx3gssk0dy57da63dsdnsfcir1hixiskkfrnax7b2w3") (f (quote (("mnl-1-0-4" "mnl-sys/mnl-1-0-4"))))))

(define-public crate-mnl-0.2.2 (c (n "mnl") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "log") (r "^0.4.4") (d #t) (k 0)) (d (n "mnl-sys") (r "^0.2.1") (d #t) (k 0)))) (h "1vc1hv63bv6x7608bpbrd1mv5qyf1i69cmxj7f0y34ys62b4d9fi") (f (quote (("mnl-1-0-4" "mnl-sys/mnl-1-0-4"))))))

