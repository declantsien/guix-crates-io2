(define-module (crates-io #{3}# m mkv) #:use-module (crates-io))

(define-public crate-mkv-0.0.1 (c (n "mkv") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1kx5in0lljl1p1rzng7rhlfxjkl0cd2j8vim6igmavz4c9hlklps")))

(define-public crate-mkv-0.0.3 (c (n "mkv") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0qfl98cb5i1dc4qyxyzifkxvmmqby70kq3rypwk1xrgraylyfg7v")))

(define-public crate-mkv-0.0.4 (c (n "mkv") (v "0.0.4") (d (list (d (n "byteorder") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1nxgdkqwbzbaxyy5fszj5a5fgg1b120fbyavanfi1r631dy3f2gw")))

(define-public crate-mkv-0.0.5 (c (n "mkv") (v "0.0.5") (d (list (d (n "byteorder") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0wbqxvqhwndri0s0dwxk879fz2gqmz9rjwmsnhxxlsh0h4svaq40")))

(define-public crate-mkv-0.0.6 (c (n "mkv") (v "0.0.6") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1ds5ksza5jjw1vjj4km1h4f3qjajjcl6w685i4ixdmsjq542hshq") (f (quote (("nightly"))))))

(define-public crate-mkv-0.0.7 (c (n "mkv") (v "0.0.7") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "15n67iaxx08kb92dwza9y841g767rhlqqi59lyv37zbz36crlnnh") (f (quote (("nightly"))))))

(define-public crate-mkv-0.0.8 (c (n "mkv") (v "0.0.8") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "random") (r "^0.12") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1djva5qvdvd6ds6nygcdk3mqlqx9h3lp389av42744i5nf3iw9mx") (f (quote (("nightly"))))))

