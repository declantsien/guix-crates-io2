(define-module (crates-io #{3}# m mem) #:use-module (crates-io))

(define-public crate-mem-0.4.0 (c (n "mem") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "136fmiw3asqija1j4ri1zgwr0s4z9svbqq6cm3va1ig1dkgwpmmc")))

(define-public crate-mem-0.4.1 (c (n "mem") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "04wv3sxfgrvw4pjghk47kzpv5via4l1j8n0rlcz6ggawsgbclgdl")))

(define-public crate-mem-0.5.0 (c (n "mem") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)))) (h "0qdd44mbmjarl7d4lximdisx0kw4an0fna0yc6n0xr70m1cn22xm")))

