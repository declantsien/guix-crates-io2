(define-module (crates-io #{3}# m mag) #:use-module (crates-io))

(define-public crate-mag-0.1.0 (c (n "mag") (v "0.1.0") (h "13mpp8j03bylrwfwrcj9nip75z13rzbvfdi05mimf3pjmfvgli87")))

(define-public crate-mag-0.2.0 (c (n "mag") (v "0.2.0") (h "0qkmyndqy6dmpprx57ran33d7rr28sdrr4kmnb3r9k5h1nlcdpf6") (f (quote (("obscure-units") ("default"))))))

(define-public crate-mag-0.3.0 (c (n "mag") (v "0.3.0") (h "0lwibmj0fd56h18zjpa8cv7qa01345fskgqqqx202n87jhqy0byr") (f (quote (("obscure-units") ("default"))))))

(define-public crate-mag-0.4.0 (c (n "mag") (v "0.4.0") (h "1n9c35504nb68bs8yzif2qsijh46in7ksmav0cpxdiy0lhc1rd7c")))

(define-public crate-mag-0.5.0 (c (n "mag") (v "0.5.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)))) (h "1fzyngn66ni3yswmi9fa73bn381n31djaikzqvfhlg8l2lzpjixj")))

