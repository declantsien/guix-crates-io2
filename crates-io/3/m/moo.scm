(define-module (crates-io #{3}# m moo) #:use-module (crates-io))

(define-public crate-moo-0.1.0 (c (n "moo") (v "0.1.0") (h "0miyzfjgxcvl6l9ksm2d10zcbbvi999wnzqsr2mjrfqrd97xy2rs")))

(define-public crate-moo-0.1.1 (c (n "moo") (v "0.1.1") (h "06g082svi8k3i19jhxbdnbm1w7m26zssdzjhp82241a7kwivvgqh")))

