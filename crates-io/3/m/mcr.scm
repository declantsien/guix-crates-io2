(define-module (crates-io #{3}# m mcr) #:use-module (crates-io))

(define-public crate-mcr-0.1.0 (c (n "mcr") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "color"))) (k 0)) (d (n "extendhash") (r "^1.0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "09z576gdpn1m47sghg3m3wrsf21ya22h6qk74ppjkymzsnm38y8p") (y #t)))

(define-public crate-mcr-0.1.1 (c (n "mcr") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0vlrrkpj27mmxfb0v2s778cv9z4vrlip8qg0hdl3palm3890bcdw") (y #t)))

(define-public crate-mcr-0.1.2 (c (n "mcr") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0c224a938izmkvpbv8yjg2z5vm54rq0b6wmyv5720xqafn2hfm82") (y #t)))

(define-public crate-mcr-0.1.3 (c (n "mcr") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0vfdxif2wx1hbqc3zwlv9qvjjvglrhx286b6v7mrpprwglr2g2y4") (y #t)))

(define-public crate-mcr-0.1.4 (c (n "mcr") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0r57pfsqk2l1lq2j1i7vpblvf0h9hmli9h667sv2cg52fw4hwq6d") (y #t)))

(define-public crate-mcr-0.1.5 (c (n "mcr") (v "0.1.5") (d (list (d (n "clap") (r "^2.33") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1sfxvqfd6f6l2xgb6ljvjnymmcppf1n9fpq36s3bl95m58yxhcdx") (y #t)))

(define-public crate-mcr-0.1.6 (c (n "mcr") (v "0.1.6") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0dyz3qqg3fs8lq4ixc600357rpvqyrwr5vkm0kj9xskpmrzahki4") (y #t)))

(define-public crate-mcr-0.1.7 (c (n "mcr") (v "0.1.7") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1mxrxjyzi48hfvp091mhxvz29d5sh9fwb6jmhrryi0x4k3s99cql") (y #t)))

(define-public crate-mcr-0.2.0 (c (n "mcr") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "09l8j5r8n8jjdzdcn10qfm0ayvsp41lqgn8gps4k28n19d5jdjza") (y #t)))

(define-public crate-mcr-0.2.1 (c (n "mcr") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "07az7vlhr71wqblb5j6bp6bl9nn5ld7aszg14v19401h8la27j6m") (y #t)))

(define-public crate-mcr-0.2.2 (c (n "mcr") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0qk11yxllzbq42la038p5qc59nx9fyc0xybfgbcqw25jiai3glg0") (y #t)))

(define-public crate-mcr-0.2.3 (c (n "mcr") (v "0.2.3") (d (list (d (n "clap") (r "^2.33") (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0m04sxpirl1kyydjqcmafpnjp3v7pjqdh8951m81cz2i2qj80xzs") (y #t)))

