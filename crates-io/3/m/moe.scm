(define-module (crates-io #{3}# m moe) #:use-module (crates-io))

(define-public crate-moe-0.1.1 (c (n "moe") (v "0.1.1") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "mockito") (r "^0.20.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.19") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snafu") (r "^0.4.4") (d #t) (k 0)))) (h "1lax0baqzdxv5x90f1wajvqcijdwiq43j66njglgh2ihf556w9by")))

