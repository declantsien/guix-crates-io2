(define-module (crates-io #{3}# m maa) #:use-module (crates-io))

(define-public crate-maa-0.1.0 (c (n "maa") (v "0.1.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c6yn76qg1i18h7qrg1smamn97zh0gymgwk63l5jchkh7v02ix11")))

(define-public crate-maa-0.2.0 (c (n "maa") (v "0.2.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1impqnfhnh464xmnshs5srz6z9a4r5xgdw0jaz8i3wjc1da9zgip")))

(define-public crate-maa-0.3.0 (c (n "maa") (v "0.3.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0d47k8j05h76j1618qx6pbbnnr9nnnrg8lnzvnxvhz51j94yzwl2")))

