(define-module (crates-io #{3}# m mdg) #:use-module (crates-io))

(define-public crate-mdg-0.0.1 (c (n "mdg") (v "0.0.1") (d (list (d (n "cclm") (r "^0.0.1") (d #t) (k 0)) (d (n "cjwt") (r "^0.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)))) (h "0gdkps9h46gfnl4jaywaaxrszkmazzyvipk2fih94b1dy97qwr5k") (f (quote (("default")))) (r "1.67")))

