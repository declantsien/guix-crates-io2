(define-module (crates-io #{3}# m mbc) #:use-module (crates-io))

(define-public crate-mbc-0.1.0 (c (n "mbc") (v "0.1.0") (d (list (d (n "anyhow") (r "~1") (d #t) (k 0)) (d (n "async-trait") (r "~0.1") (d #t) (k 0)) (d (n "byteorder") (r "~1") (d #t) (k 0)) (d (n "clap") (r "~4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "http") (r "~0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "tokio") (r "~1.21") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "~0") (f (quote ("rtu" "tcp"))) (k 0)) (d (n "tokio-serial") (r "~5.4") (d #t) (k 0)))) (h "0f2p1skggx8sfjg4cd2sdy51z15005ybq87025r8ps3sxbbl59al")))

