(define-module (crates-io #{3}# m mic) #:use-module (crates-io))

(define-public crate-mic-0.1.0 (c (n "mic") (v "0.1.0") (d (list (d (n "either") (r "=1.5.3") (d #t) (k 2)) (d (n "mic_impl") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "=1.0.9") (d #t) (k 2)) (d (n "proconio") (r "^0.3.8") (d #t) (k 2)) (d (n "quote") (r "=1.0.3") (d #t) (k 2)) (d (n "syn") (r "=1.0.17") (d #t) (k 2)) (d (n "unicode-xid") (r "=0.2.0") (d #t) (k 2)))) (h "035lb1z5jxmrrl49zzv2i9jpyl4mm7h3j9xsv9sy039pr9s9lqql")))

