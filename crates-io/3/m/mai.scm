(define-module (crates-io #{3}# m mai) #:use-module (crates-io))

(define-public crate-mai-0.1.0 (c (n "mai") (v "0.1.0") (d (list (d (n "lifeguard") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "slab") (r "^0.1") (d #t) (k 0)))) (h "0pcgw5k1358c0xs4d87fyvdchvwsyakc0bkc1ky4jbnafa9wgani")))

