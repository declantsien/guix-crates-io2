(define-module (crates-io #{3}# m mli) #:use-module (crates-io))

(define-public crate-mli-0.9.0 (c (n "mli") (v "0.9.0") (h "165lcdrrksv2xanm8b2mqwbfldrkhxdk2mnwvky8aknr20k4ch9h")))

(define-public crate-mli-0.9.1 (c (n "mli") (v "0.9.1") (h "0m37bwc824jwc8b3fgs72bp1gzqpwsb5a858xqxyk4kidfqd1az9")))

(define-public crate-mli-0.10.0 (c (n "mli") (v "0.10.0") (h "05n1sxjk0q3k6aqsyn6f2cvc2zj62a549w53wrfplfmpzlqmzmf5")))

(define-public crate-mli-0.11.0 (c (n "mli") (v "0.11.0") (h "1n98qcmj1j7mvd5wc6z27kyxmx0gc7jbpwadg38gd9jnk8dbs0d0")))

