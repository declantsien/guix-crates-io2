(define-module (crates-io #{3}# m mer) #:use-module (crates-io))

(define-public crate-mer-0.1.0 (c (n "mer") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "scroll") (r "^0.9") (d #t) (k 0)) (d (n "scroll_derive") (r "^0.9") (d #t) (k 0)))) (h "0pg5bply83wy6j5s6lzs9iv02c9siv74h9dbi9fpd3iz4ysp2mff")))

(define-public crate-mer-0.1.1 (c (n "mer") (v "0.1.1") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "scroll") (r "^0.9") (f (quote ("derive"))) (k 0)) (d (n "scroll_derive") (r "^0.9") (d #t) (k 0)))) (h "0lmc8l04i0b5ih0z9j7cwsar5i7v40942grdq360vnmq5bb8jmjp")))

(define-public crate-mer-0.2.0 (c (n "mer") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "scroll") (r "^0.9") (f (quote ("derive"))) (k 0)) (d (n "scroll_derive") (r "^0.9") (d #t) (k 0)))) (h "1zasmfz4s5n9czmd4mgdykpb3wq1r30b05kc76nlnqxzzhz4gq4g")))

(define-public crate-mer-0.3.0 (c (n "mer") (v "0.3.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "scroll") (r "^0.9") (f (quote ("derive"))) (k 0)) (d (n "scroll_derive") (r "^0.9") (d #t) (k 0)))) (h "1gdqqh3ik7rmqv8987bslya5j66xqmw33bfqv6lgpygcfw6vahpd")))

(define-public crate-mer-0.3.1 (c (n "mer") (v "0.3.1") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "scroll") (r "^0.9") (f (quote ("derive"))) (k 0)) (d (n "scroll_derive") (r "^0.9") (d #t) (k 0)))) (h "166bjlbn8m2lr6wb5h03c5g2xnznsi3vpg4qgqd05dsr7nyl9nhd")))

(define-public crate-mer-0.4.0 (c (n "mer") (v "0.4.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (f (quote ("derive"))) (k 0)) (d (n "scroll_derive") (r "^0.10") (d #t) (k 0)))) (h "12iq2r8qa2484ha0vpli8xjm75daa5s4730d9khdgrwfy6r1y67s")))

