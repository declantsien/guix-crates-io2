(define-module (crates-io #{3}# m mim) #:use-module (crates-io))

(define-public crate-mim-0.1.0 (c (n "mim") (v "0.1.0") (d (list (d (n "byteorder") (r "1.4.*") (d #t) (k 0)) (d (n "hex") (r "0.4.*") (d #t) (k 0)) (d (n "hkdf") (r "0.12.*") (d #t) (k 0)) (d (n "sha2") (r "0.10.*") (d #t) (k 0)))) (h "1xy308bjhs54vgk3q261ynpx5qknvs9d2y4k2yysly9456njrvsl")))

