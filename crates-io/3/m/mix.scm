(define-module (crates-io #{3}# m mix) #:use-module (crates-io))

(define-public crate-mix-0.1.0 (c (n "mix") (v "0.1.0") (d (list (d (n "markdown-it") (r "^0.6.0") (f (quote ("linkify" "syntect"))) (d #t) (k 0)) (d (n "minimo") (r "^0.3.1") (d #t) (k 0)))) (h "18q1ks219s3icrk9hizsdikns9ckjh5inzl673ix05y1dqpxrx8q")))

(define-public crate-mix-0.1.1 (c (n "mix") (v "0.1.1") (d (list (d (n "markdown-it") (r "^0.6.0") (f (quote ("linkify" "syntect"))) (d #t) (k 0)) (d (n "minimo") (r "^0.3.1") (d #t) (k 0)))) (h "0n2lm9qw23lx10my08p7s9v01x8hipiwxfn2ki856pgkj6p9cbpd")))

