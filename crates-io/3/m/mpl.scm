(define-module (crates-io #{3}# m mpl) #:use-module (crates-io))

(define-public crate-mpl-0.1.0 (c (n "mpl") (v "0.1.0") (h "1z6x8bziifzs9pki1vyymzp168hf2lv05hbmjgdjxvcl4hsic2gc")))

(define-public crate-mpl-0.2.0 (c (n "mpl") (v "0.2.0") (h "07irvkz30l59cbyqm4wanb3f23xi41b9b24997mija8hay2ghnsg")))

