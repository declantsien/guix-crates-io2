(define-module (crates-io #{3}# m mpc) #:use-module (crates-io))

(define-public crate-mpc-0.1.2 (c (n "mpc") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1hjmq8ssy3z36nc2fx3gx43dh5by4dgmw41w0nk8mzpdi6c0kvrz")))

(define-public crate-mpc-0.1.3 (c (n "mpc") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1wjvpzq9q03kq0ziw6dgvlz1mx0l1yjxz2zd47y1g3ia5jih53wh")))

(define-public crate-mpc-0.1.4 (c (n "mpc") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1k9w232awmdqyp8v9j1h98xr8lmry87f2azd142850n8j7a331c5")))

(define-public crate-mpc-0.1.5 (c (n "mpc") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0jswmlg39ih40v83ppn4daqi9nd7mqqnm0hwlyghgb1nnnbzd54y")))

(define-public crate-mpc-0.1.6 (c (n "mpc") (v "0.1.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0w31f275vry4y682qbixlbqb57la0x9yf6r5hcxf5nc8i3cpxg8i")))

(define-public crate-mpc-0.1.7 (c (n "mpc") (v "0.1.7") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0ps31898a4mbrxzs4i2i00mcjbvwckhhlbbcrcw2vs4wsmmr1109")))

(define-public crate-mpc-0.1.8 (c (n "mpc") (v "0.1.8") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0frx2mlk0paghpmaz903q4a60hq7wwnqmq005rq9ci7bxf7c3hja")))

(define-public crate-mpc-0.1.9 (c (n "mpc") (v "0.1.9") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0wyg5di5zqcfywv9n3s8yiqc3prf0dl186lb010z19gsspbr63s9")))

(define-public crate-mpc-0.1.10 (c (n "mpc") (v "0.1.10") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1k6d25z8pv6bla97rm6ixgi35xmja4gakbvyc8krj4ffd5114hb4")))

