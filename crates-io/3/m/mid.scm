(define-module (crates-io #{3}# m mid) #:use-module (crates-io))

(define-public crate-mid-1.0.0 (c (n "mid") (v "1.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "02ffrgrz1xrry0bz66qj4cvgdi6fqlj9dpgjnsvd92gw4ckjgbx9")))

(define-public crate-mid-1.0.1 (c (n "mid") (v "1.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "0br4r3ky043phlnxx0rr86x58kaajvnf5q1flqya72i7lz621798")))

(define-public crate-mid-1.1.0 (c (n "mid") (v "1.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1c1wwxl0ckcp57ivqmf6vx16nkhzgd9a1d1r2mc3ckin4s20qmza")))

(define-public crate-mid-1.1.1 (c (n "mid") (v "1.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "ring") (r "^0.17.5") (d #t) (k 0)))) (h "1pnpldpw19x8kc00b3lv1zqrcamss9sg2i32sc9xw01gg6rh4zqr")))

(define-public crate-mid-1.1.2 (c (n "mid") (v "1.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 0)))) (h "1018199q0gr15f0z8bcs99rn21kzc6wh30fl2zvzw6fdahskpbpy")))

(define-public crate-mid-1.1.3 (c (n "mid") (v "1.1.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 0)))) (h "16iajqv7n8xxhw0fcnz058q4b607cbbbj700iss0cvr7fkjfwa3h")))

(define-public crate-mid-1.1.4 (c (n "mid") (v "1.1.4") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 0)))) (h "09jirqhnqxhfzxa9vnkgkaklkwnyqzwamn5vi547w8d99574qdq1")))

(define-public crate-mid-1.1.5 (c (n "mid") (v "1.1.5") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 0)))) (h "1klpmdifhlmcgy1gf8hvmhfypl19jg2h2yzq4j1vvba32mabzn4g")))

(define-public crate-mid-1.2.0 (c (n "mid") (v "1.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 0)))) (h "0knn1kbg7cwwc7hv01jqh9vm4ayrxdcgv7i91y0z5zpgn0kv9pn8")))

(define-public crate-mid-2.0.0 (c (n "mid") (v "2.0.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac-sha256") (r "^1.1.7") (d #t) (k 0)))) (h "0yc0v9is7ig6b92h44qzxac46z5sl1aqnwvs9pynigqh69cpjhxr")))

