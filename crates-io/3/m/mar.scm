(define-module (crates-io #{3}# m mar) #:use-module (crates-io))

(define-public crate-mar-0.1.0 (c (n "mar") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "1pz7ic4bnrf5gwb5r4220h1z6jvg4z6swlqs99vn276m7sb0x60d")))

(define-public crate-mar-0.1.1 (c (n "mar") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)))) (h "1d6c0sy3x4i7x89z8cq8yy5hxmvdbp03ihhyiwhs0dj67j1v0rdn")))

(define-public crate-mar-0.2.0 (c (n "mar") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "xz") (r "^0.1.0") (d #t) (k 0)))) (h "1wra8mmc2h38l6mhakfh0mrw654f3rd3d0js801zxycbzz5hdssh")))

