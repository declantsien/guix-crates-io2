(define-module (crates-io #{3}# m mqf) #:use-module (crates-io))

(define-public crate-mqf-1.0.0 (c (n "mqf") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0bvyyjzp6c054hz7r5im0zysihi9m379fqm7zydrf12gcnp6iv7l") (l "libmqf")))

