(define-module (crates-io #{3}# m md5) #:use-module (crates-io))

(define-public crate-md5-0.1.0 (c (n "md5") (v "0.1.0") (h "0xh6fqd0kwi2y211w118jviymd20kw45iafvb7y2a5722vcmd9y0")))

(define-public crate-md5-0.1.1 (c (n "md5") (v "0.1.1") (h "1nw8k2z7d9fmqvnrzkw45j7mlhwbhdws61n4r44f6jlvxs6rllx5")))

(define-public crate-md5-0.2.0 (c (n "md5") (v "0.2.0") (h "0mk2lwa0c2mgfa2jcda4rziy7fdhpg24vx5d79znbkfahj4lsf7m")))

(define-public crate-md5-0.2.1 (c (n "md5") (v "0.2.1") (h "1pa9n8dqw2vzj137haahrq9cjr7ak12648gcnjrxdzyd7j831wkx")))

(define-public crate-md5-0.3.0 (c (n "md5") (v "0.3.0") (h "0cx9wgyfgl85w50jb2wkbgms2m17yx0pzkj0xib64b1vqy4bnz6n")))

(define-public crate-md5-0.3.1 (c (n "md5") (v "0.3.1") (h "1qdah95c7bkd0jlqrwfvwjbnblfi14pf5p8ya8j6sr4qh8zg5yn0")))

(define-public crate-md5-0.3.2 (c (n "md5") (v "0.3.2") (h "0bc1ws179bmysjj8xivadk9plh8d4wamvb58acihm7ipy61mmkkg")))

(define-public crate-md5-0.3.3 (c (n "md5") (v "0.3.3") (h "0ixkm1zkn3fy4c90bnpq9s0zgvvjy21yymbp0lzf666z63m6r58m")))

(define-public crate-md5-0.3.4 (c (n "md5") (v "0.3.4") (h "05b5szr8bgq845j1d5kgxg7phs0jrmvvxijf1yb79l36f58nq496")))

(define-public crate-md5-0.3.5 (c (n "md5") (v "0.3.5") (h "10caypdjrjg67rbrcgcy6zgr12dgbqmfkqhqc710dlsp10jg7whi")))

(define-public crate-md5-0.3.6 (c (n "md5") (v "0.3.6") (h "0g52ap70s8vf42vf1qchd13kgm63lh6pzyjwknsm03slisssmndn")))

(define-public crate-md5-0.3.7 (c (n "md5") (v "0.3.7") (h "1mw92b1dm8irh25wr3v359xclc2yc7zgqgyqm7axqv7p6d3018fs")))

(define-public crate-md5-0.3.8 (c (n "md5") (v "0.3.8") (h "0j2s8aqdkhwhy7awga2bmv5n8qq8bgy8672iha9f3y871dm6vibr")))

(define-public crate-md5-0.4.0 (c (n "md5") (v "0.4.0") (h "0h1gikrljvcx89dp0nnpawxd7h8ra6sgmylv97wdy1pcxjy9jncr")))

(define-public crate-md5-0.5.0 (c (n "md5") (v "0.5.0") (h "1xrp2p2ipgllcwqi2xm82l0qwpzflbgggqcz7kh59bmns0c8pbf1")))

(define-public crate-md5-0.6.0 (c (n "md5") (v "0.6.0") (h "1yamxsx3i98hr2jm893yhvqj5g4005qkia5hkpw1ghcmysap1z9c")))

(define-public crate-md5-0.6.1 (c (n "md5") (v "0.6.1") (h "17b2xm4h4cvxsdjsf3kdrzqv2za60kak961xzi5kmw6g6djcssvy")))

(define-public crate-md5-0.7.0 (c (n "md5") (v "0.7.0") (h "0wcps37hrhz59fkhf8di1ppdnqld6l1w5sdy7jp7p51z0i4c8329") (f (quote (("std") ("default" "std"))))))

