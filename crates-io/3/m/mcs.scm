(define-module (crates-io #{3}# m mcs) #:use-module (crates-io))

(define-public crate-mcs-0.1.0 (c (n "mcs") (v "0.1.0") (h "0yrjh6sl97accrapmkb12r0mrmmvvzrx36ggxi39rxk7w4lp7b5c")))

(define-public crate-mcs-0.1.1 (c (n "mcs") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 2)))) (h "0r4sz1byjvh8sgv9g9vii5ab1dph8lcmcjrq7zc6f74qgij494kq") (f (quote (("unstable") ("default" "unstable"))))))

