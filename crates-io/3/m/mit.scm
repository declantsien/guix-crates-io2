(define-module (crates-io #{3}# m mit) #:use-module (crates-io))

(define-public crate-mit-0.1.0 (c (n "mit") (v "0.1.0") (h "0bkagfl67wbf5slc2bw7zzmynxcqk5s3d4pjp3lqhvsmz5mqvcg6")))

(define-public crate-mit-0.1.1 (c (n "mit") (v "0.1.1") (d (list (d (n "handlebars") (r "^4.2.1") (d #t) (k 0)))) (h "16sldbi7073x7nkmq0gkdrxcqws79ms6qwnbhivb43g6cjlljczj")))

(define-public crate-mit-0.1.2 (c (n "mit") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "0ddh32savn3ga703jn4lqv4gllcn3bqahhqi40xzk444z10m7v9b")))

(define-public crate-mit-0.1.3 (c (n "mit") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "09fda8bgnk98gbbcvwacl251d6jknin4061a6605wy1671a7ivzg")))

(define-public crate-mit-0.1.4 (c (n "mit") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "0k8jlw4rk6924815mbcqgqr1jr892h0szjbjn9m5jbnsig2vw2jb")))

(define-public crate-mit-0.1.5 (c (n "mit") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "1n5l3dk9i14arkl33x8859azid3hskpp61rm0a4yws40ih8rimmv")))

(define-public crate-mit-0.1.5-1 (c (n "mit") (v "0.1.5-1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "1f799h73s9j0vvi12xkpyqcpwnp8sppc8slsh39mr8abz0h3chli")))

(define-public crate-mit-0.1.5-2 (c (n "mit") (v "0.1.5-2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "1ymvhy252kf6jkcdhcpavhpbzfgx9wlg9wf7j64q66sl5p8gb4is")))

(define-public crate-mit-0.1.6 (c (n "mit") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "07nsfbs81vmsh8a729l8vzajn8lzw0yygp35mvz82xl3cz2vyix7")))

(define-public crate-mit-0.1.7 (c (n "mit") (v "0.1.7") (d (list (d (n "clap") (r "^4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.2") (d #t) (k 0)))) (h "0wn2ppm2ffxgxkjn0si5di5ma84385w0nin2jhj2v5sy2p22sz1y")))

(define-public crate-mit-0.1.8 (c (n "mit") (v "0.1.8") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.3") (d #t) (k 0)))) (h "0f2c2j2ksrvagyb9axjjs7y10xxy031kxjahxq4ql6x5ksd90y7n")))

