(define-module (crates-io #{3}# m myr) #:use-module (crates-io))

(define-public crate-myr-0.0.0 (c (n "myr") (v "0.0.0") (d (list (d (n "forust") (r "^0.1.0") (d #t) (k 0)) (d (n "plotters") (r "^0.3.5") (d #t) (k 0)) (d (n "polars") (r "^0.31.1") (d #t) (k 0)) (d (n "smartcore") (r "^0.3.2") (d #t) (k 0)))) (h "1554n4y9xq88d0qdxlxzfgppmbf67ybaj775rxmsnyv89gxa6hk0") (y #t)))

