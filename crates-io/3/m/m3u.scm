(define-module (crates-io #{3}# m m3u) #:use-module (crates-io))

(define-public crate-m3u-0.1.0 (c (n "m3u") (v "0.1.0") (d (list (d (n "url") (r "^1.2.4") (d #t) (k 0)))) (h "1im3lx3y2w799w9cl0jx2b97f06vnlzk2s9dpi15gr9h7319b1ai")))

(define-public crate-m3u-1.0.0 (c (n "m3u") (v "1.0.0") (d (list (d (n "url") (r "^1.2.4") (d #t) (k 0)))) (h "0kggdp0f7adsmb94k3x22lzapb6brykwip5rm0dai5vkdazvwj6a")))

