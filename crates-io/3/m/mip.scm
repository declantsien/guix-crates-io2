(define-module (crates-io #{3}# m mip) #:use-module (crates-io))

(define-public crate-mip-0.3.0 (c (n "mip") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1zpv0qz3zw1zk590rbxxmd277b5hr1893vvwxgxmn3ax3rda2x6y")))

(define-public crate-mip-0.4.0 (c (n "mip") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0hzfdms77gg88qw8l80da513cc0xf9vnca8g55gfak1ffppc94d5")))

(define-public crate-mip-0.4.1 (c (n "mip") (v "0.4.1") (d (list (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0km0rkykk6h2clqsbslx2pah5fk8987868d9n047y3pgblk4am2g")))

