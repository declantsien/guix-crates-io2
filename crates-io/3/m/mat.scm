(define-module (crates-io #{3}# m mat) #:use-module (crates-io))

(define-public crate-mat-0.1.0 (c (n "mat") (v "0.1.0") (d (list (d (n "mat-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "09kb7z77gcxvrbxp76y9hfg1vldrgb19ijg30jy42kz46jlndkxh")))

(define-public crate-mat-0.1.1 (c (n "mat") (v "0.1.1") (d (list (d (n "mat-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "1v8k147znsmihv7sf3c7zp5x33l1lvh98946p5d8jpx8yjpsaxaz")))

(define-public crate-mat-0.2.0 (c (n "mat") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.11.0") (d #t) (k 0)))) (h "1gbyvb33r1k1avpias1qmnsws0f7c0whhj6hprl2l4z331fy60c4")))

