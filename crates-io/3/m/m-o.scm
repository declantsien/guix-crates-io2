(define-module (crates-io #{3}# m m-o) #:use-module (crates-io))

(define-public crate-m-o-0.1.0 (c (n "m-o") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "1faaxwl1nsnqpfwkj5zyz2nfwxspyr6ml3mzqrbpfv63lqjnm3is")))

(define-public crate-m-o-0.1.1 (c (n "m-o") (v "0.1.1") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "1lhi682gl6f7aknnqzrnqvjsc8ddlrm288dk354kjyxz0dlfcmr1")))

(define-public crate-m-o-0.1.2 (c (n "m-o") (v "0.1.2") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "0is53id18dhdzksayg9a9hdpxxv3gblhadj1zqsflbhwd3cy8hnl")))

(define-public crate-m-o-0.1.3 (c (n "m-o") (v "0.1.3") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "0v4cp1375wjpchg9x82cwc8rba5ax8yllf95i851y85k6hn84i79")))

(define-public crate-m-o-0.1.4 (c (n "m-o") (v "0.1.4") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "00094plfvy0z5makj3mfh8diq20n0y1k11vsxriwyxd1sw0w2khq")))

(define-public crate-m-o-0.1.5 (c (n "m-o") (v "0.1.5") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "1q6z6g6wksyzwnhqyjpxdqw3dyc408q67zf80kpwvwbzp9pqrhz4")))

(define-public crate-m-o-1.0.0 (c (n "m-o") (v "1.0.0") (d (list (d (n "nom") (r "^5.0.1") (f (quote ("regexp"))) (d #t) (k 0)) (d (n "pretty") (r "^0.6.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1zgq2261vjrid8dnh1cj1hdj8pv034ff82y3plm1xm1x11n67qd8")))

