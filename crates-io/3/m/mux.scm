(define-module (crates-io #{3}# m mux) #:use-module (crates-io))

(define-public crate-mux-0.0.2 (c (n "mux") (v "0.0.2") (h "0z0sm1z8gpykjirlrjk2zqaqvs84gyb4vz1x1m507i35fig1b0ck")))

(define-public crate-mux-0.1.0 (c (n "mux") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1.34") (d #t) (k 0)))) (h "0yrhpvjk7p19f0y79nf5316nc8jdyymaksj4vlxyv049qq44hx8c")))

(define-public crate-mux-0.1.1 (c (n "mux") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0j9v2zwg6sgrpzac0a8q50vyxg5l3w5rhrwhvl4dsawz42s06zyx")))

