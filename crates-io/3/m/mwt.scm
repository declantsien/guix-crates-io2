(define-module (crates-io #{3}# m mwt) #:use-module (crates-io))

(define-public crate-mwt-0.1.0 (c (n "mwt") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1zzq303ilhi93w15wyddpbjaascmr97lhq29k5kwzyn5il61pp18")))

(define-public crate-mwt-0.2.0 (c (n "mwt") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "08hx4jp1bbzbymgp0vamy9n2b67b3915cbpxgjrad5k0s5wgbpvx")))

(define-public crate-mwt-0.3.0 (c (n "mwt") (v "0.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "06rn20rfb1ial8syrwaqh1kbx7454y68bpyiahyip4my9jqyildv")))

(define-public crate-mwt-0.4.0 (c (n "mwt") (v "0.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1q1i37iyr2301s48kri4xlc1jw4whfsn3b9w9iiv88rambagwq2m")))

(define-public crate-mwt-0.4.1 (c (n "mwt") (v "0.4.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "162z4abyyizgjna024amhr564l958yd7bxacnyacjh34ymwvzgn2")))

(define-public crate-mwt-0.4.2 (c (n "mwt") (v "0.4.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0aphhlha80n67qi7z8y6ddh696bzjm934w0ngg7sfidf558krbnr")))

(define-public crate-mwt-0.4.3 (c (n "mwt") (v "0.4.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0w9hl0k2789wf31vkljvdv06mcckngsz3c0sq725131nyxlj6fkk")))

