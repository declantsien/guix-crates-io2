(define-module (crates-io #{3}# m mcc) #:use-module (crates-io))

(define-public crate-mcc-0.1.0 (c (n "mcc") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1k3hhw9sd861p8j699myavznlfzs03g9fw028mr401d6hkz52dml")))

