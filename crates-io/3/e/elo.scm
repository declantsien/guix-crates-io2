(define-module (crates-io #{3}# e elo) #:use-module (crates-io))

(define-public crate-elo-0.1.0 (c (n "elo") (v "0.1.0") (h "06q6qkh74cnymn44dvkdnas9q8h571hik04wb918mkk4jgvf5jh5") (y #t)))

(define-public crate-elo-0.1.1 (c (n "elo") (v "0.1.1") (h "1yfl9p93m7yd4d90rk6vv551020q3d5h5rsdkh304lg450a7gjvn")))

(define-public crate-elo-0.2.0 (c (n "elo") (v "0.2.0") (h "0ajspcd2vn7ba60q9dvmvvx1pd8d1acgdcg2a00bizb7if1qalwq")))

(define-public crate-elo-0.2.1 (c (n "elo") (v "0.2.1") (h "0hahjkqbhy28dq9rgg17s2n9ib1rkmjb61xw7slw2cb1hckgy2d2")))

(define-public crate-elo-0.3.0 (c (n "elo") (v "0.3.0") (h "0xsv5dg7djgivpyl8jxac2xbvj845rnhpmw6gx44vrmxp4xr2f1j")))

