(define-module (crates-io #{3}# e efg) #:use-module (crates-io))

(define-public crate-efg-0.1.0 (c (n "efg") (v "0.1.0") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1l0zl5v80rxyaa8xrbhrzgwxsm0rdx6nihyjpi8kp82vfvj3rnld") (r "1.55")))

(define-public crate-efg-0.1.1 (c (n "efg") (v "0.1.1") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zswsq2rz28asqi9bq6mm5w9vfzw97y82v4xlf7kg9a62z5iqlqq") (r "1.55")))

(define-public crate-efg-0.1.2 (c (n "efg") (v "0.1.2") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zq0mqhm30zl9fyhvax6ngf9n7irc7v53drr7l1y1kz63hgjrb90") (r "1.55")))

(define-public crate-efg-0.1.3 (c (n "efg") (v "0.1.3") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "18pp3ywyfb0h62kr3cv2kf7cbv2gkmbjqf88savxcxkgb9nrq0ih") (r "1.55")))

(define-public crate-efg-0.1.4 (c (n "efg") (v "0.1.4") (d (list (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0krp4yydvvprhgw2q20jvl7q70vvf16m6zvcb16jx8cqq0sz81jb") (r "1.55")))

