(define-module (crates-io #{3}# e eff) #:use-module (crates-io))

(define-public crate-eff-0.0.1 (c (n "eff") (v "0.0.1") (h "0zyn3xf0hil4r5bk9gkl06x3glx90n15jgyv9rjy4klmz20g0cxx")))

(define-public crate-eff-0.0.2-alpha.0 (c (n "eff") (v "0.0.2-alpha.0") (d (list (d (n "eff-attr") (r "^0.0.2") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "rich-phantoms") (r "^0.1.0") (d #t) (k 0)))) (h "06nfb9yjrhhl3iwvvpnvzrmqhim079z3dknqwksi20dhlqv4idaw")))

(define-public crate-eff-0.0.2 (c (n "eff") (v "0.0.2") (d (list (d (n "eff-attr") (r "^0.0.2") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "rich-phantoms") (r "^0.1.0") (d #t) (k 0)))) (h "0d5hsca8y5hxsgvydamvpismwg57q76ik7zs3qz23m5n7yb82pfx")))

(define-public crate-eff-0.0.3 (c (n "eff") (v "0.0.3") (d (list (d (n "eff-attr") (r "^0.0.3") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "rich-phantoms") (r "^0.1.0") (d #t) (k 0)))) (h "0fiy1vng2z3dp26p4gz8xd7wavlh6l6lc23n5wd9h1f8fgml6r4l")))

(define-public crate-eff-0.0.4 (c (n "eff") (v "0.0.4") (d (list (d (n "eff-attr") (r "^0.0.4") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "rich-phantoms") (r "^0.1.0") (d #t) (k 0)))) (h "0ahmkhxz6zz18x83f7ndhw8wnbq68yyl7k0wl42nq3zhfpn34fzc")))

(define-public crate-eff-0.0.5 (c (n "eff") (v "0.0.5") (d (list (d (n "eff-attr") (r "^0.0.5") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "rich-phantoms") (r "^0.1.0") (d #t) (k 0)))) (h "0ykr7f4nxr17kyayx8r3nhhig7ri9kk2b35id0ajy874lslmqp68")))

(define-public crate-eff-0.0.6 (c (n "eff") (v "0.0.6") (d (list (d (n "eff-attr") (r "^0.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)) (d (n "rich-phantoms") (r "^0.1.0") (d #t) (k 0)))) (h "13qvaj19cw66azi5ysjikwanm3z7y77bccfvq13lqbd7wch9xa5c")))

(define-public crate-eff-0.0.7 (c (n "eff") (v "0.0.7") (d (list (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "eff-attr") (r "^0.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.4") (d #t) (k 0)))) (h "1i6x9amh92dwmiqr5my25db0mx1xyh8gfg9jxkfc15fkrlixrsks")))

(define-public crate-eff-0.0.8 (c (n "eff") (v "0.0.8") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "eff-attr") (r "^0.0.8") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.18") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "runtime") (r "^0.3.0-alpha.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2.0-alpha.4") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.3.0-alpha.4") (d #t) (k 2)))) (h "1a5sl1z5dd6nrgkf69m46ammyiqbw9ms8w1qddzy4fng5xld34rr")))

(define-public crate-eff-0.1.0 (c (n "eff") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "eff-attr") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "pin-project") (r "^0.4.5") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5.11") (d #t) (k 0)) (d (n "tokio") (r "^0.2.0-alpha.6") (d #t) (k 2)) (d (n "tokio-timer") (r "^0.3.0-alpha.6") (d #t) (k 2)))) (h "0h6nfv8cd4zy79zw8l2xvba3812arahkcm74h1m3dkwv1flqdaxn") (f (quote (("futures-compat" "futures") ("default" "futures-compat"))))))

