(define-module (crates-io #{3}# e edg) #:use-module (crates-io))

(define-public crate-edg-0.1.0 (c (n "edg") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde" "clock"))) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hn9clpz6pzb9jxqvgyfdqz8mhsjcpj71lb3214zl1l734nn6jv1")))

