(define-module (crates-io #{3}# e ens) #:use-module (crates-io))

(define-public crate-ens-0.1.0 (c (n "ens") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4.2") (d #t) (k 0)) (d (n "web3") (r "^0.3.1") (d #t) (k 0)))) (h "1xz9w73gcpc47f258nb2xv9zvb8nndqh0apmzsp51krp23ck5qr1")))

