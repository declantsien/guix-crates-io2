(define-module (crates-io #{3}# e etl) #:use-module (crates-io))

(define-public crate-etl-0.1.0 (c (n "etl") (v "0.1.0") (d (list (d (n "csv") (r "^1.0.0-beta.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "unittest") (r "^0.1") (d #t) (k 0)) (d (n "wee-matrix") (r "^0.1") (d #t) (k 0)))) (h "09vclk44q6wqgxiw57lamajimhg2plk47vm5wdkcpqbsgkyc0a4c")))

