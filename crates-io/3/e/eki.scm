(define-module (crates-io #{3}# e eki) #:use-module (crates-io))

(define-public crate-eki-0.1.0 (c (n "eki") (v "0.1.0") (d (list (d (n "ohsl") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p6vza1k04l8mjkwbm4i4qk99y1c9pvknddjhap2id8r304jay4b")))

(define-public crate-eki-0.2.0 (c (n "eki") (v "0.2.0") (d (list (d (n "ohsl") (r "^0.7.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ncsaa8fj53fb1hy0ay1r1sxrr2l23all6blsndm2nbjmsgwx28s")))

