(define-module (crates-io #{3}# e ebr) #:use-module (crates-io))

(define-public crate-ebr-0.0.0 (c (n "ebr") (v "0.0.0") (h "0079x0syk7fq7azkb6ybg97v0ln3z6n9nr92y2p28djc73vpfi5j")))

(define-public crate-ebr-0.0.1 (c (n "ebr") (v "0.0.1") (d (list (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 2)))) (h "047n74j8fi6fs72nbirmy09lzlxxlfg7xi8zwgln42agg83fgsbp")))

(define-public crate-ebr-0.0.2 (c (n "ebr") (v "0.0.2") (d (list (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 2)))) (h "0j665c8vrldrma8l4y7j5nc9d4h9ibbnvx63gbfhdjvhxzc8771c")))

(define-public crate-ebr-0.0.3 (c (n "ebr") (v "0.0.3") (d (list (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 2)))) (h "0cmaxnx53h7cphm2xn5naqhfwn3nbf9wahiqc19wc9i3gvadg7nr")))

(define-public crate-ebr-0.0.4 (c (n "ebr") (v "0.0.4") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.0") (d #t) (k 0)))) (h "0bfsa041sfga0x67w46skpgjhcyv1lj359sd5k700899g0agc9fj")))

(define-public crate-ebr-0.0.5 (c (n "ebr") (v "0.0.5") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8.5") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.0") (d #t) (k 0)))) (h "0z87r65ny79nwlrpl2vi9qj8qz4988gv55l84lk738drlyiwlsgl")))

(define-public crate-ebr-0.1.38 (c (n "ebr") (v "0.1.38") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.0") (d #t) (k 0)))) (h "1sqjmq2idz87ny0fidfr5kvgq5vgzjwrx6xn5msic3wavfg2lfv8")))

(define-public crate-ebr-0.1.39 (c (n "ebr") (v "0.1.39") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.0") (d #t) (k 0)))) (h "14w2wwsw03pmw2yvk6x0skkc03ddl1nhgyvlsci2vrp806rynjl1")))

(define-public crate-ebr-0.1.40 (c (n "ebr") (v "0.1.40") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.0") (d #t) (k 0)))) (h "1dgfvd1qlmzbcik4yc8q563p56zkg9cd5fw7p8nfxrfjn5dzd7px") (y #t)))

(define-public crate-ebr-0.2.1 (c (n "ebr") (v "0.2.1") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.0") (d #t) (k 0)))) (h "1a9b73pkgbrsmdmvs38v35j1xq9ynj83jk09w93spglkkban0nvc")))

(define-public crate-ebr-0.2.2 (c (n "ebr") (v "0.2.2") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.0") (d #t) (k 0)))) (h "13hyixrx6xbx6lbavdhprv68p6sm3gi5yznxdc1rcn7qg3jcgldr")))

(define-public crate-ebr-0.2.3 (c (n "ebr") (v "0.2.3") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.1") (d #t) (k 0)))) (h "04zhz9jfq1g2mjzs7ps14cmby5zxzyysxbw0h28yh7166s4g10ch")))

(define-public crate-ebr-0.2.4 (c (n "ebr") (v "0.2.4") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.1") (d #t) (k 0)))) (h "05lp9hyq3w238fjfskfafg6sjxisa6lxh2a53dj0mq85wihmvahr")))

(define-public crate-ebr-0.2.5 (c (n "ebr") (v "0.2.5") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.1") (d #t) (k 0)))) (h "1zb528jaxi8f8xyisz1y3m9w67cl8jb1p8c9mqg98fld7nlxp626") (f (quote (("lock_free_delays"))))))

(define-public crate-ebr-0.2.6 (c (n "ebr") (v "0.2.6") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.1") (d #t) (k 0)))) (h "0x22fd5pj6pmdzhls01m07hfy8lxp8yv29wfqpnchrnl705r2h68") (f (quote (("lock_free_delays"))))))

(define-public crate-ebr-0.2.7 (c (n "ebr") (v "0.2.7") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.1") (d #t) (k 0)))) (h "10w005d9whc6spnv92yy0prziq4a27g5xvnd1bgf4i736g3md698") (f (quote (("lock_free_delays"))))))

(define-public crate-ebr-0.2.8 (c (n "ebr") (v "0.2.8") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.1") (d #t) (k 0)))) (h "1azidn87d8wfsl4rrz3p99iqamab0qrjyqmm7ypjkmr3wnzi579r") (f (quote (("lock_free_delays"))))))

(define-public crate-ebr-0.2.9 (c (n "ebr") (v "0.2.9") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.1") (d #t) (k 0)))) (h "1bgkd7dv761vc06x5ix9nbipq2yhnsddz8f3f5alnw9slvvlmrrx") (f (quote (("lock_free_delays"))))))

(define-public crate-ebr-0.2.10 (c (n "ebr") (v "0.2.10") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "shared-local-state") (r "^0.1.1") (d #t) (k 0)) (d (n "zipf") (r "^7.0") (o #t) (d #t) (k 0)))) (h "01m54sjzbrw2xx4y0qb29z3ihax3kpi9lvfjsi2dpdv3cpq49h6h") (f (quote (("lock_free_delays" "rand" "zipf"))))))

(define-public crate-ebr-0.2.11 (c (n "ebr") (v "0.2.11") (d (list (d (n "crossbeam-epoch") (r "^0.9.8") (d #t) (k 2)) (d (n "shared-local-state") (r "^0.1.1") (d #t) (k 0)))) (h "0jzwjidbs2i2llcz55ms2qmr4v1zq6i8mdjpcgrzqmaf2ss90zra")))

