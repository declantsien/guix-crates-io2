(define-module (crates-io #{3}# e efi) #:use-module (crates-io))

(define-public crate-efi-0.1.0 (c (n "efi") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)))) (h "1x3ivvvv7gnbmjf44kmvk6380zbgm47r80g9d5wqqlp0cilg829j")))

(define-public crate-efi-0.1.2 (c (n "efi") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)))) (h "04i3nrmchk76kw2j5zhgy89zwkp4ypjlmyyyag34mrz9l3jq765z")))

(define-public crate-efi-0.1.3 (c (n "efi") (v "0.1.3") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)))) (h "1l9436dpjirx4fnmmmzi7j8q29wvvglviw5fl9zpwba77phshy8w")))

(define-public crate-efi-0.2.0 (c (n "efi") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)))) (h "1ll7xms8zh8lgnxrsaq4n2il7db37marpfyq7aghpgc5vryhqd56")))

(define-public crate-efi-0.2.1 (c (n "efi") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)))) (h "128h2llnnrph1mr8db9li1gbxj1yd1v0mjnrga5nnw9c6m6y99rx")))

(define-public crate-efi-0.3.0 (c (n "efi") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "ffi") (r "^0.1.1") (d #t) (k 0) (p "efi_ffi")) (d (n "rlibc") (r "^1.0.0") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1.4") (d #t) (k 0)))) (h "1igz8yqwrdn19nxprcgnn4w2bb1jgpblkxgb097jnxyd76yqx5vh") (f (quote (("default" "allocator") ("allocator"))))))

(define-public crate-efi-0.3.1 (c (n "efi") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "failure") (r "^0.1.1") (f (quote ("derive"))) (k 0)) (d (n "ffi") (r "^0.1.1") (d #t) (k 0) (p "efi_ffi")) (d (n "rlibc") (r "^1.0.0") (d #t) (k 0)) (d (n "utf8-width") (r "^0.1.4") (d #t) (k 0)))) (h "02nqm6nvbzg18rj6y1qhj1n36fiiz9bzzpssh6x2dwcy5zczm9x3") (f (quote (("default" "allocator") ("allocator"))))))

