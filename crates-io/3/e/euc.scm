(define-module (crates-io #{3}# e euc) #:use-module (crates-io))

(define-public crate-euc-0.1.0 (c (n "euc") (v "0.1.0") (d (list (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "tobj") (r "^0.1") (d #t) (k 2)) (d (n "vek") (r "^0.9") (d #t) (k 0)))) (h "14bsrwxpz543nmnkj6znxfqqdz70r3zn4cds78wkai7vg55mjybk") (f (quote (("nightly"))))))

(define-public crate-euc-0.2.0 (c (n "euc") (v "0.2.0") (d (list (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "tobj") (r "^0.1") (d #t) (k 2)) (d (n "vek") (r "^0.9") (d #t) (k 0)))) (h "1y6cwbzjazx4s2z1ami70lkwnvx4hdrlc6xc6pzrkszf9x65jl81") (f (quote (("nightly"))))))

(define-public crate-euc-0.3.0 (c (n "euc") (v "0.3.0") (d (list (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "tobj") (r "^0.1") (d #t) (k 2)) (d (n "vek") (r "^0.9") (d #t) (k 0)))) (h "1gcfq3q5v6rrh8199m45qcmpnf5jn1yfssx3031zf3a2m226c3y2") (f (quote (("nightly"))))))

(define-public crate-euc-0.4.0 (c (n "euc") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.22.2") (d #t) (k 2)) (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "tobj") (r "^0.1") (d #t) (k 2)) (d (n "vek") (r "^0.9.9") (d #t) (k 0)))) (h "0azsy3chfwp1zagvb7fsh5lly5l5hyy7s2b9sp01zl019546k6mh") (f (quote (("nightly"))))))

(define-public crate-euc-0.4.1 (c (n "euc") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.22.2") (d #t) (k 2)) (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "tobj") (r "^0.1") (d #t) (k 2)) (d (n "vek") (r "^0.9.9") (d #t) (k 0)))) (h "0v18jxfy8czpzgvxhvw74cz8bc9b2s73yzy0vglrv0g74hbpc829") (f (quote (("nightly"))))))

(define-public crate-euc-0.4.2 (c (n "euc") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.22.2") (d #t) (k 2)) (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "tobj") (r "^0.1") (d #t) (k 2)) (d (n "vek") (r "^0.9.9") (d #t) (k 0)))) (h "1c6zik43khqqpy3gf5w263z7xwcrsk78k3rky2swibk7v9g2slw9") (f (quote (("nightly"))))))

(define-public crate-euc-0.4.3 (c (n "euc") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.22.2") (d #t) (k 2)) (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "tobj") (r "^0.1") (d #t) (k 2)) (d (n "vek") (r "^0.9.9") (d #t) (k 0)))) (h "0kkbzvlm73lwrfvn410k9sq3xhnic4dlqfiiz0vmq97l6wx5hgay") (f (quote (("nightly"))))))

(define-public crate-euc-0.5.0 (c (n "euc") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.22.2") (d #t) (k 2)) (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (o #t) (k 0)) (d (n "tobj") (r "^0.1") (d #t) (k 2)) (d (n "vek") (r "^0.10.0") (f (quote ("rgb" "rgba"))) (k 0)))) (h "19gs68kpk2f4ja0c99zw2ycqp5fjrr3qrxlrsvpx6jagsl3rq0gd") (f (quote (("std" "vek/std") ("libm" "vek/libm" "num-traits") ("default" "std"))))))

(define-public crate-euc-0.5.2 (c (n "euc") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23.12") (d #t) (k 2)) (d (n "minifb") (r "^0.19.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (o #t) (k 0)) (d (n "tobj") (r "^2.0.2") (d #t) (k 2)) (d (n "vek") (r "^0.12.1") (f (quote ("rgb" "rgba"))) (k 0)))) (h "1x844rz7srcixw6vxlmr5icq2n0i9xx9wm9hvqd0xdrif94b0jl4") (f (quote (("std" "vek/std") ("libm" "vek/libm" "num-traits") ("default" "std"))))))

(define-public crate-euc-0.5.3 (c (n "euc") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.23.12") (d #t) (k 2)) (d (n "minifb") (r "^0.19.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.11") (o #t) (k 0)) (d (n "tobj") (r "^2.0.2") (d #t) (k 2)) (d (n "vek") (r "^0.14.1") (f (quote ("rgb" "rgba"))) (k 0)))) (h "1vgm3qpix8mlm2mkp2rrxrhlsw3223ffmi9ahv14m2nj8rkd0iq9") (f (quote (("std" "vek/std") ("libm" "vek/libm" "num-traits") ("default" "std"))))))

