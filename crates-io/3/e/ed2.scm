(define-module (crates-io #{3}# e ed2) #:use-module (crates-io))

(define-public crate-ed2-0.1.6 (c (n "ed2") (v "0.1.6") (d (list (d (n "ed2-derive") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "seq-macro") (r "^0.1.5") (d #t) (k 0)))) (h "0cz84mrz7b7g2yzkwpignxrj2cbln488apk5zcjkk3vmcf9w985c")))

