(define-module (crates-io #{3}# e ets) #:use-module (crates-io))

(define-public crate-ets-0.1.0 (c (n "ets") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.1") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "predicates") (r "^1.0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)))) (h "0f7a0k8vy47klwbacbz8sbrc8rp5pv0ky5gbg5ijmwmi8nrpf1py")))

(define-public crate-ets-0.1.1 (c (n "ets") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "1rqan014vmsck94yc8dclaw24j07p65q815sabr5cljzfsz6zzp6")))

(define-public crate-ets-0.1.2 (c (n "ets") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "1khsvlzm8xwd1znvq1jw6kg650v6c0vvf1nm9j3m2i7fj6dpbh31")))

(define-public crate-ets-0.1.3 (c (n "ets") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "0frkbs8ag339h6k6s6qql1il7n8asxq5q51wc38sy0wvssfkdczn")))

(define-public crate-ets-0.2.3 (c (n "ets") (v "0.2.3") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "10dic0pnrgv8jppy5zpp43zz5z0y8ya305azzipb3chvg2ly1f25")))

(define-public crate-ets-0.2.4 (c (n "ets") (v "0.2.4") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "0b3qlx7dbp8iklq64n8r1yklwp43pqw6j723z5rcizqkpmqwpwja")))

(define-public crate-ets-0.2.5 (c (n "ets") (v "0.2.5") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "18i05nr6sqjk8xgqxahxj7sz2q5rrja1xyrh9aahx6450wlq6xxg")))

(define-public crate-ets-0.2.6 (c (n "ets") (v "0.2.6") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "0ms09sqj701qv5kiqkf7wbwfdj847vkw7j2z1irvk21p40qwj6h0")))

