(define-module (crates-io #{3}# e end) #:use-module (crates-io))

(define-public crate-end-0.0.0 (c (n "end") (v "0.0.0") (h "1ldl7bf0s7qva67y4wpb8h60l16ssw67zqni81hwhafcb07ahqd6")))

(define-public crate-end-0.0.1 (c (n "end") (v "0.0.1") (h "1nfnj6dpqdvvw1xnidg25shidz2sf66p2i7mlvnswj67k7sx9kbh")))

(define-public crate-end-0.0.2 (c (n "end") (v "0.0.2") (h "1pw4b21amxv757lk3jazcd4wq2f52zc7saf164w8sz2lq45sjm3z")))

(define-public crate-end-0.0.3 (c (n "end") (v "0.0.3") (h "1hxks0rnrda52xmjd87z0pxjx6pl10kawwlwfw38q40jclbfaww6")))

(define-public crate-end-0.0.4 (c (n "end") (v "0.0.4") (h "1rhsff341v8c1cpd2yicy8aww1y740xi827mkvppp2qx8ma3z4dq")))

(define-public crate-end-0.0.5 (c (n "end") (v "0.0.5") (d (list (d (n "num-traits") (r "^0.2.12") (k 0)))) (h "152j3rh3xb4k6lxvw8fg08xgcxh1qnnfpg3wk3b3zm0n3hhi2zvv")))

