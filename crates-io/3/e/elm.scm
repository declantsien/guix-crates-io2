(define-module (crates-io #{3}# e elm) #:use-module (crates-io))

(define-public crate-elm-0.1.0 (c (n "elm") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "08kab1z6ljj6k5j742275fywy15x54l9brvi3kss060hpz6nna1q")))

(define-public crate-elm-0.1.1 (c (n "elm") (v "0.1.1") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1pwfnfgrsb7sd7jlxm2vxhj3n41gc0gdp72llrrg722li1kq16ch")))

(define-public crate-elm-0.1.2 (c (n "elm") (v "0.1.2") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "15pa3x6nifxsf7qypgzh8nfmbprv5n80my13kj0i8qi5fdixdjwz")))

(define-public crate-elm-0.2.0 (c (n "elm") (v "0.2.0") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "08fpwwfic1rxr0azxmpahvd0235cb0whzzsph2w7ylacis9x6j2j")))

(define-public crate-elm-0.2.1 (c (n "elm") (v "0.2.1") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1rmy800zvcnx6x6fmrns55bsz3filgxap350sd5s9w90vxw3jbdw")))

(define-public crate-elm-0.2.2 (c (n "elm") (v "0.2.2") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0530ykykryqr5i866k6w7m2az4hw4lvsl1lvkm3yw7myg73zf1ym")))

(define-public crate-elm-0.2.3 (c (n "elm") (v "0.2.3") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1vgwkyfggi2a3ksqpffnfapdhamlkcrjprscpawvrl2rxznnvap3")))

(define-public crate-elm-0.2.4 (c (n "elm") (v "0.2.4") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "1jbp5qz2qsxlb05rjj26pxz477i58yj0570x7f09j1xhncw1p8aw")))

(define-public crate-elm-0.3.1 (c (n "elm") (v "0.3.1") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0c7prkgpgz67d5503amgapmmi06hw5db0q8lfc0qrbl4sbyrv6lc")))

(define-public crate-elm-0.3.2 (c (n "elm") (v "0.3.2") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zl2k918q9s282103648rb6p72c22fx6fzy09fvydpk58kaigynh")))

