(define-module (crates-io #{3}# e emi) #:use-module (crates-io))

(define-public crate-emi-0.3.0 (c (n "emi") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.0") (d #t) (k 0)))) (h "13lvq0bdc9pd4jjmv3sqgad6y18ibn7a31mgfs75k8m0pp3161jd")))

