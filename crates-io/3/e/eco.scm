(define-module (crates-io #{3}# e eco) #:use-module (crates-io))

(define-public crate-eco-0.1.0 (c (n "eco") (v "0.1.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.12.1") (d #t) (k 0)) (d (n "range") (r "^0.1.1") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "0lfa9i1f3f9jwxjjpm3bhmnxdqc09j67046mszli3phywqzh2am7")))

(define-public crate-eco-0.1.1 (c (n "eco") (v "0.1.1") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.12.1") (d #t) (k 0)) (d (n "range") (r "^0.1.1") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "1b3rdkyaimjkh4hqbmmg5y3p3s1br4dydl6jxwjdcwrjmjn04fng")))

(define-public crate-eco-0.1.2 (c (n "eco") (v "0.1.2") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.12.1") (d #t) (k 0)) (d (n "range") (r "^0.1.1") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "1rahgnq7nb813pjcvhvy8gpvxpaqwhrc59fcf67mfbn30j6zm6jg")))

(define-public crate-eco-0.1.3 (c (n "eco") (v "0.1.3") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.12.1") (d #t) (k 0)) (d (n "range") (r "^0.1.1") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "0jc3m1f9c5aqf74xd1w9q974cnpwjlla5z52xpjzfvrx0yp347y4")))

(define-public crate-eco-0.1.4 (c (n "eco") (v "0.1.4") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.12.1") (d #t) (k 0)) (d (n "range") (r "^0.1.1") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "1xnmwf7mpy9s0c6gdxikkfr0snwk8wbbldi133ch9kamsm02k5p9")))

(define-public crate-eco-0.2.0 (c (n "eco") (v "0.2.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.15.1") (d #t) (k 0)) (d (n "range") (r "^0.2.0") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "0r0pr10a46vh8c3cq9rnni6d8hdc6nik7cqhrgwnni79cxxphxkk")))

(define-public crate-eco-0.3.0 (c (n "eco") (v "0.3.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.15.1") (d #t) (k 0)) (d (n "range") (r "^0.2.0") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "0mn1mqi8q8lng6d70v15wq1a2dhgzgkh4vs3qd32sdi5gdvda04i")))

(define-public crate-eco-0.4.0 (c (n "eco") (v "0.4.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.16.0") (d #t) (k 0)) (d (n "range") (r "^0.2.0") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "18ygcl7gahhhngdifyninah6x7nbdfn36p9sjbvfghai754ihdsd")))

(define-public crate-eco-0.5.0 (c (n "eco") (v "0.5.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.19.0") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "1bzwnwpjdgx4phz69rjrcncqg4d80mr42k35i9f7b72za0xanvxr")))

(define-public crate-eco-0.6.0 (c (n "eco") (v "0.6.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.22.1") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "13npwh65fgxwi3i4dvmiagm26lrhwvs80mapklcpdp17vak47iqa")))

(define-public crate-eco-0.7.0 (c (n "eco") (v "0.7.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.24.0") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.1.20") (d #t) (k 0)))) (h "0861s23nxxsb5kfgp5qjjgdhx3gm0iiwcqyp65f46vjyqjkfhp32")))

(define-public crate-eco-0.8.0 (c (n "eco") (v "0.8.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.24.0") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.0") (d #t) (k 0)))) (h "0p6d7i6h03f8jcx1affbabbpgxcj05jxpf5n6yr44rl51k8500p1")))

(define-public crate-eco-0.9.0 (c (n "eco") (v "0.9.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.25.1") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "= 0.2.0") (d #t) (k 0)))) (h "1clvb729l3frmgdz3p55la7fcrw58fr6g8bb1i8kxkh82mlm0y4i")))

(define-public crate-eco-0.10.0 (c (n "eco") (v "0.10.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.26.1") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "= 0.2.0") (d #t) (k 0)))) (h "0yh6zy6xx5zp806khlyzsy10dhg6dgq6y5ny8jqajir2rzc0zxk8")))

(define-public crate-eco-0.11.0 (c (n "eco") (v "0.11.0") (d (list (d (n "hyper") (r "^0.6.9") (d #t) (k 0)) (d (n "piston_meta") (r "^0.26.1") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "= 0.2.0") (d #t) (k 0)))) (h "01hzp0hgnzsgfyzfrsmb4hdiscij4rlsw2hzccgkwvny6gz98gdy")))

(define-public crate-eco-0.12.0 (c (n "eco") (v "0.12.0") (d (list (d (n "hyper") (r "^0.8.1") (d #t) (k 0)) (d (n "piston_meta") (r "^0.26.1") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "0mcrdn6f3mndhvv9wsnw7v3fppfpbyx5n81nisvx25iscq0vbx8s")))

(define-public crate-eco-0.14.0 (c (n "eco") (v "0.14.0") (d (list (d (n "hyper") (r "^0.8.1") (d #t) (k 0)) (d (n "piston_meta") (r "^0.26.1") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "1ybs8bqlf03bn70pivgl1pi2zg9giic3hkffhrwm5cpnhv99a2rg")))

(define-public crate-eco-0.15.0 (c (n "eco") (v "0.15.0") (d (list (d (n "hyper") (r "^0.8.1") (d #t) (k 0)) (d (n "piston_meta") (r "^0.26.1") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "1a7sdiiqp4sw2wp28v14l3bivwzh97qjdvdcryp0v1bn6g23ii7y")))

(define-public crate-eco-0.16.0 (c (n "eco") (v "0.16.0") (d (list (d (n "hyper") (r "^0.8.1") (d #t) (k 0)) (d (n "piston_meta") (r "^0.27.0") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "0k2dq1z7k02v1f4hvfcmhgvjfnyqln56bn27qxy3vnf2bcnfs4ap")))

(define-public crate-eco-0.17.0 (c (n "eco") (v "0.17.0") (d (list (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "piston_meta") (r "^0.28.0") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "087vavy3pwzm2j5qx4vzs1y2lsx11awgkda82lqw46dpjikxjysa")))

(define-public crate-eco-0.18.0 (c (n "eco") (v "0.18.0") (d (list (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "piston_meta") (r "^0.29.0") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "0dvwaxadn4zpsq7j99anngj15war26h2gxa84ggx0ghh39bykixz")))

(define-public crate-eco-0.19.0 (c (n "eco") (v "0.19.0") (d (list (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "piston_meta") (r "^0.29.0") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "0q8f2bmdy15kmbd6ri4difwj8gnkcvjhh15vsdi1h7pq7d1g0dwf")))

(define-public crate-eco-0.20.0 (c (n "eco") (v "0.20.0") (d (list (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "piston_meta") (r "^0.30.0") (d #t) (k 0)) (d (n "range") (r "^0.3.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "1crs0bv3nk3x3pwk0417pkhpgviq6hlcir93qiq1m5qab3qpgxvl")))

(define-public crate-eco-0.21.0 (c (n "eco") (v "0.21.0") (d (list (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "piston_meta") (r "^1.0.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "11awswqd4ngncz1635jl6ci3ljhlhg5l9640na9p2v7l1rcvmkvf")))

(define-public crate-eco-0.22.0 (c (n "eco") (v "0.22.0") (d (list (d (n "hyper") (r "^0.9.7") (d #t) (k 0)) (d (n "piston_meta") (r "^2.0.0") (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "12xr14qwn85h22jy00xkv6yplmczax11hyrykw2d7xk1hipxkd55")))

(define-public crate-eco-0.23.0 (c (n "eco") (v "0.23.0") (d (list (d (n "piston_meta") (r "^2.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "semver") (r "^0.2.3") (d #t) (k 0)))) (h "1bk4cl9j8mdlc9pbnjarij75spnp7hk78vwa5kp43k5gsli8b1y9")))

