(define-module (crates-io #{3}# e eta) #:use-module (crates-io))

(define-public crate-eta-0.0.0 (c (n "eta") (v "0.0.0") (h "0lx9w32c79dkb6h87x079kyn6bagp4kbmaa9hc6qr5k8vpr6125i")))

(define-public crate-eta-0.1.0 (c (n "eta") (v "0.1.0") (h "12rglnd2zhgj9c9b8fix2g7kw0n9qj49ahv1kwb2mrj16p947k0d")))

(define-public crate-eta-0.2.0 (c (n "eta") (v "0.2.0") (h "0hiyxk54nrb28qmgz6bs436cj96j3aizkwwbppaap92c9k37p31z")))

(define-public crate-eta-0.2.1 (c (n "eta") (v "0.2.1") (h "0qg87g5nnrynkx0xng29vjz5fgabxyadnz2bri9wzqyarrxvs623")))

(define-public crate-eta-0.2.2 (c (n "eta") (v "0.2.2") (h "06g69hv3b1grymvdx79hakfv49cf608rd28wff9smn7mdchmmj71")))

