(define-module (crates-io #{3}# e eso) #:use-module (crates-io))

(define-public crate-eso-0.0.1 (c (n "eso") (v "0.0.1") (d (list (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1qk1jkr95isz9p1s7xrya6gbpzjxqhkn07nxnhi3h06b93m9ckvv") (f (quote (("default" "allow-unsafe") ("allow-unsafe"))))))

(define-public crate-eso-0.0.2 (c (n "eso") (v "0.0.2") (d (list (d (n "impls") (r "^1.0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1pkl5sx7wrpqxv3ns65zlm66qwwpzf39rmhm6q8619zqz26hdq5m") (f (quote (("default" "allow-unsafe") ("allow-unsafe"))))))

