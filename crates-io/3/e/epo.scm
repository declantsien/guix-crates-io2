(define-module (crates-io #{3}# e epo) #:use-module (crates-io))

(define-public crate-epo-0.1.0 (c (n "epo") (v "0.1.0") (d (list (d (n "Boa") (r "^0.13.0") (d #t) (k 0)) (d (n "boa_engine") (r "^0.14.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "0l16qrxh16j2gmxj7i0gf82nq0fpfsvwzn34qwyp7gddi1idc70b")))

(define-public crate-epo-1.0.1 (c (n "epo") (v "1.0.1") (d (list (d (n "Boa") (r "^0.13.0") (d #t) (k 0)) (d (n "boa_engine") (r "^0.14.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)))) (h "04v3dczn31c6pnwpj4ghl957wfdp35gai5mfvpncmn8ibclygj21")))

