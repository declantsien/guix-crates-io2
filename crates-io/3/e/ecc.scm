(define-module (crates-io #{3}# e ecc) #:use-module (crates-io))

(define-public crate-ecc-0.0.1 (c (n "ecc") (v "0.0.1") (d (list (d (n "num") (r "^0.0.5") (d #t) (k 0)))) (h "1bncbx5n08ja4fj6hjmmkyab2fwnycbdprdxlwsi4w9zr8in9gdl")))

(define-public crate-ecc-0.0.3 (c (n "ecc") (v "0.0.3") (d (list (d (n "num") (r "^0.0.5") (d #t) (k 0)))) (h "0x2inbnvj1pqvr27294s3q9jsck0mj9f30ybzh4acyhm32r5scnk")))

(define-public crate-ecc-0.0.4 (c (n "ecc") (v "0.0.4") (d (list (d (n "num") (r "^0.0.5") (d #t) (k 0)))) (h "0jjwdanzjv8dgzmxg7s2qah64cn4b1sfc5gj1isa9pcy107z8w91")))

(define-public crate-ecc-0.0.5 (c (n "ecc") (v "0.0.5") (d (list (d (n "num") (r "^0.0.5") (d #t) (k 0)))) (h "1bq9ykbdijcpm3fsacs7pn0lqcdv9j7ps8iy85g2qbkqh90vhd0s")))

