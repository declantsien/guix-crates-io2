(define-module (crates-io #{3}# e ecs) #:use-module (crates-io))

(define-public crate-ecs-0.10.0 (c (n "ecs") (v "0.10.0") (d (list (d (n "uuid") (r "^0") (d #t) (k 0)))) (h "1m0fi16gi7vw0m8jjnvb1yv9bsp3nm38xblxp6cm1gjnhvhl0kf3")))

(define-public crate-ecs-0.16.0 (c (n "ecs") (v "0.16.0") (h "1ri73vp2rnzfp7v775s6629c3aalj5xhp8ggyk9pdhdn3ch1yk95")))

(define-public crate-ecs-0.16.1 (c (n "ecs") (v "0.16.1") (h "1v1bq6kw6nin51yn1brby976w2l1c7wk1h1yppz16qd9r950x50i")))

(define-public crate-ecs-0.16.2 (c (n "ecs") (v "0.16.2") (h "0zx89gdkcvpv1y83n8nlrmz389ra22zm0j7ixl0q8r3yj3xws5vj")))

(define-public crate-ecs-0.16.3 (c (n "ecs") (v "0.16.3") (h "07mxl2wrp5wpp59253vw9n3jv08vwkbyy3ixgr8r0jzcf7yaq7ya")))

(define-public crate-ecs-0.16.4 (c (n "ecs") (v "0.16.4") (h "082nljjrcj85c5j6pnwjfvh1djccaq1gyynp3b9z4j8s59hs3nj3")))

(define-public crate-ecs-0.16.5 (c (n "ecs") (v "0.16.5") (h "0vkivfandiyw63bmi127md6qj0d98b191igr3biqhjafp4jdwlnd")))

(define-public crate-ecs-0.16.6 (c (n "ecs") (v "0.16.6") (h "0wfaa1vmkmryjhiiky0hc551f3mqj9n26kxckj8ly9qnl584ld32")))

(define-public crate-ecs-0.16.7 (c (n "ecs") (v "0.16.7") (h "01qr4qm14p0v70ld8jxkaphndvb11qn9cvpakc3b3p4m5w96pi3i")))

(define-public crate-ecs-0.17.0 (c (n "ecs") (v "0.17.0") (h "1vhsb4wrhm6fz7s5l8nrspavjmwsa1v7sa4vxccrx00ny31xxbij")))

(define-public crate-ecs-0.17.1 (c (n "ecs") (v "0.17.1") (h "04q9jw72z6awykjylr5x7sdfi2krv6gykwvq330anhh4w2q2gnry")))

(define-public crate-ecs-0.17.2 (c (n "ecs") (v "0.17.2") (h "0a7ajysvvjjrqd2yfhr4j1znlx1gkw3nz1i4xqdm0c6kfc4ggx4n")))

(define-public crate-ecs-0.17.3 (c (n "ecs") (v "0.17.3") (h "18dawfz7lz8bcracppd7cf3380mixhsfzgl2ldqz27hcfp7g6lxh") (y #t)))

(define-public crate-ecs-0.18.0 (c (n "ecs") (v "0.18.0") (h "0l3ijzy4fihhsg4cjknjaw7ap4l42wd34qpzmml4wphry6x6vh31")))

(define-public crate-ecs-0.18.1 (c (n "ecs") (v "0.18.1") (h "0naz6h67c816vzrcahyd69izjpbhzi8z8nvsrsyzvfdrs9ch9qng")))

(define-public crate-ecs-0.18.2 (c (n "ecs") (v "0.18.2") (h "1rba2my5j373h5wzbngfsp7zdspc453fqfxppl54ghwifmwfg9cv")))

(define-public crate-ecs-0.18.3 (c (n "ecs") (v "0.18.3") (h "07rnaqc09j59jivfq0xmjjmg41sl8r97ci3kizfg1ch4zsamv7pv")))

(define-public crate-ecs-0.18.5 (c (n "ecs") (v "0.18.5") (h "1zify1npfrcp79xd71xsm77nnm0cdq65mjd8xjsaw59gsyh0wi5a")))

(define-public crate-ecs-0.18.7 (c (n "ecs") (v "0.18.7") (h "1w0px1m7nwm7a4kljxnixp51pi2q6vmd59r76bdjahbn8rfndp6v")))

(define-public crate-ecs-0.19.0 (c (n "ecs") (v "0.19.0") (d (list (d (n "cereal") (r "*") (f (quote ("nightly"))) (o #t) (d #t) (k 0)) (d (n "cereal_macros") (r "*") (o #t) (d #t) (k 0)))) (h "0y046gyb71bxyk460v23czhwn8srqvc9kd99l6n4k8y40k1zvp52") (f (quote (("serialisation" "cereal" "cereal_macros"))))))

(define-public crate-ecs-0.19.1 (c (n "ecs") (v "0.19.1") (d (list (d (n "cereal") (r "*") (f (quote ("nightly"))) (o #t) (d #t) (k 0)) (d (n "cereal_macros") (r "*") (o #t) (d #t) (k 0)))) (h "19ikdawnz9my041wvrmwq6rmnlmhblcfb1dh94l64269d4ig71j6") (f (quote (("serialisation" "cereal" "cereal_macros"))))))

(define-public crate-ecs-0.19.2 (c (n "ecs") (v "0.19.2") (d (list (d (n "cereal") (r "*") (f (quote ("nightly"))) (o #t) (d #t) (k 0)) (d (n "cereal_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1wg41bkr1jixjplcmyzrg5xh8i6snwkn66p14qkf0wklrj04sy34") (f (quote (("serialisation" "cereal" "cereal_macros"))))))

(define-public crate-ecs-0.19.3 (c (n "ecs") (v "0.19.3") (d (list (d (n "cereal") (r "*") (o #t) (d #t) (k 0)) (d (n "cereal_macros") (r "*") (o #t) (d #t) (k 0)))) (h "02zf6jcbiahgpskpigzlcki14brcfadhjj37mh780mqbb2j32vyw") (f (quote (("serialisation" "cereal" "cereal_macros"))))))

(define-public crate-ecs-0.19.4 (c (n "ecs") (v "0.19.4") (d (list (d (n "cereal") (r "*") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "*") (d #t) (k 0)))) (h "0b0qpz4islcm7rnhmq08q8dkdj6xw5rps8smrljln5j5qy2y2s1c") (f (quote (("serialisation" "cereal") ("nightly"))))))

(define-public crate-ecs-0.19.5 (c (n "ecs") (v "0.19.5") (d (list (d (n "cereal") (r "*") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "*") (d #t) (k 0)))) (h "04xiq9y5fr5si766g6h94vh3cpgwlp3v8vy7q6rnmyy1m9wr8l5r") (f (quote (("serialisation" "cereal") ("nightly"))))))

(define-public crate-ecs-0.20.0 (c (n "ecs") (v "0.20.0") (d (list (d (n "cereal") (r "*") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "*") (d #t) (k 0)))) (h "1lic5ww47m4vmv9b3kc8vigj35d00n8sjmx2cs8si8r5zviibml7") (f (quote (("serialisation" "cereal") ("nightly"))))))

(define-public crate-ecs-0.21.0 (c (n "ecs") (v "0.21.0") (d (list (d (n "cereal") (r "*") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "*") (d #t) (k 0)))) (h "1ja04i0mypwxw38rc7dq556jalwshykbmc3zwx0x55p3nxjh6pll") (f (quote (("serialisation" "cereal") ("nightly"))))))

(define-public crate-ecs-0.21.1 (c (n "ecs") (v "0.21.1") (d (list (d (n "cereal") (r "*") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "*") (d #t) (k 0)))) (h "1agapscp0dm8zx1p3ydnnkf0kkyg45lym0cq3qxgbpd817x10a9n") (f (quote (("serialisation" "cereal") ("nightly"))))))

(define-public crate-ecs-0.21.2 (c (n "ecs") (v "0.21.2") (d (list (d (n "cereal") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.3") (d #t) (k 0)))) (h "0cjlsxq2sbdrhqkv9xn2jjh804kz02ndqqc3vzkxdir5bg2rvqw6") (f (quote (("serialisation" "cereal") ("nightly"))))))

(define-public crate-ecs-0.22.0 (c (n "ecs") (v "0.22.0") (d (list (d (n "cereal") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.3") (d #t) (k 0)))) (h "0975kpmmn61bd3qqz6lwl0h5kwffi0wwggrd2cav7dkx9b2y6za8") (f (quote (("serialisation" "cereal"))))))

(define-public crate-ecs-0.22.1 (c (n "ecs") (v "0.22.1") (d (list (d (n "cereal") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (d #t) (k 0)))) (h "0q98zhqaji60kqqc96q3f356har0b535z87ikx6smdg37wjmdc1b") (f (quote (("serialisation" "cereal"))))))

(define-public crate-ecs-0.23.0 (c (n "ecs") (v "0.23.0") (d (list (d (n "cereal") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (d #t) (k 0)))) (h "0v9wkx01ji8hkd80fdn6pn4ln5pjlfh6l5nddaqlj89v8v4xn784") (f (quote (("serialisation" "cereal"))))))

(define-public crate-ecs-0.23.1 (c (n "ecs") (v "0.23.1") (d (list (d (n "cereal") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.4") (d #t) (k 0)))) (h "19ycn5djbfzgj6ck5llmzkfk8al2m6y7cclrjxsij02lwl2955sv") (f (quote (("serialisation" "cereal"))))))

