(define-module (crates-io #{3}# e ech) #:use-module (crates-io))

(define-public crate-ech-0.0.1 (c (n "ech") (v "0.0.1") (h "0c6cdz7n7hwkmly0qrsncm3wbhdffs26mlqim0x379sq54a93c7i")))

(define-public crate-ech-0.0.2 (c (n "ech") (v "0.0.2") (h "1chgyp3c50ljpym81zx45vlp608jw2c3hkb40nv0ck9p9l7mjir9")))

(define-public crate-ech-0.0.3 (c (n "ech") (v "0.0.3") (h "09k1p3rlffa9dqqsznngdfkjkn4rks8zk0ml6isrp514max3a2fa")))

(define-public crate-ech-0.0.4 (c (n "ech") (v "0.0.4") (h "1s5s8qz8p6k9qf3zciz5n4yl5yclgz24wy76w0cmrrkipwpdsspy")))

