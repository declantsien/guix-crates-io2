(define-module (crates-io #{3}# e ela) #:use-module (crates-io))

(define-public crate-ela-0.1.0 (c (n "ela") (v "0.1.0") (d (list (d (n "actix-web") (r "^1.0.0-alpha.6") (d #t) (k 0)) (d (n "futures") (r "^0.1.26") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1vqdxsa324ygdvddblb8pv58bn4nzn9iqah2iki3kszczk6a5fcl")))

