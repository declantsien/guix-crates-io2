(define-module (crates-io #{3}# e ecb) #:use-module (crates-io))

(define-public crate-ecb-0.0.1 (c (n "ecb") (v "0.0.1") (h "1ab02prdy6vi1y5wsrm0wjajzvr96jsjxkq3rr3gw2nvzbdxzfxw")))

(define-public crate-ecb-0.1.0 (c (n "ecb") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "02jvw6m0zqja1v1s0vsrarz2acjxpf8dd63ffnnmhy2h79bm7j2p") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("default" "block-padding") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

(define-public crate-ecb-0.1.1 (c (n "ecb") (v "0.1.1") (d (list (d (n "aes") (r "^0.8.1") (d #t) (k 2)) (d (n "cipher") (r "^0.4.3") (d #t) (k 0)) (d (n "cipher") (r "^0.4.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "18l1frsqg84c9ymn6shp0k51q7j6l95cpg3vw8g3a159h6x89z8p") (f (quote (("std" "cipher/std" "alloc") ("default" "block-padding") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

(define-public crate-ecb-0.1.2 (c (n "ecb") (v "0.1.2") (d (list (d (n "aes") (r "^0.8.3") (d #t) (k 2)) (d (n "cipher") (r "^0.4.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)))) (h "1iw1i0mwkvg3599mlw24iibid6i6zv3a3jhghm2j3v0sbfbzm2qs") (f (quote (("std" "cipher/std" "alloc") ("default" "block-padding") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

