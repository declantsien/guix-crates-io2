(define-module (crates-io #{3}# e evo) #:use-module (crates-io))

(define-public crate-evo-0.0.1 (c (n "evo") (v "0.0.1") (d (list (d (n "bit-vec") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "simple_parallel") (r "^0.3") (d #t) (k 0)))) (h "0d159mnigsqz2k7jbzmnabaz06mgj6ybr9yhaic88rh63xjfs9bh")))

