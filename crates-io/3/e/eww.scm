(define-module (crates-io #{3}# e eww) #:use-module (crates-io))

(define-public crate-eww-0.0.0-alpha.0 (c (n "eww") (v "0.0.0-alpha.0") (d (list (d (n "egui-wgpu") (r "^0.0.0-alpha.4") (d #t) (k 0)) (d (n "egui-winit") (r "^0.0.0-alpha.3") (d #t) (k 0)))) (h "12i8cxvkia7k95gg9ywm048wksrwm9zii7zhy3avmx6x170q7wqh")))

(define-public crate-eww-0.0.1-alpha.0 (c (n "eww") (v "0.0.1-alpha.0") (d (list (d (n "egui-wgpu") (r "^0.0.1-alpha.0") (d #t) (k 0)) (d (n "egui-winit") (r "^0.0.1-alpha.0") (d #t) (k 0)))) (h "1vnzh5sfy8yfibmc9j9iprd613z872n6pakmr6qp07sargb4z2h4")))

(define-public crate-eww-0.0.1-alpha.1 (c (n "eww") (v "0.0.1-alpha.1") (d (list (d (n "egui-wgpu") (r "^0.0.1-alpha.0") (d #t) (k 0)) (d (n "egui-winit") (r "^0.0.1-alpha.1") (d #t) (k 0)))) (h "0g55kny0n054rx781inafbhvva41g8wr6720921f18cd8caz6ma2")))

(define-public crate-eww-0.0.1-alpha.2 (c (n "eww") (v "0.0.1-alpha.2") (d (list (d (n "egui-wgpu") (r "^0.0.1-alpha.1") (d #t) (k 0)) (d (n "egui-winit") (r "^0.0.1-alpha.2") (d #t) (k 0)))) (h "0j583vh5kn0k1adsh0q8mdscadlsqximd9scs76sg2n7jy4x19bi")))

