(define-module (crates-io #{3}# e e6d) #:use-module (crates-io))

(define-public crate-e6d-0.1.0 (c (n "e6d") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "flume") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "strfmt") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio") (r "^0.3.5") (f (quote ("rt" "macros" "rt-multi-thread" "time" "fs"))) (d #t) (k 0)) (d (n "tokio-compat-02") (r "^0.1.2") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1.1") (d #t) (k 0)))) (h "10dnh1gzmjw1bnj0s2djc6wvirxj2q8nyhfnji03ck6wd8rnzgi6")))

