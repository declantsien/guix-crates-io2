(define-module (crates-io #{3}# e esc) #:use-module (crates-io))

(define-public crate-esc-0.2.0 (c (n "esc") (v "0.2.0") (d (list (d (n "snailquote") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "0h2jq9cwa1hkmw8aikk8f1rk8viwpi250vpzb1zb6cfgn6hp4i49")))

(define-public crate-esc-0.2.1 (c (n "esc") (v "0.2.1") (d (list (d (n "snailquote") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1aynyddqrjfhn6iwc8qfa8744ibggpmsbyw9vz725927avl6sfbb")))

(define-public crate-esc-0.2.2 (c (n "esc") (v "0.2.2") (d (list (d (n "snailquote") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (d #t) (k 0)))) (h "1vwaj8qbki0dy5f1dbg0xby7lf80qpq561qpi6gpp5yimqjba1h8")))

