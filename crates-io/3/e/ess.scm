(define-module (crates-io #{3}# e ess) #:use-module (crates-io))

(define-public crate-ess-0.3.1 (c (n "ess") (v "0.3.1") (h "1vdx6iqfgax25rx45b8nwg8vaczsllf47a63q0ki1rmzidm94rpj")))

(define-public crate-ess-0.4.0 (c (n "ess") (v "0.4.0") (h "17cy1w6skch104nhvz0j0na0hl2q52bdr3w8j8aix0kqn6hfdx28")))

(define-public crate-ess-0.4.1 (c (n "ess") (v "0.4.1") (h "1wii79rfgm4y3y8dg18d55p5vswwia9ly1lg3v5nczxj2dpsd7la")))

(define-public crate-ess-0.4.2 (c (n "ess") (v "0.4.2") (h "1l3i6ncglv186n7v7mxjmrbp6azyh3z7imqnv6jyvagz58xcw3n7")))

