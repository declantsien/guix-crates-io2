(define-module (crates-io #{3}# e ean) #:use-module (crates-io))

(define-public crate-ean-0.1.0 (c (n "ean") (v "0.1.0") (h "080j7g34jvn954d4100x21a2kyjplp8zsgmlrbfqrv2nk7zvg064")))

(define-public crate-ean-0.2.0 (c (n "ean") (v "0.2.0") (h "1ffqcgyrs8176vqxl611152kcd1div0bma0a4w5q4vqcpmrdzxsn")))

