(define-module (crates-io #{3}# e elp) #:use-module (crates-io))

(define-public crate-elp-0.99.0 (c (n "elp") (v "0.99.0") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "18ckj68q2x3c1gixz8swzrr0rlyn26fsh9zlcbgcdrxs6jlb2jby")))

(define-public crate-elp-1.0.0 (c (n "elp") (v "1.0.0") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "0f79is16d7h0kj7dir2y8nzcyww5n8wwb993fksw2jq3givmyr8g")))

(define-public crate-elp-2.0.0-a16485a (c (n "elp") (v "2.0.0-a16485a") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "1l7z070j0h6xmn076hds56ashdv8idinz5wyqnfnsrrvisryaqqa")))

(define-public crate-elp-2.0.0-6b9ae18 (c (n "elp") (v "2.0.0-6b9ae18") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "1cacj9wmyqg46l3l9s9m5vxmmj3myxal32il611rhrwglwvpcinx")))

(define-public crate-elp-2.0.0 (c (n "elp") (v "2.0.0") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3.5") (d #t) (k 0)))) (h "1npc5x0p3fc1vwalicmyb1bbsrx3n18il0nggrrb3wxrs524ipk6")))

