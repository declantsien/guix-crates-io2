(define-module (crates-io #{3}# e ema) #:use-module (crates-io))

(define-public crate-ema-0.1.0 (c (n "ema") (v "0.1.0") (d (list (d (n "half") (r "^1.7") (f (quote ("num-traits"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.5") (d #t) (k 0)))) (h "01ypibf7allfjq6q94b6xrzs6l8a0g6n2237wpkj4k9hd0a4v857")))

(define-public crate-ema-0.1.1 (c (n "ema") (v "0.1.1") (d (list (d (n "half") (r "^1.7") (f (quote ("num-traits"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^2.5") (d #t) (k 0)))) (h "1i6x9j4dsaad1cb3vj25vlih8xh37vv0ry6sk5iyf4r5b7f6g5n7")))

