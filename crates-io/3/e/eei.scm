(define-module (crates-io #{3}# e eei) #:use-module (crates-io))

(define-public crate-eei-0.0.0 (c (n "eei") (v "0.0.0") (h "1jkwa6l5vd24g7p8j9x55bkg5xwz2j14q4xhdhzh5fkj3jvi38jf")))

(define-public crate-eei-0.0.1 (c (n "eei") (v "0.0.1") (h "0x16k8fv2vhk588i6b0yb9vqnw297jbqidamgf3khj1530g031x8")))

(define-public crate-eei-0.0.2 (c (n "eei") (v "0.0.2") (h "0mz64iazjhi9bwcsi4ygyf7apa7lr31h2nhp7jhylw2mxlzvz9yx")))

(define-public crate-eei-0.0.3 (c (n "eei") (v "0.0.3") (h "13g60x42nz97qa6rv0ssybrvarmldzgrpkv43ggpl9d4ylxgxs86")))

