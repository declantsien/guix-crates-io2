(define-module (crates-io #{3}# e epi) #:use-module (crates-io))

(define-public crate-epi-0.7.0 (c (n "epi") (v "0.7.0") (d (list (d (n "egui") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0rr8p1mzyn4v10ljars59jv3760plsrb48b8hlzib3f7y588lsd9") (f (quote (("persistence" "serde" "serde_json") ("http") ("default"))))))

(define-public crate-epi-0.8.0 (c (n "epi") (v "0.8.0") (d (list (d (n "egui") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "0rg09ra4wq3ai9641law3qs75wirsifn9h6iprka72l9p7wfln73") (f (quote (("persistence" "serde" "serde_json") ("http") ("default"))))))

(define-public crate-epi-0.9.0 (c (n "epi") (v "0.9.0") (d (list (d (n "egui") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "023rw7ymfv0lwk4b4y5ydn80mc9mx7qgr7jw7yg6ra8961ccis3s") (f (quote (("persistence" "serde" "serde_json") ("http") ("default"))))))

(define-public crate-epi-0.10.0 (c (n "epi") (v "0.10.0") (d (list (d (n "egui") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "17h512j8sks8xkqp0fshvjz9dncpwxm54h14jk7g3d8jmfjdr4ih") (f (quote (("persistence" "serde" "serde_json") ("http") ("default"))))))

(define-public crate-epi-0.11.0 (c (n "epi") (v "0.11.0") (d (list (d (n "egui") (r "^0.11.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "09ynynwihzbl404g7gk3b6wh80111y6f2rl8slbgsr5w0gp2l962") (f (quote (("persistence" "ron" "serde") ("http") ("default"))))))

(define-public crate-epi-0.12.0 (c (n "epi") (v "0.12.0") (d (list (d (n "egui") (r "^0.12.0") (f (quote ("single_threaded"))) (k 0)) (d (n "ron") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "10vmqgiy5kr9i8xcras0b61a96cilnam5y44cf1qzz71xp5zdi2r") (f (quote (("persistence" "ron" "serde") ("http") ("default"))))))

(define-public crate-epi-0.13.0 (c (n "epi") (v "0.13.0") (d (list (d (n "egui") (r "^0.13.0") (f (quote ("single_threaded"))) (k 0)) (d (n "ron") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1v69553791879gqf29b0lrvvx79dl8xcys859ljsjgsm82ia812b") (f (quote (("persistence" "ron" "serde") ("http") ("default"))))))

(define-public crate-epi-0.14.0 (c (n "epi") (v "0.14.0") (d (list (d (n "egui") (r "^0.14.0") (f (quote ("single_threaded"))) (k 0)) (d (n "ron") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "12m249ng763ni4mx4qpsgyns18jvmxiip0xqk1m5c8frnk4k7h73") (f (quote (("persistence" "ron" "serde") ("http") ("default"))))))

(define-public crate-epi-0.15.0 (c (n "epi") (v "0.15.0") (d (list (d (n "directories-next") (r "^2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.15.0") (f (quote ("single_threaded"))) (k 0)) (d (n "ron") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "10pwyc7wfkb8akyc4w5idra66jvn68q6mjahqki8d6vz2844wpiz") (f (quote (("persistence" "ron" "serde") ("file_storage" "directories-next" "ron" "serde") ("default"))))))

(define-public crate-epi-0.16.0 (c (n "epi") (v "0.16.0") (d (list (d (n "directories-next") (r "^2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.16.0") (f (quote ("single_threaded"))) (k 0)) (d (n "ron") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0z4a4qa0rpyxrdx0cqb566xcc5h3a1zdgysabg6fmsqz4zilrbml") (f (quote (("persistence" "ron" "serde") ("file_storage" "directories-next" "ron" "serde") ("default")))) (r "1.56")))

(define-public crate-epi-0.17.0 (c (n "epi") (v "0.17.0") (d (list (d (n "directories-next") (r "^2") (o #t) (d #t) (k 0)) (d (n "egui") (r "^0.17.0") (f (quote ("single_threaded"))) (k 0)) (d (n "ron") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0vhp32wxbvvwrjz2h4phxr1x6bcl5rzkz3a863x2jkggrkg4am69") (f (quote (("persistence" "ron" "serde" "egui/persistence") ("file_storage" "directories-next" "ron" "serde") ("default")))) (r "1.56")))

