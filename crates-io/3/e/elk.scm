(define-module (crates-io #{3}# e elk) #:use-module (crates-io))

(define-public crate-elk-0.3.0 (c (n "elk") (v "0.3.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "config_struct") (r "^0.5.0") (f (quote ("toml-parsing" "yaml-parsing"))) (d #t) (k 1)) (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "filesize") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "terminal_size") (r "^0.1.15") (d #t) (k 0)))) (h "0l7b6nzjmj29y99zly1y8ylrx7sgabr6qhcp4710sgx1wikcljfy")))

(define-public crate-elk-0.3.1 (c (n "elk") (v "0.3.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "config_struct") (r "^0.5.0") (f (quote ("toml-parsing" "yaml-parsing"))) (d #t) (k 1)) (d (n "dirs") (r "^3.0.1") (d #t) (k 1)) (d (n "filesize") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "terminal_size") (r "^0.1.15") (d #t) (k 0)))) (h "12i1islxvm84w3hpx76yy1nrvf1xasqzshlkxi4229hhgjrji1yh")))

