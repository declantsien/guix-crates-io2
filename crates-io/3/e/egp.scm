(define-module (crates-io #{3}# e egp) #:use-module (crates-io))

(define-public crate-egp-0.1.0 (c (n "egp") (v "0.1.0") (d (list (d (n "petgraph") (r "0.*.*") (d #t) (k 0)) (d (n "rand") (r "0.*.*") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1zh6fz8xr8h2zlkzq2dh9lrrflsic7vc2npjjsf941wk6gsjwbl0")))

(define-public crate-egp-0.1.1 (c (n "egp") (v "0.1.1") (d (list (d (n "petgraph") (r "0.*.*") (d #t) (k 0)) (d (n "rand") (r "0.*.*") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "12jjb8cgssg8pxnfi6g2rsvwk6cmih7rf1hr9ild20mp3rw6d3wl")))

(define-public crate-egp-0.1.2 (c (n "egp") (v "0.1.2") (d (list (d (n "petgraph") (r "0.*.*") (d #t) (k 0)) (d (n "rand") (r "0.*.*") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "19rhqkvz798mwcx1j84zf4fngkss2lfivb2ccq8r73jm25p80dbd")))

