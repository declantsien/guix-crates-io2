(define-module (crates-io #{3}# e eom) #:use-module (crates-io))

(define-public crate-eom-0.8.0 (c (n "eom") (v "0.8.0") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.10") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.6") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1wrg0xjwk0ddvbjrdwcacms7sb0i33mxla4x57vzzx38fg0bsmwd")))

(define-public crate-eom-0.9.0 (c (n "eom") (v "0.9.0") (d (list (d (n "derive-new") (r "^0.4") (d #t) (k 0)) (d (n "fftw") (r "^0.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.10") (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.6") (d #t) (k 0)) (d (n "num-complex") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "05n85pyx597864gcfx8pcc34gqjva4fa1zn1kgr4xqw69bsq5xiy")))

(define-public crate-eom-0.10.0 (c (n "eom") (v "0.10.0") (d (list (d (n "derive-new") (r "^0.5") (k 0)) (d (n "fftw") (r "^0.4") (o #t) (k 0)) (d (n "ndarray") (r "^0.11") (k 0)) (d (n "ndarray-linalg") (r "^0.9") (o #t) (k 0)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)))) (h "13pbm06nph8dm88lj3mzp83mjjls7j8l42xqzhbrwd3qlqd0gyyi") (f (quote (("static" "ndarray-linalg/openblas" "fftw/source") ("intel-mkl" "ndarray-linalg/intel-mkl" "fftw/intel-mkl") ("default" "intel-mkl"))))))

(define-public crate-eom-0.11.0 (c (n "eom") (v "0.11.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "derive-new") (r "^0.5.9") (k 0)) (d (n "fftw") (r "^0.8.0") (k 0)) (d (n "katexit") (r "^0.1.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (k 0)) (d (n "ndarray-linalg") (r "^0.16.0") (k 0)) (d (n "num-complex") (r "^0.4.3") (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "00a4qvpagb9ryh2mjr7zkivcjf9fcpkga5s3r78ih98va7jsd0s8") (f (quote (("oss" "ndarray-linalg/openblas-static" "fftw/source") ("intel-mkl" "ndarray-linalg/intel-mkl-static" "fftw/intel-mkl") ("default"))))))

