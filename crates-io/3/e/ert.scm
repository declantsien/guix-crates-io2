(define-module (crates-io #{3}# e ert) #:use-module (crates-io))

(define-public crate-ert-0.1.0 (c (n "ert") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0dlg5c5c114crvgpdblhgmw1wzldyhl54c4as6pad9vf1jxdagxm")))

(define-public crate-ert-0.2.0 (c (n "ert") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "17s0vjq2kqhl6080bxb0nb8rdfyh7nvmih7kl2vq6449l5z0ysag")))

(define-public crate-ert-0.2.1 (c (n "ert") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1srqkydvs6cnjqiigmkj3s6i2b4s18hzh129ss05l5gc021ra4n4")))

(define-public crate-ert-0.2.2 (c (n "ert") (v "0.2.2") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "000nmm0lwdv0krjr9b77kmxg6ifkg6rrr6lfb1h0rfcilybaam5g")))

