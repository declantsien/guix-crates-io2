(define-module (crates-io #{3}# e eon) #:use-module (crates-io))

(define-public crate-eon-0.1.0 (c (n "eon") (v "0.1.0") (h "15w9z4b6vynwks8wf99j11vknf5kqz743035nzz3ajbysvdsv93n") (y #t)))

(define-public crate-eon-0.1.1 (c (n "eon") (v "0.1.1") (d (list (d (n "lalrpop") (r "^0.19.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.5") (d #t) (k 0)) (d (n "mlua") (r "^0.5.3") (f (quote ("luajit" "vendored"))) (d #t) (k 0)))) (h "0ncv6cgrp19k9332wmxrzf9z3zs40i0vaf5xazm2kckx50hr193a") (y #t)))

(define-public crate-eon-0.1.2 (c (n "eon") (v "0.1.2") (d (list (d (n "lalrpop") (r "^0.19.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.5") (d #t) (k 0)) (d (n "mlua") (r "^0.5.3") (f (quote ("luajit" "vendored"))) (d #t) (k 0)))) (h "0wd9ssx93smggy98hrk8bxdlpij99x4a6k1n7z413p47x177ddf7") (y #t)))

