(define-module (crates-io #{3}# e ent) #:use-module (crates-io))

(define-public crate-ent-0.0.1 (c (n "ent") (v "0.0.1") (d (list (d (n "pyo3") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1a8i11w4lfrq7fiffk5mbanhfwvq9m389afaqr6vhh5s66p4fyyi") (f (quote (("python" "pyo3") ("json" "serde_json") ("default"))))))

(define-public crate-ent-0.0.2 (c (n "ent") (v "0.0.2") (d (list (d (n "pyo3") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1ad5bs78sk4cqxxxsrlsyaxjknp6l93yzw3qj2824gvrl0hkzs7a") (f (quote (("python" "pyo3") ("json" "serde_json") ("default"))))))

(define-public crate-ent-0.0.3 (c (n "ent") (v "0.0.3") (d (list (d (n "pyo3") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1awnn0vwsl77sq9h4y1qqkkq1ppa8qam47cls211an89ncq7vxb8") (f (quote (("python" "pyo3") ("json" "serde_json") ("default"))))))

(define-public crate-ent-0.0.4 (c (n "ent") (v "0.0.4") (d (list (d (n "pyo3") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)))) (h "1yxrskyypxws2b9zxkvj3kmkjwvk8368qnnclqnrmsk36yxfwg4k") (f (quote (("python" "pyo3") ("json" "serde_json") ("default"))))))

