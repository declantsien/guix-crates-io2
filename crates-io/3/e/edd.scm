(define-module (crates-io #{3}# e edd) #:use-module (crates-io))

(define-public crate-edd-0.1.0 (c (n "edd") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lihlb7459wvf793lw31s4ai5rd26imqa2fgqkj2msmz7jihcbwr")))

(define-public crate-edd-0.1.1 (c (n "edd") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xzqay1cyby8q0xm7gyqg0hrscmnnqqynphznbclhyyhh5my6fyr")))

(define-public crate-edd-0.1.2 (c (n "edd") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01nxd9ql2j9yrkq4afxknlgsf82p3j70hlrkvc5k38581vlapgqg")))

(define-public crate-edd-0.1.3 (c (n "edd") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r2y1lr3fxfn3w0i3i348wqf6wpg1qmpp72c1ih68xh3w5lq6ivc")))

