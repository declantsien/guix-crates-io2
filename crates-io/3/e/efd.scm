(define-module (crates-io #{3}# e efd) #:use-module (crates-io))

(define-public crate-efd-0.1.0 (c (n "efd") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.1") (d #t) (k 0)))) (h "0b1s4p8gbvpv5pp24xgfvdn2yvnmiapax0lm62zpcmvi1p1maxg5")))

(define-public crate-efd-0.2.0 (c (n "efd") (v "0.2.0") (d (list (d (n "ndarray") (r "^0.15.1") (d #t) (k 0)))) (h "1j2x1spax4lwfilf3m2zyzhh8hwpgsdvmwwcr5i5bn712q23f8qs")))

(define-public crate-efd-0.3.0 (c (n "efd") (v "0.3.0") (d (list (d (n "ndarray") (r "^0.15.1") (d #t) (k 0)))) (h "0swijhjc2p6vi3kgygph2yasafl41wzq65fqmfyrvf9av54i31bq")))

(define-public crate-efd-0.4.0 (c (n "efd") (v "0.4.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "155ibv4zpb87njf9nzyp4g7hcc3b22fyl3d2jjp8a8lxljsmcwb6")))

(define-public crate-efd-0.5.0 (c (n "efd") (v "0.5.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1370mqxar5qfpk52axvax1ms97yp691prnp1pwimpbrb33162wln")))

(define-public crate-efd-0.5.1 (c (n "efd") (v "0.5.1") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0wc5qn900z08b7j0lsy7mc0rhfa9f6a12l27f42hkhg5fkyq7wlm")))

(define-public crate-efd-0.5.2 (c (n "efd") (v "0.5.2") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1vn386n8rp4sb4rq8p1i02axcnvma0k048byz2binp6srqxfv5n9")))

(define-public crate-efd-0.5.3 (c (n "efd") (v "0.5.3") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "088v0jphd85004a07mn7f6hfln9hzqcyyrrm34sv9gcb5lf5kid4")))

(define-public crate-efd-0.6.0 (c (n "efd") (v "0.6.0") (d (list (d (n "ndarray") (r "^0.15") (d #t) (k 0)))) (h "1hmj8nlnvmrd8hhfy6aj18ljfr82xafh2qlrgmnvpf65haa3f43b")))

(define-public crate-efd-0.7.0 (c (n "efd") (v "0.7.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "0r63czixbi7hzspm8hcpcwl22899x2cpdgkzijvmxjbyc0xsaasw") (f (quote (("std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.8.0 (c (n "efd") (v "0.8.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "1lahyf86llyywf5n4cvf2r3jng1m2r67xrjz4hnn3fq0gqpagnqc") (f (quote (("std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.9.0 (c (n "efd") (v "0.9.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "0vkfxi5sycp5p5yc7zaf3f9zz0zlil07msfpkgzi6dc52szsb3b8") (f (quote (("std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.10.0 (c (n "efd") (v "0.10.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "1j7ajj0l10mwhj8i9g7qpkfvfqg5gx200dgxms17swsln4rz2qnx") (f (quote (("std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.11.0 (c (n "efd") (v "0.11.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "0ky4blp0a04s03bn7nji21m4yfy2rg2sr5qkcjjxhrjyjavkzhnh") (f (quote (("std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.11.1 (c (n "efd") (v "0.11.1") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "1bqjy5binmh4svkxq3g1y3v15wsad59x3k94353lj93s13qrmq3y") (f (quote (("std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.11.2 (c (n "efd") (v "0.11.2") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)))) (h "1xdgildi1a6n6nqk90zwhg7gyry9kv6dkigjcm0ska9h41w1n409") (f (quote (("std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.12.0 (c (n "efd") (v "0.12.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1m4g1rrzwnzsffwn0n5pck6pw09jg9qqdd750jj5q0wxjmr5hc81") (f (quote (("std" "ndarray/std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-efd-0.13.0 (c (n "efd") (v "0.13.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "03z163cvma467jpw3kdbxj9bln6z81rjybhksrr937rwnnrvqva3") (f (quote (("std" "ndarray/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-0.14.0 (c (n "efd") (v "0.14.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0vgk5vmdr03bjjxlg7myjp2hhr1lm12prsz2q6hjblylpfqzw5yj") (f (quote (("std" "ndarray/std" "num-traits/std") ("default" "std")))) (y #t)))

(define-public crate-efd-0.15.0 (c (n "efd") (v "0.15.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "11csza2nkbm2f95jb8siivs0a2cdyx6z411w5zc1s919xkc8pvqm") (f (quote (("std" "ndarray/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-0.15.1 (c (n "efd") (v "0.15.1") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "05z61m00y7j0h8qd5cvljhwbh5inzcgz8k1n9w6v9ra8f9x0v7k6") (f (quote (("std" "ndarray/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-0.15.2 (c (n "efd") (v "0.15.2") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1iwcvy89finx4a2a6jlsyr5rmsi8b5l6dbvp2wszayf208rxnv8g") (f (quote (("std" "ndarray/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-0.15.3 (c (n "efd") (v "0.15.3") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "18v1k4hdskms2agx69libwyd3gynadp7ic5lr9sz7z54daghm372") (f (quote (("std" "ndarray/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-0.15.4 (c (n "efd") (v "0.15.4") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "19ij5k2nkcw3i63krip7vhakbs8bk7kcf8sw0pyg6l5q3ahxj2h8") (f (quote (("std" "ndarray/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-0.15.5 (c (n "efd") (v "0.15.5") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0dnfw2r710543f5azw70g5nnqaalvy85slsxr994j7cdv9fy93ky") (f (quote (("std" "ndarray/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-0.16.0 (c (n "efd") (v "0.16.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "03bdgk5zs8ikvmc5dvhssyl13hlymlhyjv6x3zasijnb9fnf9bp1") (f (quote (("std" "ndarray/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-0.16.1 (c (n "efd") (v "0.16.1") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "14vvl68c6drws3rc8a93nl5zbgxz7r3ka4p7pzzckx4xn7sngkv4") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.16.2 (c (n "efd") (v "0.16.2") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "11cz302kxd8dwcz6sy5w7l1xy2wf6j6ln4zsm3q5jzzx1k97czpn") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.17.0 (c (n "efd") (v "0.17.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0lgrrn4qx8dlyr1v8p8px6ip1b9w8nj2wy98algirprr903dc0x0") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.17.1 (c (n "efd") (v "0.17.1") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1q40j4hhqlbllb8iwrs1xqyf42fvddi42salrc8nbc5vihw30f5h") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.17.2 (c (n "efd") (v "0.17.2") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0b069x1l7w3zszhnrck85599rafbh5j1xws7qrp5q1bd621s8j3f") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.17.3 (c (n "efd") (v "0.17.3") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0y18ryljqcsfhgxk2ql00q7knrlzpd4n1zxl99iifr2ghkypz4jy") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.18.0 (c (n "efd") (v "0.18.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0a0s34x6q4lk77kx8xagdkpw94rmpn0cw0bq8b97yvmzndzzwhkm") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.18.1 (c (n "efd") (v "0.18.1") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "07c6bjw61k0lip8dlvgjdzxvkgjkicrja6rvlcxj4gl1qgslg9c0") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.18.2 (c (n "efd") (v "0.18.2") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1gmbdb6gsb7y7h0a3237by8gnbylb6vw45g7568zl026d8pf8nqj") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.19.0 (c (n "efd") (v "0.19.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "04hk9d0ppv1gdzplykflvwcjlkzlylb916k4rs1fgdzkm4fj0whx") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.20.0 (c (n "efd") (v "0.20.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0frsvnsfdl6ky753nkczavykn770xnkn22pssvjnyz25kxhiqlgx") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.21.0 (c (n "efd") (v "0.21.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0af7azanz4v43arzdipsv06rr0b4w5gfrsilm1zp2zrh0d7mmxjh") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.22.0 (c (n "efd") (v "0.22.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1hmwk0psgzm9wh0wgndkrwkf8x4nsvn7zva0qx725nphqfmz1qlz") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.23.0 (c (n "efd") (v "0.23.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "163kq3r2bk2rsjgp4h03w5iryjibnm9862g03y6cmc521sjczhgs") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.23.1 (c (n "efd") (v "0.23.1") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "16p3n36wqpj7p8gfry6jkar7x7gjlrd01lj0k2gpvbalvwsjfg9b") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std")))) (y #t)))

(define-public crate-efd-0.23.2 (c (n "efd") (v "0.23.2") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0yfycjjqhiqiawhv4srvh9yk60473261axqcjxcwb8xd61090ln4") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std")))) (y #t)))

(define-public crate-efd-0.23.3 (c (n "efd") (v "0.23.3") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0f4r3yiw50kanx4mvw14vc9yqjn270g4q6y7irkwvy6ggv0gxr3a") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.24.0 (c (n "efd") (v "0.24.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0fp3a89rbi4i9f8xjn727s2gcaqanxfazrsgiblp29z6n9y2pa6i") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.25.0 (c (n "efd") (v "0.25.0") (d (list (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "132mnagcjd6wcwacmj7rkwzyl2qw8r7bdvpg6yrd6ydzkjirbqbv") (f (quote (("std" "num-traits/std" "ndarray/std") ("default" "std"))))))

(define-public crate-efd-0.26.0 (c (n "efd") (v "0.26.0") (d (list (d (n "nalgebra") (r "^0.31") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0724ip2r443nscrqq40w6l02my6rz1j6f1sgjdzidr02dsq3gk5c") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std")))) (y #t)))

(define-public crate-efd-0.26.1 (c (n "efd") (v "0.26.1") (d (list (d (n "nalgebra") (r "^0.31") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0qixi4hjqa8y8y8wfm0ldzhmr7cm0hdpjjssdxk5aifp66zv5gin") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std")))) (y #t)))

(define-public crate-efd-1.0.0 (c (n "efd") (v "1.0.0") (d (list (d (n "nalgebra") (r "^0.31") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0p24b2abz4srd4wbhl08qvrkli7331akv94x4nb65ck6i2ymqwk2") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-1.0.1 (c (n "efd") (v "1.0.1") (d (list (d (n "nalgebra") (r "^0.31") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1kx4gp43hzza23scip7k22mkbdah208g0kdp9x3znr9cpk9ndwx1") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-1.0.2 (c (n "efd") (v "1.0.2") (d (list (d (n "nalgebra") (r "^0.31") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1cq099jmvrzz57a4rpb3cdhrs3bm7pay264licihxb6rnynfzlfw") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-1.0.3 (c (n "efd") (v "1.0.3") (d (list (d (n "nalgebra") (r "^0.31") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1s5jy4shmvv8rnmqf1jnh2z91ksmm5144jgj18albyf9z9llrnl2") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-1.0.4 (c (n "efd") (v "1.0.4") (d (list (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0smc4zsf5vlrf57v8d0vkmqsawxx6vbnq2j4rin9h9xv0x9gnk2y") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-1.1.0 (c (n "efd") (v "1.1.0") (d (list (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1hsv96ljvxwj5zhjva2y8rp3lyy3518d389f0pl1ccjki57z0g9n") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-1.1.1 (c (n "efd") (v "1.1.1") (d (list (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1fi5y5nm4lmn7gglqcy5ykhl0nl9rl976jx1yvb8zlm5hcvsp188") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-1.2.0 (c (n "efd") (v "1.2.0") (d (list (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0s0r6zpdq36aqbqrx180x3mc2kr8bgmi2lsiv13y82s8zwds7s5h") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-1.3.0 (c (n "efd") (v "1.3.0") (d (list (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0h37d155dnvfx82xp81lklbr0a35j6ax4y5rq64db4k6yfh8dwjk") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-1.3.1 (c (n "efd") (v "1.3.1") (d (list (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "01xd757c66ifyw0lfqi7jx7syf5n5wnbaq95gr069w3jy63mfwyz") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-1.3.2 (c (n "efd") (v "1.3.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1a5pza5xv4dr827621rjyqcnff90z5nbhss5dqll7ny57cdvr2x1") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std")))) (y #t)))

(define-public crate-efd-2.0.0 (c (n "efd") (v "2.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "08dimq5d5pnjml6d1azcj4mhygzvg5kngivbmw7mn0af1plrfngb") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-2.0.1 (c (n "efd") (v "2.0.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "01hvaxbqz4bbvybi361gpsk93h5v0qqca7ddz6xpfi124jgwr5jd") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-2.0.2 (c (n "efd") (v "2.0.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1aqaa1vpp98xp4s9r0mhw9vflpsd7nxm4bqh9wx4ni3fncjxhcq8") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-2.1.0 (c (n "efd") (v "2.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1l5bijal8vni5vb8083wls1ji72f1i05l6cjms544hl4isl872g5") (f (quote (("std" "num-traits/std" "ndarray/std" "nalgebra/std") ("default" "std")))) (y #t)))

(define-public crate-efd-2.1.1 (c (n "efd") (v "2.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0lpsg56rxjsqdwqc4v6ayf61pfmdz98n267kjb3x8dpfmimqpw99") (f (quote (("std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-2.2.0 (c (n "efd") (v "2.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "19nbw3mbf8xwkgm77b4p3cmw4cm9nk1110n1vkgz06r49d46jgkj") (f (quote (("std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-2.2.1 (c (n "efd") (v "2.2.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0pavv0wbx4b2fx35a85bw9dk2dr3wc2gl3a6ipfrj2a4pidhgw4c") (f (quote (("std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-2.2.2 (c (n "efd") (v "2.2.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0gi7fghnpkagldlgfcrhpkism47g0imkl11653n3lsvswis481gw") (f (quote (("std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-2.3.0 (c (n "efd") (v "2.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "18kvg47wbgivavbgb7nkx1pscrclc37apa70wlhfb2qx56nykdn1") (f (quote (("std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-2.3.1 (c (n "efd") (v "2.3.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "00djvdprl39a518s7ncdlz7zmv0j71clasazpd7cxaks79rz425i") (f (quote (("std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-2.3.2 (c (n "efd") (v "2.3.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros"))) (k 0)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0czq63rpjvzh9fcjfwq35q17qvkr2hfcy05dsmj344r288dxdcbk") (f (quote (("std" "ndarray/std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-3.0.0 (c (n "efd") (v "3.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0i3pvgawi8qip06j3z61wclhd9mpr9wbl7mha19x64x1c1rbfqjs") (f (quote (("std" "nalgebra/std") ("default" "std")))) (y #t)))

(define-public crate-efd-3.0.1 (c (n "efd") (v "3.0.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0jlz9qld0hia14k9ncyq2yk904mxky58kz3xzw34yn9b4mx1cml6") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-3.0.2 (c (n "efd") (v "3.0.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0aps59kq93wdlp3z020mfb2kji1d81r2x4clj8pzqn9kkxnfgn8q") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-3.0.3 (c (n "efd") (v "3.0.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0p866fgxymf9srqyhw0yslhrfnxvckc2m8s33h9rgmfv5ws8mpsn") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-3.1.0 (c (n "efd") (v "3.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1pybdsfzmssk2b6xlyq4c7075xwb0z28hz44p1jc19yc99rmq3vq") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-3.1.1 (c (n "efd") (v "3.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "098d07r1l2xfnwsa21mp0nasyhg2c9if1z1cl4jsjrlcx1xaqq8s") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-3.2.0 (c (n "efd") (v "3.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "09i8ihmprrsmw36lrhy1xs43sp0pk7yvahk63k32yzfph0qppaid") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-3.2.1 (c (n "efd") (v "3.2.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0yvllr7cyxzqdb6wmddn7jrmn8w28wsxbgs0v2q6pl1pmnrvxx61") (f (quote (("std" "nalgebra/std") ("default" "std")))) (y #t)))

(define-public crate-efd-3.2.2 (c (n "efd") (v "3.2.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0vfx723bdlv4hv81jdxl0sr7f6hqx0xn96rrvxn0p51sbpdkbg7z") (f (quote (("std" "nalgebra/std") ("default" "std")))) (y #t)))

(define-public crate-efd-3.2.3 (c (n "efd") (v "3.2.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1qmcqrq5k47s77fgaj8fi3f88zg07ylxy7w0fqjyfgafcbchp25x") (f (quote (("std" "nalgebra/std") ("default" "std")))) (y #t)))

(define-public crate-efd-3.2.4 (c (n "efd") (v "3.2.4") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "18vprfrk8l68vscsz4l0q03ishjxm91w37q66gvycydbwja3r85m") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-3.3.0 (c (n "efd") (v "3.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "16i6l4jcn8b224cn1k6icxqbv1k0amrxpd89v13q154h9hgrfcig") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-4.0.0 (c (n "efd") (v "4.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "129nhjx7mgwxa71rfq7ch8xfmy9fshlsbvdy48vm067rqs35jpl7") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-4.0.1 (c (n "efd") (v "4.0.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0ra5zvsw5n364yajaz4n92apixm0mbczwwl75igdnj4l54f5xh9x") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-4.1.0 (c (n "efd") (v "4.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "interp") (r "^1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "19k051iphmyx2ryf5d56k60rsihsyhkw0kzffwd7wzccz6idrmzd") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-5.0.0 (c (n "efd") (v "5.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "09cppqsqwxrq2pxx4adc3a51fc3qkrqjmdjdjzspy9kq3c5cfrsy") (f (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-efd-5.0.1 (c (n "efd") (v "5.0.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0d909yain7pk1v90biiqk2jz2l3nipwqqby4bgf29r6i2cld7fyl") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-6.0.0 (c (n "efd") (v "6.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1iyaspzqp2awzswnl84qkfgfdx7wm01fd40h6axg0jwls7q1815m") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-7.0.0 (c (n "efd") (v "7.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "159sy58jpwma5rlfff2pxq22j66780w4953wcrvzvw601marhkcx") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std")))) (y #t)))

(define-public crate-efd-7.1.0 (c (n "efd") (v "7.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1xk1l7r0wbgmbaydfa80b4w45c73n0pcqy7jgw70fv839gqg88ks") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std")))) (y #t)))

(define-public crate-efd-7.2.0 (c (n "efd") (v "7.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1gdsrif51w2g3lc87ba459zzrs093dsq7kib2hld5k67k199808v") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-7.2.1 (c (n "efd") (v "7.2.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "19fbmxgxlsf6b73y9gn4n0wigl3r3vjvwajrzs7fxkwvbzw7siw7") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-7.2.2 (c (n "efd") (v "7.2.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "107g7qvsvrd949ab364v5zyfdc1pw10rhwnv3bz6y78axi0zilxl") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-7.3.0 (c (n "efd") (v "7.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1sx4gdxakdi857k22r1ip98hd3xylcwvihc1gf3mrgpc9c3g9j2k") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std")))) (y #t)))

(define-public crate-efd-7.3.1 (c (n "efd") (v "7.3.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0q2jli2zy4jg5qq19b4ny9mzw1fsrmhk8agpbpar52avnywxjzzc") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-7.4.0 (c (n "efd") (v "7.4.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1fsfglr3zk5v96z7winpcarv3cc6v6dr6sfzv9x0q9j4c3b0xaq6") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-7.5.0 (c (n "efd") (v "7.5.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1dhj5whfdvq54klaxpiwlbyk96anplsjiigskljnp2z1rqc3lxsa") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-8.0.0 (c (n "efd") (v "8.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "14rhqbbd85mabwqvyfszj4amgmk7nghd4q0fb5yfidknvhwcwb38") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std")))) (y #t)))

(define-public crate-efd-8.0.1 (c (n "efd") (v "8.0.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0byyvm0ss8vj9545p8pbmz0dmg8s556g9mjqip6m120n76f7riyg") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-9.0.0 (c (n "efd") (v "9.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0qn7lkdwkwihxrjv4xy52shckcp19p59a8b5yw1c32av9qkja834") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-9.1.0 (c (n "efd") (v "9.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "14zccfd97gkn5481fh9mzdi78gh0zbsnkpxqml9866ycc72pispy") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-9.1.1 (c (n "efd") (v "9.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0zggrlmsggidaypm62856d741gf1dqwpr7p20a8lgpxvg6dwwpvy") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-9.2.0 (c (n "efd") (v "9.2.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0ypx46d83w5wgm41mlrcgbk6zwfm62ll1zb4dcrzsk9px30rjwxr") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-9.2.1 (c (n "efd") (v "9.2.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "127bvk2bn25j81an4nni2swfspqlfxsv62k6q84996j6z2x7dm4v") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-9.3.0 (c (n "efd") (v "9.3.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "07552zjxh1aijg8linn8dx80940h535vf0l38174j1pkgsqal162") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-9.3.1 (c (n "efd") (v "9.3.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0rmiq9b3hahcrq4lvvbw79b7xsb3jcr3zwv57zr965r4ah28prd0") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-10.0.0 (c (n "efd") (v "10.0.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "13240h6nb3c6g8k1kvcfmhhf0g5r2y46m0hcmc9xxi1g2ljdr6c8") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std")))) (y #t)))

(define-public crate-efd-10.0.1 (c (n "efd") (v "10.0.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0j2rx4ba1sychzq6lmjyfp96w5syz0arxl8jwpkgiwf0abpak3dj") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-10.1.0 (c (n "efd") (v "10.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1yvcq63s16qqs1n8l583qlpqpxjsyim34mggp6cja7rpv3yjgpmw") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-10.1.1 (c (n "efd") (v "10.1.1") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "1bcq1imfc4yww6fwdbq5zbyd8303hm5b4mhq1lmyi9g2mdg3l9wc") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-10.1.2 (c (n "efd") (v "10.1.2") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "078538d3hvhqqnis9450i4gfcnsrqys75pw786a0n80qj61ddsha") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

(define-public crate-efd-10.1.3 (c (n "efd") (v "10.1.3") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (f (quote ("libm" "macros" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 2)))) (h "0zzxnjp1pb9qsvrx5r4kv8qhzbk6187h0kjyla7yz6f8cw4wiryz") (f (quote (("std" "nalgebra/std" "num-traits/std") ("default" "std"))))))

