(define-module (crates-io #{3}# e efw) #:use-module (crates-io))

(define-public crate-efw-0.0.0 (c (n "efw") (v "0.0.0") (h "1r0qapy9g62zy87i79c3br1rmj9wiv5gb8vrsq47s5gfdwwfgagp")))

(define-public crate-efw-0.1.0 (c (n "efw") (v "0.1.0") (d (list (d (n "late-static") (r "^0.3") (d #t) (k 0)) (d (n "r-efi") (r "^2.1") (d #t) (k 0)) (d (n "ucs2") (r "^0.1") (d #t) (k 0)))) (h "1w35c8lmmzv728iwvfq7lmhrw8wim9nh5kkl57cm6hnjk7f3lpiw")))

