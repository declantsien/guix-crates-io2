(define-module (crates-io #{3}# e egl) #:use-module (crates-io))

(define-public crate-egl-0.1.0 (c (n "egl") (v "0.1.0") (d (list (d (n "khronos") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0y3nkwh9yx97bdxlgh89k2bpskmx37pgqsfppslqf4p5i4s1k53x")))

(define-public crate-egl-0.2.0 (c (n "egl") (v "0.2.0") (d (list (d (n "khronos") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "00lz3v9xxbif9r9b4885hh10y5wxy92q64ybgi0c6yhk16dcxhwz")))

(define-public crate-egl-0.2.1 (c (n "egl") (v "0.2.1") (d (list (d (n "khronos") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "1f6y9waxd5g9pi1j5yadzgrv52rc2yp76qcygf1pplwryvbxrsin")))

(define-public crate-egl-0.2.2 (c (n "egl") (v "0.2.2") (d (list (d (n "khronos") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "1qx9319h96s5if1i4sdq6lc50yi22p5zzix5xy6h64ilw2457rgk")))

(define-public crate-egl-0.2.3 (c (n "egl") (v "0.2.3") (d (list (d (n "khronos") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "08bqa5q20d7gw821ysdyc275qg3ncsiq8ilx93agkfadvn1bj6k6")))

(define-public crate-egl-0.2.4 (c (n "egl") (v "0.2.4") (d (list (d (n "khronos") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "07bggl28jx93k1scb82d8i1sqf4gbh1y23bpw0cacwsh22lxm8xf")))

(define-public crate-egl-0.2.5 (c (n "egl") (v "0.2.5") (d (list (d (n "khronos") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.1.10") (d #t) (k 0)))) (h "0scb7yl0b85j191cvzi28c6bxp4h8bp3zsxnmp0zgmf98d36s9za")))

(define-public crate-egl-0.2.6 (c (n "egl") (v "0.2.6") (d (list (d (n "khronos") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "0ln9gc3ma4zp2xw6xwbqqb0a97vjll2d3m0ln2plr2aqhx7r331z")))

(define-public crate-egl-0.2.7 (c (n "egl") (v "0.2.7") (d (list (d (n "khronos") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)))) (h "0bbmn2j2j7qpc7rhkv740sfx080w3n9lbcnibgqiy2r08jcbqwx3")))

