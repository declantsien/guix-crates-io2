(define-module (crates-io #{3}# e edn) #:use-module (crates-io))

(define-public crate-edn-0.1.0 (c (n "edn") (v "0.1.0") (d (list (d (n "ordered-float") (r "^0.0.2") (d #t) (k 0)))) (h "1gfjkj6cdra8viffng2rdl4ji21wcqprzh2q7qvbr5fjcdhjicrm")))

(define-public crate-edn-0.1.1 (c (n "edn") (v "0.1.1") (d (list (d (n "ordered-float") (r "^0.0.2") (d #t) (k 0)))) (h "1ix0amja7jm5ixia2sd6si5l9rvbadw4lh8vycn07i0plwn6lyh1")))

(define-public crate-edn-0.2.0 (c (n "edn") (v "0.2.0") (d (list (d (n "ordered-float") (r "^0.4.0") (d #t) (k 0)))) (h "198qm6vqhzhzkn3hd2kx68ygb56nscpxkyd728gncx6bni6sp2s7")))

(define-public crate-edn-0.3.0 (c (n "edn") (v "0.3.0") (d (list (d (n "ordered-float") (r "^0.4.0") (d #t) (k 0)))) (h "04iq324d1gcfx4ff705gixbycv7d6j29dw2wxhvnnzqpfxs5miki")))

