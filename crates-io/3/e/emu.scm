(define-module (crates-io #{3}# e emu) #:use-module (crates-io))

(define-public crate-emu-0.1.0 (c (n "emu") (v "0.1.0") (d (list (d (n "emu-audio-types") (r "^0.1.0") (d #t) (k 0)) (d (n "emu-core-audio-driver") (r "^0.1.0") (d #t) (k 0)))) (h "1s4xyzyavfyih386704sc3w58mkqdk79m4mj73gmb2gwrirlgpyk")))

(define-public crate-emu-0.1.1 (c (n "emu") (v "0.1.1") (d (list (d (n "emu-audio") (r "^0.1.0") (d #t) (k 0)))) (h "0nmwmxin6j9yr33d5mqlxnz8ni85lrfvnxqlg3yxgpnpk4z4rgws")))

(define-public crate-emu-0.1.2 (c (n "emu") (v "0.1.2") (d (list (d (n "emu-audio") (r "^0.1.1") (d #t) (k 0)))) (h "0idddxsyp1p39kk4ll5f0ynrr5xw8p3vr8m5ka9lai594aw9w3yl")))

(define-public crate-emu-0.1.3 (c (n "emu") (v "0.1.3") (d (list (d (n "emu-audio") (r "^0.1.2") (d #t) (k 0)))) (h "0824m9qci6fask8n8arn4pjr6ghb7q015mrydk1v4kyiw04a3c41")))

