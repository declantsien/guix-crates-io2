(define-module (crates-io #{3}# e epc) #:use-module (crates-io))

(define-public crate-epc-0.1.1 (c (n "epc") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "epub") (r "^2.0.0") (d #t) (k 0)))) (h "0w2xd70b4cgjbn02yywla86vl51hnarsqk0rhcvg7b66a3md1xk7")))

(define-public crate-epc-0.1.2 (c (n "epc") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "epub") (r "^2.0.0") (d #t) (k 0)))) (h "1zmvawrrd3hhc15f7ss3ynkkr0a4blrq3177sxfjzfj1r3hf5rw5")))

