(define-module (crates-io #{3}# e epd) #:use-module (crates-io))

(define-public crate-epd-0.0.1 (c (n "epd") (v "0.0.1") (d (list (d (n "display-interface") (r "^0.4") (d #t) (k 0)) (d (n "display-interface-spi") (r "^0.4") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "008zd6a6ayyldwq8v7h810yna9vv3lmx2j7s78hf7qkgz8d26fq5")))

(define-public crate-epd-0.0.2 (c (n "epd") (v "0.0.2") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (f (quote ("unproven"))) (d #t) (k 0)))) (h "098bk72liqngy8mmc5dz385d703r2s658n8r9jkgdjgvyj9fzi1y")))

