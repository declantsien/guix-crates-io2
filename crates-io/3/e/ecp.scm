(define-module (crates-io #{3}# e ecp) #:use-module (crates-io))

(define-public crate-ecp-0.1.0 (c (n "ecp") (v "0.1.0") (h "118z23489awkiiv60x0l5rwvkalf5d1sx5yjs5wrps4j296zn0lk")))

(define-public crate-ecp-0.1.1 (c (n "ecp") (v "0.1.1") (h "1wxxqy24q7a49w48iar5qy7yaxasqpm4jbvn73m7wsgnvsh22m8g")))

