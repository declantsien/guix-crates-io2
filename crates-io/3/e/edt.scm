(define-module (crates-io #{3}# e edt) #:use-module (crates-io))

(define-public crate-edt-0.1.0 (c (n "edt") (v "0.1.0") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 2)))) (h "11hypzx3nxwbnbqmdn1x9y51mcm8lb7aq7d9q617jnhs8c87x9dk")))

(define-public crate-edt-0.1.1 (c (n "edt") (v "0.1.1") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 2)))) (h "1af3sz4l9j9n3m9hy7cg9v5xllyn40lwdgd00qb720anqrvf6ylj")))

(define-public crate-edt-0.1.2 (c (n "edt") (v "0.1.2") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 2)))) (h "1hs8wqqz37hh49mp81c5llhh258d9ypgb2lvnpjcjyqjh68f5iwf")))

(define-public crate-edt-0.2.0 (c (n "edt") (v "0.2.0") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 2)))) (h "1njpnmcm0ipm5g7d5vapdp17p9f8qdavg24ljb7calihd4wji168")))

(define-public crate-edt-0.2.1 (c (n "edt") (v "0.2.1") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "image") (r "^0.24.0") (d #t) (k 2)))) (h "0bs97g1199s5x986risrsdjwdbpw2zdi79m96ml97qgida548xwj")))

(define-public crate-edt-0.2.2 (c (n "edt") (v "0.2.2") (d (list (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "image") (r "^0.24.0") (d #t) (k 2)))) (h "02d9wi37n5h6bj8v8b4szpwrl3ml94k9l2qj04173bhw2s9a3w60")))

