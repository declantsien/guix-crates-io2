(define-module (crates-io #{3}# e etc) #:use-module (crates-io))

(define-public crate-etc-0.1.0 (c (n "etc") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 2)))) (h "0bvla2yzqca3yy24s6gdagif76lrzriny86n79ngah7nr2ki5gc9") (f (quote (("derive") ("default"))))))

(define-public crate-etc-0.1.1 (c (n "etc") (v "0.1.1") (d (list (d (n "dirs") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 2)))) (h "1j11cfcqmcilgrb9zjqjkzc4a2j6xkfi6a1q8glqz9lxrjyxvd7i") (f (quote (("derive") ("default") ("bonus"))))))

(define-public crate-etc-0.1.2 (c (n "etc") (v "0.1.2") (d (list (d (n "dirs") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 2)))) (h "1qxwlh5pa9wanfxv7bvs6vi1ip3y2hdjipysnqnqhq8h2i2yw4sm") (f (quote (("derive") ("default") ("bonus"))))))

(define-public crate-etc-0.1.3 (c (n "etc") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0400fhj09g063702qar0p9329zggrfsja23jza50cyjy2pmij3ks") (f (quote (("serde-tree" "serde") ("default"))))))

(define-public crate-etc-0.1.4 (c (n "etc") (v "0.1.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (k 2)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zzfqqlhf9jjn8ir7241r1f2ixj787ax77hkdqhwaysqfqvvx3z4") (f (quote (("serde-tree" "serde") ("default"))))))

(define-public crate-etc-0.1.5 (c (n "etc") (v "0.1.5") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (k 2)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0l8f0rfapn3774qvhk7dvb3c73cz7p0w6ymnz5sxfmfqf8mv0vqx") (f (quote (("serde-tree" "serde") ("default"))))))

(define-public crate-etc-0.1.6 (c (n "etc") (v "0.1.6") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (k 2)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1bxq5d9wzb64fahlwz2qajjsa5hpk0wf9gvi2fiiyhqpyv9w9wfs") (f (quote (("serde-tree" "serde") ("default"))))))

(define-public crate-etc-0.1.7 (c (n "etc") (v "0.1.7") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03by9bpnid3wryghkr6sslcwm5d1z5dqqlxrcffi3kisk0kp3p7k") (f (quote (("serde-tree" "serde") ("default"))))))

(define-public crate-etc-0.1.8 (c (n "etc") (v "0.1.8") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-clippy"))) (d #t) (k 2)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13bnnq8gwpqp6x0p3j635687fq3wccx9i3ymicc8nsd9fsy58imv") (f (quote (("serde-tree" "serde" "serde/derive") ("default"))))))

(define-public crate-etc-0.1.9 (c (n "etc") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15izbc22rkgyvah8rjbq65zkxb1wcfimpq1cficvczc8kyc30wy5") (f (quote (("serde-tree" "serde" "serde/derive") ("default"))))))

(define-public crate-etc-0.1.10 (c (n "etc") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1r03zwaj3wfk1ig5zmajldr7kzcawd2y1x2fix2hf1gwglw0szbs") (f (quote (("serde-tree" "serde" "serde/derive") ("default"))))))

(define-public crate-etc-0.1.11 (c (n "etc") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0m13ylyns01g89lbhriva3jf9lfz7z2v22548684w8f2z4m6ifgp") (f (quote (("serde-tree" "serde" "serde/derive") ("default"))))))

(define-public crate-etc-0.1.12-alpha.0 (c (n "etc") (v "0.1.12-alpha.0") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1rjvxgnk6xqbsz372pi8acjn1m8vjisl8hfmfvddkmkx7z11xfzn") (f (quote (("serde-tree" "serde" "serde/derive") ("default"))))))

(define-public crate-etc-0.1.12 (c (n "etc") (v "0.1.12") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0maf9wqai6svkisj1215nxhqj4p6jiilakh0ncp2p05ymx35zqd0") (f (quote (("serde-tree" "serde" "serde/derive") ("default"))))))

(define-public crate-etc-0.1.13 (c (n "etc") (v "0.1.13") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "04y70hwn7qm4j5v69hs8bvqlq3bf2y130fwvc87l49s050c9qrf1") (f (quote (("serde-tree" "serde" "serde/derive") ("default"))))))

(define-public crate-etc-0.1.14 (c (n "etc") (v "0.1.14") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g61rk4c2gqyh41fv2iazn7mw7ys33gqv3jj2j9cbscsm33x9va2") (f (quote (("serde-tree" "serde" "serde/derive") ("default"))))))

(define-public crate-etc-0.1.15 (c (n "etc") (v "0.1.15") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "07gdgpwx8s7dg4540wmadqmq9dfj96p5hf6999h3wzpcm1dvwmxv") (f (quote (("serde-tree" "serde" "serde/derive") ("default"))))))

(define-public crate-etc-0.1.16 (c (n "etc") (v "0.1.16") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bppv7vf614j8ljfsnp64iij2mp0hlmyhkrw0zl15r8fb763ayag") (f (quote (("serde-tree" "serde" "serde/derive") ("default"))))))

(define-public crate-etc-0.1.17 (c (n "etc") (v "0.1.17") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1izlkqirliq0w2lm2v2ak8f6ac5ivchqsvarsh7kvcqx87a40178") (f (quote (("default"))))))

(define-public crate-etc-0.1.18 (c (n "etc") (v "0.1.18") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1q537yg1smc7y5jblg5d2q61h1jckzm3b342z2m1qsq4p84dq5x1") (f (quote (("default"))))))

(define-public crate-etc-0.1.19 (c (n "etc") (v "0.1.19") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1qz2pkzsq46jk0nbff5hgfdh0phx4376rfgd869whyrwdypd0w7d") (f (quote (("default"))))))

(define-public crate-etc-0.1.20 (c (n "etc") (v "0.1.20") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zbj3a5rhbs1gxg53wsnccnrk3g9xv2zsbvc50lrhz8hqxvi290v") (f (quote (("default"))))))

