(define-module (crates-io #{3}# e evt) #:use-module (crates-io))

(define-public crate-evt-0.0.1 (c (n "evt") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "1k2g0azihy4nif9sz6n7iv8nkixqh6jj1s23i36sidb5n6r5i2r4")))

