(define-module (crates-io #{3}# e esi) #:use-module (crates-io))

(define-public crate-esi-0.1.0 (c (n "esi") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0p43pv99b8zs5p80ybwwhxczzpkcbz0vsnrbpkhhl3af44hig9g6")))

(define-public crate-esi-0.1.1 (c (n "esi") (v "0.1.1") (d (list (d (n "fancy-regex") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1q6dsf9rby1cqz454k9izsz0r7nhhmc80dcwra0pyjs8nfnapsgl")))

(define-public crate-esi-0.1.2 (c (n "esi") (v "0.1.2") (d (list (d (n "fancy-regex") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0p6d0f2jnj6g86rqvi7l29zsvbabp20lg0sphwql3jlawlbdh54m")))

(define-public crate-esi-0.1.3 (c (n "esi") (v "0.1.3") (d (list (d (n "fancy-regex") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1yxndi9rw60nwgl7a8w7hw85rq56yjg5zhg7szrk5im06llf4vp6")))

(define-public crate-esi-0.2.0 (c (n "esi") (v "0.2.0") (d (list (d (n "fastly") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qp6gapk7adz0bm8fkibff1zs4dbhcypsybm3nfgf9297fiayz0c")))

(define-public crate-esi-0.3.0-pre (c (n "esi") (v "0.3.0-pre") (d (list (d (n "fastly") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kh6b7viik8irq9lgacajgnxf5c520n5651idhb71xbr60405n4b")))

(define-public crate-esi-0.3.1-pre (c (n "esi") (v "0.3.1-pre") (d (list (d (n "fastly") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p9dib87qi9hrsan90p2frc8b19rs4dl4j3wmgnaijblvjph2jfg") (y #t)))

(define-public crate-esi-0.3.0-pre3 (c (n "esi") (v "0.3.0-pre3") (d (list (d (n "fastly") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01lkcckd4g8jidx8nwijdya4vm22dd9x54z028989dzi3bcmbxz9")))

(define-public crate-esi-0.3.0-pre4 (c (n "esi") (v "0.3.0-pre4") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "fastly") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07bj0x46i9s6whg7w244wx98x5g69scpgz50izm6db07ichm9xr0")))

(define-public crate-esi-0.3.0-pre5 (c (n "esi") (v "0.3.0-pre5") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "fastly") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "173g03mfgnj0sxq7n9jykbw6i3jp3k6wdy6d8iw4ys8qzwgd9219")))

(define-public crate-esi-0.3.0 (c (n "esi") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "fastly") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04n1x6hgbm468c1wyv40710z1d5vs5q6lhqghq551aikmhxns3rf")))

(define-public crate-esi-0.3.1 (c (n "esi") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "fastly") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rngpnx50a3izg9g2ndh9nwn241vcv4vz4rfl80jm7qnagw9yx02")))

(define-public crate-esi-0.4.0-pre1 (c (n "esi") (v "0.4.0-pre1") (d (list (d (n "fastly") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)))) (h "0z6si04fm9s71xx9g5ziswxzzlyq3l763kvz01j8if79i0maraln")))

(define-public crate-esi-0.4.0-pre2 (c (n "esi") (v "0.4.0-pre2") (d (list (d (n "fastly") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)))) (h "0pf2iz89n7xir5r0rj9whck37dy5wbyqmlkzm2s8zlqwz4rv7v8v")))

(define-public crate-esi-0.4.0-pre3 (c (n "esi") (v "0.4.0-pre3") (d (list (d (n "fastly") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)))) (h "1dhard78ikcy6fbx42m92305dmap4b9cy5hlh4y6jmvaigk5gh8v")))

(define-public crate-esi-0.4.0-pre4 (c (n "esi") (v "0.4.0-pre4") (d (list (d (n "fastly") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "=0.9.3") (d #t) (k 2)))) (h "1dzdhgmxxj9vf59jrv1d0qs8vcs0l6yfpi0psyl35m4wa1pj63hb")))

(define-public crate-esi-0.4.0 (c (n "esi") (v "0.4.0") (d (list (d (n "fastly") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "=0.9.3") (d #t) (k 2)))) (h "0pqrgva453axsi3bg8izg203052kzkibbp41kgw7k2w37im5qb7n")))

