(define-module (crates-io #{3}# e eve) #:use-module (crates-io))

(define-public crate-eve-0.1.0 (c (n "eve") (v "0.1.0") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dir-diff") (r "^0.3") (d #t) (k 2)) (d (n "dotenv") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "06j3c7zi6bvdr5j86vdy64j6cha0cixvjy2gp6izkqpvf888dm0s")))

(define-public crate-eve-0.1.1 (c (n "eve") (v "0.1.1") (d (list (d (n "assert_cli") (r "^0.6") (d #t) (k 2)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "dir-diff") (r "^0.3") (d #t) (k 2)) (d (n "dotenv") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1lwrbwazvkssbm4s3314645ppyd794nd288g9ng9mnpdy5v1mlq7")))

