(define-module (crates-io #{3}# e edl) #:use-module (crates-io))

(define-public crate-edl-1.0.0 (c (n "edl") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0zd9hwci1f7flj4sksm764bjnd9zy045hdc84d3221hqlmfd7l5a")))

(define-public crate-edl-1.1.0 (c (n "edl") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "119vfpgw73c7agd4wgb7fk9zdf86s8r6gnxjf3lrax0fq1bz61di")))

(define-public crate-edl-1.1.1 (c (n "edl") (v "1.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "102njf3wbwjb66b4m608w6r78sgf7xdr0lipa98ap13k3a2ni6r7")))

(define-public crate-edl-1.1.2 (c (n "edl") (v "1.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "03vg58k9z12zwz9p5rqr9i767q7lp2zbps75kv2zind8zxk9jfdh")))

