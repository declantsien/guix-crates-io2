(define-module (crates-io #{3}# e eio) #:use-module (crates-io))

(define-public crate-eio-0.1.0 (c (n "eio") (v "0.1.0") (h "0rvxbxinylmqj9zxkqf43n40ykqqkp9nvnr443fad2g4hq3sb02p") (f (quote (("std") ("default" "std"))))))

(define-public crate-eio-0.1.1 (c (n "eio") (v "0.1.1") (h "145cdkmbw4rlyiwq65wbkwdil9an8sdrpk65q1sqvdbicca6s72v") (f (quote (("std") ("default" "std"))))))

(define-public crate-eio-0.1.2 (c (n "eio") (v "0.1.2") (h "0xqblkbpmzxzl5sp78310cnji0m9ps7qra4ndb0xvyccz8z2b7zl") (f (quote (("std") ("default" "std"))))))

