(define-module (crates-io #{3}# e ezr) #:use-module (crates-io))

(define-public crate-ezr-0.1.0 (c (n "ezr") (v "0.1.0") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0gqvd1pbaa2809gqbf5j56sx87vy6c8rl5mjp19cmnjvrywanwr5")))

(define-public crate-ezr-0.1.1 (c (n "ezr") (v "0.1.1") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0qbql509jpw21948sl696v7wb0c3i30c7lxxmsmf8yy656l5ld2x") (y #t)))

(define-public crate-ezr-0.1.2 (c (n "ezr") (v "0.1.2") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "00y97crg2c6w1wwhxkaxdg3n7n5djk0pg3j37krvsgfvvv031zpw")))

(define-public crate-ezr-0.1.3 (c (n "ezr") (v "0.1.3") (d (list (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "184yx893zjzb7p7hwcdxfgbwbw36lr2sf2a3gh8389kk98rhjxgv")))

(define-public crate-ezr-0.1.4 (c (n "ezr") (v "0.1.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-check" "run-cargo-clippy" "run-cargo-fmt"))) (d #t) (k 2)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0f0z39h10jf5ypdmkw7c97qh6bckm5lss15x8yaxj0pxvxfmc218")))

(define-public crate-ezr-0.1.5 (c (n "ezr") (v "0.1.5") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("precommit-hook" "run-cargo-test" "run-cargo-check" "run-cargo-clippy" "run-cargo-fmt"))) (d #t) (k 2)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "12qdc6gl84s740icynwf4an5mnbk41h76ws8dlmybzwsvsya2ncr")))

