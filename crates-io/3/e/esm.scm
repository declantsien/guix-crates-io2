(define-module (crates-io #{3}# e esm) #:use-module (crates-io))

(define-public crate-esm-0.1.0 (c (n "esm") (v "0.1.0") (d (list (d (n "lettre") (r "^0.10.4") (f (quote ("file-transport" "builder"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xgvi4qfw2kh0m3cazss9rvdd7nv9x2wf0lg9sszaccp4d9sazgg")))

(define-public crate-esm-0.1.1 (c (n "esm") (v "0.1.1") (d (list (d (n "lettre") (r "^0.10.4") (f (quote ("file-transport" "builder"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "018v0b3yzp13fwhqhk2l0mfkb4h5rszhk6h54affsb9n2phmncmm")))

(define-public crate-esm-0.1.2 (c (n "esm") (v "0.1.2") (d (list (d (n "lettre") (r "^0.10.4") (f (quote ("file-transport" "builder"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kxils8xi57gbw11s0d2q0n5gcx1g03dckhnd9mr7ikv08c02cl4")))

(define-public crate-esm-0.1.3 (c (n "esm") (v "0.1.3") (d (list (d (n "lettre") (r "^0.10.4") (f (quote ("file-transport" "builder"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0i3q3vkv8w2zd8p2nlbpr3lavv6xgfc9q9l1dm56jzpl23grl0gl")))

(define-public crate-esm-0.1.4 (c (n "esm") (v "0.1.4") (d (list (d (n "lettre") (r "^0.10.4") (f (quote ("file-transport" "builder"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1da1hzjrc3gnzkhkp0ajnl37wz0rjx1gplnyyvhwxr32xb7rf4b7")))

