(define-module (crates-io #{3}# e edo) #:use-module (crates-io))

(define-public crate-edo-0.1.0 (c (n "edo") (v "0.1.0") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "06wqzj3by45nccnv2alcymf86pmsp7a8gs3ln4cdvw93ch81drx5")))

(define-public crate-edo-0.2.0 (c (n "edo") (v "0.2.0") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "0ysvykj90m2vb4wl5d9c1jzg49qhail13hr3n8vw00bvzsl3dz5f")))

(define-public crate-edo-0.3.0 (c (n "edo") (v "0.3.0") (d (list (d (n "nom") (r "^1.2.4") (d #t) (k 0)))) (h "0wanxfvcpln8k8mz3gs1hrd5m1rnrddksn0li9v111msikph0bs2")))

