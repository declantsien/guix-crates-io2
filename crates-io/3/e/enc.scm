(define-module (crates-io #{3}# e enc) #:use-module (crates-io))

(define-public crate-enc-0.1.0 (c (n "enc") (v "0.1.0") (h "0r1r1i8yd6nwi8b894h84klqfddvxz70m2ziz79xywrjq1w05rhm")))

(define-public crate-enc-0.1.1 (c (n "enc") (v "0.1.1") (h "0nrir3k3iwxiskdnzl5fsf05fibf3gp2icqp8hfkfaxv0322dskd")))

(define-public crate-enc-0.1.2 (c (n "enc") (v "0.1.2") (h "0ns0xlync38yf9g9gwx5lk8alr07qv33ra4r512xczhjjks7mksq")))

(define-public crate-enc-0.1.3 (c (n "enc") (v "0.1.3") (h "034r5blcwv63r69lqxy0avsjs67ggp7b8kagavarkp2fcjnsmkrm")))

(define-public crate-enc-0.2.0 (c (n "enc") (v "0.2.0") (h "0jg4sh4qmpnbn63k30miwv8nlavvz5pkn0dp5gm80gvgiycild7k")))

(define-public crate-enc-0.2.1 (c (n "enc") (v "0.2.1") (h "02jfbaiaxsv2lyb6hm7w08da92yplkg60vmav3g0i20rrabcy8v3")))

(define-public crate-enc-0.2.2 (c (n "enc") (v "0.2.2") (h "13f65fx4kyz934w3iyxa88ff7pw26zssl83mzysbg0lr5pxhkvbf")))

(define-public crate-enc-0.3.0 (c (n "enc") (v "0.3.0") (h "0zkkpyc8glq1pc1q4rcqc3affxh69m0jf524m8ahn0p3w934is3y")))

(define-public crate-enc-0.4.0 (c (n "enc") (v "0.4.0") (h "0i1n0m4p3as7zyykzvd56iqbpzzyggxq0jaxgd4m2zpcpdsf0wf4")))

(define-public crate-enc-0.5.0 (c (n "enc") (v "0.5.0") (h "1c2pkbx75j4w9dwai0gzr8xxldvlgk4snp6q3hac0nznkhh41yj8")))

(define-public crate-enc-0.6.0 (c (n "enc") (v "0.6.0") (h "0bqplmvf2985as6qfd9k7pr3dyb0bc70fsy2ssy2k55rwxh35fsf") (f (quote (("var-int") ("percent" "hex") ("hex") ("full" "hex" "var-int") ("base-64"))))))

