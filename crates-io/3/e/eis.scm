(define-module (crates-io #{3}# e eis) #:use-module (crates-io))

(define-public crate-eis-0.1.0 (c (n "eis") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.29.11") (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g5n4f9bk6nchvgg31vbfa633spjklfbf021mlnvhyvjhq0jnqri")))

