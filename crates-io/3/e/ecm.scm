(define-module (crates-io #{3}# e ecm) #:use-module (crates-io))

(define-public crate-ecm-0.1.0 (c (n "ecm") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "primal") (r "^0.3") (d #t) (k 0)) (d (n "rug") (r "^1") (f (quote ("integer" "rand"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0h2g8kjyrjk754wfwl29gspv3rgsralf2j8d6i9ccpwa674br6jl")))

(define-public crate-ecm-0.1.1 (c (n "ecm") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "primal") (r "^0.3") (d #t) (k 0)) (d (n "rug") (r "^1") (f (quote ("integer" "rand"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1mczkdgjspiki6ncv099arn7zpvcvpmg3mj14cg2qpz8798jz6ni")))

(define-public crate-ecm-0.2.0 (c (n "ecm") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "primal") (r "^0.3") (d #t) (k 0)) (d (n "rug") (r "^1") (f (quote ("integer" "rand"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0vzkfrh94b93m64d0blp3hm81g9b3bxmhc37zjpshjwia9zafs59")))

(define-public crate-ecm-0.3.0 (c (n "ecm") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3") (d #t) (k 0)) (d (n "rug") (r "^1") (f (quote ("integer" "rand"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "132wiv7pai4zwmkdgxi73zw0vbj7697k2myb6adabhb6nx86b2sb") (f (quote (("progress-bar" "indicatif") ("default"))))))

(define-public crate-ecm-0.4.0 (c (n "ecm") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3") (d #t) (k 0)) (d (n "rug") (r "^1") (f (quote ("integer" "rand"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0903bq52ggaalpa0m43w3pxiv447041akws56hbw920knpgkfl3g") (f (quote (("progress-bar" "indicatif") ("default"))))))

(define-public crate-ecm-0.4.1 (c (n "ecm") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3") (d #t) (k 0)) (d (n "rug") (r "^1") (f (quote ("integer" "rand"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1wrjmn9qgsh3flbxy54psz37snc25gwzylb2rb15chy2bfsj0h72") (f (quote (("progress-bar" "indicatif") ("default"))))))

(define-public crate-ecm-0.4.2 (c (n "ecm") (v "0.4.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "indicatif") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "primal") (r "^0.3") (d #t) (k 0)) (d (n "rug") (r "^1") (f (quote ("integer" "rand"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1h205zrzcr7xiiy8c7wl9zgk85f978bjw3nkisywa7k3kcgiiw61") (f (quote (("progress-bar" "indicatif") ("default"))))))

