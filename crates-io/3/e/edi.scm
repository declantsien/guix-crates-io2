(define-module (crates-io #{3}# e edi) #:use-module (crates-io))

(define-public crate-edi-0.0.1 (c (n "edi") (v "0.0.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vkp405kpaw0jg42wkmw5645ws72c6n19xzn3cymxf13lgvzf4ln")))

(define-public crate-edi-0.0.2 (c (n "edi") (v "0.0.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "08zb89spp59gisa9lw7mg68w3r0n14vq6aiyd3pxkfa1csgwzj0d")))

(define-public crate-edi-0.0.3 (c (n "edi") (v "0.0.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mhjmajig1xfr0x7cw163arvc16rrdyijh3s3ja75sria8r8vvy5")))

(define-public crate-edi-0.1.0 (c (n "edi") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jlq025d7yzlb8jmxnv22sfmxs3s4jgdp6k2614ga1b6aiwf28f6")))

(define-public crate-edi-0.1.1 (c (n "edi") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l1safnlywdyy9bgrh4i2pap07nijvfc6m9wawvvvi102sfryqxs")))

(define-public crate-edi-0.1.2 (c (n "edi") (v "0.1.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vi4mkc253f30xd6nm5fl3dcf5r8jp6a7fgjzc600icypqqhb8i6")))

(define-public crate-edi-0.1.3 (c (n "edi") (v "0.1.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "07kf7fk21byimkj015avqkk3mli8pqrf79irvlp6ds8lqhvl49jh")))

(define-public crate-edi-0.2.0 (c (n "edi") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fscjmw6y7rdlf0jvl5yglrbxc9vk1255bja8s0bawz1gqsqd8s9")))

