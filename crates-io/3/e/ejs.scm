(define-module (crates-io #{3}# e ejs) #:use-module (crates-io))

(define-public crate-ejs-0.1.0 (c (n "ejs") (v "0.1.0") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 0)) (d (n "deno_fs") (r "^0.39.0") (d #t) (k 0)) (d (n "deno_io") (r "^0.39.0") (d #t) (k 0)) (d (n "rustyscript") (r "^0.3.0") (d #t) (k 0)))) (h "1yij0z9xch1isy2pg1f7vll2fa7vm2lfn7h4j4h8i7jkifn4snrc")))

(define-public crate-ejs-0.1.2 (c (n "ejs") (v "0.1.2") (d (list (d (n "aok") (r "^0.1.11") (d #t) (k 0)) (d (n "deno_fs") (r "^0.39.0") (d #t) (k 0)) (d (n "deno_io") (r "^0.39.0") (d #t) (k 0)) (d (n "rustyscript") (r "^0.3.0") (d #t) (k 0)))) (h "08b58459r43mh9rvw5c4fk61y5bcdax3jcqkr8m8srglz2ydcs3w")))

