(define-module (crates-io #{3}# e err) #:use-module (crates-io))

(define-public crate-err-0.0.1 (c (n "err") (v "0.0.1") (h "1hifgflzv2vc9n8sjhniwzjjvrj7m7saqjlpg3dw2vimp2vz0rq1")))

(define-public crate-err-0.0.2 (c (n "err") (v "0.0.2") (h "076ppvw10qkgzw49lwjzyzgjlqqnvwybzm324iq81rak9ya0l0hw")))

(define-public crate-err-0.0.3 (c (n "err") (v "0.0.3") (h "1nvihqnbr5lzz3alg6gxd9abwi09r5p8yiyfjdrdbh9gj8a2n6gh")))

(define-public crate-err-0.0.4 (c (n "err") (v "0.0.4") (h "0bdnls06apfrh44hajrfhr3bqswmqnw2642x1z21szkaqi8zgjr4")))

(define-public crate-err-0.0.5 (c (n "err") (v "0.0.5") (h "1hh4nq6vscpzyqijmxgff078l7wx2p9bl482gjkifamrar83r1c7")))

(define-public crate-err-0.0.6 (c (n "err") (v "0.0.6") (h "099jv8xh7qw8k5sqcpknqxyy3m0jrk13y3yy9ycyyk9jwx633hf0")))

(define-public crate-err-0.0.7 (c (n "err") (v "0.0.7") (h "10h7v95v69i2j4868ba6k142n5an2xlgwki2bmzfjzy6fzdgaip4")))

(define-public crate-err-0.0.8 (c (n "err") (v "0.0.8") (h "04xfkas60zwhn167rwglf710j6lbb9iapn3v087hgwp6x5gspymc")))

