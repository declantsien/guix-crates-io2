(define-module (crates-io #{3}# e eol) #:use-module (crates-io))

(define-public crate-eol-0.1.0 (c (n "eol") (v "0.1.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)))) (h "08s5vdqa6jc8qib3sxz7g6w2cnki3aji4h6hzn81ksrhax4yzkqq")))

(define-public crate-eol-0.1.1 (c (n "eol") (v "0.1.1") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)))) (h "0vj6b6js63dd962v8by1idp86fhgs6jvcjvsilnzsn6hr4m1mh3r")))

