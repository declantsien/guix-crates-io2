(define-module (crates-io #{3}# e eva) #:use-module (crates-io))

(define-public crate-eva-0.2.0 (c (n "eva") (v "0.2.0") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 0)))) (h "1l9s8yvd3spwj881y11dgsasafcdsbaj8kifmbkydycdhwcgay1s")))

(define-public crate-eva-0.2.1 (c (n "eva") (v "0.2.1") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 0)))) (h "09026vfwx07yms10wxd0ndzkh187lgwyj45131icflrxq9pqdkqg")))

(define-public crate-eva-0.2.3 (c (n "eva") (v "0.2.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^0.1.1") (d #t) (k 0)) (d (n "rustyline") (r "^3.0.0") (d #t) (k 0)))) (h "1sai2w2p2vn5mby64gnif6jjb8mz9j6vg9gdqx8z5j3p28ldngi4")))

(define-public crate-eva-0.2.5 (c (n "eva") (v "0.2.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^4.1.0") (d #t) (k 0)))) (h "024wk36wyidp8757m7sx3qbxlw74iqr6p9s2l0zs72mfvcbqq1pn")))

(define-public crate-eva-0.2.6 (c (n "eva") (v "0.2.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "directories") (r "^2.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "rustyline") (r "^4.1.0") (d #t) (k 0)))) (h "1pfbkp206gpf5ymwj2ah1mdjd21sg5pcq3mgg8l5lxnc3w3cv88g")))

(define-public crate-eva-0.2.7 (c (n "eva") (v "0.2.7") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "directories") (r "^2.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "rustyline") (r "^4.1.0") (d #t) (k 0)))) (h "0gzcrzbrpcrhkc90qh8wv4mhkfisg8pgfsg3xr6gldfv606bqjk4")))

(define-public crate-eva-0.3.0-alpha.1 (c (n "eva") (v "0.3.0-alpha.1") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "rustyline") (r "^9.1.2") (k 0)) (d (n "term_size") (r "^0.3.2") (d #t) (k 0)))) (h "1xf8fjgzlcxanzwi9rzidg14ja37c9w61zdsrb3hpv266j7n6v69")))

(define-public crate-eva-0.3.0 (c (n "eva") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)))) (h "1cvkrbl6bm3j9vpw0l9c9sjahvr2dpcn35kjqkmblva5xh5xi5cw")))

(define-public crate-eva-0.3.1 (c (n "eva") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "rustyline") (r "^10.0.0") (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)))) (h "0iwcs55h8sy9rrr44yly6388hy8zncmvil5rxdd22426k6s1y4i0")))

