(define-module (crates-io #{3}# e ezy) #:use-module (crates-io))

(define-public crate-ezy-0.1.0 (c (n "ezy") (v "0.1.0") (d (list (d (n "glam") (r ">=0.12") (o #t) (d #t) (k 0)))) (h "16h6k9m1rwqbb4rvj282h3gib6y37naym9afj5n55bw6vhdlw0zi")))

(define-public crate-ezy-0.1.1 (c (n "ezy") (v "0.1.1") (d (list (d (n "glam") (r ">=0.12") (o #t) (d #t) (k 0)))) (h "1an6djz9l3rrsvp5358cxc1pqm9r57azkr7jajcn229vri4qbsj2")))

