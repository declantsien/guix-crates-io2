(define-module (crates-io #{3}# e evc) #:use-module (crates-io))

(define-public crate-evc-0.1.0 (c (n "evc") (v "0.1.0") (h "14g62w43gnglxvv7fad271r3acdhd71zbddc1ji3mz5n5lzyl2ig") (y #t)))

(define-public crate-evc-0.1.1 (c (n "evc") (v "0.1.1") (h "1ccqpwfc97gd35z9n7sxdyym43669dpky1a6jpi4x3m2hpxspyzz") (y #t)))

(define-public crate-evc-0.1.2 (c (n "evc") (v "0.1.2") (h "06yw1g3sr66nqsvri8hw4k4icbk3qqrwxi01ngjf2rss8sa2xpnj") (y #t)))

