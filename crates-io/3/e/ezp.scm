(define-module (crates-io #{3}# e ezp) #:use-module (crates-io))

(define-public crate-ezp-0.1.1 (c (n "ezp") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)))) (h "1060yxffvq7781bmzgik184llmiz4hjiszqdi34l04afxa9c5mg3")))

(define-public crate-ezp-0.1.2 (c (n "ezp") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)))) (h "0gqi6flc8afzhjkl79zxl24c8sf0w6wdkbkssq1101x4387fba49")))

(define-public crate-ezp-0.2.0 (c (n "ezp") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)))) (h "1dmbkprz0r6q8jah5zyd101bbjgl1xsafd8baphlir1x39r6qvij")))

(define-public crate-ezp-0.2.1 (c (n "ezp") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)))) (h "0l1ncg86yc3f8mjx80xi5k343vwrv4k3dnbahk16509p7a1rp1md")))

(define-public crate-ezp-0.2.2 (c (n "ezp") (v "0.2.2") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)))) (h "0fwhyxk12wqn9sbr336xi2h50zlx3i7qalhyl3zykpnh24078r9n")))

(define-public crate-ezp-0.2.3 (c (n "ezp") (v "0.2.3") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "open") (r "^2.0.2") (d #t) (k 0)))) (h "1njplzrmq73h31zwgnniqc3fmxh6f9ziv707i1fw4pjhgxvrcpfx")))

