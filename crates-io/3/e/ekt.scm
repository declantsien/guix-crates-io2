(define-module (crates-io #{3}# e ekt) #:use-module (crates-io))

(define-public crate-ekt-0.1.0 (c (n "ekt") (v "0.1.0") (d (list (d (n "ethers") (r "^2.0.0") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.50") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0dz4w9w2qjh8y4zc9278c090z386sgb69i83m8pshya40hci2ja5")))

(define-public crate-ekt-0.1.1 (c (n "ekt") (v "0.1.1") (d (list (d (n "ethers") (r "^2.0.0") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.50") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0p4ip67328ibk3y20p7h6srjzk10ys6v5q8vp8i8mply8f66lgyz")))

(define-public crate-ekt-0.1.2 (c (n "ekt") (v "0.1.2") (d (list (d (n "ethers") (r "^2.0.0") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.50") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "13yy9i5vl80cjj9gcbwpphkl5q0jn7yg1bg6gqwnr75nslwlfh9m")))

(define-public crate-ekt-0.1.3 (c (n "ekt") (v "0.1.3") (d (list (d (n "ethers") (r "^2.0.0") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.50") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "008b9jq391p693b5k0wilr2w597xn8dds2i58ffzafqabkihgp7h")))

(define-public crate-ekt-0.1.4 (c (n "ekt") (v "0.1.4") (d (list (d (n "ethers") (r "^2.0.0") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.50") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0hvpnsvr85j5lz5pczfn71vlhbzzrsf9pkln2ynzlkknq2fiq3cb")))

