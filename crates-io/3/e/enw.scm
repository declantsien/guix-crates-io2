(define-module (crates-io #{3}# e enw) #:use-module (crates-io))

(define-public crate-enw-0.1.0 (c (n "enw") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "11yzgl27kq20wmhlmn869bvznd85iaxbgjmfvy0xvyaiw2pk1834")))

(define-public crate-enw-0.1.1 (c (n "enw") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1faccbk81n2mdq293znwd1vq7xhsbicnvxm5ndvw4adr63bi94bk")))

(define-public crate-enw-0.2.0 (c (n "enw") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0jk9a4q2s6yf2vx9xhh8fdmd3k12jbypz2zi9ihf8v2ag1n623lr")))

(define-public crate-enw-0.2.1 (c (n "enw") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0g8sf3c9r29prnzb49rj35y234zi0bnpkh1f29n52jb61zmksxaz")))

(define-public crate-enw-0.3.0 (c (n "enw") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1crchp9bc08992jbhm84vp2p2mk4m2cdgs365lv6gyd54yr5p45z")))

(define-public crate-enw-0.3.1 (c (n "enw") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1w0sgrf7wivr4yvnzsd3swmdkcs3vg57bn8x8lyk7dizvy30lfb4")))

(define-public crate-enw-0.3.2 (c (n "enw") (v "0.3.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1c5l37c5dzcjcgx32b6fmnrprycfz2ggx86jfasj2560gp6apysb")))

(define-public crate-enw-0.4.0 (c (n "enw") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0jxb8z6068p7vnc6ra1pbbvidipvi1zr62nh59v267ivyj1si9h4")))

(define-public crate-enw-0.5.0 (c (n "enw") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0fh4bxlvvr0xyd87rkswk7ga80hm4jvfhbz55dxblfwph6n3bg7f")))

(define-public crate-enw-0.5.1 (c (n "enw") (v "0.5.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1k3mwh7llvfvfxnf97bcjl6xwa9z6l0pac3aq7mz7qbif5p4p6ny")))

(define-public crate-enw-0.6.0 (c (n "enw") (v "0.6.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0y6nwill48p6lxi3f8jm8s56jrgiigv330m1mg2qcb1mrwlghxx4")))

