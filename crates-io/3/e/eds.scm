(define-module (crates-io #{3}# e eds) #:use-module (crates-io))

(define-public crate-eds-0.4.0 (c (n "eds") (v "0.4.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "06ckyjh3zdhgqb5x3r7nryc7mpyf9qsllk77x5n4wcz4fckn2aps")))

(define-public crate-eds-0.4.2 (c (n "eds") (v "0.4.2") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "generic-vec") (r "^0.1") (f (quote ("nightly"))) (k 0)))) (h "1zm8mzj00hkg0ccg10v3qfrv7zkpidjj0280a2zjnkga8qm4ra18")))

(define-public crate-eds-0.4.3 (c (n "eds") (v "0.4.3") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.1.2") (d #t) (k 0)))) (h "13mz5f9cn08g3cas9861d52532548ssari4v902nw5jxqva19swf")))

(define-public crate-eds-0.4.4 (c (n "eds") (v "0.4.4") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.3.3") (d #t) (k 0)))) (h "0bkjxgjy3md88d2vfpjyzjkcx1r1g49p8y5rhmdvl92hys7bwqk8")))

(define-public crate-eds-0.5.0 (c (n "eds") (v "0.5.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "0r22f4l8i1kpry1rvi597n0np0a3m05a14d481prpchlvm9128lq")))

(define-public crate-eds-0.5.1 (c (n "eds") (v "0.5.1") (d (list (d (n "eds-core") (r "^0.5") (d #t) (k 0)) (d (n "eds-reader") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "eds-writer") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1wcvayci2qyllman2f5khhrscsn2q78zxmy27ywb66lk12grxbhs") (f (quote (("alloc" "eds-reader" "eds-writer"))))))

