(define-module (crates-io #{3}# e eep) #:use-module (crates-io))

(define-public crate-eep-0.1.0 (c (n "eep") (v "0.1.0") (d (list (d (n "leb128") (r "^0.2.1") (d #t) (k 0)) (d (n "signpost") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "thread-id") (r "^2.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.0") (d #t) (k 0)))) (h "1ajk59g9dabyjsqwd1zph2kiwsjn68f218li66gxbc7kcmn213mq") (f (quote (("nightly"))))))

