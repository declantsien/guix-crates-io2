(define-module (crates-io #{3}# e edb) #:use-module (crates-io))

(define-public crate-edb-0.1.0 (c (n "edb") (v "0.1.0") (d (list (d (n "edb-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)))) (h "0jnx2jl1r2g6anb0ljraf566mhgavxx70jgzh0bpx8xyhww2xwak")))

