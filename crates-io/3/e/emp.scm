(define-module (crates-io #{3}# e emp) #:use-module (crates-io))

(define-public crate-emp-1.0.0 (c (n "emp") (v "1.0.0") (d (list (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "13c9mam3vq2bad7wr7sg96i4fiwbxnz8fk81yn9qiwka5p5cfarh")))

(define-public crate-emp-1.0.1 (c (n "emp") (v "1.0.1") (d (list (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "08wj19fr8l0y86vmgn48ds43nxjadg0gkiy18dhz4ybja49wi9bg")))

(define-public crate-emp-1.0.2 (c (n "emp") (v "1.0.2") (d (list (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1p6ca4lrri7ks0skggsyrcrzag07xwy4czqkm9kknbyhg599yp78")))

(define-public crate-emp-1.0.3 (c (n "emp") (v "1.0.3") (d (list (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0y68d4cqg0jmrkb4545f18fshk54dayglk80f6ss7wrlr9g1bn04")))

(define-public crate-emp-1.1.0 (c (n "emp") (v "1.1.0") (d (list (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0kq27g1v9imkfp4cmlfhyycijcidj3bs3bg6slngwj5qxjih8840")))

