(define-module (crates-io #{3}# e ein) #:use-module (crates-io))

(define-public crate-ein-0.0.1 (c (n "ein") (v "0.0.1") (h "0h15acn06wwgkgsv5ilvhb9f1sf06916amkwzzwx57lxs5i9v3bf") (y #t)))

(define-public crate-ein-0.0.5 (c (n "ein") (v "0.0.5") (h "0w1zl4aj55y6nhzf4s66nyvwbczvwxw0jf6ybpfnxjldimjzsvm9") (y #t)))

(define-public crate-ein-0.0.7 (c (n "ein") (v "0.0.7") (h "1xhxnb7lmrzmpkxlkk2rpz6i4zin9dcabbbxrqw64skrkawr8ajh") (y #t)))

(define-public crate-ein-0.0.9 (c (n "ein") (v "0.0.9") (h "1j8b73bmw3xqh3f9563wnivhbf9dy3g7wiq5x30r6dmv5p2r8sp5") (y #t)))

(define-public crate-ein-0.1.1 (c (n "ein") (v "0.1.1") (h "18n024xvag3y68y7np7s7ss1b1gk0x9p2bdalx56hdspmm3m11qz") (y #t)))

(define-public crate-ein-0.1.3 (c (n "ein") (v "0.1.3") (h "1iw16vn441n90a04vndn3gmj80mvf4lb5agb49g4vc45yqr8rij6") (y #t)))

(define-public crate-ein-0.1.4 (c (n "ein") (v "0.1.4") (h "0f4c5nqkwcrdnpfc0qr6dzqp51ch44b7g03rjpwa0d6yam604skf") (y #t)))

