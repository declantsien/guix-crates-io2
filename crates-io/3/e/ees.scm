(define-module (crates-io #{3}# e ees) #:use-module (crates-io))

(define-public crate-ees-0.1.0 (c (n "ees") (v "0.1.0") (h "1d0p7slqyzpnvicq847ddllq8gzgpa1hykhnlrqnw004ygkh7gsd")))

(define-public crate-ees-0.1.1 (c (n "ees") (v "0.1.1") (h "16s6fs79br2anz1kqv7fwp0wrsci4li6ifr8a0wf0jql69r2f4mn")))

(define-public crate-ees-0.1.2 (c (n "ees") (v "0.1.2") (h "16d6xlql4sp79341w13ia3dhd6cbqrl7wgmndxz4lg1rc96i4qkw")))

(define-public crate-ees-0.1.3 (c (n "ees") (v "0.1.3") (h "1y6lh4l6506w3nwbc4gslfy8ldq1qnizb4mj7113sd3j7vkzhbs7")))

(define-public crate-ees-0.1.4 (c (n "ees") (v "0.1.4") (h "0xi7570j4k4836ri5myr4l3zdglf7xks95qj8c7k8bkp5j5alikr")))

(define-public crate-ees-0.1.5 (c (n "ees") (v "0.1.5") (h "1q4fx56kx2i970hs5any7x21vmgdimjazikwhv9b5md72spc3c4n")))

(define-public crate-ees-0.1.6 (c (n "ees") (v "0.1.6") (h "0653pva1h20r2sk26w58swjc81sr8b81kpkkfzlk34jld1v9lkzb")))

(define-public crate-ees-0.2.0 (c (n "ees") (v "0.2.0") (h "0vz9nh32b92mhn68nklq3l4i9k0nv1fpf84b1gmfajfccf7aypsz")))

(define-public crate-ees-0.2.1 (c (n "ees") (v "0.2.1") (h "1jl456sbvc388l3w2alzjlvd54qirjpngwd4bph4b0h1c0a3yvmi")))

(define-public crate-ees-0.2.2 (c (n "ees") (v "0.2.2") (h "146iir1y7g4qk3w68mvdkh6il86plcrmjpp1zv9cvqrlw07rjzqi")))

(define-public crate-ees-0.2.3 (c (n "ees") (v "0.2.3") (h "0cwp87ybyws8s2bp9aymnli5ap9gxd5qznmzl4fkmswff8zbgcnh")))

(define-public crate-ees-0.2.4 (c (n "ees") (v "0.2.4") (h "158nlm7qjsmp2j8r2ipjrg98n38nylcsprh0a458plgw62gbsnhq")))

(define-public crate-ees-0.2.5 (c (n "ees") (v "0.2.5") (h "0znskq92cjz0kbrf4w6qachmby3ysmp3y3js11q98pi13rlj161r")))

(define-public crate-ees-1.0.0 (c (n "ees") (v "1.0.0") (h "07bcd4p60infma6326d08i4qd8ds9gvdnbk2cay20xqlpvazbp6h")))

