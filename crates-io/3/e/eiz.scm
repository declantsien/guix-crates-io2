(define-module (crates-io #{3}# e eiz) #:use-module (crates-io))

(define-public crate-eiz-0.1.0 (c (n "eiz") (v "0.1.0") (h "08j0nxzrf2ibn9gdqkic95h6sqqlvvxnn059d7pg3yq60xm32qng")))

(define-public crate-eiz-0.2.0 (c (n "eiz") (v "0.2.0") (h "0xcnzwvz5vvmvrbwckylx27ck2alg4f1wm9v0vccx5cn46x6ajh0")))

(define-public crate-eiz-0.2.1 (c (n "eiz") (v "0.2.1") (h "1blrw4ybyczjyzn2xc7y1vzpiwqn2lm39rk9xczhxlzi1021qyiz")))

(define-public crate-eiz-0.3.0 (c (n "eiz") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.10.0") (o #t) (d #t) (k 0)))) (h "1c2adbn94nwyax8hvjjpk9wnvfh6z2jjv9qp6hbirjcvh26ybap3") (f (quote (("use_std") ("rt_queue_std" "parking_lot" "rt_queue" "use_std") ("rt_queue") ("default" "rt_queue" "rt_queue_std"))))))

