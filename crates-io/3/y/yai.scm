(define-module (crates-io #{3}# y yai) #:use-module (crates-io))

(define-public crate-yai-0.1.0 (c (n "yai") (v "0.1.0") (h "1rbzb2hnrvgl4062dz4w48wy0dn2ry72gnmfz0dzzxs1gs05b95r")))

(define-public crate-yai-0.1.1 (c (n "yai") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "handleapi" "memoryapi"))) (d #t) (k 0)))) (h "02c1jfjbshhsjslqiyk9p04ivcw5w0vwf21wssfv9xjsslqgxpa9")))

(define-public crate-yai-0.1.2 (c (n "yai") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "handleapi" "memoryapi"))) (d #t) (k 0)))) (h "0sdnfwjsbxvwqsb2zbayz6iir25gl0929z4np63gnywi8gxk9xjh")))

(define-public crate-yai-0.1.3 (c (n "yai") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "handleapi" "memoryapi"))) (d #t) (k 0)))) (h "1lsjmgrlhphm61jrfr7cxhyx9vn6c9kn8g2818g91ks0g769sivz")))

(define-public crate-yai-0.2.0 (c (n "yai") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.15") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.26.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_Threading" "Win32_Security" "Win32_System_LibraryLoader" "Win32_System_Diagnostics_Debug"))) (d #t) (k 0)))) (h "1xj63qccg4300zdap41is32xmszmyiwnnpf08sm5v2w8mqf4siym") (f (quote (("default" "cli") ("cli" "clap"))))))

