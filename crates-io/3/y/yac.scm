(define-module (crates-io #{3}# y yac) #:use-module (crates-io))

(define-public crate-yac-0.1.0 (c (n "yac") (v "0.1.0") (h "0cd674rppw4xn87lcvjkccp87a1766lb72yxidd40qr2gbx5q7mr")))

(define-public crate-yac-0.1.1 (c (n "yac") (v "0.1.1") (h "0hpcxnhgyafvnl5iafz5b9y0cj7apadzgbg3bgyzfjwncwbjvxy7")))

