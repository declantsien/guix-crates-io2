(define-module (crates-io #{3}# y yee) #:use-module (crates-io))

(define-public crate-yee-0.1.0 (c (n "yee") (v "0.1.0") (d (list (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)))) (h "0m63d5cnqkvmmamlir4fks5hffgr5a9in07hqkscv7nb9kbbgnq6")))

(define-public crate-yee-0.1.1 (c (n "yee") (v "0.1.1") (d (list (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)))) (h "13mabchdjh1ddis0xyz674i9r5119l97ib4v73ga8pk3akl4zsvm")))

(define-public crate-yee-0.1.2 (c (n "yee") (v "0.1.2") (d (list (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)))) (h "1lfranm5llny45pzkjn6q73a0k20szhxm4mlcvl6y6fvmc05kxfy")))

(define-public crate-yee-0.2.0 (c (n "yee") (v "0.2.0") (d (list (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)))) (h "1c68jivr5d14z29fgq8rav6ff4lj92vbd5rv9902vd583azavp6s")))

(define-public crate-yee-0.2.1 (c (n "yee") (v "0.2.1") (d (list (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)))) (h "1bm3h2148vr8xjkgwkywhwmyp97a6pgk6pql4fgw3qmvqn17asxj")))

(define-public crate-yee-0.2.2 (c (n "yee") (v "0.2.2") (d (list (d (n "prettytable-rs") (r "^0.9") (d #t) (k 0)))) (h "0mis8wh15inwfba2y4qmhfx2zam27529b06gsb3diafvp607i38q")))

