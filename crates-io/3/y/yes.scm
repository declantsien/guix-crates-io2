(define-module (crates-io #{3}# y yes) #:use-module (crates-io))

(define-public crate-yes-0.1.0 (c (n "yes") (v "0.1.0") (h "0asb2x7fzij6dw0k7rm4pl60liv2kgvbachhxp0kv7pp6cvhl87a")))

(define-public crate-yes-0.2.0 (c (n "yes") (v "0.2.0") (h "1gzcr8dbxxrgmdaw1h0brgr9b47nkb569ck651cwn3mivny53r1n")))

