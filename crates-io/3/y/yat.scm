(define-module (crates-io #{3}# y yat) #:use-module (crates-io))

(define-public crate-yat-0.1.0 (c (n "yat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "fern") (r "^0.5.8") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "toml") (r "^0.5.1") (d #t) (k 0)))) (h "0s0v8ab3yk9xvhzxzwk80lrkvckn05kqig6pzmhjpjsjvn030l0k")))

