(define-module (crates-io #{3}# y yok) #:use-module (crates-io))

(define-public crate-yok-0.1.1 (c (n "yok") (v "0.1.1") (h "1wc05rvpqz5ffdyczq8657wa8b0nk5z2j9p3d01ijbsdwnqcbyv1")))

(define-public crate-yok-0.1.2 (c (n "yok") (v "0.1.2") (h "060mpkw3xjp1h0aikkkmd410zg8g687583z57vbz47nbvmx546rv")))

(define-public crate-yok-0.1.4 (c (n "yok") (v "0.1.4") (h "19nndxb9lpcp7xl32wp1ghskrjw8h5ligvkv0wp5l1hvzfpl4zf4")))

(define-public crate-yok-0.1.5 (c (n "yok") (v "0.1.5") (h "0wabvkpp2fl2vp1x3d01nhp23bda12y2sxnq4bdlsaq9gclg8jw9")))

(define-public crate-yok-0.1.6 (c (n "yok") (v "0.1.6") (h "0xhq8pfdvx8v2lbz87sd5jnqxx67m6yhksbnizyq7v331mvps5cg")))

(define-public crate-yok-0.1.7 (c (n "yok") (v "0.1.7") (h "0rz8syl398lp2ji6fnx9ib9pancbyhwnwi4yhk4jhsjdkfyk2x1g")))

(define-public crate-yok-0.1.8 (c (n "yok") (v "0.1.8") (h "0kfg0jyjw2sxn9dmpjl8gmy3lz2clbb4dqcm7wn47jsiy52wjaac")))

(define-public crate-yok-0.1.9 (c (n "yok") (v "0.1.9") (h "0aj0z4fqqj2vsx9xamg4czn3vmiw53ivrzcvazc3vsbjg7h6zy87")))

