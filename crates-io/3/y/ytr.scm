(define-module (crates-io #{3}# y ytr) #:use-module (crates-io))

(define-public crate-ytr-0.1.0 (c (n "ytr") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.5.3") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "19rs5v5qd9zq63kz3vrp2r1x34pkrv65pikphb51sixl8y3j7q5z")))

(define-public crate-ytr-0.1.1 (c (n "ytr") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.5.3") (d #t) (k 0)) (d (n "url") (r "^1.7.1") (d #t) (k 0)))) (h "1gj17c3vy3j3izwylxnkswcf61y0jlgfd9h5jkkvkqk0ilh6mwwb")))

(define-public crate-ytr-0.1.2 (c (n "ytr") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "serde") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 2)) (d (n "serde_urlencoded") (r "^0.5.5") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0skqir22j2jav8rnv4vzb1jiwws6fcnrx03ybcg5winbzswp0qvm")))

