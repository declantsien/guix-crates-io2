(define-module (crates-io #{3}# y yip) #:use-module (crates-io))

(define-public crate-yip-0.1.0 (c (n "yip") (v "0.1.0") (h "0g8gqnwcgscm64iygqfn13jdj8c1z9cw1wipy1a96x7rkgzsw58d")))

(define-public crate-yip-0.2.0 (c (n "yip") (v "0.2.0") (h "0m8m2j9wpqpnzifasiri9simn67hrg39xvpylkzd1hxsxm7i3k61")))

