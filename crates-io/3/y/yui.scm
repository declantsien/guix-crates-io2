(define-module (crates-io #{3}# y yui) #:use-module (crates-io))

(define-public crate-yui-0.1.0 (c (n "yui") (v "0.1.0") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yui_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "yui_derive") (r "^0.1") (d #t) (k 2)) (d (n "yui_internal") (r "^0.1") (d #t) (k 0)))) (h "0w5nkh91adkynz4w23451x4inh5ci9lxyk0fsm7zilyzrjkvvai4") (f (quote (("generate-reader")))) (y #t)))

(define-public crate-yui-0.1.1 (c (n "yui") (v "0.1.1") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yui_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "yui_internal") (r "^0.1") (d #t) (k 0)))) (h "044lqyqzdjl403cpa4hnhf5qcpkkaksf7nwqyv6mamm8h7baxzp7") (f (quote (("generate-reader")))) (y #t)))

(define-public crate-yui-0.1.2 (c (n "yui") (v "0.1.2") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yui_derive") (r "^0.1") (d #t) (k 0)) (d (n "yui_internal") (r "^0.1") (d #t) (k 0)))) (h "04sxwvdd2ixjbafsrpkmvip9farkwgnwazv4vciw88xbyscj101h") (f (quote (("generate-reader")))) (y #t)))

(define-public crate-yui-0.1.3 (c (n "yui") (v "0.1.3") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yui_derive") (r "^0.1") (d #t) (k 0)) (d (n "yui_internal") (r "^0.1") (d #t) (k 0)))) (h "0b8l3adj9myg8a1xrwmivrjqdxk1h1lvw9akh6d21v6pyvgyp4yg") (f (quote (("generate-reader")))) (y #t)))

(define-public crate-yui-0.1.4 (c (n "yui") (v "0.1.4") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yui_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "yui_internal") (r "^0.1.3") (d #t) (k 0)))) (h "127ffb78xkk7w21d1gynlfnnh93idzyr5cjf8g0xd35jikbcg1f3") (f (quote (("generate-reader"))))))

(define-public crate-yui-0.1.5 (c (n "yui") (v "0.1.5") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yui_derive") (r "=0.1.3") (d #t) (k 0)) (d (n "yui_internal") (r "=0.1.3") (d #t) (k 0)))) (h "1di5ns9iivnixcjwmcxak9m178di2rn3dchx7pmkb44shxww0vqs") (f (quote (("generate-reader"))))))

(define-public crate-yui-0.1.6 (c (n "yui") (v "0.1.6") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yui_derive") (r "=0.1.4") (d #t) (k 0)) (d (n "yui_internal") (r "=0.1.4") (d #t) (k 0)))) (h "0669qx3vc5z3nv9dgvhdvy4ni4f8k6m62psxsxr8p806361j5fc5") (f (quote (("generate-reader"))))))

(define-public crate-yui-0.1.7 (c (n "yui") (v "0.1.7") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yui_derive") (r "=0.1.5") (d #t) (k 0)) (d (n "yui_internal") (r "=0.1.5") (d #t) (k 0)))) (h "0cx7syy65sbi9rj7pprj9ns23gyx3vai5k0vbx6y1pigwwalmpsj") (f (quote (("generate-reader"))))))

(define-public crate-yui-0.1.8 (c (n "yui") (v "0.1.8") (d (list (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "yui_derive") (r "=0.1.6") (d #t) (k 0)) (d (n "yui_internal") (r "=0.1.5") (d #t) (k 0)))) (h "0mwif0skgmbhvr0xq1d345i64rdddzgnk469qcfidvhrh2rwainj") (f (quote (("generate-reader"))))))

