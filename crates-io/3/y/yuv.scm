(define-module (crates-io #{3}# y yuv) #:use-module (crates-io))

(define-public crate-yuv-0.1.0 (c (n "yuv") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)) (d (n "rgb") (r "^0.8.25") (d #t) (k 0)))) (h "16mjq908mfy2838gxn8nd4wsiag5zlfykamzyml78c8njzbnhnn4") (y #t)))

(define-public crate-yuv-0.1.1 (c (n "yuv") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)))) (h "16wff13m4c2hkrjpflac7p4pprch6r57x0ybjvbjjfdhn53jxip2") (y #t)))

(define-public crate-yuv-0.1.2 (c (n "yuv") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)))) (h "0bhk43ka4gmx0ihlbbn0xwwkpcb7s47xq50dawrwvqwyzflig6qr") (y #t)))

(define-public crate-yuv-0.1.4 (c (n "yuv") (v "0.1.4") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)))) (h "1zzbpdd140afffp0bgwxx2z2ml3qfl8gv0671f8nkdmigxpz6429")))

(define-public crate-yuv-0.1.5 (c (n "yuv") (v "0.1.5") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)))) (h "1r1bawv5daf6n0b4sdri3573sg4mry77xkhpaj576iv294rr4z0m")))

(define-public crate-yuv-0.1.6 (c (n "yuv") (v "0.1.6") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rgb") (r "^0.8.27") (d #t) (k 0)))) (h "10mig22p7zyr5k4z39y50jzjzkkip5a6d602ghag08chypfk6y8x") (f (quote (("no_std"))))))

