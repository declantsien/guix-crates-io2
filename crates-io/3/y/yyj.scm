(define-module (crates-io #{3}# y yyj) #:use-module (crates-io))

(define-public crate-yyj-0.0.1 (c (n "yyj") (v "0.0.1") (h "133z4i124d0xp63f2wsgnq2l8hzlcd91wf3mb30iszg039nd2831")))

(define-public crate-yyj-0.0.2 (c (n "yyj") (v "0.0.2") (h "1yiirf6sldmz32xr22nxh4wz5mh5h2c3y8lmgx6shjgbdibqfvmx")))

