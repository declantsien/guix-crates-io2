(define-module (crates-io #{3}# y yap) #:use-module (crates-io))

(define-public crate-yap-0.1.0 (c (n "yap") (v "0.1.0") (h "1xp9ihbg3mypa83kank0nhi2hrnzm73mw2idkn004a4d5r30z6g8")))

(define-public crate-yap-0.2.0 (c (n "yap") (v "0.2.0") (h "00d8z0v5j3701v34h25kdhlgfnm40az6m3s1zdnfb27f3p6sncd4")))

(define-public crate-yap-0.3.0 (c (n "yap") (v "0.3.0") (h "0gik2krapgsi610adij4mvwhmw4has9g9w9ra0l7riabrnlp7hwk")))

(define-public crate-yap-0.3.1 (c (n "yap") (v "0.3.1") (h "1lwj0yyhhvcwcyrignqzixknyh6cx995amb8x5g8qivjqh8vx4dp")))

(define-public crate-yap-0.4.0 (c (n "yap") (v "0.4.0") (h "1wxdcdg429c3bngbg9a8vrvyik03bdxv1a3hfq1i14xam4wx8fx2")))

(define-public crate-yap-0.5.0 (c (n "yap") (v "0.5.0") (h "1falmbqib9k5jm0d9lrg09fr618mc2sf85qpk65bs238hrcdca4q")))

(define-public crate-yap-0.6.0 (c (n "yap") (v "0.6.0") (h "1lw5a6fww9c58c1hh3910mm87sg23m6xfn1yf1k1lwl5hd78p0n2")))

(define-public crate-yap-0.6.1 (c (n "yap") (v "0.6.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "0pbsh0c76x2y3h52qkk1p42rnvhai7l9v1lhalsp0iqxjbgihhfa")))

(define-public crate-yap-0.6.2 (c (n "yap") (v "0.6.2") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "01i6jdzsa81awn5mbggzjbxnwpvzvpw1qqdkqvk61jh312kk0xsq")))

(define-public crate-yap-0.7.0 (c (n "yap") (v "0.7.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "0a09bfa68d3rv6avw75k67xw4xdva59s3canmg1xfnhcyd68pakc")))

(define-public crate-yap-0.7.1 (c (n "yap") (v "0.7.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "15g2vnwy67wgjvid9i5p6wz1izaxz8lqp0ns24w901hlsfsxgfb0")))

(define-public crate-yap-0.7.2 (c (n "yap") (v "0.7.2") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "1vr6pk9v4fl388an8pcp2x7kqfgwnz1n4i1zbpai16wyvi97zisz")))

(define-public crate-yap-0.8.0 (c (n "yap") (v "0.8.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "14qbr903v3j0jwxrppwz74iljm9vjic75b1imsn4kcs753vjybjw")))

(define-public crate-yap-0.8.1 (c (n "yap") (v "0.8.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "1vqr004c99wy19m01s28kk28xb1vigfs9f1wwkcwdy009rvwi7rq")))

(define-public crate-yap-0.9.0 (c (n "yap") (v "0.9.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "1vrrfx9flnz7kwfc1cdxnsq3ml2anf60cf6l0ywa35c4glprkqf1")))

(define-public crate-yap-0.9.1 (c (n "yap") (v "0.9.1") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "07hxka062qpzcwh1jz295q30zcikfpywqxkrs4gzhlhv2p88wc8i")))

(define-public crate-yap-0.10.0 (c (n "yap") (v "0.10.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "1xywfzcq4x64lyxkkl1mhb6lrzws2r3q7nkbiq5ls7m1h9nyp9z2")))

(define-public crate-yap-0.11.0 (c (n "yap") (v "0.11.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "1kr49a96x7p1fjpj7vdvpk00q1q7clfyp36p12x9wqn49chj8igz") (f (quote (("std") ("default"))))))

(define-public crate-yap-0.12.0 (c (n "yap") (v "0.12.0") (d (list (d (n "logos") (r "^0.12.0") (d #t) (k 2)))) (h "07q53xz7wij2n4vnv71cpy1wsab52c78d5xx1kifi983p3knkqmz") (f (quote (("std") ("default"))))))

