(define-module (crates-io #{3}# y yas) #:use-module (crates-io))

(define-public crate-yas-0.1.0 (c (n "yas") (v "0.1.0") (d (list (d (n "cursive") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "pwhash") (r "^0.3.1") (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "0cg037r37yhcnjrsk7sj8n4157wm9l0lp1np5w3fxq5syl9d08f0") (f (quote (("tui" "cursive"))))))

(define-public crate-yas-0.1.1 (c (n "yas") (v "0.1.1") (d (list (d (n "cursive") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "pwhash") (r "^0.3.1") (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "0611xi9n7c5wyqf2yl989bmbawiszl7l848rn3n17y4xncwabr1x") (f (quote (("tui" "cursive"))))))

(define-public crate-yas-1.0.0 (c (n "yas") (v "1.0.0") (d (list (d (n "cursive") (r "^0.15.0") (o #t) (d #t) (k 0)) (d (n "pwhash") (r "^0.3.1") (d #t) (k 0)) (d (n "rpassword") (r "^5.0") (d #t) (k 0)) (d (n "users") (r "^0.10") (d #t) (k 0)))) (h "0rm0nzb6fw49b54wfw50l8pb90kvaan717iwld2sl365r860wdwp") (f (quote (("tui" "cursive"))))))

