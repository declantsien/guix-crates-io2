(define-module (crates-io #{3}# y ydl) #:use-module (crates-io))

(define-public crate-ydl-0.1.0 (c (n "ydl") (v "0.1.0") (d (list (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mj6qslr53b775ni8vyvxaf2n88jq8d2iwfpgjpdp95ila1g7lgz") (y #t)))

(define-public crate-ydl-0.1.1 (c (n "ydl") (v "0.1.1") (d (list (d (n "tokio") (r "^1.13.0") (f (quote ("full"))) (d #t) (k 0)))) (h "13gjbd9imqs5f5milgcs2hnvc87hv8f89gwdf8nf3syzpvmim7wx") (y #t)))

