(define-module (crates-io #{3}# y yul) #:use-module (crates-io))

(define-public crate-yul-0.1.0 (c (n "yul") (v "0.1.0") (h "0r39lm2byv3vyrkh3ixdnw0nafmsscscw1ydps45b13cx3iychy4")))

(define-public crate-yul-0.1.1 (c (n "yul") (v "0.1.1") (d (list (d (n "yul-parser") (r "^0.1.1") (d #t) (k 0)))) (h "1x6b4bjyxvrabpk6j7vx59l8qgmr1g92i93k73r227h28cq3gzx2")))

