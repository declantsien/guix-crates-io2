(define-module (crates-io #{3}# y yin) #:use-module (crates-io))

(define-public crate-yin-0.1.0 (c (n "yin") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "dasp") (r "^0.11.0") (f (quote ("signal"))) (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1bzc5b3xd8sg6kz941jd4h8n685spsmnz4hc2lalccl0rlzpzh25")))

(define-public crate-yin-0.2.0 (c (n "yin") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "dasp") (r "^0.11.0") (f (quote ("signal"))) (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0s5yw3w2yrj25wr026da8sx67xd4j9i67xqj1zk201ibdh7skrsc")))

