(define-module (crates-io #{3}# y y4m) #:use-module (crates-io))

(define-public crate-y4m-0.1.1 (c (n "y4m") (v "0.1.1") (d (list (d (n "resize") (r "^0.0.1") (d #t) (k 2)))) (h "0jblkyjsyz0vi3gh8jhraypvqps99z1ijnjy9wl2i0m050d32za1")))

(define-public crate-y4m-0.2.0 (c (n "y4m") (v "0.2.0") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "17ahsa9f1c8pv4rbxjhd9r40cxzlrvpaqpl2kw1hxd2yzjjm0f4r")))

(define-public crate-y4m-0.3.0 (c (n "y4m") (v "0.3.0") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "0b46dnjcl1i1giqwzzvq6yq175adkabpkccfd1mb5y36aq9m2s9m")))

(define-public crate-y4m-0.3.1 (c (n "y4m") (v "0.3.1") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "10n3y0x45xzfmkx7m6rb0g4rh9vqqa5j64il63sswmr483xhp2jh")))

(define-public crate-y4m-0.3.2 (c (n "y4m") (v "0.3.2") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "1q1lnzwxqqcdrn7macl2w81z9kskp3094gp4g1a6v34k9agcvn31")))

(define-public crate-y4m-0.3.3 (c (n "y4m") (v "0.3.3") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "0kxrpgvzwd89b6ch9v9khzmn6bcw4slllacjcy43f22l3zj92hdi")))

(define-public crate-y4m-0.3.4 (c (n "y4m") (v "0.3.4") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "1mk2r3dln8rq6ba7wdr77pyg95vnsmw208pszmraazb09qjn1g23") (y #t)))

(define-public crate-y4m-0.3.5 (c (n "y4m") (v "0.3.5") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "01gaznsgwmwjnj4ks8llp4i65l1cjgq8qm0bhxshdnb6pn5ydpd2")))

(define-public crate-y4m-0.4.0 (c (n "y4m") (v "0.4.0") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "18rmijxsrifcmrpn0pp4k21w5y2apvcl733qv9q1im1qp8w5qhic")))

(define-public crate-y4m-0.5.0 (c (n "y4m") (v "0.5.0") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "06g8c53qk4cla3xczywx5qlklvzsw54x77vm727mhizlsp5n93ar")))

(define-public crate-y4m-0.5.1 (c (n "y4m") (v "0.5.1") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "167nbgz52i4insrzi40imq14xx0nixm1wzsicbq2ansbk12n1aqx") (y #t)))

(define-public crate-y4m-0.5.2 (c (n "y4m") (v "0.5.2") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "127ibv6j7bx33hdc0ysk1b466b9fn7p3mllb0d2244i0xayymd2a") (y #t)))

(define-public crate-y4m-0.6.0 (c (n "y4m") (v "0.6.0") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "1icpcqqq4clwlsqbn19p9zf8paynadbfbh40bw2fzlz2kj2svnz5")))

(define-public crate-y4m-0.5.3 (c (n "y4m") (v "0.5.3") (d (list (d (n "resize") (r "^0.3.0") (d #t) (k 2)))) (h "1933677by64y06zfgip2yq8b2dza8xnljhaksx93czq90b54kscz")))

(define-public crate-y4m-0.6.1 (c (n "y4m") (v "0.6.1") (d (list (d (n "resize") (r "^0.4") (d #t) (k 2)))) (h "0zx1x9ailfrqmr6lavnhygrilcxjcrrfw68n0rq6jxxr4iw370db")))

(define-public crate-y4m-0.7.0 (c (n "y4m") (v "0.7.0") (d (list (d (n "resize") (r "^0.4") (d #t) (k 2)))) (h "1bhdgb7hgx7j92nm6ij5n8wisp50j8ff66ks14jzwdw2mwhrjam7")))

(define-public crate-y4m-0.8.0 (c (n "y4m") (v "0.8.0") (d (list (d (n "resize") (r "^0.4") (d #t) (k 2)))) (h "0j24y2zf60lpxwd7kyg737hqfyqx16y32s0fjyi6fax6w4hlnnks")))

