(define-module (crates-io #{3}# y yan) #:use-module (crates-io))

(define-public crate-yan-0.1.0 (c (n "yan") (v "0.1.0") (h "0avm0mca666zh16gyl8hncwlk8c0rzmz8q8py77iiyk3mpc4fqp3")))

(define-public crate-yan-0.1.1 (c (n "yan") (v "0.1.1") (h "1lxcwsryn5m4nyg8j4adkbafb8kn1b3d190bk7hjdd0az89llqdg")))

