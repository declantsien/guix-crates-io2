(define-module (crates-io #{3}# y ypm) #:use-module (crates-io))

(define-public crate-ypm-0.0.0 (c (n "ypm") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yggdrasil-bootstrap") (r "^0.1") (d #t) (k 0)))) (h "0jqzglkm5hqsff0f7ykybq82rkva9v583a6qkmrfxasxvji6afgs")))

