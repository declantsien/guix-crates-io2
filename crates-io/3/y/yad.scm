(define-module (crates-io #{3}# y yad) #:use-module (crates-io))

(define-public crate-yad-0.1.0 (c (n "yad") (v "0.1.0") (d (list (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fb31nmvc8h0w6azhbmw19vz2nx48bkywgzzvvby62j9zzwjsm4r")))

(define-public crate-yad-0.1.1 (c (n "yad") (v "0.1.1") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1p47mg4zywnncgr9kns5rfmyqkfv2w0skdvpvyxbb2yw5sj8v916")))

(define-public crate-yad-0.2.0 (c (n "yad") (v "0.2.0") (d (list (d (n "nix") (r "^0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0gjs7xzcbck0crfm955415a7wr2jcm2xjw76cjdq8wjdhd43r81v")))

