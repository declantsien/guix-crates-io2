(define-module (crates-io #{3}# y ycc) #:use-module (crates-io))

(define-public crate-ycc-0.0.0 (c (n "ycc") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.22") (f (quote ("derive"))) (d #t) (k 0)) (d (n "yggdrasil-bootstrap") (r "^0.1") (d #t) (k 0)))) (h "07zmsjlxy7c7sv7r6hh9v5b2gcdvp9k6zr9z5qqcqpqh460cz46n")))

