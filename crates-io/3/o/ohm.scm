(define-module (crates-io #{3}# o ohm) #:use-module (crates-io))

(define-public crate-ohm-0.0.0 (c (n "ohm") (v "0.0.0") (d (list (d (n "back") (r "^0.1") (d #t) (k 0) (p "gfx-backend-vulkan")) (d (n "bitvec") (r "^0.10") (d #t) (k 0)) (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "enum_dispatch") (r "^0.1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "gfx-hal") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "relevant") (r "^0.4") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1wyqzm8rbmig5ff22v46mzhvy7wzbkphzdpdvc6bs5l5agjf4cb7")))

