(define-module (crates-io #{3}# o obj) #:use-module (crates-io))

(define-public crate-obj-0.0.1 (c (n "obj") (v "0.0.1") (d (list (d (n "genmesh") (r "~0.0.1") (d #t) (k 0)))) (h "1xjqbz8j4qwp4kz99qi88zq9l1mlq8wkp2adi900h2i87y71vvf9")))

(define-public crate-obj-0.0.2 (c (n "obj") (v "0.0.2") (d (list (d (n "genmesh") (r "~0.0.1") (d #t) (k 0)))) (h "1rj75mbvyk3r672n0slxsb6j1drb0n345g1hc06svx89rjsq74v4")))

(define-public crate-obj-0.0.3 (c (n "obj") (v "0.0.3") (d (list (d (n "genmesh") (r "*") (d #t) (k 0)))) (h "0ix13rva378g2nyahd4yljimlm8n1xwiwzd3vmnzl712rwia57h5")))

(define-public crate-obj-0.0.4 (c (n "obj") (v "0.0.4") (d (list (d (n "genmesh") (r "*") (d #t) (k 0)))) (h "07gi6q6pkfvh2jyb961z2gkphwchawi959mnk32s9d4i4421pbzf")))

(define-public crate-obj-0.0.5 (c (n "obj") (v "0.0.5") (d (list (d (n "genmesh") (r "*") (d #t) (k 0)))) (h "0csdk8cvb56n1bkiyrh86xfmxlk1h1xc44ywaqnsvq40wvhb982c")))

(define-public crate-obj-0.0.6 (c (n "obj") (v "0.0.6") (d (list (d (n "genmesh") (r "*") (d #t) (k 0)))) (h "0d6qqs4hq0xhy0wxwcb8z1x3afds836n6hc71pigzbb8c74jpypm")))

(define-public crate-obj-0.0.7 (c (n "obj") (v "0.0.7") (d (list (d (n "genmesh") (r "*") (d #t) (k 0)))) (h "06pb21hsi2xhg9n10gmn1fmax572y58rylvfhv4lvaqga9100wvy")))

(define-public crate-obj-0.0.8 (c (n "obj") (v "0.0.8") (d (list (d (n "genmesh") (r "^0.0.10") (d #t) (k 0)))) (h "1yxpm36hp1z2i3rbknwpw697r3fkbbbrprdxpdzs82qrmiwl6mha")))

(define-public crate-obj-0.0.9 (c (n "obj") (v "0.0.9") (d (list (d (n "genmesh") (r "^0.0.11") (d #t) (k 0)))) (h "0hxa9n62k0vnc7avxpdcnsvl0s7q96d1q5iwpklgi9qq9fnsqcb8")))

(define-public crate-obj-0.0.10 (c (n "obj") (v "0.0.10") (d (list (d (n "genmesh") (r "^0.0.12") (d #t) (k 0)))) (h "02qckz2sr9q5fl5n7kj0cn9pqs12agh8qd09163r59a4mhj037rd")))

(define-public crate-obj-0.0.11 (c (n "obj") (v "0.0.11") (d (list (d (n "genmesh") (r "^0.0.13") (d #t) (k 0)))) (h "1izh4hc1hp1wk2cz6m0m6p5fn8kpfqlcsdn5v237xswxa87gvdmb")))

(define-public crate-obj-0.1.0 (c (n "obj") (v "0.1.0") (d (list (d (n "genmesh") (r "0.1.*") (d #t) (k 0)))) (h "005qwycdrwqk35yv1gd31z63f54vfxwrj0fr0b1rm4a3jnl1yhk8")))

(define-public crate-obj-0.1.1 (c (n "obj") (v "0.1.1") (d (list (d (n "genmesh") (r "0.1.*") (d #t) (k 0)))) (h "0sz5x13ivbawjx064yd1cymql5r446xi7sihh7dyh1zzivvj4b28")))

(define-public crate-obj-0.1.2 (c (n "obj") (v "0.1.2") (d (list (d (n "genmesh") (r "0.2.*") (d #t) (k 0)))) (h "0l23lba4h4fdh3ah4r8c61glc1h9fs3nny3p2qbgah9g425bi382")))

(define-public crate-obj-0.2.0 (c (n "obj") (v "0.2.0") (d (list (d (n "genmesh") (r "0.2.*") (d #t) (k 0)))) (h "1lsfdvvf1pbi3piaaiza3wmza7q75l4nz6hiz93h9kdxl7vqn7c8")))

(define-public crate-obj-0.2.1 (c (n "obj") (v "0.2.1") (d (list (d (n "genmesh") (r "0.2.*") (d #t) (k 0)))) (h "027ny0h6qnqn2zslgckfjnsb86m5ri0pj3r63fs5aq5hzvnb6jgm")))

(define-public crate-obj-0.2.2 (c (n "obj") (v "0.2.2") (d (list (d (n "genmesh") (r "0.2.*") (d #t) (k 0)))) (h "1cn5i823vkfqw5jnjng8sz4lb60xw1jxrgygkpc8syza3bl5wz66")))

(define-public crate-obj-0.3.0 (c (n "obj") (v "0.3.0") (d (list (d (n "genmesh") (r "0.3.*") (d #t) (k 0)))) (h "0g3h9h0m0wfb6vf1fzxfyrf1jv6xycw866nsqnyrygsg5x4zxj1k")))

(define-public crate-obj-0.4.0 (c (n "obj") (v "0.4.0") (d (list (d (n "genmesh") (r "0.3.*") (o #t) (d #t) (k 0)))) (h "0ypwn6da3fv6g9wna45p9vy7ndyx8vg3445fy4xyprhmn6nixsz1") (f (quote (("usegenmesh" "genmesh") ("default"))))))

(define-public crate-obj-0.5.0 (c (n "obj") (v "0.5.0") (d (list (d (n "genmesh") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0pl9blz3pj81yk2wih0nrbdmzg1vag0c0159xfdk818zzq91hch2") (f (quote (("usegenmesh" "genmesh") ("default"))))))

(define-public crate-obj-0.6.0 (c (n "obj") (v "0.6.0") (d (list (d (n "genmesh") (r "^0.5") (o #t) (d #t) (k 0)))) (h "08hj1q8p1bzw2af5rq9v4lz4ksf3wi9ybrxh6kycxhvv9b3f3a62") (f (quote (("default"))))))

(define-public crate-obj-0.7.0 (c (n "obj") (v "0.7.0") (d (list (d (n "genmesh") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1agv9qrp87jjrgj6prykaxy3izswq44p1idz6vhbva103a80i374") (f (quote (("default"))))))

(define-public crate-obj-0.8.0 (c (n "obj") (v "0.8.0") (d (list (d (n "genmesh") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0w29r0maclwysr5rp5ppf31d2vcibl8939gffc5mpz6hz4zapjdq") (f (quote (("default"))))))

(define-public crate-obj-0.8.1 (c (n "obj") (v "0.8.1") (d (list (d (n "genmesh") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0kl2nxbii9mhvg79sh0i1i3m23jlq6x5nriq5ckhvzipj2ry3xsb") (f (quote (("default"))))))

(define-public crate-obj-0.8.2 (c (n "obj") (v "0.8.2") (d (list (d (n "genmesh") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0bgamj3fvgg07gcxlsfl4d5p4sh3gwni8whpwh06yv0q04wjqp6l") (f (quote (("default"))))))

(define-public crate-obj-0.9.0 (c (n "obj") (v "0.9.0") (d (list (d (n "genmesh") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1p550ws511h9qic01ppn1p3kgzwyhd2gd1rnrb2z17hgc8yv9bxh") (f (quote (("default"))))))

(define-public crate-obj-0.9.1 (c (n "obj") (v "0.9.1") (d (list (d (n "genmesh") (r "^0.6") (o #t) (d #t) (k 0)))) (h "10z1r2r0xyhr4j1n07135kz4bc0zhqy98vabs99vz0x171bi8gy0") (f (quote (("default"))))))

(define-public crate-obj-0.10.0 (c (n "obj") (v "0.10.0") (d (list (d (n "genmesh") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0p1lq2xli701jm1iklddq6wc8vjz8hzk25sirsxrv89zaqyawsll") (f (quote (("default"))))))

(define-public crate-obj-0.10.1 (c (n "obj") (v "0.10.1") (d (list (d (n "genmesh") (r "^0.6") (o #t) (d #t) (k 0)))) (h "14i6rl9xqz6v765aaj1i1rvvpqwnvfwqf6pgfbgm5arfylfaj46m") (f (quote (("default"))))))

(define-public crate-obj-0.10.2 (c (n "obj") (v "0.10.2") (d (list (d (n "genmesh") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0jks8qzjn59na40dv9m0q2j540i04cddbkbq02scgk9qawj9b705") (f (quote (("default"))))))

