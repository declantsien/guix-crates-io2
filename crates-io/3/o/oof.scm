(define-module (crates-io #{3}# o oof) #:use-module (crates-io))

(define-public crate-oof-0.0.0 (c (n "oof") (v "0.0.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.0") (f (quote ("u128"))) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0xz6khj1j1xnm52qsv0xc5l4q75r8m7d0yrm6nvb7v0maqwk4m3w")))

(define-public crate-oof-0.0.1 (c (n "oof") (v "0.0.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.0") (f (quote ("u128"))) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1kl9viz7i5w0smh86gyj8vyv32wfa33893jvw2n8qdpvwg2dxa7d")))

(define-public crate-oof-0.0.2 (c (n "oof") (v "0.0.2") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.0") (f (quote ("u128"))) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0qhfa6ljc7azi3j4qpk3b4d073415j2damf8mg3zwk8gd6h09yjg")))

(define-public crate-oof-0.0.3 (c (n "oof") (v "0.0.3") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.0") (f (quote ("u128"))) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "09rfqixl4lmkvaw38c4isgz38pq8amgkzg1vb5c0x1fv79gk4gbn")))

(define-public crate-oof-0.1.1 (c (n "oof") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bonsai") (r "^0.2.1") (f (quote ("u128"))) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1b8ppsrais376cxg6lzzjzx943wazmw6wr434ar03crcvvk3c2dn") (f (quote (("generate") ("default"))))))

