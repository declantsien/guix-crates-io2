(define-module (crates-io #{3}# o ofx) #:use-module (crates-io))

(define-public crate-ofx-0.1.0 (c (n "ofx") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "ofx_sys") (r "^0.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)))) (h "0nm97f6w9ck5h7h1dd7m8y2nzczz1nw807vzkcs4xn7rd31bhvzh")))

(define-public crate-ofx-0.2.0 (c (n "ofx") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "ofx_sys") (r "^0.1.1") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)))) (h "1bwnyqk0fzxc32415klv8zn7zpq97234a8jfwxzrdlm3i3lrir7d")))

(define-public crate-ofx-0.3.0 (c (n "ofx") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "ofx_sys") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.7") (d #t) (k 0)))) (h "0vh568fdaf3mzkffjp29qi5ayrqaggbn8123d70jbqslk4aishdh")))

