(define-module (crates-io #{3}# o oxy) #:use-module (crates-io))

(define-public crate-oxy-0.1.0 (c (n "oxy") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)))) (h "0xgcsifyj4r1s9p2jfpv814fqhiijb6g3c7540swpsjhi6jjxjs9")))

(define-public crate-oxy-0.1.2 (c (n "oxy") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)))) (h "1fvnf5ibbjapxsilf8kr5skcmnx86g3fa32nplc6ix2g01ghvip5")))

(define-public crate-oxy-0.1.4 (c (n "oxy") (v "0.1.4") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)))) (h "07i6d8l064b7idkkd2f12mg1f3rhcjq9i79r9zxbbb853xpq891k")))

(define-public crate-oxy-0.1.5 (c (n "oxy") (v "0.1.5") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)))) (h "0b5mk4933yjdr5naayl4cfcfqm1c2d647996wzf6wjwnchlw5b8l")))

(define-public crate-oxy-0.1.6 (c (n "oxy") (v "0.1.6") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)))) (h "0q01725nf38l73lk39j7afl41b9h1mgkcr5zfsvd8vh9k58rcpny")))

(define-public crate-oxy-0.2.0 (c (n "oxy") (v "0.2.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "scrap") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.34") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.34") (d #t) (k 0)))) (h "1r6lfl5050z649wrbd3jrn9g6zsgxk6a0bsbsp4hpj6vpfj8nxf9")))

