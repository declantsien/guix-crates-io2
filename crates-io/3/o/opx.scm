(define-module (crates-io #{3}# o opx) #:use-module (crates-io))

(define-public crate-opx-0.0.0 (c (n "opx") (v "0.0.0") (d (list (d (n "pgrx") (r "=0.8.3") (d #t) (k 0)) (d (n "pgrx-tests") (r "=0.8.3") (d #t) (k 2)))) (h "155wzkaihwfagr8fpbz28p3d4bjdfl438mban1fnjavr85af1gah") (f (quote (("pg_test") ("pg15" "pgrx/pg15" "pgrx-tests/pg15") ("pg14" "pgrx/pg14" "pgrx-tests/pg14") ("pg13" "pgrx/pg13" "pgrx-tests/pg13") ("pg12" "pgrx/pg12" "pgrx-tests/pg12") ("pg11" "pgrx/pg11" "pgrx-tests/pg11") ("default" "pg13"))))))

(define-public crate-opx-0.0.6 (c (n "opx") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0kihq00ncy2mm2r8a1c7s4abxwhmqn1dpkhzjlz935as303cvsk6")))

