(define-module (crates-io #{3}# o omp) #:use-module (crates-io))

(define-public crate-omp-0.1.0 (c (n "omp") (v "0.1.0") (d (list (d (n "omp-gdk") (r "^0.1.0") (d #t) (k 0)))) (h "00syhki1dqd9si4hxfjyxdjmh09fga0zj94gyfnlbhhdi53k3m0n")))

(define-public crate-omp-0.2.0 (c (n "omp") (v "0.2.0") (d (list (d (n "omp-gdk") (r "^0.2.0") (d #t) (k 0)))) (h "0qai7jidywiqddvnp0hhsbz7ppys9z06qys3wjv8fw38aysb938l")))

