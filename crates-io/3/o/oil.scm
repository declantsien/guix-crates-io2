(define-module (crates-io #{3}# o oil) #:use-module (crates-io))

(define-public crate-oil-0.1.0 (c (n "oil") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "cgmath") (r "*") (d #t) (k 0)) (d (n "clock_ticks") (r "*") (d #t) (k 2)) (d (n "glium") (r "*") (f (quote ("default" "cgmath"))) (d #t) (k 0)) (d (n "glutin") (r "*") (d #t) (k 2)) (d (n "image") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "oil_parsers") (r "= 0.1.0") (d #t) (k 0)) (d (n "oil_shared") (r "= 0.1.0") (d #t) (k 0)) (d (n "phf") (r "*") (d #t) (k 0)) (d (n "phf_macros") (r "*") (d #t) (k 0)))) (h "113csfqiy3hbwknjs8h67jfi0rl7n19wq66gfb0a8svjnkr8vxna")))

