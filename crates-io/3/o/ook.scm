(define-module (crates-io #{3}# o ook) #:use-module (crates-io))

(define-public crate-ook-0.1.0 (c (n "ook") (v "0.1.0") (h "106j6kdg6npvjizg61987qfxwr33wzsgc3dm6pyq17drvgcr7j4r")))

(define-public crate-ook-0.1.1 (c (n "ook") (v "0.1.1") (h "1qphmm89rd9s77bkz40fjg9vnyifd3wq3x3l42n1g2xgzg5isaqj")))

(define-public crate-ook-0.1.2 (c (n "ook") (v "0.1.2") (h "0rrjap36jx0q1hyng9wgrr3ya6d8dcw1rgkc3bqfdizqy8zb1hjn")))

