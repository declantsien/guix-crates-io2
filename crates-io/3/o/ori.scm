(define-module (crates-io #{3}# o ori) #:use-module (crates-io))

(define-public crate-ori-0.1.0-alpha.0 (c (n "ori") (v "0.1.0-alpha.0") (d (list (d (n "ori-core") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ori-graphics") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ori-macro") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ori-wgpu") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "ori-winit") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)))) (h "0xwwz7hvqv33gaf6qgsj3agpnx27gficffnd5rzj1vms0ik1l7si") (f (quote (("winit" "ori-winit") ("wgpu" "ori-wgpu" "ori-winit/wgpu") ("default" "winit" "wgpu"))))))

(define-public crate-ori-0.1.0-alpha.1 (c (n "ori") (v "0.1.0-alpha.1") (d (list (d (n "ori-core") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ori-graphics") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ori-macro") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "ori-wgpu") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)) (d (n "ori-winit") (r "^0.1.0-alpha.0") (o #t) (d #t) (k 0)))) (h "0hikivska8j5l8zrsbckjrjh59rp4dzd1kd6pd77kkp9sa2h1a5y") (f (quote (("winit" "ori-winit") ("wgpu" "ori-wgpu" "ori-winit/wgpu") ("default" "winit" "wgpu"))))))

