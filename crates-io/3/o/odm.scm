(define-module (crates-io #{3}# o odm) #:use-module (crates-io))

(define-public crate-odm-0.0.1 (c (n "odm") (v "0.0.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "open-stock") (r "^0.1.4") (d #t) (k 0)) (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "0xsysczrclg6m9fi3d19bvhwkcjd8p3c157f7da35aammrmfryh1")))

