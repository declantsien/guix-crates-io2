(define-module (crates-io #{3}# o ova) #:use-module (crates-io))

(define-public crate-ova-0.1.0 (c (n "ova") (v "0.1.0") (h "0sgjy52dvwx6j31spqirn2gwgv3j6a0f6q75dx99kzfjdr9b8z9r") (y #t)))

(define-public crate-ova-0.1.1 (c (n "ova") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "totp-rs") (r "^5.0.1") (d #t) (k 0)))) (h "0xig0a5whja7xbli3q1dbpq0dr90s1vra3mfq4980fnh5fs6cz6s") (y #t)))

(define-public crate-ova-0.1.2 (c (n "ova") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "totp-rs") (r "^5.0.1") (d #t) (k 0)))) (h "06gvy7hqd2snfy1lx3354qn43crvg81m32yd1aca8hmq432jiks5") (y #t)))

(define-public crate-ova-0.1.3 (c (n "ova") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "totp-rs") (r "^5.0.1") (d #t) (k 0)))) (h "0230f055wi53jkdhcpv5jz3b6nqgz11d7kf9viai5gz2h1ciwjw5") (y #t)))

(define-public crate-ova-0.1.5 (c (n "ova") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "totp-rs") (r "^5.0.1") (d #t) (k 0)))) (h "0whpw0k1fwqgqbyfxcjang5m56qaplx4nvqi33k0xjz2y9z5aph4") (y #t)))

(define-public crate-ova-0.1.6 (c (n "ova") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "totp-rs") (r "^5.0.1") (d #t) (k 0)))) (h "1wwhkqrqky1752d3gy52bmwy3q39dg5d62hcgv8lz92nwphxy108") (y #t)))

