(define-module (crates-io #{3}# o oci) #:use-module (crates-io))

(define-public crate-oci-0.1.0 (c (n "oci") (v "0.1.0") (h "0q7snnhw8xbf5s83f738plk8z80mp2dkbsgak5j8fx8z452a58bz")))

(define-public crate-oci-0.1.1 (c (n "oci") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1a5xvgjw90qch9bm9jnwszdg3xsbs32gv1ap19pgfi4f11cv94k9")))

