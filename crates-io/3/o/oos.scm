(define-module (crates-io #{3}# o oos) #:use-module (crates-io))

(define-public crate-oos-0.1.0 (c (n "oos") (v "0.1.0") (h "1w4cmm5cbl4nmxjv4pdhpx9g67zla0l0wqjmh8whlz4934ivs65q") (y #t)))

(define-public crate-oos-0.1.1 (c (n "oos") (v "0.1.1") (h "0ydkd4mx9q8xcsj8pqlz8skaky0p3dds9f3ih36lqv0k280agpx0") (y #t)))

(define-public crate-oos-0.1.3 (c (n "oos") (v "0.1.3") (h "0infviw97i6c8dpj6xqprak81rdj997rbrwh2065hmmb8v2i82cx") (y #t)))

(define-public crate-oos-0.1.4 (c (n "oos") (v "0.1.4") (h "1a7jcylxg4jr5m5gak2155y0f0rmcvfb65bzhw896y9a0lq4y619") (y #t)))

(define-public crate-oos-0.1.0-x-0 (c (n "oos") (v "0.1.0-x-0") (h "1r4rf7ml08h2rjjwm84l584w9gcb2gqw9wwaqjbh5gn04iqk0yrm") (y #t)))

(define-public crate-oos-0.1.0-x-0-0 (c (n "oos") (v "0.1.0-x-0-0") (h "19xjd2n5ynnw86dahkngdz485kirjzcmhff7cnmxb9fwljy0pafw") (y #t)))

(define-public crate-oos-0.0.0-x-0-0-0-0-0 (c (n "oos") (v "0.0.0-x-0-0-0-0-0") (h "0cp6xfpj06wkrjqh2ip8wkr36x1brwzaccja9gw2dhsfnz59qv6n") (y #t)))

(define-public crate-oos-0.0.0-0-0-0-0-0-test-alpha (c (n "oos") (v "0.0.0-0-0-0-0-0-test-alpha") (h "1swszw4dngngrxb0v7lcci12bkiqxbw3wff67b5y5ivwkgm1igh6") (y #t)))

(define-public crate-oos-0.0.1-test-alpha (c (n "oos") (v "0.0.1-test-alpha") (h "08jmc8cjvfcwcxgk3h1xndjmxxq9r6zgcb88p2sjdb0ir6f0aadx")))

