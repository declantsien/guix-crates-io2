(define-module (crates-io #{3}# o o-o) #:use-module (crates-io))

(define-public crate-o-o-0.2.0 (c (n "o-o") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "zgclp") (r "^0.3") (d #t) (k 0)))) (h "1j18m7vyb9jiaw5iaq8gmv7kjwhn38pphjgwdb6mcb05zmwimssz")))

(define-public crate-o-o-0.2.1 (c (n "o-o") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "zgclp") (r "^0.3") (d #t) (k 0)))) (h "1id58qgw96043y6cs8hqkiypm1nmf9xp64kwib1391z0p4asf026")))

(define-public crate-o-o-0.2.2 (c (n "o-o") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "zgclp") (r "^0.3") (d #t) (k 0)))) (h "0rhdl9ylw1w46ifja6wq5myriih6p5qhq78pf010k79yim5ssrjx")))

(define-public crate-o-o-0.2.3 (c (n "o-o") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "zgclp") (r "^0.3") (d #t) (k 0)))) (h "1yn8aagfdy08rw0xldk1ym4i9g0g0nm6xd4xdk7f3fph9nvq5qxc")))

(define-public crate-o-o-0.3.0 (c (n "o-o") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "zgclp") (r "^0.3") (d #t) (k 0)))) (h "03a8b03vrn16v317wg1nlk9ssq8difz0vqmgqncia3wq8zfy6z11")))

(define-public crate-o-o-0.3.1 (c (n "o-o") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "zgclp") (r "^0.3") (d #t) (k 0)))) (h "0zblhbcnz4zkp9mggb6zvnkxxskjsl04xdr191prv0mp9qdk4gz9")))

(define-public crate-o-o-0.3.4 (c (n "o-o") (v "0.3.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ms4fb06ddn3pmp0ra3fa2iplja06ffj50276kzbyfhb5y326i06")))

(define-public crate-o-o-0.3.5 (c (n "o-o") (v "0.3.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "1fdxmj2gaxi646sah9zmcmr8w58c3vlfiv5adyhpmwmf7whd5vq2") (y #t)))

(define-public crate-o-o-0.3.6 (c (n "o-o") (v "0.3.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1aswj07ix22k2lmpkvm2jd94cr625fjwlnj1rwh9znshyx5n9s6s")))

(define-public crate-o-o-0.4.0 (c (n "o-o") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wl6373313clkjy23zq648dpfla5v8aff043anf1r81n8nh5p0av") (y #t)))

(define-public crate-o-o-0.4.1 (c (n "o-o") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k488imja1y9k6bqmvs4qpx8dpagmv0p8bpxq5cixaxhl44dsmjk")))

(define-public crate-o-o-0.4.2 (c (n "o-o") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12lx1lf883pl00jrv6095fx5kcnh5jx1aa0ip1mbh42xs9kaaagl")))

(define-public crate-o-o-0.4.3 (c (n "o-o") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "011qpyi3fzrijmkcizjxyzxl5akj92r2v2x64fvyv7yvca3blb0m")))

(define-public crate-o-o-0.4.4 (c (n "o-o") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ira5dhqcjkj6h206fryyb82k6j4q5qwmwkk6rbqiiailz1qyfg4")))

(define-public crate-o-o-0.4.5 (c (n "o-o") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pfl5raqq0lh9vzxfi5nyqhpgi629zrn3ys6ff639xvbrakdg5zh")))

(define-public crate-o-o-0.5.1 (c (n "o-o") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h9rjc1irbq1dvrc49g71pwhx4gidznik7c4f603pgqnjwkgid6s")))

(define-public crate-o-o-0.5.2 (c (n "o-o") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "duct") (r "^0.13") (d #t) (k 0)) (d (n "ng-clp") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14g8a72z50kjw3ddqnrnsyy2bs95jr88ysglr6zgqpnb1lgqmxdr")))

