(define-module (crates-io #{3}# o ooo) #:use-module (crates-io))

(define-public crate-ooo-0.0.0 (c (n "ooo") (v "0.0.0") (h "0y3022dknh1ajr57xh70pkr3ax940cy54kbk7nnxljm1a8741xh7") (y #t)))

(define-public crate-ooo-0.0.1 (c (n "ooo") (v "0.0.1") (h "178wv5n42h39ml9b8zmdfm0ks765pq216dd1nwhns1qssvq4df9j") (y #t)))

(define-public crate-ooo-0.0.2 (c (n "ooo") (v "0.0.2") (h "1z1cjj3h9wvsn469vmx1nisbkjbrijx17cnssrynlxyncz1q6kg3") (y #t)))

