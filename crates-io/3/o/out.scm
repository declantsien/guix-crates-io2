(define-module (crates-io #{3}# o out) #:use-module (crates-io))

(define-public crate-out-0.1.0 (c (n "out") (v "0.1.0") (h "11mi2gyvwy61ffykdy7ky9f41vw11m3sa5afjcj39fskqxa0k5jq") (y #t)))

(define-public crate-out-0.2.0 (c (n "out") (v "0.2.0") (h "15lisav9qhb994hjmb4f1fms32mhxk17h4fiaw92y0cqs4rmh9vk") (y #t)))

(define-public crate-out-0.2.1 (c (n "out") (v "0.2.1") (h "1f08fd7ncjz228kfy4ycknzlmh4iikindf7qa93g3ggx0j6sgaj2") (y #t)))

(define-public crate-out-0.3.0 (c (n "out") (v "0.3.0") (h "1aybjr07vlwl2lhkrmwljq3bav5flcjhri7r1fqj13maylxd113z") (y #t)))

(define-public crate-out-0.3.1 (c (n "out") (v "0.3.1") (h "1vr6d3wc5vj3zw1mr58fkbv015s1xjyyxhan075hym4jngdd9y1h") (y #t)))

(define-public crate-out-0.3.2 (c (n "out") (v "0.3.2") (h "1a0i4gnnzwzq6mbw8d1kqkl5jv4dmq38mc8xrdlff3lj4dxi1k4g") (y #t)))

(define-public crate-out-0.4.0 (c (n "out") (v "0.4.0") (h "0cwxwsz7gda2bd2qnvhaf9bd1pq3bf5abqgk7w7h2063gjwr3dqg") (y #t)))

(define-public crate-out-0.4.1 (c (n "out") (v "0.4.1") (h "0qqg8i70643qc3lwzl5mdjbdam0y8lp8kn159ym9hz8bx82wvq4z") (y #t)))

(define-public crate-out-0.4.2 (c (n "out") (v "0.4.2") (h "10xmiiix0vb7h80iq3zsrf45xljqabpjzgq0asz4v5ihh25ls83n") (y #t)))

(define-public crate-out-0.4.3 (c (n "out") (v "0.4.3") (h "0k2is7kwk68dnr0sjg18a20paiizsan4pkig179v5yy1p5kqdjds") (y #t)))

(define-public crate-out-0.5.0 (c (n "out") (v "0.5.0") (h "0p6fxs986lqypzcx0rxwnmchh5sncs6yskvqx8py5ygvhvq4jbgr") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.1 (c (n "out") (v "0.5.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1nq4xak086qjkxqm01kdn20ng8scm492lg7sx1wxyf5k26afmv2v") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.2 (c (n "out") (v "0.5.2") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "123kj8piadk6aa1b5fl1yg4q001x6wlirs2r356p57x70lig45nm") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.3 (c (n "out") (v "0.5.3") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1iia4jjraizs774nljs0ny4gcyhcjfzi1pcaaf06az2x9zmqacwn") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.4 (c (n "out") (v "0.5.4") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0rfj77y4brkzyn21alxwclfa3v5a48x5gmi25hdjg755xb2nilf8") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.5 (c (n "out") (v "0.5.5") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1s9ajsm2yv2yhhd23601i6g1sa8sjj2jkkpgxpyh5g22rhgi1jxy") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.6 (c (n "out") (v "0.5.6") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "13nh5aaf5pvkhzgaf5ap78jlgpfj7dwhy183qrczci0lddgx9r0c") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.7 (c (n "out") (v "0.5.7") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1hjnb166whw3j3gm7cxychw5bvhrsk6brj7pn2l3gm0znm4q3gw5") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.8 (c (n "out") (v "0.5.8") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1xf39nnawm4p24j5frib9a4gbggsajn53291pyav7y73fi8f47pl") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.9 (c (n "out") (v "0.5.9") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "00km12hf20c7dmxd7n9aw5282g2rd47nnzmzs97bn4kr725rk5bj") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.10 (c (n "out") (v "0.5.10") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1lifh6qxz091y801a655mj369qkv0aqcbxdmkja2gfabcl7rnnzd") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-0.5.11 (c (n "out") (v "0.5.11") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "14zm3rrx2038jvbwria65vvpfnd4281j7fc3ypwrdq3salngv511") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-1.0.0 (c (n "out") (v "1.0.0") (d (list (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "074dvidkrnns840py55b05ywc0gc1b8h94ch32ilhnvrjic5n695") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-out-2.0.0 (c (n "out") (v "2.0.0") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1vw1f6jr94vvv0ffbsrv1cz0qjdafxi1k1vnrfwlszww8vvjl3a7") (f (quote (("std") ("default" "std"))))))

(define-public crate-out-2.0.1 (c (n "out") (v "2.0.1") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1zy5kpl2q6bh6g40vjqyg1ibw8g91ipk55lydyy06vwzqdxm08f2") (f (quote (("std") ("default" "std"))))))

(define-public crate-out-2.0.2 (c (n "out") (v "2.0.2") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "08dgnyx87x15z6d0v50y4hrp5ibp8x1qppzlgg4103mlnwkmpy7j") (f (quote (("std") ("default" "std"))))))

(define-public crate-out-2.0.3 (c (n "out") (v "2.0.3") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0ax5jfx7d0pj9y0v6asgaqb9kph92gifqmx9p4vp23cj72bdw723") (f (quote (("std") ("default" "std"))))))

(define-public crate-out-2.0.4 (c (n "out") (v "2.0.4") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0cnzhizrsmj3gdh8x18bsz8s48blaw0lqnvf8xqy72awxwr9h25c") (f (quote (("std") ("default" "std"))))))

(define-public crate-out-2.0.5 (c (n "out") (v "2.0.5") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1lcgfdhz6g45n8lpnc3z14m7fdpwmrmcms4a7rmnph0s36mghzzb") (f (quote (("std") ("default" "std"))))))

(define-public crate-out-2.0.6 (c (n "out") (v "2.0.6") (d (list (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1awx35faiijjq35j6ayj79fwgr19jawhnmplzf9mi873b9a39kw2") (f (quote (("std") ("default" "std"))))))

(define-public crate-out-2.0.7 (c (n "out") (v "2.0.7") (d (list (d (n "quickcheck") (r "^0.8.5") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "06gg1mvrrs5kxz984ifgifps3mka5zaj3d7mgcv1wd49ij45rc03") (f (quote (("std") ("default" "std"))))))

(define-public crate-out-3.0.0 (c (n "out") (v "3.0.0") (d (list (d (n "quickcheck") (r "^0.8.5") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "04r3r5axsq3xwnph4n9la483lnarpqhryq4id514jr051vc8v19l") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-3.1.0 (c (n "out") (v "3.1.0") (d (list (d (n "quickcheck") (r "^0.8.5") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1wjxx7lm1ng9gz8z8swqsr8vyy039ps4jgzxjh4jy0jwfbssx9m4") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-3.1.1 (c (n "out") (v "3.1.1") (d (list (d (n "quickcheck") (r "^0.8.5") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0cvbnn0qgf9gngvrrl8g7nb5jmpasszkcs05rp9c93rx7zvxlhjz") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-3.1.2 (c (n "out") (v "3.1.2") (d (list (d (n "quickcheck") (r "^0.8.5") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1n49nhfxp88yhhf9bkanrq96xfspp4p9ab32i4wcxdmcfylljwlb") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-3.1.3 (c (n "out") (v "3.1.3") (d (list (d (n "quickcheck") (r "^0.8.5") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1fzv0w0sv4l8lf127qq75hhbkhsr8c7aqxrx9bbnxnf13if52mdd") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-4.0.0 (c (n "out") (v "4.0.0") (d (list (d (n "quickcheck") (r "^0.8.5") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1rm7a49zrh61m3ps7by78lkmj57q07l3bphha0zl04z63wsdnvhc") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-4.0.1 (c (n "out") (v "4.0.1") (d (list (d (n "quickcheck") (r "^0.8.5") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "15sw3yw39f7ykgac4rflxzv9ka55wbfzsmz42prag90yn0ijsd8i") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-4.0.2 (c (n "out") (v "4.0.2") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0kkd3xz594x778rdhnrwdcvqqhjrmh9j16v63p1zk1svqcpdm5aw") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-5.0.0 (c (n "out") (v "5.0.0") (d (list (d (n "quickcheck") (r "^0.9.0") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.2") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "1wqyf7nrb3wflqg89ssh9cllgf605w8g9h6gqqbjvv8zxjrspj7p") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-5.0.1 (c (n "out") (v "5.0.1") (d (list (d (n "quickcheck") (r "^0.9.1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0hj6lb9vngjlw7fc96nnjfmvn9avmzqgcywjz8y2j5pycrrjf0wq") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-6.0.0 (c (n "out") (v "6.0.0") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0i6w1z7f9y7fpicj43mqxwynds4c448n9hkrlhyrpzlmsisqxwfh") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-6.1.0 (c (n "out") (v "6.1.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0hr49df6vyfpxi01a00iwf8fx8y9bzvzisg0hsm7pd66531x0dkz") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-out-7.0.0 (c (n "out") (v "7.0.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "11mi58389sxd816x9i0ihpxbjq2nl93598in143lasjglvw7zk3w") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-out-8.0.0 (c (n "out") (v "8.0.0") (d (list (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (d #t) (k 2)))) (h "0cpn4zfgk1wpx2b198ic1jdxdc4qdnbvlr9qca6pap2hb0705fll") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

