(define-module (crates-io #{3}# o olm) #:use-module (crates-io))

(define-public crate-olm-0.1.0 (c (n "olm") (v "0.1.0") (h "0xrri9xgy8rijwzvrn97xr6bww87m6gs0ryck1cscz5chzh0f8wz") (y #t)))

(define-public crate-olm-0.1.1 (c (n "olm") (v "0.1.1") (h "1rnvmbwji9c5gjss94xc6g91phmkkjj3h3glxqnj9lsz5ms0wg85") (y #t)))

(define-public crate-olm-0.1.2 (c (n "olm") (v "0.1.2") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0zns6msi4q2ma7jvxgcqmpdcqllfk2xra521cgnqylm08mp85v86") (y #t)))

