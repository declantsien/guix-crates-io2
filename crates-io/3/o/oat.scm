(define-module (crates-io #{3}# o oat) #:use-module (crates-io))

(define-public crate-oat-0.1.0 (c (n "oat") (v "0.1.0") (h "1jwp0sspd0r692b8l5ylv803vx61ag2vzw9ajjnnsnw8b367p5wn")))

(define-public crate-oat-0.1.1 (c (n "oat") (v "0.1.1") (h "0is04vrb5drhkxvswzb8bf12cky7nxpfmscpn3h5vljaf8klvjg4")))

