(define-module (crates-io #{3}# o oaf) #:use-module (crates-io))

(define-public crate-oaf-0.1.3 (c (n "oaf") (v "0.1.3") (d (list (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1irkf0hvcvbvfm5z0psfkqi4jfdgjjchdbpfgk6b96wnis5snw2r") (f (quote (("strict") ("default" "strict"))))))

(define-public crate-oaf-0.1.4 (c (n "oaf") (v "0.1.4") (d (list (d (n "enum_dispatch") (r "^0.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "096q4ak3fii8fw0pmkv4lkwfpbymzg8rm51sd205ng1mw465znxh") (f (quote (("strict") ("default" "strict"))))))

