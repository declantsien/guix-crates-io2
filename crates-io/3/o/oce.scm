(define-module (crates-io #{3}# o oce) #:use-module (crates-io))

(define-public crate-oce-0.0.1 (c (n "oce") (v "0.0.1") (h "1lv11fcq5s7bganccwm7wa14yfsp91fyfqpi8g6k66rk7d69j79l")))

(define-public crate-oce-0.0.2 (c (n "oce") (v "0.0.2") (h "18qwi3vc7k9h5vyq54wk6cynqdljz6y1gz93vj2r0ymr4ha65zjl")))

(define-public crate-oce-0.0.3 (c (n "oce") (v "0.0.3") (h "1zn99aj99lks74bk3wvsyl41vvnpd5v4icqfg6q4msn53nv5ngfx")))

(define-public crate-oce-0.0.4 (c (n "oce") (v "0.0.4") (h "1kvhm6jq71vgppd2amjn054z630fqf15fn0s962cpydkr3sscf86")))

