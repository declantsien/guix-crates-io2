(define-module (crates-io #{3}# o osu) #:use-module (crates-io))

(define-public crate-osu-0.1.0 (c (n "osu") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "15nc4rrv2b1cv468l4zd5gyaas1pplfhkgskapm7dskk00srfid7")))

(define-public crate-osu-0.1.1 (c (n "osu") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "1v38kjq8c536iwdjhpp27ns4k7ys0v72vkqhdwwpv46bzca3k3l8")))

(define-public crate-osu-0.2.0 (c (n "osu") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "~0.1") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.11") (o #t) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1") (d #t) (k 2)) (d (n "reqwest") (r "~0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "~0.1") (d #t) (k 2)))) (h "1n75s1ra13yacg2i8j55vkdwgqm9byacnx4bxch2lbvpfkq3rcgi") (f (quote (("reqwest-support" "reqwest") ("hyper-support" "futures" "hyper") ("default" "hyper-support"))))))

