(define-module (crates-io #{3}# o omg) #:use-module (crates-io))

(define-public crate-omg-0.1.0 (c (n "omg") (v "0.1.0") (h "0wq6rd47c0wy7zrx8b9ly4jjk50k2ssfghm1gfd3fxgxx32a1w7j") (y #t)))

(define-public crate-omg-0.1.1 (c (n "omg") (v "0.1.1") (h "0y9q0lgy2zh8dcxdy5i557b5hiz87jzmv81jmm7nvs1l02awiqc2") (y #t)))

(define-public crate-omg-0.2.0 (c (n "omg") (v "0.2.0") (h "1rzbgbdr792p5hgkxgdv6bqxd5qp9rsdd0lhy877m7pb04jn6cjy")))

(define-public crate-omg-0.2.1 (c (n "omg") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (f (quote ("password"))) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "omg-api") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.6") (d #t) (k 0)))) (h "0la0wh298zvmilqs3nfb9x20qrrxf3rp03ima06rgfs1sq1vszv5")))

