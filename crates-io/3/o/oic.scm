(define-module (crates-io #{3}# o oic) #:use-module (crates-io))

(define-public crate-oic-0.1.0 (c (n "oic") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.23.1") (d #t) (k 1)) (d (n "bitflags") (r "^0.8.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gcc") (r "^0.3.45") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "slog") (r "^2.0.2") (d #t) (k 0)))) (h "0zilj2v0d5fhpsmm9pmspc5xfmf9qj4y7sacfykjyahffid8pllj")))

