(define-module (crates-io #{3}# o oyi) #:use-module (crates-io))

(define-public crate-oyi-0.1.0 (c (n "oyi") (v "0.1.0") (h "1jgg49ba49kpwk1adkidqbafdn15i1s4vs9pblvhlbvzi18iba6y")))

(define-public crate-oyi-0.1.1 (c (n "oyi") (v "0.1.1") (h "1m2wgrgis5vnlgh0i3zsk79pwxnc0sxv6a6s4dzhvlhmdi5w90vl")))

