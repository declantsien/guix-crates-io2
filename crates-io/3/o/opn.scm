(define-module (crates-io #{3}# o opn) #:use-module (crates-io))

(define-public crate-opn-0.4.0 (c (n "opn") (v "0.4.0") (h "0w33cwjnhwzf0mvj05x3kc9s367przabagpndwqfjx7xpj4hbi8k")))

(define-public crate-opn-0.4.2 (c (n "opn") (v "0.4.2") (h "0y2dscinlyc2n7wwzi8q4a2mm61vq6jja0b3hbl8makm950iaf0r")))

(define-public crate-opn-0.4.7 (c (n "opn") (v "0.4.7") (h "10dj5j35lz5zy7djh6k678qick9m3h210jfwsf1wbfd3868lnmfd")))

