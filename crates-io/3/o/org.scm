(define-module (crates-io #{3}# o org) #:use-module (crates-io))

(define-public crate-org-0.1.0 (c (n "org") (v "0.1.0") (h "135lwana8c13ncy0dzh5fkacm0zw06pl76988b7qda3rdsa8zz2v")))

(define-public crate-org-0.2.0 (c (n "org") (v "0.2.0") (h "1vf4nwsqhzadgxirx0dyy4swsanc1p1d8sf95s954cjz4f83rpyy")))

(define-public crate-org-0.3.0 (c (n "org") (v "0.3.0") (h "18zrdplx95h1aa6pf2h68nd16qki77sd5d1ca243w04a5mf59ynr")))

(define-public crate-org-0.3.1 (c (n "org") (v "0.3.1") (h "135r24z63n5il6lsm59izdgsp1g6nd4blfimcyfssd9pcdxm9pyg")))

