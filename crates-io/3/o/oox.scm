(define-module (crates-io #{3}# o oox) #:use-module (crates-io))

(define-public crate-oox-0.1.0 (c (n "oox") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "quick-xml") (r "^0.17.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.3") (d #t) (k 0)) (d (n "simple_logger") (r "^1.4.0") (d #t) (k 2)) (d (n "strum") (r "^0.17.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.17.1") (d #t) (k 0)) (d (n "zip") (r "^0.5.4") (d #t) (k 0)))) (h "0s37r1am3x75k6fjjkj0yb5gzpvacb8b77saw73s2h9cpw908hm3") (f (quote (("pptx") ("docx") ("all" "docx" "pptx"))))))

