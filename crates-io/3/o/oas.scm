(define-module (crates-io #{3}# o oas) #:use-module (crates-io))

(define-public crate-oas-0.0.1 (c (n "oas") (v "0.0.1") (h "047dxhf8gn016m61vnx13xsv66jyq6sp87sxc7ix7832pji24h1b")))

(define-public crate-oas-0.0.2 (c (n "oas") (v "0.0.2") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "1x1dyrq9xpd3amw1wdb0brjd9hbvkz97v77s2y7rgq9ph9214f29")))

(define-public crate-oas-0.0.3 (c (n "oas") (v "0.0.3") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "08kd0x8lhb9p8s6i7lqx6d3rh2mg10g7l5vxmxi244bmbqa0ghjv")))

(define-public crate-oas-0.0.4 (c (n "oas") (v "0.0.4") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "0sa340q8n7lr5d2173fa01qvmxj22zm37fsa9smy2lg4dvr3xc0z")))

(define-public crate-oas-0.1.0 (c (n "oas") (v "0.1.0") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "either") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)))) (h "16fmi67b9jwzsbxvmfryffdmq0rp0f9qf1wqs89g5yyzb4z61g29")))

