(define-module (crates-io #{3}# o oom) #:use-module (crates-io))

(define-public crate-oom-0.0.1 (c (n "oom") (v "0.0.1") (h "0qw7hw0565kcmx9rrr31sqrpp6fgi3r5iynjbh0w2bsd1670l2zy") (y #t)))

(define-public crate-oom-0.1.0 (c (n "oom") (v "0.1.0") (h "0l0f2kcxixlvq083kqzwk18613hb2nnig6jny9bq2nmjx9lp0z71") (f (quote (("vec" "slice") ("std" "vec") ("slice") ("default" "slice")))) (y #t)))

(define-public crate-oom-0.2.0 (c (n "oom") (v "0.2.0") (h "0l7yqnsqw23msgmh28c3pakkwmbfzjmmsfhl9j5z5326mfvsd5ip") (f (quote (("vec" "slice") ("std" "vec") ("slice") ("default" "slice")))) (y #t)))

(define-public crate-oom-0.3.0 (c (n "oom") (v "0.3.0") (d (list (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "07q7czxbdpxhd0v5x4dnlkrxjs99vd0vcaa5c52xsmyxgv0w97ij") (f (quote (("vec" "slice") ("std" "vec") ("slice") ("default" "slice"))))))

