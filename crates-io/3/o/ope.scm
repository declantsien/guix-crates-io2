(define-module (crates-io #{3}# o ope) #:use-module (crates-io))

(define-public crate-ope-0.1.0 (c (n "ope") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "ctr") (r "^0.9.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)))) (h "0ladc7dqnc89mmpi4wlhq348q3wg6515mca7b9nnd8fpfkk6vm34")))

(define-public crate-ope-0.1.1 (c (n "ope") (v "0.1.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "ctr") (r "^0.9.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)) (d (n "sha2") (r "^0.10.2") (k 0)))) (h "1zr4waa19nd6lshkxwfjgvflxn9mj73iwi5iqmmlmglxwkd2nfbp")))

