(define-module (crates-io #{3}# o orq) #:use-module (crates-io))

(define-public crate-orq-0.1.0 (c (n "orq") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-process") (r "^0.2") (d #t) (k 0)))) (h "1kyhjs2z7l08xqqcgqcfycnkhfcl1h9fsn2cg1d2aad33gcv5b22")))

(define-public crate-orq-0.1.1 (c (n "orq") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1x8qrm1pwswdhzdwa5n7l9pdb5bcc93qc9nys6b2xgmxq5gd4fh5")))

