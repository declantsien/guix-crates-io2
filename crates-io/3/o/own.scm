(define-module (crates-io #{3}# o own) #:use-module (crates-io))

(define-public crate-own-0.1.0 (c (n "own") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "112fdqlkj1if8sfqg73i8g7bf1k93b0n9lar7cxng7jg6v3nbpr3")))

(define-public crate-own-0.1.1 (c (n "own") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.92") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "15hzdqv7mfz61ibs66f5zwnc08hpng00b2fjqb9abaql55phgg6s")))

