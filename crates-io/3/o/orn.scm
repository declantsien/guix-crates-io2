(define-module (crates-io #{3}# o orn) #:use-module (crates-io))

(define-public crate-orn-0.1.0 (c (n "orn") (v "0.1.0") (h "1xj4wn88k9p75i6mn6kjip92jphidw64lx5cl5g8xsnydriibld5") (r "1.63")))

(define-public crate-orn-0.2.0 (c (n "orn") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jwjbif32vvahf8fnbf7sdrzqq56c7grkzmv0m95cj1y9ai3rp2v") (r "1.31")))

(define-public crate-orn-0.3.0 (c (n "orn") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0kp18a7fqjp5z8z3kx9n5xpvwpig1557wilp46czclj1a4i1cxjz") (r "1.57")))

(define-public crate-orn-0.4.0 (c (n "orn") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1b43mndq0x6g3hc4ngh1s6pbj5ffxlz5k78wkz39rr7ayvxmzhig") (r "1.57")))

(define-public crate-orn-0.4.1 (c (n "orn") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10qyy4px2x0gysd8p3qbi056gvf1gin0aw0vn97fwfwzxbfxrnd4") (r "1.57")))

(define-public crate-orn-0.4.2 (c (n "orn") (v "0.4.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hh6dhlqprd2c2k97z01w2ad4ywqjifw1bqrgp9jky4vrm4z0jjc") (r "1.57")))

