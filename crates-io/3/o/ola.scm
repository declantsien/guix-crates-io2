(define-module (crates-io #{3}# o ola) #:use-module (crates-io))

(define-public crate-ola-0.1.0 (c (n "ola") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "net" "time"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "proc-macro2") (r "^1") (d #t) (k 1)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "quote") (r "^1") (d #t) (k 1)))) (h "15wj61pbn97gj6xmx2hgr5br1n6gc6kwrzcrnxcb4h1q9axrhxha") (s 2) (e (quote (("tokio" "dep:tokio"))))))

