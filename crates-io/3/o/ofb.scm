(define-module (crates-io #{3}# o ofb) #:use-module (crates-io))

(define-public crate-ofb-0.1.0 (c (n "ofb") (v "0.1.0") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "1q6h4qn2mqq8ghck3w8v2cj3pqacraaqlx7cfrqa31q90j4mlb1j")))

(define-public crate-ofb-0.1.1 (c (n "ofb") (v "0.1.1") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "155q9fspk97hk1ls1yinzdzp1r6qa2gpj6m7wd2ix05iq1y7r1qa")))

(define-public crate-ofb-0.2.0 (c (n "ofb") (v "0.2.0") (d (list (d (n "aes") (r "^0.4") (d #t) (k 2)) (d (n "block-cipher") (r "=0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.4") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "0cr3c0d3sicmjhn8adkzz5swwxa9q1xqxxwmzw8514wiks0dvqd5")))

(define-public crate-ofb-0.3.0 (c (n "ofb") (v "0.3.0") (d (list (d (n "aes") (r "^0.5") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.7") (f (quote ("block-cipher"))) (d #t) (k 0)) (d (n "stream-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)))) (h "18ppcdqrycaqmlc6wxhldvilwm2z79b7b00ymgrfdpn1g18qas2j")))

(define-public crate-ofb-0.4.0 (c (n "ofb") (v "0.4.0") (d (list (d (n "aes") (r "^0.6") (d #t) (k 2)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0g5rj273qg03vmh1di2d5syz9afqi6abw9v4snmkvnkjigy0krpm")))

(define-public crate-ofb-0.5.0 (c (n "ofb") (v "0.5.0") (d (list (d (n "aes") (r "^0.7") (f (quote ("force-soft"))) (d #t) (k 2)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0sryvx82kjjwfwgrnfw8zfa8qp62p73w4ifpvrhyfl4jjjjlgnd6")))

(define-public crate-ofb-0.5.1 (c (n "ofb") (v "0.5.1") (d (list (d (n "aes") (r "^0.7") (f (quote ("force-soft"))) (d #t) (k 2)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "09l7dyhvlx2j0hhas46f98ijajxgn4dmvkndbs6c7qq44k0a7lim")))

(define-public crate-ofb-0.6.0 (c (n "ofb") (v "0.6.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1xdz7n70i29cn2zifwawzf260fdirdk4xwchqiz25c0xk9i83p59") (f (quote (("zeroize" "cipher/zeroize") ("block-padding" "cipher/block-padding")))) (r "1.56")))

(define-public crate-ofb-0.6.1 (c (n "ofb") (v "0.6.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1833d5dns6r6jb9aw29wcwgw6cxijkww19kfcsqlxzs5w1w0di1c") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

