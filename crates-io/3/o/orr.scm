(define-module (crates-io #{3}# o orr) #:use-module (crates-io))

(define-public crate-orr-0.0.1 (c (n "orr") (v "0.0.1") (h "02z9kcyhbjj3w5b9clxjaqy78qkx5b9r9fr0h66fdpizy04c2a9b") (y #t)))

(define-public crate-orr-0.0.5 (c (n "orr") (v "0.0.5") (h "1mygij00dq8yqzkrc6f1sl232rgrix8l8q0vrgr4whjjjpw9dcy0") (y #t)))

(define-public crate-orr-0.0.7 (c (n "orr") (v "0.0.7") (h "1kdbcajwxnvzf0r7yw9vynsnkmdf7kcim1v6ic23hlsisxj23gqx") (y #t)))

(define-public crate-orr-0.1.0 (c (n "orr") (v "0.1.0") (h "1l72zzapprilsz8mf4qgjfbagfzdwz4amcjpphvd4x2dg2l9aj7i") (y #t)))

(define-public crate-orr-0.1.1 (c (n "orr") (v "0.1.1") (h "0l6lh4qjyibrj03k0l8yq0iqz553l6c8s2z5wfd33d52iccmg435") (y #t)))

(define-public crate-orr-0.1.3 (c (n "orr") (v "0.1.3") (h "1jyf4bnrnxnjclhv57kl9zpsnm2vpkrz2hghjj141g48c1fdvyis") (y #t)))

(define-public crate-orr-0.1.4 (c (n "orr") (v "0.1.4") (h "00bl7ba3x4yby2k1wax0mgsjgmckpqzaf1x1ci5l2d0x2pl9slqh") (y #t)))

(define-public crate-orr-0.1.77 (c (n "orr") (v "0.1.77") (h "1swdf2z5l1yf3ybpl7lq3949rfhdinnsgb110vwzwwiaaw8m319n") (y #t)))

(define-public crate-orr-0.77.0 (c (n "orr") (v "0.77.0") (h "186p5gzfjlnyba2khza9frj05n8py2i05lw4wg5pjmxhljk71n0p") (y #t)))

(define-public crate-orr-7.7.18 (c (n "orr") (v "7.7.18") (h "162vnh9g7rrmdqkm76q1p7kph87avhmx3z27xgv1cqdigh4s2bx6") (y #t)))

(define-public crate-orr-2018.7.7 (c (n "orr") (v "2018.7.7") (h "07nxakz22ydhqrkhg6kzccrcq2k6w4wk3ywmr93w5is7rqh3bcfb") (y #t)))

(define-public crate-orr-2019.12.13 (c (n "orr") (v "2019.12.13") (h "1v346xmim9wdv4438h62mndh3nszmxzggsxky1kzadqkaimpfs4r") (y #t)))

(define-public crate-orr-9999.999.99 (c (n "orr") (v "9999.999.99") (h "0nhcq3133hrzc9bf3cxl2fpjxpsifzhim7z0yw47n6gnkxwj60qp") (y #t)))

(define-public crate-orr-9.9.9 (c (n "orr") (v "9.9.9") (h "066d7w7vxj7mxsca109gxwyzif7pj63bld3gf2vngq50hcdvi3pl") (y #t)))

(define-public crate-orr-99999.99999.99999 (c (n "orr") (v "99999.99999.99999") (h "0ipan3qblgsm647y2qmgpij1bmx5sr662jdipcdj9ypn9q84q7dg") (y #t)))

(define-public crate-orr-9999999.9999999.9999999 (c (n "orr") (v "9999999.9999999.9999999") (h "1li0n75f074c8h2w1bzgf7zlya9q4616b5vlb0vzsp2kyk8y55d2") (y #t)))

(define-public crate-orr-999999999.999999999.999999999 (c (n "orr") (v "999999999.999999999.999999999") (h "1lx0yz5grmjaisa81r1nw7hspqyjy977sa3sy3gp76y5n6q6ln7c")))

