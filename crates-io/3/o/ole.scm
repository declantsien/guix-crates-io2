(define-module (crates-io #{3}# o ole) #:use-module (crates-io))

(define-public crate-ole-0.1.0 (c (n "ole") (v "0.1.0") (h "00jqdq36fyhc2ck3vxracacdjfmvdq2vwmq14mlwxy0mn7r1dhq6")))

(define-public crate-ole-0.1.1 (c (n "ole") (v "0.1.1") (h "1x1zbkn9c112kr6psf4nnkl2g9fqm4rfh4h88wj3kp2043qwzxfb")))

(define-public crate-ole-0.1.2 (c (n "ole") (v "0.1.2") (h "0gnnva2i59cnz315x4iwmvq83xbqxd1rawmq9q7nk379dxszcvb6")))

(define-public crate-ole-0.1.3 (c (n "ole") (v "0.1.3") (h "0kadgb5ldd03zks4n0m55009aapc6i859z9sgl76qryahgahafnd")))

(define-public crate-ole-0.1.4 (c (n "ole") (v "0.1.4") (h "17li9r9lhrhspixfvbg917kyw3sxl70jki32m4wa609hq7lgqgw0")))

(define-public crate-ole-0.1.5 (c (n "ole") (v "0.1.5") (h "1v1v4a43ilwv6hqh8w4h7gqx80ak2iqzhn5rs4liini91bpjs52f")))

(define-public crate-ole-0.1.6 (c (n "ole") (v "0.1.6") (h "11iggr8nxshy7x42rl4jv8l0gcr7fwr01i7zv98bxar1p115q7ll")))

(define-public crate-ole-0.1.7 (c (n "ole") (v "0.1.7") (h "05s45al5d54ggn18czhvars92chfn440ymiah62wrf6dcpmd4a6b")))

(define-public crate-ole-0.1.8 (c (n "ole") (v "0.1.8") (h "1y89q2cya15y4kv6wcym9pq4kgrhvw2h3snbaajw2zd684z18j2z")))

(define-public crate-ole-0.1.9 (c (n "ole") (v "0.1.9") (h "0q4h24b89j3rdx16395g401xrma3znppdqpaawcfvwx4cx1hrrzk")))

(define-public crate-ole-0.1.10 (c (n "ole") (v "0.1.10") (h "04hpi43pmbzkfn4q609jdq384gwy53vrj71ccc27sc505gap6c3i")))

(define-public crate-ole-0.1.11 (c (n "ole") (v "0.1.11") (h "0pnmzrdp8vcdj8qsxd6yr77kybkc8immwgy1gvv250jdszaaa3ag")))

(define-public crate-ole-0.1.12 (c (n "ole") (v "0.1.12") (h "12krldz51gxk2g86pa6x3xhidqz7kp14z6v35w75y75gvybkcfy1")))

(define-public crate-ole-0.1.13 (c (n "ole") (v "0.1.13") (h "02czag9awgbql1dnqd9z5mggzrh19bps54saw3bhy11s8fcpckml")))

(define-public crate-ole-0.1.14 (c (n "ole") (v "0.1.14") (h "10yqakw763qf5c8ybvvlg7q7y553l04dzgb8a51nhkwc9zj4bpz7")))

(define-public crate-ole-0.1.15 (c (n "ole") (v "0.1.15") (h "0riszcg2qlvi4qimxya7s37nblr77xv54b9kzmxazzb2xdpds2jg")))

