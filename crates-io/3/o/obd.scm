(define-module (crates-io #{3}# o obd) #:use-module (crates-io))

(define-public crate-obd-0.1.0 (c (n "obd") (v "0.1.0") (h "148lp1vrj34jwb1y6z3mwjxqsnqsp73j5xxnbm0011kvg1ip29f5")))

(define-public crate-obd-0.1.1 (c (n "obd") (v "0.1.1") (d (list (d (n "j2534") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "socketcan") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "05mvpi4x30alms9zhp47qpyrk24jnyq0n0jkyk55d5qdfvrmd2n5") (f (quote (("passthru" "j2534") ("default" "passthru"))))))

(define-public crate-obd-0.1.2 (c (n "obd") (v "0.1.2") (d (list (d (n "j2534") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "socketcan") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "1gz5rm28xlwrwi1yf5dd8prwyc2jbngp2cmmxsrkra6dadljimba") (f (quote (("passthru" "j2534") ("default" "passthru"))))))

(define-public crate-obd-0.1.3 (c (n "obd") (v "0.1.3") (d (list (d (n "j2534") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "socketcan") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "03aaq4ljw89gxw9xvyb2bx3vlvishr8d9mcp8ga3hnfpk9bsr58s") (f (quote (("passthru" "j2534") ("default" "passthru"))))))

