(define-module (crates-io #{3}# o oid) #:use-module (crates-io))

(define-public crate-oid-0.1.0 (c (n "oid") (v "0.1.0") (h "0rzqbm9v4ppscgc3cwcmpy9jkrdwkfdgw7ivmf57m98f9cd46hxy") (f (quote (("u32") ("std") ("default" "std"))))))

(define-public crate-oid-0.1.1 (c (n "oid") (v "0.1.1") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0mbhxry1alvi34vdl4c2vx26faqzimlg710wp9lyly40i4c5yg99") (f (quote (("u32") ("std") ("serde_support" "serde") ("default" "std"))))))

(define-public crate-oid-0.2.0 (c (n "oid") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.115") (o #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 2)))) (h "0x5mv20zdklind2aln14d05qln0k8ymbm5ig7wbkynl0hsv6bc2x") (f (quote (("u32") ("std") ("serde_support" "serde") ("default" "std"))))))

(define-public crate-oid-0.2.1 (c (n "oid") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.116") (o #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.116") (d #t) (k 2)))) (h "1hh61lx2kr0ca2rvkhf5j94asxxvb6pfwfxm06hdn4w8b4y906cw") (f (quote (("u32") ("std") ("serde_support" "serde") ("default" "std"))))))

