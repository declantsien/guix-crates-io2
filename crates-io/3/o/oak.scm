(define-module (crates-io #{3}# o oak) #:use-module (crates-io))

(define-public crate-oak-0.1.0 (c (n "oak") (v "0.1.0") (d (list (d (n "oak_runtime") (r "^0.1.0") (d #t) (k 0)))) (h "16cmlqh7kk6s8dd6b2pxvpj1xndq6fpp6nd6mnv0mrvrxsdz4mw1")))

(define-public crate-oak-0.1.1 (c (n "oak") (v "0.1.1") (d (list (d (n "oak_runtime") (r "^0.1.0") (d #t) (k 2)))) (h "0ll7y1qd8ks00j5qnhv7pwabbw2cbbkghr4qha461cy1sbrk7a72")))

(define-public crate-oak-0.1.2 (c (n "oak") (v "0.1.2") (d (list (d (n "oak_runtime") (r "^0.1.1") (d #t) (k 2)))) (h "1ijzbhcn4chxljgai5dqyh866fvg6iqlpjh2b2n02pjnskk5h1bq")))

(define-public crate-oak-0.2.0 (c (n "oak") (v "0.2.0") (d (list (d (n "oak_runtime") (r "^0.2.0") (d #t) (k 2)))) (h "01kpj7l6k32m0rmzwl60x5ghqgydi6rc5x5ag74mwb6yz76xgzb5")))

(define-public crate-oak-0.3.0 (c (n "oak") (v "0.3.0") (d (list (d (n "oak_runtime") (r "^0.3.0") (d #t) (k 2)))) (h "0wrxr9imw31wqkmx8dm05sn53qriz0av2ni3k5k1cvvssh6h9393")))

(define-public crate-oak-0.3.1 (c (n "oak") (v "0.3.1") (d (list (d (n "oak_runtime") (r "^0.3.1") (d #t) (k 2)))) (h "097cx56n60czi4wd8kb3h7s5jnmncm8g6i15i51jcnln906cx4i1")))

(define-public crate-oak-0.3.2 (c (n "oak") (v "0.3.2") (d (list (d (n "oak_runtime") (r "^0.3.1") (d #t) (k 2)))) (h "1xpngxyyw2spx787hm6y8pm2b8rg5jzch9138zghbh84vl52knlk")))

(define-public crate-oak-0.3.3 (c (n "oak") (v "0.3.3") (d (list (d (n "oak_runtime") (r "^0.3.3") (d #t) (k 2)))) (h "0vwmg2fc6cn4sd374ri0w8y6s7873n1na86wx3vgvjp8x70z8bcc")))

(define-public crate-oak-0.3.4 (c (n "oak") (v "0.3.4") (d (list (d (n "oak_runtime") (r "^0.3.4") (d #t) (k 2)))) (h "1348wa13zxyi4md8hizqcp8hl2vpc3h84j95skkishd9xrg6wp3q")))

(define-public crate-oak-0.3.5 (c (n "oak") (v "0.3.5") (d (list (d (n "oak_runtime") (r "^0.3.5") (d #t) (k 2)))) (h "0grbdr0srfnbb6a3s5jv7k1cjpiwnrb88c9wbln9jmp2v6cxy462")))

(define-public crate-oak-0.3.6 (c (n "oak") (v "0.3.6") (d (list (d (n "oak_runtime") (r "^0.3.6") (d #t) (k 2)))) (h "07vf54dsni588fh180q4mnwb0k7hi2n4gm7qachqycd802j4rbhj")))

(define-public crate-oak-0.3.7 (c (n "oak") (v "0.3.7") (d (list (d (n "oak_runtime") (r "^0.3.6") (d #t) (k 2)))) (h "08abj75s5z0mq029wj3fg07a1mqzirqb2wnnddaydb8a84v318si")))

(define-public crate-oak-0.3.8 (c (n "oak") (v "0.3.8") (d (list (d (n "oak_runtime") (r "^0.3.6") (d #t) (k 2)))) (h "1llq0h5d1jz4f7ylfnszf66bp28484h37jzpwqwj921xk2g1766p")))

(define-public crate-oak-0.3.9 (c (n "oak") (v "0.3.9") (d (list (d (n "oak_runtime") (r "^0.3.6") (d #t) (k 2)))) (h "08gs3nsq53lmazjh5z0fw433qqfc431ya41ph0pls21rh27i9xb4")))

(define-public crate-oak-0.3.10 (c (n "oak") (v "0.3.10") (d (list (d (n "oak_runtime") (r "^0.3.6") (d #t) (k 2)))) (h "0lskgfd1q1nc44n3sh3hh21fgw1xkmcfwfd5b3y72zg37g0pmsbr")))

(define-public crate-oak-0.3.11 (c (n "oak") (v "0.3.11") (d (list (d (n "oak_runtime") (r "^0.3.6") (d #t) (k 2)))) (h "1lsrka6i487n3p7rlw0dmszzr8xmxyb6ylkqh68zfgx073vvxcb9")))

(define-public crate-oak-0.3.12 (c (n "oak") (v "0.3.12") (d (list (d (n "oak_runtime") (r "^0.3.7") (d #t) (k 2)))) (h "1w3nrd2an91hjnrjdkl7wg5wvzz7wwm8zsvi34w009kzq9lwgrl3")))

(define-public crate-oak-0.3.13 (c (n "oak") (v "0.3.13") (d (list (d (n "oak_runtime") (r "^0.3.7") (d #t) (k 2)))) (h "0mqh82n2ybhg29nj8aa663gfs1vawnpb4mymvcjby1n1xn1vwsk2")))

(define-public crate-oak-0.3.14 (c (n "oak") (v "0.3.14") (d (list (d (n "oak_runtime") (r "^0.3.7") (d #t) (k 2)))) (h "1ajwx3gcb6xn7fmgaqyxfixi195fc4idjnrwvqk9gwgnzg8rm6pi")))

(define-public crate-oak-0.3.15 (c (n "oak") (v "0.3.15") (d (list (d (n "oak_runtime") (r "^0.3.7") (d #t) (k 2)))) (h "1hz1wq6xyj4rs1w37b1ii3fc7z0nhm0jlp06gc58vdvnz17qhx8w")))

(define-public crate-oak-0.3.16 (c (n "oak") (v "0.3.16") (d (list (d (n "oak_runtime") (r "^0.3.7") (d #t) (k 2)))) (h "155gxfr6bawx4826vf9ysysljm4wpxbkkds9kb16l6aiv4vkq2cl")))

(define-public crate-oak-0.3.17 (c (n "oak") (v "0.3.17") (d (list (d (n "oak_runtime") (r "^0.3.7") (d #t) (k 2)))) (h "0xdanxm6k3fzcnc9cdfnjfrxbh5wyagkl27qnq19hciz9bdscr1q")))

(define-public crate-oak-0.4.0 (c (n "oak") (v "0.4.0") (d (list (d (n "oak_runtime") (r "^0.4.0") (d #t) (k 2)))) (h "0rab54b6axamlpl3c01mnr8b2d86gq822jdqk86nadxvc68j18hi")))

(define-public crate-oak-0.4.1 (c (n "oak") (v "0.4.1") (d (list (d (n "oak_runtime") (r "^0.4.0") (d #t) (k 2)))) (h "09bjcjxfyihxqbi4rqjfr19ysvx7jbpg9w0931x5j0gzb02mv7cy")))

(define-public crate-oak-0.4.2 (c (n "oak") (v "0.4.2") (d (list (d (n "oak_runtime") (r "^0.4.0") (d #t) (k 2)) (d (n "partial") (r "^0.1.0") (d #t) (k 0)))) (h "1pj9mwja5698pkl89b5xvbkybkw2cswzavlyd8jvh6gj76xdwx5p")))

(define-public crate-oak-0.4.3 (c (n "oak") (v "0.4.3") (d (list (d (n "oak_runtime") (r "^0.4.0") (d #t) (k 2)) (d (n "partial") (r "^0.1.0") (d #t) (k 0)))) (h "19pds170yn7k1yhxcsl1ba0bysvxrsa6lm10swxj78spn9an0cxf")))

(define-public crate-oak-0.4.4 (c (n "oak") (v "0.4.4") (d (list (d (n "oak_runtime") (r "^0.4.1") (d #t) (k 2)) (d (n "partial") (r "^0.1.0") (d #t) (k 0)))) (h "1q8kc76qlz3mx28k8xdd3z7q8g3ibm4q5y6za12vjhn5wdcqlxbz")))

(define-public crate-oak-0.4.5 (c (n "oak") (v "0.4.5") (d (list (d (n "oak_runtime") (r "^0.4.1") (d #t) (k 2)) (d (n "partial") (r "^0.1.0") (d #t) (k 0)))) (h "00l7bnlrs8mai4rlnlnp6006cpl8kkmk74cx7ccb6z8cx4kiqp28")))

(define-public crate-oak-0.4.6 (c (n "oak") (v "0.4.6") (d (list (d (n "oak_runtime") (r "^0.4.2") (d #t) (k 2)) (d (n "partial") (r "^0.1.0") (d #t) (k 0)))) (h "08cw8vmb3fvjzyin7sd9il0n2jq8ybawmbqfnrv8iljk4730mwrf")))

(define-public crate-oak-0.4.7 (c (n "oak") (v "0.4.7") (d (list (d (n "oak_runtime") (r "^0.4.2") (d #t) (k 2)) (d (n "partial") (r "^0.1.0") (d #t) (k 0)))) (h "0nynzhrr2dsbqngz4ydjv8nahsmkgc87arvg76zz2lgczbkg3w20")))

(define-public crate-oak-0.4.8 (c (n "oak") (v "0.4.8") (d (list (d (n "oak_runtime") (r "^0.4.3") (d #t) (k 2)) (d (n "partial") (r "^0.1.0") (d #t) (k 0)))) (h "0p8ss7l2lz7pq64cq1gj4nds52cfjy92zr4ybx2szyf9qqfsn2mp")))

(define-public crate-oak-0.4.9 (c (n "oak") (v "0.4.9") (d (list (d (n "oak_runtime") (r "^0.4.3") (d #t) (k 2)) (d (n "partial") (r "^0.1.0") (d #t) (k 0)))) (h "03pf1q1zl7dnasrwhlp8nn7c0v6gkkggm49d430ibkcfiw7bcng2")))

(define-public crate-oak-0.5.0 (c (n "oak") (v "0.5.0") (d (list (d (n "oak_runtime") (r "^0.5.0") (d #t) (k 2)) (d (n "partial") (r "^0.1.3") (d #t) (k 0)))) (h "124wscws9cgjx487xn2kc1asnzi6qvgq51r8zbv3fsnflsf1j1j6")))

(define-public crate-oak-0.5.1 (c (n "oak") (v "0.5.1") (d (list (d (n "oak_runtime") (r "^0.5.0") (d #t) (k 2)) (d (n "partial") (r "^0.1.3") (d #t) (k 0)))) (h "1kfpbn4daca5gck9l0bn0as451283s8j5mj42qhn3xq3hbj6gmvk")))

(define-public crate-oak-0.5.2 (c (n "oak") (v "0.5.2") (d (list (d (n "oak_runtime") (r "^0.5.5") (d #t) (k 2)) (d (n "partial") (r "^0.1.3") (d #t) (k 0)))) (h "0f788f8slvmkfzpdv3s9n9dahxvwfj4hwb1iqj07hpylaj06b4vq")))

(define-public crate-oak-0.5.3 (c (n "oak") (v "0.5.3") (d (list (d (n "oak_runtime") (r "^0.5.5") (d #t) (k 2)) (d (n "partial") (r "^0.1.3") (d #t) (k 0)))) (h "1k0zs83dmg46xwp9fdna2bsli292ifaflz0yc3zs35h2n6xb5hmq")))

(define-public crate-oak-0.5.4 (c (n "oak") (v "0.5.4") (d (list (d (n "oak_runtime") (r "^0.5.5") (d #t) (k 2)) (d (n "partial") (r "^0.2.3") (d #t) (k 0)))) (h "1g83vhr24i0hg7x5l6xzaisx5qwwx28rg4sls8w4ildb89905zmv")))

(define-public crate-oak-0.5.5 (c (n "oak") (v "0.5.5") (d (list (d (n "oak_runtime") (r "^0.5.5") (d #t) (k 2)) (d (n "partial") (r "^0.2.3") (d #t) (k 0)))) (h "188k3w4qgn536h5igxx7m8pla467ks0ri2bday7wycy17rgw6jli")))

(define-public crate-oak-0.5.6 (c (n "oak") (v "0.5.6") (d (list (d (n "oak_runtime") (r "^0.5.5") (d #t) (k 2)) (d (n "partial") (r "^0.2.3") (d #t) (k 0)))) (h "0xmrjqyb0478xi8rpyxsfxv1m7w40ljjrm1fhsarabp3p4vyrdk9")))

(define-public crate-oak-0.5.7 (c (n "oak") (v "0.5.7") (d (list (d (n "oak_runtime") (r "^0.5.5") (d #t) (k 2)) (d (n "partial") (r "^0.2.3") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 2)))) (h "0dwylznr9y65x5r9n0jsq6dkq6c98h43w6nmyr5y4pyinh2p7vqv")))

(define-public crate-oak-0.5.8 (c (n "oak") (v "0.5.8") (d (list (d (n "oak_runtime") (r "^0.5.5") (d #t) (k 2)) (d (n "partial") (r "^0.2.3") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 2)))) (h "04w882ax48q6sgwvwkz6513qslfsqs8azgpfg4hbfz62w0zxnghb")))

(define-public crate-oak-0.5.9 (c (n "oak") (v "0.5.9") (d (list (d (n "oak_runtime") (r "^0.5.5") (d #t) (k 2)) (d (n "partial") (r "^0.2.3") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 2)))) (h "12f96gilinj9lx5g1qhkr41r0nc0fkmi4dc9aq6v1mab3ny2gyjr")))

(define-public crate-oak-0.6.0 (c (n "oak") (v "0.6.0") (d (list (d (n "oak_runtime") (r "^0.5.5") (d #t) (k 2)) (d (n "partial") (r "^0.2.3") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 2)))) (h "1ir50nwpznvgsxfcq3jaznamrzz06mzdj64m1mvb3c7ch78pn48r")))

(define-public crate-oak-0.7.0 (c (n "oak") (v "0.7.0") (d (list (d (n "oak_runtime") (r "^0.5.5") (d #t) (k 2)) (d (n "partial") (r "^0.2.3") (d #t) (k 0)) (d (n "term") (r "^0.5.1") (d #t) (k 2)))) (h "0j5gkd3fyzhzavq9z7g6kji8lrijq3snqafvfhaglbkppdbwy9l8")))

(define-public crate-oak-0.8.0 (c (n "oak") (v "0.8.0") (d (list (d (n "oak_runtime") (r "^0.6.0") (d #t) (k 2)) (d (n "partial") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 2)))) (h "0268k43qz0ncsvrimmyc7wdqgah5dz3hskhs31kmpi6c3b31ai4q")))

