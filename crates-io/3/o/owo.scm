(define-module (crates-io #{3}# o owo) #:use-module (crates-io))

(define-public crate-owo-0.1.0 (c (n "owo") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.6") (d #t) (k 0)))) (h "0p837lrvq8zhmpim9ms4bb8cxh60i03hbw60n64l2lxlxk367r8l")))

(define-public crate-owo-0.0.1-alpha (c (n "owo") (v "0.0.1-alpha") (d (list (d (n "curl") (r "^0.4.6") (d #t) (k 0)))) (h "13r3j28bd79rl17giaw1mwi9q2026jphf4afif2ds39cq7zklir8")))

(define-public crate-owo-0.1.1 (c (n "owo") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.6") (d #t) (k 0)))) (h "050i34rfh677s4si8yb6n374n16yih1s1gr4gp2j0da5jz7x1fig")))

(define-public crate-owo-0.2.0 (c (n "owo") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "infer") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "multipart" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "05m0lrh7ifbr70m1jj4nxvmihm5qw0abry1wb2wfg9bbr2j9cnlw")))

(define-public crate-owo-0.3.0 (c (n "owo") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "infer") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "multipart" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "16bxq8mxnxchbp35yiznggb8s37lgia6gmkcayi2aq0gq65g8gh3")))

(define-public crate-owo-0.3.1 (c (n "owo") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "infer") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "multipart" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)))) (h "14563zb0yas376mdxiga32xzlyj2n505gjcphm9jpxxwb1xfh808")))

(define-public crate-owo-0.4.0 (c (n "owo") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "infer") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking" "multipart" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1wfsa9km7p4hr6sd9k9zlbydz0py3hazyj54nhqrp6h2w23342gq")))

