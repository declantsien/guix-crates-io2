(define-module (crates-io #{3}# o ovh) #:use-module (crates-io))

(define-public crate-ovh-0.1.0 (c (n "ovh") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 2)) (d (n "configparser") (r "^2.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.10") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zkv788cy5f2bgkalz97l6rqynpkxxl3xf7vsb7qccgk1ba5v1yz")))

