(define-module (crates-io #{3}# o oin) #:use-module (crates-io))

(define-public crate-oin-0.1.0 (c (n "oin") (v "0.1.0") (d (list (d (n "ed25519-dalek") (r "^1.0.0-pre.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)))) (h "0iq2vlb2n1cpmrkm6qybjcpwxfabgy774gd88k1vz3hr8srqv0pl")))

