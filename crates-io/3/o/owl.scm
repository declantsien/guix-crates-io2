(define-module (crates-io #{3}# o owl) #:use-module (crates-io))

(define-public crate-owl-0.0.1 (c (n "owl") (v "0.0.1") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "falcon_capstone") (r "^0.2.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.14") (d #t) (k 0)))) (h "11ra4d686yq84hnyh59yfmh11320pnfkc52xfbpmx84866smw2wl") (y #t)))

(define-public crate-owl-0.0.2 (c (n "owl") (v "0.0.2") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "falcon_capstone") (r "^0.2.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.14") (d #t) (k 0)))) (h "0gwm1scxyrq4b3ih1y4r598ivnzfxpdz6pdhg1z3j7cks814rs5s")))

(define-public crate-owl-0.0.3 (c (n "owl") (v "0.0.3") (d (list (d (n "clap") (r "^2.29.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "falcon_capstone") (r "^0.2.1") (d #t) (k 0)) (d (n "goblin") (r "^0.0.14") (d #t) (k 0)) (d (n "rayon") (r "^0.9") (d #t) (k 0)))) (h "0vx765096p5m4b9q0y6x3451pd8mly6d2az1k37xsfwwznxj5izh")))

