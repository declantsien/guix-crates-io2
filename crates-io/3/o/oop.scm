(define-module (crates-io #{3}# o oop) #:use-module (crates-io))

(define-public crate-oop-0.0.0-alpha.0 (c (n "oop") (v "0.0.0-alpha.0") (h "1hp6nlyzzfilmmmjic4c9drj7hv84pmgi6jqlp7shhy8sg4752qb")))

(define-public crate-oop-0.0.1 (c (n "oop") (v "0.0.1") (d (list (d (n "oop-macro") (r "^0.0.1") (d #t) (k 0)))) (h "1grv1hh00n9gw3wbkycjg9dlld2rxjy316gir1yn0pzxk4xf6626")))

(define-public crate-oop-0.0.2 (c (n "oop") (v "0.0.2") (d (list (d (n "oop-macro") (r "^0.0.2") (d #t) (k 0)))) (h "0829ivyzhz0bw7crqkf478yfm7zps7pnjg9wzkxibwwcysx18c3s")))

