(define-module (crates-io #{3}# o orf) #:use-module (crates-io))

(define-public crate-orf-0.1.0 (c (n "orf") (v "0.1.0") (d (list (d (n "memmap") (r "^0.2.3") (d #t) (k 0)) (d (n "monster") (r "^0.1.21") (d #t) (k 0)) (d (n "nom") (r "^1.2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2.11") (d #t) (k 0)) (d (n "phf") (r "^0.7.12") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.12") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)))) (h "0a5akrcdzwz6fhj10hbys4mqf7mqw5yk271qhnjad2328mrar3z1")))

