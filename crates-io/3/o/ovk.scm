(define-module (crates-io #{3}# o ovk) #:use-module (crates-io))

(define-public crate-ovk-0.1.0 (c (n "ovk") (v "0.1.0") (h "0cqi2p1dai7mml5dm6xyn2lgnpbnkapg22k3bx0yf2p59vhcd7sh") (y #t)))

(define-public crate-ovk-0.2.0 (c (n "ovk") (v "0.2.0") (h "16llcl283pyd0njh9hfwpx16lfcj0ir34h48hjppnzf2gnihmz3c") (y #t)))

(define-public crate-ovk-0.2.1 (c (n "ovk") (v "0.2.1") (h "1ckn2fp8z6vsapns0y80433vmkc9jjsb6fqdrg4jyvrcgh5lsfay") (y #t)))

