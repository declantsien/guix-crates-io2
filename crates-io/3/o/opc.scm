(define-module (crates-io #{3}# o opc) #:use-module (crates-io))

(define-public crate-opc-0.1.0 (c (n "opc") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "057yakwfrblcxxmpv32qr51ly27ig1nd8cqv1pyg5h190yki91ca")))

(define-public crate-opc-0.1.1 (c (n "opc") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)))) (h "19cw864s9m4v4j9fjr4qcx823f77s2ms7m27xkwba9kdkhdkp9ws")))

(define-public crate-opc-0.2.0 (c (n "opc") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.4") (d #t) (k 0)))) (h "1ygpway4iv12cqi80g71njx7vwqxbircy50xhm2zni1ldsda3m8b")))

(define-public crate-opc-0.3.0 (c (n "opc") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 2)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.8") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1.2") (d #t) (k 0)))) (h "0hxaqpi23kwb6vazid6cni41b66ymc57sffgj7q4f7pcd3xgzrjy")))

