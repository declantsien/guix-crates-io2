(define-module (crates-io #{3}# o o2o) #:use-module (crates-io))

(define-public crate-o2o-0.1.0 (c (n "o2o") (v "0.1.0") (d (list (d (n "o2o-impl") (r "^0.1.0") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "022b8i5wsw8pimvnh5zprcd0ln2nfm8wfdmfda65s82vmf5cbv8w")))

(define-public crate-o2o-0.1.1 (c (n "o2o") (v "0.1.1") (d (list (d (n "o2o-impl") (r "^0.1.1") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0q90jby6hjzbgi4dxrslhnnrx4cld3lwbbz3jbckciph1ixfr39q")))

(define-public crate-o2o-0.2.0 (c (n "o2o") (v "0.2.0") (d (list (d (n "o2o-impl") (r "^0.2.0") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "15xbh8kfg8klb2bc098kjchdzb3dbccjh8yyw41qd5gkiclj451w")))

(define-public crate-o2o-0.2.1 (c (n "o2o") (v "0.2.1") (d (list (d (n "o2o-impl") (r "^0.2.1") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "02j2xs2hnzy07rjn6356hl86vn97nz81npkc4pidlsmvcgg8f1vi")))

(define-public crate-o2o-0.2.2 (c (n "o2o") (v "0.2.2") (d (list (d (n "o2o-impl") (r "^0.2.2") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "0az9xr64mr27hspdgpzwpa8h4k6qn1klyg2zk1h94zvw8gwc4hmw")))

(define-public crate-o2o-0.2.3 (c (n "o2o") (v "0.2.3") (d (list (d (n "o2o-impl") (r "^0.2.3") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)))) (h "1aq6g820yb1apkgqyrwxwqr5y1f1shcrvvqnq2a0vycj7nbz6mrm")))

(define-public crate-o2o-0.2.4 (c (n "o2o") (v "0.2.4") (d (list (d (n "o2o-impl") (r "^0.2.4") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.2.4") (d #t) (k 0)))) (h "1v7a2hj1ixf0jb9cppz3fq0p3jbikdxi7l2j3g8q3hl5r6hshz95")))

(define-public crate-o2o-0.2.5 (c (n "o2o") (v "0.2.5") (d (list (d (n "o2o-impl") (r "^0.2.5") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.2.5") (d #t) (k 0)))) (h "0x2lcfizjhvj92dl7c0gmvx18dlz3aign0n0rkg32szb5gs45jcn")))

(define-public crate-o2o-0.3.0 (c (n "o2o") (v "0.3.0") (d (list (d (n "o2o-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.3.0") (d #t) (k 0)))) (h "04zpnxng37abf6n42a2r6kx9cp50dnwff6nfwlpxnagmygpl7y28")))

(define-public crate-o2o-0.3.1 (c (n "o2o") (v "0.3.1") (d (list (d (n "o2o-impl") (r "^0.3.0") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.3.0") (d #t) (k 0)))) (h "1p71yy75k0f72vqja2z2cn8w5isarslhgmk5mg8zfwl2iisb5jlz")))

(define-public crate-o2o-0.3.2 (c (n "o2o") (v "0.3.2") (d (list (d (n "o2o-impl") (r "^0.3.1") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.3.1") (d #t) (k 0)))) (h "07hcy9bmb9djxkim2f4hwzqfi71dhcja3lhmfpnp36pwywisbd32")))

(define-public crate-o2o-0.4.0 (c (n "o2o") (v "0.4.0") (d (list (d (n "o2o-impl") (r "^0.4.0") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.4.0") (d #t) (k 0)))) (h "1l7mqnpw0mp3nnz02j89y82pirzilg5x5piklp9asc9yvc2j08gg") (y #t)))

(define-public crate-o2o-0.4.1 (c (n "o2o") (v "0.4.1") (d (list (d (n "o2o-impl") (r "^0.4.1") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.4.1") (d #t) (k 0)))) (h "11ixf0zrs1vy3301p3d860922rihdv45hd7b1ycdv2am2gw0ga9x")))

(define-public crate-o2o-0.4.2 (c (n "o2o") (v "0.4.2") (d (list (d (n "o2o-impl") (r "^0.4.2") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.4.2") (d #t) (k 0)))) (h "0dngxgh5awknmn2q8pry593rc385qbf18sg3xs7zlng03an36f3m")))

(define-public crate-o2o-0.4.3 (c (n "o2o") (v "0.4.3") (d (list (d (n "o2o-impl") (r "^0.4.3") (d #t) (k 0)) (d (n "o2o-macros") (r "^0.4.3") (d #t) (k 0)))) (h "0x7rwxxmn18kn9kxp3rgjgyy022p8ilvyd6hi2an6qfl0s4ngzlj")))

