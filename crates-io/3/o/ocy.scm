(define-module (crates-io #{3}# o ocy) #:use-module (crates-io))

(define-public crate-ocy-0.1.0 (c (n "ocy") (v "0.1.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (d #t) (k 0)) (d (n "ocy-core") (r "^0.1.0") (d #t) (k 0)))) (h "0glmkmkprjqv3vqi1jlbsm9qwv0sxqvn8395akxwgqilgrfzmqms")))

(define-public crate-ocy-0.1.1 (c (n "ocy") (v "0.1.1") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.15") (d #t) (k 0)) (d (n "ocy-core") (r "^0.1.1") (d #t) (k 0)))) (h "04c9mqfcp0irz6lwk4xgmj4w39gpr44wyjwh9fm22k58lbml4zqs")))

(define-public crate-ocy-0.1.2 (c (n "ocy") (v "0.1.2") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "ocy-core") (r "^0.1.2") (d #t) (k 0)))) (h "14vyqila8sdg5b2w0069n4ln4i9ja53lkiv4q5ijqaqaq3mc7h7h")))

(define-public crate-ocy-0.1.3 (c (n "ocy") (v "0.1.3") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "ocy-core") (r "^0.1.3") (d #t) (k 0)))) (h "0fmjy4w5i3wnpvk310fvypnndi4v7swjxp7kvf7fs1x4hnqkqrsd")))

(define-public crate-ocy-0.1.4 (c (n "ocy") (v "0.1.4") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "ocy-core") (r "^0.1.4") (d #t) (k 0)))) (h "0a57f09zrl47cm30h0inzinckc86qrw9zlfrywxaxyn4nmn8k486")))

(define-public crate-ocy-0.1.5 (c (n "ocy") (v "0.1.5") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.16") (d #t) (k 0)) (d (n "ocy-core") (r "^0.1.5") (d #t) (k 0)))) (h "1gpfjhdmggd5776yjiqdlqid8jnx5f6k9a0jrbh1v0678pj56rbs")))

(define-public crate-ocy-0.1.6 (c (n "ocy") (v "0.1.6") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "ocy-core") (r "^0.1.6") (d #t) (k 0)))) (h "0cjnfg962myvm4nzk2ahb7azf2blbl68wrl4jamlrk7h3c973392")))

(define-public crate-ocy-0.1.7 (c (n "ocy") (v "0.1.7") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "ocy-core") (r "^0.1.7") (d #t) (k 0)))) (h "0ymi0m271i012j3hirlnnmlmz4zra0hawfy7m86hlqc1jhjgmqhp")))

