(define-module (crates-io #{3}# o ots) #:use-module (crates-io))

(define-public crate-ots-0.1.0 (c (n "ots") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0wc45waibyj54nars6bhx1r00iwfisf1h14ws6m6nqgngyic1x7r")))

(define-public crate-ots-0.1.1 (c (n "ots") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "0dvxrs59iyhvag7c0adbqw72pwzzq5lnjjc5p31r9yg8c0jqriid")))

(define-public crate-ots-0.1.2 (c (n "ots") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "1s1yq93gjbvhrmsnnivyhmkrh8w7ms86pjxh1wryzfchjdzcs3gb")))

(define-public crate-ots-0.1.3 (c (n "ots") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "07ax7ki78i43s8nbf9nnifx7dgpb1pl5mw4hkm7z15sy479kgiaf")))

