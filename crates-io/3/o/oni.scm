(define-module (crates-io #{3}# o oni) #:use-module (crates-io))

(define-public crate-oni-0.1.0 (c (n "oni") (v "0.1.0") (h "0max5dsqdlrlv72557g3wcrcqsfwaj12phkc4fxnksjizfp869x0")))

(define-public crate-oni-0.1.1 (c (n "oni") (v "0.1.1") (d (list (d (n "oni_simulator") (r "^0.1.0") (d #t) (k 0)))) (h "040smr6li5ymchzlhmgx4nly5sr6jz520w80asmnx3cjfrk0almx")))

(define-public crate-oni-0.1.2 (c (n "oni") (v "0.1.2") (d (list (d (n "oni_simulator") (r "^0.1.0") (d #t) (k 0)))) (h "1gckqqnp71bccil3285xvv33mh4597j75i4qj75a0zkdwv78y4vr")))

