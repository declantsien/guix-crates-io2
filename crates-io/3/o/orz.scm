(define-module (crates-io #{3}# o orz) #:use-module (crates-io))

(define-public crate-orz-0.1.0 (c (n "orz") (v "0.1.0") (h "1nl1z116m64i0rcgs7licaf9y9fa0r2nb3blbvhf9hz40is1sbg6") (y #t)))

(define-public crate-orz-0.1.1 (c (n "orz") (v "0.1.1") (h "05pna3kk17j92dzk9gdibxqnan2gidy9xxx1lnqpbykp02k0iwlw") (y #t)))

(define-public crate-orz-0.1.2 (c (n "orz") (v "0.1.2") (d (list (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "visa-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)))) (h "0i303grcdf4bs88w7dld9wdfmz651fhv3m7h4hbji7bhx7xj4zis") (y #t)))

(define-public crate-orz-0.1.3 (c (n "orz") (v "0.1.3") (h "066d5ss4y1ipnqc29sh2iapc89nwhnm9dv28gws3spxagiwhdsz8") (y #t)))

(define-public crate-orz-0.1.4 (c (n "orz") (v "0.1.4") (d (list (d (n "eframe") (r "^0.21.3") (d #t) (k 0)))) (h "0s86z1hmdvcw2bzigjlmh9gis55plxmlgx9v5l8hm9mqz6c74xg3") (y #t)))

(define-public crate-orz-0.1.5 (c (n "orz") (v "0.1.5") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 0)))) (h "104bjmcaqmxwshccbxlw1s9kpc0ld3rs4xlaiwld1kkzcbpm08rd") (y #t)))

(define-public crate-orz-0.1.6 (c (n "orz") (v "0.1.6") (d (list (d (n "eframe") (r "^0.22.0") (d #t) (k 0)))) (h "0hankar3jwbihkxqpm5aqpyqrzbip4r886sx3vy9w5m39nxgv9wc")))

(define-public crate-orz-0.1.7 (c (n "orz") (v "0.1.7") (d (list (d (n "catppuccin-egui") (r "^3.1.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "16kalzd99ysy40j9ilgjaw0vdkpyfnqiqpf66wg03q07mdw61wax")))

(define-public crate-orz-0.24.1 (c (n "orz") (v "0.24.1") (d (list (d (n "egui") (r "^0.24.0") (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lxngp09zl12mkq93fdjr1s3rbjhjbv1bfxik8bsr5aa95ifgahf")))

