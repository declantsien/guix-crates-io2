(define-module (crates-io #{3}# o orm) #:use-module (crates-io))

(define-public crate-orm-0.1.3 (c (n "orm") (v "0.1.3") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "mysql") (r "^11.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "syntex") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_errors") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58.1") (d #t) (k 0)))) (h "1gxka0npl59vbisk342wyxw2zvk4l2fylgdn4j2f15qymhp80hck")))

(define-public crate-orm-0.1.4 (c (n "orm") (v "0.1.4") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "mysql") (r "^11.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "syntex") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_errors") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58.1") (d #t) (k 0)))) (h "1ynrr14azic5x43crpl6ywrl2113vm3jhyrrwazpnr7zfqi652a5")))

(define-public crate-orm-0.2.0 (c (n "orm") (v "0.2.0") (d (list (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "mysql") (r "^11.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "syntex") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_errors") (r "^0.58.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.58.1") (d #t) (k 0)))) (h "0x699nxb16gya32scsqx7kiqhw032fvd8d4b3fv3qpqlq1s74rax")))

