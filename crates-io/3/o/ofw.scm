(define-module (crates-io #{3}# o ofw) #:use-module (crates-io))

(define-public crate-ofw-0.1.0 (c (n "ofw") (v "0.1.0") (h "1mah6132n6f7k8aggqq5gbl9pncrp6gm16rrir4bb7a2hxpv0xkr")))

(define-public crate-ofw-0.1.1 (c (n "ofw") (v "0.1.1") (h "09qmv5q9jfcnf7snnv677dlf3v3j2mv0clxgw8cwhzd7k1fi8g37")))

(define-public crate-ofw-0.2.0 (c (n "ofw") (v "0.2.0") (h "05aja5592d9lhhnl7kbq05wj6zkc61khb4r4s010jff7wva5m51a")))

(define-public crate-ofw-0.2.1 (c (n "ofw") (v "0.2.1") (h "03wzin2wvsg4f5p95lv73kmgsb91jg1zmvpaswgbgvpbmz23pbi1")))

(define-public crate-ofw-0.3.0 (c (n "ofw") (v "0.3.0") (h "1q35151qqv0q3r63vc53377x606ba14hh93hwrsz46cyzszg1pc0")))

(define-public crate-ofw-0.3.1 (c (n "ofw") (v "0.3.1") (h "00k3hmqmbayn8mga56m4ypz7f9a4b5lcyrx527a0lxaqf6gc1c0r")))

