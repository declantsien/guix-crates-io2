(define-module (crates-io #{3}# o oss) #:use-module (crates-io))

(define-public crate-oss-0.1.0 (c (n "oss") (v "0.1.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rust_util") (r "^0.2.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "1lkr1bpzll4p35x4kznxdnb1zhjm8cbvcx7zrpi7v7z29bz35kk5")))

(define-public crate-oss-0.1.1 (c (n "oss") (v "0.1.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rust_util") (r "^0.2.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "1hm1l1sgbn64xsih05ravwywz75j7ywk2c914i9gw68wc4mccgb8")))

(define-public crate-oss-0.1.2 (c (n "oss") (v "0.1.2") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "json") (r "^0.11.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rust_util") (r "^0.2.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "00xsc2kd7zilvj3ih94k1bfk7nzlkl2jyzmq02jdpbyfdfn0hlyg")))

(define-public crate-oss-0.2.0 (c (n "oss") (v "0.2.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rust_util") (r "^0.2.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "0s426ggxdxdpgj2kzj5ch61z0cmpmwc8zs6jrfmivdr16nd1850p")))

(define-public crate-oss-0.3.0 (c (n "oss") (v "0.3.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rust_util") (r "^0.2.0") (d #t) (k 0)) (d (n "urlencoding") (r "^1.0.0") (d #t) (k 0)))) (h "0l9m88x3i6riyiylfjy0x4d3c61n1djfzbg273gy4d2iydj54h4j")))

