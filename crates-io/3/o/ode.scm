(define-module (crates-io #{3}# o ode) #:use-module (crates-io))

(define-public crate-ode-0.1.0 (c (n "ode") (v "0.1.0") (h "1znwrcvpd46jik3lfgl5hd5yn66s9yp1b5ifgh423pnfp07papfr")))

(define-public crate-ode-0.1.2 (c (n "ode") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.1.36") (d #t) (k 0)))) (h "1qdd6x174hnwl2mjp8nzhxvhr5i8mq5pvs16a398pgab1x78l9pi")))

