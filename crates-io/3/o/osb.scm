(define-module (crates-io #{3}# o osb) #:use-module (crates-io))

(define-public crate-osb-0.1.0 (c (n "osb") (v "0.1.0") (h "00im2mwhgral07bq8ickpc7wir8idr67lif23ch1bdwvqvvrchvp")))

(define-public crate-osb-0.2.0 (c (n "osb") (v "0.2.0") (h "0jzqmazifcdfn8si5g98la9xms7yrljgcq2prq2qfav834c74bwm")))

(define-public crate-osb-0.3.0 (c (n "osb") (v "0.3.0") (h "0pyi9db38xvkj6xphw3ili7xzjb5w8p1xaypyfaax0h36v8izdrd")))

