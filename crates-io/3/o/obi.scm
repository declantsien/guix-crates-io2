(define-module (crates-io #{3}# o obi) #:use-module (crates-io))

(define-public crate-obi-0.0.1 (c (n "obi") (v "0.0.1") (d (list (d (n "obi-derive") (r "^0.0.1") (d #t) (k 0)))) (h "1chk8wy377ff4pib32qh69n8fkzx731m2jxmxlkh1c45234wpdg8") (f (quote (("std") ("default" "std"))))))

(define-public crate-obi-0.0.2 (c (n "obi") (v "0.0.2") (d (list (d (n "obi-derive") (r "^0.0.2") (d #t) (k 0)))) (h "1i291m6g7ahjn891dvxc5j439ym1anrhiz06hxrc559y5wq67wgp") (f (quote (("std") ("default" "std"))))))

