(define-module (crates-io #{3}# f ff7) #:use-module (crates-io))

(define-public crate-ff7-0.2.0 (c (n "ff7") (v "0.2.0") (h "13s6q05wlx67af088xrx9l0yplvqp82s2mhmwkjp71vbyvj4dpq3")))

(define-public crate-ff7-0.2.1 (c (n "ff7") (v "0.2.1") (h "01bq21jy6y291949pdq1s4d156i12l26b01k3xnjrf98y6mq57bb")))

(define-public crate-ff7-0.2.2 (c (n "ff7") (v "0.2.2") (h "1zlh1viwgbc4mh8z789wfs8gibz9s28567bd7mi5bi84ccb4ymqy")))

