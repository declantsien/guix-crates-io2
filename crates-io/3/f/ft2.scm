(define-module (crates-io #{3}# f ft2) #:use-module (crates-io))

(define-public crate-ft2-0.1.0 (c (n "ft2") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (k 0)) (d (n "null-terminated") (r "^0.2.7") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1hjbh24iayqdbzcir87rfbwhl2pg2lfsk32lncpn33k96j4alnnc")))

(define-public crate-ft2-0.1.1 (c (n "ft2") (v "0.1.1") (d (list (d (n "flags") (r "^0.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (k 0)) (d (n "null-terminated") (r "^0.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "185f4jnyllr5i9gd086fs2dl9zzis83wsikmsw6wqw6yzi4ydncl")))

(define-public crate-ft2-0.1.2 (c (n "ft2") (v "0.1.2") (d (list (d (n "flags") (r "^0.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (k 0)) (d (n "null-terminated") (r "^0.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "1i9jvlswbl749x2s121dkrv6y6zlj8ip4kqx7rjq89s8pmkimn9z")))

(define-public crate-ft2-0.2.0 (c (n "ft2") (v "0.2.0") (d (list (d (n "fallible") (r "^0.1.2") (d #t) (k 0)) (d (n "flags") (r "^0.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (k 0)) (d (n "null-terminated") (r "^0.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "03cscv9xjjls1rh95a8hmxr7hlra99aw8k03bs31z1q4i92bmp0z")))

(define-public crate-ft2-0.2.1 (c (n "ft2") (v "0.2.1") (d (list (d (n "fallible") (r "^0.1.2") (d #t) (k 0)) (d (n "flags") (r "^0.1") (d #t) (k 0)) (d (n "freetype-sys") (r "^0.7.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.1") (k 0)) (d (n "null-terminated") (r "^0.3") (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.0") (d #t) (k 2)))) (h "0866pqjfyq8mvnp25x0kckipwh4n8cjf4wfmpglclkskzgsfl0ph")))

