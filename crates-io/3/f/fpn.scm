(define-module (crates-io #{3}# f fpn) #:use-module (crates-io))

(define-public crate-fpn-0.1.0 (c (n "fpn") (v "0.1.0") (d (list (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "06bny6nygb8kv29mhq37a90hm2d91qmzrd6pj1k8kvfqg4vslrwy")))

(define-public crate-fpn-0.1.1 (c (n "fpn") (v "0.1.1") (d (list (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "1pphadxp4g5k18qvflvn2ia8ql1ikhq2jhhij06vmdbifv4f3paj")))

(define-public crate-fpn-0.1.2 (c (n "fpn") (v "0.1.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "0yqlxxis5wrpzz3ldw9bzlwl1yy57ag9dpi0bf2l1x0q861cfal0")))

