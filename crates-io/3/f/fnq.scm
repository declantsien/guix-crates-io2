(define-module (crates-io #{3}# f fnq) #:use-module (crates-io))

(define-public crate-fnq-0.1.0 (c (n "fnq") (v "0.1.0") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)))) (h "0zsgc4wmllmp5mwg55m582kxhx10x4np3y9y5x8qbxq4d65brf5f")))

(define-public crate-fnq-0.2.0 (c (n "fnq") (v "0.2.0") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)))) (h "02jx4al9x8gg8ww8mgm9z5dabzzxk6dn406zfd1gvs3xpy3qx84l")))

(define-public crate-fnq-0.3.1 (c (n "fnq") (v "0.3.1") (d (list (d (n "nix") (r "^0.20.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.16") (d #t) (k 0)))) (h "199vicqqsjy1vbhvd30lzn0vj5hjq0fb0d13xx7bxvkahzmg0ih7")))

