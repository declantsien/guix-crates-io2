(define-module (crates-io #{3}# f fsm) #:use-module (crates-io))

(define-public crate-fsm-0.1.0 (c (n "fsm") (v "0.1.0") (h "0v24bgv9aaxpkyx7zccjc845n3dqg8mcnxkkwhkq8d190i4vsi0w")))

(define-public crate-fsm-0.1.1 (c (n "fsm") (v "0.1.1") (h "1cp7sc1jccd1wgwnk618iglh6sd8a5dlh5hpn04xqqzvn3izy1ra")))

(define-public crate-fsm-0.2.0 (c (n "fsm") (v "0.2.0") (h "04l4mvp79sn4pscqqa12dd7crxrp74j80imrhcgz2xskfifi9i3g")))

(define-public crate-fsm-0.2.1 (c (n "fsm") (v "0.2.1") (h "0na45rgxgl82snpb4arh9pxmv52chsq9ydq8lfbiymkqzv8ykfv7")))

(define-public crate-fsm-0.2.2 (c (n "fsm") (v "0.2.2") (h "0s7mf34iiraqqwx7xafvh2yiwbv0sq5048c5c0z2zyv233an8rh9")))

