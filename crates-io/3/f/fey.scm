(define-module (crates-io #{3}# f fey) #:use-module (crates-io))

(define-public crate-fey-0.0.1 (c (n "fey") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1cmgacpcvav8vyhgkkl12kiny2myiryvlsdz9058rpy00jpflngm") (y #t)))

(define-public crate-fey-0.0.2 (c (n "fey") (v "0.0.2") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0b409ms62bz4prk1hdpapg1161l5xf7rbjwi0hh5aczy1qz20sw7") (y #t)))

(define-public crate-fey-0.0.3 (c (n "fey") (v "0.0.3") (d (list (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03kanmblwlxwzz5bn392x3h2b7dkh72ck1afvgamb56pjxzygz3v") (y #t)))

