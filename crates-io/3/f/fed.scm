(define-module (crates-io #{3}# f fed) #:use-module (crates-io))

(define-public crate-fed-0.1.0 (c (n "fed") (v "0.1.0") (h "07smaxlybff33ynjir6cg22pgnylxhaydbibp61vl3ff25388z1n")))

(define-public crate-fed-0.2.0 (c (n "fed") (v "0.2.0") (h "0pmlzbc5ag0nv87jjqdic7alv2ssp4ma5jkn5y227hmin1w7g2gb")))

(define-public crate-fed-0.2.1 (c (n "fed") (v "0.2.1") (h "0dfcp9l5aif7fb0h2ckyhqdkj7xsfpqw811m8c5jpjnidjz0vy8i")))

(define-public crate-fed-0.2.2 (c (n "fed") (v "0.2.2") (h "1xmx4m5fwwm1h5yigk1i5c1wkdn8gmmialrzbycikvfc0xwz3d2q")))

(define-public crate-fed-0.3.0 (c (n "fed") (v "0.3.0") (h "1bksrhnj3xf40922nkh983il4mpp01x5g0fhqy0vca64za3kmgq7")))

(define-public crate-fed-0.4.0 (c (n "fed") (v "0.4.0") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 2)))) (h "108yy098hlmb674nd9jq72amkgx2v63zrgk76b3zsghwrb3xhrm1")))

(define-public crate-fed-0.4.1 (c (n "fed") (v "0.4.1") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 2)))) (h "19xjjhhkg67s3kyc5ddgzp01q2ih3f0hfzpddsfj6c52inggxnig")))

(define-public crate-fed-0.4.2 (c (n "fed") (v "0.4.2") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 2)))) (h "1xl43f1b2nflvc44wkylah922f6rh2s45i5vmgrkpklhjycfcgan")))

(define-public crate-fed-0.4.4 (c (n "fed") (v "0.4.4") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.11") (d #t) (k 2)))) (h "1fwkcg54wrn9p7wv3l57cphy7fswld0v1fdqwsqa7j2z3f3asv7c")))

