(define-module (crates-io #{3}# f fim) #:use-module (crates-io))

(define-public crate-fim-0.1.0 (c (n "fim") (v "0.1.0") (h "19yddgv8krxlv8scajvb6xnf5slp3rc8qq0604g73x0acayzc15p") (y #t)))

(define-public crate-fim-0.2.0 (c (n "fim") (v "0.2.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)))) (h "0hvq99ykjk88xvjb2527xxq1hlrcfv1w5w1pgz4if1v9nxgdgp2c") (y #t)))

(define-public crate-fim-0.3.0 (c (n "fim") (v "0.3.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)))) (h "0387myd9w8rsdqcm9c56x80vrs7r1sv181k3ddv168rpbwjc54g8") (y #t)))

(define-public crate-fim-0.4.0 (c (n "fim") (v "0.4.0") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)))) (h "0ykqjpvy06yh7p7z4kil4ii8v2b9z4dayiw732n2n1amw7q8321g") (y #t)))

(define-public crate-fim-0.4.1 (c (n "fim") (v "0.4.1") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1yx10s2ayazszp1l92x6kw03px2lwwd3rmcyb15jiia4g3na8jpm") (y #t)))

(define-public crate-fim-0.4.2 (c (n "fim") (v "0.4.2") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "08mf7p0zc7y6dlybzrafd1knvjxbnsprvcp1x5sb2wa1fbz8cy8a") (y #t)))

(define-public crate-fim-0.4.3 (c (n "fim") (v "0.4.3") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1yc36j4bwcx2nn2mw99sh8mbpw7c808iwzmzmn9izzfz76mwidg4") (y #t)))

(define-public crate-fim-0.4.4 (c (n "fim") (v "0.4.4") (d (list (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0943m3cajzlv1l182m77qw0p0g24j34lcf26yji9chh41kbyzdsw") (y #t)))

(define-public crate-fim-0.5.1 (c (n "fim") (v "0.5.1") (h "1vqj5nhwr0c8rkrvkvbgk5g8x63y0756g2vz8335sbi4dnkiws97") (y #t)))

