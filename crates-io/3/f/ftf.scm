(define-module (crates-io #{3}# f ftf) #:use-module (crates-io))

(define-public crate-ftf-0.2.0 (c (n "ftf") (v "0.2.0") (d (list (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "1gzngv8v9y69c91jzfjwl5z1lbzb4bvy862xlh4kird4lhwqs6zf")))

(define-public crate-ftf-0.3.0 (c (n "ftf") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "01n0gy3jy391ynm0c5c3bdyp54zx0pc0p49jihsk7aj1w8xn5lpd")))

(define-public crate-ftf-0.4.0 (c (n "ftf") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2") (d #t) (k 0)))) (h "0bpbgrsfhw7di2255c059anri5sjqd2315idnda25g6ng2s7x119")))

