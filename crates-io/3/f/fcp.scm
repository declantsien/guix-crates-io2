(define-module (crates-io #{3}# f fcp) #:use-module (crates-io))

(define-public crate-fcp-0.1.0 (c (n "fcp") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1zk185abjplk6q3lck3yrbyv8nlx58sshgm5z1fqf8f37h7p009p")))

(define-public crate-fcp-0.2.0 (c (n "fcp") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nix") (r "^0.21.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "0jskvpqkskdysw7blsl9n4qdvqz87ahpi7i794qblax1cgmwxisf")))

(define-public crate-fcp-0.2.1 (c (n "fcp") (v "0.2.1") (d (list (d (n "criterion") (r "^0.3.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nix") (r "^0.22.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1yr0j4fbs8kigb5af7zgzyrkqkpcrdmd6858w468zc2y4miaxpgf")))

