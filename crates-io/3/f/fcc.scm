(define-module (crates-io #{3}# f fcc) #:use-module (crates-io))

(define-public crate-fcc-0.0.0 (c (n "fcc") (v "0.0.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0z30i5gbj90ac0win7vq6n6vnlfskm3riwgklsbg3lk0a37g1pzk") (y #t)))

(define-public crate-fcc-0.1.0 (c (n "fcc") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "05q877qic8f2qqm9yp0cyhk0zi432pk7l2dw37h5va46d0z4nlrd") (y #t)))

(define-public crate-fcc-0.1.1 (c (n "fcc") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1chp2qaqrr3ak7a536m16wymd6q68yhy4llk5x126p5cqb3ngp6c") (y #t)))

(define-public crate-fcc-0.1.2 (c (n "fcc") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "11hdjn6lhp8nqb5w8gygiiyx64p89b6ss7w82by4kd1m5ql59h7v") (y #t)))

(define-public crate-fcc-0.2.0 (c (n "fcc") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "0nbx172l2phggylfyd3pfhv46zpmvgw8bnkhm54cdjbb6rc6wqxm") (y #t)))

(define-public crate-fcc-0.3.0 (c (n "fcc") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "1mn6ky18svnrshjamwj54743m1llsgjdy6y55q8hj8gpbcf7vnpp") (y #t)))

(define-public crate-fcc-0.3.1 (c (n "fcc") (v "0.3.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0kzpzywcrs23i257xagza8xh8x7jlcs71vh6pwky7ay56mj0fn7v") (y #t)))

(define-public crate-fcc-0.3.2 (c (n "fcc") (v "0.3.2") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1xy9hybn70s7i7jn5j54hqdr1057ds7dj32d8inl84898b8vqlyh") (y #t)))

(define-public crate-fcc-0.4.0 (c (n "fcc") (v "0.4.0") (d (list (d (n "admerge") (r "^0.1.3") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1jrwkpid5100p6m3pyay1545rd8yzdpqas2bgfim4kz6vlk5mc5f")))

(define-public crate-fcc-0.4.1 (c (n "fcc") (v "0.4.1") (d (list (d (n "admerge") (r "^0.1.3") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "19sh3b6in8c9bb4895dzm782ayh364riry1m2rf9fdfm6nqgwkrl")))

(define-public crate-fcc-0.4.2 (c (n "fcc") (v "0.4.2") (d (list (d (n "admerge") (r "^0.1.3") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0") (d #t) (k 2)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "049xvgjkw4yj8a7m883isw7p2qzpc3adp9iahi5k1yinjbmva6bc")))

