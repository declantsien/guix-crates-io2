(define-module (crates-io #{3}# f fs2) #:use-module (crates-io))

(define-public crate-fs2-0.1.0 (c (n "fs2") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "02b90c0jxdlv9bmj2pbfgf5kfi3lnp9sjdkif4hcr73b3bmwmx0r")))

(define-public crate-fs2-0.2.0 (c (n "fs2") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1wn6z8sfsr61jzjf05w63zwkxmkrv97djf14sv3m4j3fhdijs2jy")))

(define-public crate-fs2-0.2.1 (c (n "fs2") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1cb0pi3qd4zz1ifxwwzqdpvpcwd8dgapd28vyyjka791f4g5kjx5")))

(define-public crate-fs2-0.2.2 (c (n "fs2") (v "0.2.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "02c9fyklphp65cbfmdpq873dc5flxm75a0zrp6b4cya71zg9h1h2")))

(define-public crate-fs2-0.2.3 (c (n "fs2") (v "0.2.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "14dzk0354ya1402h2vpjkvbr1d3q0nxcsz99cachj11xv4znjkx2")))

(define-public crate-fs2-0.2.4 (c (n "fs2") (v "0.2.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0ka5rsfh8klx49ffgsdfglkc944fdrfa0in1i7f4fg3yx5s7kzb6")))

(define-public crate-fs2-0.2.5 (c (n "fs2") (v "0.2.5") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1vsih93cvds3x6f3w9bc5rnkyv8haix1px4jpcqvjyd9l7ji9m5w")))

(define-public crate-fs2-0.3.0 (c (n "fs2") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0lg57mgcm1r0m8jm4nqpcrl6lmxg8lj854k2h0r7qp46pphh2034")))

(define-public crate-fs2-0.4.0 (c (n "fs2") (v "0.4.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1hdzi393wxibimcwsh146l8irn2gyyqi6j0a3hwr93bv668pjyr3")))

(define-public crate-fs2-0.4.1 (c (n "fs2") (v "0.4.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1bsb3x8mkffsnr4xd5ac6v8b8sv80npfd1x37k0rv1amfphaxv9l")))

(define-public crate-fs2-0.4.2 (c (n "fs2") (v "0.4.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0ri8bhz49lyg8hhs0kphjzzlprmsbfhrpbc8cszvfnda5bynrdws")))

(define-public crate-fs2-0.4.3 (c (n "fs2") (v "0.4.3") (d (list (d (n "libc") (r "^0.2.30") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processthreadsapi" "winerror" "fileapi" "winbase" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04v2hwk7035c088f19mfl5b1lz84gnvv2hv6m935n0hmirszqr4m")))

