(define-module (crates-io #{3}# f fsw) #:use-module (crates-io))

(define-public crate-fsw-0.1.0 (c (n "fsw") (v "0.1.0") (d (list (d (n "notify") (r "^4.0.0") (d #t) (k 0)))) (h "137kaabqdzbpzxy0x2mdyy3012kxwbh6q5d260f7axz8qbq9dmyv")))

(define-public crate-fsw-0.1.1 (c (n "fsw") (v "0.1.1") (d (list (d (n "notify") (r "^4.0.10") (d #t) (k 0)))) (h "1cf0sbnyd25cpnyik3yj2aamk9k03f3q5lmjz7n42myr5jxdld54")))

