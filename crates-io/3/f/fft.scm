(define-module (crates-io #{3}# f fft) #:use-module (crates-io))

(define-public crate-fft-0.0.1 (c (n "fft") (v "0.0.1") (h "0m1f4040w335f6zcaxxvzd7n4fv6xfzq6yn59akx8awidk80ywl8")))

(define-public crate-fft-0.1.0 (c (n "fft") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)))) (h "1ncc409shbfw611v422xz2ibbjmw27s4dnkk4wa8yqab2visnmby")))

(define-public crate-fft-0.2.0 (c (n "fft") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)))) (h "03cm8xhwh2m6p8h2xy7iva2fqi3ni93i28fnh147bv0zynf433gv")))

(define-public crate-fft-0.3.0 (c (n "fft") (v "0.3.0") (h "0s4xlljn5q8xqmdmc9922ln19v5p84j9djx7vrbhqsfzjlyp9y6y")))

(define-public crate-fft-0.4.0 (c (n "fft") (v "0.4.0") (d (list (d (n "dft") (r "*") (d #t) (k 0)))) (h "1zv53p8ck216caigfayhrmy0ykrqgzf6gsc7nx5w90ja5kv3wl75")))

