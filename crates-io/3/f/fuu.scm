(define-module (crates-io #{3}# f fuu) #:use-module (crates-io))

(define-public crate-fuu-0.0.1 (c (n "fuu") (v "0.0.1") (h "0f1ji50a1wn0y9h26ys4ag716q53ga3shp5zk1c2qz7bj5zch8yc")))

(define-public crate-fuu-0.1.0 (c (n "fuu") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "iced") (r "^0.10.0") (f (quote ("image" "tokio" "advanced"))) (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "infer") (r "^0.15.0") (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "io-util"))) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "07j98v1jdfibhdja1j9fbjfck1l1ffsxv8g2cxrgim44v95k11mj")))

