(define-module (crates-io #{3}# f fum) #:use-module (crates-io))

(define-public crate-fum-0.1.0 (c (n "fum") (v "0.1.0") (d (list (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "trigram") (r "^0.4.4") (d #t) (k 0)))) (h "1rshxrfln7wfz3628i4sca820c6igb7kjhv085k4fdi1qv7p6nzm")))

