(define-module (crates-io #{3}# f fit) #:use-module (crates-io))

(define-public crate-fit-0.1.0 (c (n "fit") (v "0.1.0") (d (list (d (n "csv") (r "^1.0.5") (d #t) (k 1)) (d (n "phf") (r "^0.7.23") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.23") (d #t) (k 1)))) (h "0nh2a60xxdnfmj0q10bqng22w79hm4fpslxqqh75pan75v46ig9b")))

(define-public crate-fit-0.2.0 (c (n "fit") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "00x35n5jj913d4pbiv8l7z7xal63szzv17yxaymm3sczipf8rx0f")))

(define-public crate-fit-0.2.1 (c (n "fit") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "csv") (r "^1.0") (d #t) (k 1)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1f51zq249zqsiy31snxvjnchy31jcqvqzccw18xfi958sga0nhcc")))

(define-public crate-fit-0.3.0 (c (n "fit") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "fit-sdk") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05kagksmv5ldxnphk15lpvr1wk9k2wfxb3if96ws2in3c9ff4f4x")))

(define-public crate-fit-0.4.1 (c (n "fit") (v "0.4.1") (d (list (d (n "copyless") (r "^0.1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.0") (d #t) (k 1)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "145xfrn7yz9skkw3i0a74s54xz6kln03g4bvgygk4kihhzmsl4lc")))

(define-public crate-fit-0.4.2 (c (n "fit") (v "0.4.2") (d (list (d (n "copyless") (r "^0.1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.2.11") (d #t) (k 2)) (d (n "csv") (r "^1.0") (d #t) (k 1)) (d (n "heck") (r "^0.3") (d #t) (k 1)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "13gxzny5s65k5kalga12qcg7hi2gwyypdk0wf44bjhnd8ahb4kzk")))

(define-public crate-fit-0.5.0 (c (n "fit") (v "0.5.0") (d (list (d (n "copyless") (r "^0.1.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "fitsdk") (r "^0.1.0") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "1in8ac5435h6kc87hsfj2s0qrq7dynwrwwbhxkgdspwzydlw3c70")))

