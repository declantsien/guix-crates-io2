(define-module (crates-io #{3}# f frm) #:use-module (crates-io))

(define-public crate-frm-0.0.1 (c (n "frm") (v "0.0.1") (d (list (d (n "polars") (r "^0.38.3") (f (quote ("lazy" "parquet" "csv" "polars-io"))) (d #t) (k 0)) (d (n "polars-io") (r "^0.38.3") (f (quote ("parquet" "csv"))) (d #t) (k 0)))) (h "1pihndrqayy29w3r4i22yb57i2cj4nvvhfpxpa50b836y63bp391")))

(define-public crate-frm-0.0.2 (c (n "frm") (v "0.0.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "polars") (r "^0.38.3") (f (quote ("lazy" "parquet" "csv" "polars-io"))) (d #t) (k 0)) (d (n "polars-io") (r "^0.38.3") (f (quote ("parquet" "csv"))) (d #t) (k 0)))) (h "0r3l0kj9vi24kc6533k1552j5p4wxc553lf7l6z7dks1nn4y1y06")))

