(define-module (crates-io #{3}# f foc) #:use-module (crates-io))

(define-public crate-foc-0.1.0 (c (n "foc") (v "0.1.0") (d (list (d (n "fixed") (r "^1.24.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (k 0)) (d (n "simba") (r "^0.8.1") (f (quote ("partial_fixed_point_support"))) (k 0)))) (h "164cabdf9lz1dkmhz1p9fhnysl18l3xggl1k5hskgz24aq84nk4k")))

(define-public crate-foc-0.2.0 (c (n "foc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 2)) (d (n "ciborium") (r "^0.2.1") (d #t) (k 2)) (d (n "cordic") (r "^0.1.5") (d #t) (k 0)) (d (n "fixed") (r "^1.24.0") (d #t) (k 0)) (d (n "fixed-macro") (r "^1.2.0") (d #t) (k 0)) (d (n "mcap") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.195") (d #t) (k 2)))) (h "0slbdghwyql01ixx8a3xiz4symqjczkawmpac08rmf7b38ffb9xc")))

