(define-module (crates-io #{3}# f ftc) #:use-module (crates-io))

(define-public crate-ftc-0.1.0 (c (n "ftc") (v "0.1.0") (d (list (d (n "forgedthoughts") (r "^0.1") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "0i977adyklax5j1d95sksnh0vdf4gzmaj650qnx22hdjlgp39ir4")))

(define-public crate-ftc-0.1.1 (c (n "ftc") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.5") (d #t) (k 0)) (d (n "forgedthoughts") (r "^0.1.1") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "030bn0qxqv9xhl3rwchsi7j6qk9g52acq07m2z7fxh549mqz72aa")))

(define-public crate-ftc-0.1.2 (c (n "ftc") (v "0.1.2") (d (list (d (n "clap") (r "^4.1.5") (d #t) (k 0)) (d (n "forgedthoughts") (r "^0.1.2") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "156al7k2cqk5g8m8va69p7y8jlgll0y89p4xghymrqpz5nplrfi1")))

(define-public crate-ftc-0.1.3 (c (n "ftc") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.5") (d #t) (k 0)) (d (n "forgedthoughts") (r "^0.1.3") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "1h4rs59vkmpn0zfzmkbjm1hdywdn3b61yxixm47xhr7h4vv6scci")))

(define-public crate-ftc-0.1.4 (c (n "ftc") (v "0.1.4") (d (list (d (n "clap") (r "^4.1.5") (d #t) (k 0)) (d (n "forgedthoughts") (r "^0.1.4") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "01a310pf3by0l2i5w0c3gcynm9a5c0pl1hnwzd4b4n2plj8jsgza")))

(define-public crate-ftc-0.1.5 (c (n "ftc") (v "0.1.5") (d (list (d (n "clap") (r "^4.1.5") (d #t) (k 0)) (d (n "forgedthoughts") (r "^0.1.5") (d #t) (k 0)) (d (n "png") (r "^0.17.5") (d #t) (k 0)))) (h "1jry4x7xaccgii9z4xlz8z1h55lf0jdsimx6bl9m9d8gi77h1143")))

