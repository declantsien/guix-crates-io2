(define-module (crates-io #{3}# f ftp) #:use-module (crates-io))

(define-public crate-ftp-0.0.1 (c (n "ftp") (v "0.0.1") (h "1byj2n0pq1k2dl8ixx5axa10kp9zzdwd48mf4ldyg9i4v298wgmg")))

(define-public crate-ftp-0.0.2 (c (n "ftp") (v "0.0.2") (h "0fnfhlavgb7drbznm9q7kn6dx8r8if1ck2sf283jgyn9n8y5h1c5")))

(define-public crate-ftp-0.0.3 (c (n "ftp") (v "0.0.3") (h "0jqkq73jf793hqhwxpvww45hrxamb75m0v3p51wkwl54x1g35x4m")))

(define-public crate-ftp-0.0.4 (c (n "ftp") (v "0.0.4") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "1vhr4wiqy78jz5f8hdlqf40gpsnpmfyd3ihcijqr5zqg4al0vvj5")))

(define-public crate-ftp-0.0.5 (c (n "ftp") (v "0.0.5") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "139nlkyg60w1g4p3fv3m7q26sxvhsi2yn71hsj3h5nnkr96nfhvw")))

(define-public crate-ftp-0.0.6 (c (n "ftp") (v "0.0.6") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "06dnmwd20z6rwhfd6cnm38g1c07448d64jijd26i3irp1iiz51gg")))

(define-public crate-ftp-0.0.7 (c (n "ftp") (v "0.0.7") (d (list (d (n "regex") (r "*") (d #t) (k 0)))) (h "0azaj2ggh49pp7sxgaqm1jz8icdvqncqx8sbfppz7c3b0rb369qp")))

(define-public crate-ftp-0.0.8 (c (n "ftp") (v "0.0.8") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0y9xvcdgw1qkzhrirhasklx9p4gsrsyih2kijwky4myh8cgh0knn") (f (quote (("secure" "openssl") ("debug_print"))))))

(define-public crate-ftp-1.0.0 (c (n "ftp") (v "1.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0xarzxvr24b4dg5q9kxsnqf6n4ykxxmkb2gmhq8nkismyfzdpii0") (f (quote (("secure" "openssl") ("debug_print"))))))

(define-public crate-ftp-2.0.0 (c (n "ftp") (v "2.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "101fag4zwyspn4p1cskgg5zfb8y0ypjff66y1babaympg1nsfw57") (f (quote (("secure" "openssl") ("debug_print"))))))

(define-public crate-ftp-2.0.1 (c (n "ftp") (v "2.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0614pgk55vd12gshl2kkf31vy5xk92a47xgg7ga5zq25wf3gpgy2") (f (quote (("secure" "openssl") ("debug_print"))))))

(define-public crate-ftp-2.1.1 (c (n "ftp") (v "2.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0wqalpp3gz2nsy3hj2dyb317ml1d2vkg5j90bwn1n1da7f1j7v6w") (f (quote (("secure" "openssl") ("debug_print"))))))

(define-public crate-ftp-2.2.1 (c (n "ftp") (v "2.2.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1lbzyf7naz0kp2xnbnv606i2g2622jccs7p03s0ncij35s6rlrzl") (f (quote (("secure" "openssl") ("debug_print"))))))

(define-public crate-ftp-2.1.2 (c (n "ftp") (v "2.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1la1q83s6yjcbysfk50nw6lb8fy9np65slbqyj497g61fbn5p6nc") (f (quote (("secure" "openssl") ("debug_print"))))))

(define-public crate-ftp-3.0.0 (c (n "ftp") (v "3.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "03cxmghcb7ppzi501gn73fd45d9ha8yda25cy6d0k5smz15pw212") (f (quote (("secure" "openssl") ("debug_print"))))))

(define-public crate-ftp-3.0.1 (c (n "ftp") (v "3.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "openssl") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0pddhc04x6dz0r0k3ry437a3l6idnrybvqq9fk154687s2m52aal") (f (quote (("secure" "openssl") ("debug_print"))))))

