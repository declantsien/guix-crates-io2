(define-module (crates-io #{3}# f fts) #:use-module (crates-io))

(define-public crate-fts-0.1.0 (c (n "fts") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 2)))) (h "1pacxiiqki9iw2v76vk7l10p3p7s0nc3ypqkdwxi7h4ij9a81lpd")))

(define-public crate-fts-0.1.1 (c (n "fts") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 2)))) (h "07isb023yhdmqzwhkxak1j9n0mddp95945yi1fqf3b21md027hq7")))

(define-public crate-fts-0.1.2 (c (n "fts") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 2)))) (h "0dk64g88r3a4ldllbc2gnd4mxzfgb5z893mbwpgmmvvhdnz1198z")))

(define-public crate-fts-0.1.3 (c (n "fts") (v "0.1.3") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 2)))) (h "1gbwa5kdxnmh1hhidng99ypdy00ygq45gxznyvqw5v86ps3a0cds")))

(define-public crate-fts-0.1.4 (c (n "fts") (v "0.1.4") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 2)))) (h "1pg0av1ng88k74sq242pj6y1j4lgx5plm15km2j2hkmxbvzbi209")))

(define-public crate-fts-0.1.5 (c (n "fts") (v "0.1.5") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 2)))) (h "0zipmd9pihfk1scw81pkhh2q2bvhf67qqgj9vn3q779cvgsngj1y")))

(define-public crate-fts-0.1.6 (c (n "fts") (v "0.1.6") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 2)))) (h "0kd2x432lnyycxsv4qzf7c820gnss8pbpxy81w7zwbrf7lrppwqy")))

(define-public crate-fts-0.1.7 (c (n "fts") (v "0.1.7") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 2)))) (h "06lrz5swgij1kmp5w2djxkwb8545hng91l63zw9zdkymfavsjadj")))

(define-public crate-fts-0.2.0 (c (n "fts") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.5.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "num") (r "^0.1.31") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 2)))) (h "0lk1f9wvij6lhhjqkki84j5rm12i9sp4czhasbilmvx0dd0saxya")))

(define-public crate-fts-0.3.0 (c (n "fts") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 2)))) (h "1gycqxynci688lq825ikjrpgc1kl71n5y5fhw6cxlp5g6g59ps5y")))

