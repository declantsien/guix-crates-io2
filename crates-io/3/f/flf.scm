(define-module (crates-io #{3}# f flf) #:use-module (crates-io))

(define-public crate-flf-0.1.0 (c (n "flf") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^3.1.2") (d #t) (k 0)) (d (n "humansize") (r "^1.1.1") (d #t) (k 0)) (d (n "litemap") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "03s13w9i36mnqgz1j231k7qynk4kx8pc5z9b2qg65mnwiir31dpl")))

(define-public crate-flf-0.1.1 (c (n "flf") (v "0.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4") (d #t) (k 0)) (d (n "humansize") (r "^2") (d #t) (k 0)) (d (n "litemap") (r "^0.7.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1ihh0h2rsyaansdah790nvq4vbikf7c2vxkggxvf3gnvjfi7kglh")))

