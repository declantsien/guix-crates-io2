(define-module (crates-io #{3}# f fon) #:use-module (crates-io))

(define-public crate-fon-0.1.0 (c (n "fon") (v "0.1.0") (h "18rj3f25ii33p43kniq82fq8dvszfwh0xnnwb8jqrpcm0gif39k2")))

(define-public crate-fon-0.2.0 (c (n "fon") (v "0.2.0") (h "1m072q7nvh13d6wkgvs68qa7q2dk1dlr5fzj8jwlqvyfh5idi0ai")))

(define-public crate-fon-0.3.0 (c (n "fon") (v "0.3.0") (h "0hlxrxkpina5qp0lp0bwrshhvaa1cwl2y69r8z8cavysnp2m4l6q")))

(define-public crate-fon-0.4.0 (c (n "fon") (v "0.4.0") (h "00cw6zn7p8a86jn25b6c4njz79zm8j9z3q40pnfrq0nfy4i80j7b")))

(define-public crate-fon-0.5.0 (c (n "fon") (v "0.5.0") (h "1mfdrgkas6swlm8d9w02izc63jcxm0rzy8nfrkp0dfjv3h56lwfr")))

(define-public crate-fon-0.6.0 (c (n "fon") (v "0.6.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)))) (h "1i2m5jcbbg3gbh6y0vj359ncbz88bjdrdaj2lwiqhs5wr7ka0imd") (r "1.56.1")))

