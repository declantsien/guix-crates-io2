(define-module (crates-io #{3}# f fdp) #:use-module (crates-io))

(define-public crate-fdp-0.1.0 (c (n "fdp") (v "0.1.0") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "fdp-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1cnn3j086v7x5wigi2r46rjq168wy66m1p99m0sq2mkb5lrlpyh7")))

(define-public crate-fdp-0.2.0 (c (n "fdp") (v "0.2.0") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "fdp-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "05jnjz9b830i9xy2njx398z6z5g57ifxhly45pnf6rx0by5lgpy4")))

(define-public crate-fdp-0.2.2 (c (n "fdp") (v "0.2.2") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "fdp-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0a429djxw9zzsxzbgghpr4jf7179yjwwg999qmmwgnpn1zh2f6qh")))

(define-public crate-fdp-0.2.3 (c (n "fdp") (v "0.2.3") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "fdp-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "06kcs0x87i8mm4p9jgdgx7jmkil5zvyfjisi49dnb2xlg6fjwm4c")))

(define-public crate-fdp-0.2.4 (c (n "fdp") (v "0.2.4") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "fdp-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "12rv501pybs47112avjljhyy3wzl1zp6fgifkw5z5wi8f4mzmbl1")))

(define-public crate-fdp-0.2.5 (c (n "fdp") (v "0.2.5") (d (list (d (n "custom_error") (r "^1.7.1") (d #t) (k 0)) (d (n "enum-primitive-derive") (r "^0.1") (d #t) (k 0)) (d (n "fdp-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libloading") (r "^0.6.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0firpb5vgiim5iidl38yfkg57c1pfh9y60ap5f7w71ccilm7ghh5")))

