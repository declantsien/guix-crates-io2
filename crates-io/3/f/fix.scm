(define-module (crates-io #{3}# f fix) #:use-module (crates-io))

(define-public crate-fix-0.1.0 (c (n "fix") (v "0.1.0") (d (list (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "0mxl6rp1fay9mzjslzx0hdsbaj0xx5fpn7yxh9yak98wrjgjfsg1") (f (quote (("i128" "typenum/i128"))))))

(define-public crate-fix-0.1.1 (c (n "fix") (v "0.1.1") (d (list (d (n "typenum") (r "^1.9.0") (d #t) (k 0)))) (h "1l6b4dng4ma097jd0i9v8k430ypp0b9iijzcpc6v2kbqghbhhmwi") (f (quote (("i128" "typenum/i128"))))))

