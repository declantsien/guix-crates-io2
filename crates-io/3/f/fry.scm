(define-module (crates-io #{3}# f fry) #:use-module (crates-io))

(define-public crate-fry-0.0.1 (c (n "fry") (v "0.0.1") (d (list (d (n "include_data") (r "^1.0.0") (d #t) (k 0)))) (h "07d4q5yh9l4yviqkfz53w0ms7hd3j1vclwaiyqshjwvyir1ha6nn")))

(define-public crate-fry-0.0.2 (c (n "fry") (v "0.0.2") (d (list (d (n "include_data") (r "^1.0.0") (d #t) (k 0)))) (h "13wvp2ld850zq29ky8jrj805lyn61lxfq78iwaj81vxg3zramf35")))

