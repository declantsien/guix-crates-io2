(define-module (crates-io #{3}# f flv) #:use-module (crates-io))

(define-public crate-flv-0.0.0 (c (n "flv") (v "0.0.0") (h "1sf47ywz1bz05i9md30j7f59gx9mpnfi01xbnh19zzkn7z4pxbbj")))

(define-public crate-flv-0.0.1 (c (n "flv") (v "0.0.1") (d (list (d (n "lru") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-util"))) (o #t) (d #t) (k 0)))) (h "0yzj1dv83czxn6l6j48giajlpsg0llvy2qz6in680vx4a1qglv75") (f (quote (("read-index" "lru") ("io-tokio" "tokio") ("io-std") ("full" "io-std" "io-tokio" "read-index"))))))

