(define-module (crates-io #{3}# f fbx) #:use-module (crates-io))

(define-public crate-fbx-0.1.0 (c (n "fbx") (v "0.1.0") (h "1n1dcvb2swma7vd1pj8pk60zjlg6gqy0ippdqibq339ay149x57f")))

(define-public crate-fbx-0.2.0 (c (n "fbx") (v "0.2.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.2") (d #t) (k 0)))) (h "0yn68x42m54gwpk7imd37ivqx3kz8np1xdlyb1lfwxjywdjwc2s9")))

