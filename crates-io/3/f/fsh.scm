(define-module (crates-io #{3}# f fsh) #:use-module (crates-io))

(define-public crate-fsh-0.1.0 (c (n "fsh") (v "0.1.0") (d (list (d (n "ctrlc") (r "^3.1.6") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "rudac") (r "^0.8.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0b0bnzbw9fzcwjvy6mfgfrw0knanpmnk1xppb5cnic15p13c7rg9")))

