(define-module (crates-io #{3}# f fiz) #:use-module (crates-io))

(define-public crate-fiz-0.0.1 (c (n "fiz") (v "0.0.1") (d (list (d (n "fiz-math") (r "*") (d #t) (k 0)))) (h "0v1a8jk7vnhfai466jqmrpmyjks28x5wm0yy8z1fdlvkmiwsv688")))

(define-public crate-fiz-0.0.2 (c (n "fiz") (v "0.0.2") (d (list (d (n "fiz-math") (r "^0.0.12") (d #t) (k 0)))) (h "1gw5wawilmcj638qdjq75z8g2gpjznr6dp7axg173capjp60nvn7") (y #t)))

(define-public crate-fiz-0.0.3 (c (n "fiz") (v "0.0.3") (d (list (d (n "fiz-math") (r "^0.0.12") (d #t) (k 0)))) (h "1v26f8wnijvls765dwjqh3h1xs2jixg84r2davmpkkfm7r9azs2c")))

(define-public crate-fiz-0.0.4 (c (n "fiz") (v "0.0.4") (d (list (d (n "fiz-math") (r "^0.0.13") (d #t) (k 0)))) (h "06ndi0i9pc4ab3y9a7dhjjpjlz223sk47azmy22vb7ssiizv34y0")))

