(define-module (crates-io #{3}# f fmp) #:use-module (crates-io))

(define-public crate-fmp-0.1.0 (c (n "fmp") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1zkxyiwa8xzaapp540p77zzrasz0b4ph9qldbal748i0f21q2lis") (r "1.61")))

(define-public crate-fmp-0.2.0 (c (n "fmp") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0zcjdfbs59afk0bbz10skp7mb3qaxm4s8k62192hnzisvaj671im") (r "1.61")))

(define-public crate-fmp-0.2.1 (c (n "fmp") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0miswp4zmbif85fkr5yr2sh7b9gbmjpfihij9s6fvf2rf654z1bx") (r "1.61")))

(define-public crate-fmp-0.2.2 (c (n "fmp") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "04hh1kcfz5fx1jmxgg7a0xq5lwx7xqdyfdhy1qynky9l7b3wpwnf") (r "1.61")))

(define-public crate-fmp-0.2.3 (c (n "fmp") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0n9ri8gxy36k3gpwdix7pg5ysm2c158cq721khivfrx3dy6krqha") (r "1.61")))

