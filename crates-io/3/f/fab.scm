(define-module (crates-io #{3}# f fab) #:use-module (crates-io))

(define-public crate-fab-0.1.0 (c (n "fab") (v "0.1.0") (d (list (d (n "clap") (r "^2.28") (d #t) (k 0)))) (h "0xxdar3llc2ashcniakqqm19znmp3rqnfacjiix8fgqs7zcgpb2w")))

(define-public crate-fab-0.1.1 (c (n "fab") (v "0.1.1") (d (list (d (n "clap") (r "^2.28") (d #t) (k 0)))) (h "0vb32cr60sv93vmlacj08p6micwxsfb50rcv1ba5191j28n78ry6")))

