(define-module (crates-io #{3}# f fus) #:use-module (crates-io))

(define-public crate-fus-0.1.1 (c (n "fus") (v "0.1.1") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wild") (r "^2") (d #t) (k 0)))) (h "0w1cil43yz1ravi3j3r73qkvcqfyw7vvf5ypsj0x3v4ri6fk49fi")))

(define-public crate-fus-0.1.2 (c (n "fus") (v "0.1.2") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wild") (r "^2") (d #t) (k 0)))) (h "1xczhhkrfpiwvlpcswxrgc8il1pmw655120dk85n4sb5wh643gqv")))

(define-public crate-fus-0.1.3 (c (n "fus") (v "0.1.3") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wild") (r "^2") (d #t) (k 0)))) (h "1f0q8rz9vmxlycvh5n7ngyzlhfr5w4mywa1ylsl7lvs8hl4bwsv6")))

(define-public crate-fus-0.1.4 (c (n "fus") (v "0.1.4") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wild") (r "^2") (d #t) (k 0)))) (h "0749c65g3bq5qci86qxgf9dgi0v2v4lb0d1cfvgh9yjxvgmbvk0v")))

(define-public crate-fus-0.1.5 (c (n "fus") (v "0.1.5") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wild") (r "^2") (d #t) (k 0)))) (h "1jh5qbypdqyim4abd6sxpr5wy6v5b0h86ndpvz0x2msq63wwv85n")))

(define-public crate-fus-0.1.6 (c (n "fus") (v "0.1.6") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wild") (r "^2") (d #t) (k 0)))) (h "1aqp8njs24a2b3nm3jliw1rw9anznkm4i40jp58afsn27shj9v2z")))

(define-public crate-fus-0.1.7 (c (n "fus") (v "0.1.7") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wild") (r "^2") (d #t) (k 0)))) (h "1c3p8gc0wdi8cp8yd1psxw6rmhicrd9zabakvgiywmh992644lhq")))

