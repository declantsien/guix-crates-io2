(define-module (crates-io #{3}# f flo) #:use-module (crates-io))

(define-public crate-flo-0.1.0 (c (n "flo") (v "0.1.0") (d (list (d (n "desync") (r "^0.1.2") (d #t) (k 0)) (d (n "flo_animation") (r "^0.1.0") (d #t) (k 0)) (d (n "flo_binding") (r "^0.1.0") (d #t) (k 0)) (d (n "flo_canvas") (r "^0.1.0") (d #t) (k 0)) (d (n "flo_curves") (r "^0.1.0") (d #t) (k 0)) (d (n "flo_ui") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)))) (h "0db8shx2qrykrijyimcdr56n0373d394si3gx5asri0r58hv2ybj")))

