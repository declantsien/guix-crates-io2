(define-module (crates-io #{3}# f fin) #:use-module (crates-io))

(define-public crate-fin-0.0.1 (c (n "fin") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "09mphrpayg73b3q8gcjwx07frd5n571ggi6x2zalcpwdnqmj14f6") (f (quote (("bounded_float_debug_check"))))))

(define-public crate-fin-0.0.2 (c (n "fin") (v "0.0.2") (d (list (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)))) (h "0hr7n17bsjbgf0zk8jnw8m2c56yzs0mnxdnjg83afsafpdl75k6n") (f (quote (("bounded_float_debug_check"))))))

