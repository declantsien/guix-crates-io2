(define-module (crates-io #{3}# f fbt) #:use-module (crates-io))

(define-public crate-fbt-0.1.0 (c (n "fbt") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fbt-lib") (r "^0.1.0") (d #t) (k 0)))) (h "1746zs58vcrq9imra30z21jxyhviq0pbnpvg62bg074yvihddx72")))

(define-public crate-fbt-0.1.1 (c (n "fbt") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fbt-lib") (r "^0.1.1") (d #t) (k 0)))) (h "1w27civ33ynplv8syf8pwgv98m5a7sy37az3i8dx4j5h9310np0w")))

(define-public crate-fbt-0.1.2 (c (n "fbt") (v "0.1.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fbt-lib") (r "^0.1.2") (d #t) (k 0)))) (h "1fhjk0zij5mm1w0xfh6kn7b2a5h118y9ifqfiyx38n02saxrfa4x")))

(define-public crate-fbt-0.1.3 (c (n "fbt") (v "0.1.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fbt-lib") (r "^0.1.3") (d #t) (k 0)))) (h "0qlaiyg8kgwpzlyz6y23wcnylyk9qgrk8jfdg95czzlag96rmjjm")))

(define-public crate-fbt-0.1.4 (c (n "fbt") (v "0.1.4") (d (list (d (n "fbt-lib") (r "^0.1.4") (d #t) (k 0)))) (h "171xxz1n62hhg1dh0fnv7xb2mfhhrcqkmc9ff61bgrvjwfv51ky1")))

(define-public crate-fbt-0.1.5 (c (n "fbt") (v "0.1.5") (d (list (d (n "fbt-lib") (r "^0.1.5") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "0gcx093nfsgh14gvm5jpz5wxrgrigx16dfysg5hgc94cfmnv6r55")))

(define-public crate-fbt-0.1.6 (c (n "fbt") (v "0.1.6") (d (list (d (n "fbt-lib") (r "^0.1.6") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "1cgi6qdqpvgwkn90f86yksvp4k3zfybyxbr5vv2ikcha9jr9a6id")))

(define-public crate-fbt-0.1.7 (c (n "fbt") (v "0.1.7") (d (list (d (n "fbt-lib") (r "^0.1.7") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "00wxd1pcw37mm2lqmd5nicavkf4jidwrb1idynz21yprjjnbfrrl")))

(define-public crate-fbt-0.1.8 (c (n "fbt") (v "0.1.8") (d (list (d (n "fbt-lib") (r "^0.1.8") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "131nipznc4n7j4jsm58izjnzgssrk6ms6ndzdckiym2ga7v0yqmj")))

(define-public crate-fbt-0.1.9 (c (n "fbt") (v "0.1.9") (d (list (d (n "fbt-lib") (r "^0.1.8") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "0q8y9w7flyvlzj988j0m6id1s4d7y7bq02l2bax7igwf7yi8p2bh")))

(define-public crate-fbt-0.1.10 (c (n "fbt") (v "0.1.10") (d (list (d (n "fbt-lib") (r "^0.1.10") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "0f78d4nqgp69cm424y1k5xagspvmpgkmmfygbqlamii3d6h9b5q8")))

(define-public crate-fbt-0.1.11 (c (n "fbt") (v "0.1.11") (d (list (d (n "fbt-lib") (r "^0.1.11") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "166ilk0spvfwias6k0z3cqfw692kkj2ghlqwfh54ic2m55zr7zsp")))

(define-public crate-fbt-0.1.12 (c (n "fbt") (v "0.1.12") (d (list (d (n "fbt-lib") (r "^0.1.11") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "136wmmnj11d3qn2cb814iislp76sv15ramwxa1srirfzl0qqi9vr")))

(define-public crate-fbt-0.1.13 (c (n "fbt") (v "0.1.13") (d (list (d (n "fbt-lib") (r "^0.1.13") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "078ha1cv04wv8vkpwrrlans7xab25xd50bvzds93y291a4nkd5pi")))

(define-public crate-fbt-0.1.14 (c (n "fbt") (v "0.1.14") (d (list (d (n "fbt-lib") (r "^0.1.14") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "03licpmhaxg7pjfsd01sxqyrzfdwzac596n0q1cmcdgvhl436z9v")))

(define-public crate-fbt-0.1.15 (c (n "fbt") (v "0.1.15") (d (list (d (n "fbt-lib") (r "^0.1.15") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "1kh68dmn88fq7v518lzg42bkqzili2j1lvdwvc0wrsijjpf9jnk9")))

(define-public crate-fbt-0.1.16 (c (n "fbt") (v "0.1.16") (d (list (d (n "fbt-lib") (r "^0.1.16") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "1rsglvh8chzs7rm6w9dxrg4mq7hx0n148cc3jax138pr9fdh9fdx")))

(define-public crate-fbt-0.1.17 (c (n "fbt") (v "0.1.17") (d (list (d (n "fbt-lib") (r "^0.1.17") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "0diqvsj45szz2dwcpyax1cp78224m21kma0snk80bmhwwmxbxvh1")))

(define-public crate-fbt-0.1.18 (c (n "fbt") (v "0.1.18") (d (list (d (n "fbt-lib") (r "^0.1.18") (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)))) (h "0x153k0v7vdillprfq2mkyysvvzakff3d26d4d6jx069c6bzssvw")))

