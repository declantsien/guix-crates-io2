(define-module (crates-io #{3}# f flu) #:use-module (crates-io))

(define-public crate-flu-0.0.1 (c (n "flu") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0z30355z5m405gwkq05mxznxysqf5j2y978jmlx1cam5ackndynv")))

(define-public crate-flu-0.0.2 (c (n "flu") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1b8k08wcpljfl9a9afkqxp53ljbvi5c3fq0sl4c13nmaczlfpjyb")))

