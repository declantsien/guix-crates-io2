(define-module (crates-io #{3}# f fsf) #:use-module (crates-io))

(define-public crate-fsf-0.1.0 (c (n "fsf") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "open") (r "^5.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "tera") (r "^1.19.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.6") (d #t) (k 0)))) (h "0ydzp3l4nzrqbh8zhwdzqvi4dcvs2cj80s2iiac1c05pxwaj68za")))

