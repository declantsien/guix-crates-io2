(define-module (crates-io #{3}# f flx) #:use-module (crates-io))

(define-public crate-flx-0.1.0 (c (n "flx") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0h1s0xxsyip87f5k8fjql7az7kllvg0byhipm4fy14pkn8hrijx4")))

(define-public crate-flx-0.1.1 (c (n "flx") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "0qwbg7m7j1c5gqikvggabjvnhhkp2xpcfi895cf778ajdy68gyri")))

(define-public crate-flx-0.2.0 (c (n "flx") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1h8ysgz02fmkqa5ym5lsjcwbd4v3i4a9w363jq6f2h31mc8cijmr")))

(define-public crate-flx-0.2.1 (c (n "flx") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "1dkw97w8bddf2mixbypxyarafa4zni8wpynki1gmg9jn96gsgv7g")))

(define-public crate-flx-0.2.2 (c (n "flx") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1") (d #t) (k 0)))) (h "00nl672vc0ldczd2i5rlx88i8am529g75w7j3ln7zxkxcmipv4ql")))

