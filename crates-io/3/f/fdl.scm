(define-module (crates-io #{3}# f fdl) #:use-module (crates-io))

(define-public crate-fdl-0.1.0 (c (n "fdl") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.20.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.20") (f (quote ("lexer"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1kryzc6b7mpki35zz3h6jb19zs697igkh0qdmjpjygqpbbkqicm6") (f (quote (("fixed_point"))))))

