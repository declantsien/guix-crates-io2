(define-module (crates-io #{3}# f ffd) #:use-module (crates-io))

(define-public crate-ffd-0.1.0-alpha.0 (c (n "ffd") (v "0.1.0-alpha.0") (h "04iav2z5xda4l8g1pqvjkl8j5qhbhg0nxh0a01rbl2qas8wylg6k") (f (quote (("nightly"))))))

(define-public crate-ffd-0.1.0-alpha.1 (c (n "ffd") (v "0.1.0-alpha.1") (h "0m64idm4baby6mi7sgwlsiwzmi516cxwrb8va8i3pz4mbyh14wbc") (f (quote (("nightly"))))))

