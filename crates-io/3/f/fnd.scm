(define-module (crates-io #{3}# f fnd) #:use-module (crates-io))

(define-public crate-fnd-0.1.0 (c (n "fnd") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.7.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1wzz08yv4mrz8z9d99qr69pjq89shz2qm9lb0d2j0vdym271hj1l")))

(define-public crate-fnd-0.2.0 (c (n "fnd") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "05prq1cjr5rkaiqsg0z68crcqdqwwk2jcx5qmkj0hibwnrlhyzis")))

(define-public crate-fnd-0.3.0 (c (n "fnd") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "0m77v89qabg6iffkfbg6z2xl0bvas7l97z979xcjkpcwpr888mfk")))

(define-public crate-fnd-0.3.1 (c (n "fnd") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0pp1f60cld2kls5xb1f48w6sjrg4hbgxqc7sws78apny4rfmnvhj")))

(define-public crate-fnd-0.3.2 (c (n "fnd") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "112vvynizfinb34711yrnc3ddlhska93vw4xzgy211j3kidhslm7")))

