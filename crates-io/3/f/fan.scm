(define-module (crates-io #{3}# f fan) #:use-module (crates-io))

(define-public crate-fan-0.1.0 (c (n "fan") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "time"))) (k 2)))) (h "0ip88kb7mz65m95czv0zirwbzfhqjb1g1j99c6rcc2hqq84jhkx1") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:futures"))))))

(define-public crate-fan-0.1.1 (c (n "fan") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "time"))) (k 2)))) (h "1mlxyxyiq3mz5p55jk2xcmj0rrqyqj3yggn8byfv5hydrmq36s3i") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:futures"))))))

(define-public crate-fan-0.1.2 (c (n "fan") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "time"))) (k 2)))) (h "0wr1kln08x09ms2q4pzdgpykawpnyfq4s2bblm80f883amd51chi") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:futures"))))))

(define-public crate-fan-0.1.3 (c (n "fan") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "macros" "time"))) (k 2)))) (h "0bshxmy9qkrzvfldwxqq045cxzsix42cqc5r5hgkvfl6klz3ndbk") (f (quote (("default" "tokio")))) (s 2) (e (quote (("tokio" "dep:tokio" "dep:futures"))))))

