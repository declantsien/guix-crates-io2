(define-module (crates-io #{3}# f fxd) #:use-module (crates-io))

(define-public crate-fxd-0.1.0 (c (n "fxd") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.3") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s8i9cp2lm53l89c16k15dhqfvblbcqv20mw3ylvw5h9k99sc2rv")))

(define-public crate-fxd-0.1.1 (c (n "fxd") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.3") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16x86vzw6y34vy21sz0xp3hyj7579fnx60aqxyzry6bnwyvf4dfd")))

(define-public crate-fxd-0.1.2 (c (n "fxd") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.3") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d4kszdc53pmcv7wa4lancwhnradd1l10k0ygmw67qm9nkh7xfdz")))

(define-public crate-fxd-0.1.3 (c (n "fxd") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.3") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10sc19yppmn4gxc3mm3hrj3lh5rxiydch18386bxyb977igk808i")))

(define-public crate-fxd-0.1.4 (c (n "fxd") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pprof") (r "^0.3") (f (quote ("flamegraph"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15gnl9vnxi8sg7mk0zb2dy71085ldk14m79hv1wwnpbjzjl5nb7v")))

