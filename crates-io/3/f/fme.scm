(define-module (crates-io #{3}# f fme) #:use-module (crates-io))

(define-public crate-fme-0.1.0 (c (n "fme") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04fi8zqx5l4xpn8s1vxckqfqz6mszfnlq0vjdkcjy9jp7v12fk57")))

(define-public crate-fme-0.1.1 (c (n "fme") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sfpywxqfx4073vh80g12d81fh0xx42hpvnpxyrd1b2d6vq985cg")))

(define-public crate-fme-0.1.2 (c (n "fme") (v "0.1.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hh72p1vpk5xnaf4z5bni7j1fwrgi764plymjkgf645kj3jwifmd")))

