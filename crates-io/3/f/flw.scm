(define-module (crates-io #{3}# f flw) #:use-module (crates-io))

(define-public crate-flw-0.0.1 (c (n "flw") (v "0.0.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "16wansa6mrvnakrx9nr3b2aj5z48a2amvqjv90r80zgh77pq6v68")))

(define-public crate-flw-0.0.2 (c (n "flw") (v "0.0.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "04jg3zj5yhga01smsvkin18qv2bynq0v6bdk1l55k5bm906nx0ak")))

(define-public crate-flw-0.0.3 (c (n "flw") (v "0.0.3") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0nmwrg0ik6z0kaprpccz7rqmzh73bs7nkajr28axigmc6c4lwfaz")))

