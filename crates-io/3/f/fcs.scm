(define-module (crates-io #{3}# f fcs) #:use-module (crates-io))

(define-public crate-fcs-0.1.0 (c (n "fcs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0s8h1c2b6r3dd0r4q2asc46hza9s1v1rwpm1yc70434pw7g0wbhy")))

