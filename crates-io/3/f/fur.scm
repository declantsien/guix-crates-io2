(define-module (crates-io #{3}# f fur) #:use-module (crates-io))

(define-public crate-fur-0.0.1 (c (n "fur") (v "0.0.1") (d (list (d (n "mio") (r "^0.7.0-alpha.1") (f (quote ("tcp" "os-poll"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0m63095bz3d0gmng4rsq2yf7jx5kwfi72fd33i1r579vm7wgngcb")))

