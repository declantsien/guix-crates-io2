(define-module (crates-io #{3}# f fsr) #:use-module (crates-io))

(define-public crate-fsr-0.1.0 (c (n "fsr") (v "0.1.0") (d (list (d (n "ash") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.0") (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "1983wsiia1zs02fzxb74x8i37h421w3bj5ixkcln3gj7vxch0a0d") (f (quote (("default")))) (y #t) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.1 (c (n "fsr") (v "0.1.1") (d (list (d (n "ash") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.1") (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "0dlfxpd7lq8glw7r9k2n2zx4qspbizw4i9vhygrlnizp8y1y504f") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.2 (c (n "fsr") (v "0.1.2") (d (list (d (n "ash") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.2") (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "1q4g0fd224f7qfc6sq3ps31ab0p1fj1zdqa2gql0q3ar9cna7bw5") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.3 (c (n "fsr") (v "0.1.3") (d (list (d (n "ash") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.3") (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "05mgby6fz1x6d4f668rx84wka8hkhq8qip7i2hjyql3bv4f07y95") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.4 (c (n "fsr") (v "0.1.4") (d (list (d (n "ash") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.4") (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "0lv35n8x0ywh6y32vvhlbahqaw3mx0930p07622b78p5q19ndsxd") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.5 (c (n "fsr") (v "0.1.5") (d (list (d (n "ash") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.5") (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "0af1hr80d90mb7mzkrjchwmwpj0g89kkz8b0403q94pj7vqprmna") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.6 (c (n "fsr") (v "0.1.6") (d (list (d (n "ash") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.6") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "0xbjh4p28bxjvnjsr1v2qiqlwnip9x0nqawn6wwjzkn9z1dva8jm") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.7 (c (n "fsr") (v "0.1.7") (d (list (d (n "ash") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.7") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "0rnnmfrv660l3m952pq0vgay2pam00wvhh3skw8ig9iigbpdfgcc") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.8 (c (n "fsr") (v "0.1.8") (d (list (d (n "ash") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.8") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "0dxqx311c9c51vsshapdl51gvdqr9j9bqgl098if7knv4f6i8wbi") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.9 (c (n "fsr") (v "0.1.9") (d (list (d (n "ash") (r "^0.37") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.9") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "0ghdd4w9ypmf825q3vq18pa4vnq7pswk8r388bh4c89bmlnpjxyz") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.10 (c (n "fsr") (v "0.1.10") (d (list (d (n "ash") (r "^0.38") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.10") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "061yb3qq9n81asnlw3rbnz813798hwbi4qj2sml3yb1r4xw4pp1w") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

(define-public crate-fsr-0.1.11 (c (n "fsr") (v "0.1.11") (d (list (d (n "ash") (r "^0.38") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^2.4") (d #t) (k 0)) (d (n "fsr-sys") (r "^0.1.10") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "windows") (r "^0.51") (f (quote ("Win32_Graphics_Direct3D12"))) (o #t) (d #t) (k 0)))) (h "1rhd6rln68zz5a2anmdblxspmmb8b68hrbdcsqnp9alxyl0sf8m0") (f (quote (("default")))) (s 2) (e (quote (("vulkan" "fsr-sys/vulkan" "dep:ash") ("d3d12" "fsr-sys/d3d12" "dep:windows"))))))

