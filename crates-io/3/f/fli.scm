(define-module (crates-io #{3}# f fli) #:use-module (crates-io))

(define-public crate-fli-0.0.1 (c (n "fli") (v "0.0.1") (h "1j2zmy80g3db5wjhbqrxzy23fc2xilpnj15b4fdy68gy30j48jw0")))

(define-public crate-fli-0.0.2 (c (n "fli") (v "0.0.2") (h "0livpbvmcqrwybi1mvp7vhhw3y6hlv5lckdf5kv6fnbs9mb031kr")))

(define-public crate-fli-0.0.3 (c (n "fli") (v "0.0.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1ssjylf1712lb0l833hib7ykb5fz27dv0xc2nfkm5ckg7xqb36hd")))

(define-public crate-fli-0.0.4 (c (n "fli") (v "0.0.4") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1dx8lxsrrjjdm9b6ipfz5ihjfbdi0agsiya50bz192h6qhd1lm3q")))

(define-public crate-fli-0.0.6 (c (n "fli") (v "0.0.6") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0az02zrj7002g2ig19qv76vk1rff0nxq1j9lr90f8wbmba0s8sf5")))

