(define-module (crates-io #{3}# f f01) #:use-module (crates-io))

(define-public crate-f01-0.1.0 (c (n "f01") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.158") (o #t) (d #t) (k 0)))) (h "1kchmv7h162d5rap3mwawq2g5kfyp4q7namiakj39rq53lsfp0c4")))

(define-public crate-f01-0.1.1 (c (n "f01") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (o #t) (d #t) (k 0)))) (h "1mrw0a5a3rhn2vx8ipm1pbjhf7fmvbr5zjc8qb0sygvk1d7nl1x2")))

