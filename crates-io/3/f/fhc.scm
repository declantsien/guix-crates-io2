(define-module (crates-io #{3}# f fhc) #:use-module (crates-io))

(define-public crate-fhc-0.1.0 (c (n "fhc") (v "0.1.0") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0qi531bzfpj3y44kjvg6j8nh89cf0syv4501z309r61f3giim66z")))

(define-public crate-fhc-0.2.1 (c (n "fhc") (v "0.2.1") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0f2r89gd1hgkjfzzb5d8v5lgfid6bcd5ghls73jh2iay2dw8b0xg")))

(define-public crate-fhc-0.2.2 (c (n "fhc") (v "0.2.2") (d (list (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0hf1sy0h2zg77f6zf83wi72mb42q3f6wcrd2ysmc6xgcidllq6pn")))

(define-public crate-fhc-0.3.0 (c (n "fhc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.13") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0sys87mzn3i1i45nidi5y4b0q0n5gaskjw8r42vgs6kwn7ycgj11")))

(define-public crate-fhc-0.4.0 (c (n "fhc") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1fi3vv0l17cyjnqw12bn5slfsmxjflmpaxi6v5ng13l804jqs12s")))

(define-public crate-fhc-0.4.1 (c (n "fhc") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "07fql1nw3flqjji3ywh8wlbr89xrfmpn9nqrypc7grsmd4qr4i9i")))

(define-public crate-fhc-0.4.2 (c (n "fhc") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "055vpscmalk8hdzzirlsjc67s6vgbyh2fff5f7f4xfzn7mhvyblw")))

(define-public crate-fhc-0.5.0 (c (n "fhc") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "06837dilvkwryp87gx48m9r508kgq6izg96k75b6cpxwxxa6hw0p")))

(define-public crate-fhc-0.5.1 (c (n "fhc") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "0mhzngbv3xsy96n4c3rxsqw5asw01fbb23ffdlycqdzz80vr1fs3")))

(define-public crate-fhc-0.5.2 (c (n "fhc") (v "0.5.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "00rgkba66bn0lgl6yr8q1rqfswndnw2495h6rgrjvzjvlica08nq")))

(define-public crate-fhc-0.5.3 (c (n "fhc") (v "0.5.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "1d0vc7psycd0dqyjr4xsnjsrl0jf4g2vz3bplc2dbfnd8j8pv3k0")))

(define-public crate-fhc-0.6.0 (c (n "fhc") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "blake3") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "134pxh3gxb7zsy1mikv4v40m4g950l4hbk1fbxfs1v4r47j6a96m")))

