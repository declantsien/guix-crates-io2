(define-module (crates-io #{3}# f flp) #:use-module (crates-io))

(define-public crate-flp-0.1.0 (c (n "flp") (v "0.1.0") (h "1jgc2d19hqf94vw5s6c9ajx8yih7wzfay03v8bnzm585n371pwcb")))

(define-public crate-flp-0.1.1 (c (n "flp") (v "0.1.1") (h "1dkpbqs2avxvfyb4nqs8249mbc3mdg9dfv4y0vyp0bwflim2npx4")))

