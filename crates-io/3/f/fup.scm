(define-module (crates-io #{3}# f fup) #:use-module (crates-io))

(define-public crate-fup-0.7.2 (c (n "fup") (v "0.7.2") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "02mm806sx150vd263029cgdpl41b0076ksy4naigcagiyyn7d31g")))

(define-public crate-fup-0.7.3 (c (n "fup") (v "0.7.3") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1k5f73aszrip1ax21klvvw9g12kq8r04siyabjc7n4xjpi1jvwlz")))

(define-public crate-fup-0.7.4 (c (n "fup") (v "0.7.4") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0161cn8p7g0hlsb91hwp2zgkvlqb3qvd9l9ynbvy4v0m9ys5w6w4")))

(define-public crate-fup-0.7.5 (c (n "fup") (v "0.7.5") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "08wl4a6xya7yq4k5i6mazxig1n4dnnxvkwwq7n5fkmmzfcn8cvmz")))

(define-public crate-fup-0.7.6 (c (n "fup") (v "0.7.6") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2.14") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0avfhpff9majzg8n6hca59dmnq3v1wr32f46hir47dh6y8pklnmh")))

(define-public crate-fup-0.7.7 (c (n "fup") (v "0.7.7") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.2.14") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1b5qg177385w0k2nxc4vz6jdrvc6wwwc3rp86sxrjbyn14vzjqcg")))

