(define-module (crates-io #{3}# f fsb) #:use-module (crates-io))

(define-public crate-fsb-0.0.0 (c (n "fsb") (v "0.0.0") (h "0iwshdzbr0fj6vh6ynb38ilrnh965ahy6pw6hr76493zfpyrhxfc") (y #t)))

(define-public crate-fsb-0.0.1 (c (n "fsb") (v "0.0.1") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "whirlpool") (r "^0.9") (k 0)))) (h "0j0c77c2mhw97h52a47mrig83lj7bb5grjw969ngkfhmlg1fw8vv") (f (quote (("std" "whirlpool/std") ("asm" "whirlpool/asm")))) (y #t)))

(define-public crate-fsb-0.0.2 (c (n "fsb") (v "0.0.2") (d (list (d (n "block-buffer") (r "^0.9") (f (quote ("block-padding"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)) (d (n "whirlpool") (r "^0.9") (k 0)))) (h "15vykpajza5c4v1kzsjk3z9bb17faf6bx0rx18b3ddbqs06d66wf") (f (quote (("std" "whirlpool/std") ("asm" "whirlpool/asm")))) (y #t)))

(define-public crate-fsb-0.1.0 (c (n "fsb") (v "0.1.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "whirlpool") (r "^0.10") (k 0)))) (h "1niyy9mdsawn7bz9d8nnwbacjavgbs4a6m58m4vq89wl7d1ihlh0") (f (quote (("std" "digest/std") ("default" "std")))) (y #t)))

(define-public crate-fsb-0.1.1 (c (n "fsb") (v "0.1.1") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "whirlpool") (r "^0.10.1") (k 0)))) (h "07bk4iz0b5rd4l4zafzl09ynzk2j7v5snxsi3wqi29hlsg2f0yhi") (f (quote (("std" "digest/std") ("default" "std")))) (y #t)))

(define-public crate-fsb-0.1.2 (c (n "fsb") (v "0.1.2") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "whirlpool") (r "^0.10.1") (k 0)))) (h "1s3m3z4qrf5ca5gvxhbs5mcnjklddpla59n4g2psd45xnpvkk03s") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-fsb-0.1.3 (c (n "fsb") (v "0.1.3") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)) (d (n "whirlpool") (r "^0.10.1") (k 0)))) (h "1j1pyzm170wi510spzwk687dhqg39w3wnrsc04ningixgr324jp5") (f (quote (("std" "digest/std") ("default" "std"))))))

