(define-module (crates-io #{3}# f fak) #:use-module (crates-io))

(define-public crate-fak-0.1.0 (c (n "fak") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fake") (r "^2.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bch08kpncnlkqviadyvcqkb0vw7xrrszjlc7f38p5sy9bkdj144")))

(define-public crate-fak-0.1.1 (c (n "fak") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fake") (r "^2.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fnxscy7rlqsi4393i4lq35pfn43b9b1xik025k2rwmpjnjv3gv6")))

