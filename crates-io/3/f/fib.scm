(define-module (crates-io #{3}# f fib) #:use-module (crates-io))

(define-public crate-fib-1.0.0 (c (n "fib") (v "1.0.0") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1g7yrgvyzkirqj16fw6k7r3pxiqk74ddm9fs8mk6gc8lnpml9qpv")))

(define-public crate-fib-1.0.1 (c (n "fib") (v "1.0.1") (d (list (d (n "num-bigint") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0a17c206l3gwjadd50wpf0l1i32izshmwycw6pn4b9962hbjqn6z")))

