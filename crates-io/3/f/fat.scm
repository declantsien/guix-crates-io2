(define-module (crates-io #{3}# f fat) #:use-module (crates-io))

(define-public crate-fat-0.0.1 (c (n "fat") (v "0.0.1") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "1gzhjgxkqksvhxdsiv80f1ysc41k9zjwg4sqpdq3v4whw0j49f11")))

(define-public crate-fat-0.0.2 (c (n "fat") (v "0.0.2") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)))) (h "1x4crx5h8fzqpjq1hgyfvhwdpqxsgb76lzpiaxv9q236cc2nrg91")))

