(define-module (crates-io #{3}# f fay) #:use-module (crates-io))

(define-public crate-fay-0.1.0 (c (n "fay") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sk7w3j2kd50zlzp5rjrwxyl1x6pgw1i753665kagg5rsvl1jmv9")))

(define-public crate-fay-0.1.1 (c (n "fay") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "03z9xmk5sx92fk9aczx7v1ykp55h6bhglcq00yjgv0vfhjrvvc0w")))

(define-public crate-fay-0.1.2 (c (n "fay") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ps6p858wclfgg6h4wawzmkn9vyf2m51v04c4g304gqz06flpdkm")))

(define-public crate-fay-0.1.3 (c (n "fay") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1x0aik9g57s7aycz67q3rgjgicww5cid21skl6xdg334rqlpd1vn")))

(define-public crate-fay-0.1.4 (c (n "fay") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "11dkx61pjs7i3jwq0n11vh9ijkmkan12agbi4r30vgfrlc7f8hfp")))

(define-public crate-fay-0.1.5 (c (n "fay") (v "0.1.5") (d (list (d (n "miniserde") (r "^0.1.27") (d #t) (k 0)))) (h "11lkrf3b4ma55ag4d5j4fhwcy5bh074521fzq7sry8z0pz2gvq8c")))

(define-public crate-fay-0.1.6 (c (n "fay") (v "0.1.6") (d (list (d (n "miniserde") (r "^0.1.27") (d #t) (k 0)))) (h "0qhrwaavp1zcn12zsdfsxqxj04z976yz2p5l0pc195r4rcbyx3ix")))

(define-public crate-fay-0.1.7 (c (n "fay") (v "0.1.7") (d (list (d (n "miniserde") (r "^0.1.27") (d #t) (k 0)))) (h "0qg1ayhpv68yrbvq47hghzj66jgc61ppspgky579gm4fsrycwgss")))

