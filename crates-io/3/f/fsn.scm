(define-module (crates-io #{3}# f fsn) #:use-module (crates-io))

(define-public crate-fsn-0.0.0 (c (n "fsn") (v "0.0.0") (h "0xbmf7mnpyk7rqd2pv7slg9p40cdcmcxwf4ivfiq3csc58xyvpwr")))

(define-public crate-fsn-0.1.0 (c (n "fsn") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1nmnblh8wgzf2mdbwnmdr8gf03qkfixn5bj0vr5x4z633niq30mz")))

(define-public crate-fsn-0.1.1 (c (n "fsn") (v "0.1.1") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1ci35gnfmywhnwizc4imlh6nl1cv38vka7s1wwslcb2ixq8axs07")))

(define-public crate-fsn-0.1.2 (c (n "fsn") (v "0.1.2") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1jrv7n2h97lwcvb1x34lnja1jivdg4pr2cvpcmgg0ldvrhqc12kf")))

(define-public crate-fsn-0.1.3 (c (n "fsn") (v "0.1.3") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1nphg3bll3aq27l2dc74zlxy29np9spwa8nlqqyp6y267jwqx0gq")))

(define-public crate-fsn-0.1.4 (c (n "fsn") (v "0.1.4") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0aar7sk8vzgv5abp12izj2ca8khki6ymidzxjks5dh4p8d0gqf99")))

