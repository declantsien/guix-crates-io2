(define-module (crates-io #{3}# f ffp) #:use-module (crates-io))

(define-public crate-ffp-1.0.0 (c (n "ffp") (v "1.0.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "15gl6mapqrdqmjkkk21jc8srwmmn04g3cg1wfqv8px7fzqhidy6f")))

(define-public crate-ffp-1.1.0 (c (n "ffp") (v "1.1.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "0v0py8dp2dbv2xsh8q9jf031j77fh859q31zldakvjppkqwq45sp")))

(define-public crate-ffp-1.1.1 (c (n "ffp") (v "1.1.1") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "1rp77k80i6kxvgcggp9bdcrc23zcp69i4jr48nbg6vcbw313j5y7")))

(define-public crate-ffp-1.2.0 (c (n "ffp") (v "1.2.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libusb") (r "^0.3") (d #t) (k 0)))) (h "0kzyx5cyf1qj0qx7r2r0aphs22rggkxrzchk4100ha50vrrz7y5b")))

(define-public crate-ffp-1.3.0 (c (n "ffp") (v "1.3.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "rusb") (r "^0.5.2") (d #t) (k 0)))) (h "1zy1gvhd74wx6canyy925f6nww7rwl6r7kd0sgway9ivmmkclh61")))

