(define-module (crates-io #{3}# f feo) #:use-module (crates-io))

(define-public crate-feo-0.1.0 (c (n "feo") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rounded-div") (r "^0.1.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "12b9adcplb53clixkypch2mcv01j99b59xzh0nfbap13y2g5gx3a")))

(define-public crate-feo-0.1.1 (c (n "feo") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rounded-div") (r "^0.1.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0z333ynk8sg69dkykarn1immwbshyg23341z0sx3s7hdp4562hj9")))

(define-public crate-feo-0.1.2 (c (n "feo") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rounded-div") (r "^0.1.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1fl1jpf51mh01448a4p696n0v7apy5p2r0py5nk9bq38dvpx5rgh")))

(define-public crate-feo-0.1.3 (c (n "feo") (v "0.1.3") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rounded-div") (r "^0.1.2") (d #t) (k 0)) (d (n "sinfo") (r "^0.1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1yaqmsz03wanqvbx9mb3msvn0kjqjjagj9270j135f0ziyng0np7")))

(define-public crate-feo-0.1.4 (c (n "feo") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rounded-div") (r "^0.1.2") (d #t) (k 0)) (d (n "sinfo") (r "^0.1.2") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "10mixs21dslys3qywfb9wlgknwrp667lpy1xb1idxclcav7bwpqr")))

(define-public crate-feo-0.1.5 (c (n "feo") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.30") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rounded-div") (r "^0.1.2") (d #t) (k 0)) (d (n "sinfo") (r "^0.2.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "18x2qmzskqsshy56dspn3pjr6r7f5hnqmmpbhyx8zzmj1q0s71iy")))

