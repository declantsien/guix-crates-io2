(define-module (crates-io #{3}# f fdt) #:use-module (crates-io))

(define-public crate-fdt-0.0.1 (c (n "fdt") (v "0.0.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "memchr") (r "^1.0") (k 0)))) (h "0f6rqrqs9wqlf43g85lxmryxj685a22icwmn9sl3rbq2wbxgaw8b")))

(define-public crate-fdt-0.1.0 (c (n "fdt") (v "0.1.0") (h "08q7zlaam280glicwx66c26x8s9kyf4his01d2x4s9fl523ya6wz")))

(define-public crate-fdt-0.1.1 (c (n "fdt") (v "0.1.1") (h "11hkgyly6rx1f07pnkpscrai0v3ch0yki301yx104xk9kf8mz9kk") (y #t)))

(define-public crate-fdt-0.1.2 (c (n "fdt") (v "0.1.2") (h "0qs7khbdmjhlq8j1kjzgyfkssd61j1hpfpjn15f9jav4x6hb6vxh")))

(define-public crate-fdt-0.1.3 (c (n "fdt") (v "0.1.3") (h "07izmzz45754qiirib7dd0w6gpbssjwjrsfph5mk0j89yxy8ahxn")))

(define-public crate-fdt-0.1.4 (c (n "fdt") (v "0.1.4") (h "18ncayxn1wxgjp16jlrz7fzyg6k30nyb98pf1g59s1jdskn5nkwn")))

(define-public crate-fdt-0.1.5 (c (n "fdt") (v "0.1.5") (h "0rsylhpzzh0yyfnk4wp4vh3ivljriwwras7k9ah6fqnw4bvlsjkq") (f (quote (("pretty-printing"))))))

