(define-module (crates-io #{3}# f fwm) #:use-module (crates-io))

(define-public crate-fwm-0.1.0 (c (n "fwm") (v "0.1.0") (d (list (d (n "gdk") (r "^0.13.2") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "gio") (r "^0.9.1") (f (quote ("v2_44"))) (d #t) (k 0)) (d (n "gtk") (r "^0.9.2") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1bj2yxz79zg0y6x7nqkjfvb4n51zwfql0lm628w4iw1qbizmn3p8")))

