(define-module (crates-io #{3}# f fns) #:use-module (crates-io))

(define-public crate-fns-0.0.1 (c (n "fns") (v "0.0.1") (h "01cjj5zshl983w31ay19i6pbf5w0dlqrj0x923zh2ap5dx5njdkh") (y #t)))

(define-public crate-fns-0.0.2 (c (n "fns") (v "0.0.2") (h "0pnridqwkzj12n14w6v8vzlbk9mmwj3ib8l0n3x7p0asqi3rs6w8") (y #t)))

(define-public crate-fns-0.0.3 (c (n "fns") (v "0.0.3") (h "0fmsxpci8aymd5hmfhzdk8lhgd7na78k76ydd09y59i74d1y40jh") (y #t)))

(define-public crate-fns-0.0.4 (c (n "fns") (v "0.0.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05p3qxakyld5l2lr83fdnxhx1m6fb6pacq8wp31n1aiwv53zlma5") (y #t)))

(define-public crate-fns-0.0.5 (c (n "fns") (v "0.0.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1mnm0gm3g35pi2bpqsq8dc2zzwiq25nrx8wjbqjy4zmkm4rmgl8c") (y #t)))

(define-public crate-fns-0.0.6 (c (n "fns") (v "0.0.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0p052jwaljxq7la8f7m1xmgcszns3ixv66jhrl44xrwh0mn642dy") (y #t)))

(define-public crate-fns-0.0.7 (c (n "fns") (v "0.0.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0s63h1zi7vfm99pcg856bs8mlrpby6kfhj5b5lasrzk85bw1ilzr")))

