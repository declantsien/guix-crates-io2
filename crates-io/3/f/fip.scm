(define-module (crates-io #{3}# f fip) #:use-module (crates-io))

(define-public crate-fip-1.0.0 (c (n "fip") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)))) (h "0344ff5a2swsn72mxb8a9wh25g9z62qvfqlhbmm141jaxc1nh9l6")))

(define-public crate-fip-1.0.1 (c (n "fip") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)))) (h "1hf9zll1q2r5vz8h5bzqf1hrb0a9p1dn7x60ghj2n824x9ll55d9")))

(define-public crate-fip-1.0.2 (c (n "fip") (v "1.0.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)))) (h "0cjmqqsbpzzjc1cxs99fd5xvfbynkbyaqk3rad5wgnkna8lcixwb")))

