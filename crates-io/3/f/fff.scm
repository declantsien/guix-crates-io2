(define-module (crates-io #{3}# f fff) #:use-module (crates-io))

(define-public crate-fff-0.2.0 (c (n "fff") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "fff_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "1xyjq68xln4mqsz598q5b5rnr087224bnn367f7i3jdy4180bazl") (f (quote (("derive" "fff_derive") ("default"))))))

(define-public crate-fff-0.2.1 (c (n "fff") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "fff_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "15sw4fkr6jynk9qm4i6lx1bjh95wsj2mz8yc37g4j3fcyj5bx7cg") (f (quote (("derive" "fff_derive") ("default"))))))

(define-public crate-fff-0.2.2 (c (n "fff") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 1)) (d (n "fff_derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0frc1qgjzga8gv5mh0q1w5jzr91pvp5qvpii62q4pq1kxiqfvdxy") (f (quote (("derive" "fff_derive") ("default"))))))

(define-public crate-fff-0.2.3 (c (n "fff") (v "0.2.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 1)) (d (n "fff_derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)))) (h "0cv1cxfnvb0fmvy7zgkfrg7lvs25d0ydz9visc1xh2w7nhjx6i15") (f (quote (("derive" "fff_derive") ("default"))))))

(define-public crate-fff-0.3.0 (c (n "fff") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 1)) (d (n "fff_derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "1fylngy4d63mq9mazkd2n4xlr5qbn5fjpqxklihxcvlj2ddkf28y") (f (quote (("derive" "fff_derive") ("default")))) (y #t)))

(define-public crate-fff-0.3.1 (c (n "fff") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0.50") (d #t) (t "cfg(target_arch = \"x86_64\")") (k 1)) (d (n "fff_derive") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)))) (h "0yrzdcn9yvlhb8a7bz0rfxgsfqya2c028rr1ipm4vbwv4ndvmnvy") (f (quote (("derive" "fff_derive") ("default"))))))

