(define-module (crates-io #{3}# f fpp) #:use-module (crates-io))

(define-public crate-fpp-0.1.0 (c (n "fpp") (v "0.1.0") (d (list (d (n "default-net") (r "^0.4.0") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04k2zxkmb676jn8ac3sgjgah9vicj5m78iddly4na05p33cp07z0") (y #t)))

