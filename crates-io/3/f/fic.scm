(define-module (crates-io #{3}# f fic) #:use-module (crates-io))

(define-public crate-fic-0.0.1 (c (n "fic") (v "0.0.1") (h "0nnyviiq2q3zvcd8sa5xwhizd06nddbn7qvhd7cmr0vah3h8j3z3") (y #t)))

(define-public crate-fic-0.0.2 (c (n "fic") (v "0.0.2") (h "1ci4ag636jvmg2xd4zc04basa5czjk939hpznlk7drhsmc2r4vf2")))

