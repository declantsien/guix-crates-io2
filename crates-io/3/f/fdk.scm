(define-module (crates-io #{3}# f fdk) #:use-module (crates-io))

(define-public crate-fdk-0.1.0 (c (n "fdk") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1pdg6w0ii00wklc0waadcmd5jcphpxzf3xlkz6d7xvvhf9w089c8")))

(define-public crate-fdk-0.1.1 (c (n "fdk") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0bbi8591gvndqy6hgscy2pjavrlpnm1pb29rz9znnyslzvhldv8v")))

(define-public crate-fdk-0.1.2 (c (n "fdk") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0sl5z8rnrwahgqams0ynz1hls1v1gx3g528py15i1gkxqdjs9gsb")))

