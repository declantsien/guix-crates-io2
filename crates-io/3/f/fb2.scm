(define-module (crates-io #{3}# f fb2) #:use-module (crates-io))

(define-public crate-fb2-0.1.0 (c (n "fb2") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0jz24q66f935jw9rz9jixb1r9176yjidy5a7ksbpv4gqfwxybi37")))

(define-public crate-fb2-0.2.0 (c (n "fb2") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0vw5ydxx8wyrir6bkqj8bhl9irc44vqr89nsql4b9r5acyzs15zr")))

(define-public crate-fb2-0.2.1 (c (n "fb2") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1qr46p1xjk213ai4df8960j73vavck081x31d43faw7h54fjb27n")))

(define-public crate-fb2-0.2.2 (c (n "fb2") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0fizsk8yp50lq31gsxlwv0rxwa6w1zfn7f7xdyfldip8plmak12h")))

(define-public crate-fb2-0.3.0 (c (n "fb2") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "14bq55r1wi4m1v5hma1faqq1lr3h76hq29maxw1v36aqf3p4d4qx")))

(define-public crate-fb2-0.4.0 (c (n "fb2") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bd5cjipb5db7zvbr8ipdv7dnxi3k5wc0ap8n33v0l71wcakgxn9")))

(define-public crate-fb2-0.4.1 (c (n "fb2") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vy3d3gf81xzv6ck0x3l3yrgnny8pqzsfw0ca8k9xmmcwqps4fzd")))

(define-public crate-fb2-0.4.2 (c (n "fb2") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02s4jg41f4i34320nxddy4ar5zik84087rmx5f442k4lkkljwy5m")))

(define-public crate-fb2-0.4.3 (c (n "fb2") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x1g6vyb4c94qixhz6xshi13vhv9g3njwwkix6cbf4zdap13nf5q")))

(define-public crate-fb2-0.4.4 (c (n "fb2") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ykyx1hhzrcfcqcy88lr14b874h0j4bbdlqmg3svwy41myzpplpj")))

