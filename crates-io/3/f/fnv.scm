(define-module (crates-io #{3}# f fnv) #:use-module (crates-io))

(define-public crate-fnv-1.0.0 (c (n "fnv") (v "1.0.0") (h "0k6p34h7mhqivd4k2kpbnwhvxbjvl8lzryn1iz03g221s1lzgi93")))

(define-public crate-fnv-1.0.1 (c (n "fnv") (v "1.0.1") (h "0p1z5bxs57aawnf6y3b445mmyn8an24ly3iyfysqp0r9wi07aikv")))

(define-public crate-fnv-1.0.2 (c (n "fnv") (v "1.0.2") (h "03wmika8zyhxc4a63sdqahh2lynwccf5n8q1ly7yps8fhrpdi1k9")))

(define-public crate-fnv-1.0.3 (c (n "fnv") (v "1.0.3") (h "0c6d8cjknrjqrh29lj9rcysqcifgvhgjv33x9d809kx1b9fjim6k")))

(define-public crate-fnv-1.0.4 (c (n "fnv") (v "1.0.4") (h "0pisl5lgpsky61j8kzksvxsvym5lks7lq4dd1j8w9c4a82szg2lf")))

(define-public crate-fnv-1.0.5 (c (n "fnv") (v "1.0.5") (h "0i73zsc7q1hapamwfv793k3xhan11jb9ylkgypx88a0y5y289i3c")))

(define-public crate-fnv-1.0.6 (c (n "fnv") (v "1.0.6") (h "1ww56bi1r5b8id3ns9j3qxbi7w5h005rzhiryy0zi9h97raqbb9g")))

(define-public crate-fnv-1.0.7 (c (n "fnv") (v "1.0.7") (h "1hc2mcqha06aibcaza94vbi81j6pr9a1bbxrxjfhc91zin8yr7iz") (f (quote (("std") ("default" "std"))))))

