(define-module (crates-io #{3}# f fly) #:use-module (crates-io))

(define-public crate-fly-0.1.0 (c (n "fly") (v "0.1.0") (h "1s004rf5s7yhw5nd7mdh8g2bvsyx9yl14j9lfnx9c7klqa04km76")))

(define-public crate-fly-0.1.1 (c (n "fly") (v "0.1.1") (h "03ca1gff9vcbacbsh3js1087vl4jpyl4yb13k8rmlh5za6pwdd47")))

(define-public crate-fly-0.2.0 (c (n "fly") (v "0.2.0") (h "18sqbsdzhk7qa7pa54q6fqw5pd5b4sbg4dyfax959m6l7jpbwagz")))

