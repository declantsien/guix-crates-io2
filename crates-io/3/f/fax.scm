(define-module (crates-io #{3}# f fax) #:use-module (crates-io))

(define-public crate-fax-0.1.0 (c (n "fax") (v "0.1.0") (d (list (d (n "fax_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0xb35dd2sn3k22gfwc429dzvsvk6lqkbd04ih95asf0cncmfp7i9")))

(define-public crate-fax-0.1.1 (c (n "fax") (v "0.1.1") (d (list (d (n "fax_derive") (r "^0.1.0") (d #t) (k 0)))) (h "13msqfyv6kwapwia1hykizv3shx4ffly06h2mp719kzv41r32a5y")))

(define-public crate-fax-0.2.0 (c (n "fax") (v "0.2.0") (d (list (d (n "fax_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0maw7qsphh4lzymgc11jdzlp176rwkyv7vg57lpnrh43frww3kmj")))

(define-public crate-fax-0.2.1 (c (n "fax") (v "0.2.1") (d (list (d (n "fax_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0whnb74q982k115yhngh6gs519j1cx0ag1b24zsscl6ajgrk57ca")))

(define-public crate-fax-0.2.2 (c (n "fax") (v "0.2.2") (d (list (d (n "fax_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0j9lwbcjj9l9sfbhajll3sk8ky4s16k60n1fmxac5286cq2v1wbk")))

(define-public crate-fax-0.2.3 (c (n "fax") (v "0.2.3") (d (list (d (n "fax_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0x6i8fkdlw5wjyp9gaaffff6p4yia7bizvm46kf3p57k9ic28s0v") (f (quote (("debug"))))))

(define-public crate-fax-0.2.4 (c (n "fax") (v "0.2.4") (d (list (d (n "fax_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0xzx5z1g4lh9djhadrk8n1di2ngz4d6skc6jz5743bvi1snk6gmh") (f (quote (("debug"))))))

