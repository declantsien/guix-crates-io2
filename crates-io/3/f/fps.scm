(define-module (crates-io #{3}# f fps) #:use-module (crates-io))

(define-public crate-FPS-0.0.0 (c (n "FPS") (v "0.0.0") (d (list (d (n "ilog2") (r "*") (d #t) (k 0)))) (h "11l8rbmix7kj30364128ackycjbc2p6zc697r0wp634pfin23f58")))

(define-public crate-FPS-0.0.1 (c (n "FPS") (v "0.0.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "time") (r "*") (d #t) (k 0)))) (h "02mlwjrscavj3nxzp1gdjcfjrwdcklaj19r2j26bmjgrpyhvmv24")))

(define-public crate-FPS-0.0.2 (c (n "FPS") (v "0.0.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "millefeuille") (r "^0.0.0") (d #t) (k 0)))) (h "1bnf8d0ayibqxcp271jixl3j4swn2hgrr4na4m54r7vd3sy9phsq")))

(define-public crate-FPS-0.0.3 (c (n "FPS") (v "0.0.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "millefeuille") (r "^0.0.0") (d #t) (k 0)))) (h "1d0fwbh9pwfafz7w4k7bwpfba2bqw9viimh1g460l7g1985nyddf")))

