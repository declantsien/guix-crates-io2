(define-module (crates-io #{3}# f feu) #:use-module (crates-io))

(define-public crate-feu-0.2.0 (c (n "feu") (v "0.2.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "iced") (r "^0.4.2") (d #t) (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "02d9i7z8ngykg6c4c0hx0kyasimibb2p5909m6jj5yx8nx4m1n7c")))

(define-public crate-feu-0.2.1 (c (n "feu") (v "0.2.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "iced") (r "^0.4.2") (d #t) (k 0)) (d (n "iced_native") (r "^0.5.1") (d #t) (k 0)) (d (n "miniserde") (r "^0.1.24") (d #t) (k 0)))) (h "06sz833pd1mgrq7dmf61p6mgpfgyxxjghymq5g034qw4vyp8qj1v")))

