(define-module (crates-io #{3}# f fut) #:use-module (crates-io))

(define-public crate-fut-0.1.0 (c (n "fut") (v "0.1.0") (h "1w0460ypb4lhwifzjwcmmw28nvvjk7w5miw8hcmqnmxrvrfclcfh")))

(define-public crate-fut-0.1.1 (c (n "fut") (v "0.1.1") (h "02wysmc8m9k6ac68pynsk7chyprd2m57y5nnnlyy9h176zpw6mbh")))

(define-public crate-fut-0.1.2 (c (n "fut") (v "0.1.2") (h "0lprigcdgkcp70l379x82s7cjx0hjpl02vhb444pwwk183qay4k2")))

(define-public crate-fut-0.1.3 (c (n "fut") (v "0.1.3") (h "0l64aw8may4qig0blc00pz9hp6h2hcr784fz8ya65ny63p1zph97")))

