(define-module (crates-io #{3}# f fud) #:use-module (crates-io))

(define-public crate-fud-0.0.1 (c (n "fud") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "camino") (r "^1.1.6") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.103.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (f (quote ("termcolor" "atty"))) (k 0)) (d (n "figment") (r "^0.10.12") (f (quote ("toml"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (f (quote ("camino"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ncq3qndl7k046mrd77jk0ir7d0h3sa7536nagk3ykxswc0vyx17")))

(define-public crate-fud-0.0.2 (c (n "fud") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fud-core") (r "^0.0.2") (d #t) (k 0)))) (h "163m15qa05ky5b8rmqh37nxkfdv396jsmrvi91bb4bhka9wjbpvp")))

