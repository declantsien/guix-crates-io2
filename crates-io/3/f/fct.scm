(define-module (crates-io #{3}# f fct) #:use-module (crates-io))

(define-public crate-fct-0.1.0 (c (n "fct") (v "0.1.0") (h "1da75hgn5q2h4saasxmqyy561sx8zbyz9dr1973yxj3lr6q4s7d7") (y #t)))

(define-public crate-fct-0.1.1 (c (n "fct") (v "0.1.1") (d (list (d (n "atty") (r ">=0.2.14, <0.3.0") (d #t) (k 0)) (d (n "big-rational-str") (r ">=0.1.3, <0.2.0") (d #t) (k 0)) (d (n "dirs") (r ">=3.0.1, <4.0.0") (d #t) (k 0)) (d (n "lalrpop") (r ">=0.19.1, <0.20.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r ">=0.19.1, <0.20.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "num") (r ">=0.3.0, <0.4.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.14, <0.4.0") (d #t) (k 0)) (d (n "term") (r ">=0.6.1, <0.7.0") (d #t) (k 0)))) (h "0im3vfir59b2jx2k4cfbsllqsw4y94zv2aqwxbn7qrc7w9f66cv3") (y #t)))

(define-public crate-fct-0.2.0 (c (n "fct") (v "0.2.0") (h "0p3nhdl0vmy2ljhqqzdcbjpmvbb5yn16i6xpfv4byz1207p224fl")))

