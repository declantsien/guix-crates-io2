(define-module (crates-io #{3}# f few) #:use-module (crates-io))

(define-public crate-few-0.0.1 (c (n "few") (v "0.0.1") (h "1i133glpz2hpad2h0pq2xir7d7gi9lc0ff0sc8rbicxlrmjgskxc") (f (quote (("default"))))))

(define-public crate-few-0.1.0 (c (n "few") (v "0.1.0") (h "1d627yb1apc5kirfpnbf3fizj2kg12br28l0mzvqcd6fp57hdyv0") (f (quote (("default"))))))

(define-public crate-few-0.1.1 (c (n "few") (v "0.1.1") (h "15fbi867k4mijgf3i6pjnlpv17yql5x8jgh8xqx3qx62cyj3gdcn") (f (quote (("default"))))))

(define-public crate-few-0.1.2 (c (n "few") (v "0.1.2") (h "04lwjvlk9z3zsb3pcqz3xlcz845bg9frsp6jk2gipaivr1rypv2v") (f (quote (("default"))))))

(define-public crate-few-0.1.3 (c (n "few") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pjr746w41wxwwvr7qlwpsmbz7j98hlc4hw8kjkrhq5dsjd1axkg") (f (quote (("default"))))))

(define-public crate-few-0.1.4 (c (n "few") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0py1iar47hgk9z94qbahwx43kz1110191y2j53yx6mg2axilpkxg") (f (quote (("default"))))))

(define-public crate-few-0.1.5 (c (n "few") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "002blrpcp0w7amnv1nz92kd1r058as0nz5l07bpznp2snrvm23lj") (f (quote (("default"))))))

