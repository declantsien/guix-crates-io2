(define-module (crates-io #{3}# f fap) #:use-module (crates-io))

(define-public crate-fap-0.1.0 (c (n "fap") (v "0.1.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "aprs") (r "^0.2.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0m7q5vcxbzm1cn7wl54hc5xgj4nhp2y1pk04ap44h2q63nyv3lqz")))

(define-public crate-fap-0.2.1 (c (n "fap") (v "0.2.1") (d (list (d (n "approx") (r "^0.2") (d #t) (k 2)) (d (n "aprs") (r "^0.3") (d #t) (k 0)) (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "humantime") (r "^1") (d #t) (k 0)))) (h "0kk9mrx6wj3mmvqs4c8dd4ysqnzhs17p7pw3l4s8nl35iakh3w0x")))

