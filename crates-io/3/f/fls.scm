(define-module (crates-io #{3}# f fls) #:use-module (crates-io))

(define-public crate-fls-0.1.0 (c (n "fls") (v "0.1.0") (d (list (d (n "compiler_builtins") (r "^0.1") (f (quote ("mem"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tinyvec") (r "^1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)) (d (n "veneer") (r "^0.1") (d #t) (k 0)))) (h "1ic5628gipkj63b1c5k9sy10wxafd2cwsp99c1cwkj45n86r2brh")))

