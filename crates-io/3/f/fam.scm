(define-module (crates-io #{3}# f fam) #:use-module (crates-io))

(define-public crate-fam-0.1.0 (c (n "fam") (v "0.1.0") (d (list (d (n "handlebars") (r "^4.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "1xzv9yg2p78gq54wahbrric00pxml0f3gw3y2p888bn56h16pnpb")))

(define-public crate-fam-0.1.1 (c (n "fam") (v "0.1.1") (d (list (d (n "handlebars") (r "^4.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "0qzh1rszqqff02b9mfn3qys395zskwz7i2njw2bjwqvasd62hcm4")))

(define-public crate-fam-0.1.2 (c (n "fam") (v "0.1.2") (d (list (d (n "handlebars") (r "^4.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "1bpgfa3ycwzncb0w1az4rxjivdd0b7a9gxrx4i8d0knip8fjbgw2")))

(define-public crate-fam-0.1.3 (c (n "fam") (v "0.1.3") (d (list (d (n "handlebars") (r "^4.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "10p396063pwc02wn16y0i1k9zagpcfvxx1la0cmlcf5c4prs49gl")))

(define-public crate-fam-0.1.4 (c (n "fam") (v "0.1.4") (d (list (d (n "handlebars") (r "^4.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "1a86mpfc83vklfaag4h0dy9fr9s2g84r476s5grgv8labw5cv3l3")))

(define-public crate-fam-0.1.5 (c (n "fam") (v "0.1.5") (d (list (d (n "handlebars") (r "^4.3.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "0qcfb860bvsbbzinmmjcwxm5ykl4kyv84dhnkk7sz4p9av28vs8y")))

