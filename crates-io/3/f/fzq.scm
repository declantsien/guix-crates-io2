(define-module (crates-io #{3}# f fzq) #:use-module (crates-io))

(define-public crate-fzq-0.1.0 (c (n "fzq") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1.0.0") (d #t) (k 2)) (d (n "strsim") (r "^0.9.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1jm6xf9l1jsrcnw2mfnypdj2hjkcr8b41r54w9xhjjwwb87j2vwg")))

