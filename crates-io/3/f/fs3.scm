(define-module (crates-io #{3}# f fs3) #:use-module (crates-io))

(define-public crate-fs3-0.4.4 (c (n "fs3") (v "0.4.4") (d (list (d (n "libc") (r "^0.2.49") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processthreadsapi" "winerror" "fileapi" "winbase" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "05157y736sh8a6qikipanipl72xm3x265qhlxhlsbxxbs394nhg1")))

(define-public crate-fs3-0.5.0 (c (n "fs3") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.49") (d #t) (t "cfg(unix)") (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("handleapi" "processthreadsapi" "winerror" "fileapi" "winbase" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "140ac0xmp7xz0bs4r4w362gly326ax9bcsig6f2j9xq4sxpcy5zv")))

