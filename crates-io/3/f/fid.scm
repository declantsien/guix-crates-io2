(define-module (crates-io #{3}# f fid) #:use-module (crates-io))

(define-public crate-fid-0.1.0 (c (n "fid") (v "0.1.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "175m2aab9avl62j511xq2s86byyi35xjmi60x5fm1lm8wbk447bb")))

(define-public crate-fid-0.1.1 (c (n "fid") (v "0.1.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0m1g5hbz9k8bgsi9wfny9m0mhdzhfm0g99c5cakxmdi0gw7l477k")))

(define-public crate-fid-0.1.2 (c (n "fid") (v "0.1.2") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1f2lrlhmlakf2i1ksxm25m7rlviakh3vajlj77xfwfq56vx6m7d1")))

(define-public crate-fid-0.1.3 (c (n "fid") (v "0.1.3") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0pd2ns5pmh5c8y0gh4046gqwjmnsas3x84ixcglpvlx3p35xa09b")))

(define-public crate-fid-0.1.4 (c (n "fid") (v "0.1.4") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rm3y390gq6nih0m4w3w7awfxiqm79i321jhmf27fv8vb3n96n7r")))

(define-public crate-fid-0.1.5 (c (n "fid") (v "0.1.5") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1inlxf2wam26b65f4q1g0cszjbswh3g3abk0rjhs0rjvxd6812gx")))

(define-public crate-fid-0.1.6 (c (n "fid") (v "0.1.6") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11l7pbpzl1xawk2b68w0p5ykr0adz55wy7i2cjkwisq00jicjhw7")))

(define-public crate-fid-0.1.7 (c (n "fid") (v "0.1.7") (d (list (d (n "bincode") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mim4ga8g9ma7vwqz8lvwknmia0irbxdh5dvg2dpm4vpl40z6km4")))

