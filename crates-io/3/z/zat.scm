(define-module (crates-io #{3}# z zat) #:use-module (crates-io))

(define-public crate-zat-0.1.0 (c (n "zat") (v "0.1.0") (h "0fkz6k03f0nyrwq1nn7cy4nl25ki6ycyvhhnbwhkxvajnnba8dfb")))

(define-public crate-zat-1.0.0 (c (n "zat") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "19ygdb1097aib98nc1cj1sfm14l8mv48agsm2f342cd2am9n599x")))

(define-public crate-zat-1.1.0 (c (n "zat") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)))) (h "1shrdnp1ylzjqbz1xh6bb2120kcqjmqrzram85hyid5ivadaf2kk")))

