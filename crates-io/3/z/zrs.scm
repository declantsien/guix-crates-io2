(define-module (crates-io #{3}# z zrs) #:use-module (crates-io))

(define-public crate-zrs-0.1.0 (c (n "zrs") (v "0.1.0") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "dirs") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1v5g3jg66izidw517bmiwqxmxwcxnm30wp1z6kn3rhlqijq72545")))

(define-public crate-zrs-0.1.1 (c (n "zrs") (v "0.1.1") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "dirs") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1hjh5fi7swklni3nnbsszl4y4r0afl0spw29hn47s7i2dgl74wnm")))

(define-public crate-zrs-0.1.2 (c (n "zrs") (v "0.1.2") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "dirs") (r "^1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1j4m7rdf7dkysr98rmlf6baszv183lp092x8fn3k7jkqsmpd81r9")))

(define-public crate-zrs-0.1.3 (c (n "zrs") (v "0.1.3") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "nix") (r "^0.15") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "15c0pnz5wl2lan12z9llqdlcqgrhfhfk95mnphgwzwzs7z95h2gn")))

(define-public crate-zrs-0.1.4 (c (n "zrs") (v "0.1.4") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "nix") (r "^0.16") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "19xzqb7yw5r2plxfpzqfd0hlyj4g4jn8kn6m907rx8rrwvp3b91g")))

(define-public crate-zrs-0.1.5 (c (n "zrs") (v "0.1.5") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (k 0)) (d (n "nix") (r "^0.16") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "twoway") (r "^0.2") (d #t) (k 0)))) (h "112vdmz5dzggl5d0z9gkmgxx22kg7z3xmxkx1dlpfmrvp5vhl3rh")))

(define-public crate-zrs-0.1.6 (c (n "zrs") (v "0.1.6") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "nix") (r "^0.16") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "twoway") (r "^0.2") (d #t) (k 0)))) (h "1c6771wklzrnpp4c1and7qwdg4162hj3vsdixhd4a1b3nkj1rmqn")))

(define-public crate-zrs-0.1.7 (c (n "zrs") (v "0.1.7") (d (list (d (n "clap") (r "^2") (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (f (quote ("std"))) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "twoway") (r "^0.2") (d #t) (k 0)))) (h "0mpb05bdph9qixwhaxh0vabdnzpv9jsnxsnhwi7wmy7g56mw98h4")))

(define-public crate-zrs-0.1.8 (c (n "zrs") (v "0.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (k 0)) (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "twoway") (r "^0.2") (d #t) (k 0)))) (h "1sffpjryn7zwi6vlxkqh4fyyib1i0gjsyj92xr8i329r2jlshvgf")))

