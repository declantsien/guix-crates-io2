(define-module (crates-io #{3}# z zgc) #:use-module (crates-io))

(define-public crate-zgc-0.0.0 (c (n "zgc") (v "0.0.0") (h "1w9yhc1g6ijr4nz4gg7qkp5nwns51dv27yx871wf3lw2kjv0csrq") (f (quote (("default"))))))

(define-public crate-zgc-0.0.1 (c (n "zgc") (v "0.0.1") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)))) (h "1jfh1fmgr21bi19mw15g5qcpn25z6dl581c2vm67mkhjgiw8yrvp") (f (quote (("default"))))))

