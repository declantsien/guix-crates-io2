(define-module (crates-io #{3}# z zli) #:use-module (crates-io))

(define-public crate-zli-0.0.0 (c (n "zli") (v "0.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "clap_generate") (r "^3.0.0-beta.2") (d #t) (k 1)) (d (n "zmq") (r "^0.9.2") (f (quote ("vendored"))) (d #t) (k 0)))) (h "0vxqyr34l4p7193gr58zphkpzjqrsdh31m76vsn2hqacfq3bajc2")))

