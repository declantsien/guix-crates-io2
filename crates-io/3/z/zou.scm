(define-module (crates-io #{3}# z zou) #:use-module (crates-io))

(define-public crate-zou-0.1.0 (c (n "zou") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.20.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.0") (d #t) (k 0)))) (h "1bbmxlgnsz7fa31ngxmbczry2bcnr1i564jkzg5q5r8y279cvw75")))

