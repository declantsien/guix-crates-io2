(define-module (crates-io #{3}# z z85) #:use-module (crates-io))

(define-public crate-z85-0.1.1 (c (n "z85") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.2.18") (d #t) (k 2)))) (h "1yrm4yqi518mk1kpivq0fpk19z48iqj005wfx4jpfsvwy0jwa4iy")))

(define-public crate-z85-0.1.2 (c (n "z85") (v "0.1.2") (d (list (d (n "quickcheck") (r "^0.2.18") (d #t) (k 2)))) (h "155qdcnrgcv29rghgxnbxgj7k6sjcfxkhqng819bfjbxddlgprxf")))

(define-public crate-z85-0.1.3 (c (n "z85") (v "0.1.3") (d (list (d (n "quickcheck") (r "^0.2.18") (d #t) (k 2)))) (h "15ks7ajjdsad5gb4i3dkxx48yl55pxjv9niwam4y6a0rgcccmbsa")))

(define-public crate-z85-0.1.4 (c (n "z85") (v "0.1.4") (d (list (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)))) (h "0z79vyyv9rm5xyj0b0fh5irmxbcnzvrl0z66l1qhb01hra8p5a3i")))

(define-public crate-z85-1.0.0 (c (n "z85") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "1md8sqw1vlhpq4cn9ygmx59dhd0zk4c5xyhxy137jk3j209iz1l8")))

(define-public crate-z85-1.0.1 (c (n "z85") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)))) (h "0aawl101kcmwbwh65h2naplb3grm2c7k5ph1vwgjxh4fypcg2gkl")))

(define-public crate-z85-2.0.0 (c (n "z85") (v "2.0.0") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "13dvgkdzqxycxjcx2v37id08chmgbs17jzdgvhilzyh3phryc2i8")))

(define-public crate-z85-2.0.1 (c (n "z85") (v "2.0.1") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "0gbkpl8402bf26wwiwz9vy422b4q5w5zs0sffsq9b7fsvr4pjsj5")))

(define-public crate-z85-2.0.2 (c (n "z85") (v "2.0.2") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "0xyb6izk7dkf0rhm6lljl5613xflgr2i2yfl5ga6jk7adlwji077")))

(define-public crate-z85-2.0.3 (c (n "z85") (v "2.0.3") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "0s1ip9wg5hc4h2jq0pa3v9sb2frcsa9qcw0n4lb44yahrvv3n5ry")))

(define-public crate-z85-2.1.0 (c (n "z85") (v "2.1.0") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "0prygk7cd2wb4zmq6sg7bdn6gm32m5qb311im0rbkhxb3bzlzpv5")))

(define-public crate-z85-2.1.1 (c (n "z85") (v "2.1.1") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "0mfwlks9d40bf0f60wjxb2w7x9vbjyh1kbrfrjd6zdz0853ka5pa")))

(define-public crate-z85-2.1.2 (c (n "z85") (v "2.1.2") (d (list (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "0hp84ap73c2q6mbg63rsbdlskdiyrnz1k2xqbvi2jhqv3yv4hff8")))

(define-public crate-z85-2.3.0 (c (n "z85") (v "2.3.0") (d (list (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "smallvec") (r "^1.1.0") (d #t) (k 0)))) (h "0smjrp5jcq8gp9c4ryn4j4k0bygsg08hmn46jqj8g5q3a9ygvxqq")))

(define-public crate-z85-3.0.0 (c (n "z85") (v "3.0.0") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)))) (h "16hpizv3dvj48hixf3yanrpgh3l24prvxl4d9srfnkw0bia9kqqz")))

(define-public crate-z85-3.0.1 (c (n "z85") (v "3.0.1") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)))) (h "0lay86bahwxg2amx4v4294ain2y0cfcpp0rbx8zwjbmdfggsz5gk")))

(define-public crate-z85-3.0.2 (c (n "z85") (v "3.0.2") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)))) (h "1ndbclzjkz2kbkgzvz22hn2gykg38ncawzm1vijxawrgb0krzikm")))

(define-public crate-z85-3.0.3 (c (n "z85") (v "3.0.3") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)))) (h "10979wfzbv2psign9v2w6cil0dj8wjf5x1qjakps81lr9xpbbj2s")))

(define-public crate-z85-3.0.4 (c (n "z85") (v "3.0.4") (d (list (d (n "proptest") (r "^0.10.1") (d #t) (k 2)))) (h "0mpq5932l61kyjmx0knkyf3c045jk5m2fx2znrs0nd41vf9nx2dg")))

(define-public crate-z85-3.0.5 (c (n "z85") (v "3.0.5") (d (list (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1z10407jwvjfzpzaxwxgqsm9vcbyldzzh2qz2b0ijy2h3fprsn9a")))

