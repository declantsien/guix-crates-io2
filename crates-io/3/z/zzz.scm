(define-module (crates-io #{3}# z zzz) #:use-module (crates-io))

(define-public crate-zzz-0.0.0 (c (n "zzz") (v "0.0.0") (h "0qy53psplhvdq6bj9p6bblm3jkci7f538zmgycbkbfir6l4fsv1b")))

(define-public crate-zzz-0.1.0 (c (n "zzz") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "1klvj5xmqvymk7r3117wdjgdpj0x9b4fm0wglp8bxv1lad0yjm96") (f (quote (("streams" "futures-core") ("default"))))))

(define-public crate-zzz-0.1.1 (c (n "zzz") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "0pp4h4ffh9kac92nsnsppjhfsmyy8xbrb50rgq1ng6y77impip21") (f (quote (("streams" "futures-core") ("default"))))))

(define-public crate-zzz-0.1.2 (c (n "zzz") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "0m3s2fncdziwrb0kzyy2pzaqpyvqm1rx85vn5kfrnvn5pr12bcfc") (f (quote (("streams" "futures-core") ("default"))))))

(define-public crate-zzz-0.1.3 (c (n "zzz") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "14vc6akarp6k0mhyxmpiryb33bvgc59173229x58f61jn2hj5gsx") (f (quote (("streams" "futures-core") ("default" "auto-width") ("auto-width" "term_size"))))))

(define-public crate-zzz-0.1.4 (c (n "zzz") (v "0.1.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "0z1l8gnnk5fnp161ycpv65gnpwh8mi1xdhydralvcch1cbrkx9lb") (f (quote (("streams" "futures-core") ("default" "auto-width") ("auto-width" "term_size"))))))

(define-public crate-zzz-0.2.0 (c (n "zzz") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "1lg7v8q8zy5ds3k2wjywlf1kkxkqk2bjg6y2pn32ciyvbypn3vzx") (f (quote (("streams" "futures-core") ("default" "auto-width") ("auto-width" "term_size"))))))

(define-public crate-zzz-0.2.1 (c (n "zzz") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "09k79vj1lsb2nn2gr2jw530zjcq794kn7czgff69h4n4miklhpgw") (f (quote (("streams" "futures-core") ("default" "auto-width") ("auto-width" "term_size"))))))

(define-public crate-zzz-0.3.0 (c (n "zzz") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "1smvw93bqw40lm58jv75cymsg3x6l8gcs8gg36cjc4k6bgq4zib6") (f (quote (("streams" "futures-core") ("default" "auto-width") ("auto-width" "term_size"))))))

(define-public crate-zzz-0.3.1 (c (n "zzz") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "futures-core") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "term_size") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "time"))) (d #t) (k 2)))) (h "0j9z7khp3scafd76q72b2jlmqw4crxmc38nr9z32m5wmg3iqz9qk") (f (quote (("streams" "futures-core") ("default" "auto-width") ("auto-width" "term_size"))))))

