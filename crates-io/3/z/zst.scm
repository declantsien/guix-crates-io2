(define-module (crates-io #{3}# z zst) #:use-module (crates-io))

(define-public crate-zst-0.1.0 (c (n "zst") (v "0.1.0") (d (list (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "const_fn") (r "^0.4.9") (o #t) (d #t) (k 0)) (d (n "const_trait_impl") (r "^0.1.1") (d #t) (k 0)) (d (n "remove_macro_call") (r "^0.1.1") (d #t) (k 0)) (d (n "the_assoc_ty_ext") (r "^0.1.0") (d #t) (k 0)))) (h "07ssghyggzak5liri7phjs8cz6pscsdfjqvg0s62pvlbrm88vrpz") (f (quote (("const_trait_impl") ("const_fn_trait_bound") ("const_default_impls" "const_fn"))))))

(define-public crate-zst-0.1.1 (c (n "zst") (v "0.1.1") (d (list (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "const_fn") (r "^0.4.9") (o #t) (d #t) (k 0)) (d (n "remove_macro_call") (r "^0.1.1") (d #t) (k 0)) (d (n "the_assoc_ty_ext") (r "^0.1.0") (d #t) (k 0)) (d (n "unconst_trait_impl") (r "^0.1.2") (d #t) (k 0)))) (h "0prsia275s55fdqs9r9ygqzba42p9zwp68arizdgmjv9flhld0n6") (f (quote (("const_trait_impl") ("const_fn_trait_bound") ("const_default_impls" "const_fn"))))))

(define-public crate-zst-0.1.2 (c (n "zst") (v "0.1.2") (d (list (d (n "cfg_aliases") (r "^0.1.1") (d #t) (k 1)) (d (n "const_fn") (r "^0.4.9") (o #t) (d #t) (k 0)) (d (n "remove_macro_call") (r "^0.1.1") (d #t) (k 0)) (d (n "the_assoc_ty_ext") (r "^0.1.0") (d #t) (k 0)) (d (n "unconst_trait_impl") (r "^0.1.5") (d #t) (k 0)))) (h "0m5b4j0mxzhl74ixz28jrbbkk1mx1mj6v5qmv4qgynh8b1ra1903") (f (quote (("const_trait_impl") ("const_fn_trait_bound") ("const_default_impls" "const_fn"))))))

