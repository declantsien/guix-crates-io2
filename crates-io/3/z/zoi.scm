(define-module (crates-io #{3}# z zoi) #:use-module (crates-io))

(define-public crate-zoi-0.1.0 (c (n "zoi") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "16kia9l8drcy5sz7g1dz1fqmqb515gam52r2rlhgw19aypvxfbb0")))

(define-public crate-zoi-0.1.1 (c (n "zoi") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1praysgwqz2ppfvm9dijj1c8l1h17najif0py450ck11iwmp3wln")))

(define-public crate-zoi-0.1.2 (c (n "zoi") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.24") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1wq8n27zvnl3f0hkwcjq8qc2mdwlh730wyv17jdzk8ac8lbxvayl")))

