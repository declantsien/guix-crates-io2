(define-module (crates-io #{3}# z z32) #:use-module (crates-io))

(define-public crate-z32-1.0.0 (c (n "z32") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0rrc2qllvj8lp6d8fnrv4c95jlhy493hrwfidmqjr65mcgfr0c0c")))

(define-public crate-z32-1.0.1 (c (n "z32") (v "1.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "176laqpmx8x216yjv28kyl35v6ya9rhfdf4jvzswr525vf2yx9qr")))

(define-public crate-z32-1.0.2 (c (n "z32") (v "1.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10s0w9jclpg6fl2ca026v5kr096h1lkq6y9vm2cr69gpc71jxrpl")))

(define-public crate-z32-1.0.3 (c (n "z32") (v "1.0.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1za5bma657ygkvjj4b4waszjih4b8mgb2dz1m6zw69bxfphw9hm5")))

(define-public crate-z32-1.1.0 (c (n "z32") (v "1.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0jqsj8z09g5npzmlglfl53imjx5in8barni555ji2lhrvjich9i7")))

(define-public crate-z32-1.1.1 (c (n "z32") (v "1.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1p3vifvhgig1bqhghxplshxjw29s3jlp888n10yv0a0w4mk75czd")))

