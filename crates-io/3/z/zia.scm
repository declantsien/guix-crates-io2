(define-module (crates-io #{3}# z zia) #:use-module (crates-io))

(define-public crate-zia-0.1.0 (c (n "zia") (v "0.1.0") (h "1czk9x2k8irczxszxdnla1k4kzdim3lvan1rpj2g1b7aig0r0xyy")))

(define-public crate-zia-0.1.1 (c (n "zia") (v "0.1.1") (h "0wgkvbbi26jw1ycx8lz09cg5pgy63q75y9q2pnjf5ph3zfaf4vb7")))

(define-public crate-zia-0.1.2 (c (n "zia") (v "0.1.2") (h "0y526gz7j906kcd3vjyn5a1pi6xgyy1q773jhrbgj777yifasqmh")))

(define-public crate-zia-0.1.3 (c (n "zia") (v "0.1.3") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "test_zia") (r "^0.1.0") (d #t) (k 2)))) (h "19qadxcgyqlp7rq465zba42q5bc5m5f0zbnx4hcgfyvlxlzkvr8f")))

(define-public crate-zia-0.2.0 (c (n "zia") (v "0.2.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "test_zia") (r "^0.2.0") (d #t) (k 2)))) (h "13ngivjlh7hwjn6lvxm9w2h5jf9yqkzbayqday7vhlxn6jbmwhg5")))

(define-public crate-zia-0.2.1 (c (n "zia") (v "0.2.1") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "test_zia") (r "^0.2.0") (d #t) (k 2)))) (h "0f4206sachb5v27w4iwcfmnzq9xp942q0d6kmkip5plp68yjfddj")))

(define-public crate-zia-0.3.0 (c (n "zia") (v "0.3.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (f (quote ("max_level_debug" "release_max_level_warn"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "test_zia") (r "^0.2.0") (d #t) (k 2)))) (h "1d9b60aqzkdfrv7spq06vxw53ghng815bay72392y9a5gza9r8hf")))

(define-public crate-zia-0.3.1 (c (n "zia") (v "0.3.1") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (f (quote ("max_level_error" "release_max_level_warn"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "test_zia") (r "^0.2.0") (d #t) (k 2)))) (h "0i8iyk7gbrvqw62kxf2dq5pb5krz8cjynvk1kv1y65882wa4s6qx")))

(define-public crate-zia-0.3.2 (c (n "zia") (v "0.3.2") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (f (quote ("max_level_info" "release_max_level_warn"))) (d #t) (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (k 0)) (d (n "test_zia") (r "^0.2.0") (d #t) (k 2)))) (h "0vwv5z4v486vw411vwkkzwrisjk26w0ndym869mg6f69nb2xhpfh")))

(define-public crate-zia-0.4.0 (c (n "zia") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (f (quote ("max_level_info" "release_max_level_warn"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)) (d (n "test_zia") (r "^0.4.0") (d #t) (k 2)))) (h "1k0vnrzfsf8dv4jivw5d5k6mnal4dxgr1jry2f14bffk5wq9hf45")))

(define-public crate-zia-0.5.0 (c (n "zia") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)) (d (n "slog") (r "^2.4.1") (f (quote ("max_level_info" "release_max_level_warn"))) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "slog-term") (r "^2.4.0") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)) (d (n "test_zia") (r "^0.5.0") (d #t) (k 2)))) (h "05ldrirwkkn04v3284cxy7vrdsn49f349ks97im3j8fg7pb53ba4")))

