(define-module (crates-io #{3}# z zif) #:use-module (crates-io))

(define-public crate-zif-0.0.1 (c (n "zif") (v "0.0.1") (d (list (d (n "clap") (r "^2.29.4") (d #t) (k 0)) (d (n "zif_net") (r "^0.1.0") (d #t) (k 0)))) (h "1xqn0ihd95ch2naxb2gjcj8i72i5x6racbfbs7z4m70pda8qkn8c")))

