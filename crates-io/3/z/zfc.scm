(define-module (crates-io #{3}# z zfc) #:use-module (crates-io))

(define-public crate-zfc-0.1.0 (c (n "zfc") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.4") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1x3wihvz5dwrm1fz61j43wpa8jwr3nx46366qzb5ppqf4gnjn4cd") (f (quote (("std"))))))

(define-public crate-zfc-0.1.1 (c (n "zfc") (v "0.1.1") (d (list (d (n "num-bigint") (r "^0.4.4") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "01fdglpk7241j77csg43m91h3p62p5wniic0dq7ljnxf9jag3fzg") (f (quote (("std"))))))

(define-public crate-zfc-0.2.0 (c (n "zfc") (v "0.2.0") (d (list (d (n "num-bigint") (r "^0.4.4") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "16g6y0rl450kqpcqmzxgqgwjdg6arm5ylf4bakqy596c6fpkvzna") (f (quote (("std"))))))

(define-public crate-zfc-0.3.0 (c (n "zfc") (v "0.3.0") (d (list (d (n "num-bigint") (r "^0.4.4") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "11j506krbk8bpfwk3vzq35q0z6xbfwm7iikwpcwv1lgm3n75dvc9") (f (quote (("std"))))))

(define-public crate-zfc-0.3.1 (c (n "zfc") (v "0.3.1") (d (list (d (n "num-bigint") (r "^0.4.4") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "182vqxvlhd9b93bp904v0r8ysgi7mzx56y9jgb0nmq3zh7vrrzvh") (f (quote (("std"))))))

(define-public crate-zfc-0.3.2 (c (n "zfc") (v "0.3.2") (d (list (d (n "num-bigint") (r "^0.4.4") (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1saxfakmxkgmhc3m64vms56yn73ka0r7lck69rawypgb9gpxd09l") (f (quote (("std"))))))

