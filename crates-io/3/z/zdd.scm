(define-module (crates-io #{3}# z zdd) #:use-module (crates-io))

(define-public crate-zdd-0.1.0 (c (n "zdd") (v "0.1.0") (d (list (d (n "hashconsing") (r "^0.3.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.11") (d #t) (k 0)))) (h "052lf0cn7rvgfx7rfx44yqadwgv04bfird5db6hfazx1pmz4068c")))

(define-public crate-zdd-0.2.0 (c (n "zdd") (v "0.2.0") (d (list (d (n "hashconsing") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "1z44d0f4zxj0dnsqnnzj4wjdqj46zq30gzx7kyynim0k8rf267xc")))

(define-public crate-zdd-0.3.0 (c (n "zdd") (v "0.3.0") (d (list (d (n "hashconsing") (r "1.*") (d #t) (k 0)) (d (n "rand") (r "0.7.*") (d #t) (k 0)))) (h "0m8fyi16pdgafvv80f55b4cwa9iz4pfy2i89zgk6kbi3014sssh2")))

