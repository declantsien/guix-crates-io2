(define-module (crates-io #{3}# z zuu) #:use-module (crates-io))

(define-public crate-zuu-0.0.1 (c (n "zuu") (v "0.0.1") (h "18b0kzsb1knq1f82m3p0gp0pz6man88982cvp7h14b0qi14vbdhy")))

(define-public crate-zuu-0.0.2 (c (n "zuu") (v "0.0.2") (h "0b3gk4ilw36si8c77r8rcb3p00ly1yj7nz4fvqm8xnx24wgmqmnk")))

(define-public crate-zuu-0.0.3 (c (n "zuu") (v "0.0.3") (h "1biksgplc51njf0160yycspm1i6vq65rw2zj0m2s57ynvgcbgp8h")))

(define-public crate-zuu-0.0.4 (c (n "zuu") (v "0.0.4") (h "1q7pizry9a7asg3dz2s2wq6sam26qifp6sgkz4hvaj4wd2m9jcg0")))

(define-public crate-zuu-0.1.0 (c (n "zuu") (v "0.1.0") (d (list (d (n "question") (r "^0.2.2") (d #t) (k 0)))) (h "1x44aqp226bgkjmlsmswkypzv9qs9vj7gv7xcr391bdmfgmg1y9h")))

(define-public crate-zuu-0.1.1 (c (n "zuu") (v "0.1.1") (h "0lm8sa5bp5w07rkc0kdwkrchygvhyh2ar7w1ifm4gma30q9vq40l")))

(define-public crate-zuu-0.2.0 (c (n "zuu") (v "0.2.0") (d (list (d (n "continuous-testing") (r "^0.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0xig9hvkilcayp34nk7mvx2jn9n963z9mrlygqmla2gmwsgkr305")))

(define-public crate-zuu-0.2.1 (c (n "zuu") (v "0.2.1") (d (list (d (n "continuous-testing") (r "^0.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "035lkv6r6nvrv1crj5lslsfrf61iknyf21xglc9ycgapn2cq13y3")))

(define-public crate-zuu-0.2.2 (c (n "zuu") (v "0.2.2") (d (list (d (n "continuous-testing") (r "^0.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0q9dx6plsbfjp1wxcxgsizly4hzx5amg6lhpy1pdnnsn4prhb47d")))

(define-public crate-zuu-0.2.3 (c (n "zuu") (v "0.2.3") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "continuous-testing") (r "^0.1.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "123kmmjmys9mkhvmhy05wa10xvjhiqd0i5vrgbnw0gp76yjr543h")))

(define-public crate-zuu-0.2.4 (c (n "zuu") (v "0.2.4") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "continuous-testing") (r "^0.1.3") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "progress_bar") (r "^1.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "1yiaq1f0vg24qb435a5x72s1y776sv483fx9zck66w4zfjr37wlz")))

(define-public crate-zuu-0.2.5 (c (n "zuu") (v "0.2.5") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "12985gkjwv7bqzzpkxk7za2gcqhgxd3qkk9zz3hrhww2q2dmbjsd")))

(define-public crate-zuu-0.2.6 (c (n "zuu") (v "0.2.6") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "03qxkmbb49nbi520cp453mr3gh11skidgz6gswks17lwxzi73dah")))

(define-public crate-zuu-0.2.7 (c (n "zuu") (v "0.2.7") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "09nkhjlsyq46gg3vg5qfqfknsd8sxzn3i52r18g69wrk63bcyas6")))

(define-public crate-zuu-0.2.8 (c (n "zuu") (v "0.2.8") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "1g8fk68bksvb6pwg1vvkkqddhvnc43b61wrw26yzz7lbhak6hgh9")))

(define-public crate-zuu-0.3.0 (c (n "zuu") (v "0.3.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "1brdbxjggd50vnvxlr9yxnj8hqdg4ly3y1faaz6ifny7iz7azn8v")))

(define-public crate-zuu-0.3.1 (c (n "zuu") (v "0.3.1") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "04kgsqymrc6w4969hvy2m9fwpv75bxxf3nhnk9yphx126z7izpd5")))

(define-public crate-zuu-0.3.2 (c (n "zuu") (v "0.3.2") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "1imb7lygcg6n2pj8ps0gfrfw6k5v9xcbgnnrnvlgsykc36vqivni")))

(define-public crate-zuu-0.3.3 (c (n "zuu") (v "0.3.3") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0k5kkpga0xclkiscf11vncbxw7bvwcjzk27wwr6mmsmihnkcm7ak")))

(define-public crate-zuu-0.3.4 (c (n "zuu") (v "0.3.4") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0qr4zpkn9mb9iaavc8mi7hch82iizz9gc3dkn76ri3xkm2dsr0v8")))

(define-public crate-zuu-0.3.5 (c (n "zuu") (v "0.3.5") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0xxy1pq9cmypkzikqvhbz9y8vz6fpakryk1y7d7xxi5cv6zj5b1l")))

(define-public crate-zuu-0.3.6 (c (n "zuu") (v "0.3.6") (d (list (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)))) (h "15ls10bgiahpg2skcmw1av3i60qyqhbjvhv50jh2n3g5qhnw6gn9")))

(define-public crate-zuu-0.3.7 (c (n "zuu") (v "0.3.7") (d (list (d (n "crossterm_cursor") (r "^0.4.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)))) (h "15d01fn5pqlhgyw1zm62k5m7zr01cjcffzkq6x45drpaalz18j3c")))

(define-public crate-zuu-0.4.0 (c (n "zuu") (v "0.4.0") (d (list (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)))) (h "0igrcxq23x3lkg41kq7yvha8hajzm3kcxv9msbhqfsmmq9s1cvr4")))

(define-public crate-zuu-0.4.2 (c (n "zuu") (v "0.4.2") (d (list (d (n "rsbadges") (r "^1.1.5") (d #t) (k 0)))) (h "1d0q49wr9avqnv351xmm2kwhgjfi50k4afpf7f52c44nl10zvsy6")))

(define-public crate-zuu-5.0.0 (c (n "zuu") (v "5.0.0") (h "0pyxksyp3h6i9s0i74rbclmv2gzwgz34kvy6vcvjw246awy0kzgl")))

(define-public crate-zuu-5.0.1 (c (n "zuu") (v "5.0.1") (h "0lpfspcygwxx7m756254aqh2ylfbpm3a28dz94wkwm0hg4jbm607")))

(define-public crate-zuu-5.0.2 (c (n "zuu") (v "5.0.2") (h "0y8fmx29vd78jh4fiznasw8xzglmab775i79lwh7w02jh97mmn6y")))

(define-public crate-zuu-5.0.3 (c (n "zuu") (v "5.0.3") (h "1zqjc7vkb46k4f6jfwvcsprcfqbhjs8jyd3rc9cfslyk1z5qslq0")))

(define-public crate-zuu-5.0.4 (c (n "zuu") (v "5.0.4") (h "0pch1h5zfsz15m2v89w5xjmicqby1vi7h3faz2g0lfvhpi3qifh5")))

(define-public crate-zuu-5.0.5 (c (n "zuu") (v "5.0.5") (h "136lb6v1hnrickl8zkzq24gpz12sfm4zicva1x1j8ppk84sxjbv7")))

(define-public crate-zuu-5.0.6 (c (n "zuu") (v "5.0.6") (h "01qwpnr7j7r9wkn8vrcbs9jivxf4hf0vlkgb71jp6h05x4l61i3s")))

(define-public crate-zuu-6.0.0 (c (n "zuu") (v "6.0.0") (h "0li9wc9ypkj4s7x8gbx0acj89grpr1mg41qb3c4igg78qaw97jf3")))

(define-public crate-zuu-7.0.0 (c (n "zuu") (v "7.0.0") (h "0v73g18b949qaa9lbkm2w5rjgilwhfpndnb56pzixyk4qdarbnjl")))

(define-public crate-zuu-7.0.1 (c (n "zuu") (v "7.0.1") (h "1m2hbkmblnawkq2mc2qfsz6180xz3zhcg4kg5jgn5jp0bbgqxkkd")))

(define-public crate-zuu-7.1.0 (c (n "zuu") (v "7.1.0") (h "01isla440wfvarg3qk0j8jq35xf9pnpqzjfq3q6gbgijdxm6c0l6")))

(define-public crate-zuu-7.1.1 (c (n "zuu") (v "7.1.1") (h "18805zbflhyvh8fddg11iyqg23p2x30rpc8bwh6kch6l4d8krkc9")))

(define-public crate-zuu-7.1.2 (c (n "zuu") (v "7.1.2") (h "1anf3jf0kw7d0i3p1wglnclpg7p4122fi18jf1q4fgwbq5p27da8")))

(define-public crate-zuu-7.2.0 (c (n "zuu") (v "7.2.0") (h "1lcgr8m7rqhqw77rfgnga3wy69n2k1g2grp12y22lq37zanl3zfb")))

(define-public crate-zuu-7.2.1 (c (n "zuu") (v "7.2.1") (h "00pnwa0prcjwhxsvk99mhiajlqhc1prh330v2l47lckslnv1jrgs")))

(define-public crate-zuu-8.0.0 (c (n "zuu") (v "8.0.0") (h "059v2kljsvkd4xxsmdx5w7fgcxrx0hmw8vgsrwh5mjdy8arnnm1r")))

(define-public crate-zuu-8.0.1 (c (n "zuu") (v "8.0.1") (h "0768nihs6dsb7xmghcdpmizp0n7j74ws34367l1f1rs9p7ifj01h")))

(define-public crate-zuu-10.0.0 (c (n "zuu") (v "10.0.0") (d (list (d (n "ansi-escapes") (r "=0.1.1") (d #t) (k 0)))) (h "02ss7gh3139kph2lqcjlz5gvgd920ycgbvnqf30zjc3i4h4l3a4m")))

(define-public crate-zuu-11.0.0 (c (n "zuu") (v "11.0.0") (d (list (d (n "ansi-escapes") (r "=0.1.1") (d #t) (k 0)))) (h "06v9g58radcjmlcdy6174rbpyz36qd2nn7lsarld6dzr8gxdi2lz")))

(define-public crate-zuu-12.0.0 (c (n "zuu") (v "12.0.0") (d (list (d (n "ansi-escapes") (r "=0.1.1") (d #t) (k 0)))) (h "1dvhcmspwz25ldhrjza7v7fjgdbp6x0k44gripc48rarpq5ga34z")))

(define-public crate-zuu-12.1.0 (c (n "zuu") (v "12.1.0") (d (list (d (n "ansi-escapes") (r "=0.1.1") (d #t) (k 0)))) (h "0pvbq6yrfyv9ai00dindf4gj5w00qzc2g657v2p5ii46d10k6i6p")))

(define-public crate-zuu-13.0.0 (c (n "zuu") (v "13.0.0") (d (list (d (n "ansi-escapes") (r "=0.1.1") (d #t) (k 0)))) (h "1wbn07xi9kpv85sggd2jgb0lbjxc30y0hj2lnb6074q1sziy1ni8")))

(define-public crate-zuu-14.0.0 (c (n "zuu") (v "14.0.0") (d (list (d (n "ansi-escapes") (r "=0.1.1") (d #t) (k 0)))) (h "0ahkx58m3gmy9wwidp0v3qbcf1z2h1mcl7izr178baw0g36c5n9c")))

(define-public crate-zuu-15.0.0 (c (n "zuu") (v "15.0.0") (d (list (d (n "ansi-escapes") (r "=0.1.1") (d #t) (k 0)))) (h "1s673i86nznfb2gmmwjsfcrmzpaxabk8wpxsdqdmz1444xnkmlcs")))

(define-public crate-zuu-15.1.0 (c (n "zuu") (v "15.1.0") (d (list (d (n "ansi-escapes") (r "=0.1.1") (d #t) (k 0)))) (h "0ixyldipp9j6bcp2hnlsl0h4kfb0f6agblnmkc310nqj3f68g9mc")))

(define-public crate-zuu-16.0.0 (c (n "zuu") (v "16.0.0") (h "0i9bgwqmhw1h9hjy3f8133imhmxczb1mrpkgvwwwris84rpax80n")))

(define-public crate-zuu-16.0.1 (c (n "zuu") (v "16.0.1") (h "0jpa498xbszlkhsc6sncdcm3wbzf0izhzpf27mjiv23mwz68k7kn")))

(define-public crate-zuu-16.0.2 (c (n "zuu") (v "16.0.2") (h "1qcdpim6n6whlvjjdpmyhbsxqavwr8ys971qf57d2lkw369bippa")))

(define-public crate-zuu-16.1.0 (c (n "zuu") (v "16.1.0") (h "1rxakl029cw1zkxpjxm7b3ypdk4b7jp282mva98rz3gsqrasvcgr")))

