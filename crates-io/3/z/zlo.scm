(define-module (crates-io #{3}# z zlo) #:use-module (crates-io))

(define-public crate-zlo-0.1.0 (c (n "zlo") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1rc3igis9vpwn0plaw154cmgwn7xda8biqmbs56syy8f4xgj2jm8")))

