(define-module (crates-io #{3}# z zcm) #:use-module (crates-io))

(define-public crate-zcm-0.1.0 (c (n "zcm") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comrak") (r "^0.16") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1y1rkx2lq0gkq24ds4k754adjmxyikcdqgg6f4plfmm2cp1rr17m") (y #t)))

