(define-module (crates-io #{3}# z z3d) #:use-module (crates-io))

(define-public crate-z3d-0.1.0 (c (n "z3d") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.3") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "z3") (r "^0.4.0") (d #t) (k 0)))) (h "1yz1s08hyfdsgm3l6rdl061x2kam6vdxfifpxb0k4nqfv1y2bs6a")))

