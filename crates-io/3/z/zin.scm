(define-module (crates-io #{3}# z zin) #:use-module (crates-io))

(define-public crate-zin-0.1.1 (c (n "zin") (v "0.1.1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "doe") (r "^0.1.32") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02lrgvs2nyp4irn28pmhgra50dzjwf0jrivc942qkzf9nqf7jviz")))

