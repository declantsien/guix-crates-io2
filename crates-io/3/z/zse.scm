(define-module (crates-io #{3}# z zse) #:use-module (crates-io))

(define-public crate-zse-0.1.0 (c (n "zse") (v "0.1.0") (d (list (d (n "config") (r "^0.13.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0d034qdzdlllmcz0llax3k9bl2713i8n92bsknkbnvz7f85drn3i")))

