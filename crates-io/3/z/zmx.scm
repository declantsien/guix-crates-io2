(define-module (crates-io #{3}# z zmx) #:use-module (crates-io))

(define-public crate-zmx-0.1.0 (c (n "zmx") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "020q71x3mm68c0l0aq8nan8b830k691p7z58pd7mm4i3wx5ngnkk")))

(define-public crate-zmx-0.1.1 (c (n "zmx") (v "0.1.1") (d (list (d (n "webbrowser") (r "^0.8.11") (d #t) (k 0)))) (h "0v88bnqksbayr85fnh60hrkc3gaqfgff35wllnkkpnpjv4n0696i")))

