(define-module (crates-io #{3}# z zzp) #:use-module (crates-io))

(define-public crate-zzp-0.1.0 (c (n "zzp") (v "0.1.0") (d (list (d (n "assert2") (r "^0.2.1") (d #t) (k 2)) (d (n "gregorian") (r "^0.2.0") (d #t) (k 0)))) (h "11rkhvy3k9i0kb55j37fbn5hrcdxvwqxnx12qxa4hbys7vwyw1dr")))

