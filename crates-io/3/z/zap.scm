(define-module (crates-io #{3}# z zap) #:use-module (crates-io))

(define-public crate-zap-0.0.1 (c (n "zap") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "httparse") (r "^1.1.2") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "1r0qr2xr18rdmry8sd1ax2ilhcinxfsdnn723gwgindr1lh0s3hz")))

(define-public crate-zap-0.0.2 (c (n "zap") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "httparse") (r "^1.2.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 0)))) (h "03jlm233k583pbp6acd9rsb6p6089g0zf91w798jjk83kwp0fr5l")))

(define-public crate-zap-0.0.3 (c (n "zap") (v "0.0.3") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "httparse") (r "^1.2.3") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 0)))) (h "0z0wxcizr675i5hv45flm7k3cz96fa31krpg16i6x7f4apgcczcc")))

(define-public crate-zap-0.0.4 (c (n "zap") (v "0.0.4") (d (list (d (n "bytes") (r "^0.4.5") (d #t) (k 0)) (d (n "futures") (r "^0.1.17") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 0)))) (h "0j2c43m2fyalpl77q80yg6ipyc5qn69xh16bvppmbvs412z38j07")))

