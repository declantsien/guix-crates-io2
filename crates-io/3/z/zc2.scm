(define-module (crates-io #{3}# z zc2) #:use-module (crates-io))

(define-public crate-zc2-0.0.1-dev (c (n "zc2") (v "0.0.1-dev") (d (list (d (n "clone") (r "^0.1.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "command-macros") (r "^0.2.9") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.5") (d #t) (k 0)) (d (n "ifaces") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)))) (h "0qhaflkaxrayia94jyi9skc6xr12mggjq6nngn34xp5mlnfhax8d") (y #t)))

(define-public crate-zc2-0.0.2-dev (c (n "zc2") (v "0.0.2-dev") (d (list (d (n "clone") (r "^0.1.2") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "command-macros") (r "^0.2.9") (d #t) (k 0)) (d (n "html5ever") (r "^0.22.5") (d #t) (k 0)) (d (n "ifaces") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.19") (d #t) (k 0)) (d (n "soup") (r "^0.5.1") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "0dn5vrzcnl0h85kk73dvrmk3yhz8d73f86j1720jv6pq8l1kh5mc")))

