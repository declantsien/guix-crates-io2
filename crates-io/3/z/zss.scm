(define-module (crates-io #{3}# z zss) #:use-module (crates-io))

(define-public crate-zss-1.0.0 (c (n "zss") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "glium") (r "^0.30.1") (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib" "glx" "xrender"))) (d #t) (k 0)))) (h "0dkx4xddqda8qir521vy169vwkl96cg5ms9fdr4mw9sb7mbyyjq4")))

(define-public crate-zss-1.0.1 (c (n "zss") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "glium") (r "^0.30.1") (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.10.0") (d #t) (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib" "glx" "xrender"))) (d #t) (k 0)))) (h "1khwa2xwlc4s25q2f630h3s477v1myc27615dh8a4ad1m6cf2a41")))

