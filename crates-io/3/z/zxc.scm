(define-module (crates-io #{3}# z zxc) #:use-module (crates-io))

(define-public crate-zxc-0.1.0 (c (n "zxc") (v "0.1.0") (h "03n1sxfs2jkgz5w5jw91jvzrrxqra4k2ldxyg6c77hawn1f8ssp3")))

(define-public crate-zxc-0.1.1 (c (n "zxc") (v "0.1.1") (h "06gxza99n6xdi0w0k7ycmz4gch52fs2rj1yzlrsmx3cbv521xx25")))

