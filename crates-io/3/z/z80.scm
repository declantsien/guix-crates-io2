(define-module (crates-io #{3}# z z80) #:use-module (crates-io))

(define-public crate-z80-1.0.0 (c (n "z80") (v "1.0.0") (h "1grslx8pgp9p6pikk7j2h9w0jbkxrz6q4sa0scqjcvjz10mhdzcy")))

(define-public crate-z80-1.0.1 (c (n "z80") (v "1.0.1") (h "04prqiz7v826p7gwcvi6xp6wfdrcncymf93225fvi8r9arxlba0c")))

(define-public crate-z80-1.0.2 (c (n "z80") (v "1.0.2") (h "0hy0xpxa5jfcwmr3f9m2lk7px6b7zj266ajxzj0jvbfwh3ls23av")))

