(define-module (crates-io #{3}# j jtd) #:use-module (crates-io))

(define-public crate-jtd-0.1.0 (c (n "jtd") (v "0.1.0") (d (list (d (n "arbitrary") (r "^0.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0056rmds148jfj9iw0a4pdg2i2z0j42ngfnnq121dsda2570ch79") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-jtd-0.1.1 (c (n "jtd") (v "0.1.1") (d (list (d (n "arbitrary") (r "^0.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b9927j17icvnj729597dxdly0swxvdaabansxw7xa9l52p8sjlh") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-jtd-0.1.2 (c (n "jtd") (v "0.1.2") (d (list (d (n "arbitrary") (r "^0.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0312ylgyyf100nr925bjnz05rys9444zk24vywg4ydv5a4yv8bxb") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-jtd-0.1.3 (c (n "jtd") (v "0.1.3") (d (list (d (n "arbitrary") (r "^0.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1fwg7w0x3x3lkq194fbx16aa5s1m1fjnk1hcs46bswyqmf393css") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-jtd-0.1.4 (c (n "jtd") (v "0.1.4") (d (list (d (n "arbitrary") (r "^0.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ki6f3avscmpwg815lg48mz3zzx5h70xf09vckx4w3hivhpwg8ix") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-jtd-0.2.0 (c (n "jtd") (v "0.2.0") (d (list (d (n "arbitrary") (r "^0.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0f3iyy07qaf0930g22gkvfv5295mi84b0qidcq10hn9h6cvk4d5j") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-jtd-0.2.1 (c (n "jtd") (v "0.2.1") (d (list (d (n "arbitrary") (r "^0.4.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lmwjnmnj3ml0hy5gg4jb7jmmk4xq4g29k9y5a1z4wg0691rdw2l") (f (quote (("fuzz" "arbitrary"))))))

(define-public crate-jtd-0.3.0 (c (n "jtd") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vdhv6rdmncfir7x97y512lvm1yj6is7cjiplprlyh24nk236mpw")))

(define-public crate-jtd-0.3.1 (c (n "jtd") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0wdnijzk56m7cwvq8r8vygr2dwk1wnb15578yfj86dgj214l3n9h")))

