(define-module (crates-io #{3}# j jca) #:use-module (crates-io))

(define-public crate-jca-0.0.0 (c (n "jca") (v "0.0.0") (h "0x5l2sdnzha47sfcpwcdkmxbpa3dgdrwcrq8wdzfajmrk01lqbsa") (y #t)))

(define-public crate-jca-0.0.1 (c (n "jca") (v "0.0.1") (h "1bfss9xdc9xr735l3yvyivdv35ly2q8q5y7qc5z5wiscfnlzlw86") (y #t)))

