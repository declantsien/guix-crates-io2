(define-module (crates-io #{3}# j jeb) #:use-module (crates-io))

(define-public crate-jeb-0.0.0-jeb (c (n "jeb") (v "0.0.0-jeb") (h "1s2x3r4cc8lhk113kjp5gxa0lxqn3zmgwpfza6mghh9mbg2q2zhy") (y #t)))

(define-public crate-jeb-0.0.0-- (c (n "jeb") (v "0.0.0--") (h "01ylyxwzj961l4z7607mfsvwah29wknq56q5hsxa32n0715yxbl4") (y #t)))

