(define-module (crates-io #{3}# j jfc) #:use-module (crates-io))

(define-public crate-jfc-0.1.0 (c (n "jfc") (v "0.1.0") (d (list (d (n "clap") (r "^4.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "toy-json-formatter") (r "^0.3.0") (d #t) (k 0)))) (h "0qas3vq9q10sdviq2lc4p1rssqgllsqmj4p9a6h0rfgggza5rkbk")))

(define-public crate-jfc-0.1.1 (c (n "jfc") (v "0.1.1") (d (list (d (n "clap") (r "^4.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "toy-json-formatter") (r "^0.3.0") (d #t) (k 0)))) (h "182fmq099lnrzf12mnbavz7dfhsdsgv6mys06rfgy4w5yrx3rwij")))

(define-public crate-jfc-0.1.2 (c (n "jfc") (v "0.1.2") (d (list (d (n "clap") (r "^4.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "toy-json-formatter") (r "^0.3") (d #t) (k 0)))) (h "0sf3qsh6y8zxgcqf31ah90prniiylcr7cbjs9s4rk49dlhqli1aj")))

(define-public crate-jfc-0.1.3 (c (n "jfc") (v "0.1.3") (d (list (d (n "clap") (r "^4.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "toy-json-formatter") (r "^0.3") (d #t) (k 0)))) (h "1v5l6q05jacm6vncy7si5g4bpsa73pnhj80xlbl4zrz6bv1s4dgb")))

(define-public crate-jfc-0.1.4 (c (n "jfc") (v "0.1.4") (d (list (d (n "clap") (r "^4.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "toy-json-formatter") (r "^0.3") (d #t) (k 0)))) (h "0d0lnl07wbyvlspzivkdaxjbnr0l8s6drwdb347if7hrs2761ah6")))

(define-public crate-jfc-0.1.5 (c (n "jfc") (v "0.1.5") (d (list (d (n "clap") (r "^4.5") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "toy-json-formatter") (r "^0.3") (d #t) (k 0)))) (h "1cw16igqa0ziizg5aq2m36c1ni958vk8gsqlqihlqwyfdkwimri3")))

