(define-module (crates-io #{3}# j jig) #:use-module (crates-io))

(define-public crate-jig-0.0.0 (c (n "jig") (v "0.0.0") (h "0hhgybyv4iz4kb0a6nxbyfdmqnr8nr46qll063h6q7il9y54az0q")))

(define-public crate-jig-0.0.3 (c (n "jig") (v "0.0.3") (h "0j2w71icbskzk0nqyxh6ij9yvp028y1qpxfay7jfmpz5rm5k3kzi")))

