(define-module (crates-io #{3}# j jot) #:use-module (crates-io))

(define-public crate-jot-0.1.0 (c (n "jot") (v "0.1.0") (d (list (d (n "clap") (r "^2.24.2") (d #t) (k 0)) (d (n "clap") (r "^2.24.2") (d #t) (k 1)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 0)) (d (n "shellexpand") (r "^1.0.0") (d #t) (k 0)) (d (n "text_io") (r "^0.1.6") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "xdg") (r "^2.1.0") (d #t) (k 0)))) (h "165x4m5aw025yzn6jn3alhx113y0pm07chx54vdi6p5lh7l4w4ah")))

