(define-module (crates-io #{3}# j jqk) #:use-module (crates-io))

(define-public crate-jqk-0.1.0 (c (n "jqk") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "calm_io") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "15iy68z88381imm10yh05j2s04riwksq8za9rsxhhfj56a53kag7")))

(define-public crate-jqk-0.1.3 (c (n "jqk") (v "0.1.3") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "calm_io") (r "^0.1.1") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (f (quote ("preserve_order"))) (d #t) (k 0)))) (h "17iwy6bqrkp7s6hzs610l3xqwp6aga11jl5ywdhzjymmyza1igjg")))

