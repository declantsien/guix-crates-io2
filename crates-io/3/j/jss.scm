(define-module (crates-io #{3}# j jss) #:use-module (crates-io))

(define-public crate-jss-0.1.0 (c (n "jss") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "19bbppp2r4vmsgclx54j84wmb9xmzjf0nc9rwmlr1jajrlkkgyqm")))

(define-public crate-jss-0.2.0 (c (n "jss") (v "0.2.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "17mz4v2m336x53bf7921qjv4927k1djbmc14a116ml0p5s91iig9")))

(define-public crate-jss-0.3.0 (c (n "jss") (v "0.3.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "11zir03yzakqwivhkr5h70gs37s57x7x9fjial06090dr14yawr8")))

(define-public crate-jss-0.3.1 (c (n "jss") (v "0.3.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "14r35amv57h0k03rmc5k0z8j9869w72kriimmmlwfqbkkdkb68kp") (f (quote (("strict") ("default"))))))

(define-public crate-jss-0.3.2 (c (n "jss") (v "0.3.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "0l68yd28f9libj7pf4zr8hl17bin06vf7f2fxs9rc20m4r0vs2ng") (f (quote (("strict") ("default"))))))

(define-public crate-jss-0.3.3 (c (n "jss") (v "0.3.3") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "15b4i5frkfj4cql7ra57f1pajl7bmi5kzlha350cp13vd0xysgzw") (f (quote (("strict") ("default"))))))

(define-public crate-jss-0.4.0 (c (n "jss") (v "0.4.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "18bxsq0a9k8mz83jyspwpg3ih7yhbbk47z6vz07cyj178sfx79bc") (f (quote (("strict") ("default"))))))

(define-public crate-jss-0.5.0 (c (n "jss") (v "0.5.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "04y9w9yyr84h3gqvgnpnyi9q7j5b0vw7r6248cigkrhqlkwanf6c") (f (quote (("strict") ("default"))))))

(define-public crate-jss-0.5.1 (c (n "jss") (v "0.5.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1l7xky5ha15w31b3z88ysskhhcv1yndqxm6zbsrp0bfz0sbjd0sm") (f (quote (("strict") ("default"))))))

(define-public crate-jss-0.6.0 (c (n "jss") (v "0.6.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "113jp2d3cjj7m3g01kl1xbc864njl5p2kxr9dkjy1rr8hm1dn042") (f (quote (("strict") ("default"))))))

(define-public crate-jss-0.6.1 (c (n "jss") (v "0.6.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "0579rb5r975myns0jxq0yi1pk6mmn4la89n9yq0c0m984filpngw") (f (quote (("strict") ("default"))))))

(define-public crate-jss-0.6.2 (c (n "jss") (v "0.6.2") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "phf") (r "^0.11.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "06am7awwbqf6lgy5vf05jw03vgr0yh0l4xq94j97r0r13wpnvk24") (f (quote (("strict") ("default"))))))

