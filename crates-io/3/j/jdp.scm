(define-module (crates-io #{3}# j jdp) #:use-module (crates-io))

(define-public crate-jdp-0.1.1 (c (n "jdp") (v "0.1.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "gumdrop") (r "~0.8.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "pest") (r "~2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "~2.1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "~0.4.0") (d #t) (k 0)))) (h "0cb0bj9a608zcj43sms46blwcn9cwl2m4fks708qmni62an5yf43")))

(define-public crate-jdp-0.2.0 (c (n "jdp") (v "0.2.0") (d (list (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "gumdrop") (r "~0.8.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "pest") (r "~2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "~2.1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "~0.4.0") (d #t) (k 0)))) (h "0ddhxcvjinzm8a3p3y6xan0npbww3pji1dc7dvgz8nll0kpm2y0v")))

(define-public crate-jdp-0.2.1 (c (n "jdp") (v "0.2.1") (d (list (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "gumdrop") (r "~0.8.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "pest") (r "~2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "~2.1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "~0.4.0") (d #t) (k 0)))) (h "12yzs21pq9czf4470n1r035bmi2vnii2wdgkjzbvmk4vc69fi4vm")))

(define-public crate-jdp-0.2.2 (c (n "jdp") (v "0.2.2") (d (list (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "gumdrop") (r "~0.8.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "pest") (r "~2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "~2.1.0") (d #t) (k 0)) (d (n "pretty_env_logger") (r "~0.4.0") (d #t) (k 0)))) (h "06sq4r17ia8709qc78dzp0yfm5x7ij5rqhk6b47vfsvnvdzabj64")))

