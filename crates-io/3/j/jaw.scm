(define-module (crates-io #{3}# j jaw) #:use-module (crates-io))

(define-public crate-jaw-0.1.0 (c (n "jaw") (v "0.1.0") (d (list (d (n "mocki") (r "^0.1.1") (d #t) (k 0)))) (h "06z03xds7ypr3aczfad3pc8l4h9d5fzvm6qacvrjsk50sj2b1v89")))

(define-public crate-jaw-0.1.2 (c (n "jaw") (v "0.1.2") (d (list (d (n "mocki") (r "^0.1.1") (d #t) (k 2)))) (h "1mnl3xpdd15zqvz8g91s5cqjgyhslywqq6zf6p0ian5ipqj5r6k1")))

(define-public crate-jaw-0.2.0 (c (n "jaw") (v "0.2.0") (d (list (d (n "mocki") (r "^0.1.1") (d #t) (k 2)))) (h "1d41vqnblz2c7maa6qpswjr6bgk329qlrmzvxvi9902s6ci7c8s1")))

(define-public crate-jaw-0.2.1 (c (n "jaw") (v "0.2.1") (d (list (d (n "mocki") (r "^0.1.1") (d #t) (k 2)))) (h "0dik6hmm99sw10vz1kmfimr74wcxz4q80ycqr06ivc6pb2k7k0s7")))

(define-public crate-jaw-0.3.0 (c (n "jaw") (v "0.3.0") (d (list (d (n "mocki") (r "^0.1.1") (d #t) (k 2)))) (h "0r1fdpvqiwnrl4ywvjh0cm98wrh441cf4qiy9mpmzqclxxfqyh02")))

(define-public crate-jaw-0.3.1 (c (n "jaw") (v "0.3.1") (d (list (d (n "mocki") (r "^0.1.1") (d #t) (k 2)))) (h "0dd4s5pk1a5cfr904f17n4wa1d8gy6cvbkma51pjj3admjmx5ckw")))

