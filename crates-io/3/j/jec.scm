(define-module (crates-io #{3}# j jec) #:use-module (crates-io))

(define-public crate-jec-0.1.0 (c (n "jec") (v "0.1.0") (h "14c6w3xirxhv4nshac284rrn4p48xxrprv4wqwrshhanmw7gp174")))

(define-public crate-jec-0.2.0 (c (n "jec") (v "0.2.0") (d (list (d (n "jec-rccow") (r "^0.1") (d #t) (k 0)))) (h "1i6nx6f5drwh6n7svbznk7273clqrqmrf2q4ncv84scgx533m5ci")))

(define-public crate-jec-0.2.1 (c (n "jec") (v "0.2.1") (d (list (d (n "jec-rccow") (r "^0.1") (d #t) (k 0)))) (h "05qjkj5bjazpkja9fmgvaramilcmmf8piismxkra47r91rvqabvb")))

