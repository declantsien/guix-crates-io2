(define-module (crates-io #{3}# j jch) #:use-module (crates-io))

(define-public crate-jch-0.1.0 (c (n "jch") (v "0.1.0") (h "0816s22inkj308c3j0zdf8xrfvs2fj8ikmidxbzkj555gpcql0wb")))

(define-public crate-jch-1.0.0 (c (n "jch") (v "1.0.0") (h "0s51i7lbjy3hp2rw4pb8b1pb0nlhhy25jrvrmjfw4mfq4niwyvy8")))

