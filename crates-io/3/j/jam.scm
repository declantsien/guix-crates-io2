(define-module (crates-io #{3}# j jam) #:use-module (crates-io))

(define-public crate-jam-0.0.1 (c (n "jam") (v "0.0.1") (d (list (d (n "bincode") (r "^0.7.0") (d #t) (k 0)) (d (n "jam_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)))) (h "0yx0lggrm0j17al8zg4lx664c1zis4jawrgm03q0wlb2x6yr66ni")))

