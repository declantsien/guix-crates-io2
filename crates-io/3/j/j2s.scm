(define-module (crates-io #{3}# j j2s) #:use-module (crates-io))

(define-public crate-j2s-0.1.0 (c (n "j2s") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "caisin") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "021y84938qmyin57rxzjax83c4akqrbz4m5wg4lwk65lj5mdpq56")))

