(define-module (crates-io #{3}# j jwx) #:use-module (crates-io))

(define-public crate-jwx-0.1.0 (c (n "jwx") (v "0.1.0") (d (list (d (n "base64") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)))) (h "0g880z3d3kvy842dvr18m9fh2g7x30c5h77bfqw4gx99v11r39xl") (f (quote (("reqwest"))))))

