(define-module (crates-io #{3}# j jim) #:use-module (crates-io))

(define-public crate-jim-0.1.0 (c (n "jim") (v "0.1.0") (h "0ayqvfyrvc6phpxdcqiga9kgfpnb0mfrj8j7w0jj98i8wsdqiwvl")))

(define-public crate-jim-0.1.1 (c (n "jim") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "1pgg3bwr0215ja416mp4yn8ap10380f8aij4q89pyvwr6f9y37k0")))

(define-public crate-jim-0.2.0 (c (n "jim") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "toml") (r "^0.5.3") (d #t) (k 0)))) (h "03iw5yicis6sbzw6lkkh4gkxw8alazlm7za9lny3a24w0an3xpqw")))

