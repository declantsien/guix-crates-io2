(define-module (crates-io #{3}# j jsc) #:use-module (crates-io))

(define-public crate-jsc-0.1.0+r201969 (c (n "jsc") (v "0.1.0+r201969") (d (list (d (n "jsc-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "1snqjyw4f5zjrqz1k48ma0qchx9ygbl2f7pr6300knpp07icpwix")))

(define-public crate-jsc-0.1.1+r201969 (c (n "jsc") (v "0.1.1+r201969") (d (list (d (n "jsc-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "url") (r "^1.1.1") (d #t) (k 0)))) (h "181wx6s5nlhyvsflg885pxvicfcvzqki0bn1saj5a88w9yqyx3xy")))

