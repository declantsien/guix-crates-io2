(define-module (crates-io #{3}# j jab) #:use-module (crates-io))

(define-public crate-jab-0.0.1 (c (n "jab") (v "0.0.1") (d (list (d (n "built") (r "^0.4") (d #t) (k 1)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "git2") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "mockall") (r "^0.6.0") (d #t) (k 2)) (d (n "pager") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.46") (d #t) (k 0)))) (h "0ymah1f4xrx67njavzqlwxpq57ffsanw8c1xv7ppkvcz550jbwv3")))

