(define-module (crates-io #{3}# j jtp) #:use-module (crates-io))

(define-public crate-jtp-0.1.0 (c (n "jtp") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)))) (h "12a6firiisf06fl0q4g94dbynvjx3wjwax0mcijrhq1vd7d6y37a")))

(define-public crate-jtp-0.1.1 (c (n "jtp") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.5.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)))) (h "01r8dv7a0sw14fsfavkfx18n7pf6fzbsljr94hscibwi0cb3rd9h")))

(define-public crate-jtp-0.1.2 (c (n "jtp") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)))) (h "1n1vychra85nms5km0ia4vq6k541cqghr0g3pg4s8ji9cy1i6rfc")))

(define-public crate-jtp-0.1.3 (c (n "jtp") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.5.7") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)))) (h "0blhj016qi4jc3ma24db8lrdv2ghqb2py286bxkm4pkq9nl2wx48")))

