(define-module (crates-io #{3}# j jls) #:use-module (crates-io))

(define-public crate-jls-0.1.0 (c (n "jls") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jose-jwk") (r "^0.1") (d #t) (k 0)) (d (n "jose-jws") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rsa") (r "^0.9") (f (quote ("sha2"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("serde"))) (d #t) (k 0)))) (h "0337sjfzmyid8sg3ir7as71y5q6da0s6nrx03dq2y4ghmg40w5mf")))

