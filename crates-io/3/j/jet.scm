(define-module (crates-io #{3}# j jet) #:use-module (crates-io))

(define-public crate-jet-0.1.0 (c (n "jet") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.10.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 0)))) (h "0v5zfyczx4imlfdd7wvcxn95zzkxgi9k2fz3v38wbi3pyzrmjr88")))

(define-public crate-jet-0.2.0 (c (n "jet") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.10.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 0)))) (h "0sw77j7w1pa1yv3zl49df9qgxf64ilsdaj0na7yvy01hxaws152j")))

(define-public crate-jet-0.2.1 (c (n "jet") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.10.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "19s14ckpmyrqxfm48si37ir55zg6qbyz331b7nmwn9x0dcn85y41")))

(define-public crate-jet-0.2.2 (c (n "jet") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.10.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1sfhykmzdxmvp1zhsjcx866vg36vf2ma7dvj4izjb7kiz2yrjpjy")))

(define-public crate-jet-0.2.3 (c (n "jet") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.10.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0v0aw9wvgp28b6n3lzz4zwcn58lpljas7b2bmb9syi8jsyr2cb23")))

