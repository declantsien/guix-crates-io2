(define-module (crates-io #{3}# j joy) #:use-module (crates-io))

(define-public crate-joy-0.1.0 (c (n "joy") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "syscall") (r "^0.2") (d #t) (k 0)))) (h "0hvq7v3cy1llqn5ipz0jib479dwyz08fap2l3zvk54xm7nb44lyh")))

(define-public crate-joy-0.1.1 (c (n "joy") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "syscall") (r "^0.2") (d #t) (k 0)))) (h "0aa4dg6bvwahqb700hbjdfpi1pv29g2jnk3q5wgq054v6ipvi26x")))

(define-public crate-joy-0.1.2 (c (n "joy") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "syscall") (r "^0.2") (d #t) (k 0)))) (h "0r9ardlprrhsbw5s15gvpdv1m7nsl9vdb1p9yja9azaq8ixcqvym")))

(define-public crate-joy-0.1.3 (c (n "joy") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "syscall") (r "^0.2") (d #t) (k 0)))) (h "02rwqrxs55rps41ymbhfxj25sm92ds5ga08ld5693iywcf0ivsqd")))

