(define-module (crates-io #{3}# j j5j) #:use-module (crates-io))

(define-public crate-j5j-0.2.0 (c (n "j5j") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "env" "std"))) (d #t) (k 0)) (d (n "json5") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1q599fcbxwcma721rnsm6xmhkl6ihxn4cy2h6pfr9qh1q2ycglh7")))

