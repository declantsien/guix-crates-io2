(define-module (crates-io #{3}# j jsn) #:use-module (crates-io))

(define-public crate-jsn-0.1.0 (c (n "jsn") (v "0.1.0") (d (list (d (n "bparse") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "14g7av9z3v217wnz629kk6pqwna0hmihmaklxcmx1a9ffy36k61q")))

(define-public crate-jsn-0.2.0 (c (n "jsn") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (f (quote ("format"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0nhxqscz871rx55v1g3q65i9332gidndn3iyrjbbjbp5r4gd46g8")))

(define-public crate-jsn-0.3.0 (c (n "jsn") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (f (quote ("format"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0md0d6594v18dgg7s62cl9kh3kbf523s6hd5mmda0837cclp4v0j")))

(define-public crate-jsn-0.4.0 (c (n "jsn") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (f (quote ("format"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0irh8lf0s96hhhb9z99rw2rnh5k9gkqccpfijmgy4vn2bxm8b4yn")))

(define-public crate-jsn-0.5.0 (c (n "jsn") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (f (quote ("format"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0b908m8qav265z536hks8vbp0wp15ks2qmg0kgi257zs4qnj6bjx")))

(define-public crate-jsn-0.6.0 (c (n "jsn") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (f (quote ("format"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0i6xd1n1n3inp1vl7kbkd81zfq2wiac7d6cs2hg39pnihgq3n2ln")))

(define-public crate-jsn-0.7.0 (c (n "jsn") (v "0.7.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (f (quote ("format"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0pwfdql07vah9gzqr29ii0wphw3qz1khqji4yivc4r9d9b8iy7q9")))

(define-public crate-jsn-0.8.0 (c (n "jsn") (v "0.8.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (f (quote ("format"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0ihy7rnf0mhcw2x0p703zfzwhm872hkdfgpjv11fy8lyn3ffqxxk")))

(define-public crate-jsn-0.9.0 (c (n "jsn") (v "0.9.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (f (quote ("format"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1ffn1q8b97q5qi65bj4zb4c45d7g47b5pzzqmgza95bb2bmr2gb7")))

(define-public crate-jsn-0.10.0 (c (n "jsn") (v "0.10.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (f (quote ("format"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0aqcr61xm3aj4a3w7mn63vbxgnrgj420mdrr83qxni8gk3c25k57")))

(define-public crate-jsn-0.11.0 (c (n "jsn") (v "0.11.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (f (quote ("format"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0xc0prnghyy6amk5vkvfdkg8ig7v0vz5q58h25bkq9rx100zh54f")))

(define-public crate-jsn-0.12.0 (c (n "jsn") (v "0.12.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1mk9nlhsj51y0hij1jvhnp8wygk4pvxyf58ccs3b4a986dbhla10")))

(define-public crate-jsn-0.13.0 (c (n "jsn") (v "0.13.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "02jg33d5d7vd2zmg6v00r3q6xmh99willbp13jdc5vlj916kjlxf")))

(define-public crate-jsn-0.14.0 (c (n "jsn") (v "0.14.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "lexical") (r "^6.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0ik64sgva1lrxgnjw2rkpc5dhzfias8mx1rzlq5iva3hazjy9y58")))

