(define-module (crates-io #{3}# j jlq) #:use-module (crates-io))

(define-public crate-jlq-0.1.0 (c (n "jlq") (v "0.1.0") (d (list (d (n "colored_json") (r "^3.0.1") (d #t) (k 0)) (d (n "cursive") (r "^0.20.0") (f (quote ("crossterm-backend"))) (k 0)) (d (n "indicatif") (r "^0.17.2") (d #t) (k 0)) (d (n "linemux") (r "^0.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.27.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.16") (f (quote ("net" "rt-multi-thread" "macros" "io-util" "io-std"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.6") (f (quote ("codec"))) (d #t) (k 0)))) (h "0bk6zsj3nr6rqrrbh92rvswwfh9znvhfx6w2y13v955qk61wd44m")))

