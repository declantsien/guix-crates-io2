(define-module (crates-io #{3}# j jfs) #:use-module (crates-io))

(define-public crate-jfs-0.1.0 (c (n "jfs") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "04hv7h1bs9rm4kkzz2kgcv5pyzb3h3b08p9ldgdrwjrkvxjp2sb8")))

(define-public crate-jfs-0.1.1 (c (n "jfs") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1v01pwljx5q7pla1619rk7fiamyd7q28d89kig3zbadiif3jipn8")))

(define-public crate-jfs-0.2.0 (c (n "jfs") (v "0.2.0") (d (list (d (n "fs2") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1g431ympp3717kp85mbc82j13n7sw1vjgsgcv754rsjwh2wjf832")))

(define-public crate-jfs-0.2.1 (c (n "jfs") (v "0.2.1") (d (list (d (n "fs2") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "012lmc1m8qg3b5008cc40wpnkafzr7q2ik7s4qqwbd2mqwj050qv") (f (quote (("default" "rustc-serialize"))))))

(define-public crate-jfs-0.2.2 (c (n "jfs") (v "0.2.2") (d (list (d (n "fs2") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "1l65hl8mvl709g17clk3pp3i51hg907g7l2b6w37a6dwf8d94xds") (f (quote (("default" "rustc-serialize"))))))

(define-public crate-jfs-0.3.0 (c (n "jfs") (v "0.3.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 2)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "19kjy7n924gyqjm6vg5jkjqsliki91dy73r8442qas2s03kh5qm3")))

(define-public crate-jfs-0.4.0 (c (n "jfs") (v "0.4.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("v4"))) (d #t) (k 0)))) (h "0qldf99vhy3pwl3pl2can1gh312g7ivfsawyqmj5a2blix86lqms")))

(define-public crate-jfs-0.5.0 (c (n "jfs") (v "0.5.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("v4"))) (d #t) (k 0)))) (h "1f8lphncjks9sanzczk9rpd4il7j7ljq2qxrbf24zhsj0mlasvmq")))

(define-public crate-jfs-0.6.0 (c (n "jfs") (v "0.6.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vx0421l95n9qfy7768k5i32rh5mcian6qbq5hvvmafsv2gk49c4")))

(define-public crate-jfs-0.6.1 (c (n "jfs") (v "0.6.1") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1jnzjjcdhqldvas0cpqnrjszvkvxry6zryaidj73gx4qi26yr3c3")))

(define-public crate-jfs-0.6.2 (c (n "jfs") (v "0.6.2") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fnqlb9yxlh68qpl9rh6c0z71kz3dn4bsv68fm4d5nl8bbk2z6n4")))

(define-public crate-jfs-0.7.0 (c (n "jfs") (v "0.7.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "0m7fmx6vpb4jf8g1wx61cx4xly1a3mp7yx07i98a7r73jni1i40b")))

(define-public crate-jfs-0.7.1 (c (n "jfs") (v "0.7.1") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "03r18x6ni2v33z244yiacspn1pk2gv5l436r00xy17zfxxivjaap")))

(define-public crate-jfs-0.8.0 (c (n "jfs") (v "0.8.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)))) (h "13k1zfscs0p2r8cs2b1a3lvmh4dznix7a5xbcy8fz7sb0xzg0kks")))

(define-public crate-jfs-0.9.0 (c (n "jfs") (v "0.9.0") (d (list (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "0i2yxipi1qq1i7yswzl282hvxr1w681cd5ngb21my9j8ab7v2a7l")))

