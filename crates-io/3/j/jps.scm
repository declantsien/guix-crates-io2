(define-module (crates-io #{3}# j jps) #:use-module (crates-io))

(define-public crate-jps-0.1.0 (c (n "jps") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (f (quote ("use_std"))) (k 0)) (d (n "nalgebra") (r "^0.31") (f (quote ("libm-force" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1b5y6glvgc2igrf0fl56drxg0g2pr1z0n1fc2xv2l1512xbwvdjf")))

(define-public crate-jps-0.1.1 (c (n "jps") (v "0.1.1") (d (list (d (n "itertools") (r "^0.10.3") (f (quote ("use_std"))) (k 0)) (d (n "nalgebra") (r "^0.31") (f (quote ("libm-force" "alloc"))) (k 0)) (d (n "num-traits") (r "^0.2.15") (k 0)))) (h "1vvginwm15qyk0n12ngjydg98bkmq8rimd7yy3x9cvwj880la2js")))

(define-public crate-jps-1.0.1 (c (n "jps") (v "1.0.1") (d (list (d (n "im") (r "^15.1.0") (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.2.3") (d #t) (k 0)))) (h "0miq3kk9jnknrdkzprcih0a2add7dp85lgs2yzva0sjgh5gdb3ra")))

(define-public crate-jps-1.0.2 (c (n "jps") (v "1.0.2") (d (list (d (n "im") (r "^15.1.0") (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.2.3") (d #t) (k 0)))) (h "05qxnfbcqw7wjj8xw4hv3y20a8kwpaya00gg43gnjjkhdciby598")))

(define-public crate-jps-1.0.3 (c (n "jps") (v "1.0.3") (d (list (d (n "im") (r "^15.1.0") (k 0)) (d (n "itertools") (r "^0.10.3") (f (quote ("use_std"))) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.2.3") (d #t) (k 0)))) (h "0qcbxmk7gsngmppb6mfs7kl321pbkmbzq70mp4d1rjcgin72c8i2")))

(define-public crate-jps-1.0.4 (c (n "jps") (v "1.0.4") (d (list (d (n "im") (r "^15.1.0") (k 0)) (d (n "itertools") (r "^0.10.3") (f (quote ("use_std"))) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.2.3") (d #t) (k 0)) (d (n "yakf") (r "^0.1") (o #t) (k 0)))) (h "1pl8rjhlfkmrvnm7c6p1my313zwn2iaya1y2sqg0xydar324z9c7") (s 2) (e (quote (("decomp" "dep:yakf"))))))

(define-public crate-jps-1.1.1 (c (n "jps") (v "1.1.1") (d (list (d (n "im") (r "^15.1.0") (k 0)) (d (n "itertools") (r "^0.10.3") (f (quote ("use_std"))) (k 0)) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "priority-queue") (r "^1.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "yakf") (r "^0.1.12") (o #t) (k 0)))) (h "0ml9l94cd90q9b218g715vrpkw6f5b7bk1kpyy2d8bfp7inijzqs") (s 2) (e (quote (("decomp" "dep:yakf"))))))

