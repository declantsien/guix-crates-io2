(define-module (crates-io #{3}# j joe) #:use-module (crates-io))

(define-public crate-joe-0.1.0 (c (n "joe") (v "0.1.0") (h "0mg0z9h8yiigcvlrh995cg4x531xfic44a27v58sbrg3l4vxc4sn") (y #t)))

(define-public crate-joe-39514.0.0 (c (n "joe") (v "39514.0.0") (h "103k315041jkrpc8p6r77kqdjdkggagd5hbxmss8lym42cc5a152") (y #t)))

(define-public crate-joe-1.0.0 (c (n "joe") (v "1.0.0") (h "0i9nj5rhmvqz19jali8a7wdix21vp3j7ivs2yb53arn9xlpdxb2f") (y #t)))

(define-public crate-joe-1.1.1 (c (n "joe") (v "1.1.1") (h "0naqqi5flkdk7s9fwvqpmy1p5smchm6s3ddx8m8g6cdgzspcs39n")))

