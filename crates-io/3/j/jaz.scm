(define-module (crates-io #{3}# j jaz) #:use-module (crates-io))

(define-public crate-jaz-0.0.1 (c (n "jaz") (v "0.0.1") (d (list (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "1zp48d708gdb5pwzak1xpyrr343c0ypgn380ds5d67lmwwb1p34w")))

(define-public crate-jaz-0.0.2 (c (n "jaz") (v "0.0.2") (d (list (d (n "git2") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)))) (h "0gyhvzmwa6fc00fv4cffbxg72lczy13hl6w22hxyzm3aw9na85zp")))

(define-public crate-jaz-0.0.3 (c (n "jaz") (v "0.0.3") (d (list (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0rif23dh7vlgim9aggcsppxaf47729jk1d5lp8gy6ncn4241fcmm")))

(define-public crate-jaz-0.0.4 (c (n "jaz") (v "0.0.4") (d (list (d (n "git2") (r "^0.16") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1h577dk4f0hl0vxii0kfpba4jcddi5sd7jm6smh13jazxm9q0dzv")))

