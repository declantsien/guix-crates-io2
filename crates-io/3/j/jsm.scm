(define-module (crates-io #{3}# j jsm) #:use-module (crates-io))

(define-public crate-jsm-0.1.0 (c (n "jsm") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.69") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "1s1n6pfraq4qicfijzv6gx6xr59yds86n738kx8x0qvm2rmznchi") (r "1.70.0")))

