(define-module (crates-io #{3}# j jce) #:use-module (crates-io))

(define-public crate-jce-0.1.0 (c (n "jce") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "jce-derive") (r "^0") (d #t) (k 0)))) (h "0q4jzr63phlq63ahaahm5fxwqq1xrxpbb6i910inak975lmmkwn7") (y #t)))

(define-public crate-jce-0.1.1 (c (n "jce") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "jce-derive") (r "^0.1.1") (d #t) (k 0)))) (h "1bgl132hvl9z98xz3xn04la4riwsyy6kq4d5gn41mp2g56nzdakd")))

