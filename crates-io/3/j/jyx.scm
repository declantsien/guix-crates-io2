(define-module (crates-io #{3}# j jyx) #:use-module (crates-io))

(define-public crate-jyx-0.2.0 (c (n "jyx") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "unstructured") (r "^0.2.0-pre1") (d #t) (k 0)))) (h "1xhy5c5z6hd4lzbiczzgip8jy69lm6yzqmrxbnq5cm4ai07vi2vq")))

(define-public crate-jyx-0.3.0 (c (n "jyx") (v "0.3.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "unstructured") (r "^0.3.0") (d #t) (k 0)))) (h "1afww26afgvjbbxp81b8g54q92llqm61jxqnzicwakdgnv6kdw0v")))

(define-public crate-jyx-0.3.1 (c (n "jyx") (v "0.3.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "unstructured") (r "^0.3.1") (d #t) (k 0)))) (h "1cnzgzmn6lrlsg541h3kns1bwmds54pn3l9rdldazfc02d6v742w")))

