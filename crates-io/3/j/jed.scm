(define-module (crates-io #{3}# j jed) #:use-module (crates-io))

(define-public crate-jed-0.0.1 (c (n "jed") (v "0.0.1") (d (list (d (n "rustc-serialize") (r "^0.3.6") (d #t) (k 0)))) (h "09f6sb638h4xr58sp9l9fdmcx6rk3fk235jpbxhzci4lywbjzkcj")))

(define-public crate-jed-0.1.0 (c (n "jed") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)))) (h "1xm3q3k80kflv3m7jf08361myr7s9hs0pmwqcd6njj38x5sk0312")))

(define-public crate-jed-0.1.1 (c (n "jed") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "1k3y9jbssxls0iyy00nam7baxc6hm0rhrrvzzzqhh7y02qp2iflf")))

(define-public crate-jed-0.1.2 (c (n "jed") (v "0.1.2") (d (list (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0zhkkyvs2mgjp476rngihw4nrjsag3xxwyzh53h5f08gmdfmd37i")))

