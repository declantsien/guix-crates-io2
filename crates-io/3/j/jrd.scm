(define-module (crates-io #{3}# j jrd) #:use-module (crates-io))

(define-public crate-jrd-0.1.0 (c (n "jrd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "09q34xls732i0pss6266sypdjj7vm81pxg7p41rid7w2hc3clkah")))

