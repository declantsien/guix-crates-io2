(define-module (crates-io #{3}# j jid) #:use-module (crates-io))

(define-public crate-jid-0.1.0 (c (n "jid") (v "0.1.0") (h "02scp5wj6xd1jgkf9hj2w2c8r33nbpsnccjc3hadw9b9hllvaxpd")))

(define-public crate-jid-0.2.0 (c (n "jid") (v "0.2.0") (h "0awpg7zqja9bkk37h2p8hii7xgjdnyvvfxnwlvw7n9cv8rsxfgra")))

(define-public crate-jid-0.2.1 (c (n "jid") (v "0.2.1") (h "0visqzbvh6f42xrkjn8yf924pvsjknw980zx7zqwp0hvd3zhd9s9")))

(define-public crate-jid-0.2.2 (c (n "jid") (v "0.2.2") (h "1gw1wp7c1mxb5agmk3cii5shdl014az2b14fbzh76v9l2larml5b")))

(define-public crate-jid-0.2.3 (c (n "jid") (v "0.2.3") (d (list (d (n "minidom") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "10ln0c9r4l5r8yf06f7wgcv499amc8qpzkylvrlzn9q7fxhsbhw1")))

(define-public crate-jid-0.2.4 (c (n "jid") (v "0.2.4") (d (list (d (n "minidom") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0775z1ibkc1yqiwxxxl7hw0fmpw1q3yw7vys9s7dh6ghg01caa6i") (y #t)))

(define-public crate-jid-0.3.0 (c (n "jid") (v "0.3.0") (d (list (d (n "minidom") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "00cvyd3qgyvk1rb66w5w999q2bcpmzhw0larp1fqypfci04phw26")))

(define-public crate-jid-0.3.1 (c (n "jid") (v "0.3.1") (d (list (d (n "minidom") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "0ii62z68mlwrdiwgf8iwzngpw3nxsafs08g75cznziyf9579qzb5")))

(define-public crate-jid-0.4.0 (c (n "jid") (v "0.4.0") (d (list (d (n "minidom") (r "^0.7") (o #t) (d #t) (k 0)))) (h "161f4vg0bws2k4m9kvh2cz7nbx51j2g7crspgpiinai5rcrx561b")))

(define-public crate-jid-0.5.0 (c (n "jid") (v "0.5.0") (d (list (d (n "minidom") (r "^0.8.0") (o #t) (d #t) (k 0)))) (h "0m79rfwx0g5q2c25qnjl0rh14gf16dzfqc61gmhlbsmxji0ss3ix")))

(define-public crate-jid-0.5.2 (c (n "jid") (v "0.5.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "minidom") (r "^0.9.1") (o #t) (d #t) (k 0)))) (h "1l53y513ij94m4bfclvpvrld8nni6bk9kjrf41qmxsbw7diywk3c")))

(define-public crate-jid-0.5.3 (c (n "jid") (v "0.5.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "minidom") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0fjh6v0ms566vid6w813i26nfpj2l9232vhkfj0a02l6mgra7s14")))

(define-public crate-jid-0.6.0 (c (n "jid") (v "0.6.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "minidom") (r "^0.10") (o #t) (d #t) (k 0)))) (h "02cyw8zyq8fnx7dlzm98md6ski216vhzg32anpgg3vi4r43lwlf3")))

(define-public crate-jid-0.6.1 (c (n "jid") (v "0.6.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "minidom") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0lmjg7lq95jdbaz92y5hm3z47r576z0jql5f3fq03lbv9hqi2icn")))

(define-public crate-jid-0.6.2 (c (n "jid") (v "0.6.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "minidom") (r "^0.10") (o #t) (d #t) (k 0)))) (h "09lp586wvkg5dlnk1a6dr8l4qd9zy5kd7l4275522v5rhz89vq94")))

(define-public crate-jid-0.7.0 (c (n "jid") (v "0.7.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "minidom") (r "^0.11") (o #t) (d #t) (k 0)))) (h "02shca5k6r9z3swhpw8vvgdirj7hbawgpa3jxnyn68gwdzmbbcry")))

(define-public crate-jid-0.7.1 (c (n "jid") (v "0.7.1") (d (list (d (n "minidom") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0rwcc4n1kpfciwkpqiwndzwzkrqlf78w3wka7sbf84qz5rkpcdly")))

(define-public crate-jid-0.7.2 (c (n "jid") (v "0.7.2") (d (list (d (n "minidom") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0g8nvr10n1xg387bin0wwbaswwclwl3p6kkvps3c667xxp4czd1j")))

(define-public crate-jid-0.8.0 (c (n "jid") (v "0.8.0") (d (list (d (n "minidom") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0kprkklksjxqyvv6qjink2dyg8bsfr5j3n2xpn1jjjfaigcaz396")))

(define-public crate-jid-0.9.0 (c (n "jid") (v "0.9.0") (d (list (d (n "minidom") (r "^0.12") (o #t) (d #t) (k 0)))) (h "1csabd20k48830qaa69bd6byln6c4pfa7q69fzacghpi7xy9i7rx")))

(define-public crate-jid-0.9.1 (c (n "jid") (v "0.9.1") (d (list (d (n "minidom") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0gm5knpik31chijkkgbvfdh1ms87qwkpffwqinl35167s1j92c9f")))

(define-public crate-jid-0.9.2 (c (n "jid") (v "0.9.2") (d (list (d (n "minidom") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s3dl38wwhnx0pbzm4cnwqmmr09nfg4nv6w4yl3cmbkc2n7xipma")))

(define-public crate-jid-0.9.3 (c (n "jid") (v "0.9.3") (d (list (d (n "minidom") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02w7r25vqd7pxmr9cwb38maia71s9fnbcsng4m1ym1iay4j281rh")))

(define-public crate-jid-0.9.4 (c (n "jid") (v "0.9.4") (d (list (d (n "minidom") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0709s8w5j5fygq1dcgl064mxkzsd4jxsg5wqfrj44hw6xr1kpixq")))

(define-public crate-jid-0.10.0 (c (n "jid") (v "0.10.0") (d (list (d (n "memchr") (r "^2.5") (d #t) (k 0)) (d (n "minidom") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "stringprep") (r "^0.1.3") (d #t) (k 0)))) (h "0jdjlzs17dnhky7ikis4lrnlnfkc0yi0mcc615k09f39v2n2r9dl")))

