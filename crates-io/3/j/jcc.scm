(define-module (crates-io #{3}# j jcc) #:use-module (crates-io))

(define-public crate-jcc-0.1.0 (c (n "jcc") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1y5w8dcbpyhqdfv6wymqc4h5r55lx0pgkq6pp3vsg89h9mjiaj43") (y #t)))

(define-public crate-jcc-0.1.1 (c (n "jcc") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1x94kijyzdwz939jhbqf8bqyf608g6f575cmrh5mhj31zq7nb7kf") (y #t)))

(define-public crate-jcc-0.1.2 (c (n "jcc") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "187x7m4fc1dlc9knfczgxxw6s3mw9w6ps55sb47ri6iwi402f995") (y #t)))

(define-public crate-jcc-0.1.3 (c (n "jcc") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1xgwipyzkj28fxf408jkdcb681qyz3iprfr4a5d7yj9janfdpadv")))

(define-public crate-jcc-0.1.4 (c (n "jcc") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "14hl87lcsi4mn0hyqcns4z9har2gy65vlcyj9h33bwg7g12230hk")))

(define-public crate-jcc-0.1.5 (c (n "jcc") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0m14h7hpywdqadyp7f9s9g1ngc11qg6gg9f6gzd217x1c5dm7knh")))

(define-public crate-jcc-0.1.6 (c (n "jcc") (v "0.1.6") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1bq5c2zypn2b1j1hw044h5ygkpi7cs77dgqi6v5mhw5ay9rw09q8")))

(define-public crate-jcc-0.1.7 (c (n "jcc") (v "0.1.7") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0jlg2ybssbf7gfhpwybmjmih8y3shyxqnyr4bk388mqps6s6ywva")))

