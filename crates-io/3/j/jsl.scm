(define-module (crates-io #{3}# j jsl) #:use-module (crates-io))

(define-public crate-jsl-0.1.0 (c (n "jsl") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "06jmslx653a8165qjx675di796xz3ynsdrq2bmzsx228dpy1ccyn")))

(define-public crate-jsl-0.1.1 (c (n "jsl") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "18vki1m12vw9hq83k7g4wvkz00dhh1k1vvc2wia80wn9gbp1zj0s")))

(define-public crate-jsl-0.2.0 (c (n "jsl") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1w23bxs2a8cxxh8pags1kwg7f2jckbxhqs9hjzgd2b69x3f9npw8")))

(define-public crate-jsl-0.2.1 (c (n "jsl") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f15azzh4kz41sg6cnxrmfzd9xqz0l8510hsz4qjj55d0n6yxyic")))

(define-public crate-jsl-0.2.2 (c (n "jsl") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0993vilqiffq0x9wbkhi5hfq2fpx83h50kswkpnw42nx3ammvw2j")))

(define-public crate-jsl-0.2.3 (c (n "jsl") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z7xdppxwpwj48ik073gvwk16wrs90pwqvslj7npchf4vhdal62w")))

(define-public crate-jsl-0.2.4 (c (n "jsl") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12lhybsrpiw0by34fcinh9kqgl39bgsvybvsqgjyxnkhdq0jhcfn")))

(define-public crate-jsl-0.3.0 (c (n "jsl") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "07l9hlaw8a0qpxa93b81k78fmkcdhkk94ws375p3rh06228cpy58")))

(define-public crate-jsl-0.3.1 (c (n "jsl") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "json-pointer") (r "^0.3") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g1841sh5dw398phlfnjrj5qvic8bf4d93q0q7n0larpn3r5haj4")))

