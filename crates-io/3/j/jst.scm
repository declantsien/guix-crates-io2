(define-module (crates-io #{3}# j jst) #:use-module (crates-io))

(define-public crate-jst-0.0.1 (c (n "jst") (v "0.0.1") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "00ivypxlzg12dnnzhihx56dksjpd7cgpdmbb4nlalimlb5dsfsqw")))

(define-public crate-jst-0.0.2 (c (n "jst") (v "0.0.2") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0w8yv13sqvzcnkplb90f6281l2f4dni7dkhl9xggdpd2cbqir4cp")))

(define-public crate-jst-0.0.3 (c (n "jst") (v "0.0.3") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1sbnf0131iy7rxaki08k45s6p4mnbrbyq79vp4p2djg6z3s33qir")))

