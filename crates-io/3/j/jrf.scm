(define-module (crates-io #{3}# j jrf) #:use-module (crates-io))

(define-public crate-jrf-0.0.1 (c (n "jrf") (v "0.0.1") (d (list (d (n "jsonist") (r "^0.0.1") (d #t) (k 0)))) (h "1vhf4214fwmdqipr8xp0qs61il0id33yha6a3ghhyz43dk0dyvnp")))

(define-public crate-jrf-0.0.2 (c (n "jrf") (v "0.0.2") (d (list (d (n "jsonist") (r "^0.0.3") (d #t) (k 0)))) (h "1ffcvlpdak29fk7s3rgl6civ4qbj0wmbczkicdsywwq5n7j2ch9i")))

