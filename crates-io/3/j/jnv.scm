(define-module (crates-io #{3}# j jnv) #:use-module (crates-io))

(define-public crate-jnv-0.1.0 (c (n "jnv") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 0)) (d (n "j9") (r "^0.1.1") (d #t) (k 0)) (d (n "promkit") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "1ns348dal080mks7g2xw057x434h8fzp3qqbx1lkqpzf81g1h1yz")))

(define-public crate-jnv-0.1.1 (c (n "jnv") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 0)) (d (n "j9") (r "^0.1.2") (d #t) (k 0)) (d (n "promkit") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "1lair4mbczlisd9fwi1zx1h5a9lfq5ccwdclc46bha8qcdpr1w11")))

(define-public crate-jnv-0.1.2 (c (n "jnv") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 0)) (d (n "j9") (r "^0.1.2") (d #t) (k 0)) (d (n "promkit") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "0i61zaxvj2mpv1pzdzly88z7bmmwkks2rcd1a81g9inahb7l5dvc")))

(define-public crate-jnv-0.1.3 (c (n "jnv") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 0)) (d (n "j9") (r "^0.1.2") (d #t) (k 0)) (d (n "promkit") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "0xmy7b0nkjd9d14z8vmiysamsc0024jq22868yfgi7nq9z481lg6")))

(define-public crate-jnv-0.2.0 (c (n "jnv") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 0)) (d (n "j9") (r "^0.1.3") (d #t) (k 0)) (d (n "promkit") (r "^0.3.2") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "0bvw89myl2r4slchq9g0q1xbwnzh6iyzgzcgg5ql9badrfbbm0ks")))

(define-public crate-jnv-0.2.1 (c (n "jnv") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 0)) (d (n "j9") (r "^0.1.3") (d #t) (k 0)) (d (n "promkit") (r "^0.3.3") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "14nja9mc5240rnr5dr3ixki2p48b8r8fbhdn3j2jsarhkzl9c9zx")))

(define-public crate-jnv-0.2.2 (c (n "jnv") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 0)) (d (n "j9") (r "^0.1.3") (d #t) (k 0)) (d (n "promkit") (r "^0.4.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "18rnpzg30kkn8q8dicnfhbjv06mw9827wj52wd0pq4bjkd1q3zgq")))

(define-public crate-jnv-0.2.3 (c (n "jnv") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 0)) (d (n "j9") (r "^0.1.3") (d #t) (k 0)) (d (n "promkit") (r "^0.4.3") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2.1") (d #t) (k 0)))) (h "1wb6h2bjlm6mpklszs24zbr4mzhqlj4fsvbzmq15xkzikln5kahc")))

