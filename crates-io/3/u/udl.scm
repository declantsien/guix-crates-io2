(define-module (crates-io #{3}# u udl) #:use-module (crates-io))

(define-public crate-udl-0.1.0 (c (n "udl") (v "0.1.0") (h "1wdmimmg6fkymyb4ifmk37i782f4hdgd3b6kgwlk597hr7ydq5a9") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "common" "parse") ("common"))))))

(define-public crate-udl-0.2.0 (c (n "udl") (v "0.2.0") (h "07iz7hzxz3h93v5z2cqlv02s4iazs8s0718jfjs56qzyrmsmj2j6") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "common" "parse") ("common"))))))

(define-public crate-udl-0.3.0 (c (n "udl") (v "0.3.0") (h "0rqb4d6ahdslcvdmaf6cld3hvw34yzyh1r57yc3qs4cd6pyfzzsy") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "common" "parse") ("common"))))))

(define-public crate-udl-0.3.1 (c (n "udl") (v "0.3.1") (h "06kc3fcdsnzangfl64r93svh791m6vz2w7951sw9jyc2dwkif4iy") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "common" "parse") ("common"))))))

