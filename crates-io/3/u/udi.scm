(define-module (crates-io #{3}# u udi) #:use-module (crates-io))

(define-public crate-udi-0.0.1 (c (n "udi") (v "0.0.1") (d (list (d (n "derive-error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "downcast-rs") (r "= 1.0.1") (d #t) (k 0)) (d (n "error-chain") (r "0.11.*") (d #t) (k 0)) (d (n "mio") (r "^0.6.6") (d #t) (k 0)) (d (n "native-file-tests") (r "^0.1.0") (d #t) (k 1)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.6") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "users") (r "^0.5.2") (d #t) (k 0)))) (h "055v31x7ryyfp2xglrr01ydhi1a0d9i7mcknf7xpm5i70l2k552a")))

