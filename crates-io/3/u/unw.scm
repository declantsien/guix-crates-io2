(define-module (crates-io #{3}# u unw) #:use-module (crates-io))

(define-public crate-unw-0.1.0 (c (n "unw") (v "0.1.0") (d (list (d (n "fallible") (r "^0.1") (d #t) (k 0)) (d (n "null-terminated") (r "^0.2.3") (d #t) (k 0)))) (h "1kca8cas3q7ilrlddrqwx8dxyfvrlnyh17aj25cqy6m5ph3kq41m")))

