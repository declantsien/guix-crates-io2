(define-module (crates-io #{3}# u ump) #:use-module (crates-io))

(define-public crate-ump-0.1.1 (c (n "ump") (v "0.1.1") (h "1ksi1acm9ikwv6433ic8x93m51wx5rz4lbisymwzzf78j0nbhnjd")))

(define-public crate-ump-0.5.0 (c (n "ump") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1gdyzjx5harc6n9i779m6gvxlb70b5lxf09k0g3n1zfd7ir9sn7f")))

(define-public crate-ump-0.6.0 (c (n "ump") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "sigq") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "006xm3df6pm1kmas4ll8p4immawz6lmp47l9ic33bi5w1pcmmyw4")))

(define-public crate-ump-0.6.4 (c (n "ump") (v "0.6.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "sigq") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1ldz84lwji44qjqdgd5662hxiscf5mzb7yhjs1xlm4s1kcng1pw5")))

(define-public crate-ump-0.7.0 (c (n "ump") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "sigq") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0kyphbxcw96ga3md66zqssl1y9lxs3a3h8yc30lgpavn1wd5mg4d")))

(define-public crate-ump-0.8.0 (c (n "ump") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "sigq") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1kwhb0mkl2ij6iza5hq1wf1wxhlc4s47c9pjz9kni806xp1qgid8")))

(define-public crate-ump-0.8.1 (c (n "ump") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "sigq") (r "^0.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "rt-multi-thread"))) (d #t) (k 2)))) (h "1i7bynp92x9z8wibyc1r7xjz00k8g5k4dynnxr80ph7jq6vf96vk")))

(define-public crate-ump-0.9.0 (c (n "ump") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "sigq") (r "^0.11.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.0") (f (quote ("full"))) (d #t) (k 2)))) (h "11skh2w8p5xh6wxxhpclhzfnss47amvw9c0bdjs3ijp98g96hkwv") (r "1.39")))

(define-public crate-ump-0.10.0 (c (n "ump") (v "0.10.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "sigq") (r "^0.13.2") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1h89vfc0vixljhqw4drq33r78hwvwmy5dvbyg066vz014xs9d9bq") (r "1.39")))

(define-public crate-ump-0.10.1 (c (n "ump") (v "0.10.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "sigq") (r "^0.13.3") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "157kjnfmri9dvh358ikpil099i6b0348m77zfrkyph1c7xg70blr") (r "1.39")))

(define-public crate-ump-0.10.2 (c (n "ump") (v "0.10.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "sigq") (r "^0.13.3") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0n9n08q4h0hg62zppv0pa3znhzfbrswix505iri738rhd5w7jspq") (f (quote (("dev-docs")))) (r "1.39")))

(define-public crate-ump-0.11.0 (c (n "ump") (v "0.11.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "sigq") (r "^0.13.3") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1vbyg17rn59hjcp2zfihp806biicl7spybiqkwrfrp20l6km5rhx") (f (quote (("dev-docs")))) (r "1.39")))

(define-public crate-ump-0.12.0 (c (n "ump") (v "0.12.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "sigq") (r "^0.13.3") (d #t) (k 0)) (d (n "swctx") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.31.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "1y768gp39cd4bn402z885lsinn9chrzgxnv9ljghy9ldkqyh0l97") (f (quote (("dev-docs")))) (r "1.56")))

(define-public crate-ump-0.12.1 (c (n "ump") (v "0.12.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "sigq") (r "^0.13.4") (d #t) (k 0)) (d (n "swctx") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "08zy7xh5wd2kr1s71m3m9jivqr2600lr52zn24649xzd71y3hwm2") (f (quote (("dev-docs")))) (r "1.56")))

