(define-module (crates-io #{3}# u uci) #:use-module (crates-io))

(define-public crate-uci-0.1.0 (c (n "uci") (v "0.1.0") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "1svc0d3asg7x8y1yzrqc09nwd4fidai8b3pr2nra7niy4vq5jz4a")))

(define-public crate-uci-0.1.1 (c (n "uci") (v "0.1.1") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "0ap5b6rgxvlxpn704v31ipnnivfdwc2pvvmdbyyp3sbmaqwc3bj5")))

(define-public crate-uci-0.1.2 (c (n "uci") (v "0.1.2") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "0aqpklbsf8ds18ahcpnlsgkwrvdnx5ayzgca8wzflhpmvcwlpbp6")))

(define-public crate-uci-0.1.3 (c (n "uci") (v "0.1.3") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "1q2y4pi5i99cdn4ql3ssgnbq59qlk58x33xas4clpac3whnmnw6r")))

(define-public crate-uci-0.1.4 (c (n "uci") (v "0.1.4") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "0kzgbkdph5y216p5q2ak1yzf33wlzlqybs0gfhfcpd90pkb419qp")))

