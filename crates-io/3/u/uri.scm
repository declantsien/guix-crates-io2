(define-module (crates-io #{3}# u uri) #:use-module (crates-io))

(define-public crate-uri-0.1.0 (c (n "uri") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0dy679dvpz7v8xsbaxakwrvgkbxa73vsz7h1z5ng1m8h7qzw50gn") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-uri-0.1.1 (c (n "uri") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.37") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1bchpw8pk1ympbsf24ggfkx74p5xi9jfw9wm8gbs9crp9lhqrbmf") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-uri-0.2.0 (c (n "uri") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.64") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "19496yw8n4mzymnbpj4mxnmqcyfgs12sk71g28shniyax42csssh") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-uri-0.2.1 (c (n "uri") (v "0.2.1") (d (list (d (n "clippy") (r "^0.0.64") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1qx7h507mf22mjfdlr0r4dxqi99z5dia0886dwx5jnwgqsr4migy") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-uri-0.3.0 (c (n "uri") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0.64") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1d9kk69rmvs903s3x4glqrxwmjfpmbwmqkpcj1h57vw03983i84a") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-uri-0.4.0 (c (n "uri") (v "0.4.0") (h "17w285r4s2nys7nf9nybli6l1v8m5wjzwvky9k8x01b2kvm1zimm")))

