(define-module (crates-io #{3}# u ups) #:use-module (crates-io))

(define-public crate-ups-0.1.0 (c (n "ups") (v "0.1.0") (h "0zi6dxin8nskhak839vlcbwkcpxxnzx2h8y05xs6dga939awn03n")))

(define-public crate-ups-0.2.0 (c (n "ups") (v "0.2.0") (h "1m6922ls9fmhky1q0vpv917577vvqx6a9rlk3j1qf8i0g6d5nqbs")))

(define-public crate-ups-0.2.1 (c (n "ups") (v "0.2.1") (h "0mbnjqnacnc3gzjc026h5l4al257bv740f76ymyk1fwph37dilnj")))

(define-public crate-ups-0.3.0 (c (n "ups") (v "0.3.0") (h "05cvmwgpnyhmqrzb9a7l25mvjx5y58bdd0cc83pv8i58lqgvjaz7")))

(define-public crate-ups-0.3.1 (c (n "ups") (v "0.3.1") (h "1xmsj446hsch1zsf6ia565xjbx5qabx3y3lqsafh17dd6pdncjrh")))

