(define-module (crates-io #{3}# u upl) #:use-module (crates-io))

(define-public crate-upl-0.0.0 (c (n "upl") (v "0.0.0") (h "09cl9h786qh6jzbgizxdirvsi9dmmmp1g22sqkc8iyhiq1ncrgip")))

(define-public crate-upl-0.1.0 (c (n "upl") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "multipart"))) (d #t) (k 0)))) (h "1smr56lab2n79b90m463p2kg4mzryiiv2pgm78fw5dkrl3sp4gf1")))

(define-public crate-upl-0.1.1 (c (n "upl") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.9") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("blocking" "multipart"))) (d #t) (k 0)))) (h "0h9s6qaaqsd6vw2n7f6g3x3fqlnvik2kfhrz2mr43rznn0xnq0l5")))

