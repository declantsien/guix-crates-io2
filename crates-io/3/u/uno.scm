(define-module (crates-io #{3}# u uno) #:use-module (crates-io))

(define-public crate-uno-0.1.0 (c (n "uno") (v "0.1.0") (d (list (d (n "en") (r "^0.1.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0nz0gdj6ivmlp0cpr3m4v2p7ndfnlwrwy140ba8yw2ca4wy56w02") (f (quote (("default"))))))

