(define-module (crates-io #{3}# u ukf) #:use-module (crates-io))

(define-public crate-ukf-0.1.0 (c (n "ukf") (v "0.1.0") (h "18wgfsn8mq86r60gz52ix32wa132fcz96qwkn5098rgk8yvxmiky")))

(define-public crate-ukf-0.1.1 (c (n "ukf") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.21.0") (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "1cbwvg4xk8fzr52db9png25lid5q44n97r1hkj9acmgzsk71y3xz")))

(define-public crate-ukf-0.1.2 (c (n "ukf") (v "0.1.2") (d (list (d (n "fehler") (r "^1.0.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.14.1") (d #t) (k 0)) (d (n "kf") (r "^0.1.3") (d #t) (k 2)) (d (n "nalgebra") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)) (d (n "typenum") (r "^1.12.0") (d #t) (k 0)))) (h "1k8rj4g8swb6lnyc68dgs4930p707arwp6spfjjipjbi7271x845")))

