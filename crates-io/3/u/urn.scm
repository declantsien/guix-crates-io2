(define-module (crates-io #{3}# u urn) #:use-module (crates-io))

(define-public crate-urn-0.1.0 (c (n "urn") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 1)) (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 1)))) (h "0mvqg4jra0c0w1qyfci966nymnzyvssr6065aqvlyh4a2qwi3r0n") (f (quote (("std" "displaydoc/std") ("default" "std"))))))

(define-public crate-urn-0.1.1 (c (n "urn") (v "0.1.1") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 1)) (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 1)))) (h "19ydwm67l0m5yqy3agxp3yb81cnlp20w1yc0jppdij1f8d6i51bn") (f (quote (("std" "displaydoc/std") ("default" "std"))))))

(define-public crate-urn-0.2.0 (c (n "urn") (v "0.2.0") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 1)))) (h "0dv6lg6n22v9pyhiakf6h9d4s0899a9dqls8x74khrhv0z018lv9") (f (quote (("std" "displaydoc/std") ("default" "std")))) (y #t)))

(define-public crate-urn-0.2.1 (c (n "urn") (v "0.2.1") (d (list (d (n "displaydoc") (r "^0.2.3") (k 0)) (d (n "kuchiki") (r "^0.8.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 1)))) (h "1b9d8d456l0v70337nynii3dgjxq9r5sawmknhxdxkhg72j8q8mg") (f (quote (("std" "displaydoc/std") ("default" "std"))))))

(define-public crate-urn-0.3.0 (c (n "urn") (v "0.3.0") (h "0kia28sa5knf90kdl6g77b5d2hxjp49pz122axykbq7pvadmrjw2") (f (quote (("std") ("default" "std"))))))

(define-public crate-urn-0.3.1 (c (n "urn") (v "0.3.1") (h "0kaglqwka46f7ifr4az8p9k4vl7ym5lm2r3k78049c76dqy6cr8x") (f (quote (("std") ("default" "std"))))))

(define-public crate-urn-0.3.2 (c (n "urn") (v "0.3.2") (h "0xpmxfzkzfxmn7x2b37k28jivsf2craazl6rkhwxdzml1799l72s") (f (quote (("std") ("default" "std"))))))

(define-public crate-urn-0.3.3 (c (n "urn") (v "0.3.3") (h "1fhx2w2l74jqcd97f3w92nrsb024am5r47vwviymi5mmf9sxxbhy") (f (quote (("std") ("default" "std"))))))

(define-public crate-urn-0.3.4 (c (n "urn") (v "0.3.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0c69hay78pf2w62d3smi5hsmv9h86apngryj866xhdr73wy0gl99") (f (quote (("std") ("default" "std"))))))

(define-public crate-urn-0.4.0 (c (n "urn") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "06nflygj99xxnrcni3pih59gbkpvfw6b3l49id7871gqypi1gpcz") (f (quote (("std") ("default" "std"))))))

(define-public crate-urn-0.5.0 (c (n "urn") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "18v3w48f063100pjz4mdclg7wvqp2ddx7arjy44lgfadpvy8l7qk") (f (quote (("std") ("default" "std"))))))

(define-public crate-urn-0.5.1 (c (n "urn") (v "0.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "07h1infajlkri73nn08kmmdhn9gnfh1valsq9qi2sh91p0sg5sk7") (f (quote (("std") ("default" "std"))))))

(define-public crate-urn-0.6.0-alpha.1 (c (n "urn") (v "0.6.0-alpha.1") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1f699qncp0mr4hiw7r983rd1hbsgxfizg55zwzsasyqf0vrk5aa8") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-urn-0.6.0-alpha.2 (c (n "urn") (v "0.6.0-alpha.2") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0l8y4sf872xkl3xsmq65g9zzb5kwxl79z490xdvy86i639qh4j28") (f (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc" "serde/alloc"))))))

(define-public crate-urn-0.6.0 (c (n "urn") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1xxcc6vwq9sibvpmjf0qql52dx0ck9jg7hs7p2jicxq2xa4b3n28") (f (quote (("std" "alloc") ("nightly") ("default" "std")))) (s 2) (e (quote (("alloc" "serde?/alloc"))))))

(define-public crate-urn-0.7.0 (c (n "urn") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "130msa4lihzv4sr42bg2n06nr1fj13xvh1kjkpil39sqvwyzpp48") (f (quote (("std" "alloc") ("nightly") ("default" "std")))) (s 2) (e (quote (("alloc" "serde?/alloc"))))))

