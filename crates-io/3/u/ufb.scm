(define-module (crates-io #{3}# u ufb) #:use-module (crates-io))

(define-public crate-ufb-0.1.0 (c (n "ufb") (v "0.1.0") (d (list (d (n "glfw") (r "^0.41") (d #t) (k 0)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("jpeg"))) (k 2)))) (h "0qxmnl8prpvwld8g20d45vqv94hrywxpd23rxw86bvnvcc249aaf") (y #t)))

(define-public crate-ufb-0.1.1 (c (n "ufb") (v "0.1.1") (d (list (d (n "glfw") (r "^0.41") (d #t) (k 0)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("jpeg"))) (k 2)))) (h "0kkiq8vpql9liwfxvv719kfvb2m91d1f4sfrl1jh5pcbmymwg983") (y #t)))

(define-public crate-ufb-0.1.2 (c (n "ufb") (v "0.1.2") (d (list (d (n "glfw") (r "^0.41") (d #t) (k 0)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("jpeg"))) (k 2)))) (h "0q46i25z9ipm441rc5r6axbmcfl5r9az0jv27djsc4yfylg6wg3f") (y #t)))

(define-public crate-ufb-0.1.3 (c (n "ufb") (v "0.1.3") (d (list (d (n "glfw") (r "^0.41") (d #t) (k 0)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("jpeg"))) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1ni3brpl9gsa644j1bjqrrcqlwlzdbspxxr97ma2a34y4yd6vchc") (y #t)))

(define-public crate-ufb-0.1.4 (c (n "ufb") (v "0.1.4") (d (list (d (n "glfw") (r "^0.41") (d #t) (k 0)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("jpeg"))) (k 2)) (d (n "num-complex") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "02q6hw3siwi0hdm6iry43bd36v6nbxsqram0a35zsgkwmpjkb0gk") (y #t)))

(define-public crate-ufb-0.2.0 (c (n "ufb") (v "0.2.0") (d (list (d (n "glfw") (r "^0.41") (d #t) (k 0)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("jpeg"))) (k 2)) (d (n "num-complex") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1al9s1z555dbv92chd4ykypi7yg3dqjmni0s8xzjrinfv3piiryj")))

(define-public crate-ufb-0.2.1 (c (n "ufb") (v "0.2.1") (d (list (d (n "glfw") (r "^0.41") (d #t) (k 0)) (d (n "glu-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "image") (r "^0.23") (f (quote ("jpeg"))) (k 2)) (d (n "num-complex") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1hfn8zvhsmy1qmghp1r91x8671x16iwp1l670glp7v3rn0sy7ilc")))

