(define-module (crates-io #{3}# u ubs) #:use-module (crates-io))

(define-public crate-ubs-0.0.0 (c (n "ubs") (v "0.0.0") (h "0cyckqp1bhq6jivi57mkig7dhsjwz6d9g5v30yl6c01pmfa8sc0x")))

(define-public crate-ubs-0.1.0 (c (n "ubs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "syntect") (r "^5.1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "macros"))) (k 0)) (d (n "ubs-lib") (r "^0.1.0") (d #t) (k 0)))) (h "1dbkwgkz1f62d2ri2idwdiw6kkf7sw0lvxasqjbf7dj0jsa55kkb") (f (quote (("default" "color") ("color" "syntect"))))))

(define-public crate-ubs-0.1.1 (c (n "ubs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "syntect") (r "^5.1.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("rt" "macros"))) (k 0)) (d (n "ubs-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0a51jijn0rpvd5my8kz7jwph19iwixya1sr2m7z4jk98db30kag0") (f (quote (("default" "color") ("color" "syntect"))))))

