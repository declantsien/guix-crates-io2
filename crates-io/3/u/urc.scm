(define-module (crates-io #{3}# u urc) #:use-module (crates-io))

(define-public crate-urc-0.1.0 (c (n "urc") (v "0.1.0") (h "001bv0h74d5av5jx3vqmspnvpidhnb6nmsm0mw2ywmb67yfw4g33")))

(define-public crate-urc-0.2.0 (c (n "urc") (v "0.2.0") (h "00pickfwb84hz0ibyzn49wqm7kjfd7phqygqipdhqjkmrpdy0cmy")))

