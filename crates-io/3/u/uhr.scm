(define-module (crates-io #{3}# u uhr) #:use-module (crates-io))

(define-public crate-uhr-0.1.0 (c (n "uhr") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "gregor") (r "^0.3.2") (d #t) (k 0)) (d (n "heapless") (r "^0.4.2") (d #t) (k 0)))) (h "0wlgjm7h3b1hsip2ssrjygiszrjhanqplqgf2gsawrn584r526ly")))

(define-public crate-uhr-0.2.0 (c (n "uhr") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.11") (d #t) (k 0)) (d (n "gregor") (r "^0.3.2") (d #t) (k 0)) (d (n "heapless") (r "^0.4.2") (d #t) (k 0)))) (h "0rfdyql9mhhk189wi1zcgppmc0mrfp71b44nag5pf8c91jb3qr8n")))

