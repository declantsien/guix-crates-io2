(define-module (crates-io #{3}# u utm) #:use-module (crates-io))

(define-public crate-utm-0.1.0 (c (n "utm") (v "0.1.0") (h "0jcx92dn8if1b582wz64hwp8xgh4qymqk51iib13cqh0lk1ibj78")))

(define-public crate-utm-0.1.1 (c (n "utm") (v "0.1.1") (h "06hm5ms6z7w024kji7hib3s4v2j02q4m3j8dih7f58ri35xkxhj4") (y #t)))

(define-public crate-utm-0.1.2 (c (n "utm") (v "0.1.2") (h "0jp0gdy03ird7nwgl0ap6xn9f6ly1205dlg7mjw9bf9h8r2mihji")))

(define-public crate-utm-0.1.3 (c (n "utm") (v "0.1.3") (d (list (d (n "num") (r "^0.3.0") (o #t) (d #t) (k 0)))) (h "0z4yk298xggrx2m01jk0fcphf2p2glzygrvmrlgwd2pwcqzyq6as") (f (quote (("no_std" "num"))))))

(define-public crate-utm-0.1.4 (c (n "utm") (v "0.1.4") (d (list (d (n "num") (r "^0.3.0") (f (quote ("libm"))) (o #t) (k 0)))) (h "1bahw5h608rzfkkjq3w8939vjaaim077b8gg4gl15lr36r7bl6dy") (f (quote (("no_std" "num"))))))

(define-public crate-utm-0.1.5 (c (n "utm") (v "0.1.5") (d (list (d (n "num") (r "^0.4.0") (f (quote ("libm"))) (o #t) (k 0)))) (h "0x55mb8k5aiffd2q8v5hiy2n00jk23cl1xfq2ypd5a8wms19ykhd") (f (quote (("no_std" "num"))))))

(define-public crate-utm-0.1.6 (c (n "utm") (v "0.1.6") (d (list (d (n "num") (r "^0.4.0") (f (quote ("libm"))) (o #t) (k 0)))) (h "0g2cvj987w8w8pfll5960v4d3nbmi1xnnfk7fz5irg8a78xrxc4m") (f (quote (("no_std" "num"))))))

