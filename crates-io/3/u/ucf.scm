(define-module (crates-io #{3}# u ucf) #:use-module (crates-io))

(define-public crate-ucf-0.1.0 (c (n "ucf") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.148") (d #t) (k 0)))) (h "1qak7yik28j0bcanzknqsak4fpvkbkanzswx6f5023avijs6sszw")))

(define-public crate-ucf-0.1.1 (c (n "ucf") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.148") (d #t) (k 0)))) (h "0ikphrr0n8kb3dlh2jl1qjjx2f060arnd4xqy63iklaa1awcrxgp")))

(define-public crate-ucf-0.1.2 (c (n "ucf") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.148") (d #t) (k 0)))) (h "00m1qqrihkhgijdz2i7nnzbqiyqm9ri6ggav4l3lk7dqw3kdi5vg")))

(define-public crate-ucf-0.1.3 (c (n "ucf") (v "0.1.3") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "1ww1kdnsfjrm74z623pn3cqnhmfc62s83nlmra9kgihfqrg0ixgn")))

(define-public crate-ucf-0.1.4 (c (n "ucf") (v "0.1.4") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "18zix5mmyf043crkg37yzxxb3r72536229d499yb476vqiid9z7m")))

(define-public crate-ucf-0.1.5 (c (n "ucf") (v "0.1.5") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)))) (h "1gz8fqjykqlaa4h4smnk8h5r2gfzrg3gx1br4fhhv2j1l0ib5wp8")))

