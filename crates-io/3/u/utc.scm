(define-module (crates-io #{3}# u utc) #:use-module (crates-io))

(define-public crate-utc-0.1.0 (c (n "utc") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0ifh43sasfydh9d7gmd675x08a65h1fg0jdjw4r0zjjmq6ih1smf")))

(define-public crate-utc-0.2.0 (c (n "utc") (v "0.2.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1fifz4a33bm22wxsb0l1wrn0gpsbnm5v68cxyvxn6fxsqns7xmy0")))

