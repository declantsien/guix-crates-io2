(define-module (crates-io #{3}# u uhd) #:use-module (crates-io))

(define-public crate-uhd-0.1.0 (c (n "uhd") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.3.1") (d #t) (k 0)) (d (n "uhd-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0262llzzqqiraf4628k2q9ydnlb4paap1xcs82ihlh5yrlfwg1y1")))

(define-public crate-uhd-0.1.1 (c (n "uhd") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num-complex") (r "^0.3.1") (d #t) (k 0)) (d (n "uhd-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0j5jygbljwlvrnqplrkcagkag0fm2bcyd932lln3n00znvmaxmc8")))

(define-public crate-uhd-0.2.0 (c (n "uhd") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "uhd-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0c7g7zvpsgdj1w7gq59ml8d9x1cii9ns5gz881zfsj1sndxdnn8w")))

(define-public crate-uhd-0.3.0 (c (n "uhd") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.39") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.13") (d #t) (k 2)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "uhd-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0r4zp1ywzbs8zqdlvdsniy8ijcwly5jzqwqvlkly8v26a66cklkh")))

