(define-module (crates-io #{3}# u u_u) #:use-module (crates-io))

(define-public crate-u_u-0.0.0 (c (n "u_u") (v "0.0.0") (h "1lqnkjy8n6qp8psmzj6bdsjcr0fha56zn334gy4g9601wsn1y18v")))

(define-public crate-u_u-0.1.0 (c (n "u_u") (v "0.1.0") (d (list (d (n "jpeg-decoder") (r "^0.3.0") (d #t) (k 0)) (d (n "png") (r "^0.17.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "visioncortex") (r "^0.8.0") (d #t) (k 0)) (d (n "vtracer") (r "^0.5.0") (d #t) (k 0)))) (h "1zz89yhxkrhdkyf6xp3v6ffr97adhpm87h82lnhgzpdrzjky6c32") (f (quote (("default") ("debug"))))))

