(define-module (crates-io #{3}# u uid) #:use-module (crates-io))

(define-public crate-uid-0.0.0 (c (n "uid") (v "0.0.0") (h "13x6bs0n94k8l231k8ln5g684whfn2ahqjzdl3b6wmyw8q6lbd3r")))

(define-public crate-uid-0.1.0 (c (n "uid") (v "0.1.0") (h "1cc918859gg81brbmbcgry8bp0xy4hmzg8ly9mgqghbxzlwwnwyg")))

(define-public crate-uid-0.1.1 (c (n "uid") (v "0.1.1") (h "07126xj5z1fhgzvbh9w4by7iibl2r2yqihh9kb183j4hl5fc8rzc")))

(define-public crate-uid-0.1.2 (c (n "uid") (v "0.1.2") (h "06yjm8w2ci0f6lkjscy06giwwraa20apxygk2grbmsna5dlp2dgy")))

(define-public crate-uid-0.1.3 (c (n "uid") (v "0.1.3") (h "13dy03y64f7892ydb5lsq8wi4my22bnqdagj3qljcqjax117mhqd")))

(define-public crate-uid-0.1.4 (c (n "uid") (v "0.1.4") (h "1mqb1vlfn2kvj4mky97kqc1j6i9nvk7r0c63amssh730b1dwpjjh")))

(define-public crate-uid-0.1.5 (c (n "uid") (v "0.1.5") (h "01724f6j5mr092prnrkkcaiwfaqq6r3qsmhg6flwm6c9yfggck08")))

(define-public crate-uid-0.1.6 (c (n "uid") (v "0.1.6") (h "18nk83knsrcb4hwvm0vc6fnpsmw2dnnm9vdvblvarzvbip9v1x52")))

(define-public crate-uid-0.1.7 (c (n "uid") (v "0.1.7") (h "1pqy40r63df0dibw9p6axc6nwj9wpv8q5gx70s68zrhf33j126i8")))

