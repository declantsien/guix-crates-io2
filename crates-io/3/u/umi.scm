(define-module (crates-io #{3}# u umi) #:use-module (crates-io))

(define-public crate-umi-0.0.0 (c (n "umi") (v "0.0.0") (h "1sh04qkywyvsq0qqb4705fdw23kv6995q029c7i37avhkqwb8kqv")))

(define-public crate-umi-0.0.1 (c (n "umi") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hftwo") (r "^0.1.0") (d #t) (k 0)) (d (n "open") (r "^5.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.11.0") (d #t) (k 0)) (d (n "uftwo") (r "^0.1.0") (d #t) (k 0)))) (h "0cbjmzwrxaqaf958v0cn7ls5z322gbamsbbkk3c15ccbqbaakdlk")))

(define-public crate-umi-0.0.2 (c (n "umi") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hftwo") (r "^0.1.2") (d #t) (k 0)) (d (n "open") (r "^5.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full" "net"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.11.0") (d #t) (k 0)) (d (n "uftwo") (r "^0.1.0") (d #t) (k 0)))) (h "1321wj96zvwipj9hvas5fnn194w8q0phgy8k7c5p760i7srr38pp")))

(define-public crate-umi-0.0.3 (c (n "umi") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hftwo") (r "^0.1.2") (d #t) (k 0)) (d (n "open") (r "^5.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full" "net"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.11.0") (d #t) (k 0)) (d (n "uftwo") (r "^0.1.0") (d #t) (k 0)))) (h "1qycg24wbin6h6diwziiravf5ayhk96lpw6akrzp60nhfqlyk5w9")))

(define-public crate-umi-0.0.4 (c (n "umi") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hftwo") (r "^0.1.2") (d #t) (k 0)) (d (n "open") (r "^5.1.2") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full" "net"))) (d #t) (k 0)) (d (n "tokio-modbus") (r "^0.11.0") (d #t) (k 0)) (d (n "uftwo") (r "^0.1.0") (d #t) (k 0)))) (h "07nki7v7llv38a699adgh5wq7wqq4ip75vvx9cw6r05p2qp90138")))

