(define-module (crates-io #{3}# u uap) #:use-module (crates-io))

(define-public crate-uap-0.2.0 (c (n "uap") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qf9516zn93svx0n0yii94pj3qv33dpgd50ybbi7zgkljvjy2hn8")))

(define-public crate-uap-0.2.1 (c (n "uap") (v "0.2.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "merge") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11gg14xlihq77ji5h8yvgmivfxvc4sv184q6f9lj71r7i2lc22lw")))

