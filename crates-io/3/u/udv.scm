(define-module (crates-io #{3}# u udv) #:use-module (crates-io))

(define-public crate-udv-0.0.1 (c (n "udv") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.0") (k 0)))) (h "034gdzynbq49r99vi6qg2qd55875ayl6cxz9sfiv01sxl4s1wimr") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-udv-0.1.0 (c (n "udv") (v "0.1.0") (d (list (d (n "nom") (r "^7.1.0") (k 0)))) (h "1np2dbpdmkx3lgr4ai07sxazc9jqzqzdslyzpbqwvg8linhjhzkn") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-udv-0.1.1 (c (n "udv") (v "0.1.1") (d (list (d (n "nom") (r "^7.1.0") (k 0)))) (h "0m7p4jvwvcx9zm1scfbqdbd3j693g7hkwqj0mb1k6ic6h3bf8f0f") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-udv-0.3.0 (c (n "udv") (v "0.3.0") (d (list (d (n "nom") (r "^7.1.0") (k 0)))) (h "09my2fzajqq2awcxbs9yjgrm0jxvld2c4bdxxaix90qiki8j3sd6") (f (quote (("std" "nom/std") ("default" "std"))))))

(define-public crate-udv-0.3.1 (c (n "udv") (v "0.3.1") (d (list (d (n "nom") (r "^7.1.0") (k 0)))) (h "1fcg23fcx10llzvvpvyxvsjz860ld6ci9fr80gys4jg5cmvvbmxm") (f (quote (("std" "nom/std") ("default" "std"))))))

