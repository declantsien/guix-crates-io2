(define-module (crates-io #{3}# u uvc) #:use-module (crates-io))

(define-public crate-uvc-0.1.0 (c (n "uvc") (v "0.1.0") (d (list (d (n "glium") (r "^0.22.0") (d #t) (k 2)) (d (n "uvc-sys") (r "^0.1.0") (d #t) (k 0)))) (h "16w0bxpwmn3ki75h20rjwsc07pv1sscnlkcm9ckfq80bzvz66v49")))

(define-public crate-uvc-0.1.1 (c (n "uvc") (v "0.1.1") (d (list (d (n "glium") (r "^0.22.0") (d #t) (k 2)) (d (n "uvc-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0qb50m45gbm87d7wvdj83jv2daxnyawiz6khkd1fh523y5baiicy")))

(define-public crate-uvc-0.1.2 (c (n "uvc") (v "0.1.2") (d (list (d (n "glium") (r "^0.22.0") (d #t) (k 2)) (d (n "uvc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0rvdxmcahhxxjrl0yasklld8qbf62s8hcrnhhis57jbz1jwps1mf")))

(define-public crate-uvc-0.1.3 (c (n "uvc") (v "0.1.3") (d (list (d (n "glium") (r "^0.22.0") (d #t) (k 2)) (d (n "uvc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1c711mwk9l17ki0cg32wx8s6h0l0pswp9ga8q1ah2qr3984kmbzr")))

(define-public crate-uvc-0.1.4 (c (n "uvc") (v "0.1.4") (d (list (d (n "glium") (r "^0.22.0") (d #t) (k 2)) (d (n "uvc-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1x5hf0pz2sfz03z9x1ic081751lmk2lh8g3knzyqx41jg08afwvp")))

(define-public crate-uvc-0.1.5 (c (n "uvc") (v "0.1.5") (d (list (d (n "glium") (r "^0.22.0") (d #t) (k 2)) (d (n "uvc-src") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "uvc-sys") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "1jzz06s9vv304cacnswxg1y34sd2cbsfc569frdnljadz5p5nsl1") (f (quote (("vendor" "uvc-src") ("system" "uvc-sys") ("default" "system"))))))

(define-public crate-uvc-0.1.6 (c (n "uvc") (v "0.1.6") (d (list (d (n "glium") (r "^0.27.0") (d #t) (k 2)) (d (n "uvc-src") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "uvc-sys") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1dg77api2r63hz4danxa2rkcmccm7z3ihr9hrqqimf98fsispv46") (f (quote (("vendor" "uvc-src") ("system" "uvc-sys") ("default" "system"))))))

(define-public crate-uvc-0.1.7 (c (n "uvc") (v "0.1.7") (d (list (d (n "glium") (r "^0.27.0") (d #t) (k 2)) (d (n "uvc-src") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "uvc-sys") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0gqv518pxq1my1dz53q48h7r43h9fbsgnqi316zg0vzvcrhb3ra5") (f (quote (("vendor" "uvc-src") ("system" "uvc-sys") ("default" "system")))) (y #t)))

(define-public crate-uvc-0.1.8 (c (n "uvc") (v "0.1.8") (d (list (d (n "glium") (r "^0.27.0") (d #t) (k 2)) (d (n "uvc-src") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "uvc-sys") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1gxly5hlqnj034djyh46mpr5j6gfdic31psqf99cw7f2kslxkdyb") (f (quote (("vendor" "uvc-src") ("system" "uvc-sys") ("default" "system"))))))

(define-public crate-uvc-0.1.9 (c (n "uvc") (v "0.1.9") (d (list (d (n "glium") (r "^0.27.0") (d #t) (k 2)) (d (n "uvc-src") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "uvc-sys") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "1mv0b3cq21mf4dbi8k6535f5rf8s5ry2vd3sa1x2qlrz4fjx3bm8") (f (quote (("vendor" "uvc-src") ("system" "uvc-sys") ("default" "system"))))))

(define-public crate-uvc-0.2.0 (c (n "uvc") (v "0.2.0") (d (list (d (n "glium") (r "^0.29.0") (d #t) (k 2)) (d (n "uvc-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1ygivw6hxmkjg64dcxy92f385b7lqh0h0z1g6jl6kd5i63qjchbb") (f (quote (("vendor" "uvc-sys/vendor"))))))

