(define-module (crates-io #{3}# u uio) #:use-module (crates-io))

(define-public crate-uio-0.1.0 (c (n "uio") (v "0.1.0") (d (list (d (n "libc") (r "0.1.*") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)))) (h "07dky10qxxgnaxf7a7vrbyyp0w18zw0lcv7f43agcxk804304xrn")))

(define-public crate-uio-0.1.1 (c (n "uio") (v "0.1.1") (d (list (d (n "libc") (r "0.1.*") (d #t) (k 0)) (d (n "mmap") (r "0.1.*") (d #t) (k 0)))) (h "064gj0g81dcqmppmyfw4mawaxv17c99q93f56hd7hlsnl4yx5q5m")))

(define-public crate-uio-0.2.0 (c (n "uio") (v "0.2.0") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "1lcgjvjis6xa8yb52849hp6vhcf4c9pqj904jc6h571q98nbjwsh")))

(define-public crate-uio-0.2.1 (c (n "uio") (v "0.2.1") (d (list (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)))) (h "139hxr3g9av17460wr9g18hr38c6n3vxjw731ifis672qgzrnp8y")))

