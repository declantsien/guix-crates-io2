(define-module (crates-io #{3}# u utp) #:use-module (crates-io))

(define-public crate-utp-0.1.0 (c (n "utp") (v "0.1.0") (d (list (d (n "time") (r "~0.0.3") (d #t) (k 0)))) (h "179b92k78vj3f3kd6xcgl7i0hq3blj079ayjzcrlcdlhzwr0jld3")))

(define-public crate-utp-0.1.1 (c (n "utp") (v "0.1.1") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "05ylp9qkp1ghv09vyw511yibvk7d0mlz9l5fw1jf6w89vmnvl6sr")))

(define-public crate-utp-0.1.3 (c (n "utp") (v "0.1.3") (d (list (d (n "log") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0ai071a8axf50xjyrs3w20bspz1a4j1xq92hz9rcjfha1xzgqang")))

(define-public crate-utp-0.1.4 (c (n "utp") (v "0.1.4") (d (list (d (n "log") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "024ivfpawr3npyhs69v29x3wvdx1h6rk69bv91yh13qk91ppqzbg")))

(define-public crate-utp-0.1.5 (c (n "utp") (v "0.1.5") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1spvi6q92j73q7cjpkgyq7x31yb9yx7pyjjacvzb878d81xhb4vv")))

(define-public crate-utp-0.2.0 (c (n "utp") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1dbnzzqwr3czlyf8ml2p6pizgqd1gvcwb7y6cldlvc32wd0cbjhw")))

(define-public crate-utp-0.2.1 (c (n "utp") (v "0.2.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "00blhawld7gvyj00skhfn9qb8ff5wqxd4b4bdwwcrcxz1my4ihz5")))

(define-public crate-utp-0.2.2 (c (n "utp") (v "0.2.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0kqqgxg78db9l25pyv5ydn9232qpkvhsnwv1pbb98w0df1jqp115")))

(define-public crate-utp-0.2.3 (c (n "utp") (v "0.2.3") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0lx2vf3b0w45j2x2f2hb2drg3db9al417slw2cq18i3kyygp7snw")))

(define-public crate-utp-0.2.4 (c (n "utp") (v "0.2.4") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1pnss6djbhgzshahwk4aryvs08qh8jlhnxgj8bm8wfhdk8ygafqn")))

(define-public crate-utp-0.2.5 (c (n "utp") (v "0.2.5") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "13hw2fd39hld9wgpb3pzxlfl3z6494p8d0whw43d1vp0ijh4nr7q")))

(define-public crate-utp-0.2.6 (c (n "utp") (v "0.2.6") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "064k9azksx255xz6fnbpcqfcqzpnwz5bm5s0p2qya8cj26b6y4yf")))

(define-public crate-utp-0.2.7 (c (n "utp") (v "0.2.7") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "11bsn6iadaaw5hq41p4nyk9viiqsrlr6qink7qpvq6q789bm6cgr")))

(define-public crate-utp-0.2.8 (c (n "utp") (v "0.2.8") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "07yrnsd194gdyhn0hfg0jhclz6s98clrr0rgskgrmhpsjalinvdj")))

(define-public crate-utp-0.3.0 (c (n "utp") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "06zwmvn7j2xqhibp7jvpjhql2islvrw20l2v3r4rh29babqb5n50") (y #t)))

(define-public crate-utp-0.3.1 (c (n "utp") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0c81pji51s71m8dn54rgdwrm60viyfc0z15l35r4ra2x8x2mb4fw")))

(define-public crate-utp-0.4.0 (c (n "utp") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "11ya3gziw8ad5fi3rh369nvr73wyavpb8gf43jwcmpiz2s324g1w")))

(define-public crate-utp-0.5.0 (c (n "utp") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1bib4ixnvlkvvjiiqfhsjh4kdr5d5zjrjsfb0w3a0pjs29mpbc5g")))

(define-public crate-utp-0.5.1 (c (n "utp") (v "0.5.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0blgwcp5w31cq007nh8wli4r5z6z4fsl3q5rp2hy1yw67nnk1j0m")))

(define-public crate-utp-0.6.0 (c (n "utp") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "028dyyihi4sdv2y9p9fhdz976kc04c25pip838x5fsgswbhkq8cn")))

(define-public crate-utp-0.6.1 (c (n "utp") (v "0.6.1") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "libc") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "i686-apple-darwin") (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "i686-unknown-linux-gnu") (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "x86_64-unknown-linux-gnu") (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "195385y7p0ji3wvssh249c5q2fl16hq8qz2v34n6jhb49x6wwhk9")))

(define-public crate-utp-0.6.2 (c (n "utp") (v "0.6.2") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "libc") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "i686-apple-darwin") (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "i686-unknown-linux-gnu") (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "x86_64-unknown-linux-gnu") (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1hpjc6pm1m385ww7sya0x5994ifiy8nxs5382h8ag35v11hjd12w")))

(define-public crate-utp-0.6.3 (c (n "utp") (v "0.6.3") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "libc") (r "*") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "i686-apple-darwin") (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "i686-unknown-linux-gnu") (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "x86_64-apple-darwin") (k 0)) (d (n "nix") (r "^0.3.8") (d #t) (t "x86_64-unknown-linux-gnu") (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)) (d (n "quickcheck") (r "*") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 0)))) (h "0w0sl9y6w3z3ggdc28v55qm5s1jj66idr1vg7dqyqg51lj5lqlyh")))

(define-public crate-utp-0.7.0 (c (n "utp") (v "0.7.0") (d (list (d (n "clippy") (r "^0.0.131") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "19c0pp5a94zsq2gc4qxvzkfijdq2mxcl50x52kffkxqypg730mc3") (f (quote (("unstable"))))))

