(define-module (crates-io #{3}# u uai) #:use-module (crates-io))

(define-public crate-uai-0.1.0 (c (n "uai") (v "0.1.0") (d (list (d (n "const_env") (r "^0.1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.95") (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "full"))) (d #t) (k 0)))) (h "1fh3z5z69pvv4fsar3h00isnl92cnh9pgdpycyaiz99zcbbd7vgw")))

