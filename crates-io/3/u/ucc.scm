(define-module (crates-io #{3}# u ucc) #:use-module (crates-io))

(define-public crate-ucc-0.1.0 (c (n "ucc") (v "0.1.0") (h "0346w568b8vmnnl14qkizqmq6x4j8m9bcf3bnbkbzl5rvynqas0y")))

(define-public crate-ucc-0.2.0 (c (n "ucc") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.20.0") (d #t) (k 0)))) (h "0mhpxdrjv3h2dxdpfci1yk7jw6bp6q79xd5fxlqryf8fmcsac11y") (f (quote (("ulib") ("default" "ulib"))))))

(define-public crate-ucc-0.2.1 (c (n "ucc") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.3") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.14") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.10") (d #t) (k 0)) (d (n "tree-sitter-cpp") (r "^0.20.0") (d #t) (k 0)))) (h "1wr8bbbf077xmw80lcykflzcdw9fng8awinsq6skzw3l95kbnv9f") (f (quote (("ulib") ("default" "ulib"))))))

