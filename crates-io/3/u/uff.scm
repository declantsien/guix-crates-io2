(define-module (crates-io #{3}# u uff) #:use-module (crates-io))

(define-public crate-uff-0.1.0 (c (n "uff") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.25") (d #t) (k 0)) (d (n "inkwell") (r "^0.1.0-beta.4") (d #t) (k 0)) (d (n "once_cell") (r "~1.16") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "~3.3") (d #t) (k 0)) (d (n "tokio") (r "~1.22") (d #t) (k 0)) (d (n "which") (r "~4.3") (d #t) (k 0)))) (h "1s0k678cdjd2gw0c9g6qliykqb36phrmxk545fk0j2spp3w0q6b9") (f (quote (("default" "clap/derive" "inkwell/llvm11-0" "tokio/rt-multi-thread" "tokio/macros"))))))

