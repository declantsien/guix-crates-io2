(define-module (crates-io #{3}# u udt) #:use-module (crates-io))

(define-public crate-udt-0.1.0 (c (n "udt") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libudt4-sys") (r "^0.1") (d #t) (k 0)))) (h "15dzb96hzg02ggmqzqsprkwxw5lv41scjgkivc288f8y5zscy2xg")))

(define-public crate-udt-0.1.1 (c (n "udt") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libudt4-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "00w3223ww2lp0i418gj1830ang346kyjid3mgh8pbb3mbbcar23p")))

(define-public crate-udt-0.1.2 (c (n "udt") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libudt4-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1by46wlljlcrh69j899phj8m43a366qx8ash53dsln8zdnnpgjih")))

(define-public crate-udt-0.1.4 (c (n "udt") (v "0.1.4") (d (list (d (n "bitflags") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libudt4-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1yi8fsak71fy0p51j8a4v997990axhkilnd0zlnfd68ldzv0vmam")))

(define-public crate-udt-0.2.0 (c (n "udt") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libudt4-sys") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1pb1arp2cd21555i5v1lwy126c665198cx63rbsfwb4x1p184n56")))

