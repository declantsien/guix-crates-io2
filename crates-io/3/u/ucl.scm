(define-module (crates-io #{3}# u ucl) #:use-module (crates-io))

(define-public crate-ucl-0.1.0 (c (n "ucl") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libucl-sys") (r "*") (d #t) (k 0)))) (h "0lnirk81yqnc6sgbyi25l6j3ll8fhlyhkxnn3azk6f7xzha6i3kn")))

(define-public crate-ucl-0.1.1 (c (n "ucl") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libucl-sys") (r "*") (d #t) (k 0)))) (h "17np8zw028mypncxd90862f5w4sahnmnyisfxzd0d8r3a17g71j3")))

(define-public crate-ucl-0.1.2 (c (n "ucl") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libucl-sys") (r "*") (d #t) (k 0)))) (h "1ipxcx2aq49n0cccj1c7vvfycp2ihg1dwj7szj9bhn73b6w5s4pb") (f (quote (("unstable") ("default"))))))

(define-public crate-ucl-0.1.3 (c (n "ucl") (v "0.1.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libucl-sys") (r "*") (d #t) (k 0)))) (h "066plkmp7i30xdrbg2rchsp8xv7019gv5a99y075a1daq04cpr78") (f (quote (("unstable") ("default"))))))

(define-public crate-ucl-0.1.4 (c (n "ucl") (v "0.1.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libucl-sys") (r "*") (d #t) (k 0)))) (h "1v13n74gxidh2li782dmfabpjsdbmgyi28za1fhkh22dnkzv1bak") (f (quote (("unstable") ("default"))))))

