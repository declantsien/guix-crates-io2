(define-module (crates-io #{3}# u upt) #:use-module (crates-io))

(define-public crate-upt-0.1.0 (c (n "upt") (v "0.1.0") (h "1zm97mi3l5gfqbn578k00yaacci07xb6xk0f1r1vvfg94lwx0i0x")))

(define-public crate-upt-0.1.2 (c (n "upt") (v "0.1.2") (h "1rn10sbcvdbg6cif45w4mndbnllvmx33rq8k4smf0anazby1pyqc")))

(define-public crate-upt-0.1.3 (c (n "upt") (v "0.1.3") (h "052a9wyzjbrikvzzafidggaabxihp3hiay218cyllwac55hbj1l2")))

(define-public crate-upt-0.2.0 (c (n "upt") (v "0.2.0") (h "16474xr4x718vspqq3c4npj8ar8919qbws5azimbk0kchqypnc63")))

(define-public crate-upt-0.3.0 (c (n "upt") (v "0.3.0") (h "0xaj5q56w1qj22zr1cw3pr1w3cnjq0c8h4hj38pbxv9fxw5iar39")))

(define-public crate-upt-0.4.0 (c (n "upt") (v "0.4.0") (d (list (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "1bvix9dwccbf6apqmb3f1z745pi732faz9ds4n6mvc2li80igdvl")))

(define-public crate-upt-0.5.0 (c (n "upt") (v "0.5.0") (d (list (d (n "which") (r "^5.0.0") (d #t) (k 0)))) (h "1xgqm3jcjkr4da03jhbvhn3cc5qna9czhck197jf4lx98sh0b4yz")))

(define-public crate-upt-0.6.0 (c (n "upt") (v "0.6.0") (d (list (d (n "which") (r "^6.0.0") (d #t) (k 0)))) (h "0mb4qjz252yahglb5km4afb6niyp63qjwr4a3fpiixsn2jwym4jn")))

(define-public crate-upt-0.7.0 (c (n "upt") (v "0.7.0") (d (list (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "13nfv9s24zagadr6rp1i7chwcqgi74zfwdma8y3kv4i3fnc42jq7")))

(define-public crate-upt-0.8.0 (c (n "upt") (v "0.8.0") (d (list (d (n "which") (r "^6.0.1") (d #t) (k 0)))) (h "17jlv8gimx3wkqpq425maa1j4fv8lks45260fg0r2xcsrmqhw0iy")))

