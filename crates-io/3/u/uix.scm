(define-module (crates-io #{3}# u uix) #:use-module (crates-io))

(define-public crate-uix-0.0.1 (c (n "uix") (v "0.0.1") (h "08z77cfh36k68pc9cz1ssysdhgmyi0ql7kr4p4j47ry493471vp5")))

(define-public crate-uix-0.0.2 (c (n "uix") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1c3517crv3rjpbylh1hxd7cik8hgqyb7acb1q23jdi9ifqzwbgj5")))

(define-public crate-uix-0.0.4 (c (n "uix") (v "0.0.4") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cf") (r "^0.0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0b6mi0xhnp998ws176d3c9xq95fgm821jdk96xbwq92cskzgya8c")))

(define-public crate-uix-0.0.5 (c (n "uix") (v "0.0.5") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cf") (r "^0.0.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.14") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("multipart" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mzkv3xmkxj4gmgw94ajq7hng9r8lkl92qifa4pb6jibj9zxzmxf")))

(define-public crate-uix-0.0.7 (c (n "uix") (v "0.0.7") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "cf") (r "^0.0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.14") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "redis") (r "^0.21.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("multipart" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "156rmzp7l8lwgggvksahd4lynn12ql7hckqxyar1fcc9925zya8h")))

