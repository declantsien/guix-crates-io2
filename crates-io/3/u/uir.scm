(define-module (crates-io #{3}# u uir) #:use-module (crates-io))

(define-public crate-uir-0.0.0 (c (n "uir") (v "0.0.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "generational-arena") (r "^0.2") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1dgj58z56v17q8l618l1pwwzwhh8n6yi7gjm8gqwbwx3kv00s7jw") (f (quote (("default"))))))

