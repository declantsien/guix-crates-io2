(define-module (crates-io #{3}# u ucx) #:use-module (crates-io))

(define-public crate-ucx-0.0.1 (c (n "ucx") (v "0.0.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "const-cstr-fork") (r "^0.2") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.14") (d #t) (k 0)) (d (n "ordermap") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.1") (d #t) (k 0)) (d (n "rust-extra") (r "^0.0.13") (d #t) (k 0)) (d (n "ucx-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0sa1ay2dzmcl93zpbvw3qvgqqw54jc3aidzwgrnf03h5i3inda6n")))

