(define-module (crates-io #{3}# u utf) #:use-module (crates-io))

(define-public crate-utf-0.1.0 (c (n "utf") (v "0.1.0") (h "1hqrcfj4rnii1qmwfdadw44bsz387ak073ynxjxb2bgzszqz2fid")))

(define-public crate-utf-0.1.1 (c (n "utf") (v "0.1.1") (h "1fkg1kz24k423hn4ylhvhd84fikmyb1laydivxhs899zg7ixhjl4")))

(define-public crate-utf-0.1.2 (c (n "utf") (v "0.1.2") (h "02rkxvzk2507iws1rfjghgm9zd8p1h7x8am9dqvm88m78731zdbc")))

(define-public crate-utf-0.1.3 (c (n "utf") (v "0.1.3") (h "1scfja2975czq988wpgcmzaxgyy89ygdzmsv369kf74mhndf8sam")))

(define-public crate-utf-0.1.4 (c (n "utf") (v "0.1.4") (h "0384g2wrwbp24vxwakcxbi2qnn5pvvmayz22gci8gw5f7x4py7pp")))

(define-public crate-utf-0.1.5 (c (n "utf") (v "0.1.5") (h "01k0xj2h17wxb2hcx5sp0ragx1vk5bigkcz19m2rqkm79dnkkjp5")))

(define-public crate-utf-0.1.6 (c (n "utf") (v "0.1.6") (h "0xhsc6ifq3a66r3w9yl3pnjsrrck51fgwnpz7dirfzp56hbzj147")))

