(define-module (crates-io #{3}# u ux2) #:use-module (crates-io))

(define-public crate-ux2-0.1.0 (c (n "ux2") (v "0.1.0") (d (list (d (n "ux2-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1ca3943c7ykn5mkcdskj3bmlk8hmdh45s1pkra4wdhsgfwk83mcl")))

(define-public crate-ux2-0.2.0 (c (n "ux2") (v "0.2.0") (d (list (d (n "ux2-macros") (r "^0.2.0") (d #t) (k 0)))) (h "08v4w3hw17qmf9gcfl5nc8g75w5lpcqv3rafi4wxw7g0ybkzkis0")))

(define-public crate-ux2-0.3.0 (c (n "ux2") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.3.0") (d #t) (k 0)))) (h "0mfbqnclnlj0jz544cn5z6gk1nzqsh2cxd59nbyxabdamzvpnmh8")))

(define-public crate-ux2-0.4.0 (c (n "ux2") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.4.0") (d #t) (k 0)))) (h "160l738qm31ixr41h4mzwcnscf9ghs0mmnyd52rzaff8kn7334rh")))

(define-public crate-ux2-0.5.0 (c (n "ux2") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.5.0") (d #t) (k 0)))) (h "1bvmid1r2g93lrc7hkp3by9425vsi95jhdyikby529im69z1f6fn")))

(define-public crate-ux2-0.6.0 (c (n "ux2") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.6.0") (d #t) (k 0)))) (h "0ai5j3amcpx317s7s0p5ss54fda47r1rbyx9hh3kb61v0xp4vhvg")))

(define-public crate-ux2-0.7.0 (c (n "ux2") (v "0.7.0") (d (list (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.7.0") (d #t) (k 0)))) (h "1ryc863dxc1grg5hnb3gbjrdslclq0gks3jg67ldj9fqr5rpazdw")))

(define-public crate-ux2-0.8.0 (c (n "ux2") (v "0.8.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.8.0") (d #t) (k 0)))) (h "1j53k0ccwvbccsydzlpc46sj7lv26pmzvjc0bck6cy07yf3a1v92") (f (quote (("default" "32") ("8") ("64" "32") ("32" "16") ("16" "8") ("128" "64"))))))

(define-public crate-ux2-0.8.1 (c (n "ux2") (v "0.8.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.8.0") (d #t) (k 0)))) (h "1djj68pan4kzs0szkwvjwc3ikp8hz82kslkmlrw84cvpnqkxd544") (f (quote (("default" "16") ("8") ("64" "32") ("32" "16") ("16" "8") ("128" "64"))))))

(define-public crate-ux2-0.8.2 (c (n "ux2") (v "0.8.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.8.0") (d #t) (k 0)))) (h "03dqf6b42iqdbd45z475l5x77vhqfm7skifshgdi450183i2chdh") (f (quote (("default" "8") ("8") ("64" "32") ("32" "16") ("16" "8") ("128" "64"))))))

(define-public crate-ux2-0.8.3 (c (n "ux2") (v "0.8.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.9.0") (d #t) (k 0)))) (h "0xvs1iw6c3cz3xr5v8l1lin7pcv6jldqqfg4jvdm3xlk270v7nqm") (f (quote (("default" "16") ("8") ("64" "32") ("32" "16") ("16" "8") ("128" "64"))))))

(define-public crate-ux2-0.8.4 (c (n "ux2") (v "0.8.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.9.0") (d #t) (k 0)))) (h "1298637ll4czdbvjd8gj3d8rcqyh5y0hdr1x8hvwpakzndpx8lia") (f (quote (("default" "8") ("8") ("64" "32") ("32" "16") ("16" "8") ("128" "64"))))))

(define-public crate-ux2-0.8.5 (c (n "ux2") (v "0.8.5") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "ux2-macros") (r "^0.9.0") (d #t) (k 0)))) (h "029130psbrw54bmxh7c0xvkr2xq311ycj84vcp18lafcs2sp03kb") (f (quote (("default" "8") ("8") ("64" "32") ("32" "16") ("16" "8") ("128" "64"))))))

