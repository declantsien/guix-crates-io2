(define-module (crates-io #{3}# u usb) #:use-module (crates-io))

(define-public crate-usb-0.1.0 (c (n "usb") (v "0.1.0") (h "16hbbnv8470n58k70zf8majhz27z2j64v92k8hrp68ppabbvk4v5")))

(define-public crate-usb-0.1.1 (c (n "usb") (v "0.1.1") (h "0ll9k3vm4ckdxb16ki399jax8y92zgiirrj672d065nhzmbxvxfi")))

(define-public crate-usb-0.1.2 (c (n "usb") (v "0.1.2") (h "0nsajc77s9zqki79g274ziwrd6g3xbnz9zzzjkkdj3m6q11zdvp4")))

(define-public crate-usb-0.1.3 (c (n "usb") (v "0.1.3") (h "1b98prf3cqwrdy5n968x53ycddklp3m22qjkwhbwylfpp743mqwl")))

(define-public crate-usb-0.1.4 (c (n "usb") (v "0.1.4") (h "1z2bp36af19chb3faj4lh975qac09117v2bj20g12r697c56c842")))

(define-public crate-usb-0.1.5 (c (n "usb") (v "0.1.5") (h "0b29sby2d0k5xh3jdw2sg8acd0ym5ljrq72pl9rfb8wdxmdwfcf8")))

(define-public crate-usb-0.2.0 (c (n "usb") (v "0.2.0") (h "1jvcazvqbarsfv2czamakkcyqwyg85ggs81sqir2wp6hzvzxvlh0")))

(define-public crate-usb-0.2.1 (c (n "usb") (v "0.2.1") (h "0391vp8hv177hpk4drbz3hcn8w6s1acp3yk1kn5sxpym5kkf1cai")))

(define-public crate-usb-0.2.2 (c (n "usb") (v "0.2.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "060l6vldvzijap8lkdg0fi4vqwg5bgav09n636wjxsd7rzphv0hv")))

