(define-module (crates-io #{3}# u usd) #:use-module (crates-io))

(define-public crate-usd-0.1.0 (c (n "usd") (v "0.1.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "usd-cpp") (r "^0.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.0") (d #t) (k 1)))) (h "04yzxxfa4zj4wglnkqgh8b60wgfxd44b3cxhkgm376gy8n62gja8") (y #t)))

(define-public crate-usd-0.0.1 (c (n "usd") (v "0.0.1") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.0") (d #t) (k 1)))) (h "02x2w827lllmanl8n97vsaadxphvgx0g6ghpsqjnycc15c73cyd0")))

(define-public crate-usd-0.0.2 (c (n "usd") (v "0.0.2") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.0") (d #t) (k 1)))) (h "1fhjaln5g302qin79z667maxvvac1p5ykcl4sikmrz40dc3wv28d")))

(define-public crate-usd-0.0.3 (c (n "usd") (v "0.0.3") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.0") (d #t) (k 1)))) (h "180dsfxsx43gilpdp0pc8q3l7x91d6zca3m1477brw88xpjbvfma")))

(define-public crate-usd-0.0.4 (c (n "usd") (v "0.0.4") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 1)))) (h "1gddxmawkw2ba8nyk1k9jpm37b3s37hpyfi8w8rhbp48794aphc3")))

(define-public crate-usd-0.0.5 (c (n "usd") (v "0.0.5") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 1)))) (h "06mcvi26slk394gcihh935flign5paii07a6vgqf6mpdfassf0if")))

(define-public crate-usd-0.0.6 (c (n "usd") (v "0.0.6") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 1)))) (h "0zg4q21kdykzpjilp3jgdqksfd6899a2lclg3xrshwfv0yrikasa")))

(define-public crate-usd-0.0.7 (c (n "usd") (v "0.0.7") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 1)))) (h "17vq9hgb9pc0xnx4p3rdddx28mjxd6xs5v1zrb3fv06q20iq19pz")))

(define-public crate-usd-0.0.8 (c (n "usd") (v "0.0.8") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 1)))) (h "1ynyz34zyvgik2zi3j2ny7wdw3fa3mljb9i3as6x9fk60cfd86na")))

(define-public crate-usd-0.0.9 (c (n "usd") (v "0.0.9") (d (list (d (n "c_str_macro") (r "^1.0.2") (d #t) (k 0)) (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "half") (r "^1.6.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 0)) (d (n "usd-cpp") (r "^0.1.1") (d #t) (k 1)))) (h "1si181ckcjp3v8zabfkhlmrymjwdlzvd4ngxj90l59nf1inbql7r")))

