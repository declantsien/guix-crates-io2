(define-module (crates-io #{3}# u uip) #:use-module (crates-io))

(define-public crate-uip-0.1.0 (c (n "uip") (v "0.1.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "stun") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (d #t) (k 0)))) (h "0j81n8rwsclrk4ji31gd4jy5da29sfps217wxryz0bv529px1hcp")))

(define-public crate-uip-0.1.1 (c (n "uip") (v "0.1.1") (d (list (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "stun") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (d #t) (k 0)))) (h "0zrjvhb698l0rn3c2gjkq0c50s7c92kgh1mld8lxwfci7gi3hz69")))

(define-public crate-uip-0.1.2 (c (n "uip") (v "0.1.2") (d (list (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "stun") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (d #t) (k 0)))) (h "08k7ach7ffw5035300xg9cspqic05374lij0vhr92dhvwipgvabc")))

(define-public crate-uip-0.1.3 (c (n "uip") (v "0.1.3") (d (list (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "stun") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (d #t) (k 0)))) (h "1ql6c8h2scdygszsln2qwd444c61rwfvjgprd5b0nlvq9p1b8gsr")))

(define-public crate-uip-0.1.4 (c (n "uip") (v "0.1.4") (d (list (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "stun") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (d #t) (k 0)))) (h "1hibzidx1kqpj1zmvyz2k3170hpzgd85g7yrg1yyds7kicr5cw9x")))

(define-public crate-uip-0.1.5 (c (n "uip") (v "0.1.5") (d (list (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "stun") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (d #t) (k 0)))) (h "15xwcnyq3qsckncvgan23m3ip0f77bqlrwy6m0di6jpc8xf2vqzb")))

(define-public crate-uip-0.1.6 (c (n "uip") (v "0.1.6") (d (list (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "stun") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.27") (d #t) (k 0)))) (h "0xnc7lkcfg91bqyqj9aa4xxy9h41q9hhmyz6fi0fwmfdddyz9gpf")))

