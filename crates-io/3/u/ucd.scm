(define-module (crates-io #{3}# u ucd) #:use-module (crates-io))

(define-public crate-ucd-0.1.0 (c (n "ucd") (v "0.1.0") (h "1mlqkh6hxvjr45vgjyfcajlyc8m3y1dl9ng9fnfk9sbwdyqaa8l0")))

(define-public crate-ucd-0.1.1 (c (n "ucd") (v "0.1.1") (h "0n8i7na4np0w9m7gp7bn1cfnb4qvmmcwx4a9xgqnc8vni3jsckzy")))

