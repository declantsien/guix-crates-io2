(define-module (crates-io #{3}# i id3) #:use-module (crates-io))

(define-public crate-id3-0.1.0 (c (n "id3") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "encoding") (r "*") (d #t) (k 0)) (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0rrfc8mckfkmrm40yy1v951b24dqalgd5ydaxkfbv52kh0clgpxd")))

(define-public crate-id3-0.1.1 (c (n "id3") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "encoding") (r "*") (d #t) (k 0)) (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1izgsjp50vzgxrifq1n8144did81gnha709z9n21dgz2895gjxvq")))

(define-public crate-id3-0.1.2 (c (n "id3") (v "0.1.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "encoding") (r "*") (d #t) (k 0)) (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "01jqkw80266807ss2vjbbb4y64l3dckpy3bgghrj94m7q9ni028y")))

(define-public crate-id3-0.1.3 (c (n "id3") (v "0.1.3") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "encoding") (r "*") (d #t) (k 0)) (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0j70dlh9zkk9xm4h21jij3ahr3nr06b3jy8hf6jxpslsp3j8w4ar")))

(define-public crate-id3-0.1.4 (c (n "id3") (v "0.1.4") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "encoding") (r "*") (d #t) (k 0)) (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1lvjzk10614y4g6mpbpi5ym9s1qj3cb5rxq5rqka1ibf1b734lai")))

(define-public crate-id3-0.1.5 (c (n "id3") (v "0.1.5") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "encoding") (r "*") (d #t) (k 0)) (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "16j323by10g5mcc2hcvp6gvbl4p3pmh1jy61bvz147mag62qzvg4")))

(define-public crate-id3-0.1.6 (c (n "id3") (v "0.1.6") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "encoding") (r "*") (d #t) (k 0)) (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0iggfgfc22njkrqyxjf82glrkzxi6nxli6cafw8y93b0z6xf85vb")))

(define-public crate-id3-0.1.7 (c (n "id3") (v "0.1.7") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "encoding") (r "*") (d #t) (k 0)) (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "1bgri604cyb5kvi5i2y7lyb1pxlpf0w10qfrpd7n96c98h3vd1c1")))

(define-public crate-id3-0.1.8 (c (n "id3") (v "0.1.8") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "encoding") (r "*") (d #t) (k 0)) (d (n "flate2") (r "*") (d #t) (k 0)) (d (n "lazy_static") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)))) (h "0xr2r8ywq1xzwdcsmmxn3ys2vqvhv33rpjwpka75z7hh92n91k10")))

(define-public crate-id3-0.1.9 (c (n "id3") (v "0.1.9") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "flate2") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "09fif5gvd61zywwy02rzbgf6456d4wah4fxbvp4b14cmvvgjqssy")))

(define-public crate-id3-0.1.10 (c (n "id3") (v "0.1.10") (d (list (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "flate2") (r "^0.2.11") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "rand") (r "^0.3.12") (d #t) (k 0)))) (h "14w1gjbggbqw8z7z1cza2g4k2rj515w1zi4h5k8ggsv4xyi70w2p")))

(define-public crate-id3-0.1.11 (c (n "id3") (v "0.1.11") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "07sryysynamr6hy6ild672hdv467ygcsfskpgr9a40ik8yqdsc2w")))

(define-public crate-id3-0.1.12 (c (n "id3") (v "0.1.12") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "flate2") (r "^0.2.14") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "11v8jin1l9v41hfd7p4ja77339qvfz55pdy8hbk8h2zl6lmj061w")))

(define-public crate-id3-0.2.0 (c (n "id3") (v "0.2.0") (d (list (d (n "bitflags") (r "~0.9.0") (d #t) (k 0)) (d (n "byteorder") (r "~1.0.0") (d #t) (k 0)) (d (n "derive_builder") (r "~0.4.7") (f (quote ("private_fields"))) (d #t) (k 0)) (d (n "encoding") (r "~0.2.33") (d #t) (k 0)) (d (n "flate2") (r "~0.2.19") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.8") (d #t) (k 0)) (d (n "regex") (r "~0.2.1") (d #t) (k 0)) (d (n "tempdir") (r "~0.3.4") (d #t) (k 2)))) (h "1ir9p6769wj6lskhnj5hv2dk3b93iy7dd35p7017njss54fkw3wy") (f (quote (("unstable"))))))

(define-public crate-id3-0.2.2 (c (n "id3") (v "0.2.2") (d (list (d (n "bitflags") (r "~1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "~1.1.0") (d #t) (k 0)) (d (n "derive_builder") (r "~0.5.0") (d #t) (k 0)) (d (n "encoding") (r "~0.2.33") (d #t) (k 0)) (d (n "flate2") (r "~0.2.20") (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.9") (d #t) (k 0)) (d (n "regex") (r "~0.2.2") (d #t) (k 0)) (d (n "tempdir") (r "~0.3.5") (d #t) (k 2)))) (h "0nxv9sl9b7szjcz0hqszqbwd8sg5sqc0yj4pb9sj7aa7zic9jnna") (f (quote (("unstable"))))))

(define-public crate-id3-0.2.3 (c (n "id3") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.0.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "clippy") (r "0.0.*") (o #t) (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.0") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^0.2.20") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.9") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1jv35gbii8m7gsnclzqz7scb7nhicn0imryjqqmdbm3gbciir5zy") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.2.4 (c (n "id3") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0g7yd0hpxp4gvypa73qw8zjy72sjg4p2j4li8bd08762fn7c57fh") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.2.5 (c (n "id3") (v "0.2.5") (d (list (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "derive_builder") (r "^0.5.1") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1zxirv2wk7g1rq28zrvjzcp0iqrza6jqq3zhm9cqcjxqn1174rd1") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.3.0 (c (n "id3") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.7") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "17h6pjzfsv20q8y1fakvv5igvl21cq104gqdy976fc19sk9f5jwn") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.4.0 (c (n "id3") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.7") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0yz6zlkl0sy7h1z9w2dn2ddnyb6kaq4ks6pdmf75zwdba1y9a57v") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.5.0 (c (n "id3") (v "0.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0966fhv1zca4x3zg447gr0m66nzrpnbpiiq6fpmmqjwp6hckk4nx") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.5.1 (c (n "id3") (v "0.5.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "encoding") (r "^0.2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1gz9d9214835za8fmjswirdg3siz9ibbdr7vmqb8amp11jsiph82") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.5.2 (c (n "id3") (v "0.5.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1agl2rinxpbz2whi8ni7q5hmmn81gqb7r18yqibzlvkdy7jlrdpv") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.5.3 (c (n "id3") (v "0.5.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0cb33a7pxydm1dq70zja9y07iaa8b8z1zpqj6igprmb0mbjpmcwc") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.6.0 (c (n "id3") (v "0.6.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "09h8clfkvhfkqmmkizmwmiv72szrxi5gali4qfry0siz248yannw") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.6.1 (c (n "id3") (v "0.6.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1ma85z4h2869q63syi47jib3faw6hrdyz80rpxdkg2wd05gdrhsb") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.6.2 (c (n "id3") (v "0.6.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "153x9hcd30f1sdwnb6yvg1pqiag4n381zj8x920qdpshmj654mvp") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.6.3 (c (n "id3") (v "0.6.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0lfzj0nikvs927zb8yrydkcannvbyyv65swkg5a3wgigrdbajgzj") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.6.4 (c (n "id3") (v "0.6.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0i6jwc8abihxfnkpc8x6g1sxikm00yawl61wnyzjanx1m8kbisdv") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.6.5 (c (n "id3") (v "0.6.5") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "041pamfc8kkdjiqydrvd1c7ym44rzhwzh9cpy1ljcm436cxwbrs9") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-0.6.6 (c (n "id3") (v "0.6.6") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0pds5v5lcbq4rq5qgaaqjx9h7n3dgs6hknlfdh3cddnayk2lnyg8") (f (quote (("unstable") ("default"))))))

(define-public crate-id3-1.0.0 (c (n "id3") (v "1.0.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1bzd22bzjhj4wc8zy7njnvjk27xkzk6c1vwii4bhdp2g811b1fz4") (y #t)))

(define-public crate-id3-1.0.1 (c (n "id3") (v "1.0.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1cyy0ll4rkkzd1kr3f44dnhflaq6iw2vi3mawqrn0pfvm04a6if1")))

(define-public crate-id3-1.0.2 (c (n "id3") (v "1.0.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1kfhsk7xicq5i1c61bcnp4jnd3gr0wpsbhcd6pzsgn06xpwrhdbs")))

(define-public crate-id3-1.0.3 (c (n "id3") (v "1.0.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1y8g43nma4alkx16lvvgad3xnd0fddjs4rxh9r82f483wzns7h0h")))

(define-public crate-id3-1.1.0 (c (n "id3") (v "1.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0q90g5idp8xypcibp54khcrb1kpcx69k4kq0khb1sxqjlaaz9hvb")))

(define-public crate-id3-1.1.1 (c (n "id3") (v "1.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1cq5pv7cmrfwdpz07n4gbql6xh6y6r934a791yy3s6pl0iw8hcfx")))

(define-public crate-id3-1.1.2 (c (n "id3") (v "1.1.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0bcwzkxnw78s4vh4lmz5rjizi2q4brqsc6478a1i4zf2a3mn33js")))

(define-public crate-id3-1.1.3 (c (n "id3") (v "1.1.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0v7gq7g142jbs9k2fp13lfxp3448rqxj2fvz9qs7wb914cny91j9")))

(define-public crate-id3-1.1.4 (c (n "id3") (v "1.1.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "15qc6lsc4lkna8kyy4nhsyf31cmrqfxsgrc7j24cmskid22k874j")))

(define-public crate-id3-1.2.0 (c (n "id3") (v "1.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0bp5rlw44na9ahy1iwz8rbasw7m41h0cdwiqhhwc0i98dsf81y2q")))

(define-public crate-id3-1.2.1 (c (n "id3") (v "1.2.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1sqq3al4sb0822mdlp0sksv2mqfr2qz267dgwdbl2himr19jzslr") (y #t)))

(define-public crate-id3-1.3.0 (c (n "id3") (v "1.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "183lxypqbpbah0gqc0nazga23mr0axivhqb31fjhxzyxknf5iky2")))

(define-public crate-id3-1.4.0 (c (n "id3") (v "1.4.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "1xh8488pg6ck614nf404qs55c53ljf01bcv59brmipz96ji440yb") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.5.0 (c (n "id3") (v "1.5.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "136cpdj2jpx7z9rs9x8ymydqanm56zv62f7b5h9wn7wjqpv05h1l") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.5.1 (c (n "id3") (v "1.5.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "0naq4p8xsb7y8zwp67vmjs0hffr2jbhdk90xfnzan4jl31vdjv74") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.6.0 (c (n "id3") (v "1.6.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "0j49bhpqsz29i0rcr2s16yay7d17nzwx2cj1nrg44c2b8wrsimqr") (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.7.0 (c (n "id3") (v "1.7.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "00b58gjiqi8wchfcjnx74jf3ymybphln72l7nbjb2wa6ijfdv2ck") (f (quote (("default" "decode_picture") ("decode_picture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.8.0 (c (n "id3") (v "1.8.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "07mbfg6h7k578qi1c6nv3hbigfhfbl0jpm0xzn8j7vqyyhhalp58") (f (quote (("default" "decode_picture") ("decode_picture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.9.0 (c (n "id3") (v "1.9.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "0dh9grrcp1s2wkgizva8mibsyp176sr961hh91wcg49z13pjynfr") (f (quote (("default" "decode_picture") ("decode_picture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.10.0 (c (n "id3") (v "1.10.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "1din8pni6pn2h2dsswbpbrdwc3q2nkfwhqj1ix708b50x2mm11dz") (f (quote (("default" "decode_picture") ("decode_picture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.11.0 (c (n "id3") (v "1.11.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "0d4fwi1hph4534vg6d6w5n3272aa9w9mrxjaqfvlc9zag0b1sg58") (f (quote (("default" "decode_picture") ("decode_picture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.12.0 (c (n "id3") (v "1.12.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "07k3qiv132q5lwqjm0g9jnsvihz7njyk35b2lmc8vw7n7hda381b") (f (quote (("default" "decode_picture") ("decode_picture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.13.0 (c (n "id3") (v "1.13.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "123sj0plvha26szwfy0i0iwh5282p0qd0bqksq8b0kx0nc56bci0") (f (quote (("default" "decode_picture") ("decode_picture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

(define-public crate-id3-1.13.1 (c (n "id3") (v "1.13.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "tokio") (r "^1.21") (f (quote ("rt" "macros" "io-util" "fs"))) (o #t) (k 0)))) (h "07zzw64lnm2jpkxjnscpgzx44dvmzf5aj4mialywc9fib9z1zx3r") (f (quote (("default" "decode_picture") ("decode_picture")))) (s 2) (e (quote (("tokio" "dep:tokio"))))))

