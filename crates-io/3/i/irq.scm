(define-module (crates-io #{3}# i irq) #:use-module (crates-io))

(define-public crate-irq-0.1.0 (c (n "irq") (v "0.1.0") (d (list (d (n "cortex-m-rt") (r "^0.6.11") (d #t) (k 2)) (d (n "once_cell") (r "^1.3.0") (d #t) (k 2)))) (h "0v2djkr91vkmgkw9v8p946nnga5s8yl7l59ry05pgpc7i6f57j1l") (y #t)))

(define-public crate-irq-0.2.0 (c (n "irq") (v "0.2.0") (d (list (d (n "cortex-m-rt") (r "^0.6.11") (d #t) (k 2)) (d (n "once_cell") (r "^1.3.0") (d #t) (k 2)))) (h "1d3a87w4x7d2gg570r2qfax0w60dnp48pa79v31ssanw0jlw77fv") (y #t)))

(define-public crate-irq-0.2.1 (c (n "irq") (v "0.2.1") (d (list (d (n "cortex-m-rt") (r "^0.6.11") (d #t) (k 2)) (d (n "once_cell") (r "^1.3.0") (d #t) (k 2)))) (h "0qbyxl9v1vkw0inhpaczps9wpz0jnba049mc36x7y3mj2w3bs8nq")))

(define-public crate-irq-0.2.2 (c (n "irq") (v "0.2.2") (d (list (d (n "cortex-m-rt") (r "^0.6.11") (d #t) (k 2)) (d (n "once_cell") (r "^1.3.0") (d #t) (k 2)))) (h "0r19pb3qk00aq4g1l45qcch1a55b4rg27q6s06r3rqmddgpgyzf2")))

(define-public crate-irq-0.2.3 (c (n "irq") (v "0.2.3") (d (list (d (n "cortex-m-rt") (r "^0.6.11") (d #t) (k 2)) (d (n "once_cell") (r "^1.3.0") (d #t) (k 2)))) (h "18wrm7zwmn7pjb1yckik152n2a0r5ggnxi9nn3g21b1dk11l1pjz")))

