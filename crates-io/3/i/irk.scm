(define-module (crates-io #{3}# i irk) #:use-module (crates-io))

(define-public crate-irk-0.1.0 (c (n "irk") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "136qzwznsfrjn3nxj7c2siqxcg0iigarp35g16jv4mjki2x726ma")))

(define-public crate-irk-0.1.1 (c (n "irk") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ps28b50dlsfbqm0jg8rp1qw08fq4d437k6vrmbqqzcya3aqp3bi")))

