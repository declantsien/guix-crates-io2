(define-module (crates-io #{3}# i ip3) #:use-module (crates-io))

(define-public crate-ip3-1.0.0 (c (n "ip3") (v "1.0.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "const-str") (r "^0.5.6") (d #t) (k 0)))) (h "1nsq7p9xi7nm3sgy6flnh6ffpbpif2frsbbh1yjbwasz4b2gzww3") (f (quote (("en") ("default" "en"))))))

