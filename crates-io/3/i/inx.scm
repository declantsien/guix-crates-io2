(define-module (crates-io #{3}# i inx) #:use-module (crates-io))

(define-public crate-inx-1.0.0-beta.1 (c (n "inx") (v "1.0.0-beta.1") (d (list (d (n "bee-block-stardust") (r "^1.0.0-beta.1") (f (quote ("std"))) (o #t) (k 0) (p "bee-block")) (d (n "futures") (r "^0.3") (k 2)) (d (n "packable") (r "^0.5") (o #t) (k 0)) (d (n "prost") (r "^0.10") (f (quote ("prost-derive" "std"))) (k 0)) (d (n "thiserror") (r "^1.0") (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("rt-multi-thread"))) (k 2)) (d (n "tonic") (r "^0.7") (f (quote ("codegen" "prost" "transport"))) (k 0)) (d (n "tonic-build") (r "^0.7") (f (quote ("transport" "prost"))) (k 1)))) (h "0zay72ihyshnmwl3j2zcvaji92zwm6f9cp67v6nx3qf584d3nypd") (f (quote (("types" "bee-block-stardust" "packable") ("default" "types"))))))

(define-public crate-inx-1.0.0-beta.2 (c (n "inx") (v "1.0.0-beta.2") (d (list (d (n "prost") (r "^0.10") (f (quote ("prost-derive" "std"))) (k 0)) (d (n "tonic") (r "^0.7") (f (quote ("codegen" "prost" "transport"))) (k 0)) (d (n "tonic-build") (r "^0.7") (f (quote ("transport" "prost"))) (k 1)))) (h "1gk2kc6gb3a2498irnqmf5ddg15jd1x582x2xiz5ymhazxab2msk")))

(define-public crate-inx-1.0.0-beta.3 (c (n "inx") (v "1.0.0-beta.3") (d (list (d (n "prost") (r "^0.10") (f (quote ("prost-derive" "std"))) (k 0)) (d (n "tonic") (r "^0.7") (f (quote ("codegen" "prost" "transport"))) (k 0)) (d (n "tonic-build") (r "^0.7") (f (quote ("transport" "prost"))) (k 1)))) (h "0na9qcz8wkgr2pfvmz9g1qc7v801m8nrpy390q4sbq61m1y3j4xv")))

(define-public crate-inx-1.0.0-beta.4 (c (n "inx") (v "1.0.0-beta.4") (d (list (d (n "prost") (r "^0.10") (f (quote ("prost-derive" "std"))) (k 0)) (d (n "tonic") (r "^0.7") (f (quote ("codegen" "prost" "transport"))) (k 0)) (d (n "tonic-build") (r "^0.7") (f (quote ("transport" "prost"))) (k 1)))) (h "1jx5675s2ljj5nk6dcrcz7ib6x61gfxik9d2nmdchi0vav4f3l3r")))

(define-public crate-inx-1.0.0-beta.5 (c (n "inx") (v "1.0.0-beta.5") (d (list (d (n "prost") (r "^0.11") (f (quote ("prost-derive" "std"))) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("codegen" "prost" "transport"))) (k 0)) (d (n "tonic-build") (r "^0.8") (f (quote ("transport" "prost"))) (k 1)))) (h "1zr9nrlfaic2jh7mjppnz7l7fq0jw0n37cqzn9qn5mzxvqskf7ah")))

(define-public crate-inx-1.0.0-beta.8 (c (n "inx") (v "1.0.0-beta.8") (d (list (d (n "prost") (r "^0.11") (f (quote ("prost-derive" "std"))) (k 0)) (d (n "tonic") (r "^0.8") (f (quote ("codegen" "prost" "transport"))) (k 0)) (d (n "tonic-build") (r "^0.8") (f (quote ("transport" "prost"))) (k 1)))) (h "183hbw3xqznw2l6ia01gi7vkh5r2j47zgh7lp0s89g685q7rniab")))

