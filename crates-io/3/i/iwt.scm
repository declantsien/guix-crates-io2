(define-module (crates-io #{3}# i iwt) #:use-module (crates-io))

(define-public crate-iwt-0.1.0 (c (n "iwt") (v "0.1.0") (d (list (d (n "async-openai") (r "^0.12.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ixzjnw71mpcrh57csy8s58zh2kvmap8146sr4whjjq02alsl552")))

