(define-module (crates-io #{3}# i ico) #:use-module (crates-io))

(define-public crate-ico-0.1.0 (c (n "ico") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 2)) (d (n "png") (r "^0.11") (d #t) (k 0)))) (h "01383zf7q4myi1ccsa82lykjb0iavk9n1blm82vajm22acqk6jva")))

(define-public crate-ico-0.2.0 (c (n "ico") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "1ga722vzd2nf4hnlsjxal25zxfsms7b16l33q1qqv31davz30583")))

(define-public crate-ico-0.3.0 (c (n "ico") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.30") (d #t) (k 2)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "1bkwgmqx53s6k706agjwhzyyra91sfpngbg1n7ny9d8bprh4k073")))

