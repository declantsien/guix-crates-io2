(define-module (crates-io #{3}# i inv) #:use-module (crates-io))

(define-public crate-inv-0.2.0 (c (n "inv") (v "0.2.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "humantime") (r "^1.3.0") (d #t) (k 0)) (d (n "inv_manager") (r "^0.2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)))) (h "0qffdwrxsls11vs1x3qyzaf7bpp20x9x5il8cnn182r1nifw07y6")))

