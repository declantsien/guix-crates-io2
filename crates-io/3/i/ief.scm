(define-module (crates-io #{3}# i ief) #:use-module (crates-io))

(define-public crate-ief-0.0.1 (c (n "ief") (v "0.0.1") (d (list (d (n "cpp_demangle") (r "^0.2.14") (d #t) (k 0)) (d (n "goblin") (r "^0.2.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.11") (d #t) (k 0)))) (h "1mbq5ybjxckrghm8fcbxj2wfk449x6q1mpa7y0vfribggy381za9")))

(define-public crate-ief-0.0.2 (c (n "ief") (v "0.0.2") (d (list (d (n "cpp_demangle") (r "^0.2.14") (d #t) (k 0)) (d (n "goblin") (r "^0.2.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.11") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "06p0wplkvhnd50mii46z14bp278545waclqlyfmnw3pk8nxjn2a2")))

