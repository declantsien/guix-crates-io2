(define-module (crates-io #{3}# i imd) #:use-module (crates-io))

(define-public crate-imd-1.4.0 (c (n "imd") (v "1.4.0") (d (list (d (n "clap") (r ">=3.2.17") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r ">=0.25.0") (d #t) (k 0)) (d (n "indicatif") (r ">=0.17.0") (d #t) (k 0)) (d (n "nix") (r ">=0.24.2") (d #t) (k 0)))) (h "12p8gljwghsdv883jqkjdb2mb9fcxyplp4rsy0qylnhsdwj2ncfk")))

(define-public crate-imd-1.5.0 (c (n "imd") (v "1.5.0") (d (list (d (n "clap") (r ">=3.2.17") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r ">=0.25.0") (d #t) (k 0)) (d (n "indicatif") (r ">=0.17.0") (d #t) (k 0)) (d (n "nix") (r ">=0.24.2") (d #t) (k 0)))) (h "1my9alyyrmihwj4pha8rf4j0hxrm7zri5r4na8fhgj0j5ggswc4x")))

(define-public crate-imd-2.0.0 (c (n "imd") (v "2.0.0") (d (list (d (n "clap") (r ">=4.0.32") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r ">=0.25.0") (d #t) (k 0)) (d (n "indicatif") (r ">=0.17.2") (d #t) (k 0)) (d (n "nix") (r ">=0.26.1") (d #t) (k 0)))) (h "12iv8n752yl09sa274ihfdi2x3rrzkivhr4vci5ml4l8lrfiiilx") (y #t)))

(define-public crate-imd-2.0.1 (c (n "imd") (v "2.0.1") (d (list (d (n "clap") (r ">=4.0.32") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r ">=0.25.0") (d #t) (k 0)) (d (n "indicatif") (r ">=0.17.2") (d #t) (k 0)) (d (n "nix") (r ">=0.26.1") (d #t) (k 0)))) (h "14k5w8q4smxwrfd8vslfki74fg0f2cp10vhkcjwzyxjy0bwr9pa1")))

