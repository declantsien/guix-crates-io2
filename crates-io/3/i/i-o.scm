(define-module (crates-io #{3}# i i-o) #:use-module (crates-io))

(define-public crate-i-o-0.1.0 (c (n "i-o") (v "0.1.0") (d (list (d (n "containers") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "0rfz9rha9pk8nip0p7wzn2gd88j1iai0nzybxvdrqa6g7s16ach8")))

(define-public crate-i-o-0.2.0 (c (n "i-o") (v "0.2.0") (d (list (d (n "containers") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "0vxny93z261sd0zq0vp1jrv9q2nk5nxx4bpgy9jdl2p9z27icpsb")))

(define-public crate-i-o-0.2.1 (c (n "i-o") (v "0.2.1") (d (list (d (n "containers") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.4.2") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "1xh8fgyp5c8x6mf4p5fvbyb4zx6wnmcn4i95r15kvhj2w7656pna")))

(define-public crate-i-o-0.2.2 (c (n "i-o") (v "0.2.2") (d (list (d (n "containers") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.4.2") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "0inga1kx3gbbk731l012h6w02d43kqzj6wlhf04qnx3dn6fknlh8")))

(define-public crate-i-o-0.3.0 (c (n "i-o") (v "0.3.0") (d (list (d (n "containers") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.4.2") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "00pl85chs92pzwzkrzi0w4gsizgvvkkzfgh3kziskhwwqs4qrps1") (y #t)))

(define-public crate-i-o-0.3.1 (c (n "i-o") (v "0.3.1") (d (list (d (n "containers") (r "^0.7") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.4.2") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "0j257nm9fnp9537r21zig04jy8sbfgdi8fdh13hv8dxyh0dj6dqx")))

(define-public crate-i-o-0.4.0 (c (n "i-o") (v "0.4.0") (d (list (d (n "containers") (r "^0.8") (d #t) (k 0)) (d (n "loca") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "1ny8a3kqkix4mvap7csm68xb6dd1lcmxnx7l95bfcy0y9nhgb182")))

(define-public crate-i-o-0.4.1 (c (n "i-o") (v "0.4.1") (d (list (d (n "containers") (r "^0.8") (k 0)) (d (n "loca") (r "^0.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "19nng25qfbg9vp9ryqrvd4xzqa84li9633psjq94ml45nqyxy4cl")))

(define-public crate-i-o-0.4.2 (c (n "i-o") (v "0.4.2") (d (list (d (n "containers") (r ">= 0.8, < 0.10") (f (quote ("default_allocator"))) (k 0)) (d (n "loca") (r ">= 0.6, < 0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "0lyaxfd8ii639n7hfii50j663ic29lgraki1f88kg0kfavc25wlj")))

(define-public crate-i-o-0.4.3 (c (n "i-o") (v "0.4.3") (d (list (d (n "containers") (r ">= 0.8, < 0.10") (f (quote ("default_allocator"))) (k 0)) (d (n "loca") (r ">= 0.6, < 0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "02qkflz1vinl6vyfpbn3s6xn2blrp1jf07i3l8yijpxx61zjdl3c")))

(define-public crate-i-o-0.4.4 (c (n "i-o") (v "0.4.4") (d (list (d (n "containers") (r ">= 0.8, < 0.10") (f (quote ("default_allocator"))) (k 0)) (d (n "loca") (r ">= 0.6, < 0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "1zwr2qck72f4hr4f5qfsq3zfhnnj4ahhkpfkmar7sgpiga2hlh6q")))

(define-public crate-i-o-0.4.5 (c (n "i-o") (v "0.4.5") (d (list (d (n "containers") (r ">=0.8, <0.10") (f (quote ("default_allocator"))) (k 0)) (d (n "either") (r "^1") (k 0)) (d (n "loca") (r ">=0.6, <0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "0wbsl5md2hy7ax10xdywdryxc47mj4w6v1s3hrdi23dxr1v9a419") (y #t)))

(define-public crate-i-o-0.4.6 (c (n "i-o") (v "0.4.6") (d (list (d (n "containers") (r ">=0.8, <0.10") (f (quote ("default_allocator"))) (k 0)) (d (n "either") (r "^1") (k 0)) (d (n "loca") (r ">=0.6, <0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "1mq2py799g53c6fdm0181zzxm9259wwrsm7gxvv01q1jg0im8bmh")))

(define-public crate-i-o-0.4.7 (c (n "i-o") (v "0.4.7") (d (list (d (n "containers") (r ">=0.8, <0.10") (f (quote ("default_allocator"))) (k 0)) (d (n "either") (r "^1") (k 0)) (d (n "loca") (r ">=0.6, <0.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "void") (r "^1") (k 0)))) (h "1iclqk0zhs2wfvr1ng0qs9gcdg4incisxfjmgv18kc6ax9r045yj")))

