(define-module (crates-io #{3}# i igc) #:use-module (crates-io))

(define-public crate-igc-0.1.0 (c (n "igc") (v "0.1.0") (d (list (d (n "memmap") (r "^0.7.0") (d #t) (k 2)))) (h "0qz0yam89a81wz3bbphgqqzdl3vkck8crnm6lflwx2ib46zdmgss")))

(define-public crate-igc-0.2.0 (c (n "igc") (v "0.2.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "encoding") (r "^0.2.33") (d #t) (k 2)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "1ssagvy69s7k23pasg7nfqm3lv67v856ys7z3cvjqb4r0d98f4i0")))

(define-public crate-igc-0.2.1 (c (n "igc") (v "0.2.1") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "encoding") (r "^0.2.33") (d #t) (k 2)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "1qf30zrbwp2pp65i1nkpijiv2ia4rx3pvzkb0gx2visz7m02z062")))

(define-public crate-igc-0.2.2 (c (n "igc") (v "0.2.2") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "encoding") (r "^0.2.33") (d #t) (k 2)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)))) (h "181aixxic4hgz968gqf0swyimdp4rcj8715i0ihizyw30jwp3q2p")))

