(define-module (crates-io #{3}# i ioc) #:use-module (crates-io))

(define-public crate-ioc-0.0.0 (c (n "ioc") (v "0.0.0") (h "0n8x6gw1amr6jabbmv9r5ifb7405hbv30lnhwhirjam1a4ygpcl6")))

(define-public crate-ioc-0.0.1 (c (n "ioc") (v "0.0.1") (d (list (d (n "downcast") (r "^0.2.0") (d #t) (k 0)))) (h "1lwbd3vkqc80k3sdwpa8d1yhv2w7xz53p1vbb10abvhib71fkcf1")))

(define-public crate-ioc-0.0.2 (c (n "ioc") (v "0.0.2") (d (list (d (n "downcast") (r "^0.2.0") (d #t) (k 0)))) (h "0hyh07sjlrj2bhmj3mvm8a0sm9iwnprywxnxksz2b1rphpjkw4fi")))

(define-public crate-ioc-0.0.3 (c (n "ioc") (v "0.0.3") (d (list (d (n "downcast") (r "^0.2.0") (d #t) (k 0)))) (h "0sihsghjgkng9984681r50kfm1ckxqkp7sv1dcjd48ikbv51hk6x")))

(define-public crate-ioc-0.0.4 (c (n "ioc") (v "0.0.4") (d (list (d (n "downcast") (r "^0.2.0") (d #t) (k 0)))) (h "0kfhgs97589qlb0l0wsmy7l6ayi3c7nin8wx1fxvvzr373dbc1p6")))

(define-public crate-ioc-0.0.5 (c (n "ioc") (v "0.0.5") (d (list (d (n "downcast") (r "^0.2.0") (d #t) (k 0)))) (h "1jicb9zkxyqm0c5q5abbj9s001lx0p08jhhc3dzcbrkasgs9r7f8")))

(define-public crate-ioc-0.1.0 (c (n "ioc") (v "0.1.0") (d (list (d (n "downcast") (r "^0.2.0") (d #t) (k 0)))) (h "109axqacvmgx0w1igps3cqlxyx92p9sp2wap5h4jnyb0gxx6cira")))

(define-public crate-ioc-0.1.1 (c (n "ioc") (v "0.1.1") (d (list (d (n "downcast") (r "^0.2.0") (d #t) (k 0)))) (h "18dy56fjx9qhqz1ycl331ykmykzs2s69mihjjfmp14pgzwasmng9")))

(define-public crate-ioc-0.2.0 (c (n "ioc") (v "0.2.0") (d (list (d (n "downcast") (r "^0.3") (d #t) (k 0)))) (h "00w2ffhqpv19hmb4v0xwasi1axp0rd01bhvndpxnyrr3b0f8xx0j")))

(define-public crate-ioc-0.3.0 (c (n "ioc") (v "0.3.0") (d (list (d (n "downcast") (r "^0.4") (d #t) (k 0)))) (h "1ky54k3sr0nk6f0z78n6b4wp4xzk3q8sdxr524q982776gr14mgq")))

(define-public crate-ioc-0.3.1 (c (n "ioc") (v "0.3.1") (d (list (d (n "downcast") (r "^0.4") (d #t) (k 0)))) (h "00yq3r1ssvr4bin6zhdd2px32hghxla80qz4n90n8g1vip8b2yba")))

(define-public crate-ioc-0.3.2 (c (n "ioc") (v "0.3.2") (d (list (d (n "downcast") (r "^0.4") (d #t) (k 0)) (d (n "shared-mutex") (r "^0.3") (d #t) (k 0)))) (h "04f665l9lwv7ii79zfflhl81w9pn4cx9w0hjfkia6wi5irhvcwx0")))

(define-public crate-ioc-0.5.0 (c (n "ioc") (v "0.5.0") (d (list (d (n "downcast") (r "^0.4") (d #t) (k 0)))) (h "0p7fcl47049wnckapgq01dps858fbsjhx8dwx4xmmw1yy1fc6nc0")))

(define-public crate-ioc-0.5.1 (c (n "ioc") (v "0.5.1") (d (list (d (n "downcast") (r "^0.5") (d #t) (k 0)))) (h "09asjhy6ijj2piqgqfn85p0jdid29jgrl32rqg4z84y31z0z36wb")))

(define-public crate-ioc-0.5.2 (c (n "ioc") (v "0.5.2") (d (list (d (n "downcast") (r "^0.5") (d #t) (k 0)))) (h "14dg07dkb0pkarzcq6bmzw0jrc7vw5rx7vbh14759dsijmj8qr69")))

(define-public crate-ioc-0.5.3 (c (n "ioc") (v "0.5.3") (d (list (d (n "downcast") (r "^0.5") (d #t) (k 0)))) (h "1s5f67m57002glfxnwicj7f2ij9zfrbi5fssg0kbkh247xng5lsp")))

(define-public crate-ioc-0.5.4 (c (n "ioc") (v "0.5.4") (d (list (d (n "downcast") (r "^0.6") (d #t) (k 0)))) (h "0yajzmagfp0al4a9fn856s1pgakhgi1pmn6w62fh9gp2xf5b3sx3")))

(define-public crate-ioc-0.6.0 (c (n "ioc") (v "0.6.0") (d (list (d (n "downcast") (r "^0.6") (d #t) (k 0)))) (h "1zcylqv7ldq15yim9jwzdy1hnszdpf9xfmxcmwh5965mj8mpzc0w")))

(define-public crate-ioc-0.6.1 (c (n "ioc") (v "0.6.1") (d (list (d (n "downcast") (r "^0.6") (d #t) (k 0)))) (h "0dh8d3p8750rdxi7lah1yydcj27fgq8v0m12hv5l27i4ccf8fj0s")))

(define-public crate-ioc-0.6.2 (c (n "ioc") (v "0.6.2") (d (list (d (n "downcast") (r "^0.6") (d #t) (k 0)))) (h "1ha76p7cg3g74lb068qdsbjcyp8g5ha288bzldz0x6zd3p33jzdz")))

(define-public crate-ioc-0.9.0 (c (n "ioc") (v "0.9.0") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "downcast") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variadic_generics") (r "^0.1.2") (d #t) (k 0)))) (h "08aspjmxkb3yzqs9dhjsvqzwiv8bn3jw33xkgaiby8q8796qigy9")))

(define-public crate-ioc-0.9.1 (c (n "ioc") (v "0.9.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "downcast") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variadic_generics") (r "^0.1.2") (d #t) (k 0)))) (h "1jkc6r899jvymah1xy3qqd1br4ir7rbiw16n0js3zjr6f0645qrj")))

(define-public crate-ioc-0.10.0 (c (n "ioc") (v "0.10.0") (d (list (d (n "downcast") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variadic_generics") (r "^0.1.2") (d #t) (k 0)))) (h "1ig6kyk4zd37dpsw2z2xsmarrpn45r38vrh0gxqq7d46wdd5kh15")))

(define-public crate-ioc-0.11.0 (c (n "ioc") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "downcast") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variadic_generics") (r "^0.1.2") (d #t) (k 0)))) (h "04qrsxfvclb5bpkfbypqznjazig4z1hjxn03s3pgqrd9km0vd0p3")))

(define-public crate-ioc-0.11.1 (c (n "ioc") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "downcast") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variadic_generics") (r "^0.1.2") (d #t) (k 0)))) (h "0nzd54win1vja1qdqvw9746q6vll6783rg6f5d7q6qikyk8ica8n")))

(define-public crate-ioc-0.12.0 (c (n "ioc") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "downcast") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "variadic_generics") (r "^0.1.2") (d #t) (k 0)))) (h "125ipy7xgzvmr1ryrc332c7rgj78424r7izk17zhvx4nlrs99198") (f (quote (("default" "async") ("async"))))))

