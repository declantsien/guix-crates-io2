(define-module (crates-io #{3}# i ika) #:use-module (crates-io))

(define-public crate-ika-0.1.0 (c (n "ika") (v "0.1.0") (h "1mf9ir6a9vmgg5vn2m2bkl6wv0qsg9sjiimghzwliak8w252sii7")))

(define-public crate-ika-0.2.0 (c (n "ika") (v "0.2.0") (h "1p9jk7f73p4mszwjx4vqmvm4vbv9fd07qmx5gmh9fsfgrplhgrcs")))

(define-public crate-ika-0.3.0 (c (n "ika") (v "0.3.0") (h "181axz2aypp5pw488xansdg9bsd1n5qwjm6wbb406gc8qjvipylf")))

(define-public crate-ika-0.3.1 (c (n "ika") (v "0.3.1") (h "0ajwy84rs9b4p4ly0ijn6px4lvyd8ix0f291rj0ib5wb70m4gpyz")))

(define-public crate-ika-0.4.0 (c (n "ika") (v "0.4.0") (h "0d8js7ks8bdlbagflb3papm1wmr4zvp5nw5kq0jpc3ibrz9zn5lb")))

(define-public crate-ika-0.4.1 (c (n "ika") (v "0.4.1") (h "0x7v9p32wmfigmddwy0spv6rmxiy57rp12zgc4a5l2x50fni9q4h")))

