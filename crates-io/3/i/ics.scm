(define-module (crates-io #{3}# i ics) #:use-module (crates-io))

(define-public crate-ics-0.1.0 (c (n "ics") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "1v0qmszv1ksdl5pskrjjjkqj6zmnjsy3ffcj5mk6nbji0cpwa51i") (f (quote (("fast_encoding" "regex" "lazy_static") ("default" "fast_encoding"))))))

(define-public crate-ics-0.1.1 (c (n "ics") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "15l2f1flc5mfki80szgnaw6zdbc4m0rvbzaj79zkq7kz6axa3nlp") (f (quote (("fast_encoding" "regex" "lazy_static") ("default" "fast_encoding"))))))

(define-public crate-ics-0.2.0 (c (n "ics") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0h02p2h7984bg77y1ccnw3yak6x1bnghb43qz56lb5bk3xw1rh6v") (f (quote (("rfc7986") ("fast_text" "regex" "lazy_static") ("default" "fast_text" "rfc7986"))))))

(define-public crate-ics-0.2.1 (c (n "ics") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "08bsncrnbjab88zjnnncggylxnv7av5bgz3l1lv871hh1i2ikc6k") (f (quote (("rfc7986") ("fast_text" "regex" "lazy_static") ("default" "fast_text" "rfc7986"))))))

(define-public crate-ics-0.2.2 (c (n "ics") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "1z0603i812mdm4rnzpcgpzrhz4y86q0vkxh5i6xdiykdkxmqnrqb") (f (quote (("rfc7986") ("fast_text" "regex" "lazy_static") ("default" "fast_text" "rfc7986"))))))

(define-public crate-ics-0.2.3 (c (n "ics") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)))) (h "0d6k273nqyh3xamqc1qsg3kakgmka3dix0i22w71p2jr0hxkv9v6") (f (quote (("rfc7986") ("fast_text" "regex" "lazy_static") ("default" "fast_text" "rfc7986"))))))

(define-public crate-ics-0.3.0 (c (n "ics") (v "0.3.0") (h "0gbxanlp0p936r027n3c78j7gfgf38m2ndkqjahl2xk5yk8dmg95") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.3.1 (c (n "ics") (v "0.3.1") (h "1yalmyxpmvyg9vpslfbbsp3skxdg5s877p6l0lkczxxwfksapz39") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.3.2 (c (n "ics") (v "0.3.2") (h "19qg6rb9bkngfm6zvfag74xl9p5m678ixdhg9f0lic1vdfcvi8jr") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.4.0 (c (n "ics") (v "0.4.0") (h "0qfr2q18rm8lif0mraqy6l9qdnjag4bfws9ysa6ldvl55l80h8br") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.4.1 (c (n "ics") (v "0.4.1") (h "0pmga6shrhdld8s522h69l94ksglzay4kmpj4ywhxzr7hgs63rpx") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.4.2 (c (n "ics") (v "0.4.2") (h "1mjk2rcrpa91k987rqjrcb1c67rghma9bxjsknqsiyrbz0ijkrh0") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.4.3 (c (n "ics") (v "0.4.3") (h "032kb7yhh19vx96ip7b5wiimq70ndlqvv6nw164lbyvs4cywg1sv") (f (quote (("rfc7986") ("default" "rfc7986")))) (y #t)))

(define-public crate-ics-0.4.4 (c (n "ics") (v "0.4.4") (h "0w8in5ha5crfv0hv1cc1g0scvyjmjfnj0iim7m7fx40b041hzb17") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.5.0 (c (n "ics") (v "0.5.0") (h "15yyw9mx5f98i3kk1pl5q5qwq6jsn4m7fvzmlkg6qnc9mjlrgd91") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.5.1 (c (n "ics") (v "0.5.1") (h "1r2br611xdi5j8zgmkbm4dv8f937hcj9lqbj90j7i8v4fvbm5lgj") (f (quote (("rfc7986") ("default" "rfc7986")))) (y #t)))

(define-public crate-ics-0.5.2 (c (n "ics") (v "0.5.2") (h "1lr1m0snvyifp6l6c6sm98bwq1f165icbmgw4i1k09psjlvwy8lx") (f (quote (("rfc7986") ("default" "rfc7986")))) (y #t)))

(define-public crate-ics-0.5.3 (c (n "ics") (v "0.5.3") (h "0pdkgh75a33d6q9mm0yr800wywwxyy4j9nmpb3ahqnc5rrpdq6xa") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.5.4 (c (n "ics") (v "0.5.4") (h "0afk4lmwfa46ky8dagqws9z1igwlgwslkd0ix6pvsdz5x5qiidbi") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.5.5 (c (n "ics") (v "0.5.5") (h "1wss3j1h8pc7d95qcakfn3h8qwrdz2dc5dgngvmv74axqrbh5n4z") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.5.6 (c (n "ics") (v "0.5.6") (h "05j955b24r06scfyxbg1rrhv2cs9wda073j7057di0jmwszai7m4") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.5.7 (c (n "ics") (v "0.5.7") (h "1c9650c675zr2a690ris6zj3k6ipkr350ild26bkngimyqg4i4dq") (f (quote (("rfc7986") ("default" "rfc7986"))))))

(define-public crate-ics-0.5.8 (c (n "ics") (v "0.5.8") (h "1yli393gyy0v53iama7g1sms0fvjs2yfr55mwwcjvphs8qpwbgiy") (f (quote (("rfc7986") ("default" "rfc7986"))))))

