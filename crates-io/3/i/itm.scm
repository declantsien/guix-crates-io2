(define-module (crates-io #{3}# i itm) #:use-module (crates-io))

(define-public crate-itm-0.1.0 (c (n "itm") (v "0.1.0") (d (list (d (n "ref_slice") (r "^1.1.0") (d #t) (k 0)))) (h "0ach240mfqywrw500v6jz85zzxyxng4hbfhgsdkifnc6sl5r2d8v")))

(define-public crate-itm-0.1.1 (c (n "itm") (v "0.1.1") (d (list (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "ref_slice") (r "^1.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "04r8rgrxr2rd0nm43ky3yg5w966ivk7gvgw5zmsn1gci18x6hdn4")))

(define-public crate-itm-0.2.0 (c (n "itm") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1jr11k84kmzp5vnbd620dl2c9jzrl7kmbnc566isnxrdf6raj8cb")))

(define-public crate-itm-0.2.1 (c (n "itm") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1xs8929cf5hkp8251sgmy3qhr605ylcaimdb4x298hm1g16fdn7c")))

(define-public crate-itm-0.3.0 (c (n "itm") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0xxa2wh10pzfl4k0afzx0vrg5vh98hnvj1zgxcgq47k5apfffvyz")))

(define-public crate-itm-0.3.1 (c (n "itm") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.14.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "1wknl58ac6r0si9w8rf9vfgvx3ilsxcdfmc76hbffxyaxvdns3x1")))

(define-public crate-itm-0.9.0-rc.1 (c (n "itm") (v "0.9.0-rc.1") (d (list (d (n "bitmatch") (r "^0.1.1") (d #t) (k 0)) (d (n "bitvec") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.23") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "04abrp84620daxcihl0s79cmr6vrqzbaqvyc2xqaqf5j921y02fz") (f (quote (("serial" "nix") ("default"))))))

