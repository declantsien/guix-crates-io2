(define-module (crates-io #{3}# i ioe) #:use-module (crates-io))

(define-public crate-ioe-0.1.0 (c (n "ioe") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0lyc12p2prf9fcyaqsw09aa0nhc2hggc7hhipnnv5vsgp91j4k16")))

(define-public crate-ioe-0.2.0 (c (n "ioe") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0b50apyz9ng3wnb9ciapqbfvf717ixirz57nrhizqjzqjr8c31y8")))

(define-public crate-ioe-0.3.0 (c (n "ioe") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "05rbgxb14jil88hjwzbrx6v6mcb9fwi8amczk2qz58qkfmw0kglh")))

(define-public crate-ioe-0.4.0 (c (n "ioe") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "02s82xpb61hs6fjbds8qvg968cpyigpi99zvsghlk2s9br9847dq")))

(define-public crate-ioe-0.5.0 (c (n "ioe") (v "0.5.0") (d (list (d (n "deltoid") (r "^0.11") (f (quote ("snapshot"))) (d #t) (k 0)) (d (n "deltoid-derive") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0rj17hq824ldhqjvqkqrr54f9jbyymnpkn7h8w1s849ybbaxgf68")))

(define-public crate-ioe-0.5.1 (c (n "ioe") (v "0.5.1") (d (list (d (n "deltoid") (r "^0.11") (f (quote ("snapshot"))) (d #t) (k 0)) (d (n "deltoid-derive") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0wbc1xad2x16anbbq4h96kivbn0psmjwlzyc6j428klq3sak8zxl")))

(define-public crate-ioe-0.5.2 (c (n "ioe") (v "0.5.2") (d (list (d (n "deltoid") (r "^0.12.0") (f (quote ("snapshot"))) (d #t) (k 0)) (d (n "deltoid-derive") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "11smn2g2ln926mdb54mkxahw4c2xpzmkfw676vb0sxgrnsjhvp7j")))

(define-public crate-ioe-0.6.0 (c (n "ioe") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)))) (h "181rjfkcf44bb35fqykiz11dgi5i0mycybya0kcngrk1rnp2hknd") (r "1.72.0")))

