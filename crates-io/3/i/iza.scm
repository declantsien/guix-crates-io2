(define-module (crates-io #{3}# i iza) #:use-module (crates-io))

(define-public crate-iza-0.1.0 (c (n "iza") (v "0.1.0") (d (list (d (n "blake2") (r "= 0.8.1") (d #t) (k 0)) (d (n "futures") (r "= 0.1.28") (d #t) (k 0)) (d (n "hex") (r "= 0.3.2") (d #t) (k 0)) (d (n "serde") (r "= 1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.40") (d #t) (k 0)) (d (n "uuid") (r "= 0.7.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0g21cp1p5fqgimc52nb6xc6nw40y8x95djcvmmsd8nv5aay2n5m6")))

