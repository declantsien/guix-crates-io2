(define-module (crates-io #{3}# i iia) #:use-module (crates-io))

(define-public crate-iia-0.1.0 (c (n "iia") (v "0.1.0") (h "0r8nabbks8diijjbldgwjmv71qvspv026c6ga6qnl3xrw3k9fkx2")))

(define-public crate-iia-0.1.1 (c (n "iia") (v "0.1.1") (h "03mkg0rp2h7vxnnj6bxhmlgky8rqnzzqp6jk0yrpj7nyp9pb4bg8")))

(define-public crate-iia-0.1.2 (c (n "iia") (v "0.1.2") (h "0ls1y7rp9vdl84h8f9r0hd8qym1hkch7divgsdxzc0cnw09dr1rf")))

