(define-module (crates-io #{3}# i icb) #:use-module (crates-io))

(define-public crate-icb-0.1.0 (c (n "icb") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)))) (h "1a7b33n2lybqad0lm1fd7vscdxgsj44hf4n5f18lngvdyplrxqjx")))

(define-public crate-icb-0.1.1 (c (n "icb") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)))) (h "0frwxzrclfa1cimknd6c7i182dx7wl3iznfswggqzm711qsc1nym")))

(define-public crate-icb-0.2.0 (c (n "icb") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)))) (h "16ikq9hmcn1sql0lf0ibm04vc1qd490gc8y1vzpd84ra2ynkgnfr")))

(define-public crate-icb-0.2.1 (c (n "icb") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)))) (h "0ad32z66w42sgcw9hsrgqn0knsrr77vj7j1j0fzwpykgqnx1xnsw")))

(define-public crate-icb-0.2.2 (c (n "icb") (v "0.2.2") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "maplit") (r "^1.0") (d #t) (k 0)))) (h "1vap1sxbyahnql3m6z9b4zcf607rf5klvk83hbmh9wlqjx6kdmwx")))

