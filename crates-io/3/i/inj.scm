(define-module (crates-io #{3}# i inj) #:use-module (crates-io))

(define-public crate-inj-0.1.0 (c (n "inj") (v "0.1.0") (d (list (d (n "handlebars") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 0)))) (h "11wdflrxgq9p5zj8d9rgqjd4qsfl5i8fbng3fm1nvr4has98ndxg")))

