(define-module (crates-io #{3}# i itc) #:use-module (crates-io))

(define-public crate-itc-0.1.0 (c (n "itc") (v "0.1.0") (h "1ivvi6gc4xmai2236nxpmf39gay4yc5kfij6qvr53gi87nv486xl")))

(define-public crate-itc-0.1.1 (c (n "itc") (v "0.1.1") (h "0ijm3szanc1s6960i0akx32masd6r6i4sgx83b2y0d7aq989dbk7")))

(define-public crate-itc-0.1.2 (c (n "itc") (v "0.1.2") (h "1jqb9rmcwi39fm34xlai1ld7kpx2hq5vawkl5mgxfs4dysb7mi02")))

