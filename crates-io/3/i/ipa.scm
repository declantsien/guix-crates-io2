(define-module (crates-io #{3}# i ipa) #:use-module (crates-io))

(define-public crate-ipa-0.3.0 (c (n "ipa") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0") (d #t) (k 0)) (d (n "stderrlog") (r "^0.5") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0zw0fjzfyj079md08k68f9g0gkfrn8gs9bkdbj82gghjp2blswyy")))

