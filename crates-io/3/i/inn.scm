(define-module (crates-io #{3}# i inn) #:use-module (crates-io))

(define-public crate-inn-0.1.0 (c (n "inn") (v "0.1.0") (h "1nqh90pp8v21hgk0nknqjpbgi751lg6xjjzsnywssx4dydkbp3qn")))

(define-public crate-inn-0.2.0 (c (n "inn") (v "0.2.0") (d (list (d (n "actix") (r "^0.13") (d #t) (k 0)) (d (n "actix-rt") (r "^2.0.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "inn-network") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("full"))) (d #t) (k 0)))) (h "19ybp5gw5sq196zm4fj02r7c6n2rcxjbhr4rr8q736wsvalgci11")))

