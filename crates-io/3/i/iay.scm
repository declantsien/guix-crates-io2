(define-module (crates-io #{3}# i iay) #:use-module (crates-io))

(define-public crate-iay-0.1.2 (c (n "iay") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "039147p937bh3qdff63mxnxz9hlax05dny6fk9ww6y3kh1dvp6fk")))

(define-public crate-iay-0.1.3 (c (n "iay") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "0y0iwhvf0nvw8wnds293mw95f7qgs6wv7xfn6f65425kwf7s09qa")))

(define-public crate-iay-0.1.4 (c (n "iay") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "01xhl6q4izqpgiii2z13xknpcdiwplvxm8yqnh103q24lqspfw2s")))

(define-public crate-iay-0.1.5 (c (n "iay") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "140hhfq7vf13n4wjnjhvammvjzid9f821zs7i8jqy71dfvs99r2j")))

(define-public crate-iay-0.1.6 (c (n "iay") (v "0.1.6") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "0im0i477vfvwwqn8gwlm907q7kcxj2fdy5lvc7wyc1ss7q51ikgc")))

(define-public crate-iay-0.1.7 (c (n "iay") (v "0.1.7") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "git2") (r "^0.13.25") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "047xc2gpzbcjryrlv6qx4j34n6saxla4nzc3pjjpm9crw2s7hcz3")))

(define-public crate-iay-0.1.8 (c (n "iay") (v "0.1.8") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "0r79ns72f8fi2ii0703vsk59jjha40381fi2awpmy6zx90hzr189") (y #t)))

(define-public crate-iay-0.2.0 (c (n "iay") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "0k375zhyzij69m8hd4nsaqn23c1dnzi23jl6c3rmkna47ywvzl4g")))

(define-public crate-iay-0.3.0 (c (n "iay") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^3.1.15") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.125") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "1d7ny1vwag89pcdfzlmxhi8msr41z9wpjrrbljiiyppanhxg05hj")))

(define-public crate-iay-0.4.0 (c (n "iay") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "0b0jh1kc93vgdscl71ghp4w770bjp70i0qyh2zg70f4ihfl07aw9")))

(define-public crate-iay-0.4.2 (c (n "iay") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "tico") (r "^2.0.0") (d #t) (k 0)))) (h "0dfsxliqwgqbci1d5rk13315h5lgz654snbrwn28s8jf8k0sf2zm")))

