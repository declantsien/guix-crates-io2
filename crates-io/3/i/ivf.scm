(define-module (crates-io #{3}# i ivf) #:use-module (crates-io))

(define-public crate-ivf-0.1.0 (c (n "ivf") (v "0.1.0") (d (list (d (n "bitstream-io") (r "^0.8") (d #t) (k 0)))) (h "1wfjf3rilqavrhvwagzinvng9dg28wcjk3c6c6p5qmc1xy65qfh1")))

(define-public crate-ivf-0.1.1 (c (n "ivf") (v "0.1.1") (d (list (d (n "bitstream-io") (r "^1") (d #t) (k 0)))) (h "1qmpqnwlcvp7xpi1f6l63icaafpsak6hv7s326snffhs6rj1rc0g")))

(define-public crate-ivf-0.1.3 (c (n "ivf") (v "0.1.3") (d (list (d (n "bitstream-io") (r "^2") (d #t) (k 0)))) (h "1jjy911flpfpflnxw5fqsx6a3ghaq5wi2q18nx9cawpf81qnabsm")))

