(define-module (crates-io #{3}# i irv) #:use-module (crates-io))

(define-public crate-irv-0.0.1 (c (n "irv") (v "0.0.1") (h "06cv6h8r358g5snzsas3f8sfja4648viv19v4na1lhyr53kjjsza")))

(define-public crate-irv-0.0.2 (c (n "irv") (v "0.0.2") (d (list (d (n "irv-loader") (r "^0.0.1") (d #t) (k 2)) (d (n "irv-traits") (r "^0.0.1") (d #t) (k 0)))) (h "0y99h28sv69xxchqy5mrqb5vlqm05i4myvv4ckx1q6wmxlk79bnc")))

