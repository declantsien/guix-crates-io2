(define-module (crates-io #{3}# i idx) #:use-module (crates-io))

(define-public crate-idx-0.1.0 (c (n "idx") (v "0.1.0") (d (list (d (n "diskvec") (r "^0.1.1") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "09vajq6nqpk0s570k8wbvg6ihxj17wldkzzhmlcr0cxibssf9hqy")))

(define-public crate-idx-0.1.1 (c (n "idx") (v "0.1.1") (d (list (d (n "diskvec") (r "^0.1.2") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "0ymav2kznlq31bxglhzk7iy99b3cxa8rmlpkbm0bq2n9q2c15754")))

(define-public crate-idx-0.2.0 (c (n "idx") (v "0.2.0") (d (list (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.3") (d #t) (k 0)) (d (n "seahash") (r "^3.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.5") (d #t) (k 2)))) (h "05s0i4xcxv6j1klisrq8yyjar2g2zisi772shbqs0wv1bjmxxgbi")))

