(define-module (crates-io #{3}# i ic0) #:use-module (crates-io))

(define-public crate-ic0-0.18.4 (c (n "ic0") (v "0.18.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 1)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 1)))) (h "189irgjp84cqwkgp3y31nn46v0nyzi38ipj2a71lskf922n48z1h") (r "1.60.0")))

(define-public crate-ic0-0.18.5 (c (n "ic0") (v "0.18.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)))) (h "0cdgygq4xd3clg8jw814w9j2x9wpfby8l6kjfwblchjx7xmizawq") (r "1.60.0")))

(define-public crate-ic0-0.18.6 (c (n "ic0") (v "0.18.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)))) (h "00mf1lj1amm9cicn4p88581d8wf0rm5mdf3lhwimdi84pq5xs8f2") (r "1.60.0")))

(define-public crate-ic0-0.18.7 (c (n "ic0") (v "0.18.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)))) (h "0kvz32ci3l75459a9hp6yssa0nzlj09q0s2k4zp0fyb6im1w2zp1") (r "1.60.0")))

(define-public crate-ic0-0.18.9 (c (n "ic0") (v "0.18.9") (d (list (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)))) (h "1z9xx4867jv2lfn53slr053kafyykj1igdsd2jq2x7fyg3y932wp") (r "1.60.0")))

(define-public crate-ic0-0.18.10 (c (n "ic0") (v "0.18.10") (d (list (d (n "quote") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)))) (h "0ygzyg0fpjvyhjm2249s1nh7wqxhbyka3453nwq86qj6rz7a0zqq") (r "1.60.0")))

(define-public crate-ic0-0.18.11 (c (n "ic0") (v "0.18.11") (d (list (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)))) (h "199swjs6l8idrw15nsf50k6zl66x110mrhm0s6s9yxnla68m6v2p") (f (quote (("wasi")))) (r "1.65.0")))

(define-public crate-ic0-0.18.12 (c (n "ic0") (v "0.18.12") (d (list (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)))) (h "1md5yndgvdfy9591lvr3d6ai6m5081vbzv8aln6kdsmhv7jxpvqn") (y #t) (r "1.65.0")))

(define-public crate-ic0-0.21.0 (c (n "ic0") (v "0.21.0") (d (list (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)))) (h "0lzbng14d716diy21w1mmdawsb7kbjvs2m0h9fyvj5mspm43xmv9") (r "1.66.0")))

(define-public crate-ic0-0.21.1 (c (n "ic0") (v "0.21.1") (d (list (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^1") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)))) (h "1pxwvj2vpnrfk6ypvj9vqdf1gbaxh3gl733fcx8iar8whsbm4jx5") (r "1.66.0")))

(define-public crate-ic0-0.23.0 (c (n "ic0") (v "0.23.0") (d (list (d (n "quote") (r "^1") (d #t) (k 2)) (d (n "syn") (r "^2") (f (quote ("parsing" "full" "extra-traits"))) (d #t) (k 2)))) (h "0zq61i3d1g1bmnadw68ac7fa2bx17dawih9xw9rq1mdvczfm9qld") (r "1.75.0")))

