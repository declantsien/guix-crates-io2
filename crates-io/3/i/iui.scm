(define-module (crates-io #{3}# i iui) #:use-module (crates-io))

(define-public crate-iui-0.1.0 (c (n "iui") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ui-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0gizrbx1g442932j6y3cak2kw1m4gnzhg3fbbx42h4ndqarw8904")))

(define-public crate-iui-0.2.0 (c (n "iui") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ui-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1b32rqs6ihyairs65asqqldhia204bvmm95n6z403q3nbg0b3ykf")))

(define-public crate-iui-0.3.0 (c (n "iui") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "ui-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0mqzlpfjh8sk6pv82ja249n3paigv4rlk3jxg0010gkc8sp37pqw")))

