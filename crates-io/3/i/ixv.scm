(define-module (crates-io #{3}# i ixv) #:use-module (crates-io))

(define-public crate-ixv-0.0.1 (c (n "ixv") (v "0.0.1") (d (list (d (n "anyhow") (r "1.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "0.*") (d #t) (k 0)))) (h "0042maqj70d5kal6mimpxh84nw8ldhvy9l6imigf28ckhsqnpnhl")))

(define-public crate-ixv-0.0.2 (c (n "ixv") (v "0.0.2") (d (list (d (n "anyhow") (r "1.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "0.*") (d #t) (k 0)))) (h "00i6wnasjs97icxi0fp93gky706amqmri1qxkrw3hi8ram9afnkf")))

(define-public crate-ixv-0.0.3 (c (n "ixv") (v "0.0.3") (d (list (d (n "anyhow") (r "1.*") (d #t) (k 0)) (d (n "clap") (r "4.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "0.*") (d #t) (k 0)))) (h "1xybn9mcn90dcmzidksp5xaw287jarkm76injxdzrb4hjqh2v6c2")))

