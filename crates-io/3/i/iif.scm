(define-module (crates-io #{3}# i iif) #:use-module (crates-io))

(define-public crate-iif-1.0.0 (c (n "iif") (v "1.0.0") (h "0kbs73q0w3lasjgay8vr48p6gyay1kidhvjlmjsi79qq6mwnqqjg")))

(define-public crate-iif-1.0.1 (c (n "iif") (v "1.0.1") (h "0l373iflfp4kypaznjbqlfk3nfaqs94l0y2gy9qxcshmi98qh6iq")))

(define-public crate-iif-1.1.0 (c (n "iif") (v "1.1.0") (h "1fdczxcp8m3pfnskj7jifq8y97x541jq187kvbllkbcrwvdzcq9c")))

(define-public crate-iif-1.2.0 (c (n "iif") (v "1.2.0") (h "0fc9nq0zf69lzk1mkafsh3qfzcy0an35vn8a71j00m04qy159lhy")))

(define-public crate-iif-1.2.1 (c (n "iif") (v "1.2.1") (h "0bl9f1x9ismf8jpmgdabqp08r6assj4bzb9xifkg929n91kymz58")))

