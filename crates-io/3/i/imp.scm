(define-module (crates-io #{3}# i imp) #:use-module (crates-io))

(define-public crate-imp-0.0.0 (c (n "imp") (v "0.0.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bigint") (r "^4.4.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1xk5wkhdn8vwgakpn8drxfx95v2gv19qz6sn12ms9ar0hvz4dgpa") (f (quote (("std") ("default" "std"))))))

(define-public crate-imp-0.0.1 (c (n "imp") (v "0.0.1") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bigint") (r "^4.4.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "1pk15y808qdwix32nx7yccv7x0fx922cvj6g7j3yydvsnmxa8msk") (f (quote (("std") ("default" "std"))))))

(define-public crate-imp-0.1.0 (c (n "imp") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.5") (d #t) (k 0)) (d (n "bigint") (r "^4.4.1") (d #t) (k 2)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "03nchvhsk5cyi04a0k576sqg4y4jmkyk1809jmrlqpbi26762pq1") (f (quote (("std") ("default" "std"))))))

