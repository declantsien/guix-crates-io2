(define-module (crates-io #{3}# i iai) #:use-module (crates-io))

(define-public crate-iai-0.1.0 (c (n "iai") (v "0.1.0") (h "0k4dzkisgqi0cnrmahsiirqpiv4jp59r372i9j0gcha8wskq6cgw") (f (quote (("real_blackbox") ("default"))))))

(define-public crate-iai-0.1.1 (c (n "iai") (v "0.1.1") (d (list (d (n "iai_macro") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0y6nhl8kany25njkkka4i5crlk4bf45mj1sdhfjql9a2gk4ida3i") (f (quote (("real_blackbox") ("macro" "iai_macro") ("default"))))))

