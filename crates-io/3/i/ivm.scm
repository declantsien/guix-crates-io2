(define-module (crates-io #{3}# i ivm) #:use-module (crates-io))

(define-public crate-ivm-0.1.0 (c (n "ivm") (v "0.1.0") (d (list (d (n "dircpy") (r "^0.3") (d #t) (k 0)) (d (n "dirs-next") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^1.5") (d #t) (k 0)))) (h "0973l7p42gqq5h1i1syryc761c2aqgqmbk8rir7yz0x0ja61ck8d")))

(define-public crate-ivm-0.1.1 (c (n "ivm") (v "0.1.1") (d (list (d (n "dircpy") (r "^0.3") (d #t) (k 0)) (d (n "dirs-next") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^1.5") (d #t) (k 0)))) (h "0wz6h1q13mcf4i4fmj29jh1bfgqpyj069g7ccc27d3iw1bv47xi9")))

(define-public crate-ivm-0.1.2 (c (n "ivm") (v "0.1.2") (d (list (d (n "dircpy") (r "^0.3") (d #t) (k 0)) (d (n "dirs-next") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^1.5") (d #t) (k 0)))) (h "0gphq9xmjgcichvzs95w5gk1qpl2hxw2752mjwmpqbwmvsnniw4c")))

(define-public crate-ivm-0.1.3 (c (n "ivm") (v "0.1.3") (d (list (d (n "dircpy") (r "^0.3") (d #t) (k 0)) (d (n "dirs-next") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^1.5") (d #t) (k 0)))) (h "0946gm513gx8snskprbqk90w2dgyxgs3ljlqghsmnw21p0nyj1a1")))

(define-public crate-ivm-0.2.0 (c (n "ivm") (v "0.2.0") (d (list (d (n "dircpy") (r "^0.3") (d #t) (k 0)) (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (d #t) (k 0)))) (h "03fcvf77bivn390qkml1bxcc5aw3n364vpnigwwq7fkbr3d6bczi")))

(define-public crate-ivm-0.3.0 (c (n "ivm") (v "0.3.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (d #t) (k 0)))) (h "1x9rb4xxgxljyfc4hlvq7vvb159bh2mxcwwznkx78shhxgr8i2sn")))

(define-public crate-ivm-0.4.0 (c (n "ivm") (v "0.4.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (d #t) (k 0)))) (h "0jlp8hh6x48jizs5lc3w3qabmbqdni3hhr9khfb40x1578siz3ms")))

(define-public crate-ivm-0.4.1 (c (n "ivm") (v "0.4.1") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (d #t) (k 0)))) (h "0dmjy07f1a48lra33i3q6jqfqsir8j3mpj8hsvqliv12m19fym1r")))

(define-public crate-ivm-0.5.0 (c (n "ivm") (v "0.5.0") (d (list (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "ureq") (r "^2.5") (d #t) (k 0)))) (h "0ry4741ii156w95rabnbcakzzk4z37w4h0mdnp2n0sccpdnvm3v8")))

