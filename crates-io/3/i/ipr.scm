(define-module (crates-io #{3}# i ipr) #:use-module (crates-io))

(define-public crate-ipr-0.1.0 (c (n "ipr") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1vfdw02d066q8z05ragdmxrg6zfwq0p5lh1nhz1bj1z2a83dy8br")))

(define-public crate-ipr-0.1.1 (c (n "ipr") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)))) (h "1dr2qvpyx3hfqmv9nw5f2xj9hpw674vdwz4fqy3rlsxfxvfjzxra")))

(define-public crate-ipr-0.1.2 (c (n "ipr") (v "0.1.2") (h "0r4jcwy4xlk2jd8nbz7nx67zh8g34gaslwi7g6a8sy2sq11906bd")))

