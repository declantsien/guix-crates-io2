(define-module (crates-io #{3}# i int) #:use-module (crates-io))

(define-public crate-int-0.1.0 (c (n "int") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "00pfjx8ylpvmdk9aa3vsysf23p7fvr17zbs070fhynjmlhv4amzm")))

(define-public crate-int-0.2.0 (c (n "int") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "06xskryfvfg2sy1pq67i7l5add2nd1lga40wj26b4pkx7z8njc51")))

(define-public crate-int-0.2.1 (c (n "int") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1vmmjq3x8v7n9376w4fkp2v52day8968nprcjyckik41c4q5xqq7")))

(define-public crate-int-0.2.2 (c (n "int") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0k778z62jn9kppr593bvhmn9vm1v0jqdl5q0ablbabr62w1bgbk9")))

(define-public crate-int-0.2.3 (c (n "int") (v "0.2.3") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0sxvsrvkknd1x8qjwka6gdy33md6hzmcw2jdh6zi75ll35dnvjz8")))

(define-public crate-int-0.2.4 (c (n "int") (v "0.2.4") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1fhbx954r3hfr2qpdx0094rpmrzqszc165mr84gfb4n6lspk2f7s")))

(define-public crate-int-0.2.5 (c (n "int") (v "0.2.5") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0lqmpv2bxfdga354qq0yqqr5rgcc2lp8avqsbvy7axy254ksd3qb")))

(define-public crate-int-0.2.6 (c (n "int") (v "0.2.6") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "09r9dipqdn7dciwxiidq3qik33mgq9nyc3h5k14szv8kzhz95ziw")))

(define-public crate-int-0.2.7 (c (n "int") (v "0.2.7") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0rnk3rxcjhwbcb68npaaasqk8r075nymidzfsgrphqqynj7rljj6")))

(define-public crate-int-0.2.8 (c (n "int") (v "0.2.8") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1ww58gbsybqi4g8w3i66h43zj99bwq2bdwakr0r6cr6q3msi89yx")))

(define-public crate-int-0.2.9 (c (n "int") (v "0.2.9") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "11b4wahf3fssi3xsydrw4fsq09k0rcn8kvcpkyyv39dxbcs1xh2a")))

(define-public crate-int-0.2.10 (c (n "int") (v "0.2.10") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "09257gzl0gq4p883xfzhwbcrwbc1hxni4my63qxf381a1vpy2anj")))

(define-public crate-int-0.2.11 (c (n "int") (v "0.2.11") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "119akb3v5bx5xl57xvwixwhm3a3z4gdpmlx35pgw5ad83s2415vi")))

(define-public crate-int-0.3.0 (c (n "int") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "16w9q0crbl8x0k3fcqky0qxqlh8f1hymzn2djfl9yw7wqwsvnr4x")))

