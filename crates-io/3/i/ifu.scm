(define-module (crates-io #{3}# i ifu) #:use-module (crates-io))

(define-public crate-ifu-0.1.0 (c (n "ifu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "eyepiece") (r "^0.8.0") (d #t) (k 0)) (d (n "skyangle") (r "^0.2.2") (d #t) (k 0)))) (h "0qzgj64037i16ibvwj7vh4fzxfnaq3ski42348drgdcf1klf6isd")))

(define-public crate-ifu-0.1.1 (c (n "ifu") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "eyepiece") (r "^0.8.0") (d #t) (k 0)) (d (n "skyangle") (r "^0.2.2") (d #t) (k 0)))) (h "1294sapafxyc8d6a5a1ayzkdn1mb3mnysl56hwykg4rx8j5bkbkb")))

(define-public crate-ifu-0.2.0 (c (n "ifu") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "eyepiece") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "skyangle") (r "^0.2.2") (d #t) (k 0)))) (h "0z1p3w47jf0mm1fp0wvma0nzjfmm48xn4n9731gl92hwlax0nyvl")))

(define-public crate-ifu-0.2.1 (c (n "ifu") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "eyepiece") (r "^0.8.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "skyangle") (r "^0.2.2") (d #t) (k 0)))) (h "0rnqq4vh3dm70wkw84s9srp778ymnfsp284lb3ff97wzmlaq5hra") (y #t)))

(define-public crate-ifu-0.2.2 (c (n "ifu") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "eyepiece") (r "^0.8.1") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "skyangle") (r "^0.2.2") (d #t) (k 0)))) (h "0y7mvn3ns2kkz7ipp4w9kyj3afxsa0pijg2b4wdkmlklwh4fpdvm")))

