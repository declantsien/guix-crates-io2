(define-module (crates-io #{3}# i ion) #:use-module (crates-io))

(define-public crate-ion-0.1.0 (c (n "ion") (v "0.1.0") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1dmpbnl7vf6m7ca6lv7dkgvqlrcpknsrpxvywb8hcxf3yrdvixv3")))

(define-public crate-ion-0.1.1 (c (n "ion") (v "0.1.1") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0kb47pwx6n7gfkjp8avzqjvfjm907b2fr2r9wjrghiz5dl71n6y1")))

(define-public crate-ion-0.1.2 (c (n "ion") (v "0.1.2") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1fa7cx5rwy05p73i2inrvb3sjyc26zd4b2f3b5d10pindhw59x13")))

(define-public crate-ion-0.1.3 (c (n "ion") (v "0.1.3") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0dgxik8akzimvs0hq1gnyw9dd1q74rq9slaxb3k0djmhy3l0y4gm")))

(define-public crate-ion-0.1.4 (c (n "ion") (v "0.1.4") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "0cw95ivlpxghggqnz4w9daknnns32d2n66876yyzl487alnx8bxd")))

(define-public crate-ion-0.2.0 (c (n "ion") (v "0.2.0") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1z9q8gfscdsnvpfdfklhkb7xql4vhz6n510vka98nj1f688028sm")))

(define-public crate-ion-0.3.0 (c (n "ion") (v "0.3.0") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "15s5npp6l9aaqwd5mbzfiyfxdmf6r61897s96q0qz38rk2lghghw")))

(define-public crate-ion-0.4.0 (c (n "ion") (v "0.4.0") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13cqgwg87d56zjrdy77kicnw71pw3x06190ppldsl8l2g32fvy04")))

(define-public crate-ion-0.5.0 (c (n "ion") (v "0.5.0") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0y5jkl5fb92368djky37xl4hq64h5sm199cp8drywy2p5glz7nx1") (f (quote (("default" "rustc-serialize"))))))

(define-public crate-ion-0.6.0 (c (n "ion") (v "0.6.0") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1wz3j4svgfjlgsscybcizvvl30vkn9zhyiyaz600mmp5qqxf48wn") (f (quote (("default" "rustc-serialize"))))))

(define-public crate-ion-0.7.0 (c (n "ion") (v "0.7.0") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1ma29wgp86hrqsy1f8vv2ccmr6q4qrd2ph97q4kpwllf24pkbm3h") (f (quote (("default" "rustc-serialize"))))))

(define-public crate-ion-0.7.1 (c (n "ion") (v "0.7.1") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)))) (h "13gry20wcjf3r836iy1rgbw8hdafs04wwxjmwcyln4vark3w2fh8") (f (quote (("default" "rustc-serialize"))))))

(define-public crate-ion-0.7.2 (c (n "ion") (v "0.7.2") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0zmwbkabl6ip8qgqs2l047b4dhjg51x8xxy7mwjmc2n4ayrprqmb") (f (quote (("default" "rustc-serialize"))))))

(define-public crate-ion-0.7.3 (c (n "ion") (v "0.7.3") (d (list (d (n "docopt") (r "^0.6.73") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0aimylnvna527qjchr2671mdv8ar70vcxxzp5i2l5qfj2cz5p416") (f (quote (("default" "rustc-serialize"))))))

(define-public crate-ion-0.8.0 (c (n "ion") (v "0.8.0") (h "066fr3vxpr5vcjpqk028817w2yha03v3pmcsngwbqf78qv5jjxr9")))

(define-public crate-ion-0.8.1 (c (n "ion") (v "0.8.1") (h "1ivrwpwlkbkcclpw6vl5v68bnhb9m62wgs3iph54qfgqyllf8f18")))

(define-public crate-ion-0.8.2 (c (n "ion") (v "0.8.2") (h "14yrnx69lxky38shvrafnp3f9k9bxqadm75ziv2cq97wgl53bqrw")))

(define-public crate-ion-0.8.3 (c (n "ion") (v "0.8.3") (h "0pcy0c9733mfmic8wnbv7x6wpin0hvkhljchfvavzfh07y1l03n8")))

(define-public crate-ion-0.8.4 (c (n "ion") (v "0.8.4") (h "068pmafi0f5ga1dr5242crrdlzqznpkw9xpwhjwd77awm1560hf4")))

(define-public crate-ion-0.8.5 (c (n "ion") (v "0.8.5") (h "1rsgd5hx9hsq691a3kqq935qjhcd9hjwim59pbqb2hp99rwn6h9a")))

(define-public crate-ion-0.8.6 (c (n "ion") (v "0.8.6") (h "1klira44bxqvz6sh8rgvf9m91c5dbwwy3bc2shh9f9lqpgy1av4f")))

(define-public crate-ion-0.8.7 (c (n "ion") (v "0.8.7") (h "0jsa9cdlgrsljgd4cba78nyb7dsz8zx29140ra5a32h8cmidg5ch") (y #t)))

(define-public crate-ion-0.8.8 (c (n "ion") (v "0.8.8") (d (list (d (n "quickcheck") (r "^0.9.2") (k 2)) (d (n "quickcheck_macros") (r "^0.9.1") (k 2)) (d (n "regex") (r "^1.3.3") (f (quote ("std" "unicode"))) (k 2)))) (h "1mjfr834ciqwn8xb9ravi3x4bh1vnwn830ldnv7qhdzsbplgm9sf")))

(define-public crate-ion-0.9.0 (c (n "ion") (v "0.9.0") (d (list (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (k 2)) (d (n "regex") (r "^1.3.3") (f (quote ("std" "unicode"))) (k 2)))) (h "11rfnr0ripw959a8q9a42h236c63nh7l1fbgf5igbclrfp9pdim1")))

(define-public crate-ion-0.9.1 (c (n "ion") (v "0.9.1") (d (list (d (n "quickcheck") (r "^1.0.3") (k 2)) (d (n "quickcheck_macros") (r "^1.0.0") (k 2)) (d (n "regex") (r "^1.3.3") (f (quote ("std" "unicode"))) (k 2)))) (h "0c5k25g377hmw40n1qfqhfcmx6ribn0rck3ph7w95rihh2577dia")))

