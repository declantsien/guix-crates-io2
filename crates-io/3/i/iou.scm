(define-module (crates-io #{3}# i iou) #:use-module (crates-io))

(define-public crate-iou-0.1.0 (c (n "iou") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "uring-sys") (r "^1.0.0-alpha") (d #t) (k 0)))) (h "1b2x3ah4fhcbcxdk88h9ssm32v5w0cs7z500rdz6793wdgdai1m1")))

(define-public crate-iou-0.2.0 (c (n "iou") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "uring-sys") (r "^1.0.0-beta") (d #t) (k 0)))) (h "0s6qjr3liflx7d9i8nap1375jcq008j9h3s9al2rp0rx04c2bfyy")))

(define-public crate-iou-0.0.0-ringbahn (c (n "iou") (v "0.0.0-ringbahn") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.16.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)) (d (n "uring-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0m2kris7cwdimba5nghvv89snzl6b42myhplysxl086ym3dbnkh2")))

(define-public crate-iou-0.0.0-ringbahn.1 (c (n "iou") (v "0.0.0-ringbahn.1") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.16.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)) (d (n "uring-sys") (r "^0.6.1") (d #t) (k 0)))) (h "0mqv92gajb54m01z7bd06g46sml7v5ifvp5z3xf6pq3awb3r5g75")))

(define-public crate-iou-0.3.0 (c (n "iou") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)) (d (n "uring-sys") (r "^0.7.4") (d #t) (k 0)))) (h "14r65zlc5663rmj3ga2c5vqc3g1h0bdjnvhija0yyd3z2n2jpsjz") (y #t)))

(define-public crate-iou-0.3.1 (c (n "iou") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)) (d (n "uring-sys") (r "^0.7.4") (d #t) (k 0)))) (h "1f92539q0kjfds21i36vkcg42l0f58ygcs4k5xh7wdp1a6j6a2vz")))

(define-public crate-iou-0.3.2 (c (n "iou") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)) (d (n "uring-sys") (r "^0.7.4") (d #t) (k 0)))) (h "00lpl9gsj4zmfdr274jdj6syqk6lrb33wm4xfj40mbhbipj89v8w")))

(define-public crate-iou-0.3.3 (c (n "iou") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 2)) (d (n "uring-sys") (r "^0.7.4") (d #t) (k 0)))) (h "1p6n9yzrfpv5wzc6rpjjfvkzlqvrh5i60cb4xbwidsy7372hx0i2")))

