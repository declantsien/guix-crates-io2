(define-module (crates-io #{3}# i idk) #:use-module (crates-io))

(define-public crate-idk-0.0.1 (c (n "idk") (v "0.0.1") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "jsonwebtoken") (r "^8.2.0") (d #t) (k 0)) (d (n "jwt") (r "^0.16.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)))) (h "0p8gqaifv04bx2nmmdk84dpr0fr2jbjy4h7f2nm7a27di1h772l7") (f (quote (("default")))) (r "1.66.1")))

