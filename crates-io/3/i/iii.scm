(define-module (crates-io #{3}# i iii) #:use-module (crates-io))

(define-public crate-iii-0.1.0 (c (n "iii") (v "0.1.0") (h "1qgxh60fn3s0lnmjasz65ixjnws5lbm31v87gimalnrlhs9myxj2")))

(define-public crate-iii-0.1.1 (c (n "iii") (v "0.1.1") (h "0j9spy0jk3h39m6bv1aw57y2y7rm9a0x0m66nsl7hl7pr9l0zg4p")))

(define-public crate-iii-0.1.2 (c (n "iii") (v "0.1.2") (h "08mplnq5gpl89rfzg5zbivk0hmibjjgigvd5zk1y9k94frvxlwq0")))

(define-public crate-iii-0.1.3 (c (n "iii") (v "0.1.3") (h "0w4hbm04jhqn0ch55n3wii6vdrr16w9fbfy1pyfpvn8zna25kw80")))

(define-public crate-iii-0.1.4 (c (n "iii") (v "0.1.4") (h "0738lygyhsfm9rs8jh8km1mz9knnmv835crz2nhix1pi45m8l7gn")))

(define-public crate-iii-0.1.5 (c (n "iii") (v "0.1.5") (h "14q6yw7d19029zcpx53i1iar02djl4qi1kr901n9bsx2l2q0b8bx")))

(define-public crate-iii-1.0.0 (c (n "iii") (v "1.0.0") (h "1kc1hdxwliald38xw84199qmni3fk6vjhahzafm2rrzz12h4g5r0")))

(define-public crate-iii-1.0.1 (c (n "iii") (v "1.0.1") (h "1x3p89bkcil29bs327gx6a16f4lc6h50bwvwp04xg55mkfrvkghm")))

(define-public crate-iii-1.0.2 (c (n "iii") (v "1.0.2") (h "1r564yf7fk0d362b70130fgdlla5bvhgvs80igadn7zq6w5p5nah")))

(define-public crate-iii-1.0.3 (c (n "iii") (v "1.0.3") (h "1fz4lkvwnx07vjfbvyd0n3bqd8nmapgqsj1bcwhvqc3r5bmh273s")))

(define-public crate-iii-1.0.4 (c (n "iii") (v "1.0.4") (h "15wmznx690smp232z9isl97mnavcv467w28s78w4lgyyr6gbj8cw")))

