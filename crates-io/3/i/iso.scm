(define-module (crates-io #{3}# i iso) #:use-module (crates-io))

(define-public crate-iso-0.0.0 (c (n "iso") (v "0.0.0") (d (list (d (n "iso-macro") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "115715i3ycfsmsc9f866c8j3w8jgkhnb6lrdjl5c7bfi2jk2kmlk") (f (quote (("std") ("default" "std"))))))

(define-public crate-iso-0.0.1 (c (n "iso") (v "0.0.1") (d (list (d (n "iso-macro") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "1g953j79rb3rjn97f24872m0m8sdai2973jryjja54qqy6lg5lcm") (f (quote (("std") ("default" "std"))))))

(define-public crate-iso-0.0.2 (c (n "iso") (v "0.0.2") (d (list (d (n "iso-macro") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "150gfyrfk1pdzzg0i2gvifapirf1wfv82z15nlks2pn9r4j7azyi") (f (quote (("std") ("default" "std"))))))

(define-public crate-iso-0.0.3 (c (n "iso") (v "0.0.3") (d (list (d (n "iso-macro") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0gxbxslbzby4daqijfjafi6gic8gi3zqbnbrv43lwy9qapc2hwvb") (f (quote (("std") ("default" "std"))))))

(define-public crate-iso-0.0.4 (c (n "iso") (v "0.0.4") (d (list (d (n "iso-macro") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0d0nn6ygaaa0sqdyv7s22rag49ykq6h6mgkd9nbs6i7kv22z8a5d") (f (quote (("std") ("default" "std"))))))

(define-public crate-iso-0.0.5 (c (n "iso") (v "0.0.5") (d (list (d (n "iso-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0z66aka7lqciygfgw216lfailvd612x715g0ngklqsdsqaxfxy5s") (f (quote (("std") ("serde" "serde/std") ("default" "std"))))))

(define-public crate-iso-0.0.6 (c (n "iso") (v "0.0.6") (d (list (d (n "iso-macro") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "0w11syw3ib5m4004fbipjn6k94cfqwgpr0qirxr772aga6ia07jr") (f (quote (("std") ("serde-std" "serde/std") ("language") ("default" "std" "country" "language") ("country"))))))

(define-public crate-iso-0.0.7 (c (n "iso") (v "0.0.7") (d (list (d (n "iso-macro") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "19r04kkwgdfssm69afrflvly9p6czvnpvia5l0a1q82wpkv2v9kn") (f (quote (("std") ("serde-std" "serde/std") ("language") ("default" "std" "country" "language") ("country"))))))

(define-public crate-iso-0.0.8 (c (n "iso") (v "0.0.8") (d (list (d (n "iso-macro") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "07b75c08c31p2i0b0l24vy8fr0wfmmrwdiq4g5vqy88yxxxbnflb") (f (quote (("std") ("serde-std" "serde/std") ("language") ("default" "std" "country" "language") ("country"))))))

(define-public crate-iso-0.0.9 (c (n "iso") (v "0.0.9") (d (list (d (n "iso-macro") (r "^0.0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)))) (h "1bbcvs7cxv1fyvfqkh53s86b0jfgvrf3bi4x9rwnqm8pzn96nvwd") (f (quote (("std") ("serde-std" "serde/std") ("language") ("default" "std" "country" "language") ("country"))))))

