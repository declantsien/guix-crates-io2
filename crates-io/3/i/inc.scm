(define-module (crates-io #{3}# i inc) #:use-module (crates-io))

(define-public crate-inc-0.1.0 (c (n "inc") (v "0.1.0") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1ncvzsd0gz7lk3v0zizvxg5s97qjba04ykav9g97sx4s5c444l1r")))

(define-public crate-inc-0.0.1 (c (n "inc") (v "0.0.1") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1914408vayxwqpzrynld4vy4j8bjwlzihgd8sa3dsid0yp7br1va")))

(define-public crate-inc-0.1.1 (c (n "inc") (v "0.1.1") (d (list (d (n "nom") (r "^4.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 0)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1yygvi6z9cmslx6hj96rmasgz3z2f9phy7nfdl4vnn698cpkagzy")))

(define-public crate-inc-0.1.2 (c (n "inc") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0lp41v21ssid896jvv7z23m4f9p09jvrxy9cvf1xchc7kcjpgc8z")))

(define-public crate-inc-0.1.3 (c (n "inc") (v "0.1.3") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "nom") (r "^5.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "02jzlgcf8yiw6agzpgzh27vfd5ysr03i5njsxrr01cryfxk68cq8")))

