(define-module (crates-io #{3}# i ige) #:use-module (crates-io))

(define-public crate-ige-0.0.0 (c (n "ige") (v "0.0.0") (h "1wib66svhnm6gi6fdra93vmdrzgcnd25ji5gbiypy47cs613dmxj")))

(define-public crate-ige-0.1.0 (c (n "ige") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "13m2yqsyaky7lidklki1ryd6wq2s8iz7gdk9zm9hgnarz20qbhph") (f (quote (("zeroize" "cipher/zeroize") ("default" "block-padding") ("block-padding" "cipher/block-padding")))) (r "1.56")))

(define-public crate-ige-0.1.1 (c (n "ige") (v "0.1.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "0nscsf2yp3gg56xbc4lldr08i7kskd8g2khabkz31smx0xyhq4v9") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("default" "block-padding") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

