(define-module (crates-io #{3}# i iwp) #:use-module (crates-io))

(define-public crate-iwp-0.1.0 (c (n "iwp") (v "0.1.0") (d (list (d (n "poloto") (r "^1.8.3") (d #t) (k 0)))) (h "0l98cg0i2nk32kydxzjjvzalzywv8r2jhd40zh7mrdi0rd33cry4")))

(define-public crate-iwp-0.1.1 (c (n "iwp") (v "0.1.1") (d (list (d (n "poloto") (r "^1.8.3") (d #t) (k 0)))) (h "0fbkvxpnpalvvrks18ixicd3z2m7aidcm22kdfihyp62rda00r9x")))

(define-public crate-iwp-0.1.2 (c (n "iwp") (v "0.1.2") (d (list (d (n "poloto") (r "^1.8.3") (d #t) (k 0)))) (h "0d9dlygg74xas64qbc9zjcswx4qynlddhl46i2x009a87jp5wks3")))

