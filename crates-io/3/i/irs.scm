(define-module (crates-io #{3}# i irs) #:use-module (crates-io))

(define-public crate-irs-0.1.0 (c (n "irs") (v "0.1.0") (d (list (d (n "cursive") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "unix_socket") (r "^0.4") (d #t) (k 0)))) (h "07h2smi9zkwgs6g229awdgz01dm8aznkz75dya9h2drbdzzdrcvh") (y #t)))

