(define-module (crates-io #{3}# i i2p) #:use-module (crates-io))

(define-public crate-i2p-0.0.1 (c (n "i2p") (v "0.0.1") (d (list (d (n "log") (r "^0.3.7") (d #t) (k 0)) (d (n "nom") (r "^2.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "134b0sm9qgphr84am85d41z7rcgiw4bw3rh7vm9g1yrwr777vfi3")))

