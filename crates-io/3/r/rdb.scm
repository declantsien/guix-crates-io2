(define-module (crates-io #{3}# r rdb) #:use-module (crates-io))

(define-public crate-rdb-0.1.0 (c (n "rdb") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "lzf") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "01g73swx5d9zax5zsiy0l6dvs5rxa92rqwx1f14agj2mh6jyvkic")))

(define-public crate-rdb-0.2.0 (c (n "rdb") (v "0.2.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "lzf") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0yzrlv4nhyvxshsdizf8n8i3xxsyiylndskcachb6z4ijvl6xyyp")))

(define-public crate-rdb-0.2.1 (c (n "rdb") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "lzf") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0m3g25bwv29y4y9b9qiwf1qmmq0wbm2qrd6jkaq5k782d2lrw37w")))

