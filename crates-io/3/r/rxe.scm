(define-module (crates-io #{3}# r rxe) #:use-module (crates-io))

(define-public crate-rxe-0.1.0 (c (n "rxe") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.24") (d #t) (k 0)))) (h "0ija9q6pad2gb7hsykmsfs3v19vv49h9adyw91vwa8hlrqgpni1l")))

