(define-module (crates-io #{3}# r ree) #:use-module (crates-io))

(define-public crate-ree-0.0.1 (c (n "ree") (v "0.0.1") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)))) (h "16rrscwlpsi7zyy3rcyd0s91agmipqlxlp1pn09r0l2d6hra62hv")))

(define-public crate-ree-0.0.2 (c (n "ree") (v "0.0.2") (d (list (d (n "colored") (r "^1.7") (d #t) (k 0)))) (h "05dm70xhmjm4865jqirjk7rw98w6aqz1qi5jq4i16068basrlw1w")))

