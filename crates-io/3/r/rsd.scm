(define-module (crates-io #{3}# r rsd) #:use-module (crates-io))

(define-public crate-rsd-0.1.0 (c (n "rsd") (v "0.1.0") (d (list (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "cryptoxide") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "sp800-185") (r "^0.2.0") (d #t) (k 0)))) (h "138sfrk4d09qlgq9fi4v0gbyqmbsfvk7axf3ld9q1i9l77jv4rd6")))

