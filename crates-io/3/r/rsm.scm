(define-module (crates-io #{3}# r rsm) #:use-module (crates-io))

(define-public crate-rsm-1.0.0 (c (n "rsm") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.5") (d #t) (k 0)))) (h "1z6p3a60zm9am4jp8bd0v80dgq0hwl3xkvcyl8cnfmdgv5nw83sl") (y #t)))

