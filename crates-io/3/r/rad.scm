(define-module (crates-io #{3}# r rad) #:use-module (crates-io))

(define-public crate-rad-0.1.0 (c (n "rad") (v "0.1.0") (d (list (d (n "ceph-rust") (r "^0.1.16") (d #t) (k 0)) (d (n "chrono") (r "^0.3.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "0sv49546pkgnfdgs5h608ncq941k7lk15pxw3qlgkipld2ywvkyb") (f (quote (("integration-tests"))))))

(define-public crate-rad-0.1.1 (c (n "rad") (v "0.1.1") (d (list (d (n "ceph-rust") (r "^0.1.16") (d #t) (k 0)) (d (n "chrono") (r "^0.3.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "148fsqnaykc5kp6rm911m760n98ryr7qp6ghp6vm48nqscxnbn2p") (f (quote (("integration-tests"))))))

(define-public crate-rad-0.2.0 (c (n "rad") (v "0.2.0") (d (list (d (n "ceph-rust") (r "^0.1.16") (d #t) (k 0)) (d (n "chrono") (r "^0.3.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1mq44h32dv2ih901nkz1f7jcwg42n33j69ksq0aaqd6krn524rzp") (f (quote (("integration-tests"))))))

(define-public crate-rad-0.2.1 (c (n "rad") (v "0.2.1") (d (list (d (n "ceph-rust") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1fsvxf55b39wa2xss9sf3vcxk17qmz4q8jynd1vkd0b6mzyv50gl") (f (quote (("integration-tests"))))))

(define-public crate-rad-0.3.0 (c (n "rad") (v "0.3.0") (d (list (d (n "ceph-rust") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1z5hhpjngprcb2d7cgz40vxk7bfjbgb8sp4pnlcjgvxmlk1v6181") (f (quote (("integration-tests"))))))

(define-public crate-rad-0.3.1 (c (n "rad") (v "0.3.1") (d (list (d (n "ceph-rust") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "0awqwpnd94rx17dkd8vsq14965hzz6jcy2yfhvb76acby5kq1jdv") (f (quote (("integration-tests"))))))

(define-public crate-rad-0.3.2 (c (n "rad") (v "0.3.2") (d (list (d (n "ceph-rust") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0-rc.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "0jxff36kd21d474385w69bhgxhdbkxk0189l57ff1py459m0zz2c") (f (quote (("integration-tests")))) (y #t)))

(define-public crate-rad-0.3.3 (c (n "rad") (v "0.3.3") (d (list (d (n "ceph-rust") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1a9c8h3rmll6hxmdxgwqm0xa508jc171xjqbd2ah1a8q5dxhplld") (f (quote (("integration-tests"))))))

(define-public crate-rad-0.4.0 (c (n "rad") (v "0.4.0") (d (list (d (n "ceph-rust") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "ffi-pool") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "00s3ngs668n7i7akmwlg0ymjizdpqvm4f3q0p22cmxjny408x48c") (f (quote (("integration-tests"))))))

(define-public crate-rad-0.5.0 (c (n "rad") (v "0.5.0") (d (list (d (n "ceph-rust") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "ffi-pool") (r "^0.1.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)) (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "152g4ljrwg223clxcwfl2lzh3763hjwbnbi2r2dd5zx9jc1y9328") (f (quote (("integration-tests"))))))

