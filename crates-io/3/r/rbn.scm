(define-module (crates-io #{3}# r rbn) #:use-module (crates-io))

(define-public crate-rbn-0.1.0 (c (n "rbn") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0c4bqlcphm7ww6bh3la6ybmi2k856fkz6f883w5xm8inr7l2dxhc")))

(define-public crate-rbn-0.1.1 (c (n "rbn") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1izjrsz2ycqf601wzbxkhaf42yraqlscl829d4nhhxb2jmmg4hh1")))

(define-public crate-rbn-0.1.2 (c (n "rbn") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "hambands") (r "^0.1.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "0ac05q0mcjlv3zgqb8d8bx6433av4lr0rzqrdnbhmlbgs0rb6ivp")))

(define-public crate-rbn-0.1.3 (c (n "rbn") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "hambands") (r "^0.1.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "rbn-lib") (r "^0.1.3") (d #t) (k 0)))) (h "0blvbnrj9923i8n35aw9ha6z4qcl79v6rkh0q3fi2i7w1jml8hkk")))

(define-public crate-rbn-0.1.4 (c (n "rbn") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "hambands") (r "^0.1.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)) (d (n "rbn-lib") (r "^0.1.3") (d #t) (k 0)))) (h "1gqy5qrczhm0bi9zai2ny39qaz2bg95i5z10qadfb35a681gn013")))

