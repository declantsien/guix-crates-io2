(define-module (crates-io #{3}# r rlg) #:use-module (crates-io))

(define-public crate-rlg-0.0.1 (c (n "rlg") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "dtt") (r "^0.0.1") (d #t) (k 0)))) (h "0fsfdsc1pp6p1zikfv07ypg6jyzch8345w81xgs48a0350n6hms3") (f (quote (("default")))) (r "1.66.1")))

(define-public crate-rlg-0.0.2 (c (n "rlg") (v "0.0.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dtt") (r "^0.0.5") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vrd") (r "^0.0.4") (d #t) (k 0)))) (h "1p1lj6850b0vqyymy7jglqvx082666ix3zjj3b1xa4434dqgdk66") (f (quote (("default")))) (r "1.66.1")))

(define-public crate-rlg-0.0.3 (c (n "rlg") (v "0.0.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dtt") (r "^0.0.5") (d #t) (k 0)) (d (n "hostname") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "vrd") (r "^0.0.5") (d #t) (k 0)))) (h "0s63kzq04725sbk95m9qk4jxcd1myqfb1b5468qb4rrzw8bwf0ly") (f (quote (("default")))) (r "1.71.1")))

(define-public crate-rlg-0.0.4 (c (n "rlg") (v "0.0.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dtt") (r "^0.0.6") (d #t) (k 0)) (d (n "hostname") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)) (d (n "vrd") (r "^0.0.7") (d #t) (k 0)))) (h "0wd9qi30jr2wsjil0xqg77dlavnn4n48w5k5n40qdb2hrzgm15gs") (f (quote (("default")))) (r "1.60")))

