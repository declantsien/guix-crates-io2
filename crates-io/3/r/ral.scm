(define-module (crates-io #{3}# r ral) #:use-module (crates-io))

(define-public crate-ral-0.1.1 (c (n "ral") (v "0.1.1") (d (list (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "1k7n9srs3g4chqdz1q0y7wwfy8d3xpy6an1aii49z618p1w9566r")))

(define-public crate-ral-0.2.0 (c (n "ral") (v "0.2.0") (d (list (d (n "ral-macro") (r "^0.2") (d #t) (k 0)) (d (n "vcell") (r "^0.1") (d #t) (k 0)))) (h "0r5bpp44yf82jyahm8wh9rppjyg353sdskrf9413blav8995cyyd")))

