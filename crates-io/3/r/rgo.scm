(define-module (crates-io #{3}# r rgo) #:use-module (crates-io))

(define-public crate-rgo-0.1.0 (c (n "rgo") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.63") (o #t) (d #t) (k 0)) (d (n "convenience") (r "^0.1.0") (d #t) (k 0)) (d (n "convenience") (r "^0.1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "13qsc4hkzd546lyj5pkji4qfjpljqqq2li448w0xm2jc1652pq4q") (f (quote (("default"))))))

