(define-module (crates-io #{3}# r rgl) #:use-module (crates-io))

(define-public crate-rgl-0.1.0 (c (n "rgl") (v "0.1.0") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "1mnj5mclqgig05g59a4y8822jwahnf15idv1098s0xks2p1a9l03")))

(define-public crate-rgl-0.1.1 (c (n "rgl") (v "0.1.1") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "0b4j4742gzr98aqx3kq6nh8d6yk5bnyrz0fyq4gka90bl3zh6dj9")))

(define-public crate-rgl-0.1.2 (c (n "rgl") (v "0.1.2") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "02j5c1cr4dm4r33ai5ag5l38fni9qcyjxbmx79xmxvp2lnd5ih89")))

(define-public crate-rgl-0.2.0 (c (n "rgl") (v "0.2.0") (d (list (d (n "gl") (r "^0.6.0") (d #t) (k 0)))) (h "185iwbssm1xzzyxyd7fcd490rimrxbkvx0dkakpqcf0w4h7mm408")))

