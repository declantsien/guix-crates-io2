(define-module (crates-io #{3}# r rrr) #:use-module (crates-io))

(define-public crate-rrr-0.0.0 (c (n "rrr") (v "0.0.0") (d (list (d (n "num-rational") (r "^0.3.2") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1.0.119") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g14c5pzhidi7060v6409qpz152fdvi429sm8c42ifhp9whnyvy7") (f (quote (("serde1" "serde" "num-rational/serde") ("default" "serde1"))))))

