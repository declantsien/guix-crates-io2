(define-module (crates-io #{3}# r rsn) #:use-module (crates-io))

(define-public crate-rsn-0.0.0-reserve.0 (c (n "rsn") (v "0.0.0-reserve.0") (h "087sp2ycq8ynfqcg3iw7f4cxqw7j2qpdx896biir6yfm0bsmm5cb")))

(define-public crate-rsn-0.1.0 (c (n "rsn") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.159") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11.9") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0.8") (d #t) (k 0)))) (h "04llmqwbnynwz47lpry7rgr90qykvw52l59zn671l9vqa2w9agk7") (f (quote (("std" "serde/std") ("nightly") ("integer128") ("default" "serde" "std")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.65")))

