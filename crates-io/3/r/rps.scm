(define-module (crates-io #{3}# r rps) #:use-module (crates-io))

(define-public crate-rps-0.1.0 (c (n "rps") (v "0.1.0") (h "1zapbg6h99a2rcynfv9a1fkskm3hdyj50zkllw68kq7v4qg1m2bp")))

(define-public crate-rps-0.2.0 (c (n "rps") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "rps-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xg9vzdlmfi69wih331ip01vcp6jbgcc8v2sjzv6wnz6wsnx3n7m")))

(define-public crate-rps-0.3.0 (c (n "rps") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "rps-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ldy1sv4wgkfr7iwzpl9pc2yb3zh73fm6dfwqlf5icq5s21f3syx")))

