(define-module (crates-io #{3}# r rpa) #:use-module (crates-io))

(define-public crate-rpa-0.1.0 (c (n "rpa") (v "0.1.0") (d (list (d (n "rpa_derives") (r "^0.1.0") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1s1pliwqv3jk4rbikayw22wyj5b6k61imghzgv4435sihbs9374i")))

(define-public crate-rpa-0.2.0 (c (n "rpa") (v "0.2.0") (d (list (d (n "rpa_derives") (r "^0.2.0") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.2.0") (d #t) (k 0)))) (h "0ndkpq075qgn57qw0993rg2rapzyn65izdid5zmkbm3f5v5kqk8q")))

(define-public crate-rpa-0.3.0 (c (n "rpa") (v "0.3.0") (d (list (d (n "rpa_derives") (r "^0.3.0") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.3.0") (d #t) (k 0)))) (h "1q7z3iiz7kjmxvfw91m7bzciag73d491af6xdyj71r7w6ab778df")))

(define-public crate-rpa-0.3.1 (c (n "rpa") (v "0.3.1") (d (list (d (n "rpa_derives") (r "^0.3.1") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.3.1") (d #t) (k 0)))) (h "00v793v890yjk3k22c0i9xmzhk73dlk2knjblp2niz2qwj99z18x")))

(define-public crate-rpa-0.3.2 (c (n "rpa") (v "0.3.2") (d (list (d (n "rpa_derives") (r "^0.3.2") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.3.2") (d #t) (k 0)))) (h "1wsf7lh3731zi8kzcj54mipc6fhmy11mjlfhm76af55w9zss149r")))

(define-public crate-rpa-0.4.0 (c (n "rpa") (v "0.4.0") (d (list (d (n "rpa_derives") (r "^0.4.0") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.4.0") (d #t) (k 0)))) (h "08krg0j9caqhx5ypxf86q05ga3lpdip4n69f9fc4ln81yb7gl0hp")))

(define-public crate-rpa-0.4.1 (c (n "rpa") (v "0.4.1") (d (list (d (n "rpa_derives") (r "^0.4.1") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.4.1") (d #t) (k 0)))) (h "1q9k1cifrb888i06ha8c69rayz62rnjc4k9s4a55zd967vp9kz7l")))

(define-public crate-rpa-0.4.2 (c (n "rpa") (v "0.4.2") (d (list (d (n "rpa_derives") (r "^0.4.2") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.4.2") (d #t) (k 0)))) (h "0ky34qdyzxdk176w3p512b15ph05ayg80q4wnxnhgarpz6r72nl3")))

(define-public crate-rpa-0.4.3 (c (n "rpa") (v "0.4.3") (d (list (d (n "rpa_derives") (r "^0.4.3") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.4.3") (d #t) (k 0)))) (h "1msyl140bsnzjw3xbx9q0gi5ymglm5zy3c5d5mvk88fz4ab1alw0")))

(define-public crate-rpa-0.4.4 (c (n "rpa") (v "0.4.4") (d (list (d (n "rpa_derives") (r "^0.4.4") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.4.4") (d #t) (k 0)))) (h "1gpncv9nan63h1cqkp7sv9r2gj5bpn9zvqj2f96hainyph4kwpgy")))

(define-public crate-rpa-0.4.5 (c (n "rpa") (v "0.4.5") (d (list (d (n "rpa_derives") (r "^0.4.5") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.4.5") (d #t) (k 0)))) (h "1jsq84ap9xj3c8v2mp4zgry89b2ni1pd38jq1f1kkr9cmpyw9jb2")))

(define-public crate-rpa-0.4.6 (c (n "rpa") (v "0.4.6") (d (list (d (n "rpa_derives") (r "^0.4.5") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.4.5") (d #t) (k 0)))) (h "1293xr47vgapxkq66c0zwiwfzss7cgg24g6zm67928pi7813vlwq")))

(define-public crate-rpa-0.4.61 (c (n "rpa") (v "0.4.61") (d (list (d (n "rpa_derives") (r "^0.4.6") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.4.6") (d #t) (k 0)))) (h "1w84yci7gsdxy20jqlrns1r8pxf22pdb3ds5v68a0h7idhdz0hqv")))

(define-public crate-rpa-0.4.7 (c (n "rpa") (v "0.4.7") (d (list (d (n "rpa_derives") (r "^0.4.7") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.4.7") (d #t) (k 0)))) (h "1ixxymsmw449jdwzmspbh0v6xfsbicz8zlxydalnk9nvm71ayyfk")))

(define-public crate-rpa-0.4.8 (c (n "rpa") (v "0.4.8") (d (list (d (n "rpa_derives") (r "^0.4.8") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.4.8") (d #t) (k 0)))) (h "0fr3la3gpbkg5v0rkdqzz8z41xyfvmipb7cazsmvl8c8s2m6q4ij")))

(define-public crate-rpa-0.5.0 (c (n "rpa") (v "0.5.0") (d (list (d (n "rpa_derives") (r "^0.5.0") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.5.0") (d #t) (k 0)))) (h "12i79kzln4cllswsx5ia64i5nd6ha51m9sg32ylw8amib4md1hp3")))

(define-public crate-rpa-0.5.1 (c (n "rpa") (v "0.5.1") (d (list (d (n "rpa_derives") (r "^0.5.1") (d #t) (k 0)) (d (n "rpa_macros") (r "^0.5.1") (d #t) (k 0)))) (h "0zzdipmsg09qwg5b80lvmwsi14d61g9pag8ddfamfrnwmi4d06p2")))

