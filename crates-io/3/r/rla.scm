(define-module (crates-io #{3}# r rla) #:use-module (crates-io))

(define-public crate-rla-0.1.0 (c (n "rla") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1.3.1") (d #t) (k 0)))) (h "0dqbsz5c1vfpiqpczpmriry1nk3mj2qqa3hss16ivr48klp99wgq")))

(define-public crate-rla-0.1.1 (c (n "rla") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.3.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "typenum") (r "^1.3.1") (d #t) (k 0)))) (h "1nfm677c6qx2ngyxc61kwzkgl1b0r9rqnv8kadh0jz9xiblfc1xn")))

