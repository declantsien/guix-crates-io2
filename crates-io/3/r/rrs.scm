(define-module (crates-io #{3}# r rrs) #:use-module (crates-io))

(define-public crate-rrs-0.0.6 (c (n "rrs") (v "0.0.6") (h "08grdx78837cpr8fqbrjkqwj618wfi9drlbsirhsijp46hqs1nj0") (r "1.58")))

(define-public crate-rrs-0.0.7 (c (n "rrs") (v "0.0.7") (d (list (d (n "temp-file") (r "^0.1.6") (d #t) (k 0)))) (h "0gg9jg6qrvdf5879s1kp1kk18r17ivfwpayr4iqplcc1vy70sv6z") (r "1.58")))

