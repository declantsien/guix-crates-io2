(define-module (crates-io #{3}# r ryn) #:use-module (crates-io))

(define-public crate-ryn-0.1.7 (c (n "ryn") (v "0.1.7") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "doe") (r "^0.3.14") (f (quote ("clip"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.6") (d #t) (k 0)) (d (n "rustls") (r "^0.21.8") (d #t) (k 0)))) (h "03ymxdvs78iy64g66kfll40535w923khs093jbkird4mli00kvny")))

(define-public crate-ryn-0.1.9 (c (n "ryn") (v "0.1.9") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "doe") (r "^0.3.14") (f (quote ("clip"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.6") (d #t) (k 0)) (d (n "rustls") (r "^0.21.8") (d #t) (k 0)))) (h "1ydlpq7nna12ww656p2jjmsg5d4icyw370n7mm8abxwb6253wvfj")))

(define-public crate-ryn-1.0.0 (c (n "ryn") (v "1.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "doe") (r "^0.3.14") (f (quote ("clip"))) (d #t) (k 0)) (d (n "getrandom") (r "^0.2.10") (d #t) (k 0)) (d (n "local-ip-address") (r "^0.5.6") (d #t) (k 0)) (d (n "rustls") (r "^0.21.8") (d #t) (k 0)))) (h "06a6grvg4z2ccsqc0yzbc9y0zllv1zclyiyxhs0q4y6g9s041l1n")))

