(define-module (crates-io #{3}# r rgc) #:use-module (crates-io))

(define-public crate-rgc-0.3.3 (c (n "rgc") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "simple_input") (r "^0.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "1drkf3pm3zacm1srxg195zxr11j3d4wrqbk70fp8nly65xgd5kjx")))

(define-public crate-rgc-0.3.4 (c (n "rgc") (v "0.3.4") (d (list (d (n "bunt") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "simple_input") (r "^0.4.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)))) (h "19bm8746msaw84n3gg726jz859czvg31767znkzxjakfzc89zckn")))

