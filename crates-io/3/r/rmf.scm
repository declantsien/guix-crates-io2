(define-module (crates-io #{3}# r rmf) #:use-module (crates-io))

(define-public crate-rmf-0.1.0 (c (n "rmf") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0gbm4wy9nvyn7wdkph4ydgdjm4apfidwrh671fd0109dmvwdp295")))

(define-public crate-rmf-0.1.1 (c (n "rmf") (v "0.1.1") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0hqc06l1b7p4dhcrf0aw0sdf1xdb1qa7xxk9yjsdbxcmzh7cq0v0")))

(define-public crate-rmf-0.1.2 (c (n "rmf") (v "0.1.2") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1mgn7bdva1g3xjhzndsbv49iacbmf1g1dnbw0ll6chkl4pg463z9")))

(define-public crate-rmf-0.1.3 (c (n "rmf") (v "0.1.3") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0jngc3zpx0zb6fj73fjwyiw085vr1gvfwl33c85d9m1v0r1ing4h")))

(define-public crate-rmf-0.1.4 (c (n "rmf") (v "0.1.4") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1f2p7ip55cyyfah60zahnp942rixq21ax2rbwq9lxsg1rqg36f8y")))

(define-public crate-rmf-0.1.5 (c (n "rmf") (v "0.1.5") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0xlyrmgm4vni0vbqq0xw4ja4p1xc4ycs86p2bix1vpn12qxhgkd6")))

(define-public crate-rmf-0.1.6 (c (n "rmf") (v "0.1.6") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "13jh23a71dgv0l60xk4bhm5955pg5a1c6s5hn7mdyz673mb08ns0")))

(define-public crate-rmf-0.1.7 (c (n "rmf") (v "0.1.7") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0l4gnn3k60226ciqdq7q4xlglmbhl3fmi8b400w0p7mfh3rzypyv")))

(define-public crate-rmf-0.1.8 (c (n "rmf") (v "0.1.8") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1dsnh4q4vdgbvwb8aw9bq1p2p75g5m4laqqhy8vnpw8qxb69jidm")))

(define-public crate-rmf-0.1.9 (c (n "rmf") (v "0.1.9") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "00fi3ibpdw19lr25j2mk2sgidgml55xm6vf7gshc27s7w1z6iik8")))

(define-public crate-rmf-0.2.0 (c (n "rmf") (v "0.2.0") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "02j9hwid0da5h2idlgpnxxj2gl9ylfi13rg1d555fiq4sk8gdzci")))

(define-public crate-rmf-0.2.1 (c (n "rmf") (v "0.2.1") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0rbd9qnw0samlxzjs3vb5pjvgqjl2dsmma7biwrcbpak81mr222s")))

(define-public crate-rmf-0.2.2 (c (n "rmf") (v "0.2.2") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "09gfhkzjyzs55m5c1n8lw7ahfhz1zx9f5abxv3ljjbwl7gvcilqq")))

(define-public crate-rmf-0.2.3 (c (n "rmf") (v "0.2.3") (d (list (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1bnhvh49rbkzscqhvkh6jiwy6hxz20wsadv0744gq9nq4lxsyr7y")))

(define-public crate-rmf-0.2.4 (c (n "rmf") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "03fs67ylgl2xzcjqkvsqh9p9bg0h0jr956srl6877jl6p1n7s9px")))

(define-public crate-rmf-0.2.5 (c (n "rmf") (v "0.2.5") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0v9j53w1xrzvkm8jaf2qdgap2c0m2fzflmy052f6x651ad5v8xgp")))

