(define-module (crates-io #{3}# r rlp) #:use-module (crates-io))

(define-public crate-rlp-0.1.0 (c (n "rlp") (v "0.1.0") (d (list (d (n "elastic-array") (r "^0.5") (d #t) (k 0)) (d (n "ethcore-bigint") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "11l6j6xi5qp7r9qblzkr7xlcbag4piswjjhxnnzik8947i8xxrys")))

(define-public crate-rlp-0.1.1 (c (n "rlp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.7.0") (d #t) (k 0)) (d (n "ethcore-bigint") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0vqmfkz9vab9mvdxyc0mf5jim9rikg5dgspba1qaar8wry3dbhb4")))

(define-public crate-rlp-0.2.0 (c (n "rlp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.7.0") (d #t) (k 0)) (d (n "ethcore-bigint") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1nbpship0i1k7q1vip15vr15pvk3gs6g7xb13zilyx2rgn2fc188")))

(define-public crate-rlp-0.2.1 (c (n "rlp") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.9") (d #t) (k 0)) (d (n "ethcore-bigint") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "1ybf217yy2n4qshp4dz412nx10lhav2391wrh8arpjn04376zgms")))

(define-public crate-rlp-0.2.2 (c (n "rlp") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.10") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.3") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "02wzdycyi00j30qv4k129c5nhfi7zl3pmkiymjrpmsymzn6pznw9")))

(define-public crate-rlp-0.2.3 (c (n "rlp") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.10") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (k 0)))) (h "00rw9yxp1daz79b0n6y8ybv19dmfyw41w9rj8r1z341daxfqmf6h") (f (quote (("ethereum" "ethereum-types") ("default" "ethereum"))))))

(define-public crate-rlp-0.2.4 (c (n "rlp") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "elastic-array") (r "^0.10") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (k 0)))) (h "1ziqn87gr44dgka7x1rmsga88mqbbvnz77a6r3gqb5w5akamlk2j") (f (quote (("ethereum" "ethereum-types") ("default" "ethereum"))))))

(define-public crate-rlp-0.3.0-beta (c (n "rlp") (v "0.3.0-beta") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (k 0)))) (h "05hgb0484p7k9d4h5d8ayi6pc4c040px5srwzwif9dsipb9jjq36") (f (quote (("ethereum" "ethereum-types") ("default" "ethereum"))))))

(define-public crate-rlp-0.3.0-beta.0 (c (n "rlp") (v "0.3.0-beta.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (k 0)))) (h "0iwvkk88kq2pnklyj0ay3ghd2g2pvnnw0gjgv643f84nbvpgvk9q") (f (quote (("ethereum" "ethereum-types") ("default" "ethereum"))))))

(define-public crate-rlp-0.3.0-beta.1 (c (n "rlp") (v "0.3.0-beta.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (k 0)))) (h "14yfhgjk29gjgam31sjwja0zpsyir9m50vz2f5dv8799g5ppqpyy") (f (quote (("ethereum" "ethereum-types") ("default" "ethereum"))))))

(define-public crate-rlp-0.3.0 (c (n "rlp") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (k 0)))) (h "082pfkdzsnzvqr5yx5qb2hildjg597p2115ywy84zma5k3zfzl8n") (f (quote (("ethereum" "ethereum-types") ("default" "ethereum"))))))

(define-public crate-rlp-0.4.0 (c (n "rlp") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (k 0)))) (h "0z0v7ph0hx9i0pbj6z77ilbj67fw0awpiszxh48yzyqb8p0mc39v")))

(define-public crate-rlp-0.4.2 (c (n "rlp") (v "0.4.2") (d (list (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "primitive-types") (r "^0.4") (f (quote ("impl-rlp"))) (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (k 0)))) (h "0nbb7vfq6ypqf4p8bjhyj2c71jh3wv9vrxqy22lks4rdc6f7ybzs")))

(define-public crate-rlp-0.4.3 (c (n "rlp") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "primitive-types") (r "^0.6") (f (quote ("impl-rlp"))) (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (k 0)))) (h "17zhnjm3j4kjiaf404hjapyzsqb3351bpgiv4rlkzdgb4pvs6xl3") (f (quote (("std" "rustc-hex/std") ("default" "std"))))))

(define-public crate-rlp-0.4.4 (c (n "rlp") (v "0.4.4") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "primitive-types") (r "^0.6") (f (quote ("impl-rlp"))) (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (k 0)))) (h "1kc41w4gldgdh578ab5brzpk3jf6xl3hjr3mifpkicpwiapdai1s") (f (quote (("std" "rustc-hex/std") ("default" "std"))))))

(define-public crate-rlp-0.4.5 (c (n "rlp") (v "0.4.5") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "primitive-types") (r "^0.7") (f (quote ("impl-rlp"))) (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (k 0)))) (h "0ia3rdjy526cgcxp9zysd8ihqhmckzslmwdq2nn4wxllxndkyzaa") (f (quote (("std" "rustc-hex/std") ("default" "std"))))))

(define-public crate-rlp-0.4.6 (c (n "rlp") (v "0.4.6") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.1") (d #t) (k 2)) (d (n "primitive-types") (r "^0.7") (f (quote ("impl-rlp"))) (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (k 0)))) (h "0wrf5wkza61f92aifk8p9q2y2fbz9k4bi6yhyppg24m5qg4dr40i") (f (quote (("std" "rustc-hex/std") ("default" "std"))))))

(define-public crate-rlp-0.5.0 (c (n "rlp") (v "0.5.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (k 0)))) (h "1a7vpdsqd1ijsm1jixwq1add18vwp16k1iw5p34rcxrygqa6jhz5") (f (quote (("std" "bytes/std" "rustc-hex/std") ("default" "std"))))))

(define-public crate-rlp-0.5.1 (c (n "rlp") (v "0.5.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "primitive-types") (r "^0.10") (f (quote ("impl-rlp"))) (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0.1") (k 0)))) (h "1da7b1hc4czlmsyr7ifs9bz9fv8hi5dw8q14xnmjlydfn2mhi5cr") (f (quote (("std" "bytes/std" "rustc-hex/std") ("default" "std"))))))

(define-public crate-rlp-0.5.2 (c (n "rlp") (v "0.5.2") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.1") (d #t) (k 2)) (d (n "primitive-types") (r "^0.12") (f (quote ("impl-rlp"))) (d #t) (k 2)) (d (n "rlp-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (k 0)))) (h "1v718xmnv7274y2rkrsjq8rmz9xzxnbzf3n15yyvcr23yd1r54dv") (f (quote (("std" "bytes/std" "rustc-hex/std") ("derive" "rlp-derive") ("default" "std"))))))

