(define-module (crates-io #{3}# r rsu) #:use-module (crates-io))

(define-public crate-rsu-0.1.0 (c (n "rsu") (v "0.1.0") (h "1sjjq8vs112lrvzz27xwkyl78dav7p2kbhj9ki49aahhjy8xykj5")))

(define-public crate-rsu-0.1.1 (c (n "rsu") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)) (d (n "which") (r "^4.2.4") (d #t) (k 0)))) (h "09nas0ijz1h1xlphsr5sfv0ikmk8y27ncac8zcd5s9ijkz4vp0zy")))

