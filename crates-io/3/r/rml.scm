(define-module (crates-io #{3}# r rml) #:use-module (crates-io))

(define-public crate-rml-0.1.0 (c (n "rml") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1w3q5qwxkb2k34wmj3xv3dywcxgw0vxi7jrg7kflc4qvj8a49n3v") (y #t)))

(define-public crate-rml-0.1.1 (c (n "rml") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)))) (h "1xjmjli8jdjlxwbcxaarsl6714z8abyf2zgsr9a51n6ih6qss75j")))

