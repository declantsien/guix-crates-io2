(define-module (crates-io #{3}# r rbs) #:use-module (crates-io))

(define-public crate-rbs-0.1.0 (c (n "rbs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0q09gd7jj5n8py25niwxizkx4b2l23j29aadbdb0czkg6mi1mzsh") (y #t)))

(define-public crate-rbs-0.1.1 (c (n "rbs") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0hgv6g5v2b83favkgk48yx9jgfvk49i8g2bis1zgzhg1n0cbmaqa") (y #t)))

(define-public crate-rbs-0.1.2 (c (n "rbs") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0413ka57hkc52rfj59idnyrwr7yvlcp83azh13sjim0rninc01kx") (y #t)))

(define-public crate-rbs-0.1.3 (c (n "rbs") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1rhgbhlkpzqqfrwghbkbbv5497q34f7bmz7wlkqczff1cm7zx3zp") (y #t)))

(define-public crate-rbs-0.1.4 (c (n "rbs") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "08vmva2nszd3w08f0l8gfal1s93d84ik5crw0gf728xv20bblwph") (y #t)))

(define-public crate-rbs-0.1.5 (c (n "rbs") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "047lz58fxl1dgf4qvpms89z3n5yhvcb1gg8lq4qcq3x1sznp27fv") (y #t)))

(define-public crate-rbs-0.1.6 (c (n "rbs") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0am2qjjbfhak9bi2cgk8bdwrp9vrr10zlfbjwgz20czg499h5gj0") (y #t)))

(define-public crate-rbs-0.1.7 (c (n "rbs") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0hly6bkhyygf72b41jx4a0h06wzhpfrhdlz9bgckcvp77b2vw18v") (y #t)))

(define-public crate-rbs-0.1.8 (c (n "rbs") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1d0jx5ipi12i26dmy9871ksizhl48j5dr24f817zysc3238hr2rf") (y #t)))

(define-public crate-rbs-0.1.9 (c (n "rbs") (v "0.1.9") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1klq2v40ghzvqpsx751r7kapzjx100y5f1qd5ydp75s4gidgl9nk") (y #t)))

(define-public crate-rbs-0.1.10 (c (n "rbs") (v "0.1.10") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "04b06ch2ap2syp1abypczx8068c57mam4b08l9xcsmk0arrj6lmx") (y #t)))

(define-public crate-rbs-0.1.11 (c (n "rbs") (v "0.1.11") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1w6jz180951m64c1s5x94hfbb6bb00khfy4i9k91p6p1rk5kvnnh") (y #t)))

(define-public crate-rbs-0.1.12 (c (n "rbs") (v "0.1.12") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "11fq81v9jlgh4n6s0dyl4pz22r9pr22cpp0av343mlzi641msa6h") (y #t)))

(define-public crate-rbs-0.1.13 (c (n "rbs") (v "0.1.13") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1q5b5sx55mk48v2sgvaanr0x1awpjprzjinfqgcdkb0q6xifd0nd") (y #t)))

(define-public crate-rbs-0.1.14 (c (n "rbs") (v "0.1.14") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0if73crm2k6xpgy3xkl1y9h18jcv8zrda750qqwmc3hjn877ilgq")))

(define-public crate-rbs-0.1.15 (c (n "rbs") (v "0.1.15") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "08k8yxlrnj8drhwkqjxm1xn7vhqgmf1f33gfw4416w72bbcd2f0m")))

(define-public crate-rbs-4.1.0 (c (n "rbs") (v "4.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "099a6sfdql2yfnjklywgnx4y9f14cb66wf4lcnkakhg1i0hq53ij") (y #t)))

(define-public crate-rbs-4.1.1 (c (n "rbs") (v "4.1.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0k6sg366xyv0bhz9wkfdz0qfwyv3ril9sby4sbsq5saxmgl8blcy") (y #t)))

(define-public crate-rbs-4.2.0 (c (n "rbs") (v "4.2.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "19jxlhbvyg08njk09fv6xqgwzgvnazpm6kv143dbvv3gn644mlyr") (y #t)))

(define-public crate-rbs-4.3.0 (c (n "rbs") (v "4.3.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0wkyp5zi5pkgp5d3rr2aix7vs1z34lmjhllqw0r52m33v96dwc9s") (y #t)))

(define-public crate-rbs-4.3.1 (c (n "rbs") (v "4.3.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0jss5gblfp3xwrjhpy6lvjpga3xspisvrbhd3wcjm06pk9w54zwh")))

(define-public crate-rbs-4.3.2 (c (n "rbs") (v "4.3.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1i1qkqynrmwsfbafks5kikpzizsn4gyya61ra11yi8abh35wjz7l")))

(define-public crate-rbs-4.3.3 (c (n "rbs") (v "4.3.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0igiazr81nfr3vgwvzs6r4zx906k5c5xz9p3r866rd0f6n1mqx5j")))

(define-public crate-rbs-4.4.0 (c (n "rbs") (v "4.4.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1cpv9jjjkx3rm4ca0r1ffw7py0ssk5ghdpapcx0f7j39ssbcm53n") (y #t)))

(define-public crate-rbs-4.4.1 (c (n "rbs") (v "4.4.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1sgi47yk9h351glj43snszmh4xs5i86ihz3cpwfxani29k45s994") (y #t)))

(define-public crate-rbs-4.4.2 (c (n "rbs") (v "4.4.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 0)) (d (n "rmp") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "14c1snmk580wl2g6608qhqd7vw60nmm28q8ihariaqmhawypa4ld") (y #t)))

(define-public crate-rbs-4.4.3 (c (n "rbs") (v "4.4.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gkxcwj1xa1svpqah367f128nav2nbn0hjxr6knmlad4qvqjrf4w") (y #t)))

(define-public crate-rbs-4.4.4 (c (n "rbs") (v "4.4.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i73igj3c69s09jnrapqhzq61i9c4w8fhvqg3lhs9ijd52y4jv65") (y #t)))

(define-public crate-rbs-4.4.5 (c (n "rbs") (v "4.4.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sqdxr8a60sw3hrki2pr79axj8cmqh0frf8w75rz555fvrjg6nvs")))

(define-public crate-rbs-4.5.0 (c (n "rbs") (v "4.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rgml0jl80dm0l2ng9v2gwd1mfbkzpbhxc6kwlsjcr80br048322")))

(define-public crate-rbs-4.5.1 (c (n "rbs") (v "4.5.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17gi9622ql3999a4k9b379dn1jlinq48kzpcby5aqynjc0frcblc")))

(define-public crate-rbs-4.5.2 (c (n "rbs") (v "4.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f8nkn31w1197r41q5y12p98k0wi9qkqyrhc4972wrsi3qh45aal")))

(define-public crate-rbs-4.5.3 (c (n "rbs") (v "4.5.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ppgn1hgs2i9wkcp0w81ibbyixhyxv1hi272l4xsv8h045nwxczk")))

(define-public crate-rbs-4.5.4 (c (n "rbs") (v "4.5.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ha6gij3h1qvcw010d54nlggza5zfk4ax0wvad7dcaxpjnx6qzj0")))

(define-public crate-rbs-4.5.5 (c (n "rbs") (v "4.5.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j9mic8l5hknc7bny77x8fw8njx3nikxmz7bh08xjxw2ffir12yl") (f (quote (("default") ("debug_mode")))) (y #t)))

(define-public crate-rbs-4.5.6 (c (n "rbs") (v "4.5.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17pfqklijgqr52xc81yi1lcdzq7d5cq3d2miqs3yv6c654cswwqc") (f (quote (("default") ("debug_mode")))) (y #t)))

(define-public crate-rbs-4.5.7 (c (n "rbs") (v "4.5.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hdl3ijmqrmjrrpi94q004ikmlcvc23rrvs3rhj0j974fsm8sjw8") (f (quote (("default") ("debug_mode")))) (y #t)))

(define-public crate-rbs-4.5.8 (c (n "rbs") (v "4.5.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y2scagmp98vnmx14ifl3gznkq3a75rxsfp1qln61din9mnxvd0n") (f (quote (("default") ("debug_mode")))) (y #t)))

(define-public crate-rbs-4.5.9 (c (n "rbs") (v "4.5.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09fiaznxjzb16k5fcnqw02k0p04hpk9wh0rcg7nmnllz7gani0f5") (f (quote (("default") ("debug_mode")))) (y #t)))

(define-public crate-rbs-4.5.10 (c (n "rbs") (v "4.5.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00g23vmyx4mvbf1318lszrnk4qwrdaa6kagg1zh9yxvnfwy86k7y") (f (quote (("default") ("debug_mode")))) (y #t)))

(define-public crate-rbs-4.5.11 (c (n "rbs") (v "4.5.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01mahqrcj8lmn8jd32xq01wqkvhlrc8g6043kk4jkxqdhxg8aycj") (f (quote (("default") ("debug_mode"))))))

(define-public crate-rbs-4.5.12 (c (n "rbs") (v "4.5.12") (d (list (d (n "indexmap") (r "^2.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a9x1s4w82km056kgf9pbjrd7jwk89p74inyvfd3x3kf3c0gqks9") (f (quote (("default") ("debug_mode")))) (y #t)))

(define-public crate-rbs-4.5.13 (c (n "rbs") (v "4.5.13") (d (list (d (n "indexmap") (r "^2.2.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n8xp9x2fwhv06037r2lrkysj4d1b4fy334cwcsp6apydilvv8gq") (f (quote (("default") ("debug_mode"))))))

