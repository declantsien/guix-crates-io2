(define-module (crates-io #{3}# r rit) #:use-module (crates-io))

(define-public crate-rit-0.1.0 (c (n "rit") (v "0.1.0") (h "0n2mwdrhzm0zcl31fs7h9r5bys8pq9z3qp030v1zckyxl6n4njcq")))

(define-public crate-rit-0.1.1 (c (n "rit") (v "0.1.1") (h "1bdsz9lkgdi2ab8wjhr9zldrnzvvxdxd964aqbsawh2dqmpcsvh8")))

(define-public crate-rit-0.1.2 (c (n "rit") (v "0.1.2") (h "0nkqbli0xnvginhhn6agh0d799dswnmbb152vlhsvwgbpm9lr6j5")))

(define-public crate-rit-0.1.3 (c (n "rit") (v "0.1.3") (h "0c36d4ijiwpiv7d8zxayj1qxams6ll9cv5mc0zlfn37ijnvi80g7")))

