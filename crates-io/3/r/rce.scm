(define-module (crates-io #{3}# r rce) #:use-module (crates-io))

(define-public crate-rce-0.0.1 (c (n "rce") (v "0.0.1") (h "1c62jy7aaxknnrzk29p68zxazdncym3x46iqvnq2psrwvi8jxqac")))

(define-public crate-rce-0.0.2 (c (n "rce") (v "0.0.2") (h "0v553wrs323ch1jpzzhiwil28zx4ysy8w9ckg64byvp8qjixjq98")))

(define-public crate-rce-0.0.3 (c (n "rce") (v "0.0.3") (h "16rc6j2g4f780il6z83xj7s4z6mdq4jxh3dhpnr5zjhz44s41nsi")))

