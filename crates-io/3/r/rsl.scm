(define-module (crates-io #{3}# r rsl) #:use-module (crates-io))

(define-public crate-rsl-0.1.0 (c (n "rsl") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.13") (o #t) (d #t) (k 0)))) (h "1s5jkm7fbq6258lxkjf6fay5gipfll7ddqh4g6za19dg7g6pv7vr") (f (quote (("rational") ("complex")))) (y #t)))

(define-public crate-rsl-0.1.1 (c (n "rsl") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.34") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.13") (d #t) (k 0)))) (h "1sfhf54s24paiq3652pphhdmdkydq52lcga11fbmqa2xpzxj1gyx") (y #t)))

