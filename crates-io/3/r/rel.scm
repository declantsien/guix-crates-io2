(define-module (crates-io #{3}# r rel) #:use-module (crates-io))

(define-public crate-rel-0.1.0 (c (n "rel") (v "0.1.0") (h "19a2hzrxl98zrmdngw34ziwfmkkbnqlv5cywk629dhlf2rwnvyl5")))

(define-public crate-rel-0.1.1 (c (n "rel") (v "0.1.1") (h "0z1pk48vk37wxz1jisp54dd8v27n4p43b4fqkad1ymf31dfivg61")))

(define-public crate-rel-0.1.2 (c (n "rel") (v "0.1.2") (h "0zc6bfb1bsy02n59a0cjrwgl8pm86rnw81h8i0zy5lsgz0b0sr8d")))

(define-public crate-rel-0.2.0 (c (n "rel") (v "0.2.0") (h "0samkgyvrl04h8qnj8m3izfz4dmybzng3g77wjj51w5zc4ka7x1m")))

