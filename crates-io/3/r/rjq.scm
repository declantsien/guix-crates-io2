(define-module (crates-io #{3}# r rjq) #:use-module (crates-io))

(define-public crate-rjq-0.1.0 (c (n "rjq") (v "0.1.0") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1nzk12wmz36kp4hj4wjjgicp6rm5cgh1wkc6n3m7yzp04cl55avw")))

(define-public crate-rjq-0.1.1 (c (n "rjq") (v "0.1.1") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "uuid") (r "^0.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1ffazf1rxjn6p33c4sy894dkjp95a0pil80vd0z2drdlpni31qkl")))

(define-public crate-rjq-0.1.2 (c (n "rjq") (v "0.1.2") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "uuid") (r "^0.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0s59gxk1p379s041vbqdw8rbxycr9fwh758s92375gvflp3jk6qq")))

(define-public crate-rjq-0.2.0 (c (n "rjq") (v "0.2.0") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "uuid") (r "^0.4.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1s13jc78hsrsa5xbwc8sr40n6whv586sswcmd827mchzjhksq6kd")))

(define-public crate-rjq-0.3.0 (c (n "rjq") (v "0.3.0") (d (list (d (n "redis") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "0rw37hd4immrpg53a2w3md6zmzs956irsfy4smv6cirgs43xr5a4")))

