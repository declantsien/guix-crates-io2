(define-module (crates-io #{3}# r rmd) #:use-module (crates-io))

(define-public crate-rmd-0.1.0 (c (n "rmd") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0s9lisb0dr7rfibgl3cr4yrgv71k20qwh0dca16f7hygs7yffgbk")))

(define-public crate-rmd-0.2.0 (c (n "rmd") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0hk00axwqdxxnw8lypkpdiira5irb4xw8par57nzwx7sbyxwgaz8")))

(define-public crate-rmd-0.2.1 (c (n "rmd") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0skc309agraac84gs0jj33b8p9hy2p1rxz786jb57qs27dp5adis")))

(define-public crate-rmd-0.3.0 (c (n "rmd") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "132kg6n8a20zaiaf4lzfnyrcvqqvvj86dn5vb1jwkw701jv4sz07")))

(define-public crate-rmd-0.4.0 (c (n "rmd") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0qjjqcr5i5csf75rv6mvb87wb8mxw0zkhxv1hcxcci07f5pd41pz")))

(define-public crate-rmd-0.4.1 (c (n "rmd") (v "0.4.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "06k2maijj50pdbks27a990bv3hx5a4dn6mf5hqnyqhp2gyrsn8yq")))

(define-public crate-rmd-0.4.2 (c (n "rmd") (v "0.4.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "syslog") (r "^5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "138j57z867qxmgjbn7qlx04fyrcwy2932i20501zmfryzx1jq2ys")))

(define-public crate-rmd-0.4.4 (c (n "rmd") (v "0.4.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "syslog") (r "^5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0536ipv4f145qk7ldq7dv95gnrd43zzbsx1xr4wb00rhhhfdc1hy")))

(define-public crate-rmd-0.5.0 (c (n "rmd") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "syslog") (r "^5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1wbcnqrjl8dqqgbjdl4clsw53mqbj3v0drp3is2xxn6npmk783sd")))

(define-public crate-rmd-0.5.1 (c (n "rmd") (v "0.5.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "syslog") (r "^5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "041z752nksqnq94l9brpi3k1rnwbd3wmkwskx2p19mwz1i548gm8")))

(define-public crate-rmd-0.5.2 (c (n "rmd") (v "0.5.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "syslog") (r "^5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "09acwdqs74mdksnk8x7x5lvn79jchpp637hhs2fas01md7k7w0zb")))

(define-public crate-rmd-0.5.3 (c (n "rmd") (v "0.5.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ring") (r "^0.16.9") (d #t) (k 0)) (d (n "syslog") (r "^5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1qxi4vw4r9sfx4q3gwkhzwj11rpb6qb6czpwkb8m6vm6qkg4488s")))

