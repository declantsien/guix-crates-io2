(define-module (crates-io #{3}# r rim) #:use-module (crates-io))

(define-public crate-rim-0.1.0 (c (n "rim") (v "0.1.0") (d (list (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "01jia78zz206x6wpflb37y3xqzbphrmqqw2lgjdhvc2z4y2qvn93")))

(define-public crate-rim-0.1.1 (c (n "rim") (v "0.1.1") (d (list (d (n "termion") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1") (d #t) (k 0)))) (h "1q22v77dw8f06w9i18c5i5qg9l4mxymrnj2s4vysiyfg0zl2x3vn")))

