(define-module (crates-io #{3}# r rab) #:use-module (crates-io))

(define-public crate-rab-0.1.0 (c (n "rab") (v "0.1.0") (d (list (d (n "iced") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "03fxc9lrk8xm67bz2dsnjpi977jjnq5qcpg90854cc190mf7ql8n")))

(define-public crate-rab-0.1.1 (c (n "rab") (v "0.1.1") (d (list (d (n "iced") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1a6lckm7b761hnqcsp8ica1svn37cy0kmxs0fk81xgshi441jndi")))

(define-public crate-rab-0.1.2 (c (n "rab") (v "0.1.2") (d (list (d (n "iced") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "152nfwzzxj4bg607rf8p9xjxw77lwn0qbvpmwbmm9s5mpz93b1nd")))

(define-public crate-rab-0.1.3 (c (n "rab") (v "0.1.3") (d (list (d (n "iced") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06nnlplcc8xf7dmdkvd64ygj1aznr9gs2dqg0m0l66a6aaicj3n8")))

(define-public crate-rab-0.2.0 (c (n "rab") (v "0.2.0") (d (list (d (n "iced") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lexical-sort") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "13f08msv3li5cnxy3dk4qj129h8j6339pgp17wbzx6n9ksxr3daj")))

(define-public crate-rab-0.3.0 (c (n "rab") (v "0.3.0") (d (list (d (n "iced") (r "^0.3") (f (quote ("svg" "tokio"))) (d #t) (k 0)) (d (n "iced_futures") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lexical-sort") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0lkxx9n2fipiaqx00y4jz28zjdqlp8kci8995dy8wryx99kf6h2m")))

(define-public crate-rab-0.3.1 (c (n "rab") (v "0.3.1") (d (list (d (n "iced") (r "^0.3") (f (quote ("svg" "tokio"))) (d #t) (k 0)) (d (n "iced_futures") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lexical-sort") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0yr8r4cvk7lp5rkr3vsid51y1pfv5psq4a567jhkb50hlj14ld9c")))

(define-public crate-rab-0.3.2 (c (n "rab") (v "0.3.2") (d (list (d (n "iced") (r "^0.3") (f (quote ("svg" "tokio"))) (d #t) (k 0)) (d (n "iced_futures") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lexical-sort") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0xapdhsvhqcqmpw6dhk4zd4id39dmff4pgi0f0hf162pvfsfqfdn")))

(define-public crate-rab-0.3.3 (c (n "rab") (v "0.3.3") (d (list (d (n "iced") (r "^0.3") (f (quote ("svg" "tokio"))) (d #t) (k 0)) (d (n "iced_futures") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lexical-sort") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1r819ps4q4dx3k9z3ksxnxl1sjp76l73f08f83cdffvy824bybxy")))

(define-public crate-rab-0.4.0 (c (n "rab") (v "0.4.0") (d (list (d (n "iced") (r "^0.3") (f (quote ("svg" "tokio"))) (d #t) (k 0)) (d (n "iced_futures") (r "^0.3") (d #t) (k 0)) (d (n "lexical-sort") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "rab-core") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "12bzymb9wrm0adn79xxrkkqvsb73x66wiir0jxg8lpkks1y7b3jf")))

(define-public crate-rab-0.5.0 (c (n "rab") (v "0.5.0") (d (list (d (n "iced") (r "^0.3") (f (quote ("svg" "tokio"))) (d #t) (k 0)) (d (n "iced_futures") (r "^0.3") (d #t) (k 0)) (d (n "lexical-sort") (r "^0.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "rab-core") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0kiczhrhcaxyrh9sk8hqca236gbzbaiq1rzlsq8zigl6i7vx083w")))

