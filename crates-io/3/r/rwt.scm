(define-module (crates-io #{3}# r rwt) #:use-module (crates-io))

(define-public crate-rwt-0.1.0 (c (n "rwt") (v "0.1.0") (d (list (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "1ar0c49l8557hrx1ks4h78hwddpd1rr6cwyfb28k0agk64l2y51m")))

(define-public crate-rwt-0.1.1 (c (n "rwt") (v "0.1.1") (d (list (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "0ahrmbk2ra8wh9xl9aqhlrwcbxxnfw4711a5j92449iizghrwrys")))

(define-public crate-rwt-0.2.1 (c (n "rwt") (v "0.2.1") (d (list (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)))) (h "0i6zydf7izkq17g5xnbrrkr27qls80y6d23m413xk57v64nwbmj3")))

(define-public crate-rwt-0.2.2 (c (n "rwt") (v "0.2.2") (d (list (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.0") (d #t) (k 0)))) (h "0jl5ii59049nmiqdkdwgwsbl1ppypn1kj80cf6grqx7vj8wj4567")))

(define-public crate-rwt-0.2.3 (c (n "rwt") (v "0.2.3") (d (list (d (n "rust-crypto") (r "^0.2.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^0.8.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.8.0") (d #t) (k 0)))) (h "1dr50h660wv41h669g1v76hhiazw05884jfwdv81jb6irlskqwnc")))

(define-public crate-rwt-0.3.0 (c (n "rwt") (v "0.3.0") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10bfd3p1k3dm7kcqg0l1cpxbbm44y0a4zhgr3w99nx2f4cmnzmq7")))

(define-public crate-rwt-0.4.0 (c (n "rwt") (v "0.4.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1ri1gvaf1qn2xbsv5pzrgh956gp1sp76chi8h5y51zlrqyyd9rm0")))

