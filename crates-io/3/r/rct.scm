(define-module (crates-io #{3}# r rct) #:use-module (crates-io))

(define-public crate-rct-0.1.0 (c (n "rct") (v "0.1.0") (h "1w445gqbrw3pascf2adb30rmzkik4j2wws2c4r4czh1wg5q7fhyb")))

(define-public crate-rct-0.1.1 (c (n "rct") (v "0.1.1") (h "0vy99qj8sqhrrj694hp7ngmm2swd3xadb50m4chd1sxfjx15mji1")))

(define-public crate-rct-0.1.3 (c (n "rct") (v "0.1.3") (d (list (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "09fq9l844bd475i0vj9h028x9hawbad9prfwbnkxi4lvzqga7v7n")))

(define-public crate-rct-0.1.4 (c (n "rct") (v "0.1.4") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0ma1xskrb0qd958mkrvw81h3haafcn8sgcv36s9af1jd4sz5m1km")))

(define-public crate-rct-0.1.5 (c (n "rct") (v "0.1.5") (d (list (d (n "rct_derive") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "rct_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0w23dnbc72n6v7pnzfd5f0627k59azvjpj5r8s2ssac0j84nv9pj") (f (quote (("derive" "rct_derive") ("default" "derive"))))))

(define-public crate-rct-0.2.0 (c (n "rct") (v "0.2.0") (d (list (d (n "rct_derive") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "rct_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0nm5807hk8dpzf43sc7ddfdx23rl6vl4rplyajq0s84wbqww6ln9") (f (quote (("derive" "rct_derive") ("default" "derive"))))))

(define-public crate-rct-0.2.1 (c (n "rct") (v "0.2.1") (d (list (d (n "rct_derive") (r "=0.1.0") (o #t) (d #t) (k 0)) (d (n "rct_derive") (r "^0.1.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1fx72awzg8mjplb83kyqhllfv1rln7p1c8vr1vlzj25zyiz67kgl") (f (quote (("derive" "rct_derive") ("default" "derive"))))))

