(define-module (crates-io #{3}# r rbt) #:use-module (crates-io))

(define-public crate-rbt-0.1.0 (c (n "rbt") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "shared_memory") (r "^0.11") (d #t) (k 0)))) (h "1f4dif57hfqjw4dhd8pqrzdzxw3f6gcrdwvgnvc9caqjc6nnj9m9")))

