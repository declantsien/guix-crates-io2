(define-module (crates-io #{3}# r rtc) #:use-module (crates-io))

(define-public crate-rtc-0.0.0 (c (n "rtc") (v "0.0.0") (h "0y81khrqbk8pk3g3g9ihfmzl5lhw04jl3fjr0cg62d3a42lmlk02")))

(define-public crate-rtc-0.0.1 (c (n "rtc") (v "0.0.1") (h "0zvlqg4f4c74nz8bnpzhmqxvdbmc1cw0319c1a44y8idg8d1w8i2")))

(define-public crate-rtc-0.0.2 (c (n "rtc") (v "0.0.2") (h "16713ay5r50z6day2p4qfkafcdndipw2ha9yn2f6y03cv31llb0b")))

(define-public crate-rtc-0.0.3 (c (n "rtc") (v "0.0.3") (h "15js9kw3d7k13nvnwji0cpd6p1k2g74n9jy26sxlaplgkiw271q7")))

