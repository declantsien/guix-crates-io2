(define-module (crates-io #{3}# r rux) #:use-module (crates-io))

(define-public crate-rux-0.1.0 (c (n "rux") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (f (quote ("signalfd"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.1.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.4") (d #t) (k 0)) (d (n "slab") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0x3nx9jf9jx3x6wqrm6wpfsqkyzqand7l2j7j3r4gk7f0d88ig63")))

