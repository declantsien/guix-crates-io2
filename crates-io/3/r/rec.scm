(define-module (crates-io #{3}# r rec) #:use-module (crates-io))

(define-public crate-rec-0.1.0 (c (n "rec") (v "0.1.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.6") (d #t) (k 0)))) (h "1j7k90fvmc1r46lx9qpzv8lvn934fjqa3hds859qq7qs04ys1z3w")))

(define-public crate-rec-0.2.0 (c (n "rec") (v "0.2.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.6") (d #t) (k 0)))) (h "1c7whw47mf3qvhnzbpvrjb8zw8z0kw3sh3aqzg3k7zl4vjqqi57p")))

(define-public crate-rec-0.3.0 (c (n "rec") (v "0.3.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.6") (d #t) (k 0)))) (h "0yx15x6w2m5bnad6gjw1any6sfqasr1hyal8rswm62v66yava5q6")))

(define-public crate-rec-0.4.0 (c (n "rec") (v "0.4.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "01y4a92qf8h0vppspf620a5h4bhnar71lzmgcj4v1mqcifz96b5q")))

(define-public crate-rec-0.5.0 (c (n "rec") (v "0.5.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0lqk13lkhb72s2bsap9wkfr47gdl14x1i3994dkff043ia7nxxgk")))

(define-public crate-rec-0.6.0 (c (n "rec") (v "0.6.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0a96js0f7knii4jlhigjyax8cvravmkysg1g05fw379zpyi4wb90")))

(define-public crate-rec-0.7.0 (c (n "rec") (v "0.7.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "08pr64d4pqr8ip2a4grw7sr5hhsplv8lblaq6q6dlm852q0snybi")))

(define-public crate-rec-0.8.0 (c (n "rec") (v "0.8.0") (d (list (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1gkmy9shlsc0g2q32isyc5wcw1ijvviiqc8qwkfvqakpkkhc3mvc")))

(define-public crate-rec-0.9.0 (c (n "rec") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1s4dsbhwmr2vs4mpqybaq164n8dvw569857i53f10g76sh4cxhcm")))

(define-public crate-rec-0.10.0 (c (n "rec") (v "0.10.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1p41nvchb32rv2z91frddjr1vj2sfrcp9wjwjfarixmfr469qc6r")))

(define-public crate-rec-0.11.0 (c (n "rec") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^0.4.30") (d #t) (k 0)) (d (n "quote") (r "^0.6.13") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.42") (f (quote ("extra-traits"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1v6fnzg5pnh5nk317vf2qfdb02lh41xpwxk5vw5wl9vqrr9vhbqk")))

