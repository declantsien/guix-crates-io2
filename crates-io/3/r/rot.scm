(define-module (crates-io #{3}# r rot) #:use-module (crates-io))

(define-public crate-rot-0.1.0 (c (n "rot") (v "0.1.0") (d (list (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "1blkdkq7q5g5188qj9wq71k07wja900cz96iryavyw4ki0kqdnhv")))

(define-public crate-rot-0.1.1 (c (n "rot") (v "0.1.1") (d (list (d (n "typenum") (r "^1") (d #t) (k 0)))) (h "1bsywjvd5as53kzvzh77fgd21wnwpnm7gcqx0vsxfxv8aghn15jy")))

