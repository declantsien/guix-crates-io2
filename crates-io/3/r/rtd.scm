(define-module (crates-io #{3}# r rtd) #:use-module (crates-io))

(define-public crate-rtd-0.3.0 (c (n "rtd") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "migrant_lib") (r "^0.28.0") (f (quote ("d-sqlite"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.21.0") (d #t) (k 0)) (d (n "time") (r "^0.2.9") (d #t) (k 0)))) (h "0f6l2l89d81a1ihdwmfgg4bl8yzmngrk7amdki8n776px0ixd10m") (f (quote (("d-sqlite"))))))

(define-public crate-rtd-0.3.2 (c (n "rtd") (v "0.3.2") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "dirs") (r "~2.0.2") (d #t) (k 0)) (d (n "migrant_lib") (r "^0.28.0") (f (quote ("d-sqlite"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "time") (r "~0.2.16") (d #t) (k 0)))) (h "1wp7xjsgpjcdb3j072swhpcwwf2pb6ivqq4w1zrvwxjbq2am2f1f") (f (quote (("d-sqlite"))))))

(define-public crate-rtd-0.3.3 (c (n "rtd") (v "0.3.3") (d (list (d (n "anyhow") (r "~1.0") (d #t) (k 0)) (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "dirs") (r "~2.0.2") (d #t) (k 0)) (d (n "migrant_lib") (r "^0.28.0") (f (quote ("d-sqlite"))) (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "rusqlite") (r "^0.21.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "time") (r "~0.2.16") (d #t) (k 0)))) (h "02da58czhafk4qwmzbhnygh51fnwighd4461jq4ppvczcmfs3pag") (f (quote (("d-sqlite"))))))

