(define-module (crates-io #{3}# r rnt) #:use-module (crates-io))

(define-public crate-rnt-0.1.0 (c (n "rnt") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rug") (r "^1.13.0") (d #t) (k 0)))) (h "115wjzckdvvcanmypqkg2rdhaai50fskppc46rq4m47fjbp1nmvi")))

