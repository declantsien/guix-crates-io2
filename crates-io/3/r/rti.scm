(define-module (crates-io #{3}# r rti) #:use-module (crates-io))

(define-public crate-rti-0.1.0 (c (n "rti") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)))) (h "0579fz9k2k91847rlfkv16grky6328bl1afgijjy009jdpdazb3f")))

(define-public crate-rti-0.1.1 (c (n "rti") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)))) (h "1fk4qyxlvxlwi2fzsnd6gyvjrgn21hpwndibmwn6cq4l81z2lgj9")))

(define-public crate-rti-0.1.2 (c (n "rti") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)))) (h "1xyxv9dkck76z39biy05h7530fdqgvr71kpjbww7j56arvm4881c")))

(define-public crate-rti-0.1.3 (c (n "rti") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pygbayr1i41sqd7zmdrpklh4lgha7x51av9fcxckwxynr185ik8")))

(define-public crate-rti-0.1.4 (c (n "rti") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ylj5rqvc3rqwsds5wdm4xig9aw4mmb30b70j30drr0l16929dq2")))

(define-public crate-rti-0.1.5 (c (n "rti") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "06isg1zbgg83p0sni1qbzb8wrbn5imraa5i1vngpqkvwlsq1c991")))

(define-public crate-rti-0.1.6 (c (n "rti") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qpqs66hla91d4rprzdygn21vin63nncxb709r597jgzsglhcfnm")))

