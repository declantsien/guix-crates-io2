(define-module (crates-io #{3}# r rvn) #:use-module (crates-io))

(define-public crate-rvn-0.1.0 (c (n "rvn") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)))) (h "1vqi2mzyiyfbpvxz90a2jrwxzh6xalyybimjqyprpk3p5hh18r9l")))

(define-public crate-rvn-0.1.1 (c (n "rvn") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "12df20irq9rv95j6nhfrnkdzzzj8b5zvp2yncyawcb35f79im830")))

(define-public crate-rvn-0.2.0 (c (n "rvn") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.17") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "1lbq4wb6bhhlbc5y8fzgiwmwlip8208dsjliqx4kv0d24a4wss6a")))

(define-public crate-rvn-0.3.0 (c (n "rvn") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "strum") (r "^0.15.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15.0") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "02yy3j9zn1v08kn4k1rw035mnajgws5cgpn899gib3bsnkazzqjk")))

