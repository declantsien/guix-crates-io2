(define-module (crates-io #{3}# r rcf) #:use-module (crates-io))

(define-public crate-rcf-0.1.0 (c (n "rcf") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0cvx0fw9gl5i5naz53q8avz40bcr50m5hhyfd3kijgv38f28b8sm")))

(define-public crate-rcf-0.1.1 (c (n "rcf") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "07r04hxxbbz34y856dyvvp8ny6hc0lqvv3ahngkw8rkj4nyjvdwc")))

(define-public crate-rcf-0.1.2 (c (n "rcf") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "^0.3.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "00n087afq451dw9sisy6vr8r762fl9nch2kmjplmxq0x9ck45qr4")))

