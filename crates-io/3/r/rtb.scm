(define-module (crates-io #{3}# r rtb) #:use-module (crates-io))

(define-public crate-rtb-0.1.0 (c (n "rtb") (v "0.1.0") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l1vj59pzjwjjs96a93jph0p3c163y25vr64b4rcz0qkw3fvp1w9")))

(define-public crate-rtb-0.2.0 (c (n "rtb") (v "0.2.0") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1dfgi1ddn167b80jg072cwj7v3xsi23ilrzp9kghgakhcmq5j6jq")))

(define-public crate-rtb-0.3.0 (c (n "rtb") (v "0.3.0") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cpf04b865gvyv24aqmbh87pfi3fx93r9nlcv6lgsvdg3affa9wq")))

(define-public crate-rtb-0.3.1 (c (n "rtb") (v "0.3.1") (d (list (d (n "askama") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pbqiay9wg5sm9n8hpb6bsn5x2rb2xz44sgmsr3dfzzbcny9lqq7")))

