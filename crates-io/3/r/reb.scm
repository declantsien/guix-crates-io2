(define-module (crates-io #{3}# r reb) #:use-module (crates-io))

(define-public crate-reb-0.1.0 (c (n "reb") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.50") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.50") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window" "console" "css"))) (d #t) (k 0)))) (h "0fg3y6bwja14qyxx6vd2mh4hz8r6a3lypmmmpisifgm3fdys9c9k")))

(define-public crate-reb-0.1.1 (c (n "reb") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.50") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.50") (f (quote ("Document" "Element" "HtmlElement" "Node" "Window" "console" "css"))) (d #t) (k 0)))) (h "02xwyka0iy7x6fb2mzkrl9xazcshmmx5g3vipimwjrgzmfr7hc7i")))

