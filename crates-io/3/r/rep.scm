(define-module (crates-io #{3}# r rep) #:use-module (crates-io))

(define-public crate-rep-0.1.0 (c (n "rep") (v "0.1.0") (d (list (d (n "rep_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0j0bmzp9qxfapc8774did4xg5w6h1348nv2csfyn3fmd9sz4n2ws")))

(define-public crate-rep-0.2.0 (c (n "rep") (v "0.2.0") (d (list (d (n "rep_derive") (r "^0.2.0") (d #t) (k 0)))) (h "0kdding1pfz1kvqx0ri5g63qqns4206amcnlr63lvwbrinqlixmv")))

(define-public crate-rep-0.3.0 (c (n "rep") (v "0.3.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rep_derive") (r "^0.3.0") (d #t) (k 0)))) (h "1jman52b81fcgkr6s6k5arwisw45c2l1a6325vv4hpsczbbai816")))

