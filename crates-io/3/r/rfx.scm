(define-module (crates-io #{3}# r rfx) #:use-module (crates-io))

(define-public crate-rfx-1.0.0 (c (n "rfx") (v "1.0.0") (d (list (d (n "clap") (r "~2.27.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "json") (r "~0.12.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0kmyi2vh65pbwp9rzvfdz5fknp48pzwr8nl07y5b1zksmdns7ngf")))

