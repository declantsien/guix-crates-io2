(define-module (crates-io #{3}# r rwc) #:use-module (crates-io))

(define-public crate-rwc-0.1.0 (c (n "rwc") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.4") (d #t) (k 0)))) (h "0zs65mdx2fs90x2hpn3hvc2wc7qjg02pak0spfrqh3riymg9ii2n")))

(define-public crate-rwc-0.2.0 (c (n "rwc") (v "0.2.0") (d (list (d (n "docopt") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "090asl18k74vpn45js6968cwvx37hsmxqfhcvyfg13nsdvcrapwi")))

