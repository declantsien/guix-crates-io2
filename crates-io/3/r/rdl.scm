(define-module (crates-io #{3}# r rdl) #:use-module (crates-io))

(define-public crate-rdl-0.1.1 (c (n "rdl") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "freedesktop_entry_parser") (r "^1.1") (d #t) (k 0)))) (h "1bjymlx450ilaafpqf6c734rqblqv4d0jz9390m5hl64jp8r645b") (y #t)))

(define-public crate-rdl-0.1.2 (c (n "rdl") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "freedesktop_entry_parser") (r "^1.1") (d #t) (k 0)))) (h "0pz7zkfihv5i0wxc51w41q2gxs2ca8sy9icfdlfrcllhj9l50dp1")))

(define-public crate-rdl-0.1.3 (c (n "rdl") (v "0.1.3") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "freedesktop_entry_parser") (r "^1.1") (d #t) (k 0)))) (h "1bnr15j28rrwg55hpgw1pz0injscm1c5mv08idkblqkzsz4cdcxn")))

(define-public crate-rdl-0.1.4 (c (n "rdl") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "freedesktop_entry_parser") (r "^1.1") (d #t) (k 0)))) (h "13yhx8nw9zfa4ixpzlfzwraqyxf6qjk04i5yx3sbikqxjhv5sbr8")))

(define-public crate-rdl-0.1.5 (c (n "rdl") (v "0.1.5") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "freedesktop_entry_parser") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "1vlhnm9950qvqsyk9xrrpxyl6c2h8xig03fn13pzpsk5vzplc39y")))

(define-public crate-rdl-0.2.0 (c (n "rdl") (v "0.2.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "freedesktop_entry_parser") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "00jkimsn89pvjkmcqyxfpafiq1dsqlsr5kjaxrcd7hrficim0kcp")))

(define-public crate-rdl-0.2.1 (c (n "rdl") (v "0.2.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "freedesktop_entry_parser") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "00v7vymga4a2vjm0mlgavnk15d4nimy80mdz54cd31sj5xrk8zpi")))

(define-public crate-rdl-0.2.2 (c (n "rdl") (v "0.2.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1mgcvilvp4r51694s3392cv1yyqks1d9rmb0sarm7914nzd3b718")))

(define-public crate-rdl-0.3.0 (c (n "rdl") (v "0.3.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0rn45wxpliivzcsac2j8190p7k7ax7px2miq30dq4kys7zac5f9v")))

(define-public crate-rdl-0.3.1 (c (n "rdl") (v "0.3.1") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1bc9h4p877631c5dgyilxbcpl4gyd52i1ws4rarpgchy7y6s2kfs")))

