(define-module (crates-io #{3}# r rfs) #:use-module (crates-io))

(define-public crate-rfs-0.1.0 (c (n "rfs") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1zmr2c7nyqqg39lsj8pj63bi8yh1h7c02sk3xgkxxiyx0jw615pp")))

(define-public crate-rfs-0.2.0 (c (n "rfs") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gvbn0lwkaj3rwjybb5l6qfpy858aw5vn4a0vj0kidh994rx08bh")))

(define-public crate-rfs-0.3.0 (c (n "rfs") (v "0.3.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10qkkx088yx9j526ymr9bkgjvr1kpl02qvz521rrc4l8wm8bjcsq")))

(define-public crate-rfs-1.0.0 (c (n "rfs") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)))) (h "011bqvcfwqvqsyn51n8acz5vl5qg6797qkw6n8r8lc3i6sgaqhcv")))

