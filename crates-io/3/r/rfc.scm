(define-module (crates-io #{3}# r rfc) #:use-module (crates-io))

(define-public crate-rfc-0.1.0 (c (n "rfc") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.72") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0k01waal70lc0p7x0hi7n3dpmgvdb0l5xvc90mi2pcgpddrn8h2i")))

(define-public crate-rfc-0.2.0 (c (n "rfc") (v "0.2.0") (d (list (d (n "docopt") (r "^0.6.72") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0j91n189b3di3fgq5sdwwa5l82mdj9xwjfj2mic7plqy2msfw5pl")))

(define-public crate-rfc-0.3.0 (c (n "rfc") (v "0.3.0") (d (list (d (n "docopt") (r "^0.6.72") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "18shjc037j7k4dq0kk3qci6hndlkqgcbf821ssy59ij8bcd3hs5i")))

