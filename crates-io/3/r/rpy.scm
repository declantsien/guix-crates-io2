(define-module (crates-io #{3}# r rpy) #:use-module (crates-io))

(define-public crate-rpy-0.1.0 (c (n "rpy") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1qf1vnb6b6c6024y5wqa39qwyxn7y18lwmsx924mlsqykzglzcaj")))

(define-public crate-rpy-0.2.0 (c (n "rpy") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "141vymwgy9jafl7pwf4jskr0r2gibmwh03aqnyqasqzacxmlr11z")))

(define-public crate-rpy-0.2.1 (c (n "rpy") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0ynmmx18gh6d2b6vb9id6iwjc4bl17xhzmr7f84ghi4fp5bhngm5")))

(define-public crate-rpy-0.2.2 (c (n "rpy") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "color-eyre") (r "^0.6.0") (d #t) (k 0)) (d (n "eyre") (r "^0.6.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0zfdg05nl4digciyawy3gw6j1ay9xikks8c3mhc40bvz9njp04v7")))

