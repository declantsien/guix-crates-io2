(define-module (crates-io #{3}# r rox) #:use-module (crates-io))

(define-public crate-rox-0.1.0 (c (n "rox") (v "0.1.0") (h "1qmfh6zbdcdv2v69l8d47pghind01sjhxpzrr25qzd1v2jr27vsv")))

(define-public crate-rox-0.1.1 (c (n "rox") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "1m7fc4nfzwrmibjzna5c8kwbdgal3ynrxlbrbb52pia7724i5nnh")))

(define-public crate-rox-0.1.2 (c (n "rox") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)))) (h "00nvw4y9zyyfphyvrb8npcn35xhy5xwi9mdjzdbvwwrg0cp0iyx1")))

(define-public crate-rox-0.1.3 (c (n "rox") (v "0.1.3") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "gmp-mpfr") (r "^0.3") (d #t) (k 0)))) (h "0qny9lvbwa5jhh95psh4ihblanf4bnp0gm72v3wdv55s7cqra16s")))

(define-public crate-rox-0.1.4 (c (n "rox") (v "0.1.4") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "gmp-mpfr") (r "^0.3") (d #t) (k 0)))) (h "0201rszhq5k6ajs37n02npnm7rzwmbq86ngcgkg6xh062q4c97a8")))

(define-public crate-rox-0.1.5 (c (n "rox") (v "0.1.5") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "gmp-mpfr") (r "^0.4") (d #t) (k 0)))) (h "0biv76srbck0zrd55h7zwy7jzggc3rd25nz2dsw0xzkscqaxah7w")))

(define-public crate-rox-0.1.6 (c (n "rox") (v "0.1.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "rugflo") (r "^0.2") (k 0)) (d (n "rugint") (r "^0.2") (k 0)) (d (n "rugrat") (r "^0.2") (d #t) (k 0)))) (h "1hnzwmkvj9kna41djbyg3avy44xsng6vl0x1y3ss3f3gd3v1js4s")))

