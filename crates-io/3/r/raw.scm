(define-module (crates-io #{3}# r raw) #:use-module (crates-io))

(define-public crate-raw-0.0.3 (c (n "raw") (v "0.0.3") (h "04wf13f76l1i7h78f73fh9arcp8bwyc2vwkb0886fdqyvhwqgp6h")))

(define-public crate-raw-0.0.4 (c (n "raw") (v "0.0.4") (h "0wz8s7gkpzayilxdvdrdc8mcvqr9j2skc997hyzrhw8waak83hmh")))

(define-public crate-raw-0.0.5 (c (n "raw") (v "0.0.5") (h "1pnagl4fdywxw260dqa1hq46cpffy4na4y8cwxna6a1jl5iqsz13")))

(define-public crate-raw-0.0.6 (c (n "raw") (v "0.0.6") (h "066ldhy3lzif9ms4axhz02ww3p36paxf6h7kfxr4a8bnpbs634p4")))

