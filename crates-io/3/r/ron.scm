(define-module (crates-io #{3}# r ron) #:use-module (crates-io))

(define-public crate-ron-0.0.1 (c (n "ron") (v "0.0.1") (d (list (d (n "pom") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0im3wjw8as3pkkc2cm4pw22d24nsswvqmd417x0vq3acbx3n6wi5")))

(define-public crate-ron-0.1.0 (c (n "ron") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1jf9l2f9qwag1k6c1kfc3jac4nfx1058jhb9csiffb1brln5s9pj")))

(define-public crate-ron-0.1.1 (c (n "ron") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0qrjglams4d81hlyyyq0bvdrngypipp5vmk4psaiq3ml6j3c33nc")))

(define-public crate-ron-0.1.2 (c (n "ron") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "028abswai378iqh3z31l5dsxvj9vc0kpab33r4dandp4ps1c1cn4")))

(define-public crate-ron-0.1.3 (c (n "ron") (v "0.1.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1i287rdkwid1j03disb117pwh1b4lvp18dl46wygag0lznwmqdbc")))

(define-public crate-ron-0.1.4 (c (n "ron") (v "0.1.4") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0mifrbz5jwnqdjc5l4sakdb5g1p12x9njpcavpbhn8arfnmmi05k")))

(define-public crate-ron-0.1.5 (c (n "ron") (v "0.1.5") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1yrcnwxwshvjx8gyli29nvkzmzxyl78si6mndnw2873q1d8kpn35")))

(define-public crate-ron-0.1.6 (c (n "ron") (v "0.1.6") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0ivv13263m7jwqf8qjcl1hagpyqyb7lysh6hn6nach812rirkfwi")))

(define-public crate-ron-0.1.7 (c (n "ron") (v "0.1.7") (d (list (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "16f3xcbgx1jbyr3jah0b2wi02299vq8rnxnwknmjb4gn0ymgw1ns")))

(define-public crate-ron-0.2.0 (c (n "ron") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1d4fi5x6mj0zgd27ml5xbvcz7b7fyxkwczgqmxkh8ll2fdnr3nq5")))

(define-public crate-ron-0.2.1 (c (n "ron") (v "0.2.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0i33j02x4s9116imbxlzg4qld2fjmasvx2cjwz1cj2ns11sysagm")))

(define-public crate-ron-0.2.2 (c (n "ron") (v "0.2.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1r3fafrby20vqkaj107l4zqdy2034pkl30d9kn7kjfgfz44pykkj")))

(define-public crate-ron-0.3.0 (c (n "ron") (v "0.3.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0yy4k9ayg3acdchd1g8pdxs8mgb0pdpf96lmy5px84c5lfvi3ym9")))

(define-public crate-ron-0.4.0 (c (n "ron") (v "0.4.0") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "03xfc2lxg736bqx3hky0qgyng4g2m2qb971z3w6lwyi4m7c7g1n4")))

(define-public crate-ron-0.4.1 (c (n "ron") (v "0.4.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1mrqdgw3w0yypg24jyq9mphp4zr9lr0ks7yam82m4n34x6njijyr")))

(define-public crate-ron-0.4.2 (c (n "ron") (v "0.4.2") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "13ypx80ac1minrmn9w9sgnbxlknwiv7qhx5n50azh0s484j2mx8p")))

(define-public crate-ron-0.5.0 (c (n "ron") (v "0.5.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1zzn74lg18gqvw2n618wi447qbkx7g2i296nfpv0spnnca12j225")))

(define-public crate-ron-0.5.1 (c (n "ron") (v "0.5.1") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1mb2bavvp8jg5wx0kx9n45anrsbjwhjzddim987bjaa11hg45kif")))

(define-public crate-ron-0.6.0 (c (n "ron") (v "0.6.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "13wgh5izxzrxkhpq39km4zklqr7br4fla77rdyhvbwyv47rn04m9")))

(define-public crate-ron-0.6.1 (c (n "ron") (v "0.6.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0d26jdb783dlkfa3an57x276c5f3zlh9lzqw59g4p2icwb79979a")))

(define-public crate-ron-0.6.2 (c (n "ron") (v "0.6.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1r6giq99i6yrb19cmj5p601k2ppxjjlskdy353mb50xvny0819gq")))

(define-public crate-ron-0.6.3 (c (n "ron") (v "0.6.3") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1mfnpwhr2k49qrivjlsa6sxhsmd73apzlnx3b0sc9q9l34npzchh") (y #t)))

(define-public crate-ron-0.6.4 (c (n "ron") (v "0.6.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "07vzhbrnimz1lij0f280y624j4yzipn2404jmygs24mp7xhshkh6")))

(define-public crate-ron-0.6.5 (c (n "ron") (v "0.6.5") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1ml6xh4j02bib3lwcqhp49kgwz39v9s18ipkkjj06s8i6sl5l025")))

(define-public crate-ron-0.6.6 (c (n "ron") (v "0.6.6") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "196sbfcpjg5jhqjbk0a16jlvjz2gkjb4kycfginfzgmifzqqs0c6")))

(define-public crate-ron-0.7.0 (c (n "ron") (v "0.7.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0y6n0cpgkv9cnj411ipk86gvwrhxs1hb64m5hrwcjfp4mp51x1hv")))

(define-public crate-ron-0.7.1 (c (n "ron") (v "0.7.1") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "option_set") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "06iz51r6pyi197jjpfddq8h8884y85myaswfan07cnqylqwkj1w8")))

(define-public crate-ron-0.8.0 (c (n "ron") (v "0.8.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (f (quote ("serde-1"))) (o #t) (d #t) (k 0)) (d (n "option_set") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.60") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1zvb2gxn4vv24swwp8a1l9fg5p960w9f9zd9ny05rd8w7c2m22ih") (f (quote (("integer128") ("default")))) (r "1.56.0")))

(define-public crate-ron-0.8.1 (c (n "ron") (v "0.8.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bitflags") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "option_set") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "154w53s895yxdfg7rn87c6f6x4yncc535x1x31zpcj7p0pzpw7xr") (f (quote (("integer128") ("default")))) (r "1.64.0")))

(define-public crate-ron-0.9.0-alpha.0 (c (n "ron") (v "0.9.0-alpha.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bitflags") (r "^2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "bytes") (r "^1.3") (f (quote ("serde"))) (d #t) (k 2)) (d (n "indexmap") (r "^2.0") (f (quote ("serde"))) (o #t) (d #t) (k 0)) (d (n "option_set") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.60") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "typetag") (r "^0.2") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0") (d #t) (k 0)))) (h "0smy36ybdrmg7jlmyqrwd8nwcm6xklwc5w3lg84k9b0ccj9xh2vc") (f (quote (("integer128") ("default")))) (r "1.64.0")))

