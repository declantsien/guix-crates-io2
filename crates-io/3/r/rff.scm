(define-module (crates-io #{3}# r rff) #:use-module (crates-io))

(define-public crate-rff-0.3.0 (c (n "rff") (v "0.3.0") (d (list (d (n "clap") (r "^2.30.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.0") (d #t) (k 0)))) (h "0ilxm7l8hly2qxqq73fgir4lajq3zhgacrv7gy43y9fn0xh65rkc")))

