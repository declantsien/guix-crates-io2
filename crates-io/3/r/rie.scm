(define-module (crates-io #{3}# r rie) #:use-module (crates-io))

(define-public crate-rie-0.1.0 (c (n "rie") (v "0.1.0") (d (list (d (n "rustyline") (r "^1.0.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0xkvc87cygjjlcr85r8i0x75zrhavmg8nwgs7qdw6x6z8i06lc1d")))

