(define-module (crates-io #{3}# r rdx) #:use-module (crates-io))

(define-public crate-rdx-0.0.1 (c (n "rdx") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.4") (d #t) (k 2)))) (h "0gym64wm8cdml1ns8fgbj3qklwlkr9yk07np9i11p4c7bb2cbq8r")))

(define-public crate-rdx-0.0.2 (c (n "rdx") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.4") (d #t) (k 2)))) (h "0l97mll8470vakn9jp3iqdvrx2h7m8wpgsx7d3zqy05lp02yi3gm")))

(define-public crate-rdx-0.0.3 (c (n "rdx") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.4") (d #t) (k 2)))) (h "01fq8lzlk7wbh0qwknbfdcpgw777jv5dchgcrmsnqxp44zprgsvn")))

(define-public crate-rdx-0.0.4 (c (n "rdx") (v "0.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.4") (d #t) (k 2)))) (h "1j0h01x5l33cjqkb2ik284j9v49bagpbn67788q981padvh3iia5") (f (quote (("std") ("default" "std"))))))

(define-public crate-rdx-0.0.5 (c (n "rdx") (v "0.0.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.4") (d #t) (k 2)))) (h "0ryqqps64x8ksdlgqv231kp16xsp9w1ziz3d1agv6bfxdwphm58c") (f (quote (("std") ("default" "std"))))))

(define-public crate-rdx-0.0.6 (c (n "rdx") (v "0.0.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.4") (d #t) (k 2)))) (h "1jklk26iiznfw4776v4866p1yhl740fjggyv9pyhkqdan7nkmpag") (f (quote (("std") ("default" "std"))))))

(define-public crate-rdx-0.0.7 (c (n "rdx") (v "0.0.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xoshiro") (r "^0.4") (d #t) (k 2)))) (h "1s7wkkm10ilf74i01c54517vc5dkvaw9j8cfjhs5bhn7s0j3igch") (f (quote (("std") ("default" "std"))))))

