(define-module (crates-io #{3}# r rmb) #:use-module (crates-io))

(define-public crate-rmb-0.0.1 (c (n "rmb") (v "0.0.1") (d (list (d (n "clap") (r "^2.29.4") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "135p4klhncf6yiym1df5ardclhnn10s9ak1whm7xwcs7khmr9ffc")))

(define-public crate-rmb-0.0.2 (c (n "rmb") (v "0.0.2") (d (list (d (n "clap") (r "^2.29.4") (f (quote ("suggestions" "color"))) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.13") (d #t) (k 0)))) (h "1x9p25qhilvdlfaxy1m5apndvx9a76rhy9zm1rglf458jfd025y1")))

