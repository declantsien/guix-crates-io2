(define-module (crates-io #{3}# r rgz) #:use-module (crates-io))

(define-public crate-rgz-0.1.0 (c (n "rgz") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "prost") (r "^0.11.9") (d #t) (k 0)) (d (n "prost-types") (r "^0.11.9") (d #t) (k 0)) (d (n "rgz_msgs") (r "^0.1.0") (d #t) (k 0)) (d (n "rgz_transport") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 0)))) (h "09a7b1dyj9624vkwzfrr9bvhrp6lsqg4qlmif84n1h018ya1pqr1")))

