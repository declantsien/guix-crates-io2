(define-module (crates-io #{3}# r rge) #:use-module (crates-io))

(define-public crate-rge-0.1.0 (c (n "rge") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "noodles") (r "^0.27.0") (f (quote ("fasta" "core"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1agd3x1zjp97acx0kkzb83xqbj76jj4xmqm9pm9pb00yrai025lw")))

(define-public crate-rge-0.2.0 (c (n "rge") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "clap") (r "^4.0.11") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "noodles") (r "^0.27.0") (f (quote ("fasta" "core"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "083j4mcwdwjlhfb7dq6ysgjafvfx0y7jhkk04dx5h9dsbqpf33dc")))

