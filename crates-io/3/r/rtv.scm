(define-module (crates-io #{3}# r rtv) #:use-module (crates-io))

(define-public crate-rtv-0.1.2 (c (n "rtv") (v "0.1.2") (h "0ir1gks2j7la4iypjyrqbzfn8bcf2sz7ackl82xvydkyf457n53f")))

(define-public crate-rtv-0.2.0 (c (n "rtv") (v "0.2.0") (h "0ql8xvfgmh7rql089i3xls5m418jryj9h0n3hrbb3pmwh8bz1lg8")))

(define-public crate-rtv-0.2.1 (c (n "rtv") (v "0.2.1") (h "0sksfqvfawh8qvbg70rgkh2zw65m0d94z9sf265n48s1bjrkprw4")))

(define-public crate-rtv-1.0.0 (c (n "rtv") (v "1.0.0") (h "1kxzzybn2c4cwwwrzyiwrw8zjllw3nhv2inxk0brd24wd88ahgby")))

(define-public crate-rtv-1.1.0 (c (n "rtv") (v "1.1.0") (h "17c2n1461iql7h56ajdwdldwva6v2b6s81020k22mqgybabylm0z")))

(define-public crate-rtv-1.2.0 (c (n "rtv") (v "1.2.0") (h "11gyniqb2qzdy2ak14vc2gv8wwjap315ykjf9xfpvip6wzp4nn9m")))

(define-public crate-rtv-2.0.0 (c (n "rtv") (v "2.0.0") (d (list (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "mio") (r "^0.8.8") (f (quote ("net" "os-poll"))) (d #t) (k 0)) (d (n "rustls") (r "^0.21.1") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "^0.23.1") (o #t) (d #t) (k 0)))) (h "125b5asl9yi37l7gh3bck2wk0v3sng8d4bm8b5qzy0v8hrq6grpv") (f (quote (("tls" "rustls" "webpki-roots") ("default" "tls"))))))

(define-public crate-rtv-2.0.1 (c (n "rtv") (v "2.0.1") (d (list (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "mio") (r "0.8.*") (f (quote ("net" "os-poll"))) (d #t) (k 0)) (d (n "rustls") (r "0.21.*") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "0.23.*") (o #t) (d #t) (k 0)))) (h "0wqa79k9f7s4wd2bhw3zsjrdkslz8gqxgnv97b7imbc7m0l545qj") (f (quote (("tls" "rustls" "webpki-roots") ("default" "tls"))))))

(define-public crate-rtv-3.0.0 (c (n "rtv") (v "3.0.0") (d (list (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "httparse") (r "^1.8.0") (d #t) (k 0)) (d (n "mio") (r "0.8.*") (f (quote ("net" "os-poll"))) (d #t) (k 0)) (d (n "rustls") (r "0.21.*") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "0.23.*") (o #t) (d #t) (k 0)))) (h "093561py8lw782cz85ngn2xkpsfp9ab5116n9mg81rmc2j626s0w") (f (quote (("tls" "rustls" "webpki-roots") ("default" "tls"))))))

(define-public crate-rtv-4.0.0 (c (n "rtv") (v "4.0.0") (d (list (d (n "chunked_transfer") (r "1.5.*") (d #t) (k 0)) (d (n "dns-parser") (r "0.8.*") (d #t) (k 0)) (d (n "extreme") (r "6.*") (d #t) (k 2)) (d (n "futures-io") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "2.2.*") (o #t) (d #t) (k 0)) (d (n "httparse") (r "1.8.*") (d #t) (k 0)) (d (n "mio") (r "0.8.*") (f (quote ("net" "os-poll" "os-ext"))) (d #t) (k 0)) (d (n "rustls") (r "0.21.*") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "0.23.*") (o #t) (d #t) (k 0)))) (h "1nvgs52kdwqvzzq7jkn7mns8m28v7c8azxawca5n8whq4zylhrpd") (f (quote (("tls" "rustls" "webpki-roots") ("default" "tls" "async") ("async" "futures-lite" "futures-io"))))))

(define-public crate-rtv-4.0.1 (c (n "rtv") (v "4.0.1") (d (list (d (n "chunked_transfer") (r "1.5.*") (d #t) (k 0)) (d (n "dns-parser") (r "0.8.*") (d #t) (k 0)) (d (n "extreme") (r "6.*") (d #t) (k 2)) (d (n "futures-io") (r "0.3.*") (o #t) (d #t) (k 0)) (d (n "futures-lite") (r "2.2.*") (o #t) (d #t) (k 0)) (d (n "httparse") (r "1.8.*") (d #t) (k 0)) (d (n "mio") (r "0.8.*") (f (quote ("net" "os-poll" "os-ext"))) (d #t) (k 0)) (d (n "rustls") (r "0.21.*") (o #t) (d #t) (k 0)) (d (n "webpki-roots") (r "0.23.*") (o #t) (d #t) (k 0)))) (h "1n8v315w3dlhb5qzalkd18jmjziil141saqzmk1mm7ak3n5sxwp8") (f (quote (("tls" "rustls" "webpki-roots") ("default" "tls" "async") ("async" "futures-lite" "futures-io"))))))

