(define-module (crates-io #{3}# r rjo) #:use-module (crates-io))

(define-public crate-rjo-0.1.3 (c (n "rjo") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "1zmr5c0n3z69fvghyb2p9zvmvr06nm1kl0ins5xdn8v214dkiqzd")))

(define-public crate-rjo-0.1.4 (c (n "rjo") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "17c9hxpvkifn7q90rj36hhbds051bsv7fvnvyld1vxajmqypyd9w")))

(define-public crate-rjo-0.1.5 (c (n "rjo") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "0iq1aj1jq7jz3v2238gikbni7cj8qsflja9m73vdd0jjm6xc4icz")))

(define-public crate-rjo-0.1.6 (c (n "rjo") (v "0.1.6") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)))) (h "0xgw45qg58id3xcc2zxglgsjpvpg03ycdngg84mc8xkwqgh16fyq")))

(define-public crate-rjo-0.2.0 (c (n "rjo") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "syntect") (r "^3.2.0") (d #t) (k 0)))) (h "1qy03qjsai3aybs96d3zvkcy53df741qzzhw9x3dhxbvdm19d4p6")))

(define-public crate-rjo-0.2.1 (c (n "rjo") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "syntect") (r "^3.2.0") (d #t) (k 0)))) (h "0py4cazprg0pyalgd40lwjqipx5snsnf2xc7dbl1bnwzhr2hs359")))

(define-public crate-rjo-0.2.2 (c (n "rjo") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "atty") (r "^0.2.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "syntect") (r "^3.2.0") (d #t) (k 0)))) (h "0avk9qr3c86mbyvn8npdp0l64axqqk3h4m88lyz16hfmrhv2pn58")))

(define-public crate-rjo-0.2.3 (c (n "rjo") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (t "cfg(windows)") (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "syntect") (r "^3.3") (d #t) (k 0)))) (h "1z1l5d60bvwy9fmfgpilzd50xd5z52mfvgrkhl557j9gjncda5ga")))

(define-public crate-rjo-0.3.0 (c (n "rjo") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (t "cfg(windows)") (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "syntect") (r "^4.4") (d #t) (k 0)))) (h "1jxl0cj3dgr1jj06mgk7fbpvhw9vb7jxmg8ihaf9s0wx7fapxrgx")))

