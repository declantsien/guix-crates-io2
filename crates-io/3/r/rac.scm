(define-module (crates-io #{3}# r rac) #:use-module (crates-io))

(define-public crate-rac-1.0.0 (c (n "rac") (v "1.0.0") (d (list (d (n "curve25519-dalek") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0xrd3q0yck4xsvm69irvn6knrjg80l7vqczdj3975dvny7ysiazk")))

(define-public crate-rac-1.0.1 (c (n "rac") (v "1.0.1") (d (list (d (n "curve25519-dalek") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17") (o #t) (d #t) (k 0)))) (h "0qxff4x286f49spbhjjy4v8nf5w9hcdqac79mmwk3qgc2drmj2wa")))

(define-public crate-rac-1.1.0 (c (n "rac") (v "1.1.0") (d (list (d (n "curve25519-dalek") (r "^2.1") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "secp256k1") (r "^0.17") (o #t) (d #t) (k 0)))) (h "04qdarb0dyd82z3hyzvjlqzb8y211adb22fa8vpcc519k50g2ghl")))

(define-public crate-rac-1.3.0 (c (n "rac") (v "1.3.0") (d (list (d (n "curve25519-dalek") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "secp256k1") (r "^0.17") (o #t) (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "0vxsqa20mhrqsw40zkpf1af32ijwad6zh1pz2hvnp0scdqj1nxkd")))

(define-public crate-rac-1.3.1 (c (n "rac") (v "1.3.1") (d (list (d (n "curve25519-dalek") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "secp256k1") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "1f0gwayrylzpdvz761k7232bhr6fyra4llyi1cf6pij4gl8p58ds")))

(define-public crate-rac-1.3.2 (c (n "rac") (v "1.3.2") (d (list (d (n "curve25519-dalek") (r "^3.0") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "secp256k1") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "1ijji0f2lmiix2wvjnnvfs82a7m45hpg9y0sqslzkzlr56vx1n48")))

(define-public crate-rac-1.3.3 (c (n "rac") (v "1.3.3") (d (list (d (n "curve25519-dalek") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "secp256k1") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "0bgq0h9av86j1ynhhzki5rypmj57vmcq2x6x1vs9hf66vj3738wl")))

(define-public crate-rac-1.3.4 (c (n "rac") (v "1.3.4") (d (list (d (n "curve25519-dalek") (r "^3.1") (o #t) (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "secp256k1") (r "^0.20") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "0y5r8jfjbik93hh55agx775wy8czpna06hmw88rih4gl07sx38av")))

