(define-module (crates-io #{3}# r ram) #:use-module (crates-io))

(define-public crate-ram-4.0.0 (c (n "ram") (v "4.0.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "14mwxsrmx3bnzq4dzrnjdnwjx20xcgyagg0343fp1mspn5a1y4gm") (y #t)))

(define-public crate-ram-5.0.0 (c (n "ram") (v "5.0.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1a60sspdsglrsqf223g4b0c108iqb6lc6538xc6b0qkn3l08zniz") (y #t)))

(define-public crate-ram-6.0.0 (c (n "ram") (v "6.0.0") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0rdhbmir2b1iaz1lm9rbhhw1x8zajhp5g95373fk4wlhyah227y4") (y #t)))

(define-public crate-ram-6.0.1 (c (n "ram") (v "6.0.1") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "074j78szjzdxa6azv4vxg1fc037c6840n8675k6nvfv74vqa36qs") (y #t)))

(define-public crate-ram-7.0.0 (c (n "ram") (v "7.0.0") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0nnn3ipw8ap6d41srkds8krhl6cff8mj15md6fnziajsnkb0v6kj") (y #t)))

(define-public crate-ram-7.0.1 (c (n "ram") (v "7.0.1") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "1d0c59jj7lsx0m3ax9g5m8nxifyb0amlfmpxi9q5hj0ca01v51j4") (y #t)))

(define-public crate-ram-7.0.2 (c (n "ram") (v "7.0.2") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0jhhwsairsy1lkxv7mfnwcsg22szvhq3101r6yljyijv0jxcqysp") (y #t)))

(define-public crate-ram-7.0.3 (c (n "ram") (v "7.0.3") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "0a8s8p7khlxjym10g1a5rcangmj5lm1rnw5nbim8iwfw6cwmi97k") (y #t)))

(define-public crate-ram-8.0.0 (c (n "ram") (v "8.0.0") (d (list (d (n "regex") (r "^1.1") (d #t) (k 0)))) (h "15i498kvkx0c8c3z49czk6xjm5y004qmxqyhnjg8h1k0jirmc4sb")))

