(define-module (crates-io #{3}# r rfa) #:use-module (crates-io))

(define-public crate-rfa-0.1.0 (c (n "rfa") (v "0.1.0") (d (list (d (n "erfa-sys") (r "^0.2.0") (d #t) (k 2)))) (h "05qlxaka5p7v805jlsnr4qfg0p52814rqqcn9adwyjjfaw93wpb5") (y #t)))

(define-public crate-rfa-0.1.1 (c (n "rfa") (v "0.1.1") (d (list (d (n "erfa-sys") (r "^0.2.0") (d #t) (k 2)))) (h "0yb3wijjgazbm15h9lngwqnwl1l00b6x39qi1bihmabl8l67nh9p")))

(define-public crate-rfa-0.2.0 (c (n "rfa") (v "0.2.0") (d (list (d (n "erfa-sys") (r "^0.2.0") (d #t) (k 2)))) (h "05lp5g41q5q4mv9gqqnpr9q23kr2hhcs5lghs7xi8irr4ayppn79")))

(define-public crate-rfa-0.3.0 (c (n "rfa") (v "0.3.0") (d (list (d (n "erfa-sys") (r "^0.2.0") (d #t) (k 2)))) (h "0ljhda8gisl3zxfwx61357zbygk9j6w7g5w8jm7gimldzn54ryky")))

(define-public crate-rfa-0.4.0 (c (n "rfa") (v "0.4.0") (d (list (d (n "erfa-sys") (r "^0.2.0") (d #t) (k 2)))) (h "13g1bs388rwh7k7f8g7vp3r9di2g99f4cz467qqmwkys4qzlqsp3")))

(define-public crate-rfa-0.5.0 (c (n "rfa") (v "0.5.0") (d (list (d (n "erfa-sys") (r "^0.2.0") (d #t) (k 2)))) (h "1zy05ka4mcprvknyvp4jzf4c3pg39y27k7lr6vxaccbchdnnbl38")))

