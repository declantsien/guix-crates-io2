(define-module (crates-io #{3}# r rtt) #:use-module (crates-io))

(define-public crate-rtt-0.3.3 (c (n "rtt") (v "0.3.3") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0jky6x2s598prq1k28h43lp3s9yng91f7zm2mbcr3cvzg3z4w7qp")))

(define-public crate-rtt-0.4.4 (c (n "rtt") (v "0.4.4") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)))) (h "0y8hbm55hcsrrmg7kdmk1jf3rlkw908izx51r7c5h3zmdnsnq1hm")))

