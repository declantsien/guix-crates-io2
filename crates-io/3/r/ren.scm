(define-module (crates-io #{3}# r ren) #:use-module (crates-io))

(define-public crate-ren-0.0.0 (c (n "ren") (v "0.0.0") (h "0598zfv46ld9xdfac3pr957b5a36hy2xcy65mr5pg1ilc5lf5z2w")))

(define-public crate-ren-0.0.1 (c (n "ren") (v "0.0.1") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glfw") (r "^0.51") (d #t) (k 0)) (d (n "glfw-ext") (r "^0.1") (d #t) (k 0)))) (h "1sgfgcff0a7byhbjncas2rf0crj38a3v0lgfv26r31p2ynlvcq4w")))

(define-public crate-ren-0.0.2 (c (n "ren") (v "0.0.2") (d (list (d (n "gl") (r "^0.14") (d #t) (k 0)) (d (n "glam") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "glfw") (r "^0.51") (d #t) (k 0)) (d (n "glfw-ext") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ah0g30bj1vbqidsg6cx6xq1c7nfkyipfwgzfsxffbz31y2via2w") (f (quote (("default" "glam")))) (s 2) (e (quote (("glam" "dep:glam"))))))

