(define-module (crates-io #{3}# r raf) #:use-module (crates-io))

(define-public crate-raf-0.1.0 (c (n "raf") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.48") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.28") (f (quote ("Window"))) (d #t) (k 0)))) (h "1r0wbm3f2ifhsczd9wnglxaxaabpp8xi7a012ikx3fdxrlywl2cn")))

