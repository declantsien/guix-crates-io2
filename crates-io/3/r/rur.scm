(define-module (crates-io #{3}# r rur) #:use-module (crates-io))

(define-public crate-rur-0.0.1 (c (n "rur") (v "0.0.1") (h "0c24b59z11grsm4qkqyjlbdhh2b4ll8sm3a7c4ixn8g5wb3p4632") (y #t)))

(define-public crate-rur-0.1.0 (c (n "rur") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)))) (h "0gw71nqiwacqlsfqw2zfydkm1bsrb001cizhfmlya7m8jmzahhh7")))

