(define-module (crates-io #{3}# r rvi) #:use-module (crates-io))

(define-public crate-rvi-0.1.0 (c (n "rvi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1834358abk95g2b629x2j7b7cldzii1qq0danmm4z0xl82824fnh")))

(define-public crate-rvi-0.1.1 (c (n "rvi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "026k2qggq85gjxgbrm0ah6agbzm7pgr00qyig5pd147xpdsisqgj")))

(define-public crate-rvi-0.1.2 (c (n "rvi") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0cidrn0n6bwhm5sqn1mxmjfbm5lbwn3hzavn2lrdjwkjb12p2ghf")))

(define-public crate-rvi-0.1.3 (c (n "rvi") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0zj7jzkmz92y1jhib4n2af3k0fjvav5abcwhhn2kwkxqfjn6khlk")))

(define-public crate-rvi-0.1.4 (c (n "rvi") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1ndd3zq6g0253bff6pnsh78dpjhlkblmjz50xdakrv5q4wnaqkzj")))

