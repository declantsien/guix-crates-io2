(define-module (crates-io #{3}# r rts) #:use-module (crates-io))

(define-public crate-rts-0.1.0 (c (n "rts") (v "0.1.0") (h "07g20lr2y5byjd5i702jhwiwxdx0v865nc4wdqw6fsivmrd6041b")))

(define-public crate-rts-0.1.1 (c (n "rts") (v "0.1.1") (d (list (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "rts_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1w36p2wis81ch36y9hs90mjmsjpy55gb039l4fr8y1m5h7pi85zc")))

