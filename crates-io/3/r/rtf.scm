(define-module (crates-io #{3}# r rtf) #:use-module (crates-io))

(define-public crate-rtf-0.6.2 (c (n "rtf") (v "0.6.2") (d (list (d (n "getopts") (r "~0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "~1.3.0") (d #t) (k 0)) (d (n "rtforth") (r "=0.6.2") (d #t) (k 0)) (d (n "rustyline") (r "^4") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)) (d (n "unicode-width") (r "~0.1.5") (d #t) (k 0)))) (h "0cbj4mm5v1mawwgvk87516z80z5g8js02cqb00li817spsbxjds9")))

(define-public crate-rtf-0.6.3 (c (n "rtf") (v "0.6.3") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rtforth") (r "^0.6.3") (d #t) (k 0)) (d (n "rustyline") (r "^4.0") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)))) (h "11v9s5r24dcqnrv9197lcjckgr2k99qdmy3jfqpwz8hf6m9c4y7i")))

(define-public crate-rtf-0.6.4 (c (n "rtf") (v "0.6.4") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rtforth") (r "^0.6.3") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0f8mvyby4s6rxjvyfhpd414frd65pbhd4z5qxzhr8c1n58nnjf15")))

(define-public crate-rtf-0.6.5 (c (n "rtf") (v "0.6.5") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rtforth") (r "^0.6.3") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0775041hypmpwqm6zfq2vgm0cvj3r3jry4ignbczmnpfs0cr1lkb")))

(define-public crate-rtf-0.6.6 (c (n "rtf") (v "0.6.6") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rtforth") (r "^0.6.3") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "01zb8rh03dakk3ndyy2yf0css45h911pbjy6dk5y6i1yf44d62kk")))

(define-public crate-rtf-0.6.7 (c (n "rtf") (v "0.6.7") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rtforth") (r "^0.6.7") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0n2nlq23x9aqpi11yx63acl0hg43wnpqrpm3fga5ld958d436hsx")))

(define-public crate-rtf-0.6.8 (c (n "rtf") (v "0.6.8") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "directories") (r "^4.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rtforth") (r "^0.6.8") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "00jag16zf1hwp169zwmxdbibxn17icr3g5nflws4cnpg0i2mhcyg")))

