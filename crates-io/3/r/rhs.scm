(define-module (crates-io #{3}# r rhs) #:use-module (crates-io))

(define-public crate-rhs-0.1.0 (c (n "rhs") (v "0.1.0") (h "1j2abhzgs4k0hyq5yk1jv9n8918ar5fr760yl6fa0n8c6i0bbgz3") (y #t)))

(define-public crate-rhs-0.0.0 (c (n "rhs") (v "0.0.0") (h "0kcgvg95rzdhjz70jh6vi3l84srj5pd95ldzwrwzm2fp483pl07n") (y #t)))

