(define-module (crates-io #{3}# r rfm) #:use-module (crates-io))

(define-public crate-rfm-0.5.0 (c (n "rfm") (v "0.5.0") (h "01v4sjk3wzgiagp64hvjj53zqb0fh270f54541172db8bynn05qi")))

(define-public crate-rfm-0.5.5 (c (n "rfm") (v "0.5.5") (h "12wqkdjj4l3vbd9pzpr3qnli008gcj4nd44fi5rap0rr4nnzri5r")))

(define-public crate-rfm-0.6.0 (c (n "rfm") (v "0.6.0") (h "1bhjc83yqf5mkgcy2kw8b8li9hc1sia2wm0lslvyvl5vfc981pfc")))

(define-public crate-rfm-0.7.0 (c (n "rfm") (v "0.7.0") (h "0m3x32c5hjkiyizhzdnq6ck3km5pp82ksz8j43z3hbnbvkcgj8av")))

(define-public crate-rfm-0.7.5 (c (n "rfm") (v "0.7.5") (h "0vn5l2v41n98ksq6qqsmdyd3qlhp06ab513qw4pddbxb3b19pqi7")))

(define-public crate-rfm-0.8.0 (c (n "rfm") (v "0.8.0") (h "1bgf2dy400mmjmyjdyzm441g4lwk50za6kfhnmvwjk6n5aw62dby")))

(define-public crate-rfm-1.0.0 (c (n "rfm") (v "1.0.0") (h "0z9fd8wm1pmxp9604jv63zpkxl8rqhvspsgadq7r2q7z07cjmglj")))

(define-public crate-rfm-1.0.2 (c (n "rfm") (v "1.0.2") (h "0w86yxyr4bmcvg8gss4y6i2iq5iz0la3czglibhhcah0hznjwanp")))

