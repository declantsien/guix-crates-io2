(define-module (crates-io #{3}# r res) #:use-module (crates-io))

(define-public crate-res-0.1.0 (c (n "res") (v "0.1.0") (d (list (d (n "adi_storage") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)) (d (n "utem") (r "^0.1.0") (d #t) (k 0)))) (h "1rcigl2mb2k2vrs0s9k31wf2h5nmnkfjc5bqyzfbxbc889bignqz")))

(define-public crate-res-0.2.0 (c (n "res") (v "0.2.0") (d (list (d (n "adi_storage") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "utem") (r "^0.1") (d #t) (k 0)))) (h "0vzahgmycabdmkfvir8r67haij61y4ds3mdzym4jl916ffww0i06")))

(define-public crate-res-0.2.1 (c (n "res") (v "0.2.1") (d (list (d (n "adi_storage") (r "^0.2.1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "utem") (r "^0.1") (d #t) (k 0)))) (h "1zmf06dzvnvy2qhaaz72li0fv1jzc3by5ikcj6xrrc1fncgb1snj")))

(define-public crate-res-0.3.0 (c (n "res") (v "0.3.0") (d (list (d (n "adi_storage") (r "^0.2.1") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "utem") (r "^0.1") (d #t) (k 0)))) (h "1xzxc8mh3hzswsld39xv3n1711812mmwygf1v7dl54w5z8vii8rj")))

(define-public crate-res-0.4.0 (c (n "res") (v "0.4.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "png_pong") (r "^0.0.1") (d #t) (k 0)) (d (n "sheep") (r "^0.2") (f (quote ("amethyst"))) (k 0)))) (h "0hqgajr9wqb11pb0pl98w4g7z188gjxycsh1jp8zwj37x5d8dqhd")))

(define-public crate-res-0.4.1 (c (n "res") (v "0.4.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "png_pong") (r "^0.0.2") (d #t) (k 0)) (d (n "sheep") (r "^0.2") (f (quote ("amethyst"))) (k 0)))) (h "1ip19nias5r92k70y7vznyzzch2yngnb7z0lhngkkva7v4dzys3k")))

(define-public crate-res-0.5.0 (c (n "res") (v "0.5.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "png_pong") (r "^0.0.2") (d #t) (k 0)) (d (n "sheep") (r "^0.3") (k 0)))) (h "1j6r9cgc3vvgprz40z2gfxlpakzfm3vslasql25xy3ak2clhmqnd")))

(define-public crate-res-0.6.0 (c (n "res") (v "0.6.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "png_pong") (r "^0.5") (d #t) (k 0)) (d (n "sheep") (r "^0.3") (k 0)))) (h "06cw1hza0in80haq4wc5jzjcd49hcqb87ara7whh738rmyws5zz4")))

(define-public crate-res-0.6.1 (c (n "res") (v "0.6.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "png_pong") (r "^0.6") (d #t) (k 0)) (d (n "sheep") (r "^0.3") (k 0)))) (h "1gcr68063dv43xvjmwf206yyiizxiji22h8cxwc39cxk6q37q21c")))

