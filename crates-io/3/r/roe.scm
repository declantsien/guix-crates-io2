(define-module (crates-io #{3}# r roe) #:use-module (crates-io))

(define-public crate-roe-0.0.1 (c (n "roe") (v "0.0.1") (d (list (d (n "bstr") (r "^0.2.4") (k 0)) (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "0ld9dfglsf36wwiq4b68kkh729lm8k8jrbhvps6kz5x1xjdhgkqh") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-roe-0.0.2 (c (n "roe") (v "0.0.2") (d (list (d (n "bstr") (r "^0.2.4") (k 0)) (d (n "version-sync") (r "^0.9, >=0.9.2") (d #t) (k 2)))) (h "05m7k1hxq1dvdmibaz3yayngzaji6na880ki7czq61fz2j9f5m6f") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-roe-0.0.3 (c (n "roe") (v "0.0.3") (d (list (d (n "bstr") (r "^0.2.4") (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "13q45iql6d106l72jvplabr2kqb6d9m1vw3cspf9mqxyckpi8hy9") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-roe-0.0.4 (c (n "roe") (v "0.0.4") (d (list (d (n "bstr") (r "^1.0.1") (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "18p5pgxm2hqr0094fxq294h6jzn6x8xfmp91akqi7bmhfkw7zm7v") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-roe-0.0.5 (c (n "roe") (v "0.0.5") (d (list (d (n "bstr") (r "^1.0.1") (k 0)) (d (n "version-sync") (r "^0.9.3") (f (quote ("markdown_deps_updated" "html_root_url_updated"))) (k 2)))) (h "1xrwjdl99s3riy4mhlwil1vx65f5h0p63w2wyqsmxfxnikc034k6") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

