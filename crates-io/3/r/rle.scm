(define-module (crates-io #{3}# r rle) #:use-module (crates-io))

(define-public crate-rle-0.1.0 (c (n "rle") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0nd4ci4fkjhh6pfyyz0rnpc168b2w64mmcrx4sg16ci1k5wsh9sb")))

(define-public crate-rle-0.1.1 (c (n "rle") (v "0.1.1") (d (list (d (n "smallvec") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0cjndnmqq0pwrsxf2xvz1fla43zfn1bml112d9ajwzxw9pchlhvr")))

(define-public crate-rle-0.2.0 (c (n "rle") (v "0.2.0") (d (list (d (n "smallvec") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "094crsywka2az2vy0cmfvnqc4zwc6awfb705b1w1i36raxk75m9g")))

