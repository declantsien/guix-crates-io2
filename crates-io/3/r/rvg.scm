(define-module (crates-io #{3}# r rvg) #:use-module (crates-io))

(define-public crate-rvg-0.0.1 (c (n "rvg") (v "0.0.1") (d (list (d (n "footile") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.10") (d #t) (k 2)) (d (n "usvg") (r "^0.6") (d #t) (k 2)))) (h "11imhi711bjysl4msi8m99sr1i2l5aryjqr290ibsb8n56hmgdia")))

(define-public crate-rvg-0.0.2 (c (n "rvg") (v "0.0.2") (d (list (d (n "footile") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.2") (d #t) (k 0)) (d (n "png") (r "^0.10") (d #t) (k 2)) (d (n "usvg") (r "^0.6") (d #t) (k 2)))) (h "1kq69p3vkgnpmmns4g1law857jmqi6ligk6vaafcdnbdx4skfnc1") (f (quote (("default"))))))

(define-public crate-rvg-0.0.3 (c (n "rvg") (v "0.0.3") (d (list (d (n "footile") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "footile") (r "^0.3") (d #t) (k 2)) (d (n "miniz_oxide") (r "^0.3") (d #t) (k 0)) (d (n "png") (r "^0.10") (d #t) (k 2)) (d (n "usvg") (r "^0.6") (d #t) (k 2)))) (h "0i4awid3zv6p9zr4is07jgkjfdn53gfbppwhyrs4ixif8kr1wnk1") (f (quote (("default"))))))

(define-public crate-rvg-0.1.0 (c (n "rvg") (v "0.1.0") (d (list (d (n "footile") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "png_pong") (r "^0.5") (d #t) (k 2)) (d (n "roxmltree") (r "^0.11") (d #t) (k 2)) (d (n "ruzstd") (r "^0.2") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5") (d #t) (k 2)) (d (n "usvg") (r "^0.9") (d #t) (k 2)) (d (n "zstd") (r "^0.5") (o #t) (d #t) (k 0)))) (h "17xrp1h6af5k7yr22qs3cx39bx50z7jl1rbcd50nc5dwg2fbpi4k") (f (quote (("render" "footile") ("default" "zstd"))))))

(define-public crate-rvg-0.2.0 (c (n "rvg") (v "0.2.0") (d (list (d (n "footile") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "pix") (r "^0.13") (d #t) (k 0)) (d (n "png_pong") (r "^0.5") (d #t) (k 2)) (d (n "roxmltree") (r "^0.11") (d #t) (k 2)) (d (n "ruzstd") (r "^0.2") (d #t) (k 0)) (d (n "svgtypes") (r "^0.5") (d #t) (k 2)) (d (n "usvg") (r "^0.9") (d #t) (k 2)) (d (n "zstd") (r "^0.5") (o #t) (d #t) (k 0)))) (h "195zwwb4whcmrgl9l5yz0blswf9vl0k3h3m1lcf30s51515g19pl") (f (quote (("render" "footile") ("default" "zstd"))))))

