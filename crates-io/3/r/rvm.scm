(define-module (crates-io #{3}# r rvm) #:use-module (crates-io))

(define-public crate-rvm-0.0.0 (c (n "rvm") (v "0.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rvm-lib") (r "^0.0") (d #t) (k 0)))) (h "08spaw7c1z3kkdpjdrya4zvq0pmhlzgi79axc6sx2j6pfd2gxssy")))

(define-public crate-rvm-0.0.2 (c (n "rvm") (v "0.0.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rvm-lib") (r "^0.0") (d #t) (k 0)))) (h "092csvj3ksi2vy4lc40ii7wvlh3hf55hnwky6d68q89jrga1ybdd")))

