(define-module (crates-io #{3}# r rpb) #:use-module (crates-io))

(define-public crate-rpb-0.1.0 (c (n "rpb") (v "0.1.0") (d (list (d (n "enum-iterator") (r "^1.2.0") (d #t) (k 0)) (d (n "int-enum") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1ar9i9m1ig1gnp18ws5bv4ci9lf0iz4ynb3l5rinw00pjynn0s7w")))

(define-public crate-rpb-0.1.1 (c (n "rpb") (v "0.1.1") (d (list (d (n "enum-iterator") (r "^1.2.0") (d #t) (k 0)) (d (n "int-enum") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1h59g9br3sv9kwwraz8xkvf71qbn8i9ak1m1ql2byjfqmqq2f6hz")))

(define-public crate-rpb-0.1.2 (c (n "rpb") (v "0.1.2") (d (list (d (n "enum-iterator") (r "^1.2.0") (d #t) (k 0)) (d (n "int-enum") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1wfx9czwrg7rv0jzmbqv4mmirdb8savmzyiqphcqjmva0rbbk5ay")))

(define-public crate-rpb-0.1.3 (c (n "rpb") (v "0.1.3") (d (list (d (n "enum-iterator") (r "^1.2.0") (d #t) (k 0)) (d (n "int-enum") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "0ldag1kv6f1wqcpsy0p7pf36rmy9gvl3z3qnqgrwmz2dmrl4346q")))

(define-public crate-rpb-0.1.4 (c (n "rpb") (v "0.1.4") (d (list (d (n "enum-iterator") (r "^1.2.0") (d #t) (k 0)) (d (n "int-enum") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "1mmifzdg0lks00na5w3wj5wz4hv9xd0mn961ml5fi3i3s04nf6ky")))

(define-public crate-rpb-0.1.5 (c (n "rpb") (v "0.1.5") (d (list (d (n "enum-iterator") (r "^1.2.0") (d #t) (k 0)) (d (n "int-enum") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "02hvbcsp16557p8wgljs2sw4y8cncj8rgnhdrj17r3smwrj3da1a")))

