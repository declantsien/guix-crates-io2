(define-module (crates-io #{3}# r rc5) #:use-module (crates-io))

(define-public crate-rc5-0.0.0 (c (n "rc5") (v "0.0.0") (h "0v4flb27ij247lg6sv4l68gn7wv0inkiyr4b490mi7k81pwkvl7z")))

(define-public crate-rc5-0.0.1 (c (n "rc5") (v "0.0.1") (d (list (d (n "cipher") (r "^0.4.3") (f (quote ("zeroize"))) (d #t) (k 0)) (d (n "cipher") (r "^0.4.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "0gszk4bz8vb2pdjsvhr1khvhsa222d2p564mvn9k256bd239a5nl") (f (quote (("zeroize")))) (r "1.56")))

