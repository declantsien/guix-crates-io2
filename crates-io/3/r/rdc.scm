(define-module (crates-io #{3}# r rdc) #:use-module (crates-io))

(define-public crate-rdc-0.1.0 (c (n "rdc") (v "0.1.0") (d (list (d (n "genco") (r "^0.17.3") (d #t) (k 0)) (d (n "rdc-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)) (d (n "uuid") (r "^1.2.2") (f (quote ("v4"))) (d #t) (k 2)))) (h "10a2712y6d34h3x8vb3rn4fjcdabfc6rz3xwgmpc949ivla9hmbl") (f (quote (("default"))))))

