(define-module (crates-io #{3}# r roo) #:use-module (crates-io))

(define-public crate-roo-0.1.0 (c (n "roo") (v "0.1.0") (h "1y77zvhwgkf9j1vwpxsvaqn8d63d2yq1cmair11mv10s7pz78z2j")))

(define-public crate-roo-0.1.1 (c (n "roo") (v "0.1.1") (h "09dbw2nvdhwnsggja53pa4dfgw1pfkdjjizwzimsnd4q8wfhxlx3")))

