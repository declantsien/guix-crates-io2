(define-module (crates-io #{3}# r rtq) #:use-module (crates-io))

(define-public crate-rtq-0.1.0 (c (n "rtq") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.66") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.20") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.0") (d #t) (k 0)))) (h "1mvkr110x62f4ihxhj79bk9qi162dxwdjm5hcb4iqp8knmrfnvpd")))

