(define-module (crates-io #{3}# r rmc) #:use-module (crates-io))

(define-public crate-rmc-1.1.1 (c (n "rmc") (v "1.1.1") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (d #t) (k 0)) (d (n "file_diff") (r "^1.0.0") (d #t) (k 0)))) (h "0rn338q97kw2m4jqcwqbjvvykwdyy4dgxxbqdzd08693wkqksh39")))

