(define-module (crates-io #{3}# r rrd) #:use-module (crates-io))

(define-public crate-rrd-0.1.0 (c (n "rrd") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(macos)") (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(unix)") (k 1)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0zrff2my0biy02vm4r55wwfx6gvklmcp1v9rvsqmdbr6h6v4qify")))

(define-public crate-rrd-0.1.1 (c (n "rrd") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(macos)") (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (t "cfg(unix)") (k 1)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0r2hn3yxaxmhav4627sbl7m0brigmix45nkq6w17xp8wgbaj8qbm")))

