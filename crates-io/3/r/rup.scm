(define-module (crates-io #{3}# r rup) #:use-module (crates-io))

(define-public crate-rup-0.3.0 (c (n "rup") (v "0.3.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "08560aczp2wk8hgi9miqnnkl7287jz2pddasrfa16m87gnzl9vsi")))

(define-public crate-rup-0.4.0 (c (n "rup") (v "0.4.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread" "time" "signal" "sync" "net" "io-util"))) (d #t) (k 0)))) (h "08j579if9acrx0g6kzmzvwgg7z8fdpfb18vvy8fs3n3dhrihrhsm")))

(define-public crate-rup-0.4.1 (c (n "rup") (v "0.4.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt" "rt-multi-thread" "time" "signal" "sync" "net" "io-util"))) (d #t) (k 0)))) (h "0h4n67n44nwp1j145prkb28ds5wvx7mkd440cbd632acwsbfs0rl")))

