(define-module (crates-io #{3}# r rxp) #:use-module (crates-io))

(define-public crate-rxp-0.1.0 (c (n "rxp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "0i29r4107hxk3r3yd1ybc63lndkr1pb0ndd3vfzcdkfl2vwrwpp5") (y #t)))

(define-public crate-rxp-0.1.1 (c (n "rxp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "15hbj7z17a7mjzwchypzvdaw2m5bq0l6z935g474vyi5hy9akjx1")))

(define-public crate-rxp-0.2.0 (c (n "rxp") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "06qgg9wdxb559nxhs7sk1faqm4c0w064l387csny1cnypasqy68n")))

