(define-module (crates-io #{3}# r rbc) #:use-module (crates-io))

(define-public crate-rbc-0.1.0 (c (n "rbc") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0l6f73l66qfrfh6vh08xbb5hr22y1bpnkd114dvhq2s7f5viiwzp")))

(define-public crate-rbc-0.1.1 (c (n "rbc") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0n2nw1mdza43s2wgiqr43c92cm64588ri2jmfnqm05za67ivrshn")))

(define-public crate-rbc-0.1.2 (c (n "rbc") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1ylcdpx0zvibl8ynqg7vw8iprh0mvyhwi0rdm48l2vxahs19w7g3")))

