(define-module (crates-io #{3}# r rdk) #:use-module (crates-io))

(define-public crate-rdk-0.1.1 (c (n "rdk") (v "0.1.1") (d (list (d (n "bitvec") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "rdk-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1a4xwiyzy9y9lgnp5943yv88bprassr892jphd76zkvj4p0avaxy") (f (quote (("default" "rdk-sys/conda"))))))

