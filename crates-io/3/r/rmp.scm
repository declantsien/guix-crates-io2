(define-module (crates-io #{3}# r rmp) #:use-module (crates-io))

(define-public crate-rmp-0.1.0 (c (n "rmp") (v "0.1.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1n3hl8nnd0fivjkp09y24j0bja2n6b789kh3mg5g6bvx0mpspxdh") (y #t)))

(define-public crate-rmp-0.1.1 (c (n "rmp") (v "0.1.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0kqlrp29bvjzn95v4all019dbb20j3ar5xf6g6i3j3zar7cdz7ri") (y #t)))

(define-public crate-rmp-0.2.0 (c (n "rmp") (v "0.2.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1mpr1bd29nyh4961b9w0n45kp0dr92n5mag5aq2ycfm90cz98v9i") (y #t)))

(define-public crate-rmp-0.2.1 (c (n "rmp") (v "0.2.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1d0m6fyyp7fc97blmr886q5bbx2sbxms6zidgqfdsmx1pgp60wv5") (y #t)))

(define-public crate-rmp-0.2.2 (c (n "rmp") (v "0.2.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1d2ca0f68zxa82kyfc0ksw36dc6mzf05sxmjj8icdl5yliadiq8p") (y #t)))

(define-public crate-rmp-0.3.0 (c (n "rmp") (v "0.3.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "05xz0521gij9c460xf0svjfi60ik77r9m23g8q7snx1i58mca0dh") (y #t)))

(define-public crate-rmp-0.3.1 (c (n "rmp") (v "0.3.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13jqipwkh5jjamkyldggqm3rmz0hi2qrx3iaw76jcln9zkdm1dn6") (y #t)))

(define-public crate-rmp-0.3.2 (c (n "rmp") (v "0.3.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "07j83b1qndr5rc46n64ma42bfvhrgmggvln66gbx3i79dsw0ws4c") (y #t)))

(define-public crate-rmp-0.4.0 (c (n "rmp") (v "0.4.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0apzsmg553db13nvg6dlcl6rqxh61fw4zzsl9pkl3cmlmzcmi1qk") (y #t)))

(define-public crate-rmp-0.5.0 (c (n "rmp") (v "0.5.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0cz4f1k566rw4fhls3jphm66cn1hx9r5si3dw95llhmnf2b4h936") (y #t)))

(define-public crate-rmp-0.5.1 (c (n "rmp") (v "0.5.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1n2ymhxrzkvqb1nzkci9c0kv5f77hiqj49m1fhc4ln7dv6b5qay9") (y #t)))

(define-public crate-rmp-0.6.0 (c (n "rmp") (v "0.6.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "*") (d #t) (k 0)) (d (n "serde_macros") (r "*") (o #t) (d #t) (k 0)))) (h "1rb4n0mzz2vfivma31j7fcyyzhg56vi2vn84a3j4y78g6c3apzdh") (y #t)))

(define-public crate-rmp-0.7.0 (c (n "rmp") (v "0.7.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0n2wnbjs99zkjqzpg2lhx4rrvf1brj874vz8l9pd6fy7r850gd7p") (y #t)))

(define-public crate-rmp-0.7.1 (c (n "rmp") (v "0.7.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0f9pznwwls2zs6yjj6cz6zx8nsjwpknrdvr90qp7vydmzd1mcsbg") (y #t)))

(define-public crate-rmp-0.7.2 (c (n "rmp") (v "0.7.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "00sqp9c7abp5xw9l9qzsh6gwm9yp0bl5k0pg0dn221mshrmipq22") (y #t)))

(define-public crate-rmp-0.7.3 (c (n "rmp") (v "0.7.3") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "1nbhr7faawl2fk64625100mv0s4wvlqgq3511gjhhv4k2d4769yn") (y #t)))

(define-public crate-rmp-0.7.4 (c (n "rmp") (v "0.7.4") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "1x4jn6igk11qlpmbxg4j5bsf6w09667jff97pzbv5cwm17dxgzq3") (y #t)))

(define-public crate-rmp-0.7.5 (c (n "rmp") (v "0.7.5") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "0iqlw0768kykvvk5zgx2jdiggil27lv6gmr4hzbdy0yq8p66inm2") (y #t)))

(define-public crate-rmp-0.8.0 (c (n "rmp") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0kwi66hlg3kw6yianma0s3d974jfrphb6zzm1677jxflx7ymn0vc") (y #t)))

(define-public crate-rmp-0.8.1 (c (n "rmp") (v "0.8.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "00yzcz2fnpylnsdpykhzjkwr5nqsiilxgk0divb6zj9dhy4i26gk") (y #t)))

(define-public crate-rmp-0.8.2 (c (n "rmp") (v "0.8.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "0vnrpgps3vmfzrsrpgl18xhyyajbxqiadmxwkj68zk3cvn0hx4ss") (y #t)))

(define-public crate-rmp-0.8.3 (c (n "rmp") (v "0.8.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1f3qlq0l19dlr04nyzidaglwg1xjgqbggdf842pgifm9cwl0xns1") (y #t)))

(define-public crate-rmp-0.8.4 (c (n "rmp") (v "0.8.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "06jl0yl532fpf1idcisagibwdbwahjj239j4qrcqlwa93z01g6g5") (y #t)))

(define-public crate-rmp-0.8.5 (c (n "rmp") (v "0.8.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1shh0xz30qcbkr0njscxs6zrdwg2sw3cnam2v8p1kjw5b0v02grk") (y #t)))

(define-public crate-rmp-0.8.6 (c (n "rmp") (v "0.8.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1qpz7izri8jibygqynsg9ga9723043xdgy07gxlyqklgfajn1rbw") (y #t)))

(define-public crate-rmp-0.8.7 (c (n "rmp") (v "0.8.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.3") (d #t) (k 2)))) (h "1jf51v4avsx64r9jhclbx645qp597a34i5j7l8s2n4wvzix5vm53") (y #t)))

(define-public crate-rmp-0.8.8 (c (n "rmp") (v "0.8.8") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0vkivlijg0xmgw0fn62p1lg31j4mazqr3gpn0xwml74gzyvlqn8g")))

(define-public crate-rmp-0.8.9 (c (n "rmp") (v "0.8.9") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)))) (h "0kqqq0m4bg1p1rsahbxqlhi0cb65qbxx595sqwdfxwacy5nv840g")))

(define-public crate-rmp-0.8.10 (c (n "rmp") (v "0.8.10") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.2") (d #t) (k 2)))) (h "1mksma1b18yps1s64qx2f4bhg4z19wmfvbjx3zfxbi262kxfamag")))

(define-public crate-rmp-0.8.11 (c (n "rmp") (v "0.8.11") (d (list (d (n "byteorder") (r "^1.4.2") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.2") (d #t) (k 2)))) (h "17rw803xv84csxgd654g7q64kqf9zgkvhsn8as3dbmlg6mr92la4") (f (quote (("std" "byteorder/std" "num-traits/std") ("default" "std"))))))

(define-public crate-rmp-0.8.12 (c (n "rmp") (v "0.8.12") (d (list (d (n "byteorder") (r "^1.4.2") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.2") (d #t) (k 2)))) (h "083bbqw8ibqp63v6scmaxmy5x8yznj4j0i2n6jjivv9qrjk6163z") (f (quote (("std" "byteorder/std" "num-traits/std") ("default" "std"))))))

(define-public crate-rmp-0.8.13 (c (n "rmp") (v "0.8.13") (d (list (d (n "byteorder") (r "^1.4.2") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.2") (d #t) (k 2)))) (h "16i20mvvgh8iih1lns1f1lni9lapslk1jby0i4z1mblw9dpk3nxx") (f (quote (("std" "byteorder/std" "num-traits/std") ("default" "std"))))))

(define-public crate-rmp-0.8.14 (c (n "rmp") (v "0.8.14") (d (list (d (n "byteorder") (r "^1.4.2") (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.2") (d #t) (k 2)))) (h "1i1l6dhv7vws5vp0ikakj44fk597xi59g3j6ng1q55x3dz0xg3i2") (f (quote (("std" "byteorder/std" "num-traits/std") ("default" "std"))))))

