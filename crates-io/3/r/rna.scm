(define-module (crates-io #{3}# r rna) #:use-module (crates-io))

(define-public crate-rna-0.1.0 (c (n "rna") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0653b7npbaxj10v7q3nvcn7jjh3x2qp8a9dx1mspfi06k6ihn014")))

(define-public crate-rna-0.1.1 (c (n "rna") (v "0.1.1") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "12c0xi6cyg78gjs3vxf4z7xl6ph6vjfd76n9wxivaq5jlbx0ayf6")))

(define-public crate-rna-0.1.2 (c (n "rna") (v "0.1.2") (d (list (d (n "colored") (r "^1.9.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1iki5hf9zvnqk9pf8v263y1anahbsnq1dmkr7sr7b4qzk6n7fc08")))

