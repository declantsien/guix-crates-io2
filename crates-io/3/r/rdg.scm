(define-module (crates-io #{3}# r rdg) #:use-module (crates-io))

(define-public crate-rdg-0.1.0 (c (n "rdg") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "184lma5ka0g8r2ma01qjd1qib8ragv1xmckc1c5mspcy5rk81gf3")))

(define-public crate-rdg-0.1.1 (c (n "rdg") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("std" "color"))) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1pf96vpw8mk0xw7yyy7qszi6z960645d4dibps2y6v3ds2r9685h")))

