(define-module (crates-io #{3}# r rpg) #:use-module (crates-io))

(define-public crate-rpg-0.0.1 (c (n "rpg") (v "0.0.1") (d (list (d (n "names") (r "^0.11.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6.6") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)))) (h "0ahf6bap36mq3pvw88768jqf8h5gaw9c5kcaj016ymk2v5py224b")))

(define-public crate-rpg-0.0.2 (c (n "rpg") (v "0.0.2") (d (list (d (n "names") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.21") (d #t) (k 0)))) (h "0a4dscqrdqg3jk991jmiairkzl6jz5jx0zdqmasgwzb9rz5l53sa")))

