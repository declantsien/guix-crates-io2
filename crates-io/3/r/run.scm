(define-module (crates-io #{3}# r run) #:use-module (crates-io))

(define-public crate-run-0.1.0 (c (n "run") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.10") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.10") (d #t) (k 0)) (d (n "term-painter") (r "^0.2.3") (d #t) (k 0)) (d (n "toml") (r "^0.4.2") (d #t) (k 0)))) (h "19hz5bxfrs15cq5w89ki0qaq57dxxm2b9f3iaqns9mf2f4flb1cd")))

