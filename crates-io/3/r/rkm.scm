(define-module (crates-io #{3}# r rkm) #:use-module (crates-io))

(define-public crate-rkm-0.3.0 (c (n "rkm") (v "0.3.0") (d (list (d (n "csv") (r "^0.15.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.9.1") (d #t) (k 0)) (d (n "num") (r "^0.1.42") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "141vpi6km5n6c6bnrc7vnfj9zcm73wd873j3iqkxr17x3hnkn1dl")))

(define-public crate-rkm-0.5.0 (c (n "rkm") (v "0.5.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "csv") (r "^1.0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.0") (d #t) (k 0)) (d (n "ndarray-parallel") (r "^0.9.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0ayb7jijd1ygchlzvk5z0ksbnf457h6li7q4vy5zf5qkfif9i7nm")))

(define-public crate-rkm-0.7.0 (c (n "rkm") (v "0.7.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "csv") (r "^1.0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.0") (d #t) (k 0)) (d (n "ndarray-parallel") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "1373q9cd90mbs9nq23xvjk8j9hmfxm3yzq53k3nhim5s74mjixvh") (f (quote (("parallel" "ndarray-parallel" "rayon"))))))

(define-public crate-rkm-0.8.0 (c (n "rkm") (v "0.8.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "csv") (r "^1.0.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.0") (d #t) (k 0)) (d (n "ndarray-parallel") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "0n3xbglb1j1hvnjp9hs1ql98ii1hlccw15i3nwfmfaxm1pqfqpn3") (f (quote (("parallel" "ndarray-parallel" "rayon"))))))

(define-public crate-rkm-0.8.1 (c (n "rkm") (v "0.8.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "ndarray") (r "^0.12.1") (d #t) (k 0)) (d (n "ndarray-parallel") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5.6") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (o #t) (d #t) (k 0)))) (h "19rnp7g21fy2lvf7pck4587ggscia9pdwrvqdjsh8a1r2233013b") (f (quote (("parallel" "ndarray-parallel" "rayon"))))))

