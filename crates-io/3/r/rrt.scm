(define-module (crates-io #{3}# r rrt) #:use-module (crates-io))

(define-public crate-rrt-0.1.0 (c (n "rrt") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "kdtree") (r "^0.3.1") (d #t) (k 0)) (d (n "kiss3d") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12.0") (d #t) (k 2)) (d (n "ncollide") (r "^0.12.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "06k7js32m4a5wwgbl36hqihpwxkzlysj4v9jlam55v85r1inrx0c")))

(define-public crate-rrt-0.1.1 (c (n "rrt") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "kdtree") (r "^0.3.1") (d #t) (k 0)) (d (n "kiss3d") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12.0") (d #t) (k 2)) (d (n "ncollide") (r "^0.12.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "13a64a864mb5sjfj0bhb94gmillrdsg2zrp8pfz6mmh1dq7ckw6v")))

(define-public crate-rrt-0.2.0 (c (n "rrt") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "kdtree") (r "^0.3.1") (d #t) (k 0)) (d (n "kiss3d") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13.0") (d #t) (k 2)) (d (n "ncollide") (r "^0.13.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "1rv46vw44djf7ba98faf575cpmw1n12zldvqr7xvl389hwlrrsm3")))

(define-public crate-rrt-0.3.0 (c (n "rrt") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "kdtree") (r "^0.3.1") (d #t) (k 0)) (d (n "kiss3d") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13.0") (d #t) (k 2)) (d (n "ncollide") (r "^0.13.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0hdzm360wyjvxpigdi8h4ii4mm6k054jkggcxa6kbv4kl5j82115")))

(define-public crate-rrt-0.4.0 (c (n "rrt") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "kdtree") (r "^0.5.1") (d #t) (k 0)) (d (n "kiss3d") (r "^0.11.0") (d #t) (k 2)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.13.0") (d #t) (k 2)) (d (n "ncollide") (r "^0.13.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)))) (h "0aalya2vv9cb0dr93xqmair6psfvziq46nlf5nf65hx7fnw51fpf")))

(define-public crate-rrt-0.5.0 (c (n "rrt") (v "0.5.0") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "kiss3d") (r "^0.30") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.25") (d #t) (k 2)) (d (n "ncollide3d") (r "^0.28") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0yp57k6p86a2bcxds1b2nbsyin97x3sq20kspqhasrzciw4cnm2a")))

(define-public crate-rrt-0.6.0 (c (n "rrt") (v "0.6.0") (d (list (d (n "kdtree") (r "^0.6") (d #t) (k 0)) (d (n "kiss3d") (r "^0.32") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "0qpxv3zj9158wcmhpfpyn9if5mlmilpwbrwhi2di3nkhdsb31x08")))

(define-public crate-rrt-0.7.0 (c (n "rrt") (v "0.7.0") (d (list (d (n "kdtree") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "kiss3d") (r "^0.35") (d #t) (k 2)))) (h "19pdlsz8qk95n7saii9ysxffd14iyg6xvfg5dzkaklssv0syzhfp")))

