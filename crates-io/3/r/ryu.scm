(define-module (crates-io #{3}# r ryu) #:use-module (crates-io))

(define-public crate-ryu-0.1.0 (c (n "ryu") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0w57kqldnjblhiivf0k7mvyinvkcs3dkg1ssfc3bz98r4x3kc3xi")))

(define-public crate-ryu-0.1.1 (c (n "ryu") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0zxc5x5br9vywy92y3yaxc4rgbdhsygrqkzwv4inpjms1hbnyvwr")))

(define-public crate-ryu-0.2.0 (c (n "ryu") (v "0.2.0") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0kr2ac3w841jdf1mdzgza7pcdqxsnwg46ng5rbmvxjj2fz298l9x") (f (quote (("small"))))))

(define-public crate-ryu-0.2.1 (c (n "ryu") (v "0.2.1") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1zfj5zgx7y5zis3swviznqvxradw3bpzf5iqnjgwalphxkf3brjx") (f (quote (("small"))))))

(define-public crate-ryu-0.2.2 (c (n "ryu") (v "0.2.2") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0rifpnv9is08806b35bjj04q3qw9cjmscrsnmzy1nlbn2pdf526p") (f (quote (("small"))))))

(define-public crate-ryu-0.2.3 (c (n "ryu") (v "0.2.3") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "14glh9h57h9p3v40sqaf0qds6564pimdjx2zvz6h864md7d15ahn") (f (quote (("small"))))))

(define-public crate-ryu-0.2.4 (c (n "ryu") (v "0.2.4") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "12r7bf9l1vk30kjqmpwg78v4li9ib5pq9dkp4djkrq8nf5w6h1gx") (f (quote (("small"))))))

(define-public crate-ryu-0.2.5 (c (n "ryu") (v "0.2.5") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "09rq1x2g0ab6dkgglav4piig77qq5xid41laf7a0agwjwaw6dh77") (f (quote (("small"))))))

(define-public crate-ryu-0.2.6 (c (n "ryu") (v "0.2.6") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "19x4sjc0jm89zm2b2iifd01rmf6vziicp670ffwlm1yyvabdslvi") (f (quote (("small"))))))

(define-public crate-ryu-0.2.7 (c (n "ryu") (v "0.2.7") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1xs6ibjhfhbgki1yk3la3c93zdmz349nim1dlkk9yai8vs69p7pb") (f (quote (("small"))))))

(define-public crate-ryu-0.2.8 (c (n "ryu") (v "0.2.8") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "13wsi4408qxi9w44pdf5zfds4ym7np2070wkhg1g4j4dvi4rasmr") (f (quote (("small"))))))

(define-public crate-ryu-1.0.0 (c (n "ryu") (v "1.0.0") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "15r9z2wzgbj04pks4jz7y6wif5xqhf1wqkl2nd7qrvn08ys68969") (f (quote (("small"))))))

(define-public crate-ryu-1.0.1 (c (n "ryu") (v "1.0.1") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1ian6r6imhbdvgxffcf52bas5bybmpclm2y87kjn3bwflhgjglhr") (f (quote (("small"))))))

(define-public crate-ryu-1.0.2 (c (n "ryu") (v "1.0.2") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1j0h74f1xqf9hjkhanp8i20mqc1aw35kr1iq9i79q7713mn51a5z") (f (quote (("small"))))))

(define-public crate-ryu-1.0.3 (c (n "ryu") (v "1.0.3") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "0xlx9ybzncrb7d6r9533g8ydlg6mr252pfzl4g9cqaqkpvk24mjk") (f (quote (("small"))))))

(define-public crate-ryu-1.0.4 (c (n "ryu") (v "1.0.4") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "1qa1g46584i1qvfpn8a96b2f1p4hslkfzrky7zmyyc24qqmn2ggd") (f (quote (("small"))))))

(define-public crate-ryu-1.0.5 (c (n "ryu") (v "1.0.5") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.2") (d #t) (k 2)))) (h "0vpqv1dj7fksa6hm3zpk5rbsjs0ifbfy7xwzsyyil0rx37a03lvi") (f (quote (("small"))))))

(define-public crate-ryu-1.0.6 (c (n "ryu") (v "1.0.6") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "0s35bwagycbzwmbj0fngm4jljnan272cz12i84kbmfbalssi75iw") (f (quote (("small")))) (r "1.31")))

(define-public crate-ryu-1.0.7 (c (n "ryu") (v "1.0.7") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "05hwml65sbsxjqfqjljm5b5x36gy5vjpala4hfl631p93h4gak95") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.8 (c (n "ryu") (v "1.0.8") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "1yf9qy4wmjmgkprqmj3351ac9vwq85hgkfk1vn1pw44wfh4lq3mk") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.9 (c (n "ryu") (v "1.0.9") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "17qlxkqm4h8h9xqj6rh2vnmwxyzikbsj5w223chmr5l2qx8bgd3k") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.10 (c (n "ryu") (v "1.0.10") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "15960rzj6jkjhxrjfr3kid2hbnia84s6h8l1ga7vkla9rwmgkxpk") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.11 (c (n "ryu") (v "1.0.11") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "02czvxrxhi2gmamw25drdvkfkkk9xd9758bpnk0s30mfyggsn0a5") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.12 (c (n "ryu") (v "1.0.12") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "1ppcgnyfs12p545bl7762jp9b11rlzmgb7yzrr5lnzb8xm1rfjvv") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.13 (c (n "ryu") (v "1.0.13") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "0hchlxvjmsz51l06c7r8zwj45pm8bhc3x3czcih27rkx8v03j4zr") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.14 (c (n "ryu") (v "1.0.14") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "1s9ag0xnvahv3nv8bb22xa03gmhq27klw612gdwxxj78dggjn8zy") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.15 (c (n "ryu") (v "1.0.15") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "0hfphpn1xnpzxwj8qg916ga1lyc33lc03lnf1gb3wwpglj6wrm0s") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.16 (c (n "ryu") (v "1.0.16") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "0k7b90xr48ag5bzmfjp82rljasw2fx28xr3bg1lrpx7b5sljm3gr") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.17 (c (n "ryu") (v "1.0.17") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "188vrsh3zlnl5xl7lw0rp2sc0knpx8yaqpwvr648b6h12v4rfrp8") (f (quote (("small")))) (r "1.36")))

(define-public crate-ryu-1.0.18 (c (n "ryu") (v "1.0.18") (d (list (d (n "no-panic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.8") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_xorshift") (r "^0.3") (d #t) (k 2)))) (h "17xx2s8j1lln7iackzd9p0sv546vjq71i779gphjq923vjh5pjzk") (f (quote (("small")))) (r "1.36")))

