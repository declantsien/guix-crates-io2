(define-module (crates-io #{3}# r rcw) #:use-module (crates-io))

(define-public crate-rcw-0.2.0 (c (n "rcw") (v "0.2.0") (d (list (d (n "comfy-table") (r "^6.1.1") (d #t) (k 0)))) (h "067wqzn465hvb9wja8q4c800217v0jc63904n7w2lc8dnkr67qi9")))

(define-public crate-rcw-0.3.0 (c (n "rcw") (v "0.3.0") (d (list (d (n "comfy-table") (r "^6.1.1") (d #t) (k 0)))) (h "12pqq0qldxaz8c8ym7pskz3gi96zcqfs7vp799ryhwwflbzjd1wh")))

(define-public crate-rcw-0.3.1 (c (n "rcw") (v "0.3.1") (d (list (d (n "comfy-table") (r "^6.1.1") (d #t) (k 0)))) (h "0xp0acgjxvcdj9g0srmxl6fknq4z1lgq9nqnk4hqxr0g08rgh1hb")))

