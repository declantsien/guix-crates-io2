(define-module (crates-io #{3}# r rgy) #:use-module (crates-io))

(define-public crate-rgy-0.1.0 (c (n "rgy") (v "0.1.0") (d (list (d (n "core_affinity") (r "^0.5") (d #t) (k 2)) (d (n "cpal") (r "^0.8") (d #t) (k 2)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "hashbrown") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "rustyline") (r "^4.1") (d #t) (k 2)) (d (n "signal-hook") (r "^0.1") (d #t) (k 2)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 2)))) (h "1r7z6aapvlppbr12k01pw8rbn417lf83fz2rfx8ifivrqn6kan7f") (f (quote (("default") ("color"))))))

