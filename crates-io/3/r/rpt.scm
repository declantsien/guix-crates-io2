(define-module (crates-io #{3}# r rpt) #:use-module (crates-io))

(define-public crate-rpt-0.0.0 (c (n "rpt") (v "0.0.0") (h "1dikbqb679xbx4gvm9jbvfcngahzfjs9krxa87qmy2cxywm4mjgm")))

(define-public crate-rpt-0.1.0 (c (n "rpt") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.5.6") (d #t) (k 2)) (d (n "glm") (r "^0.8.0") (d #t) (k 0) (p "nalgebra-glm")) (d (n "image") (r "^0.23.10") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "ureq") (r "^1.5.2") (d #t) (k 2)) (d (n "zip") (r "^0.5.8") (d #t) (k 2)))) (h "1pfq00vbbrr589lvqwbd77k744zjd4kkrc9szrz147v3mhwlyxqx")))

(define-public crate-rpt-0.2.0 (c (n "rpt") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.5.10") (d #t) (k 2)) (d (n "glm") (r "^0.10.0") (d #t) (k 0) (p "nalgebra-glm")) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "ureq") (r "^2.0.2") (d #t) (k 2)) (d (n "zip") (r "^0.5.10") (d #t) (k 2)))) (h "10lvvxk37g4p56jcrlnz6ayh8qhasiw7r80ch883qgvx3d9x6izz")))

(define-public crate-rpt-0.2.1 (c (n "rpt") (v "0.2.1") (d (list (d (n "color-eyre") (r "^0.5.10") (d #t) (k 2)) (d (n "glm") (r "^0.10.0") (d #t) (k 0) (p "nalgebra-glm")) (d (n "image") (r "^0.23.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "ureq") (r "^2.0.2") (d #t) (k 2)) (d (n "zip") (r "^0.5.10") (d #t) (k 2)))) (h "0743n3yvv0p01p8h68azxrpm55ka9l3l8gcrq8z7j9z8dbns50da")))

