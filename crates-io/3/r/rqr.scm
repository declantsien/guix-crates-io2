(define-module (crates-io #{3}# r rqr) #:use-module (crates-io))

(define-public crate-rqr-0.1.0 (c (n "rqr") (v "0.1.0") (d (list (d (n "bitvec") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (o #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "018a686lrpp7g3yz501jxsxdfpqdgww2h0rzywrf9v50qwrqf8rx") (f (quote (("cli" "clap"))))))

(define-public crate-rqr-0.1.1 (c (n "rqr") (v "0.1.1") (d (list (d (n "bitvec") (r "^0.10") (d #t) (k 0)) (d (n "clap") (r "~2.27.0") (o #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1050a9fxphsdpdydfyn7zpzzlf8h4lp9ynglqnvvyg6q1rv6m0xx") (f (quote (("cli" "clap"))))))

