(define-module (crates-io #{3}# r rar) #:use-module (crates-io))

(define-public crate-rar-0.2.0 (c (n "rar") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "nom") (r "^4.0.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0abm4wxv5832y9sldvsvnzw65zd5yhay2n936vkd0f7jzgmimm8v")))

