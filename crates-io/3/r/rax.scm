(define-module (crates-io #{3}# r rax) #:use-module (crates-io))

(define-public crate-rax-0.1.0 (c (n "rax") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k0dwxqr3az29z0286vxfg817kmb9rqzsr0m4dbqj35vr3y2app9")))

(define-public crate-rax-0.1.1 (c (n "rax") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "07vhmx3vkgid14lh2ak58dg6fgjd6j6aiz1a8hh3cbn7s8v20ji7")))

(define-public crate-rax-0.1.2 (c (n "rax") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "1jnf5nij14gy86a66m56f2h59815nf7i53nvmn45slq8acha6aba")))

(define-public crate-rax-0.1.3 (c (n "rax") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "04lcinfvf600jzkrk2rmp301k40c0asd9sr62w0594yzwajjli65")))

(define-public crate-rax-0.1.5 (c (n "rax") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11.0") (d #t) (k 0)))) (h "12bjgwzlhpfir9llpkz8900y74rrslbx156mr3qzkifxhrs7bajy")))

