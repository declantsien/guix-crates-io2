(define-module (crates-io #{3}# r rlr) #:use-module (crates-io))

(define-public crate-rlr-0.1.0 (c (n "rlr") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "15k3xrw5bwmw3r9lz9mb0rwbv9h69cjarfyspg3ics5h5iln2sfs")))

(define-public crate-rlr-0.2.0 (c (n "rlr") (v "0.2.0") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "mockall") (r "^0.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "07z9kls6k05vz4rsm019hb308jp6nbc9l52ha9j8nnfqa0spc4q3")))

