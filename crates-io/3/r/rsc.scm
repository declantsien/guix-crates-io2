(define-module (crates-io #{3}# r rsc) #:use-module (crates-io))

(define-public crate-rsc-0.1.1 (c (n "rsc") (v "0.1.1") (d (list (d (n "peek-nth") (r "^0.2.0") (d #t) (k 0)))) (h "1yhchyh087f5crc6lgkzzgj16b9wa9hgjmsx4x6nm9v045550qs2")))

(define-public crate-rsc-0.1.2 (c (n "rsc") (v "0.1.2") (d (list (d (n "peek-nth") (r "^0.2.0") (d #t) (k 0)))) (h "1lk1i8nyh0rvacsldq7dk272h5nslvgni521qyqjf0ndyh1kc99p")))

(define-public crate-rsc-0.1.3 (c (n "rsc") (v "0.1.3") (h "0l9y7wrypg8x4wm0zqfm2xrzp1gcp0lv3idq8xp9b7as85v5kx7w")))

(define-public crate-rsc-0.1.4 (c (n "rsc") (v "0.1.4") (h "17sgvk53axbwjar8i6lpgwh33n95lyvgz2hvbp22nbxqwyby3kmf")))

(define-public crate-rsc-0.1.5 (c (n "rsc") (v "0.1.5") (h "01wg6nqlbh2n48z6vasglimvqqjawg7blvm20i2z0y4r2mrafrg9")))

(define-public crate-rsc-0.1.7 (c (n "rsc") (v "0.1.7") (h "050mr01wrx3dy7qh9nfx5wrv7k6v6fasv8mlzglhbfzw5iw9cln4")))

(define-public crate-rsc-1.0.0 (c (n "rsc") (v "1.0.0") (h "0waf9hklxik5yzbzlns6wjgkcd90hzlv574lhz0wnaskkws6l63d")))

(define-public crate-rsc-1.1.0 (c (n "rsc") (v "1.1.0") (h "1ps74g9z8chfrch52v1vd5banbpj5xwf23xx2ymwgxx13i9wm3si")))

(define-public crate-rsc-1.1.1 (c (n "rsc") (v "1.1.1") (h "008w1j5lsinphxvc414px1jbjkk1hj9ps2j98hrg8ij4wyn7chb2") (y #t)))

(define-public crate-rsc-1.2.0 (c (n "rsc") (v "1.2.0") (h "1h0wh8mh8v4yinyicllmww8bmz6xz6valb95r7p510xabil0h7zw")))

(define-public crate-rsc-1.2.1 (c (n "rsc") (v "1.2.1") (h "1nblcjj0kskczs0v86sai8hv9vwwdpn4q1hciy2pm5qgzih75xvm")))

(define-public crate-rsc-2.0.0 (c (n "rsc") (v "2.0.0") (d (list (d (n "colored") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "structopt") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0kzbcihnkc1584q4k0gbvb1k6w865x0kbav78cc6gkgcgwklaqrd") (f (quote (("build-binary" "structopt" "colored"))))))

(define-public crate-rsc-3.0.0 (c (n "rsc") (v "3.0.0") (d (list (d (n "colored") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "peekmore") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1sgi5ascgx72p2q08g9lyyxy1hbgalbf5pdp1498x2xarahs8k3d") (f (quote (("executable" "structopt" "colored"))))))

