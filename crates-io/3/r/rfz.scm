(define-module (crates-io #{3}# r rfz) #:use-module (crates-io))

(define-public crate-rfz-0.1.0 (c (n "rfz") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "lazycell") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "pipeliner") (r "^1.0") (d #t) (k 0)))) (h "16aqq3yj9vyckxlx49dn5hl8ywfg8j8azz6zqsf0nba5y5mcn59k")))

(define-public crate-rfz-0.2.0 (c (n "rfz") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "kuchiki") (r "^0.8") (d #t) (k 0)) (d (n "lazycell") (r "^1.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "pipeliner") (r "^1.0") (d #t) (k 0)))) (h "0znn72nv3fskybvaaqk9mlpjlb7z6zcqj2409z0a0p615s0n9h0p")))

