(define-module (crates-io #{3}# r rez) #:use-module (crates-io))

(define-public crate-rez-0.1.0 (c (n "rez") (v "0.1.0") (h "00fm0j50wmk48cgyvrswm7mwip02d5xlhw1l87jk53yx9sqk0wxk")))

(define-public crate-rez-0.1.1 (c (n "rez") (v "0.1.1") (h "1322k35jvwm6r8a23avx7jacnlh135dbifnkmlnv2bvsi2bljx4v")))

(define-public crate-rez-0.1.2 (c (n "rez") (v "0.1.2") (h "0lqhd7wzmkgp9qqkg3wl9j99bpc3g07wl4d25zkncygwwyi4vmrr")))

(define-public crate-rez-0.1.3 (c (n "rez") (v "0.1.3") (h "1297sfyiarlsx41n7yvkban73b7xw66pm20xq9nvvzpjwyzpl11l")))

(define-public crate-rez-0.1.4 (c (n "rez") (v "0.1.4") (h "0xmq4wz2vidvqjixk7av5b4zy1hh7nrwx450b82ars82ah7zxxdp")))

