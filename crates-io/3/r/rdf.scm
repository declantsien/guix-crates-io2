(define-module (crates-io #{3}# r rdf) #:use-module (crates-io))

(define-public crate-rdf-0.1.0 (c (n "rdf") (v "0.1.0") (h "0plzcgxadg2z4pva9fggl4n0g8a2lydl0dvadqy7brfsvqx07d5c")))

(define-public crate-rdf-0.1.1 (c (n "rdf") (v "0.1.1") (h "14nc7p12xaiivvsl0n6bvvh80k3hw5jka8mnbkqa2spk521ig384")))

(define-public crate-rdf-0.1.2 (c (n "rdf") (v "0.1.2") (h "1kc9q2ls5p3j3klx74fvzrn25yxbl95b95d5ygm8pk8dknl5r0lc")))

(define-public crate-rdf-0.1.3 (c (n "rdf") (v "0.1.3") (h "1xac9fnfqvjxdw5m6vf065rlxslc5ar14ma949i0xa07qayli82i")))

(define-public crate-rdf-0.1.4 (c (n "rdf") (v "0.1.4") (h "0in6sjc1nbh44d8872xzas1wnlprd5nzv3p1g4r4mzik06zja5xv")))

