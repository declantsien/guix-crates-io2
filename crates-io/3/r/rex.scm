(define-module (crates-io #{3}# r rex) #:use-module (crates-io))

(define-public crate-rex-0.1.0 (c (n "rex") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "gag") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.3.24") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustbox") (r "^0.6.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "0bny6fmqd5n1x04iymcvcfx4h030yf813k4093dc41r743kk89hq")))

