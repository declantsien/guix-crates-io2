(define-module (crates-io #{3}# r rev) #:use-module (crates-io))

(define-public crate-rev-0.3.0 (c (n "rev") (v "0.3.0") (h "1vn0f2v215vkg31jf7j91z0x8dxd2j3n9n39zh7lx4l5na62xsnp")))

(define-public crate-rev-0.4.0 (c (n "rev") (v "0.4.0") (h "0d52b77a2xz0qawim9gmbnx05lz6krpgj9y0cyd42n5hdrppskn6")))

(define-public crate-rev-0.4.1 (c (n "rev") (v "0.4.1") (h "0i3zwalzbh2xmis89whk8nzlz4kpkxm22zv2ncqsk7sal5qd11x9")))

