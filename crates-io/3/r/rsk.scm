(define-module (crates-io #{3}# r rsk) #:use-module (crates-io))

(define-public crate-rsk-0.0.1 (c (n "rsk") (v "0.0.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color"))) (k 0)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "fern") (r "^0.3.5") (d #t) (k 0)) (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1v6gbwyvrrykrja8n3wy6hpczf4xj80y0m2fm7i3pa8xgsgrh7fp") (y #t)))

(define-public crate-rsk-0.1.0 (c (n "rsk") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color"))) (k 0)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "fern") (r "^0.3.5") (d #t) (k 0)) (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1vi4a35zhq6kyxilpqqfisq926ya443fiay7klsb2iqfn57dm9vs") (y #t)))

(define-public crate-rsk-0.1.1 (c (n "rsk") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color"))) (k 0)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "fern") (r "^0.3.5") (d #t) (k 0)) (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "171kxf9fhy9lvi6br123h24ji5fd7279qsn0571sd0y0jfzrd675") (y #t)))

(define-public crate-rsk-0.1.2 (c (n "rsk") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color"))) (k 0)) (d (n "fern") (r "^0.3.5") (d #t) (k 0)) (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1756hfpy6647d5kc9sgz31kz29sv5adrixb8g63wfjwf001p1lhp") (y #t)))

(define-public crate-rsk-0.1.3 (c (n "rsk") (v "0.1.3") (h "0gfbyy2fj92571r0hs4mcqxql40nv7jvpbjrn40fgkh7hlyqanv4") (y #t)))

(define-public crate-rsk-0.1.4 (c (n "rsk") (v "0.1.4") (h "0z618i1bmwriwgx2j86mlmaxyrhppqhpjmh9qnjgjr6qbr3srqjb") (y #t)))

(define-public crate-rsk-0.1.5 (c (n "rsk") (v "0.1.5") (h "10jjlxn20zzmzd1nfillhxvsn8g5k080sgakc2wmcwl7fa51jdyp")))

(define-public crate-rsk-0.0.5 (c (n "rsk") (v "0.0.5") (h "08ssk2vfla7nwr3pqw0dqawzl7ahqyhkq3q3r8b64h47xn1sifpk") (y #t)))

