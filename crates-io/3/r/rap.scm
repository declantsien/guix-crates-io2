(define-module (crates-io #{3}# r rap) #:use-module (crates-io))

(define-public crate-rap-0.1.0 (c (n "rap") (v "0.1.0") (d (list (d (n "audrey") (r "^0.2") (d #t) (k 0)) (d (n "dft") (r "^0.5") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ypd7iw7qbwcvhrvhhl1l6pnf1wbilvrbh6jmfjb37vzpi5csm8v")))

