(define-module (crates-io #{3}# r rc2) #:use-module (crates-io))

(define-public crate-rc2-0.0.0 (c (n "rc2") (v "0.0.0") (h "154bffyp2drw1v3pm9h9hs4fw884g2y39c8951ybww8v5q0066m3")))

(define-public crate-rc2-0.1.0 (c (n "rc2") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "^0.2") (d #t) (k 0)) (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "crypto-tests") (r "^0.2.4") (d #t) (k 2)) (d (n "generic-array") (r "^0.5") (d #t) (k 0)))) (h "1a84y0pkpsh6j1fyzhxhaqfcb6xydwa2vjcm6jzcw1rlyrkcp1v7")))

(define-public crate-rc2-0.2.0 (c (n "rc2") (v "0.2.0") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "1a8xg3v2n9j4wglcb12gysgrzp479q0ldavrd1gzsvias2vvqi84")))

(define-public crate-rc2-0.3.0 (c (n "rc2") (v "0.3.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0plfqxlm7p8gc86yr661hk1v6lyj7svndyvzjspb5jbl2zbhk4h3")))

(define-public crate-rc2-0.4.0 (c (n "rc2") (v "0.4.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "134c8plzcspvg799h7dy0jlhd7ixdmf90yi44m6rzvyb9jfzjpi2")))

(define-public crate-rc2-0.5.0 (c (n "rc2") (v "0.5.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0hv6hih6c35xa1476hvncnc3fiwd1jxmp0fcn2mafvh8liip81hs")))

(define-public crate-rc2-0.6.0 (c (n "rc2") (v "0.6.0") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0alzb59n0w0n53abw2nl1hgvhgylk6i3vcw81cy5savzqhid350d")))

(define-public crate-rc2-0.7.0 (c (n "rc2") (v "0.7.0") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "156by7zkqbrbrlgqp6fw5j43fy3aa9rd2nhdq92i6p87hg19gwa8")))

(define-public crate-rc2-0.8.0 (c (n "rc2") (v "0.8.0") (d (list (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "0fli9b9jp0lwpijwrglixsqrfziky7c92c1ph59jlx7j1pfp690h") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-rc2-0.8.1 (c (n "rc2") (v "0.8.1") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)))) (h "1pf17xj083bppby905ciwdh8wvrr7yli0l75m95bhf4lism4vik2") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

