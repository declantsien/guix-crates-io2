(define-module (crates-io #{3}# r rnu) #:use-module (crates-io))

(define-public crate-rnu-0.1.0 (c (n "rnu") (v "0.1.0") (h "0lw6g64km7pi2a0yspx76h9r7n38vblac9hjli28khb4m6ajy3lr") (y #t)))

(define-public crate-rnu-0.1.1 (c (n "rnu") (v "0.1.1") (h "0d81fnwd9fb85r1zpdbzzbyzd2w9g629cflqrm3b5azcad1ij91p") (y #t)))

(define-public crate-rnu-0.1.2 (c (n "rnu") (v "0.1.2") (h "1ics8lf3p5w1qx88w6dhd3j60n3ysghi9jyd5pfh7i8634h800w5") (y #t)))

(define-public crate-rnu-0.2.0 (c (n "rnu") (v "0.2.0") (h "05kcdcwkjs6k12lrc60pn25ppn3bvwmb7wszajjahiw0lifp0hlq") (y #t)))

(define-public crate-rnu-0.2.1 (c (n "rnu") (v "0.2.1") (h "0klgcvwmy7a5529alx7zx2c6d5sadv8ss35jn866889yckdn1xp6") (y #t)))

(define-public crate-rnu-0.2.2 (c (n "rnu") (v "0.2.2") (h "08lb6d45c1gsgkgkjz7jcwp47kx24kk647igmh71an7z47n9wn3a") (y #t)))

(define-public crate-rnu-0.2.3 (c (n "rnu") (v "0.2.3") (d (list (d (n "shell_command") (r "^0.1.0") (d #t) (k 0)))) (h "02967xb306qdspqsyqbzqzvggiqwa0h1zgw2nd1bg5db3d7xx81a") (y #t)))

(define-public crate-rnu-0.2.4 (c (n "rnu") (v "0.2.4") (d (list (d (n "shell_command") (r "^0.1.0") (d #t) (k 0)))) (h "0wvdfmbblcmaxnrqcy1ndq27wqvgcdqd4jmpasibc7dvzdccm1az") (y #t)))

(define-public crate-rnu-0.2.5 (c (n "rnu") (v "0.2.5") (h "13mj8sgsfyh56yhqs86hrjl8pmsal4ddanx8adiyjw6pyc1hzjjr")))

(define-public crate-rnu-0.2.6 (c (n "rnu") (v "0.2.6") (h "1qv239ipc0jh1m73cpdvxx19cc3xnlxbbjns6hvy8rhngww7nz5x")))

(define-public crate-rnu-0.3.0 (c (n "rnu") (v "0.3.0") (h "16ssng02zy4n0igmbm15i0b09rfqhs46c3vla87vhazmcw0ybqmm")))

(define-public crate-rnu-0.3.1 (c (n "rnu") (v "0.3.1") (h "11a4r5abzwadnahbdlyndk4mp7pxvd8mmfwdg1b1x3jvfah6d3pp")))

(define-public crate-rnu-0.3.2 (c (n "rnu") (v "0.3.2") (h "1p0fr1xxjl4k31xphg7hggi4n487636h9ywfjg00xc7na86k2ly7")))

(define-public crate-rnu-0.4.0 (c (n "rnu") (v "0.4.0") (h "1nq0ic3xh6415f28n6sqawkvb65ip5qn9mncjskjykkwgdbzxs1f")))

(define-public crate-rnu-0.4.1 (c (n "rnu") (v "0.4.1") (h "0phj5b0sqymvxqpsjjjdiarccsb2d6miyb3r9w6c6qw2k8xvrq6f")))

