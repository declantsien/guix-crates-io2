(define-module (crates-io #{3}# r rcp) #:use-module (crates-io))

(define-public crate-rcp-0.1.0 (c (n "rcp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ssh2") (r "^0.8") (f (quote ("vendored-openssl"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1r33251hjxmlax9vmf9vf607lbl5pvwr3jwfml6fy51kp5frrywn")))

