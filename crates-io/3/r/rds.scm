(define-module (crates-io #{3}# r rds) #:use-module (crates-io))

(define-public crate-rds-0.0.0 (c (n "rds") (v "0.0.0") (h "0zgj0xwri2kd1c3pvjlqm1jby767ibs3i3q5infwjmgzf7n2mqd0")))

(define-public crate-rds-0.0.1 (c (n "rds") (v "0.0.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)))) (h "1z7lq7kx15wdmv8r3mkn9gwdi5qk07zqxfznzaplszad9qqqw7rd")))

(define-public crate-rds-0.0.2 (c (n "rds") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)))) (h "15gqjfxfhncmp96fjpnk5a7x351gl59yxjji8jwql0frxx6vk3sn")))

(define-public crate-rds-0.0.3 (c (n "rds") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "0d9arl43hppy8s2vbdn2mky254004q4ampd97w9qdp69acrmnibc") (f (quote (("opencl"))))))

