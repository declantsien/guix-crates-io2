(define-module (crates-io #{3}# r rbf) #:use-module (crates-io))

(define-public crate-rbf-0.1.0 (c (n "rbf") (v "0.1.0") (h "1m977509avb54yhd28s0sn2nww1h2q997afsil33p3zxakmpxwd3")))

(define-public crate-rbf-0.2.0 (c (n "rbf") (v "0.2.0") (h "1n6ljnwblbb8r37nd4kdvs1wl66q50fqhklj8z92jgsvvnq7yk7v")))

