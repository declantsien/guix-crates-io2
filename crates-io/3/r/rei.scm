(define-module (crates-io #{3}# r rei) #:use-module (crates-io))

(define-public crate-rei-0.0.1 (c (n "rei") (v "0.0.1") (h "1jml97mgx1fr606wnw9p37h4sr3yh36q5y7jyvqnq98lwzmidw9m") (y #t)))

(define-public crate-rei-0.0.5 (c (n "rei") (v "0.0.5") (h "1hn165qcmyq70jz85qbqp0n6w8nw6a5pscm57wm8dxwap06gggwi") (y #t)))

(define-public crate-rei-0.0.7 (c (n "rei") (v "0.0.7") (h "03np9jmy8b5zz9q40dp25qv917mpzhidb79pfm8sjj281asj6hm0") (y #t)))

(define-public crate-rei-0.1.1 (c (n "rei") (v "0.1.1") (h "1xnd1rrvc841vd1z52vvspxld10xncr2fshvys7kxwsmr8x78h48") (y #t)))

(define-public crate-rei-0.1.3 (c (n "rei") (v "0.1.3") (h "0jqyqawwkfb8l9sv6dyljm5dfbdlbx060kyynxwirgivhpdxwyyc") (y #t)))

(define-public crate-rei-0.1.4 (c (n "rei") (v "0.1.4") (h "16g0cd4ihq6byg2106w1yy9jgf0a7c0qx7p75zvf7dp3d3w6igcr") (y #t)))

(define-public crate-rei-0.1.77 (c (n "rei") (v "0.1.77") (h "02zjsl051ry1k01shvvr0j97x7da02x93aahnyagwmnbxyvc53ln") (y #t)))

(define-public crate-rei-0.77.0 (c (n "rei") (v "0.77.0") (h "0xymak166znlg4jj6wqgcdqv7dwaf6cw2v6yckxy775fcwnxbaay") (y #t)))

(define-public crate-rei-7.7.18 (c (n "rei") (v "7.7.18") (h "03f9byp44czwa4w905pj482bsnww01k3wc2asv21v70yhai3nfh4") (y #t)))

(define-public crate-rei-2018.7.7 (c (n "rei") (v "2018.7.7") (h "1hx5a0va59h980bfbww8kq5kpcd4g71qqn0a2aigkgmlialxnsxf") (y #t)))

(define-public crate-rei-2019.12.13 (c (n "rei") (v "2019.12.13") (h "0nqbc0arckbprsg0slw9xkix5jkc3kq2b4lfs1r1zzl4biwl32yp") (y #t)))

(define-public crate-rei-9999.999.99 (c (n "rei") (v "9999.999.99") (h "0jk0016al4r09w6gmyygcsm8zkf6n088bn1scp0zwby40a1kxjss") (y #t)))

(define-public crate-rei-9.9.9 (c (n "rei") (v "9.9.9") (h "055m9aviqhbgd9my2yzgarbv1f87nl3g9hwfhip08rzlzmq0p1lj") (y #t)))

(define-public crate-rei-99999.99999.99999 (c (n "rei") (v "99999.99999.99999") (h "03kcwrlmya4d5ih9g7mfclwg97jiw4mfgmdwprb7qp25mp7amx4l") (y #t)))

(define-public crate-rei-9999999.9999999.9999999 (c (n "rei") (v "9999999.9999999.9999999") (h "1ihwv06wy3p0l40vfg2s2mzs54m1dwxzyld6i8n80z1sxmifbxm8") (y #t)))

(define-public crate-rei-999999999.999999999.999999999 (c (n "rei") (v "999999999.999999999.999999999") (h "1lfhc6wdwakp8g3f1r92raybnshpcavpkivc1gwllkfxghxzwad7")))

