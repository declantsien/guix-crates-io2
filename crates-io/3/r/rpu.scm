(define-module (crates-io #{3}# r rpu) #:use-module (crates-io))

(define-public crate-rpu-0.1.0 (c (n "rpu") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)))) (h "0x030m7zf1c4iaa9d1skaaczbwsf61xwp6hm1p430c8hg69p14p2")))

(define-public crate-rpu-0.1.1 (c (n "rpu") (v "0.1.1") (d (list (d (n "maths-rs") (r "^0.2.4") (f (quote ("short_types" "short_hand_constructors" "casts" "serde"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1k0088bbkzf19mpwmdkgiia5lk6kg88nhj0wxv22vnlyxwxhfr80")))

(define-public crate-rpu-0.1.2 (c (n "rpu") (v "0.1.2") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^4.2.8") (d #t) (k 0)))) (h "0xgyr8xn202imhbjbfh5f2bd40zcw7479ir2v4rh8j6s81i5ljkf")))

(define-public crate-rpu-0.1.3 (c (n "rpu") (v "0.1.3") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^4.2.8") (d #t) (k 0)))) (h "07fi7a84j7ir3yqg8wzrl3j6kx5jmdxjy2lbcgs5vw5xpxhakrqx")))

(define-public crate-rpu-0.1.4 (c (n "rpu") (v "0.1.4") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^4.2.8") (d #t) (k 0)))) (h "0pqigv7sa3vnfqiy1pffr76bsl52mq5shvxgmhagyi272hj2qs7j")))

(define-public crate-rpu-0.1.5 (c (n "rpu") (v "0.1.5") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^4.2.8") (d #t) (k 0)))) (h "02xy9krskkbn3nv3jj7l7vk7jsffbir3kv9hb19zd57rqmiciv09")))

(define-public crate-rpu-0.2.0 (c (n "rpu") (v "0.2.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^4.2.8") (d #t) (k 0)))) (h "06vyc0d4f0d2izcac28aclr9c9andwhykcyrhmgjs8v3w9gl081m")))

(define-public crate-rpu-0.2.1 (c (n "rpu") (v "0.2.1") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^4.2.8") (d #t) (k 0)))) (h "1hbk8d52604m05crpgvxds82mlvg83rhb8f5j4rni0akd5dq84v8")))

(define-public crate-rpu-0.2.2 (c (n "rpu") (v "0.2.2") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^4.2.8") (d #t) (k 0)))) (h "1lk8nn7lswhqlnn5y8342bb1a5k4bc1iwsw0njn0fwq593bfh0ip")))

(define-public crate-rpu-0.2.3 (c (n "rpu") (v "0.2.3") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^4.2.8") (d #t) (k 0)))) (h "01rhrnrwqkf8xc4r3sgv0vmbr422v15snh5i41kzh1mw2c6wr4vf")))

(define-public crate-rpu-0.2.5 (c (n "rpu") (v "0.2.5") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^4.2.8") (d #t) (k 0)))) (h "1a3kqm7g55pb37zmn4czib6d49nfgqbv18dr5jln52498qs4i6wq")))

(define-public crate-rpu-0.3.0 (c (n "rpu") (v "0.3.0") (d (list (d (n "image") (r "^0.25.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "wasmer") (r "^4.3.0") (d #t) (k 0)))) (h "1p3mfh83j7mjd71wcihidi1ip9kxcfycfsqi65gqqs765x1ngni5")))

