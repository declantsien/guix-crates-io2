(define-module (crates-io #{3}# r rxr) #:use-module (crates-io))

(define-public crate-rxr-0.1.0 (c (n "rxr") (v "0.1.0") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0cw99sn7y87y0n0qwh7dr8dnq0ycjkjccvg85w3inh0an1y7i3g7") (r "1.70")))

(define-public crate-rxr-0.1.1 (c (n "rxr") (v "0.1.1") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync" "macros" "time"))) (d #t) (k 0)))) (h "15j7l46mz8dcp935zin20scwmbrvhm6mg30917sjyg3n57bx7bk7") (r "1.70")))

(define-public crate-rxr-0.1.2 (c (n "rxr") (v "0.1.2") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync" "macros" "time"))) (d #t) (k 0)))) (h "123z6khw7z1y1z55k7z6la0bqkvf3wfj9996g9231vxbimlrajc9") (r "1.70")))

(define-public crate-rxr-0.1.3 (c (n "rxr") (v "0.1.3") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0wwggd18llriarjiall9a73wjb0y1d5l5c6173hbpda78wcybkhy") (r "1.70")))

(define-public crate-rxr-0.1.4 (c (n "rxr") (v "0.1.4") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync" "macros" "time"))) (d #t) (k 0)))) (h "02b33s39bh3cw919b4j3rb41jxppr236jx9h89rlmhvwld0qc0yp") (r "1.70")))

(define-public crate-rxr-0.1.5 (c (n "rxr") (v "0.1.5") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync" "macros" "time"))) (d #t) (k 0)))) (h "1n66gfdvmsrx05gki7hp7jd3jklx6a3pss801an5hicvwghnyvc2") (r "1.70")))

(define-public crate-rxr-0.1.6 (c (n "rxr") (v "0.1.6") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0549jl3cnirkp7hin7fmb6n5vs32r8zyx28l0hxhjycpynkvpcpb") (r "1.70")))

(define-public crate-rxr-0.1.7 (c (n "rxr") (v "0.1.7") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync" "macros" "time"))) (d #t) (k 0)))) (h "09zibra60gcj5ca7q8qrv8gar0ib03w9hzn3rnribzrzmgkhv70i") (r "1.70")))

(define-public crate-rxr-0.1.8 (c (n "rxr") (v "0.1.8") (d (list (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "sync" "macros" "time"))) (d #t) (k 0)))) (h "0q63f87khc030b2layf8dd0g8m5ynkl39y0b8sxg0fnp1mkq351w") (r "1.70")))

