(define-module (crates-io #{3}# r rms) #:use-module (crates-io))

(define-public crate-rms-0.0.1 (c (n "rms") (v "0.0.1") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)))) (h "118r948l7lvi2k0n2z7vq9ah1rzrhg6z0gl9qk17j5r4a94s19bf")))

(define-public crate-rms-0.0.2 (c (n "rms") (v "0.0.2") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "13kfshs2qnqqw16h87vngknissiz39jqdi9wyw872w7fx6gwcfsr")))

(define-public crate-rms-0.0.3 (c (n "rms") (v "0.0.3") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1iysbcjpbkvp2j7knj4zq07l3whr85n3pk4h3mxn9kc1lkxj5nbv")))

(define-public crate-rms-0.0.4 (c (n "rms") (v "0.0.4") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1xqwsi5vwkdpazpgmar903mp5f10plc7mskbs2n11a37vbwr6iyc")))

(define-public crate-rms-0.1.0 (c (n "rms") (v "0.1.0") (d (list (d (n "dsp-chain") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "10l4y5rmfy9wqhhamaz37plwrw3v6fagawqkfbj7cviphd18c801")))

(define-public crate-rms-0.2.0 (c (n "rms") (v "0.2.0") (d (list (d (n "dsp-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "num") (r "^0.1.27") (d #t) (k 0)))) (h "150sam70gfzfp4gk4kwpkpiky1hns8j2m9n75xgcg383rkv7dhgd")))

(define-public crate-rms-0.3.0 (c (n "rms") (v "0.3.0") (d (list (d (n "dsp-chain") (r "^0.9.0") (d #t) (k 0)))) (h "095dph9xw0pjvvc7mv7flza383v23qb0bzblchx0320r622gg30n")))

(define-public crate-rms-0.4.0 (c (n "rms") (v "0.4.0") (d (list (d (n "dsp-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 0)))) (h "01hjrk8j78l61p5jb0svshmv72r2grkws5aqsi205pxkzli64hcz")))

(define-public crate-rms-0.4.1 (c (n "rms") (v "0.4.1") (d (list (d (n "dsp-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 0)))) (h "11khh2xji7lpdpgb8sffh7dhdrzpirkry2ybbs5ff3ymzfq6j9c5")))

(define-public crate-rms-0.4.2 (c (n "rms") (v "0.4.2") (d (list (d (n "dsp-chain") (r "^0.9.0") (d #t) (k 0)) (d (n "time_calc") (r "^0.10.1") (d #t) (k 0)))) (h "0jgp2lv4fl4k4mj1l5m587dz2m73znkz0b1rkbqqv7y6ydbwvf3c")))

