(define-module (crates-io #{3}# r rye) #:use-module (crates-io))

(define-public crate-rye-0.0.1 (c (n "rye") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1.5") (f (quote ("user-hooks"))) (k 2)) (d (n "expected") (r "^0.0.1") (f (quote ("futures"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 2)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "maybe-unwind") (r "^0.1") (f (quote ("futures"))) (d #t) (k 0)) (d (n "mimicaw") (r "^0.1.3") (d #t) (k 0)) (d (n "pin-project") (r "^0.4") (d #t) (k 0)) (d (n "rye-macros") (r "= 0.0.1") (d #t) (k 0)) (d (n "scoped-tls") (r "^1") (d #t) (k 2)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "11niw3irv1vnx0ch2n1chnajx9grppd2a78m9xsmc2ld2za3xnw0")))

