(define-module (crates-io #{3}# r rgb) #:use-module (crates-io))

(define-public crate-rgb-0.1.0 (c (n "rgb") (v "0.1.0") (h "17jcfw9qnwj0b1kx8nn0waz9b8njkqqa6z38swx4x2xw7ssmi73g") (y #t)))

(define-public crate-rgb-0.2.0 (c (n "rgb") (v "0.2.0") (h "029b06ywsmqqvsrmkfx0vdrnja86xp8wgkgp9ldcsb5jyrdbpkhr") (y #t)))

(define-public crate-rgb-0.3.0 (c (n "rgb") (v "0.3.0") (h "0s16yvkq34mr0m4n899nh6ky4407ij1nd5pfj3zjxjhhzi6fs9gm") (y #t)))

(define-public crate-rgb-0.4.0 (c (n "rgb") (v "0.4.0") (h "1pc2wz2jf7hx3s5mlgv6j8czf648bdx2r81lmmnzpjpkb8n3qnwb") (y #t)))

(define-public crate-rgb-0.5.0 (c (n "rgb") (v "0.5.0") (h "0r05bam0jxwn6213lwywa2b6p1arxyrs9z7ncagxiza03rqyg3r0") (y #t)))

(define-public crate-rgb-0.5.1 (c (n "rgb") (v "0.5.1") (h "0900yhakr64g6x7ia73cvzybfkz71zdl4svd8x18wwd6b7d7cbv7") (y #t)))

(define-public crate-rgb-0.5.2 (c (n "rgb") (v "0.5.2") (h "1q4win3kd53vq0zp8fi5kad9gvgax4i9r35hg40y8a4pxyxpbnnv") (y #t)))

(define-public crate-rgb-0.5.3 (c (n "rgb") (v "0.5.3") (h "049vhp67mhap7vjx0srxg4lznsf3p7bcpg2jcfmmq06sh5hd9p0q") (y #t)))

(define-public crate-rgb-0.5.4 (c (n "rgb") (v "0.5.4") (h "1d4zxjnbymmv11813aggn6n4znmppriimsl8yjgm1r59svg1x0yv") (y #t)))

(define-public crate-rgb-0.5.5 (c (n "rgb") (v "0.5.5") (h "0mjcdfmmpzsghi2zji14fpq9j050ch5sfn452hc4z3jcip0gxxvm") (y #t)))

(define-public crate-rgb-0.5.6 (c (n "rgb") (v "0.5.6") (h "0rp6xpp8dy869mk8r73hsd7sac5wr7l19nsn1c579vxz3vf0rmp4") (y #t)))

(define-public crate-rgb-0.5.7 (c (n "rgb") (v "0.5.7") (h "052aa5bk993ls5k2zsw377nz1hvh1fc4vyy4f94lzfrdx873bdad") (y #t)))

(define-public crate-rgb-0.5.8 (c (n "rgb") (v "0.5.8") (h "1qm3h3lb8634z10vlxp5dh8z91fij7whv7b5jxa5rpdc2q684f57") (y #t)))

(define-public crate-rgb-0.7.0 (c (n "rgb") (v "0.7.0") (h "0wl46l4zpcmd2lfqsm0wv6pz03q9zg3lvzgggykk3v71h9g2nxym") (y #t)))

(define-public crate-rgb-0.7.1 (c (n "rgb") (v "0.7.1") (h "0zkkp2jjbkiwn3fq16mizldhiaiykq1jx2nc67lmq8nxh50xbrxi") (y #t)))

(define-public crate-rgb-0.7.2 (c (n "rgb") (v "0.7.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1j0ihmqjaj10kvgqmqfx13jzdn7x4vxjmch891v09ic2ixgbdwc6") (y #t)))

(define-public crate-rgb-0.8.0 (c (n "rgb") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1b2f5j24rvmjcqpf1q0wiaac9kjm23w6m71469ij9cw3k9z7m67v") (y #t)))

(define-public crate-rgb-0.8.1 (c (n "rgb") (v "0.8.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1yckvd6hif86x6cv51rfx312c8wlmgqbm78i3ylmyx09vanwipg5") (y #t)))

(define-public crate-rgb-0.8.2 (c (n "rgb") (v "0.8.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1vyf977vw4imk8cyf9x13xk8hkb7ahbh1mxq895z6wk8fsgc93bs") (y #t)))

(define-public crate-rgb-0.8.3 (c (n "rgb") (v "0.8.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "16v83zhkfnab5dfpzvms2fbzav10nwx18br6lsg3df2a6zi0r9w0") (y #t)))

(define-public crate-rgb-0.8.4 (c (n "rgb") (v "0.8.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1yrdk4lr1wzhm2zknz1xmmjqsx9sz1npz3kh19l3gprkpc51rj5c") (y #t)))

(define-public crate-rgb-0.8.5 (c (n "rgb") (v "0.8.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "046c9y04jy848jr4p9xnfy9b4cjqrpr0p345l4g4jcj2akz5z3ms") (y #t)))

(define-public crate-rgb-0.8.6 (c (n "rgb") (v "0.8.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0kpzysq2k85s5drxyx61xyasv736d34fj5hcm5qnhz28jhy5fhax") (y #t)))

(define-public crate-rgb-0.8.7 (c (n "rgb") (v "0.8.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1zk8ilrc2n3dy4mhrv8nw24yz6hw258jkvhkz5b1j8s4hs2hj9va") (y #t)))

(define-public crate-rgb-0.8.8 (c (n "rgb") (v "0.8.8") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0y3v9f67id0gyi5nil4lj5sf19z26nf4p2319zvlszy379wkqz12") (y #t)))

(define-public crate-rgb-0.8.9 (c (n "rgb") (v "0.8.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "186mgjg81jdn80rfjka46kxmci4ni7fannwbldy664nsq587mnzv") (y #t)))

(define-public crate-rgb-0.8.10 (c (n "rgb") (v "0.8.10") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01m5x7hhpxwj563gfnlfzj4rkw80x8xmqnxc49ay87dhwl8cw3n5") (y #t)))

(define-public crate-rgb-0.8.11 (c (n "rgb") (v "0.8.11") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1l9d8b2pqfqi44jid8pmxkpivyhpi8p52n8sj7b84i5jb3dfnaq0") (y #t)))

(define-public crate-rgb-0.8.12 (c (n "rgb") (v "0.8.12") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dsanbb3h8qxpp86bm3wb6061cklbj55wk9j60j02q2jmc1zf2b9") (y #t)))

(define-public crate-rgb-0.8.13 (c (n "rgb") (v "0.8.13") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "14lwlnkyjbkhiz607q3vp73nch2k58bwcplkc8maixc7r999c22g") (y #t)))

(define-public crate-rgb-0.8.14 (c (n "rgb") (v "0.8.14") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1nzg8809n0p7g3giq3ca8wi77kcpzv1cihzq07i2kl8l281y9290") (y #t)))

(define-public crate-rgb-0.8.16 (c (n "rgb") (v "0.8.16") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1wzy1124lp4lq2hhs6b1l4jh3lpgz7viq5kf4qg12zmjy0napi2y") (f (quote (("argb")))) (y #t)))

(define-public crate-rgb-0.8.17 (c (n "rgb") (v "0.8.17") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "158xf14y7lf6yfxy9j3lzm3mkadalr7pf5jkyijyf34vcbyq6nx8") (f (quote (("argb")))) (y #t)))

(define-public crate-rgb-0.8.18 (c (n "rgb") (v "0.8.18") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "15hrf0gb1l1a8rhx7b6gzrcmb43xlll4r1ym0mq55c7068dhc537") (f (quote (("argb")))) (y #t)))

(define-public crate-rgb-0.8.19-rc (c (n "rgb") (v "0.8.19-rc") (d (list (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1x4wy9mz0gnfyxcdz7k3nlamj70hl40dg5ahix6bnp5sy17vkswy") (f (quote (("argb")))) (y #t)))

(define-public crate-rgb-0.8.19-rc.2 (c (n "rgb") (v "0.8.19-rc.2") (d (list (d (n "plain") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "180ifc0k1mn29rm0750fhcwlxram510a01naw8r95nczi2nbd5cy") (f (quote (("default" "as-bytes") ("as-bytes" "plain") ("argb")))) (y #t)))

(define-public crate-rgb-0.8.20 (c (n "rgb") (v "0.8.20") (d (list (d (n "bytemuck") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1620mn5dp1rr9fpvd9wbr3b8l2g4zrij8zjri1x34cg1bas59vwh") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb")))) (y #t)))

(define-public crate-rgb-0.8.21 (c (n "rgb") (v "0.8.21") (d (list (d (n "bytemuck") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1rxyi9i5m3wazic5c74lzxq0n9s15v59k0106jdgc04h3zralksw") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb")))) (y #t)))

(define-public crate-rgb-0.8.22 (c (n "rgb") (v "0.8.22") (d (list (d (n "bytemuck") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0cq9qlrlcx60vg4kywzklv7vbmw58ifbx8s0s4lpj2q4zkrm179k") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb")))) (y #t)))

(define-public crate-rgb-0.8.23 (c (n "rgb") (v "0.8.23") (d (list (d (n "bytemuck") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "059gx4ri9jv2yk4c40kcjrbm5yqwp7yr4alckz3hc2mzcxhvqb2m") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb")))) (y #t)))

(define-public crate-rgb-0.8.24 (c (n "rgb") (v "0.8.24") (d (list (d (n "bytemuck") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0akqfpil1m2isxya3pzkf5f1i3ariyxj7azz5617j0rkxg8clrkl") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb")))) (y #t)))

(define-public crate-rgb-0.8.25 (c (n "rgb") (v "0.8.25") (d (list (d (n "bytemuck") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1lrv3x5h1lvdzg1qqr8aiysz978m35zpjdkyicnvkarnh8zkqzr8") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb")))) (y #t)))

(define-public crate-rgb-0.8.27 (c (n "rgb") (v "0.8.27") (d (list (d (n "bytemuck") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1w2c2jbrd26bdcvlz9kcxxh1z1d1w43w7pxdfqbms5166srb7pcg") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb"))))))

(define-public crate-rgb-0.8.28 (c (n "rgb") (v "0.8.28") (d (list (d (n "bytemuck") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0q6636ls9672gzh8msq9nanxk6s4ix0dslbbvbv8z9xgqg2byqpi") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb")))) (y #t)))

(define-public crate-rgb-0.8.29 (c (n "rgb") (v "0.8.29") (d (list (d (n "bytemuck") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "134pgp4m0w24g6mvmyw51adzfax7jnimb9fla8gr9qp3n4xs0zx2") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb"))))))

(define-public crate-rgb-0.8.30 (c (n "rgb") (v "0.8.30") (d (list (d (n "bytemuck") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "0v76prjw7bb2wjn7snpca4v1cnbhawrsk5v7sy58yqn46hmqba88") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb"))))))

(define-public crate-rgb-0.8.31 (c (n "rgb") (v "0.8.31") (d (list (d (n "bytemuck") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "1wah7j52d0da3lhmzixnaljymy84a3q68yqwikcwvzg5l3wlldws") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb"))))))

(define-public crate-rgb-0.8.32 (c (n "rgb") (v "0.8.32") (d (list (d (n "bytemuck") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "1inb3iwss6zzbzgvzkjcflr54kvnlm5v04ywzvdsf94g1lhxqkz7") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb"))))))

(define-public crate-rgb-0.8.33 (c (n "rgb") (v "0.8.33") (d (list (d (n "bytemuck") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "0d2syrzxdqg58gnwzabcmy7brrhdpj9fqzlm7ggjjjlyapg23cn3") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb"))))))

(define-public crate-rgb-0.8.34 (c (n "rgb") (v "0.8.34") (d (list (d (n "bytemuck") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "1cwdif1zh6nbk3v5a8nk1gkfsn4s1qid21jskgvl89m83kbvf0rn") (f (quote (("default" "as-bytes") ("as-bytes" "bytemuck") ("argb"))))))

(define-public crate-rgb-0.8.35 (c (n "rgb") (v "0.8.35") (d (list (d (n "bytemuck") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "1qvw9bfyggq9x04klbz3g3y98cfd9ca0h53inybbdkaicpvar5bl") (f (quote (("grb") ("default" "as-bytes") ("as-bytes" "bytemuck") ("argb"))))))

(define-public crate-rgb-0.8.36 (c (n "rgb") (v "0.8.36") (d (list (d (n "bytemuck") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "0ncgzkgifbyfx7vpnygfl4mgpdhhbaywxybx6pnjraf77wz2vv10") (f (quote (("grb") ("default" "as-bytes") ("as-bytes" "bytemuck") ("argb"))))))

(define-public crate-rgb-0.8.37 (c (n "rgb") (v "0.8.37") (d (list (d (n "bytemuck") (r "^1.7.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "1n275hng7hmnzjavmdf24vqd86nm6bkg80nhr4zmgzb49c0aiah5") (f (quote (("grb") ("default" "as-bytes") ("as-bytes" "bytemuck") ("argb"))))))

