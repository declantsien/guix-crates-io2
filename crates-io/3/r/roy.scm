(define-module (crates-io #{3}# r roy) #:use-module (crates-io))

(define-public crate-roy-0.1.0 (c (n "roy") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)))) (h "0mcd5kz4vr2rabxxzzsbswpqyb3hi0czncizp55dqi2nhbky25gs")))

(define-public crate-roy-0.1.1 (c (n "roy") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)))) (h "1322x25y72fnqiwncr9l7rpc53q87prd658s7n268y7dviqw2vkj")))

(define-public crate-roy-0.1.2 (c (n "roy") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)))) (h "0zwsx58srw7fqcral2b5akkhy6w1ym8qxw304mfdfgi7d2xyiplg")))

(define-public crate-roy-0.1.3 (c (n "roy") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)))) (h "12kjak9h7v7p95pa4z2akr3nl2gqswy2cbjnra4hgfpzzfdprk7m")))

(define-public crate-roy-0.1.4 (c (n "roy") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 0)))) (h "1ym21l65pyb7pg3zfrlbg35w6bgkzg8p1mwvgjbj2gingpqqhnhp")))

