(define-module (crates-io #{3}# r rls) #:use-module (crates-io))

(define-public crate-rls-0.122.0 (c (n "rls") (v "0.122.0") (h "0mjwaq7bphb7ly6qgm5lr0ic4fn36k4g48ma98392mcn01gb1wy5")))

(define-public crate-rls-0.122.1 (c (n "rls") (v "0.122.1") (h "1gv3p7phvbzyr4r40bbfyg3qb0x7g9pjdkqp8940162xksalr1sn")))

(define-public crate-rls-0.122.2 (c (n "rls") (v "0.122.2") (h "18an9hp8v68rwc5i2939h1fpjdfi4y2317c46yafv7j2rb6dfixg")))

