(define-module (crates-io #{3}# r rxh) #:use-module (crates-io))

(define-public crate-rxh-0.1.0 (c (n "rxh") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "hyper") (r "^1.0.0-rc.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "136jaxy6rw5rbq2xla0913ydalq8xpjv1m2m22ha852cmggn8kwk")))

