(define-module (crates-io #{3}# r rtr) #:use-module (crates-io))

(define-public crate-rtr-0.1.0 (c (n "rtr") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0iim0p53v8k6xkr5sb90807lda2nwrd6drwwi53g6hs87y8rfqk5")))

(define-public crate-rtr-0.1.1 (c (n "rtr") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0lvksl3ss0djp75q705llj6nd4a6z4zk9m2rxpd8mz8cvlrbvfbb")))

(define-public crate-rtr-0.1.2 (c (n "rtr") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "string_format") (r "^0.1.0") (d #t) (k 0)))) (h "0nj22m7ac13s24xdcy8alv0dm2lqwzyav68mq8kjf71qfhs0xcg0")))

