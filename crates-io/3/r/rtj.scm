(define-module (crates-io #{3}# r rtj) #:use-module (crates-io))

(define-public crate-rtj-0.0.1 (c (n "rtj") (v "0.0.1") (h "0ijcaq9kz1fzcldm3h033nir330kmqfgilm8a3y6c9hgcgjshf5w")))

(define-public crate-rtj-0.0.1+relicense (c (n "rtj") (v "0.0.1+relicense") (h "1k60z5bp29s6z4akv0pmyx72nw46kj8di6cnarviz1a9zv4rpv12")))

(define-public crate-rtj-0.1.0 (c (n "rtj") (v "0.1.0") (d (list (d (n "crypto_box") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yvp0vh87affbiprrqn58863dyqs3lqsamz5v62yb413k1793w2x")))

(define-public crate-rtj-0.1.1 (c (n "rtj") (v "0.1.1") (d (list (d (n "crypto_box") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vsnjycjjd9kd88spqxz1jcilhz665jvjnb1j2x3042znh21p8yr")))

(define-public crate-rtj-0.1.2 (c (n "rtj") (v "0.1.2") (d (list (d (n "crypto_box") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "16izr848n7p012k59jz8bzrmkmkby5b36agmls7zhi7bamxi0r50")))

(define-public crate-rtj-0.2.0 (c (n "rtj") (v "0.2.0") (d (list (d (n "crypto_box") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "029h7fm9blb9q69pw3vcbkgg4ymbycljxjck7b6w2pwijlc7nb7h")))

(define-public crate-rtj-0.3.0 (c (n "rtj") (v "0.3.0") (d (list (d (n "crypto_box") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1max0rgxcf7f2v616c8nmq7rk57ph6870i2l1cvykyb7kkqlyd59")))

(define-public crate-rtj-0.3.1 (c (n "rtj") (v "0.3.1") (d (list (d (n "crypto_box") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rmp-serde") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0k01cp6qhhxz0dqx419fb1724q3kcad92f156s63iwfszap3h50k")))

