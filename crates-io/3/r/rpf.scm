(define-module (crates-io #{3}# r rpf) #:use-module (crates-io))

(define-public crate-rpf-0.1.0 (c (n "rpf") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.6.1") (d #t) (k 0)))) (h "1gf4gn2q7hb9lsry8b4k7lw1b2ssb593x1z1ai49w7kc3bz8dpvn")))

(define-public crate-rpf-0.1.1 (c (n "rpf") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.6.1") (d #t) (k 0)))) (h "1lj513hp7m9j8gnsx00a24sp8f6sgnv3qwgpv163qi38ip2af050")))

(define-public crate-rpf-0.1.2 (c (n "rpf") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.6.1") (d #t) (k 0)))) (h "1z2lr217rdp4wh5845vy12vf874zpb4g60jg6djhq12lw3mn5jll")))

(define-public crate-rpf-0.1.3 (c (n "rpf") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.6.1") (d #t) (k 0)))) (h "0miah2s6ixzv076cqn1mh8ldw7nw21xy024dka1lxyc4w4cyv3ia")))

(define-public crate-rpf-0.1.4 (c (n "rpf") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)))) (h "0garrd0dc83gm45iwf7ybr8mq7bz6dw1sasfcsm4b9pz5vm25wsf")))

(define-public crate-rpf-0.1.5 (c (n "rpf") (v "0.1.5") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)))) (h "14skd7p0b59qsj0hl307j2sbcj0gks0h36cf5gnsbz7ajq3qzys8")))

(define-public crate-rpf-0.1.6 (c (n "rpf") (v "0.1.6") (d (list (d (n "ansi_term") (r "^0.6.3") (d #t) (k 0)))) (h "0zhnpg8g9lyi7gci6xgjd3kc1arb50ah1wr769q3wmpv4bzjk98c")))

