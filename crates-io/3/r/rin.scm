(define-module (crates-io #{3}# r rin) #:use-module (crates-io))

(define-public crate-rin-0.1.0 (c (n "rin") (v "0.1.0") (d (list (d (n "clap") (r "^2.6.0") (d #t) (k 0)))) (h "0f843d5p232kdzr8fqg0ha8pg4r7n1a321b1dqy76qansc6nfiqf")))

(define-public crate-rin-0.2.0 (c (n "rin") (v "0.2.0") (d (list (d (n "clap") (r "^2.6.0") (d #t) (k 0)))) (h "0xil5hiid3c3qiyb7ghrylrknvcrdb8wsdjd9vx1glk02ds4n3pl")))

(define-public crate-rin-0.3.0 (c (n "rin") (v "0.3.0") (d (list (d (n "clap") (r "^2.6.0") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)))) (h "10aqng2m2wgy7v4c5r9swlxzjq6rcwj0bjvkndwl66f0zy6p1c1y")))

