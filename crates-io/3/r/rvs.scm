(define-module (crates-io #{3}# r rvs) #:use-module (crates-io))

(define-public crate-rvs-0.2.0 (c (n "rvs") (v "0.2.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rvs-parser") (r "^0.2") (d #t) (k 0)))) (h "1xpz7gxyarnjb932xrwshrkrjk1cj6a4s50h654p85b1p6ia8mvf")))

(define-public crate-rvs-0.3.0 (c (n "rvs") (v "0.3.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rvs-parser") (r "^0.3") (d #t) (k 0)))) (h "0rmxsb8spy0vy83idnfv5q65c4gd4spsvrp08iz9mzq8b4ahsjxg")))

(define-public crate-rvs-0.4.0 (c (n "rvs") (v "0.4.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rvs-parser") (r "^0.4") (d #t) (k 0)))) (h "1gi8aik1xg7bjgfl2a0hzicqjjykqgf9jnqjkxgj2vybyvqyy996")))

(define-public crate-rvs-0.4.1 (c (n "rvs") (v "0.4.1") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "rvs-parser") (r "^0.4") (d #t) (k 0)))) (h "1xxhyqdyjqgmkwahrhm5vvxzyc607d1fgyhlxlvf49x5ffzizzr3")))

(define-public crate-rvs-0.5.0 (c (n "rvs") (v "0.5.0") (d (list (d (n "indexmap") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "rand") (r ">=0.7.0, <0.8.0") (d #t) (k 0)) (d (n "rand_pcg") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "rvs-parser") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "14qavkkpb34ibziik0cglhc28d9g32z6q8x3xac2c5i3svv5knhp")))

