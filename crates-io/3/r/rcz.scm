(define-module (crates-io #{3}# r rcz) #:use-module (crates-io))

(define-public crate-rcz-0.1.0 (c (n "rcz") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "1qr8vkq29l37x3j513w7v5wx2j978yl661nzcdzqpp1wfdy3f8ix")))

(define-public crate-rcz-0.1.1 (c (n "rcz") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "07ib3782lm53zggz05rqqy8jnn6dg6rqwlrhg95fbr2acc8nfvcl")))

(define-public crate-rcz-0.1.2 (c (n "rcz") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "1pyc6yj7jjd6xrqgrxbl9kbf4r7ivc0afh3hcjv7681g666j69ap")))

(define-public crate-rcz-0.1.4 (c (n "rcz") (v "0.1.4") (d (list (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)))) (h "0s3qz6zwfbkll7qbdx7x3mf5l3a9vl6myy5fc13nrjjgj1avbl2n")))

(define-public crate-rcz-0.1.5 (c (n "rcz") (v "0.1.5") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "17299al2f7mqz9jlasbv92dzzdm7q57frngyhsfx4z26351mi72i")))

(define-public crate-rcz-0.1.6 (c (n "rcz") (v "0.1.6") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0a95i6l0dgfphdnp3vmkgy93s56krhhsivs8hxm3hdxpmn5g71h8")))

(define-public crate-rcz-0.1.7 (c (n "rcz") (v "0.1.7") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "15vfx67f04rzpr2bhzagwlj071z4n46yf5vmhbsslrgxkzw8ll3i")))

(define-public crate-rcz-0.2.7 (c (n "rcz") (v "0.2.7") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0b7fvhn6797nia24q5ahjmj8013ki1w7qdfqx63w1yc1lhac81n2")))

(define-public crate-rcz-0.2.8 (c (n "rcz") (v "0.2.8") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0kc6v6b8z8qxr9f7rrczfslpqk67cifv01kzm6nhr9q0as017lhi")))

(define-public crate-rcz-0.2.9 (c (n "rcz") (v "0.2.9") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "1y7f9liyvqml6p7qlxwsx2cb9rsngv1hz4q42b5ramxwgr6av449")))

(define-public crate-rcz-0.2.10 (c (n "rcz") (v "0.2.10") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0kfscri58rr4i7wq8fcd2rvbwvln8jjk6p9mi2zll56vaz40vkbz")))

(define-public crate-rcz-0.3.10 (c (n "rcz") (v "0.3.10") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0gwy1n80hx0wqa5m9x3ki27d6d4c60rl2n0fcsvzdv2j2k0p1b6l")))

(define-public crate-rcz-0.4.10 (c (n "rcz") (v "0.4.10") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0k3m0w9292x16aiyr1mfbznwby8n5kw9k2d4jxa7irmjq42ghvk1")))

(define-public crate-rcz-0.4.12 (c (n "rcz") (v "0.4.12") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0q6lakr3bra60y93r97vkbm262574wmf52wqqjgagmjyp4z6nbxs")))

(define-public crate-rcz-0.4.13 (c (n "rcz") (v "0.4.13") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0p79cmadhhw3isikid67afgd7q8fk7a9kfy49a5g56bppcp8xygj")))

(define-public crate-rcz-0.4.14 (c (n "rcz") (v "0.4.14") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "1lhjplanb0ifwmviwd3lqyccjjdajr5rwnpcx51ajbpmxz7i1if7")))

(define-public crate-rcz-0.4.15 (c (n "rcz") (v "0.4.15") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "0xjdc867kkz5327561r90q1q4xks7317rk9i0q58xk98c734gnxg")))

(define-public crate-rcz-0.4.16 (c (n "rcz") (v "0.4.16") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "1ad6n7k40xpvi62qivgvwgvazc79g3dd5413as1qack7r7znlw90")))

(define-public crate-rcz-0.4.17 (c (n "rcz") (v "0.4.17") (d (list (d (n "confy") (r "^0.5.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (f (quote ("fuzzy-select"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.144") (d #t) (k 0)))) (h "07kw8b29drizvmb4x4y0k6dnksk181cw4sz54z8vzh88672bwrnk")))

