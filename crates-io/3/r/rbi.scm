(define-module (crates-io #{3}# r rbi) #:use-module (crates-io))

(define-public crate-rbi-0.1.0 (c (n "rbi") (v "0.1.0") (h "0y48ims7psgmckn5yk561n508kgq0mr6xjrxcvwzq8pivnx6mw0p")))

(define-public crate-rbi-0.1.1 (c (n "rbi") (v "0.1.1") (h "1chfhvkb22h30im9c7yf6673n6kvxyf44lhl94rhxxhyhjwj0rpr")))

