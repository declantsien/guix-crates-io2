(define-module (crates-io #{3}# r rdp) #:use-module (crates-io))

(define-public crate-rdp-0.1.0 (c (n "rdp") (v "0.1.0") (h "1pf5pmhk5pp3gb9f7q0asjx1abqlp6hxycxbbw5lwjrd3d21wqx1")))

(define-public crate-rdp-0.1.1 (c (n "rdp") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "0gfhdd75k69zpa7xrkh8min3b45g57k6m9vnf89iymchgcjmcshn")))

(define-public crate-rdp-0.1.2 (c (n "rdp") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "16g9yk4iwcqaqpgrlgbx1av2zsd1i8jjhx8a7w0sgkaahl2n6hx0")))

(define-public crate-rdp-0.1.4 (c (n "rdp") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "1c9fk5chra9l5k3af9avvy9rh0yna8g1c2jjli4dld86nfpf15d2")))

(define-public crate-rdp-0.1.5 (c (n "rdp") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "09nxs9s0baqard0n9lkl4y2gmnxg5ma8j9yp5mjgq8n1xj2qxg4x")))

(define-public crate-rdp-0.1.6 (c (n "rdp") (v "0.1.6") (d (list (d (n "geo") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1n75xc96f5q9c4mb46vg2kzlwdgfjik6dmca6r7668nrljffv6g5")))

(define-public crate-rdp-0.2.0 (c (n "rdp") (v "0.2.0") (d (list (d (n "geo") (r "^0.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1zmqx2cqsvhzm105gsaml1scbvpxdxfl1hr1qkn7dpajhpc7jkly")))

(define-public crate-rdp-0.2.45 (c (n "rdp") (v "0.2.45") (d (list (d (n "geo") (r "^0.4.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.14") (d #t) (k 0)) (d (n "moz-cheddar") (r "^0.4.0") (d #t) (k 1)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1qji0mfj5c6znkq11x13n4q5j691sysvgly9d4xqmprl50l2irwy")))

(define-public crate-rdp-0.4.0 (c (n "rdp") (v "0.4.0") (d (list (d (n "geo") (r "^0.10.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "moz-cheddar") (r "^0.4.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.2") (d #t) (k 0)))) (h "031jha88yqjggpd8yb4mm92f852779kg9ff5wclhfxsmiwasj6pj")))

(define-public crate-rdp-0.5.0 (c (n "rdp") (v "0.5.0") (d (list (d (n "geo") (r "^0.11.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.46") (d #t) (k 0)) (d (n "moz-cheddar") (r "^0.4.0") (d #t) (k 1)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "129l7ahkkq7yvv0sp7sjj3i0xqq5k9pk3ikmbpyxf769j4q9k0z6")))

(define-public crate-rdp-0.8.0 (c (n "rdp") (v "0.8.0") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo") (r "^0.13.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0lnkpj5bar3zbvazqw129j36h8j0b5a2595il5sfsjnnx7ajdvm0")))

(define-public crate-rdp-0.8.3 (c (n "rdp") (v "0.8.3") (d (list (d (n "cbindgen") (r "^0.9.0") (d #t) (k 1)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo") (r "^0.13.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1zqpmz5iqiwcz9iy6k92pmxc7yz40gwwzxzgf5s2p9m47cmx0c69") (f (quote (("headers"))))))

(define-public crate-rdp-0.11.20 (c (n "rdp") (v "0.11.20") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "geo") (r "^0.18.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "04gz09slvbk4xb72k7g205fvfjpjyg273v7dxmgcfkcxnd6rb7aw") (f (quote (("headers"))))))

(define-public crate-rdp-0.12.0 (c (n "rdp") (v "0.12.0") (d (list (d (n "cbindgen") (r "^0.20.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "geo") (r "^0.18.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0x000n73sgspph70drmcg68n02zg5y0391li084lnhncpik2p6i7") (f (quote (("headers"))))))

(define-public crate-rdp-0.12.7 (c (n "rdp") (v "0.12.7") (d (list (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "geo") (r "^0.25.1") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "00jk43i9nfcvmkyap9y17yjs3518dh2zp99q0acb3hxn7hxi0as7") (f (quote (("headers"))))))

(define-public crate-rdp-0.12.8 (c (n "rdp") (v "0.12.8") (d (list (d (n "cbindgen") (r "^0.26.0") (d #t) (k 1)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "geo") (r "^0.26.0") (d #t) (k 0)) (d (n "geo-types") (r "^0.7.10") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1x42jqgfzllzyz7igxsw5qn9jh1jypk69lpl56xb49p016f3p8mv") (f (quote (("headers"))))))

