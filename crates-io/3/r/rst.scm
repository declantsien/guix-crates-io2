(define-module (crates-io #{3}# r rst) #:use-module (crates-io))

(define-public crate-rst-0.1.3 (c (n "rst") (v "0.1.3") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color"))) (k 0)) (d (n "fern") (r "^0.3.5") (d #t) (k 0)) (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "1pfgcfhckc72aa9p9zw28s3xdcfb9g9lvrh8pv7nyn1wvbrykz6y") (y #t)))

(define-public crate-rst-0.1.4 (c (n "rst") (v "0.1.4") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "clap") (r "^2") (f (quote ("suggestions" "color"))) (k 0)) (d (n "fern") (r "^0.3.5") (d #t) (k 0)) (d (n "itertools") (r "^0.4.15") (d #t) (k 0)) (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "strfmt") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)))) (h "05bpyhgfxdrpajy1pikyvvfs1b87famjzgcxcmb7nkh6yw9njmx8") (y #t)))

(define-public crate-rst-0.2.0 (c (n "rst") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.5") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1.0") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "05zw85y2kn3wzzz08zri8xawapw17kpvsl7v1xx6vsz5vmx4srwf")))

(define-public crate-rst-0.3.0 (c (n "rst") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "rst_parser") (r "^0.3.0") (d #t) (k 0)) (d (n "rst_renderer") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "1dbdijwla0kgrvn6qhahclxh8pdr8xf8f0idabchvfjw51lki4y9")))

(define-public crate-rst-0.4.0 (c (n "rst") (v "0.4.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "quicli") (r "^0.4.0") (d #t) (k 0)) (d (n "rst_parser") (r "^0.4.0") (d #t) (k 0)) (d (n "rst_renderer") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.15") (d #t) (k 0)))) (h "0wqdvrn2qql9pcz01x40dagy3cysjvhqw5qwwrgqdsj31firqifd")))

