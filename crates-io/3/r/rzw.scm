(define-module (crates-io #{3}# r rzw) #:use-module (crates-io))

(define-public crate-rzw-0.1.0 (c (n "rzw") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1.0") (d #t) (k 0)) (d (n "mac") (r "^0.0.2") (d #t) (k 0)) (d (n "num") (r "^0.1.32") (d #t) (k 0)) (d (n "serial") (r "^0.3") (d #t) (k 0)))) (h "0kb196c0yh6aickpcs3nb723hgl9cb88h49v8bndrzcxrnjc3sgk")))

