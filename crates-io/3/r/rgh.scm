(define-module (crates-io #{3}# r rgh) #:use-module (crates-io))

(define-public crate-rgh-0.1.0 (c (n "rgh") (v "0.1.0") (d (list (d (n "async-std") (r "^0.99.12") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)))) (h "0vsi8da3xlh6qqn1ckc2bafl4rp0fsmd3i6zba7ibfxy7rvqwygz")))

(define-public crate-rgh-0.2.0 (c (n "rgh") (v "0.2.0") (d (list (d (n "async-std") (r "^1.1.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)))) (h "0j8mqafv8za9i465xlw3vqx70cd2bbjbcwlwmc0xlhzd5xjf100m")))

(define-public crate-rgh-0.2.1 (c (n "rgh") (v "0.2.1") (d (list (d (n "async-std") (r "^1.1.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)))) (h "0p59p5hmw4ms0hg01y9ij2gh074sy37iz6v140yk7x46247hgnnm")))

