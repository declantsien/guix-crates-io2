(define-module (crates-io #{3}# r rgm) #:use-module (crates-io))

(define-public crate-rgm-0.2.1 (c (n "rgm") (v "0.2.1") (d (list (d (n "derive_more") (r "^0.13.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "pest") (r "^2.0.2") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0.1") (d #t) (k 0)) (d (n "structopt") (r "^0.2.13") (d #t) (k 0)))) (h "1g9sqkpjq6ipvdvpygc1k2jnh36a6q4gfdmlm1v6a0nc9zddib1x")))

