(define-module (crates-io #{3}# r rte) #:use-module (crates-io))

(define-public crate-rte-0.1.0 (c (n "rte") (v "0.1.0") (h "0bvv68k3y03nb1d39n41ln5x6g69j49yajbbpayy9flzhfb8gq58")))

(define-public crate-rte-0.1.1 (c (n "rte") (v "0.1.1") (h "0d5hp254gpx9jksv6w26n18ya8269svdlc0v545kwpfd9fc6k5ka")))

(define-public crate-rte-0.1.2 (c (n "rte") (v "0.1.2") (h "1np5i5pvjhb2k7bvh1xn05d7agaq54l9v2nr4jc39n6sg925xqsv")))

(define-public crate-rte-0.1.3 (c (n "rte") (v "0.1.3") (h "143k68fr7c520s1q2gyaknf6s2a0gh6nwhp685bnxnys8ijqkd6g")))

(define-public crate-rte-0.2.0 (c (n "rte") (v "0.2.0") (h "1cadqad19qr4s56j1g5fimkwdcdp31avin2b2hhijz00hlgcfcjx")))

(define-public crate-rte-0.2.1 (c (n "rte") (v "0.2.1") (h "1y1p54y4yp2nlb4vhhjc8y7a1i2afx83zz4yc3c6xw14w6pw93q2")))

