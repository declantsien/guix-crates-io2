(define-module (crates-io #{3}# n ncw) #:use-module (crates-io))

(define-public crate-ncw-0.0.1 (c (n "ncw") (v "0.0.1") (d (list (d (n "hound") (r "^3.5.0") (d #t) (k 2)))) (h "1i80zhj8kg7z9xsf18kfblyma4q9241wic5pjf8hi0rpby1a1xas")))

(define-public crate-ncw-0.1.0 (c (n "ncw") (v "0.1.0") (h "1kxzivh0pc0mmk6dqis75i14s4cqkhkxb4pckcngibgdlbwfm2ji")))

(define-public crate-ncw-0.1.1 (c (n "ncw") (v "0.1.1") (h "0s8kn3q3bmiis10zm84g2zh3z5skdbb4ym6ygg3iygjc8hivpcf5")))

(define-public crate-ncw-0.1.2 (c (n "ncw") (v "0.1.2") (h "0agvfhm2vkn341af7igif68xqz7y96hyjl5ccf91cpqsfl41q0ag")))

