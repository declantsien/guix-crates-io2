(define-module (crates-io #{3}# n nfd) #:use-module (crates-io))

(define-public crate-nfd-0.0.1 (c (n "nfd") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "0qgi92n2ci74m7xmfd3rrv7rfxf80qwmk070zwfza3j7v3aji67c")))

(define-public crate-nfd-0.0.3 (c (n "nfd") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0aicbfak8bk6iz5fswxk3qwbawxp3kszl8120c7w07qd5103sdk9")))

(define-public crate-nfd-0.0.4 (c (n "nfd") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "161f0iavpx07irlaq68b8np7j4qgl5ns97xmqn8s9j3b44y2wxcf")))

