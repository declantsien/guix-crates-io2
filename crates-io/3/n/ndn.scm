(define-module (crates-io #{3}# n ndn) #:use-module (crates-io))

(define-public crate-ndn-0.0.0 (c (n "ndn") (v "0.0.0") (h "0qvp2q81kg4j1i0kbzf1k1nl574llqzk74h82ysz08w1i4jv0501")))

(define-public crate-ndn-0.0.1 (c (n "ndn") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "1xvizy9vlxpxlgap9cx1mj28asp0bikwnrcakdbff9kws4na2v8d") (f (quote (("pedantic"))))))

