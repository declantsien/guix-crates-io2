(define-module (crates-io #{3}# n ngx) #:use-module (crates-io))

(define-public crate-ngx-0.3.0-beta (c (n "ngx") (v "0.3.0-beta") (d (list (d (n "nginx-sys") (r "^0.1") (d #t) (k 0)))) (h "01sxb8ijvw0hpk5qyfqn18yzc2n7mz5dnjzh5jfixxjmpjlm4jf8")))

(define-public crate-ngx-0.4.0-beta (c (n "ngx") (v "0.4.0-beta") (d (list (d (n "nginx-sys") (r "^0.2") (d #t) (k 0)))) (h "0kpnnw7j0j48c4s48lirfkbz7qs4d6mbdnvh94wmspfyw3cwb7ms")))

(define-public crate-ngx-0.4.1 (c (n "ngx") (v "0.4.1") (d (list (d (n "nginx-sys") (r "^0.2.1") (d #t) (k 0)))) (h "0c0qq9kkfqdq4mm5v2hhz566x3nhv6zh3xc1w5qh4vlji1v4klva")))

