(define-module (crates-io #{3}# n nlp) #:use-module (crates-io))

(define-public crate-nlp-0.1.0 (c (n "nlp") (v "0.1.0") (d (list (d (n "clippy") (r ">= 0.0.23") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.4.3") (d #t) (k 0)))) (h "1w4dnqka52wj90qm4h37z7h2jff13j0hxzzff724dxx762770pzk") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-nlp-0.1.1 (c (n "nlp") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.35") (o #t) (d #t) (k 0)) (d (n "itertools") (r "^0.4.3") (d #t) (k 0)))) (h "116rlhqw23jv3sv7cyv520avaf55cp476qck1ifd0cdn8isy8i1i") (f (quote (("dev" "clippy") ("default"))))))

