(define-module (crates-io #{3}# n nnf) #:use-module (crates-io))

(define-public crate-nnf-0.1.0 (c (n "nnf") (v "0.1.0") (d (list (d (n "dot-writer") (r "^0.1") (o #t) (d #t) (k 0)))) (h "13i7fz8cdd8nfhmbpnccgm63anpka07wg9cncpia3zpvw3ncdvsx") (f (quote (("graphviz" "dot-writer") ("default" "graphviz"))))))

