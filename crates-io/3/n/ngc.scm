(define-module (crates-io #{3}# n ngc) #:use-module (crates-io))

(define-public crate-ngc-0.1.0 (c (n "ngc") (v "0.1.0") (d (list (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)))) (h "1xq7namrl1g2s5451rw9aq658fg9bzh1lbphsamqpg4279lf51vd") (f (quote (("eval") ("default"))))))

(define-public crate-ngc-0.2.0 (c (n "ngc") (v "0.2.0") (d (list (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)))) (h "0jjpd3hyaxpgq1b9p4y3iplw8nxfx1qbafx74ym9bwibbhjkackd") (f (quote (("eval") ("default"))))))

(define-public crate-ngc-0.2.1 (c (n "ngc") (v "0.2.1") (d (list (d (n "fixedbitset") (r "^0.1") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "strum") (r "^0.15") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15") (d #t) (k 0)))) (h "1b2wdnmjnhilwx3mgnyfpjz8jb398jyxxvplp2gd8nwb61p209zb") (f (quote (("eval") ("default"))))))

(define-public crate-ngc-0.2.2 (c (n "ngc") (v "0.2.2") (d (list (d (n "fixedbitset") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "strum") (r "^0.19") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19") (d #t) (k 0)))) (h "05vc4v82hvw5y1m1vl8faz4cr0b3kiqkq22rviwkmykqgasvrpg4") (f (quote (("eval") ("default"))))))

(define-public crate-ngc-0.2.3 (c (n "ngc") (v "0.2.3") (d (list (d (n "fixedbitset") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "strum") (r "^0.19") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19") (d #t) (k 0)))) (h "16yyfz5khw9v73mjfk6ihsbl0fq9j4671gn4mi99pw47xq34mygb") (f (quote (("eval") ("default"))))))

(define-public crate-ngc-0.2.4 (c (n "ngc") (v "0.2.4") (d (list (d (n "fixedbitset") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "1shszc4vm7l1nzxcdm60m0gl2kssj5fjcrjnw353rxd6i4qqpd2j") (f (quote (("eval") ("default"))))))

(define-public crate-ngc-0.2.5 (c (n "ngc") (v "0.2.5") (d (list (d (n "fixedbitset") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0r503xw8g46bmq28hwx0m25fjj2d7ily6bds909411lggy2bxagd") (f (quote (("eval") ("default"))))))

(define-public crate-ngc-0.2.6 (c (n "ngc") (v "0.2.6") (d (list (d (n "fixedbitset") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)))) (h "0y4hb5g5776wnlbf11ljmk204aa8gz3bij6a36hm0j99iznbsh1r") (f (quote (("eval") ("default"))))))

