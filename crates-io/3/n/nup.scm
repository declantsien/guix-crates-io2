(define-module (crates-io #{3}# n nup) #:use-module (crates-io))

(define-public crate-nup-0.1.0 (c (n "nup") (v "0.1.0") (d (list (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0cll7x5fbi5r0xpknc73z33jrgckp5bzxsvc0hzypsrlv8b4grwq")))

