(define-module (crates-io #{3}# n n2k) #:use-module (crates-io))

(define-public crate-n2k-0.1.1-alpha.1 (c (n "n2k") (v "0.1.1-alpha.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (d #t) (k 0)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)))) (h "1qb9h7zzpqfxsn4d4ayxcqwfix1hn786qxr34rcz8s1z71dd04ys") (y #t)))

