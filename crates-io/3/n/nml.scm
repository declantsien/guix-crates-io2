(define-module (crates-io #{3}# n nml) #:use-module (crates-io))

(define-public crate-nml-0.1.0 (c (n "nml") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.160") (d #t) (k 0)))) (h "0dxblac0j7jmp86jwr6l766zm6jkhki9vwdg8fwa5dk215lxbh28")))

(define-public crate-nml-0.2.0 (c (n "nml") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.160") (d #t) (k 0)))) (h "0ann8xn1zsi392qqldykfwvqrib52syx1vk4kga8hszpms3hp01x")))

