(define-module (crates-io #{3}# n nyx) #:use-module (crates-io))

(define-public crate-nyx-0.0.0 (c (n "nyx") (v "0.0.0") (h "1sm7a9x66aqlanmv8sdarhn68v6h7yy925xxrbgg6ffaavydy0zb") (y #t)))

(define-public crate-nyx-0.1.0 (c (n "nyx") (v "0.1.0") (h "1jkpayk3kf75lfqa3ziw3gwwr1swppdivigfry2xh3axdg3an7px") (y #t)))

(define-public crate-nyx-0.1.1 (c (n "nyx") (v "0.1.1") (h "0wy61s6krr0gcy5xlqvwnvs5n13s95z24x17a2s0dv4acx7r3xfr") (y #t)))

(define-public crate-nyx-0.2.0 (c (n "nyx") (v "0.2.0") (h "1c6df4wzbviaj4kvih0z1al7vrc7fla51s0066gly8xjwj3bydma") (y #t)))

(define-public crate-nyx-0.3.0 (c (n "nyx") (v "0.3.0") (h "06cyby81mlazm5a4d7ks3g5h4rl9g509zazrbqazqcys0z8a3rpv") (y #t)))

(define-public crate-nyx-0.4.0 (c (n "nyx") (v "0.4.0") (h "1rj8ys5c0q0cny1kbi3vh6wapawhv6yyssy89wlnaiz2flhfaasz") (y #t)))

(define-public crate-nyx-0.5.0 (c (n "nyx") (v "0.5.0") (h "1zjc5cgcsbr267vabpd6vdlva80x7c1293l2hs67jz9wzfz1jwyp") (y #t)))

(define-public crate-nyx-0.6.0 (c (n "nyx") (v "0.6.0") (h "16d195i3qapr9faixb4c5l48qsqgygnrjc87ksmngn5rglkhg3bh") (y #t)))

(define-public crate-nyx-0.7.0 (c (n "nyx") (v "0.7.0") (h "0n2vy4rh99b750l3wlzgdamsmqvrwbg6xpwkv6gh59rm1is91vbz") (y #t)))

(define-public crate-nyx-1.0.0 (c (n "nyx") (v "1.0.0") (h "1nn7bz8x4p9hxwmvjwp4dkg0p40d0b0xbp6hi49r2bvc2db63nqv") (y #t)))

(define-public crate-nyx-1.0.1 (c (n "nyx") (v "1.0.1") (h "1qd6xnscnxrwwzibk8l4fkjhv0cy522al1xgm7rv61psyx92ka9c") (y #t)))

(define-public crate-nyx-2.0.0 (c (n "nyx") (v "2.0.0") (d (list (d (n "byteorder") (r "^1.3.4") (k 0)) (d (n "hmac") (r "^0.9.0") (k 0)) (d (n "sha-1") (r "^0.9.1") (k 0)))) (h "0kab838zz06hizmmhzr7q1l6wx7qi9r28laxg8zcn1s78ssdxviy")))

(define-public crate-nyx-2.0.1 (c (n "nyx") (v "2.0.1") (d (list (d (n "hmac") (r "^0.9.0") (k 0)) (d (n "sha-1") (r "^0.9.1") (k 0)))) (h "0231nc2vxpf7kibfzzwhz980ili3cv8f0xi6ql869sqsbgx2r3zx")))

(define-public crate-nyx-2.1.0 (c (n "nyx") (v "2.1.0") (d (list (d (n "hmac") (r "^0.9") (k 0)) (d (n "sha-1") (r "^0.9") (k 0)))) (h "1wcsf8cfjw7d4ks1yb7s88d2w3q2j92gjbx4snkd4j9xv2gzqx6v")))

(define-public crate-nyx-2.1.1 (c (n "nyx") (v "2.1.1") (d (list (d (n "hmac") (r "^0.10") (k 0)) (d (n "sha-1") (r "^0.9") (k 0)))) (h "1lf4hgi2ynhlm0rb9axdkfhrz0lkgz3vnx8i7g4527ikzjc9sdgq")))

(define-public crate-nyx-2.2.0 (c (n "nyx") (v "2.2.0") (d (list (d (n "hmac") (r "^0.11") (k 0)) (d (n "sha-1") (r "^0.9") (k 0)))) (h "1dx86qdk1vxd4w9rcqv8f0gly9lgddm266jpcp68af7rj0ac7nfy")))

(define-public crate-nyx-2.2.1 (c (n "nyx") (v "2.2.1") (d (list (d (n "hmac") (r "^0.12") (k 0)) (d (n "sha-1") (r "^0.10") (k 0)))) (h "1843rx7fyfx9i4dvs6xq8xyrkal3jyzzlp0c5h2ja0hd6asf8qnm")))

(define-public crate-nyx-2.2.2 (c (n "nyx") (v "2.2.2") (d (list (d (n "hmac") (r "^0.12") (k 0)) (d (n "sha1") (r "^0.10") (k 0)))) (h "015x6mcgvlcl5r4dkbqmfsv76fbcs4k11rmf9iyj9vnr2zckybf0")))

