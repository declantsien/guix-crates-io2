(define-module (crates-io #{3}# n nng) #:use-module (crates-io))

(define-public crate-nng-0.0.0 (c (n "nng") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nng-sys") (r "^0.0.0") (d #t) (k 0)))) (h "1dy6s81bavmcprapv913s88j1c0b3p36zfx6maw1rw5v2hqz5q20")))

(define-public crate-nng-0.1.0 (c (n "nng") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "nng-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0w2k6wyka79jgfhxyy2zw8f3zhgvyn717bs1fwkhh22slhldw5ym") (y #t)))

(define-public crate-nng-0.2.0 (c (n "nng") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "nng-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1hzjbckk32cgdgjsjywwlb5izhhbm6fa37s000vlgxv9x80vw0lm") (y #t)))

(define-public crate-nng-0.2.1 (c (n "nng") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "nng-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0p1vm28cfl7dcmlgxc7r7hjm1nw8g0pnyhp33i4ng9s1h1q81kiy")))

(define-public crate-nng-0.3.0 (c (n "nng") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^0.1.2") (k 0)))) (h "1vs8p7rrjb1p3ml4h6f0vcw8fpygzaz06kcyhzsdkcw7ak4vvf9g") (f (quote (("default" "build-nng") ("build-nng" "nng-sys/build-nng"))))))

(define-public crate-nng-0.4.0 (c (n "nng") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^0.1.2") (k 0)))) (h "0vg8idz35l6ldp2ci22n0mhnv7nm6bfr118drqvpx6qwj9vsja5h") (f (quote (("default" "build-nng") ("build-nng" "nng-sys/build-nng"))))))

(define-public crate-nng-0.5.0-rc.1 (c (n "nng") (v "0.5.0-rc.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^1.1.1-rc.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("winnt" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "052788vim35m4m8zsph1ck8ch7yihb0y5gadfdl71qd3yrz4pw30") (f (quote (("default" "build-nng") ("build-nng" "nng-sys/build-nng"))))))

(define-public crate-nng-0.5.0-rc.2 (c (n "nng") (v "0.5.0-rc.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^1.1.1-rc.1") (k 0)) (d (n "winapi") (r "^0.3.6") (f (quote ("winnt" "std"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1hr91z3m5dpvvy8xmr21aq0yxap1f5qrj9d5jl524da72blw6wyy") (f (quote (("default" "build-nng") ("build-nng" "nng-sys/build-nng"))))))

(define-public crate-nng-0.5.0-rc.3 (c (n "nng") (v "0.5.0-rc.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^1.1.1-rc.1") (k 0)))) (h "1dhvrddkqxmzciyzvc33c6iaixl3xv20nk7s9rf1lp3vihn9i77s") (f (quote (("ffi-module") ("default" "build-nng") ("build-nng" "nng-sys/build-nng"))))))

(define-public crate-nng-0.5.0 (c (n "nng") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^1.1.1-rc.1") (k 0)))) (h "0rffqajnyrbkm3bfyhqfl2cil8g3jasbns77dmbsq2a65yg6b05r") (f (quote (("ffi-module") ("default" "build-nng") ("build-nng" "nng-sys/build-nng"))))))

(define-public crate-nng-0.5.1 (c (n "nng") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^1.1.1-rc.1") (k 0)))) (h "09n4fxs12w2szcqnq32l3a1ckd7i49pyp644z2wz4si911cp0cvb") (f (quote (("ffi-module") ("default" "build-nng") ("build-nng" "nng-sys/build-nng"))))))

(define-public crate-nng-1.0.0-rc.1 (c (n "nng") (v "1.0.0-rc.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^1.2.3-rc.1") (d #t) (k 0)))) (h "0sq8zxjs3dq2yi8w78anb5nmib4ci196lfncrcldiyqz3yvcmvz6") (f (quote (("ffi-module") ("default" "build-nng") ("build-nng" "nng-sys/build-nng" "nng-sys/nng-stats"))))))

(define-public crate-nng-1.0.0-rc.2 (c (n "nng") (v "1.0.0-rc.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^1.2.4-rc.1") (d #t) (k 0)))) (h "0rga464g4x5dbaa59vxkjrd1iixjagcc55y9bh15wz3na09c99dz") (f (quote (("ffi-module") ("default" "build-nng") ("build-nng" "nng-sys/build-nng"))))))

(define-public crate-nng-1.0.0 (c (n "nng") (v "1.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^1.4.0-rc.0") (d #t) (k 0)))) (h "1l7ng5i1cyjx1n6n39ki5ag8hixlnnxj3qgf629fdsqm2a1crakw") (f (quote (("ffi-module") ("default" "build-nng") ("build-nng" "nng-sys/build-nng"))))))

(define-public crate-nng-1.0.1 (c (n "nng") (v "1.0.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nng-sys") (r "^1.4.0-rc.0") (k 0)))) (h "00v2s4r9dfk2jwpdvs6r3pi8ppyfdpkjrsngidjz33qwxkbqx890") (f (quote (("ffi-module") ("default" "build-nng") ("build-nng" "nng-sys/build-nng"))))))

