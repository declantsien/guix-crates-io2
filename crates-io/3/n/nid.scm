(define-module (crates-io #{3}# n nid) #:use-module (crates-io))

(define-public crate-nid-0.1.0 (c (n "nid") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12351rzpd946dzdxpbjbmvxvasv4ykj9vgnil3wpl0ql45lxq9yn")))

(define-public crate-nid-1.0.0-rc.1 (c (n "nid") (v "1.0.0-rc.1") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mgdmjjlknbhii7fl582nqgc05yybh75jlhp3pkn4n3cfxc53nsf") (r "1.61")))

(define-public crate-nid-1.0.0 (c (n "nid") (v "1.0.0") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0azmw57svl0z4zlnzzhnz5n5x27bcpc83ib86sp6fqpb4mp9f8y7") (r "1.61")))

(define-public crate-nid-2.0.0-rc.1 (c (n "nid") (v "2.0.0-rc.1") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19v9yaz2z9b7qbz2m61hmv53mnrk76b71arnrhv8kfz56zjfrgyi") (r "1.61")))

(define-public crate-nid-2.0.0 (c (n "nid") (v "2.0.0") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12267yc0vkakcw0nd24g3gjkjrigq20400q3dbdybh3lr75i557m") (r "1.61")))

(define-public crate-nid-2.1.0-rc.1 (c (n "nid") (v "2.1.0-rc.1") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "1m9mjhi1wanjfsb66vp0n0dy2hmaxxg5dqfrib115bqk0q4r8rhk") (r "1.61")))

(define-public crate-nid-2.1.0 (c (n "nid") (v "2.1.0") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "0gi17k4b0k8nz8xnd3xk3iyd3d38sspn3xrww9kl92k0jmgj5h8h") (r "1.61")))

(define-public crate-nid-3.0.0-rc.1 (c (n "nid") (v "3.0.0-rc.1") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "11nwsnz7q21jcadyczi9ab3cnwf1sbzgc6dgjmf0nxi1v8lm010q") (r "1.61")))

(define-public crate-nid-3.0.0 (c (n "nid") (v "3.0.0") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (f (quote ("zeroize_derive"))) (o #t) (d #t) (k 0)))) (h "1ww57wl5kg893lmqfawsw7wk0jh6l52pzqj6jk1mvf1jk5wg3gaa") (r "1.61")))

