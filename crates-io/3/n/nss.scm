(define-module (crates-io #{3}# n nss) #:use-module (crates-io))

(define-public crate-nss-0.0.1 (c (n "nss") (v "0.0.1") (h "0fwv2a7v7xlrkwbqdz6z99jn9yq8lpc7s7k70kny16k5cbp7zq29")))

(define-public crate-nss-0.7.1 (c (n "nss") (v "0.7.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nss-sys") (r "^0.1.8") (d #t) (k 0)))) (h "0xiz9qmmq64svn9gc23v97095sspq8xg16nylyx4lr5c53anndng")))

