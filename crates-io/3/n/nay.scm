(define-module (crates-io #{3}# n nay) #:use-module (crates-io))

(define-public crate-nay-0.1.0 (c (n "nay") (v "0.1.0") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("zerocopy"))) (d #t) (k 0)))) (h "1abpkp66wq9lh38r1qmarg3764b6v0i71d0fygknx9p2904dcs87")))

(define-public crate-nay-0.2.0 (c (n "nay") (v "0.2.0") (h "0i0md57k8sb2ql2iwxy3jjir6cq2rh5lsjfa4pz7csv1wzi250qb")))

