(define-module (crates-io #{3}# n nhi) #:use-module (crates-io))

(define-public crate-nhi-0.0.1 (c (n "nhi") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)))) (h "1ld7nk0wgwynlxah6014kqn8mla2zjbh8xs9wkq2di3bsnslr2pn")))

(define-public crate-nhi-0.0.2 (c (n "nhi") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)))) (h "0msdjb4w03x0alcq8vqy0cxgw9fy8sfwc2hd41xwf09mii2aibpx")))

(define-public crate-nhi-0.1.0 (c (n "nhi") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "10hh7h4vinmfqra0mvlryzjps1h9hn9xdc1cyrp98kyxs4798882")))

(define-public crate-nhi-0.1.1 (c (n "nhi") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.9.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0y6l87j5pa9yl4cjzaisihfs5jq9cvf68gavgybp5dp5fqscypqq")))

