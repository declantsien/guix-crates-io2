(define-module (crates-io #{3}# n now) #:use-module (crates-io))

(define-public crate-now-0.1.0 (c (n "now") (v "0.1.0") (h "14sjdk7rghmajvywv80ks2wqbfpvvxks4cp25b8kdvj1vk54b3kn")))

(define-public crate-now-0.1.1 (c (n "now") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "14rg3s58smw4mdxcmjgg822n5xq5frddn2r2n8zgqk2cnilvjad2")))

(define-public crate-now-0.1.2 (c (n "now") (v "0.1.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1nzfjivgbqavgif4rpb7x2l496p8w4ds6y7kdx4vkfa78rs41ql4")))

(define-public crate-now-0.1.3 (c (n "now") (v "0.1.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "chrono") (r "^0.4") (f (quote ("clock" "std"))) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1l135786rb43rjfhwfdj7hi3b5zxxyl9gwf15yjz18cp8f3yk2bd")))

