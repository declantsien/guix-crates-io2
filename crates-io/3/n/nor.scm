(define-module (crates-io #{3}# n nor) #:use-module (crates-io))

(define-public crate-nor-0.1.0 (c (n "nor") (v "0.1.0") (h "0snx85wz28qjjj9hnqf41idwl6mq57cl62nqwdrvrfq8phskljq6")))

(define-public crate-nor-0.1.1 (c (n "nor") (v "0.1.1") (d (list (d (n "sdl2") (r "^0.34.3") (f (quote ("image"))) (k 0)))) (h "1i999b35ln6yayfb0pvf5089gpa9q03669qazsjkfyqakrrmym32")))

