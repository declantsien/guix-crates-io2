(define-module (crates-io #{3}# n nxe) #:use-module (crates-io))

(define-public crate-nxe-0.0.1 (c (n "nxe") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tui-input") (r "^0.7") (d #t) (k 0)))) (h "0mc40123izk8j1ss8kqwcnj267ar7p54x6y2fz1902a7ifvcgmfh")))

(define-public crate-nxe-0.0.5 (c (n "nxe") (v "0.0.5") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tui-input") (r "^0.7") (d #t) (k 0)))) (h "11265i18q2wbdhn3cqr3yxr90nhk9k76hhrlb3kfh01p81vx4ynj")))

(define-public crate-nxe-0.0.6 (c (n "nxe") (v "0.0.6") (d (list (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tui-input") (r "^0.7") (d #t) (k 0)))) (h "1mlmf3qnrqgzywc87dh3f58gwsfkdk9xj3qwab7iyiczlsrn3l7w")))

