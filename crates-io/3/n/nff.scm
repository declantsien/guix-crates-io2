(define-module (crates-io #{3}# n nff) #:use-module (crates-io))

(define-public crate-nff-0.1.0 (c (n "nff") (v "0.1.0") (h "1lhd5c77a2c0jmcmvm4zai0ydwff2rmbrzxwlr7hj4v9cbajjzad")))

(define-public crate-nff-0.1.1 (c (n "nff") (v "0.1.1") (h "0fhzzvsv6pyllzd2hp2k471vgyml0ad91qj0prgrvkjlxjidvdkd")))

(define-public crate-nff-0.1.2 (c (n "nff") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.1") (d #t) (k 0)))) (h "0ic20pdgp4y82is6p72q08fars0ws578666gv8qafl9585rvqfi5")))

