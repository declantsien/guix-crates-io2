(define-module (crates-io #{3}# n noa) #:use-module (crates-io))

(define-public crate-noa-0.0.1 (c (n "noa") (v "0.0.1") (d (list (d (n "backtrace") (r "^0") (d #t) (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0") (d #t) (k 2)) (d (n "structopt") (r "^0") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "096103fsq7zagqb0n91r6bphdw3yl07xjqbwgq53dw7107da6r9s")))

(define-public crate-noa-0.0.2 (c (n "noa") (v "0.0.2") (d (list (d (n "backtrace") (r "^0") (d #t) (k 0)) (d (n "dirs") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (f (quote ("max_level_trace" "release_max_level_warn"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0") (d #t) (k 2)) (d (n "signal-hook") (r "^0") (d #t) (k 0)) (d (n "structopt") (r "^0") (d #t) (k 0)) (d (n "syntect") (r "^3") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0") (d #t) (k 0)))) (h "1mckm6kfxaljxmixhsx2i2j7n6l7pgxbsai75wajdl0lga99hziz")))

