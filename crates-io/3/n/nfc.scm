(define-module (crates-io #{3}# n nfc) #:use-module (crates-io))

(define-public crate-nfc-0.1.0 (c (n "nfc") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "13iqx0jys60krgrh1w3rarj4q3xk7q2ik4zx5pf4axxvg95ypkk7")))

(define-public crate-nfc-0.1.1 (c (n "nfc") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0g0ckg4cw5bh6r65hfaziqkdlqxcxfk17d3s06yx07ral5pnf0d7")))

(define-public crate-nfc-0.1.2 (c (n "nfc") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0lgd4kflgb9yq0nyvhc2yhqb2hv8j5skq0j9gz23l448l4ks7nwj")))

(define-public crate-nfc-0.1.3 (c (n "nfc") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "15myf5d5xvj5sffr19cj3a56fbmh59xsv48rhr3b30zl6sbplc78")))

(define-public crate-nfc-0.1.4 (c (n "nfc") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1f3aspg4idc2j7sj1rrqnpfp9bnqavawkqzh4gx1w7hhdyg1viyq")))

(define-public crate-nfc-0.1.5 (c (n "nfc") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0x0cmhjbcp610yqsxc0m4nlx0vlkxkgdmcpcdqdlvawkpxlqi67m")))

(define-public crate-nfc-0.1.6 (c (n "nfc") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1m4yqj6lrx6jndskfmlz6g6hdjw15bzp4q0z6jz4vip4lpl9agjb")))

(define-public crate-nfc-0.1.7 (c (n "nfc") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1kprh2ja9x2wm7vaad69mp6dlsam24asfz24bp3ndqq3nmqcvrna")))

(define-public crate-nfc-0.1.8 (c (n "nfc") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "03dj194z1lsh05gsh0qdsav21yp1lyfjdkxbjpqij4q2n53j69xm")))

(define-public crate-nfc-0.1.9 (c (n "nfc") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.3") (d #t) (k 0)))) (h "063r3094c2ngcwkx7hc07a590nza2cq9bbkjiizm75xpbfzwnh0w")))

(define-public crate-nfc-0.1.10 (c (n "nfc") (v "0.1.10") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0rmx2cy5a06y3rcrxhbrvd52fyi0kgirg18dxz03v2yp5rk9bf1m")))

(define-public crate-nfc-0.1.11 (c (n "nfc") (v "0.1.11") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfc-sys") (r "^0.1.5") (d #t) (k 0)))) (h "1h2frx6vyw0c2acf0208gmappfim9wqzcjlrpsvx4phdm5fxblgw")))

