(define-module (crates-io #{3}# n nya) #:use-module (crates-io))

(define-public crate-nya-0.1.0 (c (n "nya") (v "0.1.0") (d (list (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1wga18glwsj7db4j2fd2m43ps378j6bkw6ilgx6vx9yyww18g8yp")))

(define-public crate-nya-0.2.0 (c (n "nya") (v "0.2.0") (d (list (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1cd8g4qzjmcmzpx13qkl4zbfkkfyidhrxpygcf1phm8bcnvikwdi")))

(define-public crate-nya-0.2.1 (c (n "nya") (v "0.2.1") (d (list (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1y6bysi0hcwwhcgsikvx913yv9s62fpr26if8019g81qax7a19gm")))

(define-public crate-nya-0.3.0 (c (n "nya") (v "0.3.0") (d (list (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1gl2k5yxgdqnrgqa4b0ikf1kcqilmn3rj6zwsg8yy3wazm8hn80v")))

(define-public crate-nya-0.3.1 (c (n "nya") (v "0.3.1") (d (list (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "06hzhzdfa0avgdg2k2jrkd3h8f7s6kvx051zn8pjfbc0xpa2ifys")))

(define-public crate-nya-0.4.0 (c (n "nya") (v "0.4.0") (d (list (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1z8847ag73jpxcm8jb1djjvj5xr2hryfs14r740bfdya6pglm0hi")))

(define-public crate-nya-0.5.0 (c (n "nya") (v "0.5.0") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1zfwy3dppnqx8625ch5irabhx6zyhsirc41dzpwhyb0ibyfmhc1y")))

(define-public crate-nya-0.5.1 (c (n "nya") (v "0.5.1") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "0pjvyg4s5b12nr8ji2h90jwpw0kjar3jmi0c7s992x78kgdv6rsn")))

(define-public crate-nya-0.5.2 (c (n "nya") (v "0.5.2") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "0cz1ccv0nnmknwlrahfc4zh1g40jcqkh57vmsri2p1ay9l5mnc8q")))

(define-public crate-nya-0.5.3 (c (n "nya") (v "0.5.3") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "18gsy4721ijzaamsqy1mwb6cr8f9by7k8blplsmz2q2n13f30vyw")))

(define-public crate-nya-0.6.0 (c (n "nya") (v "0.6.0") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "0vk8insqm7czgjzjap225md6yrlvjxj94qpd0ybjranbbk561hxj")))

(define-public crate-nya-0.7.0-rc1 (c (n "nya") (v "0.7.0-rc1") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "0m31qjxpjgxwdy22ibw31gs2ncgfw8fqyzydgaklaijbc4vn7sis")))

(define-public crate-nya-0.7.0-rc2 (c (n "nya") (v "0.7.0-rc2") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "0qm1rq8c4y6846g9ryi8f1jannrma93v1ca02i6i78akl1mh0cds")))

(define-public crate-nya-0.7.0 (c (n "nya") (v "0.7.0") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "12xwpvirhbzzd8rsb6imjgkdgkan9djbw153bswsnckgvhab7lr8")))

(define-public crate-nya-0.7.1 (c (n "nya") (v "0.7.1") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1aq8590ywikyi6vll0p09i1xvib8hadxsxmgc12xy8ivjnn8vcvs")))

(define-public crate-nya-1.0.0 (c (n "nya") (v "1.0.0") (d (list (d (n "globset") (r "^0.4.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.1.4") (d #t) (k 0)))) (h "1cqmdgndnn48q5wssjbiragabi3a4vszjriyx2sa7zlrfk95p9zf")))

