(define-module (crates-io #{3}# n nys) #:use-module (crates-io))

(define-public crate-nys-0.1.0 (c (n "nys") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.18") (f (quote ("full"))) (d #t) (k 0)))) (h "00rz622h9w12pnjjkzdbwfr9grmx8ivydi0kmhspxqbzyffiyzqj")))

