(define-module (crates-io #{3}# n ndi) #:use-module (crates-io))

(define-public crate-ndi-0.1.0 (c (n "ndi") (v "0.1.0") (h "0fc120d4cccd6cyk0xinb39lzqvpjlsim9nrqq9ls4n7lkhw1571")))

(define-public crate-ndi-0.1.1 (c (n "ndi") (v "0.1.1") (h "10893v9mka1lxsw17rr4zcixn9kqcqsj8m1q8awmv5ayz6x3cr98")))

(define-public crate-ndi-0.1.2 (c (n "ndi") (v "0.1.2") (h "13cqyg0gv1687vah3m96l29vp8r5dh6c7q1v2ir6a5v9is5vwq85")))

