(define-module (crates-io #{3}# n nbd) #:use-module (crates-io))

(define-public crate-nbd-0.1.0 (c (n "nbd") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)))) (h "0n2kpxfbvflz3psacp83qdjmq8hygir811bnjdb6nz2pwipigi7x")))

(define-public crate-nbd-0.1.1 (c (n "nbd") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "1qlka3k1dg0yaclhz4qckg1j9qa3bsfpcw608k3xw2468wsxgp0m")))

(define-public crate-nbd-0.1.2 (c (n "nbd") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "1ki4n734szmx1h9qdrclf17zwz7rlidfs0z609kw19xjkba7w2cb")))

(define-public crate-nbd-0.2.0 (c (n "nbd") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)))) (h "09vdj0nrj84rd679wjgqrlyrxf9hfmvs0pi6nj6nbccdihg0v60n")))

(define-public crate-nbd-0.2.1 (c (n "nbd") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "pipe") (r "^0.0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.8.4") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "readwrite") (r "^0.1.0") (d #t) (k 2)))) (h "1d51ibgm60kcijyzq09hpafl34bk4bxg8a5b8vw04vcgap53r996")))

(define-public crate-nbd-0.2.2 (c (n "nbd") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "pipe") (r "^0.0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.8.4") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "readwrite") (r "^0.1.0") (d #t) (k 2)))) (h "1yhrbv6b5myhkc7mrwmwgr7a547z85fn4rf2sa9nblywf0sj0rv9")))

(define-public crate-nbd-0.2.3 (c (n "nbd") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "pipe") (r "^0.0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.8.4") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "readwrite") (r "^0.1.0") (d #t) (k 2)))) (h "06m1jiiq3vmgwshhsrdq0hknjn5c6vhinlbmqw5sw5viwapb0dmf")))

(define-public crate-nbd-0.3.0 (c (n "nbd") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "pipe") (r "^0.0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.8.4") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "readwrite") (r "^0.1.0") (d #t) (k 2)))) (h "10w0qyzi8y940p9xdsclszwqjmavbrdanas3npz5wqv2i9agd3y7")))

(define-public crate-nbd-0.3.1 (c (n "nbd") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "pipe") (r "^0.0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.8.4") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "readwrite") (r "^0.1.0") (d #t) (k 2)))) (h "00609mr86iq84yna003zn0r9g96xcv8n0acabd71sczd8r2wirfv")))

