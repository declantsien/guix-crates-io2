(define-module (crates-io #{3}# n nui) #:use-module (crates-io))

(define-public crate-nui-0.0.1 (c (n "nui") (v "0.0.1") (d (list (d (n "bytes") (r "^1.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "rmp") (r "^0.8.11") (d #t) (k 0)) (d (n "rmp-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "rmpv") (r "^1.0.0") (f (quote ("with-serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.6") (f (quote ("full"))) (d #t) (k 0)))) (h "0wb9hirzhbf92cyrm9ijjghpmigjkai1kdgmfwfz4js32jkp4yhz")))

