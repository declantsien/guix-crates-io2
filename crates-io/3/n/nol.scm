(define-module (crates-io #{3}# n nol) #:use-module (crates-io))

(define-public crate-nol-0.0.1 (c (n "nol") (v "0.0.1") (h "1cyrlvx636rgb1vg60hx4rc27ffdrj7r3rpyd078gzia8qifarbf")))

(define-public crate-nol-0.0.2 (c (n "nol") (v "0.0.2") (h "1if99jismbzw741hl6wj8xgd6kf0g4h64hrybkpgmzspw4ch8kf6")))

(define-public crate-nol-0.1.0 (c (n "nol") (v "0.1.0") (d (list (d (n "eywa") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "14f13blbh244b1d70f0l3z897xmvzjhgx9pbh7m897f7w62mvg52")))

(define-public crate-nol-0.1.1 (c (n "nol") (v "0.1.1") (d (list (d (n "eywa") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "12ri9lcmfdd4pr0zzdv109dqv7xl2nczg4bv2ijlcjcw7i3qbkpc")))

(define-public crate-nol-0.1.2 (c (n "nol") (v "0.1.2") (d (list (d (n "eywa") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "00plg1vxyvi5886a0yy6iw1b70fww8bshivk8283x402p15523ap")))

(define-public crate-nol-0.1.3 (c (n "nol") (v "0.1.3") (d (list (d (n "eywa") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0c6p82rfgmbj5smwhxmhbv0yrxik8zxm8wwgxgxa5l8q74iacjv4")))

(define-public crate-nol-0.1.4 (c (n "nol") (v "0.1.4") (d (list (d (n "eywa") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1pfg4kpzw17ljzbp1i2603a474vb5rxjam4ix3agc3lrpr3d77sl")))

(define-public crate-nol-0.1.5 (c (n "nol") (v "0.1.5") (d (list (d (n "eywa") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1nfqcdfhixgfwjqa3a86sq9dab7zh5d0x9iq6rpf1abcr0yqz8pm")))

(define-public crate-nol-0.1.6 (c (n "nol") (v "0.1.6") (d (list (d (n "eywa") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "13842k9x5rw2by0wkai39lfsy2jhdmiv7h22kmy4ai2z3nfgylf2")))

(define-public crate-nol-0.1.7 (c (n "nol") (v "0.1.7") (d (list (d (n "eywa") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1sqzkc9aw2znby63hskh1hjg8pwnkg57wkrldfkmdifn0nymca22")))

(define-public crate-nol-0.1.8 (c (n "nol") (v "0.1.8") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1f21lpwsifcnjw33h7iidmmw8zrp6jwvz4hv3v2cplldp5lnq26i")))

(define-public crate-nol-0.1.9 (c (n "nol") (v "0.1.9") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "03rxxrpyv0jf8wihigpdy10xqw4c1xbhy9wpavgbi3pzwlknfbng")))

(define-public crate-nol-0.2.0 (c (n "nol") (v "0.2.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "17hf7z9ksf29wjcpr98yah8hrqsgdqr0sl7wavba0i70sk11xqa3")))

(define-public crate-nol-0.3.0 (c (n "nol") (v "0.3.0") (d (list (d (n "colored_truecolor") (r "^0.1.0") (d #t) (k 0)) (d (n "eywa") (r "^0.1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0zn3m3r2b9dic3ghy6ya3j8ghr2gg8sqbwhr5gvs2dmfnq8rcb66")))

