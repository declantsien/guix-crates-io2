(define-module (crates-io #{3}# n npy) #:use-module (crates-io))

(define-public crate-npy-0.1.0 (c (n "npy") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "npy-derive") (r "^0.1") (d #t) (k 2)))) (h "1vv73s3fa34hg5wafnjvfsnsqvffmp1l800shb4kgkrc6sq17aib")))

(define-public crate-npy-0.2.0 (c (n "npy") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "npy-derive") (r "^0.2") (d #t) (k 2)))) (h "08yzll29jzsykv1br2gnxbi9070ki720vvglvildgyww4hi2dflk")))

(define-public crate-npy-0.2.1 (c (n "npy") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "npy-derive") (r "^0.2") (d #t) (k 2)))) (h "1d2xnrbgm3fx1r8chv12nklqnny1wwl51qjf8mb9smpr7i8m3wvg")))

(define-public crate-npy-0.2.2 (c (n "npy") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "npy-derive") (r "^0.2") (d #t) (k 2)))) (h "1kw8z740g7b3w1sc1sr0cj82c9y86fshb906iap1c9a5a6y5wbls")))

(define-public crate-npy-0.2.4 (c (n "npy") (v "0.2.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "npy-derive") (r "^0.2") (d #t) (k 2)))) (h "1mxz9hp8kk1p2a1b2v579n09hpaplp1zhzg1i9qd1swjz75wkkwy")))

(define-public crate-npy-0.3.0 (c (n "npy") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "npy-derive") (r "^0.3") (d #t) (k 2)))) (h "0428p0rxy6y65dn588nw0mar49y1207p6j022m6phds3cy5w01r3")))

(define-public crate-npy-0.3.1 (c (n "npy") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "npy-derive") (r "^0.3") (d #t) (k 2)))) (h "0svvbl9y7dmd75ax8zbz62rlhjn73vvs9irn2v1i7k86vc3ng97m")))

(define-public crate-npy-0.3.2 (c (n "npy") (v "0.3.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 2)) (d (n "nom") (r "^2.1") (d #t) (k 0)) (d (n "npy-derive") (r "^0.3") (d #t) (k 2)))) (h "0a8hr976fr076y75x1jzykrpnkrhzbyk0n471hn6i3r08g6xhn9j")))

(define-public crate-npy-0.4.0 (c (n "npy") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 2)) (d (n "nom") (r "^3") (d #t) (k 0)) (d (n "npy-derive") (r "^0.4") (d #t) (k 2)))) (h "1m0yv40kpiw8m9g901ana88rv057kd7fcq8b8f6k391wsr6vp2s2")))

