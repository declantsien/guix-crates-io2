(define-module (crates-io #{3}# n nfe) #:use-module (crates-io))

(define-public crate-nfe-0.0.1 (c (n "nfe") (v "0.0.1") (d (list (d (n "parsercher") (r "^3.1.3") (d #t) (k 0)))) (h "1gdmk8pkiylclii79vajhjdxkd2n6mmfp43rdrlmw6f62gqpy1hv")))

(define-public crate-nfe-0.0.2 (c (n "nfe") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.1") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "0cgd3i19vqa3xsz455d1z4n75sv3kmmkl2r6pxnrway4zdnck0mz")))

(define-public crate-nfe-0.0.3 (c (n "nfe") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23.0-alpha3") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1") (d #t) (k 0)))) (h "1kqv3d08jk0dvm8zzx8zy2iklnqjmnddfac47wcpgma0xixip7zy")))

