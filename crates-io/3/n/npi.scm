(define-module (crates-io #{3}# n npi) #:use-module (crates-io))

(define-public crate-npi-0.0.0 (c (n "npi") (v "0.0.0") (h "1wfxzix0f89f88ja58fj5fnd5i4g6n7s283xflq3zlf6fpkxzc8w")))

(define-public crate-npi-0.1.0 (c (n "npi") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fsn") (r "^0.1.0") (d #t) (k 0)))) (h "0m00b9ll5ny6ca35f4sw6kdsvwm63lr18phvla40i96fdqaqdw0m")))

(define-public crate-npi-0.1.1 (c (n "npi") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fsn") (r "^0.1.0") (d #t) (k 0)))) (h "09rxlfzj53bd952l05jih4sx9354a671lil2lyc607gy8maxj23g")))

(define-public crate-npi-0.2.0 (c (n "npi") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "fsn") (r "^0.1.4") (d #t) (k 0)))) (h "183cpp3j8qazhjhvgdccrj7l1q9mr74sak8w54x1zqzqailvd2mm")))

