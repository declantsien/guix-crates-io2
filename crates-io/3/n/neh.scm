(define-module (crates-io #{3}# n neh) #:use-module (crates-io))

(define-public crate-neh-0.0.6 (c (n "neh") (v "0.0.6") (d (list (d (n "gtk") (r "^0.15.3") (d #t) (t "cfg(linux)") (k 0) (p "gtk")) (d (n "nxui") (r "^0.0.6") (d #t) (k 0)) (d (n "windows") (r "^0.32.0") (f (quote ("alloc" "Data_Xml_Dom" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)) (d (n "windows-sys") (r "^0.32.0") (f (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_UI_WindowsAndMessaging"))) (d #t) (k 0)))) (h "0dwjpkw7ybss8azwpscnmvbn84l6pg7lxg4ayyzivqy7203l1b4f") (r "1.58")))

