(define-module (crates-io #{3}# n ntp) #:use-module (crates-io))

(define-public crate-ntp-0.1.0 (c (n "ntp") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1g0gxnxms8538q668x454kd816cxiz51jsma19fx4z2bkyxv4gv9")))

(define-public crate-ntp-0.1.1 (c (n "ntp") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0h69ghv80nwdr6jkvh0wkcfh62x55bxmaw1ra8ckx5a24an3hbwk")))

(define-public crate-ntp-0.1.2 (c (n "ntp") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "108bvrgq1d6crqj7grj8dh8vm2rjy0aml6c38j01i7z6h11al4nr")))

(define-public crate-ntp-0.2.0 (c (n "ntp") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "125hrvnwli5rkqs2rasm74fr38x9i8lkzrax5xn2qvm7gnp30gyj")))

(define-public crate-ntp-0.3.0 (c (n "ntp") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "1cgc7kgrf3hqzqb362py8a9nccbw3y5rxv6csxamzcw1psrnrbmq") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ntp-0.3.1 (c (n "ntp") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "186pyvnbx9sjx1c72xibxkndy8lirlk2mvx8c0xm9iij71s4jc8l") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ntp-0.4.0 (c (n "ntp") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0r9rzqf8wsss1l4yr8rdr6gda1ss985glnsclrmfa6ig7flvhvas") (f (quote (("backtrace" "error-chain/backtrace"))))))

(define-public crate-ntp-0.5.0 (c (n "ntp") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "conv") (r "^0.3.2") (d #t) (k 0)) (d (n "custom_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.0") (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0za956rfz26dph3bf41bxxxwk2v64gya362wr0x0rmic9bgd5c06") (f (quote (("backtrace" "error-chain/backtrace"))))))

