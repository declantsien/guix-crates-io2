(define-module (crates-io #{3}# n npp) #:use-module (crates-io))

(define-public crate-npp-0.1.0 (c (n "npp") (v "0.1.0") (d (list (d (n "cuda-rs") (r "^0.1.7") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "npp-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.53") (d #t) (k 0)))) (h "0f86rcwxrlbd7ak45lr1v0frbsq102jwm4dpljzm68ffqkw0a08c")))

