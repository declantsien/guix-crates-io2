(define-module (crates-io #{3}# n neu) #:use-module (crates-io))

(define-public crate-neu-0.0.0 (c (n "neu") (v "0.0.0") (d (list (d (n "async-trait") (r "^0.1.48") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "nom") (r "^6.1.2") (d #t) (k 0)) (d (n "smol") (r "^1.2.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "xi-rope") (r "^0.3.0") (d #t) (k 0)) (d (n "xtra") (r "^0.5.1") (f (quote ("with-smol-1"))) (d #t) (k 0)))) (h "1jplm66pb01qbnasbxk9lkbrvkd56wh91ffkrm2kzc44bdk977p6")))

