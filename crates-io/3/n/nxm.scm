(define-module (crates-io #{3}# n nxm) #:use-module (crates-io))

(define-public crate-nxm-0.2.1 (c (n "nxm") (v "0.2.1") (d (list (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.52") (d #t) (k 0)) (d (n "pem") (r "^0.7.0") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1iypkimnr6fahcp0n0ya3sjyd255fk37m07d9wzmw1qa3g5m50cp")))

