(define-module (crates-io #{3}# n nmf) #:use-module (crates-io))

(define-public crate-nmf-0.1.0 (c (n "nmf") (v "0.1.0") (d (list (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "03rpmq21g0vvz71vsj59fg2vwad0lj2iw0cq41pl3jsxgld6h0lc")))

(define-public crate-nmf-0.1.1 (c (n "nmf") (v "0.1.1") (d (list (d (n "rulinalg") (r "^0.4.2") (d #t) (k 0)))) (h "1z3hp9ljis2vr0knp2dj44ikn1fgczffy3k7rmfh5spk348c9gsq")))

