(define-module (crates-io #{3}# n nkl) #:use-module (crates-io))

(define-public crate-nkl-0.0.0 (c (n "nkl") (v "0.0.0") (h "1imp8z2wc8bp0s00nmjfbxs9w1d9rj5y3bp9r5z33liw7hqbghrw")))

(define-public crate-nkl-0.0.1 (c (n "nkl") (v "0.0.1") (h "1z2nw6fvh69vpbrqb8qnpag9kv010h1g22fdlbfnb988mggqfc7y")))

(define-public crate-nkl-0.0.2 (c (n "nkl") (v "0.0.2") (h "0y1iyf5s4xi9mj488zhjhdimv13jcj925bb2czr2hn8klfxrr5k0")))

(define-public crate-nkl-0.0.3 (c (n "nkl") (v "0.0.3") (h "0sxdx26399l5k8s20ixld2cl1x0pz09scfx6r71kv3wzsm5jcxp4")))

(define-public crate-nkl-0.0.4 (c (n "nkl") (v "0.0.4") (h "12q595h33hx9whchjy3hs9kgiz2dsa5j0x7rvcf6am2q5q7n2qwb")))

(define-public crate-nkl-0.0.5 (c (n "nkl") (v "0.0.5") (h "0jwg7gamqm2890qdq0jq2ph9zwsqa2lhzsa30mq95j4sy76ijjyp")))

