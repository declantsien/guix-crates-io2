(define-module (crates-io #{3}# n nfq) #:use-module (crates-io))

(define-public crate-nfq-0.1.0 (c (n "nfq") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mnl-sys") (r "^0.1") (d #t) (k 0)))) (h "14nq9d395g9cqvin0247sq92m6yb1vdhpd4li9zsq6z75cb3c67a")))

(define-public crate-nfq-0.1.1 (c (n "nfq") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mnl-sys") (r "^0.1") (d #t) (k 0)))) (h "1pih5hi4y32hvwqgkb6mzqg281xqp9zf6zp9wmh87sax3ifzdacp")))

(define-public crate-nfq-0.1.2 (c (n "nfq") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mnl-sys") (r "^0.1") (d #t) (k 0)))) (h "0gz1w7535aryvihjkl57bpvf2qfpl6rm5lm40mvjffgaj8mm2dv0")))

(define-public crate-nfq-0.2.0 (c (n "nfq") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mnl-sys") (r "^0.1") (d #t) (k 0)))) (h "0p6hl16gd633v2ln6w0ldf975rpbrvwryyp4yx6qx2ga5pivzng2")))

(define-public crate-nfq-0.2.1 (c (n "nfq") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vphccvk547qws5vhhnzg9hvfxcw14ahvj69j3nrhjc9r2qw54x9")))

(define-public crate-nfq-0.2.2 (c (n "nfq") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gpav87p1qqsfkzmxfvnzqpnjfl7xfvbhjh5gb8skjp03mb8rhik")))

(define-public crate-nfq-0.2.3 (c (n "nfq") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xai2dzc3c6cscxqhqpzdxrbwrzwzbyv1x9lnsssv2glyb76zns8") (f (quote (("ct"))))))

(define-public crate-nfq-0.2.4 (c (n "nfq") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jvsmhww7g5qmdjbzpbwn17p3j4mgyqyr8qdv32arwp9749gvwyw") (f (quote (("ct"))))))

(define-public crate-nfq-0.2.5 (c (n "nfq") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17nvsrbg2gzb2r3jpjqxw9hb83v490p6l2j0z6fpsl2ji74g9j5r") (f (quote (("ct"))))))

