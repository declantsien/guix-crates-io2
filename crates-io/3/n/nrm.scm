(define-module (crates-io #{3}# n nrm) #:use-module (crates-io))

(define-public crate-nrm-0.1.0 (c (n "nrm") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "clap") (r "^3.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "os_pipe") (r "^1.0") (d #t) (k 0)) (d (n "path-clean") (r "^0.1") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (f (quote ("colors" "stderr"))) (k 0)) (d (n "spawn-ptrace") (r "^0.1") (d #t) (k 0)) (d (n "strum") (r "^0.24") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ikb83gq15wjs5xkxxh01r9w38y9k7s7fph094axphfajry1ka27") (f (quote (("bigendian"))))))

