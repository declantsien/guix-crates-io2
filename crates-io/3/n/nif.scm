(define-module (crates-io #{3}# n nif) #:use-module (crates-io))

(define-public crate-nif-0.1.0 (c (n "nif") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1jvd51cwvm78q2q9bchagk9fslh216hvv9zfz2a7yak0sq3y9vh7")))

(define-public crate-nif-0.1.1 (c (n "nif") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "binread") (r "^1.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0f7912q60k3qyvn88ci2fqrxlw5y38c65x8cbdcx5ljzs1qfg04w")))

(define-public crate-nif-0.2.0 (c (n "nif") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "binread") (r "^2.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1lfgx2f92akzlndpdcn6kgcd3brgxf37w9dl4kvis0li9wnfma9r")))

(define-public crate-nif-0.3.0 (c (n "nif") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "binread") (r "^2.1.1") (d #t) (k 0)) (d (n "glam") (r "^0.17.3") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1xfgnz67k17pn9b2j7aglhbkwadhq79mvjc4q5040rsvhc6cy3gq") (f (quote (("obj" "glam") ("default" "obj"))))))

(define-public crate-nif-0.4.0 (c (n "nif") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "binread") (r "^2.1.1") (d #t) (k 0)) (d (n "glam") (r "^0.17.3") (o #t) (d #t) (k 0)) (d (n "gltf") (r "^0.16.0") (o #t) (d #t) (k 0)) (d (n "gltf-json") (r "^0.16.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1488i1i78m2xf9qplyg3f4m8py3jsc89dm9bwaqwshs5kdq5hwx3") (f (quote (("obj_export" "glam") ("gltf_export" "glam" "gltf" "gltf-json") ("default" "obj_export" "gltf_export"))))))

(define-public crate-nif-0.4.1 (c (n "nif") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "binrw") (r "^0.11.2") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "gltf") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "gltf-json") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1c2wwp8l0dym776dnravw2rc27x78wjgf67a9fcx9dd0safjdqzp") (f (quote (("obj_export") ("gltf_export" "gltf" "gltf-json") ("default" "obj_export" "gltf_export"))))))

(define-public crate-nif-0.4.2 (c (n "nif") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "binrw") (r "^0.11.2") (d #t) (k 0)) (d (n "glam") (r "^0.24.1") (d #t) (k 0)) (d (n "gltf") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "gltf-json") (r "^1.2.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "16dlh9l60fl0yjy5av5cpxmny0clv5j9mvfji7k9ipxfflyv4g9g") (f (quote (("obj_export") ("gltf_export" "gltf" "gltf-json") ("default" "obj_export" "gltf_export"))))))

