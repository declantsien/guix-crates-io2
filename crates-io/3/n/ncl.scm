(define-module (crates-io #{3}# n ncl) #:use-module (crates-io))

(define-public crate-ncl-0.1.0 (c (n "ncl") (v "0.1.0") (d (list (d (n "nom") (r "*") (d #t) (k 0)))) (h "0kr0yph4mxvz4hmdbmsrjddhnnrniml7grx1mvl6qxcy13spgykv") (f (quote (("unstable"))))))

(define-public crate-ncl-0.1.1 (c (n "ncl") (v "0.1.1") (d (list (d (n "nom") (r "~0.3.0") (d #t) (k 0)))) (h "1nsl1nyi43p8ilwwfkmi692qsz5bkc2f5s9yniwz2c6shihi8a6a") (f (quote (("unstable"))))))

(define-public crate-ncl-0.1.2 (c (n "ncl") (v "0.1.2") (d (list (d (n "nom") (r "~0.3.0") (d #t) (k 0)))) (h "0cx2mva1aw20bbwnqx4ciskx0p6rgwmfqv1z70xdnl6nbk7wirga") (f (quote (("unstable"))))))

(define-public crate-ncl-0.1.3 (c (n "ncl") (v "0.1.3") (d (list (d (n "nom") (r "~0.3.0") (d #t) (k 0)))) (h "1i0x8cg1agazykz1sfjnf27858q9b6rq77kcvwdkmbmcz6kidlsv") (f (quote (("unstable"))))))

