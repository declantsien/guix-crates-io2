(define-module (crates-io #{3}# n ndm) #:use-module (crates-io))

(define-public crate-ndm-0.9.0 (c (n "ndm") (v "0.9.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "12nj0gy4k1d03lnklkfnvk9j079z35qf6dfm479m1fm51vcvnnh3")))

(define-public crate-ndm-0.9.1 (c (n "ndm") (v "0.9.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1psw594xl8wq9am580x189nbb42zn5d5v94gfmm835j51lnlcbjq")))

(define-public crate-ndm-0.9.2 (c (n "ndm") (v "0.9.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1afi7vl485nbl9im4gl8dyr261xq8q2zl9s6551ry8hw11wj9dmx")))

(define-public crate-ndm-0.9.3 (c (n "ndm") (v "0.9.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "1673w8r27j5nyhcssm9ajpdgylqd5sqqd8pb4v13xsnrid9wa2dy")))

(define-public crate-ndm-0.9.4 (c (n "ndm") (v "0.9.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0cx3n3bngiwfpybdakfbzbsqrbnyzz5jrbnvdyhakmz80q9w4ic8")))

(define-public crate-ndm-0.9.5 (c (n "ndm") (v "0.9.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)))) (h "0w9bf4glkisflsx8paxqddcrqap02lqlfh993rwfp2dylxbzc7wf")))

(define-public crate-ndm-0.9.6 (c (n "ndm") (v "0.9.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wdjrmzqra51xr3gv4kcf5sc7vx7jb3lb38j5cyj4c2addnds8ra")))

(define-public crate-ndm-0.9.7 (c (n "ndm") (v "0.9.7") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wvv4d57qq46s55cd67fkhajqq9rl539z122qhz43a1pzr75amhh")))

(define-public crate-ndm-0.9.8 (c (n "ndm") (v "0.9.8") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19fjqjc7di52xshfxlwl793p3bh7xkvvry32jad5d083yqpny4wf")))

(define-public crate-ndm-0.9.9 (c (n "ndm") (v "0.9.9") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wkvkyww5mxba82966kk5klarrdxqxdfzmf1085kalz3k03q33ls")))

(define-public crate-ndm-0.9.10 (c (n "ndm") (v "0.9.10") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sgllkd4hrl5wakq2jrgdh0q4q0lm9sks5viz3419yikv1lg5zqb")))

