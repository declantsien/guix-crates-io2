(define-module (crates-io #{3}# n nof) #:use-module (crates-io))

(define-public crate-nof-0.1.0 (c (n "nof") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "1bvba0qmw94rbwq6mgimhsm8nnrgblz23h7hpd7rj0j300k7qjl5")))

(define-public crate-nof-0.1.1 (c (n "nof") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "1v98zyg3dmc9p3g10c3qpks276i0yxmigmnk3cfmn0vlcf6zgnsi")))

(define-public crate-nof-0.2.0 (c (n "nof") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0khpawpa483j4j42msmmpahqlv2rcx2372x1ii3hcayxjf8b24z3")))

(define-public crate-nof-0.3.0 (c (n "nof") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "1scda2ak3qpvvxa8ny01kgzix38lwhzbm9psw68bxsjm903zkxa0")))

(define-public crate-nof-0.4.0 (c (n "nof") (v "0.4.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0i2w2vvlgacl8vb0cpxkcw958rp2sn9zzcqclzj0s5gzyyjrq1jd")))

(define-public crate-nof-0.4.1 (c (n "nof") (v "0.4.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "00l950fhr53iayc1avzprpjlqd6srfvx7kk09hyxszdafgl6jbj9")))

(define-public crate-nof-0.5.0 (c (n "nof") (v "0.5.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "0vvyq7s9rzh1yp01w44z50q7iy5qyixh31xbd1jp78xaz0ycpdqh")))

(define-public crate-nof-0.5.1 (c (n "nof") (v "0.5.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)))) (h "1grg7ag4a3wxf4nlqp2szm1x8g95pxaccg3s5i2rl2wmfahhgfv7")))

(define-public crate-nof-0.6.1 (c (n "nof") (v "0.6.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0z61nqfp3qm88rywgqwnd857cpfci8fczq02da9k81smhmps2641")))

(define-public crate-nof-0.7.0 (c (n "nof") (v "0.7.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "08sn2cz6l24agx52pk862lxiy7ywb4dvkf4w15i1w09f331rqkiy")))

(define-public crate-nof-0.8.0 (c (n "nof") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.1.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1qcnk9f45c4dkd86wr2lv9cxnrknv5jv32ckj8yzaby9rb4j64jg")))

(define-public crate-nof-0.9.0 (c (n "nof") (v "0.9.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.4.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "skim") (r "^0.10.4") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1p0ma7xk631gz6hrdi69cf1l2x9vp0fsgls1lsn0pnhmkzkl4ad8")))

