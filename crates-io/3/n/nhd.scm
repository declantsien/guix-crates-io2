(define-module (crates-io #{3}# n nhd) #:use-module (crates-io))

(define-public crate-nhd-0.0.2 (c (n "nhd") (v "0.0.2") (d (list (d (n "clap") (r "^4.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "requestty") (r "^0.4.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bj0vabcqa81bj94nhkb2vsrmqkdlr1lqz8n52w9ldkrgah707qc")))

