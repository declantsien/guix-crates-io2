(define-module (crates-io #{3}# n nsq) #:use-module (crates-io))

(define-public crate-nsq-0.0.1 (c (n "nsq") (v "0.0.1") (h "0wyp3s78glk9n4rzp6vnvww5i0rp79k1dfmdgdiz4v1dh2l0wz47")))

(define-public crate-nsq-0.0.2 (c (n "nsq") (v "0.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "derive_builder") (r "^0.4") (f (quote ("private_fields"))) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "109y3mhxxcm9h9z094qhm2zjjc9d3lb36pk7qdzw81i7hb159z0v")))

(define-public crate-nsq-0.0.3 (c (n "nsq") (v "0.0.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "derive_builder") (r "^0.4") (f (quote ("private_fields"))) (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "19dzgdqnhd298km19cg4fqx8qmcql7cqqc8vlj08k7ad6qwjgir3")))

