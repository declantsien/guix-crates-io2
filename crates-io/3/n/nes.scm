(define-module (crates-io #{3}# n nes) #:use-module (crates-io))

(define-public crate-nes-0.1.0 (c (n "nes") (v "0.1.0") (h "1h83rmhwpfgjp9cfmlinfq1784piwky7h1668im5ia3sxqp0d5mx")))

(define-public crate-nes-0.1.1 (c (n "nes") (v "0.1.1") (h "0n8y7b5vm9mhf39ka1z1im2gq9jx1r4qc7cw88miljd5blzjh12g")))

