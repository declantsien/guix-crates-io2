(define-module (crates-io #{3}# n nbt) #:use-module (crates-io))

(define-public crate-nbt-0.1.0 (c (n "nbt") (v "0.1.0") (d (list (d (n "flate2") (r "*") (d #t) (k 0)))) (h "07bxk6zcfxy5zpdzn08vpwk7knlhaj9f83vq6m4329s5ijhyr570")))

(define-public crate-nbt-0.1.1 (c (n "nbt") (v "0.1.1") (d (list (d (n "flate2") (r "*") (d #t) (k 0)))) (h "0b1fzmp46cgqawqn864iphj9lfwwjhliib1xz1z5w07as9sm2nqp")))

(define-public crate-nbt-0.1.2 (c (n "nbt") (v "0.1.2") (d (list (d (n "flate2") (r "*") (d #t) (k 0)))) (h "01s44iqrcslag8jg06w25gl4bg56wz22b0h5sb8pp663jxwdmzg8")))

