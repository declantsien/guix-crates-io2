(define-module (crates-io #{3}# n nls) #:use-module (crates-io))

(define-public crate-nls-0.1.0 (c (n "nls") (v "0.1.0") (h "0bs0yvzhr6phnrw1gjzm6zfng82a84ma4d4v831dyi700s57kz2k") (y #t)))

(define-public crate-nls-0.1.1 (c (n "nls") (v "0.1.1") (h "1hq9pa4164p95pc9andfz0nz6l2vrxnsch03s428p0n7jsw2y3n7") (y #t)))

(define-public crate-nls-0.1.2 (c (n "nls") (v "0.1.2") (h "0yg978ygrdkg8rj80caa6acnr5gw35wncf856jpyvpj2p3ck5q1y") (y #t)))

(define-public crate-nls-0.1.3 (c (n "nls") (v "0.1.3") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)))) (h "0dd42k9k6ipqdbn9cgavanmyhvac0jdwagmwcyp7dnkpjlfda8w3") (y #t)))

(define-public crate-nls-0.1.4 (c (n "nls") (v "0.1.4") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)))) (h "15zn8fqx01gh6gvg4wi7m0hryb4v572gn724fhczfhj582m1i5fd") (y #t)))

