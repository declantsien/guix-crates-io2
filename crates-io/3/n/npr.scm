(define-module (crates-io #{3}# n npr) #:use-module (crates-io))

(define-public crate-npr-0.1.0 (c (n "npr") (v "0.1.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0linrhw6k9rbqgcwlr5mmayrsqb3hymfnfaswcr5kgw51h2xszh5")))

(define-public crate-npr-0.2.0 (c (n "npr") (v "0.2.0") (d (list (d (n "docopt") (r "^1.0") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1xk7j2a5f1ylc4asa3aaxa2jpqjlvasiaf5azzahbcbp6mlmkbss")))

