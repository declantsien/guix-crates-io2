(define-module (crates-io #{3}# n noc) #:use-module (crates-io))

(define-public crate-noc-0.1.0 (c (n "noc") (v "0.1.0") (d (list (d (n "nereon") (r "^0.2") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.1") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6") (d #t) (k 0)))) (h "0x5r1xsxyfgjrimb8g071lxm4j1rc3kmskkdz740w1kr4d6v75cx")))

(define-public crate-noc-0.1.1 (c (n "noc") (v "0.1.1") (d (list (d (n "nereon") (r "^0.3") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6") (d #t) (k 0)))) (h "1i6lagnp33439r69d1rg0l44r45gsdkdafrlj42mhwssjs84sz7v")))

(define-public crate-noc-0.1.3 (c (n "noc") (v "0.1.3") (d (list (d (n "nereon") (r "^0.3") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.2") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6") (d #t) (k 0)))) (h "1drfslkccvwa2w5pygzm36rlhd0wd8a5gqfnh3sk44v02m68pc5i")))

(define-public crate-noc-0.4.0 (c (n "noc") (v "0.4.0") (d (list (d (n "nereon") (r "^0.4") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6") (d #t) (k 0)))) (h "1qpcpyw0pdlga819yx3sj262v7fvj1pbyxxpy89wcymlk4x9xka2")))

(define-public crate-noc-0.4.2 (c (n "noc") (v "0.4.2") (d (list (d (n "nereon") (r "^0.4") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6") (d #t) (k 0)))) (h "1q02dnshykvk6pyipg52459p9v4iyw5qzl5di6lbp2vm5pnrp229")))

(define-public crate-noc-0.4.3 (c (n "noc") (v "0.4.3") (d (list (d (n "nereon") (r "^0.4") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.4") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6") (d #t) (k 0)))) (h "0d54hrc278y7wsc0cgvz1ki8w7vpdbrx774cdcg694kixbcwyck0")))

(define-public crate-noc-0.5.0 (c (n "noc") (v "0.5.0") (d (list (d (n "nereon") (r "^0.5") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.5") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6") (d #t) (k 0)))) (h "05xc63mmncpp1qxmcpfi1hra5pzjb56bxlrll8iajhzgn1vhc5y7")))

(define-public crate-noc-0.6.0 (c (n "noc") (v "0.6.0") (d (list (d (n "nereon") (r "^0.6") (d #t) (k 0)) (d (n "nereon_derive") (r "^0.6") (d #t) (k 0)) (d (n "tiny_http") (r "^0.6") (d #t) (k 0)))) (h "026pzclvsbd5rviby5jdsirrwzzvy477660ll611f4xdg96zv0bp")))

