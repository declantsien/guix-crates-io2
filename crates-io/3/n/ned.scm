(define-module (crates-io #{3}# n ned) #:use-module (crates-io))

(define-public crate-ned-0.1.0 (c (n "ned") (v "0.1.0") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "0abc8nn1x0aq7gw54vih3lls33lnpj9c5wzgyd55gv1bayplnjf3")))

(define-public crate-ned-0.1.1 (c (n "ned") (v "0.1.1") (d (list (d (n "colored") (r "^1.6.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^5.0.0") (d #t) (k 0)))) (h "1wnh3s7klxxa76ksqjkaa3cmnayqavpix75q02k783nrb674hh4s")))

