(define-module (crates-io #{3}# n nbf) #:use-module (crates-io))

(define-public crate-nbf-0.1.0 (c (n "nbf") (v "0.1.0") (h "1kf7gl0rf8ikr9xjww8ynldlyrl90kixsjsqj9ban5z1af7g94zv")))

(define-public crate-nbf-0.2.0 (c (n "nbf") (v "0.2.0") (h "0xbqi3f14aldrxxk9pgq4rzqv280jkqzp9r7b7vssq3zsh9lxp64")))

(define-public crate-nbf-0.2.1 (c (n "nbf") (v "0.2.1") (h "0cp59j5h4ffcjgjjl1kpb909w9j547lvmbr4v53aycfhpdrmznkj")))

(define-public crate-nbf-0.2.2 (c (n "nbf") (v "0.2.2") (h "0a2l8qpzkz0ss69cdkgsca45fkmgmhm3yxjmy839ml8yl2aad14h")))

(define-public crate-nbf-0.2.3 (c (n "nbf") (v "0.2.3") (h "1d3xnlwlnb4z177qwzis8fgbxl780ziadw2yz6pjqzdhxfxhsy9i")))

