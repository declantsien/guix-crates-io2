(define-module (crates-io #{3}# n nge) #:use-module (crates-io))

(define-public crate-nge-0.0.0 (c (n "nge") (v "0.0.0") (d (list (d (n "aoko") (r "^0.3.0-alpha.28") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1fbg6qqr9bk3n5nkcvi6l8g72fwi85x0v9q3q21z1b4hf7cyyrq1")))

(define-public crate-nge-0.0.1 (c (n "nge") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wn1bic8y9zlgb0zmxmym1hhlx3w54y6df1fsav3yzyn0bs28kww")))

