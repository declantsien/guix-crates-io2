(define-module (crates-io #{3}# h htp) #:use-module (crates-io))

(define-public crate-htp-0.1.0 (c (n "htp") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.12") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "05wd33r6mkk8lxa0c4y4035nz445dm4vca21w73kvlhl94wlzbdw")))

(define-public crate-htp-0.2.0 (c (n "htp") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.12") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1pvl5bm9d5wcr5ajsq4ilxrmi8n84xrcki75gdy2ka3anmza3zqa")))

(define-public crate-htp-0.2.1 (c (n "htp") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.12") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0ayrgdy8h3cvgnadnhhg9nh3c24hbkpplj81r12viarw8rv0dblq")))

(define-public crate-htp-0.3.0 (c (n "htp") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.12") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1z90y61nbydkazmkzwkrszlv5z515v78d7hahmi6l2fhqd15ca3z")))

(define-public crate-htp-0.4.0 (c (n "htp") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.12") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "033gh52x6hzyz4kki94lzy0llhj3prsgcbpl49fsbj0c2dad4v4w")))

(define-public crate-htp-0.4.1 (c (n "htp") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.12") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1cjfrqb34h10kpi0xfnixyzvr646spvda1dd8nnbgz7hs4741x11")))

(define-public crate-htp-0.4.2 (c (n "htp") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.12") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "10vj8a8qcwgqmkykiv69842dz212pw9jqsvgwcggbwqaxslh2fhv")))

