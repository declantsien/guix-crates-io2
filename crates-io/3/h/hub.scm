(define-module (crates-io #{3}# h hub) #:use-module (crates-io))

(define-public crate-hub-0.0.1 (c (n "hub") (v "0.0.1") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("unstable"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)))) (h "1vbsqgxyqm09myh1smsp2x12ch91zsr62nfaypf4lm065g1jxiww")))

(define-public crate-hub-0.1.0 (c (n "hub") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "async_ach-pubsub") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.13") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("net" "io-util" "rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio-serial") (r "^5.4.1") (d #t) (k 0)))) (h "0xd9i1k7x8x57rwk2xn15h7qdwdy8i65kl5yjyhkbskj3jhgp16x")))

