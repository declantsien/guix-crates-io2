(define-module (crates-io #{3}# h h2x) #:use-module (crates-io))

(define-public crate-h2x-0.1.0 (c (n "h2x") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "h2") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "tokio-tls-listener") (r "^0.1") (d #t) (k 0)))) (h "0sgfnpxw7pw6cm399rw95kcrrb5kkzz6r324nyydfp4z8kbkmx04")))

(define-public crate-h2x-0.2.0 (c (n "h2x") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "h2") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "signal"))) (d #t) (k 2)) (d (n "tokio-tls-listener") (r "^0.1") (d #t) (k 0)))) (h "097hm1gl2csva7n2c8i5ih0cayzppz2wcm3cqfjsw9gwwq710pks")))

(define-public crate-h2x-0.3.0 (c (n "h2x") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "h2") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "signal"))) (d #t) (k 2)) (d (n "tokio-tls-listener") (r "^0.1") (d #t) (k 0)))) (h "1yaj7bfdf49j6kb0i2rzwz7m9jwcg8vjmn2fs087yxf2xn42lxmy")))

(define-public crate-h2x-0.3.1 (c (n "h2x") (v "0.3.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "h2") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "signal"))) (d #t) (k 2)) (d (n "tokio-tls-listener") (r "^0.1") (d #t) (k 0)))) (h "0ckn9srznz16ffqj4i3p0rxhfanf7jz6kg58xbn8hc52yc11j1wh")))

(define-public crate-h2x-0.4.0 (c (n "h2x") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "h2") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros" "signal"))) (d #t) (k 2)) (d (n "tokio-tls-listener") (r "^0.1") (d #t) (k 0)))) (h "0vi3vay0ksivi8zvx3d10zgr9f64ph54l4lw1cgcxqz5p32h00vz")))

