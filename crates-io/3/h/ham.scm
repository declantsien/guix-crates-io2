(define-module (crates-io #{3}# h ham) #:use-module (crates-io))

(define-public crate-ham-0.1.0 (c (n "ham") (v "0.1.0") (d (list (d (n "gpio-cdev") (r "^0.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "reed-solomon") (r "^0.2.1") (d #t) (k 0)) (d (n "spidev") (r "^0.4.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (o #t) (d #t) (k 0)))) (h "1a0iks7w9hjfribw09ppkaz0n2wb9vx4adlmjzjfh0qyf9gzbgqq") (f (quote (("hp-sleep" "spin_sleep") ("default" "hp-sleep"))))))

(define-public crate-ham-0.2.0 (c (n "ham") (v "0.2.0") (d (list (d (n "gpio-cdev") (r "^0.2.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "reed-solomon") (r "^0.2.1") (d #t) (k 0)) (d (n "spidev") (r "^0.4.0") (d #t) (k 0)) (d (n "spin_sleep") (r "^0.3.7") (o #t) (d #t) (k 0)))) (h "1awg5h26zpv56cls98zjm7s5h3p2hcsvy8mr4ynx9bmm6gw56jcn") (f (quote (("hp-sleep" "spin_sleep") ("default" "hp-sleep"))))))

