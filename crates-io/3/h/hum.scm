(define-module (crates-io #{3}# h hum) #:use-module (crates-io))

(define-public crate-hum-0.1.0 (c (n "hum") (v "0.1.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "1bb9nbk9ld3rckwigvbilx10qgsggh3dkm8z1xvck8kdk9ykgdcj")))

(define-public crate-hum-0.1.1 (c (n "hum") (v "0.1.1") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "1whg035h81f5pjjij4d6ng7syjdy39bqdnnyqsvmjd9y520gxnyz")))

(define-public crate-hum-0.1.2 (c (n "hum") (v "0.1.2") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "1szmz3gsfp6gablp1dazajxfd1s6i7m1y079jbpi5vjs59jlr9k5")))

(define-public crate-hum-0.2.0 (c (n "hum") (v "0.2.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "18q171qw4xw4ckavq0pzp3mgrxfd83rxb95qbbwsvzh42zrwsbb3")))

(define-public crate-hum-0.2.1 (c (n "hum") (v "0.2.1") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)))) (h "005yab5skacxgabl9f5jl5rb6f0vxjn72g8lmcxzsfg4b2p23p22")))

(define-public crate-hum-0.3.0 (c (n "hum") (v "0.3.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "portaudio") (r "^0.7.0") (d #t) (k 0)))) (h "00cxdissrbc8nhk4ws9qsfbgq1y9jwh9pwipp7m1dkpah3agj5rr")))

(define-public crate-hum-0.4.0 (c (n "hum") (v "0.4.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "portaudio") (r "^0.7.0") (d #t) (k 0)))) (h "1wdfznca6mkk68cj8p205snzzgbxg3aijn7c7asaykyprry5zg94")))

(define-public crate-hum-0.5.0 (c (n "hum") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)) (d (n "portaudio") (r "^0.7.0") (d #t) (k 0)) (d (n "vcpkg") (r "^0.2.11") (d #t) (k 1)))) (h "0x1za7ddvzfbma0wqxglwycd7xxnqlx9g6kssbvg956n7l9yc0ah")))

(define-public crate-hum-0.6.0 (c (n "hum") (v "0.6.0") (d (list (d (n "clap") (r "^4.4.3") (d #t) (k 0)) (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "peg") (r "^0.6.3") (d #t) (k 0)))) (h "1rgdff9sd1lkxv2b12kx281v4kh304c16qzyq9vi76wg9zpj9sza")))

