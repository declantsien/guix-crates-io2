(define-module (crates-io #{3}# h hew) #:use-module (crates-io))

(define-public crate-hew-0.1.0 (c (n "hew") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "0i3djjp4b7j73bds1lys3npg84hfvldclcsmqhc70x1h45b3vcvk")))

(define-public crate-hew-0.1.1 (c (n "hew") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "1izpksmga5mz9lvlfcmqz86l5bc4f7j960ymp2mysw3nj7x192hf")))

(define-public crate-hew-0.2.0 (c (n "hew") (v "0.2.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "1xpl7kngwj057ljqww8dwvhkx07hv4kisp3k934x6plv2dlkxpj9")))

(define-public crate-hew-0.2.1 (c (n "hew") (v "0.2.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "04xbhwlqbm5719qp7g3rbbwnx91jcwxgal5drgann8grfwlx2gw6")))

(define-public crate-hew-0.2.2 (c (n "hew") (v "0.2.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "03pva80y8r7gji4s51vi8dqbfgn3gk7sxhzd79v1df0jydb4c1vh")))

(define-public crate-hew-0.2.3 (c (n "hew") (v "0.2.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "1avznyiy5vxy5fk8ljv1sfvbix63i858ydp4ijd2lfqsqwy51lnm")))

(define-public crate-hew-0.2.4 (c (n "hew") (v "0.2.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)))) (h "0f26ryh633jc2qvrlns73cs6xwnzaapj5i10kjbpkcvvl8dh846g")))

