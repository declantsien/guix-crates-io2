(define-module (crates-io #{3}# h hlc) #:use-module (crates-io))

(define-public crate-hlc-0.1.0 (c (n "hlc") (v "0.1.0") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1bcha8pj1q4vwyfavvazm4brnlgx5bfp1dy07ysn2l9swsa487hk")))

(define-public crate-hlc-0.1.1 (c (n "hlc") (v "0.1.1") (d (list (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0a1xylq4rriwq5ysknlg0asz6x6jxrly61hdfr4pfqc74wy6spg6")))

