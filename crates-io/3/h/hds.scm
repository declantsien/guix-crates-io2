(define-module (crates-io #{3}# h hds) #:use-module (crates-io))

(define-public crate-hds-0.1.0 (c (n "hds") (v "0.1.0") (h "0zzfymgx0lmj9zgdj3j43wkmi8frqq0x3hi4fk3kbhpgjd7xdnya")))

(define-public crate-hds-0.2.0 (c (n "hds") (v "0.2.0") (h "0wa1sv3vc7rd8w37m49sjz2ivzfyqpsrih3crr0y7yanyva4d45j") (f (quote (("std") ("default"))))))

(define-public crate-hds-0.2.1 (c (n "hds") (v "0.2.1") (h "1xhayvqnxf0xvxg7vz0mlnls0d11326a0l4xbf97j4am96739ab2") (f (quote (("std") ("default"))))))

(define-public crate-hds-0.3.0 (c (n "hds") (v "0.3.0") (h "1j32mf39130pw5cpsymb5pl0rp1sfqkrskq4x5s1ih34nhdqr386") (f (quote (("std" "alloc") ("default") ("alloc"))))))

