(define-module (crates-io #{3}# h hos) #:use-module (crates-io))

(define-public crate-hos-0.1.0 (c (n "hos") (v "0.1.0") (d (list (d (n "bootloader") (r "^0.11.3") (d #t) (k 1)) (d (n "hos_kernel") (r "^0.1.0") (d #t) (k 1)) (d (n "ovmf-prebuilt") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "1grs00hc911zymvydkgmzxywq7w5v089lbg9p4qqi4j30gvrhr4g")))

