(define-module (crates-io #{3}# h hsv) #:use-module (crates-io))

(define-public crate-hsv-0.1.0 (c (n "hsv") (v "0.1.0") (h "0xbqipjzmi114hyyy8lpibm872cxggaa8ll7w40zbcm0isqgff3j")))

(define-public crate-hsv-0.1.1 (c (n "hsv") (v "0.1.1") (h "0sd81nzzbd3ryq0as3s4gps2zbx7g3i0rpgiqm9p8blk83wn8dm0")))

