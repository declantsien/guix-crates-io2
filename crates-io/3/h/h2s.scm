(define-module (crates-io #{3}# h h2s) #:use-module (crates-io))

(define-public crate-h2s-0.1.0 (c (n "h2s") (v "0.1.0") (d (list (d (n "h2s_core") (r "=0.1.0") (d #t) (k 0)) (d (n "h2s_macro") (r "=0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "180pg1yrxq0q2dfp6mnkcxmxnxp127444zb3rjj7njihmi4id621") (f (quote (("default" "backend-scraper")))) (s 2) (e (quote (("backend-scraper" "dep:scraper")))) (r "1.65")))

(define-public crate-h2s-0.1.1 (c (n "h2s") (v "0.1.1") (d (list (d (n "h2s_core") (r "=0.1.1") (d #t) (k 0)) (d (n "h2s_macro") (r "=0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "1fas2h4838af0b49pb5l7m5xysqsw4r88z8sxqkrv8l9ipwcqh6n") (f (quote (("default" "backend-scraper")))) (s 2) (e (quote (("backend-scraper" "dep:scraper")))) (r "1.65")))

(define-public crate-h2s-0.1.2 (c (n "h2s") (v "0.1.2") (d (list (d (n "h2s_core") (r "=0.1.1") (d #t) (k 0)) (d (n "h2s_macro") (r "=0.1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "14d36vg9zzfp929rz2f85lamzmgciy3winpxl0v4pylsv5ba8k56") (f (quote (("default" "backend-scraper")))) (s 2) (e (quote (("backend-scraper" "dep:scraper")))) (r "1.65")))

(define-public crate-h2s-0.1.3 (c (n "h2s") (v "0.1.3") (d (list (d (n "h2s_core") (r "=0.1.3") (d #t) (k 0)) (d (n "h2s_macro") (r "=0.1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "05ca8r9pl3navcinrrl73lbfj1bgdi01k30a6dys28qygx28mp77") (f (quote (("default" "backend-scraper")))) (s 2) (e (quote (("backend-scraper" "dep:scraper")))) (r "1.65")))

(define-public crate-h2s-0.1.4 (c (n "h2s") (v "0.1.4") (d (list (d (n "h2s_core") (r "=0.1.4") (d #t) (k 0)) (d (n "h2s_macro") (r "=0.1.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (o #t) (d #t) (k 0)))) (h "08f4qqwm1gxq5ibqw49wf2b4pyim2fvb824fmkimlmmxyfldbccz") (f (quote (("default" "backend-scraper")))) (s 2) (e (quote (("backend-scraper" "dep:scraper")))) (r "1.65")))

(define-public crate-h2s-0.1.5 (c (n "h2s") (v "0.1.5") (d (list (d (n "h2s_core") (r "^0.1.4") (d #t) (k 0)) (d (n "h2s_macro") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "0jc5nl000cmap4lrn567n1p77n1jyv9m5vdvd7qpxk3d72jl9rs1") (f (quote (("default" "backend-scraper")))) (s 2) (e (quote (("backend-scraper" "dep:scraper")))) (r "1.65")))

(define-public crate-h2s-0.1.6 (c (n "h2s") (v "0.1.6") (d (list (d (n "h2s_core") (r "^0.1.6") (d #t) (k 0)) (d (n "h2s_macro") (r "^0.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "0ghr0aadyd433y35zdjabihb76b4zahg905n8cyvmm2ir4gf0a1i") (f (quote (("default" "backend-scraper")))) (s 2) (e (quote (("backend-scraper" "dep:scraper")))) (r "1.65")))

(define-public crate-h2s-0.17.0 (c (n "h2s") (v "0.17.0") (d (list (d (n "h2s_core") (r "^0.17.0") (d #t) (k 0)) (d (n "h2s_macro") (r "^0.17.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "0zna4s7qg950x328miys4vx1djxb9rli4adyc7vzizxf68yrikpv") (f (quote (("default" "backend-scraper")))) (s 2) (e (quote (("backend-scraper" "dep:scraper")))) (r "1.65")))

(define-public crate-h2s-0.18.0 (c (n "h2s") (v "0.18.0") (d (list (d (n "h2s_core") (r "^0.18.0") (d #t) (k 0)) (d (n "h2s_macro") (r "^0.18.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (o #t) (d #t) (k 0)))) (h "11lhkbkkqghr8kbx1h1is0wr4ybawz98j033q13n1amjrb72q68j") (f (quote (("default" "backend-scraper")))) (s 2) (e (quote (("backend-scraper" "dep:scraper")))) (r "1.65")))

