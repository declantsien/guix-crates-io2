(define-module (crates-io #{3}# h hf2) #:use-module (crates-io))

(define-public crate-hf2-0.1.0 (c (n "hf2") (v "0.1.0") (d (list (d (n "hidapi") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "scroll") (r "^0.10.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rbz95zn63bx0y6ligaqhxzpc29wkq3d0hpsn9pc9c3qgqfpi33f")))

(define-public crate-hf2-0.1.1 (c (n "hf2") (v "0.1.1") (d (list (d (n "hidapi") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "scroll") (r "^0.10.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09i54830hjw2rikrj16i9firhfzk3z57l3j7kil59dsnzgimizhy")))

(define-public crate-hf2-0.1.2 (c (n "hf2") (v "0.1.2") (d (list (d (n "hidapi") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "scroll") (r "^0.10.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1g9n5vzsqwlc3agw2fmkwznhbcq5qj6k1kncd07bj48alrv0a4gn")))

(define-public crate-hf2-0.2.0 (c (n "hf2") (v "0.2.0") (d (list (d (n "hidapi") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "scroll") (r "^0.10.0") (d #t) (k 0)))) (h "19whlk83caf6l9zkicd0z4iwskd7gh2yh5l44zzwj5snaz3kxcc2") (f (quote (("default" "hidapi"))))))

(define-public crate-hf2-0.2.1 (c (n "hf2") (v "0.2.1") (d (list (d (n "hidapi") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "scroll") (r "^0.10.0") (d #t) (k 0)))) (h "0wa6i1grcc0w8an6l0lc3c2v9ysrcq668abasmswqlzpr71p6dqr") (f (quote (("default" "hidapi"))))))

(define-public crate-hf2-0.2.2 (c (n "hf2") (v "0.2.2") (d (list (d (n "crc-any") (r "^2.2.3") (o #t) (k 0)) (d (n "goblin") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "hidapi") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "scroll") (r "^0.10.0") (d #t) (k 0)))) (h "0h8by07jfpyshw3kwpfglsj00p915igb2aaz2cvbvca8mg4vpifa") (f (quote (("utils" "maplit" "goblin" "crc-any") ("default" "hidapi" "utils"))))))

(define-public crate-hf2-0.3.0 (c (n "hf2") (v "0.3.0") (d (list (d (n "crc-any") (r "^2.2.3") (o #t) (k 0)) (d (n "goblin") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "hidapi") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "scroll") (r "^0.10.0") (d #t) (k 0)))) (h "1zkzjc98rn41za2f2vfzw9qn6in19dsrv6yqfakam9i7k9619ljy") (f (quote (("utils" "maplit" "goblin" "crc-any") ("default" "hidapi" "utils"))))))

(define-public crate-hf2-0.3.1 (c (n "hf2") (v "0.3.1") (d (list (d (n "crc-any") (r "^2.2.3") (o #t) (k 0)) (d (n "goblin") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "hidapi") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "scroll") (r "^0.10.0") (d #t) (k 0)))) (h "0bcmnjmn4mb3acwpw266f4a28a46xvkszq2nv598dljfm0kcykay") (f (quote (("utils" "maplit" "goblin" "crc-any") ("default" "hidapi" "utils"))))))

(define-public crate-hf2-0.3.2 (c (n "hf2") (v "0.3.2") (d (list (d (n "crc-any") (r "^2.2.3") (o #t) (k 0)) (d (n "goblin") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "hidapi") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "scroll") (r "^0.10.0") (d #t) (k 0)))) (h "0any8yk0ifgfc7g495ziwmn8x38jdflybxs01idqz906p0z1fpps") (f (quote (("utils" "maplit" "goblin" "crc-any") ("default" "hidapi" "utils"))))))

(define-public crate-hf2-0.3.3 (c (n "hf2") (v "0.3.3") (d (list (d (n "crc-any") (r "^2.2.3") (o #t) (k 0)) (d (n "goblin") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "hidapi") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (o #t) (d #t) (k 0)) (d (n "scroll") (r "^0.10.0") (d #t) (k 0)))) (h "0jf5ig4sx98395sq0j34icx8p6rgnn7zl6hb4wnclg35w6hmhdiy") (f (quote (("utils" "maplit" "goblin" "crc-any") ("default" "hidapi" "utils"))))))

