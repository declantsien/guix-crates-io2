(define-module (crates-io #{3}# h hxa) #:use-module (crates-io))

(define-public crate-hxa-0.1.0 (c (n "hxa") (v "0.1.0") (h "0gfiv27288wp58hzrvlz33v8wd3ggsq7im716i4fyayxqv47wmiy")))

(define-public crate-hxa-0.1.1 (c (n "hxa") (v "0.1.1") (h "0z633yx9sva625arqmsj3h4746fygz80fyssrzybfxx57h9gqji5")))

(define-public crate-hxa-0.1.2 (c (n "hxa") (v "0.1.2") (h "0bi7vigj56d7704zy4y2inwkanr806rln72iyf5ckva3j0d4kkjn")))

(define-public crate-hxa-0.1.3 (c (n "hxa") (v "0.1.3") (h "027cza5nx0h9szk2358fiwza51mii0w2znxhinalw625imsc33ww")))

(define-public crate-hxa-0.1.4 (c (n "hxa") (v "0.1.4") (h "0p49znxqkbrgm11gymp6vhh8zzm0490vy1lfscah3yqshk26ydgz")))

(define-public crate-hxa-0.1.5 (c (n "hxa") (v "0.1.5") (h "1afndwgxh3ws6ad89ix2m8k2hfljqjfmya7j8vraf4qv8r9122zf")))

(define-public crate-hxa-0.2.0 (c (n "hxa") (v "0.2.0") (h "0gh88sv3lb74l23ymr10n7p89q29zqlwsy6pkl4aik0mjk825sc6")))

(define-public crate-hxa-0.2.1 (c (n "hxa") (v "0.2.1") (h "00s808lvzy2m4znf3pxrrp6vg021gfws1fbqpql3qcl3kbh6mb22")))

(define-public crate-hxa-0.2.2 (c (n "hxa") (v "0.2.2") (h "191dagi95yhnq143diyzc8qdlzlsfwffpysq4fc6dzkp2qfyg18n")))

