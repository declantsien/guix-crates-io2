(define-module (crates-io #{3}# h hut) #:use-module (crates-io))

(define-public crate-hut-0.1.0 (c (n "hut") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "1kvfxyp5rxhikrvsl6f03g43hgdn4441lydmdx5rykx4sh1kcskg")))

(define-public crate-hut-0.2.0 (c (n "hut") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)))) (h "06cz8gdrkhkl0hs69c93dkp3cn629pm1was1pdzs6i2lhq9n39hh")))

