(define-module (crates-io #{3}# h hfm) #:use-module (crates-io))

(define-public crate-hfm-0.1.0 (c (n "hfm") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)))) (h "106dvrrfnzlwkvy80iggm73ypzwi1c2njrgwy1h2bmlvsic2y847") (y #t)))

(define-public crate-hfm-0.1.1 (c (n "hfm") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.22") (f (quote ("derive"))) (d #t) (k 0)))) (h "132yr3bvsar7a94my2jhqi83xiwb17siniax55292hnzjd2n6wgy") (y #t)))

