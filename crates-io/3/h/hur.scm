(define-module (crates-io #{3}# h hur) #:use-module (crates-io))

(define-public crate-hur-0.1.0 (c (n "hur") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.14") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "httptest") (r "^0.15.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "11d6cl362y7dmcp1607ss63vwpb2kqixzpwwj9wp9qsvbbxqhvf3")))

