(define-module (crates-io #{3}# h hrx) #:use-module (crates-io))

(define-public crate-hrx-0.1.0 (c (n "hrx") (v "0.1.0") (d (list (d (n "jetscii") (r "^0.4") (d #t) (k 0)) (d (n "lazysort") (r "^0.2") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "rustfmt") (r "^0.10") (d #t) (k 1)))) (h "13j4z94vrfx65414zm5cjzzkk1m2bndzvp4gj25p3sgh56shc97m")))

(define-public crate-hrx-0.1.1 (c (n "hrx") (v "0.1.1") (d (list (d (n "lazysort") (r "^0.2") (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "memchr") (r "=2.3.4") (d #t) (k 0)) (d (n "peg") (r "^0.5") (d #t) (k 1)) (d (n "rustfmt") (r "^0.10") (d #t) (k 1)))) (h "0ynjv7akvspsg3bygk3lrajxaxp1ikpbqckvsah6ky07xbv58vbr")))

