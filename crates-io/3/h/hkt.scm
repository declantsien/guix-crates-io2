(define-module (crates-io #{3}# h hkt) #:use-module (crates-io))

(define-public crate-hkt-0.0.0 (c (n "hkt") (v "0.0.0") (h "0fbdn87wz1r8xps0dc890934ggsiybsc55dn2h0rpzxx3sfp6l2r")))

(define-public crate-hkt-0.0.1 (c (n "hkt") (v "0.0.1") (h "1gp9ghwk1ra4r2wx4a154di8p8g0hpd3zksacbvsrqpfj1nap5hl")))

(define-public crate-hkt-0.0.2 (c (n "hkt") (v "0.0.2") (d (list (d (n "shoggoth") (r "*") (d #t) (k 0)))) (h "0vcab717f4w7rsb8ig46fqjn7337ssh3ihhd7vprfp11sjb4dhfr")))

(define-public crate-hkt-0.0.3 (c (n "hkt") (v "0.0.3") (d (list (d (n "shoggoth") (r "*") (d #t) (k 0)))) (h "114nha1fd045m8dsrdvg0dplfns3hkl09pjcbq6djxjx53rl1axi")))

