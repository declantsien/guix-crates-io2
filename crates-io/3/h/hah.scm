(define-module (crates-io #{3}# h hah) #:use-module (crates-io))

(define-public crate-hah-0.0.1 (c (n "hah") (v "0.0.1") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)))) (h "182kgb1x217x5nz1731rs42msy9jplp2vj65s0xzm2ipl8f8sr01")))

(define-public crate-hah-0.0.2 (c (n "hah") (v "0.0.2") (d (list (d (n "ggez") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16p611293a2zcm07s0bk2hc0yzs96q5ahbpaf8i0ppyp4w9kd7ws")))

