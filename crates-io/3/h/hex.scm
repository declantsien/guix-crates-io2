(define-module (crates-io #{3}# h hex) #:use-module (crates-io))

(define-public crate-hex-0.1.0 (c (n "hex") (v "0.1.0") (h "1z24dmzs6i0fqk34svb49j5sklp9wp9q0la8jxdnpskli2zr2h18")))

(define-public crate-hex-0.2.0 (c (n "hex") (v "0.2.0") (h "1ajkw40qzn2ygnqjj9w584f6l31wi318258n84pn2hax8la2i8nn")))

(define-public crate-hex-0.3.0 (c (n "hex") (v "0.3.0") (h "07qbxszv8mgwzzvdx8d2kzyywb8xmici9n87jxbdscs6ar4jcv5n")))

(define-public crate-hex-0.3.1 (c (n "hex") (v "0.3.1") (h "1g1a593abqwjf5xm30mvdl39zxbz6wv51vyy9anh5frph7skr7a5")))

(define-public crate-hex-0.3.2 (c (n "hex") (v "0.3.2") (h "0xsdcjiik5j750j67zk42qdnmm4ahirk3gmkmcqgq7qls2jjcl40") (f (quote (("benchmarks"))))))

(define-public crate-hex-0.4.0 (c (n "hex") (v "0.4.0") (h "0glsfrx2pxfsf6ivxj7vfrvd7g78j4z47ssgm5idm8p376z3jfq2") (f (quote (("std") ("default" "std") ("benchmarks"))))))

(define-public crate-hex-0.4.1 (c (n "hex") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "faster-hex") (r "^0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "1bqlm4cbbpw37jdvdj5nxyw8cp4k6wmyw54gkq62l1i5ymmxmkbn") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-hex-0.4.2 (c (n "hex") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "faster-hex") (r "^0.4") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "0dbf00j3h3pz0lw8jp245rwypna6i23l4cpvym8gsczin9c92kv4") (f (quote (("std") ("default" "std"))))))

(define-public crate-hex-0.4.3 (c (n "hex") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "faster-hex") (r "^0.5") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0w1a4davm1lgzpamwnba907aysmlrnygbqmfis2mqjx5m552a93z") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

