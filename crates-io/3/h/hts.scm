(define-module (crates-io #{3}# h hts) #:use-module (crates-io))

(define-public crate-hts-0.1.17 (c (n "hts") (v "0.1.17") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "18j26dg41fza6wljy8f37702r4i15sps3sz7ab58vgldc6sg4ri8")))

(define-public crate-hts-0.1.18 (c (n "hts") (v "0.1.18") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0mgr4i0rbblk1c1hgd8lkpiyqqcwgy8qnf98b20ljz6idjm3k41v")))

