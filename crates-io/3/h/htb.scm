(define-module (crates-io #{3}# h htb) #:use-module (crates-io))

(define-public crate-htb-0.1.0 (c (n "htb") (v "0.1.0") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)))) (h "04jsjah9xspwhapda8f377brxkaag5k3ylaxng7k7g579jbmyblx") (y #t)))

(define-public crate-htb-0.1.1 (c (n "htb") (v "0.1.1") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)))) (h "0wjmcfradz8aic5801vwds77zyk2ay00rgcjgjf3j4i9w8nv8y5s")))

(define-public crate-htb-0.1.2 (c (n "htb") (v "0.1.2") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "044ffawwm84idbqk2pvwr9dw7av7jqjywfbnp0smbfjvf39ckzhk")))

(define-public crate-htb-0.1.3 (c (n "htb") (v "0.1.3") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05wghzh6g7s0f26r9c55n0l3d3h8d61p3q45h10im1z726gic3w7")))

(define-public crate-htb-0.2.0 (c (n "htb") (v "0.2.0") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1b3zmcq3lc275jhkhakyymjhzmvixfvq4b8yqdaa0krq6ha3qb3s")))

(define-public crate-htb-0.2.1 (c (n "htb") (v "0.2.1") (d (list (d (n "gcd") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "124anhmspmw0g4a5qzkdjfzcbrr6y947q0rnxawh25v8653sanla")))

(define-public crate-htb-0.2.2 (c (n "htb") (v "0.2.2") (d (list (d (n "gcd") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16kzh2c0b11kjka2d7y8570xarw86kk9mprbnz6pxxds4vy6qjlr")))

(define-public crate-htb-0.2.3 (c (n "htb") (v "0.2.3") (d (list (d (n "borsh") (r "^1.2.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "gcd") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xfmrpnyqycd6fh1rkdy8aywf441bnl1lxgj3ga2nh5fc3mj7vl5")))

