(define-module (crates-io #{3}# h hed) #:use-module (crates-io))

(define-public crate-hed-0.1.5 (c (n "hed") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "faccess") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1nxy99zjr6fwnr4bbw3qzks0n6n8wdhsknry7rq5vqm60f7fxhxm")))

(define-public crate-hed-0.1.6 (c (n "hed") (v "0.1.6") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "faccess") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "sudo") (r "^0.6.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1hz8yvs9s3xnd26jlm01y09hsplpmzmbrir5ns56rs6rhilp5ck3")))

