(define-module (crates-io #{3}# h hac) #:use-module (crates-io))

(define-public crate-hac-0.1.0 (c (n "hac") (v "0.1.0") (h "1ahf7hmpszx71q0axdxv00r7fk2j1zj4ddkgdkdvdf86x5cwgxcf")))

(define-public crate-hac-0.1.1 (c (n "hac") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.12.3") (d #t) (k 0)) (d (n "image") (r "^0.24.5") (o #t) (k 0)) (d (n "image") (r "^0.24.5") (d #t) (k 2)) (d (n "pollster") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "wgpu") (r "^0.14.2") (d #t) (k 0)))) (h "1jil5h7c8z3gb478y63d36d0j93pzf44w9iaxpvrl6hlcw2saw3v") (f (quote (("default")))) (s 2) (e (quote (("from_image" "dep:image"))))))

