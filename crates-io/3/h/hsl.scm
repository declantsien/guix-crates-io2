(define-module (crates-io #{3}# h hsl) #:use-module (crates-io))

(define-public crate-hsl-0.1.0 (c (n "hsl") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.15") (o #t) (d #t) (k 0)))) (h "1pq9g59j43p3pmmb74pz8adzssnb23nk614zgwcqbcz6rlz899iv") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-hsl-0.1.1 (c (n "hsl") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.15") (o #t) (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.24") (d #t) (k 2)))) (h "0w69w7g522d5qrjkkany3x361i4aj4afp42yhbnqhfvz2vqvfpsp") (f (quote (("dev" "clippy") ("default"))))))

