(define-module (crates-io #{3}# h h11) #:use-module (crates-io))

(define-public crate-h11-0.0.0 (c (n "h11") (v "0.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1xis083gcin814jfjwvcr0d5avricvhpvq4ar4mkx8dzrm3956kf")))

(define-public crate-h11-0.1.0 (c (n "h11") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1azyiqcp0ndhynhq0dy1238g26dqfl5ns2lrak52v3npscbj4gxx")))

