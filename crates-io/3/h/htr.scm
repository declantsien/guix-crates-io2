(define-module (crates-io #{3}# h htr) #:use-module (crates-io))

(define-public crate-htr-0.5.2 (c (n "htr") (v "0.5.2") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "07ljhpd2axydkndkkqak78kykpcf6210sbqzjqblivplvrc47gas")))

(define-public crate-htr-0.5.3 (c (n "htr") (v "0.5.3") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0dpac390gfm7z80bknfb7mlf8i579z1hp8dwif189nqr8idf69x7")))

(define-public crate-htr-0.5.4 (c (n "htr") (v "0.5.4") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1dx6n6ma1xyamxpr8fqzj8z91qmpbkm75whdk9z5sdj7s8lp05nl")))

(define-public crate-htr-0.5.5 (c (n "htr") (v "0.5.5") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1cvz00bpmcv4y7jp6rz8lb1r8b35wkas90qbs099zbf1hwf1506w")))

(define-public crate-htr-0.5.6 (c (n "htr") (v "0.5.6") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "186d2zj8y0yh7sqzn5p1ka64q9ylbdv6x5qr6zps0q68wa86k5qq")))

(define-public crate-htr-0.5.7 (c (n "htr") (v "0.5.7") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1vw5ff6ck6g2ll3sfffh5prnr80qif73qa0n3wrm4j2z3wwbk8p5") (y #t)))

(define-public crate-htr-0.5.9 (c (n "htr") (v "0.5.9") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1qyjxqbffrznn50lwaj7yqihxn1969dyk51gvm2mfp5pw702mc4x") (y #t)))

(define-public crate-htr-0.5.10 (c (n "htr") (v "0.5.10") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1kr9j13jrhwdqlsjz11bg9cxzzgcck3ibw81j55alwx0smpjwkkv")))

(define-public crate-htr-0.5.11 (c (n "htr") (v "0.5.11") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "102zl1141kp75kani4k025an11qixs1zrl7r2g47mc04szdcbykx")))

(define-public crate-htr-0.5.12 (c (n "htr") (v "0.5.12") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1ff6k8ir5zjskksa207c1sj9svhywpjsszsc6kh161fvxd3yxiyn") (y #t)))

(define-public crate-htr-0.5.13 (c (n "htr") (v "0.5.13") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0v37bks21ywprdnvxiz8mq7prifq3yw66rsd8dwqcga5pjpgghvk") (y #t)))

(define-public crate-htr-0.5.14 (c (n "htr") (v "0.5.14") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1i10qajdmp9dgl5570s6z2imnykfw255cd1w00b0sx13plcw439v") (y #t)))

(define-public crate-htr-0.5.15 (c (n "htr") (v "0.5.15") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1bnh8d4c25dmjhz856bm7bdpmbk6wvni00kv3xg2kpjf1zgdys2p") (y #t)))

(define-public crate-htr-0.5.16 (c (n "htr") (v "0.5.16") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1vy70sxkpg71jp75l9wk3qzi7m4jgssr230jgwnrklnw0zs0l5ry") (y #t)))

(define-public crate-htr-0.5.17 (c (n "htr") (v "0.5.17") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "00hqcjk9wyvgk2sckj1iyz156dmsax6spk09qsr4l8xc5la0kdgy")))

(define-public crate-htr-0.5.18 (c (n "htr") (v "0.5.18") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0psznx1skcgbnipj2pl49jxw49dayywm8r42nh78mg0qay7pz5d7")))

(define-public crate-htr-0.5.19 (c (n "htr") (v "0.5.19") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "02ggqzx7zvv1xg7y2ivjpkpsfysd73jqayn340zzv0bvz4iwrsp1")))

(define-public crate-htr-0.5.20 (c (n "htr") (v "0.5.20") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "04qcz2jbwm8ljrj0invmkcs2pcxxxbdkzj12x6kzm1mkldmjs23w") (y #t)))

(define-public crate-htr-0.5.21 (c (n "htr") (v "0.5.21") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0n8qrjcx0b4y1nbr4njb7dxb484l96rkaqb3ci01wicz8ynb6nwy") (y #t)))

(define-public crate-htr-0.5.22 (c (n "htr") (v "0.5.22") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01r6970f3k2992ih7fmv85vykdh4z9jka9jrjjg2asivj9d7bakd")))

(define-public crate-htr-0.5.23 (c (n "htr") (v "0.5.23") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1j77wcgs4dypsm73cbkvc8w4vndmx8s7znp0gbm0d7lxwqwqvnnv")))

(define-public crate-htr-0.5.24 (c (n "htr") (v "0.5.24") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "13i4ag7zqfd5xwp0kjxk3sjipw9mr55x7kl9yv8zy84byj69nvyy")))

(define-public crate-htr-0.5.25 (c (n "htr") (v "0.5.25") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0pbm072bmjsv86irlxbxc3kvd4q7yvb2rhhjncmy88mh86r7036w")))

(define-public crate-htr-0.5.26 (c (n "htr") (v "0.5.26") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "18l4y092zrl49jj346iqvk0lqvfl916pcq6fq7pbx5dgi7dhk3iq")))

(define-public crate-htr-0.5.27 (c (n "htr") (v "0.5.27") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1j7gxx21fclgixmggknnw4ayrf131pj4pqwmzm8lpqlc8n8523lb")))

