(define-module (crates-io #{3}# h hmm) #:use-module (crates-io))

(define-public crate-hmm-0.1.0 (c (n "hmm") (v "0.1.0") (h "1drxfn7zljdcq81rhxpds5w72qdqywmg108bd1443vrl7rbj1jsp")))

(define-public crate-hmm-0.2.0 (c (n "hmm") (v "0.2.0") (h "1gbi9cqvk73wm9n817mar2ns2b3fj5kb831k4a94f4jnlry863v5")))

