(define-module (crates-io #{3}# h hit) #:use-module (crates-io))

(define-public crate-hit-0.1.0 (c (n "hit") (v "0.1.0") (d (list (d (n "bossy") (r "^0.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once-cell-regex") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0ahrhz8502hp4ph031j53mm83ldrvwv03lpka24la02bkpvkgcwk")))

(define-public crate-hit-0.2.0 (c (n "hit") (v "0.2.0") (d (list (d (n "bossy") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once-cell-regex") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1z9jzrz1wf8v7d22k7p5ph7gzbr83c3z3qnxnd8jlakf7h62i2qa")))

