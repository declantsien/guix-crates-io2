(define-module (crates-io #{3}# h hwp) #:use-module (crates-io))

(define-public crate-hwp-0.1.0 (c (n "hwp") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfb") (r "^0.7") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hwp_macro") (r "^0.1.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "18v0024ld7nzsg42xpa41wif7gi5g8kfmfhb2c1cajibyqvkvyfr")))

(define-public crate-hwp-0.2.0 (c (n "hwp") (v "0.2.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "cfb") (r "^0.7") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "hwp_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1aq87k4n4namhnyvv4n0c90mnvrz1ii7g7zrrgq82xlz5fnnpbgi")))

