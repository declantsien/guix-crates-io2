(define-module (crates-io #{3}# h hup) #:use-module (crates-io))

(define-public crate-hup-0.1.0 (c (n "hup") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "ipnet") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "libtun") (r "^0.1.0") (d #t) (k 0)) (d (n "smol") (r "^0.1.11") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (f (quote ("backtraces"))) (d #t) (k 0)))) (h "12fc6pr3kans3hbhrmfidvbvwvxsdf5s0x7mhigbhrkjs6i3v8w9")))

