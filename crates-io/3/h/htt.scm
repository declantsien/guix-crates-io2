(define-module (crates-io #{3}# h htt) #:use-module (crates-io))

(define-public crate-htt-0.1.0 (c (n "htt") (v "0.1.0") (h "14rmm3w7gdywj0qhnjmx07jcrxynihjvgr3qylbjpbvas99xipq4") (f (quote (("std")))) (y #t)))

(define-public crate-htt-0.1.1 (c (n "htt") (v "0.1.1") (h "080jand2yq898jl00crpf6xdlxk4gncrdzdnsh98kyd06841djpl") (f (quote (("std")))) (y #t)))

(define-public crate-htt-0.1.2 (c (n "htt") (v "0.1.2") (h "1ivn5qc5fzf5lngwv04yjabxnjr1mz8wfaj37b5v213r8p0nnnmz") (f (quote (("std")))) (y #t)))

(define-public crate-htt-0.2.0 (c (n "htt") (v "0.2.0") (h "158bz0alfzlb3l17gf4scr5192hviqsr0j87h4mya0fpz71qzg94") (f (quote (("std")))) (y #t)))

(define-public crate-htt-0.3.0 (c (n "htt") (v "0.3.0") (h "1nyhrc50z4qrwh4npzilqy4445kk0c3wpa2xcrvfz7rabh4c66x7") (f (quote (("std")))) (y #t)))

(define-public crate-htt-0.4.0 (c (n "htt") (v "0.4.0") (h "1zi1zcndf0kgbbi105xbb8k2b7rlsaslsf1g1vqh8xblsfv60zgr") (f (quote (("std")))) (y #t)))

(define-public crate-htt-0.4.1 (c (n "htt") (v "0.4.1") (h "1bhdyy0ki0vavmgfkaffvk4gd4kxryx443sxgjlny6vhxdc5wkn4") (f (quote (("std")))) (y #t)))

(define-public crate-htt-0.5.0 (c (n "htt") (v "0.5.0") (h "17nd2jhh0pcn0p7dsvj8bd8gac7dmy00lylwcgp9535033f5glfy") (f (quote (("std")))) (y #t)))

