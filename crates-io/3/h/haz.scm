(define-module (crates-io #{3}# h haz) #:use-module (crates-io))

(define-public crate-haz-0.1.0 (c (n "haz") (v "0.1.0") (h "0q6dxc004vz7ak02g191js2mbf352bvxm8gh2lzq1kbvpgx8xnkl")))

(define-public crate-haz-0.1.1 (c (n "haz") (v "0.1.1") (h "1fnzz6q9nxgf3za71dj8pzx1ajbdapqvggkgspx4nqf9n12q6l38")))

