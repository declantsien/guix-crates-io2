(define-module (crates-io #{3}# h hxt) #:use-module (crates-io))

(define-public crate-hxt-0.1.0 (c (n "hxt") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hext") (r "^0.4") (d #t) (k 0)))) (h "0bc99ngk7ykcyi53k45yd5kfbv63ap4z747vvfn31g30j77065xd")))

(define-public crate-hxt-0.1.1 (c (n "hxt") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hext") (r "^0.4") (d #t) (k 0)))) (h "06w9x7bxb06ynyxprad35b6ywllc8s5qimrjqrqrgqac3m37wj1w")))

(define-public crate-hxt-0.1.2 (c (n "hxt") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "hext") (r "^0.4.2") (d #t) (k 0)))) (h "049p81kjjk3x2mbspal0dh5g6mki5k3iych6g7xwa87pwh72cc06")))

