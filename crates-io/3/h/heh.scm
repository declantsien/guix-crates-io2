(define-module (crates-io #{3}# h heh) #:use-module (crates-io))

(define-public crate-heh-0.1.0 (c (n "heh") (v "0.1.0") (d (list (d (n "arboard") (r "^2.1") (k 0)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "1x1i4mmygaslph05yl3hhzhisp47zdiv1x64pf10lx2wngsrj49p")))

(define-public crate-heh-0.1.1 (c (n "heh") (v "0.1.1") (d (list (d (n "arboard") (r "^2.1") (k 0)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "1694c03lnsad8hzgsv0ii1mqqj7gl1glkbc90gzfabd1mnzz72pq")))

(define-public crate-heh-0.2.0 (c (n "heh") (v "0.2.0") (d (list (d (n "arboard") (r "^2.1") (k 0)) (d (n "clap") (r "^3.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "tui") (r "^0.18") (d #t) (k 0)))) (h "176i0qwwn2z20gl141bmk99wvj8dam9ghsap7dkkw1n5i90gk8as") (r "1.59.0")))

(define-public crate-heh-0.3.0 (c (n "heh") (v "0.3.0") (d (list (d (n "arboard") (r "^3.2") (k 0)) (d (n "clap") (r "^4.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0amwh9zv8q8h2g17maqgmphidlqsq0drai6d81f91jxdbwxmka27") (r "1.64.0")))

(define-public crate-heh-0.3.1 (c (n "heh") (v "0.3.1") (d (list (d (n "arboard") (r "^3.2") (k 0)) (d (n "clap") (r "^4.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0708yba0glckar71w8rsismsxphbpjvzxa0k6bmf09x8hsw77xds") (r "1.64.0")))

(define-public crate-heh-0.4.0 (c (n "heh") (v "0.4.0") (d (list (d (n "arboard") (r "^3.2.0") (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "tui") (r "^0.22.0") (f (quote ("crossterm"))) (k 0) (p "ratatui")))) (h "03bsqx8pmwcqhr3r8zjrp5xmdmamkvkcdz5r8hi19aj5mr1cidbp") (r "1.65.0")))

(define-public crate-heh-0.4.1 (c (n "heh") (v "0.4.1") (d (list (d (n "arboard") (r "^3.2.0") (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.2") (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "tui") (r "^0.22.0") (f (quote ("crossterm"))) (k 0) (p "ratatui")))) (h "0vgk00iz09d6a6xdbsabk90aj5lbhnxsqkbbgasqn7qx7nj4pkag") (r "1.65.0")))

(define-public crate-heh-0.5.0 (c (n "heh") (v "0.5.0") (d (list (d (n "arboard") (r "^3.3.2") (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.4") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.4") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "1hm5a65255s21cr80a434pv5q7xf0scx812rj6y7pghlj0kknb46")))

