(define-module (crates-io #{3}# h hmw) #:use-module (crates-io))

(define-public crate-hmw-0.1.0 (c (n "hmw") (v "0.1.0") (d (list (d (n "fluent") (r "^0.16.0") (d #t) (k 0)) (d (n "nolog") (r "^1") (f (quote ("debug"))) (d #t) (k 0)))) (h "12q43k6jgd4xvim88fa5n7sqkhb1gb3wyz6sjclpl3czjs3f70bv") (f (quote (("warn" "nolog/warn" "nolog_setup") ("trace" "nolog/trace" "nolog_setup") ("nolog_setup") ("logonly" "nolog/logonly") ("logmod" "nolog/logmod") ("logcatch" "nolog/logcatch") ("info" "nolog/info" "nolog_setup") ("error" "nolog/error" "nolog_setup") ("debug" "nolog/debug" "nolog_setup") ("crit" "nolog/crit" "nolog_setup"))))))

