(define-module (crates-io #{3}# h hsk) #:use-module (crates-io))

(define-public crate-hsk-0.1.0 (c (n "hsk") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)))) (h "0dh6sbdcc962nxzaxm4gqcqj8v8sjh3j4jnk945kk6dafl97pzlb")))

(define-public crate-hsk-0.1.1 (c (n "hsk") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)))) (h "0l40lsyjn68si394k5n06mjsqpc54pix2l28iarasqnd6i7vp9cm")))

