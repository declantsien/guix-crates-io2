(define-module (crates-io #{3}# h hci) #:use-module (crates-io))

(define-public crate-hci-0.1.0 (c (n "hci") (v "0.1.0") (d (list (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^4.1") (d #t) (k 0)) (d (n "protogen") (r "^0.1") (d #t) (k 1)))) (h "0xr1bgc23ql7g8gmdi966p3zvz1cvwg2k0k7l3wfgz4ci3khy89i")))

