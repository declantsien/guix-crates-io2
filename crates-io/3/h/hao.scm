(define-module (crates-io #{3}# h hao) #:use-module (crates-io))

(define-public crate-hao-0.1.0 (c (n "hao") (v "0.1.0") (h "1nbkq1k9hv4g0qc1vz6hb1r05pa0vzb3ysxiic81gkgn0kngmawv") (y #t)))

(define-public crate-hao-0.0.1-prerelease (c (n "hao") (v "0.0.1-prerelease") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "goblin") (r "^0.6.1") (f (quote ("pe32"))) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1742v6vcg37cdm18rqwyik2vhm0b4qfg71az4pg7jsmh0887077y")))

(define-public crate-hao-0.0.2-prerelease (c (n "hao") (v "0.0.2-prerelease") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "goblin") (r "^0.6.1") (f (quote ("pe32"))) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "1f9p7mbvlwkaynsjd8j5vmqsjpgz6y8vvg270zm76dbilmc96gd7")))

(define-public crate-hao-0.0.3 (c (n "hao") (v "0.0.3") (d (list (d (n "bitflags") (r "^2.2.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "pewter") (r "^0.0.3") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (d #t) (k 0)))) (h "00cfnsi64ri07ayvkkfrp99s428faaqx7kzzs8jcp5mdpvzmk6dc")))

