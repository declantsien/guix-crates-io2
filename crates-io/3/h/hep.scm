(define-module (crates-io #{3}# h hep) #:use-module (crates-io))

(define-public crate-hep-0.1.0 (c (n "hep") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1v89x203k3nm3fp6hjmnw6gaw5s70fl0qywc63i0qwbb4yl01jfd")))

(define-public crate-hep-0.1.1 (c (n "hep") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "09766fxgrkknc6yrwaznaiqz0i491cmr22m6rkn1isx6favaf62d")))

(define-public crate-hep-0.1.2 (c (n "hep") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "15w9pyv6n5bcd97i88qhy9wxl5xx9rw2qvy4nkwz6q6bs92m6djx")))

