(define-module (crates-io #{3}# h hl7) #:use-module (crates-io))

(define-public crate-hl7-0.0.1 (c (n "hl7") (v "0.0.1") (h "1cxlq1hz6nw5f3iffwr7nznfaskqkbqj96jx4j6gf83a4hkkqsx7")))

(define-public crate-hl7-0.0.2 (c (n "hl7") (v "0.0.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qsk714ijyfiz9nvdwwpbw1vvmpfb4r9if15bkxd913phlcbaxsi")))

