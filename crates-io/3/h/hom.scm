(define-module (crates-io #{3}# h hom) #:use-module (crates-io))

(define-public crate-hom-0.1.0 (c (n "hom") (v "0.1.0") (d (list (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "homunculus") (r "^0.1") (d #t) (k 0)) (d (n "muon-rs") (r "^0.2") (d #t) (k 0)))) (h "18fr9bikxapnbw4k28jc58fhrl2v4mq20pgsydcjjnzd167h0aml")))

(define-public crate-hom-0.2.0 (c (n "hom") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "homunculus") (r "^0.2") (d #t) (k 0)) (d (n "muon-rs") (r "^0.2") (d #t) (k 0)))) (h "0by6cyknkwwr6j2m3wn35py841gly27sa6vzxr7ydp2an4r3x8d9")))

(define-public crate-hom-0.3.0 (c (n "hom") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bevy") (r "^0.8.0") (d #t) (k 0)) (d (n "homunculus") (r "^0.3") (d #t) (k 0)) (d (n "muon-rs") (r "^0.2") (d #t) (k 0)))) (h "0dlypssdlvbgvjw5i45185gga2splyfzizqdlcf5ip9a2qcv9b02")))

(define-public crate-hom-0.4.0 (c (n "hom") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bevy") (r "^0.12") (d #t) (k 0)) (d (n "glam") (r "^0.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "homunculus") (r "^0.4") (d #t) (k 0)) (d (n "muon-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1yiy6biwbr2bm9p51ywhm5grl07wmv751k5kkbk3q2w5v62dw2z3")))

(define-public crate-hom-0.5.0 (c (n "hom") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "glam") (r "^0.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "homunculus") (r "^0.5.0") (d #t) (k 0)) (d (n "muon-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0z63lrx1inmhwaa9frhpssmn2d3s42kk6p3hhppqbzcjwz0rcl0m")))

