(define-module (crates-io #{3}# h hcp) #:use-module (crates-io))

(define-public crate-hcp-0.2.0-alpha (c (n "hcp") (v "0.2.0-alpha") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0410383971bc4p6qz1lw5ng3anz57dpqr7d4dcgfag4bb1352cqn")))

(define-public crate-hcp-0.2.0 (c (n "hcp") (v "0.2.0") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (d #t) (k 0)))) (h "0xwdcbq67d850b4vllwdc90wyyf70wlid9p4az551jigffrcjkp9")))

