(define-module (crates-io #{3}# h han) #:use-module (crates-io))

(define-public crate-han-0.1.0 (c (n "han") (v "0.1.0") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)))) (h "00kdfhjgfac6bm9ymfjkyqfzs1qnaqsbhmr7c3ql0la5yzjszh9c")))

(define-public crate-han-0.1.1 (c (n "han") (v "0.1.1") (d (list (d (n "crc16") (r "^0.4.0") (d #t) (k 0)) (d (n "time") (r "^0.3.15") (f (quote ("parsing" "macros"))) (k 0)))) (h "1g5d5paxra8s42kqy24r6dlkxfb0mrpqrjkhf1nmkfn5iqxvbnpx")))

(define-public crate-han-0.2.0-alpha.0 (c (n "han") (v "0.2.0-alpha.0") (d (list (d (n "crc16") (r "^0.4") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("parsing" "macros"))) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1xji35c72wqw0912wxdx65zwi5hxl52y8w73pjqzw5i3f7n32ly3") (f (quote (("std" "embedded-io-async/std")))) (s 2) (e (quote (("embedded-io-async" "dep:embedded-io-async") ("defmt-03" "dep:defmt"))))))

