(define-module (crates-io #{3}# h hai) #:use-module (crates-io))

(define-public crate-hai-0.1.0 (c (n "hai") (v "0.1.0") (d (list (d (n "hai_core") (r "^0.1") (d #t) (k 0)) (d (n "hai_js_runtime") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "hai_pal") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 0)))) (h "1szgp2myzgc8xp6xmqlff95ma96saklw65f4j8lhcm3598jr26lx") (f (quote (("web" "hai_core/web") ("video" "hai_core/video") ("desktop" "hai_core/desktop") ("default" "desktop"))))))

