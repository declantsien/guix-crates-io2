(define-module (crates-io #{3}# h hrp) #:use-module (crates-io))

(define-public crate-hrp-0.1.0 (c (n "hrp") (v "0.1.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)))) (h "1sz54rlsigzv434qgvvfqxggkkvb9fkyb6ppbiyscbxpr16s1jv0")))

(define-public crate-hrp-0.1.1 (c (n "hrp") (v "0.1.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2.5") (d #t) (k 0)))) (h "069a2d358chzdppmfmg0sl0dq19qg0bbby4rf5avdqsn9libzfn0")))

