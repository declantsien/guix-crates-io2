(define-module (crates-io #{3}# h hbp) #:use-module (crates-io))

(define-public crate-hbp-0.1.0 (c (n "hbp") (v "0.1.0") (h "0s2w7y4gf0q8ylma5niggi1i98gqqfwh7k13v4j2kd0fynwyybqx")))

(define-public crate-hbp-0.2.0 (c (n "hbp") (v "0.2.0") (h "05zra3asq4d22857s5jwkycbflc722xrbh8hsdf396yya2bfxi51")))

(define-public crate-hbp-0.3.0 (c (n "hbp") (v "0.3.0") (h "0z1dz1rkqxa436ipgpsj4kpmmdnyb7y3ll6a0iazdi908k4liv1x")))

