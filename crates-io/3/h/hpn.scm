(define-module (crates-io #{3}# h hpn) #:use-module (crates-io))

(define-public crate-hpn-0.2.0 (c (n "hpn") (v "0.2.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1kzxlbwn3327alwnxa56brvjzdxx45jzh5aci6mjyn92ving086n") (y #t)))

(define-public crate-hpn-0.3.0 (c (n "hpn") (v "0.3.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1k3qdv4dlrfs9r2djv1c1669m27sm27b1d4pg9q49p6cpb0rqi1v")))

(define-public crate-hpn-0.3.1 (c (n "hpn") (v "0.3.1") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1lcqpy6rgq5cjmi0jg1n0f2jcrcmhv3s3b0137iq4b5ff2v0pb79")))

(define-public crate-hpn-0.3.2 (c (n "hpn") (v "0.3.2") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "134dqaqcwhbv82jnprs60nmnrjzayhfz8ywz59w3radm1ch2gpra")))

(define-public crate-hpn-0.4.0 (c (n "hpn") (v "0.4.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "00zvfajcls5ag4nbq1v4lkxq2pvn4fdrx28q7zab11jbjb3n4jyc")))

(define-public crate-hpn-0.5.0 (c (n "hpn") (v "0.5.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0k2s6npq3f9gblgsjx8kgpv0a0gjsp7pllyfq8f746vymny77971")))

(define-public crate-hpn-0.6.0 (c (n "hpn") (v "0.6.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1c3jhh1l3lgpdbnfwqfwan4i7g3y9xkb8v1jq2h1252njbn8kzy5")))

(define-public crate-hpn-0.6.1 (c (n "hpn") (v "0.6.1") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1djzv1scrdysfw58jfrwg92jdm6b9ixcp8mkkrxd5n2li0f3001k")))

(define-public crate-hpn-0.6.2 (c (n "hpn") (v "0.6.2") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "04cqqm4zjh32x4zzbayz07qs0kcx8hd2bj05vxqf8xd48caqnbm0")))

(define-public crate-hpn-0.6.3 (c (n "hpn") (v "0.6.3") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "getrandom") (r "~0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0n4ghxskn48a1gafidahlwnsqqq4kjsbjxqkq92v7f9ab7sv60z5")))

(define-public crate-hpn-0.6.4 (c (n "hpn") (v "0.6.4") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "getrandom") (r "~0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "02bv0p159inry0y7q51c8ccz4y119lpqsb7mbvqwppsa8g7vmfqz")))

(define-public crate-hpn-0.6.5 (c (n "hpn") (v "0.6.5") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "getrandom") (r "~0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "01xx1v58p4v6fnq1729v2glrq839ax7sw3z50l1mzrg7p8bhv76z")))

(define-public crate-hpn-0.7.0 (c (n "hpn") (v "0.7.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "getrandom") (r "~0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0nlkycmlpl48jxkcagi3gw5iccdznqnkgccnllyg6nvsy6fy8zgf")))

(define-public crate-hpn-0.7.1 (c (n "hpn") (v "0.7.1") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "getrandom") (r "~0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0ifmqmn2byc6g1rm7pzg4fjpkhvvn8hdz9x7qz4w0y2d1r235cjx")))

