(define-module (crates-io #{3}# h hdb) #:use-module (crates-io))

(define-public crate-hdb-0.1.0 (c (n "hdb") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13a69rvanzawih93sp0kb05iqchwkmkpggxhnpzvnjdxif5xdwcx")))

(define-public crate-hdb-0.1.1 (c (n "hdb") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "122hk01a3swqm1d3infy09xlgjrxmsl0zjm0gggin29k82fsqfa7")))

(define-public crate-hdb-0.1.2 (c (n "hdb") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1rwm1mwqwxj8a75hwzls1qzpjwkmszdcapqzdyhh0bc35f1fq1ws")))

(define-public crate-hdb-0.1.3 (c (n "hdb") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zqnj55w3506k9iwk4pcd725smay3vajld49wagw5x43v00pmmsz")))

