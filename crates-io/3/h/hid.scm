(define-module (crates-io #{3}# h hid) #:use-module (crates-io))

(define-public crate-hid-0.1.0 (c (n "hid") (v "0.1.0") (d (list (d (n "hidapi-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12i2kc36drhbb5fmzk9afdd5w55nc92fpr9sq4mwjpqkqllk8p8k") (f (quote (("static" "hidapi-sys/static") ("build" "static" "hidapi-sys/build"))))))

(define-public crate-hid-0.1.1 (c (n "hid") (v "0.1.1") (d (list (d (n "hidapi-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13g77my838sqib0y1spa9w6dzxpbibq9xifj5h9c34f453lb554d") (f (quote (("static" "hidapi-sys/static") ("build" "static" "hidapi-sys/build"))))))

(define-public crate-hid-0.2.0 (c (n "hid") (v "0.2.0") (d (list (d (n "hidapi-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pm3v03mwvwj7g54jlj2h4mrc12hyw76c1s79fa8jwq0g9n112sk") (f (quote (("static" "hidapi-sys/static") ("build" "static" "hidapi-sys/build"))))))

(define-public crate-hid-0.3.0 (c (n "hid") (v "0.3.0") (d (list (d (n "hidapi-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1fg9n3yapi7wfa9cvz6m2invj08p3f9wnp76s5qgl8xns9v5khlq") (f (quote (("static" "hidapi-sys/static") ("build" "static" "hidapi-sys/build"))))))

(define-public crate-hid-0.3.1 (c (n "hid") (v "0.3.1") (d (list (d (n "hidapi-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1y9r93fc76pq3m4sa57dmv3j3ycqkn8y7frbx8w3h7ghwfr4pv3l") (f (quote (("static" "hidapi-sys/static") ("build" "static" "hidapi-sys/build"))))))

(define-public crate-hid-0.4.0 (c (n "hid") (v "0.4.0") (d (list (d (n "hidapi-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "00j9g8sxaxsjwp7998fxajw1vqzabpvggwp0hp4kv3frrc0r7i3n") (f (quote (("static" "hidapi-sys/static") ("build" "static" "hidapi-sys/build"))))))

(define-public crate-hid-0.4.1 (c (n "hid") (v "0.4.1") (d (list (d (n "hidapi-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.21") (d #t) (k 0)))) (h "1jv5l9i0clnj188xlrv6bb8n06j9spv6a73gps3sazh1dmc0hqjj") (f (quote (("static" "hidapi-sys/static") ("build" "static" "hidapi-sys/build"))))))

