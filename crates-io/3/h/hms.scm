(define-module (crates-io #{3}# h hms) #:use-module (crates-io))

(define-public crate-hms-0.1.0 (c (n "hms") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "better-panic") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "cli-clipboard") (r "^0.4.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "hms-common") (r "^0.1.0") (d #t) (k 0)) (d (n "hms-db") (r "^0.1.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.2.3") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p4r5jd45nk6kq960c6syz9dlb55hnn524brqvgjxkl2bx59rigy")))

