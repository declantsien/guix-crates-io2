(define-module (crates-io #{3}# h hdv) #:use-module (crates-io))

(define-public crate-hdv-0.1.0 (c (n "hdv") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "integer-encoding") (r "^4") (d #t) (k 0)) (d (n "polars") (r "^0.40") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wl7p9n1hljw9ysjf7k0nbhdkl4zwjjg2986pfsf1zgm9sr74zg1") (f (quote (("default"))))))

(define-public crate-hdv-0.1.1 (c (n "hdv") (v "0.1.1") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "integer-encoding") (r "^4") (d #t) (k 0)) (d (n "polars") (r "^0.40") (o #t) (d #t) (k 0)) (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "strum") (r "^0.26") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c5nyjxdk7hl97vjk8jngpgrk2r8hqxrn1799g53wnnz99kpz3zn") (f (quote (("default"))))))

