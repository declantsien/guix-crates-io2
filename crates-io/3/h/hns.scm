(define-module (crates-io #{3}# h hns) #:use-module (crates-io))

(define-public crate-hns-0.1.1 (c (n "hns") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "humnum") (r ">=0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xca21k332ywh4hmmgbhbh5jxab02gxqc6qgr3l41wl2bivlg9sh") (f (quote (("force_color" "humnum/force_color"))))))

(define-public crate-hns-0.2.0 (c (n "hns") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "humnum") (r ">=0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0y7ppw1x5r517rirdmcy4rz0cw1ajd8nz9k3kz0yfmjjivjz2a49") (f (quote (("force_color" "humnum/force_color"))))))

