(define-module (crates-io #{3}# h hrs) #:use-module (crates-io))

(define-public crate-hrs-0.1.0 (c (n "hrs") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)))) (h "04yqyc3lj4y0i5gij0q8s24vkf47h71vmd1aj03gbs7b15b3farm") (y #t)))

(define-public crate-hrs-0.1.1 (c (n "hrs") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1s5irnbi6j7py83bb52l51gyg5qls1ggazxygjsqb19477bsv9fy") (y #t)))

(define-public crate-hrs-0.2.0 (c (n "hrs") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1v99cgazq6zvqlwlw0h0ys10pjicq0ngfbkzsq5ms8601j5i7sd3")))

(define-public crate-hrs-0.2.1 (c (n "hrs") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "dirs") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "16xkqf1xffc5lygxqd1bk3sgpwmfyk0bbylqxq5h848kx116xhb1")))

