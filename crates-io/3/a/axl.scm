(define-module (crates-io #{3}# a axl) #:use-module (crates-io))

(define-public crate-axl-0.1.0 (c (n "axl") (v "0.1.0") (h "0d1m0q4yrqj2zrbmdv55gsrnm0860rv812p0bnvzzv2x9990x9dk") (y #t)))

(define-public crate-axl-0.1.1 (c (n "axl") (v "0.1.1") (h "0nv1cp5vxsay5zj5p0sv9j64wy7pnvh5rwxh7vlfyx9sbrmkl97n") (y #t)))

(define-public crate-axl-0.2.0 (c (n "axl") (v "0.2.0") (h "0wq2smckad86pdl30d9bc5qaphf53kcf6c23v09363igybc5fpic") (y #t)))

(define-public crate-axl-0.2.1 (c (n "axl") (v "0.2.1") (h "0rccvfp9dm91nhh44sghpy1slwr5lz77m52nrc7d3a7iy2plgs46") (y #t)))

