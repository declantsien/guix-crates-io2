(define-module (crates-io #{3}# a ayu) #:use-module (crates-io))

(define-public crate-ayu-0.1.3 (c (n "ayu") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0sk4v4qb4gg0y9ny6wf4nx6hyacb88ccysc3y7kagiz8jrwa4mrl")))

(define-public crate-ayu-0.1.4 (c (n "ayu") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0f9yp3bvpk4k542wk070qx25k127ibcs1g5viv67kg065d3mqjyx")))

(define-public crate-ayu-0.2.2 (c (n "ayu") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0vl58yq1z8g1z6zrf497qxcscanb4lq5mqg92hhjvswy6ham0c9i")))

(define-public crate-ayu-0.3.0 (c (n "ayu") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "107mmh1rqk7c92xppwci4k41h16b78jyjsh1zjai121pbjyslifh")))

