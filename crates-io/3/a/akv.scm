(define-module (crates-io #{3}# a akv) #:use-module (crates-io))

(define-public crate-akv-0.1.0 (c (n "akv") (v "0.1.0") (d (list (d (n "always-assert") (r "^0.1.2") (d #t) (k 0)) (d (n "libarchive_src") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "01vcc5wsgn2yijpdbiwb117zcypv59ind7y2qanjrss8cas1jbdr") (r "1.65")))

