(define-module (crates-io #{3}# a adc) #:use-module (crates-io))

(define-public crate-adc-0.1.0 (c (n "adc") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)))) (h "1mw2m9y32si95l6i1g2ix2mpz77qs1cmd7h2h9d13wnagmsvs4my")))

(define-public crate-adc-0.2.0 (c (n "adc") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0clxsj99xi9b4dlgla7m6yz9qqpad7n38iq9rhrxfccvalyc7m43")))

(define-public crate-adc-0.2.1 (c (n "adc") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "11fpl2m4v3y2gz6nsrxlk7l7agj641qyvv6h32kdv6p4vcsg3ldq")))

