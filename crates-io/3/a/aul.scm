(define-module (crates-io #{3}# a aul) #:use-module (crates-io))

(define-public crate-aul-1.0.0 (c (n "aul") (v "1.0.0") (h "11dnsdfx9d029nd5hd126p8xnj71visbg47isaixzjkphrbsp7fr")))

(define-public crate-aul-1.0.1 (c (n "aul") (v "1.0.1") (h "1563mk8azzya3vy7i5k9r1ngagc951i88bnhab1pr80nqc1kbpz3")))

(define-public crate-aul-1.1.1 (c (n "aul") (v "1.1.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "1zsbaml2ckpnjrg99r43pv7k58fbhwkzn26pa8yrmdpdsbsivmf8") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-aul-1.1.2 (c (n "aul") (v "1.1.2") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "08xqlakwplhwpdvr0wrf26gnkci0wl96hh546ncava34yrkvhz66") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-aul-1.1.3 (c (n "aul") (v "1.1.3") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "009ccnmqx377flfvw5pz5ncck0kyjfswmc4h6kxc2p4r1521c9pq") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-aul-1.1.4 (c (n "aul") (v "1.1.4") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "02qxaz6ri1dvlz2kzyf1l3bn54gmwkywhfwmhgrjk1ag1ls1mj2m") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-aul-1.1.5 (c (n "aul") (v "1.1.5") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "0rw19i0mghzhkgviffqjg922018mybrr0rfg013zwygac478z7y3") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-aul-1.2.0 (c (n "aul") (v "1.2.0") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "068z5hsdx0v22rixxqvqd7y99wnr1hxnbrrphmw11zdg0q1djxh6") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-aul-1.2.1 (c (n "aul") (v "1.2.1") (d (list (d (n "colored") (r "^2.0.4") (d #t) (k 0)))) (h "06fqm3bj1falnzcv2wsks5n2kvj5islp705p2xq1331cmrz61488") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-aul-1.3.0 (c (n "aul") (v "1.3.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "1r8hpa2wxgx7s8c2kq55kca3w5sfmi0cb2nzkwx2kxlv2jzvd04y") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-aul-1.3.1 (c (n "aul") (v "1.3.1") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "10wjw0wzixa814alygqh9z2df5xrxg106pc0g082302jzqbbjxrj") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-aul-1.3.2 (c (n "aul") (v "1.3.2") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)))) (h "0sswba7kmpxa4glvj9zfln1qgbsnnhc2s1g43fisk7fkbra3n428") (f (quote (("no-color" "colored/no-color"))))))

(define-public crate-aul-1.4.0 (c (n "aul") (v "1.4.0") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)))) (h "1bw789bjv2dsnkvxmvgkf3v9zkgcq5axfy115hdq7j59xra3vw06") (s 2) (e (quote (("color" "dep:colored"))))))

(define-public crate-aul-1.4.1 (c (n "aul") (v "1.4.1") (d (list (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)))) (h "0msr0xgs6fxpglrg817rcmk6ajjq96d1c6dc86lbygsi4m3dd59g") (s 2) (e (quote (("color" "dep:colored"))))))

