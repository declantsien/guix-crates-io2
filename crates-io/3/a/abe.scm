(define-module (crates-io #{3}# a abe) #:use-module (crates-io))

(define-public crate-abe-0.1.0 (c (n "abe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1p9d5a2pfj6i6zjvidx86gkf7qxdlvz2yhy0xrivprdhh94b8qq6")))

(define-public crate-abe-0.2.0 (c (n "abe") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1lvjfgj280gkqwg2x46x3nnl9gq34c9yp8kvc78ml6nah91z60kc")))

(define-public crate-abe-0.3.0-rc1 (c (n "abe") (v "0.3.0-rc1") (d (list (d (n "anyhow") (r "=1.0.71") (d #t) (k 0)) (d (n "arrayvec") (r "=0.7.2") (d #t) (k 0)) (d (n "base64") (r "=0.21") (d #t) (k 0)) (d (n "bstr") (r "^1.5.0") (k 0)) (d (n "hex") (r "=0.4.3") (d #t) (k 0)) (d (n "serde") (r "=1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "=1.0.40") (d #t) (k 0)))) (h "095bk5nv1gaafrs14x5iq6lbpldjg46vzzq0x9cpbgrs6f0mbp0d")))

