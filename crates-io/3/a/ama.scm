(define-module (crates-io #{3}# a ama) #:use-module (crates-io))

(define-public crate-ama-0.1.3 (c (n "ama") (v "0.1.3") (h "0r9fpsp3p0592497lh2764myrgd96g0wghkw0y6fnp71hyag6k79")))

(define-public crate-ama-0.1.4 (c (n "ama") (v "0.1.4") (h "1j34y609jhxp3a39cvqycim9hh26qn2rc6zkjrnz57m1mp19614m")))

