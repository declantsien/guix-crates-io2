(define-module (crates-io #{3}# a ahv) #:use-module (crates-io))

(define-public crate-ahv-0.1.0 (c (n "ahv") (v "0.1.0") (h "1zibdf6kk44bpr8jplh21nyahkh7s5mknicsvln7b7258w90vd0l")))

(define-public crate-ahv-0.1.1 (c (n "ahv") (v "0.1.1") (h "1ijlc0v0mws0ali6zx0xpr5ysw1xal445n6jkmqp17przm2ng9p4")))

(define-public crate-ahv-0.2.0 (c (n "ahv") (v "0.2.0") (h "1a7d418jkplz3c6nl6dwq5lkqr7jp61dwbp3gchr80nhcivxa048")))

(define-public crate-ahv-0.3.0 (c (n "ahv") (v "0.3.0") (h "0pq00376502fmrjcwvqknrsqsffzmp2inxsnjryvnnlznca6nsba") (f (quote (("max" "macos_13_0_0") ("macos_13_0_0" "macos_12_1_0") ("macos_12_1_0") ("default")))) (r "1.65")))

