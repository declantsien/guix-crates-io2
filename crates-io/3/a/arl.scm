(define-module (crates-io #{3}# a arl) #:use-module (crates-io))

(define-public crate-arl-0.1.0 (c (n "arl") (v "0.1.0") (d (list (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1yjk8wap5z5ihgjgigs8k2vnvdsl7xl3ia0988bliygzn2k5s0p8")))

(define-public crate-arl-0.2.0 (c (n "arl") (v "0.2.0") (d (list (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "sync" "time"))) (d #t) (k 0)))) (h "1xn9m4cphhvhwwx1i4yj92bykmrgp10zacr5yj1nw2aclkj5xd6a")))

