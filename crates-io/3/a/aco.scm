(define-module (crates-io #{3}# a aco) #:use-module (crates-io))

(define-public crate-aco-0.1.0 (c (n "aco") (v "0.1.0") (h "1pk23wd4j119qlxk0ha9zsp2v6dh791yqz4b8fzrxdss7dj489hi") (y #t)))

(define-public crate-aco-0.1.1 (c (n "aco") (v "0.1.1") (h "1lhkz0icwwgmpsmbb4q9hbs9h3snm2ylrkc9jvba1sxirnizxwnp") (y #t)))

