(define-module (crates-io #{3}# a act) #:use-module (crates-io))

(define-public crate-act-0.1.0 (c (n "act") (v "0.1.0") (d (list (d (n "ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "1m29j5xl4fm2bgvr6g8zm22rb4n1xfi8dkcmbd7sh2vlm9vknzz7")))

(define-public crate-act-0.1.1 (c (n "act") (v "0.1.1") (d (list (d (n "ansi-escapes") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)))) (h "0c46nfana9mxsy24zsvgyjh02b32kc895zw51drbg9lkf3hg5rng")))

