(define-module (crates-io #{3}# a ati) #:use-module (crates-io))

(define-public crate-ati-0.1.0 (c (n "ati") (v "0.1.0") (h "04gpjvva5dnzj2frgjxry4byi4c8q5ap10fbzsz3snikh0fzgrlf")))

(define-public crate-ati-0.2.0 (c (n "ati") (v "0.2.0") (h "098bsgxcg0gh3cqp6hlkdaga3rswdl9zlfic87khpv68jwv1g619")))

