(define-module (crates-io #{3}# a ang) #:use-module (crates-io))

(define-public crate-ang-0.1.0 (c (n "ang") (v "0.1.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "hamcrest") (r "^0.1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.21") (d #t) (k 2)))) (h "0mrdylirqdpyrf7xfxh7i130jmavwai6gkgknqzwhph0s9a7s6in")))

(define-public crate-ang-0.2.0 (c (n "ang") (v "0.2.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "hamcrest") (r "^0.1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.21") (d #t) (k 2)))) (h "0hwc2p56ysish92xxczq0vi0ay7ayz94qbcqyzg9ww64ansczapm")))

(define-public crate-ang-0.3.0 (c (n "ang") (v "0.3.0") (d (list (d (n "approx") (r "^0.3.1") (d #t) (k 0)) (d (n "hamcrest") (r "^0.1.5") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.1") (d #t) (k 2)))) (h "1kkwwsmxryqskk4z6zprc46aavkmcqs7cdl4zba02rq3qq8wxbav")))

(define-public crate-ang-0.4.0 (c (n "ang") (v "0.4.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "0hzv9b1y0fmkgrvqhgj92nmbkr05axhy99i1dbbik58hg08j9qvk")))

(define-public crate-ang-0.5.0 (c (n "ang") (v "0.5.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "hamcrest2") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "01lvcx3rawxy9abw31xh4w36f96hkx7vixlysb0jisv10gkwd8a7") (f (quote (("std" "approx/std" "num-traits/std") ("default" "std"))))))

(define-public crate-ang-0.6.0 (c (n "ang") (v "0.6.0") (d (list (d (n "approx") (r "^0.5") (k 0)) (d (n "hamcrest2") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1sn996x95ymd8jd29lss3mgpdx53484sbnid5nbdq929h4kvrwxj") (f (quote (("std" "approx/std" "num-traits/std") ("impl-serde" "serde" "serde/derive") ("default" "std"))))))

