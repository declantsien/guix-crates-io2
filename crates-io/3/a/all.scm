(define-module (crates-io #{3}# a all) #:use-module (crates-io))

(define-public crate-all-0.0.0 (c (n "all") (v "0.0.0") (h "1g1adz98qg5hjbm956nfq1mb60nwvyqr9qw4gs29zfk77f4vnb06")))

(define-public crate-all-0.0.1 (c (n "all") (v "0.0.1") (d (list (d (n "cargo_toml") (r "^0.20.2") (d #t) (k 1)) (d (n "crates-index") (r "^2.8.0") (f (quote ("git" "parallel" "git-performance"))) (d #t) (k 1)) (d (n "rayon") (r "^1.10.0") (d #t) (k 1)) (d (n "toml") (r "^0.8.12") (d #t) (k 1)))) (h "1qqrq7w2a68fhs6c4635cv3g5awr265sa7248s7cknc5qxvhavjm")))

