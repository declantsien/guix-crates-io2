(define-module (crates-io #{3}# a ape) #:use-module (crates-io))

(define-public crate-ape-0.1.0 (c (n "ape") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "091fmg0znwm2izvlkzzpnigxqf0vlc7gqpz9sga26gx3w7x6vhrj")))

(define-public crate-ape-0.1.1 (c (n "ape") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "14y5ry6ayhicljqx4anbnacq1bwx1zrmd0j8a2qjsrj9f9qd5lbi")))

(define-public crate-ape-0.1.2 (c (n "ape") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)))) (h "14an7j8ds4lzh753d49n6r1d65hdjx6f54cgb0064xwidviw46dl")))

(define-public crate-ape-0.2.0 (c (n "ape") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "0ad272pjm33mdsxhkkigy9vmb8vmfk1nmsp3cavs8ci6qlyyn3l9")))

(define-public crate-ape-0.3.0 (c (n "ape") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "1v8hn66kwslb1650y9aqsfqskly6piy8zshbq0c24ppg1jl6dn6f")))

(define-public crate-ape-0.4.0 (c (n "ape") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "0jhxl10k1k3ccxrp41wilyblxjaik4fpi71k5pydna8mwdghxc9q")))

(define-public crate-ape-0.5.0 (c (n "ape") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)))) (h "0qi2da3y1dchhqkd11r74cpmsljscgljzpcqz5kiz97kb14qdp0w")))

