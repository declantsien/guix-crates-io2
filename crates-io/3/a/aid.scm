(define-module (crates-io #{3}# a aid) #:use-module (crates-io))

(define-public crate-aid-0.1.0 (c (n "aid") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1vipfsqw6lax0arsbraw80clg2dhsmam5yfpvkjsiq553n0iigp0")))

(define-public crate-aid-0.1.1 (c (n "aid") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1xyqh51229l4r7qyjgslfcri89vfdfrips6ld89sabnpccq16agg")))

