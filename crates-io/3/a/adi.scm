(define-module (crates-io #{3}# a adi) #:use-module (crates-io))

(define-public crate-adi-0.1.0 (c (n "adi") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)))) (h "1g3nd49r1wgn1s4k50w4rg7yd6lfqk2ik972mw5pibycbzdy5jga")))

(define-public crate-adi-0.2.0 (c (n "adi") (v "0.2.0") (d (list (d (n "adi_screen") (r "^0.1.0") (d #t) (k 0)))) (h "1khh5nai9qzr71147lgzi4xq7h181n3vzhzc4992i17173sdi3h3") (f (quote (("validation") ("checks"))))))

(define-public crate-adi-0.3.0 (c (n "adi") (v "0.3.0") (d (list (d (n "adi_screen") (r "^0.2.0") (d #t) (k 0)))) (h "1x17bqjzd542yab566wps38cz5snxq5xpqgba4jajady6802zc41") (f (quote (("validation") ("checks"))))))

(define-public crate-adi-0.3.1 (c (n "adi") (v "0.3.1") (d (list (d (n "adi_screen") (r "^0.2.2") (d #t) (k 0)))) (h "1x876hndmvhdra2z4p17i061dgi7nsmg06a43kisxllh7xn1dg5v") (f (quote (("validation") ("checks"))))))

(define-public crate-adi-0.4.0 (c (n "adi") (v "0.4.0") (d (list (d (n "adi_screen") (r "^0.3.0") (d #t) (k 0)))) (h "0308js3lndmlsbk475vzrppcg6yfx3zkkkcl49drl4bzz66ig0cc") (f (quote (("validation") ("checks"))))))

(define-public crate-adi-0.5.0 (c (n "adi") (v "0.5.0") (d (list (d (n "adi_screen") (r "^0.4.0") (d #t) (k 0)))) (h "02hhx6ab61rx463m72f8agr5w4sddsn4asz9hicblr832cn0j1py") (f (quote (("validation") ("checks"))))))

(define-public crate-adi-0.6.0 (c (n "adi") (v "0.6.0") (d (list (d (n "adi_screen") (r "^0.5.0") (d #t) (k 0)))) (h "1w7s5r5d1yfb7bkzyngzmyhln3h19h90ar6yqdlxf4k730q5f5s8")))

(define-public crate-adi-0.7.0 (c (n "adi") (v "0.7.0") (d (list (d (n "adi_screen") (r "^0.8.0") (d #t) (k 0)))) (h "1fajcjirmvgzixlwjz1v0sqqxsixjabwryi8nb25ahsl5i7ng3q7")))

(define-public crate-adi-0.7.1 (c (n "adi") (v "0.7.1") (d (list (d (n "adi_screen") (r "^0.8.0") (d #t) (k 0)))) (h "06z4n4kgd629pydswpr78p0222r5wgnvcdxv8qy85k1bf78s3vf7")))

(define-public crate-adi-0.8.0 (c (n "adi") (v "0.8.0") (d (list (d (n "adi_screen") (r "^0.9") (d #t) (k 0)))) (h "1cqg3aw7a2yxh2zymp48ydba0vzsyjxa69vjr65f1nvmlz4bhqjp")))

(define-public crate-adi-0.9.0 (c (n "adi") (v "0.9.0") (d (list (d (n "adi_screen") (r "^0.10") (d #t) (k 0)))) (h "0pgq4bvikxv4zqbfi2mnin30fhza4l30fjasvxva9hwfp4r030wr")))

(define-public crate-adi-0.10.0 (c (n "adi") (v "0.10.0") (d (list (d (n "adi_screen") (r "^0.11") (d #t) (k 0)))) (h "11bx9nyk8nh6bhyh4cp8irq7c6vxiq3wd9k5rxzq1sjvyxnnhm2x")))

(define-public crate-adi-0.11.0 (c (n "adi") (v "0.11.0") (d (list (d (n "adi_screen") (r "^0.12") (o #t) (d #t) (k 0)))) (h "17ycisarnqfs09hm4z1gl1ggp4jzv38z2674w6jpnz621yp41saj") (f (quote (("screen" "adi_screen") ("default" "screen"))))))

(define-public crate-adi-0.11.1 (c (n "adi") (v "0.11.1") (d (list (d (n "adi_screen") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0aklkim3hazy40zbqj9mq8xjyfj95jvdm7qf821027h9yylwqcng") (f (quote (("screen" "adi_screen") ("default" "screen"))))))

(define-public crate-adi-0.12.0 (c (n "adi") (v "0.12.0") (d (list (d (n "adi_screen") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "adi_speaker") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0mxzjav5n5clqigw3hrg71mbr0nnngvaj1jx870gxl324sxmk4i0") (f (quote (("speaker" "adi_speaker") ("screen" "adi_screen") ("default" "screen" "speaker"))))))

(define-public crate-adi-0.13.0-pre0 (c (n "adi") (v "0.13.0-pre0") (d (list (d (n "afi") (r "^0.8.0-pre0") (o #t) (d #t) (k 0)) (d (n "ami") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "barg") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "dl_api") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rlajzkgbxlr1bxy4wav7ksgbbary4n3rlz7xanfc1zqryby81r6") (f (quote (("usb") ("speaker" "dl_api") ("sensor") ("screen" "hid" "ami" "afi" "dl_api" "barg") ("net") ("mic") ("hid") ("gps") ("gpio") ("drive") ("default" "screen" "speaker" "mic" "hid") ("checks") ("cam") ("bluetooth"))))))

(define-public crate-adi-0.13.0-pre1 (c (n "adi") (v "0.13.0-pre1") (d (list (d (n "afi") (r "^0.8.0-pre0") (o #t) (d #t) (k 0)) (d (n "ami") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "barg") (r "^0.0.2") (o #t) (d #t) (k 0)) (d (n "dl_api") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0masirg4pjq97y11rz7ld9c4hyk6afj7j5rswpxvsyrmjw3xgwxi") (f (quote (("usb") ("speaker" "dl_api") ("sensor") ("screen" "hid" "ami" "afi" "dl_api" "barg") ("net") ("mic") ("hid") ("gps") ("gpio") ("drive") ("default" "screen" "speaker" "mic" "hid") ("checks") ("cam") ("bluetooth"))))))

(define-public crate-adi-0.13.0-pre2 (c (n "adi") (v "0.13.0-pre2") (d (list (d (n "afi") (r "^0.8.0-pre0") (o #t) (d #t) (k 0)) (d (n "ami") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "barg") (r "^0.0.3") (o #t) (d #t) (k 0)) (d (n "dl_api") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0c3dfd4lhq2axhnxhsr53kpw9manvfw6rm55ylg4is9pab34xfhr") (f (quote (("usb") ("speaker" "dl_api") ("sensor") ("screen" "hid" "ami" "afi" "dl_api" "barg") ("net") ("mic") ("hid") ("gps") ("gpio") ("drive") ("default" "screen" "speaker" "mic" "hid") ("checks") ("cam") ("bluetooth"))))))

