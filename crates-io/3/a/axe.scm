(define-module (crates-io #{3}# a axe) #:use-module (crates-io))

(define-public crate-axe-0.1.0 (c (n "axe") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0f8af5rfyqlgs6p3ymk06yvdqi3jfvmpqqy0628r3bpn6f8vl8rf")))

