(define-module (crates-io #{3}# a ale) #:use-module (crates-io))

(define-public crate-ale-0.0.1 (c (n "ale") (v "0.0.1") (d (list (d (n "ale-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0i9mqmqhsfi9igy9dvd3r9a2slk3lmv2iknw94vs8jwag521njhz")))

(define-public crate-ale-0.1.0 (c (n "ale") (v "0.1.0") (d (list (d (n "ale-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "0jbf7943xmidgyb00l52xjik92r7vmhk3j403cpg3k0qn7sbn9v0")))

(define-public crate-ale-0.1.1 (c (n "ale") (v "0.1.1") (d (list (d (n "ale-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1hl5hc35ghvg78pxqqkb2da6l1nnz57vdmddhzyyqbr3nscr35h2")))

(define-public crate-ale-0.1.2 (c (n "ale") (v "0.1.2") (d (list (d (n "ale-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1f10xfd296vmfzmj82y5lrx7r1xlsq5iaa7kspaqv64wiw9526qx")))

(define-public crate-ale-0.1.3 (c (n "ale") (v "0.1.3") (d (list (d (n "ale-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "pixels") (r "^0.0.2") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "winit") (r "^0.21.0") (d #t) (k 2)) (d (n "winit_input_helper") (r "^0.5.0") (d #t) (k 2)))) (h "0r1s4mdl2ilf5dry2m0snw4xn7c6r5xvqfajvdxkld46mmhm1i4w")))

