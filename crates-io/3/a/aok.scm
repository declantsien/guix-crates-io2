(define-module (crates-io #{3}# a aok) #:use-module (crates-io))

(define-public crate-aok-0.1.2 (c (n "aok") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0hrqdibg3gvv8zqwva01pin4hv4qdik67svma4i9bgw0x4gfdxq7")))

(define-public crate-aok-0.1.3 (c (n "aok") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "16ldzshz090xmwf61sq9x47wjimsn05h4vwc140yf9264qaqisj0")))

(define-public crate-aok-0.1.4 (c (n "aok") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "00qjl8a80dzw30jmvbp4b8g6yvgvmrzx7xivlz7v37l90mnjwb6g")))

(define-public crate-aok-0.1.5 (c (n "aok") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "0f51i1z198mdgmxcln88pnw5s0xqzsnxrm2fw6zjd6gaaljkyipy")))

(define-public crate-aok-0.1.6 (c (n "aok") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "0rj5g5m2j1w0qlp0fx35hy8fdv8fxzrwhjzi351yr2p5fbik1yyn")))

(define-public crate-aok-0.1.7 (c (n "aok") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "1xllx3srlm1gjgi2rg7fdvmmkim7haz9kv9fyap08da5w7dy1lmg")))

(define-public crate-aok-0.1.8 (c (n "aok") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "0iny06h3gkkmv9qcc4f34yn3j7mxy9rjzsj9wgx3z96mdiqkzx25")))

(define-public crate-aok-0.1.9 (c (n "aok") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "02h6j8j6f2fjy9m0z7b2ln7nldx2hf5dnjsbnl6f911zqmpwcl8z")))

(define-public crate-aok-0.1.10 (c (n "aok") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "0a22n587nk97mjjvg8v9n29v1ml3vyw5sb7fa2s6yj9jqvz9zbfg")))

(define-public crate-aok-0.1.11 (c (n "aok") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)))) (h "0qgmayl20s3b13ggacnqkb7jjk2mjm2g8l8fa5ss86gf9rdcnsaf")))

