(define-module (crates-io #{3}# a abc) #:use-module (crates-io))

(define-public crate-abc-0.1.0 (c (n "abc") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1xhs59y7vdxp26ki6hyd0szzya5c080v693p157rb3r9s09cmdan")))

(define-public crate-abc-0.1.1 (c (n "abc") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1n1wxgdfh653s4bp1dpyhyrq6pnr68ybkwaraq5viq64hrdclk3j")))

(define-public crate-abc-0.1.2 (c (n "abc") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0159ncdqmap9cvdd2h7mcm1jk1dzv0dmr7ai4yjp3ksxk0104igs")))

(define-public crate-abc-0.2.0 (c (n "abc") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1102is5xzwqp1v9gp054xgaicgi4ax2qq9bn7lf6x5djjd0dwp45")))

(define-public crate-abc-0.2.1 (c (n "abc") (v "0.2.1") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1yr4lhh6yxw9ib1kgc1nrn7b0v0phfkf4vi0a4jymw5603ig0hsx")))

(define-public crate-abc-0.2.2 (c (n "abc") (v "0.2.2") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1sw33qmfrac5s0symi2il34lvzjnnmy0d9inrps4z1ccck8599gd")))

(define-public crate-abc-0.2.3 (c (n "abc") (v "0.2.3") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "19sq0k2v24irnh28v4yv6f70si614sn5z503q7v8l98wbkx0apsl")))

