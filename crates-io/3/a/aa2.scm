(define-module (crates-io #{3}# a aa2) #:use-module (crates-io))

(define-public crate-aa2-0.0.0 (c (n "aa2") (v "0.0.0") (h "0mnyhlv0b193jk3wckggyg1cv9rq1fk2cm65f60z8p5spv8dhqjx") (y #t)))

(define-public crate-aa2-0.0.1 (c (n "aa2") (v "0.0.1") (h "0xl3qprc29azgsf0rhb9165v9xggq2jmrkbdhs2j8hfjhk9s8dxa") (y #t)))

(define-public crate-aa2-0.9.0 (c (n "aa2") (v "0.9.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "0mi5vymlr58llr3q668rw7sigszqy29ql5mjlxmjdlsrr8v363zq") (y #t)))

(define-public crate-aa2-0.9.1 (c (n "aa2") (v "0.9.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "08dyiws9fy9lprgadx7azq5has7h8y45a3mrqc41hgly7jg29lqy") (y #t)))

