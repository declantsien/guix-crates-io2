(define-module (crates-io #{3}# a art) #:use-module (crates-io))

(define-public crate-art-0.0.1 (c (n "art") (v "0.0.1") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1ighg4zdbb62jkdlm1jhzcip0zisz6fql5z7v01jk52r5jvgkx8p") (f (quote (("lock_free_delays" "rand") ("default"))))))

(define-public crate-art-0.1.0 (c (n "art") (v "0.1.0") (h "02gcqx6kmyycwp63ykb72gmnrb5jb0g2x8bfds7nxbbm55pnnf65")))

(define-public crate-art-0.1.1 (c (n "art") (v "0.1.1") (h "11cm6akid9g0vybnxjvr7fvb705z5gv60a5p2v39lz33j5lvx62r")))

(define-public crate-art-0.1.2 (c (n "art") (v "0.1.2") (h "1wwdbailmxkh10cz2gb3xdgqwg2x6hbvm711cm7y5p473b593kvl")))

(define-public crate-art-0.1.3 (c (n "art") (v "0.1.3") (h "1jlhf4a1pv3rxbciaj9dixmr3qzkl4wnhr7jp8im5df2436a7275")))

