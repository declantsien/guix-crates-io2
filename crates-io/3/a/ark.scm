(define-module (crates-io #{3}# a ark) #:use-module (crates-io))

(define-public crate-ark-0.1.0 (c (n "ark") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-tls") (r "^0.1") (d #t) (k 0)))) (h "0iibs7sja7s8ckyxarvf0ikj659adqrcf6i5ai8lfb0fqbc7m4xj")))

(define-public crate-ark-0.1.1 (c (n "ark") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-tls") (r "^0.1") (d #t) (k 0)))) (h "0s7gkj06rypdf13jd6w2zh26gvnzj9bs9lhznaj4cxasn5az0xz1")))

