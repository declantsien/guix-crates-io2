(define-module (crates-io #{3}# a agi) #:use-module (crates-io))

(define-public crate-agi-0.0.1 (c (n "agi") (v "0.0.1") (h "0np2xn9j3p3gx1zmy6pdpz15wq1dlc6swbhgn9z7flnxq122n2b2") (y #t)))

(define-public crate-agi-0.0.2 (c (n "agi") (v "0.0.2") (h "1fz2ibynjbr1kn8qdh5fkqaiq3r5qfwnagmymcwla5zma02basxr") (y #t)))

(define-public crate-agi-0.0.3 (c (n "agi") (v "0.0.3") (h "1pawymzxvl3g35z0gvp625m3rhrla7pwbm7yw3jz9jif4k7hjzpc") (y #t)))

(define-public crate-agi-0.0.4 (c (n "agi") (v "0.0.4") (h "1144ffjccjhlk4am2ld7jpg7ymwx00v617c1g5ab7cdnqaggh8hf") (y #t)))

