(define-module (crates-io #{3}# a amy) #:use-module (crates-io))

(define-public crate-amy-0.1.0 (c (n "amy") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "1hrd21f9hadnrlsl3i4a5wr1zd0rrbmz8idfipx9j9x6xcm6nk9l")))

(define-public crate-amy-0.2.0 (c (n "amy") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "1vbxsls9y526qxgkp64k483dav45spxw58dlg03ajmp2xq6baqby")))

(define-public crate-amy-0.3.0 (c (n "amy") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "1qi7angs59dhi7i0bz4rjhxb6p4hk1agjpvmxz3zdkwpvk43xbpa")))

(define-public crate-amy-0.3.1 (c (n "amy") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "13vkdlphl00wgryciag4wvalv6k5s8cqha7sabb3b22h1m6y9vbk")))

(define-public crate-amy-0.3.2 (c (n "amy") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qviwwdmfkxlwlwm1m5gf56373c458gcmqwzch688nr904zahckx")))

(define-public crate-amy-0.4.0 (c (n "amy") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "1x0in1clb77yb9lzg35g62l0vrbd8dpialp9ypwc6m1v83g281m9")))

(define-public crate-amy-0.5.0 (c (n "amy") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "12rhf3mnk9xv42af82w3p81igbg9abczxxaf2idzq4f3h3i2zw5v")))

(define-public crate-amy-0.5.1 (c (n "amy") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "1ggwmn3h0vsrdppasq7lnq2474caw6332lcp2jl416hjiqa8mzwb")))

(define-public crate-amy-0.5.2 (c (n "amy") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "0qfdhrq12zwimm3nkf4rv04jmgmkfccgsv9dg0ncsj48vk66rhqk")))

(define-public crate-amy-0.5.3 (c (n "amy") (v "0.5.3") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "0lw8jvs6c5slsfq6bwcki8xywd43l9hmpqayq01nr1s8ya5nbchj")))

(define-public crate-amy-0.5.4 (c (n "amy") (v "0.5.4") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "1m7dicwi2da6z7wm1xpn09h8k9qp0pz5pirld93a7hfkf0ihx03q")))

(define-public crate-amy-0.5.5 (c (n "amy") (v "0.5.5") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "18fifzgvka9v4a7fgwx201rd5cidv6rcnlvx649bnyaz50pqlgs4")))

(define-public crate-amy-0.5.6 (c (n "amy") (v "0.5.6") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "0dmchhlgp9dchh2rr4qzq4n01y9517nb78cp1dm2wvcbaazi0dsc")))

(define-public crate-amy-0.5.7 (c (n "amy") (v "0.5.7") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "0pqnfg9p7c19l2axyn0a04j5jkbzd7z818sl017m28dm6r4irfl7")))

(define-public crate-amy-0.6.0 (c (n "amy") (v "0.6.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "1cdkm0kd6fn9c9s34nmzpf0z2nxg5nsxvnqjvl34hfw0ma4cv0l0")))

(define-public crate-amy-0.7.0 (c (n "amy") (v "0.7.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "11xzjan7ywq38kfri74k5mzbhzds8hn83389h7ip5d35x0m91ap7") (f (quote (("no_timerfd"))))))

(define-public crate-amy-0.7.1 (c (n "amy") (v "0.7.1") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "1kammb5hzhwfphf86z8n372hslcmi60i4zw8anlcdxhb9f1gv1qz") (f (quote (("no_timerfd"))))))

(define-public crate-amy-0.7.2 (c (n "amy") (v "0.7.2") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "1dhq8ss3d8kizz7dfg1rmfqj93xhvggfqsxd9ywicm747m0iaa6a") (f (quote (("no_timerfd"))))))

(define-public crate-amy-0.8.0 (c (n "amy") (v "0.8.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "0rd2abf221ymh74055zki2qbxwpla1rk87c6zm795pzkylz3pszn") (f (quote (("no_timerfd"))))))

(define-public crate-amy-0.8.1 (c (n "amy") (v "0.8.1") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "05qds17vw0p254977c4ng5v7f988y8vda8sf0kp4rwg9jrbp7s02") (f (quote (("no_timerfd"))))))

(define-public crate-amy-0.8.2 (c (n "amy") (v "0.8.2") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "0g050b317c0vrhhp8kb8df4yimamvh74jsfy6g2qmkgila2jxirc") (f (quote (("no_timerfd"))))))

(define-public crate-amy-0.9.0 (c (n "amy") (v "0.9.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "16idj2mgirp2pay12vcg93hm74j7cr8cqvv1g1xvqk132zfazl7k") (f (quote (("no_timerfd"))))))

(define-public crate-amy-0.9.1 (c (n "amy") (v "0.9.1") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.6") (f (quote ("eventfd"))) (d #t) (k 0)))) (h "1h4k4n9r2ccqzbr7lz9ylpfjinclrqfww3dg9sdv06zynzal4cxn") (f (quote (("no_timerfd"))))))

(define-public crate-amy-0.10.0 (c (n "amy") (v "0.10.0") (d (list (d (n "assert_matches") (r "^1.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.10") (d #t) (k 0)))) (h "1amjsjxc8yxwmsslfq2vg849z2walwljvzv9pdpk5fkrgfzz77hq") (f (quote (("no_timerfd"))))))

