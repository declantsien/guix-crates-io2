(define-module (crates-io #{3}# a aof) #:use-module (crates-io))

(define-public crate-aof-0.1.0 (c (n "aof") (v "0.1.0") (d (list (d (n "clap") (r "^2.10.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.73") (d #t) (k 0)) (d (n "resp") (r "^0.4.0") (d #t) (k 0)))) (h "1y76y4zi96k9iqbp9dqrcf5wbiar162ha65ssiz2gagm6wimsnfx") (f (quote (("unstable"))))))

(define-public crate-aof-0.1.1 (c (n "aof") (v "0.1.1") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "resp") (r "^0.4") (d #t) (k 0)))) (h "1a844md2lklpnp5dkir7zyd6ngigblnbw3drv7f0bgl7k336jnhr") (f (quote (("unstable"))))))

(define-public crate-aof-0.1.2 (c (n "aof") (v "0.1.2") (d (list (d (n "clap") (r "^2.20") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "resp") (r "^0.4") (d #t) (k 0)))) (h "0n3ix118m5shmds3p56k92a71ancyzg3x4dhj4hyr0s5zp1xrxy1") (f (quote (("unstable"))))))

(define-public crate-aof-0.2.0 (c (n "aof") (v "0.2.0") (d (list (d (n "clap") (r "^2.22") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "resp") (r "^1.0") (d #t) (k 0)))) (h "0i74xygjh2vcfbm2xf5w68khqpz2bfnzmdflgvwzhp8bp2zdr1fq") (f (quote (("unstable"))))))

(define-public crate-aof-0.2.1 (c (n "aof") (v "0.2.1") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "resp") (r "^1.0") (d #t) (k 0)))) (h "0vb6yz04vhzbi44bkzxxqwl300jrndaqcy20hlfh2ydznm236288") (f (quote (("unstable"))))))

(define-public crate-aof-0.3.0 (c (n "aof") (v "0.3.0") (d (list (d (n "clap") (r "^2.23") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "resp") (r "^1.0") (d #t) (k 0)))) (h "0qazx06jidba95710qpy9acyzxkshnp4jm4qf633lz7m5ckmq74a") (f (quote (("unstable") ("nightly" "regex/simd-accel"))))))

(define-public crate-aof-0.3.1 (c (n "aof") (v "0.3.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "resp") (r "^1.0") (d #t) (k 0)))) (h "0cz0k2b4p9hhpgapiq1zfi1qmw5w8zs0l0b09z2zpcgcaig9f8h3") (f (quote (("unstable") ("nightly" "regex/simd-accel"))))))

