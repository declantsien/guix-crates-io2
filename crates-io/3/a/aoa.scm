(define-module (crates-io #{3}# a aoa) #:use-module (crates-io))

(define-public crate-aoa-0.0.0 (c (n "aoa") (v "0.0.0") (h "1s3fn0dg36qjjlm82cryjz31r8i7ds4a76als4c9id5nzyzdv0r5") (y #t)))

(define-public crate-aoa-0.1.0 (c (n "aoa") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "rusb") (r "^0.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mvfkk52wkllhqhky2pldxgmvmsbbmhkfg5vh57050mzjq1fgd9s") (f (quote (("aoa2"))))))

