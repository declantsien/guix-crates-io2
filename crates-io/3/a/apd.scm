(define-module (crates-io #{3}# a apd) #:use-module (crates-io))

(define-public crate-apd-0.0.0 (c (n "apd") (v "0.0.0") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)))) (h "1l5p0msb1qac38nick3df44k6c0h68kbdd2rxl6sl06w1yfkp4gm")))

(define-public crate-apd-0.0.1 (c (n "apd") (v "0.0.1") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)))) (h "0xba1sdkmg39v9l3cq7a8gqq2k58qpcndyhsk9k0lqgyprgpfd6p")))

