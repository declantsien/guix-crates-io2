(define-module (crates-io #{3}# a auk) #:use-module (crates-io))

(define-public crate-auk-0.1.0 (c (n "auk") (v "0.1.0") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)))) (h "1h92aj53f7xvj9pzg398lb0appnh0cbn5mki0ncnalz4azr131kx")))

(define-public crate-auk-0.2.0 (c (n "auk") (v "0.2.0") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("yaml"))) (d #t) (k 2)))) (h "16293np64x3a2cdjim2gl27a2sn9y2xrydrxh6mplya9fisjfmlb")))

(define-public crate-auk-0.3.0 (c (n "auk") (v "0.3.0") (d (list (d (n "indexmap") (r "^2.1.0") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (f (quote ("yaml"))) (d #t) (k 2)))) (h "0rimspfdyknv3dl7il6k27yv3zpx2cx3y5bv4hnf9ss1ak3fic0v")))

