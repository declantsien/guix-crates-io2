(define-module (crates-io #{3}# a abv) #:use-module (crates-io))

(define-public crate-abv-0.1.0 (c (n "abv") (v "0.1.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yvzpwlxprzvz70n5yd2qi3xahjaqdnz46nsciz6fa98iak8ls77")))

(define-public crate-abv-0.2.0 (c (n "abv") (v "0.2.0") (d (list (d (n "phf") (r "^0.11") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10gpvs0vck5c9jqdcci9nki7gvlikkvy8aylwscbm84qk78h6b2s")))

(define-public crate-abv-0.2.1 (c (n "abv") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zj36cws7krwdi8inkqrvimc05pmc6p7pwkra4rpnzpf4c9i9fa2")))

