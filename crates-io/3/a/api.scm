(define-module (crates-io #{3}# a api) #:use-module (crates-io))

(define-public crate-api-0.1.0 (c (n "api") (v "0.1.0") (d (list (d (n "hyper") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1pv9llikc7194db1h8wxqzjl0pprsdp8f5rid30y8y0md5gyahxg") (f (quote (("use-hyper" "hyper") ("default"))))))

(define-public crate-api-0.2.0 (c (n "api") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0pb2hy1mxbl2yl00vyq4809pzi7sp16nnph2x708rzkfqc6vr488") (f (quote (("use-hyper" "hyper") ("default"))))))

