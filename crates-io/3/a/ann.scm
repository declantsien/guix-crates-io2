(define-module (crates-io #{3}# a ann) #:use-module (crates-io))

(define-public crate-ann-0.0.1 (c (n "ann") (v "0.0.1") (h "07spjbprksm53znc9hbd8fsygzvdm7405vwg76pwygy1ac0iqfdi")))

(define-public crate-ann-0.0.2 (c (n "ann") (v "0.0.2") (h "0b7xih2ybm37i82cs54q9myqfii26gh3yp6d741ssgcly8rax590")))

