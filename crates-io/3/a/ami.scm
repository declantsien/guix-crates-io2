(define-module (crates-io #{3}# a ami) #:use-module (crates-io))

(define-public crate-ami-0.1.0 (c (n "ami") (v "0.1.0") (h "0k5jchskcl3qpxrjp227pgmzng51a3la14gj92xawrwwv51qawzd")))

(define-public crate-ami-0.1.1 (c (n "ami") (v "0.1.1") (h "0xrqfz43x5lwz1gp27hv1xz8fb08ycy52hdfhwdam59k64005c5j")))

(define-public crate-ami-0.2.0 (c (n "ami") (v "0.2.0") (h "05ha29djbfjs91x5dy29p4fm4clswsbx0i0b028gvhy0xq6ylaan")))

(define-public crate-ami-0.2.1 (c (n "ami") (v "0.2.1") (h "136f6ail035d562ll6bc95n2d8q4gdxf7m22yk381559k599d3a3")))

(define-public crate-ami-0.3.0 (c (n "ami") (v "0.3.0") (h "1l0wbdy2zpi0q8vf9p2ws1jhv0nra320bqa9siwl3s2a6w7k4mhr")))

(define-public crate-ami-0.4.0 (c (n "ami") (v "0.4.0") (h "01ggdgl2p2whklap4s5q84crswy12k24ljny5lwmwwwpl8damna6")))

(define-public crate-ami-0.5.0 (c (n "ami") (v "0.5.0") (h "05n60dlpzw193mqimrak9xp338zmdh2pdl79xbxrw6zbgpfrppn8")))

(define-public crate-ami-0.6.0 (c (n "ami") (v "0.6.0") (h "06hsvvf1c5ninv8832rw8zcfp8153axqd1ljf9fqqi3in6aldh6q")))

(define-public crate-ami-0.7.0 (c (n "ami") (v "0.7.0") (h "00q467yz1zsr6gc9i7cx3biycrgrdzjy820sf5x215sdal967f3v")))

(define-public crate-ami-0.8.0 (c (n "ami") (v "0.8.0") (h "1qja5cw82q2ayxsmm1prh0bxv8src0v0kl14yknsqs99wh25gvhw")))

(define-public crate-ami-0.8.1 (c (n "ami") (v "0.8.1") (h "1llk4di39zhmcr3sdi0s7zw92n4gc6vhbfqmf941h09ficf78y0l")))

(define-public crate-ami-0.8.2 (c (n "ami") (v "0.8.2") (h "0q0zdycwx3lrl0jfhpiyy79yn24571fkkk0d215wdca0xj79010l")))

(define-public crate-ami-0.9.0 (c (n "ami") (v "0.9.0") (h "17ly0mn3hcrb0rschy7d67jizhwnff02l8d028fywz26pgbgh4dp")))

(define-public crate-ami-0.9.1 (c (n "ami") (v "0.9.1") (h "08f0amiazgjmiipy4s7089czwyv5x8wsf2zn6k5kmh1r59r8l97q")))

(define-public crate-ami-0.9.2 (c (n "ami") (v "0.9.2") (h "1zhpkjc82q0q3ly3v85ang0r6rkybrh3mrcwwjlxsi3gpkmd5n3w")))

(define-public crate-ami-0.10.0 (c (n "ami") (v "0.10.0") (d (list (d (n "euler") (r "^0.3") (d #t) (k 0)))) (h "19j861rpzzcqa5qnmgc1c1irlw8skps0jhi3gz0pshvr96zmhwhd")))

(define-public crate-ami-0.11.0 (c (n "ami") (v "0.11.0") (d (list (d (n "euler") (r "^0.4") (d #t) (k 0)))) (h "09x5llcpcjzqwn7ivh71ypnsmfwkjh7mjb5a3j0qfarcmh61jp8g")))

(define-public crate-ami-0.12.0 (c (n "ami") (v "0.12.0") (d (list (d (n "euler") (r "^0.4") (d #t) (k 0)))) (h "1vvcbm2zqb5wpn9ppafc80ybnjimm0lmwxziiavki3ffabnphkxz")))

(define-public crate-ami-0.13.0 (c (n "ami") (v "0.13.0") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)))) (h "0dldacx3hfhmacyis6c3n4rmci5952a34c4617bw64g88c1x29sl")))

(define-public crate-ami-0.13.1 (c (n "ami") (v "0.13.1") (d (list (d (n "cgmath") (r "^0.16") (d #t) (k 0)))) (h "0pdfjghdw7vz0vjlq9x1ylb8zw8fw38yip3n87mgqpsri10p1cgp")))

