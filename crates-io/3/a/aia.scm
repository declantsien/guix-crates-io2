(define-module (crates-io #{3}# a aia) #:use-module (crates-io))

(define-public crate-aia-0.0.0-alpha.0 (c (n "aia") (v "0.0.0-alpha.0") (h "0h5257kwnx8l6vaw7j4aygb0g059qa6cs858vd6hcdy5i3fqrarx")))

(define-public crate-aia-0.0.0-alpha.1 (c (n "aia") (v "0.0.0-alpha.1") (d (list (d (n "aoko") (r "^0.3.0-alpha.1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)))) (h "04c1r3yjw13nwzrkbd5hp0zl479cvpsiyx0hbs3i0r53kqf349zl")))

