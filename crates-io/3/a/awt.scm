(define-module (crates-io #{3}# a awt) #:use-module (crates-io))

(define-public crate-awt-0.1.0 (c (n "awt") (v "0.1.0") (d (list (d (n "clap") (r "^4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rayon") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0hffq2bjg8sijzfnlb6pd2wgl7j430j1qcpff89mlpci61ays9ly")))

