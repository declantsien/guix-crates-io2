(define-module (crates-io #{3}# a aow) #:use-module (crates-io))

(define-public crate-aow-0.1.0 (c (n "aow") (v "0.1.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "1fdbbqaawqldhr262ygw8yp1xx0wjlgblzvxj3177icj5328gg71")))

(define-public crate-aow-0.1.1 (c (n "aow") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "1k0l7z7cssr44hd9s7knmnk934n4578z2bz5aqxafg5jr88zc12j")))

(define-public crate-aow-0.1.4 (c (n "aow") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)) (d (n "wfd") (r "^0.1.7") (d #t) (t "cfg(windows)") (k 0)))) (h "08v3dfzfjndf0154zi21g8k82iqcayf9pjj70v5in7ggr24c6y4b")))

(define-public crate-aow-0.1.5 (c (n "aow") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "directories") (r "^3.0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)) (d (n "wfd") (r "^0.1.7") (d #t) (t "cfg(windows)") (k 0)))) (h "1lm1739z7pxl8w1ql4ic88khyy49m7vdhix2fpr7q4lyahpa9zdm")))

