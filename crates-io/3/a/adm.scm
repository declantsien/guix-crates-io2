(define-module (crates-io #{3}# a adm) #:use-module (crates-io))

(define-public crate-adm-0.1.0 (c (n "adm") (v "0.1.0") (d (list (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 0)) (d (n "lifxi") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.83") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.83") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)))) (h "1x21x4k3dl6g9vgkfd2x4mhjrn0g11k1b02g6l0fpr77dl82pxrv")))

