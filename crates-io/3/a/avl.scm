(define-module (crates-io #{3}# a avl) #:use-module (crates-io))

(define-public crate-avl-0.2.0 (c (n "avl") (v "0.2.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1s20lmksrfcp2wi8p3sq4dpkl2l30q1yf57an3mrnac21lq38qk3") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.2.1 (c (n "avl") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0yhvr6q5x45ggdvf8aga7r85b49cj11yk22ih9dwh7rcp37kqpdc") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.3.0 (c (n "avl") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "06277r0sbznh7bgnf4v9a2d6q7axdrsdz2wgx8xzb7dh4s4mq3zq") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.3.1 (c (n "avl") (v "0.3.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "118wk1aqxhvvhi7mcpxnrzcfrmjym025r7q42k0zfglxmix4ndck") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.3.2 (c (n "avl") (v "0.3.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1c2bdgjd1186qbd8ywqp9i64qyb70272v7xqn995bndzjgxcywdw") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.4.0 (c (n "avl") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0va7vsg36l8srh5s4favjymf4g3zkx2l76wm9vnv6cg3lqgqsh24") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.4.1 (c (n "avl") (v "0.4.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "08rl2n35045xdbmzgba3c1jw9z00v0b3j1fsszbfysp2clv6axb9") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.4.2 (c (n "avl") (v "0.4.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "03n9k2g0ng3wq9symb4l5mp362d485bvnwka615ph5blsq2hfcyj") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.4.3 (c (n "avl") (v "0.4.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1p0gs69q87admdzad3yivhkccqmkrmjbf5zlyvy1qsg618y7pgxk") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.5.0 (c (n "avl") (v "0.5.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "19rdnvrd5wfxsjfna4cv82305jiymxn86h7wkpl63fy9m94g6hj2") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.5.1 (c (n "avl") (v "0.5.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "16j9ra0kxfyzg8kh9q1wsknqhkzr5jb3ni5mibz2188amk61cjr2") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.5.2 (c (n "avl") (v "0.5.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0ia6psbf0ndz0m5r2lgnykq874j1hybwddi3a5azpy25g7grfvgc") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.5.3 (c (n "avl") (v "0.5.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "13cl0x9c1lqf4aqadajapwasfp07q4f6ivdczgw0cdr02cs9jdn4") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.6.0 (c (n "avl") (v "0.6.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "016s7vx6j0gylhbaaa2cq5rpv1w1k5j5s74wc4cpjvivbfh08id9") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.6.1 (c (n "avl") (v "0.6.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "15wvc81kq4vfaim0p1i3zxrxkn8hg7b35dssnbnx0b47z0nkqchh") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.6.2 (c (n "avl") (v "0.6.2") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0liawwj1w4rll11jgmzkdyq0w2cnvkk9lbc4zpzbb2500kvm8w5q") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.6.3 (c (n "avl") (v "0.6.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1bq74iyzmmg3bz0k7ki6syipvms21v2hra4qxqy2vr8avk7fjynd") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.7.0 (c (n "avl") (v "0.7.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18rjjsd6kfn37z4izvbfpbz1dkw6yc101pw3l8a4hd4kv1wc38vl") (f (quote (("consistency_check"))))))

(define-public crate-avl-0.7.1 (c (n "avl") (v "0.7.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "17ipyxijx7s7bd3n0z4f52zl535pkr93fcgj4hq72hzgzwzcnj8j") (f (quote (("consistency_check"))))))

