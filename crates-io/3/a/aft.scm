(define-module (crates-io #{3}# a aft) #:use-module (crates-io))

(define-public crate-aft-7.0.0 (c (n "aft") (v "7.0.0") (d (list (d (n "aft-crypto") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util" "net" "sync" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "079l8sndic7h13kv0dzs1wkx3fb0bqqsldxjcsm9zm3dz6b4p7wd")))

(define-public crate-aft-7.0.1 (c (n "aft") (v "7.0.1") (d (list (d (n "aft-crypto") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util" "net" "sync" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1qjbm0lral4z9xcbw1pz43mdqf89bg8h69h1yvf9mbciz386b9yw")))

(define-public crate-aft-7.0.2 (c (n "aft") (v "7.0.2") (d (list (d (n "aft-crypto") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpassword") (r "^7.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-std" "io-util" "net" "sync" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0qn196dsbns6h4l9d7m0k506gvnbgyymbmfvv6vwcpd7bz2283hh")))

