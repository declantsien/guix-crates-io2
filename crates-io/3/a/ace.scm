(define-module (crates-io #{3}# a ace) #:use-module (crates-io))

(define-public crate-ace-0.0.1 (c (n "ace") (v "0.0.1") (h "0f0py7xsljkq6ms8n5bvyk90g3yawx9xjaxd55zv9nd4nd2xq2yp") (y #t)))

(define-public crate-ace-0.0.2 (c (n "ace") (v "0.0.2") (h "0h51518kjxdjqvywzbq46ig5c9d21clzyp4bz1s4w35292ais0hy") (y #t)))

(define-public crate-ace-0.0.3 (c (n "ace") (v "0.0.3") (h "0s9hy5arp5ddkyfp8my8mnx5samypyaapn87zd28bbmv0cpk1mj2") (y #t)))

(define-public crate-ace-0.1.0 (c (n "ace") (v "0.1.0") (h "02hwsbsccq0ccss1rk9b79nqayx1s9mlq1jqjfp1qkzqgy35kkzg") (y #t)))

(define-public crate-ace-0.2.0 (c (n "ace") (v "0.2.0") (h "0jpscnf998qppgggrwz0j9xsihkynkcj255959k2jxdmrmqy5jbp") (y #t)))

