(define-module (crates-io #{3}# a afp) #:use-module (crates-io))

(define-public crate-afp-0.1.10 (c (n "afp") (v "0.1.10") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "systemstat") (r "^0.2.2") (d #t) (k 0)))) (h "1dw2w01nkji7029jbi2vhjqnvlbywmg1ngn1igx6f3fwd5l4qcyp") (r "1.65.0")))

