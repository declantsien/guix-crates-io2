(define-module (crates-io #{3}# a alg) #:use-module (crates-io))

(define-public crate-alg-0.1.0 (c (n "alg") (v "0.1.0") (h "1gb0lnhfl9xkpn6672an49ngarbdym1j04srw6n0j5pckl2aig3x")))

(define-public crate-alg-0.1.1 (c (n "alg") (v "0.1.1") (h "1fb376y5dgffx2w9na17gkscki917m9wgw53lbmphrcdi4pvsgn7")))

(define-public crate-alg-0.2.0 (c (n "alg") (v "0.2.0") (h "0jl6iwywza9lpb8sj2gvk8v2radmx30jssbxk5f5z13hxrsxdwi6")))

