(define-module (crates-io #{3}# a akc) #:use-module (crates-io))

(define-public crate-akc-0.1.0 (c (n "akc") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "191qdxm47lz384827r7qcqhxq8f7yjf296zbk7hq6rprmk4718gz")))

(define-public crate-akc-0.2.0 (c (n "akc") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ffqkxncngv2p3vdb8vnz5prcb99lm68gwl0nsvs2vp506sxyqza")))

