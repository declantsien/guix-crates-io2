(define-module (crates-io #{3}# a aud) #:use-module (crates-io))

(define-public crate-aud-0.1.0 (c (n "aud") (v "0.1.0") (h "1bsjrah4m9xl994hm90z15h2yaxj9lyh8npfkfww44d1sw935rx0")))

(define-public crate-aud-0.1.1 (c (n "aud") (v "0.1.1") (h "0s4361k5xkmbsfcf2q5l2c4famj1scsbpqlyc5h7b93ya2pycj5m")))

(define-public crate-aud-0.1.2 (c (n "aud") (v "0.1.2") (h "15p6bxj4nrin3fy4rqwkmmqj27drn9zr7vfxqh7d06zv3dw40h18")))

(define-public crate-aud-0.1.3 (c (n "aud") (v "0.1.3") (h "1mjfnlvcd4jlyk8zcc44v7150w51m9555h9vnwyd5yssdp1my9wv")))

