(define-module (crates-io #{3}# a aep) #:use-module (crates-io))

(define-public crate-AeP-0.1.0 (c (n "AeP") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "12j75i88rfhbghry9xcfz2kxgfn1a5rqlbp7w6gicrqn77y122cp")))

(define-public crate-AeP-0.1.1 (c (n "AeP") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0yjn60442sq37blxkacql2xjynfy1as0y5va6ln0b4x5xyf3n7dm")))

(define-public crate-AeP-0.1.2 (c (n "AeP") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "0s1iac7m263m6h5dsl59l7rm5sn0ajcsgzcvif61h1mcymjchgid")))

(define-public crate-AeP-0.1.3 (c (n "AeP") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "sha2") (r "^0.10.2") (d #t) (k 0)))) (h "1d9zrdgh5qnizz61plgh72ypva3m9p01mfyvc93p5w11q3ywg2mc")))

