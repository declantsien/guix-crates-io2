(define-module (crates-io #{3}# a akt) #:use-module (crates-io))

(define-public crate-akt-0.1.0 (c (n "akt") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time" "rt-multi-thread"))) (d #t) (k 2)))) (h "0kxjgfma6c8yqx2myammhq0l7q9ag27y6l06b8brg95443z70w4k")))

(define-public crate-akt-0.1.1 (c (n "akt") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "error-stack") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time" "rt-multi-thread"))) (d #t) (k 2)))) (h "09smgyzfcygd7hfdnddp1y3czjbfxkrd1znviijlmwg22d0ggga3") (s 2) (e (quote (("error-stack" "dep:error-stack"))))))

(define-public crate-akt-0.1.2 (c (n "akt") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "error-stack") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "rt" "macros" "time" "rt-multi-thread"))) (d #t) (k 2)))) (h "19zpm421v9bkb99a6kxr0hypmfyrl42a5zsxnqhb8cfpscca4wir") (r "1.56")))

