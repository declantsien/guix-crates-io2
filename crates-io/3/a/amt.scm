(define-module (crates-io #{3}# a amt) #:use-module (crates-io))

(define-public crate-amt-0.1.0 (c (n "amt") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "mailparse") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "083xy5dknf1wz27d3c1q274ggk5ap8pn8cjqp5f8aqqfnsq186r6")))

(define-public crate-amt-0.1.1 (c (n "amt") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "mailparse") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0x2s9gi2zf9zjll9fwi8kw4sk1mp4v8la8b5gd5czx1vfrq7rd8n")))

