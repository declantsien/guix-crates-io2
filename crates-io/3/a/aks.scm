(define-module (crates-io #{3}# a aks) #:use-module (crates-io))

(define-public crate-aks-0.1.0 (c (n "aks") (v "0.1.0") (h "0j5b4mnlycbr9gdyrv3n4drjr670a83kv4zsc2igflasgjwbxv9a")))

(define-public crate-aks-0.1.1 (c (n "aks") (v "0.1.1") (h "0gdxi7p5rxkf12fd007pl359pfgip34d7spfrkh0i29x78mg3c58")))

