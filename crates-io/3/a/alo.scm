(define-module (crates-io #{3}# a alo) #:use-module (crates-io))

(define-public crate-alo-0.1.0 (c (n "alo") (v "0.1.0") (h "1hcfihf3gva0i1g1inmgwqc67fkgjbcby13sqgla092i05xpcngi")))

(define-public crate-alo-0.2.1 (c (n "alo") (v "0.2.1") (h "103i553knyb6h4rczj044y8v03y9jj1wkn2fs35709bbnn5ql727")))

(define-public crate-alo-0.3.0 (c (n "alo") (v "0.3.0") (h "1ya9w4cfqbd7d564p1syddxz32czyaqg4g8c1c3v8xgfcdzw2j1k")))

