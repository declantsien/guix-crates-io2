(define-module (crates-io #{3}# a app) #:use-module (crates-io))

(define-public crate-app-0.2.0 (c (n "app") (v "0.2.0") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "0gnsj4pb5iaz927g5qxirr3xqcavahkxln599653f60z1kpi7bbw")))

(define-public crate-app-0.2.1 (c (n "app") (v "0.2.1") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "0xd2y2ywl2qkn8rvgyvad3n4pbqd3p537mrz40fjlp7v9zvi86na")))

(define-public crate-app-0.3.0 (c (n "app") (v "0.3.0") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "1jqyjwqhy1rq0h2nyi5mqgzm6vp0v3jv1nvdcy78vy3cxnzx9czn")))

(define-public crate-app-0.3.1 (c (n "app") (v "0.3.1") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "0jy673rvndbq1w8349p4ib69g67znkw5l66l1kif6mf7if4vp39l")))

(define-public crate-app-0.4.0 (c (n "app") (v "0.4.0") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "1zv39qlqvn8zpy3visl8pa4458bylzmz7zgkq2h4wj7fzlzxq2jp")))

(define-public crate-app-0.4.1 (c (n "app") (v "0.4.1") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "1yi9178jipr65ksil7589xcn5vvqx3vfn877wpj683pzs3g5w3yd")))

(define-public crate-app-0.4.2 (c (n "app") (v "0.4.2") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "0sgcq0mrw7khakqlbixsyg3ki7m6hm93ij6v01zrf1l8nvbwmwcd")))

(define-public crate-app-0.5.2 (c (n "app") (v "0.5.2") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)))) (h "0yxlachyqkz9iqa6399mfjrm01vqd5s9lbjb49j27n5hdmzc54yz")))

(define-public crate-app-0.5.3 (c (n "app") (v "0.5.3") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "14w3kgkck6x4bfq5gcm5rpk2mgg3w7p39q1r77yrs20xns049jcq")))

(define-public crate-app-0.5.4 (c (n "app") (v "0.5.4") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "1crm6lljq37ccrdhkgn5kxrwa2bxkhq5l9ji23a2rldncwvlfgic")))

(define-public crate-app-0.5.5 (c (n "app") (v "0.5.5") (d (list (d (n "stderr") (r "^0.7.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "00139cz9fg4q87ymv64v39abcmmjqgw245a4npxqb8mivz55fwdr")))

(define-public crate-app-0.5.6 (c (n "app") (v "0.5.6") (d (list (d (n "stderr") (r "^0.7.1") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "15s1qp7am8gb87z403044knzdr0c44ydsdv9dwkhnm2wvi3vmf72")))

(define-public crate-app-0.6.0 (c (n "app") (v "0.6.0") (d (list (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "stderr") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "0z0sdalvw83k2sryxq982swfa84lx6lfi1hvdaszvqk27hyya82h")))

(define-public crate-app-0.6.1 (c (n "app") (v "0.6.1") (d (list (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "stderr") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "15h92x90li1ami4aqsyxp858l7blnvzvk7wnd15krmr7q0scq9bz")))

(define-public crate-app-0.6.2 (c (n "app") (v "0.6.2") (d (list (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "stderr") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "1yfn06k3arlngxd3r91pkx73np3n19zzxrmh3y2ybx77j3inljqb")))

(define-public crate-app-0.6.3 (c (n "app") (v "0.6.3") (d (list (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "stderr") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "00q4vwg3pghxrkkqsj9c5kamcqkv1af9gily58rdqmv3gwdmjg3m")))

(define-public crate-app-0.6.4 (c (n "app") (v "0.6.4") (d (list (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "stderr") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "09hgdjf3kvzf92hbpipwxq36bdbc5rwcwh2ljnygpidwxpla28w5") (y #t)))

(define-public crate-app-0.6.5 (c (n "app") (v "0.6.5") (d (list (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)) (d (n "stderr") (r "^0.8.0") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "1iz3mlxa071qgjbsydhxwk6savvx1dd8pbjmf18pjiskmaw88x24")))

