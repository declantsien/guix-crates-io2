(define-module (crates-io #{3}# a acc) #:use-module (crates-io))

(define-public crate-acc-0.1.0 (c (n "acc") (v "0.1.0") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "num") (r "^0.1.39") (d #t) (k 0)))) (h "1nwiasgcr3cslfb6c33wr2qc0njdjhd7r322b7abily70l0060x2")))

(define-public crate-acc-0.1.1 (c (n "acc") (v "0.1.1") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "num") (r "^0.1.39") (d #t) (k 0)))) (h "0fxaawgbg1c30mfax2an0mrlc2csy44bqbjn80an2qk6xx82r78m")))

(define-public crate-acc-0.1.2 (c (n "acc") (v "0.1.2") (d (list (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "num") (r "^0.1.39") (d #t) (k 0)))) (h "15a32gmhl3yrzxvhdh565zn3x2lcf6lnr4mw8gs4dw2xmn4w6kbl")))

