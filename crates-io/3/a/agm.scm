(define-module (crates-io #{3}# a agm) #:use-module (crates-io))

(define-public crate-agm-0.1.0 (c (n "agm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15") (d #t) (k 0)) (d (n "openai-api-rs") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1x5g1b26xdxkkazfn9y38spj57mwag31q8ba6x716maicyahhjw4")))

