(define-module (crates-io #{3}# a atg) #:use-module (crates-io))

(define-public crate-atg-0.1.0 (c (n "atg") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)))) (h "17dvkpvyhq7vhpgzvz79bcfwbyvb4x93ddh28ji5abgdh7cqwsyc")))

(define-public crate-atg-0.2.0 (c (n "atg") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)))) (h "15bkiwgvw40shi612h4399m0jml5x0k4jfa4nh190s6nrkw7qx0q")))

(define-public crate-atg-0.3.0 (c (n "atg") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)))) (h "1rpmp1vmq51hs3709wzmjycjkhjncpyz449n0clq8cg3zdx5kx2k")))

(define-public crate-atg-0.4.0 (c (n "atg") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "15phl1sw4iapk2079kjs95609kjccx3aanvaviwl6rk17xbw3ran")))

(define-public crate-atg-0.5.0 (c (n "atg") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sr565a20qq1pg8vw6kcl3m2c16xcmb809p41dkgwhj1szch2p43")))

(define-public crate-atg-0.5.1 (c (n "atg") (v "0.5.1") (d (list (d (n "atglib") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1414bvra85kc5zb2w6whwrdmsya99qsq8rs29qkjk1vwapmb426n")))

(define-public crate-atg-0.6.0 (c (n "atg") (v "0.6.0") (d (list (d (n "atglib") (r "^0.1.3") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h9hln85b1ahrkrywzq6d0bcx2i1w2ssinpa2nl4j2vi7jh808h7")))

(define-public crate-atg-0.7.0 (c (n "atg") (v "0.7.0") (d (list (d (n "atglib") (r "^0.1.4") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0m7qp138y9p8hg8x3y1lg8mpjwk074b6al99cjd6lg4gbn29as4l")))

(define-public crate-atg-0.8.0 (c (n "atg") (v "0.8.0") (d (list (d (n "atglib") (r "^0.2.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "s3reader") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "17spk5sphi3l6jw8n7h4hiazqbjp6ha29p9zw6yx65kbz5sb0018")))

(define-public crate-atg-0.8.1 (c (n "atg") (v "0.8.1") (d (list (d (n "atglib") (r "^0.2.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "s3reader") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qxmzbc66kqpp94a3395zg93yvg7c9ca1llfv4gsc7cwxsrrivlj")))

(define-public crate-atg-0.8.2 (c (n "atg") (v "0.8.2") (d (list (d (n "atglib") (r "^0.2.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "s3reader") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gaqav51kmqkxrh3kdrapak91xym1h50mhrfcjby8k25dx4vxx60")))

(define-public crate-atg-0.8.3 (c (n "atg") (v "0.8.3") (d (list (d (n "atglib") (r "^0.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "s3reader") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.156, <1.0.172") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wa68p3g6lbq93h9kflsaq0wa6rcd7ypd7rxxhxmznrqkmv3rbq2")))

(define-public crate-atg-0.8.4 (c (n "atg") (v "0.8.4") (d (list (d (n "atglib") (r "^0.2") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "s3reader") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07vyar3z751229bm0v5hzsa7qfsl1d9pwp6zwb5g2p8gbg2cylzv")))

(define-public crate-atg-0.8.5 (c (n "atg") (v "0.8.5") (d (list (d (n "atglib") (r "^0.2") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "s3reader") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d12xcg5hiyqyfqij6hxqirfgh4s258n4cb2xp8zblz2wab2hr2q")))

(define-public crate-atg-0.8.6 (c (n "atg") (v "0.8.6") (d (list (d (n "atglib") (r "^0.2") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^4.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "loggerv") (r "^0.7") (d #t) (k 0)) (d (n "s3reader") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pzxr8j5v344sqvr2w8czjlw9yv1wqvqw0sa5b9dl6y82zkbc65d")))

