(define-module (crates-io #{3}# a amd) #:use-module (crates-io))

(define-public crate-amd-0.1.0 (c (n "amd") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0n74gj0ssyary4fw7l8qnn19l2yah89q5mgl520556qxmk1dyhbk") (f (quote (("debug4" "debug3") ("debug3" "debug2") ("debug2" "debug1") ("debug1"))))))

(define-public crate-amd-0.2.0 (c (n "amd") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1378m8hkk51pc1vkw97ll4ck64law1azy1zq22ladqbnc4wzld5j") (f (quote (("debug4" "debug3") ("debug3" "debug2") ("debug2" "debug1") ("debug1"))))))

(define-public crate-amd-0.2.1 (c (n "amd") (v "0.2.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1h7jwixbwdzrxi86spk4hi5s4svjh5686djlaggzflc2lysqdbvl") (f (quote (("debug4" "debug3") ("debug3" "debug2") ("debug2" "debug1") ("debug1"))))))

(define-public crate-amd-0.2.2 (c (n "amd") (v "0.2.2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "15ib7hhzdifb2m099561ip0fq627gaszw4sq36ys75snaw0y0yd6") (f (quote (("debug4" "debug3") ("debug3" "debug2") ("debug2" "debug1") ("debug1"))))))

