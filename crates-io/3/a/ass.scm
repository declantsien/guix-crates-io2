(define-module (crates-io #{3}# a ass) #:use-module (crates-io))

(define-public crate-ass-1.0.0 (c (n "ass") (v "1.0.0") (h "14qkq4agjhb1g56byngnw6097dnb2yv1fk5zak91km0vhvr03372") (y #t)))

(define-public crate-ass-1.1.0 (c (n "ass") (v "1.1.0") (h "0rfbkc4vjggsxib35iy2j9bapjkdqr1zs3bgxgwbd6qnh15kllzg") (y #t)))

(define-public crate-ass-1.1.1 (c (n "ass") (v "1.1.1") (h "1ynyn2y4bbmrgffkx3fbfczpqp9wdwrpabxq49vjwjkbljbs2qph")))

(define-public crate-ass-1.1.2 (c (n "ass") (v "1.1.2") (h "16blm7vf80zg0fxgmgia9jra6zzdm19h5pbixii2x45mgli9a1pc")))

