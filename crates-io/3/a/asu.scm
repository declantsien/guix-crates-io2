(define-module (crates-io #{3}# a asu) #:use-module (crates-io))

(define-public crate-asu-0.1.0 (c (n "asu") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "06z2r71p4f3ml832fsc85h6d0j7d7zmr9f69cs4b34s05dxl72m4") (y #t)))

(define-public crate-asu-0.1.1 (c (n "asu") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1vj79m3gzjg3f6d6hgsv6p4hwb4qvip283vrfsaf701iyfz0krz5") (y #t)))

(define-public crate-asu-0.1.2 (c (n "asu") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "0mgya96ghh5gqd5hccqxd2mr3vjk858kkj2z6i28q09aldcgp3ph") (y #t)))

(define-public crate-asu-0.1.3 (c (n "asu") (v "0.1.3") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "11vs18g7smqybycnxghn3fwfd1j7fvpaz08p26w08zgym4mw5yjl") (y #t)))

(define-public crate-asu-0.1.4 (c (n "asu") (v "0.1.4") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1133s6pinai15aybj957z4xr25dikd09z8d4a0sa2vk8g5nq21d7") (y #t)))

(define-public crate-asu-0.1.5 (c (n "asu") (v "0.1.5") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "0vbzq1mgvddxi8hvx84i600abqm9c4m7rnxs0399r69wzj2a2rp6") (y #t)))

(define-public crate-asu-0.1.6 (c (n "asu") (v "0.1.6") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "0f9sscy23f6mlyi4v5hd1gzcnxg7q7sryjdslfmab3wv4g2dv7fi") (y #t)))

(define-public crate-asu-0.1.7 (c (n "asu") (v "0.1.7") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "16b20afkb5nfvp2xshx0xghb0l9m5skzfy32yjkqq5zlwkn9q6cd") (y #t)))

(define-public crate-asu-0.1.8 (c (n "asu") (v "0.1.8") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1d4zxynpzwjzklxafx7544c6sf33sbpjc79p3pj0s0x2jb196ii1") (y #t)))

(define-public crate-asu-0.1.9 (c (n "asu") (v "0.1.9") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "17x957s17ilz2ya1s27n38k7dyypplvaqx33y29n2wbg4w37fc9h") (y #t)))

(define-public crate-asu-0.1.10 (c (n "asu") (v "0.1.10") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "03c0j570y2sj89cdw2pb7y8am2bmmhsvvibckf41n3wlc5rgij22") (y #t)))

(define-public crate-asu-0.1.11 (c (n "asu") (v "0.1.11") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "07584mc2vicjgjngicp89knmi2jk05nhvz7gb6nwcyjgz0yzkmj0") (y #t)))

(define-public crate-asu-0.1.12 (c (n "asu") (v "0.1.12") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "02h4j2mixldpaakhkmnzmihrb742jjrv3z4cylbpd3678s8l2d7c") (y #t)))

(define-public crate-asu-0.1.13 (c (n "asu") (v "0.1.13") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1vpywxb4373jwsg82cwykzllqpp01n604my4ixq1n7hl0qy0wgg4") (y #t)))

(define-public crate-asu-0.1.14 (c (n "asu") (v "0.1.14") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1ygg65zaljik8nfqx54vbivfdmw41vr9zmrxl0jhmddx1xw5xiis") (y #t)))

(define-public crate-asu-0.1.15 (c (n "asu") (v "0.1.15") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "10ggh0nqlckmrrlfhfpyxv3nibc36ffbsilpsxc3yzj2ic16jpfz") (y #t)))

(define-public crate-asu-0.1.16 (c (n "asu") (v "0.1.16") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "0aavl0qw8xhvay1k2j14p058gcizlqzqms99hysrii7c663xdbbd") (y #t)))

(define-public crate-asu-0.1.17 (c (n "asu") (v "0.1.17") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1ycw8zi9dxjx440japxsg1d507pms7506lbhabwh431vvllxd5ar") (y #t)))

(define-public crate-asu-0.1.18 (c (n "asu") (v "0.1.18") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1n8w0gpxcixklwf5l32kdfs3f196inadkbrssnia9m1lnjv3w3lf") (y #t)))

(define-public crate-asu-0.1.19 (c (n "asu") (v "0.1.19") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1g5kwyvmw5zwphkbfimqy85m3w0r8gcbnvmg2vm3zx7p7m3qk7nc") (y #t)))

(define-public crate-asu-0.1.20 (c (n "asu") (v "0.1.20") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "16s4lcp7xdzd84njcj8xs8j4jx4s2rwmr240djxqgjbs72v8736j") (y #t)))

(define-public crate-asu-0.1.21 (c (n "asu") (v "0.1.21") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "071wg29g6xmydp6f0wpybw06a25lbddfivclwa9g4i3dch1jaicc") (y #t)))

(define-public crate-asu-0.1.22 (c (n "asu") (v "0.1.22") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1c418a8fbvfwkjp38zi7igc1g2hzzckbzh350qlmyk2yjckpa1iv") (y #t)))

(define-public crate-asu-0.1.23 (c (n "asu") (v "0.1.23") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "0dhspnbgcqia505hbnjvicvfygdz561rm2pg58kijxicq78xjjq3") (y #t)))

(define-public crate-asu-0.1.24 (c (n "asu") (v "0.1.24") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "0xqlq91kwg4af75srbrk511ynw8f09kwn83awd008f4q66nxkr0r") (y #t)))

(define-public crate-asu-0.1.25 (c (n "asu") (v "0.1.25") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1dnxhqb4iva8jvdb05ikwrykh7hgdw4042qbmj9z6dk3nlskdxwj") (y #t)))

(define-public crate-asu-0.1.26 (c (n "asu") (v "0.1.26") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "15c84a2qfd5i4d9gahfz797yxi691paqhcmg9yqnhkma6gwi3dbg") (y #t)))

(define-public crate-asu-0.1.27 (c (n "asu") (v "0.1.27") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1gcm2z2qvvzzlp7nvbmhx8q95w6yfxr6dm9561wn9ghh3lbgd676") (y #t)))

(define-public crate-asu-0.1.28 (c (n "asu") (v "0.1.28") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "099yrh6ns500w1v41hdhj8aj347gw4aky7avvzal0g14ial9i6pr") (y #t)))

(define-public crate-asu-0.1.29 (c (n "asu") (v "0.1.29") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1mnb0izrld3fgssrzas6a1smqb3ryyfskkgsc8h5p3phlm0jg147") (y #t)))

(define-public crate-asu-0.1.30 (c (n "asu") (v "0.1.30") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1ybmg1avc84zar6vwk4imyawqxy5av5qjcv1127hpzv20jkzkjbp") (y #t)))

(define-public crate-asu-0.1.31 (c (n "asu") (v "0.1.31") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "0k4wnx5mqky66v0274h0qwripxlc0jy44bhgr2xsnkrjyhhy2abi") (y #t)))

(define-public crate-asu-0.1.33 (c (n "asu") (v "0.1.33") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "easy-http-request") (r "^0.2.13") (d #t) (k 0)) (d (n "toml_edit") (r "^0.2.1") (d #t) (k 0)) (d (n "ureq") (r "^2.1.1") (d #t) (k 0)) (d (n "useful_macro") (r "^0.2.5") (d #t) (k 0)))) (h "1xxm4gajgv1lr1kqmm67921pn909z7va7nhv0v0rrljgp46fpylv")))

