(define-module (crates-io #{3}# a aml) #:use-module (crates-io))

(define-public crate-aml-0.4.0 (c (n "aml") (v "0.4.0") (d (list (d (n "bit_field") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14jdsj426aksw7k5vrdvg4ricwj2cpn4cz3p20khr20p2229wrmp") (f (quote (("debug_parser_verbose") ("debug_parser"))))))

(define-public crate-aml-0.5.0 (c (n "aml") (v "0.5.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0nck6sxsz7348kyggl4qr8hp3vl9swc1x3qfbsp6m15vf12hcrz7") (f (quote (("debug_parser_verbose") ("debug_parser"))))))

(define-public crate-aml-0.6.0 (c (n "aml") (v "0.6.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0as72yx6m1h3v2fp4pcd9qmxrvdhhi1bc9dfpnc7wl51l7s2ldl5") (f (quote (("debug_parser_verbose") ("debug_parser"))))))

(define-public crate-aml-0.7.0 (c (n "aml") (v "0.7.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0aj6x93q98pcjj68zgl0svnk838w1005xk7iy9f76f1nlxsd6kq3") (f (quote (("debug_parser_verbose") ("debug_parser"))))))

(define-public crate-aml-0.8.0 (c (n "aml") (v "0.8.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ryx6ng6i7b0dq31ly0jh5x4jsglq2nrcngfr9c9xabkz808vzf8") (f (quote (("debug_parser_verbose") ("debug_parser"))))))

(define-public crate-aml-0.9.0 (c (n "aml") (v "0.9.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0dgmgksqrczlvy0cky5ac2xgx57lw7c8dw7x5vk69v3l2xm6yfdr")))

(define-public crate-aml-0.10.0 (c (n "aml") (v "0.10.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02jsjd8vmvxwqznncv56bvkn1p11krr6zynyzpkfy8mai4lb3ha5")))

(define-public crate-aml-0.11.0 (c (n "aml") (v "0.11.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0g86anwvqddsbaf4sf9vas86h52s3mdsa36n7rr5hxsy0k8wvzyl")))

(define-public crate-aml-0.12.0 (c (n "aml") (v "0.12.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "16hm2cflzngicgyl6j3sw8kg3jdjj6hi2pkl6nprk8pnz8khh2xh")))

(define-public crate-aml-0.13.0 (c (n "aml") (v "0.13.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "04cg0bzj02hfp0jr268g0in14hv6p8knzvz3z54fbiym8rqgrh5r")))

(define-public crate-aml-0.14.0 (c (n "aml") (v "0.14.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "18zyv1kr3z20wvy3yyi30gd7h9pqana2h9dl7zbab9jjicf52sdc")))

(define-public crate-aml-0.15.0 (c (n "aml") (v "0.15.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pwpjbcff8b3l5xkb8fiq5rj853c9j7w7njc6bfz4bh16glz4r62")))

(define-public crate-aml-0.16.0 (c (n "aml") (v "0.16.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitvec") (r "^0.22.3") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (d #t) (k 0)))) (h "1ipxbiakkqqmy7p77lff52mqq2bbvcq017ki8is2ka75192vkmp6")))

(define-public crate-aml-0.16.1 (c (n "aml") (v "0.16.1") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitvec") (r "^0.22.3") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (d #t) (k 0)))) (h "01b6w76gzzrzw33dry528w8skd03d6zs2kx3ihbm8gpv5v336hyx")))

(define-public crate-aml-0.16.2 (c (n "aml") (v "0.16.2") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (d #t) (k 0)))) (h "1bvc45vjzlmnfyp74i85jndb04yvqgs87wn0ava524jqwyp0lig7")))

(define-public crate-aml-0.16.3 (c (n "aml") (v "0.16.3") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (d #t) (k 0)))) (h "10yi2p58vs20mzv23y3cgx3glq32ly71vp2w8dz61mnqmzi95ksx")))

(define-public crate-aml-0.16.4 (c (n "aml") (v "0.16.4") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (f (quote ("alloc" "atomic"))) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "spinning_top") (r "^0.2.4") (d #t) (k 0)))) (h "05dljsxvshcs1mm95v54092ba67pyqli1a6xf5ba03i6sjkwpy64")))

