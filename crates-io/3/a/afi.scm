(define-module (crates-io #{3}# a afi) #:use-module (crates-io))

(define-public crate-afi-0.1.0 (c (n "afi") (v "0.1.0") (d (list (d (n "ami") (r "^0.5") (d #t) (k 0)))) (h "0nk9hjx74dwk3xma5majn2pc4dlm3bh3wz4yyzidxpq5wai23pcf")))

(define-public crate-afi-0.2.0 (c (n "afi") (v "0.2.0") (d (list (d (n "ami") (r "^0.5") (d #t) (k 0)))) (h "1rbpkdzm322jm9afphmyyd38jyndyiyy6vkaa6imc1imf2fglypw")))

(define-public crate-afi-0.3.0 (c (n "afi") (v "0.3.0") (d (list (d (n "ami") (r "^0.5") (d #t) (k 0)))) (h "1ld3cid1nc4m9vwjg0hfmrifksnbkppv6mis25wjkg3cy7r0fk4y")))

(define-public crate-afi-0.3.1 (c (n "afi") (v "0.3.1") (h "0243d4cmpz6qpg8fpwa2d2kff0mdzlr85sa7n0qmi4d4rl1846fi")))

(define-public crate-afi-0.3.2 (c (n "afi") (v "0.3.2") (h "0580gdkv4wf7paxdzz2jj4ary3qspz5znp3rapp34vhmpdykkacb")))

(define-public crate-afi-0.4.0 (c (n "afi") (v "0.4.0") (h "0m5b6nifxxcgc2al5s378r0j00n8pf7h393w5205pdkn55dh6k4j")))

(define-public crate-afi-0.5.0 (c (n "afi") (v "0.5.0") (h "0sj7280ngj43rqc842sspipd465k2xb6l68hkkv3sxymv92acm9g")))

(define-public crate-afi-0.7.0 (c (n "afi") (v "0.7.0") (h "1pc5pixmfvhh9hwyk092mz0wr1xlpyn099z2k69s2pb7140fg6lk")))

(define-public crate-afi-0.8.0-pre0 (c (n "afi") (v "0.8.0-pre0") (h "1gz02vwknriiy4qf8vvkam64agbxhxya4zddn7f4qyrcfijzfq5s")))

