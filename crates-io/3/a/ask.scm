(define-module (crates-io #{3}# a ask) #:use-module (crates-io))

(define-public crate-ask-0.0.1 (c (n "ask") (v "0.0.1") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0ffjdwzdxlbqibpmwwpygbs2qh41wj8zrdsx24zy6r9s6j7mry3y")))

(define-public crate-ask-0.0.2 (c (n "ask") (v "0.0.2") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0xzyvhs4ydk239lp6jcvc6klip4bw8h7ddcswaaw6qrbkbr2ig39")))

(define-public crate-ask-0.0.3 (c (n "ask") (v "0.0.3") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "18pzi5avai54308552vcbj1vilf3wrwfqls6l1i750ndpdlhnijb")))

(define-public crate-ask-0.0.4 (c (n "ask") (v "0.0.4") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1ila4aic3i4lywlxbrgj98hw9yzda3s6srvhpkm84r0jvcm2mcb1")))

(define-public crate-ask-0.0.5 (c (n "ask") (v "0.0.5") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0rpxj9mdsnkz1m0mi6shw273qm2bfgfh0jya1pv7p8lbfgb1p2s1")))

(define-public crate-ask-0.0.6 (c (n "ask") (v "0.0.6") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "0mryap10mi824qbyhgy8j60bpng6x3hnb0ihkmdpa11298d6nnl8")))

(define-public crate-ask-0.0.7 (c (n "ask") (v "0.0.7") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1pzdbada0xan540z7wjbq31qkl1wqkq0slb2303z33ajfr4hz797")))

(define-public crate-ask-0.0.8 (c (n "ask") (v "0.0.8") (d (list (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "19sy34ywfp4r30vh4idkslbzpys2vsbavannd3pr025baraka22x")))

(define-public crate-ask-0.0.9 (c (n "ask") (v "0.0.9") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "063jdwyg5cxsvslpj5504cdppyw40h9wgybjivx2d22alyy54jgw")))

(define-public crate-ask-0.0.10 (c (n "ask") (v "0.0.10") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1yr90cqjc7991h7hag5d62jbm4fphvcglbwl70fn35spxyp7h2pv")))

(define-public crate-ask-0.0.11 (c (n "ask") (v "0.0.11") (d (list (d (n "regex") (r "^1.0.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)))) (h "1ab1qrk5w5kqj4q3k7fzpn9bdv05xzrll5zbs7v5nd35mbqfxcy4")))

