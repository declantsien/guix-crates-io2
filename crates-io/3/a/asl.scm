(define-module (crates-io #{3}# a asl) #:use-module (crates-io))

(define-public crate-asl-0.1.0 (c (n "asl") (v "0.1.0") (h "1d33s2jf5hzfxk8ljvs5hj6arlns8yjr9b3xckd3midnp8qm80c4")))

(define-public crate-asl-0.1.1 (c (n "asl") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 2)) (d (n "map-macro") (r "^0.3.0") (d #t) (k 2)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "19477xvjq908j9vwvgxrk9cdcp4b4mblzlfka10gxx1xzwi6g9zw")))

