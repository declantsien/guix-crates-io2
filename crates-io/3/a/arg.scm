(define-module (crates-io #{3}# a arg) #:use-module (crates-io))

(define-public crate-arg-0.1.0 (c (n "arg") (v "0.1.0") (d (list (d (n "arg-derive") (r "^0.1") (d #t) (k 0)))) (h "1fjs5rjvi0fr6mnki08c3qjmqja05ddqghqpliaxamw5c42bdg7g")))

(define-public crate-arg-0.1.1 (c (n "arg") (v "0.1.1") (d (list (d (n "arg-derive") (r "^0.1") (d #t) (k 0)))) (h "0xri99m42pymv5zvnj225yhkyv1kih2fydbbc33yn8rk9kscwvqw")))

(define-public crate-arg-0.2.0 (c (n "arg") (v "0.2.0") (d (list (d (n "arg-derive") (r "^0.2") (d #t) (k 0)))) (h "18b3lrx92criin9hsinzxfp7l5sjxkflkfq3lrqdghxgv405a63x")))

(define-public crate-arg-0.3.0 (c (n "arg") (v "0.3.0") (d (list (d (n "arg-derive") (r "^0.3") (d #t) (k 0)))) (h "0yjyp86h0jhnmfdam3spdlqnbzadnm65fnf0r12jyxdbw0dgc8hp")))

(define-public crate-arg-0.3.1 (c (n "arg") (v "0.3.1") (d (list (d (n "arg-derive") (r "^0.3") (d #t) (k 0)))) (h "10dg1h3x3ig9s7k2624wkvfp2lplhj0jdgjh74cz5vswffxgqq5f") (f (quote (("std"))))))

(define-public crate-arg-0.4.0 (c (n "arg") (v "0.4.0") (d (list (d (n "arg-derive") (r "^0.4") (d #t) (k 0)))) (h "0akhjlpdvb2r66zfclivg9w9wjbf0j2f32qmw1vdxdq01mw6ad56") (f (quote (("std"))))))

(define-public crate-arg-0.4.1 (c (n "arg") (v "0.4.1") (d (list (d (n "arg-derive") (r "^0.4") (d #t) (k 0)))) (h "156kfmqfvfsns4c4hcgwg2fv4613blygyq457jsd8341p78bx2sn") (f (quote (("std"))))))

