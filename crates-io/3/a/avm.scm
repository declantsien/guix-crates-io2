(define-module (crates-io #{3}# a avm) #:use-module (crates-io))

(define-public crate-avm-0.5.1 (c (n "avm") (v "0.5.1") (d (list (d (n "hyper") (r "^0.6.10") (d #t) (k 0)) (d (n "os_type") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.2.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "15qimha700iggdc43miivr7y16zkgz3qns3hs35damarpyl7w9kx")))

(define-public crate-avm-0.6.0 (c (n "avm") (v "0.6.0") (d (list (d (n "hyper") (r "^0.6.10") (d #t) (k 0)) (d (n "os_type") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.2.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "1f7x7l7xi99zmxp8q0d9a2wdzfwr8z9vd95m9xv3saxch6zvq581")))

(define-public crate-avm-1.0.0 (c (n "avm") (v "1.0.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "hyper") (r "^0.6.10") (d #t) (k 0)) (d (n "os_type") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.2.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "0i9w7kfr7lnm1hndbznkyigjmpcj60m8y98aqs4lr6krys2y6pj7")))

(define-public crate-avm-1.0.1 (c (n "avm") (v "1.0.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "hyper") (r "^0.6.10") (d #t) (k 0)) (d (n "os_type") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.2.0") (d #t) (k 0)) (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "0w015jb15bchhlk7cgl8xjidacfzipg3g4ifwvds8qbasg216yrc")))

