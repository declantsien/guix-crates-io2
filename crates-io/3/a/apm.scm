(define-module (crates-io #{3}# a apm) #:use-module (crates-io))

(define-public crate-apm-1.0.0 (c (n "apm") (v "1.0.0") (d (list (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)))) (h "198sdiyqjrpli4wfbw7xlyc85di86c9q5j2jdks89nrw4kp043mq")))

(define-public crate-apm-1.0.1 (c (n "apm") (v "1.0.1") (d (list (d (n "libarc") (r "^0.1.0") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "09zhbdg55rvd8f7q2csaffq6s710mdch1yxl42s0dp1x21l8swd5")))

(define-public crate-apm-1.0.2 (c (n "apm") (v "1.0.2") (d (list (d (n "libarc") (r "^0.1.0") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.103") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.42") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.11") (d #t) (k 0)))) (h "0s7hk9x1yw1dwkkr9mjdj5lw1nw52f1my2rz36yz7gs57nj644r9")))

