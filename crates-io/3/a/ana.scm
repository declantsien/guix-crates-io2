(define-module (crates-io #{3}# a ana) #:use-module (crates-io))

(define-public crate-ana-0.9.3 (c (n "ana") (v "0.9.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.28") (d #t) (k 0)) (d (n "grpcio") (r "= 0.4.4") (f (quote ("protobuf-codec"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "liboj") (r "^0.2.3") (f (quote ("gcc" "gxx"))) (d #t) (k 0)) (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "protobuf") (r "^2.8.0") (d #t) (k 0)) (d (n "protoc-grpcio") (r "^1.0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "tempfile") (r "^3.0.8") (d #t) (k 0)))) (h "0grl77m5vn8930adkp6v71fmnpvpy7da11x0hlprxafh47xzqgga")))

