(define-module (crates-io #{3}# a asc) #:use-module (crates-io))

(define-public crate-asc-0.0.1 (c (n "asc") (v "0.0.1") (h "07y5y3ri3idp7x7xi7nk4r21c9glj5wga0qybj77kr9xdd6i7a6w") (y #t)))

(define-public crate-asc-0.1.0 (c (n "asc") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.137") (o #t) (k 0)))) (h "132w7ywirq18yf3kr79qk37zyqmgzv6rj579xm361r3hp0igv5js")))

(define-public crate-asc-0.1.1 (c (n "asc") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.137") (o #t) (k 0)))) (h "0m3cls0wkmp8rjz7vrjq9vhhw1waslxbi0adp96mqi2l2pyncggi")))

