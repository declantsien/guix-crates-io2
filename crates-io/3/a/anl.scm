(define-module (crates-io #{3}# a anl) #:use-module (crates-io))

(define-public crate-anl-0.1.0 (c (n "anl") (v "0.1.0") (d (list (d (n "image") (r "~0.10") (d #t) (k 2)) (d (n "lazy_static") (r "~0.2") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "0m9kb84n5v486lmwk9q6nfzk0pvvcvhrr5p2i7rvy5n3l3jxpy9v")))

(define-public crate-anl-0.2.0 (c (n "anl") (v "0.2.0") (d (list (d (n "image") (r "~0.10") (d #t) (k 2)) (d (n "lazy_static") (r "~0.2") (d #t) (k 0)) (d (n "num") (r "~0.1") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "1l5c0za36mv47dmn3q3wfgq6lb8f5yrz3528rm4x0k0l45xz2fbr")))

(define-public crate-anl-0.3.0 (c (n "anl") (v "0.3.0") (d (list (d (n "image") (r "~0.10") (d #t) (k 2)) (d (n "lazy_static") (r "~0.2") (d #t) (k 0)) (d (n "num") (r "~0.1") (d #t) (k 0)) (d (n "time") (r "~0.1") (d #t) (k 0)))) (h "1j27hqkc802fh34dzgx6ahgkhq3v6xa63xh5bbwbnmkk2h43j8wh")))

