(define-module (crates-io #{3}# a ase) #:use-module (crates-io))

(define-public crate-ase-0.1.0 (c (n "ase") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.1.1") (d #t) (k 0)))) (h "05wjipziq8895al9acwhmi4mrb1v0wddclv1qa69c36iynl5vfv7")))

(define-public crate-ase-0.1.1 (c (n "ase") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.1.1") (d #t) (k 0)))) (h "0zk7c92yimnqhcciksgdry2by06n2hwx115jx4r8kc82xps1k8hf")))

(define-public crate-ase-0.1.3 (c (n "ase") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "num_enum") (r "^0.1.1") (d #t) (k 0)))) (h "1i33hqfd3v5l4lahcbmh3s050kdhdddwracfwrl8vfz8875ixpjr")))

