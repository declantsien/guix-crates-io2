(define-module (crates-io #{3}# a ada) #:use-module (crates-io))

(define-public crate-ada-0.1.0 (c (n "ada") (v "0.1.0") (d (list (d (n "minifb") (r "^0.19.2") (d #t) (k 2)))) (h "19646dy15880x3xabxnl0qa2b9aqqyvi9kmjg9mmck0448xcfclr")))

(define-public crate-ada-0.2.0 (c (n "ada") (v "0.2.0") (d (list (d (n "minifb") (r "^0.19.2") (d #t) (k 2)))) (h "14yfqimzw5milxwh9dvyqssa8ywhbq90lgwf5iw932lcn3aiby2r")))

(define-public crate-ada-0.3.0 (c (n "ada") (v "0.3.0") (d (list (d (n "minifb") (r "^0.19.2") (d #t) (k 2)))) (h "07q0v4z0pwa952qaybqky4qf2yj1sfdf7279l131v90splfawlw5")))

