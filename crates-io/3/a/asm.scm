(define-module (crates-io #{3}# a asm) #:use-module (crates-io))

(define-public crate-asm-0.1.0 (c (n "asm") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)))) (h "0qcnqz79rgvk597q3b9cg4sw18fns0vnnwvpv5fa65rbv410vyxd") (f (quote (("encode") ("decode") ("6502"))))))

(define-public crate-asm-0.2.0 (c (n "asm") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1pmcdq1q1ykm7y1qda45jmmf8iwm1h9fl4pd51xa7b854mlii4w6") (f (quote (("encode") ("decode") ("6502")))) (r "1.56.0")))

(define-public crate-asm-0.3.0 (c (n "asm") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0fs5b7jl2js11b18l5gimqfxxjsrjhm5cvb80d6wr90l1sf9msjx") (f (quote (("6502")))) (r "1.56.0")))

