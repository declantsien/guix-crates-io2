(define-module (crates-io #{3}# a abs) #:use-module (crates-io))

(define-public crate-abs-0.1.0 (c (n "abs") (v "0.1.0") (h "1a364bpaisxw7279mrv5q4gqalqz9109i0mn2ilgwa65mqsq50zn")))

(define-public crate-abs-0.1.1 (c (n "abs") (v "0.1.1") (h "0i69pjydasfvl165xwxqwkf7qqx63y0l0jrrd74y6ixmjavlgiyk")))

