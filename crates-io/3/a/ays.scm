(define-module (crates-io #{3}# a ays) #:use-module (crates-io))

(define-public crate-ays-0.1.0 (c (n "ays") (v "0.1.0") (h "02xa2j5ivhli8pi7b84raxjfni4660va6p6vrbv74nn558rdnzjp")))

(define-public crate-ays-0.1.1 (c (n "ays") (v "0.1.1") (h "0yj2c8601f11aq5fhlp5nnds1b4g0824c5l0a41n8rn968cn7ajb")))

(define-public crate-ays-0.1.2 (c (n "ays") (v "0.1.2") (h "1x28m79klb0lzrwzjl0ynrjn45wzssc75szjqfiabgqbk3i1dzih")))

(define-public crate-ays-0.1.3 (c (n "ays") (v "0.1.3") (h "1ardlc9qhw011ry3z0726jigajhdzxz66bk0rjhvgjxfrwy1sj79")))

(define-public crate-ays-0.1.4 (c (n "ays") (v "0.1.4") (h "17c9c0h692rx2qpvslj868653mf5xfbrqpnf68bmhf1mn0n1vzyl")))

(define-public crate-ays-0.1.5 (c (n "ays") (v "0.1.5") (h "1zd2y493j0qkf6hl7rb7w987f8jn7vwn60k226fq3lqsz6yxsm30")))

(define-public crate-ays-0.1.6 (c (n "ays") (v "0.1.6") (h "12imbz6q64jlpzsznw2697lfwyj9ihwiwh4arprl5llmjzgzsrb5")))

