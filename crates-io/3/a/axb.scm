(define-module (crates-io #{3}# a axb) #:use-module (crates-io))

(define-public crate-axb-0.1.2 (c (n "axb") (v "0.1.2") (h "0sg9a91jzxzbm75gfsj3nc9vdkkgrzw32f0f8w7b029wnwkg4ffp")))

(define-public crate-axb-0.1.4 (c (n "axb") (v "0.1.4") (h "0r1xh68vkczhwfazcb6s77chyig6q8bipa5ncdm5c3jx9481gbjc")))

