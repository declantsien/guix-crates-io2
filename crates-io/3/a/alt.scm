(define-module (crates-io #{3}# a alt) #:use-module (crates-io))

(define-public crate-alt-0.1.0 (c (n "alt") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ron") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 0)) (d (n "tmq") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.21") (f (quote ("rt-threaded" "sync" "io-util" "macros" "tcp"))) (d #t) (k 0)) (d (n "tokio-i3ipc") (r "^0.8.0") (d #t) (k 0)))) (h "0dgndqjr3vfvzxz5qrkm0p1rixinbfxglac72ai3qpflpgjakdnj")))

