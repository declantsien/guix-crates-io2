(define-module (crates-io #{3}# a a10) #:use-module (crates-io))

(define-public crate-a10-0.0.0 (c (n "a10") (v "0.0.0") (h "1pynq6rpqyxlg32qz70lc8p5im9nymdqi2hm5nr3xvmvblsjkc1x")))

(define-public crate-a10-0.1.0 (c (n "a10") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.132") (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("kv_unstable_std"))) (k 0)) (d (n "std-logger") (r "^0.5.0") (k 2)))) (h "0hk09kcbb60dwwhm492jawdzmg5wrxfd7aa2vk2q1dl0nyn817mj") (f (quote (("nightly") ("default"))))))

(define-public crate-a10-0.1.1 (c (n "a10") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.132") (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("kv_unstable_std"))) (k 0)) (d (n "std-logger") (r "^0.5.0") (k 2)))) (h "1m7wxicp05pqnpg10iajfh93lf4lbkvx4bxq1cncliz1xn8v5vbp") (f (quote (("nightly") ("default"))))))

(define-public crate-a10-0.1.2 (c (n "a10") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.132") (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("kv_unstable_std"))) (k 0)) (d (n "std-logger") (r "^0.5.0") (k 2)))) (h "166k0svpcimv62cq5knmszwbra5rs1l7fv3p3xnn9jagzlfva1y3") (f (quote (("nightly") ("default"))))))

(define-public crate-a10-0.1.3 (c (n "a10") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.132") (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("kv_unstable_std"))) (k 0)) (d (n "std-logger") (r "^0.5.0") (k 2)))) (h "1kngp6jr508j7dr5wjs04c6zwxr3zlc2hk5pavmh3m705y8cdbvc") (f (quote (("nightly") ("default"))))))

(define-public crate-a10-0.1.4 (c (n "a10") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.132") (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("kv_unstable_std"))) (k 0)) (d (n "std-logger") (r "^0.5.0") (k 2)))) (h "1081rwi37ydj3z6q467m9f1ywp919m2bmc0g9qc9dvwd89zh0cgb") (f (quote (("nightly") ("default"))))))

(define-public crate-a10-0.1.5 (c (n "a10") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.132") (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("kv_unstable_std"))) (k 0)) (d (n "std-logger") (r "^0.5.0") (k 2)))) (h "0c3md068icj94im5vkpg40hrw9cwyjfw4fj2fab2lczpk2h1rbbp") (f (quote (("nightly") ("default"))))))

(define-public crate-a10-0.1.6 (c (n "a10") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.132") (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("kv_unstable_std"))) (k 0)) (d (n "std-logger") (r "^0.5.0") (k 2)))) (h "1khcdrk3phz9p38hr3bdln3ri3704vfqgxd141wp9n0jaj7gdqkh") (f (quote (("nightly") ("default"))))))

(define-public crate-a10-0.1.7 (c (n "a10") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.132") (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("kv_std"))) (k 0)) (d (n "std-logger") (r "^0.5.3") (k 2)))) (h "1blijvyipliaqr05gnlii1lmhmn8g7zqx3vgb6dn11gyil6hgfzh") (f (quote (("nightly") ("default"))))))

(define-public crate-a10-0.1.8 (c (n "a10") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.132") (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("kv_std"))) (k 0)) (d (n "std-logger") (r "^0.5.3") (k 2)))) (h "04b6m5fcwsinqdivsz9b28093b43vkl4gbvv1kydqnq3cnf2jgaz") (f (quote (("nightly") ("default"))))))

(define-public crate-a10-0.1.9 (c (n "a10") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.132") (k 0)) (d (n "log") (r "^0.4.21") (f (quote ("kv_std"))) (k 0)) (d (n "std-logger") (r "^0.5.3") (k 2)))) (h "19ciaxapk0csq579z4rhcpi8j83z5gziqlc8r9wczja804cma4vl") (f (quote (("nightly") ("default"))))))

