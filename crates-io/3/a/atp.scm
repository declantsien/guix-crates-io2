(define-module (crates-io #{3}# a atp) #:use-module (crates-io))

(define-public crate-atp-0.0.0 (c (n "atp") (v "0.0.0") (h "17qj29xrh8hl9mcabpdzpx5zyzkwzgn8h8wx0i4mxy859g9s40sr")))

(define-public crate-atp-0.0.1 (c (n "atp") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.17") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("rt-multi-thread" "macros" "fs"))) (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)))) (h "1vzcjdblxnpsssmivzlp71qnscnbd6gra1ldi3a3dgq06i642p7w")))

