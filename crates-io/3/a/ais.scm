(define-module (crates-io #{3}# a ais) #:use-module (crates-io))

(define-public crate-ais-0.1.0 (c (n "ais") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "07i4nh34n65aknzjpb01jy4avz1dcbxnh84i53c6xqmqfnfkwx76")))

(define-public crate-ais-0.1.1 (c (n "ais") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1a1a8afplpy4ngsxlr0m68ccw8mzqf4575a5g3jix3l964crmiiq")))

(define-public crate-ais-0.2.0 (c (n "ais") (v "0.2.0") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0j6k1qkza8qcwiwwi6kfg30x8c3nq1y0l93v9six1xiffai9w83c")))

(define-public crate-ais-0.3.0 (c (n "ais") (v "0.3.0") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "084vbkw9jcbdnq7r46n9nm26drw0gnax1jdfkrhflvmrjhml66sm")))

(define-public crate-ais-0.4.0 (c (n "ais") (v "0.4.0") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1lby62pjb8h6x3pfxz6k6n9dzhqdc7jx4cshys4sxn8271r0l088")))

(define-public crate-ais-0.5.0 (c (n "ais") (v "0.5.0") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0j97dbpvxqhq4i3c6hw86n2q844rwk0ds5j4k9q2kns4nqj7h8mk")))

(define-public crate-ais-0.6.0 (c (n "ais") (v "0.6.0") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1zir7y6mj7chxjhc0vg8f8c8gh0yip73706d41ifj32z19k1m4xz")))

(define-public crate-ais-0.7.0 (c (n "ais") (v "0.7.0") (d (list (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rihng7iif479lgychcjk0vj05vdw5cyysl1i58w5sd3pdvqmyxq")))

(define-public crate-ais-0.8.0 (c (n "ais") (v "0.8.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fbxvzhqanr4plbyjclbjq6ndb5jkpy3254qali72z8bd4xhcwmr")))

(define-public crate-ais-0.9.0 (c (n "ais") (v "0.9.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^7") (k 0)))) (h "04dk78vf0324h88lvyx8y2ly7gb0qnca7r8hx4n1hjk7142x0csf") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-ais-0.10.0 (c (n "ais") (v "0.10.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^7") (k 0)))) (h "1i2smy3b7g459kr8v7q9jgbad7vjw00xnygfnzk40wbpbmyhiw00") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-ais-0.11.0 (c (n "ais") (v "0.11.0") (d (list (d (n "heapless") (r "^0.7") (d #t) (k 0)) (d (n "nom") (r "^7") (k 0)))) (h "15v01lf3lh6hmzrpf2mg2i53g5iy517b9af09f0w1v1y2kj5y4yv") (f (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

