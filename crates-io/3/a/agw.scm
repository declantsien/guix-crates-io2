(define-module (crates-io #{3}# a agw) #:use-module (crates-io))

(define-public crate-agw-0.1.0 (c (n "agw") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)))) (h "12pmaaf9arffdp1f9y212h0rlwna9v398j4c6hdzram781bils8h")))

(define-public crate-agw-0.1.1 (c (n "agw") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 2)))) (h "1b9ms06f7vvvi6w2hbyw71g8naixjxzkjhgg7k1m1bim8gxi1za8")))

(define-public crate-agw-0.1.2 (c (n "agw") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 2)))) (h "130cdagpzwcgj9ggjvqxlnnwmvjjmfz51zc0fcgmjp462s1psqsb")))

(define-public crate-agw-0.1.3 (c (n "agw") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 2)))) (h "0mczymckpz3qhnbwa0bc4hsn7nvdwkka6w2fj3q08bn0bv0vlwax")))

(define-public crate-agw-0.1.4 (c (n "agw") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 2)))) (h "0dwnlq736qq4k209g9h1bzfkxr6xcsdwj5kylnm2q7k9v706xb26")))

(define-public crate-agw-0.1.5 (c (n "agw") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 2)))) (h "0phjrjdyv0aal7daxck3aiqnhlqhv18nx5yahxaf1qi5j2irjaxx")))

(define-public crate-agw-0.1.6 (c (n "agw") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 2)))) (h "02wcrjgdm8wxpxhrp3r9n13bi5shm61jdxiadyk74b0nsl9p7ixb")))

(define-public crate-agw-0.1.7 (c (n "agw") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "simple-logging") (r "^2.0.2") (d #t) (k 2)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 2)))) (h "0wkqxzaaw8j017kphgdrfcv4qg5bzarv4b86i04cb9aq2h829qly")))

(define-public crate-agw-0.1.8 (c (n "agw") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.71") (d #t) (k 2)) (d (n "chrono") (r "^0.4.37") (f (quote ("serde"))) (d #t) (k 2)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 2)))) (h "01a6klzgla87brfdpp88nk2xs18yaid5n87ymgg3dmf7b1y0lpss")))

(define-public crate-agw-0.1.9 (c (n "agw") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.71") (d #t) (k 2)) (d (n "chrono") (r "^0.4.37") (f (quote ("serde"))) (d #t) (k 2)) (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "cursive") (r "^0.20.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 2)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 2)) (d (n "stderrlog") (r "^0.6.0") (d #t) (k 2)))) (h "0d5arrvwy9yikjj2zifqzscqn9iwaa75kj9xr66yfp7vfwxn4cmg")))

