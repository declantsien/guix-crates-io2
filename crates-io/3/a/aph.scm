(define-module (crates-io #{3}# a aph) #:use-module (crates-io))

(define-public crate-aph-0.1.0 (c (n "aph") (v "0.1.0") (d (list (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "rug") (r "^1.15.0") (d #t) (k 0)))) (h "123n0gw4z0baqwwdpvh6z5v3mw00bs02lb58zjvgkyf08jdg4a85")))

(define-public crate-aph-0.2.0 (c (n "aph") (v "0.2.0") (d (list (d (n "rug") (r "^1.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1qp9rkwaf32wcbfp69pww40rprwdrn5slvcz1068qh7qxx848qkn")))

(define-public crate-aph-0.2.1 (c (n "aph") (v "0.2.1") (d (list (d (n "rug") (r "^1.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1s2ryvq15j6kc3lb49m57grrp1yvddfzyaam5gfs09y157vlw35w")))

(define-public crate-aph-0.2.2 (c (n "aph") (v "0.2.2") (d (list (d (n "rug") (r "^1.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1a08x6w230dflwffwx4n7f008n74yp19mm1j3hpir5k888dhs10y")))

