(define-module (crates-io #{3}# a aop) #:use-module (crates-io))

(define-public crate-aop-0.1.0 (c (n "aop") (v "0.1.0") (d (list (d (n "dashmap") (r "^5.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "syn") (r "^1.0.84") (f (quote ("full" "fold" "printing"))) (d #t) (k 0)))) (h "189m7bjmg8wddkw3ifaq2prc1sqma1jz1dg2vlbwfjik2358zv55")))

