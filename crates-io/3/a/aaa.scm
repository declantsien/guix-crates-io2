(define-module (crates-io #{3}# a aaa) #:use-module (crates-io))

(define-public crate-aaa-0.1.0 (c (n "aaa") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rs3a") (r "^1") (d #t) (k 0)))) (h "1c543sj2jnwq77j7m3lwxwi6ia3bn8sjikjyg4wzm234ljlwwl73")))

(define-public crate-aaa-1.0.0 (c (n "aaa") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rs3a") (r "^1") (d #t) (k 0)))) (h "0x5fr9643m2p5j94p5vgasmx041rc3hc800lxyg9k6j8fr88c5x6")))

(define-public crate-aaa-1.0.1 (c (n "aaa") (v "1.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rs3a") (r "^1") (d #t) (k 0)))) (h "1b56zaygzrg771p3jmg8dlfr54430dqfc0fzlkms90hd1lknsyki")))

(define-public crate-aaa-1.1.0 (c (n "aaa") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rs3a") (r "^1") (d #t) (k 0)))) (h "16nm86w29q43k6m5ca1s7wph8ydygc19dhyfjj6i7j09a9s2gbyk")))

(define-public crate-aaa-1.1.1 (c (n "aaa") (v "1.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.22") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "rs3a") (r "^1") (d #t) (k 0)))) (h "0xyhlnn35j8yi7qkl90yqsvi7awnfrcisyxc0sh376ql47y94j75")))

