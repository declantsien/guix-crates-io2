(define-module (crates-io #{3}# a avr) #:use-module (crates-io))

(define-public crate-avr-0.0.1 (c (n "avr") (v "0.0.1") (h "07yzacgmpawjg1xay3cy8ffjvw2ny8qm5fcrzcffqvacrv9jl8xm")))

(define-public crate-avr-0.0.2 (c (n "avr") (v "0.0.2") (h "0qfq7w17033j018d9blvq2z3mvmmyzw96dwr1s0a0gnlsaaa0ww8")))

(define-public crate-avr-0.0.3 (c (n "avr") (v "0.0.3") (h "0wq5il4awsd6rvhsyqkplqph6rd2pfmz727pkbn8z4z2ilw6r6rv")))

