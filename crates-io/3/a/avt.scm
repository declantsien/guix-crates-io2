(define-module (crates-io #{3}# a avt) #:use-module (crates-io))

(define-public crate-avt-0.9.0 (c (n "avt") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0g7b31zz6dk0c79v7c8ys26hhhdxjyd1x9pxbyi6bbn8l2sknv13")))

(define-public crate-avt-0.10.0 (c (n "avt") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hpjjnsr83pnvh9v91a3dlbq9242v3k6div5qsa7kr6c7qr0w2ww")))

(define-public crate-avt-0.10.1 (c (n "avt") (v "0.10.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "18wdvgzm41myzxi2f72sqb4dh4c8dn5apm73sgb4v8rr8w8dbavm")))

(define-public crate-avt-0.10.2 (c (n "avt") (v "0.10.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "1k4ilb53ynxg0dmk67zk73hrdswyn1pb3n4mda22020yzbk3qicg")))

(define-public crate-avt-0.10.3 (c (n "avt") (v "0.10.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rgb") (r "^0.8.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.11") (d #t) (k 0)))) (h "0z68snllkn24k0kklgzc1cmg3cpkfl03yzzkdnhpwh23y7gyx78q")))

