(define-module (crates-io #{3}# a aap) #:use-module (crates-io))

(define-public crate-aap-0.2.1 (c (n "aap") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fakeit") (r "^1.1.1") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("multipart" "blocking"))) (d #t) (k 0)) (d (n "spinners") (r "^1.2.0") (d #t) (k 0)))) (h "1xjvdgvg06vllah3f0zifygj40lkxcml6hl88hqrhrf690qir0a3")))

(define-public crate-aap-0.3.0 (c (n "aap") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fakeit") (r "^1.1.1") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("multipart" "blocking"))) (d #t) (k 0)) (d (n "spinners") (r "^1.2.0") (d #t) (k 0)))) (h "1wicqkbmpv5qc4i5hx064k88rm5bay3v6kp4kzal41ncjxwwmlff")))

(define-public crate-aap-0.3.1 (c (n "aap") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fakeit") (r "^1.1.1") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("multipart" "blocking"))) (d #t) (k 0)) (d (n "spinners") (r "^1.2.0") (d #t) (k 0)))) (h "03xy8cv34gqrkaqv6xkh1n9lv7zafi226a76pd1yn09l1zz7j6p6")))

(define-public crate-aap-0.3.2 (c (n "aap") (v "0.3.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fakeit") (r "^1.1.1") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("multipart" "blocking" "cookies"))) (d #t) (k 0)) (d (n "spinners") (r "^1.2.0") (d #t) (k 0)))) (h "199vakk0fwmc96a8aiw49jpx2xfgssxrkdwjvw9ypv6s3ys5ydlb")))

(define-public crate-aap-0.4.0 (c (n "aap") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fakeit") (r "^1.1.1") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart" "blocking" "cookies" "rustls-tls"))) (d #t) (k 0)) (d (n "spinners") (r "^1.2.0") (d #t) (k 0)))) (h "1wfqgy81vcras8ziq8mghqcj4fqa395kqx0cgrb3ki00sj7a7nl6")))

(define-public crate-aap-0.4.1 (c (n "aap") (v "0.4.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "fakeit") (r "^1.1.1") (d #t) (k 0)) (d (n "luhn") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.20") (f (quote ("multipart" "blocking" "cookies" "rustls-tls"))) (d #t) (k 0)) (d (n "spinners") (r "^1.2.0") (d #t) (k 0)))) (h "1s5lbqfxjqvnvfasf1qsb5gz66f214rvjjfnwi1mmv5qxwwr6nia")))

