(define-module (crates-io #{3}# a afk) #:use-module (crates-io))

(define-public crate-afk-0.1.0 (c (n "afk") (v "0.1.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0plwy8a63g92clnrdwc0s9jcc366q581xvrhziygpgvhx6svn6g2")))

(define-public crate-afk-0.1.1 (c (n "afk") (v "0.1.1") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1597j2jhw25bc2pdymp3jhisys0r6hvqxxn3nlwb1i1n301xq0ls")))

(define-public crate-afk-0.1.2 (c (n "afk") (v "0.1.2") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1lfvj542299lnzj8p65bi8z3gzkx09vi9lfjkp8p12lscysld2k4")))

(define-public crate-afk-0.1.3 (c (n "afk") (v "0.1.3") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1i2kjsqhxg4xfd8hb8x41qw7xs6nvbg27gqy0caj2i9x39rn1p21")))

(define-public crate-afk-0.1.4 (c (n "afk") (v "0.1.4") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0pw549nb6d1ida8rg35ds44k343w9g8wf94phw2z42rmfdbfcszy")))

(define-public crate-afk-0.1.5 (c (n "afk") (v "0.1.5") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1ba72rcwymi14rifqaqpsv01gsn3imnpdpaq8kb44j2x6r0r4ays")))

(define-public crate-afk-0.1.6 (c (n "afk") (v "0.1.6") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "01mz6b5953rpcw9nv57cild385y8d2xqlq8ai4w78gdxz1br7yd0")))

(define-public crate-afk-0.1.7 (c (n "afk") (v "0.1.7") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1vvpwlhw58rml3lwmiylznidwlnmd2603vzjmn3jxa7sbbyq088v")))

(define-public crate-afk-0.1.8 (c (n "afk") (v "0.1.8") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "15sf3znp4242g6rxdl0zy3blv2p3ww21plrhv6qc7dzfkddy79br")))

(define-public crate-afk-0.1.9 (c (n "afk") (v "0.1.9") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "0jnp0ljw01vbwbdpxdig13rxwpl0bl8gp9ilx7x97s9lia3dwfxj")))

(define-public crate-afk-0.1.10 (c (n "afk") (v "0.1.10") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "1kj0b0szqg6y0zd96czd6r7fz6hrb86fb1mvihc19h5w84vnsfwr")))

(define-public crate-afk-0.1.11 (c (n "afk") (v "0.1.11") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "02wapqm977hg4l5rvlkjildy3pv94yf149p4l29rdksk2vmmhc4y")))

