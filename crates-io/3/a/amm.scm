(define-module (crates-io #{3}# a amm) #:use-module (crates-io))

(define-public crate-amm-0.1.0 (c (n "amm") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.17.0") (d #t) (k 0)) (d (n "integer-sqrt") (r "^0.1.5") (d #t) (k 0)) (d (n "uint") (r "^0.9") (d #t) (k 0)))) (h "133c5xjl7l3hwlk5pljvhpl5m9cnvs5zd8jsh4x0cxafk2lcraq7") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint")))) (y #t)))

