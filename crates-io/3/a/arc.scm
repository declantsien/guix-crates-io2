(define-module (crates-io #{3}# a arc) #:use-module (crates-io))

(define-public crate-arc-0.0.1 (c (n "arc") (v "0.0.1") (d (list (d (n "cocoa") (r "^0.15") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objc") (r "^0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "palette") (r "^0.3") (d #t) (k 0)))) (h "1zkhyjqb39aiz67n4wmb4kdvdcl72g37h9vlfsk3ypgza22m4hs5")))

