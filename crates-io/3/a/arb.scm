(define-module (crates-io #{3}# a arb) #:use-module (crates-io))

(define-public crate-arb-0.1.0 (c (n "arb") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "arb-lib") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "wrap_help" "env"))) (d #t) (k 0)) (d (n "csv") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "059jbwxicarh009jfh4nnpccw8wpw1fmn48l88sfpa2yg8d9zl5l")))

