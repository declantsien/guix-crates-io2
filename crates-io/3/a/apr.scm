(define-module (crates-io #{3}# a apr) #:use-module (crates-io))

(define-public crate-apr-0.0.2 (c (n "apr") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)))) (h "123sj6fy8dg6qif3b6yk2zy9prwixckipzjn1rd4vdh94jwqvd1i")))

(define-public crate-apr-0.1.0 (c (n "apr") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "0y89yw96k5ni25g9y9ljbzm5axvzsp6pw26smw6p7w7g77dvr0wf") (s 2) (e (quote (("url" "dep:url"))))))

(define-public crate-apr-0.1.1 (c (n "apr") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "0vcfg47bl9qlp9ca393ld78k3qckymvxgig83wi12cy32yd02gq1") (s 2) (e (quote (("url" "dep:url"))))))

(define-public crate-apr-0.1.2 (c (n "apr") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "1v9yhhmd2rjm8mqlgl7dsx121rrygzvl31w5n5isnpmw4azvkj36") (s 2) (e (quote (("url" "dep:url"))))))

(define-public crate-apr-0.1.3 (c (n "apr") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "1h1w1dnpwhzibgssr6awfwdh1srnnldwaq8k3njajwa2wh8ia8l2") (s 2) (e (quote (("url" "dep:url"))))))

(define-public crate-apr-0.1.4 (c (n "apr") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "17i570i7m5wahw5xl5bdwvzfs1mgz97p4xpiy910xs18syn3vj98") (s 2) (e (quote (("url" "dep:url"))))))

(define-public crate-apr-0.1.5 (c (n "apr") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "1m044kc23qrb683hk9sr3fcyh0xgcllqxwhsd8z12dk5g4y8vryb") (s 2) (e (quote (("url" "dep:url"))))))

(define-public crate-apr-0.1.6 (c (n "apr") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "0skyzmqz64pl7rag84a6k84yg7bcxvnrb6r7d07ihxmzn0n67rrh") (s 2) (e (quote (("url" "dep:url"))))))

(define-public crate-apr-0.1.7 (c (n "apr") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "0xid2ca3ivnb6yr4s2v4qvcpdp0py1haci6chqgc6krjwf25wzl1") (s 2) (e (quote (("url" "dep:url"))))))

(define-public crate-apr-0.1.8 (c (n "apr") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "ctor") (r "^0.2.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6.2") (d #t) (k 1)) (d (n "url") (r "^2.4.1") (o #t) (d #t) (k 0)))) (h "0a2rsqnn45g699gr95jypdyqzwcchdg3lldadfkwa5gyzyyiq2zq") (s 2) (e (quote (("url" "dep:url"))))))

(define-public crate-apr-0.1.9 (c (n "apr") (v "0.1.9") (d (list (d (n "bindgen") (r ">=0.60") (d #t) (k 1)) (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "1shhh8l48rn9s03msis2ds4i6830pra3iqga4lmgb3l2rp0dgild") (s 2) (e (quote (("url" "dep:url"))))))

(define-public crate-apr-0.1.10 (c (n "apr") (v "0.1.10") (d (list (d (n "bindgen") (r ">=0.60") (d #t) (k 1)) (d (n "ctor") (r "^0.2") (d #t) (k 0)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "url") (r "^2") (o #t) (d #t) (k 0)))) (h "1qrjg9kwzlqywi0ann2j1s4bnlgrm69362r0fsyg2i27fk50p1i1") (s 2) (e (quote (("url" "dep:url"))))))

