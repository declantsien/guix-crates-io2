(define-module (crates-io #{3}# a a2d) #:use-module (crates-io))

(define-public crate-a2d-0.1.0 (c (n "a2d") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "1l723ii4ykwmjcy4hw1v48lbnhg91y3r3psr251qa30k5a0vyzxq")))

(define-public crate-a2d-0.1.1 (c (n "a2d") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "14kzznk3zk9v15d4vmd2708jrxhk55wq6m3capi5ipyky0m8kr1m")))

(define-public crate-a2d-0.1.2 (c (n "a2d") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "0c6zp3zqx9r9br7l7zfi1wvzyidfrl17bialj5wmdknzrc3mj4jf")))

(define-public crate-a2d-0.1.3 (c (n "a2d") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "11mkvji3dpsk1cf5f58hch0h798b3z8azd5pfrfycwi4b7v43if1")))

(define-public crate-a2d-0.1.4 (c (n "a2d") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "17hv76i6b57i6kvk3f0qy4swcj3zxjvcfa5lgd5l0lbg69msbxwm")))

(define-public crate-a2d-0.1.5 (c (n "a2d") (v "0.1.5") (d (list (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "0xg8c7z6g9jr6qi8q4h7fwxp99jm1hqj84xbkg7bxfkr43lvvpm4")))

(define-public crate-a2d-0.1.6 (c (n "a2d") (v "0.1.6") (d (list (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "11w3sgm8frd1j6l538xps4da3sgl5q1jvfzksf22a8fyqhdra0nk")))

(define-public crate-a2d-0.1.7 (c (n "a2d") (v "0.1.7") (d (list (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "1bws5pc6mscs10qin447hbmj2550xc5680b8vid9z81gzm46m9pf")))

(define-public crate-a2d-0.1.8 (c (n "a2d") (v "0.1.8") (d (list (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "142ihpxfzblcc6ha8ppb1h92k80vmdab3ddffbfz7mqdca2hc8a8")))

(define-public crate-a2d-0.1.9 (c (n "a2d") (v "0.1.9") (d (list (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "0hg91x2p2vajdgcxp7pqj2vl447d6nx4sfwgm5pdg0lmwh63wjwc")))

(define-public crate-a2d-0.1.10 (c (n "a2d") (v "0.1.10") (d (list (d (n "bytemuck") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23.6") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 2)))) (h "0mg7js22ljca0xc2mb8zj0iiv0paysa7hm535k9ppapj73dmqbax")))

(define-public crate-a2d-0.1.11 (c (n "a2d") (v "0.1.11") (d (list (d (n "bytemuck") (r "^1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 0)) (d (n "shaderc") (r "^0.6.2") (d #t) (k 1)) (d (n "wgpu") (r "^0.5") (d #t) (k 0)) (d (n "winit") (r "^0.22.2") (d #t) (k 2)))) (h "0g2jvn7gqkanszcgs734hngx1ya5w6rzl46d0qh1zmixhsardsrv")))

