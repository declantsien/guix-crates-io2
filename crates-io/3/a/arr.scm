(define-module (crates-io #{3}# a arr) #:use-module (crates-io))

(define-public crate-arr-0.1.0 (c (n "arr") (v "0.1.0") (h "0sfb7v0nap2b2v3415hhswwza66l22hx941clik78865c37d0691")))

(define-public crate-arr-0.2.0 (c (n "arr") (v "0.2.0") (h "03w5ymmybib7702h3xs4g3zwg8mlcvy2x8q73l6xjb60b8a7flgp")))

(define-public crate-arr-0.3.0 (c (n "arr") (v "0.3.0") (h "1s0xs3bcrc0rfid8fqnmvlchda3439zdqrcpi056hc5b17lx5641")))

(define-public crate-arr-0.3.1 (c (n "arr") (v "0.3.1") (h "11bdk8pigimcsdiafrs1p9isc2dlg8gsi077nn5526f7mgm174a2")))

(define-public crate-arr-0.4.0 (c (n "arr") (v "0.4.0") (h "1p3mn3r5cr4b1vy0bbp56sgaldxan3lxab43ljvpiaybxasm4v4y")))

(define-public crate-arr-0.4.1 (c (n "arr") (v "0.4.1") (h "0cqigxzg0dblzyhig8ivjcnyz2zxla8w9l4656cz155mq1glnsas")))

(define-public crate-arr-0.5.0 (c (n "arr") (v "0.5.0") (h "0xi40yng1c80d6719r0h00j5l2wl5vdlzrzgk7pqkvk1g0sajarf")))

(define-public crate-arr-0.6.0 (c (n "arr") (v "0.6.0") (h "1nn20d4v0wxqz9hwr6j44wh5hswhiqdiplvg3cr5wm8g7c39h38s")))

(define-public crate-arr-0.6.1 (c (n "arr") (v "0.6.1") (h "12pd7xfbbp7xrhivhjq341cq8lj1l9dhj0xza6i86w2rhpq7rd2j")))

