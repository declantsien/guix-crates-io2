(define-module (crates-io #{3}# a acm) #:use-module (crates-io))

(define-public crate-acm-0.1.0 (c (n "acm") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "common_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0am0224p2iaybk8qxgfiydhz24f6rg57jvq2syffq7qqqyi7h4ap")))

(define-public crate-acm-0.1.1 (c (n "acm") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "common_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "03j80sdw5zb24jamhs9nybfx4802d30b9fgxdysy7q6ykiprlcim")))

(define-public crate-acm-0.1.2 (c (n "acm") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "common_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1v0qwflx28pfgw0invqhm82q47pfy26jgfriqx2088mcscx8r9n1")))

(define-public crate-acm-0.1.2-1 (c (n "acm") (v "0.1.2-1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "common_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "1f8skjaqvwq9ipvflhzzc9dszlcz2d66nzrxb89wqgy69xi93zg7") (y #t)))

(define-public crate-acm-0.1.3 (c (n "acm") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "common_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)))) (h "0n8h37pr2iy875f2j636j6dybfkgy30lgns16i30qyxrnjmzikl9")))

(define-public crate-acm-0.1.4 (c (n "acm") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.12") (d #t) (k 0)))) (h "0ylzzpj8w3wfjk5469g5hdb492i1mgqffr6a2af2axpzj6s8c5zb")))

