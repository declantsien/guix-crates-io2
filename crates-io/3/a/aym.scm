(define-module (crates-io #{3}# a aym) #:use-module (crates-io))

(define-public crate-aym-0.1.0 (c (n "aym") (v "0.1.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "03vva8xgqabhy0s95m603dkfbnj8iyp1sx74xwsr2f67acng1c4n")))

(define-public crate-aym-0.1.1 (c (n "aym") (v "0.1.1") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0f2spc0j70l6qyvdrf2x2z09v56sk43wbzn5sphczv3dlfvih7v8")))

(define-public crate-aym-0.15.0 (c (n "aym") (v "0.15.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "14wr94546v3mfm6z9z0ylqi1d3a16gwr2v1snvfrgrnqass3psd0")))

(define-public crate-aym-0.16.0 (c (n "aym") (v "0.16.0") (d (list (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "092g5c6xhjv6zizajrxi9rzd5j6kkx2yql9rbv7xh2l3fkbnvhgz")))

