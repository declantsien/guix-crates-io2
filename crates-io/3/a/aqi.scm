(define-module (crates-io #{3}# a aqi) #:use-module (crates-io))

(define-public crate-aqi-0.1.0 (c (n "aqi") (v "0.1.0") (h "0qf6mdjd2z2a2lg8vlwz9kfv6kp1r1ri0ifj1l820dxlqfvbfg75") (f (quote (("std") ("default" "std"))))))

(define-public crate-aqi-0.1.1 (c (n "aqi") (v "0.1.1") (h "068bda701nnrfwdl7jdw47lzh1p7k1q5yd7xxg5kh0rlgcxnw7dy") (f (quote (("std") ("default" "std"))))))

(define-public crate-aqi-0.2.0 (c (n "aqi") (v "0.2.0") (h "0j8gr413x9w3c1yyxhnxwc45h6q9da2gm8qmb661db2aqldxhifi") (f (quote (("std") ("default"))))))

