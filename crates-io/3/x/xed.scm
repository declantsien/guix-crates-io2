(define-module (crates-io #{3}# x xed) #:use-module (crates-io))

(define-public crate-xed-0.1.0 (c (n "xed") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "xed-sys2") (r "^0.1.1") (d #t) (k 0)))) (h "1n6ihfl4dhia19zr8bk33i74gdf702m7gmyk8mv78hk04kwiqcxf") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-xed-0.1.1 (c (n "xed") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "xed-sys2") (r "^0.1.1") (d #t) (k 0)))) (h "1w099q26dni1sja11lppna7cc60ziwbpqnpjnfj6qpnny2dj7yjm") (f (quote (("std" "thiserror-no-std/std"))))))

(define-public crate-xed-0.1.2 (c (n "xed") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "xed-sys2") (r "^0.1.1") (d #t) (k 0)))) (h "1d28mv3fsivd82mm83rfnh7mn9xmyz3452mddgl4ddrs6c3sd2j7") (f (quote (("std" "thiserror-no-std/std"))))))

