(define-module (crates-io #{3}# x xqr) #:use-module (crates-io))

(define-public crate-xqr-0.1.0 (c (n "xqr") (v "0.1.0") (d (list (d (n "jsonwebtoken") (r "^8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hkf02qqwr11h7isyn37mwfqx3craq9bbi7dbixwjyh31l6cz35a")))

(define-public crate-xqr-0.2.0 (c (n "xqr") (v "0.2.0") (d (list (d (n "jwt-simple") (r "^0.11.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0diyylh67xnvmdmh70mz1rc09cr2qpzjcs4xb20ka8ndf11yaqny")))

(define-public crate-xqr-0.3.0 (c (n "xqr") (v "0.3.0") (d (list (d (n "jwt-simple") (r "^0.11.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0166gjc75v43aidvszgv27zm0yfrn3qxzxsp1wz7wf7g1xf051qh")))

(define-public crate-xqr-0.4.0 (c (n "xqr") (v "0.4.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "jwtk") (r "^0.2.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1x44bvvmc2rd05jsrdj9bpkn15b6warxnl6vq8c1cwb8p8mxxygr")))

