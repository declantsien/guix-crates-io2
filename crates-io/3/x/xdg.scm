(define-module (crates-io #{3}# x xdg) #:use-module (crates-io))

(define-public crate-xdg-1.0.0 (c (n "xdg") (v "1.0.0") (h "1adaiskyvcqsfm5ffw93h7d764gnlpsipwd2yqg9ws6gflg5z4gb")))

(define-public crate-xdg-2.0.0-pre1 (c (n "xdg") (v "2.0.0-pre1") (h "1dgh9yj2lwbna974f2g540z1i80y98chps4w59djx3z1dmdiv75q")))

(define-public crate-xdg-2.0.0-pre2 (c (n "xdg") (v "2.0.0-pre2") (h "19b9q1xdar5mqxagwivavnn9ipp32db6m8mndy16607p4ijbz74v")))

(define-public crate-xdg-2.0.0-pre3 (c (n "xdg") (v "2.0.0-pre3") (h "1ik1vqx9xj4y1v3f1rsc6vhv22m17wbz1wvzbxdqw81mdir4000x")))

(define-public crate-xdg-2.0.0-pre4 (c (n "xdg") (v "2.0.0-pre4") (h "0qcgzn144h196zjq58rnax3par3ayxlzkawxg0p6s8azcsjbywsi")))

(define-public crate-xdg-2.0.0-pre5 (c (n "xdg") (v "2.0.0-pre5") (h "1yxfpmrh1n4rnpr3lgwalgynjm2cmrg7nsf7k3m2z14ncpfr518n")))

(define-public crate-xdg-2.0.0-pre6 (c (n "xdg") (v "2.0.0-pre6") (h "0ijyllzvv7hjf0zz8a8sk9d3dzi8jpa0z81w3mzzl9lyfg3griz8")))

(define-public crate-xdg-2.0.0-pre7 (c (n "xdg") (v "2.0.0-pre7") (h "0p9wzjk9b1hk07814r0kr4ffknw6j66lip8399hw073n59bgfcgx")))

(define-public crate-xdg-2.0.0 (c (n "xdg") (v "2.0.0") (h "0lxlblc347fbgmmvm6198dq7y27bmwx5ii8aix1hy4bppajk3f3p")))

(define-public crate-xdg-2.1.0 (c (n "xdg") (v "2.1.0") (h "0q8c1g6psrwgr9cvi9vjbv1c6ijrgr60vmwi8g7i7ppbh4i7qsx6")))

(define-public crate-xdg-2.2.0 (c (n "xdg") (v "2.2.0") (h "0mws8a0fr3cqk5nh7aq9lmkmhzghvasqy4mhw6nnza06l4d6i2fh")))

(define-public crate-xdg-2.3.0 (c (n "xdg") (v "2.3.0") (d (list (d (n "dirs") (r "^1.0") (d #t) (k 0)))) (h "11hjfkw9h3sdll9ziwfrd2ilxmhydx4ykp5ahvri6xvjr5yzqk6y")))

(define-public crate-xdg-2.4.0 (c (n "xdg") (v "2.4.0") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)))) (h "00sqvl6v0sjdrrmyk2671sshnjlbjdwgb1lw0f3jchbhijazw8rs")))

(define-public crate-xdg-2.4.1 (c (n "xdg") (v "2.4.1") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)))) (h "1xl81zfx5fsc5n06h77s0fvrslzhh2piabfz0c1lqk5xbkdq6i8c")))

(define-public crate-xdg-2.5.0 (c (n "xdg") (v "2.5.0") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vkzfsy3n85qnn1076h9111jg3h7k9r99jqi8nnrq3kmbbdrg1b8")))

(define-public crate-xdg-2.5.1 (c (n "xdg") (v "2.5.1") (d (list (d (n "home") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1nj9n6p92sdlgpx95d9wz1kh1c9dfg1gl693yq135c64l6zlrky7")))

(define-public crate-xdg-2.5.2 (c (n "xdg") (v "2.5.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0im5nzmywxjgm2pmb48k0cc9hkalarz57f1d9d0x4lvb6cj76fr1")))

