(define-module (crates-io #{3}# x x1b) #:use-module (crates-io))

(define-public crate-x1b-0.1.0 (c (n "x1b") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)))) (h "1w87azd7lzrdy0dnka262p0yd8jcb6jmm6phwqypa2bz6v5f6cm4")))

(define-public crate-x1b-0.2.0 (c (n "x1b") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)))) (h "0i370pxq5mk09j72r6aszhfxll3dqydrcwcfd09phdnwach5sb76")))

(define-public crate-x1b-0.2.1 (c (n "x1b") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)))) (h "1g7r1559g0qmzhx9dxp0kz0x3c8ij99dww1h6pla0w1w2va1n9lm")))

(define-public crate-x1b-0.2.2 (c (n "x1b") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.4.0") (d #t) (k 0)))) (h "06x7mkmqq91pdngh0bmfj201413r013cxjcc2jv04iqgy5b38zkr")))

(define-public crate-x1b-0.2.3 (c (n "x1b") (v "0.2.3") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)))) (h "0nzp6zpgwxnbd6axkhvchc9lk71sp45amvqaqghaqsyjjy6gqims")))

(define-public crate-x1b-0.3.0 (c (n "x1b") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "fnv") (r "^1.0.3") (d #t) (k 0)))) (h "11lhlwiy6z7cxhpyk7mvf5a228c58phn8p5f1drkcxy8yqwcwqb4")))

(define-public crate-x1b-0.4.0 (c (n "x1b") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)))) (h "0wyjr98r1557x40fanj2zn6whsbrmmp3pvd8grgdmf3svzf15p89") (y #t)))

(define-public crate-x1b-0.4.1 (c (n "x1b") (v "0.4.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)))) (h "0571bbq3r8263vwmswikjpxqihpm8nlh6a7jyib9wasgqp7s2qm0")))

(define-public crate-x1b-0.5.0 (c (n "x1b") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)))) (h "10kxps858imcl9nxi7pwp8n47pxc2lzzll9ji4bfz6bsk05llsdq")))

(define-public crate-x1b-0.5.1 (c (n "x1b") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)))) (h "1scncxkycd3favm0rmhf52mhr7742mhfc9ld8zsy23mafzk94gmk")))

(define-public crate-x1b-0.8.0 (c (n "x1b") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)))) (h "1xwlhlhrn28waz85a8csjhmm1kmq2bgy96fa2pa1cil98ggww3yx")))

