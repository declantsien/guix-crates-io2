(define-module (crates-io #{3}# x xvf) #:use-module (crates-io))

(define-public crate-xvf-0.1.0 (c (n "xvf") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scmp") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1dn5rd12b7v9vv7drs2m7j78yzvllwqsb8v5lh7pbs5wyl82kiky")))

(define-public crate-xvf-0.1.1 (c (n "xvf") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scmp") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "0nrp4hmk9hd614bwh4lylr2yxz2907yqwyk7fyqppn9g6gqwq920")))

(define-public crate-xvf-0.1.2 (c (n "xvf") (v "0.1.2") (d (list (d (n "bpaf") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scmp") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1bj6iw37z9a4i70nzs6ivmikvjjjcdcn816946y47r6q0n0xix9h")))

(define-public crate-xvf-0.1.3 (c (n "xvf") (v "0.1.3") (d (list (d (n "bpaf") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "scmp") (r "^0.1.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 0)))) (h "1fs6w4b2b2s02vibymwafas229mcfynx7i1j7xlc1nihbf8jb2w6")))

