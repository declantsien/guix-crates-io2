(define-module (crates-io #{3}# x xml) #:use-module (crates-io))

(define-public crate-xml-0.0.1 (c (n "xml") (v "0.0.1") (h "1x2mpsk0c8biy677hzzmcifakd4hvhhld0ipzrx0kvjkwkqwsvy2")))

(define-public crate-xml-0.8.9 (c (n "xml") (v "0.8.9") (d (list (d (n "xml_rs") (r "=0.8.9") (d #t) (k 0) (p "xml-rs")))) (h "017wb04y7sy8z0g5flr7jh1vzzfdhqyrxgbv8zsszyphs147m9fb") (r "1.63")))

(define-public crate-xml-0.8.10 (c (n "xml") (v "0.8.10") (d (list (d (n "xml_rs") (r "^0.8.10") (d #t) (k 0) (p "xml-rs")))) (h "1xx78y1i9qj714fsip25ycpac101nvryb3c1ay9sdanmv2nv1daw") (r "1.63")))

(define-public crate-xml-0.8.20 (c (n "xml") (v "0.8.20") (d (list (d (n "xml_rs") (r "^0.8.20") (d #t) (k 0) (p "xml-rs")))) (h "1d1arxzig1zf0w3l2d136jfcwm79rgng13h1941svcxlanfckqgd") (r "1.63")))

