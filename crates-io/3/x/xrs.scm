(define-module (crates-io #{3}# x xrs) #:use-module (crates-io))

(define-public crate-xrs-0.0.0 (c (n "xrs") (v "0.0.0") (h "0qw8vnikmp8z7z46rz00i98g80zjlzzbvbnvn4isk5bqy5y69nf4")))

(define-public crate-xrs-0.0.1 (c (n "xrs") (v "0.0.1") (d (list (d (n "xrb") (r "^0") (d #t) (k 0)))) (h "05gvivbknqxp71qr00xalmcapbfb9ph8fr7g9140a4if350rmi6j")))

