(define-module (crates-io #{3}# x xar) #:use-module (crates-io))

(define-public crate-xar-0.1.0 (c (n "xar") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "libflate") (r "^0.1.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)))) (h "03if0xaziy8sz6x34q8s23j27dmssnqsh7q95kk12c1irwfgjj1v")))

(define-public crate-xar-0.1.1 (c (n "xar") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.12.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "libflate") (r "^0.1.23") (d #t) (k 0)) (d (n "quick-xml") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "0040yw8pa0fy7ww1zaqyd3gwm2y2f0ai5isrgh4r3p09pwql1r9p")))

