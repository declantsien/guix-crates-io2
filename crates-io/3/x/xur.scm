(define-module (crates-io #{3}# x xur) #:use-module (crates-io))

(define-public crate-xur-0.1.0 (c (n "xur") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.2") (d #t) (k 0)))) (h "0g1115xv99cl94y4xsjhwqzmxrn9s251jjb6v4szr6cq51r57a4q")))

