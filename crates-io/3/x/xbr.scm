(define-module (crates-io #{3}# x xbr) #:use-module (crates-io))

(define-public crate-xbr-0.1.0 (c (n "xbr") (v "0.1.0") (d (list (d (n "png") (r "^0.17.9") (d #t) (k 0)))) (h "0b9zcp709346zdkl4kfqynlryspghs786y69ha4d8yadsh5jpb6s")))

(define-public crate-xbr-0.2.0 (c (n "xbr") (v "0.2.0") (d (list (d (n "image") (r "^0.24.7") (o #t) (d #t) (k 0)))) (h "00dl5zhfprhawrbn6x5nwwbms7308ifsgin7g72qir1w5wvj2j09") (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-xbr-0.2.1 (c (n "xbr") (v "0.2.1") (d (list (d (n "image") (r "^0.24.7") (o #t) (d #t) (k 0)))) (h "0n6ffjs8cc3ib7f6wk2bfqmababn149vpcyv8xx4xy7gvcp0j102") (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-xbr-0.2.2 (c (n "xbr") (v "0.2.2") (d (list (d (n "image") (r "^0.24.7") (o #t) (d #t) (k 0)))) (h "05gx7016ligqzgy1ih26bxd5d1i2m9c21syd953scr3qhpp2axl8") (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-xbr-0.2.4 (c (n "xbr") (v "0.2.4") (d (list (d (n "image") (r "^0.24.7") (o #t) (d #t) (k 0)))) (h "0ikly915gb05fdhhk2l509bvdzzyn0lbg6fx11carai4k6h3hrp3") (s 2) (e (quote (("image" "dep:image"))))))

