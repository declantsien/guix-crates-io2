(define-module (crates-io #{3}# x xbe) #:use-module (crates-io))

(define-public crate-xbe-0.1.0 (c (n "xbe") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.11") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "scroll") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1k6zhlcp0w3a5zpc1q4gm3m98y5j62m960p2b8nfrfjsa578qbd5")))

(define-public crate-xbe-0.1.1 (c (n "xbe") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.11") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.70") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.70") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "0rmyicbpm4wwg80rpjp74aaw9cxicag9wyvinfkjxyrsd7k385cz")))

