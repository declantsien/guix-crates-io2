(define-module (crates-io #{3}# x xkb) #:use-module (crates-io))

(define-public crate-xkb-0.1.0 (c (n "xkb") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (f (quote ("xkb"))) (o #t) (d #t) (k 0)) (d (n "xkbcommon-sys") (r "^0.7") (d #t) (k 0)))) (h "092lq63zfwdhp77pcmsjd4f65r896a327m2fq9gnzhb2lsyz7l3x") (f (quote (("x11" "xkbcommon-sys/x11" "xcb") ("static" "xkbcommon-sys/static"))))))

(define-public crate-xkb-0.1.1 (c (n "xkb") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (f (quote ("xkb"))) (o #t) (d #t) (k 0)) (d (n "xkbcommon-sys") (r "^0.7.3") (d #t) (k 0)))) (h "03cf15jxp52vgk7v6bw0qsbjahkf2iln10v29fs4yb6sbv4g8bwn") (f (quote (("x11" "xkbcommon-sys/x11" "xcb") ("static" "xkbcommon-sys/static"))))))

(define-public crate-xkb-0.1.2 (c (n "xkb") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.7") (f (quote ("xkb"))) (o #t) (d #t) (k 0)) (d (n "xkbcommon-sys") (r "^0.7.3") (d #t) (k 0)))) (h "03kwb32789vb9rdcmzh1r693cr584vws5pa4zgddkrq0vg6lqxj2") (f (quote (("x11" "xkbcommon-sys/x11" "xcb") ("static" "xkbcommon-sys/static"))))))

(define-public crate-xkb-0.2.0 (c (n "xkb") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.8") (f (quote ("xkb"))) (o #t) (d #t) (k 0)) (d (n "xkbcommon-sys") (r "^0.7") (d #t) (k 0)))) (h "1vc453dk2hvpix8icfcyh6n65jcxwjp4q06hffr2lcmwr4kk04gm") (f (quote (("x11" "xkbcommon-sys/x11" "xcb") ("static" "xkbcommon-sys/static"))))))

(define-public crate-xkb-0.2.1 (c (n "xkb") (v "0.2.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.7") (d #t) (k 0) (p "xkbcommon-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "1cpxd36dsn5h8pkgzfiwjr1glh5gxlicbwnjydwsaalhvv2jph5f") (f (quote (("x11" "ffi/x11" "xcb") ("static" "ffi/static"))))))

(define-public crate-xkb-0.3.0 (c (n "xkb") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^1.4.1") (d #t) (k 0) (p "xkbcommon-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "xcb") (r "^1") (f (quote ("xkb"))) (o #t) (d #t) (k 0)))) (h "03rfx8n3pajc95riksnshh3aqm8dqij2iis5icl88pa6ylk9x0gj") (f (quote (("x11" "ffi/x11" "xcb") ("static" "ffi/static"))))))

