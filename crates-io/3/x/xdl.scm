(define-module (crates-io #{3}# x xdl) #:use-module (crates-io))

(define-public crate-xdl-0.1.0 (c (n "xdl") (v "0.1.0") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "0fawcbcx59fx02mw0kcdq4a5lwpc0lf70k53j7gpfs5i8zy2dabq")))

(define-public crate-xdl-0.1.1 (c (n "xdl") (v "0.1.1") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "1lvx5ggiy1q8dvf38hbnkp2ks79n6994h9rmmzkbdaj8mm2wygdw")))

(define-public crate-xdl-0.1.2 (c (n "xdl") (v "0.1.2") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "05mgk07pvgnj8yn30rpdpsb2jksv8bibxlpfp5qrav48xr4g27xi")))

(define-public crate-xdl-0.1.3 (c (n "xdl") (v "0.1.3") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "0g6z11l3l86dqflvsyk1k9sgzvhlqvx7xaiphnvlz8vv170hpain")))

(define-public crate-xdl-0.1.4 (c (n "xdl") (v "0.1.4") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "0fc5hwn30xng7vpij4gqpfa9gvcixaxsh4mcgjqhwrfd8l8qdpzn")))

(define-public crate-xdl-0.1.5 (c (n "xdl") (v "0.1.5") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "1is0xsswslh25r81wysjvh5jsagbrgakh2pr40kxa62cm0sn9c4d")))

(define-public crate-xdl-0.1.6 (c (n "xdl") (v "0.1.6") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "0v8gy8q6s0s95z4k6h14lnpscx9hdarsvl68fcy6hi8l9n5d15zj")))

(define-public crate-xdl-0.1.7 (c (n "xdl") (v "0.1.7") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (d #t) (k 0)))) (h "18zx4q5w2gqiybbwx9kn8r3rr5dxjghhqjgz173qyvsan8z5hama")))

(define-public crate-xdl-0.2.0 (c (n "xdl") (v "0.2.0") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "rokol") (r "^0.2.2") (f (quote ("impl-app"))) (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.34.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "06kp7pzqprbnwlf26wlhijpbg4z547rd24n82w6dj9mifggbxma4")))

(define-public crate-xdl-0.2.1 (c (n "xdl") (v "0.2.1") (d (list (d (n "igri") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.6") (d #t) (k 0)) (d (n "rokol") (r "^0.4.0") (f (quote ("impl-app"))) (o #t) (d #t) (k 0)) (d (n "sdl2") (r "^0.35.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "01cvganlwbwym60c5gnz46zk22rn0zl8xlfks4rvhp2n0l5wpwlw")))

(define-public crate-xdl-0.2.2 (c (n "xdl") (v "0.2.2") (d (list (d (n "igri") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1vi7m2bpi6gnha6dvwnkzd8k66wzicm86fw3fq9kas5zfkn0ngw3") (f (quote (("default" "sdl2" "serde"))))))

(define-public crate-xdl-0.3.0 (c (n "xdl") (v "0.3.0") (d (list (d (n "igri") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0viib7vg97xcwvn5g9zb2wr30im5a32ddj2lsc7krpnsqv1dy4my") (f (quote (("default" "sdl2" "serde"))))))

