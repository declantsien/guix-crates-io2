(define-module (crates-io #{3}# x x64) #:use-module (crates-io))

(define-public crate-x64-0.0.1 (c (n "x64") (v "0.0.1") (d (list (d (n "c2rust-bitfields") (r "^0.3.0") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.0") (d #t) (k 1)))) (h "1bkwliakdgz7gghkdvh4bjd03f55j9zgwgmnb3mv7ig2hjpq2d88")))

(define-public crate-x64-0.0.2 (c (n "x64") (v "0.0.2") (d (list (d (n "c2rust-bitfields") (r "^0.3.0") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.0") (d #t) (k 1)))) (h "12pkkkfr1q1c5fxgmax6dzldc8231hwh5943n1ncmgsn7q31k63j")))

(define-public crate-x64-0.0.3 (c (n "x64") (v "0.0.3") (d (list (d (n "c2rust-bitfields") (r "^0.3.0") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "readme-rustdocifier") (r "^0.1.0") (d #t) (k 1)))) (h "1wvihfssh10hgssrw8c40z7l3r6xwz7jmq9l3w88kw09q7jrwrzv")))

