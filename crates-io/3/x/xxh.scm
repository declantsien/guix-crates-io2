(define-module (crates-io #{3}# x xxh) #:use-module (crates-io))

(define-public crate-xxh-0.1.0 (c (n "xxh") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "01db6mk1ilagka6s3hlhflf087l728gc7msgm6i3hrchw9wszw8x")))

(define-public crate-xxh-0.1.1 (c (n "xxh") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1khlz5vizvinb55g6rqi2b6asndhjka71rpynh43zzalcdmzcjps")))

(define-public crate-xxh-0.1.2 (c (n "xxh") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0ckgq0yw1b017mv83bbjgqkw1djck6z8n6hnz1rngpyz3k88fski")))

