(define-module (crates-io #{3}# x x11) #:use-module (crates-io))

(define-public crate-x11-0.0.1 (c (n "x11") (v "0.0.1") (h "075basz02lc4431mzx6nz0kv9ccb6y83g7hkclswc9xjga437m7g")))

(define-public crate-x11-0.0.2 (c (n "x11") (v "0.0.2") (h "0jl9bwq9fhhbr86z0dbmgdfdhsnxqk2shv10dd868abdyy2nhjbc")))

(define-public crate-x11-0.0.3 (c (n "x11") (v "0.0.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1my7cj4831hyn9rw5k2mxnp1jb0f43l4wmdxf8fq0cw29bzhvlrd")))

(define-public crate-x11-0.0.4 (c (n "x11") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1jk3hzfggxrzh633g12wfx5bdfhhcwgq1pqxx39p6yafk77dzp3p")))

(define-public crate-x11-0.0.5 (c (n "x11") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0vk4gikvzcdqavxwhzry8kygfgpp7na9zi037d6x2diwkggssl5p")))

(define-public crate-x11-0.0.6 (c (n "x11") (v "0.0.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "01j708j1pw4q3gcr4i261js8fmvgvp6iqbbhz5v07pii5in4ivml")))

(define-public crate-x11-0.0.7 (c (n "x11") (v "0.0.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0nfrci4npdc07rvi0dl7anyq87r5bli7pvz0zy2f86gx4sy7j7nk")))

(define-public crate-x11-0.0.8 (c (n "x11") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0n8ilrxwwmj834845xm0nfcgxdzmiwf4zx8yyn17qns5v7b3cv4v")))

(define-public crate-x11-0.0.9 (c (n "x11") (v "0.0.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0vmv3f7xi142xpil23m8pba3vkhv3knm5hqzpj3gddw6pwwd47g2")))

(define-public crate-x11-0.0.10 (c (n "x11") (v "0.0.10") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "11bimhl15dy27x1235xr48677lx0nk9ql6sqzjb6czj749hsf53s")))

(define-public crate-x11-0.0.11 (c (n "x11") (v "0.0.11") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "15z3wjg1f0mmspcrzmyh894lfwv2a6rk58mbad1w8mqb0xi1q81z")))

(define-public crate-x11-0.0.12 (c (n "x11") (v "0.0.12") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1rnn2kqiw2j9jfxjlf6d8mlcvkndv2cpqhp57v5g642j1nypl2as")))

(define-public crate-x11-0.0.13 (c (n "x11") (v "0.0.13") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1cm6nw89546mnfj50w22ic4zzdkyhibqiw3mv0493nymcz71m1cw")))

(define-public crate-x11-0.0.14 (c (n "x11") (v "0.0.14") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0fxivky2l0dwxva89x9p4b708023d17jw6vwq10pq2qgg7hfx03l")))

(define-public crate-x11-0.0.15 (c (n "x11") (v "0.0.15") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "06c68my4hz46c86nbwkk1518irhpwcdkg7bsmirhjifwkg4crzgc")))

(define-public crate-x11-0.0.16 (c (n "x11") (v "0.0.16") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "123haydr5zg5vyc9s9k47s12bf10lshxyqrsnrk61hdvz77np1km")))

(define-public crate-x11-0.0.17 (c (n "x11") (v "0.0.17") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0shfyvihq0m5ldkv66p682xf5r4db1g3vvcn836z3yx833kbr7g1")))

(define-public crate-x11-0.0.18 (c (n "x11") (v "0.0.18") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1i62lwmhwabxrlc4n7waig53z2a2lcgjwq972aya46ykbmrnq2xq")))

(define-public crate-x11-0.0.19 (c (n "x11") (v "0.0.19") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0fgp0026y12xkn44hmvlacdfwmza68mjp9n6mwravj63v06zkdgb")))

(define-public crate-x11-0.0.20 (c (n "x11") (v "0.0.20") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0l72qkxm6mvqm10fg3wcjzj69p6zx7ix84d3g619la8l19z6xncf")))

(define-public crate-x11-0.0.21 (c (n "x11") (v "0.0.21") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "15pyw066sysp9m3sms3nvb6c8f3q93gzjwgrmd49lkrkald8ds4m")))

(define-public crate-x11-0.0.22 (c (n "x11") (v "0.0.22") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "17y14nxs69rb0md3wi214vg9ns0vbpsidahy0k1lrqhh9il2m12d")))

(define-public crate-x11-0.0.23 (c (n "x11") (v "0.0.23") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "07zjnw5amj2jd23r9kd4ynlmnlx74b5a1zciar6fwjjf06qix5z1")))

(define-public crate-x11-0.0.24 (c (n "x11") (v "0.0.24") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1rhizbk374cdp6n4cb8dhva4xllk7yb4z1jsqzymp5z85mz479xp")))

(define-public crate-x11-0.0.25 (c (n "x11") (v "0.0.25") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1zfji6skzl05bg7cymgy2nh8w5wl3jmyg29p035j30sali98qscr")))

(define-public crate-x11-0.0.26 (c (n "x11") (v "0.0.26") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1gjfyb2dc6qlm5lkfzx9m72ana42bfml90zi8z223b2gl925idzn")))

(define-public crate-x11-0.0.27 (c (n "x11") (v "0.0.27") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ggg1zj7fcy8vq9y1nb47zr2ay5b5yfvah7q0ygi2yiy8x14j3hf")))

(define-public crate-x11-0.0.28 (c (n "x11") (v "0.0.28") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1wsfwb0aj275mi3g6jmj3d3wz1q3ib5m1nqzz6l3k0jh4fmv8i70")))

(define-public crate-x11-0.0.29 (c (n "x11") (v "0.0.29") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "03jlp0dpcsiki3r6806p3w2csf7n6dnw4ql63qc6wi2w7dyxabij")))

(define-public crate-x11-0.0.30 (c (n "x11") (v "0.0.30") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "130ch5dpfqh9zqrgxi07fyd2qg660cx4mnr6a1h9ns8g1h30ilyv")))

(define-public crate-x11-0.0.31 (c (n "x11") (v "0.0.31") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "08aqgsrl8jnqpsbpx39m29wjbcgwaa8wz3cb08j6cllyik5l242d")))

(define-public crate-x11-0.0.32 (c (n "x11") (v "0.0.32") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1b54b1qijk9z7syjr976sp17hipf7hd51wx8jdv2n24km1fgi7h7")))

(define-public crate-x11-0.0.33 (c (n "x11") (v "0.0.33") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "10y7izg0z69k8blw7frslj95ag3baswmbqhi0b2ilh53dyjn64s8")))

(define-public crate-x11-0.0.34 (c (n "x11") (v "0.0.34") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1yb26964hx2vz3v4ghx8k86r8x29qnkdk8528filfxwlj20wf2xd")))

(define-public crate-x11-0.0.35 (c (n "x11") (v "0.0.35") (d (list (d (n "dylib") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "16m9vdhfm8v52hmk17985fn44msyan7wfxxnbmc762asja6r2i0y") (f (quote (("dynamic" "dylib"))))))

(define-public crate-x11-0.0.36 (c (n "x11") (v "0.0.36") (d (list (d (n "dylib") (r "*") (o #t) (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0ymr3vldpvighk5li3qgsqd16k60n2c0f18z2zfvf3amx1ly8p24") (f (quote (("dynamic" "dylib"))))))

(define-public crate-x11-1.0.0 (c (n "x11") (v "1.0.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0zmalfnn36nmcp4xizarzwj58spcpf3v583jkx8g3kajk619l96z") (f (quote (("xrender") ("xlib") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-1.0.1 (c (n "x11") (v "1.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0cnzwi4apmbd1y8751fs3sw6vz9bindvvh692p866as9s9kk412d") (f (quote (("xrender") ("xlib") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-1.1.0 (c (n "x11") (v "1.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1059djvhpgc2mgvbr64ac5h4p5r2ncyyc470a0dwhzy855n37ksc") (f (quote (("xt") ("xrender") ("xmu") ("xlib") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-1.1.1 (c (n "x11") (v "1.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0vbwaaklfyf6mfh6wfccxxrqag2bjd2zpgcs1ci2pi5963a2agz5") (f (quote (("xt") ("xrender") ("xmu") ("xlib") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.0.0 (c (n "x11") (v "2.0.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1xgcsqjrv9qdcv3ib03m7ds47nkqfrpyvygxvqw9rv8rk9z5gpkk") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xmu") ("xlib") ("xinput") ("xinerama") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.0.1 (c (n "x11") (v "2.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0i6s6py9d1njq9ngdywgmj5a4a61ikzqq8jdknbcxg512m2r4l35") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xmu") ("xlib") ("xinput") ("xinerama") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.1.0 (c (n "x11") (v "2.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0xcg7wxwavh18yq1lbb91wv3aqc03vzilazxlvlmnv1wk4pis9w1") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xmu") ("xlib") ("xinput") ("xinerama") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.2.0 (c (n "x11") (v "2.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0bpbmxn46p7w5kkky1x13may2r5njx2lrri5kpcqdmq7wm87kxs6") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xmu") ("xlib") ("xinput") ("xinerama") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.2.1 (c (n "x11") (v "2.2.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "1wgrx16000ndvq14w280519cahcb7kz9hij2mpj9q17d3ll2mg68") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xmu") ("xlib") ("xinput") ("xinerama") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.3.0 (c (n "x11") (v "2.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1vwyxiiv7g8cnj1k05pjg8cknrb9ffljciwc8fmpr51zh2b79fc1") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xmu") ("xlib") ("xinput") ("xinerama") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.3.1 (c (n "x11") (v "2.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "04nj5jlbx94ri1y0cr86rgmxbpnvl0j3pmfjl8074mdy3xzin1j1") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xmu") ("xlib") ("xinput") ("xinerama") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.4.0 (c (n "x11") (v "2.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "11hmimcpsng1k0wn1xivlwbql1y8l6kgb4g31q3c49sp4cy11cbs") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xrandr") ("xmu") ("xlib") ("xinput") ("xinerama") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.5.0 (c (n "x11") (v "2.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0qvf448pq1pxgmg0g3m96a10ghy308zrshcmc1jbbwsbrk68b9v3") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xrandr") ("xmu") ("xlib") ("xinput") ("xinerama") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.5.1 (c (n "x11") (v "2.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "05mkxagmn4l16sv7xk4chi1h7cdwgni8w43vp693zv7l1mmkzdad") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xrandr") ("xmu") ("xlib") ("xinput") ("xinerama") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.6.0 (c (n "x11") (v "2.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1r4s496wmks2cvb3fnq9j8g95s7r114ra6nmi6mc397yprz3jj6g") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xrandr") ("xmu") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.6.1 (c (n "x11") (v "2.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1imvpv9mvjfgvfjpgqxkzby3k632zhz5ah9vfj0r8sjb3h1cyhkv") (f (quote (("xtest") ("xt") ("xrender") ("xrecord") ("xrandr") ("xmu") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx"))))))

(define-public crate-x11-2.7.0 (c (n "x11") (v "2.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0js44hiaklcc0h83ssdp7zmqhs5phqqjg4fv1m4i5jah96l3axk9") (f (quote (("xtest") ("xt") ("xss") ("xrender") ("xrecord") ("xrandr") ("xmu") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms"))))))

(define-public crate-x11-2.8.0 (c (n "x11") (v "2.8.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1ad272mwdnsxzccc5w50c17wbfas0dpg3dg08kgcsk7mpav2ij5z") (f (quote (("xtest") ("xt") ("xss") ("xrender") ("xrecord") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms"))))))

(define-public crate-x11-2.9.0 (c (n "x11") (v "2.9.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1aj9wsslagr9q8cvhpaw057rq1jg8aj11jx00ixki51dvqnswax4") (f (quote (("xtest") ("xt") ("xss") ("xrender") ("xrecord") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms"))))))

(define-public crate-x11-2.10.0 (c (n "x11") (v "2.10.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1pagq3azda9ik7i8if29w4wx5jvlxnnsks0b3vasl02v8w17pq2p") (f (quote (("xtest") ("xt") ("xss") ("xrender") ("xrecord") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms"))))))

(define-public crate-x11-2.11.0 (c (n "x11") (v "2.11.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0wyp5mjmi2xlygvw7nmcy27wanb26i3r714rg7cfnbcd5hl9lwdy") (f (quote (("xtest") ("xt") ("xss") ("xrender") ("xrecord") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms"))))))

(define-public crate-x11-2.12.0 (c (n "x11") (v "2.12.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "0as3qylqi7a976p5viiinzm6wxz4hrjp0v0hh72wlzawi6k2ry54") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms"))))))

(define-public crate-x11-2.12.1 (c (n "x11") (v "2.12.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "1a44494pvkp2jscy81mkpmhl5hni9kz4ybcq397aaqh2pw2v8khj") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms"))))))

(define-public crate-x11-2.13.0 (c (n "x11") (v "2.13.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "19i3b9bkvfhby9g9iv64y096b4gsw9dx81kmm99i876nzk4ckdrs") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms"))))))

(define-public crate-x11-2.14.0 (c (n "x11") (v "2.14.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "1vbq1y1plvdpxxpp5zxrm88kjl39v3kk50jb98cm5nl7q6bwa9yv") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms"))))))

(define-public crate-x11-2.15.0 (c (n "x11") (v "2.15.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "0px97j3kq9idl87wnr54n5k7bskr46qyzs3m600ijvh2kxk8z4b8") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms"))))))

(define-public crate-x11-2.16.0 (c (n "x11") (v "2.16.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "03hzlfmci3ia4x5vagd9r3mwkv91znjfsspxvzssp4glsaikjzgk") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox"))))))

(define-public crate-x11-2.17.0 (c (n "x11") (v "2.17.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "0s6i12gsj10d2q0dlb8hp05va0cv3fm6c11a49p86d4brw4l6z67") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox"))))))

(define-public crate-x11-2.17.2 (c (n "x11") (v "2.17.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "0lzzmjyhm3wkkvz9asm7d1d8lsmdn0v5ibh8ybg5kcr5bb430qb7") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox"))))))

(define-public crate-x11-2.17.3 (c (n "x11") (v "2.17.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0wdi9gp3kb16smwmfb8ya8c04ghfblmk20nx9bf29lxmg72llp3y") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox"))))))

(define-public crate-x11-2.17.4 (c (n "x11") (v "2.17.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1vj8ddrhzkp8pb3kbplsscd5qzsgprzynpnw53y0cmw5h447cwdl") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox")))) (y #t)))

(define-public crate-x11-2.17.5 (c (n "x11") (v "2.17.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0nscvjbpxg17k1bcx9kppam559s1nss6ic1bszg9fdhssswnva72") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox"))))))

(define-public crate-x11-2.18.0 (c (n "x11") (v "2.18.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0kmi7m3aq0amcnnzg5s0l74m16sz4fnb5anqq154k5cnj41080ar") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox")))) (y #t)))

(define-public crate-x11-2.18.1 (c (n "x11") (v "2.18.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0dg2d0yrqmwg6prpxkw3vpmwzwxnaki2cn0v64ylh5gp4cqpws9r") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox"))))))

(define-public crate-x11-2.18.2 (c (n "x11") (v "2.18.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0wz7l6dlbraa9zalh9i45v9wibvkir9m2m1sg0jnzcbcaj9d1v3p") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox"))))))

(define-public crate-x11-2.19.0 (c (n "x11") (v "2.19.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1b7z3zck1jgy8hvj63m53rcsb77wf7kri3h7ifak0y3rykqf36c0") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox"))))))

(define-public crate-x11-2.19.1 (c (n "x11") (v "2.19.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0ynqpisflv6p8kyjrzsff9kch57ivwa5nwh2zrg8rfmzm1gmdl3d") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox") ("all" "dpms" "glx" "xcursor" "xf86vmode" "xft" "xinerama" "xinput" "xlib" "xlib_xcb" "xmu" "xrandr" "xrecord" "xrender" "xss" "xt" "xtest" "xtst" "dox"))))))

(define-public crate-x11-2.20.0 (c (n "x11") (v "2.20.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "02197fngqvn05c8ad29wslfhf21gqlgkvkhz0qdq53cj9a3rgbpp") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox") ("all" "dpms" "glx" "xcursor" "xf86vmode" "xft" "xinerama" "xinput" "xlib" "xlib_xcb" "xmu" "xrandr" "xrecord" "xrender" "xss" "xt" "xtest" "xtst" "dox"))))))

(define-public crate-x11-2.20.1 (c (n "x11") (v "2.20.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "10pbvmcyqm6j6zr4zk7znk8silmilihv8jxmbxbl1b0pkidqsqy2") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xfixes") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox") ("all" "dpms" "glx" "xcursor" "xf86vmode" "xfixes" "xft" "xinerama" "xinput" "xlib" "xlib_xcb" "xmu" "xrandr" "xrecord" "xrender" "xss" "xt" "xtest" "xtst" "dox"))))))

(define-public crate-x11-2.21.0 (c (n "x11") (v "2.21.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0bnvl09d7044k067gqdx1ln2r0ljp5f4675icwb0216d9i3aabah") (f (quote (("xtst") ("xtest" "xtst") ("xt") ("xss") ("xrender") ("xrecord" "xtst") ("xrandr") ("xmu") ("xlib_xcb") ("xlib") ("xinput") ("xinerama") ("xft") ("xfixes") ("xf86vmode") ("xcursor") ("glx") ("dpms") ("dox") ("all" "dpms" "glx" "xcursor" "xf86vmode" "xfixes" "xft" "xinerama" "xinput" "xlib" "xlib_xcb" "xmu" "xrandr" "xrecord" "xrender" "xss" "xt" "xtest" "xtst" "dox"))))))

