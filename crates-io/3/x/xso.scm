(define-module (crates-io #{3}# x xso) #:use-module (crates-io))

(define-public crate-xso-0.0.1 (c (n "xso") (v "0.0.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.5") (f (quote ("std"))) (k 0)) (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "jid") (r "^0.10") (f (quote ("minidom"))) (d #t) (k 0)) (d (n "minidom") (r "^0.15") (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "sha3") (r "^0.10") (d #t) (k 0)) (d (n "xso_proc") (r "^0.0.1") (o #t) (d #t) (k 0)))) (h "0skmj4sniz3l57js408hbvqg7mz76ai52ij5h50gqny2npqki7ly") (f (quote (("default" "macros")))) (s 2) (e (quote (("macros" "dep:xso_proc"))))))

