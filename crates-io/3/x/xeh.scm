(define-module (crates-io #{3}# x xeh) #:use-module (crates-io))

(define-public crate-xeh-0.0.1 (c (n "xeh") (v "0.0.1") (h "0v0jh3xqqhyrz7sxsv9bmxrazqkc3kab8dd0crpc6hwkc93pch2m")))

(define-public crate-xeh-0.0.2 (c (n "xeh") (v "0.0.2") (h "0frbrcawq9bcs7fm8y58rv9jqkscb0ka9p9lspivdccwddszj380")))

