(define-module (crates-io #{3}# x xtr) #:use-module (crates-io))

(define-public crate-xtr-0.0.0 (c (n "xtr") (v "0.0.0") (h "0nf1jzpa4176msc61p2ccbwvv3c6gviwaw798wmfx4aissvdqcay")))

(define-public crate-xtr-0.1.0 (c (n "xtr") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "syn") (r "^0.15.20") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "tr") (r "^0.1") (d #t) (k 0)))) (h "1y0lhp5im9si3qc26j1kmmflj4ardnqi30p972ysja3jh7j1mi33")))

(define-public crate-xtr-0.1.3 (c (n "xtr") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "tr") (r "^0.1") (d #t) (k 0)))) (h "17gi7qxxlpq1mdhj4ljsfq54c81fa5gn5r7q6d31dwm4kvyk3lrd")))

(define-public crate-xtr-0.1.4 (c (n "xtr") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "tr") (r "^0.1") (d #t) (k 0)))) (h "1dqrgdcscd3kns0wfss2pslmv4bc151n21nn1nfaxz325hn1zxkg")))

(define-public crate-xtr-0.1.5 (c (n "xtr") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "tr") (r "^0.1") (d #t) (k 0)))) (h "0gc11cp72qrgy4fn06l2jjr3dda6vlh590lw2pawqv0fwmy5fi1z")))

(define-public crate-xtr-0.1.6 (c (n "xtr") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "visit" "extra-traits" "printing"))) (d #t) (k 0)) (d (n "tr") (r "^0.1") (d #t) (k 0)))) (h "0k9xiklvplbqjkn1qwr7rvjmdd9b194bpbbipcxbcs51vc8b7ysl")))

(define-public crate-xtr-0.1.7 (c (n "xtr") (v "0.1.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("clock"))) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit" "extra-traits" "printing"))) (d #t) (k 0)) (d (n "tr") (r "^0.1") (d #t) (k 0)))) (h "0n5wa3xrxwm15hz7jjadjrcvb2rp7094icqw4iswndrdcxn3pfpc")))

(define-public crate-xtr-0.1.8 (c (n "xtr") (v "0.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("clock"))) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit" "extra-traits" "printing"))) (d #t) (k 0)) (d (n "tr") (r "^0.1") (d #t) (k 0)))) (h "0p41lwd8zdqx497j8hzv99qy5p249dpm55fg51760y8gmhjj1wcc")))

(define-public crate-xtr-0.1.9 (c (n "xtr") (v "0.1.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (f (quote ("clock"))) (k 0)) (d (n "clap") (r "^4.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "visit" "extra-traits" "printing"))) (d #t) (k 0)) (d (n "tr") (r "^0.1") (d #t) (k 0)))) (h "0pc2sv4kwx09bx456pa2qqyhqqxra4x1k04wb3asv45qydfqs8bl")))

