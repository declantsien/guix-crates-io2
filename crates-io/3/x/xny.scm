(define-module (crates-io #{3}# x xny) #:use-module (crates-io))

(define-public crate-xny-1.0.2 (c (n "xny") (v "1.0.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "std"))) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "12h866862laxfhrj990sm179qgc1s7kxbpvjcq8aapxmgkmczzp0")))

(define-public crate-xny-1.0.3 (c (n "xny") (v "1.0.3") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive" "std"))) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("serde_derive"))) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1is8msr5ic1ibk8i9i90l7s6qc5ql5ykqzl99ra2qa359wj2hxr5")))

(define-public crate-xny-1.0.4 (c (n "xny") (v "1.0.4") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "minreq") (r "^2.7.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0qwa8jvf9gh4isb1mqzsfzyp0shr2nf9xzwqcz7k73wsqhap54x7")))

