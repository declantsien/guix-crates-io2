(define-module (crates-io #{3}# x xz2) #:use-module (crates-io))

(define-public crate-xz2-0.1.0 (c (n "xz2") (v "0.1.0") (d (list (d (n "lzma-sys") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1pmbjg01xnvpsbfmc5867alywi5wgzcfmnv3z7mfq18z22l7pwg2")))

(define-public crate-xz2-0.1.1 (c (n "xz2") (v "0.1.1") (d (list (d (n "lzma-sys") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1b7yi2kvsy1nplhdln2qyvl5a15fahlc2y6akxm0n2pb5gmwr6mc")))

(define-public crate-xz2-0.1.2 (c (n "xz2") (v "0.1.2") (d (list (d (n "lzma-sys") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "19vf1z6qa85m9znnq2av2vjyxbf49sp6g1qiffaqpp4gd3nf330r")))

(define-public crate-xz2-0.1.3 (c (n "xz2") (v "0.1.3") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lzma-sys") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1c15pbvdpyf1k64laxcgasdac4v7ykd7f7y60y8mjc8723ghnlg9") (f (quote (("tokio" "tokio-io" "futures"))))))

(define-public crate-xz2-0.1.4 (c (n "xz2") (v "0.1.4") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lzma-sys") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1yjrksnccmfbfglb5ikdwb3pd900xliq368xg7fi9l046lf5kpwq") (f (quote (("tokio" "tokio-io" "futures"))))))

(define-public crate-xz2-0.1.5 (c (n "xz2") (v "0.1.5") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lzma-sys") (r "^0.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0ypjjkvx6xxx2qdm62qd2lxz9yssl106dzaqjiy5ghrh60fz92yz") (f (quote (("tokio" "tokio-io" "futures"))))))

(define-public crate-xz2-0.1.6 (c (n "xz2") (v "0.1.6") (d (list (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lzma-sys") (r "^0.1.11") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0v4jb0193gx8s1kvd2ajsgh0ffmwhqhfmrrw1n1h2z7w6jgqcyf1") (f (quote (("tokio" "tokio-io" "futures"))))))

(define-public crate-xz2-0.1.7 (c (n "xz2") (v "0.1.7") (d (list (d (n "futures") (r "^0.1.26") (o #t) (d #t) (k 0)) (d (n "lzma-sys") (r "^0.1.18") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1.17") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1.12") (o #t) (d #t) (k 0)))) (h "1qk7nzpblizvayyq4xzi4b0zacmmbqr6vb9fc0v1avyp17f4931q") (f (quote (("tokio" "tokio-io" "futures") ("static" "lzma-sys/static"))))))

