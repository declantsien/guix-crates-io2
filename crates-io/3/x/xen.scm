(define-module (crates-io #{3}# x xen) #:use-module (crates-io))

(define-public crate-xen-0.0.0-pre (c (n "xen") (v "0.0.0-pre") (d (list (d (n "xen-sys") (r "= 0.0.0-pre") (d #t) (k 0)))) (h "0hnaxabkfqbns4541mrc5q5lvghngkqzgi8drdk34fs6gp8dp6yi")))

(define-public crate-xen-0.0.0-pre1 (c (n "xen") (v "0.0.0-pre1") (d (list (d (n "xen-sys") (r "= 0.0.0-pre1") (d #t) (k 0)))) (h "0x83jy71zwjvk9js97vp7vi72js7db7zkp2bmlzjina5rbwkd7hi")))

