(define-module (crates-io #{3}# x xcf) #:use-module (crates-io))

(define-public crate-xcf-0.1.0 (c (n "xcf") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)))) (h "0499lma9nwrdz18cwkap9ywyy1ljc2fzwwkbl0lbp2y96l2gh3mm")))

(define-public crate-xcf-0.1.1 (c (n "xcf") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)))) (h "1x888granbikxjgap908qlbzkjv7m16nn88x70iwmf71hj5zvq2w")))

(define-public crate-xcf-0.1.2 (c (n "xcf") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)))) (h "1yf9z0izbvxxxdfpg0iczn0v5vr80jv0izbf4dabw74mwy9g9159")))

(define-public crate-xcf-0.2.0 (c (n "xcf") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)))) (h "0x8qwfp7gxxd9gj7b2ki9vww6vlx8zfpbxzmrrhza75yj15s5qhy")))

(define-public crate-xcf-0.3.0 (c (n "xcf") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)))) (h "0ch21xfhqaran3kaqx4ix4lh463vss1pgmb0vlzkxilir1bq8xcs")))

(define-public crate-xcf-0.4.0 (c (n "xcf") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "derive-error") (r "^0.0.4") (d #t) (k 0)))) (h "0lm9shbrapwsvq48pi2jvv6jgsfrvalci2gjzzklxh2b8n2cch4f")))

