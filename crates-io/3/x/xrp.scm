(define-module (crates-io #{3}# x xrp) #:use-module (crates-io))

(define-public crate-xrp-0.0.0 (c (n "xrp") (v "0.0.0") (h "15xl06x7bjwmw69y7fx1h6qv7h6y1m5gj2ipgbp0nvvxli5vpd8s")))

(define-public crate-xrp-0.1.3 (c (n "xrp") (v "0.1.3") (h "05y0vx5mmwnvzwmc7anawspn104ybig9sxr96bqrp97j2qqnb4vn")))

