(define-module (crates-io #{3}# x xyz) #:use-module (crates-io))

(define-public crate-xyz-0.1.0 (c (n "xyz") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.7") (d #t) (k 2)))) (h "1fjxqhrnvr9wfr723qm26ynvmylg1496m5zvl7dxyh81bahzvfx8")))

(define-public crate-xyz-0.2.0 (c (n "xyz") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.7") (d #t) (k 2)))) (h "06d14gjd37wjbap1iq4ih7gky9s0b3n4j3r7w31fjaqis0gp3krj")))

(define-public crate-xyz-0.2.1 (c (n "xyz") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.12") (d #t) (k 2)))) (h "0by37jyxf7hfkfdqdv35s6sn240n9463nd1074b3s4w9j7f328vi")))

