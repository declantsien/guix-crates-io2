(define-module (crates-io #{3}# x xdc) #:use-module (crates-io))

(define-public crate-xdc-0.1.0 (c (n "xdc") (v "0.1.0") (d (list (d (n "linkme") (r "^0.3") (d #t) (k 0)) (d (n "xdc_macros") (r "^0.1") (k 0)))) (h "113g69gmapkmrlkp6y3v128mvwjpfbyxb164fv5zf2d8brnfar0b") (f (quote (("std" "alloc" "xdc_macros/std") ("default" "std") ("alloc" "xdc_macros/alloc"))))))

