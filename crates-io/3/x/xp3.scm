(define-module (crates-io #{3}# x xp3) #:use-module (crates-io))

(define-public crate-xp3-0.1.0 (c (n "xp3") (v "0.1.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "19zi2v6b64xljqmn7rbbs8h3nnvj1y1b8dmd8pjafygy3r42cl2p")))

(define-public crate-xp3-0.2.0 (c (n "xp3") (v "0.2.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0qq7rhj8ppywfk8bnwycb02am7dvjv0gmqk62lr1acf0fv7sh0z1")))

(define-public crate-xp3-0.2.1 (c (n "xp3") (v "0.2.1") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "0n63n53s1c0924qgsp640w9yjdf7is59dl79whrp0jsi919qy0il")))

(define-public crate-xp3-0.2.2 (c (n "xp3") (v "0.2.2") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "17bbzbmr86a25c4ddv954i1cbpyppnz4pw70y169lg3sfa5v3wc6")))

(define-public crate-xp3-0.3.0 (c (n "xp3") (v "0.3.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)))) (h "1dkaalni8hz7ng65aqlwxhingn9dx1bxjbyll9c8kngp9vd2iiv1")))

