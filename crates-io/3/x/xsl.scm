(define-module (crates-io #{3}# x xsl) #:use-module (crates-io))

(define-public crate-xsl-0.1.0 (c (n "xsl") (v "0.1.0") (d (list (d (n "allocator-api2") (r "^0.2.16") (o #t) (d #t) (k 0)) (d (n "allocator-api2") (r "^0.2.16") (d #t) (k 2)) (d (n "libc-print") (r "^0.1.22") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "15l5g80rdis0lan4g2r0li6rj1iyjjd5gxhzmhllnnhz61xylz99") (f (quote (("std") ("default" "allocator-api2")))) (s 2) (e (quote (("allocator-api2" "dep:allocator-api2"))))))

(define-public crate-xsl-0.1.1 (c (n "xsl") (v "0.1.1") (d (list (d (n "allocator-api2") (r "^0.2.16") (o #t) (d #t) (k 0)) (d (n "allocator-api2") (r "^0.2.16") (d #t) (k 2)) (d (n "libc-print") (r "^0.1.22") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1sjs0znjsjqqzbk5p0if17nmcah5rw7m9mk25yf7cv7nzl1ks4jl") (f (quote (("std") ("default" "allocator-api2")))) (s 2) (e (quote (("allocator-api2" "dep:allocator-api2"))))))

