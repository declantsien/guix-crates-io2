(define-module (crates-io #{3}# x xwt) #:use-module (crates-io))

(define-public crate-xwt-0.3.0 (c (n "xwt") (v "0.3.0") (d (list (d (n "xwt-web-sys") (r "^0.3") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.2") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "1vz273095p8vngxzdy5np9v88kfc9vnl2nhayd0yh4bakkkcvhpp")))

(define-public crate-xwt-0.3.1 (c (n "xwt") (v "0.3.1") (d (list (d (n "xwt-web-sys") (r "^0.3") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.2") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "1vxhd9b7hrmgafhi6kb749hj7hdl42x4ingdr1spsg9i1sl48zpp")))

(define-public crate-xwt-0.4.0 (c (n "xwt") (v "0.4.0") (d (list (d (n "xwt-web-sys") (r "^0.4") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.2") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0gimzygh6n82slqigvy54dbp791f0sr6f5nfvm4i2i0m4ywy7qxz")))

(define-public crate-xwt-0.4.1 (c (n "xwt") (v "0.4.1") (d (list (d (n "xwt-web-sys") (r "^0.4") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.2") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "1p1mxsm4lqjb1yr8ih8cmqipkzd6swmvrj5fv3d5sywlkqj89b1h")))

(define-public crate-xwt-0.4.2 (c (n "xwt") (v "0.4.2") (d (list (d (n "xwt-web-sys") (r "^0.4") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.2") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0ahfy70j6jrlwr96isvnx8zcx3nif4vqkdj5cvwj4jwar4rs9dvc")))

(define-public crate-xwt-0.5.0 (c (n "xwt") (v "0.5.0") (d (list (d (n "xwt-web-sys") (r "^0.5") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.2") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0r8czr75h67yz8ik9lfvcd1nn5w76q9cxbfclgx05i36yzqvr7hn")))

(define-public crate-xwt-0.6.0 (c (n "xwt") (v "0.6.0") (d (list (d (n "xwt-web-sys") (r "^0.6") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.2") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "02158p6mid8wrvcl2h6h79293aih3v5vcpdxf2qd3cd7rdqx5fj3")))

(define-public crate-xwt-0.7.0 (c (n "xwt") (v "0.7.0") (d (list (d (n "xwt-web-sys") (r "^0.7") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.2") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "00aqfs6fqvn2vxmli6kv0ynlxnv6w8q3ry6s2abr0spm17bc3q9p")))

(define-public crate-xwt-0.8.0 (c (n "xwt") (v "0.8.0") (d (list (d (n "xwt-web-sys") (r "^0.8") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.3") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0hs8m5cn9i9aw5gmisqaix22iyd8lxkg7aasvs01qd3z77bnqx6i")))

(define-public crate-xwt-0.9.0 (c (n "xwt") (v "0.9.0") (d (list (d (n "xwt-web-sys") (r "^0.9") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.4") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0fx8vb3j27hin52vbkkgbpyf0n5sap02k0rvl66lr59l5k7nx5pp")))

(define-public crate-xwt-0.10.0 (c (n "xwt") (v "0.10.0") (d (list (d (n "xwt-web-sys") (r "^0.9") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.5") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "1z83rpwhkraxbfr4dk119av9qab1ijnkb1mak21r0wrlyz5kd0q2")))

(define-public crate-xwt-0.11.0 (c (n "xwt") (v "0.11.0") (d (list (d (n "xwt-web-sys") (r "^0.10") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.6") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "0clp38pcpwf4pp4lcjbx1l9h3w8f4ap2m4y9c73xrq9wq6d9wlbr")))

(define-public crate-xwt-0.12.0 (c (n "xwt") (v "0.12.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "xwt-web-sys") (r "^0.10") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.6") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "04pcdckgxabbl38s4hw9fpglswcv86qc3whh922npz7caw75h0i2")))

(define-public crate-xwt-0.13.0 (c (n "xwt") (v "0.13.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "xwt-web-sys") (r "^0.11") (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "xwt-wtransport") (r "^0.7") (t "cfg(not(target_family = \"wasm\"))") (k 0)))) (h "1h093l4lcdkgwk95630nn289jf0ywa84x85rrnmb0m05xbjg7vmb")))

