(define-module (crates-io #{3}# x xor) #:use-module (crates-io))

(define-public crate-xor-0.2.0 (c (n "xor") (v "0.2.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.23") (d #t) (k 0)))) (h "0f2wm3dvbcn6hvnvy3izc47ly95nd0bhm047dd084kd719p8hxv1")))

(define-public crate-xor-1.0.0 (c (n "xor") (v "1.0.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)))) (h "18l0a9qskjmx1zmrkjpdrcalabd6qwlzq8rnw1g4vw06p3prjwr7")))

(define-public crate-xor-1.1.0 (c (n "xor") (v "1.1.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)))) (h "0znaxc2cl8aimmrjn6vj0hclnf1y2c4nizdrbqlq5147x5hjdh6y")))

(define-public crate-xor-1.1.1 (c (n "xor") (v "1.1.1") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)))) (h "0s1qjli14kvkpf2fa4za8qk7rw1jv8r8qzc7phs0c7ylcxc6h75q")))

(define-public crate-xor-1.2.0 (c (n "xor") (v "1.2.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)))) (h "11s34lqmv51smldlnizk3d35yp8kr6x69k5vi8awzyf0djl524sb")))

(define-public crate-xor-1.2.1 (c (n "xor") (v "1.2.1") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)))) (h "01ys33f5cd56gd18292652pd2qw3zc5dad1lz8ab05xwzn7br6cf")))

(define-public crate-xor-1.2.2 (c (n "xor") (v "1.2.2") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)))) (h "0dj783186vgfvra5f7ss7gk8s6ngfbgs4yvimvs7y0p4xjv03lk6")))

(define-public crate-xor-1.3.0 (c (n "xor") (v "1.3.0") (d (list (d (n "clap") (r "^2.21.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "xor-utils") (r "^0.6.0") (d #t) (k 0)))) (h "1dwzp1zbd5rdv94i3wz9g0pa6srxkcc9a3nv6lcwzbzh0x79d37i")))

(define-public crate-xor-1.4.0 (c (n "xor") (v "1.4.0") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "xor-utils") (r "^0.6.0") (d #t) (k 0)))) (h "0znz1nsrlvqhk9x8ykl614pnz9yrzg53p7v08pq71gzahazbp2y1")))

(define-public crate-xor-1.4.1 (c (n "xor") (v "1.4.1") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "xor-utils") (r "^0.6.0") (d #t) (k 0)))) (h "04wczzw25g7bd27r1v6g1az65579hm34imv0mvjb7mzmar8yjjqf")))

(define-public crate-xor-1.4.2 (c (n "xor") (v "1.4.2") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "xor-utils") (r "^0.6.0") (d #t) (k 0)))) (h "0jrclnbwai51h96vrgpg35dl084j9k66c61inn27mjc8jywf9njj")))

(define-public crate-xor-1.4.3 (c (n "xor") (v "1.4.3") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "xor-utils") (r "^0.6.0") (d #t) (k 0)))) (h "1n79zjxidrgls51ydn4xij0s8q02vmz61fqkvxbrfa1fhh82x1vg")))

(define-public crate-xor-1.4.4 (c (n "xor") (v "1.4.4") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "number_prefix") (r "^0.2.7") (d #t) (k 0)) (d (n "xor-utils") (r "^0.6.0") (d #t) (k 0)))) (h "1ld2xi7v6rb20ls56q3m7mrigf80il0dsxdy9ixrnriq170wm2ny")))

(define-public crate-xor-1.4.5 (c (n "xor") (v "1.4.5") (d (list (d (n "base64") (r "~0.6.0") (d #t) (k 0)) (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "number_prefix") (r "^0.2.7") (d #t) (k 0)) (d (n "xor-utils") (r "^0.6.0") (d #t) (k 0)))) (h "0hi4sqfmym6hf2nccqqacw9xa60i05f1m66rw54627jkbymjmx0c")))

