(define-module (crates-io #{3}# x xws) #:use-module (crates-io))

(define-public crate-xws-0.1.0 (c (n "xws") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "envport") (r "^0.1.0") (d #t) (k 0)) (d (n "loginit") (r "^0.1.3") (d #t) (k 0)) (d (n "ratchet_rs") (r "^0.4.1") (f (quote ("ratchet_deflate" "split" "deflate"))) (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("macros" "rt-multi-thread" "rt" "time" "sync"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.14") (f (quote ("net"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "1sksph67i02h2w5lyz3p1cjki1nwhb3w81p0ca039xxbj14l5ax8")))

