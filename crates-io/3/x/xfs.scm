(define-module (crates-io #{3}# x xfs) #:use-module (crates-io))

(define-public crate-xfs-0.1.0 (c (n "xfs") (v "0.1.0") (h "1dnms9jqy1j9bw51mlpcr8laxkdqqy1z5bah27blq0s7pbx6i7zm")))

(define-public crate-xfs-0.2.0 (c (n "xfs") (v "0.2.0") (d (list (d (n "nom") (r "~1.2") (d #t) (k 0)))) (h "0rxrvzwnmihr8jqkfmrcqjca09v4z2zm6ffz3mz4ydg5m8lgnq29")))

