(define-module (crates-io #{3}# x xes) #:use-module (crates-io))

(define-public crate-xes-0.1.0 (c (n "xes") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "roxmltree") (r "^0.17.0") (d #t) (k 0)))) (h "0lbk9yvg9dmwd7d4krnds5afiynf1k55sabg9gkax47yx42rapa5")))

