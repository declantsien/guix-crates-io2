(define-module (crates-io #{3}# x xdk) #:use-module (crates-io))

(define-public crate-xdk-0.1.0 (c (n "xdk") (v "0.1.0") (h "0kiamifndkxhd9q8j4crj3vpyspzqr0zzb05yg74zaxdqsvdbrh4")))

(define-public crate-xdk-0.1.1 (c (n "xdk") (v "0.1.1") (h "1ggx68svdm6gskak7q199icmgivfr9w8xa7bcxqx4w60rxd3mhcm")))

