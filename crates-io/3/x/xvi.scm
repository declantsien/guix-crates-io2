(define-module (crates-io #{3}# x xvi) #:use-module (crates-io))

(define-public crate-xvi-0.0.0 (c (n "xvi") (v "0.0.0") (d (list (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "x11-dl") (r "^2.18.5") (d #t) (k 0)))) (h "0yvwrw377frgq57d8wbna7imxzn0dwvas71mnil9s5g2q3j41hc0")))

