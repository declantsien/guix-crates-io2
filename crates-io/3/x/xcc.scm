(define-module (crates-io #{3}# x xcc) #:use-module (crates-io))

(define-public crate-xcc-0.1.0 (c (n "xcc") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fixedbitset") (r "^0.4.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "136prhz0r3v01w9xslrhypjcaw8cglrib2mk1aaw5xla64x9v4mk")))

