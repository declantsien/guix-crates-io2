(define-module (crates-io #{3}# d dfu) #:use-module (crates-io))

(define-public crate-dfu-0.2.0 (c (n "dfu") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-hex") (r "^0.1") (d #t) (k 0)) (d (n "usbapi") (r "^0.2") (d #t) (k 0)))) (h "1ld0jmsbzrw27z268p8k7c8wp26asw311ws1bn2l4lfda62kpdrd")))

(define-public crate-dfu-0.2.1 (c (n "dfu") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-hex") (r "^0.1") (d #t) (k 0)) (d (n "usbapi") (r "^0.2") (d #t) (k 0)))) (h "0vvkmch145vv8076z4yz3gbj1kd6mvi186mwk454bw94rfa8nsqy")))

(define-public crate-dfu-0.2.2 (c (n "dfu") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-hex") (r "^0.1") (d #t) (k 0)) (d (n "usbapi") (r "^0.2") (d #t) (k 0)))) (h "0gmqrnn0jvgnz3fgr07ag9f4wpdfy55cjd3f0qvy4z4flbdsy0zx")))

(define-public crate-dfu-0.3.0 (c (n "dfu") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-hex") (r "^0.1") (d #t) (k 0)) (d (n "usbapi") (r "^0.3") (d #t) (k 0)))) (h "1pbl94q99g3i7l0hhwbnl27bksb0gvb1pld97cx90fc4aarmwaw1")))

(define-public crate-dfu-0.4.0 (c (n "dfu") (v "0.4.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "usbapi") (r "^0.4") (d #t) (k 0)))) (h "10irs46ssmlqhi4k92yq0fbnb8gf2nd67zfykgxxypzg4pr0vx5y")))

(define-public crate-dfu-0.4.1 (c (n "dfu") (v "0.4.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "usbapi") (r "^0.4") (d #t) (k 0)))) (h "1b8pyll7nnmnl0y1m8plnib9rlpmw7z83avmr5avsrag55kn2h28")))

(define-public crate-dfu-0.4.2 (c (n "dfu") (v "0.4.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "usbapi") (r "^0.4") (d #t) (k 0)))) (h "160vpigsa4clqllq7ra5bnhmla6bm098fsbmhwrvlg3vwjviw7fb")))

