(define-module (crates-io #{3}# d doq) #:use-module (crates-io))

(define-public crate-doq-0.1.0 (c (n "doq") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.26") (d #t) (k 0)) (d (n "close_enough") (r "^0.2") (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.7") (d #t) (k 0)))) (h "0ys4x7bsxq34f27dr77ng5lixd3xgiaivk9cxplzhkl96jpylca3")))

