(define-module (crates-io #{3}# d did) #:use-module (crates-io))

(define-public crate-did-0.1.0 (c (n "did") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)))) (h "0335kdzcvy48nvldx6h1w20xrcp37kdshxi8k4yh62h6wj2sh69g")))

(define-public crate-did-0.2.0 (c (n "did") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)))) (h "0rfdpmv6pm6nxmnckxm1x3j8w234jkghidd13r16dspy2aaqd950")))

(define-public crate-did-0.2.1 (c (n "did") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)))) (h "06rx9xhkbxp0ayc4bs8nxykr1a6bd9xi8mbd01smbbh5mfyp53l7")))

(define-public crate-did-1.0.0 (c (n "did") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "~2.32") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)))) (h "1hapdvbn4jv1fhcvqhc6mfw0vliq9zs5x07zrcq0450w5vq0gphc")))

