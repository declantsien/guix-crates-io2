(define-module (crates-io #{3}# d dmc) #:use-module (crates-io))

(define-public crate-dmc-0.1.0 (c (n "dmc") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.37") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (d #t) (k 0)) (d (n "x11") (r "^2.14.0") (f (quote ("xlib" "glx"))) (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"freebsd\", target_os = \"dragonfly\", target_os = \"openbsd\", target_os = \"netbsd\"))") (k 0)))) (h "1iii3ymj286d9p649ifh4cwwq3zs2l0nx4cddn7918wwldyb2jb0")))

