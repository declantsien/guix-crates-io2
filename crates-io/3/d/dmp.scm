(define-module (crates-io #{3}# d dmp) #:use-module (crates-io))

(define-public crate-dmp-0.1.0 (c (n "dmp") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "08sm3ym7px8bf931ifiy31wrlm07qngwdyvbwd84dw70djqa79ms")))

(define-public crate-dmp-0.1.1 (c (n "dmp") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "1siklnxs8nkpii37j27hki79aqix41kwm1xaiafcx3crlbiqihsq")))

(define-public crate-dmp-0.1.2 (c (n "dmp") (v "0.1.2") (d (list (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "11b3yj0ljabqzpqdqf2sgfngjvdn9sbr55wii5sbp6jwpi5k5iix")))

(define-public crate-dmp-0.1.3 (c (n "dmp") (v "0.1.3") (d (list (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "0an9yidn6f2fx3w9pyrr1q5sf62b95s8pil62m2anl83353y35hp")))

(define-public crate-dmp-0.2.0 (c (n "dmp") (v "0.2.0") (d (list (d (n "urlencoding") (r "^2.1.2") (d #t) (k 0)))) (h "1ndnpqi456ivqfbsflsn1brx91zqba9sh9s9bg6fa9jdlcsi3amz")))

