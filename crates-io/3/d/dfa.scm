(define-module (crates-io #{3}# d dfa) #:use-module (crates-io))

(define-public crate-dfa-0.1.0 (c (n "dfa") (v "0.1.0") (h "0xx93jny5kl6kh52aazhicsv863fj7c6mf8a0jknfplcw5z7bpbn")))

(define-public crate-dfa-0.1.1 (c (n "dfa") (v "0.1.1") (h "0s798m7wi1bn9l3snkp2hslxyg1ji4jh6ngd4hlfw18z5jhjfj03")))

(define-public crate-dfa-0.2.0 (c (n "dfa") (v "0.2.0") (h "1bq9zmvc1ahavfb4z8m7x5bsi28brasngi279ad1nm7lk8wa8s1f")))

