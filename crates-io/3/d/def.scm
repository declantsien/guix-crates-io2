(define-module (crates-io #{3}# d def) #:use-module (crates-io))

(define-public crate-def-1.0.0 (c (n "def") (v "1.0.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i36wyz08qs7d9752j3040dpdbkbg7812s04lw85ajbfj6zf78ys")))

