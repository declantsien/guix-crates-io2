(define-module (crates-io #{3}# d dco) #:use-module (crates-io))

(define-public crate-dco-0.1.0 (c (n "dco") (v "0.1.0") (h "0kn58yhm847xd6j5rbfk02phrvbm1j7cf31frxz6mvylrnx4crlv") (y #t)))

(define-public crate-dco-0.1.1 (c (n "dco") (v "0.1.1") (h "0bcak79pszdci1galpi399dx8n4s2235b052iqrhpx3ci590yjak") (y #t)))

(define-public crate-dco-0.1.2 (c (n "dco") (v "0.1.2") (h "1dm35v2ad7s9mxclmmqvp53vbxrxq4yhqx55ml7richcp8qvpjny")))

(define-public crate-dco-0.1.3 (c (n "dco") (v "0.1.3") (h "0z8zl1abxdvi4jfqdfjifp1p1isig5li02jpm8rz66f2az58r4fs")))

