(define-module (crates-io #{3}# d dum) #:use-module (crates-io))

(define-public crate-dum-0.1.0 (c (n "dum") (v "0.1.0") (d (list (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wj6fyz3gphdmnbxw9kbxw748bpyamblp30vwj8a12izka865w72")))

(define-public crate-dum-0.1.1 (c (n "dum") (v "0.1.1") (d (list (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hv3py8ayrf47m4lm9l0mid4v7zrb012q5dqbn7bvij1gcbrpp27")))

(define-public crate-dum-0.1.2 (c (n "dum") (v "0.1.2") (d (list (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p9mh4pp34cczm0vvfif9i2f6540hd8014q4r08ihw2i02l61pmd")))

(define-public crate-dum-0.1.3 (c (n "dum") (v "0.1.3") (d (list (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wdv2mcihf59f3qwzk4nbclknc70q2ankm8pjn4vr3f8qcwl3by6")))

(define-public crate-dum-0.1.4 (c (n "dum") (v "0.1.4") (d (list (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0q4i6pjh3a9d3qrpi3n231r9dscy49rdq5y8arzfpfaswazmi67w")))

(define-public crate-dum-0.1.5 (c (n "dum") (v "0.1.5") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j0wnilnj2bmr1xh9k1bj1j342injbqk0bmz2cig4qn52322zngv")))

(define-public crate-dum-0.1.6 (c (n "dum") (v "0.1.6") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0swa8lrzrk2mhdcmswc2gz69zl4kdmvy4swnb0x0i9qa98fkfrgx")))

(define-public crate-dum-0.1.7 (c (n "dum") (v "0.1.7") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08hyz19dwql9ffsqjzkx91ggvgs748vd7gh9wml6kczzfg3frsip")))

(define-public crate-dum-0.1.8 (c (n "dum") (v "0.1.8") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j4zg27yc8lf6i67gg41w6va4vcz7w2pynj0v0hxhnkrd3rwmx4j")))

(define-public crate-dum-0.1.9 (c (n "dum") (v "0.1.9") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1i14x8kqagbyp33a515rpfgrm407r34px1ck6pry95zpd8575wwj")))

(define-public crate-dum-0.1.11 (c (n "dum") (v "0.1.11") (d (list (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "120nhbpnn5kkg0jski98ll64ypqfp0hh3fggygxfdchcvxqxf15v")))

(define-public crate-dum-0.1.13 (c (n "dum") (v "0.1.13") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r866jrxkvssy76vls2hwbx8pk85jcdqnb7pj69jcr3qbbj7by0s")))

(define-public crate-dum-0.1.15 (c (n "dum") (v "0.1.15") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0m7wx1crnyyg266l8fnm7vbsrxrbqjddcm73k5ar7rf9dnkh3381")))

(define-public crate-dum-0.1.16 (c (n "dum") (v "0.1.16") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a3q89wdxxji0kfx7s41ld0wlj7xv16x40a00r848wd35qydlcs0")))

(define-public crate-dum-0.1.18 (c (n "dum") (v "0.1.18") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sj38g53cn5zhypyz7hnlzpa985zs6kgfi91b65x6ibbwrfcjlvv")))

(define-public crate-dum-0.1.19 (c (n "dum") (v "0.1.19") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.9.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "053lhzf5pkiipdk6z0kc326g3f7l061ahiad2ddn2fypd4gd1ck1")))

