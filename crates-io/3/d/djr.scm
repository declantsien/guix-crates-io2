(define-module (crates-io #{3}# d djr) #:use-module (crates-io))

(define-public crate-djr-0.0.1 (c (n "djr") (v "0.0.1") (d (list (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "1mzqmgkz4jvz8yycc12wx969ba8v0yr2ywc10l28zc4hq9hiz1xp") (y #t)))

(define-public crate-djr-0.0.2 (c (n "djr") (v "0.0.2") (d (list (d (n "cargo-lock") (r "^8.0.3") (d #t) (k 1)) (d (n "djot") (r "^0.0.1") (d #t) (k 0)))) (h "0pssvccvbm50rr0b86xabp1gyv9cnsk10nb7vhk011axb06rdz3l")))

