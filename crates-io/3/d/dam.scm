(define-module (crates-io #{3}# d dam) #:use-module (crates-io))

(define-public crate-dam-0.1.0 (c (n "dam") (v "0.1.0") (d (list (d (n "checksum") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rexif") (r "^0.3.5") (d #t) (k 0)) (d (n "rustbreak") (r "^2.0.0-rc3") (f (quote ("bin_enc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1n1ibw216lj83cx8g2kzk4yf17wvcqk7562rhw74fzr3ifljjc91")))

(define-public crate-dam-0.1.1 (c (n "dam") (v "0.1.1") (d (list (d (n "checksum") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rexif") (r "^0.3.5") (d #t) (k 0)) (d (n "rustbreak") (r "^2.0.0-rc3") (f (quote ("bin_enc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1aar6dyv9dqp8xnn6adzndx3zvwidhsi1ic5phbc0j386lp155mm")))

(define-public crate-dam-0.1.2 (c (n "dam") (v "0.1.2") (d (list (d (n "checksum") (r "^0.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "rexif") (r "^0.3.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "0ljbyskzpkgr5g7lkmff05h2wdw9gp3an2vnh87lg7w1lb04dk37")))

(define-public crate-dam-0.1.3 (c (n "dam") (v "0.1.3") (d (list (d (n "checksum") (r "^0.2.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)) (d (n "rexif") (r "^0.3.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.16.0") (f (quote ("chrono" "blob" "backup"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.87") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.38") (d #t) (k 0)))) (h "1sw7lg0p5h57gawniax66lxz4dbyllrxfy5x3b7mxd2hsljdx61f")))

