(define-module (crates-io #{3}# d deb) #:use-module (crates-io))

(define-public crate-deb-0.1.0 (c (n "deb") (v "0.1.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 0)))) (h "0hxl2yb0hk8k80k9qfx0x1vs0zrig2w1p21b692fy732mh39bcc6") (y #t)))

(define-public crate-deb-0.2.0 (c (n "deb") (v "0.2.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "sailfish") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1v4186qglg5i7qh7qldp8kxzqv9yrkcf227m4j3v63qx7mqbi5v6") (y #t)))

