(define-module (crates-io #{3}# d div) #:use-module (crates-io))

(define-public crate-div-0.2.0 (c (n "div") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2.68") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.18") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("console" "CssStyleDeclaration" "Document" "Element" "HtmlCollection" "HtmlElement" "HtmlHeadElement" "HtmlScriptElement" "Window"))) (d #t) (k 0)))) (h "1p5a7waqj7l8zq83yr8ava4h3va2xwk90mfvb1a82hhl53qgxgnd")))

(define-public crate-div-0.3.0 (c (n "div") (v "0.3.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("console" "CssStyleDeclaration" "DomTokenList" "Document" "Element" "HtmlElement" "HtmlHeadElement" "HtmlScriptElement" "Window"))) (d #t) (k 0)))) (h "1fkgai91v8pl4f9gkl9s4x29c5c3wydln5pf7l3363qg0szdn11p")))

(define-public crate-div-0.4.0 (c (n "div") (v "0.4.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("console" "CssStyleDeclaration" "DomTokenList" "Document" "Element" "HtmlElement" "HtmlHeadElement" "HtmlScriptElement" "Window"))) (d #t) (k 0)))) (h "118jmxm1dxndj9mikz6x64g0q25xvyn5l2896026pbpz66fhv6fy")))

