(define-module (crates-io #{3}# d dsf) #:use-module (crates-io))

(define-public crate-dsf-0.1.0 (c (n "dsf") (v "0.1.0") (d (list (d (n "audio-duration") (r "^0.1.0") (d #t) (k 0)) (d (n "id3") (r "^0.5.0") (d #t) (k 0)))) (h "10jyvhb6nw4zqjpa2dclllxgk8ad7hvpprjrzf6i9hq3dqlsidrc")))

(define-public crate-dsf-0.2.0 (c (n "dsf") (v "0.2.0") (d (list (d (n "audio-duration") (r "^0.1.0") (d #t) (k 0)) (d (n "id3") (r "^0.5.1") (d #t) (k 0)))) (h "1zj1rvfr4zgfkw9by605wwgrij2n7pc9gifrv7xvqy1pnqzlk3ja")))

(define-public crate-dsf-0.2.1 (c (n "dsf") (v "0.2.1") (d (list (d (n "id3") (r "^0.5.1") (d #t) (k 0)) (d (n "sampled_data_duration") (r "^0.1.0") (d #t) (k 0)))) (h "0lzg8iyra53p3bxlmbm1xp6x5v8l43zxmrhcvq2sqj4cjc5d6kk9")))

(define-public crate-dsf-0.2.2 (c (n "dsf") (v "0.2.2") (d (list (d (n "id3") (r "^1.0.2") (d #t) (k 0)) (d (n "sampled_data_duration") (r "^0.3.1") (d #t) (k 0)))) (h "1kdykaqc5xzxra1w3iriq67kxhw1z2ml9bp501iv654g296gzx10")))

