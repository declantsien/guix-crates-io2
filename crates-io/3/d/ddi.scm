(define-module (crates-io #{3}# d ddi) #:use-module (crates-io))

(define-public crate-ddi-0.1.0 (c (n "ddi") (v "0.1.0") (h "0p4xr7bkkhqwpb49w1s25glbg8nra75ib5q1kikpwhy25ch2nl2w")))

(define-public crate-ddi-0.1.1 (c (n "ddi") (v "0.1.1") (h "0faiy4ahfv6dqvisgnr4jwxy7kprdv6sdyrh4d4hdxnrcry1snck")))

(define-public crate-ddi-0.1.2 (c (n "ddi") (v "0.1.2") (h "1nmwnmn8a19vwnl4yljyg7k5jzddg7fw0a6gg2w69sqcj9vxxx45")))

(define-public crate-ddi-0.2.0 (c (n "ddi") (v "0.2.0") (h "1fzw23rp6sz3p39n1gxpphzgxibakjgy0335x0axiidz659f8p2l") (f (quote (("sync" "std") ("std"))))))

(define-public crate-ddi-0.2.1 (c (n "ddi") (v "0.2.1") (h "1vd7vansgg44q4x3jb7f79ljbn8i7yc0h28m386f35dqkbj4jn6p") (f (quote (("sync" "std") ("std"))))))

