(define-module (crates-io #{3}# d dok) #:use-module (crates-io))

(define-public crate-dok-0.2.0 (c (n "dok") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05p1aa5z7784igc0b5pp7hjgkkvgm2nnq4psj8fg7haa19xd5r5a")))

