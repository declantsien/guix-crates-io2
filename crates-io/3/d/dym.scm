(define-module (crates-io #{3}# d dym) #:use-module (crates-io))

(define-public crate-dym-1.0.0 (c (n "dym") (v "1.0.0") (h "0az7azfvsdi42ws254rbmp9ghxslpac9y5mp2h13xysy0w3p3513")))

(define-public crate-dym-1.0.1 (c (n "dym") (v "1.0.1") (h "0mlfnjyr5c7qm079njicb8k2xr6dnhlknpnhrrd45jqsz7gkn7qn")))

