(define-module (crates-io #{3}# d dtt) #:use-module (crates-io))

(define-public crate-dtt-0.0.1 (c (n "dtt") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "1yp9aaphgqgqx54i7l30zn4s1dcfgiprxiiw2sbby9xphxwaiaqx") (f (quote (("default")))) (r "1.67.0")))

(define-public crate-dtt-0.0.2 (c (n "dtt") (v "0.0.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.17") (d #t) (k 0)))) (h "19sxhwb6hirmmpq72qfvb85g3hf987c8b05mb4jzbnd9cp05wpwa") (f (quote (("default")))) (r "1.67")))

(define-public crate-dtt-0.0.3 (c (n "dtt") (v "0.0.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (d #t) (k 0)))) (h "0ajwr2w6fq6nwfnag1134kp26cs0xfvxchgidmq1f1j23mlwbf77") (f (quote (("default")))) (r "1.67")))

(define-public crate-dtt-0.0.4 (c (n "dtt") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.21") (d #t) (k 0)))) (h "05nxjikm6si22j2pp2pd2wbz0dm36wbqi7ld3ww8b4d2yc5p8x72") (f (quote (("default")))) (r "1.69.0")))

(define-public crate-dtt-0.0.5 (c (n "dtt") (v "0.0.5") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.30") (d #t) (k 0)))) (h "127wb7r8yzwv2pf4y2g8n4y1p8czc9sdmmhp9kf8hs6pwagdvcnn") (f (quote (("default")))) (r "1.71.1")))

(define-public crate-dtt-0.0.6 (c (n "dtt") (v "0.0.6") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "time") (r "^0.3.36") (d #t) (k 0)))) (h "0lf2qchjha4r3amfrysql5qkn2v85l1l1h4s1qhak2p4kjj1s691") (f (quote (("default")))) (r "1.60.0")))

