(define-module (crates-io #{3}# d ded) #:use-module (crates-io))

(define-public crate-ded-0.1.0 (c (n "ded") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "schnellru") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 2)))) (h "01ic74q1nhs7pqj0ag8l8l6vcsqpyancsjwwzian66fl1kgzjyk7") (r "1.65")))

