(define-module (crates-io #{3}# d dss) #:use-module (crates-io))

(define-public crate-dss-0.1.0 (c (n "dss") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.5") (d #t) (k 0)))) (h "1x7dv9h2bw56ay2849wvga3345z7hv8xscrzzbnga917mk6d0ny1")))

(define-public crate-dss-0.1.1 (c (n "dss") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.5") (d #t) (k 0)))) (h "1c21zsdjc1lg284qvjydp0pw8dzi552dfmxm2w0m0k8a27dfnkw3")))

(define-public crate-dss-0.1.2 (c (n "dss") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.5") (d #t) (k 0)))) (h "05xr4sarvnvrbyx35rvnchcx0dlm8dizf96g95m8ss9cgwhc2b64")))

