(define-module (crates-io #{3}# d dpg) #:use-module (crates-io))

(define-public crate-dpg-0.1.0 (c (n "dpg") (v "0.1.0") (d (list (d (n "clipboard") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0wk08idi43yb523gdqznswnxl3sqkq53l294lhc9bwi1c9wkr7cf")))

(define-public crate-dpg-0.1.1 (c (n "dpg") (v "0.1.1") (d (list (d (n "clipboard") (r "^0.4.6") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0571mvjdlspiy57lxnx9aws9gwcw6gqj8aphx22psid8diapa9g3")))

