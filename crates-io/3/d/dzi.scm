(define-module (crates-io #{3}# d dzi) #:use-module (crates-io))

(define-public crate-dzi-0.1.0 (c (n "dzi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1dz0ldkrq4ljc7r683akq001h7ichwglwbs4j0cfwrzyk9bl2wix")))

(define-public crate-dzi-0.1.1 (c (n "dzi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)))) (h "1yzqmf7p4ql7blxr7w7pj4nxhwx9sjmk2vm98dswppnpvsj1x6lw")))

(define-public crate-dzi-0.2.0 (c (n "dzi") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1byv4wnkk9fbqp8jkfklvzkf38nlj3093vnbz1bmgzhqz7j2ig9s")))

(define-public crate-dzi-0.2.1 (c (n "dzi") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "06y6d01c0lbkbzvlwjq7q7i0qwad3vnqi86gqlxhgy8lla4pwk84")))

(define-public crate-dzi-0.2.2 (c (n "dzi") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dkgg0g67w0s5x3vdxk357c9fydzvk65v9d6v8f5xs2x9wzpk4y0")))

(define-public crate-dzi-0.2.3 (c (n "dzi") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jkr977pdkrc42jmgcmzvi50cqfgrn2560hacjn65csdx1i8cxmf")))

