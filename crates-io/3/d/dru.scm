(define-module (crates-io #{3}# d dru) #:use-module (crates-io))

(define-public crate-dru-0.1.0 (c (n "dru") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "async-recursion") (r "^0.3.2") (d #t) (k 0)) (d (n "async-std") (r "^1.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("color"))) (d #t) (k 0)) (d (n "relative-path") (r "^1.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "0f1b31a9njjiysc6hwy209ahkbiawfcjzd3avqhznk9hxpb4yjrl")))

