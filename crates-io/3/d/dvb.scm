(define-module (crates-io #{3}# d dvb) #:use-module (crates-io))

(define-public crate-dvb-0.1.0 (c (n "dvb") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.10") (d #t) (k 0)))) (h "0a85b6zz9vgxpmlm0kvj8cxx0x3q76r06y3brimchm8n3lc4wajr")))

(define-public crate-dvb-0.2.0 (c (n "dvb") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.10") (d #t) (k 0)) (d (n "multimap") (r "^0.3") (d #t) (k 0)))) (h "111vy6ilk951dyyawdaf7fn2h2rkrd7q6pj1xis4j3rpn8pw5i96")))

(define-public crate-dvb-0.2.1 (c (n "dvb") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.5") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.10") (d #t) (k 0)) (d (n "multimap") (r "^0.3") (d #t) (k 0)))) (h "0pr7asfrhb30kbadvnws43n05spc8zm26aigfbig600a9w4wi25h")))

(define-public crate-dvb-0.3.0 (c (n "dvb") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.6") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "json") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "multimap") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1fsn0j1m92xhb27n3kw3fs0mxnp865gpnhbc2k8zns5y49hp9wab")))

(define-public crate-dvb-0.4.0 (c (n "dvb") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z30lm7p944pdbigz7gz4ilma5f25dmggawhf1lh7pddp3gq127p")))

(define-public crate-dvb-0.4.1 (c (n "dvb") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock" "std" "serde"))) (k 0)) (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "15r8dm6x7y2962904zvripkczwasl3q7mvvqnrnh57yvs10xc9d5")))

