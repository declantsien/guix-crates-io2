(define-module (crates-io #{3}# d dbl) #:use-module (crates-io))

(define-public crate-dbl-0.1.0 (c (n "dbl") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.9") (d #t) (k 0)))) (h "071cqfpzbw5wa24r3pfccyqjqabkylxgik3484f9c2h6d5xi23lj")))

(define-public crate-dbl-0.2.0 (c (n "dbl") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "1ahf4w5lwxp11kkbqbvyc7gn6c9s25s5wy0jgc6mc48yaqxv2h3c")))

(define-public crate-dbl-0.2.1 (c (n "dbl") (v "0.2.1") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "15873pnrnribm5mq49iwcqz4224dff787nf440191k6yflxj1p18")))

(define-public crate-dbl-0.3.0 (c (n "dbl") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1pihf6zrzncbs3lsyqkzxxxqmjf8rfpwvs1sg8nmz8cv7df18d97")))

(define-public crate-dbl-0.3.1 (c (n "dbl") (v "0.3.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1cyr14ph8dxg36lrj1kndj55n9fh2ilkpdpw925542azgdl9grrp")))

(define-public crate-dbl-0.3.2 (c (n "dbl") (v "0.3.2") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1ng33ncai537xlxfy9r4v24parm9mf7zwiwkixq7d0qmj6kka9xx")))

(define-public crate-dbl-0.4.0-pre.1 (c (n "dbl") (v "0.4.0-pre.1") (d (list (d (n "hybrid-array") (r "=0.2.0-pre.6") (d #t) (k 0)))) (h "05h7s2ay3zdkr0j33mrp775nnrgm9mdq1bagvv6j07ccp8r3z1gv") (r "1.65")))

(define-public crate-dbl-0.4.0-pre.2 (c (n "dbl") (v "0.4.0-pre.2") (d (list (d (n "hybrid-array") (r "=0.2.0-pre.7") (d #t) (k 0)))) (h "08zc2553mgairhwg7z8rb9b6k67n2nljqfhk8m3a7g77534br06b") (r "1.65")))

(define-public crate-dbl-0.4.0-pre.3 (c (n "dbl") (v "0.4.0-pre.3") (d (list (d (n "hybrid-array") (r "=0.2.0-pre.8") (d #t) (k 0)))) (h "1gm6gzaj86skm2r2n37h259wcs12v5hak26w42cfgg7s7xybx75y") (r "1.65")))

(define-public crate-dbl-0.4.0-pre.4 (c (n "dbl") (v "0.4.0-pre.4") (d (list (d (n "hybrid-array") (r "^0.2.0-rc.0") (d #t) (k 0)))) (h "0wynkgiw2kkdag6h6mp9b9nh96blbb9gz5f2rqiwz7ybdfijawcp") (r "1.65")))

