(define-module (crates-io #{3}# d di2) #:use-module (crates-io))

(define-public crate-di2-0.0.0 (c (n "di2") (v "0.0.0") (h "0q9g10hi332bv0xg021rcx0j0nqhkz6210mkbnskk6hby14h7sa9") (y #t)))

(define-public crate-di2-0.0.1 (c (n "di2") (v "0.0.1") (h "0fcgyf57pvz9pzx7a272za07awq2palh1r193rba65zpgf69lqfx") (y #t)))

(define-public crate-di2-0.8.0 (c (n "di2") (v "0.8.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "15aciwly7a3wxwk0vdlg80zs5fa7aca6flr6pavdvjiy7qkcsfnp") (y #t)))

