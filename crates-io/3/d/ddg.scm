(define-module (crates-io #{3}# d ddg) #:use-module (crates-io))

(define-public crate-ddg-0.1.0 (c (n "ddg") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^0.7.11") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.11") (d #t) (k 1)) (d (n "serde_json") (r "^0.7.3") (d #t) (k 0)) (d (n "serde_xml") (r "^0.7.1") (d #t) (k 0)))) (h "0rjcg7iz11pr49l8xpla5ia25lvllgzpwljpma2mj8gx3gndslnd")))

(define-public crate-ddg-0.2.0 (c (n "ddg") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "00nag4v164192f41rcs8p183q6hjqy9hg7sdhyxwmix3gihalgk2") (f (quote (("default" "reqwest"))))))

(define-public crate-ddg-0.2.1 (c (n "ddg") (v "0.2.1") (d (list (d (n "hyper") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0d7g9b7q9xc4j699wnl1xqzgpqkfh0w9xhyh5lxgkl4miiyg2l7x") (f (quote (("default" "reqwest"))))))

(define-public crate-ddg-0.2.2 (c (n "ddg") (v "0.2.2") (d (list (d (n "hyper") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "0qkdjqws4pmgr2v3pzf1irhn2swx4qhvlnasl1adhrng0rbxf6vg") (f (quote (("default" "reqwest"))))))

(define-public crate-ddg-0.3.0 (c (n "ddg") (v "0.3.0") (d (list (d (n "hyper") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0jyb5hrycbp6ns0cl909nr00300xmsgspbw1s9q2v8l9c0vxsd8r") (f (quote (("default" "reqwest"))))))

(define-public crate-ddg-0.4.0 (c (n "ddg") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0d64l3x0ffny7zl2hqyc7pp45dh4bfdjvl6vbmghj0w4jpj177ks")))

(define-public crate-ddg-0.4.1 (c (n "ddg") (v "0.4.1") (d (list (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "06dl9nswc9b9vz2sfkcmvz6l6z2l1vmakp63pb6w55nzf1wjlksb")))

(define-public crate-ddg-0.4.2 (c (n "ddg") (v "0.4.2") (d (list (d (n "reqwest") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0q4kb75sk4svhbwxcfk9mgy39w74vq18wbr3g8fm6122db9w4y53")))

(define-public crate-ddg-0.5.0 (c (n "ddg") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "09mxqdqs0mkixqwmjvwbafv8kl3y1cr2iy938xkpl3hpyfjnbrpr")))

