(define-module (crates-io #{3}# d dlg) #:use-module (crates-io))

(define-public crate-dlg-0.1.0 (c (n "dlg") (v "0.1.0") (d (list (d (n "common_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "logos") (r "^0.12.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "198mzj95admn595nsxilkpd7sqfxapbynlvxcm56by61s3pgk14m")))

