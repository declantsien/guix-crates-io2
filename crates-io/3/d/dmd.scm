(define-module (crates-io #{3}# d dmd) #:use-module (crates-io))

(define-public crate-dmd-0.1.0 (c (n "dmd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dmdlib") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "requestty") (r "^0.1.1") (d #t) (k 0)))) (h "1j1n3p6cgr96q9qzhydb89cmvmrxb651yvcg9nzgr9d9wghdhalz") (y #t)))

