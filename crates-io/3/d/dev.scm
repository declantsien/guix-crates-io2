(define-module (crates-io #{3}# d dev) #:use-module (crates-io))

(define-public crate-dev-0.0.2 (c (n "dev") (v "0.0.2") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "err") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "00446pq26rnk1k8ybz4xnnpb9xbd993814r7f6ih51y71fhbwd7v")))

(define-public crate-dev-0.0.3 (c (n "dev") (v "0.0.3") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "err") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0s98pzpfa81bjmx97054spk5xnhhd7y1y53crbrlc6kxjjmy6hsx")))

(define-public crate-dev-0.0.4 (c (n "dev") (v "0.0.4") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "err") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "18nyvjhyaxrsw6fm3756iddyhxp29lkv2x77zlqqk6x1fpy1fbp7")))

(define-public crate-dev-0.0.5 (c (n "dev") (v "0.0.5") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "err") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1gixl4s7ibjqh4mqg7h9pm5k4n15ylq9jy0r2cskb9gs3v83i5f5")))

(define-public crate-dev-0.0.6 (c (n "dev") (v "0.0.6") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "err") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1m7qsd1za4zxlwi1vsmbwk6pr7l5zj18mbxq4l8m7gmyrzy2b359")))

(define-public crate-dev-0.0.7 (c (n "dev") (v "0.0.7") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "err") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0hrhqdrn0nc519vjyqqhvlz39lk4j1p9ma69150i3ccphqw66swb")))

(define-public crate-dev-0.0.8 (c (n "dev") (v "0.0.8") (d (list (d (n "env_logger") (r "*") (d #t) (k 2)) (d (n "err") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "nix") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "06m4i9qxmzd6cqa0ghsmbzbvfbr3m9yplv70h3ka9xrwyw961qx9")))

(define-public crate-dev-0.1.0 (c (n "dev") (v "0.1.0") (d (list (d (n "err") (r "0.*") (d #t) (k 0)) (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "nix") (r "0.*") (d #t) (k 0)))) (h "1w5myvbljlhw1ddlnh6hwva6dr7r8qwvg8wabrlf3lq36953ad5m")))

