(define-module (crates-io #{3}# d dfc) #:use-module (crates-io))

(define-public crate-dfc-0.1.0 (c (n "dfc") (v "0.1.0") (h "1g5yd6fdxpicynpzbmkxd8h0sd9xdqicf6q355nqilvvqf0c16iy")))

(define-public crate-dfc-0.1.1 (c (n "dfc") (v "0.1.1") (h "10zi2f6kwmj7q5dgn244b8vw8wagyv72x3ggjjyzrh2g2k38ysxg")))

(define-public crate-dfc-0.1.2 (c (n "dfc") (v "0.1.2") (h "1lw3knzxp6pa3m5xjz0a0b3r23wjy0fwwhpwmdyd52l9zqn2k349")))

(define-public crate-dfc-0.1.3 (c (n "dfc") (v "0.1.3") (h "12lgwv83n8ybsj65yrk5y6in3pww7lylwkdj4b0iqqzkdvk6zb5k")))

(define-public crate-dfc-0.1.4 (c (n "dfc") (v "0.1.4") (h "07rysrshaxk3i0bkyzmnvq4h3lvk1706rdpzg1amaws2jnicgs5d")))

(define-public crate-dfc-0.1.5 (c (n "dfc") (v "0.1.5") (h "0bip52gnkslyf2wxja81a1973dkhqzkfdf61yc7vnd8sm3sj9jn2")))

