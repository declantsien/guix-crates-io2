(define-module (crates-io #{3}# d dzl) #:use-module (crates-io))

(define-public crate-dzl-0.1.0 (c (n "dzl") (v "0.1.0") (d (list (d (n "time") (r "^0.3.13") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "0fdj0jz4n19vryhmn21v3pifv9rmw8ri0m3yya2l3xmihyg7nndd")))

(define-public crate-dzl-0.1.1 (c (n "dzl") (v "0.1.1") (d (list (d (n "time") (r "^0.3.13") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "1w6axsgfl4wii3rpc8rqrkfc7gnmg3q8plpfp33ir4cwim0njmfm")))

(define-public crate-dzl-0.1.2 (c (n "dzl") (v "0.1.2") (d (list (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)) (d (n "time") (r "^0.3.13") (f (quote ("local-offset"))) (d #t) (k 0)))) (h "066vi2pbihnbpmrn9ngi4x4mz1zrk29j27rdyb3mbwq8541m1flf")))

(define-public crate-dzl-0.2.0 (c (n "dzl") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "time") (r "^0.3.13") (f (quote ("local-offset"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "02hgag245m0kgm7iai2j9b66xvnvpi6z6sdd4cvpqizbr0j6pnyc")))

(define-public crate-dzl-0.2.1 (c (n "dzl") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)) (d (n "time") (r "^0.3.13") (f (quote ("local-offset"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "04bd315gc6dsnfh2blicydn0n33ncxfc0lzgayk26b9ijh7y249b")))

