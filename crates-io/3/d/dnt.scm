(define-module (crates-io #{3}# d dnt) #:use-module (crates-io))

(define-public crate-dnt-0.1.0 (c (n "dnt") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0avj3qj6lgdwjrxik0mvnh58dfxqj9h1vp2rap5rvy05harhszfl")))

(define-public crate-dnt-0.1.1 (c (n "dnt") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0al40gkrj6ha5w1jmazim2ay3b41vscz1kkv0mf4qcrychgikfrf")))

