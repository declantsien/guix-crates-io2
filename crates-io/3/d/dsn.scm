(define-module (crates-io #{3}# d dsn) #:use-module (crates-io))

(define-public crate-dsn-0.1.0 (c (n "dsn") (v "0.1.0") (h "1s80d790kin419xkpl5l1bpbr9mwmlmzi7x7zmzj56mnjin7sbps")))

(define-public crate-dsn-0.1.1 (c (n "dsn") (v "0.1.1") (h "0qxxdl9m7jyv1nl0krrqsxayfj7wy1jy47kisdxxgv63vycxgk31")))

(define-public crate-dsn-0.1.2 (c (n "dsn") (v "0.1.2") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0p26khakk3q7ccqyy7k2yb5njwwlcp6h2pw0ip95vijxpxhvsvd3")))

(define-public crate-dsn-0.2.0 (c (n "dsn") (v "0.2.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0kv8nyjgcp5cbrnfrb3lghg78kgmrh6aq4fc501c8185zvy9fhdr")))

(define-public crate-dsn-0.2.1 (c (n "dsn") (v "0.2.1") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1cisjmlbpmi4g5ny98mr3cnckprgkc1q1z2g56rn0an0dpya42dw")))

(define-public crate-dsn-0.3.0 (c (n "dsn") (v "0.3.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1j69vax5s7hy5xr0kgh750hv4ibcf3snvsg80d5i9yqc554bxlkk")))

(define-public crate-dsn-0.4.0 (c (n "dsn") (v "0.4.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1ga51xxh4wax96q5rinqw11nmqybd8pkwcvqa2g08wzfkgfj3ywm")))

(define-public crate-dsn-0.4.1 (c (n "dsn") (v "0.4.1") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1n6h82m551qx9iqp0k9lglcgb5209s1fbhmfijgrgslyk3qhyvxr")))

(define-public crate-dsn-1.0.0 (c (n "dsn") (v "1.0.0") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1g60g2k7xkxbzi3ggyjail2cp60x3vvnlqpw8285i5c8f0yqrdcp")))

(define-public crate-dsn-1.0.1 (c (n "dsn") (v "1.0.1") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1q7vv53i26s9rja75f7jqn7yqc9sxhr16vmsmm3ajvxsql9z84s1")))

(define-public crate-dsn-1.0.2 (c (n "dsn") (v "1.0.2") (d (list (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0snd8m4jqa58ysh154b9ax4c1sxsf040sbzk0iwl3r2fv9sq8gxz")))

