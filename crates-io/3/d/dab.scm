(define-module (crates-io #{3}# d dab) #:use-module (crates-io))

(define-public crate-dab-0.1.0 (c (n "dab") (v "0.1.0") (h "0bxp5zfafawk92xdiw5g915zxlndxn0361kqh4r24590jhs4n8k1") (y #t)))

(define-public crate-dab-0.2.0 (c (n "dab") (v "0.2.0") (d (list (d (n "cargo_toml") (r "^0.11.5") (d #t) (k 0)))) (h "1yakfslkr1iy1md34m9phwyfqp3f87s6lck04c8w0ml83lic0gh8")))

