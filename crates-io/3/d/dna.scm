(define-module (crates-io #{3}# d dna) #:use-module (crates-io))

(define-public crate-dna-0.1.1 (c (n "dna") (v "0.1.1") (h "0ps9kl9q3d6i3lpm1ivphwvpjpbqhv77qv38x4cygfzsggjga872")))

(define-public crate-dna-0.1.2 (c (n "dna") (v "0.1.2") (h "00x03lbv889vlbvf463mg4rfqi5663yihb9myi2z2gbyybjm3mgw")))

(define-public crate-dna-0.1.3 (c (n "dna") (v "0.1.3") (h "1na08hrq9sgac27b22swqmxip9sh4rjsac46spanaj8d233n42k5")))

