(define-module (crates-io #{3}# d dbg) #:use-module (crates-io))

(define-public crate-dbg-0.0.1 (c (n "dbg") (v "0.0.1") (h "1xgmrascl4sw58rng612y9yr7h5z6i96pz9hiym6n919xa6a4xrk")))

(define-public crate-dbg-1.0.0 (c (n "dbg") (v "1.0.0") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1gzrrm0rk6qziwzlifhbkrhz0mfqcpnfi1mvd3w4xnlawi89v4bb")))

(define-public crate-dbg-1.0.1 (c (n "dbg") (v "1.0.1") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0cwh1sc179ynkxk8jl6glca2xdzgb4z56a3wdsi73983yavs0vpq")))

(define-public crate-dbg-1.0.2 (c (n "dbg") (v "1.0.2") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0qn757njwca10qybbqy25071q6q8cqrxnvaq8bry85a19byxn3pm")))

(define-public crate-dbg-1.0.3 (c (n "dbg") (v "1.0.3") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1c66jpa69h2dv84aqvixv1zydr1qqwx215fnhynag45jwy51f1vm")))

(define-public crate-dbg-1.0.4 (c (n "dbg") (v "1.0.4") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "08djm30k4lsw6a9h1yclr6bck8z7l7wpr6fmrsnxgsg02f2ihxs6")))

