(define-module (crates-io #{3}# d d20) #:use-module (crates-io))

(define-public crate-d20-0.1.0 (c (n "d20") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1xdm9fr7yfjk4ws5p5hq5w7pryv118qq12nj8ihklwpnlc3117ic")))

