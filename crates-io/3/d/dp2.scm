(define-module (crates-io #{3}# d dp2) #:use-module (crates-io))

(define-public crate-dp2-0.0.0 (c (n "dp2") (v "0.0.0") (h "06mla0labh0n8zfgrh1pmynhrkxa4rf9q1lmca4gih5jnxn6l7wg") (y #t)))

(define-public crate-dp2-0.0.1 (c (n "dp2") (v "0.0.1") (h "01kfp7s54n08h7n9sl9ms0xhxvf0jma1qn415sx3xk3zjhvlhbfg") (y #t)))

(define-public crate-dp2-0.8.0 (c (n "dp2") (v "0.8.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "0xxlhc1l7nh6bqvx9a55xcyviz3mywj2mrlrpl3rrmvmz2g07lcl") (y #t)))

