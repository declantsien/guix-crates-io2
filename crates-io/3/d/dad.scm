(define-module (crates-io #{3}# d dad) #:use-module (crates-io))

(define-public crate-dad-0.1.0-alpha (c (n "dad") (v "0.1.0-alpha") (d (list (d (n "axum") (r "^0.6.18") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16sn9cwzfc7b7pmdxyfiiifl7c3kyyq876shjjysaz69860h1485")))

