(define-module (crates-io #{3}# d daa) #:use-module (crates-io))

(define-public crate-daa-0.0.0 (c (n "daa") (v "0.0.0") (h "02igq7x7hnyj3972rl33dn14z5y00wpzryndvjwmd37di9f4x1fx")))

(define-public crate-daa-0.1.0 (c (n "daa") (v "0.1.0") (d (list (d (n "crypto-mac") (r "^0.7") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "des") (r "^0.2") (d #t) (k 0)))) (h "0qbgdmcx6krgrjhdycknc9i9p921k1mxn8hp277idzbmbybk3al9")))

(define-public crate-daa-0.2.0 (c (n "daa") (v "0.2.0") (d (list (d (n "crypto-mac") (r "^0.8") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "des") (r "^0.4") (d #t) (k 0)))) (h "1m9wccf40asy13f9vawn4s75gq8fsg5li4p55ia59hr8fvn5s4g2")))

(define-public crate-daa-0.3.0 (c (n "daa") (v "0.3.0") (d (list (d (n "crypto-mac") (r "^0.9.1") (f (quote ("block-cipher"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.9.1") (f (quote ("dev"))) (d #t) (k 2)) (d (n "des") (r "^0.5") (d #t) (k 0)))) (h "0hrcz581pab72y2l1l4rjx1c7r47m5n5y2gi5ncbav6xgzchkpxk") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-daa-0.4.0 (c (n "daa") (v "0.4.0") (d (list (d (n "crypto-mac") (r "^0.10") (f (quote ("cipher"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "des") (r "^0.6") (d #t) (k 0)))) (h "1mk8wfrm9zbxl154nf7m0bv3zhr2a4vk77d83rxbm8v8pnwpsdhf") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-daa-0.4.1 (c (n "daa") (v "0.4.1") (d (list (d (n "crypto-mac") (r "^0.10") (f (quote ("cipher"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "des") (r "^0.6") (d #t) (k 0)))) (h "14w3y6ahdzgab81d873pijh9jc70ax17his3kx6qg0wzd4n0p9ca") (f (quote (("std" "crypto-mac/std"))))))

(define-public crate-daa-0.5.0 (c (n "daa") (v "0.5.0") (d (list (d (n "crypto-mac") (r "^0.11") (f (quote ("cipher"))) (d #t) (k 0)) (d (n "crypto-mac") (r "^0.11") (f (quote ("dev"))) (d #t) (k 2)) (d (n "des") (r "^0.7") (d #t) (k 0)))) (h "0dy1rvfv0sxzb77i3pw9pmsf5g9rv7l7pdc5v83lfn9fl0m06bzs") (f (quote (("std" "crypto-mac/std"))))))

