(define-module (crates-io #{3}# d dpr) #:use-module (crates-io))

(define-public crate-dpr-0.1.0 (c (n "dpr") (v "0.1.0") (d (list (d (n "idata") (r "^0.1.2") (d #t) (k 0)) (d (n "im") (r "^14.3.0") (d #t) (k 0)))) (h "1yy1783pdn7zkszmxalvf2k2rgf3xlkl3lpfz4p0ysm38qy6qiwb")))

(define-public crate-dpr-1.0.0 (c (n "dpr") (v "1.0.0") (d (list (d (n "idata") (r "^0.1.2") (d #t) (k 0)) (d (n "im") (r "^14.3.0") (d #t) (k 0)))) (h "1c3x735v530v2w1d753czmmgnhap7vyyw8ns6ci1c88xlvmjp2hf")))

