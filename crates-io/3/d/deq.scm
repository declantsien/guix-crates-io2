(define-module (crates-io #{3}# d deq) #:use-module (crates-io))

(define-public crate-deq-0.1.0 (c (n "deq") (v "0.1.0") (d (list (d (n "deq_core") (r "^0.1.0") (d #t) (k 0)) (d (n "deq_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1ml5f90m8wn4a2xhs0py7aqj0y5lk4mk6dbj4yibyx49hsh3n7j6")))

(define-public crate-deq-0.1.1 (c (n "deq") (v "0.1.1") (d (list (d (n "deq_core") (r "^0.1.0") (d #t) (k 0)) (d (n "deq_macros") (r "^0.1.0") (d #t) (k 0)))) (h "1z9cs0gw0ws7ia88lxb09ph1aq2vvx1j028aawyyl9mxs4m1np1b")))

(define-public crate-deq-0.1.2 (c (n "deq") (v "0.1.2") (d (list (d (n "deq_core") (r "^0.1.1") (d #t) (k 0)) (d (n "deq_macros") (r "^0.1.1") (d #t) (k 0)))) (h "1s0dckwfqmc2398jkajixnbn67lm9nwh9p1p4hbi8zjr5ga40ps9")))

(define-public crate-deq-0.1.3 (c (n "deq") (v "0.1.3") (d (list (d (n "deq_core") (r "^0.1.1") (d #t) (k 0)) (d (n "deq_macros") (r "^0.1.1") (d #t) (k 0)))) (h "0azs9yq0dqv5v8icgj5d045xsgxma5n20n76f3z2p233d8y8ihgc")))

(define-public crate-deq-0.1.5 (c (n "deq") (v "0.1.5") (d (list (d (n "deq_core") (r "^0.1.5") (d #t) (k 0)) (d (n "deq_macros") (r "^0.1.5") (d #t) (k 0)))) (h "12h3ypyi0rhdwcxq37447dnffmi5vvbj58ckgfjzilmpvh0vb63h")))

(define-public crate-deq-0.2.0 (c (n "deq") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1kza4hgg21n6q41mahbx4bgxy47bdar676w7j3ww4gwjhr83xld5") (f (quote (("serde_skip_history" "serde"))))))

(define-public crate-deq-0.2.1 (c (n "deq") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0licavx35gbb2ndmdgd1qnzrx0s3dp3x8q6hl6bfsynmh8rq98xm") (f (quote (("serde_skip_history" "serde"))))))

