(define-module (crates-io #{3}# d ddc) #:use-module (crates-io))

(define-public crate-ddc-0.1.0 (c (n "ddc") (v "0.1.0") (d (list (d (n "mccs") (r "~0.0.2") (d #t) (k 0)))) (h "1ml17qza9w0xnkr5fmbdnxzfiwbq7fdirjx1z0sqcbi6d8iqnhq9")))

(define-public crate-ddc-0.2.0 (c (n "ddc") (v "0.2.0") (d (list (d (n "mccs") (r "^0.1.0") (d #t) (k 0)))) (h "073x5wavf3i925pfwfdi44vlwpil6azldf27y6c7y8h24213kh3v")))

(define-public crate-ddc-0.2.1 (c (n "ddc") (v "0.2.1") (d (list (d (n "mccs") (r "^0.1.0") (d #t) (k 0)))) (h "0a0pxv4jmg92yxqjc6snxw708xqswajajnw6gp4sqd4fqjyj1kl0")))

(define-public crate-ddc-0.2.2 (c (n "ddc") (v "0.2.2") (d (list (d (n "mccs") (r "^0.1.0") (d #t) (k 0)))) (h "1if6nf8mkv49fls7z47931ps7l84pyxh5jqpmnmw83rj7v2z4sds")))

