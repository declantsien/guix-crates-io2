(define-module (crates-io #{3}# d dmx) #:use-module (crates-io))

(define-public crate-dmx-0.1.0 (c (n "dmx") (v "0.1.0") (d (list (d (n "dmx-serial") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)))) (h "10k11qjswnr9wp9qfm6qq16blmw97q703knsmyy1rn8jzskr6frq")))

(define-public crate-dmx-0.2.0 (c (n "dmx") (v "0.2.0") (d (list (d (n "dmx-serial") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)))) (h "1qic5icmx8200fyn14fpi1xjrdzhsn2549xlsm3804kwamz5wiw2")))

(define-public crate-dmx-0.2.1 (c (n "dmx") (v "0.2.1") (d (list (d (n "dmx-serial") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)))) (h "1cfyi1fdlyvgsrr37qgg1r37sp9bkacyh1glcy8x75w4sxi404gi")))

