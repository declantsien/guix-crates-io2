(define-module (crates-io #{3}# d dq3) #:use-module (crates-io))

(define-public crate-dq3-0.1.0 (c (n "dq3") (v "0.1.0") (d (list (d (n "gnuplot") (r "^0.0.35") (d #t) (k 0)) (d (n "num") (r "^0.2.1") (d #t) (k 0)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 0)))) (h "1r7prrg9myf7ywqnnxswpr85jxgwcwcxy3y64jvb5y2c9ymkyd7m")))

