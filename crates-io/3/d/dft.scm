(define-module (crates-io #{3}# d dft) #:use-module (crates-io))

(define-public crate-dft-0.0.1 (c (n "dft") (v "0.0.1") (h "0r3l1zbbfcn48yns8p2kwv4nwcw8i2bqk1ya8n4n392ijxrb96ka")))

(define-public crate-dft-0.2.0 (c (n "dft") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)))) (h "09zvc58vxlgy6hs77nqx68lbmk4087malbi0g28i4kirchvncvi3")))

(define-public crate-dft-0.2.1 (c (n "dft") (v "0.2.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "complex") (r "*") (d #t) (k 0)))) (h "0w14g02jcq10120c5raw4wp9fzgc9ljnnd3cs14gigrcly64b9ns")))

(define-public crate-dft-0.2.2 (c (n "dft") (v "0.2.2") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "complex") (r "^0.7") (d #t) (k 0)))) (h "01q9y8ybkcz40as5ywcs2y0bfnb5p2x97hg6pzvkh8bkm99w82n2")))

(define-public crate-dft-0.3.0 (c (n "dft") (v "0.3.0") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "09lqidlczdq8p3j2zjv5mwv314l6pw3v66yq897xs7vkr7qw18md")))

(define-public crate-dft-0.4.0 (c (n "dft") (v "0.4.0") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "133b2p4l2wczhillzrp9c0i3rdr4kknw6kqf2mlicswna4z1yf9d")))

(define-public crate-dft-0.4.1 (c (n "dft") (v "0.4.1") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "1lirdixqv1wgn4379si0j29l16282zf4y3zp80kpvm5nsv77h8a7")))

(define-public crate-dft-0.4.2 (c (n "dft") (v "0.4.2") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "num") (r "^0.1") (f (quote ("complex"))) (k 0)))) (h "0p4l2ks77m84j0glh662ip0bkh8731i47qa4vrzf6kq7v06hfl3v")))

(define-public crate-dft-0.4.3 (c (n "dft") (v "0.4.3") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (k 0)))) (h "1lksl91a41sf4wq0ma1r8wr3w842xrq23z7dy5cwnyw07gmb50rq")))

(define-public crate-dft-0.5.0 (c (n "dft") (v "0.5.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-complex") (r "^0.1") (k 0)) (d (n "num-traits") (r "^0.1") (k 0)))) (h "00hcndnc3sil60dlwic2y1gbcvbdprk3m6l1z8l289ldkr9j6gn9")))

(define-public crate-dft-0.5.1 (c (n "dft") (v "0.5.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (k 0)) (d (n "num-traits") (r "^0.1.35") (k 0)))) (h "0kg0p8iifixmnazkk9jif4jxcq2v1qy3i036jpcrwifrd9zxmq4q")))

(define-public crate-dft-0.5.2 (c (n "dft") (v "0.5.2") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (k 0)) (d (n "num-traits") (r "^0.1.35") (k 0)))) (h "0s9dxkxdkg7hh2cp3xnji0ma4gvq3cg3hbaw7qjql6r8n1050i7f")))

(define-public crate-dft-0.5.3 (c (n "dft") (v "0.5.3") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (k 0)) (d (n "num-traits") (r "^0.1.35") (k 0)))) (h "0rkmq750ffcr077agws40igng4m3jg7m32xq7vzic8gkvkpw05k0")))

(define-public crate-dft-0.5.4 (c (n "dft") (v "0.5.4") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (k 0)) (d (n "num-traits") (r "^0.1.35") (k 0)))) (h "0qb123d8rkc38xs3c2br1j7j48jsymdwrbik4wvqr9bpkchab3w3")))

(define-public crate-dft-0.5.5 (c (n "dft") (v "0.5.5") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-complex") (r "^0.1.35") (k 0)) (d (n "num-traits") (r "^0.1.35") (k 0)))) (h "01q4576c8g5ic94cmaakakcbyjv982p9j6spaxkmjpddwin3rj46")))

