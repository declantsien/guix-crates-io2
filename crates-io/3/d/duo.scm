(define-module (crates-io #{3}# d duo) #:use-module (crates-io))

(define-public crate-duo-0.1.0-alpha.0 (c (n "duo") (v "0.1.0-alpha.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "numext-fixed-hash") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "secp256k1") (r "^0.12.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.12") (d #t) (k 0)))) (h "1krncqx1clq6bx1h2g8valj353dcd8zsvn2vgsh9j192sclf87a2") (y #t)))

