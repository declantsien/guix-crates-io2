(define-module (crates-io #{3}# d dte) #:use-module (crates-io))

(define-public crate-dte-0.0.0 (c (n "dte") (v "0.0.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1fwlr0lcqjzp5qvb83jx0qjrc5916xswwcjaifb6lkzcb8y696hj")))

(define-public crate-dte-0.0.3 (c (n "dte") (v "0.0.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "dtee") (r "^0.0.3") (d #t) (k 0)))) (h "01086jdvms564dmbg6lfqhi2g0n3vw349mjldkvp90f6silflijv")))

