(define-module (crates-io #{3}# d dwl) #:use-module (crates-io))

(define-public crate-dwl-2.0.1 (c (n "dwl") (v "2.0.1") (d (list (d (n "inquire") (r "^0.7.4") (d #t) (k 0)) (d (n "notifme") (r "^0.0.2") (d #t) (k 0)))) (h "1x6sd6nwa7arh1l2ljg3nr3q1rbg0015z70rsqn8189sl7q61hdd")))

(define-public crate-dwl-2.1.0 (c (n "dwl") (v "2.1.0") (d (list (d (n "inquire") (r "^0.7.4") (d #t) (k 0)) (d (n "notifme") (r "^0.0.2") (d #t) (k 0)))) (h "0ncjx6kszd7sjp98sb8pw8h5fnm07na6k05kkmx7nacdkb3y93hg")))

(define-public crate-dwl-2.2.0 (c (n "dwl") (v "2.2.0") (d (list (d (n "inquire") (r "^0.7.4") (d #t) (k 0)))) (h "03bywfh9gyifq44sr9r3dhadhkclp98k6h43xnsw6g770s0vp2r9")))

