(define-module (crates-io #{3}# d ddp) #:use-module (crates-io))

(define-public crate-ddp-0.0.1 (c (n "ddp") (v "0.0.1") (d (list (d (n "rand") (r "^0.3.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "websocket") (r "^0.12.1") (d #t) (k 0)))) (h "149qkhsl29an7p93n77707v6xh2z5fcsrnwfqvzri6bnxmmnwvwj")))

(define-public crate-ddp-0.0.2 (c (n "ddp") (v "0.0.2") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "websocket") (r "^0.12.1") (d #t) (k 0)))) (h "0qzgnmjhckbmk0j761brffrb1grm8c585vzb9c8c6asg9q3pcnjn")))

(define-public crate-ddp-0.0.3 (c (n "ddp") (v "0.0.3") (d (list (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)) (d (n "websocket") (r "^0.12.1") (d #t) (k 0)))) (h "02rxfzxviv04yvj2r5avrkifrm3yi0r2q00n0fcmzik063h70iyd")))

