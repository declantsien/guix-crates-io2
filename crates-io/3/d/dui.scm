(define-module (crates-io #{3}# d dui) #:use-module (crates-io))

(define-public crate-dui-0.0.1 (c (n "dui") (v "0.0.1") (d (list (d (n "iup-sys") (r "^0.0") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0nqw8r18c8fssa4bcrpahvxdrh09bai7x4mh52ahi79ilia735cs") (y #t)))

(define-public crate-dui-0.1.0 (c (n "dui") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "human_bytes") (r "^0.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)))) (h "0hdgc93yic6ks6s728sgcqf1xgr9m3vihfra9cz2gs2phnp5wk34")))

(define-public crate-dui-0.2.0 (c (n "dui") (v "0.2.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "crossterm") (r "^0.23") (d #t) (k 0)) (d (n "human_bytes") (r "^0.3") (d #t) (k 0)) (d (n "ignore") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 2)))) (h "09qzar6q1dv564srnfj12r0plrz8b87lr47bbkj2m9fng3hfq40h")))

