(define-module (crates-io #{3}# d dtl) #:use-module (crates-io))

(define-public crate-dtl-0.0.1 (c (n "dtl") (v "0.0.1") (d (list (d (n "mopa") (r "*") (d #t) (k 0)))) (h "1jxp5448s1pkcxvf5j53l6x4g2zqxkb46d0hpi07l7fb2qa8wq5z")))

(define-public crate-dtl-0.0.2 (c (n "dtl") (v "0.0.2") (d (list (d (n "chrono") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "mopa") (r "^0.2") (d #t) (k 0)))) (h "0i6ppq22x9653ggsnx6g9h2719pb3ijwkf7nm12whfg24z0bh19g")))

