(define-module (crates-io #{3}# d d30) #:use-module (crates-io))

(define-public crate-d30-0.1.0 (c (n "d30") (v "0.1.0") (d (list (d (n "bluetooth-serial-port-async") (r "^0.6.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "dimensions") (r "^0.1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.3") (f (quote ("gpu_cache"))) (d #t) (k 0)) (d (n "serialport") (r "^4.2.1") (d #t) (k 0)) (d (n "snafu") (r "^0.7.5") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gy7m1pxb14sws79jmq0kn2h6a8h31lzgfflg7k6al998qzgy1r6")))

