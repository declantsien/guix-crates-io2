(define-module (crates-io #{3}# d dns) #:use-module (crates-io))

(define-public crate-dns-0.0.1 (c (n "dns") (v "0.0.1") (h "0m143n1anrvq5vmx1hr4ni2kn609jqdx527v5w7vv239ppg0dwfl")))

(define-public crate-dns-0.0.1-pre (c (n "dns") (v "0.0.1-pre") (h "0gkb53iyc1bqjga4bs4qwliyhgbmqpvgrflhbipzck7bl85nv1v7")))

(define-public crate-dns-0.0.1-pre2 (c (n "dns") (v "0.0.1-pre2") (h "0mjn91ak9v7vc7dnf20i4hj4vn3qyrf18lgdszhcc1sbngx8zk2q")))

(define-public crate-dns-0.0.2-pre1 (c (n "dns") (v "0.0.2-pre1") (h "1vfgmkd40l5zvm1djpcsyr4miw4a0cwng1slbqv3mmclgj14vajr")))

(define-public crate-dns-0.0.2-pre2 (c (n "dns") (v "0.0.2-pre2") (h "10yymr9rx9b8chcaxfz7vjrhvf2p5qzsjhpf2ka8pfxv1a2flnwf")))

(define-public crate-dns-0.0.3-pre1 (c (n "dns") (v "0.0.3-pre1") (h "0k9w093qmgyf7xycxrfqk5wvnrn82ib6mm5670i6jk2rlklwhvfr")))

