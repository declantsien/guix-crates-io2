(define-module (crates-io #{3}# d daf) #:use-module (crates-io))

(define-public crate-daf-1.0.0 (c (n "daf") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "02i3hipxymy9nr99m2qhj56wa9a7jin1irpr554rpvmwc740dscn")))

(define-public crate-daf-1.0.1 (c (n "daf") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.24") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.21") (d #t) (k 0)))) (h "0a9vk8rp5z86d8h45x5kqxy892c5q6hvlyp6h4mra31phzwb55h3")))

