(define-module (crates-io #{3}# d dwf) #:use-module (crates-io))

(define-public crate-dwf-0.1.0 (c (n "dwf") (v "0.1.0") (h "0r3diz26mvzmfr15296z3q4s4yp294dw8ibmil9x98mbvhhqzq4m")))

(define-public crate-dwf-0.2.0 (c (n "dwf") (v "0.2.0") (d (list (d (n "gnuplot") (r "^0.0.39") (d #t) (k 2)))) (h "1zi5l2y2jaix0spjg9mg46v9m6w4479dv5j88phwa6hc68z2vzpp")))

(define-public crate-dwf-0.2.1 (c (n "dwf") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "gnuplot") (r "^0.0.39") (d #t) (k 2)))) (h "1iaa8p5k2vnjxlh624q5hickal6qbqc9pc1mzs2gcl5rn5bi98ja")))

(define-public crate-dwf-0.2.2 (c (n "dwf") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "gnuplot") (r "^0.0.39") (d #t) (k 2)))) (h "0hwvi2wqidwwsfj84ckrxqxr1in454n9afsm5z4d2gh2bvyw98i8")))

