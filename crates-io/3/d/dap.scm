(define-module (crates-io #{3}# d dap) #:use-module (crates-io))

(define-public crate-dap-0.1.0-alpha1 (c (n "dap") (v "0.1.0-alpha1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "1p5yc6xa29am9qpvwnmv487a66qsvsyzcfac8sz2vgchyk9c20m5") (r "1.56.0")))

(define-public crate-dap-0.2.0-alpha1 (c (n "dap") (v "0.2.0-alpha1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "1cid4y98l7bmyxahsqdr3fqv0pbr1y2ymrp91b8pjpf74c63zc45") (r "1.56.0")))

(define-public crate-dap-0.3.0-alpha1 (c (n "dap") (v "0.3.0-alpha1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "03x9z37c0r35lmg2dv4kqrri87vf10lszxfj8xhii6n4yvdvl1vf") (r "1.56.0")))

(define-public crate-dap-0.3.1-alpha1 (c (n "dap") (v "0.3.1-alpha1") (d (list (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "025bai1fic17qv6fj930z83mlmwlz9jdmwlg3nfg1wx46qss9q4r") (r "1.56.0")))

(define-public crate-dap-0.4.1-alpha1 (c (n "dap") (v "0.4.1-alpha1") (d (list (d (n "fake") (r "2.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.*") (d #t) (k 0)) (d (n "thiserror") (r "1.*") (d #t) (k 0)))) (h "1aczfbbryvi0yqfi2g4jrp6pxcf92irlrybrlrdp9arlsf4zrirm") (f (quote (("integration_testing" "fake" "rand") ("client")))) (r "1.56.0")))

