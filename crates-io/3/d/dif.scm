(define-module (crates-io #{3}# d dif) #:use-module (crates-io))

(define-public crate-dif-0.1.0 (c (n "dif") (v "0.1.0") (h "0cxzfx1xfax4djd6ank4377gp8z4hahpmmw9dc1w1gahg4vsl5yf") (f (quote (("alloc"))))))

(define-public crate-dif-0.1.1 (c (n "dif") (v "0.1.1") (h "0pcgf8fs95yprpv2wc7mff9456q4k4pi3iswqfsqzbf8bk1dgnsp") (f (quote (("alloc"))))))

