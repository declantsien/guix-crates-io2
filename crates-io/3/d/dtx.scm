(define-module (crates-io #{3}# d dtx) #:use-module (crates-io))

(define-public crate-dtx-0.1.0 (c (n "dtx") (v "0.1.0") (h "13wsqxknwh262avj2r661a84a3dmfvywycyr8i520aqagpyys5pp") (r "1.74.0")))

(define-public crate-dtx-0.1.1 (c (n "dtx") (v "0.1.1") (h "04m8qp98qnmslnd9kw7ssl1s2yfbrdqb9gvh2a2ds6da3352dpj3") (r "1.74.0")))

