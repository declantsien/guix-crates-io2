(define-module (crates-io #{3}# d dof) #:use-module (crates-io))

(define-public crate-dof-0.1.4 (c (n "dof") (v "0.1.4") (d (list (d (n "goblin") (r "^0.3.4") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "zerocopy") (r "^0.3.0") (d #t) (k 0)))) (h "0nciwyb0d5pmrwxrb5j45pqzxd450az41kw6b7lsgxnzrw3h8rlx")))

(define-public crate-dof-0.1.5 (c (n "dof") (v "0.1.5") (d (list (d (n "goblin") (r "^0.3.4") (f (quote ("elf64" "mach64"))) (o #t) (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "zerocopy") (r "^0.3.0") (d #t) (k 0)))) (h "10ff6h5hw3fl2m2i2k3kpfphc1pk4jqf3mj528myhm8l46hj2swy") (f (quote (("des" "pretty-hex" "goblin"))))))

(define-public crate-dof-0.2.0 (c (n "dof") (v "0.2.0") (d (list (d (n "goblin") (r "^0.6") (f (quote ("elf64" "mach64"))) (o #t) (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6") (d #t) (k 0)))) (h "1c65c9l9jawgzdbahf5afrk3mg4abswr8mpav4yry8pwyfxqw92x") (f (quote (("des" "pretty-hex" "goblin"))))))

(define-public crate-dof-0.3.0 (c (n "dof") (v "0.3.0") (d (list (d (n "goblin") (r "^0.8") (f (quote ("elf64" "mach64"))) (o #t) (d #t) (k 0)) (d (n "pretty-hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (f (quote ("derive"))) (d #t) (k 0)))) (h "00nhxk043gxvydsr5i1sqq4k2s24pk1kqfy4s9cplrqv6ab573jm") (f (quote (("des" "pretty-hex" "goblin"))))))

