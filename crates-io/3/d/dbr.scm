(define-module (crates-io #{3}# d dbr) #:use-module (crates-io))

(define-public crate-dbr-0.1.0 (c (n "dbr") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ybymdym9lf3pzxz2qvpclf0xq6d26djs1r7zfq5i2qrpr9crg5s")))

(define-public crate-dbr-0.1.1 (c (n "dbr") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.42.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wl6icxk7zd02r0xzp7cy01sv5mhkmr7m7r3bw3rdfmxd6vx56fc")))

(define-public crate-dbr-0.1.2 (c (n "dbr") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.42.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1dfvcwajsgng1x2i560is2cz1cs59fxf0k9462lvspp1djwfyzyk")))

(define-public crate-dbr-0.1.3 (c (n "dbr") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.42.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0jqfy116yb9z7yvkjpr7qkcn947fsqqiir8xzwg9ym7zdlsarwpf")))

