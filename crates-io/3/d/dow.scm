(define-module (crates-io #{3}# d dow) #:use-module (crates-io))

(define-public crate-dow-0.1.0 (c (n "dow") (v "0.1.0") (h "0vckv6awhw4607kilbdgwi8yzmgnh2jww7i94bgdf1fkhqpir1qs")))

(define-public crate-dow-0.1.1 (c (n "dow") (v "0.1.1") (h "1v3ygirj36linjh9ah22nw9kml1l9avmlmigjpq6zsf2y1pq12id")))

(define-public crate-dow-0.1.2 (c (n "dow") (v "0.1.2") (h "1kn9z7y5z4mwz0a2gj35200c5578fbk2zk4ykp8w53crzkf27f4c")))

(define-public crate-dow-0.1.3 (c (n "dow") (v "0.1.3") (h "0qi1mxnw2pw04ljc2n3nmscympn7pfvyf70cghsh4r44fls4pxlp")))

