(define-module (crates-io #{3}# d des) #:use-module (crates-io))

(define-public crate-des-0.0.1 (c (n "des") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "simple_parallel") (r "^0.3") (d #t) (k 0)))) (h "1pfpdia86d978yxf1am34vw6l3sczm36dgnnanz8pxzn20qramdg")))

(define-public crate-des-0.0.2 (c (n "des") (v "0.0.2") (d (list (d (n "rayon") (r "^0.4") (d #t) (k 0)))) (h "1d0l7n8bbff94lg8cn5v0h1ynhyg410ampp33dwdi5gk8n82b4bs")))

(define-public crate-des-0.0.3 (c (n "des") (v "0.0.3") (d (list (d (n "rayon") (r "^0.4") (d #t) (k 0)))) (h "0bavgg0qs3ilf3kcsqz6lbq8z75w8397gvdmni4rbilvi5mlf5pc") (f (quote (("nightly"))))))

(define-public crate-des-0.0.4 (c (n "des") (v "0.0.4") (d (list (d (n "rayon") (r "= 0.4.2") (d #t) (k 0)))) (h "1imm2gw6cd3cz913wvh5jr4826mwkifr9ns9yhf6lp5i7h94j9df") (f (quote (("nightly"))))))

(define-public crate-des-0.1.0 (c (n "des") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.5") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byte-tools") (r "^0.2") (d #t) (k 0)) (d (n "opaque-debug") (r "^0.1") (d #t) (k 0)))) (h "14nqr4vcq5dgq4bhvdnqx2ay9cawaxnrb5rjz7iaw25f28bxm8yj")))

(define-public crate-des-0.2.0 (c (n "des") (v "0.2.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1310lw1g1lqbp856apkxxr2c51ns3nx0sr91mpdxsgzgdria1z14")))

(define-public crate-des-0.3.0 (c (n "des") (v "0.3.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1bx3i26ihayjl7ksvjp31qyajrbf620vlw16khvp55zfb8dmzfkl")))

(define-public crate-des-0.4.0 (c (n "des") (v "0.4.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "0qlkrlf1bmg83x9pvmv48j9i7ipkbfll5lr8l93z29qahpr1fvsf")))

(define-public crate-des-0.5.0 (c (n "des") (v "0.5.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "03gsswq8mj934flgw9s1rwkx3ryxa72spmr7kxn7wrzcil2bb170")))

(define-public crate-des-0.6.0 (c (n "des") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1bigk1x1kxvnfjn1alr8cc383z1flmj8q7g2pjl2zal8i1s7qkmj")))

(define-public crate-des-0.7.0 (c (n "des") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "0pbsfkkwfqnd4nsv3ik4z09h248f57y7bj2j1l134i2mzd4xshdc")))

(define-public crate-des-0.8.0 (c (n "des") (v "0.8.0") (d (list (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "0a1md2pdrypxz67aps29pp17k3ravz7rqyv73nqsi49p26gv0qfr") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-des-0.8.1 (c (n "des") (v "0.8.1") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)))) (h "07kshslxanmg0g6007scvglfhg6mli2a8qzhx4kxx4z9ik781pgz") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-des-0.9.0-pre.0 (c (n "des") (v "0.9.0-pre.0") (d (list (d (n "cipher") (r "=0.5.0-pre.4") (d #t) (k 0)) (d (n "cipher") (r "=0.5.0-pre.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "15srn98zfii8vqvs95dg1dd2bx1rlil5y6bmjxk1aw0f4bxnn41z") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.65")))

