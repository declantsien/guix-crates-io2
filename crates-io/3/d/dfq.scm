(define-module (crates-io #{3}# d dfq) #:use-module (crates-io))

(define-public crate-dfq-0.1.0 (c (n "dfq") (v "0.1.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "datafusion") (r "^33") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "parking_lot"))) (d #t) (k 0)))) (h "1js6cgaxjzazsfhp01myi6qdg563i00r089z6y74d6csg83dpsq9")))

(define-public crate-dfq-0.1.1 (c (n "dfq") (v "0.1.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "datafusion") (r "^33") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt" "rt-multi-thread" "sync" "parking_lot"))) (d #t) (k 0)))) (h "0vwv2si7679d9z61x03klgldh5wwxhz8by3faqx6xc9l30grk5zf")))

