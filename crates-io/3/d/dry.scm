(define-module (crates-io #{3}# d dry) #:use-module (crates-io))

(define-public crate-dry-0.1.0 (c (n "dry") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)))) (h "1sbljblwglq600lm6ly1bvj858dahqx65sn2v01bsrz0d50d8gyg") (f (quote (("nightly"))))))

(define-public crate-dry-0.1.1 (c (n "dry") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (k 0)))) (h "114srp3gy6cz4zsj2a99cbb6kw9r5jw46i8s68pndg564dyzg08k") (f (quote (("nightly"))))))

