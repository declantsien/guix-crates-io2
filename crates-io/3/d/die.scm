(define-module (crates-io #{3}# d die) #:use-module (crates-io))

(define-public crate-die-0.1.0 (c (n "die") (v "0.1.0") (h "0dkpyj601nx5zjlns20lnvkiajparnb6iv67ngy7skc47y8mawwc")))

(define-public crate-die-0.2.0 (c (n "die") (v "0.2.0") (h "143205dc96whf073ciiihqjjy7si5y5p3gcr9r53dxrrc5g4sqzq")))

