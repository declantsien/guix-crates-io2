(define-module (crates-io #{3}# d dio) #:use-module (crates-io))

(define-public crate-dio-0.1.0 (c (n "dio") (v "0.1.0") (h "0ilql9bz0937i0pvby9jp2pfhjjmqccjqwa1mk1n5l707h0dlnqg") (y #t)))

(define-public crate-dio-0.1.1 (c (n "dio") (v "0.1.1") (h "1n1yysszl227c5d6mh1n8m3kag4iy8i1mkz3rsds07h54psiifkh") (y #t)))

