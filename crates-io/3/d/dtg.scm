(define-module (crates-io #{3}# d dtg) #:use-module (crates-io))

(define-public crate-dtg-1.0.2 (c (n "dtg") (v "1.0.2") (d (list (d (n "assert_cmd") (r ">=1.0.2, <2.0.0") (d #t) (k 2)) (d (n "chrono") (r ">=0.4.19, <0.5.0") (d #t) (k 0)) (d (n "chrono-tz") (r ">=0.5.3, <0.6.0") (d #t) (k 0)))) (h "1i8wj8h4qps98wgbnvm7h77jmhf2vfvlv0ybhf5v0686fr3lqijk")))

(define-public crate-dtg-2.0.0 (c (n "dtg") (v "2.0.0") (d (list (d (n "assert_cmd") (r ">=1.0.2, <2.0.0") (d #t) (k 2)) (d (n "chrono") (r ">=0.4.19, <0.5.0") (d #t) (k 0)) (d (n "chrono-tz") (r ">=0.5.3, <0.6.0") (d #t) (k 0)) (d (n "iana-time-zone") (r ">=0.1.2, <0.2.0") (d #t) (k 0)))) (h "11npw8vc8jzbdw7k3jys0skag36lb7619q18bxjis0kp06kb2mkf")))

(define-public crate-dtg-2.0.1 (c (n "dtg") (v "2.0.1") (d (list (d (n "assert_cmd") (r ">=1.0.2, <2.0.0") (d #t) (k 2)) (d (n "chrono") (r ">=0.4.19, <0.5.0") (d #t) (k 0)) (d (n "chrono-tz") (r ">=0.5.3, <0.6.0") (d #t) (k 0)) (d (n "iana-time-zone") (r ">=0.1.2, <0.2.0") (d #t) (k 0)))) (h "0v4zklb4mnnksi4cr30ccrw9bcc6zjw7nim6k6j7ljsds5crc72q")))

(define-public crate-dtg-2.1.0 (c (n "dtg") (v "2.1.0") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)))) (h "1l3cimdks1n245gfjnqcczpcz8y43byc8vsv9f18p3nzmyhgvh02")))

(define-public crate-dtg-2.2.0 (c (n "dtg") (v "2.2.0") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)))) (h "1aslybm5xkavvfpjybys41xgs9i0714bvya2w24l19c1hg1nm2cm")))

(define-public crate-dtg-2.2.1 (c (n "dtg") (v "2.2.1") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)))) (h "022q4m2a5wmp0mywkx8r8dkzd0s91p16ffpwd45b711d8mx5lb8r")))

(define-public crate-dtg-2.2.2 (c (n "dtg") (v "2.2.2") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)))) (h "0xziqs4cg3qp7m5yzv4z50mbdqshx286cp2yyswkr5bci385mia6")))

(define-public crate-dtg-2.2.3 (c (n "dtg") (v "2.2.3") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)))) (h "1gcml88r48p6pmjcnsmwvzyxh92yin63i47nna1vvma7zhdl77b4")))

(define-public crate-dtg-2.2.4 (c (n "dtg") (v "2.2.4") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)))) (h "1hklv4d7x455d9r8y4vbxq74gvbk0nxaldpqsm1dhi98m02flxha")))

(define-public crate-dtg-3.0.0 (c (n "dtg") (v "3.0.0") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "05n0lyl8ssml3qw5yl773fz27aa7nwp1sh59b9vsc7gasp7x4vqy")))

(define-public crate-dtg-3.1.0 (c (n "dtg") (v "3.1.0") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1p8njdcq1q180lpfp0cgrq8c24gijbbzcpyar3b6gvz9ilwr9zlv")))

(define-public crate-dtg-3.2.0 (c (n "dtg") (v "3.2.0") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "12i9c776j2flvmdvg4xnydv91vmasmqn06k40g0804hj3sq4vzw9")))

(define-public crate-dtg-3.2.1 (c (n "dtg") (v "3.2.1") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1350nkabyh063isd7m89m8cyvrqiiirb16bdhdi7zf3z7379l9f0") (y #t)))

(define-public crate-dtg-3.2.2 (c (n "dtg") (v "3.2.2") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "05hmsdwm9fw5daj9c3qj3s8507phl4hhfci1j3vnsm2dar50m6bb")))

(define-public crate-dtg-3.3.0 (c (n "dtg") (v "3.3.0") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1x0dd89i16wvkws5k5a3yz2gzf11v90ls0cbb7vskli7vq2c8hhj")))

(define-public crate-dtg-3.3.1 (c (n "dtg") (v "3.3.1") (d (list (d (n "assert_cmd") (r "^1.0.3") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "07sxr8kkcfv3hb2gnw03vf32n1ql667320z5frm7i2qmzvfjg8zz")))

(define-public crate-dtg-3.4.0 (c (n "dtg") (v "3.4.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1izd5n4imnwn6vk9v07av8rppp0zyf0asgbzjzwhj0kiz5apwxwx")))

(define-public crate-dtg-3.5.0 (c (n "dtg") (v "3.5.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0r6vsm47qb61szy23bjlnm9lc8h7yp5a7fhm98sxffm6xlsx3rfk")))

(define-public crate-dtg-3.6.0 (c (n "dtg") (v "3.6.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0avi0i69nlnw0kans5x00zmx80dnykqv18wwbn43k00xn2a62zkx")))

(define-public crate-dtg-3.7.0 (c (n "dtg") (v "3.7.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "chrono-tz") (r "^0.5.3") (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.7") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1q2bpvn6qz5qllavnydmvn1klfzhk8xbhpm2hdzwdvi2ixdm16jn")))

(define-public crate-dtg-4.0.0 (c (n "dtg") (v "4.0.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.7") (d #t) (k 0)) (d (n "dtg-lib") (r "^4.0.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)))) (h "0scla72fvk2q1v7fkkjs7v8gh4n4f6r7v9hs2dj99fk88jjzz6d0")))

(define-public crate-dtg-4.0.1 (c (n "dtg") (v "4.0.1") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.7") (d #t) (k 0)) (d (n "dtg-lib") (r "^4.0.1") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)))) (h "1cbjp5ydy4rln9vp3vywrr6gvjwl4i7ipypc5rl5ldzgwfdv3hbp")))

(define-public crate-dtg-4.0.2 (c (n "dtg") (v "4.0.2") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.7") (d #t) (k 0)) (d (n "dtg-lib") (r "^4.0.2") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)))) (h "1b2hdbas681djf5xvkv1rdn1skdd8qrxwdvk0a0cpfw7cc9423z9")))

(define-public crate-dtg-4.0.3 (c (n "dtg") (v "4.0.3") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.7") (d #t) (k 0)) (d (n "dtg-lib") (r "^4.0.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)))) (h "0gclsgbny68l6ljcf9rx6rgiy6k64n616vkyssaav1i80pizfwz6")))

(define-public crate-dtg-4.1.0 (c (n "dtg") (v "4.1.0") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.7") (d #t) (k 0)) (d (n "dtg-lib") (r "^4.1.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)))) (h "080652awdnggl5zwg0xs2yj8chmas9v1f78sywyin6zn1sfv2k73")))

(define-public crate-dtg-4.1.1 (c (n "dtg") (v "4.1.1") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.7") (d #t) (k 0)) (d (n "dtg-lib") (r "^4.1.1") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)))) (h "0b0yqikc8pz5kv37qmpnc0fxbyigw3hz8smdpmwvgkn0dcclpldl")))

(define-public crate-dtg-4.1.2 (c (n "dtg") (v "4.1.2") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.7") (d #t) (k 0)) (d (n "dtg-lib") (r "^4.1.2") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)))) (h "1gnl0yb4x81zcb7930i7gkpdvnl0jyqxlc416xjbz2inwhv2xj6h")))

(define-public crate-dtg-4.1.3 (c (n "dtg") (v "4.1.3") (d (list (d (n "assert_cmd") (r "^2.0.0") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clearscreen") (r "^1.0.7") (d #t) (k 0)) (d (n "dtg-lib") (r "^4.1.3") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.9") (d #t) (k 0)))) (h "158aa1rdvxl8bbz3q8iaxw48h3jgy13ihdim827vra8nlx0by54d")))

(define-public crate-dtg-5.0.0 (c (n "dtg") (v "5.0.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.1") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.0") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.0.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.53") (d #t) (k 0)))) (h "10gcl7qlpav29b3l8j3aivj4gi3ll5waq23palq9jygww59yf7h6")))

(define-public crate-dtg-5.1.0 (c (n "dtg") (v "5.1.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.1.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.56") (d #t) (k 0)))) (h "08ad260yicafnr8qbbykxfg1i8l0z0859g9kv12is1fxgdf106id")))

(define-public crate-dtg-5.2.0 (c (n "dtg") (v "5.2.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.2.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.56") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (k 0)))) (h "0s0cm85x555wa9qi9b0392kmi89ahilflxv6m4frralnf85dzpzx")))

(define-public crate-dtg-5.3.0 (c (n "dtg") (v "5.3.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.3.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.56") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (k 0)))) (h "1n37dmywirgmd2h7qkvzm9iaa2xh3221qp7w9rhphxa88n9p4i0i")))

(define-public crate-dtg-5.3.1 (c (n "dtg") (v "5.3.1") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.2") (d #t) (k 0)) (d (n "clap") (r "^4.3.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.3.1") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.56") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (k 0)))) (h "0600nf2cxfjg1nzzplfsl5b5lx6kl6ys041q3vp6xxi47ccviyfc")))

(define-public crate-dtg-5.4.0 (c (n "dtg") (v "5.4.0") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.4.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0l9hbbgi8l8fjhdccx0g4h4n6m6qmc8myr7gdnhbxln0skii8z82")))

(define-public crate-dtg-5.5.0 (c (n "dtg") (v "5.5.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.4.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0wkm59gdx7maapi9ddm62qyzsq6cy1qhgmg8xjiag6h70ph7f9z6")))

(define-public crate-dtg-5.5.1 (c (n "dtg") (v "5.5.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.4.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1ad9c1zw4af8mm8hnq6yrg9rlgaf8dyrjxqid338js6mlyj7d3nn")))

(define-public crate-dtg-5.6.0 (c (n "dtg") (v "5.6.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.4.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (t "cfg(unix)") (k 0)))) (h "10z5lw3nqmawbg4l7wylbs177rpvl05a9pns1sixgjsn2pfav3d9")))

(define-public crate-dtg-5.7.0 (c (n "dtg") (v "5.7.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.4.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1pb4f5g8cv3z7gvy822npk8hf3v57xmz1wihvixwcwn2yvj8w250")))

(define-public crate-dtg-5.8.0 (c (n "dtg") (v "5.8.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.8.0") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.57") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1jw11qpk9n01qvkbr5r2r1jr4b0cxxmddw5438z3vz9fkk5y8ahb")))

(define-public crate-dtg-5.8.1 (c (n "dtg") (v "5.8.1") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "chrono-tz") (r "^0.8.3") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clearscreen") (r "^2.0.1") (d #t) (k 0)) (d (n "dtg-lib") (r "^5.8.1") (d #t) (k 0)) (d (n "iana-time-zone") (r "^0.1.58") (d #t) (k 0)) (d (n "pager") (r "^0.16.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1wj1l8wl6ym2fqjwwc7a87mx0p5lwrvm7gdmikarnw4pl792bygy")))

