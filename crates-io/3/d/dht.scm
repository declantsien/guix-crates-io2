(define-module (crates-io #{3}# d dht) #:use-module (crates-io))

(define-public crate-dht-0.0.0 (c (n "dht") (v "0.0.0") (d (list (d (n "bencode") (r "~0.1.0") (d #t) (k 0)) (d (n "num") (r "~0.1.3") (d #t) (k 0)))) (h "0jqv9n6qj69f0vf385ypcirgj2gqd9mqa8xprbilpn9v2yb5gq7d")))

(define-public crate-dht-0.0.1 (c (n "dht") (v "0.0.1") (d (list (d (n "bencode") (r "~0.1.7") (d #t) (k 0)) (d (n "log") (r "~0.2.1") (d #t) (k 0)) (d (n "num") (r "~0.1.10") (d #t) (k 0)) (d (n "rand") (r "~0.1.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.2.10") (d #t) (k 0)))) (h "0ddgi2f8icpsyz1pjw7dn520f8yrv1vnr53p6d5l45mnvx9q52vr")))

(define-public crate-dht-0.0.2 (c (n "dht") (v "0.0.2") (d (list (d (n "log") (r "~0.3.1") (d #t) (k 0)) (d (n "num") (r "~0.1.24") (d #t) (k 0)) (d (n "rand") (r "~0.3.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.14") (d #t) (k 0)))) (h "0q71xk9l60p39gm90r2q9y8q1d3j8mdag79hwz4hpbqflwxzmlfk")))

(define-public crate-dht-0.0.3 (c (n "dht") (v "0.0.3") (d (list (d (n "log") (r "~0.3.1") (d #t) (k 0)) (d (n "num") (r "~0.1.24") (d #t) (k 0)) (d (n "rand") (r "~0.3.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.14") (d #t) (k 0)))) (h "1djhxf6p0mixm736ah3kdwqpaad5wvqz1mckmywb1y8zal3wypzj")))

(define-public crate-dht-0.0.4 (c (n "dht") (v "0.0.4") (d (list (d (n "log") (r "~0.3.1") (d #t) (k 0)) (d (n "num") (r "~0.1.24") (d #t) (k 0)) (d (n "rand") (r "~0.3.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "~0.3.14") (d #t) (k 0)))) (h "156gavz53db5v337cnb6pzgng09hvdaxrdhghw85kf8kfz7s0kkc")))

(define-public crate-dht-0.0.5 (c (n "dht") (v "0.0.5") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1j7ibclgbm6ifnqxpalwkwqr5mmxzkixw24zxisysv4h95mpi5d9")))

(define-public crate-dht-0.0.6 (c (n "dht") (v "0.0.6") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1l7q54y5iv77q1zsm9xi4a8vxhamxjf2zxl9fb080fbsma6ffikh")))

