(define-module (crates-io #{3}# d df2) #:use-module (crates-io))

(define-public crate-df2-0.1.0 (c (n "df2") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0100axkgbdw63dkjqiknqvzgni8knx4rqhxkg3wcv51h7riwg25i")))

