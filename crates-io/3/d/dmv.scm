(define-module (crates-io #{3}# d dmv) #:use-module (crates-io))

(define-public crate-dmv-0.0.0 (c (n "dmv") (v "0.0.0") (d (list (d (n "dmv_derive") (r "^0.0.0") (o #t) (d #t) (k 0)))) (h "03vjv43qz21dp33sgrvy9vz5djrlm8sda2p9cqgb0w17rk45zjl2") (f (quote (("derive" "dmv_derive") ("default" "derive"))))))

(define-public crate-dmv-0.1.0 (c (n "dmv") (v "0.1.0") (d (list (d (n "dmv_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1qnryyiq8vbvxg9azxy7pk6kyjqy2kqp8nnhdhkwsry6v5snp0c9") (f (quote (("derive" "dmv_derive") ("default" "derive"))))))

(define-public crate-dmv-0.2.0-a0 (c (n "dmv") (v "0.2.0-a0") (d (list (d (n "dmv_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1rcsqr50iw4kdyyj8ggzmawhz9knd70dkmmvkwssz539xwjs4823") (f (quote (("derive" "dmv_derive") ("default" "derive"))))))

(define-public crate-dmv-0.2.0-a1 (c (n "dmv") (v "0.2.0-a1") (d (list (d (n "dmv_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "101i5lb25arhy20k2xk1lsjs3igjnay8gfl1yjqhibadlzqqx1bn") (f (quote (("derive" "dmv_derive") ("default" "derive"))))))

(define-public crate-dmv-0.2.0-a2 (c (n "dmv") (v "0.2.0-a2") (d (list (d (n "dmv_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "03wwsnzxki8g5dikwlila1v9yb13dikfqsjgmzyhd4pk8przwp0d") (f (quote (("derive" "dmv_derive") ("default" "derive"))))))

(define-public crate-dmv-0.2.0-a3 (c (n "dmv") (v "0.2.0-a3") (d (list (d (n "dmv_derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "13zsdmibsx02hcxwrapyqaxl9475wkvf35vcmmm1gljs6pcll0wp") (f (quote (("derive" "dmv_derive") ("default" "derive"))))))

(define-public crate-dmv-0.2.0-a4 (c (n "dmv") (v "0.2.0-a4") (d (list (d (n "dmv_derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0fa924zjlp9iphrxl05cfdn87vpcmdk5l65hj9vknxjqybaf0dkl") (f (quote (("derive" "dmv_derive") ("default" "derive"))))))

(define-public crate-dmv-0.2.0 (c (n "dmv") (v "0.2.0") (d (list (d (n "dmv_derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "13xwql4b8wm9l4mc93145pq9zdvi2ky9yc37algskmzsk1wc4xvj") (f (quote (("derive" "dmv_derive") ("default" "derive"))))))

(define-public crate-dmv-0.2.1 (c (n "dmv") (v "0.2.1") (d (list (d (n "dmv_derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "09ys1fdidp5a03fvsviwhynykv1y3v14yi20zr58zk8ydbga3yyy") (f (quote (("derive" "dmv_derive") ("default" "derive"))))))

(define-public crate-dmv-0.3.0-a2 (c (n "dmv") (v "0.3.0-a2") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "09x1gd7q89z2f8b7sy28ycaic5vigpw85z00cxykdfrrdirja2ll")))

(define-public crate-dmv-0.3.0-a3 (c (n "dmv") (v "0.3.0-a3") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ll84lr7wr0v6n6a5x2jjgfkk373v82wrhckgaa28s1k29zr0c2j")))

(define-public crate-dmv-0.3.0-a4 (c (n "dmv") (v "0.3.0-a4") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1g5yn9qjm4wml410rylf20vmsjni9l77x5d0dv3hbqaskmh6da6f")))

(define-public crate-dmv-0.3.0 (c (n "dmv") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0591c7wgz968215iwjkjnwbsji65a5adx2nmliamj4zf20270wr6")))

(define-public crate-dmv-0.3.1 (c (n "dmv") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0yhyglxf3i9qxyv8kwca1xfszi8316ba7d4xngqykwgb4z0w1n0p")))

(define-public crate-dmv-0.3.2-a0 (c (n "dmv") (v "0.3.2-a0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1szqc2fr8ymwmg29plyfqhnj9z5f37gyd9656jg0bvxllyd6r1db")))

(define-public crate-dmv-0.3.2-a1 (c (n "dmv") (v "0.3.2-a1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1klq29mzrqzl2k5d4rwpadrprms4xaxr64j9g6b9khpnch3ba5j6")))

