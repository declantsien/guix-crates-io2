(define-module (crates-io #{3}# d dmt) #:use-module (crates-io))

(define-public crate-dmt-0.1.0 (c (n "dmt") (v "0.1.0") (d (list (d (n "arrow") (r "^4.0.0") (d #t) (k 0)) (d (n "datafusion") (r "^4.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "0si3x11lqn28r5dyylg9mf139n687lcii4zi022qzdiqkyx11paj")))

(define-public crate-dmt-0.2.0 (c (n "dmt") (v "0.2.0") (d (list (d (n "arrow") (r "^4.0.0") (d #t) (k 0)) (d (n "datafusion") (r "^4.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "1gxrnsah8rmr2xy3dxi8hp2a9waw7m0zd8ljpkz74fy2gdmip0nx")))

(define-public crate-dmt-0.3.0 (c (n "dmt") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "arrow") (r "^4.0.0") (d #t) (k 0)) (d (n "datafusion") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 0)))) (h "06xhmsajx1s5fhmlxqwn6wvw2684lk9b83rhdx3rykjmfi2b49xn")))

(define-public crate-dmt-0.4.0 (c (n "dmt") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3.0.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "metaquery") (r "^0.3.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "sqlx") (r "^0.5") (f (quote ("runtime-tokio-rustls" "postgres"))) (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m4g9k3v7ckwpw7m94arh4m9xj62wycr4d1vr976hckmafils0qs")))

