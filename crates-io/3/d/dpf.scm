(define-module (crates-io #{3}# d dpf) #:use-module (crates-io))

(define-public crate-dpf-0.1.0 (c (n "dpf") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.14") (d #t) (k 0)))) (h "1va70acg46ik58bz2wvfdyhmkqfzwghslicc4fr0a3gbd33aas8s")))

(define-public crate-dpf-0.2.0 (c (n "dpf") (v "0.2.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "once_cell") (r "^1.14") (d #t) (k 0)))) (h "14ms7bww571a0n3d72cabq4qdihba6nhr62rn4p8rzip8z2b9izw")))

