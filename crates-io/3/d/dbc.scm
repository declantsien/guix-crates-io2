(define-module (crates-io #{3}# d dbc) #:use-module (crates-io))

(define-public crate-dbc-0.1.0 (c (n "dbc") (v "0.1.0") (h "01mr053cfmqigl2vzhyhi8lahkl99ypbix5a4jqg7ffv1j607nd9")))

(define-public crate-dbc-0.2.0 (c (n "dbc") (v "0.2.0") (h "0ykyfblql802xl0fkbi8v6vixp8rdrahjk8l1y1rvpp80x9z0q15")))

(define-public crate-dbc-0.2.1 (c (n "dbc") (v "0.2.1") (h "1xwxfvm7vsnwh4w5f5isvqvjgvcnxd9x8h19yrkqfpc35s44isvl")))

(define-public crate-dbc-0.3.0 (c (n "dbc") (v "0.3.0") (h "0rn5f6093yndyhy4fr98x6f9n0syslxyvpq12w425dqi3h8zw9wc")))

(define-public crate-dbc-0.3.1 (c (n "dbc") (v "0.3.1") (h "1zl2qhwvmfh1nli44m9q573ixazwjms64qpiwrvk9by4zjm1a4j7")))

(define-public crate-dbc-0.3.2 (c (n "dbc") (v "0.3.2") (h "0jdyksxw8al4m1jyi6gbxx698mhrl0hqv2cz4vy6yx8jc5iw51y1")))

(define-public crate-dbc-0.3.3 (c (n "dbc") (v "0.3.3") (h "042i2zza7jsk76x821lp2w1dlbdi024kp6j2i48nzq6qlzp5f9id")))

