(define-module (crates-io #{3}# d dxh) #:use-module (crates-io))

(define-public crate-dxh-0.0.1 (c (n "dxh") (v "0.0.1") (d (list (d (n "windows") (r "^0.56") (f (quote ("Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)))) (h "18j1j24zbfvdb5dyd3z03wjjf8xmanw77ww9i30k5wz14513hpfl")))

(define-public crate-dxh-0.0.2 (c (n "dxh") (v "0.0.2") (d (list (d (n "windows") (r "^0.56") (f (quote ("Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)))) (h "1w2vsyflb4sr1z6f9jki45c39xmnajxhhfgc3nnd0lwhbgidmg1l")))

(define-public crate-dxh-0.0.3 (c (n "dxh") (v "0.0.3") (d (list (d (n "windows") (r "^0.56") (f (quote ("Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)))) (h "0by4jxcf77nw2jrb7r7wq0lhvyvzdib987hbni2i3fpgx0v4z3v0")))

(define-public crate-dxh-0.0.4 (c (n "dxh") (v "0.0.4") (d (list (d (n "windows") (r "^0.56") (f (quote ("Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)))) (h "0ypxyq1c6rzlh11rlgdzxz99mg7z5lbpqh184rkv7v7m6fs4721i")))

(define-public crate-dxh-0.0.5 (c (n "dxh") (v "0.0.5") (d (list (d (n "windows") (r "^0.56") (f (quote ("Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi_Common"))) (d #t) (k 0)))) (h "0143kv9a3livbngm396if9hrmm6fdqsylz79v0q1ix882drdqrcr")))

