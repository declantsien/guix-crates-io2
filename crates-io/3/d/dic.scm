(define-module (crates-io #{3}# d dic) #:use-module (crates-io))

(define-public crate-dic-0.1.0 (c (n "dic") (v "0.1.0") (h "1f41r0z3q2c2vw6vqa1gn2cl991mb6d06y7x8ybq2ak1mfli6yja") (y #t)))

(define-public crate-dic-0.1.1 (c (n "dic") (v "0.1.1") (h "0fiqvhwa3xq3530g2vs5l26m4g7fddmbfrlxyk76pmblra2yvzrl") (y #t)))

(define-public crate-dic-0.1.2 (c (n "dic") (v "0.1.2") (h "12l815v3b55shw96zv45x61grm9grq2g2zpmmr4wzl0ddx4xlsdy") (y #t)))

(define-public crate-dic-0.1.3 (c (n "dic") (v "0.1.3") (h "0jb4mckszrabyr64z630yi6rsavdss8c0sxjl675r03yxh600icv") (y #t)))

(define-public crate-dic-0.1.4 (c (n "dic") (v "0.1.4") (h "0dsylf0nmfsi1h55p4gv67kz7y4r7705kiv1fcrj397bzv1ikq4y") (y #t)))

(define-public crate-dic-0.1.5 (c (n "dic") (v "0.1.5") (h "0mrsr9pbx4d85cwxjsjh243vlx3jxwqar7s5rqnj9adld5rawahc") (y #t)))

