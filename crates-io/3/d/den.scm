(define-module (crates-io #{3}# d den) #:use-module (crates-io))

(define-public crate-den-0.1.0 (c (n "den") (v "0.1.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "twox-hash") (r "^1") (d #t) (k 0)))) (h "045ad1syaiv99xfzdh4i2apb0q3sjmwl06k92j01riwy4ijs7i3y") (r "1.56")))

(define-public crate-den-0.2.0 (c (n "den") (v "0.2.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "twox-hash") (r "^1") (d #t) (k 0)))) (h "1ld26rx65a691sf4yx3bcjhf8zkm7pn7vgys48c8c7gbn103sjnl") (r "1.56")))

(define-public crate-den-0.2.1 (c (n "den") (v "0.2.1") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "twox-hash") (r "^1") (d #t) (k 0)))) (h "1ailchyp1k6bgm77iilj1fnibn3awdipdk11zmdw9jsmix00s348") (r "1.56")))

(define-public crate-den-0.3.0 (c (n "den") (v "0.3.0") (d (list (d (n "adler32") (r "^1.2") (d #t) (k 0)) (d (n "cyclic-poly-23") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "xxhash-rust") (r "^0.8.5") (f (quote ("xxh3" "xxh64"))) (d #t) (k 0)))) (h "1z3z8szrk1hdczpa6mj9lcqlj8scn78afds8p3izy4pr0kyjx1bx") (r "1.56")))

