(define-module (crates-io #{3}# d dro) #:use-module (crates-io))

(define-public crate-dro-0.1.0 (c (n "dro") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "0qz0ikxb35yaw9bdj8qkxm0flf7kc0r7d4li2kfqqfr0565q02nr")))

(define-public crate-dro-0.1.1 (c (n "dro") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "09idxnvk9gxyg2hi32m4iyl9lji8ivc8jdqjrzacvjlzpmis8dwj")))

(define-public crate-dro-0.1.2 (c (n "dro") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "0pdilqbankfg3y8iwynckdqivchga4vxhqph2343l9zzcri2xx8l")))

(define-public crate-dro-0.1.3 (c (n "dro") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "138dsk7124587nsh26bknspj0r67gw34n91z67vn4jjzssvp3hy4")))

(define-public crate-dro-0.1.4 (c (n "dro") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1mlchwzrxkidxvjgmz0j9x2plhmlydasa4l9yy72ph0vymyzqn1h")))

(define-public crate-dro-0.1.5 (c (n "dro") (v "0.1.5") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1l68f29ixhmmrs2hzhr8bw1hldlf0r0zlan6f0nxcjxm05ks8fp3")))

(define-public crate-dro-0.2.0 (c (n "dro") (v "0.2.0") (d (list (d (n "arw_brr") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "0yphqggm3cb0n3qdzh0b8sj3na4jd39zkpkxx9kgq5i9rnyqrx4g")))

(define-public crate-dro-0.2.3 (c (n "dro") (v "0.2.3") (d (list (d (n "arw_brr") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1b9rl20j7iph2v41djbpszr8nsngk4qssmx1fyj0r41pybhkk7v3")))

(define-public crate-dro-0.2.4 (c (n "dro") (v "0.2.4") (d (list (d (n "arw_brr") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1qdv9jhg8qd4mxn70zx82c59h18k50y5fkr6awqng1nykfrc556d")))

(define-public crate-dro-0.2.5 (c (n "dro") (v "0.2.5") (d (list (d (n "arw_brr") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1hia3xkx2rgb0krs0sarq8vrn4ic78gsfvgdv9lybmdcbgvc1xr0")))

(define-public crate-dro-0.2.6 (c (n "dro") (v "0.2.6") (d (list (d (n "arw_brr") (r "^0.1.0") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "032d1h1vfbs4nfysmk1kv05fnp0v3dy09a2w0mj2sv9c5mcpc5ms")))

(define-public crate-dro-0.2.7 (c (n "dro") (v "0.2.7") (d (list (d (n "arw_brr") (r "^0.1.3") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1lgw3nzsgs14lqjgs0gq5idib3ckvfiqcrisys8vhrmnf2xqh1xa")))

(define-public crate-dro-0.2.8 (c (n "dro") (v "0.2.8") (d (list (d (n "arw_brr") (r "^0.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "0q22f6bw2rx9dxgbhz0mrd849m8570yh82km2j2rvsqkkwzxmqln")))

(define-public crate-dro-0.2.9 (c (n "dro") (v "0.2.9") (d (list (d (n "arw_brr") (r "^0.1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1j04h2c980gs1zy1l60syhyr74lzkz9ydf38jah0a3jnyqgq30q1")))

(define-public crate-dro-0.2.10 (c (n "dro") (v "0.2.10") (d (list (d (n "arw_brr") (r "^0.1.8") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1lddi3a2rb0xrsarnsmgqid2s96c6lhpg1bpjqba3d5yhryx0j45")))

(define-public crate-dro-0.2.11 (c (n "dro") (v "0.2.11") (d (list (d (n "arw_brr") (r "^0.1.8") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1mg5rs8i06xym8vpnr53hngm7qyx5bc9z116pmr0jq0jvd5ypbxa")))

(define-public crate-dro-0.2.12 (c (n "dro") (v "0.2.12") (d (list (d (n "arw_brr") (r "^0.1.8") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "0aca8z2x8lyn4pvx11b1176kxxx0jrkkyczhvx1d07wxksshn08l")))

(define-public crate-dro-0.2.13 (c (n "dro") (v "0.2.13") (d (list (d (n "arw_brr") (r "^0.1.8") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "12wjmkhpp3q566qwzkqymjnvjbvmgabw94dg3lb8arjhjp2877k6")))

(define-public crate-dro-0.2.14 (c (n "dro") (v "0.2.14") (d (list (d (n "arw_brr") (r "^0.1.8") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "0h1sx0pnmqvny7a6wq9bnm6d08dy5qxyp1i25d59f85kbs2xqwcf")))

(define-public crate-dro-0.2.15 (c (n "dro") (v "0.2.15") (d (list (d (n "arw_brr") (r "^0.1.8") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)))) (h "1vl74xdzxqf165jrr93nawmhvc4gg04j3fbbx84gz658q5mv7znq")))

