(define-module (crates-io #{3}# d dwt) #:use-module (crates-io))

(define-public crate-dwt-0.0.1 (c (n "dwt") (v "0.0.1") (h "1xrkhvbzr0jxs8k7wk6gm6w2bqd8xpxrbb4bv1x0fn0qrqpjrky9")))

(define-public crate-dwt-0.1.0 (c (n "dwt") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "18w5ia78qw6snhsbk3lcryjabglrrm0hpxmgmvl0c7l3qiz5xfrr")))

(define-public crate-dwt-0.1.1 (c (n "dwt") (v "0.1.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "18in78n9jv2r9lh8a5v51yr38sjdhmibf0wmmbkfsfbwynzr8cj2")))

(define-public crate-dwt-0.2.0 (c (n "dwt") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "1dis7j2smpb5sqqdchvj0qgfklnj54wm658dsqh3wgnar0ip55dg")))

(define-public crate-dwt-0.2.1 (c (n "dwt") (v "0.2.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "1g883733mbix367pdwxl26yz57cy7pfnr92ygmh08fsnrm0sy0qz")))

(define-public crate-dwt-0.3.0 (c (n "dwt") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)))) (h "041ml2pil27hx71p3l38q0y8hsm7az12c9dmvhjgix1nyw8dlh4w")))

(define-public crate-dwt-0.3.1 (c (n "dwt") (v "0.3.1") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)))) (h "07pqk1gwb10ncjz3zlx8jmrp1zcvsjwlk0zb3gq1lc0xwkiia363")))

(define-public crate-dwt-0.4.0 (c (n "dwt") (v "0.4.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)))) (h "0zpxiqmi5k8wk9gs6k17di3wblrx7gbavdqnp6wz0ln9pv94xqri")))

(define-public crate-dwt-0.5.0 (c (n "dwt") (v "0.5.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.35") (k 0)))) (h "1zff4wf6gvl2296g97nw2id2z8n5ycr83z2g379s0y9vacfk69cg")))

(define-public crate-dwt-0.5.1 (c (n "dwt") (v "0.5.1") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.35") (k 0)))) (h "1asj9xj5hsk9pwkv5qfaapp6m3mh8r78f85r72fzsrhj079w1rqb")))

(define-public crate-dwt-0.5.2 (c (n "dwt") (v "0.5.2") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.35") (k 0)))) (h "0gswnbsz4si5cj4gi7mq940j4crsdaciinfhgx5d3f3jivvw2zsk")))

