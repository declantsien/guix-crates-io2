(define-module (crates-io #{3}# d dia) #:use-module (crates-io))

(define-public crate-dia-0.1.0 (c (n "dia") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dia_core") (r "^0.1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)))) (h "189c3yxj71l552hp3cv3kk8l9q1wybqynyq8h9jkyxpyjw9zz280")))

