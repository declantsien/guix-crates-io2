(define-module (crates-io #{3}# d dot) #:use-module (crates-io))

(define-public crate-dot-0.0.4 (c (n "dot") (v "0.0.4") (h "00b2kcmygl8w3x22hvv9p4a8hz76dffkg5crdjfsp7wpdwwcqr0v")))

(define-public crate-dot-0.1.0 (c (n "dot") (v "0.1.0") (h "1sbz1wyj34fjrj6j66h8z34kfiw8r5bvib3d66lqzcgprkgqblvg")))

(define-public crate-dot-0.1.1 (c (n "dot") (v "0.1.1") (h "1mnkzw2574lkvggxc0a4b1lq9m4hqlr9qd2pij0gr0ficrqhp58m")))

(define-public crate-dot-0.1.2 (c (n "dot") (v "0.1.2") (h "1sx0j4hyaxsy0m832m4jjsbnqm152d85907psvgh36cj7sbn3bbb")))

(define-public crate-dot-0.1.3 (c (n "dot") (v "0.1.3") (h "0pfpmilwdsxzfj1sc1prm4rgfh0h1qbnjwvimmx7hxd3na09pqxa")))

(define-public crate-dot-0.1.4 (c (n "dot") (v "0.1.4") (h "01jr7px6anh3yasz8cbbr9ij989gnwb3qdk4852mzzqw996nqjx7")))

