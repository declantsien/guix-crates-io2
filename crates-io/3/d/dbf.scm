(define-module (crates-io #{3}# d dbf) #:use-module (crates-io))

(define-public crate-dbf-0.1.0 (c (n "dbf") (v "0.1.0") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "0l1kmwfc2zb7rf9f25bcqj9hpy8brmzjnalqynvbkdwslybfgyf0")))

(define-public crate-dbf-0.1.1 (c (n "dbf") (v "0.1.1") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1bygr00f8fa2cxhq8k4bw2fsxsrjma2q1r8gmd3qg7whg3m2lksy")))

