(define-module (crates-io #{3}# d dpy) #:use-module (crates-io))

(define-public crate-dpy-0.0.1 (c (n "dpy") (v "0.0.1") (h "1hzp24kwaifg25ka5s457l4lnxdd6b7xzr5pk0k7j421vq4flm1i")))

(define-public crate-dpy-0.1.0 (c (n "dpy") (v "0.1.0") (h "0q56w1nnmlk19xh1wc96ra5d1xhqbijrmf71qlyxpg71c2mrznpb")))

(define-public crate-dpy-0.2.0 (c (n "dpy") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "which") (r "^4.3.0") (d #t) (k 0)))) (h "1xqq4l6j8ydgdszbfzzvxsk64yzznmkgmki8j6d598ysyjpqj1ax")))

