(define-module (crates-io #{3}# d dvc) #:use-module (crates-io))

(define-public crate-dvc-0.1.0 (c (n "dvc") (v "0.1.0") (d (list (d (n "async-std") (r "^1.11.0") (d #t) (k 0)) (d (n "git2") (r "^0.14.2") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "pyo3") (r "^0.16.2") (f (quote ("auto-initialize"))) (o #t) (d #t) (k 0)) (d (n "rs_transfer") (r "^1.0.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.23") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "0lc43ypp5fm0n8q24nmchkja7xwka1v4wnngw1pa3x3yg6l9cjby") (f (quote (("python_dvc" "pyo3"))))))

