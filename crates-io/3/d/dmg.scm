(define-module (crates-io #{3}# d dmg) #:use-module (crates-io))

(define-public crate-dmg-0.1.0 (c (n "dmg") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "plist") (r "^0.2.2") (k 0)))) (h "1j0wv33x0s3rz6c3mkixhlxwdmw93yqg3817mhcdyxc1lh4q6pg9")))

(define-public crate-dmg-0.1.1 (c (n "dmg") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.4.3") (d #t) (k 2)) (d (n "log") (r "^0.3.8") (d #t) (k 0)) (d (n "plist") (r "^0.2.2") (k 0)))) (h "13yxp3gca7ph0n5djlqwh4v0c7ymik0nqlr0ag3k0h2fwqwmnmky")))

(define-public crate-dmg-0.1.2 (c (n "dmg") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "plist") (r "^1.3.1") (k 0)))) (h "08j7ghx54j7bp6yqk7wc6crb4ggnf51pf3vmnhipz0rp0csqrhmb")))

