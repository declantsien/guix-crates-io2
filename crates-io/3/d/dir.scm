(define-module (crates-io #{3}# d dir) #:use-module (crates-io))

(define-public crate-dir-0.1.2 (c (n "dir") (v "0.1.2") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "home") (r "^0.3") (d #t) (k 0)) (d (n "journaldb") (r "^0.2.0") (d #t) (k 0)) (d (n "vapory-types") (r "^0.8.0") (d #t) (k 0)))) (h "07liqgm2r7pxn3a2y1v3hrgm26rjp6ccr9f7nzxcrgb7d3bszanv")))

