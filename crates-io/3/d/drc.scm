(define-module (crates-io #{3}# d drc) #:use-module (crates-io))

(define-public crate-drc-0.1.0 (c (n "drc") (v "0.1.0") (h "0icwv6bvijdw33hb645vrlyjf4nv57m8nwrgcxnzi0kxbraj8462") (y #t)))

(define-public crate-drc-0.1.1 (c (n "drc") (v "0.1.1") (h "04h0vr83bzxrhmqgjki3m1ff57hjdm1b02xbpqlfpd3jy0cm8znb") (y #t)))

(define-public crate-drc-0.1.2 (c (n "drc") (v "0.1.2") (h "09fxqplqhxvfi2shdb042zfpiyww770x0zmi6nsx6nnmd8g9zpyz")))

