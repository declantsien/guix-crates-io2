(define-module (crates-io #{3}# d drs) #:use-module (crates-io))

(define-public crate-drs-0.1.0 (c (n "drs") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tungstenite") (r "^0.13") (f (quote ("rustls-tls"))) (k 0)) (d (n "ureq") (r "^2.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0b4dad3y3pfva9ccwipaid4sjrms2spqnmgzxw91zkvb6bh8n74j")))

