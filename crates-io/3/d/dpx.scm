(define-module (crates-io #{3}# d dpx) #:use-module (crates-io))

(define-public crate-dpx-0.1.0 (c (n "dpx") (v "0.1.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "pkce") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.1") (d #t) (k 0)))) (h "1r0fb7a1rram1hiyqv9rgb8zv10fiyqjpm2n9nwva8pradfpz8ja")))

(define-public crate-dpx-0.1.1 (c (n "dpx") (v "0.1.1") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "pkce") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "zip-extract") (r "^0.1.1") (d #t) (k 0)))) (h "0wnh834xihybhfdrkfd4y8hf8x1ncdmlg6ga45cpzjnlllhnjaki")))

