(define-module (crates-io #{3}# d dmn) #:use-module (crates-io))

(define-public crate-dmn-0.0.1 (c (n "dmn") (v "0.0.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1dvay1bfx7avdkld3n29vggsj8jkj5fnnnh7x36vywpsijja70jn")))

(define-public crate-dmn-0.0.2 (c (n "dmn") (v "0.0.2") (d (list (d (n "clap") (r "^4.4") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0gcsix4h4rykicrc0b5d4s14gbpyax2f7z0n6xq7d41smazr93jx")))

