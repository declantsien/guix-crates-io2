(define-module (crates-io #{3}# d dmi) #:use-module (crates-io))

(define-public crate-dmi-0.1.2 (c (n "dmi") (v "0.1.2") (d (list (d (n "deflate") (r "^0.9") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s59n2k76a5dmvb1yj08z1n596p1pfkp6h2vbwbs3295bkizcgja")))

(define-public crate-dmi-0.1.3 (c (n "dmi") (v "0.1.3") (d (list (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1x2s663cbv2ncfp9wvw410n9q0mvqzzqaxzjgfbnf0nz1ggpmsf9")))

(define-public crate-dmi-0.2.0 (c (n "dmi") (v "0.2.0") (d (list (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wrj59xvxk2pylk3c28c6idcbwwppy3gkq2zjj15gwl037alljjn")))

(define-public crate-dmi-0.2.1 (c (n "dmi") (v "0.2.1") (d (list (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (f (quote ("png"))) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "18qywa0jx7n70alibfhjj9rhcvv0d0ckdl1sibfqvymp09idbp4z")))

(define-public crate-dmi-0.3.0 (c (n "dmi") (v "0.3.0") (d (list (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "010ykixsh5rj83ybgq6jxfzgcng7fk7af0pd7pn0kfk0fa9zfgfy")))

(define-public crate-dmi-0.3.1 (c (n "dmi") (v "0.3.1") (d (list (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (f (quote ("png"))) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "007rvbid3w455513v7d1jkh6b6x4mppipwf9n893hiwicdq93hfn")))

(define-public crate-dmi-0.3.2 (c (n "dmi") (v "0.3.2") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11kyw89svv4qhxrmw6zf0830kica2qwvr76j1km9bgp876j0iibk") (y #t)))

(define-public crate-dmi-0.3.3 (c (n "dmi") (v "0.3.3") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13sc0iq7qnvzwr7h8xkmascgf0mi77vb6s68if5r9bb1va24fk3m")))

(define-public crate-dmi-0.3.4 (c (n "dmi") (v "0.3.4") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z1gjm09am6hifay0vzqg4r4v2z7a8w45bkdw5zs7iismqnhv6x9")))

(define-public crate-dmi-0.3.5 (c (n "dmi") (v "0.3.5") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "deflate") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.25.1") (f (quote ("png"))) (k 0)) (d (n "inflate") (r "^0.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nh64f4z0zgh07a84diql4y8k5b14pxvggz3inghbr8n00hdprvm")))

