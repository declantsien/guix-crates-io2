(define-module (crates-io #{3}# d dvi) #:use-module (crates-io))

(define-public crate-dvi-0.1.0 (c (n "dvi") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "1l0ncsaysb1xw95n6a9rfhrhgrvg0898xpqj87j0fj55s92d6vs0")))

(define-public crate-dvi-0.2.0 (c (n "dvi") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "070l7p390vqflyzkrsrgwzv061pcgb0g971y1shy61wv110y7d12")))

(define-public crate-dvi-0.2.1 (c (n "dvi") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "nom") (r "^4.0") (d #t) (k 0)))) (h "0w69yr1wp91bwlrr5sqywdnbyqqjvqwzlpi6rbk8nf25rhh6frqi")))

(define-public crate-dvi-0.2.2 (c (n "dvi") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)))) (h "0jpdj6f2rmwdsairks8d934g8yjg3j6y3ppc8mwxg7xxmd1rzaya")))

