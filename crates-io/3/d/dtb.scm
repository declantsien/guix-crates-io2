(define-module (crates-io #{3}# d dtb) #:use-module (crates-io))

(define-public crate-dtb-0.1.0 (c (n "dtb") (v "0.1.0") (h "11fdsb03c0jjazy4z2qgrz69i4k6i2b99dphmk918l568anj1da0")))

(define-public crate-dtb-0.1.1 (c (n "dtb") (v "0.1.1") (h "12cjnaj8zwwn4v1sr3irknlvc59r36ihz4lm4flxxi6qqgq46dwy")))

(define-public crate-dtb-0.1.2 (c (n "dtb") (v "0.1.2") (h "0z7x6k6ini9s4v0wq6hzjq6c5kbwywaj9p4icr3zmczr7lyqlq6w")))

(define-public crate-dtb-0.1.3 (c (n "dtb") (v "0.1.3") (h "1j76a07l8jcji0fiwhlwk7kh2s3x42yxffah0w8zi299g6vg5028")))

(define-public crate-dtb-0.2.0 (c (n "dtb") (v "0.2.0") (h "0wbraym84pkrajxdqqjjgvngpixipq8vli90cpcsni50f8b6wv5y")))

