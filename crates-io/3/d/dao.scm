(define-module (crates-io #{3}# d dao) #:use-module (crates-io))

(define-public crate-dao-0.1.0 (c (n "dao") (v "0.1.0") (d (list (d (n "bigdecimal") (r "^0.0.10") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.5.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0rnvxbawswj0bjm1m2hj3ml2xm17mf7ijd5p022wgq73yd61jiqs")))

