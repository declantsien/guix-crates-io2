(define-module (crates-io #{3}# d dci) #:use-module (crates-io))

(define-public crate-dci-0.1.0 (c (n "dci") (v "0.1.0") (d (list (d (n "bitmatrix") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bitvec") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0m9wqh83yb9lrmn9rf8vfvz0gkq9ifkarailii2p1gxxsp1qwkyx") (f (quote (("sequential") ("parallel" "rayon") ("matrix" "bitmatrix" "bitvec") ("default" "sequential" "parallel"))))))

(define-public crate-dci-0.2.0 (c (n "dci") (v "0.2.0") (d (list (d (n "bitmatrix") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bitvec") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0y4rn1dzh3xix5d0skdn3slhdmca3p1z3m9v1v478pjnglv41lqy") (f (quote (("sequential") ("parallel" "rayon") ("matrix" "bitmatrix" "bitvec") ("default" "sequential" "parallel"))))))

(define-public crate-dci-0.3.0 (c (n "dci") (v "0.3.0") (d (list (d (n "bitmatrix") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "bitvec") (r "^0.19") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "rayon") (r "^1.4") (o #t) (d #t) (k 0)))) (h "0dif03iz134131z99wh3safl96bzxvn36l6y7yxgzxhvvdvq7ply") (f (quote (("sequential") ("parallel" "rayon") ("matrix" "bitmatrix" "bitvec") ("default" "sequential" "parallel"))))))

