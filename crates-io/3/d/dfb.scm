(define-module (crates-io #{3}# d dfb) #:use-module (crates-io))

(define-public crate-dfb-0.1.0 (c (n "dfb") (v "0.1.0") (h "01vr1317klk5xz2rk50cjqrmihhqj6qxscvmirrnb8pnnin69q3j")))

(define-public crate-dfb-0.1.1 (c (n "dfb") (v "0.1.1") (h "0ign4b4hapfj75czy2ir8yh2804f3a4j51d1h9n3jpzmn79nbbxb")))

(define-public crate-dfb-1.0.0 (c (n "dfb") (v "1.0.0") (h "1znkwmqyi0n2nj202ah3rk9y317gp6788862nii44dv0rwbgmxfw")))

(define-public crate-dfb-1.0.1 (c (n "dfb") (v "1.0.1") (h "03lashbai0vimlz8pzdhalj48ndli6lhqdd05w7dxqfxsm6sficf")))

(define-public crate-dfb-1.0.2 (c (n "dfb") (v "1.0.2") (h "08l9f2izg6mk7hph2kk4gd9x17wvl3n7j70dq3z5ry50z60nidf5")))

(define-public crate-dfb-1.0.3 (c (n "dfb") (v "1.0.3") (h "1aayd4cl5mhz3g35wwysv17fcqshdyq5xcy2l8qgqwx9js1mmywq")))

(define-public crate-dfb-1.1.0 (c (n "dfb") (v "1.1.0") (h "155iway8gc70y69igdq2ypizk4k1576lhl0bhf0nqjcs7vl6ln9m")))

(define-public crate-dfb-1.2.0 (c (n "dfb") (v "1.2.0") (h "1bvd00y86v2wa5v0ywwn1l65klw8w19w3nxcx5k95b68b6xd2w9g")))

(define-public crate-dfb-1.3.0 (c (n "dfb") (v "1.3.0") (h "1b3h8f7vl70flq86xh8l6k7z57h15j28pazg4c5w8i7y6m8fvx5v")))

(define-public crate-dfb-1.3.2 (c (n "dfb") (v "1.3.2") (h "1mg928cz02wqjragmcypl4d4g4k1yk4qx7xj66qd78sigvviic17")))

(define-public crate-dfb-1.3.3 (c (n "dfb") (v "1.3.3") (h "1cp145v5w4c01499crsxl1k0slf404q5v0hxblqxx4ka74xs5j6c")))

(define-public crate-dfb-1.4.0 (c (n "dfb") (v "1.4.0") (h "1w5hfldc785gvjh2zxp08f49wxf5lmdlqvx05d1mj4xc77niqpxp")))

