(define-module (crates-io #{3}# d dbi) #:use-module (crates-io))

(define-public crate-dbi-0.1.0 (c (n "dbi") (v "0.1.0") (d (list (d (n "dbi-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "mysql_async") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "1rcac8jf6abhy9xx259wpw0irf3jbhpdq8y3r64z6612qajrq6lh")))

(define-public crate-dbi-0.2.0 (c (n "dbi") (v "0.2.0") (d (list (d (n "dbi-macros") (r "^0.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "mysql_async") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "0789p8g27rdc39i8zc9cc38aasybf1hs51ainfgdz42f4v0vz477") (f (quote (("mysql" "dbi-macros/mysql") ("default" "mysql"))))))

(define-public crate-dbi-0.2.1 (c (n "dbi") (v "0.2.1") (d (list (d (n "dbi-macros") (r "^0.2.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "mysql_async") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "1h545g4jpjb4qzzh3nbivja97j2lnz20j4zwjsga98pz3wz3615b") (f (quote (("mysql" "dbi-macros/mysql") ("default" "mysql"))))))

(define-public crate-dbi-0.2.2 (c (n "dbi") (v "0.2.2") (d (list (d (n "dbi-macros") (r "^0.2.2") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "mysql_async") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "11v1n1yskz8dymxaz9vsyp7qrps8fgqlippw3hf7ppa9iw1g84m8") (f (quote (("mysql" "dbi-macros/mysql") ("default" "mysql"))))))

(define-public crate-dbi-0.2.3 (c (n "dbi") (v "0.2.3") (d (list (d (n "dbi-macros") (r "^0.2.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "mysql_async") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "1kn52zm3ir42p1kh6xscninji7iwdsf8ng44axmmw04a40z9b2ks") (f (quote (("mysql" "dbi-macros/mysql") ("default" "mysql"))))))

(define-public crate-dbi-0.2.4 (c (n "dbi") (v "0.2.4") (d (list (d (n "dbi-macros") (r "^0.2.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "mysql_async") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "133a2k3w337vkj6pyxg2hj8wz3k3dc979jyx75mmd8l78whhapbr") (f (quote (("mysql" "dbi-macros/mysql") ("default" "mysql"))))))

(define-public crate-dbi-0.3.0 (c (n "dbi") (v "0.3.0") (d (list (d (n "dbi-macros") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "mysql_async") (r "^0.15.1") (d #t) (k 0)) (d (n "tokio") (r "^0.1.11") (d #t) (k 0)))) (h "1h4w9by64nwdby04k3sa0jkng6hrflmh1pd490bv8pa29hs8pqw3") (f (quote (("mysql" "dbi-macros/mysql") ("default" "mysql"))))))

