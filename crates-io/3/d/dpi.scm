(define-module (crates-io #{3}# d dpi) #:use-module (crates-io))

(define-public crate-dpi-0.0.0 (c (n "dpi") (v "0.0.0") (h "1l7n9jfba8lq9vwzg66wlxmf2sp8gzypbgr692fvnpz3z8g74hx1")))

(define-public crate-dpi-0.1.0 (c (n "dpi") (v "0.1.0") (d (list (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "03x5i4wkhz6fq0fg6g4iizl5xgfsh2yvi047vs3wnj93rh6zkrai") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint")))) (r "1.70.0")))

(define-public crate-dpi-0.1.1 (c (n "dpi") (v "0.1.1") (d (list (d (n "mint") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "0lzz48gpgbwdrw0s8vib0589ij9jizv1vzsphm4xd9kw58lhwp7j") (s 2) (e (quote (("serde" "dep:serde") ("mint" "dep:mint")))) (r "1.70.0")))

