(define-module (crates-io #{3}# d dsg) #:use-module (crates-io))

(define-public crate-dsg-0.1.0 (c (n "dsg") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.300") (d #t) (k 0)) (d (n "tokio") (r "^1.1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "148n5cn20fzk95cw57dyg8pfdkrmz6wkfbjq4hvdh5pslm7r8j16")))

