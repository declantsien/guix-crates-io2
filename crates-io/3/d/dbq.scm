(define-module (crates-io #{3}# d dbq) #:use-module (crates-io))

(define-public crate-dbq-0.1.0 (c (n "dbq") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (f (quote ("with-serde_json" "with-chrono"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "083c9qkjnywc8i89bc652jf75lpi5q6826afikfganrr5h1ahq11") (f (quote (("integration_tests" "env_logger"))))))

