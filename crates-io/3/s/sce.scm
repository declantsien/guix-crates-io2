(define-module (crates-io #{3}# s sce) #:use-module (crates-io))

(define-public crate-sce-0.1.0 (c (n "sce") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "flate2") (r "^1.0.19") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sprs") (r "^0.9.4") (d #t) (k 0)))) (h "1agc7bqay0zgrf0jbawydh3133vzrdlikdgq05qsa1c8fidlmg32")))

(define-public crate-sce-0.1.1 (c (n "sce") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "flate2") (r "^1.0.19") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "sprs") (r "^0.10.0") (d #t) (k 0)))) (h "0kpxblyp77ki0cfyssrhn7xb1kvg6hlhajnpa28qybw9a1zngvdc")))

(define-public crate-sce-0.1.2 (c (n "sce") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "flate2") (r "^1.0.23") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "sprs") (r "^0.11.0") (d #t) (k 0)))) (h "1fv0nbx6r44rcqfy6881gin30d92zgxl0jrx4hfl725h0425i74j")))

(define-public crate-sce-0.2.0 (c (n "sce") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "sprs") (r "^0.11.1") (d #t) (k 0)) (d (n "trait-set") (r "^0.3.0") (d #t) (k 0)))) (h "1xjx21hypglmf42l1nzdrp5js019mmfpp21q9rjg9m38l91ar1iy")))

