(define-module (crates-io #{3}# s skr) #:use-module (crates-io))

(define-public crate-skr-0.0.1 (c (n "skr") (v "0.0.1") (h "1lwb3f08hfz8v8s5l4qv3d6m27lbs3vapq1a101gw53c6fixb0v6")))

(define-public crate-skr-0.0.2 (c (n "skr") (v "0.0.2") (d (list (d (n "lego") (r "^0.0.1") (d #t) (k 0)))) (h "0vkcivi82zawrrkzp3chr16rrb4dl8p44hmgxv91jmbgv1nsm2lx")))

