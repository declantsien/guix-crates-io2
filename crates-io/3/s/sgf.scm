(define-module (crates-io #{3}# s sgf) #:use-module (crates-io))

(define-public crate-sgf-0.1.0 (c (n "sgf") (v "0.1.0") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "1jax5mwd15iv2xk94j7wdla9srsf3kmcdbbrim8160w35xg8ngcb")))

(define-public crate-sgf-0.1.1 (c (n "sgf") (v "0.1.1") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "0gar3ip8z5f4jqns7m9janl6ynlqf656mjyrxqwhccj7gbmqal21")))

(define-public crate-sgf-0.1.2 (c (n "sgf") (v "0.1.2") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "074iy23ddj3fa8c5hhjp8qi293mjddzz4h4v575mycgg7iix793s")))

(define-public crate-sgf-0.1.3 (c (n "sgf") (v "0.1.3") (d (list (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "0ivq08vfafcpgf0b2s4fa39yqwacmrf1x70bqqhq3inibpwgic9w")))

(define-public crate-sgf-0.1.5 (c (n "sgf") (v "0.1.5") (d (list (d (n "peg") (r "^0.5.1") (d #t) (k 1)) (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "0gxhw4i9g6psw1cqsz25vq0m41kc3bca4qlqwhapf13n27ylnv3l")))

