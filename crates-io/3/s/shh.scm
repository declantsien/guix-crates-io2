(define-module (crates-io #{3}# s shh) #:use-module (crates-io))

(define-public crate-shh-0.1.0 (c (n "shh") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "wincon" "processenv" "handleapi" "namedpipeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fwdalhfv8gnlpmqzyg2w922pg19m8j7pc14rp8kp3rap3bmhi9z")))

(define-public crate-shh-0.1.1 (c (n "shh") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "wincon" "processenv" "handleapi" "namedpipeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0j2h7c8f1dh94ky9j1g3k4z72m69wbb6aaz9zz0gmx8qx2lg1sf5")))

(define-public crate-shh-0.1.2 (c (n "shh") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "wincon" "processenv" "handleapi" "namedpipeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rszw5yd69vcqgj8k4rn20mqsvw1v679crrqacsyg2pyz122k03r")))

(define-public crate-shh-1.0.0 (c (n "shh") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "wincon" "processenv" "handleapi" "namedpipeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0r09x8lh2pf9qqmm6qalga1y562dpl008gy9v420ca9x3laqpzww")))

(define-public crate-shh-1.0.1 (c (n "shh") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "wincon" "processenv" "handleapi" "namedpipeapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ayk4v4i991c6vpdw698qd10sqahl3sayw3lgg3qxgn8k83yn1aj")))

