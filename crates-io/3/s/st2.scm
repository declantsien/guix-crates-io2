(define-module (crates-io #{3}# s st2) #:use-module (crates-io))

(define-public crate-st2-0.0.0 (c (n "st2") (v "0.0.0") (h "12pjpn9la53pzi6mfvjifypkqyy35yi8rmsc2ky4s6qhszi4zq98") (y #t)))

(define-public crate-st2-0.0.1 (c (n "st2") (v "0.0.1") (h "0c0hcd2l9pxg79zsnc2w8cihvs94fdbvz0f68d92i3i8zw7gxzbp") (y #t)))

(define-public crate-st2-0.9.0 (c (n "st2") (v "0.9.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "06fc8whxfl7cdxq9wfg85m0hpa3iwgjnz7fp8wrszz1y92wz3sfd") (y #t)))

(define-public crate-st2-0.9.1 (c (n "st2") (v "0.9.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "189i59pnr3p5hyjknsw1mnqw4lpvagcnwynziqpsqdc73dq6a9hr") (y #t)))

