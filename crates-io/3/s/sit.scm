(define-module (crates-io #{3}# s sit) #:use-module (crates-io))

(define-public crate-sit-0.1.0 (c (n "sit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13wrgm2bbw8xm9amh0qkanblmx7bgj1jn255ancd7ga85099imri")))

(define-public crate-sit-0.1.1 (c (n "sit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nsamskmvzk2ldslp697mbyqk0c688yqgppi738cyffvcwm1kl4k")))

(define-public crate-sit-0.1.2 (c (n "sit") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jhmq084wxb50iwqvlck19j7lkqb84akm0mavmc2ag9ij79znq5i")))

