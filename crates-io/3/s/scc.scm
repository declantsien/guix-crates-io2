(define-module (crates-io #{3}# s scc) #:use-module (crates-io))

(define-public crate-scc-0.2.0 (c (n "scc") (v "0.2.0") (d (list (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "19pr84s3klpczfhlc09zgpmm89kb2i95jxkfz2lsb3qfzv6nibxi") (y #t)))

(define-public crate-scc-0.2.1 (c (n "scc") (v "0.2.1") (d (list (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "13pzn66gqlzsbk198dwx8z3mdfm8f393y8ggy3fz0ivvkk085jxn") (y #t)))

(define-public crate-scc-0.2.2 (c (n "scc") (v "0.2.2") (d (list (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "1n9hp8ih7bdbzi8c0zjca4a1kr2rkrhakfx9mam7nib099fgvnzx") (y #t)))

(define-public crate-scc-0.2.3 (c (n "scc") (v "0.2.3") (d (list (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0j7jf1riv0vilrx6hzpbd6pkfb8x0iv1q1iy4md9flnv2i9iyrw2") (y #t)))

(define-public crate-scc-0.2.4 (c (n "scc") (v "0.2.4") (d (list (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0w8h78m180n4gw04n7hippg4lcnclbdapf61yzvrsyi16n91lmvx") (y #t)))

(define-public crate-scc-0.2.5 (c (n "scc") (v "0.2.5") (d (list (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)))) (h "0g986kdkdawszkbnznm68imd15vcs11akqkbpq86idml0y24airy") (y #t)))

(define-public crate-scc-0.2.6 (c (n "scc") (v "0.2.6") (d (list (d (n "crossbeam-epoch") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)))) (h "1s37j3484731g1apr8ci80q12mmba4hjqxnyps2r5812ld156bvh") (y #t)))

(define-public crate-scc-0.2.7 (c (n "scc") (v "0.2.7") (d (list (d (n "crossbeam-epoch") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)))) (h "0g8js53hwkmlcmhzw9xan1jv7ns18wy7h40y07bwip428jvkxkzh") (y #t)))

(define-public crate-scc-0.2.8 (c (n "scc") (v "0.2.8") (d (list (d (n "crossbeam-epoch") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r ">=1.1.0, <2.0.0") (d #t) (k 0)))) (h "1385crvph23crxckrkx4a7gg0r9dmcgm5r05xfgd77d2dgpsgbda") (y #t)))

(define-public crate-scc-0.2.9 (c (n "scc") (v "0.2.9") (d (list (d (n "crossbeam-epoch") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r ">=1.1.0, <2.0.0") (d #t) (k 0)))) (h "1nsprd6dd8jkb5idpmimqcvpj6nprl0s7nsz8yfrkh5i5gvplsyi") (y #t)))

(define-public crate-scc-0.2.10 (c (n "scc") (v "0.2.10") (d (list (d (n "crossbeam-epoch") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "libc") (r ">=0.2.80, <0.3.0") (d #t) (k 0)) (d (n "scopeguard") (r ">=1.1.0, <2.0.0") (d #t) (k 0)))) (h "18ajgmiams2kv05dbl4zsrib651dp9szlk5b0a81zd900q83hc3j") (y #t)))

(define-public crate-scc-0.2.11 (c (n "scc") (v "0.2.11") (d (list (d (n "crossbeam-epoch") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "scopeguard") (r ">=1.1.0, <2.0.0") (d #t) (k 0)))) (h "0c0bp3rnbwrzzycnjaf4475kkk33hr0kdfs19gbzzf0clnzlzixm") (y #t)))

(define-public crate-scc-0.2.12 (c (n "scc") (v "0.2.12") (d (list (d (n "crossbeam-epoch") (r ">=0.9.1, <0.10.0") (d #t) (k 0)) (d (n "scopeguard") (r ">=1.1.0, <2.0.0") (d #t) (k 0)))) (h "0gag5g11qsv1hmg9yglfkfq37grar2q4f2wcxxpld36j2qb9wbvl") (y #t)))

(define-public crate-scc-0.2.13 (c (n "scc") (v "0.2.13") (d (list (d (n "crossbeam-epoch") (r ">=0.9.1, <0.10.0") (d #t) (k 0)) (d (n "scopeguard") (r ">=1.1.0, <2.0.0") (d #t) (k 0)))) (h "1sir6ds4q6nb92bbqy05a4kr96sg51x1kkcljkpqw7h16mfs4r5q") (y #t)))

(define-public crate-scc-0.3.0 (c (n "scc") (v "0.3.0") (d (list (d (n "crossbeam-epoch") (r ">=0.9.1, <0.10.0") (d #t) (k 0)) (d (n "scopeguard") (r ">=1.1.0, <2.0.0") (d #t) (k 0)))) (h "1zwbfm5isykqnjv5qi9ycmkjvqgyvalpv9bjzfhrx21fhlk1p3sq") (y #t)))

(define-public crate-scc-0.3.1 (c (n "scc") (v "0.3.1") (d (list (d (n "crossbeam-epoch") (r ">=0.9.1, <0.10.0") (d #t) (k 0)) (d (n "scopeguard") (r ">=1.1.0, <2.0.0") (d #t) (k 0)))) (h "1qs665bzrjx4kjsyns9hlmpy1y3zqj3lpclhga049y108xx6jqf1") (y #t)))

(define-public crate-scc-0.3.2 (c (n "scc") (v "0.3.2") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0in848invs04id3in8l5jazklx3n3iahx6anlbflpfr2y2cdmw69") (y #t)))

(define-public crate-scc-0.3.3 (c (n "scc") (v "0.3.3") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "16y9v5lf8jffc3v7s6dx3wbnw8n1s12nhad9m7rxr3nf7vgrdv3q") (y #t)))

(define-public crate-scc-0.3.5 (c (n "scc") (v "0.3.5") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0xfnq8yfijkk9nk4kd3h8j774gc1k0ix8ic9jd477nd95nnkbhy2") (y #t)))

(define-public crate-scc-0.3.6 (c (n "scc") (v "0.3.6") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "014415vr3np9l1sgzk2cqdmvcl9b5ggk1hdfi1j07dr4yrh9fhg3") (y #t)))

(define-public crate-scc-0.3.7 (c (n "scc") (v "0.3.7") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "19nq9cwdvxkddhfa5fjz6fm6bjy9r0sx8dhlyak9kawxisykvr0r") (y #t)))

(define-public crate-scc-0.3.8 (c (n "scc") (v "0.3.8") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0r0nldvixbzj1j45jddrfd7ifzcggwx55vh4ddyvypw22ys76s3p") (y #t)))

(define-public crate-scc-0.3.10 (c (n "scc") (v "0.3.10") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "08dvavma5sfxx092q0mrc7qdrsnhhx5w885k3yn9p0bwq7z1da7v") (y #t)))

(define-public crate-scc-0.3.11 (c (n "scc") (v "0.3.11") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "04l4w3xiyqiij81ad8f9r9qa3lmknr0myv5srr8wrl11g3xbwlnm") (y #t)))

(define-public crate-scc-0.3.12 (c (n "scc") (v "0.3.12") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0h52vfmlwnk8ad50mx3ginz3nnbih0j9k48c34ik6y9k6764zar5") (y #t)))

(define-public crate-scc-0.3.13 (c (n "scc") (v "0.3.13") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0s6c1xvhxxhhzj1vzzmkb8cq6sjz03li7md9hjygy9wa0d3nh8cp") (y #t)))

(define-public crate-scc-0.3.14 (c (n "scc") (v "0.3.14") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "04my4sa4wgabkb86p5qjc641zxmqzx8zbq0y5qprf3pwy3lsbmcn") (y #t)))

(define-public crate-scc-0.3.15 (c (n "scc") (v "0.3.15") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0kxciwpq7zipdqp76wagigm3clgndv12szcw7iqwjb1fq73znp1h") (y #t)))

(define-public crate-scc-0.3.16 (c (n "scc") (v "0.3.16") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "13xchqgmw23n85xmvp60rvilf02hn0w5n4l1m4d44d6gajjz3bg8") (y #t)))

(define-public crate-scc-0.3.17 (c (n "scc") (v "0.3.17") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "16qx0z2jgasldaapb7704lb659kryd2kns6z2dkkhdpw8yv53g81") (y #t)))

(define-public crate-scc-0.3.18 (c (n "scc") (v "0.3.18") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "04iwcish9vycmsxmnwyk2ic13jph31j6r180b9793fq998j1wlx8") (y #t)))

(define-public crate-scc-0.3.19 (c (n "scc") (v "0.3.19") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0wnlamaji7xl5yldrnl100i7b6360xwb0yl3zqnv4sqxqra824fx") (y #t)))

(define-public crate-scc-0.3.20 (c (n "scc") (v "0.3.20") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "06w15cfciml2v1blbqnqsagsjscy4nday9lhraj6r84p4sq74xch") (y #t)))

(define-public crate-scc-0.3.21 (c (n "scc") (v "0.3.21") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0d74vkmml6h6fjvi8pl6a1cirq2bg3wg9h98pg9mzap5sp2n53c5") (y #t)))

(define-public crate-scc-0.3.22 (c (n "scc") (v "0.3.22") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0rp0nxwq44w4p6g9hdprdacj9j9n403skkh0kyjprknh87nqjzb1") (y #t)))

(define-public crate-scc-0.4.0 (c (n "scc") (v "0.4.0") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "06077lv22s2qcrxvsznkn4hzd2vlbbv2p43j2zkr42ysglsikr5i") (y #t)))

(define-public crate-scc-0.4.1 (c (n "scc") (v "0.4.1") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0swzai4v8lfgygna1jn50l4s2h507dx44nk4jsvlcm9lrpr9nlsd") (y #t)))

(define-public crate-scc-0.4.2 (c (n "scc") (v "0.4.2") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0fwcya2m9pj7mbzbjs14gjclnxc0w049llpg42vzzfjkqflrq2qz") (y #t)))

(define-public crate-scc-0.4.3 (c (n "scc") (v "0.4.3") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0wklyffpa9bhjbch3k2x06r87k6naq08z853d2jifj1mj02j6vqi") (y #t)))

(define-public crate-scc-0.4.4 (c (n "scc") (v "0.4.4") (d (list (d (n "crossbeam-epoch") (r "^0.9.1") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0arqjxlnaimlw39wjcxfrqhmhwc7gkv21pmcszrbhxn5lygcjsd5") (y #t)))

(define-public crate-scc-0.4.5 (c (n "scc") (v "0.4.5") (d (list (d (n "crossbeam-epoch") (r "^0.9.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "13b8qxbkqbj5jw9ysnjscvv4g97m19c3bhkqvbr7npl2w86jzl8f") (y #t)))

(define-public crate-scc-0.4.6 (c (n "scc") (v "0.4.6") (d (list (d (n "crossbeam-epoch") (r "^0.9.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "10kb6lcy2v63zr1daz9dzr0xa002yadsxlfflqa9gzlmi3ma1dw0") (y #t)))

(define-public crate-scc-0.4.7 (c (n "scc") (v "0.4.7") (d (list (d (n "crossbeam-epoch") (r "^0.9.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1lmnpnkjfwhw59ja5km8yzcmpcsbvaxbzr8w91rvba3m36hmmkx7") (y #t)))

(define-public crate-scc-0.4.8 (c (n "scc") (v "0.4.8") (d (list (d (n "crossbeam-epoch") (r "^0.9.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "06xz8p9s0wzjhm0mk4j9ifd8q4s83b364q9hdlpiwfh8wq75qrr8") (y #t)))

(define-public crate-scc-0.4.9 (c (n "scc") (v "0.4.9") (d (list (d (n "crossbeam-epoch") (r "^0.9.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "14cg3lx06hihq17xfxhqan14fhdvkc0gfrc6hmhxzjwhxmyrw1sl") (y #t)))

(define-public crate-scc-0.4.10 (c (n "scc") (v "0.4.10") (d (list (d (n "crossbeam-epoch") (r "^0.9.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1k4gjbdywsn9yiz0hc8i97z0lk39k0kmv17j2r7kyzm9mf8ysxgg") (y #t)))

(define-public crate-scc-0.4.11 (c (n "scc") (v "0.4.11") (d (list (d (n "crossbeam-epoch") (r "^0.9.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1kkashxrwbv48c9mp2banq8p77k3x1pwj25p594ahp5c7332d0fn") (y #t)))

(define-public crate-scc-0.4.12 (c (n "scc") (v "0.4.12") (d (list (d (n "crossbeam-epoch") (r "^0.9.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1akk2jaz6zm4d5qc91c4f052rbnw2xdi4c7wdi48hmrj5lg08270") (y #t)))

(define-public crate-scc-0.4.13 (c (n "scc") (v "0.4.13") (d (list (d (n "crossbeam-epoch") (r "^0.9.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1d53crb42cv2arp8nzk2zjd95a4zrlbvvwgn4vhnkgqq7sr688g2") (y #t)))

(define-public crate-scc-0.4.14 (c (n "scc") (v "0.4.14") (d (list (d (n "crossbeam-epoch") (r "^0.9.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "017444s2a7prsydjrx4k998f3srwliq2dr9vv4icklaa4hdhq8aj") (y #t)))

(define-public crate-scc-0.5.0 (c (n "scc") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "loom") (r "^0.5.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1zqfky0njibyi7k3qq7zjjzm6srd4zdmwl0i41gll29mhfxvr4cp") (y #t)))

(define-public crate-scc-0.5.1 (c (n "scc") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "loom") (r "^0.5.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1jrqdl4cjwlc81lwzbw503zvis29yixr6hxcz81d7bapi4mwaqb0") (y #t)))

(define-public crate-scc-0.5.2 (c (n "scc") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "loom") (r "^0.5.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1dn02p815h2zbicxfjykw33agp7aqg4cxgm9ng89w4mh4yph5mip") (y #t)))

(define-public crate-scc-0.5.3 (c (n "scc") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "loom") (r "^0.5.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1msh0qga687pdxwnwb5kdyclvamv3zck4dkch4s4i0a7sqqnrryf") (y #t)))

(define-public crate-scc-0.5.4 (c (n "scc") (v "0.5.4") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "loom") (r "^0.5.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1305iw203pmag337mhpsqh2hcvqjfvxz75zhwngvyg9v2ii8nm7j") (y #t)))

(define-public crate-scc-0.5.5 (c (n "scc") (v "0.5.5") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "loom") (r "^0.5.1") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0dacpzq4wwd310l8b60n78kpdi7bnwcfsj2i8sfs931gl9j4z3zp") (y #t)))

(define-public crate-scc-0.5.6 (c (n "scc") (v "0.5.6") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "157bnl4dr8b4hy12wr1q0qjhmy3z8mr5cvnp953i89yxc0k37vmj") (y #t)))

(define-public crate-scc-0.5.7 (c (n "scc") (v "0.5.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)))) (h "022pbxamhgi3xb1ch64f72d068kdzfydyabspy5hzl0lqcxjc60y") (y #t)))

(define-public crate-scc-0.5.8 (c (n "scc") (v "0.5.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)))) (h "0ci59dfh66flknx47k05g97ikacjwc27sl1af3qsxkgf1aj0cdmd") (y #t)))

(define-public crate-scc-0.6.0 (c (n "scc") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0x5v6nlahw1jxyd6rn8nx99msx8x66wsqh0jv2nxf58c7dwp4b17") (y #t)))

(define-public crate-scc-0.6.1 (c (n "scc") (v "0.6.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0hqwlas6sp0qpn5vam5852qdynd60dccybymc5y2n9pjrk784i4y") (y #t)))

(define-public crate-scc-0.6.2 (c (n "scc") (v "0.6.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "07dwwv8ij9j4kfvrqnx4plbhs8hx1rf9g8518gmx9pjshbx3v7kp") (y #t)))

(define-public crate-scc-0.6.3 (c (n "scc") (v "0.6.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1avnbbjv2mwydlv2x6a9ip4h6f6jrr52pw1ji827hvgv78xvgjw5") (y #t)))

(define-public crate-scc-0.6.4 (c (n "scc") (v "0.6.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0j366ngxvf5a6ziplb4s0c2pvd1pwxiw3l45w0hcd6ic9vc3f8rz") (y #t)))

(define-public crate-scc-0.6.5 (c (n "scc") (v "0.6.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "17ai37sw5k2a844cy633n6zl6kmsvxxa7b9csrfwc9k1zwnhiyrs") (y #t)))

(define-public crate-scc-0.6.6 (c (n "scc") (v "0.6.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1plnqq2v69wg1av2gc4qxm1fc6cdjlzrm5c0iasjkk48xbf1908c") (y #t)))

(define-public crate-scc-0.6.7 (c (n "scc") (v "0.6.7") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "00js8mb718jkgifzjmpw3jdgfymzlxmky0zdrjs5vyrwzcjqbayl") (y #t)))

(define-public crate-scc-0.6.8 (c (n "scc") (v "0.6.8") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0zj1gmp2r3gw8cw1z2yxaw172iixz4pzcsivmy78m9wkxr3rwhdj") (y #t)))

(define-public crate-scc-0.6.9 (c (n "scc") (v "0.6.9") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18.0") (f (quote ("full"))) (d #t) (k 2)))) (h "00c78xkdv2zb4q4jyrn8s8fcvf8q6wqs464i6wja6mdr2cakilp2") (y #t)))

(define-public crate-scc-0.6.10 (c (n "scc") (v "0.6.10") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 2)))) (h "0cl2d7ji7dwb7ql9xcynz3agwc5nph3mcl91symncsdgx7yshy05") (y #t)))

(define-public crate-scc-0.7.0 (c (n "scc") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 2)))) (h "10cd0ahp5m734j80djnp9hp4abxgcrij8vz6sn5249i7cyjhp0ha") (y #t)))

(define-public crate-scc-0.7.1 (c (n "scc") (v "0.7.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 2)))) (h "0rcnh0ph9y7ia9xjykc4rzkdd9v5kq5nl3y05xmkzafn3hkyfax6") (y #t)))

(define-public crate-scc-0.8.0 (c (n "scc") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 2)))) (h "1lbmhsslff4hc4d2xmcawy4n3qkzklyk0yvabc5kfd0zzxm8kxf1") (y #t)))

(define-public crate-scc-0.8.1 (c (n "scc") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 2)))) (h "172khckzmv9k3l0s2p84q980ivyxz4a8z6a8s37qvr7awxhskc3n") (y #t)))

(define-public crate-scc-0.8.2 (c (n "scc") (v "0.8.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 2)))) (h "1dcllk82nq5h6a6920cx81j1p1jl3dzwhjqq7pzjxvgvvzi4z9c5") (y #t)))

(define-public crate-scc-0.8.3 (c (n "scc") (v "0.8.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (f (quote ("full"))) (d #t) (k 2)))) (h "1p83vg5r2g8q7wdg3lzlmgsa4zcgfpk0fh99iaxdyl4dz0zmkqcb") (y #t)))

(define-public crate-scc-0.8.4 (c (n "scc") (v "0.8.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)))) (h "0p56q6mx1z0akda7fvd027k7vyqmvlxvqn8b5ssgiipmv0ywaxjx") (y #t)))

(define-public crate-scc-0.9.0 (c (n "scc") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)))) (h "1hfdhw3kzpf0350yp148p3bb0w8ws08g50sb82s4gprssxr51xrb") (y #t)))

(define-public crate-scc-0.9.1 (c (n "scc") (v "0.9.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)))) (h "1qxs7yan11yy008q186836df2z6xwv4b1r2yvkfrjn3zm7wb2b54") (y #t)))

(define-public crate-scc-0.10.0 (c (n "scc") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)))) (h "1b471g3hy3a0kyzxpi24c95q2lyv50zzyrfxfrf1bsqs5r5b43d6") (y #t)))

(define-public crate-scc-0.10.1 (c (n "scc") (v "0.10.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)))) (h "15f3p5vj6pxds3s4pz3zqrd349wrfg9xx1sc5mpxrj2gxcmd04zi") (y #t)))

(define-public crate-scc-0.10.2 (c (n "scc") (v "0.10.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)))) (h "0g5vfdwz6yf6l4jyipsfjk8d6kbfj46m9jrm9cxicbm9f16p210a") (y #t)))

(define-public crate-scc-0.11.0 (c (n "scc") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20") (f (quote ("full"))) (d #t) (k 2)))) (h "1bw15652qr3v0m4zl2s7qpnizhmks6783r11iqc1gfk6nxhaxd53") (y #t)))

(define-public crate-scc-0.11.1 (c (n "scc") (v "0.11.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 2)))) (h "0f9n7nw39lq38vprhzfi9xgkkzgxlhicjp2bbajcz9h6350sck85") (y #t)))

(define-public crate-scc-0.11.2 (c (n "scc") (v "0.11.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 2)))) (h "1p416fxd0cq582rxz1xyan2xn4ll6vgql8lkjx9psvbxmizkh55i") (y #t)))

(define-public crate-scc-0.11.3 (c (n "scc") (v "0.11.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 2)))) (h "16j6hy9hkx0wp3cy1pymka2h5gakj4sinrckg633x70fz7i341cq") (y #t)))

(define-public crate-scc-0.11.4 (c (n "scc") (v "0.11.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 2)))) (h "022lyzjs5rr3k611rjxba1a4sjfq9z9mcx3a2cf4hisdxl4kf92b") (y #t)))

(define-public crate-scc-0.11.5 (c (n "scc") (v "0.11.5") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.21") (f (quote ("full"))) (d #t) (k 2)))) (h "1qzhjhifrn6gwz5aa83zb9kp5h0l35gqr4vzqdpdq61zp79wpqh5") (y #t)))

(define-public crate-scc-0.12.0 (c (n "scc") (v "0.12.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)))) (h "04pyq2j9irr150gkg8k4p1h1v0n8aq7gf2c7j128d58g8ivka4kw") (y #t)))

(define-public crate-scc-0.12.1 (c (n "scc") (v "0.12.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)))) (h "0ryqwqvl3hvydjzpmb9wcm56wwaqgzyhblakj12hh8fh3bsn7xh5") (y #t)))

(define-public crate-scc-0.12.2 (c (n "scc") (v "0.12.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)))) (h "1v18qdqs0s0dizbn2y8fkgbsymxq4fxic92sn0iba2d4zjalywhf") (y #t)))

(define-public crate-scc-0.12.3 (c (n "scc") (v "0.12.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)))) (h "0qxd3l7ks7spn60vfr9pqqa9w72mqphlay1lwb2i4g2lmymbfyjc") (y #t)))

(define-public crate-scc-0.12.4 (c (n "scc") (v "0.12.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)))) (h "0fqw7p3g3ckqk38n4v67amh8q7xcf5jw158zrjnjrqcrbimkdig1") (y #t)))

(define-public crate-scc-1.0.0 (c (n "scc") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)))) (h "0l6bh52crz505mhw9dpmbhs27nmma8s8l8l8g4y2wlf5wq6xby3k") (y #t)))

(define-public crate-scc-1.0.1 (c (n "scc") (v "1.0.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)))) (h "1w04igpspk4f12mp39gs3f5q8grdgkah7hbk3j0xiynavsyxm21v") (y #t)))

(define-public crate-scc-1.0.2 (c (n "scc") (v "1.0.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.23") (f (quote ("full"))) (d #t) (k 2)))) (h "002w8xf630gi5h8ks85rfphbksdl8z6lxp1fblgcfagknh5ad7v2") (y #t)))

(define-public crate-scc-1.0.3 (c (n "scc") (v "1.0.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1b3h71aisrxvf3v9yy7i42jzkkd7smhlni8dfdlf821w0f20gblh") (y #t)))

(define-public crate-scc-1.0.4 (c (n "scc") (v "1.0.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1z06k1za90gnk3cfy3y7dkcz4k276b284kd8z96kcpq8hrfw4gz2") (y #t)))

(define-public crate-scc-1.0.5 (c (n "scc") (v "1.0.5") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "17n2h43kdvjz2qf3i69fb29d41pbmkvcq4cpbzp0sya5q04nfcm9") (y #t)))

(define-public crate-scc-1.0.6 (c (n "scc") (v "1.0.6") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "105vm2n06vpqja5cpkw4x6szch2n63w855493fjhk59b4y8prglm") (y #t)))

(define-public crate-scc-1.0.7 (c (n "scc") (v "1.0.7") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0wfi1c5jyakz31gk7b6fvfmc3xkld1yyxi49p7qxqfi9sy8rgwsf") (y #t)))

(define-public crate-scc-1.0.8 (c (n "scc") (v "1.0.8") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1bb2nrn348rpg0l6mbm2c5vyaz5qwfjvpf0vxs6lvprn826sk6jk") (y #t)))

(define-public crate-scc-1.0.9 (c (n "scc") (v "1.0.9") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1rdydci8jimfgcilvc52gy650aybsmy8yvsd6r13692r1l8cp5cg") (y #t)))

(define-public crate-scc-1.1.0 (c (n "scc") (v "1.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1bpr0v3v985f60s9g8xy9p00b6lvg6qli8zs8hszksg00an0k3wm") (y #t)))

(define-public crate-scc-1.1.1 (c (n "scc") (v "1.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0mvynfyqppm8c6nmjgdpgz4sk32z7kgnwf6phb41zxvmcg3m163a") (y #t)))

(define-public crate-scc-1.1.2 (c (n "scc") (v "1.1.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "03c6a376wfyh1abjy1wbv33gf2qfb4izk1dj5sg70958lhrshzwf") (y #t)))

(define-public crate-scc-1.1.3 (c (n "scc") (v "1.1.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0dyyy720crh2ldkkkb0zklqc8849hm17csfcfmiq3xxfgc81w9gl") (y #t)))

(define-public crate-scc-1.1.4 (c (n "scc") (v "1.1.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1mvap9m4l3kl78wk156yrali772krcs5w4vvdqd7q7w88ahxwb0r") (y #t)))

(define-public crate-scc-1.1.5 (c (n "scc") (v "1.1.5") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "11g21d6vi2s4ilifg64cnzkv2zq6h6c2970pbvx1mqmj12rw16gi") (y #t)))

(define-public crate-scc-1.2.0 (c (n "scc") (v "1.2.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1g89nvypqczx4mw0vvqqfim93jaxh1mdnakx3hxwqwls5yc0ppyc") (y #t)))

(define-public crate-scc-1.3.0 (c (n "scc") (v "1.3.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0xandrfa3qk64cj0j8pdn948krm6kwzaihyl2jry6j46dk3qzcv9") (y #t)))

(define-public crate-scc-1.4.0 (c (n "scc") (v "1.4.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1sdlnfms6l4s8i49n32rjsbbm1q96wym0rpgm3fiwg4l7z7j1x3b") (y #t)))

(define-public crate-scc-1.4.1 (c (n "scc") (v "1.4.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "04fdzhbfsfv5cs69522vhjxqgyqzv9zkif399j2qzcyxs2jy70rs") (y #t)))

(define-public crate-scc-1.4.2 (c (n "scc") (v "1.4.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0fxjaq81qb9lg0wfkqgq185prc29hzisjqz2sbwv8ki6mvj2h3m3") (y #t)))

(define-public crate-scc-1.4.3 (c (n "scc") (v "1.4.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0ap20df60wd5y1v42119wvyyqf3ll2qbg2xx9dyrhbj80nknddlv") (y #t)))

(define-public crate-scc-1.4.4 (c (n "scc") (v "1.4.4") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1pp8g6g0v9sgdx19yrl2cm09sxy7174jz7jqki1x242phkgalajj") (y #t)))

(define-public crate-scc-1.5.0 (c (n "scc") (v "1.5.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0nxacisfznvdddzkb9di3i92cjsbqy2hh46p07s0id2753jn94vd") (y #t)))

(define-public crate-scc-1.6.0 (c (n "scc") (v "1.6.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "13rnk329fn8y2473kxi7nr8s7awpm3frg275z4iva6y7ahyc3hyi") (y #t)))

(define-public crate-scc-1.6.1 (c (n "scc") (v "1.6.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "1l0dsp10xx7y1shndy9x7qb4aj1nvz65m4qlj1lk8n9aqj53ff3d") (y #t)))

(define-public crate-scc-1.6.2 (c (n "scc") (v "1.6.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "18hz5sfx65lz72r6y7y3x696i4cs99gay7s7vv863qcw2g5i1svd") (y #t)))

(define-public crate-scc-1.6.3 (c (n "scc") (v "1.6.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0a51kh7xws7ysb1irrhv7p1xhi8fq8baw294x28gvqjmryly0hm2") (y #t)))

(define-public crate-scc-1.7.0 (c (n "scc") (v "1.7.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "195dg2jnvnsq531j4jk07h598gvpir1nn7brw5ms98xlp0n2cn88") (y #t)))

(define-public crate-scc-1.7.1 (c (n "scc") (v "1.7.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0gglmdpgiw531fhmf1gfa0a2w0ssf0k724x026fqrl0w9mr4fjrn") (y #t)))

(define-public crate-scc-1.7.2 (c (n "scc") (v "1.7.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "13v9wczv4ycl8b5a2divhsl7lv4fxr00pcfbidj8xk8gd1yz496f") (y #t)))

(define-public crate-scc-1.7.3 (c (n "scc") (v "1.7.3") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "18v369y5h3k4205pfkafj0dxa85x73izngdychzzx06rz95k212m") (y #t)))

(define-public crate-scc-1.8.0 (c (n "scc") (v "1.8.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "05yc7dp1cnp4kyh5ssqbg7wzr8mq864c9nxwbd7dhj9ijplgb6sx") (y #t)))

(define-public crate-scc-1.8.1 (c (n "scc") (v "1.8.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.2") (f (quote ("full"))) (d #t) (k 2)))) (h "0z39p4hbij0f2zmdr81y5czglapxn1ys4q9a18r89yjrv71vmd49") (y #t)))

(define-public crate-scc-1.8.2 (c (n "scc") (v "1.8.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.5") (d #t) (k 2)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.28") (f (quote ("full"))) (d #t) (k 2)))) (h "1wv8vr4pqqnbzn7zpn6q7ksdlg9yvs85b3d6qz82rl3as1fnzj0k") (y #t)))

(define-public crate-scc-1.8.3 (c (n "scc") (v "1.8.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.29") (f (quote ("full"))) (d #t) (k 2)))) (h "18v3l6sm3vs9mjh4xi7gmma6dliq78gdk8y64ddmg8vkyk3ggygd") (y #t)))

(define-public crate-scc-1.9.0 (c (n "scc") (v "1.9.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.29") (f (quote ("full"))) (d #t) (k 2)))) (h "067qc826i3dl088rbcjb0s11g3a3qaz4104g5mz07yh4w6gqm0s1") (y #t)))

(define-public crate-scc-1.9.1 (c (n "scc") (v "1.9.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.29") (f (quote ("full"))) (d #t) (k 2)))) (h "1xixf2xvzmvdx1yylfh2mhs7x4d81rq72cb9g81x6fjnsymzyvmy")))

(define-public crate-scc-2.0.0 (c (n "scc") (v "2.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.6") (d #t) (k 2)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.29") (f (quote ("full"))) (d #t) (k 2)))) (h "0k5flwmn5kalb782a9q73bjndjris9nrz4nxpri9fzfak4ljz0k7") (y #t)))

(define-public crate-scc-2.0.1 (c (n "scc") (v "2.0.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.32") (f (quote ("full"))) (d #t) (k 2)))) (h "0pv7i9hrddw5hidr5hvl6zwp1xhr9nfd6k34bp6zlvd1faz0lgis") (y #t)))

(define-public crate-scc-2.0.2 (c (n "scc") (v "2.0.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.32") (f (quote ("full"))) (d #t) (k 2)))) (h "1lpcx29nh5lrs6qywjzj9lbg9ikzr3xyx97hiqdrd3ar9xzzpka3") (y #t)))

(define-public crate-scc-2.0.3 (c (n "scc") (v "2.0.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.33") (f (quote ("full"))) (d #t) (k 2)))) (h "1hzvps3048lsrv4pqid538xs5mrq8p7l9522mspgy5myqjy8nqna") (y #t)))

(define-public crate-scc-2.0.4 (c (n "scc") (v "2.0.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.34") (f (quote ("full"))) (d #t) (k 2)))) (h "0prl5v8vjbvbarwrfa5kcgmy61pn2m2mh5vmlvawc5cfrkkx74bk") (y #t)))

(define-public crate-scc-2.0.5 (c (n "scc") (v "2.0.5") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.34") (f (quote ("full"))) (d #t) (k 2)))) (h "1xmkxx74cw491ikx13y9yqqg2a4blh1xgzp8yzjxyvl3kxczdr86") (y #t)))

(define-public crate-scc-2.0.6 (c (n "scc") (v "2.0.6") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "0mgpgk2gs71s2gicfpf5kq5abxim2r1iy94s5pscwyz6wr5z8936") (y #t)))

(define-public crate-scc-2.0.7 (c (n "scc") (v "2.0.7") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "132pl6qdaf24yg99vjlwwk6rs0wz9g949iz0pa6m5zr4r1r9mnk0") (y #t)))

(define-public crate-scc-2.0.8 (c (n "scc") (v "2.0.8") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "039756fajfrgf5lm5bn51519xazfg3r6alf221s0njglryb76vs2") (y #t)))

(define-public crate-scc-2.0.9 (c (n "scc") (v "2.0.9") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "0dyl67j13aybbafh15h31mk3434jpwd9fkr0lxrii8fc6whhc48a") (y #t) (r "1.65.0")))

(define-public crate-scc-2.0.10 (c (n "scc") (v "2.0.10") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "0zxmqdcd8srpwqvb5cv0im2h596rk18p4rndyn6zwrl57m7ym8w1") (y #t) (r "1.65.0")))

(define-public crate-scc-2.0.11 (c (n "scc") (v "2.0.11") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "1a37midhs8pxcpnc65ni2drx6imxsqhf15visvcfkcdkvz2ymbdj") (y #t) (r "1.65.0")))

(define-public crate-scc-2.0.12 (c (n "scc") (v "2.0.12") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "11b6z9zq23yp529hkk7lqdavhl2p5lqm574f03l937cvnk6sixbx") (r "1.65.0")))

(define-public crate-scc-2.0.13 (c (n "scc") (v "2.0.13") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "1iwq5dviqwyxk2w3xfpz4r87azw0qszv7nw93r0wdbp3bgqnd9cr") (r "1.65.0")))

(define-public crate-scc-2.0.14 (c (n "scc") (v "2.0.14") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "13awizradvlc5zfcz27bmgks6w19z6ggdyhj0n1j1gzikvh72d4h") (r "1.65.0")))

(define-public crate-scc-2.0.15 (c (n "scc") (v "2.0.15") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "0gzy67qmlbc1akzggpwzlkbxaywnvbgvmidl7c01p2ixngglng6s") (r "1.65.0")))

(define-public crate-scc-2.0.16 (c (n "scc") (v "2.0.16") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.35") (f (quote ("full"))) (d #t) (k 2)))) (h "0zh3hd0f302d88d58jfb544f6qsd2dh69b686acxvhlvy64s0sk7") (r "1.65.0")))

(define-public crate-scc-2.0.17 (c (n "scc") (v "2.0.17") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 2)))) (h "1yc98pq0chcah406dw7i1a9wy8xsr1z6z3hm7r2y08a8437c0a8x") (r "1.65.0")))

(define-public crate-scc-2.0.18 (c (n "scc") (v "2.0.18") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 2)))) (h "0l3fq41jkpwz5i4dryszrd7rpcmxji0dm0m842hy7g8b6wnwmzzn") (r "1.65.0")))

(define-public crate-scc-2.0.19 (c (n "scc") (v "2.0.19") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 2)))) (h "1gpwjd6v4gcb2mymln5cwayl68shbqzn4h35ic7az7zxs9h0vhg4") (r "1.65.0")))

(define-public crate-scc-2.0.20 (c (n "scc") (v "2.0.20") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 2)))) (h "1p25q6672w4a56fxcc4z6mzgig9ihfhz3kmk27a5n5c09xp19rmq") (r "1.65.0")))

(define-public crate-scc-2.1.0 (c (n "scc") (v "2.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sdd") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 2)))) (h "1ql3sx60j77cq3yb6p7wqjw1k6n0jcn6l7xvw329qyiix875d5pc") (r "1.65.0")))

(define-public crate-scc-2.1.1 (c (n "scc") (v "2.1.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^1.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sdd") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 2)))) (h "1p7gldp08856ny4pjl2c39ala07xnmxfvwm6nw3hl4751axjpbbn") (r "1.65.0")))

