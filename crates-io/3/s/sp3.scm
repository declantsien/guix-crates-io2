(define-module (crates-io #{3}# s sp3) #:use-module (crates-io))

(define-public crate-sp3-0.0.1 (c (n "sp3") (v "0.0.1") (d (list (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.12") (d #t) (k 0)))) (h "04df3z22w2xsk4vpnldah1m8z61i0k8pz62sryspx7i2zf20hfg7") (f (quote (("default"))))))

(define-public crate-sp3-0.0.2 (c (n "sp3") (v "0.0.2") (d (list (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fibj21sfwbir7gai583wd4zy5fvm8hjx0khski4043lgiqlp31f") (f (quote (("default"))))))

(define-public crate-sp3-0.0.3 (c (n "sp3") (v "0.0.3") (d (list (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1sa2xznhbqx0ya9shp8dc8k8cbhz083clwy1f3pcczw1shq1is6r") (f (quote (("default"))))))

(define-public crate-sp3-0.0.4 (c (n "sp3") (v "0.0.4") (d (list (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0l2khnll5ajf5xbbrr1rgdjjlg85f0rb9y0gf6b03nk2pgnrmk77") (f (quote (("default"))))))

(define-public crate-sp3-0.0.5 (c (n "sp3") (v "0.0.5") (d (list (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rinex") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0a6zbmf572sfa6c7hwgg06nyhj7clsrgaa97jwxsdv05qhpg86hn") (f (quote (("default"))))))

(define-public crate-sp3-0.0.6 (c (n "sp3") (v "0.0.6") (d (list (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rinex") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1cz1f531p108l6c7dpl0z3slmf91sxq9kj3av95yhlar8r8fqdxk") (f (quote (("default"))))))

(define-public crate-sp3-0.0.7 (c (n "sp3") (v "0.0.7") (d (list (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (o #t) (k 0)) (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0qm568rnf2dpm95j99hcqp39jfi8m2x4hx3p1szfh9jbph8wci6v") (f (quote (("default"))))))

(define-public crate-sp3-0.0.8 (c (n "sp3") (v "0.0.8") (d (list (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (o #t) (k 0)) (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "03mk5h2b9i0m9kn1a4mxm02m3r9wd8nis664y5g5s70ml3xc3kvs") (f (quote (("default"))))))

(define-public crate-sp3-1.0.0 (c (n "sp3") (v "1.0.0") (d (list (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (o #t) (k 0)) (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ycy8nasn1gnhzy2axg5dfjrc6cn7v0hwdfanhs2w2rxb6381p5i") (f (quote (("default"))))))

(define-public crate-sp3-1.0.1 (c (n "sp3") (v "1.0.1") (d (list (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (o #t) (k 0)) (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kpbmlj2n4v9kjcvhkkjqqdsyzj2dv56979s91bxhjcxblw6zqf2") (f (quote (("default"))))))

(define-public crate-sp3-1.0.2 (c (n "sp3") (v "1.0.2") (d (list (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (o #t) (k 0)) (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.13.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0sq6np4xb70vs3v5rrnsikdfpj77svz536jnbqs1hcv2d6ili5xr") (f (quote (("default"))))))

(define-public crate-sp3-1.0.3 (c (n "sp3") (v "1.0.3") (d (list (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (o #t) (k 0)) (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.13.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "05mfjj4jk5ns89h045fianm6nyh2c5nl45rlv0yvzswvkf1adam5") (f (quote (("default"))))))

(define-public crate-sp3-1.0.4 (c (n "sp3") (v "1.0.4") (d (list (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (o #t) (k 0)) (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "rinex") (r "^0.14.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1npplhmdmd5fq0avd894899x51cb3bqbkyddv31gpjzf6f0wxkyg") (f (quote (("default"))))))

(define-public crate-sp3-1.0.6 (c (n "sp3") (v "1.0.6") (d (list (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (o #t) (k 0)) (d (n "gnss-rs") (r "=2.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hifitime") (r "^3.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1s783p0clqajw18arns3ys2n0gd4vpfj9s6f507kywb1nvg240n8") (f (quote (("default"))))))

(define-public crate-sp3-1.0.7 (c (n "sp3") (v "1.0.7") (d (list (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (o #t) (k 0)) (d (n "gnss-rs") (r "^2.1.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hifitime") (r "^3.9.0") (d #t) (k 0)) (d (n "map_3d") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ahsl0j60rpzcm2z8qn76pdqzab2w3242vg22910r9k5q4wjmk8r") (f (quote (("default"))))))

(define-public crate-sp3-1.0.8 (c (n "sp3") (v "1.0.8") (d (list (d (n "flate2") (r "^1.0.24") (f (quote ("zlib"))) (o #t) (k 0)) (d (n "gnss-rs") (r "^2.1.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hifitime") (r "^3.9.0") (d #t) (k 0)) (d (n "map_3d") (r "^0.1.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0435g8abv8s0im5054nm0rgrh07dvlqj24m3qnvsz3gb2yqwjcvr") (f (quote (("default"))))))

