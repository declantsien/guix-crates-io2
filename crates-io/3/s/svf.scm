(define-module (crates-io #{3}# s svf) #:use-module (crates-io))

(define-public crate-svf-0.1.0 (c (n "svf") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nw6v6q1gdbvqsmdi7sphcd4y3mdxcsb2ypvljvsfy9k9ilbmpsq")))

(define-public crate-svf-0.2.0 (c (n "svf") (v "0.2.0") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ibn12ff6vc76zamdx688zii3jphbff5f2q0qg2qlkixjhrcym8c")))

(define-public crate-svf-0.2.1 (c (n "svf") (v "0.2.1") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6.1") (d #t) (k 0)) (d (n "nom_locate") (r "^3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1r26saql2lfpbln7anwscv6plyz0zppbrd49rjrz3hmj2f1kh4y7")))

(define-public crate-svf-0.3.0 (c (n "svf") (v "0.3.0") (d (list (d (n "bytecount") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kgdzh1nsb7gnr0j4j039vb0k2dl2g9ii8kwqy4lw1c0cjhrhc9w")))

