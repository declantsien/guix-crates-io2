(define-module (crates-io #{3}# s spn) #:use-module (crates-io))

(define-public crate-spn-0.1.0 (c (n "spn") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)))) (h "0z0brmb1qsxn5q8dwjh0ry1znkgqgh5gbsrnls9kqngbvpis8wl0")))

(define-public crate-spn-0.1.1 (c (n "spn") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)))) (h "0iz80bx5scyvmj3lywjjh3sqniv345dq7dhca3c60l691rp6n3yw")))

(define-public crate-spn-0.2.0 (c (n "spn") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "rusty-hook") (r "^0.11.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7") (d #t) (k 2)) (d (n "tokio") (r "^1.21.2") (f (quote ("net"))) (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "macros" "time"))) (d #t) (k 2)))) (h "14bp8n1qfck0c9mr8wq84qn3bnndzl8kr8r3l96kaqkkryv5ks33")))

