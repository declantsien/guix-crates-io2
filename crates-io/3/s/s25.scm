(define-module (crates-io #{3}# s s25) #:use-module (crates-io))

(define-public crate-s25-0.1.0 (c (n "s25") (v "0.1.0") (h "1qzndq5hs6flb39vya2p3942m5bx8sii3nxlddhl65zvxz2560wg")))

(define-public crate-s25-0.1.1 (c (n "s25") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "1pjv6xq5rip8rhcwrk6vndwy74js7c5pbfhv9a389j84si13kiq9") (f (quote (("fail" "failure") ("default"))))))

(define-public crate-s25-0.2.0 (c (n "s25") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.8") (o #t) (d #t) (k 0)))) (h "1g70l6x1xfy5w07var3icwq6ph79qizqsdwa3775n5vl81bxpqfl") (f (quote (("fail" "failure") ("default"))))))

(define-public crate-s25-0.2.1 (c (n "s25") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.20") (o #t) (d #t) (k 0)))) (h "0bi73l97cvyjwh7iip3vig11ghp294iwsch62pr80rixmi5gk4pg") (f (quote (("fail" "thiserror") ("default"))))))

