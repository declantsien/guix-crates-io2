(define-module (crates-io #{3}# s slp) #:use-module (crates-io))

(define-public crate-slp-0.1.0 (c (n "slp") (v "0.1.0") (h "1wbjx513bh2rddrrraz1hwnzhxqgk0rpbcqlpmwpssdvcj3wrxi2")))

(define-public crate-slp-0.1.1 (c (n "slp") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "037kpafz02dy6s34v4dansvl3s5073gk1fn81pmg5j36c2xmbm8p")))

(define-public crate-slp-0.1.2 (c (n "slp") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0kjh1kg9allv706rbh22g5533aa4npb1w2v08355n3c9qmymrs8d")))

(define-public crate-slp-0.1.3 (c (n "slp") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0rcf9pj7m1qawfadqfzzw16d4lr5pcl7kn13ckq04pyblikp5hb2")))

(define-public crate-slp-0.1.4 (c (n "slp") (v "0.1.4") (d (list (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1l1yap4lzd9k80ks5s030437mymmzpbs6r5pnjxikp2sw8wkx4x5")))

(define-public crate-slp-0.1.5 (c (n "slp") (v "0.1.5") (d (list (d (n "num-rational") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1qwxfy627yx5rarjsw0cb5pr07mcnklxslrw7h7m0mv259kwb9kd")))

(define-public crate-slp-0.1.6 (c (n "slp") (v "0.1.6") (d (list (d (n "num-rational") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1v0pyz80w5g36hniqbjhpspi1wil16y8wm5a67h268bmhvmw1cyf")))

(define-public crate-slp-0.1.7 (c (n "slp") (v "0.1.7") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "026msd8a8cikcpz247308p48jck7ngwkch6c4vi0b2l9pd54y1h5")))

(define-public crate-slp-0.1.8 (c (n "slp") (v "0.1.8") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "07aq9x40r12glv41dscxi6fav1k452q0ppjvhn5mwrzcwim0l37w")))

(define-public crate-slp-0.1.9 (c (n "slp") (v "0.1.9") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1qd1dpjzvhg1j38aza1vf2nsb58d62pglq2ybw6l7zl85w2l81yr")))

(define-public crate-slp-0.1.10 (c (n "slp") (v "0.1.10") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0s6l8102wsyf1bbygw5bbdx4jsfviqw6dq7p3y6mxb9xngi1p27x")))

(define-public crate-slp-0.1.11 (c (n "slp") (v "0.1.11") (d (list (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1") (d #t) (k 0)) (d (n "rayon") (r "^1.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0arhb4x8p6cyqj63m06isf931c9v8xqakxz1ffx1x2qliscy74jz")))

