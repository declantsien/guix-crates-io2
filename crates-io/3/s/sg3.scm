(define-module (crates-io #{3}# s sg3) #:use-module (crates-io))

(define-public crate-sg3-0.1.0 (c (n "sg3") (v "0.1.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "nix") (r "^0") (d #t) (k 0)))) (h "0jwxyq5v9ygn193fy1c77q5dva5rvg2daa0q1mw5j1ph7pl83h3d")))

(define-public crate-sg3-0.1.1 (c (n "sg3") (v "0.1.1") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)))) (h "0agdphkq0ks6mb18abiyvw1kv9xzw22xmca4d9hvx0y8ix9854cn")))

(define-public crate-sg3-0.1.2 (c (n "sg3") (v "0.1.2") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)))) (h "1lvp1pmf2vhf3jbvdlpdm5rsfzb3z6f64x84qnvr7ncacc8984iq")))

(define-public crate-sg3-0.1.3 (c (n "sg3") (v "0.1.3") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^3") (d #t) (k 0)))) (h "022ryim7jsjcd7wdr05d6zzv70s7fpamy3d1b6wg1w4csbwvq9mi")))

(define-public crate-sg3-0.1.4 (c (n "sg3") (v "0.1.4") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "0pgmhlxgpz9jdjj5p9ksl0wp1mwji80jvzkzh847zdgp85y76d8w")))

(define-public crate-sg3-0.1.5 (c (n "sg3") (v "0.1.5") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^3.2") (d #t) (k 0)))) (h "1zn12g7ksd4gmhrjyq9qfy6937r1zi2cdg5syizljcvqfs7gfnm5")))

