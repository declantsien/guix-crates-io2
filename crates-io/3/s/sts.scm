(define-module (crates-io #{3}# s sts) #:use-module (crates-io))

(define-public crate-sts-0.0.4 (c (n "sts") (v "0.0.4") (d (list (d (n "coarsetime") (r "^0.1.23") (d #t) (k 0)))) (h "1q73xqp6svr1y110jdj9ga0r6dnri72f69fq5pnx0c7xk0jzdnsj")))

(define-public crate-sts-0.1.2 (c (n "sts") (v "0.1.2") (d (list (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)))) (h "0a9xp7klrhq1n4l5aawmxc3lbwvrjywc5pqdj1k0r8m967kzwlbx")))

(define-public crate-sts-0.1.4 (c (n "sts") (v "0.1.4") (d (list (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)))) (h "1khfczaicka3ylzv9xh1nq98kzbqaxjr289f3m44sm2i2vz8zlzn")))

(define-public crate-sts-0.1.6 (c (n "sts") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "16c17bnlvxvp3sjbrh91cnl6g2vm0ymqi5x79piw34945ha28b5l")))

(define-public crate-sts-0.1.7 (c (n "sts") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0ch8idj58c0z2d5ackf8996dwhvjqlvz6mcq4c9czwkazpa2qvid")))

(define-public crate-sts-0.1.8 (c (n "sts") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1hbiscn1zc964hs2ndj62rahc0r7q95z85pagcycyq18hrybkd8d")))

(define-public crate-sts-0.1.9 (c (n "sts") (v "0.1.9") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0wphggkb9la601xlhghlm2l1pv0pc7xya3i76l987lmwacpmc9my")))

(define-public crate-sts-0.1.10 (c (n "sts") (v "0.1.10") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1ypj9i2japq67wlc036s9gqk6j7jj3iw7jqgcwix5fq8hy25y014")))

(define-public crate-sts-0.1.11 (c (n "sts") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0zzb8w0s7hf0gyfjv9wivqgz6nzcizii8rbpvxk1grbgp0yfygsw")))

(define-public crate-sts-0.1.12 (c (n "sts") (v "0.1.12") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0n6dwhlna5a1j9qj9n5azny6l6bvhz48s8akrgcnacz83jyyvs7w")))

(define-public crate-sts-0.1.13 (c (n "sts") (v "0.1.13") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0n85kjd66601zp2f7lv0g776y27d49fzr1xljh9mc43sc6s201fh")))

(define-public crate-sts-0.1.14 (c (n "sts") (v "0.1.14") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0a08sdajs8346iyvdnx8f8l9p4xyl9h4xq5h5d37nw3h5bqymv0c")))

(define-public crate-sts-0.1.15 (c (n "sts") (v "0.1.15") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1z5wf1blncnjsc14gw3cmwij7p843i9qha6p17c105fx322m9ibk")))

(define-public crate-sts-0.1.16 (c (n "sts") (v "0.1.16") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0cjvsx70i9qn4r9ad5bzp2rg9i8axhrr0m1zjm9cm3gl38cmd6k2")))

(define-public crate-sts-0.1.17 (c (n "sts") (v "0.1.17") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1g2l3lrqdn0nchv7lm8qgy9c5lvpkqzkiw4q8x56ka74d7bclwff")))

(define-public crate-sts-0.1.18 (c (n "sts") (v "0.1.18") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "09iz3fyqw83s4xgayv5bhn97wcf448lp4zv5r5gqfq4pfj80zgp6")))

(define-public crate-sts-0.1.19 (c (n "sts") (v "0.1.19") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.33") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "1cg0qlj1c60cjzwxnqdsb5nh8skq8ppnfv7vm4qa969x4sn4l9f8")))

(define-public crate-sts-0.1.20 (c (n "sts") (v "0.1.20") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.34") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1y3sym7pvnccg0v4v769ljmrxylwsajpfbvm5mz089yw9am3gy0l")))

(define-public crate-sts-0.1.21 (c (n "sts") (v "0.1.21") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "coarsetime") (r "^0.1.34") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "00cnsqzbff3c300bs1f4kd6v37qchvqky3x9l296v486azgrrc60")))

