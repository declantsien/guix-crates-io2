(define-module (crates-io #{3}# s soa) #:use-module (crates-io))

(define-public crate-soa-0.0.1 (c (n "soa") (v "0.0.1") (h "1qw7l96i2ybgql88dcl7lxx36p7l1gk6364krf8pzn01wlkgab1x")))

(define-public crate-soa-0.0.2 (c (n "soa") (v "0.0.2") (h "0adlpbpqihgjswplydmyfkllwzsfkkvqs2h76x63q815bq9b535p")))

(define-public crate-soa-0.0.3 (c (n "soa") (v "0.0.3") (h "0p4ajc2s14863j70s5fv783rmlzca02m68h45ywgznyx82k3c0a7")))

(define-public crate-soa-0.0.4 (c (n "soa") (v "0.0.4") (h "0m1zlizxayydbfhsizhgyl3zjd2knn5li2w1dxrl5pf24mlvzcmq")))

(define-public crate-soa-0.0.5 (c (n "soa") (v "0.0.5") (h "1r4bv83qwk44j4pxa0mx22q7y3hn3sy7r1j81dnzfz4bfxvj55jz")))

(define-public crate-soa-0.0.6 (c (n "soa") (v "0.0.6") (h "0z2a8kbvnpc017smbycvcixl54z4071gbdnz8dkrw37hpkbr4mqk")))

(define-public crate-soa-0.0.7 (c (n "soa") (v "0.0.7") (h "12riri230yl30v0ymawdcvgglgvr828i56a4vj4xlqz0pxzc4kra")))

(define-public crate-soa-0.0.8 (c (n "soa") (v "0.0.8") (h "0haw2jq96sxhapkarivm8kan38z3h2r80izlr92pp7zd9g4w2qmz")))

(define-public crate-soa-0.0.9 (c (n "soa") (v "0.0.9") (h "1mlmgvajsyqkk1gc76k8d59xhgx5hf69w21m72b0sb2l0xbka1fc")))

(define-public crate-soa-0.9.1 (c (n "soa") (v "0.9.1") (h "130p8n4a67q9sjy2zi9n2m416xlfz65zyqcwwdvhc89m577dczbw")))

(define-public crate-soa-0.9.2 (c (n "soa") (v "0.9.2") (h "03snrbk4nyswrfx1jnmhh6fa3p9vjkh5b43vm4piflv2q28m1ywv")))

