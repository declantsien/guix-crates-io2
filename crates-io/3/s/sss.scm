(define-module (crates-io #{3}# s sss) #:use-module (crates-io))

(define-public crate-sss-0.0.0 (c (n "sss") (v "0.0.0") (d (list (d (n "chrono") (r "*") (d #t) (k 0)) (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "openssl") (r "*") (d #t) (k 0)) (d (n "rust-crypto") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0zj5yardk3ii5qpczxz3rk818894adrz53q0n3yxlrp4a32z8i3h")))

