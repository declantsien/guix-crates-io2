(define-module (crates-io #{3}# s sio) #:use-module (crates-io))

(define-public crate-sio-0.1.0 (c (n "sio") (v "0.1.0") (h "14zahvrpi9j6l71r0gzyilz5wawq2g560zgjagbjjwzs2s7ibv7y")))

(define-public crate-sio-0.2.0 (c (n "sio") (v "0.2.0") (d (list (d (n "ring") (r "^0.14.6") (o #t) (d #t) (k 0)))) (h "05j2cxrr05i4d589791lpyiqy7slnpxsy6lhrl2107hc68wmw0h2") (f (quote (("default" "ring"))))))

