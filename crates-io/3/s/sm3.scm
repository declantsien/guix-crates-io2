(define-module (crates-io #{3}# s sm3) #:use-module (crates-io))

(define-public crate-sm3-0.1.0 (c (n "sm3") (v "0.1.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "0jklfygffvqcmgmlhwjcdq7wgnxsz1nn27j2lzzhl0av1zf09byw") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sm3-0.2.0 (c (n "sm3") (v "0.2.0") (d (list (d (n "block-buffer") (r "^0.7") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "15hg004kmn2zca7vi1k34cg1xgkj68n31kzi41z7k1pd1di6pmzv") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sm3-0.3.0 (c (n "sm3") (v "0.3.0") (d (list (d (n "block-buffer") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "14c7b48hldg4nabn864c4y47aa148jf6zygm31qd77z081n23mll") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sm3-0.4.0 (c (n "sm3") (v "0.4.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "17gdcc1idns0cp22nwalq8i35lmsxhmd4w38zs1p758vl6ay3srh") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sm3-0.4.1 (c (n "sm3") (v "0.4.1") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "1nhmc84875k4hc4kcrp91yzrd4x59ygiw7928v82p7q8wg2sfhzr") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sm3-0.4.2 (c (n "sm3") (v "0.4.2") (d (list (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "0q2qn4d7qhd8v5grr0xdq9jv3likcr2i8nnqqhxy79yh0avs7fgb") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-sm3-0.5.0-pre.0 (c (n "sm3") (v "0.5.0-pre.0") (d (list (d (n "digest") (r "=0.11.0-pre.3") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "14lwnv2xj2h6341c02l21r60ra486g9a6r4k972dwqgxicadq2ag") (f (quote (("std" "digest/std") ("default" "std")))) (r "1.71")))

(define-public crate-sm3-0.5.0-pre.1 (c (n "sm3") (v "0.5.0-pre.1") (d (list (d (n "digest") (r "=0.11.0-pre.4") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "1pbjwxmm7gdcxfy1g6772w0h3s6di9wir8snjpi44fl7fwyyzdkj") (f (quote (("std" "digest/std") ("default" "std")))) (r "1.71")))

(define-public crate-sm3-0.5.0-pre.2 (c (n "sm3") (v "0.5.0-pre.2") (d (list (d (n "digest") (r "=0.11.0-pre.7") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "0nsl3xdmj2lrmcldkzipv3xjhixfp663sd180hlix8k48n712893") (f (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("default" "std")))) (r "1.71")))

(define-public crate-sm3-0.5.0-pre.3 (c (n "sm3") (v "0.5.0-pre.3") (d (list (d (n "digest") (r "=0.11.0-pre.8") (d #t) (k 0)) (d (n "digest") (r "=0.11.0-pre.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)))) (h "0g84zvcwkgp1dg6cmvdkwd6h6n3w9gyvs9452kmbx4vai4dqxwd6") (f (quote (("zeroize" "digest/zeroize") ("std" "digest/std") ("default" "std")))) (r "1.71")))

