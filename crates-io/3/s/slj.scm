(define-module (crates-io #{3}# s slj) #:use-module (crates-io))

(define-public crate-slj-0.6.6 (c (n "slj") (v "0.6.6") (d (list (d (n "no-panic") (r "^0.1.22") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.2") (d #t) (k 0)) (d (n "unsafe_unwrap") (r "^0.1.0") (d #t) (k 0)))) (h "00mn0yp2sfvlffixni8xbkikqpsm2klv97jwvhb9njf7lg3l2417")))

