(define-module (crates-io #{3}# s sdl) #:use-module (crates-io))

(define-public crate-sdl-0.3.5 (c (n "sdl") (v "0.3.5") (h "184sb9hlsbc55v6djwv0j4iacnqm5a2hn3bn18ifa09lrilwxqwg")))

(define-public crate-sdl-0.3.6 (c (n "sdl") (v "0.3.6") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.1.24") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "06sxb8sqrjks7pm1563kw0wqlkm8n08qi0bnar1gz7bsi9c05fwk")))

