(define-module (crates-io #{3}# s scr) #:use-module (crates-io))

(define-public crate-scr-0.1.0 (c (n "scr") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "13a5sxxcbh3a02maz0c0vhdfn7w8xmicqy9hh4ab11b8hj71dfpy")))

(define-public crate-scr-0.1.1 (c (n "scr") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "0h50p2dpbc89yc5r8vbvylycnzy247vbvb7qrg0yj98li909wk4i")))

(define-public crate-scr-0.2.0 (c (n "scr") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "0677kznvngqjwvfi2iacflifgdfkkbxr76g2cnq5nzlppd9q2mid")))

(define-public crate-scr-0.2.1 (c (n "scr") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "1bbbl2cy95h4hnyhnv07zswyr7gl9gws2nhwiivcjpbsrlx536ns")))

(define-public crate-scr-0.2.2 (c (n "scr") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "15ypczn5vmnv3jw4142gf5rdy9rdk03qhzbnjld5j4q9ziyspsvx")))

(define-public crate-scr-0.2.3 (c (n "scr") (v "0.2.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "175d0g7fbl3pwcyxfq464f685afrq3w4xz7irr05bw1vxbpsfly4")))

(define-public crate-scr-1.0.0 (c (n "scr") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "10gmln6yny8xmcpx2spqzz1bhnjnkdx40lf8zc4agmm2iw5mksd4")))

(define-public crate-scr-1.0.1 (c (n "scr") (v "1.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "0pgnkcwr70x9ic6bwy4j8shi7cj5xpg23xfk7smxjh10y5vzakmx")))

(define-public crate-scr-1.0.2 (c (n "scr") (v "1.0.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.17.1") (d #t) (k 0)))) (h "0fr2m4dl67ai68wshfh4a11c2c5w785xnvai40ghhqzh2x7gsc4d")))

