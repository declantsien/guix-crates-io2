(define-module (crates-io #{3}# s sub) #:use-module (crates-io))

(define-public crate-sub-0.1.0 (c (n "sub") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.11") (d #t) (k 2)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "predicates") (r "^1.0") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0wy25s96gb6hfiisqprzri4rix706cjh8029lv5al6x24cskkqqa")))

