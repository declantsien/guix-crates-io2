(define-module (crates-io #{3}# s smo) #:use-module (crates-io))

(define-public crate-smo-0.1.0 (c (n "smo") (v "0.1.0") (d (list (d (n "calamine") (r "^0.22.1") (f (quote ("dates" "picture"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "evcxr") (r "^0.15.1") (d #t) (k 0)) (d (n "plotly") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0k9qkj88kpfxxf3gdxk79kfjhkywbh1h8lig7ivkc7jpaa4k4w25")))

(define-public crate-smo-0.1.1 (c (n "smo") (v "0.1.1") (d (list (d (n "calamine") (r "^0.22.1") (f (quote ("dates" "picture"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "evcxr") (r "^0.15.1") (d #t) (k 0)) (d (n "plotly") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "0izb72xxn4n4c0ndp8bnfr10rgik8l08f6afhv4wn5ybn4xxlnwg")))

(define-public crate-smo-0.1.2 (c (n "smo") (v "0.1.2") (d (list (d (n "calamine") (r "^0.22.1") (f (quote ("dates" "picture"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "evcxr") (r "^0.15.1") (d #t) (k 0)) (d (n "plotly") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "16bfk8ijzpsz33693367b96iwy7z26qvl6dzr02ndmpkdnzvs7vb")))

(define-public crate-smo-0.1.3 (c (n "smo") (v "0.1.3") (d (list (d (n "calamine") (r "^0.22.1") (f (quote ("dates" "picture"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "evcxr") (r "^0.15.1") (d #t) (k 0)) (d (n "plotly") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)))) (h "1zc87ph9ia6wnzsz9llpwbcddcfqa1lwv0a49mdalb4cz3nwz1xy")))

