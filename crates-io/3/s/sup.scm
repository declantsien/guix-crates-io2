(define-module (crates-io #{3}# s sup) #:use-module (crates-io))

(define-public crate-sup-0.0.1 (c (n "sup") (v "0.0.1") (d (list (d (n "git2") (r "^0.13.6") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "00lk4lvcarmrymaf92xb7vr5vw31695203vkxdfqhb3lppcgbisx")))

(define-public crate-sup-0.0.2 (c (n "sup") (v "0.0.2") (d (list (d (n "etc") (r "^0.1.7") (f (quote ("serde-tree"))) (d #t) (k 0)) (d (n "etc") (r "^0.1.7") (f (quote ("serde-tree"))) (d #t) (k 1)) (d (n "gita") (r "^0.0.2") (d #t) (k 1)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 1)))) (h "0zxm6hb3i3lfm865nxhh085i6gmasnlk6s4c7z4ybkk3waf6bdwq")))

(define-public crate-sup-0.0.3 (c (n "sup") (v "0.0.3") (d (list (d (n "etc") (r "^0.1.7") (f (quote ("serde-tree"))) (d #t) (k 0)) (d (n "etc") (r "^0.1.7") (f (quote ("serde-tree"))) (d #t) (k 1)) (d (n "gita") (r "^0.0.2") (d #t) (k 1)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 1)))) (h "1c1y4xv0wgsi0j43kck3yg2y7vpzqq2khrb2n7p3dmp33iihiy3d")))

(define-public crate-sup-0.0.4 (c (n "sup") (v "0.0.4") (d (list (d (n "etc") (r "^0.1.7") (f (quote ("serde-tree"))) (d #t) (k 0)) (d (n "etc") (r "^0.1.7") (f (quote ("serde-tree"))) (d #t) (k 1)) (d (n "gita") (r "^0.0.2") (d #t) (k 1)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 1)))) (h "0qvfqd2l1k33l0bgqmgcyq7aiwlwj19h9zc3myhm0lpqawi633v6")))

(define-public crate-sup-0.0.5 (c (n "sup") (v "0.0.5") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0wh4hvfmf4nbpm1mg0ncj0ss30mgka8pixh7wi817l874d9vkqyd")))

(define-public crate-sup-0.0.6 (c (n "sup") (v "0.0.6") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1kg9q96kw6sng7ys361h4dn99kc7yzh4915ib47yz5f2xas2a8ay")))

(define-public crate-sup-0.0.7 (c (n "sup") (v "0.0.7") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0p3d8c8g9nl0v1fmbd30pmwmqbl11kipq6lm3ygyp1pd4mcshaw9")))

(define-public crate-sup-0.0.9 (c (n "sup") (v "0.0.9") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.10") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0i96rc4pkirfy0mp4vnnkxxzy5yikfgcwmyyj48mchqax58ha608")))

(define-public crate-sup-0.1.0 (c (n "sup") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0hbv08cjjnky1958l2xggzfhah8kr9b8zjjcd3y6bm0frll9394x")))

(define-public crate-sup-0.1.1 (c (n "sup") (v "0.1.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1n6csndw3h9qvs0xvp3w02bv66bpwa2xcag9agiqv4kz88wk8dsx")))

(define-public crate-sup-0.1.2 (c (n "sup") (v "0.1.2") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1jfh2zbdn5l9lfl3w9ymif57k5487xxpryb8fz5kayxm5a4irl6c")))

(define-public crate-sup-0.1.3 (c (n "sup") (v "0.1.3") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0lqn6k4g22kfa8ya45h4n5s23kz9cmvwznxazxxpz5igv65pwgnl")))

(define-public crate-sup-0.1.4 (c (n "sup") (v "0.1.4") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0abpgqxdirkqahcbc6j1gw1mgyziz2lwzfjyw12ad80dmbgnf79p")))

(define-public crate-sup-0.1.5 (c (n "sup") (v "0.1.5") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1rzyd18f5l7vn2r9qln84zh455kyh72jm8dmmh23im5q0y4lwwzd")))

(define-public crate-sup-0.1.6 (c (n "sup") (v "0.1.6") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0qi0x6rdsnqq81zf8qipnd0ajn9yk1mh6khrl07pr3vqvbvcmbrz")))

(define-public crate-sup-0.1.7 (c (n "sup") (v "0.1.7") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0qqk6xa8dhvn5qxmhbzll30pg44nddh59721lszqn8w4xycfls63")))

(define-public crate-sup-0.1.8 (c (n "sup") (v "0.1.8") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0783law4qwwxradws4khsp9c6scvn5mqr2bj90r34q858wr2bxb4")))

(define-public crate-sup-0.1.9 (c (n "sup") (v "0.1.9") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0gqz5gzqjqzy5ssc21yd3w22icrii61c1mpz6mxxvr8b3ls9rv9w")))

(define-public crate-sup-0.2.0 (c (n "sup") (v "0.2.0") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "07w94ywl2msk3vk46pjr6ndfy2jap13x9iqgpklqciakhkp4dz0m")))

(define-public crate-sup-0.2.1 (c (n "sup") (v "0.2.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1kzl43x7q9al3jgj6b4zih6sj97p082ip745wx7zds8ipjcllk73")))

(define-public crate-sup-0.2.2 (c (n "sup") (v "0.2.2") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "06fwpi3l6b9b8lrfmn4p4n8cmvnjpandizk6r3rw345v8ns1b9sm")))

(define-public crate-sup-0.2.3 (c (n "sup") (v "0.2.3") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1fay5gxgd0glkkxibrq4lppc96i61zv9wra7bikjmc5qb28m6sn5")))

(define-public crate-sup-0.2.4 (c (n "sup") (v "0.2.4") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1njqi10zm9y762j9p68n7dyy48j8vrn3xsrwpw9y3c2ij2fzirmf")))

(define-public crate-sup-0.2.5 (c (n "sup") (v "0.2.5") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0k214jd5alag2lhcl31kd59svzb9lhpndjkq6xgjkwk5lz6yfp21")))

(define-public crate-sup-0.2.6 (c (n "sup") (v "0.2.6") (d (list (d (n "dirs") (r ">=3.0.1, <4.0.0") (d #t) (k 0)) (d (n "etc") (r ">=0.1.11, <0.2.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r ">=0.3.14, <0.4.0") (d #t) (k 0)) (d (n "toml") (r ">=0.5.6, <0.6.0") (d #t) (k 0)))) (h "1i3701a4h189ax9cim6r9gk02yw9k72i1v85jsa5xsnhh7i210mr")))

(define-public crate-sup-0.2.7 (c (n "sup") (v "0.2.7") (d (list (d (n "dirs") (r ">=3.0.1, <4.0.0") (d #t) (k 0)) (d (n "etc") (r ">=0.1.11, <0.2.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r ">=0.3.14, <0.4.0") (d #t) (k 0)) (d (n "toml") (r ">=0.5.6, <0.6.0") (d #t) (k 0)))) (h "0xr3ffh4l67yn1w2ryd83kwi49p266cwn03a2xlwpf2kvry6l1qg")))

(define-public crate-sup-0.2.8 (c (n "sup") (v "0.2.8") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.11") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0qmd3qrxxixfs8zlray8qq9mb01l1qrz0g3h0q6lvmgxavf2zagk")))

(define-public crate-sup-0.2.9 (c (n "sup") (v "0.2.9") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0fql310ch2rlisa75r0apd8aag9y8mrp96ljbwdcgjkkzx05h1wi")))

(define-public crate-sup-0.2.10 (c (n "sup") (v "0.2.10") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0pb80md55270vwmj5v12lc9k7g4fwamwwkqhaianhgnfmabsvxhz")))

(define-public crate-sup-0.2.11 (c (n "sup") (v "0.2.11") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0i23l3mw8vzfqwbbrrj8vb51ndgzxc5nfa01wjlkp44c0gz2k9g6")))

(define-public crate-sup-0.2.12 (c (n "sup") (v "0.2.12") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "0li5qi8k6kn6m15y7kg1d3lg8jgai42h4ln96nkbpwhplvzjzg89")))

(define-public crate-sup-0.2.13 (c (n "sup") (v "0.2.13") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "etc") (r "^0.1.16") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (d #t) (k 0)))) (h "1lvn9slrzbf6a6gkdcjfkqv9hyynccsifv1gi1xif704hj2y2qjs")))

