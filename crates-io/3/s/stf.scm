(define-module (crates-io #{3}# s stf) #:use-module (crates-io))

(define-public crate-stf-0.1.0 (c (n "stf") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xnhh1rhg1v6bydjsjyd3cxxbd42l0iy1v2b541izmrxwn8nahjj")))

(define-public crate-stf-0.1.1 (c (n "stf") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wf2gwzh62ky9s6y6c6cmr4azrr42xhdvc71zyd7grkijcgr5mfg")))

(define-public crate-stf-0.1.2 (c (n "stf") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "0lkh6g58l7xy4cxfq2kzmcfpmdvxis7whw7ihd72aypzvxwbvvic")))

