(define-module (crates-io #{3}# s sws) #:use-module (crates-io))

(define-public crate-sws-0.0.0 (c (n "sws") (v "0.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.22") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1x006vkb0z697wa3nrkdz9h7p8yjylib1g8mcqihp8h47lb4sgvr")))

