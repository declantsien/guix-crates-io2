(define-module (crates-io #{3}# s ssr) #:use-module (crates-io))

(define-public crate-ssr-0.0.0 (c (n "ssr") (v "0.0.0") (h "1r2lrj1xl0f0x1d6gibcl5ppji42c88bwzc1pfs7a2gi9s9xrlj2")))

(define-public crate-ssr-0.0.1 (c (n "ssr") (v "0.0.1") (d (list (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("process" "net" "sync" "time" "io-util"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1axgsmsnvwq447bx5rizjlyn6r08582maj5fqmhc10jqgdi4bw5l")))

(define-public crate-ssr-0.0.2 (c (n "ssr") (v "0.0.2") (d (list (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("process" "net" "sync" "time" "io-util"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "13j2hhi7vgbdpl9jphb67m2x4j7dbd5xark9hm3iyr2qar6nzah8")))

(define-public crate-ssr-0.0.3 (c (n "ssr") (v "0.0.3") (d (list (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("process" "net" "sync" "time" "io-util"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0f5jrycrvb4h76gd29qian8j3hn8pj87kqq468w1mcym5v3bwzq2")))

(define-public crate-ssr-0.0.4 (c (n "ssr") (v "0.0.4") (d (list (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("process" "net" "sync" "time" "io-util"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1c8pcrfz7ylyvv09sc05aih2hqhazqp06k6ajls5rkspmlixyd8n")))

(define-public crate-ssr-0.0.5 (c (n "ssr") (v "0.0.5") (d (list (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("process" "net" "sync" "time" "io-util"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1kn11hdv7p7zn1n6kvpzxqzvm2f931bi5vhsn5rab5ksm81rscji")))

(define-public crate-ssr-0.0.6 (c (n "ssr") (v "0.0.6") (d (list (d (n "http") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("process" "net" "sync" "time" "io-util"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "03y56hn9svh6bg7g8603155b52jy238205jpj1y8jhrg3r03zxz1")))

