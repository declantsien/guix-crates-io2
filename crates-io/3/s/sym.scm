(define-module (crates-io #{3}# s sym) #:use-module (crates-io))

(define-public crate-sym-0.0.0 (c (n "sym") (v "0.0.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "0bcri6r6jfim9rhp1x86hviidanay2np1yydg5d41z9gv1vwvbxh") (y #t)))

