(define-module (crates-io #{3}# s sdc) #:use-module (crates-io))

(define-public crate-sdc-0.1.0 (c (n "sdc") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1a0mbmxvjiikv23jb0mir0fa86zigvq9xw83dffhmfjb7kzmwagf")))

(define-public crate-sdc-0.1.1 (c (n "sdc") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0z0crydg3bd842ia7vwiic3nwizn65rc5843a4x4bbpnr2sgpg4b")))

