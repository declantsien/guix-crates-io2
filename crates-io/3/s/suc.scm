(define-module (crates-io #{3}# s suc) #:use-module (crates-io))

(define-public crate-suc-0.1.0 (c (n "suc") (v "0.1.0") (d (list (d (n "argon2") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0py8z0phxl9rya57bhdgws664550x01g0makr2lfml620ixhp82l")))

