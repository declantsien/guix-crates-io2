(define-module (crates-io #{3}# s str) #:use-module (crates-io))

(define-public crate-str-0.1.0 (c (n "str") (v "0.1.0") (h "030k71ahr9rn8q10qh9kc9a2q3v2dk40xkhcc4y6r23708y8vkby")))

(define-public crate-str-0.1.1 (c (n "str") (v "0.1.1") (h "1j52jphpgwp6whpj8v1pixmvpm1c0yq2jc39iybd081hq2y0q0kl")))

(define-public crate-str-0.1.2 (c (n "str") (v "0.1.2") (h "0np4ibb3p7c9d302y9sana99a32pq33xni08b671dx2ac9bzina0")))

(define-public crate-str-0.1.3 (c (n "str") (v "0.1.3") (h "1wly5wxxljbljpa9chfa3jv2ynnxyvhlrm255pmydxm9l8fdwa32")))

(define-public crate-str-0.1.4 (c (n "str") (v "0.1.4") (h "0gcxp0p1kjq7hc5ydchrf6wmmj4bvc1rc4y6jvl2ppb9zbfk144h")))

