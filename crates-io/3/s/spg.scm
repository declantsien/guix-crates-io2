(define-module (crates-io #{3}# s spg) #:use-module (crates-io))

(define-public crate-spg-0.1.0 (c (n "spg") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "ctrlc") (r "^3.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)))) (h "0hfbc5sd6m298pfvyg2w7247sklxafr9fd2c0kd719h4i5m2plva")))

