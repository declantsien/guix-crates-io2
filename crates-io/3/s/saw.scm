(define-module (crates-io #{3}# s saw) #:use-module (crates-io))

(define-public crate-saw-1.0.0 (c (n "saw") (v "1.0.0") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "saw_mcr") (r "^0.1.0") (d #t) (k 0)))) (h "0his8bvs7yd0qgzy6v7yyvdhf4ssyfma5nrsbxqw8m6q7xydn7kz") (y #t)))

(define-public crate-saw-2.0.0 (c (n "saw") (v "2.0.0") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "saw_mcr") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)))) (h "0q4swylbvjc6x2s1vajnc85qhyb9vr0nsrpm6hy73wnd98nsg4vv") (y #t)))

(define-public crate-saw-2.1.0 (c (n "saw") (v "2.1.0") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "saw_mcr") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)))) (h "0d7vs4ypb0wm4w553nryylmh3mj2gnzazbjpvi3ffj4i812n5ckz") (y #t)))

(define-public crate-saw-0.1.0 (c (n "saw") (v "0.1.0") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "saw_mcr") (r "^0.2.0") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)))) (h "17yq771rwpj1wyfq2qxd17m74fg9q6h4p4mad7fq7igs1s2ngqv4") (y #t)))

(define-public crate-saw-3.0.0 (c (n "saw") (v "3.0.0") (d (list (d (n "actix-web") (r "^3") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres" "r2d2"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "saw_mcr") (r "^0.2.0") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)))) (h "01wl9p4xnqbq0q9az7r80wkybgmf717lifyhwiigqdmdpxsjph6r")))

