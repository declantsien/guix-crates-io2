(define-module (crates-io #{3}# s sdr) #:use-module (crates-io))

(define-public crate-sdr-0.1.0 (c (n "sdr") (v "0.1.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0pgjxnkhijbwmbl8lar8pr5w9vcmdcr0cqh00ymxd920zyg010y4")))

(define-public crate-sdr-0.2.0 (c (n "sdr") (v "0.2.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 2)))) (h "0r9yz48awlwsh0fg06szh100g2k3c95q3ykdfwirl7s5zik22y3z")))

(define-public crate-sdr-0.3.0 (c (n "sdr") (v "0.3.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 2)))) (h "0vlj9wr5nbgqz2g17bnjvshyr2varyfpk41h7c43541xjmm41q1k")))

(define-public crate-sdr-0.4.0 (c (n "sdr") (v "0.4.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 2)))) (h "0102w38d1h96yy4ays9gnyzqj94ixspd0qyfxkbx8fkw3i9kk70h")))

(define-public crate-sdr-0.5.0 (c (n "sdr") (v "0.5.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 2)))) (h "1kh00r0wz5avpp80rrp4k0g6rhp1xgma33adz0j0z5hvcgwvikw8")))

(define-public crate-sdr-0.5.1 (c (n "sdr") (v "0.5.1") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 2)))) (h "1vbvrpyl49ifq3y9y937zhql6p6cng5gk17hh7c5rm1vq9nsgvhh")))

(define-public crate-sdr-0.5.2 (c (n "sdr") (v "0.5.2") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 2)))) (h "1frzj1mla53v88a1cf73xa01v4wp4yjbsymsf2hvaifwrz27gwyg")))

(define-public crate-sdr-0.6.0 (c (n "sdr") (v "0.6.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 2)))) (h "0vd01zamc1rvy17lx69mwvqv2z4lkgv0cm6vrmn7xx7py1l796bb")))

(define-public crate-sdr-0.7.0 (c (n "sdr") (v "0.7.0") (d (list (d (n "num") (r "^0.1.42") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 2)))) (h "00r27g7flnn1k7yfmavxi2ahxgdvdpid88i6ng74r4wsgbvg7grj")))

