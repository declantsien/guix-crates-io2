(define-module (crates-io #{3}# s su3) #:use-module (crates-io))

(define-public crate-su3-0.0.1 (c (n "su3") (v "0.0.1") (d (list (d (n "deku") (r "^0.13.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "029dccbr97zljvx1lcqd6kscvm8915868wlqmi2klvp6dz7z03i1") (y #t)))

(define-public crate-su3-0.0.2 (c (n "su3") (v "0.0.2") (d (list (d (n "deku") (r "^0.13.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1r39ipbwvzrpvcf6zl3c1y8iig55ifxnj4k431ljmj0v1k8kxfaw")))

(define-public crate-su3-0.1.0 (c (n "su3") (v "0.1.0") (d (list (d (n "deku") (r "^0.14.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (d #t) (k 0)))) (h "1h61frv7gik2zj8y4jfq7midbmfa6rmxb85pdhgjh553pnl54wpn")))

(define-public crate-su3-0.2.0 (c (n "su3") (v "0.2.0") (d (list (d (n "deku") (r "^0.15.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)))) (h "04hm1bjhqkaja0p3fsp2cdcvz33kzh7d9i92ya73gxf48mznv638")))

