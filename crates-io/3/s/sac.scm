(define-module (crates-io #{3}# s sac) #:use-module (crates-io))

(define-public crate-sac-0.2.1 (c (n "sac") (v "0.2.1") (h "018wiybsnpx5mb14xmv9pvjkw5qx4lvflhbhlv1d19w84h169rmf")))

(define-public crate-sac-0.2.2 (c (n "sac") (v "0.2.2") (h "1j7i5fvqrk2bij02risayml7ryqhy3d63d0kzrs1iizmma00krzw")))

