(define-module (crates-io #{3}# s slb) #:use-module (crates-io))

(define-public crate-slb-0.1.0 (c (n "slb") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0d9livyx2wpxkvjvnpqlzad911qwyy38nhhhzh7znqjyp46ry9af")))

(define-public crate-slb-0.1.1 (c (n "slb") (v "0.1.1") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1salhdfqr1jar31vfmrdlz024bfgjh78fg70k54z7jg4vy8ddv72")))

(define-public crate-slb-0.2.0 (c (n "slb") (v "0.2.0") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1w8k7ikci2gp2093db7fi7bvj8p8gsbh28101ycc3qcqd1d9gv2k")))

(define-public crate-slb-0.2.1 (c (n "slb") (v "0.2.1") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0nral0gxglma3dinw36h2nmamd222zncd9yb1skhr66qvikgcjqj")))

(define-public crate-slb-0.2.2 (c (n "slb") (v "0.2.2") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0m0aqx32bn93s9gybhz3rjnyblq6520alqpzjy5lxl1hq5g9vam9")))

(define-public crate-slb-0.2.3 (c (n "slb") (v "0.2.3") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0n8fpx6b80psyrbfqfiz68y7w3y5skvpzg5xxv4wkn2sa9jqdn0n")))

(define-public crate-slb-0.2.4 (c (n "slb") (v "0.2.4") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1xr7qnlbqhcjcfdhbqq6zkf70izhrnldjwlyrzp7km51l3xpc8xr")))

(define-public crate-slb-0.3.0 (c (n "slb") (v "0.3.0") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "121q4ln39jlflxwckab3sf5wqjgdnhcl8ncjnx8g24mg4hhcgawf")))

(define-public crate-slb-0.3.1 (c (n "slb") (v "0.3.1") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0wvwkkskg7f76ps7qxga9s67rhqy4fdcc5qpy4rqbhhzmw93dj45")))

