(define-module (crates-io #{3}# s ssa) #:use-module (crates-io))

(define-public crate-ssa-0.1.1 (c (n "ssa") (v "0.1.1") (h "1754hkz39a89yzwfk8his2m55dbqp96idyszjlmzda6b0vvkyrxs")))

(define-public crate-ssa-0.1.2 (c (n "ssa") (v "0.1.2") (d (list (d (n "json") (r "^0.11.13") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)))) (h "00ki11hhlszyylm6a0i6z6znlijh951adphbgzs9aqbs4bkmrhs4")))

