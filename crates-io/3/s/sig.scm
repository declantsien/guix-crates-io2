(define-module (crates-io #{3}# s sig) #:use-module (crates-io))

(define-public crate-sig-0.1.0 (c (n "sig") (v "0.1.0") (h "04blka0pqby0scwyqkm5h7k6md477w8202kcl74589s6gg23p7fl")))

(define-public crate-sig-0.1.1 (c (n "sig") (v "0.1.1") (h "1xbd0gdf7fb681y11kvpv80vkwvcbcxxrl2nxllqvrm1q51rwr66")))

(define-public crate-sig-1.0.0 (c (n "sig") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18hl4shgj880mf648grailslhqr5ljwk4jnrlpkavgzrg2ay4rv5")))

