(define-module (crates-io #{3}# s sll) #:use-module (crates-io))

(define-public crate-sll-0.1.0 (c (n "sll") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "020igh4wyv92py3j5g0lr3p2r8kyidbnv07mw9bpla8h5dxms8kd") (y #t)))

(define-public crate-sll-0.1.1 (c (n "sll") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "06fsq698x7znwgr6cxnmxmpigxmbn37zqzq01npry2464dr9bzj6") (y #t)))

(define-public crate-sll-0.1.2 (c (n "sll") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0z06zrabrsa5xiz64f5ykq343h4cyk709fmnc59mgmy7h91nqyg1") (y #t)))

(define-public crate-sll-0.1.3 (c (n "sll") (v "0.1.3") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1f558mp9msrnvy6zi28na2yb028li3fczad47vs45xks3d0lz911")))

