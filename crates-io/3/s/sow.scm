(define-module (crates-io #{3}# s sow) #:use-module (crates-io))

(define-public crate-sow-0.0.0 (c (n "sow") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dashmap") (r "^5.2.0") (f (quote ("rayon" "send_guard"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.10") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "tera") (r "^1.15.0") (d #t) (k 0)))) (h "0hj97hij4xv0kpazpgkrwfbd87n4y2j5wbny3if4z4fnlisaq2pr")))

