(define-module (crates-io #{3}# s sdd) #:use-module (crates-io))

(define-public crate-sdd-0.0.1 (c (n "sdd") (v "0.0.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 2)))) (h "18hw3j51ykbwnh16hqbsd4b3zi1hf5lp1h4dip70cl0k73hjbw2f") (r "1.65.0")))

(define-public crate-sdd-0.0.2 (c (n "sdd") (v "0.0.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 2)))) (h "13sncai791a9jch0agwi8a51d2zg04gim9jzzj685iq3a43wxz1k") (r "1.65.0")))

(define-public crate-sdd-0.1.0 (c (n "sdd") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 2)))) (h "1fwx549f985f1688vs20ckq18vqzlc98b6bddkwiqy7r0nz8byli") (r "1.65.0")))

(define-public crate-sdd-0.2.0 (c (n "sdd") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "loom") (r "^0.7") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.37") (f (quote ("full"))) (d #t) (k 2)))) (h "179h2b27wmv6jbfs3yldwhmn3dwrmb581yw2l1s34w5xr7j4ahxq") (r "1.65.0")))

