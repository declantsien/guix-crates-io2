(define-module (crates-io #{3}# s sus) #:use-module (crates-io))

(define-public crate-sus-0.1.0 (c (n "sus") (v "0.1.0") (h "0agxvn64q6q82n5an8ij7vvxhk9zf5b70bnsjxxrhglky3fgzslb")))

(define-public crate-sus-0.1.1 (c (n "sus") (v "0.1.1") (h "1bd6hqppgnmq6dzv80fb2fijs1hl8bi1izgs552d164ncli9smx7")))

