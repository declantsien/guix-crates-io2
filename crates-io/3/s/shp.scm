(define-module (crates-io #{3}# s shp) #:use-module (crates-io))

(define-public crate-shp-0.1.0 (c (n "shp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0wqcp37skdi4h9h56yc7d62nfglsnw2jv42wrmix9qyhw375jhcj")))

(define-public crate-shp-0.1.1 (c (n "shp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0bqlh3rz184j21j7i56w5w5vha5y2zf75j74z25vxf1s1k85qrif")))

