(define-module (crates-io #{3}# s shi) #:use-module (crates-io))

(define-public crate-shi-0.1.0 (c (n "shi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "18bqvgis1b50lznidkb8lqa2hjw6ammc3i2lj10iix8z5pbn016v")))

(define-public crate-shi-0.1.1 (c (n "shi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0qpxgm796h9n2vpk5y3x3fisdg6h355q4v889m4qnf0zi5sy6l6y")))

(define-public crate-shi-0.1.2 (c (n "shi") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1ig5jm2p5wb7gpc4l56pfqsqfcgryl8swb4fyk99lr7kxngwx3vy")))

(define-public crate-shi-0.1.3 (c (n "shi") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "006r1iiwi2zzh7babza50mm17rb9iqsh3zkiqzkaifa16v0gandl")))

(define-public crate-shi-0.1.4 (c (n "shi") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "05q6z02hmppajwyf4sj92y9w4hlq8x3vypgz13xq6x0ng5kwd724")))

(define-public crate-shi-0.1.5 (c (n "shi") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.36") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "rustyline") (r "^7.1.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "052cw4gags5n9b1hiv1igm8givq77mlvxxblwzx9r4l44kvlvxsr")))

