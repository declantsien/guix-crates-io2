(define-module (crates-io #{3}# s sp2) #:use-module (crates-io))

(define-public crate-sp2-0.1.0 (c (n "sp2") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ga2") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vector-space") (r "^0.3.0") (d #t) (k 0)))) (h "1l8sz9gmxg4ns39xc600hlvv7k3gjbkcszmibr93s4hby1np87w3")))

(define-public crate-sp2-0.2.0 (c (n "sp2") (v "0.2.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "ga2") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "vector-space") (r "^0.3.0") (d #t) (k 0)))) (h "1fdfc4hm5ah3g6w7dmwfv22xwsshxycd14w3zj82b76m083idg7q")))

