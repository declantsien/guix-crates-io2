(define-module (crates-io #{3}# s sha) #:use-module (crates-io))

(define-public crate-sha-0.2.2 (c (n "sha") (v "0.2.2") (d (list (d (n "bswap") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0j1daz24fv2na94ycpaanyp0ccghwpbhcxqvvv2yfzw534bsigr5")))

(define-public crate-sha-0.3.2 (c (n "sha") (v "0.3.2") (d (list (d (n "bswap") (r "*") (d #t) (k 0)))) (h "1flc01645bqbv3bh8pacyyrf0qffrv9b2hskdx4rk2b292qi8rg3")))

(define-public crate-sha-1.0.3 (c (n "sha") (v "1.0.3") (d (list (d (n "bswap") (r "^1.0.0") (d #t) (k 0)))) (h "0znbh365wmpkmc0b2lxh8h9xma0jpk2zdzbsg4xrysi70flxa222")))

