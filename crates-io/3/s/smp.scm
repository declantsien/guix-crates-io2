(define-module (crates-io #{3}# s smp) #:use-module (crates-io))

(define-public crate-smp-0.0.1 (c (n "smp") (v "0.0.1") (h "1131gdjmpx5sf992xivhzc2jibw9296l77vaa2xxinz3f2zi4g31") (y #t)))

(define-public crate-smp-0.0.3 (c (n "smp") (v "0.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "12mh08d2vs8qxa42g8md7y34vi2721ny017qfwqvr2x14jmfk4r4")))

