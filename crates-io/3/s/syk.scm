(define-module (crates-io #{3}# s syk) #:use-module (crates-io))

(define-public crate-syk-0.2.0 (c (n "syk") (v "0.2.0") (d (list (d (n "aes-gcm") (r "^0.10.3") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)))) (h "12zaxbxyx8hi6y7qr99iq4ngxdbs71ffdhm8ax0gly65b240p1nr")))

