(define-module (crates-io #{3}# s soi) #:use-module (crates-io))

(define-public crate-soi-0.1.0 (c (n "soi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "glib") (r "^0.14.5") (f (quote ("v2_66"))) (d #t) (k 0)) (d (n "gst") (r "^0.17.4") (d #t) (k 0) (p "gstreamer")) (d (n "gst-audio") (r "^0.17.2") (d #t) (k 0) (p "gstreamer-audio")) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0az5f76j9pmaqblbwfv47286r4dix0imrx5n30hyx8n9xfzwdaqv")))

(define-public crate-soi-0.1.1 (c (n "soi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "glib") (r "^0.14.5") (f (quote ("v2_66"))) (d #t) (k 0)) (d (n "gst") (r "^0.17.4") (d #t) (k 0) (p "gstreamer")) (d (n "gst-audio") (r "^0.17.2") (d #t) (k 0) (p "gstreamer-audio")) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "termion") (r "^3.0.0") (d #t) (k 0)))) (h "0xcvms4jrs6j45y8z9h7dx8shqgw5dnazy26x7xwp0pdg2badyzp")))

