(define-module (crates-io #{3}# s squ) #:use-module (crates-io))

(define-public crate-squ-0.1.0 (c (n "squ") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crowbook-text-processing") (r "^1.0.0") (d #t) (k 0)))) (h "0x07z1w6w6bmxv4mg7adpfgksv49yi01sywx2r9jn3isjhsllxla")))

