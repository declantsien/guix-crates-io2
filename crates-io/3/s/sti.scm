(define-module (crates-io #{3}# s sti) #:use-module (crates-io))

(define-public crate-sti-0.1.1 (c (n "sti") (v "0.1.1") (h "1rd2a6misvahf5zkw1pxc5n11y30kc3y168ysa7m3zbx34q905kg")))

(define-public crate-sti-0.1.2 (c (n "sti") (v "0.1.2") (h "0zi3im1k67544s4m0c3bdnr69nfa8kcypd92kxgkzk0w7kb11wzh")))

(define-public crate-sti-0.1.3 (c (n "sti") (v "0.1.3") (h "0h4a95swg6fmir7nipc8lkzhr2ybsqcz9hpk7p727n38gy084dvd")))

(define-public crate-sti-0.1.4 (c (n "sti") (v "0.1.4") (h "0351w2jv3dwr7v9cs8mi922hg9sxan3y036pzvlwdr4as19ip1f1")))

(define-public crate-sti-0.1.6 (c (n "sti") (v "0.1.6") (h "02dl9ygc97pa5mhy97xwn1d9qbr4mzb5h936bm4311k5zdfflsxj")))

(define-public crate-sti-0.1.8 (c (n "sti") (v "0.1.8") (h "1112rai1ijsq9jacissnlmfn4jgy75vyih8qmii72njgfxvsqfhh")))

(define-public crate-sti-0.5.0 (c (n "sti") (v "0.5.0") (h "0ck12cn60viv1d30f1qllrwjhqvch0w0zaj71cxipg1qj0qdkmx6") (f (quote (("std") ("default" "std"))))))

