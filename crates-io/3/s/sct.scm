(define-module (crates-io #{3}# s sct) #:use-module (crates-io))

(define-public crate-sct-0.1.0 (c (n "sct") (v "0.1.0") (d (list (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "1mqj0nskqsh5k63wns11k978vd7kvygzs9n002a484a5d1acsagw")))

(define-public crate-sct-0.1.1 (c (n "sct") (v "0.1.1") (d (list (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "0ji7fnmgggvl1n6x1cq95lnaadci4xdc1ryczscqs8q3gkic11g3")))

(define-public crate-sct-0.1.2 (c (n "sct") (v "0.1.2") (d (list (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "1kqsjn9c01qhq8czm0jmqlkrg3rl6j2pvjim6cgy57k7vgx05v8k")))

(define-public crate-sct-0.1.3 (c (n "sct") (v "0.1.3") (d (list (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "06fp1s4df780s31g378mx6ki7cb5wwcckbad1x7ni64r102a5ryd")))

(define-public crate-sct-0.1.4 (c (n "sct") (v "0.1.4") (d (list (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "untrusted") (r "^0.5") (d #t) (k 0)))) (h "1wg5nqnb2a83ai74cag80qx5garwx0ppg7rjna1rjyvaxxnjnaf9")))

(define-public crate-sct-0.2.0 (c (n "sct") (v "0.2.0") (d (list (d (n "ring") (r "^0.12") (d #t) (k 0)) (d (n "untrusted") (r "^0.5.1") (d #t) (k 0)))) (h "0ch6h2g7v32zfdfi02s0v5ai5m1f8abxsfwraq3d7i71pdkvfdqi")))

(define-public crate-sct-0.3.0 (c (n "sct") (v "0.3.0") (d (list (d (n "ring") (r "^0.13.0-alpha") (d #t) (k 0)) (d (n "untrusted") (r "^0.6.1") (d #t) (k 0)))) (h "0z090j3lvy0lqbhmpswm4vb2n4i8dqswy0l93abdx9biipnhlm5l")))

(define-public crate-sct-0.4.0 (c (n "sct") (v "0.4.0") (d (list (d (n "ring") (r "^0.13.2") (d #t) (k 0)) (d (n "untrusted") (r "^0.6.2") (d #t) (k 0)))) (h "0nkl03nqfczz0784sg3bf2j08qq350yh9063f4m0dpgawvwn33yb")))

(define-public crate-sct-0.5.0 (c (n "sct") (v "0.5.0") (d (list (d (n "ring") (r "^0.14") (d #t) (k 0)) (d (n "untrusted") (r "^0.6.2") (d #t) (k 0)))) (h "1fb9ym5bwswx01yyggn7v2vfryih4vnqpp4r4ssv3qaqpn7xynig")))

(define-public crate-sct-0.6.0 (c (n "sct") (v "0.6.0") (d (list (d (n "ring") (r "^0.16.0") (d #t) (k 0)) (d (n "untrusted") (r "^0.7.0") (d #t) (k 0)))) (h "0g4dz7las43kcpi9vqv9c6l1afjkdv3g3w3s7d2w7a7w77wjl173")))

(define-public crate-sct-0.6.1 (c (n "sct") (v "0.6.1") (d (list (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "untrusted") (r "^0.7.0") (d #t) (k 0)))) (h "1ki8qa7yf4d9i4ynsfvwwkpnnqw0m8ayx0jva4w9zrp0k0wbhqmk")))

(define-public crate-sct-0.7.0 (c (n "sct") (v "0.7.0") (d (list (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "untrusted") (r "^0.7.0") (d #t) (k 0)))) (h "193w3dg2pcn7138ab4c586pl76nkryn4h6wqlwvqj5gqr6vwsgfm")))

(define-public crate-sct-0.7.1 (c (n "sct") (v "0.7.1") (d (list (d (n "ring") (r "^0.17.0") (d #t) (k 0)) (d (n "untrusted") (r "^0.9.0") (d #t) (k 0)))) (h "056lmi2xkzdg1dbai6ha3n57s18cbip4pnmpdhyljli3m99n216s") (r "1.61")))

