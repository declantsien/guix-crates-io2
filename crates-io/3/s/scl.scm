(define-module (crates-io #{3}# s scl) #:use-module (crates-io))

(define-public crate-scl-0.0.1 (c (n "scl") (v "0.0.1") (d (list (d (n "pest") (r "^1.0.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0zc08w7amdgiyplg4pvjhrqkwp93wj0dqcvmx7vvf9l6vk7vakyg")))

