(define-module (crates-io #{3}# s spy) #:use-module (crates-io))

(define-public crate-spy-0.1.0 (c (n "spy") (v "0.1.0") (h "0z3cma6sh50kjpfc5iql754nr9jd8q5ybvrgzrhm746x2la2hgaf")))

(define-public crate-spy-0.1.1 (c (n "spy") (v "0.1.1") (h "1kxfqdsmpyin3hi9qza3b5dkg2khy2k3pb5q3rvgyj15bfl46xsc")))

(define-public crate-spy-0.2.0 (c (n "spy") (v "0.2.0") (h "16gq17n2nb2dglbq31bqi543b5nq3vflcckymxvlwq11w2pgqpw5")))

