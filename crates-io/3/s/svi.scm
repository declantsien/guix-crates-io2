(define-module (crates-io #{3}# s svi) #:use-module (crates-io))

(define-public crate-svi-0.1.0 (c (n "svi") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "148dhqyhlcnmsindr60ifa9xd9bk7d2rc346fsfvy6hgl9w0blr7") (y #t)))

(define-public crate-svi-0.1.1 (c (n "svi") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lxhk73sgjm4c3ijwmnfnd9216v7p75nia51g3prbw5bzarl7jq8") (y #t)))

(define-public crate-svi-0.1.2 (c (n "svi") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "099f922cnnnhkqkpm37b4qhl56avr4cgp8syf4m9p10z7xw9bg9r")))

(define-public crate-svi-0.1.3 (c (n "svi") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h71mp8chz0ddbqvkq3pj17jdz406rm7y0xsnkri04wnrzkfa7pc")))

(define-public crate-svi-0.1.4 (c (n "svi") (v "0.1.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vwjpklmya4b85l26a1rfdr0cidlrb2y0k89a8504z77lyw5p9kq")))

(define-public crate-svi-1.0.0 (c (n "svi") (v "1.0.0") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "0fswkbbi0hbjjc4cskf1ypa54sba746kildz7qg35gd7nqkvjdk1")))

(define-public crate-svi-1.0.1 (c (n "svi") (v "1.0.1") (d (list (d (n "thiserror") (r "^1.0.61") (d #t) (k 0)))) (h "05zh7x0r0fca0r7ibc1ac84xjk81xaqjq4hsbnv8vqcjc9g65z7c")))

