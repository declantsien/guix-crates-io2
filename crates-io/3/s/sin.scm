(define-module (crates-io #{3}# s sin) #:use-module (crates-io))

(define-public crate-sin-0.0.0 (c (n "sin") (v "0.0.0") (h "09gs7dh4533rzy1nrmg8bn6k3rlvcw2ccfl2hs89ys0g1r7py3sa") (y #t)))

(define-public crate-sin-0.0.1 (c (n "sin") (v "0.0.1") (h "1a9jhd4gy0brqznnwifqfxx8nwx76lzh543h08a53v15cpm4ggm2")))

(define-public crate-sin-0.0.2 (c (n "sin") (v "0.0.2") (h "0n61pz8m1fdy4jd0845xvxvsi7y3gsgqwjpc53w6r5bk7wpcsfsd")))

