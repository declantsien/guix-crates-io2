(define-module (crates-io #{3}# s shd) #:use-module (crates-io))

(define-public crate-shd-0.1.4 (c (n "shd") (v "0.1.4") (d (list (d (n "byte-unit") (r "^4.0.12") (d #t) (k 0)) (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "083llr57birgj4yhnlq48jfbqfj0k98kvz3a57kn8aqisq0azasp")))

(define-public crate-shd-0.1.5 (c (n "shd") (v "0.1.5") (d (list (d (n "byte-unit") (r "^4.0.12") (d #t) (k 0)) (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "18zncy8y6lgrgfcgj35lrgfix2yi1yf2nb5zi41j2h4ga2gk8xp2")))

