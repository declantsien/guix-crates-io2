(define-module (crates-io #{3}# s sdb) #:use-module (crates-io))

(define-public crate-sdb-0.0.1 (c (n "sdb") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "sanakirja") (r "^1.2.4") (d #t) (k 0)))) (h "095ql6hg1lrjzbwfwq721m0wp207n3c4m5mblcsiry83y8z4lyn5")))

(define-public crate-sdb-0.0.2 (c (n "sdb") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "sanakirja") (r "^1.2.4") (d #t) (k 0)))) (h "07132b1q60px4hwllzbyv47jwah5yfs6nyijrima65ic91i24ri3")))

(define-public crate-sdb-0.0.3 (c (n "sdb") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 2)) (d (n "sanakirja") (r "^1.2.4") (d #t) (k 0)))) (h "18kggk0raxnabyan61jzvq3077v762ibqfy0rckw5wvxb6kqjjh3")))

(define-public crate-sdb-0.0.4 (c (n "sdb") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "desse") (r "^0.2.1") (d #t) (k 2)) (d (n "sanakirja") (r "^1.2.5") (d #t) (k 0)))) (h "114mj9brjv17xrii9qvhzg8p64km4s64qlajyg6f3gjyv1yfsw50")))

(define-public crate-sdb-0.0.5 (c (n "sdb") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "sanakirja") (r "^1.2.5") (d #t) (k 0)))) (h "033zjp2grhpbfkl6g1dm7hyiwxk23mmqskdxjq8r3jzazzws8h5v")))

(define-public crate-sdb-0.0.6 (c (n "sdb") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "desse") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "sanakirja") (r "^1.2.5") (d #t) (k 0)))) (h "0d1fz2gv9jmnm5i6wxxzg3sgzszcg0yqxm3cw6pqg3b670z6lfm0") (f (quote (("default" "desse"))))))

(define-public crate-sdb-0.0.7 (c (n "sdb") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "desse") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "sanakirja") (r "^1.2.5") (d #t) (k 0)) (d (n "sdb_macro") (r "^0.0.1") (d #t) (k 0)))) (h "1457fv5ryriqls9pp38kcwy5fglbfw39z9d2482f1glskdcz1558") (f (quote (("default" "desse"))))))

(define-public crate-sdb-0.0.8 (c (n "sdb") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "desse") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "sanakirja") (r "^1.2.5") (d #t) (k 0)) (d (n "sdb_macro") (r "^0.0.1") (d #t) (k 0)))) (h "0d6r7q4rhp1bh24d0yqn1f2vk3rr4kbkg9pfpi0r13ghr9cacjxc") (f (quote (("default" "desse"))))))

(define-public crate-sdb-0.0.9 (c (n "sdb") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "desse") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "sanakirja") (r "^1.2.5") (d #t) (k 0)) (d (n "sdb_macro") (r "^0.0.1") (d #t) (k 0)))) (h "1nbpmm6x4aa6ldzp9r3x7zhvflij2a3nndwhgpkl3j0vg8icdkrs") (f (quote (("default" "desse"))))))

(define-public crate-sdb-0.0.10 (c (n "sdb") (v "0.0.10") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 2)) (d (n "desse") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "sanakirja") (r "^1.2.5") (d #t) (k 0)) (d (n "sdb_macro") (r "^0.0.1") (d #t) (k 0)))) (h "0q6qmyk23xr161rw3i4mbs3lypjl7j89kg9m6nkj6247zd1g69fz") (f (quote (("default" "desse"))))))

