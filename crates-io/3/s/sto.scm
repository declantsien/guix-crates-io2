(define-module (crates-io #{3}# s sto) #:use-module (crates-io))

(define-public crate-sto-0.1.0 (c (n "sto") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8") (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "once_cell") (r "^1.17") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "04r3ar26vyza2fx6n1zkn88i7bmvj36bllr3jyp8w9zv17s0r368") (f (quote (("global" "once_cell") ("default" "global"))))))

(define-public crate-sto-0.1.1 (c (n "sto") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8") (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.17") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "1bz6fh5fadbmdnkxncc8jwxh2ld840k1w31dw7ahiyhyq2s030ga") (f (quote (("global" "once_cell") ("default" "global"))))))

