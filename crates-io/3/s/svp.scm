(define-module (crates-io #{3}# s svp) #:use-module (crates-io))

(define-public crate-svp-0.1.0 (c (n "svp") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rug") (r "^1.17.0") (d #t) (k 0)))) (h "0dhgw4s4xgbgnb5p4jrsjhd584cj3p48qhchhl4hywgnsg0pwaa4")))

(define-public crate-svp-0.2.0 (c (n "svp") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rug") (r "^1.17.0") (d #t) (k 0)))) (h "1nh6zr3gabdp3x84l4g4w80hzjik3y8myr3kh2132dcjhppccsg0")))

