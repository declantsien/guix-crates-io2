(define-module (crates-io #{3}# s sty) #:use-module (crates-io))

(define-public crate-sty-0.1.0 (c (n "sty") (v "0.1.0") (h "01z1l7xj6zm88bmp9r5zair3979w0f0jfc13qkklsz02iaf24p8d")))

(define-public crate-sty-0.2.0 (c (n "sty") (v "0.2.0") (h "1jwy6ngbqdgp5f0j6nmpgbjpzigczhc17id74imk724zl90qldw6")))

(define-public crate-sty-0.3.0 (c (n "sty") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 0)))) (h "1wa5xx1wkqw4k2f7al2n5fib18xccia72if7cgj3gsxv64d6gwrv")))

(define-public crate-sty-0.3.1 (c (n "sty") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "once_cell") (r "^1.19") (d #t) (k 0)) (d (n "rustversion") (r "^1") (d #t) (k 0)))) (h "1xq740xg959h1vh15h0mr2hkmy5fkxz935kcv18isg1yv2nb8kn9")))

