(define-module (crates-io #{3}# s skc) #:use-module (crates-io))

(define-public crate-skc-0.1.0 (c (n "skc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "niffler") (r "^2.5.0") (d #t) (k 0)) (d (n "noodles") (r "^0.40.0") (f (quote ("fasta"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0yq0dy9xqrq0nnbms13j9qfvwpsrf7z9z5ll6awkdhh1n1p3sn50")))

