(define-module (crates-io #{3}# s sap) #:use-module (crates-io))

(define-public crate-sap-0.1.0-alpha.1 (c (n "sap") (v "0.1.0-alpha.1") (d (list (d (n "libc") (r "^0.2.126") (k 2)) (d (n "windows") (r "^0.39.0") (f (quote ("Win32_System_LibraryLoader" "Win32_System_Threading" "Win32_Foundation" "Win32_System_Kernel"))) (d #t) (t "cfg(windows)") (k 0)))) (h "108i10ny86kvdjjn7w02nalafp7f4n6wjr1h7jyrwvsnkdlvhnf3") (f (quote (("std") ("default" "std"))))))

