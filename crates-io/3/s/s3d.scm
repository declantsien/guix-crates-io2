(define-module (crates-io #{3}# s s3d) #:use-module (crates-io))

(define-public crate-s3d-0.0.1-alpha (c (n "s3d") (v "0.0.1-alpha") (d (list (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1i3319gcgcj65196sysmi6j2x7l3hyfjfjz42r88f408lds0w1js") (y #t)))

(define-public crate-s3d-0.0.1-alpha2 (c (n "s3d") (v "0.0.1-alpha2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1nd4r16zqpsvb2qz9whvs5598ski4kylxz8fmlzcdy6mqh3ccifc") (y #t)))

(define-public crate-s3d-0.0.1-alpha3 (c (n "s3d") (v "0.0.1-alpha3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zcfbwhwl2g5y7crsfbcx1q2x9cz8d0cxclw9pn3sjsbl5r7wpfv") (y #t)))

(define-public crate-s3d-0.0.1-alpha4 (c (n "s3d") (v "0.0.1-alpha4") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.4") (d #t) (k 0)) (d (n "fuser") (r "^0.9.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14.13") (f (quote ("full"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("v4"))) (d #t) (k 0)))) (h "11y0l0d6jhmijljml9dqcdq0y1ijabm78n0jp6nvdj2v3nb7l3il")))

