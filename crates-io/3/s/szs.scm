(define-module (crates-io #{3}# s szs) #:use-module (crates-io))

(define-public crate-szs-0.0.0 (c (n "szs") (v "0.0.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0k392xdv858gavzvg37jqk6mv33qbfiiz7y4jvddfkz1qawpk4hf")))

(define-public crate-szs-0.0.3 (c (n "szs") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0nd9z86l3wppwgzilwh86wv7w6r85hbr3bjfmj9ncfli9sfdfyyj")))

(define-public crate-szs-0.0.4 (c (n "szs") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "05irm32ylryx055jgz98nd947vqw9gfs9h7i7bgvbmjbcjnva8vp")))

(define-public crate-szs-0.0.5 (c (n "szs") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "19x98gxj7nx5f1l7zi4vvhr2dfkbjiiaz9jcqjg84jspszgalz91")))

(define-public crate-szs-0.0.12 (c (n "szs") (v "0.0.12") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "01jn99rdh0hm0182za78fnx2dpik7ixacysx8r640i10p2rhvxnf")))

(define-public crate-szs-0.0.17 (c (n "szs") (v "0.0.17") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1wzm35j1cqr3lzg70yvqanxcdivpmcq3ql80399yrxc4dcp04i6b")))

(define-public crate-szs-0.0.24 (c (n "szs") (v "0.0.24") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1zh1b23b597dk660913p9zgqj05lcpg9ikimi1iwaj367mddckx1")))

(define-public crate-szs-0.0.26 (c (n "szs") (v "0.0.26") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1jl2wa3wwf3higa5ymanjchqgrsi0yamr4pf45hjppwavhwfpygn")))

(define-public crate-szs-0.0.27 (c (n "szs") (v "0.0.27") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0qj939pps42a4z9kfgawyhmnb4fv32lp2j7irc74j6izr12fd04h")))

(define-public crate-szs-1.0.0 (c (n "szs") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "114yp4la5p3gm5k8jfnhhls8plf1dl6m387nxwg3s9cah26vgkv0") (y #t)))

(define-public crate-szs-1.1.0 (c (n "szs") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0rxgj1llzy384x8x3mngzchgzxiqiqfszkqwb8r7q4xifvdx301m") (y #t)))

(define-public crate-szs-0.1.0 (c (n "szs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "03gf3xgnh74krhq2bq2wg5d6bkdzxm62f47zlk0921655ncnmwvr")))

(define-public crate-szs-0.1.1 (c (n "szs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1larlm10ymhnfwjccwvr9i8avfpfj19md2nzm0jalacpifji17hy")))

(define-public crate-szs-0.2.0 (c (n "szs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "04vpvab92gadzm9fjgagrlccdkbazddan5p7n2nznsc1az9rcsh9")))

(define-public crate-szs-0.2.7 (c (n "szs") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0hiw52g1a2j0mhjg723fsdar6xyddcc8nxjv7f6cwskbgpbfm2r4")))

(define-public crate-szs-0.2.8 (c (n "szs") (v "0.2.8") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1mvmfxnznjascf4zcli89awc6bffwnn9c6q89k98v01m681fs8nc")))

(define-public crate-szs-0.2.9 (c (n "szs") (v "0.2.9") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "18pp2v665s37sc4gmcmvama4vn4pc48x9dz5z8lp172969mwsvnw")))

(define-public crate-szs-0.3.0 (c (n "szs") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "18rp7jj93jw75vi62gp20ksxr9a3hsk4r71mnfzamvnni2nwrwq9")))

(define-public crate-szs-0.3.1 (c (n "szs") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "16bw67j2fa9i22q75j93zi8apf8z0z0xra0zdn880j3xl38g7avh")))

(define-public crate-szs-0.3.2 (c (n "szs") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "02rdzm9gx9g9d391w55ji9ylvq9az7xh28m5y9ffjxfm4qqzfzl8") (f (quote (("default" "c_api") ("c_api"))))))

(define-public crate-szs-0.3.3 (c (n "szs") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0xmcg8330v2lnqldjcddwav5g95s6fsw6xsy39j433423r0nylky") (f (quote (("run_bindgen" "bindgen") ("default" "c_api") ("c_api"))))))

(define-public crate-szs-0.3.4 (c (n "szs") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.57") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0c554pc2bfvfr428xha76kmszjigxsq2gfxrs0c3488k78fxnd3v") (f (quote (("run_bindgen" "bindgen") ("default" "c_api") ("c_api"))))))

