(define-module (crates-io #{3}# s sid) #:use-module (crates-io))

(define-public crate-sid-0.1.0 (c (n "sid") (v "0.1.0") (h "1jf5hy86yw0bx0ndf51yy5inql22l70h54q5r9mv3mw1494dxb52")))

(define-public crate-sid-0.3.0 (c (n "sid") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "10hxzn2r8zny6dfhxnki6y9j2ycyhbrhh5i1dz9ij694qyf0bzbs") (y #t)))

(define-public crate-sid-0.3.1 (c (n "sid") (v "0.3.1") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "09xmjrnk7m3k1ygi5z1kv2b2l0b6s0mzkrsqnywlb2jvfm3c9gqy")))

(define-public crate-sid-0.3.2 (c (n "sid") (v "0.3.2") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "0h56bwhfp3c45lz7pv1b3drxaljfgk7xwmya6sfcz6zbv5d4hirh")))

(define-public crate-sid-0.3.3 (c (n "sid") (v "0.3.3") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "1wijybwchbnn2wnhfh31r3f4nzsf0kqcalwqayj1dcddl21kplz9")))

(define-public crate-sid-0.3.4 (c (n "sid") (v "0.3.4") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "0s2893abay5lw3a67dfm8gww9ph0albkj81cw221r3s1dnmpia44")))

(define-public crate-sid-0.4.0 (c (n "sid") (v "0.4.0") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "1aya4fcvn0jd018i45bs8sn3ymk28z8ny8hw5m9gvcb4fzq2whm6")))

(define-public crate-sid-0.5.0 (c (n "sid") (v "0.5.0") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "1brg10j07fhj286vwnnyrsw12zx9lddzhiaxy9kfdgmkazxla4xz")))

(define-public crate-sid-0.5.1 (c (n "sid") (v "0.5.1") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "0k65hdjq1pfqzla8hf4vaxh9l3gd51h9gxbp6ak6j9nqdsrj44zh")))

(define-public crate-sid-0.5.2 (c (n "sid") (v "0.5.2") (d (list (d (n "num-traits") (r "^0.1.39") (d #t) (k 0)))) (h "1k8f2ifmyps05mlpcz4gad3bxnxxvaisqfrn9655ckghdh0adq19")))

(define-public crate-sid-0.6.0 (c (n "sid") (v "0.6.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0v9s1n7s8k0k9dg8fxilqb4arf9kyk6rx0p3mxv0myl96hawgx6m")))

(define-public crate-sid-0.6.1 (c (n "sid") (v "0.6.1") (d (list (d (n "num-traits") (r "^0.2") (k 0)))) (h "1nqvp7qfy7a4cvvzykh1izfvq4f2aac537mskf3v8j0r29ncanmx")))

