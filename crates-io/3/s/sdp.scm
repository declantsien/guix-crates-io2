(define-module (crates-io #{3}# s sdp) #:use-module (crates-io))

(define-public crate-sdp-0.1.0 (c (n "sdp") (v "0.1.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.1") (d #t) (k 0)))) (h "09s02vzfsq87k2x5jmq76g0i7qwsxrn8fxmp052y3gv2l38zbpbc") (y #t)))

(define-public crate-sdp-0.1.1 (c (n "sdp") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0p6ny776aab976vx1gdc5vbrdzh03k7bz0n4qal66ij1ihmh5vv1")))

(define-public crate-sdp-0.1.2 (c (n "sdp") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1156nfzbi2prxjbrkczkzy3cmmyz0g8ibvi86hnpcxsllbjhx0a5")))

(define-public crate-sdp-0.1.3 (c (n "sdp") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "18xpral06g7704h63c79b9913zn5s9s7w1qkyciz2kjwyx8pzii7")))

(define-public crate-sdp-0.1.4 (c (n "sdp") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "0lmiv1bbpi309yjkd4fygvr467wq2wq3ap2gcvhm0ga7y7fs8jpi")))

(define-public crate-sdp-0.1.5 (c (n "sdp") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "1v5q1d1a2wpkj9x7bxw148wic9y33gjs06zv0w286wxr561wwwyv")))

(define-public crate-sdp-0.2.0 (c (n "sdp") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1ax73ian2a0wik0qkwhpab3y5vic1ddclvmpwg2hzhgxgjwz1j8a")))

(define-public crate-sdp-0.2.1 (c (n "sdp") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0fqncmqbk65rlr553n925f0jmdnfj3lsnjaihyylnfzlb270m3cw")))

(define-public crate-sdp-0.2.2 (c (n "sdp") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0jvypcqq1fdq7rs7kjb0ixl0gn8h90fq7wa6bbkagwim8b2470w8")))

(define-public crate-sdp-0.2.3 (c (n "sdp") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0zd5k08kcjgxjcvq61lx2fgbsz8w8my7dr9qrdqp2q3ciirih16d")))

(define-public crate-sdp-0.3.0 (c (n "sdp") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0hqs88hlc7msb28am6g0in0jhfq0fk9h6ghaj2bp7q066x5ssr2m")))

(define-public crate-sdp-0.4.0 (c (n "sdp") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0mj9l4nmf09lyz6j03r3xrz1gsgljasq6aiyq7g3q2wc0870pnip")))

(define-public crate-sdp-0.5.0 (c (n "sdp") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "029q9mjr4sp5jc93gh9fnbk0mfv4805c2ms52r0k3j9li5k7z0jf")))

(define-public crate-sdp-0.5.1 (c (n "sdp") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "substring") (r "^1.4.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "178rnllnz6b35999pajl8ajvng4dlzraq5qcp2cq7mcbjxrrh1vb")))

(define-public crate-sdp-0.5.2 (c (n "sdp") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "substring") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.10") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0gskw4jliirldybvb7x408fmb9dxmadcvi4yhljipj4wsp1d0iw7")))

(define-public crate-sdp-0.5.3 (c (n "sdp") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "substring") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "~1.0.10") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "04sf86656vyzp2s9v5bkn5lj4x742pp64i9bswzqjwbq83psa8jd") (r "1.60.0")))

(define-public crate-sdp-0.6.0 (c (n "sdp") (v "0.6.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "substring") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0h3nfv7j1hp63a0p8i2njn58cwvnw9j0vsv2hxjn7kpb6160als6")))

(define-public crate-sdp-0.6.1 (c (n "sdp") (v "0.6.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "substring") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1ir4gq3qcw6jgbf2lqsvjs9shcl8jlr9hpz380bynl0i3wg7745g")))

(define-public crate-sdp-0.6.2 (c (n "sdp") (v "0.6.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "substring") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0gq945a835bswsv44acd1kpld97hxfbp67ijxnn52x5icsvls98k")))

