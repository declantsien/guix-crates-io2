(define-module (crates-io #{3}# s ssh) #:use-module (crates-io))

(define-public crate-ssh-0.1.0 (c (n "ssh") (v "0.1.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0md61i3gv7q06pjl33kxw9n3iwhbs4b6khsi987fi1d2c7xys48w")))

(define-public crate-ssh-0.1.1 (c (n "ssh") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0s80prbcv1zwbyhm1q40ql7v41i2n31w1llq8gwc2ps7d7xa5mzv")))

(define-public crate-ssh-0.1.2 (c (n "ssh") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "19b1zg66qg9xp12sy3zi9vvllwl9l0cv7br5mxlw3kp9qjm17j87")))

(define-public crate-ssh-0.1.3 (c (n "ssh") (v "0.1.3") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0zdm709dvs0qs4lc41i8nibnasyfifjb1i1vm2rr5rb26dy2fazw")))

(define-public crate-ssh-0.1.4 (c (n "ssh") (v "0.1.4") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1bb820bykl3yxldpybiz6g21sbl3bsw2lr5g0skd63811gg1dn1d")))

