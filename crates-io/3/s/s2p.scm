(define-module (crates-io #{3}# s s2p) #:use-module (crates-io))

(define-public crate-s2p-0.2.0 (c (n "s2p") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.1") (d #t) (k 0)) (d (n "polars") (r "^0.30.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.27") (d #t) (k 0)) (d (n "syn") (r "^2.0.16") (d #t) (k 0)))) (h "02pfx9k11cmjghfy53m4mw1wzx8ni1yqi29dq3fnc58fz15npnnm")))

