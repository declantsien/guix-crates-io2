(define-module (crates-io #{3}# s stm) #:use-module (crates-io))

(define-public crate-stm-0.1.0 (c (n "stm") (v "0.1.0") (h "0c7az4r30x56iwi46wsalq4gww7r4klr7rh9mk3hivx2msyjfxia")))

(define-public crate-stm-0.1.1 (c (n "stm") (v "0.1.1") (h "0l2bahm13phvd5cw2f8g0ky876gr7gjn621rlp3wd0nh1z7z024i")))

(define-public crate-stm-0.1.2 (c (n "stm") (v "0.1.2") (h "17labwrf5zzwvnxl1kp82paz9cgl6c2rm5q3dgjlph1vv7ld5i8m")))

(define-public crate-stm-0.2.0 (c (n "stm") (v "0.2.0") (h "1rayngdsx394dy4cgpwh3ryl4l947ng4wh9iwg2yhszrlcakvhns")))

(define-public crate-stm-0.2.1 (c (n "stm") (v "0.2.1") (h "13677vis4r6a94af3a6lvlwd36x4p2kx069anza50p1c6r667h1g")))

(define-public crate-stm-0.2.2 (c (n "stm") (v "0.2.2") (d (list (d (n "clippy") (r "^0.0.98") (o #t) (d #t) (k 0)))) (h "0a0ryr9i488annbws20hrlwqgmznbb8v2j5pq86hnwlwrqxwcd1i") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-stm-0.2.3 (c (n "stm") (v "0.2.3") (h "192zgakjbrid5j3nh0iq1i6iv42pfwjqjds1ika8wai6909cyyzk") (f (quote (("default"))))))

(define-public crate-stm-0.2.4 (c (n "stm") (v "0.2.4") (h "1yv7r1qvgwaxz29gyyxjml5s49qss6fl5kkjm1ajiwh6vw9hi9wd") (f (quote (("default"))))))

(define-public crate-stm-0.3.0 (c (n "stm") (v "0.3.0") (h "1a6vzl4abxz9dsy10bfnm98mx47ygnxkmvv2i8hywj4aq8b0rgxf") (f (quote (("default"))))))

(define-public crate-stm-0.4.0 (c (n "stm") (v "0.4.0") (d (list (d (n "stm-core") (r "^0.4") (d #t) (k 0)))) (h "0aq0ya6mfbj792qq22ygb3mwnxsdvn7ak6864d1hansmfmf56wz7") (f (quote (("default"))))))

