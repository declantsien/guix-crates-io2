(define-module (crates-io #{3}# s sod) #:use-module (crates-io))

(define-public crate-sod-0.1.0 (c (n "sod") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)))) (h "12lz88rlnqwa8gxh41l166j7sj5z7r81p91ssi3h4ihvyypdxwb4")))

(define-public crate-sod-0.1.1 (c (n "sod") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)))) (h "1n5vc4f0vnl5dkfrlljyc5ww6f42aj0m05wd6jrkfs7bxr656l17")))

(define-public crate-sod-0.1.2 (c (n "sod") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)))) (h "08qfaxqvdzlhdldc129qb29zxs051pix8x9drjfvnv7c9abxis9n")))

(define-public crate-sod-0.1.3 (c (n "sod") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)))) (h "0iqddx1ix1xdbb4hak8ha15v3zrl6318j16xfw7j7wf4yvyl662a")))

(define-public crate-sod-0.2.1 (c (n "sod") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)))) (h "1qvw5jl5bhdxm4aiiap1dmmzy85a236vnai4h7jkvkwp8z3mv4b8")))

(define-public crate-sod-0.2.2 (c (n "sod") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)))) (h "0kwlnl2nivdzv5qmpsmghasa1gsid05vja24yrz22zlga7bxvw13")))

(define-public crate-sod-0.2.3 (c (n "sod") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)))) (h "1wyifbq5ilxpg9gn72ry0aiggah10hn7fsrf4wfan4pzal4xrf1b")))

(define-public crate-sod-0.3.1 (c (n "sod") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)))) (h "1zcxiybmz4vvmmh6xcx9c9fhwsaxhrlzas4ma49c04nrm90avwsj")))

(define-public crate-sod-0.3.2 (c (n "sod") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.5") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 2)))) (h "01pizp99hcdynh5c2fm262ri5ljmd1913wsilbmq7sv5dypyfrjg")))

