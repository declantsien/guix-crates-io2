(define-module (crates-io #{3}# s slc) #:use-module (crates-io))

(define-public crate-slc-0.1.0 (c (n "slc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1nzh2mlmcqdmmiw0xr4bzqw7yhkg00knxr7pc0i72svgg4k4xkrj")))

(define-public crate-slc-0.2.0 (c (n "slc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0i52ggb941zgivfm9mrpn4sv1y19wrywfs92j8ibc9m88v4kb3v9")))

(define-public crate-slc-0.3.0 (c (n "slc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1kbg08sx5xjgq6v62pnqkcn1s1cpr6j2d75bq8lr11gq36fiv0s6")))

