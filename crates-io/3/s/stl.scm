(define-module (crates-io #{3}# s stl) #:use-module (crates-io))

(define-public crate-stl-0.1.0 (c (n "stl") (v "0.1.0") (h "1xayylvr0ll17q4vm3pcxh08vzvdqqa8n6260dfkjjxj7pdw4w90")))

(define-public crate-stl-0.1.1 (c (n "stl") (v "0.1.1") (h "0zc27g0a88s6k40dc45mzdw0sd6svj3fcvij5gn1v2h8dl6kkfa6")))

(define-public crate-stl-0.1.2 (c (n "stl") (v "0.1.2") (h "0f479lq7iqnpqvy6fm6i1dbq3glndsggxai4xwmgk5z5jlk5cxjp")))

(define-public crate-stl-0.1.3 (c (n "stl") (v "0.1.3") (h "09m9rxh8xplp7li1fv8i9flz53235d4f7b9rb418pj0bjkj7i62f")))

(define-public crate-stl-0.2.0 (c (n "stl") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "0chmj9995z765fflqldf3vnp3cani859lyfan8krh3352qh71ny0")))

(define-public crate-stl-0.2.1 (c (n "stl") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.4.2") (d #t) (k 0)))) (h "04kfj8isclf2cl60bx4pk4fvfzvilfpg409aavq533r5m6rp4vj6")))

