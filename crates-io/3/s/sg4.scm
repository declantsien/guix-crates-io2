(define-module (crates-io #{3}# s sg4) #:use-module (crates-io))

(define-public crate-sg4-0.11.0 (c (n "sg4") (v "0.11.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "163rl3dfkzba1ma55skfm1n8kk8j2y65kbn1r23rihbc17wx9rif")))

(define-public crate-sg4-0.12.1 (c (n "sg4") (v "0.12.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0nn6hw66jip99sxchk7krclgcla7qp15i5mq51izsgf7zb2yr3bk")))

(define-public crate-sg4-0.16.0 (c (n "sg4") (v "0.16.0") (d (list (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1d91qx9jf32ll6k05zj30mhcysa32i4jsn5kvhv1hwym4rw5inzf")))

(define-public crate-sg4-0.19.0 (c (n "sg4") (v "0.19.0") (d (list (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "08c0x29mmr71jgi5y00c4afa28kk4gl69yjd1nyj8ylcf79hm4cd")))

(define-public crate-sg4-0.20.0 (c (n "sg4") (v "0.20.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "02dzn6yd3c0cry98a7iwdf150w3rk62d3zmsy18l2jh00ljizyv7")))

(define-public crate-sg4-0.21.0 (c (n "sg4") (v "0.21.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1c2hi0v3fshb81zpx72gxig51pvrdwx2hyij9czvna7pbl0m9vfg")))

(define-public crate-sg4-0.21.1 (c (n "sg4") (v "0.21.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0l3bnrksdf5nv11pcgzis3yr1cn85f0hqr5c795vcfnxylmk4pjd")))

(define-public crate-sg4-0.21.2 (c (n "sg4") (v "0.21.2") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1anq10pg662br6b28h0c2l94apwxwp1nja679gc6j6r6fhibqx7g")))

(define-public crate-sg4-0.21.3 (c (n "sg4") (v "0.21.3") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "12l1l0q7gdbxvr8ai67k3w8z2lkc35k4bhkhvkibvp4fa1cyhss5")))

(define-public crate-sg4-0.21.4 (c (n "sg4") (v "0.21.4") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1vrigwyandbf0j5dvrwxm8cf88z1b0z5vbrx4afm31cgrn8kp7zy")))

(define-public crate-sg4-0.21.5 (c (n "sg4") (v "0.21.5") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0w03r9yn4capq6ds4cy8lc0mh23jadmvfhyy095m1yp29vxsyhwa")))

(define-public crate-sg4-0.21.6 (c (n "sg4") (v "0.21.6") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "00sn8h24dmn36w293zs76fjxzchylsfxcyb4dg52cvnyk9lz6vsd")))

(define-public crate-sg4-0.21.7 (c (n "sg4") (v "0.21.7") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0mrmiamm4bhkffncsj66qgm6686xg8adhdwrq58acwv87avng9fd")))

(define-public crate-sg4-0.21.8 (c (n "sg4") (v "0.21.8") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "089nikha1dvq8w64h4vcs032vjkgpjy79z5bw73kmnw9pbhw58nr")))

(define-public crate-sg4-0.22.0 (c (n "sg4") (v "0.22.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1a9ncgxibphawcg34759ig3y8rph0g4dawz783589p6d5awsjzs7")))

(define-public crate-sg4-0.21.9 (c (n "sg4") (v "0.21.9") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1949x2lprm0vb8ca9xmhiv898l57iii1zcph39rxvhqvn8x0f3l2")))

(define-public crate-sg4-0.21.10 (c (n "sg4") (v "0.21.10") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1dkm1z2hs4ncx850655g8x1kscwla9bmkvk0s3fbxli8s3d10n2r")))

(define-public crate-sg4-0.21.12 (c (n "sg4") (v "0.21.12") (d (list (d (n "cosmwasm-schema") (r "^1.1.5") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0y3ah69ls3dsjwz3l1yd83ywrh7ncchpgdva48k0npmmb5yxlaxd")))

(define-public crate-sg4-0.22.1 (c (n "sg4") (v "0.22.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "09rj5f1lxpk4682kkz55k2ag05pfizn9prmi4q46zxgf80kxn8j9")))

(define-public crate-sg4-0.22.2 (c (n "sg4") (v "0.22.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "053zdh5rxasp9msv5rvigxcxdyp5m95vxs34kqnklii19n0himx4")))

(define-public crate-sg4-0.22.3 (c (n "sg4") (v "0.22.3") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "15zalahq7gpq7z5fz54kz85p4i3gsr35fclkydcnrngnkxawxc7s")))

(define-public crate-sg4-0.22.4 (c (n "sg4") (v "0.22.4") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1jckgyfkivdq37sc4qpd9sx7qin638aibgkkan3f9vk00qnlv2ds")))

(define-public crate-sg4-0.22.5 (c (n "sg4") (v "0.22.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "07w1pr7h3419854d89nsm20h3l3hn74sbxq3q3dsj7dwaang9bq4")))

(define-public crate-sg4-0.22.6 (c (n "sg4") (v "0.22.6") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1sqjs2xbk3cbnz59x5i90yk11287nhzwdkg8jf6d0bxyslqpkfdz")))

(define-public crate-sg4-0.22.7 (c (n "sg4") (v "0.22.7") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "02aqx1zni6v1cdl1z2vbj5gr3rzljfrkgv0wznrvzfalvfyc7pvn")))

(define-public crate-sg4-0.22.8 (c (n "sg4") (v "0.22.8") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "17hfgphc484dgwda59ddgnrr436mgcbnmgjircsw2c9pja6z32ry")))

(define-public crate-sg4-0.22.9 (c (n "sg4") (v "0.22.9") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0wpgrdn1dlcaklqxgcha9ghlnb2aimspapk1lnw098v493a6kw92")))

(define-public crate-sg4-0.22.10 (c (n "sg4") (v "0.22.10") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ib789n3vk5zlp80igrfnw9lzmsp7khrp2qjmiiqinjv5d31gf48")))

(define-public crate-sg4-0.22.11 (c (n "sg4") (v "0.22.11") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1k26j2hg2xlssrw2524r5x73izrpswnr986qr4m71sxb21rzfqc3")))

(define-public crate-sg4-0.23.0 (c (n "sg4") (v "0.23.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1fx68m1jv54s524n8pgvpybxyp7vh1kcx583zgnnzj4qgdap7cv9")))

(define-public crate-sg4-0.23.1 (c (n "sg4") (v "0.23.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1nnf73zb4by4azmzcpvvc88z6zyjndl37aibl6wy7p2bkrhv5hhq")))

(define-public crate-sg4-0.24.0 (c (n "sg4") (v "0.24.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0qxxmncy8kvp0i8j8k4fab7yq5qx9kkka5qjfird4531vgqnqjna")))

(define-public crate-sg4-0.24.1 (c (n "sg4") (v "0.24.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "00hqk0b76g82ds75xp37bbx3y0mnydkncfm28112x52cqd73knka")))

(define-public crate-sg4-0.25.0 (c (n "sg4") (v "0.25.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0nbcr4h2llxx2qjp3k705h7k7gp09synzawvpznh6rdk12wq41nc")))

(define-public crate-sg4-2.1.0 (c (n "sg4") (v "2.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "03xr6pl7rl9pj0ngn2ml0r0nl3hc80jybqarqcih6p05kmzhc11w")))

(define-public crate-sg4-2.2.0 (c (n "sg4") (v "2.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "09yv14q7siw3pjhqk5m04gid9cn72wqhq9q9rdiw1fygss14fzb1")))

(define-public crate-sg4-2.3.0 (c (n "sg4") (v "2.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0h6khm4xjz0mx72pfmi35s41qjafvsscsrsqafjlkm0yxg8mzv1c")))

(define-public crate-sg4-2.3.1 (c (n "sg4") (v "2.3.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "01pfhlrrzg0b36bp2d596j1brwfkvdl6i418bm5knjrf8lq00hk0")))

(define-public crate-sg4-3.0.0 (c (n "sg4") (v "3.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0sncwf32bd9flf0r08a66ssswrs1y1l0sp5w9zg30ycc1j5dcmip")))

(define-public crate-sg4-2.4.0 (c (n "sg4") (v "2.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1r2wp5z784f9l7yp09wkazb2i3q9a33cp4hzm053hm24clbrxnhq")))

(define-public crate-sg4-3.1.0 (c (n "sg4") (v "3.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "188553c7nniq6x5ivnfwwfamyrnls9gcrq480xa2l90n97jc6p40")))

(define-public crate-sg4-3.2.0 (c (n "sg4") (v "3.2.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "04icszynv0nmwffizgdmirwhcykm5czq902jji13lfsdq9xxz5bq")))

(define-public crate-sg4-3.2.1 (c (n "sg4") (v "3.2.1") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0dhkh1f1rixah4dkqjvmrwdlgcmw7qxcvifr7cqgcqhv9h9gjiib")))

(define-public crate-sg4-3.2.2 (c (n "sg4") (v "3.2.2") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1mmw28cr01p09qz51d6v10xi5wrbz828kwsfxj2ddw0iarc6d729")))

(define-public crate-sg4-3.2.3 (c (n "sg4") (v "3.2.3") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0yb639alnnqlbzzgkm21ybahb385prkzf1pwijp1zlywcx8ljcwd")))

(define-public crate-sg4-3.2.4 (c (n "sg4") (v "3.2.4") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0si6yr5q5x45crjzw3pxwnglq0935f68fwcmwdppnfmkkyvchiw1")))

(define-public crate-sg4-3.2.5 (c (n "sg4") (v "3.2.5") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "10lf5h6sykcq2wdr7j2sd49kfpnm9iqwfbn720dj7rih0ngmsavl")))

(define-public crate-sg4-3.2.6 (c (n "sg4") (v "3.2.6") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0xk3wl8i7knb3hv397zbr06ayzbfnf830fdh6bc78ky4p0a5cvj1")))

(define-public crate-sg4-3.2.7 (c (n "sg4") (v "3.2.7") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1i8al81600himicvmmfdvr438i6r7hksgagx80ngms7xwr2b18j4")))

(define-public crate-sg4-3.2.8 (c (n "sg4") (v "3.2.8") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0rcm38x4dzslhi71is7gz42nxiv93rhfwpkz2wb1jc6lz0am0jxw")))

(define-public crate-sg4-3.2.9 (c (n "sg4") (v "3.2.9") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1ig7bjv6drhw28bxiand5yg7mfj9a3bwr2csxrp5hn821y38f8x9")))

(define-public crate-sg4-3.3.0 (c (n "sg4") (v "3.3.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1fdns0pja13xnv5ggna5wavq3ndfnbyg42ajlwimcf3slbnzwwbv")))

(define-public crate-sg4-3.4.0 (c (n "sg4") (v "3.4.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "04096hjrkz9l57627adnpbd00xbrv8s2sw9vkv1h08hkvqly86x1")))

(define-public crate-sg4-3.5.0 (c (n "sg4") (v "3.5.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0anwlib7n91xddqwgwpxzw8xh7qr3gnhlfk39nwd864zi6h6vh0y")))

(define-public crate-sg4-3.6.0 (c (n "sg4") (v "3.6.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1cpmn61j342ggc9mk5mib8vsfxyyc3jl1fc0bx84vqqdq0qlsk8j")))

(define-public crate-sg4-3.7.0 (c (n "sg4") (v "3.7.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0l17iy919fw33z38317qw17bkpb09j1dw2r2z8jwyip6hqcpbbjl")))

(define-public crate-sg4-3.8.0 (c (n "sg4") (v "3.8.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "076m90i4k4q9xzkg0cbl5dgvv1lbpdkzcjb6sfn6m2p1afy6889g")))

(define-public crate-sg4-3.9.0 (c (n "sg4") (v "3.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1d4bkajs81662bg0s55bysls86wjbbqi4v6hxm97szkgv9x2qaqs")))

(define-public crate-sg4-3.10.0 (c (n "sg4") (v "3.10.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0nfr1hr0vvrrc70zl73ci6gjbjyqwqz506iii16ygn13gynvrnki")))

(define-public crate-sg4-3.11.0 (c (n "sg4") (v "3.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0nl6isja3g0h6baj0xgc6fi415q1068f4xcqld33mvxlksmxqi5h")))

(define-public crate-sg4-3.12.0 (c (n "sg4") (v "3.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "00saixnvkq24wnlvkhf0fz2zvd75hg3kqkzx9xczappl4dbq95r6")))

(define-public crate-sg4-3.13.0 (c (n "sg4") (v "3.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0sfk1sqzh9smqamm8aj37g9z7wn5xmlsmm93ag3mcqisapmv5wj2")))

