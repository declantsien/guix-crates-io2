(define-module (crates-io #{3}# s sam) #:use-module (crates-io))

(define-public crate-sam-1.0.0 (c (n "sam") (v "1.0.0") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)))) (h "08li3i3l561637bxlij6rbzw3w7acr8lx27wirqd7jn07hs0k4l1") (y #t)))

(define-public crate-sam-0.1.0 (c (n "sam") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)))) (h "07wmcscz4g2zfmw2d3g8wcsm5m1mkasi03piayx6zxfn073w9qr6")))

(define-public crate-sam-0.1.1 (c (n "sam") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)))) (h "0jhjs9qphp726x1pmqsp5sj8syc1l76s9vz3nsnv9z3hya9i7f7q")))

(define-public crate-sam-0.1.2 (c (n "sam") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.8.2") (d #t) (k 0)))) (h "0ppsfq0757k2rsz0jcdnmb02llsm5rhw12kxf78gvvj5m0iklnsw")))

(define-public crate-sam-0.2.0 (c (n "sam") (v "0.2.0") (h "1in8pfygygq8awxpkfsgsc03cczw0maa9l2qhan5821scy1l5qsw")))

(define-public crate-sam-0.3.0 (c (n "sam") (v "0.3.0") (h "028wf16hxal5am37pifb9vzqkvlqr9fmp7kv8dwhixxwncx51l6s")))

(define-public crate-sam-0.3.1 (c (n "sam") (v "0.3.1") (h "0y11sa59w9a38r274xkz69m4cqy1lk0x4c4p7i0bf9ci91km3qaz")))

