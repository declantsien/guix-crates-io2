(define-module (crates-io #{3}# s sul) #:use-module (crates-io))

(define-public crate-sul-0.0.0 (c (n "sul") (v "0.0.0") (d (list (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("derive" "full"))) (d #t) (k 0)))) (h "1d4447np3n5zsbb4058x6yfvslyr365k2q6nip6pb2blpahxjhcr")))

