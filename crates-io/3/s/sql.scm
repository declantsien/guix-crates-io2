(define-module (crates-io #{3}# s sql) #:use-module (crates-io))

(define-public crate-sql-0.0.1 (c (n "sql") (v "0.0.1") (h "1a1jwl5gbi9jvg6vf8zjalsf3h0649k1xpg2x130jijravz7a4wn")))

(define-public crate-sql-0.1.0 (c (n "sql") (v "0.1.0") (h "04vdixdg9m0jbqm3cvvlra8wayx3lcbn3dyi2hwcqiknrbz46brn")))

(define-public crate-sql-0.1.1 (c (n "sql") (v "0.1.1") (h "0kwa0fsqfd8l8091hlkr4pk4dh3d6bfl86pr95apm1q1sq30mg1s")))

(define-public crate-sql-0.2.0 (c (n "sql") (v "0.2.0") (h "0lylwhwblfr89w5ja1vc3fnvh2rmy4lv8fyd2xsxg0mzh48g64nw")))

(define-public crate-sql-0.3.0 (c (n "sql") (v "0.3.0") (h "1rmnwxm2ixd17l03zc2ssm1gb8m7nbm5lva22vhw5sn8rnvc3504")))

(define-public crate-sql-0.4.0 (c (n "sql") (v "0.4.0") (h "1gskpxpsks3jk6yqmbvcn0049ll5k2z7s2yf1mr7wvqch4813a28")))

(define-public crate-sql-0.4.1 (c (n "sql") (v "0.4.1") (h "01kr1r2m9swyqjjjdvdjfvc5kg0yvmz9is2izh1x9h00zzjad1j3")))

(define-public crate-sql-0.4.2 (c (n "sql") (v "0.4.2") (h "1yvfxmmnzcdclr0lvr18wbfdikxic8vs0650pw19p3rf6w2n6061")))

(define-public crate-sql-0.4.3 (c (n "sql") (v "0.4.3") (h "0m3d6w7040cqrlg8f99pnc5ffqpcjq35i8im3zlcfd78j6kihqc9")))

