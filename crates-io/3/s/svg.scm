(define-module (crates-io #{3}# s svg) #:use-module (crates-io))

(define-public crate-svg-0.0.1 (c (n "svg") (v "0.0.1") (h "12drg82kqwgzlbbs58mrxq7bjqwvh8azvy5fjyxi19b727il4yql")))

(define-public crate-svg-0.0.2 (c (n "svg") (v "0.0.2") (h "07gxi2rvy9almjrgkw3aypzkbxx7xk7yxxqm20knqwmzgiyb15fj")))

(define-public crate-svg-0.0.3 (c (n "svg") (v "0.0.3") (h "1awqrv2l4pn67rzh0rsmkjgz8z5pr2yi43f5z1gx7snmwbqvg1f3")))

(define-public crate-svg-0.0.4 (c (n "svg") (v "0.0.4") (h "096pi4jlbj0dlj61why9qyhgd5rbrivzjpykh9ly2370w7ddaa4c")))

(define-public crate-svg-0.0.5 (c (n "svg") (v "0.0.5") (h "1qhvj6vg55qmap3hms7571388d7g10zkz7464c56dgjnbs2wpli5")))

(define-public crate-svg-0.0.6 (c (n "svg") (v "0.0.6") (h "0hg2pwlrc4fr8g1v0kar4ls5hz27sk06004q3w1x8zqj0v1jlm9k")))

(define-public crate-svg-0.0.7 (c (n "svg") (v "0.0.7") (h "06qa5zlbqfh25jmnqqgi94h319q830s7i5m06ps7zkxb34w4cz9x")))

(define-public crate-svg-0.0.8 (c (n "svg") (v "0.0.8") (h "04vhzaqnf6b09r4a0prsy5986324ldwc6srxqvkvddsbvffhggf3")))

(define-public crate-svg-0.1.0 (c (n "svg") (v "0.1.0") (h "1mxvcchslqlvxgxjxhad6a8nf8x5hi1b2n6simi7g3zm1mkpi1xv")))

(define-public crate-svg-0.1.1 (c (n "svg") (v "0.1.1") (h "03nb0gzclqfx1dm4ynpxnfjghn8dp2m0dc2f373vbp42cd3zx1ir")))

(define-public crate-svg-0.1.2 (c (n "svg") (v "0.1.2") (h "118ynkrw5bgyz81brj8q2dlzdyy3frvwdkh91hbshq4c6b1b32ni")))

(define-public crate-svg-0.2.0 (c (n "svg") (v "0.2.0") (h "1jyv9prr0d8jjjq5dsmvwf7640c4syg8p1xwrsvfq3vqlxck7app")))

(define-public crate-svg-0.3.0 (c (n "svg") (v "0.3.0") (h "1nl4ab2f7gr91hc7wssw0wmg61p8bv53wnkm4fi9iv3zb39anxdr")))

(define-public crate-svg-0.3.1 (c (n "svg") (v "0.3.1") (h "03275fxc9c19dwjmj95winigd55ax5hhdjh60q3jirff0iwmc20b")))

(define-public crate-svg-0.3.2 (c (n "svg") (v "0.3.2") (h "1bq45zm8kpr5q63l00ry3g62vpsjvy5lvfi2vg6gd1bhx3nayiwn")))

(define-public crate-svg-0.4.0 (c (n "svg") (v "0.4.0") (h "1gz01c2a5jzkmnmrrvf89x1ij9wannf37vryj69czrnav9ihbq93")))

(define-public crate-svg-0.5.0 (c (n "svg") (v "0.5.0") (h "1b0wbi4bswfs2m5mrzanzsxdy5md3wf3lddp1iwyy4a9pv8mjnch")))

(define-public crate-svg-0.5.1 (c (n "svg") (v "0.5.1") (h "08km73ynbh65jj7irrz4lfgxn89c6y1y5h81advfv5dwwn2qwlaa")))

(define-public crate-svg-0.5.2 (c (n "svg") (v "0.5.2") (h "1qzlirxd4l4w4airsqz819bi2y5snaq1zjkg9pzd0wh8h0zm4ah2")))

(define-public crate-svg-0.5.3 (c (n "svg") (v "0.5.3") (h "07nsqz0bmxqi74scjwjrmdf1v06zs19y24q6xj1drhnm6xijifq5")))

(define-public crate-svg-0.5.4 (c (n "svg") (v "0.5.4") (h "102w986947b10kb2qh0680yriw1ndhvq2767gyn468m2y1ncms8k")))

(define-public crate-svg-0.5.5 (c (n "svg") (v "0.5.5") (h "1ihhl7vqjwrcp0vw64922f8625ffznikqci080mhdl7c77vcf223")))

(define-public crate-svg-0.5.6 (c (n "svg") (v "0.5.6") (h "0s6nn010l0j1ywrcnj20598a5ddv9spsj6gbyg4zqaij13pdhsb5")))

(define-public crate-svg-0.5.7 (c (n "svg") (v "0.5.7") (h "1abn9bq287sngag8c2nl6c6s6al46ss1d3b7zi19xfgswn0d1xb8")))

(define-public crate-svg-0.5.8 (c (n "svg") (v "0.5.8") (h "18q2vnna2k4swcvpsm3302392pry8h7z9xa72y1ywhwbxj6q4bby")))

(define-public crate-svg-0.5.9 (c (n "svg") (v "0.5.9") (h "1zkhlgda99znpyk5w381g24z6mfvwkyj1j07h88rynpckxyyq3a6")))

(define-public crate-svg-0.5.10 (c (n "svg") (v "0.5.10") (h "0wmv8fkc6gn04gg0l5rw075zrki7zjr7sh55iwi5idw2d46lbr2w")))

(define-public crate-svg-0.5.11 (c (n "svg") (v "0.5.11") (h "16qchv73sy8rdiyk6g3pw3x1xpwadn614486r90kr84dqy5a263m")))

(define-public crate-svg-0.5.12 (c (n "svg") (v "0.5.12") (h "1isjvchg0ppnn09cyz37k85j9b3al364ax7p96j4xzbwiqgyqqx8")))

(define-public crate-svg-0.6.0 (c (n "svg") (v "0.6.0") (h "199l1z89b6cgy14pm1xyxzpb61z2gy3zfbk2k7mkhgzaf8ca2mqz")))

(define-public crate-svg-0.7.0 (c (n "svg") (v "0.7.0") (h "1addvxzmy7q2q52q4y83k7byccbfi6y7nabmmcdgcr37hbag3pxk")))

(define-public crate-svg-0.7.1 (c (n "svg") (v "0.7.1") (h "0gw59rxc0q10i6chaj45bw0iw9b404pv5gjwibz4g43x0zjw9gwr")))

(define-public crate-svg-0.7.2 (c (n "svg") (v "0.7.2") (h "0rb9m67jd7czf6x8mjmha8m7vixla87hnmghi32g0sjs0hmci19n")))

(define-public crate-svg-0.8.0 (c (n "svg") (v "0.8.0") (h "0yfs6yqhf4xhz29xdbc1yrpj8vx2rhf3r80s12lb47d4696scr8b")))

(define-public crate-svg-0.8.1 (c (n "svg") (v "0.8.1") (h "013bq3pc9gyh71xlnkll6l1h8isf2dxk3x0rvj3alknb9hawf1xy")))

(define-public crate-svg-0.8.2 (c (n "svg") (v "0.8.2") (h "0b848rnm3mqr0c79zs15gqami9pp4m06yhhrjcimcrixb6j2bnrv")))

(define-public crate-svg-0.9.0 (c (n "svg") (v "0.9.0") (h "19xa32cyw04m77p99nsl6hkaay02v7b0vx5i5xvxfpgp7k7zkgnw")))

(define-public crate-svg-0.9.1 (c (n "svg") (v "0.9.1") (h "0xh79qsadgr34aijbxxcbcvwwcbgid9isxd218hxwlbjnfsmcmm9")))

(define-public crate-svg-0.9.2 (c (n "svg") (v "0.9.2") (h "0cm88i9r3dw44gg0lpm6zp76szrnis1nb5id5wf53i31z0c1nrps")))

(define-public crate-svg-0.9.3 (c (n "svg") (v "0.9.3") (h "1ynkglx62kbnq6drv8x3vsl67d97982gkrmp28zckaam5c9ns851")))

(define-public crate-svg-0.10.0 (c (n "svg") (v "0.10.0") (h "02fkic8yrm3p9iw19f0yj2h6c7hc0i3vyrnazkz7m0h5mccqnbg7")))

(define-public crate-svg-0.11.0 (c (n "svg") (v "0.11.0") (h "0w62wwh41ri3zjf4vcwgfklr2q3iky032n4f0azsijnz43vccjh5")))

(define-public crate-svg-0.12.0 (c (n "svg") (v "0.12.0") (h "1ccp7wp6qd35gjf3sd5kcb98h6hj57fjdg6pnsraidd0dmfwz19g")))

(define-public crate-svg-0.12.1 (c (n "svg") (v "0.12.1") (h "1y1gvrymyr9a04m9cc4xy3mhqf6668j5c452jkms3rlj6f4zzrm6")))

(define-public crate-svg-0.13.0 (c (n "svg") (v "0.13.0") (h "00c04wfj13n30pkaqxa6lwcnkalwzm0rc667bm1lqzwqzk1y05g7")))

(define-public crate-svg-0.13.1 (c (n "svg") (v "0.13.1") (h "04kim0zxjfcif7aksd4rwrsgxva5hr24hhjd6z94k13y6fnibn02")))

(define-public crate-svg-0.14.0 (c (n "svg") (v "0.14.0") (h "05y2qidpl19vzndd5cb8jkplfl5npzfhj0211r6lx3a16lv3lw0d")))

(define-public crate-svg-0.15.0 (c (n "svg") (v "0.15.0") (h "06pj366iiqw71ci3q9dpx8h55r5s2ms99pl060h4342lrn8zk611")))

(define-public crate-svg-0.15.1 (c (n "svg") (v "0.15.1") (h "1rdn3g6yxm3sg0iwzm0nmca82bi973dndlc75zwpic52v6dysgk8")))

(define-public crate-svg-0.16.0 (c (n "svg") (v "0.16.0") (h "10scx3bxv9sv5jam4aar1bbbrmjsp7ins04phzggxmkg69f1qgjq")))

(define-public crate-svg-0.17.0 (c (n "svg") (v "0.17.0") (h "17kp090hniz0axnv75ripfr5d2xhcbnyhiml30yc4ngmyd0gn3kh")))

