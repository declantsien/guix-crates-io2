(define-module (crates-io #{3}# s seq) #:use-module (crates-io))

(define-public crate-seq-0.1.0 (c (n "seq") (v "0.1.0") (h "1ry1zry7xmjgigpam9yz6k5i0v15paz12vyjjksyg3qszfag7p7p") (y #t)))

(define-public crate-seq-0.2.0 (c (n "seq") (v "0.2.0") (h "03hl602an9dbys4r1g7rshrx0dg76pxqpc4dy9c429vmwab1m4li") (y #t)))

(define-public crate-seq-0.3.0 (c (n "seq") (v "0.3.0") (h "1qbywrkarxjfpb1vxhxjnp7v233ncs2nqfginq9xx674z9hhs2cc") (y #t)))

(define-public crate-seq-0.3.1 (c (n "seq") (v "0.3.1") (h "11z4g03rbiwd2zqlkli9imqkw0jbm2m1agjb3gyg56w6ylkxpisv") (y #t)))

(define-public crate-seq-0.4.0 (c (n "seq") (v "0.4.0") (h "1g542qn14ak2mfjmwmy261b0fsp7dw3zba2pwr17lf6n6iggf6nv") (f (quote (("benchmark")))) (y #t)))

(define-public crate-seq-0.5.0 (c (n "seq") (v "0.5.0") (h "00a5dh0hkrraad8q5d7lbq78iw44s52lhf0glwl6s7hf6p6yc193") (f (quote (("benchmark"))))))

