(define-module (crates-io #{3}# s sfa) #:use-module (crates-io))

(define-public crate-sfa-1.0.0 (c (n "sfa") (v "1.0.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("cargo" "wrap_help"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1frkmk5pyn4281ijpf4av2fw59pch7jridssgr3a84gqds0i4lgk") (f (quote (("cli" "clap"))))))

(define-public crate-sfa-1.1.0 (c (n "sfa") (v "1.1.0") (d (list (d (n "clap") (r "^3.0.10") (f (quote ("cargo" "wrap_help"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "18kwdl60m6cqa1gc7pk7knzdxm9mm1vg8slc559ccixv81yy883d") (f (quote (("cli" "clap"))))))

