(define-module (crates-io #{3}# s shy) #:use-module (crates-io))

(define-public crate-shy-0.1.0 (c (n "shy") (v "0.1.0") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0cw534c4p36xhfccbrakdyifswjlr1m7bbcd4lkdb1xj29zra4c4")))

(define-public crate-shy-0.1.1 (c (n "shy") (v "0.1.1") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "05ynzwkdk8hnigawgd4zy7ri8vsnh4vp7bdhw1xs0jvbx2s22z9z")))

(define-public crate-shy-0.1.2 (c (n "shy") (v "0.1.2") (d (list (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "1al5mji43rp1sg8hm7f9w6ly8fbl6r9icx34s4arpxfj3vcacb1j")))

(define-public crate-shy-0.1.3 (c (n "shy") (v "0.1.3") (d (list (d (n "flume") (r "^0.7.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0b0357xb3z1m8x2c0dvi9cpnc7bd219g0n1714kgdnblj8xf62vp")))

(define-public crate-shy-0.1.4 (c (n "shy") (v "0.1.4") (d (list (d (n "flume") (r "^0.7.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "174k3kdz6qn5i9ip24zb0wbhn144k5y1ahhm76094qimxx8gcp1x")))

(define-public crate-shy-0.1.5 (c (n "shy") (v "0.1.5") (d (list (d (n "flume") (r "^0.7.1") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "09krhjvyd7g31k6k9p24qf5l5zqhzaqgnvddjxaibrza79wmdiv7")))

(define-public crate-shy-0.1.6 (c (n "shy") (v "0.1.6") (d (list (d (n "flume") (r "^0.7.1") (f (quote ("select"))) (k 0)) (d (n "indexmap") (r "^1.3.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.14") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 0)))) (h "0fyhbgzccw195z7nlvlzkfqaiz6d65mdgvhxw9nhb8vinzzihrdp")))

(define-public crate-shy-0.1.7 (c (n "shy") (v "0.1.7") (d (list (d (n "flume") (r "=0.7.1") (f (quote ("select"))) (k 0)) (d (n "indexmap") (r "=1.3.2") (d #t) (k 0)) (d (n "signal-hook") (r "=0.1.14") (d #t) (k 0)) (d (n "termion") (r "=1.5.5") (d #t) (k 0)))) (h "11dqhm1jm0m9g7nvdi9iws533kfpzyz7mswcyzn7f4awa6ypyaxb")))

(define-public crate-shy-0.1.8 (c (n "shy") (v "0.1.8") (d (list (d (n "flume") (r "=0.7.1") (f (quote ("select"))) (k 0)) (d (n "fuzzy-matcher") (r "=0.3.5") (d #t) (k 0)) (d (n "indexmap") (r "=1.3.2") (d #t) (k 0)) (d (n "signal-hook") (r "=0.1.14") (d #t) (k 0)) (d (n "termion") (r "=1.5.5") (d #t) (k 0)))) (h "0khzc4zg3g29mvyl8szhzz03cd6qlwsrzlad5rivmgnmvcscapa0")))

(define-public crate-shy-0.1.9 (c (n "shy") (v "0.1.9") (d (list (d (n "flume") (r "=0.7.1") (f (quote ("select"))) (k 0)) (d (n "fuzzy-matcher") (r "=0.3.5") (d #t) (k 0)) (d (n "indexmap") (r "=1.3.2") (d #t) (k 0)) (d (n "signal-hook") (r "=0.1.14") (d #t) (k 0)) (d (n "termion") (r "=1.5.5") (d #t) (k 0)))) (h "0w0laxv183zz40ykniyly4654xa704pg9h8kn0firqf27jq8m56k")))

(define-public crate-shy-0.1.10 (c (n "shy") (v "0.1.10") (d (list (d (n "flume") (r "=0.7.1") (f (quote ("select"))) (k 0)) (d (n "fuzzy-matcher") (r "=0.3.5") (d #t) (k 0)) (d (n "indexmap") (r "=1.3.2") (d #t) (k 0)) (d (n "signal-hook") (r "=0.1.14") (d #t) (k 0)) (d (n "termion") (r "=1.5.5") (d #t) (k 0)))) (h "1glikg7nfsl5bz2ymwqig482g3iylp1qrfznmd1b3afcfyml0rna")))

