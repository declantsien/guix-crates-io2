(define-module (crates-io #{3}# s sec) #:use-module (crates-io))

(define-public crate-sec-0.99.0 (c (n "sec") (v "0.99.0") (d (list (d (n "diesel") (r "^0.99.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (o #t) (d #t) (k 0)))) (h "0fc5033l73xryn24n2hrdh647w3jkgacrfgjj1v5vj75gyr2q98a") (f (quote (("std") ("serialize" "serde") ("diesel_sql" "diesel") ("deserialize" "serde") ("default" "std"))))))

(define-public crate-sec-0.99.1 (c (n "sec") (v "0.99.1") (d (list (d (n "diesel") (r "^0.99.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (o #t) (d #t) (k 0)))) (h "12ans1y3rql6rnmm403r3p27qf3y6zavw7m262f1pywliqcx6xz8") (f (quote (("std") ("serialize" "serde") ("ord") ("diesel_sql" "diesel") ("deserialize" "serde") ("default" "std")))) (y #t)))

(define-public crate-sec-0.99.2 (c (n "sec") (v "0.99.2") (d (list (d (n "diesel") (r "^0.99.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (o #t) (d #t) (k 0)))) (h "1q47dcagn5hdvdqqm5bri8b64plnxqc78akp7yv91x634k0gy0cd") (f (quote (("std") ("serialize" "serde") ("ord") ("diesel_sql" "diesel") ("deserialize" "serde") ("default" "std")))) (y #t)))

(define-public crate-sec-0.99.3 (c (n "sec") (v "0.99.3") (d (list (d (n "diesel") (r "^0.99.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (o #t) (d #t) (k 0)))) (h "0n7phl72hchdj7bl8rpkfcmqz3sayb1wgaigv7yyyaizmqch3jw0") (f (quote (("std") ("serialize" "serde") ("ord") ("diesel_sql" "diesel") ("deserialize" "serde") ("default" "std"))))))

(define-public crate-sec-0.99.4 (c (n "sec") (v "0.99.4") (d (list (d (n "diesel") (r "^0.99.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (o #t) (d #t) (k 0)))) (h "0353kg2l4p905k628sb8gz5kx2l0z0bzjw6cb98iknhnc3mj9hxi") (f (quote (("std") ("serialize" "serde") ("ord") ("diesel_sql" "diesel") ("deserialize" "serde") ("default" "std"))))))

(define-public crate-sec-1.0.0 (c (n "sec") (v "1.0.0") (d (list (d (n "diesel") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.24") (o #t) (d #t) (k 0)))) (h "06pgwc9zik2l8dkgcphv46pjwlhncjas3s0ri4pfx84px43mzcdw") (f (quote (("std") ("serialize" "serde") ("ord") ("diesel_sql" "diesel") ("deserialize" "serde") ("default" "std"))))))

