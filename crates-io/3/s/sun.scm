(define-module (crates-io #{3}# s sun) #:use-module (crates-io))

(define-public crate-sun-0.1.0 (c (n "sun") (v "0.1.0") (h "0y9rm7i6nw9bx9j9zvgip62s9r6jf98n3fzngixmwglhnxi05lmc")))

(define-public crate-sun-0.2.0 (c (n "sun") (v "0.2.0") (h "0n8vg108fa8dwdczf802ar0dsig7np5b7h6v547dq038yhw95hnz")))

