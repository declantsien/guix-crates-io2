(define-module (crates-io #{3}# s sm4) #:use-module (crates-io))

(define-public crate-sm4-0.0.0 (c (n "sm4") (v "0.0.0") (h "0h32h87xw6jdqibz7xp6kx0n0r3la95c12av4w69k8zrbz905sls") (y #t)))

(define-public crate-sm4-0.0.1 (c (n "sm4") (v "0.0.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "block-cipher-trait") (r "^0.6") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "02id9g7l9hkwndwkbwpfi6r98mxdfjs3ha7hrwb3jn4xz3c304i9")))

(define-public crate-sm4-0.1.0 (c (n "sm4") (v "0.1.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "opaque-debug") (r "^0.2") (d #t) (k 0)))) (h "1gfssasdrjgb1y5fj9cdlz5rg8m2nxja02axpyivpx0f60vfrf56")))

(define-public crate-sm4-0.2.0 (c (n "sm4") (v "0.2.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byteorder") (r "^1") (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "09vyb4hmm756xzqdwsbz79dp1g37w13q61wrvpi65c1qj3zmbbf3")))

(define-public crate-sm4-0.3.0 (c (n "sm4") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "139zmr5nky48m1xpm6wfs534abkxi1wz89g99kljw7s7ck26jw4y")))

(define-public crate-sm4-0.4.0 (c (n "sm4") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "opaque-debug") (r "^0.3") (d #t) (k 0)))) (h "1g0s2rl4znddd81yb36mc81gda2r9c7mnfx0myl2g98nm1hs41hh")))

(define-public crate-sm4-0.5.0 (c (n "sm4") (v "0.5.0") (d (list (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "05ksavm936bssf0jbq8a9cnsljafx6x4ic9x6d1xl447k481xyla") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-sm4-0.5.1 (c (n "sm4") (v "0.5.1") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0pzp03nx6jmvk2vxi1m3xljhvf13d4jgpq9qnjs8zmpz6m8vyyid") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

