(define-module (crates-io #{3}# s ssf) #:use-module (crates-io))

(define-public crate-ssf-0.1.1 (c (n "ssf") (v "0.1.1") (d (list (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "0yrrgb78drb1pdw6hrpy4bi690fw3m8rrcsk87hhg0py2jdbsry1")))

(define-public crate-ssf-0.2.0 (c (n "ssf") (v "0.2.0") (d (list (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "1355d2mpmpw6yjazm2jdw032jlj9mq0nxk57h7m75rjjrvk00iqx")))

(define-public crate-ssf-0.1.0 (c (n "ssf") (v "0.1.0") (d (list (d (n "petgraph") (r "^0.5") (d #t) (k 0)))) (h "0ksqhllzzgp1jlb99v7dg737i248chiv7n5rcm1d8gadiipnil0g")))

