(define-module (crates-io #{3}# s stk) #:use-module (crates-io))

(define-public crate-stk-0.2.0 (c (n "stk") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "1gsngs8vpbsmkn4h7r00scrlqp8b4fyb49mimyymrjr2ramr55ww")))

(define-public crate-stk-0.2.1 (c (n "stk") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 0)))) (h "0g6830hnlv449mnsdsya8msv8nqppf55qsgvhc877jmkgihcv7jh")))

