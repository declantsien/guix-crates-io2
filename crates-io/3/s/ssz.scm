(define-module (crates-io #{3}# s ssz) #:use-module (crates-io))

(define-public crate-ssz-0.1.0 (c (n "ssz") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "hash-db") (r "^0.11") (k 0)) (d (n "plain_hasher") (r "^0.2") (d #t) (k 2)) (d (n "primitive-types") (r "^0.2") (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)))) (h "0icmgn17m0pkq3ki29mpxv360grlmcv0kdfkkl6rwc8sglq3il3l") (f (quote (("std" "primitive-types/std" "arrayvec/std" "hash-db/std") ("default" "std"))))))

(define-public crate-ssz-0.1.1 (c (n "ssz") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "hash-db") (r "^0.11") (k 0)) (d (n "plain_hasher") (r "^0.2") (d #t) (k 2)) (d (n "primitive-types") (r "^0.2") (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)))) (h "0vd46qdry4cswib58c1f8352jv9y1p5dqdxqi9drimih2lq8mgwx") (f (quote (("std" "primitive-types/std" "arrayvec/std" "hash-db/std") ("default" "std"))))))

(define-public crate-ssz-0.1.2 (c (n "ssz") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.4") (k 0)) (d (n "hash-db") (r "^0.11") (k 0)) (d (n "plain_hasher") (r "^0.2") (d #t) (k 2)) (d (n "primitive-types") (r "^0.2") (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)))) (h "1iglrhpq9fcii9s1wkpkwwy7xxk1g9fvfm816jhf38bc6iigs4ws") (f (quote (("std" "primitive-types/std" "arrayvec/std" "hash-db/std") ("default" "std"))))))

(define-public crate-ssz-0.2.0 (c (n "ssz") (v "0.2.0") (d (list (d (n "bm-le") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "primitive-types") (r "^0.4") (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 2)) (d (n "ssz-derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)) (d (n "vecarray") (r "^0.1") (d #t) (k 0)))) (h "0pdm3x34dq8zyflb1v8qy6vh1q6fyvk0asl63w8c7v1v59hbif9d") (f (quote (("std" "primitive-types/std" "bm-le/std" "vecarray/std") ("derive" "ssz-derive") ("default" "std" "derive"))))))

