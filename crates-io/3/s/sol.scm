(define-module (crates-io #{3}# s sol) #:use-module (crates-io))

(define-public crate-sol-0.1.0 (c (n "sol") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)))) (h "0gkz2h8aiblz0brniwb28synbjxf1s779i0vhkmp6n5rnxlqlpn6")))

(define-public crate-sol-0.1.1 (c (n "sol") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)))) (h "10das6xnx1kzxjhw9mddmcn9gc9dr1w9mrnqhkis93dlql60lgry")))

(define-public crate-sol-0.1.2 (c (n "sol") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)))) (h "1yi9i5xvqcvmkdn9rg9f16avxy74s6spd7vxq823wymlnqq6052f")))

(define-public crate-sol-0.1.3 (c (n "sol") (v "0.1.3") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)))) (h "1bxff7hnc837dbgiz6gi07w0x35k8j1kfy985mpqjb4pq6a6g0mc")))

(define-public crate-sol-0.1.4 (c (n "sol") (v "0.1.4") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)))) (h "14zn2q9sy1s84q6hgkd21y4qi6fbnnr0js72fvlfxmmjnfk9bixf")))

(define-public crate-sol-0.1.5 (c (n "sol") (v "0.1.5") (d (list (d (n "cgmath") (r "^0.16.1") (d #t) (k 0)))) (h "167ndmp1b8zxrwpmkrhksbcmq00ip01hvilzz2lihv32sl5xiywd")))

