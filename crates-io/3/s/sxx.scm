(define-module (crates-io #{3}# s sxx) #:use-module (crates-io))

(define-public crate-sxx-1.0.0 (c (n "sxx") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yphbyp6f4j0ch3b7jjg98rnnvnm0fbiisx6gbz4hs9f3xdrd1ki")))

(define-public crate-sxx-1.1.0 (c (n "sxx") (v "1.1.0") (d (list (d (n "clap") (r "^4.3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)))) (h "0swm7212m1gdnli8zcdpc2l99ph2nxzap9cs3b6z0pkpcgpd0lwk")))

