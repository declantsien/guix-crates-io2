(define-module (crates-io #{3}# s sdi) #:use-module (crates-io))

(define-public crate-sdi-0.1.0 (c (n "sdi") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1jzsz65dnm35xs29ky8672jxwqv6hgdr9bjq0d6rc4lkpg9jm84y")))

(define-public crate-sdi-0.1.1 (c (n "sdi") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)))) (h "1adcm6rr9ywrf0wd3w5bmrvk2kcliqrlf65ymgq7iasjpw5vkhdd")))

