(define-module (crates-io #{3}# s sdf) #:use-module (crates-io))

(define-public crate-sdf-0.1.0 (c (n "sdf") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "peakbag") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sdc") (r "^0.1") (d #t) (k 0)))) (h "1shhqji876krkv2yixpirkv3m5lmj94w8i7j40fxp1i6ksc100wj")))

(define-public crate-sdf-0.1.1 (c (n "sdf") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "peakbag") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sdc") (r "^0.1") (d #t) (k 0)))) (h "0rrp6f3583wnbg9yknzckarl3sc7kxa5mv08hqcc6j70wkhb7gyb")))

(define-public crate-sdf-0.1.2 (c (n "sdf") (v "0.1.2") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "peakbag") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1a1r97xima78i5ik0na0jdgfgp56ivbnik83bpd9b9ghmjlcbrr3")))

