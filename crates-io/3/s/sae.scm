(define-module (crates-io #{3}# s sae) #:use-module (crates-io))

(define-public crate-sae-0.1.0 (c (n "sae") (v "0.1.0") (d (list (d (n "argmin") (r "^0.4.3") (o #t) (d #t) (k 0)))) (h "0z63ap0zqnk53dwcay8nlxai9saphfqnl1d3fvvv91k8lr2jgcm8") (f (quote (("optimize" "argmin") ("default"))))))

(define-public crate-sae-0.1.1 (c (n "sae") (v "0.1.1") (d (list (d (n "argmin") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)))) (h "028xfi9j8f5vbi1xhwn4fn36g5ph00h9rf78phc6isj9b1sz24x4")))

