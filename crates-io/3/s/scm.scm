(define-module (crates-io #{3}# s scm) #:use-module (crates-io))

(define-public crate-scm-0.0.1 (c (n "scm") (v "0.0.1") (d (list (d (n "commandext") (r "*") (d #t) (k 0)))) (h "0v9jpb1wgx0x4gzakdnaxmg54xmbnljc1pzrnd3kvxh40mlnwigd")))

(define-public crate-scm-0.0.2 (c (n "scm") (v "0.0.2") (d (list (d (n "commandext") (r "*") (d #t) (k 0)))) (h "1ia8w9q1kdl5mkzxa3c4gx7lsk3p23qvkzh6xy5q62dn54vqa7q8")))

(define-public crate-scm-0.0.3 (c (n "scm") (v "0.0.3") (d (list (d (n "commandext") (r "*") (d #t) (k 0)))) (h "1f6m5zrcslaiy98skmjsmwqa513m1bi0m56c5h8ajjy7nc8dldjr")))

