(define-module (crates-io #{3}# s sai) #:use-module (crates-io))

(define-public crate-sai-0.1.0 (c (n "sai") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.35") (d #t) (k 0)) (d (n "sai_component_derive") (r "=0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0kdrm9gimz6bg6c6xwmwf18y8rpj1cp6dfdskgpg5y6ixphhw78p")))

(define-public crate-sai-0.1.1 (c (n "sai") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.35") (d #t) (k 0)) (d (n "sai_component_derive") (r "=0.1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1c3xvybzda73yxfcxbvacy1a237ysxki2ibhr0b5dlbavzvpnw5c")))

(define-public crate-sai-0.1.2 (c (n "sai") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.35") (d #t) (k 0)) (d (n "sai_component_derive") (r "=0.1.2") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1zzj42gah099m70g801zjbjw1w8yasn1sv9pln6x1cqdc42fyrby")))

(define-public crate-sai-0.1.3 (c (n "sai") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.35") (d #t) (k 0)) (d (n "sai_component_derive") (r "=0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0z13bigm0jb3wxh0jxiyhfg2nzlqcshzc1w1m628fflqalnw9ywm")))

(define-public crate-sai-0.1.4 (c (n "sai") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.35") (d #t) (k 0)) (d (n "sai_component_derive") (r "=0.1.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "1cibpx4q26dj97lbn1b1n1hgq8sik33d1hm38b26ay4lwxrlnpq8")))

