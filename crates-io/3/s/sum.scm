(define-module (crates-io #{3}# s sum) #:use-module (crates-io))

(define-public crate-sum-0.1.0 (c (n "sum") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1gfj294cxr5a71yf44z0wnq5cj58lji8ayvr8ia5wlhkddlww38z")))

(define-public crate-sum-0.1.1 (c (n "sum") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15jlidnqa35233hjhs4fhv0iahi49s6jzs0ss81h6y6fs3v0nr59")))

(define-public crate-sum-0.1.2 (c (n "sum") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1cyl1m4rvgx6k11mxzchgwa5c7mbiamp820gb8pyxky98gbprd01")))

(define-public crate-sum-0.1.3 (c (n "sum") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0acxnzszzxjmcb39vd4ki5q195l5pgzp1pa72csai7prih08qwq1")))

(define-public crate-sum-0.1.4 (c (n "sum") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1a8x3dnvz2337bgfqnxjvqzcppz8r3pi9w45lif5522jba7lv3lx")))

(define-public crate-sum-0.1.5 (c (n "sum") (v "0.1.5") (d (list (d (n "futures-core") (r "^0.3") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jm9kxz636l854zsp245b1rn6kpp8h6gvm7pwb2xnb418zsv5qcp") (f (quote (("futures" "futures-core"))))))

(define-public crate-sum-0.1.6 (c (n "sum") (v "0.1.6") (d (list (d (n "futures-core") (r "^0.3") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wf56xf5ww04myvi7gcw9nwn9x60070dg95vmiraq0vy5dd5b3wk") (f (quote (("futures" "futures-core"))))))

(define-public crate-sum-0.1.7 (c (n "sum") (v "0.1.7") (d (list (d (n "futures-core") (r "^0.3") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "11cxkvp5xy977j03yxf98fgmvhy6j2b08iwbs86bq41kra8qkwdi") (f (quote (("futures" "futures-core") ("default" "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "15" "16" "17" "18" "19" "20" "21" "22" "23" "24" "25" "26" "27" "28" "29" "30" "31" "32") ("9") ("8") ("7") ("6") ("5") ("4") ("32") ("31") ("30") ("3") ("29") ("28") ("27") ("26") ("25") ("24") ("23") ("22") ("21") ("20") ("2") ("19") ("18") ("17") ("16") ("15") ("14") ("13") ("12") ("11") ("10") ("1") ("0"))))))

