(define-module (crates-io #{3}# s swu) #:use-module (crates-io))

(define-public crate-swu-0.1.0 (c (n "swu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.24.1") (d #t) (k 0)) (d (n "shadow-rs") (r "^0.24.0") (d #t) (k 1)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "1bw06p5j60fg9mnixmdg55jr2wmlcsw3m9nzxiahpk3a9f0nblnz")))

