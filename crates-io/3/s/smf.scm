(define-module (crates-io #{3}# s smf) #:use-module (crates-io))

(define-public crate-smf-0.1.0 (c (n "smf") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cnfa0pnc4ha3183k2l6zr5fy74wa2n0f6fi8dplhjvb635sffmw")))

(define-public crate-smf-0.1.1 (c (n "smf") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jnsqvw8ka6g5qp3hr2xxcmj6l7bwa1iyascnysfm829d62filw2")))

(define-public crate-smf-0.2.0 (c (n "smf") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q7bfhccfjvclb3xaprxdxpnd44axa95ch5920av783cb86rjvl3")))

(define-public crate-smf-0.2.1 (c (n "smf") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bjf7kfy5zgwr168zzhijd4pqmr78shwzza91ivw44ckx1x457gi")))

(define-public crate-smf-0.2.2 (c (n "smf") (v "0.2.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12qqmddzad617qwdbipicpqadpxvh0bf2s6w534q96r6pydml0g6")))

(define-public crate-smf-0.2.3 (c (n "smf") (v "0.2.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "136iz1vv32c196a4lisy36kv1x79kng3gg37qaip1ynz8zy1njaa")))

