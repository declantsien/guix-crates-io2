(define-module (crates-io #{3}# s snd) #:use-module (crates-io))

(define-public crate-snd-0.0.0 (c (n "snd") (v "0.0.0") (d (list (d (n "cpal") (r "^0.8.1") (d #t) (k 0)) (d (n "float-cmp") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.4.2") (d #t) (k 0)))) (h "0ga8qs9vwbhqrqaypv1819x043kg9cpgq2dpk1x208rscyzp81xj")))

