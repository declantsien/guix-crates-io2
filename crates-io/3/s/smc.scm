(define-module (crates-io #{3}# s smc) #:use-module (crates-io))

(define-public crate-smc-0.1.0 (c (n "smc") (v "0.1.0") (d (list (d (n "core-foundation") (r "^0.6.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "ctor") (r "^0.1.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "12i7j5y5vnyb5myqx3797pqsjjyx12gqxfgiw4glz781516l32za")))

(define-public crate-smc-0.2.0 (c (n "smc") (v "0.2.0") (d (list (d (n "four-char-code") (r "^0.0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "10dlsqig9c758mrq6yjg2kql8c88d9j0yh599r06glhmb4zb5c6q")))

(define-public crate-smc-0.2.1 (c (n "smc") (v "0.2.1") (d (list (d (n "four-char-code") (r "^0.0.4") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "15ybj83p8v0l2dkkfm7gvnzbbgp2r439zcdxwqbgz33zdhhz4q4j")))

(define-public crate-smc-0.2.2 (c (n "smc") (v "0.2.2") (d (list (d (n "four-char-code") (r "^0.0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "14v86a7qnxi0c2b623cpzglr7rb64fhnfq44lxv28gyng63ckgx2")))

(define-public crate-smc-0.2.3 (c (n "smc") (v "0.2.3") (d (list (d (n "four-char-code") (r "^0.0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "11szxhhqk6121389ra4bdkq87s2myi0zj8qmmwpl4rhrhhbngj9m")))

(define-public crate-smc-0.2.4 (c (n "smc") (v "0.2.4") (d (list (d (n "four-char-code") (r "^0.0.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1djrffcf8i0fiv1n8cy65jhyv67h1mbxrd4pzb2lfynxycdasa3i")))

