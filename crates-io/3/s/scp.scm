(define-module (crates-io #{3}# s scp) #:use-module (crates-io))

(define-public crate-scp-0.1.0 (c (n "scp") (v "0.1.0") (h "1nyip0p65y28hvig83vfwf7v7msjrwklj054yk47f37k3rfx7cxw") (y #t)))

(define-public crate-scp-0.1.1 (c (n "scp") (v "0.1.1") (h "1hg5xlklczyp98raqc2nz0g7mb7ii28a8hqfyx0b742syy84f2pm") (y #t)))

(define-public crate-scp-0.1.2 (c (n "scp") (v "0.1.2") (h "1wrv6gf844d9xlrj3038rmnkm8zjr90yd2kiggj899zi5wh8952z") (y #t)))

(define-public crate-scp-0.2.0 (c (n "scp") (v "0.2.0") (h "1p6p9ajx0ayjjb1l0m1midc12hvabyzp4alnnhgjb9bbgbzf5ld9") (y #t)))

(define-public crate-scp-0.2.1 (c (n "scp") (v "0.2.1") (h "0xinj0kqia94a4wz5f30cyfgsmyrqddd37aqpwg9jqcgncnmkgx7") (y #t)))

(define-public crate-scp-0.2.2 (c (n "scp") (v "0.2.2") (h "1z279qggkl6f4hsgwwhi1bbmcabvyk6q5f5d8a41y2hw8v77rgw4") (y #t)))

(define-public crate-scp-0.2.3 (c (n "scp") (v "0.2.3") (h "0p6ic41q76w8cslyajwl0df7j64sz6vzn3595vx4m6izw56f86va") (y #t)))

(define-public crate-scp-0.3.0 (c (n "scp") (v "0.3.0") (h "16jgis4fw9vd6x50cc58yb7l3038gb9khb9ckgivzgafggn5pvys") (y #t)))

(define-public crate-scp-0.3.1 (c (n "scp") (v "0.3.1") (h "134k8m46vs0hh4d96fcs10k1slk06qhnqqbyzhscm89qa34mbj64") (y #t)))

(define-public crate-scp-0.3.2 (c (n "scp") (v "0.3.2") (h "0w8yz18ivkaq20pvgp5f48nnvnxv97ms28xqchk5fd5m5fz2fyv2") (y #t)))

(define-public crate-scp-0.3.3 (c (n "scp") (v "0.3.3") (h "11s1mwlkrwidg4c1viz1dvh926zl9rwkn0i7w4f9m0jlbwwf7dma") (y #t)))

(define-public crate-scp-0.4.0 (c (n "scp") (v "0.4.0") (h "0kkx35hbsx7kigv9sm12g4hxps0jwf7hm4wgx6zvihaxchvyjhb7") (y #t)))

(define-public crate-scp-0.5.0 (c (n "scp") (v "0.5.0") (h "1aasbwhbjdsy5mis6b1h93xdh9rpryr9n20hgr4xb02x7ggblin9") (y #t)))

(define-public crate-scp-0.6.0 (c (n "scp") (v "0.6.0") (h "15xv1n4bh80fdq6ijgd2vvpq8vvl85q5sqssv3vnkrb0w66h3ikk") (y #t)))

(define-public crate-scp-0.6.1 (c (n "scp") (v "0.6.1") (h "19f5cj0zphs0ginj9la0qxvvj2spgxyff87bwk5irlgfj5zb9s3y") (y #t)))

