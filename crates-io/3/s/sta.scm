(define-module (crates-io #{3}# s sta) #:use-module (crates-io))

(define-public crate-sta-0.1.1 (c (n "sta") (v "0.1.1") (h "0zrz4809k75lvcaljrn4mzm1n7xqd29w4slci6bnwdcq052qk0hh")))

(define-public crate-sta-0.1.2 (c (n "sta") (v "0.1.2") (h "0hnyk8q1iiaw2d3i197x2yawvx48sl631v1w115y93yj856w54l4")))

(define-public crate-sta-0.1.3 (c (n "sta") (v "0.1.3") (h "0xyg7n4z0pxh5hlxhfc5a606wm14sal8ih5rylj97ri2v44xvhd9")))

(define-public crate-sta-0.1.4 (c (n "sta") (v "0.1.4") (h "1kln8xvfhqc2iqi928hplrvz2pi0xji43il4xyrajgq7grz5z0g8")))

(define-public crate-sta-0.1.5 (c (n "sta") (v "0.1.5") (h "1hgjd2gncxcszm0wgb9zvlvi5jnc8d4wnyic1i5ijc4q0imw12wm")))

(define-public crate-sta-0.1.6 (c (n "sta") (v "0.1.6") (h "0f3zl1ic81hrxc9zbrizifc0r52wq589z60chpljy85vpa0s8czz")))

(define-public crate-sta-0.1.7 (c (n "sta") (v "0.1.7") (h "1qfkkywhhrlldw12lj4jyyqzv3p4576pa39s2bvp04rzvk6k29fb")))

(define-public crate-sta-0.1.8 (c (n "sta") (v "0.1.8") (h "1b8gi5yvygiibfr7ls2kjfaazs8lcqvppgslaawz6ylh6jikfahj")))

(define-public crate-sta-0.1.9 (c (n "sta") (v "0.1.9") (h "03ixbdwjsrfky73py68i50332ljrl8y4wlwrar9rh4p1zxzk0jk8")))

(define-public crate-sta-0.1.10 (c (n "sta") (v "0.1.10") (h "0zikrpl79n8dlsq2375wgl39ly0sk97iqrqhpv55d4xh4xxm18pi")))

(define-public crate-sta-0.1.11 (c (n "sta") (v "0.1.11") (h "0jgpwrx44nfc79mpw9695k05wmimcblxviahlckn6f542gpmxc6s")))

(define-public crate-sta-0.1.12 (c (n "sta") (v "0.1.12") (h "06danpqslz213rpvnhxcp1v0zmb8pafb121q02mqd7qz01ps58hk")))

(define-public crate-sta-0.1.13 (c (n "sta") (v "0.1.13") (h "1rz480xliqxkn7yqqk7354knw4x5hsqnflaffrrzbmaff4j6bkvp")))

(define-public crate-sta-0.1.14 (c (n "sta") (v "0.1.14") (d (list (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)))) (h "11yijigdqiy3ccx8fnmn8v4x1cg8i0dj8xj4fs44yzd7rdbwvwxv")))

(define-public crate-sta-0.1.15 (c (n "sta") (v "0.1.15") (d (list (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)))) (h "0ssqmmdraqah8sv7xbcrjwg2ql4a5ngj0f1kph9rlrgpbykvls0q")))

(define-public crate-sta-0.1.16 (c (n "sta") (v "0.1.16") (d (list (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)))) (h "15x0zjzcqn61wihzgj4iw9mrdpkqn8gknp4jrpn40rnix12a1vbv")))

(define-public crate-sta-0.2.1 (c (n "sta") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "007xnbz2rd0jykdw3ijyyz38w2cf360b170jc9mdy2hg3fnxi7zx")))

(define-public crate-sta-0.2.2 (c (n "sta") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "1ckpql17ipaiabz007x3qw9vd9byfazrnnkiaib0f0ivfwv220lz")))

(define-public crate-sta-0.2.3 (c (n "sta") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.37") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.37") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "0klm96d69c3yl2g4wv0ngbk3zrb8na6vbqkinqkfd0ga6g1v1b1w")))

(define-public crate-sta-0.2.4 (c (n "sta") (v "0.2.4") (d (list (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "06r38ms387d4ivcg1j9npw1j2dxzxyyp7fkamqwzgqc7nm3v9kw6")))

(define-public crate-sta-0.2.5 (c (n "sta") (v "0.2.5") (d (list (d (n "snap") (r "^0.2") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1.7") (d #t) (k 0)))) (h "1bqgy2y9c863f11vv9w4vkyzz33irpwkb6a442x2h6cpjivnqz6n")))

