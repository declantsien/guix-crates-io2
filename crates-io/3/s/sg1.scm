(define-module (crates-io #{3}# s sg1) #:use-module (crates-io))

(define-public crate-sg1-0.11.1 (c (n "sg1") (v "0.11.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.11.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ajxv3qyqllla1nwxsi1s4ay8sqypdkha24bxq7r8nirddcncr4q")))

(define-public crate-sg1-0.12.0 (c (n "sg1") (v "0.12.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ddprkjg7npb0pwk1k3lw2drp1y132rxndii6gs5m7zrbxm6r3vm")))

(define-public crate-sg1-0.12.1 (c (n "sg1") (v "0.12.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "04p2j5vh70a0gfvchw65mnjpywb8s6j9kzddz3hk5zfw7ckjd6sb")))

(define-public crate-sg1-0.13.0 (c (n "sg1") (v "0.13.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1nnjhl50jwzsrvig8hkdq8pwvc82jk9a66pshhjgnlgwir2aakgq")))

(define-public crate-sg1-0.14.0 (c (n "sg1") (v "0.14.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.12.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0447z55gaiwrmq2fsm3jb1ig3wf6yw63bkcin0659vf3ibnp65lc")))

(define-public crate-sg1-0.14.1 (c (n "sg1") (v "0.14.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1hf4ksr1yaapy9iaipbv27bl3y3599l6gw25wby7z07qapq47363")))

(define-public crate-sg1-0.16.0 (c (n "sg1") (v "0.16.0") (d (list (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "cw-utils") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "15rf513r054p620xfacghxk5s1pw489b228mwk6qnxfblczfillb")))

(define-public crate-sg1-0.19.0 (c (n "sg1") (v "0.19.0") (d (list (d (n "cosmwasm-std") (r "^1.1.4") (d #t) (k 0)) (d (n "cw-utils") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "06ax779sr3mvrggx5rhmfxfbdkjpgic1pja9gp3lnrz839h4msjj")))

(define-public crate-sg1-0.20.0 (c (n "sg1") (v "0.20.0") (d (list (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1fgsvxa9s3hbw5ixrl2h7kfsj6idjnmnk147dqbx7khz6zr05gvb")))

(define-public crate-sg1-0.21.0 (c (n "sg1") (v "0.21.0") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ws85x7bxlknzny2axv0p4b7q33mvfpvzdi8k3nba5fdcsl26w7f")))

(define-public crate-sg1-0.21.1 (c (n "sg1") (v "0.21.1") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1k33rgx8fwxgsvxk8jif4a9s1dzvypf3xmn7xn9vmafz7qz1i2q8")))

(define-public crate-sg1-0.21.2 (c (n "sg1") (v "0.21.2") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "15s49pixla8lm6412f9naknja4dh5a606z44zzpvpv5z66nsrm70")))

(define-public crate-sg1-0.21.3 (c (n "sg1") (v "0.21.3") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "00wr3djmy27n2jnzvvabb1sr13xw2hyrx5wgjrkws0r8nc001ln6")))

(define-public crate-sg1-0.21.4 (c (n "sg1") (v "0.21.4") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0a9cfs5cn2jmf060b451ga4iyrzpnmqp1r6364c88kmgadkmnd6g")))

(define-public crate-sg1-0.21.5 (c (n "sg1") (v "0.21.5") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1qi1vxk4m1vjvlz7jkf90fy9jj0clb5aqvyhkz524crqkqjqzgqm")))

(define-public crate-sg1-0.21.6 (c (n "sg1") (v "0.21.6") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1hkspy1xaapxg0z8kgx5qwinmmzr7aiwgz4x2dwpgnsd7gggrvi3")))

(define-public crate-sg1-0.21.7 (c (n "sg1") (v "0.21.7") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "19446mp2q8xcyihxa78l6dk3jnqhs51b603iffvhidg0bflri9x5")))

(define-public crate-sg1-0.21.8 (c (n "sg1") (v "0.21.8") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "15nfv8lhvnybff04v0z2b3q3qnlsgm2sgb6c3c49dn3dp0k48ckk")))

(define-public crate-sg1-0.22.0 (c (n "sg1") (v "0.22.0") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "099cpzfxm18bic8x5887hv4wgbwbjhbl7ak3abbnam96cr59k20v")))

(define-public crate-sg1-0.21.9 (c (n "sg1") (v "0.21.9") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "09an4s7l2zqkcwn1rkqk0nblfml13iynxwjimarzmhk1bx90d7c0")))

(define-public crate-sg1-0.21.10 (c (n "sg1") (v "0.21.10") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1xh5smzw3h0iwfzqr3c7hs6sajjvqkfi4jglb7z4b1myz1z9fqnx")))

(define-public crate-sg1-0.21.12 (c (n "sg1") (v "0.21.12") (d (list (d (n "cosmwasm-std") (r "^1.1.5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0mbini5v6qiy73fsk83fwfcp36kd51fp7naggbw03k5jvcdbdkcq")))

(define-public crate-sg1-0.22.1 (c (n "sg1") (v "0.22.1") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.21.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0fx2pfp69fc2w61i9rxnnd4s6xyxcrkrz7qyl8hpajk967iy494h")))

(define-public crate-sg1-0.22.2 (c (n "sg1") (v "0.22.2") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1l49w5hh99y05hwvk26myv2hkin8qrdjq2vvmkhip128n86adl0p")))

(define-public crate-sg1-0.22.3 (c (n "sg1") (v "0.22.3") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "18skkmnk077dmiwmhsivafqcdz70dl6p30s55y4akwzp9f3prfv8")))

(define-public crate-sg1-0.22.4 (c (n "sg1") (v "0.22.4") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0r75rbb461a5cn3jvkaqy73h0q9jr49rxcdyx8hzxyl8zzi43j0s")))

(define-public crate-sg1-0.22.5 (c (n "sg1") (v "0.22.5") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ff3b0nhqzbkrpbc7a24xm2nhciq269dnckw1r2zwwjqrb8iripa")))

(define-public crate-sg1-0.22.6 (c (n "sg1") (v "0.22.6") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0i5v9hwsxia2lyj1mwn013l0r6mbp21szz9g87jhq3dwpnnwyh4l")))

(define-public crate-sg1-0.22.7 (c (n "sg1") (v "0.22.7") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1xrzq1y9ldn6768wcgmmgnylhy4d3kn28ll3rrg3jhh5p1j61zf1")))

(define-public crate-sg1-0.22.8 (c (n "sg1") (v "0.22.8") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0dbi79r1njayqj8bqpyx4cnz35k13pmbkmzxkkwr6pcqd9pyd568")))

(define-public crate-sg1-0.22.9 (c (n "sg1") (v "0.22.9") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1y5n6ls9180qsbjrcbi8hb5g7b6bkiv80vlsqwmhi6k7p3wdlwqf")))

(define-public crate-sg1-0.22.10 (c (n "sg1") (v "0.22.10") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0saxf2z2kk2cvdmfzr49dsyim7hwpsh5z3gq9q4mbivn3wkf51sa")))

(define-public crate-sg1-0.22.11 (c (n "sg1") (v "0.22.11") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0yynsn5i0ypgax543rhx807r519blm46hpsi9m9ic7cig4inj1c4")))

(define-public crate-sg1-0.23.0 (c (n "sg1") (v "0.23.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.22.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0hqxlknahirzjrmrmcy9838iaxcjx5vlk1j2radp6jvrbd3v5d0c")))

(define-public crate-sg1-0.23.1 (c (n "sg1") (v "0.23.1") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.23.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "02ljri1ws0p9zas5kqfsgck67k1r37i3bz5ljam67dcr0c2773y4")))

(define-public crate-sg1-0.24.0 (c (n "sg1") (v "0.24.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.24.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0z34jy4ahj36d3alw60ykpz3ndjw24w1ix0918n3fx6cplry8d1x")))

(define-public crate-sg1-0.24.1 (c (n "sg1") (v "0.24.1") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.24.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1c7q05r6w8xpdir628a8cl40fgcjaxzls5zfip0iqpk48i939zag")))

(define-public crate-sg1-0.25.0 (c (n "sg1") (v "0.25.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0wnwzr680lhsr9p03ak9yrp1yysd6nqcgvp9gccm3dr6jmni15kb")))

(define-public crate-sg1-2.1.0 (c (n "sg1") (v "2.1.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^2.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0q6dp8p77sdwch56nymk2r45vnd5h5p8rw8la73q7gwh7aj20l7s")))

(define-public crate-sg1-2.2.0 (c (n "sg1") (v "2.2.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16cc7ilz2v0vs3qvmal1z58z5l2hm3nx26xzm5gpqqz3r855mc5n")))

(define-public crate-sg1-2.3.0 (c (n "sg1") (v "2.3.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^2.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "04f73if7mn2j2n1r3h8j3hn7kn4dkrzhrcjnh1ymnql0lj3m93wh")))

(define-public crate-sg1-2.3.1 (c (n "sg1") (v "2.3.1") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^2.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "19v25y3fnkjm5l7zrm2d4x7gg3x0qg1bxlpgmxdly3d04sb89j8l")))

(define-public crate-sg1-3.0.0 (c (n "sg1") (v "3.0.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0lvscmmaxbjing1cxis8gzl2spyhm7nslavr5cdhbzhm62r8qfll")))

(define-public crate-sg1-2.4.0 (c (n "sg1") (v "2.4.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^2.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "13kc4z2qk3dp4bzfkiqp9cdqlyc0fbdq42jbjkibkppk6nziys8w")))

(define-public crate-sg1-3.1.0 (c (n "sg1") (v "3.1.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1axz2a27hgy8rf4nwzrjsngs1arcdvbj8bfap1inb8s0f5nx8msv")))

(define-public crate-sg1-3.2.0 (c (n "sg1") (v "3.2.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1zzk82l7xxy65ps2dx8pbdm4gzkdh76q7336iijmif3jxfjm931i")))

(define-public crate-sg1-3.2.2 (c (n "sg1") (v "3.2.2") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1fid83mbdp980f1vx481i2r62wkp7yv3q2799apiz0l9n3n0kb6n")))

(define-public crate-sg1-3.2.3 (c (n "sg1") (v "3.2.3") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0i8pcf7z5gr1xxb9h2l75zyn431fqpvvfp8s03cczfiv107iji0z")))

(define-public crate-sg1-3.2.4 (c (n "sg1") (v "3.2.4") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "022namlp37amknmgnb3vli5s9vf03fzc23hi1hnvix9860zgqjni")))

(define-public crate-sg1-3.2.5 (c (n "sg1") (v "3.2.5") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "15s5p7qcklik2k3cjj9r9kyva9g2xixabw581l9ac6hqicbkwcr0")))

(define-public crate-sg1-3.2.6 (c (n "sg1") (v "3.2.6") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0abrk4sli1k6r1x69kfa6p0a2r1jnv3yb2l9mxn90qf6rwdf2wc5")))

(define-public crate-sg1-3.2.7 (c (n "sg1") (v "3.2.7") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vg8lq61nn9jxm6avfvlc9n1qh58qyra2i9qbgvzq00c51p2bvmz")))

(define-public crate-sg1-3.2.8 (c (n "sg1") (v "3.2.8") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0vxhqcd4hj78snrn8lcin2g268zkp70ywkilpjd26njry4xy5iag")))

(define-public crate-sg1-3.2.9 (c (n "sg1") (v "3.2.9") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1947k8m4nwd6vlfhvnywdx5jbbnl085y6r43jfz8ks0xiaaz0hza")))

(define-public crate-sg1-3.3.0 (c (n "sg1") (v "3.3.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0j94j5cylz068zqnnv1j8kmv5ndkcblg754y2d4mx2am7sbk07cr")))

(define-public crate-sg1-3.4.0 (c (n "sg1") (v "3.4.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0mfm5jazsbln14570g7ixkzdypjm71fwiz9bxiycjqyg5n7hvpj7")))

(define-public crate-sg1-3.5.0 (c (n "sg1") (v "3.5.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1mkk05xi4sdmqgbaawmzhxp508p2yq6i5ssdpild505gawl39jbp")))

(define-public crate-sg1-3.6.0 (c (n "sg1") (v "3.6.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1vi47pwwpq5mmvpbj3hl2qajna7qhsl7bkb1rcrrp6s5digb0xqa")))

(define-public crate-sg1-3.7.0 (c (n "sg1") (v "3.7.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "09f6wv1vd04rnyvlilhkhv19sp2hpa5ykk831lgm2z31c4hpada8")))

(define-public crate-sg1-3.8.0 (c (n "sg1") (v "3.8.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0i6kp10gzbnyxpclkw3sx0x9323p98dsd0m4mb3yrialylskqz43")))

(define-public crate-sg1-3.9.0 (c (n "sg1") (v "3.9.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1spixxwgs5vmvrp806dmsixkp4xr0bxxz8gv1rk55kikyb0xqxy3")))

(define-public crate-sg1-3.10.0 (c (n "sg1") (v "3.10.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "03c9ff0j1g57pz8b07qmcaz1w44gp11ldq80hzkms1vddf05x167")))

(define-public crate-sg1-3.11.0 (c (n "sg1") (v "3.11.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1yg4y3xla8mdywxgsbqq4w53bm8yaaw4iynrpkqb2awygsv2zg3y")))

(define-public crate-sg1-3.12.0 (c (n "sg1") (v "3.12.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1gz00fwr54740i5bqy3h0bsi4d84ck44l3av5sis8x5mrjlivb38")))

(define-public crate-sg1-3.13.0 (c (n "sg1") (v "3.13.0") (d (list (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)) (d (n "sg-std") (r "^3.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1aack4a5s0crh1yx9jhy16r4si5l47j0mqls87h77byg8l4pksp0")))

