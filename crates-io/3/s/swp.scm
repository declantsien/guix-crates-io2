(define-module (crates-io #{3}# s swp) #:use-module (crates-io))

(define-public crate-swp-1.0.2 (c (n "swp") (v "1.0.2") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "19bxk2dak5lz92964s9gz4cdzqcyvs94xwkrk974mffq1yb2dj1c")))

(define-public crate-swp-1.0.3 (c (n "swp") (v "1.0.3") (d (list (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "dunce") (r "^1.0.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 2)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "1kc0bf3pyhmcsqglp8ydinfjjcnqczlvcny8l297nzp2mjs2hc00")))

