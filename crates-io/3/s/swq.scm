(define-module (crates-io #{3}# s swq) #:use-module (crates-io))

(define-public crate-swq-0.1.0 (c (n "swq") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "clipboard-ext") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "keyring") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "153cdds979knzxla4lf4wdznxb0v6n8q2xd1p9kja29x8v1sz93s")))

(define-public crate-swq-0.1.1 (c (n "swq") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "clipboard-ext") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "keyring") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "0vk6sgs334f078gi6gw0pkq36md3lhgq80dq812yx94sdfyzll84")))

(define-public crate-swq-0.1.2 (c (n "swq") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "clipboard-ext") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "keyring") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "10q4v7qdl6afinvzsby2yn3mx1izy05p4yna37x60js0n96nzisg")))

(define-public crate-swq-0.1.3 (c (n "swq") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "clipboard-ext") (r "^0.2") (d #t) (k 0)) (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "keyring") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)))) (h "0s4r56zygmsbar9i0gfrbnbpq1wjk0p8vv2sjp95jcf8wswh0wh3")))

