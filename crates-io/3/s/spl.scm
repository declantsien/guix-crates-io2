(define-module (crates-io #{3}# s spl) #:use-module (crates-io))

(define-public crate-spl-0.0.1 (c (n "spl") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "0j2x460g9rkzy0wyyzjr5iv09kxz37i0vqyn5ajlpyps0r89q1dn")))

(define-public crate-spl-0.0.2 (c (n "spl") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "1j38rp8y09vh23y5lrlwd9klkb3hpf35jgdpp0ql3wv8bfddzivh")))

(define-public crate-spl-0.0.3 (c (n "spl") (v "0.0.3") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "1gsab9zrb32fkvpz0rrhmw8xrqdnrz71ggi0ynyqhzv7m3qifdkv")))

(define-public crate-spl-0.0.4 (c (n "spl") (v "0.0.4") (d (list (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "1gdd12271z1s8lmnapq5pkp67mcj0rhi0zw3ymncx49n8xl0kvdg")))

(define-public crate-spl-0.1.0 (c (n "spl") (v "0.1.0") (d (list (d (n "multicall") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "1simxa3y577vgjqchngmdhjnrbzkvfgb6p64jc51cqsgwc01nwyd") (y #t)))

(define-public crate-spl-0.1.1 (c (n "spl") (v "0.1.1") (d (list (d (n "multicall") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "1z07g8ln6kw5n5i68dny44kc86ggb6y9gy6i03vmjjd7f60vhfrl") (y #t)))

(define-public crate-spl-0.1.2 (c (n "spl") (v "0.1.2") (d (list (d (n "multicall") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "0mkyyq9j8zq8kb4f3bkgrj1086ix42xgqm82rajhxqbjyqcqj0q7") (y #t)))

(define-public crate-spl-0.1.3 (c (n "spl") (v "0.1.3") (d (list (d (n "multicall") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "0q55w6cbg9w3lh24mai2iw3v87isy8r3fq4w6ihjbf45mybnpcsv") (y #t)))

(define-public crate-spl-0.1.4 (c (n "spl") (v "0.1.4") (d (list (d (n "multicall") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "1j60q4bw56dnfh9ysdl3y81vfibdy468sggldc81xxb2nxsnb64l")))

(define-public crate-spl-0.1.5 (c (n "spl") (v "0.1.5") (d (list (d (n "multicall") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "00y0dvgy3z4bfgmn01m2kjkilb3k89hbv5cy6k6zfl843kpfv1ca")))

(define-public crate-spl-0.1.6 (c (n "spl") (v "0.1.6") (d (list (d (n "multicall") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "07xx7zryqg0bcfs7hp3f28hliyfa1dlv8vavl79cyfg2yv67jzaz")))

(define-public crate-spl-0.1.7 (c (n "spl") (v "0.1.7") (d (list (d (n "multicall") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "0x3a1zmay8jb1fwc2mq0zkhknldk93mfiha5875m2s09p0wb4fna")))

(define-public crate-spl-0.1.8 (c (n "spl") (v "0.1.8") (d (list (d (n "multicall") (r "^0.1.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "readformat") (r "^0.1") (d #t) (k 0)))) (h "0s9pq2mbnvmn54aihdq1jlml5bgziv7m62bq8w8fijkqm89p9l3x")))

