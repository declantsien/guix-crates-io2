(define-module (crates-io #{3}# s sel) #:use-module (crates-io))

(define-public crate-sel-0.1.0 (c (n "sel") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "victoria-dom") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "01kkcwh359gy2bhakmj4b52qpm8zjvqxd8k2vdq79wxnk227c8a6")))

(define-public crate-sel-0.1.1 (c (n "sel") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "victoria-dom") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1b19jwym8s5fjyf7mnxsxq5y5gdb527jq7l2dhp2fya9zrk6ynxb")))

