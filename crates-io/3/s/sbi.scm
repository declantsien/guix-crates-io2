(define-module (crates-io #{3}# s sbi) #:use-module (crates-io))

(define-public crate-sbi-0.0.0 (c (n "sbi") (v "0.0.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "0dyw08ldkq0df9im5dkysc4jpi31a24q92vfnsyyw0hxfgw24l7n")))

(define-public crate-sbi-0.1.0 (c (n "sbi") (v "0.1.0") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "0knxilk2sja5b2aglv6vjiqf1vvkx2ys58li8nirmgvy6jbmb08w")))

(define-public crate-sbi-0.1.1 (c (n "sbi") (v "0.1.1") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "1g4kaxdyfqb982jcp0ga4myhv2h138kaazcqi5pl1dp18xsbshdm")))

(define-public crate-sbi-0.1.2 (c (n "sbi") (v "0.1.2") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "1kv69iznhrrflry431q1qr36ds6886yl2h68bdfcwlrawbkb3xmp")))

(define-public crate-sbi-0.1.3 (c (n "sbi") (v "0.1.3") (d (list (d (n "bit_field") (r "^0.10.1") (d #t) (k 0)))) (h "1sfm46hnp3xlk0ngp92fxypxb8bgaj0l1c0mzxacq8fm4a4hq4ly")))

(define-public crate-sbi-0.2.0 (c (n "sbi") (v "0.2.0") (h "1b92clmyc0jivk7a47fbcfkqsa6lz69ixv78hx27xjha81q0ijr9")))

