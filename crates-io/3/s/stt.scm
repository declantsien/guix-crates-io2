(define-module (crates-io #{3}# s stt) #:use-module (crates-io))

(define-public crate-stt-0.0.1 (c (n "stt") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (d #t) (k 0)) (d (n "audrey") (r "^0.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hound") (r "^3.5.0") (d #t) (k 0)) (d (n "rubato") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "symphonia") (r "^0.5.1") (f (quote ("mp3"))) (d #t) (k 0)) (d (n "uuid") (r "^1.2.1") (f (quote ("v4"))) (d #t) (k 0)) (d (n "whisper-rs") (r "^0.2.0") (d #t) (k 0)))) (h "04223rgr713dayhpxnxnpx2ygg1jpxwc9ywwxr6ifrliap69mkh4")))

