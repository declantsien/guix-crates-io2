(define-module (crates-io #{3}# s set) #:use-module (crates-io))

(define-public crate-set-0.1.0 (c (n "set") (v "0.1.0") (h "0xklmfm9n22jbls6px7ggfmarsbnh3rfr46wrjn9b0c532viw4r1") (y #t)))

(define-public crate-set-0.1.1 (c (n "set") (v "0.1.1") (h "1anzgjm3z5b5xzrvri2m4dzdkfmbbv6l8ch3vay6xsq3fy839832") (f (quote (("no_std")))) (y #t)))

