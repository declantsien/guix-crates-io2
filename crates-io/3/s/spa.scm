(define-module (crates-io #{3}# s spa) #:use-module (crates-io))

(define-public crate-spa-0.1.0 (c (n "spa") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)))) (h "18652ih5m1vl8l7q3gk5wi9sq0laa4z2sqy6p0ilj4qvj1zm6zhm")))

(define-public crate-spa-0.1.1 (c (n "spa") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "108jq2x3fbqb48xnj38asm5y4waxa9wf6liwlnz0l45rh7n274q1") (f (quote (("benchmark"))))))

(define-public crate-spa-0.2.0 (c (n "spa") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "12p6d4kv71px0hgfj8782js5daf94bsnymqppddjp77an5rhpjx2") (f (quote (("benchmark"))))))

(define-public crate-spa-0.3.0 (c (n "spa") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "06rgsqqsp6mnrp0fb1ik3wyj5sbganwcxm56agwvk34rvxv073hq")))

(define-public crate-spa-0.3.1 (c (n "spa") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("clock"))) (k 0)))) (h "16h0vcj5akyixjp0x6bf3z3smprrrx1836dpswy172ppnfal21xb")))

(define-public crate-spa-0.4.0 (c (n "spa") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (k 0)))) (h "03p7y56q88ifkv43aa2p164ir34is5lmq88274zlpd07dyawp387")))

(define-public crate-spa-0.5.0 (c (n "spa") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "thiserror") (r "^1.0.49") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 2)))) (h "1d1ayz749dfg2x8implnrihcx8zvm4rdyv8a2rfkxfkd4d9383n1") (f (quote (("std" "thiserror") ("default" "std"))))))

(define-public crate-spa-0.5.1 (c (n "spa") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (k 0)) (d (n "thiserror") (r "^1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 2)))) (h "0dmx7a9jdv0z69qfzha96bb1nk56clnny7gnkilv6yddb3sh8d5j") (f (quote (("std" "thiserror") ("default" "std"))))))

