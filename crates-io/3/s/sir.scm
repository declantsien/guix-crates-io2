(define-module (crates-io #{3}# s sir) #:use-module (crates-io))

(define-public crate-sir-0.1.0 (c (n "sir") (v "0.1.0") (d (list (d (n "dioxus") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "sir-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0z349n7qg6xbspgb55y0r343apj3k3vk6pcgivvswp2r2hiayyja")))

(define-public crate-sir-0.2.0 (c (n "sir") (v "0.2.0") (d (list (d (n "dioxus") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "dioxus") (r "^0.2.4") (f (quote ("desktop"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "sir-macro") (r "^0.2.0") (d #t) (k 0)))) (h "02wihmrii7vgmnrbismvll7vrha8vih1s0r05n7gbhpa095d2fc0")))

(define-public crate-sir-0.2.1 (c (n "sir") (v "0.2.1") (d (list (d (n "dioxus") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "dioxus") (r "^0.2.4") (f (quote ("desktop"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "sir-macro") (r "^0.2.0") (d #t) (k 0)))) (h "10prap9b31lmfwgsmi4ga1nxrg8w3kkgf67ifq4cqyj9jwpcfgvd")))

(define-public crate-sir-0.2.2 (c (n "sir") (v "0.2.2") (d (list (d (n "dioxus") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "dioxus") (r "^0.2.4") (f (quote ("desktop"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "sir-macro") (r "^0.2.0") (d #t) (k 0)))) (h "0pqw75nyvq7vlac9kr0kkqrnk1zgy4k1ibg07zg8y6k75yg7n747")))

(define-public crate-sir-0.3.0 (c (n "sir") (v "0.3.0") (d (list (d (n "dioxus") (r "^0.3.1") (o #t) (d #t) (k 0)) (d (n "dioxus") (r "^0.3.1") (d #t) (k 2)) (d (n "dioxus-desktop") (r "^0.3.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "sir-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0xx0wga7v9y0fnwdpimryf2rcyf6kwspidfqapzkbll0hifg5llb")))

(define-public crate-sir-0.4.0 (c (n "sir") (v "0.4.0") (d (list (d (n "dioxus") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "dioxus") (r "^0.4.0") (d #t) (k 2)) (d (n "dioxus-desktop") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "sir-macro") (r "^0.3.0") (d #t) (k 0)))) (h "1ajwvj4b9v9zc99zmhswv6nd1qnr1zlbwwj95z5c4qwf6b1rfrll")))

(define-public crate-sir-0.5.0 (c (n "sir") (v "0.5.0") (d (list (d (n "dioxus") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "dioxus") (r "^0.5.1") (f (quote ("desktop"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "sir-macro") (r "^0.5.0") (d #t) (k 0)))) (h "1hs5kh7k9gxpwzhcy0c4rd0aj6xp5y9mvbzrllkxdh2dlnxam9km")))

