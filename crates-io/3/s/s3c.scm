(define-module (crates-io #{3}# s s3c) #:use-module (crates-io))

(define-public crate-s3c-0.1.0 (c (n "s3c") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.47.0") (d #t) (k 0)) (d (n "rusoto_s3") (r "^0.47.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "06rlx71kyvglirqxh95zlv6si66fsb62z4k120ywrwpas0jnvdjq")))

