(define-module (crates-io #{3}# s spx) #:use-module (crates-io))

(define-public crate-spx-0.1.0 (c (n "spx") (v "0.1.0") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_shared") (r "^0.11") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "0k9qydbndlv2haccwisssv7asidf0xijmrijv17yid7pqdvigd3r")))

(define-public crate-spx-0.2.0 (c (n "spx") (v "0.2.0") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_shared") (r "^0.11") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "16dibcqjyig6y6r6j5gi8qsydr1hyab4i19j5v2qc5zwy1k4bm0v")))

(define-public crate-spx-0.3.0 (c (n "spx") (v "0.3.0") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_shared") (r "^0.11") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "0f2d0pjdpl5i2irvfz020pwcw73zb4p63ysdpvycym2cjnishm8p") (y #t)))

(define-public crate-spx-0.3.1 (c (n "spx") (v "0.3.1") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_shared") (r "^0.11") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1a8i4qvjwakxibyvkrz3b7raiab2lwnkxzv6yi2van24jwl72334") (y #t)))

(define-public crate-spx-0.4.0 (c (n "spx") (v "0.4.0") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_shared") (r "^0.11") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1gcbz3accasj12gcyzz6hs3la4cgh1a2q82q9p50kh7pfbg3jf4d") (y #t)))

(define-public crate-spx-0.4.1 (c (n "spx") (v "0.4.1") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_shared") (r "^0.11") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "1kk3zdbwl2kxf38ssgv0vcx8k9r28n4rhr46xv1jw6f6408vfqni")))

(define-public crate-spx-0.5.0 (c (n "spx") (v "0.5.0") (d (list (d (n "chacha20") (r "^0.9.1") (d #t) (k 0)) (d (n "const-fnv1a-hash") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "phf") (r "^0.11") (k 0)) (d (n "phf_shared") (r "^0.11") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "0in2vbpv0g6c65kawn2rj37p6dsg9r2iknc2xs1z3lwidkx7ia52")))

