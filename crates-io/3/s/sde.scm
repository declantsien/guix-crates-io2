(define-module (crates-io #{3}# s sde) #:use-module (crates-io))

(define-public crate-sde-0.0.1 (c (n "sde") (v "0.0.1") (d (list (d (n "fux_kdtree") (r "^0.2.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.30.4") (d #t) (k 0)))) (h "0j6b5byz7hcq16z194y9f1wicxkbnp06qdiar1a6i375d49cijmb")))

(define-public crate-sde-0.0.2 (c (n "sde") (v "0.0.2") (d (list (d (n "fux_kdtree") (r "^0.2.0") (d #t) (k 0)) (d (n "sqlite") (r "^0.30.4") (d #t) (k 0)))) (h "1gqfjhqnizl35prlmzgx7s5f2hmr891c8jcdipzbd7pvqxab2kvn")))

(define-public crate-sde-0.0.3 (c (n "sde") (v "0.0.3") (d (list (d (n "fux_kdtree") (r "^0.2.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "11mngg0axj90rwd7ij2ff5jypzxxs41nkp5s2r6nllyab645bm1w")))

(define-public crate-sde-0.0.4 (c (n "sde") (v "0.0.4") (d (list (d (n "fux_kdtree") (r "^0.2.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1n6a85vda574a3fmjisdcfh7pc614a1bbfa5zs50wgnhzzz2kldm")))

(define-public crate-sde-0.0.5 (c (n "sde") (v "0.0.5") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "fux_kdtree") (r "^0.2.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "1i4wpi90za4r0bfaxbp7g21sbdmxrwfpnp003vb36xaj9f4cxncb")))

(define-public crate-sde-0.0.6 (c (n "sde") (v "0.0.6") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "fux_kdtree") (r "^0.2.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)))) (h "0c8vys8vnphgfdwsg9x9bxk27zni00n3zpyx63qd93s6qkiyr81d")))

(define-public crate-sde-0.0.7 (c (n "sde") (v "0.0.7") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "fux_kdtree") (r "^0.2.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0c8zq3c54jbn7sqgj8ahczsmg4alln5zkdh80aa88i48kvpnql7c")))

(define-public crate-sde-0.0.8 (c (n "sde") (v "0.0.8") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hqm01cl5k0kp2rdgljyaqwciq3adqpmbk3ypcqf8vqkkwcqmqvb")))

(define-public crate-sde-0.0.9 (c (n "sde") (v "0.0.9") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h2l46x3jcs1wb3ippa321xdg61k0z110c9c2iylgmhfqz9q28f5")))

(define-public crate-sde-0.0.10 (c (n "sde") (v "0.0.10") (d (list (d (n "async-std") (r "^1.12.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (f (quote ("thread-pool"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gm3amyxjiiw660dpg7myzyj7wnncw7jg6ca9paa529bjgpwnbsx")))

(define-public crate-sde-0.0.11 (c (n "sde") (v "0.0.11") (d (list (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i2y8qpwl2n9aqqklyg9a25i8kvzj7czxhijp01z0q66rrjmajg1")))

(define-public crate-sde-0.0.12 (c (n "sde") (v "0.0.12") (d (list (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cqhc564drx2f9a45fv79gkvq8v8aa3hkvv3b3m56r5fvqg7pj8k")))

(define-public crate-sde-0.0.13 (c (n "sde") (v "0.0.13") (d (list (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled-windows"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i8p0yh9pihcqy0rhnk34vyn15gldyxrf1m5xigjw6g4jg7cx001")))

(define-public crate-sde-0.0.14 (c (n "sde") (v "0.0.14") (d (list (d (n "egui-map") (r "^0.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled-windows"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1p2235qbw48f57wd3n84mzgm3y0rfjdkj6wvqrd3qzf12sdzc60w")))

(define-public crate-sde-0.0.15 (c (n "sde") (v "0.0.15") (d (list (d (n "egui-map") (r "^0.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled-windows"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vgvqy9skbxk2r7r0mz411m7fqi7wks7nvif1c787mq2761ga75a")))

(define-public crate-sde-0.0.16 (c (n "sde") (v "0.0.16") (d (list (d (n "egui-map") (r "^0.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled-windows"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kjjl4a3iywyc5m9p9ifd073cdkmdf2czdw8anip0mbnwdaw7363")))

(define-public crate-sde-0.0.17 (c (n "sde") (v "0.0.17") (d (list (d (n "egui-map") (r "^0.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0") (f (quote ("bundled-windows"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jm8xrj6irqhmyg44091sna0ili7k4ayf5mwc7pnd3125adp7191")))

