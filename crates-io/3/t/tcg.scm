(define-module (crates-io #{3}# t tcg) #:use-module (crates-io))

(define-public crate-tcg-0.1.0 (c (n "tcg") (v "0.1.0") (h "19phnnxlb1ihwwrxh6ip0910dbmzsaqbqykszdbhsw1ji3xn0xi3")))

(define-public crate-tcg-0.1.1 (c (n "tcg") (v "0.1.1") (h "1q1bdgz1i9sij2zbafaq7hynm4d7rhrjy0618mdjchq6sh1as817")))

(define-public crate-tcg-0.1.2 (c (n "tcg") (v "0.1.2") (h "1fhrs368i4720b0a0k9lljcbbj54n90f226n0mrfwk3p6k846v1x")))

(define-public crate-tcg-0.1.3 (c (n "tcg") (v "0.1.3") (h "1p1wwd7l8w8yi89v7h8svkfpnkjsnpl5b6xn87fr23v1gs5vjxza")))

(define-public crate-tcg-0.1.4 (c (n "tcg") (v "0.1.4") (h "1qvx9i9iv83i5mqpigc0w26wfa707k836kll4bvp58xkp2akbfk0")))

(define-public crate-tcg-0.1.5 (c (n "tcg") (v "0.1.5") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "15ba7z41ga9yr77w4k5b8xsqgd4hkxjrfcw0yg2l44v4b5hld8c0")))

(define-public crate-tcg-0.1.6 (c (n "tcg") (v "0.1.6") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1106mhj78sbaad4jrpygvz1hdf14hjlmilpxxvk27lnqvxsya2bc")))

