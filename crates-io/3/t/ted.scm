(define-module (crates-io #{3}# t ted) #:use-module (crates-io))

(define-public crate-ted-0.1.0 (c (n "ted") (v "0.1.0") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)))) (h "1hhhiglyy93dng1f2f6s4598h82rs9k76y2mf32yrk7ji5jf4gjr")))

(define-public crate-ted-0.2.0 (c (n "ted") (v "0.2.0") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)))) (h "0kwc1n08bjd3dlm17mpx3nlylr9a5pjyz3c5p2lfvijaw6i7xmy1")))

(define-public crate-ted-0.2.1 (c (n "ted") (v "0.2.1") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)))) (h "05lvsdpbfqgvmb6z5174si3176rhqb9hcdp2m6nj1qpwl527l9x5")))

(define-public crate-ted-0.3.0 (c (n "ted") (v "0.3.0") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)) (d (n "sequence_trie") (r "~0.3.5") (d #t) (k 0)))) (h "0v4ld36wdk8r1yk17bnhq9v4qar9fdqnl6ldh6rp4zsbsk7m6cp9")))

(define-public crate-ted-0.4.0 (c (n "ted") (v "0.4.0") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)) (d (n "sequence_trie") (r "~0.3.5") (d #t) (k 0)))) (h "0xg56f9lwk08nwl94rhjc1k3jb5f64p9h26c361ldcazn3gv35m0")))

(define-public crate-ted-0.4.1 (c (n "ted") (v "0.4.1") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)) (d (n "sequence_trie") (r "~0.3.5") (d #t) (k 0)))) (h "0ip4gy7m3zq7ffp8xacnqvmbhfbn33phl1hwzphza2xinybb80dn")))

(define-public crate-ted-0.4.2 (c (n "ted") (v "0.4.2") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)) (d (n "sequence_trie") (r "~0.3.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.0") (d #t) (k 0)))) (h "1xs3ck3nchzn6b205si6n97501sd41355br6548y6dpzxq4x4dln")))

(define-public crate-ted-0.4.3 (c (n "ted") (v "0.4.3") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)) (d (n "sequence_trie") (r "~0.3.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.0") (d #t) (k 0)))) (h "1mi02lc1jgbcnqdyqqlqna04b2mhqbyqy20xywxyzwzqrwxhqvif")))

(define-public crate-ted-0.5.0 (c (n "ted") (v "0.5.0") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)) (d (n "sequence_trie") (r "~0.3.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.0") (d #t) (k 0)))) (h "11hai6zvgrdhgsnbzqmlfiiqirz3vask7qzfjfhc1sswavaix4vz")))

(define-public crate-ted-0.6.0 (c (n "ted") (v "0.6.0") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)) (d (n "sequence_trie") (r "~0.3.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.0") (d #t) (k 0)))) (h "095jiccj9hzlvw62h8ak0vqmnddxffj0cnj8w5nll28flya6fwzl")))

(define-public crate-ted-0.7.0 (c (n "ted") (v "0.7.0") (d (list (d (n "frappe") (r "~0.3.1") (d #t) (k 0)) (d (n "ropey") (r "~0.6.3") (d #t) (k 0)) (d (n "sequence_trie") (r "~0.3.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.0") (d #t) (k 0)))) (h "0md2n7jr7zkvr0g8hxma42684fyypfqgif9ff7b3wg7xl65iz9f4")))

