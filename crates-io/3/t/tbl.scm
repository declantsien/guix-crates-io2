(define-module (crates-io #{3}# t tbl) #:use-module (crates-io))

(define-public crate-tbl-0.1.0 (c (n "tbl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zcxahirfpi9kynnp4jk3510yzxbva4kw7xri8lxam5jl3d3ra67")))

(define-public crate-tbl-0.1.1 (c (n "tbl") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1blaj9a95r493ykpqqkbk35hhxna6nxz901va3pza7y7idyb8hmy")))

(define-public crate-tbl-1.0.0-alpha (c (n "tbl") (v "1.0.0-alpha") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bm31wbdpg92h56kf6cpv6x1dj0xnhc2dyqppcgmxsjncjg6ni27")))

(define-public crate-tbl-1.1.0-alpha (c (n "tbl") (v "1.1.0-alpha") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qn4pv6qv41mynci1izbshxqhn4pqs3xl6r9h7rnxxlahvbq9rr7")))

(define-public crate-tbl-1.1.0-alpha.1 (c (n "tbl") (v "1.1.0-alpha.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b6v6x0x8q049kwq6c4nrivh34na6qvw8sl0didiijmnnsi534s8")))

(define-public crate-tbl-1.1.0 (c (n "tbl") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 2)) (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "termion") (r "^1.5.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yh4h2bkzmh3p3ma27afjlnymc8174kxr89qfjpi6c7xjrkwihq1")))

