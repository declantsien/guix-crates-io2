(define-module (crates-io #{3}# t tbp) #:use-module (crates-io))

(define-public crate-tbp-1.0.0 (c (n "tbp") (v "1.0.0") (d (list (d (n "enum-map") (r "^1.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3.2") (d #t) (k 0)))) (h "029mvrd1cmglf25q6kfam67hl6g95xvxilngsrafv13pl73xqabi") (f (quote (("randomizer") ("move_info"))))))

(define-public crate-tbp-2.0.0 (c (n "tbp") (v "2.0.0") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "enum-map") (r "^1.1.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)))) (h "1pj7n198dy4q6rvk8nnc78mph393iq47c7zj8d0hhk79mz7cbk0l") (f (quote (("randomizer") ("move_info"))))))

(define-public crate-tbp-3.0.0 (c (n "tbp") (v "3.0.0") (d (list (d (n "derive_more") (r "^0.99.16") (d #t) (k 0)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.3.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 0)))) (h "04nrc469i0fsvcsybdv4xdzbljj7aalpy7wp1794pnwwk84skbji")))

