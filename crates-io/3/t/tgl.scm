(define-module (crates-io #{3}# t tgl) #:use-module (crates-io))

(define-public crate-tgl-0.1.0 (c (n "tgl") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "minifb") (r "^0.20") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0claarj48h7kfbgqi93vxzvahap9f5xsxyqmqjphk1f1r69griqw") (f (quote (("text") ("default" "text"))))))

(define-public crate-tgl-0.1.1 (c (n "tgl") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "minifb") (r "^0.20") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.1") (d #t) (k 2)) (d (n "winit") (r "^0.26") (d #t) (k 2)))) (h "1i6vkrppgd4kpnrl5lc8i8q2yf74i6v7pn30dgwjzqqix1f23n8q") (f (quote (("text") ("default" "text"))))))

