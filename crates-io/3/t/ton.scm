(define-module (crates-io #{3}# t ton) #:use-module (crates-io))

(define-public crate-ton-0.0.0 (c (n "ton") (v "0.0.0") (h "0gvi0sfq8wzyhdlk53yirm79a4bw4ya318lmacj5ymxqx3lfmaba")))

(define-public crate-ton-0.1.3 (c (n "ton") (v "0.1.3") (h "009niy71zzvqr6f2v8v3vj8a6qb690pjh7c6qbc5slz7x3sqgf1b")))

