(define-module (crates-io #{3}# t tgd) #:use-module (crates-io))

(define-public crate-tgd-0.1.0 (c (n "tgd") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cli-table") (r "^0.4.7") (d #t) (k 0)) (d (n "csv") (r "^1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "select") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "095nv57wc7bkp43x8v4wycvqmcvaz6mpqxvd90ikx1ny752d4jld")))

