(define-module (crates-io #{3}# t tog) #:use-module (crates-io))

(define-public crate-tog-0.0.0 (c (n "tog") (v "0.0.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "1xydxhm188vhxinywnhki05dy9dd8giaiisasghlz3laxpj1z8xq") (y #t)))

(define-public crate-tog-0.1.0 (c (n "tog") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "1hlbc45wacmfvnh1k025hzfbnqm1h52lv6q93c64z467b201lhjm") (y #t)))

(define-public crate-tog-0.1.1 (c (n "tog") (v "0.1.1") (d (list (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "dirs") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)))) (h "026151xdgylnmai8scclvr5dzv2b7annsgwlg30xdw3a5w4lpzj0") (y #t)))

