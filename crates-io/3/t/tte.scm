(define-module (crates-io #{3}# t tte) #:use-module (crates-io))

(define-public crate-tte-0.1.0 (c (n "tte") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.4.1") (d #t) (k 0)))) (h "066nxq1ccmr3na40hpw0jmxz65n88wx86qs7633gp8gdrnz7408f")))

(define-public crate-tte-0.1.1 (c (n "tte") (v "0.1.1") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.4.1") (d #t) (k 0)))) (h "1y146v3g3wr2pppsvf4p5bp3pa2bqmqngwrvmgnfgzagrgk3aa30")))

(define-public crate-tte-0.1.2 (c (n "tte") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "rust_decimal") (r "^1.4.1") (d #t) (k 0)))) (h "0hkjxg01la5g2i1nvx88ysia568rg33bi0n3w8pyrrxyrhzwvpjg")))

