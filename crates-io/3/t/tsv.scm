(define-module (crates-io #{3}# t tsv) #:use-module (crates-io))

(define-public crate-tsv-0.1.0 (c (n "tsv") (v "0.1.0") (d (list (d (n "reflection") (r "^0.1") (d #t) (k 0)) (d (n "reflection_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "trees") (r "^0.1") (d #t) (k 0)))) (h "04al12kz3406lxyw5bhgzc1bwfahff73w50bdha6hm0qdv8j3gm2")))

(define-public crate-tsv-0.1.1 (c (n "tsv") (v "0.1.1") (d (list (d (n "reflection") (r "^0.1.3") (d #t) (k 0)) (d (n "reflection_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "trees") (r "^0.2") (d #t) (k 0)))) (h "1am06s2cn1fjys7xva8f6bfqvl8h1hf0bzwh8qlg5k8c1l4h3021")))

