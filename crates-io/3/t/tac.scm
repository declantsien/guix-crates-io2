(define-module (crates-io #{3}# t tac) #:use-module (crates-io))

(define-public crate-tac-0.1.0 (c (n "tac") (v "0.1.0") (d (list (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0ar6wni4lpzq8h1a3jbrh3nngcsvfm7vhb8941l35xbyrn770yqc")))

(define-public crate-tac-0.1.1 (c (n "tac") (v "0.1.1") (d (list (d (n "ctrlc") (r "^3.0") (d #t) (k 0)) (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nalj8mxw82zhn9s1qk610h8vhm6niy4k17y3qaqhvgdcpdaswkq")))

(define-public crate-tac-0.2.0 (c (n "tac") (v "0.2.0") (d (list (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "simple-signal") (r "^1.1") (d #t) (k 0)) (d (n "try_print") (r "^0.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "175na6cyvrrhjmd3980nhcd0y99wbdhn9bqljd05mmwqypnqnsnp")))

(define-public crate-tac-0.2.1 (c (n "tac") (v "0.2.1") (d (list (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "try_print") (r "^0.0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1bjl1qabixl7m2k7z6ivdrwmzahc0bzg3cq5f5kfdqgd8c0qf25q")))

(define-public crate-tac-0.2.2 (c (n "tac") (v "0.2.2") (d (list (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1dzz4x3rx660zwbbs0pj37b7p0n42p8lrfmia429z897a8lkcj7y")))

(define-public crate-tac-0.2.3 (c (n "tac") (v "0.2.3") (d (list (d (n "memmap") (r "^0.5") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1w3k35bj9mi3anmzzbm613hf4c2x28xwla1dimn5f58azvjqm6cc")))

(define-public crate-tac-2.0.0 (c (n "tac") (v "2.0.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "0crq3hpxk2zz9cp742a33jv8j6ahdcv494rmi93iyd3rp04fcbby")))

(define-public crate-tac-2.1.0 (c (n "tac") (v "2.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)))) (h "08ps34kck43x43hfyrxmc9y3ahs3fcyvj56acsc3iwcww1wncyxc") (f (quote (("nightly"))))))

