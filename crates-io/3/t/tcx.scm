(define-module (crates-io #{3}# t tcx) #:use-module (crates-io))

(define-public crate-tcx-0.9.0 (c (n "tcx") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0qjhrassfjjkllwfk90v7z23qbihcjf2fv7fx31h7z5ayf77cv8v")))

(define-public crate-tcx-0.9.1 (c (n "tcx") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0yz5h2mdpv7rjna6l3j1xpxkkp0hw8aq9aw2b8zx2v0gzm4i3a8b")))

(define-public crate-tcx-0.9.2 (c (n "tcx") (v "0.9.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1gz6dvklzj7xswykr6irwkrap9z0qnjbj232qh8dnhk7k287jx72")))

(define-public crate-tcx-0.9.3 (c (n "tcx") (v "0.9.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "0ss1jdj826cjh3ryxyylilxhd1gwqv2vbw5x71sxhk8g2p6yx2g3")))

