(define-module (crates-io #{3}# t tdo) #:use-module (crates-io))

(define-public crate-tdo-1.0.0 (c (n "tdo") (v "1.0.0") (d (list (d (n "clap") (r "^2.23.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "tdo-core") (r "^0.1.1") (d #t) (k 0)) (d (n "tdo-export") (r "^0.1.2") (d #t) (k 0)))) (h "0px8waz19pg4rylg0m2y8xxhk3fvbm1qr1h95d1sxjrp37vp6rwv")))

(define-public crate-tdo-2.0.0 (c (n "tdo") (v "2.0.0") (d (list (d (n "clap") (r "^2.23.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "tdo-core") (r "^0.2.0") (d #t) (k 0)) (d (n "tdo-export") (r "^0.2.0") (d #t) (k 0)))) (h "02f9w8g2zggpyxlfdi3ilxr4qhhffpgzgrjzbm54p0z1zv0a23gs")))

(define-public crate-tdo-2.2.0 (c (n "tdo") (v "2.2.0") (d (list (d (n "clap") (r "^2.23.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "tdo-core") (r "^0.2") (d #t) (k 0)) (d (n "tdo-export") (r "^0.2") (d #t) (k 0)))) (h "09bg3qw2d6i42f4ylf6lw62qhbi26yl2vyg2xmy6fkhw0sld2bmv")))

