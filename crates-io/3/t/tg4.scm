(define-module (crates-io #{3}# t tg4) #:use-module (crates-io))

(define-public crate-tg4-0.5.3 (c (n "tg4") (v "0.5.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.5.3") (d #t) (k 0)))) (h "1kw1l5fn2lj28x99x94ywpv8qfnk3xc0q66lzxv519a0r0x5jp9z")))

(define-public crate-tg4-0.5.3-2 (c (n "tg4") (v "0.5.3-2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.5.3-2") (d #t) (k 0)))) (h "1fyrs0y7xl6p045hismq3j48v2ndlkb5zhr24kmv8dzxgzyf33bn")))

(define-public crate-tg4-0.5.4 (c (n "tg4") (v "0.5.4") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.5.4") (d #t) (k 0)))) (h "09vdg3hyfqzwbagv90inpj26lfmkfglnxsmgrggjg4g97q83v5wf")))

(define-public crate-tg4-0.5.5 (c (n "tg4") (v "0.5.5") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.5.5") (d #t) (k 0)))) (h "05slqdj4v75qh7sxqjl0vmx19qckvv4vih49385c69cid881aqlk")))

(define-public crate-tg4-0.6.0-alpha1 (c (n "tg4") (v "0.6.0-alpha1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.6.0-alpha1") (d #t) (k 0)))) (h "1rja1kl051lp757qxvmvkzrvwwx0fs69jry36fy7iadpk6jq15nh")))

(define-public crate-tg4-0.6.0-beta1 (c (n "tg4") (v "0.6.0-beta1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.6.0-beta1") (d #t) (k 0)))) (h "0c0d03ss66vz41rbdf8r654zqrawwighhkazjq0vma131cbxdjbi")))

(define-public crate-tg4-0.6.0-rc1 (c (n "tg4") (v "0.6.0-rc1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.6.0-rc1") (d #t) (k 0)))) (h "0r19jfwx5mrhkbr3pa6v9xly8pw2kysci71pljkwb8jy77q16p9j")))

(define-public crate-tg4-0.6.0-rc2 (c (n "tg4") (v "0.6.0-rc2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.6.0-rc2") (d #t) (k 0)))) (h "1aksfm0mgdqidlygr3zyh54dmk5gawijs8x2xbjafbydag96lrm9")))

(define-public crate-tg4-0.6.0 (c (n "tg4") (v "0.6.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.6.0") (d #t) (k 0)))) (h "18n20g0bwn9rrwqxq7nbi2i19cx2q3rd0r9is03qii6m9nlhnwh5")))

(define-public crate-tg4-0.6.1 (c (n "tg4") (v "0.6.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.6.1") (d #t) (k 0)))) (h "1hm413qm04ql973851gg5n1hr7fyllvb1b50iyhb0mzfirq1qv0m")))

(define-public crate-tg4-0.6.2 (c (n "tg4") (v "0.6.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.6.2") (d #t) (k 0)))) (h "0h8k5f5djhwww62nkq8zdavgvydhfpq50lzr3ri9v2l0rllp9xiw")))

(define-public crate-tg4-0.7.0-alpha1 (c (n "tg4") (v "0.7.0-alpha1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.7.0-alpha1") (d #t) (k 0)))) (h "1k55j6yxfxc1qf8p3zky9bjp623ks6hk7b3m3bz9y12qvng6ijcg")))

(define-public crate-tg4-0.7.0-alpha2 (c (n "tg4") (v "0.7.0-alpha2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.7.0-alpha2") (d #t) (k 0)))) (h "02zjgkc7yczqnc3g67r8a6b6chmb57p4jc0nznrs1h3sriadmj92")))

(define-public crate-tg4-0.8.0 (c (n "tg4") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.8.0") (d #t) (k 0)))) (h "1hp9hq00c3d828mjfz2ijniq1ja683dj61n1d5c21m35h5pi9by0")))

(define-public crate-tg4-0.8.1 (c (n "tg4") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.8.1") (d #t) (k 0)))) (h "04imvpkw6xriv4syimf4fsr81ik6763k83xcra5blf9mqxx0g0q8")))

(define-public crate-tg4-0.9.0 (c (n "tg4") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta8") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.9.0") (d #t) (k 0)))) (h "0bsr1bmdi8jy7skcv0pmqfy8hi5816plkyx2mwfq0yr1zfl30znm")))

(define-public crate-tg4-0.10.0 (c (n "tg4") (v "0.10.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.10.0") (d #t) (k 0)))) (h "1135145aipjr517ldjlw49hn0jk8686anilmf0lj7d3qx7f957il")))

(define-public crate-tg4-0.11.0 (c (n "tg4") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.11.0") (d #t) (k 0)))) (h "0am46s3pc04zydii8fms81fii83sx2qdsqkmbv50gg2lcn69q4qi")))

(define-public crate-tg4-0.12.0 (c (n "tg4") (v "0.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.12.0") (d #t) (k 0)))) (h "1z8xx3j6a52z1ay5c28pshx4lzaw7nanblbkhsl8pns9vk05vfzv")))

(define-public crate-tg4-0.13.0 (c (n "tg4") (v "0.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.13.0") (d #t) (k 0)))) (h "017hf4z17k6ij28bc2h9j09sw1p6cj9jkrgggrchiqjp2hhafz46")))

(define-public crate-tg4-0.14.0 (c (n "tg4") (v "0.14.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.14.0") (d #t) (k 0)))) (h "1gm5qy03kk5r74q70w28kkv9nzxhv1r2hwklqyqsnq8chji55kqr")))

(define-public crate-tg4-0.15.0 (c (n "tg4") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.15.0") (d #t) (k 0)))) (h "1lz05jld93xn69pj08ry5kd8l7crk5jgyjl5m9b3mafbm2g69n3v")))

(define-public crate-tg4-0.15.1 (c (n "tg4") (v "0.15.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.15.1") (d #t) (k 0)))) (h "1aaxbdrysfki3b2gzl8mlb2w411z6nizfjr5bhc1dvip8yq825j1")))

(define-public crate-tg4-0.16.0 (c (n "tg4") (v "0.16.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.16.0") (d #t) (k 0)))) (h "038imfj6z2xjjjkbxzmmnwjkhl9c37av6v1vymavqj0qmdn5mvjc")))

(define-public crate-tg4-0.17.0 (c (n "tg4") (v "0.17.0") (d (list (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.17.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 2)))) (h "11bdidb41m7iv4z7483az6wx5dn19sdx4g10rif05wx4ag11389s")))

(define-public crate-tg4-0.17.1 (c (n "tg4") (v "0.17.1") (d (list (d (n "cosmwasm-std") (r "^1.1.9") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "tg-bindings") (r "^0.17.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.1.9") (d #t) (k 2)))) (h "11apv8akzbrxhl1986q6riw1y75901kg4kb5szing4wbzs7k0mn1")))

