(define-module (crates-io #{3}# t tkm) #:use-module (crates-io))

(define-public crate-tkm-0.8.5 (c (n "tkm") (v "0.8.5") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)) (d (n "pretty-bytes") (r "^0.2.2") (d #t) (k 0)) (d (n "sysinfo") (r "0.8.*") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "tui") (r "^0.4.0") (d #t) (k 0)))) (h "1g25qwkhf5pfpkaakwm2wilybh8nkl6lk4fgcq0a2ddxy7y12rpz")))

