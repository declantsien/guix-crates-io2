(define-module (crates-io #{3}# t tmc) #:use-module (crates-io))

(define-public crate-tmc-0.1.0 (c (n "tmc") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rusb") (r "^0.8.1") (d #t) (k 0)))) (h "0bxmm4l9x104vwqyvh1n1xjdf5m99k4xqwgrs2y1ig5zazfm53s0")))

(define-public crate-tmc-0.1.1 (c (n "tmc") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "rusb") (r "^0.8.1") (d #t) (k 0)))) (h "1mdf994gsagn8jv58kyf8gbqvw83pqnvfijjmynx0nwppdrmny6q")))

