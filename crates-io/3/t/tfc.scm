(define-module (crates-io #{3}# t tfc) #:use-module (crates-io))

(define-public crate-tfc-0.1.0 (c (n "tfc") (v "0.1.0") (h "0yzbbalsvwz9n65rzckwksjsha7l70v00q4fs8bqwc4pixzmf4wm")))

(define-public crate-tfc-0.1.1 (c (n "tfc") (v "0.1.1") (h "1s28cip3y62svbw60mx9lybrr6x73r06r2afajsflqmm3zl4gq8w")))

(define-public crate-tfc-0.2.0 (c (n "tfc") (v "0.2.0") (h "0knfdy8hw8icxjl7yb9xsng7hx6nzqvzwp8x9xmh3iidn0aphslb")))

(define-public crate-tfc-0.2.1 (c (n "tfc") (v "0.2.1") (h "1ym0i9wqgcd8zi1pqk0vydx9lbz3p6sc6l61akxwvdw7figgvvgc")))

(define-public crate-tfc-0.3.0 (c (n "tfc") (v "0.3.0") (h "1mf8r0nd890225q9p7fp3xgg4ypzzqpc2ig2zrykq8rifd6d9xyk")))

(define-public crate-tfc-0.4.0 (c (n "tfc") (v "0.4.0") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "044rk8kn9mrqnzfp9m1knd4zip16yx3xybm44ry70dvq4aawsl2a")))

(define-public crate-tfc-0.5.0 (c (n "tfc") (v "0.5.0") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1c516yblpmnl3j0gdq7wz8jpgxqwn6xy9smbb9m053rpqwrfx0nm") (f (quote (("ascii-fallback"))))))

(define-public crate-tfc-0.5.1 (c (n "tfc") (v "0.5.1") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1vdzdd6b99rj4lj17raqxqbphfd4nm437ipp623rmdddkm938h15") (f (quote (("ascii-fallback"))))))

(define-public crate-tfc-0.5.2 (c (n "tfc") (v "0.5.2") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "unicode-segmentation") (r "^1.7") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1lyqbk7mm1gavc38q230dgvdj6bh7m24iqpnkzbf52b9hkx5fr0w") (f (quote (("ascii-fallback"))))))

(define-public crate-tfc-0.6.0 (c (n "tfc") (v "0.6.0") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "17ni2wc53xwqi0g33z4hrk1njqsalchwf9rr33py5q3xz9gz7dql") (f (quote (("ascii-fallback"))))))

(define-public crate-tfc-0.6.1 (c (n "tfc") (v "0.6.1") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "03p9a3jzzn1dgprfm5gz0ipm1fs043ajmvwh7s7751wvrrphriad") (f (quote (("ascii-fallback"))))))

(define-public crate-tfc-0.6.2 (c (n "tfc") (v "0.6.2") (d (list (d (n "core-graphics") (r "^0.22") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1jxf6p0wwvgy2bl0z5afvq8i4qs3qpn4p4w0h44vb2lpnml9kyal") (f (quote (("ascii-fallback"))))))

(define-public crate-tfc-0.7.0 (c (n "tfc") (v "0.7.0") (d (list (d (n "core-graphics") (r "^0.23") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (t "cfg(target_os = \"macos\")") (k 0)))) (h "1gr1frs9y4mvlbmh8k3as8dsx5fgfibsr7558f454iy54xl5zihb") (f (quote (("x11") ("default" "check-x11") ("check-x11") ("ascii-fallback"))))))

