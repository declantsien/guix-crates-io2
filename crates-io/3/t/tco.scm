(define-module (crates-io #{3}# t tco) #:use-module (crates-io))

(define-public crate-tco-0.0.1 (c (n "tco") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "105ypq2fi1yv2b1yhzz5fq2ipgxc4sf3bdl0xaaksqfkmi1b3mhf")))

(define-public crate-tco-0.0.2 (c (n "tco") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "10ai1fb0d86pxp3igrl686vcqyizx1lj1mmza9a6zw4w60fzgwhx")))

