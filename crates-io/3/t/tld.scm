(define-module (crates-io #{3}# t tld) #:use-module (crates-io))

(define-public crate-tld-0.1.0 (c (n "tld") (v "0.1.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "1yazb2z66ap2ai6ig4gb39l8jw34jq5bq8p833nzlr8v6lsqna3g")))

(define-public crate-tld-0.2.0 (c (n "tld") (v "0.2.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "1x8ag0j2pgnalf91q33zrbwvi1c1vqj7328v7x2wvdcr6vbcnaym")))

(define-public crate-tld-0.4.0 (c (n "tld") (v "0.4.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "1b673zhdf48cprp7m6swblsb11jdqsjm45xjylx5p0brsp70lwvr")))

(define-public crate-tld-0.5.0 (c (n "tld") (v "0.5.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "0wcipzmv3caw5yf5ixy5dd4dh66zgx5k486gg4hil5r49hj34s65")))

(define-public crate-tld-0.6.0 (c (n "tld") (v "0.6.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "1jm2jy798di2idnqc1sfnj0kmwyrn1xwgv8l8l53w8jbjvxjh4bq")))

(define-public crate-tld-1.0.0 (c (n "tld") (v "1.0.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "0n5q34kf7x4yzi1hr3pax24qg32h30s2pqmnwg50cdybx1rzjbiy")))

(define-public crate-tld-1.1.0 (c (n "tld") (v "1.1.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "1i4z46i4qb1y7z1d3j4nzwf6yf8zhxddp16h1k47n8iiham0kgl7")))

(define-public crate-tld-1.2.0 (c (n "tld") (v "1.2.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "0vf03hzfzc6qaqkrplhlrnk397ld2q14f0mn5hbhpk7qsw29jq3c")))

(define-public crate-tld-1.3.0 (c (n "tld") (v "1.3.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "17vbjx6mgnhyrsf3b23wgnga80alr2zfk1vb3dvflysw0ifz7ld0")))

(define-public crate-tld-1.4.0 (c (n "tld") (v "1.4.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "0896m2cv5a44zwffzj7sn10dv2cf2g4hgf6r0c4zys4j63krxkr7")))

(define-public crate-tld-1.5.0 (c (n "tld") (v "1.5.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "1zs5bszy7r7y3535pzg6mwn6858c5fy4mllva0cajkrzz5dmpiv8")))

(define-public crate-tld-1.6.0 (c (n "tld") (v "1.6.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "0y3hjz74l4dfqcrgvg0c5g64y9kqkviswfah5n40n915gjm685k6")))

(define-public crate-tld-1.7.0 (c (n "tld") (v "1.7.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "14sarbyma6jzwr8lx7jzs2217j7icnjil3v596mp643grjaf6hdw")))

(define-public crate-tld-1.8.0 (c (n "tld") (v "1.8.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "0mq1g8phk8kv8ih2qx9yvjj0ix4gi1ghrq9ljdjayf01zns3grwf")))

(define-public crate-tld-1.9.0 (c (n "tld") (v "1.9.0") (d (list (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)))) (h "1q9gq2jgx7ac8xvkv8s839l85fx33543fqh1z9jmzyb1bw7dg3j6")))

(define-public crate-tld-1.10.0 (c (n "tld") (v "1.10.0") (d (list (d (n "phf") (r "^0.7.22") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.22") (d #t) (k 0)))) (h "0skhb9h60h4b62mdywachmnrsrxyq98nfbz4iphk9cxzbimp5avk")))

(define-public crate-tld-1.11.0 (c (n "tld") (v "1.11.0") (d (list (d (n "phf") (r "^0.7.22") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.22") (d #t) (k 0)))) (h "0afwh3ggk8cmnki5hvj676r9m1ngvscyijinnr14wfr5kfvv6nqp")))

(define-public crate-tld-1.12.0 (c (n "tld") (v "1.12.0") (d (list (d (n "phf") (r "^0.7.22") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.22") (d #t) (k 0)))) (h "0kp3m50830iafkllm85whrqay3klk944rgpabs16m3a1scz373if")))

(define-public crate-tld-1.14.0 (c (n "tld") (v "1.14.0") (d (list (d (n "phf") (r "^0.7.22") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.22") (d #t) (k 0)))) (h "1kmnfbcvh11z6lwxks8ahy2j0mnbyyj3xv3i8j5xw27big688j2m")))

(define-public crate-tld-1.15.0 (c (n "tld") (v "1.15.0") (d (list (d (n "phf") (r "^0.7.22") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.22") (d #t) (k 0)))) (h "176flqg8mabdycsrd464zfisixik3vp4ag4fan94kas3xsf8fw9f")))

(define-public crate-tld-1.16.0 (c (n "tld") (v "1.16.0") (d (list (d (n "phf") (r "^0.7.22") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.22") (d #t) (k 0)))) (h "1d07gjydjqdmbmb7a6k4b56k0lzk1hly3n32ciq68iawrj8c7g1x")))

(define-public crate-tld-1.17.0 (c (n "tld") (v "1.17.0") (d (list (d (n "phf") (r "^0.7.22") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.22") (d #t) (k 0)))) (h "0i2ahjsch6n59j3si2a69ayn6wm35bq7vjzqid6jd2q63316sp61")))

(define-public crate-tld-2.0.0 (c (n "tld") (v "2.0.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "0s8ikp46snim755lwb56dxpw13sqm4frd9fjva9zxdxx9r2l2jyy")))

(define-public crate-tld-2.1.0 (c (n "tld") (v "2.1.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "0cf9sqy634sm6phwa9yx545dv0r3ar97jsgnrkb014qnv5fckdsg")))

(define-public crate-tld-2.2.0 (c (n "tld") (v "2.2.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "15i2ka29s996z3gpw1dgcf84pdjwx1vh2awchi3q2dfnqhyd3yij")))

(define-public crate-tld-2.3.0 (c (n "tld") (v "2.3.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "1pmmfsi8im45lqsgm2hnnpi2bm3jg67ichl27n1hd7pjfc3lgpv2")))

(define-public crate-tld-2.4.0 (c (n "tld") (v "2.4.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "132p3yhlh0s1clxpmx4999fwhfjlrwfba6kbyhk794dmwygv9vmv")))

(define-public crate-tld-2.5.0 (c (n "tld") (v "2.5.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "1wzbsd0z826hd286344yhngrpvydlyf3valqmyihmq4g6z9d438s")))

(define-public crate-tld-2.6.0 (c (n "tld") (v "2.6.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "0w7brkvnf1512kwgi57q94frcn3zahfgxjvaz17i5lcvfs5a0p0k")))

(define-public crate-tld-2.7.0 (c (n "tld") (v "2.7.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "1v13i90hniy74d8za15b81rgjjdg23kmpy5bqlfzc40dj38w4sls")))

(define-public crate-tld-2.8.0 (c (n "tld") (v "2.8.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "11s636w7pqlmw3byjxzz8ax89vwcp3bax2grpwl7y04zam7niksj")))

(define-public crate-tld-2.9.0 (c (n "tld") (v "2.9.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "1b2w0msxarqdv145n3idgvm0smr9wskjbisjfadzl3xr5yjq4gml")))

(define-public crate-tld-2.10.0 (c (n "tld") (v "2.10.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "1cl82s0pr5ckly803b5937dk2cca5mhm7x0pb8gldccvk9rynjzz")))

(define-public crate-tld-2.11.0 (c (n "tld") (v "2.11.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "0mg9hrvwpqck242cvglsh0cbb2ah6agzznj7p4gwfahjsaxgr2jv")))

(define-public crate-tld-2.12.0 (c (n "tld") (v "2.12.0") (d (list (d (n "phf") (r "^0.7.24") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.7.24") (d #t) (k 1)))) (h "0snsnc68slvzpq45ib0brjgh246iqrbd1ivhqxh5chzp2d1xppnw")))

(define-public crate-tld-2.13.1 (c (n "tld") (v "2.13.1") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "06kfq8z6ws69wxhxxzzka5gy83y9m92vkjxbj25w6jhqbsgq52a6")))

(define-public crate-tld-2.14.0 (c (n "tld") (v "2.14.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "1xms4ngzsxs60kg6ax0qhnlkz9r0fszhll15vqz1x7rqjbi4czr8")))

(define-public crate-tld-2.15.0 (c (n "tld") (v "2.15.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "1x41jdqrr2bamv3a3vys0qsdzvhh1c1a3i60xhna79ahrsqg2k1h")))

(define-public crate-tld-2.16.0 (c (n "tld") (v "2.16.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "184rpwq2sfyani0x97a542r8j18rh1qv5kawd7wg8h616sr7w3dn")))

(define-public crate-tld-2.17.0 (c (n "tld") (v "2.17.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "0s3j1vkqx25w7ba91rbbdmvbmd2npxwixk0imyx3wc58fnpfrdbh")))

(define-public crate-tld-2.18.0 (c (n "tld") (v "2.18.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "0llfra44xadv1wp71va27mr12iiflw84xkfm6g2lrchwcydcvsb4")))

(define-public crate-tld-2.20.0 (c (n "tld") (v "2.20.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "0irfsi1fppcz3xa23gw2jgwr94xjfhrk995zpls1rlh1vpxw9scr")))

(define-public crate-tld-2.21.0 (c (n "tld") (v "2.21.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "1f2b8b0ma15s2h5g8kqrfhddcnr43127gmy5638zvrafhxgm5sd3")))

(define-public crate-tld-2.22.0 (c (n "tld") (v "2.22.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "11w06x54krvmbb2gsywwcpjfblbpxa7cmqc075iahlqsfgz9r583")))

(define-public crate-tld-2.23.0 (c (n "tld") (v "2.23.0") (d (list (d (n "phf") (r "^0.8") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.8") (d #t) (k 1)))) (h "07vvwsn5b81iqbvqyw5ma0laigqj7fqbp1vyxnffl01x4v3932hs")))

(define-public crate-tld-2.24.0 (c (n "tld") (v "2.24.0") (d (list (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)))) (h "1jnapxxfqa6pha2d72dc09ndfpaym4hwllmhdbg3a7lvp34162m7")))

(define-public crate-tld-2.25.0 (c (n "tld") (v "2.25.0") (d (list (d (n "phf") (r "^0.9") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.9") (d #t) (k 1)))) (h "0pxa88d9pszfnqyvaki6as6zqsp0xvfli6iva8npcgdzjf472ldl")))

(define-public crate-tld-2.26.0 (c (n "tld") (v "2.26.0") (d (list (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)))) (h "10nvqlr33q1jbhmfff1zi8i3xrc27cvanc0bx0i5fw20z5cijhy5")))

(define-public crate-tld-2.27.0 (c (n "tld") (v "2.27.0") (d (list (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)))) (h "0a9k94qnrgp8r7s475nb004k0x62gb232sqgwb2a78hk3zyvv3rp")))

(define-public crate-tld-2.28.0 (c (n "tld") (v "2.28.0") (d (list (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)))) (h "1avkw7gvzr7sczpnyqpbh70kzlhnjm39kpqznrnrznhl0z70rdqh")))

(define-public crate-tld-2.29.0 (c (n "tld") (v "2.29.0") (d (list (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)))) (h "0q08j580ldscyhmcyx3m9sxl92w75q75y1xyybh7qjlxhdh0xxhh")))

(define-public crate-tld-2.30.0 (c (n "tld") (v "2.30.0") (d (list (d (n "phf") (r "^0.10") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10") (d #t) (k 1)))) (h "1bzi2hljfihprvk87ljlqj68qj4ciwsmxa6n05as0l7s4c9s83hx")))

(define-public crate-tld-2.31.0 (c (n "tld") (v "2.31.0") (d (list (d (n "phf") (r "^0.11.2") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11.2") (d #t) (k 1)))) (h "14n8m5xcf0f1pw7gc9l5vi4j9krp18sbw54gp9svki8l5mlhqaqf")))

(define-public crate-tld-2.32.0 (c (n "tld") (v "2.32.0") (d (list (d (n "phf") (r "^0.11.2") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11.2") (d #t) (k 1)))) (h "1swdzwzwalb701f2l62blwlrcfmr6qk60b79s2b8gb764bbaymmd")))

(define-public crate-tld-2.33.0 (c (n "tld") (v "2.33.0") (d (list (d (n "phf") (r "^0.11.2") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11.2") (d #t) (k 1)))) (h "0q3xx4adkhiz2minivi982q5gnwlf9a7hs15mjcspdb57w938bwg")))

(define-public crate-tld-2.34.0 (c (n "tld") (v "2.34.0") (d (list (d (n "phf") (r "^0.11.2") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11.2") (d #t) (k 1)))) (h "1jx250qnshz733j657wshb01sw8ld51j6yhcabzqwkk9zmhnnlrd")))

(define-public crate-tld-2.35.0 (c (n "tld") (v "2.35.0") (d (list (d (n "phf") (r "^0.11.2") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.11.2") (d #t) (k 1)))) (h "1xvc05a44qz350sv0azgx4hmabf27x2cy0n5f0jzbd7w831mzjkk")))

