(define-module (crates-io #{3}# t tty) #:use-module (crates-io))

(define-public crate-tty-0.2.0 (c (n "tty") (v "0.2.0") (d (list (d (n "fd") (r "*") (d #t) (k 0)) (d (n "termios") (r "*") (d #t) (k 0)))) (h "0xab5l0zki61c67y4q0qkxsyvf45iyi158lq9qm5fgsyjga417h1")))

(define-public crate-tty-0.3.0 (c (n "tty") (v "0.3.0") (d (list (d (n "fd") (r "*") (d #t) (k 0)) (d (n "termios") (r "*") (d #t) (k 0)))) (h "0inmxk7vrmzjjjhnbx2wmp2iasqsqpx2xmf1j5khgiixvy1qzhs3")))

(define-public crate-tty-0.4.0 (c (n "tty") (v "0.4.0") (d (list (d (n "fd") (r "*") (d #t) (k 0)) (d (n "termios") (r "*") (d #t) (k 0)))) (h "07rlgdzblwhbp8a7kmngsjkw3kxsswk3m8v65n7kq0kclb6yww64")))

(define-public crate-tty-0.4.2 (c (n "tty") (v "0.4.2") (d (list (d (n "fd") (r "*") (d #t) (k 0)) (d (n "termios") (r "*") (d #t) (k 0)))) (h "0zy6lzkzy52vdqdpmy2bhnfyvbfv5ww07vnp2qg2nz82jg7nvmq2")))

(define-public crate-tty-0.4.3 (c (n "tty") (v "0.4.3") (d (list (d (n "fd") (r "0.2.*") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "termios") (r "0.2.*") (d #t) (k 0)))) (h "0rwgzw0cj31cvhfpzyg8chmgpcfhd3rzgn4firdh1w5l8h0i9m99")))

(define-public crate-tty-0.4.4 (c (n "tty") (v "0.4.4") (d (list (d (n "fd") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "termios") (r "0.2.*") (d #t) (k 0)))) (h "02nsfjcrkq70pcaja8k07f89jgwqcg1ba0qjzld2h44asaqml0pn")))

(define-public crate-tty-0.4.5 (c (n "tty") (v "0.4.5") (d (list (d (n "fd") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "termios") (r "0.2.*") (d #t) (k 0)))) (h "1rnfgms0hjxsn6yljyv2j3m1pw83kdw1ydpak9rffrvclkmmkdrv")))

(define-public crate-tty-0.5.0 (c (n "tty") (v "0.5.0") (d (list (d (n "chan") (r "^0.1") (d #t) (k 0)) (d (n "chan-signal") (r "^0.1.7") (d #t) (k 0)) (d (n "fd") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "termios") (r "0.2.*") (d #t) (k 0)))) (h "0mpk1m6wk5b07fjscihjps0m831qnvvaq3yp77zc787qzxh216wq")))

