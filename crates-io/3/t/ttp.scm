(define-module (crates-io #{3}# t ttp) #:use-module (crates-io))

(define-public crate-ttp-0.1.0 (c (n "ttp") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "0sfrv7zkpfqlvq8z1fxmjgbpicnqscjdhj74l7whcp48dn7379jh")))

(define-public crate-ttp-0.1.1 (c (n "ttp") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "04rgxvngqr486c3gryq3aczsypiwi6zf7hjs61589jpmy9k1g454")))

(define-public crate-ttp-0.1.2 (c (n "ttp") (v "0.1.2") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1d5wjr2hpd23dagdaik0q74pfh1m5g7mwmhwhlrdjvnfa8qhr80i")))

