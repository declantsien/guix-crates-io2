(define-module (crates-io #{3}# t tsz) #:use-module (crates-io))

(define-public crate-tsz-0.1.0 (c (n "tsz") (v "0.1.0") (h "1mnd5zk4zmnaws87gm6wv3z46b1y0izn7a7kcqb94wrxha1jmbxr") (f (quote (("nightly"))))))

(define-public crate-tsz-0.1.1 (c (n "tsz") (v "0.1.1") (h "10c7v71mk1l46qssf03prbxqvwv6lc4l23gi3y12wipywx299f55") (f (quote (("nightly"))))))

(define-public crate-tsz-0.1.2 (c (n "tsz") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hh52a0l5kcnximz41gwrbyg30qajws5ajzp01dpwq51di1vp3ga") (f (quote (("nightly"))))))

(define-public crate-tsz-0.1.3 (c (n "tsz") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0w7l2sk49gg1yyqczd71dk0kwihji7vxgscngszjhsvln03hvlph") (f (quote (("nightly"))))))

(define-public crate-tsz-0.1.4 (c (n "tsz") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "0drdr1ygl6a6rvm31jknspfdl59vdkdm1510jp9bz2fngr8j95kq") (f (quote (("nightly"))))))

