(define-module (crates-io #{3}# t tau) #:use-module (crates-io))

(define-public crate-tau-0.0.1 (c (n "tau") (v "0.0.1") (h "0kzks0i317xr168slgwdg05xz9g7yk70hy75sfnz7836anijppcm")))

(define-public crate-tau-0.0.2 (c (n "tau") (v "0.0.2") (h "0skr0x69mwma9nazc816q04iji2rwq1nzfbkrpf8b0iwskbmh9y8")))

(define-public crate-tau-1.0.0 (c (n "tau") (v "1.0.0") (h "0i9kvik3akjhvf00140abjbpvggiia5xz69l0lnklkkfdknyamhy")))

(define-public crate-tau-1.0.1 (c (n "tau") (v "1.0.1") (h "0abrk9cmk8zyxp4258rn1p1qg1xpnxz9rf3bpi0y5xcgfkci5a6v")))

(define-public crate-tau-1.0.2 (c (n "tau") (v "1.0.2") (h "1mn6bg4z4q5ky4b4wm0mr2cgczrfmbi7y17hi4lgc1vqqbcn61hs")))

(define-public crate-tau-1.0.3 (c (n "tau") (v "1.0.3") (h "0pifgnlnpm5iynhj2is86abv2vlpz9580fn7slybxfvfajdh8vhd")))

(define-public crate-tau-1.0.4 (c (n "tau") (v "1.0.4") (h "0gf5acp1qjl9wvs3pgxd5an6z7r4l1g3232zzgz64plb6bpishqh")))

