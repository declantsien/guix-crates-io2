(define-module (crates-io #{3}# t tsf) #:use-module (crates-io))

(define-public crate-tsf-0.1.0 (c (n "tsf") (v "0.1.0") (d (list (d (n "tsf-sys") (r "^0.1.0") (d #t) (k 0)))) (h "12shpaqmdr7qirvi83xhz1rg1cyw8l3m0fizb2wxax3819ylsqgy")))

(define-public crate-tsf-0.1.1 (c (n "tsf") (v "0.1.1") (d (list (d (n "tsf-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1r40wjvdxifpsw7y1bknx45mxa7dwi5x1hqld8l1mwdpghsxhbyq")))

(define-public crate-tsf-0.2.0 (c (n "tsf") (v "0.2.0") (d (list (d (n "tsf-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1sz7dygr6k23ygcv9skxzklzmfp7drxgz16b048090q2bng119ma")))

