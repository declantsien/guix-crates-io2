(define-module (crates-io #{3}# t tsk) #:use-module (crates-io))

(define-public crate-tsk-0.0.0 (c (n "tsk") (v "0.0.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "tsk_lib") (r "^0.0.0") (d #t) (k 0)))) (h "1daxjg2xf1hf9ax8f1rk77p2jcawjklncc2xwahmkdiv29s5djs9")))

