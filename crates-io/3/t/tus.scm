(define-module (crates-io #{3}# t tus) #:use-module (crates-io))

(define-public crate-tus-0.1.0 (c (n "tus") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)))) (h "0pf16nk0w9dpnfsis5g05pmpjzqj7v3lbwcz2vlfv854g33w6awy")))

(define-public crate-tus-0.2.0 (c (n "tus") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)))) (h "1fd60zap6zvdw4bw94lqnvx4ipmc9d74w3nx3njkk5xsrzwn6pgq")))

(define-public crate-tus-0.3.0 (c (n "tus") (v "0.3.0") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "13gybghxlmrswd7yf8bmn8bvpyzq8w94zlf8542r97m2nccpddxf")))

(define-public crate-tus-1.0.0 (c (n "tus") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)) (d (n "url") (r "^2.1.0") (d #t) (k 0)))) (h "05jfv1f2bszrww26sqlvl5igmdnwr8hs1fkl4r9nq4mcc0qp0sg6")))

(define-public crate-tus-2.0.0-rc1.0 (c (n "tus") (v "2.0.0-rc1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.8.1") (d #t) (k 2)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "17lyd0y7kg756n6akykkr8s5qp67bqzgbm7mbjx4kfz1a2s0p36y")))

