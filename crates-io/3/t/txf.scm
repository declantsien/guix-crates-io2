(define-module (crates-io #{3}# t txf) #:use-module (crates-io))

(define-public crate-txf-0.1.0 (c (n "txf") (v "0.1.0") (d (list (d (n "base64") (r "^0.20") (d #t) (k 2)) (d (n "bech32") (r "^0.9") (d #t) (k 0)) (d (n "cosmos-sdk-proto") (r "^0.19") (f (quote ("grpc"))) (d #t) (k 0)) (d (n "hex-literal") (r "^0.4") (d #t) (k 2)) (d (n "k256") (r "^0.13") (d #t) (k 0)) (d (n "ripemd") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tonic") (r "^0.9") (d #t) (k 0)))) (h "1vi49qgrh5dmmn50h2wnjzv1l7ianszny1s6r6ahi5w2zvklqvv7") (r "1.70.0")))

