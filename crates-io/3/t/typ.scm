(define-module (crates-io #{3}# t typ) #:use-module (crates-io))

(define-public crate-typ-0.1.0 (c (n "typ") (v "0.1.0") (d (list (d (n "by_address") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.34") (d #t) (k 2)) (d (n "typenum") (r "^1.12") (d #t) (k 2)))) (h "0q0ga50xk3ps0zq2dq4m4ldrpcfzas4ca1vmaajxr8zzi8yb6233")))

(define-public crate-typ-0.1.1 (c (n "typ") (v "0.1.1") (d (list (d (n "by_address") (r "^1.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "radix_trie") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "visit-mut"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.34") (d #t) (k 2)) (d (n "typenum") (r "^1.12") (d #t) (k 2)))) (h "1nf10w8r8diy1ghgim9r1vrmraa4c6gj9w9f0vikc5zxx0swwrck")))

