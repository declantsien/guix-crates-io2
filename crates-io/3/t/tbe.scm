(define-module (crates-io #{3}# t tbe) #:use-module (crates-io))

(define-public crate-tbe-0.1.0 (c (n "tbe") (v "0.1.0") (d (list (d (n "base2") (r "^0.2.2") (d #t) (k 0)) (d (n "bitrw") (r "^0.6.0") (d #t) (k 0)) (d (n "int") (r "^0.2.10") (d #t) (k 0)))) (h "192mqa31ac8k2yck1va6f9llncaa21mvpg0kn8niipkfb7yvpa3q")))

(define-public crate-tbe-0.2.0 (c (n "tbe") (v "0.2.0") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "bitrw") (r "^0.7.0") (d #t) (k 0)) (d (n "int") (r "^0.2.1") (d #t) (k 0)))) (h "0pbz0mzwri9w5d5frq6i7gn0s6ci1friy5afvfq4yg9vhyq1i91h")))

(define-public crate-tbe-0.2.1 (c (n "tbe") (v "0.2.1") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "bitrw") (r "^0.8.0") (d #t) (k 0)) (d (n "int") (r "^0.2.11") (d #t) (k 0)))) (h "0znfkdrzila218cya7s4ng5l2zjs83j2lxqhx1wim6gwadwl195r")))

(define-public crate-tbe-0.2.2 (c (n "tbe") (v "0.2.2") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "bitrw") (r "^0.8.0") (d #t) (k 0)) (d (n "int") (r "^0.2.11") (d #t) (k 0)))) (h "1avjv1576v4fik5acdyajpyg4w1124xj9kk01i0j385qdrv7f26m")))

(define-public crate-tbe-0.3.0 (c (n "tbe") (v "0.3.0") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "bitrw") (r "^0.8.0") (d #t) (k 0)) (d (n "int") (r "^0.2.11") (d #t) (k 0)))) (h "08lqp6m09zhi6cgcf9m6lydk5cnppjw89fx4m6n716wv4vmdcqbz")))

(define-public crate-tbe-0.3.1 (c (n "tbe") (v "0.3.1") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "bitrw") (r "^0.8.1") (d #t) (k 0)) (d (n "int") (r "^0.2.11") (d #t) (k 0)))) (h "18nnilcb63ylh5vrfw21dfi13hqkahsb1l51ifxjfiapsad6qcha")))

(define-public crate-tbe-0.3.2 (c (n "tbe") (v "0.3.2") (d (list (d (n "base2") (r "^0.3.0") (d #t) (k 0)) (d (n "bitrw") (r "^0.8.2") (d #t) (k 0)) (d (n "int") (r "^0.3.0") (d #t) (k 0)))) (h "00m8ny6dajxh0gk2iwfsfy8mpwxy4pc81x5nn8m2s20hjdnn0ax7")))

(define-public crate-tbe-0.3.3 (c (n "tbe") (v "0.3.3") (d (list (d (n "base2") (r "^0.3.1") (d #t) (k 0)) (d (n "bitrw") (r "^0.8.3") (d #t) (k 0)) (d (n "int") (r "^0.3.0") (d #t) (k 0)))) (h "1j6gjcaq1pmi45dbs69hw5gbcmzcqcfwwrlvqpn1hbrx65j13knw")))

