(define-module (crates-io #{3}# t tja) #:use-module (crates-io))

(define-public crate-tja-0.1.0 (c (n "tja") (v "0.1.0") (d (list (d (n "rhythm-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (o #t) (d #t) (k 0)))) (h "09rrpzc2zilwjngpxwbdcln8s6qkga2d49wghaacqisy59j6wnnk") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-tja-0.1.1 (c (n "tja") (v "0.1.1") (d (list (d (n "rhythm-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (o #t) (d #t) (k 0)))) (h "18r60rhc1bb9bw5cqgavx10vmx2g6b13vhwdy3f7japyhm9ani15") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

(define-public crate-tja-0.2.0 (c (n "tja") (v "0.2.0") (d (list (d (n "rhythm-core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (o #t) (d #t) (k 0)))) (h "0ss0q09nl0hjnq3m0i5ddrwdjx8nwi8a62b44spca5i5m3bw7bsm") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_json"))))))

