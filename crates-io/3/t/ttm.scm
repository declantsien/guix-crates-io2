(define-module (crates-io #{3}# t ttm) #:use-module (crates-io))

(define-public crate-ttm-0.1.0 (c (n "ttm") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (f (quote ("bundled"))) (d #t) (k 0)))) (h "171z4ypc6qjhmvyybybkwy2jjipg7w34r9lhxzzaa4cp3vi3fbsq")))

