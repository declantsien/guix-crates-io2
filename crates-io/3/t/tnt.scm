(define-module (crates-io #{3}# t tnt) #:use-module (crates-io))

(define-public crate-tnt-0.1.0 (c (n "tnt") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "onig") (r "^6.1.1") (d #t) (k 0)))) (h "07rchrdn3sb477n4kiic40g2zlj5fhrvzinha0pf4f6s11c11w0k")))

(define-public crate-tnt-0.1.1 (c (n "tnt") (v "0.1.1") (d (list (d (n "fancy-regex") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "01qaig1v4fl61b26rf6swrifs5wsfllzdjdg8dwfhcvmwvrwdysd")))

(define-public crate-tnt-0.2.0 (c (n "tnt") (v "0.2.0") (d (list (d (n "fancy-regex") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0zmzvzza3c5c39ggq8abah9j5gbng9xfmvrw84l70p3kzbzw801v")))

(define-public crate-tnt-0.4.0 (c (n "tnt") (v "0.4.0") (d (list (d (n "fancy-regex") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0bzsq6apf0l3lbgb94rs90z4s7ipiw384irihrr2a9p758nyp6h7")))

(define-public crate-tnt-0.4.1 (c (n "tnt") (v "0.4.1") (d (list (d (n "fancy-regex") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "14xdyyz72kjc90x53ni78wvgm68xs7wy3wmqna6qcmdadmxkffyp")))

(define-public crate-tnt-1.0.0 (c (n "tnt") (v "1.0.0") (d (list (d (n "fancy-regex") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1jy20gln427pmnbjn3wxwjr3mfkbzqj5k2l5z8yjg6yl0h79gzzl")))

(define-public crate-tnt-1.0.1 (c (n "tnt") (v "1.0.1") (d (list (d (n "fancy-regex") (r "^0.5.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "1rh6nkijbj5gc9x48sm39vffvdz6yb8lxc5dig2rg77fv07jic0b")))

(define-public crate-tnt-1.0.2 (c (n "tnt") (v "1.0.2") (d (list (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.4.1") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1kih8ad9nrx67v0a2ydci5xk9pp2l09cjr83l0f64drcsnq66jz4")))

