(define-module (crates-io #{3}# t tbf) #:use-module (crates-io))

(define-public crate-tbf-0.1.0 (c (n "tbf") (v "0.1.0") (d (list (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1sidx09k4qfm3gdl3r3qqx4nd7dif468h7jlvpxjkmq9ln4pdcv8") (f (quote (("std") ("imfs" "spin") ("dfs" "std") ("default" "std" "imfs" "dfs"))))))

(define-public crate-tbf-0.1.1 (c (n "tbf") (v "0.1.1") (d (list (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0vg0cm3awx1rz5s0aqwxq1vjh9pbkf86v1pp6d65aiwxbgylhybf") (f (quote (("std") ("imfs" "spin") ("dfs" "std") ("default" "std" "imfs" "dfs"))))))

(define-public crate-tbf-0.1.2 (c (n "tbf") (v "0.1.2") (d (list (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "12bj9sd4x4fn21q88il5a0kbya2fpxvjd8ksmsz1jbd8mc95ifdb") (f (quote (("std") ("imfs" "spin") ("dfs" "std") ("default" "std" "imfs" "dfs"))))))

(define-public crate-tbf-0.1.3 (c (n "tbf") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1xiy3jlvmyrf8y4m5glr79122m70a2zh3plma8yk5w4zb9kavbq6") (f (quote (("std") ("imfs" "spin") ("dfs" "std") ("default" "std" "imfs" "dfs"))))))

(define-public crate-tbf-0.1.4 (c (n "tbf") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1s71d8r3mvlpxwfqqbsrl6ywrc89ajrg9irc191hk0fi4vzsak1b") (f (quote (("std") ("imfs" "spin") ("dfs" "std") ("default" "std" "imfs" "dfs"))))))

(define-public crate-tbf-0.1.5 (c (n "tbf") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1jp7lx1dr0d64r7qr9afakas2hvwi57lw5j38408jbcky2zg7rx2") (f (quote (("std") ("imfs" "spin") ("dfs" "std") ("default" "std" "imfs" "dfs"))))))

(define-public crate-tbf-0.1.6 (c (n "tbf") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "09whdlj3kmk2h9g0s39dnb1vk7qc19gmxhfn94dxfc43bi0541qk") (f (quote (("std") ("imfs" "spin") ("dfs" "std") ("default" "std" "imfs" "dfs"))))))

(define-public crate-tbf-0.1.7 (c (n "tbf") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0rns6wqwmdjrmpj077r8p8814gak1wz216hqyw1yjc4fc7pz2a0d") (f (quote (("std") ("imfs" "spin") ("dfs" "std") ("default" "std" "imfs" "dfs"))))))

