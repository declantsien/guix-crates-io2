(define-module (crates-io #{3}# t tlv) #:use-module (crates-io))

(define-public crate-tlv-0.1.0 (c (n "tlv") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0693d463zmn43337kv7d902lk53gbb8vf3lwqp147s6vl6fzk4ai")))

(define-public crate-tlv-0.2.0 (c (n "tlv") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "16qv68xycb64wk0wsv5339k6nyldrn3hg7girdx77757i3d13gid")))

