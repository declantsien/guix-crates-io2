(define-module (crates-io #{3}# t ttv) #:use-module (crates-io))

(define-public crate-ttv-0.4.0 (c (n "ttv") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.1") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "flate2") (r "^1.0.14") (d #t) (k 0)) (d (n "indicatif") (r "^0.14.0") (d #t) (k 0)) (d (n "jemallocator") (r "^0.3.2") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)) (d (n "try_from") (r "^0.3.2") (d #t) (k 0)))) (h "0g8khxr1nhpnpzgrbgl6p7knzgnjm5aaj12cja6im8hmbwrj59ln")))

