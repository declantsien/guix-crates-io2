(define-module (crates-io #{3}# t tcv) #:use-module (crates-io))

(define-public crate-tcv-0.1.1 (c (n "tcv") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pn240nm6wl7r6alm5jxn7kbyb4hq2mmcnpsn3cc6jhsx6464f05") (y #t)))

(define-public crate-tcv-0.1.2 (c (n "tcv") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)))) (h "00chx1ripnb3q8w62igq6pwz3p4zby44c3gaj11plgfj7lb0nqwv")))

