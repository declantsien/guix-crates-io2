(define-module (crates-io #{3}# t tbc) #:use-module (crates-io))

(define-public crate-tbc-0.1.0 (c (n "tbc") (v "0.1.0") (h "1h6w5ljbavqd41vi780xjp4xlqgmb8ww8dz2pafw9h46gl85s7zp")))

(define-public crate-tbc-0.2.0 (c (n "tbc") (v "0.2.0") (h "0rk3l7vyspn2hjkvm7nngimwnkmdzgr5qwayd54pwpavaxck6ii1")))

(define-public crate-tbc-0.3.0 (c (n "tbc") (v "0.3.0") (h "1x9vsva4dk3ga1a7yjrmpzyq0432xaklxg31sqr0jlqkriz8122l")))

