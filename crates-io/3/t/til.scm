(define-module (crates-io #{3}# t til) #:use-module (crates-io))

(define-public crate-til-0.1.0 (c (n "til") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1.0.0") (f (quote ("sqlite" "chrono"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0rzqvh2nkb7wnd8m06i5icxww1yg12nrn15gns1v02cgyy75bxvv")))

(define-public crate-til-0.1.1 (c (n "til") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1.0.0") (f (quote ("sqlite" "chrono"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "0w44h99nrdv2xrcpm2f5w3vwwkc0b8yfg36ygc2csvm0bifjaapw")))

(define-public crate-til-0.1.2 (c (n "til") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "diesel") (r "^1.0.0") (f (quote ("sqlite" "chrono"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.1.0") (d #t) (k 0)) (d (n "xdg") (r "^2.1") (d #t) (k 0)))) (h "178nsxa4bpvmgnn6fci3ff52bcd9n6nxfbp8g7z9hxr1bm8avdms")))

