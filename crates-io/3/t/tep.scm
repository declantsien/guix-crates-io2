(define-module (crates-io #{3}# t tep) #:use-module (crates-io))

(define-public crate-tep-0.1.0 (c (n "tep") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0kfx3shcgklzsjk6lmnbzbmg8hwkfjs1iqcj59bw6nnw3sn8ql15")))

(define-public crate-tep-0.1.1 (c (n "tep") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1g8w8q3276wn9sacxdahklbxax3x53w05fd0carjk5xh5kh4g111")))

(define-public crate-tep-0.1.2 (c (n "tep") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "14zc1lp58xmipwllw3jrl98bv6j8lymihmx58wq8c02kzm787ijh")))

(define-public crate-tep-0.1.3 (c (n "tep") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0vmxbqfbcl36bmiy75jsavkclsbwp8hjk55p8x612c9ka83a0maq")))

(define-public crate-tep-0.1.4 (c (n "tep") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1m7ghkrlsia0wmdiq2sfx2wq1qs243xvnkg53xg3lwww4lv3r5bv")))

(define-public crate-tep-0.2.0 (c (n "tep") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0h49n845wakac1s2jva48z0wwzpxf7m3lma79kaqck0d70im1171")))

(define-public crate-tep-0.2.1 (c (n "tep") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1k4kiqbnynii52mdkch2d2szrqkd7q6dgr1m3iwi6jpqz81kza1a")))

(define-public crate-tep-0.2.2 (c (n "tep") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "062nv7ghw8bkd5pvpgn3fnym97jsfv40dr4bk3vlrh6k52cy8yry") (y #t)))

(define-public crate-tep-0.2.3 (c (n "tep") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0ckkhzb1sr6magccyv3qih5rm9w3nab0icrhvyk8ll71sg7z70q1")))

(define-public crate-tep-0.2.4 (c (n "tep") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1cxg869giq5m4s51n42n306hvqnpivwrvjikws9b2672sbr3ic2p")))

