(define-module (crates-io #{3}# t tsl) #:use-module (crates-io))

(define-public crate-tsl-1.0.0 (c (n "tsl") (v "1.0.0") (h "14kk0f6rfm8mjm7l47q8jjfw9772wf6zyhfbwsknq9yrap1ksrj8") (r "1.66.0")))

(define-public crate-tsl-1.0.1 (c (n "tsl") (v "1.0.1") (h "0pbygh2sd5fipyw1v79iwbxkwpf36sg4z9b4d42ij725khn5sgjc") (r "1.66.0")))

(define-public crate-tsl-1.0.3 (c (n "tsl") (v "1.0.3") (h "12ccf5j13mh4lyzdx6xzcjdx5y1iv8s6b3fr0x84ww4j7zc8xfvs") (r "1.66.0")))

