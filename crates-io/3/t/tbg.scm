(define-module (crates-io #{3}# t tbg) #:use-module (crates-io))

(define-public crate-tbg-0.1.0 (c (n "tbg") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)))) (h "0bc16fz818whb7a9y1kxkh6vsvkmqfwm23gyabmxq6n8128xj5mp") (y #t)))

(define-public crate-tbg-0.1.1 (c (n "tbg") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)))) (h "08jzpnagcb9lw4h0mmgb5kzsms9drdsr9jcdx420vy61369csjif") (y #t)))

(define-public crate-tbg-0.1.2 (c (n "tbg") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)))) (h "1xi29dr9c38m7xg99dsldyar17813458a3wkh3abnw15lyxqlnz6") (y #t)))

(define-public crate-tbg-0.1.3 (c (n "tbg") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)))) (h "0wzq17bl9pnvghbk09lckn7g895bgz2g8c3zxkigs44m2f5vi63q")))

