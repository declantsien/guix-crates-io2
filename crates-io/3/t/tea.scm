(define-module (crates-io #{3}# t tea) #:use-module (crates-io))

(define-public crate-tea-0.1.0 (c (n "tea") (v "0.1.0") (d (list (d (n "conrod") (r "^0.51.1") (f (quote ("glium" "winit"))) (d #t) (k 0)) (d (n "glium") (r "^0.16.0") (d #t) (k 0)) (d (n "glutin") (r "^0.8.0") (d #t) (k 0)))) (h "0lf4a2bsfyibfarf4myldjgyh70n7mi6g9cj2li4l7x4g17mx68a")))

