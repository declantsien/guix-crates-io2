(define-module (crates-io #{3}# t tsu) #:use-module (crates-io))

(define-public crate-tsu-0.1.0 (c (n "tsu") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1rg0c210xqzgims533dhwwq34c0dzadadf1h9db7ri6khz21jplf")))

(define-public crate-tsu-0.1.1 (c (n "tsu") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "19126l0c1vm5zjl9zfkcgv31m9p56p0dr6lr56b4k6idr4cqnmhh")))

(define-public crate-tsu-0.1.2 (c (n "tsu") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0jwgsxn5y2yg6r00iydllxxg4dr5ybyzz9s22qs28ixbi4012hwm")))

(define-public crate-tsu-0.1.3 (c (n "tsu") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1rm8xwz47l9041jxha5wsn37cb722sj5jya2gj7fbcw6y4c2ymqf")))

(define-public crate-tsu-0.1.4 (c (n "tsu") (v "0.1.4") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0cshixvix3bmchba9xbmr0pc47b9yw1ygbvgrbsv7v55n31c0yw2")))

(define-public crate-tsu-0.1.5 (c (n "tsu") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0jizi301lp38pwq62yxjxpwwhnjcnhpa4vxprz61s95nxgj15f2a")))

(define-public crate-tsu-0.1.6 (c (n "tsu") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1sqsgwbqgr1iqczpxd9xrs8r3nd4sfizzzyjjfwcpbgzx0anj6ds")))

(define-public crate-tsu-0.1.7 (c (n "tsu") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "157bshijslqkih8n0v4bj1ryp9vgnvhr0brmz7j8qsmvgplxf690")))

(define-public crate-tsu-0.1.8 (c (n "tsu") (v "0.1.8") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0sahjk9n26h0vlqa9wq7f6fr8hg43h5gmn8r0d265hlgpk42lnam")))

(define-public crate-tsu-0.1.9 (c (n "tsu") (v "0.1.9") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "117g03xjvyyn2pxkc1h35845ylqi4hz02fm7dk7j360985mlcndg")))

(define-public crate-tsu-0.1.10 (c (n "tsu") (v "0.1.10") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "16np411ml5bix4lnfbv2162ax0js86v452n4y6zxz89pcac1cd50")))

(define-public crate-tsu-0.1.11 (c (n "tsu") (v "0.1.11") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1s54px1z2wamb8jwmx7wj029qixzvrlfpxmlsafjvp8jyb4yhaql")))

(define-public crate-tsu-0.1.12 (c (n "tsu") (v "0.1.12") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0h68nfqdimiwbiprxqhl4z8zl1v08byhxshkpcq5nd5j6w90g8m4")))

(define-public crate-tsu-0.1.13 (c (n "tsu") (v "0.1.13") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0f57326x1175xs2zhbjp0x6brd0i07k6vxbpia2gv3rqnirnr0pk")))

(define-public crate-tsu-0.1.14 (c (n "tsu") (v "0.1.14") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0jd33r8i3366k860zdsr7i45javr06cnizcr8dydna5fqrhd1qcr")))

(define-public crate-tsu-0.1.15 (c (n "tsu") (v "0.1.15") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1afish5lq8nrqlni4nv44bbzwj953d4lkrq46f81z14b639vard6")))

(define-public crate-tsu-0.1.16 (c (n "tsu") (v "0.1.16") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0qk7n3ysy9h20xr2mcmjkiafrppzyr5l1mn7cls2pfla7a5f22zy")))

(define-public crate-tsu-0.1.17 (c (n "tsu") (v "0.1.17") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1pyilly9sppp4nhccqrisqik6g37wn3bz9dszcqpm3xwpzdhphd7")))

(define-public crate-tsu-0.1.18 (c (n "tsu") (v "0.1.18") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0ycivdx431kfr0s3zlhmz66wcfhm342gb19blb4xfhscr8ckp895")))

(define-public crate-tsu-0.1.19 (c (n "tsu") (v "0.1.19") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0yfb4d53n2f58ha0i9zhp6y8hsh7hr1dl86g16brnsfljvw3vzjd")))

(define-public crate-tsu-0.1.20 (c (n "tsu") (v "0.1.20") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "05nxbcizbqd28w37fg2sdj626gvyrqmqv1izv8ww8zj6ncajn5hc")))

(define-public crate-tsu-0.1.21 (c (n "tsu") (v "0.1.21") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1lhspp9caqfkv2g4fjxfyh0v4z3hca4wkwvgf3br7j539ynj3hcd")))

(define-public crate-tsu-0.1.22 (c (n "tsu") (v "0.1.22") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0gjdgz2qv6injy8gn4xk8wqrjc9dxqcgbxhs0cc4ms75rkzm1273")))

(define-public crate-tsu-0.1.23 (c (n "tsu") (v "0.1.23") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0k80v90876ja7sgsbqnkqmmbdsayxg4pgmvkdk8d3ln7y3l78dmm")))

(define-public crate-tsu-0.1.24 (c (n "tsu") (v "0.1.24") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0pl19m68jw82vhkx9afbadn3yagng8s9m08v83y77k99nkx945v5")))

(define-public crate-tsu-0.1.25 (c (n "tsu") (v "0.1.25") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1imacnllbwwk42ia7g2y305f24v2ig2gdf7r5c1mx0hs2y1svvgi")))

(define-public crate-tsu-1.0.0 (c (n "tsu") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "16m7wh56g7bz0c41qr1nnqn9n6l71b3m0q55sm1zf6p2xy8d5y8n")))

(define-public crate-tsu-1.0.1 (c (n "tsu") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde-transcode") (r "^1.1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.10") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0fbi48hsbsqmmdn9qvb9f696kikkxz4wrj7a8ydj1sh0qm6d1crf")))

