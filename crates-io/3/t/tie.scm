(define-module (crates-io #{3}# t tie) #:use-module (crates-io))

(define-public crate-tie-0.0.0 (c (n "tie") (v "0.0.0") (h "1zd9hdbid2ss8y5vjik3sf86qcyhsriyd905w2jrn0j4ylk740qw") (y #t)))

(define-public crate-tie-0.0.1 (c (n "tie") (v "0.0.1") (h "04mp257549a7pmhvybqgfdrm7q17m7135q23kdiid0sxnl0r5s3j") (y #t)))

(define-public crate-tie-0.0.2 (c (n "tie") (v "0.0.2") (h "1jlsya4s9in6941lkb554ldh5cqvfkyp3xgls0bk9jja0hvi29s7")))

(define-public crate-tie-0.0.3 (c (n "tie") (v "0.0.3") (h "1is4ccnm58pfs7969mdcim0kmqmqcy90mlk0nkw36fsys8xsqkfx")))

