(define-module (crates-io #{3}# t trx) #:use-module (crates-io))

(define-public crate-trx-0.1.0 (c (n "trx") (v "0.1.0") (h "0gjhfsspk2d2l3s01m03lks75c2ywpf5a87hvnzgm364nd2vls75")))

(define-public crate-trx-0.1.1 (c (n "trx") (v "0.1.1") (d (list (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1r1vyyv6gyldmw6y7sgyfjwvpyh5i9i3dpd1hmmijndycm4dknhy")))

(define-public crate-trx-0.2.0 (c (n "trx") (v "0.2.0") (d (list (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1bqh0micyw69zsj0c0rh597lqy8pxjbyd1jlqh44gzhkaa3zhbjq")))

(define-public crate-trx-0.2.1 (c (n "trx") (v "0.2.1") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "117ljr2v05yi6rg7z3fgkr1dy0kg0wl9d739bpx8jayw9sxx0n39")))

(define-public crate-trx-0.2.2 (c (n "trx") (v "0.2.2") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0m8f7ryv9fvsg0l09mgbgdn9l7a96a21bxbp4zpl31wsd6sf3602")))

(define-public crate-trx-0.2.3 (c (n "trx") (v "0.2.3") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0bsx6l7zrb4i6i645zchk86vam0ffmgkwbkb6ma77aw4pj1q5h4m")))

(define-public crate-trx-0.2.4 (c (n "trx") (v "0.2.4") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "05b7y10rfszy9qsiddwnhagiha3dpj2b2j68b8nqy6n45cfb3xi3")))

(define-public crate-trx-0.2.5 (c (n "trx") (v "0.2.5") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1v0zz3rsbfyf3bnrxsf46mc14pxz9xcrb5iqxhc44hha0n11cwpm")))

(define-public crate-trx-0.2.6 (c (n "trx") (v "0.2.6") (d (list (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "07bgwc2d6j3klfjpyj06jdxyi8y7nwqrffcah099wz8v4bn17qbf")))

