(define-module (crates-io #{3}# t tst) #:use-module (crates-io))

(define-public crate-tst-0.1.0 (c (n "tst") (v "0.1.0") (h "1fgsk673z5y63sifkdja8dldz4ic1026k5yl83vq92nv6x35hlrh")))

(define-public crate-tst-0.1.1 (c (n "tst") (v "0.1.1") (h "1bh6m1chvxa8s23kr9xyrdhyydzpi5nscbc6zhgx7hh8hb2drhw4")))

(define-public crate-tst-0.1.2 (c (n "tst") (v "0.1.2") (h "1b0jnsvv7gc652pxskynm3785mrzcha021acx8qf4ydps3mljxf4")))

(define-public crate-tst-0.2.0 (c (n "tst") (v "0.2.0") (h "045waf50wr7mf0kmnklxb77wsmdacnaphbxa4c9k6dfhz7l8rq8p")))

(define-public crate-tst-0.3.0 (c (n "tst") (v "0.3.0") (h "17l1pvd5784097n9k9jy740rm3h00jkjnwxw97qbvvkpgk2r1rw8")))

(define-public crate-tst-0.3.1 (c (n "tst") (v "0.3.1") (h "1pafb7ai54qs3b18lcsy6kamvaq1jl5w7nacl2a1yy3ns77iahmr")))

(define-public crate-tst-0.3.2 (c (n "tst") (v "0.3.2") (h "1mckdgdph9fndxyvcsqw4kvkkkkz8sbg23cpic58zv3xl4gwmrxs")))

(define-public crate-tst-0.3.3 (c (n "tst") (v "0.3.3") (h "14626k467haid997zc6wzvqvbycc0df4fs9pi4nlm9227k2hnxrg")))

(define-public crate-tst-0.3.4 (c (n "tst") (v "0.3.4") (h "0qik0sd5rcbk7n6rk5i2mgbw8ndy91zg62k56awjhwl3pmybklvq")))

(define-public crate-tst-0.4.0 (c (n "tst") (v "0.4.0") (h "1zjmj298v4kxh7wnadyb8dr4w5cl3iqpbfb0nbdwa7d7p8pklxz9")))

(define-public crate-tst-0.5.0 (c (n "tst") (v "0.5.0") (h "1f15hgcg0n03pbgs20vijrs70dfd47b48qz7sdp4x0gk1wb0wvmk")))

(define-public crate-tst-0.6.0 (c (n "tst") (v "0.6.0") (h "1d6381rg7x3v3nv2nkxla9hdnkdgml8svk773kgkd6sa4qcjv488")))

(define-public crate-tst-0.7.0 (c (n "tst") (v "0.7.0") (h "10zdmwyhjjv8zwam9amzr6yl9imbmv6n2n9drmx3d3rykjkx2hss")))

(define-public crate-tst-0.8.0 (c (n "tst") (v "0.8.0") (h "0w7vpp3vsq76p59lplj63iy4ljij63iy1b9159fiif3b0dmsi1q4")))

(define-public crate-tst-0.8.1 (c (n "tst") (v "0.8.1") (h "12i1i05x55vcy3iv7fib1ch08xzxp6lwa3jdl395ncnwnwp6wfwf")))

(define-public crate-tst-0.8.2 (c (n "tst") (v "0.8.2") (h "1d022gkg82pmiahc3d099232ibq76ng7s2wlb896dsy1xnj3jaf1")))

(define-public crate-tst-0.8.3 (c (n "tst") (v "0.8.3") (h "14n63f4wiqyyncl0y12x031y0d4dwhr62arbc82jr3729lj4p57n")))

(define-public crate-tst-0.8.4 (c (n "tst") (v "0.8.4") (h "0y7xs95yf1irw6fqrspqrz40zan8sdk5lnjqhjb1j3y083kxz7bm")))

(define-public crate-tst-0.9.0 (c (n "tst") (v "0.9.0") (h "14rlg974mj9dvwzgdfgg7lk6ss7svndb96gq340d7ilbay7p1dqd")))

(define-public crate-tst-0.9.1 (c (n "tst") (v "0.9.1") (d (list (d (n "libc") (r "0.2.*") (d #t) (k 2)) (d (n "rand") (r "0.4.*") (d #t) (k 2)))) (h "127m0dryj64zy3yppnaajx0i9hbs6p4lzds2g9zsp9a76xhy4fzc")))

(define-public crate-tst-0.10.0 (c (n "tst") (v "0.10.0") (d (list (d (n "jemalloc-sys") (r "0.1.*") (d #t) (k 2)) (d (n "jemallocator") (r "0.1.*") (d #t) (k 2)) (d (n "libc") (r "0.2.*") (d #t) (k 2)) (d (n "rand") (r "0.6.*") (d #t) (k 2)))) (h "0dvxsvsl9n8zib3zf58gydff9iqd3xw20hzpwbbagdl4lzz53vky")))

(define-public crate-tst-0.10.1 (c (n "tst") (v "0.10.1") (d (list (d (n "jemalloc-sys") (r "0.1.*") (d #t) (k 2)) (d (n "jemallocator") (r "0.1.*") (d #t) (k 2)) (d (n "libc") (r "0.2.*") (d #t) (k 2)) (d (n "rand") (r "0.6.*") (d #t) (k 2)))) (h "0a4rqi9ldjiwdhabc3kisilrh03bvh857xhmy0znsy4qr22j8fmj")))

