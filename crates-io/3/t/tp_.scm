(define-module (crates-io #{3}# t tp_) #:use-module (crates-io))

(define-public crate-tp_-0.3.0 (c (n "tp_") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sugiura-hiromichi_mylibrary") (r "^1") (d #t) (k 0)))) (h "19scp2wknlcbsbjfh2bi1j6lngc33pbbynqfcr973rnwnf0s5fqp")))

(define-public crate-tp_-0.3.1 (c (n "tp_") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mylibrary_") (r "^1") (d #t) (k 0)))) (h "1xcwjjj07rhcv6mlcf1n8fzgszi7jpf5nmfgfns3kk0zyvqib4wh")))

