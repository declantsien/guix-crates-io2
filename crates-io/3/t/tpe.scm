(define-module (crates-io #{3}# t tpe) #:use-module (crates-io))

(define-public crate-tpe-0.1.0 (c (n "tpe") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "kurobako_core") (r "^0.1") (d #t) (k 2)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_distr") (r "^0.3") (d #t) (k 0)) (d (n "statrs") (r "^0.13") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1v9iqy9kdgc9hk8aq0cq1d2g18ny0wlc0sk2qqy06j38ll2m9gps")))

(define-public crate-tpe-0.1.1 (c (n "tpe") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "kurobako_core") (r "^0.1") (d #t) (k 2)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "statrs") (r "^0.13") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "17qf2r2ml7paynbya8848a1ab5bpd2jlw2pl1407nc8q1y5pqhvg")))

(define-public crate-tpe-0.2.0 (c (n "tpe") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "kurobako_core") (r "^0.1") (d #t) (k 2)) (d (n "ordered-float") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "statrs") (r "^0.15") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1vmzhmh8n2rwc4a6w9nr1czll1y2abbj1z511sfi1lvcfw309181")))

