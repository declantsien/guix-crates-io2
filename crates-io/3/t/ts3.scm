(define-module (crates-io #{3}# t ts3) #:use-module (crates-io))

(define-public crate-ts3-0.1.0 (c (n "ts3") (v "0.1.0") (h "18c49wbp0bz9li81cy7hccmaqzbvs4xl17kh2fl1ljly1jfblg98")))

(define-public crate-ts3-0.2.0 (c (n "ts3") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "11zrxcplbvsdcy7lw7i85091kfvmq140kn6vd1ny0xbva3fl0k45")))

(define-public crate-ts3-0.2.1 (c (n "ts3") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)))) (h "04jdv4qlkdqmzphid8gf2fv35cygfs1g6zclrkfm16hc0sdg47qa")))

(define-public crate-ts3-0.2.2 (c (n "ts3") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)))) (h "1v4qag371h3v8aggpw511v4b8x0caiyns8acn54ajih52y7mwyg0")))

(define-public crate-ts3-0.2.3 (c (n "ts3") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)))) (h "1vc7b91yf1nfq90sq8jjxcm06v0yqhlslwszbqxxn57rgivcj1m0")))

(define-public crate-ts3-0.2.4 (c (n "ts3") (v "0.2.4") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "174jj5dmfijk5fr5f6sk3f90aqvzjn2np6rfx27wmj627zdln2fa")))

(define-public crate-ts3-0.2.5 (c (n "ts3") (v "0.2.5") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "0nichx48arym08hqw7zb1famrb8bv04y0m57vv9j3z9qa2pclqy4")))

(define-public crate-ts3-0.2.6 (c (n "ts3") (v "0.2.6") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "ts3_derive") (r "^0.1.0") (d #t) (k 0)))) (h "13js7wf8g3xmi0sjvkai4v14wv1fkigkhpwvv7b1vvy5fhc77g5b")))

(define-public crate-ts3-0.2.7 (c (n "ts3") (v "0.2.7") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "ts3_derive") (r "^0.1.0") (d #t) (k 0)))) (h "019jl8xilf8dd50wdw5cqd43mm11fcbax5hkyvzgjv4y8h3gjbvy")))

(define-public crate-ts3-0.2.8 (c (n "ts3") (v "0.2.8") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "ts3_derive") (r "^0.1.2") (d #t) (k 0)))) (h "13djv6962giyiza67g4g7fqkphricdyp5f5l2h5brmg12rmxjmbs")))

(define-public crate-ts3-0.3.0 (c (n "ts3") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "ts3_derive") (r "^0.1.2") (d #t) (k 0)))) (h "08mqqhl040yh0j6dmg8nx2ax1bk6s883x3psrk50c66bk0qjvafv")))

(define-public crate-ts3-0.3.1 (c (n "ts3") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "ts3_derive") (r "^0.2.0") (d #t) (k 0)))) (h "0dgfqchrhqms6mm58knd2avamfvpg39b0js1vi10d85g4q6lfnsx")))

(define-public crate-ts3-0.3.2 (c (n "ts3") (v "0.3.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "ts3_derive") (r "^0.2.0") (d #t) (k 0)))) (h "059wgk6s0n1jfrl6d1kf4vk5i4z3661ad94bxhmz3yisprzv6bhc")))

(define-public crate-ts3-0.3.3 (c (n "ts3") (v "0.3.3") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt" "rt-multi-thread" "io-util" "net" "time" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("macros" "rt" "rt-multi-thread" "signal"))) (d #t) (k 2)) (d (n "ts3_derive") (r "^0.2.0") (d #t) (k 0)))) (h "11wg719qcdmm825w8f5nq1ydqllfhndqyy37zndrnsgfxw9s53nn")))

(define-public crate-ts3-0.4.0 (c (n "ts3") (v "0.4.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("net" "time" "sync" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt" "rt-multi-thread" "signal"))) (d #t) (k 2)) (d (n "ts3_derive") (r "^0.4.0") (d #t) (k 0)))) (h "1ci49f7zdxy5cg1v87ax9rwbdmij6ybg9p1za2zy9cfiw6i6s95g")))

(define-public crate-ts3-0.4.1 (c (n "ts3") (v "0.4.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("net" "time" "sync" "io-util" "rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("macros" "rt" "rt-multi-thread" "signal"))) (d #t) (k 2)) (d (n "ts3_derive") (r "^0.4.0") (d #t) (k 0)))) (h "14dd6fsh28d3b9b6115yhs93npqqm7lkiancfwci0bvixqajmwn0")))

