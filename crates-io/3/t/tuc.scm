(define-module (crates-io #{3}# t tuc) #:use-module (crates-io))

(define-public crate-tuc-0.1.0 (c (n "tuc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1v7v5ziylyx2p5vwcxdiii654vmdyc63avpl1flgvlajgiaz1zj3")))

(define-public crate-tuc-0.3.0 (c (n "tuc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "0wqn0lpxx86xb07w2nl37z3py77dv5k4vnvw6925ckgk50snkkab")))

(define-public crate-tuc-0.4.0 (c (n "tuc") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1cirhpr1iwl61zsfh243s17jj4y685xi4a6fqdlzixhki78dzg97")))

(define-public crate-tuc-0.5.0 (c (n "tuc") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.41") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.5") (d #t) (k 2)) (d (n "pico-args") (r "^0.4.2") (f (quote ("short-space-opt" "combined-flags"))) (d #t) (k 0)))) (h "15shi3f7bdi9z4yq9f3c9kgb1bvg2c35m5dhlizr1ac7lm70ymif")))

(define-public crate-tuc-0.6.0 (c (n "tuc") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "pico-args") (r "^0.4.2") (f (quote ("short-space-opt" "combined-flags"))) (d #t) (k 0)))) (h "0m4fima3w12y74vdf61pkj2ydnq4328dmab26pj58y2dvwpmhkzd")))

(define-public crate-tuc-0.7.0 (c (n "tuc") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "pico-args") (r "^0.4.2") (f (quote ("short-space-opt" "combined-flags"))) (d #t) (k 0)))) (h "0z5w98wfv18sxfdrgc0z4kwq98wsr90bsrzz3rb285wv6wqqlbjn")))

(define-public crate-tuc-0.8.0 (c (n "tuc") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "pico-args") (r "^0.4.2") (f (quote ("short-space-opt" "combined-flags"))) (d #t) (k 0)))) (h "0qpzkc38zqvn7wsm9axm7k2brginqp3k22rvk1mj6jl1m8fg5bw7")))

(define-public crate-tuc-0.9.0 (c (n "tuc") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "pico-args") (r "^0.4.2") (f (quote ("short-space-opt" "combined-flags"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1fxzh0pc6bpn9g1933s473s4d2v0s0bgvgmh821aj2bx67amnlby") (f (quote (("default"))))))

(define-public crate-tuc-0.10.0 (c (n "tuc") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "pico-args") (r "^0.5.0") (f (quote ("short-space-opt" "combined-flags" "eq-separator"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1dz09mf9068a56g1fy335cg7c24fkx2ra6h143x172wfzbp4dibf") (f (quote (("default"))))))

(define-public crate-tuc-0.11.0 (c (n "tuc") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "pico-args") (r "^0.5.0") (f (quote ("short-space-opt" "combined-flags" "eq-separator"))) (d #t) (k 0)) (d (n "regex") (r "^1.5") (o #t) (d #t) (k 0)))) (h "15w9ai5kngmhfax8qp0hdyjb63m7p4vs6jdxsz81l118ypvwndkg") (f (quote (("default"))))))

(define-public crate-tuc-1.0.0 (c (n "tuc") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 2)) (d (n "pico-args") (r "^0.5.0") (f (quote ("short-space-opt" "combined-flags" "eq-separator"))) (d #t) (k 0)) (d (n "regex") (r "^1.7") (f (quote ("std" "unicode-bool" "unicode-perl" "unicode-gencat"))) (o #t) (k 0)))) (h "158j0g4dss5zzf11zzzrnrfb1ansvcah32mp5jbybnap8zxp27qv") (f (quote (("default" "regex"))))))

(define-public crate-tuc-1.1.0 (c (n "tuc") (v "1.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "pico-args") (r "^0.5.0") (f (quote ("short-space-opt" "combined-flags" "eq-separator"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (k 2)) (d (n "regex") (r "^1.8") (f (quote ("std" "unicode-bool" "unicode-perl" "unicode-gencat"))) (o #t) (k 0)))) (h "0aacbfwzl8j4jmr3mlnpqsq0q6zj60fnm8m02kznckxffxcg6w9y") (f (quote (("default" "regex"))))))

(define-public crate-tuc-1.2.0 (c (n "tuc") (v "1.2.0") (d (list (d (n "anyhow") (r "^1.0.78") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "pico-args") (r "^0.5.0") (f (quote ("short-space-opt" "combined-flags" "eq-separator"))) (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (k 2)) (d (n "regex") (r "^1.10") (f (quote ("std" "unicode-bool" "unicode-perl" "unicode-gencat"))) (o #t) (k 0)))) (h "0zv06cgly398l9y94c9rd5gpx515j3prmvxzdymwr3n21i49iacs") (f (quote (("default" "regex"))))))

