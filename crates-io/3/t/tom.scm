(define-module (crates-io #{3}# t tom) #:use-module (crates-io))

(define-public crate-tom-0.0.1 (c (n "tom") (v "0.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "m_lexer") (r "^0.0.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "string-interner") (r "^0.6") (d #t) (k 0)) (d (n "text_unit") (r "^0.1") (d #t) (k 0)) (d (n "uncover") (r "^0.1") (d #t) (k 0)))) (h "14b0h90ih7blz7n4fbdgay23kdwzl7f8ipyydzq7hp5wjqjvp7ws")))

