(define-module (crates-io #{3}# t tnl) #:use-module (crates-io))

(define-public crate-tnl-0.1.0 (c (n "tnl") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)))) (h "0v2zf0wr3d5wn7cp0p435aj5zgdv75j2aw7slyv3nw93ik7i414p")))

(define-public crate-tnl-0.2.0 (c (n "tnl") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.13") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.2") (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "predicates") (r "^3.0.4") (d #t) (k 2)))) (h "1r0f1cc0whalk27jq6fdia24z5c5vffw2h1gmj86lpljibi1pnvp")))

