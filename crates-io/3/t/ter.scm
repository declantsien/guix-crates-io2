(define-module (crates-io #{3}# t ter) #:use-module (crates-io))

(define-public crate-ter-0.1.0 (c (n "ter") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "te") (r "^0.1.0") (d #t) (k 0)))) (h "02ymmy0yn50a4n27hsrb88l2jk68050z1104a7n29miaj3hy9djq") (y #t)))

(define-public crate-ter-0.1.1 (c (n "ter") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "te") (r "^0.1.0") (d #t) (k 0)))) (h "0kyjwlb59yppif68yrc4wkpa9rcpzdwb1jc0h6bqq9k7yg2yh054") (y #t)))

