(define-module (crates-io #{3}# t tas) #:use-module (crates-io))

(define-public crate-tas-0.1.0 (c (n "tas") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive" "std"))) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "xdg") (r "^2.4.1") (d #t) (k 0)))) (h "058gvv0x0fghdgd17ai2cnr5smj9h6l99kjcmw5l981y4857l9w0")))

