(define-module (crates-io #{3}# t tml) #:use-module (crates-io))

(define-public crate-tml-0.1.0 (c (n "tml") (v "0.1.0") (d (list (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.8.1") (k 0)))) (h "0f6vvlp45w7jj1dxfva07gq5xm9w674n38kcgv95c1i7wy2df371")))

(define-public crate-tml-0.2.0 (c (n "tml") (v "0.2.0") (d (list (d (n "clap") (r "^2.20.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.8.1") (k 0)))) (h "03v4vyd0n5cz50av5i55ydpcqzyzyc04lhmxfydxvh993wh4yzla")))

