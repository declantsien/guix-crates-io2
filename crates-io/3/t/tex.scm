(define-module (crates-io #{3}# t tex) #:use-module (crates-io))

(define-public crate-tex-0.1.0 (c (n "tex") (v "0.1.0") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)) (d (n "tex_derive") (r "^0.1.0") (d #t) (k 0)))) (h "126i7yhinfn8y5glddmx2f34a3shwk8n71ng7ca7q21pvifrwhsy")))

(define-public crate-tex-0.1.1 (c (n "tex") (v "0.1.1") (d (list (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "rayon") (r "^1.2.1") (d #t) (k 0)) (d (n "tex_derive") (r "^0.1.0") (d #t) (k 0)))) (h "1lfr6wrbn7zgakfxmhrzfvymp2kyhvrhhc34dcqw0aw00rvhkqfb")))

