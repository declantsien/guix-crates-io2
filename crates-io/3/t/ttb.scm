(define-module (crates-io #{3}# t ttb) #:use-module (crates-io))

(define-public crate-ttb-0.1.0 (c (n "ttb") (v "0.1.0") (d (list (d (n "proptest") (r "^0.9.6") (d #t) (k 2)) (d (n "regenboog") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "14xv17vq7n5cl1qf0gv0rv1h0jdds7i963dwwihv3al28j2xnfms")))

(define-public crate-ttb-0.1.1 (c (n "ttb") (v "0.1.1") (d (list (d (n "proptest") (r "^0.9.6") (d #t) (k 2)) (d (n "regenboog") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "105554qlgdd9dgqim22yf7j7x5lr9p36i17bhwlk1n4ib399nnqb")))

