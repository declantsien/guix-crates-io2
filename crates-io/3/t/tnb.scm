(define-module (crates-io #{3}# t tnb) #:use-module (crates-io))

(define-public crate-tnb-0.1.0 (c (n "tnb") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13") (d #t) (k 0)))) (h "0ja98h2nn1j1sdzxl6y0w9xdxpdgvdz2jkvwxs5ddsyh4l9zq69q")))

(define-public crate-tnb-0.1.1 (c (n "tnb") (v "0.1.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13") (d #t) (k 0)) (d (n "ureq") (r "^0.11.2") (f (quote ("tls" "json"))) (d #t) (k 0)))) (h "05sa8b4bfk3i0fnh8rax1dryxz6ilabdxkv7nbg3pjc7p07x1g60")))

(define-public crate-tnb-0.1.2 (c (n "tnb") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.17") (d #t) (k 0)) (d (n "ureq") (r "^2.4.0") (f (quote ("tls" "json"))) (d #t) (k 0)))) (h "16q8dgkhv19b7grwhs9pbzjfld123l526wbdpsss38fiysinra55")))

