(define-module (crates-io #{3}# t tar) #:use-module (crates-io))

(define-public crate-tar-0.0.1 (c (n "tar") (v "0.0.1") (h "1q26d900b2wsfhc9gmqy5pk98ibj8bkifgp8jd6ag7jj9dl8190k") (y #t)))

(define-public crate-tar-0.0.2 (c (n "tar") (v "0.0.2") (h "1b127mi30jqh2dvnmhq8wdqypl4jdw6hqdw70yfdznlgf6zqd6g9")))

(define-public crate-tar-0.0.3 (c (n "tar") (v "0.0.3") (h "1rv80akhbafjd4rz4ayv6kaqzvscsvfg9vqd794jijvzxlisykfi")))

(define-public crate-tar-0.1.0 (c (n "tar") (v "0.1.0") (h "0424vbqch22spjkyfx0bg74rka60gghg1avdzw6niw0qyf6mg8ka")))

(define-public crate-tar-0.1.1 (c (n "tar") (v "0.1.1") (h "144nlshqy7r4vi5shxcqn7sdmxdab4a6jgcwk03m3rrrd778i060")))

(define-public crate-tar-0.1.2 (c (n "tar") (v "0.1.2") (h "0n47l1smzrymdsbjx013snglj90vkgnmniimx5444cyl4ciai344")))

(define-public crate-tar-0.1.3 (c (n "tar") (v "0.1.3") (h "0nq3cqw7ivwxynyas51qsr3ab6w2f20nqfzsgxfj7a3gg6v2l2ky")))

(define-public crate-tar-0.1.4 (c (n "tar") (v "0.1.4") (h "04r758kiclwis8dhjnklbci4mp7dyqqrqqnjzlgns201c96a5x66")))

(define-public crate-tar-0.1.5 (c (n "tar") (v "0.1.5") (h "01m4j2mbkswav24imbawdv87nmar2qazfffsd3kdpa5h9k95ksiw")))

(define-public crate-tar-0.1.6 (c (n "tar") (v "0.1.6") (h "1xgh34rnij4bffhdpdinf3lcbjsjh5pk9bil32d1lqjmx8gllmaq")))

(define-public crate-tar-0.1.7 (c (n "tar") (v "0.1.7") (h "1x329l5b7a6lb15gxw1g18j30bvp7wmy034lbvag150z6jah0ka2")))

(define-public crate-tar-0.1.8 (c (n "tar") (v "0.1.8") (h "1kshlz427dxknfs04j1ziiqhly97l7kwlkk25llbpmsby09rbzf3")))

(define-public crate-tar-0.1.9 (c (n "tar") (v "0.1.9") (h "1iwz9my5v5mcf5l6p8hzi4d72w403jlh50lil991ha8982rx7mpv")))

(define-public crate-tar-0.1.10 (c (n "tar") (v "0.1.10") (h "0bgw0biisa55li7ccgkiz93yp7qbxy2l6vhpyqk1cfn66ln9s08h")))

(define-public crate-tar-0.1.11 (c (n "tar") (v "0.1.11") (h "1dyn5lwv690a041v60rw3ic078w74nzsn78nxgpxafhr1313ryd1")))

(define-public crate-tar-0.2.0 (c (n "tar") (v "0.2.0") (d (list (d (n "rand") (r "^0.1") (d #t) (k 2)))) (h "0qaw9a4yvbs3szdnmkdhg1qn1lmgrmb5ikv0sc8bkkzq2jsc7rd0")))

(define-public crate-tar-0.2.1 (c (n "tar") (v "0.2.1") (h "1na28wxh1n0q1v0f4l6vzdzr4269cq2ac5izigyqg40a6gl4xdk6")))

(define-public crate-tar-0.2.2 (c (n "tar") (v "0.2.2") (h "1crrnwxq2kzvhfl8afslcn1yqn1d81cqh357pfsnmmwhadvynp1v")))

(define-public crate-tar-0.2.3 (c (n "tar") (v "0.2.3") (h "0c0nd8zi69m1rlbii2czci4d46y9ffz5inkc2v0jqi8r3fxh7l0l")))

(define-public crate-tar-0.2.4 (c (n "tar") (v "0.2.4") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0z9j1578cmd6cmc47dmwjiy6wkxrh47pcxizj3yjlw3drj6p7h0y")))

(define-public crate-tar-0.2.5 (c (n "tar") (v "0.2.5") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "01nvl7iri2lqv2bmq7lm3zrhgar8p1qv8qk5afn7kfjfvfrdr298")))

(define-public crate-tar-0.2.6 (c (n "tar") (v "0.2.6") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1jh0igjqbgpzsq9r9py83ig16zxqq7y21yh2q160yp348ipsjnv2")))

(define-public crate-tar-0.2.7 (c (n "tar") (v "0.2.7") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0a2zvdw3irz60zkf8lj6h6ps471p4rb0n2sj0vg01c2ckp845vr7")))

(define-public crate-tar-0.2.8 (c (n "tar") (v "0.2.8") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1fs7kk16v1sfgpfnj957layxh6drd1fwdqfi5bxprpvajz3an5a8")))

(define-public crate-tar-0.2.9 (c (n "tar") (v "0.2.9") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1jh2lzq56a97jpgb98w2l434ysbz6khwysl9r1qinsgrfq38byb9")))

(define-public crate-tar-0.2.10 (c (n "tar") (v "0.2.10") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1f0nlwvpp6s3wny8z4k09b31ag0dvdnfq48x4ndngb1j81jsr2a2")))

(define-public crate-tar-0.2.11 (c (n "tar") (v "0.2.11") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "17rlyng3mmpq8ikv3x52qv44rg05ky7f2d8chx103hrcn1irayli")))

(define-public crate-tar-0.2.12 (c (n "tar") (v "0.2.12") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1g41ai8ypv45vzxmym4m5w598y3xmqa694ydh160385figw5d3qk") (f (quote (("nightly"))))))

(define-public crate-tar-0.2.13 (c (n "tar") (v "0.2.13") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1jnwwzx80sk4m6dji8d45gzqwwhzrbwrddwl1g6sd32w0q55ss6j") (f (quote (("nightly"))))))

(define-public crate-tar-0.2.14 (c (n "tar") (v "0.2.14") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ffgj29g9y50p9lp6438dkm9p6lgkh9h5l8v37xdkkla06677735") (f (quote (("nightly"))))))

(define-public crate-tar-0.3.0 (c (n "tar") (v "0.3.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1hb8apzcwhd81g3h8agah5n46sc2iypg6742xzvbjp0m24smg45h")))

(define-public crate-tar-0.3.1 (c (n "tar") (v "0.3.1") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "06snakq1svh414dim04cbr2cwnf18imvw4jx01znqvw87k5g56n6")))

(define-public crate-tar-0.3.2 (c (n "tar") (v "0.3.2") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1i3qcfljbm2bz0qgvq1dhp4lnr9rns6vfvar4c431f06qp66898v")))

(define-public crate-tar-0.3.3 (c (n "tar") (v "0.3.3") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1yd6x69c4k626sn28h47lkssas8nxqp613gqdha50fb1lzfrhv4s")))

(define-public crate-tar-0.4.0 (c (n "tar") (v "0.4.0") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0pkz7lhafixj7n1kq9aw08fiwdxwzw35mrc5dvg3fy7nb1nr665k")))

(define-public crate-tar-0.4.2 (c (n "tar") (v "0.4.2") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ki5lcqzkzyan6clhwl98dim46034yv3xjsfnsf1jdx51j0j41s0")))

(define-public crate-tar-0.4.3 (c (n "tar") (v "0.4.3") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1m8g9phwmdhrsr8k6yi2a8158kyrjln091a0b3rfapi63k4k6fpx")))

(define-public crate-tar-0.3.4 (c (n "tar") (v "0.3.4") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "03md1gs50vagc0ann12j1dzmx4fhn0yllifa2mzfi5msv31j4s3c")))

(define-public crate-tar-0.4.4 (c (n "tar") (v "0.4.4") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1lwj2y6i8dzjc9srcq7kcmrwp00xj24b1sj2z74bhqz6l3cr49n8")))

(define-public crate-tar-0.4.5 (c (n "tar") (v "0.4.5") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1qghxcyck6vw6xvskcpp7q4sm48w6d57hc7ixbanddvbdhvjglxr")))

(define-public crate-tar-0.4.6 (c (n "tar") (v "0.4.6") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "15jl3lpx08f4vcaz2m4c6szmkwf2h3bsapgzkmww5mlkji6h39ar")))

(define-public crate-tar-0.4.7 (c (n "tar") (v "0.4.7") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.1.7") (d #t) (t "cfg(unix)") (k 0)))) (h "1p42pjw09w25nwpwx7f9wa71fcjs6xbfz7850ddp5cb272zn4px0")))

(define-public crate-tar-0.4.8 (c (n "tar") (v "0.4.8") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.1.7") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0r9v7012v0695w65vhxqm6nvmv89qrylvgxxkp3f9hipyrcvkq8j") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.9 (c (n "tar") (v "0.4.9") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.1.7") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1vi3nl8s3jjf5l20ni47gmh1p4bdjfh7q50fbg7izzqrf7i4i40c") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.10 (c (n "tar") (v "0.4.10") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.1.7") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0lkxzzibhnap9gdqay0lbf0k81g3vz35zyzwyj9wlhr8r5pbzcqy") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.11 (c (n "tar") (v "0.4.11") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.1.7") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1n15nk3kg1lddxykz6ykf9pi0wqsbw6j9mixr335lb5yicqlydy2") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.12 (c (n "tar") (v "0.4.12") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.1.7") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1s3c85jqcfjh55sfyya72bs46lgxgch6mabmhkhsj2pysbmgj3mb") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.13 (c (n "tar") (v "0.4.13") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.1.7") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "06rb7yjja28hffw7yhw9gmf80x9xqs4yy1frka8slv4j2yvqa4i8") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.14 (c (n "tar") (v "0.4.14") (d (list (d (n "filetime") (r "^0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.1.7") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "158ijnxq7hk1b4binj74szm1fc64bm5spszza8ljal7bihwd618n") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.15 (c (n "tar") (v "0.4.15") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "060xddyjvqhvd4dxgz8bxs2kafbr83sp2nwsfszp39grb53bkxka") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.16 (c (n "tar") (v "0.4.16") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0ah61fnpa5fp20mdzj8cjpqn1ix61nvgqiq2iychd7v8lnj1rx78") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.17 (c (n "tar") (v "0.4.17") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1pmkbbh9d4mrk829wr2b13k9jpw13rjxvyik35l65zfvad5x3c43") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.18 (c (n "tar") (v "0.4.18") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0x7fn6hjp4jakq9bp0gn4s9bhhn1lk3qaa77jlhw3nbj49a1idc9") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.19 (c (n "tar") (v "0.4.19") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0k322l66xkpzrj3ag2lvr6b59f54qqffyfdph2lg38g0l506iqb9") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.20 (c (n "tar") (v "0.4.20") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1qlskn85h5hs7pnvgr43aakrc6jr4hklvca6lsmd5z4rl1hbl0x3") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.21 (c (n "tar") (v "0.4.21") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0rdxm3i0i5xnwpiy6384nf3fpwimalg2iyl48jf956y9agd46jwh") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.22 (c (n "tar") (v "0.4.22") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0cq19hjng70z9f4b5d1nxzzis6xnj4dgg69j5dq639m27pspy5n2") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.23 (c (n "tar") (v "0.4.23") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0j8vw4lnxrbdi5q0d7ijfpjapk21ydip8icf787hc3fkid6qkkwa") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.24 (c (n "tar") (v "0.4.24") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1yy7vrwcjwsqsy26z16dwc37mv91d22hhad45j8di3scc2i73l1d") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.25 (c (n "tar") (v "0.4.25") (d (list (d (n "filetime") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1w322xk7rdjx7wa47lr8yhdgsd7cvjv5g58cq0xlxcwmxm6j20bj") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.26 (c (n "tar") (v "0.4.26") (d (list (d (n "filetime") (r "^0.2.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1lr6v3cpkfxd2lk5ll2jd8wr1xdskwj35smnh5sfb8xvzzxnn6dk") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.27 (c (n "tar") (v "0.4.27") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "02czc7cnl0ncjf6w5ccq1rk3k6mw53nzin794affzv3qy7casq3i") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.28 (c (n "tar") (v "0.4.28") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1hpamv8j4ccr9nm37zx9gdfqmvlvr57lvi2clbx89jvcpp88l1aw") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.29 (c (n "tar") (v "0.4.29") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "12ifihz34hk58lp9cpcm081wyqxmxxgc2dj3aiwh28z3pv8c3968") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.30 (c (n "tar") (v "0.4.30") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1452rf0y7jf4apyngvbfpszpik7a9z7gl9y5jbhl76kyanvrg6a8") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.31 (c (n "tar") (v "0.4.31") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1nrl9mfx2d6idbcxzj6491r33wqn1skmlqzxfnnpr6a4ry1l56x6") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.32 (c (n "tar") (v "0.4.32") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1ijcgx0hwypcd9xn1hqwjgn0qsdn9z5pns099yz2k7nm05n584q3") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.33 (c (n "tar") (v "0.4.33") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0a7jjp9lqyqz2fd5q5ryqffhhr9xzygld0hd4zd1ydlqlpbgpg60") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.34 (c (n "tar") (v "0.4.34") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1d122dz80pnyw5a795yhi7032hflqcbfwdcjl8mhifw79z4byiqs") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.35 (c (n "tar") (v "0.4.35") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "100v082cda6g8a237vrnpf075pqr7z46wrhgawa960pzmv39sxvx") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.36 (c (n "tar") (v "0.4.36") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0iz40bd47xr69dsbckd6rv5ry2nqb2dp3z850q41pvpnmk6xk441") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.37 (c (n "tar") (v "0.4.37") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "0p5srm08dly2bdgkpnjdfmxvldmvi31q6yxdvjxy0lnx79fm3xfn") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.38 (c (n "tar") (v "0.4.38") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1ikiz14wbfmaaw5mrv93msa8v6n3i595z5kw9p0fdqa40dy80mab") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.39 (c (n "tar") (v "0.4.39") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "15mcfm91iq2xlhi0xaxl2g2j675y16rirzs6ilv9d0h7mpzx55pc") (f (quote (("default" "xattr"))))))

(define-public crate-tar-0.4.40 (c (n "tar") (v "0.4.40") (d (list (d (n "filetime") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "xattr") (r "^1.0") (o #t) (d #t) (t "cfg(unix)") (k 0)))) (h "1nrd3v2kfhb2zh0a44ag0s2348xjcdxiqx8cl14ir2923zmgqsmi") (f (quote (("default" "xattr"))))))

