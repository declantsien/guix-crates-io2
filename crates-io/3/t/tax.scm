(define-module (crates-io #{3}# t tax) #:use-module (crates-io))

(define-public crate-tax-0.0.2 (c (n "tax") (v "0.0.2") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "19v6cx83mlxmbg08bly9iwmk4ln4jr333fh7581kqcxam2pxfph0")))

(define-public crate-tax-0.0.3 (c (n "tax") (v "0.0.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "1hwvcyvns8rpxklnadl87hwllflwqa4kryhlcwb0hni1iwa93cd0")))

(define-public crate-tax-0.0.4 (c (n "tax") (v "0.0.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "0c988fqn02c8wz7y0invr079lrflvmckg1z6d6szg0np3mdpyahx")))

(define-public crate-tax-0.0.5 (c (n "tax") (v "0.0.5") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "13xxflhsrnrzhly8cyw9jcya48v8536nvycgbq0fbxxr402n4bhs")))

(define-public crate-tax-0.0.6 (c (n "tax") (v "0.0.6") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "0np5zb6919v67b682dfrbbq4ja37xmp55almmjbbjcqa88hn72z7")))

(define-public crate-tax-0.0.7 (c (n "tax") (v "0.0.7") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "which") (r "^4.0.1") (d #t) (k 0)))) (h "1hsqxlkyfxk1pi488dbzdirg4d548sympv7wvgavsm1927d4zy5b")))

