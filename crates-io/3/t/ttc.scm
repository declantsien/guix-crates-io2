(define-module (crates-io #{3}# t ttc) #:use-module (crates-io))

(define-public crate-ttc-0.0.1 (c (n "ttc") (v "0.0.1") (h "15h23myfahbfbbhd54klw0m0clivaq2xd6nf7692yw1qza5g5yl6")))

(define-public crate-ttc-0.0.2 (c (n "ttc") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)))) (h "1m7h2sdbl9wk5h43giw7wzq9xd1s3h44cgz6w6z01f6sbr8dm15n")))

(define-public crate-ttc-0.0.3 (c (n "ttc") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0z8idn6skbj882zykkhwz4b7sb1r9f6nphm4zwdmgns337akzgr1")))

(define-public crate-ttc-0.0.4 (c (n "ttc") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0jsppy7w6nf6518p6c006a2q5368fpkh4jmhjqcxx12sfgi5kc6d")))

(define-public crate-ttc-0.0.5 (c (n "ttc") (v "0.0.5") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "structopt") (r "^0.3.26") (d #t) (k 0)))) (h "0c10knbn16p3ifnlcxnrmy90ib8x6cxj1y10sf2xn7dh0x58kasi")))

