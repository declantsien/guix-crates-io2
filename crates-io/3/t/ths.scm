(define-module (crates-io #{3}# t ths) #:use-module (crates-io))

(define-public crate-ths-0.1.0 (c (n "ths") (v "0.1.0") (d (list (d (n "billboard") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.10") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "1fm0l5a85x30fmnsqxjzbjqzgkqwpdp7qqf9h2d98gpwr21yrn3z")))

(define-public crate-ths-0.2.0 (c (n "ths") (v "0.2.0") (d (list (d (n "billboard") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.10") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "1p3xl7rdsrldwh9ms7rm3zsrb9f8gavb87x2pg6rc0pnm2ddp3kj")))

(define-public crate-ths-0.2.1 (c (n "ths") (v "0.2.1") (d (list (d (n "billboard") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.10") (d #t) (k 0)) (d (n "dialoguer") (r "^0.5.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "owo-colors") (r "^3.4.0") (d #t) (k 0)))) (h "1mc7cpaz4vg1s7lv5d8dxgbzscgh2ap12lisisr8l6sfqrjwkgbx")))

