(define-module (crates-io #{3}# t tpk) #:use-module (crates-io))

(define-public crate-tpk-0.0.1 (c (n "tpk") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "16sxw9v6js7bfii03k5yl82hb3k6wxkizj2xxxn71kg5c5ag843w")))

(define-public crate-tpk-0.0.2 (c (n "tpk") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0f4ripi3rar4kzaz5yplrsqri93qin7b180l3raq086m3sd6d9f2")))

