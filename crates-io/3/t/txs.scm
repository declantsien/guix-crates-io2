(define-module (crates-io #{3}# t txs) #:use-module (crates-io))

(define-public crate-txs-0.1.0 (c (n "txs") (v "0.1.0") (d (list (d (n "sha2") (r "^0.7.0") (d #t) (k 0)))) (h "10fhfjgpzzf1l70mqz2sdcnbksdk58mpddva0psqc4b9abl5z219")))

(define-public crate-txs-0.2.0 (c (n "txs") (v "0.2.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "rocksdb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "0zljkkl061p2jszzrwnxm18iih0wxffch8r6djl643w9mwi3zpmb")))

(define-public crate-txs-0.3.0 (c (n "txs") (v "0.3.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "rocksdb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "1l8gb98c4adk8b75vhwlzs5lz43lzxzdyh4b6hfrmn7k8vx0wigp")))

(define-public crate-txs-0.4.0 (c (n "txs") (v "0.4.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "rocksdb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "02b3b6ja0v3cp7r0czrjgxqf33iq78lrcny0j48n4j2g1fff4dwc")))

(define-public crate-txs-0.5.0 (c (n "txs") (v "0.5.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "rocksdb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "1awpvxbzx4ama5lmd1ag9i59ld8wxyj5qnkhl3h752d1pf0dk1lz")))

(define-public crate-txs-0.6.0 (c (n "txs") (v "0.6.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "rocksdb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "0fh37gw1xfdb03w5pz20l9av3y7bfg0pw4hsw9fgac6a6afiwxhn")))

(define-public crate-txs-0.7.0 (c (n "txs") (v "0.7.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "rocksdb") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "0ljm8yldrifgnlw1r3wymjkm638lnwnrxs2vlwk8wbcgp3x7yqpz")))

