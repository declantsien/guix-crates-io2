(define-module (crates-io #{3}# t tip) #:use-module (crates-io))

(define-public crate-tip-0.1.0 (c (n "tip") (v "0.1.0") (h "1zkr3gxmbxxz1p5npsx0cqwxgq7p91nmrnrwcl48hyk08f92db23")))

(define-public crate-tip-0.2.0 (c (n "tip") (v "0.2.0") (h "05ims4n3l6hkfb4ng6ahy0mhsl5vs2626jibsfz03xshv929d67j")))

(define-public crate-tip-0.3.0 (c (n "tip") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "0zbbs7bpjsxnd0wbrrrpnrq8n3xijjwi2mbnqrqp5awha2llqyf2")))

(define-public crate-tip-0.3.1 (c (n "tip") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1y957ah870fvaa6vbm754c36p4a8gm95nfx6i2iaf9r2d9j4134g")))

