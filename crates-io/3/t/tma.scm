(define-module (crates-io #{3}# t tma) #:use-module (crates-io))

(define-public crate-tma-0.1.0 (c (n "tma") (v "0.1.0") (d (list (d (n "error-chain") (r "^0-.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0pg18l9c6ixz9s8k9pxlw1n6mffbg9f4hqvp0vkx7c9lw3vlbd64")))

(define-public crate-tma-0.1.1 (c (n "tma") (v "0.1.1") (d (list (d (n "error-chain") (r "^0-.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0nj43cgy70fli86z0gac6iiic7qpk8w9a7r6sbb4ab4gx9hk1g65")))

