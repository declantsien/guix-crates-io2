(define-module (crates-io #{3}# t tmt) #:use-module (crates-io))

(define-public crate-tmt-0.1.0 (c (n "tmt") (v "0.1.0") (h "158pi4a7k9n6cz78dlpp6hsv79vias6c0sx473i22nabf4pkl76j")))

(define-public crate-tmt-0.1.1 (c (n "tmt") (v "0.1.1") (h "15x3dm179cxaf9a20aw7903zlk29d6r994220rcf263jk85yh44s")))

(define-public crate-tmt-0.1.2 (c (n "tmt") (v "0.1.2") (h "1v3rdqrmlcdscpby26wgnrnjikh7wqnnxz3vh7qp7w01hhwd365s")))

