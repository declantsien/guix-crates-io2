(define-module (crates-io #{3}# t tdb) #:use-module (crates-io))

(define-public crate-tdb-0.3.0 (c (n "tdb") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "fern") (r "^0.5.8") (d #t) (k 0)) (d (n "libtectonic") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0g8ysjyinzqsya6wkb1xf93ms8g6rvspiyhabxrhd57s61yf4300") (y #t)))

