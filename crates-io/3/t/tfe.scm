(define-module (crates-io #{3}# t tfe) #:use-module (crates-io))

(define-public crate-tfe-0.1.0 (c (n "tfe") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1wwfiaimq6qaxdm787f3f6i7l6fac29f9106jpxcd24bwyiyjjas")))

(define-public crate-tfe-0.1.1 (c (n "tfe") (v "0.1.1") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0znk90gyfa6il38p386n7q9gk6x7xlihdzqzhwiss8rvdnsxmxyp") (y #t)))

(define-public crate-tfe-0.1.2 (c (n "tfe") (v "0.1.2") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1qallhk7p6jzsk53fs8ihfdls1l11a1m2gpv5lby88lyzx017db9")))

