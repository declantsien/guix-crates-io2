(define-module (crates-io #{3}# t trc) #:use-module (crates-io))

(define-public crate-trc-0.1.0 (c (n "trc") (v "0.1.0") (h "0sm23vd3wv1ic377n229a65xzy4rbp21mi4rbw03sn6c6jgy4k9x")))

(define-public crate-trc-0.1.1 (c (n "trc") (v "0.1.1") (h "1iajkj17fa4lrchqqp1vp45s8vk8qj7vji7c14d18qzgzg1wnvy0")))

(define-public crate-trc-0.1.2 (c (n "trc") (v "0.1.2") (h "0yk1001mr5rpk8k9nmn89lgk1jg58qmkyxxrkg9hy4vv7cd8abc5") (y #t)))

(define-public crate-trc-0.1.3 (c (n "trc") (v "0.1.3") (h "1q58vwvbf2kwm80hkgq73yf1rydmaiw1ybfm3kdr0l1j3ri21fzj") (y #t)))

(define-public crate-trc-1.1.0 (c (n "trc") (v "1.1.0") (h "0hhkdvvyx4pnk3wx9gircp1jwkcgc9k5l5ahg1w2vdm56r1ywjf8")))

(define-public crate-trc-1.1.1 (c (n "trc") (v "1.1.1") (h "0ixjg4dkah7j5pm0vd6hxwpbsaqf5966lyqvh1cg036mckirszh7")))

(define-public crate-trc-0.2.1 (c (n "trc") (v "0.2.1") (h "153j6556hqw6wb3bdyl9sgz3bz9m4hm8mbjlc5lfl85v2phaqsza") (y #t)))

(define-public crate-trc-1.1.2 (c (n "trc") (v "1.1.2") (h "15fw5j2m3qiczy6n96m8z0qkajwwbrj7id5ddm19da23xxpfvq5g")))

(define-public crate-trc-1.1.3 (c (n "trc") (v "1.1.3") (h "192my7iny2fiaajz032hvfs3q0cxk91mv7l4vg70ril6a2rmskw9")))

(define-public crate-trc-1.1.4 (c (n "trc") (v "1.1.4") (h "0kr3ks5p3sr67szmpqh914926nwpjpga18mjkhr368spscghwwpz")))

(define-public crate-trc-1.1.5 (c (n "trc") (v "1.1.5") (h "0j4kvp3fckfrx4q9vkrsjqxmjmqcjy2c0qzlpxa8vkx7jv89l1g4") (y #t)))

(define-public crate-trc-1.1.6 (c (n "trc") (v "1.1.6") (h "0rdykmlbzzgyxy8b4wcgb8pg8rln7ryrbjh0i66fwvrj3kwcdkck") (y #t)))

(define-public crate-trc-1.1.7 (c (n "trc") (v "1.1.7") (h "08h4ypci1y0ivgaj4scmld57z0d4q078w4ynamjw8fdw3magpfm0")))

(define-public crate-trc-1.1.8 (c (n "trc") (v "1.1.8") (d (list (d (n "spin") (r "^0.9.8") (o #t) (d #t) (k 0)))) (h "1y30p9wdp05a01296pgb6dk4m2li5g5755sdqq9dr6gcaffid8fb") (f (quote (("test_lock_nostd" "spin/rwlock" "nostd" "test_lock") ("test_lock" "spin/rwlock") ("test_atomic") ("nostd") ("default"))))))

(define-public crate-trc-1.1.9 (c (n "trc") (v "1.1.9") (d (list (d (n "spin") (r "^0.9.8") (o #t) (d #t) (k 0)))) (h "1sj2r22hkcgq3yvpqj8c5zki066rs8csp73xb00dn29h9jmqb68h") (f (quote (("nostd") ("force_lock" "spin/rwlock") ("force_atomic") ("default"))))))

(define-public crate-trc-1.1.10 (c (n "trc") (v "1.1.10") (d (list (d (n "spin") (r "^0.9.8") (o #t) (d #t) (k 0)))) (h "0lh9fd53nxcfh41q01daiwwdics1in0dny55p0miac1sv0n0rimz") (f (quote (("nostd") ("force_lock" "spin/rwlock") ("force_atomic") ("default"))))))

(define-public crate-trc-1.1.11 (c (n "trc") (v "1.1.11") (d (list (d (n "spin") (r "^0.9.8") (o #t) (d #t) (k 0)))) (h "0hcl9xqyrh2d02ggvb8n2yv5x12g44qj6y2f6h5hfc7fmcsqxdm8") (f (quote (("nostd") ("force_lock" "spin/rwlock") ("force_atomic") ("default"))))))

(define-public crate-trc-1.1.12 (c (n "trc") (v "1.1.12") (d (list (d (n "spin") (r "^0.9.8") (o #t) (d #t) (k 0)))) (h "16fs8lr9bjl26zai0ms7sji8ycmikz279n86kf0ch3n89cq167cr") (f (quote (("nostd") ("force_lock" "spin/rwlock") ("force_atomic") ("default"))))))

(define-public crate-trc-1.1.13 (c (n "trc") (v "1.1.13") (d (list (d (n "spin") (r "^0.9.8") (o #t) (d #t) (k 0)))) (h "1h4a050j2k0zlf6l9aj2yq3bm4ks11b8d7xvvcc1zlzw7ddaxkg5") (f (quote (("nostd") ("force_lock" "spin/rwlock") ("force_atomic") ("default"))))))

(define-public crate-trc-1.1.14 (c (n "trc") (v "1.1.14") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0yzpbpdsd1pg7njqysv236h8xb5s8kxhyj0y6cdm78j75msh31gk") (f (quote (("force_lock") ("force_atomic") ("default"))))))

(define-public crate-trc-1.1.15 (c (n "trc") (v "1.1.15") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "143xflk759l16rhj0r4mhmhmb6hhn2pgcb0zqm3mp1ylz0c2mkjn") (f (quote (("force_lock") ("force_atomic") ("default"))))))

(define-public crate-trc-1.1.16 (c (n "trc") (v "1.1.16") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0nq52laa1g1rcybnjjciplaxq652rpjimhhg04pf53ggq0373c4d") (f (quote (("force_lock") ("force_atomic") ("default"))))))

(define-public crate-trc-1.1.17 (c (n "trc") (v "1.1.17") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0krgv16f42l3fjm02sjj34p4c49zb3i4qndbykd1v1g9k20jsiik") (f (quote (("force_lock") ("force_atomic") ("default"))))))

(define-public crate-trc-1.1.18 (c (n "trc") (v "1.1.18") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0iky7zmr9gybnlpd12m14bg2v70drsjyay40n2kjdahzdp947m8a") (f (quote (("force_lock") ("force_atomic") ("default"))))))

(define-public crate-trc-1.2.0 (c (n "trc") (v "1.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1lq35nfwri3qgv1j027h2r34mhf9fb15vzcwi9rznfiki54xjqkf")))

(define-public crate-trc-1.2.1 (c (n "trc") (v "1.2.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1zbhr5rxr4ipj7qx880vhlldb6dirc00rgp3s9frs3f8rx8hnqms")))

(define-public crate-trc-1.2.2 (c (n "trc") (v "1.2.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0jsbidq2kl51394d7xnvln9ld4s63g4fj3sc2ymjp8a8wharh0ib") (f (quote (("dyn_unstable"))))))

(define-public crate-trc-1.2.3 (c (n "trc") (v "1.2.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "1pl19svhjycz9y4wnsy4ph5zdlf5rkpbyh2qh0xl6yw5l5xglpp2") (f (quote (("dyn_unstable"))))))

(define-public crate-trc-1.2.4 (c (n "trc") (v "1.2.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.2.0") (d #t) (k 0)))) (h "1im3jnrq2wibxi4aqqcmflpm22d42xzc7f3y80bbi5bb5a98xpci") (f (quote (("stable_deref_trait") ("serde") ("dyn_unstable"))))))

