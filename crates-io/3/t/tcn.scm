(define-module (crates-io #{3}# t tcn) #:use-module (crates-io))

(define-public crate-tcn-0.4.0 (c (n "tcn") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ed25519-zebra") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0x9ypk4g8lx2cmbax813aharwgn4m3yx3mhb8q720hy4acf6crhd")))

(define-public crate-tcn-0.4.1 (c (n "tcn") (v "0.4.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ed25519-zebra") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)) (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rhqmnyl8mac4ak7axdn5ds8jsih4rjl8qswzd86ingp4f7kr76s")))

