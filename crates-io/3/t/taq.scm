(define-module (crates-io #{3}# t taq) #:use-module (crates-io))

(define-public crate-taq-0.1.0 (c (n "taq") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25") (f (quote ("full"))) (d #t) (k 2)))) (h "1bh8x86rvkb2c8lp8xxxvpy53wm8qlgnkqbkgqr8rbrynan3xv8j")))

(define-public crate-taq-0.1.1 (c (n "taq") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25") (f (quote ("full"))) (d #t) (k 2)))) (h "0d906v0nn2xn62n3rl8vlbgi19pva7kclxni0fkfiv8c1alcxnj6")))

(define-public crate-taq-0.2.0 (c (n "taq") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25") (f (quote ("full"))) (d #t) (k 2)))) (h "1z5rkqcyk4nwjpgsx8cnfkq95kaf8ndp56ss7b1w4lhvf9fr72gv")))

(define-public crate-taq-1.0.0 (c (n "taq") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "state") (r "^0.5.3") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25") (f (quote ("full"))) (d #t) (k 2)))) (h "0q5a745vrn7gnvhld40i44r6d392f9sjd8pfcc5vmgwaf3ccncm2")))

(define-public crate-taq-1.1.0 (c (n "taq") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "state") (r "^0.5.3") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25") (f (quote ("full"))) (d #t) (k 2)))) (h "0gpvd24n6xmbs8d8xydb94iszqgza30dsjsls3s0d2s3jnyppd62")))

(define-public crate-taq-1.2.0 (c (n "taq") (v "1.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "state") (r "^0.5.3") (d #t) (k 0)) (d (n "tokio") (r "^1.25") (f (quote ("sync" "rt"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "tokio") (r "^1.25") (f (quote ("full"))) (d #t) (k 2)))) (h "1jklpwa1qcx855rq5qi7jfscifrdzx1s9xkqlq261fgr2hi8fsyk")))

