(define-module (crates-io #{3}# t tdf) #:use-module (crates-io))

(define-public crate-tdf-0.0.0 (c (n "tdf") (v "0.0.0") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "01hnwxyl80rj18g2vqb6365xjvqj74s5yqy408d08343qn6g8p9p") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-tdf-0.1.0 (c (n "tdf") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tdf-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0i2g937gfkw8qi13hnnd0qgd7v75apnvd3fkjp011bw1y73kw0wa") (f (quote (("default" "serde" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:tdf-derive") ("bytes" "dep:bytes"))))))

(define-public crate-tdf-0.1.1 (c (n "tdf") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tdf-derive") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0mmf70dbrjk8y4n3ldpycvpzq4h5jzjz83v9dml2sypj835dqw9d") (f (quote (("default" "serde" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:tdf-derive") ("bytes" "dep:bytes"))))))

(define-public crate-tdf-0.1.2 (c (n "tdf") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tdf-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0637hy9l0zf5sy9vzy4n39nwmym5xyyda0s6yx756f82zv6gv69j") (f (quote (("default" "serde" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:tdf-derive") ("bytes" "dep:bytes"))))))

(define-public crate-tdf-0.1.3 (c (n "tdf") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tdf-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "12844jckxy873h74nhzzpljypwj2cgzbwaqvr49pxr7fmxw4gd76") (f (quote (("default" "serde" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:tdf-derive") ("bytes" "dep:bytes"))))))

(define-public crate-tdf-0.1.4 (c (n "tdf") (v "0.1.4") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tdf-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "113qviciw6r7x2zycw8pqcisp9zs8ic2y5d130ksnwi94zv1c2hl") (f (quote (("default" "serde" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:tdf-derive") ("bytes" "dep:bytes"))))))

(define-public crate-tdf-0.3.0 (c (n "tdf") (v "0.3.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tdf-derive") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0psajsllv41dlxm3ypkh1d1ci78d6jsl3s247hx3mz3vca5pawk6") (f (quote (("default" "serde" "derive")))) (y #t) (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:tdf-derive") ("bytes" "dep:bytes"))))))

(define-public crate-tdf-0.4.0 (c (n "tdf") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "tdf-derive") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "00csvw7kqp00ydq136hmkfbanzahfvxm0w0rhi3q7b7r8zkxl3pd") (f (quote (("default" "serde" "derive")))) (s 2) (e (quote (("serde" "dep:serde") ("derive" "dep:tdf-derive") ("bytes" "dep:bytes"))))))

