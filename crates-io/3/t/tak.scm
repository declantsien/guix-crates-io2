(define-module (crates-io #{3}# t tak) #:use-module (crates-io))

(define-public crate-tak-0.1.0 (c (n "tak") (v "0.1.0") (h "1knclm790zy9gaq9343hyajd9zzysfnpx12jvnrc9n0xfady5km7")))

(define-public crate-tak-0.2.0 (c (n "tak") (v "0.2.0") (h "1wa69msxn5nflvnx1rqiszn5iywm4v9dg9z4sg4giq9l7j3bhsdq")))

(define-public crate-tak-0.2.1 (c (n "tak") (v "0.2.1") (h "0pv2b285dv64mhi0hn0dlxkb7i9b90aafmah0ldjf8x61avm4640")))

(define-public crate-tak-0.2.2 (c (n "tak") (v "0.2.2") (h "1m42p1fr51gaw3nk7rgi270nf8rlndppsz3v24yiwhf3gnp1rpfi")))

(define-public crate-tak-0.2.3 (c (n "tak") (v "0.2.3") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1x8dhvbj1kmc71n9dmqwi2nppjiwmd33m75yr3yifziprv62yvz1")))

(define-public crate-tak-0.3.0 (c (n "tak") (v "0.3.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1xlw2klcmrnxsqygd2lz4p53ic2h1j9izjzwcrf69brjc6msxbki")))

(define-public crate-tak-0.4.0 (c (n "tak") (v "0.4.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1vmsfi8jnbgdm0f1kf5j3dyid099f16q7b9hhaskr986nl7wnigs")))

(define-public crate-tak-0.5.0 (c (n "tak") (v "0.5.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1f881g37bsbb4mzl82hr34nwf2kqfaidfgb5w81vdkp4ni15rrqq")))

(define-public crate-tak-0.5.1 (c (n "tak") (v "0.5.1") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0wcrn3mgbk02b01hwxvvsnrccqvmrj9wil2wmr6m3qm6v9n77nqs")))

(define-public crate-tak-0.6.1 (c (n "tak") (v "0.6.1") (d (list (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "twiddle") (r "^0.1.2") (d #t) (k 0)))) (h "1qsglbrhs80s97kqjka3kip52863krbniqjsljzybg074f6q2n1q")))

