(define-module (crates-io #{3}# t tbn) #:use-module (crates-io))

(define-public crate-tbn-0.4.4 (c (n "tbn") (v "0.4.4") (d (list (d (n "bincode") (r "^0.6") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "^1.0") (f (quote ("i128"))) (k 0)) (d (n "crunchy") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (f (quote ("i128_support"))) (k 0)) (d (n "rand") (r "^0.5") (f (quote ("i128_support"))) (d #t) (k 2)) (d (n "rustc-hex") (r "^2") (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "02yl82730kr57cvvirhp0s6dw9qh60wfdbkl66pw2f3rksx09vdm") (f (quote (("default" "rustc-serialize"))))))

