(define-module (crates-io #{3}# t tge) #:use-module (crates-io))

(define-public crate-tge-0.0.0 (c (n "tge") (v "0.0.0") (h "1l4xxfa6kh0qv1f4rb8zmqjngbq2vzm6gx39jznlh9lksp3msz07")))

(define-public crate-tge-0.0.1 (c (n "tge") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 2)) (d (n "fontdue") (r "^0.0.1") (d #t) (k 0)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 0)) (d (n "glam") (r "^0.8.6") (d #t) (k 0)) (d (n "glow") (r "^0.4.0") (d #t) (k 0)) (d (n "glutin") (r "^0.23.0") (d #t) (k 0)) (d (n "image") (r "^0.23.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "rodio") (r "^0.10.0") (d #t) (k 0)) (d (n "winit") (r "^0.21.0") (d #t) (k 0)))) (h "1b040sx1qa9z708azaaj99jnn5gkildc5j73ksw1qhlvz09lqila")))

(define-public crate-tge-0.0.2 (c (n "tge") (v "0.0.2") (d (list (d (n "ab_glyph") (r "^0.2.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.13") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 0)) (d (n "glam") (r "^0.9.2") (d #t) (k 0)) (d (n "glow") (r "^0.5.0") (d #t) (k 0)) (d (n "glutin") (r "^0.24.1") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "0bbxf3c0540ajyz0jzckx4rhmvd1m7qyiff3sy661h0a8ir61nmz")))

(define-public crate-tge-0.0.3 (c (n "tge") (v "0.0.3") (d (list (d (n "ab_glyph") (r "^0.2.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 2)) (d (n "gilrs") (r "^0.7.4") (d #t) (k 0)) (d (n "glam") (r "^0.9.3") (d #t) (k 0)) (d (n "glow") (r "^0.5.0") (d #t) (k 0)) (d (n "glutin") (r "^0.24.1") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "winit") (r "^0.22.2") (d #t) (k 0)))) (h "0z46a3j2x47whlmv9v8xm5bly004mp6l7z3zb15q6lz5p77l5vl2")))

(define-public crate-tge-0.0.4 (c (n "tge") (v "0.0.4") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 2)) (d (n "fontdue") (r "^0.6.2") (d #t) (k 0)) (d (n "gilrs") (r "^0.8.1") (d #t) (k 0)) (d (n "glam") (r "^0.19.0") (d #t) (k 0)) (d (n "glow") (r "^0.11.0") (d #t) (k 0)) (d (n "glutin") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "winit") (r "^0.25.0") (d #t) (k 0)))) (h "13jzc077yp5jn8jf8irwcwmwh6fvpcl8bq8lx6v5sqbmdj32qxvn")))

