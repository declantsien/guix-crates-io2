(define-module (crates-io #{3}# t trk) #:use-module (crates-io))

(define-public crate-trk-0.2.0 (c (n "trk") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "drawille") (r "^0.2") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "read_input") (r "^0.5.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "19v4176bxhwfi6cjixblcw47kabmkrzblwzvkzkpjrad7y6lal0p")))

