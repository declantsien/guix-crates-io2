(define-module (crates-io #{3}# t tbm) #:use-module (crates-io))

(define-public crate-tbm-0.1.0 (c (n "tbm") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "oxidebpf") (r "^0.1.0") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)))) (h "0n98d1a1yjxixb9jvm05fp87vvbydw0rdj64wp282fybp4f4rp4y")))

(define-public crate-tbm-0.1.1 (c (n "tbm") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "oxidebpf") (r "^0.1.0") (d #t) (k 0)) (d (n "tui") (r "^0.16") (f (quote ("crossterm"))) (k 0)))) (h "17rgd4xdwbnwma2afxjlxh8axszx87mp5cmklmynxlh7cvs2fhzh")))

