(define-module (crates-io #{3}# t tia) #:use-module (crates-io))

(define-public crate-tia-1.0.0 (c (n "tia") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xxzam0q4nhfppaz5zr8gxfvms9gg8n4iir9cm2qja9d24xh2ih5") (f (quote (("print") ("include-pretty") ("include-force") ("include") ("file-pretty") ("file") ("disable") ("default"))))))

(define-public crate-tia-1.0.1 (c (n "tia") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.60") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12hqyf2d4hzmf8vdsia5w9cp346il5mjvgna154pifn95v7wxi0x") (f (quote (("print") ("include-pretty") ("include-force") ("include") ("file-pretty") ("file") ("disable") ("default"))))))

(define-public crate-tia-1.0.2 (c (n "tia") (v "1.0.2") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gm1zy6ykppzlw1gid16awz7y0qckdwz6a5c6mdgcn7hs8j57dpn") (f (quote (("print") ("include-pretty") ("include-force") ("include") ("file-pretty") ("file") ("disable") ("default"))))))

(define-public crate-tia-1.0.3 (c (n "tia") (v "1.0.3") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1280lri5rbrk7kwyssxpchgp4asqlk9q06q4c0377jpvzjm1rjnm") (f (quote (("print") ("include-pretty") ("include-force") ("include") ("file-pretty") ("file") ("disable") ("default"))))))

