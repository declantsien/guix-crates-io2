(define-module (crates-io #{3}# t tls) #:use-module (crates-io))

(define-public crate-tls-0.0.1 (c (n "tls") (v "0.0.1") (h "1y31why3y6yx02r5rrbzw5zhzkr8376saa74c4i4sxawh4p4b3yb")))

(define-public crate-tls-0.0.2 (c (n "tls") (v "0.0.2") (h "17779gkzk86r53f001a5cln9flbcgh41r5qbvjdy3n7qsr6px3dq")))

(define-public crate-tls-0.0.3 (c (n "tls") (v "0.0.3") (h "1nb769flg379xjcqjmaz7c5ay370r49drv97ihji6hdh3nm2m9lg")))

