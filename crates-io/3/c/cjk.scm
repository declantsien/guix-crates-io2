(define-module (crates-io #{3}# c cjk) #:use-module (crates-io))

(define-public crate-cjk-0.0.1 (c (n "cjk") (v "0.0.1") (h "0fpv2fmpbqlmyz3vn4kx1i6jh0s8nkp9lsbs217kgl3h59d2wa5q")))

(define-public crate-cjk-0.0.2 (c (n "cjk") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1lxm67waqrxzxy3fv9l3wmf96rvn16yrjp24lmwajy7d12zjia3k")))

(define-public crate-cjk-0.0.3 (c (n "cjk") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0xkd5893nd9y8fndz6iakm5mh0151zvb39rk12q4jc9mlj307704")))

(define-public crate-cjk-0.0.4 (c (n "cjk") (v "0.0.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0rid9rpamcha9v6qv1fg6cg953jwgxmz9l8nrjxpc51k0r5v7lb5")))

(define-public crate-cjk-0.0.5 (c (n "cjk") (v "0.0.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0aijxdvb3lkzwrkji6iasmydag3g8mwzr4hyy9appv8gpk1jwbsr")))

(define-public crate-cjk-0.0.6 (c (n "cjk") (v "0.0.6") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1hpqx0cb5lmybsahv1ig8gjzjb3hk2dl6wc9ij49avrxdqjc86jm")))

(define-public crate-cjk-0.0.7 (c (n "cjk") (v "0.0.7") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "12c9j6q15k5mv40d7bvss3jy1dfz54m6qbch0p1vp47i2hc72whw")))

(define-public crate-cjk-0.0.8 (c (n "cjk") (v "0.0.8") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "15x1sqf86pxb24zyapdx1zf37dzyxl6pkm1j83swgmfi4a3cwc09")))

(define-public crate-cjk-0.0.9 (c (n "cjk") (v "0.0.9") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0f7mk5r1ahzqklwmpxn0n25191hi16g5svczf9cdwszgmj8dn7vh")))

(define-public crate-cjk-0.0.10 (c (n "cjk") (v "0.0.10") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0sj39za2pr4qw2277sjqw1j23k9n5q6k948a4fn88884h8l27wjm")))

(define-public crate-cjk-0.0.11 (c (n "cjk") (v "0.0.11") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0hckjd3kqbqihcy7fxfqhghmgpz5q2k84d0jx86vrx374rf47d5a")))

(define-public crate-cjk-0.0.12 (c (n "cjk") (v "0.0.12") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "188pyc55k79n68b9jmpjzrqfwdawsj2g7ianydy69g1j39nf9dl7")))

(define-public crate-cjk-0.0.13 (c (n "cjk") (v "0.0.13") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0zz5hlbn0l66g14ryzp52cv8jcc172xh6wx277djh14jkb5cv1bs")))

(define-public crate-cjk-0.0.14 (c (n "cjk") (v "0.0.14") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1mdscyw6mqkdllkic05dg0xh8cfh2jdlc6c6sbrxx6glcijyspj9")))

(define-public crate-cjk-0.0.15 (c (n "cjk") (v "0.0.15") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "05pw9jk8pgscli9r7hr205f2vbskx3a9dbfs4fi7ixswan9y9qi8")))

(define-public crate-cjk-0.1.0 (c (n "cjk") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "16lbjy4ad70l24hyjn00q9msq5ini8alih95p1hrscr7sixgqwhz")))

(define-public crate-cjk-0.1.1 (c (n "cjk") (v "0.1.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0rwi49bhbq416xfabf4pnrkxnnj1bpamqkb1mnmfzalgjs7bnv4h")))

(define-public crate-cjk-0.1.3 (c (n "cjk") (v "0.1.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0xa4fwh30ali51a8pysah0vcvg30l6ilw8hihqcahprl5jq1430r")))

(define-public crate-cjk-0.1.5 (c (n "cjk") (v "0.1.5") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0rr73wky33k4k7a4rr1vqrq7982kvv1z0kw5l43jp4cfyip6a06k")))

(define-public crate-cjk-0.1.6 (c (n "cjk") (v "0.1.6") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "17qy2zbxbagsb925q4glywl3m92h5n7rs87bh162w2v47jgvmfq5")))

(define-public crate-cjk-0.1.7 (c (n "cjk") (v "0.1.7") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "17g5qwqgg9i7v5y53zw5gx8ag1v796s34432x5dn2hpl44nvwgnp")))

(define-public crate-cjk-0.1.8 (c (n "cjk") (v "0.1.8") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0v9dwrqx0gksgbjnbivsbwp9i4gl4da01h1dqjlv3ab941fy2w3k")))

(define-public crate-cjk-0.1.9 (c (n "cjk") (v "0.1.9") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1jizdxdaxip2wd9w3m58fqsic85qhy92kjla6g2b1gxkpwd7hz2r")))

(define-public crate-cjk-0.1.10 (c (n "cjk") (v "0.1.10") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1a5p002bc37llhc853xhlpfm7my65jn036x7s7dbisv35zlqfs5d")))

(define-public crate-cjk-0.1.11 (c (n "cjk") (v "0.1.11") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0iia24zl9klmjkpj7q895gqvzzdlg40p2fmjqn12sklpcrgjxrbz")))

(define-public crate-cjk-0.1.12 (c (n "cjk") (v "0.1.12") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "04x7f45mc9qp7dmnpz8xkxcwy19rq09dyrgixbnvlk8jsj4sia5k")))

(define-public crate-cjk-0.2.0 (c (n "cjk") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0p2nqvwcz2x24x2ykpgrm71a1dspyhyg9nfmmqijmawb7qcb8m8w")))

(define-public crate-cjk-0.2.1 (c (n "cjk") (v "0.2.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "1a9w1r5zizjr1x7g7ivmpc6zpfm82gdm92s1xd9v2zq0mdzx6md4")))

(define-public crate-cjk-0.2.2 (c (n "cjk") (v "0.2.2") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "01fdf8qiy9x1jp9c95byvig3hbmmq90a6n6d2q3k95g13mhvv00d")))

(define-public crate-cjk-0.2.3 (c (n "cjk") (v "0.2.3") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0ijmvba9375b6cayqy9gzlbg0f2rnc5xqj9yc5i6lc91433z492c")))

(define-public crate-cjk-0.2.4 (c (n "cjk") (v "0.2.4") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "unicode-blocks") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "01lfqsdhcns39jd7czqpf5qnsmbasiwsf0ksq8v8w31mx35y9c5x")))

(define-public crate-cjk-0.2.5 (c (n "cjk") (v "0.2.5") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "unicode-blocks") (r "^0.1.1") (d #t) (k 0)) (d (n "widestring") (r "^0.4") (d #t) (k 0)))) (h "0msjv9zqjxy6pk3fbrwksvwvqs0wdjawik494wrifcl7zyqq2mcc")))

