(define-module (crates-io #{3}# c cbm) #:use-module (crates-io))

(define-public crate-cbm-0.1.0 (c (n "cbm") (v "0.1.0") (d (list (d (n "clap") (r "~2.31.2") (d #t) (k 0)) (d (n "memmap") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.2") (d #t) (k 2)))) (h "10sgj8qm7qg30ygfvdh8v66z46g53xv97q23g125qbs8jmdq5jga")))

