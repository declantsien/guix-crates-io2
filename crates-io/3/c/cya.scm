(define-module (crates-io #{3}# c cya) #:use-module (crates-io))

(define-public crate-cya-0.0.1 (c (n "cya") (v "0.0.1") (h "05wajg5dy288jmvfiqypkh388f5dwicpzvq5dg6d9vgjmqc200ah")))

(define-public crate-cya-0.0.2 (c (n "cya") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "023639j204vrj6mqgmb9wg472w19jxqd5j0y7f8mm2j8jl2p59lp")))

(define-public crate-cya-0.0.3 (c (n "cya") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1la1ydkqdck1x4skgpizh3qsilkdzh3q33350isd8ipjdrhm69vv")))

(define-public crate-cya-0.0.4 (c (n "cya") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "average") (r "^0.10.6") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "1fmgxalbzza73bdqgjzjkrkl2hsx1aqw9hw0449h62mfv6844wg5")))

