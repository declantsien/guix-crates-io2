(define-module (crates-io #{3}# c cni) #:use-module (crates-io))

(define-public crate-cni-0.1.0 (c (n "cni") (v "0.1.0") (h "12jypr3dbmp1h09pi75r3xp9m3iphpp739n5ragks355xv7xdvcg") (y #t)))

(define-public crate-cni-0.1.1 (c (n "cni") (v "0.1.1") (h "1l2mrk3z2gqpwpd6mdy4gf6mmg4h30izfwh0ddwsh3bk78c8dv15")))

(define-public crate-cni-0.1.3 (c (n "cni") (v "0.1.3") (d (list (d (n "ipnetwork") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "typetag") (r "^0.1.5") (d #t) (k 0)))) (h "0l4kg04pk131p2pk3bfnlgvy1rpfrabcr76wgs91niv8rgnmjc31")))

