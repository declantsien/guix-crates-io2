(define-module (crates-io #{3}# c cfb) #:use-module (crates-io))

(define-public crate-cfb-0.1.0 (c (n "cfb") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "1qfgnkcc4z4kg9vkxm06v0vx3llccz1l8ghpawn4hakgcj03k64f")))

(define-public crate-cfb-0.2.0 (c (n "cfb") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.26") (d #t) (k 2)))) (h "11jpp7pazrw3z576ywcm39dyvlgnjx3b9d14livq2ip34px13md4")))

(define-public crate-cfb-0.3.0 (c (n "cfb") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.26") (d #t) (k 2)) (d (n "uuid") (r "^0.5") (d #t) (k 0)))) (h "0jdlnlhgsagl4v02zjy7ad6raw5s69fg7b7d1w9z010lsadng9ks")))

(define-public crate-cfb-0.3.1 (c (n "cfb") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.27") (d #t) (k 2)) (d (n "uuid") (r "^0.5") (d #t) (k 0)))) (h "0rxn4n7z3vfbxzrfgszl3w298c9drpvf24ma0s896kmmp5sv0zwf")))

(define-public crate-cfb-0.4.0 (c (n "cfb") (v "0.4.0") (d (list (d (n "byteorder") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "chrono") (r ">=0.4.0, <0.5.0") (d #t) (k 2)) (d (n "clap") (r ">=2.27.0, <3.0.0") (d #t) (k 2)) (d (n "uuid") (r ">=0.8.1, <0.9.0") (d #t) (k 0)))) (h "186b7pl3vd37s0jyw9x8a9i1dnpy32ipcw5l9q7jy6vi4j33wifa")))

(define-public crate-cfb-0.5.0 (c (n "cfb") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.27") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "040v5498f0l1h1dz417l0n1rs7aqxr9rc7mpc8yf9mz1vinw7dw9")))

(define-public crate-cfb-0.6.0 (c (n "cfb") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.27") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "1l1mibnciyfszbzxbhr96pi80h10gh7cxy14qdkfl8x8z6crlc07")))

(define-public crate-cfb-0.6.1 (c (n "cfb") (v "0.6.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "clap") (r "^2.27") (d #t) (k 2)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "172zg3wpynhfx802f8lfpc0jl1l1agv1f2dpj5gz3qwrhwj9vy3l")))

(define-public crate-cfb-0.7.0 (c (n "cfb") (v "0.7.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "05hh28hwwyxv5hibap9qblq8fqxr3xpndnk7zqdrdycn5n1wrxi5")))

(define-public crate-cfb-0.7.1 (c (n "cfb") (v "0.7.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "05s5fs3c4a5wfiqzn7swagws0d96q8mrmip45hrd6yjym45wfss6")))

(define-public crate-cfb-0.7.2 (c (n "cfb") (v "0.7.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "01h6dq77xdwy478mix74ls38jf8vzsvdalx54as4ynvzcjlnxzqr")))

(define-public crate-cfb-0.7.3 (c (n "cfb") (v "0.7.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "03y6p3dlm7gfds19bq4ba971za16rjbn7q2v0vqcri52l2kjv3yk")))

(define-public crate-cfb-0.8.0 (c (n "cfb") (v "0.8.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0yk7lbixa8r9iz5w3m7zrdc7la28icvk15qkpi141z529wlw3nrl")))

(define-public crate-cfb-0.8.1 (c (n "cfb") (v "0.8.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.27") (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0c1jry5manqy73asxl26n0gwkqpfpn6xlabzc880875d8sha3ngk")))

(define-public crate-cfb-0.9.0 (c (n "cfb") (v "0.9.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "07wv6imgki4bami8k64bq7k2841r1kyzfa9lf6mg2013j4z7k45k")))

(define-public crate-cfb-0.10.0 (c (n "cfb") (v "0.10.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0hfscapkl295ffqlzj2arwbyrxlpm831zkygz9wb68z3bgjzi96q")))

