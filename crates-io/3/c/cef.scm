(define-module (crates-io #{3}# c cef) #:use-module (crates-io))

(define-public crate-cef-0.1.0 (c (n "cef") (v "0.1.0") (h "1wcn5vprf71kmvcxxcqj8n0ar37f2fbq4xska1qwqbf12mkw8c3l")))

(define-public crate-cef-0.1.1 (c (n "cef") (v "0.1.1") (d (list (d (n "cef-sys") (r "^1.0") (d #t) (k 0) (p "libcef-sys")) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "10k8rvj5418ym00fynd5mh0axz2ws7lpp1k5i359mdz7w2vdcbsa") (f (quote (("dox" "cef-sys/dox"))))))

(define-public crate-cef-0.1.2 (c (n "cef") (v "0.1.2") (d (list (d (n "cef-sys") (r "^1.0") (d #t) (k 0) (p "libcef-sys")) (d (n "widestring") (r "^1.0") (d #t) (k 0)))) (h "0mmhdjydy3fnpndq54dj7jv7chk4jw22p3q9k3civ2pqz7lxgqq3") (f (quote (("dox" "cef-sys/dox"))))))

(define-public crate-cef-117.2.5 (c (n "cef") (v "117.2.5") (d (list (d (n "cef-sys") (r "^117.2.5") (d #t) (k 0) (p "libcef-sys")) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "05wbijbznsbgcj7qkmz13dd1w801xgyc3flmiql02bphznv92ivr") (f (quote (("dox" "cef-sys/dox"))))))

(define-public crate-cef-117.2.6 (c (n "cef") (v "117.2.6") (d (list (d (n "cef-sys") (r "^117.2.5") (d #t) (k 0) (p "libcef-sys")) (d (n "widestring") (r "^1.0") (d #t) (k 0)) (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "08in0vidcdwkd52n7jnjv1y1lmyhb0jfviaxs49izil3ripjkzfv") (f (quote (("dox" "cef-sys/dox"))))))

