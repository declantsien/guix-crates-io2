(define-module (crates-io #{3}# c cow) #:use-module (crates-io))

(define-public crate-cow-0.0.1 (c (n "cow") (v "0.0.1") (h "1nl3808xgglj93kw4j913cwqfyvidc50nvbxvpl8gqk7wx8603an")))

(define-public crate-cow-0.0.2 (c (n "cow") (v "0.0.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1q6g3fl9izzzybhd2wa51wzzshqfrm4nwnm5gczqdlj22lv05g25")))

(define-public crate-cow-0.0.3 (c (n "cow") (v "0.0.3") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "186xam7i4qd6vh8038n6d6nv4v7sws37lhk0d9i4z6vqf176s31m")))

(define-public crate-cow-0.0.4 (c (n "cow") (v "0.0.4") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1xz3awm5g1w52qyssf4qcszr502zraq6jpgc7093ax68iq99g7rs")))

(define-public crate-cow-0.0.5 (c (n "cow") (v "0.0.5") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0fwx68481cd7749is5k36knzpabcih8mb0lsgrgfvh0a8hnk0vj4")))

(define-public crate-cow-0.0.6 (c (n "cow") (v "0.0.6") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "10qa5mz0qbii7avh9b10l54ilxfhw0bn40lpphh0adq9rq7131mg")))

(define-public crate-cow-0.0.7 (c (n "cow") (v "0.0.7") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "17ggacyrk5gd27i38c68azrddx5dsjkwgqmxvkij21s12m7bl5p5")))

(define-public crate-cow-0.0.8 (c (n "cow") (v "0.0.8") (d (list (d (n "rand") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0c42dd5hhdzdy020jglkvmgzi5zrhbxwhrx1c9ymqgy81i0v070v")))

