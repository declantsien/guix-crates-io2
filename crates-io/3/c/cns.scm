(define-module (crates-io #{3}# c cns) #:use-module (crates-io))

(define-public crate-cns-0.1.0 (c (n "cns") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "crates_io_api") (r "^0.7.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "curl") (r "^0.4.34") (d #t) (k 0)) (d (n "strip_markdown") (r "^0.2.0") (d #t) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm"))) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0ds11smnz97ifmnvy9c6f38ywg8lnh3pvj7ham3p4mbi2nr6myl0")))

(define-public crate-cns-0.1.1 (c (n "cns") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "consecrates") (r "^0.1.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.0") (d #t) (k 0)) (d (n "http_req") (r "^0.7.2") (f (quote ("rust-tls"))) (k 0)) (d (n "tui") (r "^0.14") (f (quote ("crossterm"))) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1gww9x8cv0fqpxrajgpb0j5g9r6z2p2196798g8zyis9v14zgq02") (f (quote (("default" "clipboard"))))))

