(define-module (crates-io #{3}# c cb2) #:use-module (crates-io))

(define-public crate-cb2-0.1.0 (c (n "cb2") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "cb2_core") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.13") (d #t) (k 0)))) (h "0d9hf4bnl41l3f2xzbjih6qbj0sw3qmssddqlva186lhzbqc9ysl")))

(define-public crate-cb2-0.1.1 (c (n "cb2") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "cb2_core") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.13") (d #t) (k 0)))) (h "08qn472g2ksvzmyyprnwvn3s741cfwmq3q086vdpx5xbj008dyj5")))

(define-public crate-cb2-0.1.2 (c (n "cb2") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "cb2_core") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.25") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1.13") (d #t) (k 0)))) (h "0fhxpaw11nyhyfbvjzfyrxhv977wv3md27qmkfalfajzg3q91k9q")))

