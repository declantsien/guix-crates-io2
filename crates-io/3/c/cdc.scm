(define-module (crates-io #{3}# c cdc) #:use-module (crates-io))

(define-public crate-cdc-0.0.2 (c (n "cdc") (v "0.0.2") (h "1jj4zijp5q6v4mh2nrfdq3bxz911hq2771rq2sa93m5q48pqslhs")))

(define-public crate-cdc-0.1.0 (c (n "cdc") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 2)) (d (n "ring") (r "^0.12.1") (d #t) (k 2)))) (h "0m5d4nb8j0q7563bbmi61xinr58fg1canip3pka3sjkmjzgv6fj3")))

(define-public crate-cdc-0.1.1 (c (n "cdc") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "ring") (r "^0.16.1") (d #t) (k 2)))) (h "0nqm3kgvp8abq7d3arz92ga9nrs2g0ys4bgpjap1qgcmd1gna8gl")))

