(define-module (crates-io #{3}# c crc) #:use-module (crates-io))

(define-public crate-crc-0.0.1 (c (n "crc") (v "0.0.1") (h "1cwh1mlasl6qqfrqfvabvxjhis7vl7m3rdfyrpmb7rr4fi81pwzx")))

(define-public crate-crc-0.1.0 (c (n "crc") (v "0.1.0") (h "1ldbv2bdfcqikazdmjwcsx78alx2a52rzasv3pdq7l4axaryqj25")))

(define-public crate-crc-0.1.1 (c (n "crc") (v "0.1.1") (h "116f9w9pfprwk4zjlskf9r27nszrk3cimqj63z2npjfnds1zzwyp")))

(define-public crate-crc-0.2.0 (c (n "crc") (v "0.2.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "0cdxny1izbap6c2xsn4xfg7378gziwkb7lxdnvr5rmrz87lmq5zd")))

(define-public crate-crc-0.3.0 (c (n "crc") (v "0.3.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "056wpy4mv47lvhymm3m4nxbvkg5icz60vwpx25hp1zwcs3xxg1mx")))

(define-public crate-crc-0.3.1 (c (n "crc") (v "0.3.1") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "0symszqg1bfx1q0zz8jiql3h3zs8gk3689r94vnjrsy11zinmpww")))

(define-public crate-crc-1.0.0 (c (n "crc") (v "1.0.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "0naicpcy4j500ir1a0wzq5zqabk8ys8ar4r371fwf8a9qr2ak048")))

(define-public crate-crc-1.1.0 (c (n "crc") (v "1.1.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "1hpv80m48zir39d5brcf9q4ibf344zb9w8dwbi7daw6hjmbxlyla")))

(define-public crate-crc-1.2.0 (c (n "crc") (v "1.2.0") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)))) (h "0yrbb97kg104zqnp3az7w462wdv4qamw279sifn4sah96r4pl6jl")))

(define-public crate-crc-1.3.0 (c (n "crc") (v "1.3.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "02n9xnmifgc24ah302c979992bnwgh8yjx7mnv6y4920wxcr2gra")))

(define-public crate-crc-1.4.0 (c (n "crc") (v "1.4.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1c0igpvavs673vj1yqpa6z01f3i7zkjaa2qg693y72piwvx186dw")))

(define-public crate-crc-1.5.0 (c (n "crc") (v "1.5.0") (d (list (d (n "build_const") (r "^0.2") (k 0)) (d (n "build_const") (r "^0.2") (d #t) (k 1)) (d (n "crc-core") (r "^0.1") (d #t) (k 0)) (d (n "crc-core") (r "^0.1") (d #t) (k 1)))) (h "16p7c5cpwgilz8ff32ibwd0vszq0anyg677y3iz0ywjyw6h9x9pv") (f (quote (("std") ("default" "std"))))))

(define-public crate-crc-1.6.0 (c (n "crc") (v "1.6.0") (d (list (d (n "build_const") (r "^0.2") (k 0)) (d (n "build_const") (r "^0.2") (d #t) (k 1)))) (h "002glgkmigq54is0l4892zhyq72z9nrig0cmfv9wgyqfqj3sdm34") (f (quote (("std") ("default" "std"))))))

(define-public crate-crc-1.7.0 (c (n "crc") (v "1.7.0") (d (list (d (n "build_const") (r "^0.2") (d #t) (k 1)))) (h "1my1vp28sqc9f7dljqsw0y4kwzs5qaxh17nn7qwnigf6mb004pdx") (f (quote (("std") ("default" "std"))))))

(define-public crate-crc-1.8.0 (c (n "crc") (v "1.8.0") (d (list (d (n "build_const") (r "^0.2") (d #t) (k 1)))) (h "15swf622034wxh5xc76w75vyikvhaqm0cwiv7hznjl2yrc30f6l0") (f (quote (("std") ("default" "std"))))))

(define-public crate-crc-1.8.1 (c (n "crc") (v "1.8.1") (d (list (d (n "build_const") (r "^0.2") (d #t) (k 1)))) (h "1sqal6gm6lbj7f45iv3rw2s9w3pvvha8v970y51s7k7mwy6m8qyn") (f (quote (("std") ("default" "std"))))))

(define-public crate-crc-1.9.0 (c (n "crc") (v "1.9.0") (d (list (d (n "build_const") (r "^0.2") (d #t) (k 1)) (d (n "criterion") (r "^0.2.5") (d #t) (k 2)))) (h "1nwgidcckh1gkndhi0c0zry3qv6ly2z6fp8fgy8q5cfca9jcma50") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-crc-2.0.0-rc.2 (c (n "crc") (v "2.0.0-rc.2") (d (list (d (n "crc-catalog") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0w0iwwidakc3fc9snb05n8rnb6jxc3zmdifbgnzy1wdrdv9c1qni") (y #t)))

(define-public crate-crc-2.0.0-rc.1 (c (n "crc") (v "2.0.0-rc.1") (d (list (d (n "crc-catalog") (r "^1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "024fn8yjpdajz6ijnv3mxjfl4d4dfzqybvbrjy0d4059jn62bs6y")))

(define-public crate-crc-2.0.0 (c (n "crc") (v "2.0.0") (d (list (d (n "crc-catalog") (r "^1.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "17w853d05xr823awmbjbpj5c6vjxm0rxj71fhykhh0a6jlkp5hhh")))

(define-public crate-crc-2.1.0 (c (n "crc") (v "2.1.0") (d (list (d (n "crc-catalog") (r "^1.1.1") (d #t) (k 0)))) (h "08qfahmly0n5j27g1vkqx9s6mxhm8k4dsp61ykskazyabdlrmz29")))

(define-public crate-crc-3.0.0 (c (n "crc") (v "3.0.0") (d (list (d (n "crc-catalog") (r "^2.1.0") (d #t) (k 0)))) (h "1cqa2j5cqmzvyq978brzynrpm8fillrdfn1lp1w6rhcnnl97sxak")))

(define-public crate-crc-3.0.1 (c (n "crc") (v "3.0.1") (d (list (d (n "crc-catalog") (r "^2.1.0") (d #t) (k 0)))) (h "1zkx87a5x06xfd6xm5956w4vmdfs0wcxpsn7iwj5jbp2rcapmv46")))

(define-public crate-crc-3.1.0-beta.1 (c (n "crc") (v "3.1.0-beta.1") (d (list (d (n "crc-catalog") (r "^2.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1av5xknzp874xb24cf9vdxlldk5y7yvjaw3pj7cljc5ayvlnwb50") (f (quote (("slice16-mem-limit") ("no-table-mem-limit") ("bytewise-mem-limit")))) (y #t) (r "1.56")))

(define-public crate-crc-3.1.0 (c (n "crc") (v "3.1.0") (d (list (d (n "crc-catalog") (r "^2.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1xkrddvq1fiq3zrpjzc61cvzbs2kgrchp3ip6b0hp6al17gan6xg") (y #t) (r "1.56")))

(define-public crate-crc-3.1.0-beta.2 (c (n "crc") (v "3.1.0-beta.2") (d (list (d (n "crc-catalog") (r "^2.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "134ignk4i3jm8r40sk0rw25a4qbdass41m57pixvwh45wzzdbvf1") (y #t) (r "1.65")))

(define-public crate-crc-3.2.0 (c (n "crc") (v "3.2.0") (d (list (d (n "crc-catalog") (r "^2.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1g76wwpyfqby0rhqc41i1988qlfmqdzfspr4pa6ny4qmcv2k5d62") (y #t) (r "1.65")))

(define-public crate-crc-3.2.1 (c (n "crc") (v "3.2.1") (d (list (d (n "crc-catalog") (r "^2.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "0dnn23x68qakzc429s1y9k9y3g8fn5v9jwi63jcz151sngby9rk9") (r "1.65")))

