(define-module (crates-io #{3}# c chr) #:use-module (crates-io))

(define-public crate-chr-0.0.0 (c (n "chr") (v "0.0.0") (h "1k6r5v5n7mannyj7rxlb7qd90zc05m1vj8b4wdqqiwhprd60597z") (y #t)))

(define-public crate-chr-1.0.0 (c (n "chr") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^1.0.2") (d #t) (k 2)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.5") (d #t) (k 1)) (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 2)) (d (n "minus") (r "^2.1.0") (f (quote ("static_output"))) (d #t) (k 0)) (d (n "predicates") (r "^1.0.6") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.10") (f (quote ("blocking"))) (d #t) (k 1)) (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.24.2") (f (quote ("bundled"))) (d #t) (k 1)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 1)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "strum") (r "^0.20.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20.1") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 0)) (d (n "zip") (r "^0.5.9") (d #t) (k 1)))) (h "0661jsnivifjdl5ii2vp9m0qyg7v3sfpbr61aj5vvw5m05avdz7m")))

