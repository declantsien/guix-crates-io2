(define-module (crates-io #{3}# c cw3) #:use-module (crates-io))

(define-public crate-cw3-0.2.2 (c (n "cw3") (v "0.2.2") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "cw0") (r "^0.2.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0zvf8wq3pg895c4dvs2p53bbh6lzb7n0z1hw36qr1y72miw12pgf")))

(define-public crate-cw3-0.2.3 (c (n "cw3") (v "0.2.3") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "cw0") (r "^0.2.3") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0ppjvisx1sikv8ci07vzyl9fhvwdacdn3nla791dllzm9v5kjvhh")))

(define-public crate-cw3-0.3.0 (c (n "cw3") (v "0.3.0") (d (list (d (n "cosmwasm-schema") (r "^0.11.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.11.0") (d #t) (k 0)) (d (n "cw0") (r "^0.3.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "10p2qg4v46h2v7mncx8p2jh3y2kkl2pllkfdrbwr3qi4zdrzxlm0")))

(define-public crate-cw3-0.3.1 (c (n "cw3") (v "0.3.1") (d (list (d (n "cosmwasm-schema") (r "^0.11.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "cw0") (r "^0.3.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1ngc4vml75wn6r8vi3zdhbvkcrx0zy09zx2gcfnr51k4kcgizxzi")))

(define-public crate-cw3-0.3.2 (c (n "cw3") (v "0.3.2") (d (list (d (n "cosmwasm-schema") (r "^0.11.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "cw0") (r "^0.3.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0qqql5qvyzxg691bqdcrr3h32j5fzzfr4fmjx0wn6yvf4bh5nq6v")))

(define-public crate-cw3-0.4.0 (c (n "cw3") (v "0.4.0") (d (list (d (n "cosmwasm-schema") (r "^0.12.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.12.2") (d #t) (k 0)) (d (n "cw0") (r "^0.4.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "15g9i4bmgkb159kc9iyi5d3b73fpgmw6cgc23n96acbh7v3imyrs")))

(define-public crate-cw3-0.5.0 (c (n "cw3") (v "0.5.0") (d (list (d (n "cosmwasm-schema") (r "^0.13.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.13.2") (d #t) (k 0)) (d (n "cw0") (r "^0.5.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1mkd504ls51yss2wyc2xb321vmq8pfjr4baxh7xi5yai8d1q40ln")))

(define-public crate-cw3-0.6.0-alpha1 (c (n "cw3") (v "0.6.0-alpha1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-alpha1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1lh567kmqb6y9x8w02krdd23finzx7chzdi1dn1xl925306q547w")))

(define-public crate-cw3-0.6.0-alpha2 (c (n "cw3") (v "0.6.0-alpha2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-alpha2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0bidp5cjrg9cmgrf5rjw5drichfy7kinjz0gf5aml1vm6nh09kky")))

(define-public crate-cw3-0.6.0-alpha3 (c (n "cw3") (v "0.6.0-alpha3") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-alpha3") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "07fnvcwqpm720l9s6nvkpw1yvni5xxfi37i3fjfmwfggdwynz5yw")))

(define-public crate-cw3-0.6.0-beta1 (c (n "cw3") (v "0.6.0-beta1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0j28blzjzkhvilg0jhacsdi0av5nb59q4rfrw03jhdjfxdzszmw5")))

(define-public crate-cw3-0.6.0-beta2 (c (n "cw3") (v "0.6.0-beta2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-beta2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1gr3cxw22j748jf89mwjszc9i3xq6iy8amf28s88yjj9m496r0f6")))

(define-public crate-cw3-0.6.0-beta3 (c (n "cw3") (v "0.6.0-beta3") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta5") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1280k8cqi64iz8lcmgvcah6z6gzdd3y6ks07bmrjvz3ni43ccz32")))

(define-public crate-cw3-0.6.0 (c (n "cw3") (v "0.6.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw0") (r "^0.6.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0f7lnvyk1df4440fi0iwziq48z6dbs70bmg7n9zm0cd7xzn0jiyk")))

(define-public crate-cw3-0.6.1 (c (n "cw3") (v "0.6.1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw0") (r "^0.6.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "110a8xywcllf4b03ipchzs57vlyghhnvzvzmm9n1v8m3rjs746x8")))

(define-public crate-cw3-0.6.2 (c (n "cw3") (v "0.6.2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw0") (r "^0.6.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1b406zrygfw38npzzsnqrvy9idj4id04rainikrr546f41cqf2k0")))

(define-public crate-cw3-0.7.0 (c (n "cw3") (v "0.7.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.15.0") (d #t) (k 0)) (d (n "cw0") (r "^0.7.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1dazxxnqmklransw82glmgfi1qzzk27vah4accdm30y214nxs84z")))

(define-public crate-cw3-0.8.0-rc1 (c (n "cw3") (v "0.8.0-rc1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0-rc5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0-rc5") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0-rc1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "00gya781cs86wfabr5ifjhhcnmcal59nixvpvmm8h2lnw7c9wqpn")))

(define-public crate-cw3-0.8.0-rc2 (c (n "cw3") (v "0.8.0-rc2") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0-rc2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1pbnq114ddkdjjdzfa4n5v1k6z9dn509a5kjq6qq6wx63qmgfys6")))

(define-public crate-cw3-0.8.0-rc3 (c (n "cw3") (v "0.8.0-rc3") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0-rc3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1qwkh1f70w44iv421gli8kxcr98xj5hg9xkssgk4hfaczdnkqrnf")))

(define-public crate-cw3-0.8.0 (c (n "cw3") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "147vg4j4g2wxw1al7g8ihasy42880xjgmaz65qsy1v1s6dq42r2x")))

(define-public crate-cw3-0.8.1 (c (n "cw3") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.8.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "16ssrga03awjbmaa4di88mb8p6dvggmzb57k8fs76p3chyqwv2h7")))

(define-public crate-cw3-0.9.0 (c (n "cw3") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.9.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "15k8x2vf3kqnfbw872ylsv255glrphkz6ca3h1fkk5vzpd4caqh7")))

(define-public crate-cw3-0.10.0-soon (c (n "cw3") (v "0.10.0-soon") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "18c3pq2b4h0c2cwm8vypqympjck202ahlsvjc2b9vj5zb0dj0p5l")))

(define-public crate-cw3-0.9.1 (c (n "cw3") (v "0.9.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw0") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0jmf0rlx6519njybsd5dqp262882brpr4blq3kvi92s683ayc0l7")))

(define-public crate-cw3-0.10.0-soon2 (c (n "cw3") (v "0.10.0-soon2") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1p3hdzwypvymrsyficha87c934vq5q3chq2mnxp40jd30jsqkrwk")))

(define-public crate-cw3-0.10.0-soon3 (c (n "cw3") (v "0.10.0-soon3") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0mhbj2yrmfhvh86b0pwz2p51k1533chpp8ip1cmv8vcr145dhgpr")))

(define-public crate-cw3-0.10.0-soon4 (c (n "cw3") (v "0.10.0-soon4") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon2") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0-soon4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1zm7cwz44h4k8sz2vf4h0byi68217q03v1g7a614gvkvrabpx8qb")))

(define-public crate-cw3-0.10.0 (c (n "cw3") (v "0.10.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1i3kkm19ryfs0gza6gdab0jx9lmc0fnyvr2p8vdm9aapwbwv0lr2")))

(define-public crate-cw3-0.10.1 (c (n "cw3") (v "0.10.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0zhrq3zvmhqbs8f41shy0n5y6k8zsij3dabgxy5kq28y7lzh43di")))

(define-public crate-cw3-0.10.2 (c (n "cw3") (v "0.10.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0ff1lqg1z1k785vmhp80qzs2858fyngvwy89b12a3wwyxxn4kncq")))

(define-public crate-cw3-0.10.3 (c (n "cw3") (v "0.10.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw0") (r "^0.10.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1k3i1349qgvgrm8l8i5qkxcns2zm97way3vvk195ib3zgf68qk1x")))

(define-public crate-cw3-0.11.0 (c (n "cw3") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-utils") (r "^0.11.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1kailybip85zbycikdvq7yma13435wqf82psg6a1wk4741pxabal")))

(define-public crate-cw3-0.11.1 (c (n "cw3") (v "0.11.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-utils") (r "^0.11.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0dhrqmc8qg7aqh49f6albmqpsdlis0cwmpf1mydzw5zmfqrfd0kc")))

(define-public crate-cw3-0.12.0 (c (n "cw3") (v "0.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-utils") (r "^0.12.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1npi1i3a7z8in7d4b7b0ryndinkki31y8q90lyz7pzvw4y41xkgb")))

(define-public crate-cw3-0.12.1 (c (n "cw3") (v "0.12.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "cw-utils") (r "^0.12.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1q2icb9600kpnl7blyrkcxr8zp52im6nyqlpx8f0hn2c85in4yq1")))

(define-public crate-cw3-0.13.0 (c (n "cw3") (v "0.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1838qxkqpmrhpnzrr3mr8sdfkbkz0ngxgv2vnw94vq9hrq5mjmmm")))

(define-public crate-cw3-0.13.1 (c (n "cw3") (v "0.13.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0b6mwwyn9bghx6y6f320vl381q9kwn8scfgcr4r78qyhms3plz3v")))

(define-public crate-cw3-0.13.2 (c (n "cw3") (v "0.13.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta8") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1vs0w9vrnaj0r7jbr7b5v53b34rb44bzwvyijjq7nk55711qawgq")))

(define-public crate-cw3-0.13.3 (c (n "cw3") (v "0.13.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "06p38ncxs3iwhf2wm0i40vz2cij5azn1zf7glyw97vpw1wwfw9rj")))

(define-public crate-cw3-0.13.4 (c (n "cw3") (v "0.13.4") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "16wpmxhq5pwkr9zkl2ivdfdhvi801nwkqi6k346acjv4gwm4c6gy")))

(define-public crate-cw3-0.14.0 (c (n "cw3") (v "0.14.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1xsi41aggcwadamfr6mf9s5sfs46cfyvffz1z4xsvlwvlrb7yf1w")))

(define-public crate-cw3-0.15.0 (c (n "cw3") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1ypdsgr9q8s8p9wsmd7axlm8x69rq0xk6gzwyihvrqadw1cnj7wn")))

(define-public crate-cw3-0.15.1 (c (n "cw3") (v "0.15.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.15.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0wwgryqiagafn6217j45klhlmqny1m7hlljz6li91k0ggm2sm80b")))

(define-public crate-cw3-0.16.0 (c (n "cw3") (v "0.16.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0373dd405891rmm8scfg4mlj99vckrwlw7c5f9sgq9l8v0f92m20")))

(define-public crate-cw3-1.0.0 (c (n "cw3") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^0.16.0") (d #t) (k 0)) (d (n "cw20") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1i0j3rzkd359ccixg2jmlnmqddcvx12s2kp4rmwk7p3qhf8xrq8c")))

(define-public crate-cw3-1.0.1 (c (n "cw3") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1ykfbdgpdbb8ghb47bakvi3l7352j0cs4y955b6j38la023vbq1g")))

(define-public crate-cw3-1.1.0 (c (n "cw3") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "cw20") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1m9kgw59y9gg5ks96hazm0z6qbvx1h3zn6fqgmd81rkx2bcz66hp")))

(define-public crate-cw3-1.1.1 (c (n "cw3") (v "1.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "cw20") (r "^1.1.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0d20nmi7wdvc00y78iwsb1sxnh9kfqsrav6il555ain17v1nw18x")))

(define-public crate-cw3-1.1.2 (c (n "cw3") (v "1.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw-utils") (r "^1.0.1") (d #t) (k 0)) (d (n "cw20") (r "^1.1.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1593n7xhy4ai4ghl44z7jjbvvgm3hidf0j3ikvfjddnlfg8gnrr9")))

(define-public crate-cw3-2.0.0-rc.0 (c (n "cw3") (v "2.0.0-rc.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^2.0.0") (d #t) (k 0)) (d (n "cw20") (r "^2.0.0-rc.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "1whsh7k3512kkzk6c7whpncf3nwcrw3aw8blx0zyz3bkzncn74jz")))

(define-public crate-cw3-2.0.0 (c (n "cw3") (v "2.0.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0") (d #t) (k 0)) (d (n "cw-utils") (r "^2.0.0") (d #t) (k 0)) (d (n "cw20") (r "^2.0.0-rc.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)))) (h "0flidx272nk6jxxyjznqlgbvm7kjcjjv5s4br3cnav2jawh3rrfm")))

