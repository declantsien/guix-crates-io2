(define-module (crates-io #{3}# c cil) #:use-module (crates-io))

(define-public crate-cil-0.1.0 (c (n "cil") (v "0.1.0") (h "08rqd9jkwy7dgharlgzgmjps5bq889vzc6c9lwkzx7hxzx27r3k5")))

(define-public crate-cil-0.1.1 (c (n "cil") (v "0.1.1") (h "11fszj8p5lw7k6xhrq9yh3hkmxhk5zii9vb2mwg1z0620nmc46ka")))

(define-public crate-cil-0.1.2 (c (n "cil") (v "0.1.2") (h "055dagiak7vzpwvlv0n9flnj0cyibv90ymqajcb13k7p1q6qkdaz")))

