(define-module (crates-io #{3}# c cwd) #:use-module (crates-io))

(define-public crate-cwd-1.1.2 (c (n "cwd") (v "1.1.2") (h "15yrqpjrjpqjqh45iqjjqn09an4v6vsvhbir92aa072bvc8avwc2")))

(define-public crate-cwd-1.1.3 (c (n "cwd") (v "1.1.3") (h "0cw8hv0rn14i1l1wsl07z9x022wapcf3hyyywh4gslnjkmpnfd9s")))

(define-public crate-cwd-1.1.4 (c (n "cwd") (v "1.1.4") (h "0nd21zpaclwxp495jk6ba0xl1938xlsbwkhyhq7m33klf5qj7nij")))

(define-public crate-cwd-1.1.5 (c (n "cwd") (v "1.1.5") (h "1i5c2h2xgy5wnygq7k11qsjvw417bq9qvfk01pljmplq59xbk95i")))

(define-public crate-cwd-1.1.6 (c (n "cwd") (v "1.1.6") (h "1vizfrawfv98yzc7k06d4j93pbx0h43im1dxqyayd1abdf72wqxl")))

