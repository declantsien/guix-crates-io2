(define-module (crates-io #{3}# c com) #:use-module (crates-io))

(define-public crate-com-0.0.1 (c (n "com") (v "0.0.1") (h "0mfkcvi8qqb18x6gm0j34y7m83ppzch43ccahpmqr6jwjzidmcn4")))

(define-public crate-com-0.0.2 (c (n "com") (v "0.0.2") (h "0krgfbxk1g2d33mdxmabfm8iy3z33xz8jvq7ac1b2aralyaqpmi6")))

(define-public crate-com-0.0.3 (c (n "com") (v "0.0.3") (h "0jncr1blalpcsm50560pjx315iqlcbz3v83s1v97521smgbxn31r")))

(define-public crate-com-0.1.0 (c (n "com") (v "0.1.0") (d (list (d (n "com_macros") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "winreg" "winerror" "winnt" "libloaderapi" "olectl" "objbase"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0fswmnwvjk04xgq1iaasvs3l78zcbdyvwg81yfbl8r1f3x4mzawi")))

(define-public crate-com-0.2.0 (c (n "com") (v "0.2.0") (d (list (d (n "com_macros") (r "^0.2") (d #t) (k 0)))) (h "14y703q013gbaqiipgimz19rvmf034yxlgncqmnrinhkl2ra4c2s")))

(define-public crate-com-0.3.0 (c (n "com") (v "0.3.0") (d (list (d (n "com_macros") (r "^0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "10skn2hjzndaq3vmkx4n21kz817z43ic0aa44kn5p2yfjbvjwfx8") (f (quote (("production"))))))

(define-public crate-com-0.4.0 (c (n "com") (v "0.4.0") (d (list (d (n "com_macros") (r "^0.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1zg4lgldcxm4ax0nphy0varn54czs4063dbfd1wiz848qf4vwffp") (f (quote (("std") ("production" "std") ("default" "std"))))))

(define-public crate-com-0.5.0 (c (n "com") (v "0.5.0") (d (list (d (n "com_macros") (r "^0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0zia9lay51934gris26d67l791qz5wm4mx9pqfan1nj1a3wrraji") (f (quote (("std") ("production" "std") ("default" "std"))))))

(define-public crate-com-0.6.0 (c (n "com") (v "0.6.0") (d (list (d (n "com_macros") (r "^0.6") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1xnryd43mqdyq66qlnagwxrcs9iyr0kcbw9f3ddvclvks5zqh5vy") (f (quote (("std") ("production" "std") ("default" "std"))))))

