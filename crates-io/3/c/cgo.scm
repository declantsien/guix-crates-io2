(define-module (crates-io #{3}# c cgo) #:use-module (crates-io))

(define-public crate-cgo-0.1.0 (c (n "cgo") (v "0.1.0") (h "057ih38pa7x4djpb9wgr751lxp7qbvh81z6fhzszqsyqv1inbc7v")))

(define-public crate-cgo-0.2.0 (c (n "cgo") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "0g7zcr6r7mdwsc42vwh3whibpgya3cdxl7cymfajgix50ikznilv")))

(define-public crate-cgo-0.2.1 (c (n "cgo") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "0f9z0zmqqjzmnl0qvac5g696x7yb2avkhyil8gkf318cmcm03f6l")))

(define-public crate-cgo-0.3.0 (c (n "cgo") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 0)))) (h "1sm2vp9f85h5kghgadx09pk0cbar41if27c7q3m2qp5db1l9xvib")))

