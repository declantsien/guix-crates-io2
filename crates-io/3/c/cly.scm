(define-module (crates-io #{3}# c cly) #:use-module (crates-io))

(define-public crate-cly-0.1.0 (c (n "cly") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "cly-impl") (r "=0.1.0") (d #t) (k 0)) (d (n "repc-impl") (r "^0.1.0") (d #t) (k 0)))) (h "1vljz3kcpv00ic11i8cx1yx1cm20lpabqvp84pbyq5nchf9mc5yz")))

(define-public crate-cly-0.1.1 (c (n "cly") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "cly-impl") (r "=0.1.1") (d #t) (k 0)) (d (n "repc-impl") (r "^0.1.1") (d #t) (k 0)))) (h "0qyw10qjp99zs4ik9n6566zpyc41gz0nlv9zxf08a4xksrnrm9xc")))

