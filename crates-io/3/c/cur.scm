(define-module (crates-io #{3}# c cur) #:use-module (crates-io))

(define-public crate-cur-0.1.0 (c (n "cur") (v "0.1.0") (d (list (d (n "cur_macro") (r "^0.1.0") (d #t) (k 0)))) (h "06gv32xm1824hga2sgg8syc46p0xlx5hp03gls8jmyq952a2d1dd")))

(define-public crate-cur-0.2.0 (c (n "cur") (v "0.2.0") (d (list (d (n "cur_macro") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0ksb5hjsw8y5vczdw7jb82jin168c4bzriirz972h0fn31nxlnjr")))

(define-public crate-cur-0.3.0 (c (n "cur") (v "0.3.0") (d (list (d (n "cur_macro") (r "^0.3.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "0wd5ps4zg0wjw1pfplbb4bvq404pvfnql1rsi3mjra3kbzr4nac4")))

(define-public crate-cur-0.4.0 (c (n "cur") (v "0.4.0") (d (list (d (n "cur_macro") (r "^0.4.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.4.0") (d #t) (k 0)))) (h "069mw92x57nfamh0k8m228n1jzhljjfbgqmr3ksl679hmdhv0la1")))

(define-public crate-cur-0.5.0 (c (n "cur") (v "0.5.0") (d (list (d (n "cur_macro") (r "^0.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)))) (h "1m692qc1skbi3z78r1z60fn2mfk2kwffyqcgcr5k7gqvc9mf947k")))

