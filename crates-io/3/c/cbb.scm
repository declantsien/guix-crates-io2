(define-module (crates-io #{3}# c cbb) #:use-module (crates-io))

(define-public crate-cbb-0.1.0 (c (n "cbb") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "0ai4hj0piq7vqgvbkcsqk9sdpqj320dil27kg1s7x3dmx0jpszi8") (y #t)))

(define-public crate-cbb-0.1.1 (c (n "cbb") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "1pvrf706iqic7i2wvj54jg67bb37k7an4yzqklafmif60xsamafd") (y #t)))

(define-public crate-cbb-0.1.2 (c (n "cbb") (v "0.1.2") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "0mdcbfwal0ycavqjsjsr05ad1ff5dphamsh4m4x4m6v6h2kjjxls") (y #t)))

(define-public crate-cbb-0.1.4 (c (n "cbb") (v "0.1.4") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "05ihi1104k66nkgrcb40b4f2c2ipfcf1dbqnp2w3wfj4fxnql8fn")))

(define-public crate-cbb-0.1.5 (c (n "cbb") (v "0.1.5") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "1pfinkvrlsq2hizf0r29p7h74z7x3k9xp0lpvdarxyzkgxkxps7r")))

(define-public crate-cbb-0.1.6 (c (n "cbb") (v "0.1.6") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "0nhr284jawxi3bqsa8z13frfzzvr3psjcksalimrwqqbnbia3ncb")))

(define-public crate-cbb-0.1.7 (c (n "cbb") (v "0.1.7") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "0npf9ig31qkbqzffn29gdsrwrbrbf5d860kl7gl090825zkdl5lz")))

(define-public crate-cbb-0.1.8 (c (n "cbb") (v "0.1.8") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "1drsl0f5d322k7wwfgbnn64anc7djqyqxj03vl4ymhh5x4i0rp86")))

(define-public crate-cbb-0.1.9 (c (n "cbb") (v "0.1.9") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "1xzmczhyf7hy7z3fdg1j5i1yxrhiva4cxhbwm3qkp8sbggnjnzyl")))

(define-public crate-cbb-0.1.10 (c (n "cbb") (v "0.1.10") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "1vkc3kzy2602yypgbjzhd7frrax6mrnrgkdq0lnr8hifw7ds7hnl")))

(define-public crate-cbb-0.1.11 (c (n "cbb") (v "0.1.11") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "0yk2sl8fh978yqazps4lrj8wr26b58q7bvz295a1zqwsj3gpvfd6")))

(define-public crate-cbb-0.1.12 (c (n "cbb") (v "0.1.12") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "0ny2h3fhzrmzznsif6lfgqwzj6hrngdn7mr5ywvcc3yqyda0r4gv")))

(define-public crate-cbb-0.1.14 (c (n "cbb") (v "0.1.14") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "0hlqvmvrzg8bp2lmchqyhzfh4qab9gja8z2l9sg5bmhn4s20i35c")))

(define-public crate-cbb-0.2.0 (c (n "cbb") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "0zb8k13f6y1jckzxj04gl1rdgwrh4pnqrpvza9d7hbwksjb6npcc") (y #t)))

(define-public crate-cbb-0.2.1 (c (n "cbb") (v "0.2.1") (d (list (d (n "clap") (r "^3.0.8") (d #t) (k 0)) (d (n "num") (r "^0.3.1") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)))) (h "0gpfsjyz858rfqmc8gjdpxfnhksmwbgz962hp279qcjdkzwaygy1")))

