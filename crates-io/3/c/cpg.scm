(define-module (crates-io #{3}# c cpg) #:use-module (crates-io))

(define-public crate-cpg-0.1.0 (c (n "cpg") (v "0.1.0") (d (list (d (n "bcrypt-pbkdf") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time-elapsed") (r "^0.1.0") (d #t) (k 0)))) (h "1kacwf6hkvdihy4kp0mwd4bkxvf1havhzvblmgf6pp6qqpcdiq59")))

(define-public crate-cpg-0.1.1 (c (n "cpg") (v "0.1.1") (d (list (d (n "bcrypt-pbkdf") (r "^0.10.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.13") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "time-elapsed") (r "^0.1.0") (d #t) (k 0)))) (h "0b07yl6v2qswh2dgfqr50s5q1fnxid40w5bvn762rawvjkrjfcmz")))

