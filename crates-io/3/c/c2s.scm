(define-module (crates-io #{3}# c c2s) #:use-module (crates-io))

(define-public crate-c2s-0.1.0 (c (n "c2s") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "1vs9kq91j60jm7rllx0sxky5v85vv70qwi5ba7n94lzb7fnvhvbw")))

(define-public crate-c2s-0.1.1 (c (n "c2s") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "1pzky3rlmnjs3r2h6hr432597rbs50kypiizcpidkm7ydcmy5lmz")))

(define-public crate-c2s-0.2.0 (c (n "c2s") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "0xxrgkzc7746rfpbx7q5njvr7gmhnhj53vg32s43b36x8bajpgz6")))

