(define-module (crates-io #{3}# c cao) #:use-module (crates-io))

(define-public crate-cao-0.1.0 (c (n "cao") (v "0.1.0") (d (list (d (n "curl") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "if-addrs") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20") (d #t) (k 2)) (d (n "ureq") (r "^2.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "155lkxfbdk9h27yb647r282c10c7xaqkpvrlkhp5s7v6v1sykwim") (f (quote (("dnspod") ("default" "dnspod" "ureq"))))))

(define-public crate-cao-0.1.1 (c (n "cao") (v "0.1.1") (d (list (d (n "curl") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "if-addrs") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20") (d #t) (k 2)) (d (n "ureq") (r "^2.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0llwbga8jd1abcjyx3nini3fwhs2pd2i4ip5wq5l1a5j4i4jhsn7") (f (quote (("dnspod") ("default" "dnspod" "ureq"))))))

(define-public crate-cao-0.1.2 (c (n "cao") (v "0.1.2") (d (list (d (n "curl") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "if-addrs") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.20") (d #t) (k 2)) (d (n "ureq") (r "^2.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "0l37csrbhydm2kx0bvrbmrjkvn8ma89ipcxcmiikrhn2x2s7291j") (f (quote (("dnspod") ("default" "dnspod" "ureq") ("curl_invailed_cert" "curl"))))))

