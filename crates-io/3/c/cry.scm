(define-module (crates-io #{3}# c cry) #:use-module (crates-io))

(define-public crate-cry-0.1.0 (c (n "cry") (v "0.1.0") (d (list (d (n "curve25519-dalek") (r "^3") (f (quote ("u32_backend"))) (k 0)) (d (n "digest") (r "^0.9") (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "1qcfzlrg55k2qjnpdafkpy4zbjj44hbb14v4j5fwm9jl3q5f95ky")))

(define-public crate-cry-0.1.1 (c (n "cry") (v "0.1.1") (d (list (d (n "curve25519-dalek") (r "^3") (f (quote ("u32_backend"))) (k 0)) (d (n "digest") (r "^0.9") (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "1ij4gmpkm8flis8mz13n24ys0ai88ikwq58abrcfb5z7999nfkz9")))

(define-public crate-cry-0.1.2 (c (n "cry") (v "0.1.2") (d (list (d (n "curve25519-dalek") (r "^3") (f (quote ("u32_backend"))) (k 0)) (d (n "digest") (r "^0.9") (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "1f5v85i98djp9853jn1gqc9w3bv0bv6m2bi7qf7813147z394yg9")))

(define-public crate-cry-0.1.3 (c (n "cry") (v "0.1.3") (d (list (d (n "curve25519-dalek") (r "^3") (f (quote ("u32_backend"))) (k 0)) (d (n "digest") (r "^0.9") (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "04i8fl1lzc84zrm83yi750kxhw48q8idvmw4a16fyzxhqjh4p2k7")))

(define-public crate-cry-0.1.4 (c (n "cry") (v "0.1.4") (d (list (d (n "curve25519-dalek") (r "^3") (f (quote ("u32_backend"))) (k 0)) (d (n "digest") (r "^0.9") (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sha3") (r "^0.9") (d #t) (k 2)))) (h "1bpv0f3dx5zh3c20fsamml9shbiv2rlaib83rkd70dkkmdc5wkrf")))

(define-public crate-cry-0.1.5 (c (n "cry") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "curve25519-dalek") (r "^3") (f (quote ("u32_backend"))) (k 0)) (d (n "digest") (r "^0.9") (k 0)) (d (n "generic-array") (r "^0.14") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "sha3") (r "^0.9") (d #t) (k 2)) (d (n "unroll") (r "^0.1") (d #t) (k 0)))) (h "08ail76jawivnd0wg3gyi465lbclpv5yfa3c8kv127c51m8066pz") (f (quote (("ristretto255") ("default"))))))

