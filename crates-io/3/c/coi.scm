(define-module (crates-io #{3}# c coi) #:use-module (crates-io))

(define-public crate-coi-0.1.0 (c (n "coi") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)))) (h "1c2h4am1y4ggiby9rl0c6ajlkwyk7bdavmq7k1hkbz557yrz9k9f")))

(define-public crate-coi-0.2.0 (c (n "coi") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "coi-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1pd14iqdvzhq104vxd2r36qv29qbi6m22g4r6av6m7vpmljm5c88")))

(define-public crate-coi-0.2.1 (c (n "coi") (v "0.2.1") (d (list (d (n "async-std") (r "^1.4.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "coi-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1yw53y3y1l79v4ywkrxqsnaj692q9905s6szixyiid7ir4xq9vpd")))

(define-public crate-coi-0.2.2 (c (n "coi") (v "0.2.2") (d (list (d (n "async-std") (r "^1.4.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "coi-derive") (r "^0.1.1") (d #t) (k 0)))) (h "0cds34kalnidi321mri3y27j1va4krab87i9vg8sqhjg4asmr2jq")))

(define-public crate-coi-0.2.3 (c (n "coi") (v "0.2.3") (d (list (d (n "async-std") (r "^1.4.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "coi-derive") (r "^0.2.0") (d #t) (k 0)))) (h "0hxpbdh18ri73ja23jpvfrh6nxfwm62h2h16xgl4vdamygyjmlcx")))

(define-public crate-coi-0.2.4 (c (n "coi") (v "0.2.4") (d (list (d (n "async-std") (r "^1.4.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "coi-derive") (r "^0.2.1") (d #t) (k 0)))) (h "0ns7fml7fgydgq15ik3ivnngl78pxyyn974c6i9vpwnvxfa71p5h")))

(define-public crate-coi-0.3.0 (c (n "coi") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "coi-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "0i5a43dsahclbfzik56pj0xpd6z3hnzkd708f7qf61qg2din4kgv")))

(define-public crate-coi-0.3.1 (c (n "coi") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "coi-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1d16im7afq1a8pm8rmxjw2djc48a11y0jbk44axis2pmljghj4gj")))

(define-public crate-coi-0.4.0 (c (n "coi") (v "0.4.0") (d (list (d (n "async-std") (r "^1.4.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.22") (d #t) (k 0)) (d (n "coi-derive") (r "^0.3.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "0d95jrfwxjpivfkny1j2pk1xsf3c97m19f13la1nwrfbh0a675m0")))

(define-public crate-coi-0.5.0 (c (n "coi") (v "0.5.0") (d (list (d (n "async-std") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.22") (o #t) (d #t) (k 0)) (d (n "coi-derive") (r "^0.4.1") (o #t) (k 0)) (d (n "futures") (r "^0.3.1") (o #t) (d #t) (k 0)))) (h "15qqkkfr213x2ib0rdifvsf1hdayvncsdsm669h6n0s65v6h70kg") (f (quote (("derive-async" "coi-derive/async" "async") ("derive" "coi-derive") ("default" "derive-async") ("async" "async-std" "async-trait" "futures"))))))

(define-public crate-coi-0.6.0 (c (n "coi") (v "0.6.0") (d (list (d (n "coi-derive") (r "^0.5.0") (o #t) (k 0)))) (h "1mzlg8pi6601jaz2brhlz9lrnjs00lgdr5xsghfvhfpvdm94flv7") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug"))))))

(define-public crate-coi-0.6.1 (c (n "coi") (v "0.6.1") (d (list (d (n "coi-derive") (r "^0.5.0") (o #t) (k 0)))) (h "115fc6k989js1h5d5fmx2d6fdbv6rvbwcsrmg462nns0z7mzfri6") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug"))))))

(define-public crate-coi-0.7.0 (c (n "coi") (v "0.7.0") (d (list (d (n "coi-derive") (r "^0.7.0") (o #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)))) (h "0ifqif0s5n0a8mj3f7wd5sba0ym5qynnbkdmps4lg5s5hzj91wm9") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug" "petgraph"))))))

(define-public crate-coi-0.8.0 (c (n "coi") (v "0.8.0") (d (list (d (n "coi-derive") (r "^0.8.0") (o #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)))) (h "11lvqihzcx7hc22y1x2xa1m7fgmjvi40319hv4lngfndnla3dj7a") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug" "petgraph"))))))

(define-public crate-coi-0.9.0 (c (n "coi") (v "0.9.0") (d (list (d (n "coi-derive") (r "^0.9.0") (o #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0k05qi8n4hj395ywsi8j28r9lyrh30y94ljxdngxmdjv67m3iprl") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug" "petgraph"))))))

(define-public crate-coi-0.9.1 (c (n "coi") (v "0.9.1") (d (list (d (n "coi-derive") (r "^0.9.0") (o #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "196ymh3ia6mjzb1ivjmxamr3m6gbyl6zxayjsxz83npw53cj5z5y") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug" "petgraph"))))))

(define-public crate-coi-0.9.2 (c (n "coi") (v "0.9.2") (d (list (d (n "coi-derive") (r "^0.9.0") (o #t) (k 0)) (d (n "petgraph") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vara3wjrb52ajirsfw6jps574nm7bwpin4bq4a0jjdlhmn91jzq") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug" "petgraph"))))))

(define-public crate-coi-0.10.0 (c (n "coi") (v "0.10.0") (d (list (d (n "coi-derive") (r "^0.10.0") (o #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "petgraph") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "13c7i4lc0r6383790n8zd9675bnqwbjirvkk6rizcfvdh4ij1sdx") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug" "petgraph"))))))

(define-public crate-coi-0.10.1 (c (n "coi") (v "0.10.1") (d (list (d (n "coi-derive") (r "^0.10.0") (o #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "petgraph") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lqp2rqkxppa8qv0997cwi43vi2xb9m1d7np2jqpy8n70xnly5ql") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug" "petgraph"))))))

(define-public crate-coi-0.10.2 (c (n "coi") (v "0.10.2") (d (list (d (n "coi-derive") (r "^0.10.0") (o #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "petgraph") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fnqs3x41m698wkpwa81vibxlik05cr4amzyf5zqcrmdm70lnxjn") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug" "petgraph"))))))

(define-public crate-coi-0.10.3 (c (n "coi") (v "0.10.3") (d (list (d (n "coi-derive") (r "^0.10.1") (o #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "petgraph") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ib0si1p1an4pp00sxd86kmy4w7i4zvjhy7a8j1p8rppfqmc7q5y") (f (quote (("derive" "coi-derive") ("default" "derive") ("debug" "coi-derive/debug" "petgraph"))))))

