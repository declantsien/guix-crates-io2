(define-module (crates-io #{3}# c cok) #:use-module (crates-io))

(define-public crate-cok-0.1.0 (c (n "cok") (v "0.1.0") (d (list (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1y6d8jfr1914f37j8gjp3790rrpnqbbywanw554yap81zhl62jqx")))

(define-public crate-cok-0.1.1 (c (n "cok") (v "0.1.1") (d (list (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0zicndrfzink7ph0zhg2lg0h2mikprjsnsjmhf0mbrmwf8h75dvw")))

(define-public crate-cok-0.1.2 (c (n "cok") (v "0.1.2") (d (list (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1xr93cnvxv4g2f31kxcj2ihqw4mx982l85gs2ll69cmlh7qf6v1s")))

(define-public crate-cok-0.1.3 (c (n "cok") (v "0.1.3") (d (list (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1iyakw7k0css1r5vmqg3p9w33k3j791xdgxqcva355z6q3fvggha")))

(define-public crate-cok-0.1.4 (c (n "cok") (v "0.1.4") (d (list (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1ldhqhrif54ng98a9x6acqqp2qyrwiz3ng1356jp05m95xygmhx1")))

(define-public crate-cok-0.1.5 (c (n "cok") (v "0.1.5") (d (list (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1j5f7kni02kvyx7s9fkj0g5j6vnfa6kzdf9zd6njf39srp9a88jw")))

(define-public crate-cok-0.1.6 (c (n "cok") (v "0.1.6") (d (list (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1290w7m6c9m2qlvvvwz7vniwska2b6wz427fmciycl5h8za5lfik")))

(define-public crate-cok-0.1.7 (c (n "cok") (v "0.1.7") (d (list (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1ygpf0b0kkxcl1xzd5h4sa36kd95p6bilpcra8dirik0an9n29f6")))

(define-public crate-cok-0.1.8 (c (n "cok") (v "0.1.8") (d (list (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "1wbfavv4hzijk6fbq81k87f2vx9mn8vp71782p9smjs8r1y05gqv")))

(define-public crate-cok-0.1.9 (c (n "cok") (v "0.1.9") (d (list (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0ac6akdbnq4abnvmagnvh85f0m9zscksammqrwln3y1g182a1g1c")))

(define-public crate-cok-0.1.10 (c (n "cok") (v "0.1.10") (d (list (d (n "doe") (r "^0.1.76") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0phny8vrdrf8395bj7jvppr70wagy0lf8yjgwkzlfdwc4my8mdsx")))

(define-public crate-cok-0.1.11 (c (n "cok") (v "0.1.11") (d (list (d (n "doe") (r "^0.1.85") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "01qm903zlp270vymv3iigwlizsydk22a7kg5p1c64mls8w5zqqyx")))

(define-public crate-cok-0.1.12 (c (n "cok") (v "0.1.12") (d (list (d (n "doe") (r "^0.1.85") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "12b6xni2gvrbrq8mlxsrwp7qf7arpml7fhap4gjzxq4xgssyrgfr")))

(define-public crate-cok-0.1.13 (c (n "cok") (v "0.1.13") (d (list (d (n "doe") (r "^0.1.85") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)))) (h "0wywfdnv3p7gaxxlcsvbdfsy3m4c7w3cm0m5wi9l8cb9an915s5q")))

