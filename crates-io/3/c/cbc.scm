(define-module (crates-io #{3}# c cbc) #:use-module (crates-io))

(define-public crate-cbc-0.0.0 (c (n "cbc") (v "0.0.0") (h "17dk51xwfj4rvpkf611pi8ipqd97yq4mcv2iq73q9biv394sg8xp")))

(define-public crate-cbc-0.1.0 (c (n "cbc") (v "0.1.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "162b089bbaxrl96msi60i60nvrqq4dlg1339782fmrwfbj05242k") (f (quote (("zeroize" "cipher/zeroize") ("default" "block-padding") ("block-padding" "cipher/block-padding")))) (r "1.56")))

(define-public crate-cbc-0.1.1 (c (n "cbc") (v "0.1.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "0a51za4zy6naa0w08300xy3rlmvnr5av5qb735jca7s698fqpim2") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("default" "block-padding") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

(define-public crate-cbc-0.1.2 (c (n "cbc") (v "0.1.2") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "19l9y9ccv1ffg6876hshd123f2f8v7zbkc4nkckqycxf8fajmd96") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("default" "block-padding") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

