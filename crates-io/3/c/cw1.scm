(define-module (crates-io #{3}# c cw1) #:use-module (crates-io))

(define-public crate-cw1-0.1.0 (c (n "cw1") (v "0.1.0") (d (list (d (n "cosmwasm-schema") (r "^0.10.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "14ks9mwpk6h87dl7665j6z8h3qz1m021akyz1ah9asvy181nf0h7")))

(define-public crate-cw1-0.1.1 (c (n "cw1") (v "0.1.1") (d (list (d (n "cosmwasm-schema") (r "^0.10.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1nlp6s3c5yl3jcz5yx1400s9xvk82wif0jad75j5dx1p5fxf94pb")))

(define-public crate-cw1-0.2.0 (c (n "cw1") (v "0.2.0") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "193h6nm66ia6yz0gnkjm228573zv0bmq9ynxgi8826ij0bx9bfs3")))

(define-public crate-cw1-0.2.1 (c (n "cw1") (v "0.2.1") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "11xlrckndk7inpbwzfjwcdd7zy5csqdwvjsyh7r3a1s2p1ayrk35")))

(define-public crate-cw1-0.2.2 (c (n "cw1") (v "0.2.2") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "073cgd8x1ddjrk5hd1jfp2jj12mlhi9s4a94jrj9l44syf4i1q6d")))

(define-public crate-cw1-0.2.3 (c (n "cw1") (v "0.2.3") (d (list (d (n "cosmwasm-schema") (r "^0.10.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0jwzf27pyai28kva18qywfinr48fqh24k4r6spdg5mdqwilyzn3g")))

(define-public crate-cw1-0.3.0 (c (n "cw1") (v "0.3.0") (d (list (d (n "cosmwasm-schema") (r "^0.11.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.11.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0q9aq5k96jv7y2lh1nn5pmkp62b1f2d2151rv1azc4pdbfzxx5sz")))

(define-public crate-cw1-0.3.1 (c (n "cw1") (v "0.3.1") (d (list (d (n "cosmwasm-schema") (r "^0.11.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0fx22pllhnyka86pa74ihj77l6dzwmylr23064d0zwhisqxddf6g")))

(define-public crate-cw1-0.3.2 (c (n "cw1") (v "0.3.2") (d (list (d (n "cosmwasm-schema") (r "^0.11.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1101y37ckybn073bpcpbmsx860j2akkh8rsf5f4wa2ym5s7a2rr1")))

(define-public crate-cw1-0.4.0 (c (n "cw1") (v "0.4.0") (d (list (d (n "cosmwasm-schema") (r "^0.12.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.12.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1p3d5nxqblyscgpnnd259y4z4av4ayx2ira326db9pda09nyxz6d")))

(define-public crate-cw1-0.5.0 (c (n "cw1") (v "0.5.0") (d (list (d (n "cosmwasm-schema") (r "^0.13.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.13.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1g26svwnjhjkkrvg3lk7wlczy3clbz1isfmlas35x58i3jx0271g")))

(define-public crate-cw1-0.6.0-alpha1 (c (n "cw1") (v "0.6.0-alpha1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "057kh5s6nxr8wbdxnknb1frxfpw4i5j7cis3ddfyly98v9a45s2p")))

(define-public crate-cw1-0.6.0-alpha2 (c (n "cw1") (v "0.6.0-alpha2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0z1z6hiyvmd03ia9qan7akqkkx2y9yw1g8hb97hzxdv46xb59sns")))

(define-public crate-cw1-0.6.0-alpha3 (c (n "cw1") (v "0.6.0-alpha3") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1zwgrjph9pya5iqvc2hnif1z5jqhg1faz1qj0wz1hn90p8y6j1yg")))

(define-public crate-cw1-0.6.0-beta1 (c (n "cw1") (v "0.6.0-beta1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0f2ikhxbfvwxs6m6qamwv2wjgi65lkvw5rvp1a0asvi3yv6qqzi1")))

(define-public crate-cw1-0.6.0-beta2 (c (n "cw1") (v "0.6.0-beta2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1983brqlixjdxqnkgc4jv2ld6qpk4srwlhgl5qi06smn87s7yq7x")))

(define-public crate-cw1-0.6.0-beta3 (c (n "cw1") (v "0.6.0-beta3") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "090vd3mk9x2fq8zxlnpwlsvw885nn1nq37zz26x8f5nay7w1hpwd")))

(define-public crate-cw1-0.6.0 (c (n "cw1") (v "0.6.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0llawral1bh76k919nal5l1blih2yc3b04yqkz9n3b1gfiwrgl4y")))

(define-public crate-cw1-0.6.1 (c (n "cw1") (v "0.6.1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0zl52lk57cglyvqyy4rklc1vbhg2z0k1nhm46fr2bcg7hyaxhh02")))

(define-public crate-cw1-0.6.2 (c (n "cw1") (v "0.6.2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1i0ws1kakjd42mnzn3i4dc6ahnp4764xc805i61r9ds9lyc9sxng")))

(define-public crate-cw1-0.7.0 (c (n "cw1") (v "0.7.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "02yr5jz1m81mklns55a6hky4r7n9ynv1llf7rr71xpj3z26b1p5s")))

(define-public crate-cw1-0.8.0-rc1 (c (n "cw1") (v "0.8.0-rc1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0-rc5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0-rc5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1cdv2sr75kjfixp6vcqj1vniih4hy2r8mb8xc0ka4yhpcl3vrbk9")))

(define-public crate-cw1-0.8.0-rc2 (c (n "cw1") (v "0.8.0-rc2") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0c3s9fb9i9jvhkbs98vkfl5hjqx0pqr7hn4x9magcjlz5i2rbhf6")))

(define-public crate-cw1-0.8.0-rc3 (c (n "cw1") (v "0.8.0-rc3") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0z8c13gd4072dzfxjz3zilymlqdsj4qhlrv8r0lxixx31vmlmq4j")))

(define-public crate-cw1-0.8.0 (c (n "cw1") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1cjvxkk35fkcgzd9q94ry4xw125w27cwhh7m8l3vmr25vlc013i0")))

(define-public crate-cw1-0.8.1 (c (n "cw1") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "114wxnd50qq2ar1p5zkgbww6q4llf28ln71w0cgv7wm3bl11cjxm")))

(define-public crate-cw1-0.9.0 (c (n "cw1") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "13fmn0a5gfgz0y10wdj9wq1lsdc90ncqcwdk8nibz24xw5q1pc92")))

(define-public crate-cw1-0.10.0-soon (c (n "cw1") (v "0.10.0-soon") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0ys7asl93z57kmn1c76yrvlni1wphgwdjyvx24nycnsvqk7bhwmq")))

(define-public crate-cw1-0.9.1 (c (n "cw1") (v "0.9.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1i922cidxdn9w5hr32zgxwdlphzqhnjarzibrvnkkac1sv99kfcz")))

(define-public crate-cw1-0.10.0-soon2 (c (n "cw1") (v "0.10.0-soon2") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1siqswh550lyi7p0ghc4f6pa4yjxsfs5jmilbh0vcnxx403iqqzi")))

(define-public crate-cw1-0.10.0-soon3 (c (n "cw1") (v "0.10.0-soon3") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0c0bz6k2bp79m18waji2xbmkxhh7svnmv6lzg927ww5ydk75srr3")))

(define-public crate-cw1-0.10.0-soon4 (c (n "cw1") (v "0.10.0-soon4") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0rb2c4yjkx33rczpgq5pbrny10jc0g04sa7rnshvzdpb77y0pxmk")))

(define-public crate-cw1-0.10.0 (c (n "cw1") (v "0.10.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0dagaar1bigl2mhxrpdiq4vdh6pg3syxalm42y7j1xacvsxn0wk4")))

(define-public crate-cw1-0.10.1 (c (n "cw1") (v "0.10.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1a4ljkli3xsi992l6i7zq0a750zc87bwdasmk8191h0w60lkh25v")))

(define-public crate-cw1-0.10.2 (c (n "cw1") (v "0.10.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1mxkcr2injn8y1rhd56k3mx4dslpii9ff89d7zjbkivkcx6r7g2n")))

(define-public crate-cw1-0.10.3 (c (n "cw1") (v "0.10.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1yglp9fla19qlvlqfkgrs9lfrpwss04psshgsb2680h83hcrkx4w")))

(define-public crate-cw1-0.11.0 (c (n "cw1") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "11xq67d7h2gq1zg4yaij7rl9wf92ppmdc9d05wfg7316fpkp0dim")))

(define-public crate-cw1-0.11.1 (c (n "cw1") (v "0.11.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "15msinzpy6kg5390aqi6fi19n6qchb2336jvy29xjrq685mai8f4")))

(define-public crate-cw1-0.12.0 (c (n "cw1") (v "0.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "146rhakfb7fwyn05gfy7x946in80ksivhiv7zgvifg323sryyh2v")))

(define-public crate-cw1-0.12.1 (c (n "cw1") (v "0.12.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "14f5paqivl155gn5vras8fd7bhrkjglnqzc67s9q6ly3k2j64325")))

(define-public crate-cw1-0.13.0 (c (n "cw1") (v "0.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "14il8jyklwgp07bf82s4ckixh55ivc9b05n2adj6a8jzgp2rh97r")))

(define-public crate-cw1-0.13.1 (c (n "cw1") (v "0.13.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0aj6g859ww0a25gd9gijaw78qc56w29grn10yjd7ar0dx59i0bfi")))

(define-public crate-cw1-0.13.2 (c (n "cw1") (v "0.13.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta8") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0mm5qjbcn0dxy9h2nr8f12vjb3224b2miiw8qbp8wgcs0s4rr97k")))

(define-public crate-cw1-0.13.3 (c (n "cw1") (v "0.13.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0wvp78wpj8qmhpw9ywc2kvz97b7mpg93cwk0ip73dnh796n3yvys")))

(define-public crate-cw1-0.13.4 (c (n "cw1") (v "0.13.4") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1q15w6rcqcmw047cxjgk2xjab6wfav2vzwhqga5gz67q32m4j9in")))

(define-public crate-cw1-0.14.0 (c (n "cw1") (v "0.14.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "18x3f1zr0i6d3rq48gyr8whgr5mcfr5cchvb4q5kbsjwkq8yb7n6")))

(define-public crate-cw1-0.15.0 (c (n "cw1") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "04i5lrpmhz7rjzna782vzdp8qd7578wvvrm376p9gr5p29hlkq7b")))

(define-public crate-cw1-0.15.1 (c (n "cw1") (v "0.15.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "075j83lm1fbw8anl5xw013dwcsgl0948gngyrpha82r1qhz7iq6v")))

(define-public crate-cw1-0.16.0 (c (n "cw1") (v "0.16.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0jxcbg2q9kbawva24qqminb671sv39v0bassfw1l8gv0iivd7mza")))

(define-public crate-cw1-1.0.0 (c (n "cw1") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1d4k3kipyd8cqjar6ff54022y4kisrs1gf3mak8b803jpx3jy1lb")))

(define-public crate-cw1-1.0.1 (c (n "cw1") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "13zq7pxkv46dqkifw957h86dhwag7n0cp6x5szxgir2b2h1sm49k")))

(define-public crate-cw1-1.1.0 (c (n "cw1") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "021j77f4gmbf8vnkv06ld0al6jl44b1xfi61hsdq0ycxyvdxc04z")))

(define-public crate-cw1-1.1.1 (c (n "cw1") (v "1.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "008i5ymgi5yc6q4m5nc0hxg1s0wkih3l78b2ymlhb5jm2197pn2c")))

(define-public crate-cw1-1.1.2 (c (n "cw1") (v "1.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "10qflygsfpak843f3lkhpw7fbkyi4j1bi133xazr7z8a34i5fq7i")))

(define-public crate-cw1-2.0.0-rc.0 (c (n "cw1") (v "2.0.0-rc.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "08rc48fz9wjf9m09zrxqg3wd5wik5d5vycwy5s63djnvn9g3d5b8")))

(define-public crate-cw1-2.0.0 (c (n "cw1") (v "2.0.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "1jsr4jyqbb7skmx1h8fwq85009kr2zskwk9z5c6ldcq6cyzg5s42")))

