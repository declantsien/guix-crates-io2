(define-module (crates-io #{3}# c cod) #:use-module (crates-io))

(define-public crate-cod-1.1.0 (c (n "cod") (v "1.1.0") (h "0vl4v4n2qg7hf893n4cr8mrhna7wylva2zw24l247l08v9k1ki3m") (y #t)))

(define-public crate-cod-1.1.1 (c (n "cod") (v "1.1.1") (h "15b1viyfwc7w0zsglp8cysa3xwya4q761dnj62hpynpkga4pkbjr")))

(define-public crate-cod-1.1.2 (c (n "cod") (v "1.1.2") (h "1h8sfivbq5hkjldb40xfchwcb5z8g9rm6v50cd2hqn22lk5bia9r")))

(define-public crate-cod-2.0.0 (c (n "cod") (v "2.0.0") (h "0pdszkhc69s1r6bmy8917ndhn7jbn5k1d8pwsblpg2ssvvys66rc")))

(define-public crate-cod-3.0.0 (c (n "cod") (v "3.0.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1s31jwh8wyij6bx5lm9fb6jwdcj16kdkjlgkw5bjn1n0qd1k8lcf") (f (quote (("input" "console"))))))

(define-public crate-cod-3.0.1 (c (n "cod") (v "3.0.1") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "03ngkay2qynd9vvpv9ybypa013gvm81fxvsdwq27wzbblxjbvbxf") (f (quote (("input" "console"))))))

(define-public crate-cod-3.1.0 (c (n "cod") (v "3.1.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "13yd7k8fk92n78d8jr4n2mj423ck2gixrksdg1jgskv2anf3z56m") (f (quote (("input" "console"))))))

(define-public crate-cod-3.1.1 (c (n "cod") (v "3.1.1") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0mqbm86j9qj2667585yi6k5la6jyhg0y2hk604fy38cpkxji7rf0") (f (quote (("input" "console"))))))

(define-public crate-cod-3.1.2 (c (n "cod") (v "3.1.2") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "06y8dsvrlfgiz472agr0r7620krkfi9bg6q0xal81a5ngirhnnpc") (f (quote (("input" "console"))))))

(define-public crate-cod-3.1.3 (c (n "cod") (v "3.1.3") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "03mf4dsia36yj5y59s83p7rjgg5s3jaj8islzjbb3ln823401zk0") (f (quote (("input" "console"))))))

(define-public crate-cod-4.0.0 (c (n "cod") (v "4.0.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "11177vkddw1ck5bjq5hmgwz7njskl06qr9ngdh4zfkvj13pmf657") (f (quote (("input" "console"))))))

(define-public crate-cod-4.0.1 (c (n "cod") (v "4.0.1") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1h9068jgrh4hr50fwf31hgbbww526jg0y94bvr52j4dmz3jw5fr9") (f (quote (("input" "console"))))))

(define-public crate-cod-4.0.2 (c (n "cod") (v "4.0.2") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1ci0hfjry4xk51rkmrr2h5m0cgadaaw6im6kvm29jndb7ny65q8i") (f (quote (("input" "console"))))))

(define-public crate-cod-4.0.3 (c (n "cod") (v "4.0.3") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0chwbaq0l573lrc4az08kmjy0d32y1fb736gzxhjx5nncfq18n80") (f (quote (("input" "console"))))))

(define-public crate-cod-4.0.4 (c (n "cod") (v "4.0.4") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "08nq1s00wqfp06pp59nz9b6c5s5zf8knvjira10g3k8kk4in9v2c") (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-4.1.0 (c (n "cod") (v "4.1.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1mlb4r96s8b6v603c6flls951rr3wfqdzmpkzls24w1v8an8ksy8") (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-4.1.1 (c (n "cod") (v "4.1.1") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1jm4w2ss8rrpkffqbqxgdyflmqsy8wxv9xs4hrdwzn31qc3mrrkl") (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-5.0.0 (c (n "cod") (v "5.0.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0d2na4inzcycr7kbxjsj51bgmpxn0kbl7qqwvmhf43qx6apglizc") (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-5.0.1 (c (n "cod") (v "5.0.1") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1wv5zdymyjwnmdccwj1c2ck9jwlf817ddw4x379jyjyxjwcvkl40") (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-6.0.0 (c (n "cod") (v "6.0.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0lndhxhzxs0fl6jravqgmfi4hz5l6qz2i9pnn4rx8qafi3inw279") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-6.0.1 (c (n "cod") (v "6.0.1") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0v22isw5gra4p80vjcl48kax5md3n4j9br3vpgwv389zh767sscy") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-6.0.2 (c (n "cod") (v "6.0.2") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "094ll7prm6v7qp2ngr7v5prpwv8mk2panz86jyp2d2dbqxv0z51r") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-6.0.3 (c (n "cod") (v "6.0.3") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0gs2cx3c79a0ydx9rbykhzc4wwkcxc46fmlf0kb9isfd7vncyh6a") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-6.1.0 (c (n "cod") (v "6.1.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1bq982q4b6lwk99qfjzg8w4yifm4w84glc9wk954xkny14pjf6ai") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-6.1.1 (c (n "cod") (v "6.1.1") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1k1j6d1y9d70vc4hgnm44p2hz1d6alm33spk3vndz06r9ymz5igg") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-6.1.2 (c (n "cod") (v "6.1.2") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "012bdy1bfdr8aas5sskk6swbkz1jbx21i9v755jr45im5hz3arf9") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-6.1.3 (c (n "cod") (v "6.1.3") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "025gyn59q5rkwx2qksb0l5sbb2a262x82pmsasn19rqar9302v5q") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-6.1.4 (c (n "cod") (v "6.1.4") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "190aagrh93jw74rykqn194lkifrr60ac201ry4wdqy59chdaffi4") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-6.1.5 (c (n "cod") (v "6.1.5") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0anl2z7r73g0a61696vy44gbbzlfkmij3mzgfbs3pdlyz6cia8x7") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-7.0.0 (c (n "cod") (v "7.0.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0kssc9cvf4f5bgiydbj0imkhlin68b6xifbw2k2clh4nw14jcapx") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-7.0.1 (c (n "cod") (v "7.0.1") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "1fsn0a0cbryqg1s01g7fby06ih4gsfiy02cd2gc37pdpvqiw54f8") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-7.1.1 (c (n "cod") (v "7.1.1") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "09x9p4chs7habakixz8yb2b233n07yxsh1f8l79ai0fj8y31l5y4") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-7.2.0 (c (n "cod") (v "7.2.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0qljxxd773ijmnzgj92gfj7z9g3nrr1m4vbigypcvgcp6szmcpq3") (f (quote (("default")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-7.3.0 (c (n "cod") (v "7.3.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0msvm35dlrlhrfkg0zcbn9nz036k2fv6ml6ax6i6cwc29hmv1api") (f (quote (("default" "color_stack") ("color_stack")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-7.3.1 (c (n "cod") (v "7.3.1") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "0i2c2grv4gy627yrxqfrnfn52j8v494j76zgxyf4i5jdv62f7fmr") (f (quote (("default" "color_stack") ("color_stack")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-7.4.0 (c (n "cod") (v "7.4.0") (d (list (d (n "console") (r "^0.15") (o #t) (d #t) (k 0)))) (h "00k17466s7crbkv2vdxscvkgjs8dix2kfp6vvr7i5bf333srxrg8") (f (quote (("default" "color_stack") ("color_stack")))) (s 2) (e (quote (("input" "dep:console"))))))

(define-public crate-cod-8.0.0 (c (n "cod") (v "8.0.0") (d (list (d (n "crossterm") (r "^0.27") (o #t) (d #t) (k 0)))) (h "0alpxj5cl9x6cgyclj3k3d89hw0zgm4w7qs2rxjd5palisj5pqcx") (f (quote (("default" "color_stack") ("color_stack")))) (s 2) (e (quote (("crossterm" "dep:crossterm"))))))

