(define-module (crates-io #{3}# c crf) #:use-module (crates-io))

(define-public crate-crf-0.1.0 (c (n "crf") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "028j1kqqp29mphxk7qkr12bf2c0y5nzk488gd3h6h1m6vbf2x3j0")))

(define-public crate-crf-0.1.1 (c (n "crf") (v "0.1.1") (d (list (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "11i6dclz2ldvg2d72smm9palmcnr19mxgyhkx1lkddbk0j50i0yj")))

(define-public crate-crf-0.1.2 (c (n "crf") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0y2vrw9ikpgbkrxjf2ph10hjlvrqiz02clrj1jc19mj45apsh7vh")))

