(define-module (crates-io #{3}# c csl) #:use-module (crates-io))

(define-public crate-csl-0.0.1 (c (n "csl") (v "0.0.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "indoc") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "roxmltree") (r "^0.6.1") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("rc"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "string_cache") (r "^0.7.3") (d #t) (k 0)) (d (n "strum") (r "^0.15.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.15.0") (d #t) (k 0)))) (h "1lhyz9203ll47067iqp1icg8nqb4fb0d7x712h0fqnh8sl5rphq2")))

