(define-module (crates-io #{3}# c ctx) #:use-module (crates-io))

(define-public crate-ctx-0.1.0 (c (n "ctx") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "10wmzmh6hspl1pf385585rhvr372rk4zmqxlv7fxyvyg7nabnzs1")))

(define-public crate-ctx-0.1.1 (c (n "ctx") (v "0.1.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "1g0bnr0n60n9yis34jnmjmqapj8vd94rqyv88x02bfj9jigmr8cb")))

(define-public crate-ctx-0.1.2 (c (n "ctx") (v "0.1.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "0bgga0k2ywmsngk1s303kgi8znbsa5s2cl7y4h919jkh0sj0d8si")))

(define-public crate-ctx-0.2.0 (c (n "ctx") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "tokio-timer") (r "^0.1") (d #t) (k 0)))) (h "00nc3sq5mbbhhrnnfnsqkklkc00gc6l7fv065gi2fg132n1bqs3g")))

