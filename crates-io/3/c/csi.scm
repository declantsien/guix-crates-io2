(define-module (crates-io #{3}# c csi) #:use-module (crates-io))

(define-public crate-csi-1.0.0 (c (n "csi") (v "1.0.0") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "00psq3kvc9svcnqadm0hympgz327cc1gi7dqgmijvgyx264b2r0h")))

(define-public crate-csi-1.0.0-20-ge0b8331 (c (n "csi") (v "1.0.0-20-ge0b8331") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1wfiralhnpwsiw5h2n0x3zfvbmy8ikrhf72milidzzliqqc02wd5") (y #t)))

(define-public crate-csi-0.0.1 (c (n "csi") (v "0.0.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0b30a909ypz78acy5ghpwvg56aqq6rnl1gjza38p4kz7p3jihdpl") (y #t)))

(define-public crate-csi-0.9.8 (c (n "csi") (v "0.9.8") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0y6lw3bjj0m0bxkzz2xblfz0r28qfv0jr1icyqrm2p8q92s43mhn") (y #t)))

(define-public crate-csi-0.9.9 (c (n "csi") (v "0.9.9") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "199fvsdc7vkbs1i6zr5hwmkl44aba6kvx0714g6vzh5v8ikjga8b") (y #t)))

(define-public crate-csi-1.0.1 (c (n "csi") (v "1.0.1") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0z5cjkpq6y5xifp5vmyfdc9s02rfn5cnskbjlpzjcwp1hqn5n3w3")))

(define-public crate-csi-1.0.2 (c (n "csi") (v "1.0.2") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0nvbksh9kai308sq3agbh75lsy5gy7rp854pm1bzp2v90q4hjacs")))

