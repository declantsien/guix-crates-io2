(define-module (crates-io #{3}# c cue) #:use-module (crates-io))

(define-public crate-cue-0.1.0 (c (n "cue") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "syncbox") (r "^0.2") (d #t) (k 0)))) (h "12glymb3p9kh5jrrd0h39cmabzrjrzi03gmikg0pl0j2xd3lbp81") (f (quote (("default" "log"))))))

(define-public crate-cue-1.0.0 (c (n "cue") (v "1.0.0") (d (list (d (n "cue-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "0ig64ygrd7zy3zckh9qgfrm0p0d1wp2ni7ybr77v628k6nk7mjix")))

(define-public crate-cue-1.0.1 (c (n "cue") (v "1.0.1") (d (list (d (n "cue-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "15f752zh24ni4d386lic95zaghhr08236kfin0zkpr2azhbnbgbj")))

(define-public crate-cue-1.1.0 (c (n "cue") (v "1.1.0") (d (list (d (n "cue-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "1mij1yv7n2886cl4ap86xx3daq1gj0rn66wvnv5mmimxfbg17vsb")))

(define-public crate-cue-2.0.0 (c (n "cue") (v "2.0.0") (d (list (d (n "cue-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "1bylq18yx30mc7grrqlkcd6mrjspchz9f25imr7crk1ncx6cz2xa")))

