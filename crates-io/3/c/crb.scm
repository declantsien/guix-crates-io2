(define-module (crates-io #{3}# c crb) #:use-module (crates-io))

(define-public crate-crb-0.0.0 (c (n "crb") (v "0.0.0") (h "1ksrinsfswmmskmimhhkpl57ph5kxwjddbd5g8hkcvk4q5ma9h0w")))

(define-public crate-crb-0.0.1 (c (n "crb") (v "0.0.1") (h "0kmnwjn1z06a67bnvphvhhxsn21qbzh6y9ma4jxsr0a0nz6i54yf")))

(define-public crate-crb-0.0.2 (c (n "crb") (v "0.0.2") (h "12m23hl4qdqjx2406sifp8pa3vl1dikakzyx61faw66ll0xq6n8w")))

(define-public crate-crb-0.0.3 (c (n "crb") (v "0.0.3") (h "05zphj9phmrf4g3x67r8717ww6zw07vlmdybvicc3cp03k8sdav4")))

(define-public crate-crb-0.0.4 (c (n "crb") (v "0.0.4") (h "0l68ky625ws016n0jy0gpfng52mh18r69n8gm8g3iqvmjpzf0a3a")))

