(define-module (crates-io #{3}# c cra) #:use-module (crates-io))

(define-public crate-cra-0.1.0 (c (n "cra") (v "0.1.0") (d (list (d (n "infer") (r "^0.15.0") (d #t) (k 0)) (d (n "sevenz-rust") (r "^0.5.4") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "09aq5jz9405zhl4bna26nahlndzbvvmbbhaqx2vcfh6dnzy9drxc")))

(define-public crate-cra-0.1.1 (c (n "cra") (v "0.1.1") (d (list (d (n "infer") (r "^0.15.0") (d #t) (k 0)) (d (n "sevenz-rust") (r "^0.5.4") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1633nyl0xxvgraqnhdvfccyjsdw9k9yh6vahycvl3qhmhqicdhm7")))

(define-public crate-cra-0.1.2 (c (n "cra") (v "0.1.2") (d (list (d (n "infer") (r "^0.15.0") (d #t) (k 0)) (d (n "sevenz-rust") (r "^0.6.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "uzers") (r "^0.12.0") (d #t) (k 0)) (d (n "zip") (r "^1.1.4") (d #t) (k 0)))) (h "171j93ndqznxj14qc5l0rm8a81l80jh0gri8llwdh02fhlp1bxba")))

(define-public crate-cra-0.1.3 (c (n "cra") (v "0.1.3") (d (list (d (n "infer") (r "^0.15.0") (d #t) (k 0)) (d (n "sevenz-rust") (r "^0.6.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "uzers") (r "^0.12.0") (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "zip") (r "^1.1.4") (d #t) (k 0)))) (h "14r58xnnbrda1njnjb1c8w5qsxl2jhzhss6pa0fvnkd8kz36yf0g")))

