(define-module (crates-io #{3}# c cff) #:use-module (crates-io))

(define-public crate-cff-0.1.0 (c (n "cff") (v "0.1.0") (d (list (d (n "microbench") (r "^0.3.2") (d #t) (k 2)) (d (n "sfnt") (r "^0.5.0") (d #t) (k 2)) (d (n "tarrasque") (r "^0.4.0") (d #t) (k 0)))) (h "0ikgv1ipw4zqkc89lgb3qfxsd0i57dn7n3dshhh83h3rws80q9f1")))

(define-public crate-cff-0.2.0 (c (n "cff") (v "0.2.0") (d (list (d (n "microbench") (r "^0.3.2") (d #t) (k 2)) (d (n "sfnt") (r "^0.7.0") (d #t) (k 2)) (d (n "tarrasque") (r "^0.5.0") (d #t) (k 0)))) (h "1hxhgn8s1dwpn6fqzmqxcclg5k9vc3dyl45scfs2dc57mzy0mhx5")))

(define-public crate-cff-0.3.0 (c (n "cff") (v "0.3.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "sfnt") (r "^0.8") (d #t) (k 2)) (d (n "tarrasque") (r "^0.6") (d #t) (k 0)) (d (n "tarrasque-macro") (r "^0.6") (d #t) (k 0)))) (h "0xzikd4iqdv0va3vmajq9z21nr44xdjjiv1ms2kiyvryqvpihifz")))

(define-public crate-cff-0.3.1 (c (n "cff") (v "0.3.1") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "sfnt") (r "^0.8") (d #t) (k 2)) (d (n "tarrasque") (r "^0.7") (d #t) (k 0)))) (h "06lvly3bk8icqrjflwmf979w686c189vxmihkyid6dki9ibkmg53")))

(define-public crate-cff-0.4.0 (c (n "cff") (v "0.4.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "sfnt") (r "^0.10") (d #t) (k 2)) (d (n "tarrasque") (r "^0.9") (d #t) (k 0)))) (h "1975zjg5x0fl4z98pjj2dj2fp4rl49m1h0p0yakfrrz9g23n9z8d")))

(define-public crate-cff-0.5.0 (c (n "cff") (v "0.5.0") (d (list (d (n "microbench") (r "^0.3") (d #t) (k 2)) (d (n "sfnt") (r "^0.12") (d #t) (k 2)) (d (n "tarrasque") (r "^0.10") (d #t) (k 0)))) (h "1s14681lh5ki0x4jrlj3x9jl85wyl603f1b1zfv3bx05q4a06ih3")))

