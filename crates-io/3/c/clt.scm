(define-module (crates-io #{3}# c clt) #:use-module (crates-io))

(define-public crate-clt-0.0.1 (c (n "clt") (v "0.0.1") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1fp3zippxzdjq488zqdriyqn5gbdz00wjwgxq61ia6zpxycf956n") (y #t)))

(define-public crate-clt-0.0.2 (c (n "clt") (v "0.0.2") (d (list (d (n "getopts") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "067chapvq41x2b8mka8hz0xvagihh73amnnm6f5qsz7kcc6i5z3i") (y #t)))

(define-public crate-clt-0.0.3 (c (n "clt") (v "0.0.3") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "1r37arz62wsy1blaq3l50bim1hq35gk67hck9apcyn2ll7zjblf6") (y #t)))

(define-public crate-clt-0.0.4 (c (n "clt") (v "0.0.4") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "10b8j9f45nvq6161691x1y1x9v21yxqwz1cy5bhln7cr8ibghn65") (y #t)))

(define-public crate-clt-0.0.5 (c (n "clt") (v "0.0.5") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "0ag9d493ahv4hjhg2xspbx316xsw1jq2vacr9bdnm39494gakvc4") (y #t)))

(define-public crate-clt-0.0.6 (c (n "clt") (v "0.0.6") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "libc") (r "^0.2.12") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)))) (h "185gsrq5qsmj4f9za5gjf7mmvrw62znd5pns99bgx6xzgqimpa5d")))

