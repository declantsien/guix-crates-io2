(define-module (crates-io #{3}# c cm2) #:use-module (crates-io))

(define-public crate-cm2-0.0.0 (c (n "cm2") (v "0.0.0") (h "098h0rjv3hm1rqv7p1wq46y38brkmy5jxmfzi8wxn6x73np1lygs") (y #t)))

(define-public crate-cm2-0.0.1 (c (n "cm2") (v "0.0.1") (h "05qwy2rnx5s6n6pq17s23h6nj4ikjcyl2yvwdym5iswavh7qya5s") (y #t)))

(define-public crate-cm2-0.9.0 (c (n "cm2") (v "0.9.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "0gr8y30yln2l1na7i9jj8r894mgqf7r6f2mwim899mjlja1in0kj") (y #t)))

