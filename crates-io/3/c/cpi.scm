(define-module (crates-io #{3}# c cpi) #:use-module (crates-io))

(define-public crate-cpi-0.1.0 (c (n "cpi") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)))) (h "1qbcfppkja1gib5cb9ar7vkvlbdvrj310fq1zid2p1rv42w7g2gj")))

(define-public crate-cpi-0.2.0 (c (n "cpi") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "14cjpsqckslg9vyvkgqv7mqb9lhil0zy638nrkdrp3cinvklh4px")))

