(define-module (crates-io #{3}# c cmb) #:use-module (crates-io))

(define-public crate-cmb-0.1.0 (c (n "cmb") (v "0.1.0") (h "0psjpqhpw8r257rw61na0pgl3fq70ngngp379jzg3h17r65dl9bx") (y #t)))

(define-public crate-cmb-0.0.1 (c (n "cmb") (v "0.0.1") (h "0sdkysnp4996j690h4axkrzsm71ck7kggxfpx0jdp0h7b7nff1v2")))

(define-public crate-cmb-0.0.2 (c (n "cmb") (v "0.0.2") (h "00pzc0bc9z5mi2x9v83x600vll9ysj3i0gbacib3kb2y2p004dx0")))

