(define-module (crates-io #{3}# c csr) #:use-module (crates-io))

(define-public crate-csr-0.6.0 (c (n "csr") (v "0.6.0") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0xrvsdri2414qdj7zg53jrgyy7lidrm3hw8madpqmz4nz2ahhknl")))

(define-public crate-csr-0.6.1 (c (n "csr") (v "0.6.1") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1bqxc86vkxqx3h9wxcn29193s0f02vqfmbxggiy3wa3jnp1w3vyj")))

(define-public crate-csr-0.6.2 (c (n "csr") (v "0.6.2") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "17iv06dgr4rxargsmh3afg9bq37qkxp7nyygq761zm2zcrb9pqf7")))

(define-public crate-csr-0.6.3 (c (n "csr") (v "0.6.3") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0sl2mkwcfcd8iizqngjbr6ip1f1m8lcg51lm8799vna4476kq26m")))

(define-public crate-csr-0.6.4 (c (n "csr") (v "0.6.4") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "02nlxrs3kc2p7fdp3hc400y4vd7pcn9sf4rr3g2s5gp75mn4h3g4")))

(define-public crate-csr-0.7.0 (c (n "csr") (v "0.7.0") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "0i16zj0pfsjjsygw3mdc58jvml4qazk2fsfj1g4hy48kg6bs8rc3")))

(define-public crate-csr-0.8.0 (c (n "csr") (v "0.8.0") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "19744dyil99sah2bdmiba91lvi6wxvmv53jhpxxylv5gnqwispih")))

(define-public crate-csr-0.8.1 (c (n "csr") (v "0.8.1") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1637cyzrsk7zwd6m1azx9w52pgxjpfgy06c9p3458hmdyp90yfps")))

(define-public crate-csr-0.8.2 (c (n "csr") (v "0.8.2") (d (list (d (n "num") (r "^0.2.1") (d #t) (k 0)))) (h "1izfwy86989sg50w7zkbc7rq3cbibwwpk8ylkqhdyh9i1xiimgvv")))

