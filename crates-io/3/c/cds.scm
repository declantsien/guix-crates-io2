(define-module (crates-io #{3}# c cds) #:use-module (crates-io))

(define-public crate-cds-0.0.1 (c (n "cds") (v "0.0.1") (h "1alxk5raf6p8p4fyvw5ccn1szlxx7vark9v6vwcmdcn5da07lggr")))

(define-public crate-cds-0.0.2 (c (n "cds") (v "0.0.2") (d (list (d (n "heapless") (r "^0.7.8") (d #t) (k 2)))) (h "03wzyz64hmi0c6sxhxfmki7f3im8zggnjzjz2mr9x429y1b8807n") (f (quote (("std") ("default") ("arrayvec"))))))

(define-public crate-cds-0.0.3 (c (n "cds") (v "0.0.3") (h "1s9l8yn8lfpjwk6vla7s83wh1r9326f6gbk4vkxiqwvcglxsqlkd") (f (quote (("std") ("default") ("arrayvec"))))))

(define-public crate-cds-0.0.4 (c (n "cds") (v "0.0.4") (h "0a1hk1gsz63pchvsakl6vk7jgw67wj7q40pps31fml3cq54mhyp3") (f (quote (("std") ("default") ("arrayvec"))))))

(define-public crate-cds-0.0.5 (c (n "cds") (v "0.0.5") (h "1ckkly8bxcxw03kggqm7yk7is10d6hrhyjfa9n07j75yxvy6pxdq") (f (quote (("std") ("default") ("arrayvec"))))))

(define-public crate-cds-0.0.6 (c (n "cds") (v "0.0.6") (h "000g3kgh6xnr3dqsdgxamc7fyykyfxwkqjc8l89q9dn34dk1k8qq") (f (quote (("std") ("default") ("arrayvec"))))))

(define-public crate-cds-0.1.0 (c (n "cds") (v "0.1.0") (h "1pjk1jf17xdz3f772phj8jiazhbgqp2wlyqj7qdvwgpvk0r22mmc") (f (quote (("std") ("default") ("arrayvec"))))))

(define-public crate-cds-0.2.0 (c (n "cds") (v "0.2.0") (h "0z8mijb1qzav9azkcfh3lkm5v3nvkv92rb8k2wf7ahrh41fk18fd") (f (quote (("std") ("default") ("arrayvec"))))))

(define-public crate-cds-0.3.0 (c (n "cds") (v "0.3.0") (h "0nk7svqs4rz6ni5wd7x3crrw2fbq9w7zkwif7fm6wr0d3n196xml") (f (quote (("std" "alloc") ("default") ("arrayvec") ("arraystring") ("alloc"))))))

(define-public crate-cds-0.4.0 (c (n "cds") (v "0.4.0") (h "1h0ff4f9ycwb6lb2qlscdby89j6iqsb8j1mxvsyra0nnq7wg0bx4") (f (quote (("std" "alloc") ("default") ("arrayvec") ("arraystring") ("alloc"))))))

(define-public crate-cds-0.5.0 (c (n "cds") (v "0.5.0") (h "0jghs0g3zxvvga8b5hk4lqpawhibzpza10mnl44q87pns1wfcjfi") (f (quote (("std" "alloc") ("default") ("arrayvec") ("arraystring") ("alloc"))))))

(define-public crate-cds-0.6.0 (c (n "cds") (v "0.6.0") (h "0gxh60f4v8id25k3xj68ann8y2wdnmib9klwi8kyi4wf0smpw1d2") (f (quote (("std" "alloc") ("default") ("arrayvec") ("arraystring") ("alloc"))))))

(define-public crate-cds-0.7.0 (c (n "cds") (v "0.7.0") (h "0yq2ni2nsr9dd1nc6wy35xnsp3v8w5ijaagkw9pq9j4kacshdn98") (f (quote (("std" "alloc") ("default") ("arrayvec") ("arraystring") ("alloc"))))))

(define-public crate-cds-0.8.0 (c (n "cds") (v "0.8.0") (h "18kh8pgkmg993b38a7qyrgrx52y4wh44p0hi5mr9iqkpa9p1rbga") (f (quote (("std" "alloc") ("smallvec" "alloc") ("default") ("arrayvec") ("arraystring") ("alloc"))))))

(define-public crate-cds-0.8.1 (c (n "cds") (v "0.8.1") (h "0skhcviif47hmvbl8pbypjf4z05n4mzkbh5yiicsahi3yj0v895k") (f (quote (("std" "alloc") ("smallvec" "alloc") ("default") ("arrayvec") ("arraystring") ("alloc"))))))

(define-public crate-cds-0.9.0 (c (n "cds") (v "0.9.0") (h "16i6n04psvc6swk45inxam93qngc110klnaqv1chql7g4h7x60ia") (f (quote (("std" "alloc") ("smallvec" "alloc") ("default" "std" "arrayvec" "arraystring" "smallvec") ("arrayvec") ("arraystring") ("alloc"))))))

(define-public crate-cds-0.10.0 (c (n "cds") (v "0.10.0") (h "1y06inaia44wagh4hfh3hziicwnmq590i19hk1bzn9siglxw175z") (f (quote (("std" "alloc") ("smallvec" "alloc") ("default" "std" "arrayvec" "arraystring" "smallvec") ("arrayvec") ("arraystring") ("alloc"))))))

