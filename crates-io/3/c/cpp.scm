(define-module (crates-io #{3}# c cpp) #:use-module (crates-io))

(define-public crate-cpp-0.0.1 (c (n "cpp") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1mn6knzgk5jc818yvn5yl529rb67b81nihqqdg9fa9wb80qzbiib")))

(define-public crate-cpp-0.0.2 (c (n "cpp") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0wqljdmlkhqma3dk68ssh9fz3skw5hzb1y7wd5alb49rlszyg59m")))

(define-public crate-cpp-0.0.3 (c (n "cpp") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1h4382as9jclkmccwwkw2cpnh9przn6x1c7sm71192x882q8rakv")))

(define-public crate-cpp-0.0.4 (c (n "cpp") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0dv46ni17f36fqhmh2cbqnbz681q4iqa99wgc9lmzj733km673fm")))

(define-public crate-cpp-0.0.5 (c (n "cpp") (v "0.0.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0v14ih26gn1rrawl354xba1aszddsvbq4bj5vvgg38fqvcyl2zrk")))

(define-public crate-cpp-0.0.6 (c (n "cpp") (v "0.0.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "16p799smbfnlslyvi8pgjgzg82iajj66vf1zhjriikzmlp8pc2fw")))

(define-public crate-cpp-0.0.10 (c (n "cpp") (v "0.0.10") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "12a3jhsrs7s81g07z3cxir0sb1fnlc5swyli6vyjbjqravjxxgyv")))

(define-public crate-cpp-0.0.12 (c (n "cpp") (v "0.0.12") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0rh9pl5azh4zrhs04676l9k0960p57axsfgvr6kmp6ra8cyidj1w")))

(define-public crate-cpp-0.0.13 (c (n "cpp") (v "0.0.13") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1526c88234yivrjd0zv2585c70lg64xaxhfhmxadp62rjw4y7ra0")))

(define-public crate-cpp-0.0.14 (c (n "cpp") (v "0.0.14") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0x73k7im810i1d3xgaxs4k4kqlzh055aya5j79jk2ijm2pisllwv")))

(define-public crate-cpp-0.0.16 (c (n "cpp") (v "0.0.16") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0g1i8ry9yicr0nc5fq1n2fni3i7xmd34kl2zyz1a6hs5x2pnhq48")))

(define-public crate-cpp-0.0.17 (c (n "cpp") (v "0.0.17") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1k60r6306hi8j7wv2lvzwsny8x4dhjczryy5h40ygbmrfza5m8lg")))

(define-public crate-cpp-0.0.18 (c (n "cpp") (v "0.0.18") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "0ri5flgwbmy3smcd9isy8z9bjwq7lriwx44a10nimn0pjbdjbcfq")))

(define-public crate-cpp-0.0.19 (c (n "cpp") (v "0.0.19") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "1mi6npnnilc9hi7m29dxfgpp91hanikcgw9j917fwcn6sy93ww7f")))

(define-public crate-cpp-0.1.0 (c (n "cpp") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42") (o #t) (d #t) (k 0)))) (h "02qa5x40az7zndxsf7h9jms03mixjancpfjsh5brqgd8ky4k23m2") (f (quote (("macro") ("build" "gcc" "syntex_syntax"))))))

(define-public crate-cpp-0.1.1 (c (n "cpp") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.42") (o #t) (d #t) (k 0)))) (h "0c8q4h5iqqkik8zk302yn2wqv1973ja32g5hiqq0sfj8rijqblhi") (f (quote (("macro") ("build" "gcc" "syntex_syntax"))))))

(define-public crate-cpp-0.2.0 (c (n "cpp") (v "0.2.0") (h "1zrs9dg5b7aaava783dlkkc0gjnv6glzd6vy9jx47vi5z80bvjqf")))

(define-public crate-cpp-0.2.1 (c (n "cpp") (v "0.2.1") (h "14l3a9sckbfl3i8i26azbcfrj2kjg4fasqzv8kh7ag5pv46sfxx8")))

(define-public crate-cpp-0.3.0 (c (n "cpp") (v "0.3.0") (d (list (d (n "cpp_macros") (r "= 0.3.0") (d #t) (k 0)))) (h "06npsvfpjfpi6l91x4rra3i7rx3mw1v3d0gr6z97isdygk8y6wif")))

(define-public crate-cpp-0.3.1 (c (n "cpp") (v "0.3.1") (d (list (d (n "cpp_macros") (r "= 0.3.1") (d #t) (k 0)))) (h "0552x194cqq0f8xqkbvzkg833mpvvnjbbfwxhzqdvhrxija05nab")))

(define-public crate-cpp-0.3.2 (c (n "cpp") (v "0.3.2") (d (list (d (n "cpp_macros") (r "= 0.3.2") (d #t) (k 0)))) (h "0z6mmw2lxvqjgzwizq6m0qf5mzwdfqy9dyg7sqkp5qi8q0vcyi5j")))

(define-public crate-cpp-0.4.0 (c (n "cpp") (v "0.4.0") (d (list (d (n "cpp_macros") (r "= 0.4.0") (d #t) (k 0)))) (h "1zq7lmqllamlwglrbv66spdysi855rvgg0w173yih6zskxlxh71d")))

(define-public crate-cpp-0.5.0 (c (n "cpp") (v "0.5.0") (d (list (d (n "cpp_macros") (r "= 0.5.0") (d #t) (k 0)))) (h "1zy1sraxxpihajwxz2n3i3lg88jpqbkqknasp0icbldk2rzd9b8a")))

(define-public crate-cpp-0.5.1 (c (n "cpp") (v "0.5.1") (d (list (d (n "cpp_macros") (r "= 0.5.1") (d #t) (k 0)))) (h "1krs3x3lx00sq9b7vjqqhprwhvv2vpcl41blb9fhs24326s8mqbj")))

(define-public crate-cpp-0.5.2 (c (n "cpp") (v "0.5.2") (d (list (d (n "cpp_macros") (r "= 0.5.2") (d #t) (k 0)))) (h "1cz4xlypf31mlwkshjg2hx3aigf2bnfhspnzr1kdzjgjy3qngpr8")))

(define-public crate-cpp-0.5.3 (c (n "cpp") (v "0.5.3") (d (list (d (n "cpp_build") (r "= 0.5.3") (d #t) (k 2)) (d (n "cpp_macros") (r "= 0.5.3") (d #t) (k 0)))) (h "0lfz83p74nnjwvcykyjcahi1v7189c24mzgchs4llhjngaqhq6b5")))

(define-public crate-cpp-0.5.4 (c (n "cpp") (v "0.5.4") (d (list (d (n "cpp_build") (r "= 0.5.4") (d #t) (k 2)) (d (n "cpp_macros") (r "= 0.5.4") (d #t) (k 0)))) (h "1gb139xvrxyz934506ris8k87kwf9mwa4n9xa3gw41fn1w9lvnfh")))

(define-public crate-cpp-0.5.5 (c (n "cpp") (v "0.5.5") (d (list (d (n "cpp_build") (r "=0.5.5") (d #t) (k 2)) (d (n "cpp_macros") (r "=0.5.5") (d #t) (k 0)))) (h "07ygmr4g67haf3m3g9izc42b1pi7n9r8y7w57gwp5z0i7fg6ggb8")))

(define-public crate-cpp-0.5.6 (c (n "cpp") (v "0.5.6") (d (list (d (n "cpp_build") (r "=0.5.6") (d #t) (k 2)) (d (n "cpp_macros") (r "=0.5.6") (d #t) (k 0)))) (h "1s2hpy068k8461mdxpl4a7lm41ry9887zq3frg4xqj5y023a0xa8")))

(define-public crate-cpp-0.5.7 (c (n "cpp") (v "0.5.7") (d (list (d (n "cpp_build") (r "=0.5.7") (d #t) (k 2)) (d (n "cpp_macros") (r "=0.5.7") (d #t) (k 0)))) (h "11aib7rp0kwbd7k9ccbn7gw1x9s4h98dj8yri8hz0iv59xnyiify")))

(define-public crate-cpp-0.5.8 (c (n "cpp") (v "0.5.8") (d (list (d (n "cpp_build") (r "=0.5.8") (d #t) (k 2)) (d (n "cpp_macros") (r "=0.5.8") (d #t) (k 0)))) (h "1sq2dbcvhz0c69kqznqy5j1bjy8q6gz0h518xaxn4sipgzaa3dj3")))

(define-public crate-cpp-0.5.9 (c (n "cpp") (v "0.5.9") (d (list (d (n "cpp_build") (r "=0.5.9") (d #t) (k 2)) (d (n "cpp_macros") (r "=0.5.9") (d #t) (k 0)))) (h "0r6d9wa9n1rargci2f9zw6v8qffirl4ala4q1v34agl5xxlmi9mz")))

