(define-module (crates-io #{3}# c cfx) #:use-module (crates-io))

(define-public crate-cfx-0.1.0 (c (n "cfx") (v "0.1.0") (d (list (d (n "cfx-client") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "cfx-core") (r "^0.1.0") (d #t) (k 0)) (d (n "cfx-server") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1cqynnxp463bjpj1mjj6v08mbi1w93inbb6sysxv6d73zjl32df3") (f (quote (("server" "cfx-server") ("default") ("client" "cfx-client"))))))

(define-public crate-cfx-0.1.1 (c (n "cfx") (v "0.1.1") (d (list (d (n "cfx-client") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "cfx-core") (r "^0.1.0") (d #t) (k 0)) (d (n "cfx-server") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1cx85mig39i2dhsv5qbjvpsgb0vvb9hbh7ffv8ysh4m01wc44iqv") (f (quote (("server" "cfx-server") ("default") ("client" "cfx-client"))))))

