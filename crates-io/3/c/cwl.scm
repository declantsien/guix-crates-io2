(define-module (crates-io #{3}# c cwl) #:use-module (crates-io))

(define-public crate-cwl-0.1.0 (c (n "cwl") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.3") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1wdshrhby4dbhndl34r17whc9dgdx2bqzg72rw0bd4dc873m2w7j")))

