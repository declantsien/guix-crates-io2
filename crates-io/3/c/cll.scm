(define-module (crates-io #{3}# c cll) #:use-module (crates-io))

(define-public crate-cll-0.1.0 (c (n "cll") (v "0.1.0") (h "1abkz270fa0zr5m0clc2k3lv6705h0y38sbxjh8fxw2nvrm2iv19")))

(define-public crate-cll-0.1.1 (c (n "cll") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1ipziv3954zki65s57rmp28x1ryjbzdm8dk5gyxma2fycj9mwvpj")))

