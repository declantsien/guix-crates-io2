(define-module (crates-io #{3}# c chn) #:use-module (crates-io))

(define-public crate-chn-0.0.0 (c (n "chn") (v "0.0.0") (h "11f1nizw3jaa5h4qzl57nd9f55a6g5myq9r0jbrw63yf8ban07d7")))

(define-public crate-chn-0.1.0 (c (n "chn") (v "0.1.0") (h "1ys2g61hm7jwphlgik48vy6qjbpg7f7akbcib2yqddq4zikfq6h6")))

(define-public crate-chn-0.1.1 (c (n "chn") (v "0.1.1") (h "1dna9nh12b7kv3h4246965hw0ydmh807qkzj13g2csjdpzmj2xkf")))

