(define-module (crates-io #{3}# c cld) #:use-module (crates-io))

(define-public crate-cld-0.1.0 (c (n "cld") (v "0.1.0") (h "16dlrd2l3if43lb67mi772rrvvc2kl59h58ck1x1rrzbmnq1bydk") (f (quote (("std"))))))

(define-public crate-cld-0.2.0 (c (n "cld") (v "0.2.0") (h "15qa7rxwnl88ickvmrcl909vqbqlr0djpyrqxsjzll996j3rwgw7") (f (quote (("std"))))))

(define-public crate-cld-0.2.1 (c (n "cld") (v "0.2.1") (h "1zvr7b2n3b6i583hagdddmx7grs5y690n77a8qjvzxjp3sdf8gzf") (f (quote (("std"))))))

(define-public crate-cld-0.3.0 (c (n "cld") (v "0.3.0") (h "183n87l4ljvh53hmvfx4xwfdyvqj98al8yg4jz29p78bag8sab5f") (f (quote (("std"))))))

(define-public crate-cld-0.4.0 (c (n "cld") (v "0.4.0") (h "172d239mcpcx0dfi1qscgaf219qsxnk9cxmsyvs92lgngys27zqm") (f (quote (("std"))))))

(define-public crate-cld-0.4.1 (c (n "cld") (v "0.4.1") (h "1bmsrj1ykyjjx3wdc3sjrcrk6srg5zylgjx0v7pcvygb4df6bqww") (f (quote (("std"))))))

(define-public crate-cld-0.4.2 (c (n "cld") (v "0.4.2") (h "1pqx1f9agi0nj4v91y42in3731184irpk00cghh4yjqbf02qqav4") (f (quote (("std"))))))

(define-public crate-cld-0.5.0 (c (n "cld") (v "0.5.0") (h "1pndlkby0s1c01in0ip4ski07wqy1lsaxb7cqkvv6rkr7hicay3a") (f (quote (("std"))))))

(define-public crate-cld-0.5.1 (c (n "cld") (v "0.5.1") (h "1xdsc83hpb38q4wc6kl73n01c7xq4flxcfhb3wfczvyvlfq80402") (f (quote (("std"))))))

