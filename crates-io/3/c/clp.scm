(define-module (crates-io #{3}# c clp) #:use-module (crates-io))

(define-public crate-clp-0.1.0 (c (n "clp") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "03jcv4scw0y9pm6pgj1kdxmn6zz0p79g0ss82a6x7gf1bkls02zr")))

(define-public crate-clp-0.2.0 (c (n "clp") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "02cl7w6pf74pim2abhzvh0g01v812p1krfaa7695ammkz8dyw1h3")))

(define-public crate-clp-0.3.0 (c (n "clp") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "spin_sleep") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "0zwa9dmnf4n7qcvfl9wzz8nl8nfpvh9bk65nrqzqhvw63hn5s3jw")))

(define-public crate-clp-0.4.0 (c (n "clp") (v "0.4.0") (d (list (d (n "artem") (r "^1.1.7") (d #t) (k 2)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (f (quote ("jpeg"))) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "012v2ysl4kgpx91c7jvbhrk799icy1nlmpsc6qczr8gd4g8h347j")))

(define-public crate-clp-0.5.0 (c (n "clp") (v "0.5.0") (d (list (d (n "artem") (r "^1.1.7") (d #t) (k 2)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "figlet-rs") (r "^0.1.5") (d #t) (k 2)) (d (n "image") (r "^0.24.6") (f (quote ("jpeg"))) (k 2)) (d (n "spin_sleep") (r "^1.1.1") (o #t) (d #t) (k 0)))) (h "06yf8r86a7n8jzrb1xh76q520k1sg5qd7b3b461dvz5iswkhhfjx")))

