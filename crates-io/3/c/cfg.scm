(define-module (crates-io #{3}# c cfg) #:use-module (crates-io))

(define-public crate-cfg-0.0.1 (c (n "cfg") (v "0.0.1") (h "0b1x6g9hrpc1cxjpjjsmma6fifp397mxjixhi5ccbbm1wxci2kl8")))

(define-public crate-cfg-0.0.2 (c (n "cfg") (v "0.0.2") (d (list (d (n "bit-matrix") (r "^0.0.1") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)))) (h "0qviyh8j4qqxkmxgxyv1mg6dqhg85slyara8j9a17anc86yg2c0k")))

(define-public crate-cfg-0.2.0 (c (n "cfg") (v "0.2.0") (d (list (d (n "bit-matrix") (r "^0.1") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)))) (h "0c76lzi6binbcdks75jm9796bl4pj7f8p91r2j0ayqa5znp0rpwx")))

(define-public crate-cfg-0.3.0 (c (n "cfg") (v "0.3.0") (d (list (d (n "bit-matrix") (r "^0.2") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)))) (h "05npyfp7vrxj4jx595ki6ry7zli86a2ayh1d5licmipxbgvk5wf0")))

(define-public crate-cfg-0.4.0 (c (n "cfg") (v "0.4.0") (d (list (d (n "bit-matrix") (r "^0.4") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0q29ycqfl7r9vszrp7cbb6m1hcvcm73cfgpz28xc71b5w855n18a")))

(define-public crate-cfg-0.5.0 (c (n "cfg") (v "0.5.0") (d (list (d (n "bit-matrix") (r "^0.6") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "12d5j9bwyrqv81s0y3zj2kn652sqygqrp5ibcr89v767la028dl1")))

(define-public crate-cfg-0.6.0 (c (n "cfg") (v "0.6.0") (d (list (d (n "bit-matrix") (r "^0.6") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "108ydf3z9rxqg1rzwd7r5vnwmn68ls4i1c5sy0mfdl7fhlvn2imi") (f (quote (("serialize" "serde" "serde_derive") ("generation" "rand" "num"))))))

(define-public crate-cfg-0.6.1 (c (n "cfg") (v "0.6.1") (d (list (d (n "bit-matrix") (r "^0.6") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "131mc08cw3fiql4d1kz455y7f2fklq01gdv22hq9abxvrakqvy2z") (f (quote (("serialize" "serde" "serde_derive") ("generation" "rand" "num"))))))

(define-public crate-cfg-0.6.2 (c (n "cfg") (v "0.6.2") (d (list (d (n "bit-matrix") (r "^0.6") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "06ck1jmhdr6fr6lnhgzqxwf1fig0cphld5c0l8py5w592qsxb9cz") (f (quote (("serialize" "serde" "serde_derive") ("generation" "rand" "num"))))))

(define-public crate-cfg-0.7.0 (c (n "cfg") (v "0.7.0") (d (list (d (n "bit-matrix") (r "^0.6") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "num") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zfw8490kka2nkfgj48ysv35hx1sip6bk11jamcvn1w8g45qjc7z") (f (quote (("serialize" "serde" "serde_derive") ("generation" "rand" "num"))))))

(define-public crate-cfg-0.8.0 (c (n "cfg") (v "0.8.0") (d (list (d (n "bit-matrix") (r "^0.6") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "rpds") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1nsqgri5zxzppdk7j8bbaa2rjranjikdqc7g4v7qplxwihhal5yd") (f (quote (("serialize" "serde" "serde_derive") ("generation" "rand" "num" "rpds" "env_logger" "log"))))))

