(define-module (crates-io #{3}# c cge) #:use-module (crates-io))

(define-public crate-cge-0.0.1 (c (n "cge") (v "0.0.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "0jrv8rg8q4p7py41fml5q5g37q4z4lb0i0l3lww11nl0fhsxq55g")))

(define-public crate-cge-0.1.0 (c (n "cge") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ig9jnhifyk3f2bn2008gks7ndprmll8gvnscg064hmrirxcbf90") (f (quote (("json" "serde" "serde_json") ("default" "serde" "json")))) (r "1.43")))

(define-public crate-cge-0.1.1 (c (n "cge") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "05742x9vxsqskwgf261j6knsvkkfkpi2b7dp023zjr86q9i9vqzv") (f (quote (("json" "serde" "serde_json") ("default" "serde" "json")))) (r "1.43")))

