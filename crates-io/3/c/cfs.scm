(define-module (crates-io #{3}# c cfs) #:use-module (crates-io))

(define-public crate-cfs-0.1.0 (c (n "cfs") (v "0.1.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "seahorse") (r "^2.1.0") (d #t) (k 0)))) (h "14dc0ns2lqknpyy9v06jmr47cn2wqpcbhb1yi4gz974xz7j8nhjx") (y #t)))

(define-public crate-cfs-0.1.1 (c (n "cfs") (v "0.1.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "seahorse") (r "^2.1.0") (d #t) (k 0)))) (h "0g4m098ba22057csf98lakpkqpqifsmyrk5i7xfhixljmq9bwf4a")))

(define-public crate-cfs-0.1.2 (c (n "cfs") (v "0.1.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "seahorse") (r "^2.1.0") (d #t) (k 0)))) (h "1wb8x544xci5kr3wc432k3vxji4rw6xn24sclk8w1kklf27bfh0s")))

(define-public crate-cfs-0.2.0 (c (n "cfs") (v "0.2.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "seahorse") (r "^2.1.0") (d #t) (k 0)))) (h "1yyhmhijgy01p0dfz10nimiw1dyddvpcmh6bmj1x47lmhf42p363") (y #t)))

(define-public crate-cfs-0.2.1 (c (n "cfs") (v "0.2.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "seahorse") (r "^2.1.0") (d #t) (k 0)))) (h "1qdqjdxw586c3rn788i49zqk8a96zldxl9swwgwybwf9585qgw5d")))

(define-public crate-cfs-0.3.0 (c (n "cfs") (v "0.3.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "seahorse") (r "^2.1.0") (d #t) (k 0)))) (h "0lm4s11bsihvbvx88z9q7j20nz6qdl5fpl5n7f1fng2720mr3a7l")))

(define-public crate-cfs-0.3.1 (c (n "cfs") (v "0.3.1") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "seahorse") (r "^2.1.0") (d #t) (k 0)))) (h "0zcghwa965763d3612sbhbhp8208v34fb5kikw95f609q8phcf7y")))

(define-public crate-cfs-0.3.2 (c (n "cfs") (v "0.3.2") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "seahorse") (r "^2.1.0") (d #t) (k 0)))) (h "0cdcinvkpbndcb444grln2jwjylrimilwzn5j0zkr5qy23gs529f")))

(define-public crate-cfs-1.0.0 (c (n "cfs") (v "1.0.0") (d (list (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "seahorse") (r "^2.1.0") (d #t) (k 0)))) (h "0qh6vc93i5aav6fyyynjabh6vj95llnb55s1ggw4lq7xymg9937a")))

