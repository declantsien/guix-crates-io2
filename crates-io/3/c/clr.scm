(define-module (crates-io #{3}# c clr) #:use-module (crates-io))

(define-public crate-clr-0.2.0 (c (n "clr") (v "0.2.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0") (d #t) (k 0)))) (h "0niapchpwbianfabn9bk8lbn7f2h834p13vhdb1hlbv9vny5c54v")))

(define-public crate-clr-0.3.0 (c (n "clr") (v "0.3.0") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0") (d #t) (k 0)))) (h "1nxyz1hzal9jb8vsx8x002wzjl9dmxfszddzcy5k5h3d5qk64smq")))

(define-public crate-clr-0.3.1 (c (n "clr") (v "0.3.1") (d (list (d (n "csv") (r "^1") (d #t) (k 0)) (d (n "encoding_rs") (r "^0") (d #t) (k 0)))) (h "0n777q6j1l32qm78kf38bxgqr3cr15xa6yjg8x8b4c9y49zwax1g")))

