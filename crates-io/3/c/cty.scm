(define-module (crates-io #{3}# c cty) #:use-module (crates-io))

(define-public crate-cty-0.1.0 (c (n "cty") (v "0.1.0") (h "1nbdf20jmpf2nddv3xggp11ghv09wjrjg1liahh5jczbg35xpkrx") (y #t)))

(define-public crate-cty-0.1.1 (c (n "cty") (v "0.1.1") (h "1h3b90l0xcx9f4vh1q33fvq0h6wsij6hqxqp5lcjgxvjnqa8rkqr") (y #t)))

(define-public crate-cty-0.1.2 (c (n "cty") (v "0.1.2") (h "02gjbks6s4l0a840c4m46g6agfg314zq4qmcnq18bvn9d5fvy6gw") (y #t)))

(define-public crate-cty-0.1.3 (c (n "cty") (v "0.1.3") (h "0am1mqlm53dl3rsnm21qghh1v62ps7i2fhdc7ibl6hsyk8g362ym") (y #t)))

(define-public crate-cty-0.1.4 (c (n "cty") (v "0.1.4") (h "0k4jcp5vzjcwcvvv153kkk77zyg8y3h82d558pfp6y2pl1p9pdkc")))

(define-public crate-cty-0.1.5 (c (n "cty") (v "0.1.5") (h "04ikp490d4sknanahrlnj3smnfv9awsi35q3y0nn2wqm8wfd9qf4")))

(define-public crate-cty-0.2.0 (c (n "cty") (v "0.2.0") (h "0gn2nb3l3vhqg1wpqswg6zfsakshfxlm0jsa2dmgl96hgzilw6hm")))

(define-public crate-cty-0.2.1 (c (n "cty") (v "0.2.1") (h "1qvkdnkxmd7g6fwhmv26zxqi0l7b9cd4d7h1knylvjyh43bc04vk")))

(define-public crate-cty-0.2.2 (c (n "cty") (v "0.2.2") (h "0d8z0pbr87wgzqqb2jk5pvj0afzc6d3rb772ach6fijhg6yglrdk")))

