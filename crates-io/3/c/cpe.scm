(define-module (crates-io #{3}# c cpe) #:use-module (crates-io))

(define-public crate-cpe-0.1.0 (c (n "cpe") (v "0.1.0") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "language-tags") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pp04369ff6qdf9rpngfavcpfkn3x39ww0m4jzw3246swcwglwms") (f (quote (("display") ("default" "display"))))))

(define-public crate-cpe-0.1.1 (c (n "cpe") (v "0.1.1") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03mq672i766lsh1kpffxxp3z8zhs40d5jh6wf66wbkwx1hn5z3kp") (f (quote (("display") ("default" "display"))))))

(define-public crate-cpe-0.1.2 (c (n "cpe") (v "0.1.2") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "151h2h3zdada4jjwiy6gn61b0kff64n30f50jah6k7vs8f57yqxl") (f (quote (("permissive_encoding") ("display") ("default" "display"))))))

(define-public crate-cpe-0.1.3 (c (n "cpe") (v "0.1.3") (d (list (d (n "derive_builder") (r "^0.9") (d #t) (k 0)) (d (n "language-tags") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xpfjp9aj3djqq6w5nq44dqy08ba2bn2v20ciq88chjmw345hw05") (f (quote (("permissive_encoding") ("display") ("default" "display"))))))

