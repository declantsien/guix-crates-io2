(define-module (crates-io #{3}# c cgh) #:use-module (crates-io))

(define-public crate-cgh-0.1.0 (c (n "cgh") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0914n9aznfgh49g32avw4s9av3bqxjag3rsb6ghnqhlrbaccmh67")))

