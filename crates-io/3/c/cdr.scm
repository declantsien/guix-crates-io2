(define-module (crates-io #{3}# c cdr) #:use-module (crates-io))

(define-public crate-cdr-0.2.0 (c (n "cdr") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1cn7zcs8h2fr86zs13f8d10didfx4hb060n796v02yy6nh19sp2x")))

(define-public crate-cdr-0.2.1 (c (n "cdr") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0l8z2qcs7yn6mq2ca1a6lka7s08ws8bzvrlc78j8df03a2qypc3r")))

(define-public crate-cdr-0.2.2 (c (n "cdr") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0bxyywalmd3qmsynb3nbywvl3cizmjyxbq044gib7swkm646y4nn")))

(define-public crate-cdr-0.2.3 (c (n "cdr") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "0vxq2jf82n2jq9x4rg9310ahhbl30w48bymdysx59kkw7mar5g48")))

(define-public crate-cdr-0.2.4 (c (n "cdr") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)))) (h "1yhxsh30ybarcw7hpizmhb0qjhs9yzw90zksf2095pizyhml45wn")))

