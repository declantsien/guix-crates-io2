(define-module (crates-io #{3}# c cqr) #:use-module (crates-io))

(define-public crate-cqr-0.1.2 (c (n "cqr") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "quircs") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking"))) (k 0)))) (h "0gp64as1zl4f1h1hwhlrhrzzn561dafd7s9xrzxwgjkfrzwgci90")))

(define-public crate-cqr-0.1.3 (c (n "cqr") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "quircs") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking"))) (k 0)))) (h "0hvg7bzfkw9cp9spm0vzjidakj86r81pdbbxb47kiwnvf719xdf6")))

(define-public crate-cqr-0.1.4 (c (n "cqr") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "quircs") (r "^0.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking"))) (k 0)))) (h "0xwbn631pglj3sl19af8c2yml1z1nsdcx0n8wl9819h0yx5qalg1")))

