(define-module (crates-io #{3}# c csc) #:use-module (crates-io))

(define-public crate-csc-0.1.0 (c (n "csc") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "0zpdxwm73d2srzcr59arrqarrh7y0f2j745c4rdf103ixdqpgp4l")))

(define-public crate-csc-0.1.1 (c (n "csc") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "1jf6416wi8xg6lx0dvsp2h9ywj1xcggkw3ggqga2lkwnbzgnaxlz")))

(define-public crate-csc-0.1.2 (c (n "csc") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "1wvf4hanmc02g1k8vl2lbk5a10yj6j67wl5bdh1nf947anl5816q")))

(define-public crate-csc-0.1.3 (c (n "csc") (v "0.1.3") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "0y35smj5bc3dr0vfmx1lmil9j46v1lkwrjx9vh9h0n1njjv1h4lq")))

(define-public crate-csc-0.1.4 (c (n "csc") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "16ym71msxf2n4i0r76q1y0jbqasiyqnyyr7gvya3kmzyg2i04b44")))

(define-public crate-csc-0.1.5 (c (n "csc") (v "0.1.5") (d (list (d (n "chainchomp") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "1bwaxr9s05qkrcry4f4jwzn1dcpbmkn17wyafnyl26g6bvi0rgnl")))

(define-public crate-csc-0.1.6 (c (n "csc") (v "0.1.6") (d (list (d (n "chainchomp") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "13yp31ixcml0g4l2hmdgxcb271knz8970pg14swdfs7759k6v0cc")))

(define-public crate-csc-0.1.7 (c (n "csc") (v "0.1.7") (d (list (d (n "chainchomp") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "1pl1dc4r58jclf14ygk86l2i784k0zy2n4lp6ajwf53vck36v9rs")))

(define-public crate-csc-0.1.8 (c (n "csc") (v "0.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chainchomp") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^12") (d #t) (k 0)))) (h "1fd8yn08bji50w35mr68zj3isq5wwg5a0p3bwxixsd6g3j6lxhcl")))

(define-public crate-csc-0.1.9 (c (n "csc") (v "0.1.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "chainchomp") (r "^0.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^14") (d #t) (k 0)))) (h "0y1czkyvf2r59mxi9vfrqh0h9fpw76vf77n3isan628hrq8j0hji")))

