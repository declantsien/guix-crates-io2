(define-module (crates-io #{3}# c cdt) #:use-module (crates-io))

(define-public crate-cdt-0.1.0 (c (n "cdt") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 2)) (d (n "geometry-predicates") (r "^0.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.0") (d #t) (k 2)) (d (n "rusttype") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1141w7s63xm4rih3jw1dr8ssm08nar0l3a9anf3gagpnjhi8f6xr") (f (quote (("long-indexes"))))))

