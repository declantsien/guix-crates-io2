(define-module (crates-io #{3}# c cvx) #:use-module (crates-io))

(define-public crate-cvx-0.0.1 (c (n "cvx") (v "0.0.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "ndarray") (r "^0.13") (d #t) (k 0)) (d (n "osqp") (r "^0.6.0") (d #t) (k 0)))) (h "1nybrfdy8hnay4bn8fk1gy9ki6s6nfwnm3fpjds755vypcz7r8ac")))

