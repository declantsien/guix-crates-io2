(define-module (crates-io #{3}# c cst) #:use-module (crates-io))

(define-public crate-cst-0.1.201 (c (n "cst") (v "0.1.201") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "substring") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0wf44aifxwykyibzfjdmcpny0731nvh0dlf94qbdz7zg266zxrr7")))

(define-public crate-cst-0.1.202 (c (n "cst") (v "0.1.202") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "substring") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "01qd2hfld34n7ahh988smvxzg3hx17bwra0npc6riv59s7ms9bdd")))

(define-public crate-cst-0.1.203 (c (n "cst") (v "0.1.203") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "substring") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1yrpjfwm9aj37hzbzaf1wzibkind66f7v6rwb0b62f8mkiph657x")))

