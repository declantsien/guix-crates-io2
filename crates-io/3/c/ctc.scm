(define-module (crates-io #{3}# c ctc) #:use-module (crates-io))

(define-public crate-ctc-0.1.0 (c (n "ctc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1jv442lp01zf360vslipvs7xr4gn4xcig6ylp8dbj9qklgqhic5a") (y #t)))

(define-public crate-ctc-0.2.0 (c (n "ctc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "0pij1hdlz8pws1p6gm2fys0xxjz4fpq06dh90wy31h80868cxy1w") (y #t)))

(define-public crate-ctc-0.2.1 (c (n "ctc") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1a3bq7nhz62rpak3as46g768k5cngnk235ynqrw5z3syh12z0qg1")))

(define-public crate-ctc-0.2.2 (c (n "ctc") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)))) (h "1b0yyhk75cbppfpyglbzfgx75b0xkh1sq06vvr0drjfiysrydcc6")))

