(define-module (crates-io #{3}# c cuo) #:use-module (crates-io))

(define-public crate-cuo-0.2.0 (c (n "cuo") (v "0.2.0") (d (list (d (n "cargo") (r "^0.26") (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 0)))) (h "01lq1kmfpwx7jhbjj279kwjkr52spi3bv46cynyds6b5z04khg58")))

(define-public crate-cuo-0.3.0 (c (n "cuo") (v "0.3.0") (d (list (d (n "cargo") (r "^0.27") (d #t) (k 0)) (d (n "git2") (r "^0.7") (d #t) (k 0)))) (h "0ccrin3yjmysbl0glk3l63gd1vr9j945w26kyvnnsnxl52q37k26")))

