(define-module (crates-io #{3}# c cuv) #:use-module (crates-io))

(define-public crate-cuv-0.1.0 (c (n "cuv") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1gn34w93lks3iibml0yv3anjdmrqjp5xhhc9kjj9dnjk65cbwfrw")))

