(define-module (crates-io #{3}# c ctp) #:use-module (crates-io))

(define-public crate-ctp-0.1.0 (c (n "ctp") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "05rb6b320mh2fx2wgi89b3j4iry4b35r168rzj02vm24sma91x7q")))

(define-public crate-ctp-0.1.1 (c (n "ctp") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1ikfzwcqfijagix39w76j6mpggjh8xm589snbr63vx5b7aaqjl62")))

(define-public crate-ctp-0.1.2 (c (n "ctp") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "10mk6p94bjvq98k5h74wbr5zfl8kb236bxl0mqzyjhn6s4wi65i5")))

(define-public crate-ctp-0.1.3 (c (n "ctp") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "06vs1q1iar6mw0jgqylzlrzbc2c8f5bwdmibk7a77q79xwgzqzjp")))

