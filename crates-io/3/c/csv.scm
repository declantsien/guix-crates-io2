(define-module (crates-io #{3}# c csv) #:use-module (crates-io))

(define-public crate-csv-0.5.1 (c (n "csv") (v "0.5.1") (h "046xsxsh0gybxzip71vki4ilz6sg7hn82cvqpap7j2fvnh4x5shm")))

(define-public crate-csv-0.5.2 (c (n "csv") (v "0.5.2") (h "06347g27f2cmbwx9hp0yqfw1cqkjxbpl02kq3yr6h39lxpm542c2")))

(define-public crate-csv-0.6.0 (c (n "csv") (v "0.6.0") (h "1m9cy6pakwbv8r44d0x40zvvvvpp4hxxv1i3sm2ccl2j4s5cs4h1")))

(define-public crate-csv-0.6.1 (c (n "csv") (v "0.6.1") (h "1138082p8wbz4ppy4nynpkicpq3ai71jbybd2simb4xcaiqwlxiz")))

(define-public crate-csv-0.6.2 (c (n "csv") (v "0.6.2") (h "16kmc0bjsll1v3yrliybdbvqis3dv4i0np0vdhrq86mcm68b6a1c")))

(define-public crate-csv-0.6.3 (c (n "csv") (v "0.6.3") (h "1bmf18zryw57cpay0jsbyd6kk3m841rzrwzk7pb8d51p8pnbgy2n")))

(define-public crate-csv-0.7.0 (c (n "csv") (v "0.7.0") (h "1zi7wqq9y82fg2apfmvxkn8ymv51f6al8swk8zn5ijq0ccxgwpfs")))

(define-public crate-csv-0.7.1 (c (n "csv") (v "0.7.1") (h "1fajzv9rvdgai484k2w3h8msjjr6fxz170p6a63a40w085v70b9c")))

(define-public crate-csv-0.7.2 (c (n "csv") (v "0.7.2") (h "1gil5ybjf5lkd45m49i6xdgsrw239afij4k6fs8n3v4kiiagcfk7")))

(define-public crate-csv-0.7.3 (c (n "csv") (v "0.7.3") (h "1h6k19aw2lvq8k19iqpv4lcin6pq4270g9x20wgwzgdg5rpcrajk")))

(define-public crate-csv-0.7.4 (c (n "csv") (v "0.7.4") (h "0j292w7ylb6mm62gb4np5k6h7jqfj2ay70a93lidyry9v2w4zbpa")))

(define-public crate-csv-0.7.5 (c (n "csv") (v "0.7.5") (h "1xx9vcmkx0fpqxrp1a3hnrdzi5974z2bnpsiq5jikf33ranv3xly")))

(define-public crate-csv-0.8.0 (c (n "csv") (v "0.8.0") (h "01i0fl5q4cpg3rj807l18ly6zizcm0iqcp55aqzs32hx4m8x0zf2")))

(define-public crate-csv-0.8.1 (c (n "csv") (v "0.8.1") (h "1b8q46sz6q9ldzqwir66iyx63yi47yhjm39bjx8mjpmqpv4f8giz")))

(define-public crate-csv-0.9.0 (c (n "csv") (v "0.9.0") (h "0wd0k771gcdwwjg8wmjy2skxshxlrpbdhd90f3q59j3iw4204y51")))

(define-public crate-csv-0.10.0 (c (n "csv") (v "0.10.0") (h "01k0hw1by9a4alxbwbgmh739xzwss5qhr7wcbqijbd9gsachrjz0")))

(define-public crate-csv-0.10.1 (c (n "csv") (v "0.10.1") (h "1kxzl57wjvwsgnlrv7grfbwc7kvkn1agms92qchmldl5m1zfiwzc")))

(define-public crate-csv-0.10.2 (c (n "csv") (v "0.10.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0iv787j7shpcxxwj4416gqqvwk28nhyhgc4yzpb651n9irlidas4")))

(define-public crate-csv-0.11.0 (c (n "csv") (v "0.11.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1mxkfihf31ydrl11h7lc0kfqbkh8y7n3lcz4y06bqx7vx5n84h5g")))

(define-public crate-csv-0.12.0 (c (n "csv") (v "0.12.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "06sjrnp7pgqzs36spg34bvymhw7cyi4ka86b07hz8vx780y8d3l2")))

(define-public crate-csv-0.12.1 (c (n "csv") (v "0.12.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0plpyi4vim4xpqk162n74kxkagjqms6ghawzl0x62c73ilzib8yz")))

(define-public crate-csv-0.12.2 (c (n "csv") (v "0.12.2") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1iqgdqij4fy6z56b33fk1crsgjj6g1bchi571wi7j5shc1qynhlc")))

(define-public crate-csv-0.12.3 (c (n "csv") (v "0.12.3") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0dbnnjl2b3l0qk6mqpicjxkjl6bf10hxc3kf0pb2zzjjn74m6g2l")))

(define-public crate-csv-0.12.4 (c (n "csv") (v "0.12.4") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1hzv0y4h1k9nblss7a6mssjy02vg0isdc1vnhy81087nx6wxnmlp")))

(define-public crate-csv-0.12.5 (c (n "csv") (v "0.12.5") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "19yy55lzjrv8yxdwf8qwjwwwl8n99mdy4sjn95n60sn5gyjfmhc0")))

(define-public crate-csv-0.12.6 (c (n "csv") (v "0.12.6") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0lj86l8lmh2j3234rbvx3wd1cqk3h94k9ka390nf6bwp1y023w6b")))

(define-public crate-csv-0.12.7 (c (n "csv") (v "0.12.7") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0n62jh1xn90iaa46xp8sjdg3bcp61ab4xb2vph3z2qisskjklqwp")))

(define-public crate-csv-0.12.8 (c (n "csv") (v "0.12.8") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1rypn1yxrlvjrq54r1x4jyaq0svyivapcxz0q4cd7npqh6v7ik3r")))

(define-public crate-csv-0.12.9 (c (n "csv") (v "0.12.9") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1k8xyg18idxk40aj0kk31hz0vlq9zkg35q71a6x22pvr67f79nyk")))

(define-public crate-csv-0.12.10 (c (n "csv") (v "0.12.10") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1v3ijv54js5ifrs7lcwgx92dbkq0ww073j698r98niyib62655p9")))

(define-public crate-csv-0.12.11 (c (n "csv") (v "0.12.11") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "09qz1jh362m6m3wnb2gvv2cr9rxibhbm3lf8mn8z5d1igjy1lxcz")))

(define-public crate-csv-0.12.12 (c (n "csv") (v "0.12.12") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1zcqra8g8g4iajbm3s6alkq9sv7bmxhlxasvpcxik11q5wdrx8mg")))

(define-public crate-csv-0.12.13 (c (n "csv") (v "0.12.13") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0j7inqyf9igld3100vlzzfj1gljm78cr55zsqgn9iphxzf3q9j8f")))

(define-public crate-csv-0.12.14 (c (n "csv") (v "0.12.14") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0w2bbjmsrnwjl7i4jbxnyx8pwyrbzrpcdxyzh606haifdnaq2dff")))

(define-public crate-csv-0.12.15 (c (n "csv") (v "0.12.15") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0dm78xp2gdzghn6c71scapcd3ib9vrc12h328mnc393cmyrsakps")))

(define-public crate-csv-0.12.16 (c (n "csv") (v "0.12.16") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "10p9a5d4mmdv60mz5v503c7nx9x4apc5qx38x05w3j6qspr02166")))

(define-public crate-csv-0.12.17 (c (n "csv") (v "0.12.17") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0x4m0y28xrng7lf5wi1dx7xz26yaaijxfb12r7668xa10jjkqmis")))

(define-public crate-csv-0.12.18 (c (n "csv") (v "0.12.18") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1hb11vmzbrlqlrjncnm28wr0c01agqgx9vkcc8jcrdq2iddzk2h6")))

(define-public crate-csv-0.12.19 (c (n "csv") (v "0.12.19") (d (list (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.2.0") (d #t) (k 0)))) (h "1y47hcwp7nhiklf5aykm1r0p9m8s9acmhdz23jqs47xjk4zgfn6k")))

(define-public crate-csv-0.13.0 (c (n "csv") (v "0.13.0") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "06ysqrxs5850g1jyvcybw289rvk2dgbfmlbss728dw13qqvgdgcw")))

(define-public crate-csv-0.13.1 (c (n "csv") (v "0.13.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "10bs9c6ilzg11yphdgxiax5g4sk4vrkkki2ylxqniv5plpnbaqkg")))

(define-public crate-csv-0.13.2 (c (n "csv") (v "0.13.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "01mm72x8jcvmi1kid27ph20ylyqjpmnxggvfbz8gvz9xn5aqc3sh")))

(define-public crate-csv-0.13.3 (c (n "csv") (v "0.13.3") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0gxhpkqpzj758ahh62yjpfgrz8zdxdsll1j05sp9alfi05rwn1pp")))

(define-public crate-csv-0.13.4 (c (n "csv") (v "0.13.4") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "172l5di05ix50q4x6i72x8zcsq8mpxlxsxminm0yrbi191bda0ga")))

(define-public crate-csv-0.13.5 (c (n "csv") (v "0.13.5") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "08x9rhsv2h7qrc9sgma8hq5ad1vx08m2psrk0j6253r9wqdsmsbp")))

(define-public crate-csv-0.13.6 (c (n "csv") (v "0.13.6") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1rd0mvj6wk3n7bxhbckkzhbp60sz04r1ay21yzg31j1n73yna72a")))

(define-public crate-csv-0.13.7 (c (n "csv") (v "0.13.7") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "05dvjjk1kkvp82bmj7m0pmqka4lygvkh8z2670y3p9hmf68sv7dj")))

(define-public crate-csv-0.13.8 (c (n "csv") (v "0.13.8") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0m08yz1ixi495wgkl10hf56igilzn6wdjgjiiapy6hc3vipi3dim")))

(define-public crate-csv-0.14.1 (c (n "csv") (v "0.14.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1r3livm6cndhwgbix11yl88wpdlw0hmdfqk6acr8a7yahpgmnjhd")))

(define-public crate-csv-0.14.2 (c (n "csv") (v "0.14.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)) (d (n "regex") (r "*") (d #t) (k 2)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0g4b6nmklv0k9s9cagrm3mwyq0adf3p8wdmrv538k4igr59vsvyy")))

(define-public crate-csv-0.14.3 (c (n "csv") (v "0.14.3") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1g5yl4jprg5mw3vs6s1m0w93sf7kj7alkzaygvxdmncdq4l0vjwg")))

(define-public crate-csv-0.14.4 (c (n "csv") (v "0.14.4") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1f73wp8rgyln90sxcifirrb7nvqnki9n51m4a31pv0qdzhhgdzmd")))

(define-public crate-csv-0.14.5 (c (n "csv") (v "0.14.5") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1anaqy1xnqkvgv1md89ys1iznsfdrr77bd4bxmwi808b20zv1d77")))

(define-public crate-csv-0.14.6 (c (n "csv") (v "0.14.6") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1v99y3zdq2b49wkgvjcj1apl86m77819yvc2q0zwcqmxvhpmmfbg")))

(define-public crate-csv-0.14.7 (c (n "csv") (v "0.14.7") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1g0qvr9fj25fq1fy0p758glrb30yz7x46h18hsysaqyaswaihv16")))

(define-public crate-csv-0.15.0 (c (n "csv") (v "0.15.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "memchr") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "173dv65zmmm4kfqnjk66vhgj4w82vh9c14jq6r55c755qwvjpwky")))

(define-public crate-csv-1.0.0-beta.1 (c (n "csv") (v "1.0.0-beta.1") (d (list (d (n "csv-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0m893rlyf212cpwwgbibfw9zrg65qnfj99g6i44smqjijvc5srw1")))

(define-public crate-csv-1.0.0-beta.2 (c (n "csv") (v "1.0.0-beta.2") (d (list (d (n "csv-core") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0dp8d6b7vpxb97i6s7f1k6j6rmd8n2q710p4a221gab39vmv8kn0")))

(define-public crate-csv-1.0.0-beta.3 (c (n "csv") (v "1.0.0-beta.3") (d (list (d (n "csv-core") (r "^0.1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "0crljvl4px0c69aw9ppkvwhlmygpax0vi6k8w8maz16sycaxr3hm")))

(define-public crate-csv-1.0.0-beta.4 (c (n "csv") (v "1.0.0-beta.4") (d (list (d (n "csv-core") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1r9icn8fy1ggz0xjavjhpjvdmsad0h24971ci4z7lfv15jyg6hd8")))

(define-public crate-csv-1.0.0-beta.5 (c (n "csv") (v "1.0.0-beta.5") (d (list (d (n "csv-core") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1w3jjlm9rhpj6mmnjvazxjl3vprizlmnfgpj0v1mdnzbviiy1ag7")))

(define-public crate-csv-1.0.0 (c (n "csv") (v "1.0.0") (d (list (d (n "csv-core") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)))) (h "1qs9ibgz54q99a6g3m0pmz50xllhfgqjzczkwxawaq4rmy23343i")))

(define-public crate-csv-1.0.1 (c (n "csv") (v "1.0.1") (d (list (d (n "csv-core") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.4") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 2)))) (h "1ykpbpk9aypy93vvnfkxm7jrb5d6amw4658p72728wp0m9snvfqb")))

(define-public crate-csv-1.0.2 (c (n "csv") (v "1.0.2") (d (list (d (n "csv-core") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.4") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 2)))) (h "0lk45506x28bsdjv2xycq5ihk6l4ymxfb8xijhl8l4k9znqgcm3d")))

(define-public crate-csv-1.0.3 (c (n "csv") (v "1.0.3") (d (list (d (n "csv-core") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.4") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 2)))) (h "0n3c7inizrq6wbsn4wrqkxyv71f9hyvm8dr0rxq75aw7f4zi4isg")))

(define-public crate-csv-1.0.4 (c (n "csv") (v "1.0.4") (d (list (d (n "csv-core") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.4") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 2)))) (h "07d3cwmzwg1jlims4380p7vz0yil676py7aafh7857ya8gl2ngbz")))

(define-public crate-csv-1.0.5 (c (n "csv") (v "1.0.5") (d (list (d (n "csv-core") (r "^0.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.4") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 2)))) (h "012gmrcfcj7v9g671msf00ilqlz5kan3xfqzy7mgp307b16c9lcz")))

(define-public crate-csv-1.0.6 (c (n "csv") (v "1.0.6") (d (list (d (n "csv-core") (r "^0.1.4") (d #t) (k 0)) (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "ryu") (r "^0.2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.4") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 2)))) (h "0wzfm81nyr5v226sczazj60h5hbqn4ma4npbmps0ipfqaiqjqy7h")))

(define-public crate-csv-1.0.7 (c (n "csv") (v "1.0.7") (d (list (d (n "csv-core") (r "^0.1.4") (d #t) (k 0)) (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "ryu") (r "^0.2.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.10.4") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0.55") (d #t) (k 2)))) (h "0fqvxa2ixgvzx3jr39cfss2rwqhq16dnh4amzjjva909zddf4i4h")))

(define-public crate-csv-1.1.0 (c (n "csv") (v "1.1.0") (d (list (d (n "bstr") (r "^0.2.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "csv-core") (r "^0.1.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 2)))) (h "0qxvzq030hi915dszazv6a7f0apzzi7gn193ni0g2lzkawjxck55")))

(define-public crate-csv-1.1.1 (c (n "csv") (v "1.1.1") (d (list (d (n "bstr") (r "^0.2.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "csv-core") (r "^0.1.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 2)))) (h "0zgq18xam24rbggm3ybmrygipa0mrr7rscf9r8hmi9vkzp6rql9p")))

(define-public crate-csv-1.1.2 (c (n "csv") (v "1.1.2") (d (list (d (n "bstr") (r "^0.2.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "csv-core") (r "^0.1.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 2)))) (h "0rjjyp6vjmma33m7p2hkc488zdi6i91bih7s5pak395rhk8cpy0i")))

(define-public crate-csv-1.1.3 (c (n "csv") (v "1.1.3") (d (list (d (n "bstr") (r "^0.2.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "csv-core") (r "^0.1.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 2)))) (h "0yd2z55m2pg4al4yng4nl2y7c9dw2v7yhg5ynihxyrmmd9zzxbq0")))

(define-public crate-csv-1.1.4 (c (n "csv") (v "1.1.4") (d (list (d (n "bstr") (r "^0.2.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "csv-core") (r "^0.1.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 2)))) (h "0m4b2dpamqkrripdp3gy4wcsimbgm60jxnpisqzsy16h9wancipw")))

(define-public crate-csv-1.1.5 (c (n "csv") (v "1.1.5") (d (list (d (n "bstr") (r ">=0.2.1, <0.3.0") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "csv-core") (r ">=0.1.6, <0.2.0") (d #t) (k 0)) (d (n "itoa") (r ">=0.4.3, <0.5.0") (d #t) (k 0)) (d (n "ryu") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.55, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.55, <2.0.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "15wydz6klf1shh1ac5n6rsihc4xrz1lzi8vjmhava94v54rqdmgr")))

(define-public crate-csv-1.1.6 (c (n "csv") (v "1.1.6") (d (list (d (n "bstr") (r "^0.2.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "csv-core") (r "^0.1.6") (d #t) (k 0)) (d (n "itoa") (r "^0.4.3") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 2)))) (h "1q9nqn0qlamwl18v57p82c8yhxy43lkzf2z1mndmycsvqinkm092")))

(define-public crate-csv-1.2.0 (c (n "csv") (v "1.2.0") (d (list (d (n "bstr") (r "^1.2.0") (f (quote ("alloc" "serde"))) (k 2)) (d (n "csv-core") (r "^0.1.10") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 2)))) (h "0ng3sg4n21z61iqg390d1fxlana7k41yfl0zi452py2mfc5z94dg") (r "1.60")))

(define-public crate-csv-1.2.1 (c (n "csv") (v "1.2.1") (d (list (d (n "bstr") (r "^1.2.0") (f (quote ("alloc" "serde"))) (k 2)) (d (n "csv-core") (r "^0.1.10") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 2)))) (h "1bb4xw15il0bglr0ldm63q2yzvd6q3k5vliaq1lrv6lv0ybm808b") (r "1.60")))

(define-public crate-csv-1.2.2 (c (n "csv") (v "1.2.2") (d (list (d (n "bstr") (r "^1.2.0") (f (quote ("alloc" "serde"))) (k 2)) (d (n "csv-core") (r "^0.1.10") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 2)))) (h "11l0iqh54jmpk31i92sk5pj4mnrrh8j25696yilddn6kji4y6sk2") (r "1.60")))

(define-public crate-csv-1.3.0 (c (n "csv") (v "1.3.0") (d (list (d (n "bstr") (r "^1.2.0") (f (quote ("alloc" "serde"))) (k 2)) (d (n "csv-core") (r "^0.1.11") (d #t) (k 0)) (d (n "itoa") (r "^1") (d #t) (k 0)) (d (n "ryu") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (d #t) (k 0)) (d (n "serde") (r "^1.0.55") (f (quote ("derive"))) (d #t) (k 2)))) (h "1zjrlycvn44fxd9m8nwy8x33r9ncgk0k3wvy4fnvb9rpsks4ymxc") (r "1.61")))

