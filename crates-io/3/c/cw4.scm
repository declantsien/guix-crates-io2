(define-module (crates-io #{3}# c cw4) #:use-module (crates-io))

(define-public crate-cw4-0.3.2 (c (n "cw4") (v "0.3.2") (d (list (d (n "cosmwasm-schema") (r "^0.11.1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1mik8b5y4gvxcd5rh835wr31ya724bypkdxr3r08g68gbbghirqp")))

(define-public crate-cw4-0.4.0 (c (n "cw4") (v "0.4.0") (d (list (d (n "cosmwasm-schema") (r "^0.12.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.12.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0r065a2hqw8knzrg805i2n0n16rrc1ff7njniifp72f9n54zz2ih")))

(define-public crate-cw4-0.5.0 (c (n "cw4") (v "0.5.0") (d (list (d (n "cosmwasm-schema") (r "^0.13.2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.13.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1ff45g8qwx2br5md1408cjdbn68nkcyybjxlyh1f64dlsycg09ld")))

(define-public crate-cw4-0.6.0-alpha1 (c (n "cw4") (v "0.6.0-alpha1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "123h7jacfnrs6yh13zd2v8bd5c3b0n70pvimxxrl303vspvq5b9n")))

(define-public crate-cw4-0.6.0-alpha2 (c (n "cw4") (v "0.6.0-alpha2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0a44l0ghdghi1933c47czk733f2aml7hp78sc3mzklf8zh2vbrxf")))

(define-public crate-cw4-0.6.0-alpha3 (c (n "cw4") (v "0.6.0-alpha3") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta1") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "101ikk9qqw34l2qsw6lc6x5hrqkrbl3r8bqyycz89pwgaf5sl7qp")))

(define-public crate-cw4-0.6.0-beta1 (c (n "cw4") (v "0.6.0-beta1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "161h9nhs3ipnc90ia13iyfwalk00cyhcwq286s4cgqn6hh74pr3v")))

(define-public crate-cw4-0.6.0-beta2 (c (n "cw4") (v "0.6.0-beta2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1778qhqzw7sxj3zznphn32hlk4fkd7kk8cq51wygxkxl0ldv41jl")))

(define-public crate-cw4-0.6.0-beta3 (c (n "cw4") (v "0.6.0-beta3") (d (list (d (n "cosmwasm-schema") (r "^0.14.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0-beta5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1vnnx0w4dgmymnrbrngyqzy3s06v7cmw02plq4ciwylqbkbf7gnp")))

(define-public crate-cw4-0.6.0 (c (n "cw4") (v "0.6.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0739wa27rrcxjyckjbvgxrks9sc95wfd3sw496kfbfg3200j95gq")))

(define-public crate-cw4-0.6.1 (c (n "cw4") (v "0.6.1") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1v1i9bdlqm127phgzbhb812n8jix0sz0jmj3zba8fb0janagw7x9")))

(define-public crate-cw4-0.6.2 (c (n "cw4") (v "0.6.2") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "01i9jbwxlkwgv8jzcwmhnyi8xznzjwdzgvbx2nkr0m3yzbir1346")))

(define-public crate-cw4-0.7.0 (c (n "cw4") (v "0.7.0") (d (list (d (n "cosmwasm-schema") (r "^0.14.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "02w9klrslyl44vk564hic02qviw8lqzj1cx3yw363jgm1bz0lxs2")))

(define-public crate-cw4-0.8.0-rc1 (c (n "cw4") (v "0.8.0-rc1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0-rc5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0-rc5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0fy5g162hk2vw9960r74l69bn5yqy5z4akw40b6jmmsc5mf1vqi2")))

(define-public crate-cw4-0.8.0-rc2 (c (n "cw4") (v "0.8.0-rc2") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0srsqqr8aqknpjaadzrk6yknzlfiyik5z3mvd3xaqck02s51m42z")))

(define-public crate-cw4-0.8.0-rc3 (c (n "cw4") (v "0.8.0-rc3") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "17r1jllsx54y5sjnjqiid5xcvhndb8gq0j4rb0gdz7m98h0zcx4d")))

(define-public crate-cw4-0.8.0 (c (n "cw4") (v "0.8.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0xg4nrnmy6dvb872zzd05n0i6kh5vnk6vyxf6jz9g6idy5bz2x8r")))

(define-public crate-cw4-0.8.1 (c (n "cw4") (v "0.8.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0n326dsg7bj49vn5b1mnbp7r31bwb129pckgi4pnmqb8wjbkwqfl")))

(define-public crate-cw4-0.9.0 (c (n "cw4") (v "0.9.0") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1mvsr3wq9x6rlqcbb0smig39iarjr8i5yni9s24yb0m9dbzmwdbk")))

(define-public crate-cw4-0.10.0-soon (c (n "cw4") (v "0.10.0-soon") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1xkm5ds4wi1rnzkcd28s075bnryfqbljsy9q5zpld1s7k15kjwc9")))

(define-public crate-cw4-0.9.1 (c (n "cw4") (v "0.9.1") (d (list (d (n "cosmwasm-schema") (r "^0.16.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "13anrysfawmnms10qgqnl5q568k2vwv0zrxjnszn5p06aia0vwiq")))

(define-public crate-cw4-0.10.0-soon2 (c (n "cw4") (v "0.10.0-soon2") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0jrllqgrrc5sziw164hfn752ssgd916lh7fsfawn79bahmwnqhsw")))

(define-public crate-cw4-0.10.0-soon3 (c (n "cw4") (v "0.10.0-soon3") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "00nycx9kwc3vgly64yz0gz1gff1snhzh9dszmf5br95dgaf5833x")))

(define-public crate-cw4-0.10.0-soon4 (c (n "cw4") (v "0.10.0-soon4") (d (list (d (n "cosmwasm-schema") (r "=1.0.0-soon2") (d #t) (k 2)) (d (n "cosmwasm-std") (r "=1.0.0-soon2") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "14q0rnwkaf1xwl49ax4bv9pnl1xbgbx0ahxpwsghn64hmf12267d")))

(define-public crate-cw4-0.10.0 (c (n "cw4") (v "0.10.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0dppxgapa0ykawhiqv13v5lj2s71p61x4n02d7qizcv84gl22mwx")))

(define-public crate-cw4-0.10.1 (c (n "cw4") (v "0.10.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0znwqzj9ag33hxsvwdspyi89qbckm2gmgnd11mvgf49ch08i7hd4")))

(define-public crate-cw4-0.10.2 (c (n "cw4") (v "0.10.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "12pga3fy7g4a2wmjpj6wc51jpwzap9grw4gmbckj3xl9mnfn358b")))

(define-public crate-cw4-0.10.3 (c (n "cw4") (v "0.10.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0c255jjlf7h9vww1z7v8sx51xyrkr570rxviw136v6ppq23cki0d")))

(define-public crate-cw4-0.11.0 (c (n "cw4") (v "0.11.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.11.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0azz5yrgvp7qivi6y97d7pamjbnb8rxxi4zibvavj1p3kzshxf9j")))

(define-public crate-cw4-0.11.1 (c (n "cw4") (v "0.11.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.11.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1szs1h0gwa5f62fid3j4pfads2idnl5j9ck9ymr4qa7nwdk5ci21")))

(define-public crate-cw4-0.12.0 (c (n "cw4") (v "0.12.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta3") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta3") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.12.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1ppmfj0mrcwfw8j2jghqx98w92bdch9jkyia28449rlarbjcp9y8")))

(define-public crate-cw4-0.12.1 (c (n "cw4") (v "0.12.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta5") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.12.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "117hrp82ysxcxfk9qjq8s4yyqw9ivadw2q9xa9vs5rcrbs2afbgj")))

(define-public crate-cw4-0.13.0 (c (n "cw4") (v "0.13.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "10lma4vx7qirk9swkn2p3bzkdspsw4r3g09cac21xjn13fqr9hqy")))

(define-public crate-cw4-0.13.1 (c (n "cw4") (v "0.13.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta6") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta6") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1ydppxfjlqznwvq8n7yndzqi1vir3xfsynwz0a8znjq90mp1pmai")))

(define-public crate-cw4-0.13.2 (c (n "cw4") (v "0.13.2") (d (list (d (n "cosmwasm-schema") (r "^1.0.0-beta8") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0-beta8") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0lpfyvq3397j3wp850zl2dxw73b4rzhx02r6zzcnxi0kgim6siy4")))

(define-public crate-cw4-0.13.3 (c (n "cw4") (v "0.13.3") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1nl8dlz5cka0q9fc85s8k5amlmxgxkz7k0s85n9iy9jw9g73yh7x")))

(define-public crate-cw4-0.13.4 (c (n "cw4") (v "0.13.4") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1gawxmsa27hhs1ydjm3h34i4fs184qp6yrrs3f8cc4ffsm4kbk0a")))

(define-public crate-cw4-0.14.0 (c (n "cw4") (v "0.14.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 2)) (d (n "cosmwasm-std") (r "^1.0.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0af36g0x1b6k49y4pd4ipx081ng51gkvqzpymmd4grfy1crcbp4m")))

(define-public crate-cw4-0.15.0 (c (n "cw4") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0ldp69mk8mym2g7n1v07l9qb4cpbfx6qdavnjqgys6p4p5qjqqyf")))

(define-public crate-cw4-0.15.1 (c (n "cw4") (v "0.15.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.15.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1shky7ar7h0yfainvxyl6flibc0dsbkg766w9nk3ygi267jckysx")))

(define-public crate-cw4-0.16.0 (c (n "cw4") (v "0.16.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "04ilwj8n1lgsyad28q9yxzsr57ywmalakna8p4w41d6xchkkrac6")))

(define-public crate-cw4-1.0.0 (c (n "cw4") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "11spq1kg29fdbfpb4zaq79xx2475695a3p1r0x1m0cpxqs6q97z4")))

(define-public crate-cw4-1.0.1 (c (n "cw4") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "16q4dczdn16p13wix7rn9d99kl071vfq39imjbl9gkh2mq5nw8rc")))

(define-public crate-cw4-1.1.0 (c (n "cw4") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0r3snzk2pxql6chb8g29knanqw7scpw7c045s2iambgg0xink653")))

(define-public crate-cw4-1.1.1 (c (n "cw4") (v "1.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "19nq4fs5959mhlv4awavf2cdqlshrl0qlrcclxi3fqn045fcx7sd")))

(define-public crate-cw4-1.1.2 (c (n "cw4") (v "1.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "1lnciwwb7apz868k48a0qh960rl7xfrdj2f4mmh1qajzwkv4yx94")))

(define-public crate-cw4-2.0.0-rc.0 (c (n "cw4") (v "2.0.0-rc.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^2.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "0797mys23dhv8gwyq3b0rj8bz7dl49pcqngk4qalq0x5m1mfkdgd")))

(define-public crate-cw4-2.0.0 (c (n "cw4") (v "2.0.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^2.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)))) (h "0b83vgjmz3y5w6lgkhww9wyglyv9cx4z9mqjw8jbvlbcdf55qgyk")))

