(define-module (crates-io #{3}# c cic) #:use-module (crates-io))

(define-public crate-cic-0.1.0 (c (n "cic") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "csv") (r "^1.1.6") (d #t) (k 0)))) (h "1q4xb0k13w0gpj5wy58s87qsgnbrc5kip4g2jl0x9m91ic8g8bxn")))

