(define-module (crates-io #{3}# c cft) #:use-module (crates-io))

(define-public crate-cft-0.1.0 (c (n "cft") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colored_json") (r "^2") (d #t) (k 0)) (d (n "cw_parser") (r "^0.1.3") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 0)) (d (n "minus") (r "^4.0.2") (f (quote ("static_output"))) (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "rusoto_cloudformation") (r "^0.47.0") (d #t) (k 0)) (d (n "rusoto_core") (r "^0.47.0") (d #t) (k 0)) (d (n "rusoto_logs") (r "^0.47.0") (d #t) (k 0)) (d (n "tokio") (r "^1.12.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jj0ci2z96y078amvzjsgznag5gfjwkrb59vyi85p614ig0c24ml")))

