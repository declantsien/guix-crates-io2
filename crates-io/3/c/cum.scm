(define-module (crates-io #{3}# c cum) #:use-module (crates-io))

(define-public crate-cum-0.1.0 (c (n "cum") (v "0.1.0") (h "14xh0i9zjbrdi58kl5b5c3q3jyzhb86277mz1grwsxfw8jzpw39l") (y #t)))

(define-public crate-cum-0.2.0 (c (n "cum") (v "0.2.0") (h "04qlvb03p3bl8jfbdr475zfb7k58s92cn6znayzaldl1hy1xcjcb")))

