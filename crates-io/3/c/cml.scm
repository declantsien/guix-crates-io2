(define-module (crates-io #{3}# c cml) #:use-module (crates-io))

(define-public crate-cml-0.1.6 (c (n "cml") (v "0.1.6") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.12") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("json"))) (d #t) (k 0)) (d (n "schemars") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.120") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "serde_with") (r "^1.6.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0w424ycpzixvl9k5rnil6l4q8glci02d9hvr91272a49wj74hzh1") (f (quote (("untyped_requests") ("default"))))))

