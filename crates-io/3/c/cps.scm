(define-module (crates-io #{3}# c cps) #:use-module (crates-io))

(define-public crate-cps-0.1.0 (c (n "cps") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "04202awcpidl11sb481qw95pkkimvgy0mjpd1md05pd7yyxm2yhq")))

(define-public crate-cps-0.2.0 (c (n "cps") (v "0.2.0") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03nccnw4jfi2x1kxs6c6jafah5nn9j2j8dkfxra871gzg4qpn73j")))

(define-public crate-cps-0.2.1 (c (n "cps") (v "0.2.1") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19sc3jf5w16x25v445w0qynx3b5symp5x24vd5r2bw3pvp771d5b")))

(define-public crate-cps-0.2.2 (c (n "cps") (v "0.2.2") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gfqr6azrbwd5ll1y6a5alzmn56rpn5i0ykk0b66mah0hfplhwnc")))

(define-public crate-cps-0.2.3 (c (n "cps") (v "0.2.3") (d (list (d (n "litrs") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0584n5jgmjad4f5ygd4yfsnkl3q0mph7p7413n9jm644bin763jp")))

