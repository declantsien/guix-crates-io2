(define-module (crates-io #{3}# c ctb) #:use-module (crates-io))

(define-public crate-ctb-1.0.0 (c (n "ctb") (v "1.0.0") (h "0df4glxps7pm3pdwvx9yqg6ky66gvk9sw6xnyv7b25v2l8siqr73")))

(define-public crate-ctb-1.1.0 (c (n "ctb") (v "1.1.0") (h "1275vn9v5j1c4ri9xmym199gbxnczg2xmjpmncjj7fwlv4gklmql")))

