(define-module (crates-io #{3}# c cnu) #:use-module (crates-io))

(define-public crate-cnu-0.1.1 (c (n "cnu") (v "0.1.1") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1hxbhkk3bsc234bzgcq98mpbzkpzvywcvf31j31flvah5yvz2lzh")))

(define-public crate-cnu-0.1.2 (c (n "cnu") (v "0.1.2") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "11bp236h1p1j1qrkk1h1xgc17pd974jkhwqzrihqmhkjkv01wcxm")))

(define-public crate-cnu-0.1.3 (c (n "cnu") (v "0.1.3") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "07sm2x62in1v8v97dl5f5f81qnh444gfdac26dbfcib6p3zkch2b")))

(define-public crate-cnu-0.1.4 (c (n "cnu") (v "0.1.4") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "19sqb9wzsxsbzgcqh2jzw2p4qj3f6zl04gsf2h6p673ahqhmsgs4")))

(define-public crate-cnu-0.1.5 (c (n "cnu") (v "0.1.5") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1sgp4fsgqyaxw0zsljw4f0gf22yx6ysf7ml8b5fki83pypdb41wz")))

(define-public crate-cnu-0.1.6 (c (n "cnu") (v "0.1.6") (d (list (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "17j77ywgl898r869qq77xamw9ln5l2pbg0s5db8b5sb5jw65jbpk")))

(define-public crate-cnu-0.1.7 (c (n "cnu") (v "0.1.7") (d (list (d (n "lang") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "13avd5nnizgv0a1mpsdv62pdicy7rr03188k60rmasd05rb5fzzi") (f (quote (("default")))) (s 2) (e (quote (("lang" "dep:lang"))))))

(define-public crate-cnu-0.1.8 (c (n "cnu") (v "0.1.8") (d (list (d (n "lang") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1zazflmk4rz3wr1k7p6qwkla6lb2p1ld6vnqvqaspbxh2sy1wp7s") (f (quote (("default")))) (s 2) (e (quote (("lang" "dep:lang"))))))

(define-public crate-cnu-0.1.9 (c (n "cnu") (v "0.1.9") (d (list (d (n "enum_dispatch") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "lang") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1s57l2il4ffpj78qw6phzi1h48hbzh5llsni037srmr61800g0f7") (f (quote (("default")))) (s 2) (e (quote (("lang" "dep:lang" "dep:enum_dispatch"))))))

(define-public crate-cnu-0.1.10 (c (n "cnu") (v "0.1.10") (d (list (d (n "enum_dispatch") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "lang") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "00111b63n155mc5l7bnimvipnxzdlf1fvcsyzsvigw9haxir1q4j") (f (quote (("default")))) (s 2) (e (quote (("lang" "dep:lang" "dep:enum_dispatch"))))))

(define-public crate-cnu-0.1.11 (c (n "cnu") (v "0.1.11") (d (list (d (n "enum_dispatch") (r "^0.3.12") (o #t) (d #t) (k 0)) (d (n "lang") (r "^0.1.9") (o #t) (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0mbdgmh88v5lw154vv4r70frhk9a9b7dlgpc0m2bzina3lrca17f") (f (quote (("default")))) (s 2) (e (quote (("lang" "dep:lang" "dep:enum_dispatch"))))))

