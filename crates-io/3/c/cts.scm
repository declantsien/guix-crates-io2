(define-module (crates-io #{3}# c cts) #:use-module (crates-io))

(define-public crate-cts-0.1.0 (c (n "cts") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "1k4w9b79n08s357xjw0f78s2ld0lyrrca3k12bx04h0ia6nh6pv0")))

(define-public crate-cts-0.1.1 (c (n "cts") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "0qmg6h5l5grrs8zl21qmq8c31vm1w9l7kmvwvsvi7631yappvgn2")))

(define-public crate-cts-0.2.0 (c (n "cts") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "08q9m1f9wf090i5j4dpsghb76rqrid8h9w23f4rl87550wmapsrb")))

(define-public crate-cts-0.2.1 (c (n "cts") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "04946h1ijc2zvwi6kb2wlg1wmhxnhwxx5pa0p54bc6lrbbkwaida")))

(define-public crate-cts-0.3.0 (c (n "cts") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "1mn6i82702v4sx4sc4acfjxl1yv98mki85nn8gfi5isk0k7cxjks")))

(define-public crate-cts-0.4.0 (c (n "cts") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "14gfvrqhw23jrz5809lrykjh0i4g172ylms2ri9p1j3idd4rli78")))

(define-public crate-cts-0.4.1 (c (n "cts") (v "0.4.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "1x8090rhraxgb06qdkdml6x13bf04mm9m9pcdfv3md7ym63zyaxa")))

(define-public crate-cts-0.5.0 (c (n "cts") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0-beta.5") (d #t) (k 0)) (d (n "tempfile") (r "^2.2") (d #t) (k 2)))) (h "1g8kcbrz3n7c2c5cgi73xikq8f2y1jmcfxlcx58jfjaff2j81hkd")))

