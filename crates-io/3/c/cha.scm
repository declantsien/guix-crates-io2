(define-module (crates-io #{3}# c cha) #:use-module (crates-io))

(define-public crate-cha-0.1.0 (c (n "cha") (v "0.1.0") (h "18ack02sqa8lk956qxi8fbxw49jgkxi32y4ifqga0fm0dl3wp3f4")))

(define-public crate-cha-0.1.1 (c (n "cha") (v "0.1.1") (d (list (d (n "ring") (r "^0.12.1") (d #t) (k 0)))) (h "0fw6y4b4b0dbnmrhpz7bj0k1xwyz9k2r4a9dyrrwxwx4jf9bjgaw")))

