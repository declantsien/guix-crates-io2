(define-module (crates-io #{3}# c cap) #:use-module (crates-io))

(define-public crate-cap-0.1.0 (c (n "cap") (v "0.1.0") (h "18cvy3c1rz760cq5digqdmhg303mcl0kfnc0xfrvkgwm3sy45cyz") (f (quote (("nightly"))))))

(define-public crate-cap-0.1.1 (c (n "cap") (v "0.1.1") (h "009zdnc7xyfcyyrv5dkfg8k8pikx2jhclxlhxjkbq761kc1nkks2") (f (quote (("nightly"))))))

(define-public crate-cap-0.1.2 (c (n "cap") (v "0.1.2") (h "00q00chl1y8j6p9f9rndnd9jyqyqklnd5l9fn0v4r8l4bfw5w4kg") (f (quote (("stats") ("nightly"))))))

