(define-module (crates-io #{3}# c csq) #:use-module (crates-io))

(define-public crate-csq-0.2.0 (c (n "csq") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0606n94fj8swahn9gj51sjxmjbv3hi5mj3z64cjvndawxrmjz794")))

(define-public crate-csq-0.2.1 (c (n "csq") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "0m95142vr058lw618czkmivfl9w2cmh7yhl02cpqzvppg9dqixyc")))

