(define-module (crates-io #{3}# c csa) #:use-module (crates-io))

(define-public crate-csa-0.1.0 (c (n "csa") (v "0.1.0") (d (list (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "1k73z45jnpl1cw3d02v1iwpk0wc2w755jnsd3ps1db1xdayvk9y5")))

(define-public crate-csa-0.2.0 (c (n "csa") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "14ji94naagplk1bikjdg6zf9v5pk577nvhcczmpfdpcx3i2vgpgl")))

(define-public crate-csa-0.3.0 (c (n "csa") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "0aapc5kcmjlrblz875c10a6qk19fnrik8rjjpmbpazmpiadamcr1")))

(define-public crate-csa-0.4.0 (c (n "csa") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^3.0") (d #t) (k 0)))) (h "0rm6nf6v2ra7pzsqd7vigav068s9jldzh1b6abvn6rkdj5hzj6yv")))

(define-public crate-csa-0.5.0 (c (n "csa") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "034jqwpl690q5v2qdls8sndimgz2lprz7hj3nm8y4sgv4dvk7zbp")))

(define-public crate-csa-0.6.0 (c (n "csa") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0prywjnllsmqxy92zrf2a1q6p3kw3kh5my4dvrga1v78fyy7q6z5")))

(define-public crate-csa-0.6.1 (c (n "csa") (v "0.6.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0ckllav7dqs7qqx21smjmla4212m7kfvyp93x71lj02jmpjjpp0x")))

(define-public crate-csa-1.0.0 (c (n "csa") (v "1.0.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("formatting" "std"))) (d #t) (k 0)))) (h "0wsw33k84vck5rchasjq4p8xs32v0qdv6gadzqdbvlvbcwjidbxk")))

(define-public crate-csa-1.0.1 (c (n "csa") (v "1.0.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "time") (r "^0.3.5") (f (quote ("formatting" "std"))) (d #t) (k 0)))) (h "09zxjqvyhypfmf8ld6cb69glj3aih36iwlndpawn54axkksq7qka")))

(define-public crate-csa-1.0.2 (c (n "csa") (v "1.0.2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting" "std"))) (d #t) (k 0)))) (h "0wv952bqh6hvnfc2ryw3nbfjqvd1jg9qqmd64zqin5qrrnys5q3l")))

