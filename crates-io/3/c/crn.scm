(define-module (crates-io #{3}# c crn) #:use-module (crates-io))

(define-public crate-crn-0.1.0 (c (n "crn") (v "0.1.0") (d (list (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1s95i7jh88l1zxbw8cym70nkwhj97mhb7j5m55yyi7753l722qy4")))

(define-public crate-crn-0.1.1 (c (n "crn") (v "0.1.1") (d (list (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "egui") (r "^0.22.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "07r5rnqdihllnglkv9i7l47s37a0csnk88h2d7qlgc13aihq0nnb")))

(define-public crate-crn-0.1.2 (c (n "crn") (v "0.1.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "bimap") (r "^0.6.3") (d #t) (k 0)) (d (n "eframe") (r "^0.22.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "more-asserts") (r "^0.3.1") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1n013304dzmhcj70gmy6rijqfsdczqn367dgfry8p5qn1xpr12pb")))

