(define-module (crates-io #{3}# c cgi) #:use-module (crates-io))

(define-public crate-cgi-0.1.0 (c (n "cgi") (v "0.1.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)))) (h "0jzh1m2mj9r36khb99j2r6wi8r3cbji2c93najbpxpi7712k3vsy")))

(define-public crate-cgi-0.2.0 (c (n "cgi") (v "0.2.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)))) (h "1d6slr9w36313p8jymldi4c8lbw6k18yqva6lqlc618a0an6ks01")))

(define-public crate-cgi-0.3.0 (c (n "cgi") (v "0.3.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)))) (h "18z6660wl4l6npm2mzay6i4qppplpcffczay86609spipyhvifq0") (y #t)))

(define-public crate-cgi-0.3.1 (c (n "cgi") (v "0.3.1") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)))) (h "1519bv4wsflh0626p4zfz0wq212c2x10b41cllbw6kb056ryzgzf")))

(define-public crate-cgi-0.4.0 (c (n "cgi") (v "0.4.0") (d (list (d (n "http") (r "^0.1") (d #t) (k 0)))) (h "15qcmf3viqz4smd76cxsra593516kjzggnykmsqb98iiz8nikwfi")))

(define-public crate-cgi-0.5.0 (c (n "cgi") (v "0.5.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "03mlpjradszz5202layrhyzvf332bdza1s9gx27z13xvy3v7ay23")))

(define-public crate-cgi-0.6.0 (c (n "cgi") (v "0.6.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "000adsyacsvlj8p7lhvrq5zh642j45qn44n00ycj5nd4dav487d7")))

(define-public crate-cgi-0.6.1-rc1 (c (n "cgi") (v "0.6.1-rc1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)))) (h "0hnr43pa6b92asazwqdyw1f02yfvf4xhb6wi36hz1hdi9b07mp20")))

