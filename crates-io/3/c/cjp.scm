(define-module (crates-io #{3}# c cjp) #:use-module (crates-io))

(define-public crate-cjp-0.1.0 (c (n "cjp") (v "0.1.0") (h "0jrcwvhdg8rnis60k3jnm3lv13rsnj35316vym38ad9hlzywpnap")))

(define-public crate-cjp-1.0.0 (c (n "cjp") (v "1.0.0") (h "0yim5592r7an8b46i4h8s96ja7599l80q0r2srwbvbjk6h924xbx")))

