(define-module (crates-io #{3}# c cgl) #:use-module (crates-io))

(define-public crate-cgl-0.1.0 (c (n "cgl") (v "0.1.0") (d (list (d (n "gleam") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ckpvj22fz0bm9ry65i8m3c89rhv62m6mhglg586444snlxkr3qm")))

(define-public crate-cgl-0.1.1 (c (n "cgl") (v "0.1.1") (d (list (d (n "gleam") (r "*") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0yjadgnvam2s9fkam08jh0ix9yk5aasfs0c4lbg6q0zjph6yqxmi")))

(define-public crate-cgl-0.1.2 (c (n "cgl") (v "0.1.2") (d (list (d (n "gleam") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15bs0861rc7hw29w05mhqxmsvd9j11pbbvhsh3fhfxc491nwq72l")))

(define-public crate-cgl-0.1.3 (c (n "cgl") (v "0.1.3") (d (list (d (n "gleam") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "07cdjychws6svcbi09mrnjsil8fp3r8c55vfll5wylmf8gjl9mcs")))

(define-public crate-cgl-0.1.4 (c (n "cgl") (v "0.1.4") (d (list (d (n "gleam") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "038nsm8dvf1iwkx1bqfg41c1yj6s3dabyl995bbr21690zl6v3x8")))

(define-public crate-cgl-0.1.5 (c (n "cgl") (v "0.1.5") (d (list (d (n "gleam") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "044cycnn760gki4jnvsagwr3wds9pdmnpgsx8ysrqwsslv67ipcb")))

(define-public crate-cgl-0.2.0 (c (n "cgl") (v "0.2.0") (d (list (d (n "gleam") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09l0s7cg7z6mympq92hwff711j5zbrrz4aigslrn5ld7ray46zdk")))

(define-public crate-cgl-0.2.1 (c (n "cgl") (v "0.2.1") (d (list (d (n "gleam") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "129d6kr455n6lmvzlpgjn9dsxmxlq4bjbxra2iz4jb1a5js5qxl6")))

(define-public crate-cgl-0.2.2 (c (n "cgl") (v "0.2.2") (d (list (d (n "gleam") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ajhq77hcfddl1bqhxhyr8497944lbnhy4ckcvjxw7v3z4jmxw40")))

(define-public crate-cgl-0.2.3 (c (n "cgl") (v "0.2.3") (d (list (d (n "gleam") (r "^0.6") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j8ayr8pbwvyv6l8r7m5z197rs3pqn97085w9j4rfn7yfh5yrrsm")))

(define-public crate-cgl-0.3.0 (c (n "cgl") (v "0.3.0") (d (list (d (n "gleam") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x8jp663viih8mj587qyyi5wy7zd7v9kg6w7dr6iji8m0lad0k72")))

(define-public crate-cgl-0.3.2 (c (n "cgl") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zs7skrsyrsm759vfy2cygkx52fx91b567a12bpaz1sf4d8hbv8c")))

