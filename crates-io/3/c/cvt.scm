(define-module (crates-io #{3}# c cvt) #:use-module (crates-io))

(define-public crate-cvt-0.1.0 (c (n "cvt") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "0i87lcki193ra5mlnbnzn4dj80wn3bp6rfxszqqq189cl6a4dr9p")))

(define-public crate-cvt-0.1.1 (c (n "cvt") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1.10") (d #t) (k 0)))) (h "0wccnhyj989rlmjyfh7xjgvg49pc18bv4qdw4p6q1jzwgr639b1l")))

(define-public crate-cvt-0.1.2 (c (n "cvt") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1wdwg2rbjl86bcrymscl34pw31kyv1ada19jazpkjbdzgzvrpbnj")))

