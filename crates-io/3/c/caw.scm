(define-module (crates-io #{3}# c caw) #:use-module (crates-io))

(define-public crate-caw-0.1.0 (c (n "caw") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "caw_core") (r "^0.1") (k 0)) (d (n "cpal") (r "^0.15") (d #t) (k 0)) (d (n "hound") (r "^3.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "midir") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "midly") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "nix") (r "^0.28") (f (quote ("fs" "term"))) (o #t) (d #t) (k 0)))) (h "033fsr6y5yqmjp8m0hdcr0nhigiynwkqnfia5l9p7mv0fa1595y1") (f (quote (("web" "cpal/wasm-bindgen" "caw_core/web") ("midi" "midly" "midir" "nix" "caw_core/midi") ("default" "midi"))))))

