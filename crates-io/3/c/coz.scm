(define-module (crates-io #{3}# c coz) #:use-module (crates-io))

(define-public crate-coz-0.1.0 (c (n "coz") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)))) (h "1vqb63m3wnipimygybb80sd75c97qp2c1sz8sn8ws3zqjkq4hx4l")))

(define-public crate-coz-0.1.1 (c (n "coz") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)))) (h "0xz7l9h0li62kkd56ilw5p2hpy75y29dipgdq2jq7xmys40njlpq")))

(define-public crate-coz-0.1.2 (c (n "coz") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)))) (h "0pr1y03cck4q5al000m1qq3kwd6b29x0psw49c5c7qz7s9zppizd")))

(define-public crate-coz-0.1.3 (c (n "coz") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)))) (h "1qknvqva3kkf02pczbcy16yjann9ngl95irbw5cpsizmw8zmpxff")))

