(define-module (crates-io #{3}# c cfn) #:use-module (crates-io))

(define-public crate-cfn-0.0.1 (c (n "cfn") (v "0.0.1") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1is4jwm9xkmkr1xvxa5lznlwmacmwwg4y14wxwz783sk3w8hhp99")))

(define-public crate-cfn-0.0.2 (c (n "cfn") (v "0.0.2") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14m0hg1f22x3fhsbddxn68wqw23zw7p4cwfqv7fm8xc7hqanggv0")))

(define-public crate-cfn-0.0.3 (c (n "cfn") (v "0.0.3") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1qkk3h1wq69nl6m5b6nba2s3h3kni4wzddbqp7hb85bc57cy2yia")))

(define-public crate-cfn-0.0.4 (c (n "cfn") (v "0.0.4") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qnpfgxwg8qzzr2md76xdky730gipajkkfmdnj240vkd6l6g98vi")))

(define-public crate-cfn-0.0.5 (c (n "cfn") (v "0.0.5") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dzhy1x3iincwjmp9ba1rwx01rxzw8v8sbn3vld90iz3zc7lq32h")))

(define-public crate-cfn-0.0.6 (c (n "cfn") (v "0.0.6") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0y20x1pr947i03q3qyqk1bdyp66zxi2jxwz3rv7yjx3df8bcapxr")))

(define-public crate-cfn-0.0.7 (c (n "cfn") (v "0.0.7") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cjpfi34mr4dybmvrabgk43vwjyanxxzxq3xdqpfz0bp763qs0yb")))

(define-public crate-cfn-0.0.8 (c (n "cfn") (v "0.0.8") (d (list (d (n "indexmap") (r "^1.0") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0x4xnfhmxnzf0q3kb8lmxbq134f3cx0zn5g095fv8ir42jp32qv1")))

