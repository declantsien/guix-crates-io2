(define-module (crates-io #{3}# c cag) #:use-module (crates-io))

(define-public crate-cag-0.0.1 (c (n "cag") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.20.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "12gnr4hk9x6ccwahgk81wkkx7r7zq8lrbrnn1z9pjcg9zmwjl50n")))

(define-public crate-cag-0.0.2 (c (n "cag") (v "0.0.2") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "ratatui") (r "^0.20.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "039myksizpbkkkb0lc0bygj2rm1xigswg61gxy0c27q69cwdn5x8")))

