(define-module (crates-io #{3}# c cnr) #:use-module (crates-io))

(define-public crate-cnr-0.1.0 (c (n "cnr") (v "0.1.0") (d (list (d (n "arr_macro") (r "^0.1.2") (d #t) (k 0)) (d (n "chashmap") (r "^2.2") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.6") (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "13c5bg2higl5vg8vy0s37r0mli4j12r9slcrkq831ysgw7yfwsfx")))

