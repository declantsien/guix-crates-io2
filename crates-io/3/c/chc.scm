(define-module (crates-io #{3}# c chc) #:use-module (crates-io))

(define-public crate-chc-0.1.0 (c (n "chc") (v "0.1.0") (h "00f9v0carg2s24l2dw45ss0g7ish7gvz19jmgim4zirjm9a2qp5z")))

(define-public crate-chc-0.1.1 (c (n "chc") (v "0.1.1") (h "0gzp94jz3p4dnd4d4m2dx7xjnlcdym0bkq7k8ra1mj5mwilzjgng")))

(define-public crate-chc-0.1.2 (c (n "chc") (v "0.1.2") (h "0c8ayf08mvmdcbxw6kg3vh5lbnwvcj1a6znh79aic5cgxjzg8fps")))

