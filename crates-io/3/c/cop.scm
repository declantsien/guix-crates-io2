(define-module (crates-io #{3}# c cop) #:use-module (crates-io))

(define-public crate-cop-0.1.0 (c (n "cop") (v "0.1.0") (d (list (d (n "colosseum") (r "^0.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tptp") (r "^0.28") (d #t) (k 0)))) (h "0yr1jzhhnva3xwfjaicmi5gcrc02lzsm5c7qqdp7gp8dikdlfzf6")))

(define-public crate-cop-0.2.0 (c (n "cop") (v "0.2.0") (d (list (d (n "colosseum") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "hashbrown") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "tptp") (r "^0.31.1") (o #t) (d #t) (k 0)))) (h "0kwj3wkdzn4vnsa2s7sd3pkxqpm0qc2wxa3a5pq3sjlsfzxk6dzp") (f (quote (("order" "num-bigint" "num-traits") ("default" "colosseum" "serde" "tptp" "order")))) (r "1.62")))

