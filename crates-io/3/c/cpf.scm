(define-module (crates-io #{3}# c cpf) #:use-module (crates-io))

(define-public crate-cpf-0.0.1 (c (n "cpf") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (o #t) (d #t) (k 0)))) (h "19dyz3vrhw4g9pmw3b2pfg59cadyabpgx1ii41anpfxb8d4n3dcx") (f (quote (("default"))))))

(define-public crate-cpf-0.1.0 (c (n "cpf") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "05kfcnykkk35p9hn19f8bcg40rmv6srwycw6kfrgizbav7va9ss6") (f (quote (("std") ("full" "std" "rand") ("default" "std")))) (y #t)))

(define-public crate-cpf-0.1.1 (c (n "cpf") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1a7mfnz89qfl34bdlka2fahlw06cj6vlbh35cx4nxjdm2sv9d2h7") (f (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cpf-0.1.2 (c (n "cpf") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "13ra7a0vsjblz904zm4jcxm2rflkybli6p6s4wbi0x57iqcr99is") (f (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cpf-0.1.3 (c (n "cpf") (v "0.1.3") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1xzshpcy9l16ghjw21jq01g7p7bvdfdnn13cxkr8jp32dly4h40k") (f (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cpf-0.1.4 (c (n "cpf") (v "0.1.4") (d (list (d (n "arrayvec") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1gssva72j5zjh8mj9spjfndaz5jbdhnw7k0sc3bwp5apzg7wicrm") (f (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cpf-0.2.0 (c (n "cpf") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0j7s9n0gfhj2hii67r90y22cagf7yp2lvdbca8n61c48ka04amcr") (f (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cpf-0.2.1 (c (n "cpf") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1c67rvkp77102xgl1lsz3yfm2yfg3s7g1ah50745fff3br1xppwm") (f (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cpf-0.2.2 (c (n "cpf") (v "0.2.2") (d (list (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng" "small_rng"))) (k 2)))) (h "05vcy569024dbg5y6zwbzp0ccccm9c20ynl19mxc8krwsiq841ld") (f (quote (("std") ("full" "std" "rand") ("default" "std"))))))

(define-public crate-cpf-0.3.0 (c (n "cpf") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng" "small_rng"))) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "1hc24lhp914lp7y6wp88mn0p3hd955cvdhrdqgd7pl1i8ifrq5qz") (f (quote (("full" "serde" "std" "rand") ("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-cpf-0.3.1 (c (n "cpf") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng" "small_rng"))) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "16jpcxzarlab9091mc7290kpw6968016jgkrhzkkby64720m6jrv") (f (quote (("full" "serde" "std" "rand") ("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

(define-public crate-cpf-0.3.2 (c (n "cpf") (v "0.3.2") (d (list (d (n "rand") (r "^0.8") (o #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std" "std_rng" "small_rng"))) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)))) (h "0j90dmgs6psnh04108piqbischnlfa2b2qrdy8bahlym1i3fnb5s") (f (quote (("full" "serde" "std" "rand") ("default" "std")))) (s 2) (e (quote (("std" "serde?/std") ("serde" "dep:serde"))))))

