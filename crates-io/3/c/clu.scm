(define-module (crates-io #{3}# c clu) #:use-module (crates-io))

(define-public crate-clu-0.0.2 (c (n "clu") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.33") (f (quote ("suggestions"))) (k 0)) (d (n "libclu") (r "^0.0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)) (d (n "witcher") (r "^0.1.19") (d #t) (k 0)))) (h "08c70dgklhlm8vh5vpa2nr1hrr2gg4q5rbynqcj41hb2hv14a085")))

