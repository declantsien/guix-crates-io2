(define-module (crates-io #{3}# c cpq) #:use-module (crates-io))

(define-public crate-cpq-0.0.0 (c (n "cpq") (v "0.0.0") (h "1cjfyf30wfc15kfwyjgdnxf6sc0ycfqrp7m3r39srsiz0gnkbg98")))

(define-public crate-cpq-0.1.0 (c (n "cpq") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1") (d #t) (k 0)))) (h "02xvq67grdr40i8i8186dzc22d1rjx1bwzwb2x33acba8p1wbbp3")))

(define-public crate-cpq-0.2.0 (c (n "cpq") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)))) (h "04f6fksm56gp5h3fnzjzjg8vb2pzd7wria9qm91l0mxypd9g503v")))

