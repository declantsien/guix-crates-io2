(define-module (crates-io #{3}# c cxp) #:use-module (crates-io))

(define-public crate-cxp-0.1.0 (c (n "cxp") (v "0.1.0") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "12dv4g8ydigm1v3syawiki17j2q46aml73hj5iqjhlnw7958v6l0")))

(define-public crate-cxp-0.1.1 (c (n "cxp") (v "0.1.1") (d (list (d (n "dirs") (r "^4.0.0") (d #t) (k 0)))) (h "0r65m9f996m4698asn34mfb6k15k5vd68m4q8mpsq3vz57qj2jp5")))

