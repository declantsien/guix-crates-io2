(define-module (crates-io #{3}# c clf) #:use-module (crates-io))

(define-public crate-clf-0.1.0 (c (n "clf") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0im01n8aqja9g6hlpmiwgwll7p8qqlqqxqa3v8jikynax95bp0kw")))

(define-public crate-clf-0.1.1 (c (n "clf") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0igz7drzxws2vwd6c7ssiagyv1xppknrpij11jkdcbg7xsswmcy4")))

(define-public crate-clf-0.1.2 (c (n "clf") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1h6mshgifgfbq805r7sy554ppirxz0ri9g16yj512dca2f0za7rh")))

(define-public crate-clf-0.1.3 (c (n "clf") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1acv7d2h3skly7n5hzh695faxya3fb6b5ba7f8m0h4v2abjjvbf2")))

(define-public crate-clf-0.1.4 (c (n "clf") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1mg9m88vmj4a5qxrqh5mcnfmdpqbgh96z1msqldf85wxs9wyz6l9")))

(define-public crate-clf-0.1.5 (c (n "clf") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "09x7xhcxpydknkqgfqksw20x6q2vx83qjccixlxzglp7al52mnha") (r "1.56.0")))

(define-public crate-clf-0.1.6 (c (n "clf") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1cdkwbg19xlg11qhsw6cmf3ayx7m5xg1dgipif4p8pilpsdy8wcb") (r "1.56.0")))

(define-public crate-clf-0.1.7 (c (n "clf") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0jac476rpb5lr8gh2mmmzkg2fll7xnnj7fah75bjz12b83m4dnsg") (r "1.56.0")))

