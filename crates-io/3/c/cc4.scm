(define-module (crates-io #{3}# c cc4) #:use-module (crates-io))

(define-public crate-cc4-0.1.0 (c (n "cc4") (v "0.1.0") (h "16x0mabkpd7xqx1isya8sjl96h3lp61kya5hy6kjxq6x3ka3n9dh")))

(define-public crate-cc4-0.1.1 (c (n "cc4") (v "0.1.1") (h "13b64vc71v3ih6npdv56dy5hdmb9yllc81nfcnklzrazj5synwr8")))

(define-public crate-cc4-0.1.2 (c (n "cc4") (v "0.1.2") (h "18yp6r3j902aa9f9wc9ssm8ns8s4pd2d00vln5l2di4jnimhfgfc")))

