(define-module (crates-io #{3}# c c99) #:use-module (crates-io))

(define-public crate-c99-0.1.0 (c (n "c99") (v "0.1.0") (h "1ajysfqah91yrq03zbrvaimmch91a1g58sdjkld1ygic665xskxf")))

(define-public crate-c99-0.1.1 (c (n "c99") (v "0.1.1") (h "00zlriq6j5p50kgnwrww5a1h1kcz67gpriyz6i42ji7av6q4jc5h")))

(define-public crate-c99-1.0.0 (c (n "c99") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "165708rqm6s3zril53ylrffqm55hwkfc3yw5sbjdlbf1aixqy95f")))

