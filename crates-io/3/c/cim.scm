(define-module (crates-io #{3}# c cim) #:use-module (crates-io))

(define-public crate-cim-0.3.5 (c (n "cim") (v "0.3.5") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1vidym1kilyck00drs1j39jd421dmg9fzwyaylh6cfg4ras92dvi")))

(define-public crate-cim-0.3.6 (c (n "cim") (v "0.3.6") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0j12dhn87igm1n7m0rr6fcisvfk7z345vhn1fyynp2r0niwg3p15")))

