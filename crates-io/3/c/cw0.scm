(define-module (crates-io #{3}# c cw0) #:use-module (crates-io))

(define-public crate-cw0-0.2.0 (c (n "cw0") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0535y9bs7pjpcck91ywmyskkawj7n2fvbxy0iixjj2c1dr5a908b")))

(define-public crate-cw0-0.2.1 (c (n "cw0") (v "0.2.1") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0cdbys7nn5fjkfhkhq9scf7szgjyja484h3iy3yrvffbpf64nm11")))

(define-public crate-cw0-0.2.2 (c (n "cw0") (v "0.2.2") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "061nwvdwfadaz2b9gb88rzzdj4h599darnc0bl3gw6dl1fxv2mfx")))

(define-public crate-cw0-0.2.3 (c (n "cw0") (v "0.2.3") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1phrgd4kj4qwx87l4pxii545ssq1ah9gnhihpff36dm0cdfvnsdl")))

(define-public crate-cw0-0.3.0 (c (n "cw0") (v "0.3.0") (d (list (d (n "cosmwasm-std") (r "^0.11.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1y8xgn1b7d0mxz2li90km6n5ifl229gk96px80z24ca7vpx28sfw")))

(define-public crate-cw0-0.3.1 (c (n "cw0") (v "0.3.1") (d (list (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0i64wv2lwxpnjl410bhl98g599gy4gznbqgd0zcp7m8csc8q4h19")))

(define-public crate-cw0-0.3.2 (c (n "cw0") (v "0.3.2") (d (list (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "146aah70c1rafqwlp1xs73py8f50zjn12gx85xr1k97sx7017qvm")))

(define-public crate-cw0-0.4.0 (c (n "cw0") (v "0.4.0") (d (list (d (n "cosmwasm-std") (r "^0.12.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0chbpnbzimfplk36xlgh8m7g1aag6kn1wsjw61ywizx8h3kx83r0")))

(define-public crate-cw0-0.5.0 (c (n "cw0") (v "0.5.0") (d (list (d (n "cosmwasm-std") (r "^0.13.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1v1sf6bd92cb1sbkz34g982s9w0sfa2j5dsg0spqfnb4sx4wspf1")))

(define-public crate-cw0-0.6.0-alpha1 (c (n "cw0") (v "0.6.0-alpha1") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "08dbf6swbyn96xrdjh4xhpq65mbywz1wvg678bd5ivwcd85y83ca")))

(define-public crate-cw0-0.6.0-alpha2 (c (n "cw0") (v "0.6.0-alpha2") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0ghv8kp7qsczfvamhx6s6r1r4rp34grmd7x68gy8hkqbw4r931qd")))

(define-public crate-cw0-0.6.0-alpha3 (c (n "cw0") (v "0.6.0-alpha3") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0l6336aw84ilcrmqs1bczy6ciqaqszmdmk0il1yhvia6h2nnp2x4")))

(define-public crate-cw0-0.6.0-beta1 (c (n "cw0") (v "0.6.0-beta1") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0fwp72wxdvx38by4drw7wq1bcqs2cywqzfblf6mk8i67mc875cpn")))

(define-public crate-cw0-0.6.0-beta2 (c (n "cw0") (v "0.6.0-beta2") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "02g63ski3vfc1vnwk8k52jbd508d9gdmbf66z3jbx6xqwl77n16q")))

(define-public crate-cw0-0.6.0-beta3 (c (n "cw0") (v "0.6.0-beta3") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta5") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1bsldnrsix6svv0vhw04g5mgz2gxawpm5nbfw8lxazyymxn7kv4r")))

(define-public crate-cw0-0.6.0 (c (n "cw0") (v "0.6.0") (d (list (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0ndhbj0pp5k0zlf3avyyq9yfaj6zshq6w3bij84zggqsc92c66zh")))

(define-public crate-cw0-0.6.1 (c (n "cw0") (v "0.6.1") (d (list (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "057sfxhr4206xzzhjcyn1d20z0r3y5vfx6yrsm0n6z2v63gz48xl")))

(define-public crate-cw0-0.6.2 (c (n "cw0") (v "0.6.2") (d (list (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1sps1jhkiy60d09gvrm41skjf2hzcqk4570kw35gaghbbwvf16v5")))

(define-public crate-cw0-0.7.0 (c (n "cw0") (v "0.7.0") (d (list (d (n "cosmwasm-std") (r "^0.15.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.7.0") (f (quote ("iterator"))) (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1y9pfxj6wpbsi5yg9jrahmkdxjznjhxiarg353kz7rd72kdvksy7")))

(define-public crate-cw0-0.8.0-rc1 (c (n "cw0") (v "0.8.0-rc1") (d (list (d (n "cosmwasm-std") (r "^0.16.0-rc5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.8.0-rc1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1si8s9cqjz64hkvjbw83zckyqnr0vhvkqj4c2bpj4srwbbvbkhk3")))

(define-public crate-cw0-0.8.0-rc2 (c (n "cw0") (v "0.8.0-rc2") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.8.0-rc2") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0b97klw5vdkpl41f65wpxy092vvj5nj1dd369mh40r0cg2h25y04")))

(define-public crate-cw0-0.8.0-rc3 (c (n "cw0") (v "0.8.0-rc3") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.8.0-rc3") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0c2zfjwsd8zsvdxnc54iipkcznkfdrs6szrbiv1d97zfvikri61h")))

(define-public crate-cw0-0.8.0 (c (n "cw0") (v "0.8.0") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.8.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "14s3dpvpxk0x71cvfc5kh7ifb9q373rbcnqrbzy6796chcz65g6y")))

(define-public crate-cw0-0.8.1 (c (n "cw0") (v "0.8.1") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.8.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1ag0bmxj6l4r3rcxrcr1zifa0pwn14cchf6rywns0609lj2zi5y4")))

(define-public crate-cw0-0.9.0 (c (n "cw0") (v "0.9.0") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1gigs18y6cjvmygzlkiqj888qqi9mz5p0avdcq7w4wnb6vy5xnii")))

(define-public crate-cw0-0.10.0-soon (c (n "cw0") (v "0.10.0-soon") (d (list (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "11vwlg7dzh2bvl7i7sd8hfnblh83qrkgsd4mkqrknlmj1zl83gdr")))

(define-public crate-cw0-0.9.1 (c (n "cw0") (v "0.9.1") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1jniv73g8qg4q92q1qpywhmmvn8m5bg1ggpiw68z1gd331abnnfp")))

(define-public crate-cw0-0.10.0-soon2 (c (n "cw0") (v "0.10.0-soon2") (d (list (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon2") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0d6mnxx0zd7qcc0iaf5c5jypdkcg9lfhymz4x4gb677pp8117gav")))

(define-public crate-cw0-0.10.0-soon3 (c (n "cw0") (v "0.10.0-soon3") (d (list (d (n "cosmwasm-std") (r "=1.0.0-soon") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon3") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0xjm2v0fhz8953ih92qprw1d4sn12pn7j5v9m6l6cpsjx69x5h7k")))

(define-public crate-cw0-0.10.0-soon4 (c (n "cw0") (v "0.10.0-soon4") (d (list (d (n "cosmwasm-std") (r "=1.0.0-soon2") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon4") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0pf7s7dxss9x7v9bpj72d4n6q6hr7smyrvbs5j0x5hjb1zya4r3j")))

(define-public crate-cw0-0.10.0 (c (n "cw0") (v "0.10.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.0") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "1icvzvk9ks8sk1hh6q98dsni0xwxdq0knqcmaay87j0fqvjd4zhs")))

(define-public crate-cw0-0.10.1 (c (n "cw0") (v "0.10.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.1") (d #t) (k 2)) (d (n "prost") (r "^0.9") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "15mf01y2s53vv6n32rbc2xfkn27idhdhxisnd2ljmwjwhv1816n8")))

(define-public crate-cw0-0.10.2 (c (n "cw0") (v "0.10.2") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.2") (d #t) (k 2)) (d (n "prost") (r "^0.9") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0n2sxrzmkh56xxaqnfccxlriznr7algbwzy6wh9znr4pnjh2hwzw")))

(define-public crate-cw0-0.10.3 (c (n "cw0") (v "0.10.3") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.10.3") (d #t) (k 2)) (d (n "prost") (r "^0.9") (d #t) (k 2)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.21") (d #t) (k 0)))) (h "0fs7zlpcnwjkzmmhsg0w36n7iz36xns1mc6lib9s6y7drjv7drla")))

