(define-module (crates-io #{3}# c cig) #:use-module (crates-io))

(define-public crate-cig-0.1.0 (c (n "cig") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1mxy78lp3ss8nxq8a4qpb6aqkmxw1psxyvx7wgm03c92zm0jlz1g")))

(define-public crate-cig-0.1.1 (c (n "cig") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "0xrlcdl9jx09apkma8nwkqyyxaxi9sspk6r5fcirwjdfkpa9ps5k")))

(define-public crate-cig-0.1.2 (c (n "cig") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("serde_derive"))) (d #t) (k 0)))) (h "1swzclh5gm8sgw8skv5pz06589s0vl2pclvlgy8rh7yrz3ig7p0k")))

