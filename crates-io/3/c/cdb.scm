(define-module (crates-io #{3}# c cdb) #:use-module (crates-io))

(define-public crate-cdb-0.5.0 (c (n "cdb") (v "0.5.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "mmap") (r "^0.1.1") (d #t) (k 0)))) (h "0c66ahmxkx3n0cczr9blb00fkk0km5lc47qgjrgjgiilqc1hyidc")))

(define-public crate-cdb-0.6.0 (c (n "cdb") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "filebuffer") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)))) (h "1990nfarpp4fc1limrvi2x07wxmx6cmq0mzb4v4c69nbgx46a0yl")))

