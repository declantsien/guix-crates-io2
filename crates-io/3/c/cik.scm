(define-module (crates-io #{3}# c cik) #:use-module (crates-io))

(define-public crate-cik-0.1.0 (c (n "cik") (v "0.1.0") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0izw4q4b2wykabcg5cj06zb64iyn7xd1c4zgkjhw0wydzf2hiv6a")))

(define-public crate-cik-0.1.1 (c (n "cik") (v "0.1.1") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1xwv5rzcg16i2c7njk8p3r5jpkdxrgicvhhapsbnsrkmzwwld20l")))

(define-public crate-cik-0.1.2 (c (n "cik") (v "0.1.2") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0xap0r7psmarc1xcxhp7p3wbqs32rrjqxaw9ajyzb8lbyhb9s33y")))

(define-public crate-cik-0.1.3 (c (n "cik") (v "0.1.3") (d (list (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0dlxmkfzj761fg2i4yzl5cpmxlmw8g8wyyrpx8qf198a3fqfga60")))

(define-public crate-cik-0.1.4 (c (n "cik") (v "0.1.4") (d (list (d (n "proptest") (r "^1.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "05ffrscx4zd8pcwxnxcr2hm209fh2wxjs8f8ap17kdaqh8z4s16x") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

