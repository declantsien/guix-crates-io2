(define-module (crates-io #{3}# c cew) #:use-module (crates-io))

(define-public crate-cew-0.1.0 (c (n "cew") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (k 0)))) (h "0ksp2658q2phcnpbmlbp2qphv54dmfsql3fry242bmyilli3mc7m") (f (quote (("url" "color-eyre/issue-url") ("track-caller" "color-eyre/track-caller") ("default" "track-caller" "capture-spantrace") ("capture-spantrace" "color-eyre/capture-spantrace"))))))

(define-public crate-cew-0.2.0 (c (n "cew") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6.2") (k 0)))) (h "1dyfif3iw81x0jbmrj7g1fi4prmcclm3gbdynvmcrmmibmk85s0m") (f (quote (("url" "color-eyre/issue-url") ("track-caller" "color-eyre/track-caller") ("default" "track-caller" "capture-spantrace") ("capture-spantrace" "color-eyre/capture-spantrace"))))))

(define-public crate-cew-0.3.0 (c (n "cew") (v "0.3.0") (d (list (d (n "color-eyre") (r "^0.6.2") (o #t) (k 0)))) (h "1s3dmpp1z5kzdkb9qr2s5g753z4kr50xdl3pf03x9jac5lpb8s6q") (f (quote (("default" "color_eyre" "track_caller" "capture_spantrace") ("color_eyre" "color-eyre")))) (s 2) (e (quote (("url" "color-eyre?/issue-url") ("track_caller" "color-eyre?/track-caller") ("capture_spantrace" "color-eyre?/capture-spantrace"))))))

(define-public crate-cew-0.4.0 (c (n "cew") (v "0.4.0") (d (list (d (n "color-eyre") (r "^0.6.2") (o #t) (k 0)))) (h "1s81nygplkxjq9kfpvg391vs8dmaw0x3hmyrplf7xdgqand59kya") (f (quote (("default" "color_eyre" "track_caller" "capture_spantrace") ("color_eyre" "color-eyre")))) (s 2) (e (quote (("url" "color-eyre?/issue-url") ("track_caller" "color-eyre?/track-caller") ("capture_spantrace" "color-eyre?/capture-spantrace"))))))

(define-public crate-cew-0.5.0 (c (n "cew") (v "0.5.0") (d (list (d (n "color-eyre") (r "^0.6.2") (o #t) (k 0)))) (h "18k0m54sn1kfdacbf7imbz9aywj3801x0iw1nm2nna0q8na9dimi") (f (quote (("piping") ("default" "color_eyre" "track_caller" "capture_spantrace" "block_on" "piping") ("color_eyre" "color-eyre") ("block_on")))) (s 2) (e (quote (("url" "color-eyre?/issue-url") ("track_caller" "color-eyre?/track-caller") ("capture_spantrace" "color-eyre?/capture-spantrace"))))))

(define-public crate-cew-0.5.1 (c (n "cew") (v "0.5.1") (d (list (d (n "color-eyre") (r "^0.6.2") (o #t) (k 0)))) (h "1sfy2g3pahkr025wkxm1m82s0pcyl6d1xzsck80ha7y9r5yxi6cy") (f (quote (("piping") ("default" "color_eyre" "track_caller" "capture_spantrace" "block_on" "piping") ("color_eyre" "color-eyre") ("block_on")))) (s 2) (e (quote (("url" "color-eyre?/issue-url") ("track_caller" "color-eyre?/track-caller") ("capture_spantrace" "color-eyre?/capture-spantrace"))))))

(define-public crate-cew-0.6.0 (c (n "cew") (v "0.6.0") (d (list (d (n "color-eyre") (r "^0.6.2") (o #t) (k 0)))) (h "09cw359avzcwhgyw5g1swnplkj8kqmfifk171pjcdap14mn7rwrf") (f (quote (("piping") ("default" "color-eyre" "track-caller" "capture-spantrace" "block-on" "piping") ("block-on")))) (s 2) (e (quote (("track-caller" "color-eyre?/track-caller") ("issue-url" "color-eyre?/issue-url") ("capture-spantrace" "color-eyre?/capture-spantrace"))))))

