(define-module (crates-io #{3}# c cmn) #:use-module (crates-io))

(define-public crate-cmn-0.0.1 (c (n "cmn") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "13s8kg5mzrmpc66l0mg77manrd29gdp0rinz9gg80iambbb08xl8") (f (quote (("default")))) (r "1.67.0")))

(define-public crate-cmn-0.0.2 (c (n "cmn") (v "0.0.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)))) (h "14zvqjcz2a4163fawh7nm1llfr5xn5haq6bid26xrsqkr87m0h1w") (f (quote (("default")))) (r "1.67")))

(define-public crate-cmn-0.0.3 (c (n "cmn") (v "0.0.3") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "120klc29gcd9fqhgaif6xs6cq7yghc1y0415w6whsf9c7r92g550") (f (quote (("default")))) (r "1.69.0")))

(define-public crate-cmn-0.0.4 (c (n "cmn") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.201") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (d #t) (k 0)))) (h "1bn4fgl6hns58bhybjnw2lmbhk95yy4xwfrx2ldia0mjd7z74yzq") (f (quote (("default")))) (r "1.60")))

