(define-module (crates-io #{3}# c can) #:use-module (crates-io))

(define-public crate-can-0.1.0 (c (n "can") (v "0.1.0") (h "03h5z8i3i297wks8vys0290fh0ib65srck02nshh9h391a6dm2v8")))

(define-public crate-can-0.2.0 (c (n "can") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-can") (r "^0.3.0") (o #t) (k 0)) (d (n "socketcan") (r "^1.7.0") (o #t) (k 0)))) (h "0biwmfnzq5idgg3pa0jdlgw6phqd9hwz12yi38s7vacmdw9k26wp") (f (quote (("socketcan-compat" "socketcan") ("embedded-can-compat" "embedded-can") ("default" "embedded-can-compat" "socketcan-compat"))))))

(define-public crate-can-0.2.1-alpha.1 (c (n "can") (v "0.2.1-alpha.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-can") (r "^0.3.0") (o #t) (k 0)) (d (n "socketcan") (r "^1.7.0") (o #t) (k 0)))) (h "1w69m3wxiyyjjjs2khm12g426w3vdyg3jqy3f4gmas47xismyznv") (f (quote (("socketcan-compat" "socketcan") ("embedded-can-compat" "embedded-can") ("default" "embedded-can-compat" "socketcan-compat"))))))

(define-public crate-can-0.2.1-alpha.2 (c (n "can") (v "0.2.1-alpha.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-can") (r "^0.3.0") (o #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "socketcan") (r "^1.7.0") (o #t) (k 0)))) (h "1nwbikdcd9p7ddajxsy24vsjyzgr9kkmd1cb5rg88d7ikisw8vw9") (f (quote (("socketcan-compat" "socketcan") ("embedded-can-compat" "embedded-can") ("default" "embedded-can-compat" "socketcan-compat"))))))

(define-public crate-can-0.2.1-alpha.3 (c (n "can") (v "0.2.1-alpha.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-can") (r "^0.3.0") (o #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "socketcan") (r "^1.7.0") (o #t) (k 0)))) (h "0l1svk511nfri9k1blc03d5h60xmh0n0rn5yr9pgyx6pcv1ay92y") (f (quote (("socketcan-compat" "socketcan") ("embedded-can-compat" "embedded-can") ("default" "embedded-can-compat" "socketcan-compat"))))))

(define-public crate-can-0.2.1-alpha.4 (c (n "can") (v "0.2.1-alpha.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "embedded-can") (r "^0.3.0") (o #t) (k 0)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "socketcan") (r "^1.7.0") (o #t) (k 0)))) (h "1inyh96g9b6gs7wmr8sx8wfdvp0110n428x3ffhr13ayab74ndfz") (f (quote (("socketcan-compat" "socketcan") ("embedded-can-compat" "embedded-can") ("default" "embedded-can-compat" "socketcan-compat"))))))

