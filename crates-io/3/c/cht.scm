(define-module (crates-io #{3}# c cht) #:use-module (crates-io))

(define-public crate-cht-0.1.0 (c (n "cht") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.7") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.5") (d #t) (k 2)) (d (n "lock_api") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8") (d #t) (k 2)))) (h "1xkq1i1hzb6jy8p50y410mibpk0m5wj7c4bgwf02nl59h2qh6sy3")))

(define-public crate-cht-0.1.1 (c (n "cht") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.7") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.5") (d #t) (k 2)) (d (n "lock_api") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8") (d #t) (k 2)))) (h "0llljr42198jnhdvzsr2qy3hdfszw052ya5nli4fkwk8q4mr16c6")))

(define-public crate-cht-0.1.2 (c (n "cht") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.7") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.5") (d #t) (k 2)) (d (n "lock_api") (r "^0.2") (d #t) (k 2)) (d (n "parking_lot") (r "^0.8") (d #t) (k 2)))) (h "114byqlv65i6x3h2ypy0m3d5bk8f7h7jjrk1l2ch8npv0gp7nn44")))

(define-public crate-cht-0.2.0 (c (n "cht") (v "0.2.0") (d (list (d (n "ahash") (r "^0.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.8.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.0") (d #t) (k 2)) (d (n "lock_api") (r "^0.3.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 2)))) (h "0br34czf9qgsysmx9w6djqy24x48dg5qk0if1c7l341m2mhd0ig3")))

(define-public crate-cht-0.3.0 (c (n "cht") (v "0.3.0") (d (list (d (n "ahash") (r "^0.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.8.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.0") (d #t) (k 2)) (d (n "lock_api") (r "^0.3.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 2)))) (h "0h7pdpnb1xhlspigyz8wmv1nkd8pw42scad8w786w46l8k2xp0qi")))

(define-public crate-cht-0.4.0 (c (n "cht") (v "0.4.0") (d (list (d (n "ahash") (r "^0.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.8.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.0") (d #t) (k 2)) (d (n "lock_api") (r "^0.3.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 2)))) (h "0pgbbkjdlnfl1ql2p7bwiaqm2hws839lfq55vi39xqfjqc5cs4la") (f (quote (("num-cpus" "num_cpus") ("default" "num-cpus"))))))

(define-public crate-cht-0.4.1 (c (n "cht") (v "0.4.1") (d (list (d (n "ahash") (r "^0.3.2") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.8.2") (d #t) (k 0)) (d (n "hashbrown") (r "^0.7.0") (d #t) (k 2)) (d (n "lock_api") (r "^0.3.3") (d #t) (k 2)) (d (n "num_cpus") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 2)))) (h "0zxpva9ydq2k2af5ln81bs6fd8j1p6kw9xl9balsvflbf26bbmbi") (f (quote (("num-cpus" "num_cpus") ("default" "num-cpus"))))))

(define-public crate-cht-0.5.0 (c (n "cht") (v "0.5.0") (d (list (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)))) (h "1r7j9qsy6cx5wjssyygzwlhy2hfz20dqiinhy6mhcy5x8l3z7zz5")))

