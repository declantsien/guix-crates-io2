(define-module (crates-io #{3}# c cex) #:use-module (crates-io))

(define-public crate-cex-0.0.1 (c (n "cex") (v "0.0.1") (h "04a3nwms08nsd2myc08bygnsgj4ixkgpbhx7bilng383pj8cdcdd")))

(define-public crate-cex-0.1.0 (c (n "cex") (v "0.1.0") (d (list (d (n "cex_derive") (r "^0.1") (d #t) (k 0)) (d (n "enumx") (r "^0.1") (d #t) (k 0)))) (h "10kdsw947mf0hwdjizig9a6aviv29zv98bjx8zizyv2jwdh9n46s")))

(define-public crate-cex-0.1.1 (c (n "cex") (v "0.1.1") (d (list (d (n "cex_derive") (r "^0.1") (d #t) (k 0)) (d (n "enumx") (r "^0.1") (d #t) (k 0)))) (h "05rpr0pk7yj5r9n8lb5fdy6r6r7fnplzs7w4aaflyaq2bah493mg")))

(define-public crate-cex-0.2.1 (c (n "cex") (v "0.2.1") (d (list (d (n "cex_derive") (r "^0.2.1") (d #t) (k 0)) (d (n "enumx") (r "^0.2.1") (d #t) (k 0)) (d (n "enumx_derive") (r "^0.2.1") (d #t) (k 2)))) (h "09dibqx2v0wvj8v6bkvbcsw7a2rd0zplvs5gvj99q2axwxy1lqfx")))

(define-public crate-cex-0.3.0-alpha (c (n "cex") (v "0.3.0-alpha") (d (list (d (n "cex_derive") (r "^0.3.0-alpha") (d #t) (k 2)) (d (n "enumx") (r "^0.3.0-alpha") (d #t) (k 0)) (d (n "enumx_derive") (r "^0.3.0-alpha") (d #t) (k 2)))) (h "06zb3blzk0k3d8bjwhr0zir9rxhhvanqxk9cd008556dd4kr5f41")))

(define-public crate-cex-0.3.0 (c (n "cex") (v "0.3.0") (d (list (d (n "cex_derive") (r "^0.3") (d #t) (k 2)) (d (n "enumx") (r "^0.3") (d #t) (k 0)) (d (n "enumx_derive") (r "^0.3") (d #t) (k 2)) (d (n "failure") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0va4486q3im4h1kddgxg99iw4kwlgxp3dyxbz4b5vz64kf4z2iqc") (f (quote (("dyn_err" "failure"))))))

(define-public crate-cex-0.4.0 (c (n "cex") (v "0.4.0") (d (list (d (n "cex_derive") (r "^0.4") (d #t) (k 0)) (d (n "enumx") (r "^0.3.1") (d #t) (k 0)))) (h "0p2y9y71s8583v05fi95540jsi53026vv6dw62dl23hw11706vw8") (f (quote (("pretty_log") ("log") ("env_log"))))))

(define-public crate-cex-0.5.0 (c (n "cex") (v "0.5.0") (d (list (d (n "cex_derive") (r "^0.5") (d #t) (k 0)) (d (n "enumx") (r "^0.4") (d #t) (k 0)))) (h "0r94h3ikrjxzwj3i8qncsyc887hznbvpaiz6nri0bfqfipmaa4py") (f (quote (("pretty_log") ("log") ("env_log") ("enum32" "enumx/enum32"))))))

(define-public crate-cex-0.5.1 (c (n "cex") (v "0.5.1") (d (list (d (n "cex_derive") (r "^0.5.1") (d #t) (k 0)) (d (n "enumx") (r "^0.4.1") (d #t) (k 0)))) (h "1qyvj89k97ykj4h3mml6jb4rpdmj5n8rd1209jrgpkcyzzvi77ib") (f (quote (("pretty_log") ("log") ("env_log") ("enum32" "enumx/enum32"))))))

(define-public crate-cex-0.5.2 (c (n "cex") (v "0.5.2") (d (list (d (n "cex_derive") (r "^0.5.2") (d #t) (k 0)) (d (n "enumx") (r "^0.4.1") (d #t) (k 0)))) (h "1nl021b0s6fhlz1kl1jbifsw6s1yirl95dnvpfnkyhijyaii80cb") (f (quote (("unstable") ("pretty_log") ("log") ("env_log") ("enum32" "enumx/enum32"))))))

