(define-module (crates-io #{3}# c cce) #:use-module (crates-io))

(define-public crate-cce-0.1.0 (c (n "cce") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "dirs") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "urlparse") (r "^0.7") (d #t) (k 0)))) (h "03x5ivmxijjfv76pnzqvc4rb5n6m517p178a18ffy1w5jqq6137d")))

