(define-module (crates-io #{3}# c ced) #:use-module (crates-io))

(define-public crate-ced-0.1.0 (c (n "ced") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0xml1crl0867w8jhlg1af4pqs6sq7mw0cr77fx3d3my90i4sa404") (f (quote (("cli"))))))

(define-public crate-ced-0.1.1 (c (n "ced") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0i2px3hq9ya3vgc5fj67f3papmpsqar79zxp1cqgz2g6v1nfgm7m") (f (quote (("cli"))))))

(define-public crate-ced-0.1.2 (c (n "ced") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0v4qimjbb7f4gm474p9s7k3gbfn0rm8hmnlxmf2b61vq7w40vvgz") (f (quote (("cli"))))))

(define-public crate-ced-0.1.3 (c (n "ced") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1xij7djf4n6sbdmbqq60b41c7391lghpgfglfs45z0fflywarf0c") (f (quote (("cli"))))))

(define-public crate-ced-0.1.4 (c (n "ced") (v "0.1.4") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "18d9ym5cd1r068fb0kl0djcd6qbnxia3i6mpnn69qlx1cdh6fijk") (f (quote (("cli"))))))

(define-public crate-ced-0.1.5 (c (n "ced") (v "0.1.5") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1zjgia3mylm3kbc9msrfbylcz5x9n2kk663gngyyqfggxbiys4fp") (f (quote (("cli"))))))

(define-public crate-ced-0.1.6 (c (n "ced") (v "0.1.6") (d (list (d (n "dcsv") (r "^0.1.1") (d #t) (k 0)))) (h "1gzpc41rw2rzqlz12x58394jdna4v2ribrv9m6jwyfk9a202r688") (f (quote (("cli"))))))

(define-public crate-ced-0.1.7 (c (n "ced") (v "0.1.7") (d (list (d (n "dcsv") (r "^0.1.3") (d #t) (k 0)))) (h "00ff8j862sfrp6nqzi2irlgqswpjq7cr9z81hs3zhgjr6id8syzg") (f (quote (("cli"))))))

(define-public crate-ced-0.2.0 (c (n "ced") (v "0.2.0") (d (list (d (n "dcsv") (r "^0.3.0") (d #t) (k 0)))) (h "131jjbvx1i778aq79dqyzcrzxmr804hp21gk60n4khqdpnd3s0nd") (f (quote (("cli"))))))

(define-public crate-ced-0.2.1 (c (n "ced") (v "0.2.1") (d (list (d (n "dcsv") (r "^0.3.0") (d #t) (k 0)))) (h "1sdik7plqygfpyc7hdqrj002h855lfxjc5vfi8a3w28lr9zgnwbf") (f (quote (("cli"))))))

(define-public crate-ced-0.2.2 (c (n "ced") (v "0.2.2") (d (list (d (n "dcsv") (r "^0.3.0") (d #t) (k 0)))) (h "137mq01aq9hhc4x0c22jy1ibqzip2ixqd28n4mr6mzc36r7whd2k") (f (quote (("cli"))))))

