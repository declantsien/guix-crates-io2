(define-module (crates-io #{3}# c cyl) #:use-module (crates-io))

(define-public crate-cyl-0.3.0 (c (n "cyl") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cylinder") (r "=0.3.0") (f (quote ("jwt" "key-load"))) (d #t) (k 0)) (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1rg4dpsq7s709g1lr430v5vvhr91c6z45jmzz9sf7akxv655k6dz") (f (quote (("stable" "default") ("experimental" "stable") ("default"))))))

(define-public crate-cyl-0.3.1 (c (n "cyl") (v "0.3.1") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cylinder") (r "=0.3.1") (f (quote ("jwt" "key-load"))) (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "flexi_logger") (r "^0.27") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "14iw6ki07jqif0k68df597jaq41fj6cbkc0vipx5i8l2dz3b9w2h") (f (quote (("stable" "default") ("experimental" "stable") ("default"))))))

