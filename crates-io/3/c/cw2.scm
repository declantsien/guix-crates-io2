(define-module (crates-io #{3}# c cw2) #:use-module (crates-io))

(define-public crate-cw2-0.1.0 (c (n "cw2") (v "0.1.0") (d (list (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1gby3agp6c82d6yczmsgxy1r7jswnlc91sfyw4a0d7ywy1a85csj")))

(define-public crate-cw2-0.1.1 (c (n "cw2") (v "0.1.1") (d (list (d (n "cosmwasm-std") (r "^0.10.0") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "19db525609an5qpys17kw2dp90x526q9d4702fsw17mxl7rs33rd")))

(define-public crate-cw2-0.2.0 (c (n "cw2") (v "0.2.0") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0fciprkc8ihddmbkzhj0nrkrf2k1c3ijjppa5l6g1npv1zxrflnb")))

(define-public crate-cw2-0.2.1 (c (n "cw2") (v "0.2.1") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1jq7cc5k40p74y539z6ywjkk1mk9n52q57gc8db1pgccg3bjrx6f")))

(define-public crate-cw2-0.2.2 (c (n "cw2") (v "0.2.2") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0kpkzg9fsw2wmdxbc4nnm1bdnb7jpq903djdz8xvapjslxz5sz9b")))

(define-public crate-cw2-0.2.3 (c (n "cw2") (v "0.2.3") (d (list (d (n "cosmwasm-std") (r "^0.10.1") (d #t) (k 0)) (d (n "cosmwasm-storage") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "02x9yb32biilqz4chb64q7cs53cjd63wqzq7w2093bkp0abdv9d1")))

(define-public crate-cw2-0.3.0 (c (n "cw2") (v "0.3.0") (d (list (d (n "cosmwasm-std") (r "^0.11.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.3.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1d83axlbnjshxf516dnqfimpf7xsnw4hfjnj28a4n4a5v51yp9hx")))

(define-public crate-cw2-0.3.1 (c (n "cw2") (v "0.3.1") (d (list (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.3.1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "174gl6fpch443vhlq2rgbiix2a91c8nyp1m2l4dcxvfh0y5b763v")))

(define-public crate-cw2-0.3.2 (c (n "cw2") (v "0.3.2") (d (list (d (n "cosmwasm-std") (r "^0.11.1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.3.2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0l0yx8sq3jm0jsavb9h8wfmnndr43g6wpmfprnkl6rzd4dmy03lm")))

(define-public crate-cw2-0.4.0 (c (n "cw2") (v "0.4.0") (d (list (d (n "cosmwasm-std") (r "^0.12.2") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.4.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1p2iwj4p3ar05sm6mndnhy22gkixnp0bcwd56ryg8sbivq8l8mmf")))

(define-public crate-cw2-0.5.0 (c (n "cw2") (v "0.5.0") (d (list (d (n "cosmwasm-std") (r "^0.13.2") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.5.0") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1bwv6lbv4hp5vb8hr8h36haph32l9599bwshb1agykhcpkhifq2q")))

(define-public crate-cw2-0.6.0-alpha1 (c (n "cw2") (v "0.6.0-alpha1") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.6.0-alpha1") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "143hcc84qv2xidkjx3g005fmq1vwz5r519vc9fsmknigrpdq1rxv")))

(define-public crate-cw2-0.6.0-alpha2 (c (n "cw2") (v "0.6.0-alpha2") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.6.0-alpha2") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0rsapavhxzmnq45r5zqr2q2sq6vjjy6a65ajlm2qw22x1y2gy3nv")))

(define-public crate-cw2-0.6.0-alpha3 (c (n "cw2") (v "0.6.0-alpha3") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta1") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.6.0-alpha3") (d #t) (k 0)) (d (n "schemars") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1wicfxrv34jwgich1svzd5pwi29x4dkbih5a6lkp8w8wlq8fbxdd")))

(define-public crate-cw2-0.6.0-beta1 (c (n "cw2") (v "0.6.0-beta1") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.6.0-beta1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0n0jrm825mdfrhpczp1qzxxbbqk020i906l9hhgd62phi5yp6ayz")))

(define-public crate-cw2-0.6.0-beta2 (c (n "cw2") (v "0.6.0-beta2") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta3") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.6.0-beta2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0in04wxds71x1r7k0n3sff826y0jhyiy8c04pdi8763a1882r26n")))

(define-public crate-cw2-0.6.0-beta3 (c (n "cw2") (v "0.6.0-beta3") (d (list (d (n "cosmwasm-std") (r "^0.14.0-beta5") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.6.0-beta3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1w00vx1d3yin85jn8m1p4d9fi9x6jx4xrlx9817x5cyziswzmcfw")))

(define-public crate-cw2-0.6.0 (c (n "cw2") (v "0.6.0") (d (list (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.6.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0m32b8g47x1lspk92mkj1j5l19s566pml2zyja3gh53b4zvj5iar")))

(define-public crate-cw2-0.6.1 (c (n "cw2") (v "0.6.1") (d (list (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.6.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0qr0rw2b6y6v133r1nkm3xv7lzs1ijwply704hgbkqd3fbq64s6b")))

(define-public crate-cw2-0.6.2 (c (n "cw2") (v "0.6.2") (d (list (d (n "cosmwasm-std") (r "^0.14.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.6.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "12ns61zj0kma9dn2nwddmwcw732rpqnijppd07wa61330ps8dq82")))

(define-public crate-cw2-0.7.0 (c (n "cw2") (v "0.7.0") (d (list (d (n "cosmwasm-std") (r "^0.15.0") (d #t) (k 0)) (d (n "cw-storage-plus") (r "^0.7.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1rpfka5vv4xcw3pp3fv43z9iknn4mcw0piv4fjxmhswf4s38qqvy")))

(define-public crate-cw2-0.8.0-rc1 (c (n "cw2") (v "0.8.0-rc1") (d (list (d (n "cosmwasm-std") (r "^0.16.0-rc5") (k 0)) (d (n "cw-storage-plus") (r "^0.8.0-rc1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0qhgsvy21sqd3fzxbpa86hapl8c0q2nimjynvayfi5pqlbzg0c9z")))

(define-public crate-cw2-0.8.0-rc2 (c (n "cw2") (v "0.8.0-rc2") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (k 0)) (d (n "cw-storage-plus") (r "^0.8.0-rc2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "12bxm222cm8dj2iwgwvy045zdsbz6kbghr67h519nspm0nyzfg7g")))

(define-public crate-cw2-0.8.0-rc3 (c (n "cw2") (v "0.8.0-rc3") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (k 0)) (d (n "cw-storage-plus") (r "^0.8.0-rc3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0l0izhg0k3d8gl8gphzy99hbabqpr9xz88ikhfgnsn03j8bmjf62")))

(define-public crate-cw2-0.8.0 (c (n "cw2") (v "0.8.0") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (k 0)) (d (n "cw-storage-plus") (r "^0.8.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "144bxvd43yjfzn99gy4x53k4zl454q2jy231f8fx7qwbvx88lsai")))

(define-public crate-cw2-0.8.1 (c (n "cw2") (v "0.8.1") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (k 0)) (d (n "cw-storage-plus") (r "^0.8.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0n029g15xsm2rxkwgrgkgv2whp1pfxwlbkanhl0s2jj9jr7laj2d")))

(define-public crate-cw2-0.9.0 (c (n "cw2") (v "0.9.0") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (k 0)) (d (n "cw-storage-plus") (r "^0.9.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "03g4ajax8ifi5n60gm2zlmvyzjmd8a58rmfqqbl9ivqr9m879y3k")))

(define-public crate-cw2-0.10.0-soon (c (n "cw2") (v "0.10.0-soon") (d (list (d (n "cosmwasm-std") (r "=1.0.0-soon") (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "067avisbz15c37rn1bzn2pr9abpar2gs9ra0gb2iydaqq4gzcw91")))

(define-public crate-cw2-0.9.1 (c (n "cw2") (v "0.9.1") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (k 0)) (d (n "cw-storage-plus") (r "^0.9.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1xm8yah2fcf22fjx7cjiikxccisqv5ss0z1wyhpl2dmj9wb80qrw")))

(define-public crate-cw2-0.10.0-soon2 (c (n "cw2") (v "0.10.0-soon2") (d (list (d (n "cosmwasm-std") (r "=1.0.0-soon") (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1h615c5g5dkjzj6an494rfmhw0x82bl8jxnwrl99ay6ihh8vfmbx")))

(define-public crate-cw2-0.10.0-soon3 (c (n "cw2") (v "0.10.0-soon3") (d (list (d (n "cosmwasm-std") (r "=1.0.0-soon") (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0l4sgvrmypgb2ycnxw698s1zwsyyx4z7iqlwfkcbxfpwhf0acanh")))

(define-public crate-cw2-0.10.0-soon4 (c (n "cw2") (v "0.10.0-soon4") (d (list (d (n "cosmwasm-std") (r "=1.0.0-soon2") (k 0)) (d (n "cw-storage-plus") (r "^0.10.0-soon4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0vhk87ac823h3zdiz9p33sqg3djvhkcgs3vrbf7bqd0anqn1v0wi")))

(define-public crate-cw2-0.10.0 (c (n "cw2") (v "0.10.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta") (k 0)) (d (n "cw-storage-plus") (r "^0.10.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "03yricy70ricaal5xxjm47y3myx3l4iyz8y0z61kjxclwpippp7l")))

(define-public crate-cw2-0.10.1 (c (n "cw2") (v "0.10.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta") (k 0)) (d (n "cw-storage-plus") (r "^0.10.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1aylaa5bgv29w2wkjpw3bqw1fc898frmzyppjgjan4rw7nng1dlx")))

(define-public crate-cw2-0.10.2 (c (n "cw2") (v "0.10.2") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta") (k 0)) (d (n "cw-storage-plus") (r "^0.10.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "178dpj1ixfxfgvmw3hmp7w3sylkpqszc36jd3fingimzf21pkdq8")))

(define-public crate-cw2-0.10.3 (c (n "cw2") (v "0.10.3") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta") (k 0)) (d (n "cw-storage-plus") (r "^0.10.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1n53z4scfxv03majinggjnac605s0rillw6abn8z67hskipjw07p")))

(define-public crate-cw2-0.11.0 (c (n "cw2") (v "0.11.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta3") (k 0)) (d (n "cw-storage-plus") (r "^0.11.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "19jxfqpm5znlhjg4071qa8wsgd3z09rncrsshzp1asra3cg0sid1")))

(define-public crate-cw2-0.11.1 (c (n "cw2") (v "0.11.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta3") (k 0)) (d (n "cw-storage-plus") (r "^0.11.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0hwpavdw6mqjz1iax00kaw9fb6grqrzasgd87ax1yv4x6my1vn61")))

(define-public crate-cw2-0.12.0 (c (n "cw2") (v "0.12.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta3") (k 0)) (d (n "cw-storage-plus") (r "^0.12.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1hfsbqlwkwj8frgyv170d2ihrmh1h73wg0z1hl2476b6rwj2vsz8")))

(define-public crate-cw2-0.12.1 (c (n "cw2") (v "0.12.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta5") (k 0)) (d (n "cw-storage-plus") (r "^0.12.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0v490w28fcl3x3vh3abcvd63jgpb4i8mmlq5gdm3zqwnqc06b2jg")))

(define-public crate-cw2-0.13.0 (c (n "cw2") (v "0.13.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta6") (k 0)) (d (n "cw-storage-plus") (r "^0.13.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0dkjkd2c83j6xk20iyrq111dxylwwd81zv3dk8b4zvy7d6jpmnf1")))

(define-public crate-cw2-0.13.1 (c (n "cw2") (v "0.13.1") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta6") (k 0)) (d (n "cw-storage-plus") (r "^0.13.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "07qqnrsi6x6vnjkrpvs0gix0jcp1qcjxqj6l2pm6nr1v5cnxm1nn")))

(define-public crate-cw2-0.13.2 (c (n "cw2") (v "0.13.2") (d (list (d (n "cosmwasm-std") (r "^1.0.0-beta8") (k 0)) (d (n "cw-storage-plus") (r "^0.13.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0b8jjaf33ydp5biy6vcbcg01pfab924w3c1y8kfp95gjfhaz2gcr")))

(define-public crate-cw2-0.13.3 (c (n "cw2") (v "0.13.3") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (k 0)) (d (n "cw-storage-plus") (r "^0.13.3") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1yivxcraablxf986asbgzfwn8fxsqq9bayc08wr8p7fxw17pdd2v")))

(define-public crate-cw2-0.13.4 (c (n "cw2") (v "0.13.4") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (k 0)) (d (n "cw-storage-plus") (r "^0.13.4") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1hdzcgmwafvm84xnix5cs1gk4bnrzg2ddfrkncvdv43la4wldkq4")))

(define-public crate-cw2-0.9.2 (c (n "cw2") (v "0.9.2") (d (list (d (n "cosmwasm-std") (r "^0.16.0") (k 0)) (d (n "cw-storage-plus") (r "^0.9.2") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0dnndixz0a1awjzaqy7vpvkv3w29d43wzi87w7q5bw26x839yb82")))

(define-public crate-cw2-0.14.0 (c (n "cw2") (v "0.14.0") (d (list (d (n "cosmwasm-std") (r "^1.0.0") (k 0)) (d (n "cw-storage-plus") (r "^0.14.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1mk3hj0d5x9a0i0ch4z409sgilzabck9lxahipyhcdcfmwjc6x5a")))

(define-public crate-cw2-0.15.0 (c (n "cw2") (v "0.15.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (k 0)) (d (n "cw-storage-plus") (r "^0.15.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "0nh645dn6cyg4i6znwfbm2lmygpazrj1ls6nkz5zfyv0515958dh")))

(define-public crate-cw2-0.15.1 (c (n "cw2") (v "0.15.1") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (k 0)) (d (n "cw-storage-plus") (r "^0.15.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1njk4q41cic18c5c5wnfwrf99bqgnqy9clh2hgzsy29flz78xfss")))

(define-public crate-cw2-0.16.0 (c (n "cw2") (v "0.16.0") (d (list (d (n "cosmwasm-schema") (r "^1.1.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (k 0)) (d (n "cw-storage-plus") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (k 0)))) (h "1bc4p083g5z1fcs7xy4bjvazy82af225dl7qsnld5x06p09q2fci")))

(define-public crate-cw2-1.0.0 (c (n "cw2") (v "1.0.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.0") (k 0)) (d (n "cw-storage-plus") (r "^0.16.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (k 0)))) (h "177z6z661qpbcyryhwyfqaq6l9z4llxbll5gpp0ppd20fmsg7g83")))

(define-public crate-cw2-1.0.1 (c (n "cw2") (v "1.0.1") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.0.1") (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.1") (f (quote ("derive"))) (k 0)))) (h "1y32m87q2rsgsn3qy81pxgl0ay8h8xkbrrjkfbzsid7h5kp0rdwg")))

(define-public crate-cw2-1.1.0 (c (n "cw2") (v "1.1.0") (d (list (d (n "cosmwasm-schema") (r "^1.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.1.0") (k 0)) (d (n "cw-storage-plus") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.1") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "024rp57dl957hhg5mxa22g2sbdrigillc2hyr9rl3mjslp3jvb19")))

(define-public crate-cw2-1.1.1 (c (n "cw2") (v "1.1.1") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "15yim0vr1jydm3g5ar5i1iyl271rll8ysqamxz3437plci7x2ccl")))

(define-public crate-cw2-1.1.2 (c (n "cw2") (v "1.1.2") (d (list (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (k 0)) (d (n "cw-storage-plus") (r "^1.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1yn1jd97nnbyir1274p0hwr1rypvhp6g55xvvszc7xdv9yr21hf6")))

(define-public crate-cw2-2.0.0-rc.0 (c (n "cw2") (v "2.0.0-rc.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0-rc.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0-rc.1") (f (quote ("std"))) (k 0)) (d (n "cw-storage-plus") (r "^2.0.0-rc.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1sz6rp6facpm2pj7piw9mga96xpg0x803kylymzy53whvhjpw123")))

(define-public crate-cw2-2.0.0 (c (n "cw2") (v "2.0.0") (d (list (d (n "cosmwasm-schema") (r "^2.0.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^2.0.0") (f (quote ("std"))) (k 0)) (d (n "cw-storage-plus") (r "^2.0.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.15") (d #t) (k 0)) (d (n "semver") (r "^1.0.20") (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "09vdpmk7dpgg1576va5q6v8r0xfhan1ggmar29sw0i7h736m4j5h")))

