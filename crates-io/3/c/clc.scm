(define-module (crates-io #{3}# c clc) #:use-module (crates-io))

(define-public crate-clc-0.1.0 (c (n "clc") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1hwc4rszmbyhi31kzwgn43ynym8xsrzvb7k1p8ghvadn2hn2j6gq")))

(define-public crate-clc-0.1.1 (c (n "clc") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clc-core") (r "^0.1.0") (d #t) (k 0)) (d (n "reedline") (r "^0.3.0") (d #t) (k 0)))) (h "03ljlpjih2sfp455v73x5qibdfpcsp5q6yzazpfwaq7775fs03p6")))

(define-public crate-clc-0.1.2 (c (n "clc") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clc-engine") (r "^0.1.1") (d #t) (k 0)) (d (n "reedline") (r "^0.3.0") (d #t) (k 0)))) (h "15lxafz0a0d0hcph9cj7ab281s2mqg6ja4v5xvfws5l8pqqg0fgp")))

(define-public crate-clc-0.1.3 (c (n "clc") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clc-engine") (r "^0.1.3") (d #t) (k 0)) (d (n "reedline") (r "^0.3.0") (d #t) (k 0)))) (h "06d185r8gcv5kwknpwqbf6d8j9b1k1d8adqhw4yvna2hf6biwc4l")))

(define-public crate-clc-0.1.4 (c (n "clc") (v "0.1.4") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "clc-engine") (r "^0.1.4") (d #t) (k 0)) (d (n "reedline") (r "^0.3.0") (d #t) (k 0)))) (h "0pz0qv4dkrh65nzyllmnxlcyi2g0r2kkwllmzz1ba22l88fwrlg3")))

