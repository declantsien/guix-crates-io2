(define-module (crates-io #{3}# g gml) #:use-module (crates-io))

(define-public crate-gml-1.0.0 (c (n "gml") (v "1.0.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "0bcx1sabd88i0dh2yrn94lq4n8ppbw93i6xlz359iyc0cwlk063r")))

(define-public crate-gml-1.0.1 (c (n "gml") (v "1.0.1") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "13hn4aqn42i3dbdsln379l1jfcy2lb1xk5zx30gwl6rpbzdirj8v")))

(define-public crate-gml-1.1.0 (c (n "gml") (v "1.1.0") (d (list (d (n "num") (r "*") (d #t) (k 0)))) (h "1d2k9a7gvwvx9g6vshzpd4w88hwdsnsbb4yyw05jmvyf3vzffpa5")))

