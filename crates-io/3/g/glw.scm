(define-module (crates-io #{3}# g glw) #:use-module (crates-io))

(define-public crate-glw-0.1.0 (c (n "glw") (v "0.1.0") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glfw") (r "^0.23.0") (d #t) (k 0)))) (h "1ndp2admbxjgq3h4ll6jf5jy04pvk8y94j8mw7vc2hhy173nx4dv")))

(define-public crate-glw-0.1.1 (c (n "glw") (v "0.1.1") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glfw") (r "^0.23.0") (d #t) (k 0)))) (h "00whg3dwkk7zgbbjzl6s802b9wq46x1qmmjzcv88px69fx50nrjs")))

(define-public crate-glw-0.1.2 (c (n "glw") (v "0.1.2") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glfw") (r "^0.23.0") (d #t) (k 0)))) (h "1kbj3d2hxl14a6argd9bfszc22kxbrhi1kcxhrmglqa49ly4ghxq")))

(define-public crate-glw-0.1.4 (c (n "glw") (v "0.1.4") (d (list (d (n "gl") (r "^0.10.0") (d #t) (k 0)) (d (n "glfw") (r "^0.23.0") (d #t) (k 0)))) (h "0dm9qq6jkdxikjpn2p3nkaqdskxp5yqz8f5jl28v72g2lgn5jmv7")))

