(define-module (crates-io #{3}# g gid) #:use-module (crates-io))

(define-public crate-gid-0.1.0 (c (n "gid") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "03lhgp8fdzad5jk3rl7k1vy5ckd38l8iil9h7wrnk5m126wmccf4") (y #t)))

(define-public crate-gid-0.1.1 (c (n "gid") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "06c65mwlnx23ssy9vij5dcyy6fv8d7z26j1znmcd7xwlyr85hmmh")))

