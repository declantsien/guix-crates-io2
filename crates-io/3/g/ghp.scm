(define-module (crates-io #{3}# g ghp) #:use-module (crates-io))

(define-public crate-ghp-0.1.0 (c (n "ghp") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1cm4plqibxdki8wrh02d2nkvk739w910rj9g7s9r3cq81vaziyrv")))

(define-public crate-ghp-0.1.1 (c (n "ghp") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1wkn8flbk5j70jjbli48s3np71h373ncqc89b74qgd1j3awsywwl")))

(define-public crate-ghp-0.1.2 (c (n "ghp") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)))) (h "1w1psdls7is547mwkwys9jw3s6d31l51a522h6yb7j9whw2sb093")))

