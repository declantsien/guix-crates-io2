(define-module (crates-io #{3}# g gff) #:use-module (crates-io))

(define-public crate-gff-0.1.0 (c (n "gff") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.0") (d #t) (k 0)))) (h "1js8h0k87fraf17n183010kn6a2zxhl3vb22w9csk9r21g7rdn22")))

