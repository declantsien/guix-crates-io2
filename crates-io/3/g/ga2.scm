(define-module (crates-io #{3}# g ga2) #:use-module (crates-io))

(define-public crate-ga2-0.1.0 (c (n "ga2") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "token-parser") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vector-space") (r "^0.1.0") (d #t) (k 0)))) (h "1iaqra93aivpbwfl0irkzsrb77rl0fah0wsn97l658hlv2g0vpnb") (f (quote (("parsable" "token-parser"))))))

(define-public crate-ga2-0.2.0 (c (n "ga2") (v "0.2.0") (d (list (d (n "data-stream") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "token-parser") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vector-basis") (r "^0.1") (d #t) (k 0)) (d (n "vector-space") (r "^0.3") (d #t) (k 0)))) (h "1mdlzd1f1lskqg4jfy4nmbfb2s2wfjw294fxx2bgm19qfxvc3kws") (f (quote (("parsable" "token-parser"))))))

(define-public crate-ga2-0.3.0 (c (n "ga2") (v "0.3.0") (d (list (d (n "data-stream") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "token-parser") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vector-basis") (r "^0.1") (d #t) (k 0)) (d (n "vector-space") (r "^0.3") (d #t) (k 0)))) (h "1d3w446pimcqn60dgid3qdb2ihbck54n3f7bq8qrpfgvphgkywii") (f (quote (("parsable" "token-parser"))))))

(define-public crate-ga2-0.3.1 (c (n "ga2") (v "0.3.1") (d (list (d (n "data-stream") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "token-parser") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "vector-basis") (r "^0.1") (d #t) (k 0)) (d (n "vector-space") (r "^0.3") (d #t) (k 0)))) (h "1863vz5nnv0ngwpvq4hkbam32yir2b6jsivx670yjqx0gam5jxgs") (f (quote (("parsable" "token-parser"))))))

