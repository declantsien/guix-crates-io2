(define-module (crates-io #{3}# g ghl) #:use-module (crates-io))

(define-public crate-ghl-0.1.0 (c (n "ghl") (v "0.1.0") (h "1ydsky1gygfqvhnpnhcfav9pqh7aj2s52bj0rw5qyjjczmr3b6c6")))

(define-public crate-ghl-1.0.0 (c (n "ghl") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)))) (h "11f50my22q6fj29nslbvapdgyn8f7lcp13qs1icjafikjzqsdwbc")))

