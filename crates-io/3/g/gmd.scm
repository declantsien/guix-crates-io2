(define-module (crates-io #{3}# g gmd) #:use-module (crates-io))

(define-public crate-gmd-0.1.0 (c (n "gmd") (v "0.1.0") (h "0bmhcyk6yd7bzpbfdlsjlbgywpyab5idmqjkxsv7chpwq531sn1h")))

(define-public crate-gmd-0.1.1 (c (n "gmd") (v "0.1.1") (h "12y683haa08yb0grqllnqcvkahs3iiayzz8ygv92h57cmq1sd79n")))

