(define-module (crates-io #{3}# g gds) #:use-module (crates-io))

(define-public crate-gds-0.1.1 (c (n "gds") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5.2") (d #t) (k 0)))) (h "1629mfi2wij83fszy69zw7kv2mlbksrf4z31zy8pgzmad3qwb5g2")))

(define-public crate-gds-0.1.2 (c (n "gds") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5.2") (d #t) (k 0)))) (h "0azwp38a1ph1ckx3jbrdqrjfyswlr382b6wci2prq38n0h2ip7yw")))

