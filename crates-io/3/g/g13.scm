(define-module (crates-io #{3}# g g13) #:use-module (crates-io))

(define-public crate-g13-0.1.0 (c (n "g13") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "11n5qpjvrxwi6lqxzpj1zzaw4hd8jbqhj600xlxmc4qkknj3vh2v")))

