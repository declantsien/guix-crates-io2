(define-module (crates-io #{3}# g gal) #:use-module (crates-io))

(define-public crate-gal-0.1.0 (c (n "gal") (v "0.1.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "0vvnr3cmwc17llf5hv5qnxiyxcy42c82sy8q9im1ddgg5h5bgnld")))

(define-public crate-gal-0.1.1 (c (n "gal") (v "0.1.1") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "0sajh66z3sax0nfqpv3bq4kb7vnxr6appnig507vbi5l0v3vny6r")))

(define-public crate-gal-0.1.2 (c (n "gal") (v "0.1.2") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1shhchfxjmz2620xv57hsx3s3l330sl6d2cmciz0jcjyf5330z7p")))

(define-public crate-gal-0.2.0 (c (n "gal") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1f2ml6yn8795lc1gdgnl8z4nh231avk4xby7b9xr98zvlpw8d5i0")))

(define-public crate-gal-0.2.1 (c (n "gal") (v "0.2.1") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1hk8ikzh0qab76l9kzmxn7kxixfz9bzl4ax4s2q0bimvjwakamz1")))

(define-public crate-gal-0.2.2 (c (n "gal") (v "0.2.2") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "031zdyiclqpbfz83nnn1qbmi0p4jfg2qin5v8dmh9jd5bzqqjq1m")))

(define-public crate-gal-0.2.3 (c (n "gal") (v "0.2.3") (d (list (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "07vq8pg7i8dhm5ah70jxbc74fki6w7fgrb1sy7v8fsacycdr97jx")))

