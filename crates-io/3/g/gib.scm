(define-module (crates-io #{3}# g gib) #:use-module (crates-io))

(define-public crate-gib-0.1.0 (c (n "gib") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1jdsm8j6510ksamkxb1za06s4cwcmhkjq3cdfyk5hf9j7k3z15vb")))

(define-public crate-gib-0.1.1 (c (n "gib") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1a1p4s54g6ja1nh3g21ip483kkp5p3i9r8071cphpkzk8ydl34c1")))

(define-public crate-gib-0.1.2 (c (n "gib") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0w1ahlngdyp4jl6zjvkgizb6c3k73xsdhrd553nwm8bj47jpzll4")))

(define-public crate-gib-0.2.0 (c (n "gib") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1iw3n8m54p10mkxmxwqs6bdivj72zzx7np22jmyzblhi3yxpy19a")))

(define-public crate-gib-0.2.1 (c (n "gib") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1qfabcnbl09lxfwmwc412p6fdkn3743x3qljvngg7ws056jx98xx")))

(define-public crate-gib-0.2.2 (c (n "gib") (v "0.2.2") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0hrfsln72is9xy8h2bmyaynpgpqhjm4d4djxhwhvc53rwyv404rr")))

(define-public crate-gib-0.2.3 (c (n "gib") (v "0.2.3") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "17bbwlyr9kfj7qplfqld8qql3w77j6hwzd93wqp8vj95s3vjwqnb")))

(define-public crate-gib-0.2.4 (c (n "gib") (v "0.2.4") (d (list (d (n "assert_cmd") (r "^1") (d #t) (k 2)) (d (n "assert_fs") (r "^1") (d #t) (k 2)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1gcyakk18p2klrdskbvp9zzvaz2wnhnwcznnglirvz16famsi6nh")))

