(define-module (crates-io #{3}# g gw2) #:use-module (crates-io))

(define-public crate-gw2-0.1.0 (c (n "gw2") (v "0.1.0") (d (list (d (n "hyper") (r "^0.8.0") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.18") (d #t) (k 0)))) (h "0w2d91pzbsqmalaf3sw2pidz6rqfn95j07sk81w94bcr0bgqzv1n")))

(define-public crate-gw2-0.1.1 (c (n "gw2") (v "0.1.1") (d (list (d (n "hyper") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.7.2") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7.2") (o #t) (d #t) (k 0)) (d (n "syntex") (r "^0.31.0") (d #t) (k 1)))) (h "0sfhrhfy8pxldj0xxgb37g82i8dyq42h1985rhca7kgslzipz8v0") (f (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

