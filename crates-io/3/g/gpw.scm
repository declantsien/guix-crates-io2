(define-module (crates-io #{3}# g gpw) #:use-module (crates-io))

(define-public crate-gpw-0.1.0 (c (n "gpw") (v "0.1.0") (d (list (d (n "ndarray") (r "~0.12") (f (quote ("serde-1"))) (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "~1") (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)))) (h "0kjnx33111phi4zs4rwa2wa29milwzfsd5v1k4c9jr5g5sf2iw65")))

