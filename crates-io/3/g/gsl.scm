(define-module (crates-io #{3}# g gsl) #:use-module (crates-io))

(define-public crate-GSL-0.4.10 (c (n "GSL") (v "0.4.10") (h "1gy4aras7h9d4565vl7j03x2qzbhxx5fnxdhi0znkd05ccz0a9w7")))

(define-public crate-GSL-0.4.11 (c (n "GSL") (v "0.4.11") (h "05s1pzsa8x68r6i5k6nzp5fx5a2q3ck57zw4r05xi72hgfa6xchg")))

(define-public crate-GSL-0.4.12 (c (n "GSL") (v "0.4.12") (h "1sj73lq6y1lpbkfgy0ix524r1c2vllfgs251n95n5xpjdmi7vscv")))

(define-public crate-GSL-0.4.13 (c (n "GSL") (v "0.4.13") (h "105z22i87vl2lyrbayik7gz7fx9wf6k4jhqmdkp9h51r44d55yz4")))

(define-public crate-GSL-0.4.15 (c (n "GSL") (v "0.4.15") (d (list (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "0a05fqip4jd2ix123x98g16yxcvb5jkhgw2x17ar30bn7gddrp4l")))

(define-public crate-GSL-0.4.16 (c (n "GSL") (v "0.4.16") (d (list (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "0bx09sp28mqdb6v7waa56gk1q8a60jqd67is788nwmr4cb3y6lg8")))

(define-public crate-GSL-0.4.17 (c (n "GSL") (v "0.4.17") (d (list (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "1nvixp416qsdvd86hngf66iyk8sgrdagybnbyzn0ix5hywzfakwd")))

(define-public crate-GSL-0.4.18 (c (n "GSL") (v "0.4.18") (d (list (d (n "c_str") (r "^1.0.0") (d #t) (k 0)) (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "0clr03f98naqna4mcy9mm84wdv33hgl8fpimr5zazkrb11dnp3sa")))

(define-public crate-GSL-0.4.19 (c (n "GSL") (v "0.4.19") (d (list (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "1ijmp66a68q1jr91fbf1msz7xazm2iwxqcy1xh2kmjmag3h0y78f")))

(define-public crate-GSL-0.4.20 (c (n "GSL") (v "0.4.20") (d (list (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)))) (h "1jrjis4xbnyxrs0pi0lbymlvahw9dqxvzfacyzgbq4wzagxd7w7j")))

(define-public crate-GSL-0.4.21 (c (n "GSL") (v "0.4.21") (d (list (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0kr3abspwsvp54yksxp5wfrp47sw78b40p0gca3dyb5m6c8jb4z7")))

(define-public crate-GSL-0.4.22 (c (n "GSL") (v "0.4.22") (d (list (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "159ad8ivnv9liqnj1lv04ppmwrqxjn5gv16jsw3l0bd97yf6wr9y")))

(define-public crate-GSL-0.4.23 (c (n "GSL") (v "0.4.23") (d (list (d (n "c_vec") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1b48xb79xj1sn4np6y1d0qkqcvzni7169spsmckwb534vhc3maqw")))

(define-public crate-GSL-0.4.24 (c (n "GSL") (v "0.4.24") (d (list (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1qri4d4zrzfqardr6y4nncnaca1f9js49s8sfjycwfyxvclfx27k")))

(define-public crate-GSL-0.4.25 (c (n "GSL") (v "0.4.25") (d (list (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "0ma03i5s1gaichdx08aaga196y2nwgd17iw0glvg8jlni09haph8")))

(define-public crate-GSL-0.4.26 (c (n "GSL") (v "0.4.26") (d (list (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "num") (r "~0.1") (d #t) (k 0)))) (h "1pfkzmvhl8jkk0q1v4nvslsymn9sq485j5j59n7ycgwi82ckw5gh")))

(define-public crate-GSL-0.4.27 (c (n "GSL") (v "0.4.27") (d (list (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1bn3p1w2d9vyx096l311xhjiv5ag925qzd5ann130wdkhppxhnkf")))

(define-public crate-GSL-0.4.28 (c (n "GSL") (v "0.4.28") (d (list (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "09wqhqfmzgamx8r878fc35bx00zvka1cdrf0bfl2ng8amnj7hhw7")))

(define-public crate-GSL-0.4.29 (c (n "GSL") (v "0.4.29") (d (list (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0inpkfv4f696frjqkm60pwwfpwgp6dbs3wfj3bbs4lar32r8f0rn") (f (quote (("v2"))))))

(define-public crate-GSL-0.4.30 (c (n "GSL") (v "0.4.30") (d (list (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1bsg5z5xlniz5bgf5ij2q1ka0nhbjh3w6dixxzhigzjijippb2ly") (f (quote (("v2"))))))

(define-public crate-GSL-0.4.31 (c (n "GSL") (v "0.4.31") (d (list (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0farqlx84rxawibcivq77azfhgyqmfpda703f4dbhp03scvkb39q") (f (quote (("v2"))))))

(define-public crate-GSL-1.0.0 (c (n "GSL") (v "1.0.0") (d (list (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0p6qjydmzx9mb4gl4a7z4z35asxdaydqd5dblv6pcy3m65qicw1f") (f (quote (("v2"))))))

(define-public crate-GSL-1.1.0 (c (n "GSL") (v "1.1.0") (d (list (d (n "c_vec") (r "~1.0") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lssybk2psjpk77nrv6hrgvjp2cvc05i9gq1izfbpg49ldp1ac3q") (f (quote (("v2"))))))

(define-public crate-GSL-2.0.0 (c (n "GSL") (v "2.0.0") (d (list (d (n "GSL-sys") (r "^2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0vjbakh4sv5vaghi6bzhs1a4p627415wg6wqvvyqqvm0k12avlly") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-2.0.1 (c (n "GSL") (v "2.0.1") (d (list (d (n "GSL-sys") (r "^2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1c09khrbmhqg7qbkqi6sq3587fnvir5r64yd93n05ig8xfkp4g3j") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-3.0.0 (c (n "GSL") (v "3.0.0") (d (list (d (n "GSL-sys") (r "^2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0canrdbs7mhl9krj8ibighcd5gqlqk7w7mkifp3qh1b98pjgwbx8") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-4.0.0 (c (n "GSL") (v "4.0.0") (d (list (d (n "GSL-sys") (r "^2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "143anzmr4k5m2nxksiviqczaqqdl1yhf32k4fijzrslcy0xsca9g") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-4.0.1 (c (n "GSL") (v "4.0.1") (d (list (d (n "GSL-sys") (r "^2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0k4jhzk0hnz0yafhpycmjy2g14jvh94fgh2mf9z66h9vwfrgb055") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-4.0.2 (c (n "GSL") (v "4.0.2") (d (list (d (n "GSL-sys") (r "^2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0qdfwkfc19b6s3iiyc5nr3hx1fihvy47xbzhi668ks5d5yp9464n") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-4.0.3 (c (n "GSL") (v "4.0.3") (d (list (d (n "GSL-sys") (r "^2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "19y25pf8ia825mf8cg8v06h4cni9y5g7vdh3qh8x5i38423asn2b") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-4.0.4 (c (n "GSL") (v "4.0.4") (d (list (d (n "GSL-sys") (r "^2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0z4zh17l6fwfn56cman7n6b555xq4m4lr03ykl0g0hrc0a64i41w") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-5.0.0 (c (n "GSL") (v "5.0.0") (d (list (d (n "GSL-sys") (r "^2.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1b7h32ilvbplznbnhvfn4v1pxnvzj6irlmpv1jivkfz6j8gvvj4v") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-6.0.0 (c (n "GSL") (v "6.0.0") (d (list (d (n "GSL-sys") (r "^3.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1kpiivagrsk9ags7d4k2521jwix0lqgnd3i8ayj3dfniszvcmgn9") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-7.0.0-rc1 (c (n "GSL") (v "7.0.0-rc1") (d (list (d (n "GSL-sys") (r "^3.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "02q5q6h5a75rxm2v4pyicivp8dj2spc9zh39fgkqfi9nf5j6k8hg") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

(define-public crate-GSL-7.0.0 (c (n "GSL") (v "7.0.0") (d (list (d (n "GSL-sys") (r "^3.0.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "01xkv5pr5asfxf5bnpdjzz4wg2dwl7kbwjhjj7llcp2vl7al6ffv") (f (quote (("v2_7" "GSL-sys/v2_7" "v2_6") ("v2_6" "GSL-sys/v2_6" "v2_5") ("v2_5" "GSL-sys/v2_5" "v2_4") ("v2_4" "GSL-sys/v2_4" "v2_3") ("v2_3" "GSL-sys/v2_3" "v2_2") ("v2_2" "GSL-sys/v2_2" "v2_1") ("v2_1" "GSL-sys/v2_1") ("dox" "v2_7" "GSL-sys/dox"))))))

