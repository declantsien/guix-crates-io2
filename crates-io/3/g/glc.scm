(define-module (crates-io #{3}# g glc) #:use-module (crates-io))

(define-public crate-glc-0.1.0 (c (n "glc") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0m3z46gxk2hx76xzkfa7nb43j23kmr3i408nhk1rwzzpvx82bkgr")))

(define-public crate-glc-0.2.0 (c (n "glc") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "15asy5g0bmb5gz18d8pbn62b77i4y6jb36sj5wg8c2bxkr0nv1hz")))

(define-public crate-glc-0.3.0 (c (n "glc") (v "0.3.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19sypk45b53yd2x0vfnlx97ff889yafs2wj62hy26g6c2l8ch02p")))

(define-public crate-glc-0.3.1 (c (n "glc") (v "0.3.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "18rxqkk6vjl7758fafylmy7jz6v3xnwpbnwsnrwmxshshysqxq87")))

(define-public crate-glc-0.3.2 (c (n "glc") (v "0.3.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1h9gd5pnwy96izd6ivjqjy11il7pi9b89r4pd2ychzji85lzbvsq")))

(define-public crate-glc-0.4.0 (c (n "glc") (v "0.4.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "00bzw3j8sz1nb78xlayra1bv1vv94risngq6gjh1gpd6yyvr6f1m")))

(define-public crate-glc-0.4.1 (c (n "glc") (v "0.4.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1skgz8y8a07s9pmdfpplnv13pqi198q2s1xpp9i4v1g3m0x32hjn") (y #t)))

(define-public crate-glc-0.4.2 (c (n "glc") (v "0.4.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0y46kzp8v0jg9prpnq8s4fw2g7c122j9zx6x3zlvfsvc9r6j16ak")))

