(define-module (crates-io #{3}# g gou) #:use-module (crates-io))

(define-public crate-gou-0.1.0 (c (n "gou") (v "0.1.0") (h "1v8iyjg8s3i6sk35byp7q7czznaflcg9im9csci7hqfkj5lqsy0k")))

(define-public crate-gou-0.1.1 (c (n "gou") (v "0.1.1") (h "1yc60hrpwv00637ivbacm5jbyhmd7jqr4g49n54b4gjcq20yd5g7")))

(define-public crate-gou-0.1.2 (c (n "gou") (v "0.1.2") (h "128jskvz0m4qlf4cawk3xmvi8gm9nivv1yvxzawmfyxvq0fj3dyr")))

(define-public crate-gou-0.1.3 (c (n "gou") (v "0.1.3") (h "1433ddjq8p9vpyl9nsjk76k35yv5jgr6qi5aqsqfgk549gzk66d9")))

(define-public crate-gou-0.1.4 (c (n "gou") (v "0.1.4") (h "1k62dhypsm6hc910wvyns1rl7lp3fxfg73d18wz4ryda8f2p7ga2")))

