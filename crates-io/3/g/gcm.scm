(define-module (crates-io #{3}# g gcm) #:use-module (crates-io))

(define-public crate-gcm-0.1.0 (c (n "gcm") (v "0.1.0") (d (list (d (n "curl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0bb1c7l63fcmdyycs6bhp81n1l6hkwcix1fmkl9db9j6pgl8873b")))

(define-public crate-gcm-0.1.1 (c (n "gcm") (v "0.1.1") (d (list (d (n "curl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0f2ywsx2wcp97znr5q816dlkhynykpgv3w55q9alsx4phlfkcvpg")))

(define-public crate-gcm-0.1.2 (c (n "gcm") (v "0.1.2") (d (list (d (n "curl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0bygx79691flc6pd5896clkf7lr28kmx7334jmhif83qrzpz6x8w")))

(define-public crate-gcm-0.1.3 (c (n "gcm") (v "0.1.3") (d (list (d (n "curl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0jwff0kkqm2i5i6gmbb8avg6xwl5gw68ncgri75bj5bblxx1vbf5")))

(define-public crate-gcm-0.1.4 (c (n "gcm") (v "0.1.4") (d (list (d (n "curl") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "0czgrflhq962v3z4fmdllrdd6mk2j0jk6g5ngv1l1gv0fm389r3m")))

(define-public crate-gcm-0.2.0 (c (n "gcm") (v "0.2.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "05wgka9x735nh7zyp34w7vsnzkjq8jhdwph5sqs8rm2v0346wrz9")))

