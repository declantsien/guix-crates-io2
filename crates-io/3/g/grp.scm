(define-module (crates-io #{3}# g grp) #:use-module (crates-io))

(define-public crate-grp-0.1.0 (c (n "grp") (v "0.1.0") (d (list (d (n "grp_api") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1hiab5yk1r5mrblajvjngrm739y462jhc7ywh129zkj5v2x30237")))

(define-public crate-grp-0.1.1 (c (n "grp") (v "0.1.1") (d (list (d (n "grp_api") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0359rph0lpm7dxm6nw2yj43lsh0fph34ryikw538pwpd2zy26n54")))

