(define-module (crates-io #{3}# g gee) #:use-module (crates-io))

(define-public crate-gee-0.1.0 (c (n "gee") (v "0.1.0") (d (list (d (n "euclid") (r "^0.19.5") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)))) (h "1mnv3bw6csv6pbdy69bz039by9gf9na515js5qp276binak7gvp2")))

(define-public crate-gee-0.2.0 (c (n "gee") (v "0.2.0") (d (list (d (n "en") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.19.2") (d #t) (k 0)) (d (n "strum_macros") (r "^0.19.2") (d #t) (k 0)))) (h "1wg5hkb67kw1ddvsivc41fckiy48xa9n3k2fgvskmf7s36839492") (f (quote (("default"))))))

(define-public crate-gee-0.3.0 (c (n "gee") (v "0.3.0") (d (list (d (n "en") (r "^0.1.5") (d #t) (k 0)) (d (n "euclid") (r "^0.22.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.1") (d #t) (k 0)))) (h "1jaymqndi8daiyvqyyrfw5f8qxnpiz6s8xw49k3fhf4qamhglpqj") (f (quote (("default"))))))

