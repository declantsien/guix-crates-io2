(define-module (crates-io #{3}# g gai) #:use-module (crates-io))

(define-public crate-gai-0.1.0 (c (n "gai") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1jf7as1622apb330jlj075v14ayrghfzbs7fxhd9yj23lmdkd79r")))

(define-public crate-gai-0.1.1 (c (n "gai") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02vlhiav7pryb23xkc5m0ij92wrql3h73hara3bgcm0z5ggg4f1b")))

(define-public crate-gai-0.1.2 (c (n "gai") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qfc94ymfzp66rxg2cyqgrpq0l9mpxpi0im5cnc2x048r5w7q67a")))

(define-public crate-gai-0.1.3 (c (n "gai") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "19ikf8qv6yjgqy1lvx9sh0yn2wjddq0ycglw626f1fa67yv64qm6")))

(define-public crate-gai-0.1.4 (c (n "gai") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "158k4mah7khymhg504rpyichv1y1xh5ml2gib555g8shw9dh104b")))

