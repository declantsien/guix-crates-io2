(define-module (crates-io #{3}# g gcc) #:use-module (crates-io))

(define-public crate-gcc-0.0.1 (c (n "gcc") (v "0.0.1") (h "0456xi09r2ybq6v7q4sjf0pgsp1flr42n9ss8xhp8l8rgaz4m4iy")))

(define-public crate-gcc-0.0.2 (c (n "gcc") (v "0.0.2") (h "0mfjnih68cpx8af3hhbyqx9ax16dxj3w8cgp2x2llfbb2r9jfp18")))

(define-public crate-gcc-0.1.0 (c (n "gcc") (v "0.1.0") (h "0imssva8sf34fzdakdkqkgif6ww5ci55rwi80knh46f6q9200r0k")))

(define-public crate-gcc-0.1.1 (c (n "gcc") (v "0.1.1") (h "15n6x5byz3q9w8zw77lf13mn819r1kjnp4y8zg567mfp0n3iybfh")))

(define-public crate-gcc-0.1.2 (c (n "gcc") (v "0.1.2") (h "11671na4pnpx0c7rf7sw5y48c1pkcx17r4fjyz5rn0q8xvnnxcmk")))

(define-public crate-gcc-0.1.3 (c (n "gcc") (v "0.1.3") (h "1w4bp0d2l6ci2rylxycr2c68wmzlnjlrwxvcycjhirf4qmkhkmnz")))

(define-public crate-gcc-0.1.4 (c (n "gcc") (v "0.1.4") (h "12572wciy54dpc8lygpcwcwgadb2hvyr4gbbqmpbgxsbjjqdzfs0")))

(define-public crate-gcc-0.1.5 (c (n "gcc") (v "0.1.5") (h "0ky807yf7gsa7n31faslvm09kfmrivb6j4g9j628kqyfbbsq6j29")))

(define-public crate-gcc-0.1.6 (c (n "gcc") (v "0.1.6") (h "0lixp61n5b3d1bv4c8gy4cfgbpshkkq4szivapj7hiwi862d34a9")))

(define-public crate-gcc-0.1.7 (c (n "gcc") (v "0.1.7") (h "0wph9cdpv42xd2gbb6rqhyi01zxrrp81ijwasfw0hfqrla00pyh8")))

(define-public crate-gcc-0.2.0 (c (n "gcc") (v "0.2.0") (h "10y54jwdcmply46s7dxhsvr3zi5k3cc46j0ji3w0d7zj7mbad7r7")))

(define-public crate-gcc-0.2.1 (c (n "gcc") (v "0.2.1") (h "1hz4my3c27aqhwkp5q6ybpqh0393g5q2v5iplxm9kmw9ypi38j0k")))

(define-public crate-gcc-0.3.0 (c (n "gcc") (v "0.3.0") (h "1xlkifwxyg57iyb83rmjbs3yq1a50g1kff4z3l09hz61l4zfjyxp")))

(define-public crate-gcc-0.3.1 (c (n "gcc") (v "0.3.1") (h "1h0yknhyr0c9y39vd8z0ng3b0l517w6s4jha4n8nny54fkvp7bxr")))

(define-public crate-gcc-0.3.2 (c (n "gcc") (v "0.3.2") (h "1jmfqcyia1qa46l15kzlp9wid23608ndxbbaf7j6cjxlpxx59apj")))

(define-public crate-gcc-0.3.3 (c (n "gcc") (v "0.3.3") (h "15ibrjb40aaydr9qskpj24npc5p0ivbg8kg3y0ys7x5yz5gz8cqf")))

(define-public crate-gcc-0.3.4 (c (n "gcc") (v "0.3.4") (h "1q97lpi48qvdk8cqikig2m4bqbb4wdy6a9f48gkvcdvgxydxxmfg")))

(define-public crate-gcc-0.3.5 (c (n "gcc") (v "0.3.5") (h "12a5big0agvf8w2v2zdb4yw224m2clng2ff64vpb6frsp7r108hc")))

(define-public crate-gcc-0.3.6 (c (n "gcc") (v "0.3.6") (h "08r7ndh4ayz0l8j2hzdn6n8dams61rfkbqard9k53hgj7jp6czzj")))

(define-public crate-gcc-0.3.8 (c (n "gcc") (v "0.3.8") (h "132ccrhqbk1y7bf9lwajaz1fbaz84n4s4q3a7hjwz45b3647f4x5")))

(define-public crate-gcc-0.3.9 (c (n "gcc") (v "0.3.9") (h "0vl2g0mb7hm9kafj3caa57ckdl9acjai7lnrc3jjkzqd4yj3fcxg")))

(define-public crate-gcc-0.3.10 (c (n "gcc") (v "0.3.10") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "0hazrvny472m0ii1amx0k2ds600flq7jy733nkz3kn1lniaqinlg")))

(define-public crate-gcc-0.3.11 (c (n "gcc") (v "0.3.11") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "1nzgddh209hxllablwc91xlmydczpyjxyx6q2cy772mv90rxv270")))

(define-public crate-gcc-0.3.12 (c (n "gcc") (v "0.3.12") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "1yjfxp1bl83hiy69hvac2ni1k5dvzdfizng1s8dr74ywnd5w7g0q")))

(define-public crate-gcc-0.3.13 (c (n "gcc") (v "0.3.13") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "0y9572ld2fxycma2y3df8sqhj64b8xrypxb23zkjiybgk8z7a5y8")))

(define-public crate-gcc-0.3.14 (c (n "gcc") (v "0.3.14") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "07ddycs9fsq23w6sh62cpam4cxy62w75xsbm1rc2w31gv7dx3nkq")))

(define-public crate-gcc-0.3.15 (c (n "gcc") (v "0.3.15") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "0iab5yzv3l67rqyyij4dxi5jacrk4jwijs2njr8zpliixwp27v2y")))

(define-public crate-gcc-0.3.16 (c (n "gcc") (v "0.3.16") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "0m0rfqgp07a81gb4qcmg0i7lv5b375inbd25q232fkg78p3ns00h")))

(define-public crate-gcc-0.3.17 (c (n "gcc") (v "0.3.17") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "1majxq4rdj67cgm0vi86l5lafc75mz3j5ccphc4jmj2zqmr0bab3")))

(define-public crate-gcc-0.3.18 (c (n "gcc") (v "0.3.18") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "02md2zfiyjr1lwq0l8vhkkllygi5m9gb4jf35bd5kz0j9m8574r3")))

(define-public crate-gcc-0.3.19 (c (n "gcc") (v "0.3.19") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "0pax1dgppmj0zxdl64xpf701z4w3n82cxkkgvxgqy5j75k1hjivw")))

(define-public crate-gcc-0.3.20 (c (n "gcc") (v "0.3.20") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "0r3yh3k1ssfwb32hmphakwfrcyk8wdvq919krvha60azkx9gz2cz")))

(define-public crate-gcc-0.3.21 (c (n "gcc") (v "0.3.21") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "0mnk8yc1vnzfaahcxyyma08qd5s3gk0lvf8rv93q09y8y7hy646a")))

(define-public crate-gcc-0.3.22 (c (n "gcc") (v "0.3.22") (d (list (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "advapi32-sys") (r "^0.1.2") (d #t) (t "x86_64-pc-windows-msvc") (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "i686-pc-windows-msvc") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-gnu") (k 0)) (d (n "winapi") (r "^0.2.1") (d #t) (t "x86_64-pc-windows-msvc") (k 0)))) (h "088bk3wgnvqbi8xvfvaj8hp7a9rxnfxak2qhg02f9amclr9in2yi")))

(define-public crate-gcc-0.3.23 (c (n "gcc") (v "0.3.23") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0j7nnz9qwhw70110gcbq907hl6hbg2vva5r26b1z551hsmby1r83")))

(define-public crate-gcc-0.3.24 (c (n "gcc") (v "0.3.24") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0k5bz6vg8awmnskjy0s4a5bhylb19bw2vc0b6w94d85ji8xrfqzy")))

(define-public crate-gcc-0.3.25 (c (n "gcc") (v "0.3.25") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "19m2hmac0pjcchzc2va4j2wgz8ql80wnq9lqyjdmznymn1qsj6vs")))

(define-public crate-gcc-0.3.26 (c (n "gcc") (v "0.3.26") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0gjx68naqxk9sn41i6q8p1wqfhsvixq5jax6671k7pf8xr6vydrk")))

(define-public crate-gcc-0.3.27 (c (n "gcc") (v "0.3.27") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1vfmr4nc3ik70f3yi0yiav9inzplx78w4pqa0q5pcc5z3w966vl0")))

(define-public crate-gcc-0.3.28 (c (n "gcc") (v "0.3.28") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0yfw41yva3q4xz0x9ar7dxyb3kmj1vyxjkvhwg4664xhmv5s58rx")))

(define-public crate-gcc-0.3.29 (c (n "gcc") (v "0.3.29") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "19vfnd0mq7n63fm8vqc3nrsw4fzlx4pc7bgvz4d976pw9w9s25hk")))

(define-public crate-gcc-0.3.30 (c (n "gcc") (v "0.3.30") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0p92m2kdz3dmjf02i0q8c6sjcyv9srwgzrzmrqx9dll2d8y217hj")))

(define-public crate-gcc-0.3.31 (c (n "gcc") (v "0.3.31") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "01wv0gjqslayqsrm86r902aqs69yyh5mscp71jxhwsakdr3pgs6g")))

(define-public crate-gcc-0.3.32 (c (n "gcc") (v "0.3.32") (d (list (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "18k1bzhyqymankrvnmkbwvbw2mmygqlr0lgp6z3g97fzssmh1c6w")))

(define-public crate-gcc-0.3.33 (c (n "gcc") (v "0.3.33") (d (list (d (n "rayon") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "045kijgm9m4m1sri4prfnjgxlz79zskpks4lq0hzdg9sqg3qlgh6") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.34 (c (n "gcc") (v "0.3.34") (d (list (d (n "rayon") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0x762zyxkn5c720gqr66yzalxbx66shbpy2kf1wcb3flbg7zsb7n") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.35 (c (n "gcc") (v "0.3.35") (d (list (d (n "rayon") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "04ikxhb6aaz9v2bh5izim9waaxllx0vhp5fniyb0ryzgf4vx1v4i") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.36 (c (n "gcc") (v "0.3.36") (d (list (d (n "rayon") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "07jbwvn1fmy81liqbn57m6nb3j03za7hq9iz1vda7nmrw84xm93v") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.37 (c (n "gcc") (v "0.3.37") (d (list (d (n "rayon") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1n5mrnhgkl6wa071galwz4scy6zn81sbylpjnhvdzfhfr2fpwcs1") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.38 (c (n "gcc") (v "0.3.38") (d (list (d (n "rayon") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1xdw674bkgl6p2afa91rmwx0z9qx1y169ck6yddpbznykd1i2gsm") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.39 (c (n "gcc") (v "0.3.39") (d (list (d (n "rayon") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1q0idjvmhp6shkb9hqabh51rgfr8dqpi1xfmyzq7q8vgzybll7kp") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.40 (c (n "gcc") (v "0.3.40") (d (list (d (n "rayon") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0l6rnf3b7vaxhcs7yg9nyv7brqidy2g0xhg8z0a2pvw6jkjvjbc7") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.41 (c (n "gcc") (v "0.3.41") (d (list (d (n "rayon") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0vmbfya11rhfnv1krz2ck3yvhav3m9c4ffmfc14zffjn5acf329n") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.42 (c (n "gcc") (v "0.3.42") (d (list (d (n "rayon") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1c5k3p6qblpi3hgbwimvhv9i75263i8cj9lhr623vjjriz3ma419") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.43 (c (n "gcc") (v "0.3.43") (d (list (d (n "rayon") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0pd7k0j6rz0rq2l9sfgk3v60fmqj77dnls32ll1yfs13jy5paz60") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.44 (c (n "gcc") (v "0.3.44") (d (list (d (n "rayon") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0fgl8a0j946mlf1rm62hjj58rcib9chb7p23cfvilqfpf00d8b53") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.45 (c (n "gcc") (v "0.3.45") (d (list (d (n "rayon") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1bny15zimjkqrkz64qmpdwkcgn2lzixfhlqgf5w0rnshzcv972a0") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.46 (c (n "gcc") (v "0.3.46") (d (list (d (n "rayon") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "13wla1w0l9ibfj6xw07b4zh9f1g1hykjs3mr5vcknrhxpbmkq7hq") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.47 (c (n "gcc") (v "0.3.47") (d (list (d (n "rayon") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0c5xj3xnqwf87wc1d226ahgqpwn74pjfzn2gip1kni97z0nkfwsp") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.48 (c (n "gcc") (v "0.3.48") (d (list (d (n "rayon") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0igxsllsm2r5ggf5crrlf4ylpm65j6v0s4x3yrp6yfg7l5av4w9r") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.49 (c (n "gcc") (v "0.3.49") (d (list (d (n "rayon") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1fb5bc0h1n85m0k4bydax05ib0phpad1mfjp55qq29hj9h331rwv") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.50 (c (n "gcc") (v "0.3.50") (d (list (d (n "rayon") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1rpr46baijm5li7dfdvi3f38r0nzaf31ib3fayqir9if5wwpr0sz") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.51 (c (n "gcc") (v "0.3.51") (d (list (d (n "rayon") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "02jzz02svnkl503dy4y5liin9zk65d9669a2b4lgghyw0br0f38j") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.52 (c (n "gcc") (v "0.3.52") (d (list (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "15ygclmwyhyp055cf0nisszz4pfdd5z0f8r7schns4q865l1jz8v") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.53 (c (n "gcc") (v "0.3.53") (d (list (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0npwkzx7b9vc9148zjkznb91i6af8x7iqc0fx2q9h0w9kiz0ycg8") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.54 (c (n "gcc") (v "0.3.54") (d (list (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1fxi12ysghnbcq8wxiwh8sbf9gi8zjypsn8ka212gld01llyqcsy") (f (quote (("parallel" "rayon"))))))

(define-public crate-gcc-0.3.55 (c (n "gcc") (v "0.3.55") (d (list (d (n "rayon") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1hng1sajn4r67hndvhjysswz8niayjwvcj42zphpxzhbz89kjpwg") (f (quote (("parallel" "rayon"))))))

