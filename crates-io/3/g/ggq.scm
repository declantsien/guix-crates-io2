(define-module (crates-io #{3}# g ggq) #:use-module (crates-io))

(define-public crate-ggq-0.1.0 (c (n "ggq") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0n20hi7nb8hhmg3xxn2cxc4b69sbmd2n55rh9qsj0n54a0ny3klj")))

(define-public crate-ggq-0.1.1 (c (n "ggq") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)))) (h "0argg4rwlnq3yb5zkln0y3dkfa8mq98625p0ry180dwfzaczd6hs")))

