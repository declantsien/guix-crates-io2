(define-module (crates-io #{3}# g gac) #:use-module (crates-io))

(define-public crate-gac-0.1.0 (c (n "gac") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("cargo" "derive"))) (d #t) (k 0)))) (h "1fp4rsv59qg8jwi0kif6r7zzv3k0whmiscp4abrypcrwk1ailpkb")))

(define-public crate-gac-0.1.1 (c (n "gac") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("cargo" "derive"))) (d #t) (k 0)))) (h "1d6ylvnakswdy3g2v205qjkijqv0acg5m5lpxq33sr4kpwcghicy")))

(define-public crate-gac-0.1.2 (c (n "gac") (v "0.1.2") (h "0h3ji00y2732cplpdi9cqgj6ilpi0qw9p9dbcka39va51z8k8a7k")))

