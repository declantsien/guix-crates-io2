(define-module (crates-io #{3}# g guy) #:use-module (crates-io))

(define-public crate-guy-1.0.0 (c (n "guy") (v "1.0.0") (h "0gg6mrlhbglj4zxjivqrml0jzxgdsfg48mb2z29npmqvrsbxgh4a")))

(define-public crate-guy-1.0.1 (c (n "guy") (v "1.0.1") (h "0aj322s2qj538hl5qbmyzi711n0x33ay33rlz9x9wvgjvrsx9b8c")))

