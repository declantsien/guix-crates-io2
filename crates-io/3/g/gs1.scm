(define-module (crates-io #{3}# g gs1) #:use-module (crates-io))

(define-public crate-gs1-0.1.0 (c (n "gs1") (v "0.1.0") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "050wvcwgk3q0qw5n8ix7hc8rxibq4g7h8ms23sv2fp3f232sip91")))

(define-public crate-gs1-0.1.1 (c (n "gs1") (v "0.1.1") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1sz6wq5k29a8sbih7h6v4np283a8qknanw3ry6gjic8d0fb9jyv6")))

(define-public crate-gs1-0.1.2 (c (n "gs1") (v "0.1.2") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "067p3qnjjbd79h89b61s1jh3y73m7fk3xvv5ydkv67xgzp3jxppw")))

(define-public crate-gs1-0.1.3 (c (n "gs1") (v "0.1.3") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1slfkrh9apnd90b2swv7r4bwscycj09i9fknszx02c3wzxfnwhl0")))

(define-public crate-gs1-0.1.4 (c (n "gs1") (v "0.1.4") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "12kszy4x8nn2lmwz8jnzmbaj4dl8048rbiqjcrfvq8r26ms3yjp4")))

(define-public crate-gs1-0.1.5 (c (n "gs1") (v "0.1.5") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1z8g7v6700sn2d6r40rkkzx7n4pcqnjl2crd0z8zbdzlhphjaq0z")))

(define-public crate-gs1-0.1.6 (c (n "gs1") (v "0.1.6") (d (list (d (n "bitreader") (r "^0.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)) (d (n "pad") (r "^0.1.5") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "1d0w8vg4ky8y72dh7iiy65fczc22jhimpf5yv6av9pcnif1l1rzm")))

