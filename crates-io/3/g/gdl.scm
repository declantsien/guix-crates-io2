(define-module (crates-io #{3}# g gdl) #:use-module (crates-io))

(define-public crate-gdl-0.1.0 (c (n "gdl") (v "0.1.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "13306pwxzxhb1k7iva0iqrq1m4rv2544fjybjw3alxcyavmkk4q7")))

(define-public crate-gdl-0.2.0 (c (n "gdl") (v "0.2.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0krzs6cc7cs6b2n0swaira1dkvvxmavhifav8v1yf3rvyzb5i8ng")))

(define-public crate-gdl-0.2.1 (c (n "gdl") (v "0.2.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1xwd7rr10w7qq6bml5kd6xzz59s2hxqkqbqqaaq44d28d9qbvsfi")))

(define-public crate-gdl-0.2.2 (c (n "gdl") (v "0.2.2") (d (list (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "196zm57wl58xfr796j0jiiwqqbda09xp8hral1kwhq63hb9b4da4")))

(define-public crate-gdl-0.2.3 (c (n "gdl") (v "0.2.3") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "15zvlb4a5d474ywchlgs94rrv9bg80s4waxqc8gf0q01bwfnwpnl")))

(define-public crate-gdl-0.2.4 (c (n "gdl") (v "0.2.4") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1yih6bks6qhb70n85536745sdjjw6gwc73p52xbl8s6pvb2zpbzp")))

(define-public crate-gdl-0.2.5 (c (n "gdl") (v "0.2.5") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1dlx7xj9qrbl9naqhgfss0y2yn2jh24ww2z1sk0k4d47rdp6b432")))

(define-public crate-gdl-0.2.6 (c (n "gdl") (v "0.2.6") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.1") (d #t) (k 2)) (d (n "test-case") (r "^1.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1zpqgz8g138db176a5rgsczl0rck5zkq6advgz1zgdw5dqm7vi0l")))

(define-public crate-gdl-0.2.7 (c (n "gdl") (v "0.2.7") (d (list (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3") (d #t) (k 2)) (d (n "test-case") (r "^2.2") (d #t) (k 2)))) (h "19a3nxiz66lvc3j8yg5bqaj4f5im3rgm3r9qcc41v7hidv3kj2q6")))

