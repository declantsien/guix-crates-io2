(define-module (crates-io #{3}# g gbr) #:use-module (crates-io))

(define-public crate-gbr-0.1.0 (c (n "gbr") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.32.1") (d #t) (k 0)))) (h "156jafxmf488r4jkrl3gdlzplmzc2dnyv42wb2cy0cjrl056liiq")))

(define-public crate-gbr-0.1.1 (c (n "gbr") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "sdl2") (r "^0.32.1") (d #t) (k 0)))) (h "1hv0k1x9zjrpcbwlvr24sh4by3k5yi63alcwwa5dbj4hb5jljzh5")))

