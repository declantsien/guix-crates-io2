(define-module (crates-io #{3}# g god) #:use-module (crates-io))

(define-public crate-god-0.0.1 (c (n "god") (v "0.0.1") (h "17zlz5wq9vsh63b0xz3p385x5jj00xqss4hvvlv9v457yd91gjkd")))

(define-public crate-god-0.0.2 (c (n "god") (v "0.0.2") (d (list (d (n "eframe") (r "^0.25.0") (d #t) (k 0)))) (h "0xsgw0si3ic9flgqvra3j2fp5via4bsdq2xfyy5fdj0k2p5x343a")))

(define-public crate-god-0.0.3 (c (n "god") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "eframe") (r "^0.25.0") (o #t) (d #t) (k 0)) (d (n "headless_chrome") (r "^1.0.9") (f (quote ("fetch"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "1mjddgsjyag68f4jpw3dpniisz0d2i3nm1gqac1052f56f0197wb") (s 2) (e (quote (("gui" "dep:eframe"))))))

(define-public crate-god-0.0.4 (c (n "god") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "eframe") (r "^0.25.0") (o #t) (d #t) (k 0)) (d (n "headless_chrome") (r "^1.0.9") (f (quote ("fetch"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.196") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0kd8add0cxn6xl6yg63cjpqnsch3gl6dnx3rl5iw3xhfn05xr9zf") (s 2) (e (quote (("gui" "dep:eframe"))))))

