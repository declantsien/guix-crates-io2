(define-module (crates-io #{3}# g gpp) #:use-module (crates-io))

(define-public crate-gpp-0.1.0 (c (n "gpp") (v "0.1.0") (h "0f9hwdwmc2s19hphxxrazhqbakyxd6l0g34a3kyq9imjcfm4zn4x")))

(define-public crate-gpp-0.2.0 (c (n "gpp") (v "0.2.0") (h "0lzfvdb2p7phmc5plnlv3w3pgsfggmb5hz2xqw7nsni6qpszh02a")))

(define-public crate-gpp-0.2.1 (c (n "gpp") (v "0.2.1") (h "00nmicnm8zsf6jdfs3pvkdhb76f21hylcvj38jps6d274zvncrmr")))

(define-public crate-gpp-0.2.2 (c (n "gpp") (v "0.2.2") (h "10qla6m72ddspa8h2x0f3kykamzk1jfkpzh88drhcp59lasszcha")))

(define-public crate-gpp-0.2.3 (c (n "gpp") (v "0.2.3") (h "0kc7qbxcw0fs2is8n3rg0lhdsndz3yhbpdxfqbf8wsslbqikc9hy")))

(define-public crate-gpp-0.2.4 (c (n "gpp") (v "0.2.4") (h "1d3pa56y9s8awy7vq1mrqif881qw9jsfvjiq36nqh51vj5azgi19")))

(define-public crate-gpp-0.2.5 (c (n "gpp") (v "0.2.5") (h "0qq0yhqjnfy9218q5fmlhcq43mryb6xyl5z198g8qyh7gkhpyqgc")))

(define-public crate-gpp-0.3.0 (c (n "gpp") (v "0.3.0") (h "13wfr3phx42zadvw72bzzy8xz4wkbpdlqn9fdzdgbsas95y58z4x")))

(define-public crate-gpp-0.4.0 (c (n "gpp") (v "0.4.0") (h "1ns68ml7d5bqsllf3mazdc6d8k331zzwb5n4v501iqmdjsa75ppw")))

(define-public crate-gpp-0.4.1 (c (n "gpp") (v "0.4.1") (h "1c51vhxy9zqaawwk3g4q08g51rqpdbqk851id4xjsji28ngsmc72")))

(define-public crate-gpp-0.5.0 (c (n "gpp") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)))) (h "0ffa9daml42lzsgg06ivqldn559sy9n4qzqjx8fkjsrcvadgfzm9")))

(define-public crate-gpp-0.6.0 (c (n "gpp") (v "0.6.0") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)))) (h "1dd3q2c0hhlz96zyiy6i75m846fc40wx66k01dzf3zmby9dfrq1m")))

(define-public crate-gpp-0.6.1 (c (n "gpp") (v "0.6.1") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)))) (h "07zbigzmz4yc3zvg2qbr3wfy2zgifrlswry3jz05v6aak6chyzh5")))

(define-public crate-gpp-0.6.2 (c (n "gpp") (v "0.6.2") (d (list (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)))) (h "0vdahgh7h6kca33ak6m8bwpdfgfnbg1brqg1igaagcndm2inhc88")))

