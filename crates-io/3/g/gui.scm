(define-module (crates-io #{3}# g gui) #:use-module (crates-io))

(define-public crate-gui-0.0.0 (c (n "gui") (v "0.0.0") (h "15gddgnzmginvpmjawxha3nzqs600zvnd2j2bnhkpl9pf7sfhrmi")))

(define-public crate-gui-0.1.0 (c (n "gui") (v "0.1.0") (d (list (d (n "gui-derive") (r "^0.1") (d #t) (k 2)))) (h "0dz24wxwlb05dq0hg8x1aghp94yv2mpm7jl8kcqslvr4q35b6046")))

(define-public crate-gui-0.1.1 (c (n "gui") (v "0.1.1") (d (list (d (n "gui-derive") (r "^0.1") (d #t) (k 2)))) (h "0khnvzrbbb85hbgf6asmmw0zijw904xjwj33bg21qs6y1nr1vsx6")))

(define-public crate-gui-0.2.0 (c (n "gui") (v "0.2.0") (d (list (d (n "gui-derive") (r "^0.2") (d #t) (k 2)))) (h "1jjhlpnx1k4bpxqcy2yy97npf0mx6gcm63z840hka8fqpk64ccs5")))

(define-public crate-gui-0.2.1 (c (n "gui") (v "0.2.1") (d (list (d (n "gui-derive") (r "^0.2") (d #t) (k 2)))) (h "1mrw34ik9ipl3c8jvdl8b5v0j03c2j8pa5jsgf546p3x25lva55w")))

(define-public crate-gui-0.2.2 (c (n "gui") (v "0.2.2") (d (list (d (n "gui-derive") (r "^0.2") (d #t) (k 2)))) (h "1xlz28mdmlrwkjff0b1874z4mkmc94fwzz2gvbz5ff2szhjlfxc2")))

(define-public crate-gui-0.1.0-alpha.0 (c (n "gui") (v "0.1.0-alpha.0") (d (list (d (n "gui-derive") (r "^0.1.0-alpha.0") (d #t) (k 2)))) (h "0c2jlzb14dagzxq24hmxjys3w21x4jk615l7acgg47hbk30llykz")))

(define-public crate-gui-0.1.0-alpha.1 (c (n "gui") (v "0.1.0-alpha.1") (d (list (d (n "gui-derive") (r "^0.1.0-alpha.1") (d #t) (k 2)))) (h "008krvfppqvdrmh4bpkyy109k1fys9w30g0k91ni0f4dp3wg8ay7")))

(define-public crate-gui-0.1.0-alpha.2 (c (n "gui") (v "0.1.0-alpha.2") (d (list (d (n "gui-derive") (r "^0.1.0-alpha.2") (d #t) (k 2)))) (h "1ksx82xjgz6n4v6359gl1zpwq3kd367bxmjh5hbmd7y12jy9rw1h")))

(define-public crate-gui-0.1.0-alpha.3 (c (n "gui") (v "0.1.0-alpha.3") (d (list (d (n "gui-derive") (r "^0.1.0-alpha.3") (d #t) (k 2)))) (h "05ylki9k55r9s3fvfl2y5jw7qw9j2fbmwmvyvnmsn64kgrw5g4kq")))

(define-public crate-gui-0.3.0 (c (n "gui") (v "0.3.0") (d (list (d (n "gui-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1gqvdls7hgla8jcnps92ianra0bwwncmz2z4pjxx73ax3y7r3bml") (f (quote (("derive" "gui-derive") ("default" "derive"))))))

(define-public crate-gui-0.4.0 (c (n "gui") (v "0.4.0") (d (list (d (n "gui-derive") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0znbdnad7ywba4asnzkvz8n30dfhnzrdp5wpzpr1j1n32yyvfay9") (f (quote (("derive" "gui-derive") ("default" "derive"))))))

(define-public crate-gui-0.5.0 (c (n "gui") (v "0.5.0") (d (list (d (n "gui-derive") (r "^0.5") (o #t) (d #t) (k 0)))) (h "17c0a19b56b6sb4cf1dly0y31z6hcfzpkp3xcz01kwkix8y7967q") (f (quote (("derive" "gui-derive") ("default" "derive"))))))

(define-public crate-gui-0.6.0-alpha.0 (c (n "gui") (v "0.6.0-alpha.0") (d (list (d (n "gui-derive") (r "=0.6.0-alpha.0") (o #t) (d #t) (k 0)))) (h "0mf9cvf0l2nlf160n6gbsrz4k7gl28mmj9l6rc6nsxkhb4grkvns") (f (quote (("derive" "gui-derive") ("default" "derive"))))))

(define-public crate-gui-0.6.0-alpha.1 (c (n "gui") (v "0.6.0-alpha.1") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "gui-derive") (r "=0.6.0-alpha.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3.1") (f (quote ("macros" "rt"))) (k 2)))) (h "19a84821yq8v2xcpzmx4h7pb7vazafyj2lf61m68513ghpx8ghs4") (f (quote (("derive" "gui-derive") ("default" "derive"))))))

(define-public crate-gui-0.6.0-alpha.2 (c (n "gui") (v "0.6.0-alpha.2") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "gui-derive") (r "=0.6.0-alpha.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3.1") (f (quote ("macros" "rt"))) (k 2)))) (h "0f8bi1az3bl4qip6bgn7iyxin8i7ic103zymrhqb0izs5jw0wrsv") (f (quote (("derive" "gui-derive") ("default" "derive"))))))

(define-public crate-gui-0.6.0 (c (n "gui") (v "0.6.0") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "gui-derive") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.3.1") (f (quote ("macros" "rt"))) (k 2)))) (h "0r4jf1ycy2gvlj5iq9kk19a43iyn17wlj23jvcq1kzr8rf2z1q7l") (f (quote (("derive" "gui-derive") ("default" "derive"))))))

(define-public crate-gui-0.6.1 (c (n "gui") (v "0.6.1") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "gui-derive") (r "^0.6.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (k 2)))) (h "1ayq0ml7jamxnglvyj4lv619vnhl0s1jah4v42vqm4l3761c4myd") (f (quote (("derive" "gui-derive") ("default" "derive")))) (r "1.58")))

(define-public crate-gui-0.6.2 (c (n "gui") (v "0.6.2") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "gui-derive") (r "^0.6.2") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt"))) (k 2)))) (h "0ma30mv8c52v6ba4nhliv2bc58crk3wy7y8kzc2la6yhgl4zhasi") (f (quote (("derive" "gui-derive") ("default" "derive")))) (r "1.58")))

