(define-module (crates-io #{3}# g gig) #:use-module (crates-io))

(define-public crate-gig-0.1.5 (c (n "gig") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "12v32cp4l27linbalkasnxla8f2dib3mrf8vqdr3bj9rfc1yv2y8")))

(define-public crate-gig-0.2.0 (c (n "gig") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "04ab522axa1hbm5b6scb0bx1a5r2myzzykpy7ibw1sxiyizi4hyk")))

(define-public crate-gig-0.2.2 (c (n "gig") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0zri456ndpbfxjbajcj0i34vq7ib8ia5dyr80yf6fz16kq4axgf4")))

(define-public crate-gig-0.2.3 (c (n "gig") (v "0.2.3") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1ral4fjji6lp8gn980h6v9hrqd94azijfaw0c1azn88xxlxji5ap")))

(define-public crate-gig-0.2.4 (c (n "gig") (v "0.2.4") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "git2") (r "^0.8.0") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "14gnlcjagdrk0kpg8dxha2n911d09c1dmwi1x7vhfcrr4l5321pd")))

