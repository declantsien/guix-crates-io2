(define-module (crates-io #{3}# g gox) #:use-module (crates-io))

(define-public crate-gox-0.2.0 (c (n "gox") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "png") (r "^0.13.2") (d #t) (k 0)))) (h "10f0fgwjirniv6xcs7yd55idqyb83j4alzf2d8lhz6k8kn7adbv2")))

(define-public crate-gox-0.3.0 (c (n "gox") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "png") (r "^0.13.2") (d #t) (k 0)))) (h "03p2q9zm4wi8sgzb627n0bk5w2lryy44ddlybv9j4ffh0qjs4gl9")))

(define-public crate-gox-0.4.0 (c (n "gox") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 0)))) (h "1375k9z2visklk0hl6sm0rmb9xy4pqp5r3wkl3dnr11vnjyv919y")))

