(define-module (crates-io #{3}# g gun) #:use-module (crates-io))

(define-public crate-gun-0.1.1 (c (n "gun") (v "0.1.1") (h "0vbnkh6vkqfc2hdf7ivx4lpajywiwr0vdasjywzy98xhh4phv58y")))

(define-public crate-gun-0.1.2 (c (n "gun") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "substring") (r "^1.4") (d #t) (k 0)) (d (n "tungstenite") (r "^0.11") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 0)))) (h "02nqns5ka17vq5qkicdzrnnqjnkrg15nic163snqcga95lm6p2d0")))

