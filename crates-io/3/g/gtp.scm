(define-module (crates-io #{3}# g gtp) #:use-module (crates-io))

(define-public crate-gtp-0.1.0 (c (n "gtp") (v "0.1.0") (h "15il8d694ngn8vbq6ll2smf51kqm15xah7p2cvzqhcp26x9f31ml")))

(define-public crate-gtp-0.1.1 (c (n "gtp") (v "0.1.1") (h "0ih9h3yh1sf14rw6hjm8whah72i7x5kr4jx1rmpb9kss7sz5whkg")))

(define-public crate-gtp-0.1.2 (c (n "gtp") (v "0.1.2") (h "00h31a5ii55iy7ffhckb6ag2anbfg2cyskbd33ckh7awkss0qwg0")))

