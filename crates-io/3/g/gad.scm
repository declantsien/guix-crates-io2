(define-module (crates-io #{3}# g gad) #:use-module (crates-io))

(define-public crate-gad-0.0.0 (c (n "gad") (v "0.0.0") (h "0yr236s1r5scr5y1z70pb45rwzfzgbgqqqpc7vn0ikyl8j5k2dm4")))

(define-public crate-gad-0.1.0 (c (n "gad") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "arrayfire") (r "^3.8.0") (f (quote ("afserde"))) (o #t) (d #t) (k 0)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00qy39bk5mb82p7gc6zmr0swf4maij73y77361w9wjzflfxh2xhf") (f (quote (("default" "arrayfire"))))))

(define-public crate-gad-0.2.0 (c (n "gad") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "arrayfire") (r "^3.8.0") (f (quote ("afserde"))) (o #t) (d #t) (k 0)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1lalpx08xhcn47mnbpgjn27swavjgm2cpri2frp56kjvs82lkffj")))

