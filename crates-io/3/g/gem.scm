(define-module (crates-io #{3}# g gem) #:use-module (crates-io))

(define-public crate-gem-0.0.0 (c (n "gem") (v "0.0.0") (h "1199353q13pj7xnp9b55dd8m7dqyr8zy53xxbrzfg5mmp1l5vkdf") (y #t)))

(define-public crate-gem-0.0.1 (c (n "gem") (v "0.0.1") (h "0sy8mh7zk3xmzc1xn3w9ngbkb75bqgm3g5yarz7zjakvp5qxvxxg") (y #t)))

