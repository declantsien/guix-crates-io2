(define-module (crates-io #{3}# g gif) #:use-module (crates-io))

(define-public crate-gif-0.3.0 (c (n "gif") (v "0.3.0") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.5") (d #t) (k 0)))) (h "0rrndi3b7q0v6i0a7j46gqpkbmshbm9sd29cln0zh86skpw6yfx9") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.4.0 (c (n "gif") (v "0.4.0") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.5") (d #t) (k 0)))) (h "16y2q4gcnm44ck5x99x8l682h0wv85ldg7ljhsxifjyyz0cam0nm") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.4.1 (c (n "gif") (v "0.4.1") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.5") (d #t) (k 0)))) (h "121x9rfq7525a3v396as31bbdr9qdmczlirwm8fwcswynl3adi4a") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.5.0 (c (n "gif") (v "0.5.0") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.8") (d #t) (k 0)))) (h "0ys235sifb8wpp6c0kw86zmfcyrmah7qmgi9z6679yr7l2b86r5s") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.5.1 (c (n "gif") (v "0.5.1") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.8") (d #t) (k 0)))) (h "13jf70z71x2kx4hwn8vwzjhr4fh84rhwy5vichs2nbi8pir51zll") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.6.0 (c (n "gif") (v "0.6.0") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.9") (d #t) (k 0)))) (h "0wpsnagjbkyn5pridvy7ry1liicbqy40c74j9ysj3k40nr8dk3pk") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.7.0 (c (n "gif") (v "0.7.0") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.9") (d #t) (k 0)))) (h "1wqds4da9zpa62lcbj1fmgqq9fwsy6fkwrkfykhd022j9jxksh25") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.8.0 (c (n "gif") (v "0.8.0") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "118f6sg5d3ab7zj434hp1p407v72vwqgj5cav6mwpzsmgllp8fg8") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.9.0 (c (n "gif") (v "0.9.0") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "05nacsjsybiis94b6q7mf0ddzyxd38j2r9msmzblpsax0fdc3iq1") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.9.1 (c (n "gif") (v "0.9.1") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "15m5dfakz0xvi4av0dcwrhwprqbvk927jizxkbgkgxjjkvzdd04a") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.9.2 (c (n "gif") (v "0.9.2") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "0py271291bkg5k1wqgawv0l4zjqsv1rnsx943gskpnr3p92ikr72") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.10.0 (c (n "gif") (v "0.10.0") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "0b2am30x3f89nr0b0a44d41l8pspv5bji7a813kifwv54js18d7z") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.10.1 (c (n "gif") (v "0.10.1") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.2.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "0gwh2v2g54ygw7p4lwgildgz3wc64gbcq9rmrqh3j88zmiawljyx") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.10.2 (c (n "gif") (v "0.10.2") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "0s7mm8i971i0clm8xs6kvbl8yins7cib4isrxs35rq6njysz5hl6") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.10.3 (c (n "gif") (v "0.10.3") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "lzw") (r "^0.10") (d #t) (k 0)))) (h "1bw174f7civdfgryvc8pvyhicpr96hzdajnda4s3y8iv3ch907a7") (f (quote (("raii_no_panic") ("default" "raii_no_panic") ("c_api" "libc"))))))

(define-public crate-gif-0.11.0 (c (n "gif") (v "0.11.0") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "16y301g3dls3ifwxpssbz2amkrvnqfhdajkam7hcy5br6fw0fm04") (f (quote (("std") ("raii_no_panic") ("default" "raii_no_panic" "std"))))))

(define-public crate-gif-0.11.1 (c (n "gif") (v "0.11.1") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "weezl") (r "^0.1.0") (d #t) (k 0)))) (h "1i4n9fwg3zrp07pi5zsgyza2gl8lqnap6fj6875lfy121xbbmvq2") (f (quote (("std") ("raii_no_panic") ("default" "raii_no_panic" "std"))))))

(define-public crate-gif-0.11.2 (c (n "gif") (v "0.11.2") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "weezl") (r "^0.1.4") (d #t) (k 0)))) (h "1pnqfg0k84v0cnqyf85jqvwy3jcs580bfjaps5rzbl3kk5lqyrjs") (f (quote (("std") ("raii_no_panic") ("default" "raii_no_panic" "std"))))))

(define-public crate-gif-0.11.3 (c (n "gif") (v "0.11.3") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "weezl") (r "^0.1.4") (d #t) (k 0)))) (h "0nsfd5qvp69z8kn17ziiq8zv4mclfycyxppf5k9fm2h8g1z1i9y3") (f (quote (("std") ("raii_no_panic") ("default" "raii_no_panic" "std"))))))

(define-public crate-gif-0.11.4 (c (n "gif") (v "0.11.4") (d (list (d (n "color_quant") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17.2") (d #t) (k 2)) (d (n "weezl") (r "^0.1.5") (d #t) (k 0)))) (h "01hbw3isapzpzff8l6aw55jnaqx2bcscrbwyf3rglkbbfp397p9y") (f (quote (("std") ("raii_no_panic") ("default" "raii_no_panic" "std"))))))

(define-public crate-gif-0.12.0 (c (n "gif") (v "0.12.0") (d (list (d (n "color_quant") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17.2") (d #t) (k 2)) (d (n "weezl") (r "^0.1.5") (d #t) (k 0)))) (h "0ibhjyrslfv9qm400gp4hd50v9ibva01j4ab9bwiq1aycy9jayc0") (f (quote (("std") ("raii_no_panic") ("default" "raii_no_panic" "std" "color_quant"))))))

(define-public crate-gif-0.13.0-beta.1 (c (n "gif") (v "0.13.0-beta.1") (d (list (d (n "color_quant") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17.10") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "weezl") (r "^0.1.7") (d #t) (k 0)))) (h "0nw6dksk8d5ichvlpc2ml3048vlcpsjc30g7pz56xnfav6lhs5w7") (f (quote (("std") ("raii_no_panic") ("default" "raii_no_panic" "std" "color_quant")))) (s 2) (e (quote (("color_quant" "dep:color_quant"))))))

(define-public crate-gif-0.13.0 (c (n "gif") (v "0.13.0") (d (list (d (n "color_quant") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17.10") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "weezl") (r "^0.1.8") (d #t) (k 0)))) (h "0m2w4n5jjr42whr7baj0rssj7j7nkdisl2x2jp5g4jz705xqa2hk") (f (quote (("std") ("raii_no_panic") ("default" "raii_no_panic" "std" "color_quant")))) (s 2) (e (quote (("color_quant" "dep:color_quant"))))))

(define-public crate-gif-0.13.1 (c (n "gif") (v "0.13.1") (d (list (d (n "color_quant") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "png") (r "^0.17.10") (d #t) (k 2)) (d (n "rayon") (r "^1.8.0") (d #t) (k 2)) (d (n "weezl") (r "^0.1.8") (d #t) (k 0)))) (h "1whrkvdg26gp1r7f95c6800y6ijqw5y0z8rgj6xihpi136dxdciz") (f (quote (("std") ("raii_no_panic") ("default" "raii_no_panic" "std" "color_quant")))) (s 2) (e (quote (("color_quant" "dep:color_quant"))))))

