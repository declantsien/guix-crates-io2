(define-module (crates-io #{3}# g gll) #:use-module (crates-io))

(define-public crate-gll-0.0.0 (c (n "gll") (v "0.0.0") (h "091i34znwrjyrnp06yv8a0kcjpncpq3b4l3k8iqab997wbnbb1j8")))

(define-public crate-gll-0.0.1 (c (n "gll") (v "0.0.1") (d (list (d (n "indexing") (r "^0.3.1") (d #t) (k 0)) (d (n "indexing") (r "^0.3.1") (d #t) (k 1)) (d (n "ordermap") (r "^0.3.0") (d #t) (k 0)) (d (n "ordermap") (r "^0.3.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^0.4.0") (d #t) (k 0)))) (h "1c4j5vjcpifil2zfzd8jk73rlcz9zf9q8q35vmy2g21h7b433lqf")))

(define-public crate-gll-0.0.2 (c (n "gll") (v "0.0.2") (d (list (d (n "indexing") (r "^0.3.1") (d #t) (k 0)) (d (n "indexing") (r "^0.3.1") (d #t) (k 1)) (d (n "ordermap") (r "^0.3.0") (d #t) (k 0)) (d (n "ordermap") (r "^0.3.0") (d #t) (k 1)) (d (n "proc-macro2") (r "^0.4.0") (d #t) (k 0)))) (h "1a6fxlzg583zkn0608m13fw3nc6ch9m41418dpxs7s54glg81giy")))

