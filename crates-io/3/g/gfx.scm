(define-module (crates-io #{3}# g gfx) #:use-module (crates-io))

(define-public crate-gfx-0.0.0-test (c (n "gfx") (v "0.0.0-test") (d (list (d (n "cgmath") (r "*") (d #t) (k 2)) (d (n "genmesh") (r "*") (d #t) (k 2)) (d (n "gfx_device_gl") (r "*") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 2)) (d (n "gfx_macros") (r "*") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "0z7vqkg7kc7jd6llcr2mvhpqs1hwc1dd30xk9hmfg7pnswpv79bk")))

(define-public crate-gfx-0.1.0 (c (n "gfx") (v "0.1.0") (d (list (d (n "cgmath") (r "*") (d #t) (k 2)) (d (n "genmesh") (r "*") (d #t) (k 2)) (d (n "gfx_device_gl") (r "^0.1.0") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 2)) (d (n "gfx_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "noise") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 0)))) (h "19wsv3a4bq3gabks3s5xim63gvyhha8g5damrjcv5awfkvzfkxac")))

(define-public crate-gfx-0.1.1 (c (n "gfx") (v "0.1.1") (d (list (d (n "cgmath") (r "*") (d #t) (k 2)) (d (n "genmesh") (r "*") (d #t) (k 2)) (d (n "gfx_device_gl") (r "^0.1.0") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 2)) (d (n "gfx_macros") (r "^0.1.1") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "noise") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1rxl4bcgbz09fba9c15sd4mvd9hpw5vmgjhmxdga3261vgmz8h0k")))

(define-public crate-gfx-0.1.2 (c (n "gfx") (v "0.1.2") (d (list (d (n "cgmath") (r "*") (d #t) (k 2)) (d (n "genmesh") (r "*") (d #t) (k 2)) (d (n "gfx_device_gl") (r "^0.1.1") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 2)) (d (n "gfx_macros") (r "^0.1.3") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "noise") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)))) (h "0q7vqk011625bfb6js7rwd8i8sb151lrqcjq4hs1xal62ndp955m")))

(define-public crate-gfx-0.1.4 (c (n "gfx") (v "0.1.4") (d (list (d (n "cgmath") (r "*") (d #t) (k 2)) (d (n "genmesh") (r "*") (d #t) (k 2)) (d (n "gfx_device_gl") (r "^0.1.1") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 2)) (d (n "gfx_macros") (r "^0.1.4") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "noise") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)))) (h "0w5p0divjmmwnswq6kvb4cgxgb79pc8887zyc40vb3gzkaj89g2f")))

(define-public crate-gfx-0.1.5 (c (n "gfx") (v "0.1.5") (d (list (d (n "cgmath") (r "*") (d #t) (k 2)) (d (n "genmesh") (r "*") (d #t) (k 2)) (d (n "gfx_device_gl") (r "^0.1.1") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 2)) (d (n "gfx_macros") (r "^0.1.5") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "noise") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)))) (h "1jqp1l9qkhrfsg1ff1knnybvhzjyvl9xdl3cdamd4fc0q1kiakgn")))

(define-public crate-gfx-0.1.6 (c (n "gfx") (v "0.1.6") (d (list (d (n "cgmath") (r "*") (d #t) (k 2)) (d (n "genmesh") (r "*") (d #t) (k 2)) (d (n "gfx_device_gl") (r "^0.1.1") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 2)) (d (n "gfx_macros") (r "^0.1.5") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "noise") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)))) (h "01s6m9avc2indy20cs6g4l2i8x9pv057ydmylk3mvypnjm3ml6hr")))

(define-public crate-gfx-0.1.7 (c (n "gfx") (v "0.1.7") (d (list (d (n "cgmath") (r "*") (d #t) (k 2)) (d (n "genmesh") (r "*") (d #t) (k 2)) (d (n "gfx_device_gl") (r "^0.1.3") (d #t) (k 0)) (d (n "gfx_gl") (r "*") (d #t) (k 2)) (d (n "gfx_macros") (r "^0.1.6") (d #t) (k 0)) (d (n "glfw") (r "*") (d #t) (k 2)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "noise") (r "*") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)) (d (n "time") (r "*") (d #t) (k 2)))) (h "1l9rnl8djdchqxy81idgry5drrydqyi04bk89j5dihvsqi3z7q6w")))

(define-public crate-gfx-0.2.1 (c (n "gfx") (v "0.2.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1i4pri4rvks8n1kccimm31jg81gi83pxf8v6211bwn7l69zbm7iq")))

(define-public crate-gfx-0.2.2 (c (n "gfx") (v "0.2.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0ahr5cm38kyi4bv04whbras8y3g5psii84zksm79ia4h8hm0680a")))

(define-public crate-gfx-0.2.4 (c (n "gfx") (v "0.2.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1i070qzmbgk23a0jhz1jc11x1j7azs6mllzr4wsdad7dnwd7v23i")))

(define-public crate-gfx-0.2.5 (c (n "gfx") (v "0.2.5") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "^0.0.5") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "05gmrcrsf2xkcznjlnv98l3iy03h656369wbiw51bsv43vdpfyfl")))

(define-public crate-gfx-0.3.1 (c (n "gfx") (v "0.3.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0a5rf5xvfwr3yhrb11llqprny9inab7b00zphvkqwwxqpp51ng17") (f (quote (("unstable"))))))

(define-public crate-gfx-0.4.0 (c (n "gfx") (v "0.4.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0vsvl7jw23bj6s8q17y8ip8lfa8liimsl6vl1g0dl2dn9yqpb9nm") (f (quote (("unstable"))))))

(define-public crate-gfx-0.4.1 (c (n "gfx") (v "0.4.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1rgdwc2c9daxydzs4qn7nal9hmww742r8gzjk7ddv7q9479xisl0") (f (quote (("unstable"))))))

(define-public crate-gfx-0.5.0 (c (n "gfx") (v "0.5.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "00isnpz9jppldabl0kjjp7hbi1d54biz9hizing7101z0ggwmwcl") (f (quote (("unstable"))))))

(define-public crate-gfx-0.5.2 (c (n "gfx") (v "0.5.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "11f25gk3rxv722ffcvd076hd2xcvmy2ap1sxzwvhm22wksac87wq") (f (quote (("unstable"))))))

(define-public crate-gfx-0.6.0 (c (n "gfx") (v "0.6.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1h14sgc5mi29g5nydwznz9917k2niimwv7acmalx23yq99lvraw7") (f (quote (("unstable"))))))

(define-public crate-gfx-0.6.1 (c (n "gfx") (v "0.6.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "0.0.*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0qvlpqh6c4hi42cj4r7452as1b8dlaah2lp33lzn2vdj2xia5npb") (f (quote (("unstable"))))))

(define-public crate-gfx-0.6.2 (c (n "gfx") (v "0.6.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "0ab5r1af4vc9bx2a3yl8b63pxzlfvr0vlbdmdb49jf8dp76979cd") (f (quote (("unstable"))))))

(define-public crate-gfx-0.6.3 (c (n "gfx") (v "0.6.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1gx0q6np452hy0a01kkzda4qkgrfc513a30xdgsckx1sdwlmpmjv") (f (quote (("unstable"))))))

(define-public crate-gfx-0.6.4 (c (n "gfx") (v "0.6.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1dl97rppdw1crgrpvar3ayb0v012m5vmdqyxzcqz3bfrm9p1gsgk") (f (quote (("unstable"))))))

(define-public crate-gfx-0.6.5 (c (n "gfx") (v "0.6.5") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1z9bwf5m008f2587kfmpz07y1jr75b78d5i5bvzs5q91ma5sbpv2") (f (quote (("unstable"))))))

(define-public crate-gfx-0.7.0 (c (n "gfx") (v "0.7.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "1v74ynsciapgfcapmwkkgj9zbcj81qavmk3mmikxm5nlsbx51amn") (f (quote (("unstable"))))))

(define-public crate-gfx-0.7.1 (c (n "gfx") (v "0.7.1") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "11fpqxgrjx838y5ck1ljmppw8m4z7r71x1jm39n64bwb1wwjgyd2") (f (quote (("unstable"))))))

(define-public crate-gfx-0.7.2 (c (n "gfx") (v "0.7.2") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "02ffqdyvv43s4r15fdarp1g61a6xvzd9hp048zi4my5905cmvbf1") (f (quote (("unstable"))))))

(define-public crate-gfx-0.8.0 (c (n "gfx") (v "0.8.0") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "draw_state") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)))) (h "12kqrl8xv6h7ma8cqhlrpw26rs4ydcjhgi67pg8qr8gqz86v41is") (f (quote (("unstable"))))))

(define-public crate-gfx-0.8.1 (c (n "gfx") (v "0.8.1") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "draw_state") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)))) (h "1yniv1baq2d3cr0k3a6kdnjh69y8wrzqbwyp0gn0igz51dl76f7x") (f (quote (("unstable"))))))

(define-public crate-gfx-0.9.0 (c (n "gfx") (v "0.9.0") (d (list (d (n "draw_state") (r "^0.3.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "097cvs7m1pwn0hifzfp5r11v3g60x7pla0vx89c79i51d9adzlf9") (f (quote (("unstable"))))))

(define-public crate-gfx-0.9.1 (c (n "gfx") (v "0.9.1") (d (list (d (n "draw_state") (r "^0.3.0") (d #t) (k 0)) (d (n "gfx_core") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "00229fqmw090vfrbi2xh17gmg2i9gv6pj2mjiih68z5hlqmvah52") (f (quote (("unstable"))))))

(define-public crate-gfx-0.10.0 (c (n "gfx") (v "0.10.0") (d (list (d (n "draw_state") (r "^0.4") (d #t) (k 0)) (d (n "gfx_core") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1yzhkw9ij0jk4w8nqdcfighfrdvxkqi99ayfzqlg13cdqb74vnmx") (f (quote (("unstable"))))))

(define-public crate-gfx-0.10.1 (c (n "gfx") (v "0.10.1") (d (list (d (n "draw_state") (r "^0.4") (d #t) (k 0)) (d (n "gfx_core") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0v8clawyx6vqx4qyh4za5fxcs0syxnnd063giy04776v4j7icrm6") (f (quote (("unstable"))))))

(define-public crate-gfx-0.10.2 (c (n "gfx") (v "0.10.2") (d (list (d (n "draw_state") (r "^0.4") (d #t) (k 0)) (d (n "gfx_core") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "02ic58sa0qvpl3p3q6hx4l86kx229m6rzx3b7nymfmqzn3w2iccb") (f (quote (("unstable"))))))

(define-public crate-gfx-0.11.0 (c (n "gfx") (v "0.11.0") (d (list (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "gfx_core") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0wrza7n6zml1f2i4f12dvqpz2824fhq6w21p7zdpd13mqmcx2507") (f (quote (("unstable"))))))

(define-public crate-gfx-0.12.0 (c (n "gfx") (v "0.12.0") (d (list (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "gfx_core") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1spaq1q26xin686f9nmsay4ysygn91zkw5hq865qpyfqlmyb8gql") (f (quote (("unstable"))))))

(define-public crate-gfx-0.12.1 (c (n "gfx") (v "0.12.1") (d (list (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "gfx_core") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "026ba0qqjc7mckkhznb4bcnwrlngi7srga5hgg1s0b4sr7cra4xw") (f (quote (("unstable"))))))

(define-public crate-gfx-0.12.2 (c (n "gfx") (v "0.12.2") (d (list (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "gfx_core") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "05jypppx9lx6zq9fiyvsl85n9gcy27zjl2yifrcwblrpxmp3171p") (f (quote (("unstable"))))))

(define-public crate-gfx-0.13.0 (c (n "gfx") (v "0.13.0") (d (list (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "gfx_core") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "066r6d42ylgx4nw4sdbpsnd1vhn1pk56w40jw8bpv52k4046igfx") (f (quote (("unstable"))))))

(define-public crate-gfx-0.14.0 (c (n "gfx") (v "0.14.0") (d (list (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "gfx_core") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1hb3y30ibbsq4i875pvzspqmg3lzkq3bj4y5gb9awb5d323l3bn4") (f (quote (("unstable"))))))

(define-public crate-gfx-0.14.1 (c (n "gfx") (v "0.14.1") (d (list (d (n "draw_state") (r "^0.6") (d #t) (k 0)) (d (n "gfx_core") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "11pb970rrj8xm183fi1k24iw4kjn1ddlh2pcsfq8cpa5vzapsri4") (f (quote (("unstable"))))))

(define-public crate-gfx-0.15.0 (c (n "gfx") (v "0.15.0") (d (list (d (n "draw_state") (r "^0.7") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1g3fkz4vlgss3ki37bs0bpvw8ypwmmfsnc41sj6swwj72mqfw01c") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serialize"))))))

(define-public crate-gfx-0.15.1 (c (n "gfx") (v "0.15.1") (d (list (d (n "draw_state") (r "^0.7") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1fhdwzpa24l06vrq49i0ibcx417xddvsr6a2visvrfnla97ihk96") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serialize"))))))

(define-public crate-gfx-0.16.0 (c (n "gfx") (v "0.16.0") (d (list (d (n "cgmath") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "draw_state") (r "^0.7") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "08h686dkbhm2nzyxwddd6ks7dvldnxfg9r94scka1v82pqdnz89s") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serialize") ("cgmath-types" "gfx_core/cgmath-types" "cgmath"))))))

(define-public crate-gfx-0.16.1 (c (n "gfx") (v "0.16.1") (d (list (d (n "cgmath") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "draw_state") (r "^0.7") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "04dxw7fz9jy57plmdaggswph9nss302lp1zx6zhfg45ximik5i7j") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serialize") ("cgmath-types" "gfx_core/cgmath-types" "cgmath"))))))

(define-public crate-gfx-0.16.2 (c (n "gfx") (v "0.16.2") (d (list (d (n "cgmath") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "draw_state") (r "^0.7") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1p1k8xgfi6h3lid6j7k7fsyz712agxqyyllgxna2h3hpcm976bjq") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serialize") ("cgmath-types" "gfx_core/cgmath-types" "cgmath"))))))

(define-public crate-gfx-0.17.0 (c (n "gfx") (v "0.17.0") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)))) (h "17cc17w3k6s2vkww26r39zv3m6md8rmw87qirw9balz4ic1cid1c") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serde"))))))

(define-public crate-gfx-0.16.3 (c (n "gfx") (v "0.16.3") (d (list (d (n "cgmath") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "draw_state") (r "^0.7") (d #t) (k 0)) (d (n "gfx_core") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "14yhfk56y2v23630qnkrkgb7rcc1992b1j2mq33hdrk34d0xy8az") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serialize") ("cgmath-types" "gfx_core/cgmath-types" "cgmath"))))))

(define-public crate-gfx-0.17.1 (c (n "gfx") (v "0.17.1") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "gfx_core") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0d28vs7fzj6dk16vq7s2632681zak2qgslrllx156927yz0y0z3x") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serde"))))))

(define-public crate-gfx-0.18.0 (c (n "gfx") (v "0.18.0") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0vs6h6xh6y3ls6wk7fxl2g1jmw3kmsliy2vpyx8vs55fppds1b0w") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serde")))) (y #t)))

(define-public crate-gfx-0.18.1 (c (n "gfx") (v "0.18.1") (d (list (d (n "derivative") (r "^1.0") (d #t) (k 0)) (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0scv4gijrqzxdhbqr6dn7ip3kb4avdvzk53a5kwzlpi73siki7k1") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serde"))))))

(define-public crate-gfx-0.18.2 (c (n "gfx") (v "0.18.2") (d (list (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)))) (h "0nqmxqi3x4ni0g78g77a6aldrv8cfvzhnpqhxyd2ap4aa3wldph1") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serde"))))))

(define-public crate-gfx-0.18.3 (c (n "gfx") (v "0.18.3") (d (list (d (n "draw_state") (r "^0.8") (d #t) (k 0)) (d (n "gfx_core") (r "^0.9.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mint") (r "^0.5") (o #t) (d #t) (k 0)))) (h "02lf16bgyvdk5imzw0r0xyxjrsyv2ficmf9kiarzvf5zx1bl0xgb") (f (quote (("unstable") ("serialize" "gfx_core/serialize" "draw_state/serde"))))))

