(define-module (crates-io #{3}# g g2p) #:use-module (crates-io))

(define-public crate-g2p-0.1.0 (c (n "g2p") (v "0.1.0") (d (list (d (n "g2poly") (r "^0.1") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "0j5lb0dxi4fi1kp98ziwivgz5g8n3rhlsx9v3mcpssafisj9wxh9")))

(define-public crate-g2p-0.1.1 (c (n "g2p") (v "0.1.1") (d (list (d (n "g2poly") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)))) (h "1fdc4d2sxzjiy4gavik6z34x1sddcnkib0bvk3kgfgr3bll13f1q")))

(define-public crate-g2p-0.1.2 (c (n "g2p") (v "0.1.2") (d (list (d (n "g2poly") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^0.15.23") (f (quote ("parsing"))) (d #t) (k 0)))) (h "0ynlgxlra60m5whvsivlmsygmlgza0r6fc4dhb8c0i0h33zcrf1r")))

(define-public crate-g2p-0.2.0 (c (n "g2p") (v "0.2.0") (d (list (d (n "g2gen") (r "^0.1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)))) (h "0lfqbfsb1mhr522ch4rfkss2askl8b4rnli6mwp6y2n98iwi16jl")))

(define-public crate-g2p-0.3.0 (c (n "g2p") (v "0.3.0") (d (list (d (n "g2gen") (r "^0.2.0") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)))) (h "13shl77rpkch7b2fiq5g8slxljsqxnf18gdncwfniifza6him8dv")))

(define-public crate-g2p-0.3.1 (c (n "g2p") (v "0.3.1") (d (list (d (n "g2gen") (r "^0.2.1") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)))) (h "0ji4idjc45xshrq3nlghxg2ks8csc12g66zs4fm8vlq7zphiyih8")))

(define-public crate-g2p-0.3.2-pre (c (n "g2p") (v "0.3.2-pre") (d (list (d (n "g2gen") (r "^0.2.1") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)))) (h "0i9pyxz5xwab02gbmb2wj8h87wpp14p9bcdicvfgiy0wxgfd1inf") (y #t)))

(define-public crate-g2p-0.4.0 (c (n "g2p") (v "0.4.0") (d (list (d (n "g2gen") (r "^0.4") (d #t) (k 0)) (d (n "g2poly") (r "^0.4") (d #t) (k 0)) (d (n "static_assertions") (r "^0.3") (d #t) (k 2)))) (h "0m97qvalz4aq68hps0m19gkh617qr2y4a3ik1d1fbjr94rivq2dz")))

(define-public crate-g2p-1.0.0 (c (n "g2p") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "g2gen") (r "^1.0") (d #t) (k 0)) (d (n "g2poly") (r "^1.0") (d #t) (k 0)) (d (n "galois_2p8") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "reed-solomon-erasure") (r "^6.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "0kb1c1kzk01ikbcm760xl9v6520ai8hcad2078f84m6g3949p9lz")))

(define-public crate-g2p-1.0.1 (c (n "g2p") (v "1.0.1") (d (list (d (n "g2gen") (r "^1.0") (d #t) (k 0)) (d (n "g2poly") (r "^1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "galois_2p8") (r "^0.1.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "reed-solomon-erasure") (r "^6.0") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "17jip1izln1k6p694yns1qzapv89gdyzmx59fmvhbnnjqfyxjdpw")))

