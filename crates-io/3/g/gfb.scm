(define-module (crates-io #{3}# g gfb) #:use-module (crates-io))

(define-public crate-gfb-1.0.0 (c (n "gfb") (v "1.0.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "05dgmkh4pja4cm5lbpcfqscp7rb65i7k6k7gl6dpap0hkwdylvsr")))

(define-public crate-gfb-1.1.1 (c (n "gfb") (v "1.1.1") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "0p36f2l1qjyvbjhpd7bry8l5kx09k91ci8mypwf3nal3vr962d7n")))

(define-public crate-gfb-1.1.2 (c (n "gfb") (v "1.1.2") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1qwdm3i6wfv11lxgq3s8j478jmy5r3r0hkll7kk4fs8ifab11pmn")))

(define-public crate-gfb-1.1.3 (c (n "gfb") (v "1.1.3") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.156") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1ynak1lrkcw6d08q0dkryb56ipq9llbcz1n5r4s50qrl6pr5kn7v")))

