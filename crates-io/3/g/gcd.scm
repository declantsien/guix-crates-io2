(define-module (crates-io #{3}# g gcd) #:use-module (crates-io))

(define-public crate-gcd-1.0.0 (c (n "gcd") (v "1.0.0") (h "1jkczxrj5nqah6xw8wpwzkclb7n70525kic1zvdclwxlfy2wqnii")))

(define-public crate-gcd-1.1.0 (c (n "gcd") (v "1.1.0") (h "0fnzn69a7r7c7dq9r04cs8qjh0xpamxwj4kkn7idnw9n6mrwp5h8")))

(define-public crate-gcd-1.2.0 (c (n "gcd") (v "1.2.0") (h "17sdv9srw8kr737h2smyi1q71h1y91gbmfqxcr6p59xv0bhpc6n2")))

(define-public crate-gcd-2.0.0 (c (n "gcd") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "0bzyhcqcnrczgnxj79fcmb5g4394qgj35zsa47l57rfn1xsd2xvb")))

(define-public crate-gcd-2.0.1 (c (n "gcd") (v "2.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "14ib5nrrrl795sk49488dkj2aynr4717kzdxwpj1mc9apw0x6z0w")))

(define-public crate-gcd-2.0.2 (c (n "gcd") (v "2.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "02z03yl5iypcjpawznzxjci99nfnpqjc83wg2biqzvh851vn71vc")))

(define-public crate-gcd-2.1.0 (c (n "gcd") (v "2.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "16m7mb5p5zwy836sclchmwrndnrjxz0qnbrvm0w9jy6anbd7hygk")))

(define-public crate-gcd-2.2.0 (c (n "gcd") (v "2.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "1lmzvyryzi09jwggfcsxfxagh0w9zj16nia0jni7m5hamn4b1cd4")))

(define-public crate-gcd-2.3.0 (c (n "gcd") (v "2.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 2)))) (h "06l4fib4dh4m6gazdrzzzinhvcpcfh05r4i4gzscl03vnjhqnx8x")))

