(define-module (crates-io #{3}# g gsa) #:use-module (crates-io))

(define-public crate-gsa-0.1.0 (c (n "gsa") (v "0.1.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "gif") (r "^0.12.0") (d #t) (k 0)) (d (n "gilrs") (r "^0.10.2") (d #t) (k 0)) (d (n "glam") (r "^0.24.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "softbuffer") (r "^0.3.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.6") (d #t) (k 0)))) (h "1h0jrxrjj261px96f8rl9yfkrg90jyi5pad5hsncspp19y8w60cc")))

(define-public crate-gsa-0.2.0 (c (n "gsa") (v "0.2.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dunce") (r "^1.0.4") (d #t) (k 0)) (d (n "gif") (r "^0.12.0") (d #t) (k 0)) (d (n "gilrs") (r "^0.10.2") (d #t) (k 0)) (d (n "glam") (r "^0.24.0") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.3.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.6") (d #t) (k 0)))) (h "02xq0h8yg6vvg6r4j0sqx5s7py5152xm0gilpv6crcijdimgm566")))

(define-public crate-gsa-0.2.1 (c (n "gsa") (v "0.2.1") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "dunce") (r "^1.0.4") (d #t) (k 0)) (d (n "gif") (r "^0.12.0") (d #t) (k 0)) (d (n "gilrs") (r "^0.10.2") (d #t) (k 0)) (d (n "glam") (r "^0.24.0") (d #t) (k 0)) (d (n "path-slash") (r "^0.2.1") (d #t) (k 0)) (d (n "softbuffer") (r "^0.3.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.6") (d #t) (k 0)))) (h "0ss9z8ngpsq45hzgp4kygckv958gl85kc95bcrlj45mas5b2iz73")))

