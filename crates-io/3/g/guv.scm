(define-module (crates-io #{3}# g guv) #:use-module (crates-io))

(define-public crate-guv-0.1.0 (c (n "guv") (v "0.1.0") (h "0j8rap66zjn3r03ikkn2bi96mr47v7d5b9wiyp5vpzrd9ycnhzgg") (y #t)))

(define-public crate-guv-0.1.1 (c (n "guv") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0zz8si2wiciza4dgz6xjp75ln9s1mm1ark8h1zb5vrdx5kzmfzzi")))

(define-public crate-guv-0.2.0 (c (n "guv") (v "0.2.0") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 2)) (d (n "cordic") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "fixed") (r "^1") (o #t) (d #t) (k 0)) (d (n "fixed") (r "^1") (f (quote ("arbitrary" "num-traits"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (d #t) (k 2)) (d (n "plotters") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)) (d (n "proptest-arbitrary-interop") (r "^0.1") (d #t) (k 2)))) (h "1hznn2cpqcg8kd5rp3b88d4jmg0xfk5ghbcvr2n3kwrv5hj2m1z7") (f (quote (("std") ("default")))) (s 2) (e (quote (("float" "dep:num-traits") ("fixed" "dep:num-traits" "dep:fixed" "dep:cordic")))) (r "1.60")))

