(define-module (crates-io #{3}# g glx) #:use-module (crates-io))

(define-public crate-glx-0.1.0 (c (n "glx") (v "0.1.0") (d (list (d (n "gl_common") (r "^0.1.0") (d #t) (k 0)) (d (n "gl_generator") (r "^0.4") (d #t) (k 1)) (d (n "khronos_api") (r "^0.0.8") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16hlr0v71d7by51j8ys1x12b1mmi6whz71lm9k8mqc28fnif7zhx")))

(define-public crate-glx-0.1.1 (c (n "glx") (v "0.1.1") (d (list (d (n "gl_generator") (r "^0.4") (d #t) (k 1)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rwgdywzjygq7dymhvvfdfi6522s7vgk9si7ipsmsbqpv1qg3cr0")))

(define-public crate-glx-0.1.2 (c (n "glx") (v "0.1.2") (d (list (d (n "gl_generator") (r "^0.5") (d #t) (k 1)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wiggvxpnb3yixgp1s0hf4k414m5s6wds6hb3vyjqi67m5zh105j")))

(define-public crate-glx-0.2.0 (c (n "glx") (v "0.2.0") (d (list (d (n "gl_generator") (r "^0.6") (d #t) (k 1)) (d (n "khronos_api") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00x5hlnf9x36gx68lii93pdqh9l8jflz0ci6gx0w1d1qzvapgq65")))

(define-public crate-glx-0.2.1 (c (n "glx") (v "0.2.1") (d (list (d (n "gl_generator") (r "^0.6") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qp4qghrqi72kb0nydxywxk9g5m2k9qlm7z4rc8sa8qjpjki7chr")))

(define-public crate-glx-0.2.2 (c (n "glx") (v "0.2.2") (d (list (d (n "gl_generator") (r "^0.8") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g4kbx63616q3gg8bpfkdmkg87lmvvqpm97ngfm92zpkvx4dlmmc")))

(define-public crate-glx-0.2.3 (c (n "glx") (v "0.2.3") (d (list (d (n "gl_generator") (r "^0.9") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fd7ii92w0cqs7ch79qgvlv185ljkpzvlpplamj644kfhk1fg9k3")))

(define-public crate-glx-0.2.4 (c (n "glx") (v "0.2.4") (d (list (d (n "gl_generator") (r "^0.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mhi3rgvl2759vl842pql12j611fifiy7wlz8r7gvzyyy52iiv2j")))

(define-public crate-glx-0.2.5 (c (n "glx") (v "0.2.5") (d (list (d (n "gl_generator") (r "^0.11") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gdjiaidbcdrmcg0v6ha32mx2rcwch8dyrs7vl24y25si6m5jvfm")))

(define-public crate-glx-0.2.6 (c (n "glx") (v "0.2.6") (d (list (d (n "gl_generator") (r "^0.13") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05bdxs5d636sbpr8kx73x1cfi17ipvnpi953shpfa58w931ksx7p")))

