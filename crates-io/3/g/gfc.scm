(define-module (crates-io #{3}# g gfc) #:use-module (crates-io))

(define-public crate-gfc-1.0.0 (c (n "gfc") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colorful") (r "^0.2.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "random-string") (r "^1.0.0") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "04a0gjphxfgcfhsr8xsrn864hf4msh8hrxi4y0j8ca1ys7dszimw")))

