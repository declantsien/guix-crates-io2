(define-module (crates-io #{3}# g gha) #:use-module (crates-io))

(define-public crate-gha-0.1.0 (c (n "gha") (v "0.1.0") (h "0815cg5wa5vslcva2bv8spkwqkcl8mmy3n5vhyz4vcrflhpzfbig") (r "1.70")))

(define-public crate-gha-0.1.1 (c (n "gha") (v "0.1.1") (h "168r6k2xizi2qvwhzrq25vzfj1bxm39drnkxvm75hr2vx93v2z7s") (r "1.70")))

(define-public crate-gha-0.1.2 (c (n "gha") (v "0.1.2") (h "1fyvdwlbsvh0kyciqx6z6ijhh6sbz701iqkswflrsav4hr5vjv9w") (r "1.70")))

(define-public crate-gha-0.1.3 (c (n "gha") (v "0.1.3") (h "0yjc1lxcybhzcmkrl48zknd9kifj4l6rwv6z641wzkdm85ik563d") (r "1.70")))

(define-public crate-gha-0.1.4 (c (n "gha") (v "0.1.4") (h "1spcfccg1cydji5a01pf3jkl7rlpiasxq041j0kyx3yqqhpgymmk") (y #t) (r "1.70")))

(define-public crate-gha-0.1.5 (c (n "gha") (v "0.1.5") (h "17s5y591yhpkjy6wwh60qlxpkgcafxx227kfj0i6yh6kwwwbglbw") (r "1.70")))

(define-public crate-gha-0.1.6 (c (n "gha") (v "0.1.6") (h "0h5bdjms23rmq7z7z9w4yh6i75sg5l93shnq58cppcyxbm446y18") (r "1.70")))

