(define-module (crates-io #{3}# g gpc) #:use-module (crates-io))

(define-public crate-gpc-0.1.0 (c (n "gpc") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "0qs1hzd3yisd4d73vhq2lg77qzkdsz7q6f0m48x0gskqp3bpm1iy")))

