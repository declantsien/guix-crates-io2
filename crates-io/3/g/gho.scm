(define-module (crates-io #{3}# g gho) #:use-module (crates-io))

(define-public crate-gho-0.2.4 (c (n "gho") (v "0.2.4") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "1vcasw40yb81q8329r84ph916w5kf96bgfnhzybccxlnrpknfd07")))

(define-public crate-gho-0.3.0 (c (n "gho") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "19hxrjl31vslxr675wxw910gvf43n2j4pfp6h53wh4sfig0l3w6h")))

(define-public crate-gho-0.4.0 (c (n "gho") (v "0.4.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive" "env" "unicode" "cargo"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8") (d #t) (k 0)))) (h "0fvmv75kw2ii7mhryra8bvgw8rr5vamfzywq2vq83y4rc9smcapg")))

