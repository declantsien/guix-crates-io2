(define-module (crates-io #{3}# g gma) #:use-module (crates-io))

(define-public crate-gma-0.1.0 (c (n "gma") (v "0.1.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.18") (d #t) (k 0)))) (h "1qxqxwk8s930n7lh8m3nrs13ds9ar4vm0j3gp1blm2cbf6ifxcy6")))

(define-public crate-gma-0.1.1 (c (n "gma") (v "0.1.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.18") (d #t) (k 0)))) (h "152h1fzapnymgy5dj84wsi44ikli0mhvzjdgw9s4jbji6k76jxgb")))

(define-public crate-gma-0.2.0 (c (n "gma") (v "0.2.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.18") (d #t) (k 0)))) (h "11bw94r9kgk97pclrysp36c8r1ynfqyd8qmwd6wlisyarivxkjam")))

(define-public crate-gma-0.2.1 (c (n "gma") (v "0.2.1") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.18") (d #t) (k 0)))) (h "1m0hw3w16hx1mdfw78k4lsib01bcfdgfvlbd0glk6hz1ya96np89")))

(define-public crate-gma-0.3.0 (c (n "gma") (v "0.3.0") (d (list (d (n "crc") (r "^1.8.1") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.1.3") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.18") (d #t) (k 0)))) (h "03ah6qfj2vbny7bic7231pfg9gsmrkgs38fdnfy1b17khpxl0rs9")))

(define-public crate-gma-0.3.1 (c (n "gma") (v "0.3.1") (d (list (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.29") (d #t) (k 0)))) (h "0as4npp75y1gvwxx6nir169cnd6n1jg3gs32n3dvfxnv610p027h")))

(define-public crate-gma-1.0.0 (c (n "gma") (v "1.0.0") (d (list (d (n "crc") (r "^2.1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1.29") (d #t) (k 0)))) (h "08ngmwqpbsmz2c5n5jf4yy83vswhdmrbfmwdysds74qjpfs7kdg7")))

