(define-module (crates-io #{3}# g g60) #:use-module (crates-io))

(define-public crate-g60-0.1.0 (c (n "g60") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1nlqndz1r9cn1cj16cf62jpf00gv0k57cji0zyn3ikj36yj5byyf")))

(define-public crate-g60-0.1.1 (c (n "g60") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "1kx3i7cwfdxx4qw45a1schgc45f8g8nq9rgpysabhpjkjd26s395")))

(define-public crate-g60-0.2.0 (c (n "g60") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)))) (h "0d6bk7841x4dijvb5nj1vs1yksp6a95fxpy36nrzayflk05cxj0z") (f (quote (("random" "rand") ("naive"))))))

(define-public crate-g60-0.3.0 (c (n "g60") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports" "plotters"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0k9w9vpp1r5isfqbk68v52x6rm5xfgf3n828qcjqjhwvry9g4kpw")))

