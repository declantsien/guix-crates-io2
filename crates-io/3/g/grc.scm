(define-module (crates-io #{3}# g grc) #:use-module (crates-io))

(define-public crate-grc-0.6.0 (c (n "grc") (v "0.6.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "19cgwpsmv0dwgcfz0pq5a11pcxq1v2b48ikzpbl2rxrk7z3qryih")))

(define-public crate-grc-0.6.1 (c (n "grc") (v "0.6.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "0k7qsasr5xpbk01l6dqny5bycibiv19sxy4m27blbbhnfm96x4ln")))

(define-public crate-grc-0.6.2 (c (n "grc") (v "0.6.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "0gbgy8rrskyg9w5wqifj0b4gyknkjhbrrz68lz1j0s0pp7h34fpi")))

(define-public crate-grc-0.6.3 (c (n "grc") (v "0.6.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "03ifq4s6kf1zsks0db2a5x006n3p6vgf89657xggw8l93v3ppmza")))

(define-public crate-grc-0.7.0 (c (n "grc") (v "0.7.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "1hh4zyzl8i1v50jr7bd4hwj374aw5r42y953gzmz0ggm36ij86kv")))

(define-public crate-grc-0.7.1 (c (n "grc") (v "0.7.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "1skx4g9l59dldyf4y6j15g770cs80db3c65a4ml3rh3wfw11wvz6")))

(define-public crate-grc-0.8.0 (c (n "grc") (v "0.8.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "1nk3y62vwj7fav4d009w0vqgpbiv23yfs4kvlmp5dz76la6zzwxh")))

(define-public crate-grc-0.8.1 (c (n "grc") (v "0.8.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.12.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "0fzm8lvxfbfkhjmdyx8j3camlb7yj6wxgj3ldx54ddng65gvjdwr")))

(define-public crate-grc-0.8.2 (c (n "grc") (v "0.8.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)))) (h "0hzzs8kqmchr33wcmk8x23yd3kf2prgiyi96fdg8ivzkq087m59g")))

(define-public crate-grc-0.9.0-rc1 (c (n "grc") (v "0.9.0-rc1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0hfdkyfrjnh59rsjnr0186lv33hx7y417idnhw76ak2diqq5yfdy")))

(define-public crate-grc-0.9.0-rc.1 (c (n "grc") (v "0.9.0-rc.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "1ilg0hn7hlff11c4b8hwipcqi4bps1vba7aisk19xqh7c8zvh478")))

(define-public crate-grc-0.9.1 (c (n "grc") (v "0.9.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "0mggqfd2l4qxpzc1xib6ln0j3qkihnqpfn6vn9s5v7cn88bwcqym")))

(define-public crate-grc-0.9.2 (c (n "grc") (v "0.9.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "1fgsfizzy4lcajy0zyci38axgw9hi8crpnw09vl0zmq3clm1qfc2")))

(define-public crate-grc-1.0.0 (c (n "grc") (v "1.0.0") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "1pajyzc3nkypd1vjr7xl99rmphh8r7vn2l04392a74ivwcd4bf9d")))

(define-public crate-grc-1.1.0 (c (n "grc") (v "1.1.0") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.13.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "042rql2bmk6lai49mrk9aj238q3v71k97lvqb9gyzqbkxchq7n2j")))

(define-public crate-grc-1.2.0 (c (n "grc") (v "1.2.0") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.7.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "092s92z6dj6qjva1fn09lfagfw3y324n5mf6xk9l8i5sdggsyj0q")))

(define-public crate-grc-1.2.1 (c (n "grc") (v "1.2.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "console") (r "^0.14.1") (d #t) (k 0)) (d (n "dialoguer") (r "^0.8.0") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.7") (d #t) (k 0)))) (h "1jjm769q1cf2jrgr6lcd8gg957a1d526vdc2gsql7p8r1ccc01sb")))

(define-public crate-grc-1.2.2 (c (n "grc") (v "1.2.2") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.0") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0nia2kgqba2fx7i88k7aflf4250nka4brzn5ld86sy7792zgrw63")))

(define-public crate-grc-1.3.1 (c (n "grc") (v "1.3.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "clap") (r "^3.1.18") (d #t) (k 0)) (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.1") (f (quote ("editor" "fuzzy-select"))) (d #t) (k 0)) (d (n "git2") (r "^0.14.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 1)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 1)))) (h "0dx4dahfzma843hy37w2fq5g4mqb3xj4dbsfkvxxrr7776hlag9i")))

