(define-module (crates-io #{3}# g ger) #:use-module (crates-io))

(define-public crate-ger-0.1.0 (c (n "ger") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "term") (r "^0.4.5") (d #t) (k 0)))) (h "14y469sx0lx24d0l2sppfzq6b2fxfxmpv75k0lbj067i68z9gnlm")))

(define-public crate-ger-0.1.1 (c (n "ger") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "term") (r "^0.4.5") (d #t) (k 0)))) (h "0gvgvpkxgmc1scgsca6qhvvzsvhpn5khnby35kp02x34cd9szz9r")))

(define-public crate-ger-0.2.0 (c (n "ger") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.9.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)) (d (n "term") (r "^0.4.5") (d #t) (k 0)))) (h "1y7lmdhh0qs11l1vjcyhdgw0z7wq45x7fw1j7wq081bgwhq36j36")))

