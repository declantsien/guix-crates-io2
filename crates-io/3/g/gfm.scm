(define-module (crates-io #{3}# g gfm) #:use-module (crates-io))

(define-public crate-gfm-0.1.0 (c (n "gfm") (v "0.1.0") (d (list (d (n "comrak") (r "^0.6.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1376jdh2811986fca1g9prnsfg10mdl7w7wj5390xszd4ijvl1jg")))

