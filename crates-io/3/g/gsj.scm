(define-module (crates-io #{3}# g gsj) #:use-module (crates-io))

(define-public crate-gsj-1.0.0 (c (n "gsj") (v "1.0.0") (d (list (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1zcki2rrrg346j9khw7shwihxlayk2glx6kl4avn9x3nynf227wr")))

(define-public crate-gsj-1.0.1 (c (n "gsj") (v "1.0.1") (d (list (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1nvh546s6yr1g5dmgwk6cqv0h0m4z6vfsjl20dp5c4gv6q62jh0k")))

