(define-module (crates-io #{3}# g gme) #:use-module (crates-io))

(define-public crate-gme-0.1.0 (c (n "gme") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1na1rmh2avdqm62k68bli6xdmas7rydkj5qpjmf56knnp129lv0j") (f (quote (("ym2612_emu_nuked") ("ym2612_emu_mame") ("vgm") ("spc") ("sap") ("nsfe") ("nsf") ("kss") ("hes") ("gym") ("gbs") ("default" "ay" "gbs" "gym" "hes" "kss" "nsf" "nsfe" "sap" "spc" "vgm" "ym2612_emu_nuked") ("ay"))))))

(define-public crate-gme-0.1.1 (c (n "gme") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m6z9q5pbwk2sjxkm985byc0ncxphklpr41q3x8kh227lq96irmb") (f (quote (("ym2612_emu_nuked") ("ym2612_emu_mame") ("vgm") ("spc") ("sap") ("nsfe") ("nsf") ("kss") ("hes") ("gym") ("gbs") ("default" "ay" "gbs" "gym" "hes" "kss" "nsf" "nsfe" "sap" "spc" "vgm" "ym2612_emu_nuked") ("ay"))))))

(define-public crate-gme-0.1.2 (c (n "gme") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06bkkj9drzfjjly4cywwks7znsfax9xy9m4v6dlyhv7wzczdxyy7") (f (quote (("ym2612_emu_nuked") ("ym2612_emu_mame") ("vgm") ("spc") ("sap") ("nsfe") ("nsf") ("kss") ("hes") ("gym") ("gbs") ("default" "ay" "gbs" "gym" "hes" "kss" "nsf" "nsfe" "sap" "spc" "vgm" "ym2612_emu_nuked") ("ay"))))))

