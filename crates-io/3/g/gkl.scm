(define-module (crates-io #{3}# g gkl) #:use-module (crates-io))

(define-public crate-gkl-0.1.0 (c (n "gkl") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1i1jbicgmqhwb0v5a0zihdbfb605607wwf2rars8dq6hypppbhgh")))

(define-public crate-gkl-0.1.1 (c (n "gkl") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0d5wvw3f47g9rpj5x637lbbwz1jq1cb5hixpka7rjgn6szaxdxfy")))

