(define-module (crates-io #{3}# g gpr) #:use-module (crates-io))

(define-public crate-gpr-0.1.0 (c (n "gpr") (v "0.1.0") (d (list (d (n "enum-display-derive") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 1)) (d (n "libloading") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lsc0ar771pwjrv0jbs6cjw0fza22fvnd575b7q9yssg6dwdpahs")))

(define-public crate-gpr-0.1.1 (c (n "gpr") (v "0.1.1") (d (list (d (n "enum-display-derive") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0sda0zcs5gdawrzazp57cjjzq1zhgcvg3fprim9dhy8cgqvcnwp1")))

(define-public crate-gpr-0.1.2 (c (n "gpr") (v "0.1.2") (d (list (d (n "enum-display-derive") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.14") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0lblx4ncp6gkmclb900zp4r3lmnnk5kvz72pkvd1zh0yagszar0d")))

(define-public crate-gpr-0.1.3 (c (n "gpr") (v "0.1.3") (d (list (d (n "enum-display-derive") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.18") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libloading") (r "^0.8") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jxr4ff97vq9cbx32didyhiakniqn5cnl5w9prynrn40axihwdr0")))

