(define-module (crates-io #{3}# g glk) #:use-module (crates-io))

(define-public crate-glk-0.1.0 (c (n "glk") (v "0.1.0") (d (list (d (n "glk-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0g17pzp54s3hihls28lkqm1f9iykphvqkidg3m61d8qjf9wzwgn2") (y #t)))

(define-public crate-glk-0.1.1 (c (n "glk") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.11") (d #t) (k 2)) (d (n "glk-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "glulxe") (r "^0.1") (d #t) (k 2)))) (h "1j57vvn2a82r8ci5njv14sfcixcdcnhbsa2ndpmkj8xz976b0wcx")))

(define-public crate-glk-0.2.0 (c (n "glk") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.12") (d #t) (k 2)) (d (n "glk-sys") (r "^0.2") (d #t) (k 0)) (d (n "glulxe") (r "^0.2") (d #t) (k 2)))) (h "0m9j14dr4abg049p4nzwn4kbg9d6m22mx03h9c4n5pr8isvb5mk7")))

