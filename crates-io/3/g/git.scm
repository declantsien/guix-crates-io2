(define-module (crates-io #{3}# g git) #:use-module (crates-io))

(define-public crate-git-0.0.1 (c (n "git") (v "0.0.1") (h "1dpjf7jid4rhfadvdqzsi76qnmmwk6l8bj1n5sx5lzc60f84gy5r")))

(define-public crate-git-0.0.2 (c (n "git") (v "0.0.2") (d (list (d (n "tape") (r "^0.0.3") (d #t) (k 0)))) (h "00hag66w554qp4bxbxv4wg0f3v807im53qr1h0k58gd9xn4fzkzw")))

(define-public crate-git-0.0.3 (c (n "git") (v "0.0.3") (d (list (d (n "tape") (r "^0.0.4") (d #t) (k 2)))) (h "19rbija9iizprsrp2vbqqzr6852zc98327p3arm1l0hl9h2nwp9r")))

(define-public crate-git-0.0.4 (c (n "git") (v "0.0.4") (d (list (d (n "tape") (r "^0.0.4") (d #t) (k 2)))) (h "00dxxjc6l0arzkzbd9w9b0d4pqy6kdxj3qf5rk1waqrwgw66zdc7")))

(define-public crate-git-0.0.5 (c (n "git") (v "0.0.5") (d (list (d (n "tape") (r "^0.0.4") (d #t) (k 2)))) (h "0a8dm7l6jj0qbam9pa9idvc8brws6iginshxwgckky1vcnn81ri2")))

(define-public crate-git-0.0.6 (c (n "git") (v "0.0.6") (d (list (d (n "tape") (r "^0.0.4") (d #t) (k 2)))) (h "1l00zh06r4g2xaqpsxjz3xf3vi84s7rjj8fzlffdq5dqz18mj1c8")))

(define-public crate-git-0.0.7 (c (n "git") (v "0.0.7") (d (list (d (n "tape") (r "^0.0.4") (d #t) (k 2)))) (h "1wnss4kfgpg3f8z3p5fp9b9rzvhf48i2vysygh51lv56l98012qx")))

(define-public crate-git-0.0.8 (c (n "git") (v "0.0.8") (d (list (d (n "tape") (r "^0.0.5") (d #t) (k 2)))) (h "0p34x6s9nd6d25qkg6c68warwwprybhc0pscmr1shw0wakl9q5s2")))

(define-public crate-git-0.0.9 (c (n "git") (v "0.0.9") (d (list (d (n "tape") (r "^0.0.6") (d #t) (k 2)))) (h "1r016qgbp4hpfb22vs9arvq0c8azqc0hwihr3835arnvlwkl6iwi")))

(define-public crate-git-0.0.10 (c (n "git") (v "0.0.10") (d (list (d (n "tape") (r "^0.0.7") (d #t) (k 2)))) (h "0pbc26x07w8053rnwcgjq0assg8q054d80nr0339vd8hbk07qybw")))

(define-public crate-git-0.0.11 (c (n "git") (v "0.0.11") (d (list (d (n "tape") (r "^0.0.7") (d #t) (k 2)))) (h "160zmkkb4vpm2zr2x747zpfmsawksrvbx3cvd5nfb4kn7bfdc6mq")))

(define-public crate-git-0.0.12 (c (n "git") (v "0.0.12") (d (list (d (n "tape") (r "^0.0.7") (d #t) (k 2)))) (h "0p5w73hc9pma8mnx9pslhq2zhyqky6m06xagkkm614q2l9qzz08h")))

(define-public crate-git-0.0.13 (c (n "git") (v "0.0.13") (d (list (d (n "tape") (r "^0.0.7") (d #t) (k 2)))) (h "15hz81536m708mn1za0s628ifl1z3y3jh9z1n0xyvj8hwsji6q4s")))

(define-public crate-git-0.0.14 (c (n "git") (v "0.0.14") (d (list (d (n "tape") (r "^0.0.8") (d #t) (k 2)))) (h "0synafkvz11n3j50kva5j407f4sj228ww20hdp94zgy1p87psbyv")))

(define-public crate-git-0.0.15 (c (n "git") (v "0.0.15") (d (list (d (n "tape") (r "^0.0.9") (d #t) (k 2)))) (h "1ckxdq578m4z2lmp66kzdc8g676dhf8g522c71h01b4hi6y08qqm")))

(define-public crate-git-0.1.0 (c (n "git") (v "0.1.0") (d (list (d (n "tape") (r "^0.1") (d #t) (k 2)))) (h "1j8mpvkp91hi0ns8n8lhi9p18y9axh3zajii36slrj4782gpmyxa")))

(define-public crate-git-0.1.1 (c (n "git") (v "0.1.1") (d (list (d (n "tape") (r "^0.1") (d #t) (k 2)))) (h "07wwyckmap7yj54kqiwyk3yxfml231ls5d0ai8dvhvjfylbmqc5s")))

(define-public crate-git-0.1.2 (c (n "git") (v "0.1.2") (d (list (d (n "tape") (r "^0.1") (d #t) (k 2)))) (h "1a3ynk4qha929rw45wp0rd3q59vqh02snmnh470w01lqa54yvlh1")))

(define-public crate-git-0.1.3 (c (n "git") (v "0.1.3") (d (list (d (n "tape") (r "^0.1") (d #t) (k 2)))) (h "10w8q891nx01x5dl90iak6fhmb9fksxclwhs2ahq4cm1ddfisbdh")))

(define-public crate-git-0.1.4 (c (n "git") (v "0.1.4") (d (list (d (n "tape") (r "^0.1") (d #t) (k 2)))) (h "1v6in5f3qgyf2n9y49ypwyv3iip3a0cik9nd894l8ph2i008z154")))

(define-public crate-git-0.1.5 (c (n "git") (v "0.1.5") (d (list (d (n "tape") (r "^0.1") (d #t) (k 2)))) (h "0f00h4j7l3ka52sszll62423rp75nf172r5vzmipgymj5f2m7v53")))

(define-public crate-git-0.1.6 (c (n "git") (v "0.1.6") (d (list (d (n "tape") (r "^0.1") (d #t) (k 2)))) (h "0mq4i5d4wl3nqz8gakncx83zlqdygk823ksid3k88137md85ds40")))

(define-public crate-git-0.1.7 (c (n "git") (v "0.1.7") (d (list (d (n "tape") (r "^0.1") (d #t) (k 2)))) (h "0p8nqqjsm4ykdvmac19as2pnf8kxqq7qv9zqrd4vj1118lysf9wx")))

(define-public crate-git-0.1.8 (c (n "git") (v "0.1.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "^0.1") (d #t) (k 2)))) (h "0x785l1lha26gqns5d08sbmf1aa1x8l0ajz2q8mbqnx30baw9xdn")))

(define-public crate-git-0.1.9 (c (n "git") (v "0.1.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0x8ddxvslaxg3mhgg7xzq0xzj6vgg9fqa5j63w4gln57k8wcyn79")))

(define-public crate-git-0.1.10 (c (n "git") (v "0.1.10") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0hajgc2vwzxbrf56vkcm5lmsmf6vm2j6aw5qbngf9dksv5hmqdby")))

(define-public crate-git-0.1.11 (c (n "git") (v "0.1.11") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1yd1w6v3zapqikvi9v41x5n76rm143jx8fmxr95rzvrbblhblj1q")))

(define-public crate-git-0.1.12 (c (n "git") (v "0.1.12") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "15f4g007jvvm0gq4hyyc18xwyvymimdj20hmikh2rqm48gsliywg")))

(define-public crate-git-0.1.13 (c (n "git") (v "0.1.13") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1dp0111f55i2db8g6g8vavdamw9g53w189agpxqj3s55xvr13v9z")))

(define-public crate-git-0.1.15 (c (n "git") (v "0.1.15") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "122g0gcwbm38nigx35876kmxx9r04dixklwnjcyhg9wrma1y59a5")))

(define-public crate-git-0.1.16 (c (n "git") (v "0.1.16") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0465fgw7n78scm4l4myx6p5d981c9q70b8fx8fl6yj3l1s05zicc")))

(define-public crate-git-0.1.17 (c (n "git") (v "0.1.17") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0rizsi8v7lk6n5lih2zxf6qxr5v8z24w87m9i5k4qh0rzq4n9x88")))

(define-public crate-git-0.2.0 (c (n "git") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "0ypkdsiz8gl100h36cf1y0pfa01jpjn9d69wbw7l75bwfkzxp29n")))

(define-public crate-git-0.2.1 (c (n "git") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "13vc1bhmj5s45jbjmsbak390qx7bn36pqqy3ag9239km1xlasqpw")))

(define-public crate-git-0.2.2 (c (n "git") (v "0.2.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tape") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1rjhcyqlsnmcdvyadhxgsg1h7ffpsa8pwsjzkydcnrrcs5r6mlvh")))

(define-public crate-git-0.3.0 (c (n "git") (v "0.3.0") (h "0xaa0b17fm2q1zwyy8r102zd262izda3yjkx6mvxrkbgkgv57wfz")))

