(define-module (crates-io #{3}# g gag) #:use-module (crates-io))

(define-public crate-gag-0.1.0 (c (n "gag") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)))) (h "17ff1m2qfg14a149k3b3ra0fhlh0ryibfkzxmfnjbrz826p4kz1p")))

(define-public crate-gag-0.1.1 (c (n "gag") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)))) (h "0q07xsd8p2a9xy135bq4b8flglsjkyninm90630w6riqg3y7q9jh")))

(define-public crate-gag-0.1.2 (c (n "gag") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)))) (h "1l77q93igqf7cyh5c3flyaa1bgjxdz6abadwlfb11pxgacjcyp8k")))

(define-public crate-gag-0.1.3 (c (n "gag") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)))) (h "0d1x0x0kpx1ahpkwd67kdg80jbpzfas0sl8ws5ih01zy005dm9vw")))

(define-public crate-gag-0.1.4 (c (n "gag") (v "0.1.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 0)))) (h "1324qfnipsv1la0na8yrz486lw8339wgjv1055643ksxary0wmv5")))

(define-public crate-gag-0.1.5 (c (n "gag") (v "0.1.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 0)))) (h "0khh5pdacv528i9hnyw9ar985zwv8wclsys9a6q4nnb1l7yxjc7x")))

(define-public crate-gag-0.1.6 (c (n "gag") (v "0.1.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "tempfile") (r "*") (d #t) (k 0)))) (h "0ywbadw8apryzizckq3885ibrl04y7hz0lfw05hd9abhbbj0xix6")))

(define-public crate-gag-0.1.7 (c (n "gag") (v "0.1.7") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^1.0") (d #t) (k 0)))) (h "0glnbpv6q11d7xz7rklzzbz44921718jdb14f9flnj9lh1z51wd6")))

(define-public crate-gag-0.1.8 (c (n "gag") (v "0.1.8") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^2.0") (d #t) (k 0)))) (h "15cf2sxkb5sp1bmllccalkk2mncqvd3sxxmh83dgmmjddpmcwy3n")))

(define-public crate-gag-0.1.9 (c (n "gag") (v "0.1.9") (d (list (d (n "lazy_static") (r "*") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^2.0") (d #t) (k 0)))) (h "0zml54sqfiqzmqkcz8qhc4dhw53wxj5dwwvy6wb5j1qqvb0ga1n6")))

(define-public crate-gag-0.1.10 (c (n "gag") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "1d874gmyhyqbb78k6mkk9p0sd21n5vwd5w88m2nmzp3m6bsvkh4c")))

(define-public crate-gag-1.0.0 (c (n "gag") (v "1.0.0") (d (list (d (n "dirs") (r "^3.0.2") (d #t) (k 2)) (d (n "filedescriptor") (r "^0.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)))) (h "0wjr02svx7jir7b7r69lpfh3assasmqsz4vivzzzpsb677hvw4x7")))

