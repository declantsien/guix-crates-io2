(define-module (crates-io #{3}# g gaf) #:use-module (crates-io))

(define-public crate-gaf-0.2.0 (c (n "gaf") (v "0.2.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "1ab44zmcq2898k6md8vw4z52rbd5na7nla3dhi1qbpjxgljzrsaf")))

(define-public crate-gaf-0.3.0 (c (n "gaf") (v "0.3.0") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "0pk4mqq6d34dnfzc91wxj4m0hgcnsln9wa5dca9z24yvwms7yf22")))

(define-public crate-gaf-0.3.1 (c (n "gaf") (v "0.3.1") (d (list (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)))) (h "0pkmyp3jwfmkf3v63gxzc57wx7805cn9b8qa3lcw15arabx3mkmd")))

