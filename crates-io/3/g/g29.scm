(define-module (crates-io #{3}# g g29) #:use-module (crates-io))

(define-public crate-g29-0.1.0 (c (n "g29") (v "0.1.0") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)))) (h "1wgdmg0kq1vysqhdpd8037yn4dhqr239qcvqsxd3lr0x4w05vpzl")))

(define-public crate-g29-0.1.1 (c (n "g29") (v "0.1.1") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)))) (h "1c7425hijgqc8y6vfr1v93mi93q0figsz21bn6lhkhagb71bmhnm")))

(define-public crate-g29-0.2.1 (c (n "g29") (v "0.2.1") (d (list (d (n "hidapi") (r "^2.5.0") (d #t) (k 0)))) (h "1kd1yp44lwz99x65hp5dvi572mybngh3gnrjvpwavc14r5mwicnw")))

