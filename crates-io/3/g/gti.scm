(define-module (crates-io #{3}# g gti) #:use-module (crates-io))

(define-public crate-gti-0.1.0 (c (n "gti") (v "0.1.0") (h "0lh1bhs1miwy7j6r6is3g3773sj3v3c9j4favi7isrxm5yy4rda8") (y #t)))

(define-public crate-gti-0.1.1 (c (n "gti") (v "0.1.1") (h "1rgjh7qfd838fd4p1rbrhlqlpwxxxh2nh8d06rp0c750z69yvfkg")))

