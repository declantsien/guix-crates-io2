(define-module (crates-io #{3}# g ghc) #:use-module (crates-io))

(define-public crate-ghc-0.1.0 (c (n "ghc") (v "0.1.0") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0kdh12na7rl3ryjpdx2kksn1wqmx4kwrplpkppxaxyn49vmxmq08")))

(define-public crate-ghc-0.1.1 (c (n "ghc") (v "0.1.1") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "07km39pfijwk711f022kvwjygr399dz62799fa1h25q28xfwjz7a")))

(define-public crate-ghc-0.1.2 (c (n "ghc") (v "0.1.2") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1gw74x7l6gycz229r6d9i362jvzkbj84gyf17by7j7vy37rmbzbm")))

(define-public crate-ghc-0.1.3 (c (n "ghc") (v "0.1.3") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "14bnn1k21a8m9lm4wfd8qbscinb9952prn4sccpr520vylh8s5j7")))

(define-public crate-ghc-0.1.4 (c (n "ghc") (v "0.1.4") (d (list (d (n "argh") (r "^0.1.9") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "useful_macro") (r "^0.1.37") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "15l89ip45029w15740jfrijsn7jdrckr9j4lw4bjpqz77gniyga9")))

(define-public crate-ghc-0.1.5 (c (n "ghc") (v "0.1.5") (d (list (d (n "cok") (r "^0.1.7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0lffdvr76gw2m409c59jz1crv3syz91grb52m57sa760855qf8ls")))

(define-public crate-ghc-0.1.6 (c (n "ghc") (v "0.1.6") (d (list (d (n "cok") (r "^0.1.7") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "07bdz2lb0cdgsq0am1b4anb2dkfjvkd96ynslfg5c3qncnwj924p")))

(define-public crate-ghc-0.1.8 (c (n "ghc") (v "0.1.8") (d (list (d (n "cok") (r "^0.1.9") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0qpa86d4hwakwyb51g8zhxw2n3qnvb34a7lfqc4ip602g5baa6ci")))

(define-public crate-ghc-0.1.9 (c (n "ghc") (v "0.1.9") (d (list (d (n "cok") (r "^0.1.9") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1zsif2g5wb49kjbyf4mipi94n40wmjjrjzjhghlbnvh524wfklw3")))

(define-public crate-ghc-0.1.10 (c (n "ghc") (v "0.1.10") (d (list (d (n "cok") (r "^0.1.9") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0zdxppgzk9il41xnx7n9c5zkqzg8pnyf8nwhd219ijh53n39qjds")))

(define-public crate-ghc-0.1.11 (c (n "ghc") (v "0.1.11") (d (list (d (n "cok") (r "^0.1.9") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1z6cqy72cncv9afcgv8lrw378yfhkm7a0z0y6frqgkvq70ajv78v")))

(define-public crate-ghc-0.1.12 (c (n "ghc") (v "0.1.12") (d (list (d (n "cok") (r "^0.1.9") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1hk8fawvqp5rlyvnvcs1nx3arac5yb4j8fakr8cjvjf6zngvsdig")))

(define-public crate-ghc-0.1.13 (c (n "ghc") (v "0.1.13") (d (list (d (n "cok") (r "^0.1.9") (d #t) (k 0)) (d (n "easy_file") (r "^0.1.20") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0zwh26a7rsmwr1llw6xg8a9vzxmvhbqaxskhqn124rj89qlbafn1")))

