(define-module (crates-io #{3}# g gym) #:use-module (crates-io))

(define-public crate-gym-0.1.0 (c (n "gym") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1mawwbyaz1hiwm27cjslh5zh3q1nfnhkw3xyx0p2r23pf3af067v")))

(define-public crate-gym-1.0.0 (c (n "gym") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)))) (h "1zihdkcbzp8qns70mlf33rvp03ic7qx5qgx2ar91207r7p9l4371")))

(define-public crate-gym-2.0.0-beta (c (n "gym") (v "2.0.0-beta") (d (list (d (n "cpython") (r "^0.3.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1kyggrgy5j4wpha4mx149bxi9qcch83i8f6994qs4lx4bhjs0rdj")))

(define-public crate-gym-2.0.2-beta (c (n "gym") (v "2.0.2-beta") (d (list (d (n "cpython") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1k9q0jw440hwd0qz0h8h9bl5wwavcgywiz9wx9x9p3k2x5jsr6k2")))

(define-public crate-gym-2.1.0 (c (n "gym") (v "2.1.0") (d (list (d (n "cpython") (r "^0.3.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1whhj7mv0ybb724ya02swixggr3bcc01aqw66ga1nxy4mfs0f2wy")))

(define-public crate-gym-2.2.0 (c (n "gym") (v "2.2.0") (d (list (d (n "cpython") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1v3q17jlwv8kxsz4ayq5npq8dcb3ivrjpmrddxqkasgxgdzb9jp8")))

(define-public crate-gym-2.2.1 (c (n "gym") (v "2.2.1") (d (list (d (n "cpython") (r "^0.4.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)))) (h "1p6shfhv75gh901ax8m62vyck19zs2yv7djinh7flfzkrb6hgx6j")))

