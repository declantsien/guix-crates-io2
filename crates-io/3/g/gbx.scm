(define-module (crates-io #{3}# g gbx) #:use-module (crates-io))

(define-public crate-gbx-1.0.0 (c (n "gbx") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xp6qdzl0784qi1hvrzkggv05lxlzyy8qvj44z7xgs2c7bnq8z97")))

(define-public crate-gbx-1.0.1 (c (n "gbx") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1v6pblaaf4hnc3nkdd5wzq7kxph0yacfx0x04dcrspr5chcpcncr")))

