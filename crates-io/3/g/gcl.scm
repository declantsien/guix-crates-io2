(define-module (crates-io #{3}# g gcl) #:use-module (crates-io))

(define-public crate-gcl-0.1.0 (c (n "gcl") (v "0.1.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.3") (d #t) (k 0)) (d (n "rustypath") (r "^0.1.1") (d #t) (k 0)))) (h "1nlw0w0qnhhk0spavap1bc83wliqqwq6l41yqrzcnlair8cm3rgj")))

