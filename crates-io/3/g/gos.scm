(define-module (crates-io #{3}# g gos) #:use-module (crates-io))

(define-public crate-gos-0.1.0 (c (n "gos") (v "0.1.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "16zyj9pw6v3yc3f7cx56rd9khj10vcxq6ggimip49jqxipy46yav")))

(define-public crate-gos-0.1.1 (c (n "gos") (v "0.1.1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1fppjzvv53jf6k37l7f97gkyzbap631i68bxwbl5vdnml0kwsl9c")))

(define-public crate-gos-0.1.1-1 (c (n "gos") (v "0.1.1-1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1y0zk1syl89n4i0knfwd87dx6ysnr4wrgix6c2y2650mrz377ahw")))

(define-public crate-gos-0.1.2 (c (n "gos") (v "0.1.2") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1wbgrn851hj97s08fsrq1cgpga1k2m4w4p0bfkvg7x4lzs0yq1yw")))

(define-public crate-gos-0.1.3 (c (n "gos") (v "0.1.3") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "16ykzm8j1mbgc8mp8278rpn29qffbyyj2qvsqsrfya4rrnf9f0n3")))

(define-public crate-gos-0.1.4 (c (n "gos") (v "0.1.4") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.29") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0ndda445ak9mqf88j1cbj522lssf1jknar9r0n1np7m1wvykzqx2")))

