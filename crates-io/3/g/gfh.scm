(define-module (crates-io #{3}# g gfh) #:use-module (crates-io))

(define-public crate-gfh-0.0.1 (c (n "gfh") (v "0.0.1") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctap-hid-fido2") (r "^3.4.2") (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)) (d (n "yubikey_api") (r "^0.6.0") (d #t) (k 0) (p "yubikey")))) (h "1nc6xjlnwkwkgjkfr3qlrdgs2xif5mbwfdypbkiv2m513maijk5x")))

(define-public crate-gfh-0.0.2 (c (n "gfh") (v "0.0.2") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctap-hid-fido2") (r "^3.4.2") (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)) (d (n "yubikey_api") (r "^0.6.0") (d #t) (k 0) (p "yubikey")))) (h "1rqrvqp17blypzzmqmjj3jbm7sdbiw8h3c3kk82cgw9xzd7by4gk")))

(define-public crate-gfh-0.0.3 (c (n "gfh") (v "0.0.3") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctap-hid-fido2") (r "^3.4.2") (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)) (d (n "yubikey_api") (r "^0.6.0") (d #t) (k 0) (p "yubikey")))) (h "05nvfd1hial7g329vzpcbaagyzyq40lh6332c5pk8p4c9229vbxv")))

(define-public crate-gfh-0.0.4 (c (n "gfh") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctap-hid-fido2") (r "^3.4.2") (d #t) (k 0)) (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "shellexpand") (r "^2.1.2") (d #t) (k 0)) (d (n "yubikey_api") (r "^0.6.0") (d #t) (k 0) (p "yubikey")))) (h "092sfpvwx5xh6p9k6ixsjqr31i3cx0312nw65rd95kdbmglx1nac")))

