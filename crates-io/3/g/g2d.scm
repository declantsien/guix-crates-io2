(define-module (crates-io #{3}# g g2d) #:use-module (crates-io))

(define-public crate-g2d-0.0.0 (c (n "g2d") (v "0.0.0") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.5.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (d #t) (k 2)) (d (n "wgpu") (r "^0.18.0") (d #t) (k 0)))) (h "0b2spjw5dxjixgdii7zpvifz05bkb992bc9k0mfl6w1y3yc6ank8")))

(define-public crate-g2d-0.0.1 (c (n "g2d") (v "0.0.1") (d (list (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-intrusive") (r "^0.5.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (d #t) (k 2)) (d (n "wgpu") (r "^0.18.0") (d #t) (k 0)))) (h "0lk10r1f86bg3q33h3f9d3np509mbmqjkfls0x6s194128gsqzlr")))

(define-public crate-g2d-0.0.2 (c (n "g2d") (v "0.0.2") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.5.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (d #t) (k 2)) (d (n "wgpu") (r "^0.18") (d #t) (k 0)))) (h "0h71f8bfafi1rqx33ykkafg0dlxq1drwhp8qwy5qyb5fkb1a9dr0")))

(define-public crate-g2d-0.0.3 (c (n "g2d") (v "0.0.3") (d (list (d (n "futures") (r "^0.3.29") (d #t) (k 2)) (d (n "futures-intrusive") (r "^0.5.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (f (quote ("png"))) (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5.2") (d #t) (k 0)) (d (n "wgpu") (r "^0.18") (d #t) (k 0)))) (h "0ry025qcqr6i1g0jqaq1bjga53jz6w393hmcz3yh6lks02m66qjl")))

