(define-module (crates-io #{3}# g gon) #:use-module (crates-io))

(define-public crate-gon-0.1.0 (c (n "gon") (v "0.1.0") (d (list (d (n "gee") (r "^0.3.0") (f (quote ("euclid"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "lyon_tessellation") (r "^0.17.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1r2k9lgpq1463r6j0mmsb4xirphvi176m9lwspp9g33rjl35ympg") (f (quote (("serde" "gee/serde"))))))

