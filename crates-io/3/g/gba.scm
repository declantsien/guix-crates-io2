(define-module (crates-io #{3}# g gba) #:use-module (crates-io))

(define-public crate-gba-0.1.0 (c (n "gba") (v "0.1.0") (d (list (d (n "gba-proc-macro") (r "^0.1.1") (d #t) (k 0)))) (h "1z7pazijkkiks98gdqj99fhy1gn2aqqvqicancqyzbbc59piw502")))

(define-public crate-gba-0.2.0 (c (n "gba") (v "0.2.0") (d (list (d (n "gba-proc-macro") (r "^0.1.1") (d #t) (k 0)))) (h "1g5x80h3ig2j20ivf809ywqr4w33693x3nfwk6811c19qywvzash")))

(define-public crate-gba-0.3.0 (c (n "gba") (v "0.3.0") (d (list (d (n "gba-proc-macro") (r "^0.5") (d #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1xiqj1k2bmhgfy5rvx90adxck47rv19g16lpj34fvc34y1iqdb9r")))

(define-public crate-gba-0.3.1 (c (n "gba") (v "0.3.1") (d (list (d (n "gba-proc-macro") (r "^0.5") (d #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "10vzcg645izzs4map0n2i45h2dgq21nxjxpn3yaabsnrp35w07gp")))

(define-public crate-gba-0.3.2 (c (n "gba") (v "0.3.2") (d (list (d (n "gba-proc-macro") (r "^0.5") (d #t) (k 0)) (d (n "typenum") (r "^1.10") (d #t) (k 0)))) (h "1j9z0wr0xm01amklq7r1629bcd8g4gbwl1f52qs69am8z8lgwm4i")))

(define-public crate-gba-0.4.0 (c (n "gba") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "voladdress") (r "^0.4") (d #t) (k 0)))) (h "02mxg2qja3kpnychnrc3gh56373ncvl7mb9sszlraf3jlrgl8g8y") (f (quote (("serial" "embedded-hal" "nb") ("default" "serial"))))))

(define-public crate-gba-0.5.0 (c (n "gba") (v "0.5.0") (d (list (d (n "embedded-hal") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "voladdress") (r "^0.4") (d #t) (k 0)))) (h "19qi2hgc2kfpasrccca3hnw2rwajikrlxibxsmvjx7j06d8wf6z0") (f (quote (("serial" "embedded-hal" "nb") ("default"))))))

(define-public crate-gba-0.5.1 (c (n "gba") (v "0.5.1") (d (list (d (n "embedded-hal") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "voladdress") (r "^0.4") (d #t) (k 0)))) (h "0iydln2qnnsg48q00j7p3v0gyk9aqnajj1p3vabjha2xpkamyf15") (f (quote (("serial" "embedded-hal" "nb") ("default"))))))

(define-public crate-gba-0.5.2 (c (n "gba") (v "0.5.2") (d (list (d (n "embedded-hal") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "voladdress") (r "^0.4") (d #t) (k 0)))) (h "15rkp8pwlsfj2m3llrhs1pkyq4prfjda3hda4978h5qyyqh9jzky") (f (quote (("serial" "embedded-hal" "nb") ("default"))))))

(define-public crate-gba-0.5.3 (c (n "gba") (v "0.5.3") (d (list (d (n "embedded-hal") (r "^0.2.4") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (o #t) (d #t) (k 0)) (d (n "voladdress") (r "^0.4") (d #t) (k 0)))) (h "0f3pmymqbcg670kpj54ir3kknavk6rhz4p3g7h46a770haj0jkrj") (f (quote (("serial" "embedded-hal" "nb") ("default"))))))

(define-public crate-gba-0.7.0 (c (n "gba") (v "0.7.0") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.0.2") (d #t) (k 0)))) (h "05x3shwr9ywfh9qpw3j8dxvd9hfk6bjajnmja84w5j7h2zlnf056")))

(define-public crate-gba-0.7.1 (c (n "gba") (v "0.7.1") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.0.2") (d #t) (k 0)))) (h "0kl4bb3rpj2ki10s2xl014y1w8y83silhi2bdmca3fasxvjh4p76")))

(define-public crate-gba-0.7.2 (c (n "gba") (v "0.7.2") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.0.2") (d #t) (k 0)))) (h "13ynlrp4ky68sv7157g7hji07h169kddw50b2p0205b8ibz38gqy")))

(define-public crate-gba-0.7.3 (c (n "gba") (v "0.7.3") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.0.2") (d #t) (k 0)))) (h "108vbfbwh7ak2vi5k50v81hzm5d3mqyd18ha6qjx1c2cxgcgx9rj")))

(define-public crate-gba-0.7.4 (c (n "gba") (v "0.7.4") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.0.2") (d #t) (k 0)))) (h "1wz1g15ml2s9zss01j75nazg66sc81wn1m8n55d2gv8ahadcqbry")))

(define-public crate-gba-0.8.0 (c (n "gba") (v "0.8.0") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.0.2") (d #t) (k 0)))) (h "1j865y64w0c9ajgjgiphrhy2a84cfhj488fw9afwj6hrin1c542c")))

(define-public crate-gba-0.9.0 (c (n "gba") (v "0.9.0") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.2.1") (f (quote ("experimental_volregion"))) (d #t) (k 0)))) (h "0aq46jpvf8agbn7ba2p18cc564dk4xgi7ckm4ybn9s2aw8r3kjk1")))

(define-public crate-gba-0.9.1 (c (n "gba") (v "0.9.1") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.2.1") (f (quote ("experimental_volregion"))) (d #t) (k 0)))) (h "01dlgsr4017kz29zmd68xiiis1fq4wyj59858r80cyw54kvscprp")))

(define-public crate-gba-0.9.2 (c (n "gba") (v "0.9.2") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.2.1") (f (quote ("experimental_volregion"))) (d #t) (k 0)))) (h "0sdv4zibl6kbqm56xzkvjhs874qdjg5niazw8byk673dph94mgxn")))

(define-public crate-gba-0.9.3 (c (n "gba") (v "0.9.3") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.2.1") (f (quote ("experimental_volregion"))) (d #t) (k 0)))) (h "04ay0k76zrzf9g61frqwald9fc1f29mpjh596vxyl02y745s2igf")))

(define-public crate-gba-0.10.0 (c (n "gba") (v "0.10.0") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "voladdress") (r "^1.2.1") (f (quote ("experimental_volregion"))) (d #t) (k 0)))) (h "1j266yhh4331i75x6wq0vqn6an1yq1cilhs7hpb8vg76a74h6kck") (f (quote (("track_caller") ("default" "track_caller"))))))

(define-public crate-gba-0.10.1 (c (n "gba") (v "0.10.1") (d (list (d (n "bitfrob") (r "^0.2.3") (d #t) (k 0)) (d (n "bracer") (r "^0.1.2") (d #t) (k 0)) (d (n "voladdress") (r "^1.2.1") (f (quote ("experimental_volregion"))) (d #t) (k 0)))) (h "19mkvvcfm4afcll64wa54wg974iiaxmigsgh544g0mnzkmvp0kh9") (f (quote (("track_caller") ("default" "track_caller"))))))

(define-public crate-gba-0.10.2 (c (n "gba") (v "0.10.2") (d (list (d (n "bitfrob") (r "^1") (d #t) (k 0)) (d (n "bracer") (r "^0.1.2") (d #t) (k 0)) (d (n "voladdress") (r "^1.3.0") (d #t) (k 0)))) (h "071x11mvllvfjvbq92d5vnm56x67z0jfpxxkfqsp8aqglci3dicj") (f (quote (("track_caller") ("default" "track_caller"))))))

(define-public crate-gba-0.11.0 (c (n "gba") (v "0.11.0") (d (list (d (n "bitfrob") (r "^1") (d #t) (k 0)) (d (n "bracer") (r "^0.1.2") (d #t) (k 0)) (d (n "voladdress") (r "^1.3.0") (d #t) (k 0)))) (h "1rqzssngdx5rd9ymn7d5q2i6lq5nllxadm6daz6dx44b6mac7m1k") (f (quote (("track_caller") ("default" "track_caller"))))))

(define-public crate-gba-0.11.1 (c (n "gba") (v "0.11.1") (d (list (d (n "bitfrob") (r "^1") (d #t) (k 0)) (d (n "bracer") (r "^0.1.2") (d #t) (k 0)) (d (n "voladdress") (r "^1.3.0") (d #t) (k 0)))) (h "16xhwcq08g6q4amj8r9bi34av1187j259647hx5nymhrjg7kjani") (f (quote (("track_caller") ("default" "track_caller"))))))

(define-public crate-gba-0.11.2 (c (n "gba") (v "0.11.2") (d (list (d (n "bitfrob") (r "^1") (d #t) (k 0)) (d (n "bracer") (r "^0.1.2") (d #t) (k 0)) (d (n "voladdress") (r "^1.3.0") (d #t) (k 0)))) (h "194vz7zrgysm7gv5ng6mz84nwqw5j5q3ks3mzd8q6ypfj91xbpnf") (f (quote (("track_caller") ("default" "track_caller"))))))

(define-public crate-gba-0.11.3 (c (n "gba") (v "0.11.3") (d (list (d (n "bitfrob") (r "^1") (d #t) (k 0)) (d (n "bracer") (r "^0.1.2") (d #t) (k 0)) (d (n "voladdress") (r "^1.3.0") (d #t) (k 0)))) (h "1mks0gsjddwjkv3shqh4gzpi45ckd1fwal4q1pc264rcw4fhvfbs") (f (quote (("track_caller") ("default" "track_caller"))))))

(define-public crate-gba-0.11.4 (c (n "gba") (v "0.11.4") (d (list (d (n "bitfrob") (r "^1") (d #t) (k 0)) (d (n "bracer") (r "^0.1.2") (d #t) (k 0)) (d (n "voladdress") (r "^1.3.0") (d #t) (k 0)))) (h "18frxnl325nw7sk5wz7fhfzqg3b0vkvgwkxnjmzznv4c6xr23nhw") (f (quote (("track_caller") ("default" "track_caller"))))))

(define-public crate-gba-0.11.5 (c (n "gba") (v "0.11.5") (d (list (d (n "bitfrob") (r "^1") (d #t) (k 0)) (d (n "bracer") (r "^0.1.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-bool"))) (o #t) (d #t) (k 0)) (d (n "voladdress") (r "^1.3.0") (d #t) (k 0)))) (h "085h8yxzk69hb7cchibrkh51v166ffid9lfb9sjkb6z93d6c14pz") (f (quote (("track_caller") ("default" "track_caller"))))))

(define-public crate-gba-0.11.6 (c (n "gba") (v "0.11.6") (d (list (d (n "bitfrob") (r "^1") (d #t) (k 0)) (d (n "bracer") (r "^0.1.2") (d #t) (k 0)) (d (n "critical-section") (r "^1.1.2") (f (quote ("restore-state-bool"))) (o #t) (d #t) (k 0)) (d (n "voladdress") (r "^1.3.0") (d #t) (k 0)))) (h "0djrshpfmxswqnz58040fndjl8v9jrlcfb5r6jf95ypcybld6z5b") (f (quote (("track_caller") ("on_gba") ("default" "track_caller" "on_gba"))))))

