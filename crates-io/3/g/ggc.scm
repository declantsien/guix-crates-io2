(define-module (crates-io #{3}# g ggc) #:use-module (crates-io))

(define-public crate-ggc-0.0.1 (c (n "ggc") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.9.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1cjdl40pf7cq52ni0gj22zxril0w95bafai4hg1v5lwa35h4mvmz")))

