(define-module (crates-io #{3}# g gsm) #:use-module (crates-io))

(define-public crate-gsm-0.1.0 (c (n "gsm") (v "0.1.0") (h "16nn3pfg165rb7k5shcnspcs3zzzpr9c5ivd4jsjb22wkjv7jf2l")))

(define-public crate-gsm-0.1.1 (c (n "gsm") (v "0.1.1") (h "00863v9mys94bqzs40gd91ga4yv276dq5jcm7bdapx769l27736i")))

(define-public crate-gsm-0.2.0 (c (n "gsm") (v "0.2.0") (h "0d2psjv5ry3w3ymv8n8fqvcv6fjzvgp2chfj984rd468pkwwczb9")))

(define-public crate-gsm-0.3.0 (c (n "gsm") (v "0.3.0") (h "08jl6jvmzm8xfx6yb5g01bjxayvs9b18mw9f1aww7l5p4971wfsf")))

(define-public crate-gsm-0.4.0 (c (n "gsm") (v "0.4.0") (h "0kd7szzc67sngyyzddxz3wx9kdfzywbinzvk0l0jxq860rm0hlb8")))

(define-public crate-gsm-0.5.0 (c (n "gsm") (v "0.5.0") (h "13ykcwcp1rrns26msagahhgd1zqwfg187la5h8w8dagwdjjqkvsz")))

(define-public crate-gsm-1.0.0 (c (n "gsm") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "05igm98pcczkrp3k7h6mzfm0cbsgvcjjv2x7sbzs6by1fk8rq5y3")))

(define-public crate-gsm-1.1.0 (c (n "gsm") (v "1.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0xvpm9xdrx491rgi8gwgz2i4d57wv1aqb8a8mp04225rby10ls65")))

(define-public crate-gsm-1.2.1 (c (n "gsm") (v "1.2.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a1ibz1nc26fmz0rikzkww4vqfflnrq892n3y3yr28zx1xjqsmmk")))

(define-public crate-gsm-1.2.3 (c (n "gsm") (v "1.2.3") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1r9hbzkfr5gfrh4kci4j1wm7drxgg4rvgsrchv10c9f4frdb9v8f")))

(define-public crate-gsm-1.2.4 (c (n "gsm") (v "1.2.4") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1alnmlsshgrf9w8w6r6lbqng3w7nz15p8fzb3frvvny3plirqql7")))

(define-public crate-gsm-1.3.0 (c (n "gsm") (v "1.3.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "semver") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0n6hpk0dx4kgfjh5kqd25wwcvk1s8nfmgzyix5anassc1lwxjc4y")))

