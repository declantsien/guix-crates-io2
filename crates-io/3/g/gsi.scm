(define-module (crates-io #{3}# g gsi) #:use-module (crates-io))

(define-public crate-gsi-1.0.0 (c (n "gsi") (v "1.0.0") (d (list (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "gsj") (r "^1.0.0") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (o #t) (d #t) (k 0)) (d (n "smol") (r "^1.0.0") (d #t) (k 2)) (d (n "strip_bom") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0cngis6lh50gpzw79bxb514j5i2132y302cag4hjg5xz2sv5kh6i") (f (quote (("layers" "serde" "serde_json" "futures" "strip_bom") ("default" "layers"))))))

