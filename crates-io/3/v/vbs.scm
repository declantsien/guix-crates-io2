(define-module (crates-io #{3}# v vbs) #:use-module (crates-io))

(define-public crate-vbs-0.1.3 (c (n "vbs") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "displaydoc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.4") (d #t) (k 0)))) (h "187qyh5x0w9gkraqmbr0kwkfanqbcqlpqdbpynm06q3dc9bc3asx") (r "1.75.0")))

(define-public crate-vbs-0.1.4 (c (n "vbs") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.4") (d #t) (k 0)))) (h "1q7x2wx8rg7y3q8b91iy5yz2c65gvnlb55dbkvx56khyqkbcn6wp") (r "1.75.0")))

(define-public crate-vbs-0.1.5 (c (n "vbs") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^3.4") (d #t) (k 0)))) (h "16qyh9j31jmi15schz00l046830v5lwmc0pp3y2r5666l2f7v80x") (r "1.75.0")))

