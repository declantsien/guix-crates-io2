(define-module (crates-io #{3}# v vte) #:use-module (crates-io))

(define-public crate-vte-0.1.0 (c (n "vte") (v "0.1.0") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "1da4dyxxc1kk92rrxzwgny18wljd42506f2fr7rqlsrcj1anydg5")))

(define-public crate-vte-0.1.1 (c (n "vte") (v "0.1.1") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "03agr6pifqn586wswjnl37iw7wf1s5m08v94imq77my88jbm37c8")))

(define-public crate-vte-0.1.2 (c (n "vte") (v "0.1.2") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "12lcgpq1i28aixww5vx4qz71vk66p28g7jfzbnmjavdlxnkwnahq")))

(define-public crate-vte-0.2.0 (c (n "vte") (v "0.2.0") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "0chs0msmw91m8f5bp2qj4pnv38xqiaq554w3x8vc3w3lwk6jdxkm")))

(define-public crate-vte-0.2.1 (c (n "vte") (v "0.2.1") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "1nbqvv41wc6bfm4a7rmn90yc068vi0md1jjdxyixx0bfl24cs882")))

(define-public crate-vte-0.2.2 (c (n "vte") (v "0.2.2") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "18fzmpig825cyv4wxyjjf02gfajxypd5afwgws79yvlcr51grfla")))

(define-public crate-vte-0.3.0 (c (n "vte") (v "0.3.0") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "00vhk79crv4wh4x97b727r00jya7yqjp5al74d1ck0fx0wggfmzk")))

(define-public crate-vte-0.3.1 (c (n "vte") (v "0.3.1") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "1f7ry6d5cq3si2szrlzrk7iz0ciy3i100xdawgf8ikl7v7qz3nny")))

(define-public crate-vte-0.3.2 (c (n "vte") (v "0.3.2") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "1n102xalp4q9wxz22rf5h2x8nmqcqikqb3g0bm07i55mbp3k85m0")))

(define-public crate-vte-0.3.3 (c (n "vte") (v "0.3.3") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "1kz8svnqnxclllsgh0ck20rplw3qzp46b5v30yscnzrgw8vgahjg")))

(define-public crate-vte-0.5.0 (c (n "vte") (v "0.5.0") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "1phk2d685z0d2z1gkk4qi9acvqjh6js01ndl895rqhr3k32vjn55") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std"))))))

(define-public crate-vte-0.4.0 (c (n "vte") (v "0.4.0") (d (list (d (n "utf8parse") (r "^0.1") (d #t) (k 0)))) (h "17wajp8lylgzzckhnbbfqy6yba3xsps5rj47cqa39y8afpr8wg0j")))

(define-public crate-vte-0.6.0 (c (n "vte") (v "0.6.0") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "07p9x7nxzxprh923cq42x2yv3pg1vgfnn35q7ij5f3mdjgaxfv1g") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std"))))))

(define-public crate-vte-0.7.0 (c (n "vte") (v "0.7.0") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "1adn1p2jxp5mwy8y3wsip2smhqq3vxkv38lgnid8071rfc1zpbg5") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std"))))))

(define-public crate-vte-0.7.1 (c (n "vte") (v "0.7.1") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "1f5212d3fs9nbn7c6ywckl4i3rgzms4406x379ihmm852p6zd6c2") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std"))))))

(define-public crate-vte-0.8.0 (c (n "vte") (v "0.8.0") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "07bih4z7a90qjgqywnkpl36f6dzgmwfw2vlcww8kcq082qcqmk4n") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std"))))))

(define-public crate-vte-0.9.0 (c (n "vte") (v "0.9.0") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "0yjl1jzlrk617kwk445mg9zmc71nxvwghhgsxfqhmm9401hlaxvf") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std"))))))

(define-public crate-vte-0.10.0 (c (n "vte") (v "0.10.0") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "1mnjw3f071xbvapdgdf8mcdglw60dadcc5hhvz5zpljm53nmzwid") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std"))))))

(define-public crate-vte-0.10.1 (c (n "vte") (v "0.10.1") (d (list (d (n "arrayvec") (r "^0.5.1") (o #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "10srmy9ssircrwsb5lpx3fbhx71460j77kvz0krz38jcmf9fdg3c") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std"))))))

(define-public crate-vte-0.11.0 (c (n "vte") (v "0.11.0") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "0iah6qvi8r6a1lslcwjg2g0jnczz72f3cvr3ihb2vv6j5b0j3bhs") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std")))) (r "1.56.0")))

(define-public crate-vte-0.11.1 (c (n "vte") (v "0.11.1") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "15r1ff4j8ndqj9vsyil3wqwxhhl7jsz5g58f31n0h1wlpxgjn0pm") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std") ("ansi" "log")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.56.0")))

(define-public crate-vte-0.12.0 (c (n "vte") (v "0.12.0") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)) (d (n "bitflags") (r "^2.3.3") (o #t) (k 0)) (d (n "cursor-icon") (r "^1.0.0") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "06pr6j2ymyz0603rv8h54y8dr1fd5jwil7vcc4w4vxqh1q1c27a0") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std") ("ansi" "log" "cursor-icon" "bitflags")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.62.1")))

(define-public crate-vte-0.12.1 (c (n "vte") (v "0.12.1") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)) (d (n "bitflags") (r "^2.3.3") (o #t) (k 0)) (d (n "cursor-icon") (r "^1.0.0") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "1jabax2v0ivcriyrz059nqiy4y9l2mkqjc6gf2z7lvq81xna1c4q") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std") ("ansi" "log" "cursor-icon" "bitflags")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.62.1")))

(define-public crate-vte-0.13.0 (c (n "vte") (v "0.13.0") (d (list (d (n "arrayvec") (r "^0.7.2") (o #t) (k 0)) (d (n "bitflags") (r "^2.3.3") (o #t) (k 0)) (d (n "cursor-icon") (r "^1.0.0") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "utf8parse") (r "^0.2.0") (d #t) (k 0)) (d (n "vte_generate_state_changes") (r "^0.1.0") (d #t) (k 0)))) (h "12qqlvx7qlw1r8l6k9fyqj7k8v72xbz47kppsv0f0l7hjsp25ss0") (f (quote (("no_std" "arrayvec") ("nightly" "utf8parse/nightly") ("default" "no_std") ("ansi" "log" "cursor-icon" "bitflags")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.62.1")))

