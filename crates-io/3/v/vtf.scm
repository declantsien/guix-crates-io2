(define-module (crates-io #{3}# v vtf) #:use-module (crates-io))

(define-public crate-vtf-0.1.1 (c (n "vtf") (v "0.1.1") (d (list (d (n "image") (r "^0.19.0") (d #t) (k 0)))) (h "108vs7b3hyk6v9y7hyss428m7a9wyk2s7znkk7r5r4mr11zv64s2")))

(define-public crate-vtf-0.1.2 (c (n "vtf") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.4") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)))) (h "00d361rkfb727i9asmd3r8m7w0gh5i52zq55iv0np638ffbl68a4")))

(define-public crate-vtf-0.1.3 (c (n "vtf") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "image") (r "^0.22.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.4") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)))) (h "11w5znki9l1rlj89rqd32s3inybih7w06cpq3ajglwa18gmjlhxq")))

(define-public crate-vtf-0.1.4 (c (n "vtf") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "num_enum") (r "^0.4") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)))) (h "056bh0k71hj7mdrmhhb0qznhlfqpncxybb6w1whyp8qcl2a2s652")))

(define-public crate-vtf-0.1.5 (c (n "vtf") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "num_enum") (r "^0.4") (d #t) (k 0)) (d (n "parse-display") (r "^0.1.1") (d #t) (k 0)))) (h "13cavjrpqh6irqzivqb9lwla1byw2r5w4z4gvbqppqgzs9wbs3w7")))

(define-public crate-vtf-0.1.6 (c (n "vtf") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "parse-display") (r "^0.8.2") (d #t) (k 0)) (d (n "texpresso") (r "^2.0.1") (d #t) (k 0)))) (h "02nvxib74c1p4hdnky9nqfv3jwv1y1q36523hpgs79l8r0mmkhzb") (r "1.60")))

(define-public crate-vtf-0.2.0 (c (n "vtf") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "parse-display") (r "^0.8.2") (d #t) (k 0)) (d (n "texpresso") (r "^2.0.1") (d #t) (k 0)))) (h "0vmwyz0j8bl7pk4r84k0fygb6clpi0fx1jbbf4lsyjyps0cyb0wz") (r "1.67.0")))

(define-public crate-vtf-0.2.1 (c (n "vtf") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.3.1") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "parse-display") (r "^0.8.2") (d #t) (k 0)) (d (n "texpresso") (r "^2.0.1") (d #t) (k 0)))) (h "0ws7rpnc078jfjw7zkx8gxbqkhxgyq436rfj3cdl4819pvn2qjj0") (r "1.67.0")))

