(define-module (crates-io #{3}# v vlq) #:use-module (crates-io))

(define-public crate-vlq-0.1.0 (c (n "vlq") (v "0.1.0") (h "1lv85m2px97rafpgrg770p7pfm95nivksbjaxkxs2gall8y2gar7")))

(define-public crate-vlq-0.2.0 (c (n "vlq") (v "0.2.0") (h "0xgfz6asqr4md9jpwjggxgvbh7qbqgnvvfa5lnbgvim3wxwbnqiy")))

(define-public crate-vlq-0.3.0 (c (n "vlq") (v "0.3.0") (h "11yzlb71gzdmbww3yi8mp5h8p2qp3carf6jjkcp22xlchk73r8z7")))

(define-public crate-vlq-0.4.0 (c (n "vlq") (v "0.4.0") (h "041611a2cxs2wq8rhb9vqxm1s6dm0hff4was5n2j97rm8ky55aa7")))

(define-public crate-vlq-0.5.0 (c (n "vlq") (v "0.5.0") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)))) (h "0rqxhmiw7rch5j5liasnvdnsdl03q1i3c2wlzrfny53ycasllgba")))

(define-public crate-vlq-0.5.1 (c (n "vlq") (v "0.5.1") (d (list (d (n "quickcheck") (r "^0.5.0") (d #t) (k 2)))) (h "1zygijgl47gasi0zx34ak1jq2n4qmk0cx2zpn13shba157npxpb5")))

