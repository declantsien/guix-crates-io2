(define-module (crates-io #{3}# v vie) #:use-module (crates-io))

(define-public crate-vie-0.1.0 (c (n "vie") (v "0.1.0") (h "0h6q9zmxfz1v7la7920lssg73fn2dw4pcavljxd5paazqcyhhml5") (y #t)))

(define-public crate-vie-1.0.0 (c (n "vie") (v "1.0.0") (h "0xnll5z7fn8iaardixiz9x3y1v3w4xmc1zl76kxkj1ij9pjq1gyq") (y #t)))

(define-public crate-vie-1.0.1 (c (n "vie") (v "1.0.1") (h "1cfx2v1nd6wcnrmihr605ymgz4sr1x0sigyhqw0cxpmiajc5qz5j") (y #t)))

(define-public crate-vie-1.0.2 (c (n "vie") (v "1.0.2") (h "1h5kh5vk83nmw08mbfi54k7d7vh0pa6c63j6msckqhcj1407s579")))

