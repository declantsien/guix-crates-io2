(define-module (crates-io #{3}# v vma) #:use-module (crates-io))

(define-public crate-vma-0.4.0 (c (n "vma") (v "0.4.0") (d (list (d (n "ash") (r "^0.37") (k 0)) (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1spzhwximay7lna9m8a7vnfi803b2a52sygw49jhb2djhcy7m1bn") (f (quote (("recording") ("loaded" "ash/loaded") ("linked" "ash/linked") ("generate_bindings" "bindgen") ("default" "loaded")))) (y #t)))

(define-public crate-vma-0.3.0 (c (n "vma") (v "0.3.0") (d (list (d (n "ash") (r "^0.37") (k 0)) (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0pqf26l74413874n3l36yi35z18ydj8a5y2iz3g0smw5j4zwm7x2") (f (quote (("recording") ("loaded" "ash/loaded") ("linked" "ash/linked") ("generate_bindings" "bindgen") ("default" "loaded"))))))

(define-public crate-vma-0.3.1 (c (n "vma") (v "0.3.1") (d (list (d (n "ash") (r "^0.37") (k 0)) (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1gx7npw0zylvvs58vvhka5bla6mdi136lqswakx0hb3r3zr9wli4") (f (quote (("recording") ("loaded" "ash/loaded") ("linked" "ash/linked") ("generate_bindings" "bindgen") ("default" "loaded"))))))

