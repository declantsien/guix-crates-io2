(define-module (crates-io #{3}# v vcd) #:use-module (crates-io))

(define-public crate-vcd-0.1.0 (c (n "vcd") (v "0.1.0") (h "0x8niafj03hn594asbd7j9p4gnyw0jbacymw27gd9gx3clm737a7")))

(define-public crate-vcd-0.2.0 (c (n "vcd") (v "0.2.0") (h "06j1ml74janji188dn1wmzj491qx4knmx3mhc5bvqfmkgrqsx1z6")))

(define-public crate-vcd-0.3.0 (c (n "vcd") (v "0.3.0") (h "1nrbslrlw15f32p5dsszz78bp9vvbhdk6b1iljwl5rqjgldivq2b")))

(define-public crate-vcd-0.4.0 (c (n "vcd") (v "0.4.0") (h "12l8vg3la96c3wn41d4kjvnynn7mq97brrr4zd0755ah0zzrda3w")))

(define-public crate-vcd-0.5.0 (c (n "vcd") (v "0.5.0") (h "1v4m34l8p833mgh6v2yz02ydcbljlwibn5nlxqg430nf1kh6gzj5")))

(define-public crate-vcd-0.6.0 (c (n "vcd") (v "0.6.0") (h "1d8vizjv7fzf4j3zjpavg160lwzy902f1kjpmnychvr49m5f9z7d")))

(define-public crate-vcd-0.6.1 (c (n "vcd") (v "0.6.1") (h "1z3127lwzx3y9w4px8xv01alb2jlsyfggr8q4jrkpgr4c4cyv9c4")))

(define-public crate-vcd-0.7.0 (c (n "vcd") (v "0.7.0") (h "0160cbflacjr958z8vacdwvsl39b6icxhx5a9jq2xs2dr17pf1a5")))

