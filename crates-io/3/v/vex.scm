(define-module (crates-io #{3}# v vex) #:use-module (crates-io))

(define-public crate-vex-0.1.0 (c (n "vex") (v "0.1.0") (h "1bisxh0jyp5brjjhgr7k1bkibdbdcc1q7chknjvv4ms9bwzrga3z")))

(define-public crate-vex-0.1.1 (c (n "vex") (v "0.1.1") (h "0pcj5qh7bp55j59mnn63jlp2irj3c6b4jjqjh42llw10x1l21grq")))

(define-public crate-vex-0.1.2 (c (n "vex") (v "0.1.2") (h "01y3rjs8wm3ish8j7k4jlv0fc3x8rfv374bamfpv0gr6q70589qc")))

(define-public crate-vex-0.1.3 (c (n "vex") (v "0.1.3") (h "1dyd0y7mn3jj2m3mqkr84z2r0jrc31lrcrdpl0wvxvixphsyfm4s")))

(define-public crate-vex-0.2.0 (c (n "vex") (v "0.2.0") (h "1qsmn3077clqnrkmv24fg3rv7ycmn9bd5cs5dmvkvsyz429w4av3")))

(define-public crate-vex-1.0.0 (c (n "vex") (v "1.0.0") (h "0yhyjfb7kjwrlvckbr07a1bbv5p94lffshsy9nsh6dl2r4mzjd9l")))

(define-public crate-vex-1.0.1 (c (n "vex") (v "1.0.1") (h "0s3cr1493sh43h3imjs7zaqk2dra731d57gz5skln60jmmvq0w52")))

(define-public crate-vex-1.0.2 (c (n "vex") (v "1.0.2") (h "1daf2ywcc2wlijrmg5ry16cjn3llxby7xglgcvphx49hwpwi02jb")))

(define-public crate-vex-2.0.0 (c (n "vex") (v "2.0.0") (h "10kn85xqjd9hdpz6xnh9dv7j7934jzd66745gj40hvscd2p8rfm3")))

(define-public crate-vex-2.0.1 (c (n "vex") (v "2.0.1") (h "023bbjjfb3kpf3a33352i804mvs2sf5w8nm29a05d5lvzimm5z6s")))

(define-public crate-vex-2.1.1 (c (n "vex") (v "2.1.1") (h "1z2h5204bqhzmrzkryx6a5v7zfnym1i86cfs0klvncvn9djnl7cs")))

