(define-module (crates-io #{3}# v vad) #:use-module (crates-io))

(define-public crate-vad-0.1.2 (c (n "vad") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "1y6pwbp0xrjcpzx81pfrh2kcjvc9bwhb0hwl09r0hm9v3c05ypp9") (l "vad")))

(define-public crate-vad-0.1.3 (c (n "vad") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "18cj0n9iwbld9j7s6x65c1y4w5m3phr74kqdmmdcgxa5bf1fs8ff") (l "vad")))

(define-public crate-vad-0.1.4 (c (n "vad") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "0sn79iii1y4kbw40x0n64x6pb4g8m6c2455277s0l5fss0l4hx5h") (l "vad")))

(define-public crate-vad-0.1.5 (c (n "vad") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "1bbrwiz4vzj4hj0ddinnp1cbiwqnd2chyx4gd9dgz3jfnp80id6r") (l "vad")))

