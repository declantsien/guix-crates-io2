(define-module (crates-io #{3}# v vps) #:use-module (crates-io))

(define-public crate-vps-0.1.0 (c (n "vps") (v "0.1.0") (h "07zvi5wg5hsbpc49i3zxj9aq0ssdpm6dw9riwr0bgyff5fb2kipv")))

(define-public crate-vps-0.1.1 (c (n "vps") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.10.0") (f (quote ("editor" "password" "completion" "history"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.10") (d #t) (k 0)))) (h "19jis9ixsypv3vgxfi1cph12ww8ziksb9fp2gyjh90rc95vwizpj")))

(define-public crate-vps-0.1.2 (c (n "vps") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.10.0") (f (quote ("editor" "password" "completion" "history"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.10") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "10agccgy66f04j3ln8w8m0ikshlp508bjpsnq0gvk7dlfrim71l1")))

(define-public crate-vps-0.1.3 (c (n "vps") (v "0.1.3") (d (list (d (n "dialoguer") (r "^0.10.0") (f (quote ("editor" "password" "completion" "history"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.10") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "1f90x89n6lv9ad9vm2g036ign46pjg55fpwx0ml7nx7mrczxjr1j")))

(define-public crate-vps-0.1.4 (c (n "vps") (v "0.1.4") (d (list (d (n "dialoguer") (r "^0.10.0") (f (quote ("editor" "password" "completion" "history"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.10") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "0f1nrvlzfizh39lp8d0788g2i3vbs9lddqgn6jwjlnfs56pxc7d4")))

(define-public crate-vps-0.1.5 (c (n "vps") (v "0.1.5") (d (list (d (n "dialoguer") (r "^0.10.0") (f (quote ("editor" "password" "completion" "history"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.10") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)))) (h "0zf46amzf8dy6z2gcb294mc8fvc9dgwfdcl6v9abk6mrxq88lvad")))

