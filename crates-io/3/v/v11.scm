(define-module (crates-io #{3}# v v11) #:use-module (crates-io))

(define-public crate-v11-0.0.5 (c (n "v11") (v "0.0.5") (d (list (d (n "bit-vec") (r "^0.5.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3.5") (d #t) (k 0)) (d (n "itertools") (r "^0.4.16") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "mopa") (r "0.2.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)) (d (n "procedural-masquerade") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "v11_macros") (r "^0.0.5") (d #t) (k 0)))) (h "1k6m9jmpzgld42gb718vb8jngj7imcbk1sq53ndd5dd5rrdw071a") (f (quote (("doc"))))))

