(define-module (crates-io #{3}# v vep) #:use-module (crates-io))

(define-public crate-vep-0.1.0 (c (n "vep") (v "0.1.0") (d (list (d (n "blake3") (r "^1.0.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.9.5") (d #t) (k 2)) (d (n "sha3") (r "^0.9.1") (d #t) (k 2)))) (h "1msyr5awlahlkixwdix6j10zi67kchyai8laaavavhfgglp4vllj") (f (quote (("std") ("default"))))))

(define-public crate-vep-1.0.0 (c (n "vep") (v "1.0.0") (d (list (d (n "blake3") (r "^1.0.0") (f (quote ("traits-preview"))) (d #t) (k 2)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.9.5") (d #t) (k 2)) (d (n "sha3") (r "^0.9.1") (d #t) (k 2)) (d (n "typenum") (r "^1.14.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "175qdlj2wj5bw99ls4fn5c8xpqy3spfly5y0adlm6fknvr70axrb") (f (quote (("std") ("default" "std"))))))

(define-public crate-vep-1.0.1 (c (n "vep") (v "1.0.1") (d (list (d (n "blake3") (r "^1.0.0") (f (quote ("traits-preview"))) (d #t) (k 2)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.9.5") (d #t) (k 2)) (d (n "sha3") (r "^0.9.1") (d #t) (k 2)) (d (n "typenum") (r "^1.14.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1156s7l97ph30mgx25bnkg5snfgl7135ab9sv34q0xlshalwfvwb") (f (quote (("std") ("default" "std"))))))

(define-public crate-vep-2.0.0 (c (n "vep") (v "2.0.0") (d (list (d (n "blake3") (r "^1.0.0") (f (quote ("traits-preview"))) (d #t) (k 2)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.9.5") (d #t) (k 2)) (d (n "sha3") (r "^0.9.1") (d #t) (k 2)) (d (n "typenum") (r "^1.14.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0n3131mavz9yj5wjq9iqd565k2w10v6yhj6ci768nw35ikihbawa") (f (quote (("std") ("default" "std"))))))

(define-public crate-vep-2.1.0 (c (n "vep") (v "2.1.0") (d (list (d (n "blake3") (r "^1.0.0") (f (quote ("traits-preview"))) (d #t) (k 2)) (d (n "digest") (r "^0.9.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "sha2") (r "^0.9.6") (d #t) (k 2)) (d (n "sha3") (r "^0.9.1") (d #t) (k 2)) (d (n "typenum") (r "^1.14.0") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "zeroize") (r "^1.4.1") (d #t) (k 0)))) (h "0qpf817bj0kqhmphfwdr1hcc6smx4n240s44v05i4aafbsvw573q") (f (quote (("std") ("default" "std"))))))

