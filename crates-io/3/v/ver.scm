(define-module (crates-io #{3}# v ver) #:use-module (crates-io))

(define-public crate-ver-0.1.0 (c (n "ver") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)) (d (n "recital") (r "^0.3.0") (d #t) (k 0)))) (h "1p6xhf9krkz4k90i0nifa2in4n1mvlcy1mpp5rmzy4ycaf7cv72b")))

