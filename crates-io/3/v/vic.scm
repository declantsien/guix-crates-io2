(define-module (crates-io #{3}# v vic) #:use-module (crates-io))

(define-public crate-vic-0.1.1 (c (n "vic") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.66") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^4.2.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "miette") (r "^5.8.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)) (d (n "vicuna-compiler") (r "^0.1.1") (d #t) (k 0)) (d (n "vicuna-runtime") (r "^0.1.1") (d #t) (k 0)))) (h "17l6hlygfmy59rhcbq1ym4akjslahd12phcx269qcq4jv3v1xjc3")))

