(define-module (crates-io #{3}# v vcf) #:use-module (crates-io))

(define-public crate-vcf-0.1.0 (c (n "vcf") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "indexmap") (r "^1") (d #t) (k 0)))) (h "0ncn453ghv4lhz1hw812xddfcmqingw3ras8h72csribg0w86lfm")))

(define-public crate-vcf-0.2.0 (c (n "vcf") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0ivi1r5yj2a1f1ksis7yk2q3czw1wqd4gxl1nba70xfsclsaydk7")))

(define-public crate-vcf-0.2.1 (c (n "vcf") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0b4gjn2bhxhllhkxqn6k2a5q9jbm3d6xmdh2xhj69adj08qmpxrh")))

(define-public crate-vcf-0.2.2 (c (n "vcf") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "06s4qg9p2q7x7v3wikcybsqw52kj0ignwrxqxjfggq5dhl7a8mvz")))

(define-public crate-vcf-0.2.3 (c (n "vcf") (v "0.2.3") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0flhnbg2pq814w3v7kw324qqp7rhw4h0m2ccmnrsbjnjy4ndni4g")))

(define-public crate-vcf-0.3.0 (c (n "vcf") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1rvfplwc7iahwrkgcjrf4rd4w59d7lklz6qqlh6rsbph2nbma2n1")))

(define-public crate-vcf-0.3.1 (c (n "vcf") (v "0.3.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1kcqf1yw2zjf1fafdjj8havs50b4vb5rrlh0yx8hrcvj0nnhsskj")))

(define-public crate-vcf-0.4.0 (c (n "vcf") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0nv9jbaif5vl62pgw4niaslxhw756mj548kfq66hs902ndwpjd76")))

(define-public crate-vcf-0.5.0 (c (n "vcf") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1fqg1j4csw1zsjxy8mw44lryx3fi9sq6v7bw3bzgpnbq031grffv")))

(define-public crate-vcf-0.6.0 (c (n "vcf") (v "0.6.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "nom") (r "^5") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1dy0i2z6kf9dvh29g6d64a72cr9fxk66ypf8wy0b8yvhzhpa76l8")))

(define-public crate-vcf-0.6.1 (c (n "vcf") (v "0.6.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0dc0p00a19rpmhrqcshrn2qg5l716b5s1fy8vpd3p32bw77vpbs0")))

