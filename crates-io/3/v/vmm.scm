(define-module (crates-io #{3}# v vmm) #:use-module (crates-io))

(define-public crate-vmm-0.1.0 (c (n "vmm") (v "0.1.0") (h "0wf320g3mkg35jafsvwxlhv33i478nr7c3b5b6zxjds1na18n8nw")))

(define-public crate-vmm-0.1.1 (c (n "vmm") (v "0.1.1") (h "0ac4yr886c4mvdm3h5m39nrjh0i10l8k6id8vkbr82cr08n1qkj2")))

(define-public crate-vmm-0.1.2 (c (n "vmm") (v "0.1.2") (h "0nv9r3r3cg0sshqki06xa8nvwps3qhknkp2xc7lb6zbdn7rmv1ck")))

(define-public crate-vmm-0.2.0 (c (n "vmm") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.14.1") (d #t) (k 0)))) (h "15mhx8x99n3y2s7zdjhg427ncfn01wz39zyis06l4wc1ys8b6fa2")))

