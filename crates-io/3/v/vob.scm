(define-module (crates-io #{3}# v vob) #:use-module (crates-io))

(define-public crate-vob-0.1.0 (c (n "vob") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1.42") (d #t) (k 0)))) (h "1flwgg8iqsdv0l02fslyfl4w66xqwhjmclrgdjz6br1y39kmn4bh")))

(define-public crate-vob-1.0.0 (c (n "vob") (v "1.0.0") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)))) (h "1qdpv445yvkz3ib5fl2fa00vj2i2msh71mbfin8nbmgy4xcvybp8")))

(define-public crate-vob-1.1.0 (c (n "vob") (v "1.1.0") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1s4nda2hd6fphpvvrz6h2ilczpd48va776vm7rlqxrs5bjnk7fif")))

(define-public crate-vob-1.2.0 (c (n "vob") (v "1.2.0") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1v6nhxd1sm422r3yvpfdp1vjb86hfm7zjd65r38iclvl1290riqk")))

(define-public crate-vob-1.3.0 (c (n "vob") (v "1.3.0") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13xxs3x55c54ffnxsbfzwa1paih8gkv3x3r5gj896gzn6ykky8p7") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-1.3.1 (c (n "vob") (v "1.3.1") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1xv6inj052v434874z39c4wmafihkx1q2qk69jky3i5xjndw554z") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-1.3.2 (c (n "vob") (v "1.3.2") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1y16xi3cabrbxk7v7gqvagq48w3m27r04bpd0yzjn4h1yl8kc1ij") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-2.0.0 (c (n "vob") (v "2.0.0") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fiw65jahnv18mlnbwg45hx9x5lyfpmbk1xvvb2mvfr60lzk8snz") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-2.0.1 (c (n "vob") (v "2.0.1") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b4z12jwbkq2hds093pwsrfznzjiil2as730qqjbis5pz1pwyp5h") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-2.0.2 (c (n "vob") (v "2.0.2") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18xf4dnbzp50pn34vvfibdkhcdn10cwhswlwn8x01yhliyqj690m") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-2.0.3 (c (n "vob") (v "2.0.3") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1lf834470h28jgd45sigq0bw78mv7557v83yrcrim46nls8275x2") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-2.0.4 (c (n "vob") (v "2.0.4") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.1") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0bz5b80j6a63hy8jyp0982r24l212gbrq8rprmifvw72fy4ylc2l") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-2.0.5 (c (n "vob") (v "2.0.5") (d (list (d (n "num-traits") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.2") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1fzc1ndvnajagr7dzay1hilsngkqg3xnqsh6cixpfld77y4xkap7") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-2.0.6 (c (n "vob") (v "2.0.6") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "058g52dc5c046zc3v88hpi8dcg8wfkavlxnhsmcw54xfz6fxgxla") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-3.0.0 (c (n "vob") (v "3.0.0") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0w02qd7k39h041fi90mb0vcqdd841qvsbmj7j6rpw3ccmg9h3c65") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-3.0.1 (c (n "vob") (v "3.0.1") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0hbv3zvczkhvirvscm9s52yl7n5ivmphq3ba6543hrnl521gp8ba") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-3.0.2 (c (n "vob") (v "3.0.2") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0vwqjxllcxbraj3dwdmclscyd20q2yis9g1jk092g2nkbpp3xnyb") (f (quote (("unsafe_internals"))))))

(define-public crate-vob-3.0.3 (c (n "vob") (v "3.0.3") (d (list (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1ri3xhnwrk4w91r2lwy153iyqqxfq7f7djs4fz34783i3v2g8n60") (f (quote (("unsafe_internals"))))))

