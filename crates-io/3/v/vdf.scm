(define-module (crates-io #{3}# v vdf) #:use-module (crates-io))

(define-public crate-vdf-0.1.0 (c (n "vdf") (v "0.1.0") (d (list (d (n "classgroup") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r ">= 0.2") (d #t) (k 2)) (d (n "hex") (r "^0.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0a3jp7x0i7ab004j5n2cyprjq806p12cxwskf2ln63zhabncswyj")))

