(define-module (crates-io #{3}# v vfp) #:use-module (crates-io))

(define-public crate-vfp-0.1.0 (c (n "vfp") (v "0.1.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ed25519") (r "^2.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1.12") (d #t) (k 0)) (d (n "tokio-util") (r "^0.7.7") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "1ahmvzgzzbza152w678rivvz444v82i3xdgljhslnqf8f4sas39j")))

