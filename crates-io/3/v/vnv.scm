(define-module (crates-io #{3}# v vnv) #:use-module (crates-io))

(define-public crate-vnv-0.1.0 (c (n "vnv") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yvzd8hbd4md8387wfkwff8h416kma0cl7rdhvmj388xlzbm38vi")))

(define-public crate-vnv-0.1.1 (c (n "vnv") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "077ps511w44fagwy89himlh6hdgm3r24903qr2rszmsnhjn34cl3")))

(define-public crate-vnv-0.1.2 (c (n "vnv") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vr0bkcsz5izap81g50z33d5wnmd20r7lqgp5xn71ph61n164lqj")))

(define-public crate-vnv-0.1.3 (c (n "vnv") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "051q27d0xg2b4y2plhjlp095a9iddx19b5nmx04hgc9pxnr2mssc")))

