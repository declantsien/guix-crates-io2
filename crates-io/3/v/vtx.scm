(define-module (crates-io #{3}# v vtx) #:use-module (crates-io))

(define-public crate-vtx-0.1.0 (c (n "vtx") (v "0.1.0") (d (list (d (n "aym") (r "^0.1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "delharc") (r "^0.3") (k 0)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1jxylm4z0lfjr5gr706wxlf1i0qmi924a29cwqgd09kxjl9ly1v2")))

(define-public crate-vtx-0.1.1 (c (n "vtx") (v "0.1.1") (d (list (d (n "aym") (r "^0.1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "delharc") (r "^0.3") (k 0)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "180wli8vgd3gaszx9f78dz7rqx4h2aw7xja88a4pviqah38rn10d")))

(define-public crate-vtx-0.1.2 (c (n "vtx") (v "0.1.2") (d (list (d (n "aym") (r "^0.1.1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "delharc") (r "^0.3") (k 0)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "16mvnmv83dnnpgm62fbgfhqrxd12bfy0nlfq0h099mqdvcz289c7")))

(define-public crate-vtx-0.15.0 (c (n "vtx") (v "0.15.0") (d (list (d (n "aym") (r "^0.15") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "delharc") (r "^0.3") (k 0)) (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04r3hnr0wqvl2cd9hsia2yi22307lwfzrifn6hkg04z2g542kicb")))

(define-public crate-vtx-0.16.0 (c (n "vtx") (v "0.16.0") (d (list (d (n "aym") (r "^0.16.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "delharc") (r "^0.3") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "expect-test") (r "^1.1") (d #t) (k 2)) (d (n "sha2") (r "^0.9") (d #t) (k 2)))) (h "19jfvrqv971jl8sl35zpfgqln6g2swqkah0x1vw20b10v8sn3nvr")))

