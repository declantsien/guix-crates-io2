(define-module (crates-io #{3}# v vmx) #:use-module (crates-io))

(define-public crate-vmx-0.0.0 (c (n "vmx") (v "0.0.0") (h "01yk8awps0cz2v2z2y010n7bnghia5dpi9n7al14dr5mgyx669xf")))

(define-public crate-vmx-0.0.1 (c (n "vmx") (v "0.0.1") (h "1103x6rj4wv0casq6121p37wfr9d3rjsp2111hw7a40jaqwicbsh")))

