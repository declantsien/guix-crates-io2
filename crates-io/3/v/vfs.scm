(define-module (crates-io #{3}# v vfs) #:use-module (crates-io))

(define-public crate-vfs-0.1.0 (c (n "vfs") (v "0.1.0") (h "0yikz903iga2ymjpjjjc208m59xkqpcpm7g993rs50c3whybhrrx")))

(define-public crate-vfs-0.1.1 (c (n "vfs") (v "0.1.1") (h "0cf8r96nlm6lm7a3q58sxh0qqbpyjxwi98l2vwr5lbsr53b9q9wc")))

(define-public crate-vfs-0.1.2 (c (n "vfs") (v "0.1.2") (h "0ly2wvaxh4vi6pwb2fhkbrsj30qk7a3d6zvsbwjgylybzp7yq30h")))

(define-public crate-vfs-0.2.0 (c (n "vfs") (v "0.2.0") (h "0cfqiplgfw483s9wcsvabnwamxlbm1jpkb1yjsa6b9jjkiq1cic3")))

(define-public crate-vfs-0.2.1 (c (n "vfs") (v "0.2.1") (h "10a23ggdfdfnffz04nk6fa865x8g6v6rm4r7pf3pn75cv500wy9p")))

(define-public crate-vfs-0.3.0 (c (n "vfs") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "0d99sqhbrrzww3yyby5sp1c7map3djgdfbyqa4yr5azli3i07jl8")))

(define-public crate-vfs-0.4.0 (c (n "vfs") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "11lxmh404y1iicdlk1nvq8csfsld82kl4vdrj2a6z39gzk7q5hvw")))

(define-public crate-vfs-0.5.0 (c (n "vfs") (v "0.5.0") (d (list (d (n "radix_trie") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^5.6") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("v4"))) (d #t) (k 2)))) (h "1rfjrsdsd1a8j57fz4ryz198f16lpjc0qqkcsn17q8c92ymgk0d2") (f (quote (("embedded-fs" "rust-embed" "radix_trie"))))))

(define-public crate-vfs-0.5.1 (c (n "vfs") (v "0.5.1") (d (list (d (n "radix_trie") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^5.6") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0lkhkgml4kcbgl7zlq21q039nb1sf8ymwv1j7c0k85g8pc5dzc7y") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed" "radix_trie"))))))

(define-public crate-vfs-0.5.2 (c (n "vfs") (v "0.5.2") (d (list (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "radix_trie") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "^5.6") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "18gkm4kvpgyr0fsjrd4xc7yrbs23j1zj63bivnpfxp5gh6y1bzk7") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed" "radix_trie"))))))

(define-public crate-vfs-0.6.0 (c (n "vfs") (v "0.6.0") (d (list (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "rust-embed") (r "^5.6") (o #t) (d #t) (k 0)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "03bqzdgp16gg96cvlxip6x776syhydrz9m8brsl7zcbdpsc1nfrr") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed"))))))

(define-public crate-vfs-0.6.1 (c (n "vfs") (v "0.6.1") (d (list (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "rust-embed") (r "^5.6") (o #t) (d #t) (k 0)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "18gbiq86mlzxvnw26sxv33a4xcrnl24kpj8qlqazb6v9q5hbjqha") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed"))))))

(define-public crate-vfs-0.6.2 (c (n "vfs") (v "0.6.2") (d (list (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "rust-embed") (r "^5.6") (o #t) (d #t) (k 0)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0lkf1p1m050bz1km82bk195bnsdzygg32mqrflkjwfs64ab4gai8") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed"))))))

(define-public crate-vfs-0.7.0 (c (n "vfs") (v "0.7.0") (d (list (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "rust-embed") (r "^6") (o #t) (d #t) (k 0)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "11b2m08hkz8m4b4i2krvaii4fdwf46ssy4fqjqzs9h6rx8z60mz8") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed"))))))

(define-public crate-vfs-0.7.1 (c (n "vfs") (v "0.7.1") (d (list (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "rust-embed") (r "^6") (o #t) (d #t) (k 0)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0n87ynbfqnp069jkzfybg10yl2rzmjsj50b55shyqkznmk8f1cd1") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed"))))))

(define-public crate-vfs-0.8.0 (c (n "vfs") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "rust-embed") (r "^6") (o #t) (d #t) (k 0)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0xvr0bczidxmi47lgfdm5kxh7b00lia1935633a7i4fg869s95gm") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed"))))))

(define-public crate-vfs-0.9.0 (c (n "vfs") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "rust-embed") (r "6.3.*") (o #t) (d #t) (k 0)) (d (n "rust-embed-impl") (r "6.2.*") (o #t) (d #t) (k 0)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0s3qxj04k01jixb08xc3bs4hcqgmb6z7c9mqv8xh2ckv5p4p7mfy") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed" "rust-embed-impl"))))))

(define-public crate-vfs-0.10.0 (c (n "vfs") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)) (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "6.3.*") (o #t) (d #t) (k 0)) (d (n "rust-embed-impl") (r "6.2.*") (o #t) (d #t) (k 0)) (d (n "tokio") (r "=1.29.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0bafyyzcrb31na1h3wagxsyxp105ngjfxm95k70ikb8vzhnfjkrf") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed" "rust-embed-impl") ("async-vfs" "tokio" "async-std" "async-trait" "futures" "async-recursion"))))))

(define-public crate-vfs-0.11.0 (c (n "vfs") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)) (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "8.0.*") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.29.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "11bw61g0yzdsasvl7bdgxhm0mibl9kgzy0d7zf1z33zv7mvyg7hf") (f (quote (("export-test-macros") ("embedded-fs" "rust-embed") ("async-vfs" "tokio" "async-std" "async-trait" "futures" "async-recursion"))))))

(define-public crate-vfs-0.12.0 (c (n "vfs") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "async-recursion") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.12.0") (o #t) (d #t) (k 0)) (d (n "async-trait") (r "^0.1.73") (o #t) (d #t) (k 0)) (d (n "camino") (r "^1.0.5") (o #t) (d #t) (k 0)) (d (n "camino") (r "^1.0.5") (d #t) (k 2)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (o #t) (d #t) (k 0)) (d (n "rust-embed") (r "8.0.*") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.29.0") (f (quote ("macros" "rt"))) (o #t) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)) (d (n "uuid") (r "=0.8.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1lbkq5pqy00qvrsmkzca1gfmf89ccssycy4ii6ziv9w2w6bx0k35") (f (quote (("export-test-macros" "camino") ("embedded-fs" "rust-embed") ("async-vfs" "tokio" "async-std" "async-trait" "futures" "async-recursion"))))))

