(define-module (crates-io #{3}# v vat) #:use-module (crates-io))

(define-public crate-vat-0.1.0 (c (n "vat") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0fgmlxkmc94d2p3x8rf2m9ywf6qw50s364yzcw9qn5p2vfjl0wnb")))

