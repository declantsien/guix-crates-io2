(define-module (crates-io #{3}# v vii) #:use-module (crates-io))

(define-public crate-vii-0.0.1 (c (n "vii") (v "0.0.1") (h "0airqx71a70yxc79g1d81037k0ma6yb17f1xs3gdpia2ar3qzl9q")))

(define-public crate-vii-0.0.2 (c (n "vii") (v "0.0.2") (h "1q4gzspijgr1hllxbigm56f1ma99ngwbfpa2i6d6cddmjhwls0qh")))

(define-public crate-vii-0.0.3 (c (n "vii") (v "0.0.3") (h "1kp951hm443fz8wddkh039dsyxkf7fyp0xfavi4nnxmrp2g2clxz")))

(define-public crate-vii-0.0.4 (c (n "vii") (v "0.0.4") (h "1325m93r4jf6lwwn7ljrmdvlka4mawh341lffmdqzh38f9hvdgy0")))

(define-public crate-vii-0.0.5 (c (n "vii") (v "0.0.5") (h "108jwg62kz9xxkr3inac02c4gclb30wkl730gkh7g31fsmc1fv55")))

(define-public crate-vii-0.0.6 (c (n "vii") (v "0.0.6") (h "0n9vlmzvs60zwyp84acvy53wn2njxl8c5zxqlflab452l7jr4fh7")))

(define-public crate-vii-0.0.8 (c (n "vii") (v "0.0.8") (h "0kgym915p34184hs96yb9wbylpd6qzn9zxns1iv3wla1icjgsf6x")))

(define-public crate-vii-0.0.9 (c (n "vii") (v "0.0.9") (h "11nziij5j2i3rjzcybm98z071rgz266qns1zf6n4kmrfs4g0af2n")))

(define-public crate-vii-0.0.18 (c (n "vii") (v "0.0.18") (h "13mpgh7v1rfdgn4wgbd871y1bpx18n9azrj8cwi7vi1m6b0r4kyn")))

(define-public crate-vii-0.0.19 (c (n "vii") (v "0.0.19") (d (list (d (n "serde") (r "^1.0.142") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (o #t) (d #t) (k 0)))) (h "14snm59xgwv0zxp0vwr5hci4j081x77i69vml4pjs8k43l6ma6m9") (f (quote (("textprop" "serde" "serde_json") ("default" "textprop" "channel") ("channel"))))))

