(define-module (crates-io #{3}# v vdm) #:use-module (crates-io))

(define-public crate-vdm-1.0.0 (c (n "vdm") (v "1.0.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0h8yaz5rkgvsapjpry4div0xkz1bij36xv274c3ms7nfdhy6chc5")))

(define-public crate-vdm-1.0.1 (c (n "vdm") (v "1.0.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "149y4n7nqziqqf09g5pzfhdfbgjzhd6s5qcp2rxw27s4wb355fwn")))

(define-public crate-vdm-1.0.2 (c (n "vdm") (v "1.0.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1iw5rw5xpki318ql9jy4zj27fx9m9aw85b78y35nppy7vmral1fn")))

(define-public crate-vdm-1.0.3 (c (n "vdm") (v "1.0.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1k8qg6glxaibgmvll8y349hdv4cnyysdz33zddd39cmf7j8b1i04")))

(define-public crate-vdm-1.0.4 (c (n "vdm") (v "1.0.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1idvrabhh4vdwaxmlzm2h6411i9k63qhlspq2ng4xkx5azm12p7z")))

(define-public crate-vdm-1.0.5 (c (n "vdm") (v "1.0.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1lc6cbvk6yxlwxji8w0s8ylh9d6zx8400gz5aiqhpc0h10d36qhx")))

(define-public crate-vdm-1.0.6 (c (n "vdm") (v "1.0.6") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1l03ani1r4fkqffypx85mrb5m99ha2mih1wycnicbd0kd8rks9px")))

(define-public crate-vdm-1.0.7 (c (n "vdm") (v "1.0.7") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0n4p9ndsmip1fwmmi907h4rx90qyvcqnl7ljz1a74nw75x8wnb8c")))

(define-public crate-vdm-1.0.8 (c (n "vdm") (v "1.0.8") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "18yza55wc30y20hkg61rc8ny6ds6gj9kcb4p2djlx9spx45pqz2r")))

