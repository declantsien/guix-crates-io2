(define-module (crates-io #{3}# v vrd) #:use-module (crates-io))

(define-public crate-vrd-0.0.1 (c (n "vrd") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01n3ibsajlk2s5psyfkmjpz66yl05ndbqf4rwmg5syrc8bgyk1kk") (f (quote (("default")))) (r "1.66.1")))

(define-public crate-vrd-0.0.2 (c (n "vrd") (v "0.0.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)))) (h "16xvqbr89wy4657hd6myfrj73ah5ql8i3sp7yzaljn3k966r376s") (f (quote (("default")))) (r "1.69.0")))

(define-public crate-vrd-0.0.3 (c (n "vrd") (v "0.0.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hfy0rfkjajd4cwl2v6jjxvafqm6aqsayz6myg0sz5g7kmglfksm") (f (quote (("default")))) (r "1.69.0")))

(define-public crate-vrd-0.0.4 (c (n "vrd") (v "0.0.4") (d (list (d (n "assert_cmd") (r "^2.0.11") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "089582h1m1qpnlylqqvcamlcs80ck2yachl13gx7lgag81dqn6x8") (f (quote (("default")))) (r "1.69.0")))

(define-public crate-vrd-0.0.5 (c (n "vrd") (v "0.0.5") (d (list (d (n "assert_cmd") (r "^2.0.12") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bxl5dl1kdj19857x428cgizymg9571i37dmf6zgb0bls6w6f47f") (f (quote (("default")))) (r "1.71.1")))

(define-public crate-vrd-0.0.6 (c (n "vrd") (v "0.0.6") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dtt") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rlg") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1pj8mhy8fmgw4s6mpg3madpcrvhcfqx8mx340n6pbj4fgz3avi9n") (f (quote (("default")))) (r "1.71.1")))

(define-public crate-vrd-0.0.7 (c (n "vrd") (v "0.0.7") (d (list (d (n "assert_cmd") (r "^2.0.14") (d #t) (k 2)) (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "dtt") (r "^0.0.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rlg") (r "^0.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "102c84jwj004vgs8y68ffa6g7s83h7nsgpxn442n0j1gh804rz88") (f (quote (("default")))) (r "1.60")))

