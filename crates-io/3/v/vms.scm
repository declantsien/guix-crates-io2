(define-module (crates-io #{3}# v vms) #:use-module (crates-io))

(define-public crate-vms-0.1.0 (c (n "vms") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1x3chdib6hmmkyy0bshzz5d2xl2z6v5ggplsx08v9ik00wqrmfx2")))

(define-public crate-vms-0.1.1 (c (n "vms") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.5") (d #t) (k 0)) (d (n "shellexpand") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0mrh1snjsn5fjk1m0168kxyzmbmmjbcg5xljkzbnzkvhnin2z9mj")))

