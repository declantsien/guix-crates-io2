(define-module (crates-io #{3}# v vsv) #:use-module (crates-io))

(define-public crate-vsv-2.0.0 (c (n "vsv") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.55") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "clap") (r "^3.1.1") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "yansi") (r "^0.5.0") (d #t) (k 0)))) (h "11rz7195nbsnij7bqxnizfx7012gavayac9r90gc764647kxv0az")))

