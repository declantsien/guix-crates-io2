(define-module (crates-io #{3}# v vml) #:use-module (crates-io))

(define-public crate-vml-0.0.1 (c (n "vml") (v "0.0.1") (h "0p9jhpf07ql2d14s9d4z6azz5s1wj0zi40z9l7akmiayinzii8q7") (y #t)))

(define-public crate-vml-0.0.2 (c (n "vml") (v "0.0.2") (h "1aq20w78h2gx94f89gslx7pli6wl16xw3cjjka18y53wn1hhzj72") (y #t)))

