(define-module (crates-io #{3}# v vox) #:use-module (crates-io))

(define-public crate-vox-0.1.0 (c (n "vox") (v "0.1.0") (h "1sbklmv4nszjnzkvjpyzish9bzsfdiwzs4lim0lx0sz3vaxwlnhb") (y #t)))

(define-public crate-vox-0.1.1 (c (n "vox") (v "0.1.1") (h "1wm94bwv66lr77ikz4gb5yaxfk5ka0pa3jbj4vaqx5g0j04wnbwy") (y #t)))

