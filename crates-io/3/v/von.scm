(define-module (crates-io #{3}# v von) #:use-module (crates-io))

(define-public crate-von-0.0.0 (c (n "von") (v "0.0.0") (d (list (d (n "bigdecimal") (r "^0.3.0") (d #t) (k 0)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 0)) (d (n "peginator") (r "^0.3.0") (d #t) (k 1)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 2)) (d (n "unicode-ident") (r "^1.0.5") (d #t) (k 0)) (d (n "voml-error") (r "^0.1.0") (f (quote ("peginator"))) (d #t) (k 0)))) (h "1467k9pmmhb1a1hasmpffnp59z25xc1yb30zmkzb7wnsdrqp16y1") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "bigdecimal/serde" "indexmap/serde" "num/serde"))))))

