(define-module (crates-io #{3}# v vrf) #:use-module (crates-io))

(define-public crate-vrf-0.1.0 (c (n "vrf") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0k5xqzadfrxlsgwdniiarm26sxbpdngfzzn9inwd936y9667q6gp")))

(define-public crate-vrf-0.1.1 (c (n "vrf") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "0h8s50ayp2cmkx7v85cijkf0rhi8m7zs0yly6n564fg0p4dc933b")))

(define-public crate-vrf-0.1.2 (c (n "vrf") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "1v7a34zwal4d28rf93z5ry2nr5dljj4k6gxsljl443npsa8z2q9n")))

(define-public crate-vrf-0.2.0 (c (n "vrf") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)))) (h "12ym5h9iwinnawn122pfzgygnwjwgf158h0s9zzbwakxcsb2sf27")))

(define-public crate-vrf-0.2.1 (c (n "vrf") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0l56j31m85hdvm11dn44x7lk1zr6wiysflszyrj8d89gv8l2wpyl")))

(define-public crate-vrf-0.2.2 (c (n "vrf") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 0)) (d (n "hmac-sha256") (r "^0.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "15v3cnphiwb551nh1mvdq0qr2qmmxxm1q7c3cdic068bz1qp2cki")))

(define-public crate-vrf-0.2.3 (c (n "vrf") (v "0.2.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac-sha256") (r "^0.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0kx1xm2bl29c48p4kn2v4kjnv47p3dprnc7m6ybiqcfa99vxp46i")))

(define-public crate-vrf-0.2.4 (c (n "vrf") (v "0.2.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 2)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "hmac-sha256") (r "^1.1.2") (d #t) (k 0)) (d (n "openssl") (r "^0.10.38") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0ic9l0g0ycybnb72w8nqcq8y35wvvzz7hdvcfy9a42w4nlyr9ygg") (f (quote (("vendored" "openssl/vendored"))))))

