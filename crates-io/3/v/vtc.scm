(define-module (crates-io #{3}# v vtc) #:use-module (crates-io))

(define-public crate-vtc-0.1.5 (c (n "vtc") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1di5vj5g7q0wh9vg6v5v60yb55ydgv1c4nad7hss9wa1crrr16ds")))

(define-public crate-vtc-0.1.6 (c (n "vtc") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1b6nzjqplfc0j7kir77w59rzni86bw96p881n9q4sz4h2f077p1m")))

(define-public crate-vtc-0.1.7 (c (n "vtc") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1na1ml3x3krs4wpwdvhq8iqk3jjpx7dzlcw1gb6wybh2s1n0jpyg")))

(define-public crate-vtc-0.1.8 (c (n "vtc") (v "0.1.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1s2jcr29qhywswq62j6b7md3v80h7pdx2csddp9khcdmyjbrblfv")))

(define-public crate-vtc-0.1.9 (c (n "vtc") (v "0.1.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0lcwfqd8yvqjnj4qs8h5321imyv9hm12yfd1gz0hdsq66ml2kf78")))

(define-public crate-vtc-0.1.10 (c (n "vtc") (v "0.1.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dxqja3wx8cs7fl1d9fm0g1hld7hgigi6v1xwwzprillcdngzysm")))

(define-public crate-vtc-0.1.11 (c (n "vtc") (v "0.1.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0x4phk3ipadm9wgc8wisjk8y2cdr51as5xnf6xmga9r2swx72ygd")))

(define-public crate-vtc-0.1.12 (c (n "vtc") (v "0.1.12") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1s0qp49pxbyvygmccl35f9xnhh2sffwzng436cph63np6d6gkqxy")))

(define-public crate-vtc-0.1.13 (c (n "vtc") (v "0.1.13") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "rstest") (r "^0.9.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "052n5jgl2vf6nm3vx234gllfhgrf57gh06nxvbakq4kixrr64k9n")))

