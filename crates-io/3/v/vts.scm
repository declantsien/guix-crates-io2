(define-module (crates-io #{3}# v vts) #:use-module (crates-io))

(define-public crate-vts-1.0.0 (c (n "vts") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "02k5s4xhqg13lc0ac35531gjap1qwwyb8g43gf79b7gpyf184r2s")))

(define-public crate-vts-1.1.0 (c (n "vts") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "1h7xyr53p98bqk5xbi54a8v5dc33hk8s547dg15lxql54a0384s2") (y #t)))

(define-public crate-vts-1.1.1 (c (n "vts") (v "1.1.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0ql0gz5024gryhhgdvjjb3kdpl74m72kffnygz7bqc05nj5frfhh")))

(define-public crate-vts-1.1.2 (c (n "vts") (v "1.1.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "0pgir8mxw453584b10h3rbrbhm04rsf2pxddiqdz8ha265zzv54w")))

