(define-module (crates-io #{3}# v vdb) #:use-module (crates-io))

(define-public crate-vdb-0.1.0 (c (n "vdb") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0sqamw8rbp9c13fm2zfzl9jmxha6c6nppms1b6vdf0zbp01bqp64")))

(define-public crate-vdb-0.2.0 (c (n "vdb") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1ra40jsksa8nvkg5kmnhca9av5j26c76n0zcwyvdb0z761rb9cn7")))

(define-public crate-vdb-0.3.0 (c (n "vdb") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "00i4487rf44n70mqcb187yvdq71ck1vibi58x7mgdnjsr88wmpy6")))

(define-public crate-vdb-0.4.0 (c (n "vdb") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (d #t) (k 0)) (d (n "serde_derive") (r "1.0.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "0z1fwg8s1nz7ql6q9s6z2sqa5z4p9x7rphbakrar3kccq25cgv9m")))

