(define-module (crates-io #{3}# v vqr) #:use-module (crates-io))

(define-public crate-vqr-0.0.0 (c (n "vqr") (v "0.0.0") (d (list (d (n "async-trait") (r "^0.1.50") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m4dpdrsgw75j418l1hy5fwd7yjs75k7jfn3ycv351zy67hpcd04")))

