(define-module (crates-io #{3}# v vit) #:use-module (crates-io))

(define-public crate-vit-1.0.0 (c (n "vit") (v "1.0.0") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)))) (h "0m57hfa0i93za7mkj078gwr4vpzkif4jmyha11kjfbdklqd54z6k")))

(define-public crate-vit-1.1.1 (c (n "vit") (v "1.1.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.12") (d #t) (k 0)))) (h "0m2p2d3ib2r00mblcirm3xfp37y75rf68ya169rnwwgpf99jkrc8")))

(define-public crate-vit-1.2.1 (c (n "vit") (v "1.2.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.12") (d #t) (k 0)))) (h "08dkxkm4l9rdyc6y32ndcjz5a0vxyd77p5ff30mpxrb3wlglny3p") (f (quote (("default" "clipboard")))) (s 2) (e (quote (("clipboard" "dep:clipboard"))))))

(define-public crate-vit-1.2.3 (c (n "vit") (v "1.2.3") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "image") (r "^0.24.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.12") (d #t) (k 0)))) (h "1mn7nylgfl8siy4yn28zzki3sy0miq32jc9i6vhkry84kk3fg37i")))

