(define-module (crates-io #{3}# v vpp) #:use-module (crates-io))

(define-public crate-vpp-0.0.0 (c (n "vpp") (v "0.0.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-types") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0w3n71r9k4fzh45ksajs2qq47x32bzb29z3qh5nsa37a5w95aj4j") (f (quote (("default"))))))

(define-public crate-vpp-0.0.1 (c (n "vpp") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive" "unicode" "error-context"))) (d #t) (k 0)) (d (n "json5") (r "^0.4.1") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-types") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0xni7drajshwid10r2p68k9xwv2n3svnsa9lr65a1pjpr4fn2jhx") (f (quote (("default"))))))

