(define-module (crates-io #{3}# v veg) #:use-module (crates-io))

(define-public crate-veg-0.1.0 (c (n "veg") (v "0.1.0") (h "1p4ag5q7rh8ysk9rsxpcr38990jyc65hg0dddxk6yaav23b6z7sl")))

(define-public crate-veg-0.1.1 (c (n "veg") (v "0.1.1") (h "1i7dc1gvl1arhi3jkg045gcwmz491b7bsy00hc6vya0kc9qi7lq4")))

(define-public crate-veg-0.2.0 (c (n "veg") (v "0.2.0") (h "1wiyyp547zxijg2431m3rfxyfm6l8ig1cdklzj4cmlfiv71gbgyp")))

(define-public crate-veg-0.2.1 (c (n "veg") (v "0.2.1") (h "159hz2xbq6xga6m257rhg34vkjvsz1fxh2gss2004zf1dr3h2amg")))

(define-public crate-veg-0.3.0 (c (n "veg") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)))) (h "0j7rb30fzfwymqz7lp0jqs6qdc80lqwla3x4gxqk9mgygmry4g90")))

(define-public crate-veg-0.4.0 (c (n "veg") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (o #t) (d #t) (k 0)))) (h "0279nfhzy7lly7ndiwn8zw8h6j3wmb45azakvbr5w0cy1534v9fq") (s 2) (e (quote (("colored" "dep:colored" "dep:strip-ansi-escapes" "dep:unicode-segmentation"))))))

(define-public crate-veg-0.4.1 (c (n "veg") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (o #t) (d #t) (k 0)))) (h "173jlhl27psl7m3q5pw32z9c5cyd4b8sfsrn1xd7vnyij86zq19a") (s 2) (e (quote (("colored" "dep:colored" "dep:strip-ansi-escapes" "dep:unicode-segmentation"))))))

(define-public crate-veg-0.4.2 (c (n "veg") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (o #t) (d #t) (k 0)))) (h "0i9bxlnx3qgsvdh3mncq4xi7d61x5ad52cf3qxsxr4a78mb4gpsg") (s 2) (e (quote (("colored" "dep:colored" "dep:strip-ansi-escapes" "dep:unicode-segmentation"))))))

(define-public crate-veg-0.4.3 (c (n "veg") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (o #t) (d #t) (k 0)))) (h "0g9g6qja60ab5lqyww4kpdlbzdcmp1jrfk9xyvy6jjlwyf5zcych") (s 2) (e (quote (("colored" "dep:colored" "dep:strip-ansi-escapes" "dep:unicode-segmentation"))))))

(define-public crate-veg-0.4.4 (c (n "veg") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (o #t) (d #t) (k 0)))) (h "0mxwf27q1dxnivqdhwa3sqbdw2fbw9qfwz40p7n7byp7vlg75syq") (s 2) (e (quote (("colored" "dep:colored" "dep:strip-ansi-escapes" "dep:unicode-segmentation"))))))

(define-public crate-veg-0.4.5 (c (n "veg") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (o #t) (d #t) (k 0)))) (h "066hk7nm5sfn1g1ynjy6zk62s2996y81v85ab03yn6slp1b9drf6") (s 2) (e (quote (("colored" "dep:colored" "dep:strip-ansi-escapes" "dep:unicode-segmentation"))))))

(define-public crate-veg-0.4.6 (c (n "veg") (v "0.4.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (o #t) (d #t) (k 0)))) (h "148l4m23q8b8c5kki898h7c5qzd3n6zg1a0cz9rvy4krr60zw7lp") (s 2) (e (quote (("colored" "dep:colored" "dep:strip-ansi-escapes" "dep:unicode-segmentation"))))))

(define-public crate-veg-0.4.7 (c (n "veg") (v "0.4.7") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (o #t) (d #t) (k 0)))) (h "1wcwj1fpflr6v5qhf50pgkw3hi81dipicigc8n9cbdlf824jsk48") (s 2) (e (quote (("colored" "dep:colored" "dep:strip-ansi-escapes" "dep:unicode-segmentation"))))))

(define-public crate-veg-0.4.8 (c (n "veg") (v "0.4.8") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (o #t) (d #t) (k 0)))) (h "055df2w4xwyva4mpsgvvq10mkvrmjqf1vpx08fh9acisywixnzv5") (s 2) (e (quote (("colored" "dep:colored" "dep:strip-ansi-escapes" "dep:unicode-segmentation"))))))

(define-public crate-veg-0.5.0 (c (n "veg") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (o #t) (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.11.0") (d #t) (k 0)))) (h "19zfzl1bnfxzhww8h2a1yc4066cd2qdnxcnmzvy7ffmwh8bfc5xb") (s 2) (e (quote (("colored" "dep:colored"))))))

