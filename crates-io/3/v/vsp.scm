(define-module (crates-io #{3}# v vsp) #:use-module (crates-io))

(define-public crate-vsp-0.1.0 (c (n "vsp") (v "0.1.0") (d (list (d (n "bincode") (r "^1") (d #t) (k 0)) (d (n "bson") (r "^2.0") (d #t) (k 0)) (d (n "erased-serde") (r "^0.3") (d #t) (k 0)) (d (n "flexbuffers") (r "^2.0") (d #t) (k 0)) (d (n "postcard") (r "^0.7") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde-pickle") (r "^0.6") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0xgkvvmgknm4lgzijs436hglps4nlkb25fl12dbj0na16y23r1hy")))

