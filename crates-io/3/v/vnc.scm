(define-module (crates-io #{3}# v vnc) #:use-module (crates-io))

(define-public crate-vnc-0.1.0 (c (n "vnc") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.13") (d #t) (k 0)) (d (n "x11") (r "^2.3") (d #t) (k 0)))) (h "0fi76ralwg1wkiq4kj2al9287nal6v6jjarkksim2hyfkljgg7ax")))

(define-public crate-vnc-0.2.0 (c (n "vnc") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.13") (d #t) (k 0)) (d (n "x11") (r "^2.3") (d #t) (k 0)))) (h "1n9xai6wd985j8g2y6cnflr047h0fa44dm4zn7ys9iyvqyi9ds0d")))

(define-public crate-vnc-0.3.0 (c (n "vnc") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^1.5") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.13") (d #t) (k 0)) (d (n "x11") (r "^2.3") (d #t) (k 0)))) (h "00bca6xm8mcy0d9cfpj4l93zfp6xn4glij28hzxbwjv0la822hg0")))

(define-public crate-vnc-0.4.0 (c (n "vnc") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "flate2") (r "^0.2.13") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1nym5b99p3i995kvn0c25mk2qm0xqbls72znpizfrm7qjczkxrhf")))

