(define-module (crates-io #{3}# v vga) #:use-module (crates-io))

(define-public crate-vga-0.1.0 (c (n "vga") (v "0.1.0") (d (list (d (n "conquer-once") (r "^0.2.0") (k 0)) (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.9.6") (d #t) (k 0)))) (h "0kfa7lq6rjghj4mkh7zalf8n98411k9kbfd5mg0pkczbvy7wqz8z")))

(define-public crate-vga-0.1.1 (c (n "vga") (v "0.1.1") (d (list (d (n "conquer-once") (r "^0.2.0") (k 0)) (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.9.6") (d #t) (k 0)))) (h "1q2ki9n6dg2cy71wplhk588v2980arhn2hklvq4smqdfxl73h9zy")))

(define-public crate-vga-0.1.2 (c (n "vga") (v "0.1.2") (d (list (d (n "conquer-once") (r "^0.2.0") (k 0)) (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.9.6") (d #t) (k 0)))) (h "0h7g8ssypflaynq3vyc4fys7drf1ah5dkacpmsx3qdj8aqr1fwhm")))

(define-public crate-vga-0.2.0 (c (n "vga") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "conquer-once") (r "^0.2.0") (k 0)) (d (n "font8x8") (r "^0.2.5") (f (quote ("unicode"))) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.9.6") (d #t) (k 0)))) (h "1gcswy34jias97k79vnrcsbxzgqrcifbibaz1kx6lbsdffkqrwkw")))

(define-public crate-vga-0.2.1 (c (n "vga") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "conquer-once") (r "^0.2.0") (k 0)) (d (n "font8x8") (r "^0.2.5") (f (quote ("unicode"))) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.9.6") (d #t) (k 0)))) (h "1q7fxaf0w1mkd9n26h74wwhhm2zl2wmqgdvc5liwadyj518qnhx6")))

(define-public crate-vga-0.2.2 (c (n "vga") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "conquer-once") (r "^0.2.0") (k 0)) (d (n "font8x8") (r "^0.2.5") (f (quote ("unicode"))) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.9.6") (d #t) (k 0)))) (h "1d88nd022ipnfmysj78rr3vpghy6lx28y6s268ldwfi6v6qarsfn")))

(define-public crate-vga-0.2.3 (c (n "vga") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "conquer-once") (r "^0.2.0") (k 0)) (d (n "font8x8") (r "^0.2.5") (f (quote ("unicode"))) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.9.6") (d #t) (k 0)))) (h "0vn638bpzyzx8jpwh56n34fha9w2n5w9wq9y5liq3fjzhw62mszm")))

(define-public crate-vga-0.2.4 (c (n "vga") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "conquer-once") (r "^0.2.0") (k 0)) (d (n "font8x8") (r "^0.2.5") (f (quote ("unicode"))) (k 0)) (d (n "num-traits") (r "^0.2.11") (k 0)) (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.11.0") (d #t) (k 0)))) (h "05crij07gd5mgyxdf3wwy8bkqsm8arap3yijhsi4bcj6w62959lf")))

(define-public crate-vga-0.2.5 (c (n "vga") (v "0.2.5") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "conquer-once") (r "^0.2.1") (k 0)) (d (n "font8x8") (r "^0.2.5") (f (quote ("unicode"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "spinning_top") (r "^0.2.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.12.2") (d #t) (k 0)))) (h "1lzpyv2991zwhckm3gdk32b93c0byadmc1s9dz84y6frlcs3hpwg")))

(define-public crate-vga-0.2.6 (c (n "vga") (v "0.2.6") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "conquer-once") (r "^0.2.1") (k 0)) (d (n "font8x8") (r "^0.2.5") (f (quote ("unicode"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "spinning_top") (r "^0.2.2") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.13.2") (d #t) (k 0)))) (h "10hnrsizjq8l4psxgpf126qqs0rf1ddyziskzzzg7jm5cpk007vr")))

(define-public crate-vga-0.2.7 (c (n "vga") (v "0.2.7") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "font8x8") (r "^0.3.1") (f (quote ("unicode"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.14.2") (d #t) (k 0)))) (h "1gq12302hfgwcinfwvg380kyc3ybsvgw7wmqzxvd367rzxxwpjv7")))

(define-public crate-vga-0.2.8 (c (n "vga") (v "0.2.8") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "font8x8") (r "^0.3.1") (f (quote ("unicode"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.14.2") (d #t) (k 0)))) (h "0bq6ydr8yfplk7rkcsad9plch3373pd04c7swvjy1674j27z3v82")))

(define-public crate-vga-0.2.9 (c (n "vga") (v "0.2.9") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "conquer-once") (r "^0.3.2") (k 0)) (d (n "font8x8") (r "^0.3.1") (f (quote ("unicode"))) (k 0)) (d (n "num-traits") (r "^0.2.14") (k 0)) (d (n "spinning_top") (r "^0.2.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.14.2") (d #t) (k 0)))) (h "1ilv57qvwzkrfkz224vczrgqyzdcf4xrxqhydr0f0f7rkhx1dpd5")))

