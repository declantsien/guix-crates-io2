(define-module (crates-io #{3}# v vbo) #:use-module (crates-io))

(define-public crate-vbo-0.1.0 (c (n "vbo") (v "0.1.0") (d (list (d (n "dms-coordinates") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("formatting"))) (d #t) (k 0)))) (h "12cvxgp9wi3ic8myhk63n9fwq2p00zp555sap1njj2as3c4ql74y")))

