(define-module (crates-io #{3}# v vla) #:use-module (crates-io))

(define-public crate-vla-0.0.0 (c (n "vla") (v "0.0.0") (h "1q6cd8v324zy6ydmkr71b2z0xjki9xpjnk9qabjxv7z0sw82ckfs") (y #t)))

(define-public crate-vla-0.1.0 (c (n "vla") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0a7gkm8lhdk9azmcnzdhafj32na7qpzwczwpbqsgvpcp10f504iz") (y #t)))

(define-public crate-vla-0.1.1 (c (n "vla") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1kmmnl3660sh7m7p43p13ayjs9maicmdpcn76rd2vlr51jb9dv6h") (y #t)))

