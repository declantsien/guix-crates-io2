(define-module (crates-io #{3}# v vkt) #:use-module (crates-io))

(define-public crate-vkt-0.1.0 (c (n "vkt") (v "0.1.0") (d (list (d (n "futures") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0") (d #t) (k 0)) (d (n "log4rs") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15y2wrf9k11nr8km94zqjnx2bcmis6naqsviall7rqfdxvkrhlcf")))

