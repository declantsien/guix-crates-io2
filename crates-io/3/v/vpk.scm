(define-module (crates-io #{3}# v vpk) #:use-module (crates-io))

(define-public crate-vpk-0.1.0 (c (n "vpk") (v "0.1.0") (h "1n15qpwn1v10l48pp10j6qpy4h3h9rdxw49d7jbpkbfn6yp3ffyj")))

(define-public crate-vpk-0.1.1 (c (n "vpk") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)))) (h "147sc2x971in7pmachkingly8qx3cpyfsq30hy1q8flv79kv6wrn")))

(define-public crate-vpk-0.1.2 (c (n "vpk") (v "0.1.2") (d (list (d (n "binread") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "175plrih1ds5r161s0kw2j1v8d8jkdivlxzgqs0sgcn0052cvlxp")))

(define-public crate-vpk-0.1.4 (c (n "vpk") (v "0.1.4") (d (list (d (n "binread") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0gzw98kkqhbj4z0i3sxkmvfwclvw8r96vw7xabx268mhvjl82r2c")))

(define-public crate-vpk-0.2.0 (c (n "vpk") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.6") (d #t) (k 0)) (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "iai") (r "^0.1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0svvv351n0siazshyyfb1x8chvwwgl12yisd97amhpsi67ki1v30") (r "1.66")))

