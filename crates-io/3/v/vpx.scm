(define-module (crates-io #{3}# v vpx) #:use-module (crates-io))

(define-public crate-vpx-0.1.0 (c (n "vpx") (v "0.1.0") (d (list (d (n "libvpx-sys") (r "*") (d #t) (k 0)))) (h "1s0vkhrzpc77wrnm9gyj50pq3wd4pk59mc329qrafbzxr5bbbk4g")))

(define-public crate-vpx-0.2.0 (c (n "vpx") (v "0.2.0") (d (list (d (n "libvpx-sys") (r "*") (d #t) (k 0)))) (h "16qc6ils8zhkmx8z3ncaxk6p0c17qgqndr7c113ldalls0myfqg0")))

(define-public crate-vpx-0.3.0 (c (n "vpx") (v "0.3.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libvpx-sys") (r "*") (d #t) (k 0)))) (h "0y0v0xsjnnbq3pvjgcsjcpaglcw24641nlcfr8wig0wvfdyc65di") (y #t)))

(define-public crate-vpx-0.3.1 (c (n "vpx") (v "0.3.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libvpx-sys") (r "*") (d #t) (k 0)))) (h "1bydbcb9c797pzcn9mw1vsml6r1pfij1272b7ywcdfbxmkhhkcmf")))

