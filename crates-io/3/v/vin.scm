(define-module (crates-io #{3}# v vin) #:use-module (crates-io))

(define-public crate-vin-1.0.0 (c (n "vin") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "14lvswsy2191mvzd2nsbb4clj1dfn17jvl8rmdssz7w7qvp4hpf4") (y #t)))

(define-public crate-vin-2.0.0 (c (n "vin") (v "2.0.0") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^1.0") (d #t) (k 0)) (d (n "vin-macros") (r "^1.0") (d #t) (k 0)))) (h "1wl0f8yy674l2jir6wxm5api3bglpnwfsgvy60d4kzvwf1257ln7")))

(define-public crate-vin-2.0.1 (c (n "vin") (v "2.0.1") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^1.0") (d #t) (k 0)) (d (n "vin-macros") (r "^1.0") (d #t) (k 0)))) (h "0dn8viddq8zasxqs4kw84b9za7a7v8i0fp39c55akwb82vr6g9y8")))

(define-public crate-vin-2.0.2 (c (n "vin") (v "2.0.2") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^1.0") (d #t) (k 0)) (d (n "vin-macros") (r "^1.0") (d #t) (k 0)))) (h "0azqy1s2rfnbjd93406plfb1lsmcy38iisy5k0rqfrjlabyfz8vr")))

(define-public crate-vin-2.1.2 (c (n "vin") (v "2.1.2") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^1.1") (d #t) (k 0)) (d (n "vin-macros") (r "^1.0") (d #t) (k 0)))) (h "0lz4jzi2zir1j6xjci7sv8vhsca5qkr81ri3ii2bdvard83mys61")))

(define-public crate-vin-2.1.3 (c (n "vin") (v "2.1.3") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^1.1") (d #t) (k 0)) (d (n "vin-macros") (r "^1.0") (d #t) (k 0)))) (h "0zvh4x595rjy4ypg72xli9mcxw2mzk3dvfawnvcijx29j3g04l51")))

(define-public crate-vin-2.1.4 (c (n "vin") (v "2.1.4") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^1.1") (d #t) (k 0)) (d (n "vin-macros") (r "^1.0") (d #t) (k 0)))) (h "1436i2sb6cgacvgqnf7pc42wrfkr6qfcpjhb94yv24b392j9pv0y")))

(define-public crate-vin-2.1.5 (c (n "vin") (v "2.1.5") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^1.1") (d #t) (k 0)) (d (n "vin-macros") (r "^1.0") (d #t) (k 0)))) (h "0nrs0phd8dc42191jks0c729jgrbvd48mxr0rb5qqdfzfslikf6k")))

(define-public crate-vin-2.2.5 (c (n "vin") (v "2.2.5") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^1.1") (d #t) (k 0)) (d (n "vin-macros") (r "^1.0") (d #t) (k 0)))) (h "08mhzqgw2iwjh26vdrs9l2lysk6cdgaa6lcad46c43lqf0irdb7x")))

(define-public crate-vin-2.2.6 (c (n "vin") (v "2.2.6") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^1.2") (d #t) (k 0)) (d (n "vin-macros") (r "^1.2") (d #t) (k 0)))) (h "0ws0kv6ygn96ss348l0rx3jkvjp1227znc1ny2wqzd7ghai0pzwd")))

(define-public crate-vin-3.0.0 (c (n "vin") (v "3.0.0") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^2.0") (d #t) (k 0)) (d (n "vin-macros") (r "^2.0") (d #t) (k 0)))) (h "1zmbw97kag69clq1dillv78a36v0p5b8g88hkfnfy8rxb7wqihk7")))

(define-public crate-vin-3.1.0 (c (n "vin") (v "3.1.0") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^2.1") (d #t) (k 0)) (d (n "vin-macros") (r "^2.1") (d #t) (k 0)))) (h "0bfan87lfkhfywx3qp3qn25jqz0p3w0vyl2yh86kkmvn7bl56bp1")))

(define-public crate-vin-3.1.1 (c (n "vin") (v "3.1.1") (d (list (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^2.1") (d #t) (k 0)) (d (n "vin-macros") (r "^2.1") (d #t) (k 0)))) (h "044c8fxhk7snbnmppi5ni35ib5hw5ymrc4l3vmlspxylh7m4kavn")))

(define-public crate-vin-4.0.0 (c (n "vin") (v "4.0.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^3.0") (d #t) (k 0)) (d (n "vin-macros") (r "^3.0") (d #t) (k 0)))) (h "1hdjy0p8m4kjg71w2dlhgf9bx2ckw4zxjcav29xzb4vm7ygxaj13")))

(define-public crate-vin-5.0.0 (c (n "vin") (v "5.0.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^4.0") (d #t) (k 0)) (d (n "vin-macros") (r "^4.0") (d #t) (k 0)))) (h "1d5c2gwzgfssjhv4jmhm0qwbqfglzr2shv9pcaqmh2ii5rf0ag5q")))

(define-public crate-vin-5.0.1 (c (n "vin") (v "5.0.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^4.0") (d #t) (k 0)) (d (n "vin-macros") (r "^4.0") (d #t) (k 0)))) (h "0px5yayzzqjsyihplrkfsv357mljacm1ns102cwzasasrw0x2dyz")))

(define-public crate-vin-5.0.2 (c (n "vin") (v "5.0.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^4.0") (d #t) (k 0)) (d (n "vin-macros") (r "^4.0") (d #t) (k 0)))) (h "0rgpqhs4p58ksrpq8xlwgj8ixqfdh0ssq386854zcl70qbcxs7vh")))

(define-public crate-vin-5.0.3 (c (n "vin") (v "5.0.3") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^4.0") (d #t) (k 0)) (d (n "vin-macros") (r "^4.0") (d #t) (k 0)))) (h "1k6wsigj3yxr61hyz80y17hsg9r2bsdafwbq3dydm6586xnkrqa1")))

(define-public crate-vin-5.0.4 (c (n "vin") (v "5.0.4") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^4.0") (d #t) (k 0)) (d (n "vin-macros") (r "^4.0") (d #t) (k 0)))) (h "0jfnjajqkhqay2c57cwafcc634shdh6zrgg65k2qlpjd9r39j3dg")))

(define-public crate-vin-5.1.0 (c (n "vin") (v "5.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^4.1") (d #t) (k 0)) (d (n "vin-macros") (r "^4.1") (d #t) (k 0)))) (h "00snxyx9jd326bnv9in64jwm7ks8xvmvjlbikh4mk8f26xmx32dc")))

(define-public crate-vin-5.1.1 (c (n "vin") (v "5.1.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^4.1") (d #t) (k 0)) (d (n "vin-macros") (r "^4.1") (d #t) (k 0)))) (h "0s3vf3dlzd90ww0l4az7v7x4kisx8k5mph6i86p7sj7g7yqqbmqw")))

(define-public crate-vin-6.0.0 (c (n "vin") (v "6.0.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^5.0") (d #t) (k 0)) (d (n "vin-macros") (r "^5.0") (d #t) (k 0)))) (h "1n0qh7nkb25wk4wpm88nawkmaqim1v0iviz5n4y0a2vfv3yxl4x1")))

(define-public crate-vin-6.0.1 (c (n "vin") (v "6.0.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^5.0") (d #t) (k 0)) (d (n "vin-macros") (r "^5.0") (d #t) (k 0)))) (h "1wrpmpzmhbljy5wm2avayr95mgykyf34g3plyg8i1b0zd7lq4zgj")))

(define-public crate-vin-6.0.2 (c (n "vin") (v "6.0.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^5.0") (d #t) (k 0)) (d (n "vin-macros") (r "^5.0") (d #t) (k 0)))) (h "0lkf5fdlpjc251zap7z9vllavzw4gjsdpf6hdxbkhnfr2dm7fsf2")))

(define-public crate-vin-7.0.0 (c (n "vin") (v "7.0.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^6.0") (d #t) (k 0)) (d (n "vin-macros") (r "^6.0") (d #t) (k 0)))) (h "1jwgq16q80ivzwl8504h1pcpzhm2zz97nci741bzbs3pkn7ax041")))

(define-public crate-vin-7.0.1 (c (n "vin") (v "7.0.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^6.0.0") (d #t) (k 0)) (d (n "vin-macros") (r "^6.0.1") (d #t) (k 0)))) (h "19z6rdnjr64w1z8mcfp5zylx0vk4pq9spnc1rpa7n0x0l5n08m8l")))

(define-public crate-vin-8.0.0 (c (n "vin") (v "8.0.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^7.0.0") (d #t) (k 0)) (d (n "vin-macros") (r "^7.0.0") (d #t) (k 0)))) (h "127qv3d416c91ir25drf177bhml7p8f7fargkhidgaz7k8frm3qd")))

(define-public crate-vin-8.0.1 (c (n "vin") (v "8.0.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^7.0.0") (d #t) (k 0)) (d (n "vin-macros") (r "^7.0.1") (d #t) (k 0)))) (h "1cvd9y09i99jlm31v97yjdfxawy8w63l598jar9ambny8jldvxyn")))

(define-public crate-vin-9.0.0 (c (n "vin") (v "9.0.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^8.0.0") (d #t) (k 0)) (d (n "vin-macros") (r "^8.0.0") (d #t) (k 0)))) (h "09sq0g7dsayy5xx7cq1v9vlw94ljg9scb8a5hxna842qadc2khpa")))

(define-public crate-vin-9.1.0 (c (n "vin") (v "9.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^8.1.0") (d #t) (k 0)) (d (n "vin-macros") (r "^8.0.0") (d #t) (k 0)))) (h "1rc0s9gf6709nvqbs9cdq6vvbahcjgjmpwxjdcdh2nn5nrpqlirj")))

(define-public crate-vin-9.1.1 (c (n "vin") (v "9.1.1") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^8.1.0") (d #t) (k 0)) (d (n "vin-macros") (r "^8.0.0") (d #t) (k 0)))) (h "01wlcsig8gq4xcwsh965qg2xqbxy4ka7h21s3sc3906l7rg8vi9f")))

(define-public crate-vin-9.1.2 (c (n "vin") (v "9.1.2") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^8.1.0") (d #t) (k 0)) (d (n "vin-macros") (r "^8.0.1") (d #t) (k 0)))) (h "1sncpcfigxvk4y52i86jhgy7cxkv9ydi1fiy84gds0bs12k68y40")))

(define-public crate-vin-9.1.3 (c (n "vin") (v "9.1.3") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-log") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3") (d #t) (k 2)) (d (n "vin-core") (r "^8.1.0") (d #t) (k 0)) (d (n "vin-macros") (r "^8.0.2") (d #t) (k 0)))) (h "1h9l4ywfwh9qsc765dqmzpr1fmzkz4qfsd6sp1pssam7s6qiq233")))

