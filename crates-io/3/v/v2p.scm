(define-module (crates-io #{3}# v v2p) #:use-module (crates-io))

(define-public crate-v2p-0.1.0 (c (n "v2p") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "04z5gjh14ymd391f4i3f1ri9f26dm44g2imp24ijhw8wdi2ykr9z")))

