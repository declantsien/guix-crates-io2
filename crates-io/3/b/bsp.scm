(define-module (crates-io #{3}# b bsp) #:use-module (crates-io))

(define-public crate-bsp-0.1.0 (c (n "bsp") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "1f4layx411997c1534wya0haf8a6g1859qc8r4fijz9mxz72m2i4")))

(define-public crate-bsp-0.1.1 (c (n "bsp") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "0gfwlzyr840wccsrz0nhyfkribmqa5b4vxzsng4v88k21frwyvj6")))

(define-public crate-bsp-0.1.2 (c (n "bsp") (v "0.1.2") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "bv") (r "^0.11") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)))) (h "1rs63izax44za31zc84c45q35zpdlywjj7cmlp4g0cgzpjcxdmmr") (f (quote (("bench"))))))

