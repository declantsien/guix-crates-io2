(define-module (crates-io #{3}# b buf) #:use-module (crates-io))

(define-public crate-buf-0.1.0 (c (n "buf") (v "0.1.0") (d (list (d (n "containers") (r "^0.8") (d #t) (k 0)) (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "loca") (r "^0.6") (d #t) (k 0)))) (h "1pxs3c1b7nwmkf2yk94x619nw9asnmzw4k7vcr4y1nsycvlyqw70")))

(define-public crate-buf-0.1.1 (c (n "buf") (v "0.1.1") (d (list (d (n "containers") (r "^0.8") (d #t) (k 0)) (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "loca") (r "^0.6") (d #t) (k 0)))) (h "0k6hkv6v3fd18b2ici9wzqkvszmcvs6zv13qmhk4h7zmvm6zbwk3")))

(define-public crate-buf-0.1.2 (c (n "buf") (v "0.1.2") (d (list (d (n "containers") (r "^0.8") (d #t) (k 0)) (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "loca") (r "^0.6") (d #t) (k 0)))) (h "1lqz3a1ap7jgja9j2f32mh50xrygyfhpg28ml0pyfq4idvl75zyh")))

(define-public crate-buf-0.2.0 (c (n "buf") (v "0.2.0") (d (list (d (n "containers") (r "^0.8") (k 0)) (d (n "either") (r "^1.5") (k 0)) (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "loca") (r "^0.6") (d #t) (k 0)))) (h "14ri41kf1n0kgy99qa3nn54x60p2i3g6ql3aggc5zn2dbar4jjgz")))

(define-public crate-buf-0.2.1 (c (n "buf") (v "0.2.1") (d (list (d (n "containers") (r "^0.8") (k 0)) (d (n "either") (r "^1.5") (k 0)) (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "loca") (r "^0.6") (d #t) (k 0)))) (h "1j3a798aaq33w5glnfzd1hpcjxw7knck4an2hcsxhkl6bbz1s362")))

(define-public crate-buf-0.2.2 (c (n "buf") (v "0.2.2") (d (list (d (n "containers") (r "^0.9") (k 0)) (d (n "either") (r "^1.5") (k 0)) (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "loca") (r "^0.7") (d #t) (k 0)))) (h "1qsh88pd74wr7x9clv8732v2jgv8i1nk4m9kbfzqii88nmycpz1h")))

(define-public crate-buf-0.2.3 (c (n "buf") (v "0.2.3") (d (list (d (n "containers") (r "^0.9") (k 0)) (d (n "either") (r "^1.5") (k 0)) (d (n "i-o") (r "^0.4") (d #t) (k 0)) (d (n "loca") (r "^0.7") (d #t) (k 0)))) (h "0n193v132nyr0amadljxclv5q4pqqpsvf88c2mix7cvbaw7qfdyb")))

