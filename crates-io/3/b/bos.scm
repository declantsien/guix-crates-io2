(define-module (crates-io #{3}# b bos) #:use-module (crates-io))

(define-public crate-bos-0.1.0-rc1 (c (n "bos") (v "0.1.0-rc1") (h "0344jhq88gksw5rxcpwljgscwz9fpw83j7132wyjcf9k7ngdnswn") (f (quote (("unstable"))))))

(define-public crate-bos-0.1.0-rc2 (c (n "bos") (v "0.1.0-rc2") (h "1s7xnzvk92v39nifn76hgdcy91g6nkg4m9wzk9qgl7bfbvzqs01n") (f (quote (("unstable"))))))

(define-public crate-bos-0.1.0 (c (n "bos") (v "0.1.0") (h "1sg9f1r30mjffwlbq2v79vhnirj2kb1i03lzyccm0qm903i54zsw") (f (quote (("unstable"))))))

(define-public crate-bos-0.1.1 (c (n "bos") (v "0.1.1") (h "0xa8asf3b4qdgv5bbpqpmnj0y1v3bzfky0vnff0mbj4r3wbgbx31") (f (quote (("unstable"))))))

(define-public crate-bos-0.2.0 (c (n "bos") (v "0.2.0") (h "0s7mkb7kzmpkl650wiknmyiwp2mz4752a2bp42r27nz10b44ydwa") (f (quote (("unstable"))))))

(define-public crate-bos-0.2.1 (c (n "bos") (v "0.2.1") (h "0kmpx0j524ww4flm8hlkayg16ggp2kqdnl7nazxvmj9h8nsw3s4l") (f (quote (("unstable"))))))

(define-public crate-bos-0.2.2 (c (n "bos") (v "0.2.2") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 2)))) (h "09zsqlacvszg8rpwlgk706pvq5z89ll35rqsn2qfc1g14x5jqcy3") (f (quote (("unstable"))))))

(define-public crate-bos-0.3.0-beta1 (c (n "bos") (v "0.3.0-beta1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 2)))) (h "0x434bcljp30zdlngd1m7257k6q0lm614i84q051ipqzjl3idlv0") (f (quote (("unstable"))))))

(define-public crate-bos-0.3.0 (c (n "bos") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "lifetime") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lifetime") (r "^0.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 2)))) (h "1j5m5jf4lhrsr3clcsv0rhz661xvs5fgz6r9xnzhdwidxp79fs9m") (f (quote (("unstable"))))))

(define-public crate-bos-0.3.1 (c (n "bos") (v "0.3.1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "lifetime") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lifetime") (r "^0.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tap") (r "^1.0") (d #t) (k 2)))) (h "12b69ds9cp1y2knivwzwlm7yirx5cbvisgv6r9vhz3mmvncsp3vv") (f (quote (("unstable"))))))

