(define-module (crates-io #{3}# b bly) #:use-module (crates-io))

(define-public crate-bly-0.1.0 (c (n "bly") (v "0.1.0") (d (list (d (n "bly-ac") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.5.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.1") (d #t) (k 2)) (d (n "bly-cairo") (r "^0.1.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "bly-dx2d") (r "^0.1.0") (d #t) (t "cfg(windows)") (k 0)))) (h "0dp3k0fn2zrhzhlh5bjam42m3g471g5nnr3xvr97ifq4hqi95s37")))

