(define-module (crates-io #{3}# b bow) #:use-module (crates-io))

(define-public crate-bow-0.1.0 (c (n "bow") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (d #t) (k 2)))) (h "187fyx0a8a8zcql0xd72n955ag7z59zhb6bv6mmhsk4zgazw2iag")))

(define-public crate-bow-0.1.1 (c (n "bow") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0h72cjh17j5iw1qivdfkh13zqlksvygx3jr671qh0f9nbfjx38yf") (f (quote (("std") ("default" "std"))))))

(define-public crate-bow-1.0.0 (c (n "bow") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1wi35y2njqblrlkw0vfxiy594hnjw6zvr0sy0ah5flah5xnnqp0n") (f (quote (("std") ("default" "std"))))))

(define-public crate-bow-1.0.1 (c (n "bow") (v "1.0.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)))) (h "1jxh5mjn15r8zd3hnwj3whmwb81xqq91xymp9dcwh4dwizhx5fkk") (f (quote (("std") ("default" "std"))))))

(define-public crate-bow-1.0.2 (c (n "bow") (v "1.0.2") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)))) (h "0ljwvhc5hnaqzg7ck7i1b1i33pdjcsl99ym7xanjd4pg65l85bf0") (f (quote (("std") ("default" "std"))))))

(define-public crate-bow-1.0.3 (c (n "bow") (v "1.0.3") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)))) (h "0c788mlb3yhx3wgzfh12ni94m0qwpncxwr2qa8j46dc5z57sl59m") (f (quote (("std") ("default" "std"))))))

(define-public crate-bow-2.0.0 (c (n "bow") (v "2.0.0") (h "09k8ax14w95kq7r1az2fkcsmimy52l89lzi8al0pavbg0dhsvdms")))

(define-public crate-bow-2.0.1 (c (n "bow") (v "2.0.1") (h "05yjqb4hk7zrmhzx9p5pgm26sq9z69x4yslcisk1njg6b01c4ci1")))

