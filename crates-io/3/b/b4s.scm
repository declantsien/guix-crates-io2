(define-module (crates-io #{3}# b b4s) #:use-module (crates-io))

(define-public crate-b4s-0.1.0 (c (n "b4s") (v "0.1.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "1dy1v7id5kv790q56y8f9nhzd8l2g1xhv2jj8h2cqm27gy47n4vf")))

(define-public crate-b4s-0.2.0 (c (n "b4s") (v "0.2.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "1lcgib162d96hkyqjyg083161732qk09zcflh84z15w29lhfiwf4") (r "1.64.0")))

(define-public crate-b4s-0.3.0 (c (n "b4s") (v "0.3.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "0k5b8jb1rv77q86h82lf6dxbca86x3brpc31ppjvn8ha9n7afdam") (r "1.64.0")))

(define-public crate-b4s-0.3.1 (c (n "b4s") (v "0.3.1") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "0h0mn3lfbvkw22w7aixxx6mlv6skslxkx17iwprj20n3zd4i48dv") (r "1.64.0")))

(define-public crate-b4s-0.3.2 (c (n "b4s") (v "0.3.2") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "trie-rs") (r "^0.1.1") (d #t) (k 2)))) (h "00dag8q3by057rpx2kgxfms7nnypdkqp5gypy06adkdxh2y20sb4") (r "1.64.0")))

(define-public crate-b4s-0.3.3 (c (n "b4s") (v "0.3.3") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "trie-rs") (r "^0.1.1") (d #t) (k 2)))) (h "0zq672xcn9zixkwa2r5fkrzmrkar4vfpm0d2lfzmzf514xmnz8k8") (r "1.64.0")))

(define-public crate-b4s-0.3.4 (c (n "b4s") (v "0.3.4") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fst") (r "^0.4.7") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.1") (f (quote ("macros"))) (d #t) (k 2)) (d (n "rstest") (r "^0.18.1") (d #t) (k 2)) (d (n "trie-rs") (r "^0.1.1") (d #t) (k 2)))) (h "04lj4kk7r887qggvfgi0b556aq2yh0rk6i4axmsm126z8xh2l8j0") (r "1.64.0")))

