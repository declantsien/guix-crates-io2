(define-module (crates-io #{3}# b bpf) #:use-module (crates-io))

(define-public crate-bpf-0.1.1 (c (n "bpf") (v "0.1.1") (d (list (d (n "clippy") (r "~0") (o #t) (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "1lj8g451xgfvvviqggjkjzvhwh5ap1qwcrjh2yz3a7i17df27hb2")))

(define-public crate-bpf-0.1.2 (c (n "bpf") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17c74gqxag0xl9wzkm53mf6jhrjkszw8cx493g06ygk2vcdnql8l")))

(define-public crate-bpf-0.1.3 (c (n "bpf") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hnvx3d6ibri75kafdnwvc5jfxigr0imr1rkgd2iszk5y3lkv128")))

