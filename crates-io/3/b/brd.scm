(define-module (crates-io #{3}# b brd) #:use-module (crates-io))

(define-public crate-brd-0.1.0 (c (n "brd") (v "0.1.0") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "console") (r "^0.7.7") (d #t) (k 0)) (d (n "dialoguer") (r "^0.4.0") (d #t) (k 0)) (d (n "git2") (r "^0.7") (d #t) (k 0)))) (h "0syzk1jby77aw2zab2rm9qn8zx2whzdcw43z0c6756h5cwfyjsvx")))

