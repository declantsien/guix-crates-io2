(define-module (crates-io #{3}# b bsc) #:use-module (crates-io))

(define-public crate-bsc-0.1.0 (c (n "bsc") (v "0.1.0") (d (list (d (n "beanstalkc") (r "^0.2.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0aw20ka279nwyldii9flyxp31q5s0xf7ipnqbjycpbwyw3gjlcpq")))

(define-public crate-bsc-0.1.1 (c (n "bsc") (v "0.1.1") (d (list (d (n "beanstalkc") (r "^0.2.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1rkabiaarf9lhfisrv63ih7w87344nwfsw90csh9nqmfcywxpns3")))

(define-public crate-bsc-0.1.2 (c (n "bsc") (v "0.1.2") (d (list (d (n "beanstalkc") (r "^0.2.9") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0175basd54dwbhnaf1hc1rldyxzfrz6q54hzms0h5slxwm30gfi7")))

(define-public crate-bsc-0.1.3 (c (n "bsc") (v "0.1.3") (d (list (d (n "beanstalkc") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "1pfmndaf1s0jn57m8bg9p15ws6pc9wwb7i7ldy4bjskr06bisbsz")))

(define-public crate-bsc-0.1.4 (c (n "bsc") (v "0.1.4") (d (list (d (n "beanstalkc") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)))) (h "0dggqnyck9rj0rk65fbwxdyi60shjnra32zjrfm6bxz9vwzg38b1")))

(define-public crate-bsc-0.2.0 (c (n "bsc") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "0ncdy81pvbbdvclpj1qhxmb7xr4l3yrkjm2wyip7iaw7xj0qgmxs")))

