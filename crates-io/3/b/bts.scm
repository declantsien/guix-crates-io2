(define-module (crates-io #{3}# b bts) #:use-module (crates-io))

(define-public crate-bts-0.1.0 (c (n "bts") (v "0.1.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "1lppafh86k19mqc9v0iwaxlbb8qm8a04pafyfssvjxdjr7ajdj1n")))

(define-public crate-bts-0.1.1 (c (n "bts") (v "0.1.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "1sh2xfkg23j1b9nbq902qcpfgbvk57wqkrw8k2yflj2wk8qcc0gj")))

(define-public crate-bts-0.1.2 (c (n "bts") (v "0.1.2") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)))) (h "1rcqzx4ihh87b8jrs39q2c3fbyhlkqps5kw2ypwd8b6yx6b64wwp")))

(define-public crate-bts-0.2.0 (c (n "bts") (v "0.2.0") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.3") (k 0)) (d (n "test-case") (r "^0.3.3") (d #t) (k 2)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 2)))) (h "17w1qfxvbnapr55mw5h84d30fvwhrzg4ia308gqzqjmbhazkl27a")))

(define-public crate-bts-0.2.1 (c (n "bts") (v "0.2.1") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (k 0)) (d (n "test-case") (r "^0.3.3") (d #t) (k 2)) (d (n "twox-hash") (r "^1.5.0") (d #t) (k 2)))) (h "0wn2dn0dj95zbjiyapj8mz7bfmxsgkhym2n9w1brk0fa3v1c18hw")))

(define-public crate-bts-0.2.2 (c (n "bts") (v "0.2.2") (d (list (d (n "dir-assert") (r "^0.1.0") (d #t) (k 2)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.5") (k 0)) (d (n "test-case") (r "^1.0.0-alpha-1") (d #t) (k 2)))) (h "1pvyma45kyji0ickjvrz22k5cwx0q3h910gz9jhmvgq7s3bmn3s9")))

(define-public crate-bts-0.3.0 (c (n "bts") (v "0.3.0") (d (list (d (n "dir-assert") (r "^0.2.0") (d #t) (k 2)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "1g4yw0aa3a218xsim5dg760i2dhcg2ajrblgl4dkk5rbdi8rq6j9")))

