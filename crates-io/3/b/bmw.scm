(define-module (crates-io #{3}# b bmw) #:use-module (crates-io))

(define-public crate-bmw-0.1.0 (c (n "bmw") (v "0.1.0") (h "19raclvkabbiaas0ppw7prwdv8vg4cz3iqkfds64417lyx091rgx")))

(define-public crate-bmw-0.2.0 (c (n "bmw") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "mio") (r "^0.8.1") (d #t) (k 0)) (d (n "udev") (r "^0.6.3") (f (quote ("mio08"))) (d #t) (k 0)))) (h "0l52nz56wpnb943naf33qraj841wl84m64xpb9zma3rbmvc0bwrj")))

(define-public crate-bmw-0.2.1 (c (n "bmw") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "mio") (r "^0.8.1") (d #t) (k 0)) (d (n "udev") (r "^0.6.3") (f (quote ("mio08"))) (d #t) (k 0)))) (h "0qnr6x2402jnnf69h4wmf779b85rvs7clax9kadps6yxlsw79kn4")))

(define-public crate-bmw-0.3.0 (c (n "bmw") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.56") (d #t) (k 0)) (d (n "bmw-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "mio") (r "^0.8.1") (d #t) (k 0)) (d (n "udev") (r "^0.6.3") (f (quote ("mio08"))) (d #t) (k 0)))) (h "13r8whd01sawbxb4qlyzp7229qg593jny2j2nxfjnr1z4n3xfhyl")))

