(define-module (crates-io #{3}# b bff) #:use-module (crates-io))

(define-public crate-bff-0.1.0 (c (n "bff") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "19w9zsx005phalzrs6w79l0w0wmihk173dj20rrsh0zdf7lyhqkq")))

(define-public crate-bff-0.2.0 (c (n "bff") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "15b9q4lbi757n2qvdb642m4a7xhl6y1yzxybjgm28lj0iwvwk7m9")))

(define-public crate-bff-0.3.0 (c (n "bff") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "colour") (r "^0.6.0") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0rxaxww8rhx02v0nphnxj6x4ab4glqs9ggs44dydfjc7iij383sx")))

(define-public crate-bff-1.0.0 (c (n "bff") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "bstr") (r "^0.2.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^3.0.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colour") (r "^0.6.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)))) (h "0nfpncysqp0bq76p5qliydilzrb7kjnxkalq4sxjs943lw6agnj3")))

