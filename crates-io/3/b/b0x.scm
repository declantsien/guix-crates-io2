(define-module (crates-io #{3}# b b0x) #:use-module (crates-io))

(define-public crate-b0x-0.1.0 (c (n "b0x") (v "0.1.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)))) (h "0x4vi0vz3b142k8qqlgkir2bpav66hczsm73sbhbvgyy5pr2chj1")))

(define-public crate-b0x-0.1.1 (c (n "b0x") (v "0.1.1") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)))) (h "18r1990wgkh6s0hz0sdd1ilbg1zb2332b98w322i3rg0iddmsa6x")))

(define-public crate-b0x-0.2.0 (c (n "b0x") (v "0.2.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)))) (h "086blcdbd6mm2vax6cl862gqr87090j9b4b8y7a4ck22p4apr8k6")))

(define-public crate-b0x-0.2.1 (c (n "b0x") (v "0.2.1") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)))) (h "1pxp4ycgpfnlkwmxhjzb6m5jgb2hyfx46h4xh8dspv3zvzn52z2x")))

(define-public crate-b0x-0.3.0 (c (n "b0x") (v "0.3.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "1223292h0gdnhcdbgyq1v22slxvbkdnl05irk954kg2h4nwy9i1a")))

(define-public crate-b0x-0.3.1 (c (n "b0x") (v "0.3.1") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "0pkfcis9ihx0jfwrgdn3h8ql728fs8lzrvwfarcvba26k8ck1pc7")))

(define-public crate-b0x-0.3.2 (c (n "b0x") (v "0.3.2") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "1bj2fhr8d21s5hg7x5a1a1x94q1sg74jldpr9fh0xabg601plmiw")))

(define-public crate-b0x-0.4.0 (c (n "b0x") (v "0.4.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)) (d (n "primal") (r "^0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "14d3m7ld35dqx1vas0d8v76m832yk93fgb11f6hbqnxssh246xqj")))

(define-public crate-b0x-0.5.0 (c (n "b0x") (v "0.5.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)) (d (n "english-numbers") (r "~0.3.3") (d #t) (k 0)) (d (n "primal") (r "~0.2.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "0mv3gb4cz0pwmriiv6ppjz4m4rcwsxski1bcy0xnlvqnd1b7r0vp")))

(define-public crate-b0x-0.6.0 (c (n "b0x") (v "0.6.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)) (d (n "english-numbers") (r "~0.3.3") (d #t) (k 0)) (d (n "primal") (r "~0.2.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "0wjsvh5i1d467pxy3yspl4pggz3qbshxhhmzwkkkiyq2bmggb99i")))

(define-public crate-b0x-0.7.0 (c (n "b0x") (v "0.7.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)) (d (n "english-numbers") (r "~0.3.3") (d #t) (k 0)) (d (n "primal") (r "~0.2.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "09gr1npf96l5c3f4bxjvyaxihgxpzddybazz9iwl90v15li9hkjv")))

(define-public crate-b0x-0.7.1 (c (n "b0x") (v "0.7.1") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)) (d (n "english-numbers") (r "~0.3.3") (d #t) (k 0)) (d (n "primal") (r "~0.2.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "14y8bcyhpy4z7xprc387gjcgwr9z5n2cslkl73cf9m699qvvx8j1")))

(define-public crate-b0x-0.7.2 (c (n "b0x") (v "0.7.2") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.6.1") (d #t) (k 0)) (d (n "english-numbers") (r "~0.3.3") (d #t) (k 0)) (d (n "primal") (r "~0.2.3") (d #t) (k 0)) (d (n "term_size") (r "~1.0.0-beta1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "012rdzzcbgnffcqx3zi4di94hfgnzfv3ic8agyx3zf0ki5sj4zdx")))

(define-public crate-b0x-0.8.0 (c (n "b0x") (v "0.8.0") (d (list (d (n "clap") (r "~2.32.0") (d #t) (k 0)) (d (n "colored") (r "~1.7.0") (d #t) (k 0)) (d (n "english-numbers") (r "~0.3.3") (d #t) (k 0)) (d (n "primal") (r "~0.2.3") (d #t) (k 0)) (d (n "term_size") (r "~1.0.0-beta1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "1qkkhy855hj492wpgnn1l7sz4kinicchgbni70aj4ms2pfgkanxf")))

(define-public crate-b0x-1.0.0 (c (n "b0x") (v "1.0.0") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "colored") (r "~1.8.0") (d #t) (k 0)) (d (n "english-numbers") (r "~0.3.3") (d #t) (k 0)) (d (n "primal") (r "~0.2.3") (d #t) (k 0)) (d (n "term_size") (r "~1.0.0-beta1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.2.1") (d #t) (k 0)))) (h "1kvxxca2jrf6slbraqfy8fq2aid05mcnhixgkg87cay1ydydqj09")))

(define-public crate-b0x-1.0.1 (c (n "b0x") (v "1.0.1") (d (list (d (n "clap") (r "~2.33.0") (d #t) (k 0)) (d (n "colored") (r "~1.8.0") (d #t) (k 0)) (d (n "english-numbers") (r "~0.3.3") (d #t) (k 0)) (d (n "primal") (r "~0.2.3") (d #t) (k 0)) (d (n "term_size") (r "~1.0.0-beta1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "~1.3.0") (d #t) (k 0)))) (h "0l0k6ivriyvx1vy8av3i0yfd7b56d7mqx6c136frl00ivi4gv0jg")))

