(define-module (crates-io #{3}# b ber) #:use-module (crates-io))

(define-public crate-ber-0.0.1 (c (n "ber") (v "0.0.1") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "1x02dbd2cl2js040z8f68pz6zagz94312za35azrhc54b3lpivk3")))

(define-public crate-ber-0.0.2 (c (n "ber") (v "0.0.2") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0fvj7rfhgzmvcsrq9pgip0w8iaq6jgm9c0ardg92cqwdlm3z7r7p")))

(define-public crate-ber-0.0.3 (c (n "ber") (v "0.0.3") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "1cpcvs2w6cxa08v3g0jss6vsz2l2bldj2h724xfl3h091xrlpgcy")))

(define-public crate-ber-0.0.4 (c (n "ber") (v "0.0.4") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0chwpgh596ll855kjkx83yng4amlj72sg69gf77kpb9ylgk1hy23")))

(define-public crate-ber-0.0.5 (c (n "ber") (v "0.0.5") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "1mhll27cplhwywbsgxz51lk9b2a8a82lz8h76kh4sczx62mlbw98")))

(define-public crate-ber-0.0.6 (c (n "ber") (v "0.0.6") (d (list (d (n "byteorder") (r "*") (d #t) (k 0)))) (h "0b58lgma1r771sm94p3676gf3c0m7lj60qg0iy6bwdsis1g50pf5")))

