(define-module (crates-io #{3}# b byt) #:use-module (crates-io))

(define-public crate-byt-0.0.2 (c (n "byt") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "055zy21c17frygywmy1hnbc0j4nn8h0s119iafgb7xlj4jwbzwf3")))

