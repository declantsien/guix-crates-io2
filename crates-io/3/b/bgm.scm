(define-module (crates-io #{3}# b bgm) #:use-module (crates-io))

(define-public crate-bgm-0.1.0 (c (n "bgm") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "sqlx") (r "^0.2") (f (quote ("runtime-tokio" "macros" "postgres"))) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0p552vc1hasy1255w2nnr1pqq9wvjvr0x62dphizqii86n5a71rv")))

