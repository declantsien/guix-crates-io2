(define-module (crates-io #{3}# b bam) #:use-module (crates-io))

(define-public crate-bam-0.0.1 (c (n "bam") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^1.8.1") (o #t) (d #t) (k 0)) (d (n "libflate") (r "^0.1.27") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "12vk8qp4nfhyg6ky14dw35kml1gzyrcb94ahb4jn2dd553ifdgdg") (f (quote (("default") ("check_crc" "crc"))))))

(define-public crate-bam-0.0.2 (c (n "bam") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1ksljs4kz3nxilp2dkknz0f08ir6d33n1jq77h9j9l2j60damd1j") (f (quote (("default") ("check_crc" "crc"))))))

(define-public crate-bam-0.0.3 (c (n "bam") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1kasrq6q2hxjcmskm2c72zgszgsra1fypc30n6bnw0a76f1y4h9v") (f (quote (("default") ("check_crc" "crc"))))))

(define-public crate-bam-0.0.4 (c (n "bam") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1lj8x648x8irz7d1dnh6jlivvspd8x0k4k1bm9n8f11vh2lb46rm") (f (quote (("default") ("check_crc" "crc"))))))

(define-public crate-bam-0.0.5 (c (n "bam") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "inflate") (r "^0.4") (d #t) (k 0)) (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0dx1h5vv5wyyiw4vrjfqznmkjnxjxwqa7p0nlz5lp2jnqb2ggh2f") (f (quote (("default") ("check_crc" "crc"))))))

(define-public crate-bam-0.0.6 (c (n "bam") (v "0.0.6") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "lru-cache") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0cifj1yycxh14flpz0xj4q7wak6zk1kinr45qzcfm81b4dhpkcsr")))

(define-public crate-bam-0.0.7 (c (n "bam") (v "0.0.7") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0dfi2g1p99zhcr9m018cdspf4ms75d9h3z1kixi64js33zpjlp0m")))

(define-public crate-bam-0.0.8 (c (n "bam") (v "0.0.8") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0a436wvrfnmpsrqnj6r276lfmm807mfklhdlkc4s5z7kn0v3lgji")))

(define-public crate-bam-0.0.9 (c (n "bam") (v "0.0.9") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1w52jmh7z8x267vpxgngj68an9h80jp6p6wrqnkzg7sxkvqlm39z")))

(define-public crate-bam-0.0.10 (c (n "bam") (v "0.0.10") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0ihswpz0anxj05bwv728ar7jksj6i01vr4dnzs85kad6s273m1v5")))

(define-public crate-bam-0.0.11 (c (n "bam") (v "0.0.11") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "100ljcasq244z2sbqv0i7m18m83hlvcp446xdjmpz2qnf1yvsims")))

(define-public crate-bam-0.0.12 (c (n "bam") (v "0.0.12") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1825drny4rn6vf8nxq38krfp1aivmsk5j8wmzl26jmzkg1rx825n")))

(define-public crate-bam-0.0.13 (c (n "bam") (v "0.0.13") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1d31rzdk9kavx2k5xjnar6a155i8h6xx3kgscx6ypqlp60yxibv7")))

(define-public crate-bam-0.0.14 (c (n "bam") (v "0.0.14") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0l19jyksg7jcvw5vsiix818qb9m1scq5qb20wirfkh0vppzhglr3")))

(define-public crate-bam-0.0.15 (c (n "bam") (v "0.0.15") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0va04ph0v6blhzpn660yfb32hwmgdvk5x244479jw9rshzbvnm2r")))

(define-public crate-bam-0.1.0 (c (n "bam") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "02w8iwb1mc0wnp14s2wmgfgrlj3ipk4cwsyjnd7kxr54gpsmdz73")))

(define-public crate-bam-0.1.1 (c (n "bam") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1nj841nl5474d6rdkfl48bmq9nmx3cw1lj6afxrsrv2d95m6w097")))

(define-public crate-bam-0.1.2 (c (n "bam") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "16rj93z1s7vqfj2lnzj1ijnixx1w62xysaqghidlsizxss3gv1m6")))

(define-public crate-bam-0.1.3 (c (n "bam") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "00zfmkdx6x8qpx1mr3fkdrdw86b46yznifcg4pq7vf1ja6zvsvsz")))

(define-public crate-bam-0.1.4 (c (n "bam") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crc32fast") (r "^1.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (f (quote ("rust_backend"))) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "03ffmzf4dpi4q3qbln85ll7kgdmzm2qdisjggbkd5h9g2jx1i68h")))

