(define-module (crates-io #{3}# b bae) #:use-module (crates-io))

(define-public crate-bae-0.0.1 (c (n "bae") (v "0.0.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "11ik3if3a70j4k0m4gigm3iayi87ysm5cq0hlsb34q2hswx8all2")))

(define-public crate-bae-0.1.0 (c (n "bae") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "1ldmykx9k3x0zcsiv1skzdf1b3hlmp0f0p4yi4scqnf0i83bnh44")))

(define-public crate-bae-0.1.1 (c (n "bae") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "0snv91hdl90dfsv5zx3s985y01a0srvhniw4cs4qfz2r3wl4g3xg")))

(define-public crate-bae-0.1.2 (c (n "bae") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.3.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.7") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.17") (d #t) (k 2)))) (h "0lfagi7bsssd9a3wgwa33j2avz060yw13rarlim1vjxjs26z9r75")))

(define-public crate-bae-0.1.3 (c (n "bae") (v "0.1.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0pz0y4z2bmrbhby4ir407nc8s7f7g47is4vrpvnpjj9bfspla82c")))

(define-public crate-bae-0.1.4 (c (n "bae") (v "0.1.4") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "07mrr8kh4wma5zlh5p5n3kx9bqagrg6y38w0sc5fqbwcl4nmjzb3")))

(define-public crate-bae-0.1.5 (c (n "bae") (v "0.1.5") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1szwhckr3har10hmfmc3iw15574zsrqq6ii6vy0pdiz9rbyk1s2a")))

(define-public crate-bae-0.1.6 (c (n "bae") (v "0.1.6") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "1kq0smgqv254yrxyfn9r8vpmd5bnmfniglbdbvjain733r1py47c")))

(define-public crate-bae-0.1.7 (c (n "bae") (v "0.1.7") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "trybuild") (r "^1") (d #t) (k 2)))) (h "0yirh3s6fxjbji6f65m5bgw1bjzg0i45iqpcxq3ja4s1rikxxf1k")))

