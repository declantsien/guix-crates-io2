(define-module (crates-io #{3}# b bge) #:use-module (crates-io))

(define-public crate-bge-0.1.0 (c (n "bge") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ort") (r "^2.0.0-rc.0") (f (quote ("ndarray"))) (k 0)) (d (n "ort") (r "^2.0.0-rc.0") (f (quote ("download-binaries"))) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokenizers") (r "^0.15.2") (f (quote ("http"))) (d #t) (k 0)))) (h "06xa00z3p0hy21n6cgmf3ps9923g4v5mddfwpjdhg6r5mg660kyv")))

(define-public crate-bge-0.2.0 (c (n "bge") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "ort") (r "^2.0.0-rc.1") (f (quote ("ndarray"))) (k 0)) (d (n "ort") (r "^2.0.0-rc.0") (f (quote ("download-binaries"))) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokenizers") (r "^0.15.2") (f (quote ("http"))) (d #t) (k 0)))) (h "0706cvmiyfmjg5r3dnhdmd7n86h3r921mh09mq6mg1wabgycvggf")))

