(define-module (crates-io #{3}# b bet) #:use-module (crates-io))

(define-public crate-bet-0.1.0 (c (n "bet") (v "0.1.0") (h "1lmw65i4n1zcmqac7jcbnij4ippd6w21bmhdpgdhp2cqx8wdnjjw")))

(define-public crate-bet-0.2.0 (c (n "bet") (v "0.2.0") (h "0ia2v7qg136ispkmw7xf6y11z2s8h33dghgq2sf4bipgiic7y1an")))

(define-public crate-bet-0.3.0 (c (n "bet") (v "0.3.0") (h "1kklfvnj163p9qw2q928dsipc0vg86ylx15pvwg60kcr05d2l4x2")))

(define-public crate-bet-0.3.1 (c (n "bet") (v "0.3.1") (h "1nbywhyj53mzdsx216sszbdiaj3i4flqwgci3mcwgzfkigmjrpkw")))

(define-public crate-bet-0.3.2 (c (n "bet") (v "0.3.2") (h "0qjh9nsd4j46c0yql5b9zs9s6lp48qr6j855bcdaj4qv0hwaals9")))

(define-public crate-bet-0.3.3 (c (n "bet") (v "0.3.3") (h "0cbd4xf69nwxwwp92qyhx0z1ap1rzgm3zv2pvairxsh10a5qkkyq")))

(define-public crate-bet-0.3.4 (c (n "bet") (v "0.3.4") (h "1d7yliqm53qyqv6ma4fqy5i7dfg627z7ymqkk0vvj6w03y43wyki")))

(define-public crate-bet-0.4.0 (c (n "bet") (v "0.4.0") (h "0bmgd14qqnd4nnxh2i40phnr905fr7i5wi9s4sr0sam9c6v01pay")))

(define-public crate-bet-1.0.0 (c (n "bet") (v "1.0.0") (h "0w46dpfdhwrr90qvqi3nli46gxfalhic49vxbrchpysqfrc8w89y")))

(define-public crate-bet-1.0.1 (c (n "bet") (v "1.0.1") (h "0phjf75zdwf8n0bdi139c7yp2n0p823kw35bmswjnnjn8x07n605")))

(define-public crate-bet-1.0.2 (c (n "bet") (v "1.0.2") (h "165c4fb9czp7jvfii7143a6l4z8m2h3p6f3ss7ivbm68v4xd2wqn")))

(define-public crate-bet-1.0.3 (c (n "bet") (v "1.0.3") (h "0f6cjygvdhlqdfy6cl2ki0hm71yciff3y3srm09q3phvk96f6v1l")))

