(define-module (crates-io #{3}# b btf) #:use-module (crates-io))

(define-public crate-btf-0.1.0 (c (n "btf") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "13ppd4wq11s9d47pzjpw7f092ywsf14k5zicxfym6gd8nhgicq09")))

(define-public crate-btf-0.1.1 (c (n "btf") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0rijj49gyi7nfaim8af5qdjp51x6b58wgki5843dips2i161bia6")))

(define-public crate-btf-0.2.0 (c (n "btf") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0p2w6r21kl739bh55da7igg8n88hc0186y0cx8pwdqbgmx7gdgb8")))

(define-public crate-btf-0.3.0 (c (n "btf") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "054as6l0yfcf3b4zbcpp3rl0dqf9ksqrjayn4khcsr1c5lbv1830")))

(define-public crate-btf-0.3.1 (c (n "btf") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0300px4rfnqfrcnq679npcgwg5ypmaaqgxvar2k01pq3x1r63q4v")))

(define-public crate-btf-0.3.2 (c (n "btf") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0pn5wf4sikkk2zilfa9v4f6aicbsyxag58m3bf0vx4w3flhgqc92")))

(define-public crate-btf-0.4.0 (c (n "btf") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0ik0ciy85r40lqiq5j488wx7z17gkbhdp1f9kfjdhl70ks9blgjs")))

(define-public crate-btf-0.5.0 (c (n "btf") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0b98fcylc5l5slz1lcy4wjidjvx4gj35ydayz6vhadn450x9mmcf")))

(define-public crate-btf-0.5.1 (c (n "btf") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0a91sgrfh0iallm2vm47j7nsd81xc9rmhligvi95s0vi9nagksz9")))

