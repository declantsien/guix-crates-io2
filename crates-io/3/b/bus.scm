(define-module (crates-io #{3}# b bus) #:use-module (crates-io))

(define-public crate-bus-0.2.0 (c (n "bus") (v "0.2.0") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "08bnlsyjr98s1rrxp5sdd9qy16zsakhmvn7zhzxf9vbfamzdwhyc")))

(define-public crate-bus-0.2.1 (c (n "bus") (v "0.2.1") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0mafwddnzj9d28lvlpp111fcw9zqc0mpm4n73rhfyh5irp6cnaz1") (f (quote (("bench"))))))

(define-public crate-bus-1.0.0 (c (n "bus") (v "1.0.0") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "10fw70ijj8rnz7pzdwi9d2vmsyh9g5vi42727djmjzhbm83jfmcd") (f (quote (("bench"))))))

(define-public crate-bus-1.0.1 (c (n "bus") (v "1.0.1") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)))) (h "1793a7grka6y1s0icw82gzchh512gk2fsijv683xyssavclqqnll") (f (quote (("bench"))))))

(define-public crate-bus-1.1.0 (c (n "bus") (v "1.1.0") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.1") (d #t) (k 0)))) (h "1l0y54naak1wpxnbgh8n6wwvac8j92snfgbz4y9783np8rkz115w") (f (quote (("bench"))))))

(define-public crate-bus-1.1.1 (c (n "bus") (v "1.1.1") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.1") (d #t) (k 0)))) (h "065vrmyl2n7janyim3zw50168rkg41d3lcra7jzxhfqr91fz7wpk") (f (quote (("bench"))))))

(define-public crate-bus-1.1.2 (c (n "bus") (v "1.1.2") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.1") (d #t) (k 0)))) (h "1gq5fpia30hmqp4djmx4psca9ll6ng84yjcrfvja5jrvs37634pm") (f (quote (("bench"))))))

(define-public crate-bus-1.1.3 (c (n "bus") (v "1.1.3") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.1") (d #t) (k 0)))) (h "0i8x4lc8n6q11nii9n6jar3ns76pjz20d0788j649vs6niy7n0cy") (f (quote (("bench"))))))

(define-public crate-bus-1.2.0 (c (n "bus") (v "1.2.0") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.1") (d #t) (k 0)))) (h "0wmdqc32m46as364gb757xmipcnv8vn26bbn9i8n3x03lkw0vzl1") (f (quote (("bench"))))))

(define-public crate-bus-1.3.0 (c (n "bus") (v "1.3.0") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.1") (d #t) (k 0)) (d (n "void") (r "^1.0") (o #t) (d #t) (k 0)))) (h "18bx1hx6snp4hhfqn2jf3wd3nb1g2gkjjj6qm79xqxj56h55kr1r") (f (quote (("bench") ("async" "futures" "void"))))))

(define-public crate-bus-1.3.1 (c (n "bus") (v "1.3.1") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.2.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "1gwg9m7sp9kxhqrpb0nqdhrw05fxyagg6c8g8wps43wdlgl6jppf") (f (quote (("bench") ("async" "futures" "void"))))))

(define-public crate-bus-1.3.2 (c (n "bus") (v "1.3.2") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.2.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "1pxc7l617q0q6v0zafjkd195qamb9vl4s8w74fb20r0p7icr8z7s") (f (quote (("bench") ("async" "futures" "void"))))))

(define-public crate-bus-1.4.0 (c (n "bus") (v "1.4.0") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.2.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "0jivr9cykjmfd1ffrcny0hcbhnm3mlk3vd1a10zdlnlp45n6hri5") (f (quote (("bench") ("async" "futures" "void"))))))

(define-public crate-bus-1.4.1 (c (n "bus") (v "1.4.1") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1.14") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.2.2") (d #t) (k 0)) (d (n "void") (r "^1.0.2") (o #t) (d #t) (k 0)))) (h "1jj480j62hxsldqd4mq8d61bh3bbnbr01y2am2s15mwl7976sbsh") (f (quote (("bench") ("async" "futures" "void"))))))

(define-public crate-bus-2.0.0 (c (n "bus") (v "2.0.0") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.2.2") (d #t) (k 0)))) (h "03qfll7r9iqg0493gyvw7mz11w12h2q93mg46zrkk0spdqxy9fn9") (f (quote (("bench"))))))

(define-public crate-bus-2.0.1 (c (n "bus") (v "2.0.1") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.4") (d #t) (k 0)))) (h "1srq6bs16vs5zpa98862f9lgar6fwvlj01y0jqsdd01p08dawxcw") (f (quote (("bench"))))))

(define-public crate-bus-2.1.0 (c (n "bus") (v "2.1.0") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.4") (d #t) (k 0)))) (h "1j9v5jjlbc9q3vyqmdrhab0fm66akqim7kmp94xxnl84qjgpqihq") (f (quote (("bench"))))))

(define-public crate-bus-2.1.1 (c (n "bus") (v "2.1.1") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.4") (d #t) (k 0)))) (h "111nhp1qp48mpz8z84mbijnfc4g1p2b75r8i4pq9yj3ralqq7gr1") (f (quote (("bench"))))))

(define-public crate-bus-2.2.1 (c (n "bus") (v "2.2.1") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.4") (d #t) (k 0)))) (h "17jx8ww78mbcxbmdl1c3v7v52p1rzn8q4qvy22z54p03b1566ksh") (f (quote (("bench"))))))

(define-public crate-bus-2.2.2 (c (n "bus") (v "2.2.2") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.5") (d #t) (k 0)))) (h "0qffgacvr7kxz2ix9d548a4jjkjlilg0ar794bks61g0czcz22yr") (f (quote (("bench"))))))

(define-public crate-bus-2.2.3 (c (n "bus") (v "2.2.3") (d (list (d (n "atomic-option") (r "^0.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.7") (d #t) (k 0)))) (h "0awg9s5iylk6f26zpaghfmql5m6ynfihn8jq387l9cgmg4bnxrpi")))

(define-public crate-bus-2.2.4 (c (n "bus") (v "2.2.4") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.9") (d #t) (k 0)))) (h "0g3xss3rhkj8q1wpwc49gb58il5ydj709568i38qmmvrrbwpgz82")))

(define-public crate-bus-2.3.0 (c (n "bus") (v "2.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.9") (d #t) (k 0)))) (h "1qjinyk49fv8zawzl3bqwpimmgrq5v7d9j8q23zma0dnyljldjw0")))

(define-public crate-bus-2.4.0 (c (n "bus") (v "2.4.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.9") (d #t) (k 0)))) (h "04f8lq2wgvbcf583kgbgn0qv8y1s2nv8wsviphbn6wqr3j11sw1l")))

(define-public crate-bus-2.4.1 (c (n "bus") (v "2.4.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6.2") (d #t) (k 0)) (d (n "parking_lot_core") (r "^0.9") (d #t) (k 0)))) (h "10xpb71c04p2kr67dj3rvc453mbwvcnpqr8vi3dgm10x4b81hwab")))

