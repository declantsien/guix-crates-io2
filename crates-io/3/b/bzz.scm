(define-module (crates-io #{3}# b bzz) #:use-module (crates-io))

(define-public crate-bzz-0.0.1 (c (n "bzz") (v "0.0.1") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "1rrz45rl2s0psms1rwnfyz1plr6682j9j122hj3ancal384z4pjm")))

(define-public crate-bzz-0.0.2 (c (n "bzz") (v "0.0.2") (d (list (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "termion") (r "^1") (d #t) (k 0)))) (h "1199hqj4nn294fcksmkr07y89ifp00s2a35nxpychmmh87kjl3i5")))

