(define-module (crates-io #{3}# b ble) #:use-module (crates-io))

(define-public crate-ble-0.1.0 (c (n "ble") (v "0.1.0") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "040s2qxdallxhh3qq21p8qr4f27q0ik8n2kxw0whb60c6z09bi2y")))

(define-public crate-ble-0.1.1 (c (n "ble") (v "0.1.1") (d (list (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "1ncr941x04d5kpkbqb644kmpqryqs7pyl2rviyj0hsg1qa9k30hn")))

