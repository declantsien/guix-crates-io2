(define-module (crates-io #{3}# b bmv) #:use-module (crates-io))

(define-public crate-bmv-0.1.0 (c (n "bmv") (v "0.1.0") (d (list (d (n "clap") (r "~3.1.6") (f (quote ("cargo" "env"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "0rk8qp81gyahh8m8dnzcip5q1iw2ihkinncj3dprjk5wghlsfa9r")))

(define-public crate-bmv-0.1.1 (c (n "bmv") (v "0.1.1") (d (list (d (n "clap") (r "~3.1.6") (f (quote ("cargo" "env"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)))) (h "1msngk74wlq37q7i5pqify3dnhlpd0sss5g909yiv4c443ima1fk")))

