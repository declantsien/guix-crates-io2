(define-module (crates-io #{3}# b btw) #:use-module (crates-io))

(define-public crate-btw-0.1.0 (c (n "btw") (v "0.1.0") (h "0j3ribb0dics3l0r6kik0s7707wy36h8iii1dxz1wm3c0kr0nwp0")))

(define-public crate-btw-0.1.1 (c (n "btw") (v "0.1.1") (h "1jqn8p6ch4i6fkpmjkgbr31m2m77r85ckpxj908wxcsi6iz9p7iz")))

