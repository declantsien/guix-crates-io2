(define-module (crates-io #{3}# b brs) #:use-module (crates-io))

(define-public crate-brs-0.0.0 (c (n "brs") (v "0.0.0") (h "0cdpd6h7cfp3dbw0sjqb7c6450spywm068jrp6425s577gpih9d2") (y #t)))

(define-public crate-brs-0.1.0 (c (n "brs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.2.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (d #t) (k 0)))) (h "05wy2a7fr7ycyh0qyf3whljbcy0f5w7ffjj6hzn34w422r08g898")))

(define-public crate-brs-0.2.0 (c (n "brs") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libflate") (r "^1.0.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.3") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "0mamxvmnmk5j2sq2fck0ngf3m2x9ak0bwsap3nvnizhcvn6ybrgi")))

