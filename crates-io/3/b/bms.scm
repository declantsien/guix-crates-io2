(define-module (crates-io #{3}# b bms) #:use-module (crates-io))

(define-public crate-bms-0.1.0 (c (n "bms") (v "0.1.0") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)) (d (n "read_input") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)))) (h "1z543nf6ghnnhr8q4cwd9wawa74kv49yp0narm1j7q8wsb558r4c") (y #t)))

(define-public crate-bms-0.1.1 (c (n "bms") (v "0.1.1") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)) (d (n "read_input") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)))) (h "0v7rnm361cln59izv67sva32zzcb9mbdb18z9nyqmf6i1wj1mgx6") (y #t)))

(define-public crate-bms-0.1.2 (c (n "bms") (v "0.1.2") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1") (d #t) (k 0)) (d (n "read_input") (r "^0.8.4") (d #t) (k 0)) (d (n "regex") (r "^1.4.6") (d #t) (k 0)))) (h "12yy0dkamb9wdsfdd45pkrdfnin14r945g94wzkylj4s2zad28zw")))

