(define-module (crates-io #{3}# b bdt) #:use-module (crates-io))

(define-public crate-bdt-0.1.0 (c (n "bdt") (v "0.1.0") (h "0svilmw49zwi7iwzn1cbsqpqkla7qi16zgv6z2s7j8nh46nfrwa0")))

(define-public crate-bdt-0.1.1 (c (n "bdt") (v "0.1.1") (d (list (d (n "datafusion") (r "^8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "0k5rs016xlmgjalkb2m5py56mzqjvxdcxnspv6g1jb8427kfkk87")))

(define-public crate-bdt-0.1.2 (c (n "bdt") (v "0.1.2") (d (list (d (n "datafusion") (r "^8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "0r53vfrv5bw9rbkk6p0ph5hfcf48sjc0p1fgd5cp3kd29hjzqy0a")))

(define-public crate-bdt-0.1.3 (c (n "bdt") (v "0.1.3") (d (list (d (n "datafusion") (r "^8.0") (f (quote ("avro-rs"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "15m4djl0sr7ph16rb0mwaj9clxxfn9lkv0a2l0n01y3qnlb296dq")))

(define-public crate-bdt-0.2.0 (c (n "bdt") (v "0.2.0") (d (list (d (n "datafusion") (r "^8.0") (f (quote ("avro-rs"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "1nwd4sh4cncp4i8m4pxfa0q4x7abqg4akvpfv61jmw4q682r281w")))

(define-public crate-bdt-0.2.1 (c (n "bdt") (v "0.2.1") (d (list (d (n "datafusion") (r "^8.0") (f (quote ("avro-rs"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "041frxkmgpi0l0kxfx4vp4w07z8fmw9dvq37v0sv824za7v06k0b")))

(define-public crate-bdt-0.3.0 (c (n "bdt") (v "0.3.0") (d (list (d (n "datafusion") (r "^9.0") (f (quote ("avro-rs"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "1wvc9mm8h5bwl4hxcck6c12jn8l84ixv9z7ypcl3ca8m977g1ni9")))

(define-public crate-bdt-0.4.0 (c (n "bdt") (v "0.4.0") (d (list (d (n "datafusion") (r "^13.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "1gpaq6ff51h8jpp94aa5cdfz261i3ar0spa998agzls5v8kqz4yh")))

(define-public crate-bdt-0.4.1 (c (n "bdt") (v "0.4.1") (d (list (d (n "datafusion") (r "^13.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "0hzd5l6r6jz2d7ljlw80ysvx4p9h1vb6islw71bxxy8539v3rzk7")))

(define-public crate-bdt-0.5.0 (c (n "bdt") (v "0.5.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^13.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "11g5napkhdkh4s728ll0grg2dm2a4mgxz43n6ynyqzpfw6g69mc6")))

(define-public crate-bdt-0.6.0 (c (n "bdt") (v "0.6.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^14.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "191mjxv9nvkna3dlz7xaxkjn00ijmnchq169kcd5n8plcl5m783m")))

(define-public crate-bdt-0.7.0 (c (n "bdt") (v "0.7.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^14.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "05mppl1mzkj2ln2932ab943sxhr9lda666ladpadmxxj1zgwm1ib")))

(define-public crate-bdt-0.8.0 (c (n "bdt") (v "0.8.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^15.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "1p7qbacrc7fj070rdcnws7d7k5wsl39c0dww84zv7sv26wslixnq")))

(define-public crate-bdt-0.9.0 (c (n "bdt") (v "0.9.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^15.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "0jjj6avz6ycxcy00akx8q9ypnx6ym4v1fhksyg4j2r1b6684lhgw")))

(define-public crate-bdt-0.10.0 (c (n "bdt") (v "0.10.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^15.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "0sphd0d0mkfbbnv0i07kx00pnpk5kgg2wgnxihyjlk9qvjy5plr3")))

(define-public crate-bdt-0.11.0 (c (n "bdt") (v "0.11.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^16.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "0r904zs5vd1s7ylj7n3f8y6z5s97gbgr3rsd6c5zy38hsayrgyah")))

(define-public crate-bdt-0.13.0 (c (n "bdt") (v "0.13.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^16.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "0vcljzvw54n942wcb9rlb3bqfq8xddxqvr1ybk3mv6ddpr3b6si1")))

(define-public crate-bdt-0.14.0 (c (n "bdt") (v "0.14.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^28.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "1jv0h671wpb5zih3whm0jx0ivqkh79db8h4sf61r3zqhiq807qy6")))

(define-public crate-bdt-0.15.0 (c (n "bdt") (v "0.15.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^32.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "1f68n9gci8wx4jav794z33hr7z86c7dgl1f32k6qkbw75ky1hy12")))

(define-public crate-bdt-0.16.0 (c (n "bdt") (v "0.16.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^33.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "1x700phd6zaj97wac8lm4mns19sicv71r79bdjcm59613zn9yh3j")))

(define-public crate-bdt-0.17.0 (c (n "bdt") (v "0.17.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^33.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.19") (d #t) (k 0)))) (h "1z51b18ldx19yfkgjbgbkjg0zimy2khliwq9c6f1x7aay3hz006y")))

(define-public crate-bdt-0.18.0 (c (n "bdt") (v "0.18.0") (d (list (d (n "comfy-table") (r "^6.1.2") (d #t) (k 0)) (d (n "datafusion") (r "^35.0") (f (quote ("avro"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("rt-multi-thread"))) (d #t) (k 0)))) (h "0gk37a5r49yf6cl19nhrckk622v34jjb0j48h7a8y8pm9b04bzwk")))

