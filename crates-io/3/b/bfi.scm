(define-module (crates-io #{3}# b bfi) #:use-module (crates-io))

(define-public crate-bfi-0.1.0 (c (n "bfi") (v "0.1.0") (h "1rjmn2vnlv7vasiiwd70h4rskln37n82xx4knixd04h0wjj3sbbi")))

(define-public crate-bfi-0.1.1 (c (n "bfi") (v "0.1.1") (h "0vqzxsg5f58fxcgyggswb40hdw435vf150a2d3y6k42bpa7imm16")))

(define-public crate-bfi-0.2.0 (c (n "bfi") (v "0.2.0") (h "0srldzygg7i4832mq2g5p1w03078fj7rp3z9jy7704727ygjh3y9")))

(define-public crate-bfi-0.2.1 (c (n "bfi") (v "0.2.1") (h "0wr785f6vsp6bgqhyi3mag6ndnjg2a7b3zr158nyy8w4q2ib0f4z")))

(define-public crate-bfi-0.2.2 (c (n "bfi") (v "0.2.2") (h "17w6mbhk2m23s0i5s6yy1gmrbw8spywnylzpq3n09b1kjz5qijbd")))

