(define-module (crates-io #{3}# b bug) #:use-module (crates-io))

(define-public crate-bug-0.2.0 (c (n "bug") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1n1fii4nrmg2ygy0jwa90v4mgc8n64bijlnr236yashyjrggj6gn") (f (quote (("schedule") ("default"))))))

