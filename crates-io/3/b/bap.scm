(define-module (crates-io #{3}# b bap) #:use-module (crates-io))

(define-public crate-bap-0.1.0 (c (n "bap") (v "0.1.0") (d (list (d (n "bap-sys") (r "^0.1") (d #t) (k 0)) (d (n "bit-vec") (r "^0.4") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "mktemp") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "01l4p4kisn8i03kb65999l07qw4mrddy7fnhby2vc2gqmzc34yps") (f (quote (("json" "rustc-serialize") ("default"))))))

