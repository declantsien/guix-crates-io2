(define-module (crates-io #{3}# b b2m) #:use-module (crates-io))

(define-public crate-b2m-0.18.2 (c (n "b2m") (v "0.18.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0avwlzzm7gqzpcsp0xg4z0v92k4ymn5m1k2mmpcnvggn9pxdy6d5") (f (quote (("youget") ("default" "annie" "youget") ("annie"))))))

(define-public crate-b2m-0.19.0 (c (n "b2m") (v "0.19.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jxi32pmc6j9y8rdjq9fid61sb1mgq0236wcq4g5crfargwqpc4p") (f (quote (("youget") ("default" "annie" "youget") ("annie"))))))

(define-public crate-b2m-0.20.0 (c (n "b2m") (v "0.20.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0719hz2smja26d5p0954c6y1hcv6smzcx2w3g2fsvmpqhnf9avqd") (f (quote (("youget") ("default" "annie" "youget") ("annie"))))))

(define-public crate-b2m-0.22.0 (c (n "b2m") (v "0.22.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "finata") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1q91r4fxlm0sdhv88n4v9wiidv3ql3rd3lijb399d64nfqnxizg1") (f (quote (("youget") ("nfinata" "finata" "tokio") ("default" "annie" "youget") ("annie"))))))

(define-public crate-b2m-0.22.1 (c (n "b2m") (v "0.22.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "finata") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0p6kvp0xs48q0dgmnhy0vlgh5j1jikqswrqhq4xszvx0l90jkjb6") (f (quote (("youget") ("nfinata" "finata" "tokio") ("default" "annie" "youget") ("annie"))))))

(define-public crate-b2m-0.23.0 (c (n "b2m") (v "0.23.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "finata") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0z29sjv89l0r34sclafghfqk9jd10x5av3a7i19zz7wzgv0rlwg6") (f (quote (("youget") ("nfinata" "finata" "tokio") ("default" "annie" "youget") ("annie"))))))

(define-public crate-b2m-0.24.0 (c (n "b2m") (v "0.24.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "finata") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "netscape-cookie") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f7q0z8dvw88b4z4c2rm6k7xpsz2vhjslmh7n71qlpdrdz09zhx1") (f (quote (("youget") ("nfinata" "finata" "netscape-cookie") ("default" "annie" "youget") ("annie"))))))

(define-public crate-b2m-0.25.0 (c (n "b2m") (v "0.25.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "finata") (r "^0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0la13429qggnqr4vp2jlcm7i6i2bnpr6agcccq14kgbgqwjglvg0") (f (quote (("fina"))))))

(define-public crate-b2m-0.25.1 (c (n "b2m") (v "0.25.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "finata") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1l6nw42w8pgkbf1xr7zda792xz15p4jwlf156bi2g691sblc16w6") (f (quote (("fina"))))))

