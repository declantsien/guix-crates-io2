(define-module (crates-io #{3}# b bhv) #:use-module (crates-io))

(define-public crate-bhv-0.1.2 (c (n "bhv") (v "0.1.2") (h "0pfs3704fp9sxlg7nv1mp29b8klxy94b7imcxcizdb036xjy4fvq")))

(define-public crate-bhv-0.2.0 (c (n "bhv") (v "0.2.0") (h "1ryj30szyaapib85kqg6dd00b9djzx3flwq3jrjn1wzhz89z7hkw")))

(define-public crate-bhv-0.2.1 (c (n "bhv") (v "0.2.1") (h "04p0p68vxl6y97m4cdbrnb3ll78911al674cpv7k0p523r81wc57")))

(define-public crate-bhv-0.2.2 (c (n "bhv") (v "0.2.2") (h "0xg3d71z1jg8143pfksrjy23z705vjs7f0fn5dmldki1yvaxx21p")))

(define-public crate-bhv-0.2.3 (c (n "bhv") (v "0.2.3") (h "0swyif9jqbpy9bhwmbipb8a6456qpgxxnrqb9sb6a08q729x9g38")))

(define-public crate-bhv-0.3.0 (c (n "bhv") (v "0.3.0") (h "17qka43jgd1ghaap848d6pfab6s0r2kj1qp68cqnx2n06kq8gfgd") (f (quote (("events"))))))

(define-public crate-bhv-0.4.0 (c (n "bhv") (v "0.4.0") (h "0lw9ims717j38z9f53c2l3phadnwrw5b0n3fimz09pzyd0mq7avl") (f (quote (("events"))))))

