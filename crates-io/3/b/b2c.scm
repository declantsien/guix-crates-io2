(define-module (crates-io #{3}# b b2c) #:use-module (crates-io))

(define-public crate-b2c-0.1.0 (c (n "b2c") (v "0.1.0") (h "14mw8pzagiqwnpa48i7x3d4v8whxa89gcssgycjf9i5dni7q73vv") (y #t)))

(define-public crate-b2c-0.1.1 (c (n "b2c") (v "0.1.1") (h "0gyp06vffgajd7g1hj01d3xbnhfxazdsc93dy1l7qs67mc4d8mpl") (y #t)))

