(define-module (crates-io #{3}# b bip) #:use-module (crates-io))

(define-public crate-bip-0.0.1 (c (n "bip") (v "0.0.1") (h "1sd5j2xkv3qvhbgc710blhqijd3d69aw5c3z847jqylf0vrlk8hx")))

(define-public crate-bip-0.1.0 (c (n "bip") (v "0.1.0") (h "0wqbakh2xhdqwdfabzvcdbb07f2c2c02fi2nr3snfrpv2i5z909c")))

