(define-module (crates-io #{3}# b bps) #:use-module (crates-io))

(define-public crate-bps-0.1.0 (c (n "bps") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "teloxide") (r "^0.9") (f (quote ("macros" "auto-send"))) (d #t) (k 0)) (d (n "tokio") (r "^1.8") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "0jk5vkl3v13inz253kg0ahvg087qc9m5njj4ll5lggjamr90lp8q")))

