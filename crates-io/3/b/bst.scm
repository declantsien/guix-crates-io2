(define-module (crates-io #{3}# b bst) #:use-module (crates-io))

(define-public crate-bst-0.0.1 (c (n "bst") (v "0.0.1") (d (list (d (n "compare") (r "*") (d #t) (k 0)) (d (n "ordered_iter") (r "*") (o #t) (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "13av6cgxr0mvw51k2y0qdk6j48d07dkwal70z6vdlkffvpbvyav8") (f (quote (("default" "ordered_iter"))))))

