(define-module (crates-io #{3}# b bfk) #:use-module (crates-io))

(define-public crate-bfk-0.1.0 (c (n "bfk") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1dnalf0hr1wmr6fvh9h1fj2zfnkd3rjl5gr6z5f2cj1pf88yh6vx")))

(define-public crate-bfk-0.1.1 (c (n "bfk") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1i19nc8bk71nkaala477mp0ydzap6cw4z0vjw9ksrxqf8vygyvq6")))

(define-public crate-bfk-0.1.2 (c (n "bfk") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1y61446zk3yjy49pykxw6bgfjjd0kmy1gy287sxhj4fvv71zcn7z")))

(define-public crate-bfk-0.2.0 (c (n "bfk") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "0fdz3hvgjb0qv2g9z04j6d3cxpk64lirrzp393b1lq1j0c9lpl94")))

(define-public crate-bfk-0.2.1 (c (n "bfk") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.22") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)))) (h "01w8zqrliq184mk6brxy2m6507vva0sb7qjfymfnamg6h3gpak0g")))

