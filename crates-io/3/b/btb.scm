(define-module (crates-io #{3}# b btb) #:use-module (crates-io))

(define-public crate-btb-0.1.0 (c (n "btb") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "16pkqh2w2hlrmv06v0qn8wi7dxjvqqmgg7flgqysjdc1hbrzax4a")))

(define-public crate-btb-0.1.1 (c (n "btb") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0crr2l7779nfs7w1nwbwr5sin35n740i0i3pdhyhhr3iln705kcq")))

(define-public crate-btb-0.1.2 (c (n "btb") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qqs2vpm8j3kmdfrjznqxpdv152wk4ims4n1f8vgavxvwz1w3mwp")))

(define-public crate-btb-0.2.0 (c (n "btb") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v13nbyj4m0lm81bdy0njdzp24r6cvqr7clg619apnxgys5x8mb8")))

