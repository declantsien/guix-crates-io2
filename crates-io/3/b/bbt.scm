(define-module (crates-io #{3}# b bbt) #:use-module (crates-io))

(define-public crate-bbt-0.1.0 (c (n "bbt") (v "0.1.0") (h "0r1v2kvn6yybkqibxlj6rwmanlrqxsfzdlbj1637fqr9qr45xp42")))

(define-public crate-bbt-0.2.0 (c (n "bbt") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.70") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.24") (d #t) (k 2)))) (h "15m62xv2axyr3ylh5di9q484p5h45gh2k09iw2pj8plnhpvx4ylk")))

