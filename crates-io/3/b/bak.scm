(define-module (crates-io #{3}# b bak) #:use-module (crates-io))

(define-public crate-bak-1.0.0 (c (n "bak") (v "1.0.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0hwhpvxfq0zs55a9lkjkq7wyvcjhai4gfnfj452ghsg91bqqbgx2")))

(define-public crate-bak-1.0.1 (c (n "bak") (v "1.0.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0zzzjf0h1bkf7hxf6y9ylrs4hc6h3swx5fljgc0d4aaylyh8xrgx")))

(define-public crate-bak-2.0.0 (c (n "bak") (v "2.0.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "17iwsngla1rmvs02cw7lzrj04cr0sf8dqj5r01wxda9i7mlb9fiq")))

