(define-module (crates-io #{3}# b bun) #:use-module (crates-io))

(define-public crate-bun-0.1.0 (c (n "bun") (v "0.1.0") (d (list (d (n "dirs") (r "^3") (d #t) (k 0)) (d (n "key-list") (r "^1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "14nn62gscyc8j93xbfxka21s07423194h25ykzdi6zkmz0jn6y5r")))

