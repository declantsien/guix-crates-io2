(define-module (crates-io #{3}# b bru) #:use-module (crates-io))

(define-public crate-bru-0.1.0 (c (n "bru") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0dp9jc6kfa152zsv9zj4qf9n61kaq6w8s4xm0w1hcs9dg43dvdnj")))

(define-public crate-bru-0.1.1 (c (n "bru") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "08dq6l4al8kybh31f5p799hhgpbbhywdd9ag3k1r0z5g9p1qj2nl")))

(define-public crate-bru-0.1.2 (c (n "bru") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0cav7iy939r4dmc11qjqhqm1nkn0pvl14bf7lzl15184kiqv18yc")))

(define-public crate-bru-0.1.3 (c (n "bru") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "0gbbsl6h33rlhxijsdd7fmfrbagw16n8qw2v778baar95ibz082v")))

(define-public crate-bru-0.1.4 (c (n "bru") (v "0.1.4") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1fgxl1akva8sgyc99yh1bxx02yxl04nv0w6rw6pqllvbf70qxvv1")))

(define-public crate-bru-0.1.5 (c (n "bru") (v "0.1.5") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1zsva367dg88vniqnidi3sgay8y2ixcp6an4mb6rihhgf519hwzq")))

(define-public crate-bru-0.1.6 (c (n "bru") (v "0.1.6") (d (list (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.6") (d #t) (k 0)) (d (n "which") (r "^4.4.0") (d #t) (k 0)))) (h "1s0092a29fgk183gp7mkp9c0sfpsh6xd492ajn75a63yp1qnfza4")))

