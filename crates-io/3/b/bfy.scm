(define-module (crates-io #{3}# b bfy) #:use-module (crates-io))

(define-public crate-bfy-0.1.0 (c (n "bfy") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.14") (f (quote ("derive" "color" "cargo" "wrap_help" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "1ax1yf7irgnj9nqrgdavfy74ljq0r1ikpx71x8wfx3wq4v4qxg3c")))

(define-public crate-bfy-0.1.1 (c (n "bfy") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.14") (f (quote ("derive" "color" "cargo" "wrap_help" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "console") (r "^0.15.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)))) (h "0ny5qr7fkz47nvmnq9b8r4q22jn8aib2prs7xvjhjgn602syk53r")))

(define-public crate-bfy-0.1.2 (c (n "bfy") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.12") (f (quote ("derive" "color" "cargo" "wrap_help" "suggestions"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "console") (r "^0.15.7") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)))) (h "1avh2dixclymh9q8ahgch77xd0vzc9n7xxbdg1ix5jkcl5yjwhc2")))

