(define-module (crates-io #{3}# b bsr) #:use-module (crates-io))

(define-public crate-bsr-0.5.0 (c (n "bsr") (v "0.5.0") (h "15gfp3iq8kcl1c8icnxvxd388i059w6v5bmvdrq5bkkwym04skbh") (y #t)))

(define-public crate-bsr-0.6.0 (c (n "bsr") (v "0.6.0") (h "0s8ajfpdrmql86aa7bbvls3jvjb7f9kj957zbs0ik5z4vnvmd6yi") (y #t)))

(define-public crate-bsr-0.7.0 (c (n "bsr") (v "0.7.0") (h "02xdf0l77xjsxamhjm8v5bkf3jisgmv2rj2mlp2sqwny660qinh3") (y #t)))

(define-public crate-bsr-0.8.0 (c (n "bsr") (v "0.8.0") (h "11mymy3pq38lz9i9yg7v1r0d6bwhlylpzbl6lrxla3ymss5hilva") (y #t)))

(define-public crate-bsr-0.9.0 (c (n "bsr") (v "0.9.0") (h "0ajxng2b8facn46as8k9nkkws5psavijgnv1by5wf5dcr0w25wq3") (y #t)))

(define-public crate-bsr-0.10.0 (c (n "bsr") (v "0.10.0") (h "08ywypi7fj1gkwmab420wi0gl8l6c99ar2dbvc2npjniqv53hpfl") (y #t)))

(define-public crate-bsr-0.11.0 (c (n "bsr") (v "0.11.0") (h "1kp3vblvhn1wzr88n8m763ypxbnyksmwq42yc9jgly915qwgyh78")))

