(define-module (crates-io #{3}# b brx) #:use-module (crates-io))

(define-public crate-brx-0.1.0 (c (n "brx") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "093i53w65j00xbfk58ffsva9fawjmsj5zg01jnr80l4cqblc32f0") (y #t)))

(define-public crate-brx-0.1.1 (c (n "brx") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "0xrld1cryvhhdil08pz4864d0kis5dq4wzdw6d3id4syvgxz1vcg") (y #t)))

(define-public crate-brx-0.1.2 (c (n "brx") (v "0.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.5.6") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "09c4wikjizgpbgrjsj2qdy8jm0lcvaz5dqp1lf41l7j4q5r0a266") (y #t)))

