(define-module (crates-io #{3}# b beb) #:use-module (crates-io))

(define-public crate-beb-0.0.1 (c (n "beb") (v "0.0.1") (h "0f70sj32j1va56n0006z63ywlmn8r7gwylr86vkcj6g4vbmn77mb")))

(define-public crate-beb-0.0.2 (c (n "beb") (v "0.0.2") (h "0j31i0if7fpjfir1visyicag2hxpqdkiry2wbpkkj28fzgpbd0ww")))

