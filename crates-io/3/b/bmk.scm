(define-module (crates-io #{3}# b bmk) #:use-module (crates-io))

(define-public crate-bmk-0.1.0 (c (n "bmk") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "13h7x7qz1a7kmlc80cbxlgasmk7fjs8j712i6460cyxf0hanlgm3")))

(define-public crate-bmk-0.1.1 (c (n "bmk") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)))) (h "1r43nxdbka0g83b686f39728alfn0079gbdklc5gr4m9ilxyrnrd")))

