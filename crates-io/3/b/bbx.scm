(define-module (crates-io #{3}# b bbx) #:use-module (crates-io))

(define-public crate-bbx-0.1.0 (c (n "bbx") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)))) (h "1z8hh8pxsk2hw9mmn9za706f3a16i1lv9crn1n5kmfsax9bfvkva") (f (quote (("track_open_tags") ("parser_rules" "track_open_tags") ("nightly") ("default" "track_open_tags" "parser_rules")))) (r "1.65")))

(define-public crate-bbx-0.2.0 (c (n "bbx") (v "0.2.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "082hmn8zfj14nnlz1vchwrgzi5nc80s4r1drmmqpr7ywsgadhf9y") (f (quote (("track_open_tags") ("parser_rules" "track_open_tags") ("nightly") ("default" "track_open_tags" "parser_rules")))) (r "1.65")))

(define-public crate-bbx-0.3.0 (c (n "bbx") (v "0.3.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "03fjpj7528xwq3vl8lcdyqabs3wn8aia67m5jaqc1rd88iq7la1m") (f (quote (("track_open_tags" "alloc") ("parser_rules" "alloc" "track_open_tags") ("nightly") ("default" "html_gen" "track_open_tags" "parser_rules") ("alloc")))) (s 2) (e (quote (("html_gen" "alloc" "track_open_tags" "parser_rules" "dep:html-escape")))) (r "1.65")))

(define-public crate-bbx-0.3.1 (c (n "bbx") (v "0.3.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (o #t) (d #t) (k 0)) (d (n "static_assertions") (r "^1") (d #t) (k 0)))) (h "0h7ghzmmkj4ilpll9znwqsl9zn8bjrls4nhirnz1if7w8f9f090b") (f (quote (("track_open_tags" "alloc") ("parser_rules" "alloc" "track_open_tags") ("nightly") ("default" "html_gen" "track_open_tags" "parser_rules") ("alloc")))) (s 2) (e (quote (("html_gen" "alloc" "track_open_tags" "parser_rules" "dep:html-escape")))) (r "1.65")))

