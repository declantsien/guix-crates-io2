(define-module (crates-io #{3}# b bdf) #:use-module (crates-io))

(define-public crate-bdf-0.1.0 (c (n "bdf") (v "0.1.0") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "02lq3h9m76l9hzhjzmnflzs8fkxybsgbp313s5ancsj677qr6rrz")))

(define-public crate-bdf-0.2.0 (c (n "bdf") (v "0.2.0") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "0qqvs251jcxdw19pgi03zpzgjj3mjjzqaq7642q0hl158nzl741v")))

(define-public crate-bdf-0.2.1 (c (n "bdf") (v "0.2.1") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "1xdy74r5m5h6ydzxf78da2gflhmrfc8nqh1hfdsqpzwkmm41l1hx")))

(define-public crate-bdf-0.2.2 (c (n "bdf") (v "0.2.2") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "1ch8ry7llaqvvibj3598yf38h9kjbsvjm6f1z51fi7r24lkk7jhk")))

(define-public crate-bdf-0.2.3 (c (n "bdf") (v "0.2.3") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "0c4d585c5k1d0pwc9645fqskr30l53qcxsjp8733pw4w5y75y620")))

(define-public crate-bdf-0.3.0 (c (n "bdf") (v "0.3.0") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "1nhizwcdbaggwza5ga7gmqqf77dgzfp3sg7m4fg73fnnw5m2ypr4")))

(define-public crate-bdf-0.4.0 (c (n "bdf") (v "0.4.0") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "1sn1sldcw49a54j0myx0q496sxwz1bajfxw0294hd8388fzpfxqh")))

(define-public crate-bdf-0.5.0 (c (n "bdf") (v "0.5.0") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "1ffw1ska1ikjjy8can49k35v0w6hasybxjan5q6cmnzpxzz5ni9a")))

(define-public crate-bdf-0.5.1 (c (n "bdf") (v "0.5.1") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "13yl78q1k8pbl46sr3kl3mkqz1m647fdvc1p937sk08q1zi6dr9g")))

(define-public crate-bdf-0.5.2 (c (n "bdf") (v "0.5.2") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "1xjzk58m52scjvvnkl0cx3rfq69wvdz9najxysk9cz03rsfi0q9b")))

(define-public crate-bdf-0.5.3 (c (n "bdf") (v "0.5.3") (d (list (d (n "bit-set") (r "*") (d #t) (k 0)))) (h "03h8hn34rn0xl8sah2p4dvpcfcra42lfzy161i78vh2p4n6abnvv")))

(define-public crate-bdf-0.5.4 (c (n "bdf") (v "0.5.4") (d (list (d (n "bit-set") (r "^0.3") (d #t) (k 0)))) (h "1f0y2bpchlprl9xqan8p727agj7kn69w8fqyp8lx82k4h6gqi6z3")))

(define-public crate-bdf-0.5.5 (c (n "bdf") (v "0.5.5") (d (list (d (n "bit-set") (r "^0.3") (d #t) (k 0)))) (h "1fay8djbw7r67qy888p2n9f5inn4f42r8n6w74mdr9rx0ip9i2x2")))

(define-public crate-bdf-0.6.0 (c (n "bdf") (v "0.6.0") (d (list (d (n "bit-set") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "055gh03qxmyiwy0xraq91md42s2gzxgy8i1zi3awqhkgis0scl7m")))

