(define-module (crates-io #{3}# b bva) #:use-module (crates-io))

(define-public crate-bva-0.1.0 (c (n "bva") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1734dlay2narxslbfwha7j4xly3cwn2bmass5yf9ixbl8pj37lac")))

(define-public crate-bva-0.1.1 (c (n "bva") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0d2znd7kzq9vv1f6pcyzl92v9rrj0phd42scz3898ff1pjhk0pjj")))

(define-public crate-bva-0.2.0 (c (n "bva") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "00l6jnv00lzmgp7jz12l7an5wfsbjjv6diql7c5bv64mn6fycqm9") (r "1.61")))

