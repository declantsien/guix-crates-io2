(define-module (crates-io #{3}# b bcp) #:use-module (crates-io))

(define-public crate-bcp-0.1.0 (c (n "bcp") (v "0.1.0") (d (list (d (n "pbr") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0lrmahqdj2b9sh39574farmbqvqax8ny1p59k7f2vximcbcyq80j")))

(define-public crate-bcp-0.2.0 (c (n "bcp") (v "0.2.0") (d (list (d (n "pbr") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1iacdzgp5bpagm5impqikmpq2v6yvz5wypliiwkjzniqckz0fmgx")))

(define-public crate-bcp-0.3.0 (c (n "bcp") (v "0.3.0") (d (list (d (n "pbr") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "1bnxbsfj6y4c7mw1jxdcffrp5ngvl2p7w1zm2zlql0v79fa637bf")))

(define-public crate-bcp-0.3.1 (c (n "bcp") (v "0.3.1") (d (list (d (n "pbr") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)))) (h "0gb6z5wxbd9lkkxqp8y22w092y981s5n0pacwyw7wyiz82d0c2l5")))

