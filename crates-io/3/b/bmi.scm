(define-module (crates-io #{3}# b bmi) #:use-module (crates-io))

(define-public crate-bmi-0.1.2 (c (n "bmi") (v "0.1.2") (h "0ykkvmhsda80m73l7d4wnm8k5i0s95q81xylk3nk8km45k9rnpn9")))

(define-public crate-bmi-0.1.3 (c (n "bmi") (v "0.1.3") (h "1vchh2b6crlagdqi7x6wm8nv6sija1s2dacakvni949jzvbmsd8a")))

