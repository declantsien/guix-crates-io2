(define-module (crates-io #{3}# b b2b) #:use-module (crates-io))

(define-public crate-b2b-0.1.0 (c (n "b2b") (v "0.1.0") (d (list (d (n "clap") (r "^2.3.0") (f (quote ("suggestions" "color"))) (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.0.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.10") (d #t) (k 0)))) (h "1r5055978bd02fizz1ykysb39n9l984fnpa1bscjc96nci3a7k62") (y #t)))

