(define-module (crates-io #{3}# b bui) #:use-module (crates-io))

(define-public crate-bui-0.0.1 (c (n "bui") (v "0.0.1") (d (list (d (n "bytemuck") (r "^1.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "constrainer") (r "^0.0.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "owned_ttf_parser") (r "^0.15") (d #t) (k 0)) (d (n "ttf-parser") (r "^0.15") (d #t) (k 0)) (d (n "wgpu") (r "^0.13") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wgpu") (r "^0.13") (f (quote ("webgl"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "winit") (r "^0.26") (d #t) (k 0)))) (h "0310pf9is29vxyn28lal8y63xi6s7qdyzycbnw6nz9p03s4sa8ks") (y #t)))

