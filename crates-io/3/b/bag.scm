(define-module (crates-io #{3}# b bag) #:use-module (crates-io))

(define-public crate-bag-0.1.0 (c (n "bag") (v "0.1.0") (h "104nksa2smmpyzv4sbk03xbnc28k95i25i7mp4a73s6zfh02k9ck") (y #t)))

(define-public crate-bag-0.1.1 (c (n "bag") (v "0.1.1") (h "04fbxl4sb5z3q7g9r5hijppkk9sjcajgfq1iyvdfbzczznfqrk12") (y #t)))

(define-public crate-bag-0.1.2 (c (n "bag") (v "0.1.2") (h "0zyz46nxk1mhj718588zdy19d37sxkhcwripb40hj60dblii48ll") (y #t)))

(define-public crate-bag-0.1.2-2 (c (n "bag") (v "0.1.2-2") (h "11mm4nhsqbra44i6f2qqrxga4d94nmxhiiyc60v5mm7binvamas6") (y #t)))

(define-public crate-bag-0.1.2-3 (c (n "bag") (v "0.1.2-3") (h "0fms90pck4h386mhkaqhwaz03ngmch0gi3lkxgi94kk4k57wycqb") (y #t)))

