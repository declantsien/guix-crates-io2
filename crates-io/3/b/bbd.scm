(define-module (crates-io #{3}# b bbd) #:use-module (crates-io))

(define-public crate-bbd-0.1.0 (c (n "bbd") (v "0.1.0") (d (list (d (n "bbd-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)))) (h "0jlzf8g0vr6p3bjy0qlv4m6zmg0zb0i3cyl2qv26p299qxawb9sb")))

(define-public crate-bbd-0.1.1 (c (n "bbd") (v "0.1.1") (d (list (d (n "bbd-lib") (r "^0.1.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)))) (h "1q4z9v1ybk02agqswnmvkjvmks833kpvjn7pka1bggmn4gf89d93")))

(define-public crate-bbd-0.2.0 (c (n "bbd") (v "0.2.0") (d (list (d (n "bbd-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)))) (h "111axsfxam82z9580v37h6ag0kq13977jajpm5qlz7xpf3cybsvr")))

