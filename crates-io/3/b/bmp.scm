(define-module (crates-io #{3}# b bmp) #:use-module (crates-io))

(define-public crate-bmp-0.0.2 (c (n "bmp") (v "0.0.2") (h "16wzds5f45fb1djyzms6gz50pcghkq9mvs1vcc0dxw7p3kxqnxf2")))

(define-public crate-bmp-0.0.3 (c (n "bmp") (v "0.0.3") (h "1f056g2air9l68vcimkzhz05z0mbzapzgdq66brcn81974017i6a")))

(define-public crate-bmp-0.0.4 (c (n "bmp") (v "0.0.4") (h "0xwq2vd6mymdwchhy6y7w991gp1mdb0w6vqy5x9qpfld4s38ll6y")))

(define-public crate-bmp-0.0.5 (c (n "bmp") (v "0.0.5") (h "0r5x57r3ii2b75r39gbpvqhmp9b585cbw36n5sslg030y20dfisf")))

(define-public crate-bmp-0.0.6 (c (n "bmp") (v "0.0.6") (h "15sfvpsw28br2278mbn9jds38p0amrh1xqsaap3fiah2c5rfq8gc")))

(define-public crate-bmp-0.0.7 (c (n "bmp") (v "0.0.7") (h "024phncxg05k9dwzfpzk4sli62dm71640yr1jk3cqymkqa8d4n4n")))

(define-public crate-bmp-0.0.8 (c (n "bmp") (v "0.0.8") (h "12clqd406fcm9wprl3f2zajyjmf19j0ngj03d6wr9pqphjdgc3sp")))

(define-public crate-bmp-0.1.0 (c (n "bmp") (v "0.1.0") (h "0bz96li7k3gcnl2vbfxmvq50sfi4gk2jxhz5w1b21nymv0j86ddz")))

(define-public crate-bmp-0.1.1 (c (n "bmp") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "1cwghp33vr5b6zimj6pqysqgbb7wgxlzmbjvhp5knr4rznabj967")))

(define-public crate-bmp-0.1.2 (c (n "bmp") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.3.5") (d #t) (k 0)))) (h "07vzxgjdw6pw4521xjsjydqqm2hk2lnzif6zafbr72xfc941ysvm")))

(define-public crate-bmp-0.1.3 (c (n "bmp") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "1fhl1gnvvj1scyq0jycydfqg3q4q9f6gk4i5lw3fwcn89s4dz3xn")))

(define-public crate-bmp-0.1.4 (c (n "bmp") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "02cm075qpl3bk1s6111f52agbwahr9pczbila9gwwj1jj9dbp9ik")))

(define-public crate-bmp-0.2.0 (c (n "bmp") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)))) (h "0xlscgqv246w8sw8r2bz5zlmzca1f9nhxjm1y50m5cdkvi6yi02d")))

(define-public crate-bmp-0.3.0 (c (n "bmp") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1whrxip389v7hfb1ff0z59lw66q0av4bqxwdnhmnk2zmsdy03yfp")))

(define-public crate-bmp-0.4.0 (c (n "bmp") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1nkn1aszikb7s3cs501qnsmh5k4jdavncjjjivixvid7hdpyp37g")))

(define-public crate-bmp-0.5.0 (c (n "bmp") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)))) (h "1bhiymfvnlfwj562kgn9khgssrkach5jssalcilsr1c0yps5z639")))

