(define-module (crates-io #{3}# b bbq) #:use-module (crates-io))

(define-public crate-bbq-0.1.0 (c (n "bbq") (v "0.1.0") (h "0h92zzkxcgrblfhng0gdzpylf00kz3igq7gnwp2kbls920wccd10") (y #t)))

(define-public crate-bbq-0.1.1 (c (n "bbq") (v "0.1.1") (h "17xj6a79aypc2dmqspqj4fs1cmran1ayxmcw7hm7rzwpz3lfcxf1") (y #t)))

(define-public crate-bbq-0.1.2 (c (n "bbq") (v "0.1.2") (h "012l8qv3n70igrh3x5j70ixs4ksmssy8spf6l17dbb8mj2nrmmi5") (y #t)))

(define-public crate-bbq-0.1.3 (c (n "bbq") (v "0.1.3") (h "11f5r9nd7w22ykf4a3aqyccvrnpqnhl1yzjcvmi0i2dvwcfhw565")))

