(define-module (crates-io #{3}# b blc) #:use-module (crates-io))

(define-public crate-blc-0.1.0 (c (n "blc") (v "0.1.0") (d (list (d (n "lambda_calculus") (r "^0.1.4") (d #t) (k 0)))) (h "1qv0r1aqf7biv18rzc7sdpyrx0r2h1db3x62gqh6djyzlzr8mi3q")))

(define-public crate-blc-0.1.1 (c (n "blc") (v "0.1.1") (d (list (d (n "lambda_calculus") (r "^0.1.4") (d #t) (k 0)))) (h "1xsb6ccf39jfaxsgahd1myyg0r44vppvivz5jzykh4qwgbjc6v04")))

(define-public crate-blc-0.1.2 (c (n "blc") (v "0.1.2") (d (list (d (n "lambda_calculus") (r "^0.1.4") (d #t) (k 0)))) (h "1vs6rr4cd8k5z0i7jpfhrfczag8hnhxwkp19hmcmqv00f1qwimwg")))

(define-public crate-blc-0.1.3 (c (n "blc") (v "0.1.3") (d (list (d (n "lambda_calculus") (r "^0.1.4") (d #t) (k 0)))) (h "1diffcm5y3rm5r65ha414iwsfy52jf0qh8d227nffmcbqpd7hwwr")))

(define-public crate-blc-0.2.0 (c (n "blc") (v "0.2.0") (d (list (d (n "lambda_calculus") (r "^0.1.4") (d #t) (k 0)))) (h "0y4zvs4cli51zmsqwq42y8vkvfdpqri3fd390r4wxzp1lv4v7dx3")))

(define-public crate-blc-0.2.1 (c (n "blc") (v "0.2.1") (d (list (d (n "lambda_calculus") (r "^0.1.4") (d #t) (k 0)))) (h "0720ylv2j78v0j8fsr001a0068mzni1zfswz4w7qnsvg0r670n8z")))

(define-public crate-blc-0.3.0 (c (n "blc") (v "0.3.0") (d (list (d (n "lambda_calculus") (r "^0.6.0") (d #t) (k 0)))) (h "14wjap2id0vxrlmg3gjspymsiyjvj4jxrkxdamvyjh4q2nv4z8js")))

(define-public crate-blc-0.3.1 (c (n "blc") (v "0.3.1") (d (list (d (n "lambda_calculus") (r "^0.6.0") (d #t) (k 0)))) (h "1yp3xnq7iah48vgh6qxx01mn9w7ll7qnriihgmp1jm0qpcqcy921")))

(define-public crate-blc-0.4.0 (c (n "blc") (v "0.4.0") (d (list (d (n "lambda_calculus") (r "^0.7.0") (d #t) (k 0)))) (h "1pwa4ys141zdn7spwp3qv90hzm8lsw6cks377skzm9nq7pv61dmx")))

(define-public crate-blc-0.4.1 (c (n "blc") (v "0.4.1") (d (list (d (n "lambda_calculus") (r "^0.8.0") (d #t) (k 0)))) (h "0104zvkn72ri6nc7275q14hlhk0gvzph2mvqrxh7l5zlm2bpgpzb")))

(define-public crate-blc-0.4.2 (c (n "blc") (v "0.4.2") (d (list (d (n "lambda_calculus") (r "^0.10.0") (d #t) (k 0)))) (h "02fxx1skh71lfq1pwynd6059aqijfwwbwqgdlfwisfnjqr7d6xj0")))

(define-public crate-blc-0.4.3 (c (n "blc") (v "0.4.3") (d (list (d (n "lambda_calculus") (r "^0.11.0") (d #t) (k 0)))) (h "03m33bmx1l5h5zygcvmhjji6y3hdlhp0505pv7drsaxg9668anbk")))

(define-public crate-blc-0.4.4 (c (n "blc") (v "0.4.4") (d (list (d (n "lambda_calculus") (r "^0.12.0") (d #t) (k 0)))) (h "07zppsv2ccs7m25jmabjmxab1bx8g46q5328p1alasawdpv8r1hd")))

(define-public crate-blc-0.4.5 (c (n "blc") (v "0.4.5") (d (list (d (n "lambda_calculus") (r "^1.0") (d #t) (k 0)))) (h "0kfpzmdfv1priq24azk3r1m7a8rsjxra3j3lwpsf66384yvmgxyh")))

(define-public crate-blc-0.5.5 (c (n "blc") (v "0.5.5") (d (list (d (n "lambda_calculus") (r "^2.0") (d #t) (k 0)))) (h "0c9ban8f1b3wq6mii1b1m0ximkjzcsa05pl38jqv6gr33ibp4vyc")))

(define-public crate-blc-0.5.6 (c (n "blc") (v "0.5.6") (d (list (d (n "lambda_calculus") (r "^2.0") (d #t) (k 0)))) (h "1vchnsjz6kh9i8607asvgs858r5m2g1qs1kr60ixwz9009sd6g6g")))

(define-public crate-blc-0.6.0 (c (n "blc") (v "0.6.0") (d (list (d (n "lambda_calculus") (r "^2.0") (d #t) (k 0)))) (h "0hxx43vcbf00va1ysq6d9k95z40a1jpzjhysfs15pxxm4wv90s63")))

