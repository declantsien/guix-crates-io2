(define-module (crates-io #{3}# b ban) #:use-module (crates-io))

(define-public crate-ban-0.1.0 (c (n "ban") (v "0.1.0") (h "1m1jffa49gvn6jiig8gj6j0r2ifxbpi2ihfhis6i6ppzazicglvn")))

(define-public crate-ban-0.1.1 (c (n "ban") (v "0.1.1") (d (list (d (n "morel") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1xabpislnm1h46ch6421rq01a97i2v73brg7na12rdl8qn4jmvmf")))

(define-public crate-ban-0.1.2 (c (n "ban") (v "0.1.2") (d (list (d (n "morel") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "10zzqjlff8csm3v8zywg1i5qyqga88p4ywhr97cn477y9902p1al")))

(define-public crate-ban-0.11.0 (c (n "ban") (v "0.11.0") (d (list (d (n "morel") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1i2lxjq8g4vcdg77j2kq26j4pzwmy5kj1wkf5cd2k72b4s1dcl9c")))

(define-public crate-ban-0.12.0 (c (n "ban") (v "0.12.0") (d (list (d (n "morel") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1jly7b5lmw6xcmkphk13fngp3zil4f15ccw5jbyzr1ykdjcvfiz8")))

(define-public crate-ban-0.2.0 (c (n "ban") (v "0.2.0") (d (list (d (n "morel") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "06imjxcd2rm8rr7znm3gyh1mcamvx2ggbpgyzgp5pk6k11j6ss51") (y #t)))

(define-public crate-ban-0.20.0 (c (n "ban") (v "0.20.0") (d (list (d (n "morel") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0cfp4fmgzacx29dfhmyl86iy9k8rfprg3apy78mbr3b4lfh5fk7f")))

(define-public crate-ban-0.30.0 (c (n "ban") (v "0.30.0") (d (list (d (n "morel") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1pisdjq4qrh0wsd8lcvpmiqsp0b214ydj35jfval3dx8cdijdaw0")))

(define-public crate-ban-0.40.0 (c (n "ban") (v "0.40.0") (d (list (d (n "morel") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1xn893a09bghn189w11vaa6w7qrwrwv8m4v108y8ry56b8gc3ypk")))

(define-public crate-ban-0.40.1 (c (n "ban") (v "0.40.1") (d (list (d (n "morel") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0a1680v6gvl4xij76sxqa80kfwqkcb3g6vvaixcamc1gbcpya9f0")))

(define-public crate-ban-0.50.1 (c (n "ban") (v "0.50.1") (d (list (d (n "morel") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0h73hzya3rri8y00fnaj61cm7wyfjrrshxxv77qrx3frg2pd855s")))

(define-public crate-ban-0.50.2 (c (n "ban") (v "0.50.2") (d (list (d (n "morel") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1k7b3d8n4lr1q288iqdvv9xvvfjiy20rzpb34186grid0nh64mll")))

(define-public crate-ban-0.51.0 (c (n "ban") (v "0.51.0") (d (list (d (n "morel") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1ygsfw9pp1wcjczdlzjqlfz8s2wra6cmz6ivhr4343xc7q0281k2")))

(define-public crate-ban-0.52.0 (c (n "ban") (v "0.52.0") (d (list (d (n "morel") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "18xypjm0vsgx86w9x1a2db6m07in2l3mwifbnbsqmrc3h4mkd7w8")))

(define-public crate-ban-0.60.0 (c (n "ban") (v "0.60.0") (d (list (d (n "morel") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "0bcl12am4v1x8gpqdzbxrk1marsi118fxzl08zz6x78mdh1vqgf6")))

(define-public crate-ban-0.60.1 (c (n "ban") (v "0.60.1") (d (list (d (n "morel") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.102") (d #t) (k 0)) (d (n "unicode-ident") (r "^1.0.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.10") (d #t) (k 0)))) (h "1zpmgvx29sicsflyirprrvbpkd6yy993xj77qbgh7r74gvj4mxyq")))

