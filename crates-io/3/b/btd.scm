(define-module (crates-io #{3}# b btd) #:use-module (crates-io))

(define-public crate-btd-1.0.0 (c (n "btd") (v "1.0.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "edit") (r "^0.1.5") (d #t) (k 0)) (d (n "open") (r "^5.1.2") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.33") (d #t) (k 0)) (d (n "system") (r "^0.3.0") (d #t) (k 0)) (d (n "winres") (r "^0.1.12") (d #t) (k 1)))) (h "0r2hm9cd8fhksymga2bqhs6dmmzv7f1kf1f1y0w0m9s2sajx0n0v")))

