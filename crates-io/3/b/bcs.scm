(define-module (crates-io #{3}# b bcs) #:use-module (crates-io))

(define-public crate-bcs-0.0.0 (c (n "bcs") (v "0.0.0") (h "0fds6lpw17agcgvhy5sdj803z7g4iwblalq4sfdvfi0pp8dhs2f1")))

(define-public crate-bcs-0.1.1 (c (n "bcs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "042n56f712mwg0s2bmcnnhwr3vsr6qlsn4m7ipimjlhwzgdz54yy")))

(define-public crate-bcs-0.1.2 (c (n "bcs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1b4i2p6lr7bbzbhmnhza2lxyn9mnhd559zz65ddik2ssa67bcsm8")))

(define-public crate-bcs-0.1.3 (c (n "bcs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0cr8rdzymxczjqj81arnrd1azwha9h5mbww2c6q66wmg7qzdh3si")))

(define-public crate-bcs-0.1.4 (c (n "bcs") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1k1wml05wwsjx32xn4115gcxan3xqx2cjjh8wxq2n02ky30v81lb")))

(define-public crate-bcs-0.1.5 (c (n "bcs") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0xygvpmxpg9qf43c7yq3b59kw8j60fkx8qflllhl8q4sn7lgzlsb")))

(define-public crate-bcs-0.1.6 (c (n "bcs") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "02j4gd4kh6829lx8d2xxf9dgz363s5pv1ijxhnvlymjx5y55kdl5")))

