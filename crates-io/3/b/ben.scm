(define-module (crates-io #{3}# b ben) #:use-module (crates-io))

(define-public crate-ben-0.1.0 (c (n "ben") (v "0.1.0") (h "17h9msszsipm8ibk1wqc8k259m36mssmffwhzm9zpkrg2wyrgi6z")))

(define-public crate-ben-0.1.1 (c (n "ben") (v "0.1.1") (h "1dwqfag9vk0fxp7mc6y67lilyq6m1ds6xmzcgvfadzvqcsxwv77j")))

(define-public crate-ben-0.1.2 (c (n "ben") (v "0.1.2") (h "1z99nrn5w15dija7vlzfpxrfy1pg7ldis6flzf18v1za56cs9kg4")))

(define-public crate-ben-0.1.3 (c (n "ben") (v "0.1.3") (h "0dh1nq2z1fcv1mcyv9wixr1jdlvwyrqkbxsnglzp8qjzmvywrqzj")))

(define-public crate-ben-0.1.4 (c (n "ben") (v "0.1.4") (h "0brik9jxyzsy8krc51njijr7szysddzqksa1l2x25737wsx64brm")))

(define-public crate-ben-0.1.5 (c (n "ben") (v "0.1.5") (h "19k3ymnsfpjvaxqgxn9qikbc5ybp008mqgghniz3nk7gkh9clyv8")))

(define-public crate-ben-0.1.6 (c (n "ben") (v "0.1.6") (h "1wfd21y3niy08km7g8csl98rzsqj83mwj1f84q65cgs2hxx5zf6i")))

(define-public crate-ben-0.1.7 (c (n "ben") (v "0.1.7") (h "1d4r0h4lljkhm2jm22dgikljw6hz1i08zh040440gf8zr9jjxy5z")))

(define-public crate-ben-0.1.8 (c (n "ben") (v "0.1.8") (d (list (d (n "itoa") (r "^0.4.5") (d #t) (k 0)))) (h "1mmrll5x84hyv03zyd3vnfk36y5r68b8gwv88cmr2jy61jc54jlz")))

(define-public crate-ben-0.1.9 (c (n "ben") (v "0.1.9") (d (list (d (n "itoa") (r "^0.4.5") (d #t) (k 0)))) (h "1yyp4ip8g4rca1qclqwzbj2n77svz3nlrfk87icswbskziva6r8f")))

(define-public crate-ben-0.1.10 (c (n "ben") (v "0.1.10") (d (list (d (n "itoa") (r "^0.4.5") (d #t) (k 0)))) (h "1rpjkn74bzlzcqcrz6jlk2ssmr1jbbsycaj85hsx4bkaay05jjvg")))

(define-public crate-ben-0.2.0 (c (n "ben") (v "0.2.0") (d (list (d (n "itoa") (r "^0.4.5") (d #t) (k 0)))) (h "01vr08i28zpqhvgjy28sbqys6z4g80nsxywgrwyx08ig48if0giq")))

