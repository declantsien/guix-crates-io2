(define-module (crates-io #{3}# b bxs) #:use-module (crates-io))

(define-public crate-bxs-0.1.0 (c (n "bxs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "13af7xc4wid3slw7sgi4pa8a6ky40frd3k1p37f7aczn6y4j2a9s")))

(define-public crate-bxs-0.1.1 (c (n "bxs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)))) (h "1dfvavlaz2vmdhg4znaaqlxjlr1jimkdd833zj4lnwcxp3r7nd2c")))

