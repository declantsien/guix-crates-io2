(define-module (crates-io #{3}# b bee) #:use-module (crates-io))

(define-public crate-bee-0.1.0 (c (n "bee") (v "0.1.0") (h "0gwl9nl7kxn0hikv4gwd5liz8g1i216w5cv6g6pgfmyx7ivvv7x2")))

(define-public crate-bee-0.1.1 (c (n "bee") (v "0.1.1") (h "0zwycpbdx3gicina73y97j1mllvdah02qyk9cvr851waxg53zdma")))

