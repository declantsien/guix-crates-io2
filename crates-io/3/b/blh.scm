(define-module (crates-io #{3}# b blh) #:use-module (crates-io))

(define-public crate-blh-0.1.0 (c (n "blh") (v "0.1.0") (h "0yl4z2irw0rf133lmzvmmc66w7rripxrmawvk7fdnvpz7b580ckd")))

(define-public crate-blh-0.2.0 (c (n "blh") (v "0.2.0") (h "08g44xxi80j32x1akk85l16yyanjic6spca118501r5qfp478sy5")))

(define-public crate-blh-0.3.0 (c (n "blh") (v "0.3.0") (d (list (d (n "approx") (r "^0.1.1") (d #t) (k 0)) (d (n "euclid") (r "^0.10.2") (d #t) (k 0)))) (h "07fymh97hafm69ydld63sqdyr13gf0qq8bm0rcavw8sfjz39751p")))

