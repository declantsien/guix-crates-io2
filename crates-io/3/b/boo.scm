(define-module (crates-io #{3}# b boo) #:use-module (crates-io))

(define-public crate-boo-0.1.0 (c (n "boo") (v "0.1.0") (h "01lc486jypbgp9sp21ara0w7q19by3aykld4anc41x80xnqqmayk")))

(define-public crate-boo-0.1.1 (c (n "boo") (v "0.1.1") (h "1ki32yzq70mad2kyq0vr10430519mcbbwx77zyfxfasggmnn9090")))

