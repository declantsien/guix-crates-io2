(define-module (crates-io #{3}# b bad) #:use-module (crates-io))

(define-public crate-bad-0.0.0 (c (n "bad") (v "0.0.0") (h "0j75mdvihn4gpslqd6l7br74l51kwlfq6hlbqxbdgdg9casxm3pz") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-bad-0.1.0 (c (n "bad") (v "0.1.0") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1pfqcv0v3nmg8jm63n312f1dmzj2g98r3q2cdy9bhapsx8mxkijj") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-bad-0.1.1 (c (n "bad") (v "0.1.1") (d (list (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "1sxn4nb77xvkr5iyv8bjz15ndlaiy417dkip6ndf5pcdkglkaxvf") (f (quote (("std") ("default" "std") ("alloc"))))))

