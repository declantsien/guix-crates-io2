(define-module (crates-io #{3}# b bml) #:use-module (crates-io))

(define-public crate-bml-0.1.0 (c (n "bml") (v "0.1.0") (d (list (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)))) (h "1ya66c4w0fx2z68pkx7dw9iwbxgdkg90hg01drf5ipxhcymxb91i")))

(define-public crate-bml-0.2.0 (c (n "bml") (v "0.2.0") (d (list (d (n "err-derive") (r "^0.1") (d #t) (k 0)) (d (n "ordered-multimap") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "smallstr") (r "^0.1") (d #t) (k 0)))) (h "1l80a1ah6md3kdwybv7pq88zw4yzj7gz8ika1s2j907v5rzpc3yv")))

(define-public crate-bml-0.3.0 (c (n "bml") (v "0.3.0") (d (list (d (n "ordered-multimap") (r "^0.3") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "smartstring") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hgbbpkpdfks4v4d60ib9g973ndg1a85h6z5d6x3vjvlmiz81ai7")))

(define-public crate-bml-0.3.1 (c (n "bml") (v "0.3.1") (d (list (d (n "ordered-multimap") (r "^0.6") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "smartstring") (r "^1.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0fvgc3mf6dc98f1nafgrzh18cwmaa1rvkwbnsx2k6q6rrrhxcz7l")))

