(define-module (crates-io #{3}# b blr) #:use-module (crates-io))

(define-public crate-blr-0.1.0 (c (n "blr") (v "0.1.0") (d (list (d (n "builder_derive_more") (r "^0.1") (d #t) (k 0)) (d (n "derive_builder") (r "^0.20") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "pyo3") (r "^0.20") (f (quote ("abi3-py310" "auto-initialize"))) (d #t) (k 0)) (d (n "pyo3_derive_more") (r "^0.1") (d #t) (k 0)) (d (n "pyo3_macros_more") (r "^0.1") (d #t) (k 0)) (d (n "semver") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "which") (r "^6.0") (d #t) (k 0)))) (h "1f80403sg10lfcva5jxry0z44a14chivb960cpr7c9lh8dcg62id") (r "1.70")))

