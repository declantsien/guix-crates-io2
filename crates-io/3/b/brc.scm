(define-module (crates-io #{3}# b brc) #:use-module (crates-io))

(define-public crate-brc-0.1.0 (c (n "brc") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rustyline") (r "^8.2.0") (d #t) (k 0)))) (h "1bsd3vvsbx74baz54m6pp6y8jvrz56ylzj3avrw9dzfkwsc14rgd") (y #t)))

