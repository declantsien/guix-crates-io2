(define-module (crates-io #{3}# b bcn) #:use-module (crates-io))

(define-public crate-bcn-0.1.0 (c (n "bcn") (v "0.1.0") (h "1f1l8743hbw6z5pi4778qjavw4hjv62a3ml3300ppwkdpg3m1s6d")))

(define-public crate-bcn-0.1.1 (c (n "bcn") (v "0.1.1") (h "0iy02732pv3cj7pj6gkcbk3zfr6xb0i7qfypiz9wyz6n3lbrhchp")))

