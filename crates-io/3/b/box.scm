(define-module (crates-io #{3}# b box) #:use-module (crates-io))

(define-public crate-box-0.1.0 (c (n "box") (v "0.1.0") (h "0r3d7jaadf1a527xzfvr49a17jlmp56809kn815ad8dij2w9j4nm")))

(define-public crate-box-0.2.0 (c (n "box") (v "0.2.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0sg6maz51zh38493k0q34afmqlfc16w4ik9swgin6ap0lx5bijaz")))

