(define-module (crates-io #{3}# b bob) #:use-module (crates-io))

(define-public crate-bob-0.0.1 (c (n "bob") (v "0.0.1") (d (list (d (n "fancy-regex") (r "^0.3.3") (d #t) (k 0)) (d (n "http-types") (r "^1.0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1nzb55rkrzbj0li9cagl580m3hfw0fgnkm16x5dqqsyrxfsb7cwf")))

