(define-module (crates-io #{3}# b bit) #:use-module (crates-io))

(define-public crate-bit-0.1.0 (c (n "bit") (v "0.1.0") (h "1xp0ij4r19ad591169810x65ad6ix19ka66cgwd9v7k20rp5np3b")))

(define-public crate-bit-0.1.1 (c (n "bit") (v "0.1.1") (h "117jj7213hx8m3yka3402xpsval5ay8imkng95ch7m5715f5qr1b")))

