(define-module (crates-io #{3}# b bgv) #:use-module (crates-io))

(define-public crate-bgv-0.1.0 (c (n "bgv") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)))) (h "15sbigcg1d25mwjxv7nkc4896kjhdklzqpb1d7g8gxdj8c3ng4kp")))

(define-public crate-bgv-0.2.0 (c (n "bgv") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)))) (h "1zf2hsd94byyqfwqphxbsszvxzdrgvz3syis0phip5k15pl61kh4")))

(define-public crate-bgv-0.3.0 (c (n "bgv") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jzywxfjzkj7gmam0a4gfwc73wm00f6s9qwyar24ivs9pcrgif17")))

(define-public crate-bgv-0.4.0 (c (n "bgv") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pg1jimfy1gy0xfa55h1w43zrjg3gqnbdnc2r6n7dqs98hsx31ii")))

(define-public crate-bgv-0.5.0 (c (n "bgv") (v "0.5.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "rug") (r "^1.24.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l5bjz7pam9dpznqzi1af7s3l2yj4ymk8n6svl79x3p4cb30ix10")))

