(define-module (crates-io #{3}# w wkg) #:use-module (crates-io))

(define-public crate-wkg-0.0.0 (c (n "wkg") (v "0.0.0") (h "145hj7s9m9k4ls1qr9dfck7dnm9293dwfn4fpkzylw4b87kjfv1k")))

(define-public crate-wkg-0.2.0 (c (n "wkg") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (f (quote ("io"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "wasm-pkg-loader") (r "^0.2.0") (d #t) (k 0)) (d (n "wit-component") (r "^0.207") (d #t) (k 0)))) (h "0xwhpwqayfmk5l5a52al6axlwfpsmw3j561pkhsb87cv4mj3y5bg")))

(define-public crate-wkg-0.3.0 (c (n "wkg") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.29") (f (quote ("io"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "wasm-pkg-loader") (r "^0.3.0") (d #t) (k 0)) (d (n "wit-component") (r "^0.207") (d #t) (k 0)))) (h "1da08p3pg9kfczrv93rhav6nzfaygzjvk48wfbyz29s3a16npp90")))

