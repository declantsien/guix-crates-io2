(define-module (crates-io #{3}# w wex) #:use-module (crates-io))

(define-public crate-wex-0.1.0 (c (n "wex") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ff0hprgzk7bgf4z5s8snpzcrkqyi8f5mjyh8rbxrfa51n7ingh3")))

(define-public crate-wex-0.1.1 (c (n "wex") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.8") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1fv74y2cfmbywrcgqnkjjibbqjza82gh3gy4ql9vp8q85kdzizpn")))

(define-public crate-wex-0.1.2 (c (n "wex") (v "0.1.2") (d (list (d (n "curl") (r "^0.4.14") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1s1mlr7xfcsv51wv4f32nshzkr53cm3h3k8x5hm6x8m3lv5bmrg6")))

