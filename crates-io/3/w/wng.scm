(define-module (crates-io #{3}# w wng) #:use-module (crates-io))

(define-public crate-wng-3.2.1 (c (n "wng") (v "3.2.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "see_directory") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g7rfahwmni6fqhfdzbldag2hqdcmqz341h19hfc22nh4pwcrlhz")))

(define-public crate-wng-3.2.2 (c (n "wng") (v "3.2.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "see_directory") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1s063gb18qfrk8dxj8q19grrpnyxkarwynq10zpily8wvklxvkhl")))

(define-public crate-wng-3.2.3 (c (n "wng") (v "3.2.3") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "see_directory") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f3fmlgdffwhyklwq7q434m8bknmc65n52w3bbidjrrsdjkvgm7x")))

(define-public crate-wng-3.2.4 (c (n "wng") (v "3.2.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "see_directory") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "136c8vz0xx2gxv4g12yx454rmybld8gl3bav2jxvbsmk94ir6wbx")))

(define-public crate-wng-3.3.4 (c (n "wng") (v "3.3.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "see_directory") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qpmfb5ba2acbpvjy8ycsn8wln12z11n688yhy2pvg5xw6h5jix0")))

(define-public crate-wng-3.3.6 (c (n "wng") (v "3.3.6") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "see_directory") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "13ybpz41nqdvnxw57dc1jbrwjm6ghh27m9nnfmbxi41jvmfwhhib")))

(define-public crate-wng-3.4.0 (c (n "wng") (v "3.4.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "see_directory") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "0qz1zc6z5v6czpgn32sih7h7fgkgklny639r39j1rap0mn153h8r")))

(define-public crate-wng-3.4.1 (c (n "wng") (v "3.4.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "1qgfgrf967k7qfbp3bx2b0rznsda4nykkz2a7m6v7b8hjsxb7jph")))

(define-public crate-wng-3.4.2 (c (n "wng") (v "3.4.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "1dg4ilq1bl0bm7f4k9z8bhspwk2w553fz7hynnlz4dvgilqd59a2")))

(define-public crate-wng-3.5.2 (c (n "wng") (v "3.5.2") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "0i6hiqhmi07yhxk9rarrkjv7ymwphddmfd7sbb9lfdaqnwdlpn7l") (y #t)))

(define-public crate-wng-3.5.0 (c (n "wng") (v "3.5.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "lines_from_file") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.30") (d #t) (k 0)))) (h "0n25mg7kdffkxxriir9wjwzsgxjw1wpw003h8syral0ps84q33xj")))

(define-public crate-wng-4.0.0 (c (n "wng") (v "4.0.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.0") (d #t) (k 0)) (d (n "wng-lib") (r "^0.1.0") (d #t) (k 0)))) (h "19w248l1n5v1cqr6jfs2wl7zvl0h00bj0ppfjm4yz5cg3112jayb")))

(define-public crate-wng-4.0.1 (c (n "wng") (v "4.0.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.0") (d #t) (k 0)) (d (n "wng-lib") (r "^0.1.1") (d #t) (k 0)))) (h "0hrvjacm9zk07ncq3637826pb1p5cnc56in38ig6v3ialn9wc0q2")))

(define-public crate-wng-4.0.2 (c (n "wng") (v "4.0.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "is_executable") (r "^1.0.0") (d #t) (k 0)) (d (n "wng-lib") (r "^0.1.3") (d #t) (k 0)))) (h "1zh4y26l9xpyxx0g3dz922rillkmks24bvjby2ksf7mj0qymjl4j")))

