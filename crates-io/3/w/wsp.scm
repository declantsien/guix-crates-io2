(define-module (crates-io #{3}# w wsp) #:use-module (crates-io))

(define-public crate-wsp-0.1.0 (c (n "wsp") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0r8lyjdiq00mhrsfxyjq0spd5md75v8y6lgws4rjvy1q6vws9hbm")))

(define-public crate-wsp-0.1.1 (c (n "wsp") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1168vizh9bik3j3pi1rhqz00r0sl0szmxvw1727ap617c1l7ckj6")))

(define-public crate-wsp-0.1.2 (c (n "wsp") (v "0.1.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0ikfkw11n93ic5qxizdrx4s87qp5ggpy09h3bc4a8h0a8jj21zjp")))

(define-public crate-wsp-0.1.3 (c (n "wsp") (v "0.1.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0xnwa4mnzj03c8zb4l20ql0crm47v5vf1754yy6pxsspgn5w9hkx")))

(define-public crate-wsp-0.1.4 (c (n "wsp") (v "0.1.4") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "119d28zifalyl0nlyyillv5r9xklhhf98hbmrh3rqidc1f8syhih")))

(define-public crate-wsp-0.1.5 (c (n "wsp") (v "0.1.5") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1mrvndsi61w278vggj1hfydsdqd84vb48y7bgwl5cjib9z83szdy")))

(define-public crate-wsp-0.1.6 (c (n "wsp") (v "0.1.6") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1lrw0di52wial6p9c56z5mb3zi81as0fg1z2n4x5lrc1a0lchigb")))

