(define-module (crates-io #{3}# w wql) #:use-module (crates-io))

(define-public crate-wql-0.1.0 (c (n "wql") (v "0.1.0") (d (list (d (n "bcrypt") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.121") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0518668i34axkbgip3l4k280d5dfs31i8rjmkxd7p6nvdrixb4vs")))

(define-public crate-wql-0.1.3 (c (n "wql") (v "0.1.3") (d (list (d (n "bcrypt") (r "^0.8") (d #t) (k 0)) (d (n "edn-rs") (r "^0.16.12") (d #t) (k 2)) (d (n "serde") (r "^1.0.121") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0mq0xif0qf1ym9q6j165pqjrlcav6ifba6jdnad9afgqrl82fp6m")))

