(define-module (crates-io #{3}# w wrc) #:use-module (crates-io))

(define-public crate-wrc-0.1.0 (c (n "wrc") (v "0.1.0") (h "0pgw8ii8npm4r25wxismg9k56m77iz8w8lk436rr6nwmxvg7yfmq")))

(define-public crate-wrc-0.2.0 (c (n "wrc") (v "0.2.0") (h "0fkqln2f7gww4f6ibfj5c17q06sg1i9k9kmmazymj87a7cvq6i92")))

(define-public crate-wrc-0.2.1 (c (n "wrc") (v "0.2.1") (h "1m3mp9issqa5mnjd3g2kgwdy6kpw1qc45cxm468wpcspqzmbzfpj")))

(define-public crate-wrc-0.2.2 (c (n "wrc") (v "0.2.2") (h "0jbzdhif5q6nq3gj4bsnxdalj6nc7dxkyz7wma43hizshxp78smz")))

(define-public crate-wrc-0.2.3 (c (n "wrc") (v "0.2.3") (h "0drh2pbnh0wj5pjx6mf2i82hw991kmai87ykc63bg1j3f6678lj8")))

(define-public crate-wrc-0.3.0 (c (n "wrc") (v "0.3.0") (h "0g26qjpaxna93b24pi83zl94p865jc4k3qpzpimi04largbhfsfi")))

(define-public crate-wrc-0.4.1 (c (n "wrc") (v "0.4.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0jnplqi9y1a1h58fspzjv7502v4a0kkf0w830jvg4vbh61skgn7s")))

(define-public crate-wrc-0.4.2 (c (n "wrc") (v "0.4.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "1v24hqr6sy41bk7vvlmls03ig7c2zg68ppg4gpbqla4bw33q6fkw")))

(define-public crate-wrc-1.0.0 (c (n "wrc") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)))) (h "0lrvg87pd2f5s6davh5addsf0cngvzzcih8zpkmx6g3vxf38fbf1")))

(define-public crate-wrc-2.0.0 (c (n "wrc") (v "2.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0avhij52q067kcg02yzsbslqxiqpdj6wdssyld3h0nw3zi0rm52i")))

