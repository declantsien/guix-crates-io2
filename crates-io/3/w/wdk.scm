(define-module (crates-io #{3}# w wdk) #:use-module (crates-io))

(define-public crate-wdk-0.0.0 (c (n "wdk") (v "0.0.0") (h "0ng6n2ijvijnl9vw6kmim948zfg6rcdi35a0a7flj88a2vnnjbm9")))

(define-public crate-wdk-0.1.0 (c (n "wdk") (v "0.1.0") (d (list (d (n "wdk-build") (r "^0.1.0") (d #t) (k 1)) (d (n "wdk-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "wdk-sys") (r "^0.1.0") (f (quote ("test-stubs"))) (d #t) (k 2)))) (h "1g6jddz9fi3rlqj30byfjpqfbcr6hwfggcdz3di27agl6rzggz7v") (f (quote (("nightly" "wdk-sys/nightly") ("default" "alloc") ("alloc")))) (l "wdk-sys")))

(define-public crate-wdk-0.2.0 (c (n "wdk") (v "0.2.0") (d (list (d (n "wdk-build") (r "^0.2.0") (d #t) (k 1)) (d (n "wdk-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "wdk-sys") (r "^0.2.0") (f (quote ("test-stubs"))) (d #t) (k 2)))) (h "17rf2927l2axxd23yv663yjpmdy98w9q6wlckdgmqfdpkjh5y9ay") (f (quote (("nightly" "wdk-sys/nightly") ("default" "alloc") ("alloc")))) (l "wdk-sys")))

