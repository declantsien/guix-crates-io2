(define-module (crates-io #{3}# w wuf) #:use-module (crates-io))

(define-public crate-wuf-0.1.0 (c (n "wuf") (v "0.1.0") (h "1jyp1zvr96ihd2k44f4lkfml9hqq2w952lqb5d005lxgpw0m42gv")))

(define-public crate-wuf-0.1.1 (c (n "wuf") (v "0.1.1") (h "0z3y1favdqlxgacvvydi92nvj4f9g46l0lmcd4cp7x3ms9i7x2hn")))

(define-public crate-wuf-0.1.2 (c (n "wuf") (v "0.1.2") (h "02f0l4aklks70qwg7kichggqv7hls6b1bxr8r1p4dh888js8bajh")))

