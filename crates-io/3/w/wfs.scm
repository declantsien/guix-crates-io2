(define-module (crates-io #{3}# w wfs) #:use-module (crates-io))

(define-public crate-wfs-0.0.1 (c (n "wfs") (v "0.0.1") (d (list (d (n "thrift") (r "^0.17.0") (d #t) (k 0)))) (h "0vqkmhpwqamsw4d768xgg20p58q1jysz8hy4as8r7cz0wc95164h")))

(define-public crate-wfs-0.0.2 (c (n "wfs") (v "0.0.2") (d (list (d (n "native-tls") (r "^0.2.11") (d #t) (k 0)) (d (n "thrift") (r "^0.17.0") (d #t) (k 0)))) (h "09mhyvybgygyilsaca595777xjhl73rqyad7pil3dk72iz8v3v2l")))

