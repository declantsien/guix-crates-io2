(define-module (crates-io #{3}# w woe) #:use-module (crates-io))

(define-public crate-woe-0.1.0 (c (n "woe") (v "0.1.0") (h "0fdb2j815bvlkxxnx7yqy5khs63nwg4b75xramlzpk7qabxppnis") (y #t)))

(define-public crate-woe-0.1.1 (c (n "woe") (v "0.1.1") (h "1hw3r11xakmg7das5sm044ckz1gk74wlk2vigy5fw8pvrxqlyjwr") (y #t)))

(define-public crate-woe-0.1.2 (c (n "woe") (v "0.1.2") (h "1jizsh6rh7d4njbwwmryfdypv9skk6myd6ssfnq3drqyjqq6qawr") (y #t)))

(define-public crate-woe-0.1.3 (c (n "woe") (v "0.1.3") (h "1bz5mvvh4wg9dbxf7vghjwivl5wpkagyicdm95fm5y2npza3lrh2") (y #t)))

(define-public crate-woe-0.1.4 (c (n "woe") (v "0.1.4") (h "0sxph2bykkal4kl9v1m7vv6f9rhycwxm0b47gfzxacci2kizagaz") (y #t)))

(define-public crate-woe-0.1.5 (c (n "woe") (v "0.1.5") (h "0mkvhjk51a1a9k028xa0warpcgs64sqf4wd5d0m0cb9n10r41f9w") (y #t)))

(define-public crate-woe-0.1.6 (c (n "woe") (v "0.1.6") (d (list (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)))) (h "127g30sqkayg00as6iwpj4sib6al4s2jcp7w41dgh9m5fld05x3w") (f (quote (("either-methods" "either") ("default" "either-methods")))) (y #t)))

(define-public crate-woe-0.1.7 (c (n "woe") (v "0.1.7") (d (list (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)))) (h "10lb68wrf0s0sdas79jlcy47a3w91cgnd4jl682fan83fcsxlldm") (f (quote (("either-methods" "either") ("default" "either-methods")))) (y #t)))

(define-public crate-woe-0.1.8 (c (n "woe") (v "0.1.8") (d (list (d (n "either") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0k6vmqxcdnb0rvf414ni8haahm2cxn5d28xh0fz1k8lvr4f2blix") (f (quote (("try_trait") ("trusted_len") ("termination_trait" "never_type") ("sum_trait" "try_trait") ("product_trait" "try_trait") ("no_std") ("nightly" "try_trait" "trusted_len" "never_type" "termination_trait" "product_trait" "sum_trait" "from_iterator_trait") ("never_type") ("from_iterator_trait" "try_trait") ("either_methods" "either") ("default" "either_methods")))) (y #t)))

