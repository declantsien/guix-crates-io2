(define-module (crates-io #{3}# w wkr) #:use-module (crates-io))

(define-public crate-wkr-0.0.0 (c (n "wkr") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.63") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasi") (r "^0.11.0") (d #t) (k 0)))) (h "01a10py70dr2frzv4036f730zq589vzq4qvay1jb0lvvnrs35zid")))

