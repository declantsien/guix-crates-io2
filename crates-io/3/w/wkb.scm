(define-module (crates-io #{3}# w wkb) #:use-module (crates-io))

(define-public crate-wkb-0.1.0 (c (n "wkb") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "geo") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1dmz8d33p47wilss0syvqfg0gwdzqng7901dza9kngcx72iyrwcn")))

(define-public crate-wkb-0.2.0 (c (n "wkb") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "geo") (r "^0.7.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0pcb6969ncrn8yr3zwpk5x911qw27kxgv96rfjlvn2b198kj53hi")))

(define-public crate-wkb-0.3.0 (c (n "wkb") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "geo") (r "^0.10.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1c3qgz9gfwd8jbl3r8khqrq80hzbwmxh298dx8575s0qr6xqnfy0")))

(define-public crate-wkb-0.4.0 (c (n "wkb") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "geo-types") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "12vwvvl4vh2pal1ajbs695nbhrkv6j210sc1wd07r25rqlb6vw70")))

(define-public crate-wkb-0.5.0 (c (n "wkb") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "geo-types") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "1gny861qsaa06ifnwknlac3gwy3mng7mcfvr6qkzahicd4k5k6nj")))

(define-public crate-wkb-0.6.0 (c (n "wkb") (v "0.6.0") (d (list (d (n "geo-types") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0rwq47yiqigdza1xplxn42yndbw7h2h0r1w8hymrpd09b6labvhp")))

(define-public crate-wkb-0.7.0 (c (n "wkb") (v "0.7.0") (d (list (d (n "geo-types") (r "^0.6") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0xp492n9lx21v7ikbzgndq9hjk2f9mdvgdkmr96lizhymv4glnbp")))

(define-public crate-wkb-0.7.1 (c (n "wkb") (v "0.7.1") (d (list (d (n "geo-types") (r "^0.7") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "18h4j3ywp3qrj5a581d0dsgl4lga1xibzfp8n1i7lq21r3d0dj36")))

