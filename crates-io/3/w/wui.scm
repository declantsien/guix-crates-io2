(define-module (crates-io #{3}# w wui) #:use-module (crates-io))

(define-public crate-wui-0.0.0 (c (n "wui") (v "0.0.0") (h "15jh9zirj3pmp4s79kx9mx4y85q9br9cssxg168cnbp781k3nr24")))

(define-public crate-wui-0.0.1 (c (n "wui") (v "0.0.1") (d (list (d (n "js_ffi") (r "^0.0.13") (d #t) (k 0)) (d (n "view") (r "^0.1.3") (d #t) (k 0)))) (h "1fnlagdxiv14rnilmnm5xnaf94clk7mn38vkmmnpg6n9d8jxjmjf")))

