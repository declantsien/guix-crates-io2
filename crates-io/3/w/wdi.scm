(define-module (crates-io #{3}# w wdi) #:use-module (crates-io))

(define-public crate-wdi-0.1.0 (c (n "wdi") (v "0.1.0") (d (list (d (n "bstr") (r "^1.6.0") (d #t) (k 0)) (d (n "libwdi-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("setupapi"))) (d #t) (k 0)))) (h "1srwp16w3c5yh6r1r3gagy26r13af4qs2mga6calgjz7npczm98w")))

