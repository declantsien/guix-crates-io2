(define-module (crates-io #{3}# w wns) #:use-module (crates-io))

(define-public crate-wns-1.0.0 (c (n "wns") (v "1.0.0") (h "0wycb9dpicq2v1zmbiyckzwy2rbfdhmnk1zsch1yfrr57xwcvimy")))

(define-public crate-wns-1.0.1 (c (n "wns") (v "1.0.1") (h "09r2d3nhnca15zsjwrr3a5a0vpjjrv3r3c8lh8hz10krjd80bvd1")))

