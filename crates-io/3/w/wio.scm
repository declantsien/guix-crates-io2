(define-module (crates-io #{3}# w wio) #:use-module (crates-io))

(define-public crate-wio-0.0.1 (c (n "wio") (v "0.0.1") (d (list (d (n "kernel32-sys") (r "*") (d #t) (k 0)) (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0paq8jqc9yb4niyw265cq8ynz2ym0byl0hjsw83dhxrsmnn8757c")))

(define-public crate-wio-0.1.0 (c (n "wio") (v "0.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "1n3mxllxrp12l68crci7s2j3l2nxvzdndqm9s8pksds58ysffhvg")))

(define-public crate-wio-0.1.1 (c (n "wio") (v "0.1.1") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 2)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "1ngixpm7y33a3p3jq85h5230cxilayzdj6g0f8dd5fzii642qi9v")))

(define-public crate-wio-0.1.2 (c (n "wio") (v "0.1.2") (d (list (d (n "advapi32-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.13") (d #t) (k 2)) (d (n "user32-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2.5") (d #t) (k 0)))) (h "0ggwavhbiqgpbdxqz7awj7jm22ifs9b7nbrz3xvrkwypq114wnmf")))

(define-public crate-wio-0.2.0 (c (n "wio") (v "0.2.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "minwindef" "unknwnbase"))) (d #t) (k 0)))) (h "04xjmlfvxd3j1907v8km7syr4iiyr6grimzqnzzl2scd4s51x8zq")))

(define-public crate-wio-0.2.1 (c (n "wio") (v "0.2.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "errhandlingapi" "fileapi" "handleapi" "minwindef" "processthreadsapi" "std" "unknwnbase" "wincon" "winnt"))) (d #t) (k 0)))) (h "0fdcbxciqsxjah0qczn25m2g0kvrkhn0ssc8xy529bj62g413l56")))

(define-public crate-wio-0.2.2 (c (n "wio") (v "0.2.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "errhandlingapi" "fileapi" "handleapi" "minwindef" "processthreadsapi" "std" "unknwnbase" "wincon" "winnt"))) (d #t) (k 0)))) (h "199p404fp96w1f1c93bf1jrvaqwypxf3hmmldhww4jk4yhr9j4jx")))

