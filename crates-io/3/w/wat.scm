(define-module (crates-io #{3}# w wat) #:use-module (crates-io))

(define-public crate-wat-0.1.0 (c (n "wat") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.10") (k 0)))) (h "03slw7d2wch7r3mqwsrwj2biwqcz8wd8sbia11j2v5ximfghhhnh") (y #t)))

(define-public crate-wat-1.0.2 (c (n "wat") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)) (d (n "wast") (r "^2.0.0") (d #t) (k 0)))) (h "1k4d403s58qsvmlvisq8m39s38lz94xnw6xbwh22f0rm31hmjnnj")))

(define-public crate-wat-1.0.3 (c (n "wat") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)) (d (n "wast") (r "^3.0.0") (d #t) (k 0)))) (h "0yqkikh64sds12jqvfpd8viky37n5ghs094wy6q3vdmsap8i7nw0")))

(define-public crate-wat-1.0.4 (c (n "wat") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)) (d (n "wast") (r "^3.0.3") (d #t) (k 0)))) (h "17adpm1l9s2mw8r4sqp36p1ifmr6h5p0p0z4s1lyl29jjsssglsm")))

(define-public crate-wat-1.0.5 (c (n "wat") (v "1.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.44") (d #t) (k 2)) (d (n "wast") (r "^4.0.0") (d #t) (k 0)))) (h "1jbdh9400xqkc6jjdi7lk24pi9jw1vhi5rad768r5xnrm2pqdfkq")))

(define-public crate-wat-1.0.6 (c (n "wat") (v "1.0.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.45") (d #t) (k 2)) (d (n "wast") (r "^5.0.0") (d #t) (k 0)))) (h "14pi548rd61xmgqv75pnyxkr2y3bqmp8vbw3fv5c36mpn5hcq5nr")))

(define-public crate-wat-1.0.7 (c (n "wat") (v "1.0.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.45") (d #t) (k 2)) (d (n "wast") (r "^6.0.0") (d #t) (k 0)))) (h "00rdwqz1mml6j161pc8acj22sif6ijqx12agbvahfmag49dvlncx")))

(define-public crate-wat-1.0.8 (c (n "wat") (v "1.0.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.48") (d #t) (k 2)) (d (n "wast") (r "^7.0.0") (d #t) (k 0)))) (h "1kp8v61sqyln936bw6nfw2c17hw5mi7n8zn9vr9kd29r9d5f75ap")))

(define-public crate-wat-1.0.9 (c (n "wat") (v "1.0.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.51") (d #t) (k 2)) (d (n "wast") (r "^8.0.0") (d #t) (k 0)))) (h "1fycpp96rfy10hc3kcw4v0vcjbzpyzp8j9p8jxywjafwp8spp4ja")))

(define-public crate-wat-1.0.10 (c (n "wat") (v "1.0.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.51") (d #t) (k 2)) (d (n "wast") (r "^9.0.0") (d #t) (k 0)))) (h "1xnhpsrvccbr94nkcmpv9cacgip18lw44wbybbiyp6mm9xzky5sn")))

(define-public crate-wat-1.0.11 (c (n "wat") (v "1.0.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.51") (d #t) (k 2)) (d (n "wast") (r "^10.0.0") (d #t) (k 0)))) (h "17065f66krpyfm20n35mfmc655krgm61lfmfydi3mk3kabiabppz")))

(define-public crate-wat-1.0.12 (c (n "wat") (v "1.0.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.51") (d #t) (k 2)) (d (n "wast") (r "^11.0.0") (d #t) (k 0)))) (h "00fk3rd1clg3wclzdb78wwk6wkkc1adinjwp5mxhh4l53kf0154s")))

(define-public crate-wat-1.0.13 (c (n "wat") (v "1.0.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.51") (d #t) (k 2)) (d (n "wast") (r "^12.0.0") (d #t) (k 0)))) (h "1g889laixgqjbbs440ac4y0iqmkh5zqdk9yflkirbrnapxgn2pq9")))

(define-public crate-wat-1.0.14 (c (n "wat") (v "1.0.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.51") (d #t) (k 2)) (d (n "wast") (r "^13.0.0") (d #t) (k 0)))) (h "1i0jl65q5jqmy99dysdfvqag3hma17n1yi2wa05j1rz31s1ib9ji")))

(define-public crate-wat-1.0.15 (c (n "wat") (v "1.0.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.51") (d #t) (k 2)) (d (n "wast") (r "^14.0.0") (d #t) (k 0)))) (h "0pc3lblcx88m6k5avvlzad2acqq7q3ffzyvf56f8bwyg6fy1inq3")))

(define-public crate-wat-1.0.16 (c (n "wat") (v "1.0.16") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.52") (d #t) (k 2)) (d (n "wast") (r "^15.0.0") (d #t) (k 0)))) (h "1i2nvb8ifvrhn4zg391s3ddxkn6qmyc5p4nljad9yz84dkgjhvaj")))

(define-public crate-wat-1.0.17 (c (n "wat") (v "1.0.17") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.52") (d #t) (k 2)) (d (n "wast") (r "^16.0.0") (d #t) (k 0)))) (h "08j2z2hy44087vzxb2sy4r0lnn9lmd4jwpkrkl45l38scv366950")))

(define-public crate-wat-1.0.18 (c (n "wat") (v "1.0.18") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.54") (d #t) (k 2)) (d (n "wast") (r "^17.0.0") (d #t) (k 0)))) (h "1givq4g8yxxmn2nqq7gc5pgm3dxvyjlrfsdfhzcny7n8wpjzjl1b")))

(define-public crate-wat-1.0.19 (c (n "wat") (v "1.0.19") (d (list (d (n "wast") (r "^18.0.0") (d #t) (k 0)))) (h "05c5m6v9cyga8rci6yaahx8w37a5fdvdpspyxcm1g4wjslcd81j0")))

(define-public crate-wat-1.0.20 (c (n "wat") (v "1.0.20") (d (list (d (n "wast") (r "^18.0.0") (d #t) (k 0)))) (h "1afvbflqhm04hkx5l3c6914mhaxjqv2da0ns4xn0bxxgs9gbpkdn")))

(define-public crate-wat-1.0.21 (c (n "wat") (v "1.0.21") (d (list (d (n "wast") (r "^20.0.0") (d #t) (k 0)))) (h "1h59lbrk3n6hj5xh618v2xf5x6f62jjigf5jgfwjyngvvlariyls")))

(define-public crate-wat-1.0.22 (c (n "wat") (v "1.0.22") (d (list (d (n "wast") (r "^21.0.0") (d #t) (k 0)))) (h "1371l8wpwmxm87a2s5y34jrpnbb50avcz4ilkq738b14fhmxg1ff")))

(define-public crate-wat-1.0.23 (c (n "wat") (v "1.0.23") (d (list (d (n "wast") (r "^22.0.0") (d #t) (k 0)))) (h "1f10lnjlq00rscl4z7vlgh4n8nw335846appb6w3jz2bka6ib27q")))

(define-public crate-wat-1.0.24 (c (n "wat") (v "1.0.24") (d (list (d (n "wast") (r "^23.0.0") (d #t) (k 0)))) (h "1k4fw6pi0ia2a8ydj86ykjmjzh2rgnspbpwcsa468j5a65s0sdbc")))

(define-public crate-wat-1.0.25 (c (n "wat") (v "1.0.25") (d (list (d (n "wast") (r "^24.0.0") (d #t) (k 0)))) (h "1h4y7q1ynlcxqc58pkhz0jk1vphwlzqipzy6rsc3yidff8lbph67")))

(define-public crate-wat-1.0.26 (c (n "wat") (v "1.0.26") (d (list (d (n "wast") (r "^25.0.0") (d #t) (k 0)))) (h "0gpy9vpzd9lg2dw4877axz2l24drkr136865jag2f8wy4ikd8rj7")))

(define-public crate-wat-1.0.27 (c (n "wat") (v "1.0.27") (d (list (d (n "wast") (r "^26.0.0") (d #t) (k 0)))) (h "0wadj6nqvf3r6i1ddl6x31xjcs93188nx14ia23i83jdrv5xrci6")))

(define-public crate-wat-1.0.28 (c (n "wat") (v "1.0.28") (d (list (d (n "wast") (r "^27.0.0") (d #t) (k 0)))) (h "0ibhjcpy50snkzndhi445bz0cl7k11z1al0gqaxy4rvzj2fgap43")))

(define-public crate-wat-1.0.29 (c (n "wat") (v "1.0.29") (d (list (d (n "wast") (r "^28.0.0") (d #t) (k 0)))) (h "1gy9j2p1453ki7yxy9vqwnap6nc80vxamawrzab977agxjsmavf0")))

(define-public crate-wat-1.0.30 (c (n "wat") (v "1.0.30") (d (list (d (n "wast") (r "^29.0.0") (d #t) (k 0)))) (h "0s1c656qcyv10mfpaqivhw96wkizhli8zl8qs9r82a9vjn6sh48d")))

(define-public crate-wat-1.0.31 (c (n "wat") (v "1.0.31") (d (list (d (n "wast") (r "^30.0.0") (d #t) (k 0)))) (h "0i6a43n0j6n12xlh6znr5xv1i6fgdsy3qg1bxmhjxwavhc19l9x8")))

(define-public crate-wat-1.0.32 (c (n "wat") (v "1.0.32") (d (list (d (n "wast") (r "^31.0.0") (d #t) (k 0)))) (h "13mnh37dl210fa16hjs5xqprnxansgx5m59dh95bilvkv92302ra")))

(define-public crate-wat-1.0.33 (c (n "wat") (v "1.0.33") (d (list (d (n "wast") (r "^32.0.0") (d #t) (k 0)))) (h "1kkwmlxrrw1dz191lkmfz98g5ha1g6zkla4pi27339izfws7z3sy")))

(define-public crate-wat-1.0.34 (c (n "wat") (v "1.0.34") (d (list (d (n "wast") (r "^33.0.0") (d #t) (k 0)))) (h "1zjwxddcwvh7jq76xh79ps60nzm9qwifvpywv9ma41xf07pcdjby")))

(define-public crate-wat-1.0.35 (c (n "wat") (v "1.0.35") (d (list (d (n "wast") (r "^34.0.0") (d #t) (k 0)))) (h "1dn4fddgxpgyc2wmldid3h28s2lgi3fk30db408v544sbcrh6i27")))

(define-public crate-wat-1.0.36 (c (n "wat") (v "1.0.36") (d (list (d (n "wast") (r "^35.0.0") (d #t) (k 0)))) (h "1k6vvb0sqpamfp4qjzr7p3g00r08cyfl4azh57hsnp9c09cs03qb")))

(define-public crate-wat-1.0.37 (c (n "wat") (v "1.0.37") (d (list (d (n "wast") (r "^35.0.0") (d #t) (k 0)))) (h "00iasw5mlf75q6dyv4p9limq75kra1c1cb6izzh774dn76kq1hlf")))

(define-public crate-wat-1.0.38 (c (n "wat") (v "1.0.38") (d (list (d (n "wast") (r "^36.0.0") (d #t) (k 0)))) (h "1ri37aik1kqx2j26akyf4wvalp49sr72k9nxqa219473y3vksf0n")))

(define-public crate-wat-1.0.39 (c (n "wat") (v "1.0.39") (d (list (d (n "wast") (r "^37.0.0") (d #t) (k 0)))) (h "0l1zr1jab5xmz0jndmf9iyx7m4mv0sxlk48xlhlaplb9ka6wrcia")))

(define-public crate-wat-1.0.40 (c (n "wat") (v "1.0.40") (d (list (d (n "wast") (r "^38.0.0") (d #t) (k 0)))) (h "0iphpm3s2nzxn4szybm1ldxnsph59agn0i8s4z3d4y15gsraxkxd")))

(define-public crate-wat-1.0.41 (c (n "wat") (v "1.0.41") (d (list (d (n "wast") (r "^39.0.0") (d #t) (k 0)))) (h "0c0qwqzxvwil8shw7da0c0ryb4iygkr1g1km52gwd5sg94jyv65b")))

(define-public crate-wat-1.0.42 (c (n "wat") (v "1.0.42") (d (list (d (n "wast") (r "^40.0.0") (d #t) (k 0)))) (h "0vn715k76fmvy5cwi7xga2cs4vmnrjbrbckmk9i93nz0bhwvc084")))

(define-public crate-wat-1.0.43 (c (n "wat") (v "1.0.43") (d (list (d (n "wast") (r "^41.0.0") (d #t) (k 0)))) (h "03zq5x4dcziikqv967j91wk1dlj48kkqbrwb7zywfrlywfrvkcs8")))

(define-public crate-wat-1.0.44 (c (n "wat") (v "1.0.44") (d (list (d (n "wast") (r "^42.0.0") (d #t) (k 0)))) (h "1kv5r5qvmynfhqjz8zmhr9l1nrjjfdiw0js1r1k70lmc8avj0bxr")))

(define-public crate-wat-1.0.45 (c (n "wat") (v "1.0.45") (d (list (d (n "wast") (r "^43.0.0") (d #t) (k 0)))) (h "0fwqhhl4xd2wadchq0xrnjl2sfh2s2y6s6a1sv4kvwzs1kzvyw1b")))

(define-public crate-wat-1.0.46 (c (n "wat") (v "1.0.46") (d (list (d (n "wast") (r "^44.0.0") (d #t) (k 0)))) (h "1lafir5pgh1py67wpc2gwyphr3adkjg7289cvz3301ya5v705l42")))

(define-public crate-wat-1.0.47 (c (n "wat") (v "1.0.47") (d (list (d (n "wast") (r "^45.0.0") (d #t) (k 0)))) (h "0mimh519m3wm2klnrh34y8869wjymhyhbawc5i42mw5l4i3vrm62")))

(define-public crate-wat-1.0.48 (c (n "wat") (v "1.0.48") (d (list (d (n "wast") (r "^46.0.0") (d #t) (k 0)))) (h "0ghy5f25hm5x6a6vzkbic6jgmrmzaz6nw3fnjkyvzmglvs154xwg")))

(define-public crate-wat-1.0.49 (c (n "wat") (v "1.0.49") (d (list (d (n "wast") (r "^47.0.0") (d #t) (k 0)))) (h "1628l6z569ndn78fd8qap9300lprzzhcm9khjsxgna84qqh4xavs")))

(define-public crate-wat-1.0.50 (c (n "wat") (v "1.0.50") (d (list (d (n "wast") (r "^48.0.0") (d #t) (k 0)))) (h "1gp0d1q8q8cama82yvfqw1drsy2fh90zd21cyhasiln67sha978j")))

(define-public crate-wat-1.0.51 (c (n "wat") (v "1.0.51") (d (list (d (n "wast") (r "^49.0.0") (d #t) (k 0)))) (h "03cc5kg9infb1h7bpcz4p50z5228kv1dik5gbbli3czzc127qd2c")))

(define-public crate-wat-1.0.52 (c (n "wat") (v "1.0.52") (d (list (d (n "wast") (r "^50.0.0") (d #t) (k 0)))) (h "059gcgs4ipjwyrzp15k7rimgk3xv1jmyw98spqxkhkfg3rxayjjq")))

(define-public crate-wat-1.0.53 (c (n "wat") (v "1.0.53") (d (list (d (n "wast") (r "^51.0.0") (d #t) (k 0)))) (h "1pnckx7n3sadb1gfp0rr9b65skwj0c13qwagkcyp9s6pd08qrlax")))

(define-public crate-wat-1.0.54 (c (n "wat") (v "1.0.54") (d (list (d (n "wast") (r "^52.0.0") (d #t) (k 0)))) (h "01722isln5qd7lgzyc52brb7d4iic0ig3i5b8szg5zihwlcr0cbi") (y #t)))

(define-public crate-wat-1.0.55 (c (n "wat") (v "1.0.55") (d (list (d (n "wast") (r "^52.0.1") (d #t) (k 0)))) (h "08xv4wp2wa4j5yrcnz7ni6pf5i519hr6zznbkvk7b6lf5p4fg4rl")))

(define-public crate-wat-1.0.56 (c (n "wat") (v "1.0.56") (d (list (d (n "wast") (r "^52.0.2") (d #t) (k 0)))) (h "1gn2mgdc8rc7mdb2yp14i6fdjwf90n92iqh36f52zk0sm2x3rmwi")))

(define-public crate-wat-1.0.57 (c (n "wat") (v "1.0.57") (d (list (d (n "wast") (r "^52.0.3") (d #t) (k 0)))) (h "1lvy29726pi3wkciw8si0ignfnsvb8g7yw3fzzvg1rnbsh02289p")))

(define-public crate-wat-1.0.58 (c (n "wat") (v "1.0.58") (d (list (d (n "wast") (r "^53.0.0") (d #t) (k 0)))) (h "0jxa3370vp5hg57x6pii5qz0s2jaigcdbfgc3m8ssvfxk82z2826")))

(define-public crate-wat-1.0.59 (c (n "wat") (v "1.0.59") (d (list (d (n "wast") (r "^54.0.0") (d #t) (k 0)))) (h "115ch97d9qkh88fq11g8f10sbss9glvnxhvqh5ad15kn2xypr6iy")))

(define-public crate-wat-1.0.60 (c (n "wat") (v "1.0.60") (d (list (d (n "wast") (r "^54.0.1") (d #t) (k 0)))) (h "184i6zrf8ydqgxkcii47myxyzxisxi5ijf8lfr1i58sys0z2xnyi")))

(define-public crate-wat-1.0.61 (c (n "wat") (v "1.0.61") (d (list (d (n "wast") (r "^55.0.0") (d #t) (k 0)))) (h "1490vys3f020nxrdjpdxfp2j0rnzmcbwdsbh5v9hbnqlvbs56axg")))

(define-public crate-wat-1.0.62 (c (n "wat") (v "1.0.62") (d (list (d (n "wast") (r "^56.0.0") (d #t) (k 0)))) (h "168jb1rvfsyl1wln85g401w5l2sh1pvm1dvbv63an5l2h0i1js2n")))

(define-public crate-wat-1.0.63 (c (n "wat") (v "1.0.63") (d (list (d (n "wast") (r "^57.0.0") (d #t) (k 0)))) (h "0lrfz7ndk7nm1n5091gfnjydd7xsqhqx6p7wnqmvxhrpfgcb16mb")))

(define-public crate-wat-1.0.64 (c (n "wat") (v "1.0.64") (d (list (d (n "wast") (r "^58.0.0") (d #t) (k 0)))) (h "0ws1px3hg7sfy21lc4z5fjfqb79igsnv6h5bg4asn4p1j1hl8ivd")))

(define-public crate-wat-1.0.65 (c (n "wat") (v "1.0.65") (d (list (d (n "wast") (r "^59.0.0") (d #t) (k 0)))) (h "1w81synsawww1mjpn65zvrivyvg0z7yjpycvdr6sj5q4pqjs0dn9")))

(define-public crate-wat-1.0.66 (c (n "wat") (v "1.0.66") (d (list (d (n "wast") (r "^60.0.0") (d #t) (k 0)))) (h "0fzavidmfsqvhzng4n1b5bil9pkizjrfdly7k9p3c1dj187m5gjs")))

(define-public crate-wat-1.0.67 (c (n "wat") (v "1.0.67") (d (list (d (n "wast") (r "^61.0.0") (d #t) (k 0)))) (h "149lx859g6lqi4qwxig7lvg7czjw0b61gmpbl7mppay34x6pd7j5")))

(define-public crate-wat-1.0.68 (c (n "wat") (v "1.0.68") (d (list (d (n "wast") (r "^62.0.0") (d #t) (k 0)))) (h "020wrj2fv6zmgfy29gm9v2m4ls3ficzav0qsjxd6hnxa4jzp4m99")))

(define-public crate-wat-1.0.69 (c (n "wat") (v "1.0.69") (d (list (d (n "wast") (r "^62.0.1") (d #t) (k 0)))) (h "0dz68igazwisjp9833ayngwgdw7arm8hfjridnlv8gr03n31abl4")))

(define-public crate-wat-1.0.70 (c (n "wat") (v "1.0.70") (d (list (d (n "wast") (r "^63.0.0") (d #t) (k 0)))) (h "0v21grp66fdiknhm18y6gx1pfahd3mrq7q39pbr2nbsc5in31p1v")))

(define-public crate-wat-1.0.71 (c (n "wat") (v "1.0.71") (d (list (d (n "wast") (r "^64.0.0") (d #t) (k 0)))) (h "1xn3asszjfrlshfsa2qxcgglz6y707b628fwqyhzq4xl1a93s9ak")))

(define-public crate-wat-1.0.72 (c (n "wat") (v "1.0.72") (d (list (d (n "wast") (r "^65.0.0") (d #t) (k 0)))) (h "1dhhl49rl83inznzpc6jigjz3jm67p5dz06srp2qvi5ip82xw3ly")))

(define-public crate-wat-1.0.73 (c (n "wat") (v "1.0.73") (d (list (d (n "wast") (r "^65.0.1") (d #t) (k 0)))) (h "07q5r3hhzfbwz9macgsf61gsv6p60d53zsbb9i7p30zlx9gf629j")))

(define-public crate-wat-1.0.74 (c (n "wat") (v "1.0.74") (d (list (d (n "wast") (r "^65.0.2") (d #t) (k 0)))) (h "1251bhn5xdzvf5d72angiyh9k1j45lrz8p7spb3ss26hhs6ilgnq")))

(define-public crate-wat-1.0.75 (c (n "wat") (v "1.0.75") (d (list (d (n "wast") (r "^66.0.0") (d #t) (k 0)))) (h "1rw0ly1841jfi9vxz15nbs16d2y1h3z97401hrp6nny68x03g027")))

(define-public crate-wat-1.0.76 (c (n "wat") (v "1.0.76") (d (list (d (n "wast") (r "^66.0.1") (d #t) (k 0)))) (h "1yml1drcci4fy6wpr3mir8d34ip6qfrg0y5yyrxjpw9iqzikjiln")))

(define-public crate-wat-1.0.77 (c (n "wat") (v "1.0.77") (d (list (d (n "wast") (r "^66.0.2") (d #t) (k 0)))) (h "0wkhzqsgv1rnghsa9vq1f1kwgf8x1qabpb59xsp3r46jjlh5hrz3")))

(define-public crate-wat-1.0.78 (c (n "wat") (v "1.0.78") (d (list (d (n "wast") (r "^67.0.0") (d #t) (k 0)))) (h "0x4swrw1m93js0lfzbpy37skkw8vgb9qkx0r9s7v3p2i6z8haaf0")))

(define-public crate-wat-1.0.79 (c (n "wat") (v "1.0.79") (d (list (d (n "wast") (r "^67.0.1") (d #t) (k 0)))) (h "1rv2j9nvzyr3srgg8fcir5s0c1ksqldks0608h8mby4j9y9j1cmd")))

(define-public crate-wat-1.0.80 (c (n "wat") (v "1.0.80") (d (list (d (n "wast") (r "^68.0.0") (d #t) (k 0)))) (h "0jdyrry4r4q132d6a8q90if13pkk1nc9xfk2asyv6dx849yy1avg")))

(define-public crate-wat-1.0.81 (c (n "wat") (v "1.0.81") (d (list (d (n "wast") (r "^69.0.0") (d #t) (k 0)))) (h "1cqypql0hb34n0z5x5yhm10q4i7asksl6sh8dr32d0q5im4c593l")))

(define-public crate-wat-1.0.82 (c (n "wat") (v "1.0.82") (d (list (d (n "wast") (r "^69.0.1") (d #t) (k 0)))) (h "0g6s68rx8j6gkj1kx468rz3ps26544znh9k4bv84qkgfipp3icxf")))

(define-public crate-wat-1.0.83 (c (n "wat") (v "1.0.83") (d (list (d (n "wast") (r "^70.0.0") (d #t) (k 0)))) (h "06hh7916wmxs6qsi4l9lsm2q8yqs8lg1lihyy1y73318vj6cw3cz")))

(define-public crate-wat-1.0.84 (c (n "wat") (v "1.0.84") (d (list (d (n "wast") (r "^70.0.1") (d #t) (k 0)))) (h "0pm1bi927m7qxra7qi0f5arvys7g7as5l09178jd44ylk52z6hc2")))

(define-public crate-wat-1.0.85 (c (n "wat") (v "1.0.85") (d (list (d (n "wast") (r "^70.0.2") (d #t) (k 0)))) (h "1mnjz9gs1d8pf3c64mg7ykxxlca1vnqxqhwwa2i4cvf4dixkbmxg")))

(define-public crate-wat-1.0.86 (c (n "wat") (v "1.0.86") (d (list (d (n "wast") (r "^71.0.0") (d #t) (k 0)))) (h "15nzyrnyzy70sa3jxrwsiz2ml2aks1p4z4jvfisg3vzz7ffl295p")))

(define-public crate-wat-1.0.87 (c (n "wat") (v "1.0.87") (d (list (d (n "wast") (r "^71.0.1") (d #t) (k 0)))) (h "0aji9j5i7fl2d9b17fkn1f67cbisaahhjnkqivcaypl84d8ijmqd")))

(define-public crate-wat-1.0.88 (c (n "wat") (v "1.0.88") (d (list (d (n "wast") (r "^71.0.1") (d #t) (k 0)))) (h "00frca0ysdy30pbf23rl0gm3f7lnfjv2hj6jqsi6h5a16kv3d75n")))

(define-public crate-wat-1.200.0 (c (n "wat") (v "1.200.0") (d (list (d (n "wast") (r "^200.0.0") (d #t) (k 0)))) (h "0gacd8djdc8darpvz6z2xvar37mv2bil0gxaprlkiy0pw88bsv3p")))

(define-public crate-wat-1.201.0 (c (n "wat") (v "1.201.0") (d (list (d (n "wast") (r "^201.0.0") (d #t) (k 0)))) (h "0cizrxad8y4wdrgcdwdy7f4dfd1nqxgh2s6bykjdx62vlhvmnga5")))

(define-public crate-wat-1.202.0 (c (n "wat") (v "1.202.0") (d (list (d (n "wast") (r "^202.0.0") (d #t) (k 0)))) (h "0j28d38x7fd98949nialc5x7h64mgfbyj1ilayimcp0k8xdb3r2d")))

(define-public crate-wat-1.203.0 (c (n "wat") (v "1.203.0") (d (list (d (n "wast") (r "^203.0.0") (d #t) (k 0)))) (h "0dvsxyhid5wcmp38wbvq4ivdhc2k5n1h7kjgfjc8f11585akp4g2")))

(define-public crate-wat-1.204.0 (c (n "wat") (v "1.204.0") (d (list (d (n "wast") (r "^204.0.0") (d #t) (k 0)))) (h "0d77yfqi8nccszn5llfr5598s256vmdy41dw0d6h451ja8nk5022")))

(define-public crate-wat-1.205.0 (c (n "wat") (v "1.205.0") (d (list (d (n "wast") (r "^205.0.0") (d #t) (k 0)))) (h "067b1f87k76ias92cs6762n5xv7cg0ssgjnl7jzwdrq6sqj2d0qr")))

(define-public crate-wat-1.206.0 (c (n "wat") (v "1.206.0") (d (list (d (n "wast") (r "^206.0.0") (d #t) (k 0)))) (h "1bg4xxrxh2iy5ackkw4kff01gicjzjr43x7b3acnwv170qk6yk6s")))

(define-public crate-wat-1.207.0 (c (n "wat") (v "1.207.0") (d (list (d (n "wast") (r "^207.0.0") (d #t) (k 0)))) (h "1rqyas0g2p5bdin5nr838ayysj95gwixrrq929g0yc2z5mgb3clf")))

(define-public crate-wat-1.208.0 (c (n "wat") (v "1.208.0") (d (list (d (n "wast") (r "^208.0.0") (d #t) (k 0)))) (h "03880wcy6gk6xks7j6n1h638bw7v7xb0903fxgr4bdqz3r6mpzbj") (r "1.76.0")))

(define-public crate-wat-1.208.1 (c (n "wat") (v "1.208.1") (d (list (d (n "wast") (r "^208.0.1") (d #t) (k 0)))) (h "0yz0q87055dssnc6dbyhj9myin7gxv82psi5q0a04mbnj7jkivaq") (r "1.76.0")))

(define-public crate-wat-1.209.0 (c (n "wat") (v "1.209.0") (d (list (d (n "wast") (r "^209.0.0") (d #t) (k 0)))) (h "1sa3nyb8f9yq4a69w54i2gwxy8xbjxl9k04bh4ffz66z3df17k0f") (r "1.76.0")))

(define-public crate-wat-1.209.1 (c (n "wat") (v "1.209.1") (d (list (d (n "wast") (r "^209.0.1") (d #t) (k 0)))) (h "092ddvqk2wa97bvaf5arngn0qlw691ibqzppn673y48x4z03w822") (r "1.76.0")))

