(define-module (crates-io #{3}# w wyo) #:use-module (crates-io))

(define-public crate-wyo-0.1.0 (c (n "wyo") (v "0.1.0") (h "0d2mzv4f5cc8prbj6c2x6z11n3a296c1fzlzgqnw8v47pkc8aqdc")))

(define-public crate-wyo-0.1.1 (c (n "wyo") (v "0.1.1") (h "1353iqisr8anzx8ml00c2508bnqj88vf0q3jlicd9kvw68vckbrh")))

