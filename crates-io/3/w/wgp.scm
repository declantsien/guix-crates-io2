(define-module (crates-io #{3}# w wgp) #:use-module (crates-io))

(define-public crate-wgp-0.0.1 (c (n "wgp") (v "0.0.1") (h "03dvdd1brkgka8mzkj6yg50pj6rl64m1dlqqnid9khbmqyqdj0dj") (y #t)))

(define-public crate-wgp-0.1.0 (c (n "wgp") (v "0.1.0") (d (list (d (n "atomic-waker") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.19") (o #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt-multi-thread" "time" "macros"))) (d #t) (k 2)))) (h "1myip2v056g78dqv8xgjv3gkpy03djkwvwizs2n31sk3h4y72h9w") (f (quote (("default" "futures-util"))))))

(define-public crate-wgp-0.2.0 (c (n "wgp") (v "0.2.0") (d (list (d (n "atomic-waker") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.19") (o #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("rt-multi-thread" "time" "macros"))) (d #t) (k 2)))) (h "0y5byj8wfzzf5i4az1qdcbwny97a62r57s2vxxb3aiwj0ar5wng1") (f (quote (("default" "futures-util"))))))

