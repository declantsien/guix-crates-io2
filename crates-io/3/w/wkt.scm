(define-module (crates-io #{3}# w wkt) #:use-module (crates-io))

(define-public crate-wkt-0.0.1 (c (n "wkt") (v "0.0.1") (h "0p2py6gxc4zvfxby6vwb9rf7gr1wdi1gxvrypjkk5n87kz3hb2vk")))

(define-public crate-wkt-0.0.2 (c (n "wkt") (v "0.0.2") (h "1js3mh1gn6lhkkdfcklbmvs62fnr0rz012fi22d89d6zj3hzhvhi")))

(define-public crate-wkt-0.0.3 (c (n "wkt") (v "0.0.3") (d (list (d (n "geo") (r "^0.0.2") (d #t) (k 0)))) (h "137d1kjddbsgvpi1w1z5xk96d1rk2dpc3qlzby3msi08nypv4hzg")))

(define-public crate-wkt-0.0.4 (c (n "wkt") (v "0.0.4") (d (list (d (n "geo") (r "^0.0.4") (d #t) (k 0)))) (h "1q6wi13d1fla3q3qsaf7pkgw54rgga847dhm31ak1qfr8zr809k2")))

(define-public crate-wkt-0.0.5 (c (n "wkt") (v "0.0.5") (d (list (d (n "geo") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "1wk17nwidc23lqsjbilrr13ca6z6m4ns6rckpnmmzzmqs20gqajh") (f (quote (("default" "geo"))))))

(define-public crate-wkt-0.0.6 (c (n "wkt") (v "0.0.6") (d (list (d (n "geo") (r "^0.0.4") (o #t) (d #t) (k 0)))) (h "1ggbzjamcg0frbb416d1f1lg09k43q60b9qzrfcdnjyqakj7vmcl") (f (quote (("default" "geo"))))))

(define-public crate-wkt-0.1.0 (c (n "wkt") (v "0.1.0") (d (list (d (n "geo-types") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0yv0qzhaqzfdxxxmvxddil76ri5nw7la5nsk6m1rxxfd5lplfqra") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.1.1 (c (n "wkt") (v "0.1.1") (d (list (d (n "geo-types") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0xavscn95i59c222hxcq0wc1knr5q22vha8674j779jfyxq1cps9") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.1.2 (c (n "wkt") (v "0.1.2") (d (list (d (n "geo-types") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1wf90f9yhf2xkdhk4xg5d6fi613kl5mnymbrl22zkhmmvkq5xfa2") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.1.3 (c (n "wkt") (v "0.1.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1isvl7cngfdq48js5hdn0yw16nnga56ik4z5i49gkdqzzk99p38v") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.2.0 (c (n "wkt") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0rwifdk72d0vl4j5jsdmn91cdwfi8y7g4r21cljnsirj97sgjkgn") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.2.1 (c (n "wkt") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1z0mcvwamwlhdxkcp2vl38a172q7446kzjrj9q3pmhxf8hadp07b") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.4.0 (c (n "wkt") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1ppclym6whhciv75x9hi14a9gvwq6clji424kc822irxax90klx8") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.5.0 (c (n "wkt") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1y2nls6iamplq7pkvr4ipcbs99n438csna2kpxq67pkp9681fvl1") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.6.0 (c (n "wkt") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hmakd4rvkyl7yz9zrzp51bmvlxbc0k4ja3ywpdm169zgjh55gqp") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.7.0 (c (n "wkt") (v "0.7.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19iw3gg5jxc871s2xz30wjmr6gmxq1lmrwhdk55p7q20klym22y4") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.8.0 (c (n "wkt") (v "0.8.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rjqsj2qinqqg0cjchhqx1c1zbnyxa5lv3rbfsba5rn64g70hpyd") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.9.0 (c (n "wkt") (v "0.9.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "177c0gpb35xlxpq7mql2sbkl8siq41il9y8jg9969kvdxcb89d9d") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.9.1 (c (n "wkt") (v "0.9.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0zxznw8rixwbswaqhl19m3rihqzlpk9n9v3y975xpmjjl3w01zdi") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.9.2 (c (n "wkt") (v "0.9.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0ahi0ycrdv6pr9zj8iv8jb0gkvf62ai0hmgg1virvjs8yxhlzjx3") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.10.0 (c (n "wkt") (v "0.10.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.3") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0kjgvgj6jgxv6vmg4gc3wmslabhah729c5mb4cpjsykp53vzdklq") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.10.1 (c (n "wkt") (v "0.10.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1qc6m3p5ixfw6zylm5rw4xxa59s1rzvkzsimr14b0n13bajlvx3a") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.10.2 (c (n "wkt") (v "0.10.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1mdl9ahsifvfy8gdxdbxibqfgvyz5av625by3n25k4rkxk2y9ly7") (f (quote (("default" "geo-types"))))))

(define-public crate-wkt-0.10.3 (c (n "wkt") (v "0.10.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "geo-types") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0hcg3jxnnhk2d1gq5cd2iswa4nd7cp4nfsmsx1s7k4pqh4kjbhn3") (f (quote (("default" "geo-types"))))))

