(define-module (crates-io #{3}# w wet) #:use-module (crates-io))

(define-public crate-wet-0.1.0 (c (n "wet") (v "0.1.0") (h "1mxvqrmizv2s6w453gg3i5yahl6hd7y40fa6wcmmsw9hph3i3w20")))

(define-public crate-wet-0.2.0 (c (n "wet") (v "0.2.0") (h "19ndj6kpy68nhq4zyb9d3yi64na7v6b0b9nf3s44zjfk1j8lgvm8")))

