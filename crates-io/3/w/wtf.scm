(define-module (crates-io #{3}# w wtf) #:use-module (crates-io))

(define-public crate-wtf-0.0.0 (c (n "wtf") (v "0.0.0") (h "1a041cnkrn3qzwlnj84dic49127mdcjg5ynk294azfb33lgzdf9a")))

(define-public crate-wtf-0.0.1 (c (n "wtf") (v "0.0.1") (h "0vfw1bdn0j9dqmif8afq2ax7gxkn1bck2j6wzc4jvcv3dgd9fn5r")))

(define-public crate-wtf-0.0.2 (c (n "wtf") (v "0.0.2") (h "173qaz7dyhdiyk1g1wf4h4128nl35y58nfcvjw5z3kjjaqn28qqp")))

(define-public crate-wtf-0.0.3 (c (n "wtf") (v "0.0.3") (h "08ysi6cpvw2s1f21w8arapq8ghvlm9fzm4mc9jj40mrs6h8arxrn")))

(define-public crate-wtf-0.1.0 (c (n "wtf") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "bumpalo") (r "^3.7") (o #t) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "flume") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "snap") (r "^1.0") (d #t) (k 0)))) (h "06g5r0mcdkrd7i0fk8ma07b70d7ii25lwwali0a85l4mmp1dipc4") (f (quote (("profile" "bumpalo" "chrono" "flume" "once_cell"))))))

