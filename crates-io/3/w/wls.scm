(define-module (crates-io #{3}# w wls) #:use-module (crates-io))

(define-public crate-wls-0.1.0 (c (n "wls") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4.4.18") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "gzip"))) (d #t) (k 0)) (d (n "robotstxt") (r "^0.3.0") (d #t) (k 0)) (d (n "sitemap") (r "^0.4.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "0n96yp34mpn6l479jam4055g0axyvs4j02adwv9y909xwaa3328m")))

