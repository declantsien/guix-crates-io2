(define-module (crates-io #{3}# w wjp) #:use-module (crates-io))

(define-public crate-wjp-0.1.0 (c (n "wjp") (v "0.1.0") (h "0mdg1fny5dh33dz8ik22fvz4axwwak8wvcbs1wf2szhy43gi6jfn")))

(define-public crate-wjp-1.0.0 (c (n "wjp") (v "1.0.0") (h "163xnrwydib340whpxrnbb377122lr8wkpvknbprfx55p81hl67a")))

(define-public crate-wjp-1.0.1 (c (n "wjp") (v "1.0.1") (h "1afw1086lbca4aqq87l9p7wy5mvim37w82q56z9d4d97cza6zbx8")))

(define-public crate-wjp-1.0.2 (c (n "wjp") (v "1.0.2") (h "1bh62riwywwsz4r7dzkv95fxb0yginlq17axpkyvkj8p27l24354")))

(define-public crate-wjp-1.1.0 (c (n "wjp") (v "1.1.0") (h "0hfglgxxdsvpkl28598fnkf31nhgixjyb3lykf4ysqmiyri36x31")))

(define-public crate-wjp-1.1.1 (c (n "wjp") (v "1.1.1") (h "0pzbhk0fpl6xnq692dnkzd1aqjhkdhayrka56lsx4dk3p6rbsn46")))

(define-public crate-wjp-1.1.2 (c (n "wjp") (v "1.1.2") (h "0j7zhyxd90k7xhdxyzj6p0svf62w0fd5janv5bhfv7yv33p0hf4a")))

(define-public crate-wjp-1.1.3 (c (n "wjp") (v "1.1.3") (h "0xpylrvykf0ij76smf0ylnrb4b3xmhq28jsnf1zzcfq83fcyxhp4")))

