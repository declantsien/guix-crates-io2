(define-module (crates-io #{3}# w wpp) #:use-module (crates-io))

(define-public crate-wpp-0.0.1 (c (n "wpp") (v "0.0.1") (d (list (d (n "wgpu") (r "^0.14") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "meshtext") (r "^0.2") (d #t) (k 2)) (d (n "pollster") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "winit") (r "^0.27") (d #t) (k 2)))) (h "07j4gq28zpywszgzxhnaswv3bwkrv5ww4vlxldi5xsq2kjknaypz") (f (quote (("grayscale") ("default" "grayscale"))))))

