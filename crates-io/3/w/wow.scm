(define-module (crates-io #{3}# w wow) #:use-module (crates-io))

(define-public crate-wow-0.1.0 (c (n "wow") (v "0.1.0") (h "1hkqp39nrh94411z3ihlgm25a1mcg4diids4vy90l9s2kk74m8hy")))

(define-public crate-wow-0.1.2 (c (n "wow") (v "0.1.2") (h "1ds1ghcb5w9m0hs40rvgsc3wv9w72nbwx3629rs5w7b6w5haz90y")))

