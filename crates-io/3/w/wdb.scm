(define-module (crates-io #{3}# w wdb) #:use-module (crates-io))

(define-public crate-wdb-0.1.0 (c (n "wdb") (v "0.1.0") (d (list (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "object") (r "^0.28.3") (d #t) (k 0)))) (h "0zpb52z4shniybcr4gmyv9cbm53idlnd0jnwflfws0a8qsj1zmqp") (y #t)))

(define-public crate-wdb-0.1.1 (c (n "wdb") (v "0.1.1") (d (list (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "object") (r "^0.28.3") (d #t) (k 0)))) (h "0dpgfyvyx2mznrbp12aw63kdwppkpnna6llv6h3b7xbij01gh94a") (y #t)))

(define-public crate-wdb-0.1.2 (c (n "wdb") (v "0.1.2") (d (list (d (n "gimli") (r "^0.26.1") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)) (d (n "object") (r "^0.28.3") (d #t) (k 0)))) (h "016dli9h7szqfq9mnhxbssykqy59zw4k4z9819rdcqpn718slb8d") (y #t)))

