(define-module (crates-io #{3}# w wig) #:use-module (crates-io))

(define-public crate-wig-0.0.0 (c (n "wig") (v "0.0.0") (h "10ps66wfwc5xflqd3rpkckgay07rwr3k9pnkpjan98k7p2q41w5m")))

(define-public crate-wig-0.1.0 (c (n "wig") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.4.0") (d #t) (k 0)))) (h "14q4jc6327dn33v37nmlpvpd544gdy6q4p678d8x0mcd1j6np6qf")))

(define-public crate-wig-0.6.0 (c (n "wig") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.4.0") (d #t) (k 0)))) (h "13v3fm9vb4vm2jq0pdnnqrq8n5wv1zrfmxxbpmyn47cbpkw0pp1h")))

(define-public crate-wig-0.7.0 (c (n "wig") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.4.0") (d #t) (k 0)))) (h "06ccg8bvhyz1vbgscz5j2i8cnmlwbjvbkcy579ih1z0vmkgmcf5f")))

(define-public crate-wig-0.8.0 (c (n "wig") (v "0.8.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.5.0") (d #t) (k 0)))) (h "1p819gz6dvjl84dzyvwcf0kr0qx3m8hqq3a80nf0m61h02k5z83d")))

(define-public crate-wig-0.9.0 (c (n "wig") (v "0.9.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.6.0") (d #t) (k 0)))) (h "1vv3yiidr98mcdf7gy9h1sipypcms5zfjbwyi4xrcryzgj4n6gld")))

(define-public crate-wig-0.9.1 (c (n "wig") (v "0.9.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.6.0") (d #t) (k 0)))) (h "15d4qy0pa2lgzqy0gkhr31a52z92iwkp4a2fqjayyg1jlyvs1qd2")))

(define-public crate-wig-0.9.2 (c (n "wig") (v "0.9.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.6.0") (d #t) (k 0)))) (h "1l280j12j3cfrxd7pasd3kvfnf3180fzv9vy3kzz6hs5wlns107q")))

(define-public crate-wig-0.10.0 (c (n "wig") (v "0.10.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.6.0") (d #t) (k 0)))) (h "0agwr6w7xkn3lfc32cxkj36nndcb5cmfsp5ipxg8k4874ag119s0")))

(define-public crate-wig-0.11.0 (c (n "wig") (v "0.11.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.7.0") (d #t) (k 0)))) (h "1havdzv9ivv7772rdzd3p76bi83mmcc1g0k1vna87bkqb6339p97")))

(define-public crate-wig-0.12.0 (c (n "wig") (v "0.12.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.8.0") (d #t) (k 0)))) (h "19w97k2acjzz6qh8kcim4wzksz1gadwaxlyd4207zpmbmkndss55")))

(define-public crate-wig-0.13.0 (c (n "wig") (v "0.13.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.8.4") (d #t) (k 0)))) (h "0svj18ghgnz94m90cfpsdiyybqnanx6vp0l1vr2q268asvv9scpy")))

(define-public crate-wig-0.14.0 (c (n "wig") (v "0.14.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.8.4") (d #t) (k 0)))) (h "0rwpzhvb2mg406a9igkjijhcvmhp459azzb9kyqnk42fvikbk0h4")))

(define-public crate-wig-0.15.0 (c (n "wig") (v "0.15.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.8.4") (d #t) (k 0)))) (h "0knsvw96mp6b0xc19yxal5psi5ivfka7vv52d74vqg7kjr0sa0z3")))

(define-public crate-wig-0.16.0 (c (n "wig") (v "0.16.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "0zarwynbdnwlavajr5g7cdls9dfs6s04zhlfndjdmgx1dv16bi41")))

(define-public crate-wig-0.17.0 (c (n "wig") (v "0.17.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "03mdy8qiz45cza3gbhsfjjy8yb3zp7j0l71i9xvdvwdyf9g4nrfj")))

(define-public crate-wig-0.18.0 (c (n "wig") (v "0.18.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "1czqyg5mfw1695yf2l227gh5rbhifwgfylfyglji02nzppb74xwg")))

(define-public crate-wig-0.19.0 (c (n "wig") (v "0.19.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.8.5") (d #t) (k 0)))) (h "1qrvx0yz2cp6kwwwlr4ar2apava3bn08byyz44i3nbgm0icwvaix")))

(define-public crate-wig-0.20.0 (c (n "wig") (v "0.20.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.8.7") (d #t) (k 0)))) (h "0xpfsqqm7wyqj53c876shwxa5nqp3cxm0kqrwipfhjqi7wwl0nkn")))

(define-public crate-wig-0.21.0 (c (n "wig") (v "0.21.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "witx") (r "^0.8.7") (d #t) (k 0)))) (h "0qmzyirvx5jvs8873w2wyws4v7d1m5vm8w9xsfc3xlpqmhf00h8s")))

