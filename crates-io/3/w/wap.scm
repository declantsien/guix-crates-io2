(define-module (crates-io #{3}# w wap) #:use-module (crates-io))

(define-public crate-wap-0.1.0 (c (n "wap") (v "0.1.0") (h "01j7l6i127q18zmqnlidmwshcsxlps38f0vwzp8xb3bv0hzdkq5k")))

(define-public crate-wap-0.1.1 (c (n "wap") (v "0.1.1") (h "1lhw2mg1mkzdhvhss8wikfahrkbh2hhfqdn31ylvkqh210cwxl79") (f (quote (("default") ("console-log"))))))

(define-public crate-wap-0.1.2 (c (n "wap") (v "0.1.2") (h "18g4jjfn1vq0fyvgdiyrdznq1b9kdq8mysyh12yhxx4p6mz70918") (f (quote (("default") ("console-log"))))))

(define-public crate-wap-0.1.3 (c (n "wap") (v "0.1.3") (h "1x0aw9wh5x0y1lxxas62rdhsb46bdaypq2bahnsyjd2s5285c3cx") (f (quote (("default") ("console-log"))))))

