(define-module (crates-io #{3}# w wux) #:use-module (crates-io))

(define-public crate-wux-0.1.0 (c (n "wux") (v "0.1.0") (d (list (d (n "binrw") (r "^0.13.0") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("zerocopy"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0ng6g60qkkc6brmyhq4j2dxz6vn65qxg0w57pznx0wm5p8fxkcn7")))

