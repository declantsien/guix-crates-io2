(define-module (crates-io #{3}# w wsv) #:use-module (crates-io))

(define-public crate-wsv-0.1.0 (c (n "wsv") (v "0.1.0") (h "0b2fw5rmc62ypnhw9llv5q4cyqbn4cncqvqp3lvxjwz45zf0zinc")))

(define-public crate-wsv-0.1.1 (c (n "wsv") (v "0.1.1") (h "1z3206v9bvrmw530y0f712psnbrrrkkxm3m86wb3jnn4qcbvwv8v")))

(define-public crate-wsv-0.1.2 (c (n "wsv") (v "0.1.2") (h "1cb4vaj2x0hg2vlp6rj9janamzajc0gvqh5fyqma9k5374fglf4i")))

(define-public crate-wsv-0.1.3 (c (n "wsv") (v "0.1.3") (d (list (d (n "unicode-bom") (r "^2") (d #t) (k 0)))) (h "15813k5dzzcninzni8g1h71n0xhznc7zzkc8h562vp1jy9bjlq5s")))

(define-public crate-wsv-0.1.4 (c (n "wsv") (v "0.1.4") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "unicode-bom") (r "^2") (d #t) (k 0)))) (h "1p5llpwvgscqd2rk6hwv08m93nlrymijmjm13sxjg438bwhhwfiw")))

(define-public crate-wsv-0.1.5 (c (n "wsv") (v "0.1.5") (d (list (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.6") (d #t) (k 0)) (d (n "pest_derive") (r "^2.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "unicode-bom") (r "^2") (d #t) (k 0)))) (h "0nzvzl4r16bpk8pk168sizqs10ny9hfn5drba3yq2fgr0ivafkp9")))

(define-public crate-wsv-0.2.0 (c (n "wsv") (v "0.2.0") (d (list (d (n "comfy-table") (r "^7") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "082jh1rgmkb6176nlzysg6xj1df3996992sp70hndci0y92rzq4l")))

(define-public crate-wsv-0.3.0 (c (n "wsv") (v "0.3.0") (d (list (d (n "comfy-table") (r "^7") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "03pb5avq5747d0dg8cyr6f2wqcd2zyp17vn2znz7gv15zk5klcjg")))

(define-public crate-wsv-0.3.1 (c (n "wsv") (v "0.3.1") (d (list (d (n "comfy-table") (r "^7") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "1yc7hwyn6rryhgscxb0v84gg2kmrywzgn7k6n66wrc7rm3saydhl")))

(define-public crate-wsv-0.3.2 (c (n "wsv") (v "0.3.2") (d (list (d (n "comfy-table") (r "^7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "pest") (r "^2") (d #t) (k 0)) (d (n "pest_derive") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0l0a44lzf096z4r9ny8ic9nbyxar4s6kz2n2yl7idxfr4yl0w6z9")))

(define-public crate-wsv-0.3.3 (c (n "wsv") (v "0.3.3") (d (list (d (n "comfy-table") (r "^7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2") (o #t) (d #t) (k 0)) (d (n "pest_derive") (r "^2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "058fkzqrafr8xj8hcxslh7pbvv2m9nch2m4n86h121rxjlsdl4m3") (s 2) (e (quote (("pest" "dep:pest" "dep:pest_derive") ("nom" "dep:nom"))))))

(define-public crate-wsv-0.4.0 (c (n "wsv") (v "0.4.0") (d (list (d (n "comfy-table") (r "^7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2") (o #t) (d #t) (k 0)) (d (n "pest_derive") (r "^2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)))) (h "0lw0dvrwqa4k04724h6lxl2x5jxgbxz672kjnhws818q7gw62mdn") (s 2) (e (quote (("pest" "dep:pest" "dep:pest_derive") ("nom" "dep:nom"))))))

(define-public crate-wsv-0.5.0 (c (n "wsv") (v "0.5.0") (d (list (d (n "comfy-table") (r "^7") (d #t) (k 0)) (d (n "divan") (r "^0") (d #t) (k 0)) (d (n "divan") (r "^0.1.14") (d #t) (k 2)) (d (n "itertools") (r "^0") (d #t) (k 0)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "nom-supreme") (r "^0") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2") (o #t) (d #t) (k 0)) (d (n "pest_derive") (r "^2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0") (d #t) (k 0)))) (h "1y0b47p22gp4ii4ai3cwgmqkjpc7l03rwb12qzbsz5na9g0pl13a") (f (quote (("default" "nom" "pest")))) (s 2) (e (quote (("pest" "dep:pest" "dep:pest_derive") ("nom" "dep:nom" "dep:nom-supreme"))))))

