(define-module (crates-io #{3}# w waj) #:use-module (crates-io))

(define-public crate-waj-0.2.0 (c (n "waj") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "jbk") (r "^0.2.0") (d #t) (k 0) (p "jubako")) (d (n "libwaj") (r "^0.2.0") (d #t) (k 0)))) (h "1gn8yf2xcfb1i3ri7dzvyamx5wz50h3s064y291xwny97zzbq3rg")))

(define-public crate-waj-0.2.1 (c (n "waj") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.5.0") (d #t) (k 0)) (d (n "clap_mangen") (r "^0.2.20") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "human-panic") (r "^1.2.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.3") (d #t) (k 0)) (d (n "jbk") (r "^0.2.1") (d #t) (k 0) (p "jubako")) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "waj") (r "^0.2.1") (d #t) (k 0) (p "libwaj")))) (h "14988ai3qxrjk1c06mw30xfswdxgmzmh28dssmvbp7djpid3hj8l")))

