(define-module (crates-io #{3}# w wil) #:use-module (crates-io))

(define-public crate-wil-0.0.1 (c (n "wil") (v "0.0.1") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("securitybaseapi" "handleapi" "processthreadsapi" "errhandlingapi"))) (d #t) (k 0)))) (h "1rv7424fyxwzkdy0a5jld8m0q9c2i64xi0cls6h79p6wj5w4mmkd")))

(define-public crate-wil-0.0.2 (c (n "wil") (v "0.0.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("securitybaseapi" "handleapi" "processthreadsapi" "errhandlingapi"))) (d #t) (k 0)))) (h "0wgzwrw62hg4si4535h2wknzkms216r8m1zx8pza2p28df2m73d8")))

(define-public crate-wil-0.0.3 (c (n "wil") (v "0.0.3") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("securitybaseapi" "handleapi" "processthreadsapi" "errhandlingapi"))) (d #t) (k 0)))) (h "1kf4qfcx72jllakpmw6k730716y3hzkpq04mxl274rjz3ksk89vf")))

(define-public crate-wil-0.0.4 (c (n "wil") (v "0.0.4") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("securitybaseapi" "handleapi" "processthreadsapi" "errhandlingapi"))) (d #t) (k 0)))) (h "094asilcw1d3gcs22qa4q9hazmq3ngxgq7z5zgphyz7617z78pzj")))

(define-public crate-wil-0.0.5 (c (n "wil") (v "0.0.5") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("securitybaseapi" "handleapi" "processthreadsapi" "errhandlingapi"))) (d #t) (k 0)))) (h "1ivgj5sdml3hhmh5raxh52szg2pm18g7kcp6ldxc7rmk9cwxqb49")))

(define-public crate-wil-0.0.6 (c (n "wil") (v "0.0.6") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("securitybaseapi" "handleapi" "processthreadsapi" "errhandlingapi"))) (d #t) (k 0)))) (h "0jnp165wrlpxwgpjpma4vy3a5x3p9pmifalb0wbjnmlzdhiscfrc")))

