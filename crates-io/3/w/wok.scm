(define-module (crates-io #{3}# w wok) #:use-module (crates-io))

(define-public crate-wok-0.1.0 (c (n "wok") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.82") (f (quote ("std"))) (k 0)))) (h "0cbkvv8blf14f7j9f5aq74k11ib8rqg15srmp6m7lh8ksyfnhqn9") (f (quote (("trace")))) (y #t)))

(define-public crate-wok-0.0.0 (c (n "wok") (v "0.0.0") (d (list (d (n "wasm-bindgen") (r "^0.2.82") (f (quote ("std"))) (k 0)))) (h "0lzi7hgarhvbxfjmwjfgb3ysahrr2qs1yq06sdsxm2vkvd4wamab") (f (quote (("trace"))))))

