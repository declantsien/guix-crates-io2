(define-module (crates-io #{3}# w wec) #:use-module (crates-io))

(define-public crate-wec-1.0.0 (c (n "wec") (v "1.0.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "01l1519f1blqy5wd4y97rlpr2b81ac2k5vq5v3zr1558yxvlrpak")))

(define-public crate-wec-1.0.1 (c (n "wec") (v "1.0.1") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0srcng2jh0ihzd6861hcv3nharic0kc5bvrlsni87gdc4gfpd0j8")))

(define-public crate-wec-1.0.2 (c (n "wec") (v "1.0.2") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.80") (d #t) (k 2)))) (h "0gkhy14vy1al0h7yz3ziapncd3mjxh24yshdv3sjw13708ghy9zc")))

