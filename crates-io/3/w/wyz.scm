(define-module (crates-io #{3}# w wyz) #:use-module (crates-io))

(define-public crate-wyz-0.1.0 (c (n "wyz") (v "0.1.0") (h "1c4mqhlmk7dk3qbh98w0vnf1vknc1hh54a0zmdrklnvh7ph0pkxf") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-wyz-0.1.1 (c (n "wyz") (v "0.1.1") (h "06l3nx0xh3r5aanr845662yhf8qc1slddq5l4xdznzs5j40ml740") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-wyz-0.2.0 (c (n "wyz") (v "0.2.0") (h "05028bk49b2ix1lz22sj65fnlxr0f29j2klkaqjxp6az3c6hprl5") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-wyz-0.3.0 (c (n "wyz") (v "0.3.0") (d (list (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (o #t) (d #t) (k 0)))) (h "030bwah8xxh3gzpr41apcr0vwmci9hhzfavks9f9kmh0nq70ygq4") (f (quote (("std" "alloc") ("garbage" "once_cell" "typemap") ("default" "std") ("alloc"))))))

(define-public crate-wyz-0.4.0 (c (n "wyz") (v "0.4.0") (d (list (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (o #t) (d #t) (k 0)))) (h "126irvwn63kxgqjk5ap1pw3p3grw2sskyg32h0v4bqawsrx057hj") (f (quote (("std" "alloc") ("garbage" "once_cell" "typemap") ("default" "std") ("alloc"))))))

(define-public crate-wyz-0.5.0 (c (n "wyz") (v "0.5.0") (d (list (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (o #t) (d #t) (k 0)))) (h "03ir858jfk3sn98v3vzh33ap8s27sfgbalrv71n069wxyaa1bcrh") (f (quote (("std" "alloc") ("garbage" "once_cell" "typemap") ("default" "std") ("alloc"))))))

(define-public crate-wyz-0.6.0 (c (n "wyz") (v "0.6.0") (d (list (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)))) (h "15aa04wir3si83k6kwzqykpm9acjmnyy7r50gpp09n5421yr64km") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.40")))

(define-public crate-wyz-0.5.1 (c (n "wyz") (v "0.5.1") (d (list (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "tap") (r "^1") (d #t) (k 0)) (d (n "typemap") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1vdrfy7i2bznnzjdl9vvrzljvs4s3qm8bnlgqwln6a941gy61wq5") (f (quote (("std" "alloc") ("garbage" "once_cell" "typemap") ("default" "std") ("alloc"))))))

(define-public crate-wyz-0.6.1 (c (n "wyz") (v "0.6.1") (d (list (d (n "once_cell") (r "^1") (o #t) (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)))) (h "1si1s1lxr1w4zfqimz2dih56dx9zd4gxpkbf1j9d8b87is19mvjf") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.40")))

