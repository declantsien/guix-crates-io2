(define-module (crates-io #{3}# w wot) #:use-module (crates-io))

(define-public crate-wot-0.1.0 (c (n "wot") (v "0.1.0") (d (list (d (n "wot-discovery") (r "^0.1") (d #t) (k 0)) (d (n "wot-serve") (r "^0.1") (d #t) (k 0)) (d (n "wot-td") (r "^0.1") (d #t) (k 0)))) (h "0xfg87b8kni5rxjfig53alkb9yx20kp5b7jy412r18znh3a6jsa4")))

(define-public crate-wot-0.2.0 (c (n "wot") (v "0.2.0") (d (list (d (n "wot-serve") (r "^0.3.1") (d #t) (k 0)) (d (n "wot-td") (r "^0.2.1") (d #t) (k 0)))) (h "0ybkg9ljz1zn2v0i5n7blsy47i6hxlds9qjv181ljcybvjhdl08d")))

