(define-module (crates-io #{3}# w wyr) #:use-module (crates-io))

(define-public crate-wyr-0.1.0 (c (n "wyr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "ipnet") (r "^2.3.0") (d #t) (k 0)) (d (n "miscreant") (r "^0.5.2") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "040zz567q7ilfhsxhqpyavlq30wyv086wfk1bsv45nmbdb7vgs3l")))

(define-public crate-wyr-0.2.1 (c (n "wyr") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "ipnet") (r "^2.3.0") (d #t) (k 0)) (d (n "miscreant") (r "^0.5.2") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "11q07akyv8gzaanbjdl2cxb0rfal3wkmg0g6vaq6rrsxi6ii6flj")))

(define-public crate-wyr-0.3.2 (c (n "wyr") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 0)) (d (n "derive_builder") (r "^0.9.0") (d #t) (k 0)) (d (n "ipnet") (r "^2.3.0") (d #t) (k 0)) (d (n "miscreant") (r "^0.5.2") (d #t) (k 0)) (d (n "my_internet_ip") (r "^0.1.1") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.15.2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)))) (h "0z5qgryk1dziq42sb8d5989ygzgziva8sqv5ll6ydy6f6dhhrkx2")))

