(define-module (crates-io #{3}# w wst) #:use-module (crates-io))

(define-public crate-wst-0.1.0 (c (n "wst") (v "0.1.0") (d (list (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "1bq5ilgmyr55qpsx0xcj3f0g2swam9jp91z92rimvs3x1hrc1zgn")))

(define-public crate-wst-0.1.1 (c (n "wst") (v "0.1.1") (d (list (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "1hp0qb5z560v96g725smgralbkkb4jbdq00p31f69z0zqamwi1kq")))

(define-public crate-wst-0.1.2 (c (n "wst") (v "0.1.2") (d (list (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "0l5dbx16p6hrw0v15r9f4ml3b910cxjxc6z1hk6yb4p0x74ml082")))

(define-public crate-wst-0.2.0 (c (n "wst") (v "0.2.0") (d (list (d (n "getopts") (r "^0.2.15") (d #t) (k 0)) (d (n "ws") (r "^0.7.3") (d #t) (k 0)))) (h "0s11mmz2m2w396j3nidd3lvgq3ca4ay9xav5ciigvmgd8fyiwl1z")))

