(define-module (crates-io #{3}# w wyw) #:use-module (crates-io))

(define-public crate-wyw-0.1.0 (c (n "wyw") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.8") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.26.0") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)))) (h "0zszrv1r0cd2fagk71v86lxyx5bvkbgmk45b7l7h1blpqnw28jmd")))

