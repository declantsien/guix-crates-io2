(define-module (crates-io #{3}# w wpr) #:use-module (crates-io))

(define-public crate-wpr-0.1.0 (c (n "wpr") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "073rpwkyy6hg2dx9ij0wvwhviqzxvfz2vam5133jw2kk6nwx2krm")))

(define-public crate-wpr-0.1.2 (c (n "wpr") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1zalp9kc8m92nksirgvim77v0f5l2fr6pc2bl7yipckk298s8g40")))

(define-public crate-wpr-0.2.0 (c (n "wpr") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "109mf5y5ibsfq7ipk94hdg03g4r756ppmw8krd4km41ji4kcdxm4")))

(define-public crate-wpr-0.3.0 (c (n "wpr") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("cargo" "std"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1qghhaslzhdwvxfwnrjjvkwc7npph44mkwszcp6qlnsr27899sxg")))

