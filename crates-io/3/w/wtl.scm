(define-module (crates-io #{3}# w wtl) #:use-module (crates-io))

(define-public crate-wtl-0.1.0 (c (n "wtl") (v "0.1.0") (d (list (d (n "axum") (r "^0.6.20") (d #t) (k 0)))) (h "190bb3ma0hrclj5hmk84124hkp8knr0q2ar7snfwwbw9x7qbs5qi")))

(define-public crate-wtl-0.1.1 (c (n "wtl") (v "0.1.1") (d (list (d (n "axum") (r "^0.6.20") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0mlzmpv44xi627dwyw10cdl9g7pgkcms5j5j7p66xphwczj38gj7")))

(define-public crate-wtl-0.1.2 (c (n "wtl") (v "0.1.2") (d (list (d (n "axum") (r "^0.6.20") (o #t) (d #t) (k 0)) (d (n "hyper") (r "^0.14.27") (d #t) (k 0)) (d (n "url") (r "^2.4.1") (d #t) (k 0)))) (h "0jf0rzqg8kd373qxj6f1as0mc5jb77h5hb93img2aycgk89xfdy1")))

