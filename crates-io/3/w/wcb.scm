(define-module (crates-io #{3}# w wcb) #:use-module (crates-io))

(define-public crate-wcb-1.0.0 (c (n "wcb") (v "1.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "env"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.1.1") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "webcryptobox") (r "^2.0.0") (d #t) (k 0)))) (h "187d8jj3slbamgwb62qp5f0fcv4ax2n5rk96wwi5x3b59vi0c7xp")))

(define-public crate-wcb-2.0.0 (c (n "wcb") (v "2.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "env"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.1.1") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "webcryptobox") (r "^2.0.0") (d #t) (k 0)))) (h "0c5zdk07pa1wlclgyxq3gag9qn4ad61d1xb167cc3df9qc9wq7cf")))

(define-public crate-wcb-2.0.1 (c (n "wcb") (v "2.0.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "clap") (r "^3.1.6") (f (quote ("derive" "env"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.1.1") (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "webcryptobox") (r "^2.0.0") (d #t) (k 0)))) (h "151fq5jbd7xkv3611sm0fijfzlk9j8q06qhdn8ji3p1d7wnbaa3s")))

