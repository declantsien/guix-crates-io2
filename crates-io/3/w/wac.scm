(define-module (crates-io #{3}# w wac) #:use-module (crates-io))

(define-public crate-wac-0.0.1 (c (n "wac") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "wabt") (r "^0.9") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.17") (d #t) (k 0)))) (h "13n7sn44g9c2lsc43p7jznrhckjpfr8v8fx1kzx8zpqkyrb9pr6x")))

