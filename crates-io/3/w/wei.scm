(define-module (crates-io #{3}# w wei) #:use-module (crates-io))

(define-public crate-wei-0.1.0 (c (n "wei") (v "0.1.0") (h "00j91snlgw2a7mwrnka3299r0vpmlz5h6axjj9lbdg9bc8izzhhn")))

(define-public crate-wei-0.1.1 (c (n "wei") (v "0.1.1") (h "1kjphw31wbaas0mx2kd7z9d7xzj514plwbvhhaik5p1s4mh7akxw")))

(define-public crate-wei-0.1.2 (c (n "wei") (v "0.1.2") (d (list (d (n "rkill_lib") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("full"))) (d #t) (k 0)))) (h "01qrjc702rr554blrcb6yvk0x3vbznkvghd228k230rb84kar4h6")))

(define-public crate-wei-0.1.3 (c (n "wei") (v "0.1.3") (h "1k6mvw4azgk4wmkrqxb8g5g97wvi6698mw6bjjgh85d2mvcj34in")))

(define-public crate-wei-0.1.4 (c (n "wei") (v "0.1.4") (h "05rhj56z07pzmr7sh4hfm7c84nki6m6vh4d4r03zi9xsnllgvwi6")))

(define-public crate-wei-0.1.5 (c (n "wei") (v "0.1.5") (h "042bq1s0wdnlvsx43wp3infbgyqxjv49z7q21gkbcdfm42mv8d00")))

(define-public crate-wei-0.1.6 (c (n "wei") (v "0.1.6") (d (list (d (n "wei-env") (r "^0.2.4") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.5") (d #t) (k 0)))) (h "0x40rl199baa3bqvvzbbhqczr5s418c0gx0b6msfb6flrbp4mik6")))

(define-public crate-wei-0.1.7 (c (n "wei") (v "0.1.7") (d (list (d (n "wei-env") (r "^0.2.4") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.5") (d #t) (k 0)))) (h "06mfhpiyy46mif7pf343p1w7jmjfm7f85n5rb0xz090yjkviifax")))

(define-public crate-wei-0.1.8 (c (n "wei") (v "0.1.8") (d (list (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.3") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.4") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.10") (d #t) (k 0)))) (h "11n8c36wi8wf4vq4s3pf8nzzgra3nngi31qxnqj7zpfa0szw5a9g")))

(define-public crate-wei-0.1.9 (c (n "wei") (v "0.1.9") (d (list (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.3") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.4") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.10") (d #t) (k 0)))) (h "08wjd7vcdfjifp9lm3kalmnfcyzmiir8azhp5x0czjlgx54fr1ry")))

(define-public crate-wei-0.1.10 (c (n "wei") (v "0.1.10") (d (list (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.5") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.10") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "0qc481c2q9f22vr4pzsxjdkawv5ypdm00kdsw7n1cc6yqg9nhmiv")))

(define-public crate-wei-0.1.11 (c (n "wei") (v "0.1.11") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.5") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.10") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "0akkpwhp27fg5fpy4qjx3l2mhrihjby078shlrkjbc2h742r1xkl")))

(define-public crate-wei-0.1.12 (c (n "wei") (v "0.1.12") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.5") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.10") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "092w99kvxdbpfmqq2xjj2dvwqpd6qwqkvygzyhvn6w4cyjjkmlga")))

(define-public crate-wei-0.1.13 (c (n "wei") (v "0.1.13") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.15") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.17") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "1kqbaypdc48148wwdi9rf915jwcxdjmycvn2f1qibch751raab8r")))

(define-public crate-wei-0.1.19 (c (n "wei") (v "0.1.19") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.16") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.18") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "06r8vmihnmkgnhpn8gcv5dnjia0yxlmg9h35q3sb63ki07idg5bh")))

(define-public crate-wei-0.1.32 (c (n "wei") (v "0.1.32") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.19") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.22") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "0h49jx0d7c4f2kf760b59czsp5nya6q8jqkyjaszl03jjax7z97m")))

(define-public crate-wei-0.1.33 (c (n "wei") (v "0.1.33") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.19") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.22") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "1z3djhwkj6ww3j6d7qvb7xfx7lwvpmwms855jl7l1kfj0iw40acf")))

(define-public crate-wei-0.1.35 (c (n "wei") (v "0.1.35") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.19") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.22") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "1jsiskiwy3nfvlw5x31a1gmcsd321sf0i3mx7xrznm428cwjhlin")))

(define-public crate-wei-0.1.37 (c (n "wei") (v "0.1.37") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.19") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.22") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "1qy4d7j7fz4n8fzrys9v6ffhzr9hq0fq4sr2710xxsxvad3icvpz")))

(define-public crate-wei-0.2.4 (c (n "wei") (v "0.2.4") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "single-instance") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wei-daemon") (r "^0.2.20") (d #t) (k 0)) (d (n "wei-env") (r "^0.2.10") (d #t) (k 0)) (d (n "wei-log") (r "^0.2.5") (d #t) (k 0)) (d (n "wei-run") (r "^0.1.23") (d #t) (k 0)) (d (n "winres") (r "^0.1") (d #t) (k 1)))) (h "069vfhvrmcjh0xlp2d44n7c53c4zn9w8dw2j2yzpw4k1rka095hd")))

