(define-module (crates-io #{3}# w wdl) #:use-module (crates-io))

(define-public crate-wdl-0.0.0 (c (n "wdl") (v "0.0.0") (h "0sbx1z835ql2z5hy15r6cj7ac0ib984axjmmbljc9r2k28s88d4y")))

(define-public crate-wdl-0.1.0 (c (n "wdl") (v "0.1.0") (d (list (d (n "wdl-grammar") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "17ywhjvmcpdwsyjhq8dl4rzw763mxpiydhcjlvg80pq9kl1cph32") (f (quote (("default")))) (s 2) (e (quote (("grammar" "dep:wdl-grammar"))))))

(define-public crate-wdl-0.2.0 (c (n "wdl") (v "0.2.0") (d (list (d (n "wdl-ast") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "wdl-core") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "wdl-grammar") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1rfaggl7cv0ynkvnn1pg8cyy8dl5r0p5z83l7lj5fva2vwbmymws") (f (quote (("default")))) (s 2) (e (quote (("grammar" "dep:wdl-grammar") ("core" "dep:wdl-core") ("ast" "dep:wdl-ast"))))))

