(define-module (crates-io #{3}# w wll) #:use-module (crates-io))

(define-public crate-wll-0.1.0 (c (n "wll") (v "0.1.0") (d (list (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wll-macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wll-sys") (r "^0.1") (d #t) (k 0)))) (h "0b2nfkyw6vlpmkmfy45ig6j2dc2qwlj5mv457lrg5hi7j9fqdkyz") (f (quote (("macros" "wll-macros") ("default") ("auto-link" "wll-sys/auto-link")))) (y #t)))

(define-public crate-wll-0.1.1 (c (n "wll") (v "0.1.1") (d (list (d (n "trybuild") (r "^1.0") (f (quote ("diff"))) (d #t) (k 2)) (d (n "wll-macros") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wll-sys") (r "^0.1") (d #t) (k 0)))) (h "15z436rd5mpsafaxv0065zi1jrqkq0057ab953b2dwcakzr2ihrs") (f (quote (("macros" "wll-macros") ("default") ("auto-link" "wll-sys/auto-link"))))))

