(define-module (crates-io #{3}# w wag) #:use-module (crates-io))

(define-public crate-wag-0.1.0 (c (n "wag") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1d34if2c1p83lp5q28s9q6h25hpnyqz3h6fhf6zlnk70rdy3bvg1")))

(define-public crate-wag-0.2.0 (c (n "wag") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1jxn0p7w3jybl2rpph2qjn6i9pnmmgf3xg70kf8l5q1h7cn3whxj")))

(define-public crate-wag-0.3.0 (c (n "wag") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1j4bmmnjlziqccpi9wy0y296cgbp5fqyvdk8d2qxg586fiiczvfg")))

