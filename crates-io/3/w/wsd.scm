(define-module (crates-io #{3}# w wsd) #:use-module (crates-io))

(define-public crate-wsd-0.0.0 (c (n "wsd") (v "0.0.0") (h "15sllr1yk8vx6nxra7ygf7m3k6lls389wgvssjx81dfiblli7qg9")))

(define-public crate-wsd-0.0.1 (c (n "wsd") (v "0.0.1") (h "1f1700yi3549lc8rcfbkjwyaa5slqqggvd3m8l4li0zi900219a9")))

(define-public crate-wsd-0.0.2 (c (n "wsd") (v "0.0.2") (h "1abx26qzhldqp00k220bi6xcf53bi5jlm5k5b6gk0yjk7ni6a7za")))

(define-public crate-wsd-0.0.3 (c (n "wsd") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "gzip"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04wf7a7ls26i32jmdnbb03b8cifizvh72lh5jj5yk8fncixhymgs")))

(define-public crate-wsd-1.0.0 (c (n "wsd") (v "1.0.0") (d (list (d (n "native-json") (r "^1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "0z7kqayzgarkcha6s5nwwfmjxb96c3ndrplq679wxv7g2clb2ras")))

(define-public crate-wsd-1.0.1 (c (n "wsd") (v "1.0.1") (d (list (d (n "native-json") (r "^1.0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "07h0n3sgdj2z580k9pbcpbzk0s8fvx0drs1792hhpfdjmizk6pd6")))

(define-public crate-wsd-1.1.0 (c (n "wsd") (v "1.1.0") (d (list (d (n "native-json") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1mqvn5kswdjl9pglya6z8pw3c563q7yp3mydp2swlm0dmpbf96z2")))

(define-public crate-wsd-1.1.1 (c (n "wsd") (v "1.1.1") (d (list (d (n "native-json") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json" "gzip"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)))) (h "1lw4jis02nzlhl7y6axbk008jj8ai29zsn1cpywvqz6lhzl02ksv")))

