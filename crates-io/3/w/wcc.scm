(define-module (crates-io #{3}# w wcc) #:use-module (crates-io))

(define-public crate-wcc-0.1.0 (c (n "wcc") (v "0.1.0") (h "009bg7ymp103kjh6pb6md1c397gykv2dzv22fjwn9abq2hph8adw")))

(define-public crate-wcc-1.0.0 (c (n "wcc") (v "1.0.0") (h "0mngv1xhivdv2hsa788ni3wpj7ah4pha6gy7963lcj5agysnc8i3")))

(define-public crate-wcc-1.0.1 (c (n "wcc") (v "1.0.1") (h "1fnl6qc7pf46dl3zrp62zqpg4d60mmwy15agaabjbpjyqw54rl55")))

(define-public crate-wcc-1.0.2 (c (n "wcc") (v "1.0.2") (h "0618s2d3xfmkc3gfhn3q82p4gsacvvi2zyb2gpvhikm4r4nrq7d0")))

(define-public crate-wcc-1.0.3 (c (n "wcc") (v "1.0.3") (h "13pasa81jvvkaxaj5fdixxh9qbjpa17hd1j0was8frg32rvn54xy")))

(define-public crate-wcc-1.0.4 (c (n "wcc") (v "1.0.4") (h "0ldgm7h3yhfw78m1997cfhnwxkyhkbx20pcs70xw2x7w9pld8w30")))

(define-public crate-wcc-1.0.5 (c (n "wcc") (v "1.0.5") (h "0g8msxxffl08mf8hg2g07mfdk412apwz4jgmj71vpjdg6p01z2fb")))

(define-public crate-wcc-1.0.6 (c (n "wcc") (v "1.0.6") (h "0i0cfizq9k94pibvadnmzrbdlx2czgxify15a4yy7301mdn4nphl")))

(define-public crate-wcc-1.0.7 (c (n "wcc") (v "1.0.7") (h "1l5lj7frnxlvvwqs1p631xx0mzs5kyfc63znlniraazyz1yy68cm")))

(define-public crate-wcc-1.0.8 (c (n "wcc") (v "1.0.8") (h "1dzzrh8v6c90j9vsh6avkq73x2ynm16a7fg0y9cagr1qgkl7c8cs")))

(define-public crate-wcc-1.0.9 (c (n "wcc") (v "1.0.9") (d (list (d (n "regex") (r "^1.9.0") (d #t) (k 0)))) (h "0x9bnqr5q3ljnq6phllm7cwkay5zp2m9559m10qjy58b75hai1qw")))

(define-public crate-wcc-1.0.10 (c (n "wcc") (v "1.0.10") (d (list (d (n "regex") (r "^1.9.0") (d #t) (k 0)))) (h "0sk0g2r96lzh3hxgg5vdyn1y3i5phm7bf4h83qcihhdm0gmb9wba")))

(define-public crate-wcc-1.0.11 (c (n "wcc") (v "1.0.11") (d (list (d (n "regex") (r "^1.9.0") (d #t) (k 0)))) (h "0vfinbhrlik5cqxdsa1p0wxvna4fjnlaqr8mxk962118d2z2jvab")))

