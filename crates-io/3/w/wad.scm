(define-module (crates-io #{3}# w wad) #:use-module (crates-io))

(define-public crate-wad-0.1.0 (c (n "wad") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1bp7l6dlpkrrgaydxppz04fx6iypw78iqy8frbkh0pbbasbxi118")))

(define-public crate-wad-0.2.0 (c (n "wad") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0lscjl6bslp1m2kdg63jx4i9v2f7n8rpz0w4f0ii4xk4vjxfdcw8")))

(define-public crate-wad-0.2.1 (c (n "wad") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1jzjx6swa0qcvzs9rlmhic9m5f9lkpq9bs4asq3sngddp9jc9nvl")))

(define-public crate-wad-0.3.0 (c (n "wad") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1d0z6shgrd6h5j0ks6kdx5i7y97gny131jakwbngapqxpsfjhfdc")))

(define-public crate-wad-0.3.1 (c (n "wad") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1whyvf9sfjgayq294m06vxakr25d87li2yzpig42xqw15p5x9f4l")))

(define-public crate-wad-0.3.2 (c (n "wad") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "17daw7ilwmmjqwc4g0p28fppm2g9zqyb8ca786ibafp3xxs6i8fc")))

