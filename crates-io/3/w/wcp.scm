(define-module (crates-io #{3}# w wcp) #:use-module (crates-io))

(define-public crate-wcp-0.1.0 (c (n "wcp") (v "0.1.0") (d (list (d (n "http_io") (r "^0.2.5") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "17y8gj8958j206sc3dgks7wv4nz7lr2ydq3gkvpyfzfjkqynkakp")))

(define-public crate-wcp-0.1.1 (c (n "wcp") (v "0.1.1") (d (list (d (n "http_io") (r "^0.2.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "05nzzpf9nx6v1j664vh9ps6k6x9rgnnvc9sh21s3gz1g3m8d2xvi")))

(define-public crate-wcp-0.1.2 (c (n "wcp") (v "0.1.2") (d (list (d (n "http_io") (r "^0.2.8") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1kqmqdidhz2ma9nv0159r9vhm9n1d21sc74l7rzqk0v4ylx2bijs")))

(define-public crate-wcp-0.1.3 (c (n "wcp") (v "0.1.3") (d (list (d (n "http_io") (r "^0.2.14") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "17vg9ws75f316304bi2c9fsqf9yrw9m0mv9cs8ky87w7gbac09yf")))

(define-public crate-wcp-0.1.4 (c (n "wcp") (v "0.1.4") (d (list (d (n "http_io") (r "^0.2.15") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1yngcmq4zw0xwz971qig67g9wis0vycim6p7j6rynwsvh692i8mf")))

(define-public crate-wcp-0.1.5 (c (n "wcp") (v "0.1.5") (d (list (d (n "http_io") (r "^0.2.16") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.7") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1pkbik6g00ic5n314irpa215rh6kgfvaflgl2idmkn8s3zh6g5lz")))

