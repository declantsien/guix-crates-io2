(define-module (crates-io #{3}# w wee) #:use-module (crates-io))

(define-public crate-wee-0.1.0 (c (n "wee") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1p2nccwrpzns0jn2rb75w62p6a6fxlnbi690q59k8dd0ad2daznd")))

(define-public crate-wee-0.2.0 (c (n "wee") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "run_script") (r "^0.6.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.4") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "188c25rjlvni2bgx86jwlnd9yjwlj9k8nws65bd7x4pvncrppzc6")))

(define-public crate-wee-0.3.0 (c (n "wee") (v "0.3.0") (d (list (d (n "clap") (r "^3.0.12") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.8") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1h9xgj85114a2ixxx02fkayb1llfv43x1s0syrrh6r6ixi8g20si")))

