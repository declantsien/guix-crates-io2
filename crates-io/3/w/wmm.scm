(define-module (crates-io #{3}# w wmm) #:use-module (crates-io))

(define-public crate-wmm-0.1.1-alpha.1 (c (n "wmm") (v "0.1.1-alpha.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "time") (r "^0.2.16") (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 2)))) (h "1p9ich9yzqriycf68gviz162bfk61hzncn3bh5nz0vsaw745cqfy")))

(define-public crate-wmm-0.1.1-beta.1 (c (n "wmm") (v "0.1.1-beta.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)) (d (n "time") (r "^0.2.16") (k 0)) (d (n "time") (r "^0.2.16") (d #t) (k 2)))) (h "1sw08bnkf99mq7yblmbr2bvp9xz0nbdhhjaikrljz9wvj4sn6c1z")))

(define-public crate-wmm-0.1.1 (c (n "wmm") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.59") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (d #t) (k 0)) (d (n "time") (r "^0.3.2") (k 0)))) (h "0vlpw4kyczi3iy8pc4f66z4zdxn08bs1mg6a5f2hd9wspz9ckalz")))

(define-public crate-wmm-0.1.2 (c (n "wmm") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (d #t) (k 0)) (d (n "time") (r "^0.3.4") (k 0)))) (h "1f33lll3xadccqq2vs8ip8qiqlk85gga2313dg3i3i7kyq1ly2qj")))

(define-public crate-wmm-0.2.0 (c (n "wmm") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.71") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (d #t) (k 0)) (d (n "time") (r "^0.3.4") (k 0)))) (h "1il1lj1gfjp492z5x6gzrrwjlk4h987z245l31ihm4d19kpj583i")))

(define-public crate-wmm-0.2.1 (c (n "wmm") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "cty") (r "^0.2.2") (d #t) (k 0)) (d (n "spin") (r "^0.9.2") (d #t) (k 0)) (d (n "time") (r "^0.3.7") (k 0)))) (h "1ihm89sz38sz84ik7q88y9nny0vbbfwbyd5827xlkqlr9hlp71sw")))

(define-public crate-wmm-0.2.2 (c (n "wmm") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "136rdh8ari2h607g5a5rs4g6sz28zr832ccbc6qp8mxfb7k682dp")))

(define-public crate-wmm-0.2.3 (c (n "wmm") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "spin") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.3") (k 0)))) (h "0vszip0mczblmccrzcab0g7r19s6qagi2jf559pix67jaxrn7qvz")))

