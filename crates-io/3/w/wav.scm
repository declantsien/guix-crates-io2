(define-module (crates-io #{3}# w wav) #:use-module (crates-io))

(define-public crate-wav-0.1.0 (c (n "wav") (v "0.1.0") (d (list (d (n "riff") (r "^0.1") (d #t) (k 0)))) (h "0ba7icjvcdwx5a6v5v6ardi6w9yqrdsrp6zqkfbn6vf420gdh2n7")))

(define-public crate-wav-0.1.1 (c (n "wav") (v "0.1.1") (d (list (d (n "riff") (r "^0.1") (d #t) (k 0)))) (h "0ifa423f0nm8shwzy5ghwj5s9jzlvj7rhxvckpqimbh1pvfcsg9k")))

(define-public crate-wav-0.2.0 (c (n "wav") (v "0.2.0") (d (list (d (n "riff") (r "^0.1") (d #t) (k 0)))) (h "04rk7nwiikfihrpb5yfmg49f009y4qdwlgrjb3pa6wr0dw0yj0bn")))

(define-public crate-wav-0.3.0 (c (n "wav") (v "0.3.0") (d (list (d (n "riff") (r "^0.1") (d #t) (k 0)))) (h "18hvv1dbqrlak3pc7k8b3idxx4is2ha74jdybsgfcwq3d22gmsxn")))

(define-public crate-wav-0.4.0 (c (n "wav") (v "0.4.0") (d (list (d (n "riff") (r "^0.1") (d #t) (k 0)))) (h "1p7d8byl8i8dpdh6vp55pbgn3nsngsixzb8sf8m2nw9l202r3bl9")))

(define-public crate-wav-0.4.1 (c (n "wav") (v "0.4.1") (d (list (d (n "riff") (r "^0.1") (d #t) (k 0)))) (h "0q0j5f2z7j9b3b1s0ibq1pxkyx9qrcccbdhx29xf6kbq4qyysynx")))

(define-public crate-wav-0.4.2 (c (n "wav") (v "0.4.2") (d (list (d (n "riff") (r "^0.1") (d #t) (k 0)))) (h "157rd2z6cr4zblf0d7ybhpnckcf4iqd4pbfna5vg01pazaj715rf") (y #t)))

(define-public crate-wav-0.5.0 (c (n "wav") (v "0.5.0") (d (list (d (n "riff") (r "^1.0") (d #t) (k 0)))) (h "0lgsdanfi5766sr6799dlqpkf8lvixrh3klncr4m2gmhzbfxfmsg")))

(define-public crate-wav-0.6.0 (c (n "wav") (v "0.6.0") (d (list (d (n "riff") (r "^1.0") (d #t) (k 0)))) (h "030b72hcs6p09i7kj5zjwa8cgsdva6s9w5cz90d907ry0xzgk7al")))

(define-public crate-wav-1.0.0 (c (n "wav") (v "1.0.0") (d (list (d (n "riff") (r "^1.0") (d #t) (k 0)))) (h "028npq8g88qa5zz9f7qr807zhd60fgba8ah7jzwv8j4qg6f1jpm6")))

(define-public crate-wav-1.0.1 (c (n "wav") (v "1.0.1") (d (list (d (n "riff") (r "^1.0") (d #t) (k 0)))) (h "13b9l9fqmhg86axfsla24bba2mgls59amwipxiwvaxcqyq179ncr")))

