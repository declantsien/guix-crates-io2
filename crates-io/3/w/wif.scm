(define-module (crates-io #{3}# w wif) #:use-module (crates-io))

(define-public crate-wif-0.1.0 (c (n "wif") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "openssl") (r "^0.10.42") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cw511gq8ln2fabl5fqkivhbjsvjywldhx662jjf77f57lbby65i")))

