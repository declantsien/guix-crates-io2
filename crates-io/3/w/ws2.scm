(define-module (crates-io #{3}# w ws2) #:use-module (crates-io))

(define-public crate-ws2-0.1.0 (c (n "ws2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "0j0irfpmaxw6w9x065nbag0k381i2mg45szwr1frjpq8qsi4cjda")))

(define-public crate-ws2-0.1.1 (c (n "ws2") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "12j49w20190a22gipclrgx7824kwfbayfmlbl617kr6i339lnppv")))

(define-public crate-ws2-0.1.2 (c (n "ws2") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1hzws9sxallrw4ydc9f99gmhp4cvafjr5ri9gxk07k94g7s748gk")))

(define-public crate-ws2-0.1.3 (c (n "ws2") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1i28dk5ivg2gdw99jc6ni68ja3xp87qj198l3imr38ffh5fkd58h")))

(define-public crate-ws2-0.1.4 (c (n "ws2") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1am76n26b3d7nzlbdr0wwhz4v53cgvz82hh8a4sqmmfdy4wkblyv")))

(define-public crate-ws2-0.1.5 (c (n "ws2") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1a574vhz9i252h2pdlgcr5yxy7iv2blhcgmafmalk0mxvd9h3zyf")))

(define-public crate-ws2-0.1.6 (c (n "ws2") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "18qqkc6m97hx4vhjnnb3i1pfvgrm4ivrfpqgriyxmdgk0p1b30l4")))

(define-public crate-ws2-0.1.7 (c (n "ws2") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1assijn5mcqxc05dw0awzf20kpgg5cfzi52qf1jr7fli5ki766qj")))

(define-public crate-ws2-0.1.8 (c (n "ws2") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "0iibywv48rmj4d2dhrrn2iwkl9sbnn04gg4xh83wzqf900xiaxic")))

(define-public crate-ws2-0.1.9 (c (n "ws2") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1cmnhz3xgib0a9afam2xz6shvhnwh91aab103sfs772akyh4k7i8")))

(define-public crate-ws2-0.2.0 (c (n "ws2") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "poll-channel") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "0fm8grxxp6688g5rvf35c1p0yljvksqlcy8bl8245gp4zqx0vl22")))

(define-public crate-ws2-0.2.1 (c (n "ws2") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "poll-channel") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "06hipcl6j85pwjvlrkdvwspj64i9hzk55q35hw13x8nhisahi92h")))

(define-public crate-ws2-0.2.2 (c (n "ws2") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "poll-channel") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1vj20y7ly5bh9x6whb93mm6zi4xwqc1jmqf157wblfcks42d5a3g")))

(define-public crate-ws2-0.2.3 (c (n "ws2") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "poll-channel") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "1gi9w78lmhj86rj4ajw62739rk0i6z2bparp4in5spk71mhqf27b")))

(define-public crate-ws2-0.2.4 (c (n "ws2") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "poll-channel") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "0vnm2fw9p7l9lkfn9m9jc7cd4rmf5l9d7k0zbp1w1y29czax8yra")))

(define-public crate-ws2-0.2.5 (c (n "ws2") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "log2") (r "^0.1.7") (d #t) (k 0)) (d (n "poll-channel") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (d #t) (k 0)) (d (n "ws") (r "^0.9.2") (f (quote ("ssl"))) (d #t) (k 0)))) (h "04di7cld7cc470fsq4lssj8pdj9g416wh2pj9jk9q46kw6yxc27w")))

