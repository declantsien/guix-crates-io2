(define-module (crates-io #{3}# w wfd) #:use-module (crates-io))

(define-public crate-wfd-0.1.0 (c (n "wfd") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r ">= 0.3.8") (f (quote ("winuser" "objbase" "shobjidl" "shobjidl_core" "winerror" "shellapi"))) (d #t) (k 0)))) (h "05fkrgzxqdx9llmmy5ys7560d9bh95xyj83cj37zn0x909sh7vgq")))

(define-public crate-wfd-0.1.1 (c (n "wfd") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r ">= 0.3.8") (f (quote ("winuser" "objbase" "shobjidl" "shobjidl_core" "winerror" "shellapi"))) (d #t) (k 0)))) (h "0lh1jb6nx6mxb6v96h70gg667z2k97qm1lhx3k6wrpp9g1wld5hg")))

(define-public crate-wfd-0.1.2 (c (n "wfd") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r ">= 0.3.8") (f (quote ("winuser" "objbase" "shobjidl" "shobjidl_core" "winerror" "shellapi"))) (d #t) (k 0)))) (h "0bhwd09wv980sgrgj234hiaryq74adjzj67hv67pswjr3pg5ifz9")))

(define-public crate-wfd-0.1.3 (c (n "wfd") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r ">= 0.3.8") (f (quote ("winuser" "objbase" "shobjidl" "shobjidl_core" "winerror" "shellapi"))) (d #t) (k 0)))) (h "0b3cb48vbcmqan1wymzyrzc9jx6vdi9r7gkfklnfplgzn2v0paja")))

(define-public crate-wfd-0.1.4 (c (n "wfd") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.8") (f (quote ("winuser" "objbase" "shobjidl" "shobjidl_core" "winerror" "shellapi"))) (d #t) (k 0)))) (h "1lxix0f9fq0pmzl0mpz9k711dzrf1hrwf36kxi8dlvv1a0gk5p16")))

(define-public crate-wfd-0.1.5 (c (n "wfd") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.9") (f (quote ("winuser" "objbase" "shobjidl" "shobjidl_core" "winerror" "shellapi"))) (d #t) (k 0)))) (h "1ca69ghaq4rvq36pzji89z2c3mf94szbbb421nrc8ry1hrxyrgcv") (y #t)))

(define-public crate-wfd-0.1.6 (c (n "wfd") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.9") (f (quote ("winuser" "objbase" "shobjidl" "shobjidl_core" "winerror" "shellapi"))) (d #t) (k 0)))) (h "03h2gfyqkm3rmbr74finbqmnb6pvh3gr3n77ivayvb8chmmirwya")))

(define-public crate-wfd-0.1.7 (c (n "wfd") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r ">=0.3.9") (f (quote ("winuser" "objbase" "shobjidl" "shobjidl_core" "winerror" "shellapi"))) (d #t) (k 0)))) (h "1a6p8651l0q9dvzipam91nwv56n2ijxfpqg318dbzrdacw5h84z7")))

