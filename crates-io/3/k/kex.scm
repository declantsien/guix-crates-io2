(define-module (crates-io #{3}# k kex) #:use-module (crates-io))

(define-public crate-kex-0.1.0 (c (n "kex") (v "0.1.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)))) (h "189blcl5lczqn9s5q49wgj1bkajf9skq9fbr9pys9i6aa4ns69n8")))

(define-public crate-kex-0.1.1 (c (n "kex") (v "0.1.1") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)))) (h "024any8q4a921hx85rvm6rhd666bb0rs9mz2v5sp38irdgdkxydh")))

(define-public crate-kex-0.1.2 (c (n "kex") (v "0.1.2") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)))) (h "19rajiyz5bf8ph1jf423zij9r5p6b9i4ffir7r3250ral4iqjybp")))

(define-public crate-kex-0.1.3 (c (n "kex") (v "0.1.3") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)))) (h "1kzi02cvxn0zdxkbqv4v2fim6n675566visk0w3g8d70p7157xfj")))

(define-public crate-kex-0.1.4 (c (n "kex") (v "0.1.4") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)))) (h "0f90vgpszkjr4m052r345iz0nfql6mrkfqhfy6hp08qahpqykm97")))

(define-public crate-kex-0.1.5 (c (n "kex") (v "0.1.5") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)))) (h "03aksdz3pyy4wn2g9wwy4ykm3x7rklsqqzvi0yk5q606sribn47j")))

(define-public crate-kex-0.1.6 (c (n "kex") (v "0.1.6") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)))) (h "1xfjr8vsqqp66828zl3jc4vg36fbwcd07i5qs0cffggk8g8pcvcq")))

(define-public crate-kex-0.1.7 (c (n "kex") (v "0.1.7") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)))) (h "1crp5kcxf3disr4wxa7kqf3pfz0g9dyn2q5rz6fjm45p23vpa0gv")))

(define-public crate-kex-0.1.8 (c (n "kex") (v "0.1.8") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)))) (h "0514rpz7y960000mdybfl034153pdqqva3qjsklp2dzjvic3r6dh")))

(define-public crate-kex-0.1.9 (c (n "kex") (v "0.1.9") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)))) (h "131ipbzh014plcggvi29hxggcvmxp170a39ci2rz1day4q9frj4g")))

(define-public crate-kex-0.2.0 (c (n "kex") (v "0.2.0") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "1ywdadk3ddxh0dbqvy6h1qkwsi6xhq2wv8cwgr8gsfqn75rbvdfz")))

(define-public crate-kex-0.2.1 (c (n "kex") (v "0.2.1") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "07b3x76i45ps0w8jyj876pj0nvxc46nmad7vd30idsvnrzav38r3")))

(define-public crate-kex-0.2.2 (c (n "kex") (v "0.2.2") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "1fmgqwcs7kr3z1yiz7q0l4xmr6gsxak612146x7afi3mq9vi9cii")))

(define-public crate-kex-0.2.3 (c (n "kex") (v "0.2.3") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "0igjajm9k828dyqw54y2css0kb28j8w47376d3cqy8bpbnr4m4mb")))

(define-public crate-kex-0.2.4 (c (n "kex") (v "0.2.4") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "0kz17y7mrzwa9n7y3nsg01zx7xbspapv1yp1q0fa1vccqnppbr7f")))

(define-public crate-kex-0.2.5 (c (n "kex") (v "0.2.5") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "0zwfaj79b3y7s0rz0zj1shdbq1w3n4hbifmdglr94ix57chwf10h")))

(define-public crate-kex-0.2.6 (c (n "kex") (v "0.2.6") (d (list (d (n "ascii") (r "^1.1.0") (d #t) (k 0)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)))) (h "14shvcl9365p0jcpsr32svabxj1p299r4lrr2a2g3lds2ql59hkp")))

