(define-module (crates-io #{3}# k kon) #:use-module (crates-io))

(define-public crate-kon-0.1.0 (c (n "kon") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "viuer") (r "^0.6.2") (d #t) (k 0)))) (h "1a7kcqgk3wbjdmgiqpchd042qi9p4f7ccz16nhgc1rpjrx7fbhvj")))

(define-public crate-kon-0.1.1 (c (n "kon") (v "0.1.1") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "viuer") (r "^0.6.2") (d #t) (k 0)))) (h "1kbc6jmxlmykm1wn3rz1nrgmffaycpnw8zvcc6i32syn7g3yxyxb")))

