(define-module (crates-io #{3}# k kvs) #:use-module (crates-io))

(define-public crate-kvs-0.0.0 (c (n "kvs") (v "0.0.0") (h "1z86qa3yiwfrahpqq2pjnjwg4kz9w28iksfrp3ckrirsj9b93pz5")))

(define-public crate-kvs-0.0.1 (c (n "kvs") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.3.2") (k 0)) (d (n "hash32") (r "^0.1.1") (d #t) (k 0)))) (h "09k0y071ys6s83c20vgd3hkf04zqdpgppvmkld242paxxca1rix4")))

(define-public crate-kvs-0.0.2 (c (n "kvs") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "hash32") (r "^0.2.1") (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)))) (h "1jlmx4w829vwkj7gx8p3wqxw6w8mhm89c7i8i496xlymbkayspf7")))

(define-public crate-kvs-0.0.3 (c (n "kvs") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "hash32") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (o #t) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "postcard") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (o #t) (k 0)))) (h "1ma5c4djfpg4dyzskrb8bxf5ihasb25p9z9ly80yhmi15idlar5w") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:postcard" "dep:heapless"))))))

(define-public crate-kvs-0.0.4 (c (n "kvs") (v "0.0.4") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "hash32") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (o #t) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "postcard") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (o #t) (k 0)))) (h "0q3c6d9mramk8pr9il58ds8fr886kpf3g7zbgb1cri52arz6jr93") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:postcard" "dep:heapless"))))))

(define-public crate-kvs-0.0.5 (c (n "kvs") (v "0.0.5") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "hash32") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (o #t) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "postcard") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (o #t) (k 0)))) (h "1mx1gip2ld1bjl5rvi4ds7l48ifgllindkh5msrwavyb5r8c6pmp") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:postcard" "dep:heapless"))))))

(define-public crate-kvs-0.0.6 (c (n "kvs") (v "0.0.6") (d (list (d (n "byteorder") (r "^1.4.3") (k 0)) (d (n "embedded-hal") (r "^0.2.4") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "hash32") (r "^0.3.0") (d #t) (k 0)) (d (n "heapless") (r "^0.7.15") (o #t) (d #t) (k 0)) (d (n "modular-bitfield") (r "^0.11.2") (d #t) (k 0)) (d (n "postcard") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (o #t) (k 0)))) (h "07xvv926l50xshv19m0j7v2s5csxf2n2xr1617sh49nnfvmwmfqd") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde" "dep:postcard" "dep:heapless"))))))

