(define-module (crates-io #{3}# k kvu) #:use-module (crates-io))

(define-public crate-kvu-0.1.0 (c (n "kvu") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "1zfwhhcag65nbhdyf95c46jh7qcl2i7czflrva4gj4xbfasrwfs8")))

(define-public crate-kvu-0.1.1 (c (n "kvu") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "01ngafg83pqgglkj3lrd9h3x1vdm6i29xmrl6hjfkfv10dq29hh0")))

(define-public crate-kvu-0.1.2 (c (n "kvu") (v "0.1.2") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "1b26wvjkr9865dcrnd0vyxfp8hix4cgd6llyhjglcq2dm5w4bbqv")))

(define-public crate-kvu-0.1.3 (c (n "kvu") (v "0.1.3") (d (list (d (n "assert_cmd") (r "^2.0.4") (d #t) (k 2)) (d (n "getopts") (r "^0.2.21") (d #t) (k 0)) (d (n "indoc") (r "^1.0.6") (d #t) (k 2)) (d (n "predicates") (r "^2.1.1") (d #t) (k 2)))) (h "127hwnnhgyy5yk7l6873p6isasmc4q51hb1z0ni0gl9r22xrpzi3")))

