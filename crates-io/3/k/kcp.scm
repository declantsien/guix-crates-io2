(define-module (crates-io #{3}# k kcp) #:use-module (crates-io))

(define-public crate-kcp-0.1.0 (c (n "kcp") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)))) (h "01z5189xaqvnp3id9vpq1qsyn4yp1d8zvjkp9pljgiv551lff434")))

(define-public crate-kcp-0.1.1 (c (n "kcp") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "03vway5gdr6bjg6cdghd3qdc2qnjrkg25818w94xy67kgixy63q0")))

(define-public crate-kcp-0.1.2 (c (n "kcp") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "0h4wwwagchr944fji81racpm6v81snxhc7srb85h7qnm54pif6m4")))

(define-public crate-kcp-0.1.3 (c (n "kcp") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "15v31zvaq0lkjhsi3788a7f65sryxvgsnsh1dz5kx79by1w2b52m")))

(define-public crate-kcp-0.1.4 (c (n "kcp") (v "0.1.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)))) (h "12amjvb0mpbpf6wkz6swrzjnpgb09rz25dx4482rqsar00shrv1g")))

(define-public crate-kcp-0.1.5 (c (n "kcp") (v "0.1.5") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0kj3bmjd0gkf9pw8qvy4zfqmjwpyq04pg39n8ki37kghkqv921y7")))

(define-public crate-kcp-0.2.0 (c (n "kcp") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1fr03fb97pg8zrz6li2k5kb9igmrlyy0gq58a9a9vnmrl2anpfi0")))

(define-public crate-kcp-0.3.0 (c (n "kcp") (v "0.3.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "055h6bwzvba4la6hf5v1qywkb6g4s94wz4cdcwb8v8s5w3662llg")))

(define-public crate-kcp-0.4.0 (c (n "kcp") (v "0.4.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "02j1ykyrcl02wkca1bzyq8qfaxb47a4kavp46in5fcfp540hcaf7")))

(define-public crate-kcp-0.4.1 (c (n "kcp") (v "0.4.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0607mvbsymbias9vkrm43rivhqiw55wvbfsp9hyzbl03xninyd9z")))

(define-public crate-kcp-0.4.2 (c (n "kcp") (v "0.4.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "16gq6ng5b5m93njj4gjdi22l3fqysj93s8w783n0nbab54x4f7vw")))

(define-public crate-kcp-0.4.3 (c (n "kcp") (v "0.4.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1bmh6r8qmfpl9mrx6x85gfzjjp347ixfr7fpr0kiyaxln1pzcx1h")))

(define-public crate-kcp-0.4.4 (c (n "kcp") (v "0.4.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "16qgv8h89x479d97ggv33xfq9lhziwsbkbk9w136k1k9nvf4r6q8")))

(define-public crate-kcp-0.4.5 (c (n "kcp") (v "0.4.5") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "0ylar9a6622a55mw1ggm87iy0vqfqhwnkrpmnrq9lblk9cai414z")))

(define-public crate-kcp-0.4.6 (c (n "kcp") (v "0.4.6") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1gp4msh9ww6ln7lb7fnq1lvxpkjnj7qrhn2gh37ahjyr37ld3fbj")))

(define-public crate-kcp-0.4.7 (c (n "kcp") (v "0.4.7") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "107phsz2025d84cqdnd7w5y9wic91k4mfgscck4kpaxpqw2q4gyr")))

(define-public crate-kcp-0.4.8 (c (n "kcp") (v "0.4.8") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "1nkbiks5my22fbwjf69b8vls8vgaqyxzqzv930khvfgqk3j25pj9")))

(define-public crate-kcp-0.4.9 (c (n "kcp") (v "0.4.9") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "08a0b8bdcllri90mkb6f9apzvdb1s18b1bssiafz82hd35x0rmhr")))

(define-public crate-kcp-0.4.10 (c (n "kcp") (v "0.4.10") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "088vr47hfwnyrri7acszmd4b1vkprbx1f0kpidyl37g0zx91wj09")))

(define-public crate-kcp-0.4.13 (c (n "kcp") (v "0.4.13") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "1n63ygxpgg77n2f2q40fdd954pw9r8n0szx7iy48infrr2pwy7rs")))

(define-public crate-kcp-0.4.14 (c (n "kcp") (v "0.4.14") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "0nlm9v40j0dvl3jq72v2izcq4nbwpz5nc92x1rwp5q2rvwmj88ic")))

(define-public crate-kcp-0.4.15 (c (n "kcp") (v "0.4.15") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "1wlpd11snmkypbi8x9d9xkagykg9j1yda4b3fcrdga391j2n5dby")))

(define-public crate-kcp-0.4.16 (c (n "kcp") (v "0.4.16") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "1r29gyk8zw022iasj7picj37fi4yq6b0yqmyhfd46pd0n8dwivka")))

(define-public crate-kcp-0.5.0 (c (n "kcp") (v "0.5.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "1rc6mm2kk2nf9kjpb4x2m26rnm1x2z1y1376hhr72r7p2pxq6pbc") (f (quote (("fastack-conserve"))))))

(define-public crate-kcp-0.5.1 (c (n "kcp") (v "0.5.1") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "116617k92ba0gzc41fqx51y0d3jw448nslnplh8pabfsavsqkicw") (f (quote (("fastack-conserve"))))))

(define-public crate-kcp-0.5.2 (c (n "kcp") (v "0.5.2") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "0fvs84acqp85yj1cjsb7k0f83cchy9qh1zwxq8wzpr5hi22qzvm3") (f (quote (("fastack-conserve")))) (y #t)))

(define-public crate-kcp-0.5.3 (c (n "kcp") (v "0.5.3") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "0s4fi86kgra4aa4visnv8iz1f31g99qmfj711cw3s1j29y97lf0f") (f (quote (("fastack-conserve"))))))

