(define-module (crates-io #{3}# k k3d) #:use-module (crates-io))

(define-public crate-k3d-0.1.0 (c (n "k3d") (v "0.1.0") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "directories") (r "^1.0.2") (d #t) (k 0)))) (h "0p8a88r555avvqnnr7rf1dnz2dl2vxiz7qs92p05yrjghzhfyhl5")))

(define-public crate-k3d-0.1.1 (c (n "k3d") (v "0.1.1") (d (list (d (n "clap") (r "^2.26.0") (d #t) (k 0)) (d (n "directories") (r "^1.0.2") (d #t) (k 0)))) (h "0jfsg6a2g164hi8h2whjck5kcckn39hcbwz7lbahb8m08qh6hjfc")))

