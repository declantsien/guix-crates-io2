(define-module (crates-io #{3}# k kai) #:use-module (crates-io))

(define-public crate-kai-0.1.0 (c (n "kai") (v "0.1.0") (h "0vgbzbkcmhshhifk9hkw6zwsivyd4j8zwww1hh9qakprhdsjp32x")))

(define-public crate-kai-0.2.0 (c (n "kai") (v "0.2.0") (h "0gxgg9nh8a5xscp0f864mb0wsa7j225ng3yxq5g5yfifsl44vzg0")))

(define-public crate-kai-0.2.1 (c (n "kai") (v "0.2.1") (h "0id8xnssw3m3j6lrlfsx1dbpwaz1hris9kzl9kikf1hixpj03935")))

(define-public crate-kai-0.3.0 (c (n "kai") (v "0.3.0") (h "0zhgznsz0nsiqij0k0y8pqmqdismsvwvgnxvmav3dfx5icnkh9ml")))

(define-public crate-kai-0.4.0 (c (n "kai") (v "0.4.0") (h "179k50jvgf1vx1cl7a02ff0l8pfg49dwcwcv7xqk4nfl7wx00gwn")))

(define-public crate-kai-0.5.0 (c (n "kai") (v "0.5.0") (h "0012aflh75jvqi92zzdca52hkrk51wb3v195fh53a6nsj5r5yg9f")))

(define-public crate-kai-0.6.0 (c (n "kai") (v "0.6.0") (h "19n5y2zcb8g8951glqy3323fjqzpwy90ffpv48xx4byd227xfi49")))

(define-public crate-kai-0.7.0 (c (n "kai") (v "0.7.0") (h "082yb92j2y91b73hy8niriv2csnzp9vmchqfl16xkijaqc14ksx0")))

