(define-module (crates-io #{3}# k kit) #:use-module (crates-io))

(define-public crate-kit-0.0.1 (c (n "kit") (v "0.0.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sokol") (r "^0.3") (d #t) (k 0)))) (h "0gb1isxiydxjqnpww1b3z6ahjnqnhbsqrxlg09m3fqrx7wr0niyl")))

(define-public crate-kit-0.0.2 (c (n "kit") (v "0.0.2") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "sokol") (r "^0.3") (d #t) (k 0)))) (h "04jq9pf2k43284ls4am0fbr09cjp0hcdhgkksb06jpgxswvl96nc")))

