(define-module (crates-io #{3}# k kth) #:use-module (crates-io))

(define-public crate-kth-0.1.0 (c (n "kth") (v "0.1.0") (d (list (d (n "index-fixed") (r "^0.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)))) (h "02blkpr6adyz82y8qsiv3yjpwybgxnrb8dmwb34zym21hq5lg976") (f (quote (("nightly"))))))

