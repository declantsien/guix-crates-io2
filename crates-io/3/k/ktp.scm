(define-module (crates-io #{3}# k ktp) #:use-module (crates-io))

(define-public crate-ktp-0.1.0 (c (n "ktp") (v "0.1.0") (d (list (d (n "bytesize") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "libmacchina") (r "^3.9.1") (d #t) (k 0)) (d (n "notify-rust") (r "^4.5.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02fkg65vk0rgh0drwxx08488xjb8gwc1qshhb8hpcbjp4mpmjzlj") (y #t)))

