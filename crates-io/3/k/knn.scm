(define-module (crates-io #{3}# k knn) #:use-module (crates-io))

(define-public crate-knn-0.1.0 (c (n "knn") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "07x4c34dkvplkhxdgwvmx4z86k9gxv507g7kd20makqhksps4d6l")))

(define-public crate-knn-0.1.1 (c (n "knn") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "09qhmyd7zxycv0ymdcs5bzs65f589bn0818sj63c4gpp50n3bpxw")))

(define-public crate-knn-0.1.2 (c (n "knn") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "07y8ir9wcv74cn6i78miy256sbayvrj60fs8q72gv5g8wc5ib90d") (y #t)))

(define-public crate-knn-0.1.3 (c (n "knn") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "07f7x2mxa3d2j8ib7h3ja2rlj2ssf6ls0yzyk0jads297xhknjf1")))

