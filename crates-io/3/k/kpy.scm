(define-module (crates-io #{3}# k kpy) #:use-module (crates-io))

(define-public crate-kpy-0.1.0-alpha (c (n "kpy") (v "0.1.0-alpha") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "dircpy-stable") (r "^0.3.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "simple_logger") (r "^1.12.1") (d #t) (k 0)) (d (n "strum") (r "^0.21.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.21.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0knigmg4x2jwywa6r33nfld5rg4670qczi8kcm94fnzw9gxkvn00")))

