(define-module (crates-io #{3}# k klu) #:use-module (crates-io))

(define-public crate-klu-0.1.0 (c (n "klu") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.8") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "15dp18p6vkqn2j0srqppc6392f0wpnxjslqixkxf5y0icazlaq0j") (f (quote (("default")))) (y #t) (s 2) (e (quote (("cli" "dep:clap"))))))

