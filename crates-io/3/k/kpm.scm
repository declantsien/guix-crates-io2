(define-module (crates-io #{3}# k kpm) #:use-module (crates-io))

(define-public crate-kpm-1.0.0 (c (n "kpm") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "sysctl") (r "^0.4") (d #t) (k 0)))) (h "024xgipi7n2q1m9dgf822lnyi4a3ng12jpc67q9al0wbhl32j263")))

