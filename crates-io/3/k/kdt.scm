(define-module (crates-io #{3}# k kdt) #:use-module (crates-io))

(define-public crate-kdt-0.0.1 (c (n "kdt") (v "0.0.1") (d (list (d (n "kdtree") (r "^0.6.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0ch6gf2znakdwwdfa28ih70wzkvfjvaksaxzij1l6b09ppj58ybx")))

(define-public crate-kdt-0.0.2 (c (n "kdt") (v "0.0.2") (d (list (d (n "kdtree") (r "^0.6.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (d #t) (k 0)))) (h "15vhqv0wqsw1bgqj42ry0l53lqyhdyiasrc6a95rpbj7czq30xvz")))

