(define-module (crates-io #{3}# k k64) #:use-module (crates-io))

(define-public crate-k64-0.0.1 (c (n "k64") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.1.1") (d #t) (k 0)) (d (n "cortex-m") (r "^0.4.3") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.3.15") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0spg03h8zk2vhgmd6m84fs72bsnb7yssjdznz688xgbybiwab1lm") (f (quote (("rt" "cortex-m-rt"))))))

(define-public crate-k64-0.1.0 (c (n "k64") (v "0.1.0") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "0m4k45gf0mppp56cldsw7c0rrqmg2k503yax6qd16p5h270r5684") (f (quote (("rt" "cortex-m-rt/device"))))))

