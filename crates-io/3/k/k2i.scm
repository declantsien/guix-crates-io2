(define-module (crates-io #{3}# k k2i) #:use-module (crates-io))

(define-public crate-k2i-0.1.0 (c (n "k2i") (v "0.1.0") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1n7qiy308qsl8n41nyp5pqhy470c5npiba5zyrw9gs4yqj6729h1")))

(define-public crate-k2i-0.1.1 (c (n "k2i") (v "0.1.1") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "076b31h1vp6c27ig4mjfkajd275zv2pxmgdhka95x5hhmbwgr2rw")))

(define-public crate-k2i-0.1.2 (c (n "k2i") (v "0.1.2") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1yrapy2clc5isz4b6ddmgaaqrsq9rzz3v4nxhzhbmc00x43xq0gh")))

(define-public crate-k2i-0.1.3 (c (n "k2i") (v "0.1.3") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0yv8apdlavk80i95frs3cnws7n1543gmwgl1xxcmnrhprii1f54v")))

(define-public crate-k2i-0.1.4 (c (n "k2i") (v "0.1.4") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0dpymzwy7xij2lzd8arq0bbabgm940pdxxwnmxjw5dxfqyqp1frh")))

(define-public crate-k2i-0.1.5 (c (n "k2i") (v "0.1.5") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "11z93s8njy2y61pdizrwyjg9qi0lzk3jkf5vh39nfyakdw663bhx")))

(define-public crate-k2i-0.1.6 (c (n "k2i") (v "0.1.6") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "01qs8dz3kvhjrxlbiak7w0xx2h1vmysc6snvxqlq3bc1ya3d21ns")))

(define-public crate-k2i-0.2.0 (c (n "k2i") (v "0.2.0") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1wrwi7slkil6wr0z22ms43aixf03v8ljw1y9h6jlidggfxap1hhx")))

(define-public crate-k2i-0.2.1 (c (n "k2i") (v "0.2.1") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0zjmsilk7ikgk1257vfyg19kbkhbpd5ljr31yip6ca9rcv30hnx1")))

(define-public crate-k2i-0.2.2 (c (n "k2i") (v "0.2.2") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "1j50ydd5n64ns9ik5caiiw1pwnc6bps3jh05dv1xxjiamzn89hgw")))

(define-public crate-k2i-0.2.3 (c (n "k2i") (v "0.2.3") (d (list (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "procps-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "router") (r "^0.5.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.24") (d #t) (k 0)))) (h "0xhbb5hb78lkd508cz5rjil6p8bdvwgdqlnv53vx2wal888rd8kw")))

