(define-module (crates-io #{3}# k kvr) #:use-module (crates-io))

(define-public crate-kvr-0.3.0 (c (n "kvr") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "ulid") (r "^0.4.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1nzms7jf69m9fyb9c48gsgfw5s3wgj7fdb83xr99mj5hiz1kdc2d")))

