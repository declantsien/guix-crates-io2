(define-module (crates-io #{3}# k kdl) #:use-module (crates-io))

(define-public crate-kdl-0.0.0 (c (n "kdl") (v "0.0.0") (h "0sq8pkv2syi5xi7mzcsypjl8ixikdvc752wjhn4rnrf74rchjibz")))

(define-public crate-kdl-1.0.0 (c (n "kdl") (v "1.0.0") (d (list (d (n "nom") (r "^6.0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "06v8fbdf7mahh0si5xr8wyvhyhp7b2r949y275wlbfkhr8apij4n")))

(define-public crate-kdl-1.1.0 (c (n "kdl") (v "1.1.0") (d (list (d (n "nom") (r "^6.0.1") (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "0f9wss7fw8939wj3cgymx6ynsdgm4hh69k18snjh191f7jsnax1g")))

(define-public crate-kdl-2.0.0 (c (n "kdl") (v "2.0.0") (d (list (d (n "nom") (r "^6.0.1") (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "02vkr72dhnck6i5i8hyk7mdagh8kvh1slhza1v31aqbix3q80p7v")))

(define-public crate-kdl-3.0.0 (c (n "kdl") (v "3.0.0") (d (list (d (n "nom") (r "^7.0.0") (k 0)) (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "10vvy6jskyz6l86i8acskmyk55lf3wa1ssfaarq85v2n2kip2h0i")))

(define-public crate-kdl-4.0.0 (c (n "kdl") (v "4.0.0") (d (list (d (n "miette") (r "^4.6.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1lm3ffdhqfq988q9isdmqdbb07n3837j62hiqvc6nknny08q1v6z")))

(define-public crate-kdl-4.1.0 (c (n "kdl") (v "4.1.0") (d (list (d (n "miette") (r "^4.6.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0l5qpsp97z4nzb529c8af6ws5hq6gx6djdv725lqcrx0hv3ayz46")))

(define-public crate-kdl-4.1.1 (c (n "kdl") (v "4.1.1") (d (list (d (n "miette") (r "^4.6.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "14fjb9i763ara67w03mmkvg2dsgqfgj6mnx91rnr1bwxzrkcaaal")))

(define-public crate-kdl-4.2.0 (c (n "kdl") (v "4.2.0") (d (list (d (n "miette") (r "^4.6.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1nqcw9qbkwriwv3r2i7lqlinccp865jn1mgg96v3h27kwczi5fpp")))

(define-public crate-kdl-4.3.0 (c (n "kdl") (v "4.3.0") (d (list (d (n "miette") (r "^4.6.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "02vy4mk41jm7hbwa0ddi8qqy7ahqwxzlkb2l8xb5qih3in00w5yh")))

(define-public crate-kdl-4.4.0 (c (n "kdl") (v "4.4.0") (d (list (d (n "miette") (r "^5.3.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1hf199if36ga3cygr7jxyqypgkfsfgbh8n05yzklbk000iyi67q3")))

(define-public crate-kdl-4.5.0 (c (n "kdl") (v "4.5.0") (d (list (d (n "miette") (r "^5.3.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "19kljlwdhfyidz0g1vgwrrjsrd25rmplfknrpccdwbhf3qvqlf58") (f (quote (("span") ("default" "span"))))))

(define-public crate-kdl-4.6.0 (c (n "kdl") (v "4.6.0") (d (list (d (n "miette") (r "^5.3.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0ixfa7lyij6iars58r633164dm14707a816883ypddnch9a8fb06") (f (quote (("span") ("default" "span"))))))

(define-public crate-kdl-5.0.0-alpha.0 (c (n "kdl") (v "5.0.0-alpha.0") (d (list (d (n "miette") (r "^5.5.0") (d #t) (k 0)) (d (n "miette") (r "^5.5.0") (f (quote ("fancy"))) (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1hbdal2z3y807ll3vzyfpjdfsnp4swnc5nhmpg31kl1gm2p270ca") (f (quote (("span") ("default" "span"))))))

(define-public crate-kdl-5.0.0-alpha.1 (c (n "kdl") (v "5.0.0-alpha.1") (d (list (d (n "miette") (r "^5.7.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "miette") (r "^5.7.0") (f (quote ("fancy"))) (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1j2pwpkccnvc1abi64gjphd99bccz8asi2c0hmzcc6l82krcmvhi") (f (quote (("span") ("default" "span"))))))

