(define-module (crates-io #{3}# k khi) #:use-module (crates-io))

(define-public crate-khi-0.4.0 (c (n "khi") (v "0.4.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "0420gsxbd8hn6wxxhk017pr376mdpjvzlx78mi6iiiva0r8kmshn") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.5.0 (c (n "khi") (v "0.5.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "0lr26zm3k3zmxzarbcmbmlflhipsn1xiwdbz8i1k44fzlr51hy22") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.6.0 (c (n "khi") (v "0.6.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "0mk4djl3srns3b9jg6c566p430j8wnxrlyqc0r4rjsb7gkzsdy4c") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.7.0 (c (n "khi") (v "0.7.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (d #t) (k 0)))) (h "048rzhawq1mqshxhh4hxf5l8ag2jyi53asp91jz2mpvp1003bx33") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.8.0 (c (n "khi") (v "0.8.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "0mimwvfdlq1hr2rlap5x75b0sr0glzk8aq23qxgm77f66m70l6m9") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.9.0 (c (n "khi") (v "0.9.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "1nk4rq0xzbmhrnasbqz90bpanrj3swvkrb7kmxfsql1aaa9mdhpf") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.9.1 (c (n "khi") (v "0.9.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "0pzlh2d5j8kqyk0a6k3d2zvia9q97b5476h7z3xjvx58nv5lajz1") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.9.2 (c (n "khi") (v "0.9.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "13kmwxa3nm7dr12ll2zggsa4ssvads2iacgcy0bjmjs6pw34bwpi") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.9.3 (c (n "khi") (v "0.9.3") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "0f1wdgjbjbaz97slqklvpvzllcflxi3d0bwg5p9k2m708ynz7k91") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.10.0 (c (n "khi") (v "0.10.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "0rgwp0harwg7cqks03rhcrxkaz4nvjsakqh90l3dgl4lz8k3h1rq") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.10.1 (c (n "khi") (v "0.10.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "0r6kxndyr6bi99kri0azi42p3qk5ydpcnj4l5v89in078pxyvnxd") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.10.2 (c (n "khi") (v "0.10.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "009ldv5lyfl9y4fiaqqlaxjb3kwrgjc4jc3cax339hbkfz862bgs") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.11.0 (c (n "khi") (v "0.11.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "1bz8vr3yy4nf9da57c0farwr7054dzzijyzj9l5g1fq2k9dcgjga") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.11.1 (c (n "khi") (v "0.11.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "1v9jxq3rmpkwy9vcz5r1c5l6wb1c3i8d18bkckqwk7d76gid5i54") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.12.0 (c (n "khi") (v "0.12.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "1db7vyzbqrbnfq940vcrx3fi47i40mghwhj5axm1zbr7v3x8k08f") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.12.1 (c (n "khi") (v "0.12.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "019jkagz86dq72bkzd874hjfxn0w13banvyrnzrw48cw7fnavfn9") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.12.2 (c (n "khi") (v "0.12.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "0b7ngq1k70g0k6s8ibp2h4fg4h7f9irx4bq5218n4l9qnkpnjvj9") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.13.0 (c (n "khi") (v "0.13.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "12430a4zhrfrggkp7nv50lhs4qrc87krxrx10d33xfqr10mhi7pj") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.13.1 (c (n "khi") (v "0.13.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "0ah7qzr8nvv98sdf5vr5z1qjr5rsm8rnhd4fvlk3m60pgqfa7s18") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.14.0 (c (n "khi") (v "0.14.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "1vk136k7l2q96z1pswph25wwxa6r4562f7vvww8p5gaailmhgbii") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

(define-public crate-khi-0.14.1 (c (n "khi") (v "0.14.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "numtoa") (r "^0.2.4") (d #t) (k 0)) (d (n "ref-cast") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)))) (h "10i9m4kf5namn7fzqyvq5zq523kv59fswwnnjbnapasvlhiypfj3") (f (quote (("tex" "parse") ("parse") ("html" "parse") ("default" "parse")))) (r "1.65")))

