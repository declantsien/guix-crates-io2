(define-module (crates-io #{3}# k ket) #:use-module (crates-io))

(define-public crate-ket-0.0.1 (c (n "ket") (v "0.0.1") (h "16z8ad3186dqm6cqifiqrqk6kgh0lcn4hjwvwqvnn8vgjib43mgd")))

(define-public crate-ket-0.0.2 (c (n "ket") (v "0.0.2") (d (list (d (n "wasm-bindgen") (r "^0.2.70") (d #t) (k 0)) (d (n "yew") (r "^0.17.4") (d #t) (k 0)))) (h "1z3avgfr1mrh1sscz0nfjxwz4kk48mpgjak7fmdr8jiirvcj954z")))

