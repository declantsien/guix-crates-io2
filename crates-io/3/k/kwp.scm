(define-module (crates-io #{3}# k kwp) #:use-module (crates-io))

(define-public crate-kwp-0.1.0 (c (n "kwp") (v "0.1.0") (h "0b62hdr30qj2jl15kx5rk6l9z646205nf4ms3f7h05rhqbkhas4z")))

(define-public crate-kwp-0.1.1 (c (n "kwp") (v "0.1.1") (h "1lkxy0scdqhp9fcmv757iy6kvy25mj29y210gmnh5c8mbp1bpi02")))

(define-public crate-kwp-0.2.0 (c (n "kwp") (v "0.2.0") (h "0zylj6c31as30a01q6gznrdvyiznl6m5v2ylqnlwi9244nn4y20d")))

(define-public crate-kwp-0.2.1 (c (n "kwp") (v "0.2.1") (h "022rbmhjjsx5628gwp7vq6ynskv2dxdc39kcgc4z4aadshcmanxw")))

