(define-module (crates-io #{3}# k k66) #:use-module (crates-io))

(define-public crate-k66-0.0.1 (c (n "k66") (v "0.0.1") (d (list (d (n "bare-metal") (r "^0.2.0") (d #t) (k 0)) (d (n "cortex-m") (r "^0.5.8") (d #t) (k 0)) (d (n "cortex-m-rt") (r "^0.6.5") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.0") (d #t) (k 0)))) (h "13x5whwgygw84wjqb2ihkqm8sax8rdna04y98al43sj1079ca4ri") (f (quote (("rt" "cortex-m-rt/device"))))))

