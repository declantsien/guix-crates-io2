(define-module (crates-io #{3}# k kul) #:use-module (crates-io))

(define-public crate-kul-0.1.0 (c (n "kul") (v "0.1.0") (d (list (d (n "kul_core") (r "^0.1.0") (d #t) (k 0)))) (h "0kj17iicybrrjl647fcvv509147ysa4lc3gi3k0w4q56sw2q2wqa")))

(define-public crate-kul-0.1.1 (c (n "kul") (v "0.1.1") (d (list (d (n "kul_core") (r "^0.1.1") (d #t) (k 0)))) (h "0w2rc6495bmhwmhp0vsix1190cd008dblp74xdk9nsz4xafgg0z7")))

(define-public crate-kul-0.1.2 (c (n "kul") (v "0.1.2") (d (list (d (n "kul_core") (r "^0.1.2") (d #t) (k 0)))) (h "0wsj2206a2d98cnfl8gmzb1lrxr2hbkv60zsk6z72w4h1srwvckh")))

