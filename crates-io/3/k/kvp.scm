(define-module (crates-io #{3}# k kvp) #:use-module (crates-io))

(define-public crate-kvp-1.0.0 (c (n "kvp") (v "1.0.0") (h "1yf82wrypd3qlh447pk54mm39kixqvlrimm3y91vwaka3nj9lzar")))

(define-public crate-kvp-1.0.1 (c (n "kvp") (v "1.0.1") (h "1j1za6qw2imz00d2n6zfb9z4qa9dd2p54b9j9dk9j3cqysj4x6dr")))

(define-public crate-kvp-1.0.2 (c (n "kvp") (v "1.0.2") (h "0lb58z8qm64lf4nhxi0hkdaa2qc4p5sf011arjhj6wjrxmryxhk8")))

