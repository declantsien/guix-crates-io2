(define-module (crates-io #{3}# k koi) #:use-module (crates-io))

(define-public crate-koi-0.1.0-alpha.1 (c (n "koi") (v "0.1.0-alpha.1") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "s_app_dir") (r "^0.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.4") (d #t) (k 0)))) (h "03wvywc21nsz9pm6md0awnc2igh4n6122whlmv3yppchq521li1s") (y #t)))

(define-public crate-koi-0.1.0-alpha.3 (c (n "koi") (v "0.1.0-alpha.3") (d (list (d (n "clap") (r "^2.28") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (k 0)) (d (n "koi-core") (r "^0.1.0-alpha.3") (d #t) (k 0)))) (h "14vdf7nj1vqdxjjwakf24dh7425z7hc3gxz1mb1qr6ngch90gv8n") (y #t)))

(define-public crate-koi-0.1.0 (c (n "koi") (v "0.1.0") (h "0mlk46kxx554cdjmnbi0vbihggvp9v38zvsghpzy94mkmz7w12cr") (y #t)))

