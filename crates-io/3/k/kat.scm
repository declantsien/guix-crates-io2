(define-module (crates-io #{3}# k kat) #:use-module (crates-io))

(define-public crate-kat-0.1.0 (c (n "kat") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.137") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1mc8ggl30q2zjka2i8k1vz2a98g6kk58facl1wyp33l25i2m3fv4")))

