(define-module (crates-io #{3}# k kdb) #:use-module (crates-io))

(define-public crate-kdb-0.2.0 (c (n "kdb") (v "0.2.0") (d (list (d (n "array_iterator") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "1wwvnbd226q1rnndkl80ynz6jlyn84hib5rvi2b0i9chlv05njh7") (f (quote (("embedded") ("default" "uuid"))))))

(define-public crate-kdb-0.2.1 (c (n "kdb") (v "0.2.1") (d (list (d (n "array_iterator") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "137g0p2apwjldpniay4rjm3xh9rraw1jwmfml8igmz49748a2d6x") (f (quote (("embedded") ("default" "uuid"))))))

(define-public crate-kdb-0.3.0 (c (n "kdb") (v "0.3.0") (d (list (d (n "array_iterator") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "1wgv9h2lmqrbv6c3ib391rg4q4riwjc7c8sdzg7m0yhyz1qkhnkk") (f (quote (("embedded") ("default" "uuid"))))))

