(define-module (crates-io #{3}# k ktx) #:use-module (crates-io))

(define-public crate-ktx-0.1.0 (c (n "ktx") (v "0.1.0") (d (list (d (n "blake2") (r "^0.8") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "163lfvpfww3nrss903k59664cml39nw316va63zyq9nn1wm4v7j4")))

(define-public crate-ktx-0.2.0 (c (n "ktx") (v "0.2.0") (d (list (d (n "blake2") (r "^0.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "104nbwk6dysn1d30jgr8x8lwz92bsisivcmlnmda9cqxfy6fbrh6")))

(define-public crate-ktx-0.3.0 (c (n "ktx") (v "0.3.0") (d (list (d (n "blake2") (r "^0.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)))) (h "12lmdlgkh2lvn7dr71r5ygdrf19vxmyv54b12xal7gvgmds4nzgq")))

(define-public crate-ktx-0.3.1 (c (n "ktx") (v "0.3.1") (d (list (d (n "blake2") (r "^0.8") (d #t) (k 2)) (d (n "byteorder") (r "^1.3") (k 0)))) (h "06jgiifyf9a9sq0343d2k4lshgy59d4522c41vcm2fgf54sfh6xv") (f (quote (("std") ("default" "std"))))))

(define-public crate-ktx-0.3.2 (c (n "ktx") (v "0.3.2") (d (list (d (n "blake2") (r "^0.9") (k 2)) (d (n "byteorder") (r "^1.3") (k 0)))) (h "1y59bsah49dbrckw5sh646vfx6ld2ywh22cc0rp0qnlpc3bca4h1") (f (quote (("std") ("default" "std"))))))

