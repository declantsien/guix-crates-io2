(define-module (crates-io #{3}# k kms) #:use-module (crates-io))

(define-public crate-kms-0.1.0 (c (n "kms") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (f (quote ("paris"))) (d #t) (k 0)) (d (n "test_package") (r "^0.0.1") (d #t) (k 0)))) (h "0vhsa53b80wsi1aqiwfxfhnmj7mshngqmrczy54hgpffxx5sk927") (y #t)))

(define-public crate-kms-0.1.1 (c (n "kms") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.4") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (f (quote ("paris"))) (d #t) (k 0)))) (h "0pn60qdq4gv9dhbyjl1bvk36j3mn9fddppzzj2ka95szh6839ykw")))

