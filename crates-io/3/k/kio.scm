(define-module (crates-io #{3}# k kio) #:use-module (crates-io))

(define-public crate-kio-0.1.0 (c (n "kio") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "io-uring") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 0)))) (h "0942mplf14001ri7661141b1q3g09q49h456wx6kisw63l0lz6z3")))

(define-public crate-kio-0.2.0 (c (n "kio") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "io-uring") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "slotmap") (r "^1") (d #t) (k 0)))) (h "0rd29g99p368zm8qcc8429ijansgn5nh574rhb31kydx36kybr6s")))

