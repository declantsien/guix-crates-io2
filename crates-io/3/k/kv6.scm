(define-module (crates-io #{3}# k kv6) #:use-module (crates-io))

(define-public crate-kv6-0.1.0 (c (n "kv6") (v "0.1.0") (d (list (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nw3h8w0cix4pwgxk6xj88q1xam5594r1v0x6wgjjpz9hh4kaf4y") (f (quote (("kvx") ("kv6") ("default" "kv6"))))))

(define-public crate-kv6-0.1.1 (c (n "kv6") (v "0.1.1") (d (list (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zbylc1y6yr0cd68y9680ga06wjdqryw7nsmrg35kywnkl34dy9c") (f (quote (("kvx") ("kv6") ("default" "kv6"))))))

(define-public crate-kv6-0.1.2 (c (n "kv6") (v "0.1.2") (d (list (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i0kzr324hy1jjnlihyi6m52shgaw9ca44wbwnxv5g1pnm18vdhb") (f (quote (("kvx") ("kv6") ("default" "kv6"))))))

(define-public crate-kv6-0.1.3 (c (n "kv6") (v "0.1.3") (d (list (d (n "scroll") (r "^0.11.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zm05x7kwaqa26hid9lbnf4mkp9pkhcjaksw0v0yry8wbz2i3y6f") (f (quote (("kvx") ("kv6") ("default" "kv6")))) (y #t)))

(define-public crate-kv6-0.1.4 (c (n "kv6") (v "0.1.4") (d (list (d (n "scroll") (r "^0.12.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z9fj76zwcipds4s6lgapgzxlgwfzikgxrwqb6vgpmba3xh5p4wv") (f (quote (("kvx") ("kv6") ("default" "kv6"))))))

