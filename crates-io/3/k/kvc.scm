(define-module (crates-io #{3}# k kvc) #:use-module (crates-io))

(define-public crate-kvc-0.1.0 (c (n "kvc") (v "0.1.0") (h "1v3rwzw3jzdnpl59q7cc88m1d975mfhb1y5s1yvmxlrd1ccvfw7y")))

(define-public crate-kvc-0.2.0 (c (n "kvc") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "0pdikqkhkn6xd0q1zjvdr3ghm106mcmggbk7v01d9fhmzg8jmla8")))

(define-public crate-kvc-0.2.1 (c (n "kvc") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "1xhv301ig8j4friz6sablxblf7g3wplizq892a0hvpk3jk3zbbdl")))

(define-public crate-kvc-0.3.0 (c (n "kvc") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "097hcnf1534c63lc3kr4drbw8mir1d7c67qw65083qas0316d0pj")))

(define-public crate-kvc-0.4.0 (c (n "kvc") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "0fb3cyk9rw3jncbi0sb54z40l24w67ja78d9nswb8yi1cgs4vsva")))

(define-public crate-kvc-0.5.2 (c (n "kvc") (v "0.5.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "1wv23hzjv8fb8rs3cyia8a7cspxp0xqzpvknh1w74pr2vq4vn1ig")))

(define-public crate-kvc-0.5.3 (c (n "kvc") (v "0.5.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "05akfk0zrglmizy24r0ax3ps1gvnnqj6c2lcs7zrbhv7g3px3rlp")))

(define-public crate-kvc-0.5.4 (c (n "kvc") (v "0.5.4") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "1799w8jy3zal9ww4ck6h6zxsy3g5i45cn4z9xzhz0pa5x52kc1aa")))

(define-public crate-kvc-0.5.5 (c (n "kvc") (v "0.5.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "04i2d449pharz12175p53xkxm48170bmgly5pza57pa8rsd6prfq")))

(define-public crate-kvc-0.5.6 (c (n "kvc") (v "0.5.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "0cyw5jdxzlnll8zbgv26ivp1x2hgsiiz13w0nxvfzcdnqjiwhigp")))

(define-public crate-kvc-1.0.0 (c (n "kvc") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "17fxwhz4hm9jm1yynmdbkzqbqp5i2y3393gxy5pccqxgwxkzx8la")))

(define-public crate-kvc-1.1.0 (c (n "kvc") (v "1.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "0yf2cx8fxm46dphch6hl8zmzf2jyxif94cmqs3pv03cbd7n5w96p")))

(define-public crate-kvc-1.1.1 (c (n "kvc") (v "1.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "1h0gpy6z2rl1varp5absfhxxiavp78p4xjpjm6wh06xsqm7pwn7n")))

(define-public crate-kvc-1.1.2 (c (n "kvc") (v "1.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "1vv9100r35b104j7knl33dclphjj9qzv99mrdz16lz835b9f8ing")))

(define-public crate-kvc-1.1.3 (c (n "kvc") (v "1.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.0") (d #t) (k 0)))) (h "036120pvyj1r5ka3p2d86shlghvivah9bbgigg05gqsbabhjqdnw")))

