(define-module (crates-io #{3}# k kah) #:use-module (crates-io))

(define-public crate-kah-0.2.0 (c (n "kah") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "rust-ini") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "zip") (r "^0.4") (d #t) (k 0)))) (h "1r4xpzb9dxgs3xp236cfa5jscaanks8hk5c7068aiwh86r3xhqgf")))

