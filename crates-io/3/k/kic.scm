(define-module (crates-io #{3}# k kic) #:use-module (crates-io))

(define-public crate-kic-0.1.0 (c (n "kic") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "08blj051kzn6m41vykg6vi95d2ychf07lg44kvki643ivb54ygz4")))

(define-public crate-kic-0.1.1 (c (n "kic") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "168dbk5kxlm7680zi0xpj9v233nk3yqiwxw8cyn2s8lzn6qa7sy4")))

