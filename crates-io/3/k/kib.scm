(define-module (crates-io #{3}# k kib) #:use-module (crates-io))

(define-public crate-kib-0.1.0 (c (n "kib") (v "0.1.0") (h "09svamcs96ng96iyi2858fdydzwsw0flr474x8zxg0bdgj0vgfys")))

(define-public crate-kib-0.2.0 (c (n "kib") (v "0.2.0") (h "031ls6rl3fdkdszrla2gzfgrxjhiw9rs0ax3raz3yqp4pi11wpsy")))

(define-public crate-kib-0.2.1 (c (n "kib") (v "0.2.1") (h "1xgbhib4w3z2pysbdpx6xhll6fsaz81plp7iq03xpkziph4flw70")))

(define-public crate-kib-0.3.0 (c (n "kib") (v "0.3.0") (h "0iy5f0w53s28fz3am2l8l5iwni385kqlcfl5wgbq5xmifd037k6k")))

(define-public crate-kib-0.4.0 (c (n "kib") (v "0.4.0") (h "1wilf2p4xq4fngvv77wa4ffy3fxcy1mzwczi9w3z6kwzhw4mir6g")))

(define-public crate-kib-0.5.0 (c (n "kib") (v "0.5.0") (h "0pvwkafa9m8v3pm6bq3vhfwz4l4m5vl27vd4inmbl91r5fyycrhx")))

(define-public crate-kib-1.0.0 (c (n "kib") (v "1.0.0") (h "1j38bb1xx96f7006fbh62ck501mlbzxn9ij9j1yjbxzb2g1a6136")))

(define-public crate-kib-1.0.1 (c (n "kib") (v "1.0.1") (h "0fw94rbmwlj9hcnxzxlp3yg6ka596jqjvr36zrajka7z4z6rhqcw")))

(define-public crate-kib-1.1.0 (c (n "kib") (v "1.1.0") (h "12nlb1vh4z20dqx1livv1fxkwm3xw7b4lfjvvyakwszjqrh04skx")))

(define-public crate-kib-1.2.0 (c (n "kib") (v "1.2.0") (h "0ipsrdrwvzklnxlc36qjhbnxdklvxw3ymxwixhy6bxzlnchbvdrg")))

(define-public crate-kib-2.0.0 (c (n "kib") (v "2.0.0") (h "16h8f2xfqp37yf5s5dmn8cakbz8wl4xmnx9b94d8nha82vyq9ng9")))

(define-public crate-kib-3.0.0 (c (n "kib") (v "3.0.0") (h "1hqhykpk3h1dlpcbz78jdw6mj3qlr4201v46d5vyh0a0l85b6sd1")))

(define-public crate-kib-4.0.0 (c (n "kib") (v "4.0.0") (h "09k2xacppl5xifay1nmcmrb7v2dnbqrn43cq17wqkshlw9rvd0zj")))

(define-public crate-kib-5.0.0 (c (n "kib") (v "5.0.0") (h "0rjgnlf7gwwdsqsnw6x316lr4kl3abmmnb5fljnmy1h1hr32hljk") (f (quote (("std"))))))

(define-public crate-kib-6.0.0 (c (n "kib") (v "6.0.0") (h "069778v2iy1dgdijncill4bc3s719p7izzx3jakv8fhc5070clwn") (f (quote (("std"))))))

(define-public crate-kib-7.0.0 (c (n "kib") (v "7.0.0") (h "0qs67ch8j03hvn7sws4vyv2vjc08m8i4c9l8rpn8w9nyq172c3qq")))

(define-public crate-kib-7.0.1 (c (n "kib") (v "7.0.1") (h "1642zyxwz6zq1z62237g4d6gvlamazjb9wpmdk9jxrlvbva8cn08")))

