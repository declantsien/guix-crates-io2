(define-module (crates-io #{3}# k k12) #:use-module (crates-io))

(define-public crate-k12-0.0.0 (c (n "k12") (v "0.0.0") (h "0askl8bnhk0y6mb5q1zms0c5vyvfwxr8lg4y1ham88ybcw6swdhy")))

(define-public crate-k12-0.0.1 (c (n "k12") (v "0.0.1") (d (list (d (n "digest") (r "^0.8") (d #t) (k 0)) (d (n "digest") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "0a83km4amvldxn72s49fm1wq1wqjs1fjyhxd7kq9yn85ln69frha") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-k12-0.1.0 (c (n "k12") (v "0.1.0") (d (list (d (n "digest") (r "^0.9") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "digest") (r "^0.9") (f (quote ("alloc" "dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "178gk77rcgcgb49a9x5bpkix9vs2m7fn6q3rnssii8yf362ywc3b") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-k12-0.2.0 (c (n "k12") (v "0.2.0") (d (list (d (n "digest") (r "^0.10") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "digest") (r "^0.10") (f (quote ("alloc" "dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "07qi9w3jj4zrjhjh8v7i0s51ds9mfh7wx7igkbg6xxqc5mi6bh25") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-k12-0.2.1 (c (n "k12") (v "0.2.1") (d (list (d (n "digest") (r "^0.10.3") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (f (quote ("alloc" "dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2.2") (d #t) (k 2)))) (h "17p99b7s5mky65qjw42b4bs92bzl1n1cnyi6kfd71pzh8cimg7fm") (f (quote (("std" "digest/std") ("default" "std"))))))

(define-public crate-k12-0.3.0 (c (n "k12") (v "0.3.0") (d (list (d (n "digest") (r "^0.10.7") (f (quote ("core-api"))) (k 0)) (d (n "digest") (r "^0.10.7") (f (quote ("alloc" "dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "sha3") (r "^0.10.8") (k 0)))) (h "0m1cj561jif0zl7bj5s8gdkc5cr5bpqh8wwj2q0m4bxgcbdmzp7l") (f (quote (("std" "digest/std") ("default" "std")))) (r "1.56")))

