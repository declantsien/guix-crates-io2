(define-module (crates-io #{3}# k kmp) #:use-module (crates-io))

(define-public crate-kmp-0.1.0 (c (n "kmp") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "0s9w0rqyvvd587lvd4f20h1wbhdkksrh1kp0m5jyn6c4d3q9r24q")))

(define-public crate-kmp-0.1.1 (c (n "kmp") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)))) (h "0a1093n8v20p43jvg2ima5clisyp23h5n34z3v616hm6b6mxs6qk")))

