(define-module (crates-io #{3}# k kal) #:use-module (crates-io))

(define-public crate-kal-0.1.0 (c (n "kal") (v "0.1.0") (h "004afp9z8nxhz9ibhrvm3ds1a34hsy4396p2clgb68cb912y0bd9")))

(define-public crate-kal-0.1.1 (c (n "kal") (v "0.1.1") (d (list (d (n "kal-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0za7yv50i28gqxqmri9d8m35sy5vzllap2i021dar22pq0cwly19") (f (quote (("derive" "kal-derive") ("default" "derive"))))))

(define-public crate-kal-0.1.2 (c (n "kal") (v "0.1.2") (d (list (d (n "kal-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "08rlf8f8g90j3mzval92dd5n5s4ax8hsh38h4hd3jri3sp9dfkdr") (f (quote (("derive" "kal-derive") ("default" "derive")))) (y #t)))

(define-public crate-kal-0.1.3 (c (n "kal") (v "0.1.3") (d (list (d (n "kal-derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0mzbnlq95dlixij7vzzaq5ww3ij49h1zxpf17l6w9c6mxa8p0m61") (f (quote (("derive" "kal-derive") ("default" "derive"))))))

(define-public crate-kal-0.2.0 (c (n "kal") (v "0.2.0") (d (list (d (n "kal-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1lxgdhl6nw5ijv3yfdsg0r1ig8jsnf3c0960src41jswrrzq5zn9") (f (quote (("derive" "kal-derive") ("default" "derive"))))))

(define-public crate-kal-0.2.1 (c (n "kal") (v "0.2.1") (d (list (d (n "kal-derive") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1wyrpm1w93anq5cjjcykk6xwcr0h4s0mqsxripjlxinl704n7d8z") (f (quote (("derive" "kal-derive") ("default" "derive"))))))

(define-public crate-kal-0.3.0 (c (n "kal") (v "0.3.0") (d (list (d (n "kal-derive") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1935xfxzfhaq0nv3c5gghg68b79h7yxzmgj7nin4d8zb83l08j67") (f (quote (("default" "derive")))) (s 2) (e (quote (("lex" "kal-derive?/lex") ("derive" "dep:kal-derive"))))))

(define-public crate-kal-0.4.0 (c (n "kal") (v "0.4.0") (d (list (d (n "kal-derive") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1a98xiqn38pqqxx1126vwpx7llgb3i4jxjfadlcz86yhxj9mi39q") (f (quote (("default" "derive")))) (y #t) (s 2) (e (quote (("lex" "kal-derive?/lex") ("derive" "dep:kal-derive"))))))

(define-public crate-kal-0.4.1 (c (n "kal") (v "0.4.1") (d (list (d (n "kal-derive") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0x0x1aa0zqfhmz4d65q3abxgrx24cnn7gvj5pmisv1rcq0pc4kwn") (f (quote (("default" "derive")))) (s 2) (e (quote (("lex" "kal-derive?/lex") ("derive" "dep:kal-derive"))))))

(define-public crate-kal-0.4.2 (c (n "kal") (v "0.4.2") (d (list (d (n "kal-derive") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0di4xkxb0gy4ds17g9kbnygdqg6brqgb6fadrv4ph19lgyjdp2q0") (f (quote (("default" "derive")))) (s 2) (e (quote (("lex" "kal-derive?/lex") ("derive" "dep:kal-derive"))))))

(define-public crate-kal-0.5.0 (c (n "kal") (v "0.5.0") (d (list (d (n "kal-derive") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1znvh1jj6bhk4d847fv26wwn81j307sij6qwcshshpl1m1dxnack") (f (quote (("default" "derive")))) (s 2) (e (quote (("lex" "kal-derive?/lex") ("derive" "dep:kal-derive"))))))

