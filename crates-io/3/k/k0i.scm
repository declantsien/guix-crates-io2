(define-module (crates-io #{3}# k k0i) #:use-module (crates-io))

(define-public crate-k0i-0.1.0 (c (n "k0i") (v "0.1.0") (h "1q26p90qzky00lnr7jns1lw6gw9ibmkmxld4fshpib99dsykbfh5") (y #t)))

(define-public crate-k0i-0.1.1 (c (n "k0i") (v "0.1.1") (h "164c5ijnh9sshc5x58mr4dvb326xy47mbkqbcz4lx318hnv5hwwk") (y #t)))

(define-public crate-k0i-0.0.1 (c (n "k0i") (v "0.0.1") (h "1rny3bvr2aigfc6mm5s2ksnc9fbg8ya7m3a01mi8qhlvcaps7x7v")))

(define-public crate-k0i-0.0.2 (c (n "k0i") (v "0.0.2") (h "0h6c1kaykbpz830879b3n4vml8y4ld7hx3wjf7vlj1hzhw9kx88j")))

(define-public crate-k0i-0.0.3 (c (n "k0i") (v "0.0.3") (h "0nff3cpg19d0r6qp6i8zmqcn6gfr7k1fv1wpf41xsjz1xx5yjbic")))

(define-public crate-k0i-0.0.4 (c (n "k0i") (v "0.0.4") (h "0li64dyvs8zqjz0rw7g6w1a5ryshzi7h5kqgrlbwypas7h8f25ag")))

(define-public crate-k0i-0.0.5 (c (n "k0i") (v "0.0.5") (h "0f6p619bfqdzrs2jyg9alck3h40cj5i8558dxb18n9zr367kwmcm")))

(define-public crate-k0i-0.0.6 (c (n "k0i") (v "0.0.6") (h "0642m8vvkqs4zr6n9x7xq6hbcdnh597jn1vpg21zmcl72i0fwbkk")))

(define-public crate-k0i-0.0.7 (c (n "k0i") (v "0.0.7") (h "0qhf6a512mf0l6v94xkdxilqi3b5m7l4f1x05a1x2g83jvz941n7")))

