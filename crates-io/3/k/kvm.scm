(define-module (crates-io #{3}# k kvm) #:use-module (crates-io))

(define-public crate-kvm-0.1.0 (c (n "kvm") (v "0.1.0") (h "1bdnqm2gykv3p0ypkql8ka636kivwwkqpkpa1v42l8cjhv6zwq4a")))

(define-public crate-kvm-0.2.0 (c (n "kvm") (v "0.2.0") (d (list (d (n "clippy") (r "^0.0.23") (o #t) (d #t) (k 0)) (d (n "errno") (r "^0.1.4") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "ioctl") (r "^0.3.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "memmap") (r "^0.2.1") (d #t) (k 0)))) (h "1kfqxjdhlsif5f0907nm15kaaxz7hip11yvv2ql68adpjlg3nawf") (f (quote (("dev" "clippy") ("default"))))))

