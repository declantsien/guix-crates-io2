(define-module (crates-io #{3}# k kcl) #:use-module (crates-io))

(define-public crate-kcl-0.3.1 (c (n "kcl") (v "0.3.1") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "base64-serde") (r "^0.6.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "03vjg37v8zm28c64hfmilf0bh3vzrk8lq36lkf27in5amm1fs581")))

(define-public crate-kcl-0.3.2 (c (n "kcl") (v "0.3.2") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "base64-serde") (r "^0.6.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1hclaxbp1q64xl754830wxsk9pf0ddsxz9vhpypgc74cbhg5ai6m")))

(define-public crate-kcl-0.3.3 (c (n "kcl") (v "0.3.3") (d (list (d (n "base64") (r "^0.13.1") (d #t) (k 0)) (d (n "base64-serde") (r "^0.6.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1fv2z3djn7xbabbp9s62m5s3zgx9wyihb2fdb9fz6xlvqlzwgpv8")))

