(define-module (crates-io #{3}# k kaf) #:use-module (crates-io))

(define-public crate-kaf-1.0.0 (c (n "kaf") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "094kpr52my5p4japwbz87p7sswb9v033vmq9q9igf2849nx8c19x")))

(define-public crate-kaf-1.0.1 (c (n "kaf") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1k97aa8f0r7a60ybysgqg4w8ayvqaggmi9yd0rbkn3apdmrl886s")))

(define-public crate-kaf-1.0.2 (c (n "kaf") (v "1.0.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "0vfd1k1cpr6rk314j45fv98p7d5xp2a54nfdp67w77fafd2c11kl")))

