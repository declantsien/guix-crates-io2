(define-module (crates-io #{3}# l lad) #:use-module (crates-io))

(define-public crate-lad-0.1.0 (c (n "lad") (v "0.1.0") (h "1ajf7r33j52nk8z3wy2fb8kbxsf68cfvj0lccvxld91cg8xk9x9j")))

(define-public crate-lad-0.1.1 (c (n "lad") (v "0.1.1") (h "11na00sivmlf7zk3c2ywzkkrrndxsf3l2dlpn13ymqk2c0jcmxpd")))

