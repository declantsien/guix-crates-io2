(define-module (crates-io #{3}# l ley) #:use-module (crates-io))

(define-public crate-ley-0.0.1 (c (n "ley") (v "0.0.1") (h "15qrz0zmbkgipwa81x66d4rc232cmdwjrx49m08la05srhc5jv5j")))

(define-public crate-ley-0.0.2 (c (n "ley") (v "0.0.2") (h "0n8yfcwrf0whabxr3v22fqhf4bwmrly9isfghk03hzj1y41pjhvi")))

(define-public crate-ley-0.0.3 (c (n "ley") (v "0.0.3") (h "03axjl2wi3j5hk3c7iwjl8ib7rh9plaa9ns7bpk76bsmkn25zv20")))

(define-public crate-ley-0.0.4 (c (n "ley") (v "0.0.4") (h "0mfbapd62181pn8cbg07mg00m6np57l9si3fs1wg95j2bvm3kfph")))

(define-public crate-ley-0.0.5 (c (n "ley") (v "0.0.5") (h "048hkg174z4hs3b9g7im21ylwi2h9s3aa6hyxaalrybc1razsh4z")))

