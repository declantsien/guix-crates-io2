(define-module (crates-io #{3}# l lzf) #:use-module (crates-io))

(define-public crate-lzf-0.1.0 (c (n "lzf") (v "0.1.0") (d (list (d (n "gcc") (r "*") (d #t) (k 0)))) (h "1bl3pg0hcgvgnl3y70b2vv9p1xc43ln5zksvhs0p67zff4yws2n5")))

(define-public crate-lzf-0.1.1 (c (n "lzf") (v "0.1.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "034s87mp0ci7swvhrz7rlbaiqs8p7l8i6ks70hfaqhw43dmw3wc3")))

(define-public crate-lzf-0.1.2 (c (n "lzf") (v "0.1.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1x2siyxlrycqbcfk2qalijwkj4pg444h9a3hb6378n19hf4xh1wm")))

(define-public crate-lzf-0.1.3 (c (n "lzf") (v "0.1.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1zi1p3fa4n8zzaghr9y48h2729548n7zk53s5d4i5xx8v1y7bzzw")))

(define-public crate-lzf-0.1.4 (c (n "lzf") (v "0.1.4") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1rvgibwg7rpagp4da05a2j6d4h9nl4q5411rvg4jqcw4jv3iw0qb")))

(define-public crate-lzf-0.2.0 (c (n "lzf") (v "0.2.0") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0lsq3kvhzy7qb3xps5wh4i8kzd4q5gk1hdvag8m4x5h6id2yfhzb")))

(define-public crate-lzf-0.2.1 (c (n "lzf") (v "0.2.1") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0iy8ha6s47lnaa87x7d02ls0yxkdwdmsdwivm5pijvrqw32m29sg")))

(define-public crate-lzf-0.2.2 (c (n "lzf") (v "0.2.2") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1841v4891fvc587vk91gwn67n0zbfzdwv1rw73wm7pdrzc5fgyhp")))

(define-public crate-lzf-0.2.3 (c (n "lzf") (v "0.2.3") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "00v0dxm4c8pcn7x87k4ia5bbvpcdv8s7l283il84d0qxayl1gk35")))

(define-public crate-lzf-0.3.0 (c (n "lzf") (v "0.3.0") (h "1z6jgf2j5vihgafcfa2k0fir94rbsv11laqbl90xrbs2dggd9bzy")))

(define-public crate-lzf-0.3.1 (c (n "lzf") (v "0.3.1") (h "000d608k90mjimppgd6398hq260fkzhr4yhhhd61i8kxnw7bxr8p") (y #t)))

(define-public crate-lzf-0.3.2 (c (n "lzf") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "10sskq8r9dc9iv0qd3lzpd00r227bmxqlgng6f5250r0cpv795hz") (f (quote (("default"))))))

(define-public crate-lzf-1.0.0 (c (n "lzf") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "0885adpy5p64md0hjr5m8fx16br3051zgri2hn7hh5whlryfyd60") (f (quote (("default"))))))

