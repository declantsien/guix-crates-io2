(define-module (crates-io #{3}# l lrc) #:use-module (crates-io))

(define-public crate-lrc-0.1.0 (c (n "lrc") (v "0.1.0") (d (list (d (n "educe") (r ">= 0.4.2") (f (quote ("Default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "0j5asjr73jm91x9ca3xpfr2dxy9csg0bvd2vjnql0mvhsma4a1qc")))

(define-public crate-lrc-0.1.1 (c (n "lrc") (v "0.1.1") (d (list (d (n "educe") (r ">=0.4.2") (f (quote ("Default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "1gy4g3lr8bsf3hs7fk4igwlrmb4dn3fnz2ynjffc3dvqk0xla8i3")))

(define-public crate-lrc-0.1.2 (c (n "lrc") (v "0.1.2") (d (list (d (n "educe") (r ">=0.4.2") (f (quote ("Default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "0fcnifxcwhi3fyrrrhpxd8g3sn3ha0sxdf32lrz57ys22hsq0h77")))

(define-public crate-lrc-0.1.3 (c (n "lrc") (v "0.1.3") (d (list (d (n "educe") (r ">=0.4.2") (f (quote ("Default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "0zxg1b8sz9j9hn8dmfcjzmmsg1by2shyqa8l3q5j6cgrd424qn5p")))

(define-public crate-lrc-0.1.4 (c (n "lrc") (v "0.1.4") (d (list (d (n "educe") (r ">=0.4.2") (f (quote ("Default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "122r96jqk6xxq5ma0la65yxfsfiw0mk2sgabdkimdlrx251ccmcc")))

(define-public crate-lrc-0.1.5 (c (n "lrc") (v "0.1.5") (d (list (d (n "educe") (r ">=0.4.2") (f (quote ("Default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "0hv5qy8vk3nmp9yf0in99l057azhz8yvk4a89xf049dk27sdkiam")))

(define-public crate-lrc-0.1.6 (c (n "lrc") (v "0.1.6") (d (list (d (n "educe") (r ">=0.4.2") (f (quote ("Default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "1kv8f2hk03rm1001x4kfy3p9cmb63wcwp764mf0hcky6gsrrc6as")))

(define-public crate-lrc-0.1.7 (c (n "lrc") (v "0.1.7") (d (list (d (n "educe") (r ">=0.4.2") (f (quote ("Default"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "08mpaz8iylq58pgbbxmnvcqd3k7d5jc4v35nm6jmvlqdz473gzf3")))

(define-public crate-lrc-0.1.8 (c (n "lrc") (v "0.1.8") (d (list (d (n "educe") (r ">=0.4.2") (f (quote ("Default"))) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "0p50wj7x01754k138nd55k3jf9nfif9xwws6rfrnigia1mwykpzb") (r "1.61")))

