(define-module (crates-io #{3}# l lep) #:use-module (crates-io))

(define-public crate-lep-0.1.0 (c (n "lep") (v "0.1.0") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 2)))) (h "17vz69rhjs118lgj7a4dw9c2sjf9dx601pw67r8gg2sx7vgvvpd2")))

(define-public crate-lep-0.2.0 (c (n "lep") (v "0.2.0") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 2)))) (h "119822zpphi4kbafhvsahckkif9n916i0jh9c4bw40khqw5j58p9")))

(define-public crate-lep-0.2.1 (c (n "lep") (v "0.2.1") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 2)))) (h "0dkkk2g8xvj8vcsg84xj4dh3lqqv8ihx54c1l64xrg6jsg48scqr")))

(define-public crate-lep-0.2.2 (c (n "lep") (v "0.2.2") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 2)))) (h "1g7sasr0gpm1md9idq1whp36ppw4pr9bvxkdzcswk45gk3lrmlrj")))

(define-public crate-lep-0.3.0 (c (n "lep") (v "0.3.0") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 2)))) (h "12hfik9p6ksys3gg9b4yvvyhqyvlgzcsjvp48wr9rcb6x09f909m")))

(define-public crate-lep-0.3.1 (c (n "lep") (v "0.3.1") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 2)))) (h "0ly7x639n5j3q6123csi0vyx4vk711qx0rpcwaz0p1camzxl2dxz")))

(define-public crate-lep-0.4.0 (c (n "lep") (v "0.4.0") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 2)))) (h "0ixibgjs490d6ijbdrlb3v08rzf61m9z16lsdgwm9anrzqm444c3")))

(define-public crate-lep-0.4.1 (c (n "lep") (v "0.4.1") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 2)))) (h "0zzl3dv60w6wvy7hxgqi8yrnzx07zsxr0q5xka10hqcy558cfbm4")))

(define-public crate-lep-0.4.2 (c (n "lep") (v "0.4.2") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 2)))) (h "1va206jg7ghyzhqnv4yh2f3lcn22nhxg3ijf4a5xl2i0gigb4926")))

(define-public crate-lep-0.4.3 (c (n "lep") (v "0.4.3") (d (list (d (n "rustyline") (r "^3.0.0") (d #t) (k 2)))) (h "1ni0wk672z0k4k47hk2wlzkdcjghcpdgqg8z1ab6wg0r88ss4bsp")))

(define-public crate-lep-0.4.4 (c (n "lep") (v "0.4.4") (d (list (d (n "rustyline") (r "^6.0.0") (d #t) (k 2)))) (h "01vyw6r3x8vi11hzaxkfhnkqm11c9jmlcjj0z80sh49w7n1837nn")))

(define-public crate-lep-0.4.5 (c (n "lep") (v "0.4.5") (d (list (d (n "rustyline") (r "^6.0.0") (d #t) (k 2)))) (h "0f8a2jljk11k13xb89q8659y7i5nw3rz1h2vc751cq7i2sryx63j")))

(define-public crate-lep-0.5.0 (c (n "lep") (v "0.5.0") (d (list (d (n "rustyline") (r "^6.0.0") (d #t) (k 2)))) (h "1y0vqrfb1vj6yx1fd835wcdf6kr3kng6hpl63qs7x1cf272pjihj")))

