(define-module (crates-io #{3}# l len) #:use-module (crates-io))

(define-public crate-len-0.1.0 (c (n "len") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.0") (d #t) (k 0)))) (h "0dikgvsa5h8am7011252swi35dq8qqir63grimm9smfhmsq4i1w9")))

(define-public crate-len-0.1.1 (c (n "len") (v "0.1.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.0") (d #t) (k 0)))) (h "0dxk0sn4q6cy9yvn38ammad5zsqbl95byz6015zl69fk2c4bsy35")))

(define-public crate-len-0.2.0 (c (n "len") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "bytesize") (r "^1.0.0") (d #t) (k 0)))) (h "04lax29bfdpg1l4kbsjlcrd66xf8pbgdq68c7ajq61lqv5h23yln")))

