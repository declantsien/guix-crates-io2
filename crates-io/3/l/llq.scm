(define-module (crates-io #{3}# l llq) #:use-module (crates-io))

(define-public crate-llq-0.1.0 (c (n "llq") (v "0.1.0") (h "1v8prpjqwy1df2x6gahlxr4f6drdwbjirrkr7zh54gf66vmmxh9p") (y #t)))

(define-public crate-llq-0.1.1 (c (n "llq") (v "0.1.1") (h "18x40ghcilqssq5lxr6d3qwjb4ghk9qqlmxswwdm2zpwlqwwpv92")))

