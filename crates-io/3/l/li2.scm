(define-module (crates-io #{3}# l li2) #:use-module (crates-io))

(define-public crate-li2-0.0.1 (c (n "li2") (v "0.0.1") (d (list (d (n "digest") (r "^0.10.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 2)) (d (n "signature") (r "^1.6.4") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1k2mk7gxwwychnc1b4sjzlkjb22900m5nqfpys945gm9rl021ys1")))

