(define-module (crates-io #{3}# l lxd) #:use-module (crates-io))

(define-public crate-lxd-0.1.0 (c (n "lxd") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vg655lgsgpl85sp366rha0v4kacri3d0ccgw7ildwzil1320k8b")))

(define-public crate-lxd-0.1.1 (c (n "lxd") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "02408h1g7752ha0zqyvzjblm0aa39k0mg36vyacvvnkr7937ksyf")))

(define-public crate-lxd-0.1.2 (c (n "lxd") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1hh1skhhgq69nyam2cgavby5399g7fzn94y4accr2n6x4yn9kfi0")))

(define-public crate-lxd-0.1.3 (c (n "lxd") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1vri7kiw60qis7h3dfq3gyxgd44ns1pl66fapskxm9w445llrn6i")))

(define-public crate-lxd-0.1.4 (c (n "lxd") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0w2w28yaiyl6bn161b6jdz5x0p3cpkq0j5l6ys5qday2945v0gcq")))

(define-public crate-lxd-0.1.5 (c (n "lxd") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "072as66jz0zdzxlch5fbpwcm5yyrj7411pf7vpdpjg54askfwnba")))

(define-public crate-lxd-0.1.6 (c (n "lxd") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0zb1b5j1mghb81hr7p9sqc0c2hjsnggg16c02cgkhh63sacimq1z")))

(define-public crate-lxd-0.1.7 (c (n "lxd") (v "0.1.7") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1ih0ps5fglfff0anhrax70haj2a90vmws050sqq1lwhsfscibwgv")))

(define-public crate-lxd-0.1.8 (c (n "lxd") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "07a3ccb1yfyvm9qlb727b7gsqbvfvb4l7kpls48i79l2bzs9dzml")))

(define-public crate-lxd-0.1.9 (c (n "lxd") (v "0.1.9") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0nkwgks0pxb958n4zblblqjpr8ran5ll0jqm7nff3m5pir5k32li")))

