(define-module (crates-io #{3}# l lts) #:use-module (crates-io))

(define-public crate-lts-0.1.0 (c (n "lts") (v "0.1.0") (h "03mi1r6p7b27wdqiiwzyf4qhp1qwl05l1rhxhgzzrd3qh0czddj1")))

(define-public crate-lts-0.1.1 (c (n "lts") (v "0.1.1") (h "0v2zwzh70r2ydyb5bzkp8702dim21ijql05czmlvpwslj1sya3cs")))

(define-public crate-lts-0.1.2 (c (n "lts") (v "0.1.2") (h "1389pn82v4px72y6a0cp80wssr0wx43saw1qayjy10v5kvw2rdjs")))

(define-public crate-lts-0.1.3 (c (n "lts") (v "0.1.3") (h "1q6bvg0a2rr27bdfix63l0nh43bi2kx2qm6cij8g0q8g8av15lzd")))

(define-public crate-lts-0.1.4 (c (n "lts") (v "0.1.4") (h "1afvn6h1188cjdpypb96yyhlgr0lnvg60h2wivg4082nnjkpq237")))

(define-public crate-lts-0.1.5 (c (n "lts") (v "0.1.5") (h "0a38s34gadx9411vzgz60029g8fgdgn36rafpgkp0a1p5ja97qsc")))

(define-public crate-lts-0.1.6 (c (n "lts") (v "0.1.6") (h "1m8jqay072k0v02m1ix117921lpcnhcpv5x5c6ydkhc9s54dwq8k")))

(define-public crate-lts-0.2.0 (c (n "lts") (v "0.2.0") (h "1gb8sr0avkapxdwglzvybavrzaggb9sxhvv5gchwpcnj4fi4nnhp")))

(define-public crate-lts-0.3.0-alpha.1 (c (n "lts") (v "0.3.0-alpha.1") (d (list (d (n "ryu") (r "=1.0.4") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "=1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "=1.0.44") (d #t) (k 0)))) (h "1x2828bw1q5wlamzgg7q24sz51m1qkjppj15cyrkj4qilwapqhd2") (y #t)))

(define-public crate-lts-0.3.0 (c (n "lts") (v "0.3.0") (d (list (d (n "ryu") (r "=1.0.4") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "=1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "=1.0.44") (d #t) (k 0)))) (h "14vbdh5z5yi8c691k4ms7iam4i7dcv9k3zx6d84p0fck1wxqrpdc")))

(define-public crate-lts-0.3.1 (c (n "lts") (v "0.3.1") (d (list (d (n "ryu") (r "=1.0.4") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "=1.0.118") (d #t) (k 0)) (d (n "serde_derive") (r "=1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "=1.0.44") (d #t) (k 0)))) (h "10fx93lg1pdnzrv6p613v0pwxvzrcg4i8bigzljr167kwj97hs4z")))

