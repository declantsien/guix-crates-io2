(define-module (crates-io #{3}# l lvl) #:use-module (crates-io))

(define-public crate-lvl-0.1.0 (c (n "lvl") (v "0.1.0") (h "1l20nscx2kvivijyasm7pinn04afr58rwnv1wvp2n3wkycinyq3l") (y #t)))

(define-public crate-lvl-0.2.0 (c (n "lvl") (v "0.2.0") (h "01ysd21q34imhgr82ml3rqawipv7y9lj5bsvamc2s4hkkr6hmbsp") (y #t)))

(define-public crate-lvl-0.3.0 (c (n "lvl") (v "0.3.0") (h "0ciqk4rj9cv8nf5g4cb7i5skxmy04br5gf0lhjq4rpyvqrb604a5") (y #t)))

