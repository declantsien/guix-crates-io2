(define-module (crates-io #{3}# l lzw) #:use-module (crates-io))

(define-public crate-lzw-0.1.0 (c (n "lzw") (v "0.1.0") (h "0yypnzx00fgrq2yks83swri5wsi2kagzpgbjzilw5a5x90qbyxi9")))

(define-public crate-lzw-0.2.0 (c (n "lzw") (v "0.2.0") (h "09fgq5nkvxrfz260wvn6267ad7jyv63wsgs73xg5vkas25rkfl4w")))

(define-public crate-lzw-0.3.0 (c (n "lzw") (v "0.3.0") (h "0cfg125433y6r541arqlkp95pj520xnyn2d0fp1i8d6jixbm24ph")))

(define-public crate-lzw-0.4.0 (c (n "lzw") (v "0.4.0") (h "0dkf0qs5vfsd153568hc98al7q0gxqpmx2qrsppq1f4xii90b96j")))

(define-public crate-lzw-0.5.0 (c (n "lzw") (v "0.5.0") (h "13v2qfmcjrl2yfk9bphf6rfxdr6hfl19frn6sd884y4s7f4lx5bv")))

(define-public crate-lzw-0.7.0 (c (n "lzw") (v "0.7.0") (h "0iss0wyw6kblh5iy2glrvafply2wif2afpgzz2n8bg3g0cs5ndzk") (f (quote (("raii_no_panic") ("default" "raii_no_panic"))))))

(define-public crate-lzw-0.8.0 (c (n "lzw") (v "0.8.0") (h "1y18lk7jdrw3rb2ncmwq0minrirjrrz4yk1lpv4rpgwajkbl0bvc") (f (quote (("raii_no_panic") ("default" "raii_no_panic"))))))

(define-public crate-lzw-0.9.0 (c (n "lzw") (v "0.9.0") (h "14pf04x8ps9a2vly4i275cjhcaplg1p5l752iywvq69f8d227a6b") (f (quote (("raii_no_panic") ("default" "raii_no_panic"))))))

(define-public crate-lzw-0.10.0 (c (n "lzw") (v "0.10.0") (h "1170dfskhzlh8h2bm333811hykjvpypgnvxyhhm1rllyi2xpr53x") (f (quote (("raii_no_panic") ("default" "raii_no_panic"))))))

