(define-module (crates-io #{3}# l lut) #:use-module (crates-io))

(define-public crate-lut-0.1.0-unstable (c (n "lut") (v "0.1.0-unstable") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "072hsdcqg018dkl685qc33jgk5m0zvwz90x5wwlbkg5zdl4bqkd0") (f (quote (("media-type-chars") ("default"))))))

(define-public crate-lut-0.1.1-unstable (c (n "lut") (v "0.1.1-unstable") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "1nyrr2xiy8sn52pvkmzjy5z26r4x77h0x6a2i0gviwxzcsxl73vy") (f (quote (("media-type-chars") ("default"))))))

