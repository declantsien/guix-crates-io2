(define-module (crates-io #{3}# l lei) #:use-module (crates-io))

(define-public crate-lei-0.1.0 (c (n "lei") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "iso_iec_7064") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0p2ziszhy3jc0lmcn05ri9p8pdwhawpikliyzck1vlwjyb6xfbfd") (y #t)))

(define-public crate-lei-0.2.0 (c (n "lei") (v "0.2.0") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "iso_iec_7064") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1bk1qngf9hh87q8c07ajfl6fxy02zmqqvhl2g2j3fgc5bxlnlfdz")))

(define-public crate-lei-0.2.1 (c (n "lei") (v "0.2.1") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "iso_iec_7064") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1lxxfjxvy7aixf0hwhg21ycw9b4v8j19hsshs1viz17f2888cfcj")))

(define-public crate-lei-0.2.2 (c (n "lei") (v "0.2.2") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "iso_iec_7064") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "1bbnx5vvxwkd9vwl53wa5dxgh8pygd9kx3na9srycwfppdw0280j")))

(define-public crate-lei-0.2.3 (c (n "lei") (v "0.2.3") (d (list (d (n "bstr") (r "^0.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "iso_iec_7064") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "0i8rbgd340fj7x1jll5wlxw2wsqhfjl1qwrqkx69wgs72nm9k916")))

(define-public crate-lei-0.2.4 (c (n "lei") (v "0.2.4") (d (list (d (n "bstr") (r "^1.2") (f (quote ("std"))) (d #t) (k 0)) (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "iso_iec_7064") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)))) (h "15wgnbg2h6b17jdky5yaaqn262ghkvwlc22052csi0d4awjsfg49")))

(define-public crate-lei-0.2.5 (c (n "lei") (v "0.2.5") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "iso_iec_7064") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^1.2.0") (d #t) (k 2)))) (h "0iv5wbgh6c79k5swfc1xld6j8zdzfwb6a5qha2y688j8n91lxf8h")))

