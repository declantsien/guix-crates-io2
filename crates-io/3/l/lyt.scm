(define-module (crates-io #{3}# l lyt) #:use-module (crates-io))

(define-public crate-lyt-0.1.0 (c (n "lyt") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "pulldown-cmark-frontmatter") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syntect") (r "^5.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "04iq0wmvdf5ac9yag6psxvmn5wajhjn1fmmj131cs00zxghlnyzy")))

(define-public crate-lyt-0.1.1 (c (n "lyt") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "handlebars") (r "^4.5.0") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "notify-debouncer-full") (r "^0.3.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "pulldown-cmark-frontmatter") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "syntect") (r "^5.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0hcxajnhr6qq0yngcmfz5661is81ly11mf2bschp1znp2wkz5igc")))

