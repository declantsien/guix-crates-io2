(define-module (crates-io #{3}# l lxr) #:use-module (crates-io))

(define-public crate-lxr-0.1.0 (c (n "lxr") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.3") (d #t) (k 0)))) (h "1rlf9q4w077478r8qi10ask3gqw60srqz96sphbglmw3f0g85qdx")))

