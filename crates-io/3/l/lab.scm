(define-module (crates-io #{3}# l lab) #:use-module (crates-io))

(define-public crate-lab-0.1.0 (c (n "lab") (v "0.1.0") (h "1ps6pbd20jssmdim3qwwir75k0r5krazsfbq61ya7x9748akb3qz")))

(define-public crate-lab-0.2.0 (c (n "lab") (v "0.2.0") (h "1349mknqfn7nrkpp13m27drxm6cy3hvhrk7f0j2jlnh6vb2l6bg4")))

(define-public crate-lab-0.3.0 (c (n "lab") (v "0.3.0") (h "1130xbqqfwzhdzf3qrlhfxnrpl0i26c9lir3p990xnha6355hk75")))

(define-public crate-lab-0.4.0 (c (n "lab") (v "0.4.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1xycspc4hyp69hqvfs52qfih83pfbrqz1p83qf4md2khn6bb3f29")))

(define-public crate-lab-0.4.1 (c (n "lab") (v "0.4.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1nk2v190m33ha0y80dbp4hg8nk3pi2h5cpy2yr8fs3mkrlr4x113")))

(define-public crate-lab-0.4.2 (c (n "lab") (v "0.4.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1k92yg78y6llcjx1xmpxwkcxylkjnhx9w0v9z263ry00wkr8z09h")))

(define-public crate-lab-0.4.3 (c (n "lab") (v "0.4.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0a48kfzyr91d5b79fb715kqgkq19fdmnqpvbdw1cxmgax15bp74l")))

(define-public crate-lab-0.4.4 (c (n "lab") (v "0.4.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0h4ig5bvzmwlzd74zj7b4sh7kzi3c6mjjnw7yjz8ijxvr4mrcr1s")))

(define-public crate-lab-0.5.0 (c (n "lab") (v "0.5.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0067ra893zm6ivch57z6hw507l0vmj93rpqm0nw090rnrqbvxm2h")))

(define-public crate-lab-0.6.0 (c (n "lab") (v "0.6.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "08wvqvwcjf6idlmvkwgfj89gz2268g0ymc2252bibwzq6zpq5mzm")))

(define-public crate-lab-0.7.0 (c (n "lab") (v "0.7.0") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0rq8x9q53jkacd3mm577kjz7mky3jxl7sh86l53z9yi8rraalrhc")))

(define-public crate-lab-0.7.1 (c (n "lab") (v "0.7.1") (d (list (d (n "criterion") (r "^0.2") (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0zgvmaa3qj7ijq8q2s648lja13jmgj9kzw46chycrk91aj0688hm")))

(define-public crate-lab-0.7.2 (c (n "lab") (v "0.7.2") (d (list (d (n "criterion") (r "^0.3") (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0g692d489lq01pv3mzfhxd98j0r22lw28l6bk112m74djlfzxdmw")))

(define-public crate-lab-0.8.0 (c (n "lab") (v "0.8.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0r75153gxzmf9463h1dfm5xk2bhb056k2km4x2qd9qk5wi10qw31") (y #t)))

(define-public crate-lab-0.8.1 (c (n "lab") (v "0.8.1") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1ysnbviwi35mq6xyz9c59mpgigyfp4s4y2mispxzrms4vk83bx15")))

(define-public crate-lab-0.8.2 (c (n "lab") (v "0.8.2") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0ksv9nz2wazg1vrfm28jhbp0jfz2xdz8agkd00nl78ybvmqx05g9")))

(define-public crate-lab-0.9.0 (c (n "lab") (v "0.9.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1cj8wajn2ylrv2x9p18yypajva7wiin3lhj68sralraq5rk5skpn")))

(define-public crate-lab-0.10.0 (c (n "lab") (v "0.10.0") (d (list (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "095brkq0xip7g82hsx8smv9n63cllz22fmb8j84sjh355111z5d4")))

(define-public crate-lab-0.11.0 (c (n "lab") (v "0.11.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "13ymsn5cwl5i9pmp5mfmbap7q688dcp9a17q82crkvb784yifdmz")))

