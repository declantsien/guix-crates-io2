(define-module (crates-io #{3}# l lok) #:use-module (crates-io))

(define-public crate-lok-0.0.0-alpha.0 (c (n "lok") (v "0.0.0-alpha.0") (h "0wjlk6y3klbkwbxq3rh48acb9kjdr21ghk2smr8881gd5hlhqrpa")))

(define-public crate-lok-0.0.1 (c (n "lok") (v "0.0.1") (d (list (d (n "ace") (r "^0.0.2") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)))) (h "1bvndwadf1smd9vyfpb18ahv10xvxfvqz0d13sf4snww1vigm6gn")))

(define-public crate-lok-0.0.2 (c (n "lok") (v "0.0.2") (d (list (d (n "ace") (r "^0.0.2") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "17adc5lxjq9k9g6f8wczkx7r131jwmd8gm9s7pf7l8vd746nw02g")))

(define-public crate-lok-0.0.3 (c (n "lok") (v "0.0.3") (d (list (d (n "ace") (r "^0.0.2") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "01rvkfb8qk58zngzhmkz2alk1xzznn2hzw0ff1gkcq1qnm9av1vc")))

(define-public crate-lok-0.0.4 (c (n "lok") (v "0.0.4") (d (list (d (n "ace") (r "^0.0.2") (d #t) (k 0)) (d (n "bright") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "0niqyg38gdxnivmmky3qyyb9l5zq7cgkl741h8h06r52s5xi8zk8")))

(define-public crate-lok-0.0.5 (c (n "lok") (v "0.0.5") (d (list (d (n "ace") (r "^0.0.2") (d #t) (k 0)) (d (n "bright") (r "^0.2.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)))) (h "0gjgivchrv4dx86x21ppbgirs0gn8vwij24kh58hr50sigl82l98")))

(define-public crate-lok-0.0.6 (c (n "lok") (v "0.0.6") (d (list (d (n "ace") (r "^0.2.0") (d #t) (k 0)) (d (n "bright") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0p05pznspgds4xnklyasjlxh1ww17xsn17700fkwm2b29scj1wg0")))

(define-public crate-lok-0.0.7 (c (n "lok") (v "0.0.7") (d (list (d (n "ace") (r "^0.2.0") (d #t) (k 0)) (d (n "bright") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1089b9nbvdim3kc8qcvqrn1j1n0cm39fdzf4jwqvgzd2x4gc4h2g")))

(define-public crate-lok-0.0.8 (c (n "lok") (v "0.0.8") (d (list (d (n "ace") (r "^0.2.0") (d #t) (k 0)) (d (n "bright") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1rjwh26fqfpqmb6y7shk36pg6l679qhp000fasi2l1wk3fphw1zn")))

(define-public crate-lok-0.0.9 (c (n "lok") (v "0.0.9") (d (list (d (n "ace") (r "^0.2.0") (d #t) (k 0)) (d (n "bright") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "09cdhv630mnrd1w0gf4rhy00595qa8av9gc2305k6khbvy2wrw06")))

(define-public crate-lok-0.1.0 (c (n "lok") (v "0.1.0") (d (list (d (n "ace") (r "^0.2.0") (d #t) (k 0)) (d (n "bright") (r "^0.4.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0p3m54sy46ialikj28lvl15yc4a55m0qxgzs43zhz93vi8ilmmvz")))

(define-public crate-lok-0.1.1 (c (n "lok") (v "0.1.1") (d (list (d (n "bright") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.3") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0w4n66b3mv0952c2wqjxf8d65cwwz523vqgqwqxr227462d8k2qx")))

(define-public crate-lok-0.1.2 (c (n "lok") (v "0.1.2") (d (list (d (n "bright") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1y0kll7567rpd2wy5p6z8ms3p940h1q2lnych88aa98wypii2xy4")))

(define-public crate-lok-0.1.3 (c (n "lok") (v "0.1.3") (d (list (d (n "bright") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "04gidnwv7q8zn2nr5xb9ypzj1a42n0r91m2d6n80yv55hpqbs5j2")))

(define-public crate-lok-0.1.4 (c (n "lok") (v "0.1.4") (d (list (d (n "bright") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "07yb0bkqjw8y2x1z4vrkwapd4mci71yx2n5q4c0dymh9b98kh69d")))

(define-public crate-lok-0.1.5 (c (n "lok") (v "0.1.5") (d (list (d (n "bright") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0wajplqd3lnnl74z19r5bbyq6p764mdfndb0wbwlvayxcjqzmmfr")))

(define-public crate-lok-0.2.0 (c (n "lok") (v "0.2.0") (d (list (d (n "bright") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0wrkw442karjxdscvdfiaync1qgdckd3xb4saw83d9agd4pgr046")))

(define-public crate-lok-0.2.1 (c (n "lok") (v "0.2.1") (d (list (d (n "bright") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.1") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "13pcld23cg9ahs1pc5wxzb4g0ww5lvla3naw2ippz3w0qb1mh214")))

(define-public crate-lok-0.2.2 (c (n "lok") (v "0.2.2") (d (list (d (n "bright") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0f0wmjhn9l8421dp4g5bccnsc5smllz301875pl0vgicfr6cgywr")))

(define-public crate-lok-0.2.3 (c (n "lok") (v "0.2.3") (d (list (d (n "bright") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0zlhiiag6x65p818mkfw0fzha17zns851cf2qgsgz03017jdpwdx")))

(define-public crate-lok-0.2.4 (c (n "lok") (v "0.2.4") (d (list (d (n "bright") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "04kn2r2zxsnspr0ql72dq5s56s8zqvw1sn855sddx8ks8ixdhjxp")))

(define-public crate-lok-0.2.5 (c (n "lok") (v "0.2.5") (d (list (d (n "bright") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.8.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1nh2vj64djbwqf62wl3wjb38li4gnrhxznxmh7lpv33sljl78paj")))

