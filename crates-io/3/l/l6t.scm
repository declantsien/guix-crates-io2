(define-module (crates-io #{3}# l l6t) #:use-module (crates-io))

(define-public crate-l6t-0.1.0 (c (n "l6t") (v "0.1.0") (d (list (d (n "file") (r "^0") (d #t) (k 0) (p "l6t-file")) (d (n "symbolic") (r "^0") (o #t) (d #t) (k 0) (p "l6t-symbolic")))) (h "1jc549jv00g7hi5nh8709phljn469h8nz9p8df1i9g82qdsvj6ya") (f (quote (("default" "symbolic")))) (r "1.70")))

