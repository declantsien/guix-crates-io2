(define-module (crates-io #{3}# l lpr) #:use-module (crates-io))

(define-public crate-lpr-0.1.0 (c (n "lpr") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "username") (r "^0.2.0") (d #t) (k 0)))) (h "0dksjsdwdzhin0n2cliarvgxg4x3f8bj7fvhfkn0ysdg1b0rm23l")))

(define-public crate-lpr-0.1.1 (c (n "lpr") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "username") (r "^0.2.0") (d #t) (k 0)))) (h "00mhq0jiw43wrlzpra4bgr5sq7mghq1mfjfh6n4y7piy8gn63b0j")))

(define-public crate-lpr-0.1.2 (c (n "lpr") (v "0.1.2") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "username") (r "^0.2.0") (d #t) (k 0)))) (h "1ccsq5bfkc5bw1qp49nimggar05blhqpx2nkx7zbwd99s4mhpzyl")))

(define-public crate-lpr-0.1.3 (c (n "lpr") (v "0.1.3") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "username") (r "^0.2.0") (d #t) (k 0)))) (h "0mbxpnhg7phfkq2xf99rz3p8f9zys8xb007apmy5zkvrhq5fv4fd")))

(define-public crate-lpr-0.1.4 (c (n "lpr") (v "0.1.4") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "username") (r "^0.2.0") (d #t) (k 0)))) (h "0zp6dqifdwpr2hghwqwkf2ggnrvx36zz6pbdar01lxcwvazii790")))

(define-public crate-lpr-0.1.5 (c (n "lpr") (v "0.1.5") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "hostname") (r "^0.1.5") (d #t) (k 0)) (d (n "username") (r "^0.2.0") (d #t) (k 0)))) (h "0qc8114jzx2g5pswm6ym6ngk5ag7hb4aqjw0y21klsn8sh5nssmx")))

