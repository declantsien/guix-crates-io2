(define-module (crates-io #{3}# l led) #:use-module (crates-io))

(define-public crate-led-0.1.0 (c (n "led") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)))) (h "0a3glh7w4bq636ixz3l9v5fq59bib1z8vss6wvkfim6dr0h4ma0y")))

(define-public crate-led-0.1.1 (c (n "led") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)))) (h "1kgmlq1xj0fwzhgz65a2ym4g7kxna1rhnn4nwnii3gi728d9yrhw")))

(define-public crate-led-0.2.0 (c (n "led") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)))) (h "0mr820gi2i63srgcn1fyfv5bs5xq7vigipkq7kqw6n5grjcmzb0j")))

(define-public crate-led-0.3.0 (c (n "led") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0nxclk7ggqm5vdai8pqbf2dicp3lgm2kqiicvph01qsljgvxgs2f")))

(define-public crate-led-0.3.1 (c (n "led") (v "0.3.1") (d (list (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "0wk9xfm17cmkimx421mk44jli3kyb3h47gzwy9ziid9q60hazb46")))

