(define-module (crates-io #{3}# l lsk) #:use-module (crates-io))

(define-public crate-lsk-0.0.0 (c (n "lsk") (v "0.0.0") (h "0p0ny6mmaazl9b6yi0p3q56i8qaijbcil5qf4f4z4si081gm3ch9")))

(define-public crate-lsk-0.1.0 (c (n "lsk") (v "0.1.0") (d (list (d (n "ansi_term") (r "~0.12.0") (d #t) (k 0)) (d (n "cmd_lib") (r "~0.7.8") (d #t) (k 0)) (d (n "easy-hasher") (r "~2.1.1") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "~0.3.4") (d #t) (k 0)) (d (n "seahorse") (r "~0.7.1") (d #t) (k 0)) (d (n "sha2") (r "~0.8.2") (d #t) (k 0)) (d (n "term_grid") (r "~0.1.7") (d #t) (k 0)) (d (n "termion") (r "~1.5.5") (d #t) (k 0)) (d (n "walkdir") (r "~2.3.1") (d #t) (k 0)) (d (n "xdotool") (r "~0.0.2") (d #t) (k 0)))) (h "1yrn2rlv70523qhplw8waqsm48ljil2cvmxay5s0b3z5ns8d1rn2") (y #t)))

(define-public crate-lsk-0.2.0 (c (n "lsk") (v "0.2.0") (d (list (d (n "ansi_term") (r "~0.12.0") (d #t) (k 0)) (d (n "cmd_lib") (r "~0.7.8") (d #t) (k 0)) (d (n "easy-hasher") (r "~2.1.1") (d #t) (k 0)) (d (n "fuzzy-matcher") (r "~0.3.4") (d #t) (k 0)) (d (n "seahorse") (r "~0.7.1") (d #t) (k 0)) (d (n "sha2") (r "~0.8.2") (d #t) (k 0)) (d (n "term_grid") (r "~0.1.7") (d #t) (k 0)) (d (n "termion") (r "~1.5.5") (d #t) (k 0)) (d (n "walkdir") (r "~2.3.1") (d #t) (k 0)) (d (n "xdotool") (r "~0.0.2") (d #t) (k 0)))) (h "1769p0pj9zhmy89l8grsa2zyri5522vr6i9dk1lg6l30wzpzjl90")))

