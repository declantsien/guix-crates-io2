(define-module (crates-io #{3}# l lum) #:use-module (crates-io))

(define-public crate-lum-0.1.0 (c (n "lum") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "sqlx") (r "^0.7.3") (f (quote ("runtime-tokio" "any" "postgres" "mysql" "sqlite" "tls-native-tls" "migrate" "macros" "uuid" "chrono" "json"))) (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ka73vmb6i5jf0r8d0z77xxsk0nqpjj7bs5rzhv0vdii3s84pc1k")))

