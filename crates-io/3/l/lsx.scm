(define-module (crates-io #{3}# l lsx) #:use-module (crates-io))

(define-public crate-lsx-1.1.1 (c (n "lsx") (v "1.1.1") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)))) (h "1hg693f33001hwf984hl8rhcc6274ng8rakhzpylwy9waf5xrp2s") (f (quote (("twofish") ("sha256") ("default" "sha256" "twofish"))))))

(define-public crate-lsx-1.1.2 (c (n "lsx") (v "1.1.2") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)))) (h "001rwk9xj3nml0bszmxpfp45yxd6m3lm3sn1vaijrjvsrn1l4vfa") (f (quote (("twofish") ("sha256") ("default" "sha256" "twofish"))))))

