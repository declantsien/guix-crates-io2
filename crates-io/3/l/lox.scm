(define-module (crates-io #{3}# l lox) #:use-module (crates-io))

(define-public crate-lox-0.0.0 (c (n "lox") (v "0.0.0") (h "0k9bpkfw0628la82sp4x3hpn6pvnm0c6jzgadjvwphhkxnwvqg3k")))

(define-public crate-lox-0.1.0 (c (n "lox") (v "0.1.0") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "leer") (r "^0.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lina") (r "^0.1.4") (k 0)) (d (n "lox-macros") (r "=0.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (d #t) (k 0)) (d (n "stable-vec") (r "^0.4") (d #t) (k 0)) (d (n "typebool") (r "^0.1.0") (d #t) (k 0)))) (h "13zbzx4jp69h0wjgfm9x8qhdvyj2jhhz7myr5fw0md42ygpv9rfn") (f (quote (("large-handle"))))))

(define-public crate-lox-0.1.1 (c (n "lox") (v "0.1.1") (d (list (d (n "ahash") (r "^0.8.3") (d #t) (k 0)) (d (n "leer") (r "^0.1.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lina") (r "^0.1.4") (k 0)) (d (n "lox-macros") (r "=0.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (f (quote ("union"))) (d #t) (k 0)) (d (n "stable-vec") (r "^0.4") (d #t) (k 0)) (d (n "typebool") (r "^0.1.0") (d #t) (k 0)))) (h "0mnn95zx0d1982jzambxs850hq678cr2q319lya62kcavxyyvm6p") (f (quote (("large-handle"))))))

