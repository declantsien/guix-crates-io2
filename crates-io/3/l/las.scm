(define-module (crates-io #{3}# l las) #:use-module (crates-io))

(define-public crate-las-0.1.0 (c (n "las") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.3.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "1nbpbllx0c4wn4849hc5nbychjjvdingvn7z4p5lpvacq5jfqslc")))

(define-public crate-las-0.1.1 (c (n "las") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.3.9") (d #t) (k 0)) (d (n "getopts") (r "^0.2.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.14") (d #t) (k 0)))) (h "0b3ah540x0bhpwacjcqr3apnasxdxi5d1jasxk6zy9yx9jad2b59")))

(define-public crate-las-0.2.0 (c (n "las") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.0.2") (d #t) (k 0)) (d (n "num") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ym2x1ygz9dky47h9k6i2g8s3hch6mvxvvqixbx9d6vp8q338ji2")))

(define-public crate-las-0.2.1 (c (n "las") (v "0.2.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ryd7jr0f5hnsag7hw9fri738944z3vnjdbhfbdxk25j72rpb6fj")))

(define-public crate-las-0.3.0 (c (n "las") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)))) (h "0lrfidj7pzyw0gg7y40i4j9kxbdph682k7w0jv6wjnmp7wb3p54r")))

(define-public crate-las-0.3.1 (c (n "las") (v "0.3.1") (d (list (d (n "byteorder") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0888a7fj2jhzz37jwkf9laylq0lyjhsi9cgqispzbjabvs9mpvsn")))

(define-public crate-las-0.3.2 (c (n "las") (v "0.3.2") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "11ky4hhikj6zm9jk6k70fxj7ac3vr7a6clinchn98p5hv4xv983v")))

(define-public crate-las-0.3.3 (c (n "las") (v "0.3.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1mpwfi1f1vvsx463055k8ryxj842ify7jn3fjvbyyf1k052a2l9y")))

(define-public crate-las-0.3.4 (c (n "las") (v "0.3.4") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "111ilwc099fjmk38ypinyw5x0rw130rj5hylid42wx1inpd7j2k0")))

(define-public crate-las-0.4.0 (c (n "las") (v "0.4.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)))) (h "137hysfdwa52myb01dvhbrq99wmabqji8hiz34z3w0426ycd5qc5")))

(define-public crate-las-0.5.0 (c (n "las") (v "0.5.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0h8j7c143dkcsl0hygdwzq73pr4ipxdghvlpp2y60bi2nvk41wiq")))

(define-public crate-las-0.5.1 (c (n "las") (v "0.5.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0yyxx27kgv6ybpmkynrlnj6xr2kfgxwyair5qdk7ji97sj1rmixj")))

(define-public crate-las-0.5.2 (c (n "las") (v "0.5.2") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "19jn4qcy352m573y05qdzmsh6nwsxwphzqdcxxdrc0pn0mlj1z98")))

(define-public crate-las-0.6.0 (c (n "las") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (d #t) (k 0)))) (h "03a2yryfm299xynh53anxy46shivblplprqma9n3wsarhxnai1k0")))

(define-public crate-las-0.6.1 (c (n "las") (v "0.6.1") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "uuid") (r "^0.4") (d #t) (k 0)))) (h "1n3sf500pdjgigqp947ds9k389swjjv8g8xjxr9h1iqgkgmhid4x")))

(define-public crate-las-0.6.2 (c (n "las") (v "0.6.2") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "laz") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (d #t) (k 0)))) (h "0j5aifly0mv4ghcry32j323k3k3k0zb1hy8yp9rghg8fa17mxkdq") (y #t)))

(define-public crate-las-0.7.0 (c (n "las") (v "0.7.0") (d (list (d (n "byteorder") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "laz") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (d #t) (k 0)))) (h "11yazi0qws1y4l904bw1b4gcf39ycslfiaymg9w8dwpw2wznc21z")))

(define-public crate-las-0.7.1 (c (n "las") (v "0.7.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "laz") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "15kn2x8vxsmnp2mbsxa355a6251038mzlpzvfz02ikmk92n756dp")))

(define-public crate-las-0.7.2 (c (n "las") (v "0.7.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "laz") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1j10xi9f5vycbrwp1k4a7s4d87d1572yshj674x0jn9q112sghfd") (f (quote (("unstable"))))))

(define-public crate-las-0.7.3 (c (n "las") (v "0.7.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "laz") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0hlnvb2pbrnp6ahqwd2w8i2ghg1xvv8d43mi7m3f9ggaxvalydrx") (f (quote (("unstable"))))))

(define-public crate-las-0.7.4 (c (n "las") (v "0.7.4") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "laz") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0k6ij1cc05bkkpdqfqzwkb9cm9mvcy1srr0avf12b52sb6in230w") (f (quote (("unstable"))))))

(define-public crate-las-0.7.5 (c (n "las") (v "0.7.5") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "laz") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1ca4jvk237dyi2rla9drkdqlzzkb2hh7r8fkigq00d1kdvz13pc0") (f (quote (("unstable"))))))

(define-public crate-las-0.7.6 (c (n "las") (v "0.7.6") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "laz") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0zwv4p76pbv938a9glil59q65vdnrvjg4y10y623jfsfnrj5rb8i") (f (quote (("unstable"))))))

(define-public crate-las-0.7.7 (c (n "las") (v "0.7.7") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "laz") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1ippf5sdmmsshbmf50q1l23jwdrg650n25h2nz1a0l1n429gm3a8") (f (quote (("unstable"))))))

(define-public crate-las-0.7.8 (c (n "las") (v "0.7.8") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "laz") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1970qi9vxn5xf9g8wb7q8pk4pxnfwkf2r43sbbcvpgv9gvyyrw37") (f (quote (("unstable"))))))

(define-public crate-las-0.8.0 (c (n "las") (v "0.8.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "laz") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0zkrkgjy1min1s7kd6mcwqzvpnzaxjjnl51fz4gbs1vfhsd3yhpy")))

(define-public crate-las-0.8.1 (c (n "las") (v "0.8.1") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "laz") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1hx48qcsnvspwlnggba0qw6bhpxjzgb7sql7s8n20rai2dwd68b0")))

(define-public crate-las-0.8.2 (c (n "las") (v "0.8.2") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "laz") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1g8c7w7d8nx3w0gixv7z0w491yxwdfk4pbq9nrk9ck2xf8ysl6s6")))

(define-public crate-las-0.8.3 (c (n "las") (v "0.8.3") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "laz") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0l3nmpiyr7zzs1lb8ii6dcqaric1wj1zalygdx1n6lyz6ig7pyss")))

(define-public crate-las-0.8.4 (c (n "las") (v "0.8.4") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "laz") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "00gpqv4ypzpfjwprwl5pd05adyr47gpkvbpqhf5czfvhibqz3rfn") (s 2) (e (quote (("laz-parallel" "dep:laz" "laz/parallel") ("laz" "dep:laz"))))))

(define-public crate-las-0.8.5 (c (n "las") (v "0.8.5") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "laz") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0h1anbfdb1s1l40sawr6xwdbw3jz213bvm49jvn5nwfq3wbs94gp") (s 2) (e (quote (("laz-parallel" "dep:laz" "laz/parallel") ("laz" "dep:laz"))))))

(define-public crate-las-0.8.6 (c (n "las") (v "0.8.6") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "laz") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "05qmpwp692ppihr4pd23jxaq8s8w0i0npabq82lkqgn77hqwa88f") (s 2) (e (quote (("laz-parallel" "dep:laz" "laz/parallel") ("laz" "dep:laz"))))))

(define-public crate-las-0.8.7 (c (n "las") (v "0.8.7") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "laz") (r "^0.9.1") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0yzcggiim2lvp8f8c9ggs5l19w9gxcfikxhvrkcbaidv7brrkqw9") (s 2) (e (quote (("laz-parallel" "dep:laz" "laz/parallel") ("laz" "dep:laz"))))))

