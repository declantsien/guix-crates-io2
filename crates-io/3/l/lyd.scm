(define-module (crates-io #{3}# l lyd) #:use-module (crates-io))

(define-public crate-lyd-0.0.1-alpha.1 (c (n "lyd") (v "0.0.1-alpha.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "cpal") (r "^0.15.2") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "06w9331rg6915mc6xb11z8isk1gzsci21nmp1bmsm077vd8wkkf6")))

(define-public crate-lyd-0.0.1-alpha.2 (c (n "lyd") (v "0.0.1-alpha.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "cpal") (r "^0.15.2") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "0vw4dkbhdd0wsirq6ryfd8h3qk63vahwnh9jsidy8v06dm0p1w08")))

(define-public crate-lyd-0.0.1-alpha.3 (c (n "lyd") (v "0.0.1-alpha.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "cpal") (r "^0.15.2") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "1lcwp3i39ci6n71179rd0kmw3bjy5y3inag23lizvcyxkq98myww")))

(define-public crate-lyd-0.0.1 (c (n "lyd") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "cpal") (r "^0.15.2") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "libm") (r "^0.2.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "167f7f1xsxi7mw48r5p32f9lhyqcm8bibr3960a7n96j77qrii02") (f (quote (("no_std" "libm") ("default"))))))

(define-public crate-lyd-0.0.2 (c (n "lyd") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 2)) (d (n "cpal") (r "^0.15.2") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.13.2") (d #t) (k 0)) (d (n "libm") (r "^0.2.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10.0") (d #t) (k 0)))) (h "1xh1wd5q3lqpc62pj00lgi3xgbcdcqhr2gzg5jn5c1hhjviz571h") (f (quote (("no_std" "libm") ("default"))))))

