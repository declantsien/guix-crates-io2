(define-module (crates-io #{3}# l lvm) #:use-module (crates-io))

(define-public crate-lvm-0.1.0 (c (n "lvm") (v "0.1.0") (d (list (d (n "errno") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "lvm-sys") (r "~0.1") (d #t) (k 0)) (d (n "uuid") (r "~0.7") (d #t) (k 0)))) (h "1yc4a73fshx7lnsbm9wm2d6qx9prhxkx11bcxvk9sfjmfpnz435i")))

(define-public crate-lvm-0.2.0 (c (n "lvm") (v "0.2.0") (d (list (d (n "errno") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "lvm-sys") (r "~0.1") (d #t) (k 0)) (d (n "uuid") (r "~0.7") (d #t) (k 0)))) (h "0kp4yab911bfdmbwlkn6i6a795gq1s80c59baf2rfhfwx3yk50rk")))

(define-public crate-lvm-0.3.0 (c (n "lvm") (v "0.3.0") (d (list (d (n "errno") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "lvm-sys") (r "~0.1") (d #t) (k 0)) (d (n "uuid") (r "~0.7") (d #t) (k 0)))) (h "1qhbl870dy7icmd7ba7yrvsld98h95h1bvl4lfnckb42gkvchns7")))

(define-public crate-lvm-0.3.1 (c (n "lvm") (v "0.3.1") (d (list (d (n "errno") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "lvm-sys") (r "~0.1") (d #t) (k 0)) (d (n "uuid") (r "~0.7") (d #t) (k 0)))) (h "18f9q6z2hxnglbng88fpr72ahcn23ylzmqprnxll1vpblfccncw4")))

