(define-module (crates-io #{3}# l lnk) #:use-module (crates-io))

(define-public crate-lnk-0.1.0 (c (n "lnk") (v "0.1.0") (d (list (d (n "bitflags") (r "~1.2") (d #t) (k 0)) (d (n "byteorder") (r "~1.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1sh6rbh1z4p717bf3cnfaxm6kki0lazczcqcnm2mkbzykpvcpnpz")))

(define-public crate-lnk-0.2.0 (c (n "lnk") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "1mk27bray8kqlqyx3c0dw4b6qsgf1fwvkfp5ickjx4wi42w37j6i")))

(define-public crate-lnk-0.2.1 (c (n "lnk") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "0zpbfwqws7yzdm36809krica6smadp4q8jf9hl7q9rf6j7sacxda")))

(define-public crate-lnk-0.3.0 (c (n "lnk") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "1ci20chgh263i0zr0f6y4ry0cdlj44rwanpylwbxnim4sh8k9z2s")))

(define-public crate-lnk-0.4.1 (c (n "lnk") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "1fx0bm1c9jgd62fjmirma2npfyyzbg8kz3mr9g25pmmhxbm2rhkl") (f (quote (("experimental_save") ("default"))))))

(define-public crate-lnk-0.5.0 (c (n "lnk") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "16vfzdn65xjk8bglq3yn2avj4acq8hzii2wzsp2n4jzwi93n6y1r") (f (quote (("experimental_save") ("default"))))))

(define-public crate-lnk-0.5.1 (c (n "lnk") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)))) (h "0b0v88ra3xv1gjv4q6nvs3gab8iz1qkw2164axxp4lfsshlwwrp0") (f (quote (("experimental_save") ("default"))))))

