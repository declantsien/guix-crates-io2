(define-module (crates-io #{3}# l lig) #:use-module (crates-io))

(define-public crate-lig-0.1.0 (c (n "lig") (v "0.1.0") (d (list (d (n "gaze") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "ligature") (r "^0.5") (d #t) (k 0)))) (h "0gy0vvz58jm6i05m8x7q59vjnlh1d8adlip3m9nhyxq8wvrshaq5")))

