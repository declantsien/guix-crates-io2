(define-module (crates-io #{3}# l lci) #:use-module (crates-io))

(define-public crate-lci-0.1.0 (c (n "lci") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-name") (r "^0.7.0") (d #t) (k 0)))) (h "0v7yxl0hpa66f5bjzbh581pi53wmfx2z5n7czki03d4q7q7nb182")))

(define-public crate-lci-0.1.1 (c (n "lci") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-name") (r "^0.7.0") (d #t) (k 0)))) (h "0g7i1nc2fl4crrxrrg9zhdnsrj6wazrcpf641zjlmjqjni2v5c1h") (f (quote (("debug"))))))

(define-public crate-lci-0.1.2 (c (n "lci") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-name") (r "^0.7.0") (d #t) (k 0)))) (h "0fhvyy4gz7dpjhv0fw88764kpclcmzm4z4265pa5sgkz536cb8km") (f (quote (("debug"))))))

(define-public crate-lci-0.1.3 (c (n "lci") (v "0.1.3") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-name") (r "^0.7.0") (d #t) (k 0)))) (h "03g2d1z3vvc86pj7sf4i9w7msqq4gwv0sb81j46gi96bi8i5hpx1") (f (quote (("debug"))))))

(define-public crate-lci-0.1.4 (c (n "lci") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-name") (r "^0.7.0") (d #t) (k 0)))) (h "16g14wfzfmjjgdzpiywb5kw5ji0czrdcl2x3vrnibpdd1h48yaij") (f (quote (("debug"))))))

(define-public crate-lci-0.1.5 (c (n "lci") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-name") (r "^0.7.0") (d #t) (k 0)))) (h "1p84a09srznnl94dd6q5yr70m1kfppw4jmihydkdg81psjhq6khf") (f (quote (("debug"))))))

(define-public crate-lci-0.1.6 (c (n "lci") (v "0.1.6") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-name") (r "^0.7.0") (d #t) (k 0)))) (h "0q6db04srcbfbxsshlamf7q9p21l6id1asmh8s8vx0cmblbjai6k") (f (quote (("debug"))))))

(define-public crate-lci-0.1.7 (c (n "lci") (v "0.1.7") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-name") (r "^0.7.0") (d #t) (k 0)))) (h "1pwlggxd4c0g6yqlglbb783s10j0qna3fq19c4a1hsq890v1sag5") (f (quote (("debug"))))))

(define-public crate-lci-0.1.8 (c (n "lci") (v "0.1.8") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "unic-char-range") (r "^0.7.0") (d #t) (k 0)) (d (n "unic-ucd-name") (r "^0.7.0") (d #t) (k 0)))) (h "13ry5d7rvgzma548fknn96szdz1rngp3vibma2kfhwc25afmvarw") (f (quote (("debug"))))))

