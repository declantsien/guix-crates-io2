(define-module (crates-io #{3}# l ltc) #:use-module (crates-io))

(define-public crate-ltc-0.1.0 (c (n "ltc") (v "0.1.0") (h "13kf6i3h23zawdnxfmshldw81jqf4f8snc2d1qldk35a846s2i5q") (y #t)))

(define-public crate-ltc-0.1.1 (c (n "ltc") (v "0.1.1") (h "0mhgiifnxyp5d4iwh7bzx8cj0za6kqzkgwdc9h0g1b1xgzh0x2a0")))

(define-public crate-ltc-0.2.0 (c (n "ltc") (v "0.2.0") (h "03gw2crjrrswz6rym9iar2pmrkv94p2fip7aifpb0i7p55gqb4kg")))

