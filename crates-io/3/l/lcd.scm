(define-module (crates-io #{3}# l lcd) #:use-module (crates-io))

(define-public crate-lcd-0.1.1 (c (n "lcd") (v "0.1.1") (d (list (d (n "fast_fmt") (r "^0.1.3") (o #t) (k 0)))) (h "044asyrkc46y0ahvpmrdbb56mpqqwybfyh07jbjwani5ixz1k1k2") (f (quote (("fast-format" "fast_fmt") ("default"))))))

(define-public crate-lcd-0.2.0-pre (c (n "lcd") (v "0.2.0-pre") (d (list (d (n "fast_fmt") (r "^0.1.3") (o #t) (k 0)) (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)))) (h "1ybzipfdif66yavfg10yrz55p2g9hnk2y72x5qwydhxl26d541cs") (f (quote (("fast-format" "fast_fmt") ("default")))) (y #t)))

(define-public crate-lcd-0.2.0 (c (n "lcd") (v "0.2.0") (d (list (d (n "fast_fmt") (r "^0.1.3") (o #t) (k 0)) (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)))) (h "1yf0j5jcnnsdm0s9jigdf3wrm1y0d49g2773h6vvrahgjsq7p7cp") (f (quote (("fast-format" "fast_fmt") ("default"))))))

(define-public crate-lcd-0.2.1 (c (n "lcd") (v "0.2.1") (d (list (d (n "fast_fmt") (r "^0.1.3") (o #t) (k 0)) (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)))) (h "1v0ck9myzczhb7770s3lnkmfyvs20k4fgpxr83hh2iv1mc10zhd5") (f (quote (("fast-format" "fast_fmt") ("default"))))))

(define-public crate-lcd-0.3.0 (c (n "lcd") (v "0.3.0") (d (list (d (n "fast_fmt") (r "^0.1.3") (o #t) (k 0)) (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)))) (h "1czlsm4lcckcscsy8rr4xham2zw9lrw8zjbzgwfsgl9lcw7bmwyf") (f (quote (("fast-format" "fast_fmt") ("default"))))))

(define-public crate-lcd-0.4.0-alpha.0 (c (n "lcd") (v "0.4.0-alpha.0") (d (list (d (n "pretty_assertions") (r "^0.3.4") (d #t) (k 2)))) (h "03bqln9sb4knn3cab5agmp5jb8d6l5nkkvwmb4aivpw2wlgxgs42")))

(define-public crate-lcd-0.4.0 (c (n "lcd") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "1aidfwn8q04p2d574x71jl274yw2x3hlw44mpysz4amcsq3gxbxm")))

(define-public crate-lcd-0.4.1 (c (n "lcd") (v "0.4.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "09f34g2lp4wjv4rqayriydajyjhkrqy4jhdk6yhvvh32l7cqy7cp")))

