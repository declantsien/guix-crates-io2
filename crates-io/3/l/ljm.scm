(define-module (crates-io #{3}# l ljm) #:use-module (crates-io))

(define-public crate-ljm-0.1.0 (c (n "ljm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "ljm-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1xpmmmkvp89pgr35mwp5as7jgspk0g06f2m7njklh48fnkzy27lw")))

(define-public crate-ljm-0.1.1 (c (n "ljm") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "ljm-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0l59x17qc28imp87ipnjjb38qbm89l2ffn63yajh39ylc6prpysm")))

(define-public crate-ljm-0.2.0 (c (n "ljm") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "ljm-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "03wsx1nci1ipi665vxjcyzlamc1ga5wp5mhibkffir88kahl8yv6")))

(define-public crate-ljm-0.2.1 (c (n "ljm") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "ljm-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "15sjdlg0gbpv8ingpaqgdj5kc484y4zx7fs5a4d99j6f84h4sx43")))

(define-public crate-ljm-0.3.0 (c (n "ljm") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "ljm-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)))) (h "0zfad8rw28k0qzsi463w14kz2byfv1lyd4xzbgf54smx6ggvc1n7")))

