(define-module (crates-io #{3}# l lav) #:use-module (crates-io))

(define-public crate-lav-0.1.0 (c (n "lav") (v "0.1.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "15fqry0l8drxxj6jk2gnqwj9kxjqv7z59k2jh7fb77ni88bagcs7")))

(define-public crate-lav-0.1.1 (c (n "lav") (v "0.1.1") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0ga64pzqmc2l3y5vw9rgci8clz7cywrpkg2swd1z1a275z3pllvw")))

(define-public crate-lav-0.1.2 (c (n "lav") (v "0.1.2") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1w90nar1qxzkfgh44y59bg74mljlmmjbdw22n0p2l2mvqzka8f12")))

(define-public crate-lav-0.2.0 (c (n "lav") (v "0.2.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1d92sv0zdb10ykyffsabsv6lxapsgzzm3vnfkkdpdnx0ia30kjcc")))

(define-public crate-lav-0.2.1 (c (n "lav") (v "0.2.1") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1cl39d8zkh7sp4c715wlxrjdf4ayaf3k6r3b6iigxswz0i0kfqji")))

(define-public crate-lav-0.3.0 (c (n "lav") (v "0.3.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0d8blz6qmxzq1p3c02w3cwf5sl09gb2kkcvz8c2nqa0biam3qds8")))

(define-public crate-lav-0.4.0 (c (n "lav") (v "0.4.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0m9i6a1n1w7fl452pvwg3rbngj2lpir577dwkzg7g9fj01ayl5n1")))

(define-public crate-lav-0.5.0 (c (n "lav") (v "0.5.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0xr4pmamdckfqq21scfkgzgyr5b50c6781k8zy3xhly7l8dnbn1s")))

(define-public crate-lav-0.5.1 (c (n "lav") (v "0.5.1") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1bvi0709j17kr0xshmk8r1qxwzbd9kp8nk7xlx3p7glsfmx83vsb")))

(define-public crate-lav-0.6.0 (c (n "lav") (v "0.6.0") (d (list (d (n "libm") (r "^0.2") (o #t) (d #t) (k 0)))) (h "14ka2xxjnhgx1y4ncwz1j2mvlz5ckiinjfqa926lx3zmy7jg4mml")))

(define-public crate-lav-0.7.0 (c (n "lav") (v "0.7.0") (d (list (d (n "libm") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1pfzi9l0y12nplzbpq2w3dnviv4pmyk4902kd4cg0plpfnrkagff")))

(define-public crate-lav-0.7.1 (c (n "lav") (v "0.7.1") (d (list (d (n "libm") (r "^0.2.5") (o #t) (d #t) (k 0)))) (h "0wckb40blf4jnfwc76crwv10jhb2dw0dlrfb87857kc77v2vzx3j")))

(define-public crate-lav-0.7.2 (c (n "lav") (v "0.7.2") (d (list (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "10swzfyryaj78wpf1v9csd6f4nh77fgi5wdpk0ff6gvv629z087l")))

(define-public crate-lav-0.7.3 (c (n "lav") (v "0.7.3") (d (list (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "target-features") (r "^0.1.2") (o #t) (d #t) (k 0)))) (h "03h5sq709xw1vmk98l5r3s2sd7bkmjvqgz0wzh7h3awq40y8yv71")))

(define-public crate-lav-0.7.4 (c (n "lav") (v "0.7.4") (d (list (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "target-features") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0vcc8k3nifyv2p5zz6grq162za2dkganhl58hwx4cpvwv0mr2daw")))

(define-public crate-lav-0.7.5 (c (n "lav") (v "0.7.5") (d (list (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)) (d (n "target-features") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "11g7gjdr3v8qpn08cicinrrg442fhwfznw3xsbmbqrfcnf691jwp")))

(define-public crate-lav-0.7.6 (c (n "lav") (v "0.7.6") (d (list (d (n "libm") (r "^0.2.7") (o #t) (d #t) (k 0)) (d (n "target-features") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0rk1lcadvny8z0br7vr3d5sdra6fmqvbl1i66fcd2qph05x15871")))

(define-public crate-lav-0.7.7 (c (n "lav") (v "0.7.7") (d (list (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "target-features") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0pfg2rv4dcl2a8fxfid9w8krgnp1gbrxaf3wlr9vfg9hpw2hbip0")))

(define-public crate-lav-0.7.8 (c (n "lav") (v "0.7.8") (d (list (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "target-features") (r "^0.1.5") (o #t) (d #t) (k 0)))) (h "0115wkvrikv7169z7s4kgn2ih82ynsy3rasi055lw3h0hv6va9bv")))

(define-public crate-lav-0.8.0 (c (n "lav") (v "0.8.0") (d (list (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "target-features") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "0big453a3s035mzc8qli1wi77qc0g7yf28pzlvlkq4lkj473102v")))

(define-public crate-lav-0.8.1 (c (n "lav") (v "0.8.1") (d (list (d (n "libm") (r "^0.2.8") (o #t) (d #t) (k 0)) (d (n "target-features") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "0vpy5yvcwdmglc9wa4nc3agp2rmzcd4c1fn32qdl2y1y184c8smd")))

