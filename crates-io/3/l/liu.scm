(define-module (crates-io #{3}# l liu) #:use-module (crates-io))

(define-public crate-liu-0.0.1 (c (n "liu") (v "0.0.1") (d (list (d (n "aliu") (r "0.1.*") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "region") (r "^3.0.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0h30akj1z55my7shvrby11k0w5nbisl409xqbjzrnxa9hnpzlahf")))

