(define-module (crates-io #{3}# l lz4) #:use-module (crates-io))

(define-public crate-lz4-1.0.127 (c (n "lz4") (v "1.0.127") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "09ryz8brmdf77cgi9p5arvsl5kvh9xvp9dqndf8qzpk3ik8w256p")))

(define-public crate-lz4-1.1.127 (c (n "lz4") (v "1.1.127") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "08qf9af15f6i7p1gcf0iyw04qprxryclcmszk9wjzj6zsag8914g")))

(define-public crate-lz4-1.2.127 (c (n "lz4") (v "1.2.127") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "0cqpwxywqjw2d4s21g60cnxd1lslcql441wq342qvqr3nl98wg3c")))

(define-public crate-lz4-1.3.127 (c (n "lz4") (v "1.3.127") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1imq42nvrfjk21mbqvxrdy15ijkypp6vnzj7jzz1znqkcvidazbs")))

(define-public crate-lz4-1.4.127 (c (n "lz4") (v "1.4.127") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "162w88rbx2q6amksivlj7bgkhqd9rhy0izgsvb47fz3qs04fskqj")))

(define-public crate-lz4-1.4.128 (c (n "lz4") (v "1.4.128") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0iymg9r7g84vgys4d9p0bwxsqcbzy2j1s239pasiiy3725by0q1r")))

(define-public crate-lz4-1.5.128 (c (n "lz4") (v "1.5.128") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "13j5vrw44bmymw4y5c47mqcax3v8w2vwpwlbnyicfajd0bp9s8k4")))

(define-public crate-lz4-1.5.129 (c (n "lz4") (v "1.5.129") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "107v29nb6s19acmv7smkyixk0g75q3vrk5b2hgyjjww4qkrhysmv")))

(define-public crate-lz4-1.6.129 (c (n "lz4") (v "1.6.129") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "07pzf6avzmjcwqzvsrj7x5lzwcjpfjg09jhyihdpd5vmnjpdw3v3")))

(define-public crate-lz4-1.7.129 (c (n "lz4") (v "1.7.129") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1m3i4cpvamq2wzycvzjfa5w70fzsqxsih0985z8bm03f9zmkx6ip")))

(define-public crate-lz4-1.8.131 (c (n "lz4") (v "1.8.131") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1g1mmy3d5m2i2gn09py04yqfv5vzcs6apwjfzh597cs58zz7lhkb")))

(define-public crate-lz4-1.9.131 (c (n "lz4") (v "1.9.131") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0iisb5940il3ww6pn1lh168ph6fbiyvdrmi9kvg92sxkgxkgzj7x")))

(define-public crate-lz4-1.10.131 (c (n "lz4") (v "1.10.131") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1dxq59d0lflp9f5vf3mdbvnp5mkpjkm5lxxa29a5kfhw3srvz6hw")))

(define-public crate-lz4-1.11.131 (c (n "lz4") (v "1.11.131") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0x1zfmzf6g0dswi35laagp32ayah6k9dddhylzzs180b2na969gr")))

(define-public crate-lz4-1.12.131 (c (n "lz4") (v "1.12.131") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "06lv512napdahfhpm13af0mhggmn7r2vjwafa66y410xl6xm78j3")))

(define-public crate-lz4-1.13.131 (c (n "lz4") (v "1.13.131") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)))) (h "08i7fhpfmxhqwh9gggghpzairypqypzhhmdm43fvhcg7pijv20ng")))

(define-public crate-lz4-1.14.131 (c (n "lz4") (v "1.14.131") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)))) (h "03xw722bd3rb8bac62n4jp4j5rjhv6pc18symlkv4nxrhwyadipv")))

(define-public crate-lz4-1.15.131 (c (n "lz4") (v "1.15.131") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)))) (h "0xkj1ibyznsqhz76170dnsqml6s6m052mykqc4jgcvzdipr5z1da")))

(define-public crate-lz4-1.16.131 (c (n "lz4") (v "1.16.131") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "skeptic") (r "^0.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.4") (d #t) (k 2)))) (h "1sg1lq1bjvkcpvkyxzpax5gr9y2q9f7c3kvd7a09xrxc8773319c")))

(define-public crate-lz4-1.17.131 (c (n "lz4") (v "1.17.131") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "skeptic") (r "^0.4") (d #t) (k 1)) (d (n "skeptic") (r "^0.4") (d #t) (k 2)))) (h "01lgk310rdshs21h05q4f21f3ah48qngxkpcsg6j4rabf86dq9hx")))

(define-public crate-lz4-1.18.131 (c (n "lz4") (v "1.18.131") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "^0.2.5") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 2)))) (h "1g8jzg06fip1r2rdd2pwlidl004cgjiqpbycjlzpm5mgby7248bx")))

(define-public crate-lz4-1.19.131 (c (n "lz4") (v "1.19.131") (d (list (d (n "gcc") (r "^0.3.35") (d #t) (k 1)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 2)))) (h "0vw2rq9dygshv2nfim8qmq4j4q4plrzibjrhqmx4fnwq082w0i4d")))

(define-public crate-lz4-1.19.173 (c (n "lz4") (v "1.19.173") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 2)))) (h "0g0q54zklfsp21adcldqfsbzpa8i62dprrkpqhz4w3gbnwbxpirj")))

(define-public crate-lz4-1.20.0 (c (n "lz4") (v "1.20.0") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)) (d (n "lz4-sys") (r "^1.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 2)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 1)) (d (n "skeptic") (r "^0.6.1") (d #t) (k 2)))) (h "0vd25rag69x6k64r1i1q3ndkhriq3ajkmrlbym3v4p5wcnigvmhr")))

(define-public crate-lz4-1.21.0 (c (n "lz4") (v "1.21.0") (d (list (d (n "gcc") (r "^0.3.49") (d #t) (k 1)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "lz4-sys") (r "^1.7.5") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 2)))) (h "1sky8pvhhpkrd11a1m1qdidh9130yh32dxskg5aq09gfin642ca2")))

(define-public crate-lz4-1.21.1 (c (n "lz4") (v "1.21.1") (d (list (d (n "gcc") (r "^0.3.49") (d #t) (k 1)) (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "lz4-sys") (r "^1.7.5") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 2)))) (h "04qp6qjdy9qwwgj37p8rjc1nlfj8bkbl79vp2iak2svcm5g1qzrg")))

(define-public crate-lz4-1.22.0 (c (n "lz4") (v "1.22.0") (d (list (d (n "libc") (r "^0.2.23") (d #t) (k 0)) (d (n "lz4-sys") (r "^1.8.0") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 2)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 1)) (d (n "skeptic") (r "^0.9.0") (d #t) (k 2)))) (h "037q8847kahksqj13g2hdv80cyyr2vs17z7vws3wjkrfpkmx4mgy")))

(define-public crate-lz4-1.23.0 (c (n "lz4") (v "1.23.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "lz4-sys") (r "^1.8.2") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "0gl55adpr57m096p0z9wwviqf67nqxhvrgy5br5s8h5l0xbqi810")))

(define-public crate-lz4-1.23.1 (c (n "lz4") (v "1.23.1") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "lz4-sys") (r "^1.8.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 1)) (d (n "skeptic") (r "^0.13.3") (d #t) (k 2)))) (h "1m93vh4pvwi7b7r4zdhd5gclrnwi88ywj302fgrif05616glmja3")))

(define-public crate-lz4-1.23.2 (c (n "lz4") (v "1.23.2") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "lz4-sys") (r "^1.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "0k3j1wsgn4c5ys4vma326r00g5rq5ggp7k385rmby08yk7b0xhma")))

(define-public crate-lz4-1.23.3 (c (n "lz4") (v "1.23.3") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lz4-sys") (r "^1.9.3") (d #t) (k 0)) (d (n "rand") (r ">=0.7, <=0.8") (d #t) (k 2)))) (h "11d8s7wg7y2491l7409pqckcihb807xykaz5b727bhxia51bkp2f")))

(define-public crate-lz4-1.24.0 (c (n "lz4") (v "1.24.0") (d (list (d (n "docmatic") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lz4-sys") (r "^1.9.4") (d #t) (k 0)) (d (n "rand") (r ">=0.7, <=0.8") (d #t) (k 2)))) (h "1wad97k0asgvaj16ydd09gqs2yvgaanzcvqglrhffv7kdpc2v7ky")))

