(define-module (crates-io #{3}# l lzd) #:use-module (crates-io))

(define-public crate-lzd-0.1.0 (c (n "lzd") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1crkipxwq6hcnscg8dnblz66gaml6gf8s9zp0y19xsxzmliy3fih") (f (quote (("debug_print") ("cli_test" "assert_cmd"))))))

(define-public crate-lzd-0.1.1 (c (n "lzd") (v "0.1.1") (d (list (d (n "assert_cmd") (r "^1.0.7") (o #t) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1qc8iprqm1bwl6x1dss4yp439ng17hgdxfzvd2isamwayplh6dz8") (f (quote (("default") ("debug_print") ("cli_test" "assert_cmd"))))))

