(define-module (crates-io #{3}# l lbs) #:use-module (crates-io))

(define-public crate-lbs-0.1.0 (c (n "lbs") (v "0.1.0") (d (list (d (n "lbs_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "16cnbm5syhd3aqglzm6zld9vwsvqcvx850kxzv8l2zrwc4lj82cp")))

(define-public crate-lbs-0.2.0 (c (n "lbs") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "lbs_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "0i4lj0998lg42qv4sq2gv8b63n9mqjng8szms6fac7hh56bdx8pb") (f (quote (("default"))))))

(define-public crate-lbs-0.3.0 (c (n "lbs") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ipnet") (r "^2.3.0") (o #t) (d #t) (k 0)) (d (n "lbs_derive") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.6.1") (o #t) (d #t) (k 0)))) (h "1jlxcswyapi6ds8phm6y8w0vm7r2vm3yybai64z28cqlfvwk7rh6") (f (quote (("default"))))))

(define-public crate-lbs-0.3.1 (c (n "lbs") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (o #t) (d #t) (k 0)) (d (n "lbs_derive") (r "^0.3.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (o #t) (d #t) (k 0)))) (h "1as8py734dwh7r9idqgwl1y9rwn50qziy1ypv08ych4fy5qny248") (f (quote (("default"))))))

(define-public crate-lbs-0.4.0 (c (n "lbs") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (o #t) (d #t) (k 0)) (d (n "lbs_derive") (r "^0.4.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1ad0c5nlqg2x5w0ps2hdh72mzxg3pbnz9r2p58h8mxcvch8c5s8p") (f (quote (("default"))))))

(define-public crate-lbs-0.4.1 (c (n "lbs") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (o #t) (d #t) (k 0)) (d (n "lbs_derive") (r "^0.4.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0nqwrnmknz878w8nanx4qq76d0kia7libzzcrqglb6ass506nm51") (f (quote (("default"))))))

(define-public crate-lbs-0.4.2 (c (n "lbs") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "fraction") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (o #t) (d #t) (k 0)) (d (n "lbs_derive") (r "^0.4.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (o #t) (d #t) (k 0)))) (h "0kjc1a2n7677wma5xcx47lm6c933r1wf4l9jwcvc2faah3wliakb") (f (quote (("default"))))))

(define-public crate-lbs-0.4.3 (c (n "lbs") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "fraction") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "ipnet") (r "^2.3") (o #t) (d #t) (k 0)) (d (n "lbs_derive") (r "^0.4.1") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.11") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "time") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.6") (o #t) (d #t) (k 0)))) (h "1mddr3nj187xr2w1y502x0fwlm4qhzlfp7n5ky18vwad2bfpyfna") (f (quote (("default"))))))

