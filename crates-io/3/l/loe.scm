(define-module (crates-io #{3}# l loe) #:use-module (crates-io))

(define-public crate-loe-0.1.0 (c (n "loe") (v "0.1.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "yansi") (r "^0.4") (d #t) (k 0)))) (h "0w9rx1n9j4bmf70khhd85kpga71akpyx06ar2kgp11sbs7jxydsv")))

(define-public crate-loe-0.2.0 (c (n "loe") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "yansi") (r "^0.4") (d #t) (k 0)))) (h "0sjlkrcb6v05a4ak3m337360yyimmy8b9lxn8xb5k3698rwp5d0f")))

(define-public crate-loe-0.2.1 (c (n "loe") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "yansi") (r "^0.4") (d #t) (k 0)))) (h "1fc76nbjwywc2k71q0zvhllgkq24vjc2rd5p12q98bg6q1xsqn6z")))

(define-public crate-loe-0.3.0 (c (n "loe") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "proptest") (r "^0.8.7") (d #t) (k 2)) (d (n "yansi") (r "^0.4") (d #t) (k 0)))) (h "1jmlvb2jb947jidih00ksi83ngn44i45nhhi3y4x4gib54w67rby")))

