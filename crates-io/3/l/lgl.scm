(define-module (crates-io #{3}# l lgl) #:use-module (crates-io))

(define-public crate-lgl-0.1.0 (c (n "lgl") (v "0.1.0") (h "193qaq2yhqzfgrzcglh2xqxlfhabm9p2k5s19ywwghx30xips7k7")))

(define-public crate-lgl-0.1.1 (c (n "lgl") (v "0.1.1") (h "1vy3i56znidkj330x2k266xvjqq6g82cbryr6126rcvglkpcq4rb")))

