(define-module (crates-io #{3}# l lhi) #:use-module (crates-io))

(define-public crate-lhi-0.0.0 (c (n "lhi") (v "0.0.0") (d (list (d (n "kern") (r "^1.0.3") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "1028d5sdgwd88sh3nd01qg4dlhs7bhrcg0dwp24n69nzmm6974z3")))

(define-public crate-lhi-0.0.0-dev.0 (c (n "lhi") (v "0.0.0-dev.0") (d (list (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "1aiwvnw20r0maszl0lnw4jglycphvyqjhrickv1ap34dqdnhnvh4")))

(define-public crate-lhi-0.0.0-dev.1 (c (n "lhi") (v "0.0.0-dev.1") (d (list (d (n "kern") (r "^1.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "0lvma54b8g5636a5bag69i7i7l02qax13bdpy51v1xxp9g4xi9vd")))

(define-public crate-lhi-0.0.0-dev.2 (c (n "lhi") (v "0.0.0-dev.2") (d (list (d (n "kern") (r "^1.1.2") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "1icp8pf3i6n3l0d2na43rwrpj36f5d55850w6jn3pkflvkhzsplm")))

(define-public crate-lhi-0.0.0-dev.3 (c (n "lhi") (v "0.0.0-dev.3") (d (list (d (n "kern") (r "^1.1.3") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "10gyr5i97vxg8fxv9xy61sgpnmppccv187va44nhnlzq5piflpk2")))

(define-public crate-lhi-0.0.0-dev.4 (c (n "lhi") (v "0.0.0-dev.4") (d (list (d (n "kern") (r "^1.1.3") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "1wlf4f3zqznvvq2pwrfcxhcqjj9zhly98i4gf8ndzg533aj7cvhv")))

(define-public crate-lhi-0.0.0-dev.5 (c (n "lhi") (v "0.0.0-dev.5") (d (list (d (n "kern") (r "^1.1.3") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "1293b5ri7k5fda4cshxrrjrav6q7ign9995c1hi9zvkp4xaq1p6f")))

(define-public crate-lhi-0.0.0-dev.6 (c (n "lhi") (v "0.0.0-dev.6") (d (list (d (n "kern") (r "^1.1.3") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "1b0677c9gmy3ki6vbhxzlinqmgnzdi6b7p41cg4g90d35nawczmv")))

(define-public crate-lhi-0.0.0-dev.7 (c (n "lhi") (v "0.0.0-dev.7") (d (list (d (n "kern") (r "^1.1.4") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "1qr1azzd16nb3a76ckc50gk9knkb83a38wpzviz3l12ihzjwsyfz")))

(define-public crate-lhi-0.0.0-dev.8 (c (n "lhi") (v "0.0.0-dev.8") (d (list (d (n "kern") (r "^1.1.4") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "0f5yfksrqjbyifh4dw8jkqy9liwg4rlk9ps5nia4x1gd570izmpx")))

(define-public crate-lhi-0.0.0-dev.9 (c (n "lhi") (v "0.0.0-dev.9") (d (list (d (n "kern") (r "^1.1.4") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "19fkdpvjxvh3vphldz6aqjyq4azbmkd30a7xr3h0ihsxh45cbrh3")))

(define-public crate-lhi-0.0.0-dev.10 (c (n "lhi") (v "0.0.0-dev.10") (d (list (d (n "kern") (r "^1.1.4") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "1jyvhcgbmj0zpysngz74vm71zgwcgflvldzp9n0swni466lbn60w")))

(define-public crate-lhi-0.0.0-dev.11 (c (n "lhi") (v "0.0.0-dev.11") (d (list (d (n "kern") (r "^1.1.4") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "0aw2n4nwc7hcmbzgjjg0bx2gfnkll63ix7pr3jc37f58s2xbngis")))

(define-public crate-lhi-0.0.0-dev.12 (c (n "lhi") (v "0.0.0-dev.12") (d (list (d (n "kern") (r "^1.1.4") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "1y365njjil449m3zcyipmn4l0m4h86axamwms8w4mb87kjzynw6w")))

(define-public crate-lhi-0.0.0-dev.13 (c (n "lhi") (v "0.0.0-dev.13") (d (list (d (n "kern") (r "^1.1.6") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "0l8fsxpy15v3j4i40fkg9pmim9hixcj9l6xfk47rjl772xfzha6p")))

(define-public crate-lhi-0.0.0-dev.14 (c (n "lhi") (v "0.0.0-dev.14") (d (list (d (n "kern") (r "^1.1.6") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "0hr4if9adbvbd2ay9rvb4pgya9n6s01gd17kx8bkmchgai9w8xap")))

(define-public crate-lhi-0.0.1 (c (n "lhi") (v "0.0.1") (d (list (d (n "kern") (r "^1.1.6") (d #t) (k 0)) (d (n "rustls") (r "^0.17.0") (d #t) (k 0)))) (h "0lj8jv22x3vp9racnvjp3fcy5145nw1bnxpbjciz255g97p17rqh")))

(define-public crate-lhi-0.0.2 (c (n "lhi") (v "0.0.2") (d (list (d (n "kern") (r "^1.1.6") (d #t) (k 0)) (d (n "rustls") (r "^0.18.0") (d #t) (k 0)))) (h "0pdkvcdq70fsa3qs7bbl6m2q6s7fp8z1ssrxn0zmbpx4gc2vdng3")))

