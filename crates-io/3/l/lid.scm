(define-module (crates-io #{3}# l lid) #:use-module (crates-io))

(define-public crate-lid-0.1.0 (c (n "lid") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (f (quote ("spin_mutex"))) (k 0)))) (h "1wkvqqglh8gxnjdgfkkhvp2nc8ykhfmnnfb89rinz6h355l3aswl")))

(define-public crate-lid-0.1.1 (c (n "lid") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (f (quote ("spin_mutex"))) (k 0)))) (h "19gb9a4w88bcz4r88qhpj5dc0bmn676jdhihh1pd1g2d4fxk3scp")))

(define-public crate-lid-0.1.2 (c (n "lid") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (f (quote ("spin_mutex"))) (k 0)))) (h "01zvv8xdfwdzpzsizvw7xba0frpw1sc6s51392p0gcg7ar286bnc")))

(define-public crate-lid-0.2.0 (c (n "lid") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (f (quote ("spin_mutex"))) (k 0)))) (h "013lzc6avq3hpdi98762pgiw9n1j1wycwpm7h602s9lvkfkp0am4") (f (quote (("default" "base36") ("base62") ("base36") ("base32"))))))

(define-public crate-lid-0.2.1 (c (n "lid") (v "0.2.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spin") (r "^0.9.8") (f (quote ("spin_mutex"))) (k 0)))) (h "1habdf605z0nivc99hsk40av5gkw37rqcbfgm1wn6p21avs3q8v1") (f (quote (("default" "base36") ("base62") ("base36") ("base32"))))))

(define-public crate-lid-0.3.0 (c (n "lid") (v "0.3.0") (d (list (d (n "colorid") (r "^0") (d #t) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "nanoid") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "snowflaked") (r "^1") (d #t) (k 2)) (d (n "spin") (r "^0.9") (f (quote ("spin_mutex"))) (o #t) (k 0)))) (h "0jfrg7ll449g78j0a80dqn1izpgfiicxjya0wng2arzm5hwcg5sy") (f (quote (("no-unsafe") ("easy" "lazy_static" "spin") ("default" "base36") ("base62") ("base36") ("base32"))))))

