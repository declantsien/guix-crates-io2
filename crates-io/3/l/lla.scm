(define-module (crates-io #{3}# l lla) #:use-module (crates-io))

(define-public crate-lla-0.1.0 (c (n "lla") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0b5bp8vga886r57aladks770i8919myc6kwwxppvjf8ib3qxy6px") (y #t)))

(define-public crate-lla-0.1.1 (c (n "lla") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "1pkjxq416f3pmiqb3llhqic3n5mdlvb4w9540kb118jh1s2z21xy") (y #t)))

(define-public crate-lla-0.1.2 (c (n "lla") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0zdj6z7g8jffv0m1ghdx5dn342my3xny9sf21d1i6lv4h300xnx1")))

(define-public crate-lla-0.1.3 (c (n "lla") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "17vx44ia72ggqhc5kvcwfyfssr0flvsrcnzcq6bbflgbg1jvs7zz")))

(define-public crate-lla-0.1.4 (c (n "lla") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0ars02n1yp0wd63mm0w19rnz3v7s5992kb7ff8r4c614klkqmn9x")))

(define-public crate-lla-0.1.5 (c (n "lla") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0j1dh366bwjgc1ky1v2fldqhyx1rsxgilql1dgdqsw6j754ay94q")))

(define-public crate-lla-0.1.6 (c (n "lla") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0m00msfln99drz92l33d76fymy4s3hwxnq8jdrmjx5pngv3qkmjr")))

(define-public crate-lla-0.1.7 (c (n "lla") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^3.2.25") (d #t) (k 0)) (d (n "git2") (r "^0.17.2") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "188fnmm9v0m1fgxnbwk3ij7yrk320gzpkxmz1xwvfi1afzf52vqz")))

