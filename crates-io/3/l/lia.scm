(define-module (crates-io #{3}# l lia) #:use-module (crates-io))

(define-public crate-lia-0.1.0 (c (n "lia") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.11.0") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.11.0") (d #t) (k 0)))) (h "1s55k1ilw71h39h0a1f5ndrjsyw3rwq5pbr4i5ym6ncdw0gn3mw5")))

