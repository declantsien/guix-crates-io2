(define-module (crates-io #{3}# l le2) #:use-module (crates-io))

(define-public crate-le2-0.0.0 (c (n "le2") (v "0.0.0") (h "1bwkx4p2flm08qcmagm8faj3l20liwp0xk11wn822x7y9bp0l48m") (y #t)))

(define-public crate-le2-0.0.1 (c (n "le2") (v "0.0.1") (h "1rvwn2sxls6akldiznq38vsmi0z4k8sjb8dqqrwlh94rj1d2raz4") (y #t)))

(define-public crate-le2-0.9.0 (c (n "le2") (v "0.9.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "19rg12msyphjnwq74v8a0g8vdci8b6wg2kb6dpm0zvf7cm11gb2l") (y #t)))

(define-public crate-le2-0.9.1 (c (n "le2") (v "0.9.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "1n7zjyyi4zi5ca0dnav5aga97j1smccf42x13cd4ys09dyw7p261") (y #t)))

(define-public crate-le2-0.9.2 (c (n "le2") (v "0.9.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "exec") (r "^0.3.1") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 1)))) (h "1b55ipc8k0bwb2bvv4h0lyr1df0mj9hza8vw39d51vl83a0ly3h5") (y #t)))

