(define-module (crates-io #{3}# l lnx) #:use-module (crates-io))

(define-public crate-lnx-0.1.0 (c (n "lnx") (v "0.1.0") (h "00m9v8hdfj0ishhrgx92p23h2z8864269m0j1ygl79bdryncmqlr")))

(define-public crate-lnx-0.1.1 (c (n "lnx") (v "0.1.1") (h "1rdcbid2nncgfnp7flx8yqiqa95z66j5m8ipn4lwpq0vpnkpmp67")))

(define-public crate-lnx-0.1.2 (c (n "lnx") (v "0.1.2") (h "15sg35r9jyvlbxxwv5qxcism76b9wm8wzmnx14v66fajm7jx7p2i")))

(define-public crate-lnx-1.0.0 (c (n "lnx") (v "1.0.0") (h "0lya5bcnskz1c49racqg53cjgjmfg1g86rb4rcb5izs63lqjwbsb")))

