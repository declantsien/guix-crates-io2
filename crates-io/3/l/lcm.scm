(define-module (crates-io #{3}# l lcm) #:use-module (crates-io))

(define-public crate-lcm-0.1.0 (c (n "lcm") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "102pimiqka1dadhv76d7n4gr78z60qr73jmcia0rjg54vmr1fsgg")))

(define-public crate-lcm-0.2.0 (c (n "lcm") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0f6r0g1a4453ympnfnx9zrj6vwbz5cx3cnag0qc8f83jqv9chni7")))

