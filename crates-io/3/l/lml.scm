(define-module (crates-io #{3}# l lml) #:use-module (crates-io))

(define-public crate-lml-0.0.0 (c (n "lml") (v "0.0.0") (h "0j0f8ky6mld4jiyxamy70ynhng0xq8k2q567bj1z4k2sf74ncr5d") (f (quote (("default"))))))

(define-public crate-lml-0.1.0 (c (n "lml") (v "0.1.0") (h "1p6mkvf0k6rk04y80jdl1lqbn6k26176l0pl3ra9kwvr1k7dgb8j") (f (quote (("default"))))))

