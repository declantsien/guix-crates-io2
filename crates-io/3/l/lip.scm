(define-module (crates-io #{3}# l lip) #:use-module (crates-io))

(define-public crate-lip-0.1.0 (c (n "lip") (v "0.1.0") (d (list (d (n "im") (r "^14.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1rh0jrpbz3wmrfpg46d9n14pl2jdy6h98z8ah38p6kwbfqdzrj4k")))

(define-public crate-lip-0.1.1 (c (n "lip") (v "0.1.1") (d (list (d (n "im") (r "^14.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1kwlx8p76xzdp81n57p25gmvig8lg0pd1z9yfzax1885lckd7hvd")))

(define-public crate-lip-0.1.2 (c (n "lip") (v "0.1.2") (d (list (d (n "im") (r "^14.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0479wq637k0lp9yixrdsgjiqb2qnvya3zcmx2kqmymzgp2j2vj5h")))

(define-public crate-lip-1.2.2 (c (n "lip") (v "1.2.2") (d (list (d (n "im") (r "^14.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05b144pdjjqg116lxs4vdx6rjszd98i47jz1v1wxk2w5r03zwqmw")))

(define-public crate-lip-1.2.3 (c (n "lip") (v "1.2.3") (d (list (d (n "im") (r "^14.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1j108ci59l4vaga9jw7vfz6pxdmij4zg9lfpdad3m53hp42gvp0s")))

(define-public crate-lip-1.2.4 (c (n "lip") (v "1.2.4") (d (list (d (n "im") (r "^14.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0px9d4496hd75q43i9yfwi0i22vwv122fz1lvxaglnnvlkpmq8h3")))

(define-public crate-lip-2.0.0 (c (n "lip") (v "2.0.0") (h "1i813ywgy5y9aghn3mq573ygqnl7m5qbqqcfc5s7klm3sbslmag3")))

(define-public crate-lip-3.0.0 (c (n "lip") (v "3.0.0") (h "160fdn9rkgskh14sii7frw60rws2x95k4l7dqmq3jhk09klqcpqc")))

(define-public crate-lip-3.1.0 (c (n "lip") (v "3.1.0") (h "0z7765lqik7wnbdz6yn4w3jmh67wh230v7ks2xp644gp4qm6zkmn")))

(define-public crate-lip-4.0.0 (c (n "lip") (v "4.0.0") (h "0jkyycyvllhc65v5rpzaz7i1ki87k969irc82i34as58gs37msfc")))

(define-public crate-lip-4.1.0 (c (n "lip") (v "4.1.0") (h "1zmmf1vnd6mc7bv0vfpj8xailf6n7yk83yv89y2slaassh8nlip1")))

(define-public crate-lip-4.1.1 (c (n "lip") (v "4.1.1") (h "0fbxr99pzxjkfiz69hw55096p098mk5vh7apcgpb7pf3ahxz7ayk")))

(define-public crate-lip-4.1.2 (c (n "lip") (v "4.1.2") (h "0qdrppnz9ipzn0cq47w6y9j30m57pqxifyr8g74q5kxzc8kw47sw")))

(define-public crate-lip-4.1.3 (c (n "lip") (v "4.1.3") (h "0g6c07c3mi352xnz20887xyv3p66xr5lp399i0cbd0rivj4sjfpj")))

(define-public crate-lip-5.0.0 (c (n "lip") (v "5.0.0") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0si4ck7fnm6wd8a9asg19hzxpb0kms0pz4fwi45k6za3if2q896b")))

(define-public crate-lip-5.1.0 (c (n "lip") (v "5.1.0") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "1ywl0m6pqrcjaszvm3slyw4kbxq2vxs0zd64iaivx35iffl1gcjc")))

(define-public crate-lip-5.1.1 (c (n "lip") (v "5.1.1") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "0w33lpvkhb19p8f7b3c49s9m28r08j93z4fzpxla2zz5rfsjms7w")))

(define-public crate-lip-5.2.1-beta.0 (c (n "lip") (v "5.2.1-beta.0") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)))) (h "18k9hxv8yrk86yy7924nf3k8qigv7qjk07mny55b09gly6b4sfs7") (y #t)))

(define-public crate-lip-6.0.0 (c (n "lip") (v "6.0.0") (d (list (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.3.0") (d #t) (k 0)))) (h "0skq5599k168slr6ihrqdvk3dhmfh2v2c11iyjpa4xd3y01cba3d")))

(define-public crate-lip-7.0.0 (c (n "lip") (v "7.0.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.3.0") (d #t) (k 0)))) (h "0g4wdnbhyifa7jrfyjgmqm5vrqzhhy4j40fy3dnfvc2p3h92mvqp")))

(define-public crate-lip-7.0.1 (c (n "lip") (v "7.0.1") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.3.0") (d #t) (k 0)))) (h "18h2n6cy00srj37dm8qpv5g3gry2b3r4ffffj2pzrnqr21m0p373")))

(define-public crate-lip-7.0.2 (c (n "lip") (v "7.0.2") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.3.0") (d #t) (k 0)))) (h "1gwhb3qrycqf8mm54yf7dw24c0ckzy66d7zil6gc6s62jwmjzkby")))

(define-public crate-lip-8.0.0 (c (n "lip") (v "8.0.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "partial_application") (r "^0.2.1") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.3.0") (d #t) (k 0)))) (h "1g7d1bf70pwj7vypg5qkbvn83q28nh8ii6xgv1jyj0a2yaxlzqff")))

(define-public crate-lip-8.0.1 (c (n "lip") (v "8.0.1") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "partial_application") (r "^0.2.1") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.3.0") (d #t) (k 0)))) (h "1cnwkggx68wl9626r48jifpf3baj2nfwnj2lzq3j4gr1ripanlr1")))

(define-public crate-lip-8.0.2 (c (n "lip") (v "8.0.2") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "partial_application") (r "^0.2.1") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.3.0") (d #t) (k 0)))) (h "19dfpf7hvdb1zs52kzzlyz3hvpw4h8bwnb7z8vr93x7i6wzgcvsg")))

(define-public crate-lip-8.1.0 (c (n "lip") (v "8.1.0") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "partial_application") (r "^0.2.1") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.3.0") (d #t) (k 0)))) (h "0k19hmi78pwnnmgx2flfb6c9absgpy05il19yy9fc0qd4ydfg5kq")))

(define-public crate-lip-8.1.1 (c (n "lip") (v "8.1.1") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "partial_application") (r "^0.2.1") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.3.0") (d #t) (k 0)))) (h "131jpsrhmbzf5b96swxibnq8iqmaaq15f8jjjam1cxzpg9nprqgp")))

(define-public crate-lip-8.1.2 (c (n "lip") (v "8.1.2") (d (list (d (n "criterion") (r "^0.5") (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "partial_application") (r "^0.2.1") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.8.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "xi-unicode") (r "^0.3.0") (d #t) (k 0)))) (h "06z1gpxgbn9xg27v2hhwkc465fnwqvgbczzwcrysjc1p91jqd5yx")))

