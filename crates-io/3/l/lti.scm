(define-module (crates-io #{3}# l lti) #:use-module (crates-io))

(define-public crate-lti-0.1.0 (c (n "lti") (v "0.1.0") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "1i65n35gki6wgqsbva21br4hwdziky6hhhlwb276d2jn17bpwqh3")))

(define-public crate-lti-0.2.0 (c (n "lti") (v "0.2.0") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "17ppy8m61s2qa91w2b2mjx9mw7mml3dj5s6ic10f3w7jz38d4z09")))

(define-public crate-lti-0.2.1 (c (n "lti") (v "0.2.1") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "0nm6mmgvpl03ismr7aksabkqb28n9bwh9cbvf3pwca62332yzlp0")))

(define-public crate-lti-0.2.2 (c (n "lti") (v "0.2.2") (d (list (d (n "base64") (r "^0.5") (d #t) (k 0)) (d (n "ring") (r "^0.11") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "06zy0if9gki98w3hhqnf4qrb22lx2yxwyq16lidi6ssk6mk38d6f")))

(define-public crate-lti-0.3.0 (c (n "lti") (v "0.3.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "ring") (r "^0.17.0-alpha.8") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "16z8ds478q4g9a8abwz83ac5p5q40dbc5ii5z5a759qmgfg26727")))

(define-public crate-lti-0.3.1 (c (n "lti") (v "0.3.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "ring") (r "^0.16.20") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.0") (d #t) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "16iypc60q3hd2vaj9klzhay04kmj015wfx4mwqak8hcdw91k9gm5")))

