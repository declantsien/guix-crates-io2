(define-module (crates-io #{3}# l lie) #:use-module (crates-io))

(define-public crate-lie-0.1.0 (c (n "lie") (v "0.1.0") (d (list (d (n "approx") (r "^0.4.0") (f (quote ("num-complex"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13.1") (f (quote ("openblas"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.3.1") (d #t) (k 0)))) (h "0xwik6jx7dygkfi4viyhq3glgh137431krfyialrfg7ijzbaghaf")))

(define-public crate-lie-0.1.1 (c (n "lie") (v "0.1.1") (d (list (d (n "approx") (r "^0.4.0") (f (quote ("num-complex"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13.1") (f (quote ("openblas-system"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.3.1") (d #t) (k 0)))) (h "1pcb9h19399jny1pvgikyb7yba9pzvkmlvanwk4bm5748n6gcggz")))

(define-public crate-lie-0.1.2 (c (n "lie") (v "0.1.2") (d (list (d (n "approx") (r "^0.4.0") (f (quote ("num-complex"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.14.0") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13.1") (f (quote ("openblas-system"))) (d #t) (k 0)) (d (n "num-complex") (r "^0.3.1") (d #t) (k 0)))) (h "0pnid4a0b5mknnfa6k8dd0ar2llrz4gzdrkvc4xj4flq32z3dr4r")))

