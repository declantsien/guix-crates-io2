(define-module (crates-io #{3}# l leg) #:use-module (crates-io))

(define-public crate-leg-0.1.0 (c (n "leg") (v "0.1.0") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)))) (h "1nif4ha20409gqpd672kdavqyryp6h7aajp0m3s99i2rdyy85j5p") (y #t)))

(define-public crate-leg-0.1.1 (c (n "leg") (v "0.1.1") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)))) (h "1z0n70cb9gc96hr1hj1ks02jka0k94xhi8gdcqfjfsjbl86h4xg3") (y #t)))

(define-public crate-leg-0.1.2 (c (n "leg") (v "0.1.2") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)))) (h "04bvlk8hzsq69ly37323mmlxw7qc7jvj65xl8djn82agdlim5yqx")))

(define-public crate-leg-0.1.3 (c (n "leg") (v "0.1.3") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)))) (h "0j0p89ncmdiz0p3pk2mpf3pm7nbjkf9gnw7kymxm0v0hhjbrpy4p") (y #t)))

(define-public crate-leg-0.2.0 (c (n "leg") (v "0.2.0") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.8") (o #t) (d #t) (k 0)))) (h "1d03i20983kyk8b18941a2za7wbj2iix6yg7yfr2w415p51z501q") (f (quote (("log_crate" "env_logger" "log") ("default")))) (y #t)))

(define-public crate-leg-0.3.0 (c (n "leg") (v "0.3.0") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)))) (h "00sjyb70l3q5n6n02hv0qnanhz3dyw8nc1lf80z82i22hi4mr0cz") (y #t)))

(define-public crate-leg-0.4.0 (c (n "leg") (v "0.4.0") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)))) (h "08nmrz0r926dk4laprbkmskgsr4cqdmbclbppi59nhrmdivklzav") (y #t)))

(define-public crate-leg-0.4.1 (c (n "leg") (v "0.4.1") (d (list (d (n "colored") (r "^1.8.0") (d #t) (k 0)))) (h "1ciijj2vq1gd4cyk5ywxvqi5b8b2r074nf6646i7h2c0119s5ns9") (y #t)))

(define-public crate-leg-0.4.2-alpha.0 (c (n "leg") (v "0.4.2-alpha.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1g376wad8dqx09yw0jciszbvqndgsc2244cbs299wyibwlax3akh") (y #t)))

(define-public crate-leg-1.0.0 (c (n "leg") (v "1.0.0") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0qw880i9pnxbx4wn8w3xgml65mip16pzphsiq7fklfglh1c6d4lm") (y #t)))

(define-public crate-leg-1.0.1 (c (n "leg") (v "1.0.1") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0lypj0s9cy54mi3zrlp26ljs992m1h9a6kk2lbs0fz4lgjwqpbgw") (y #t)))

(define-public crate-leg-1.0.2 (c (n "leg") (v "1.0.2") (d (list (d (n "async-std") (r "^1.9.0") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1x8vihwgfsa71hry5blhpxs1p2k1g73s2sa1y5bnidqxgsixvs55") (y #t)))

