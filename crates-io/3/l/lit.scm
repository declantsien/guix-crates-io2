(define-module (crates-io #{3}# l lit) #:use-module (crates-io))

(define-public crate-lit-0.1.0 (c (n "lit") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "0cqzqcamh2xg1zc172wyf19ss3vrvh6r9b3cnywbbdm4gw8gybhw")))

(define-public crate-lit-0.2.0 (c (n "lit") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0vsnnj779xxqb98php4mz2cfr5lgksgimndpidh80jwcxlq986sh")))

(define-public crate-lit-0.2.1 (c (n "lit") (v "0.2.1") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "010xf4fr4jafbha07q0gmlqrki87fzr0kc7k03nqg2bvg0f79pqn")))

(define-public crate-lit-0.2.2 (c (n "lit") (v "0.2.2") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0s76b8rxwk74imlg1kzy9p9ikkhg3p5w31rd7q6sznaya8gnahhy")))

(define-public crate-lit-0.2.3 (c (n "lit") (v "0.2.3") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "1lhm6gcz4nam1s7d9m03r52ricvfxyp6xhwy1vmv744gr28h1xks")))

(define-public crate-lit-0.2.4 (c (n "lit") (v "0.2.4") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "07qnwjhjqkf2af8iprymm1zrqi0dsvv2vb3bin87z925iil8rk1b")))

(define-public crate-lit-0.2.5 (c (n "lit") (v "0.2.5") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0ip12hskpmzps567yhr8q7vcbdxz3q5lxfmgrra8v8bsxrlz3s7w")))

(define-public crate-lit-0.2.6 (c (n "lit") (v "0.2.6") (d (list (d (n "argparse") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^1.0") (d #t) (k 0)))) (h "0aqd6gny0mk706g55pqq43lkyrz0q3azjbm1p3gm6ahkvplyjkw6")))

(define-public crate-lit-0.2.7 (c (n "lit") (v "0.2.7") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.0") (d #t) (k 0)))) (h "1zlixjxykssvmxsb5vksba393ycp3v83sw586d0fxa191qpv3idc")))

(define-public crate-lit-0.2.8 (c (n "lit") (v "0.2.8") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.1") (d #t) (k 0)))) (h "0krhhss4y2br6m1apcbhkr8vvhyvqwy6qjp0z0impd76kknyi7qv")))

(define-public crate-lit-0.3.0 (c (n "lit") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.1") (d #t) (k 0)))) (h "1iaz2bsamvxf4ps29q918x3lv63dzgl6zh91sr1lp51j0fj1b9d8")))

(define-public crate-lit-0.3.1 (c (n "lit") (v "0.3.1") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "17kjv9sg82d1xjpj94bbsz6psk85pf1973cd0ip523bg7qp6caxq")))

(define-public crate-lit-0.3.2 (c (n "lit") (v "0.3.2") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "00n6k4cw4i6spfgn2dhkm8l5g4bi8wa5f2353mr1a31h67r5qzqz")))

(define-public crate-lit-0.3.3 (c (n "lit") (v "0.3.3") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "14p41ycbf1xg2m2ks9hj527ddpwv55y7vqx65acp7hi3vjfv7a62")))

(define-public crate-lit-0.3.4 (c (n "lit") (v "0.3.4") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.0") (d #t) (k 0)) (d (n "term") (r "^0.5") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1wi9yz7iix0w6m8kxz9fdwppmxja0lrwjg1slm3a0b5dvn5n4v0x")))

(define-public crate-lit-0.4.0 (c (n "lit") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0gp7271nvq86clf2wvli42f8s9jh48399sa81ggq2ilhlpwcfiy3")))

(define-public crate-lit-1.0.0 (c (n "lit") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1fn85ayb28ydkgrniipk1fdgn58np3595x0np6ywgigdmgw2w2ac") (f (quote (("default" "clap"))))))

(define-public crate-lit-1.0.1 (c (n "lit") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0jv7f3m4pkcs139p72201p4crq8p1yy3xz2vipl7ljf72ra9njb3") (f (quote (("default" "clap"))))))

(define-public crate-lit-1.0.2 (c (n "lit") (v "1.0.2") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "07xz0pqqb2w8d54x0wz46j9cwq53bq8049yn6b5k556cfwpkn3ih") (f (quote (("default" "clap"))))))

(define-public crate-lit-1.0.3 (c (n "lit") (v "1.0.3") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0hr1x0y1d4ng1vmmi4xf5hxpxvg6yhy6ngh6kiy5fgqnrjb6ddv6") (f (quote (("default" "clap"))))))

(define-public crate-lit-1.0.4 (c (n "lit") (v "1.0.4") (d (list (d (n "clap") (r "^2.33") (o #t) (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)) (d (n "term") (r "^0.6") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "1i9r1c6l02jn7h0c51d2y991pp6jy67yvakl68rhis84ys0b8mnn") (f (quote (("default" "clap"))))))

