(define-module (crates-io #{3}# l lxi) #:use-module (crates-io))

(define-public crate-lxi-0.1.0 (c (n "lxi") (v "0.1.0") (h "16cx953y5gw3mvqv1gvxdhz2k1l30m8dsdsrbl7hhb75128plgdd")))

(define-public crate-lxi-0.1.2 (c (n "lxi") (v "0.1.2") (h "0k9lmn2zdf7i0nflldr7chl80f5ab4briznl77xakh45rrfsf1cn")))

(define-public crate-lxi-0.1.1 (c (n "lxi") (v "0.1.1") (h "09nzqipmr0rhzfai0zkw2yrbbvjyl8dw1zxlx5kmhridak0i4x4b")))

(define-public crate-lxi-0.1.3 (c (n "lxi") (v "0.1.3") (h "0kcs2cw62vi2pc9xxw89p2kvvj4gqyzwlbng4w0ajk506skghjdn")))

