(define-module (crates-io #{3}# l lde) #:use-module (crates-io))

(define-public crate-lde-0.1.1 (c (n "lde") (v "0.1.1") (h "0sybmi8jqswnsk023lb996canl8b28kkk9y556fgq2kzfb4n8ww6")))

(define-public crate-lde-0.2.0 (c (n "lde") (v "0.2.0") (h "16zhadg89fidsjhwvw16prhw91pwfk9dqdk986r541i2zbjqsgag")))

(define-public crate-lde-0.3.0 (c (n "lde") (v "0.3.0") (h "1nnlwc9sk17waaw555hvbmjk5np38mf75n8z1xipigmcc818153h")))

