(define-module (crates-io #{3}# l lex) #:use-module (crates-io))

(define-public crate-lex-0.1.0 (c (n "lex") (v "0.1.0") (h "1la10kg38sqm23y2hx8wh48xnbrn84avachs8rc6x56srivyva06")))

(define-public crate-lex-0.2.0 (c (n "lex") (v "0.2.0") (h "17gz48x82jpq9v475cnz9lhqxmrjb2vvw3z3471ffrb9q7j5liwv")))

(define-public crate-lex-0.3.0 (c (n "lex") (v "0.3.0") (h "18wsl6pk5ydn7l08h8g99swm5r4fr9rn5mhxpay24ir72iv1qxgd")))

(define-public crate-lex-0.4.0 (c (n "lex") (v "0.4.0") (h "0rgq3k3fs7350xr97wa1gpk0jlhyxk4jhb5551pj6k3csb35dkb0")))

(define-public crate-lex-0.5.0 (c (n "lex") (v "0.5.0") (h "0k94vkbf32ly32zix1pjaa1czxl0j3xvlcr4ly1vy930z4v8yk7j")))

(define-public crate-lex-0.6.0 (c (n "lex") (v "0.6.0") (h "1bk55n91zj33706pihy13pbqgp1nj4a2lhq7vb4f5nmh86ry0k2d")))

(define-public crate-lex-0.7.0 (c (n "lex") (v "0.7.0") (h "0w3z3blbkypr2ym3aqb170wncik8zdnbx6aspc52ca8z4zc79l1g")))

