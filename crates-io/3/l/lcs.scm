(define-module (crates-io #{3}# l lcs) #:use-module (crates-io))

(define-public crate-lcs-0.1.0 (c (n "lcs") (v "0.1.0") (h "1prkmx5fxvsv6jg978hlfixyy5wj3czbk68sjcsqlsncx2ih64nv")))

(define-public crate-lcs-0.1.1 (c (n "lcs") (v "0.1.1") (h "08bmx1qc1qhygmc8m173izyi6ri6fgx9ykl408ps6j4b3qfm7mmx")))

(define-public crate-lcs-0.2.0 (c (n "lcs") (v "0.2.0") (h "0hazw664p9qnr248nlgh6xyypw4qs8825m4my9n7l40xnnbqs13h")))

