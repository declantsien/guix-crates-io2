(define-module (crates-io #{3}# l lxc) #:use-module (crates-io))

(define-public crate-lxc-0.1.0 (c (n "lxc") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.1") (d #t) (k 0)))) (h "0n12f583ihxd33fdgwk3sqdkg2bibs9jx79l8im6301q5j24h578")))

(define-public crate-lxc-0.2.0 (c (n "lxc") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.1") (d #t) (k 0)))) (h "09v685cgid161vydfjz4ynnfddx1bhjsnr98ppic1dqsaa79ci2l") (f (quote (("v3_0" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

(define-public crate-lxc-0.2.1 (c (n "lxc") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.1") (d #t) (k 0)))) (h "1mgr81amvyzfl7862dl6w87mm7wjiggkls4v6czbrjplvlgniqkp") (f (quote (("v3_0" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

(define-public crate-lxc-0.3.0 (c (n "lxc") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.2") (d #t) (k 0)))) (h "0c09iwwnniq32xxqmk9scbxyr4rfabidwxc59cjj52i7baz5c5av") (f (quote (("v3_2" "v3_1") ("v3_1" "v3_0") ("v3_0" "v2_1") ("v2_1" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

(define-public crate-lxc-0.3.1 (c (n "lxc") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.2") (d #t) (k 0)))) (h "0sv75cl645g693xrnp8ad6m4myi1m9irz8dq68w6q1g2dnq30yzj") (f (quote (("v3_2" "v3_1") ("v3_1" "v3_0") ("v3_0" "v2_1") ("v2_1" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

(define-public crate-lxc-0.4.0 (c (n "lxc") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.2") (d #t) (k 0)))) (h "180kjajlf9062sm3ixb09pj2xkkzgj1ms16vnkirqvzlbvdcffl5") (f (quote (("v4_0" "v3_2") ("v3_2" "v3_1") ("v3_1" "v3_0") ("v3_0" "v2_1") ("v2_1" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

(define-public crate-lxc-0.4.1 (c (n "lxc") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.2") (d #t) (k 0)))) (h "18r4ccrs02hn21gmrxc0hv23r7ig9yc8mbn3lkbs42rp9h775gpi") (f (quote (("v4_0" "v3_2") ("v3_2" "v3_1") ("v3_1" "v3_0") ("v3_0" "v2_1") ("v2_1" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

(define-public crate-lxc-0.4.3 (c (n "lxc") (v "0.4.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.2") (d #t) (k 0)))) (h "11i2lr8jwfgc46wf78yr8lnl866n5s5f1957d7100c7g3wdwmza5") (f (quote (("v4_0" "v3_2") ("v3_2" "v3_1") ("v3_1" "v3_0") ("v3_0" "v2_1") ("v2_1" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

(define-public crate-lxc-0.5.0 (c (n "lxc") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.3") (d #t) (k 0)))) (h "1g0cd20wb23r4mk4picz7p1pc9hggpai27k7jf8r8b7h6arabvfg") (f (quote (("v4_0" "v3_2") ("v3_2" "v3_1") ("v3_1" "v3_0") ("v3_0" "v2_1") ("v2_1" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

(define-public crate-lxc-0.6.0 (c (n "lxc") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.4") (d #t) (k 0)))) (h "17yhg1vgp1mcgnvdwkc1qi2mz4373lw8f5q6w7r1crabv3gy6bcp") (f (quote (("v4_0_5" "v4_0") ("v4_0" "v3_2") ("v3_2" "v3_1") ("v3_1" "v3_0") ("v3_0" "v2_1") ("v2_1" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

(define-public crate-lxc-0.7.0 (c (n "lxc") (v "0.7.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.5") (d #t) (k 0)))) (h "0ggppn3kzqsn1civmjf5bj4vww0aq05br6alhxqir2w6y7p37fmj") (f (quote (("v4_0_5" "v4_0") ("v4_0" "v3_2") ("v3_2" "v3_1") ("v3_1" "v3_0") ("v3_0" "v2_1") ("v2_1" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

(define-public crate-lxc-0.8.0 (c (n "lxc") (v "0.8.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "lxc-sys") (r "^0.5") (d #t) (k 0)))) (h "1kj5idlh01qzx1x7ph6v9js84rlircxvjwxpzikk4zbjz265hd8r") (f (quote (("v6_0" "v5_0") ("v5_0" "v4_0") ("v4_0" "v3_2") ("v3_2" "v3_1") ("v3_1" "v3_0") ("v3_0" "v2_1") ("v2_1" "v2_0") ("v2_0" "v1_1") ("v1_1" "v1_0") ("v1_0"))))))

