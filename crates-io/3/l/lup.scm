(define-module (crates-io #{3}# l lup) #:use-module (crates-io))

(define-public crate-lup-0.1.0 (c (n "lup") (v "0.1.0") (h "1xdjk1f2c7hjpy9ha7bhlv2fpql5v04ibw157r98plx48aawmxm0")))

(define-public crate-lup-0.2.0 (c (n "lup") (v "0.2.0") (h "1hmm9f2xvwsvdrxm6aw8gnhr3nar102ijxgvyn8yd1gcmkyw0hsh")))

(define-public crate-lup-0.3.0 (c (n "lup") (v "0.3.0") (h "0ghijiq09dn9x1d4jbnb9a26z91wpzrvj67249nvi0dxwix0n15q")))

