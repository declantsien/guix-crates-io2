(define-module (crates-io #{3}# l lea) #:use-module (crates-io))

(define-public crate-lea-0.1.0 (c (n "lea") (v "0.1.0") (d (list (d (n "block-cipher-trait") (r "0.6.*") (d #t) (k 0)))) (h "0p5wj7ca17k052rx207w75six19sc5w77nccyl1i10i73ckam207") (y #t)))

(define-public crate-lea-0.1.1 (c (n "lea") (v "0.1.1") (d (list (d (n "block-cipher-trait") (r "0.6.*") (d #t) (k 0)))) (h "0f9iz51wygcmak9g1cwxca0agwp8s6vhjsc80bv56dncrxw71nms") (y #t)))

(define-public crate-lea-0.2.0 (c (n "lea") (v "0.2.0") (d (list (d (n "block-cipher-trait") (r "0.6.*") (d #t) (k 0)) (d (n "cfg-if") (r "0.1.*") (d #t) (k 0)) (d (n "generic-array") (r "0.12.*") (d #t) (k 0)) (d (n "stream-cipher") (r "0.3.*") (d #t) (k 0)))) (h "19g1k68m8mszk85mgcgnmrys2hjcjl1kinazzlmf28amv6bg3jd4") (y #t)))

(define-public crate-lea-0.2.1 (c (n "lea") (v "0.2.1") (d (list (d (n "block-cipher-trait") (r "0.6.*") (d #t) (k 0)) (d (n "cfg-if") (r "0.1.*") (d #t) (k 0)) (d (n "generic-array") (r "0.12.*") (d #t) (k 0)) (d (n "stream-cipher") (r "0.3.*") (d #t) (k 0)))) (h "0rnf1gzggc6ah6g47kwnc2qi9w61gzs56qf6j6j694d6vrf4wgsk") (y #t)))

(define-public crate-lea-0.2.2 (c (n "lea") (v "0.2.2") (d (list (d (n "block-cipher-trait") (r "0.6.*") (d #t) (k 0)) (d (n "cfg-if") (r "0.1.*") (d #t) (k 0)) (d (n "stream-cipher") (r "0.3.*") (d #t) (k 0)))) (h "04jy1k2s1lj6qgy0mizqal58yb471zpm2224lccwpkzrq0r9f5qz") (y #t)))

(define-public crate-lea-0.2.3 (c (n "lea") (v "0.2.3") (d (list (d (n "block-cipher-trait") (r "0.6.*") (d #t) (k 0)) (d (n "cfg-if") (r "0.1.*") (d #t) (k 0)) (d (n "stream-cipher") (r "0.3.*") (d #t) (k 0)))) (h "0grzqis8c4mid054nm2sl6zclkvpx88nh2ryz3glkilsxwzk3f3n") (y #t)))

(define-public crate-lea-0.3.0 (c (n "lea") (v "0.3.0") (d (list (d (n "aes") (r "0.5.*") (d #t) (k 2)) (d (n "block-cipher") (r "0.8.*") (d #t) (k 0)) (d (n "cfg-if") (r "0.1.*") (d #t) (k 0)) (d (n "chacha20") (r "0.5.*") (d #t) (k 2)) (d (n "criterion") (r "0.3.*") (d #t) (k 2)) (d (n "ctr") (r "0.5.*") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "1.1.*") (d #t) (k 0)))) (h "06pfxz89ah583jamayxfh77cd33a4z615w0h8yl04bfghjvqbcni") (f (quote (("feature-ctr" "ctr") ("default" "feature-ctr")))) (y #t)))

(define-public crate-lea-0.3.1 (c (n "lea") (v "0.3.1") (d (list (d (n "aes") (r "0.6.*") (d #t) (k 2)) (d (n "cfg-if") (r "1.*.*") (d #t) (k 0)) (d (n "chacha20") (r "0.6.*") (d #t) (k 2)) (d (n "cipher") (r "0.2.*") (d #t) (k 0)) (d (n "criterion") (r "0.3.*") (d #t) (k 2)) (d (n "ctr") (r "0.6.*") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "1.1.*") (d #t) (k 0)))) (h "0dbrxlpclqm7sxbzlf2x9rrxi6r57mnnfhs89365c6wgknw92gkb") (f (quote (("feature-ctr" "ctr") ("default" "feature-ctr")))) (y #t)))

(define-public crate-lea-0.4.0 (c (n "lea") (v "0.4.0") (d (list (d (n "aes") (r ">=0.6.0, <0.7.0") (d #t) (k 2)) (d (n "ccm_crate") (r ">=0.3.0, <0.4.0") (o #t) (d #t) (k 0) (p "ccm")) (d (n "cfg-if") (r ">=1.0.0, <1.1.0") (d #t) (k 0)) (d (n "chacha20") (r ">=0.6.0, <0.7.0") (d #t) (k 2)) (d (n "cipher") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "ctr_crate") (r ">=0.6.0, <0.7.0") (o #t) (d #t) (k 0) (p "ctr")) (d (n "zeroize") (r ">=1.1.0, <1.2.0") (d #t) (k 0)))) (h "1kp2wkh9d2gwfwgxnb3avjaaai1qrqg7hiscampgpbyd9lvwkxkl") (f (quote (("default" "ccm" "ctr") ("ctr" "ctr_crate") ("ccm" "ccm_crate")))) (y #t)))

(define-public crate-lea-0.5.0 (c (n "lea") (v "0.5.0") (d (list (d (n "ccm") (r "0.4.*") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "1.*.*") (d #t) (k 0)) (d (n "cipher") (r "0.3.*") (d #t) (k 0)) (d (n "criterion") (r "0.3.*") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "0.1.*") (d #t) (k 2)) (d (n "ctr") (r "0.7.*") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "1.*.*") (o #t) (k 0)))) (h "1q594pm7zgfyw0203nvd6zc4sqmpxf89ia3ik7jf7w3yn91bg6h0") (f (quote (("default")))) (y #t)))

(define-public crate-lea-0.5.1 (c (n "lea") (v "0.5.1") (d (list (d (n "ccm") (r "0.4.*") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "1.*") (d #t) (k 0)) (d (n "cipher") (r "0.3.*") (d #t) (k 0)) (d (n "criterion") (r "0.3.*") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "0.1.*") (d #t) (k 2)) (d (n "ctr") (r "0.8.*") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "1.*") (o #t) (k 0)))) (h "1z7aczrb4g1rzvaxygh0lyqipwr2zbs5pryfbjqgzlzqac6pcbfb") (f (quote (("default")))) (y #t)))

(define-public crate-lea-0.5.2 (c (n "lea") (v "0.5.2") (d (list (d (n "ccm") (r "0.4.*") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "1.*") (d #t) (k 0)) (d (n "cipher") (r "0.3.*") (d #t) (k 0)) (d (n "criterion") (r "0.3.*") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "0.1.*") (d #t) (k 2)) (d (n "ctr") (r "0.8.*") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "1.*") (o #t) (k 0)))) (h "13mnh4x4i9bkmyac018yn8gfmxq6v6w3b8n0ys8v37b9hidjwwr0") (f (quote (("default")))) (y #t)))

(define-public crate-lea-0.5.3 (c (n "lea") (v "0.5.3") (d (list (d (n "ccm") (r "0.4.*") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "1.*") (d #t) (k 0)) (d (n "cipher") (r "0.3.*") (d #t) (k 0)) (d (n "criterion") (r "0.3.*") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "0.1.*") (d #t) (k 2)) (d (n "ctr") (r "0.8.*") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "1.*") (o #t) (k 0)))) (h "1hn2b1vnilwfhf6pr3k2l1l016jysai0jpjghsi0qplgigc6z5gx") (f (quote (("default")))) (y #t)))

(define-public crate-lea-0.5.4 (c (n "lea") (v "0.5.4") (d (list (d (n "ccm") (r "0.4.*") (o #t) (d #t) (k 0)) (d (n "cfg-if") (r "1.*") (d #t) (k 0)) (d (n "cipher") (r "0.3.*") (d #t) (k 0)) (d (n "ctr") (r "0.8.*") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "1.*") (o #t) (k 0)) (d (n "criterion") (r "0.4.*") (d #t) (k 2)) (d (n "criterion-cycles-per-byte") (r "0.4.*") (d #t) (k 2)))) (h "12k035f426vd7s14jdpw9yfghk9a2pjmrwjaf8vvwqin1mk3mw00") (f (quote (("default"))))))

