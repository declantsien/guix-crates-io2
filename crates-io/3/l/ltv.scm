(define-module (crates-io #{3}# l ltv) #:use-module (crates-io))

(define-public crate-ltv-0.1.0 (c (n "ltv") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "0cqgbdjh50038z7q5ahpks2gipcnajiicylhcp1jl8dkcgwa5wp8")))

(define-public crate-ltv-0.1.1 (c (n "ltv") (v "0.1.1") (d (list (d (n "ltv_derive") (r "^0.1") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.1") (d #t) (k 2)))) (h "02pqziwmxjcvcmlr5b288rpfx41gczcyavra2pl3fysqnjajn1bv")))

(define-public crate-ltv-0.1.2 (c (n "ltv") (v "0.1.2") (d (list (d (n "ltv_derive") (r "^0.1") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.1") (d #t) (k 2)))) (h "0066gic8mfx1m67cbrj1grqvphi4hdhb39zq0kmnxa2g1dgnwp2n")))

(define-public crate-ltv-0.1.3 (c (n "ltv") (v "0.1.3") (d (list (d (n "ltv_derive") (r "^0.1") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.1") (d #t) (k 2)))) (h "0qrfj6hrxdxyhdij2kx4bx88wbczxvq23gminsagbliqmiqa7ncf")))

(define-public crate-ltv-0.1.5 (c (n "ltv") (v "0.1.5") (d (list (d (n "ltv_derive") (r "^0.1") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.1") (d #t) (k 2)))) (h "1g8d13bh337ybf201p8g3p8qa2mxl7cb5xxjfmjks05s4xhhgxwl")))

(define-public crate-ltv-0.1.6 (c (n "ltv") (v "0.1.6") (d (list (d (n "ltv_derive") (r "^0.1") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.1") (d #t) (k 2)))) (h "13bfi0288krxhrrdc7vqvf3w1p9nhl30yqi9wr0hfs1igy1xkhs3")))

(define-public crate-ltv-0.2.1 (c (n "ltv") (v "0.2.1") (h "1cz6pl9414zarkplsw80mcsg2n9snnn8pa804pb7f0j8n5a6vvcv")))

(define-public crate-ltv-0.2.2 (c (n "ltv") (v "0.2.2") (d (list (d (n "ltv_derive") (r "^0.2") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2") (d #t) (k 2)))) (h "1939rkg1szx0fgphkhfixnj6jdrccpa0pms7pqmjqchzk0dr0fhj")))

(define-public crate-ltv-0.2.3 (c (n "ltv") (v "0.2.3") (d (list (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 2)))) (h "1jailysdb8rmnhaa8r0fg9ajmsl9mv6n1hgday0k7nci7cpz8k51")))

(define-public crate-ltv-0.2.4 (c (n "ltv") (v "0.2.4") (d (list (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 2)))) (h "0qqq0ckv21x2jg0npss2bfg3w7v7mc3vq5j8pb18sbj6gqj0i828")))

(define-public crate-ltv-0.2.5 (c (n "ltv") (v "0.2.5") (d (list (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 2)))) (h "1rbm526q6x08lgg7yphk6cl1l7zky456vmkb10q8zgfnfm2w1n32")))

(define-public crate-ltv-0.2.6 (c (n "ltv") (v "0.2.6") (d (list (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 2)))) (h "0xigp3rkfsfm14a99ma3qxzc6pvqsq66hh0gx6ycccw37wmim0si")))

(define-public crate-ltv-0.2.7 (c (n "ltv") (v "0.2.7") (d (list (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 2)))) (h "0dzld8gbhz96m2r41qw7vh4dz366y6d135c3i4pwssccdx370mib")))

(define-public crate-ltv-0.2.8 (c (n "ltv") (v "0.2.8") (d (list (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 2)))) (h "1jw190drclhqcxf38gnda2npm8yykrxa0vxv3phck156dhd4km8a")))

(define-public crate-ltv-0.2.9 (c (n "ltv") (v "0.2.9") (d (list (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 2)))) (h "11lqdlxlgpcz25m9swb5d93npwhbl6m5pcjs8xpj0d3v273hwj0l")))

(define-public crate-ltv-0.2.10 (c (n "ltv") (v "0.2.10") (d (list (d (n "ltv_derive") (r "^0.2.3") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 2)))) (h "014ibbhaqyppw3kj2w5bmjb89lhbwqqcp1pdkvdbhrcm86dck696")))

(define-public crate-ltv-0.2.11 (c (n "ltv") (v "0.2.11") (d (list (d (n "ltv_derive") (r "^0.2.3") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.2") (d #t) (k 2)))) (h "0j08ay4yqj8fii6qg6162djk6bvlfif575lvmhzjxcwna32hm9wd")))

(define-public crate-ltv-0.2.12 (c (n "ltv") (v "0.2.12") (d (list (d (n "ltv_derive") (r "^0.2.4") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.4") (d #t) (k 2)))) (h "1pvcq28s3vvypd6rp59pax0d1258nirfhaczg68a8s3ky8jzby91")))

(define-public crate-ltv-0.2.13 (c (n "ltv") (v "0.2.13") (d (list (d (n "ltv_derive") (r "^0.2.4") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.4") (d #t) (k 2)))) (h "1640h35s9c0sx9bhar45ivkifaa1i9d940bfi81gr7hkzfb50hsq")))

(define-public crate-ltv-0.2.14 (c (n "ltv") (v "0.2.14") (d (list (d (n "ltv_derive") (r "^0.2.4") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.4") (d #t) (k 2)))) (h "0qs0a75scn3gkc3fb3xc16j4a5zj6g90cm3pzmd16zgdv0ksd54c")))

(define-public crate-ltv-0.2.15 (c (n "ltv") (v "0.2.15") (d (list (d (n "ltv_derive") (r "^0.2.4") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.4") (d #t) (k 2)))) (h "1x2jfnns7dq0wvmswzgzf86sbj6r4cdy16ry39yxh39f9ysbpm70")))

(define-public crate-ltv-0.2.16 (c (n "ltv") (v "0.2.16") (d (list (d (n "ltv_derive") (r "^0.2.5") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.5") (d #t) (k 2)))) (h "1vgs2az24gpb7xawpyrmq8a8k9a93npyy78b604lj24qj9j6qrbl")))

(define-public crate-ltv-0.2.17 (c (n "ltv") (v "0.2.17") (d (list (d (n "ltv_derive") (r "^0.2.5") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.5") (d #t) (k 2)))) (h "1xq525yph7p0dk3xd08n14rdpjh9wvp67q03d9h0vf9xkqh514h8")))

(define-public crate-ltv-0.2.18 (c (n "ltv") (v "0.2.18") (d (list (d (n "ltv_derive") (r "^0.2.6") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.6") (d #t) (k 2)))) (h "1da6b5c13jrv7a89phrb4dhfdwf37gy405q5hn87v60iw3hr9aip")))

(define-public crate-ltv-0.2.19 (c (n "ltv") (v "0.2.19") (d (list (d (n "ltv_derive") (r "^0.2.6") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.6") (d #t) (k 2)))) (h "1h8p4vp1djcqxwbc01zc4c4bf3rrc9zyg2hhsiqnq4c94g0l1q5s")))

(define-public crate-ltv-0.2.20 (c (n "ltv") (v "0.2.20") (d (list (d (n "ltv_derive") (r "^0.2.7") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.7") (d #t) (k 2)))) (h "0ii2w4gd72svrm23xsfacwwydpr9d4hrm9lwfzr6hwzzff7mvngn")))

(define-public crate-ltv-0.2.21 (c (n "ltv") (v "0.2.21") (d (list (d (n "ltv_derive") (r "^0.2.8") (d #t) (k 0)) (d (n "ltv_derive") (r "^0.2.8") (d #t) (k 2)))) (h "10v7lg4d0zx7rj3myv4a1rldx0irp3vyrj0yq8gb69s8q298wdsr")))

