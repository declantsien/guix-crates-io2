(define-module (crates-io #{3}# l lac) #:use-module (crates-io))

(define-public crate-lac-0.4.0 (c (n "lac") (v "0.4.0") (d (list (d (n "argh") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "async-recursion") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "async-std") (r "^1.9") (f (quote ("attributes" "unstable"))) (o #t) (d #t) (k 0)) (d (n "claxon") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hound") (r "^3.4") (o #t) (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "sha2") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0bdidkpp42m8mcnmqrpfs7zvyz68xipnfzssf11m8zxnqrh2d8pr") (f (quote (("bin" "async-std" "sha2" "hex" "hound" "claxon" "argh" "num_cpus" "async-recursion" "futures"))))))

