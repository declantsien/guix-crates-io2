(define-module (crates-io #{3}# l lpt) #:use-module (crates-io))

(define-public crate-lpt-0.1.0 (c (n "lpt") (v "0.1.0") (d (list (d (n "libc") (r ">=0.2") (t "cfg(unix)") (k 0)) (d (n "windows") (r ">=0.51") (f (quote ("Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0w3jhfmkf8jpj4f7fd985w2z4x5nxijdx5275sxz1yihgvsk6lwa")))

(define-public crate-lpt-1.0.0 (c (n "lpt") (v "1.0.0") (d (list (d (n "libc") (r ">=0.2") (t "cfg(unix)") (k 0)) (d (n "windows") (r ">=0.51") (f (quote ("Win32_System_Threading" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "084ljj45pzii9qb3fxcwq35avc49pgg3yawmlfwvv1l35qd5bkys")))

