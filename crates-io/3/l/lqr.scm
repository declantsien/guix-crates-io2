(define-module (crates-io #{3}# l lqr) #:use-module (crates-io))

(define-public crate-lqr-0.1.0 (c (n "lqr") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)))) (h "1wrysq20lhcindhg77lhmf7f8mrpm84ah96sfjfjrizwb58nf7av")))

