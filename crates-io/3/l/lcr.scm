(define-module (crates-io #{3}# l lcr) #:use-module (crates-io))

(define-public crate-lcr-0.0.1 (c (n "lcr") (v "0.0.1") (h "1hmq0j090n5a8rr3p70ms4ay50n8yr53rn58629x178zk67dzz3z")))

(define-public crate-lcr-0.0.2 (c (n "lcr") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)))) (h "12spfr3r44pp6ssm645ip0l1fd0xm9qgf3gi6s5q1nrqsqblydns")))

(define-public crate-lcr-0.0.3 (c (n "lcr") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)))) (h "13p1hm2r5ypwgwdxxag45c6m6nwf528prxhpb7v0v3n5cnlbvr6i")))

(define-public crate-lcr-0.0.7 (c (n "lcr") (v "0.0.7") (d (list (d (n "reqwest") (r "^0.11.4") (d #t) (k 0)))) (h "04d3ifbgw565sy528csh7w8lrpggkq98yp1j0n973gwan9r2raqz")))

