(define-module (crates-io #{3}# l lll) #:use-module (crates-io))

(define-public crate-lll-0.0.1 (c (n "lll") (v "0.0.1") (h "1ig9mx4kn4jsfi4hmlka7xs9wbf832x9m8zgwn3g4fvvkbdz0g2f")))

(define-public crate-lll-0.0.2 (c (n "lll") (v "0.0.2") (d (list (d (n "cursive") (r "^0.7.4") (d #t) (k 0)))) (h "1plbcg1hxy52d0fz5rxhvvmix3fdsp4y02qwi0zs00fsnb1ikcd9")))

(define-public crate-lll-0.0.3 (c (n "lll") (v "0.0.3") (d (list (d (n "cursive") (r "^0.7.4") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "09qqbds12vb34nhgmzgvh5w7lh4c106pl8kfbnpn0k1chbmqw1y4")))

(define-public crate-lll-0.0.4 (c (n "lll") (v "0.0.4") (d (list (d (n "cursive") (r "^0.7.4") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "0sw19hdy3lhsh83c9sypc6mwz9car8gjkij4g4xjrwg3nh8facxw")))

(define-public crate-lll-0.0.5 (c (n "lll") (v "0.0.5") (d (list (d (n "cursive") (r "^0.12.0") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "0nfyi2a8b0yvcscfdwixhwfg5y6kli0lh6bb22i2y7yk2a5vr55q")))

(define-public crate-lll-0.0.6 (c (n "lll") (v "0.0.6") (d (list (d (n "cursive") (r "^0.12.0") (d #t) (k 0)) (d (n "open") (r "^1.2.2") (d #t) (k 0)))) (h "0qzl635c1a3i07gwkbl80cr6cpqgxhdz3bilppgwll12cfj7hvvz")))

(define-public crate-lll-0.0.9 (c (n "lll") (v "0.0.9") (d (list (d (n "alphanumeric-sort") (r "^1.0.3") (d #t) (k 0)) (d (n "cursive") (r "^0.9") (f (quote ("termion-backend"))) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "users") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.6") (d #t) (k 0)))) (h "08r3b9vkpnv17mhbzbswf5vhs0n9w2ppcgx5mx31j0wzlgc5cszl")))

(define-public crate-lll-0.1.1 (c (n "lll") (v "0.1.1") (d (list (d (n "alphanumeric-sort") (r "^1.0.3") (d #t) (k 0)) (d (n "cursive") (r "^0.9") (f (quote ("termion-backend"))) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.3") (d #t) (k 0)) (d (n "fern") (r "^0.5") (d #t) (k 0)) (d (n "uname") (r "^0.1.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.5") (d #t) (k 0)) (d (n "users") (r "^0.8.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.6") (d #t) (k 0)))) (h "01fbhgrkif21f8r54k22ididrqkb80yvfsynli7awjaj43j3c2qc")))

