(define-module (crates-io #{3}# l liz) #:use-module (crates-io))

(define-public crate-liz-0.1.0 (c (n "liz") (v "0.1.0") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "0rk1v61m3rzh9akydmm5d0hdpysnfql0il5dq4fijv0yz3qjnbny")))

(define-public crate-liz-0.1.1 (c (n "liz") (v "0.1.1") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1ahf5vd87m1xhj2gmd5whkk8ix8wwbwq0krrb1x3klackk5psws7")))

(define-public crate-liz-0.1.2 (c (n "liz") (v "0.1.2") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "0fdvj46siqkw3kqw98vzmwc1dw1msaflj27rgy5pgy7vpzldv7bb")))

(define-public crate-liz-0.1.3 (c (n "liz") (v "0.1.3") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "0nywp7gfdq8myn1zv8fp73sr1hnhb9ild7hmzqmc61rwih0mqfms")))

(define-public crate-liz-0.1.4 (c (n "liz") (v "0.1.4") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "05hz6k4r2b4j60vnsw7rd94yzpzyzx7lm1xkxdgnqlg2z4068hbx")))

(define-public crate-liz-0.1.5 (c (n "liz") (v "0.1.5") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1maiiaf6hymil3lkz92hp6qrq9jjpksdvq5rawj5f4y1h5c6a6kf")))

(define-public crate-liz-0.1.6 (c (n "liz") (v "0.1.6") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1nw1lbycgxcf9p8yabmi7r8bnqlkn5ims30gb9l34nr9s6cv09qf")))

(define-public crate-liz-0.1.7 (c (n "liz") (v "0.1.7") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1mh0qg2nmic45p7v2y2d1d0rsi15bpdzy389rp5wxhkk3ixwgs9i")))

(define-public crate-liz-0.1.8 (c (n "liz") (v "0.1.8") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1b592x3c30x8ay7qzgr3d6nyfh3ga922psymx24a4lpj3bmdqpgw")))

(define-public crate-liz-0.1.9 (c (n "liz") (v "0.1.9") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "06zxv0hm8nbjjfa2ff800m0nc3rzgf99662lhmrq9ilfs1pagm5i")))

(define-public crate-liz-0.1.10 (c (n "liz") (v "0.1.10") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "09v5gvvmbrqnix61fs31wwlrdrkyv67z90i9bk3a8lqh3lhcxnaq")))

(define-public crate-liz-0.1.11 (c (n "liz") (v "0.1.11") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1lamkqakqysaymp91pwy2v6rk3f26hds791njyl1q0mcrscadn39")))

(define-public crate-liz-0.1.12 (c (n "liz") (v "0.1.12") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1mm2yp4172lpgppqf7wc6whbinfj604z6rb0d9plpfmr5009rvjl")))

(define-public crate-liz-0.1.13 (c (n "liz") (v "0.1.13") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "0b4yv0j58xzf0c1swg3hm6n47jfx6kqvrkqh5jv8k9f8yw66ygfc")))

(define-public crate-liz-0.1.14 (c (n "liz") (v "0.1.14") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "18kfmjy3njh4y7qm7ryqssa7hfhajklx7gp89sz35m2vbrvp8hm8")))

(define-public crate-liz-0.1.15 (c (n "liz") (v "0.1.15") (d (list (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1jszzm3gq1cfwpyprk1307hwbmi22i7kalgnkwx8f9rffnhmshhl")))

(define-public crate-liz-0.1.16 (c (n "liz") (v "0.1.16") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1vj3py6hnqdlpnvzj3hdj5gsvw22qm5k24nlf03p2279k8m4p8v2")))

(define-public crate-liz-0.1.17 (c (n "liz") (v "0.1.17") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "1sqvq13v7iqkyvr5aj245vw4z14dgv4fyhy8crs9zbjvlp1scanh")))

(define-public crate-liz-0.1.18 (c (n "liz") (v "0.1.18") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rlua") (r "^0.18") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "0sjagszbq8bfnban7b6wvv81ymq7mgwmibk9jm32cwzdg624s95x")))

(define-public crate-liz-0.1.19 (c (n "liz") (v "0.1.19") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rlua") (r "^0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "190gjqlwzb29swg49idbl6iy6bcpz1abqcn8r3fslb0h2rjpnxk1")))

(define-public crate-liz-0.1.20 (c (n "liz") (v "0.1.20") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rlua") (r "^0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "0v0mpd839zbfv1m9xifz8l88khc06qv1bgdch9xzm1jijwa9xkvf")))

(define-public crate-liz-0.1.21 (c (n "liz") (v "0.1.21") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rlua") (r "^0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "07wg6l6197ns1d7xz3cpwj5v83czjvhzxy6ywrdy1946za7nwhwq")))

(define-public crate-liz-0.1.22 (c (n "liz") (v "0.1.22") (d (list (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rlua") (r "^0.19") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "simple-error") (r "^0.2") (d #t) (k 0)))) (h "09f49g2iqa0xiaffr9sa8n64vdlmvrpmnq8qzybghyq6bc3hvxlw")))

