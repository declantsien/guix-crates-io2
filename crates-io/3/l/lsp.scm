(define-module (crates-io #{3}# l lsp) #:use-module (crates-io))

(define-public crate-lsp-0.0.0 (c (n "lsp") (v "0.0.0") (h "113iz963zxyqxgz6wg4aslk47ya35p1v9x8mz7ys3gbacjwrkapb") (y #t)))

(define-public crate-lsp-0.1.0 (c (n "lsp") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "130liyqhva7a1v73hhgf5i0j7hisl2ckxyg4c63ghzk3bwpakr4p") (y #t)))

(define-public crate-lsp-0.2.0 (c (n "lsp") (v "0.2.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qvlffrk6cw2zvhkw8fzavx6vzl7anws3sy8bqp1305hqg6rx0vf") (y #t)))

(define-public crate-lsp-0.3.0 (c (n "lsp") (v "0.3.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06l95ipiqrk4z0ccz183vgvwkmk7b3yq0wvybqq68crbp9wyaszi") (y #t)))

(define-public crate-lsp-0.3.1 (c (n "lsp") (v "0.3.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zq9fmfr6dhw2w6f5wp651v4x0c7yyw47s6apcdm4l772si1fqkr") (y #t)))

