(define-module (crates-io #{3}# l lrk) #:use-module (crates-io))

(define-public crate-lrk-0.1.0 (c (n "lrk") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1zp21ss0pv5ryxks4mlm18ghfd591br5iyb2y2x05jyfpda66cls") (r "1.68.0")))

(define-public crate-lrk-0.1.1 (c (n "lrk") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1j7mz57qdmnilhh5210pr9xz23g0ji3mjv1dfmrdy2fsqr8cy83r") (r "1.68.0")))

(define-public crate-lrk-0.1.2 (c (n "lrk") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1bqkqw4gpb9xk53qgp9rmzhakjpyvdbz4hnq4pgbkp3msay0jcrd") (r "1.68.0")))

(define-public crate-lrk-0.1.3 (c (n "lrk") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1jmr4vb5kr2fzny5slic1zp67yg6by3d1xg5v2g8kgx2sc71djy7") (r "1.68.0")))

