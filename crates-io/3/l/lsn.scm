(define-module (crates-io #{3}# l lsn) #:use-module (crates-io))

(define-public crate-lsn-0.1.0 (c (n "lsn") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive" "wrap_help" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0clk0n7j314sd5v3y4kb322hqn8ss7p3g20882n2gngp6fwqx3yh")))

