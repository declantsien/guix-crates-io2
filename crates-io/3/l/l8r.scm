(define-module (crates-io #{3}# l l8r) #:use-module (crates-io))

(define-public crate-l8r-0.1.0 (c (n "l8r") (v "0.1.0") (h "1viimxbn1dgwrkh0cajrc287fm0qrrhs2hcvkam3d2pr3169mbp2")))

(define-public crate-l8r-0.1.1 (c (n "l8r") (v "0.1.1") (d (list (d (n "hecs") (r "^0.2.7") (d #t) (k 0)))) (h "1q7l4zj4cpf42imc7jx6xm4db4z0qg52q1r2hxrzbnjldaassyba")))

