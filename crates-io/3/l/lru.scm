(define-module (crates-io #{3}# l lru) #:use-module (crates-io))

(define-public crate-lru-0.1.0 (c (n "lru") (v "0.1.0") (h "1b4bihkmb6fn8bnlnjj20pmgrkvkxwsgzscbb88kas9m1n1dwhm0") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.1 (c (n "lru") (v "0.1.1") (h "1bk16qvh7z969nmmgdlnip30wlyzpvybanchpl5sqysh1qy1vxqh") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.2 (c (n "lru") (v "0.1.2") (h "02z1m1zsxwnnbz9pw8ppmwva3j9h2hf2xcf1rk40wspzyhi75sr6") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.3 (c (n "lru") (v "0.1.3") (h "1z5s9c6xr01kxpxs3649c6lf6ipywi9775fr87b0zrxv4gng76sj") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.4 (c (n "lru") (v "0.1.4") (h "0fh9qgrn28h45pb02bk3q3n7bykwji0008iswy7bac1xl90z592d") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.5 (c (n "lru") (v "0.1.5") (h "0yc61wzc8j0iy0q7q47pz3x6mk04b1zbdff0d0nv9r41p6y5nirs") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.6 (c (n "lru") (v "0.1.6") (h "0j7b9ik02gpv6y614gbck33niawgm5m8iasqjn4vy1x96if8zisw") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.7 (c (n "lru") (v "0.1.7") (d (list (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "0jhqq2bkk64h72ifw0jwg94gxh3lg5kblqx5fs3f1w8r1zjjshpi") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.8 (c (n "lru") (v "0.1.8") (d (list (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "1hwn3jdk2g5zajcb1gv36m2y1f15xjiazn3smgr83a5x6hcird2z") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.9 (c (n "lru") (v "0.1.9") (d (list (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "1d2ikf8ggqxmyw9rzq1zpb6h24wq54074csfhaw3ksg6ay60ndz8") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.10 (c (n "lru") (v "0.1.10") (d (list (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "0w10prw3ddsns2mdcax132kdd7dnpzf6hcvvrjdzwqmpask82d74") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.11 (c (n "lru") (v "0.1.11") (d (list (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "1ml24apbfsp4xs259kn6b691qhlcvivz5wmwbzxjsqlixsmv4pvg") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.12 (c (n "lru") (v "0.1.12") (d (list (d (n "hashbrown") (r "0.1.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "161dcdw344wccmmafjd4kfkw8xzfnq1kvwn7qld5lyh5rrv8adah") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.13 (c (n "lru") (v "0.1.13") (d (list (d (n "hashbrown") (r "0.1.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "0dyqsdnwrnz16bk5lms6qcpcmkpns3i0z3nl33mrqkfis1ppwxm9") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.14 (c (n "lru") (v "0.1.14") (d (list (d (n "hashbrown") (r "0.1.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "1l8x3lv3z6fzbswi7dphpvyglcp6jbmjls31ivj1bpgj0k69a2kx") (f (quote (("nightly"))))))

(define-public crate-lru-0.1.15 (c (n "lru") (v "0.1.15") (d (list (d (n "hashbrown") (r "0.1.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "02kp0r6kz87w0i81gvvrc9dxm0ny2lwfjrabnj034xv0dfxkaqi7") (f (quote (("nightly" "hashbrown/nightly"))))))

(define-public crate-lru-0.1.16 (c (n "lru") (v "0.1.16") (d (list (d (n "hashbrown") (r "0.1.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "1h3ri6n09slij9jp888m8n8j2j27ghrj9qigsv727hkspnjr1cxa") (f (quote (("nightly" "hashbrown/nightly"))))))

(define-public crate-lru-0.1.17 (c (n "lru") (v "0.1.17") (d (list (d (n "hashbrown") (r "0.5.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "1j4l8m07z9kpsxdwp06r2w53gxn5i5b13a7w9m8ihbf78afnd3sx") (f (quote (("nightly" "hashbrown/nightly"))))))

(define-public crate-lru-0.1.18 (c (n "lru") (v "0.1.18") (d (list (d (n "hashbrown") (r "0.6.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "0qj5q7x6dh92nwpffgrqsw9j1v758lfa5r9ig3vy8bm67rsvv5w2") (f (quote (("nightly" "hashbrown/nightly")))) (y #t)))

(define-public crate-lru-0.2.0 (c (n "lru") (v "0.2.0") (d (list (d (n "hashbrown") (r "0.6.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "0mrnglvpshdp3qw1nqfgsn1xcvsfhrxykrfrffrqr164knrfmaa3") (f (quote (("nightly" "hashbrown/nightly"))))))

(define-public crate-lru-0.3.0 (c (n "lru") (v "0.3.0") (d (list (d (n "hashbrown") (r "0.6.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "1hzinzc80s7nm8f8cni5vjmavy27hcdhd1iaw46pidpfbif483ar") (f (quote (("nightly" "hashbrown/nightly"))))))

(define-public crate-lru-0.3.1 (c (n "lru") (v "0.3.1") (d (list (d (n "hashbrown") (r "0.6.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "0lqqbad8cypp3cdd63hbyi7qy4cccl8bknql0vnww28k1a52lm0f") (f (quote (("nightly" "hashbrown/nightly"))))))

(define-public crate-lru-0.4.0 (c (n "lru") (v "0.4.0") (d (list (d (n "hashbrown") (r "0.6.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "08jbnp367wwlifby8h4rcp3z2ga7wq2j5ccs2v2q6l2vmjjdrc16") (f (quote (("nightly" "hashbrown/nightly"))))))

(define-public crate-lru-0.4.1 (c (n "lru") (v "0.4.1") (d (list (d (n "hashbrown") (r "0.6.*") (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "15xsm2nkmxcchvj3l2kylhjzsqg1s0c1x78vkvx1aah7kflasmws") (f (quote (("nightly" "hashbrown/nightly"))))))

(define-public crate-lru-0.4.2 (c (n "lru") (v "0.4.2") (d (list (d (n "hashbrown") (r "0.6.*") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "0yjarsy57c5scpqw33rmhyk9xnvl3k32qqq7d2v302dashyadbkr") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown")))) (y #t)))

(define-public crate-lru-0.4.3 (c (n "lru") (v "0.4.3") (d (list (d (n "hashbrown") (r "0.6.*") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)))) (h "0ds2l1rkbxw7v16c51bhnxyrhbjs1rz58kvxhp7xmfp5vrfk8286") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.4.4 (c (n "lru") (v "0.4.4") (d (list (d (n "hashbrown") (r "0.6.*") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "0syz4p7gla44616bp5jl9piv0pyr870f1v568yidglfp8f7852rl") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown")))) (y #t)))

(define-public crate-lru-0.4.5 (c (n "lru") (v "0.4.5") (d (list (d (n "hashbrown") (r "0.6.*") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "18ywc4sa6civc3h4ml7549rm31zw7ks681x3cli3h271m6rqsj4y") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown")))) (y #t)))

(define-public crate-lru-0.5.0 (c (n "lru") (v "0.5.0") (d (list (d (n "hashbrown") (r "0.6.*") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "1psdkyfk5snfnpl01bjrrbxh7wjxl25v2z99kkjxkxb9yrz7m6gb") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown")))) (y #t)))

(define-public crate-lru-0.5.1 (c (n "lru") (v "0.5.1") (d (list (d (n "hashbrown") (r "0.6.*") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "036njd890zsfx0hxfkyav883wlp69xzblsvrl94hxmlw462wdq18") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.5.2 (c (n "lru") (v "0.5.2") (d (list (d (n "hashbrown") (r "0.6.*") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "1hji28n6cdhdvbvlyabbnhs360llq5x3sn3ak2vgfp2406agnzi9") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.5.3 (c (n "lru") (v "0.5.3") (d (list (d (n "hashbrown") (r "0.6.*") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "0d7glrnl4bsl7fbjjq5li0m59al61mp7h0yd42if6zcm4g0mdi1m") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.6.0 (c (n "lru") (v "0.6.0") (d (list (d (n "hashbrown") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "0lxiaddsf89rknk54pr4lxahbs667kfgp832pjvrxh1fqxd986qi") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.6.1 (c (n "lru") (v "0.6.1") (d (list (d (n "hashbrown") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "1h21ahnqnrmjzgs6mzhrynaafr1jl4d7h2nhnlz2d8lchyv6wwdy") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.6.2 (c (n "lru") (v "0.6.2") (d (list (d (n "hashbrown") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "01kn3ddyb2srqgzgzzca5qz4h18ymd591pd6r5maad9222phggis") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.6.3 (c (n "lru") (v "0.6.3") (d (list (d (n "hashbrown") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "0d8ncw3jcwmhz3y5sa0lj7zc3whr91c15gb4nfwd0ynmfcmk9bis") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.6.4 (c (n "lru") (v "0.6.4") (d (list (d (n "hashbrown") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "18y5xfyliiyvgkqz6r8r6gh6i8a79svxb60dlx5yl61rxpc848zy") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.6.5 (c (n "lru") (v "0.6.5") (d (list (d (n "hashbrown") (r "^0.9.0") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "1flh9fznv65mjpbk8cisykxpzf9fmfpjiv1x7nzps7gwrm14sdqz") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.6.6 (c (n "lru") (v "0.6.6") (d (list (d (n "hashbrown") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "14dsf7345kwb8fg75dmkywkjpchxc9yxkwng124nwhc5nhldk8ky") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.7.0 (c (n "lru") (v "0.7.0") (d (list (d (n "hashbrown") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "00y8kzd1vl3npah8xbmahj61j77aic8b75bmqcjs53fb8zz8qx3c") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.7.1 (c (n "lru") (v "0.7.1") (d (list (d (n "hashbrown") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "0fls4ai6306b7q0q5l395z549lqcb89lgcwk8y24sxx117lri626") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.7.2 (c (n "lru") (v "0.7.2") (d (list (d (n "hashbrown") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "07sdl7gpg30hbz7cgph75n2xl8915rshi90c7jqr5j9mi62m6hr7") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.7.3 (c (n "lru") (v "0.7.3") (d (list (d (n "hashbrown") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "1sy07gq134a7i6p0rr0w8p6ahyhxvv7hyk2nijgddlgnh0q7zf7w") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown")))) (y #t)))

(define-public crate-lru-0.7.4 (c (n "lru") (v "0.7.4") (d (list (d (n "hashbrown") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "1ikz2lipwhybavbmzgbydchs6q45ihjiij1pqx7jvc5m187q3s12") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.7.5 (c (n "lru") (v "0.7.5") (d (list (d (n "hashbrown") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "12d82ras225hf3q5lx95cq8wz0kkmskqqd0cjw2aniscvr0kwq9j") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.7.6 (c (n "lru") (v "0.7.6") (d (list (d (n "hashbrown") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "0a224iddhhmrrq5kivhk8yds1vmirs1q58rcsg0d7pdjnxfdj5c0") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.7.7 (c (n "lru") (v "0.7.7") (d (list (d (n "hashbrown") (r "^0.11.2") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "16illf6b1a07qapkxlk50n59jkywz357vkw70zh6rp2scpjnykn8") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.7.8 (c (n "lru") (v "0.7.8") (d (list (d (n "hashbrown") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "0yp4ai5rpr2czxklzxxx98p6l2aqv4g1906j3dr4b0vfgfxbx6g9") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.8.0 (c (n "lru") (v "0.8.0") (d (list (d (n "hashbrown") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "0laa3zbahzk51w7ilba09rllj54q16xyf2b73ij1i76pvp99hvck") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.8.1 (c (n "lru") (v "0.8.1") (d (list (d (n "hashbrown") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "028rayhr9hqb5r4rdapqblk51wz77bfdbcl4ggalpfriyaisms5n") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.9.0 (c (n "lru") (v "0.9.0") (d (list (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "05yz4qqx7wxbhgxs5hx22j13g8mv9z3gn2pkspykyq48winx9rvi") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.10.0 (c (n "lru") (v "0.10.0") (d (list (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "07ms2ic8hdgl1gq7pm1h89rqcm3x4qiclbd86g410vskjq11dw83") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.10.1 (c (n "lru") (v "0.10.1") (d (list (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "0w5n2sgh66ac8ihqv6688mlm7zb3ks18jlbzpbhwgw3x8jp8z3ki") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.11.0 (c (n "lru") (v "0.11.0") (d (list (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "1igfdlk9xhw45nr8lfdav8q021vv95pmkgw977w381kympdjpnzf") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.11.1 (c (n "lru") (v "0.11.1") (d (list (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "08dzlpriy9xajga5k2rgsh7qq5zhx3rfd6jgwfh46dlbd6vkza54") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.12.0 (c (n "lru") (v "0.12.0") (d (list (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "0q3cwh81mb23wy1p1m1g9f5rkl1qsl4m1mr7mra1ibfz5npmkyhy") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.12.1 (c (n "lru") (v "0.12.1") (d (list (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "1myillpwqfcins062g28jvj48cxw8818zcx08ydzsl6misxfx519") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.12.2 (c (n "lru") (v "0.12.2") (d (list (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "08hzyc6qki67hvmsarlhw0rv74j0n2m85fk601c4152i855h4b6v") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

(define-public crate-lru-0.12.3 (c (n "lru") (v "0.12.3") (d (list (d (n "hashbrown") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "scoped_threadpool") (r "0.1.*") (d #t) (k 2)) (d (n "stats_alloc") (r "0.1.*") (d #t) (k 2)))) (h "1p5hryc967wdh56q9wzb2x9gdqy3yd0sqmnb2fcf7z28wrsjw9nk") (f (quote (("nightly" "hashbrown" "hashbrown/nightly") ("default" "hashbrown"))))))

