(define-module (crates-io #{3}# l luu) #:use-module (crates-io))

(define-public crate-luu-0.1.0 (c (n "luu") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "copypasta-ext") (r "^0.3.7") (d #t) (k 0)))) (h "1d5yp0pkhn9vambpxr6hw5cq58lcm8dg1qdn66mp1cczl2xdw2qz") (r "1.60")))

