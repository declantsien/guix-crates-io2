(define-module (crates-io #{3}# l luk) #:use-module (crates-io))

(define-public crate-luk-0.1.0 (c (n "luk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "prettyprint") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0ynrysv5rbc8flv66s8x0sg1f0gdsy7y271f7p0qvs458i7ws63q")))

