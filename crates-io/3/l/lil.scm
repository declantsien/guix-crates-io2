(define-module (crates-io #{3}# l lil) #:use-module (crates-io))

(define-public crate-lil-0.1.0 (c (n "lil") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)))) (h "03vbchr9gsnk6is6drgaiqz2kl6dijcsy9y7f506m55aqamzxr5i")))

(define-public crate-lil-0.1.1 (c (n "lil") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)))) (h "0z551ls03qkxjxq5hxi500n0svkkrgplrjmbmhcb4fpi4b1l2hcv")))

(define-public crate-lil-0.1.2 (c (n "lil") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 2)))) (h "156km7kcmp8cg4s4razdpjg0ychklhdh7wi6kadpsw0iz9lzgnlf")))

