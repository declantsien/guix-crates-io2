(define-module (crates-io #{3}# l lrn) #:use-module (crates-io))

(define-public crate-lrn-0.1.0 (c (n "lrn") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lettre") (r "^0.10.1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "18ia67fvm0pl0f22743v8fppgx0wbjw25c8k8ghgqf6gjjwyc6nw")))

(define-public crate-lrn-0.2.0 (c (n "lrn") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dbus") (r "^0.9.7") (d #t) (k 0)) (d (n "lettre") (r "^0.10.1") (d #t) (k 0)) (d (n "trust-dns-resolver") (r "^0.22.0") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "1rjbdg8q4b3hl1l2xnq0pism4i85hw0k0sylnv94zzdjcfdkma3s")))

