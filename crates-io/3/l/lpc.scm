(define-module (crates-io #{3}# l lpc) #:use-module (crates-io))

(define-public crate-lpc-0.1.0 (c (n "lpc") (v "0.1.0") (d (list (d (n "common_uu") (r "^1") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fast-able") (r "^1") (d #t) (k 0)) (d (n "ipc-channel") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1h4q9k54nkpanwnmanbal7acagx2pqm8i4bjd06ir6jxqxqp8znx") (r "1.72")))

(define-public crate-lpc-1.0.0 (c (n "lpc") (v "1.0.0") (d (list (d (n "common_uu") (r "^1") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fast-able") (r "^1") (d #t) (k 0)) (d (n "ipc-channel") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1031nhdyp5bkd4qmxb7wk5bra1h0k1mmgw89wkma09zx5snfs390") (r "1.72")))

(define-public crate-lpc-1.0.1 (c (n "lpc") (v "1.0.1") (d (list (d (n "common_uu") (r "^1") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fast-able") (r "^1") (d #t) (k 0)) (d (n "ipc-channel") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1wiil7qxab9xbpymkx7ap56kz88b8gkryyy3bkkwr6aqzkfq8d0n") (r "1.72")))

(define-public crate-lpc-1.0.2 (c (n "lpc") (v "1.0.2") (d (list (d (n "common_uu") (r "^1") (d #t) (k 2)) (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "fast-able") (r "^1") (d #t) (k 0)) (d (n "ipc-channel") (r "^0.17") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1a0alcmg0f665l54vq3zl89a0mfvsz823swklx47hzgpz6mylw85") (r "1.72")))

