(define-module (crates-io #{3}# l llc) #:use-module (crates-io))

(define-public crate-llc-0.1.0 (c (n "llc") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0hp7sx359h0if4mz0inq3s2hink51vbddlgb84j09y32gm6lzz02")))

(define-public crate-llc-0.1.1 (c (n "llc") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1rzinhbxc5i3xqgwwavgykz1a2m5gs80s6fa95dr5srzs3ahfd9z")))

(define-public crate-llc-0.1.2 (c (n "llc") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1di7g4nvxk1wwfvk9b01hsj6a875yl7037shmf1fa8whn7fxj560")))

(define-public crate-llc-0.1.3 (c (n "llc") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1v5vk227mmh2wprbkqnzjk9bg9nz0n6n99ab0zgqbps8g71kwn35")))

(define-public crate-llc-0.1.4 (c (n "llc") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "114ji7z3sr22s1k16vxknbgi79kiqiygv0kk4q0jh67v69n4989h")))

(define-public crate-llc-0.1.5 (c (n "llc") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1da9mrya7jvdnr80igv5li56b3qhv5j50gkal5aw3ydcbx9sv0zm")))

(define-public crate-llc-0.1.6 (c (n "llc") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0naga55fz5hm5yc94kkdahvlnbw074i7ps60nsmkri612ip3nb9a")))

(define-public crate-llc-0.1.7 (c (n "llc") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "1gpi6amn0r7774n79zwln2nqwk6yz9sm63p8adz8abn08nvqif3z")))

(define-public crate-llc-0.2.0 (c (n "llc") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)))) (h "0s2l1ffz37xkq8m4vn6s1pbsl93ccfxagxn6rfnz376as269lnzd") (y #t)))

