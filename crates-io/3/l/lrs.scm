(define-module (crates-io #{3}# l lrs) #:use-module (crates-io))

(define-public crate-lrs-0.1.0 (c (n "lrs") (v "0.1.0") (h "1jhy0c776ianx383lvzklq9wi4jwg2jnwi7h4f72jckmdbjhzi8l")))

(define-public crate-lrs-0.1.1 (c (n "lrs") (v "0.1.1") (h "1w3zwgcli78za6y4gz758zmn25xxzgy652cimmmah3s7bm5066py")))

