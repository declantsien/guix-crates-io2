(define-module (crates-io #{3}# l los) #:use-module (crates-io))

(define-public crate-los-0.1.0 (c (n "los") (v "0.1.0") (h "0lj7nnafwabh3l8j3y8y4gz733ccw17p66xhp1zjvnfa1029wmnn")))

(define-public crate-los-0.1.1 (c (n "los") (v "0.1.1") (h "0l9f5pd5ify6z2zh6pwppqkb0x0iaqm6nscclb32xhqfvgva79ba")))

