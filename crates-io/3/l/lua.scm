(define-module (crates-io #{3}# l lua) #:use-module (crates-io))

(define-public crate-lua-0.0.1 (c (n "lua") (v "0.0.1") (h "0x33ap6x01b3whs30i7yb73m3n9kssx1xvqpbv3v70qxrp0lzrif") (y #t)))

(define-public crate-lua-0.0.2 (c (n "lua") (v "0.0.2") (h "0rhry4207y97wp9nhn26xkf6riar28pvb0s87sg4q7ysv2jgfa7h")))

(define-public crate-lua-0.0.3 (c (n "lua") (v "0.0.3") (h "0afqz0j000icgr46pvjlhffw7rn58azd4alnkrb4a28xz524jsg2")))

(define-public crate-lua-0.0.4 (c (n "lua") (v "0.0.4") (h "167lb5bh7y97xvrsvd2srqd33hr3ywzv93a0jl7f4siqsaqc19lk")))

(define-public crate-lua-0.0.5 (c (n "lua") (v "0.0.5") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)))) (h "1gq9391zy31x4fx0ndy6v5j9622sxas3rn1s255pn882arf2fps1")))

(define-public crate-lua-0.0.6 (c (n "lua") (v "0.0.6") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)))) (h "04lp6171liq4rlxldfj04vbfjyssmgx6jdhh3amvwdks7g4j1b35")))

(define-public crate-lua-0.0.7 (c (n "lua") (v "0.0.7") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)))) (h "1n20a29qwb2vj8q5baz7pblmqb9gqyla4h5c696snxjmdfcd7mcf")))

(define-public crate-lua-0.0.8 (c (n "lua") (v "0.0.8") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)))) (h "0gaphq7kc5dq4b8nvfajj1ld9qsw8ann53rf6rcqjwfigmn8fjxd")))

(define-public crate-lua-0.0.9 (c (n "lua") (v "0.0.9") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)))) (h "0m86n5dj97jr5fnhz4xpz6j0ksqvv9n9fg1wb91j59bfr709gjbr")))

(define-public crate-lua-0.0.10 (c (n "lua") (v "0.0.10") (d (list (d (n "bitflags") (r "^0.1") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)))) (h "1wij093kha8v0w4vc47yyknhbwga8q096ac6bsp042yxgvx4abv5")))

