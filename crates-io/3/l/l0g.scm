(define-module (crates-io #{3}# l l0g) #:use-module (crates-io))

(define-public crate-l0g-1.0.0 (c (n "l0g") (v "1.0.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "l0g-macros") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "171cc00cg835ddn1b3dswj78c2x8m8vxgc34viv522m4i5f5wkbq") (s 2) (e (quote (("log" "dep:log" "l0g-macros/log") ("defmt" "dep:defmt" "l0g-macros/defmt"))))))

