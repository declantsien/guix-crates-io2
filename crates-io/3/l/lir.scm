(define-module (crates-io #{3}# l lir) #:use-module (crates-io))

(define-public crate-lir-0.0.1 (c (n "lir") (v "0.0.1") (d (list (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("cargo"))) (d #t) (k 1)) (d (n "clap_complete") (r "^4") (d #t) (k 1)) (d (n "clap_mangen") (r "^0.2.14") (d #t) (k 1)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)))) (h "1zrnsrz9az6yiamxdwiqyr3v8kibljzmdg08ac5g86xnlg2hyqiz")))

