(define-module (crates-io #{3}# l lrg) #:use-module (crates-io))

(define-public crate-lrg-0.1.0 (c (n "lrg") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1ai2aqxxz8gj8maq9r5033ppskgw4czalgvckrl0s36f2l2f31lc")))

(define-public crate-lrg-0.2.0 (c (n "lrg") (v "0.2.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "02jcj9z09pi46kkz4218s9sya3fmg00z5hp692iy9y4jlkgwyma4")))

(define-public crate-lrg-0.3.0 (c (n "lrg") (v "0.3.0") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "humansize") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2.2") (d #t) (k 0)))) (h "1c426ss7gi48v635mnfr0gl1gh14dgfiglbmf1kmmxqwvv5xa5gx")))

