(define-module (crates-io wz -u wz-utf16) #:use-module (crates-io))

(define-public crate-wz-utf16-1.0.2 (c (n "wz-utf16") (v "1.0.2") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "speculoos") (r "^0.9.0") (d #t) (k 2)) (d (n "wz-core") (r "^1.0.0") (d #t) (k 0)))) (h "0sv5cr7aq177smxsadpwfc8kwshn9mhmbv493pnnjppskmrpc794") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd"))))))

