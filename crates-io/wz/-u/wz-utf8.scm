(define-module (crates-io wz -u wz-utf8) #:use-module (crates-io))

(define-public crate-wz-utf8-0.1.0 (c (n "wz-utf8") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 0)) (d (n "speculoos") (r "^0.9.0") (d #t) (k 0)) (d (n "wz-core") (r "^0.1.0") (d #t) (k 0)))) (h "146dh6saymdh24dhlzrnazyn8549m43a98mjdc3q10wdd2sh97bl") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd"))))))

(define-public crate-wz-utf8-0.1.1 (c (n "wz-utf8") (v "0.1.1") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "speculoos") (r "^0.9.0") (d #t) (k 2)) (d (n "wz-core") (r "^0.1.0") (d #t) (k 0)))) (h "1nfqfs3pb6kwmjaipn677m92zlywz7gidbfz3svlxyv7py3zkank") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd"))))))

(define-public crate-wz-utf8-1.0.0 (c (n "wz-utf8") (v "1.0.0") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "speculoos") (r "^0.9.0") (d #t) (k 2)) (d (n "wz-core") (r "^1.0.0") (d #t) (k 0)))) (h "00nr5jq675ibhbix5jmqa0wvcaiyzblvxqv4jx8v2am0v79jdpk6") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd"))))))

(define-public crate-wz-utf8-1.0.1 (c (n "wz-utf8") (v "1.0.1") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "speculoos") (r "^0.9.0") (d #t) (k 2)) (d (n "wz-core") (r "^1.0.0") (d #t) (k 0)))) (h "0ifiyl67ji1kgalvwf0cviwjw79vf5p2lg7jzaing7mrxqzw89kx") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd"))))))

