(define-module (crates-io wz -f wz-fmt) #:use-module (crates-io))

(define-public crate-wz-fmt-0.1.0 (c (n "wz-fmt") (v "0.1.0") (d (list (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "wz-core") (r "^0.1.0") (d #t) (k 0)))) (h "1pvw1fkdi04sj96wj79fmdnm1z1spq8gnhsqk4zvrnvsw78k2fj7")))

(define-public crate-wz-fmt-0.1.1 (c (n "wz-fmt") (v "0.1.1") (d (list (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "wz-core") (r "^0.1.0") (d #t) (k 0)))) (h "08v2w4za02zci25bsvnbdwqnxvxkgwjzilkgzv6q3y4fd49cnfpr")))

(define-public crate-wz-fmt-1.0.0 (c (n "wz-fmt") (v "1.0.0") (d (list (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "wz-core") (r "^1.0.0") (d #t) (k 0)))) (h "1p9v8nd046nnwh2q6rhklpk7w5qb0ybfrzv3h9w010cln3fz00ha")))

(define-public crate-wz-fmt-1.0.1 (c (n "wz-fmt") (v "1.0.1") (d (list (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tabled") (r "^0.8.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "wz-core") (r "^1.0.0") (d #t) (k 0)))) (h "1hzcgqhih20qzhmrqy99sqbwcwdd1dapabyii9s5kijaayhyj4ka")))

(define-public crate-wz-fmt-1.0.3 (c (n "wz-fmt") (v "1.0.3") (d (list (d (n "rayon") (r "^1.5.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.2.1") (d #t) (k 0)) (d (n "wz-core") (r "^1.0.0") (d #t) (k 0)))) (h "0cmx6vk6lr5ljnc14g7yrsliib86z81pgkkc3nff2xp2awrpsgma")))

