(define-module (crates-io wz iu wzium-library) #:use-module (crates-io))

(define-public crate-wzium-library-0.0.2 (c (n "wzium-library") (v "0.0.2") (h "0fxkcc82d6h732s2n6grls20zm2n0s17ln1pp1ihwvfihh4ingbg")))

(define-public crate-wzium-library-0.0.3 (c (n "wzium-library") (v "0.0.3") (h "1sdib361zrb89cw9ij7xpm7hq6i3rp6q9341ydrhg0lb3lqhq0vr")))

