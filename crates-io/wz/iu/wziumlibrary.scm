(define-module (crates-io wz iu wziumlibrary) #:use-module (crates-io))

(define-public crate-wziumLibrary-0.0.1 (c (n "wziumLibrary") (v "0.0.1") (h "1ifqgymyilx74mdxg84cmy3azd5xj0byhxkqpj4chhvbbvmdb8zj")))

(define-public crate-wziumLibrary-0.0.2 (c (n "wziumLibrary") (v "0.0.2") (h "1rhphmcwc5g4jcnc0arc144dy9r280wy777scph6ic5v3hy7a6w9")))

