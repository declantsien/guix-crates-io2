(define-module (crates-io wz ha wzhao_art) #:use-module (crates-io))

(define-public crate-wzhao_art-0.1.0 (c (n "wzhao_art") (v "0.1.0") (h "1jhp5wwr7f5nbw4cqvbbhnvk7nzqzppvbkif6ppg9y05jrizx5n9")))

(define-public crate-wzhao_art-0.1.1 (c (n "wzhao_art") (v "0.1.1") (h "0j5h8l70inq197bqpbqblqg5l7l9n1arrxhhn2sr348rjhnybgww")))

(define-public crate-wzhao_art-0.1.2 (c (n "wzhao_art") (v "0.1.2") (h "0xzc99k82p6pkr91ihpvhqj645a2lvplpnczqsl0h519f34d35rd")))

