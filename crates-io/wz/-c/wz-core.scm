(define-module (crates-io wz -c wz-core) #:use-module (crates-io))

(define-public crate-wz-core-0.1.0 (c (n "wz-core") (v "0.1.0") (h "0dy1pgk5l3amdgkfbrcny0rfajrzi7wd4inyxz4pm8xr3rmqczxw")))

(define-public crate-wz-core-0.1.1 (c (n "wz-core") (v "0.1.1") (h "0jgmd6xyz791j775zbnrfrs4pfqwrxnxjhlngg5h8gzxdhldq5rs")))

(define-public crate-wz-core-1.0.0 (c (n "wz-core") (v "1.0.0") (h "1hxc4b1s9kgvymlhlww41zm8ffhj6sgsy535ly20q40vxfn7iqgb")))

