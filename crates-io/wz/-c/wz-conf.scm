(define-module (crates-io wz -c wz-conf) #:use-module (crates-io))

(define-public crate-wz-conf-0.1.0 (c (n "wz-conf") (v "0.1.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("std" "color" "suggestions" "derive" "wrap_help"))) (k 0)))) (h "0xhs5605dfm0q4f1mvylk90nria2g69rifzvqswmv9ghmqays4hw")))

(define-public crate-wz-conf-0.1.1 (c (n "wz-conf") (v "0.1.1") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("std" "color" "suggestions" "derive" "wrap_help"))) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 1)) (d (n "clap_mangen") (r "^0.1") (d #t) (k 1)))) (h "1d4dgcc4mps0jncndfc8xbnh56iwzqjnp3ga9vn3gqxlfspspwm0")))

(define-public crate-wz-conf-0.1.3 (c (n "wz-conf") (v "0.1.3") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("std" "color" "suggestions" "derive" "wrap_help"))) (k 0)) (d (n "clap") (r "^3.2.20") (f (quote ("derive"))) (d #t) (k 1)) (d (n "clap_complete") (r "^3.2.4") (d #t) (k 1)) (d (n "clap_mangen") (r "^0.1") (d #t) (k 1)))) (h "00b8b2y6vg3mm35d2iplffj04whlihxzjkkl50zb4kw1hw4mxb0w")))

(define-public crate-wz-conf-1.0.0 (c (n "wz-conf") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.20") (f (quote ("std" "color" "suggestions" "derive" "wrap_help"))) (k 0)))) (h "1y4nwhp8lvn85nwgqrj1yg30c3jrf3dqh2mqlzkiinmh6000s7hr")))

(define-public crate-wz-conf-1.0.1 (c (n "wz-conf") (v "1.0.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("std" "color" "suggestions" "derive" "wrap_help"))) (k 0)))) (h "16jccrh9yzm547w5fxm6ncppwfginpgsnryr2y1y4lb2k7j4aaa7")))

