(define-module (crates-io wz -p wz-publish-test) #:use-module (crates-io))

(define-public crate-wz-publish-test-0.1.0 (c (n "wz-publish-test") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03zrbm74nayf7yayl57ilzrvmdypmg7az0l3rrrm97byk2bacxkr")))

(define-public crate-wz-publish-test-0.1.1 (c (n "wz-publish-test") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01ccpydy1cqckpdxzbqdqkv5pxbyyiclvxdc57yv6n7vf11ixvv2")))

(define-public crate-wz-publish-test-0.1.2 (c (n "wz-publish-test") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0340ijn6bcr5balqyc8am6pixr85n266a2km2y6bd7mi6lfn4q3g")))

(define-public crate-wz-publish-test-0.1.3 (c (n "wz-publish-test") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0p8yham2ml52z2kijv3q5g37dyypgklrxhs10lqima00kfclgjpz")))

(define-public crate-wz-publish-test-0.1.4 (c (n "wz-publish-test") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1rf5gqm3i884pddpv9bcryq3dbfhni3li8jgr0pikmfgsqdklybw")))

