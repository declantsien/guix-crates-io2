(define-module (crates-io h3 ro h3ron-h3-sys) #:use-module (crates-io))

(define-public crate-h3ron-h3-sys-3.7.1 (c (n "h3ron-h3-sys") (v "3.7.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "0pv39ndwprdv1q2885w47nhccl4bw4m83g8rf9lgfrfv7lybznrd") (y #t)))

(define-public crate-h3ron-h3-sys-0.6.0 (c (n "h3ron-h3-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1v64lpv94x3ks0n68bj99jww590fv0zn9zhqkhn24krc560nmpfr")))

(define-public crate-h3ron-h3-sys-0.7.0 (c (n "h3ron-h3-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "15x784dxj96ngdkqbfl3wlvlay8rp5gmlg3grnnm3hlz9mn8skqg")))

(define-public crate-h3ron-h3-sys-0.7.1 (c (n "h3ron-h3-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "0y53yqgavwm6l81m1w3zpwpwmlrl6s8qpr9vkzgnx7mfsji7hlc8")))

(define-public crate-h3ron-h3-sys-0.7.2 (c (n "h3ron-h3-sys") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1fhj1y9rd9vvp5b0alxz620kcf63v8a4rqsqrjnw4n59a74vvwhy")))

(define-public crate-h3ron-h3-sys-0.8.0 (c (n "h3ron-h3-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0806zxyk1v3pjlxkz1ilgzfra7j200v2i4ccwbgq3dcnia9k8s14")))

(define-public crate-h3ron-h3-sys-0.8.1 (c (n "h3ron-h3-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1hd5000vbxb87nbwdrvy0x0zmjhmp4fxry3kq0gclr8wf48jbb6j")))

(define-public crate-h3ron-h3-sys-0.9.0 (c (n "h3ron-h3-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "05giyzb60f125prfsg4pg8l93xg1mbxigsfnkvm4jf70ab1vkd5p")))

(define-public crate-h3ron-h3-sys-0.10.0 (c (n "h3ron-h3-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1wfln6zbxb2vwydiik4iknfcjpz80gs26y2xd8383z5zi5fnx3j9")))

(define-public crate-h3ron-h3-sys-0.11.0 (c (n "h3ron-h3-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1w8pvqhy3dflbcsxrh0nf86dplwfjcfs5d0vlabzfq3ddmccp1aq")))

(define-public crate-h3ron-h3-sys-0.12.0 (c (n "h3ron-h3-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0a3xjwixymx8s734217p0yqiyd0l70y5v1xf0cq1p8zygb83k06c")))

(define-public crate-h3ron-h3-sys-0.13.0 (c (n "h3ron-h3-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1mkzfxsv43qykb63hv57gcplm9bl3iahv2s0l1p4g8v916snklma")))

(define-public crate-h3ron-h3-sys-0.14.0 (c (n "h3ron-h3-sys") (v "0.14.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 1)))) (h "0bhaaljb2ff6k6z7x1aakmpjkr8kjxnxp0lk5vg938ghdra76v4p")))

(define-public crate-h3ron-h3-sys-0.15.0 (c (n "h3ron-h3-sys") (v "0.15.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1.5.5") (d #t) (k 1)))) (h "188cjjw8yg2gksraszlvai8sppgl1pnqpb4c57p2lnjdv9z1pbnx")))

(define-public crate-h3ron-h3-sys-0.15.1 (c (n "h3ron-h3-sys") (v "0.15.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1.5.5") (d #t) (k 1)))) (h "0swch1l4dmhnpx2ps1aqx0v301v5zj0kcjx2j4qylphlzm6cinmi")))

(define-public crate-h3ron-h3-sys-0.15.2 (c (n "h3ron-h3-sys") (v "0.15.2") (d (list (d (n "bindgen") (r "^0.61") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1.5.5") (d #t) (k 1)))) (h "044zqkqrqnypjm9kfmwviy47c157j19csf8pydvqpz6lsvwkkh08")))

(define-public crate-h3ron-h3-sys-0.16.0 (c (n "h3ron-h3-sys") (v "0.16.0") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1.5.5") (d #t) (k 1)))) (h "1xla512sf42qfqqkd9yrlf5gfi563435jq45p72bq3dh6r132szi")))

(define-public crate-h3ron-h3-sys-0.17.0 (c (n "h3ron-h3-sys") (v "0.17.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "geo-types") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 1)) (d (n "regex") (r "^1.5.5") (d #t) (k 1)))) (h "0hx04b2h429axcy50kiwlha2p3z91va5fjippmzydiwzy63g05wg")))

