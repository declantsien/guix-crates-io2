(define-module (crates-io h3 o- h3o-zip) #:use-module (crates-io))

(define-public crate-h3o-zip-0.1.6 (c (n "h3o-zip") (v "0.1.6") (d (list (d (n "bitvec") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "byteorder") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "criterion") (r "^0.5") (f (quote ("plotters" "html_reports"))) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "h3o") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "h3o-bit") (r "^0.1") (k 0)))) (h "1x25ixxkpsf7h48zk6ksjqc0y25w8nz6dls4p3rk2raq27axrcn5")))

