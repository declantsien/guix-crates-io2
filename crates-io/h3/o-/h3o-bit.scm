(define-module (crates-io h3 o- h3o-bit) #:use-module (crates-io))

(define-public crate-h3o-bit-0.1.0 (c (n "h3o-bit") (v "0.1.0") (h "0njjz7cl91h9pybj1f0msgkgpnsbv0f6p2cs24qs1fmv10glix7v")))

(define-public crate-h3o-bit-0.1.1 (c (n "h3o-bit") (v "0.1.1") (h "087kgspjbhbhbgc440qdf5mmlm3vj7hnggqsg19h731pc205xd3g")))

