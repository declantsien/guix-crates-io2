(define-module (crates-io h3 o- h3o-ice) #:use-module (crates-io))

(define-public crate-h3o-ice-0.1.0 (c (n "h3o-ice") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "fst") (r "^0.4") (k 0)) (d (n "h3o") (r "^0.4") (k 0)) (d (n "thc") (r "^0.1") (k 2)))) (h "02zlr07yjni3nyip5ngsw0z9b2b7bmaxr8sfpry4mbjbynpzy3h9")))

(define-public crate-h3o-ice-0.1.1 (c (n "h3o-ice") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "fst") (r "^0.4") (k 0)) (d (n "h3o") (r "^0.5") (k 0)) (d (n "thc") (r "^0.1") (k 2)))) (h "12scd41kg6cmh5b32xd37fvs9j1rxdcs51y1grkqghvcp4k55917")))

(define-public crate-h3o-ice-0.1.2 (c (n "h3o-ice") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "fst") (r "^0.4") (k 0)) (d (n "h3o") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "thc") (r "^0.1") (k 2)))) (h "1q93hfp0y5b5wg77g57wdy14j8diixbmlh3qhq75psssmsnsrgnq")))

(define-public crate-h3o-ice-0.1.3 (c (n "h3o-ice") (v "0.1.3") (d (list (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (k 2)) (d (n "either") (r "^1.0") (k 0)) (d (n "fst") (r "^0.4") (k 0)) (d (n "h3o") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "h3o-zip") (r "^0.1") (k 2)))) (h "032cd09wszqmhrkvhm5vsc6zl61gmjvkjhkx68n48c9sr29pv7ax")))

