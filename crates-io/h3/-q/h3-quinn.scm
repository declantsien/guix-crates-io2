(define-module (crates-io h3 -q h3-quinn) #:use-module (crates-io))

(define-public crate-h3-quinn-0.0.0 (c (n "h3-quinn") (v "0.0.0") (h "1zpl1rdg0r96cd8yq24xlzg4mff8i39i3wlzf3hbjh8ij8zxnkic")))

(define-public crate-h3-quinn-0.0.1 (c (n "h3-quinn") (v "0.0.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("io"))) (k 0)) (d (n "h3") (r "^0.0.1") (d #t) (k 0)) (d (n "quinn") (r "^0.8.0") (k 0)) (d (n "quinn-proto") (r "^0.8.0") (k 0)))) (h "0grrch2ygrkf7zfwqrx26nq31zh41f8f78d23zxmkrm5r5xj745w")))

(define-public crate-h3-quinn-0.0.2 (c (n "h3-quinn") (v "0.0.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "h3") (r "^0.0.2") (d #t) (k 0)) (d (n "quinn") (r "^0.9.3") (k 0)) (d (n "quinn-proto") (r "^0.9.2") (k 0)) (d (n "tokio-util") (r "^0.7.7") (d #t) (k 0)))) (h "01pdiilldd049808ad59g60z7ihjy5rk5f620ja30nz6501frmk2") (r "1.59")))

(define-public crate-h3-quinn-0.0.3 (c (n "h3-quinn") (v "0.0.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.27") (d #t) (k 0)) (d (n "h3") (r "^0.0.2") (d #t) (k 0)) (d (n "quinn") (r "^0.10") (k 0)) (d (n "quinn-proto") (r "^0.10") (k 0)) (d (n "tokio-util") (r "^0.7.7") (d #t) (k 0)))) (h "0kf6bqmm751gwj24dqgb2rrwq8ibhv7z5v7ix4pfiwz4ccbiljid") (r "1.63")))

(define-public crate-h3-quinn-0.0.4 (c (n "h3-quinn") (v "0.0.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "h3") (r "^0.0.3") (d #t) (k 0)) (d (n "quinn") (r "^0.10") (f (quote ("futures-io"))) (k 0)) (d (n "quinn-proto") (r "^0.10") (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "1r0sm0j51crlfpy2j1wfhgpg2lrfq2xmf5qjd98ksg3h9l0pb5mc") (r "1.63")))

(define-public crate-h3-quinn-0.0.5 (c (n "h3-quinn") (v "0.0.5") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "h3") (r "^0.0.4") (d #t) (k 0)) (d (n "quinn") (r "^0.10") (f (quote ("futures-io"))) (k 0)) (d (n "quinn-proto") (r "^0.10") (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "0ii06bi5a19k4qfkppn5019nw8xca2wzfl66cax949jc1v66ny3k") (r "1.63")))

(define-public crate-h3-quinn-0.0.6 (c (n "h3-quinn") (v "0.0.6") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "h3") (r "^0.0.5") (d #t) (k 0)) (d (n "quinn") (r "^0.11") (f (quote ("futures-io"))) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (k 0)) (d (n "tokio-util") (r "^0.7.9") (d #t) (k 0)))) (h "0lb5ppa583pvg5qvr7bm5r7y5y8djin5w4yz9p9jz0fgsycivh5q") (r "1.66")))

