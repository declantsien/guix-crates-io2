(define-module (crates-io h3 -w h3-webtransport) #:use-module (crates-io))

(define-public crate-h3-webtransport-0.1.0 (c (n "h3-webtransport") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "h3") (r "^0.0.3") (f (quote ("i-implement-a-third-party-backend-and-opt-into-breaking-changes"))) (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2") (k 0)) (d (n "tokio") (r "^1.28") (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1g7m7llnykjxkz878y3vqhglb1fkrzfd2s8ic07xpr6k81k99y1h")))

