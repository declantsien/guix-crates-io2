(define-module (crates-io dl q- dlq-core) #:use-module (crates-io))

(define-public crate-dlq-core-0.1.0 (c (n "dlq-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "aws-config") (r "^1.1") (f (quote ("behavior-version-latest"))) (d #t) (k 0)) (d (n "aws-sdk-sqs") (r "^1") (d #t) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sqlx") (r "^0.7") (f (quote ("sqlite" "macros" "runtime-tokio-rustls"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread" "parking_lot"))) (d #t) (k 0)))) (h "097bd58w5q3fbpvaj50vpvfkwdhq9j141bc0r5w76qaw2niisxmd")))

