(define-module (crates-io dl q- dlq-cli) #:use-module (crates-io))

(define-public crate-dlq-cli-0.1.0 (c (n "dlq-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "color-eyre") (r "^0.6.1") (d #t) (k 0)) (d (n "dlq-gateway-grpc") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)))) (h "08k23xnvdfz33mlz5j4qyxmg4cqnm7cfs7hy7m3nrcg9aksw941m")))

