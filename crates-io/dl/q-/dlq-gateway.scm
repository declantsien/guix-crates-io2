(define-module (crates-io dl q- dlq-gateway) #:use-module (crates-io))

(define-public crate-dlq-gateway-0.1.0 (c (n "dlq-gateway") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dlq-gateway-grpc") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)))) (h "02z84cm97fbyzczmfwichfmajl6svnq7c8g3yrc1dxd169ifb70n")))

