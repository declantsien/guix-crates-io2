(define-module (crates-io dl q- dlq-gateway-grpc) #:use-module (crates-io))

(define-public crate-dlq-gateway-grpc-0.1.0 (c (n "dlq-gateway-grpc") (v "0.1.0") (d (list (d (n "prost") (r "^0.10.3") (d #t) (k 0)) (d (n "tonic") (r "^0.7.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.7.2") (d #t) (k 1)))) (h "1r4l8k0myl2v96hhsv7sjf6khfldgghml4rh9r6wg4fh7bldqix5")))

