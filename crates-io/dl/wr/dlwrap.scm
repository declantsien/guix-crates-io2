(define-module (crates-io dl wr dlwrap) #:use-module (crates-io))

(define-public crate-dlwrap-0.1.0 (c (n "dlwrap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clang") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1cihk4xiq4w04zkl5708dln8s5z8n4zd5lbinqn4cf9g5zzsgssi")))

(define-public crate-dlwrap-0.1.1 (c (n "dlwrap") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clang") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0kq1c1dadxqcdr0ws75zibbfm5dmci844rsqi9c8jz9ndnycmsr1")))

(define-public crate-dlwrap-0.2.0 (c (n "dlwrap") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clang") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0lna5i82qf266rb7n9r03y584gv8139nhqdrx3d6vbvidjq2pij3")))

(define-public crate-dlwrap-0.3.0 (c (n "dlwrap") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clang") (r "^2") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "01wpp284ww6qdlmns4a29dp5rwrjzmchvxzi11l08agld94jbxkb")))

