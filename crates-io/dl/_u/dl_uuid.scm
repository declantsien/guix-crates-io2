(define-module (crates-io dl _u dl_uuid) #:use-module (crates-io))

(define-public crate-dl_uuid-0.4.0 (c (n "dl_uuid") (v "0.4.0") (d (list (d (n "arc-swap") (r "^0.4.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (d #t) (k 0)))) (h "089wnqd5vn94ynpz3i47w3njjc14ms92b52wf0bhd8x1l09bkbgh")))

(define-public crate-dl_uuid-0.4.1 (c (n "dl_uuid") (v "0.4.1") (d (list (d (n "arc-swap") (r "^0.4.8") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)))) (h "0wx0492fl3rik0ir0m423bs2fykmm1y7kighs8ks0wmsps20s7hn")))

