(define-module (crates-io dl -o dl-openvdb-query-sys) #:use-module (crates-io))

(define-public crate-dl-openvdb-query-sys-0.1.0 (c (n "dl-openvdb-query-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking"))) (d #t) (k 1)))) (h "177mhwpfhp7k4rl99b13n930pmm5ypjy7nznd31ir42ayqpy4127") (f (quote (("download_3delight_lib"))))))

(define-public crate-dl-openvdb-query-sys-0.1.1 (c (n "dl-openvdb-query-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.10.7") (f (quote ("blocking"))) (d #t) (k 1)))) (h "07xfdyid3gbwjzpmsx61blv088lbd99l7fpvnrkxlnkiaiha2r2c") (f (quote (("download_3delight_lib"))))))

(define-public crate-dl-openvdb-query-sys-0.1.2 (c (n "dl-openvdb-query-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "reqwest") (r "^0.11.3") (f (quote ("blocking"))) (d #t) (k 1)))) (h "17ani336f6v70p05ck1y9zidmrdxcnszmx3agy5f6pi9md9hni31") (f (quote (("download_3delight_lib"))))))

(define-public crate-dl-openvdb-query-sys-0.1.3 (c (n "dl-openvdb-query-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 1)))) (h "094dk0ahkb6vhf1f2jsa8fj8ip5a0vvvwzh378skazvk98ragpz5") (f (quote (("download_3delight_lib"))))))

