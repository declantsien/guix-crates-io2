(define-module (crates-io dl ar dlarm) #:use-module (crates-io))

(define-public crate-dlarm-0.1.0 (c (n "dlarm") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "man") (r "^0.1.1") (d #t) (k 0)))) (h "10lh33nwf3za2fwibf3w3a3b6vh4qs3zz4vja6dljvksl7zkjh9f")))

(define-public crate-dlarm-0.3.0 (c (n "dlarm") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^0.10.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "colored") (r "^1.7.0") (d #t) (k 0)) (d (n "dirs") (r "^1.0.4") (d #t) (k 0)) (d (n "humantime") (r "^1.2.0") (d #t) (k 0)) (d (n "man") (r "^0.2.0") (d #t) (k 0)) (d (n "predicates") (r "^1.0.0") (d #t) (k 0)))) (h "1jc0bqk1k2lvzvjhn4kndhxi9k5pjwrzx6fybhc3412anfmaqg0b")))

