(define-module (crates-io dl ib dlib-face-recognition-cv) #:use-module (crates-io))

(define-public crate-dlib-face-recognition-cv-0.1.0 (c (n "dlib-face-recognition-cv") (v "0.1.0") (d (list (d (n "cpp") (r "^0.5") (d #t) (k 0)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "dlib-face-recognition") (r "^0.1") (d #t) (k 0)) (d (n "opencv") (r "^0.38") (d #t) (k 0)))) (h "0asbf7n80ql5px2wf6rbrlhkkhh13j0rrda4y3ssq3bpg5fn5g83")))

