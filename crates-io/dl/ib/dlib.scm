(define-module (crates-io dl ib dlib) #:use-module (crates-io))

(define-public crate-dlib-0.1.0 (c (n "dlib") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0j144whxxd484z5y1gjpygvyxrqkj1r7lmdi2qmafl5isk2p26wy") (f (quote (("dlopen"))))))

(define-public crate-dlib-0.1.1 (c (n "dlib") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1wdmxjqdlq2af6n05mbsf31ymv8322bcbk9imzjwwic346xr04x4") (f (quote (("dlopen"))))))

(define-public crate-dlib-0.2.0 (c (n "dlib") (v "0.2.0") (d (list (d (n "libloading") (r "^0.2.0") (d #t) (k 0)))) (h "09ph9z9hcnppkc7d2zc8c4cm44c4fdl9gnijgfv8m1xr1ajjbgwq") (f (quote (("dlopen"))))))

(define-public crate-dlib-0.3.0 (c (n "dlib") (v "0.3.0") (d (list (d (n "libloading") (r "^0.2.0") (d #t) (k 0)))) (h "0582m6dbl9kgfmxnrdh90aqwrwyc4jw9yn7icvzy9mrk1pq1bl4b") (f (quote (("dlopen"))))))

(define-public crate-dlib-0.3.1 (c (n "dlib") (v "0.3.1") (d (list (d (n "libloading") (r "^0.3.1") (d #t) (k 0)))) (h "1v36322ilmdd1w3kh2xhm58ma9mxq9i4xdcwy84lav63w56cx2ql") (f (quote (("dlopen"))))))

(define-public crate-dlib-0.4.0 (c (n "dlib") (v "0.4.0") (d (list (d (n "libloading") (r "^0.4.0") (d #t) (k 0)))) (h "0z4yy859iqw5l5l5rs7yznlrgbdx46gn451hkcnfcmnmi27qslcm") (f (quote (("dlopen"))))))

(define-public crate-dlib-0.4.1 (c (n "dlib") (v "0.4.1") (d (list (d (n "libloading") (r "^0.5.0") (d #t) (k 0)))) (h "0smp2cdvy12xfw26qyqms273w5anszfadv73g75s88yqm54i5rbp") (f (quote (("dlopen"))))))

(define-public crate-dlib-0.4.2 (c (n "dlib") (v "0.4.2") (d (list (d (n "libloading") (r "^0.6") (d #t) (k 0)))) (h "0xlsf3lrz9hd7q3ff6lp5mw4kn3nbryn746kd07i93r6wg8ia7xi") (f (quote (("dlopen"))))))

(define-public crate-dlib-0.5.0 (c (n "dlib") (v "0.5.0") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1547hy7nrhkrb2i09va244c0h8mr845ccbs2d2mc414c68bpa6xc")))

(define-public crate-dlib-0.5.1 (c (n "dlib") (v "0.5.1") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0qkfggq7zik86jj83kcs22agpwpkrz5b4344mp3djrmg5c6a04hm")))

(define-public crate-dlib-0.5.2 (c (n "dlib") (v "0.5.2") (d (list (d (n "libloading") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "04m4zzybx804394dnqs1blz241xcy480bdwf3w9p4k6c3l46031k")))

