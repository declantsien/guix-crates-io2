(define-module (crates-io dl sc dlscan_tools) #:use-module (crates-io))

(define-public crate-dlscan_tools-0.1.0 (c (n "dlscan_tools") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)))) (h "169yn9s66cgfpqs28wil58ygxcm6b9cs7i3j6vil2yvnjnh5ahv0")))

(define-public crate-dlscan_tools-0.1.1 (c (n "dlscan_tools") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)))) (h "0xjppim3pb12crzp6si8vpw13yg55w05yx4kjdc7rswzs4mp8d1s")))

(define-public crate-dlscan_tools-0.1.2 (c (n "dlscan_tools") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12") (d #t) (k 0)))) (h "1yr4kqkhf23v8c9lg0hwd1866wc2gdx7fnaca0ijqd5yaf2lfwrm")))

