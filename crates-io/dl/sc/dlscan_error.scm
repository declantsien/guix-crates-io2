(define-module (crates-io dl sc dlscan_error) #:use-module (crates-io))

(define-public crate-dlscan_error-0.1.0 (c (n "dlscan_error") (v "0.1.0") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.23") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "1gfwkwclkqm95ab4j69bfzndmk9rbclvp5vwmx7a7in1vk5mhcpz")))

(define-public crate-dlscan_error-0.1.1 (c (n "dlscan_error") (v "0.1.1") (d (list (d (n "json") (r "^0.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.23") (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0chh58s751zyjdi4l50izfdkh2xgh3knf5qby2f1ap6s66b065zm")))

