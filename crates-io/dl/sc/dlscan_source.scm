(define-module (crates-io dl sc dlscan_source) #:use-module (crates-io))

(define-public crate-dlscan_source-0.1.0 (c (n "dlscan_source") (v "0.1.0") (d (list (d (n "dlscan_tools") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "06w31pabwh5ymi13qjfg1hyk7x7s8p58p0yigja0md8j5rf6ri0l")))

(define-public crate-dlscan_source-0.1.1 (c (n "dlscan_source") (v "0.1.1") (d (list (d (n "dlscan_tools") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "zip") (r "^0.5") (d #t) (k 0)))) (h "0qa1z7h136qnjsm1d2im8qlwqmssrir5a5gp0bpalpdcwamfv35c")))

