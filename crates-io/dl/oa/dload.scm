(define-module (crates-io dl oa dload) #:use-module (crates-io))

(define-public crate-dload-0.1.0 (c (n "dload") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("stream"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.11") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0wczvmq1rg76hvd34rg9x2a7gqklsxbbf2920w9w8bz4v95ipq27")))

(define-public crate-dload-0.1.1 (c (n "dload") (v "0.1.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("stream"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.11") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1lcwx1zwpxjmimk9sxbprxiyblp3xhdjwzp6ns224c73q6xpy5vj")))

(define-public crate-dload-0.1.2 (c (n "dload") (v "0.1.2") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("stream"))) (d #t) (k 0)) (d (n "sysinfo") (r "^0.30.11") (d #t) (k 2)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 2)))) (h "10iswlr2shdm693x4c6kqvbnq6ikd87c8pacp3jfg02mi0axgylc")))

