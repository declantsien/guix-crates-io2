(define-module (crates-io dl op dlopen_derive) #:use-module (crates-io))

(define-public crate-dlopen_derive-0.1.0 (c (n "dlopen_derive") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1f0iifysdi8307za5901v9sqp712gzv1r1sfjd9mni7czymvfdl5")))

(define-public crate-dlopen_derive-0.1.1 (c (n "dlopen_derive") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0sa8jvfq6ajg7iw4g5fccs6x1aj3c4xc32khagfd9yp7p40ycsn8")))

(define-public crate-dlopen_derive-0.1.2 (c (n "dlopen_derive") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "0mdc18yrvvwibkh94inay9znvrmz9zivx9h6mk4zaf57la4kpc0a")))

(define-public crate-dlopen_derive-0.1.3 (c (n "dlopen_derive") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1s9h9w3iak0da74spidhbbmv3sclmbv5pi8kf6j0n2fn8giy85kf")))

(define-public crate-dlopen_derive-0.1.4 (c (n "dlopen_derive") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.29") (d #t) (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (d #t) (k 0)))) (h "10f58rcw1xnm5b5xg735ws0cvsz8qzfcigcw1zm1rn7vn7hxjdpj")))

