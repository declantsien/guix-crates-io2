(define-module (crates-io dl op dlopen2_derive) #:use-module (crates-io))

(define-public crate-dlopen2_derive-0.2.0 (c (n "dlopen2_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0y40i0v62g3z2c4n0l67mycckf87lzxxyyf3chi2hsn1p25sq29s")))

(define-public crate-dlopen2_derive-0.3.0 (c (n "dlopen2_derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ww1nq5iz560c39w238zg7z3asr2vbda73kf8lmcx3yync8sxjx6")))

(define-public crate-dlopen2_derive-0.4.0 (c (n "dlopen2_derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0m2cpmwki0lklcql1n6zyijhidb5lqrxv3f2xd2zbmv273q9pfgj")))

