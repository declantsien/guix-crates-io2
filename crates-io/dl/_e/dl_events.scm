(define-module (crates-io dl _e dl_events) #:use-module (crates-io))

(define-public crate-dl_events-0.1.0 (c (n "dl_events") (v "0.1.0") (d (list (d (n "candid") (r "^0.7.14") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5.2") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.5.2") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "16b78mnwdwvlsfjba0ardmz3f2m9ax7vh9vlvr7z9nqac57hra98")))

(define-public crate-dl_events-0.1.1 (c (n "dl_events") (v "0.1.1") (d (list (d (n "candid") (r "^0.7.14") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5.2") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.5.2") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "00h4ah3r9b5fw7pcfhy5hf3xz6ssvkj8w0f5cmlb239r59pwh0pr")))

