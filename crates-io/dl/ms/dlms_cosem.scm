(define-module (crates-io dl ms dlms_cosem) #:use-module (crates-io))

(define-public crate-dlms_cosem-0.1.0 (c (n "dlms_cosem") (v "0.1.0") (d (list (d (n "aes") (r "^0.7.5") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.9.4") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "derive-try-from-primitive") (r "^1.0") (d #t) (k 0)) (d (n "mbusparse") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0ksagg34zbpjdz7mq7ji52n23az90qq4y1vqvmzbz76y89r2ks3j")))

(define-public crate-dlms_cosem-0.1.1 (c (n "dlms_cosem") (v "0.1.1") (d (list (d (n "aes") (r "^0.7") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "derive-try-from-primitive") (r "^1.0") (d #t) (k 0)) (d (n "mbusparse") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0lnxbfv0r8ajlcpp3q1pwmfbiqwahscpl9gg3yx30icx4slqycj6")))

(define-public crate-dlms_cosem-0.2.0 (c (n "dlms_cosem") (v "0.2.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "aes-gcm") (r "^0.9") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "derive-try-from-primitive") (r "^1.0") (d #t) (k 0)) (d (n "mbusparse") (r "^0.1") (d #t) (k 0)) (d (n "nom") (r "^7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0n7bjx7x1i26m2mlh2qm3wnkh7qz8n1c3ngzjdl1nrwzb5x0swys")))

