(define-module (crates-io dl c- dlc-decrypter) #:use-module (crates-io))

(define-public crate-dlc-decrypter-0.0.1 (c (n "dlc-decrypter") (v "0.0.1") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1231hnw4a5sxsx1siwl6m1l2nm446mx0gg3zwk86jw637fzjap9c")))

(define-public crate-dlc-decrypter-0.1.0 (c (n "dlc-decrypter") (v "0.1.0") (d (list (d (n "base64") (r "^0.7.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)))) (h "00fbdcysgqjk1ihp7d0i1fq5g8p9wawi5r0nk1p9sf4yjvx3h8s8")))

(define-public crate-dlc-decrypter-0.2.0 (c (n "dlc-decrypter") (v "0.2.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1xl5wqn9w2vmw32n3fkdkz63ib25lf49snvhx655351yjdrackax")))

(define-public crate-dlc-decrypter-0.2.1 (c (n "dlc-decrypter") (v "0.2.1") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.5") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1jn9l92qmkaf2ry7pp1xmxbafvw3nfz0zzvxp4i0fc3xgij52qcx")))

