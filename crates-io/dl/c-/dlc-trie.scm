(define-module (crates-io dl c- dlc-trie) #:use-module (crates-io))

(define-public crate-dlc-trie-0.1.0 (c (n "dlc-trie") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.27") (d #t) (k 0)) (d (n "dlc") (r "^0.1.0") (d #t) (k 0)) (d (n "secp256k1-zkp") (r "^0.5.0") (d #t) (k 0)))) (h "1v8xqq85xjfipbwrz2bw8c60bnymrb48xlp6hdj66l7nn1dq6xr7")))

(define-public crate-dlc-trie-0.2.0 (c (n "dlc-trie") (v "0.2.0") (d (list (d (n "bitcoin") (r "^0.27") (d #t) (k 0)) (d (n "dlc") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "secp256k1-zkp") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "066682li9kck2p2zr5dga0lgw56l0sh7pwd0db6zvvbj2bdb1qmm") (f (quote (("use-serde" "serde" "dlc/use-serde") ("parallel" "rayon"))))))

(define-public crate-dlc-trie-0.3.0 (c (n "dlc-trie") (v "0.3.0") (d (list (d (n "bitcoin") (r "^0.27") (d #t) (k 0)) (d (n "dlc") (r "^0.3.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "secp256k1-zkp") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "07wf6ja82yh4pqgc9i697vydc7bi0ld3lp7dx154xw9pvvq7ylca") (f (quote (("use-serde" "serde" "dlc/use-serde") ("parallel" "rayon"))))))

(define-public crate-dlc-trie-0.4.0 (c (n "dlc-trie") (v "0.4.0") (d (list (d (n "bitcoin") (r "^0.29.2") (d #t) (k 0)) (d (n "dlc") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "secp256k1-zkp") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "1lfdkg9bsk10kcylrfhxgbmhfb4vkx0w2820rahiira8fs9jx7mm") (f (quote (("use-serde" "serde" "dlc/use-serde") ("parallel" "rayon"))))))

