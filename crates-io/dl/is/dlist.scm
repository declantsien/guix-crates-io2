(define-module (crates-io dl is dlist) #:use-module (crates-io))

(define-public crate-dlist-0.1.0 (c (n "dlist") (v "0.1.0") (d (list (d (n "lipsum") (r "^0.7") (d #t) (k 0)))) (h "1b55ikkj88midxhi09zf3kanhfhs0xg7ka99fzlil19rapkcywrk")))

(define-public crate-dlist-0.1.1 (c (n "dlist") (v "0.1.1") (d (list (d (n "lipsum") (r "^0.7") (d #t) (k 2)))) (h "004jzch3lcfi2za07j259nxw879k4w5iigkc6rayjjqn7rcncdld")))

(define-public crate-dlist-0.1.2 (c (n "dlist") (v "0.1.2") (d (list (d (n "lipsum") (r "^0.7") (d #t) (k 2)))) (h "1s21gqz4dv01wmxc7601f0rkxb83pl5mv2r5xq7ssgsyf5yx2lcx")))

(define-public crate-dlist-0.1.3 (c (n "dlist") (v "0.1.3") (d (list (d (n "lipsum") (r "^0.7") (d #t) (k 2)))) (h "0i3wkf4sq6xwrmv0x03c6pv7l3f69ym3bq1974lqdav3sx65fv9c")))

(define-public crate-dlist-0.1.4 (c (n "dlist") (v "0.1.4") (d (list (d (n "lipsum") (r "^0.7") (d #t) (k 2)))) (h "18w1gzr3j6dxhqqsbxmjz177k6jg7xhkm1sirwr6w5q1zi0za1wq")))

