(define-module (crates-io dl eq dleq_vrf) #:use-module (crates-io))

(define-public crate-dleq_vrf-0.0.1 (c (n "dleq_vrf") (v "0.0.1") (d (list (d (n "ark-bls12-377") (r "^0.4") (f (quote ("curve"))) (k 2)) (d (n "ark-ec") (r "^0.4") (k 0)) (d (n "ark-ff") (r "^0.4") (k 0)) (d (n "ark-serialize") (r "^0.4") (f (quote ("derive"))) (k 0)) (d (n "ark-std") (r "^0.4") (k 0)) (d (n "ark-transcript") (r "^0.0.1") (k 0)) (d (n "arrayvec") (r "^0.7.2") (k 0)) (d (n "rand_chacha") (r "^0.3") (k 0)) (d (n "rand_core") (r "^0.6") (k 0)) (d (n "zeroize") (r "^1.0") (f (quote ("zeroize_derive"))) (k 0)))) (h "0nhy0gnq425dnna1mky1gmjkkx4mn5jc57848gnhfgwn93r8v1y2") (f (quote (("getrandom" "rand_core/getrandom") ("default" "getrandom"))))))

