(define-module (crates-io dl hn dlhn_derive) #:use-module (crates-io))

(define-public crate-dlhn_derive-0.1.0 (c (n "dlhn_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "05f5vk4bgp9z8369b7w7vd6x22m25p97azgdpz5pn2w4kv7qmwi3")))

(define-public crate-dlhn_derive-0.1.1 (c (n "dlhn_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (d #t) (k 0)))) (h "1h10qrm9s8izwp4s8nlz4gfi1g6dwph2qy094jwr7ps5b0fm2yg5")))

