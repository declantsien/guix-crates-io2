(define-module (crates-io dl _a dl_authorize) #:use-module (crates-io))

(define-public crate-dl_authorize-0.1.0 (c (n "dl_authorize") (v "0.1.0") (d (list (d (n "candid") (r "^0.7.14") (d #t) (k 0)) (d (n "ic-cdk") (r "^0.5.2") (d #t) (k 0)) (d (n "ic-cdk-macros") (r "^0.5.2") (d #t) (k 0)) (d (n "ic-stable-structures") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)) (d (n "typed-builder") (r "^0.12.0") (d #t) (k 0)))) (h "1zrpg6cvxmld67sggbjsza2nr177c8r9ijf57nxmg94z08shy1i7")))

