(define-module (crates-io dl _a dl_api) #:use-module (crates-io))

(define-public crate-dl_api-0.1.0 (c (n "dl_api") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "libloaderapi" "winerror" "minwindef" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0kv0v1apq3y9z6s3gg23dam1vgccrvpy5nrwsmcvckkd91n0k6b0")))

(define-public crate-dl_api-0.2.0 (c (n "dl_api") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "libloaderapi" "winerror" "minwindef" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04swzch4fpadb38hljlkji4g83xgvwb5yxvmkj8d658fvpiy0423")))

(define-public crate-dl_api-0.3.0 (c (n "dl_api") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "libloaderapi" "winerror" "minwindef" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ljx6lmj31y66rsajv1yb8cb15hc6ddx2jbs3jpnrsblpm0vlkqn")))

(define-public crate-dl_api-0.3.1 (c (n "dl_api") (v "0.3.1") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser" "libloaderapi" "winerror" "minwindef" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04fwm5bhx7phzfc53ymxrspxn77cnxiqysbp5m1px92d8svdz5dc")))

(define-public crate-dl_api-0.4.0 (c (n "dl_api") (v "0.4.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser" "libloaderapi" "winerror" "minwindef" "errhandlingapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "18v714wpggwr3r0rf338nxlvwap9y30rfx0lpggfbfn17l3m51wf") (f (quote (("docs-rs") ("default"))))))

