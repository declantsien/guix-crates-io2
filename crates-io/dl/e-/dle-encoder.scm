(define-module (crates-io dl e- dle-encoder) #:use-module (crates-io))

(define-public crate-dle-encoder-0.1.1 (c (n "dle-encoder") (v "0.1.1") (h "1arkhmjl2ix8zly6nzic832s0i4hhgz7jjp5sdzf6hmhf3wfs64w")))

(define-public crate-dle-encoder-0.1.2 (c (n "dle-encoder") (v "0.1.2") (h "048675wmgk4as9kpnqznghjvkixbgmk4g3ip336zw1vmf4b7h4gq")))

(define-public crate-dle-encoder-0.1.3 (c (n "dle-encoder") (v "0.1.3") (h "1zrshan5q4psz1jgb2ypxqy77vd0c1ib5w8m0v894hxrig7rzq0v")))

(define-public crate-dle-encoder-0.1.4 (c (n "dle-encoder") (v "0.1.4") (h "06z63cnvw6qvz7ym8wr2c74cvg4fq4z41p3jv5g53bqvhcnpm4i7")))

