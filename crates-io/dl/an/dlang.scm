(define-module (crates-io dl an dlang) #:use-module (crates-io))

(define-public crate-dlang-0.1.0 (c (n "dlang") (v "0.1.0") (h "10wn8n8aipd2d584vccm093s9lkh02j3j964d6qmqx3rhmb6qkkj")))

(define-public crate-dlang-0.1.1 (c (n "dlang") (v "0.1.1") (h "04p7b1pgplhpm0jrfv41z023j9168kpb3lzfsn9zmf7ydq5svbsy")))

(define-public crate-dlang-0.1.2 (c (n "dlang") (v "0.1.2") (h "1zfxskfi2zv05apdcv4pasmfpc2qh5r32ah0p2znqkayykdn8lvr")))

(define-public crate-dlang-0.1.3 (c (n "dlang") (v "0.1.3") (h "0hlcq7xb3zq0d8nlmlry3zzrxhsmppkamj6v2p138kmdkx68dlny")))

