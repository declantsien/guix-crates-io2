(define-module (crates-io dl og dlog_rs) #:use-module (crates-io))

(define-public crate-dlog_rs-0.3.0 (c (n "dlog_rs") (v "0.3.0") (d (list (d (n "dlog_core") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "0nv8h5x905s20baz43b04s3gqwm244nb7bhkxqfv61cgpaqb9k9c") (y #t)))

(define-public crate-dlog_rs-0.3.1 (c (n "dlog_rs") (v "0.3.1") (d (list (d (n "dlog_core") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "1dl2z5fdf2081bd43p7n2myag7vb2hdzd9z508j8wyhrr0n46vds") (y #t)))

(define-public crate-dlog_rs-0.3.2 (c (n "dlog_rs") (v "0.3.2") (d (list (d (n "dlog_core") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "01j1jzwqf4vvn520k4p59l86fac54rc439mvbwcv2j9ifp5cpz8v") (y #t)))

(define-public crate-dlog_rs-0.3.3 (c (n "dlog_rs") (v "0.3.3") (d (list (d (n "dlog_core") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "0qmz5q130rvx7mm5c9dv70c153lzpxlnwvz280v3vb3azfwdpnc1") (y #t)))

(define-public crate-dlog_rs-0.3.4 (c (n "dlog_rs") (v "0.3.4") (d (list (d (n "dlog_core") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "03ij0sd9bn3a2ggmw1k85vg9b1zn5ag3sxr2ivis6jgv1cjkxw7n") (y #t)))

(define-public crate-dlog_rs-0.3.5 (c (n "dlog_rs") (v "0.3.5") (d (list (d (n "dlog_core") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "00n5hfqa5jj9jckz6hqdjp75z7cii5dk780zkpdr0am4s0pid3rj") (y #t)))

(define-public crate-dlog_rs-1.0.0 (c (n "dlog_rs") (v "1.0.0") (d (list (d (n "dlog_core") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "1ss1a5vvkf4k1x4if120zf484f7vyga1znkc7xmc9kfv1v9zl0lp") (y #t)))

(define-public crate-dlog_rs-1.0.1 (c (n "dlog_rs") (v "1.0.1") (d (list (d (n "dlog_core") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "1f04h14smkq4j9nwq2rxckxdbnmjsvxffbhp6hl7d03yy93ridwp") (y #t)))

(define-public crate-dlog_rs-1.0.2 (c (n "dlog_rs") (v "1.0.2") (d (list (d (n "dlog_core") (r "^1.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "06gw7rdvmays3ipvbx68x3i9lyfagf8kq6bh4h62jc5912r39s1w") (y #t)))

(define-public crate-dlog_rs-1.1.0 (c (n "dlog_rs") (v "1.1.0") (d (list (d (n "dlog_core") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "065pbgin5l0lfba3arzxf0vam7a2habhr01zhm3hz1sswnjsy41f") (y #t)))

(define-public crate-dlog_rs-1.1.1 (c (n "dlog_rs") (v "1.1.1") (d (list (d (n "dlog_core") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "1dxnznallilcd4vnpc965v0s8xlkbv1k3029gbl6svbndfg0v5h9") (y #t)))

(define-public crate-dlog_rs-1.1.2 (c (n "dlog_rs") (v "1.1.2") (d (list (d (n "dlog_core") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "0nkg6ca7vqaxr1l2rrp26asspy18jllbm60yff4gdj0wgapz14vv") (y #t)))

(define-public crate-dlog_rs-1.1.3 (c (n "dlog_rs") (v "1.1.3") (d (list (d (n "dlog_core") (r "^1.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "1lp20bkpzpn5lk2yrjhplnqzajy9bwzypmbgjhb069bnkgc6f2a6") (y #t)))

(define-public crate-dlog_rs-1.1.4 (c (n "dlog_rs") (v "1.1.4") (d (list (d (n "dlog_core") (r "^1.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "1hqbhhg3zl01h03xav96cavdz9bvgsw2vld4jrdqg87ys3lfsyp3") (y #t)))

(define-public crate-dlog_rs-1.1.5 (c (n "dlog_rs") (v "1.1.5") (d (list (d (n "dlog_core") (r "^1.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "19jnj2j8ajc4f858ihlanydmxmmd5sx5sl278mywhqbi2nw9jfmz") (y #t)))

(define-public crate-dlog_rs-1.1.6 (c (n "dlog_rs") (v "1.1.6") (d (list (d (n "dlog_core") (r "^1.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "1csgpf164ajy1s9ih05amg5x2yxjky0cavcpgzsdkqq3n6p03irp")))

(define-public crate-dlog_rs-1.1.7 (c (n "dlog_rs") (v "1.1.7") (d (list (d (n "dlog_core") (r "^1.1.7") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)))) (h "0r7935yn6vj7rjfsx11qly8d64w7512fma60682phjsrcf76zplq")))

(define-public crate-dlog_rs-1.1.8 (c (n "dlog_rs") (v "1.1.8") (d (list (d (n "dlog_core") (r "^1.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)))) (h "1jhxsiflycg1hip3j1n3xgyx9ia6zi7fa3hzlq9nvf5dz22sags5")))

