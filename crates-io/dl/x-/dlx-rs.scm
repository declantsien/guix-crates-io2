(define-module (crates-io dl x- dlx-rs) #:use-module (crates-io))

(define-public crate-dlx-rs-0.0.0 (c (n "dlx-rs") (v "0.0.0") (h "1qqic9xnps2bd21c325k8p8aqqrcsfd2qi79d4pnxg4lnss13c3j")))

(define-public crate-dlx-rs-0.0.1 (c (n "dlx-rs") (v "0.0.1") (h "18kd5wyc3nk0acnz8v6ffr45nvfbr3z4rwc30818p99dj40g07ri")))

(define-public crate-dlx-rs-0.0.2 (c (n "dlx-rs") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)))) (h "1af5a999v0wl5494ir54kz35dpw26bkm76bhkg9qgv3jf1g4ibji")))

(define-public crate-dlx-rs-0.0.3 (c (n "dlx-rs") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "00120v5vn9avc613g7id8lj8l3z05appl78r6xx94m36wbas1cy0")))

(define-public crate-dlx-rs-1.0.0 (c (n "dlx-rs") (v "1.0.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "058zlicvg728vvr20jid65lwf7vbws39lh5lczx8722kicj9lfn1")))

(define-public crate-dlx-rs-1.1.0 (c (n "dlx-rs") (v "1.1.0") (d (list (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1xh4svkr320jprppk0gyggfginx0w4y64ax3zpb131n97q1ds5m4")))

