(define-module (crates-io dl t- dlt-tracing-subscriber) #:use-module (crates-io))

(define-public crate-dlt-tracing-subscriber-0.1.0 (c (n "dlt-tracing-subscriber") (v "0.1.0") (d (list (d (n "autoincrement") (r "^1") (f (quote ("derive" "async"))) (d #t) (k 0)) (d (n "libdlt-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1qviclb24cn5qw1gpvwx3yrfnirll6m35phjjs177hh61s5kdy7x")))

(define-public crate-dlt-tracing-subscriber-0.1.1 (c (n "dlt-tracing-subscriber") (v "0.1.1") (d (list (d (n "autoincrement") (r "^1") (f (quote ("derive" "async"))) (d #t) (k 0)) (d (n "libdlt-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1x1xdkd2qbyjn5fvasghsznn320hahbyhs9r7b1gmcfnmbas1fi1")))

(define-public crate-dlt-tracing-subscriber-0.1.2 (c (n "dlt-tracing-subscriber") (v "0.1.2") (d (list (d (n "autoincrement") (r "^1") (f (quote ("derive" "async"))) (d #t) (k 0)) (d (n "libdlt-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0rl1bamg3wv9g55x8wmimpi861fb6xgcjq5qbyfahkwya1ijmj3c")))

