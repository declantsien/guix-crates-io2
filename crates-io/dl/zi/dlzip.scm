(define-module (crates-io dl zi dlzip) #:use-module (crates-io))

(define-public crate-dlzip-0.1.0 (c (n "dlzip") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip") (r "^0.5.11") (d #t) (k 0)))) (h "0v2l2hqxmzcx7hw68sh93aq7mvxc1a0cspn0dxp7842jwg7hgdxv") (y #t)))

(define-public crate-dlzip-0.1.1 (c (n "dlzip") (v "0.1.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "number_prefix") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "zip") (r "^0.5.11") (d #t) (k 0)))) (h "0hg5csn0prc74b6g9wis7kh7kp7i007naclxb9fspan3nl3lrxvw") (y #t)))

