(define-module (crates-io dl in dlint) #:use-module (crates-io))

(define-public crate-dlint-0.1.0 (c (n "dlint") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "swc_atoms") (r "^0.2.2") (d #t) (k 0)) (d (n "swc_common") (r "=0.5.12") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "=0.20.0") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "=0.23.0") (d #t) (k 0)) (d (n "swc_ecma_visit") (r "=0.5.1") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.0") (d #t) (k 0)))) (h "1a5crc5blb2zaqy09pxy1bbxb7z1rdjjxzfr950fsipg3r645s7h")))

