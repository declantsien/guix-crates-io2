(define-module (crates-io dl l- dll-syringe-payload-utils) #:use-module (crates-io))

(define-public crate-dll-syringe-payload-utils-0.1.0 (c (n "dll-syringe-payload-utils") (v "0.1.0") (h "05k16hsqh7bgwp621x9vqjmz6bi3b8i4qciizj2vzk3f9cbscfsh")))

(define-public crate-dll-syringe-payload-utils-0.1.1 (c (n "dll-syringe-payload-utils") (v "0.1.1") (h "0s7fshmgy0xx19zfsrx1m1vl312s410bk2mjl3dcdmpj8gdqzc4a")))

(define-public crate-dll-syringe-payload-utils-0.1.2 (c (n "dll-syringe-payload-utils") (v "0.1.2") (h "1gi4wiiwqq8kzw7k5mrmyavgz9ikw1lg7kz5va5wf2wsdpsr5w49")))

(define-public crate-dll-syringe-payload-utils-0.1.3 (c (n "dll-syringe-payload-utils") (v "0.1.3") (h "0g9havh18xd893qn4k6327pf15kd5nlxfghg6m8fw6lqkyv1jm9f")))

(define-public crate-dll-syringe-payload-utils-0.1.4 (c (n "dll-syringe-payload-utils") (v "0.1.4") (h "1xp28i0qyzxvx9sq00zfjf6rxpyqhn3x483rqr5n1p9abvf7bw30")))

