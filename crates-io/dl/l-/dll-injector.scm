(define-module (crates-io dl l- dll-injector) #:use-module (crates-io))

(define-public crate-dll-injector-0.1.0 (c (n "dll-injector") (v "0.1.0") (d (list (d (n "goblin") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi" "winnt" "minwindef" "errhandlingapi" "handleapi" "memoryapi" "libloaderapi" "synchapi" "winbase"))) (d #t) (k 0)))) (h "0mfb8ncla4x9ikgx7xvkc1lx833k8va8sq6c33gy37dv6w61y26d")))

(define-public crate-dll-injector-0.1.1 (c (n "dll-injector") (v "0.1.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi" "winnt" "minwindef" "errhandlingapi" "handleapi" "memoryapi" "libloaderapi" "synchapi" "winbase"))) (d #t) (k 0)))) (h "1npz60vshs8nc6xb8khwcvgxaj7bkk35s7pblhj9m73cv9yvmm2q")))

(define-public crate-dll-injector-0.1.2 (c (n "dll-injector") (v "0.1.2") (d (list (d (n "exe") (r "^0.4") (d #t) (k 0)) (d (n "iced-x86") (r "^1") (f (quote ("code_asm"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi" "winnt" "minwindef" "errhandlingapi" "handleapi" "memoryapi" "libloaderapi" "synchapi" "winbase"))) (d #t) (k 0)))) (h "0wd144fmnnnyh6ngz4gizd0g73nms3b4f2pvz9srxwsprj8h0j0q")))

