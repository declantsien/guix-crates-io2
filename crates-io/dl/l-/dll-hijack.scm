(define-module (crates-io dl l- dll-hijack) #:use-module (crates-io))

(define-public crate-dll-hijack-1.0.0 (c (n "dll-hijack") (v "1.0.0") (d (list (d (n "dll-hijack-derive") (r "^1.0.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Threading" "Win32_System_Kernel" "Win32_System_WindowsProgramming" "Win32_System_LibraryLoader" "Win32_UI_Shell"))) (d #t) (k 0)))) (h "17b936gdwgbr4qy4pww70s3k9pvwff0qvqlfydp0sx7nzff48w93")))

(define-public crate-dll-hijack-1.0.1 (c (n "dll-hijack") (v "1.0.1") (d (list (d (n "dll-hijack-derive") (r "^1.0.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Threading" "Win32_System_Kernel" "Win32_System_WindowsProgramming" "Win32_System_LibraryLoader" "Win32_UI_Shell"))) (d #t) (k 0)))) (h "0xal9wr0awkzzkvygyvgj94x39p50lax9dyhp7i1xgpmqm7aiwy2")))

