(define-module (crates-io dl -t dl-translate) #:use-module (crates-io))

(define-public crate-dl-translate-0.1.0 (c (n "dl-translate") (v "0.1.0") (d (list (d (n "anyhow") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "dirs") (r ">=3.0.0, <4.0.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.0, <0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r ">=0.5.0, <0.6.0") (d #t) (k 0)))) (h "0309zl1fjcpzjjjk168nan90jc0yzs05b94l7kx6z0s9bysw67fx")))

(define-public crate-dl-translate-0.2.0 (c (n "dl-translate") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "multipart" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "08bqp2b0vln9n7ayiflpq2qjnnq30xs9yayigd09skqid4hnjmja")))

