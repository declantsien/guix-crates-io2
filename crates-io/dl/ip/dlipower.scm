(define-module (crates-io dl ip dlipower) #:use-module (crates-io))

(define-public crate-dlipower-0.1.0 (c (n "dlipower") (v "0.1.0") (d (list (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "scraper") (r "^0.18") (d #t) (k 0)))) (h "0makhxg92620kwggmdgjagv9m2fmql9lc1c8mvslk24d38x0abbf")))

