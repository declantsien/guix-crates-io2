(define-module (crates-io dl pa dlpark) #:use-module (crates-io))

(define-public crate-dlpark-0.1.0 (c (n "dlpark") (v "0.1.0") (d (list (d (n "pyo3") (r "^0.18.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "1g282x5m1a25d6wnvj9czl4gr0dfy0vyvfa9v0whqvy7i2yr67cn") (f (quote (("default" "python")))) (s 2) (e (quote (("python" "dep:pyo3"))))))

(define-public crate-dlpark-0.2.0 (c (n "dlpark") (v "0.2.0") (d (list (d (n "pyo3") (r "^0.18.3") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)))) (h "1csi6ixs80ajdh2g07jixvs9dzbrfi2xpnpn8gp774drxm1dd2ja") (f (quote (("default" "pyo3")))) (s 2) (e (quote (("pyo3" "dep:pyo3"))))))

(define-public crate-dlpark-0.2.1 (c (n "dlpark") (v "0.2.1") (d (list (d (n "pyo3") (r "^0.18.3") (o #t) (d #t) (k 0)))) (h "1ni5qldsd56sjn61ay6azrb47bffjysj8jr3sprf2vnxkppyrc9a") (f (quote (("default" "pyo3")))) (s 2) (e (quote (("pyo3" "dep:pyo3"))))))

(define-public crate-dlpark-0.2.2 (c (n "dlpark") (v "0.2.2") (d (list (d (n "pyo3") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1vgdwwzg9m06dy7hwiy3dcxpd4xyjmk6mfw3hr1ydg4900yibs3b") (f (quote (("default" "pyo3")))) (s 2) (e (quote (("pyo3" "dep:pyo3"))))))

(define-public crate-dlpark-0.3.0 (c (n "dlpark") (v "0.3.0") (d (list (d (n "half") (r "^2.3.1") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "0y4arddwb80rbpy661486mh215bxgfnf4jbrvxlx5ai4vkhba985") (f (quote (("default")))) (s 2) (e (quote (("pyo3" "dep:pyo3") ("half" "dep:half"))))))

(define-public crate-dlpark-0.4.0 (c (n "dlpark") (v "0.4.0") (d (list (d (n "half") (r "^2.3") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.19") (o #t) (d #t) (k 0)))) (h "1n1bh4c5r2d9k3zcn3k96s3mk4kwlq11qqspzznp66maj1sm6hgr") (f (quote (("default")))) (s 2) (e (quote (("pyo3" "dep:pyo3") ("half" "dep:half"))))))

(define-public crate-dlpark-0.4.1 (c (n "dlpark") (v "0.4.1") (d (list (d (n "half") (r "^2.3") (o #t) (d #t) (k 0)) (d (n "pyo3") (r "^0.21") (o #t) (d #t) (k 0)))) (h "130ws4y4wv96l7i1v55zs81pijf375y190qn85wwpr9csbapb4k4") (f (quote (("default")))) (s 2) (e (quote (("pyo3" "dep:pyo3") ("half" "dep:half"))))))

