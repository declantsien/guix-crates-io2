(define-module (crates-io dl pa dlpackrs) #:use-module (crates-io))

(define-public crate-dlpackrs-0.1.0 (c (n "dlpackrs") (v "0.1.0") (d (list (d (n "dlpack-sys") (r "=0.1.0") (d #t) (k 0)) (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0z4z83xnmdj39p474q43wnszgcwz6x0ics2dqsav8qnkprsm8gh6") (y #t) (r "1.57.0")))

(define-public crate-dlpackrs-0.2.0 (c (n "dlpackrs") (v "0.2.0") (d (list (d (n "dlpack-sys") (r "=0.1.1") (d #t) (k 0)) (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "pin-project") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jp6017mcbyhlgk1jhwxvm4009wkhsqja325w94p3hakic49fzyw") (r "1.57.0")))

