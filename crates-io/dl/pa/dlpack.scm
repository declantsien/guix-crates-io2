(define-module (crates-io dl pa dlpack) #:use-module (crates-io))

(define-public crate-dlpack-0.1.0 (c (n "dlpack") (v "0.1.0") (h "1ini3jsn0wn71drizayyl8apygr81yn3s0grdyfq6yz54nmkibiz")))

(define-public crate-dlpack-0.1.1 (c (n "dlpack") (v "0.1.1") (h "1p2c1b7ygxygwlfg0yawyb1s3pgqykz0wgjib3m5vzmnk8vbmvai")))

(define-public crate-dlpack-0.1.2 (c (n "dlpack") (v "0.1.2") (h "0s16hj53mvla0fm2zv0is4fh1g3mhkhbbqaddh310yyh7flj8l9q")))

(define-public crate-dlpack-0.1.3 (c (n "dlpack") (v "0.1.3") (h "1id1aylmpkmm04p6xxy1c3w6dish7jsk7aba4vd27ysc2kwbbbrv")))

(define-public crate-dlpack-0.2.0 (c (n "dlpack") (v "0.2.0") (h "19ihf97w6wf6vajs3cx96d5hrsjpghqk7xfsgjawakmjvzpw3grm")))

