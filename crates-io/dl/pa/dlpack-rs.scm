(define-module (crates-io dl pa dlpack-rs) #:use-module (crates-io))

(define-public crate-dlpack-rs-0.1.0 (c (n "dlpack-rs") (v "0.1.0") (d (list (d (n "bindgen") (r ">=0.60.1") (d #t) (k 1)))) (h "00w0m3pisb6mw5520lq57h1w6xkq6153c07b0yrir79imqz5hm98")))

(define-public crate-dlpack-rs-0.1.1 (c (n "dlpack-rs") (v "0.1.1") (d (list (d (n "bindgen") (r ">=0.60.1") (d #t) (k 1)))) (h "1vgp27lx2jp1mlqydavh1mzhxa70swcmc8l0sfsppm7k3bcnnpyf")))

(define-public crate-dlpack-rs-0.1.2 (c (n "dlpack-rs") (v "0.1.2") (d (list (d (n "bindgen") (r ">=0.60.1") (d #t) (k 1)))) (h "16n76rdw0ha7j7aragdbj7ikp3nkzff2b5hbrqvanc9bk9ic5ass")))

