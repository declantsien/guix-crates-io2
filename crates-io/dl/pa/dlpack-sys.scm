(define-module (crates-io dl pa dlpack-sys) #:use-module (crates-io))

(define-public crate-dlpack-sys-0.1.0 (c (n "dlpack-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)))) (h "0cgyh0dxp99pl3wnciq0i260lvnsn0pap7q82df1mdhh09fwvh1y")))

(define-public crate-dlpack-sys-0.1.1 (c (n "dlpack-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)))) (h "1gs5b9yill885rjd48g3lfpf3vbdgrxa34kfyd5p2kp8y3ibip2y")))

