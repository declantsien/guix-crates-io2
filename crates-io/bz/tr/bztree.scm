(define-module (crates-io bz tr bztree) #:use-module (crates-io))

(define-public crate-bztree-0.1.0 (c (n "bztree") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.4") (d #t) (k 2)) (d (n "jemallocator") (r "^0.3.2") (d #t) (k 2)) (d (n "mwcas") (r "^0.1.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "17pcik011r05zdrfg4z1y9sl3vlwcf5hmkf9sdgsnvm1pprdwj3a")))

(define-public crate-bztree-0.2.0 (c (n "bztree") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.9.4") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.4") (d #t) (k 2)) (d (n "jemallocator") (r "^0.3.2") (d #t) (k 2)) (d (n "mwcas") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "06ghjs9rsfn6a1qybjii9jya4ijcs3b194z8wy162mhy9kyp968j")))

