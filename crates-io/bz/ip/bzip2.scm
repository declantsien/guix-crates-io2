(define-module (crates-io bz ip bzip2) #:use-module (crates-io))

(define-public crate-bzip2-0.0.1 (c (n "bzip2") (v "0.0.1") (d (list (d (n "bzip2-sys") (r "*") (d #t) (k 0)))) (h "0fm23jgvfv6f27s82566017r02pgrndk916iq5p4vzdl5vi457zq")))

(define-public crate-bzip2-0.0.2 (c (n "bzip2") (v "0.0.2") (d (list (d (n "bzip2-sys") (r "*") (d #t) (k 0)))) (h "1hx57dvrx98jkmhc9zv8fzkvkg26vq1f95s039lb9fxablp2al3c")))

(define-public crate-bzip2-0.0.3 (c (n "bzip2") (v "0.0.3") (d (list (d (n "bzip2-sys") (r "*") (d #t) (k 0)))) (h "0d8jhik4v9ixkyd12l39zpm55407a2xn463wa4aklxdjjcr8yhz4")))

(define-public crate-bzip2-0.1.0 (c (n "bzip2") (v "0.1.0") (d (list (d (n "bzip2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0pwp2fdzk6nbnn36qj8cf6c66srjg7qi5yd0a0dkx8ghahszc58c")))

(define-public crate-bzip2-0.1.1 (c (n "bzip2") (v "0.1.1") (d (list (d (n "bzip2-sys") (r "^0.0.1") (d #t) (k 0)))) (h "12h7dsx88cdhdhil4ipm6xips4xyfvjaqvh1c6bn80q3wq284b8p")))

(define-public crate-bzip2-0.1.2 (c (n "bzip2") (v "0.1.2") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)))) (h "1pm78sb4b96qj324fghxf8wihp77fr7vr5vjyinanprs0wnw9m7m")))

(define-public crate-bzip2-0.1.3 (c (n "bzip2") (v "0.1.3") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)))) (h "1crydhgj2xh7i0nfy6y4jb88yaxf7xq15mqzr38frsav5z6p3d09")))

(define-public crate-bzip2-0.2.0 (c (n "bzip2") (v "0.2.0") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1fi8020si2dcyvd42a2ncj96mm87c7zrz582xl5dgjm3k44ypa1s")))

(define-public crate-bzip2-0.2.1 (c (n "bzip2") (v "0.2.1") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1i1i47zsc4j1w3kqy9ibf5yp7z66hkb3gscrh53yklz4g9bibyij")))

(define-public crate-bzip2-0.2.2 (c (n "bzip2") (v "0.2.2") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1sqb989zvsb9639z3m8cfr47rv5gjlr1ap9rl2kyxvbw1l6dnnfh")))

(define-public crate-bzip2-0.2.3 (c (n "bzip2") (v "0.2.3") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1pmbxca0bgkvkjgdnziz22szq7gffxjs7wkmg3r9xi1g810dk614")))

(define-public crate-bzip2-0.2.4 (c (n "bzip2") (v "0.2.4") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0sz56k5j307i056zp2cik59kqkv5y5ipvdwwdc0m0fpppxs050yr")))

(define-public crate-bzip2-0.3.0 (c (n "bzip2") (v "0.3.0") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0lva3h8sfa4bp0ajn0q51zksfiwdz8x09ah1c7p4bbrx75waac3w")))

(define-public crate-bzip2-0.3.1 (c (n "bzip2") (v "0.3.1") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "1lis2zlxmwmksnjr09dh5za6z3pkbm68jsil81r58yshzzy73773")))

(define-public crate-bzip2-0.3.2 (c (n "bzip2") (v "0.3.2") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "095yi13ajgnbcmza9s4c7fspzzlqa0bk3hdiwryq43afqi1grsn3") (f (quote (("tokio" "tokio-io" "futures"))))))

(define-public crate-bzip2-0.3.3 (c (n "bzip2") (v "0.3.3") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "partial-io") (r "^0.2.1") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0fvfwanp42j1zpig880jhb5mc0na50bijmwd6211p77sy35w7ds2") (f (quote (("tokio" "tokio-io" "futures"))))))

(define-public crate-bzip2-0.4.1 (c (n "bzip2") (v "0.4.1") (d (list (d (n "bzip2-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "partial-io") (r "^0.3") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck6") (r "^0.6") (d #t) (k 2) (p "quickcheck")) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1gpwm7qj8adi0zffm8r17vkv6f98d1q9glvpjk28v0wb6kz88p97") (f (quote (("tokio" "tokio-io" "futures"))))))

(define-public crate-bzip2-0.4.2 (c (n "bzip2") (v "0.4.2") (d (list (d (n "bzip2-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "partial-io") (r "^0.3") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck6") (r "^0.6") (d #t) (k 2) (p "quickcheck")) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1a23mf77gzww5mqdij42508cz78lws9qs9fgbxsdzm8mi8n03y5b") (f (quote (("tokio" "tokio-io" "futures"))))))

(define-public crate-bzip2-0.4.3 (c (n "bzip2") (v "0.4.3") (d (list (d (n "bzip2-sys") (r "^0.1.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "partial-io") (r "^0.3") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck6") (r "^0.6") (d #t) (k 2) (p "quickcheck")) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1c495c2zh3knxwby2v1m7b21qddvrkya4mvyqlbm197knn0dkz3a") (f (quote (("tokio" "tokio-io" "futures") ("static" "bzip2-sys/static"))))))

(define-public crate-bzip2-0.4.4 (c (n "bzip2") (v "0.4.4") (d (list (d (n "bzip2-sys") (r "^0.1.11") (d #t) (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "partial-io") (r "^0.3") (f (quote ("quickcheck"))) (d #t) (k 2)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)) (d (n "quickcheck6") (r "^0.6") (d #t) (k 2) (p "quickcheck")) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1y27wgqkx3k2jmh4k26vra2kqjq1qc1asww8hac3cv1zxyk1dcdx") (f (quote (("tokio" "tokio-io" "futures") ("static" "bzip2-sys/static"))))))

