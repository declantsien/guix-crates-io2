(define-module (crates-io bz ip bzip2-sys) #:use-module (crates-io))

(define-public crate-bzip2-sys-0.0.1 (c (n "bzip2-sys") (v "0.0.1") (d (list (d (n "gcc") (r "*") (d #t) (k 0)))) (h "1vd03bapyp499dh3w3ldp5psixfi3n7is4d3gq8m0d8v0xil0hzv")))

(define-public crate-bzip2-sys-0.1.0 (c (n "bzip2-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1h5sjqdrwfgprwhpygpl3in378fpbngfbldp5fh6vhg07h9yy9lk")))

(define-public crate-bzip2-sys-0.1.1 (c (n "bzip2-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1jwb9ayayslxrrvd405cxg46991gsz2ckgk78py80r12p9nznf1s")))

(define-public crate-bzip2-sys-0.1.2 (c (n "bzip2-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0c0xm2bk6g2mm7jvsr60mhakx05606vf5db8j9g3vj92hpkvbl0z")))

(define-public crate-bzip2-sys-0.1.3 (c (n "bzip2-sys") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0w8159p14snwnz1ympn7sppkvinn8h8v6d5lcli117l2ad9afqyi")))

(define-public crate-bzip2-sys-0.1.4 (c (n "bzip2-sys") (v "0.1.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04d35lxjwdrpbwik904f3bf87vqzq54ns4jkvqj2b1jxps31bjc1")))

(define-public crate-bzip2-sys-0.1.5 (c (n "bzip2-sys") (v "0.1.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0c7wxnmd52fliy307xrr0hkh93s2wd4dzfv4yh8h1sflhkzkzklq")))

(define-public crate-bzip2-sys-0.1.6 (c (n "bzip2-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16z7ka26vc95gj7cqkdbslrky2hjlvm4gs7dj1vb3fwr85h64l9c")))

(define-public crate-bzip2-sys-0.1.7 (c (n "bzip2-sys") (v "0.1.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pz2mdhkk8yphiqdh2kghdxb60kqyd10lfrjym3r4k5dylvam135") (l "bzip2")))

(define-public crate-bzip2-sys-0.1.8+1.0.8 (c (n "bzip2-sys") (v "0.1.8+1.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f1hv0b6ig30jxgc8khic0k5m6qh7l9n9b1p77lz0kq3qm0mnc05") (l "bzip2")))

(define-public crate-bzip2-sys-0.1.9+1.0.8 (c (n "bzip2-sys") (v "0.1.9+1.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0pi8lxzb1104q9cpvv1jgnk909cggqh2zcdhywqwlbq6c2i3jfxd") (l "bzip2")))

(define-public crate-bzip2-sys-0.1.10+1.0.8 (c (n "bzip2-sys") (v "0.1.10+1.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0v3c44s384n1740vyizwyrv4a215xg1z75vawg2ca8faq4d3vyhp") (l "bzip2")))

(define-public crate-bzip2-sys-0.1.11+1.0.8 (c (n "bzip2-sys") (v "0.1.11+1.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1p2crnv8d8gpz5c2vlvzl0j55i3yqg5bi0kwsl1531x77xgraskk") (f (quote (("static")))) (l "bzip2")))

