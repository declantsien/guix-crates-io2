(define-module (crates-io bz fq bzfquery) #:use-module (crates-io))

(define-public crate-bzfquery-0.1.0 (c (n "bzfquery") (v "0.1.0") (h "0l6z2rimpkf38ccbffipp0p40lxa2gikn0s6vxv33ybsjm7ykzc7") (f (quote (("default" "color") ("color"))))))

(define-public crate-bzfquery-1.0.0 (c (n "bzfquery") (v "1.0.0") (h "1qgkcqdznzk2pm15pgiysnl3pnn7q81aw097wf6c8x9vlffqhdkg") (f (quote (("default" "color") ("color"))))))

(define-public crate-bzfquery-1.0.1 (c (n "bzfquery") (v "1.0.1") (h "1hwh9v1gk53a9vflhdqc8k5m79qc70ld1ha3aa2v5m3bv244d60l") (f (quote (("default" "color") ("color"))))))

