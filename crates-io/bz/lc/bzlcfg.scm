(define-module (crates-io bz lc bzlcfg) #:use-module (crates-io))

(define-public crate-bzlcfg-0.1.0 (c (n "bzlcfg") (v "0.1.0") (d (list (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "recap") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "15bvh4yqzc2drgspwa2ybp81ji1ri8grjckkmp6c4azlzj03phiy")))

