(define-module (crates-io x2 #{64}# x264) #:use-module (crates-io))

(define-public crate-x264-0.1.0 (c (n "x264") (v "0.1.0") (d (list (d (n "x264-sys") (r "^0.1") (d #t) (k 0)))) (h "0cmys8agmpmxaldgqy9sjrp6x9dq7m6yj8dmda8gnar0icvwdjyk")))

(define-public crate-x264-0.1.1 (c (n "x264") (v "0.1.1") (d (list (d (n "x264-sys") (r "^0.1") (d #t) (k 0)))) (h "1zfpds7dxsp81h6mpc6nip3lm0vb7kv3xlhpff6zrr9a9ikpcs0i")))

(define-public crate-x264-0.2.0 (c (n "x264") (v "0.2.0") (d (list (d (n "x264-sys") (r "^0.1") (d #t) (k 0)))) (h "1vz68z0l2fm925wcsdasf037vnvgahnghi9dbj7z4h0glnmnbclp")))

(define-public crate-x264-0.2.1 (c (n "x264") (v "0.2.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "x264-sys") (r "^0.1") (d #t) (k 0)))) (h "1zvm551p9vbx1r3xy5myy0hbascy7lwly463h8zmwwydmd61jcps")))

(define-public crate-x264-0.2.2 (c (n "x264") (v "0.2.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "x264-sys") (r "^0.1") (d #t) (k 0)))) (h "0pk9pli3h7hj2c1bggbxd0idnzvzi5kq6ih3s7fncc83hb74d8v2")))

(define-public crate-x264-0.3.0 (c (n "x264") (v "0.3.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "x264-sys") (r "^0.1") (d #t) (k 0)))) (h "1q5rxks6vfrizkj729agsv45m03rxj4lg8k4yvcpsm2q9csrlrc9")))

(define-public crate-x264-0.4.0 (c (n "x264") (v "0.4.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "x264-sys") (r "^0.2") (d #t) (k 0)))) (h "0br149fxs2j31shczpjk58x2r0h3vzw560659x5j6yfsk5cmvbm8")))

(define-public crate-x264-0.5.0 (c (n "x264") (v "0.5.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "x264-sys") (r "^0.2") (d #t) (k 0)))) (h "14pc1pnhcrhy17qwzgwrda5ffzz1j6r3zdq2bz8cxgnfb39gz4xw")))

