(define-module (crates-io x2 #{64}# x264-dev) #:use-module (crates-io))

(define-public crate-x264-dev-0.1.0 (c (n "x264-dev") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "1ds041kliyi6xm1r9ka4a9h1g99h8m7b7di1akw8niwkw328dyak")))

(define-public crate-x264-dev-0.1.1 (c (n "x264-dev") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "08am104hlpdf76s1qww23mfns9azm5qdkc33npr7r1avda6mv0n4")))

(define-public crate-x264-dev-0.2.0 (c (n "x264-dev") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "flate2") (r "^1.0.12") (d #t) (k 1)) (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 1)) (d (n "tar") (r "^0.4.26") (d #t) (k 1)))) (h "0jqjrgavhilwzcv18vh2g98hpyx7lb8yb495ypc64k6cs8bx32g8") (l "x264")))

