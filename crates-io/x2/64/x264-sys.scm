(define-module (crates-io x2 #{64}# x264-sys) #:use-module (crates-io))

(define-public crate-x264-sys-0.1.0 (c (n "x264-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.23.1") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "1fnr0r6hy0yy7gd82zzn09vgk9x1cn9skch0d4cqpim5rf9iz40m")))

(define-public crate-x264-sys-0.2.0 (c (n "x264-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "1v3kndi94sw9c6zjfwrfzfww4nm4zrlfnc13lcb3468y232zkn2y")))

(define-public crate-x264-sys-0.2.1 (c (n "x264-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "system-deps") (r "^6.1.0") (d #t) (k 1)))) (h "0pj8qqyp59cb85imsdyh934csd5zxp2s1j15j7p2rwqx9pnzaxqj")))

