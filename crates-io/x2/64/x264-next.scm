(define-module (crates-io x2 #{64}# x264-next) #:use-module (crates-io))

(define-public crate-x264-next-0.4.1 (c (n "x264-next") (v "0.4.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "x264-sys") (r "^0.2") (d #t) (k 0)))) (h "1bcgh9bx295aib13y6yjj3fyc97x97gwqsl7c4znkyhhjsa7xfil")))

(define-public crate-x264-next-0.4.2 (c (n "x264-next") (v "0.4.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "x264-sys") (r "^0.2") (d #t) (k 0)))) (h "1ak0329b079aw1kzpzy3hsf2arnj9gm2q5gxwzahibir65j9gpwx")))

