(define-module (crates-io x2 ap x2apic) #:use-module (crates-io))

(define-public crate-x2apic-0.1.0 (c (n "x2apic") (v "0.1.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.5") (d #t) (k 0)) (d (n "raw-cpuid") (r "^6.1.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.7.1") (d #t) (k 0)))) (h "17kh7pqj315bq5f4yfcc35nssqxk5dd7c98zbq0j19460p8bnmn2")))

(define-public crate-x2apic-0.1.1 (c (n "x2apic") (v "0.1.1") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "raw-cpuid") (r "^8.1") (d #t) (k 0)) (d (n "x86_64") (r "^0.11") (d #t) (k 0)))) (h "09syqqwsv08ggxk1z1qy875ax1yw6i5wvrk1swz2m8aiwdr9vic5")))

(define-public crate-x2apic-0.1.2 (c (n "x2apic") (v "0.1.2") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^9.0.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.13") (d #t) (k 0)))) (h "1bppcd5afh1l2ig7ij25mq3a85a10g7l1mzi3rgb2c8b93zblqya")))

(define-public crate-x2apic-0.1.3 (c (n "x2apic") (v "0.1.3") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^9.0.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14") (d #t) (k 0)))) (h "1a9cnpxfmnm1k7c87xa82nfz6kanw73yfras3vl3fpwcjmjqbqr7")))

(define-public crate-x2apic-0.2.0 (c (n "x2apic") (v "0.2.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^9.0.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14") (d #t) (k 0)))) (h "0g23qnj50k5lfkz4misi8zalcva73wx86iryxh2cgzfr0dpmb531")))

(define-public crate-x2apic-0.3.0 (c (n "x2apic") (v "0.3.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "raw-cpuid") (r "^9.0.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14") (d #t) (k 0)))) (h "0q9d8ggq4ln30351006iqhccx2xbfjz17zwdw8gjjfpz04zy5rqm")))

(define-public crate-x2apic-0.4.0 (c (n "x2apic") (v "0.4.0") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10.2.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.6") (d #t) (k 0)))) (h "1z2fqzr8l67iqg9yvbhbkmgawwbs2lbfxnyfa3hv68kid2mcsnyw")))

(define-public crate-x2apic-0.4.1 (c (n "x2apic") (v "0.4.1") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10.2.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.6") (d #t) (k 0)))) (h "0blixs5rfvsjnhkhqqkz8gqhpr09rdnxxqal95hiviyg63ha7dij")))

(define-public crate-x2apic-0.4.2 (c (n "x2apic") (v "0.4.2") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10.2.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.6") (d #t) (k 0)))) (h "1cqd9qqa3vgkqidrs5z24bnsg6h3yrq6877glkkhrbnignsm4wal")))

(define-public crate-x2apic-0.4.3 (c (n "x2apic") (v "0.4.3") (d (list (d (n "bit") (r "^0.1.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "raw-cpuid") (r "^10.2.0") (d #t) (k 0)) (d (n "x86_64") (r "^0.14.6") (d #t) (k 0)))) (h "00j68a6mamh0v4x75bqjnnqp9w2glr4wi8r4zhfyzf6b84jmikfb")))

