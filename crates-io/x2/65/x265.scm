(define-module (crates-io x2 #{65}# x265) #:use-module (crates-io))

(define-public crate-x265-0.1.0 (c (n "x265") (v "0.1.0") (d (list (d (n "x265-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1g4px4bbjn6vf9m062xadrpnky3w5swch61clhxndmmwpw86r7kh") (y #t)))

(define-public crate-x265-0.1.1 (c (n "x265") (v "0.1.1") (d (list (d (n "x265-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0whgyr62ji2ihrfxbhw6sjki8cm7hlwjyn9wg5b4gjyanrl59abp") (y #t)))

