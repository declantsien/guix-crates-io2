(define-module (crates-io hn sp hnspool-stratum-types) #:use-module (crates-io))

(define-public crate-hnspool-stratum-types-0.2.0 (c (n "hnspool-stratum-types") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "stratum-types") (r "^0.6.0") (d #t) (k 0)))) (h "090b7bi4613n75dk5cifr7594acd1637d7pasqjsl33818nsv4ab")))

(define-public crate-hnspool-stratum-types-0.3.0 (c (n "hnspool-stratum-types") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "stratum-types") (r "^0.7.0") (d #t) (k 0)))) (h "0hz382q0amhqvd0n9kkzhkz0kmmlk5iqbl2wr66k1v92gbn1f2h9")))

(define-public crate-hnspool-stratum-types-0.4.0 (c (n "hnspool-stratum-types") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_tuple") (r "^0.4.0") (d #t) (k 0)) (d (n "stratum-types") (r "^0.9.4") (d #t) (k 0)))) (h "05g6jv67ln9v6ra7yx5fyc5km1ix0bdl9q69hh1v0p3bj2q9ls94")))

