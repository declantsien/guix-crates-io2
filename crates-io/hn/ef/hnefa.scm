(define-module (crates-io hn ef hnefa) #:use-module (crates-io))

(define-public crate-hnefa-0.1.0 (c (n "hnefa") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0v9f75ywqj5ijiifwg7pcdcv3l43m38b9wjjpnjrmkajzcs9b899") (y #t)))

(define-public crate-hnefa-0.2.0 (c (n "hnefa") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("v4" "serde"))) (d #t) (k 0)))) (h "0xhldk0wsy9j0fq0c36jh0i3b3rgwfarp7jc2pa172cgnl429960")))

