(define-module (crates-io hn ls hnls) #:use-module (crates-io))

(define-public crate-hnls-0.1.0 (c (n "hnls") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.28.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1izc0qvfb4fa07h5ksdvg8w6l5538hyh8w7nik8l1l3k8yg4f199")))

