(define-module (crates-io hn ew hnews) #:use-module (crates-io))

(define-public crate-hnews-0.1.0 (c (n "hnews") (v "0.1.0") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "1hlv4wchi4dmlkjrv883h7b8afn08gb2f9kz1arj0q4r267xg3mj")))

(define-public crate-hnews-0.2.0 (c (n "hnews") (v "0.2.0") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "1li5fnyy0ln2mbi412c3rpw5a6v40iymngkaxhfhgxkhg83hl48w")))

(define-public crate-hnews-0.2.1 (c (n "hnews") (v "0.2.1") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "142l32ri1bgzl7ggmngdj6h7wlc6391nq2bdpsblgr5y2pyjqg6b")))

(define-public crate-hnews-0.2.2 (c (n "hnews") (v "0.2.2") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "0hn1ggxyrnk8z7dbnizch1cfyp9fjld1zsz7583zh11hdsqzi922")))

(define-public crate-hnews-0.3.0 (c (n "hnews") (v "0.3.0") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "1s7s0nsd8vb2pn8zwg31sp2a311r5mk0k94kj1vfydhc81azvc0j")))

(define-public crate-hnews-0.3.1 (c (n "hnews") (v "0.3.1") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "071qg4j1rzrn6naq9bf1x5syrhxhq2p803aagla4faxb3f9lpz00")))

(define-public crate-hnews-0.3.2 (c (n "hnews") (v "0.3.2") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "1zg9lc6d7wp48d2hbyhamd0kd3fb5650w4ag97mp8p8bdq745ljv")))

(define-public crate-hnews-0.3.3 (c (n "hnews") (v "0.3.3") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "1iw2qx1salys9srakqsh40a9c9ldny5f3f3iqw8p62799bykg7ah")))

(define-public crate-hnews-0.3.4 (c (n "hnews") (v "0.3.4") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "15syxj3hyy7p7g1sxkjp6bj46l2bxcad5znv266gzhzi072k07bh")))

(define-public crate-hnews-0.3.5 (c (n "hnews") (v "0.3.5") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "14vy0mbfk46pdy3af0955bzvc94nwahigzwg34xm6kp7hn8221x6")))

(define-public crate-hnews-0.4.0 (c (n "hnews") (v "0.4.0") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "1q3bqajh0p6717grrr0afgc3jlx91w88w1wml5zab9bpg6i7rv88")))

(define-public crate-hnews-0.4.1 (c (n "hnews") (v "0.4.1") (d (list (d (n "arc-swap") (r "^1.2") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)))) (h "1bka6q4xw3xzs61x97q2dg8mwq5p1nnncky1b69zp9kf0p5n1805")))

