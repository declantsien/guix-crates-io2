(define-module (crates-io wd k- wdk-macros) #:use-module (crates-io))

(define-public crate-wdk-macros-0.0.0 (c (n "wdk-macros") (v "0.0.0") (h "09wwys9w66dkcgpi274mgm3a2vz1skx6m78bns3zs22vm9x5s6z0")))

(define-public crate-wdk-macros-0.1.0 (c (n "wdk-macros") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.84") (d #t) (k 2)))) (h "04iv9fxfclm3ys193c010m9sk3vrjir33bnqis48mnzdss1qsk25") (f (quote (("nightly") ("default"))))))

(define-public crate-wdk-macros-0.2.0 (c (n "wdk-macros") (v "0.2.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "owo-colors") (r "^4.0.0") (d #t) (k 2)) (d (n "paste") (r "^1.0.14") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.89") (d #t) (k 2)))) (h "1nmj8420ivkc13avm5dcplksbzd25sjq513pibfaqaszwi1pg8k5") (f (quote (("nightly") ("default"))))))

