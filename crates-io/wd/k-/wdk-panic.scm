(define-module (crates-io wd k- wdk-panic) #:use-module (crates-io))

(define-public crate-wdk-panic-0.0.0 (c (n "wdk-panic") (v "0.0.0") (h "1g0h7sin73dk22nq4gda8s7hc77amsvsp25cisww5a178y6a3hl1")))

(define-public crate-wdk-panic-0.1.0 (c (n "wdk-panic") (v "0.1.0") (h "10xz76wiamqpq9mb0bz73xjfvqpnfc9vqkhf9gicmppcq3ax0q83")))

(define-public crate-wdk-panic-0.2.0 (c (n "wdk-panic") (v "0.2.0") (h "1r45hpjbc6bxi31zzz14znippnv55rshkd5jlfd0dmy0fa4mamkg")))

