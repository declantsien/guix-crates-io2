(define-module (crates-io wd k- wdk-sys) #:use-module (crates-io))

(define-public crate-wdk-sys-0.0.0 (c (n "wdk-sys") (v "0.0.0") (h "1knglf8padwv6s2j9kkcwdbrnkn531gr06l5rhizzl30zgiy0s7p")))

(define-public crate-wdk-sys-0.1.0 (c (n "wdk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 1)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 1)) (d (n "wdk-build") (r "^0.1.0") (d #t) (k 1)) (d (n "wdk-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0lznzhzkgca57kygyv2g0x3xq6rplz1iip6s52kaskiba2rhdwfj") (f (quote (("test-stubs") ("nightly" "wdk-macros/nightly") ("default")))) (l "wdk")))

(define-public crate-wdk-sys-0.2.0 (c (n "wdk-sys") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "rustversion") (r "^1.0.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 1)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 1)) (d (n "wdk-build") (r "^0.2.0") (d #t) (k 1)) (d (n "wdk-macros") (r "^0.2.0") (d #t) (k 0)))) (h "11qh827khj11f7qvk9kaccwaqb7f47rwy5mvxdj52lx0cv3gpiin") (f (quote (("test-stubs") ("nightly" "wdk-macros/nightly") ("default")))) (l "wdk")))

