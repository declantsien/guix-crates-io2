(define-module (crates-io wd g- wdg-converter) #:use-module (crates-io))

(define-public crate-wdg-converter-0.1.0 (c (n "wdg-converter") (v "0.1.0") (h "00bp850pg4i7sms8rh9237rbpblr9d8qxlnrkzm4igp7arnnzpmi")))

(define-public crate-wdg-converter-0.1.1 (c (n "wdg-converter") (v "0.1.1") (h "0ixmfdl79kc8k3llwcl018fsc7k8np2fvn0nhi1hnj7gafkpxrnf")))

(define-public crate-wdg-converter-0.1.2 (c (n "wdg-converter") (v "0.1.2") (h "1x5mppnlm4vxjsncpikwhkcwh99ck4n37xfj3zzn1xx6n7sjhazg")))

(define-public crate-wdg-converter-0.1.3 (c (n "wdg-converter") (v "0.1.3") (h "0nznixwagvpsp62f6sf3wfx7vdap0knvpwl0jh48bahvs587qryb")))

(define-public crate-wdg-converter-0.1.4 (c (n "wdg-converter") (v "0.1.4") (h "0ykyhdspmxwvqrm14g7lzj63d6nz1nz671553ypqxd84xzrp5nv9")))

(define-public crate-wdg-converter-0.1.5 (c (n "wdg-converter") (v "0.1.5") (h "1fbqfmjmdyw53bshjkm5v4swn99x43b3nnjdivqc1r6lkxa0f4aw")))

(define-public crate-wdg-converter-0.1.6 (c (n "wdg-converter") (v "0.1.6") (h "0ixgfsv2yh97aqngs2ddrymqa84h9dnwi4ydq8p6inr7v45izp23")))

(define-public crate-wdg-converter-0.1.7 (c (n "wdg-converter") (v "0.1.7") (h "1h0k615vn160bk4wm76jdjgbq96zim58xh398annbx7gcija70y0")))

(define-public crate-wdg-converter-0.1.8 (c (n "wdg-converter") (v "0.1.8") (h "0zb3jjxkmxslilxf56kyicd7yssd9p9n8clwg7330j1zacn17fi4")))

(define-public crate-wdg-converter-0.1.9 (c (n "wdg-converter") (v "0.1.9") (h "0rlnw8vhlip21az09avxa1rlmsnzabxhnb6ly3hssjnb0xracrph")))

(define-public crate-wdg-converter-0.2.0 (c (n "wdg-converter") (v "0.2.0") (h "1nz1vxyf0p7872w9dhlvkpyqy68pyz318391w404p4y2cic8c9nh")))

(define-public crate-wdg-converter-0.2.1 (c (n "wdg-converter") (v "0.2.1") (h "0manbzcvfx5hw2rfvskb47xf42vkw8whnnjqqdvvgzlw4nd1gawj")))

(define-public crate-wdg-converter-0.2.2 (c (n "wdg-converter") (v "0.2.2") (h "0yihp25jw5li82mknrqfspksgy12ndj79l0za7v6g23dvybq6a3m")))

(define-public crate-wdg-converter-0.2.3 (c (n "wdg-converter") (v "0.2.3") (h "0j62vsmhsh0f9cv8hq5jw4zgckprazhpn8yq2mbflcpbl01p7mbx")))

(define-public crate-wdg-converter-0.2.4 (c (n "wdg-converter") (v "0.2.4") (h "0z9kfadg9bm8m7cyn1fgj929rvssix2arm0zdlgyvqmsnrsly9iw")))

(define-public crate-wdg-converter-0.2.5 (c (n "wdg-converter") (v "0.2.5") (h "0mnm8gid8l234dgy9az7dba6k08w19440904m5xvgr88a1vhq327")))

(define-public crate-wdg-converter-0.2.6 (c (n "wdg-converter") (v "0.2.6") (h "1v26m2s2m7rk2cda5waclynq0nb8438f9zsia2955n990szsak0d")))

(define-public crate-wdg-converter-0.2.7 (c (n "wdg-converter") (v "0.2.7") (h "0z0wic6fnpwm8jgpzs4g0amwpqahjzmdajjd1bzv796nva0v51li")))

(define-public crate-wdg-converter-0.2.8 (c (n "wdg-converter") (v "0.2.8") (h "09w5a03ar1hj1c803bwij34aw2wfigjy23pjpiph8ip6ls5zdc0d")))

(define-public crate-wdg-converter-0.2.9 (c (n "wdg-converter") (v "0.2.9") (h "0cl39yl8q3xqk4wplba8fg72f83n0fhyic3kmlas1997jlrixjy6")))

(define-public crate-wdg-converter-0.3.0 (c (n "wdg-converter") (v "0.3.0") (h "0ipinkq1zjzizhilzv8imp49cwv5x9f8b32bc7njz6jf9vdna3fz")))

(define-public crate-wdg-converter-0.3.1 (c (n "wdg-converter") (v "0.3.1") (h "07cx6w09lbrz7cjy322l9f9zx5p321hjk5jfv1p0kxc424c6pdpb")))

(define-public crate-wdg-converter-0.3.2 (c (n "wdg-converter") (v "0.3.2") (h "0vi1dr3y12k7kn955fvlxfncr4ap51bgbsij6p7wjckzljgqja03")))

(define-public crate-wdg-converter-0.3.3 (c (n "wdg-converter") (v "0.3.3") (h "1mzfz2bsa4h19n2ws9c32n5j0b5l7av78mgx0f4hskzlwfni8n0y")))

(define-public crate-wdg-converter-0.3.4 (c (n "wdg-converter") (v "0.3.4") (h "05apv6ycj9lwi2rg186hhpsl11vrfnhlyhpiv5rv71db5b0xywjj")))

(define-public crate-wdg-converter-0.3.5 (c (n "wdg-converter") (v "0.3.5") (h "1hy9clnlg3z5jkvvdh4a3bcfslf048d9x8qnwqpccnx0lyv7qfyw")))

(define-public crate-wdg-converter-0.3.6 (c (n "wdg-converter") (v "0.3.6") (h "1vyzs5dr4mv3b3iw0sikfg26j6gkvqvrd4471gv7kr0pxpvmcypb")))

(define-public crate-wdg-converter-0.3.7 (c (n "wdg-converter") (v "0.3.7") (h "1q5sfdf8mj20cm8qv4jydy2vgf9lqmklbsq3p0mcd0vcc5j4zv8b")))

(define-public crate-wdg-converter-0.3.8 (c (n "wdg-converter") (v "0.3.8") (h "1v9b2hgs6zwf3n7qi9wk8s47wz84h1kfdair1164s766blr9j5ax")))

(define-public crate-wdg-converter-0.4.0 (c (n "wdg-converter") (v "0.4.0") (h "13qfvj3my5sfwd96fskxhw6a7rdan6my9hf2nlgy8gbf7m67q5yy")))

