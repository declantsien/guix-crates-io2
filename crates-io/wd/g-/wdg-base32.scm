(define-module (crates-io wd g- wdg-base32) #:use-module (crates-io))

(define-public crate-wdg-base32-0.1.0 (c (n "wdg-base32") (v "0.1.0") (h "18f4rxr0hg5z99181dp1plp2bqsinnvxqbcngv2ja2n1h7czrygx")))

(define-public crate-wdg-base32-0.1.1 (c (n "wdg-base32") (v "0.1.1") (h "0brj71x1faqd7rw20rafq35pdp1629ryrdksr534zh95c65lrbnx")))

(define-public crate-wdg-base32-0.1.2 (c (n "wdg-base32") (v "0.1.2") (h "0m9hqx9gva5jz0gyzd8j4964k5qvz8pfrn6vyhyxy0f2zrxmr8ms")))

(define-public crate-wdg-base32-0.2.0 (c (n "wdg-base32") (v "0.2.0") (h "1r4wkv69hapwjk44kh1f6ga3j448bsvdy68sysqpjh9y7sk1khfi")))

(define-public crate-wdg-base32-0.2.1 (c (n "wdg-base32") (v "0.2.1") (h "0kjkng8c0imm8f95i822kx3czc59zmk7bwnvdy3l4daf8shfn40g")))

(define-public crate-wdg-base32-0.2.2 (c (n "wdg-base32") (v "0.2.2") (h "08i0vj9mwahk0r7m0alnz0j00yzs43prgdkwinb4jnic9dfqcmza")))

(define-public crate-wdg-base32-0.2.3 (c (n "wdg-base32") (v "0.2.3") (h "19nqnwsdz3rnsmhpimyslb246l8g26a430s6i4l92xnkag3x8z8h")))

(define-public crate-wdg-base32-0.3.0 (c (n "wdg-base32") (v "0.3.0") (h "0mxma1kqyak2i1w9fpl4iwiks45bbpxvsj10yj51dn98x4fr4s6x")))

(define-public crate-wdg-base32-0.3.1 (c (n "wdg-base32") (v "0.3.1") (h "0j54x1f6mfmji84vzdxdjzw3xzvdvqr1a2mvmll19hm16bv42rl3")))

(define-public crate-wdg-base32-0.3.2 (c (n "wdg-base32") (v "0.3.2") (h "1n31g7bpa1hskbb82ns5ds8l69lwq2f9z1k1yivm5ybdc8bkzyzn")))

(define-public crate-wdg-base32-0.3.3 (c (n "wdg-base32") (v "0.3.3") (h "00znnixsvi11ipyqpib146v3ykpz4yjml9blns6kyvfiqwsjmbxc")))

(define-public crate-wdg-base32-0.3.4 (c (n "wdg-base32") (v "0.3.4") (h "10s9sscy7d53jsxk9bkcilw81674igf39gv9i10cpsbfzn6x77sd")))

(define-public crate-wdg-base32-0.3.5 (c (n "wdg-base32") (v "0.3.5") (h "1fqspfxgz9fzpgn28qlhjmv2xjmkpzqksz8azqzb7k0xg47caysb")))

(define-public crate-wdg-base32-0.3.6 (c (n "wdg-base32") (v "0.3.6") (h "0bvq52hnjxwqib7q6x68rls8wqaza9cyfwha1b9024l2wfxzmaf1")))

(define-public crate-wdg-base32-0.3.7 (c (n "wdg-base32") (v "0.3.7") (h "0gybdc8iv6ryzc4am8rav7pcdh4np323fzxpg68wl0ga8xxb624r")))

(define-public crate-wdg-base32-0.3.8 (c (n "wdg-base32") (v "0.3.8") (h "13zjba2w728y8w5knjhswqrg8hg49k2bxjix3ya36lw97j18rfkc")))

(define-public crate-wdg-base32-0.3.9 (c (n "wdg-base32") (v "0.3.9") (h "1calap6nlg28k84ckvid6glcjw2wnxv64g8095iji4gxs5zr4x00")))

(define-public crate-wdg-base32-0.3.10 (c (n "wdg-base32") (v "0.3.10") (h "1gh6n1db4gl37q8d3m3q8qb6isq5lm9dpcdszlawmhc7lmccm53q")))

(define-public crate-wdg-base32-0.3.11 (c (n "wdg-base32") (v "0.3.11") (h "13q2881hm5426nbmk1xl8nhhbk7rjhncl0fl4v6zrzwy0awxmnqw")))

(define-public crate-wdg-base32-0.3.12 (c (n "wdg-base32") (v "0.3.12") (h "03dz7j2b77s9akf8hib04m4i3184amgis34v8x33y1jidm7wfw60")))

(define-public crate-wdg-base32-0.3.13 (c (n "wdg-base32") (v "0.3.13") (h "0a4mmban7f4nfg3wxcg3n128wxxgwlq7as4wdwfc86hv11r2jdn5")))

(define-public crate-wdg-base32-0.3.14 (c (n "wdg-base32") (v "0.3.14") (h "0bynzbwmsjhgxnf9kqpm05j188az83sz0lv1xdr4dxf4a8mfwj3p")))

(define-public crate-wdg-base32-0.3.15 (c (n "wdg-base32") (v "0.3.15") (h "0ln16lr465nrmsldnv11gf9p4918d5c399qzc4g63br8pnc5y1jh")))

(define-public crate-wdg-base32-0.3.16 (c (n "wdg-base32") (v "0.3.16") (h "0lkia56i4jwkc0cw7dw9i4q1rcrd1s9nvvy548lr7z0s4f2ix5db")))

(define-public crate-wdg-base32-0.3.17 (c (n "wdg-base32") (v "0.3.17") (h "1fr4gjp3hv9vcqn34d0jg9ljg2xdcpyxl0swj9mjb5yvnpnd221x")))

(define-public crate-wdg-base32-0.3.18 (c (n "wdg-base32") (v "0.3.18") (h "1p7wakq8grndhp35b9ppcbnq5lz956km7raslh4c858i0lc399xp")))

(define-public crate-wdg-base32-0.3.19 (c (n "wdg-base32") (v "0.3.19") (h "1ihkf2apcii9sr43cbw20zyfa26dw8bg9ynmk4v504rqz31hn2xh")))

(define-public crate-wdg-base32-0.3.20 (c (n "wdg-base32") (v "0.3.20") (h "01vglmr705qfivx53n2kildwlyq3nmir7d1vd7ldpzykp297pyvr")))

(define-public crate-wdg-base32-0.3.21 (c (n "wdg-base32") (v "0.3.21") (h "08m14z4g44jsh3xb7icl3k4ky39s3qhl30bl32rng8aby0mmp7g0")))

(define-public crate-wdg-base32-0.3.22 (c (n "wdg-base32") (v "0.3.22") (h "038jg3yj1gail0p58is3ax4hw2hw5n912wij9nm9vw8frfidnvjk")))

(define-public crate-wdg-base32-0.3.23 (c (n "wdg-base32") (v "0.3.23") (h "1ski0c82i924w183w644cwvlx0x8d2r9y6wgci55iykp61i9prm0")))

(define-public crate-wdg-base32-0.3.24 (c (n "wdg-base32") (v "0.3.24") (h "1vmfmxmiw4cap7b55zf963skcmc7hf9fgdqxfcag82iw5qrvfpz6")))

(define-public crate-wdg-base32-0.3.25 (c (n "wdg-base32") (v "0.3.25") (h "1wgj3pk68a21r5d3hsxljig8l1c8viasf6f7l4pdd5i98qh0a9i6")))

(define-public crate-wdg-base32-0.3.26 (c (n "wdg-base32") (v "0.3.26") (h "060ch8xncrbb21yyrw2502q0zx5zicf6i6azg5ka4wj2xcij3sa9")))

(define-public crate-wdg-base32-0.4.0 (c (n "wdg-base32") (v "0.4.0") (h "1a4li7jgk341rrfzpxmynvj8i27nlv0dij7qwbx0rf44l9jc2xcv")))

(define-public crate-wdg-base32-0.4.1 (c (n "wdg-base32") (v "0.4.1") (h "1nv8kv53ihq91frp0fj304442q20gp5h1j4rwyr7mxkf88mjykhv")))

(define-public crate-wdg-base32-0.4.2 (c (n "wdg-base32") (v "0.4.2") (h "00qscwfsvwrz1br1wjbrwn0ccn8rmcvp4gznpw4iqvqh344gs4b0")))

(define-public crate-wdg-base32-0.5.0 (c (n "wdg-base32") (v "0.5.0") (h "0i4bhqamwnahisisfp464anba90ybnzp7rpf2bjbn3jqxf2ghxl1")))

(define-public crate-wdg-base32-0.5.1 (c (n "wdg-base32") (v "0.5.1") (h "1wkz8qs0dzflbpzq8dnaka0v7rvm1bw487hwb8qhy1pgh1xm12f4")))

(define-public crate-wdg-base32-0.5.2 (c (n "wdg-base32") (v "0.5.2") (h "0w9nhxz9f1c9jss4z2m0clxg33j2nyqa3qjhz79gv0rabn3jd3lr")))

(define-public crate-wdg-base32-0.5.3 (c (n "wdg-base32") (v "0.5.3") (h "0swqhimfklajkrx0kismnx3d3y8lrlfcxs3s7sx2q4xnnwpj6sja")))

(define-public crate-wdg-base32-0.5.4 (c (n "wdg-base32") (v "0.5.4") (h "1j0p4vkznb5igykq1aylcnnp7xrlg749f177d5av1c7fb6cfqd55")))

(define-public crate-wdg-base32-0.5.5 (c (n "wdg-base32") (v "0.5.5") (h "0vmm6411kzl3drbg0px87bz9a461vpjynzq8vc5hb0skmdcnxik3")))

(define-public crate-wdg-base32-0.5.6 (c (n "wdg-base32") (v "0.5.6") (h "0pdk9cn0w6dzwrdlva5mxg5z97dlxy019r827x7km7hfgqgba84n")))

(define-public crate-wdg-base32-0.5.7 (c (n "wdg-base32") (v "0.5.7") (d (list (d (n "wdg-converter") (r "^0.1.3") (d #t) (k 0)))) (h "00kl1w6582ksmz32di8d6ygbjippk4r1q48hrqq4i5r39xwadlps")))

(define-public crate-wdg-base32-0.6.0 (c (n "wdg-base32") (v "0.6.0") (d (list (d (n "wdg-converter") (r "^0.1.3") (d #t) (k 0)))) (h "0b7dphm4mlcmmxparnfj6mvlxgkyyfajjiyv7g6fc8n1335w6cp0")))

(define-public crate-wdg-base32-0.6.1 (c (n "wdg-base32") (v "0.6.1") (d (list (d (n "wdg-converter") (r "^0.1.8") (d #t) (k 0)))) (h "0sr32m6v1m966srgg19j1qi70brkz14npmzrrjibwgj4fnp23ld3")))

