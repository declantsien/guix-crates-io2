(define-module (crates-io wd g- wdg-base64) #:use-module (crates-io))

(define-public crate-wdg-base64-0.1.0 (c (n "wdg-base64") (v "0.1.0") (h "1qwb6zrrwik2lxvzc6iw7ir4qhyg64kmczs5sz3pcq5gi5sdksja")))

(define-public crate-wdg-base64-0.2.0 (c (n "wdg-base64") (v "0.2.0") (h "02scyn93m5qww32d4qsrqr9zz4mbs1091ga21bc6n3avyv7gsmx8")))

(define-public crate-wdg-base64-0.2.1 (c (n "wdg-base64") (v "0.2.1") (h "0lhq4mjdz9mhsjsvwjcm0ffhsq9z30d39rm4nzlpyhd836ifvfmm")))

(define-public crate-wdg-base64-0.2.2 (c (n "wdg-base64") (v "0.2.2") (h "00lk25dw2lcys2m4lxj610qzhin47mk3r108d22mw2g2dsk81qiw")))

(define-public crate-wdg-base64-0.2.3 (c (n "wdg-base64") (v "0.2.3") (h "04fam2rli63wjqr57m5pi4jciwhhjl01fzpmxk9vq75rrkia2nka")))

(define-public crate-wdg-base64-0.2.4 (c (n "wdg-base64") (v "0.2.4") (h "08czf6rqgqccfk7a2mdigv577bsiphhkz0nwar2shn9838jzaxi6")))

(define-public crate-wdg-base64-0.2.5 (c (n "wdg-base64") (v "0.2.5") (h "1ra44n7vmp03mwzk256fv3x5qppy4q9b4vdpk35s0nz1hn91x4v2")))

(define-public crate-wdg-base64-0.2.6 (c (n "wdg-base64") (v "0.2.6") (h "0knpif6x9szc885ii0giafdz49r70y26rhdws48f4ngr88lwnhj3")))

(define-public crate-wdg-base64-0.2.7 (c (n "wdg-base64") (v "0.2.7") (h "1j3n1i4r5rvcbbcqpf4gdrzz6i5ls30hrr52a7vzj3i1bfp4sf8p")))

(define-public crate-wdg-base64-0.2.8 (c (n "wdg-base64") (v "0.2.8") (h "0dcc1d3jmcag9nwwpzly6bigxmqgvq48g461ki05k0f2rpanjsif")))

(define-public crate-wdg-base64-0.2.9 (c (n "wdg-base64") (v "0.2.9") (h "0rgaajwl5i2wkbsp1flbc744zb2pax7f7pq4dwfk95l4qkx9mnws")))

(define-public crate-wdg-base64-0.3.0 (c (n "wdg-base64") (v "0.3.0") (h "0v3ijawxqyz1kz445zm9nim51wf06kml8xqdfii6qhbhz9v6kpvg")))

(define-public crate-wdg-base64-0.3.1 (c (n "wdg-base64") (v "0.3.1") (h "1bpdf01mziyj5lk8s60wir175xx2w304qp72gq095zkn78ag60bf")))

(define-public crate-wdg-base64-0.3.2 (c (n "wdg-base64") (v "0.3.2") (h "0gn8ncp12ryj2xnzsrh276kyxl9w1i9s5gk7a5pjccn8xcmb657z")))

(define-public crate-wdg-base64-0.3.3 (c (n "wdg-base64") (v "0.3.3") (h "0lqwjvn684whls4ymcx1d9g73rfx8z5ym3fbfq0x0i0cb05xx2mc")))

(define-public crate-wdg-base64-0.3.4 (c (n "wdg-base64") (v "0.3.4") (h "0qq0z2ky8aw7srcc5hn0yaj488iw9gdb4025fxdrc5xk9fca7vj0")))

(define-public crate-wdg-base64-0.3.5 (c (n "wdg-base64") (v "0.3.5") (h "1zkdkj9488iqfvjcd2l7wyfwvw5i6cmnwy9dbvmxb6j2szm9d34j")))

(define-public crate-wdg-base64-0.3.6 (c (n "wdg-base64") (v "0.3.6") (h "0qgk3i6c08ijaz381ad0gq47fri3pxd0ib6crh6qp5r1acm8pzbz")))

(define-public crate-wdg-base64-0.3.7 (c (n "wdg-base64") (v "0.3.7") (h "0f4lyvm5wk95bav4a7c19a7l9gd9xh9ypk5n1hlaj2kygkbm8lk5")))

(define-public crate-wdg-base64-0.3.8 (c (n "wdg-base64") (v "0.3.8") (h "0h9i311mji8vc2fjnwc9320sr1pnlchyifaak667ffhjbdyg46ii")))

(define-public crate-wdg-base64-0.3.9 (c (n "wdg-base64") (v "0.3.9") (h "0lmnf9hxmqq86b8mb2cqldxwjc8p0nysw38d9mxvdddvvr37vkhn")))

(define-public crate-wdg-base64-0.3.10 (c (n "wdg-base64") (v "0.3.10") (h "0x47x2vd182fp7wi4d2vj0zrxhyxgrnf3iagbx0bbf1di2r6g87f")))

(define-public crate-wdg-base64-0.3.11 (c (n "wdg-base64") (v "0.3.11") (h "0abi365yygrv3x2nakcilpq26nwvmynmjbf2543g4g3z5449gvwq")))

(define-public crate-wdg-base64-0.3.12 (c (n "wdg-base64") (v "0.3.12") (h "1y648mnp8yvnppxkshksbxs4s55r7mvqqchmjx6y3p0cl1zcv9vh")))

(define-public crate-wdg-base64-0.3.13 (c (n "wdg-base64") (v "0.3.13") (h "0zm8jcdc5jzfsmy4ilc6iwf3b6chs69jzh4qm900hajk5qjcnhaq")))

(define-public crate-wdg-base64-0.3.14 (c (n "wdg-base64") (v "0.3.14") (h "142nrhh670a9qq3frvzw67hkq5d9nljn982lz2fd4blr1r0a7znp")))

(define-public crate-wdg-base64-0.3.15 (c (n "wdg-base64") (v "0.3.15") (h "1lcydxcd77966yn5fsjfkkmj3245f2dih7sdhjbjbfjf40hpv8z5")))

(define-public crate-wdg-base64-0.3.16 (c (n "wdg-base64") (v "0.3.16") (h "19hzdq4smgf2w2pls2hsp8y5gwqx42k2896sh2xq4a9z2bw63wi1")))

(define-public crate-wdg-base64-0.3.18 (c (n "wdg-base64") (v "0.3.18") (h "1z20dgwrk7wf9rp9j46k3b8v6jybxwsp7799ixpsp5asd833pxks")))

(define-public crate-wdg-base64-0.3.19 (c (n "wdg-base64") (v "0.3.19") (h "0cdbcvhcyxm1qvflga1gk6pq2p7vyj47bzxhks7yd99sg1i5lgxy")))

(define-public crate-wdg-base64-0.4.0 (c (n "wdg-base64") (v "0.4.0") (h "11q4psl7v46bh4gk6r383csn1j6xygmb33apb3ijkpk5m4pw7xsp")))

(define-public crate-wdg-base64-0.4.1 (c (n "wdg-base64") (v "0.4.1") (h "12mfkh5bvgyr57xnpdzsv7zbrxdd1ljkg4pap5ipj0clwk0l3m6i")))

(define-public crate-wdg-base64-0.4.2 (c (n "wdg-base64") (v "0.4.2") (h "0ddg16la5lddd7d3xxxwa0sbsb3m075495ji5xb6wpwkq6gh8fil")))

(define-public crate-wdg-base64-0.4.3 (c (n "wdg-base64") (v "0.4.3") (h "020i3sq4rys79wzmldsxwxpkvgs75l5fkdab2wqyifc09z8hjgfi")))

(define-public crate-wdg-base64-0.4.4 (c (n "wdg-base64") (v "0.4.4") (h "16qbm5wavkrhsdhk2g8r224ih8crqvyjzd37x99fx4pdbv61k6pv")))

(define-public crate-wdg-base64-0.4.5 (c (n "wdg-base64") (v "0.4.5") (h "03w9dk2fi207v1sqpkpdrc00i093c6nly3cb5a7ficlrhgkqls1p")))

(define-public crate-wdg-base64-0.4.6 (c (n "wdg-base64") (v "0.4.6") (h "0hj3smm3ipff009h9hal97y8gis2d44hzciq4jr7jnml20bslryf")))

(define-public crate-wdg-base64-0.4.7 (c (n "wdg-base64") (v "0.4.7") (h "1qnw52ylknijhgasq74769n2ra8cz1p53950gn9h1hwmkxvn8nsf")))

