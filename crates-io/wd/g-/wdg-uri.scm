(define-module (crates-io wd g- wdg-uri) #:use-module (crates-io))

(define-public crate-wdg-uri-0.1.0 (c (n "wdg-uri") (v "0.1.0") (h "1sy0h03whw4kp7j8vv26gr95ajr463k0j5dwafca43jh1pxkq0c2")))

(define-public crate-wdg-uri-0.1.1 (c (n "wdg-uri") (v "0.1.1") (h "07shsjzqd3qymgv03f58py50r75kyzs1gbks17n1p17f1k6l5v94")))

(define-public crate-wdg-uri-0.1.2 (c (n "wdg-uri") (v "0.1.2") (h "140dqc8awf4azznpf280glb8g3w4iz18f1qf5ada2qaqj17kvrfn")))

(define-public crate-wdg-uri-0.1.3 (c (n "wdg-uri") (v "0.1.3") (h "0j3ncskj5d6ckf3cvxchzpanyrsp1a0pchq8wvvl484i2843kwnc")))

(define-public crate-wdg-uri-0.1.4 (c (n "wdg-uri") (v "0.1.4") (h "1rcqcd2wnx7w4na60w99d154r69r8f6ms41rib1mr0ycqd0jh9q9")))

(define-public crate-wdg-uri-0.1.5 (c (n "wdg-uri") (v "0.1.5") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "16r32vgh39sn4ji2jgqqnz5akcdmmp9ypqx5h1pzk0r9bhaxvwyk")))

(define-public crate-wdg-uri-0.1.6 (c (n "wdg-uri") (v "0.1.6") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "006w2yr4w6s0b6a856i01ky4wkf7knivby7zb0ahgryahk3nqqhp")))

(define-public crate-wdg-uri-0.1.7 (c (n "wdg-uri") (v "0.1.7") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "15bjp37cnsk8qygxw4mlmf7kyq70d4jhmx9j7y1qkqrw6vkl6vip")))

(define-public crate-wdg-uri-0.1.8 (c (n "wdg-uri") (v "0.1.8") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1hkxbzkrsjmm5am2nn5vmp98bqxzn6lj6w6r51k3qd76y43jvwm1")))

(define-public crate-wdg-uri-0.1.9 (c (n "wdg-uri") (v "0.1.9") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1yb987krlk6wbpd6ym0803zqaaziy8ag9xvixpzra24qrlkgz9sb")))

(define-public crate-wdg-uri-0.1.10 (c (n "wdg-uri") (v "0.1.10") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1ihvavdjz8r1s1niqzficv4j56dksha723g12gzqa0xh819gd1gv")))

(define-public crate-wdg-uri-0.1.11 (c (n "wdg-uri") (v "0.1.11") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1w4avi5k9c2ag8mqmfcsdyvfnvb2bff144302yr4bcxim60pf265")))

(define-public crate-wdg-uri-0.1.12 (c (n "wdg-uri") (v "0.1.12") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "1bkmghdj77rc1lyk3sdsh35367719s82x4flr3brlsmmfrpcqr1l")))

(define-public crate-wdg-uri-0.1.13 (c (n "wdg-uri") (v "0.1.13") (d (list (d (n "regex") (r "^1.3.4") (d #t) (k 0)))) (h "0c4aphcpqcvhz04n2l539x2pch23vjgf2x1igazpjl7hngv3sbw8")))

(define-public crate-wdg-uri-0.1.14 (c (n "wdg-uri") (v "0.1.14") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "0dr6jy6lvvcxjwl44szbypqp7gmirrkp88mm7jff5n8aab13dry7")))

(define-public crate-wdg-uri-0.1.15 (c (n "wdg-uri") (v "0.1.15") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "1bcj3f8glbg5dq8mgz21bhhj2zc0mg832y0m704vm4f3m5klv1fz")))

(define-public crate-wdg-uri-0.1.16 (c (n "wdg-uri") (v "0.1.16") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "10z391x4z0yjwk4zhisrd5x5vvjrva0bjcgz9mh2y7dshx0fz89j")))

(define-public crate-wdg-uri-0.1.17 (c (n "wdg-uri") (v "0.1.17") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "0qcj81iibr3vk5ip2yzv0spg3md4mj5kq3xaxvqxgc99l90qz456")))

(define-public crate-wdg-uri-0.1.18 (c (n "wdg-uri") (v "0.1.18") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "1zci2q1ibwxgbv5rv4ax3qkkambi5dzamz7pn1x1p5b2nfk35cqy")))

(define-public crate-wdg-uri-0.1.19 (c (n "wdg-uri") (v "0.1.19") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "1hcrqvhw6xc8knj47zdll6657j2ki2a0bavhw0bk6970dimdhfwp")))

(define-public crate-wdg-uri-0.1.20 (c (n "wdg-uri") (v "0.1.20") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "1dd9wiwcp3496d1mmqab2ypxxfxgcl4z2j04qxcpvki13vv6ph4y")))

(define-public crate-wdg-uri-0.1.21 (c (n "wdg-uri") (v "0.1.21") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "134fci920wrpv3rbf6m5klphg1cjpfpca0sdqghd8s1rg842y66h")))

(define-public crate-wdg-uri-0.1.22 (c (n "wdg-uri") (v "0.1.22") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "0m462cld07jwllprxdbbadqvilwppf79m2kn6r9z2r39ipmlykn2")))

(define-public crate-wdg-uri-0.1.23 (c (n "wdg-uri") (v "0.1.23") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "0qdp35xrqni3n0hripi5x6g80ji66bg0mj1w1fdnsayk7h6jlrsz")))

(define-public crate-wdg-uri-0.1.24 (c (n "wdg-uri") (v "0.1.24") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "1zzv4h31xw7fg3fhcd41jgk5bj2wv7na40gn6p2g67292lgkhq2v")))

(define-public crate-wdg-uri-0.1.25 (c (n "wdg-uri") (v "0.1.25") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "1kf7ydazprzkf3vffqqhsrzj3vhg2ln7dq6k1zz136z8ikb5i7ix")))

(define-public crate-wdg-uri-0.1.26 (c (n "wdg-uri") (v "0.1.26") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.1.1") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "12jj3xirzrpa0p53p3a8db1ikvx7xbkzvyyixhafzvf8vd74vd1i")))

(define-public crate-wdg-uri-0.1.27 (c (n "wdg-uri") (v "0.1.27") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.2.3") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "1zwrrjkn3vy0iifj81vpz08ydkmr7wyrw93n33yq339hmv6ihlvb")))

(define-public crate-wdg-uri-0.2.0 (c (n "wdg-uri") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.2.3") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "1b91im8xbxsd9kin24z375p109riig20nqi6bmnr5jh1zsc99dbg")))

(define-public crate-wdg-uri-0.3.0 (c (n "wdg-uri") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.2.3") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "0dl69mklycy9nbgax30vcizvqslyw3qqlb0sw1f0ys88g568ad46")))

(define-public crate-wdg-uri-0.3.1 (c (n "wdg-uri") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.2.3") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "0pz30gywqjy9h9ckm90i6kmkjscbg1yw4ji5hw08l74fl8kxn7pc")))

(define-public crate-wdg-uri-0.3.2 (c (n "wdg-uri") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.2.3") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "0lfgmbjrsrhgzn71qbqccqqhn6dfg9rw17rg0jcbajlj3b2vc5z5")))

(define-public crate-wdg-uri-0.3.3 (c (n "wdg-uri") (v "0.3.3") (d (list (d (n "default-port") (r "^2.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.2.3") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "01wqv7fr56bh1kd2rbcvbsvi34c84whph655gxb6xmdyx0d8m5jm")))

(define-public crate-wdg-uri-0.3.4 (c (n "wdg-uri") (v "0.3.4") (d (list (d (n "default-port") (r "^2.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.2.3") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "17v3d3mm07v1k077b75ds3c1br4sgp46m068id3dw53wx16h24kr")))

(define-public crate-wdg-uri-0.3.5 (c (n "wdg-uri") (v "0.3.5") (d (list (d (n "default-port") (r "^2.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.2.5") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "1g6dz37imk7ibmqy89fbz9gkndnzad2gc51d917lj4pg4w824p68")))

(define-public crate-wdg-uri-0.3.6 (c (n "wdg-uri") (v "0.3.6") (d (list (d (n "default-port") (r "^2.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.2.5") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "0n3x99rmn7n25ifnzkh66p7xiah603mjdj9w2jh9gwgyk3xpnr0y")))

(define-public crate-wdg-uri-0.3.7 (c (n "wdg-uri") (v "0.3.7") (d (list (d (n "default-port") (r "^2.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "regexp") (r "^0.2.5") (d #t) (k 0)) (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)))) (h "0gyv8xrg6m14d926xhmair2wwv60lp2ipp0rgjn2asj6hbmrl6gp")))

