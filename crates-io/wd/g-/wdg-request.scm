(define-module (crates-io wd g- wdg-request) #:use-module (crates-io))

(define-public crate-wdg-request-0.1.0 (c (n "wdg-request") (v "0.1.0") (h "0f7h9sl2wi9wiwmldr27944zg43fsjbbk8ngr282n51galidkrdj")))

(define-public crate-wdg-request-0.1.1 (c (n "wdg-request") (v "0.1.1") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)) (d (n "wdg-uri") (r "^0.1.18") (d #t) (k 0)))) (h "0463bm824nslk3z8iy64myfrd5j3jszjg4sbim4asm19j4lrgsqm")))

(define-public crate-wdg-request-0.1.2 (c (n "wdg-request") (v "0.1.2") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)) (d (n "wdg-uri") (r "^0.1.18") (d #t) (k 0)))) (h "05nhbqni3x7qiqmsd5q1b6nv1ryjc52868azd29ls8d2iwwnpz2l")))

(define-public crate-wdg-request-0.1.3 (c (n "wdg-request") (v "0.1.3") (d (list (d (n "string-repr") (r "^1.0.1") (d #t) (k 0)) (d (n "wdg-uri") (r "^0.3.7") (d #t) (k 0)))) (h "1yv52814l4m5sbhsbprmm5k1f5s3b9dbykn3bcvbcd43310cji22")))

