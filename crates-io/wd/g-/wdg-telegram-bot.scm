(define-module (crates-io wd g- wdg-telegram-bot) #:use-module (crates-io))

(define-public crate-wdg-telegram-bot-0.1.0 (c (n "wdg-telegram-bot") (v "0.1.0") (h "11b8j3h537zhl2nlxxf0ylbms8k095y924npbq618hl59asr7i28")))

(define-public crate-wdg-telegram-bot-0.1.1 (c (n "wdg-telegram-bot") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.16") (d #t) (k 0)) (d (n "native-tls") (r "^0.1.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.9") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1.3") (d #t) (k 0)) (d (n "tokio-tls") (r "^0.1.3") (d #t) (k 0)))) (h "0k1zsjajspqvnv987mg0q2z9nz32id8f6w5n2fjkkl4h7i1l9gm5")))

