(define-module (crates-io wd g- wdg-telegram-types) #:use-module (crates-io))

(define-public crate-wdg-telegram-types-0.1.0 (c (n "wdg-telegram-types") (v "0.1.0") (h "0nf50qy31pf483wzic9f20mbrv2pl2r0bb98n46rf9r6pzbavv52")))

(define-public crate-wdg-telegram-types-0.1.1 (c (n "wdg-telegram-types") (v "0.1.1") (h "04jgcnn2l65vbjnm30rwicaf98y405jk5c9q6310m34daflb4idk")))

(define-public crate-wdg-telegram-types-0.1.2 (c (n "wdg-telegram-types") (v "0.1.2") (h "0277v09qplq907n84d5r44sqa02d2sbq6qclz2yrxdd1hnbwb399")))

