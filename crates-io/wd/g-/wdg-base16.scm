(define-module (crates-io wd g- wdg-base16) #:use-module (crates-io))

(define-public crate-wdg-base16-0.1.0 (c (n "wdg-base16") (v "0.1.0") (h "0yyb5dzm97p55cv5mn2chif7dkxqrxqxhmrhk77dwvivz3dlbd6g")))

(define-public crate-wdg-base16-0.2.0 (c (n "wdg-base16") (v "0.2.0") (h "1sjsmr47f70p6am7pyckgylv632lags9z4gls12m9r5q5hbfjyf0")))

(define-public crate-wdg-base16-0.2.1 (c (n "wdg-base16") (v "0.2.1") (h "1d9a5hxi33jgjbyyz7hx15ama34kbhganrv9shmk0fd39f5b1m5r")))

(define-public crate-wdg-base16-0.2.2 (c (n "wdg-base16") (v "0.2.2") (h "1kp5h2gj49cs25cjrkibb3w03m2hpaz29069bdm2iwzlyns8n6v0")))

(define-public crate-wdg-base16-0.2.3 (c (n "wdg-base16") (v "0.2.3") (h "1miw1d1w2qff6wriqyw28vjywhvgp7r5mq3wiccgrpga7i0y3hm1")))

(define-public crate-wdg-base16-0.2.4 (c (n "wdg-base16") (v "0.2.4") (h "1rnx6ng1qrgs15a8jx5jazh0x5sxs1v4sfngssnmawvqmw6yar3v")))

(define-public crate-wdg-base16-0.2.5 (c (n "wdg-base16") (v "0.2.5") (h "0kqg8zd40nyi8i30z6njina7vgns7xrrp5m770crhfi1ckhd47ag")))

(define-public crate-wdg-base16-0.2.6 (c (n "wdg-base16") (v "0.2.6") (h "0dzwj9hg52ar89iv3xmaz6prh5ry8a0sw6dma9vpiv4imyx4i816")))

(define-public crate-wdg-base16-0.2.7 (c (n "wdg-base16") (v "0.2.7") (h "0psgcrjx8ymf5mnn1wr9d8kn2iq5kyirqcfr0y5qf6040xx880pa")))

(define-public crate-wdg-base16-0.2.8 (c (n "wdg-base16") (v "0.2.8") (h "1ylvv09rjvazkmq0fdws8na3b83chmhdlccl0saxvblpwshwhbvn")))

(define-public crate-wdg-base16-0.3.0 (c (n "wdg-base16") (v "0.3.0") (h "01lfhjmsfkls4bn1f0fcmly66kdz34l5w39dwydd5sck7w9cylw5")))

(define-public crate-wdg-base16-0.3.1 (c (n "wdg-base16") (v "0.3.1") (h "11j4gjp7k0b8fx4cagy5xxn6hsw0k1gqyk2qzj476i3ny9cqffsb")))

(define-public crate-wdg-base16-0.3.2 (c (n "wdg-base16") (v "0.3.2") (h "0g2cyk7gflvlmp6hwzsnyygdx9zi7nzfn0db4vm2gl37gxmnhdgx")))

(define-public crate-wdg-base16-0.3.3 (c (n "wdg-base16") (v "0.3.3") (h "11sxaxw18sbfd5vnnzlmpqngvs3lxz7nspvgacpvjh0m6p4jyj7c")))

(define-public crate-wdg-base16-0.3.4 (c (n "wdg-base16") (v "0.3.4") (h "0k56f91w3fkb7jk1gwk9jkywvr7xp561n9bjr0fnpb5bynzybnaq")))

(define-public crate-wdg-base16-0.3.5 (c (n "wdg-base16") (v "0.3.5") (h "0lr7iqnnl1wmbiw7dcycdckj7ihiga2jk3fgq6wvdcrbnfvr5cz6")))

(define-public crate-wdg-base16-0.3.6 (c (n "wdg-base16") (v "0.3.6") (h "11jxbcz9a2fdp08rrfbxwfvyzhqbavp6narrwlbcpzysqpvv6wad")))

(define-public crate-wdg-base16-0.3.7 (c (n "wdg-base16") (v "0.3.7") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "182cl413l5la3cj0x6n22sg92b68xxx463gyrxrgh8x5yxwpjf4h")))

(define-public crate-wdg-base16-0.3.8 (c (n "wdg-base16") (v "0.3.8") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "0sknxk1gh7byr862h408rblpyg0xl5jcck7g07ykavrn9c8xjizi")))

(define-public crate-wdg-base16-0.3.9 (c (n "wdg-base16") (v "0.3.9") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "0xc8vjhvvbhnfwqpncx42s645xpd1rjnlznlwpgrjbg1h560jk12")))

(define-public crate-wdg-base16-0.3.10 (c (n "wdg-base16") (v "0.3.10") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "1814d150s13bapxd0j2y3gn95pm4qy1ggmzyd47qmcihnjpaiklz")))

(define-public crate-wdg-base16-0.3.11 (c (n "wdg-base16") (v "0.3.11") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "1k44041jv295zlfhk2735k8cavbhncy0dmbpl6hbaxw09r15abrf")))

(define-public crate-wdg-base16-0.4.0 (c (n "wdg-base16") (v "0.4.0") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "14rkmsxfxgd57bssyz0sbwpw4q68njyqkf9p2z64h2z4ah1kyb8v")))

(define-public crate-wdg-base16-0.4.1 (c (n "wdg-base16") (v "0.4.1") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "01p34xn1nbmmgzfbzl406s00rywa3vmrbz0vs7fj9vvc0aqq7z3m")))

(define-public crate-wdg-base16-0.4.2 (c (n "wdg-base16") (v "0.4.2") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "0bb4ghpp4wbyy751l7xwddyp0vbanbqfn2lc8dpq2m45l6mwmwi5")))

(define-public crate-wdg-base16-0.4.3 (c (n "wdg-base16") (v "0.4.3") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "1qm3c4q36ln7a41byxigann1b86331cibx3yyi3c0ra2z0qra7pc")))

(define-public crate-wdg-base16-0.4.4 (c (n "wdg-base16") (v "0.4.4") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "0194d3apigfdccmd41yl898a106pnp1j6nqi3q9bwb3ma7i6qgw3")))

(define-public crate-wdg-base16-0.4.5 (c (n "wdg-base16") (v "0.4.5") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "0j01j451lx9fvn44lw873gad5klls6qz5nhz47glljpidid1qfj5")))

(define-public crate-wdg-base16-0.4.6 (c (n "wdg-base16") (v "0.4.6") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "1c89xdycx6c2i802dw5h1xibshsa19x61nb7g8qzz4w3jx938qdx")))

(define-public crate-wdg-base16-0.4.7 (c (n "wdg-base16") (v "0.4.7") (d (list (d (n "futures") (r "^0.1.15") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.6") (d #t) (k 0)))) (h "0i4jnklps8j74pnm6zpq438qig2318jm4vdjd3vggplaajc5341p")))

