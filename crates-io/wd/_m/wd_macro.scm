(define-module (crates-io wd _m wd_macro) #:use-module (crates-io))

(define-public crate-wd_macro-0.0.1 (c (n "wd_macro") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15v9lqldbfx243wrz10wyjklx0nmz4j42s9gj81agkcx3dyj0ln6")))

(define-public crate-wd_macro-0.0.2 (c (n "wd_macro") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19y198wfqkwv7kgj3amlkhqs6krnzih4x1y6j8m5m88mzzpg55hv")))

(define-public crate-wd_macro-0.0.3 (c (n "wd_macro") (v "0.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qlmc1i2xl2pkl61jyzswn4m5my81npfrnc50p69q5a67xqik84r")))

