(define-module (crates-io wd _p wd_pool) #:use-module (crates-io))

(define-public crate-wd_pool-0.0.1 (c (n "wd_pool") (v "0.0.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-channel") (r "^1.8") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "wd_log") (r "^0.1") (d #t) (k 0)))) (h "05ja5aa5lnzi2yyl28xxnd210w5nfmx83wdcmqwv2bzi2jlypvpa")))

(define-public crate-wd_pool-0.0.2 (c (n "wd_pool") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-channel") (r "^1.8") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time" "rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "wd_log") (r "^0.1") (d #t) (k 0)))) (h "1lda3wrf3lq4v0ypf7m9fnq9glmacvly5f6zppn989n841vx2ax0")))

(define-public crate-wd_pool-0.0.3 (c (n "wd_pool") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-channel") (r "^1.8") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wd_log") (r "^0.1") (d #t) (k 0)))) (h "1nixp6n4ymzsd6w7q22xza1ihlf1ygbd8wg8f6pfz2x12g4jc2lq")))

(define-public crate-wd_pool-0.1.0 (c (n "wd_pool") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-channel") (r "^1.8") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wd_log") (r "^0.1") (d #t) (k 0)))) (h "199rx7nm00yajy2qhj8h1axkg7bw68qih3rawabq75jjfv1bz5hz")))

