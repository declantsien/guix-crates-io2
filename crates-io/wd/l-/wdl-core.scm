(define-module (crates-io wd l- wdl-core) #:use-module (crates-io))

(define-public crate-wdl-core-0.0.0 (c (n "wdl-core") (v "0.0.0") (h "0w4pxgwicdb01pjfqn6x3xalpjh4rvs2s3wzmifcx3hv622wnapg")))

(define-public crate-wdl-core-0.1.0 (c (n "wdl-core") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nonempty") (r "^0.9.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (f (quote ("pretty-print"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "to_snake_case") (r "^0.1.1") (d #t) (k 0)))) (h "19x0hs1alvfv0qnv6f0i5y6hc489vbn9prsap4n9ljc2r03d9nd3")))

