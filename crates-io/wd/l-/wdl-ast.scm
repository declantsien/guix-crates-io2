(define-module (crates-io wd l- wdl-ast) #:use-module (crates-io))

(define-public crate-wdl-ast-0.0.0 (c (n "wdl-ast") (v "0.0.0") (h "11wa4drrgjph36yjgkj0ycn3zyqbf3nqgj7gwikhjds34ylpbh76")))

(define-public crate-wdl-ast-0.1.0 (c (n "wdl-ast") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^2.1.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (d #t) (k 0)) (d (n "nonempty") (r "^0.9.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.5") (f (quote ("pretty-print"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (o #t) (d #t) (k 0)) (d (n "wdl-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wdl-grammar") (r "^0.2.0") (d #t) (k 0)) (d (n "wdl-macros") (r "^0.1.0") (d #t) (k 0)))) (h "0xj93kcjiq8plpwlb6nbfh0hgpz316mda18lhzz6brrn76wzv2bd") (s 2) (e (quote (("binaries" "dep:clap" "dep:env_logger" "dep:log" "dep:tokio"))))))

