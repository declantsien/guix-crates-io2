(define-module (crates-io wd #{2s}# wd2sql) #:use-module (crates-io))

(define-public crate-wd2sql-0.1.0 (c (n "wd2sql") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "clap") (r "^4.0.26") (f (quote ("derive"))) (d #t) (k 0)) (d (n "humansize") (r "^2.1.2") (d #t) (k 0)) (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "jemallocator") (r "^0.5.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (f (quote ("bundled" "chrono"))) (d #t) (k 0)) (d (n "simd-json") (r "^0.7.0") (d #t) (k 0)) (d (n "wikidata") (r "^0.3.0") (d #t) (k 0)))) (h "0xq0djxpriqy65k2b1pfsgqg0zj9wlmywq2gpj4afnv104j80r72")))

