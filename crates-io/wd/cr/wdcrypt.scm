(define-module (crates-io wd cr wdcrypt) #:use-module (crates-io))

(define-public crate-wdcrypt-0.0.1 (c (n "wdcrypt") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.14") (d #t) (k 0)) (d (n "fernet") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0k4fb7ga2m0vq7hyjv72gvgs5sd69b6fhcb87im45qz9as71wxbf") (y #t)))

(define-public crate-wdcrypt-2.0.0 (c (n "wdcrypt") (v "2.0.0") (d (list (d (n "clap") (r "^4.0.29") (d #t) (k 0)) (d (n "fernet") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "13801jkp3xw9wzrymbpcld6sfrlk0wg91l43c4nk41825ds9zq2x") (y #t)))

(define-public crate-wdcrypt-2.0.1 (c (n "wdcrypt") (v "2.0.1") (d (list (d (n "clap") (r "^4.0.29") (d #t) (k 0)) (d (n "fernet") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "09z8074axz5rs3ya41yiikgcm90lxhnrsr2lzbv9xk39d0j93pcy") (y #t)))

(define-public crate-wdcrypt-2.1.0 (c (n "wdcrypt") (v "2.1.0") (d (list (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "fernet") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "094nr68mldffk0by38hz62sal08v0cs2158ix7ffqkbjxzg2saxl") (y #t)))

(define-public crate-wdcrypt-2.1.1 (c (n "wdcrypt") (v "2.1.1") (d (list (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "fernet") (r "^0.2.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0bh03vlgzjvgv262297yrzc4f0n6r88zaly8xskfrpjmck9f265b") (y #t)))

(define-public crate-wdcrypt-2.2.0 (c (n "wdcrypt") (v "2.2.0") (d (list (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "fernet") (r "^0.2.0") (f (quote ("rustcrypto"))) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1sj60sbpysdpynpskmyvxh9hw4cyv4ng31ksznyhgc3sx0i2ydrg")))

(define-public crate-wdcrypt-2.2.1 (c (n "wdcrypt") (v "2.2.1") (d (list (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "fernet") (r "^0.2.0") (f (quote ("rustcrypto"))) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "13ir764n45rjd801fjj43vsp6vn9cwf95lz7jwagj52hkyg7i2hj")))

(define-public crate-wdcrypt-2.2.2 (c (n "wdcrypt") (v "2.2.2") (d (list (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "fernet") (r "^0.2.0") (f (quote ("rustcrypto"))) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "06irkfizm9mhafxs9jyxg3m0pz3fsq7h8xvfdbg7w1qpihbgxa59")))

(define-public crate-wdcrypt-2.2.3 (c (n "wdcrypt") (v "2.2.3") (d (list (d (n "clap") (r "^4.0.32") (d #t) (k 0)) (d (n "fernet") (r "^0.2.0") (f (quote ("rustcrypto"))) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1micdgm1in81c49yxrzp62yz2yrz6pscpqg943vl5p9d5dwfah28")))

(define-public crate-wdcrypt-2.2.4 (c (n "wdcrypt") (v "2.2.4") (d (list (d (n "clap") (r "^4.1.4") (d #t) (k 0)) (d (n "fernet") (r "^0.2.0") (f (quote ("rustcrypto"))) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "061qh6wksfag39saxyxvk89644af8lxsmczvc9p1l9z7k38smbcs")))

(define-public crate-wdcrypt-2.3.0 (c (n "wdcrypt") (v "2.3.0") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "fernet") (r "^0.2.1") (f (quote ("rustcrypto"))) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1f463yb3d73py6dr7pq4nvbxli8www1rsxzfkw2g45c0w64pw4jh")))

(define-public crate-wdcrypt-2.3.1 (c (n "wdcrypt") (v "2.3.1") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "fernet") (r "^0.2.1") (f (quote ("rustcrypto"))) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "num_cpus") (r "^1.15") (d #t) (k 0)) (d (n "tar") (r "^0.4.38") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0ab90bsh04k6s8fvm5c0z93j6ripanc2245sg5sli1jd88p4y0p3")))

(define-public crate-wdcrypt-2.3.2 (c (n "wdcrypt") (v "2.3.2") (d (list (d (n "clap") (r "^4.3.19") (d #t) (k 0)) (d (n "fernet") (r "^0.2.1") (f (quote ("rustcrypto"))) (k 0)) (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 2)) (d (n "num_cpus") (r "^1.16") (d #t) (k 0)) (d (n "tar") (r "^0.4.40") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "16d41s6syhrrnjy8sbqkaa4kyqil6fmcs4zr72x914vy3ami2ijc")))

