(define-module (crates-io wd _b wd_balancing) #:use-module (crates-io))

(define-public crate-wd_balancing-0.0.1 (c (n "wd_balancing") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wd_log") (r "^0.1") (d #t) (k 0)))) (h "1mxvk67r4k5g9m9wh36804bhx45mdydx5z1y90pyvdzpjcw1rrl2")))

(define-public crate-wd_balancing-0.0.2 (c (n "wd_balancing") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wd_log") (r "^0.1") (d #t) (k 0)))) (h "1fmqamib6r88i85w9my1yp36fgzvcz9xg76nhgcpyblcvqnbs99m")))

(define-public crate-wd_balancing-0.0.3 (c (n "wd_balancing") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wd_log") (r "^0.1") (d #t) (k 0)))) (h "1awjyx3frl3wmk3bazs7b3ka9isgdl766nb6wp7z1xwzjglr2ky3")))

(define-public crate-wd_balancing-0.0.4 (c (n "wd_balancing") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wd_log") (r "^0.1") (d #t) (k 0)))) (h "1ps7lx378x5sa8fd9jq6a2v8cad4881hm7yz907h1cj9vx7cw94l")))

