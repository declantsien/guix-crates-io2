(define-module (crates-io wd _l wd_log) #:use-module (crates-io))

(define-public crate-wd_log-0.0.1 (c (n "wd_log") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1za5db8ajdn1z65awk34jkfj0bgjhf0im2zyqydh8z0qqhi2536g")))

(define-public crate-wd_log-0.0.2 (c (n "wd_log") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1g8khfa0p2413dpl2zwr7r22w3k3mdir3gw4f57q4xvnanmvlial")))

(define-public crate-wd_log-0.1.0 (c (n "wd_log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "128844yhdp0866qsadilzvymj26ks0am5r1cvym7xnbbapji6yrw")))

(define-public crate-wd_log-0.1.1 (c (n "wd_log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "162j5n04icqwa7wm77iadgxxpmbc5ql23q84zny6l7g72jhy3bfj")))

(define-public crate-wd_log-0.1.3 (c (n "wd_log") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05mihmff2h5sn7rsj592i7gmcx3ysgvgxss95ma2cd45i6rqjcgb")))

(define-public crate-wd_log-0.1.4 (c (n "wd_log") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0wxbq3xvhz3hb8vikx8zzq7ml5v95ycr81xf2cxb40gqmzpwph48")))

(define-public crate-wd_log-0.1.5 (c (n "wd_log") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0i59qy5ayhaix9yc8xws5pgy76qvpkwym86w5vn8b93477cfkjck")))

(define-public crate-wd_log-0.1.6 (c (n "wd_log") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1n908qi5cmfjb5z2iiz8w1z6prcpl521mbkv0ryfhl5q4b75ch6v")))

(define-public crate-wd_log-0.1.8 (c (n "wd_log") (v "0.1.8") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "15jr5ds67l4m7anicmkalhr05kgznn80hag8mrh22gjn8m3w12f1")))

(define-public crate-wd_log-0.2.0 (c (n "wd_log") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1h9wl4bx70l9ld1ggs2bkdhm983yiwy4zg1zvfpj640ksdmplcbi")))

(define-public crate-wd_log-0.3.0 (c (n "wd_log") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.16") (f (quote ("serde" "clock"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "136r0cf22qg8s1rkpqy7xlff85cdb8rh236sfpbk1fr8lbvfjbf3")))

