(define-module (crates-io wd _e wd_event) #:use-module (crates-io))

(define-public crate-wd_event-0.0.1 (c (n "wd_event") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1g6yh13hww1l545mhr87v6nj6gspyawc4n2w4vyx9823yz1ypcax")))

(define-public crate-wd_event-0.1.0 (c (n "wd_event") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3.3") (f (quote ("full"))) (d #t) (k 0)))) (h "0h3kivwgydzmcviby0kcfq3zmf9195ys9d8xj7ar02wwk3nkxdi3")))

(define-public crate-wd_event-0.1.1 (c (n "wd_event") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1cnk50pzi4l62bimzxgnfrwwfn5ckwjwymk74l78d77fivy6pkzg")))

(define-public crate-wd_event-0.1.2 (c (n "wd_event") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1g8hir91173nfqk6amn93qhkbnblb7nn0sf0097n42shznc7hlj0")))

(define-public crate-wd_event-0.1.3 (c (n "wd_event") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^0.3.3") (f (quote ("full"))) (d #t) (k 0)))) (h "1x124kdx8m3hljwsdjkq6h1xjbbf8s043h11xcbzrrs6xwk4zswg")))

(define-public crate-wd_event-0.2.0 (c (n "wd_event") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0pjyblkckivpyf1kfxg9j6ww96b1kj06bn4nla12hmn4ypv4a7n0")))

(define-public crate-wd_event-0.3.0 (c (n "wd_event") (v "0.3.0") (d (list (d (n "async-recursion") (r "^0.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 0)))) (h "0zqd1iiam98vq3dwxij3pkmfm3y7cfkvdm5yypimfmnrwgr5k4dy")))

(define-public crate-wd_event-0.3.1 (c (n "wd_event") (v "0.3.1") (d (list (d (n "async-recursion") (r "^0.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("full"))) (d #t) (k 0)))) (h "0k7qfkfaafjr4m8a3xn30nnimqki48ii6kg0rvryjyap7lna3510")))

(define-public crate-wd_event-0.3.2 (c (n "wd_event") (v "0.3.2") (d (list (d (n "async-recursion") (r "^0.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("time" "sync"))) (d #t) (k 0)))) (h "04h4799i9lccl700s2hssxz505jqajp38mxfia47yaa5z23ws8ps")))

(define-public crate-wd_event-0.3.3 (c (n "wd_event") (v "0.3.3") (d (list (d (n "async-recursion") (r "^0.3") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.42") (d #t) (k 0)) (d (n "crossbeam-queue") (r "^0.3.1") (d #t) (k 0)) (d (n "tokio") (r "^1.10") (f (quote ("time" "sync"))) (d #t) (k 0)))) (h "0y87rdn7mmm6s8cfvs2frfnq6vgnnq810p7nz3597ay4cvnx5qd0")))

(define-public crate-wd_event-0.4.0 (c (n "wd_event") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("time" "rt-multi-thread" "sync" "macros"))) (d #t) (k 0)) (d (n "wd_tools") (r "^0.2.1") (f (quote ("ptr" "point-free"))) (d #t) (k 0)))) (h "1pj58iqyy96pyyncgl3cmz97c7nv5jwl4mcqd3ggdcl5s4w4n0rz")))

(define-public crate-wd_event-0.4.1 (c (n "wd_event") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("time" "rt-multi-thread" "sync" "macros"))) (d #t) (k 0)) (d (n "wd_tools") (r "^0.4.1") (f (quote ("ptr" "point-free" "uid"))) (d #t) (k 0)))) (h "0s8xrqsxp50sirn6bhaji7jxk7w71n55jn1mxk49qz6csmci51dz") (f (quote (("cloud-event" "wd_tools/time"))))))

(define-public crate-wd_event-0.5.0 (c (n "wd_event") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("time" "rt-multi-thread" "sync" "macros"))) (d #t) (k 0)) (d (n "wd_tools") (r "^0.4.1") (f (quote ("ptr" "point-free" "uid"))) (d #t) (k 0)))) (h "07pl9vc3a2lqbrzrfmqgfvc217q98m9d9fczg0vm399cf94l9kd5") (f (quote (("cloud-event" "wd_tools/time"))))))

(define-public crate-wd_event-0.6.0 (c (n "wd_event") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("time" "rt-multi-thread" "sync" "macros"))) (d #t) (k 0)) (d (n "wd_tools") (r "^0.2.2") (f (quote ("ptr" "point-free"))) (d #t) (k 0)))) (h "1q3m7c1nd9zfvn66hpbv9599r83slky1wfxl62hp1fz5yr1nnh19")))

(define-public crate-wd_event-0.6.1 (c (n "wd_event") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("time" "rt-multi-thread" "sync" "macros"))) (d #t) (k 0)) (d (n "wd_tools") (r "^0.2.2") (f (quote ("ptr" "point-free"))) (d #t) (k 0)))) (h "19c15cgcdqanb2wpvz4z1jd9xcg4np4cpv797x87bqmbggzvhryi")))

