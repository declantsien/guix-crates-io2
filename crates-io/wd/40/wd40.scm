(define-module (crates-io wd #{40}# wd40) #:use-module (crates-io))

(define-public crate-wd40-0.1.0 (c (n "wd40") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "0ksnhinv55pn45hwg7crxsl6vw4y2965zyzgwva8hibmwyallrmh")))

(define-public crate-wd40-0.2.0 (c (n "wd40") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "0gwz2bi82sgbj5lkyxmy454a15s2mm4pa73p0fd4r2v4hnspf007")))

(define-public crate-wd40-0.2.1 (c (n "wd40") (v "0.2.1") (d (list (d (n "chrono") (r "^0.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hyper") (r "^0.9.9") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (d #t) (k 0)))) (h "181wkfhjf6a7h6ysq94bh7b4wxw8qmwin1hijcxxagmibvv9nr68")))

