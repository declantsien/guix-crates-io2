(define-module (crates-io xi np xinput) #:use-module (crates-io))

(define-public crate-xinput-0.1.0 (c (n "xinput") (v "0.1.0") (d (list (d (n "winapi") (r "^0.2.8") (d #t) (k 0)) (d (n "xinput-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0ik8f9avnzxfg6p4qi1yxklgsiknpzsk6lkxm2bd25lbil4jwfrh")))

(define-public crate-xinput-0.1.1 (c (n "xinput") (v "0.1.1") (d (list (d (n "winapi") (r "^0.2.8") (d #t) (k 0)) (d (n "xinput-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0yw5mlswhjx1w20v93kj0k31mxbfvprm44vqdyl2vb8jh4chnzfj")))

