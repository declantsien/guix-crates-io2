(define-module (crates-io xi np xinput-sys) #:use-module (crates-io))

(define-public crate-xinput-sys-0.0.1 (c (n "xinput-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "1yid1wqwz14xk875kb6ws6fymvb6c8b6vhiyg18w9ncvwyfv0ixh")))

(define-public crate-xinput-sys-0.2.0 (c (n "xinput-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "082a140fzmzh6g1r68p9x14sw38cb1hsnj8y4n095v8al9p5gk1a")))

