(define-module (crates-io xi np xinput_visualiser) #:use-module (crates-io))

(define-public crate-xinput_visualiser-0.1.0 (c (n "xinput_visualiser") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.17") (d #t) (k 0)) (d (n "rusty-xinput") (r "^1.2.0") (d #t) (k 0)) (d (n "tui") (r "^0.19") (f (quote ("crossterm"))) (k 0)))) (h "0z2xlx1cn0dgn3rwkgsii99wh3imkxjiqyf7zql8ds2wk78xps28")))

