(define-module (crates-io xi ap xiapi-sys) #:use-module (crates-io))

(define-public crate-xiapi-sys-0.1.0 (c (n "xiapi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 0)))) (h "0zwrqkskc89x66y0slyh64ng01ijl3md04sr1fiq8zhiad2x9jyi")))

(define-public crate-xiapi-sys-0.1.1 (c (n "xiapi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 0)))) (h "1hyabkxq7r5q51gbg2cjikc34ra5jg3i8fdzhkq9ayxlznpsfz0k")))

(define-public crate-xiapi-sys-0.1.2 (c (n "xiapi-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 0)))) (h "15lfzxg0yqiqvlh81wc5v7j938f9rawp1d27svxnw3hsskf9p6s7")))

