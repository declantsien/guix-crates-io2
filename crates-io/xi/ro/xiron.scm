(define-module (crates-io xi ro xiron) #:use-module (crates-io))

(define-public crate-xiron-0.3.0 (c (n "xiron") (v "0.3.0") (d (list (d (n "egui-macroquad") (r "^0.15.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "macroquad") (r "^0.3.25") (d #t) (k 0)) (d (n "parry2d") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rfd") (r "^0.11.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "zmq") (r "^0.10.0") (d #t) (k 0)))) (h "1wrvjiyxp4njm8hwxwpj3b41g69wfb7yqk95f0h37vaxlq40xh6l")))

