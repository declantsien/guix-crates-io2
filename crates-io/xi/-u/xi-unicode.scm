(define-module (crates-io xi -u xi-unicode) #:use-module (crates-io))

(define-public crate-xi-unicode-0.0.1 (c (n "xi-unicode") (v "0.0.1") (h "0hmlqzfl5ayy9g6afc4ivv4a1s87pi82xcrmxbipg8kzilalwp1i")))

(define-public crate-xi-unicode-0.1.0 (c (n "xi-unicode") (v "0.1.0") (h "1cbwsiwivsfxpkav5hfqalzs6midhcijwh28s412zdqy9gd8xshj")))

(define-public crate-xi-unicode-0.2.0 (c (n "xi-unicode") (v "0.2.0") (h "1xyym8ymydihrqx49k9gqy21k76zdaa8rl3pxah9y8d6s2wwv5bk")))

(define-public crate-xi-unicode-0.2.1 (c (n "xi-unicode") (v "0.2.1") (h "1cj8pp53w6nlxwrzmfy247b8q6i09maqfhc3bi5szgxqn7c8a6z7")))

(define-public crate-xi-unicode-0.3.0 (c (n "xi-unicode") (v "0.3.0") (h "12mvjgrhr7557cib69wm4q5s4srba27pg2df9l1zihrxgnbh0wx6")))

