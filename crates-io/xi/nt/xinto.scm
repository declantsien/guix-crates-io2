(define-module (crates-io xi nt xinto) #:use-module (crates-io))

(define-public crate-xinto-0.2.0 (c (n "xinto") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "err-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0pxvljn1hwpqzr0m4mz7ijfg1wbmfxdd91rhglvmin81izv95y07")))

