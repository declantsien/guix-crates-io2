(define-module (crates-io xi tc xitca-unsafe-collection) #:use-module (crates-io))

(define-public crate-xitca-unsafe-collection-0.1.0 (c (n "xitca-unsafe-collection") (v "0.1.0") (d (list (d (n "bytes_crate") (r "^1.4") (o #t) (d #t) (k 0) (p "bytes")) (d (n "tokio") (r "^1.30") (f (quote ("rt" "sync"))) (d #t) (k 2)))) (h "0w3jh0rrfk4d7j7jf0iwfya1313fql7c8krg5jxqda9bqy95p363") (f (quote (("bytes" "bytes_crate"))))))

(define-public crate-xitca-unsafe-collection-0.1.1 (c (n "xitca-unsafe-collection") (v "0.1.1") (d (list (d (n "bytes_crate") (r "^1.4") (o #t) (d #t) (k 0) (p "bytes")) (d (n "tokio") (r "^1.30") (f (quote ("rt" "sync"))) (d #t) (k 2)))) (h "0gnjn6b82pa6213agk3grlad8jzlk2gvkcscciqc83ax3br6najm") (f (quote (("bytes" "bytes_crate"))))))

