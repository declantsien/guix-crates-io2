(define-module (crates-io xi tc xitca-codegen) #:use-module (crates-io))

(define-public crate-xitca-codegen-0.1.0 (c (n "xitca-codegen") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0z0jjvc4qy7q2vr0ykpd20gpkcn7r3xzprbijc9ba2k2sdlcaphq")))

(define-public crate-xitca-codegen-0.1.1 (c (n "xitca-codegen") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "032vrkcy1kpddg9x811wf4qrswlay3wgavydfrcc91qqmfbkgg34")))

(define-public crate-xitca-codegen-0.2.0 (c (n "xitca-codegen") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0glfykigv0lrbn0idc46pc206m9h0xjc3pnc4bq1wj40yajhcsc6")))

