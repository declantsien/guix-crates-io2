(define-module (crates-io xi tc xitca-router) #:use-module (crates-io))

(define-public crate-xitca-router-0.1.0 (c (n "xitca-router") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "matchit") (r "^0.7.3") (d #t) (k 2)) (d (n "xitca-unsafe-collection") (r "^0.1") (d #t) (k 0)))) (h "0gmpwld0zdpc2702ral3q7q90sjwvky4rg3pql5yxfh12ipyk5l6") (f (quote (("default") ("__test_helpers"))))))

(define-public crate-xitca-router-0.2.0 (c (n "xitca-router") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "matchit") (r "^0.7.3") (d #t) (k 2)) (d (n "xitca-unsafe-collection") (r "^0.1") (d #t) (k 0)))) (h "02cfgdkzx143ywppyfj9dbpbhhk9qr60sy3xmd7m529blfq3yyk8") (f (quote (("default") ("__test_helpers"))))))

