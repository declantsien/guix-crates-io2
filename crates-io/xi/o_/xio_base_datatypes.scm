(define-module (crates-io xi o_ xio_base_datatypes) #:use-module (crates-io))

(define-public crate-xio_base_datatypes-0.1.0 (c (n "xio_base_datatypes") (v "0.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0mvshikq6xdbp4iqmr1rvyhh38yw0sy4i01msmvga373jalqya3z")))

(define-public crate-xio_base_datatypes-0.1.1 (c (n "xio_base_datatypes") (v "0.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1pmycwdba5dgivzr8nv8d900ipqn99h0scazj4qwxrx47w63s13l")))

(define-public crate-xio_base_datatypes-0.1.2 (c (n "xio_base_datatypes") (v "0.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1vqxg5lk08h2lw1bj0z5a4zn7s0r41wy8fkmp2agrl8rdzd65ciq")))

(define-public crate-xio_base_datatypes-0.1.3 (c (n "xio_base_datatypes") (v "0.1.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0yqqp1a48ym3hpyzzzc0xkwr2ym4pqkpizfbx5gvqxx0f8vj9l4q")))

(define-public crate-xio_base_datatypes-0.1.4 (c (n "xio_base_datatypes") (v "0.1.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "15c6k2wmmpyab1s4dyjbpjnaq9zzz457zqawcqr5d77r9p47qli1")))

(define-public crate-xio_base_datatypes-0.1.5 (c (n "xio_base_datatypes") (v "0.1.5") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "08jsfmg1vczc8kg0m7gqll9nv45kyg2fn2a9ff522af7f0qxcy7l")))

(define-public crate-xio_base_datatypes-0.1.6 (c (n "xio_base_datatypes") (v "0.1.6") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0rbi3fbnq0km8zypfm22ldrmrm3kg7pfaffamr2pn0jnlgfpxprn")))

(define-public crate-xio_base_datatypes-0.2.0 (c (n "xio_base_datatypes") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)))) (h "0q2gmc23vfc4nmw0jlgh7l2w4dmyyh4cilrv29pij65rdmg3m8ql") (f (quote (("with_serde" "serde" "serde_derive") ("default"))))))

(define-public crate-xio_base_datatypes-0.2.1 (c (n "xio_base_datatypes") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0j0cffsj19wibp0k4qhz4k5s6303n7z2nn95d2bsycbjgkh7mc5a") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.2.2 (c (n "xio_base_datatypes") (v "0.2.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1inmgh8vz8m4r94i12fq521vk974md80bmw375a3m6a5jbjw7n2j") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.2.3 (c (n "xio_base_datatypes") (v "0.2.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1395pfl9qp88kgbkc47blh6mswg065r8h20nwn667nmaaax3247w") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.3.0 (c (n "xio_base_datatypes") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19nmym4y793pij9y14radwimmll3njh8g5wks83713nm9km48yfx") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.3.1 (c (n "xio_base_datatypes") (v "0.3.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "00692bz5kjqb6m4ixczhcpy2grhi0cjl2nyfmmmis4xk0mnidznd") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.3.2 (c (n "xio_base_datatypes") (v "0.3.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "18wrcvd8xzxkr4cpmhgprqzzhm1pf9v15mxqbph1asgrbjq6jc7b") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.3.3 (c (n "xio_base_datatypes") (v "0.3.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "03lgwxcs2mkr4bd7qf1bf3lz8r60syr683f1lkc4iqpcpgyb2cyd") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.3.4 (c (n "xio_base_datatypes") (v "0.3.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cl246w7f5mnc9iy3nfllqlja7y8z0iwyyrqjvvsayf7pgkz363v") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.3.5 (c (n "xio_base_datatypes") (v "0.3.5") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wwymj5b4d6pq9vjvs4g04xj1z824k05d8zqfkad9zwlglja9dz8") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.3.6 (c (n "xio_base_datatypes") (v "0.3.6") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "01zqyxwvhh380zbs9v3k2vyks6ly3g3y5ya2pdvd4lisnmiz05im") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.4.0 (c (n "xio_base_datatypes") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0piv12r56m64yvdkxbmkc25ycirjip6vs2wkwbs2386fiyskcy9x") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.4.1 (c (n "xio_base_datatypes") (v "0.4.1") (d (list (d (n "bidir-map") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1w2xq8sl1x5dpf2vj8c4rpzmzizvp6z1vpyhyv1v04apvjl3mmws") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.4.2 (c (n "xio_base_datatypes") (v "0.4.2") (d (list (d (n "bidir-map") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1www5ng90vfybxvpvihk6sfy7jvxwl0ch0l7vydpbq7m923xli33") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.4.3 (c (n "xio_base_datatypes") (v "0.4.3") (d (list (d (n "bidir-map") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "05nfab5310wzhi6cc2gszhwlbzzbm3gm43whi29ggxw9a49q9pkx") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.5.0 (c (n "xio_base_datatypes") (v "0.5.0") (d (list (d (n "bidir-map") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0cmn29sxsbjvzlry1vly22rmpaff0k016ylfnvc2j0p9q8zl6s8c") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.5.1 (c (n "xio_base_datatypes") (v "0.5.1") (d (list (d (n "bidir-map") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mj244k1zvqy0k0gk8gddk0j0qmzysxq9fxni7kg08ch2shlhjhi") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.6.0 (c (n "xio_base_datatypes") (v "0.6.0") (d (list (d (n "bidir-map") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.4.1") (d #t) (k 0)))) (h "0mwa091cpg2x218vrvzi2am7c35hh00fz5mbm083srnd976lvshh") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.7.0 (c (n "xio_base_datatypes") (v "0.7.0") (d (list (d (n "bidir-map") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.4.1") (d #t) (k 0)))) (h "03nmnbqch7n9cd8hywidbnwwsr7125xzjiyi54b9a8mam80c2zns") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.7.1 (c (n "xio_base_datatypes") (v "0.7.1") (d (list (d (n "bidir-map") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.4.1") (d #t) (k 0)) (d (n "try_from") (r "^0.3.2") (d #t) (k 0)))) (h "0bqdn4y8zlzmzidn3x1riyl0d77wzpaplbc7dr51h9gsdxxzisfd") (f (quote (("default"))))))

(define-public crate-xio_base_datatypes-0.8.0 (c (n "xio_base_datatypes") (v "0.8.0") (d (list (d (n "bidir-map") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "indexmap") (r "^1.0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.5.0") (d #t) (k 0)) (d (n "try_from") (r "^0.3.2") (d #t) (k 0)))) (h "1r3zz65i80vnapzb0d910ld7nbngas68gz05imzllh7if2z2li9d") (f (quote (("default"))))))

