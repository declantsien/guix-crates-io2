(define-module (crates-io xi fe xifetch) #:use-module (crates-io))

(define-public crate-xifetch-0.1.0 (c (n "xifetch") (v "0.1.0") (h "05i11i7sgh59wq075cr0vgvv0g4qkcbz7yd1ah34yxsfn1rfvccq")))

(define-public crate-xifetch-0.1.1 (c (n "xifetch") (v "0.1.1") (h "0flnyaz2c8qq3js9ihzsyj5frmmbsbih3lx9l36yjsbxaank9phs")))

(define-public crate-xifetch-0.1.2 (c (n "xifetch") (v "0.1.2") (h "1vm19x5rnz48v5vxlwigw74nw1mfil5w4ww5kx1xia57r27w7arq")))

(define-public crate-xifetch-0.1.3 (c (n "xifetch") (v "0.1.3") (h "1ajj4x6mp9hypkb55n997nl1icgpli3q9ijx3nz8xi4hb8x0mchb")))

(define-public crate-xifetch-0.1.4 (c (n "xifetch") (v "0.1.4") (h "041i73fgv52bldk36qbgs82nhk23bs9vdlfnnkz0bz5fxyc593g8")))

(define-public crate-xifetch-2.0.0 (c (n "xifetch") (v "2.0.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0m2gislz1nyr6b9fmm7yjy30bh7pkhlb1yz0nk64q86qx6nppz5l")))

(define-public crate-xifetch-2.0.1 (c (n "xifetch") (v "2.0.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1l255pf3bx3mc08w841hla19l5764qc50lg5fjj0cj5k8dj9g8k0")))

