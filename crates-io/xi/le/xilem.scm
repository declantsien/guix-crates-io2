(define-module (crates-io xi le xilem) #:use-module (crates-io))

(define-public crate-xilem-0.1.0 (c (n "xilem") (v "0.1.0") (d (list (d (n "accesskit") (r "^0.14.0") (d #t) (k 0)) (d (n "accesskit_winit") (r "^0.20.0") (d #t) (k 0)) (d (n "masonry") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "vello") (r "^0.1.0") (d #t) (k 0)) (d (n "winit") (r "^0.30.0") (d #t) (k 0)))) (h "1vq3ns037f5c0vx69j1ik0ywnpm100k55qzjb4mr52n7dzsz9xs5")))

