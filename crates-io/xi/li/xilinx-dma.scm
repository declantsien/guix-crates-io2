(define-module (crates-io xi li xilinx-dma) #:use-module (crates-io))

(define-public crate-xilinx-dma-0.0.2 (c (n "xilinx-dma") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "144sgysszp6xfd7mc3k3xi4kx7rgzl07qrk2pipbb6g13myyld58")))

(define-public crate-xilinx-dma-0.0.3 (c (n "xilinx-dma") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "1rz15rh24hhygy5jlm6rc5c1mkwfczdbrs8d18n1gb4zmzzwdx7i")))

(define-public crate-xilinx-dma-0.0.4 (c (n "xilinx-dma") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "1ixmf9iz6h9nigs893jd0hb32clq88cbmxx4d91sjxfzfmg3lllp")))

(define-public crate-xilinx-dma-0.0.5 (c (n "xilinx-dma") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "1y92j4nmzna6vawmyq6jn0rzzkkrnghbcnh2ddd7add83jjgzg1x")))

(define-public crate-xilinx-dma-0.0.6 (c (n "xilinx-dma") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "08k8yd1dddbljq15wvam6nrdp7p7h1lgyhcvyywyyqhgai4q7y7w")))

(define-public crate-xilinx-dma-0.0.7 (c (n "xilinx-dma") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "async-io") (r "^1.3.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fastrand") (r "^1.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "1di1bx9fq7yrci8zp7gny37zhla7ri6kd5fiicjh7g5fd029qakg")))

(define-public crate-xilinx-dma-0.0.8 (c (n "xilinx-dma") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-io") (r "^1.13") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "110xshafbjx55nlkyw4pp8cnl21rwjs81h6a123qrmgf5xyxvci0") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:async-io"))))))

(define-public crate-xilinx-dma-0.0.9 (c (n "xilinx-dma") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "async-io") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xindj8dsl1l90d9wywr7y5whhv8haw8z0nh3d96v32g55zl6v9f") (f (quote (("default")))) (s 2) (e (quote (("async" "dep:async-io"))))))

(define-public crate-xilinx-dma-0.0.10 (c (n "xilinx-dma") (v "0.0.10") (d (list (d (n "async-io") (r "^2.2") (o #t) (d #t) (k 0)) (d (n "fastrand") (r "^2.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0a40lrln2f1101q259v1cvpb05rab0mpjxdsqxswmza241z888j8") (f (quote (("scatter-gather") ("default")))) (s 2) (e (quote (("async" "dep:async-io"))))))

