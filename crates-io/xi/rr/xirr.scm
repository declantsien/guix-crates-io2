(define-module (crates-io xi rr xirr) #:use-module (crates-io))

(define-public crate-xirr-0.1.0 (c (n "xirr") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 2)))) (h "0h79zr1dmxclksjk8n632fw27szvvvc0pjrlzmz2zhqm9lj48c83")))

(define-public crate-xirr-0.1.1 (c (n "xirr") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 2)))) (h "0vi404va74ia3fb1217c7jmb2781ndk4366nfr2ricb5rk0hmr7w")))

(define-public crate-xirr-0.1.2 (c (n "xirr") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 2)))) (h "0rxk95q5z94lnfycjl5lmnz9y58rqk8864zvdlvx11njy8za7qvl")))

(define-public crate-xirr-0.1.3 (c (n "xirr") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 2)))) (h "0hm6wva0p6iamxdnaiaglf8nkp5f6x316fnpsc9x2jlsk6mir6fq")))

(define-public crate-xirr-0.2.0 (c (n "xirr") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.2") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 2)))) (h "04ljbgkml1sycv2nvw0rjyvr0n6y9fx55v7njh53ppqldhkvj1km")))

(define-public crate-xirr-0.2.1 (c (n "xirr") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 2)))) (h "012gqb18sljyd379r9fjkjx6zxi839izqzw2ifnw1z8gc045f8dn")))

(define-public crate-xirr-0.2.2 (c (n "xirr") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 2)))) (h "1bclggdhp1z243i0164fr990ksxp9dd9fr907n0visp8v7vcky81")))

(define-public crate-xirr-0.2.3 (c (n "xirr") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "csv") (r "^1.0.0") (d #t) (k 2)))) (h "1zdqhfi6dv0nwcqfdmwfsb2ba5m4960966dqmi21f339mnvic47p")))

