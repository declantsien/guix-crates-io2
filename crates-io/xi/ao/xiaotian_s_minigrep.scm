(define-module (crates-io xi ao xiaotian_s_minigrep) #:use-module (crates-io))

(define-public crate-xiaotian_s_minigrep-0.1.0 (c (n "xiaotian_s_minigrep") (v "0.1.0") (h "1qnfi8jwr8zncl9mpppj09m7r25j2qwr4khk4li4iw2x7ydp90ss")))

(define-public crate-xiaotian_s_minigrep-0.1.1 (c (n "xiaotian_s_minigrep") (v "0.1.1") (h "1mvdsbli65lqsxbyc3646bqczpi4iykdq15qpi572c601yd1yykg")))

(define-public crate-xiaotian_s_minigrep-0.1.2 (c (n "xiaotian_s_minigrep") (v "0.1.2") (h "16i0iiyc3rb31sbna6f4fhyabkhvpbj4xgprvvwnqz61h1agzbdy")))

(define-public crate-xiaotian_s_minigrep-0.1.3 (c (n "xiaotian_s_minigrep") (v "0.1.3") (h "17fk5j7p4yrl485yxmmsp6bs8z4b2g86nyn902hcfvdajhj39a41")))

(define-public crate-xiaotian_s_minigrep-0.1.4 (c (n "xiaotian_s_minigrep") (v "0.1.4") (h "16cj00jm7h21lm8xllqjkwigrv2qwc0g9ahgcqjkzib9brpvc33q")))

