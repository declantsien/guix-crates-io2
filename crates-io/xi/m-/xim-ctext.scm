(define-module (crates-io xi m- xim-ctext) #:use-module (crates-io))

(define-public crate-xim-ctext-0.1.0 (c (n "xim-ctext") (v "0.1.0") (h "0hx7ff1aw6mcfdvp08bf28srhswvrh137z1fbqcmk0a0vh0rk8nq")))

(define-public crate-xim-ctext-0.2.0 (c (n "xim-ctext") (v "0.2.0") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)))) (h "0bidbhyvkz1s842vfpdpx65yqi0x9h3knj6x46jdd5xz5b5wrc7v")))

(define-public crate-xim-ctext-0.3.0 (c (n "xim-ctext") (v "0.3.0") (d (list (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 0)))) (h "0k186cy9dcw999w74ak39f1cfp2xhgpywbp8nqvkq3y4c9q1miia") (f (quote (("std") ("default" "std"))))))

