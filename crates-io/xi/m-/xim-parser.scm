(define-module (crates-io xi m- xim-parser) #:use-module (crates-io))

(define-public crate-xim-parser-0.1.0 (c (n "xim-parser") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)) (d (n "xim-ctext") (r "^0.1.0") (d #t) (k 2)) (d (n "xim-gen") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "01v6nxyziifs4jyvgjywvmw58dp07hykks36p16pykb9lngj1fyv") (f (quote (("bootstrap" "xim-gen"))))))

(define-public crate-xim-parser-0.1.1 (c (n "xim-parser") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.7.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.28") (d #t) (k 0)) (d (n "xim-ctext") (r "^0.2.0") (d #t) (k 2)) (d (n "xim-gen") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "1pcijgs5sa3y7mfl5hq3jpf4ygi0ws3vv7jnrvzmz3mb90q3drap") (f (quote (("bootstrap" "xim-gen"))))))

(define-public crate-xim-parser-0.2.0 (c (n "xim-parser") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "xim-ctext") (r "^0.3.0") (d #t) (k 2)) (d (n "xim-gen") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "1736nwganwwnqnpf5q43xvyc40lc92ygqg8nvrgf1kw8wn7ix2hm") (f (quote (("std") ("default" "std") ("bootstrap" "xim-gen")))) (y #t)))

(define-public crate-xim-parser-0.2.1 (c (n "xim-parser") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3.2") (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "xim-ctext") (r "^0.3.0") (d #t) (k 2)) (d (n "xim-gen") (r "^0.1.0") (o #t) (d #t) (k 1)))) (h "0lzysrpqs4bwgbqkc80yvl4bqx6l1p62ll1945hbzvwgfnavh05i") (f (quote (("std") ("default" "std") ("bootstrap" "xim-gen"))))))

