(define-module (crates-io xi m- xim-gen) #:use-module (crates-io))

(define-public crate-xim-gen-0.1.0 (c (n "xim-gen") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.14") (d #t) (k 0)))) (h "1kqkz8snpb34c6af6hki13cq90vis2frhzgxkdm066xl09z6lzn2")))

