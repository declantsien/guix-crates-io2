(define-module (crates-io xi sf xisf) #:use-module (crates-io))

(define-public crate-xisf-0.1.0 (c (n "xisf") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)))) (h "16z665zjhnn2dagsyan3cr3w2li2awwdl9p2v0v996yv4qvw2s0g")))

