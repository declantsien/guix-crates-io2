(define-module (crates-io xi -r xi-rpc) #:use-module (crates-io))

(define-public crate-xi-rpc-0.1.0 (c (n "xi-rpc") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)))) (h "16a4wb66vv43zcjipxbnfkipgck8fz48gdir26657sxmxflcad79")))

(define-public crate-xi-rpc-0.2.0 (c (n "xi-rpc") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0mxx9lw926kd12jkrhjxiivpzwi7xmrdr8lsr5psnmvs0d12pyck")))

(define-public crate-xi-rpc-0.3.0 (c (n "xi-rpc") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "xi-trace") (r "^0.2.0") (d #t) (k 0)))) (h "03wraxd9215z0pzybrzkqa0w3scl35s9c9796i8zl5jylhlc2p5y")))

