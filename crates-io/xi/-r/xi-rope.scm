(define-module (crates-io xi -r xi-rope) #:use-module (crates-io))

(define-public crate-xi-rope-0.1.0 (c (n "xi-rope") (v "0.1.0") (h "17qqmnh126sh1m4flq95n0shvrvdkmdjccwmsj5japnqdrdx7wn4")))

(define-public crate-xi-rope-0.1.1 (c (n "xi-rope") (v "0.1.1") (d (list (d (n "bytecount") (r "^0.1.2") (d #t) (k 0)))) (h "1ny4q5i0hashqvgw0jlpijr97mpps22ihqk408847h2lq0i9wygm") (f (quote (("simd-accel" "bytecount/simd-accel") ("avx-accel" "bytecount/avx-accel"))))))

(define-public crate-xi-rope-0.2.0 (c (n "xi-rope") (v "0.2.0") (d (list (d (n "bytecount") (r "^0.1.2") (d #t) (k 0)))) (h "165cma9vms7jjp4wcikf78hzcwsb7cpkm5jbx7qib3jz1a9h9421") (f (quote (("simd-accel" "bytecount/simd-accel") ("avx-accel" "bytecount/avx-accel"))))))

(define-public crate-xi-rope-0.3.0 (c (n "xi-rope") (v "0.3.0") (d (list (d (n "bytecount") (r "^0.5") (d #t) (k 0)) (d (n "memchr") (r "^2.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.2.1") (d #t) (k 0)))) (h "0njxqqmljdaaxa16pni670zpx269pg3jndq55538cjhr29k6q9n1") (f (quote (("default"))))))

