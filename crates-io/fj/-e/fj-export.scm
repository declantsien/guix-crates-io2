(define-module (crates-io fj -e fj-export) #:use-module (crates-io))

(define-public crate-fj-export-0.6.0 (c (n "fj-export") (v "0.6.0") (d (list (d (n "fj-interop") (r "^0.6.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.6.0") (d #t) (k 0)) (d (n "threemf") (r "^0.3.0") (d #t) (k 0)))) (h "1h5jd0yq2zb3ihvs8413wqp2h4nllslfavblaa253154ncgc5l1s")))

(define-public crate-fj-export-0.7.0 (c (n "fj-export") (v "0.7.0") (d (list (d (n "fj-interop") (r "^0.7.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.7.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "1lwwn3hja92wlsf0hkcsr8bqfvgbm69bfyjiz1v5wmibndavazjp")))

(define-public crate-fj-export-0.8.0 (c (n "fj-export") (v "0.8.0") (d (list (d (n "fj-interop") (r "^0.8.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.8.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "1496873zpsc8l3nliws30asqg3icwn5s4sgf7knkf3zqn32ijf1g")))

(define-public crate-fj-export-0.9.0 (c (n "fj-export") (v "0.9.0") (d (list (d (n "fj-interop") (r "^0.9.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.9.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "1gr20x2h2sk4a61l6iqxckvc22jryk3w1li3sk0ymmclhg9rbr1x")))

(define-public crate-fj-export-0.10.0 (c (n "fj-export") (v "0.10.0") (d (list (d (n "fj-interop") (r "^0.10.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.10.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0pdmpbzig8l13dyfj682250fi02nh54ybizxsgr4c55m98ix5frq")))

(define-public crate-fj-export-0.11.0 (c (n "fj-export") (v "0.11.0") (d (list (d (n "fj-interop") (r "^0.11.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.11.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0lwb8i1jpqjdz95f79m76ylbl099rasx2g33visyx678mb85k1nx")))

(define-public crate-fj-export-0.12.0 (c (n "fj-export") (v "0.12.0") (d (list (d (n "fj-interop") (r "^0.12.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.12.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "1cg8rr7qhdq5bxd5f9fajf015rjkqkh0mis2k616n5lhpxw54437")))

(define-public crate-fj-export-0.13.0 (c (n "fj-export") (v "0.13.0") (d (list (d (n "fj-interop") (r "^0.13.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.13.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0llkmzmg6f711ny1laqciwl85v3v38vbkfi3bswi0ds4z0zai5xj")))

(define-public crate-fj-export-0.14.0 (c (n "fj-export") (v "0.14.0") (d (list (d (n "fj-interop") (r "^0.14.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.14.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "1hj8rbm481x7g4ahjmpqp6ljw5w04nj9mrih1rvq1mnxy1sxk7bq")))

(define-public crate-fj-export-0.15.0 (c (n "fj-export") (v "0.15.0") (d (list (d (n "fj-interop") (r "^0.15.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.15.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "12c2mlm96k1f33vchjvj17q54pwd11rja9ipb76kbsfx7s0vadm8")))

(define-public crate-fj-export-0.16.0 (c (n "fj-export") (v "0.16.0") (d (list (d (n "fj-interop") (r "^0.16.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.16.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "10bpq4cxbf14z6dybsg51bxbwpknkk0bf62m981pcc0hzvf6ys8g")))

(define-public crate-fj-export-0.17.0 (c (n "fj-export") (v "0.17.0") (d (list (d (n "fj-interop") (r "^0.17.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.17.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "01bm5yzxiigdkd24n9qxz9hrgfp627wvgk94w783h7s3c4pfd4pi")))

(define-public crate-fj-export-0.18.0 (c (n "fj-export") (v "0.18.0") (d (list (d (n "fj-interop") (r "^0.18.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.18.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0nxf4h5jadc2wkg40ky10bxbjkqpfxx6baj24z3cnqri11f0jvqz")))

(define-public crate-fj-export-0.19.0 (c (n "fj-export") (v "0.19.0") (d (list (d (n "fj-interop") (r "^0.19.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.19.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0cqb5bxima3wgw7hfn99sjd97hpr4p5x5nnckmlnip7ihiwm89bn")))

(define-public crate-fj-export-0.20.0 (c (n "fj-export") (v "0.20.0") (d (list (d (n "fj-interop") (r "^0.20.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.20.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "1rfzzk0w3gb81npjn88p7pm7r87644r3w01jczi8afg3c8jq96cg")))

(define-public crate-fj-export-0.21.0 (c (n "fj-export") (v "0.21.0") (d (list (d (n "fj-interop") (r "^0.21.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.21.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "141cxyn2gaz8v4wrh06xj5b974l7jhv99kk6zp9a502fbm7sjwp4")))

(define-public crate-fj-export-0.22.0 (c (n "fj-export") (v "0.22.0") (d (list (d (n "fj-interop") (r "^0.22.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.22.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "055lq3wzw4ijw103qmfv88vhaq1pzpykab8vmnhdibb8d8bp1kl9")))

(define-public crate-fj-export-0.23.0 (c (n "fj-export") (v "0.23.0") (d (list (d (n "fj-interop") (r "^0.23.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.23.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "128w3kp1q4qyaa1n73flxyky0321nz8jn8ffzm5palkv5g6gk8pk")))

(define-public crate-fj-export-0.24.0 (c (n "fj-export") (v "0.24.0") (d (list (d (n "fj-interop") (r "^0.24.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.24.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0gdlq0s5hnryj07d12n5m9f1h5x50kcfjv535c13bc3c96rvvsf3")))

(define-public crate-fj-export-0.25.0 (c (n "fj-export") (v "0.25.0") (d (list (d (n "fj-interop") (r "^0.25.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.25.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0my7pcspggd596rj6qhhs50309vysxzyifgys3npmnslj9cnh5ja")))

(define-public crate-fj-export-0.26.0 (c (n "fj-export") (v "0.26.0") (d (list (d (n "fj-interop") (r "^0.26.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.26.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0s49fc3037ryqp8mpxlwcmqpmwwshr5ckc0l8bzhyjdqrczz098n")))

(define-public crate-fj-export-0.27.0 (c (n "fj-export") (v "0.27.0") (d (list (d (n "fj-interop") (r "^0.27.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.27.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "1r6hhzkcla2ps964c91ks6yr8mpgghs1x7pbdrhrr8bs6vranbd5")))

(define-public crate-fj-export-0.28.0 (c (n "fj-export") (v "0.28.0") (d (list (d (n "fj-interop") (r "^0.28.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.28.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0v26cgrzx9qczma5fdvii3jfdbvd4nsf4bhz8yld6vpnair7i71h")))

(define-public crate-fj-export-0.29.0 (c (n "fj-export") (v "0.29.0") (d (list (d (n "fj-interop") (r "^0.29.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.29.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0khi5cym1c42gm20zrdpi4a4yf5g4g4ql7hz7qw9vdnxs5f1smk6")))

(define-public crate-fj-export-0.30.0 (c (n "fj-export") (v "0.30.0") (d (list (d (n "fj-interop") (r "^0.30.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.30.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "1xry6faxkfwzj9xfjfq7kvxzz7ds255hshrp6phbaq38d07bwvwa")))

(define-public crate-fj-export-0.31.0 (c (n "fj-export") (v "0.31.0") (d (list (d (n "fj-interop") (r "^0.31.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.31.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "1w6wbv8c220360989xj30j7bfz7nzqhxq2dsdbbw088y7v38v0kg")))

(define-public crate-fj-export-0.32.0 (c (n "fj-export") (v "0.32.0") (d (list (d (n "fj-interop") (r "^0.32.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.32.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0m1xlz33p2l6p4sxw5grxqdq6b98nkzr8j54893igxig05ddipsp")))

(define-public crate-fj-export-0.33.0 (c (n "fj-export") (v "0.33.0") (d (list (d (n "fj-interop") (r "^0.33.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.33.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "15yaphdwaj899h4b5jaykxwxrp7jzps4pxq7d04yj70wsmvdzgjn")))

(define-public crate-fj-export-0.34.0 (c (n "fj-export") (v "0.34.0") (d (list (d (n "fj-interop") (r "^0.34.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.34.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "1sq93fckf90rb64qhfdhr2ff6i074y378mldkb60h16vs1mmnmd3")))

(define-public crate-fj-export-0.35.0 (c (n "fj-export") (v "0.35.0") (d (list (d (n "fj-interop") (r "^0.35.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.35.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0pnhlyq1663fv5jn1l7j1dbg61pbvi4256v9f9dc3hqzcpnb1r4s")))

(define-public crate-fj-export-0.36.0 (c (n "fj-export") (v "0.36.0") (d (list (d (n "fj-interop") (r "^0.36.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.36.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.3.1") (d #t) (k 0)))) (h "0jl60f391l81h0018sm7b0j8camzsm5h9gkw4pf2whwi8g60sqyc")))

(define-public crate-fj-export-0.37.0 (c (n "fj-export") (v "0.37.0") (d (list (d (n "fj-interop") (r "^0.37.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.37.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)))) (h "1fkw9sapv58lf81vcmq87i0fc16d8s0ksjbmcx89f1fn6rxh6s8z")))

(define-public crate-fj-export-0.38.0 (c (n "fj-export") (v "0.38.0") (d (list (d (n "fj-interop") (r "^0.38.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.38.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)))) (h "1nmrmiy5bhwpm5b8brvlfcaqw8x9bpyl9y7k18pw4ksaz89zh62j")))

(define-public crate-fj-export-0.39.0 (c (n "fj-export") (v "0.39.0") (d (list (d (n "fj-interop") (r "^0.39.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.39.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)))) (h "0qwshgagsf5snizclgqvllird4dp5vxqpxx2ghi1k2kahd44w3s5")))

(define-public crate-fj-export-0.40.0 (c (n "fj-export") (v "0.40.0") (d (list (d (n "fj-interop") (r "^0.40.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.40.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)))) (h "10nfg9akqcxsiv1xdg2n9phlhmkfqb4pja11rwmybbi9zmr04x0m")))

(define-public crate-fj-export-0.41.0 (c (n "fj-export") (v "0.41.0") (d (list (d (n "fj-interop") (r "^0.41.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.41.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)))) (h "08v9smw66wkdkfgp7nn2rzxyb3w0kzydqxjfa4cpmjq2ia0rcsyn")))

(define-public crate-fj-export-0.42.0 (c (n "fj-export") (v "0.42.0") (d (list (d (n "fj-interop") (r "^0.42.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.42.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)))) (h "11fym61rkd4psj8gz6jyc11xpxyl485dr6l0swd5f476jwb7zgnv")))

(define-public crate-fj-export-0.43.0 (c (n "fj-export") (v "0.43.0") (d (list (d (n "fj-interop") (r "^0.43.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.43.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)) (d (n "wavefront_rs") (r "=2.0.0-alpha.1") (d #t) (k 0)))) (h "1vjjh21kbqj086il0yxrp4y8c58lngw28283dzzrk147dicxdk74")))

(define-public crate-fj-export-0.44.0 (c (n "fj-export") (v "0.44.0") (d (list (d (n "fj-interop") (r "^0.44.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.44.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)) (d (n "wavefront_rs") (r "=2.0.0-beta.1") (d #t) (k 0)))) (h "15c8jp2532d4fkg738vmqmqr0snwxf9v4w9brzj8w786x9d6rqdr")))

(define-public crate-fj-export-0.45.0 (c (n "fj-export") (v "0.45.0") (d (list (d (n "fj-interop") (r "^0.45.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.45.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)) (d (n "wavefront_rs") (r "=2.0.0-beta.1") (d #t) (k 0)))) (h "1ngidv55pv0daalj3v114a0dj5cvbzn8c3i6swapg17f7zkgwmdl")))

(define-public crate-fj-export-0.46.0 (c (n "fj-export") (v "0.46.0") (d (list (d (n "fj-interop") (r "^0.46.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.46.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)) (d (n "wavefront_rs") (r "=2.0.0-beta.1") (d #t) (k 0)))) (h "1pf1162qfw2d5g41k2ja98agd4v5xqjzhjrxfixjjas4qmfa8gzf")))

(define-public crate-fj-export-0.47.0 (c (n "fj-export") (v "0.47.0") (d (list (d (n "fj-interop") (r "^0.47.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.47.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)) (d (n "wavefront_rs") (r "=2.0.0-beta.1") (d #t) (k 0)))) (h "0sj4x38s5gx0w28alcdr728k1hpb9gciya9awv2lfadf9is7jixv")))

(define-public crate-fj-export-0.48.0 (c (n "fj-export") (v "0.48.0") (d (list (d (n "fj-interop") (r "^0.48.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.48.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "threemf") (r "^0.4.0") (d #t) (k 0)) (d (n "wavefront_rs") (r "=2.0.0-beta.1") (d #t) (k 0)))) (h "1si0vfkvjy1g37b5ff2hy0z2jm15kcp792n55npxf6qbpb66hijx")))

(define-public crate-fj-export-0.49.0 (c (n "fj-export") (v "0.49.0") (d (list (d (n "fj-interop") (r "^0.49.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.49.0") (d #t) (k 0)) (d (n "stl") (r "^0.2.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "threemf") (r "^0.5.0") (d #t) (k 0)) (d (n "wavefront_rs") (r "=2.0.0-beta.1") (d #t) (k 0)))) (h "15ajqi4c7rhmgszvai40wmi315hpfsq64xwgg1z4jpsrlfdwx9z7")))

