(define-module (crates-io fj -o fj-operations) #:use-module (crates-io))

(define-public crate-fj-operations-0.6.0 (c (n "fj-operations") (v "0.6.0") (d (list (d (n "fj") (r "^0.6.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.6.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.6.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.6.0") (d #t) (k 0)))) (h "1wk7878524wrl0gac8hwh9nq1x8hqq43443hgg35mh0b2k1vn1wk")))

(define-public crate-fj-operations-0.7.0 (c (n "fj-operations") (v "0.7.0") (d (list (d (n "fj") (r "^0.7.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.7.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.7.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "07x2zals4920fqiz8aadf8719b0920pp7iwjihqx52hx5r4gzv5g")))

(define-public crate-fj-operations-0.8.0 (c (n "fj-operations") (v "0.8.0") (d (list (d (n "fj") (r "^0.8.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.8.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.8.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1v1by14xrl3fl286qc7cb2i6y5gcm00shbqzxg5xsvck8chf36cf")))

(define-public crate-fj-operations-0.9.0 (c (n "fj-operations") (v "0.9.0") (d (list (d (n "fj") (r "^0.9.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.9.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.9.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0pcwvn65jjn6vic7lr675dbn28irimwybx533bxgc9lz7s98lcw6")))

(define-public crate-fj-operations-0.10.0 (c (n "fj-operations") (v "0.10.0") (d (list (d (n "fj") (r "^0.10.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.10.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.10.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.10.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0bb331ql0j7r6sy5xd4gr8y93fh3yqd4srl2c66bwixs1z94xfy8")))

(define-public crate-fj-operations-0.11.0 (c (n "fj-operations") (v "0.11.0") (d (list (d (n "fj") (r "^0.11.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.11.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.11.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "00zmzv4msqnh8gc2pryalfqnsdl69shmzq1k53rwjyjl743xcbrl")))

(define-public crate-fj-operations-0.12.0 (c (n "fj-operations") (v "0.12.0") (d (list (d (n "fj") (r "^0.12.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.12.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.12.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0jyzwsc8s77n1162fl7i6sa5ai4xw14i8l6i3342yyszsgj9x3zi")))

(define-public crate-fj-operations-0.13.0 (c (n "fj-operations") (v "0.13.0") (d (list (d (n "fj") (r "^0.13.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.13.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.13.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0rs6ykzybamsxffhkmgfl7jsp1kv0hzzsbn6kq10v2n922fi538s")))

(define-public crate-fj-operations-0.14.0 (c (n "fj-operations") (v "0.14.0") (d (list (d (n "fj") (r "^0.14.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.14.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.14.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.14.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "0g711gswpnhslfwarm6f17yzqcx45sgf6vg7ydn0i33vp51as6a2")))

(define-public crate-fj-operations-0.15.0 (c (n "fj-operations") (v "0.15.0") (d (list (d (n "fj") (r "^0.15.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.15.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.15.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.15.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "19wlm2ydsh2niyp4lhxqwn5wjay37pfxnc5gr2qlp6i440vjxmxw")))

(define-public crate-fj-operations-0.16.0 (c (n "fj-operations") (v "0.16.0") (d (list (d (n "fj") (r "^0.16.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.16.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.16.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.34") (d #t) (k 0)))) (h "1q8m57i1q1a1pfmcmmi5jrspzrjdc6xvf934dbfk74bmq2957sjb")))

(define-public crate-fj-operations-0.17.0 (c (n "fj-operations") (v "0.17.0") (d (list (d (n "fj") (r "^0.17.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.17.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.17.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.17.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0rpr2a30m2j1a1slfp9ymxwkz2i4h1pfapxaz43ay0k7ddrdkv9n")))

(define-public crate-fj-operations-0.18.0 (c (n "fj-operations") (v "0.18.0") (d (list (d (n "fj") (r "^0.18.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.18.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.18.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.18.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0rnfsqq2d8hp5rf4whjm3cp6liygpsppqxasgs3zdys9bbxfsj6r")))

(define-public crate-fj-operations-0.19.0 (c (n "fj-operations") (v "0.19.0") (d (list (d (n "fj") (r "^0.19.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.19.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.19.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1i2r55m0s613lpnyw51wgi10f6f7sh48x5b2raimf6z1msw29ms5")))

(define-public crate-fj-operations-0.20.0 (c (n "fj-operations") (v "0.20.0") (d (list (d (n "fj") (r "^0.20.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.20.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.20.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.20.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "18fdlk7zswl1i0992wzh4fclk6p6hznx4xqr1fhsqpwj6f8gdd5h")))

(define-public crate-fj-operations-0.21.0 (c (n "fj-operations") (v "0.21.0") (d (list (d (n "fj") (r "^0.21.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.21.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.21.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.21.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "080yvf0rg4vjymm150l547vcgp8wb9qq09m1rmcpk5zzvna6q2dm")))

(define-public crate-fj-operations-0.22.0 (c (n "fj-operations") (v "0.22.0") (d (list (d (n "fj") (r "^0.22.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.22.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.22.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.22.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0sqdm886fr40c1x2w5i3wblg4w5pmbanf0qkn5fgnirn6hq9ynpx")))

(define-public crate-fj-operations-0.23.0 (c (n "fj-operations") (v "0.23.0") (d (list (d (n "fj") (r "^0.23.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.23.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.23.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.23.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "10gzgqd4kyqj6q2sla3fjl42dmqi2fmpfnh0lb1wnxkq40dlyq0y")))

(define-public crate-fj-operations-0.24.0 (c (n "fj-operations") (v "0.24.0") (d (list (d (n "fj") (r "^0.24.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.24.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.24.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.24.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1qldlgxd5nx2jb80xk05xy9qhvw51yh8afcs78hqvkzrbv4lnm7v")))

(define-public crate-fj-operations-0.25.0 (c (n "fj-operations") (v "0.25.0") (d (list (d (n "fj") (r "^0.25.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.25.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.25.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1m5644gpmnrvr0rqb4cnrwga6nnzbrc0l28l6g220gfi68838ps2")))

(define-public crate-fj-operations-0.26.0 (c (n "fj-operations") (v "0.26.0") (d (list (d (n "fj") (r "^0.26.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.26.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.26.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.26.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0zjw59r43y5bppr0m6awv20qmjkfnmr20ih2nlwlsi903l4fsl3x")))

(define-public crate-fj-operations-0.27.0 (c (n "fj-operations") (v "0.27.0") (d (list (d (n "fj") (r "^0.27.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.27.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.27.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.27.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1ccxh9vkxxb4l6yvlvy9ylz8i3i1xqgf87rg7pxmywh44vpg8fjy")))

(define-public crate-fj-operations-0.28.0 (c (n "fj-operations") (v "0.28.0") (d (list (d (n "fj") (r "^0.28.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.28.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.28.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.28.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1zxyiamlj0b6j3xch4l8qpy63ppmad02cnnvc9bw2nk41jsk2d7w")))

(define-public crate-fj-operations-0.29.0 (c (n "fj-operations") (v "0.29.0") (d (list (d (n "fj") (r "^0.29.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.29.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.29.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.29.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1yfr9iv20057wkil2xyfkg62w5c07crvmlpk3m9fj6a57kv7ikam")))

(define-public crate-fj-operations-0.30.0 (c (n "fj-operations") (v "0.30.0") (d (list (d (n "fj") (r "^0.30.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.30.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.30.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.30.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1sqlwva8xl973wpxckjdkl8axkdbwnh3mbjdzl9q0s4wx8kv5hqg")))

(define-public crate-fj-operations-0.31.0 (c (n "fj-operations") (v "0.31.0") (d (list (d (n "fj") (r "^0.31.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.31.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.31.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.31.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1x8qs74vjk642g8ppyd7jvgylaa5rrzbpxlckphign0fsiz6s4fc")))

(define-public crate-fj-operations-0.32.0 (c (n "fj-operations") (v "0.32.0") (d (list (d (n "fj") (r "^0.32.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.32.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.32.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.32.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "06l2y3l1b2cgd71pk1hj7nyakyq4rvwjhzgfkqfg1gwhmyzgp6kh")))

(define-public crate-fj-operations-0.33.0 (c (n "fj-operations") (v "0.33.0") (d (list (d (n "fj") (r "^0.33.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.33.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.33.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.33.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1wkk7g88gxfjah7gjvwfs7vs7k5j2sfnpsla3pvzv5h60nllm5al")))

(define-public crate-fj-operations-0.34.0 (c (n "fj-operations") (v "0.34.0") (d (list (d (n "fj") (r "^0.34.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.34.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.34.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.34.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0im39snq63asjy71913nd4k4slw2v2cbbm7hh2a8klax94frgdr5")))

(define-public crate-fj-operations-0.35.0 (c (n "fj-operations") (v "0.35.0") (d (list (d (n "fj") (r "^0.35.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.35.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.35.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.35.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0h423hsklgin4qdwzwgy31b5vg8j865r9zpny53f6h5rlm4n1znd")))

(define-public crate-fj-operations-0.36.0 (c (n "fj-operations") (v "0.36.0") (d (list (d (n "fj") (r "^0.36.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.36.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.36.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.36.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "17wqjxhxyf7vgq6qv4aqdm4cxlwfz760yzvs6l2fk9lcdlfqqhm2")))

(define-public crate-fj-operations-0.37.0 (c (n "fj-operations") (v "0.37.0") (d (list (d (n "fj") (r "^0.37.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.37.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.37.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.37.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1caylyg43vx6h1g8bdx1rv47kiyqg9k46hl4jdv5navs3khw5ghz")))

(define-public crate-fj-operations-0.38.0 (c (n "fj-operations") (v "0.38.0") (d (list (d (n "fj") (r "^0.38.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.38.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.38.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.38.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0bq15fwvhkn35bjsawcaq4s18zliz18lmrvf7ppchw6a8r18wh28")))

(define-public crate-fj-operations-0.39.0 (c (n "fj-operations") (v "0.39.0") (d (list (d (n "fj") (r "^0.39.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.39.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.39.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.39.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "161avirc7z6gz6bj9a07lkwy7cas16p7mi4jhzyd1bic1v2nwcp3")))

(define-public crate-fj-operations-0.40.0 (c (n "fj-operations") (v "0.40.0") (d (list (d (n "fj") (r "^0.40.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.40.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.40.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.40.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0pyiikkk3zv13vjxzzn2f66ffx90n2g311cl5iabyrhb7v400byd")))

(define-public crate-fj-operations-0.41.0 (c (n "fj-operations") (v "0.41.0") (d (list (d (n "fj") (r "^0.41.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.41.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.41.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.41.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1g5nclhjin1nl8bra64rn2hmk2mnqjy1z5z3xk7jgkf12z6drzi7")))

(define-public crate-fj-operations-0.42.0 (c (n "fj-operations") (v "0.42.0") (d (list (d (n "fj") (r "^0.42.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.42.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.42.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.42.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1vny822191z8n1il1nc40491p86p3rdhl2zbzc6qq9rxkpp33akv")))

(define-public crate-fj-operations-0.43.0 (c (n "fj-operations") (v "0.43.0") (d (list (d (n "fj") (r "^0.43.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.43.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.43.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.43.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1znn7daf91qiw9iqip6lnmvfwc10gv66cdz83f1mr1wfac89l67w")))

(define-public crate-fj-operations-0.44.0 (c (n "fj-operations") (v "0.44.0") (d (list (d (n "fj") (r "^0.44.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.44.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.44.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.44.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "17g727ha5m3wcmkq8nvqj7q9pcqr5gqxwz0chx0hcai5qrm532wz")))

(define-public crate-fj-operations-0.45.0 (c (n "fj-operations") (v "0.45.0") (d (list (d (n "fj") (r "^0.45.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.45.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.45.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.45.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "11kzg34vz7l9spz1jmrnj0caa2wm2ilvl1ay8f7sqh5sb92qczs7")))

(define-public crate-fj-operations-0.46.0 (c (n "fj-operations") (v "0.46.0") (d (list (d (n "fj") (r "^0.46.0") (d #t) (k 0)) (d (n "fj-interop") (r "^0.46.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.46.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.46.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1fmklpp54qqa397d0kyvr9lrlp8rzvv94b873cgg24lkfbhb44kg")))

(define-public crate-fj-operations-0.46.1 (c (n "fj-operations") (v "0.46.1") (d (list (d (n "fj") (r "^0.46.1") (d #t) (k 0)) (d (n "fj-interop") (r "^0.46.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.46.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.46.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0kywqsms2zchhxwl7kvzb13m03zslzjyjn3w8i67ngj8qvdgyc42")))

(define-public crate-fj-operations-0.46.2 (c (n "fj-operations") (v "0.46.2") (d (list (d (n "fj") (r "^0.46.2") (d #t) (k 0)) (d (n "fj-interop") (r "^0.46.0") (d #t) (k 0)) (d (n "fj-kernel") (r "^0.46.0") (d #t) (k 0)) (d (n "fj-math") (r "^0.46.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1p5nx92ik2kmh8pk359hfaaw78lcw94fdg8i6cn6ivyavmnzyqy7")))

