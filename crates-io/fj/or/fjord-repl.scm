(define-module (crates-io fj or fjord-repl) #:use-module (crates-io))

(define-public crate-fjord-repl-0.1.0 (c (n "fjord-repl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fjord") (r "^0.1") (d #t) (k 0)))) (h "12i2xpz9wzvs3gq7apg4ka9djsm07npjqljrs5zlgkwp6y3cv82r")))

(define-public crate-fjord-repl-0.2.0 (c (n "fjord-repl") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fjord") (r "^0.4") (d #t) (k 0)))) (h "0mgv70rr1pcxjigkfb50fymzf2lqbiwf0xbph44rn6hcms8xnw61")))

(define-public crate-fjord-repl-0.2.1 (c (n "fjord-repl") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "fjord") (r "^0.4") (d #t) (k 0)))) (h "07nlb9906nhwbxyw31daf6mgfknfija425x020ndipr3ww01m34v")))

(define-public crate-fjord-repl-0.2.2 (c (n "fjord-repl") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "fjord") (r "^0.4") (d #t) (k 0)))) (h "16pmgmxqp8r0l38gqngk80fv6zr7330dfw9wam19rrahb9jx3s10")))

(define-public crate-fjord-repl-0.2.3 (c (n "fjord-repl") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "fjord") (r "^0.4") (d #t) (k 0)))) (h "1rmsa343n8awk8sgk9vpwpb33il6yyc036zq3g3lcx3blgk5j5g1")))

(define-public crate-fjord-repl-0.2.4 (c (n "fjord-repl") (v "0.2.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "fjord") (r "^0.4") (d #t) (k 0)))) (h "1ydjxwc9ngajpym1br7yfbg35vd0n9rj2l7hxgg1f24fsalh1ri7")))

