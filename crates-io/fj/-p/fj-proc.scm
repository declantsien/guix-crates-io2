(define-module (crates-io fj -p fj-proc) #:use-module (crates-io))

(define-public crate-fj-proc-0.7.0 (c (n "fj-proc") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x3dzpmy1h5by1xwhbmsbakyqwz1mqb1zqczkf41hzrf0z3iixw9")))

(define-public crate-fj-proc-0.8.0 (c (n "fj-proc") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lfkqiclwxxf45wh8canikkcmkzn5yprj84lx529xhbbcc1xxxha")))

(define-public crate-fj-proc-0.9.0 (c (n "fj-proc") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1snlll0383vsyj2zbvqj2f7097vhxa0bkmk2az7sny5cp25339qs")))

(define-public crate-fj-proc-0.10.0 (c (n "fj-proc") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.41") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "176b0zhzf5angvnfgjb9nr2a1xf5d05b9jd8wx9y267npa5fn51l")))

(define-public crate-fj-proc-0.11.0 (c (n "fj-proc") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.42") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xcrf11nv5ajg9gd1waln1lbag1hkxrcik0g1pqny856r9svpjim")))

(define-public crate-fj-proc-0.12.0 (c (n "fj-proc") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "13d7krd50zkwpxbb46pssk6cvh1q29734yk9w3i1vvvmcgshfz6d")))

(define-public crate-fj-proc-0.13.0 (c (n "fj-proc") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bmqrdqmzjqinmj8gfw29g5nk49wd7v21mb7sh371s1psh16vc5j")))

(define-public crate-fj-proc-0.14.0 (c (n "fj-proc") (v "0.14.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xg2xsj3ija9ywp89spp729kgifn51760l82gyjn68w96v3lzg33")))

(define-public crate-fj-proc-0.15.0 (c (n "fj-proc") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vmj0v22ahj675q8x1p4hvmf00g87iakjrkfk4qbh2ram0d0qs89")))

(define-public crate-fj-proc-0.16.0 (c (n "fj-proc") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "00v6k5vszvdrqg5s2qpdqxbfcrcd6j5z13ngaya745qcksw1v1ng")))

(define-public crate-fj-proc-0.17.0 (c (n "fj-proc") (v "0.17.0") (d (list (d (n "fj") (r "^0.17.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fhf38yqny62zfk1fqmv9aagh346mlvrs1l54w3frnk4ld51nlp9")))

(define-public crate-fj-proc-0.18.0 (c (n "fj-proc") (v "0.18.0") (d (list (d (n "fj") (r "^0.18.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "16wyncki5rqq34mjn3l6pjz97rn13q7pfxhy9jzrmxsml6r448i4")))

(define-public crate-fj-proc-0.19.0 (c (n "fj-proc") (v "0.19.0") (d (list (d (n "fj") (r "^0.19.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y1dhqva0jjnp1axvswv2xc1d8pgch3x3dpz1hbmsvkwgb3wkvrd")))

(define-public crate-fj-proc-0.20.0 (c (n "fj-proc") (v "0.20.0") (d (list (d (n "fj") (r "^0.20.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14fiz4gb35aqh7pgyj8xdb4hqi07hnqipm07vxni139f02xc52lz")))

(define-public crate-fj-proc-0.21.0 (c (n "fj-proc") (v "0.21.0") (d (list (d (n "fj") (r "^0.21.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bap2lwmglc24hx5aih08985812vifh9i89rz27wl3iprzk7r0hs")))

(define-public crate-fj-proc-0.22.0 (c (n "fj-proc") (v "0.22.0") (d (list (d (n "fj") (r "^0.22.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09id27bandb3450pfv429d4cqb3krsf583sb8b7dp3j9c1zvakyg")))

(define-public crate-fj-proc-0.23.0 (c (n "fj-proc") (v "0.23.0") (d (list (d (n "fj") (r "^0.23.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1js2rn2vrm0b5hwmgzqjhzafh5i4pkarsnzj7j49q393zbakkwgi")))

(define-public crate-fj-proc-0.24.0 (c (n "fj-proc") (v "0.24.0") (d (list (d (n "fj") (r "^0.24.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w21z45bgl2lgj6kz80sc9zcg95vkwxrv7xnc7isy15jkfwsc1vb")))

(define-public crate-fj-proc-0.25.0 (c (n "fj-proc") (v "0.25.0") (d (list (d (n "fj") (r "^0.25.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14arr9dc5swxwwhfh2gcss9745gmjls48iwlkynpp6rrq9k5h28k")))

(define-public crate-fj-proc-0.26.0 (c (n "fj-proc") (v "0.26.0") (d (list (d (n "fj") (r "^0.26.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.102") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15a3s3fyiw8dxn35k5v4q35d3mnm7i8mcqp0s9nk7h7xspfr9prj")))

(define-public crate-fj-proc-0.27.0 (c (n "fj-proc") (v "0.27.0") (d (list (d (n "fj") (r "^0.27.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.104") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0hi4qanww8crlrxb3swgrh5i45889s79scg5n5qnp91r82gilrj3")))

(define-public crate-fj-proc-0.28.0 (c (n "fj-proc") (v "0.28.0") (d (list (d (n "fj") (r "^0.28.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.149") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0syg858khf7qdm1l1kdwmklkv0vxar2lag5ns5b7rji7121ahcmh")))

(define-public crate-fj-proc-0.29.0 (c (n "fj-proc") (v "0.29.0") (d (list (d (n "fj") (r "^0.29.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k5af05k9z1zq7c5w8igvncywfvs7y00yb7yjakjbmpnbl96rdsx")))

(define-public crate-fj-proc-0.30.0 (c (n "fj-proc") (v "0.30.0") (d (list (d (n "fj") (r "^0.30.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lg96hzhf1flch9izr9s440a9vzrg3nh7vn4lp0fldaq19rs4r87")))

(define-public crate-fj-proc-0.31.0 (c (n "fj-proc") (v "0.31.0") (d (list (d (n "fj") (r "^0.31.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wfkpddpwgpvazp52y80nxcsc923dwlgp5p0arb18lgzhj45nj75")))

(define-public crate-fj-proc-0.32.0 (c (n "fj-proc") (v "0.32.0") (d (list (d (n "fj") (r "^0.32.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ppczigwh0rdjjxmvrpjbyfd5idpzjlq26b1zypwczxjlnb74xr6")))

(define-public crate-fj-proc-0.33.0 (c (n "fj-proc") (v "0.33.0") (d (list (d (n "fj") (r "^0.33.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.47") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cj0a9p4798acxq82bs61sylnbi2mgw7f3n69zcmyppdmch0syql")))

(define-public crate-fj-proc-0.34.0 (c (n "fj-proc") (v "0.34.0") (d (list (d (n "fj") (r "^0.34.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lszprjrngffjb3p5cfcb5sn6j404im9bai9sdlhr3s1vim0401m")))

(define-public crate-fj-proc-0.35.0 (c (n "fj-proc") (v "0.35.0") (d (list (d (n "fj") (r "^0.35.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0y33xgn987hck8vxpc01aakwcvq54m4v3w3nb3kqmrdvmmhrkls0")))

(define-public crate-fj-proc-0.36.0 (c (n "fj-proc") (v "0.36.0") (d (list (d (n "fj") (r "^0.36.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pilv4j8dcdsphxlhx953a8p8xvw49v56zvw3vwd54a1gwf86hll")))

(define-public crate-fj-proc-0.37.0 (c (n "fj-proc") (v "0.37.0") (d (list (d (n "fj") (r "^0.37.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0v51kw8c9r274msjw0kcq25lhcw86q9fzb1kdgl615gy6xdq19n0")))

(define-public crate-fj-proc-0.38.0 (c (n "fj-proc") (v "0.38.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0acwgnrllhwgvmnrbcvvpnhaf85qwwnq2fx0bfa7q0h1ib4yh5jq")))

(define-public crate-fj-proc-0.39.0 (c (n "fj-proc") (v "0.39.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "01skcb397n3m30vi0r1qn8igl8858zkj595nm9mrhqh1qw7ax23q")))

(define-public crate-fj-proc-0.40.0 (c (n "fj-proc") (v "0.40.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1k1kvvvwmc718k1i8qnf9nl4sg4z2pr0vv2yr5qaqi5j9j30rxpd")))

(define-public crate-fj-proc-0.41.0 (c (n "fj-proc") (v "0.41.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.155") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0by137193v1p273z967caapsdjkzx0crh2r6yxj0gsr3d9zijvil")))

(define-public crate-fj-proc-0.42.0 (c (n "fj-proc") (v "0.42.0") (d (list (d (n "proc-macro2") (r "^1.0.52") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (o #t) (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14w7fpv5bdjbjwdyjlp9lbxnjfkrc40kbxvxwf68n6y3clkmrz69")))

(define-public crate-fj-proc-0.43.0 (c (n "fj-proc") (v "0.43.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1glhzx7dfspckjzji27qa0szrfwjda8i1s4906clypfv2mjjblnb")))

(define-public crate-fj-proc-0.44.0 (c (n "fj-proc") (v "0.44.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1b5frppm9dwmwrxy4wk8imdqxi8xmdnv614vh6ps55sw03wspmnr")))

(define-public crate-fj-proc-0.45.0 (c (n "fj-proc") (v "0.45.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08zp8a10hw4jqp027pddmz9mlrkyp5lxvpipgw48b9ql2kwnfh31")))

(define-public crate-fj-proc-0.46.0 (c (n "fj-proc") (v "0.46.0") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0waa75010w7p6jin2vax6lkz9ib23kw00xzd99a3gn6lsdjq1y83")))

(define-public crate-fj-proc-0.46.1 (c (n "fj-proc") (v "0.46.1") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11iv2ami78ig5lmb156m6zxazld7bh5s5699xw55lpgf35jdx3d6")))

(define-public crate-fj-proc-0.46.2 (c (n "fj-proc") (v "0.46.2") (d (list (d (n "proc-macro2") (r "^1.0.56") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.162") (o #t) (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qwxhrp1xvbclhr070z3wr2i6a2xrmsikjlkjsk1554rqh8vfgab")))

