(define-module (crates-io fj #{-i}# fj-interop) #:use-module (crates-io))

(define-public crate-fj-interop-0.6.0 (c (n "fj-interop") (v "0.6.0") (d (list (d (n "fj-math") (r "^0.6.0") (d #t) (k 0)))) (h "06sivplsnd00igml0q3x0dggn5lw9s997wpg1a00f9xmnxm9dsvf")))

(define-public crate-fj-interop-0.7.0 (c (n "fj-interop") (v "0.7.0") (d (list (d (n "fj-math") (r "^0.7.0") (d #t) (k 0)))) (h "018xpnmqxnr0l1s5223nl8alc1kayfqwnrlkc9hmm91p5cqvq5q9")))

(define-public crate-fj-interop-0.8.0 (c (n "fj-interop") (v "0.8.0") (d (list (d (n "fj-math") (r "^0.8.0") (d #t) (k 0)))) (h "0lij61gpxxa25zf48hdjax01lgwr7mcrbr1sl8fq63nakcllxf7b")))

(define-public crate-fj-interop-0.9.0 (c (n "fj-interop") (v "0.9.0") (d (list (d (n "fj-math") (r "^0.9.0") (d #t) (k 0)))) (h "1f2h9bv5yh0z309i5zkpvwskjbghycpbcg4vdvnm4nr7svjnqxzw")))

(define-public crate-fj-interop-0.10.0 (c (n "fj-interop") (v "0.10.0") (d (list (d (n "fj-math") (r "^0.10.0") (d #t) (k 0)))) (h "12xmbiwxnkdam9app9pw8s9q088vi133n3vc6r44jgq2d6vc9l12")))

(define-public crate-fj-interop-0.11.0 (c (n "fj-interop") (v "0.11.0") (d (list (d (n "fj-math") (r "^0.11.0") (d #t) (k 0)))) (h "0iz3cy63zi4h7qdiwz7jn9a89wi7ayprznz14spny346ygibwv20")))

(define-public crate-fj-interop-0.12.0 (c (n "fj-interop") (v "0.12.0") (d (list (d (n "fj-math") (r "^0.12.0") (d #t) (k 0)))) (h "1ackh23l7ip9nfa2xdbzg6qm7cwjdar9jv5qzvld9pg21hy3pcfd")))

(define-public crate-fj-interop-0.13.0 (c (n "fj-interop") (v "0.13.0") (d (list (d (n "fj-math") (r "^0.13.0") (d #t) (k 0)))) (h "1l9923bcnix199bqyp7siafqn7ixn9dnx0ca0in1xbgnzwf0wwq2")))

(define-public crate-fj-interop-0.14.0 (c (n "fj-interop") (v "0.14.0") (d (list (d (n "fj-math") (r "^0.14.0") (d #t) (k 0)))) (h "0z7di6s3cqfsfnkpkv1sqkmkck01rbd7l1wk5klkm1j1brrd41lp")))

(define-public crate-fj-interop-0.15.0 (c (n "fj-interop") (v "0.15.0") (d (list (d (n "fj-math") (r "^0.15.0") (d #t) (k 0)))) (h "1cv6c2r4j0whhhq9hj19s91v0ypaz6lh646iwmyq2vzisfy7b7wi")))

(define-public crate-fj-interop-0.16.0 (c (n "fj-interop") (v "0.16.0") (d (list (d (n "fj-math") (r "^0.16.0") (d #t) (k 0)))) (h "1cb0z7rgjjk4kkj32vpk2mj5cpsipv7axgcw6sm4ci8sm8qwhl41")))

(define-public crate-fj-interop-0.17.0 (c (n "fj-interop") (v "0.17.0") (d (list (d (n "fj-math") (r "^0.17.0") (d #t) (k 0)))) (h "0c8sz9pwv3zay5hnnykg5qkh72h5krlia9ifk8lwvyyy4k5k2qcc")))

(define-public crate-fj-interop-0.18.0 (c (n "fj-interop") (v "0.18.0") (d (list (d (n "fj-math") (r "^0.18.0") (d #t) (k 0)))) (h "1pcjpsk1ps1s8pzsrknb2ydddqh3sa4z3kzzif8fqip8191klh5b")))

(define-public crate-fj-interop-0.19.0 (c (n "fj-interop") (v "0.19.0") (d (list (d (n "fj-math") (r "^0.19.0") (d #t) (k 0)))) (h "14zq9r0a3s446kg6l690ny885h0wiwvmq45rw8xnglpn1ada0bw5")))

(define-public crate-fj-interop-0.20.0 (c (n "fj-interop") (v "0.20.0") (d (list (d (n "fj-math") (r "^0.20.0") (d #t) (k 0)))) (h "0h6f4bfa18xm2p6xj33vb138ng3gp6j0jqc7dly6fq3j4sa3vl0q")))

(define-public crate-fj-interop-0.21.0 (c (n "fj-interop") (v "0.21.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "fj-math") (r "^0.21.0") (d #t) (k 0)))) (h "1ras9nsrrz5ih1xv43m6042g58spdh1z17nrxwd8zn9cb5ckjx3n")))

(define-public crate-fj-interop-0.22.0 (c (n "fj-interop") (v "0.22.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "fj-math") (r "^0.22.0") (d #t) (k 0)))) (h "0z8xx21hdhka3rzq6x861h6bsmnk4f6sshxzf8s8l66w5vr7xvfm")))

(define-public crate-fj-interop-0.23.0 (c (n "fj-interop") (v "0.23.0") (d (list (d (n "fj-math") (r "^0.23.0") (d #t) (k 0)))) (h "026vqgsar73h465z9i1w29ya8l62s99ykqbyxy5wrm2nsl06g3mq")))

(define-public crate-fj-interop-0.24.0 (c (n "fj-interop") (v "0.24.0") (d (list (d (n "fj-math") (r "^0.24.0") (d #t) (k 0)))) (h "0056wwpp09vzfp432x60vwg56j8s0zfam1jcs6g565ys1jp1qcbx")))

(define-public crate-fj-interop-0.25.0 (c (n "fj-interop") (v "0.25.0") (d (list (d (n "fj-math") (r "^0.25.0") (d #t) (k 0)))) (h "0xzaphsh8sbwxhgrz1rr1v63324bchkwglcif50qx70g8i0i410x")))

(define-public crate-fj-interop-0.26.0 (c (n "fj-interop") (v "0.26.0") (d (list (d (n "fj-math") (r "^0.26.0") (d #t) (k 0)))) (h "103jhn0i0zimy1s9znz7wjsm397r8pzqs0142afxmnwh5kh7g0d5")))

(define-public crate-fj-interop-0.27.0 (c (n "fj-interop") (v "0.27.0") (d (list (d (n "fj-math") (r "^0.27.0") (d #t) (k 0)))) (h "14ydpi0ak0sqy7ik6iic81wsrfqwwli6qi0nj4q9l9qq8k05gyly")))

(define-public crate-fj-interop-0.28.0 (c (n "fj-interop") (v "0.28.0") (d (list (d (n "fj-math") (r "^0.28.0") (d #t) (k 0)))) (h "1hnnhb01vyaxxwb4rjm7hg0avz7gd543xbdhr67ha8k60m6r797c")))

(define-public crate-fj-interop-0.29.0 (c (n "fj-interop") (v "0.29.0") (d (list (d (n "fj-math") (r "^0.29.0") (d #t) (k 0)))) (h "1pm7zm7rsmx7v9mfxcvqmjc1wj0f41z4k1iy0cy6f74w26rwh9ph")))

(define-public crate-fj-interop-0.30.0 (c (n "fj-interop") (v "0.30.0") (d (list (d (n "fj-math") (r "^0.30.0") (d #t) (k 0)))) (h "0jx79786nww9r1gyrf2l45s76kikzvwxhyiwwdp2jsyxr077zfvy")))

(define-public crate-fj-interop-0.31.0 (c (n "fj-interop") (v "0.31.0") (d (list (d (n "fj-math") (r "^0.31.0") (d #t) (k 0)))) (h "1wpla4ki5nh0inql2ywkh2i4r6nk218k8v7nbjg9q3xhr4a31qmx")))

(define-public crate-fj-interop-0.32.0 (c (n "fj-interop") (v "0.32.0") (d (list (d (n "fj-math") (r "^0.32.0") (d #t) (k 0)))) (h "0bz5yy6nq235rql1z3k7ppbinar3zaqnyy3n7bm47wcfzps8yh3g")))

(define-public crate-fj-interop-0.33.0 (c (n "fj-interop") (v "0.33.0") (d (list (d (n "fj-math") (r "^0.33.0") (d #t) (k 0)))) (h "0i8248vzmc49gii86qdb4fbl7qawdiw3i4m9vy9mfhzkj9bxmhl5")))

(define-public crate-fj-interop-0.34.0 (c (n "fj-interop") (v "0.34.0") (d (list (d (n "fj-math") (r "^0.34.0") (d #t) (k 0)))) (h "1a0dpk7d1k9lf5y9iz6vgmvm83h8wqvynjx88nb03hr9b92vz1f0")))

(define-public crate-fj-interop-0.35.0 (c (n "fj-interop") (v "0.35.0") (d (list (d (n "fj-math") (r "^0.35.0") (d #t) (k 0)))) (h "1v75q5xxbxc4n6zx9qpwqmgdvbax3s07nafp4n8qln0xrkw98asn")))

(define-public crate-fj-interop-0.36.0 (c (n "fj-interop") (v "0.36.0") (d (list (d (n "fj-math") (r "^0.36.0") (d #t) (k 0)))) (h "1r6vrn9ych5pffvxwxr6i33scnp1ifza8gm19rnrzy4bppgsp6h0")))

(define-public crate-fj-interop-0.37.0 (c (n "fj-interop") (v "0.37.0") (d (list (d (n "fj-math") (r "^0.37.0") (d #t) (k 0)))) (h "1zwz2w3vczjmgnykj1h7bn78ybbr98kss4bskffn3gpszsnq7i85")))

(define-public crate-fj-interop-0.38.0 (c (n "fj-interop") (v "0.38.0") (d (list (d (n "fj-math") (r "^0.38.0") (d #t) (k 0)))) (h "12jmaycyfvy8fyil44ydfabjnpgp3805j01n6az9y27xls44fr8b")))

(define-public crate-fj-interop-0.39.0 (c (n "fj-interop") (v "0.39.0") (d (list (d (n "fj-math") (r "^0.39.0") (d #t) (k 0)))) (h "0jmx31pv2a99ya8fr28a3hry264928dz24w19razwr7n3j4sqx8g")))

(define-public crate-fj-interop-0.40.0 (c (n "fj-interop") (v "0.40.0") (d (list (d (n "fj-math") (r "^0.40.0") (d #t) (k 0)))) (h "0hp85knqi1c8zi3qmlsa7xnrvmjf2ljrv909jxpvwy7lc9xxdb7y")))

(define-public crate-fj-interop-0.41.0 (c (n "fj-interop") (v "0.41.0") (d (list (d (n "fj-math") (r "^0.41.0") (d #t) (k 0)))) (h "1ihffb8kvcfmlph99xkq9zjn7gj2jsvgsmrarnjnxkmi46lx88gl")))

(define-public crate-fj-interop-0.42.0 (c (n "fj-interop") (v "0.42.0") (d (list (d (n "fj-math") (r "^0.42.0") (d #t) (k 0)))) (h "1v4fykq30wvx084srx2j2p66m0kv49hvwbnzfx0rlrc7jli8m9b3")))

(define-public crate-fj-interop-0.43.0 (c (n "fj-interop") (v "0.43.0") (d (list (d (n "fj-math") (r "^0.43.0") (d #t) (k 0)))) (h "11jh6gdd9xni8qm10srghk8czdbsgp975mldfbg73xg6j3pml2ik")))

(define-public crate-fj-interop-0.44.0 (c (n "fj-interop") (v "0.44.0") (d (list (d (n "fj-math") (r "^0.44.0") (d #t) (k 0)))) (h "0cnq0m5k2brmpc12q2h2lrp3cnm5ij5gins53ixg6p6l4sxqjpc5")))

(define-public crate-fj-interop-0.45.0 (c (n "fj-interop") (v "0.45.0") (d (list (d (n "fj-math") (r "^0.45.0") (d #t) (k 0)))) (h "0d941kqrqn1qd9bxpwymqjrk625jrhf86bblp4yvgrbb6xprss4k")))

(define-public crate-fj-interop-0.46.0 (c (n "fj-interop") (v "0.46.0") (d (list (d (n "fj-math") (r "^0.46.0") (d #t) (k 0)))) (h "1djjv9yb5zjcrqmfr9wphqpxwb7cl0q669ad5jjv09cvlg8x42rw")))

(define-public crate-fj-interop-0.47.0 (c (n "fj-interop") (v "0.47.0") (d (list (d (n "fj-math") (r "^0.47.0") (d #t) (k 0)))) (h "1w5n7xx70mfccxvx56kshdk447hr0qnsqh6i5h8naqd9cb394244")))

(define-public crate-fj-interop-0.48.0 (c (n "fj-interop") (v "0.48.0") (d (list (d (n "fj-math") (r "^0.48.0") (d #t) (k 0)))) (h "1cg8pxvji04c61c6zvqwbcxliyk9dry8i4bbpm6vbjaxsy08f3hp")))

(define-public crate-fj-interop-0.49.0 (c (n "fj-interop") (v "0.49.0") (d (list (d (n "fj-math") (r "^0.49.0") (d #t) (k 0)))) (h "1na4vdg1kpmpfsfqkmaj7zin57aqwk4phim9129hdkshai5ib0k9")))

