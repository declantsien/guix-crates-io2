(define-module (crates-io fj so fjson) #:use-module (crates-io))

(define-public crate-fjson-0.1.0 (c (n "fjson") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 2)))) (h "1zbj2hgm2gb5w35dx6w3jvmyva3ks5mc3770qyw3bgp9cs892l96")))

(define-public crate-fjson-0.2.0 (c (n "fjson") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (f (quote ("preserve_order"))) (d #t) (k 2)))) (h "06mvhwmgdgfdvmidxjlk5zaim0ig901vnyb433l8dpaan809qx8b")))

(define-public crate-fjson-0.2.1 (c (n "fjson") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (f (quote ("preserve_order"))) (d #t) (k 2)))) (h "0i0xmv1q14n29f7zhq728n8g8ln6bj11k1lc4h6h3hpi1wmmy5k4")))

(define-public crate-fjson-0.3.0 (c (n "fjson") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (f (quote ("preserve_order"))) (d #t) (k 2)))) (h "0r5cl307pg2c57l6hnp4mrkh2xqpx3fb1az0475byn900x06acv3")))

(define-public crate-fjson-0.3.1 (c (n "fjson") (v "0.3.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (f (quote ("preserve_order"))) (d #t) (k 2)))) (h "0pfb8k6vd8aqb0yrwaz5cznwgg392grdyh0al73j0g3snb19sx5r")))

