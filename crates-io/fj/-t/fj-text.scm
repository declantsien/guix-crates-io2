(define-module (crates-io fj -t fj-text) #:use-module (crates-io))

(define-public crate-fj-text-0.1.0 (c (n "fj-text") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "bezier-rs") (r "^0.4.0") (d #t) (k 0)) (d (n "fj") (r "^0.48.0") (d #t) (k 0)) (d (n "font") (r "^0.29.1") (d #t) (k 0)) (d (n "test-case") (r "^3.3.1") (d #t) (k 2)))) (h "12w4l9bds13z3lahdy582v5gq3rvviy4wnhqd7fzp9a7mv99mwrb")))

