(define-module (crates-io mn is mnist_read) #:use-module (crates-io))

(define-public crate-mnist_read-1.0.0 (c (n "mnist_read") (v "1.0.0") (d (list (d (n "ndarray") (r ">=0.13.1, <0.14.0") (d #t) (k 0)))) (h "046cv4iypdd27f72q5x167i3x6ibl2awnb1dnpl6nwgaiq62w6gm")))

(define-public crate-mnist_read-1.0.1 (c (n "mnist_read") (v "1.0.1") (d (list (d (n "ndarray") (r ">=0.13.1, <0.14.0") (d #t) (k 0)))) (h "08vhdwgvmfakv1p9lkxf1f014f3826dfx3qvq7pasj7ksdy45xfk")))

