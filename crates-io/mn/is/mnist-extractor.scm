(define-module (crates-io mn is mnist-extractor) #:use-module (crates-io))

(define-public crate-mnist-extractor-0.1.0 (c (n "mnist-extractor") (v "0.1.0") (d (list (d (n "libflate") (r "^0.1.27") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)))) (h "17xihw70ngkdq9hvlkqi4fh10fa2d91bds3qbsdzaqrsnwd099wr")))

(define-public crate-mnist-extractor-0.1.1 (c (n "mnist-extractor") (v "0.1.1") (d (list (d (n "libflate") (r "^0.1.27") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)))) (h "1qckj4sq5craqp0qimp71d5x6hjfmh4g3gm910sakgvdb505q20k")))

