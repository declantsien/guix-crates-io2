(define-module (crates-io mn is mnist) #:use-module (crates-io))

(define-public crate-mnist-0.1.0 (c (n "mnist") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.1") (d #t) (k 0)))) (h "1530f3hbxq4hvhn792c1iqz988148hgdjyhzy8g5q2wr5vcfrgmf")))

(define-public crate-mnist-0.2.0 (c (n "mnist") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.1") (d #t) (k 2)))) (h "1ajgmsbi7c89hsxqyxfh8wr0wb4fr1mgy7mhskan5sxyy2xm5078")))

(define-public crate-mnist-0.3.0 (c (n "mnist") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.1") (d #t) (k 2)))) (h "0fkgqkgbfnxc0a1h4pws76z7aibqlyqx4hmfy6kp2hmhmfmf7nnn")))

(define-public crate-mnist-0.4.0 (c (n "mnist") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.1") (d #t) (k 2)))) (h "1g162swp7dpb8szzmdyxs2g18a7h0q2vbfq080lv9580m3yrpw95")))

(define-public crate-mnist-0.4.1 (c (n "mnist") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (f (quote ("rust_backend"))) (o #t) (k 0)) (d (n "minifb") (r "^0.17") (d #t) (k 2)) (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.1") (d #t) (k 2)))) (h "19a285vf1sx7hrl8ji5jqjgwndyh6psk9fkf9wfx84szd8iybllz") (f (quote (("download" "reqwest" "flate2") ("default"))))))

(define-public crate-mnist-0.5.0 (c (n "mnist") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (f (quote ("rust_backend"))) (o #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "ndarray") (r "^0.14") (d #t) (k 2)) (d (n "pbr") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "show-image") (r "^0.6") (f (quote ("image"))) (d #t) (k 2)))) (h "0q62cfvqkwqw3qdni8hbmxkrwa1vkvxp5a0png75jw3m4rr0j9i1") (f (quote (("download" "curl" "pbr" "flate2") ("default"))))))

(define-public crate-mnist-0.6.0 (c (n "mnist") (v "0.6.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "curl") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "flate2") (r "^1.0.2") (f (quote ("rust_backend"))) (o #t) (k 0)) (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.14") (d #t) (k 2)) (d (n "pbr") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "show-image") (r "^0.6") (f (quote ("image"))) (d #t) (k 2)))) (h "0p7r8qcdbl5zh6g0bbfs686n74cqkix3pnq4ad83m7blq2abavm0") (f (quote (("download" "curl" "pbr" "flate2") ("default"))))))

