(define-module (crates-io mn td mntdf) #:use-module (crates-io))

(define-public crate-mntdf-0.1.0 (c (n "mntdf") (v "0.1.0") (d (list (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "mnt") (r "^0.3.1") (d #t) (k 0)))) (h "0arzfciaz9k61xzd36k4f79wkj9a94ij9mhff9zvfs5mryw9sbrd")))

(define-public crate-mntdf-0.1.1 (c (n "mntdf") (v "0.1.1") (d (list (d (n "getopt") (r "^1.1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.113") (d #t) (k 0)) (d (n "mnt") (r "^0.3.1") (d #t) (k 0)))) (h "1slf4nj1fqsjzh4jri149das3fwbwj8lk8s50ss8b610l1vymy17")))

