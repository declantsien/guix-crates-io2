(define-module (crates-io mn sw mnswpr) #:use-module (crates-io))

(define-public crate-mnswpr-0.1.0 (c (n "mnswpr") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1fjhc68gq096spqglg5c1wq585rk3xhzygkc1w7j4xk4j8lmsw6j")))

(define-public crate-mnswpr-0.1.1 (c (n "mnswpr") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0f4mx3hmnk5rsxzifr7h6sd8fssrssak0hkllz5sn0xw0gyapavx")))

(define-public crate-mnswpr-0.1.2 (c (n "mnswpr") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "1irnl3c7581h5wx8a8cm16r6y3qxym4zlyfrcrl4h4686ihawrr8")))

(define-public crate-mnswpr-0.2.0 (c (n "mnswpr") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0rchkjy3yrmks0pqllhny3v4h3micmnvwhqkvhhzvj32kw6mal7y")))

(define-public crate-mnswpr-0.3.0 (c (n "mnswpr") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.16") (d #t) (k 0)) (d (n "termion") (r "^2.0.1") (d #t) (k 0)))) (h "0i6h9f3pvqm0jz3rwzhxvacpkaslll94f4i4wqr00nxhky2xg7ms")))

