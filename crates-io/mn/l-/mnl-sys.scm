(define-module (crates-io mn l- mnl-sys) #:use-module (crates-io))

(define-public crate-mnl-sys-0.1.0 (c (n "mnl-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1cs19n2i63w23ysc5x2r8z4ls9h1fqmysyj3dj5ydxpy2w8b0z2x") (f (quote (("mnl-1-0-4"))))))

(define-public crate-mnl-sys-0.2.0 (c (n "mnl-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.41") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08kilcrbj35f1ygvhlqc21nxq0f5fgzh4r9ap6jv68n7k5ah60jk") (f (quote (("mnl-1-0-4"))))))

(define-public crate-mnl-sys-0.2.1 (c (n "mnl-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.41") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0mq4yxas22h894dlik49b5bglw4iha3h6pdayymcy7hy41dnhl4p") (f (quote (("mnl-1-0-4"))))))

