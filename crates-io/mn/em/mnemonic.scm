(define-module (crates-io mn em mnemonic) #:use-module (crates-io))

(define-public crate-mnemonic-1.0.0 (c (n "mnemonic") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)))) (h "073cijjqx4vvjyy49r3gqjfklvgv3h6hx0jq29icbqhwvigrapnq")))

(define-public crate-mnemonic-1.0.1 (c (n "mnemonic") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 2)))) (h "0xxg5x7g9xg9kj42fkc10rff342aprxc5jx736qd6mdiq3jf1yi9")))

(define-public crate-mnemonic-1.1.0 (c (n "mnemonic") (v "1.1.0") (d (list (d (n "byteorder") (r "^1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "0ygnybn7d68vrj6kmz1cj8787cs4nbpym15qy2c2dx9jl8dxbbjb") (r "1.70.0")))

(define-public crate-mnemonic-1.1.1 (c (n "mnemonic") (v "1.1.1") (d (list (d (n "quickcheck") (r "^1.0") (d #t) (k 2)))) (h "0fzn0zp0n89xp6yss35r948hjgppwd5cw159j5g5slfvb2ig7f7j") (r "1.70.0")))

