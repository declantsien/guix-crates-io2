(define-module (crates-io mn em mnemos-common) #:use-module (crates-io))

(define-public crate-mnemos-common-0.0.1 (c (n "mnemos-common") (v "0.0.1") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "postcard") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "122kykzxdxmnil567kd0k0j6k2id7cwcynm58479jzvxvvhxgwpg") (f (quote (("use-defmt" "defmt") ("default"))))))

(define-public crate-mnemos-common-0.1.0 (c (n "mnemos-common") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "postcard") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (k 0)))) (h "0fb2gdghbf2q6bazmp9zfzd2bb7c02zj3zns2mp2a0g035bswc7l") (f (quote (("use-defmt" "defmt") ("default"))))))

