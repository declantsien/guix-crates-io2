(define-module (crates-io mn em mnemos-userspace) #:use-module (crates-io))

(define-public crate-mnemos-userspace-0.0.1 (c (n "mnemos-userspace") (v "0.0.1") (d (list (d (n "common") (r "^0.0.1") (d #t) (k 0) (p "mnemos-common")))) (h "13lwm8cgg245whh3wdxi5pxs525ir5r1ik51ad3gxs20z82cnkrz")))

(define-public crate-mnemos-userspace-0.1.0 (c (n "mnemos-userspace") (v "0.1.0") (d (list (d (n "common") (r "^0.1.0") (d #t) (k 0) (p "mnemos-common")))) (h "1yy14sxscy2r5wilcbvyla2jpdjbnl7jnk751mszbxxix2zqxrjb")))

