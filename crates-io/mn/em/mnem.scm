(define-module (crates-io mn em mnem) #:use-module (crates-io))

(define-public crate-mnem-0.1.0 (c (n "mnem") (v "0.1.0") (d (list (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "skim") (r "^0.8.1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (k 0)))) (h "04zihdsrqnm3vyxy1nqhr2yy1zw87ixhz61mi8p45clf1r36cb2k")))

