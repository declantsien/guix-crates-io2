(define-module (crates-io mn em mnemonic-16bit) #:use-module (crates-io))

(define-public crate-mnemonic-16bit-0.1.0 (c (n "mnemonic-16bit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "seed15") (r "^0.1") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "userspace-rng") (r "^1") (d #t) (k 2)))) (h "059wrmzls6ka46h27j0rzsk0dfxnfc2whxrjglnhzr1573cp6l1c")))

(define-public crate-mnemonic-16bit-0.1.1 (c (n "mnemonic-16bit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dictionary-1024") (r "^0.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.5") (d #t) (k 2)) (d (n "userspace-rng") (r "^1") (d #t) (k 2)))) (h "19xb87ci2cqwzpj2f7dy09qadbqa3351gn5f9ys5s35r3k5shzzm")))

