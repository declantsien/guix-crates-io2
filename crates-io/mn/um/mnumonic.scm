(define-module (crates-io mn um mnumonic) #:use-module (crates-io))

(define-public crate-mnumonic-0.1.0 (c (n "mnumonic") (v "0.1.0") (h "1x3f2f8pk1d584dn4wc29wk0s9y9knc4xmaxlpfaqp0yc0ifhwqb") (f (quote (("en") ("default" "en"))))))

(define-public crate-mnumonic-0.2.0 (c (n "mnumonic") (v "0.2.0") (h "10zfcxr5dm9k4bhbw4k3m0nvsxlrdjvh2nmmgcn2234w219r9v2k") (f (quote (("en") ("default" "en"))))))

