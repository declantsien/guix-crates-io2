(define-module (crates-io gt fj gtfjson) #:use-module (crates-io))

(define-public crate-gtfjson-0.1.5 (c (n "gtfjson") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gtftools") (r "^0.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (d #t) (k 0)))) (h "1xbwg94x4y9lkr3v8ka41lmjzwslvvyqqdpghhy5bzw43c4f0qih")))

(define-public crate-gtfjson-0.1.6 (c (n "gtfjson") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gtftools") (r "^0.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "spinoff") (r "^0.7.0") (d #t) (k 0)))) (h "0ly3axdj4qdxhq1bn8yxbhsl52lz2zh1bpjsj3x2jd07y28hg913")))

