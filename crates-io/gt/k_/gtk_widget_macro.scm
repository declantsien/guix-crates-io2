(define-module (crates-io gt k_ gtk_widget_macro) #:use-module (crates-io))

(define-public crate-gtk_widget_macro-0.1.0 (c (n "gtk_widget_macro") (v "0.1.0") (d (list (d (n "gtk_widget_macro_derive") (r "^0.1.0") (d #t) (k 0)))) (h "17pv73zp6bad1aayvyhm95bcy8fxv262zdrzclzzc8fs9pfv19s0")))

(define-public crate-gtk_widget_macro-0.1.1 (c (n "gtk_widget_macro") (v "0.1.1") (d (list (d (n "gtk_widget_macro_derive") (r "^0.1.0") (d #t) (k 0)))) (h "03j76fydnxajq2g8xm35z0bfmf61yjh2nyixlra91w278xx2q29k")))

