(define-module (crates-io gt k_ gtk_widget_macro_derive) #:use-module (crates-io))

(define-public crate-gtk_widget_macro_derive-0.1.0 (c (n "gtk_widget_macro_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "0yzmhw9m2w5vhlhnvdri1wmjqilm0zlr1q3jaf4sqli40rsv67rz")))

(define-public crate-gtk_widget_macro_derive-0.1.1 (c (n "gtk_widget_macro_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^2.0.13") (d #t) (k 0)))) (h "17f8z2145qj406g9a086jjggya4dia98hqb93pwjc0d0fz4mbbpn")))

