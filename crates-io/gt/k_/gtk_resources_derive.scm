(define-module (crates-io gt k_ gtk_resources_derive) #:use-module (crates-io))

(define-public crate-gtk_resources_derive-0.1.0 (c (n "gtk_resources_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "1v24zh7rwz3kb2zqwlsdzc9axa1vxxs529r3qm4gimfmffina4dj") (y #t)))

(define-public crate-gtk_resources_derive-0.1.2 (c (n "gtk_resources_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (d #t) (k 0)))) (h "19nmdqka76csgpkag6icwqwvfmrnyhna7l275ym5fd42w7iwgycw") (y #t)))

(define-public crate-gtk_resources_derive-0.1.4 (c (n "gtk_resources_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p9mf1i464p0z94qi7ca3pdy2fqar88l7paag4cndwg1ijc2pvlv") (y #t)))

(define-public crate-gtk_resources_derive-0.1.5 (c (n "gtk_resources_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06m7vxmhnqqscfdvyfkahk36dwda464ck1c6v4xfsxasiav36a7l") (y #t)))

(define-public crate-gtk_resources_derive-0.1.6 (c (n "gtk_resources_derive") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "059y2wh7xdz54cd7n75h0d76m5j0qpg6vymf5wjcjlr059xn8md1") (y #t)))

