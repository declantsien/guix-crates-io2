(define-module (crates-io gt yp gtypes) #:use-module (crates-io))

(define-public crate-gtypes-0.1.0 (c (n "gtypes") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "171vz4f4jwbjfm1i496difh4a77v6iziav9fqljimhaykfi1jism")))

(define-public crate-gtypes-0.1.1 (c (n "gtypes") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "01f6lag07mlvj6nhxm9mip1jwx1x0wxkyz2ncbbg9zmlv2f9z9rp")))

(define-public crate-gtypes-0.1.2 (c (n "gtypes") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1f28dc3jahzq4pkwrrqrcnrnwjlln16fi57nn5fvn9prrgnalgzh")))

(define-public crate-gtypes-0.2.0 (c (n "gtypes") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h9wxjq2prp1af20vvvqxx67ay7wa14i9wqmw5q1dpsyaxvjv1f8")))

