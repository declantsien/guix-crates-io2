(define-module (crates-io gt ra gtrans) #:use-module (crates-io))

(define-public crate-gtrans-0.1.0 (c (n "gtrans") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ahinxk35h1cc78cb60bvyxjd0p05d37lv1ihjkf03idskx6mn6l")))

(define-public crate-gtrans-0.1.1 (c (n "gtrans") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k0i0jqw7zbr0l5c20n3yai3k2rs1b9yn1z8jrfrwgvyqfbgdpp6")))

(define-public crate-gtrans-0.1.2 (c (n "gtrans") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0kahhwvgblc0rwajcvpgfclksm3mmjjdlpdycshy5ghc6s9zb2j8")))

(define-public crate-gtrans-0.1.3 (c (n "gtrans") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^0.4.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zzqqf76vsngd3z6qvgm6aad4a00k0zrj9kqy95s435nm9f9is59")))

(define-public crate-gtrans-0.2.0 (c (n "gtrans") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^0.6.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hgx69hq6y8hlv787r45g200pxdyqlqd0cccnngcbz92knni5b7k")))

(define-public crate-gtrans-0.2.1 (c (n "gtrans") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "seahorse") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08wisk7jd7bmqynk90pk58d8ryh7ch1mcwr18g2k3vfy0gjj7fw3")))

