(define-module (crates-io gt un gtunnel) #:use-module (crates-io))

(define-public crate-gtunnel-0.1.0 (c (n "gtunnel") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "h2") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-rustls") (r "^0.22") (d #t) (k 0)) (d (n "tonic") (r "^0.6") (d #t) (k 0)) (d (n "tonic-build") (r "^0.6") (d #t) (k 1)))) (h "0pqx48ar699929qvmdk7bbl3xakyv6rg565v2am05pbyjqxh1jqm")))

