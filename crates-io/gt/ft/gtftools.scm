(define-module (crates-io gt ft gtftools) #:use-module (crates-io))

(define-public crate-gtftools-0.1.4 (c (n "gtftools") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "00wsypl0b9pmi3646n70p3v7627ij8lvpcjp9v61ddpi4xp74a6b")))

(define-public crate-gtftools-0.1.5 (c (n "gtftools") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "10m3vxdc3kgd21n19vkzsqykdfblnb9bkxmc3mximkamzfhndwvv")))

(define-public crate-gtftools-0.1.6 (c (n "gtftools") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "18svfql0facrcjy85lsck1x1x2m08z7bdl127mdhmh73f1sk399s")))

(define-public crate-gtftools-0.1.7 (c (n "gtftools") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0snbswgp9jzrniw2igr4nwj1jyp6n5xgbrchqvr49ddhmdympfnh")))

(define-public crate-gtftools-0.1.8 (c (n "gtftools") (v "0.1.8") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0w46xqz1q4vv566cmbrjijjq0c1rv1a23adzjs2nrpa0xps7vmpw")))

(define-public crate-gtftools-0.1.9 (c (n "gtftools") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "bstr") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0a62v98s7bnyy8rnwlsf8s038n1rnka50yqw3lwjijvc5sargfcc")))

