(define-module (crates-io gt fo gtfo) #:use-module (crates-io))

(define-public crate-gtfo-0.1.0 (c (n "gtfo") (v "0.1.0") (h "1gmhg8dpw50q7zn35shaiv8c33cgdcd1yd12d822ryz3gq4hi9bl")))

(define-public crate-gtfo-0.1.1 (c (n "gtfo") (v "0.1.1") (h "17mjnpaxiyr5nyp0p79fdba4mbnpg6zlfng9ck3cf44pclwhchhd")))

