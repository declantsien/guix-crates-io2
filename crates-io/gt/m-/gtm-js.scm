(define-module (crates-io gt m- gtm-js) #:use-module (crates-io))

(define-public crate-gtm-js-0.1.0 (c (n "gtm-js") (v "0.1.0") (d (list (d (n "gtm-js-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.58") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0arm3sqc33zr5bbikskgkf94b4sma5m6zk13jwj1xykp12ccz4ma")))

