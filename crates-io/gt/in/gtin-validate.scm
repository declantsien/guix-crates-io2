(define-module (crates-io gt in gtin-validate) #:use-module (crates-io))

(define-public crate-gtin-validate-0.5.0 (c (n "gtin-validate") (v "0.5.0") (h "0v9v8dhlzff0c7vj3hibbbsagwsc6b96rw66h7dp0ashhv9yvfwn")))

(define-public crate-gtin-validate-1.0.0 (c (n "gtin-validate") (v "1.0.0") (h "1ry735ql5q6vclb0116waavghnryp77mgabh11pi2fva3wpyfkkm")))

(define-public crate-gtin-validate-1.1.0 (c (n "gtin-validate") (v "1.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^0.7") (d #t) (k 2)))) (h "01d9vz8flcr3dg1fm4lkkazbgnnid3a6sgi54b5clp408rz6wbl7")))

(define-public crate-gtin-validate-1.2.0 (c (n "gtin-validate") (v "1.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "proptest") (r "^0.7") (d #t) (k 2)))) (h "110zczq031msl9k5jm6vyvq2cgykrhjs64plv6ayjyhim77d0pvh")))

(define-public crate-gtin-validate-1.3.0 (c (n "gtin-validate") (v "1.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "0k76s8wsgjq4j7n94py9wf1icnhm8gj77bbh56lmks2a619d1p7q")))

