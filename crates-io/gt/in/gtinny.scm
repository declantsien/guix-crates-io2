(define-module (crates-io gt in gtinny) #:use-module (crates-io))

(define-public crate-gtinny-0.1.0 (c (n "gtinny") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1ay604lc8qmgyacg0qaw9d9zpvf31676vk8nfpzzx39biqvsrimc")))

(define-public crate-gtinny-0.1.1 (c (n "gtinny") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1pkzkvz4zd161qvxz8bpy29fhgbnvsy3vzhnrz5346qha9nnr47m")))

(define-public crate-gtinny-0.1.2 (c (n "gtinny") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1ji6hb2qy1pcx8k0m8g6dgdqix539f5jxnpwbcwr9r71gn4j08ml")))

(define-public crate-gtinny-0.1.3 (c (n "gtinny") (v "0.1.3") (d (list (d (n "clap") (r "^4.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "12nyp1bi0ihzld22vxlla0hjzib3fp4db9w85f24r3him9qy1wgm")))

(define-public crate-gtinny-0.1.4 (c (n "gtinny") (v "0.1.4") (d (list (d (n "clap") (r "^4.4.14") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "1z80l2bkncgd28snxr71wx086nbrysx2h12ijcybmfxzlfymmyjy")))

