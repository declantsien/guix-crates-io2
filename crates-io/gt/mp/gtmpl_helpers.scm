(define-module (crates-io gt mp gtmpl_helpers) #:use-module (crates-io))

(define-public crate-gtmpl_helpers-0.1.0 (c (n "gtmpl_helpers") (v "0.1.0") (d (list (d (n "gtmpl") (r "^0.5.7") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.3.3") (d #t) (k 0)))) (h "00bgkf9jcby03w4iggpxz2miyygrpzbgdyyh1vmsxvk9gzhavkb5")))

(define-public crate-gtmpl_helpers-0.1.1 (c (n "gtmpl_helpers") (v "0.1.1") (d (list (d (n "gtmpl") (r "^0.5.7") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.3.3") (d #t) (k 0)))) (h "0g4k2x6vwr46yi2p7sq113vhh83rzf0b4gsy8i45mzrwiimkyrm2")))

(define-public crate-gtmpl_helpers-0.1.2 (c (n "gtmpl_helpers") (v "0.1.2") (d (list (d (n "gtmpl") (r "^0.5.7") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.3.3") (d #t) (k 0)))) (h "1a02hsfxba5ainhdw6vvv84ndd82g9jr396zadm28qrn33phxdpj")))

(define-public crate-gtmpl_helpers-0.1.3 (c (n "gtmpl_helpers") (v "0.1.3") (d (list (d (n "gtmpl") (r "^0.5.7") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.3.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (d #t) (k 0)))) (h "06s6dp5lhwjbhswki2rrm4afz0dqzfn7ridfnhfk3y423gip7j8i")))

