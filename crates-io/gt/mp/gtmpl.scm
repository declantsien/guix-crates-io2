(define-module (crates-io gt mp gtmpl) #:use-module (crates-io))

(define-public crate-gtmpl-0.1.0 (c (n "gtmpl") (v "0.1.0") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0skgfkx68163rygvks4f4aqrsz3av9m9rlfdsn96g34gh3jj31h4")))

(define-public crate-gtmpl-0.1.1 (c (n "gtmpl") (v "0.1.1") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19maif7hsp0w1b8j5s695dql5j0h628bsqgzr2x094vd9asgs0wh")))

(define-public crate-gtmpl-0.1.2 (c (n "gtmpl") (v "0.1.2") (d (list (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lgqlg1mkapfa5w45h5q30c1r7s2il074nlyvynfrhwvp9jg4wlp")))

(define-public crate-gtmpl-0.2.0 (c (n "gtmpl") (v "0.2.0") (d (list (d (n "gtmpl_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)))) (h "1lnbzj9pvigr5q9559ycb47kx4854a3xr05r41zr36yc3r8551qi")))

(define-public crate-gtmpl-0.2.1 (c (n "gtmpl") (v "0.2.1") (d (list (d (n "gtmpl_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)))) (h "1fqas3m53dz56prjyvgbv330hb4h2s8a61kn1kgi95qwppds4qsi")))

(define-public crate-gtmpl-0.2.2 (c (n "gtmpl") (v "0.2.2") (d (list (d (n "gtmpl_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)))) (h "19c9d61rvvn90c86igrdma0h0hc7d44al1pss1my513rbjdykr2s")))

(define-public crate-gtmpl-0.3.0 (c (n "gtmpl") (v "0.3.0") (d (list (d (n "gtmpl_derive") (r "^0.2") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0.0") (d #t) (k 0)))) (h "0s6j6kb2ph1brigv2aycwzaap6vmfylri906vgkr4zzswb5l0qna")))

(define-public crate-gtmpl-0.3.1 (c (n "gtmpl") (v "0.3.1") (d (list (d (n "gtmpl_derive") (r "^0.2") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "1bvfn267fbhhg63sr3swdyv2lhgy05hjw60w91zly9aacx3w3xnr")))

(define-public crate-gtmpl-0.3.2 (c (n "gtmpl") (v "0.3.2") (d (list (d (n "gtmpl_derive") (r "^0.2") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "1dkr5ns03yg5s0crnq25r571jmx6xi40m67g3n6hlhck1w3x4s30")))

(define-public crate-gtmpl-0.3.3 (c (n "gtmpl") (v "0.3.3") (d (list (d (n "gtmpl_derive") (r "^0.2") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "005xvshm31p1kc6lvf40i44959s04y83b3hbvc5blb2vcf2vj3z6")))

(define-public crate-gtmpl-0.4.0 (c (n "gtmpl") (v "0.4.0") (d (list (d (n "gtmpl_derive") (r "^0.2") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "15zhw6pqn0al83cp39c6s7sj3carnfbv3jnj4mar1bxjwggcz1xg")))

(define-public crate-gtmpl-0.4.1 (c (n "gtmpl") (v "0.4.1") (d (list (d (n "gtmpl_derive") (r "^0.3") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "0p4z9xcfq2jamympr2mlh713qk350aawmni7ipqjvbz3qp9fniiv")))

(define-public crate-gtmpl-0.5.0 (c (n "gtmpl") (v "0.5.0") (d (list (d (n "gtmpl_derive") (r "^0.3") (d #t) (k 0)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "1rmp1nn9hl3v7xd6z5zh1x9gibl58n4if6iwd4xm0cmknv1x669b") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-gtmpl-0.5.1 (c (n "gtmpl") (v "0.5.1") (d (list (d (n "gtmpl_derive") (r "^0.3") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "10rcngz79y8bzrcrw14454dw8xbjw7d42gi5sv9bv9167qpzrgl5") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-gtmpl-0.5.2 (c (n "gtmpl") (v "0.5.2") (d (list (d (n "gtmpl_derive") (r "^0.3") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "09r6kq7cn459iv2wjy7p0jhh0gaihx5nnqnx47lhw9mghnv05v5j") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-gtmpl-0.5.3 (c (n "gtmpl") (v "0.5.3") (d (list (d (n "gtmpl_derive") (r "^0.3") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "01n7ynk2vm323wzg496j2x27vdv2v2qxh5kcfxapa4ny5bgy24ns") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-gtmpl-0.5.4 (c (n "gtmpl") (v "0.5.4") (d (list (d (n "gtmpl_derive") (r "^0.3") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "1yayq1dh9601naajmxj5j8vxkszy9lcr4n3x2pw8rwyrzfynvly5") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-gtmpl-0.5.5 (c (n "gtmpl") (v "0.5.5") (d (list (d (n "gtmpl_derive") (r "^0.3") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "0hfafcl6vsnw7szp7k145bcawmj1m4jh9fp03wvj3dgl3p0lgmqr") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-gtmpl-0.5.6 (c (n "gtmpl") (v "0.5.6") (d (list (d (n "gtmpl_derive") (r "^0.3") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "0218jzdh9m2493ykba10kkykd342wknd82hhjx3pizlsn43l3lrd") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-gtmpl-0.5.7 (c (n "gtmpl") (v "0.5.7") (d (list (d (n "gtmpl_derive") (r "^0.3") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^1.0") (d #t) (k 0)))) (h "05dg0k2pjmvm9zcz2sarc55lh517ph8hwg4xvr9ysyvrcbgmyh6q") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-gtmpl-0.6.0 (c (n "gtmpl") (v "0.6.0") (d (list (d (n "gtmpl_derive") (r "^0.4") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)))) (h "02mhkhs7368xsgw8579jp232zhq9x1qd8qhs2cird45jrsyn9g7q") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-gtmpl-0.7.0 (c (n "gtmpl") (v "0.7.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gtmpl_derive") (r "^0.5") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0q477mvm7xy3qvqmszxxqxxgk4rj1s0g3fc7ya88gnkzpq52cm8i") (f (quote (("gtmpl_dynamic_template"))))))

(define-public crate-gtmpl-0.7.1 (c (n "gtmpl") (v "0.7.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "gtmpl_derive") (r "^0.5") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1d52i1ln286yd62h844wnfsgydmhmjj372r0kg6db70ll474r4lv") (f (quote (("gtmpl_dynamic_template"))))))

