(define-module (crates-io gt mp gtmpl_derive) #:use-module (crates-io))

(define-public crate-gtmpl_derive-0.1.0 (c (n "gtmpl_derive") (v "0.1.0") (d (list (d (n "gtmpl_value") (r "^0.1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "09h1g106jhwgw9kc7fnxng650iaf1l6m4wfxnvw8rxbcmynpgdz6")))

(define-public crate-gtmpl_derive-0.2.0 (c (n "gtmpl_derive") (v "0.2.0") (d (list (d (n "gtmpl_value") (r "^0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1fzgq0rch3nsv3c31pl7a21dx7sg1l2kpvbk1nz0jhi44r1bw9mw")))

(define-public crate-gtmpl_derive-0.3.0 (c (n "gtmpl_derive") (v "0.3.0") (d (list (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^0.11.11") (d #t) (k 0)))) (h "1559nwy7sf17i5676ma71bfkpkjsmvvxkh1yqja6fh761s3s9a3j")))

(define-public crate-gtmpl_derive-0.3.1 (c (n "gtmpl_derive") (v "0.3.1") (d (list (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (d #t) (k 0)))) (h "06d9cpk0mk42l1ial1mkm379xdlcwmdlqnlgp3ks3v3ic0hwly07")))

(define-public crate-gtmpl_derive-0.3.2 (c (n "gtmpl_derive") (v "0.3.2") (d (list (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0afk76bhnkayi447l47b57z0md25g35hwhkd6ql3ygw0a8g6dscf")))

(define-public crate-gtmpl_derive-0.3.3 (c (n "gtmpl_derive") (v "0.3.3") (d (list (d (n "gtmpl_value") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "17r08whdcdpllzhgfq3jkjsnq26isgqcl6ds5ih8sqvnvb2m0f9r")))

(define-public crate-gtmpl_derive-0.4.0 (c (n "gtmpl_derive") (v "0.4.0") (d (list (d (n "gtmpl_value") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1bf99jadcdz1m6r8apmp69ndqxs6xz4906scxxv1jw9d0wi5yjwq")))

(define-public crate-gtmpl_derive-0.5.0 (c (n "gtmpl_derive") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "gtmpl_value") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0gxni93iln88mln0mw9gsbis1iy7szsm18aznfyrfi2pbf1jh5z6")))

