(define-module (crates-io gt mp gtmpl_value) #:use-module (crates-io))

(define-public crate-gtmpl_value-0.1.0 (c (n "gtmpl_value") (v "0.1.0") (h "1dl1rsgi4xrjd2pjy8xj7ix7s31msas951h84q96pp9jnqdascp4")))

(define-public crate-gtmpl_value-0.2.0 (c (n "gtmpl_value") (v "0.2.0") (h "1a2zdjqxh5r0yc1a7p2vy19xqdpypdflxgkig624m3vzh9sjisj1")))

(define-public crate-gtmpl_value-0.3.0 (c (n "gtmpl_value") (v "0.3.0") (h "13nxq1701j37avdk4lv4mg8pxsm4k55wjkq8fjdvgsc90qzlhnhb")))

(define-public crate-gtmpl_value-0.3.1 (c (n "gtmpl_value") (v "0.3.1") (h "051inaj8lnzhjc3m5amxxvr96mil3dm1kb74hc7m04p0rgg29pcq")))

(define-public crate-gtmpl_value-0.3.2 (c (n "gtmpl_value") (v "0.3.2") (h "0pk2yxywszbkkdhsmbb9xxxr30w6mwf4zck4ck8czm1y3bp7gy39")))

(define-public crate-gtmpl_value-0.3.3 (c (n "gtmpl_value") (v "0.3.3") (h "0hk8dday4rw511rrhjwilxrsrnvqmbg366j2yjbaj47vfwm3p6d1")))

(define-public crate-gtmpl_value-0.4.0 (c (n "gtmpl_value") (v "0.4.0") (h "1pkkv4vagxwccb4n2ic4i224hjqxawz6w51bzvyjdyczvmrvv5c9")))

(define-public crate-gtmpl_value-0.5.0 (c (n "gtmpl_value") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i1j9rmnj2781l6kxb46zkk37pqwphf6f2ch3llh3fsi2sr4vgpy")))

(define-public crate-gtmpl_value-0.5.1 (c (n "gtmpl_value") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1m9d2l1729xcar6fmgf8rh02ka0a9qcyvdkdxy8s9g7a0mkbyq8x")))

