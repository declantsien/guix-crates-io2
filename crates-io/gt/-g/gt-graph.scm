(define-module (crates-io gt -g gt-graph) #:use-module (crates-io))

(define-public crate-gt-graph-0.1.0 (c (n "gt-graph") (v "0.1.0") (h "05sq58gnfzb0cm4y7j7gixsnz8kf7431kaj20q56186v61096qgr")))

(define-public crate-gt-graph-0.2.0 (c (n "gt-graph") (v "0.2.0") (h "176z91mhxp5sqih2rpfnnqqxf540f229ih6ika9lixvi9sagsl63") (y #t)))

(define-public crate-gt-graph-0.1.1 (c (n "gt-graph") (v "0.1.1") (h "1vq4dh271g79kw2l9dm922nyyfhzmi11mbghkcs8rd2ji8h61l1s")))

