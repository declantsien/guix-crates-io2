(define-module (crates-io gt -l gt-ltq) #:use-module (crates-io))

(define-public crate-gt-ltq-0.1.0 (c (n "gt-ltq") (v "0.1.0") (d (list (d (n "gt-directed-bijective-connection-graph") (r "^0.2") (d #t) (k 0)) (d (n "gt-graph") (r "^0.1") (d #t) (k 0)))) (h "0fmq1nqdwpk6i5idcs27ww31jzmy2521ck5qh90ajhj8ldz8scsz")))

(define-public crate-gt-ltq-0.1.1 (c (n "gt-ltq") (v "0.1.1") (d (list (d (n "gt-directed-bijective-connection-graph") (r "^0.2") (d #t) (k 0)) (d (n "gt-graph") (r "^0.1") (d #t) (k 0)))) (h "1q1cm1dm1bdjhhkqsn8jly26fqza4m3712zfmmhpqr82a5a40pjd")))

