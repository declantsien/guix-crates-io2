(define-module (crates-io gt -h gt-hypercube) #:use-module (crates-io))

(define-public crate-gt-hypercube-0.1.0 (c (n "gt-hypercube") (v "0.1.0") (d (list (d (n "gt-directed-bijective-connection-graph") (r "^0.2.0") (d #t) (k 0)) (d (n "gt-graph") (r "^0.1") (d #t) (k 0)))) (h "14vnabv9bic6lg741x8z3pbal7ibsadq1fhsaw6rjc4hvda3zf69")))

(define-public crate-gt-hypercube-0.1.1 (c (n "gt-hypercube") (v "0.1.1") (d (list (d (n "gt-directed-bijective-connection-graph") (r "^0.2.0") (d #t) (k 0)) (d (n "gt-graph") (r "^0.1") (d #t) (k 0)))) (h "109n91iycnw05s2pn198hn02y8gk0xyxpsbh18p27l6xiqbjdf25")))

