(define-module (crates-io gt p- gtp-parser-generator) #:use-module (crates-io))

(define-public crate-gtp-parser-generator-0.1.0 (c (n "gtp-parser-generator") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "pom") (r "^1.0.1") (d #t) (k 0)))) (h "18r67lk60kj573s5nkrws8sp4pxk748lgfvk007hl64kll28p3pl")))

