(define-module (crates-io gt a- gta-ide-parser) #:use-module (crates-io))

(define-public crate-gta-ide-parser-0.0.1 (c (n "gta-ide-parser") (v "0.0.1") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "nom_locate") (r "^4.0.0") (d #t) (k 0)))) (h "1r2k2xypiqq32m7dpy58qv2mcrhh8hcqaxwysvg00km7xzqx5k3d")))

(define-public crate-gta-ide-parser-0.0.2 (c (n "gta-ide-parser") (v "0.0.2") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "1qwavw1nx9qjfn6sy4h0l487bq2nl7r4f9qwlq6f1l0q6jjnzfbd")))

(define-public crate-gta-ide-parser-0.0.3 (c (n "gta-ide-parser") (v "0.0.3") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0vpzymrl7mi98jx51bynzx3d3db7sqbkk5nz96mvylf757x2zawb")))

(define-public crate-gta-ide-parser-0.0.4 (c (n "gta-ide-parser") (v "0.0.4") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)))) (h "0n6wk6fwilhwzj7ggzqi18jb9amcm2jrs26ips95aqifv3zqm1f9")))

