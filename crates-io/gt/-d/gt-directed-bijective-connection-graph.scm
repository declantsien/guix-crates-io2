(define-module (crates-io gt -d gt-directed-bijective-connection-graph) #:use-module (crates-io))

(define-public crate-gt-directed-bijective-connection-graph-0.2.1 (c (n "gt-directed-bijective-connection-graph") (v "0.2.1") (d (list (d (n "gt-graph") (r "^0.1") (d #t) (k 0)) (d (n "gt-graph-path") (r "^0.1") (d #t) (k 0)))) (h "1ckxn9vkif5blva7p0abjf3bxhiwvv8r9i2vphqis8zlv4ikcygj")))

(define-public crate-gt-directed-bijective-connection-graph-0.3.0 (c (n "gt-directed-bijective-connection-graph") (v "0.3.0") (d (list (d (n "gt-graph") (r "^0.2") (d #t) (k 0)) (d (n "gt-graph-path") (r "^0.2") (d #t) (k 0)))) (h "1hmhkiglvn31lwdwq012yzvy8zcmhgr7dipcc6c31fsf5fwkpzr4") (y #t)))

(define-public crate-gt-directed-bijective-connection-graph-0.2.2 (c (n "gt-directed-bijective-connection-graph") (v "0.2.2") (d (list (d (n "gt-graph") (r "^0.1") (d #t) (k 0)) (d (n "gt-graph-path") (r "^0.1") (d #t) (k 0)))) (h "18pg78vdgqhccav3aak92q4i9in3gq2659qi5gcilnfxbw8l5x99")))

(define-public crate-gt-directed-bijective-connection-graph-0.2.3 (c (n "gt-directed-bijective-connection-graph") (v "0.2.3") (d (list (d (n "gt-graph") (r "^0.1") (d #t) (k 0)) (d (n "gt-graph-path") (r "^0.1") (d #t) (k 0)))) (h "17j4zzx43lhq8a1f22g7pmpm8d972axjm6w1xfp15m6lgh9j256w")))

