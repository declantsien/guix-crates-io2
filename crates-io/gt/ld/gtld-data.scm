(define-module (crates-io gt ld gtld-data) #:use-module (crates-io))

(define-public crate-gtld-data-0.2.1 (c (n "gtld-data") (v "0.2.1") (h "010cvcicajw31l7fpkaq5nd00kyrkajawzbvmp3zlb6q0rlhaxk1")))

(define-public crate-gtld-data-0.2.2 (c (n "gtld-data") (v "0.2.2") (h "1bs15h2lrnqlffpir4vi3mybi2xp7nszd5555j1p409ppsz194nf")))

(define-public crate-gtld-data-0.2.3 (c (n "gtld-data") (v "0.2.3") (h "1lsn80vabj5z2hmwl2bypf23dvd3zqigxzvlxdbrwp833awad9g5")))

(define-public crate-gtld-data-0.2.4 (c (n "gtld-data") (v "0.2.4") (h "152y8m12bqr54hnn2ykw0zy1daqzx4xsm2ccrjfk4zi3d8kqp1v3")))

(define-public crate-gtld-data-0.3.0 (c (n "gtld-data") (v "0.3.0") (h "17hcwaqmq79jlsh80jzjalq8kj2myq9ac7hnzyqlb23g4h2i03hy")))

(define-public crate-gtld-data-0.3.1 (c (n "gtld-data") (v "0.3.1") (h "1i86yxizcjj8krhlxmibz1bpngsqz1nwkjc0yfgsagc8ar83ndc8")))

(define-public crate-gtld-data-0.3.3 (c (n "gtld-data") (v "0.3.3") (h "0jaigrn906g4ki2dmllaimm81nl1c8wjbj9g848xbpfajcijq1zz")))

(define-public crate-gtld-data-0.3.4 (c (n "gtld-data") (v "0.3.4") (h "1i59smq0nblr3vqiajy64xjbhx40q9vz1fw1p0f4irczfqbcwprm")))

(define-public crate-gtld-data-0.3.5 (c (n "gtld-data") (v "0.3.5") (h "1rlv3g5qxykrq0v11ir0kjmcb833wi8y781w6rzynp3pi64r2ln1")))

(define-public crate-gtld-data-0.3.6 (c (n "gtld-data") (v "0.3.6") (h "1579j8z14v50c1g1jmgdhcqjyjxcrdyjhmz0fw1lygjdik8haf1a")))

(define-public crate-gtld-data-0.3.7 (c (n "gtld-data") (v "0.3.7") (h "1pdama0gxv6w6rvcxh4j1kcl275dknjav093wy3h1w5ircdk74xm")))

(define-public crate-gtld-data-0.3.9 (c (n "gtld-data") (v "0.3.9") (h "0ba61g6li7dbl0pnjsj8anz6jk08scsbhjnh50xlb6m6nsizmfpi")))

(define-public crate-gtld-data-0.3.10 (c (n "gtld-data") (v "0.3.10") (h "0wf86nwl1jkxnsccmzrqsidv23h6ayiczjq4s5brskhsagzrxzlc")))

(define-public crate-gtld-data-0.3.11 (c (n "gtld-data") (v "0.3.11") (h "17960k5bwv1wc2krrq79kifnhlv2bbgzkvs7h6xj4x5bf1wch4bm")))

(define-public crate-gtld-data-0.3.12 (c (n "gtld-data") (v "0.3.12") (h "0fki8fhl39z2rn1jd398q613v1xv8gjswjisiy8kbyzg0vbxs4mc")))

(define-public crate-gtld-data-0.4.0 (c (n "gtld-data") (v "0.4.0") (d (list (d (n "curl") (r "^0.2") (d #t) (k 1)) (d (n "select") (r "^0.3") (d #t) (k 1)))) (h "135fbn6fxqihrn5f7fld044jb0aixrkljd0kp8h97pkr73xj5w96")))

(define-public crate-gtld-data-0.4.1 (c (n "gtld-data") (v "0.4.1") (d (list (d (n "curl") (r "^0.2") (d #t) (k 1)) (d (n "select") (r "^0.3") (d #t) (k 1)))) (h "0h1q132wm9cp85wy5z8qmd7jb124snidsfjldpi8wr93iw73wkh8")))

