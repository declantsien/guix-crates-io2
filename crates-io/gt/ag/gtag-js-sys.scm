(define-module (crates-io gt ag gtag-js-sys) #:use-module (crates-io))

(define-public crate-gtag-js-sys-0.1.0 (c (n "gtag-js-sys") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.58") (d #t) (k 0)))) (h "0zpixndngiv2mhai3nxr9lhjxmzi373kfmbcqxfrsyiz6kmmzw2h")))

(define-public crate-gtag-js-sys-0.2.0 (c (n "gtag-js-sys") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2.58") (d #t) (k 0)))) (h "0zkv0ffazlqs7li3msw44cr6swdgxjwl902cvlbkarkg1rh8nccj")))

