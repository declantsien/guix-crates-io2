(define-module (crates-io gt ag gtag-js) #:use-module (crates-io))

(define-public crate-gtag-js-0.2.0 (c (n "gtag-js") (v "0.2.0") (d (list (d (n "gtag-js-sys") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.58") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "0rxqikfpamgrqm330rbiffss4qgjzavv6np3sw55hln1pb7cmvwx")))

