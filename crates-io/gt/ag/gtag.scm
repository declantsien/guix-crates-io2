(define-module (crates-io gt ag gtag) #:use-module (crates-io))

(define-public crate-gtag-0.1.0 (c (n "gtag") (v "0.1.0") (d (list (d (n "clap") (r "~1.5") (d #t) (k 0)))) (h "1r9r7nf5p207wkjwy1y04nn0hy6yzb0qb4z0j93x4v54njccbia1")))

(define-public crate-gtag-0.2.0 (c (n "gtag") (v "0.2.0") (d (list (d (n "clap") (r "~1.5") (d #t) (k 0)))) (h "1yybzh63bnjb0hlfj8yy2mi46cqingkaybyhb25fcvd472km0ca9")))

(define-public crate-gtag-0.2.1 (c (n "gtag") (v "0.2.1") (d (list (d (n "clap") (r "~1.5") (d #t) (k 0)))) (h "15d19nf0qb5qzy17jmjzlhffq77c1vyy5lsn4amwj5a7xk7apmsh")))

(define-public crate-gtag-0.3.0 (c (n "gtag") (v "0.3.0") (d (list (d (n "clap") (r "~1.5") (d #t) (k 0)))) (h "01icil5hhwajq0d9lip28bbdm58q2k1rsvflanijcrph0vaw2qkg")))

