(define-module (crates-io gt ag gtag-rs) #:use-module (crates-io))

(define-public crate-gtag-rs-0.1.0 (c (n "gtag-rs") (v "0.1.0") (d (list (d (n "gtag-js-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.58") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1pjjdxb0i6brsxjk67n2wfma4iqjl3ljhmf6br35h33127jp8haw") (y #t)))

