(define-module (crates-io gt ts gtts) #:use-module (crates-io))

(define-public crate-gtts-0.1.0 (c (n "gtts") (v "0.1.0") (d (list (d (n "minreq") (r "^2.0.3") (f (quote ("https"))) (d #t) (k 0)) (d (n "percent-encoding") (r "^2.1.0") (d #t) (k 0)))) (h "0zpdkq996jd9vngr7bqb2f4zzr24l5a5g3rqwnngkwhvawqsp1gp")))

