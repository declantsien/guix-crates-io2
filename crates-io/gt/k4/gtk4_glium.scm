(define-module (crates-io gt k4 gtk4_glium) #:use-module (crates-io))

(define-public crate-gtk4_glium-0.1.0 (c (n "gtk4_glium") (v "0.1.0") (d (list (d (n "gl_loader") (r "^0.1.2") (d #t) (k 0)) (d (n "glib") (r "^0.14.8") (d #t) (k 0)) (d (n "glium") (r "^0.31.0") (d #t) (k 0)) (d (n "gtk4") (r "^0.3.1") (d #t) (k 0)))) (h "02i46y2dnx8l97lyy8glj6hdbsarfv37cibwgsy67axcxnky32k1")))

(define-public crate-gtk4_glium-0.1.1 (c (n "gtk4_glium") (v "0.1.1") (d (list (d (n "gl_loader") (r "^0.1.2") (d #t) (k 0)) (d (n "glib") (r "^0.15.0") (d #t) (k 0)) (d (n "glium") (r "^0.31.0") (d #t) (k 0)) (d (n "gtk4") (r "^0.4.0") (d #t) (k 0)))) (h "0ag5rrz6lsbx4j3dz7flvb3f7csa3n6g036kz4ss0hi3rhk6kxmm")))

(define-public crate-gtk4_glium-0.2.0 (c (n "gtk4_glium") (v "0.2.0") (d (list (d (n "gl_loader") (r "^0.1.2") (d #t) (k 0)) (d (n "glib") (r "^0.18.0") (d #t) (k 0)) (d (n "glium") (r "^0.33.0") (d #t) (k 0)) (d (n "gtk4") (r "^0.6.0") (d #t) (k 0)))) (h "0452hdi0cxqza43pvn8acqf45fz2gvzllm4hgbg9nsvnh0gk1z3v")))

(define-public crate-gtk4_glium-0.3.0 (c (n "gtk4_glium") (v "0.3.0") (d (list (d (n "gl_loader") (r "^0.1.2") (d #t) (k 0)) (d (n "glib") (r "^0.18.0") (d #t) (k 0)) (d (n "glium") (r "^0.33.0") (d #t) (k 0)) (d (n "gtk4") (r "^0.7.0") (d #t) (k 0)))) (h "049vy7842g8cvbq26mbbdmhww9g5zr80fi40hh4pb3rn777sx4w2")))

(define-public crate-gtk4_glium-0.4.0 (c (n "gtk4_glium") (v "0.4.0") (d (list (d (n "gl_loader") (r "^0.1.2") (d #t) (k 0)) (d (n "glib") (r "^0.19.0") (d #t) (k 0)) (d (n "glium") (r "^0.34.0") (d #t) (k 0)) (d (n "gtk4") (r "^0.8.0") (d #t) (k 0)))) (h "0vn1ahnvrh7sjl9z8102pnayy50s4pk22agjivbdjhxh2z45v5lb")))

