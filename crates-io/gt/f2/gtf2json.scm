(define-module (crates-io gt f2 gtf2json) #:use-module (crates-io))

(define-public crate-gtf2json-0.1.0 (c (n "gtf2json") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gtftools") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0r6rhw9fph1xhppi3pn7cv72g7in3alc1svali1cdy5v1nr0k3lk") (y #t)))

(define-public crate-gtf2json-0.1.1 (c (n "gtf2json") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gtftools") (r "^0.1.8") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0c30sc99jq1d0n44jb1xbv1dznrdr4n2m4l0lzqz8fidcp84nisp") (y #t)))

(define-public crate-gtf2json-0.1.2 (c (n "gtf2json") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "gtftools") (r "^0.1.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)))) (h "0qm0qy1zjbdzdz3i0q16fxf57bzis693x97fmkyyszcgf2r9bd3r") (y #t)))

