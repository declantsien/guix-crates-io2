(define-module (crates-io gt fs gtfs-rt) #:use-module (crates-io))

(define-public crate-gtfs-rt-0.1.0 (c (n "gtfs-rt") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.4") (d #t) (k 0)) (d (n "prost-build") (r "^0.4") (d #t) (k 1)) (d (n "prost-derive") (r "^0.4") (d #t) (k 0)))) (h "05x98pn8pb0284w8nifs6c0hryxhwm6x8bk0kb13msv4fhzyhqw3")))

(define-public crate-gtfs-rt-0.2.0 (c (n "gtfs-rt") (v "0.2.0") (d (list (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-build") (r "^0.4") (d #t) (k 1)) (d (n "prost-derive") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ffacighv1nrxhzlpx6r1yfncvh19gqpa25hf66nhy5wfy189jh6") (y #t)))

(define-public crate-gtfs-rt-0.2.1 (c (n "gtfs-rt") (v "0.2.1") (d (list (d (n "prost") (r "^0.8") (d #t) (k 0)) (d (n "prost-build") (r "^0.8") (d #t) (k 1)) (d (n "prost-derive") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nchh2zxi1s45k1dm853d58rgx3f8fqf6rwplsdahi3b68bf525k")))

(define-public crate-gtfs-rt-0.3.0 (c (n "gtfs-rt") (v "0.3.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-derive") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mnlrjb1lrph54rwbsrij4c3xqvj259algmnlz9mvg6v61r331mx")))

(define-public crate-gtfs-rt-0.4.0 (c (n "gtfs-rt") (v "0.4.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-derive") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b6l4ak5jy28jflxwymh9mfvjrn7w6kmc0wjdl7lyvz89jrvjbkv")))

(define-public crate-gtfs-rt-0.5.0 (c (n "gtfs-rt") (v "0.5.0") (d (list (d (n "prost") (r "^0.11") (d #t) (k 0)) (d (n "prost-build") (r "^0.11") (d #t) (k 1)) (d (n "prost-derive") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mb2703k6jn78rs6dw2hpxyhp7lyigmpx6rrlh84jrla9lgg2jm3")))

