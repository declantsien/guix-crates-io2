(define-module (crates-io gt fs gtfs-translations) #:use-module (crates-io))

(define-public crate-gtfs-translations-0.1.0 (c (n "gtfs-translations") (v "0.1.0") (d (list (d (n "csv") (r "^1.3.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "gtfs-structures") (r "^0.41.0") (d #t) (k 0)) (d (n "language-tags") (r "^0.3.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36") (f (quote ("full"))) (d #t) (k 2)) (d (n "zip") (r "^0.6.6") (d #t) (k 2)))) (h "0ixg1cj8k8imbydhh6h37g9vfxd3i2n47c6a6m2v8hpybsd0lppb")))

