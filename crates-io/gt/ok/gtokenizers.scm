(define-module (crates-io gt ok gtokenizers) #:use-module (crates-io))

(define-public crate-gtokenizers-0.0.11 (c (n "gtokenizers") (v "0.0.11") (d (list (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)))) (h "0bpdid5vdahzh16fzswpnv962xjm0gwm1w4nk3am5hfanjpx7632")))

(define-public crate-gtokenizers-0.0.12 (c (n "gtokenizers") (v "0.0.12") (d (list (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)))) (h "05njznfcllaqpkky7y2kpgafzkgsa8ylwbkgbypyzklvaqs894cp")))

(define-public crate-gtokenizers-0.0.13 (c (n "gtokenizers") (v "0.0.13") (d (list (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)))) (h "0hi8pnzfb5svg077gzl4yxjzm1gpq1ks47nljjyhs08m99qra0ys")))

(define-public crate-gtokenizers-0.0.14 (c (n "gtokenizers") (v "0.0.14") (d (list (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)))) (h "0kxv0343wbg16drznvg89090b6b5rhxv5lbggfzf382ihh5jgq5r")))

(define-public crate-gtokenizers-0.0.15 (c (n "gtokenizers") (v "0.0.15") (d (list (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)))) (h "07zk8a7zbq7a2r64i9vg31aqw912q76pq7y5lab7fqkkmz3kkfqs")))

(define-public crate-gtokenizers-0.0.17 (c (n "gtokenizers") (v "0.0.17") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)))) (h "0bc991aql65da3anl5wl3jrdxd2h2y8ag429yrwnbm4jj312swvv")))

(define-public crate-gtokenizers-0.0.18 (c (n "gtokenizers") (v "0.0.18") (d (list (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "rust-lapper") (r "^1.1.0") (d #t) (k 0)))) (h "15mqsh6pmkzbfzy9fx1f4ayfz5vdiri53k2qnrc8ckshkfyv8p69")))

