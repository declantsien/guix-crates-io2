(define-module (crates-io gt k- gtk-rs-state) #:use-module (crates-io))

(define-public crate-gtk-rs-state-0.3.0 (c (n "gtk-rs-state") (v "0.3.0") (d (list (d (n "glib") (r "^0.9") (d #t) (k 0)) (d (n "gtk") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1ya0md6klvdqqaqyhi8ylf74syrswm3yx6vplbhkzf27nwbjavxy")))

(define-public crate-gtk-rs-state-0.4.0 (c (n "gtk-rs-state") (v "0.4.0") (d (list (d (n "glib") (r ">=0.10.0, <0.11.0") (d #t) (k 0)) (d (n "gtk") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "0y0dw2n5p990p3phg8qaj0yqllaxi78npqxf93q46idwa3k6i3qx")))

(define-public crate-gtk-rs-state-0.4.1 (c (n "gtk-rs-state") (v "0.4.1") (d (list (d (n "glib") (r "^0.10") (d #t) (k 0)) (d (n "gtk") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0fqjq0gvhvl0fysdy24hhw2m5r9b8mnq7wc14qc7k14w0spqxbi5")))

