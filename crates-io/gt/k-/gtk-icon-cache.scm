(define-module (crates-io gt k- gtk-icon-cache) #:use-module (crates-io))

(define-public crate-gtk-icon-cache-0.1.0 (c (n "gtk-icon-cache") (v "0.1.0") (h "1l2xfw6ybb8hp95nqf558q23znrsd6lyd1j2qvxqk8yj4y45ipak")))

(define-public crate-gtk-icon-cache-0.1.1 (c (n "gtk-icon-cache") (v "0.1.1") (h "0iq53gwsnx0dn2almc1qxx4krdsnvpd9g362x3lj8ngsmascvdbk")))

(define-public crate-gtk-icon-cache-0.2.0 (c (n "gtk-icon-cache") (v "0.2.0") (d (list (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "0fxqiimz3sxsl7aq9wv2igwfnn2fkx4f5ddg6zs38y4a8k4z4dnf")))

(define-public crate-gtk-icon-cache-0.2.1 (c (n "gtk-icon-cache") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "18mccq2ad3l8ksdvijx6fcd2ym4q6kc9j1mx31q269ql46lp9cdf")))

(define-public crate-gtk-icon-cache-0.2.2 (c (n "gtk-icon-cache") (v "0.2.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.6") (d #t) (k 0)))) (h "0q5d0p49wpjvn1izwgdsj4j1p8rmb73mmvwys3v88bdyaz4v4kcx")))

