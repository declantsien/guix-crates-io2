(define-module (crates-io gt k- gtk-properties-macro) #:use-module (crates-io))

(define-public crate-gtk-properties-macro-0.1.0 (c (n "gtk-properties-macro") (v "0.1.0") (d (list (d (n "cargo-expand") (r "^1.0") (d #t) (k 2)) (d (n "gtk") (r "^0.4.8") (d #t) (k 2) (p "gtk4")) (d (n "macrotest") (r "^1.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "11833520z6i35dpmgsr91qyz799q4hv260m7zyp6b5c9nsz0gysw")))

