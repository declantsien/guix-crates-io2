(define-module (crates-io gt k- gtk-ui-builder) #:use-module (crates-io))

(define-public crate-gtk-ui-builder-0.1.0 (c (n "gtk-ui-builder") (v "0.1.0") (h "0dc2r98w4yxn7bfp554fb36kk4asgrvj5yimqx6fknw85wv72ksh")))

(define-public crate-gtk-ui-builder-0.1.1 (c (n "gtk-ui-builder") (v "0.1.1") (d (list (d (n "gtk4") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0zw3br29l4c5nl0ik8zzs6gz8w3pfnpik8hnfr8v2hnp1mlc03iy") (s 2) (e (quote (("gtk-builder" "dep:gtk4"))))))

(define-public crate-gtk-ui-builder-0.2.0 (c (n "gtk-ui-builder") (v "0.2.0") (d (list (d (n "gtk4") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rhai") (r "^1.8.0") (o #t) (d #t) (k 0)))) (h "110kc84hg9cic5clcgh2ycxylwgz9yh6l5vbn8cwzhfmni7cxcsb") (s 2) (e (quote (("rhai-events" "dep:rhai") ("gtk-builder" "dep:gtk4"))))))

