(define-module (crates-io gt k- gtk-rust-app-derive) #:use-module (crates-io))

(define-public crate-gtk-rust-app-derive-0.0.5 (c (n "gtk-rust-app-derive") (v "0.0.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "01i2xn3dp9k7gkf3sdcy7nqwklk2qlmlsmzjasag2b2qzj4yb1wr")))

(define-public crate-gtk-rust-app-derive-0.0.6 (c (n "gtk-rust-app-derive") (v "0.0.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0c6ii97qr4cgmx66id30n4r7cafdwf76na6hv0ml5mj5amyrdi26")))

(define-public crate-gtk-rust-app-derive-0.0.7 (c (n "gtk-rust-app-derive") (v "0.0.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0azs736caa849hl07s32imvmbi2k88vfa363z4az7az2kh34x3c8")))

(define-public crate-gtk-rust-app-derive-0.1.0-adw1 (c (n "gtk-rust-app-derive") (v "0.1.0-adw1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1d646p5rpvsq9dbrgn6syw8smxnys96jcvlakjm19xa7h9mc3xjr")))

(define-public crate-gtk-rust-app-derive-0.1.1 (c (n "gtk-rust-app-derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "086zqywf7cq6qd5y2qfgh2j2jv6akk7b6bld2x7q9gcmvdgzipgy")))

(define-public crate-gtk-rust-app-derive-0.1.2 (c (n "gtk-rust-app-derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0kkb3m1z3rk1dxjp9y31ycn23md7bv0kkvqc2nhgbjmvpvb71zlg")))

(define-public crate-gtk-rust-app-derive-0.1.3 (c (n "gtk-rust-app-derive") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1q9mkq3xdhiilwy6r5kjjifxr87a1jqvjvmi8yddc8kjscywwkrr")))

(define-public crate-gtk-rust-app-derive-0.1.4 (c (n "gtk-rust-app-derive") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "09v35bl9m4klxx22ksykcpsfh49dzpzjgiic67nivkrlms3p6z6p")))

(define-public crate-gtk-rust-app-derive-0.1.5 (c (n "gtk-rust-app-derive") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1blyjz6b26rzmmr1s4vs6i9f1c9l21llvyax99cch5nk93kb3dls")))

(define-public crate-gtk-rust-app-derive-0.2.0 (c (n "gtk-rust-app-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0213xgzs1c6jcw6501cf4g4gkwq2ma2m8nlzzfm9736arscgc78b")))

(define-public crate-gtk-rust-app-derive-0.2.1 (c (n "gtk-rust-app-derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1q54vif73c7dnkhyz9ks4fy65h0adiaywzmgkqxfk3cb1kly25rj")))

(define-public crate-gtk-rust-app-derive-0.2.2 (c (n "gtk-rust-app-derive") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1knzvda7kdhvxbqh49iqcmy7is90c57z7wjh47f8p0gjwhgcy5fw") (y #t)))

(define-public crate-gtk-rust-app-derive-0.2.3 (c (n "gtk-rust-app-derive") (v "0.2.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1dlbnghvr6dg1h9c4ls7pb3s7bphvrkl9qybma4hjayhnsawr3g1") (y #t)))

(define-public crate-gtk-rust-app-derive-0.2.4 (c (n "gtk-rust-app-derive") (v "0.2.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1pkslq73gg72bd2qmmgaz0adw0mmqby4wmq35snmrnm4hn3j5f8d")))

(define-public crate-gtk-rust-app-derive-0.2.5 (c (n "gtk-rust-app-derive") (v "0.2.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0in8agk3487hf6hjaz59vx6w731pnmjb8g94syblpfgjsgf38d5p")))

(define-public crate-gtk-rust-app-derive-0.2.6 (c (n "gtk-rust-app-derive") (v "0.2.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "08kcixh5n4h6w88krvyk4vbmy1flini987sibp0ylg5ax8qnv29x")))

(define-public crate-gtk-rust-app-derive-0.2.7 (c (n "gtk-rust-app-derive") (v "0.2.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "03f4ig77y07mkh3j5nsgp806b53lgrf1inhqq1x1ljxj1qpqbcbn")))

(define-public crate-gtk-rust-app-derive-0.2.8 (c (n "gtk-rust-app-derive") (v "0.2.8") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1b8sizzxxpwr0bvb6vypdia8vn30qjzxziqh7sn7acbsijjz271n")))

