(define-module (crates-io gt k- gtk-blueprint) #:use-module (crates-io))

(define-public crate-gtk-blueprint-0.1.0 (c (n "gtk-blueprint") (v "0.1.0") (d (list (d (n "syn") (r "^1.0.100") (d #t) (k 0)))) (h "0n8116ybd8m5sjzc746fkx1lpf4xs5za0miw9k9jqhs2zipizw4h")))

(define-public crate-gtk-blueprint-0.1.1 (c (n "gtk-blueprint") (v "0.1.1") (d (list (d (n "syn") (r "^1.0.100") (d #t) (k 0)))) (h "0zxksq6xpnrrxj319rs9p3z3gb76223niy7yk5l7dh7zmx6sbhda")))

(define-public crate-gtk-blueprint-0.2.0 (c (n "gtk-blueprint") (v "0.2.0") (d (list (d (n "phf_codegen") (r "^0.11.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (d #t) (k 0)))) (h "0ywvw4a9j07a74w59p1m6v5i0irdax93mkq54ric9j3c8kmvlq9v")))

