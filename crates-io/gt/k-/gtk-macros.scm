(define-module (crates-io gt k- gtk-macros) #:use-module (crates-io))

(define-public crate-gtk-macros-0.1.0 (c (n "gtk-macros") (v "0.1.0") (h "1bnl9md1zqil8nd65jk14vbja0mffphhs17xq1jwymrv51bjs5wg")))

(define-public crate-gtk-macros-0.2.0 (c (n "gtk-macros") (v "0.2.0") (h "0qrskjijcxr3xwi8gchq5p5ygpd611n91b4k40cww685cy7c8x0q")))

(define-public crate-gtk-macros-0.3.0 (c (n "gtk-macros") (v "0.3.0") (h "0nny573sjabcp4qrbkjfxv5xn28qj76fp3fzj0j0pkflixsgfnys")))

