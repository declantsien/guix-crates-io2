(define-module (crates-io gt k- gtk-test) #:use-module (crates-io))

(define-public crate-gtk-test-0.1.0 (c (n "gtk-test") (v "0.1.0") (d (list (d (n "enigo") (r "^0.0.11") (d #t) (k 0)) (d (n "gdk") (r "^0.8.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)))) (h "1iv16kr04zv2si6dg5l8s0hihz1xnh5kfxbgr2isa2861ply2xyx") (f (quote (("default" "gtk/v3_10"))))))

(define-public crate-gtk-test-0.2.0 (c (n "gtk-test") (v "0.2.0") (d (list (d (n "enigo") (r "^0.0.11") (d #t) (k 0)) (d (n "gdk") (r "^0.9.0") (d #t) (k 0)) (d (n "gtk") (r "^0.5.0") (d #t) (k 0)))) (h "19bd8xvak9advph3m1aic8j64d40kqbp8gd4dpyyf9gafxwkcrr5") (f (quote (("default" "gtk/v3_10"))))))

(define-public crate-gtk-test-0.3.0 (c (n "gtk-test") (v "0.3.0") (d (list (d (n "enigo") (r "^0.0.11") (d #t) (k 0)) (d (n "gdk") (r "^0.10.0") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (d #t) (k 0)))) (h "1v6q1zd40ax6882bp74n43bhffsbipn0q8g2hmvzfvly2x65zs0b")))

(define-public crate-gtk-test-0.4.0 (c (n "gtk-test") (v "0.4.0") (d (list (d (n "enigo") (r "^0.0.11") (d #t) (k 0)) (d (n "gdk") (r "^0.11.0") (d #t) (k 0)) (d (n "gtk") (r "^0.7.0") (d #t) (k 0)))) (h "1c9kcgnkmf434nm60xqa9awafq12q9p4g6df0b4qmlzjb8f5kq6b")))

(define-public crate-gtk-test-0.5.0 (c (n "gtk-test") (v "0.5.0") (d (list (d (n "enigo") (r "^0.0.11") (d #t) (k 0)) (d (n "gdk") (r "^0.12.0") (d #t) (k 0)) (d (n "glib") (r "^0.9.0") (d #t) (k 0)) (d (n "gtk") (r "^0.8.0") (d #t) (k 0)))) (h "0c58yxs4k3ddirss0p5k9vhp3fvvzrm1nif2gllly23sjiqfj19w")))

(define-public crate-gtk-test-0.6.0 (c (n "gtk-test") (v "0.6.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gdk") (r "^0.13.0") (d #t) (k 0)) (d (n "glib") (r "^0.10.0") (d #t) (k 0)) (d (n "gtk") (r "^0.9.0") (d #t) (k 0)))) (h "0995in8rryvwjfn8ljlcp9ydzqz8akwfzcpk5p028nq07ymsxvli")))

(define-public crate-gtk-test-0.6.1 (c (n "gtk-test") (v "0.6.1") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gdk") (r "^0.13.0") (d #t) (k 0)) (d (n "glib") (r "^0.10.0") (d #t) (k 0)) (d (n "gtk") (r "^0.9.0") (d #t) (k 0)))) (h "0x856fxkff4yc7f0qgfcsjv9b6p2s6is016rzzvgiv2pgzzyzhmm")))

(define-public crate-gtk-test-0.7.0 (c (n "gtk-test") (v "0.7.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gdk") (r "^0.14.0") (d #t) (k 0)) (d (n "glib") (r "^0.14.0") (d #t) (k 0)) (d (n "gtk") (r "^0.14.0") (d #t) (k 0)))) (h "029yn7gj2lr9za6h3qsg67p3rb6c16dqn08fll3yi44zwa79xvd2")))

(define-public crate-gtk-test-0.14.0 (c (n "gtk-test") (v "0.14.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.14") (d #t) (k 0)))) (h "0i9cn9a151zd1v8hvbqiwv2gxvcpb1hynfc660lk7z82g30vprwk")))

(define-public crate-gtk-test-0.15.0 (c (n "gtk-test") (v "0.15.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.15") (d #t) (k 0)))) (h "0xxa9rfkgmzc31kdm6n867wl2s51ipm7hyvg4hfpi2v0sphdfj37")))

(define-public crate-gtk-test-0.16.0 (c (n "gtk-test") (v "0.16.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.16") (d #t) (k 0)))) (h "126108n86c3iivbav91shgciivhv21ard72n9wr0jv5v05p44qjy")))

(define-public crate-gtk-test-0.17.0 (c (n "gtk-test") (v "0.17.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.17") (d #t) (k 0)))) (h "1cbmg21yqbyrln8i62kgln9522lp15d1jzaqh1j1ddlay1z63r08")))

(define-public crate-gtk-test-0.18.0 (c (n "gtk-test") (v "0.18.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.18") (d #t) (k 0)))) (h "1apz6mvg2xq9jrmw4d28hfz33l27yd3jy0sm8jkmz1y02f76ljgr")))

