(define-module (crates-io gt k- gtk-future-executor) #:use-module (crates-io))

(define-public crate-gtk-future-executor-0.1.0 (c (n "gtk-future-executor") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.27") (d #t) (k 0)) (d (n "futures-cpupool") (r "^0.1.8") (d #t) (k 2)) (d (n "glib") (r "^0.7.1") (d #t) (k 0)) (d (n "gtk") (r "^0.6.0") (d #t) (k 0)))) (h "0znay8ry6wdrhn6gcg4x3a8ydrfa1nq3vmmgvpafzgw35gsfmjr9")))

