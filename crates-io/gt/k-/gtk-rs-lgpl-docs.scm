(define-module (crates-io gt k- gtk-rs-lgpl-docs) #:use-module (crates-io))

(define-public crate-gtk-rs-lgpl-docs-0.0.1 (c (n "gtk-rs-lgpl-docs") (v "0.0.1") (d (list (d (n "rustdoc-stripper") (r "^0.1.0") (d #t) (k 0)))) (h "030gq5i3gpwnbw0ly7xfqmmj5af57ljc6m81kapm2jzspfwyadla")))

(define-public crate-gtk-rs-lgpl-docs-0.1.0 (c (n "gtk-rs-lgpl-docs") (v "0.1.0") (d (list (d (n "rustdoc-stripper") (r "^0.1.2") (d #t) (k 0)))) (h "1y8w1xy9xg8njb8h4xi25ylby09yz88z1zkk2bvizbjfsycsly3q")))

(define-public crate-gtk-rs-lgpl-docs-0.1.1 (c (n "gtk-rs-lgpl-docs") (v "0.1.1") (d (list (d (n "rustdoc-stripper") (r "^0.1.2") (d #t) (k 0)))) (h "0gkhj4jb2ivl969064jw4mbprnavbp03lvz6i3zqxf851fxqk9bf")))

(define-public crate-gtk-rs-lgpl-docs-0.1.2 (c (n "gtk-rs-lgpl-docs") (v "0.1.2") (d (list (d (n "rustdoc-stripper") (r "^0.1.2") (d #t) (k 0)))) (h "1v2v81d8j6ca722azw0rzyw24a2rhc84w1pmmj23ylks403pz89f")))

(define-public crate-gtk-rs-lgpl-docs-0.1.3 (c (n "gtk-rs-lgpl-docs") (v "0.1.3") (d (list (d (n "rustdoc-stripper") (r "^0.1.2") (d #t) (k 0)))) (h "1j7chkmals2kc7phvrqfyf51gbhzhmn988d4gp0qjnpkj5g1q471")))

(define-public crate-gtk-rs-lgpl-docs-0.1.4 (c (n "gtk-rs-lgpl-docs") (v "0.1.4") (d (list (d (n "rustdoc-stripper") (r "^0.1.5") (d #t) (k 0)))) (h "0l3jac3c88y1msynycjcs55ajqlpymmnpvpc2wmd8qryn1rls81i")))

(define-public crate-gtk-rs-lgpl-docs-0.1.5 (c (n "gtk-rs-lgpl-docs") (v "0.1.5") (d (list (d (n "rustdoc-stripper") (r "^0.1.5") (d #t) (k 0)))) (h "0bx307z52m6ipzccsdywi8a2316gm3y6x9mbzmia7cj67z8p5kj3")))

(define-public crate-gtk-rs-lgpl-docs-0.1.6 (c (n "gtk-rs-lgpl-docs") (v "0.1.6") (d (list (d (n "rustdoc-stripper") (r "^0.1.5") (d #t) (k 0)))) (h "1vm95irmnzdn0li5ql4nix9nl583f6mvl3lw0g8hxbv4nn3g1z9g")))

(define-public crate-gtk-rs-lgpl-docs-0.1.7 (c (n "gtk-rs-lgpl-docs") (v "0.1.7") (d (list (d (n "rustdoc-stripper") (r "^0.1.5") (d #t) (k 0)))) (h "1b0mif1k018ixkfgzj0anc7nkdy9isxbi6vl4kpdlx72wq2pwjav")))

(define-public crate-gtk-rs-lgpl-docs-0.1.8 (c (n "gtk-rs-lgpl-docs") (v "0.1.8") (d (list (d (n "rustdoc-stripper") (r "^0.1.5") (d #t) (k 0)))) (h "1ghxkf7hzrmj7vr0k9m0jin1j4vk5dzm24fa9rdidq8alk7lvhp9")))

(define-public crate-gtk-rs-lgpl-docs-0.1.11 (c (n "gtk-rs-lgpl-docs") (v "0.1.11") (d (list (d (n "rustdoc-stripper") (r "^0.1.6") (d #t) (k 0)))) (h "0kzk9fl0gyyr07gj4yyrdyr8ss5w9z4fi9qaibba6lx79w4gxrw4")))

(define-public crate-gtk-rs-lgpl-docs-0.1.12 (c (n "gtk-rs-lgpl-docs") (v "0.1.12") (d (list (d (n "rustdoc-stripper") (r "^0.1.6") (d #t) (k 0)))) (h "15pwvlgckikc7mkxg90dvmhwkrpkbcjfdihr4lapk5g28ppxs9jv")))

(define-public crate-gtk-rs-lgpl-docs-0.1.13 (c (n "gtk-rs-lgpl-docs") (v "0.1.13") (d (list (d (n "rustdoc-stripper") (r "^0.1.6") (d #t) (k 0)))) (h "01hwfm8jidxvn78ncjm3y25klwki5hv35zmysz93yxbafvxnfkfs")))

(define-public crate-gtk-rs-lgpl-docs-0.1.14 (c (n "gtk-rs-lgpl-docs") (v "0.1.14") (d (list (d (n "rustdoc-stripper") (r "^0.1.6") (d #t) (k 0)))) (h "11jyb1sd27vqb78s36wyjbckm5g16bxl64d8v7pj28v3wsi4nan8")))

(define-public crate-gtk-rs-lgpl-docs-0.1.15 (c (n "gtk-rs-lgpl-docs") (v "0.1.15") (d (list (d (n "rustdoc-stripper") (r "^0.1.6") (d #t) (k 0)))) (h "06b1j64zg0xmhwfkyhzh3y0apclg6qihn3f7s1bd7kgjmkia4jlr")))

(define-public crate-gtk-rs-lgpl-docs-0.1.16 (c (n "gtk-rs-lgpl-docs") (v "0.1.16") (d (list (d (n "rustdoc-stripper") (r "^0.1.13") (d #t) (k 0)))) (h "0bnj37fk9jiszzbz8wmxv7sf5cyn18nqyhkbw4saa2xxbjcmyl8a")))

(define-public crate-gtk-rs-lgpl-docs-0.1.17 (c (n "gtk-rs-lgpl-docs") (v "0.1.17") (d (list (d (n "rustdoc-stripper") (r "^0.1.13") (d #t) (k 0)))) (h "1m8qa4k1a6119yz1w6jx349zpyfbhzih2b1p4cgsfkxvsyrqr5bf")))

(define-public crate-gtk-rs-lgpl-docs-0.1.18 (c (n "gtk-rs-lgpl-docs") (v "0.1.18") (d (list (d (n "rustdoc-stripper") (r "^0.1.13") (d #t) (k 0)))) (h "0xm3lm15j8yfn2jzh3sz6hrq2g2k917ahnp5caxw9c7z8sgr9f4m")))

