(define-module (crates-io gt k- gtk-ui) #:use-module (crates-io))

(define-public crate-gtk-ui-0.1.0 (c (n "gtk-ui") (v "0.1.0") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "132ndyl59vn13bax5h85k8jzw9xcwb6fdacjxdmp7bsb1a38mprb")))

(define-public crate-gtk-ui-0.2.0 (c (n "gtk-ui") (v "0.2.0") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "0nqyspq23hn3vvzylph071vyaba2zip8rrsa6al3vl984i5a2sz3")))

(define-public crate-gtk-ui-0.2.1 (c (n "gtk-ui") (v "0.2.1") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "109317bcvvmkqhii190d24ixgv5mwv9ra8j2d87n80pk8wfpvr0p")))

(define-public crate-gtk-ui-0.2.2 (c (n "gtk-ui") (v "0.2.2") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "0p447c9lnflizkbkvz5grgbzm8qjslay71n9kmpwgykj80br4s53")))

(define-public crate-gtk-ui-0.2.3 (c (n "gtk-ui") (v "0.2.3") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1hflvq9bn65wlwy509kqxd8qx8p9jysvccbysz41v90hhqzz6lwz")))

(define-public crate-gtk-ui-0.2.4 (c (n "gtk-ui") (v "0.2.4") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "08mb1ka96mhlh151rhji3ryqjdcbp2p79syqmq34aabz5cmdki2c")))

(define-public crate-gtk-ui-0.2.5 (c (n "gtk-ui") (v "0.2.5") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1j062cfzvddya93d57v5lyi5is1346mjfbmzhs2kwk1b829wswld")))

(define-public crate-gtk-ui-0.2.6 (c (n "gtk-ui") (v "0.2.6") (d (list (d (n "unescape") (r "^0.1.0") (d #t) (k 0)))) (h "1p3pqh44w1q99c1gg53i65fbdvavpyrh78c29010wa93syr6vvg8")))

