(define-module (crates-io gt _p gt_proc) #:use-module (crates-io))

(define-public crate-gt_proc-0.0.1 (c (n "gt_proc") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "0qrzn0b3ycjwvwaz9il8vawmmx26is7v8nyxm369wazw9aqxlp2j")))

(define-public crate-gt_proc-0.0.2 (c (n "gt_proc") (v "0.0.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "regex") (r "^1.9.3") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "1imgm862jki1q2bfj5bm3ymdxy5bm2h2icxcb4ay0q4w9wbasx8v")))

(define-public crate-gt_proc-0.0.8 (c (n "gt_proc") (v "0.0.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "0v890xcfhc87hz7398aj35rd8090mx871ql4vxlghzp6iim0dmwx")))

(define-public crate-gt_proc-0.0.10 (c (n "gt_proc") (v "0.0.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pgw") (r "^0.2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "regex") (r "^1.9.5") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 0)) (d (n "trt") (r "^0.1.5") (d #t) (k 0)))) (h "1gnj8acrqq95925kmzbvhx5l4isjrsvgivmq8aqgr3zqn4bv9bip")))

