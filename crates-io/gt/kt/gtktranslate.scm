(define-module (crates-io gt kt gtktranslate) #:use-module (crates-io))

(define-public crate-gtktranslate-0.1.5 (c (n "gtktranslate") (v "0.1.5") (d (list (d (n "gtk") (r "^0.8.0") (f (quote ("v3_16"))) (d #t) (k 0)))) (h "18l18jfrlvi9lac67p32dj8cl17jz45m15kaaz04cjjv51i7g8ri")))

(define-public crate-gtktranslate-0.2.0 (c (n "gtktranslate") (v "0.2.0") (d (list (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "glib") (r "^0") (d #t) (k 0)) (d (n "glib-sys") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (d #t) (k 0)))) (h "187ac9s1fyj0lvpqxrbfpas01ii014hwnbig58q31pyal1fnwf1w")))

(define-public crate-gtktranslate-0.2.5 (c (n "gtktranslate") (v "0.2.5") (d (list (d (n "gio") (r "^0") (d #t) (k 0)) (d (n "glib") (r "^0") (d #t) (k 0)) (d (n "glib-sys") (r "^0") (d #t) (k 0)) (d (n "gtk") (r "^0") (d #t) (k 0)))) (h "1fc92rwcal215m4wk7yinxdkq0phjw25y044wzlbx7ipl3xd7sg5")))

