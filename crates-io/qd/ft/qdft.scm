(define-module (crates-io qd ft qdft) #:use-module (crates-io))

(define-public crate-qdft-0.1.0 (c (n "qdft") (v "0.1.0") (d (list (d (n "ndarray") (r "0.15.*") (d #t) (k 2)) (d (n "ndarray-npy") (r "0.8.*") (d #t) (k 2)) (d (n "num") (r "0.4.*") (d #t) (k 0)))) (h "02iacjxwr4gxj8wzgsxq56m2g79280vxdgkvm60s2idw0pv200f9")))

