(define-module (crates-io mr aa mraa-sys) #:use-module (crates-io))

(define-public crate-mraa-sys-0.1.0 (c (n "mraa-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52") (f (quote ("logging" "runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "070gh92ivqa19mxhagjna2jfxmqk9rw57igp0nx4vr5z3b7ccpp2") (l "mraa")))

