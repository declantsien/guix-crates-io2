(define-module (crates-io mr i- mri-sys) #:use-module (crates-io))

(define-public crate-mri-sys-0.1.0 (c (n "mri-sys") (v "0.1.0") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)))) (h "0nhckj838zmyy0647kd2qi4pmanl6xjc8x1mk0s5rghbfg2z3cna")))

(define-public crate-mri-sys-0.1.1 (c (n "mri-sys") (v "0.1.1") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)))) (h "023zc76rn3f9k52vkrc9aq3djz8wg2qb0qig7sc11hnl73541jd2")))

(define-public crate-mri-sys-0.1.2 (c (n "mri-sys") (v "0.1.2") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "16zjzidcywxck12d566f0spf5vwkqkpr8rf24q6bns58282vhgj5")))

(define-public crate-mri-sys-0.1.3 (c (n "mri-sys") (v "0.1.3") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "084g7zi2xrvjsrm69vy2pca3xpyfn1vkqk3l5wlkh2w9kml4f3cb")))

(define-public crate-mri-sys-0.1.4 (c (n "mri-sys") (v "0.1.4") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "1xh6ysq08dpcgwf64176b2vyldc761dd8p98lns1pn8fhwb8cngi")))

(define-public crate-mri-sys-0.1.5 (c (n "mri-sys") (v "0.1.5") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "1k6fnlx90yhclpvvh4dhcqyygj6gayhvdjp1hpm623mk4h0qnvmr")))

(define-public crate-mri-sys-0.1.6 (c (n "mri-sys") (v "0.1.6") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "0nyzi5lzq487cabfm6sim50mkq99n2gnwwf2fyf6mrhhsjcammq8")))

(define-public crate-mri-sys-0.1.7 (c (n "mri-sys") (v "0.1.7") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "0w75kjm4h074qhfsa13d5gmlwzqy4nsqy3j0bi6d6f5w1b5grcqa")))

(define-public crate-mri-sys-0.1.8 (c (n "mri-sys") (v "0.1.8") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "0r7qxa3jvnjzdxik3v87yvahacqc6gidrkrn1krcxsvaz9ysc0db")))

(define-public crate-mri-sys-0.1.9 (c (n "mri-sys") (v "0.1.9") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "0aq7xn01i7l1v7pl1n9pnli69chjm5d889jfb3vl0p2ngg937wli")))

(define-public crate-mri-sys-0.2.0 (c (n "mri-sys") (v "0.2.0") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "166xxi9f0qxpj156mfqrsd0dh50xmda6s2vhb5ajj92ybw21xn8w")))

(define-public crate-mri-sys-0.2.1 (c (n "mri-sys") (v "0.2.1") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "10naphija1gddhb714gvk71pybnbl25kilhv666hwqznbzk15k5l")))

(define-public crate-mri-sys-0.2.2 (c (n "mri-sys") (v "0.2.2") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "0mnqm3fgw90gp7aicnzvhpa9mgjk0bp643zd97bqx4vhz05g245z")))

(define-public crate-mri-sys-0.2.3 (c (n "mri-sys") (v "0.2.3") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "02939l2qjw183m6k232iawhags6b19qr60kfgympvwcx4dwmb0xd")))

(define-public crate-mri-sys-0.2.4 (c (n "mri-sys") (v "0.2.4") (d (list (d (n "libc") (r ">= 0.2.11") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.11") (d #t) (k 1)))) (h "0fcgdb3rb6skvy82alhyppyg9pr069z4yll1szdan6wzdl4mxh6m")))

(define-public crate-mri-sys-0.2.5 (c (n "mri-sys") (v "0.2.5") (d (list (d (n "libc") (r ">=0.2.86") (d #t) (k 0)) (d (n "libc") (r ">=0.2.86") (d #t) (k 1)))) (h "12ajhgl744igrsxdngjpr0cr2sjjlaif9pcsyhf2fb38jw1hp1p7")))

(define-public crate-mri-sys-0.2.6 (c (n "mri-sys") (v "0.2.6") (d (list (d (n "libc") (r ">=0.2.86") (d #t) (k 0)) (d (n "libc") (r ">=0.2.86") (d #t) (k 1)))) (h "0n2gnlljkqqvzlvnlxf0w0mhla9w15m7cbbsqw8cyiarghcgs5ww") (f (quote (("helpers") ("default" "helpers"))))))

