(define-module (crates-io mr og mrogalski-looper) #:use-module (crates-io))

(define-public crate-mrogalski-looper-1.0.0 (c (n "mrogalski-looper") (v "1.0.0") (h "170655k8y10za9gan07sv99h1zzkgp98lc6npkfyw9anxvdszi7q")))

(define-public crate-mrogalski-looper-1.0.1 (c (n "mrogalski-looper") (v "1.0.1") (h "0hj669hxy0aagc05x3y2h501vg3x9c0ra23jr00rkq19lfzg7din")))

(define-public crate-mrogalski-looper-1.0.2 (c (n "mrogalski-looper") (v "1.0.2") (h "1mmrv3hgbkxg197s3dnp67iph7353fm6sjinba6bxmxvffrgmadq")))

(define-public crate-mrogalski-looper-1.0.3 (c (n "mrogalski-looper") (v "1.0.3") (h "027zw2k3v9hdp15l0lxnk9g6ihlqqw0di1acinx50av3w6jr8lns")))

