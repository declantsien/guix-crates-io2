(define-module (crates-io mr t_ mrt_parser) #:use-module (crates-io))

(define-public crate-mrt_parser-0.1.0 (c (n "mrt_parser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ip_network") (r "^0.1.2") (d #t) (k 0)))) (h "06iz6m5xbmvas56lb3ga7aw8c07cxgrk2cl8lg0lygw5r1wxzh82")))

(define-public crate-mrt_parser-0.2.0 (c (n "mrt_parser") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ip_network") (r "^0.2.0") (d #t) (k 0)))) (h "0ckxjv6qznq13rxbn92dj8j8zyh6is8m8p37id64k4n76jwp7wy6")))

(define-public crate-mrt_parser-0.3.0 (c (n "mrt_parser") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ip_network") (r "^0.2.3") (d #t) (k 0)))) (h "0irb6fkpi7crhr11xz3r7k55f7wfqqsgk5a189lv2ajm53icpq89")))

(define-public crate-mrt_parser-0.4.0 (c (n "mrt_parser") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ip_network") (r "^0.3.0") (d #t) (k 0)))) (h "080i86y9lr62q9z7jl31n1shj3pknd679f5lali72dc5p6ccdavs")))

(define-public crate-mrt_parser-0.5.0 (c (n "mrt_parser") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "ip_network") (r "^0.4.0") (d #t) (k 0)))) (h "0yqfw2chj9mm4kmdl2kvxn2ydpgxkmvv381qvhj22q9z9zldyqg5")))

