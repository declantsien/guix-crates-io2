(define-module (crates-io mr sl mrslac) #:use-module (crates-io))

(define-public crate-mrslac-0.1.0 (c (n "mrslac") (v "0.1.0") (h "194xzrm1s21220khc75p2gxg56hgc1g7qgxgn1b4i0fplxzas3l1")))

(define-public crate-mrslac-0.2.0 (c (n "mrslac") (v "0.2.0") (h "0678riw9k92b6g16infrykz4c154fggi2sdj71ca5kkdd9wh71hn")))

