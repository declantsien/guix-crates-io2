(define-module (crates-io mr b- mrb-sys) #:use-module (crates-io))

(define-public crate-mrb-sys-0.1.0 (c (n "mrb-sys") (v "0.1.0") (d (list (d (n "bindgen") (r ">=0.55.0, <0.56.0") (d #t) (k 1)) (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)))) (h "0fwq4038v89ma7sxj9vwn7mxyss357lfisqsp962yj5h9fj6a0n6") (l "mruby")))

(define-public crate-mrb-sys-0.1.1 (c (n "mrb-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0z992r6bkc0lm5sm9by8y8xckaag1ycrm8wvd4c2im9p4ldwap1j") (l "mruby")))

(define-public crate-mrb-sys-0.1.2 (c (n "mrb-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "149x7cwlg6rs48k1vkagmnjdyj5xvvvsmw65hy4yca8kcpc15vpa") (l "mruby")))

