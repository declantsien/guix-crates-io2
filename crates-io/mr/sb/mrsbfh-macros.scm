(define-module (crates-io mr sb mrsbfh-macros) #:use-module (crates-io))

(define-public crate-mrsbfh-macros-0.1.0 (c (n "mrsbfh-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0g5iywwb6c2y7cjyi75sfl4cyk6nfd00110aq8sahs6zw0cbivl8")))

(define-public crate-mrsbfh-macros-0.2.0 (c (n "mrsbfh-macros") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w3rksp5l0kpl05rfzjc5a6x3bhxjxbd41v8r0pxkpfkq826j9sz")))

(define-public crate-mrsbfh-macros-0.3.0 (c (n "mrsbfh-macros") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0195lzgkz3gkszaj6137p4km0nmiwvj6w9vipb485dgncc3v6fjm")))

(define-public crate-mrsbfh-macros-0.4.0 (c (n "mrsbfh-macros") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1jg8my2928a249fzavh3swqnbpnx9xzwsdi88w9azlrxzs06a40b")))

(define-public crate-mrsbfh-macros-0.4.1 (c (n "mrsbfh-macros") (v "0.4.1") (d (list (d (n "convert_case") (r "^0.5.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "086gfjzxffmsmjay89j18v350aqsxjqd1hdgrfh58ygr897j9xak")))

