(define-module (crates-io mr dm mrdm) #:use-module (crates-io))

(define-public crate-mrdm-0.1.0 (c (n "mrdm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)))) (h "07dhvca5qwkb6fds6xa14w4jxz3gwaiy5nyfg3ijr6bj5fh5gpr2")))

