(define-module (crates-io mr tu mrtui) #:use-module (crates-io))

(define-public crate-mrtui-0.1.0 (c (n "mrtui") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f5ryh8pp87f6a02vil8zy9b1sdvj5g2cqk5pibswc6m0dwb9rr0")))

(define-public crate-mrtui-0.1.1 (c (n "mrtui") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ydb9f2ihdcdigglk2cxc0cp1f20yg555ihx4bysgb95xidbppwf")))

(define-public crate-mrtui-0.1.2 (c (n "mrtui") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.178") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1c63bkh77mrrczfjzv714zz1sisqqcz7n20qpd4ipbndv3nwi6y2")))

