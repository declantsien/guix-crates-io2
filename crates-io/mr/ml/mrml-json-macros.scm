(define-module (crates-io mr ml mrml-json-macros) #:use-module (crates-io))

(define-public crate-mrml-json-macros-0.1.0 (c (n "mrml-json-macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0xs6g3n2s39d5vdrafgy6xmhvrskbjryfjpg4gjlpbjx9xwxw23g")))

(define-public crate-mrml-json-macros-0.1.1 (c (n "mrml-json-macros") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1d55akq8bsxc9xim3hgjii9gd94s9c2wr1lysiww309nprcmv4a2")))

(define-public crate-mrml-json-macros-0.1.2 (c (n "mrml-json-macros") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "02y0k9aw02w7dx9pqslxhwzbbglgifisp46gzqz5vi33zc0yr5bz")))

(define-public crate-mrml-json-macros-0.1.3 (c (n "mrml-json-macros") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0vp366kybdm3nn2rs00nipkj1ygm8lnyvby6i4b7zgisb41yw2kr")))

