(define-module (crates-io mr ml mrml-python) #:use-module (crates-io))

(define-public crate-mrml-python-0.1.0 (c (n "mrml-python") (v "0.1.0") (d (list (d (n "mrml") (r "^2.1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "116r21i4rvwayibp8kr1mwavjh2m7dlwvgny4ymy59bpwyi1pgv9")))

(define-public crate-mrml-python-0.1.1 (c (n "mrml-python") (v "0.1.1") (d (list (d (n "mrml") (r "^3.0.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1m20sh8n0fwx5bzi489xc0kp8994wv2ym2rk5wls2hw7d973gcg9")))

(define-public crate-mrml-python-0.1.4 (c (n "mrml-python") (v "0.1.4") (d (list (d (n "mrml") (r "^3.0.3") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "11l3m821i9gaam3hvlmlnmn11jqa3nnqf2r6hsl680krdfrnzflh")))

(define-public crate-mrml-python-0.1.5 (c (n "mrml-python") (v "0.1.5") (d (list (d (n "mrml") (r "^3.0.3") (f (quote ("http-loader-ureq" "local-loader"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "00zi24b4hmx63j1b436942ks4vn4dvypvamn9gdbib701ba5kyp4")))

(define-public crate-mrml-python-0.1.6 (c (n "mrml-python") (v "0.1.6") (d (list (d (n "mrml") (r "^3.0.4") (f (quote ("http-loader-ureq" "local-loader"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1nzg9f1spidqq2yxlab7briwyjfl4qidqklx35i8rcmhx7hk6r8a")))

(define-public crate-mrml-python-0.1.7 (c (n "mrml-python") (v "0.1.7") (d (list (d (n "mrml") (r "^3.1.0") (f (quote ("http-loader-ureq" "local-loader"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "09asz55vf8sdnmjx8q5pv46ddih91baik8wfxncciw5kjgyj414h")))

(define-public crate-mrml-python-0.1.8 (c (n "mrml-python") (v "0.1.8") (d (list (d (n "mrml") (r "^3.1.1") (f (quote ("http-loader-ureq" "local-loader"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0z254q6bqg0mwljscy08k8nfpg433y29gck83iwpwl5picsz9qg1")))

(define-public crate-mrml-python-0.1.9 (c (n "mrml-python") (v "0.1.9") (d (list (d (n "mrml") (r "^3.1.2") (f (quote ("http-loader-ureq" "local-loader"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1vffn2fpc2p5dfrvg9lgph1p8cis6c6qf9pvx399c1jn0q909djc")))

(define-public crate-mrml-python-0.1.10 (c (n "mrml-python") (v "0.1.10") (d (list (d (n "mrml") (r "^3.1.3") (f (quote ("http-loader-ureq" "local-loader"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0navi06na06b9g6902q9nvy9qnih9g3mf1klzifp3cw5hwfhkl7j")))

(define-public crate-mrml-python-0.1.11 (c (n "mrml-python") (v "0.1.11") (d (list (d (n "mrml") (r "^3.1.4") (f (quote ("http-loader-ureq" "local-loader"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0qyij9ldncyals6nqmaw7rgsgsr4x160m6vjkfsl3pf9181sahxr")))

(define-public crate-mrml-python-0.1.12 (c (n "mrml-python") (v "0.1.12") (d (list (d (n "mrml") (r "^3.1.5") (f (quote ("http-loader-ureq" "local-loader"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "1f05l1khf12i19s2mpyddicxkhn6i8pppiq0r2nlxjyi8gjmygcb")))

(define-public crate-mrml-python-0.1.13 (c (n "mrml-python") (v "0.1.13") (d (list (d (n "mrml") (r "^3.1.5") (f (quote ("http-loader-ureq" "local-loader"))) (d #t) (k 0)) (d (n "pyo3") (r "^0.21.2") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "00xwfk7fhhjhh42jj220j6rmzbsyv3hqmhhdjfflkwgklyqf36h7")))

