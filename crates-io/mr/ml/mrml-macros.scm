(define-module (crates-io mr ml mrml-macros) #:use-module (crates-io))

(define-public crate-mrml-macros-0.1.0 (c (n "mrml-macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1gb2sw6apj8695l9bljfgrjagqfa9k7qjlxmq7bkgkj8qbzmnb0w")))

(define-public crate-mrml-macros-0.1.1 (c (n "mrml-macros") (v "0.1.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1zykp9x0vkabh4nwd4bv047r8lsy8kw3fd3r2bhvxjmv48qhy08w")))

(define-public crate-mrml-macros-0.1.2 (c (n "mrml-macros") (v "0.1.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1wj145m62ybrx9dbrm4jhya8m73pps0svbjyzy2wivvr9f7nlfjp")))

(define-public crate-mrml-macros-0.1.3 (c (n "mrml-macros") (v "0.1.3") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1504yqqlfz6fshy0ad8k7p4ychpmsfv5mxh45040nzkaqn6wzax8")))

