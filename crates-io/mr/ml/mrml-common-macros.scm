(define-module (crates-io mr ml mrml-common-macros) #:use-module (crates-io))

(define-public crate-mrml-common-macros-0.1.0 (c (n "mrml-common-macros") (v "0.1.0") (d (list (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "08z6kf5wzgba6wrm1gcpx08lbih3qri522fwblsnark6rmbbwsv4")))

(define-public crate-mrml-common-macros-0.1.1 (c (n "mrml-common-macros") (v "0.1.1") (d (list (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0mrvz6f40nxs94ayi1w6xs40dbn5d00k7qbkhisf46dw5f4csyf3")))

(define-public crate-mrml-common-macros-0.1.2 (c (n "mrml-common-macros") (v "0.1.2") (d (list (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "03z0ik54ik7cnqj68vzrrb0kp46jsp7slg8xkghm2qfb6f4sfhf1")))

(define-public crate-mrml-common-macros-0.1.3 (c (n "mrml-common-macros") (v "0.1.3") (d (list (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "11w7fj4lxxyf9dm7gslfpkmlpbl1qqr8g7bgvppi5f3rdxhqdx76")))

(define-public crate-mrml-common-macros-0.1.4 (c (n "mrml-common-macros") (v "0.1.4") (d (list (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0dcdzgvvsyqy22wg7rbic8i5j1ybj2h9rkcc60b6kdg24mf936cb")))

