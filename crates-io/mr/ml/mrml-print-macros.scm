(define-module (crates-io mr ml mrml-print-macros) #:use-module (crates-io))

(define-public crate-mrml-print-macros-0.1.0 (c (n "mrml-print-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0mdx50jqfz3f823xcaar5xn43lcwws5m8zq84anzhp9wn4vdcgiw")))

(define-public crate-mrml-print-macros-0.1.1 (c (n "mrml-print-macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "02sfp1d42cvg5wzsl9a3v28rdxxjs8pkwx0r5cwj2wgg5aldifz4")))

(define-public crate-mrml-print-macros-0.1.2 (c (n "mrml-print-macros") (v "0.1.2") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0wf9dxk5766mal88q2jqf94b0ahj8qxc3aclfn3alnbv6iazr2ia")))

(define-public crate-mrml-print-macros-0.1.3 (c (n "mrml-print-macros") (v "0.1.3") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "18c1rz5jyrlgqr3kx1dk8h6p5n9cq2dsjkv46hqmrcb4nq616c6x")))

