(define-module (crates-io mr ml mrml-parse-macros) #:use-module (crates-io))

(define-public crate-mrml-parse-macros-0.1.0 (c (n "mrml-parse-macros") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "mrml-common-macros") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1iy7d62md4x9qx8143zn0vf9zx0hxnpfhy5w8iar2xjz5vnpj881")))

