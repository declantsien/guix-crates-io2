(define-module (crates-io mr td mrtd) #:use-module (crates-io))

(define-public crate-mrtd-0.1.0 (c (n "mrtd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04r6qshpfxsz7xlwqvfrkpwwbxi4nfg3rh3sg30g05d1jwdx500d")))

(define-public crate-mrtd-0.2.0 (c (n "mrtd") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0zgl1dficb2jzk3pxr1y6dxhx35zsm510rf5yslsibpkw85fy6n9")))

(define-public crate-mrtd-0.3.0 (c (n "mrtd") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0sav93y1zqra66470hy075yal3hcnh2asi9bk86zlfgxldj66745")))

(define-public crate-mrtd-0.4.0 (c (n "mrtd") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0bcf2357pvwv59x0w2szq3xha71hzcrgv0lb8jjsqnrwwsl5sczx")))

(define-public crate-mrtd-0.5.0 (c (n "mrtd") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "02jp0fw1mmyxhyfbwy9c14zzbh1mpy46r4zl4hz25z42wajcif29")))

(define-public crate-mrtd-0.5.1 (c (n "mrtd") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kb63ka3czc9cvkppif001fmqbmxc122x756q7x42cj03jf01aa3")))

