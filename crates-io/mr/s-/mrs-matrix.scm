(define-module (crates-io mr s- mrs-matrix) #:use-module (crates-io))

(define-public crate-mrs-matrix-1.0.0 (c (n "mrs-matrix") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "coolor") (r "^0.5.0") (f (quote ("crossterm"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03k8ms53rb5f7mhh73hmww6hqdinf5h9spc8wy54xjsfp2sf4wcd")))

(define-public crate-mrs-matrix-1.0.1 (c (n "mrs-matrix") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "coolor") (r "^0.5.0") (f (quote ("crossterm"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ag7l242x3rdwbjhx9knjpk0d7vzm892m2ahvgdqna2p45ibn0km")))

(define-public crate-mrs-matrix-1.0.2 (c (n "mrs-matrix") (v "1.0.2") (d (list (d (n "clap") (r "^3.2.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "coolor") (r "^0.5.0") (f (quote ("crossterm"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.23.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0lk4kf8i1ws9n0mg1xkr6mn3g0pkc8z7b98021rf77mm0a6rp4wf")))

