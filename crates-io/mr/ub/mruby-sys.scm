(define-module (crates-io mr ub mruby-sys) #:use-module (crates-io))

(define-public crate-mruby-sys-0.1.0 (c (n "mruby-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)))) (h "02z6nv9971af8p9h3yn1i3xvxz330gs16xzq07rdld5vscfa0qry") (l "mruby")))

(define-public crate-mruby-sys-0.1.1 (c (n "mruby-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)))) (h "1whp3lbm86mrflzxjfnrjc1sbsqdqg5rl7w6rjcnlwm0c6d6ia53") (l "mruby")))

(define-public crate-mruby-sys-0.2.0 (c (n "mruby-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)))) (h "0avgq35yb0w7c8996hp6h14a0bi0w38plhd0nnh00xwwr6g1d01l") (l "mruby")))

