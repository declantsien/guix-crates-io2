(define-module (crates-io mr us mrusty) #:use-module (crates-io))

(define-public crate-mrusty-0.1.0 (c (n "mrusty") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "04nlsbzjvrsf401pwpjn7wbykzvk01ssgp509m78z8srd3j77dcn") (f (quote (("repl" "rl-sys"))))))

(define-public crate-mrusty-0.1.1 (c (n "mrusty") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "1dcy7ri8jhj8nv7yl1fcasxi6nrxnx5ysaavwv6dijjslyvijbpj") (f (quote (("repl" "rl-sys"))))))

(define-public crate-mrusty-0.2.0 (c (n "mrusty") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "10451kk7596xklyw5694c4whyzjrzizgag6jl9kbgw60z6z815vy") (f (quote (("repl" "rl-sys"))))))

(define-public crate-mrusty-0.2.1 (c (n "mrusty") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "1qg32jkz0y0aph4p9vj4wcvwm298rzhaljfzpxqgg0467m3ivh6j") (f (quote (("repl" "rl-sys"))))))

(define-public crate-mrusty-0.2.2 (c (n "mrusty") (v "0.2.2") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "1nxnhzwgh6636a3wx4rg17vwbvm5q064x11qzi53i86lhv0wl8m7") (f (quote (("repl" "rl-sys"))))))

(define-public crate-mrusty-0.2.3 (c (n "mrusty") (v "0.2.3") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0f0qw8676chn45gmgwbwl90fpgppl8dixxdqf9wkx689bfnm6m8j") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.2.4 (c (n "mrusty") (v "0.2.4") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "16m4q999jxa0cq78xg6csmxcj7jjz7av8l2a02pz20zcwimxgr2h") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.2.5 (c (n "mrusty") (v "0.2.5") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0bgmjs0mb9vf3ahzwcyh1x6k8jrrkb2qyay9n48i8zirpkripzx3") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.2.6 (c (n "mrusty") (v "0.2.6") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0b8wvxasja96qcjv3rkx7vz9rligw06cgnv24mxj9ivldssinfpv") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.2.7 (c (n "mrusty") (v "0.2.7") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "1ckq9hrxlrkdf89pbm6698b4dck3hsgwaf409i8yyxaa6xm47a5m") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.2.8 (c (n "mrusty") (v "0.2.8") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0i16122pq0cqqx6ckq8jmlqmm33kg24wby0anv05aq5419x6qmfd") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.3.0 (c (n "mrusty") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)))) (h "0px3571pp00kwz417cjwx6hzv3r5bhzcbfqkpsd0p5c4j0w949nl") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.3.1 (c (n "mrusty") (v "0.3.1") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4.4") (d #t) (k 1)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 1)))) (h "0n7q4kbl35lyr80z845crkrql5070pj17mh0qvr3ri87vn7640j6") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.3.2 (c (n "mrusty") (v "0.3.2") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4.4") (d #t) (k 1)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 1)))) (h "1rdcvsx0fqjm3mrsqmyc6qcdykng8lw1njl95bp6xqp9p1f2jxs0") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.4.0 (c (n "mrusty") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4.4") (d #t) (k 1)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 1)))) (h "0ma188ql9vl5jw14zdp9ffhrpw1sxn3vy7l6zk95r8k7yxs5nra4") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.4.1 (c (n "mrusty") (v "0.4.1") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4.4") (d #t) (k 1)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 1)))) (h "1vpr4dylzq1rxa3pqbgxrpbf2wlx00g91jir9s5n1bhalw6zr68c") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.4.2 (c (n "mrusty") (v "0.4.2") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4.4") (d #t) (k 1)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 1)))) (h "10amfpsa1494pvhpq0xrhx4v3psdir21bj383ilkpm4ph6hjw6a5") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.4.3 (c (n "mrusty") (v "0.4.3") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4.4") (d #t) (k 1)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 1)))) (h "1kfhg16g38238knq8zx4ix89fi0ym1pz0s9wvmaygr18ycjqx115") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.5.0 (c (n "mrusty") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4.4") (d #t) (k 1)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 1)))) (h "0kxjg8kzggn15kp3zndbllyh5df9v62c3c51qpmwx2xkyndqiq67") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-0.5.1 (c (n "mrusty") (v "0.5.1") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4.4") (d #t) (k 1)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 1)))) (h "18xdm63z4s8isi5fbvzl3afd2g9hfni81zg58m4pffmwyinsh10b") (f (quote (("gnu-readline" "rl-sys"))))))

(define-public crate-mrusty-1.0.0 (c (n "mrusty") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3.22") (d #t) (k 1)) (d (n "rl-sys") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4.4") (d #t) (k 1)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 1)))) (h "120rqfw4canh7sddxqs5w81gdsd910y4gh5z5mmkf88c66yp6kgh") (f (quote (("gnu-readline" "rl-sys"))))))

