(define-module (crates-io mr sa mrsa) #:use-module (crates-io))

(define-public crate-mrsa-0.1.0 (c (n "mrsa") (v "0.1.0") (h "1zc7bhi3b1z7iz1kgbmjacr08pjsv40jf8x6pf5nbcjn9dyfa7m0")))

(define-public crate-mrsa-0.1.2 (c (n "mrsa") (v "0.1.2") (h "04jgmqr1pa2vyy551bbcbmbw1sigl72zvwz0csvqmm15p1p1r9y1")))

