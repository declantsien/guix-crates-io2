(define-module (crates-io mr -m mr-mime) #:use-module (crates-io))

(define-public crate-mr-mime-0.1.0 (c (n "mr-mime") (v "0.1.0") (d (list (d (n "autocfg") (r "^1.1.0") (d #t) (k 1)) (d (n "intern-str") (r "^0.1") (d #t) (k 0)))) (h "1jj22yvqq9c1bs7dx40a36fb0m0c1l6dnn55sigz8sixbgk93i86") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-mr-mime-0.1.1 (c (n "mr-mime") (v "0.1.1") (d (list (d (n "intern-str") (r "^0.1") (d #t) (k 0)) (d (n "memchr") (r "^2.5.0") (k 0)))) (h "17ay0hdrdkhmamfzl0jfn0sjw68kdhppn230qfl542pw96b59566") (f (quote (("std" "alloc") ("default" "std") ("alloc"))))))

