(define-module (crates-io mr gr mrgraph) #:use-module (crates-io))

(define-public crate-mrgraph-0.0.1 (c (n "mrgraph") (v "0.0.1") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "meritrank2") (r "^0.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1zxmxklp0ygzw9hdkizbhi5d3k8w9vsfp9sa684bl9z9b5wh73x6")))

(define-public crate-mrgraph-0.0.2 (c (n "mrgraph") (v "0.0.2") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "meritrank2") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0sdiybgkj8fvady4w8wy8yh42ma9vrzsqck30w584s3m9ch945fg")))

(define-public crate-mrgraph-0.0.3 (c (n "mrgraph") (v "0.0.3") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "meritrank2") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0isdlkd7ysr96gmyd2dasj64kar72h5pccqzyw2dbsnv14dhxfka")))

(define-public crate-mrgraph-0.0.4 (c (n "mrgraph") (v "0.0.4") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "meritrank2") (r "^0.5.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0m4nckq74ryv7x13hfyx2wk039h0zf1qay48hdmpi5dg9pci1dsg")))

