(define-module (crates-io mr ep mreplace) #:use-module (crates-io))

(define-public crate-mreplace-0.1.1 (c (n "mreplace") (v "0.1.1") (d (list (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "1ncrfl2a5by8l8f0ppxqwp53m1fyay0v51jglmc69a8rxir8va4g")))

(define-public crate-mreplace-0.1.2 (c (n "mreplace") (v "0.1.2") (d (list (d (n "const-str") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)))) (h "02njs3q138hylhg9vg07dhy9yn4jhxdwnd4ls4lbnkxzqd6gb4l1") (f (quote (("macro" "const-str") ("default" "macro"))))))

(define-public crate-mreplace-0.1.3 (c (n "mreplace") (v "0.1.3") (d (list (d (n "const-str") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (f (quote ("lock_statistics" "parking_lot" "parking_lot_core"))) (d #t) (k 2)))) (h "0s6jvwlm391dvcfdkpha8461z1g6xc0lp134ifm4mvc05svkxm1b") (f (quote (("macro" "const-str") ("default" "macro"))))))

(define-public crate-mreplace-0.1.4 (c (n "mreplace") (v "0.1.4") (d (list (d (n "const-str") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (f (quote ("lock_statistics" "parking_lot" "parking_lot_core"))) (d #t) (k 2)))) (h "0gl347lcqbak60yq1qg0nq7gmvhdx4v8azmy7js636l67hgx1jxq") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:const-str"))))))

(define-public crate-mreplace-0.1.5 (c (n "mreplace") (v "0.1.5") (d (list (d (n "const-str") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "daachorse") (r "^1.0.0") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (f (quote ("lock_statistics" "parking_lot" "parking_lot_core"))) (d #t) (k 2)))) (h "031qr77781h31xx5g0ps4phmrg42kh3n4bjycsv6p64mn2h6n2qq") (f (quote (("default" "macro")))) (s 2) (e (quote (("macro" "dep:const-str"))))))

