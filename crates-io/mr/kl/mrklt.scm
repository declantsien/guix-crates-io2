(define-module (crates-io mr kl mrklt) #:use-module (crates-io))

(define-public crate-mrklt-0.1.0 (c (n "mrklt") (v "0.1.0") (d (list (d (n "blake2") (r "^0.8.1") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)))) (h "071lyhiyg6j89dxi0wxvs19liyd3vkacs1nyvqyc1bjmllyi3j5k")))

