(define-module (crates-io mr do mrdo) #:use-module (crates-io))

(define-public crate-mrdo-0.1.6 (c (n "mrdo") (v "0.1.6") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.6") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02agf9jgpg7nfppshic60bzhnffpkf56zdinlhajrnl09zf9cg26")))

