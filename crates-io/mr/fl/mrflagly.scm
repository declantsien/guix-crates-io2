(define-module (crates-io mr fl mrflagly) #:use-module (crates-io))

(define-public crate-mrflagly-0.1.0 (c (n "mrflagly") (v "0.1.0") (d (list (d (n "httptest") (r "^0.15.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "0apc8hsh59pf1i2g89zd9mr8hqm96n7pyg6zgk7ysc76gnhafmjj")))

(define-public crate-mrflagly-0.2.0 (c (n "mrflagly") (v "0.2.0") (d (list (d (n "httptest") (r "^0.15.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "12ggnzl3ywip0qkrd39brl57ms9drvz7bvz3wxjpqwyv036qsvw9")))

(define-public crate-mrflagly-0.2.1 (c (n "mrflagly") (v "0.2.1") (d (list (d (n "httptest") (r "^0.15.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1a90bmv2yaa916rd44x82kwl0f2a7xq29ivzlzdni6xr81sxxq7q")))

(define-public crate-mrflagly-0.2.2 (c (n "mrflagly") (v "0.2.2") (d (list (d (n "httptest") (r "^0.15.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "0yb7p208j1z6zyx08k3df59mzza3l93gndrw4n5694ijryj7jxga")))

(define-public crate-mrflagly-0.2.3 (c (n "mrflagly") (v "0.2.3") (d (list (d (n "httptest") (r "^0.15.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "0jm3fa6qp7ldlhxwbpp6bhkd4vbc186xjahfi2rgafadgxwisd8l")))

(define-public crate-mrflagly-0.2.5 (c (n "mrflagly") (v "0.2.5") (d (list (d (n "httptest") (r "^0.15.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "0vr6yvs0gh6hykwdngzgmm2hv0w63a94fis7hpmrjqp788g14aqi")))

(define-public crate-mrflagly-0.2.7 (c (n "mrflagly") (v "0.2.7") (d (list (d (n "httptest") (r "^0.15.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1xqcm8gdlynfddc774kmhshwbr8qhvc1i2b285mwyx283kfhzghq")))

(define-public crate-mrflagly-0.2.8 (c (n "mrflagly") (v "0.2.8") (d (list (d (n "httptest") (r "^0.15.4") (d #t) (k 0)) (d (n "json") (r "^0.12.4") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.0") (f (quote ("extension-module"))) (o #t) (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "0r3ik8pl2plkynz2pvcppwhqi45n8cczf5wwzym78r4ww2kdqiiw") (f (quote (("default" "python")))) (s 2) (e (quote (("python" "dep:pyo3"))))))

