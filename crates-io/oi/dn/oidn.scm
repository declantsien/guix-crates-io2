(define-module (crates-io oi dn oidn) #:use-module (crates-io))

(define-public crate-oidn-0.1.0 (c (n "oidn") (v "0.1.0") (h "0bi53cjh74qdinlm3bvl68z7p7bvngrfgnaabj7p07s7mhw41al7")))

(define-public crate-oidn-0.2.0 (c (n "oidn") (v "0.2.0") (h "0zsz8ickiqcv6sd4x3gffz90r3nqagjh7l32hn4xvq5wsgs7xza1")))

(define-public crate-oidn-1.0.0 (c (n "oidn") (v "1.0.0") (h "0bza2niazpmir8v8nrfqkxxzjvrzkjlq6mns6fcschnhqqy2nanh")))

(define-public crate-oidn-1.1.0 (c (n "oidn") (v "1.1.0") (h "01hn994zldq6m76ycvya6d37n6xvsra09wj930r3wc33f11nzwpg")))

(define-public crate-oidn-1.2.0 (c (n "oidn") (v "1.2.0") (h "19k56jjyy6pp2vgmsa7kzawa4h5qdn94jx3kk4ch760j7411jwmh")))

(define-public crate-oidn-1.2.1 (c (n "oidn") (v "1.2.1") (h "1sijcax5ggwfzwmvqr6wjhj4frr2xn41p4b66lij7ic4m3r3b6g0")))

(define-public crate-oidn-1.3.0 (c (n "oidn") (v "1.3.0") (h "0bfr6zf6s56xv1g5v29w6wlc300b004msw5r1nddaryqjva84ylh")))

(define-public crate-oidn-1.3.1 (c (n "oidn") (v "1.3.1") (h "08yhfrbcbdnn36458z4kwhyl88mwchg1vh57nva0cdr5zg8wl16x")))

(define-public crate-oidn-1.4.0 (c (n "oidn") (v "1.4.0") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "104vyq74gy9hsbdn9pynv1gjx8clqpqrqgdh8428qdal8blhxzcf") (y #t)))

(define-public crate-oidn-1.4.1 (c (n "oidn") (v "1.4.1") (d (list (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "19ya6sbrlnj0yygb1frywlm5afxwdcbdf4vz7g6z4bi3pxcm26zy")))

(define-public crate-oidn-1.4.2 (c (n "oidn") (v "1.4.2") (d (list (d (n "num_enum") (r "^0.5.6") (d #t) (k 0)))) (h "02yliphp1ff9pfxsz7y0x2zilajbkls965pzf5whk45gb7xh97nk") (l "OpenImageDenoise")))

(define-public crate-oidn-1.4.3 (c (n "oidn") (v "1.4.3") (d (list (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "15mx3ckr7857ys3aq5z2s89a4r5x1zvndfylzfrs7n2xv67jcrvg") (l "OpenImageDenoise")))

(define-public crate-oidn-2.0.1 (c (n "oidn") (v "2.0.1") (d (list (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "19v2b7gpfcjl4v396yrxp1dl3gny5mxvnmfvdxr77fw6c1b2gxq9") (l "OpenImageDenoise")))

(define-public crate-oidn-2.2.2 (c (n "oidn") (v "2.2.2") (d (list (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)))) (h "1s27j3sjhnwx3j58qhs1nw09pqsdf7hmcrhbm8apx1ljcr1jz720") (l "OpenImageDenoise")))

(define-public crate-oidn-2.2.3 (c (n "oidn") (v "2.2.3") (d (list (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)))) (h "0rc68h4dys2dfjqdp3z9mdhldw4b26pjzmih52s8vg7wwm4n34js") (l "OpenImageDenoise")))

