(define-module (crates-io oi h_ oih_grrs) #:use-module (crates-io))

(define-public crate-oih_grrs-0.1.0 (c (n "oih_grrs") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "predicates") (r "^1") (d #t) (k 2)) (d (n "structopt") (r "^0.2.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0w0dyiihrrjvf5f7dgisrzz1m236pwss064wp2ykg8453345sz8h")))

