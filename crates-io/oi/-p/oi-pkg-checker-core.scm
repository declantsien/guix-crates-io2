(define-module (crates-io oi -p oi-pkg-checker-core) #:use-module (crates-io))

(define-public crate-oi-pkg-checker-core-0.1.0 (c (n "oi-pkg-checker-core") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fmri") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1yhb3fihsvriqp4kzdvjsbl2d6h0xwsw5lq1v5f83phnvfri6cwq")))

(define-public crate-oi-pkg-checker-core-0.1.1 (c (n "oi-pkg-checker-core") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fmri") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1hx9mxd159bln8bm722651ahq1izir6p1w8231d0jgfip512a7kb")))

(define-public crate-oi-pkg-checker-core-0.2.0 (c (n "oi-pkg-checker-core") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fmri") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0r6llmb5da5k7bd5kqww6c9haylzd4ws9wy3dqc1cwzp50njb034")))

(define-public crate-oi-pkg-checker-core-0.2.1 (c (n "oi-pkg-checker-core") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "fmri") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "14ls8czzyrcvaxxg1yzsaqf1szziadml6hqx5nrlw3l55q96afgf")))

