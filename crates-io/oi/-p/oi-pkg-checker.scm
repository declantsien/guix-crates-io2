(define-module (crates-io oi -p oi-pkg-checker) #:use-module (crates-io))

(define-public crate-oi-pkg-checker-1.0.0 (c (n "oi-pkg-checker") (v "1.0.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "fmri") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oi-pkg-checker-core") (r "^0.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0l6g1b5cw6rx4m33mcx47284csly3yb0xp4qkn2gqw6mz8m1d3i0")))

(define-public crate-oi-pkg-checker-1.0.1 (c (n "oi-pkg-checker") (v "1.0.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "fmri") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oi-pkg-checker-core") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "0njfxcgx6qwbrxiwbr3fq33pffbn56ya6r5v9l2g9k2qx1208jzj")))

(define-public crate-oi-pkg-checker-1.1.0 (c (n "oi-pkg-checker") (v "1.1.0") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "fmri") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oi-pkg-checker-core") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1a8axfccgxsdkdbfp7r555si5f3965pagd6yc1y1lhzj0lxkl5yg")))

(define-public crate-oi-pkg-checker-1.1.1 (c (n "oi-pkg-checker") (v "1.1.1") (d (list (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0") (d #t) (k 0)) (d (n "fmri") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oi-pkg-checker-core") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)))) (h "1xzcv7sgkhlw47i8sj5kiwbrkbwrlikkrj33kix9nh24say4n859")))

