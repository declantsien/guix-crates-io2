(define-module (crates-io oi su oisuite) #:use-module (crates-io))

(define-public crate-oisuite-1.0.0 (c (n "oisuite") (v "1.0.0") (d (list (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.6") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "1rv89xa6fs1b06qmsy9wk89w9a7d3i0yzyrjg0qbq0fqla8x25l4")))

