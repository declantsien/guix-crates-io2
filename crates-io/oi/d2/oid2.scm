(define-module (crates-io oi d2 oid2) #:use-module (crates-io))

(define-public crate-oid2-0.1.0 (c (n "oid2") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "sqlx") (r "^0.6.3") (f (quote ("runtime-tokio-rustls"))) (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.20") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.3.1") (o #t) (d #t) (k 0)))) (h "0pr4y4rvvs78iz9rpmg61z0bnzkrrxfifp66z0qxslviflbd428s") (f (quote (("default" "rand" "chrono"))))))

