(define-module (crates-io oi -u oi-unwrap) #:use-module (crates-io))

(define-public crate-oi-unwrap-0.1.0 (c (n "oi-unwrap") (v "0.1.0") (h "0g9sv0fpsnlapszr2l9n4ax05nmwbmjlk7xifnrbs9hk6ylc7zzz")))

(define-public crate-oi-unwrap-0.1.1 (c (n "oi-unwrap") (v "0.1.1") (h "12vg37fhbs0y4b2jgci4x3pc8dgyi78lb9v7q5cgh52p5yd3iflk")))

