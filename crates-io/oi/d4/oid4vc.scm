(define-module (crates-io oi d4 oid4vc) #:use-module (crates-io))

(define-public crate-oid4vc-0.1.0 (c (n "oid4vc") (v "0.1.0") (d (list (d (n "dif-presentation-exchange") (r "^0.1.0") (d #t) (k 0)) (d (n "oid4vci") (r "^0.1.0") (d #t) (k 0)) (d (n "oid4vp") (r "^0.1.0") (d #t) (k 0)) (d (n "siopv2") (r "^0.1.0") (d #t) (k 0)))) (h "008bf52bb5023s46g7lg2x5c46iy8jb5sl2p46x76h3pib2hmx70")))

