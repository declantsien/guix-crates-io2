(define-module (crates-io oi _h oi_helper) #:use-module (crates-io))

(define-public crate-oi_helper-0.0.0-alpha (c (n "oi_helper") (v "0.0.0-alpha") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "1gk9h3i25x9zna84cfdpxklh9jnzbnldpkakccrjvcx95kskw55c")))

(define-public crate-oi_helper-0.0.1 (c (n "oi_helper") (v "0.0.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "12mj7a3v7qs58ais7068wb654r1cbrljsxmprhgp2y459lndvyqp")))

(define-public crate-oi_helper-0.0.2 (c (n "oi_helper") (v "0.0.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "1aadqlpvxk3ir7xjb0yh9lrkfy85c197qb77nnz3r15j7f12h9s6")))

(define-public crate-oi_helper-0.0.3 (c (n "oi_helper") (v "0.0.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "1hhcv82123ywf4s3vzyyrnnphixckpzwkq2gs8lma4agk1mc2zi2")))

(define-public crate-oi_helper-0.0.4 (c (n "oi_helper") (v "0.0.4") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "13gfmfnca0iqxlayz9prmrshkzcp68bmcrs00xfh3d7vx27hm74w")))

(define-public crate-oi_helper-0.1.0 (c (n "oi_helper") (v "0.1.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "1n1n2hl5n4m4cl697ij0f5fw7yfbw3nxm90qmqql1pg45sdsjpk1")))

(define-public crate-oi_helper-0.1.1 (c (n "oi_helper") (v "0.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "1dmwqsza6qp1mybr5439nqp6aviklm1d6rx3h2ppi7m221swxmlr")))

(define-public crate-oi_helper-0.1.2 (c (n "oi_helper") (v "0.1.2") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "0nr5a7sqg068hz9hj6xxssab8q1jcnh9w1240bwlm1ygg2jvb6p4")))

(define-public crate-oi_helper-0.1.3 (c (n "oi_helper") (v "0.1.3") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "0sf19ab9z6iwnh7chmw8v1m3wgsi899qjbiixvmy708kbk08vy1b")))

(define-public crate-oi_helper-0.1.4 (c (n "oi_helper") (v "0.1.4") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "0n7b74dywcpaj0gj7qcd7dblgpja62fx2m6szkgmryng9i3c740x")))

(define-public crate-oi_helper-0.1.5 (c (n "oi_helper") (v "0.1.5") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.24.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)))) (h "05cmgl1k5hg4msd5p56d7fdd0hz4gph2a96jgip3sh6rzk7z0qms")))

(define-public crate-oi_helper-2.0.0-0-devpreview (c (n "oi_helper") (v "2.0.0-0-devpreview") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "0rm30hpmqikl4mi812cv7z5dg500jb0arrqbqa5v4pghdpv71ygy")))

(define-public crate-oi_helper-2.0.0-1-devpreview (c (n "oi_helper") (v "2.0.0-1-devpreview") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "0f4wrj19nwrs6vs6h7p1qgirv3ll5dw5wn1pn5a3ssp4y4l40q9v")))

(define-public crate-oi_helper-2.0.0-2-devpreview (c (n "oi_helper") (v "2.0.0-2-devpreview") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "0nvspjzjdda54wxnpc8qkmn111h7zgfp5xh14cplzca00lb20jba")))

(define-public crate-oi_helper-2.0.0-3-devpreview (c (n "oi_helper") (v "2.0.0-3-devpreview") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "19p4c4prkjgcrx97314c4lr3qpkq6r2nf3vks5fra8dbj456bpig")))

(define-public crate-oi_helper-2.0.0-4-devpreview (c (n "oi_helper") (v "2.0.0-4-devpreview") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.3") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "0l23ffnyqjv4s8cl3r6apllc70dqjmhxwfcri0vv6d8972v2fjcf")))

(define-public crate-oi_helper-2.0.0-5-devpreview (c (n "oi_helper") (v "2.0.0-5-devpreview") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.3") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "1dzsld01amhz2f4478n5n9wm2691gv3x8115sf13snjl2ka4fxsg")))

(define-public crate-oi_helper-2.0.0 (c (n "oi_helper") (v "2.0.0") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.3") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "1iax33i373wz60k6sqlln36lidzf1fgc7qy3h2g98ywn9ax4dv2v")))

(define-public crate-oi_helper-2.0.1 (c (n "oi_helper") (v "2.0.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.3") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "0gasxn1ax6mf0qif0g69b5h6p86y4n28qmfdxqwdm5hnmml2ihpl")))

(define-public crate-oi_helper-2.0.2 (c (n "oi_helper") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0.61") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "home") (r "^0.5.0") (d #t) (k 0)) (d (n "html_parser") (r "^0.6.3") (d #t) (k 0)) (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "wait-timeout") (r "^0.2.0") (d #t) (k 0)))) (h "1dajdxhglchl9l7kglgb3gs9321b8p8i8pdzb9fi4b62saynbpx7")))

