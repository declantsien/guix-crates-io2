(define-module (crates-io oi d- oid-registry) #:use-module (crates-io))

(define-public crate-oid-registry-0.0.1 (c (n "oid-registry") (v "0.0.1") (d (list (d (n "der-parser") (r "^4.1") (d #t) (k 0)))) (h "0ihpvp0vm15d1pvd4y5i6qmksbv14dr948x29scingwzy2zpa0r3") (f (quote (("x962") ("x509") ("rsadsi") ("pkcs9") ("pkcs7") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "rsadsi" "kdf" "pkcs7" "pkcs9" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.0.2 (c (n "oid-registry") (v "0.0.2") (d (list (d (n "der-parser") (r "^4.1") (d #t) (k 0)))) (h "1as5cbj44vg9s76a79mz20509izjd8616bxk9kwgzy9xyx99qs4b") (f (quote (("x962") ("x509") ("pkcs9") ("pkcs7") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.0.3 (c (n "oid-registry") (v "0.0.3") (d (list (d (n "der-parser") (r "^5.0.0-beta1") (d #t) (k 0)))) (h "1rv9c8c6wix00kvqx0nqnr7z7d60gfw0nhk9rdd74s16pgj346da") (f (quote (("x962") ("x509") ("pkcs9") ("pkcs7") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.1.0 (c (n "oid-registry") (v "0.1.0") (d (list (d (n "der-parser") (r "^5.0.0-beta1") (d #t) (k 0)))) (h "0diway5s7d7v2azqg8dbj8pp1lksy71zzclls7z4m88d8nmy6nrf") (f (quote (("x962") ("x509") ("pkcs9") ("pkcs7") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.1.1 (c (n "oid-registry") (v "0.1.1") (d (list (d (n "der-parser") (r ">=5.0.0-beta2, <6.0.0") (d #t) (k 0)))) (h "048jcaca2v5i9y1zh8i0yj22da30lxb3j4di122ycnz5f3qwh215") (f (quote (("x962") ("x509") ("pkcs9") ("pkcs7") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.1.2 (c (n "oid-registry") (v "0.1.2") (d (list (d (n "der-parser") (r "^5.1") (d #t) (k 0)))) (h "0va5svixj5ysksc9n75pwif0v3zqkrr5fj4yxg11h7vd690lhhxj") (f (quote (("x962") ("x509") ("pkcs9") ("pkcs7") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.1.3 (c (n "oid-registry") (v "0.1.3") (d (list (d (n "der-parser") (r "^5.1") (d #t) (k 0)))) (h "0hxdqzgm7lg4cy2cp9z7p11qfzmbs3bf43g7vlgcgv5lhci08v5j") (f (quote (("x962") ("x509") ("pkcs9") ("pkcs7") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.1.4 (c (n "oid-registry") (v "0.1.4") (d (list (d (n "der-parser") (r "^5.1") (d #t) (k 0)))) (h "1ha7mdlcam83mfsvvw5w7dxyi15amqhaqxz696qwjrbw1vqmz0al") (f (quote (("x962") ("x509") ("pkcs9") ("pkcs7") ("pkcs12") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "pkcs12" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.1.5 (c (n "oid-registry") (v "0.1.5") (d (list (d (n "der-parser") (r "^5.1") (d #t) (k 0)))) (h "05a7ldcq043w3dzdjj9r4bcn6gg053ipj8dfv2nbx0sg8wzfgapn") (f (quote (("x962") ("x509") ("pkcs9") ("pkcs7") ("pkcs12") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "pkcs12" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.2.0-beta1 (c (n "oid-registry") (v "0.2.0-beta1") (d (list (d (n "der-parser") (r "^6.0.0-beta1") (d #t) (k 0)))) (h "1l4niis490iccyr92hl1qivjchqrqfhyffsma5536s666lf6w2wk") (f (quote (("x962") ("x509") ("pkcs9") ("pkcs7") ("pkcs12") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "pkcs12" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.2.0 (c (n "oid-registry") (v "0.2.0") (d (list (d (n "der-parser") (r "^6.0.0") (d #t) (k 0)))) (h "058qip5j5y0i95ckmw67mp73372rq16ci0lcczyq9irv76r4qmgy") (f (quote (("x962") ("x509") ("pkcs9") ("pkcs7") ("pkcs12") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "pkcs12" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.3.0 (c (n "oid-registry") (v "0.3.0") (d (list (d (n "asn1-rs") (r "^0.3") (d #t) (k 0)))) (h "1jxr4nqpcsgirl53yndhfhch1gzddkjh27z0p7rbsr2xngcpyj9n") (f (quote (("x962") ("x509") ("registry") ("pkcs9") ("pkcs7") ("pkcs12") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default" "registry") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "pkcs12" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.4.0 (c (n "oid-registry") (v "0.4.0") (d (list (d (n "asn1-rs") (r "^0.3") (d #t) (k 0)))) (h "0akbah3j8231ayrp2l1y5d9zmvbvqcsj0sa6s6dz6h85z8bhgqiq") (f (quote (("x962") ("x509") ("registry") ("pkcs9") ("pkcs7") ("pkcs12") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default" "registry") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "pkcs12" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.5.0 (c (n "oid-registry") (v "0.5.0") (d (list (d (n "asn1-rs") (r "^0.4") (d #t) (k 0)))) (h "0g4cdbr7mrc34dm8k79x30ap6k5xyq7p0c3q58nrbgsg6z6119db") (f (quote (("x962") ("x509") ("registry") ("pkcs9") ("pkcs7") ("pkcs12") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default" "registry") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "pkcs12" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.6.0 (c (n "oid-registry") (v "0.6.0") (d (list (d (n "asn1-rs") (r "^0.5") (d #t) (k 0)))) (h "0qb7dnrcpk02b4hasnffif1wf6rb9r2bam3fdsy4r10vzm1xljvx") (f (quote (("x962") ("x509") ("registry") ("pkcs9") ("pkcs7") ("pkcs12") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default" "registry") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "pkcs12" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.6.1 (c (n "oid-registry") (v "0.6.1") (d (list (d (n "asn1-rs") (r "^0.5") (d #t) (k 0)))) (h "1zwvjp3ad6gzn8g8w2hcn9a2xdap0lkzckhlnwp6rabbzdpz7vcv") (f (quote (("x962") ("x509") ("registry") ("pkcs9") ("pkcs7") ("pkcs12") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default" "registry") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "pkcs12" "nist_algs" "x962"))))))

(define-public crate-oid-registry-0.7.0 (c (n "oid-registry") (v "0.7.0") (d (list (d (n "asn1-rs") (r "^0.6") (d #t) (k 0)))) (h "07cdchd80199apgf1rxjf0b4dsrlnsdkcir57jf5n926a3a8v58w") (f (quote (("x962") ("x509") ("registry") ("pkcs9") ("pkcs7") ("pkcs12") ("pkcs1") ("nist_algs") ("ms_spc") ("kdf") ("default" "registry") ("crypto" "kdf" "pkcs1" "pkcs7" "pkcs9" "pkcs12" "nist_algs" "x962")))) (r "1.63")))

