(define-module (crates-io oi nt ointer) #:use-module (crates-io))

(define-public crate-ointer-0.1.0 (c (n "ointer") (v "0.1.0") (h "04z0pkxkys6mibyajcg40ybjrxf4916ra1f2dp5q9zznllali8fr") (y #t)))

(define-public crate-ointer-0.2.0 (c (n "ointer") (v "0.2.0") (h "0gk07fzhn20sncjwi2mlcz2rp5f81c9c4ilz9j1cw2bj8wcbk6s2") (y #t)))

(define-public crate-ointer-1.0.0 (c (n "ointer") (v "1.0.0") (h "0r98bp5n69xzikwss75mk0idhhbk5cxf139xavlady0514mk38cw") (y #t)))

(define-public crate-ointer-1.1.0 (c (n "ointer") (v "1.1.0") (h "1w13wbg9mxqsf450cpazg9gslisrwq7nggsxnbk67ss77fbadl0q") (y #t)))

(define-public crate-ointer-1.1.1 (c (n "ointer") (v "1.1.1") (h "1yyrkqqr1zm32vn09ngj6ab8i6yhnl8badq8d8ry0jrwhm0hawq8") (y #t)))

(define-public crate-ointer-2.0.0 (c (n "ointer") (v "2.0.0") (h "17hqmmbrcv1mw3xz5d7ra2q61dnfwkfis69hlxmrshpx2h1v7ibf") (y #t)))

(define-public crate-ointer-2.1.0 (c (n "ointer") (v "2.1.0") (h "1s3w58i2lhnji4j968wfyzirjar3h2p9kvx9mplsvxcqznwn96mb") (y #t)))

(define-public crate-ointer-2.1.1 (c (n "ointer") (v "2.1.1") (h "0yl3q7gf2bp2pjngvqi7v0micg5aqrkxaamrz9f6zlh6xlf74q16") (y #t)))

(define-public crate-ointer-3.0.0 (c (n "ointer") (v "3.0.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1717bj4wv1xyni16mys1c65amal8zqr2cn36mrzi4yyqyaqw8p9z") (y #t)))

(define-public crate-ointer-3.0.1 (c (n "ointer") (v "3.0.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0wl86pi4l0zqf7gc6964qvvx6cdw9nq7mr317w1w3g6cy3hj9sg5") (y #t)))

(define-public crate-ointer-3.0.2 (c (n "ointer") (v "3.0.2") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "107nhlpfn77d9yf9p8v52c2hf206zb2y7p5qlh6jmdayxcll7n9s") (y #t)))

(define-public crate-ointer-3.0.3 (c (n "ointer") (v "3.0.3") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1yv4zd9jxlrv02kh2dv2k85a27qs52hni2rk1p5ahyb4lsh67b5c") (y #t)))

(define-public crate-ointer-3.0.4 (c (n "ointer") (v "3.0.4") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0z8xhj4ilw7i6yhji00jzgwgfjcf9acrxnqx2msz30sqabpx121f") (y #t)))

(define-public crate-ointer-3.0.5 (c (n "ointer") (v "3.0.5") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0l4l86ll0gyjgdwi7pcb8pragzw5plsph01imap6g7l001ynv87g") (y #t)))

(define-public crate-ointer-3.0.6 (c (n "ointer") (v "3.0.6") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1sfdgs7z798x9n0qz65qihmqlgdpyi9zq07w1w2kdb4vv4qyhd3w") (y #t)))

(define-public crate-ointer-3.0.7 (c (n "ointer") (v "3.0.7") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1a0s5bswryhqi4q87fp70ymj2wn1kdi0l03k4b7lhcdk1g3dq0pj") (y #t)))

(define-public crate-ointer-3.0.8 (c (n "ointer") (v "3.0.8") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1wk1wmvzyx4z63hzl0pjrcwpzncc03836rapvzva4kg97m7h9lw2") (y #t)))

(define-public crate-ointer-3.0.9 (c (n "ointer") (v "3.0.9") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0rzkpqfqzsbh3wddsp7zz11iphd7kgz72mkbakhz73qiz30y0x1x") (y #t)))

(define-public crate-ointer-3.0.10 (c (n "ointer") (v "3.0.10") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "11zml9ka6123qmbw27blr1g1w075hiwl2zyfcfb8h3094g4yrczh") (y #t)))

(define-public crate-ointer-3.0.11 (c (n "ointer") (v "3.0.11") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0p0d0vidy3l40nv6islwyq38nnr1wixnkkbviivhj65nrdqk1lmb") (y #t)))

(define-public crate-ointer-3.0.12 (c (n "ointer") (v "3.0.12") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0q2sg2zqh509hi7i4fzpshvg5mnpk3p97ii7qyi0w52gcxzrajxy")))

