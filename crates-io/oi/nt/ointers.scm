(define-module (crates-io oi nt ointers) #:use-module (crates-io))

(define-public crate-ointers-1.0.0 (c (n "ointers") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0qwawfiisqs9zy4x9b6mdfnfyjbsgb31zcx0g8b9fagnxc0cip1y") (y #t)))

(define-public crate-ointers-2.0.0 (c (n "ointers") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "1zfrdq9rg69626gndkbwm52nr7r8qix8bm2gfzlg8czr67g7kwg3") (y #t)))

(define-public crate-ointers-3.0.0 (c (n "ointers") (v "3.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0645wpzvyi5fkl9nqjli5x0qm8rni00jsp6hqikwp01bar6r2n6m") (y #t)))

(define-public crate-ointers-3.0.1 (c (n "ointers") (v "3.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "16nnj8cr636w4fs09amvdrb4rxi9vxrgm26zwxmrvrxbykggb9d8") (f (quote (("i_know_what_im_doing") ("default")))) (y #t)))

(define-public crate-ointers-4.0.0 (c (n "ointers") (v "4.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)))) (h "0s31gfs51j1gqzvamwf4lgz689z99f49lvkl65f8i246327d4bmg") (f (quote (("i_know_what_im_doing") ("default" "alloc") ("alloc"))))))

(define-public crate-ointers-4.0.1 (c (n "ointers") (v "4.0.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "rayon") (r "^1.5.1") (d #t) (k 2)) (d (n "sptr") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1zjfqqk28r3zfbnhq2lww516zj0d8j5chysf8kxqvayv26y6rc12") (f (quote (("i_know_what_im_doing") ("default" "alloc") ("alloc"))))))

