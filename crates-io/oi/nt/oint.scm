(define-module (crates-io oi nt oint) #:use-module (crates-io))

(define-public crate-oint-0.1.0 (c (n "oint") (v "0.1.0") (h "1mdl9964a5c3b1r31pixwgrz5iqw3wmkl3x8rwa8z9cdrvhwl1yn")))

(define-public crate-oint-0.1.1 (c (n "oint") (v "0.1.1") (h "0qada6n4vvk5kn9nxyw3van70bsidy47yxjjbk76hhq9j86nv58h")))

