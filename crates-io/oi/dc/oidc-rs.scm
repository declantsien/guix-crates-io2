(define-module (crates-io oi dc oidc-rs) #:use-module (crates-io))

(define-public crate-oidc-rs-0.1.0 (c (n "oidc-rs") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "08yfg34xs3msz3fd5wrxigpv6v0km78kv5vpm8g2r44yng4fvnk2") (f (quote (("default")))) (s 2) (e (quote (("ureq" "dep:ureq") ("reqwest" "dep:reqwest"))))))

(define-public crate-oidc-rs-0.1.1 (c (n "oidc-rs") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.139") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json"))) (o #t) (d #t) (k 0)))) (h "1bqxdcyb3lci12l7s6i3cmp5xzl6ry6knwwzip38rm5lzjp5lqgp") (f (quote (("default")))) (s 2) (e (quote (("ureq" "dep:ureq") ("reqwest" "dep:reqwest"))))))

