(define-module (crates-io oi l_ oil_parsers) #:use-module (crates-io))

(define-public crate-oil_parsers-0.1.0 (c (n "oil_parsers") (v "0.1.0") (d (list (d (n "num") (r "*") (d #t) (k 0)) (d (n "oil_shared") (r "= 0.1.0") (d #t) (k 0)) (d (n "phf") (r "*") (d #t) (k 0)) (d (n "phf_macros") (r "*") (d #t) (k 0)) (d (n "xml-rs") (r "*") (d #t) (k 0)))) (h "1pgw1rrr0if5i8cv5bypa9y8kz1m46cgflr9hcdbbsmbz39g6yk7")))

