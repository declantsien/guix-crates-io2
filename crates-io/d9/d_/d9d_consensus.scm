(define-module (crates-io d9 d_ d9d_consensus) #:use-module (crates-io))

(define-public crate-d9d_consensus-0.1.0 (c (n "d9d_consensus") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("async-await" "compat" "nightly"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "143bb91w6r3r9g97nr7a02rpfcdaqwmjx5ifdlm2l4ylpa43w1l8")))

