(define-module (crates-io d9 d_ d9d_p2p) #:use-module (crates-io))

(define-public crate-d9d_p2p-0.1.0 (c (n "d9d_p2p") (v "0.1.0") (d (list (d (n "base64") (r "^0.10") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "futures-preview") (r "^0.3.0-alpha.16") (f (quote ("async-await" "compat" "nightly"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "068x5yd166ln8i7ldk5bzfglw7bj3nrkfjbw6dyfil97vjwffklw")))

