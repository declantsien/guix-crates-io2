(define-module (crates-io a- ma a-mazed) #:use-module (crates-io))

(define-public crate-A-Mazed-0.1.0 (c (n "A-Mazed") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00b101pa4dvl3kkzp15920wlgdr8xd5c6iy8xnhg84ijrynlbpn3")))

