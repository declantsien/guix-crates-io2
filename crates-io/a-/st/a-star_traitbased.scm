(define-module (crates-io a- st a-star_traitbased) #:use-module (crates-io))

(define-public crate-a-star_traitbased-0.1.0 (c (n "a-star_traitbased") (v "0.1.0") (h "0568gwrlyh1gvpq4n3jvc147iajzj4kch0kblyiv8vav2xa0z71i")))

(define-public crate-a-star_traitbased-0.1.1 (c (n "a-star_traitbased") (v "0.1.1") (h "1plcqc4lhgx77fymmh6c0alpiqysaad15dpacdwf0khd2iixhn10")))

(define-public crate-a-star_traitbased-0.1.2 (c (n "a-star_traitbased") (v "0.1.2") (h "14xpvg0p59rsygg0dvysag1n2p529kr18vi2c2isfy46c9hdwcc8")))

(define-public crate-a-star_traitbased-0.1.3 (c (n "a-star_traitbased") (v "0.1.3") (h "1dxfwrnim6l95jshf0d1agmzsnwbjq9zx87waicqkw9vagxc1xi9")))

