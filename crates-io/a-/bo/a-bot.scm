(define-module (crates-io a- bo a-bot) #:use-module (crates-io))

(define-public crate-a-bot-0.1.0 (c (n "a-bot") (v "0.1.0") (h "1sdxr1m63h77rmrlbrrj46a9kxylw0hy7b7c70ssvhsbw6048gnk") (y #t)))

(define-public crate-a-bot-0.1.1 (c (n "a-bot") (v "0.1.1") (h "086qymj76669ydbkdb5w3zs4iaghpjpjwss6wgc2pvrj3j87rhjx") (y #t)))

(define-public crate-a-bot-0.0.1 (c (n "a-bot") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 0)))) (h "12b5p7pl4bqb1jsqhgb8qzz3yn0wl1sshg6cvn9qvrygx6bj8wz8") (y #t)))

