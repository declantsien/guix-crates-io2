(define-module (crates-io z_ os z_osmf_core) #:use-module (crates-io))

(define-public crate-z_osmf_core-0.1.0 (c (n "z_osmf_core") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ra0fhcyqiz3w8xb8rqwgw2znwmpbw7yd6bdhvsb5nzc3xm153c7")))

(define-public crate-z_osmf_core-0.1.1 (c (n "z_osmf_core") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fdxamzvdqxzs7kv7mdkbs4ds84fvp7i29cw58rlv3cyjr84mx2k")))

(define-public crate-z_osmf_core-0.2.0 (c (n "z_osmf_core") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09cllrc5pk8zm3429dd4zip2cvh7s2qf0bkfykng6pdin8pc98m1")))

