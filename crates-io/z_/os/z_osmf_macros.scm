(define-module (crates-io z_ os z_osmf_macros) #:use-module (crates-io))

(define-public crate-z_osmf_macros-0.1.0 (c (n "z_osmf_macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1414iz45whjxgj5ypr4k698cs9xgl8chys94dxqpkq9j25njdy89")))

(define-public crate-z_osmf_macros-0.1.1 (c (n "z_osmf_macros") (v "0.1.1") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1zns6r3szlf6r8nzn6834zld0il9j81vc8gg50gcwfiqfslr05s9")))

(define-public crate-z_osmf_macros-0.2.0 (c (n "z_osmf_macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0y9laxrw31r0m36jx5fpcs9vviayfv0m30nl8kvmmhyrprzpp08c")))

(define-public crate-z_osmf_macros-0.3.0 (c (n "z_osmf_macros") (v "0.3.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0raj2jz7a1k4ih8g5w9a8i545w8db8vr0kk4dzp8yv4axc2bcz2k")))

(define-public crate-z_osmf_macros-0.4.0 (c (n "z_osmf_macros") (v "0.4.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0mh69fr3j5nb3v78ijnmabcmj8fc9lqwnwl1xnbg66p4704c74kc")))

(define-public crate-z_osmf_macros-0.5.0 (c (n "z_osmf_macros") (v "0.5.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0l7gn6yz4c3kzc4fiv4k5dkplr6kfcs437bb3wh59c01icsdg74m")))

(define-public crate-z_osmf_macros-0.6.0 (c (n "z_osmf_macros") (v "0.6.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "01a82cwphm4k3g02ncy71hdq1c782gax8hgspij25lmi6cjwgfgw")))

(define-public crate-z_osmf_macros-0.7.0 (c (n "z_osmf_macros") (v "0.7.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1mb9mx111k60334zx5cadrkvix2v4qkqvv5s696gqbbxan1d9i03")))

(define-public crate-z_osmf_macros-0.8.0 (c (n "z_osmf_macros") (v "0.8.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0d5203hdb9ymwfcyy7iyy62p0ba5figbmcmlr191k2jfjl9nsg6z")))

(define-public crate-z_osmf_macros-0.9.0 (c (n "z_osmf_macros") (v "0.9.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1177ilig08ih52dx5ziflq0rn8vk6wj1xjjbibvsdf16ynygm6vh")))

(define-public crate-z_osmf_macros-0.10.0 (c (n "z_osmf_macros") (v "0.10.0") (d (list (d (n "darling") (r "^0.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0p7z6c3pkx6isw9mh0svj5pkb8y8a9whn0d13ajpshva3vgxg3x0")))

