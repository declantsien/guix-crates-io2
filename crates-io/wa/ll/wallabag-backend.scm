(define-module (crates-io wa ll wallabag-backend) #:use-module (crates-io))

(define-public crate-wallabag-backend-0.1.0 (c (n "wallabag-backend") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)) (d (n "wallabag-api") (r "^0.1.0") (d #t) (k 0)))) (h "1qf1djhgjnlnvblpgkd1slnfssijbnz44xax4r3gnj5vpxwwb852") (y #t)))

