(define-module (crates-io wa ll wall-cli) #:use-module (crates-io))

(define-public crate-wall-cli-0.1.0 (c (n "wall-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "wall") (r "^0.2.0") (d #t) (k 0)))) (h "0dgfj5hw851j3r1lpqd3vc45j9408fwlyidknv6krii8pm08rfiz") (y #t)))

(define-public crate-wall-cli-0.1.1 (c (n "wall-cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "wall") (r "^0.2.0") (d #t) (k 0)))) (h "1l7kwya4ijcsaqq2ilz77qicf3vfy01fd2ay7wf0f472dlmnqlk1") (y #t)))

(define-public crate-wall-cli-0.1.2 (c (n "wall-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "wall") (r "^0.2.0") (d #t) (k 0)))) (h "0a0hgjrfazjhxsv238iziqicbakm4jnvdqpnx8m5207xz109kgxa")))

