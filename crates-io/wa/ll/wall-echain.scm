(define-module (crates-io wa ll wall-echain) #:use-module (crates-io))

(define-public crate-wall-echain-0.1.0 (c (n "wall-echain") (v "0.1.0") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (f (quote ("encoding" "hyper-client"))) (k 0)))) (h "16q4dyz44c38vd7ad0hm3al18av1arfypwxq57385qmhc8gc9kk1")))

(define-public crate-wall-echain-0.1.1 (c (n "wall-echain") (v "0.1.1") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (f (quote ("encoding" "hyper-client"))) (k 0)))) (h "1qsw5z2zp69fr618qd60p1p7d5mzhzwj3slzlkf2mxpv67f3s4vh")))

(define-public crate-wall-echain-0.1.2 (c (n "wall-echain") (v "0.1.2") (d (list (d (n "async-std") (r "^1.12.0") (f (quote ("attributes" "tokio1"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (f (quote ("encoding" "hyper-client"))) (k 0)))) (h "0jwzmziq3hwxk6wf2fg46v4lps1yj2f2r02bf00nl6sg4d3d673h")))

