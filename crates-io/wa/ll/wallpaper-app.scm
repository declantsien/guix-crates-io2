(define-module (crates-io wa ll wallpaper-app) #:use-module (crates-io))

(define-public crate-wallpaper-app-0.1.0 (c (n "wallpaper-app") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (k 0)))) (h "03sq8864nmswsn7hgncjq71lndrshqkn9md4sbmc4avcjf0gm3im")))

(define-public crate-wallpaper-app-0.1.1 (c (n "wallpaper-app") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (k 0)))) (h "0la39285cx1z1cc955ymg48idqk3f90vxysh4x0lykkd9gmnmd0g")))

(define-public crate-wallpaper-app-0.1.2 (c (n "wallpaper-app") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (k 0)))) (h "0lrgw4066g7x6ymw1r9329yc29m7h1wpmwqvkf6f5cjxplmzqq0n")))

(define-public crate-wallpaper-app-0.1.3 (c (n "wallpaper-app") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (k 0)))) (h "1ffl92raf90qxfi7q7rjmhhf99s6n5hvh4hbrlzsxa8hcyl6b7m0")))

(define-public crate-wallpaper-app-0.1.4 (c (n "wallpaper-app") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yxnxmayfccrxlc8s5z93m4k5bla0ivs68wlnhv3m8an9gn7symr")))

(define-public crate-wallpaper-app-0.1.5 (c (n "wallpaper-app") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1yfh3pmhj0n04h1k4qi9jdw7ymq8qbmlw22g6b3iaggmfg31qdf6")))

(define-public crate-wallpaper-app-0.1.6 (c (n "wallpaper-app") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07wkghiih2wxs9yjn4wv213b0p3ji7mip7pwn94774kn9r3mpc5d")))

(define-public crate-wallpaper-app-0.1.7 (c (n "wallpaper-app") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zpwp5qrgnv489famj29sci2lk0sfsn00dciv4dqzwhfmfz5y39k")))

(define-public crate-wallpaper-app-0.1.8 (c (n "wallpaper-app") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser" "processthreadsapi" "libloaderapi" "errhandlingapi" "impl-default"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09j963vy27p9bwbpnfx4zwm9m23x49v15nnd1msdhfzpil269xi3")))

