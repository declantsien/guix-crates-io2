(define-module (crates-io wa ll wallpaper-dl) #:use-module (crates-io))

(define-public crate-wallpaper-dl-0.1.0 (c (n "wallpaper-dl") (v "0.1.0") (d (list (d (n "apputils") (r "^0.1") (d #t) (k 0)) (d (n "blake3") (r "^1.5") (f (quote ("serde" "neon"))) (d #t) (k 0)) (d (n "mailparse") (r "^0.14") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking" "rustls-tls-native-roots" "deflate" "gzip" "brotli" "json" "charset" "multipart"))) (k 0)) (d (n "scraper") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.8") (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "132rh7hx6x47h7zalk2innb80c870as0x5x8lz4h1qa9424r1a9j")))

(define-public crate-wallpaper-dl-0.2.0 (c (n "wallpaper-dl") (v "0.2.0") (d (list (d (n "apputils") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("blocking" "rustls-tls-native-roots" "deflate" "gzip" "brotli" "json" "charset"))) (k 0)) (d (n "scraper") (r "^0.19") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.5") (d #t) (k 0)))) (h "1ydzc44s7cwl691nzan3rs84pls4dn85nwl1mgymrnk8y20f6yq9")))

