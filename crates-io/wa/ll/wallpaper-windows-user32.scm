(define-module (crates-io wa ll wallpaper-windows-user32) #:use-module (crates-io))

(define-public crate-wallpaper-windows-user32-0.1.0 (c (n "wallpaper-windows-user32") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0yjq9613wl4ydh69wqlxdl0227vg5m9g6ivjq4yz8d8jcp1ym2ss")))

(define-public crate-wallpaper-windows-user32-0.1.1 (c (n "wallpaper-windows-user32") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "15is95nnihpnibq33dc8k7ma6dakfm85n37zkz19pd58y5xj0nb7")))

(define-public crate-wallpaper-windows-user32-0.1.2 (c (n "wallpaper-windows-user32") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "09vzn2cz44z1kbbr9b4bcaljvwdabx4lskyjlqi678sp8bq8xrrc")))

(define-public crate-wallpaper-windows-user32-0.2.0 (c (n "wallpaper-windows-user32") (v "0.2.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0gk16lbaf0lxvlyc73dk7h1q8wx2hddnj3z3jmprs5h3lcz0qpyg")))

(define-public crate-wallpaper-windows-user32-0.3.0 (c (n "wallpaper-windows-user32") (v "0.3.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "18gwli97szjng86p73g4vdn555jdrgcaqifzyvmwrg9v99lvpmwf")))

(define-public crate-wallpaper-windows-user32-0.4.0 (c (n "wallpaper-windows-user32") (v "0.4.0") (d (list (d (n "widestring") (r "^0.4") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "0ghis7dxq12201qxvj4ixcxg60095kw987dby4h7r55wpdlrcshh")))

