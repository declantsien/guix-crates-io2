(define-module (crates-io wa ll wallet-keychain) #:use-module (crates-io))

(define-public crate-wallet-keychain-0.1.0 (c (n "wallet-keychain") (v "0.1.0") (d (list (d (n "uniffi") (r "^0.23.0") (d #t) (k 0)) (d (n "uniffi_build") (r "^0.23.0") (f (quote ("builtin-bindgen"))) (d #t) (k 1)) (d (n "uniffi_macros") (r "^0.23.0") (d #t) (k 0)))) (h "1cf42lf078csv1gjwjw8yqahsnn8023j0rswb2dxwfklzmcrrf58")))

