(define-module (crates-io wa ll walloc) #:use-module (crates-io))

(define-public crate-walloc-0.1.0 (c (n "walloc") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0d0kz5z4pvvsgqywml7ckp2l2m12wmbpl6wmpwhim2gbjw2gzymr")))

(define-public crate-walloc-0.1.1 (c (n "walloc") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1xw80wqsmgnd0qcvk566dzzdh4nqiar4l97vxf57j9rm7ad1bx9a")))

