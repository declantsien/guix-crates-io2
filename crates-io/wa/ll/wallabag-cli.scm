(define-module (crates-io wa ll wallabag-cli) #:use-module (crates-io))

(define-public crate-wallabag-cli-0.1.0 (c (n "wallabag-cli") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)) (d (n "simplelog") (r "^0.5.3") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)) (d (n "toml") (r "^0.4.10") (d #t) (k 0)) (d (n "wallabag-backend") (r "^0.1.0") (d #t) (k 0)))) (h "1zhhll987iv1bl5pbnkln6qgribn31fj4n3jnc1r32mqrqnz0yfi") (y #t)))

