(define-module (crates-io wa ll wallet-dat) #:use-module (crates-io))

(define-public crate-wallet-dat-0.1.0 (c (n "wallet-dat") (v "0.1.0") (d (list (d (n "aes") (r "^0.7") (d #t) (k 0)) (d (n "blake3") (r "^1.3") (d #t) (k 0)) (d (n "block-modes") (r "^0.8") (d #t) (k 0)) (d (n "dusk-bytes") (r "^0.1.7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "14yarg933xaaq9hxd0vp6pxaavwki84x0w1a2pd436881mx5bdw2")))

