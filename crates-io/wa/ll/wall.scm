(define-module (crates-io wa ll wall) #:use-module (crates-io))

(define-public crate-wall-0.1.0 (c (n "wall") (v "0.1.0") (h "0a1ab0z8700fil6vylzyq2d5r9g5jlnsdh231l3d3lsgihvbzgri") (y #t)))

(define-public crate-wall-0.1.1 (c (n "wall") (v "0.1.1") (h "1pgcjn66x77nln1q8i3133bva45vkdczcw47far305v6ik4242rq") (y #t)))

(define-public crate-wall-0.2.0 (c (n "wall") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1m3bhhccc766cadc5kv2yrmfax35i4sm3hhpybi8gzhq3av5xgi4") (y #t)))

(define-public crate-wall-0.2.1 (c (n "wall") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0f1r3fd1fpi7ilvlnsyxzcpkp298rw4w7y5mpwgxqxb5lj44km21") (y #t)))

(define-public crate-wall-0.2.2 (c (n "wall") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1lvzk2m1mlxlm813izi94j0si2c1wivnr8wlq1xfsvrwx9al9dwi") (y #t)))

(define-public crate-wall-0.2.3 (c (n "wall") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "image") (r "^0.23.9") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)) (d (n "x11") (r "^2.18.2") (f (quote ("xlib"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "1xd3i1pcbp6sawyin0bqnivncz8q5n0mcxsha342pqx6jw3azlv5")))

