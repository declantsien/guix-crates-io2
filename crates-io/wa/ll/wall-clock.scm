(define-module (crates-io wa ll wall-clock) #:use-module (crates-io))

(define-public crate-wall-clock-0.1.0 (c (n "wall-clock") (v "0.1.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "diesel") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "080cz96lfd946jdli5in1g1lqmqa5dxn0idk361yqd25mjsd4sxi") (f (quote (("default" "diesel-pg" "serde")))) (s 2) (e (quote (("diesel-pg" "dep:diesel" "diesel/postgres")))) (r "1.70")))

(define-public crate-wall-clock-0.2.0 (c (n "wall-clock") (v "0.2.0") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "diesel") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1cb5risr12cmlib6pjyzxnkpvi03rry5dfpqckcwsn1yvm97yizk") (f (quote (("default" "serde")))) (s 2) (e (quote (("diesel-pg" "dep:diesel" "diesel/postgres")))) (r "1.70")))

(define-public crate-wall-clock-0.2.1 (c (n "wall-clock") (v "0.2.1") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "diesel") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "037i5sa255b6cdajlf95czs7q2b72sl2rdrqp080xqj2bdm218wj") (f (quote (("default" "serde")))) (s 2) (e (quote (("diesel-pg" "dep:diesel" "diesel/postgres")))) (r "1.70")))

(define-public crate-wall-clock-0.2.2 (c (n "wall-clock") (v "0.2.2") (d (list (d (n "assert2") (r "^0.3") (d #t) (k 2)) (d (n "diesel") (r "^2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)) (d (n "strptime") (r "^0.2") (d #t) (k 0)))) (h "12ihlyk5i0i85zxkr422rqpn8k9g58qfsg4j7kapy390sll4x04y") (f (quote (("default" "serde")))) (s 2) (e (quote (("diesel-pg" "dep:diesel" "diesel/postgres")))) (r "1.70")))

