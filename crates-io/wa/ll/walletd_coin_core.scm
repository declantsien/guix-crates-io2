(define-module (crates-io wa ll walletd_coin_core) #:use-module (crates-io))

(define-public crate-walletd_coin_core-0.1.0 (c (n "walletd_coin_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.6") (d #t) (k 0)) (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "walletd_hd_key") (r "^0.1") (d #t) (k 0)) (d (n "walletd_mnemonics_core") (r "^0.1") (d #t) (k 0)))) (h "0cdwy16kcf7r0ikxfi4l5kivy33d6mfvpxcbyv4azhc1ifi6lm1c")))

(define-public crate-walletd_coin_core-0.2.0 (c (n "walletd_coin_core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.6") (d #t) (k 0)) (d (n "base58") (r "^0.2.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "walletd_hd_key") (r "^0.2") (d #t) (k 0)) (d (n "walletd_mnemonics_core") (r "^0.2") (d #t) (k 0)))) (h "0qa8ccs26q7mfh1a63a9wapvvl23fidfccphhlils5nsxdjm5i3h")))

