(define-module (crates-io wa ll walletgen) #:use-module (crates-io))

(define-public crate-walletgen-0.1.0 (c (n "walletgen") (v "0.1.0") (d (list (d (n "acryl") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "01i04ky37y51bbk2qbn62rrz7gycscbfgg7cd1aaja0fss4bbbgm")))

(define-public crate-walletgen-0.1.1 (c (n "walletgen") (v "0.1.1") (d (list (d (n "acryl") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "0z79bqr8r81zan9cd5j9d58k46jnh7nap8yn4bv4044s3fa4h3i8")))

(define-public crate-walletgen-0.1.2 (c (n "walletgen") (v "0.1.2") (d (list (d (n "acryl") (r "^0.4.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1m20qlvds0j9q8nax2vxn2i61qwyvl2s1lrqv6sddilixby7520g")))

(define-public crate-walletgen-0.1.3 (c (n "walletgen") (v "0.1.3") (d (list (d (n "acryl") (r "^0.4.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "09pl2l8hyva25h9bxckx9ild8nffj6a4b9d7qic27w1zivcdx0ig")))

(define-public crate-walletgen-0.2.0 (c (n "walletgen") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cyber") (r "^0.1.3") (d #t) (k 0)))) (h "17zkzsdmyk3351sdfgciry9w9805pdlzwkmv0gs8plgyw2rzp15h")))

(define-public crate-walletgen-1.0.0 (c (n "walletgen") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cyber") (r "^0.1.3") (d #t) (k 0)) (d (n "data-encoding") (r "^2.2.1") (d #t) (k 0)) (d (n "sp-core") (r "^2.0.1") (d #t) (k 0)) (d (n "wallet-gen") (r "^0.4.1") (d #t) (k 0)))) (h "0wq38k535cy3bgr8fls619sn8m53pqinqyksn9p35ffbpnswyfk9")))

(define-public crate-walletgen-1.0.1 (c (n "walletgen") (v "1.0.1") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "cyber") (r "^0.1.4") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "sp-core") (r "^6.0.0") (d #t) (k 0)) (d (n "wallet-gen") (r "^0.4.1") (d #t) (k 0)))) (h "1zqiljlky47m7874ivlvab52d9264mpxvzmjq66jbi0rffshwb7v")))

(define-public crate-walletgen-1.1.0 (c (n "walletgen") (v "1.1.0") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "cyber") (r "^1.0.1") (d #t) (k 0)) (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "sp-core") (r "^7.0.0") (d #t) (k 0)) (d (n "wallet-gen") (r "^0.4.1") (d #t) (k 0)))) (h "1w87h88wpzq733majg3r5d5dg5sf22hdsk41jf4kgcq97gyb8c66")))

(define-public crate-walletgen-1.1.1 (c (n "walletgen") (v "1.1.1") (d (list (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "cyber") (r "^1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.4") (d #t) (k 0)) (d (n "sp-core") (r "^20.0.0") (d #t) (k 0)) (d (n "wallet-gen") (r "^0.4") (d #t) (k 0)))) (h "05vwjfx2akqym4d7myssnphiyi4qd5vbs1r7nk3livqxw0l12phh")))

(define-public crate-walletgen-1.2.0 (c (n "walletgen") (v "1.2.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cyber") (r "^1.0") (d #t) (k 0)) (d (n "data-encoding") (r "^2.4") (d #t) (k 0)) (d (n "sp-core") (r "^20.0.0") (d #t) (k 0)) (d (n "wallet-gen") (r "^0.4") (d #t) (k 0)))) (h "0kypjx6rnj45maxrvmblhrppxqcl0k1pim8clvykvd4a00x6r5rf")))

