(define-module (crates-io wa ll walleth) #:use-module (crates-io))

(define-public crate-walleth-0.1.0 (c (n "walleth") (v "0.1.0") (d (list (d (n "bip32") (r "^0.5.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r ">=0.12, <=0.13") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.9.0") (f (quote ("stream"))) (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "pbkdf2") (r "^0.12.2") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("std"))) (d #t) (k 0)) (d (n "secp256k1") (r "^0.27.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "0i6wryh9p1b534gqxv7n2416rypzq2sxjf5j4czl0zn58h1y6v1j")))

