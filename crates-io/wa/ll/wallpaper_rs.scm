(define-module (crates-io wa ll wallpaper_rs) #:use-module (crates-io))

(define-public crate-wallpaper_rs-0.1.0 (c (n "wallpaper_rs") (v "0.1.0") (d (list (d (n "enquote") (r "^1.0.3") (d #t) (k 0)))) (h "17w1y7pxa9711dr61dmll8n7vfhp4aw0mfb7gylx72x1hajqfifg")))

(define-public crate-wallpaper_rs-0.1.1 (c (n "wallpaper_rs") (v "0.1.1") (d (list (d (n "dirs-next") (r "^2.0.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "enquote") (r "^1.0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1rf6imnlq45bxrqwgv3y0l273ka3qk0jk27zj8wkbk4bhzavx1jm")))

