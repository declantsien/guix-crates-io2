(define-module (crates-io wa ll wallee) #:use-module (crates-io))

(define-public crate-wallee-0.1.2 (c (n "wallee") (v "0.1.2") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.45") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0gyxlwd1kn9wd5lh49wraw3n7f11iwapjld35w3dx6yfgsyqzb4p") (f (quote (("std") ("default" "std")))) (r "1.76")))

(define-public crate-wallee-0.1.3 (c (n "wallee") (v "0.1.3") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.45") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0jsp0v7k0v6mrmflwn5b4w4dadwd3lvgrmn8m964rhfchmpvj4ax") (f (quote (("std") ("default" "std") ("backtrace")))) (r "1.76")))

(define-public crate-wallee-0.1.4 (c (n "wallee") (v "0.1.4") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.45") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "096q7jm1kfl33vydd8kagxxf4xls40sn700ms2sr0syqi1f714z0") (r "1.76")))

(define-public crate-wallee-0.1.5 (c (n "wallee") (v "0.1.5") (d (list (d (n "futures") (r "^0.3") (k 2)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.45") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0x10s606ap7h95dvd823wbn8g7zjfjyd3fgzqm2hn8jqjakw4b73") (r "1.76")))

(define-public crate-wallee-0.1.6 (c (n "wallee") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 2)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.45") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zbzlpd2iv0v45xqq22v23dkscrm71bc0gd6q8nm7szw9dwgd0di") (r "1.76")))

(define-public crate-wallee-0.1.7 (c (n "wallee") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 2)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.45") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1pn7m9a17c8142ngbl451v5kad40ll4srbyjag430l9vkn6ynmx3") (r "1.76")))

(define-public crate-wallee-0.2.0 (c (n "wallee") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 2)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.45") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ijhzrmjyj43xl6gn4y5vn8b73fmpc3ks7apy1vr183412lbga3x") (r "1.76")))

(define-public crate-wallee-0.2.1 (c (n "wallee") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "futures") (r "^0.3") (k 2)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.45") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0ni40449vn979mfgpz536ii8z99pk1js6hqg62cirqy7x7m3hd8r") (r "1.76")))

