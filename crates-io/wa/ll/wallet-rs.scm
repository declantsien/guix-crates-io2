(define-module (crates-io wa ll wallet-rs) #:use-module (crates-io))

(define-public crate-wallet-rs-0.1.0 (c (n "wallet-rs") (v "0.1.0") (d (list (d (n "uniffi") (r "^0.23.0") (d #t) (k 0)) (d (n "uniffi_build") (r "^0.23.0") (f (quote ("builtin-bindgen"))) (d #t) (k 1)) (d (n "uniffi_macros") (r "^0.23.0") (d #t) (k 0)))) (h "1851ir3sk51g6h8in2srh65ppz0mpaj79swrz58yi9dfiqgh0gzj")))

(define-public crate-wallet-rs-0.2.0 (c (n "wallet-rs") (v "0.2.0") (d (list (d (n "uniffi") (r "^0.23.0") (d #t) (k 0)) (d (n "uniffi_build") (r "^0.23.0") (f (quote ("builtin-bindgen"))) (d #t) (k 1)) (d (n "uniffi_macros") (r "^0.23.0") (d #t) (k 0)))) (h "1npiifq385mg2s801mlhb8sn6cb5aq5x277vlazsv788imm5f9pd")))

(define-public crate-wallet-rs-0.3.0 (c (n "wallet-rs") (v "0.3.0") (d (list (d (n "uniffi") (r "^0.23.0") (d #t) (k 0)) (d (n "uniffi_build") (r "^0.23.0") (f (quote ("builtin-bindgen"))) (d #t) (k 1)) (d (n "uniffi_macros") (r "^0.23.0") (d #t) (k 0)))) (h "0ifrwlr3gmj3lg194hzh4fxjfgrahsbfw2ngx7wa140c96jh06jy")))

(define-public crate-wallet-rs-0.5.0 (c (n "wallet-rs") (v "0.5.0") (d (list (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "uniffi") (r "^0.23.0") (d #t) (k 0)) (d (n "uniffi_build") (r "^0.23.0") (f (quote ("builtin-bindgen"))) (d #t) (k 1)) (d (n "uniffi_macros") (r "^0.23.0") (d #t) (k 0)) (d (n "wallet-signer") (r "^0.1.0") (k 0)))) (h "08psi6wh66ql9zqfdlkvi1afych2h36nq53vnwrmkxq9gny2b3wi")))

