(define-module (crates-io wa ll walle-macro) #:use-module (crates-io))

(define-public crate-walle-macro-0.6.0 (c (n "walle-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0r6hx9d83hp0aj341n6h6npll0pgnghzp93lyq287hmh3khl8hyn")))

(define-public crate-walle-macro-0.7.0-a1 (c (n "walle-macro") (v "0.7.0-a1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "041wg9yy45lbmy36fda6grrqr2h043c1bznfrg56wgdwl47vabmz")))

(define-public crate-walle-macro-0.7.0-a2 (c (n "walle-macro") (v "0.7.0-a2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "11dj4v7skfv053vpf1vx81l1bj45049h0h49dkshhljs639pap2n")))

