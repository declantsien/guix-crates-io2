(define-module (crates-io wa ll wally-vibe) #:use-module (crates-io))

(define-public crate-wally-vibe-0.0.4 (c (n "wally-vibe") (v "0.0.4") (d (list (d (n "buttplug") (r "^6.3.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.25") (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nf28piixw134wglakx5np29l9y2pgz1sa73mn54rma3x4hparh3")))

