(define-module (crates-io wa ll wallpepper) #:use-module (crates-io))

(define-public crate-wallpepper-1.0.0 (c (n "wallpepper") (v "1.0.0") (d (list (d (n "ffmpeg-next") (r "^5.1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("image"))) (k 0)) (d (n "x11") (r "^2.20.0") (f (quote ("xlib"))) (k 0)))) (h "1vasf1h246ghb6bn0368110sk2y7rc48hkbx3fccx67h29hnq859") (y #t)))

(define-public crate-wallpepper-1.0.1 (c (n "wallpepper") (v "1.0.1") (d (list (d (n "ffmpeg-next") (r "^5.1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (k 0)) (d (n "x11") (r "^2.20.0") (f (quote ("xlib"))) (k 0)))) (h "0xwyy803zqph5jmqm5wk8cd2d5pwsnw34vzjvpywr3kr0lwdf8r9")))

(define-public crate-wallpepper-1.1.0 (c (n "wallpepper") (v "1.1.0") (d (list (d (n "ffmpeg-next") (r "^5.1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("gfx"))) (k 0)) (d (n "x11") (r "^2.20.0") (f (quote ("xlib"))) (k 0)))) (h "00agn34wk7h8p2yz0rb86f8akak4dsib4l26zm2a5r7g9c8k86x7")))

(define-public crate-wallpepper-1.1.1 (c (n "wallpepper") (v "1.1.1") (d (list (d (n "ffmpeg-next") (r "^5.1.1") (d #t) (k 0)) (d (n "sdl2") (r "^0.35.2") (f (quote ("gfx"))) (k 0)) (d (n "x11") (r "^2.20.0") (f (quote ("xlib"))) (k 0)))) (h "0082741vrs325ksyyjsv5y4li52ddi3cgcnskxxm0j6snaa5i88b")))

