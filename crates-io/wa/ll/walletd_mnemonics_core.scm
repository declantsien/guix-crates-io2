(define-module (crates-io wa ll walletd_mnemonics_core) #:use-module (crates-io))

(define-public crate-walletd_mnemonics_core-0.1.0 (c (n "walletd_mnemonics_core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0nzy15dbbn9p8bpdnhg0am0m1y99jpxdn8xixi4yizv36kfclgnk")))

(define-public crate-walletd_mnemonics_core-0.2.0 (c (n "walletd_mnemonics_core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1xxzpaad2z9gmjj4xda3ifns8xwgvswqq090x6izz0h6yb2m7c0b")))

