(define-module (crates-io wa nd wandio-sys) #:use-module (crates-io))

(define-public crate-wandio-sys-0.0.1 (c (n "wandio-sys") (v "0.0.1") (h "1238d89fdj9bk1kqdp2wx2c1900iy9sbn802lbv27nkf2hb0631k")))

(define-public crate-wandio-sys-0.1.0 (c (n "wandio-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "02g9c0nkjjldddbxjblz7p67b1fcw8xzakfv912dlxi5f2lc0b7k") (l "wandio")))

(define-public crate-wandio-sys-0.2.0 (c (n "wandio-sys") (v "0.2.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1r2naihadv1rvgk5bk79jrxgjnyasym9xwz1b2k1whfwzhzkhc0y") (l "wandio")))

