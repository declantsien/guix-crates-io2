(define-module (crates-io wa nd wander-repl) #:use-module (crates-io))

(define-public crate-wander-repl-0.1.0 (c (n "wander-repl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "wander") (r "^0.2.0") (d #t) (k 0)))) (h "15rq17mp2208lj27n45ylf4304pyn51gg82divcrcnaqkcb8wzzg")))

(define-public crate-wander-repl-0.3.0 (c (n "wander-repl") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "wander") (r "^0.3.0") (d #t) (k 0)))) (h "0cl9hhi16rh703d18d188m84l51zb99z89d5yhvq9ypffw7wfyiw")))

(define-public crate-wander-repl-0.4.0 (c (n "wander-repl") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "wander") (r "^0.4.0") (d #t) (k 0)))) (h "07m83g7gd7d0ynnv8d6mi00grsglbf7ba6qljzzjssnk0zx0nj37")))

(define-public crate-wander-repl-0.5.0 (c (n "wander-repl") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "tabled") (r "^0.14.0") (d #t) (k 0)) (d (n "wander") (r "^0.5.0") (d #t) (k 0)))) (h "1insnaqnywwa08a8i0pha5y1vfsg63infdzcagm7j0zna2iwx8dy")))

