(define-module (crates-io wa nd wandbox) #:use-module (crates-io))

(define-public crate-wandbox-0.1.0 (c (n "wandbox") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1h18fsh9pz9575ppzgbi3rvvmsgal5ly8xn3d78jbd4m9fli4lbp")))

(define-public crate-wandbox-0.1.1 (c (n "wandbox") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0sxwjjiymw7p6jm35w73cnk9mb9v0y7pa3yfmzq2kb2p3ngjbrx5")))

(define-public crate-wandbox-0.1.2 (c (n "wandbox") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "1qpic51bvkk97jbk56dycnk7xi9g2lx6xg2s8x3c0yr169n4f7ls")))

(define-public crate-wandbox-0.1.3 (c (n "wandbox") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 0)))) (h "15hzl8gmi9afvgdfps8nccgxdha6d8vij6sq1azlpvipxkban6cz")))

