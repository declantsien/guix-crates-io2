(define-module (crates-io wa nd wander-pad) #:use-module (crates-io))

(define-public crate-wander-pad-0.1.0 (c (n "wander-pad") (v "0.1.0") (d (list (d (n "eframe") (r "^0.23.0") (d #t) (k 0)) (d (n "wander") (r "^0.5.0") (d #t) (k 0)))) (h "15r9idaip3yn03h9330yclsyj5lpdk3yh74qgwlg03hpf3jg1y9f")))

