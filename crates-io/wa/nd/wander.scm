(define-module (crates-io wa nd wander) #:use-module (crates-io))

(define-public crate-wander-0.1.0 (c (n "wander") (v "0.1.0") (d (list (d (n "gaze") (r "^0.4") (d #t) (k 0)) (d (n "lig") (r "^0.1") (d #t) (k 0)) (d (n "ligature") (r "^0.5") (d #t) (k 0)))) (h "1g28lqh6xa5k22ywm4kihy4n6dh4n8iimpjfr0qpr0d05fhh4gh9")))

(define-public crate-wander-0.2.0 (c (n "wander") (v "0.2.0") (d (list (d (n "gaze") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "logos") (r "^0.13") (d #t) (k 0)) (d (n "rpds") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0qw812pnrmckppwh32whzs7gd7ph3i1wrlq1v61d8n4cy172ax33")))

(define-public crate-wander-0.3.0 (c (n "wander") (v "0.3.0") (d (list (d (n "gaze") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "logos") (r "^0.13") (d #t) (k 0)) (d (n "rpds") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0334bqf0pdzbbxzb20r0lhr03h12mxr39r01j4346fw74rjs266x")))

(define-public crate-wander-0.4.0 (c (n "wander") (v "0.4.0") (d (list (d (n "gaze") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "logos") (r "^0.13") (d #t) (k 0)) (d (n "rpds") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0grdhiglb2psbpmxildmzia3fpncba4swf94x8mdlr3q4ignq2ch")))

(define-public crate-wander-0.5.0 (c (n "wander") (v "0.5.0") (d (list (d (n "gaze") (r "^0.5.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "logos") (r "^0.13") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rpds") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0dcbnmri18drfns68c02qgd76ik0jk84xxk4nn6sidn0bqzdpxxs")))

