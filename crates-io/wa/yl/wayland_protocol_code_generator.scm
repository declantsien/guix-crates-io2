(define-module (crates-io wa yl wayland_protocol_code_generator) #:use-module (crates-io))

(define-public crate-wayland_protocol_code_generator-0.1.0 (c (n "wayland_protocol_code_generator") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "wayland_protocol_scanner") (r "^0.1.0") (d #t) (k 0)))) (h "1gcyca29mvbzs226g1f8bg9n335v4n8gyp0w62mrbl4c4bx4k8a8")))

(define-public crate-wayland_protocol_code_generator-0.1.1 (c (n "wayland_protocol_code_generator") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "wayland_protocol_scanner") (r "^0.1.0") (d #t) (k 0)))) (h "06q31bf74446ghd179c3al1kl4saf1skh0y5wvaqabinszidb0l1")))

(define-public crate-wayland_protocol_code_generator-0.1.2 (c (n "wayland_protocol_code_generator") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)) (d (n "wayland_protocol_scanner") (r "^0.1.0") (d #t) (k 0)))) (h "19x37hwws1444nj3nz2gpy2sv4mrcabfm5fq5fafw8m82rkwmxkr")))

