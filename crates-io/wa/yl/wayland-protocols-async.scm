(define-module (crates-io wa yl wayland-protocols-async) #:use-module (crates-io))

(define-public crate-wayland-protocols-async-1.0.0 (c (n "wayland-protocols-async") (v "1.0.0") (d (list (d (n "slotmap") (r "^1.0.7") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("time" "net" "process" "sync" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "wayland-client") (r "^0.31.1") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.31.0") (f (quote ("client" "staging"))) (d #t) (k 0)) (d (n "wayland-protocols-misc") (r "^0.2.0") (f (quote ("client"))) (d #t) (k 0)) (d (n "wayland-protocols-wlr") (r "^0.2.0") (f (quote ("client"))) (d #t) (k 0)))) (h "1ic42v7ppvy5mmdbzilvysllyyqq8d49zxi2bpps1qrcgj6hbg4j")))

