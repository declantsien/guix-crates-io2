(define-module (crates-io wa yl wayland-egl) #:use-module (crates-io))

(define-public crate-wayland-egl-0.24.0 (c (n "wayland-egl") (v "0.24.0") (d (list (d (n "wayland-client") (r "^0.24.0") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.24.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "1r1430cgpm76w943xxzpnrrk1y2wvr7cyhvh4w60x0k6k2zknfic")))

(define-public crate-wayland-egl-0.24.1 (c (n "wayland-egl") (v "0.24.1") (d (list (d (n "wayland-client") (r "^0.24.1") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.24.1") (f (quote ("egl"))) (d #t) (k 0)))) (h "0hhk9csdcjrfc7931ns7ppjavpi8sqgvxws1rf40igay0vzslhiw")))

(define-public crate-wayland-egl-0.25.0 (c (n "wayland-egl") (v "0.25.0") (d (list (d (n "wayland-client") (r "^0.25.0") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.25.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "1v4f1xw26d04xk4j9nqvbyfvpg15qibz3ipnmr3fz6i5q6nlyhdl")))

(define-public crate-wayland-egl-0.26.0 (c (n "wayland-egl") (v "0.26.0") (d (list (d (n "wayland-client") (r "^0.26.0") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "13bygg2jkjzs68283h4834g1w9jyg8blakcyvxzdz9845k2mna75")))

(define-public crate-wayland-egl-0.26.1 (c (n "wayland-egl") (v "0.26.1") (d (list (d (n "wayland-client") (r "^0.26.1") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.1") (f (quote ("egl"))) (d #t) (k 0)))) (h "1v5wa1akl1vsni423jr5d5qpxv3qq86w69ichakczwzz0pyljk3a")))

(define-public crate-wayland-egl-0.26.3 (c (n "wayland-egl") (v "0.26.3") (d (list (d (n "wayland-client") (r "^0.26.3") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.3") (f (quote ("egl"))) (d #t) (k 0)))) (h "1nc3p3lc5fp60i6bqq2w6jyyzxr0rzw0rkdbigxpcigxvqqff0y3")))

(define-public crate-wayland-egl-0.26.4 (c (n "wayland-egl") (v "0.26.4") (d (list (d (n "wayland-client") (r "^0.26.4") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.4") (f (quote ("egl"))) (d #t) (k 0)))) (h "0w83lpx2awv4j70r4dmvy6dvcibvahckanabaasx9dmx7mq51fkk")))

(define-public crate-wayland-egl-0.26.5 (c (n "wayland-egl") (v "0.26.5") (d (list (d (n "wayland-client") (r "^0.26.5") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.5") (f (quote ("egl"))) (d #t) (k 0)))) (h "0jwv4sva71qpiq7dck49824c81z8jk86nq88hjh2h2gw3ycz8b93")))

(define-public crate-wayland-egl-0.26.6 (c (n "wayland-egl") (v "0.26.6") (d (list (d (n "wayland-client") (r "^0.26.6") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.6") (f (quote ("egl"))) (d #t) (k 0)))) (h "1mn9y3wgffkbh5hp3nry5cdrx0i9w4i5j11fi9ar3j4vkrf2vxjf")))

(define-public crate-wayland-egl-0.27.0 (c (n "wayland-egl") (v "0.27.0") (d (list (d (n "wayland-client") (r "^0.27.0") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.27.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "1bii4ap18dz1vh4g7z44v428rnvs77kyh5phakcgz3r5dyz4ffqj")))

(define-public crate-wayland-egl-0.28.0 (c (n "wayland-egl") (v "0.28.0") (d (list (d (n "wayland-client") (r "^0.28.0") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "0vsvcjqy6pc2bikig2f75kfr0zpz2yvvj664cis501hhvpg8kkds")))

(define-public crate-wayland-egl-0.28.1 (c (n "wayland-egl") (v "0.28.1") (d (list (d (n "wayland-client") (r "^0.28.1") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.1") (f (quote ("egl"))) (d #t) (k 0)))) (h "0q8dz6bpz1iqva4mw9aksqrdn2b7hvs9nqfcxj5wbkabr2863jp7")))

(define-public crate-wayland-egl-0.28.2 (c (n "wayland-egl") (v "0.28.2") (d (list (d (n "wayland-client") (r "^0.28.2") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.2") (f (quote ("egl"))) (d #t) (k 0)))) (h "1d2hq3vpaxhszg3lnlhv5pbn8hkvys0mqqk09smd36d0a733x3rh")))

(define-public crate-wayland-egl-0.28.3 (c (n "wayland-egl") (v "0.28.3") (d (list (d (n "wayland-client") (r "^0.28.3") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.3") (f (quote ("egl"))) (d #t) (k 0)))) (h "1xd7iap0x4sidmy9dv02cdnxjhnbk9li7r7f39x9cg0i8xs50ly6")))

(define-public crate-wayland-egl-0.28.4 (c (n "wayland-egl") (v "0.28.4") (d (list (d (n "wayland-client") (r "^0.28.4") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.4") (f (quote ("egl"))) (d #t) (k 0)))) (h "1y967y82gvaap4gh18m89jmk565lwxhrn7cvld7z0s077macqsvz")))

(define-public crate-wayland-egl-0.28.5 (c (n "wayland-egl") (v "0.28.5") (d (list (d (n "wayland-client") (r "^0.28.5") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.5") (f (quote ("egl"))) (d #t) (k 0)))) (h "150w8aa2q6xx110chgml81r43x13zaghxdfq9xxdl5pc61wscqcl")))

(define-public crate-wayland-egl-0.28.6 (c (n "wayland-egl") (v "0.28.6") (d (list (d (n "wayland-client") (r "^0.28.6") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.6") (f (quote ("egl"))) (d #t) (k 0)))) (h "0mk9yv9b5w64syi09x0ma3s7s7ajdn2hhvykh8wv4ml7w6qimflr")))

(define-public crate-wayland-egl-0.29.0 (c (n "wayland-egl") (v "0.29.0") (d (list (d (n "wayland-client") (r "^0.29.0") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "13xd3r9li2g7jrs1rxggi4ar6izzgqwi1cgm1xki16chr7r8wmb4")))

(define-public crate-wayland-egl-0.29.1 (c (n "wayland-egl") (v "0.29.1") (d (list (d (n "wayland-client") (r "^0.29.1") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.1") (f (quote ("egl"))) (d #t) (k 0)))) (h "0h1fcazhms2qn9pkg8xy2h8cq5n9q8kdjgv81filpxp1wp8jgkxc")))

(define-public crate-wayland-egl-0.29.2 (c (n "wayland-egl") (v "0.29.2") (d (list (d (n "wayland-client") (r "^0.29.2") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.2") (f (quote ("egl"))) (d #t) (k 0)))) (h "028mx2p3ns8kfzajyg6c7zvrvibkng551r76nq1c71sgfdrars79")))

(define-public crate-wayland-egl-0.30.0-alpha1 (c (n "wayland-egl") (v "0.30.0-alpha1") (d (list (d (n "wayland-backend") (r "^0.1.0-alpha1") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-alpha1") (f (quote ("egl"))) (d #t) (k 0)))) (h "0qr833m629xrw951q4ck4gcgz2kk0afzcvwa0njjh48nkdplaycq")))

(define-public crate-wayland-egl-0.29.3 (c (n "wayland-egl") (v "0.29.3") (d (list (d (n "wayland-client") (r "^0.29.3") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.3") (f (quote ("egl"))) (d #t) (k 0)))) (h "0s88504jjpda6052lf72dsh9vi73r3b9crva97b2px8fwidmf1fq")))

(define-public crate-wayland-egl-0.29.4 (c (n "wayland-egl") (v "0.29.4") (d (list (d (n "wayland-client") (r "^0.29.4") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.4") (f (quote ("egl"))) (d #t) (k 0)))) (h "0flslbp8q4nv3hcw941vapn3jh6y7glqaqv63h1mjaqnxrlisa43")))

(define-public crate-wayland-egl-0.30.0-alpha2 (c (n "wayland-egl") (v "0.30.0-alpha2") (d (list (d (n "wayland-backend") (r "^0.1.0-alpha2") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-alpha2") (f (quote ("egl"))) (d #t) (k 0)))) (h "0w8s9mpp11qn9c6n5kighd5mzkfiyc5d22ajs4cp3mpin5w6ap3h")))

(define-public crate-wayland-egl-0.30.0-alpha3 (c (n "wayland-egl") (v "0.30.0-alpha3") (d (list (d (n "wayland-backend") (r "^0.1.0-alpha3") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-alpha3") (f (quote ("egl"))) (d #t) (k 0)))) (h "0ghv9cgydc7jyv3kvrzzn5bzb4x8qg3ay9f72p6ajk4fb1r5l13g")))

(define-public crate-wayland-egl-0.30.0-alpha4 (c (n "wayland-egl") (v "0.30.0-alpha4") (d (list (d (n "wayland-backend") (r "^0.1.0-alpha4") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-alpha4") (f (quote ("egl"))) (d #t) (k 0)))) (h "1pv83ikzw9acs9i1yh5hq1rvqlrzvaqj0917x2aq8xh26cdpsbd6")))

(define-public crate-wayland-egl-0.30.0-alpha5 (c (n "wayland-egl") (v "0.30.0-alpha5") (d (list (d (n "wayland-backend") (r "^0.1.0-alpha5") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-alpha5") (f (quote ("egl"))) (d #t) (k 0)))) (h "1qd40hzxpjlv9szgj12vkc1dysg9ha5fqhbyn9m5ck6nw907dhqb")))

(define-public crate-wayland-egl-0.30.0-alpha6 (c (n "wayland-egl") (v "0.30.0-alpha6") (d (list (d (n "wayland-backend") (r "^0.1.0-alpha6") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-alpha6") (f (quote ("egl"))) (d #t) (k 0)))) (h "012ldixllblr2z6lvv9b2agbkbhc8kkgd03rx5qm6zvhprbxbada")))

(define-public crate-wayland-egl-0.30.0-alpha7 (c (n "wayland-egl") (v "0.30.0-alpha7") (d (list (d (n "wayland-backend") (r "^0.1.0-alpha7") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-alpha7") (f (quote ("egl"))) (d #t) (k 0)))) (h "163drs6464vcrpxpp3l9lr3xg4bxrfyvh9i82ym5pqfqvj8vfmh4")))

(define-public crate-wayland-egl-0.30.0-alpha8 (c (n "wayland-egl") (v "0.30.0-alpha8") (d (list (d (n "wayland-backend") (r "^0.1.0-alpha8") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-alpha8") (f (quote ("egl"))) (d #t) (k 0)))) (h "0rrx0nbwrxwbwp1ycz8rk9iyq75vzc0s2854vqmj83jn5qy1xcj3")))

(define-public crate-wayland-egl-0.30.0-alpha9 (c (n "wayland-egl") (v "0.30.0-alpha9") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-alpha9") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-alpha9") (f (quote ("egl"))) (d #t) (k 0)))) (h "19rgfqr4pfln19c8rasjq0gyzryqz1l1xl94vg37y4007fhn5s79")))

(define-public crate-wayland-egl-0.30.0-alpha10 (c (n "wayland-egl") (v "0.30.0-alpha10") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-alpha10") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-alpha10") (f (quote ("egl"))) (d #t) (k 0)))) (h "0xpc0mcz4pj44pqmn54hbcryn08svfbg6mjad78k56gki16mha91")))

(define-public crate-wayland-egl-0.30.0-beta.1 (c (n "wayland-egl") (v "0.30.0-beta.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-beta.1") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-beta.1") (f (quote ("egl"))) (d #t) (k 0)))) (h "0dwm9zvxvvk1rpr1ghhc3kb9nawjr4avmijg29l9j392c47kgy4s")))

(define-public crate-wayland-egl-0.30.0-beta.2 (c (n "wayland-egl") (v "0.30.0-beta.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-beta.2") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-beta.2") (f (quote ("egl"))) (d #t) (k 0)))) (h "11zgxkq67q34qn6sjdxjgmnxh32hayq9nby7sxalbypic2wmb1lh")))

(define-public crate-wayland-egl-0.30.0-beta.3 (c (n "wayland-egl") (v "0.30.0-beta.3") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-beta.3") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-beta.3") (f (quote ("egl"))) (d #t) (k 0)))) (h "161z213li7322ns3ajpgvjiiw1hsqkm9r0c8khkwmzsf18vd2fiz")))

(define-public crate-wayland-egl-0.30.0-beta.4 (c (n "wayland-egl") (v "0.30.0-beta.4") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-beta.4") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-beta.4") (f (quote ("egl"))) (d #t) (k 0)))) (h "1yxy9lfm72fv7a95hh84pjxniaypr8l79q3v599g1r3718pnvjyq")))

(define-public crate-wayland-egl-0.30.0-beta.5 (c (n "wayland-egl") (v "0.30.0-beta.5") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-beta.5") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-beta.5") (f (quote ("egl"))) (d #t) (k 0)))) (h "0hf2vzkmc6fwm8afh42qdp66rl0f21byljbgz605wp32bg8mwv2d")))

(define-public crate-wayland-egl-0.30.0-beta.6 (c (n "wayland-egl") (v "0.30.0-beta.6") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-beta.6") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-beta.6") (f (quote ("egl"))) (d #t) (k 0)))) (h "0srk8cn7bdi2i3n0a8mzxjj8688rdjr97jkgkx4xwvdmwvmv14gz")))

(define-public crate-wayland-egl-0.30.0-beta.7 (c (n "wayland-egl") (v "0.30.0-beta.7") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-beta.7") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-beta.7") (f (quote ("egl"))) (d #t) (k 0)))) (h "1psmx19jxyaf9szmww10jli98bx7fmizxrx0m671qiwlfha14x9c")))

(define-public crate-wayland-egl-0.30.0-beta.8 (c (n "wayland-egl") (v "0.30.0-beta.8") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-beta.8") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-beta.8") (f (quote ("egl"))) (d #t) (k 0)))) (h "16p9p3xfj6v550pxh0d7a2x2z9xxq14gsbvdjxn08vzambzpdi60")))

(define-public crate-wayland-egl-0.29.5 (c (n "wayland-egl") (v "0.29.5") (d (list (d (n "wayland-client") (r "^0.29.5") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.5") (f (quote ("egl"))) (d #t) (k 0)))) (h "0z8hwixv5kj201p2pnbdwxbl4s9hz5cxd8i1v0k2j08sz14yjba0")))

(define-public crate-wayland-egl-0.30.0-beta.9 (c (n "wayland-egl") (v "0.30.0-beta.9") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.0-beta.9") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0-beta.9") (f (quote ("egl"))) (d #t) (k 0)))) (h "195l8z0b9qx6yzdwnkc76dql2nlpdj90kp18d2zil0nk244lhg9r")))

(define-public crate-wayland-egl-0.30.0-beta.10 (c (n "wayland-egl") (v "0.30.0-beta.10") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "=0.1.0-beta.10") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "=0.30.0-beta.10") (f (quote ("egl"))) (d #t) (k 0)))) (h "17408q3a970ikfx7rz3fpnmhjjhngz9g87b9h2qy2hjisy8mbjxn")))

(define-public crate-wayland-egl-0.30.0-beta.11 (c (n "wayland-egl") (v "0.30.0-beta.11") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "=0.1.0-beta.11") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "=0.30.0-beta.11") (f (quote ("egl"))) (d #t) (k 0)))) (h "1rdh5jg3yskj0fqz1c2fhnp674l3wgqmq2fx8da6z2d2l8axl726") (y #t)))

(define-public crate-wayland-egl-0.30.0-beta.12 (c (n "wayland-egl") (v "0.30.0-beta.12") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "=0.1.0-beta.12") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "=0.30.0-beta.12") (f (quote ("egl"))) (d #t) (k 0)))) (h "0pwwzvh0nfjh5vfvqzpas221ps1xxnr9s7n214n99ggavyfk1hfg")))

(define-public crate-wayland-egl-0.30.0-beta.13 (c (n "wayland-egl") (v "0.30.0-beta.13") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "=0.1.0-beta.13") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "=0.30.0-beta.13") (f (quote ("egl"))) (d #t) (k 0)))) (h "03p2k0l861j9m53cyb0ypfbpv7c844nbc1b3g289pn2winiv8afs")))

(define-public crate-wayland-egl-0.30.0-beta.14 (c (n "wayland-egl") (v "0.30.0-beta.14") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "=0.1.0-beta.14") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "0p73h8z89p3syb17wqw8m6cis92v092rwkncpcn8zs1v2y3mppb8")))

(define-public crate-wayland-egl-0.30.0-beta.15 (c (n "wayland-egl") (v "0.30.0-beta.15") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wayland-backend") (r "=0.1.0-beta.15") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "1nq0xwmz5gc56hzw69s9s32wdwf8z1763k8rl76w20z7f6nmrvkv") (r "1.59")))

(define-public crate-wayland-egl-0.30.0 (c (n "wayland-egl") (v "0.30.0") (d (list (d (n "wayland-backend") (r "^0.1.0") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.30.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "1v983sbg98scj64fjvxwgmfqqqwz2i9jk79z2v1m6c8wx1gnk1qi") (r "1.59")))

(define-public crate-wayland-egl-0.30.1 (c (n "wayland-egl") (v "0.30.1") (d (list (d (n "wayland-backend") (r "^0.1.3") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.31.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "1cd2r561md7kvx9nn27nc7z559r558wr3g6xv5zp19838b4jnmx3") (y #t) (r "1.59")))

(define-public crate-wayland-egl-0.31.0 (c (n "wayland-egl") (v "0.31.0") (d (list (d (n "wayland-backend") (r "^0.2.0") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.31.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "0w56674hdy460fm79d7vgyk77k7yzn84d3xaafc3zhfns1nk5kd6") (r "1.59")))

(define-public crate-wayland-egl-0.32.0 (c (n "wayland-egl") (v "0.32.0") (d (list (d (n "wayland-backend") (r "^0.3.0") (f (quote ("client_system"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.31.0") (f (quote ("egl"))) (d #t) (k 0)))) (h "18pdbvbyg4x10dhws9shcfyw5inwsg18yv2kmp905bi4b8p6aprm") (r "1.65")))

