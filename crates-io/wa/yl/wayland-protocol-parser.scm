(define-module (crates-io wa yl wayland-protocol-parser) #:use-module (crates-io))

(define-public crate-wayland-protocol-parser-0.1.0 (c (n "wayland-protocol-parser") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("std" "derive"))) (o #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1kr7s7smjfq0cn4b591mjs72j5azfg1zvnlhyj05ra0iqhhpbf41")))

