(define-module (crates-io wa yl wayland-scanner) #:use-module (crates-io))

(define-public crate-wayland-scanner-0.1.0 (c (n "wayland-scanner") (v "0.1.0") (d (list (d (n "xml-rs") (r "^0.1.26") (d #t) (k 0)))) (h "1244bs8sjgfy49grn9nxlryqhjvcjja5s1b86d07shwk3fv1sfbb")))

(define-public crate-wayland-scanner-0.1.1 (c (n "wayland-scanner") (v "0.1.1") (d (list (d (n "xml-rs") (r "^0.1.26") (d #t) (k 0)))) (h "1gww761znw6qg64gj9s7bzqmqippsa2dmckv7z6w9s6qyxf6wg3z")))

(define-public crate-wayland-scanner-0.2.0 (c (n "wayland-scanner") (v "0.2.0") (d (list (d (n "xml-rs") (r "^0.1.26") (d #t) (k 0)))) (h "10nb8z37zc0mqkhgg4ybl2vxl122kymmmlzhhzkkg2sr7qgkc5wi")))

(define-public crate-wayland-scanner-0.5.0 (c (n "wayland-scanner") (v "0.5.0") (d (list (d (n "xml-rs") (r "^0.1.26") (d #t) (k 0)))) (h "0cm0a1zrjmy00c7y426qkjqfaq8gwj2kk571zrbxm8b200vcwjv0")))

(define-public crate-wayland-scanner-0.5.5 (c (n "wayland-scanner") (v "0.5.5") (d (list (d (n "xml-rs") (r "^0.2") (d #t) (k 0)))) (h "02wq4cqn8i6i3ic254j4mg9wrpplk8zrhzvbjdghnaz7256ymbrk")))

(define-public crate-wayland-scanner-0.5.6 (c (n "wayland-scanner") (v "0.5.6") (d (list (d (n "xml-rs") (r "^0.2") (d #t) (k 0)))) (h "0v2729zblj2c9aji1y4ksgpkk7spxkpmi6ya91nh2rad0ak5njxb")))

(define-public crate-wayland-scanner-0.5.7 (c (n "wayland-scanner") (v "0.5.7") (d (list (d (n "xml-rs") (r "^0.2") (d #t) (k 0)))) (h "1hph9pi8l9bff8zkqq7skb1d5apkqgrr30gaixp5rhz2vybc9h8a") (y #t)))

(define-public crate-wayland-scanner-0.5.8 (c (n "wayland-scanner") (v "0.5.8") (d (list (d (n "xml-rs") (r "^0.2") (d #t) (k 0)))) (h "1vzvw69ggzy72lcs0f82fmfzz8z4djackbn6jvh1wpvpnf48gdmi")))

(define-public crate-wayland-scanner-0.5.11 (c (n "wayland-scanner") (v "0.5.11") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0rjg6qs9a9j9895djjp9f2d20kizh0fm2947msmwpbvb1lvnj62s")))

(define-public crate-wayland-scanner-0.6.0 (c (n "wayland-scanner") (v "0.6.0") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1yjh0rp2mcrvqs5mjq6zn8a5014i7ck38x815l54c06515l121rk")))

(define-public crate-wayland-scanner-0.7.0 (c (n "wayland-scanner") (v "0.7.0") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0jdz97wcwf3wxrxrvbqjd464zd1rhlsq0j4paisg8bl08dyirzfs")))

(define-public crate-wayland-scanner-0.7.1 (c (n "wayland-scanner") (v "0.7.1") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "19kdkglnw6fmpdk6c7r3x5iixg906fkzildpprw0jlrin7dpyc1b")))

(define-public crate-wayland-scanner-0.7.2 (c (n "wayland-scanner") (v "0.7.2") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0mnqdjgi2lfjccp5hqm6n5qwn6m4lk13amffv86b4al0iqsc848b")))

(define-public crate-wayland-scanner-0.7.3 (c (n "wayland-scanner") (v "0.7.3") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1z97ivnpzfqkngbms2mcvildyg94163vvpwghxizc5i3vqshdz5d")))

(define-public crate-wayland-scanner-0.7.4 (c (n "wayland-scanner") (v "0.7.4") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "02r4c128g7jb72saafgxvcb3lwvkrxqqj463b05l2xip5cw6na8n")))

(define-public crate-wayland-scanner-0.7.5 (c (n "wayland-scanner") (v "0.7.5") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1iy0lf4ikia4z4k3fzhgfncbrpxj86bw3hs0fi98vaq7hx85vzz7")))

(define-public crate-wayland-scanner-0.7.6 (c (n "wayland-scanner") (v "0.7.6") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0yrrh6gzjbkyvg4cyx9djg3ir1vhyizidrkwcls8zavv9gx9wif7")))

(define-public crate-wayland-scanner-0.7.7 (c (n "wayland-scanner") (v "0.7.7") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "18y3d0gk26630rx3nni7q2hp1i1x24rkhrgz5pldnmjbf1arl2ar")))

(define-public crate-wayland-scanner-0.7.8 (c (n "wayland-scanner") (v "0.7.8") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1zzcpqgq04xwrshly4da2a19z3l3r82sk5h561qfqfbmdf33iz91")))

(define-public crate-wayland-scanner-0.8.0 (c (n "wayland-scanner") (v "0.8.0") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "15fx0912affz6dawdcqgazhcd1wxsrfcsjqwvplw07kdqkls0i26")))

(define-public crate-wayland-scanner-0.8.4 (c (n "wayland-scanner") (v "0.8.4") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1xvgfbzzscyrdh6x4m2pa9qmc4h5y4csys2yn564fn0fc3628c1w")))

(define-public crate-wayland-scanner-0.8.5 (c (n "wayland-scanner") (v "0.8.5") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1i4wnw1bmkkwz7293b5cjhk48rms8l92mgdf2v5qm0rbpgzv03y9")))

(define-public crate-wayland-scanner-0.8.6 (c (n "wayland-scanner") (v "0.8.6") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "085ncjhv61al01j9rpikrng1lc0g1a24h27f3zmv6qhw7wslr93k")))

(define-public crate-wayland-scanner-0.8.7 (c (n "wayland-scanner") (v "0.8.7") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1gy05niclp9hhpkzk4b86zys98drrhkf1naimkv7ryb3xw02n09a")))

(define-public crate-wayland-scanner-0.9.0 (c (n "wayland-scanner") (v "0.9.0") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1j7q1krmkhmwiydxq710dz3nvgjqy7b8420dbf2l5wqljqi072cd")))

(define-public crate-wayland-scanner-0.9.1 (c (n "wayland-scanner") (v "0.9.1") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "16k66zpx03vm93ykpvqdw7xm5q4f1wjdzy35la61bzynkvyhj3yh")))

(define-public crate-wayland-scanner-0.9.2 (c (n "wayland-scanner") (v "0.9.2") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "149dbn1i22vifxr126lpmwm453ydvw4csmrldjdyvbz9r02f297b")))

(define-public crate-wayland-scanner-0.9.3 (c (n "wayland-scanner") (v "0.9.3") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1kyf1cjln1x9m94q24h4v3kw11ahp3iz34y40z7ar2x5b380bh31")))

(define-public crate-wayland-scanner-0.9.4 (c (n "wayland-scanner") (v "0.9.4") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "13hp3r5hw660jh5dx9dz8a71xp6cbb8y3bxi9wsqbn2q4l4vgkdz")))

(define-public crate-wayland-scanner-0.9.5 (c (n "wayland-scanner") (v "0.9.5") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "11wxq7zd0nq9372xy6fhkd0n2v4vi330jhq1ay9kamd3xfn2ykcn")))

(define-public crate-wayland-scanner-0.9.6 (c (n "wayland-scanner") (v "0.9.6") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "1hzny8v0fzx2kfsc7s18nx08kb987xissingyxz6a95avkjp5849")))

(define-public crate-wayland-scanner-0.9.7 (c (n "wayland-scanner") (v "0.9.7") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0qkf4mlh4i87dniqfpl42dgpxpi2xg97h85d93h89ai5hw4lzs4b")))

(define-public crate-wayland-scanner-0.9.8 (c (n "wayland-scanner") (v "0.9.8") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "19ms90bdlrghc39imap35yvg489h00z4vnki695lq4q9f8adgvnj")))

(define-public crate-wayland-scanner-0.9.9 (c (n "wayland-scanner") (v "0.9.9") (d (list (d (n "xml-rs") (r "^0.3") (d #t) (k 0)))) (h "0k06rh4gs6qkq0fy24056y88izbw8252szcfjzg7nv1mvgy95y8d")))

(define-public crate-wayland-scanner-0.9.10 (c (n "wayland-scanner") (v "0.9.10") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "0s979b6rz1ihk9k23d526i4l7raclawjycckg2mf8vmp68hjc838")))

(define-public crate-wayland-scanner-0.10.0 (c (n "wayland-scanner") (v "0.10.0") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "0s079xmiwj79d0sd1l9ysj0pawkfb33x7fdi5nign4hb7868h6hl")))

(define-public crate-wayland-scanner-0.10.1 (c (n "wayland-scanner") (v "0.10.1") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "0z9301ywjacslkw6b3k7jxfvagsnwd12gsvygbhmcls7kl33m847")))

(define-public crate-wayland-scanner-0.10.2 (c (n "wayland-scanner") (v "0.10.2") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "09lcjxljv06j5m2c1lbfra15i2sih1m0z9paq474qr0hhiy5ajcr")))

(define-public crate-wayland-scanner-0.10.3 (c (n "wayland-scanner") (v "0.10.3") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "1g58ng7l74f58ly4w57fcf0r937b1nf95dhkw91i541dcbvd5yaf")))

(define-public crate-wayland-scanner-0.11.0 (c (n "wayland-scanner") (v "0.11.0") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "1p41vjvd0rh7fzw1f8m2mqz12vq6w1khss80v2lpsd9dxmm99cgy")))

(define-public crate-wayland-scanner-0.11.1 (c (n "wayland-scanner") (v "0.11.1") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "0d87k0slmdlj44vamf6v0pamya6qnppn6al0m4f55bjmxi23pvi1")))

(define-public crate-wayland-scanner-0.11.2 (c (n "wayland-scanner") (v "0.11.2") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "0y9ljckfyprj7f5xm2yhg7zwighm985x6wmpwgajsfl5rcblya3r")))

(define-public crate-wayland-scanner-0.11.3 (c (n "wayland-scanner") (v "0.11.3") (d (list (d (n "xml-rs") (r "^0.6") (d #t) (k 0)))) (h "1apnhknyh3wm0vvrdb10j56vk24q3ivm7sqr57w01ri1ayphhgxq")))

(define-public crate-wayland-scanner-0.11.4 (c (n "wayland-scanner") (v "0.11.4") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "0in7im5biic5dgwqnv19r8iqdh84x9pny81qhi4692h88kbih1bq")))

(define-public crate-wayland-scanner-0.12.0 (c (n "wayland-scanner") (v "0.12.0") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "11xq666cmn47m8z0mj17lrr93v2yg56g29j7wam6q8i46b06vljn")))

(define-public crate-wayland-scanner-0.12.1 (c (n "wayland-scanner") (v "0.12.1") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "12wj0xifs4w292s50a79691jw2rnp8g8flm9njfrvyhg267d05d4")))

(define-public crate-wayland-scanner-0.12.2 (c (n "wayland-scanner") (v "0.12.2") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "016iyckwan5scpcyw14ckfcg3hp50cq741llimqimbrrd418fdw2")))

(define-public crate-wayland-scanner-0.12.3 (c (n "wayland-scanner") (v "0.12.3") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1mgb172s66bdpfgfsi833aw58p7w4087l4xkwj0kf7iql5ic7jiw")))

(define-public crate-wayland-scanner-0.12.4 (c (n "wayland-scanner") (v "0.12.4") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1ril4xkybirw401zyn59vblr837lx590sy49rs2l2ca5pj2d5c34")))

(define-public crate-wayland-scanner-0.12.5 (c (n "wayland-scanner") (v "0.12.5") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1b885raifbi9wzjjxfb7lk8f29f31y24vrkd8cyjqvqyc9dabzyw")))

(define-public crate-wayland-scanner-0.13.0 (c (n "wayland-scanner") (v "0.13.0") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "16y2vxpprp6m3f3ggqgcjqyks6wp7whiifzd5d8fb58agp1cag6m") (y #t)))

(define-public crate-wayland-scanner-0.14.0 (c (n "wayland-scanner") (v "0.14.0") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1zkq9hrsdb81f6ycg32l5qnaxvkhr37wsdyn8fb3d8x4w40fa4zk")))

(define-public crate-wayland-scanner-0.14.1 (c (n "wayland-scanner") (v "0.14.1") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1wrlmwjvksn1mzybjngqzv08pa3d94zflcq3zv0h2jybca1ifn16")))

(define-public crate-wayland-scanner-0.20.0 (c (n "wayland-scanner") (v "0.20.0") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1p7x4kgc04my2mgr07cnyikz5fdi7bi4qmbbvj96f4c8rxfqbair")))

(define-public crate-wayland-scanner-0.20.1 (c (n "wayland-scanner") (v "0.20.1") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "0az23jy1lixm1l4kvig3qpwqfgf2ajijmapffhzw268il9n2rhdr")))

(define-public crate-wayland-scanner-0.20.2 (c (n "wayland-scanner") (v "0.20.2") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1p83x38641218rnbzyz2bbp2m0j209jv6czgbyna59bfrqg9xih1")))

(define-public crate-wayland-scanner-0.20.3 (c (n "wayland-scanner") (v "0.20.3") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1gq38qd04f7kma07gv1cia4iwba74920l7rzwnf0bvvv0172ycay")))

(define-public crate-wayland-scanner-0.20.4 (c (n "wayland-scanner") (v "0.20.4") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1inz47lvk484nlgccg73zqzn1z4k4bxgjkc9i9rd85ybwzri526x")))

(define-public crate-wayland-scanner-0.20.5 (c (n "wayland-scanner") (v "0.20.5") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "0jqcivb4xin2yfjkhlnb73qjcc0x4ccnc2rn2b368yfdjkzfv8q7")))

(define-public crate-wayland-scanner-0.20.6 (c (n "wayland-scanner") (v "0.20.6") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1yid789wl1312hdmw161g4mbp24m7xjrbk9b2hlxvlk771l8pphf")))

(define-public crate-wayland-scanner-0.20.7 (c (n "wayland-scanner") (v "0.20.7") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1sxnqrrq5hd13mi3analarl52a7vv767hzr2dxsaxpi12p4gs6hp")))

(define-public crate-wayland-scanner-0.20.8 (c (n "wayland-scanner") (v "0.20.8") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1wgcpniqjdyqganp87m1y1qdmpfiabn7wrg3a672hlzfrqcr1dqg")))

(define-public crate-wayland-scanner-0.20.9 (c (n "wayland-scanner") (v "0.20.9") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "17n5vf9ay3c4dxvnxd0krrhfc0kbh8flm5mamf4h57rfwahgciv9")))

(define-public crate-wayland-scanner-0.20.10 (c (n "wayland-scanner") (v "0.20.10") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "0z713nv2bp9jhjh30n5q53b5nhkd9vv0vgxjm70kzpxfiks4xkwk")))

(define-public crate-wayland-scanner-0.20.11 (c (n "wayland-scanner") (v "0.20.11") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "106dpgh8hsn3y57sp1bzc4inf9zliym16ldmi26qqpfqzxxiia1w")))

(define-public crate-wayland-scanner-0.21.0-alpha1 (c (n "wayland-scanner") (v "0.21.0-alpha1") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1gzj6i17kz91253lz51qgmwa6nf19bs7yvm0is4g1jhji345ixdd")))

(define-public crate-wayland-scanner-0.20.12 (c (n "wayland-scanner") (v "0.20.12") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1b85ip7gwfi1aby32fnfhzzjz85740p8yk9pj32vnz66x5ddhx76")))

(define-public crate-wayland-scanner-0.21.0 (c (n "wayland-scanner") (v "0.21.0") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "1p6favi98s30x54j677hxicw8jp9bkhfbsr9y6g9hylhdmg2wfpr")))

(define-public crate-wayland-scanner-0.21.1 (c (n "wayland-scanner") (v "0.21.1") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "19vicqgnfrjzlqbad5pdjy4yg11vxl7qcxavl91kjpp87q34y891")))

(define-public crate-wayland-scanner-0.21.2 (c (n "wayland-scanner") (v "0.21.2") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "01142qf1f6rzyprcb0xali203afjfbylv3rmnzwn0gl4ms6wv5xm")))

(define-public crate-wayland-scanner-0.21.3 (c (n "wayland-scanner") (v "0.21.3") (d (list (d (n "xml-rs") (r "^0.7") (d #t) (k 0)))) (h "0dj6i2zhv5b4wz3lbrzbcgkdyn89b13x2nl7yasg4x65796qhybl")))

(define-public crate-wayland-scanner-0.21.4 (c (n "wayland-scanner") (v "0.21.4") (d (list (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0c2cg71yka7qp676hpzlsrn42f728z5aiv691l0rq52f5vk7x4pi")))

(define-public crate-wayland-scanner-0.21.5 (c (n "wayland-scanner") (v "0.21.5") (d (list (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0682fncj8vs9ig420qpngnnjn92lrlnja7jnbyan03p9xnxh7pan")))

(define-public crate-wayland-scanner-0.21.6 (c (n "wayland-scanner") (v "0.21.6") (d (list (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "1c5812c83528pdys2rg7vd0k7igiszdsq39mnaqgn5hn4i6bpmzb")))

(define-public crate-wayland-scanner-0.21.7 (c (n "wayland-scanner") (v "0.21.7") (d (list (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "19zkrggyn0rmw39h1clwl0xlp8xkqg4ac60wy4bpk7x181m885sg")))

(define-public crate-wayland-scanner-0.21.8 (c (n "wayland-scanner") (v "0.21.8") (d (list (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "1krym9hazhxql8bmgs3f0k3pq5f8zj1wggm7sfwk2ml7fshrqfa1")))

(define-public crate-wayland-scanner-0.21.9 (c (n "wayland-scanner") (v "0.21.9") (d (list (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "01xvr9g48syfxw7ag1v1yxm452gnxlrwislha3qfhkj5hgs8phs8")))

(define-public crate-wayland-scanner-0.21.10 (c (n "wayland-scanner") (v "0.21.10") (d (list (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "169f68apvqxjjqnkym06zhsh9bvxwg7ffxh7g5bbhms0h9x8d2qw")))

(define-public crate-wayland-scanner-0.21.11 (c (n "wayland-scanner") (v "0.21.11") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0m8kash91r8n7vxwm6051gqwdva9chz108vnxcpmrqbmwqqz449n")))

(define-public crate-wayland-scanner-0.22.0 (c (n "wayland-scanner") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0apjrjz2x070az63i5c08kw1ghmbkm2v7p6rky1007b6b32ddd4a")))

(define-public crate-wayland-scanner-0.22.1 (c (n "wayland-scanner") (v "0.22.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "1h6bm15fd1jp1mi7mrsvprsj81d9x5629mb74bqpwwflbx0fwn8y")))

(define-public crate-wayland-scanner-0.22.2 (c (n "wayland-scanner") (v "0.22.2") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0viwwk5cqr8g1s0hwvppmghhh970ddqccif04wmkwqrh883p98xd")))

(define-public crate-wayland-scanner-0.23.0 (c (n "wayland-scanner") (v "0.23.0") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "148mmj95qapfi84g2ayjmlw6k6p758rbvis26w9dyq28pfkjsp12")))

(define-public crate-wayland-scanner-0.23.1 (c (n "wayland-scanner") (v "0.23.1") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "13w6qfp6877fiamqwzp7kpbx0bfr6nw93mapn1y20z6m58n7kvgn")))

(define-public crate-wayland-scanner-0.21.12 (c (n "wayland-scanner") (v "0.21.12") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "1qk03fgjknvb0n7dccyifrqc8zsps30725ax5r6z1f6dgpx5xg33")))

(define-public crate-wayland-scanner-0.23.2 (c (n "wayland-scanner") (v "0.23.2") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "19b5y2lldwssah5439ffrj18zcknpg933mv39s6lirxvgmhvi4wp")))

(define-public crate-wayland-scanner-0.23.3 (c (n "wayland-scanner") (v "0.23.3") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0jw6wdyzsh98klar0yy3k2rqqrmyakksb0anifphlhsdi7gi0j68")))

(define-public crate-wayland-scanner-0.23.4 (c (n "wayland-scanner") (v "0.23.4") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0yyby5j2bpbg7myd8mhd3xl1g62jl2sqcl81y3kbn1cjianfzngi")))

(define-public crate-wayland-scanner-0.23.5 (c (n "wayland-scanner") (v "0.23.5") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0qnfiycki3hypi2r7lfjniqy6fch5r9y42aa7fmp07hf0ncc8aw4")))

(define-public crate-wayland-scanner-0.21.13 (c (n "wayland-scanner") (v "0.21.13") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "17mp49v7w0p0x5ry628lj2llljnwkr9aj9g4bqqhfibid32jhf5z")))

(define-public crate-wayland-scanner-0.23.6 (c (n "wayland-scanner") (v "0.23.6") (d (list (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0g8wcphykjrcpslznyi3qccx1pckw97rckq5b295nfbg6r3j5c4k")))

(define-public crate-wayland-scanner-0.24.0 (c (n "wayland-scanner") (v "0.24.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "01s9hzrb6mxby4ap8pa6di42cwy13ibhf56ja5rr3bqdsj94pjg5")))

(define-public crate-wayland-scanner-0.24.1 (c (n "wayland-scanner") (v "0.24.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "09nl8h5y361wb2q9daccbqxphscd34fl75y1q1r7s4qpl6hmxsj3")))

(define-public crate-wayland-scanner-0.25.0 (c (n "wayland-scanner") (v "0.25.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0dn7zd35h7dw3w4dx498f4q3pif9ihk17x5mjvx3x3q7m04dqpgl")))

(define-public crate-wayland-scanner-0.26.0 (c (n "wayland-scanner") (v "0.26.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "1k9k8i7qw6mw8yzv262xf46z24jjfwmhr46frsw804s1iw0fhlky")))

(define-public crate-wayland-scanner-0.26.1 (c (n "wayland-scanner") (v "0.26.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "1kw7qawsir09qq5m4by0vfwb4p3sygjib17aw4p22l5clw4k28vc")))

(define-public crate-wayland-scanner-0.26.3 (c (n "wayland-scanner") (v "0.26.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "0wd978kdn3q7izfl5ylcis6232g49q7hs0c8rr0jyfslc6bkgbi1")))

(define-public crate-wayland-scanner-0.26.4 (c (n "wayland-scanner") (v "0.26.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "1qzi0fxhn1iva7z40ragc6k7j6pniwabdzi1bzpaxb38ljfv1577")))

(define-public crate-wayland-scanner-0.26.5 (c (n "wayland-scanner") (v "0.26.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "005rfvdfx9c12d0ll88a03915dnlq164alcghimdynbd5z3ygdg7")))

(define-public crate-wayland-scanner-0.26.6 (c (n "wayland-scanner") (v "0.26.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">= 0.7, < 0.9") (d #t) (k 0)))) (h "1h8wsd73xv8p2d8s4wm5nr9qshg61v9hbjzkpznabpdk6gjlsjqz")))

(define-public crate-wayland-scanner-0.27.0 (c (n "wayland-scanner") (v "0.27.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "0a03qcx98v29fp6xk0n41wdvw2c1x97pcwml1d0djawkkl05c3q3")))

(define-public crate-wayland-scanner-0.28.0 (c (n "wayland-scanner") (v "0.28.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "0g7qja7pzhkwk0mg0pqli2ds5cl0198sldv04cv8g2q57jzlfmaj")))

(define-public crate-wayland-scanner-0.28.1 (c (n "wayland-scanner") (v "0.28.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "1qp5d0yjrxqkm4n3cn3ajqbj0kc24qxgxkpp299l6nmhyhd0kk71")))

(define-public crate-wayland-scanner-0.28.2 (c (n "wayland-scanner") (v "0.28.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "1k2ngq7m1x8hkyajp03licr5y8273rbfqpsihv2fyinpl51vvray")))

(define-public crate-wayland-scanner-0.28.3 (c (n "wayland-scanner") (v "0.28.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "0g8ky63qk27in7zajycj3fyydsxlj19hanfcvr8d7z5kcxbvl43h")))

(define-public crate-wayland-scanner-0.28.4 (c (n "wayland-scanner") (v "0.28.4") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "1sspnywkczbzw6d5b1z9hqpqpig2cmd75hh32xygap5qw68zj5hd")))

(define-public crate-wayland-scanner-0.28.5 (c (n "wayland-scanner") (v "0.28.5") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "0847nr0h943hz0rqs3hgxf6h69s349h9bwrpkkf14xfngc6ni79q")))

(define-public crate-wayland-scanner-0.28.6 (c (n "wayland-scanner") (v "0.28.6") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "1w839jsh7nrni4f2x5bkapf98w7kddxyqmpks4rf67dnvsr3x4nf")))

(define-public crate-wayland-scanner-0.29.0 (c (n "wayland-scanner") (v "0.29.0") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "1pg2vv827igqsg1vj56ldg4cla137qwzp2q195kq7ci6i4x08gbx")))

(define-public crate-wayland-scanner-0.29.1 (c (n "wayland-scanner") (v "0.29.1") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "1x66zn034r104dqxnxj33hwvwnjajlyx4szrk8al74bvbv42pbza")))

(define-public crate-wayland-scanner-0.29.2 (c (n "wayland-scanner") (v "0.29.2") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "1b458vxv4d3k6wqbn8s3y6mjm07krlkzvs1hai4155gglw2ij1dr")))

(define-public crate-wayland-scanner-0.30.0-alpha1 (c (n "wayland-scanner") (v "0.30.0-alpha1") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "19zgcw0xjkl9lzsjmb93gvxvngaf2m8vz8ackkzp4y7gdnf1lfwf")))

(define-public crate-wayland-scanner-0.29.3 (c (n "wayland-scanner") (v "0.29.3") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "1wdbzbjnp1f4yw5dyn6axrv7crqmrhd4dxh76v8wyasd07gmxz9c")))

(define-public crate-wayland-scanner-0.29.4 (c (n "wayland-scanner") (v "0.29.4") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "1q7r764z8k922xf51fj56b1xm29ffi9ap8jnf4c478gp8cqyv89r")))

(define-public crate-wayland-scanner-0.30.0-alpha2 (c (n "wayland-scanner") (v "0.30.0-alpha2") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0cgmjlbg4lcgpy44ridsdq68p3zxhrzdbzjix8lbfry4hr1spn69")))

(define-public crate-wayland-scanner-0.30.0-alpha3 (c (n "wayland-scanner") (v "0.30.0-alpha3") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "15fx663kixs6270bxc3r4264qar0n7fjxaxjapb3kzfw4388lwia")))

(define-public crate-wayland-scanner-0.30.0-alpha4 (c (n "wayland-scanner") (v "0.30.0-alpha4") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0lyhn94jgr8v87y92aai5jww2xg7w1rzqdxi8k3z9sjwbqfbc9jl")))

(define-public crate-wayland-scanner-0.30.0-alpha5 (c (n "wayland-scanner") (v "0.30.0-alpha5") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0jcf7h3k81sfj2vdk15lkiqqrsp16fyzw0c8c838kl6yvh35zh9v")))

(define-public crate-wayland-scanner-0.30.0-alpha6 (c (n "wayland-scanner") (v "0.30.0-alpha6") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "034xsvzwyd1bagklw50s5vfg7375f3yq9n4xwxglsj6ljjqphvnq")))

(define-public crate-wayland-scanner-0.30.0-alpha7 (c (n "wayland-scanner") (v "0.30.0-alpha7") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0hh60bzva11k0pgqg3aq45jwak5srqyncdxl2al5qlg2zbzf9xn6")))

(define-public crate-wayland-scanner-0.30.0-alpha8 (c (n "wayland-scanner") (v "0.30.0-alpha8") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "02gjczh6dw75lvrdc017l2bvn46f3r8r52mblhv7ad5vhg9syzxg")))

(define-public crate-wayland-scanner-0.30.0-alpha9 (c (n "wayland-scanner") (v "0.30.0-alpha9") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0hbdfql0afnjqc6kn6kfp3mqzwhqakya2mlqbh5chpagja5mq8qf")))

(define-public crate-wayland-scanner-0.30.0-alpha10 (c (n "wayland-scanner") (v "0.30.0-alpha10") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "08ppwrj428gx3p8ybl85fc9mk98fcc9npwfg5cy8gd0vbq7xl4fl")))

(define-public crate-wayland-scanner-0.30.0-beta.1 (c (n "wayland-scanner") (v "0.30.0-beta.1") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0vsc29wak18m1s7v8yiw78rkw3k71pj60pzrqkdqx4bz64j42m66")))

(define-public crate-wayland-scanner-0.30.0-beta.2 (c (n "wayland-scanner") (v "0.30.0-beta.2") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0cqwm4c324n60kbj80g3bhs6p03yal8i9pl3m130zxrcddgqsxcx")))

(define-public crate-wayland-scanner-0.30.0-beta.3 (c (n "wayland-scanner") (v "0.30.0-beta.3") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0i3fazxs84hlmkn3ykiiykq6ys7z8a29vkj9k26f43bhyny99yj7")))

(define-public crate-wayland-scanner-0.30.0-beta.4 (c (n "wayland-scanner") (v "0.30.0-beta.4") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "0gvj06gfj384vmg539bs96pl9sbmaw3i7dlwlpqw3qc73acj6hr0")))

(define-public crate-wayland-scanner-0.30.0-beta.5 (c (n "wayland-scanner") (v "0.30.0-beta.5") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1kda4lgzkwn318cra9dx1bg989g0klk73q29h4bzyxssvpw6xnx3")))

(define-public crate-wayland-scanner-0.30.0-beta.6 (c (n "wayland-scanner") (v "0.30.0-beta.6") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1ymh3x4dmncsayj4gchd0ga24y7c1si3f71bw9vbirdq32jn88wa")))

(define-public crate-wayland-scanner-0.30.0-beta.7 (c (n "wayland-scanner") (v "0.30.0-beta.7") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "1mlky0pmbzk937zmgrbjkvrcrbwg7h5msncnswr0qnigbiagzfp3")))

(define-public crate-wayland-scanner-0.30.0-beta.8 (c (n "wayland-scanner") (v "0.30.0-beta.8") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8") (d #t) (k 0)))) (h "11h3w7s4vjj5yq1sbpcksvyijhx3g9ja1b20y9f37xpl7p63r4w7")))

(define-public crate-wayland-scanner-0.29.5 (c (n "wayland-scanner") (v "0.29.5") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "xml-rs") (r ">=0.7, <0.9") (d #t) (k 0)))) (h "0lxx3i2kxnmsk421qx87lqqc9kd2y1ksjxcyg0pqbar2zbc06hwg")))

(define-public crate-wayland-scanner-0.30.0-beta.9 (c (n "wayland-scanner") (v "0.30.0-beta.9") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qgp23sm0mpk1zqvnvj7mrz4039mv4y89v353paxr8m8j845npsg")))

(define-public crate-wayland-scanner-0.30.0-beta.10 (c (n "wayland-scanner") (v "0.30.0-beta.10") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14zzvzp0w6gj4dzh0pb9vga01a3bxv411lyzbf2fzhs0y9k8afd1")))

(define-public crate-wayland-scanner-0.30.0-beta.11 (c (n "wayland-scanner") (v "0.30.0-beta.11") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.25") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0l1nlsjjv2yggpxnj9r8lbhq24ss3jljk0dd271rgsv5vm0fkky2") (y #t)))

(define-public crate-wayland-scanner-0.30.0-beta.12 (c (n "wayland-scanner") (v "0.30.0-beta.12") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b64aw53kdsjh0v6vqwk7g8vnmbd7da15229y7pw69mws07g9pl7")))

(define-public crate-wayland-scanner-0.30.0-beta.13 (c (n "wayland-scanner") (v "0.30.0-beta.13") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "07xz61a6r37ka23w8q8j66yj9kxa56yy5d4d0xaw3p0hhyi5rd8z")))

(define-public crate-wayland-scanner-0.30.0-beta.14 (c (n "wayland-scanner") (v "0.30.0-beta.14") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0f2qf5yfsswcxi0chp0b4zvvxlpipj54xn5ca8v6karbmmhp4h18")))

(define-public crate-wayland-scanner-0.30.0-beta.15 (c (n "wayland-scanner") (v "0.30.0-beta.15") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1fxz1y4p695jnm12qx1xv45m0hvsfil9w55ccgx23b5dzwd3b3jj") (r "1.59")))

(define-public crate-wayland-scanner-0.30.0 (c (n "wayland-scanner") (v "0.30.0") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)))) (h "0pg4khjn6w26yq14f53w9j3ykyrsgqg9p9rwr239j7fz7r5w2d28") (r "1.59")))

(define-public crate-wayland-scanner-0.30.1 (c (n "wayland-scanner") (v "0.30.1") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)))) (h "03ikmfwacsgbym2y4raf05knl1qjlgg81sy0174jxhzvayr77f5r") (r "1.59")))

(define-public crate-wayland-scanner-0.31.0 (c (n "wayland-scanner") (v "0.31.0") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)))) (h "1760n887j18lzd1ni087q7jzsmpcf7ny3dq2698zkjb56r02i3pv") (r "1.65")))

(define-public crate-wayland-scanner-0.31.1 (c (n "wayland-scanner") (v "0.31.1") (d (list (d (n "proc-macro2") (r "^1.0.11") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "similar") (r "^2") (d #t) (k 2)))) (h "10y2nq076x4zml8wc5bw75560rwvrsfpi35mdyc02w1854lsdcv3") (r "1.65")))

