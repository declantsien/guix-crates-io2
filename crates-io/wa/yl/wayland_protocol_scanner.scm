(define-module (crates-io wa yl wayland_protocol_scanner) #:use-module (crates-io))

(define-public crate-wayland_protocol_scanner-0.1.0 (c (n "wayland_protocol_scanner") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.88") (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.3.1") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.88") (d #t) (k 0)))) (h "0651c5zxwpzfx1wpmn7mrc00rvlj06v94l2wmk52ngm4m84rgf20")))

