(define-module (crates-io wa yl wayland) #:use-module (crates-io))

(define-public crate-wayland-0.0.1 (c (n "wayland") (v "0.0.1") (h "0p37qmdcg6ckckbj1vxx5y8mja2kid7qwkrlsbqb5rl1s8945j79")))

(define-public crate-wayland-0.0.2 (c (n "wayland") (v "0.0.2") (h "055bj8acssbndm356qj9v28h6w484n85f4683jslv2f99nw353kf")))

