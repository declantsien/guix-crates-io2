(define-module (crates-io wa yl wayland-cursor) #:use-module (crates-io))

(define-public crate-wayland-cursor-0.24.0 (c (n "wayland-cursor") (v "0.24.0") (d (list (d (n "wayland-client") (r "^0.24.0") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.24.0") (f (quote ("cursor"))) (d #t) (k 0)))) (h "1qdq2i8d88z14ri2ffdm33cklzrzpbrq8gbbdfgwsp3cnbdavhii")))

(define-public crate-wayland-cursor-0.24.1 (c (n "wayland-cursor") (v "0.24.1") (d (list (d (n "wayland-client") (r "^0.24.1") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.24.1") (f (quote ("cursor"))) (d #t) (k 0)))) (h "0sff3dyph06nrnfbwd53r6yxfhm9a3h97f1x5r042a9d2k7zl04r")))

(define-public crate-wayland-cursor-0.25.0 (c (n "wayland-cursor") (v "0.25.0") (d (list (d (n "wayland-client") (r "^0.25.0") (f (quote ("use_system_lib"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.25.0") (f (quote ("cursor"))) (d #t) (k 0)))) (h "1fnbnh8rl2cg0sr2212jjzwyf3kmli2ym752s73fmad4lrllyxrx")))

(define-public crate-wayland-cursor-0.26.0 (c (n "wayland-cursor") (v "0.26.0") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.26.0") (d #t) (k 0)) (d (n "xcur") (r "^0.1") (d #t) (k 0)) (d (n "xcursor") (r "^0.1") (d #t) (k 0)))) (h "0cpqqmc43q8gd8ww9g418i2kzhcl4s2c70pxa8n217dhwnw40ywg")))

(define-public crate-wayland-cursor-0.26.1 (c (n "wayland-cursor") (v "0.26.1") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.26.1") (d #t) (k 0)) (d (n "xcur") (r "^0.1") (d #t) (k 0)) (d (n "xcursor") (r "^0.1") (d #t) (k 0)))) (h "1vymxnqw4rxd47iwhzy739vxa0rlbqd0j9aaajwy0zp680wvpmmr")))

(define-public crate-wayland-cursor-0.26.3 (c (n "wayland-cursor") (v "0.26.3") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.26.3") (d #t) (k 0)) (d (n "xcur") (r "^0.1") (d #t) (k 0)) (d (n "xcursor") (r "^0.1") (d #t) (k 0)))) (h "02kkm7g218inv6i6hz4krj24xgq3m9m3xy1ykc15dr86p5a5rsa5")))

(define-public crate-wayland-cursor-0.26.4 (c (n "wayland-cursor") (v "0.26.4") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.26.4") (d #t) (k 0)) (d (n "xcur") (r "^0.1") (d #t) (k 0)) (d (n "xcursor") (r "^0.1") (d #t) (k 0)))) (h "0yzl4z24r2sn8fxk8b0ya3c483d9b39agy8af5s2hfq82gnxs8xq")))

(define-public crate-wayland-cursor-0.26.5 (c (n "wayland-cursor") (v "0.26.5") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.26.5") (d #t) (k 0)) (d (n "xcursor") (r "^0.2") (d #t) (k 0)))) (h "0gayg47yvq5rlz4jgpdsxij612qpc28qssmqblq2my13bpinmrnh")))

(define-public crate-wayland-cursor-0.26.6 (c (n "wayland-cursor") (v "0.26.6") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.26.6") (d #t) (k 0)) (d (n "xcursor") (r "^0.3") (d #t) (k 0)))) (h "0awj2wmx6716xh5y5i5wyykd0s0hlg2gnw5c6sya1g68p4vw5fzd")))

(define-public crate-wayland-cursor-0.27.0 (c (n "wayland-cursor") (v "0.27.0") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.27.0") (d #t) (k 0)) (d (n "xcursor") (r "^0.3") (d #t) (k 0)))) (h "0h8dnvsv4pb6gp10bdp6ng4v2bqy02b13gncr0w6yw1z39p397sk")))

(define-public crate-wayland-cursor-0.28.0 (c (n "wayland-cursor") (v "0.28.0") (d (list (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.28.0") (d #t) (k 0)) (d (n "xcursor") (r "^0.3") (d #t) (k 0)))) (h "0f2dj9b1b8l2nj2s5kc03a7wqn3a9qrla07gn4hgsyip2gmhqf5i")))

(define-public crate-wayland-cursor-0.28.1 (c (n "wayland-cursor") (v "0.28.1") (d (list (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.28.1") (d #t) (k 0)) (d (n "xcursor") (r "^0.3") (d #t) (k 0)))) (h "015gbdl7aa33xaqd4h63ybcyql7brg3jy4b33z0v5d5mqmcvjih4")))

(define-public crate-wayland-cursor-0.28.2 (c (n "wayland-cursor") (v "0.28.2") (d (list (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.28.2") (d #t) (k 0)) (d (n "xcursor") (r "^0.3") (d #t) (k 0)))) (h "1qdk3azgvcc0rbdizi5s6i3mynhmx0v4fsh5cigzpk6d051ipb8a")))

(define-public crate-wayland-cursor-0.28.3 (c (n "wayland-cursor") (v "0.28.3") (d (list (d (n "nix") (r "^0.18.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.28.3") (d #t) (k 0)) (d (n "xcursor") (r "^0.3") (d #t) (k 0)))) (h "0pvf96a9hg7b40vyvamcg491sa0006fr9bzf1xkaf8q22qn15syn")))

(define-public crate-wayland-cursor-0.28.4 (c (n "wayland-cursor") (v "0.28.4") (d (list (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "wayland-client") (r "^0.28.4") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "069cs4qjpwsi7p26cc8inwhx906n5lw0dhy4w3m4d0wlgq9sl77f")))

(define-public crate-wayland-cursor-0.28.5 (c (n "wayland-cursor") (v "0.28.5") (d (list (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "wayland-client") (r "^0.28.5") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1av59qnzxm82zszx0l6d5ryc0sh30hvmr6y3braxxxbjxiam8zmk")))

(define-public crate-wayland-cursor-0.28.6 (c (n "wayland-cursor") (v "0.28.6") (d (list (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "wayland-client") (r "^0.28.6") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "0nm61zkxwddq9x64dalcb5rihz2w6kz7blmxwx2nsn6ixn200qdy")))

(define-public crate-wayland-cursor-0.29.0 (c (n "wayland-cursor") (v "0.29.0") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "wayland-client") (r "^0.29.0") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1d9b5nzqlf1fhsnv3w2m5xlaapihwhhbdabynn1f5d80dsi9g4dz")))

(define-public crate-wayland-cursor-0.29.1 (c (n "wayland-cursor") (v "0.29.1") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "wayland-client") (r "^0.29.1") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "19q7fhkd66whg53qjq2silqi67ipx20i94dpb1z0kx6s51kbn6bc")))

(define-public crate-wayland-cursor-0.29.2 (c (n "wayland-cursor") (v "0.29.2") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "wayland-client") (r "^0.29.2") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1lzg92ikqkb5p2pjf7h7dy94zdpdp8a9rbapkrd9mwjvhkykggqk")))

(define-public crate-wayland-cursor-0.30.0-alpha1 (c (n "wayland-cursor") (v "0.30.0-alpha1") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-alpha1") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1flxrx7bqfl7h1s6gpm782qpk41d42v1ifkgy7gc9jf3iqfaar5q")))

(define-public crate-wayland-cursor-0.29.3 (c (n "wayland-cursor") (v "0.29.3") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "wayland-client") (r "^0.29.3") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "00xhq4xn7f4f8zq5wpzyi42p66yj5vm2h498h3m9z03hck0azcdw")))

(define-public crate-wayland-cursor-0.29.4 (c (n "wayland-cursor") (v "0.29.4") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "wayland-client") (r "^0.29.4") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1gd6aswkrdz556n54pjpd4rchw7jkgcx6hnrhgy62y2y7pqmh9y5")))

(define-public crate-wayland-cursor-0.30.0-alpha2 (c (n "wayland-cursor") (v "0.30.0-alpha2") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-alpha2") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "01wxkjdy7hsy9prlhfryj9g2sbrlz2z0vfkk8mvvkw6gjrmnkbvq")))

(define-public crate-wayland-cursor-0.30.0-alpha3 (c (n "wayland-cursor") (v "0.30.0-alpha3") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-alpha3") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1vzgn75dgm1m1i1gld3803w1yxd50wnyzbj5n2h5lp98zx9qpf70")))

(define-public crate-wayland-cursor-0.30.0-alpha4 (c (n "wayland-cursor") (v "0.30.0-alpha4") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-alpha4") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "070wcnanykna90ggr1pfwg8zas8xnx9pl2mnkz71175qf0hnnvhc")))

(define-public crate-wayland-cursor-0.30.0-alpha5 (c (n "wayland-cursor") (v "0.30.0-alpha5") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-alpha5") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1s52x59kj657wlgk3b1knbv0yb9s99dcxfwx4nvnd09qc1pyzvll")))

(define-public crate-wayland-cursor-0.30.0-alpha6 (c (n "wayland-cursor") (v "0.30.0-alpha6") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-alpha6") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1ibgqhwzgff0j3v5b47w6znd83a72lqci7h9j2rqqz877i7wam0q")))

(define-public crate-wayland-cursor-0.30.0-alpha7 (c (n "wayland-cursor") (v "0.30.0-alpha7") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-alpha7") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1kdd6z64ap8z5rr95pg331lga2lbfs0dj3plq5l6z93wbx24n4c2")))

(define-public crate-wayland-cursor-0.30.0-alpha8 (c (n "wayland-cursor") (v "0.30.0-alpha8") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-alpha8") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1h4kz0whchsrz6cqsajk3qj733xw3bpw6rjfaqxv7ki8wdwxccv1")))

(define-public crate-wayland-cursor-0.30.0-alpha9 (c (n "wayland-cursor") (v "0.30.0-alpha9") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-alpha9") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "06q4l7dny2f71j3qyk5z9lrrj6bivhbvdy4zv6i4vf3gynxmavnj")))

(define-public crate-wayland-cursor-0.30.0-alpha10 (c (n "wayland-cursor") (v "0.30.0-alpha10") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "wayland-client") (r "^0.30.0-alpha10") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "0jgi97m7x6794xyb51rfld87pfg04n47qbadh82n3bx3232j3nhq")))

(define-public crate-wayland-cursor-0.30.0-beta.1 (c (n "wayland-cursor") (v "0.30.0-beta.1") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.1") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "0ac9915mcdjqbva8chpcmc2s9b03b6k5cdn9nji20scf1mcib69p")))

(define-public crate-wayland-cursor-0.30.0-beta.2 (c (n "wayland-cursor") (v "0.30.0-beta.2") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.2") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1v8yyhm0h8sbiz8cxl4p2zg0jp2mvj1xkp7vwn7daxgn8f4mpx2y")))

(define-public crate-wayland-cursor-0.30.0-beta.3 (c (n "wayland-cursor") (v "0.30.0-beta.3") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.3") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "0qd2x9grs4pzqzpqz0lfscqpgx92zfvqrj346iw03x6iwjqaddq3")))

(define-public crate-wayland-cursor-0.30.0-beta.4 (c (n "wayland-cursor") (v "0.30.0-beta.4") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.4") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "0pwdyfml2gxsa9p5gk9mrp753xmhzj3afxybqphbk53fvjl3wa90")))

(define-public crate-wayland-cursor-0.30.0-beta.5 (c (n "wayland-cursor") (v "0.30.0-beta.5") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.5") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1k6sp6dfv0m3iv7sgqhbaw7ylj1hjb7l8h58iz974971nd66kin0")))

(define-public crate-wayland-cursor-0.30.0-beta.6 (c (n "wayland-cursor") (v "0.30.0-beta.6") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.6") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "15xrr54696z3wqw95j279c0pvvx43ln41f8gbz5zynkjxcf58ygp")))

(define-public crate-wayland-cursor-0.30.0-beta.7 (c (n "wayland-cursor") (v "0.30.0-beta.7") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.7") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "10r59khdp861wgayprq7xikr7xcqh75iawjf6ssx17275m06m786")))

(define-public crate-wayland-cursor-0.30.0-beta.8 (c (n "wayland-cursor") (v "0.30.0-beta.8") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.8") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "09q0j2jpm51kcb8mpsx08fjiq8q2zhb5ypgdj6l5nkydz912nwsk")))

(define-public crate-wayland-cursor-0.29.5 (c (n "wayland-cursor") (v "0.29.5") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("fs" "mman"))) (k 0)) (d (n "wayland-client") (r "^0.29.5") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "0qbn6wqmjibkx3lb3ggbp07iabzgx2zhrm0wxxxjbmhkdyvccrb8")))

(define-public crate-wayland-cursor-0.30.0-beta.9 (c (n "wayland-cursor") (v "0.30.0-beta.9") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.30.0-beta.9") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "150ibfghb8hz466gnqigmchlnc2pqj9lfgkp74s10fkmhvn4xda7")))

(define-public crate-wayland-cursor-0.30.0-beta.10 (c (n "wayland-cursor") (v "0.30.0-beta.10") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "=0.30.0-beta.10") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "0jqj9fcy97ydcdrpvnynr5rb0s2cnymyv9m2rm41ap7ay8jl77z3")))

(define-public crate-wayland-cursor-0.30.0-beta.11 (c (n "wayland-cursor") (v "0.30.0-beta.11") (d (list (d (n "nix") (r "^0.25.0") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "=0.30.0-beta.11") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "08x6l3hylql5w7vi1rz5185ql3hpph9q0irc3xqpx18gli1a2qqv") (y #t)))

(define-public crate-wayland-cursor-0.30.0-beta.12 (c (n "wayland-cursor") (v "0.30.0-beta.12") (d (list (d (n "nix") (r "^0.25.0") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "=0.30.0-beta.12") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "0v5a8h2blahz0l9a2aa10krx7m4wwz4ykgmn20laps3934rxwja2")))

(define-public crate-wayland-cursor-0.30.0-beta.13 (c (n "wayland-cursor") (v "0.30.0-beta.13") (d (list (d (n "nix") (r "^0.25.0") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "=0.30.0-beta.13") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "0qsjzi815kjcgqafp0wlq8mfl7s8acfvlk4p91k80g9sq6xy2p7s")))

(define-public crate-wayland-cursor-0.30.0-beta.14 (c (n "wayland-cursor") (v "0.30.0-beta.14") (d (list (d (n "nix") (r "^0.25.0") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "=0.30.0-beta.14") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1cbq7aiqwy474hqsrds152rns62jn6n19jykvpm0z9sljzws430y")))

(define-public crate-wayland-cursor-0.30.0-beta.15 (c (n "wayland-cursor") (v "0.30.0-beta.15") (d (list (d (n "nix") (r "^0.25.0") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "=0.30.0-beta.15") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "03dgbxg1zqd84cd0bs27p5nf5mc4rzagqlnpqdcqybllmqi936gc") (r "1.59")))

(define-public crate-wayland-cursor-0.30.0 (c (n "wayland-cursor") (v "0.30.0") (d (list (d (n "nix") (r "^0.26.0") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.30.0") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1al7qkdr0nr8nsddcv61zhj4gw3bxp9n48s4n03qns2bbc6kl31d") (r "1.59")))

(define-public crate-wayland-cursor-0.31.0 (c (n "wayland-cursor") (v "0.31.0") (d (list (d (n "nix") (r "^0.26.0") (f (quote ("mman"))) (k 0)) (d (n "wayland-client") (r "^0.31.0") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "0nxdyyg3a3649n316fbnm8rak5k90j580kfnfxn9src6x45a4jm4") (r "1.65")))

(define-public crate-wayland-cursor-0.31.1 (c (n "wayland-cursor") (v "0.31.1") (d (list (d (n "rustix") (r "^0.38.15") (f (quote ("shm"))) (d #t) (k 0)) (d (n "wayland-client") (r "^0.31.0") (d #t) (k 0)) (d (n "xcursor") (r "^0.3.1") (d #t) (k 0)))) (h "1fii68l6x235b867q96yx1xqkl16azkf5i841ldd24yxd2l5zkki") (r "1.65")))

