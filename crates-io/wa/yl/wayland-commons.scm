(define-module (crates-io wa yl wayland-commons) #:use-module (crates-io))

(define-public crate-wayland-commons-0.20.0 (c (n "wayland-commons") (v "0.20.0") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.0") (o #t) (d #t) (k 0)))) (h "005a3zgfp1zdawcsa3529khvh2gkjxncn4wkcawlkslxw6f0wby9") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.1 (c (n "wayland-commons") (v "0.20.1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.1") (o #t) (d #t) (k 0)))) (h "0gin29rv50z73siz10aw5iy850l0q69wmcvdm4645xqq797d4fx5") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.2 (c (n "wayland-commons") (v "0.20.2") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.2") (o #t) (d #t) (k 0)))) (h "0achdh4m8k2kahs4zx07f84c1y5i5yqavk0znjv1i65hjmw5aday") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.3 (c (n "wayland-commons") (v "0.20.3") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.3") (o #t) (d #t) (k 0)))) (h "1f2hi89l007q0nf7s5ba5x4jaz9lv5mqi87754m2wplsx55vhwmq") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.4 (c (n "wayland-commons") (v "0.20.4") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.4") (o #t) (d #t) (k 0)))) (h "058rihl267m1zyh642mw007w685hk3i0np8mll80rd5s306qdm4k") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.5 (c (n "wayland-commons") (v "0.20.5") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.5") (o #t) (d #t) (k 0)))) (h "171d7qa1fmwnv8gni8q3hgmxmc9rc0w44vblazqy23s84ia0zl9q") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.6 (c (n "wayland-commons") (v "0.20.6") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.6") (o #t) (d #t) (k 0)))) (h "0rl536nwrx5jkmnjbyhl20basbrq4xi37zis53f3pl8dql11wb00") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.7 (c (n "wayland-commons") (v "0.20.7") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.7") (o #t) (d #t) (k 0)))) (h "1vai9pqssgj47linhx2sd5szawixsa7f91dxhilrlyhshrx30wjz") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.8 (c (n "wayland-commons") (v "0.20.8") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.8") (o #t) (d #t) (k 0)))) (h "0lx3h7n462m1p315hhh65g8jn89vvnz2rcyhdsw44psdmq2arynk") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.9 (c (n "wayland-commons") (v "0.20.9") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.9") (o #t) (d #t) (k 0)))) (h "1w9ikzr6z659gvpvkfdvn5idpgk27hccl2lcm6l1zlghrh82qbif") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.10 (c (n "wayland-commons") (v "0.20.10") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.10") (o #t) (d #t) (k 0)))) (h "1gvxzdvav9gf41z7ngh2q11c29m0rmxfgp1dm13l03q53ngwgiaa") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.20.11 (c (n "wayland-commons") (v "0.20.11") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.11") (o #t) (d #t) (k 0)))) (h "175vmvlsbdj886gb02lgkpyf68wrrxx3wp7fv8gi4ifx68nx74lq") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.21.0-alpha1 (c (n "wayland-commons") (v "0.21.0-alpha1") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.0-alpha1") (o #t) (d #t) (k 0)))) (h "198yxhhb5qp9mqz6jvag3inqbfz85wh2pflggq7q5l0ahqs5rvv9") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.20.12 (c (n "wayland-commons") (v "0.20.12") (d (list (d (n "downcast-rs") (r "^1.0") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.20.12") (o #t) (d #t) (k 0)))) (h "0ynl274rqvhzrjcaqspckr92sbvibban9c7kwjx9iwavp5crsq6q") (f (quote (("native_lib" "wayland-sys") ("default" "native_lib"))))))

(define-public crate-wayland-commons-0.21.0 (c (n "wayland-commons") (v "0.21.0") (d (list (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.0") (o #t) (d #t) (k 0)))) (h "0iq3vg8vjjdyjzabv3kiqsjbxx4zcmvx788bac8xj7c7wf3ylyp3") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.1 (c (n "wayland-commons") (v "0.21.1") (d (list (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.1") (o #t) (d #t) (k 0)))) (h "1kwhmlxjbbdxld3cmxhd17aszh7hil0wpfkrn115fgw0jqdv96ax") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.2 (c (n "wayland-commons") (v "0.21.2") (d (list (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.2") (o #t) (d #t) (k 0)))) (h "0ckkbcghqrwb7qsq74j72gymvfs1xlh2vqnnbfh3vfyf0qrmbd6v") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.3 (c (n "wayland-commons") (v "0.21.3") (d (list (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.3") (o #t) (d #t) (k 0)))) (h "0sqyc1f1rzhwamsqa1pwcy8fq8yml8a3hf466vb9q94g80zcpkj4") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.4 (c (n "wayland-commons") (v "0.21.4") (d (list (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.4") (o #t) (d #t) (k 0)))) (h "0jm23y8hlh693zqh7hylc46ix0n1q62d0kv8w4z07gm2anqxc1kg") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.5 (c (n "wayland-commons") (v "0.21.5") (d (list (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.5") (o #t) (d #t) (k 0)))) (h "0h7gxd1nn6v3xrirh8bxljyv7884nn43wr0495kb9zy28d81qwah") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.6 (c (n "wayland-commons") (v "0.21.6") (d (list (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.6") (o #t) (d #t) (k 0)))) (h "1j7pbzpkr5w49c3iqya5km6rzbnl3z62gdpv6dxxx120yqfgsir3") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.7 (c (n "wayland-commons") (v "0.21.7") (d (list (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.7") (o #t) (d #t) (k 0)))) (h "1bx3nidncwvw1w139z9pnx1y5sx6kgbfq4c84h7wn8l2df7gk5fs") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.8 (c (n "wayland-commons") (v "0.21.8") (d (list (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.8") (o #t) (d #t) (k 0)))) (h "0la7bdqfhqwmnzv3hisr8gib6w3jfyyw6np1fk5nziwgrs1v4s4y") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.9 (c (n "wayland-commons") (v "0.21.9") (d (list (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.9") (o #t) (d #t) (k 0)))) (h "04g55djkds2swbc02xijha50islxlb6n7dkqz6sadpzrnw8c33bd") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.10 (c (n "wayland-commons") (v "0.21.10") (d (list (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.10") (o #t) (d #t) (k 0)))) (h "0cplp55njwz7ifvph8wpkm9pjqgw432qzgv1zs8ay46z31w971fr") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.21.11 (c (n "wayland-commons") (v "0.21.11") (d (list (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.11") (o #t) (d #t) (k 0)))) (h "1lzkc7k0aa3d1bphalrwi6aamax6v5iqazwjppllkh14qxfhrbwj") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.22.0 (c (n "wayland-commons") (v "0.22.0") (d (list (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.22.0") (o #t) (d #t) (k 0)))) (h "0l6rmlbacsf1jp0ijhh9yinw8ivrw7lfzd2h8b74sh2jcycn5w6g") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.22.1 (c (n "wayland-commons") (v "0.22.1") (d (list (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.22.1") (o #t) (d #t) (k 0)))) (h "02ilxw9xzcqf1lhl7g0j3d7q8cdh9rz198hi0pdlrldkagy97ls4") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.22.2 (c (n "wayland-commons") (v "0.22.2") (d (list (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.22.2") (o #t) (d #t) (k 0)))) (h "09c0xh41b6fcvnplbqqcs2g8lrk6j2mg9bp68nw66jzf0xf9dd17") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.23.0 (c (n "wayland-commons") (v "0.23.0") (d (list (d (n "nix") (r "^0.12") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.23.0") (d #t) (k 0)))) (h "0j1m7g1bmvsdwin153l2vmabw7p4nsn8698byglx4jl7v3b8wl3f")))

(define-public crate-wayland-commons-0.23.1 (c (n "wayland-commons") (v "0.23.1") (d (list (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.23.1") (d #t) (k 0)))) (h "1n0c39hwh6rw0yhj6782z43vfmhr8ra4zpik150z4y3p039hzs3d")))

(define-public crate-wayland-commons-0.21.12 (c (n "wayland-commons") (v "0.21.12") (d (list (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.12") (o #t) (d #t) (k 0)))) (h "17z0l048ash4rad9ncpss1h9kwbk6wmfxwv697x2w8fak59rmxnz") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.23.2 (c (n "wayland-commons") (v "0.23.2") (d (list (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.23.2") (d #t) (k 0)))) (h "0khhmwkhr52anx549gmphx18sxwsp7naikk8hbik7kqmdvr81yq0")))

(define-public crate-wayland-commons-0.23.3 (c (n "wayland-commons") (v "0.23.3") (d (list (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.23.3") (d #t) (k 0)))) (h "0sxm8yfshh190jr35s1pd59xsqi6xps232nv4m4zkvkgp7mpw1i3")))

(define-public crate-wayland-commons-0.23.4 (c (n "wayland-commons") (v "0.23.4") (d (list (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.23.4") (d #t) (k 0)))) (h "038ksl8nsdwwj6bvp4h52m0l7wf4h2mk6zsv6rf0mnasmj2npwn4")))

(define-public crate-wayland-commons-0.23.5 (c (n "wayland-commons") (v "0.23.5") (d (list (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.23.5") (d #t) (k 0)))) (h "1n21m99041k7374ca2wb8p18kv4v64s4j4r1x58q8pyvjnwn6163")))

(define-public crate-wayland-commons-0.21.13 (c (n "wayland-commons") (v "0.21.13") (d (list (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.21.13") (o #t) (d #t) (k 0)))) (h "1v1jpcsnn6cwwy5ii5pdl58i6b9slmi8mn4my4fpwrlbfsb8ih20") (f (quote (("native_lib" "wayland-sys"))))))

(define-public crate-wayland-commons-0.23.6 (c (n "wayland-commons") (v "0.23.6") (d (list (d (n "nix") (r "^0.14.1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.23.6") (d #t) (k 0)))) (h "1nyvcs6xxxzqgh0wvc7z0fgi89bf3h9p4qrbf77bnfbwlb8v0rmv")))

(define-public crate-wayland-commons-0.24.0 (c (n "wayland-commons") (v "0.24.0") (d (list (d (n "nix") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.24.0") (d #t) (k 0)))) (h "1jnx2b8k2l9d8w4xfndvn5vn9sag1vbx23mnksfz58r8w06bykpy")))

(define-public crate-wayland-commons-0.24.1 (c (n "wayland-commons") (v "0.24.1") (d (list (d (n "nix") (r "^0.15") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)) (d (n "spin") (r "^0.5") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.24.1") (d #t) (k 0)))) (h "0hji95mfj582zscqxx9xggvbv0vzk9rn7xizfqnk60k8546dv72k")))

(define-public crate-wayland-commons-0.25.0 (c (n "wayland-commons") (v "0.25.0") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.25.0") (d #t) (k 0)))) (h "0cjr2zy2a8p9zkdxdjwkxjf0c1j4ir3897kaihsp3krqc482zalc")))

(define-public crate-wayland-commons-0.26.0 (c (n "wayland-commons") (v "0.26.0") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.0") (d #t) (k 0)))) (h "167a1sjziclmvwyldg0ykz4ygy71xwq5qf81162zm4b5r6rdwqjg")))

(define-public crate-wayland-commons-0.26.1 (c (n "wayland-commons") (v "0.26.1") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.1") (d #t) (k 0)))) (h "1fzamyssx96arasl9jg52d3lk8qmnwddajlikpdn5xynzf7d8ds9")))

(define-public crate-wayland-commons-0.26.3 (c (n "wayland-commons") (v "0.26.3") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.3") (d #t) (k 0)))) (h "07wwy2jylvrmwbw77zix24v8ps4lkiv2d166gvzmn601dkbsw6lm")))

(define-public crate-wayland-commons-0.26.4 (c (n "wayland-commons") (v "0.26.4") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.4") (d #t) (k 0)))) (h "1005zfmg30cg4vra4jwaqlqjaay4nn4abnnnqzcrlp0qagvxc2iv")))

(define-public crate-wayland-commons-0.26.5 (c (n "wayland-commons") (v "0.26.5") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.5") (d #t) (k 0)))) (h "1iim5hm5gkxfbgz2xrazr3k6hywwcldgkgm5d7hdzwjkb2qwjzi8")))

(define-public crate-wayland-commons-0.26.6 (c (n "wayland-commons") (v "0.26.6") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.26.6") (d #t) (k 0)))) (h "0zr4ws3f1k4kd5rdgvllf94iz85b9iim73v7rp91jszkgi15my8k")))

(define-public crate-wayland-commons-0.27.0 (c (n "wayland-commons") (v "0.27.0") (d (list (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.27.0") (d #t) (k 0)))) (h "1wcm3f2jyq8kk71nagc0nk3fblvimlszy8af3f3dvafmd8ryjwp9")))

(define-public crate-wayland-commons-0.28.0 (c (n "wayland-commons") (v "0.28.0") (d (list (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.0") (d #t) (k 0)))) (h "0bcwbzrzpl3b07wmxp4rmr93a9is1vfp7xg0l37fcwpjlca8z1h1")))

(define-public crate-wayland-commons-0.28.1 (c (n "wayland-commons") (v "0.28.1") (d (list (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.1") (d #t) (k 0)))) (h "1ykwmd6rvlhqk2chfl8vaihgwhl0cp79qq13qz504vzyc1axf0kn")))

(define-public crate-wayland-commons-0.28.2 (c (n "wayland-commons") (v "0.28.2") (d (list (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.2") (d #t) (k 0)))) (h "1rm3g5lc0kpk9my815xi8nrvxrr6blzmgf7cz1zqf7qhvbz3y2r3")))

(define-public crate-wayland-commons-0.28.3 (c (n "wayland-commons") (v "0.28.3") (d (list (d (n "nix") (r "^0.18") (d #t) (k 0)) (d (n "once_cell") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.3") (d #t) (k 0)))) (h "0mid1sgy3bmiywnrhsr31b8w6zvk1ll2ci2as15ddv8pczvm0128")))

(define-public crate-wayland-commons-0.28.4 (c (n "wayland-commons") (v "0.28.4") (d (list (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.4") (d #t) (k 0)))) (h "1n7inxihk5zp8zf37d21c9cfb0b66bp6bb6i06xaa9ci9fwgxyvz")))

(define-public crate-wayland-commons-0.28.5 (c (n "wayland-commons") (v "0.28.5") (d (list (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.5") (d #t) (k 0)))) (h "0npxc023z3c5fvqcyd6msv9a2zl2d6ccvw074zzvqp9jh3immmwb")))

(define-public crate-wayland-commons-0.28.6 (c (n "wayland-commons") (v "0.28.6") (d (list (d (n "nix") (r "^0.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.28.6") (d #t) (k 0)))) (h "1npvcrwh8chjcji73c24hlp05zbv6dxv24bylb8bn4bhgja1f652")))

(define-public crate-wayland-commons-0.29.0 (c (n "wayland-commons") (v "0.29.0") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.0") (d #t) (k 0)))) (h "1dy2cgh5i77mzai1lircgc798834hkfcay6b0pd21ipgq1zkgmlk")))

(define-public crate-wayland-commons-0.29.1 (c (n "wayland-commons") (v "0.29.1") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.1") (d #t) (k 0)))) (h "1sqvbybvwj27rjmdq89qki2kyri4jv1ja17ik3q9wp1y7cdzapi6")))

(define-public crate-wayland-commons-0.29.2 (c (n "wayland-commons") (v "0.29.2") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.2") (d #t) (k 0)))) (h "1pzxk0zp8626g2fgbi9f6flhak7if4yab9ryya0gk0z2jrdhma3f")))

(define-public crate-wayland-commons-0.29.3 (c (n "wayland-commons") (v "0.29.3") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.3") (d #t) (k 0)))) (h "1qjys0z4hzv2bb9rffkv0jafgsncdj82jb9z31xax9jls42qvwln")))

(define-public crate-wayland-commons-0.29.4 (c (n "wayland-commons") (v "0.29.4") (d (list (d (n "nix") (r "^0.22") (d #t) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.4") (d #t) (k 0)))) (h "0gnk4a771i3g1k4fbzx54xnganpc9j68jrx8xj839hfp83iybxll")))

(define-public crate-wayland-commons-0.29.5 (c (n "wayland-commons") (v "0.29.5") (d (list (d (n "nix") (r "^0.24.1") (f (quote ("fs" "socket" "uio"))) (k 0)) (d (n "once_cell") (r "^1.1") (d #t) (k 0)) (d (n "smallvec") (r "^1") (d #t) (k 0)) (d (n "wayland-sys") (r "^0.29.5") (d #t) (k 0)))) (h "00m90bnxqy0d6lzqlyazc1jh18jgbjwigmyr0rk3m8w4slsg34c6")))

