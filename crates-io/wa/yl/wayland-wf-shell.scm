(define-module (crates-io wa yl wayland-wf-shell) #:use-module (crates-io))

(define-public crate-wayland-wf-shell-0.1.0 (c (n "wayland-wf-shell") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.29") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.3.3") (f (quote ("client_system" "log"))) (d #t) (k 0)) (d (n "wayland-client") (r "^0.31.2") (f (quote ("log"))) (d #t) (k 0)) (d (n "wayland-scanner") (r "^0.31.1") (d #t) (k 0)))) (h "14ix78jvp9hfv3v7q3w6v8vsd0ki84rdrq83xw7kr0amvdyq4dwv") (f (quote (("system_proto") ("default"))))))

