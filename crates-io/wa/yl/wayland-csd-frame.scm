(define-module (crates-io wa yl wayland-csd-frame) #:use-module (crates-io))

(define-public crate-wayland-csd-frame-0.1.0 (c (n "wayland-csd-frame") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "cursor-icon") (r "^1.0.0") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.1.1") (k 0)))) (h "05l16b4yz0ac9549sg96jyf4aibzpqki6b6k4l9lk0qb54q1w6bj") (r "1.64.0")))

(define-public crate-wayland-csd-frame-0.2.0 (c (n "wayland-csd-frame") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "cursor-icon") (r "^1.0.0") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.2.0") (k 0)))) (h "1vd98mfa88392ib3pksj4lpxpz43v7n62av8n67mq99g9nm28krn") (r "1.64.0")))

(define-public crate-wayland-csd-frame-0.2.1 (c (n "wayland-csd-frame") (v "0.2.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "cursor-icon") (r "^1.0.0") (d #t) (k 0)) (d (n "wayland-backend") (r ">=0.1.0, <0.3.0") (k 0)))) (h "1fq25l4ay1qj9zs6c7jnhff9ylb1pgj0dl250a27klqgm430iz12") (y #t) (r "1.64.0")))

(define-public crate-wayland-csd-frame-0.2.2 (c (n "wayland-csd-frame") (v "0.2.2") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "cursor-icon") (r "^1.0.0") (d #t) (k 0)) (d (n "wayland-backend_0_1") (r "^0.1.0") (o #t) (k 0) (p "wayland-backend")) (d (n "wayland-backend_0_2") (r "^0.2.0") (o #t) (k 0) (p "wayland-backend")))) (h "074s51dfvk2yf5xh192di7aaivww3i8f7l70d0xfzhjfslf5hiki") (f (quote (("default" "wayland-backend_0_2")))) (r "1.64.0")))

(define-public crate-wayland-csd-frame-0.3.0 (c (n "wayland-csd-frame") (v "0.3.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "cursor-icon") (r "^1.0.0") (d #t) (k 0)) (d (n "wayland-backend") (r "^0.3.0") (k 0)))) (h "0zjcmcqprfzx57hlm741n89ssp4sha5yh5cnmbk2agflvclm0p32") (r "1.65.0")))

