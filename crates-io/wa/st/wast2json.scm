(define-module (crates-io wa st wast2json) #:use-module (crates-io))

(define-public crate-wast2json-0.0.0 (c (n "wast2json") (v "0.0.0") (h "0sxsgpjsi3sr6hh6082d3f7diishlri6p01h056dzpkqb434drxc")))

(define-public crate-wast2json-0.1.0 (c (n "wast2json") (v "0.1.0") (d (list (d (n "assert-json-diff") (r "^2.0.2") (d #t) (k 2)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap-verbosity-flag") (r "^2.2.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "wasmparser") (r "^0.201.0") (d #t) (k 0)) (d (n "wasmprinter") (r "^0.201.0") (d #t) (k 2)) (d (n "wast") (r "^201.0.0") (d #t) (k 0)) (d (n "wat") (r "^1.201.0") (d #t) (k 0)))) (h "1vfljbbjn6bm0nl0bzw83yc4iy2kn86j3q88z7p3prn72shisa8h")))

