(define-module (crates-io wa st wast-parser) #:use-module (crates-io))

(define-public crate-wast-parser-0.1.0 (c (n "wast-parser") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)))) (h "1z83rfvvknqyxf6qm6x8d1zpgs5fbvi7hjhy2l6r2x55lrnb6cpm")))

(define-public crate-wast-parser-0.2.0 (c (n "wast-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)))) (h "0fwxz31n93q87a95m1ld39x7wwc31sv15izcbdfk74i7jj6nwycs")))

(define-public crate-wast-parser-0.3.0 (c (n "wast-parser") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)))) (h "00h3c5h19z8ckbkvn7l197b997hhdp1paqxvyh6p4j334wr6pmx7")))

(define-public crate-wast-parser-0.4.0 (c (n "wast-parser") (v "0.4.0") (h "0khcpzrw5s2c27l8yyq45zbw9j6376abcwswrrrs8bk716kasapj")))

