(define-module (crates-io wa st wast) #:use-module (crates-io))

(define-public crate-wast-1.0.0 (c (n "wast") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)) (d (n "wast-parser") (r "^0.1.0") (d #t) (k 0)))) (h "0jak05ll890525rimbcrk8l0a38vswnb63h8l7xnhjm2n804pp0c")))

(define-public crate-wast-1.0.1 (c (n "wast") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)) (d (n "wast-parser") (r "^0.2.0") (d #t) (k 0)))) (h "0pwmrbxjrkxfvhqrywx4mjmgd8ifrz39b5r11iq4zqgydzz3x4dd")))

(define-public crate-wast-1.0.2 (c (n "wast") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)) (d (n "wast-parser") (r "^0.3.0") (d #t) (k 0)))) (h "0pp47ra86xcipidvb4y44fkz9z056npr3lilj9iv6pl4p28r9k2s")))

(define-public crate-wast-2.0.0 (c (n "wast") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)))) (h "1vc4dbbqm8kll3hqbjb7sxvap5q61554hsq0r4qfhjwdpx024pz8")))

(define-public crate-wast-3.0.0 (c (n "wast") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)))) (h "0vs6lxms1m3sbb282d7zjwqik8vnk5r4qmb0969s2d38czw2rlkh")))

(define-public crate-wast-3.0.1 (c (n "wast") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)))) (h "0y817326l049955zdqfrqnk5snvdg92srw1r7sr6crp5p61v29wb")))

(define-public crate-wast-3.0.2 (c (n "wast") (v "3.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)))) (h "0i9jd9ln8aggbkd7f9pjwnqigwqzk4cs1zmv2sr5d9vkpl125dc6") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-3.0.3 (c (n "wast") (v "3.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)))) (h "005pyvb6akc4ihk4ynmcj6kf929i9pa99sg1zqjin0nbcw0s9zpw") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-3.0.4 (c (n "wast") (v "3.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)) (d (n "wasmparser") (r "^0.39") (d #t) (k 2)))) (h "1cf7kx6lp97197ck4v8v2b0m19z8rbxz4di4jydwwzzh83slhdi3") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-4.0.0 (c (n "wast") (v "4.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "1py7dn4wzdsis1z7z5vffn1anyfv8p0m3kybv9ncgrbz012kdl7x") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-5.0.0 (c (n "wast") (v "5.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "15db1rhmq16rwmampm8cncr289zr3b7hyvda6qd1i1fyq0xbdv08") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-5.0.1 (c (n "wast") (v "5.0.1") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "0wn3a1kgxkg3zkam09lzkxfcpk0sihd711wlsd09hjl5221yc7cd") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-6.0.0 (c (n "wast") (v "6.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "114rfwdjj0gv43xqmg5gxzx825z5b25yf9h1q4aimlyi55qdplry") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-7.0.0 (c (n "wast") (v "7.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "0g8nbprm5cfaiznl8s29z6a977vj4zag4wgs162rrcnyfv82k9qj") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-8.0.0 (c (n "wast") (v "8.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "138yn3j4b3hqgbg02yqnrjq7m691dgs6zfvb5wxm12hi2vbz779z") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-9.0.0 (c (n "wast") (v "9.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "0sniz1hcwbpxz5yvplqxjh21d8diskc259bb6yi2mjh5ah81cyzf") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-10.0.0 (c (n "wast") (v "10.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "01shq3pir0cyic9plnf0a1jrqa0nv0h9lc2l56zrvk7mxgn65ysf") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-11.0.0 (c (n "wast") (v "11.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "1plby3d6snpqppnb21ycmr8rx59abvfgl6la5spzrx36jax6fkfz") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-12.0.0 (c (n "wast") (v "12.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "15f6bkqpx810b4b3bnib484yqylrfnys3s40rwwxmg0i111bl586") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-13.0.0 (c (n "wast") (v "13.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "1bprfybmnz5jnb4p4i79j30mwm9x1blmgqym9l6pwvx2nkcan82v") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-14.0.0 (c (n "wast") (v "14.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "05sy71m147iax8x45cp4z8rh41dnwpkgi1x2dsknalrxqsa1rca7") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-15.0.0 (c (n "wast") (v "15.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "015wn47iivnvhsmcjplysnrlqyph6l1b85x1pdjyxbb8gwkza3d1") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-16.0.0 (c (n "wast") (v "16.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "1lgqrsgvg35qrdwq7zrifdpaqb7sfpvd2g965cfdfs6lyk4a0r45") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-17.0.0 (c (n "wast") (v "17.0.0") (d (list (d (n "leb128") (r "^0.2") (d #t) (k 0)))) (h "0chrvymzpl3bc362c3hhhzp25baziwc3aqprp8ys7z18p4v1q3js") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-18.0.0 (c (n "wast") (v "18.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0dzk70sfxcd89jpvz92az9mwg5bs76yzxlhb63wq22kl64sz5c81") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-19.0.0 (c (n "wast") (v "19.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0p28b4krxyzxx8mnyxlgxbwz5h9i0cdv9zmq0lli6zd0lzc30swn") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-20.0.0 (c (n "wast") (v "20.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "09490ikl6ikybwgkz09gj2gnzsyalz0f9wqy63n7kix8azhmrfaz") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-21.0.0 (c (n "wast") (v "21.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1dfyj6xlw5wxfr2p4ysrl7zmk3lawz0080b9f5nm5j1bdbv4860b") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-22.0.0 (c (n "wast") (v "22.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "080da6frn3fygblihyb21ypqw11x82ijaqd74ss94jc2gznj04py") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-23.0.0 (c (n "wast") (v "23.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0175g0z0d1ndvm3fsrvc3hp6n3ldgf6f5hpajd8v3h93hsj81c16") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-24.0.0 (c (n "wast") (v "24.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "001m7f3a3vblsyansp9ynia9mf4nvhq9rn5dhivyxcnh7ayy7wbg") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-25.0.0 (c (n "wast") (v "25.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1l1j5gsw6ian94ssj94rwr0zkdx4qvc4ha2mcc2y3vz4hr1pminq") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-25.0.1 (c (n "wast") (v "25.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0gplbw7b87fg2vqmdlm8lf49kf2vxb9ps545jfw37hmmxsww9iwl") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-25.0.2 (c (n "wast") (v "25.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "15b86im0ica7kbxyzjahb0lz3cxdhvmfbrxi1cfvqk6vbkhg8380") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-26.0.0 (c (n "wast") (v "26.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0cf7dqqd88si48w3v38lpgvp0fw2ad5qlc0niivg3z9v6iw3cjym") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-26.0.1 (c (n "wast") (v "26.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1ayx8avnprfdjrj6fh0qabx7lxf3m2z3zk4gdkg5x21yszp79wdk") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-27.0.0 (c (n "wast") (v "27.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0p43sd0jp1di8z9kfdn8vfhihkkh7w912n2dq92gmpvjd9gyzhy2") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-28.0.0 (c (n "wast") (v "28.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1qcvvwbca99474m2xac09jmjid0211v8sb376i807k7s3c38c1cw") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-29.0.0 (c (n "wast") (v "29.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "065c56n5vqc069gjdd38v2g6yh3mw3sjn91kv31n678k6y4jdwnw") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-30.0.0 (c (n "wast") (v "30.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1ljwzwmj8w73hvkr9ax8jlq5dyfhv78xi0p821466h7p49xr0ycv") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-31.0.0 (c (n "wast") (v "31.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1f980abvdiysdksh9igi9six5x4a0mqgqxlf3qx5537hcdmizswv") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-32.0.0 (c (n "wast") (v "32.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0z3wspx6da62k63bzc6d911ydk3adavhm5hg1bnn07fhc3ikwjn2") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-33.0.0 (c (n "wast") (v "33.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "06qwh49bqkalljihc8h96vkh8f37fn47sgi9f54j2y3zbhbzw10x") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-34.0.0 (c (n "wast") (v "34.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0g5bncly2n4wrqixxmbg028ljz5b4jzpxcp4pp860sp44alixrrx") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-35.0.0 (c (n "wast") (v "35.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1qd1z74hmcnkcc80lzys2i3lxkm8nl4x8vsi85ir5dcbl5nyjnnv") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-35.0.1 (c (n "wast") (v "35.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0mkvnfkkam9ykfrky5ada5nky9gxc0ga3giqbs9sw7kaz3lh0n0s") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-35.0.2 (c (n "wast") (v "35.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0s2d43g326dw21bygpalzjnr1fi83lx4afimg1h5hilrnkql1w9f") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-36.0.0 (c (n "wast") (v "36.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1i5rbfnnhnz3py1s2dwr5lm5h0n37nd3f2nbl4fmfr53fjipnpcb") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-37.0.0 (c (n "wast") (v "37.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0s1x0r2x5v6ybfsn5pzva06yxbqhyi7pacg003nps125d2kvkiwv") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-38.0.0 (c (n "wast") (v "38.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "1wkv397j81rzc1nspd3fxy2np9d42c7x9b1si7h9gx198vgjkg0f") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-38.0.1 (c (n "wast") (v "38.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)))) (h "0wf2y17sdqga0l5cwcllfid9yjg84zba4i3kzacch9pgdcjpn3df") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-39.0.0 (c (n "wast") (v "39.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "01fxycnka12j7x0fkzkwkak1ylrn4kjy7vkg30hp89ib8d9vvfz9") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-40.0.0 (c (n "wast") (v "40.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "1b34dj2rzx33rixraa04n0mhj97mp2pk1r4iw98bqg88if5g9d4v") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-41.0.0 (c (n "wast") (v "41.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)))) (h "0w1q3kiq2lav9wbc5h4cvip3ayq8vhpnk8vaq7nw8z41if5qk0pq") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-42.0.0 (c (n "wast") (v "42.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.13.0") (d #t) (k 0)))) (h "1m4xzv5vd8ippd10jsqyyr198rdyjybdm57jvbq3z63gjwzv1p5s") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-43.0.0 (c (n "wast") (v "43.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.14.0") (d #t) (k 0)))) (h "1a2hbi1dphg4r4dvp33sq75s9yz8016v2wvqjmad3yfvyvmym3s0") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-44.0.0 (c (n "wast") (v "44.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.14.0") (d #t) (k 0)))) (h "1p4wrxhr3fmzpmvbynvs34ax3cn9if7g54xjc19jxndp3hdlsisz") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-45.0.0 (c (n "wast") (v "45.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.15.0") (d #t) (k 0)))) (h "15hd6gzdfc9zm8f1fha839mm5d4icnhr5mb6nmb2gfcv9x64fv0q") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-46.0.0 (c (n "wast") (v "46.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.16.0") (d #t) (k 0)))) (h "06qk0xbf3kpbbsfd6ggyn47x1yj0pswng4d6pf8nisp3c2bb22pa") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-47.0.0 (c (n "wast") (v "47.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.17.0") (d #t) (k 0)))) (h "0wxdxl5ymr1sy8m81gh8i8gfgzqrgwa8lm7h2f5a4qif4v2cyz0i") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-47.0.1 (c (n "wast") (v "47.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "leb128") (r "^0.2") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.18.0") (d #t) (k 0)))) (h "1a7jjcrq3zk7193xkmqpac0ycy3nd2k03s2ijnjdx2lpyc18bf82") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-48.0.0 (c (n "wast") (v "48.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.19.0") (d #t) (k 0)))) (h "1kiwqbm5wfmrd4j6yhmcwzdlwcvm820jwawy1hkghk8nqxd5p0l4") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-49.0.0 (c (n "wast") (v "49.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.19.1") (d #t) (k 0)))) (h "0z2qacrj7bm86j7s3b56vpyv5z8mfv8w7ypazyplq90dsvy83vq5") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-50.0.0 (c (n "wast") (v "50.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.20.0") (d #t) (k 0)))) (h "0d5fy1pxnxp1r0ffaf6maianpgyvlmpq0zpyj4kq96f79afvbjx2") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-51.0.0 (c (n "wast") (v "51.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.21.0") (d #t) (k 0)))) (h "16dmql1rpak646ca4r7r5ngybnmilnfnkw057s6l75mgx7k23xm1") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-52.0.0 (c (n "wast") (v "52.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.21.0") (d #t) (k 0)))) (h "17jx0mhyddj3mip3jvc4vdiz7sqcw6w85chygnm40m6f42ihihm1") (f (quote (("wasm-module") ("default" "wasm-module")))) (y #t)))

(define-public crate-wast-52.0.1 (c (n "wast") (v "52.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.22.0") (d #t) (k 0)))) (h "0jzy0xbdxgk8ryg2ny7a6kawvxz8v0z5nv1cg9aj2bg8r1kvi7w2") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-52.0.2 (c (n "wast") (v "52.0.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.22.0") (d #t) (k 0)))) (h "1jcvdydx0gc4w351haa9rrpfmzrnf1rj27x3y0qcai01kgaryykh") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-52.0.3 (c (n "wast") (v "52.0.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.22.1") (d #t) (k 0)))) (h "0c34652wc5j2502xj0hwhb8k203px4wv4f5wwznhwa35ya02350m") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-53.0.0 (c (n "wast") (v "53.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)))) (h "1bx41hs6fk21mxnappn5k1j883y4cjhs75jarb9qy7bb34jgli42") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-54.0.0 (c (n "wast") (v "54.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.24.0") (d #t) (k 0)))) (h "05k57rqb1grm0p8yvd6j14191g57ayd7x7dbk3z5h2dicd5dzlzh") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-54.0.1 (c (n "wast") (v "54.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.24.1") (d #t) (k 0)))) (h "0aldimvmi2201p3gaxwqrc98h1my8xyxxf6bvbwg8dfq67bxjj1x") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-55.0.0 (c (n "wast") (v "55.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.25.0") (d #t) (k 0)))) (h "01az11fhx6yi1dp505vyx3842pzpf2ypkkx51f9z8wb583hx7129") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-56.0.0 (c (n "wast") (v "56.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.25.0") (d #t) (k 0)))) (h "1h1056ja9n8gl7vfklh65xm458l0b1bzwl3xflivwyqx0mf1hm3b") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-57.0.0 (c (n "wast") (v "57.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.26.0") (d #t) (k 0)))) (h "1p32cr0qc85824y9wqzrczyxlbici42xlxvl7hcj2i5c2znzbc3f") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-58.0.0 (c (n "wast") (v "58.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.27.0") (d #t) (k 0)))) (h "0af40x7qnbfhas08619cvzngxmpcsxvj7cq540f0k98h5npfqbip") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-59.0.0 (c (n "wast") (v "59.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.28.0") (d #t) (k 0)))) (h "0dgw0kkz8x9rqmjqm9ah6ph1hq73md4by4jzz46rjgqyr5w22iiq") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-60.0.0 (c (n "wast") (v "60.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.29.0") (d #t) (k 0)))) (h "1bcplshaf5n675f42kxv734vj19197fqz93jgqw30vjk9dscq1mx") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-61.0.0 (c (n "wast") (v "61.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.30.0") (d #t) (k 0)))) (h "1i2ssbmh8vvvz377kl81amcng34yqxai2c3xcl0dabxma5w38syw") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-62.0.0 (c (n "wast") (v "62.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.31.0") (d #t) (k 0)))) (h "0clsnbn16z68pmy8b3jxb5ixlcrcyrjrn0azi4v99mhrh23yxxy7") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-62.0.1 (c (n "wast") (v "62.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.31.1") (d #t) (k 0)))) (h "07wzrdq9igydq21ljdlx3nfx9q91zbw0yqmxky47ndxykpq0dbmq") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-63.0.0 (c (n "wast") (v "63.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.31.1") (d #t) (k 0)))) (h "0fpnr6akkp69rvgmj2vr3v79f756znb0gx7srvy7g2x4c0glfq15") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-64.0.0 (c (n "wast") (v "64.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.32.0") (d #t) (k 0)))) (h "1k1ph8r18hpsh2dv9q9c1z8bccwrklpsisxslxd24439zlkb4nd2") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-65.0.0 (c (n "wast") (v "65.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.32.0") (d #t) (k 0)))) (h "0y2b3a9w3jy9znzrzbsh2avzy2y3byx4qqdnzwa6rfmhwrjb130v") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-65.0.1 (c (n "wast") (v "65.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.33.1") (d #t) (k 0)))) (h "1yqavl4fa1jls66q0y9c37apw6zarz9q3ibi206hnjprmp5w3n2z") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-65.0.2 (c (n "wast") (v "65.0.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.33.2") (d #t) (k 0)))) (h "0ziijkwahji59gcwz8bkdnxafshdmksfij1jpzmw1hpq9ir8hnm5") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-66.0.0 (c (n "wast") (v "66.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.33.2") (d #t) (k 0)))) (h "01xdhdyix6jqikjxaf6k5gpblqxkcp00y8rjpyw8mma8p2dm59qd") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-66.0.1 (c (n "wast") (v "66.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.34.1") (d #t) (k 0)))) (h "1c54dzj6dx5famr6jjal3g7r87wk0kc5w3ym5gvy3f6ljmz4bla9") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-66.0.2 (c (n "wast") (v "66.0.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.35.0") (d #t) (k 0)))) (h "0wk0dv5x7f4r6i80jvbf19y2l09bsz7mqwvmqgr5dlbdmjq47jwk") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-67.0.0 (c (n "wast") (v "67.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.36.0") (d #t) (k 0)))) (h "0f0w7xarpqb8iqn1vanrxxxbdr7zhj4sj5rqp2c27zvpzlz97hin") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-67.0.1 (c (n "wast") (v "67.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.36.2") (d #t) (k 0)))) (h "07pvsg4swwcbn3f6ybp22hhg71bs9r8icgk6q4km4aq9mhpxhx59") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-68.0.0 (c (n "wast") (v "68.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.37.0") (d #t) (k 0)))) (h "01ad1w6s7j24llpbdrxpqafm5svgasrr65j05avsbcxwqqd0iwvv") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-69.0.0 (c (n "wast") (v "69.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.38.0") (d #t) (k 0)))) (h "11airl13is5swrdi2sz7kqckiscgy987wlxdpz8l669rs5d1p9gg") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-69.0.1 (c (n "wast") (v "69.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.38.1") (d #t) (k 0)))) (h "0yhy2bpbpsadf11ykrp02xnqvi22b5s96xadiqsxxbr1fcqkgvn1") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-70.0.0 (c (n "wast") (v "70.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.39.0") (d #t) (k 0)))) (h "0zi1pc4x3kg61qn67sw0flp561qx98vmxxxrc10r5ip1pdabrr1f") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-70.0.1 (c (n "wast") (v "70.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.40.0") (d #t) (k 0)))) (h "00jqr7qfwlp82rgwmsxpbxqbfs9zqzb8nv6760ms6iz7dw1ibm7m") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-70.0.2 (c (n "wast") (v "70.0.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.41.0") (d #t) (k 0)))) (h "1p0nl94529bjhiy978ilgmb3zh00gpif3ni3a43gabq4009hdmd3") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-71.0.0 (c (n "wast") (v "71.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.41.1") (d #t) (k 0)))) (h "0bla296pxv8zd1cwpq75yw9d59pcapa2pd2g616fs8s6x8wss3d1") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-71.0.1 (c (n "wast") (v "71.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.41.2") (d #t) (k 0)))) (h "09pxl6481n978yv8qacn33sqsmvczv9c93vyaf42d8sd6p23lz34") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-200.0.0 (c (n "wast") (v "200.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.200.0") (d #t) (k 0)))) (h "0rhkv4p1grvxrqxxi5sins4wjjapml4l1vsyn27vngmhwqa0v0fi") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-201.0.0 (c (n "wast") (v "201.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.201.0") (d #t) (k 0)))) (h "1aig4bvd0n4vvwj1nvjrmmngyyi2q2lv3ljg6wmkxnnp6kpy3xhy") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-202.0.0 (c (n "wast") (v "202.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.202.0") (d #t) (k 0)))) (h "1zpln3qzz3hs42p4yp7h5bw1fphwdajf1va2kcy9ap2i0h9b3g0z") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-203.0.0 (c (n "wast") (v "203.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.203.0") (d #t) (k 0)))) (h "1kc2lknpd71akpwf40mlv44giqhn12k0p39hxkm9m857kzvj0847") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-204.0.0 (c (n "wast") (v "204.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.204.0") (d #t) (k 0)))) (h "0qmd9q5m22wzii09rr8aag7xwd8rfld2fx9pl4plqg9bd4cxxqx0") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-205.0.0 (c (n "wast") (v "205.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.205.0") (d #t) (k 0)))) (h "1fxj2c728wrwpqygc8ia719bdik62g4vq2s5dpi4alivbccnl6j4") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-206.0.0 (c (n "wast") (v "206.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.206.0") (d #t) (k 0)))) (h "0nkrd4a47kpvmkh8ih1v2zk1g2v27ggjdgsfv3sv2q29xr9njn38") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-207.0.0 (c (n "wast") (v "207.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.207.0") (d #t) (k 0)))) (h "14scr1wjdb2fqnyyqr14hkk9n8iz1gfd51wl600sbgwlsjgvwh0f") (f (quote (("wasm-module") ("default" "wasm-module"))))))

(define-public crate-wast-208.0.0 (c (n "wast") (v "208.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.208.0") (d #t) (k 0)))) (h "15vbsmp3kbcm6yclvk6yk5gzimipps8bv6fach948dcv259shl04") (f (quote (("wasm-module") ("default" "wasm-module")))) (r "1.76.0")))

(define-public crate-wast-208.0.1 (c (n "wast") (v "208.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.208.1") (d #t) (k 0)))) (h "1wb77b4861sm7r9fd4i8jsp55nv388ll1qjlw39crqml4gqb605w") (f (quote (("wasm-module") ("default" "wasm-module")))) (r "1.76.0")))

(define-public crate-wast-209.0.0 (c (n "wast") (v "209.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.209.0") (d #t) (k 0)))) (h "11h5q226pjz4g3dspf6knh3jsls01qnsmwvsbdjm912akyi1csbr") (f (quote (("wasm-module") ("default" "wasm-module")))) (r "1.76.0")))

(define-public crate-wast-209.0.1 (c (n "wast") (v "209.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bumpalo") (r "^3.14.0") (d #t) (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "libtest-mimic") (r "^0.7.0") (d #t) (k 2)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.209.1") (d #t) (k 0)))) (h "197hr3zq23bqgamx8w95f8fa3iijagbmyxkjx494szhlyqpyzzwg") (f (quote (("wasm-module") ("default" "wasm-module")))) (r "1.76.0")))

