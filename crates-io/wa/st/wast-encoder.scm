(define-module (crates-io wa st wast-encoder) #:use-module (crates-io))

(define-public crate-wast-encoder-0.0.0 (c (n "wast-encoder") (v "0.0.0") (d (list (d (n "indexmap") (r "^2.2.3") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "string-pool") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.22") (d #t) (k 2)))) (h "12byf7khk33w9znkzhkzbvq6a8sgw9jbnkvm5hh4im67fk9y4g44") (f (quote (("wast"))))))

(define-public crate-wast-encoder-0.0.1 (c (n "wast-encoder") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "dependent_sort") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.3") (d #t) (k 0)) (d (n "nyar-error") (r "^0.1.12") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.22") (d #t) (k 2)))) (h "1ydmqqv8y2hgghyqd3b8yck07n0na9alzg49yk344z2fx5162rmv") (f (quote (("wast"))))))

(define-public crate-wast-encoder-0.0.2 (c (n "wast-encoder") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "dependent_sort") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.3") (d #t) (k 0)) (d (n "nyar-error") (r "^0.1.12") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.22") (d #t) (k 2)))) (h "0a074ix2xgwi8wcb97mjfdqs4yhm0kh5sf95hlrkj40ldfz1jxzv") (f (quote (("wast"))))))

(define-public crate-wast-encoder-0.1.0 (c (n "wast-encoder") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "dependent_sort") (r "^0.2.0") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.3") (d #t) (k 0)) (d (n "itertools") (r "^0.12.1") (d #t) (k 0)) (d (n "nyar-error") (r "^0.1.12") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (d #t) (k 0)) (d (n "wasm-parser") (r "^0.1.22") (d #t) (k 2)))) (h "1cjrlq94b1zrf40wakg5bw8ccbpf2pxjrb5qc75fmsip24jjsqiv") (f (quote (("wast"))))))

