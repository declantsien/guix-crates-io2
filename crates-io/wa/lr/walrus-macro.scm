(define-module (crates-io wa lr walrus-macro) #:use-module (crates-io))

(define-public crate-walrus-macro-0.1.0 (c (n "walrus-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1d74fh7l525h3zmz66wh5hlcx2y7syn68fs3s5kz7vi79d5nds50")))

(define-public crate-walrus-macro-0.2.0 (c (n "walrus-macro") (v "0.2.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14zhvccswaw2c1v63x3fjsqxl7z6vsg2hdyqd6qcasjs52ygqfdy")))

(define-public crate-walrus-macro-0.2.1 (c (n "walrus-macro") (v "0.2.1") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1k4h49kvqldda0z1rdfjajjhplnc6pk8v3cx8b3dgxmdk72lrsyg")))

(define-public crate-walrus-macro-0.3.0 (c (n "walrus-macro") (v "0.3.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "09f6zj9q8rd9yalcb5l6h4x9s8wb6xz12f5n34xrdy0wk1yszc8f")))

(define-public crate-walrus-macro-0.4.0 (c (n "walrus-macro") (v "0.4.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11vz5bvp1a85fhlypicmrlsldjqkrfidlr7mw31g8qjvapgd7490")))

(define-public crate-walrus-macro-0.5.0 (c (n "walrus-macro") (v "0.5.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "18yz9p00828n8ddvgs2gsbpasiwlvxr5maz581ccl6hici0fjbfr")))

(define-public crate-walrus-macro-0.6.0 (c (n "walrus-macro") (v "0.6.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10ppqjszrzm8q718dpky80yr04a3di60knjcv5hzywmq71km1vw7")))

(define-public crate-walrus-macro-0.7.0 (c (n "walrus-macro") (v "0.7.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0kanfy3rxr567nlhaylyx5g68ywchi4n9psrsi21qb2rjc4ifvgs")))

(define-public crate-walrus-macro-0.8.0 (c (n "walrus-macro") (v "0.8.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "155hby5m12agi06iyxyd4l88phisqgw94amll5f05f7zvfac3p1h")))

(define-public crate-walrus-macro-0.9.0 (c (n "walrus-macro") (v "0.9.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0nh7bxj4bhyz86cs4xbad807h1in1gy77kk03rk89sx9b4zrmzjy")))

(define-public crate-walrus-macro-0.10.0 (c (n "walrus-macro") (v "0.10.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zg4pw08s3bvjz8sjiylhh1hcg3vh3l5r7cxbcm8q795zy9lszyp")))

(define-public crate-walrus-macro-0.11.0 (c (n "walrus-macro") (v "0.11.0") (d (list (d (n "heck") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.20") (d #t) (k 0)) (d (n "quote") (r "^0.6.8") (d #t) (k 0)) (d (n "syn") (r "^0.15.11") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0lbj02g1rxxx1dr7py83q7ai54xy2jax7vc9xh2sqyf1fxkv1grj")))

(define-public crate-walrus-macro-0.12.0 (c (n "walrus-macro") (v "0.12.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0m75qi9csy14n3p6yn592q91x0yz20cwp2w3iifckr3bv3hacr04")))

(define-public crate-walrus-macro-0.13.0 (c (n "walrus-macro") (v "0.13.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0xjza0znfv3fgnq6mq9nvn0gjhglm1lfwkbzd2lmag9m73db0mw7")))

(define-public crate-walrus-macro-0.14.0 (c (n "walrus-macro") (v "0.14.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "152d6hv6a7v0bvi7clcb2jlmr8lsajjw5nhww28kl585shjnkh9b")))

(define-public crate-walrus-macro-0.15.0 (c (n "walrus-macro") (v "0.15.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1yg3sn4s10gzscbv27pp6zmava21grhymrgh4ibfrz5i6g54a61b")))

(define-public crate-walrus-macro-0.16.0 (c (n "walrus-macro") (v "0.16.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0gvf1dfsnvrx8swpkcdd02v1fzawhazpbp5kc15cnp5hhq2kg0y9")))

(define-public crate-walrus-macro-0.17.0 (c (n "walrus-macro") (v "0.17.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0wrmn4bn9y3xrp37g61s1g363rynhg4wwjviqx42n4v5hv57z4l0")))

(define-public crate-walrus-macro-0.18.0 (c (n "walrus-macro") (v "0.18.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05md757if2s4br17adxr0vvxm2qzsac4jkf5vh7ipjs41dlvphnp")))

(define-public crate-walrus-macro-0.19.0 (c (n "walrus-macro") (v "0.19.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1ms0rk841019hxglzqhlppjkfnhmaszda2qb2ih7vrvi5k95nvha")))

