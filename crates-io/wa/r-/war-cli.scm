(define-module (crates-io wa r- war-cli) #:use-module (crates-io))

(define-public crate-war-cli-0.1.0 (c (n "war-cli") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "notify") (r "^4.0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)) (d (n "war") (r "^0.1.0") (d #t) (k 0)))) (h "01qxrl9mgisrr7hi3kz85rpp9fgms4n9slgx7jfhgaspw76pfjgb") (f (quote (("strict"))))))

(define-public crate-war-cli-0.1.1 (c (n "war-cli") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "notify") (r "^4.0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)) (d (n "war") (r "^0.1.1") (d #t) (k 0)))) (h "1cykf4hfr2v1s7qkhq0n3lwky8mp7mh36xn1kk60x12gxs8bjf67") (f (quote (("strict"))))))

(define-public crate-war-cli-0.2.0 (c (n "war-cli") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "notify") (r "^4.0.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.3.1") (d #t) (k 0)) (d (n "war") (r "^0.2.0") (d #t) (k 0)))) (h "05ml40v9mygv7816p402hri5ji0fykrrlnbildsvp4nkidlvlzik") (f (quote (("strict"))))))

