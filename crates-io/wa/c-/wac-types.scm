(define-module (crates-io wa c- wac-types) #:use-module (crates-io))

(define-public crate-wac-types-0.0.0 (c (n "wac-types") (v "0.0.0") (h "05c7n5h6xkcd7z5kbkg8zzpm2d9azrry0ypb91f8gv4rx3v51das")))

(define-public crate-wac-types-0.1.0 (c (n "wac-types") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.202.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.202.0") (d #t) (k 0)))) (h "1x0n8r66iwsvaavkn82navnl1ld0bj5yf569wi3xyc5pyfqrf3v7") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wac-types-0.2.0 (c (n "wac-types") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.202.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.202.0") (d #t) (k 0)))) (h "1h12xqcz4f9nhbj3d01pwzmz0cddjbqnl9hqr6nc10dka6g8fsz4") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wac-types-0.3.0 (c (n "wac-types") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.202.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.202.0") (d #t) (k 0)))) (h "1mcl7x6mzgvkk8y9w3lfv5cjzmlz3xd22x8hihb90z6brp6blwnd") (s 2) (e (quote (("serde" "dep:serde"))))))

