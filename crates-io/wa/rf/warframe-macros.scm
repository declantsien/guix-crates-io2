(define-module (crates-io wa rf warframe-macros) #:use-module (crates-io))

(define-public crate-warframe-macros-0.1.0 (c (n "warframe-macros") (v "0.1.0") (d (list (d (n "darling") (r "^0.20.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1iq7j7pm5zzdlaz5sz6k38mgvkkq8g2bdl4xn74iyags12rnllwm")))

(define-public crate-warframe-macros-3.0.0 (c (n "warframe-macros") (v "3.0.0") (d (list (d (n "darling") (r "^0.20.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "02j7y6y64825f3039chjhfzyj9grrxp4gh02bary96qk3vy4k87s")))

(define-public crate-warframe-macros-3.1.0 (c (n "warframe-macros") (v "3.1.0") (d (list (d (n "darling") (r "^0.20.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1pajif5p3c7cqgbq71cm0xn2z4ypbqpkqx1b259s00x0b42i3ywm")))

(define-public crate-warframe-macros-0.2.0 (c (n "warframe-macros") (v "0.2.0") (d (list (d (n "darling") (r "^0.20.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "0vbnfrn0fsdx38kwjjz4ig7ivilbipn4a9h0f1r01iiiaabysj1z")))

(define-public crate-warframe-macros-3.1.1 (c (n "warframe-macros") (v "3.1.1") (d (list (d (n "darling") (r "^0.20.4") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.78") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)))) (h "1d0619f0lvxrrpiyfip3bw6d7p87vwma791dbmlz4xyw49lrdddz")))

