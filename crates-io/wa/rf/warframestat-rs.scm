(define-module (crates-io wa rf warframestat-rs) #:use-module (crates-io))

(define-public crate-warframestat-rs-0.0.1 (c (n "warframestat-rs") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1jcgqy3df6k1yh6srrjpsipkad4pa65vhgbwrlg2n89bbcz1i1hr")))

