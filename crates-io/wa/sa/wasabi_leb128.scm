(define-module (crates-io wa sa wasabi_leb128) #:use-module (crates-io))

(define-public crate-wasabi_leb128-0.3.0 (c (n "wasabi_leb128") (v "0.3.0") (d (list (d (n "gimli_leb128") (r "^0.2.4") (d #t) (k 2) (p "leb128")) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "1appk74j7drl6z3nz2naqfilf3niwa0yz295zavwhg6rqlwf441x")))

(define-public crate-wasabi_leb128-0.4.0 (c (n "wasabi_leb128") (v "0.4.0") (d (list (d (n "gimli_leb128") (r "^0.2.4") (d #t) (k 2) (p "leb128")) (d (n "num-traits") (r "^0.2.8") (d #t) (k 0)))) (h "0dxnxr5fcck8ksihyic31rka2xvjmabv4qs7dxq4pavdq8pmpy8y")))

