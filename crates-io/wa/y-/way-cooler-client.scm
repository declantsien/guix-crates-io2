(define-module (crates-io wa y- way-cooler-client) #:use-module (crates-io))

(define-public crate-way-cooler-client-0.0.0 (c (n "way-cooler-client") (v "0.0.0") (d (list (d (n "docopt") (r "0.6.*") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "term") (r "0.4.*") (d #t) (k 0)) (d (n "way-cooler-ipc") (r "0.0.*") (d #t) (k 0)))) (h "1n6mypdyy30ysl4vk91xaa4rjiq42vfj94pzivi5j3r870bkp6kc")))

