(define-module (crates-io wa y- way-lib-rust) #:use-module (crates-io))

(define-public crate-way-lib-rust-0.0.1 (c (n "way-lib-rust") (v "0.0.1") (d (list (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "18aaw87lncla5rgfc16ndl33lqqsp998knaz9nvphmpb1rjvy707")))

(define-public crate-way-lib-rust-0.0.2 (c (n "way-lib-rust") (v "0.0.2") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "0gc8mql0zz80ky79ld1jmdll1wmvb79kyzxggfn68sbalbwy0rrl")))

(define-public crate-way-lib-rust-0.0.3 (c (n "way-lib-rust") (v "0.0.3") (d (list (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ulid") (r "^1.0.0") (d #t) (k 0)))) (h "0nj6cswj810d1c6ddl0k6cglfk7c0crypkvhii992hd0wn111yr4")))

