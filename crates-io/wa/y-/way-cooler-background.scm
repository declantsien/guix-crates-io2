(define-module (crates-io wa y- way-cooler-background) #:use-module (crates-io))

(define-public crate-way-cooler-background-0.1.0 (c (n "way-cooler-background") (v "0.1.0") (d (list (d (n "byteorder") (r ">= 0.3, < 0.6") (d #t) (k 0)) (d (n "gtk") (r "^0.1.0") (f (quote ("v3_16"))) (d #t) (k 0)) (d (n "image") (r "^0.10.3") (d #t) (k 0)) (d (n "tempfile") (r "^2.1") (d #t) (k 0)) (d (n "wayland-client") (r "^0.6.0") (f (quote ("cursor" "dlopen"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.6.0") (f (quote ("client" "dlopen"))) (d #t) (k 0)))) (h "0kian6f3kr37vik7gk38kbc2qj196qvkqx5d0bg25r53py773vmh")))

