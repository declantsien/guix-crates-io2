(define-module (crates-io wa y- way-cooler-client-helpers) #:use-module (crates-io))

(define-public crate-way-cooler-client-helpers-0.1.0 (c (n "way-cooler-client-helpers") (v "0.1.0") (d (list (d (n "wayland-client") (r "^0.9.0") (f (quote ("cursor" "dlopen"))) (d #t) (k 0)) (d (n "wayland-sys") (r "^0.9.0") (f (quote ("client" "dlopen"))) (d #t) (k 0)))) (h "17f1idcrznmflpnqy684gqsbdynp8xwb9fa16miyp0l2c59g8c7p")))

