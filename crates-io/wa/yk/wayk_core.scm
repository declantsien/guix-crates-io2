(define-module (crates-io wa yk wayk_core) #:use-module (crates-io))

(define-public crate-wayk_core-0.1.0 (c (n "wayk_core") (v "0.1.0") (d (list (d (n "gfwx") (r "^0.3") (k 0)) (d (n "num") (r "^0.2") (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "wayk_proto") (r "^0.1") (d #t) (k 0)) (d (n "wayk_proto_derive") (r "^0.1") (d #t) (k 0)))) (h "0mwk3graqrw5rij2aapn4wl31160dlmld8was25v22w926hvpanx")))

(define-public crate-wayk_core-0.2.0 (c (n "wayk_core") (v "0.2.0") (d (list (d (n "gfwx") (r "^0.3") (k 0)) (d (n "num") (r "^0.2") (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "wayk_proto") (r "^0.2") (d #t) (k 0)) (d (n "wayk_proto_derive") (r "^0.2") (d #t) (k 0)))) (h "0q7b4pm126g9cwzm3kjgsca1md0x1g5ar6y83sprkvrpbvv0m14i")))

(define-public crate-wayk_core-0.2.1 (c (n "wayk_core") (v "0.2.1") (d (list (d (n "gfwx") (r "^0.3") (k 0)) (d (n "num") (r "^0.2") (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "wayk_proto") (r "^0.2") (d #t) (k 0)) (d (n "wayk_proto_derive") (r "^0.2") (d #t) (k 0)))) (h "1z64dz04adi3w19zmhn4y5xjrcrmcnqxasai5av3wx596las9a3j")))

(define-public crate-wayk_core-0.2.2 (c (n "wayk_core") (v "0.2.2") (d (list (d (n "gfwx") (r "^0.3") (k 0)) (d (n "num") (r "^0.3") (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "wayk_proto") (r "^0.2") (d #t) (k 0)) (d (n "wayk_proto_derive") (r "^0.2") (d #t) (k 0)))) (h "12kwp9icrx21xbdzlzvgb5yvvdi3rn2m71jp0vny7plmk63978yr")))

