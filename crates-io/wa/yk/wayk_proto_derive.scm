(define-module (crates-io wa yk wayk_proto_derive) #:use-module (crates-io))

(define-public crate-wayk_proto_derive-0.1.0 (c (n "wayk_proto_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bzdiibch7qixrd9192brpqrsv6ah3wmr8msm8c9yw9f2n76s5va")))

(define-public crate-wayk_proto_derive-0.2.0 (c (n "wayk_proto_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b7ajrjz5gxw1snbxw0q84lwhmqxbxbk4vvgjc57bldf8zaz6my3")))

