(define-module (crates-io wa pp wappu) #:use-module (crates-io))

(define-public crate-wappu-0.1.0 (c (n "wappu") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0722j81cm70fhsgcb6k0v19dshrsxppigd4kirmj1c3f6bl5rmnp")))

(define-public crate-wappu-0.2.0 (c (n "wappu") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f5jl78mi82mczdh31h3yg4ksb7xnbyjxwfcpffvwvhls5s34q4w")))

(define-public crate-wappu-0.2.1 (c (n "wappu") (v "0.2.1") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dklbmlpxs4w4zfr017yywwl06ymvkcvmgfzcmdj2hkp2lii4wsz")))

(define-public crate-wappu-0.2.2 (c (n "wappu") (v "0.2.2") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12pjgyssh205ppiaayz26n2fnfqkfnbrxncsv724l44yabj3sd25")))

(define-public crate-wappu-0.2.3 (c (n "wappu") (v "0.2.3") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ibv0xy06bkkxgb0nd715m9bl9vcwrmx7brm8x3zlnay9rqy20p1")))

(define-public crate-wappu-0.2.4 (c (n "wappu") (v "0.2.4") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("cookies"))) (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nbvs4k4b5p0wyhyk3syd88pnrfpamv24i76hlpzffqmgm6gm8lp")))

(define-public crate-wappu-0.3.0 (c (n "wappu") (v "0.3.0") (d (list (d (n "html5ever") (r "^0.26.0") (d #t) (k 0)) (d (n "markup5ever") (r "^0.11.0") (d #t) (k 0)) (d (n "markup5ever_rcdom") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "118dl3gdf57p8gygrx04wpp8426v1w9k1d0rympiiwxr0jjmbdk7") (f (quote (("captcha"))))))

