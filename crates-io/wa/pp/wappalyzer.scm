(define-module (crates-io wa pp wappalyzer) #:use-module (crates-io))

(define-public crate-wappalyzer-0.1.0 (c (n "wappalyzer") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (f (quote ("compat"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("cookies" "json" "blocking"))) (d #t) (k 0)) (d (n "scraper") (r "^0.10.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.99") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.1.1") (d #t) (k 0)))) (h "06ii7xqaj6mnvdhb88px9irv7117b7vq1rlnry6cz7bjzw6q313q")))

