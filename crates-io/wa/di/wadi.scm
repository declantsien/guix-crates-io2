(define-module (crates-io wa di wadi) #:use-module (crates-io))

(define-public crate-wadi-0.0.1 (c (n "wadi") (v "0.0.1") (h "1p4bnc7wwa675a00p5hrygddmbr7dxnqasj5cwxbsmcsv35w1xhg")))

(define-public crate-wadi-0.0.2 (c (n "wadi") (v "0.0.2") (d (list (d (n "cstring") (r "^0.0") (d #t) (k 0)))) (h "1npzhq0ygx3dcggvqks0ayr3gfawws60i5lxkksyvpjz329j61c5")))

(define-public crate-wadi-0.0.3 (c (n "wadi") (v "0.0.3") (d (list (d (n "cstring") (r "^0.0") (d #t) (k 0)))) (h "1jh6jrsh6rk4a6ljg5naxb91jbnk7jh1d34jqlqpbqfsibbwg2f0")))

(define-public crate-wadi-0.0.4 (c (n "wadi") (v "0.0.4") (d (list (d (n "cstring") (r "^0.0") (d #t) (k 0)))) (h "1dvagcs5g0qj61kn1zs8k6a4008gcvcpg1v2pcxnkwy07d6dpjn3")))

