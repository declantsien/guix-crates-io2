(define-module (crates-io wa bd wabdavc) #:use-module (crates-io))

(define-public crate-wabdavc-0.1.0 (c (n "wabdavc") (v "0.1.0") (d (list (d (n "quick-xml") (r "^0.27.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1v056lqvnnyvnj8rp756m0kjzlapq6ik2kxyhnh1wrhxydlpx894") (y #t)))

