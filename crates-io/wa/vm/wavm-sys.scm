(define-module (crates-io wa vm wavm-sys) #:use-module (crates-io))

(define-public crate-wavm-sys-0.1.0 (c (n "wavm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "llvm-sys") (r "^80") (d #t) (k 0)))) (h "0xqh5qzc6xvq1jhlz5ia0xrnin0m52h0n5g6kbd9zwyl7axrgqs0")))

