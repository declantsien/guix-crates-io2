(define-module (crates-io wa vm wavm) #:use-module (crates-io))

(define-public crate-wavm-0.1.0 (c (n "wavm") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wavm-sys") (r "^0.1") (d #t) (k 0)))) (h "1y2b3wi0glh94bqkfyxgklgpl2ydwp8878arn4khjmigzzajxa2d")))

