(define-module (crates-io wa rl warlock) #:use-module (crates-io))

(define-public crate-warlock-0.0.1 (c (n "warlock") (v "0.0.1") (h "0mfar989alxys6fsiiyk4c1gj44xl8wix7w0wmidj2mzv80cbdhn")))

(define-public crate-warlock-0.0.2 (c (n "warlock") (v "0.0.2") (h "1wlr2pfcpypj3wa34q7wl25nv1679rzxqbncwlmckzhh61n8qx2c")))

(define-public crate-warlock-0.0.3 (c (n "warlock") (v "0.0.3") (h "0az053bmgmz4qpn3h6f4jp0pzli147s9iv11ni03jxnw41hzb8b5")))

(define-public crate-warlock-0.0.4 (c (n "warlock") (v "0.0.4") (h "1fdcbw6dz4rwyvrc3b4gpw9qmd1swzn26331hlvc20s9igddlhjn")))

