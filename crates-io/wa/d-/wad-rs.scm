(define-module (crates-io wa d- wad-rs) #:use-module (crates-io))

(define-public crate-wad-rs-0.1.0 (c (n "wad-rs") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1qz263my24wmpi46zwddd00g3gbrxap2kj35j76jhzfkmv7ayk27") (f (quote (("parallel" "rayon")))) (y #t)))

(define-public crate-wad-rs-0.2.0 (c (n "wad-rs") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "12rhj6h05nvd49hls4d5rylxhnw6f045hl215n1ypndk1bkmxqzw") (f (quote (("parallel" "rayon")))) (y #t)))

