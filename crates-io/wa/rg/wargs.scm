(define-module (crates-io wa rg wargs) #:use-module (crates-io))

(define-public crate-wargs-0.1.0 (c (n "wargs") (v "0.1.0") (d (list (d (n "utfx") (r "^0.1.0") (d #t) (k 0)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_UI_Shell" "Win32_Foundation" "Win32_System_Memory" "Win32_System_Environment"))) (d #t) (k 0)))) (h "15dx4nfy6w7rsd8x3c0lsaaasikfysy83cqbk695kfv778g4ikh7")))

