(define-module (crates-io wa rg warg-credentials) #:use-module (crates-io))

(define-public crate-warg-credentials-0.4.1 (c (n "warg-credentials") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "keyring") (r "^2.3.0") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "warg-client") (r "^0.4.1") (d #t) (k 0)) (d (n "warg-crypto") (r "^0.4.1") (d #t) (k 0)))) (h "17z1ixkq85p91j2qrvcnafhr4qpxahb63lk9ns1555h03ax28qk2") (r "1.66.0")))

(define-public crate-warg-credentials-0.4.2 (c (n "warg-credentials") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "keyring") (r "^2.3.3") (f (quote ("platform-windows" "platform-macos" "linux-default-keyutils"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "warg-client") (r "^0.4.2") (d #t) (k 0)) (d (n "warg-crypto") (r "^0.4.2") (d #t) (k 0)))) (h "12nn2drs545l44myqrwgwj1sb2a8m1rfl1y4lkd7n0kkax7h2yx8") (r "1.76.0")))

(define-public crate-warg-credentials-0.4.3 (c (n "warg-credentials") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "keyring") (r "^2.3.3") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "warg-client") (r "^0.4.3") (d #t) (k 0)) (d (n "warg-crypto") (r "^0.4.3") (d #t) (k 0)))) (h "0xr34cgkly7b1216qyz5mm5zpn488iz39jbz38grjzkjdxgf6wwv") (r "1.76.0")))

(define-public crate-warg-credentials-0.5.0 (c (n "warg-credentials") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "keyring") (r "^2.3.3") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "warg-client") (r "^0.5.0") (d #t) (k 0)) (d (n "warg-crypto") (r "^0.5.0") (d #t) (k 0)))) (h "0dgkfrgz6gz7ddv61drrbv4w0rbp6rqag5l1cprw4mwi35i32xi4") (r "1.76.0")))

(define-public crate-warg-credentials-0.5.0-dev (c (n "warg-credentials") (v "0.5.0-dev") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "keyring") (r "^2.3.3") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "warg-client") (r "^0.5.0-dev") (d #t) (k 0)) (d (n "warg-crypto") (r "^0.5.0-dev") (d #t) (k 0)))) (h "1sspp7k2znhx0jv58fw004ss6zy7q953kfp39bn4d5sc67vlnrf3") (r "1.76.0")))

(define-public crate-warg-credentials-0.4.4 (c (n "warg-credentials") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "indexmap") (r "^2.2.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "keyring") (r "^2.3.0") (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "warg-client") (r "^0.4.4") (d #t) (k 0)) (d (n "warg-crypto") (r "^0.4.4") (d #t) (k 0)))) (h "0isfyd2y3xq8md28xx7cvynn75ykvg2x8jdcch2jzj8c3mh3nsci") (r "1.66.0")))

(define-public crate-warg-credentials-0.6.0 (c (n "warg-credentials") (v "0.6.0") (d (list (d (n "warg-client") (r "^0.6.0") (d #t) (k 0)))) (h "1gba9qspffqcgbb3j3ipvqmb4kbpl3mafs85173q7adn1j85dbdw") (r "1.76.0")))

(define-public crate-warg-credentials-0.7.0-dev (c (n "warg-credentials") (v "0.7.0-dev") (d (list (d (n "warg-client") (r "^0.7.0-dev") (d #t) (k 0)))) (h "16a1hcahj96j6s8426xhbbx6xd0aw1k6mcijqf411h4mz8m9ylg1") (r "1.76.0")))

(define-public crate-warg-credentials-0.7.0 (c (n "warg-credentials") (v "0.7.0") (d (list (d (n "warg-client") (r "^0.7.0") (d #t) (k 0)))) (h "0jhi8j7ys6nabx2f3js6plrjsdvkk3bnn508qpiza5pj0147a4s0") (r "1.76.0")))

