(define-module (crates-io wa lu walutomat) #:use-module (crates-io))

(define-public crate-walutomat-0.1.0 (c (n "walutomat") (v "0.1.0") (d (list (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "07l3yrm8rsi50knbzpp35rvvakvcch3n7998994rsd7iarg2zg3z")))

