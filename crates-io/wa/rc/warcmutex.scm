(define-module (crates-io wa rc warcmutex) #:use-module (crates-io))

(define-public crate-warcmutex-1.0.0 (c (n "warcmutex") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0lx1bw4khxw7kwkd49q5s6aci80war8ysq698h9bspmjz2gfc4fb")))

(define-public crate-warcmutex-1.0.1 (c (n "warcmutex") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "142d5b2cy6iisnakkhvi1xlsri321q2zq8gljax67bk73pbr9nzc")))

(define-public crate-warcmutex-1.0.2 (c (n "warcmutex") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "083qc82pa8gf50y0lzdv6vl4fnm8svp6m2m8k27jsc8mv8hgfb71")))

