(define-module (crates-io wa rc warc) #:use-module (crates-io))

(define-public crate-warc-0.0.1 (c (n "warc") (v "0.0.1") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "1grnjf7v846p03q5c66fp52km7c4hziqm2djjn0rbx2kkiagj8nd")))

(define-public crate-warc-0.0.2 (c (n "warc") (v "0.0.2") (d (list (d (n "chrono") (r "^0.2.19") (d #t) (k 0)) (d (n "hyper") (r "^0.7.2") (d #t) (k 0)) (d (n "uuid") (r "^0.1.18") (d #t) (k 0)))) (h "1pxj8mbxvyp4pw28y4jwzplnv4swxkiafdn4v26m0s7lfcjy52vz")))

(define-public crate-warc-0.0.3 (c (n "warc") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "hyper") (r "^0.11.0") (d #t) (k 0)) (d (n "uuid") (r "^0.5.0") (f (quote ("v4"))) (d #t) (k 0)))) (h "1gc1ik59wzzpmp7dwarg4qz2mkqwncb0dy9s7ibfxhr96fxfklq6")))

(define-public crate-warc-0.1.0 (c (n "warc") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0652bl1rg380cf5xkdxrwv8zgz5rqk23h4ssn0fmcbmx2p9k2vrb")))

(define-public crate-warc-0.1.1 (c (n "warc") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1y1kv1jd1jfsi1rppim73s6v0kgjgf9cvcd3vpzgl2h1h7c01qif")))

(define-public crate-warc-0.2.0 (c (n "warc") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0jf1jana4x2mi4qs75amldk9mb7ycqazdgbs4hd8vjn0vamhh0ka")))

(define-public crate-warc-0.2.1 (c (n "warc") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libflate") (r "^1") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0cv26b3q2vxiw5jzy20fpj66wk1d2fa9ryfin0rhl37r695625kp") (f (quote (("gzip" "libflate") ("default" "gzip"))))))

(define-public crate-warc-0.3.0 (c (n "warc") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libflate") (r "^1") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1wa85rsna2y2nqyca3y6qbp9ggfncdarfgdsm76ch92rcm9s2k11") (f (quote (("with_serde" "serde") ("gzip" "libflate") ("default" "gzip"))))))

(define-public crate-warc-0.3.1 (c (n "warc") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libflate") (r "^1") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0pkqplxaz2n860mic91ld5x2fkkwkvzyzfzvznya9y84kihqaig4") (f (quote (("with_serde" "serde") ("gzip" "libflate") ("default" "gzip"))))))

(define-public crate-warc-0.3.2 (c (n "warc") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libflate") (r "^1") (o #t) (d #t) (k 0)) (d (n "nom") (r "^5.1.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "1vr0y6lfmhf6ykfkhrszkw0dzq8cxydb6r754r0413ln26gxph5q") (f (quote (("with_serde" "serde") ("gzip" "libflate") ("default" "gzip"))))))

