(define-module (crates-io wa rc warcrwlock) #:use-module (crates-io))

(define-public crate-warcrwlock-1.0.0 (c (n "warcrwlock") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "03fyc4srfpnyrnkvp46p1j3a5nydcj5wjwrraxzksyrsxlzh2v1z") (y #t)))

(define-public crate-warcrwlock-1.0.1 (c (n "warcrwlock") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "175i2gxqjjlh0b43ihynw110pjckhq3ganvzjdn9x150hwn2av06") (y #t)))

(define-public crate-warcrwlock-1.0.2 (c (n "warcrwlock") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1dk0sxijr51hzi6c92f81dqn3sv5cqql17bi7qrxzvr6fzb9a21n") (y #t)))

(define-public crate-warcrwlock-1.0.3 (c (n "warcrwlock") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0mbikzv3kr5qbnv6naazdd2vpij9ykxpqbcsg252h6qlskhbncbf") (y #t)))

(define-public crate-warcrwlock-1.1.0 (c (n "warcrwlock") (v "1.1.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1dg1yvm6xbi2k20xq3x6j99l3i4lsngs4crybyqvbxk3p1bafgha") (y #t)))

(define-public crate-warcrwlock-1.2.0 (c (n "warcrwlock") (v "1.2.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "15p0mm8fcyzx8p2w9yn9pvamxr6kc0i44lihwdznsis6x2cmzxmh") (y #t)))

(define-public crate-warcrwlock-1.2.1 (c (n "warcrwlock") (v "1.2.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "09iys1llfal63b5355n2bkigzj9pd3vpfrcq7bvn0n11lbvy2kng") (y #t)))

(define-public crate-warcrwlock-1.3.0 (c (n "warcrwlock") (v "1.3.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "01p6sp7avsmffk7iaq8rxxlxf4d5016da2fvqr0rbpg36kn80y9m") (y #t)))

(define-public crate-warcrwlock-1.4.0 (c (n "warcrwlock") (v "1.4.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "1wg2bflcnadd3d1k27c149b7dbl8k6zgp7y4j8653q4xp8xic2zh") (y #t)))

(define-public crate-warcrwlock-1.4.1 (c (n "warcrwlock") (v "1.4.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0b77gkc0k79kz73xrmk1j5apx7snjqva5wd70f670n1pbxixyhdc") (y #t)))

(define-public crate-warcrwlock-1.4.2 (c (n "warcrwlock") (v "1.4.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)))) (h "0vsxmk65axiwb3b6ry5nlr9sw71w7qrq3w8mqjf37ag8a3vpb546") (y #t)))

(define-public crate-warcrwlock-1.5.0 (c (n "warcrwlock") (v "1.5.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0s8xk76423swr2sr5v50i1kpakxid1bxjba4gp54250w4jb4i2vx")))

(define-public crate-warcrwlock-1.5.1 (c (n "warcrwlock") (v "1.5.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1bkgxzw0vf64qrlkj2xflan1b8kcgrm8hf57cicbn4kxc9a1028d")))

(define-public crate-warcrwlock-1.5.2 (c (n "warcrwlock") (v "1.5.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1m2y7w414iz4p26zk96rzbw5v9nn7gg34sl8j5z7347f2sqqcgpx")))

(define-public crate-warcrwlock-1.6.0 (c (n "warcrwlock") (v "1.6.0") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "0888mls9iafvapjd0kba806c95ifxxxqacgq9q55820pyp9bg9ws")))

(define-public crate-warcrwlock-1.6.1 (c (n "warcrwlock") (v "1.6.1") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1xaz9fzdbqs1av69wcfh4d4mhq75208pfqla4dmfvjgbx5b0dk04")))

(define-public crate-warcrwlock-1.6.2 (c (n "warcrwlock") (v "1.6.2") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "128g0y1hdhddbj1i2m8ifdbzwszvv1drzghwbgjg0k7mzdgg1r5r")))

(define-public crate-warcrwlock-1.6.3 (c (n "warcrwlock") (v "1.6.3") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "static_init") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^2.0.26") (f (quote ("full"))) (d #t) (k 0)))) (h "1w8vds2r27kdp8lhasqr5fbaw8cpr5l67kcnr569jdhqvpgmf9iw")))

