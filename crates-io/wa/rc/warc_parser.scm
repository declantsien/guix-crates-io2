(define-module (crates-io wa rc warc_parser) #:use-module (crates-io))

(define-public crate-warc_parser-0.0.1 (c (n "warc_parser") (v "0.0.1") (d (list (d (n "nom") (r "*") (d #t) (k 0)))) (h "1ah9rajc12r5vpwd9b6icgc1cxppfankikfixghpba7vl5nabclz")))

(define-public crate-warc_parser-1.0.0 (c (n "warc_parser") (v "1.0.0") (d (list (d (n "nom") (r "^0.3.9") (d #t) (k 0)))) (h "1avyvfv9jjj0hdxf3winm08zq8q1p93znidqzv2jd91p6k0cm9dv")))

(define-public crate-warc_parser-1.0.1 (c (n "warc_parser") (v "1.0.1") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1l95wjj5s8bzq1mp1fap87sy8rij25hqs38hxhbwjmc6lkqcczkr")))

(define-public crate-warc_parser-2.0.0 (c (n "warc_parser") (v "2.0.0") (d (list (d (n "nom") (r "^4.2.3") (d #t) (k 0)))) (h "1nyrrf3xmskiibjhh786i3hss6h4rkvb1hmsmp4kfi155m4733l8")))

