(define-module (crates-io wa rc warcptr) #:use-module (crates-io))

(define-public crate-warcptr-0.0.0 (c (n "warcptr") (v "0.0.0") (h "0ca6i92c4rjnx8apivcjv2yp98sslfsgbx0idjn0y46yp1s2nrmj")))

(define-public crate-warcptr-0.1.0 (c (n "warcptr") (v "0.1.0") (h "0bgzn0ij0jwh9nx0icl42k22brzgclm3zk8f7cbcs6c5av4aylr5")))

