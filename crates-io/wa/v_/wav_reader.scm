(define-module (crates-io wa v_ wav_reader) #:use-module (crates-io))

(define-public crate-wav_reader-0.1.0 (c (n "wav_reader") (v "0.1.0") (h "19psyqgpv1bnysma2b7cny5kj2s7pj91haz080s19gm6dy474j2s")))

(define-public crate-wav_reader-0.1.1 (c (n "wav_reader") (v "0.1.1") (h "1dc6yqv8f5fwdp7xav3iy43zxrbbrw2xa5xknylq8hgawwgg2jfs")))

(define-public crate-wav_reader-0.1.2 (c (n "wav_reader") (v "0.1.2") (h "16sklmcfjla0pdqk2jnxx3h562vdy7x4vw07k4855vf3plampkq2")))

(define-public crate-wav_reader-0.1.3 (c (n "wav_reader") (v "0.1.3") (h "1d9wid00rnqgdqjhwixfds45w3yls00rlpgl1b6wgd7cyp9irs01")))

