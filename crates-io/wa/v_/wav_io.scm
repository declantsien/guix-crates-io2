(define-module (crates-io wa v_ wav_io) #:use-module (crates-io))

(define-public crate-wav_io-0.1.0 (c (n "wav_io") (v "0.1.0") (h "14mkrbflhwxm9v41y3x6wnkx518d4v2v9c2w8fgirmf15y4f1yig")))

(define-public crate-wav_io-0.1.1 (c (n "wav_io") (v "0.1.1") (h "1jfgz1mkpy99g6p9slr8871ymh0lbjcagjyf0l338hflpfpfia15")))

(define-public crate-wav_io-0.1.2 (c (n "wav_io") (v "0.1.2") (h "0ip7gsz5jn5wfsnqwv5bzyfzrxjbnc1445xym5pcg7645x7z3v95")))

(define-public crate-wav_io-0.1.3 (c (n "wav_io") (v "0.1.3") (h "185mkgmscm8i60j91vcqgd7k3scdi225nckplraf3nmwgcfzip0c")))

(define-public crate-wav_io-0.1.4 (c (n "wav_io") (v "0.1.4") (h "122ls6agfzgr7f0xnnwyshx3j6plvxyrf5p929zs7n45q99l8mhy")))

(define-public crate-wav_io-0.1.5 (c (n "wav_io") (v "0.1.5") (h "0nfar6p13ch6ylbazq46ma2mnpp2inzrn1h7c7f2j53fz2igxpdb")))

(define-public crate-wav_io-0.1.6 (c (n "wav_io") (v "0.1.6") (h "0612d8jq90n83gjzbb23i9yrllzw9942li7znv210pvy354pzxqi")))

(define-public crate-wav_io-0.1.7 (c (n "wav_io") (v "0.1.7") (h "04bmb14b30y61qn7flfq3mf2qv6p9hc72pbbnlf6x3vwg212xk7f")))

(define-public crate-wav_io-0.1.8 (c (n "wav_io") (v "0.1.8") (h "1wk6h6556gsdipq7m9z057c7hvzmw2acl3dcrsff0fyn3yywfa1r")))

(define-public crate-wav_io-0.1.10 (c (n "wav_io") (v "0.1.10") (h "0b9gxxds8km2l3275bdzfryvznb4n6nhisrcn3izclkfjv53v4rf")))

(define-public crate-wav_io-0.1.11 (c (n "wav_io") (v "0.1.11") (h "10vihqhhp5idiln6xyiw2v1jpljfqd2982x14aafdpll24w8i5yb")))

(define-public crate-wav_io-0.1.12 (c (n "wav_io") (v "0.1.12") (h "0s7l6lib1i486q5w378y3fbm3pyf5s24klfps31i25pc49n906v2")))

(define-public crate-wav_io-0.1.13 (c (n "wav_io") (v "0.1.13") (h "1mljwv89cfh1hbkj9bwfwf0azrdahriji6vc28ic7whhky0047kp")))

(define-public crate-wav_io-0.1.14 (c (n "wav_io") (v "0.1.14") (h "0cqxg944shb620y9hgrzk5c3kqfkpcfavfwllv5956ppz7zny8xk")))

