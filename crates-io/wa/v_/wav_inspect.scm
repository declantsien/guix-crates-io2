(define-module (crates-io wa v_ wav_inspect) #:use-module (crates-io))

(define-public crate-wav_inspect-0.1.0 (c (n "wav_inspect") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "wavers") (r "^1.3.1") (f (quote ("colored"))) (d #t) (k 0)))) (h "1m2ks73jzpylr592zgk57wi0snpicbn73z01h725hh4bfvsp25hd")))

