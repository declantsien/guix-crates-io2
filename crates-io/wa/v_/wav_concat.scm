(define-module (crates-io wa v_ wav_concat) #:use-module (crates-io))

(define-public crate-wav_concat-1.0.0 (c (n "wav_concat") (v "1.0.0") (h "1l0mcqcij6s4ls5g5iya3i6knijyrl0iqwlpsxan97blsmhy92y8")))

(define-public crate-wav_concat-1.1.0 (c (n "wav_concat") (v "1.1.0") (h "0rzh7z42yywcy1k38rhj7srws5i4k9pg5sd82aqjrmnf3gkld78k")))

