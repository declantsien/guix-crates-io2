(define-module (crates-io wa sp wasp-core) #:use-module (crates-io))

(define-public crate-wasp-core-0.0.0 (c (n "wasp-core") (v "0.0.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "wasmly") (r "^0.2.0") (d #t) (k 0)))) (h "073w8qbcvfm4yhfd61m955psjypd18y6bj3kfbyc907v86s385k3")))

