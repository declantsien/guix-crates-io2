(define-module (crates-io wa sp wasp) #:use-module (crates-io))

(define-public crate-wasp-0.0.1 (c (n "wasp") (v "0.0.1") (h "057m41xnnbz1v77z67cw1vd7x4d83x5a4vadgp1vpj3yvlycy6yg")))

(define-public crate-wasp-0.0.2 (c (n "wasp") (v "0.0.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "wasmly") (r "^0.0.8") (d #t) (k 0)))) (h "1gdd1hir8g2lpyyqlhv4ah271h81ch73kg2qk9qf1qc7i8wymzzg")))

(define-public crate-wasp-0.1.0 (c (n "wasp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.0.13") (d #t) (k 0)))) (h "17znxl88yby3ni9r90nzpjvj1frbp924v5zwfa3ax50cvzv6p9rz")))

(define-public crate-wasp-0.1.1 (c (n "wasp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.0.13") (d #t) (k 0)))) (h "1mm926khi11x28a9jcfr2r6l6qcx6r386ynncgbi742mylj43h2y")))

(define-public crate-wasp-0.1.2 (c (n "wasp") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.0.13") (d #t) (k 0)))) (h "1x8pxnaacsqy3d4nz9sf7mxp99sis0ch97v1hy4lbzvbk0nar3gy")))

(define-public crate-wasp-0.1.3 (c (n "wasp") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.0.13") (d #t) (k 0)))) (h "0wwzwff1xb2nmax0011q7n9hng0yw8fa40mlq1f0nlm92nns1d95")))

(define-public crate-wasp-0.1.4 (c (n "wasp") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.0.13") (d #t) (k 0)))) (h "1b71hcim0lf8qamyqrvfny2b0nx2w1nswnzjssyb3msdfrggyw0k")))

(define-public crate-wasp-0.1.5 (c (n "wasp") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.0.13") (d #t) (k 0)))) (h "0q7kbsrpb51zlblznl47sngrn92pwzwwd23bh77sid6lbj7jlxy0")))

(define-public crate-wasp-0.1.6 (c (n "wasp") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.0.13") (d #t) (k 0)))) (h "19k041mqsbfqiwcigqqz9v5iwnb53mjjsb5mn7ypkr38pq1qn8f8")))

(define-public crate-wasp-0.1.7 (c (n "wasp") (v "0.1.7") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.0.13") (d #t) (k 0)))) (h "1g8l4hya5h812v4jm03kdm2qcfvfkxd2vzpnnyyydj8a7mx90hg6")))

(define-public crate-wasp-0.1.8 (c (n "wasp") (v "0.1.8") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.1") (d #t) (k 0)))) (h "1w0g0kmyxrf7v9479spqc74wq1xnwfxfrgx4lvs4v7r34y5f4vz2")))

(define-public crate-wasp-0.2.0 (c (n "wasp") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.1.7") (d #t) (k 0)))) (h "01aiz1zwrq437ip8siw3q7y3592dcb3w2rlz5s8f523skg72x8gk")))

(define-public crate-wasp-0.2.1 (c (n "wasp") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.1.7") (d #t) (k 0)))) (h "1phdqf4qmbkkgw25l1k8s8x9jbi6yylam1qga8m896h9b8cwb2pp")))

(define-public crate-wasp-0.2.2 (c (n "wasp") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.1.7") (d #t) (k 0)))) (h "0f5zb6azzj9g5zlzpn16h4x4wxs4smdc8jqnbg57dkamsb09n9rz")))

(define-public crate-wasp-0.2.3 (c (n "wasp") (v "0.2.3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.1.7") (d #t) (k 0)))) (h "0g604y4h4n8489imdjx92y0y0sx9z19pzg28vixmq29j76f3938x")))

(define-public crate-wasp-0.2.4 (c (n "wasp") (v "0.2.4") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.1.7") (d #t) (k 0)))) (h "0l5z5vz8vwsva1i153csa4fgiignh8vi7il3jjysa1vhmzbhaw4i")))

(define-public crate-wasp-0.2.5 (c (n "wasp") (v "0.2.5") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.1.7") (d #t) (k 0)))) (h "1lyixz62nm4ppc1b8wmb9wqyw1icks3jzfypc5an565b5vhni305")))

(define-public crate-wasp-0.3.0 (c (n "wasp") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.1.7") (d #t) (k 0)))) (h "0md5y2mi23ljj7q8zqhhl36i8r3z6fzmyxqgrmfc9259iyiqb7fn")))

(define-public crate-wasp-0.3.1 (c (n "wasp") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.1.7") (d #t) (k 0)))) (h "13jp21a1i8w8xi9gzvxa79fcl5fpl0v4b543bqam8khaxz3hqvmm")))

(define-public crate-wasp-0.3.3 (c (n "wasp") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.1.7") (d #t) (k 0)))) (h "1br4mzarqg3p0pjhdr2c9lb5nvppvwikqspaivzcm4lbn58snn2i")))

(define-public crate-wasp-0.4.0 (c (n "wasp") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.2.0") (d #t) (k 0)))) (h "1psidybz2s5zyhch6cri0ggrgcpl9i6416z337rk6pq3r6s09c9l")))

(define-public crate-wasp-0.4.1 (c (n "wasp") (v "0.4.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.2.0") (d #t) (k 0)))) (h "1hcg4xvwad95v99ij65sa2iry22smp3vb4ycx10v4k3h1wmllfv2")))

(define-public crate-wasp-0.4.2 (c (n "wasp") (v "0.4.2") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.2.0") (d #t) (k 0)))) (h "0g5zj238a04xwn4ssrf25s92fsg2k4bqnv65r7k23zj89l9g780q")))

(define-public crate-wasp-0.5.0 (c (n "wasp") (v "0.5.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "nom") (r "^4.2") (f (quote ("verbose-errors"))) (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.2.0") (d #t) (k 0)))) (h "02rklkq1p0vdyqmyf4zr2vhb39k7rzcwvvfydik9d6nkp5ka2x7g")))

(define-public crate-wasp-0.5.1 (c (n "wasp") (v "0.5.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)) (d (n "wasmly") (r "^0.2.0") (d #t) (k 0)) (d (n "wasp-core") (r "^0") (d #t) (k 0)))) (h "1ghzqvhavy7bi64fcxdsdj40nr3wkfjz80sc4qz45jfd7r7m2g0w")))

