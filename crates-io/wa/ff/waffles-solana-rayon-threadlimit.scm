(define-module (crates-io wa ff waffles-solana-rayon-threadlimit) #:use-module (crates-io))

(define-public crate-waffles-solana-rayon-threadlimit-1.15.0 (c (n "waffles-solana-rayon-threadlimit") (v "1.15.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1icd2fs1c5mn222j1d70kji4r37r76j08krdljhzy684flajyiql")))

(define-public crate-waffles-solana-rayon-threadlimit-1.15.0-1 (c (n "waffles-solana-rayon-threadlimit") (v "1.15.0-1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1zrajkpyagsmzxsyp4hxgbpbbwgxnawdx2378maa3shd604fy14x")))

(define-public crate-waffles-solana-rayon-threadlimit-1.15.0-2 (c (n "waffles-solana-rayon-threadlimit") (v "1.15.0-2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1xa629gz31xkwdl61bjnzcn6hv86fi0v79z9y1i63vbycgrp7yd3")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1g1hicvkrmjpapprc8v8l6flxjmxdna2v6b4wwr0zsybwki80q50")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha-1 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha-1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "051j8n53za8pxmw31mi21xz77gjicnxdc5mgpgv15idcc5vfcpgg")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.1 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1fcy14pm6x55msdzin3jlgcsrwz6sgdhfiblymsb5vyv3cc637sy")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.2 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "145g4jawjvjpsdjgfaxpiyyx121vm0hhkrr274zpn88xi0gahspx")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.3 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0isqwv9axz8a4pxkn7fgfyzj64hvj3xw9manmc4zg2p34y0i3sw8")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.4 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0425iqzmzdhy039bvv6n1iwpdgncpda63yyyiszyj12ig05218k8")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.5 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0yfcvm7j9fwyl5qsh1gvl9plj0q0i3l0z8af81yrhmwjj4qxp6c0")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.6 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "03y5zi5rzva4f3yls25w5wjkrxy2jr38j9hnp4dbypm1a9ajzbzq")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.7 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1lch7vhmlk6b42c40aw9wzj1hcwyfj6ybw3zr6v13db6yhrp0dqg")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.8 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "0was57rs0kqyjkjjn8hwbnh2ljpbk44p9v7s21ilydrd1i7hbzlg")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.9 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "03s4v08af7mcw3pl87zif2vnxd4kylqr70r461p77ymbj3vifwm9")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.10 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.10") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1sglcql9pmc66pmlhng3ap40152mw5iw6m26lg07l77lvy9mqyn9")))

(define-public crate-waffles-solana-rayon-threadlimit-1.16.0-alpha.11 (c (n "waffles-solana-rayon-threadlimit") (v "1.16.0-alpha.11") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)))) (h "1g9dinffjaf566bji5lyilyqm9sihzs4z1m7cjivc08g6nj6q1yv")))

