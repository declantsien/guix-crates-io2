(define-module (crates-io wa ff waffles-solana-memory-management) #:use-module (crates-io))

(define-public crate-waffles-solana-memory-management-1.15.0 (c (n "waffles-solana-memory-management") (v "1.15.0") (h "13fcg3kap5j1jd6j0my07pm7m1b3a2ci94k5pw2w8fhyj3dl1drh")))

(define-public crate-waffles-solana-memory-management-1.16.0-alpha.1 (c (n "waffles-solana-memory-management") (v "1.16.0-alpha.1") (h "0ma5p4b58xx4zf6gzh8z63xd4l696pkm9iv25hzqcp7h15jzh86m")))

(define-public crate-waffles-solana-memory-management-1.16.0-alpha.2 (c (n "waffles-solana-memory-management") (v "1.16.0-alpha.2") (h "081jbk900qp66asrid8g2rj5ixiwjx236vi9szdlkzars4w4r888")))

(define-public crate-waffles-solana-memory-management-1.16.0-alpha.4 (c (n "waffles-solana-memory-management") (v "1.16.0-alpha.4") (h "086vyz3nn0w1sx1llh584z4smd7cs5b0xq2xx9li9h917959zgd1")))

(define-public crate-waffles-solana-memory-management-1.16.0-alpha.5 (c (n "waffles-solana-memory-management") (v "1.16.0-alpha.5") (h "0qp3n7yg9is875amrsbxh9jdabknfblkpw8gig03wxp15y663g77")))

(define-public crate-waffles-solana-memory-management-1.16.0-alpha.8 (c (n "waffles-solana-memory-management") (v "1.16.0-alpha.8") (h "03ihhdzfpa036cd4qf9alp16phpgmr6a1q9xn5pq6pwz9nkcx6qc")))

(define-public crate-waffles-solana-memory-management-1.16.0-alpha.9 (c (n "waffles-solana-memory-management") (v "1.16.0-alpha.9") (h "1yj7i7m15m33h0rjrwylp4xc4ncg32yklacg3q9wca04v3sh4g5k")))

(define-public crate-waffles-solana-memory-management-1.16.0-alpha.10 (c (n "waffles-solana-memory-management") (v "1.16.0-alpha.10") (h "1p41gyanbb756l1y7qkbs71z2zpgw6p7f77rfydsk6fl2j73731x")))

