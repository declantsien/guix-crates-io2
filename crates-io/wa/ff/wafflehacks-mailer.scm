(define-module (crates-io wa ff wafflehacks-mailer) #:use-module (crates-io))

(define-public crate-wafflehacks-mailer-0.2.0 (c (n "wafflehacks-mailer") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s4zz7f12wv6a4dzkj6ijajg6nx60cx0lsw37inik4zczdkanyi4")))

(define-public crate-wafflehacks-mailer-0.2.1 (c (n "wafflehacks-mailer") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.16") (f (quote ("json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s8rk9wz3gdlcfh63s0llpf2jwps1dzpgphjz6f2zhzqhrg0mr1a")))

