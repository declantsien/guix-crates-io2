(define-module (crates-io wa ff waffles-solana-measure) #:use-module (crates-io))

(define-public crate-waffles-solana-measure-1.15.0 (c (n "waffles-solana-measure") (v "1.15.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "waffles-solana-sdk") (r "=1.15.0") (d #t) (k 0)))) (h "0zn4ni2m33rhkdc6zicpac88v1fyqhi2cda0cnh79d765dyai22c")))

(define-public crate-waffles-solana-measure-1.15.0-1 (c (n "waffles-solana-measure") (v "1.15.0-1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.0") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "043py3mf2i0nxgp5gq590jdvry6zwlqynmyslc6lvy2v3m75r8xf")))

(define-public crate-waffles-solana-measure-1.15.0-2 (c (n "waffles-solana-measure") (v "1.15.0-2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.0-2") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "07hrzic4ih2z9q8pqkvmgm0lqqk3gk59mg4i4j3fmzh66cp54mdv")))

(define-public crate-waffles-solana-measure-1.16.0 (c (n "waffles-solana-measure") (v "1.16.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "1isdrxhmzndqqjm4idpaz1k4cjm1gnlz072kllrf7j4sdcxvsz9p")))

(define-public crate-waffles-solana-measure-1.16.0-alpha-1 (c (n "waffles-solana-measure") (v "1.16.0-alpha-1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha-1") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "14i6gdj5x1qx0gacjghzdxaiq02fl5g4b0hla1fsshh37p0nsmw4")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.1 (c (n "waffles-solana-measure") (v "1.16.0-alpha.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.1") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0z97l7dspj7zpj7x3j0qg5q7xz7xv6k3jbd4z3l9ya90x5v1s757")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.2 (c (n "waffles-solana-measure") (v "1.16.0-alpha.2") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.2") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "03myvildr0fq6i235bidlabw26vbll680iwqsjmpb5cb5vmw081b")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.3 (c (n "waffles-solana-measure") (v "1.16.0-alpha.3") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.2") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "1a5f1cl42nzyl06glaw25bsa9syybz6610ck8isxl43fw2vylf72")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.4 (c (n "waffles-solana-measure") (v "1.16.0-alpha.4") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.4") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0pmf2wk2dbp5hyid6a2dw5p8yzcasm4s1h9f0kszl2zjbfs11b51")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.5 (c (n "waffles-solana-measure") (v "1.16.0-alpha.5") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.5") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0s52f3am6wwmapg2913zxbqn3dggmb35fsr9ydhsly7zkf98i9k4")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.6 (c (n "waffles-solana-measure") (v "1.16.0-alpha.6") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.6") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0dybq7sxvjm7zlnjldnhx1wd2dp24czawrzzzaqw4866lh0nfzlh")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.7 (c (n "waffles-solana-measure") (v "1.16.0-alpha.7") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.7") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "13kyb8mq0fg9s0q7l0aidqsz81gszsklsssbwmdfqak3rh9m19ky")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.8 (c (n "waffles-solana-measure") (v "1.16.0-alpha.8") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.8") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "1dbhl4wvamv0z6d85f0yz4y5kc4sxkhgm3dmgjhzwcn5gg4l2vxa")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.9 (c (n "waffles-solana-measure") (v "1.16.0-alpha.9") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.9") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "1bcv8zpdcfbfd70cjyfzwb1f5x1z0anmwawj5qy4fqf402l0p9xj")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.10 (c (n "waffles-solana-measure") (v "1.16.0-alpha.10") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.10") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0d7kfx7gy3zgvbyzb6dpp4mg0z7015mvml0mnh2ay6y6n84hq8px")))

(define-public crate-waffles-solana-measure-1.16.0-alpha.11 (c (n "waffles-solana-measure") (v "1.16.0-alpha.11") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.10") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "1mj6f46f9avvcq90fva2z4z25l0wpnnalp013i835xjd3jivr8x4")))

