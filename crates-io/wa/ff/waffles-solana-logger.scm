(define-module (crates-io wa ff waffles-solana-logger) #:use-module (crates-io))

(define-public crate-waffles-solana-logger-1.15.0 (c (n "waffles-solana-logger") (v "1.15.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1dg7wqvyh96l0chyhnxzz8scvinhmhg11rvg5gn7z466yaz7i9fh")))

(define-public crate-waffles-solana-logger-1.15.0-2 (c (n "waffles-solana-logger") (v "1.15.0-2") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0lp2bq18sm3i2zcv0s36bppdxk1f6zly20yjfnra3d16jfra8pqs")))

(define-public crate-waffles-solana-logger-1.16.0 (c (n "waffles-solana-logger") (v "1.16.0") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1c9j89sjvr5f7lhhbx6xph7v7c936x7y331qbyyyd7yhj5swf37p")))

(define-public crate-waffles-solana-logger-1.16.0-alpha-1 (c (n "waffles-solana-logger") (v "1.16.0-alpha-1") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1jd1mq9i4lraa4dfif4iv12msm3iw9q5644rp80z7h3abqrq22nl")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.1 (c (n "waffles-solana-logger") (v "1.16.0-alpha.1") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1kqzrr6mkvzs1nqli3ihx9x8wr0sygjfp9yns3jmmqaqzgjcfmc8")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.2 (c (n "waffles-solana-logger") (v "1.16.0-alpha.2") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0pp198igaz9p8qqw9mpqgn0z9s77hvz126rlwrqqygmxnkzw5vjd")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.3 (c (n "waffles-solana-logger") (v "1.16.0-alpha.3") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0q2g4s1gz7iq5pqwyn3lw6a8lig8aqciw4wxpwax14ff2vnv4xwr")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.4 (c (n "waffles-solana-logger") (v "1.16.0-alpha.4") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1dn1wflg5h2lmpd3k2mciylw365m7zvllyn727injnzgwv87rkf9")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.5 (c (n "waffles-solana-logger") (v "1.16.0-alpha.5") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1jmlhgy3srsx63q90hp9nrb11g2cmqx41l6zq1iqb8v5n4qlhv1g")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.6 (c (n "waffles-solana-logger") (v "1.16.0-alpha.6") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1pgzb7s0a7rfbd42z65g2m188vkgxfnf7h49dibp9fj051gzmi8j")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.7 (c (n "waffles-solana-logger") (v "1.16.0-alpha.7") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0cdnbk7xavxdfdxmxir771c59p1w7dckqhzph87741l12wyjkfjx")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.8 (c (n "waffles-solana-logger") (v "1.16.0-alpha.8") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1l6pa0z9cq9i5zbw40s1kprzfryrbrdd5y98wiqw021avasvk5mh")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.9 (c (n "waffles-solana-logger") (v "1.16.0-alpha.9") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0rwzr3jssmxy9g0r2s6i20bpvb3hxq8cv4s0jy7rc4dvmzhxsh61")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.10 (c (n "waffles-solana-logger") (v "1.16.0-alpha.10") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1mxhcwf2j6dgd32ry1haxwi6yaad1h1h65ifca9w0mx2nf04wql5")))

(define-public crate-waffles-solana-logger-1.16.0-alpha.11 (c (n "waffles-solana-logger") (v "1.16.0-alpha.11") (d (list (d (n "env_logger") (r "^0.9.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "00lzj7j6v9g8c9wza3yya2vq0dsh99j5xwphsi7rci1zxk62mv36")))

