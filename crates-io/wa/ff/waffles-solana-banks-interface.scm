(define-module (crates-io wa ff waffles-solana-banks-interface) #:use-module (crates-io))

(define-public crate-waffles-solana-banks-interface-1.15.0 (c (n "waffles-solana-banks-interface") (v "1.15.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.0") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mlkf01zw01lnd0b061cfbl1w5cdpqg48zzdzgxngfq0na67gjyw")))

(define-public crate-waffles-solana-banks-interface-1.15.0-1 (c (n "waffles-solana-banks-interface") (v "1.15.0-1") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.0") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f7jbpgpmkg7llw18v3ps6dshdbm5lhyx4b8zmxi8rza1jm1c3xw")))

(define-public crate-waffles-solana-banks-interface-1.15.0-2 (c (n "waffles-solana-banks-interface") (v "1.15.0-2") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.15.0-2") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hcg0fl5msny2cxy920q3dhr2dik1m1xjz7xrg6byaqmvyb2bpa4")))

(define-public crate-waffles-solana-banks-interface-1.16.0 (c (n "waffles-solana-banks-interface") (v "1.16.0") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wppp6zkqgkla1n2clbv1lr9ycq6ri4d7xx2g6h7dgrv0955bjy1")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha-1 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha-1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha-1") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iw9ammzm3pxwb02736h852c0hilkmmxpscpfq1cj9l949dnq2a8")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.1 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.1") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.1") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rj1cmyhshmzf7hs7zg9majjpcsx21f3gva7vdknwd5ir4gg6z09")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.2 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.2") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.2") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f0rcn1wfry8fkswyi44bj2b6jlyn22jj15vdsqmzwizl8p4d76h")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.3 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.3") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.2") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17l92g4d0hmvlj32bq8rg1dpbg9my1qadh7z8lcf3m7616fhs2hc")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.4 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.4") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.4") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qd1703r07s65i1dmfrk9yv62bqf06bycvwv8dx7y3bsqphnv1li")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.5 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.5") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.5") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kcnb2lw8m59hkymbxyxsx4m6f9g93qiy74d6msk3r0pfcbww806")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.6 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.6") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.6") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qr62vp6kkx9fpy89pd2avhx65f8zdi4kx55ipj0nl1jb78ajvs5")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.7 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.7") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.7") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1r3f5d3jqhvyysj8vvbrr6af8v4yaga7kbw8piaw1fg94qma50w2")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.8 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.8") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.8") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1933fi1dl2dn3rkmqp26r5qzk4dryi9fn8b61h4jnjin8dyks3lz")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.9 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.9") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.9") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06g8x9raas1gi3v2pjrh2cygl2wgvymyhm18nk98x7im1x1aanms")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.10 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.10") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.10") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17lqwk6z1p6kdryfs3awfrglyhgnkynv6pqb71b14n4xqmw5rlam")))

(define-public crate-waffles-solana-banks-interface-1.16.0-alpha.11 (c (n "waffles-solana-banks-interface") (v "1.16.0-alpha.11") (d (list (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-sdk") (r "=1.16.0-alpha.10") (d #t) (k 0) (p "waffles-solana-sdk")) (d (n "tarpc") (r "^0.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xai12lbp0mpr99ysppyx740fsprg2c8x0qd0v4ssyx9r8hvm7y8")))

