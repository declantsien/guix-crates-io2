(define-module (crates-io wa ff waffles-solana-compute-budget-program) #:use-module (crates-io))

(define-public crate-waffles-solana-compute-budget-program-1.15.0 (c (n "waffles-solana-compute-budget-program") (v "1.15.0") (d (list (d (n "solana-program-runtime") (r "=1.15.0") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.15.0") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "11m6sz5vshs4cfawzpxfg14ca5b8vi376y901z5xs1ijk3ibc7lv")))

(define-public crate-waffles-solana-compute-budget-program-1.15.0-1 (c (n "waffles-solana-compute-budget-program") (v "1.15.0-1") (d (list (d (n "solana-program-runtime") (r "=1.15.0") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.15.0") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "1h3f74gv4b5ikfsjggck3a79v8qmsz2r1zfz8zjvhid3mgfryr3y")))

(define-public crate-waffles-solana-compute-budget-program-1.15.0-2 (c (n "waffles-solana-compute-budget-program") (v "1.15.0-2") (d (list (d (n "solana-program-runtime") (r "=1.15.0-2") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.15.0-2") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0zrdsw0izz89pwfhrpx0yggabv9mszwhbhj75j1qaqcc2b4lpw70")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0 (c (n "waffles-solana-compute-budget-program") (v "1.16.0") (d (list (d (n "solana-program-runtime") (r "=1.16.0") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "1cgbfq4kdkr08ydkj6m4l2ybxkcg27il7fi06v2mk93xd1gzrd86")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha-1 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha-1") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha-1") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha-1") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0wd9xhc21xnr8hjv3gq4hnfwmnkg2k5wmzihsn1sz8kzjawi99i1")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha.1 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha.1") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha.1") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha.1") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0lp9mphmkxm9y3haam3m5lqb0iiqchdfvlmnila11gahx5qlmq5j")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha.2 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha.2") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha.2") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha.2") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "00d28ycqp6mijb18039f3ig9bbsiisja9fghs93ak1z5db376znd")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha.3 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha.3") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha.2") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha.2") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "1ps5yff1462f6g1h8nv1bzbs5d021ijridf5l8zmcwps1h5dsqbl")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha.4 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha.4") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha.4") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha.4") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "1ap6699d13bqvfkhxqz9w5xr53ihr3h8v5wmj9d3rmw8sdb6iywz")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha.5 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha.5") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha.5") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha.5") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0n5kx3ph6kpck5zf8zxakv55nijrn341vwk5bgkqcwn9xjag01j1")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha.7 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha.7") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha.7") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha.7") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "03pybrbls8z2x2zjydl2vw20pcymmqjwpdncfnkbh6vxl4rrbh7k")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha.8 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha.8") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha.8") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha.8") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0vw2nc625m32sc43yfb6x9nkr85lsjrrpzf5sdvf333ndm61fyh2")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha.9 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha.9") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha.9") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha.9") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "00g7i2lpnqsb2gvlh0xw7f5azcj15y1vpl0q4na6k32dd3b76px0")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha.10 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha.10") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha.10") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha.10") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0b3xyb86libcsc26ggfqm7sngf8xv2nahrjphnk56sdinv4hdh2q")))

(define-public crate-waffles-solana-compute-budget-program-1.16.0-alpha.11 (c (n "waffles-solana-compute-budget-program") (v "1.16.0-alpha.11") (d (list (d (n "solana-program-runtime") (r "=1.16.0-alpha.10") (d #t) (k 0) (p "waffles-solana-program-runtime")) (d (n "solana-sdk") (r "=1.16.0-alpha.10") (d #t) (k 0) (p "waffles-solana-sdk")))) (h "0g10i2p0m5nwmxgpywlhzkm8l9f2hnf5ic95fly1xbkybzr490ia")))

