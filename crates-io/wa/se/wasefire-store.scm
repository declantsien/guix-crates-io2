(define-module (crates-io wa se wasefire-store) #:use-module (crates-io))

(define-public crate-wasefire-store-0.1.0 (c (n "wasefire-store") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "19fylf3ycp2fhdva40840vkbr06bq97pix6pny8babd91fs53ph1") (f (quote (("std"))))))

(define-public crate-wasefire-store-0.1.1 (c (n "wasefire-store") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.4.0") (d #t) (k 2)))) (h "1w0311hpg8fcwcxlnasyy3qg2hilzxjnn0arz7g8yjkpmg1gqzg3") (f (quote (("std"))))))

(define-public crate-wasefire-store-0.2.0 (c (n "wasefire-store") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.5.0") (d #t) (k 2)))) (h "06y94p5lky9vk00iq9jjm5q9pjky44yfd7ksjrr5bpbz7xpp5q07") (f (quote (("std"))))))

(define-public crate-wasefire-store-0.2.1 (c (n "wasefire-store") (v "0.2.1") (d (list (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)))) (h "16y6dya16xf4z8208bwa2vg0s9369j2afvhh3rsj8pj797hymn98") (f (quote (("std"))))))

(define-public crate-wasefire-store-0.2.2 (c (n "wasefire-store") (v "0.2.2") (d (list (d (n "tempfile") (r "^3.8.0") (d #t) (k 2)))) (h "0bjdaixzr2z4dy0jspgkhn0bjfn3mghqyig4hy5268fzcl0pajv2") (f (quote (("std"))))))

(define-public crate-wasefire-store-0.2.3 (c (n "wasefire-store") (v "0.2.3") (d (list (d (n "tempfile") (r "^3.9.0") (d #t) (k 2)))) (h "1a76qhx2alm3hl48jaz1xylzmz3lh7v8wmrksc93zzhcrbjc13rl") (f (quote (("std"))))))

