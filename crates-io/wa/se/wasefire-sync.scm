(define-module (crates-io wa se wasefire-sync) #:use-module (crates-io))

(define-public crate-wasefire-sync-0.1.0 (c (n "wasefire-sync") (v "0.1.0") (d (list (d (n "portable-atomic") (r "^1.6.0") (k 0)) (d (n "spin") (r "^0.9.8") (f (quote ("portable_atomic" "spin_mutex"))) (k 0)))) (h "1m3amccbkj5f3qz0ccq5bb8zc5kjabxxs63j4p1jy9i94py2amp4") (f (quote (("unsafe-assume-single-core" "portable-atomic/unsafe-assume-single-core"))))))

