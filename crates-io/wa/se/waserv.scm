(define-module (crates-io wa se waserv) #:use-module (crates-io))

(define-public crate-waserv-0.1.4 (c (n "waserv") (v "0.1.4") (d (list (d (n "path-tree") (r "^0.1") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("ExtendableEvent" "Request" "RequestInit" "Response" "ResponseInit" "Headers" "Url"))) (d #t) (k 0)))) (h "0lmwva8idx0aimjckjx51d7mlyprnkk88240y50jay0c3gav0brk")))

