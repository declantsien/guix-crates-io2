(define-module (crates-io wa se wasefire-logger) #:use-module (crates-io))

(define-public crate-wasefire-logger-0.1.0 (c (n "wasefire-logger") (v "0.1.0") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "0fknf9di7h60ydqqb7k3g6fibvdf51mchh2jkf2v7nxcz7sqxw0g") (s 2) (e (quote (("log" "dep:lazy_static" "dep:log") ("defmt" "dep:defmt"))))))

(define-public crate-wasefire-logger-0.1.1 (c (n "wasefire-logger") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "19dyk4dqxadba69szdmc5wzbiw32y675yd42pf4avdj0rjks61pr") (s 2) (e (quote (("log" "dep:lazy_static" "dep:log") ("defmt" "dep:defmt"))))))

(define-public crate-wasefire-logger-0.1.2 (c (n "wasefire-logger") (v "0.1.2") (d (list (d (n "defmt") (r "^0.3.4") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (o #t) (d #t) (k 0)))) (h "0ifkci5jd0ciamy7s8ahbf2r9mcgyqrh9lqq1786a02i6wzlsxqr") (s 2) (e (quote (("log" "dep:lazy_static" "dep:log") ("defmt" "dep:defmt"))))))

(define-public crate-wasefire-logger-0.1.3 (c (n "wasefire-logger") (v "0.1.3") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (k 0)))) (h "0w2i8wlf80r61cilhq9wi8pys4117z8hg95xlaq4p815dh19n9dk") (s 2) (e (quote (("log" "dep:log") ("defmt" "dep:defmt"))))))

(define-public crate-wasefire-logger-0.1.4 (c (n "wasefire-logger") (v "0.1.4") (d (list (d (n "defmt") (r "^0.3.5") (o #t) (k 0)) (d (n "log") (r "^0.4.20") (o #t) (k 0)))) (h "0i7qnmr404xsr4ymbafqazkca080giiyjq0s5cmhw0vq9b62k3b3") (s 2) (e (quote (("log" "dep:log") ("defmt" "dep:defmt"))))))

