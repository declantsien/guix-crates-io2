(define-module (crates-io wa se waseda-moodle) #:use-module (crates-io))

(define-public crate-waseda-moodle-0.1.0 (c (n "waseda-moodle") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "html-extractor") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("cookies" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1qg0hdwxi0anpyliizxsisq0sn97raafx5jrf4gz2bsr6v72glpw")))

(define-public crate-waseda-moodle-0.1.1 (c (n "waseda-moodle") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "html-extractor") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("cookies" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "085fkv94511qisxaxlnv20k3y1p01nw9zm5znfcyzvfxj5skavhp")))

(define-public crate-waseda-moodle-0.1.2 (c (n "waseda-moodle") (v "0.1.2") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "html-extractor") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("cookies" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "11mwb68bjfrb1xp6bkc3sv3ar4xgfksysjsjwsjbbvnrz0hnsl67")))

(define-public crate-waseda-moodle-0.2.0 (c (n "waseda-moodle") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "html-extractor") (r "^0.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.4") (f (quote ("cookies" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "06nx3dmnqx9zr9r70lwwgvkwk3vfm885d8why06saj4m4xa4nxxn")))

