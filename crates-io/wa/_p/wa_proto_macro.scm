(define-module (crates-io wa _p wa_proto_macro) #:use-module (crates-io))

(define-public crate-wa_proto_macro-0.1.0 (c (n "wa_proto_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ymx6g43zm233qqk6qmpjb7z77rfy5slblq41j24hq79n50s3298")))

(define-public crate-wa_proto_macro-0.1.2 (c (n "wa_proto_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ip0nh7y0x8q6arv13pjkghnyk1r1jln6kma0vk67hsfsr0b7bsm")))

