(define-module (crates-io wa te waterfall) #:use-module (crates-io))

(define-public crate-waterfall-0.1.0 (c (n "waterfall") (v "0.1.0") (h "1ra9k1g3g80z8ny6nm8hisra9aj7irk9y7d9lg4iyvfyx9ld67ci")))

(define-public crate-waterfall-0.1.1 (c (n "waterfall") (v "0.1.1") (d (list (d (n "heatmap") (r "^0.1.4") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.0") (d #t) (k 0)))) (h "0m52wvcm9wrw2ja693890bkfw25vjr0qdcsjzjh5wqp5h38ndk0c")))

(define-public crate-waterfall-0.1.2 (c (n "waterfall") (v "0.1.2") (d (list (d (n "heatmap") (r "^0.1.5") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.0") (d #t) (k 0)))) (h "1kh236ywba64byg19wdxia3n1dcgbqqan7941j2hv902ybq2sg7c")))

(define-public crate-waterfall-0.1.3 (c (n "waterfall") (v "0.1.3") (d (list (d (n "heatmap") (r "^0.1.6") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "0ndd2vkjg78hbdiyi1kx0vzd8p3ndyh93yihpz1yyp647inds8ak") (y #t)))

(define-public crate-waterfall-0.1.4 (c (n "waterfall") (v "0.1.4") (d (list (d (n "heatmap") (r "^0.1.7") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "1d93w8mxdz8rcgwsqj9zsaf67710hhps5agn27xa1vgg706j2r83")))

(define-public crate-waterfall-0.2.0 (c (n "waterfall") (v "0.2.0") (d (list (d (n "heatmap") (r "^0.2.0") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "0ki3x2hndflmy7zxbsk0z74b7hpp5d9x6kijzx9vikdvcfw6z5cx")))

(define-public crate-waterfall-0.3.0 (c (n "waterfall") (v "0.3.0") (d (list (d (n "heatmap") (r "^0.3.0") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "1m5wcrwqqgmjx89g20nfzigrz8k48p58y6i6ikkjshbizp4ywkpf")))

(define-public crate-waterfall-0.4.0 (c (n "waterfall") (v "0.4.0") (d (list (d (n "heatmap") (r "^0.4.0") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "0jjad2hw2pbbn6w5d7ahwxhpgljnmycqrkw5ncp84y3jpinkz15l")))

(define-public crate-waterfall-0.5.0 (c (n "waterfall") (v "0.5.0") (d (list (d (n "heatmap") (r "^0.5.0") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "0p4hprx9s93z87i0ylm4xcxnaflj0bd6ic1dixlfca9cp7dlb2qq")))

(define-public crate-waterfall-0.6.0 (c (n "waterfall") (v "0.6.0") (d (list (d (n "heatmap") (r "^0.6.0") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "1wk6sa2a417kggw5afjm2r85g4lhjd5nq0xf4ql38qh24lcfa9za")))

(define-public crate-waterfall-0.6.1 (c (n "waterfall") (v "0.6.1") (d (list (d (n "heatmap") (r "^0.6.1") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "178nbglfb9ky15ms61hv2psiy36926a61ypkdsv8kcy8ix0sw6lp")))

(define-public crate-waterfall-0.6.2 (c (n "waterfall") (v "0.6.2") (d (list (d (n "heatmap") (r "^0.6.2") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "179p95c5w0mkyhhrh4wi9243wzkgd70siqgqdlvpgl05z13rgsvq")))

(define-public crate-waterfall-0.6.3 (c (n "waterfall") (v "0.6.3") (d (list (d (n "heatmap") (r "^0.6.2") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "17jmiqddf471n1n2v4hyzcaf4r4f7mfqzmnkydb3xqc0cdydzkc8")))

(define-public crate-waterfall-0.6.4 (c (n "waterfall") (v "0.6.4") (d (list (d (n "heatmap") (r "^0.6.2") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "04xc8b5vhclnk202r4z2hrdiil125mw9y6rsf95xvn1yj94b2x96")))

(define-public crate-waterfall-0.6.5 (c (n "waterfall") (v "0.6.5") (d (list (d (n "heatmap") (r "^0.6.2") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "0c7mjq8pja78f5fnr4c0wpv79kg760ayilnib0hbz9zzdbdcz1wd")))

(define-public crate-waterfall-0.6.6 (c (n "waterfall") (v "0.6.6") (d (list (d (n "heatmap") (r "^0.6.2") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "lodepng") (r "^0.5.3") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "095faqxcd68bl6m9i1nyz7gqav68cbi7x82mcszcakffvg7hmf5z")))

(define-public crate-waterfall-0.7.0 (c (n "waterfall") (v "0.7.0") (d (list (d (n "heatmap") (r "^0.6.2") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "png") (r "^0.7.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "0v82f9x02hdw0hy7iwgpabvy09c06cc7sdi8l6fr8rw1imgd6r6h")))

(define-public crate-waterfall-0.7.1 (c (n "waterfall") (v "0.7.1") (d (list (d (n "heatmap") (r "^0.6.2") (d #t) (k 0)) (d (n "hsl") (r "^0.1.1") (d #t) (k 0)) (d (n "png") (r "^0.7.0") (d #t) (k 0)) (d (n "rusttype") (r "^0.1.2") (d #t) (k 0)))) (h "0a870kbg894x6n2zcrzalxgk0v3jvb7iml6640l1a3djzqcjmzfx")))

(define-public crate-waterfall-0.8.0 (c (n "waterfall") (v "0.8.0") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "dejavu") (r "^2.37.0") (d #t) (k 0)) (d (n "heatmap") (r "^0.7.0") (d #t) (k 0)) (d (n "histogram") (r "^0.7.0") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "1zpl5r6mllnp5q6lrxflszm43s5hbfbw5d0h7gc12lm9zghzq0n5")))

(define-public crate-waterfall-0.8.1 (c (n "waterfall") (v "0.8.1") (d (list (d (n "clocksource") (r "^0.6.0") (d #t) (k 0)) (d (n "dejavu") (r "^2.37.0") (d #t) (k 0)) (d (n "heatmap") (r "^0.7.1") (d #t) (k 0)) (d (n "histogram") (r "^0.7.1") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 2)))) (h "14j2yng6phdnh4gwfp8gkq45j3vwwhavi29ddbjp3wdkm24r5yjj")))

