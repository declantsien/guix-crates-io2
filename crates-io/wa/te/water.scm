(define-module (crates-io wa te water) #:use-module (crates-io))

(define-public crate-water-0.0.1 (c (n "water") (v "0.0.1") (h "1zkd7qkrnir921x9wg0qvri852vn1j1gpfa1skmri1i6s4fzhq6d") (y #t)))

(define-public crate-water-0.0.2 (c (n "water") (v "0.0.2") (h "1xcpvn2fym5m9b8dvd71yqagiqjhamcxawc60qr734dcp3yzfnai") (y #t)))

(define-public crate-water-0.1.3 (c (n "water") (v "0.1.3") (h "10vrxi47ngagmkd5c5406cgqc89adwmbh8n4bjd0xpkzaw4bsbzh") (y #t)))

(define-public crate-water-0.1.5 (c (n "water") (v "0.1.5") (h "0ifn9imc4m9nqpxyx9dvppr0z87dqjm83f9rdd605gq57l1hwm8y") (y #t)))

(define-public crate-water-0.1.6-alpha (c (n "water") (v "0.1.6-alpha") (h "166hnaypjl0x0gzcpyafx16fcpcmy4frs0c5r1l3fafi0l6z4kzw") (y #t)))

(define-public crate-water-0.1.7-alpha (c (n "water") (v "0.1.7-alpha") (h "0dw3x20kd5xq00awkhsnszydw8n6xg2l3xybp465qrnqs1gyx6ym") (y #t)))

(define-public crate-water-0.2.8-alpha (c (n "water") (v "0.2.8-alpha") (h "00pppdvnh1b3zyffy5h8bxgxicb0c9hcifcz6fnbs401dsbgvsjj") (y #t)))

(define-public crate-water-0.3.9-alpha (c (n "water") (v "0.3.9-alpha") (h "1bagd9i8rh8bhbs4n06cz9kpgl8rs0qaabv5k2s3hax3shyj47dv") (y #t)))

(define-public crate-water-0.3.10-alpha (c (n "water") (v "0.3.10-alpha") (h "0jv0haql3k2648mr8jl17hwks289b0hvanbs9n37iw04gsah39h2") (y #t)))

(define-public crate-water-0.3.11-alpha (c (n "water") (v "0.3.11-alpha") (h "08wia11wc1kpajsn0ab6hb5f3ak24qkf92zk2wk9g3jcfqk8k7hy") (y #t)))

(define-public crate-water-0.4.12-alpha (c (n "water") (v "0.4.12-alpha") (h "1kg68krb9wdw8y1lxpxkhr2dm2q9v7ygv8kpnvr2s7sxhln8kdr8") (y #t)))

(define-public crate-water-0.4.13-alpha (c (n "water") (v "0.4.13-alpha") (h "1lsrcmiwbd2372r32d15a2msxi2hzr4li5fs9qfvvgfhl7ygsfb1") (y #t)))

(define-public crate-water-0.6.18-alpha (c (n "water") (v "0.6.18-alpha") (h "1r8vdd0iikpr224dhx1g3qy5ji1daq6qdr4fhdrcxjw8zznp14hz") (y #t)))

(define-public crate-water-0.6.19-alpha (c (n "water") (v "0.6.19-alpha") (h "1vvmp8i1c5n710v0dabvs8lp5qd7f0jma0dh7lk8qw8hs3yzf3hp") (y #t)))

(define-public crate-water-0.6.20-alpha (c (n "water") (v "0.6.20-alpha") (h "1gi9q49bx5p931qd9p2mxymf33ci1a40q4g9bdf3z467910ydsvd") (y #t)))

(define-public crate-water-0.6.21-alpha (c (n "water") (v "0.6.21-alpha") (h "1synlf2zj7gavpp4g9fl6mbv2zc4ypz2dardb18zm862rpj311y6")))

(define-public crate-water-0.7.22-alpha (c (n "water") (v "0.7.22-alpha") (h "1gj5jyyjg6p4c1g18gxrizwxdnbfayfqwrs3k1knb3z2zbk87z6j")))

(define-public crate-water-0.8.23-alpha (c (n "water") (v "0.8.23-alpha") (h "00j6ym99s95lrmv8jxqqz2j1ii0yqy0k01pmp0jsa9m4i2lgkb75")))

(define-public crate-water-0.10.25-alpha (c (n "water") (v "0.10.25-alpha") (h "0cvx8m859dgd9f51ky52l9zdrjwkc489jd6mr4a7yaqibab07633")))

(define-public crate-water-0.10.26-alpha (c (n "water") (v "0.10.26-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "03b1l0hr5idykpf1jwny2a7cwb4grdbh0fxmhn0217cpb2gpfnmh")))

(define-public crate-water-0.11.27-alpha (c (n "water") (v "0.11.27-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1zsgd810xil4nqawx69ix3hz1yzixfga3swpimh5dwxald0wrz1r")))

(define-public crate-water-0.12.30-alpha (c (n "water") (v "0.12.30-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "05f6xc8c1dzabrh8f8qvs0wh042bmzq8wrim707q2cknsqa65hgk")))

(define-public crate-water-0.13.31-alpha (c (n "water") (v "0.13.31-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1gag6im8vj0c81zk9ljmgncw5lszizswq8w6ggwhpmhdy87is37r")))

(define-public crate-water-0.14.33-alpha (c (n "water") (v "0.14.33-alpha") (d (list (d (n "time") (r "^0.1.5") (d #t) (k 0)))) (h "11nzk4m5ylwy8n39yf18f3s737gx6srzfvnfzq66bs34f1ym6z9a")))

(define-public crate-water-0.14.34-alpha (c (n "water") (v "0.14.34-alpha") (d (list (d (n "time") (r "^0.1.5") (d #t) (k 0)))) (h "1p3d611qcdj22bz0s21lqz7swzs5ablrjci8sjbr1qsg271rkdyj")))

(define-public crate-water-0.14.38-alpha (c (n "water") (v "0.14.38-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0w93hfb04sjfgjg78smlydldrb57m4glysvl7yazyqnw664n95x9")))

(define-public crate-water-0.14.39-alpha (c (n "water") (v "0.14.39-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "14ppbwj35q8l33wgs6chv7kwxd71w6ixda0jlk3iv2h6s9c56ini")))

(define-public crate-water-0.14.41-alpha (c (n "water") (v "0.14.41-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1y0d3xxniw37bzh0j9l57sp2z8nwy0hzaaz55nkkrilq6v3n2gfk")))

(define-public crate-water-0.15.42-alpha (c (n "water") (v "0.15.42-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1szdpx13wmvwsgdb6wsqngjxxb99d6zsraixhsg3yv3234rfvrcr")))

(define-public crate-water-0.16.43-alpha (c (n "water") (v "0.16.43-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "0sddlhkd9xrls8rhkci6a8hyqwxzv7zxgmr1y7nmqn61vlsn7cqb")))

(define-public crate-water-0.16.44-alpha (c (n "water") (v "0.16.44-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1swzpjq78ki0k4jzzp0gfvd3n0vzhp984g8j3mcn2qinh0r83vl4")))

(define-public crate-water-0.16.45-alpha (c (n "water") (v "0.16.45-alpha") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "05g74s063npdpylr1g8y2qlwlbm437wbbrl6vviq9xzjr1zhz0aq")))

