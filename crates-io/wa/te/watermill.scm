(define-module (crates-io wa te watermill) #:use-module (crates-io))

(define-public crate-watermill-0.1.0 (c (n "watermill") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1prq0cijv1kywdngcsx1sjqa4kljvm3fddmqz763ps7rwafbk5rl")))

(define-public crate-watermill-0.1.1 (c (n "watermill") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "ordered-float") (r "^3.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00n5rfni34jknp4p0vc5ycdb2497ycml2iliyczi0q4i57l98mdq")))

