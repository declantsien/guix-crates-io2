(define-module (crates-io wa te waterbear) #:use-module (crates-io))

(define-public crate-waterbear-0.12.0 (c (n "waterbear") (v "0.12.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "termcolor") (r "^1") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.2.0") (d #t) (k 0)) (d (n "unicode_categories") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)) (d (n "waterbear-instruction-derive") (r "^0.1") (d #t) (k 0)))) (h "1ra8ba5gijv1rw7hj2a9fc647384712cx3ygs22hdh2p17qf0vf7")))

