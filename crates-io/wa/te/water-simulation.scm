(define-module (crates-io wa te water-simulation) #:use-module (crates-io))

(define-public crate-water-simulation-0.1.1 (c (n "water-simulation") (v "0.1.1") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "06d0byw55rwwvh14fwv94m2wz1k8x7i1sfpgdjin31f66gyarcc0")))

(define-public crate-water-simulation-0.1.2 (c (n "water-simulation") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "log") (r "^0.4.19") (d #t) (k 0)))) (h "0cdssnlp1nc01b27bjr2azarrcz6wc515h67h5ifr4ylc3vvap62")))

