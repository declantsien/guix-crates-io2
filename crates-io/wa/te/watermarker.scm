(define-module (crates-io wa te watermarker) #:use-module (crates-io))

(define-public crate-watermarker-0.3.0 (c (n "watermarker") (v "0.3.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "image") (r "^0.24.6") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.29.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sarr5nds15b512g45qpv2q40dk3wyv12fln4sqp1j5lx751yxnw")))

