(define-module (crates-io wa te waterbear-instruction-derive) #:use-module (crates-io))

(define-public crate-waterbear-instruction-derive-0.1.0 (c (n "waterbear-instruction-derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^0.11") (d #t) (k 0)))) (h "00kkpz5gm43i8kgk15g61ysxry8mqsappy61w3wrd39m3ss2knf9")))

