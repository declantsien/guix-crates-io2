(define-module (crates-io wa te waterlogged) #:use-module (crates-io))

(define-public crate-waterlogged-0.1.0 (c (n "waterlogged") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.41") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "tide") (r "^0.15.0") (d #t) (k 0)))) (h "08yw3sz0sjln6f84qkm6cklh16pzcvhcbgjn2746pl5sz2d07xhz")))

