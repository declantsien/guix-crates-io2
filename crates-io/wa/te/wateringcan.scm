(define-module (crates-io wa te wateringcan) #:use-module (crates-io))

(define-public crate-wateringcan-0.1.0 (c (n "wateringcan") (v "0.1.0") (h "0m91w205xa6f6dx8sksm28hhwxf9dmm0gc402s16s3jhfxkgyyfp")))

(define-public crate-wateringcan-0.1.1 (c (n "wateringcan") (v "0.1.1") (d (list (d (n "seed") (r "^0.8") (d #t) (k 0)))) (h "19cgdw2dbfjx21gbhh0z7ppbcyz79n4xb7v7q6i30ypjj9nbwzg3")))

