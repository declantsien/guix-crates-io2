(define-module (crates-io wa te water_mon_app) #:use-module (crates-io))

(define-public crate-water_mon_app-1.0.0 (c (n "water_mon_app") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "local_ipaddress") (r "^0.1.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.10") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.10") (f (quote ("serve"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "1wh4paibzzlbi7yy431inp7zqzvvvhk9qagcy8in70yi03yanczv")))

(define-public crate-water_mon_app-1.0.1 (c (n "water_mon_app") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "local_ipaddress") (r "^0.1.3") (d #t) (k 0)) (d (n "rocket") (r "^0.4.10") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.10") (f (quote ("serve"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)) (d (n "serialport") (r "^3.3.0") (d #t) (k 0)))) (h "1mqk8nv588rrnylfxq1bfykm57jbn769hnpzf3gw0bx3z8vn1d14")))

