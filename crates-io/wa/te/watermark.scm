(define-module (crates-io wa te watermark) #:use-module (crates-io))

(define-public crate-watermark-1.0.0 (c (n "watermark") (v "1.0.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "0y1g6sfvpc1jqi3zd3yw9fzylam4c7sj348chci63v9yq9ss984q")))

(define-public crate-watermark-1.0.1 (c (n "watermark") (v "1.0.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "020ya4157f03nhlsjywxi9dhg9gsaip22rdd8lxlrl2qvxvkxmkk")))

(define-public crate-watermark-1.0.2 (c (n "watermark") (v "1.0.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "1hbwpdqvlp9q3jmmrpgp3z5icfwwcs4xaan2033n1w0z7wmrnmip")))

(define-public crate-watermark-1.1.0 (c (n "watermark") (v "1.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "14cpxvvjhh2w1q7f1jj24v2829ijb99qamndgqxx08agjal8l9bd")))

