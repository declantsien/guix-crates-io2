(define-module (crates-io wa te waterrower) #:use-module (crates-io))

(define-public crate-waterrower-0.1.0 (c (n "waterrower") (v "0.1.0") (d (list (d (n "bufstream") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.7") (d #t) (k 0)) (d (n "serial") (r "^0.3") (d #t) (k 0)))) (h "09dsqakzfzij3b99lja4r7mlf1lypgpjrdd2z6va79vk5r7rh3dg")))

