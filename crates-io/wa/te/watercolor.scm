(define-module (crates-io wa te watercolor) #:use-module (crates-io))

(define-public crate-watercolor-0.1.0 (c (n "watercolor") (v "0.1.0") (h "189bzmh8x2sx7k94w94bj1jqyjdfs1kjqccywxh5wc3wm7cpn3vx")))

(define-public crate-watercolor-0.2.0 (c (n "watercolor") (v "0.2.0") (h "01p9hiam2j21s8x125fqnxfpyrpxbkh9ys9mv5aa3a0x01r4sp4m")))

