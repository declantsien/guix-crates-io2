(define-module (crates-io wa ti watime) #:use-module (crates-io))

(define-public crate-watime-0.1.0 (c (n "watime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x805d5ly3hqqm3ys7mwdbdk9y9x2xzmcc407l38kj4r3dvdywl7")))

(define-public crate-watime-0.2.0 (c (n "watime") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "126iiqpsws8p9gjp8ykcjp314n310g8k2rk4djp80qg60bdsip1q")))

(define-public crate-watime-0.2.1 (c (n "watime") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rlyh93r2acpi7z6aqy9vj5miym3rkcga3m43kqrkhag4dwshiv6")))

(define-public crate-watime-0.2.2 (c (n "watime") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "19w6bjbdyppn9vhvl741hsb51bdpdqzxlkb91h29m9q7zy3fcwh5")))

(define-public crate-watime-0.2.3 (c (n "watime") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10cqxdx1qvmqx0zwarvwr6x9bl4anx6a381x52nhbw9qbh538iql")))

