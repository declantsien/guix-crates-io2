(define-module (crates-io wa rn warned) #:use-module (crates-io))

(define-public crate-warned-0.1.0 (c (n "warned") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0ng9npfv7dxyy75hh14bc7z2pva6m9ygkg97s1z7vp4g78y7ic9b")))

(define-public crate-warned-0.1.1 (c (n "warned") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "19g3vpnjs9wp954r4jkz7i3w2rrhcwarz0fhac17rxnip805abd3")))

