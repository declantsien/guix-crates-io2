(define-module (crates-io wa rn warn) #:use-module (crates-io))

(define-public crate-warn-0.1.0 (c (n "warn") (v "0.1.0") (d (list (d (n "log") (r "^0.3.0") (d #t) (k 0)))) (h "1dyqb0r59yp16xh29czwflbpi4ibq3z4wlxdl0kgq3489n3l7ksq")))

(define-public crate-warn-0.1.1 (c (n "warn") (v "0.1.1") (d (list (d (n "log") (r "^0.3.0") (d #t) (k 0)))) (h "1v9irifbb8lp0ab2a3jwyfyszh9r282d54i4b9xfjf485k3nj4kx")))

(define-public crate-warn-0.2.0 (c (n "warn") (v "0.2.0") (d (list (d (n "log") (r "^0.3.0") (d #t) (k 0)))) (h "05wy0kp48qrvww9fhgphf0gsn1a3viqgz20aqqmq2cvk8s60i3fk")))

(define-public crate-warn-0.2.1 (c (n "warn") (v "0.2.1") (d (list (d (n "log") (r "^0.3.0") (d #t) (k 0)))) (h "0qwxd98swwg382xdzbbm4py9ssx7jinkv11c16aj7908m18ss2wi")))

(define-public crate-warn-0.2.2 (c (n "warn") (v "0.2.2") (d (list (d (n "log") (r "^0.3.0") (d #t) (k 0)))) (h "1jr2xnv9sziyiaypcs36wn10601480h7hvg9wkigvrsh503kh9jz")))

