(define-module (crates-io wa kf wakflo-schema) #:use-module (crates-io))

(define-public crate-wakflo-schema-0.1.0 (c (n "wakflo-schema") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "0mxghd28m1wckzzybnkk9psqij35rmfmka4hv66nhfsf7ys28wja")))

(define-public crate-wakflo-schema-0.1.1 (c (n "wakflo-schema") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1w1fjb91w8x2gszzllfrnfx1rrna87jx27p9qg4dq1wm2wgyv78b")))

