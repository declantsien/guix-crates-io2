(define-module (crates-io wa kf wakflo-funx) #:use-module (crates-io))

(define-public crate-wakflo-funx-0.0.1 (c (n "wakflo-funx") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "js-sandbox") (r "^0.2.0-rc.1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j0mmq61n36hx4rz38fjqqvrqlqrydxbisny4h7zziccysvnw4zj") (f (quote (("js" "js-sandbox") ("default" "js"))))))

