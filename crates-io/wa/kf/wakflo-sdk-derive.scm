(define-module (crates-io wa kf wakflo-sdk-derive) #:use-module (crates-io))

(define-public crate-wakflo-sdk-derive-0.0.1 (c (n "wakflo-sdk-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0sb6394m59g54msnxmh7lp8p7amcn67k3qp4df5163pw4n6jrnp3")))

(define-public crate-wakflo-sdk-derive-0.1.1 (c (n "wakflo-sdk-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "wakflo-common") (r "=0.1.1") (d #t) (k 0)) (d (n "wakflo-core") (r "=0.1.1") (d #t) (k 0)))) (h "0y1n57hqrwadf9j21f4cmwk7njqv1fs4hrnll10hxhxsb0m1idgf")))

(define-public crate-wakflo-sdk-derive-0.1.2 (c (n "wakflo-sdk-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "wakflo-common") (r "=0.1.2") (d #t) (k 0)) (d (n "wakflo-core") (r "=0.1.2") (d #t) (k 0)))) (h "0pw5r0dmy2fvwbx8pi45m3rwrzkg0bf2l8g4nsr4hqlwngxdxc75")))

(define-public crate-wakflo-sdk-derive-0.1.3 (c (n "wakflo-sdk-derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "wakflo-common") (r "=0.1.3") (d #t) (k 0)) (d (n "wakflo-core") (r "=0.1.3") (d #t) (k 0)))) (h "0d2nfa4a19sdjmwv91bwk7mb0cmmdmd8v0490mwhqj6g8dzw39b3")))

