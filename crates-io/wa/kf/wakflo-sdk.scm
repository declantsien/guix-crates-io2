(define-module (crates-io wa kf wakflo-sdk) #:use-module (crates-io))

(define-public crate-wakflo-sdk-0.0.1 (c (n "wakflo-sdk") (v "0.0.1") (d (list (d (n "wakflo-core") (r "^0.0.1") (d #t) (k 0)) (d (n "wakflo-sdk-derive") (r "^0.0.1") (d #t) (k 0)))) (h "0jhjg80272p51ak45xp99pi10l8jnfm9cwxw8gbvd382x6w19wa8")))

(define-public crate-wakflo-sdk-0.1.1 (c (n "wakflo-sdk") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wakflo-common") (r "=0.1.1") (d #t) (k 0)) (d (n "wakflo-core") (r "=0.1.1") (d #t) (k 0)) (d (n "wakflo-sdk-derive") (r "=0.1.1") (d #t) (k 0)))) (h "0rch8g8x7im87glymn59fgrjvjyf6vf7rdq0a1388ag2h04xqmck")))

(define-public crate-wakflo-sdk-0.1.2 (c (n "wakflo-sdk") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wakflo-common") (r "=0.1.2") (d #t) (k 0)) (d (n "wakflo-core") (r "=0.1.2") (d #t) (k 0)) (d (n "wakflo-sdk-derive") (r "=0.1.2") (d #t) (k 0)))) (h "1w57n4bvcrip8qf606imzaslir9vy5blj5h4v1cfpsppmbs3jd5m")))

(define-public crate-wakflo-sdk-0.1.3 (c (n "wakflo-sdk") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wakflo-common") (r "=0.1.3") (d #t) (k 0)) (d (n "wakflo-core") (r "=0.1.3") (d #t) (k 0)) (d (n "wakflo-sdk-derive") (r "=0.1.3") (d #t) (k 0)))) (h "017xh7p6i153gqf6f5ghldfijv4r410f5v64f0jd0xjgg09g83fi")))

