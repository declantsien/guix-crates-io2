(define-module (crates-io wa rt warts) #:use-module (crates-io))

(define-public crate-warts-0.1.0 (c (n "warts") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "07101c1fib40xd1vmxhxh01fa0rafkj9jrpdnryd7kixx1ml30pa")))

(define-public crate-warts-0.1.1 (c (n "warts") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "0vw3y0j51w93gp93mjknj387chc516mkazncyqpvi1k5dc0g106f")))

(define-public crate-warts-0.1.2 (c (n "warts") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "0sxfvz17pw16h5y1lldbcqdzy6mmfk62b2zgz8rs2mvjsbm0xl8a")))

(define-public crate-warts-0.2.0 (c (n "warts") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "1kxwvpf1gln01mm5js6larr0cw1v93vifihnnp0zx8p3kp3lg89r")))

(define-public crate-warts-0.2.1 (c (n "warts") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "0lb4awnfghy33j9jc8ra703hkspclsixj3q2xfnx88fyf1m9xnhy")))

(define-public crate-warts-0.2.2 (c (n "warts") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "1lzfwfp1z75c1yj3hgyg8y9zz1jkd47fj2llf7x87pj6q12phm8r")))

(define-public crate-warts-0.3.0 (c (n "warts") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "0ig4hi4qjrkl98f14s01228qhp708mgwvisqp9yx97rxybzi9aan")))

(define-public crate-warts-0.3.1 (c (n "warts") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "0dprjrqdd27qz20r26d1f7kchhn6r8729ffm4l0bi8gz2654y0jl")))

(define-public crate-warts-0.3.2 (c (n "warts") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "17c64n1ld908md9mn44pxp5f4n2gghvv91gcq6xjjli3q0w8yp77")))

(define-public crate-warts-0.3.3 (c (n "warts") (v "0.3.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.12") (d #t) (k 0)))) (h "0clm90mnnkg25w0a5bdmpqxsmds2friykx0jf6rws3zyk9xwr9xb")))

(define-public crate-warts-0.3.4 (c (n "warts") (v "0.3.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.15") (d #t) (k 0)))) (h "0abnzz88pdzy3lncc1nrfrrqc9f3cwbb8g7g6cyz8i1fninylh5j")))

(define-public crate-warts-0.4.0 (c (n "warts") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "deku") (r "^0.16") (d #t) (k 0)))) (h "15wj9hckiknpqgh26vhzk0ksfxvm6x8g30959d79lbijfa213z48")))

