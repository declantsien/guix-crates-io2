(define-module (crates-io wa i- wai-bindgen-gen-markdown) #:use-module (crates-io))

(define-public crate-wai-bindgen-gen-markdown-0.2.0-rc.1 (c (n "wai-bindgen-gen-markdown") (v "0.2.0-rc.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "0px5dccizikyvagxqrs5rcq8hnyfwg0kh8dvdd4rmbk94vkymi5r") (r "1.64")))

(define-public crate-wai-bindgen-gen-markdown-0.2.0 (c (n "wai-bindgen-gen-markdown") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0") (d #t) (k 0)))) (h "068fc58pa7dwkprngvw4j8x4fx1q7q1gnmfxcnm0rkn6qzl8fr17") (r "1.64")))

(define-public crate-wai-bindgen-gen-markdown-0.2.1 (c (n "wai-bindgen-gen-markdown") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.1") (d #t) (k 0)))) (h "1rf9768xqc6667dxy7hw7by3aj8gyldlw2qk78bj4j2795k0gxj2") (r "1.64")))

(define-public crate-wai-bindgen-gen-markdown-0.2.2 (c (n "wai-bindgen-gen-markdown") (v "0.2.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.2") (d #t) (k 0)))) (h "0g63y5h2xp2ky6yyyflldrcm0avc311valm4albsqhn275szm055") (r "1.64")))

(define-public crate-wai-bindgen-gen-markdown-0.2.3 (c (n "wai-bindgen-gen-markdown") (v "0.2.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.3") (d #t) (k 0)))) (h "0gl4zx4ckrjlk9iaarl9nrp9ai3jja59saydpmydh975kdvi931g") (r "1.64")))

