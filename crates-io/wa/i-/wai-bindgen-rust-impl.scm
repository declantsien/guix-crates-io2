(define-module (crates-io wa i- wai-bindgen-rust-impl) #:use-module (crates-io))

(define-public crate-wai-bindgen-rust-impl-0.2.0-rc.1 (c (n "wai-bindgen-rust-impl") (v "0.2.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "wai-bindgen-gen-rust-wasm") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "0l3bf8925fiy939m9jaqp991aq5yrknan2c30zxsvw61yk3jyhqf") (r "1.64")))

(define-public crate-wai-bindgen-rust-impl-0.2.0 (c (n "wai-bindgen-rust-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-rust-wasm") (r "^0.2.0") (d #t) (k 0)))) (h "1v44z31vl2qwzmlign60ypjc4f4k925k920xa3097x90cq6mha5b") (r "1.64")))

(define-public crate-wai-bindgen-rust-impl-0.2.1 (c (n "wai-bindgen-rust-impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.1") (d #t) (k 0)) (d (n "wai-bindgen-gen-rust-wasm") (r "^0.2.1") (d #t) (k 0)))) (h "1fzvyq2p2psds6nn81mbagy8hr73zd0idd6rzkhsadwq6c3w8dpb") (r "1.64")))

(define-public crate-wai-bindgen-rust-impl-0.2.2 (c (n "wai-bindgen-rust-impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.2") (d #t) (k 0)) (d (n "wai-bindgen-gen-rust-wasm") (r "^0.2.2") (d #t) (k 0)))) (h "03999yvnhybg91wzk4psvv5savcafdmpwv0ak8pg0g0xl2yh0g3d") (r "1.64")))

(define-public crate-wai-bindgen-rust-impl-0.2.3 (c (n "wai-bindgen-rust-impl") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.3") (d #t) (k 0)) (d (n "wai-bindgen-gen-rust-wasm") (r "^0.2.3") (d #t) (k 0)))) (h "197ydqvia8zyd79s28mml9dw0396xzkj7qd34n2dwih22z0vbvmx") (r "1.64")))

