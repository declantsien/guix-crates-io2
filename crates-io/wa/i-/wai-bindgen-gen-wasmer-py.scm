(define-module (crates-io wa i- wai-bindgen-gen-wasmer-py) #:use-module (crates-io))

(define-public crate-wai-bindgen-gen-wasmer-py-0.2.0-rc.1 (c (n "wai-bindgen-gen-wasmer-py") (v "0.2.0-rc.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "09wb85xmcnfzb9dckng0y4vxzpzy9ldznjccz1p7l98bjgqlzfdr") (r "1.64")))

(define-public crate-wai-bindgen-gen-wasmer-py-0.2.0 (c (n "wai-bindgen-gen-wasmer-py") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0") (d #t) (k 0)))) (h "1gj7hfvijm6cr290b40zsbr9bnmfy9q3rd7kf835d5690ssjmhb6") (r "1.64")))

(define-public crate-wai-bindgen-gen-wasmer-py-0.2.1 (c (n "wai-bindgen-gen-wasmer-py") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.1") (d #t) (k 0)))) (h "18m61ahb67pwh9d1nqibxx7zmpzwpjn9s1clkk87wrrvlfpi1bd3") (r "1.64")))

(define-public crate-wai-bindgen-gen-wasmer-py-0.2.2 (c (n "wai-bindgen-gen-wasmer-py") (v "0.2.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.2") (d #t) (k 0)))) (h "1rq0q83wiadz8n3q0pqcqb0wlk691h7rjia283rpqpbjzwq2a743") (r "1.64")))

(define-public crate-wai-bindgen-gen-wasmer-py-0.2.3 (c (n "wai-bindgen-gen-wasmer-py") (v "0.2.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.3") (d #t) (k 0)))) (h "1qcilm35ghymm8q5172by7msxdx9skh0cp5bxim6lganp1b20545") (r "1.64")))

