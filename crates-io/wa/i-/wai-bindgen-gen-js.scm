(define-module (crates-io wa i- wai-bindgen-gen-js) #:use-module (crates-io))

(define-public crate-wai-bindgen-gen-js-0.2.0-rc.1 (c (n "wai-bindgen-gen-js") (v "0.2.0-rc.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "00w1qnfkzz2gxbzr0gb75xdrc883i2ww4rx6gilhi6xn148i6ap6") (r "1.64")))

(define-public crate-wai-bindgen-gen-js-0.2.0 (c (n "wai-bindgen-gen-js") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0") (d #t) (k 0)))) (h "07icrjvsfagd5k3cbf1k7mm4ffxj2ljp48vfpbiviivy3sbpwp06") (r "1.64")))

(define-public crate-wai-bindgen-gen-js-0.2.1 (c (n "wai-bindgen-gen-js") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.1") (d #t) (k 0)))) (h "0rks6war0k8yphkh8rqsff57fq9ih9yyq8qbnvb4m85b6jin6mby") (r "1.64")))

(define-public crate-wai-bindgen-gen-js-0.2.2 (c (n "wai-bindgen-gen-js") (v "0.2.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.2") (d #t) (k 0)))) (h "0l5srjs3q8j5yc3cy0j71gz23l56qksxbqb2viqmg4xld2h1j3zc") (r "1.64")))

(define-public crate-wai-bindgen-gen-js-0.2.3 (c (n "wai-bindgen-gen-js") (v "0.2.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.3") (d #t) (k 0)))) (h "0cixnv9fa223hx7p56lw2ljs1ra2xhnnnvy372fgrlpd5qqk5hqm") (r "1.64")))

