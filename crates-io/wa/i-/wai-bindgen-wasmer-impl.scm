(define-module (crates-io wa i- wai-bindgen-wasmer-impl) #:use-module (crates-io))

(define-public crate-wai-bindgen-wasmer-impl-0.2.0-rc.1 (c (n "wai-bindgen-wasmer-impl") (v "0.2.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "wai-bindgen-gen-wasmer") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "1f96wsph9psbfyhlij23h7idx1r32pz5z3lfkhpwf9pdzkfrxi0a") (f (quote (("tracing") ("async")))) (r "1.64")))

(define-public crate-wai-bindgen-wasmer-impl-0.2.0 (c (n "wai-bindgen-wasmer-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-wasmer") (r "^0.2.0") (d #t) (k 0)))) (h "0jcq3rr9hyrpmffrisfk7hyiyfib2hhd92rpw7gyilsqm65w0za0") (f (quote (("tracing") ("async")))) (r "1.64")))

(define-public crate-wai-bindgen-wasmer-impl-0.2.1 (c (n "wai-bindgen-wasmer-impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.1") (d #t) (k 0)) (d (n "wai-bindgen-gen-wasmer") (r "^0.2.1") (d #t) (k 0)))) (h "1i6a5vgl748pdv4ayhmx6m2bw0s1w54jak8z5bcdaknmhdcpkdc9") (f (quote (("tracing") ("async")))) (r "1.64")))

(define-public crate-wai-bindgen-wasmer-impl-0.2.2 (c (n "wai-bindgen-wasmer-impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.2") (d #t) (k 0)) (d (n "wai-bindgen-gen-wasmer") (r "^0.2.2") (d #t) (k 0)))) (h "0s7zv51d4y93hmfppqms83bqhgsf0gzfl9nxbhc0h5qdh9r3mbzs") (f (quote (("tracing") ("async")))) (r "1.64")))

(define-public crate-wai-bindgen-wasmer-impl-0.2.3 (c (n "wai-bindgen-wasmer-impl") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.3") (d #t) (k 0)) (d (n "wai-bindgen-gen-wasmer") (r "^0.2.3") (d #t) (k 0)))) (h "0fl6vk406j22lbfsa8niwnm33jy6v8klxbavz0xhxpfli3nqhd2b") (f (quote (("tracing") ("async")))) (r "1.64")))

