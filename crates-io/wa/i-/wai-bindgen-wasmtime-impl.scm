(define-module (crates-io wa i- wai-bindgen-wasmtime-impl) #:use-module (crates-io))

(define-public crate-wai-bindgen-wasmtime-impl-0.2.0-rc.1 (c (n "wai-bindgen-wasmtime-impl") (v "0.2.0-rc.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "wai-bindgen-gen-wasmtime") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "0x0yah3b825k8lkfkcp2sf35p6pkgy1861jjv91dc0hbrx0rcnbz") (f (quote (("tracing") ("async")))) (r "1.64")))

(define-public crate-wai-bindgen-wasmtime-impl-0.2.0 (c (n "wai-bindgen-wasmtime-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-wasmtime") (r "^0.2.0") (d #t) (k 0)))) (h "00wshb18d9abqnjqz8hyc9bkid8gwf6lnihdw3gds491v4zfc870") (f (quote (("tracing") ("async")))) (r "1.64")))

(define-public crate-wai-bindgen-wasmtime-impl-0.2.1 (c (n "wai-bindgen-wasmtime-impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.1") (d #t) (k 0)) (d (n "wai-bindgen-gen-wasmtime") (r "^0.2.1") (d #t) (k 0)))) (h "0a6iwxwkj8gnjcgm7570xmlnb2wy2421nhxa8bjy08dpd2zxs66l") (f (quote (("tracing") ("async")))) (r "1.64")))

(define-public crate-wai-bindgen-wasmtime-impl-0.2.2 (c (n "wai-bindgen-wasmtime-impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.2") (d #t) (k 0)) (d (n "wai-bindgen-gen-wasmtime") (r "^0.2.2") (d #t) (k 0)))) (h "0a60p5mrhvnzhlqpbibxfd85h5468v9q799hjfi71ps2rhnvx5xd") (f (quote (("tracing") ("async")))) (r "1.64")))

(define-public crate-wai-bindgen-wasmtime-impl-0.2.3 (c (n "wai-bindgen-wasmtime-impl") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.3") (d #t) (k 0)) (d (n "wai-bindgen-gen-wasmtime") (r "^0.2.3") (d #t) (k 0)))) (h "1c68wzalv0d4kmbcrhxs2952zqmlhhgmizlrgnfln6vri40zmc44") (f (quote (("tracing") ("async")))) (r "1.64")))

