(define-module (crates-io wa i- wai-bindgen-gen-rust-wasm) #:use-module (crates-io))

(define-public crate-wai-bindgen-gen-rust-wasm-0.2.0-rc.1 (c (n "wai-bindgen-gen-rust-wasm") (v "0.2.0-rc.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0-rc.1") (d #t) (k 0)) (d (n "wai-bindgen-gen-rust") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "1w6a1b4rrkmmdk88p9fvrbgnd43h0vcabqbsz6sfnvij3l23zagr") (r "1.64")))

(define-public crate-wai-bindgen-gen-rust-wasm-0.2.0 (c (n "wai-bindgen-gen-rust-wasm") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wai-bindgen-gen-rust") (r "^0.2.0") (d #t) (k 0)))) (h "00j4mib1sghm0b25zqnid6djmin7fy9q5vpgw2a3b8rgbwlp4g7k") (r "1.64")))

(define-public crate-wai-bindgen-gen-rust-wasm-0.2.1 (c (n "wai-bindgen-gen-rust-wasm") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.1") (d #t) (k 0)) (d (n "wai-bindgen-gen-rust") (r "^0.2.1") (d #t) (k 0)))) (h "1xyjhljzbrx72arcfilkngw0kg8in44s5kkrx275gl0ibpqvyzvm") (r "1.64")))

(define-public crate-wai-bindgen-gen-rust-wasm-0.2.2 (c (n "wai-bindgen-gen-rust-wasm") (v "0.2.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.2") (d #t) (k 0)) (d (n "wai-bindgen-gen-rust") (r "^0.2.2") (d #t) (k 0)))) (h "0xk3r77ws7ijkbzkfvv68znz9zpfw2rz11l06qm2a1alxzp5125b") (r "1.64")))

(define-public crate-wai-bindgen-gen-rust-wasm-0.2.3 (c (n "wai-bindgen-gen-rust-wasm") (v "0.2.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.3") (d #t) (k 0)) (d (n "wai-bindgen-gen-rust") (r "^0.2.3") (d #t) (k 0)))) (h "16b1i8fa1jhyaa45rdxdb4szw01zch7xayrsgz4gm1j0wzjmrwyn") (r "1.64")))

