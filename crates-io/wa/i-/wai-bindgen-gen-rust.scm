(define-module (crates-io wa i- wai-bindgen-gen-rust) #:use-module (crates-io))

(define-public crate-wai-bindgen-gen-rust-0.2.0-rc.1 (c (n "wai-bindgen-gen-rust") (v "0.2.0-rc.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "1g9ym4wh2qcc2gp8gi0mnfspqj0n6ryyp18ga3aifxvyhl17nbf1") (r "1.64")))

(define-public crate-wai-bindgen-gen-rust-0.2.0 (c (n "wai-bindgen-gen-rust") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0") (d #t) (k 0)))) (h "16f39099kqsf6lk22dar0ichm1y3mdwmxy8px877vabp73aaisbv") (r "1.64")))

(define-public crate-wai-bindgen-gen-rust-0.2.1 (c (n "wai-bindgen-gen-rust") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.1") (d #t) (k 0)))) (h "0zfzr82l6ga46vm6x7s2ragarbmp8a431d2navznxp8j6y15yqhp") (r "1.64")))

(define-public crate-wai-bindgen-gen-rust-0.2.2 (c (n "wai-bindgen-gen-rust") (v "0.2.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.2") (d #t) (k 0)))) (h "1wi1mvs7dgf6k6chz73k9km37k5p8g326rzl6whqhqvg0k9zr1nx") (r "1.64")))

(define-public crate-wai-bindgen-gen-rust-0.2.3 (c (n "wai-bindgen-gen-rust") (v "0.2.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.3") (d #t) (k 0)))) (h "1zifa8yp0h46vqha6y5i2l9kjgi3zyr07vs0ghrw858573l0bg0r") (r "1.64")))

