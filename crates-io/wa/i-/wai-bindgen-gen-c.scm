(define-module (crates-io wa i- wai-bindgen-gen-c) #:use-module (crates-io))

(define-public crate-wai-bindgen-gen-c-0.2.0-rc.1 (c (n "wai-bindgen-gen-c") (v "0.2.0-rc.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "04azjjclsi9k6mbl867gad3kkf9sf58sdpbgp7kgzwbp0br469ik") (r "1.64")))

(define-public crate-wai-bindgen-gen-c-0.2.0 (c (n "wai-bindgen-gen-c") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0") (d #t) (k 0)))) (h "1b7c9f39vnl2lczrbjr5hcdkya8ghgjxpgh62znn7fvzcv9sr05f") (r "1.64")))

(define-public crate-wai-bindgen-gen-c-0.2.1 (c (n "wai-bindgen-gen-c") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.1") (d #t) (k 0)))) (h "1w6spgyrvkd07413q60a6kd80jkmxdjy1qn2p3b601kg4g5z391q") (r "1.64")))

(define-public crate-wai-bindgen-gen-c-0.2.2 (c (n "wai-bindgen-gen-c") (v "0.2.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.2") (d #t) (k 0)))) (h "131hiwy96p05xrrxy2nz5iwirl17gjzb25kpfwn9jzrqgzs0ffy8") (r "1.64")))

(define-public crate-wai-bindgen-gen-c-0.2.3 (c (n "wai-bindgen-gen-c") (v "0.2.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.3") (d #t) (k 0)))) (h "143h8cc74ydb6nq0v66rh5v381k7v2z1j2dahmvnd60mzf4qzyi7") (r "1.64")))

