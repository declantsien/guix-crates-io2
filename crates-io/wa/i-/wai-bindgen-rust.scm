(define-module (crates-io wa i- wai-bindgen-rust) #:use-module (crates-io))

(define-public crate-wai-bindgen-rust-0.2.0-rc.1 (c (n "wai-bindgen-rust") (v "0.2.0-rc.1") (d (list (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "wai-bindgen-rust-impl") (r "^0.2.0-rc.1") (o #t) (d #t) (k 0)))) (h "1k5lfi4sm664nrwj1s46mdwnkmh60rrakbxkgrhn849gh26vl1qb") (f (quote (("macros" "wai-bindgen-rust-impl") ("default" "macros" "async") ("async" "async-trait")))) (r "1.64")))

(define-public crate-wai-bindgen-rust-0.2.0 (c (n "wai-bindgen-rust") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "wai-bindgen-rust-impl") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "040j5zsx9lfnj84n4zpphypj5m8d9qsvhcrj08k2hkrlqjb735ck") (f (quote (("macros" "wai-bindgen-rust-impl") ("default" "macros" "async") ("async" "async-trait")))) (r "1.64")))

(define-public crate-wai-bindgen-rust-0.2.1 (c (n "wai-bindgen-rust") (v "0.2.1") (d (list (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "wai-bindgen-rust-impl") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1kb12sqgy2gsfv2jap8520qjk5wqifpg8lk26aclaw8zd0gwkrlh") (f (quote (("macros" "wai-bindgen-rust-impl") ("default" "macros" "async") ("async" "async-trait")))) (r "1.64")))

(define-public crate-wai-bindgen-rust-0.2.2 (c (n "wai-bindgen-rust") (v "0.2.2") (d (list (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "wai-bindgen-rust-impl") (r "^0.2.2") (o #t) (d #t) (k 0)))) (h "1kgnh3nhs3jdw926rwp4ga12jrkqqplba10kyb8w5rpykf4fy3ny") (f (quote (("macros" "wai-bindgen-rust-impl") ("default" "macros" "async") ("async" "async-trait")))) (r "1.64")))

(define-public crate-wai-bindgen-rust-0.2.3 (c (n "wai-bindgen-rust") (v "0.2.3") (d (list (d (n "async-trait") (r "^0.1.51") (o #t) (d #t) (k 0)) (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "wai-bindgen-rust-impl") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0hbpsvix4j69f8ix8jj2v8df1xydxy7ip4sy7bl67h28yk302mjf") (f (quote (("macros" "wai-bindgen-rust-impl") ("default" "macros" "async") ("async" "async-trait")))) (r "1.64")))

