(define-module (crates-io wa i- wai-bindgen-gen-core) #:use-module (crates-io))

(define-public crate-wai-bindgen-gen-core-0.2.0-rc.1 (c (n "wai-bindgen-gen-core") (v "0.2.0-rc.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wai-parser") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "0snhgdv9crcanjrqgjzp5r8491qsim402g0arzfgncpmv4pzm08f") (r "1.64")))

(define-public crate-wai-bindgen-gen-core-0.2.0 (c (n "wai-bindgen-gen-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wai-parser") (r "^0.2.0") (d #t) (k 0)))) (h "0zn8sidnsznvvysf0glpj81zplvcjasihqmclb8q5fxrkar7kir7") (r "1.64")))

(define-public crate-wai-bindgen-gen-core-0.2.1 (c (n "wai-bindgen-gen-core") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wai-parser") (r "^0.2.1") (d #t) (k 0)))) (h "0ywl9ljj14h9qkssdkbhpzq8iz5p0k8a2c0msaisl7xpbzfa85nd") (r "1.64")))

(define-public crate-wai-bindgen-gen-core-0.2.2 (c (n "wai-bindgen-gen-core") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wai-parser") (r "^0.2.2") (d #t) (k 0)))) (h "0k3fb8vcmr85byjxrvb0xwlas9mc3zdjfb5q1ca9ks4p2rwj2fn4") (r "1.64")))

(define-public crate-wai-bindgen-gen-core-0.2.3 (c (n "wai-bindgen-gen-core") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wai-parser") (r "^0.2.3") (d #t) (k 0)))) (h "0v470cl1xqfqdlq3dy9ysv58zq179hirg088nci1308hnm0xr8qs") (r "1.64")))

