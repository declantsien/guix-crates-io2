(define-module (crates-io wa i- wai-bindgen-gen-wasmtime-py) #:use-module (crates-io))

(define-public crate-wai-bindgen-gen-wasmtime-py-0.2.0-rc.1 (c (n "wai-bindgen-gen-wasmtime-py") (v "0.2.0-rc.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0-rc.1") (d #t) (k 0)))) (h "0n4pslnbi1h9zyjijjqpyhrfccq5crdhs4xrxj9dw0namymck2b4") (r "1.64")))

(define-public crate-wai-bindgen-gen-wasmtime-py-0.2.0 (c (n "wai-bindgen-gen-wasmtime-py") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.0") (d #t) (k 0)))) (h "03vkvcsq5px8zdf6xp6hy0kfswn9qswxrd8mb8yz7hhnnqqvxhyb") (r "1.64")))

(define-public crate-wai-bindgen-gen-wasmtime-py-0.2.1 (c (n "wai-bindgen-gen-wasmtime-py") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.1") (d #t) (k 0)))) (h "0j64gy3625vw26jvzcsrfiflnszxcnbb5rf6vhlg39bx4vf5l2pp") (r "1.64")))

(define-public crate-wai-bindgen-gen-wasmtime-py-0.2.2 (c (n "wai-bindgen-gen-wasmtime-py") (v "0.2.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.2") (d #t) (k 0)))) (h "1xwzzq54f63wflp8h2s7llqr6rrjy67yc2q4r9wphbhms2ih5rcm") (r "1.64")))

(define-public crate-wai-bindgen-gen-wasmtime-py-0.2.3 (c (n "wai-bindgen-gen-wasmtime-py") (v "0.2.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wai-bindgen-gen-core") (r "^0.2.3") (d #t) (k 0)))) (h "05g214zwaj32hj7ri64rvvsll3g5igj4ifychk58hkyxasjiz9w2") (r "1.64")))

