(define-module (crates-io wa -s wa-serde-derive) #:use-module (crates-io))

(define-public crate-wa-serde-derive-0.1.101 (c (n "wa-serde-derive") (v "0.1.101") (d (list (d (n "watt") (r "^0.1") (d #t) (k 0)))) (h "0fdqaswca71w04sgrl8zxxk8nzs8njn8s38ywh41g1n9hdjvhm7s")))

(define-public crate-wa-serde-derive-0.1.102 (c (n "wa-serde-derive") (v "0.1.102") (d (list (d (n "watt") (r "^0.3") (d #t) (k 0)))) (h "0ir17q20djgs5jhz2b99cd7kv513l80f7ahv4l4fjh3xkzcwf36v")))

(define-public crate-wa-serde-derive-0.1.137 (c (n "wa-serde-derive") (v "0.1.137") (d (list (d (n "watt") (r "^0.4") (d #t) (k 0)))) (h "1mrrh60l9q5yldxgbzhhzsjs4prb0bm1yzzgix3q23w66bpqn6yz")))

