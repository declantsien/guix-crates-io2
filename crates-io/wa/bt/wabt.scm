(define-module (crates-io wa bt wabt) #:use-module (crates-io))

(define-public crate-wabt-0.1.0 (c (n "wabt") (v "0.1.0") (d (list (d (n "wabt-sys") (r "^0.1") (d #t) (k 0)))) (h "0knanlbhs3bmvpqfd0ka4njs2547wm47qy0nvcjdxf5fb06kb9v5")))

(define-public crate-wabt-0.1.1 (c (n "wabt") (v "0.1.1") (d (list (d (n "wabt-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0n2vc9wl3420d8dw80lp88lhc5kzkkj8gvd2hi7w85n80yzavb0z")))

(define-public crate-wabt-0.1.2 (c (n "wabt") (v "0.1.2") (d (list (d (n "wabt-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1gyapinqk1g289zkakf52x35phc8hp4wsq70j7yla9vwdgw443r3")))

(define-public crate-wabt-0.1.3 (c (n "wabt") (v "0.1.3") (d (list (d (n "wabt-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0pmjrl9prd2l9anh7qa0wliarccmfm9ynp95vr1ds4hbwk949nxf")))

(define-public crate-wabt-0.1.4 (c (n "wabt") (v "0.1.4") (d (list (d (n "wabt-sys") (r "^0.1.2") (d #t) (k 0)))) (h "03sp367brrnvarj6ncpzbcsbc9gigzw0n98r325a2g7v008r7gxq")))

(define-public crate-wabt-0.1.5 (c (n "wabt") (v "0.1.5") (d (list (d (n "wabt-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0xma0chjnwrc1yylkhnw3l9sh6p903cr27zmfh9y1q2494h2jgs9")))

(define-public crate-wabt-0.1.6 (c (n "wabt") (v "0.1.6") (d (list (d (n "wabt-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0rkhdwacq69fvd8rpsikhn4ndrgp4r6n6dak5mgbn3d0c1svfzlg")))

(define-public crate-wabt-0.1.7 (c (n "wabt") (v "0.1.7") (d (list (d (n "wabt-sys") (r "^0.1.3") (d #t) (k 0)))) (h "138bqpl3x0s5r32l3j0s1f1dh5xpanca8b34jfnc113484yhdiah")))

(define-public crate-wabt-0.1.8 (c (n "wabt") (v "0.1.8") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1rah0250c7bawqcmjfmjb27dsr6rb3b0vf912cqr17108iqrj0p9")))

(define-public crate-wabt-0.2.0 (c (n "wabt") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0b1xxphwj2h2xbfzlarx67zwycm3mjwxq0bb0bdpy412h96mizqn")))

(define-public crate-wabt-0.2.1 (c (n "wabt") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.1.5") (d #t) (k 0)))) (h "1x51cn8c5k34l5al436g294hrjn3jrancgpz5cb1jan1i8ga89g6") (y #t)))

(define-public crate-wabt-0.2.2 (c (n "wabt") (v "0.2.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.1.4") (d #t) (k 0)))) (h "0gfzr4rypddjxwll4k8d5rrifnhpvljxfngz63zlklcaq2iikqci")))

(define-public crate-wabt-0.2.3 (c (n "wabt") (v "0.2.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.1.4") (d #t) (k 0)))) (h "1a3ispbaaprr24l8277lbdn6i21bisa687svm59gvafxwp7dg5pr") (y #t)))

(define-public crate-wabt-0.3.0 (c (n "wabt") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.6") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.1.4") (d #t) (k 0)))) (h "00d8ih9qv2cnq4v0b1jzz39vhpycgnzc8666zv7jimz5y2csw4k4")))

(define-public crate-wabt-0.4.0 (c (n "wabt") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.2.0") (d #t) (k 0)))) (h "18hq7zlvz79xlidvadz18ahzqxhi3j4k7li4yc2jgkww4i1yaahq")))

(define-public crate-wabt-0.5.0 (c (n "wabt") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0d47kfrv4jn6zrd7447s4xjlq139dslhrddgnlr1a7r0zd1pgcz9")))

(define-public crate-wabt-0.6.0 (c (n "wabt") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0xas6g95jjfzml4k051fda81d9zgixjfxva5fl58r2gj6bwzls07")))

(define-public crate-wabt-0.7.0 (c (n "wabt") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1lknfv6b747aaizm38jh6ip6kxxb60s7z9g5li8mpkcwa3f6x2lf")))

(define-public crate-wabt-0.7.1 (c (n "wabt") (v "0.7.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.5.1") (d #t) (k 0)))) (h "0qxp9hqqnv3ak9c9i0apizan209ydqzm4hbzf7zps1yn9d4v492v")))

(define-public crate-wabt-0.7.2 (c (n "wabt") (v "0.7.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.5.2") (d #t) (k 0)))) (h "19ciw4j755km7rrxfh3q80ihfh847bqmrbqz1hj5awj6gvwg1wcg")))

(define-public crate-wabt-0.7.3 (c (n "wabt") (v "0.7.3") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.5.3") (d #t) (k 0)))) (h "07x9wfs0l2pk84mfkmff329ixpcfqyi342bwv0ymyz646hcalih2")))

(define-public crate-wabt-0.7.4 (c (n "wabt") (v "0.7.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.5.3") (d #t) (k 0)))) (h "1p9ascb9bygxw6n18lq9pphm5ba4pw7n83z78xscr47312jn7r3l")))

(define-public crate-wabt-0.8.0 (c (n "wabt") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0fr4nbdhk4pi5h8x2qgsqcmz8byvkpz47jrga8slhxvcyl66frgg")))

(define-public crate-wabt-0.9.0 (c (n "wabt") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.6.1") (d #t) (k 0)))) (h "1hg1xhw82nqfp8q8d59nzw17mx0lnb65089w278cppchs50dsznh")))

(define-public crate-wabt-0.9.1 (c (n "wabt") (v "0.9.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.6.1") (d #t) (k 0)))) (h "1rr0370332d9fjsq0idgvmrs3261bahsifl0cbv2v92ck3bgbdcl")))

(define-public crate-wabt-0.9.2 (c (n "wabt") (v "0.9.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.7") (d #t) (k 0)))) (h "14v6lh2xcfj0glakdf7ri7agjra2b53ryq42d50pirf6hq95qp1w") (y #t)))

(define-public crate-wabt-0.10.0 (c (n "wabt") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "wabt-sys") (r "^0.8") (d #t) (k 0)))) (h "171vjvc9arnkd54a2ns55ww3jwm47bs7q46gpj9s50bcbqyzkgh0")))

