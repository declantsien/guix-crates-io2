(define-module (crates-io wa bt wabt-sys) #:use-module (crates-io))

(define-public crate-wabt-sys-0.1.0 (c (n "wabt-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "0q95vgsrxsabxcrqkkcwb2aq84srxm34srnaj6h0gv3bvy971kc2")))

(define-public crate-wabt-sys-0.1.1 (c (n "wabt-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "1ardhf3biacphfrisnkm9r4z3nzlc9fnr2r4ai8rgy21h01qzqag")))

(define-public crate-wabt-sys-0.1.2 (c (n "wabt-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "0hrhgn68gbv5g48hyz5hzqfysivhqm9v41hpn8890x72hzb6kvcm")))

(define-public crate-wabt-sys-0.1.3 (c (n "wabt-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "1mc138mkc92lsw3bz13052phdja0yn3j33hz89pphbnmsc8k0q35")))

(define-public crate-wabt-sys-0.1.4 (c (n "wabt-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "0ab5bsgwvb1kvip7rk792kv8679mcsz99pvfk7k9p0ypdkcv2rzw")))

(define-public crate-wabt-sys-0.1.5 (c (n "wabt-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "0dda4d0smadzikklvraijifsl877sbsw0bhgwqfl9pav3zwwv64j") (y #t)))

(define-public crate-wabt-sys-0.2.0 (c (n "wabt-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "18vxspfgrn54zfsc6y0k2jvqqfy4m9jzbcj1i5hk4asajdmpr9wc")))

(define-public crate-wabt-sys-0.3.0 (c (n "wabt-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.25") (d #t) (k 1)))) (h "0w54am4y5rikjb47adrsr0m4mnbws749d1xswwz5hli3f78ch7ap")))

(define-public crate-wabt-sys-0.4.0 (c (n "wabt-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)))) (h "1p79cpv2786z22axv83ginxqys0y788kw7v1lmfk366nl27rmaja")))

(define-public crate-wabt-sys-0.5.0 (c (n "wabt-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)))) (h "1mkj5c7dsk4a398xfnrhyl4vvib9vliak56pk83j11kr54hcmb4z")))

(define-public crate-wabt-sys-0.5.1 (c (n "wabt-sys") (v "0.5.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)))) (h "1pbdkqzqch137rm28801n47cwiyas64hvln53p77q1icsbhyyahj")))

(define-public crate-wabt-sys-0.5.2 (c (n "wabt-sys") (v "0.5.2") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 1)))) (h "18hz0l1lyqvn9pcdkxjar9rzx5vg1bjr3ncn5qakz4hazqmrij0c")))

(define-public crate-wabt-sys-0.5.3 (c (n "wabt-sys") (v "0.5.3") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 1)))) (h "0lca9vgwzwc17j0px738wga7nxwbpjhxxq1h2xv68vh9c6xkc8s6")))

(define-public crate-wabt-sys-0.5.4 (c (n "wabt-sys") (v "0.5.4") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 1)))) (h "0g3yxp3m047mwg6q8b1yfm0isripbqvifdsb226mk0lyf4jmn9m6")))

(define-public crate-wabt-sys-0.6.0 (c (n "wabt-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 1)))) (h "0qjvg0mm0xa9ycm0fi7cny158hd4fs3x7mwg16k515irkdyisldd")))

(define-public crate-wabt-sys-0.6.1 (c (n "wabt-sys") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 1)))) (h "1apfcgz7yb0nn1ad9fy347dq7w7yczyfqg4jf55xn2hh44cchr5h")))

(define-public crate-wabt-sys-0.7.0 (c (n "wabt-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 1)))) (h "021sw7271lr2wncm5n8jh6a6g666jaw3a25r7b0pvbbar4yiapdg")))

(define-public crate-wabt-sys-0.7.1 (c (n "wabt-sys") (v "0.7.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 1)))) (h "114hiwxrv95jry981qf3z27p904qxqicmlx8szx9cp9ypcz09mr3")))

(define-public crate-wabt-sys-0.7.2 (c (n "wabt-sys") (v "0.7.2") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 1)))) (h "1pv6wr6b6xnkqkpywr92vi8aj5n9rh0m7dpnwba1zf3yizwrbih1") (y #t)))

(define-public crate-wabt-sys-0.8.0 (c (n "wabt-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.32") (d #t) (k 1)) (d (n "glob") (r "^0.2.11") (d #t) (t "cfg(windows)") (k 1)))) (h "14bp9j327i975rkvyrc5hks3s1063ig9ngkidsc1cgpnb4qh8khs")))

