(define-module (crates-io wa mu wamu-core) #:use-module (crates-io))

(define-public crate-wamu-core-0.1.0 (c (n "wamu-core") (v "0.1.0") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "crypto-bigint") (r "^0.5.2") (d #t) (k 0)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "k256") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("alloc" "zeroize_derive"))) (d #t) (k 0)))) (h "0wi596l0drz6l5zqbg7mhx37a84iwcbv0rp40cvpy5c36y053z55") (f (quote (("dev") ("default"))))))

(define-public crate-wamu-core-0.1.1 (c (n "wamu-core") (v "0.1.1") (d (list (d (n "aes-gcm") (r "^0.10.2") (d #t) (k 0)) (d (n "crypto-bigint") (r "^0.5.2") (d #t) (k 0)) (d (n "hkdf") (r "^0.12.3") (d #t) (k 0)) (d (n "k256") (r "^0.13.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (f (quote ("alloc" "zeroize_derive"))) (d #t) (k 0)))) (h "1w1dm13rzz1q7jfliwpc1f3fnjm9gbq4i3xfqqaqanm0dgn5vi6y") (f (quote (("dev") ("default"))))))

