(define-module (crates-io wa bi wabisabi) #:use-module (crates-io))

(define-public crate-wabisabi-0.0.0 (c (n "wabisabi") (v "0.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1v9vxgxg1bifcajzjc8ma67cddwl6rzn4737vi45gsfgwl6m358j")))

(define-public crate-wabisabi-0.0.1 (c (n "wabisabi") (v "0.0.1") (d (list (d (n "const-random") (r "^0.1.13") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07qg9r248sj7pz009c75b9lxfnw2g3ay3m9qpjaamvgj1k0kgrva")))

