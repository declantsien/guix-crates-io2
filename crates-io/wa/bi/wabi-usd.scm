(define-module (crates-io wa bi wabi-usd) #:use-module (crates-io))

(define-public crate-wabi-usd-0.1.0 (c (n "wabi-usd") (v "0.1.0") (h "1rrzg14ajr8rrrxyw2cdpwy8xmxvi7qaqa43xm0h80lyi4f9awiw")))

(define-public crate-wabi-usd-0.1.1 (c (n "wabi-usd") (v "0.1.1") (h "1hgj6d4zc9zvx0zass171g20kiy7a7yh129j36f464mfalqfd5hl")))

