(define-module (crates-io wa rp warp-range) #:use-module (crates-io))

(define-public crate-warp-range-0.1.0 (c (n "warp-range") (v "0.1.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.7") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1mi75rdvb1v20vvkxpdwy8pjr1cpwnpaagqjfa8j6bc2fg0c716c")))

(define-public crate-warp-range-1.0.0 (c (n "warp-range") (v "1.0.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1vb93skdgsjq39y5jrv76vkiy3bwwa1f3dg0ydq2v8yg3c1gcc46")))

(define-public crate-warp-range-1.0.1 (c (n "warp-range") (v "1.0.1") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "07r508vzl6qf7wmv23fz34v9qnhir6kyv9yj14668wnjig2sxs63")))

(define-public crate-warp-range-2.0.0 (c (n "warp-range") (v "2.0.0") (d (list (d (n "async-stream") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0fl1crm9a1y2qdimr1hc3g8szs8mg8zdl47xb310r986bs5k1fxf")))

