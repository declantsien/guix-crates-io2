(define-module (crates-io wa rp warp_form_method) #:use-module (crates-io))

(define-public crate-warp_form_method-0.1.0 (c (n "warp_form_method") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1mfmldxb105ppgxbwd9cj9zi1hry1dbpaf1mnski1s6fhwja7sf3")))

