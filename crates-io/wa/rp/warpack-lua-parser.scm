(define-module (crates-io wa rp warpack-lua-parser) #:use-module (crates-io))

(define-public crate-warpack-lua-parser-0.0.1 (c (n "warpack-lua-parser") (v "0.0.1") (d (list (d (n "pest") (r "^2.5.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.4") (d #t) (k 0)))) (h "1kh51bf11sh8m12fgakq7kq17v8fh4v61gl06hjvvh9xdpzq4fdd")))

