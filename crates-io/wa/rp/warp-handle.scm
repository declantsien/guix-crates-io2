(define-module (crates-io wa rp warp-handle) #:use-module (crates-io))

(define-public crate-warp-handle-0.1.0 (c (n "warp-handle") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "0rnp0aid0d6sw2kl7f6lr1dj96r39my7mbs29if8667dxpq27i10")))

(define-public crate-warp-handle-0.2.0 (c (n "warp-handle") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1li7ngvb3xi9nwxr5g252fmfqjxww8nhn663972h10ykng0h53sh")))

(define-public crate-warp-handle-0.2.1 (c (n "warp-handle") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "0hjw2zyf39vxqs2k362x368cx82bl6vsn9lchv52kz9qfqdnvw9g")))

(define-public crate-warp-handle-0.2.2 (c (n "warp-handle") (v "0.2.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1zkqf3p6m6vjhn33pqmbj4ja36kgp33x6893a4grnxwgk7j7fbzg")))

(define-public crate-warp-handle-0.2.3 (c (n "warp-handle") (v "0.2.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "0nl7c1dsrgarfxavndysh8dcfm525662klax6h1414xdz9lff2rz")))

(define-public crate-warp-handle-0.3.0 (c (n "warp-handle") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "0m0jh0c79bn97wy4wq7ciibj768kn2b3p1845rz4prk32224gjib")))

(define-public crate-warp-handle-0.4.0 (c (n "warp-handle") (v "0.4.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1cdh1xcfakcaz4082hzs6s14a5nc7y5icqk7d1bml129a615jbjw")))

(define-public crate-warp-handle-0.5.0 (c (n "warp-handle") (v "0.5.0") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 2)) (d (n "tokio") (r "^1.4.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.1") (d #t) (k 0)))) (h "1y3sj5v6rbzh1x8z6qk7s1favd5i39g4b6jnx5929b0k9d0arv77")))

(define-public crate-warp-handle-0.5.1 (c (n "warp-handle") (v "0.5.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.6") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1x1dp18p22hj0hnkf6kzplm1aaab4014bc7qpjf4cngdzw6gjxw9")))

