(define-module (crates-io wa rp warp-websockify) #:use-module (crates-io))

(define-public crate-warp-websockify-0.1.0 (c (n "warp-websockify") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "tcp" "uds"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1jgx0ls47695cxzrjrzhq6m7vbs247zfjmskmm8cba8d065izy2d")))

(define-public crate-warp-websockify-0.1.1 (c (n "warp-websockify") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "tcp" "uds"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "063i3hpq7nascq1xq4hdmhv4ccsqwr6nwyvvzr6q8j5xa1c6gb7v")))

(define-public crate-warp-websockify-0.2.0 (c (n "warp-websockify") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "tcp" "uds"))) (d #t) (k 0)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1ix69wgq25hgchswwhhh3y00750g4bydagqin2pqd3lj6lvjzj9p")))

(define-public crate-warp-websockify-0.3.0 (c (n "warp-websockify") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "01pa9biqzv2kj0v7rj4rmr1772w0nlnnk2q7fkf2f86sqaw6gwfg")))

