(define-module (crates-io wa rp warp_dsl) #:use-module (crates-io))

(define-public crate-warp_dsl-0.0.1 (c (n "warp_dsl") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "warp") (r "^0.1") (d #t) (k 2)) (d (n "warp_dsl_impl") (r "^0.0.1") (d #t) (k 0)))) (h "0000x2ccviqw00a9xnk6pzcap76b25q70avhwfcig21qsxncm37h")))

