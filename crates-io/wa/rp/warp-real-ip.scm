(define-module (crates-io wa rp warp-real-ip) #:use-module (crates-io))

(define-public crate-warp-real-ip-0.1.0 (c (n "warp-real-ip") (v "0.1.0") (d (list (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1ssp47mln66nbzc29a6dj5i0r60rsgimf3s1py84bxjbkwazpm60")))

(define-public crate-warp-real-ip-0.1.1 (c (n "warp-real-ip") (v "0.1.1") (d (list (d (n "rfc7239") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "1kkxmwz4jwqwypyszrzz8szwv54rv7dscwrc9gdvnsn1b5v4razc")))

(define-public crate-warp-real-ip-0.2.0 (c (n "warp-real-ip") (v "0.2.0") (d (list (d (n "rfc7239") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0fq0hkkbw491zq0wnlrmimhlvs7dihvs4mqh2ssfl6bvy3wkzwi2")))

