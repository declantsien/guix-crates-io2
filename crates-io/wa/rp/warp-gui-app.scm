(define-module (crates-io wa rp warp-gui-app) #:use-module (crates-io))

(define-public crate-warp-gui-app-0.1.0 (c (n "warp-gui-app") (v "0.1.0") (d (list (d (n "slint") (r "^1.0") (d #t) (k 0)) (d (n "slint-build") (r "^1.0") (d #t) (k 1)))) (h "1j0hf868gj43vs3gssxzlfdnvp6kjz4z0jfv19s9zcrrkm200mbz")))

(define-public crate-warp-gui-app-0.1.1 (c (n "warp-gui-app") (v "0.1.1") (d (list (d (n "slint") (r "^1.0") (d #t) (k 0)) (d (n "slint-build") (r "^1.0") (d #t) (k 1)))) (h "0ncdmfwrsar9ccxgawzimc2xcprbfcgppxj29s78x70vh95n0xsh") (y #t)))

(define-public crate-warp-gui-app-0.1.2 (c (n "warp-gui-app") (v "0.1.2") (d (list (d (n "slint") (r "^1.0") (d #t) (k 0)) (d (n "slint-build") (r "^1.0") (d #t) (k 1)))) (h "0r72w5zv9avdh6l2dxh63l4cnpaqiscbv3brlryf3bx7qr2fr9v7")))

