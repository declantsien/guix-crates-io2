(define-module (crates-io wa rp warpgate_pdk) #:use-module (crates-io))

(define-public crate-warpgate_pdk-0.1.0 (c (n "warpgate_pdk") (v "0.1.0") (d (list (d (n "extism-pdk") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.3.1") (d #t) (k 0)))) (h "0pibh22qp0hrgjl01g4nsznkn73nbpp1da6ffh7x5m77iw5xcm1y")))

(define-public crate-warpgate_pdk-0.2.0 (c (n "warpgate_pdk") (v "0.2.0") (d (list (d (n "extism-pdk") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.4.0") (d #t) (k 0)))) (h "01iy2w156g7x489gid9gfz1g85g314id2ar8pkvazwsfhw03g6kn")))

(define-public crate-warpgate_pdk-0.2.1 (c (n "warpgate_pdk") (v "0.2.1") (d (list (d (n "extism-pdk") (r "^1.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.4.1") (d #t) (k 0)))) (h "0pmsikn4n9j7w39f2qkpacyjan6pbml5pzfz85dccw53mnikbp0l")))

(define-public crate-warpgate_pdk-0.2.2 (c (n "warpgate_pdk") (v "0.2.2") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.4.3") (d #t) (k 0)))) (h "10swvxqfiqbzfdcbbdprjm38025w54xqmnv1580h1j39kis8wvw1")))

(define-public crate-warpgate_pdk-0.2.3 (c (n "warpgate_pdk") (v "0.2.3") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.4.4") (d #t) (k 0)))) (h "11gppjr78nrvgfq54z30m9dmsnz76d5imj9wirycpyyldixsy76c")))

(define-public crate-warpgate_pdk-0.2.4 (c (n "warpgate_pdk") (v "0.2.4") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.4.5") (d #t) (k 0)))) (h "02sjcxddc2ansy0ygz8qd5dswnwscd5l10gb3ljn65057b2ck1hj")))

(define-public crate-warpgate_pdk-0.3.0 (c (n "warpgate_pdk") (v "0.3.0") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.5.0") (d #t) (k 0)))) (h "04s0jni6lzg1wypm38xmkxq0ah0l4rqfk9mcx1hjasx3675i1113")))

(define-public crate-warpgate_pdk-0.3.1 (c (n "warpgate_pdk") (v "0.3.1") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.5.1") (d #t) (k 0)))) (h "08gghafaxzc076w47ca6czxlhd0cnn3nmi2qfvzd74jkrqfsk8nr")))

(define-public crate-warpgate_pdk-0.4.0 (c (n "warpgate_pdk") (v "0.4.0") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.6.0") (d #t) (k 0)))) (h "01zmh6cmapwphrlggw1dzi2z6nlcmvdmgsk2q6cx5q9vpn6p66sz")))

(define-public crate-warpgate_pdk-0.5.0 (c (n "warpgate_pdk") (v "0.5.0") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.7.0") (d #t) (k 0)))) (h "0mlhzd2c9k5zvbif01qyi2l27dxxcnla7g942p8gqx24j7zvwcs4")))

(define-public crate-warpgate_pdk-0.5.1 (c (n "warpgate_pdk") (v "0.5.1") (d (list (d (n "extism-pdk") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.202") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.7.1") (d #t) (k 0)))) (h "0zabdcnxvgp8y1bswl2smxaar16wiz3f0wq48myrhpsc7ibhfnny")))

(define-public crate-warpgate_pdk-0.5.2 (c (n "warpgate_pdk") (v "0.5.2") (d (list (d (n "extism-pdk") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warpgate_api") (r "^0.7.2") (d #t) (k 0)))) (h "1x9gba1gb90f4xqyyxzg145wqzd1fzj1kkv9zx7yg79xc2zqf3g0")))

