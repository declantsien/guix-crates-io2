(define-module (crates-io wa rp warp_subdomain) #:use-module (crates-io))

(define-public crate-warp_subdomain-1.0.0 (c (n "warp_subdomain") (v "1.0.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("test-util" "macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.2") (d #t) (k 0)))) (h "1jgz0lxbbjd07hb55d2h93akc56cal56vvzrfr13dx0vd112friv")))

(define-public crate-warp_subdomain-1.1.0 (c (n "warp_subdomain") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("test-util" "macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "15xhc2xmaszcg8k5hgjci10f22dr6x1l8fw4pc5n5w52486p9bd5") (f (quote (("default" "warp" "tokio/test-util" "tokio/macros")))) (y #t)))

(define-public crate-warp_subdomain-1.2.0 (c (n "warp_subdomain") (v "1.2.0") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "tokio") (r "^1.20.1") (f (quote ("test-util" "macros"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.2") (o #t) (d #t) (k 0)))) (h "0fbqd5k7w6vfijj68wd01gbwszg85sjms2p61d09jpi16z79d6wx") (f (quote (("default" "warp"))))))

