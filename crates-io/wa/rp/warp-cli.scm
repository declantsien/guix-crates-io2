(define-module (crates-io wa rp warp-cli) #:use-module (crates-io))

(define-public crate-warp-cli-0.1.0 (c (n "warp-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.29") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "rpassword") (r "^7.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "termcolor") (r "^1.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "17fl0bvpx3830xailzdj2n6hi2rvknsgrv2vr5k9ksq83plq0dp6")))

