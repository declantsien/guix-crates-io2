(define-module (crates-io wa rp warp-embed) #:use-module (crates-io))

(define-public crate-warp-embed-0.1.0 (c (n "warp-embed") (v "0.1.0") (d (list (d (n "mime_guess") (r "^1") (d #t) (k 0)) (d (n "rust-embed") (r "^5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "0ic8pw2q2pysq0v56d3q9080q3qfvrgr4gf3cbik29p1qm2q4i4q")))

(define-public crate-warp-embed-0.1.1 (c (n "warp-embed") (v "0.1.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "mime_guess") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "rust-embed") (r "^5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "155d0w69n2yyhwiydazi0v4j8ay7x46sjf8iiph82xdbhm86v51z")))

(define-public crate-warp-embed-0.1.2 (c (n "warp-embed") (v "0.1.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "mime_guess") (r "^1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "rust-embed") (r "^5") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "warp") (r "^0.2") (d #t) (k 0)))) (h "13an1mb7v1wqlhhab22zydagqbkd8xdc33lzgzk2c6gzy3z565h3")))

(define-public crate-warp-embed-0.3.0 (c (n "warp-embed") (v "0.3.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "rust-embed") (r "^5") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1r45jz1mdd9cqjn7675m8zxgks2x8cjzxa03x3flbvcrnwr0jpm2")))

(define-public crate-warp-embed-0.4.0 (c (n "warp-embed") (v "0.4.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "rust-embed") (r "^6") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1bqhf4rlix5qgxmsjfw9ra1qjwmv5lx2ld45vvmy15zh4ng16n5r")))

(define-public crate-warp-embed-0.5.0 (c (n "warp-embed") (v "0.5.0") (d (list (d (n "clap") (r "^4") (f (quote ("cargo"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "mime_guess") (r "^2") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)) (d (n "rust-embed") (r "^8") (d #t) (k 0)) (d (n "tokio") (r "^1.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "0injqzc9k36q446d69xsh9k9m7nhn9lwr8r2b3hiir8j9xzcvsgl")))

