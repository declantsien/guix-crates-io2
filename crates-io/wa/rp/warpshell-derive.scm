(define-module (crates-io wa rp warpshell-derive) #:use-module (crates-io))

(define-public crate-warpshell-derive-0.1.0 (c (n "warpshell-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0s3ah7p52x2znhnwwy9yclkmnrira250h5f3mmwfvxs5dqxx4lp7")))

