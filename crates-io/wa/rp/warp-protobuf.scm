(define-module (crates-io wa rp warp-protobuf) #:use-module (crates-io))

(define-public crate-warp-protobuf-0.1.0 (c (n "warp-protobuf") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "warp") (r "^0.2.1") (d #t) (k 0)))) (h "0j5sfb14sjxzi3wy60c11n3c2phavqzyypjil5fa3b5iv6mh6drs")))

