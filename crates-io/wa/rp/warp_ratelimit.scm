(define-module (crates-io wa rp warp_ratelimit) #:use-module (crates-io))

(define-public crate-warp_ratelimit-1.0.0 (c (n "warp_ratelimit") (v "1.0.0") (d (list (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "18dr7g4qs3vwxmz4p8fcspgc09g0ykcz879f1d8i55bwbs17hr4x")))

