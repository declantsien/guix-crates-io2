(define-module (crates-io wa rp warpshell-cli) #:use-module (crates-io))

(define-public crate-warpshell-cli-0.1.0 (c (n "warpshell-cli") (v "0.1.0") (d (list (d (n "amplify") (r "^3.14.1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "warpshell") (r "^0.1.0") (d #t) (k 0)))) (h "1snxykzcb36k2w886k94p73wjfwib42l6mn2xhlzaj35w3lakxbq")))

