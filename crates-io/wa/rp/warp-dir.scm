(define-module (crates-io wa rp warp-dir) #:use-module (crates-io))

(define-public crate-warp-dir-0.1.0 (c (n "warp-dir") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.21") (f (quote ("derive" "string" "cargo"))) (d #t) (k 0)) (d (n "clap_complete") (r "^4.3.2") (d #t) (k 0)) (d (n "etcetera") (r "^0.8.0") (d #t) (k 0)) (d (n "indoc") (r "^2.0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "shellexpand") (r "^3.1.0") (f (quote ("tilde" "base-0" "path"))) (k 0)))) (h "1g3zyvxdvklfd8b5y6jxjkblx2gy2rb2fhinqmb9ym2qpax5mcz4")))

