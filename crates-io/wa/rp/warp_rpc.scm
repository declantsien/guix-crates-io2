(define-module (crates-io wa rp warp_rpc) #:use-module (crates-io))

(define-public crate-warp_rpc-0.1.0 (c (n "warp_rpc") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 2)) (d (n "config") (r "^0.11") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1x7hqf3nc9ddyvqdmpkz478352znxp5f4kjavicnb28nr55lssmi")))

