(define-module (crates-io wa rp warp-prometheus) #:use-module (crates-io))

(define-public crate-warp-prometheus-0.1.0 (c (n "warp-prometheus") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.11.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.0") (d #t) (k 0)))) (h "1qfglwig1zd18cbq6vwgp1nx67k11dxvpxj3sr6sm3mhd1w40jcx")))

(define-public crate-warp-prometheus-0.2.0 (c (n "warp-prometheus") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.11.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.0") (d #t) (k 0)))) (h "1f0w5jbb6a3j3b1mckfdr0l2kzw9fjm5f1mk1v27ll6fyg4gkl1n")))

(define-public crate-warp-prometheus-0.3.0 (c (n "warp-prometheus") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.11.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.0") (d #t) (k 0)))) (h "1vwq2053bhjpi896ija13lqjnwln20hdsr9w0bbp7ch5cpq1fkdc")))

(define-public crate-warp-prometheus-0.4.0 (c (n "warp-prometheus") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.12.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.0") (d #t) (k 0)))) (h "0h6xn7ax8k670q6gxcrlr4q52krz85mllphzxjnhyksqphdx207b")))

(define-public crate-warp-prometheus-0.5.0 (c (n "warp-prometheus") (v "0.5.0") (d (list (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "prometheus") (r "^0.13.0") (d #t) (k 0)) (d (n "warp") (r "^0.3.0") (d #t) (k 0)))) (h "1894alh7rdilz94q8rlkj7fmhb2y457qzh8k35ay64419a27x9v3")))

