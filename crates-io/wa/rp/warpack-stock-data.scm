(define-module (crates-io wa rp warpack-stock-data) #:use-module (crates-io))

(define-public crate-warpack-stock-data-0.0.1 (c (n "warpack-stock-data") (v "0.0.1") (d (list (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "warpack-formats") (r "^0.0.1") (d #t) (k 0)) (d (n "warpack-formats") (r "^0.0.1") (d #t) (k 1)))) (h "1mi5m9x1mn817hh1fj91blim83z2c2k2pn4iws83harfgmqy8bi2")))

