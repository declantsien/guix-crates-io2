(define-module (crates-io wa rp warpack-cli) #:use-module (crates-io))

(define-public crate-warpack-cli-0.0.1 (c (n "warpack-cli") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "bincode") (r "^1.2.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.2") (d #t) (k 0)) (d (n "rlua") (r "^0.16.3") (d #t) (k 0)) (d (n "ron") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "warpack-core") (r "^0.0.1") (d #t) (k 0)) (d (n "warpack-formats") (r "^0.0.1") (d #t) (k 0)) (d (n "warpack-lua-parser") (r "^0.0.1") (d #t) (k 0)))) (h "0pczwfdf6n5a1j6iwmyqzl608mx1b31m1kwvgb8jl183sh2mnnk7")))

