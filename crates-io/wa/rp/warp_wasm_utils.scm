(define-module (crates-io wa rp warp_wasm_utils) #:use-module (crates-io))

(define-public crate-warp_wasm_utils-0.1.0 (c (n "warp_wasm_utils") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.56") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.29") (d #t) (k 0)))) (h "0kcw3qmb2sx760cmsvbxvnap8qm0p0ci3zb3g68i6favxli8cg7f")))

