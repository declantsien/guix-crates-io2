(define-module (crates-io wa rp warped-sampling) #:use-module (crates-io))

(define-public crate-warped-sampling-0.1.0 (c (n "warped-sampling") (v "0.1.0") (d (list (d (n "halton") (r "^0.1.0") (d #t) (k 2)) (d (n "image") (r "^0.21.1") (d #t) (k 2)) (d (n "imageproc") (r "^0.18") (d #t) (k 2)) (d (n "minifb") (r "^0.11.2") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 2)))) (h "0kkprz01a8assq8c3m1cfbb5acdd18gp0jx3qk356scp8fss7qyl")))

