(define-module (crates-io wa rp warp-filter-request) #:use-module (crates-io))

(define-public crate-warp-filter-request-0.1.0 (c (n "warp-filter-request") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1"))) (k 2)) (d (n "portpicker") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (k 0)))) (h "04bsg825pgfd91rr1jv6zpqzkzc6bq2i45zypcw0z86bl22b6jd3") (y #t)))

(define-public crate-warp-filter-request-0.1.1 (c (n "warp-filter-request") (v "0.1.1") (d (list (d (n "bytes") (r "^1.1") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1"))) (k 2)) (d (n "portpicker") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^1.15") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (k 0)))) (h "1v8h0j55am1yadw1zdsshxqif1cfw9d43nlzfi6vl2638fqlccsk")))

(define-public crate-warp-filter-request-0.2.0 (c (n "warp-filter-request") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "warp") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1"))) (k 2)) (d (n "portpicker") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "006v50qdpxa2nwl402w2ry90rmp6dxjydqpr5haaqhscblq1lg24")))

