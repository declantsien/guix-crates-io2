(define-module (crates-io wa rp warp10) #:use-module (crates-io))

(define-public crate-warp10-0.1.0 (c (n "warp10") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "itertools") (r "^0.4.19") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "0hk26dwid69915c14a0x0bdr5sqjrpxk4qan99cqg5awv1w8325j")))

(define-public crate-warp10-0.2.0 (c (n "warp10") (v "0.2.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "itertools") (r "^0.4.19") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "1g383xyy8rdxrw8mrnng9ssy3ijkh7dkih4g5sknndwmh3z23yn9")))

(define-public crate-warp10-0.3.0 (c (n "warp10") (v "0.3.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "itertools") (r "^0.4.19") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "1sswrx8kxfrcnhxvcj53ly005yi0cf6f8yvnnh8d3cgab6xmz778")))

(define-public crate-warp10-0.4.0 (c (n "warp10") (v "0.4.0") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "itertools") (r "^0.4.19") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "url") (r "^1.2.0") (d #t) (k 0)))) (h "005969r1c82r8k6a1rl8vl3zml439nal86lm30ln4vj6pf6j24qw")))

(define-public crate-warp10-0.4.1 (c (n "warp10") (v "0.4.1") (d (list (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "itertools") (r "^0.5.1") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)) (d (n "url") (r "^1.2.1") (d #t) (k 0)))) (h "1avx7d09wdkfl37a8j1fvbsbra09jd6p6y6v75c1g52gb7lslgs9")))

(define-public crate-warp10-0.4.2 (c (n "warp10") (v "0.4.2") (d (list (d (n "hyper") (r "^0.10.0") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)) (d (n "url") (r "^1.3.0") (d #t) (k 0)))) (h "10a2fp04pf4zsp7r4k5wpi7r80ad8qyfzbdzbg2jgvnvhjp6lrvy")))

(define-public crate-warp10-0.4.3 (c (n "warp10") (v "0.4.3") (d (list (d (n "hyper") (r "^0.10.4") (d #t) (k 0)) (d (n "itertools") (r "^0.5.9") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "0r4h2sa9y34hls9dllaq6p5a6snvpl6wqi2k95ckipl8fln3q5xj")))

(define-public crate-warp10-0.5.0 (c (n "warp10") (v "0.5.0") (d (list (d (n "hyper") (r "^0.10.5") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.3.2") (d #t) (k 0)) (d (n "time") (r "^0.1.36") (d #t) (k 0)) (d (n "url") (r "^1.4.0") (d #t) (k 0)))) (h "01513fk563ffa4xfjlq7vqz0c20vfbb0ggqrz48r6khsyx6vxi7x")))

(define-public crate-warp10-0.6.0 (c (n "warp10") (v "0.6.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "1bj1aag4g983795nxbv7ma444qm35ypfxbb0yzidzibmx692x5qx")))

(define-public crate-warp10-0.7.0 (c (n "warp10") (v "0.7.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "1f96j5j5srm6k1r5w9s27a5j829l906pxhk27vyf66vlg68mvk01")))

(define-public crate-warp10-0.8.0 (c (n "warp10") (v "0.8.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "04wcsfwb16pkdr7hl8x2sp7cgkcak9gkcq08lwbwv46gikdjpm67")))

(define-public crate-warp10-0.8.1 (c (n "warp10") (v "0.8.1") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-rustls") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "10432nx364yqsxayjiyhwwb24z2144rmm2zz96arkd467mh3rl9g")))

(define-public crate-warp10-0.9.0 (c (n "warp10") (v "0.9.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "0smdsq27wxrir3igjfc0zd487106k68316rh0ng72vjz49mamwny")))

(define-public crate-warp10-0.10.0 (c (n "warp10") (v "0.10.0") (d (list (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "07hcslh57zk1jahpr6jayqa1agnlyar9i6145qsf9jbwwgpqrcnd")))

(define-public crate-warp10-0.11.0 (c (n "warp10") (v "0.11.0") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "0gvd4j9ff6pakq05m1y3070rnq8rb9bgrh9957lqw77i40n9cv65")))

(define-public crate-warp10-0.11.1 (c (n "warp10") (v "0.11.1") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "049ijir1j168yqg6jx9ymclf0k90gfjgnb309a3idwhmj0c63phv")))

(define-public crate-warp10-0.11.2 (c (n "warp10") (v "0.11.2") (d (list (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0jsj8cw13m29j62wirzyg4zgcmgdp17jxjgnk6avlv8c5svv522b")))

(define-public crate-warp10-0.12.0 (c (n "warp10") (v "0.12.0") (d (list (d (n "isahc") (r "^0.9") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "031h9b3hhf550amj0h6bwz35x9x4raca2gqfmpsizii3nnbqpxqb")))

(define-public crate-warp10-0.13.0 (c (n "warp10") (v "0.13.0") (d (list (d (n "isahc") (r "^0.9") (d #t) (k 0)) (d (n "mime") (r "^0.3") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0cy1wxrj89jzg3x4aa0m8325gp4wal1xqqkaf3ml68ybjrcd4ka6")))

(define-public crate-warp10-1.0.0 (c (n "warp10") (v "1.0.0") (d (list (d (n "isahc") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "0z7gdxxknv6s5l2s61l43kkp6f4j08k4wcy97n6zzm5jh925dgd7")))

(define-public crate-warp10-1.1.0 (c (n "warp10") (v "1.1.0") (d (list (d (n "isahc") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1zrh37r9pms2f8y468v3dn2hnpa84m5fllxpy9dy5rj380g0qp1k")))

(define-public crate-warp10-1.1.1 (c (n "warp10") (v "1.1.1") (d (list (d (n "isahc") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "19pdcbi7kxj692xfnvb1n7a5xry0aikvv3dbsydnaqj9smbywy0n")))

(define-public crate-warp10-1.2.0 (c (n "warp10") (v "1.2.0") (d (list (d (n "isahc") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.2") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "1gwpxyd3zymxa9v0fykbvvkiqbgkgpgg8cqib04lxnljbsf9h3hl") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

(define-public crate-warp10-2.0.0 (c (n "warp10") (v "2.0.0") (d (list (d (n "isahc") (r "^1.1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^2.0") (d #t) (k 0)))) (h "08fnwhfmfv8lrnhj15y61p0mv0n0fxf8gwnib5sgahlakbj50nz4") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

