(define-module (crates-io wa rp warp-request-body) #:use-module (crates-io))

(define-public crate-warp-request-body-0.0.0 (c (n "warp-request-body") (v "0.0.0") (h "192c928wqz5g0qsa6lj72s3pf5b0g89cqmi46d93azss3k44rmjk") (y #t)))

(define-public crate-warp-request-body-0.1.0 (c (n "warp-request-body") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (k 0)) (d (n "warp-filter-request") (r "^0.1") (d #t) (k 2)))) (h "0ym4nxbcm90v4nhzlhf2hg33ggkx59xwvscx8is7figh8v9axnvw")))

(define-public crate-warp-request-body-0.1.1 (c (n "warp-request-body") (v "0.1.1") (d (list (d (n "bytes") (r "^1.1") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "pin-project-lite") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp") (r "^0.3") (k 0)) (d (n "warp-filter-request") (r "^0.1") (d #t) (k 2)))) (h "17biij8iidlvs3nxa5ajikbpbdyn1klqzvv2lwmvmsw0rmbbyn58")))

(define-public crate-warp-request-body-0.2.0 (c (n "warp-request-body") (v "0.2.0") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "pin-project-lite") (r "^0.2") (k 0)) (d (n "warp") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp-filter-request") (r "^0.2") (d #t) (k 2)))) (h "0r7sbn94imw961h4vw72534l819qh1h4vrhq9gwwz0a2lcnv3cx0")))

(define-public crate-warp-request-body-0.2.1 (c (n "warp-request-body") (v "0.2.1") (d (list (d (n "bytes") (r "^1") (k 0)) (d (n "futures-util") (r "^0.3") (k 0)) (d (n "hyper-body-to-bytes") (r "^0.1") (k 0)) (d (n "pin-project-lite") (r "^0.2") (k 0)) (d (n "warp") (r "^0.3") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)) (d (n "warp-filter-request") (r "^0.2") (d #t) (k 2)))) (h "1pnqmdcw7bmarifwf9ccd2dg8ph54mmpwhydcif2vl6mgqsj4a2j")))

