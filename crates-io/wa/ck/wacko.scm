(define-module (crates-io wa ck wacko) #:use-module (crates-io))

(define-public crate-wacko-0.1.0 (c (n "wacko") (v "0.1.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)))) (h "09iq809vz4sx1ix7mx6fxj5askqfn5w2g9pi17dfnszwkgxnd124")))

(define-public crate-wacko-0.1.1 (c (n "wacko") (v "0.1.1") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)))) (h "0a0scgbmw7qcagjn9jwsyws1kk266n8casanm79ms443sfhifw0f")))

(define-public crate-wacko-0.3.0 (c (n "wacko") (v "0.3.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)))) (h "1qjhz0gfyi4ks2fc0c7yxvipckp4p24rhfk76vrjqwhcx1qhl40r")))

(define-public crate-wacko-0.4.0 (c (n "wacko") (v "0.4.0") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)))) (h "12d64w941vibwdlgi2nx8604adw52h3ywaw4ba0b3c7imr9x3x5b")))

