(define-module (crates-io wa ck wacker-api) #:use-module (crates-io))

(define-public crate-wacker-api-0.1.0 (c (n "wacker-api") (v "0.1.0") (d (list (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "0vlddg2641qidzxlrblz947a6a7257rpdkksqhxvbr90fb90hrcl") (y #t)))

(define-public crate-wacker-api-0.1.1 (c (n "wacker-api") (v "0.1.1") (d (list (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "0bljjga6svkkwr9i680f4c2z1dy6xrdf2p0jjv6kbnlxd1dvh8g7") (y #t)))

(define-public crate-wacker-api-0.1.2 (c (n "wacker-api") (v "0.1.2") (d (list (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "134d0anfkccpdqpn37s77674b43hz9zlw0l46kmy8xycix116ya1") (y #t)))

(define-public crate-wacker-api-0.1.3 (c (n "wacker-api") (v "0.1.3") (d (list (d (n "const_format") (r "^0.2.32") (d #t) (k 0)) (d (n "prost") (r "^0.12.3") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "1i88czr993xslwnyx75swxvipvi2jzy7da98z5fm3z60phgyzxv9") (y #t)))

