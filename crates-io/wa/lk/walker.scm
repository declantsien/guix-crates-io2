(define-module (crates-io wa lk walker) #:use-module (crates-io))

(define-public crate-walker-1.0.0 (c (n "walker") (v "1.0.0") (h "0466m9a7wr0mgkk7bw34mmnhngnmrdb5x3v8pa43bb4qn618nj60")))

(define-public crate-walker-1.0.1 (c (n "walker") (v "1.0.1") (h "0sv482pvck520h77752a22pbrrqh76yhw9mnzx6r1xz4b9g1v5s4")))

