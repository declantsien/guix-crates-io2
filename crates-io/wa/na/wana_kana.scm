(define-module (crates-io wa na wana_kana) #:use-module (crates-io))

(define-public crate-wana_kana-0.9.0 (c (n "wana_kana") (v "0.9.0") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "stainless") (r "^0.1.12") (d #t) (k 2)))) (h "04gxwqnn2zgqdsvxmkhzigr5nfwc0379f7wkmakd1gq8rfz5nfkm")))

(define-public crate-wana_kana-0.9.1 (c (n "wana_kana") (v "0.9.1") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "stainless") (r "^0.1.12") (d #t) (k 2)))) (h "1za4a54xcfx2xrb0738m24z67mi0vxsn4ylix1gxsqlxjparzzdq")))

(define-public crate-wana_kana-0.9.2 (c (n "wana_kana") (v "0.9.2") (d (list (d (n "itertools") (r "^0.7.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.21") (d #t) (k 0)) (d (n "phf_macros") (r "^0.7.21") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "stainless") (r "^0.1.12") (d #t) (k 2)))) (h "1wwnhkr13vrw3ij6yqy7jchdajrdvff24j35kwpganara6wl9phi")))

(define-public crate-wana_kana-0.9.3 (c (n "wana_kana") (v "0.9.3") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0.2") (d #t) (k 0)) (d (n "stainless2") (r "^0.1.12") (d #t) (k 2)))) (h "14wrgw63zppyb89rhqh2zdsbpbfmglxfifw5mz84vk95bx1rsn2a")))

(define-public crate-wana_kana-1.0.0 (c (n "wana_kana") (v "1.0.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "1klfvxywj3kq2vlzv9lmys5arjwy89caf7sf0pi4qphsz2qjhxpq")))

(define-public crate-wana_kana-2.0.0 (c (n "wana_kana") (v "2.0.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "1xalq0qfagqrzdmf30p7qxbfjpyzvxxa23fkz0i8vcz6kppk0zqs") (f (quote (("enable_regex" "regex") ("default"))))))

(define-public crate-wana_kana-2.0.1 (c (n "wana_kana") (v "2.0.1") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "14lvj1hd64y739bsy8plj31bqncpd536p8nbir9yn4fhc7724xbk") (f (quote (("enable_regex" "regex") ("default"))))))

(define-public crate-wana_kana-2.1.0 (c (n "wana_kana") (v "2.1.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "004c7lj4rmih6mnl1003ggjsys55xn52jmjpqm2kszhbw00v88jl") (f (quote (("enable_regex" "regex") ("default"))))))

(define-public crate-wana_kana-2.1.1 (c (n "wana_kana") (v "2.1.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "030m6d0v8mri9gz8jk0ics6w89wn429k8w87yc58s9wgn2zldk6c") (f (quote (("tokenize" "itertools") ("enable_regex" "regex") ("default")))) (y #t)))

(define-public crate-wana_kana-2.1.2 (c (n "wana_kana") (v "2.1.2") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (o #t) (d #t) (k 0)) (d (n "speculate") (r "^0.1.2") (d #t) (k 2)))) (h "0m5s5pw8c06b0zvx9y8mazpn2y1xfqv0lbv0xr5xdmn9l6az6w2p") (f (quote (("tokenize" "itertools") ("enable_regex" "regex") ("default" "tokenize"))))))

(define-public crate-wana_kana-3.0.0 (c (n "wana_kana") (v "3.0.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (o #t) (d #t) (k 0)))) (h "02y3s5zqm5ykbd68902r9p6qppnj137a5xaxg4ab1dvgqnjpcya7") (f (quote (("tokenize" "itertools") ("enable_regex" "regex") ("default" "tokenize"))))))

