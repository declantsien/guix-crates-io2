(define-module (crates-io wa ss wassily) #:use-module (crates-io))

(define-public crate-wassily-0.1.0 (c (n "wassily") (v "0.1.0") (d (list (d (n "color-thief") (r "^0.2") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "noise") (r "^0.8") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "rand_distr") (r "^0.4") (d #t) (k 0)) (d (n "tiny-skia") (r "^0.11") (d #t) (k 0)))) (h "1faaksvmvni2348pi4s97p8g69xln4982nwgx2ianw7kz60ws6s0")))

