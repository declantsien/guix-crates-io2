(define-module (crates-io wa yb wayback) #:use-module (crates-io))

(define-public crate-wayback-0.1.0 (c (n "wayback") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0k0np8525vg1lfmldcbwb1hf91rqhcra3kc50v1g2hgrr4c2y1y9")))

(define-public crate-wayback-0.1.1 (c (n "wayback") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "1mam9dby6pplz4qdaydgmfnprv077vjffjsk51gcrddwhdgjni31")))

(define-public crate-wayback-0.2.0 (c (n "wayback") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "142pf107kcdr35179jahg9dn6kmfb921h8gr48kjfiglmqns38n4")))

(define-public crate-wayback-0.2.1 (c (n "wayback") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0dkxax9n1mfaagcbv4w96vlqnb4i9k5j6ik7i50sbrqnpbln5h0l")))

(define-public crate-wayback-0.3.1 (c (n "wayback") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.66") (d #t) (k 0)))) (h "0zj4gqii9ca2sm2rd5mkhwm9ykfp6p6s5saxr6vd0iin369di5m1")))

