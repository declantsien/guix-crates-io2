(define-module (crates-io wa yb waybar_weather_display) #:use-module (crates-io))

(define-public crate-waybar_weather_display-0.2.0 (c (n "waybar_weather_display") (v "0.2.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1x6ynmvmxj8cys6wzd31y5a8ys7khxpg4xa468lr8c3aw4g2ad6p") (r "1.62.1")))

(define-public crate-waybar_weather_display-0.2.1 (c (n "waybar_weather_display") (v "0.2.1") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1qhvzk5vlzcm2sh79n9vvr0bs5a31y15adl8basfm9bkd59ihs47") (r "1.62.1")))

(define-public crate-waybar_weather_display-0.2.2 (c (n "waybar_weather_display") (v "0.2.2") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1lrbgl37m0f6bsbqhbbac4fw5aw77nwzhqya77h6rp0nh71s8lw2") (r "1.62.1")))

(define-public crate-waybar_weather_display-0.2.3 (c (n "waybar_weather_display") (v "0.2.3") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "095v1wj7sz6kgr6za4nzvn29wc79xx6lh2n0c2jlyi5qfg1bcs5s") (r "1.62.1")))

(define-public crate-waybar_weather_display-0.2.4 (c (n "waybar_weather_display") (v "0.2.4") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "13j67yylshgp6h2vfxs97yv47hb9x9j26swidbsv5l0mc5hlybap") (r "1.62.1")))

(define-public crate-waybar_weather_display-0.3.0 (c (n "waybar_weather_display") (v "0.3.0") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "1ri4970bd5ab9q1y3rhr7s9m62jxmsp4hllj5zpfyznd86cba28b") (r "1.62.1")))

