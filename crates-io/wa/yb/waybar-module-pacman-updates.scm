(define-module (crates-io wa yb waybar-module-pacman-updates) #:use-module (crates-io))

(define-public crate-waybar-module-pacman-updates-0.1.0 (c (n "waybar-module-pacman-updates") (v "0.1.0") (h "13zldxc74f2s7rxs1scs0c5v2cm36fq9rrhwhrjx3cr7j3wlq2bq")))

(define-public crate-waybar-module-pacman-updates-0.2.0 (c (n "waybar-module-pacman-updates") (v "0.2.0") (h "1hqdkws12kxq7sb2pjw86jdq2bk0nv3dl1m6a3izj0s06brwbzzb")))

(define-public crate-waybar-module-pacman-updates-0.2.1 (c (n "waybar-module-pacman-updates") (v "0.2.1") (h "0wzpdnkszq8ym4gnw1pa6pfk8d60j7gy3xw2z1sbxld51sjpjs44")))

(define-public crate-waybar-module-pacman-updates-0.2.2 (c (n "waybar-module-pacman-updates") (v "0.2.2") (h "0cp4w9rp8z40zy5sfzq92fqc4m1y27a9f6x4dqw643cyp5mhsmcg")))

(define-public crate-waybar-module-pacman-updates-0.2.4 (c (n "waybar-module-pacman-updates") (v "0.2.4") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0666hs2y989lrq92nhvg639cvnh30lczk029fd6bmardlfk6jzk0")))

(define-public crate-waybar-module-pacman-updates-0.2.5 (c (n "waybar-module-pacman-updates") (v "0.2.5") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1qzvnafdji1gfq4j9wf71vbslc5s9rcbrgsix2b0c50p7sdh97j7")))

