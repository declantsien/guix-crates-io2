(define-module (crates-io wa yb waybar_media_display) #:use-module (crates-io))

(define-public crate-waybar_media_display-0.1.0 (c (n "waybar_media_display") (v "0.1.0") (h "09ix74m7a7dpr10sr3dvlyr4y7iwb8s7p36hlsicahzkfhagqj3q") (r "1.62.1")))

(define-public crate-waybar_media_display-1.0.0 (c (n "waybar_media_display") (v "1.0.0") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j2afi3c6adb5lxnzbjv5ldbm2pwcwp0jr04hihzw83sj0za9f0k") (r "1.62.1")))

(define-public crate-waybar_media_display-1.0.1 (c (n "waybar_media_display") (v "1.0.1") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jdhhamkqx9gpl25ihdhhmdp1qhwlkahqvjcgjhc8vcgmxqjmwdy") (r "1.62.1")))

(define-public crate-waybar_media_display-1.0.2 (c (n "waybar_media_display") (v "1.0.2") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f3d6ip8l4knn08a0smlmymvzzm2y85yd2sjrnxq1lqjq2d283qa") (r "1.62.1")))

(define-public crate-waybar_media_display-1.0.3 (c (n "waybar_media_display") (v "1.0.3") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "077l6dximv4nn3rbxhd30064wsah3kc3p18kfqvkjsbipj72j69m") (r "1.62.1")))

(define-public crate-waybar_media_display-1.0.4 (c (n "waybar_media_display") (v "1.0.4") (d (list (d (n "clap") (r "^3.2.15") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q1v6ypqs50rk3v08hw3k9qvj9sw7nf6sl8p8r4c00z969dsczfp") (r "1.62.1")))

(define-public crate-waybar_media_display-1.0.5 (c (n "waybar_media_display") (v "1.0.5") (d (list (d (n "clap") (r "^3.2.16") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f99bgd7bb4izghzm0awm75bbinsxack2pxr22xs433c384ril6r") (r "1.62.1")))

