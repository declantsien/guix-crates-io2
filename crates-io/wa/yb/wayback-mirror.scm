(define-module (crates-io wa yb wayback-mirror) #:use-module (crates-io))

(define-public crate-wayback-mirror-0.1.0 (c (n "wayback-mirror") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (f (quote ("h1-client-rustls" "encoding"))) (k 0)))) (h "1qvmdxp3l4ngn60vkh67n93xd9vn2v4rayan0jmp6r3nv9karqin")))

