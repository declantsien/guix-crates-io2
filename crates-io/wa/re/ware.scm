(define-module (crates-io wa re ware) #:use-module (crates-io))

(define-public crate-ware-0.1.0 (c (n "ware") (v "0.1.0") (h "12fmm5lrj8iapd8a4g3yzj8j0az7jx2v8py6vmg02myl5gqwkfam")))

(define-public crate-ware-1.0.0 (c (n "ware") (v "1.0.0") (h "1xnxi9micqjwr8rz5bq52i32gi4sjj210nkdzaqvn01cppiiwmg2")))

(define-public crate-ware-2.0.0-rc1 (c (n "ware") (v "2.0.0-rc1") (h "1bjvj4ybkc6iyifvhskncc690dlk68pfcpv9669kh8nqd2d4l8ph")))

(define-public crate-ware-2.0.0 (c (n "ware") (v "2.0.0") (h "0q7klx652wnfqh5g87gz7gwb24zzkla5wb0a96ly39n86vz39k1x")))

(define-public crate-ware-2.0.1 (c (n "ware") (v "2.0.1") (h "0d69wndl9pg9p1jbbrzydf51f5s6k15lshdbrwg7qmw7b5p0nqhw")))

(define-public crate-ware-2.0.2 (c (n "ware") (v "2.0.2") (h "0y3f8692j7g8mbrmak5vdzsidd9rkgykpmbwmg1li124iy6c7j7b")))

