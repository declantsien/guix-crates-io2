(define-module (crates-io wa re wareki-conv) #:use-module (crates-io))

(define-public crate-wareki-conv-0.1.0 (c (n "wareki-conv") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "unicode-jp") (r "^0.4.0") (d #t) (k 0)))) (h "0gsc4f5v31xan874l6kphm7rcm9c9zyhh2ckwfl0li22i077iar8")))

(define-public crate-wareki-conv-0.2.0 (c (n "wareki-conv") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "unicode-jp") (r "^0.4.0") (d #t) (k 0)))) (h "1g4nb7klva2gyc1p5mawm02gxi6k2n1l1m84107s3xchxw5am3xp")))

(define-public crate-wareki-conv-0.2.1 (c (n "wareki-conv") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.34") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "unicode-jp") (r "^0.4.0") (d #t) (k 0)))) (h "19anv8p5clk4pzc23r10jz8w2fhr4w40hq5dnqcz471z6j259lhf")))

