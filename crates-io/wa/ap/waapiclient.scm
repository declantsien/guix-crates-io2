(define-module (crates-io wa ap waapiclient) #:use-module (crates-io))

(define-public crate-waapiclient-0.1.0 (c (n "waapiclient") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "186bmq1xi2yjy8qc91bfggmpw7kwxizi50vl0jlds9l1lf7ykcc0")))

(define-public crate-waapiclient-0.1.1 (c (n "waapiclient") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1yb5hkm1lshz95qdykqf5ji0fa17j3z103sgjm98wmkd8xg6pfvi")))

(define-public crate-waapiclient-0.1.2 (c (n "waapiclient") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0276kfla4z70vgnl53wnjv4ihh49kg7af13crdqyfnf3d3axji4k")))

(define-public crate-waapiclient-0.1.3 (c (n "waapiclient") (v "0.1.3") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0mny6ilqwnm1hxyrcss3q11cvcf1rvw1ahhgv2hh50nldh35qm8a")))

(define-public crate-waapiclient-0.1.4 (c (n "waapiclient") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0g61f1clrrz41m4w1fk9f9vwvys5pdj8czcbf043hrljiqzw02s1")))

(define-public crate-waapiclient-0.1.5 (c (n "waapiclient") (v "0.1.5") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "07r6ncly0m542gaqyiwz84bzjzvc7kfspk9csbdi13smdwxd4yp6")))

(define-public crate-waapiclient-0.2.0 (c (n "waapiclient") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "12faj3ymhi5664kkfcwjxc5hx9cf7hyg33vrxhxq5h3x051p395j")))

(define-public crate-waapiclient-0.2.1 (c (n "waapiclient") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0hkpnljnm98r8vsqxiffbcls58p9lph1kcnyxf6dy6bfr6ip838i")))

(define-public crate-waapiclient-0.2.2 (c (n "waapiclient") (v "0.2.2") (d (list (d (n "reqwest") (r "^0.11.22") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^3.3.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1qlgdvmdiy6xcsgpl4mw01xlqz25dqaa9lfd1i1ly70xc8mfw5mg")))

