(define-module (crates-io wa ve waver) #:use-module (crates-io))

(define-public crate-waver-0.0.1 (c (n "waver") (v "0.0.1") (d (list (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0a7h2r3bgi74lkq12qnhghh83rzndhl445z7iw79vsgsjzd3m4v1") (y #t)))

(define-public crate-waver-0.1.0 (c (n "waver") (v "0.1.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 0)) (d (n "libm") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "05inxx6kqiapx10szgqpx4sayk3q0jwndgn06shz54w5my8rqh5i")))

(define-public crate-waver-0.2.0 (c (n "waver") (v "0.2.0") (d (list (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "libm") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0s7n64jyyilhnvcqjvkbflzb971qk60bjdzv2cr9n91a085msprw")))

