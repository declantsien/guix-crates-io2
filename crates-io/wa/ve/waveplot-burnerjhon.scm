(define-module (crates-io wa ve waveplot-burnerjhon) #:use-module (crates-io))

(define-public crate-waveplot-burnerjhon-0.2.0 (c (n "waveplot-burnerjhon") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (f (quote ("all-widgets"))) (d #t) (k 0)) (d (n "vcd") (r "^0.7.0") (d #t) (k 0)))) (h "0q7pz2fahr1ayx9zy4knxkkagf75nd3c5v76vbfmpx5a7nldvisr")))

