(define-module (crates-io wa ve wavefc) #:use-module (crates-io))

(define-public crate-wavefc-3.0.0 (c (n "wavefc") (v "3.0.0") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)))) (h "0l9kdfarx8dqklm526pcy8rl30z7j9v7bf0d6lfknqijwhr5g6y4")))

(define-public crate-wavefc-3.1.1 (c (n "wavefc") (v "3.1.1") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (o #t) (d #t) (k 0)))) (h "057cjplb63f795ayj33jqn0yv0dkzhbic7rqm469435pkh03dcld") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wavefc-3.1.2 (c (n "wavefc") (v "3.1.2") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (o #t) (d #t) (k 0)))) (h "00i3l019rsykffzn3ksxv4mf9916824r3vlrvrv1gssgib76y9mj") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wavefc-3.1.3 (c (n "wavefc") (v "3.1.3") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (o #t) (d #t) (k 0)))) (h "031izpb8yq1h818dm5q3s8ndfm6ns7zznkxb992lzz1f19jrp9bh") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wavefc-3.1.4 (c (n "wavefc") (v "3.1.4") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (o #t) (d #t) (k 0)))) (h "100kk4rbxnls1nxq6dbbng4dd65cc95r7yqy6l8ljh1ny63bmzlz") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wavefc-3.1.5 (c (n "wavefc") (v "3.1.5") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (o #t) (d #t) (k 0)))) (h "1pnqihpfqh8jwqy4dhk2zgv9mbvbwc40wpazp6zy3mvm21ipw12z") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-wavefc-3.1.6 (c (n "wavefc") (v "3.1.6") (d (list (d (n "cgmath") (r "^0.18.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (o #t) (d #t) (k 0)))) (h "058bzhmgf1s4wwf7glcn92c8gf89x1gw8kcb277ckj9q5k20sznb") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

