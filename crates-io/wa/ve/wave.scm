(define-module (crates-io wa ve wave) #:use-module (crates-io))

(define-public crate-wave-0.0.0 (c (n "wave") (v "0.0.0") (h "1445ylk9ggr4vm3g75xbh99w4j1x5hcn3lczmndg3bgglfwcq5wz")))

(define-public crate-wave-0.1.0 (c (n "wave") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)))) (h "0vxffixqfrzq1ib8ylwh4v2sz8dmqgxmk9ljnrg5z85lycrcbf4a") (y #t)))

(define-public crate-wave-0.1.1 (c (n "wave") (v "0.1.1") (d (list (d (n "libm") (r "^0.2.5") (d #t) (k 0)) (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "1pfj4c84cn9lsk6nmdyq91l8gri2gj5h7nsyhhsk90j42xfxxrig")))

