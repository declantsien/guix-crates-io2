(define-module (crates-io wa ve waveforms-sdk) #:use-module (crates-io))

(define-public crate-waveforms-sdk-0.0.1 (c (n "waveforms-sdk") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "uom") (r "^0.31") (d #t) (k 0)))) (h "0w3yjm9wclz93gq38i24aslfidi9r800vm03cs1lfmk85rhriybc")))

