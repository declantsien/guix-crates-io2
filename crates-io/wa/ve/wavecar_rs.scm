(define-module (crates-io wa ve wavecar_rs) #:use-module (crates-io))

(define-public crate-wavecar_rs-0.1.0 (c (n "wavecar_rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "fftw") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.1") (f (quote ("rayon" "approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.12.1") (f (quote ("openblas"))) (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3.1") (d #t) (k 0)) (d (n "vasp-poscar") (r "^0.3.2") (d #t) (k 0)) (d (n "vaspchg_rs") (r "^0.1.1") (d #t) (k 0)))) (h "0zwmc72q37r5xa3msfw4lr4amwhl2lvjrscdqbgla1jhxncwggf6")))

