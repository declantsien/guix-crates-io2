(define-module (crates-io wa ve wavefront_rs) #:use-module (crates-io))

(define-public crate-wavefront_rs-0.1.0 (c (n "wavefront_rs") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1hsgpsm9qa3g0nf4y0c6nbc2shyd46v8wrha3rlmsllj3yk3abxr")))

(define-public crate-wavefront_rs-0.1.1 (c (n "wavefront_rs") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)))) (h "1v6anjbpj0iy5bnrc5qy8fvjhagfxd7gmjncbvmsidk6njsgld8b")))

(define-public crate-wavefront_rs-0.1.2 (c (n "wavefront_rs") (v "0.1.2") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0gh5vj9bnn3yg9cm0y3ic33ck76pq5qkqwkqiypdd8hkl2fbgjvf")))

(define-public crate-wavefront_rs-0.1.3 (c (n "wavefront_rs") (v "0.1.3") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "1n4is60gk5am91rqxicycmlzw8s3c0pjc6s388jw8sqq3xq7crah")))

(define-public crate-wavefront_rs-0.1.4 (c (n "wavefront_rs") (v "0.1.4") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0npd8psa2pmfkphnjjwpvk8nfaypy7ck4a5p8n6c5acgx8737k3w")))

(define-public crate-wavefront_rs-0.1.5 (c (n "wavefront_rs") (v "0.1.5") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0mlng0jwnnz871v7f1mb5wkrn471171hva921yjz8zzsjkfyfkmx")))

(define-public crate-wavefront_rs-0.2.0 (c (n "wavefront_rs") (v "0.2.0") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "1wmz9mp2f5cpccbwprclsh3vahz1kakild7vlwqmq6f1hm31hnk5")))

(define-public crate-wavefront_rs-0.3.0 (c (n "wavefront_rs") (v "0.3.0") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0f8cwmgfc61jf2g7xag0kqjmx6cqg72lqnkh6xa156gxnb5x3va3")))

(define-public crate-wavefront_rs-0.3.1 (c (n "wavefront_rs") (v "0.3.1") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0w0mmh975cq8bzlg3v4i77c34vx19nn334lxiwl6qpdla63ghz9s")))

(define-public crate-wavefront_rs-0.4.0 (c (n "wavefront_rs") (v "0.4.0") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0zklb4cgmjv2g4gn4mkxzbw0vlif276jd7wgapwzs1vzm4pjyjm4")))

(define-public crate-wavefront_rs-0.4.1 (c (n "wavefront_rs") (v "0.4.1") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "1193pisg0z4ldrh8y6sinbmlvbpkk0v84sx33q2fi6vprbyxpxzd")))

(define-public crate-wavefront_rs-0.4.2 (c (n "wavefront_rs") (v "0.4.2") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0258niwailv69qmcz7mvh8s6cikqvg2kqz2zkzk0qch4gxqr8pmn")))

(define-public crate-wavefront_rs-0.4.3 (c (n "wavefront_rs") (v "0.4.3") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 2)))) (h "0qqizi9hyc9km2qpvw9i8dsnvr4qa1jjpfjcr2vz4vxm18my1pj7")))

(define-public crate-wavefront_rs-0.4.4 (c (n "wavefront_rs") (v "0.4.4") (h "0yk467pbaj5nkd3zby4dgapn2lnwvyq178jlsc5sv3sxdinmvls4")))

(define-public crate-wavefront_rs-0.4.5 (c (n "wavefront_rs") (v "0.4.5") (h "1wg5rdwzjl5rfj2wyi3lppazcbwapwixv7aarkc163mdivnlzybs")))

(define-public crate-wavefront_rs-0.4.6 (c (n "wavefront_rs") (v "0.4.6") (h "1lq7p04n362bna31khinwa7iq9g48d15xay7dx31145l9vl4cvw1")))

(define-public crate-wavefront_rs-0.4.7-f1208fc (c (n "wavefront_rs") (v "0.4.7-f1208fc") (h "03qbka2lfp796gbf66af3m5qdcyak1xisglw3mv6p6qwwyw38jjf")))

(define-public crate-wavefront_rs-0.4.8-9f9991e (c (n "wavefront_rs") (v "0.4.8-9f9991e") (h "045ah8av3s8mbbbckxd8sm00clyvxx92g2p7br5icanpj2iasnik")))

(define-public crate-wavefront_rs-0.4.9 (c (n "wavefront_rs") (v "0.4.9") (h "06n4q8jlpxda9g0242irm82s7q4cs9b45jfy9ai8nv1d4q8mgmd9")))

(define-public crate-wavefront_rs-0.4.10 (c (n "wavefront_rs") (v "0.4.10") (h "0l6wnx11h55f1dsaablhyymprjaihnj2cjfpnkn4hwsyzkjxl1y1")))

(define-public crate-wavefront_rs-0.4.11 (c (n "wavefront_rs") (v "0.4.11") (h "0jhvsps1hhjglhgrg63lp4bn41qr0b9dpm08k7z15zm54587nsq7")))

(define-public crate-wavefront_rs-1.0.0 (c (n "wavefront_rs") (v "1.0.0") (h "09kjwwv4fygis4l447vv1shrhad3mmjfc9hy65nznjazgy34i5y8")))

(define-public crate-wavefront_rs-1.0.1 (c (n "wavefront_rs") (v "1.0.1") (h "1jcrmbfqjhnc8yxhhbbxzvlaqsji9n02si3yyqxkghyrr8rjpr63")))

(define-public crate-wavefront_rs-1.0.2 (c (n "wavefront_rs") (v "1.0.2") (h "0yv45akay13za3vaqxakvfwjlrnvxkpz06j5wq6bk3nxmfj23bk6")))

(define-public crate-wavefront_rs-1.0.3 (c (n "wavefront_rs") (v "1.0.3") (h "0yiildbf5c6k9wrxr1pgf0745p6yfb45zvrl2ms3jf36jca4652d")))

(define-public crate-wavefront_rs-1.0.4 (c (n "wavefront_rs") (v "1.0.4") (h "1c7cnphzy1jzfka9pw0h6hy3j0c3mca5visrqxlsx1kqxw1imw1c")))

(define-public crate-wavefront_rs-2.0.0-alpha.1 (c (n "wavefront_rs") (v "2.0.0-alpha.1") (h "18g914360nj0nry8jl7n3qnzsfq4z4a1f3dklvxqlpg3kvsfhsmd")))

(define-public crate-wavefront_rs-2.0.0-beta.1 (c (n "wavefront_rs") (v "2.0.0-beta.1") (h "0c4r3l5772hzwbf2vxivh9dgs54mg0c6kq9ycg69qgqw4zi3gwn2")))

