(define-module (crates-io wa ve wavefront) #:use-module (crates-io))

(define-public crate-wavefront-0.1.0 (c (n "wavefront") (v "0.1.0") (h "12akfl202nl48i71zzfr7x9jik0yy2y7b1pwvmq2hj080f5xv7ys")))

(define-public crate-wavefront-0.1.1 (c (n "wavefront") (v "0.1.1") (h "00yfk38337av84vfqq8cnm2g29qcp9h7f18lnzsrwrmcp5rry7ah")))

(define-public crate-wavefront-0.1.2 (c (n "wavefront") (v "0.1.2") (h "0lzajx9n37yhqm3242f8h5rhq9rcxaa25fw28ngrzssifnd9n8gx")))

(define-public crate-wavefront-0.1.3 (c (n "wavefront") (v "0.1.3") (h "02cs9aay4zbb1vg6jpacc3bwi7sshwf17fb8c105xx8am9adm1dm")))

(define-public crate-wavefront-0.2.0 (c (n "wavefront") (v "0.2.0") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "0m60bfsmd8j5cjjpq89p2cv60s5clm1f2l2afsrjx5cnyap6pnfy") (f (quote (("std") ("default" "std"))))))

(define-public crate-wavefront-0.2.1 (c (n "wavefront") (v "0.2.1") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "0qc8ir6p2j5v0gz2896p4djxclakcdcz8bnwpi7rqwlavx8hby57") (f (quote (("std") ("default" "std"))))))

(define-public crate-wavefront-0.2.2 (c (n "wavefront") (v "0.2.2") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "0ll654wqpm8slna4lr6wyamyp2y9j1bmwiyhc65cf75xsmn7x6r4") (f (quote (("std") ("default" "std"))))))

(define-public crate-wavefront-0.2.3 (c (n "wavefront") (v "0.2.3") (d (list (d (n "hashbrown") (r "^0.9") (d #t) (k 0)))) (h "1yy40d5nrj21p9pg6d6sqd5qgh57qahmfay0gdxv0y7a687ijqh1") (f (quote (("std") ("default" "std"))))))

