(define-module (crates-io wa ve waverly) #:use-module (crates-io))

(define-public crate-waverly-0.1.0 (c (n "waverly") (v "0.1.0") (d (list (d (n "binrw") (r "^0.8.4") (d #t) (k 0)))) (h "0amd1k23x9x243m2mw8ccmqlgzqhmhsjcfaak71chiw2d5r5k1az")))

(define-public crate-waverly-0.2.0 (c (n "waverly") (v "0.2.0") (d (list (d (n "binrw") (r "^0.8.4") (k 0)))) (h "1zimfrwsmlb3wk53j5k6rzfypmkp51msgpq3h2l73s30cb2541w9") (f (quote (("std" "binrw/std") ("default"))))))

