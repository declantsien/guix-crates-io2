(define-module (crates-io wa ve wave_func_collapse) #:use-module (crates-io))

(define-public crate-wave_func_collapse-0.1.0 (c (n "wave_func_collapse") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tikv-jemallocator") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1wih1mjia8s9rzwf3s0b3349isqry31kn2h7x8l1i259w1h635kh") (f (quote (("default")))) (s 2) (e (quote (("jemalloc" "dep:tikv-jemallocator"))))))

