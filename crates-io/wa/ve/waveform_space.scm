(define-module (crates-io wa ve waveform_space) #:use-module (crates-io))

(define-public crate-waveform_space-0.1.0 (c (n "waveform_space") (v "0.1.0") (d (list (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.1") (d #t) (k 0)) (d (n "stream-dct") (r "^0.1.0") (d #t) (k 0)))) (h "19g4z521jb7nk1hgmi8ysszc2xlbifc7mnp4816lwgj210alfkch")))

(define-public crate-waveform_space-0.1.1 (c (n "waveform_space") (v "0.1.1") (d (list (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.1") (d #t) (k 0)) (d (n "stream-dct") (r "^0.1.0") (d #t) (k 0)))) (h "10yabh6r2lxk3ggfam8xbm1xf730zh54m9ncx0x0ghcg7hmfh2s3")))

(define-public crate-waveform_space-0.1.2 (c (n "waveform_space") (v "0.1.2") (d (list (d (n "num") (r "^0.1.37") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rustfft") (r "^1.0.1") (d #t) (k 0)) (d (n "stream-dct") (r "^0.1.0") (d #t) (k 0)))) (h "0c68za77zmp1pkn8apkgmklw2j35qbv1xzcfcf6x9rfhl1npimsk")))

