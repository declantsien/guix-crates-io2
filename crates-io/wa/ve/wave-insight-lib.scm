(define-module (crates-io wa ve wave-insight-lib) #:use-module (crates-io))

(define-public crate-wave-insight-lib-0.1.0 (c (n "wave-insight-lib") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f8rhfifpb3cmzywma1g14kmipbqn645n7v0apymmxni32333fya") (f (quote (("default"))))))

(define-public crate-wave-insight-lib-0.1.1 (c (n "wave-insight-lib") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1ip236nkbaj2wy0hjnfk7mzr4byr5gg7q5r020z62qk8ydnpb4vc") (f (quote (("default"))))))

(define-public crate-wave-insight-lib-0.1.2 (c (n "wave-insight-lib") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ia6rxrdwpk2qbiygpd6jzn876xh372va939jk7zj8nzk6kllfra") (f (quote (("default"))))))

(define-public crate-wave-insight-lib-0.1.3 (c (n "wave-insight-lib") (v "0.1.3") (d (list (d (n "bincode") (r "^2.0.0-rc.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.136") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0sap4xv8ah7mjjxykzym5xngri21xmq0r1zn6jc06hzix22a28rk") (f (quote (("default"))))))

