(define-module (crates-io wa ve wave_stream) #:use-module (crates-io))

(define-public crate-wave_stream-0.1.0 (c (n "wave_stream") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1sy3b0mlqz73cv4m7ldr0nbc5f7nb7na6yafm79i0i378zc466aq")))

(define-public crate-wave_stream-0.2.0 (c (n "wave_stream") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "0kcfrzfkiql6v85rksvkpiksjshsb223i397rjfsgl0nk0b71d87") (y #t)))

(define-public crate-wave_stream-0.2.1 (c (n "wave_stream") (v "0.2.1") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "18d2rcszkhgqdkk4wlb9cfhjh0p3nd0gkzij209h1kjyfqc8m0h9")))

(define-public crate-wave_stream-0.3.0 (c (n "wave_stream") (v "0.3.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1vxr5ipglx9yp2wswcljy1jyklhigv5irwj47hmxad775swgmjyg")))

(define-public crate-wave_stream-0.4.0 (c (n "wave_stream") (v "0.4.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1d5nankkagysi7i1jkmr646pq2cdw9gz98xv9ca22044wxlmlmv7")))

(define-public crate-wave_stream-0.5.0 (c (n "wave_stream") (v "0.5.0") (d (list (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "test-case") (r "^3.0.0") (d #t) (k 2)))) (h "1400b22wbpjqpya0r6vxx2bx84rianfmycqngh0x01rhk8gj2j08")))

