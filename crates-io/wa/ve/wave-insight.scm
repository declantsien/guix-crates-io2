(define-module (crates-io wa ve wave-insight) #:use-module (crates-io))

(define-public crate-wave-insight-0.1.0 (c (n "wave-insight") (v "0.1.0") (h "1zc1xild1igf20n75c4r6ldj3k3ca9v503cy6jndsfb3bp7qlmfm")))

(define-public crate-wave-insight-0.1.1 (c (n "wave-insight") (v "0.1.1") (d (list (d (n "wave-insight-lib") (r "^0.1.2") (d #t) (k 0)))) (h "0ibqhhs52kniryx8czj00bcbcvm16lafscsj43299gf6i4rmxibb")))

