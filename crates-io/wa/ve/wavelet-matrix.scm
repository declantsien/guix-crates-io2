(define-module (crates-io wa ve wavelet-matrix) #:use-module (crates-io))

(define-public crate-wavelet-matrix-0.1.0 (c (n "wavelet-matrix") (v "0.1.0") (d (list (d (n "succinct") (r "^0.4.2") (d #t) (k 0)))) (h "0acjkqklcqr8wn2zap40ydnlxll8606wr9nkql8dc2ljwplgsgdl")))

(define-public crate-wavelet-matrix-0.2.0 (c (n "wavelet-matrix") (v "0.2.0") (d (list (d (n "succinct") (r "^0.4.2") (d #t) (k 0)))) (h "1vxnq8i9hidm7bi3pcdg7v3kc1zhclw5kfmhb4nx436avkh7sk3g")))

(define-public crate-wavelet-matrix-0.3.0 (c (n "wavelet-matrix") (v "0.3.0") (d (list (d (n "succinct") (r "^0.4.2") (d #t) (k 0)))) (h "1p5war12afr4cd3z1kccjwzaa5apg34fdipf8z4d6vkgbz5sbmgp")))

(define-public crate-wavelet-matrix-0.4.0 (c (n "wavelet-matrix") (v "0.4.0") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 2)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "succinct") (r "^0.4.2") (d #t) (k 0)))) (h "0jkiar0fg3p1290a15a8sr4ikr1q53n6gp3p5j15jdyzfiaz5i92")))

(define-public crate-wavelet-matrix-0.4.1 (c (n "wavelet-matrix") (v "0.4.1") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 2)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "succinct") (r "^0.4.2") (d #t) (k 0)))) (h "068xbkn6dvf2vkm0mc3y4xdyvj3zr8498brbjc0s0r7b30s5lw74")))

(define-public crate-wavelet-matrix-0.4.2 (c (n "wavelet-matrix") (v "0.4.2") (d (list (d (n "num") (r "^0.1.40") (d #t) (k 2)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "succinct") (r "^0.4.2") (d #t) (k 0)))) (h "03rm25xsjppka1i9l7zvvjk6l01c0pij2v944m967bh4zcmqc68m")))

(define-public crate-wavelet-matrix-0.4.3 (c (n "wavelet-matrix") (v "0.4.3") (d (list (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "num") (r "^0.1.40") (d #t) (k 2)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "succinct") (r "^0.4.2") (d #t) (k 0)))) (h "1734c2mbxgln6j870865rz298cmr6jp0s344f6viqpmjir0m0v6w")))

(define-public crate-wavelet-matrix-0.4.5 (c (n "wavelet-matrix") (v "0.4.5") (d (list (d (n "easybench") (r "^0.1.3") (d #t) (k 2)) (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "num") (r "^0.1.40") (d #t) (k 2)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "succinct") (r "^0.4.2") (d #t) (k 0)))) (h "15v4jaqxjr97gj0npbipz361bp44d0j5vq13vxhnnspkpbv0336n")))

(define-public crate-wavelet-matrix-0.4.6 (c (n "wavelet-matrix") (v "0.4.6") (d (list (d (n "easybench") (r "^0.1.3") (d #t) (k 2)) (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "num") (r "^0.1.40") (d #t) (k 2)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "succinct") (r "^0.4.2") (d #t) (k 0)))) (h "0vd238v76pp1h1wzx55nzlniylqzvmsjbgnj19gavrg26s054byh") (y #t)))

(define-public crate-wavelet-matrix-0.4.7 (c (n "wavelet-matrix") (v "0.4.7") (d (list (d (n "easybench") (r "^0.1.3") (d #t) (k 2)) (d (n "itertools") (r "^0.7.2") (d #t) (k 2)) (d (n "num") (r "^0.1.40") (d #t) (k 2)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)) (d (n "succinct") (r "^0.4.2") (d #t) (k 0)))) (h "1gks4xw9hdqf737ny2nzzsnwm499zh0vm26vk2lw698xrcq5nprm")))

