(define-module (crates-io wa ve wave-insight-web) #:use-module (crates-io))

(define-public crate-wave-insight-web-0.1.0 (c (n "wave-insight-web") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "yew") (r "^0.17") (d #t) (k 0)))) (h "0kssvy37m6s7b34fj2q5w3ricz3mmh64laf8xck12jn7sxbnsix9")))

