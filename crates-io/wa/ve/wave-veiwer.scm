(define-module (crates-io wa ve wave-veiwer) #:use-module (crates-io))

(define-public crate-wave-veiwer-0.1.0 (c (n "wave-veiwer") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)))) (h "0vjm6dw644nllv9j3dgk7ig5y4fpcq6d8av8lz7q2a0hwbr21iq3")))

(define-public crate-wave-veiwer-0.1.1 (c (n "wave-veiwer") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0yjyhgjszj98gy1v56j7c831z2z53dkiz22syzn1ixinyidiap1s")))

(define-public crate-wave-veiwer-0.1.2 (c (n "wave-veiwer") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "ron") (r "^0.6.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01zdqc91xdk16r4w0knn50pzajdp45p15bi0h8dq2gmskvww9lhv")))

(define-public crate-wave-veiwer-0.1.3 (c (n "wave-veiwer") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "nom") (r "^7.1.0") (d #t) (k 0)) (d (n "ron") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nnh98fsf2zqdwjl2d7xwxw1haqavcwmih1yc4dzz9ffk8z7db4c")))

