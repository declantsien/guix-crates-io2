(define-module (crates-io wa ve waveform) #:use-module (crates-io))

(define-public crate-waveform-0.1.0 (c (n "waveform") (v "0.1.0") (d (list (d (n "gdk-pixbuf") (r "^0.1.3") (d #t) (k 2)) (d (n "gtk") (r "^0.1.3") (d #t) (k 2)))) (h "0bjy9jwyllmjk6n9fa0ki1yymc024brhzxnxcfznndgxas8i65hf")))

(define-public crate-waveform-0.2.0 (c (n "waveform") (v "0.2.0") (d (list (d (n "gdk-pixbuf") (r "^0.1.3") (d #t) (k 2)) (d (n "gtk") (r "^0.1.3") (d #t) (k 2)) (d (n "rlibc") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "11jgqbgf366j9icb6qdqph9q0c7x66c5s4c0q3m45ddpzysvs888") (f (quote (("default" "rlibc"))))))

(define-public crate-waveform-0.2.2 (c (n "waveform") (v "0.2.2") (d (list (d (n "gdk-pixbuf") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "rlibc") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1finpd4601cf1g0kp76f1pzxg1k1239p794v915wnmv56856id3b") (f (quote (("example-gui" "gtk" "gdk-pixbuf") ("default" "rlibc"))))))

(define-public crate-waveform-0.3.2 (c (n "waveform") (v "0.3.2") (d (list (d (n "gdk-pixbuf") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "gtk") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "rlibc") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0wdxcjvh91rwii4s4xl9rbl238cr3flxf44miwvhiva1mvnx72cb") (f (quote (("example-gui" "gtk" "gdk-pixbuf") ("default" "rlibc"))))))

