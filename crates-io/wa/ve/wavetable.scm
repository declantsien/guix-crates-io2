(define-module (crates-io wa ve wavetable) #:use-module (crates-io))

(define-public crate-wavetable-0.1.0 (c (n "wavetable") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustfft") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0595x0ffkvjk40hy7k6bkiv7jn47lzyv3x6wsw1cb1gxb5b6n610")))

(define-public crate-wavetable-0.1.1 (c (n "wavetable") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustfft") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aml010d71pvsdafs6l4hwk2038hz8xb3y140dmrxd4p7scr6w07")))

(define-public crate-wavetable-0.1.2 (c (n "wavetable") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rustfft") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0qdvvahxl3gk0rq36gy2nsdxyw512rzfmj39h7mvciynw85iiz62")))

