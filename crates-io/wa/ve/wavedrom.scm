(define-module (crates-io wa ve wavedrom) #:use-module (crates-io))

(define-public crate-wavedrom-0.1.0 (c (n "wavedrom") (v "0.1.0") (d (list (d (n "json5") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "ttf-parser") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "08kyd8scbd20a71dmkslmcxcc5qar1k7y5c2xa6wfg141x5w5pjc") (f (quote (("skins" "serde") ("default" "json5" "embed_font" "skins")))) (s 2) (e (quote (("serde_json" "dep:serde_json" "serde") ("json5" "dep:json5" "serde") ("embed_font" "dep:ttf-parser"))))))

