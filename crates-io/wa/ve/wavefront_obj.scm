(define-module (crates-io wa ve wavefront_obj) #:use-module (crates-io))

(define-public crate-wavefront_obj-0.0.1 (c (n "wavefront_obj") (v "0.0.1") (h "003vz07mx8xhfypfijxz16z40cpzj0n0jz18dla9scqikf16l2wi")))

(define-public crate-wavefront_obj-0.0.2 (c (n "wavefront_obj") (v "0.0.2") (h "1krcf0s4myibban65bvd35qiqnmhwkxj76lljy0nxlva6inhqqd0")))

(define-public crate-wavefront_obj-1.0.0 (c (n "wavefront_obj") (v "1.0.0") (h "091dw773wsbylxpfxrkzm37va9ksy9dpsm90a55103khlghhpvzz")))

(define-public crate-wavefront_obj-2.0.0 (c (n "wavefront_obj") (v "2.0.0") (h "1nk5rrl4fc5syp31yy3v7iwfyz0phfvdm5hndj28y87l4nixnhrr")))

(define-public crate-wavefront_obj-2.0.1 (c (n "wavefront_obj") (v "2.0.1") (h "0044jh251w596b8rzf4dr3i81cibjgy888h7vmzmksv4b7x6mii4")))

(define-public crate-wavefront_obj-2.0.2 (c (n "wavefront_obj") (v "2.0.2") (h "1rxma4arryq4h687pm9ia9w5xy71n1v86ik2q4wpfca1y1q3v0qw")))

(define-public crate-wavefront_obj-2.0.3 (c (n "wavefront_obj") (v "2.0.3") (h "051l3m20cwgzzjc3h9nmrlbddpy2lq3p8h9fk85b41x4dv282f2z")))

(define-public crate-wavefront_obj-2.0.4 (c (n "wavefront_obj") (v "2.0.4") (h "1jpg2d48vc0kd2r223svsy9b1ypy5blvwpraxmhr2k39g3v9m913")))

(define-public crate-wavefront_obj-3.0.0 (c (n "wavefront_obj") (v "3.0.0") (h "1gawzl4nvyw766n4c4lb3y5bgsc62aakyn38rpj23nyrhhsvq27y")))

(define-public crate-wavefront_obj-4.0.0 (c (n "wavefront_obj") (v "4.0.0") (h "1bx8rffn8wb717fzlhrrmmx265zgnyal1dvm28j6d6x9jpkvfjk9")))

(define-public crate-wavefront_obj-4.0.1 (c (n "wavefront_obj") (v "4.0.1") (h "0z9qpnavfnc8fiwp11cbxd6x60rzvb5s922n2jm38xq2n8hmj096")))

(define-public crate-wavefront_obj-4.0.2 (c (n "wavefront_obj") (v "4.0.2") (h "0jqlzxy17a652srrwr1fk3a6d2dy1mmywlj59r1a5qvi2p6qkkn8")))

(define-public crate-wavefront_obj-5.0.0 (c (n "wavefront_obj") (v "5.0.0") (h "15pkiqjl6q9nzvnxmmarblx6hm9waiydwzrj78jgw550zrzpfpgy")))

(define-public crate-wavefront_obj-5.1.0 (c (n "wavefront_obj") (v "5.1.0") (h "1zq6jimnmjagyh0pbkbwajdx324myh8vxnhc029n9b2sbjvcpj9k")))

(define-public crate-wavefront_obj-6.0.0 (c (n "wavefront_obj") (v "6.0.0") (d (list (d (n "lexical") (r "^2.1.0") (d #t) (k 0)))) (h "0hx38y57f1qa33vjj9d87xfk2h8aff5whs8sainyq3flwmq6sj52")))

(define-public crate-wavefront_obj-7.0.0 (c (n "wavefront_obj") (v "7.0.0") (d (list (d (n "lexical") (r "^2.1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "154xgvss58qjqmkgq0l05cpg4rk5jzvax9y0l9g4s12h3ypq67vc")))

(define-public crate-wavefront_obj-8.0.0 (c (n "wavefront_obj") (v "8.0.0") (d (list (d (n "lexical") (r "^2.1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "1sylgmk0ia4ih43hfhdsz6phkqnymcsn2qpvj4y0rcynxhv6vj28")))

(define-public crate-wavefront_obj-9.0.0 (c (n "wavefront_obj") (v "9.0.0") (d (list (d (n "lexical") (r "^2.1.0") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "13z03y3fh21x7shlr25b5ri0nz0yyzw6lfhsv9fgisd2321v5dj7")))

(define-public crate-wavefront_obj-10.0.0 (c (n "wavefront_obj") (v "10.0.0") (d (list (d (n "lexical") (r "^5.2") (d #t) (k 0)) (d (n "proptest") (r "^0.9") (d #t) (k 2)))) (h "12jfncrjz1vz07nbjsm5x7na3m2gm3dn5558jxjma882lb7bni2v")))

