(define-module (crates-io wa ve wavefile) #:use-module (crates-io))

(define-public crate-wavefile-0.1.0 (c (n "wavefile") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.2") (d #t) (k 0)))) (h "0lg807gin3aq6p37mxdyh2f6s4a88g7642vmglm9snfz5jsqdzr1")))

(define-public crate-wavefile-0.2.0 (c (n "wavefile") (v "0.2.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.2") (d #t) (k 0)))) (h "0lxb5fyzrzl8890nai56lm5hmyzwsdff6wxnb8mjil1pk9pnbnq0")))

