(define-module (crates-io wa ve waves-wallet-generator) #:use-module (crates-io))

(define-public crate-waves-wallet-generator-0.1.0 (c (n "waves-wallet-generator") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "wavesplatform") (r "^0.2.1") (d #t) (k 0)))) (h "03b7zzmh05nma9ikw89h38835f95dk94mfikmwd7y77pn7x42v11")))

