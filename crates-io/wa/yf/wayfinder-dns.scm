(define-module (crates-io wa yf wayfinder-dns) #:use-module (crates-io))

(define-public crate-wayfinder-dns-0.1.0 (c (n "wayfinder-dns") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wayfinder-godaddy") (r "^0.1.0") (d #t) (k 0)) (d (n "wayfinder-shared") (r "^0.1.0") (d #t) (k 0)))) (h "0pgcbclgjv6iwxjbaizqb4d8fqmdk0r3gbncnayp6ryakv0aq95p")))

(define-public crate-wayfinder-dns-0.1.1 (c (n "wayfinder-dns") (v "0.1.1") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "wayfinder-godaddy") (r "^0.1.0") (d #t) (k 0)) (d (n "wayfinder-shared") (r "^0.1.0") (d #t) (k 0)))) (h "1hsdlfza3xpk8sl2xx0kcfd90jr2k6j6ybmf2x46b888qrfi0y3s")))

