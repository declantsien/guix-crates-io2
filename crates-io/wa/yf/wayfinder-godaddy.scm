(define-module (crates-io wa yf wayfinder-godaddy) #:use-module (crates-io))

(define-public crate-wayfinder-godaddy-0.1.0 (c (n "wayfinder-godaddy") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (d #t) (k 0)) (d (n "wayfinder-shared") (r "^0.1.0") (d #t) (k 0)))) (h "1nzzvf8c071chb48fia2rhb0kgxbva6cak1280xxf55bwbwvwm1q")))

