(define-module (crates-io wa yf wayfinder) #:use-module (crates-io))

(define-public crate-wayfinder-0.1.0 (c (n "wayfinder") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "0bhmfwi234maq033ffdlsb4c6kzb5zq9j4jnqa9g9myi5hs2smkh")))

(define-public crate-wayfinder-0.2.0 (c (n "wayfinder") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1b57m48sia0gxihb5ckr90si3vjzhm9ybjgcsdvckdj40akqrb51")))

(define-public crate-wayfinder-0.2.1 (c (n "wayfinder") (v "0.2.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nom") (r "^4.1.1") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1wx6kmj6mhw63wrn6jssb1qqbdnhfr65mnw21sdgghfy8k7928dk")))

