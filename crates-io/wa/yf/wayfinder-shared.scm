(define-module (crates-io wa yf wayfinder-shared) #:use-module (crates-io))

(define-public crate-wayfinder-shared-0.1.0 (c (n "wayfinder-shared") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0azzzhkybpmp5gr1sbic9jp77s01nw909270ipcasvyk9csicgar")))

