(define-module (crates-io wa ku waku-sys) #:use-module (crates-io))

(define-public crate-waku-sys-0.1.0-beta1 (c (n "waku-sys") (v "0.1.0-beta1") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1mnd5lqq5mb1la249rlnwm7nv3ymgwvg2kfg3gn9mdhs2zynf4nj")))

(define-public crate-waku-sys-0.1.0-beta2 (c (n "waku-sys") (v "0.1.0-beta2") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1mv5bzr7h6gbn33144fp4ng1676rbma22jvawj4c4kcwafidws8a")))

(define-public crate-waku-sys-0.1.0-beta3 (c (n "waku-sys") (v "0.1.0-beta3") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1qz8ggbm4vc408x2nph355mxqzslpr6i0nq3xlpb55ssb92gx69v")))

(define-public crate-waku-sys-0.1.0-beta4 (c (n "waku-sys") (v "0.1.0-beta4") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "19khk8c73h53kal12wsrsqsyzblj55yplc4rnh6scc6xqmwdpix7")))

(define-public crate-waku-sys-0.1.0-beta.5 (c (n "waku-sys") (v "0.1.0-beta.5") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "01y1l3viid31kwwf3h2jrirx28waq53vf60l86zyabchg2kvqpcz")))

(define-public crate-waku-sys-0.1.0-rc.1 (c (n "waku-sys") (v "0.1.0-rc.1") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "0wvadgqqzcnyyc9nk5qdy7fzcwdlbih670hpjnmszmlm677hvzxk")))

(define-public crate-waku-sys-0.1.0-rc.2 (c (n "waku-sys") (v "0.1.0-rc.2") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "1ky9p8gi5ydzgfvsnakq4dj3afhv5dsabgbky2virm5gjcbbkaaz")))

(define-public crate-waku-sys-0.1.0-rc.3 (c (n "waku-sys") (v "0.1.0-rc.3") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "04ir1rrmf68vw9qd48lxc4ysrf2g5bgdh1sbbmil332671cly4mx")))

(define-public crate-waku-sys-0.1.0 (c (n "waku-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "19ywvxb35cg1k26gpriyxzfxx6k5s83q3452a2kapm7vccl9g1ib")))

(define-public crate-waku-sys-0.2.0 (c (n "waku-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "0wkdq7jywpzzg8ilj6m23v031blv8cg3rlwdm66i5ys2z43snz5d")))

(define-public crate-waku-sys-0.3.0 (c (n "waku-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "0jc7gjsfpap59dzx0afghinp5yfh912bb0slvxp76ks72rbfflif")))

(define-public crate-waku-sys-0.3.1 (c (n "waku-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "16assk5parw9sa5fqq4jkgbs2wmrh0dixcfghwmi8i3nr6k78cd2")))

(define-public crate-waku-sys-0.4.0 (c (n "waku-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "0mk52fajdblhmx3h3zbc02gmya6c75pc4zbsgg5yl6fp3vfi1w1i")))

(define-public crate-waku-sys-0.5.0 (c (n "waku-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "0xvql5fc4avbfzlvsglgmfhmhq766vjvmz17g8y2fa5ih6mnxif1")))

(define-public crate-waku-sys-0.6.0 (c (n "waku-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "16mdcy6l8s8z3zwjd7s45fgm35ijshs0r1621c3zbkcdr48mfwva")))

