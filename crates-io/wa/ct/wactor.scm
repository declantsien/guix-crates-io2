(define-module (crates-io wa ct wactor) #:use-module (crates-io))

(define-public crate-wactor-0.1.0 (c (n "wactor") (v "0.1.0") (d (list (d (n "lunatic") (r "^0.3.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)))) (h "1n4adkkyfi6jzrrvj4imzrjriy7646h5y6vbbnhbqzcn1qiswb70")))

(define-public crate-wactor-0.1.1 (c (n "wactor") (v "0.1.1") (d (list (d (n "lunatic") (r "^0.3.10") (d #t) (k 0)) (d (n "serde") (r "^1.0.124") (d #t) (k 0)))) (h "10qq4kic7zk5421v8iwil6n26bsnx62i5hw400gk51bb4z8pc8fw")))

