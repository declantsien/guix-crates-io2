(define-module (crates-io wa yo wayout) #:use-module (crates-io))

(define-public crate-wayout-1.0.0 (c (n "wayout") (v "1.0.0") (d (list (d (n "clap") (r "^3.1.2") (d #t) (k 0)) (d (n "smithay-client-toolkit") (r "^0.15.3") (d #t) (k 0)))) (h "1l5ii2ddvlx2nyrbjagf8f26v637x35f68vnvdpvqqmgg6f3v6wm")))

(define-public crate-wayout-1.1.2 (c (n "wayout") (v "1.1.2") (d (list (d (n "clap") (r "^3.1.2") (d #t) (k 0)) (d (n "wayland-client") (r "^0.29.4") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.29.4") (f (quote ("unstable_protocols" "client"))) (d #t) (k 0)))) (h "0vcmmq5a075g2dc24asrx0bn1j60l5bbmid855mmpnssyh5qiizz")))

(define-public crate-wayout-1.1.3 (c (n "wayout") (v "1.1.3") (d (list (d (n "clap") (r "^3.1.2") (d #t) (k 0)) (d (n "wayland-client") (r "^0.29.4") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.29.4") (f (quote ("unstable_protocols" "client"))) (d #t) (k 0)))) (h "10r4367xx9441mr8jx3sw96bv5mmzpjh98y8k090q5k6x2bwziz7")))

