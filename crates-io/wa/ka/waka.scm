(define-module (crates-io wa ka waka) #:use-module (crates-io))

(define-public crate-waka-0.1.0 (c (n "waka") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "query-string-builder") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "0b5jkk22xd3l8dyn1idzq7zi6150vlkwcbi8mzd235hypbl94z4d")))

