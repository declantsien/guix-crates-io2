(define-module (crates-io wa ln walnut) #:use-module (crates-io))

(define-public crate-walnut-0.1.0 (c (n "walnut") (v "0.1.0") (h "07nfynfrv0ly4ci7k792n2zpnicaqxrj0mzpyjyp3g7cms2d8hyl") (y #t)))

(define-public crate-walnut-0.1.2 (c (n "walnut") (v "0.1.2") (h "11pqh28wsisbjdp60j7il0q7r0d2qzxxa7wyxsyiqmhwm6mas50r") (y #t)))

(define-public crate-walnut-0.1.3 (c (n "walnut") (v "0.1.3") (h "099b8arqs7cj8hvmlg419q59dr6zd0y9x9z6479z8p1k7h4g55ql") (y #t)))

(define-public crate-walnut-0.1.4 (c (n "walnut") (v "0.1.4") (h "125mdcg99kp67c1mi5ny6klk3b7c8rjdjvf06sga6gjpvy4rirq5") (y #t)))

(define-public crate-walnut-0.1.5 (c (n "walnut") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crc32fast") (r "^1.2.0") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "18pky855954fb1pa7zf8mck4m8z7gp9k4x1378j3sczbv5jkqip1")))

