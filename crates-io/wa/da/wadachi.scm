(define-module (crates-io wa da wadachi) #:use-module (crates-io))

(define-public crate-wadachi-0.1.0-beta.0 (c (n "wadachi") (v "0.1.0-beta.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "surf") (r "^2.1.0") (d #t) (k 0)))) (h "1spkzmk6pxhz0pki0lr3zj7mbdqxrxqp89aymb9shid0f8kp3mkl")))

