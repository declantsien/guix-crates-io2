(define-module (crates-io wa lt walter) #:use-module (crates-io))

(define-public crate-walter-0.1.0 (c (n "walter") (v "0.1.0") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "04rywzvi7ip2h1jm2rfask8hdzncj53kgwhp0fibdwfy8fly14rq") (y #t)))

(define-public crate-walter-0.1.1 (c (n "walter") (v "0.1.1") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "04b098m0v0h91q1xmmr5j76laldwb2hj99x9f3aybfn8ncqhcg94") (y #t)))

(define-public crate-walter-0.1.2 (c (n "walter") (v "0.1.2") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "1m5mj4fk19zi71akx1sai5iz4c7cwbfwpmv3z9maj2hr1421w93r") (y #t)))

(define-public crate-walter-0.1.3 (c (n "walter") (v "0.1.3") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "1q14z16xabwn62fsx09ijwqs1shyysz4a2800hayalh8bgagxp6r") (y #t)))

(define-public crate-walter-0.1.4 (c (n "walter") (v "0.1.4") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "0062jk278c8clj836rkp8r611rs75jg4xd404qqhnz04y0jm46jj") (y #t)))

(define-public crate-walter-0.1.5 (c (n "walter") (v "0.1.5") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "0v4n7vn39c88mdw4bh0s6cay0a74dw7pn2h0zgnx8p02k81i0qfr") (y #t)))

(define-public crate-walter-0.1.6 (c (n "walter") (v "0.1.6") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "06r4lgnzhk6sa1rjgdzai2fixacx8r87srbrq4s202adyzjqhc2n") (y #t)))

(define-public crate-walter-0.1.8 (c (n "walter") (v "0.1.8") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "1mscpq3dm8sl2p71rff9q7w5dq2680yqpsx46dw6fwwgpdxynwzl") (y #t)))

(define-public crate-walter-0.1.9 (c (n "walter") (v "0.1.9") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "0h8q81mfzbdqjhd6cn4b1xccz9kan5njzg9an3jzw7sl85i4sqyg") (y #t)))

(define-public crate-walter-0.1.10 (c (n "walter") (v "0.1.10") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "15pyw5bxwlrgkacyjpm97izn8bcwmh43hi8ha0c57aqp14zsxbps") (y #t)))

(define-public crate-walter-0.1.11 (c (n "walter") (v "0.1.11") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "1c6nm06756yfr9p9s1c471ld41901chrfzx83q9dd3wp5ipb3s6r") (y #t)))

(define-public crate-walter-0.1.12 (c (n "walter") (v "0.1.12") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "11bbbd4jj80bfk0nh4jicibwg1zrhgn81yv45mgpmx03lkkjsn12") (y #t)))

(define-public crate-walter-0.1.13 (c (n "walter") (v "0.1.13") (d (list (d (n "windows") (r "^0.17.2") (d #t) (k 0)) (d (n "windows") (r "^0.17.2") (d #t) (k 1)))) (h "1pphchl2rgspcf2x49j7lybl7laaq6826b9wh7v6v04n692l40z7") (y #t)))

