(define-module (crates-io wa lt waltz) #:use-module (crates-io))

(define-public crate-waltz-0.2.0 (c (n "waltz") (v "0.2.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1cvlrkyg0lykyvqmdaxqvr78fhgnhqmagkab1fcyx3ryr55bd2z4")))

(define-public crate-waltz-0.2.1 (c (n "waltz") (v "0.2.1") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "09zrl1n4pmbn94g0kqs2nicdrp9nm652bdnx6bcw5klv1gpb6vl0")))

(define-public crate-waltz-0.3.0 (c (n "waltz") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.4") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "1n3g9padn3kvm65n2yvcqw580hshdbv1k10dmf8lb4a0q092zcxp")))

(define-public crate-waltz-0.4.0 (c (n "waltz") (v "0.4.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "08l9vg31a8bd7s5cfplb71y0hgsn19kgm6kgvgz5r2drmk6g50zb")))

(define-public crate-waltz-0.4.1 (c (n "waltz") (v "0.4.1") (d (list (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)) (d (n "unindent") (r "^0.1") (d #t) (k 2)))) (h "00bk1vqpvp3wql537i2rp50f48yh7v5m1j1ldlmxik8dbg42lzqg")))

