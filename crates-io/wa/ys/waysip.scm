(define-module (crates-io wa ys waysip) #:use-module (crates-io))

(define-public crate-waysip-0.1.1 (c (n "waysip") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libwaysip") (r "^0.1.0") (d #t) (k 0)))) (h "0q96qi2gd4p9vri4ym8vyfb4abnyn17ax5q3adq5xxylap1wywvk")))

(define-public crate-waysip-0.2.0 (c (n "waysip") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libwaysip") (r "^0.2.0") (d #t) (k 0)))) (h "1s5gx1ljsgz6s58sfsjxij5vp7fqlvnj25kqsx2hh379wjnmwz1a")))

(define-public crate-waysip-0.2.1 (c (n "waysip") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libwaysip") (r "^0.2.1") (d #t) (k 0)))) (h "0ls07ckc7cd38m1gyv0gqzrlri31hg96qd849ajjkz6xqj7l7vw5")))

(define-public crate-waysip-0.2.2 (c (n "waysip") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libwaysip") (r "^0.2.2") (d #t) (k 0)))) (h "0y0ay0g3bcwa42gc8pir8kjimb9mg34f65296b470p28lz1pl8s2")))

