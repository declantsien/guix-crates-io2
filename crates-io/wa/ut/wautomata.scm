(define-module (crates-io wa ut wautomata) #:use-module (crates-io))

(define-public crate-wautomata-0.1.0 (c (n "wautomata") (v "0.1.0") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)))) (h "1zh9q9jfx6nkvf6v0fjscgqdb2981yr0cip69jzsw07bpyvkyjyb")))

(define-public crate-wautomata-0.1.1 (c (n "wautomata") (v "0.1.1") (d (list (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "wtools") (r "~0.2") (d #t) (k 0)))) (h "1m9p8ca0ajk7ahqlinln64zgdi6a1mk3fncx2glfljy8r6k8k185")))

(define-public crate-wautomata-0.1.2 (c (n "wautomata") (v "0.1.2") (d (list (d (n "automata_tools") (r "~0.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "test_tools") (r "~0.1") (d #t) (k 2)) (d (n "wtools") (r "~0.2") (f (quote ("full"))) (d #t) (k 2)))) (h "120n71gqq994zpkq699381967axc5bwbdlcc91nw1rabx7143g7d") (f (quote (("use_std") ("use_alloc") ("full" "use_std") ("default" "use_std"))))))

