(define-module (crates-io wa pm wapm-resolve-url) #:use-module (crates-io))

(define-public crate-wapm-resolve-url-0.1.0 (c (n "wapm-resolve-url") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "graphql_client") (r "^0.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (f (quote ("rustls-tls" "blocking" "json" "gzip" "socks" "multipart"))) (t "cfg(not(target_os = \"wasi\"))") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)))) (h "08gavyfnmry6w6phpy2vxxqvwws8km66ppbql152xfncjskij2i6")))

