(define-module (crates-io wa ke wake-rs) #:use-module (crates-io))

(define-public crate-wake-rs-0.2.3 (c (n "wake-rs") (v "0.2.3") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serialport") (r "^4.0.1") (k 2)))) (h "1n274z3jnyp41fcmx575phhzawvyqh3an6745332vf09429dzi9p")))

(define-public crate-wake-rs-0.2.4 (c (n "wake-rs") (v "0.2.4") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serialport") (r "^4.0.1") (k 2)))) (h "15f0v64a8imvhy6ximb6szma0xlvxg6ck2gn2460ijx4v7g1yggp")))

(define-public crate-wake-rs-0.2.5 (c (n "wake-rs") (v "0.2.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "serialport") (r "^4.0.1") (k 2)))) (h "1vl2rrj6nl4ji8g92a1ambf0nhwylgyxg8lv6xvliqvfk9qc2rhv")))

