(define-module (crates-io wa ke wakey) #:use-module (crates-io))

(define-public crate-wakey-0.1.0 (c (n "wakey") (v "0.1.0") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)))) (h "0gyq5hp7b1qixqn26niix3xygh8ikpbxcb72xhp48xc46glllplb")))

(define-public crate-wakey-0.1.1 (c (n "wakey") (v "0.1.1") (d (list (d (n "hex") (r "^0.3.2") (d #t) (k 0)))) (h "0cwmjgp6jsm4g4c85c1dq6bhjrx694px8w7x66c427n344w0m00v")))

(define-public crate-wakey-0.1.2 (c (n "wakey") (v "0.1.2") (d (list (d (n "hex") (r "~0.3") (d #t) (k 0)))) (h "0c3ixgg1dbhdabwi38ngf61anjl7iqw4y9qd2nmrpk7fzc71kln1")))

(define-public crate-wakey-0.2.0 (c (n "wakey") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "~0.3") (d #t) (k 0)))) (h "19k34x3k23d1y5pgq7292x033k5xixg0c5fgl37m32h756gsdbja")))

(define-public crate-wakey-0.2.1 (c (n "wakey") (v "0.2.1") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "~0.3") (d #t) (k 0)))) (h "1m2kh17zd9zjv1mwzr5z39zvkdhjfjgpm9n1jvmrks3sghwly142")))

(define-public crate-wakey-0.2.2 (c (n "wakey") (v "0.2.2") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "hex") (r "~0.3") (d #t) (k 0)))) (h "05drf2msjsyah5rqqhcfafx6irhcaxx9394v0wz5cxa6gi0jwxs1")))

(define-public crate-wakey-0.3.0 (c (n "wakey") (v "0.3.0") (d (list (d (n "arrayvec") (r "^0.7.2") (d #t) (k 0)) (d (n "hex") (r "~0.4") (d #t) (k 0)))) (h "1xg94c7kl1i0kgnjwa4j6gnk3rb5faqnpvf1np7kply0j6kbbnny")))

