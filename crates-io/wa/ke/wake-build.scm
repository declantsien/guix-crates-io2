(define-module (crates-io wa ke wake-build) #:use-module (crates-io))

(define-public crate-wake-build-0.1.0 (c (n "wake-build") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0fb7j3163rpg26ckbfhr9xp4zl2jl2c4ji0458q6k755bgk314zz")))

(define-public crate-wake-build-0.1.1 (c (n "wake-build") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1c6ijh3gf6bx6m02c3i9bnziiw0ciz3y908gd7zn8iqw5y0p3mjn")))

