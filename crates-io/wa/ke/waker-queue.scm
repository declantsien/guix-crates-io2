(define-module (crates-io wa ke waker-queue) #:use-module (crates-io))

(define-public crate-waker-queue-0.1.0 (c (n "waker-queue") (v "0.1.0") (d (list (d (n "atomic-waker") (r "^1") (d #t) (k 0)) (d (n "concurrent-queue") (r "^1") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.1") (d #t) (k 0)))) (h "1dff38f9mz8kd63klnlvcrlxyqaapvhimiyvwhqcfhd91fjd2x5b")))

