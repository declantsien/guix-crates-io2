(define-module (crates-io wa ke waker-waiter) #:use-module (crates-io))

(define-public crate-waker-waiter-0.1.0 (c (n "waker-waiter") (v "0.1.0") (h "0dwhmrr3bh62gg33xx937hgp1fxwy035c202fv6c79fappvchcn9")))

(define-public crate-waker-waiter-0.2.0 (c (n "waker-waiter") (v "0.2.0") (h "0lz2mqscpq6hc5ywvybfya7z35if229mkmb8aqampz442rw6dzcs")))

(define-public crate-waker-waiter-0.2.1 (c (n "waker-waiter") (v "0.2.1") (h "12j7ys30df6bpy84lrjypzc2135gcww4z0a18nr74bwmlm4snkqx")))

