(define-module (crates-io wa ke wakeup) #:use-module (crates-io))

(define-public crate-wakeup-0.1.0 (c (n "wakeup") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "home") (r "^0.5.3") (d #t) (k 0)) (d (n "soloud") (r "^1.0.2") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.9.0") (d #t) (k 0)))) (h "07visdysj691ml54wpp13qqfgx02sy4fsm60sp9c93sqayxdlibx")))

