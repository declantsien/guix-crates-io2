(define-module (crates-io wa ke wakerpool) #:use-module (crates-io))

(define-public crate-wakerpool-0.1.0 (c (n "wakerpool") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)))) (h "1xkgp15w14pk833l8318yjy64i95zr2dhqxwygc20fwf2k1lf7ga") (r "1.60")))

