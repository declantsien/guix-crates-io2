(define-module (crates-io wa ke waker-set) #:use-module (crates-io))

(define-public crate-waker-set-0.1.0 (c (n "waker-set") (v "0.1.0") (d (list (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "024kg2qs3g0sfzfxhk3vxcm8vvxaqh56nas51pm0n9nivp9jcgck")))

(define-public crate-waker-set-0.1.1 (c (n "waker-set") (v "0.1.1") (d (list (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "0ljkhqyaq0sgxw5wi1dg36gng6dscghvyaqcl72glg7sl4c56j63") (f (quote (("unstable"))))))

(define-public crate-waker-set-0.2.0 (c (n "waker-set") (v "0.2.0") (d (list (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.2") (d #t) (k 0)))) (h "01xwqhdc1qiwx0vw315kl1rha845mhq210b1bjpy2ib3qi9835cy") (f (quote (("unstable"))))))

