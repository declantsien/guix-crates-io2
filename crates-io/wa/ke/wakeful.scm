(define-module (crates-io wa ke wakeful) #:use-module (crates-io))

(define-public crate-wakeful-0.1.0 (c (n "wakeful") (v "0.1.0") (h "09wrnsy19mij4609f49kl9rqqxzlk8m2b14dp93h2nqcpylqadnr") (y #t)))

(define-public crate-wakeful-0.1.1 (c (n "wakeful") (v "0.1.1") (h "17ksrdjmi5i6mwxc55z53iplmyk8537a2ijzrvsds8cbch2pdajd") (y #t)))

