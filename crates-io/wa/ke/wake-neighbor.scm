(define-module (crates-io wa ke wake-neighbor) #:use-module (crates-io))

(define-public crate-wake-neighbor-0.1.0 (c (n "wake-neighbor") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pnet") (r "^0.31.0") (f (quote ("default" "std"))) (d #t) (k 0)))) (h "14h7shmc1sh2jgncdx6klzp4lc1wmfl1ahd6iq369pxydsklvsqk")))

