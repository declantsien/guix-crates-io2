(define-module (crates-io wa ke wakerset) #:use-module (crates-io))

(define-public crate-wakerset-0.1.0 (c (n "wakerset") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "futures") (r "^0.3") (f (quote ("async-await"))) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 2)) (d (n "pinned-mutex") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.37") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "01vf37phlipl4yapvqa6bqrkvrscn2jq0qn2znwwpxfp1r83pdx5") (r "1.77")))

(define-public crate-wakerset-0.1.1 (c (n "wakerset") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "arrayvec") (r "^0.7") (k 0)) (d (n "futures") (r "^0.3") (f (quote ("async-await"))) (k 2)) (d (n "pin-project") (r "^1") (d #t) (k 2)) (d (n "pinned-mutex") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "tokio") (r "^1.37") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "17ba0yk01bikpacwvynypn4h63fj2l6b2iccvanmpx6vcznr5h3k") (r "1.77")))

