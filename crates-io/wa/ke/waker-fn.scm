(define-module (crates-io wa ke waker-fn) #:use-module (crates-io))

(define-public crate-waker-fn-1.0.0 (c (n "waker-fn") (v "1.0.0") (h "1xsz20imqkyx38hpryqd6qk3bd9gv9j372xmwr1fcp785hn58wcm")))

(define-public crate-waker-fn-1.1.0 (c (n "waker-fn") (v "1.1.0") (h "1jpfiis0frk2b36krqvk8264kgxk2dyhfzjsr8g3wah1nii2qnwx")))

(define-public crate-waker-fn-1.1.1 (c (n "waker-fn") (v "1.1.1") (h "142n74wlmpwcazfb5v7vhnzj3lb3r97qy8mzpjdpg345aizm3i7k") (r "1.51")))

(define-public crate-waker-fn-1.2.0 (c (n "waker-fn") (v "1.2.0") (d (list (d (n "portable-atomic-util") (r "^0.2") (f (quote ("alloc"))) (o #t) (k 0)))) (h "1dvk0qsv88kiq22x8w0qz0k9nyrxxm5a9a9czdwdvvhcvjh12wii") (f (quote (("portable-atomic" "portable-atomic-util")))) (r "1.51")))

