(define-module (crates-io wa rk warkov-wordgen) #:use-module (crates-io))

(define-public crate-warkov-wordgen-0.1.0 (c (n "warkov-wordgen") (v "0.1.0") (d (list (d (n "quicli") (r "^0.3") (d #t) (k 0)) (d (n "warkov") (r "^0.1") (d #t) (k 0)))) (h "1dva5f7qhm16xb5wzg57g1n8bx2apdsg7f0qca3x0nq59lqm3zil")))

(define-public crate-warkov-wordgen-0.2.0 (c (n "warkov-wordgen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.62") (d #t) (k 0)) (d (n "clap") (r "^3.2.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "warkov") (r "^0.1") (d #t) (k 0)))) (h "0fm39ff2ilpnrv9bxdg6v8a9r5i76bdj48w1rw5h9gqi5lxralq0")))

