(define-module (crates-io wa ni wanisabi-model) #:use-module (crates-io))

(define-public crate-wanisabi-model-0.1.1 (c (n "wanisabi-model") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)))) (h "0d07hf43dwyz00z0r1q12kz9kqwzvrckfraflr863azwq686nq14")))

(define-public crate-wanisabi-model-0.1.2 (c (n "wanisabi-model") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.150") (f (quote ("derive"))) (d #t) (k 0)))) (h "0br6kjxzvg0pazvc7w6y00fv279knzfd0xxs2q246aw2d881cbly")))

