(define-module (crates-io wa ni wanikani) #:use-module (crates-io))

(define-public crate-wanikani-0.1.0 (c (n "wanikani") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rb860431mlb58jzn2lia11w9ld0j1wyrl06kkyzxnrg8jfgx9wz")))

(define-public crate-wanikani-0.1.1 (c (n "wanikani") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "0s9z5xpds6sx3hi9nlc1n1vxcaing0ih8qi709r0k1dq0aikgkan")))

(define-public crate-wanikani-0.1.2 (c (n "wanikani") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "1wgfq0d5aavxby8b6g2yypshhd44ggrdlfk3py649yblsqw109pq")))

(define-public crate-wanikani-0.1.3 (c (n "wanikani") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "docopt") (r "^0.8") (d #t) (k 2)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.5.1") (d #t) (k 0)))) (h "1rprmxhmyk7z9v14qz0ibgpvzv86i11yvy709skv07p51jwb75zi")))

