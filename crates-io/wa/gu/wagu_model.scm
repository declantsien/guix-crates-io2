(define-module (crates-io wa gu wagu_model) #:use-module (crates-io))

(define-public crate-wagu_model-0.6.0 (c (n "wagu_model") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "hex") (r "^0.3.2") (d #t) (k 2)) (d (n "ripemd160") (r "^0.7") (d #t) (k 0)) (d (n "sha2") (r "^0.7") (d #t) (k 0)))) (h "0hza6gl15byarwzld235nca5q7li0r7lmyldlqzs133bhvvgf32f") (y #t)))

