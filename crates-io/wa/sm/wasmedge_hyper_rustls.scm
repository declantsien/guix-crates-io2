(define-module (crates-io wa sm wasmedge_hyper_rustls) #:use-module (crates-io))

(define-public crate-wasmedge_hyper_rustls-0.1.0 (c (n "wasmedge_hyper_rustls") (v "0.1.0") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper_wasi") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio_wasi") (r "^1") (f (quote ("rt" "macros" "net" "time" "io-util"))) (d #t) (k 0)) (d (n "wasmedge_rustls_api") (r "^0.1.0") (f (quote ("tokio_async"))) (d #t) (k 0)))) (h "1ppc54s8hy2mwa760w39g557a5a4ff91ibsck87v6yid45l48zd7")))

(define-public crate-wasmedge_hyper_rustls-0.1.1 (c (n "wasmedge_hyper_rustls") (v "0.1.1") (d (list (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper_wasi") (r "^0.15") (f (quote ("full"))) (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.5.0") (d #t) (k 0)) (d (n "tokio_wasi") (r "^1") (f (quote ("rt" "macros" "net" "time" "io-util"))) (d #t) (k 0)) (d (n "wasmedge_rustls_api") (r "^0.1.0") (f (quote ("tokio_async"))) (d #t) (k 0)))) (h "1k5nbbzvys29dhdjz2g8nkx7azwdnx9hn41ps3qk03g72b6i823d")))

