(define-module (crates-io wa sm wasmedge_process_interface) #:use-module (crates-io))

(define-public crate-wasmedge_process_interface-0.2.0 (c (n "wasmedge_process_interface") (v "0.2.0") (h "06jaqmzwia7c6jqfd7n1plpkmwm78lwdnif88nqzp4crav1l9rfz")))

(define-public crate-wasmedge_process_interface-0.2.1 (c (n "wasmedge_process_interface") (v "0.2.1") (h "0ihg49206b2kpdaj372f4zsnp7c84ci8ai2lzliivqva8khvr9g1")))

