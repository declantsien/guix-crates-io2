(define-module (crates-io wa sm wasm_main_executor) #:use-module (crates-io))

(define-public crate-wasm_main_executor-0.1.0 (c (n "wasm_main_executor") (v "0.1.0") (d (list (d (n "arc-swap") (r "^1.6.0") (d #t) (k 0)) (d (n "dummy-waker") (r "^1.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (f (quote ("executor"))) (k 0)) (d (n "gloo-timers") (r "^0.2.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.24") (f (quote ("Window"))) (d #t) (k 0)))) (h "0z2h1rnnld9zhb4wp7b3d2w05scld8pylqr5gf8bhjcbkgnjj0j0")))

