(define-module (crates-io wa sm wasmlite-parser) #:use-module (crates-io))

(define-public crate-wasmlite-parser-0.0.1 (c (n "wasmlite-parser") (v "0.0.1") (d (list (d (n "wasmlite-llvm") (r "^0.0.1") (d #t) (k 0)) (d (n "wasmlite-utils") (r "^0.0.1") (d #t) (k 0)))) (h "0b415zvs68rvvmjx58p7ai06nd8rcvn85cph052rmx8bqrax52a5") (f (quote (("debug"))))))

