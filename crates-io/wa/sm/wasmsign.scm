(define-module (crates-io wa sm wasmsign) #:use-module (crates-io))

(define-public crate-wasmsign-0.1.0 (c (n "wasmsign") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "13676wf27df5xck5qydwkv40jr1525bkq3akj4hziiqprbn1mpz2")))

(define-public crate-wasmsign-0.1.1 (c (n "wasmsign") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.34") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "0pxgcb7ig6r445h39s1v3anqdmrrvgvpfb6l3phb8zf53riaizvg")))

(define-public crate-wasmsign-0.1.2 (c (n "wasmsign") (v "0.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^0.9") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "sha2") (r "^0.8") (d #t) (k 0)))) (h "1khdswa6iww3mqpax8s6kinb8g1q1c87ab90mdryrzcafpzn0yb9")))

