(define-module (crates-io wa sm wasmesh-macros) #:use-module (crates-io))

(define-public crate-wasmesh-macros-0.1.0 (c (n "wasmesh-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0yyr7zb05rf0q1dypzhj9b6ffkwjlzvjlamsw97i6xdbl4rf19i9")))

