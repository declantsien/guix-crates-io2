(define-module (crates-io wa sm wasm4fun-random) #:use-module (crates-io))

(define-public crate-wasm4fun-random-0.1.0 (c (n "wasm4fun-random") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.6") (k 0)) (d (n "rand_xorshift") (r "^0.3") (k 0)) (d (n "wasm4fun-log") (r "^0.1.0") (k 0)) (d (n "wasm4fun-time") (r "^0.1.0") (d #t) (k 0)))) (h "0sbq2hwa0m5b5r6c0idrj0ipx3qnswrvm88y7g940yddafxa7x3d") (f (quote (("default") ("debug" "wasm4fun-log/debug"))))))

