(define-module (crates-io wa sm wasm-ext) #:use-module (crates-io))

(define-public crate-wasm-ext-0.0.0 (c (n "wasm-ext") (v "0.0.0") (h "0q610kd2gbc0c3apqcprfv23cwbzj8d7cdvinnz86hmh2sar7wb8") (y #t)))

(define-public crate-wasm-ext-0.27.0 (c (n "wasm-ext") (v "0.27.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.19") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "tetsy-send-wrapper") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.42") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.4") (d #t) (k 0)))) (h "1xn6yb0yx4zv3gfxpkvqnijg480z7w9yr3xr7yzlr8p4gy1rfxw2") (f (quote (("websocket"))))))

