(define-module (crates-io wa sm wasmcloud-logging) #:use-module (crates-io))

(define-public crate-wasmcloud-logging-0.9.0 (c (n "wasmcloud-logging") (v "0.9.0") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.9.0") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmcloud-actor-logging") (r "^0.1.0") (d #t) (k 0)))) (h "0lqmja8s1yignykc2lc6mgp7rang00p5sdq3gq9w1aranr8zn032") (f (quote (("static_plugin"))))))

(define-public crate-wasmcloud-logging-0.9.1 (c (n "wasmcloud-logging") (v "0.9.1") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.9.0") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmcloud-actor-logging") (r "^0.1.0") (d #t) (k 0)))) (h "08q9xzsa3iz591rc5d6hr32765xsgwdzgr5i28mzqhgm72rjcr5i") (f (quote (("static_plugin"))))))

(define-public crate-wasmcloud-logging-0.9.3 (c (n "wasmcloud-logging") (v "0.9.3") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.2") (d #t) (k 0)) (d (n "wasmcloud-actor-logging") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmcloud-provider-core") (r "^0.1.0") (d #t) (k 0)))) (h "0sf1dl3abqdkpqzfscz63qpqwmi70lwnlghi7gpm6d02alxmiz1b") (f (quote (("static_plugin"))))))

(define-public crate-wasmcloud-logging-0.9.4 (c (n "wasmcloud-logging") (v "0.9.4") (d (list (d (n "env_logger") (r "^0.8.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.2") (d #t) (k 0)) (d (n "wasmcloud-actor-logging") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmcloud-provider-core") (r "^0.1.0") (d #t) (k 0)))) (h "04a0gah9kcdbp5b3mvn1145sfyamgv80az78wb0984lwrr6s540k") (f (quote (("static_plugin"))))))

