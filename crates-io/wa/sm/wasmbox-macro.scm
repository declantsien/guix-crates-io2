(define-module (crates-io wa sm wasmbox-macro) #:use-module (crates-io))

(define-public crate-wasmbox-macro-0.1.0 (c (n "wasmbox-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "1xw1k8i9l07wiidy0xz6f1vhzqqilnr89wzn16bm0mg5g0x76av3")))

(define-public crate-wasmbox-macro-0.1.1 (c (n "wasmbox-macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0gsj3h4qm5cwpq4qjg6z30ia3zjygp08zy5k96sz26x3b52k68j3")))

