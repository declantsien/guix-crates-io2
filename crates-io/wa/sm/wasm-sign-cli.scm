(define-module (crates-io wa sm wasm-sign-cli) #:use-module (crates-io))

(define-public crate-wasm-sign-cli-0.2.0 (c (n "wasm-sign-cli") (v "0.2.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.3") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.24.1") (d #t) (k 0)) (d (n "wasm-sign") (r "^0.2") (d #t) (k 0)))) (h "11psxm3sw8ym85y9gd4w2r67q8ipvd1lb63d3cyzz3bfrxna8k7w")))

