(define-module (crates-io wa sm wasmer-bus-macros) #:use-module (crates-io))

(define-public crate-wasmer-bus-macros-1.1.0 (c (n "wasmer-bus-macros") (v "1.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)) (d (n "wasmer-bus-types") (r "^1") (d #t) (k 0)))) (h "0br2zj9vv21w3nb8r7jprb7c9j371mma5jr1b5x52smxn3596k68")))

