(define-module (crates-io wa sm wasm-rust-utils) #:use-module (crates-io))

(define-public crate-wasm-rust-utils-0.1.0 (c (n "wasm-rust-utils") (v "0.1.0") (d (list (d (n "clippy") (r "= 0.0.174") (o #t) (d #t) (k 0)) (d (n "memalloc") (r "^0.1.0") (d #t) (k 0)))) (h "0vs4n90bj1p4gkzmqbiq8q4dbwa4yw1axnfipfajs0c5p1fiiji6") (f (quote (("lint" "clippy"))))))

(define-public crate-wasm-rust-utils-0.2.0 (c (n "wasm-rust-utils") (v "0.2.0") (d (list (d (n "clippy") (r "= 0.0.174") (o #t) (d #t) (k 0)) (d (n "memalloc") (r "^0.1.0") (d #t) (k 0)))) (h "1qqxdv563s5ckmb1yidnmhnlfa42zksm63ip13rpbp1ss09j1w9n") (f (quote (("lint" "clippy"))))))

(define-public crate-wasm-rust-utils-0.2.1 (c (n "wasm-rust-utils") (v "0.2.1") (d (list (d (n "clippy") (r "= 0.0.174") (o #t) (d #t) (k 0)) (d (n "memalloc") (r "^0.1.0") (d #t) (k 0)))) (h "1hc5qawd7iia3yld2l9kancas87inl8k9gg4ri13fn6i7lbkz8jp") (f (quote (("lint" "clippy"))))))

