(define-module (crates-io wa sm wasmcloud-provider-core) #:use-module (crates-io))

(define-public crate-wasmcloud-provider-core-0.1.0 (c (n "wasmcloud-provider-core") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "rmp-serde") (r "^0.15.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 2)) (d (n "structopt") (r "^0.3.17") (d #t) (k 2)) (d (n "wasmcloud-actor-core") (r "^0.2.2") (d #t) (k 2)))) (h "1rhm5m3wy9jafn7x1qjf4h6970fs65jw8jpvpfp7cfz9smzfdqwi")))

(define-public crate-wasmcloud-provider-core-0.1.1 (c (n "wasmcloud-provider-core") (v "0.1.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 2)) (d (n "rmp-serde") (r "^0.15.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.115") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 2)) (d (n "wasmcloud-actor-core") (r "^0.2.2") (d #t) (k 2)))) (h "1g1dzh64dgyqcarb5a4xgrqiakxhiq6fs6mxcwpb76i54r86if6h")))

