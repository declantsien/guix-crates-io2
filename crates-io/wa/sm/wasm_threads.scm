(define-module (crates-io wa sm wasm_threads) #:use-module (crates-io))

(define-public crate-wasm_threads-0.1.0 (c (n "wasm_threads") (v "0.1.0") (h "0y271fp0p7qyvmgib90wr6cn6160p3k915fbv63f1q338v8kq61y") (y #t)))

(define-public crate-wasm_threads-0.1.1 (c (n "wasm_threads") (v "0.1.1") (h "16mij8l6dn19n8nmqyzxjgj9jypnj6574cjsrg35408l24698as1") (y #t)))

(define-public crate-wasm_threads-0.1.2 (c (n "wasm_threads") (v "0.1.2") (h "123hpikilv694nivn0dkzibvs20m4m69gxm64gi87p711vjsi85r") (y #t)))

