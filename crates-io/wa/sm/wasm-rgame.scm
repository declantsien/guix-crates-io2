(define-module (crates-io wa sm wasm-rgame) #:use-module (crates-io))

(define-public crate-wasm-rgame-0.0.1 (c (n "wasm-rgame") (v "0.0.1") (d (list (d (n "dmsort") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "raii-counter") (r "^0.1") (d #t) (k 0)))) (h "0386c8wdyc0gykl2lap143ypyw6s4zh4j1a5j6iq325xfdbn47sj")))

