(define-module (crates-io wa sm wasm4-sys) #:use-module (crates-io))

(define-public crate-wasm4-sys-0.0.1 (c (n "wasm4-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rlnfykhv9wz3da62c3ij9sslbsll8kyc3i11ny68k22p4sky717")))

(define-public crate-wasm4-sys-0.1.0 (c (n "wasm4-sys") (v "0.1.0") (h "19p8z3628xxn8nnqsy9x4sdrhl4hxypcl90k35vq1iwaj7s0p5ln")))

(define-public crate-wasm4-sys-0.1.1 (c (n "wasm4-sys") (v "0.1.1") (h "0qbba667kckkw2xv6prlsja7xfdym22sip04m3hpz2sz43645fn5")))

(define-public crate-wasm4-sys-0.1.2 (c (n "wasm4-sys") (v "0.1.2") (h "121byf7jikf95sr7lsijcbcdimkv439dmifkajx85m00bggq9ab2")))

(define-public crate-wasm4-sys-0.1.3 (c (n "wasm4-sys") (v "0.1.3") (h "05f02p85icba9pm8ifs45xf0a7dqqdbpvsxya85vnskn6yqv5wwn")))

