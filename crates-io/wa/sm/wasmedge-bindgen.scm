(define-module (crates-io wa sm wasmedge-bindgen) #:use-module (crates-io))

(define-public crate-wasmedge-bindgen-0.1.0 (c (n "wasmedge-bindgen") (v "0.1.0") (h "06abvgcgjrm3kg4ycdyhzcv5gf5vmqbb9zpxbqj2cadfbqvlcngw")))

(define-public crate-wasmedge-bindgen-0.1.7 (c (n "wasmedge-bindgen") (v "0.1.7") (h "0aip7mr316854dfsn1swxmjcwpwpksrj0ar1kva52gx8rp5zyls2")))

(define-public crate-wasmedge-bindgen-0.1.8 (c (n "wasmedge-bindgen") (v "0.1.8") (h "1l92xzi280cvcjvkggxa431qq4ypn2nmlr5plvw6aqsyjhama42h")))

(define-public crate-wasmedge-bindgen-0.1.11 (c (n "wasmedge-bindgen") (v "0.1.11") (h "1ksv0bhb5ir4qvjrqsr2pdb8jlb204n47qrw7ahpk4iq0wqrkqa6")))

(define-public crate-wasmedge-bindgen-0.1.12 (c (n "wasmedge-bindgen") (v "0.1.12") (h "0hsd64ws6g4cxw3yvdcg3sp9v3dw80nln1q4sddqpl0lpw5sylz5")))

(define-public crate-wasmedge-bindgen-0.1.13 (c (n "wasmedge-bindgen") (v "0.1.13") (h "1m1x128jzz5ll8k8b370xq7aa3bkkw1djrw4gi2a86dc4ckp9153")))

(define-public crate-wasmedge-bindgen-0.1.14 (c (n "wasmedge-bindgen") (v "0.1.14") (h "09qa150xdh13fikfj4sykngch1826pn9s4ysmv64cijaj8yykfpi")))

(define-public crate-wasmedge-bindgen-0.4.0 (c (n "wasmedge-bindgen") (v "0.4.0") (h "02c9hqw0rnqn427000kc15qc5kc68l3ff16p38zjvyczgbr4bf3h")))

(define-public crate-wasmedge-bindgen-0.4.1 (c (n "wasmedge-bindgen") (v "0.4.1") (h "1kbszb0gjkjja1nhl3awvq66kvfh5lskyyx96qn1dzf2yz300gin")))

