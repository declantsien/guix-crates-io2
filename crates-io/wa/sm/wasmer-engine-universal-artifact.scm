(define-module (crates-io wa sm wasmer-engine-universal-artifact) #:use-module (crates-io))

(define-public crate-wasmer-engine-universal-artifact-2.3.0 (c (n "wasmer-engine-universal-artifact") (v "2.3.0") (d (list (d (n "enum-iterator") (r "^0.7.0") (d #t) (k 0)) (d (n "enumset") (r "^1.0") (d #t) (k 0)) (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-artifact") (r "=2.3.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "=2.3.0") (f (quote ("translator"))) (d #t) (k 0)) (d (n "wasmer-types") (r "=2.3.0") (d #t) (k 0)))) (h "0hddz5md13d4gmkzwa43mlfq1xsza9k4pi46dvmmf9hmahzxpwb8") (f (quote (("compiler"))))))

