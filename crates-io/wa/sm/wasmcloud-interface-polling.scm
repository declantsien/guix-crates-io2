(define-module (crates-io wa sm wasmcloud-interface-polling) #:use-module (crates-io))

(define-public crate-wasmcloud-interface-polling-0.1.0 (c (n "wasmcloud-interface-polling") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.13.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7") (d #t) (k 1)))) (h "0nq2snii3p4p9p4igzg5fg0jva3xndypq446cnadhmqd93glhz3a")))

