(define-module (crates-io wa sm wasmtime-async) #:use-module (crates-io))

(define-public crate-wasmtime-async-0.1.0 (c (n "wasmtime-async") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "context") (r "^2.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha4") (d #t) (k 0)) (d (n "smol") (r "^0.1.4") (d #t) (k 2)) (d (n "wasmtime") (r "^0.16.0") (d #t) (k 0)))) (h "063w3ma9qqm80pry6h9brvr14wz1nabgg5rss5b0jl797ha7lihh")))

(define-public crate-wasmtime-async-0.1.1 (c (n "wasmtime-async") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "context") (r "^2.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.4") (d #t) (k 2)) (d (n "futures-timer") (r "^3.0.2") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha4") (d #t) (k 0)) (d (n "smol") (r "^0.1.4") (d #t) (k 2)) (d (n "wasmtime") (r "^0.16.0") (d #t) (k 0)))) (h "1zjks72r9hjj7c4f9bkqwy04h2gn056frlkrv83r3amldkmyrlzs")))

(define-public crate-wasmtime-async-1.0.0 (c (n "wasmtime-async") (v "1.0.0") (h "0c8frq0gagihdm0sinww7597bimq462lcq5k7pq5fbfdbb4wz0rj")))

