(define-module (crates-io wa sm wasm-info) #:use-module (crates-io))

(define-public crate-wasm-info-0.1.0 (c (n "wasm-info") (v "0.1.0") (d (list (d (n "parity-wasm") (r "^0.42.1") (d #t) (k 0)))) (h "04x4d441n4pzva7nc19i8hn5y0ki4fwf8n1zxx7ldsmqbl9vkx75")))

(define-public crate-wasm-info-0.1.1 (c (n "wasm-info") (v "0.1.1") (d (list (d (n "parity-wasm") (r "^0.42.1") (d #t) (k 0)))) (h "0cc69q6q3zhx7qnsbv7qkxmjbk2h8a1ggh8i24nsd0lqzqrb6a2d")))

(define-public crate-wasm-info-0.1.2 (c (n "wasm-info") (v "0.1.2") (d (list (d (n "parity-wasm") (r "^0.42.1") (d #t) (k 0)))) (h "0818gyzji7frj2l5qi1adkhac9vv5b0rp8327f2ijkl7wgva33n8")))

(define-public crate-wasm-info-0.1.3 (c (n "wasm-info") (v "0.1.3") (d (list (d (n "parity-wasm") (r "^0.42.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1") (d #t) (k 0)))) (h "1kibrn8k92a9zqbn3lmv439c40axzxsrx0q8ddy5jhcxz1mscz9c")))

(define-public crate-wasm-info-0.1.4 (c (n "wasm-info") (v "0.1.4") (d (list (d (n "parity-wasm") (r "^0.42.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1") (d #t) (k 0)))) (h "1ba9qbxvlpbx4xhda1c9vv8pf3fhishn03ygrg4fpgi08vrhyh9g")))

