(define-module (crates-io wa sm wasmflow-invocation) #:use-module (crates-io))

(define-public crate-wasmflow-invocation-0.10.0-beta.4 (c (n "wasmflow-invocation") (v "0.10.0-beta.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "serde"))) (d #t) (k 0)) (d (n "wasmflow-entity") (r "^0.10.0-beta.4") (d #t) (k 0)) (d (n "wasmflow-packet") (r "^0.10.0-beta.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "wasmflow-transport") (r "^0.10.0-beta.4") (d #t) (k 0)))) (h "0cpkm6g7pr71k4h997c53jjry5x40mqlisb6jg0z8bss59bsz1wj")))

(define-public crate-wasmflow-invocation-0.10.0 (c (n "wasmflow-invocation") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("v4" "serde"))) (d #t) (k 0)) (d (n "wasmflow-entity") (r "^0.10.0-beta.4") (d #t) (k 0)) (d (n "wasmflow-packet") (r "^0.10.0-beta.4") (f (quote ("all"))) (d #t) (k 0)) (d (n "wasmflow-transport") (r "^0.10.0-beta.4") (d #t) (k 0)))) (h "1wmnfhrhi1c6c57fq2br4sscakgln77kvaydk785i8zk4zms1yxm")))

