(define-module (crates-io wa sm wasm-bindgen-test-project-builder) #:use-module (crates-io))

(define-public crate-wasm-bindgen-test-project-builder-0.2.12 (c (n "wasm-bindgen-test-project-builder") (v "0.2.12") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "= 0.2.12") (d #t) (k 0)))) (h "1l4m1vmrzwdyb1bnahj00rvh4612kf891z1abvf4bdd72zn0c1wi")))

(define-public crate-wasm-bindgen-test-project-builder-0.2.14 (c (n "wasm-bindgen-test-project-builder") (v "0.2.14") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "= 0.2.14") (d #t) (k 0)))) (h "0p9p6y9mlp0xfphgzwxifggf2s7j6fa20vpd6a5inn43m1f9f83k")))

(define-public crate-wasm-bindgen-test-project-builder-0.2.15 (c (n "wasm-bindgen-test-project-builder") (v "0.2.15") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "wasm-bindgen-cli-support") (r "= 0.2.15") (d #t) (k 0)))) (h "0q6js0inaklbznnjsa83vjwn1zyk0nn840jwgadag7h29hjx077k")))

