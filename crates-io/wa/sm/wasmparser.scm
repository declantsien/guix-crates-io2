(define-module (crates-io wa sm wasmparser) #:use-module (crates-io))

(define-public crate-wasmparser-0.1.0 (c (n "wasmparser") (v "0.1.0") (h "14dqxkyskz29lf963yh5pcr0drx8mhjf55hr5z71b36gf6ghy3fj")))

(define-public crate-wasmparser-0.1.1 (c (n "wasmparser") (v "0.1.1") (h "1mv54z20fv15rx8kzn4y4niy41s2g1sgnhk399hn3zj68jcvlm81")))

(define-public crate-wasmparser-0.1.2 (c (n "wasmparser") (v "0.1.2") (h "0ipwwb3dgizx89b1grdy8q31dczpirghgp4m35rmlzc9w9zmiv6b")))

(define-public crate-wasmparser-0.2.0 (c (n "wasmparser") (v "0.2.0") (h "1wk5lv65arnxv9sbvixrvrfw21syar1na5pjp87vf56i08765k2l")))

(define-public crate-wasmparser-0.2.1 (c (n "wasmparser") (v "0.2.1") (h "0jsvdjf94zwda7kxdz9h474w5dlizw5v1k53kdi3v5gjswd9sy3y")))

(define-public crate-wasmparser-0.3.0 (c (n "wasmparser") (v "0.3.0") (h "13gbhh93rfr938g8pawf8gabx7ns9ndljg9cv508nz176y0ymnag")))

(define-public crate-wasmparser-0.4.0 (c (n "wasmparser") (v "0.4.0") (h "13nm6wjhj36q8gz4cza8wgr8lw3vlgq1p6y6r0xmxa4pi546ayg5")))

(define-public crate-wasmparser-0.4.3 (c (n "wasmparser") (v "0.4.3") (h "15iq9zln47mhahvc0ls3gdlf6vcly4bplczm29fiz17vr7adj8ky")))

(define-public crate-wasmparser-0.4.4 (c (n "wasmparser") (v "0.4.4") (h "0d17rhpgiijhclfmbvb48zjrbc0h14r499l1s5lbx9lhzcnaz4b0")))

(define-public crate-wasmparser-0.4.5 (c (n "wasmparser") (v "0.4.5") (h "15ka4bx6rwa4a9yj99wr549x66159d1bn6s3kz7k6dp5sy2kmzah")))

(define-public crate-wasmparser-0.4.6 (c (n "wasmparser") (v "0.4.6") (h "13h02d03468jp0ybbv29lzpqrlrigh6cr12s7yq9y9mdg53r5msk")))

(define-public crate-wasmparser-0.4.7 (c (n "wasmparser") (v "0.4.7") (h "1wr367lk8ikjy60bcsrv7jniz0787102bc4qqiqbg66cm9lycjsj")))

(define-public crate-wasmparser-0.5.0 (c (n "wasmparser") (v "0.5.0") (h "161grqrb32n2v8sjmdmdn9j9fdrgwq5m8n3x5jdkc5yl0fvnsgjz")))

(define-public crate-wasmparser-0.5.1 (c (n "wasmparser") (v "0.5.1") (h "0fq5ac1brg9y33qg9j7yf840n7hbk5mdzpdyqshsad0k0a9h28n6")))

(define-public crate-wasmparser-0.5.2 (c (n "wasmparser") (v "0.5.2") (h "15f5c4gdd5b61dz2qs3ap1yrws3s6zhymcjahswdcd5svb8g3wjw")))

(define-public crate-wasmparser-0.5.3 (c (n "wasmparser") (v "0.5.3") (h "186ar1vnbshizf2g39vxvm7zyhamw188zizf1k6v668nx3zbwxgn")))

(define-public crate-wasmparser-0.5.4 (c (n "wasmparser") (v "0.5.4") (h "187wvmwx9nmhvbyvllwx8xjhsjg2aq5396yjn6yx5vc74pnjjjy9")))

(define-public crate-wasmparser-0.5.5 (c (n "wasmparser") (v "0.5.5") (h "1j8ywdk5cpbbx1391qy4wk87pdl34cxgl1m7drxv7yfiqk3193pr")))

(define-public crate-wasmparser-0.6.0 (c (n "wasmparser") (v "0.6.0") (h "0grz9jqpknxmwzns3dgwvwxsm0x42p7z71zwx5rl08264ik2cis8")))

(define-public crate-wasmparser-0.6.1 (c (n "wasmparser") (v "0.6.1") (h "1663r74mhy4y14mwdmmzf3am4hmg7sxhk26ib2mbhd7hnxgmb69v")))

(define-public crate-wasmparser-0.7.0 (c (n "wasmparser") (v "0.7.0") (h "0mvlasz7a1mxls0gq5ldakh6zavri3b1al6jd4kna3lbx38nimi8")))

(define-public crate-wasmparser-0.7.1 (c (n "wasmparser") (v "0.7.1") (h "0k5dc3habnz4i6j3c322j60w0w1pgcjwpplkdwrc7w5k7a0glavl")))

(define-public crate-wasmparser-0.7.2 (c (n "wasmparser") (v "0.7.2") (h "1a310li2mw8rb1fzvl2m6jrqwbwdmmxxi5arg857fsp2yfncq0ha")))

(define-public crate-wasmparser-0.8.0 (c (n "wasmparser") (v "0.8.0") (h "1944w51vsqhq2rrdj18i3v0npdkacq8hi34f1hw140q3qgcr8ld1")))

(define-public crate-wasmparser-0.8.1 (c (n "wasmparser") (v "0.8.1") (h "0mqbc3l030c878ly2hj0sczlzrh43n8q4hv1vk72fjwzscb6daml")))

(define-public crate-wasmparser-0.8.2 (c (n "wasmparser") (v "0.8.2") (h "1n54lqkqnqijvgr0kp2l5nxhy4xpdnj53ab7npnjw9jf9z1h1p89")))

(define-public crate-wasmparser-0.9.3 (c (n "wasmparser") (v "0.9.3") (h "0jxvcrsya03w9ja85ka8vdv9iz1a5rjf4mzwbgrswcmmpz8vlrzr")))

(define-public crate-wasmparser-0.9.4 (c (n "wasmparser") (v "0.9.4") (h "09ficg4gcd4lakw66s2psnmpsvxpx7rkaz65ik2sc75dbnlgm0d3")))

(define-public crate-wasmparser-0.10.0 (c (n "wasmparser") (v "0.10.0") (h "13w8yrylpcbwwx2827rd7cnhv7lf59yl21pkhsb7kji490a1hz7v")))

(define-public crate-wasmparser-0.10.1 (c (n "wasmparser") (v "0.10.1") (h "1b2k838pbxky8i4vpxajr6ng9l6aqpxb8bdlwxnw1ksdsaxz8mlv")))

(define-public crate-wasmparser-0.11.2 (c (n "wasmparser") (v "0.11.2") (h "0mj6cf40whygk3blhqyy30ds6f8svfzfpnwzdpa7qs5b7bdmbf5k")))

(define-public crate-wasmparser-0.12.1 (c (n "wasmparser") (v "0.12.1") (h "1jfwvi46kfxay6dzm4gmzwhmpzqnwl85xlpg58pns1mjqkpwprgj")))

(define-public crate-wasmparser-0.13.0 (c (n "wasmparser") (v "0.13.0") (h "1zack71yly8wq4w8k70s4wia45a8lw7iwx6dff32550jli5f1xyl")))

(define-public crate-wasmparser-0.14.0 (c (n "wasmparser") (v "0.14.0") (h "0xsrh6c3xbk85smnpi5f8p6f8vlijg3xyx7skfcy0lhkyxcx589i")))

(define-public crate-wasmparser-0.14.1 (c (n "wasmparser") (v "0.14.1") (h "0ky3sswam1pz5kfziy62ajvzmm10c0d6a4rw9agfk60kvmfddjq0")))

(define-public crate-wasmparser-0.15.0 (c (n "wasmparser") (v "0.15.0") (h "1bx8y0876qz5cmjq04g1zjf70d05j70af6dcww3l956455mg6yph")))

(define-public crate-wasmparser-0.15.1 (c (n "wasmparser") (v "0.15.1") (h "07qx10c7n2w58p5rvdmfmaqwdxzmw3hdddahd6lh1k0l53x9zq2l")))

(define-public crate-wasmparser-0.15.2 (c (n "wasmparser") (v "0.15.2") (h "0i8xz1c49890hbpv25i2v51qcxb41v6qrwnqx91r3wlxhjl7azgh")))

(define-public crate-wasmparser-0.16.0 (c (n "wasmparser") (v "0.16.0") (d (list (d (n "hashmap_core") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1r2jd47mbsdcahm5rg8rxxzgwhhajmbmwrqkrm16jc74d9mbdkyh") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.15.3 (c (n "wasmparser") (v "0.15.3") (d (list (d (n "hashmap_core") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0c17qvpsac6pc6l34azgjhfpxsxri0l3vb3r5mkyzh757m4ndf19") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.16.1 (c (n "wasmparser") (v "0.16.1") (d (list (d (n "hashmap_core") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0rrfib0a8gzrsm3cp4ld41n0s01nxn7w36rqvmg1hcnyrb5bwn2f") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.17.0 (c (n "wasmparser") (v "0.17.0") (d (list (d (n "hashmap_core") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1b0hjabcp5hhz003g4kryidq8cn93f33l33mnlvdbij0awkmyhas") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.17.1 (c (n "wasmparser") (v "0.17.1") (d (list (d (n "hashmap_core") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "03q6lxxivlrm4kqjc2mlrczpf9qa1vqf0f1xzy67bwg1shyfzsar") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.17.2 (c (n "wasmparser") (v "0.17.2") (d (list (d (n "hashmap_core") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1607npz4b8xfap8pm8xw1bn8z2sfvahwrz2l4az7aqbrlriqmlgy") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.17.3 (c (n "wasmparser") (v "0.17.3") (d (list (d (n "hashmap_core") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "1jn42q4dxcj0nc5nnc1r3r7yp9lm0khlh3gqk1vhsq9kmldm3ihg") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.18.1 (c (n "wasmparser") (v "0.18.1") (d (list (d (n "hashmap_core") (r "^0.1.1") (o #t) (d #t) (k 0)))) (h "0bhsaifvmcrpjl7m2xnkywj8rk065742fk9vsi7idk53cykwps6m") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.19.0 (c (n "wasmparser") (v "0.19.0") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1k43l5g3vrinv9pyrs9mcm9ixiba7nnn7q7ncxbzc960fc5j7323") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.19.1 (c (n "wasmparser") (v "0.19.1") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "173vh6kddcwfvdnnzzpm9zxmcqz34mb1lb2j9afyv9620iwhlxif") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.20.0 (c (n "wasmparser") (v "0.20.0") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1p3yp807ks5zjs2rxa6x54g7k92vw1dqld0137clykcszv1y0x9a") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.21.1 (c (n "wasmparser") (v "0.21.1") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1r2ihmqjhs5qfbpbwhm31xd3l3x647mb09fz7pvrh004hfrcg7hn") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.21.2 (c (n "wasmparser") (v "0.21.2") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "168k6a9ih4bhx28mhwjmfh02zdy5lm2724xlx2p6vg0vlnrfid0m") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.21.3 (c (n "wasmparser") (v "0.21.3") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0n4na613lqmzzvwf681z0lqm4cxp2nfym0vx1wbasglaamasdh4d") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.21.4 (c (n "wasmparser") (v "0.21.4") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0f07cl2nq3jh055v495cz4bi4312d2c8nd74zc4alfn8y4sffkg8") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.21.5 (c (n "wasmparser") (v "0.21.5") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "09m0af15qps72il4724sl8qd5h1ly8ch61jmpwpk7wnzp87h3022") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.21.6 (c (n "wasmparser") (v "0.21.6") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0i5m59wdipzc6q7vng2r8lqafb3p5b2sdxv4rc5k84m58a5df516") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.21.7 (c (n "wasmparser") (v "0.21.7") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1whjj9ixqpdx0i09zl2mkydzbicflgc8imdir1c9f1pyy8cvkjgj") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.21.8 (c (n "wasmparser") (v "0.21.8") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0fggc2x4vw4zjvk7vhh9ily01rfjzncy1032zswava4sv7a4qbi0") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.21.9 (c (n "wasmparser") (v "0.21.9") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0h0b3zd80j0n6ks4qp1m90001w4imhdzb3wasncml0s1b3bxn7gd") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.21.10 (c (n "wasmparser") (v "0.21.10") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0vyznvz3p08f5909gp5379bafn0g4wf4jgq4zzh51zv4ngzfpf28") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.22.0 (c (n "wasmparser") (v "0.22.0") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0bxxvvcz3xyw1ab1mdlbx6gdx8yhyw1m3k63ylzyda0lwdk0ykhv") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.22.1 (c (n "wasmparser") (v "0.22.1") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0p46jpdymqnsrpn2i8akv9q7wckzq78gjjcsln1n8h2ardp6cvpl") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.23.0 (c (n "wasmparser") (v "0.23.0") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0q6h4rq1hmwhrhi7221lifx65587gcbf2hnjidvnxly71d11rq5m") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.24.0 (c (n "wasmparser") (v "0.24.0") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0janskkzhqdxxikrdg7k65dqnzk6j2wk7vka76kcpi6fjsv74kn4") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.25.0 (c (n "wasmparser") (v "0.25.0") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "11hhc4bgfqqnbvb31z1xlnygrrqakv0jxg939602gs64r3wvbyyb") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.26.0 (c (n "wasmparser") (v "0.26.0") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1gpx6j4c56yfwbhg5h0zplzn8nfanj8l9nybyli41swf4dxhryrh") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.27.0 (c (n "wasmparser") (v "0.27.0") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "0rd0x06hvgv13ipli35g7z4rdqswnpkvzfankykcf9x7nbrda5gm") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.28.0 (c (n "wasmparser") (v "0.28.0") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1h86n886yhjg4i6slrwmqi2ih3ajiqm2ww0hzcbnbllvjaqjdx20") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.29.0 (c (n "wasmparser") (v "0.29.0") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1cyan6sjhdbxwrpang4lq7s9ik6lddzjf7vng038m5hr6lx981g1") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.29.1 (c (n "wasmparser") (v "0.29.1") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "1i7g87hw15brqanm9mb4gylz0l74s37s85kybvx41g3yps8sy4b1") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.29.2 (c (n "wasmparser") (v "0.29.2") (d (list (d (n "hashmap_core") (r "^0.1.9") (o #t) (d #t) (k 0)))) (h "004dqm51xq6fwq7rybg1mvgs8yfb67kzligc6c12wxl9rybqf6lq") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.30.0 (c (n "wasmparser") (v "0.30.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashmap_core") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "0cavc4hxi457l3yjxz6387zwv4a1x0k7702rmwr1lyi6lbprwsjn") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.31.0 (c (n "wasmparser") (v "0.31.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashmap_core") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "1lascy9xvsb6jysdhba53m6j8hl41p2c8wqkiwyim72w4v93gn7z") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.31.1 (c (n "wasmparser") (v "0.31.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashmap_core") (r "^0.1.10") (o #t) (d #t) (k 0)))) (h "15yvchc3sia3b9dmf2pixx5gf80w9a2sx7f4pf185z85zi534vwa") (f (quote (("std") ("default" "std") ("core" "hashmap_core"))))))

(define-public crate-wasmparser-0.32.0 (c (n "wasmparser") (v "0.32.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "171zsyn19viz01lfcwi3z3qys01mq886qmjfiabp961bqbzqlzrx") (f (quote (("std") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.32.1 (c (n "wasmparser") (v "0.32.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0jk734smpl11m59lnhfbs4v3mqsfd6bi0ab5yvd2s48gwcfq1l92") (f (quote (("std") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.33.0 (c (n "wasmparser") (v "0.33.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "19qrnaszm30vfjn1ml5dbn0d9bjbh42r3wygq7d055lpyfbd1h81") (f (quote (("std") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.34.0 (c (n "wasmparser") (v "0.34.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0l0qc791rbadqliylm6rrsd95h57x59bgza736a92ai4gidfqc40") (f (quote (("std") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.35.0 (c (n "wasmparser") (v "0.35.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0xb3j5xm717b8hb9nw18v542j723pgvqzl10gij7cv30hsrdrf4g") (f (quote (("std") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.35.1 (c (n "wasmparser") (v "0.35.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1y05fz84gy6x22xa55wxaa7dmvvi6cbvhzz1qh6nh8n60dhivx99") (f (quote (("std") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.35.3 (c (n "wasmparser") (v "0.35.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "02vn9ysz937v6gz4rrf3alvyjikan4lkq9apmgcx7yjzcdvsz6h9") (f (quote (("std") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.36.0 (c (n "wasmparser") (v "0.36.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.1") (d #t) (k 2)))) (h "0v2ja4qbg9bhg0q08dcv4w5qm51i750n0hidjnnnbdnxp4jlq0ca") (f (quote (("std") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.37.0 (c (n "wasmparser") (v "0.37.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.1") (d #t) (k 2)))) (h "1dhxw74vvy3yffaxlfycpwz1654nry4pim4z522xbnlm15lfmnw2") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.37.1 (c (n "wasmparser") (v "0.37.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.1") (d #t) (k 2)))) (h "0dxixpvwki5v98anv9ff87bhq53pqy7kba9i3anxi6d3vxiycabk") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.37.2 (c (n "wasmparser") (v "0.37.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.1") (d #t) (k 2)))) (h "1ssyxc15cnx81lclwmi8z24yibkm6drix5kx3p89rp8kgjk7nf7p") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.38.0 (c (n "wasmparser") (v "0.38.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 2)))) (h "1kbq0cmr5pqjscmpylyn150cldh57sknfgbv15vw80drmrl961x4") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.39.0 (c (n "wasmparser") (v "0.39.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 2)))) (h "0qlc10023kgwrdw9v29m76g0sx2cy18bhmxysxfw0z4cvvqdlwk7") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.39.1 (c (n "wasmparser") (v "0.39.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 2)))) (h "07198ybmlyc8dqpfaps46r77ayhzz1s7872nqwvmb7gld91w29m0") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.39.2 (c (n "wasmparser") (v "0.39.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 2)))) (h "10nds3x7hbrbvw8wzbdrviqmljz5gphywcgi2l5xxxslji23n275") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.39.3 (c (n "wasmparser") (v "0.39.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 2)))) (h "0w2ldh8nw23pibqxp94dp7c7kkmrwm9xkr19pkzyxzm5rm5920n7") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.40.0 (c (n "wasmparser") (v "0.40.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 2)))) (h "0nabpdiy9cwrcmgvxlxahvjkk7f3qaxlcnz1k7lifa243w9a745x") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.41.0 (c (n "wasmparser") (v "0.41.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wabt") (r "^0.9.2") (d #t) (k 2)))) (h "19w03x6bq6vh4nnjanm0c9ds0yk78231i6jz4canzplc9d06ws95") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.42.0 (c (n "wasmparser") (v "0.42.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wast") (r "^3.0.4") (d #t) (k 2)))) (h "02iyclv4946xdpc8jsihxm4p5gqsglbcs0dmfpmq6gvsgbdy3mdd") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.42.1 (c (n "wasmparser") (v "0.42.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wast") (r "^3.0.4") (d #t) (k 2)))) (h "079irg3gsrmdzwk0v5zw9nnpir3la8m0wsy055gj31fmbd5ch9qm") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.43.0 (c (n "wasmparser") (v "0.43.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wast") (r "^3.0.4") (d #t) (k 2)))) (h "0b3b42ba10ipz840raj2v52llzcv6b8m9vjx8v94drdxfhcz11pg") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.43.1 (c (n "wasmparser") (v "0.43.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5.0") (o #t) (d #t) (k 0)) (d (n "wast") (r "^3.0.4") (d #t) (k 2)))) (h "1wzhpmwbjqvzj95fd76h340an5qgqr9h31pibjcfl47h4c1pzz1z") (f (quote (("std") ("deterministic") ("default" "std") ("core" "hashbrown"))))))

(define-public crate-wasmparser-0.44.0 (c (n "wasmparser") (v "0.44.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "wast") (r "^3.0.4") (d #t) (k 2)))) (h "0fjyp11xsrz96kya4cm0fjr96yfjc6qqkayi1nj9p7i08shn9g5s") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.45.0 (c (n "wasmparser") (v "0.45.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "wast") (r "^3.0.4") (d #t) (k 2)))) (h "081h5mb0cbclbgv24spnzqrmnxa8hr6d6azv54z7q0i5if22q96d") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.45.1 (c (n "wasmparser") (v "0.45.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "wast") (r "^3.0.4") (d #t) (k 2)))) (h "0fyhk3k6hbdlg0gs2v6gnwhhv5h8xmf80mmfh4iplpdacydkrqjf") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.45.2 (c (n "wasmparser") (v "0.45.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "wast") (r "^3.0.4") (d #t) (k 2)))) (h "1m905yj43vc072v1qdi5pnsgq17barxn38yb54vq1l3ik4fsnklb") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.46.0 (c (n "wasmparser") (v "0.46.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "wast") (r "^3.0.4") (d #t) (k 2)))) (h "1yhf4lj5x4zg3j90m7kgqxy1ba5jxcjn9wvmr977qsb9fc6sr6k4") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.47.0 (c (n "wasmparser") (v "0.47.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "wast") (r "^5.0.0") (d #t) (k 2)))) (h "0frrilyn7kncmpfabs71m7bk7daa5jc43chq6kqn8brslnsqvp8s") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.48.0 (c (n "wasmparser") (v "0.48.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "wast") (r "^6.0.0") (d #t) (k 2)))) (h "0i3h6wimipciyvciy3dywclryl2246z45yh4ibmqwc3z2n94ig3z") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.48.1 (c (n "wasmparser") (v "0.48.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "wast") (r "^6.0.0") (d #t) (k 2)))) (h "01yc6ynw20z7aha1c98rdbq4wbh9kd2i64fmyi93way75ycajhqq") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.48.2 (c (n "wasmparser") (v "0.48.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "wast") (r "^6.0.0") (d #t) (k 2)))) (h "1y50wkfsprxr98wy7lcgsaif622a3c66dkk8vl0b0kf8y6dshg87") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.49.0 (c (n "wasmparser") (v "0.49.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "wast") (r "^6.0.0") (d #t) (k 2)))) (h "1yw49yc1qxlmykcrpdj19cjdyhx3qzvswrpq3sbgppnplgpwyy3d") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.50.0 (c (n "wasmparser") (v "0.50.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "wast") (r "^7.0.0") (d #t) (k 2)))) (h "0wl5w2j3nckiadkhsvsf51dgr2w92rhshi8h27ghji4b084kcr1i") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.51.0 (c (n "wasmparser") (v "0.51.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "wast") (r "^7.0.0") (d #t) (k 2)))) (h "0h7v75x6sdyj98c3y4babx543h4l705n6aprbbxzbjb4vr9a8q38") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.51.1 (c (n "wasmparser") (v "0.51.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "wast") (r "^7.0.0") (d #t) (k 2)))) (h "1m2vzrj116zvgya34b6fzvvi9fysap5fyjfy2p0jizkp2rxb4hcy") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.51.2 (c (n "wasmparser") (v "0.51.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "wast") (r "^7.0.0") (d #t) (k 2)))) (h "0p7ikrcmnrl32jhw18mlff33ixng429034n2kr2lpwm32kqj83d4") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.51.3 (c (n "wasmparser") (v "0.51.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "wast") (r "^7.0.0") (d #t) (k 2)))) (h "1yi1cb1jr6vdh7a8751azf3rr0z00803mvqri68xbv7mbjxnax24") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.51.4 (c (n "wasmparser") (v "0.51.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "wast") (r "^7.0.0") (d #t) (k 2)))) (h "02n1wrjs5y1njzqcwvziv5cgb82sgfgd4ng4cdg1r7a635mrbcdf") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.52.0 (c (n "wasmparser") (v "0.52.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "wast") (r "^7.0.0") (d #t) (k 2)))) (h "04v51namygc5milpg6dzpsjv50iq7b8ycdizzdypvh8fpcjg9bvx") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.52.1 (c (n "wasmparser") (v "0.52.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "wast") (r "^15.0.0") (d #t) (k 2)))) (h "0wxhkgzwr78mnp0xky3m78qpdlabwyj5dznx4nbf2n5idrcqpnsx") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.52.2 (c (n "wasmparser") (v "0.52.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "wast") (r "^15.0.0") (d #t) (k 2)))) (h "0ccvlxjm218vagjfi8fkjqqh06ykdw96a2p674j60f8b7h158fbk") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.53.0 (c (n "wasmparser") (v "0.53.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "wast") (r "^15.0.0") (d #t) (k 2)))) (h "0a5fhhvmsn1v2pf6sf8f9bs9q0lwlvy3b16yhivv740miwifcgmp") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.54.0 (c (n "wasmparser") (v "0.54.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "wast") (r "^15.0.0") (d #t) (k 2)))) (h "0n8xfraam6ha3bkwgjlvhdl7fkhpf0m06gk4yrn80h1623d6fx3l") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.55.0 (c (n "wasmparser") (v "0.55.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "wast") (r "^17.0.0") (d #t) (k 2)) (d (n "wat") (r "^1.0.18") (d #t) (k 2)))) (h "11ra70bmn4ir8wxgzj5ymhsny0ydrb2gwqxh5153zib034p1x4xg") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.56.0 (c (n "wasmparser") (v "0.56.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0s0qq7kg1p93yj2kb4bijfvxld84mhgfzdcrp38n38a9c9pbfnr9") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.57.0 (c (n "wasmparser") (v "0.57.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "19kslk9pv1bcyp85w63dn1adbp13kz7kjha80abnwz27bmbxvz9j") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.58.0 (c (n "wasmparser") (v "0.58.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1nw61v0hzdavvwx9gm8s1dj65n2cy3wdqziryspdff1p91wqs6kj") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.59.0 (c (n "wasmparser") (v "0.59.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1fd9xp3miw45gxwn06bmhb9j0cdml2r4bx2ga7ylf8gn32kfcl59") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.60.0 (c (n "wasmparser") (v "0.60.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1jx5zzxp3bjqyi24w37sfixq62iipk4li7dlc5lr84faqxdkjh6g") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.60.1 (c (n "wasmparser") (v "0.60.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1gpwlmp8aryz43g27hgxc5jr48q6p6ailjpwyjjp7vmxwnh16569") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.60.2 (c (n "wasmparser") (v "0.60.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0zxcbahgdi0kfwa3hnnwwcav4x6zaa8w9xk7axmks7zi1057kmf2") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.61.0 (c (n "wasmparser") (v "0.61.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "139cdby6y6278vdqgllk8zfvzbgk1i594hcm1znld56a3rzpb6ps") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.62.0 (c (n "wasmparser") (v "0.62.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1klkklijaixq91f1mpx1xnzrx2737r591sy90sk3xn558625nsz3") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.63.0 (c (n "wasmparser") (v "0.63.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1ddp6kr246hs40pxxhb9dn9ghspf85p77yydngc7b3a201rmvnjp") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.63.1 (c (n "wasmparser") (v "0.63.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0gn263p3adb71l4gxjyk1a5iixqhv9p1y1ynkba6ckq377hvrr49") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.64.0 (c (n "wasmparser") (v "0.64.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0mkqbbgbfj8rmkp4ck53a223lbczambq0qrx3yw0wlahkll8m1kr") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.65.0 (c (n "wasmparser") (v "0.65.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1krz687dxd4y6m2k3yn1knw7hpa0mbkh22ds53jv8d086pk2zk47") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.66.0 (c (n "wasmparser") (v "0.66.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1xp6761q2mw9gbpramcfzb6daacg9r7w6cxwcixwl2bddm2w0zy9") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.67.0 (c (n "wasmparser") (v "0.67.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1l0a7xaq0s35ns1wans304i9yibpj8bgynr5c1mffpwyhkriq2cz") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.68.0 (c (n "wasmparser") (v "0.68.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0k3xqhza2yb3k9v29pk5i60bc8cxyj2b4kyxvfxyrhnrxqa0x819") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.69.0 (c (n "wasmparser") (v "0.69.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0c812bx8l10fc9xk0h0pkqsd0vkr8n051qfzcms5784465p5lpdx") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.69.1 (c (n "wasmparser") (v "0.69.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0i6rpsmda75qdi0whli6v8iiglriky09pnw1dyfilffgdc3cc6gx") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.69.2 (c (n "wasmparser") (v "0.69.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1a4l31i22pjprd4kzqk8pwc83ap00g0hxk3v55qkjwisvzddcbgx") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.70.0 (c (n "wasmparser") (v "0.70.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "18n26q0f2fm05hqyx544zdfjp4mizm6j309jkw5mh6phkjg3y6zd") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.71.0 (c (n "wasmparser") (v "0.71.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1waz2jy1727r9l8vjv7603gmikq0hprgqaw04vlfsabq8fchr8w9") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.72.0 (c (n "wasmparser") (v "0.72.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "00fspab73hz2ch7iybqlp17xrw7a5rc41ran96s9yzs05n3lvprc") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.73.0 (c (n "wasmparser") (v "0.73.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "07f6hcpcckbgwf3586fa1h31p891b6hj80gnyv0ws1644gmi2l51") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.73.1 (c (n "wasmparser") (v "0.73.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1rqfwy97kxsc55gapb5da4svirrsj5a8kjc3ljar9i6b66qnllmq") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.74.0 (c (n "wasmparser") (v "0.74.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0y1w5iqd5zmmxqxfvlxbmajhkdpk84l0y8nf9cns5n91hih66kaa") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.75.0 (c (n "wasmparser") (v "0.75.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0r4w6ga0m9sc63pr7kmkazxyz4290iiq7z1if4kw7kr9ffzbd025") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.76.0 (c (n "wasmparser") (v "0.76.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1n4704fmq9c4p9z58gpr06q61wjcykp6bsfpwsxwqv1zzr59lnkm") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.77.0 (c (n "wasmparser") (v "0.77.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1mm82j4mzfqkyijd80vxzqd5100is1rdggpbakchf2kj5v98cp5k") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.78.0 (c (n "wasmparser") (v "0.78.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1dj7alafghax9igf7jm504ds2n99pxlfzq1l5jfc0hvd60h8nykc") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.78.1 (c (n "wasmparser") (v "0.78.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "031vlw45y0r83v2z48frmqkhmmv2igwmrvsdsjfsa901bli5b4ig") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.78.2 (c (n "wasmparser") (v "0.78.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0rgx29yg656rrwras0k0blqy8k414bxfbf7abh2qpkz5g164s52j") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.79.0 (c (n "wasmparser") (v "0.79.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "03kcssgnsqicr3jm54d6nm1zh0jzsghh0ir5g5bwhnd52nz98n2v") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.80.0 (c (n "wasmparser") (v "0.80.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1kzj2qn36nfbpa2as7ybn514yavdjnyd3rqr168m0ghrp201pxx5") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.80.1 (c (n "wasmparser") (v "0.80.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1jid9xld3iwra0c42c9ikpkbkyh22jzy76xjfshv5x2smbfbd4my") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.80.2 (c (n "wasmparser") (v "0.80.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0ssa6d22qyc4d7kwhj54hsay142fh392ipjcyazs3496hgi6g4a4") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.81.0 (c (n "wasmparser") (v "0.81.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1g4s5dmvrjibkwvp25fcvglflv3ncwpw5zbfhc5d0qwza53094wq") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.82.0 (c (n "wasmparser") (v "0.82.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0w5x2b1wcclrkaff6gjgv29zd57afy44jcs9i5phs93r2w7wqn85") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.83.0 (c (n "wasmparser") (v "0.83.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0yhx2kq7da4sdglh1x1di4xxg33k7lwddpd3ri46bp9abk2xg3ki") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.84.0 (c (n "wasmparser") (v "0.84.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1ckl14685hgj4hag6fpgypphc0nk2a4fsnvlgfj4kkmm5g19gp3p") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.85.0 (c (n "wasmparser") (v "0.85.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1rv32jl0pr3nr7ff16h0xjy38v41nzdsxsph5l6ibs91ig2n012p") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.86.0 (c (n "wasmparser") (v "0.86.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0p4gbrlxqx79j2qrnwjsqxbyn4vm8b47z1biy5zsm8kx8jazxjsb") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.87.0 (c (n "wasmparser") (v "0.87.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0xd800fh9y6sq5fa5qvgxvz6nk4v27rjr2njkdpwp3ifrl3y412w") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.88.0 (c (n "wasmparser") (v "0.88.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0plfwchdmq9spyyv7sgnxcr6yn8mvrzxbp5yc60yczs0hbfzg37v") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.89.0 (c (n "wasmparser") (v "0.89.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1nk02kav3bgaf0n8lyb3cvqa02v11i54ljcdqkjxsy279akar4qy") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.89.1 (c (n "wasmparser") (v "0.89.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1vysjrn8f98ry6nxacj9i12hafl8shyd022nsmnzjxiqn443wpdb") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.90.0 (c (n "wasmparser") (v "0.90.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "10c5c56qprg4y2gpyzjg46iynvc89485misd67pfy8s47y28sb5n") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.91.0 (c (n "wasmparser") (v "0.91.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0jp2vkcckcqbz67f3m1fhdh5p91xlbzda8n5320sympkp2ldr713") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.92.0 (c (n "wasmparser") (v "0.92.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "indexmap") (r "^1.8.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0dqv3bv7lr19j4lm2pgb97az19ypi3ljd2yzdj8dn8wc5bn4r8vx") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.93.0 (c (n "wasmparser") (v "0.93.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0a4k0h3alk33r6khzfp363xn6fcgf8z00pba1ccglwg2lc54d965") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.94.0 (c (n "wasmparser") (v "0.94.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "14ginwlxj1d31c528jn4skgppsn835sds8s97fp162fpk0fpxb6d") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.95.0 (c (n "wasmparser") (v "0.95.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0yn8ma731gpzra72k0sh84v8h3db07df2js1698v36gafdi8kspj") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.96.0 (c (n "wasmparser") (v "0.96.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "06myqbcjmpnnjcqa3mawbdigi313ajwhpvf81v8sbf8swjnh3pmd")))

(define-public crate-wasmparser-0.97.0 (c (n "wasmparser") (v "0.97.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1wqm3dzsx8x3pg4zg4wpp3cnav6nrcbb2ccj4f395kxssah270dr")))

(define-public crate-wasmparser-0.98.0 (c (n "wasmparser") (v "0.98.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1kjdvy4r6vvpmgzi6l1d42pqxckkvmjkwkb0jdjdv8wy8l75jq50") (y #t)))

(define-public crate-wasmparser-0.98.1 (c (n "wasmparser") (v "0.98.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "0dkgcsll7bhnh2i7hcb2d48ap7zdnzc4npq5kjbram2rvhjcf947")))

(define-public crate-wasmparser-0.99.0 (c (n "wasmparser") (v "0.99.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1lq1vr7xy504b22ccn6dxsh3agnx4z0h4krd867q8zy6mwbvgwwy")))

(define-public crate-wasmparser-0.100.0 (c (n "wasmparser") (v "0.100.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.0.0") (d #t) (k 0)))) (h "1m39s83pv6bfnqw8hfdq847ayrmgkahi5kv2pgglfhb2mcv05ck4")))

(define-public crate-wasmparser-0.101.0 (c (n "wasmparser") (v "0.101.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.24.0") (d #t) (k 2)))) (h "1xm2wk0kxbk5avg95bw0zyy11xd9qq9z1qlmrj126ha7kqnj5hww")))

(define-public crate-wasmparser-0.101.1 (c (n "wasmparser") (v "0.101.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.24.1") (d #t) (k 2)))) (h "0c5ib29iq2fq9xq1xrnj378g65iyy4kg6lps99a6cmmchkpj4bxz")))

(define-public crate-wasmparser-0.102.0 (c (n "wasmparser") (v "0.102.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.25.0") (d #t) (k 2)))) (h "0jqh6p7w5kng9vz1k1bblwfx6l4fbnqr2sxgksmik0jrszils4s8")))

(define-public crate-wasmparser-0.103.0 (c (n "wasmparser") (v "0.103.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.25.0") (d #t) (k 2)))) (h "0yzfnp821kqqfvk0mh4h7nzgyl8pyx8qsr1x27qq9sn5r9rp6hrc")))

(define-public crate-wasmparser-0.104.0 (c (n "wasmparser") (v "0.104.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.26.0") (d #t) (k 2)))) (h "0zaa4mz5gd1nshxq8mq3xahphfv99dga7miic6bssmkw3bw6lfba")))

(define-public crate-wasmparser-0.77.1 (c (n "wasmparser") (v "0.77.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0vxgdmk8yz0ijrskm0h76rxqnbci4046wvb5g8qw39lybr0dbqsz") (f (quote (("deterministic"))))))

(define-public crate-wasmparser-0.105.0 (c (n "wasmparser") (v "0.105.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.27.0") (d #t) (k 2)))) (h "0cpcap0yalv5jycjn8v42lhp6b03knwfffm3g4cxqw4m7w5rxgl3")))

(define-public crate-wasmparser-0.106.0 (c (n "wasmparser") (v "0.106.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "url") (r "^2.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.28.0") (d #t) (k 2)))) (h "0kb2c2r98rbgzq6nn3rfdc8dx129jyyb0jb3z9aidffajcvy656h")))

(define-public crate-wasmparser-0.107.0 (c (n "wasmparser") (v "0.107.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^1.9.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.29.0") (d #t) (k 2)))) (h "1jvs1ps5iixn16r9s725wg37qw6nsbbaalksmh6dlz8cg2dsrqr9")))

(define-public crate-wasmparser-0.108.0 (c (n "wasmparser") (v "0.108.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.30.0") (d #t) (k 2)))) (h "1i9nbrz9gv2jycz2kqaqn7habl72nvckj49r75m46hfbkl85djbn")))

(define-public crate-wasmparser-0.109.0 (c (n "wasmparser") (v "0.109.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.31.0") (d #t) (k 2)))) (h "0v3fsg7qvnymmp2xz3ac0kpwdplj58nzasj06kp90a6y557mdycb")))

(define-public crate-wasmparser-0.110.0 (c (n "wasmparser") (v "0.110.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.31.1") (d #t) (k 2)))) (h "0g214qyjbgrbmcdk0dy3snmdnfs2ww103wkbbg4fc0bgv5rdpz0x")))

(define-public crate-wasmparser-0.111.0 (c (n "wasmparser") (v "0.111.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.31.1") (d #t) (k 2)))) (h "16iiximfhr8ggrgaavyg85mxsxpiykj9fvjla69b1xm3mmm06wdd")))

(define-public crate-wasmparser-0.112.0 (c (n "wasmparser") (v "0.112.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.32.0") (d #t) (k 2)))) (h "1gvp11fqkgjvg2pfhfmm28pq6dr7bng5ypgaz2ff9k3zyh8b11p9")))

(define-public crate-wasmparser-0.113.0 (c (n "wasmparser") (v "0.113.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.32.0") (d #t) (k 2)))) (h "029y51sgw7as65a4xq91rsisbl7bxk6h2fhazzxbbshgk9bpq8ny")))

(define-public crate-wasmparser-0.113.1 (c (n "wasmparser") (v "0.113.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.33.1") (d #t) (k 2)))) (h "1nv8wbpdsiwr910j6yfk9i2x065sm4d0n2mi86mh6rsip2kwwa51")))

(define-public crate-wasmparser-0.113.2 (c (n "wasmparser") (v "0.113.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.33.2") (d #t) (k 2)))) (h "1wwq9q30ziiij2y542sqlhb6bxzf4j9kkwsjwc289mqbmd7x9l0z")))

(define-public crate-wasmparser-0.113.3 (c (n "wasmparser") (v "0.113.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.33.2") (d #t) (k 2)))) (h "1bh7rnv8v8rkmz5gcgfcqzzspbr4d3lin5vkhydd0nsskf24jq18")))

(define-public crate-wasmparser-0.114.0 (c (n "wasmparser") (v "0.114.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.34.1") (d #t) (k 2)))) (h "18sy66a218zx1n6mkiaxbiirvvzqk0ipn6bdxcvv026b1m0i3wlf")))

(define-public crate-wasmparser-0.115.0 (c (n "wasmparser") (v "0.115.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.35.0") (d #t) (k 2)))) (h "1mbwahxp61vviwfv5bqydzvldmcg4zja3cycf6x7kn5dli0hcv70")))

(define-public crate-wasmparser-0.116.0 (c (n "wasmparser") (v "0.116.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.36.0") (d #t) (k 2)))) (h "18bf63yca40xpsn63qp7j0bga0ccachakcagd5yx9hn5fq90naak")))

(define-public crate-wasmparser-0.116.1 (c (n "wasmparser") (v "0.116.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.36.2") (d #t) (k 2)))) (h "0l7scjcvkdvrsqiq20yhr1piy5jn8xjswhl2gfq0qd6q1nw2i3m5")))

(define-public crate-wasmparser-0.117.0 (c (n "wasmparser") (v "0.117.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.2") (f (quote ("ahash"))) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.37.0") (d #t) (k 2)))) (h "1azm6xkf9l4wpbc2q5wgnsga3q13ar2vjbzga45rzbwjr7h6s84v")))

(define-public crate-wasmparser-0.118.0 (c (n "wasmparser") (v "0.118.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.38.0") (d #t) (k 2)))) (h "092rqyar86mm39x33mzrlag5xpg7fwkv2kdi6bnis0g09mbr3fzb")))

(define-public crate-wasmparser-0.118.1 (c (n "wasmparser") (v "0.118.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.38.1") (d #t) (k 2)))) (h "1ngw3g39yrb6pd5bann8j3j60ymhwzmckph063akbrr8p4irgvlm")))

(define-public crate-wasmparser-0.119.0 (c (n "wasmparser") (v "0.119.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.39.0") (d #t) (k 2)))) (h "1397nlxba6r9xm9wracwm4nfr18h755189b604a9nkzvgbvxldcc")))

(define-public crate-wasmparser-0.120.0 (c (n "wasmparser") (v "0.120.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.40.0") (d #t) (k 2)))) (h "01cp70sf3j8yma6jd1bpaqhvmkbfn51593gfxx1zxgwwyckq2579")))

(define-public crate-wasmparser-0.121.0 (c (n "wasmparser") (v "0.121.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.41.0") (d #t) (k 2)))) (h "1a7x5ga40w3y2vdfw4lfgw6afyz70gj5mahwrf117cvac2kzcg4m")))

(define-public crate-wasmparser-0.121.1 (c (n "wasmparser") (v "0.121.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.41.1") (d #t) (k 2)))) (h "01bc5q68zawymyjz22zira4iqj6mikirw77n4j3spsx199my3zwr")))

(define-public crate-wasmparser-0.118.2 (c (n "wasmparser") (v "0.118.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.38.1") (d #t) (k 2)))) (h "0365n7dd01fh2jklq6q41nszhyyaz82shd4q3nhf4s5q397ibwbp")))

(define-public crate-wasmparser-0.121.2 (c (n "wasmparser") (v "0.121.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.41.2") (d #t) (k 2)))) (h "1aza0g3v49nqsc856fd7x8zwrh4hzy4si9a7jifx5nyhz745bglx")))

(define-public crate-wasmparser-0.200.0 (c (n "wasmparser") (v "0.200.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)))) (h "0qmhahrwr8v8ffqgsb3wx766f04rwj7knv7zaw6184k6hyn6agx0")))

(define-public crate-wasmparser-0.201.0 (c (n "wasmparser") (v "0.201.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)))) (h "027pdclgl642hqzdi592nnhdf7j570bhyi9sqsppy3bcp9nxzrc4")))

(define-public crate-wasmparser-0.202.0 (c (n "wasmparser") (v "0.202.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)))) (h "04qljgwjv6a6nn9sx6bbz167s0dim4liphgp1sc8ngygscaqb6fn")))

(define-public crate-wasmparser-0.203.0 (c (n "wasmparser") (v "0.203.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)))) (h "0dwx6jzszp4v4818mr2il0m5cph7926jqkkz6z4fi0phzwskniqv")))

(define-public crate-wasmparser-0.204.0 (c (n "wasmparser") (v "0.204.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)))) (h "11b51n14ypg19n7hbazjb58rzc3299lxyh4xfb7qj95rv4wxaf8j")))

(define-public crate-wasmparser-0.205.0 (c (n "wasmparser") (v "0.205.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (d #t) (k 0)))) (h "1pygw0p43vd3xsjbcgfqkv9d2rdwvm9nwc2ssl4jw90452spni8x")))

(define-public crate-wasmparser-0.206.0 (c (n "wasmparser") (v "0.206.0") (d (list (d (n "ahash") (r "^0.8.11") (k 0)) (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("ahash"))) (k 0)) (d (n "indexmap") (r "^2.0.0") (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (k 0)))) (h "1g5xcy2x74aafh9bbda78kq5c8al1fdx83xl7nb42nymapdjw69r") (f (quote (("std" "indexmap/std") ("default" "std"))))))

(define-public crate-wasmparser-0.207.0 (c (n "wasmparser") (v "0.207.0") (d (list (d (n "ahash") (r "^0.8.11") (o #t) (k 0)) (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("ahash"))) (o #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (o #t) (k 0)))) (h "0b694q3frf4xvavj0rw7xk3j852gqljdp2pghajnsq87mgwbk6z1") (f (quote (("std" "indexmap/std") ("default" "std" "validate")))) (s 2) (e (quote (("validate" "dep:indexmap" "dep:semver" "dep:hashbrown" "dep:ahash"))))))

(define-public crate-wasmparser-0.208.0 (c (n "wasmparser") (v "0.208.0") (d (list (d (n "ahash") (r "^0.8.11") (o #t) (k 0)) (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("ahash"))) (o #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (o #t) (k 0)))) (h "10wwqnvfvgvrpi9k5mww64rv7rchlbiy8nm0nyx8dv075lmvy82x") (f (quote (("std" "indexmap/std") ("no-hash-maps") ("default" "std" "validate")))) (s 2) (e (quote (("validate" "dep:indexmap" "dep:semver" "dep:hashbrown" "dep:ahash")))) (r "1.76.0")))

(define-public crate-wasmparser-0.208.1 (c (n "wasmparser") (v "0.208.1") (d (list (d (n "ahash") (r "^0.8.11") (o #t) (k 0)) (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("ahash"))) (o #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0n4wbw54xqmm4crqbsxyljs6bvjx2l0d4dybi7srbi6wr64ig4nx") (f (quote (("std" "indexmap/std") ("no-hash-maps") ("default" "std" "validate" "serde")))) (s 2) (e (quote (("validate" "dep:indexmap" "dep:semver" "dep:hashbrown" "dep:ahash") ("serde" "dep:serde" "indexmap/serde" "hashbrown/serde")))) (r "1.76.0")))

(define-public crate-wasmparser-0.209.0 (c (n "wasmparser") (v "0.209.0") (d (list (d (n "ahash") (r "^0.8.11") (o #t) (k 0)) (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("ahash"))) (o #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("alloc"))) (o #t) (k 0)))) (h "1jf7jsx82gy92azzfas30b0gsim4hnlavlvzflmhj7f1k1z7ag13") (f (quote (("std" "indexmap/std") ("no-hash-maps") ("default" "std" "validate" "serde")))) (s 2) (e (quote (("validate" "dep:indexmap" "dep:semver" "dep:hashbrown" "dep:ahash") ("serde" "dep:serde" "indexmap/serde" "hashbrown/serde")))) (r "1.76.0")))

(define-public crate-wasmparser-0.209.1 (c (n "wasmparser") (v "0.209.1") (d (list (d (n "ahash") (r "^0.8.11") (o #t) (k 0)) (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (k 2)) (d (n "env_logger") (r "^0.11") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (f (quote ("ahash"))) (o #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (o #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "semver") (r "^1.0.0") (o #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("alloc"))) (o #t) (k 0)))) (h "1nrpv6cq98gjlfrgc77y0qy9jz5qdak1af1spg9n47mlm74mq0q7") (f (quote (("std" "indexmap/std") ("no-hash-maps") ("default" "std" "validate" "serde")))) (s 2) (e (quote (("validate" "dep:indexmap" "dep:semver" "dep:hashbrown" "dep:ahash") ("serde" "dep:serde" "indexmap/serde" "hashbrown/serde")))) (r "1.76.0")))

