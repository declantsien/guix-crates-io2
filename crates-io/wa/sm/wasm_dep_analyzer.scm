(define-module (crates-io wa sm wasm_dep_analyzer) #:use-module (crates-io))

(define-public crate-wasm_dep_analyzer-0.0.1 (c (n "wasm_dep_analyzer") (v "0.0.1") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1kr39jw13wb3b38cvxf69fcjsx0pkdy7k4zqpcgw2zh6vwpminkz")))

(define-public crate-wasm_dep_analyzer-0.1.0 (c (n "wasm_dep_analyzer") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1f04gjp02gvidxjrn85zs6qx0nflzgcb12swca8gv0qpm43049vz")))

