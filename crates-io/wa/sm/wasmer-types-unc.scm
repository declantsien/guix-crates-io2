(define-module (crates-io wa sm wasmer-types-unc) #:use-module (crates-io))

(define-public crate-wasmer-types-unc-2.3.0 (c (n "wasmer-types-unc") (v "2.3.0") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1hvkmprikmf57p9cqzwij5jjggj16fjl89plpkyakcnc0r4mxgzv") (f (quote (("std") ("experimental-reference-types-extern-ref") ("default" "std") ("core"))))))

(define-public crate-wasmer-types-unc-2.3.2 (c (n "wasmer-types-unc") (v "2.3.2") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05rqfim9mr6xiayn5r0dgidriadg4pj7hdr889yx37lnkbgwwaig") (f (quote (("std") ("experimental-reference-types-extern-ref") ("default" "std") ("core"))))))

(define-public crate-wasmer-types-unc-2.3.4 (c (n "wasmer-types-unc") (v "2.3.4") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "07h2d620iadipkn4prnjgx93v1yaq7l99rbzdwg4rb378can3fn5") (f (quote (("std") ("experimental-reference-types-extern-ref") ("default" "std") ("core"))))))

(define-public crate-wasmer-types-unc-2.4.1 (c (n "wasmer-types-unc") (v "2.4.1") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04m6hq7d2zbn0y55gzh4b377ypz8835dnkn2bxp3dvk38kkr9wp6") (f (quote (("std") ("experimental-reference-types-extern-ref") ("default" "std") ("core"))))))

