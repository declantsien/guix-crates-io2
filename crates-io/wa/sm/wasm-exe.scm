(define-module (crates-io wa sm wasm-exe) #:use-module (crates-io))

(define-public crate-wasm-exe-0.0.0 (c (n "wasm-exe") (v "0.0.0") (d (list (d (n "dynasm") (r "^2.0.0") (d #t) (k 0)) (d (n "dynasmrt") (r "^2.0.0") (d #t) (k 0)) (d (n "pelite") (r "^0.10.0") (d #t) (k 0)))) (h "1g0fx4f9l078ghjqiix4na8745i7r1wcqanc90c62xpy5qcqf2kp") (f (quote (("default"))))))

