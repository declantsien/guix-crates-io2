(define-module (crates-io wa sm wasmer-deploy-util) #:use-module (crates-io))

(define-public crate-wasmer-deploy-util-0.1.0 (c (n "wasmer-deploy-util") (v "0.1.0") (d (list (d (n "wasmer-deploy-schema") (r "^0.1.0-alpha.1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "17bk246a9xydgcmrilgbw2r3ag1lfkqdkh9qsy5rvrbia1gva4d3") (y #t)))

(define-public crate-wasmer-deploy-util-0.0.1 (c (n "wasmer-deploy-util") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "171dxlwvs6z7jq76bqda0jllqgch3vrdqv6wxrhxdarjikqs93az")))

(define-public crate-wasmer-deploy-util-0.0.2 (c (n "wasmer-deploy-util") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.2") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "11mvg8yidk0x143ffm043pxpnch4v3dalpn9wdmiwhby1rij3bbf")))

(define-public crate-wasmer-deploy-util-0.0.3 (c (n "wasmer-deploy-util") (v "0.0.3") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.3") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "0dfnrwhi98aj780zl77gvq4r6mwh3ljymr11pbfzqpjziw5fmmwy")))

(define-public crate-wasmer-deploy-util-0.0.4 (c (n "wasmer-deploy-util") (v "0.0.4") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.5") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "0vm4cxa42w49iwr4hfsvf3581cf45czfiij3n8f7px0yfmfp86j6")))

(define-public crate-wasmer-deploy-util-0.0.6 (c (n "wasmer-deploy-util") (v "0.0.6") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.6") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "11381l6mnlprzk3am59kchfz4sj0dpvmrypah6p02srm4807hh32")))

(define-public crate-wasmer-deploy-util-0.0.7 (c (n "wasmer-deploy-util") (v "0.0.7") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.7") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "0si5rmi4d12rd9w628qylxcy41dnr6x95prxvnzpyka5wpd3lakc")))

(define-public crate-wasmer-deploy-util-0.0.8 (c (n "wasmer-deploy-util") (v "0.0.8") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.8") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "1538gln1h7ifbcpyj6267xrvp3xnfhbwvmzxla0nw1jvq8f6rjwl")))

(define-public crate-wasmer-deploy-util-0.0.9 (c (n "wasmer-deploy-util") (v "0.0.9") (d (list (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.9") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "0wbd9wj2az6dd7b2h3izzpm639iwyc7hlahcs8xl8s1kl9llq1w1")))

(define-public crate-wasmer-deploy-util-0.0.10 (c (n "wasmer-deploy-util") (v "0.0.10") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.10") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "13slpv6mdzvznxw8438gm0py2m2z4c2184vg6cz2pxj645yad897")))

(define-public crate-wasmer-deploy-util-0.0.11 (c (n "wasmer-deploy-util") (v "0.0.11") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.11") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "1i07k11zxlgrqhha1ilq9bkrpmy5684qgvpd9n5gqppdjz6bsddk") (r "1.70")))

(define-public crate-wasmer-deploy-util-0.0.12 (c (n "wasmer-deploy-util") (v "0.0.12") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.12") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "1hkb8qlpvvsaayi66xniamkmim38lmk91369hhb76pckvddha91m")))

(define-public crate-wasmer-deploy-util-0.0.13 (c (n "wasmer-deploy-util") (v "0.0.13") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.13") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "0dfghgdcbm905ihl1bjs6lpz873bsv3qakjbssmsxjhykk98k0ba")))

(define-public crate-wasmer-deploy-util-0.0.14 (c (n "wasmer-deploy-util") (v "0.0.14") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.14") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "121yvilp3vga5g0kx7prd8bpazzyb46hfkbdmgvf2g8nn5gm35bm")))

(define-public crate-wasmer-deploy-util-0.0.15 (c (n "wasmer-deploy-util") (v "0.0.15") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.14") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "1awx4pxhym0vi7mhl9va30j19kkqy6zki06gv19kqls21v6ks4yz")))

(define-public crate-wasmer-deploy-util-0.0.20 (c (n "wasmer-deploy-util") (v "0.0.20") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.20") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "12xxs1a9h8p3hqzl143s0rb9fd2cdmfyrp2f3grg1qk9ccpib6jl")))

(define-public crate-wasmer-deploy-util-0.0.21 (c (n "wasmer-deploy-util") (v "0.0.21") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmer-deploy-schema") (r "^0.0.21") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "167c7hprkpvjw015sx2scvyy4sm33hk828dcrvla5gg1wwwi7slg")))

