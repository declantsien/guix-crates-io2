(define-module (crates-io wa sm wasmer-as) #:use-module (crates-io))

(define-public crate-wasmer-as-0.1.0 (c (n "wasmer-as") (v "0.1.0") (d (list (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.13.1") (d #t) (k 0)))) (h "1bkvydni0kmpkmsgshv1q32cfwld9za4d26bmgjf0zcq76hssz01") (y #t)))

(define-public crate-wasmer-as-0.1.1 (c (n "wasmer-as") (v "0.1.1") (d (list (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.13.1") (d #t) (k 0)))) (h "04392h4mycd6yjalvh9lgx6xmamz5pdb32k28ma2c8q7gnvjlch5")))

(define-public crate-wasmer-as-0.1.2 (c (n "wasmer-as") (v "0.1.2") (d (list (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.13.1") (d #t) (k 0)))) (h "070d9dspv39ma16i84fwndx091g4c304b88q3db7xfs3b62ii99w")))

(define-public crate-wasmer-as-0.2.0 (c (n "wasmer-as") (v "0.2.0") (d (list (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.13.1") (d #t) (k 0)))) (h "1mcm6h0ixk769q1s4ncx33w2l5vd6d2i96hd8428587vwaa69qr5")))

(define-public crate-wasmer-as-0.2.1 (c (n "wasmer-as") (v "0.2.1") (d (list (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.13.1") (d #t) (k 0)))) (h "1qm7jl9v5rfsyzwqs0idk565db4hwgqf3qpz68pnqsh1n2pfs4cw")))

(define-public crate-wasmer-as-0.2.2 (c (n "wasmer-as") (v "0.2.2") (d (list (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.14.1") (d #t) (k 0)))) (h "06saabyzs5qwfbwsx8yzpmyr7j9qhq9a4kf90hgs5pjbsbr58cba")))

(define-public crate-wasmer-as-0.3.0-alpha.0 (c (n "wasmer-as") (v "0.3.0-alpha.0") (d (list (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.15.0") (d #t) (k 0)))) (h "15hp9ma1xxm5yifxxq192qxq2rj99igzlj8b1ij77kl229qvhzsx")))

(define-public crate-wasmer-as-0.3.0-alpha.2 (c (n "wasmer-as") (v "0.3.0-alpha.2") (d (list (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.16") (d #t) (k 0)))) (h "15f9q8x1p7j2f8hg9f9sicpnllxiilyg2rynla9dcqfyx7c2rkfp")))

(define-public crate-wasmer-as-0.3.0-alpha.3 (c (n "wasmer-as") (v "0.3.0-alpha.3") (d (list (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.17") (d #t) (k 0)))) (h "19cwqnaa4dgc4fhh80by0cxsv1f77m24cgxxyqlkj62p48kgaswd")))

(define-public crate-wasmer-as-0.3.0-alpha.4 (c (n "wasmer-as") (v "0.3.0-alpha.4") (d (list (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "ucs2") (r "^0.3.1") (d #t) (k 0)) (d (n "wasmer-runtime") (r "^0.17") (d #t) (k 2)) (d (n "wasmer-runtime-core") (r "^0.17") (d #t) (k 0)))) (h "19wnsqvm3a15f2r9kv056y5mdpcmhaxz4hbns6zn15vlg0qgydfs")))

(define-public crate-wasmer-as-0.4.0 (c (n "wasmer-as") (v "0.4.0") (d (list (d (n "skeptic") (r "^0.13") (d #t) (k 1)) (d (n "skeptic") (r "^0.13") (d #t) (k 2)) (d (n "wasmer") (r "^2.0") (d #t) (k 0)) (d (n "wasmer") (r "^2.0") (d #t) (k 2)))) (h "0srysii3zk6s8ah0gv9ipn8hxfvm2ggsi3qf5rwq80xkqgv85v04")))

