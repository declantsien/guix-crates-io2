(define-module (crates-io wa sm wasm-bindgen-downcast) #:use-module (crates-io))

(define-public crate-wasm-bindgen-downcast-0.1.0 (c (n "wasm-bindgen-downcast") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm-bindgen-downcast-macros") (r "^0.1.0") (d #t) (k 0)))) (h "196v842xw9iz9ywwv2hm5245w734whsf53r957pmj9cxs2a82p66") (r "1.64")))

(define-public crate-wasm-bindgen-downcast-0.1.1 (c (n "wasm-bindgen-downcast") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "once_cell") (r "^1.16.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm-bindgen-downcast-macros") (r "^0.1.1") (d #t) (k 0)))) (h "0h23lhqjrrqvi5fhm4wf7r0gdvariyk6p5f0w5y6xjmw8dnh5b2x") (r "1.63")))

