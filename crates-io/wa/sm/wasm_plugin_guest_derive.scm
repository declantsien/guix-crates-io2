(define-module (crates-io wa sm wasm_plugin_guest_derive) #:use-module (crates-io))

(define-public crate-wasm_plugin_guest_derive-0.1.1 (c (n "wasm_plugin_guest_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zih3xlwi27h7f51cf0zhwnvx6vsqi1ygxkjayxh3p5cgrqm3hj3")))

(define-public crate-wasm_plugin_guest_derive-0.1.2 (c (n "wasm_plugin_guest_derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1d2033sj39aswrk3zvcfpa2g6hnkw28vxlvqhx1irxjiq1rahicg")))

(define-public crate-wasm_plugin_guest_derive-0.1.3 (c (n "wasm_plugin_guest_derive") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02dwbrb6y95zhxv6y6idhjv9hbsa8zkwbhswnjag25wqv0jdnccp")))

(define-public crate-wasm_plugin_guest_derive-0.1.4 (c (n "wasm_plugin_guest_derive") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1l7z8ld69d1ssh53ggbbfxfli3617i8cmm345mngcc83wb0pg03r")))

(define-public crate-wasm_plugin_guest_derive-0.1.5 (c (n "wasm_plugin_guest_derive") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "16l7mmv780xylnnws2cb1l1hbqlagq4ij219zgy94svj8szanac0")))

