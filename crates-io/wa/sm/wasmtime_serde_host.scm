(define-module (crates-io wa sm wasmtime_serde_host) #:use-module (crates-io))

(define-public crate-wasmtime_serde_host-0.1.0 (c (n "wasmtime_serde_host") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "wasmtime") (r "^4.0.0") (d #t) (k 0)) (d (n "wasmtime_serde_host_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1jzy5p5b3dh2biqja5rwxrr56apxaa9mlsn11dga6mx7ha4viqxp")))

(define-public crate-wasmtime_serde_host-0.1.1 (c (n "wasmtime_serde_host") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "wasmtime") (r "^4.0.0") (d #t) (k 0)) (d (n "wasmtime_serde_host_macro") (r "^0.1.0") (d #t) (k 0)))) (h "0rjh7y3wj0a0m66kq8vz8gflplnsn7xdad0v9iza4qf0rrp3bgza")))

(define-public crate-wasmtime_serde_host-0.1.2 (c (n "wasmtime_serde_host") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "wasmtime") (r "^4.0.0") (d #t) (k 0)) (d (n "wasmtime_serde_host_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1j9xncp3w5182ih8msh6w40cn120j7gy6839xjkqg4fbbbaa75mw")))

(define-public crate-wasmtime_serde_host-0.1.3 (c (n "wasmtime_serde_host") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "wasmtime") (r "^7.0.0") (d #t) (k 0)) (d (n "wasmtime_serde_host_macro") (r "^0.1.0") (d #t) (k 0)))) (h "1nspfxxk4sqhmnbqykl0a36qz1npzmgby16db1vy9ka9xgw4ad6l")))

