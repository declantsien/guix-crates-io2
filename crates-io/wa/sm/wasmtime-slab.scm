(define-module (crates-io wa sm wasmtime-slab) #:use-module (crates-io))

(define-public crate-wasmtime-slab-19.0.0 (c (n "wasmtime-slab") (v "19.0.0") (h "09xmjgwm34q93rmy5a6hc6avrz8nam5aibpb35q9g5dd03v8wi43")))

(define-public crate-wasmtime-slab-19.0.1 (c (n "wasmtime-slab") (v "19.0.1") (h "1qzhk79gpc838xnmwlw7a3nwb3l22li3v9clx7dny4nzj9xvkmsj")))

(define-public crate-wasmtime-slab-19.0.2 (c (n "wasmtime-slab") (v "19.0.2") (h "1h44wpcbm7wrbr2mn8jirh2wn5xg0g80i3xmmh3gsxz8kkpqpi90")))

(define-public crate-wasmtime-slab-20.0.0 (c (n "wasmtime-slab") (v "20.0.0") (h "1vw1mmqqbi32g1c5i2ymyg6myxmvhf9crcm26v2p98r4b03madlb")))

(define-public crate-wasmtime-slab-20.0.1 (c (n "wasmtime-slab") (v "20.0.1") (h "0jwv601pb8635gvi1sjz8pmck2wqxl18q7xwhv0nmaxvivxxvj05")))

(define-public crate-wasmtime-slab-20.0.2 (c (n "wasmtime-slab") (v "20.0.2") (h "1d6mypmiw32sn8x745qdi98x9mz6ahrhr31aklz7qhjwiy38arfa")))

(define-public crate-wasmtime-slab-21.0.0 (c (n "wasmtime-slab") (v "21.0.0") (h "0dm8549x49s8scl9gkmjy5skmkhnhvwaihr990r05yjaqrblz0ch")))

(define-public crate-wasmtime-slab-21.0.1 (c (n "wasmtime-slab") (v "21.0.1") (h "0q348hns4i6gn1brvl8fbd9574901xqy6p1q6sq08yz4zypmrxsg")))

