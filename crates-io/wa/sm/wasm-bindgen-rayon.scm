(define-module (crates-io wa sm wasm-bindgen-rayon) #:use-module (crates-io))

(define-public crate-wasm-bindgen-rayon-1.0.0 (c (n "wasm-bindgen-rayon") (v "1.0.0") (d (list (d (n "js-sys") (r "^0.3.48") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "spmc") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "06fmr3l2gjnqiimr6fd0qxf399ia4vc40rg4793x3jwkzp2ga3i7") (f (quote (("no-bundler") ("nightly"))))))

(define-public crate-wasm-bindgen-rayon-1.0.1 (c (n "wasm-bindgen-rayon") (v "1.0.1") (d (list (d (n "js-sys") (r "^0.3.48") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "spmc") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1541wzjfvmfbcjh2g6xkl8f70ddwbzdlmy38wvyknzks5sjd4s9h") (f (quote (("no-bundler") ("nightly"))))))

(define-public crate-wasm-bindgen-rayon-1.0.2 (c (n "wasm-bindgen-rayon") (v "1.0.2") (d (list (d (n "js-sys") (r "^0.3.48") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "spmc") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1qi8lqsa5r1wihqpvvnbbczbdwk5fl1bcqhdsnfvxgwgi6kmp9nl") (f (quote (("no-bundler") ("nightly"))))))

(define-public crate-wasm-bindgen-rayon-1.0.3 (c (n "wasm-bindgen-rayon") (v "1.0.3") (d (list (d (n "js-sys") (r "^0.3.48") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "spmc") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1cdgrhvdqbnd7vy5mhjkj413s9dh6z2kljp479fk0p40a1scd1yz") (f (quote (("no-bundler") ("nightly"))))))

(define-public crate-wasm-bindgen-rayon-1.1.0 (c (n "wasm-bindgen-rayon") (v "1.1.0") (d (list (d (n "js-sys") (r "^0.3.48") (d #t) (k 0)) (d (n "rayon-core") (r "^1.12") (d #t) (k 0)) (d (n "spmc") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "11va7rrn222n9k8qrw90cdwzb3jslx9fikhy5apwcjgm2rih12gy") (f (quote (("no-bundler") ("nightly"))))))

(define-public crate-wasm-bindgen-rayon-1.1.1 (c (n "wasm-bindgen-rayon") (v "1.1.1") (d (list (d (n "js-sys") (r "^0.3.48") (d #t) (k 0)) (d (n "rayon-core") (r "^1.12") (d #t) (k 0)) (d (n "spmc") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "1w809r774zqcyhqs8pvbf9ylh96jb9fk4bvsbqm7mrlzigllwlq0") (f (quote (("no-bundler") ("nightly"))))))

(define-public crate-wasm-bindgen-rayon-1.1.2 (c (n "wasm-bindgen-rayon") (v "1.1.2") (d (list (d (n "js-sys") (r "^0.3.48") (d #t) (k 0)) (d (n "rayon-core") (r "^1.12") (d #t) (k 0)) (d (n "spmc") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "0k9gakfsja9mzaw3w5gzk3lzmzffwi9izxlkhxpf85ms5f1hqx34") (f (quote (("no-bundler") ("nightly"))))))

(define-public crate-wasm-bindgen-rayon-1.1.3 (c (n "wasm-bindgen-rayon") (v "1.1.3") (d (list (d (n "js-sys") (r "^0.3.48") (d #t) (k 0)) (d (n "rayon-core") (r "^1.12") (d #t) (k 0)) (d (n "spmc") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "0wn9bi1j6a8njxy4srk467frwvlpqcrv13szg8j0dmw2cw99k8zh") (f (quote (("no-bundler") ("nightly"))))))

(define-public crate-wasm-bindgen-rayon-1.2.0 (c (n "wasm-bindgen-rayon") (v "1.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.9") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.48") (d #t) (k 0)) (d (n "rayon-core") (r "^1.12.1") (f (quote ("web_spin_lock"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "0m6lw2vvwfycblas5a2dpa7ih62dg6h31qa71gnf1wpszn6xmgm9") (f (quote (("no-bundler") ("nightly"))))))

(define-public crate-wasm-bindgen-rayon-1.2.1 (c (n "wasm-bindgen-rayon") (v "1.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5.9") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.48") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (f (quote ("web_spin_lock"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)))) (h "19yb5576vq0md40j4251ipyjn17g6jid7dsphqcdw5gyhmvjpq69") (f (quote (("no-bundler") ("nightly"))))))

