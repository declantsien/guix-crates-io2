(define-module (crates-io wa sm wasm-bus-tok) #:use-module (crates-io))

(define-public crate-wasm-bus-tok-1.0.0 (c (n "wasm-bus-tok") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 2)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "wasm-bus") (r "^1") (f (quote ("rt" "macros"))) (k 0)))) (h "1yggi93j91l7y53kwhf97264p150nc3xl7zhkl35vykbypfdsrwm") (f (quote (("frontend" "wasm-bus/syscalls") ("default" "frontend") ("backend"))))))

