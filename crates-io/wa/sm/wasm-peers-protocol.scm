(define-module (crates-io wa sm wasm-peers-protocol) #:use-module (crates-io))

(define-public crate-wasm-peers-protocol-0.3.0 (c (n "wasm-peers-protocol") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cgdnaxnkb39dhfnncfb84cyvzl3fwf26nvqm2fsv8qg73r5akh9")))

(define-public crate-wasm-peers-protocol-0.3.1 (c (n "wasm-peers-protocol") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fxn30kv4d94n471np3sviccq7wich98mygnxcwxscg19lnrxgqr")))

(define-public crate-wasm-peers-protocol-0.3.2 (c (n "wasm-peers-protocol") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1s0q4h7my7zsdy269l54h87dwd3lh3rjip5vkfcygf0sb1h4jyjr")))

