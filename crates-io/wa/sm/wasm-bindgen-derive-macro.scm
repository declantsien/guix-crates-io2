(define-module (crates-io wa sm wasm-bindgen-derive-macro) #:use-module (crates-io))

(define-public crate-wasm-bindgen-derive-macro-0.2.0 (c (n "wasm-bindgen-derive-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jkms3hw1qs79226pnl5p8ccnz1qi9bq6sszaazyzb31zgd83f1n") (r "1.65.0")))

(define-public crate-wasm-bindgen-derive-macro-0.2.1 (c (n "wasm-bindgen-derive-macro") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.85") (d #t) (k 2)))) (h "1fzwypamclhl8nqsrza00fmcplj50kph05w9kb36nq973nrjhz5q") (r "1.65.0")))

(define-public crate-wasm-bindgen-derive-macro-0.3.0 (c (n "wasm-bindgen-derive-macro") (v "0.3.0") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.91") (d #t) (k 2)))) (h "1isckfv102bcsm0d3dgcm4jljb8ccdjaxfsjgm28zcswy60c1yzj") (r "1.74.0")))

