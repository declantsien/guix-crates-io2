(define-module (crates-io wa sm wasmer-object-near) #:use-module (crates-io))

(define-public crate-wasmer-object-near-1.0.1 (c (n "wasmer-object-near") (v "1.0.1") (d (list (d (n "object") (r "^0.22") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.1") (f (quote ("std" "translator"))) (k 0) (p "wasmer-compiler-near")) (d (n "wasmer-types") (r "^1.0.1") (d #t) (k 0) (p "wasmer-types-near")))) (h "0z0nh1v8a4vly6h7x18irl9rgm3fzm6d3gj81b9hp2cdjfkb6k38")))

