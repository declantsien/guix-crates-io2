(define-module (crates-io wa sm wasm-nopackage) #:use-module (crates-io))

(define-public crate-wasm-nopackage-0.1.0 (c (n "wasm-nopackage") (v "0.1.0") (h "12mqnvj098mmwws3r8q65d4xyjklka3p33p0fdx0v3s5zmf1l2kd") (y #t)))

(define-public crate-wasm-nopackage-0.1.1 (c (n "wasm-nopackage") (v "0.1.1") (h "1chshaj38dasq5jpfa5bwgsl0v3rbxs7rcc6rfn88r03xc6yasa3") (y #t)))

(define-public crate-wasm-nopackage-0.1.2 (c (n "wasm-nopackage") (v "0.1.2") (h "0zf1vdw65gdvvvy793lwcfkdfrz3pjygbvncb0l6ba79nd9r748f") (y #t)))

(define-public crate-wasm-nopackage-0.1.3 (c (n "wasm-nopackage") (v "0.1.3") (h "07g53p9d29iaqw6a9dspxjimg6ib3kvd9d749mvprg83lz8lx9s6")))

(define-public crate-wasm-nopackage-0.1.4 (c (n "wasm-nopackage") (v "0.1.4") (h "1sc60ma6p0fl13v9m4c0yra0jn5f7zyzkwcqgfiwl3ysvf5si0jg")))

(define-public crate-wasm-nopackage-0.1.5 (c (n "wasm-nopackage") (v "0.1.5") (h "0pdbm4sp3122wyhy7wjxg70b3dhhfpzi2mi14hfrpqig9cdpmyj0")))

