(define-module (crates-io wa sm wasmflow-macros) #:use-module (crates-io))

(define-public crate-wasmflow-macros-0.10.0-beta.0 (c (n "wasmflow-macros") (v "0.10.0-beta.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)))) (h "1h7zpqlnq5mk38g2zz08qd3aggk2khrdkk0wfrbxq2v4j9r2x3gy")))

(define-public crate-wasmflow-macros-0.10.0-beta.1 (c (n "wasmflow-macros") (v "0.10.0-beta.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)))) (h "0w2kns4yppskg9bsg77bjbfbj2fcq7izzyb2w0q4pcvlw3fj7i27")))

(define-public crate-wasmflow-macros-0.10.0-beta.4 (c (n "wasmflow-macros") (v "0.10.0-beta.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)))) (h "0sv9qz366qs52kddyczj2iign70056qnkjchyvs1p6207wz260vz")))

(define-public crate-wasmflow-macros-0.10.0 (c (n "wasmflow-macros") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)))) (h "1j3vhprcpv30f7qwnwyfh3dxycjxxdl6g460d3cnlbjb4rbya74j")))

