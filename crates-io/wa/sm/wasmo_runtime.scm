(define-module (crates-io wa sm wasmo_runtime) #:use-module (crates-io))

(define-public crate-wasmo_runtime-0.1.0 (c (n "wasmo_runtime") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bytecheck") (r "^0.6.7") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "llvm-sys") (r "^130") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasmparser") (r "^0.82.0") (d #t) (k 0)) (d (n "wat") (r "^1.0.41") (d #t) (k 0)))) (h "00nrgw1xwaqihyfqaq8zj69nz7p745ps0kqbqamih6iw1vpp95hx")))

