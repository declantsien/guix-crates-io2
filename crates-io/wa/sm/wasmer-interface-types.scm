(define-module (crates-io wa sm wasmer-interface-types) #:use-module (crates-io))

(define-public crate-wasmer-interface-types-0.15.0 (c (n "wasmer-interface-types") (v "0.15.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "wast") (r "^8.0") (d #t) (k 0)))) (h "09279wvnkcfax6ask79h275mqr43y5lczwjzx6blf6lmqrjsvpxm")))

(define-public crate-wasmer-interface-types-0.16.0 (c (n "wasmer-interface-types") (v "0.16.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "wast") (r "^8.0") (d #t) (k 0)))) (h "0grdrbmzzl7bji4fq9kp7vp8fhfiicm3z9bpgi197qp7iywx5l89")))

(define-public crate-wasmer-interface-types-0.16.1 (c (n "wasmer-interface-types") (v "0.16.1") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "wast") (r "^8.0") (d #t) (k 0)))) (h "1hxrxkvpv03ffsyd1linnzmmdx85pkib8a02ffzf86jvi7zjrmc3")))

(define-public crate-wasmer-interface-types-0.16.2 (c (n "wasmer-interface-types") (v "0.16.2") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "wast") (r "^8.0") (d #t) (k 0)))) (h "18bymy0snw91d0cw11npkqlhbv0lnr4mfcgvsmx0sk4nci24pli6")))

(define-public crate-wasmer-interface-types-0.17.0 (c (n "wasmer-interface-types") (v "0.17.0") (d (list (d (n "nom") (r "^5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "wast") (r "^8.0") (d #t) (k 0)))) (h "1ilm0sk5hgz30k9vq768d42b78xhyzj681xv2avj1vjzzfdp1mdd") (f (quote (("default" "serde"))))))

