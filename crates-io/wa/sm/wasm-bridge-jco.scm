(define-module (crates-io wa sm wasm-bridge-jco) #:use-module (crates-io))

(define-public crate-wasm-bridge-jco-0.2.0 (c (n "wasm-bridge-jco") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "heck") (r "^0.4") (f (quote ("unicode"))) (d #t) (k 0)) (d (n "indexmap") (r "^1.9") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^11.0.0") (f (quote ("component-model"))) (d #t) (k 0)) (d (n "wit-component") (r "^0.11.0") (f (quote ("dummy-module"))) (d #t) (k 0)) (d (n "wit-parser") (r "^0.8.0") (d #t) (k 0)))) (h "1wyiwx7slk6bspsnlnshkqgnkph6xn2sgpkhr2njj90xkv1hvmpp") (f (quote (("transpile-bindgen") ("default" "transpile-bindgen"))))))

