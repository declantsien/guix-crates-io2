(define-module (crates-io wa sm wasmc) #:use-module (crates-io))

(define-public crate-wasmc-0.1.0 (c (n "wasmc") (v "0.1.0") (d (list (d (n "wat") (r "^1.0.47") (d #t) (k 0)))) (h "1760aizp51kgapsjdl07vj47b6vyiziggl4mxy1nglg1v7f66w0m")))

(define-public crate-wasmc-1.0.0 (c (n "wasmc") (v "1.0.0") (d (list (d (n "wat") (r "^1.0.47") (d #t) (k 0)))) (h "05g059an824pkvn2wi219h9bs6rawcy5pl9lqfysla4p0dnllsc6")))

