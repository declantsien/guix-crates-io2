(define-module (crates-io wa sm wasm_mutex) #:use-module (crates-io))

(define-public crate-wasm_mutex-0.1.0 (c (n "wasm_mutex") (v "0.1.0") (h "1rcaqc88cz8fmvrk0av8a6ccdidk420cpgfc44j8pmv0m9l27pbw")))

(define-public crate-wasm_mutex-0.1.1 (c (n "wasm_mutex") (v "0.1.1") (h "0h4hq0hgid7qgkzwky8b8rmxyclq6h67lzxj5hw04md0nx7qpvb3")))

(define-public crate-wasm_mutex-0.1.2 (c (n "wasm_mutex") (v "0.1.2") (h "1dmk24ws9gd02813h08jas3dvq7b86vr5fgdma7h6c7msjgx86qy")))

(define-public crate-wasm_mutex-0.1.3 (c (n "wasm_mutex") (v "0.1.3") (h "1pyl7qmf95xw7xa0dmrh0hg4a5jq34s7fmks19g7zycidi7sx9kw")))

(define-public crate-wasm_mutex-0.1.4 (c (n "wasm_mutex") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1kb83sgy6i8m1db0lh1ap98y3l98ahd43cq0k1ijvz33n71xvggg")))

(define-public crate-wasm_mutex-0.1.5 (c (n "wasm_mutex") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06qa4jcvcwrqmy424bvrwpwvvwjijwidqlj22czawcwv5n52fn6d")))

