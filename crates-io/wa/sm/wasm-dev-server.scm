(define-module (crates-io wa sm wasm-dev-server) #:use-module (crates-io))

(define-public crate-wasm-dev-server-0.1.0 (c (n "wasm-dev-server") (v "0.1.0") (d (list (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.2") (d #t) (k 0)))) (h "0rp22mz6zjxl5rdc7wnlspgk9p2lzab78czsygr9314ihdy2c7a7")))

(define-public crate-wasm-dev-server-0.1.1 (c (n "wasm-dev-server") (v "0.1.1") (d (list (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.2") (d #t) (k 0)))) (h "0705018p6dj4znglf88cq8nlj9yawbkkb2n787ii0dfif0h7nljj")))

