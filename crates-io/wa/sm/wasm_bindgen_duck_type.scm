(define-module (crates-io wa sm wasm_bindgen_duck_type) #:use-module (crates-io))

(define-public crate-wasm_bindgen_duck_type-0.1.0 (c (n "wasm_bindgen_duck_type") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.60") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.83") (d #t) (k 0)) (d (n "wasm_bindgen_duck_type_macro_impl") (r "^0.1.0") (d #t) (k 0)))) (h "12ra4jzziif4q81305npzixdq0wgs9pgr814yaljyy0dvm951aqv")))

