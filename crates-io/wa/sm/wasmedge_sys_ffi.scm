(define-module (crates-io wa sm wasmedge_sys_ffi) #:use-module (crates-io))

(define-public crate-wasmedge_sys_ffi-0.11.2 (c (n "wasmedge_sys_ffi") (v "0.11.2") (h "0fcbcnqc393r9x862fn3vfazvpq62ldb90skrvphdx597x3v904k")))

(define-public crate-wasmedge_sys_ffi-0.13.0 (c (n "wasmedge_sys_ffi") (v "0.13.0") (h "0d96yhagcxai0rljv2nfsi8pylqygs9920wjk8ljilrd8r8zm3r6")))

