(define-module (crates-io wa sm wasmcloud-fs) #:use-module (crates-io))

(define-public crate-wasmcloud-fs-0.3.0 (c (n "wasmcloud-fs") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.9.0") (d #t) (k 0)) (d (n "wasmcloud-actor-blobstore") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.0") (d #t) (k 0)))) (h "0s5c1par26dmd8rm300gqk7fngq1z2ckyx1x7k7gwx4k6sshz3my") (f (quote (("static_plugin"))))))

(define-public crate-wasmcloud-fs-0.3.1 (c (n "wasmcloud-fs") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.9.0") (d #t) (k 0)) (d (n "wasmcloud-actor-blobstore") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.0") (d #t) (k 0)))) (h "1vfqkimbg53d28b64q3z35v6mdidmlwmawn3sz04dcn9wxb7xspy") (f (quote (("static_plugin"))))))

(define-public crate-wasmcloud-fs-0.3.2 (c (n "wasmcloud-fs") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasmcloud-actor-blobstore") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmcloud-provider-core") (r "^0.1.0") (d #t) (k 0)))) (h "06gzlvbvh3i9nii2ipbws3pg3r90z76vcsp6jgx7pp256n6amyzw") (f (quote (("static_plugin"))))))

(define-public crate-wasmcloud-fs-0.3.3 (c (n "wasmcloud-fs") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasmcloud-actor-blobstore") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmcloud-provider-core") (r "^0.1.0") (d #t) (k 0)))) (h "1dn0pivpfxyw2rbrykb37piv5lww4asgpa0xdhinvr3krs791vl2") (f (quote (("static_plugin"))))))

(define-public crate-wasmcloud-fs-0.4.0 (c (n "wasmcloud-fs") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasmcloud-actor-blobstore") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmcloud-provider-core") (r "^0.1.0") (d #t) (k 0)))) (h "1c609irq8rscc29n0v25vqiqdxgg5s09gwaghaz4ialxgwihnx2v") (f (quote (("static_plugin"))))))

(define-public crate-wasmcloud-fs-0.4.1 (c (n "wasmcloud-fs") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasmcloud-actor-blobstore") (r "^0.2.2") (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmcloud-provider-core") (r "^0.1.0") (d #t) (k 0)))) (h "04rwvyyqy5b8mzvr67al0x61a0hbygpzgkbf63sly2hr75xasw5s") (f (quote (("static_plugin"))))))

