(define-module (crates-io wa sm wasmrepl) #:use-module (crates-io))

(define-public crate-wasmrepl-0.1.0 (c (n "wasmrepl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "wast") (r "^65.0.0") (d #t) (k 0)))) (h "1i4n6c7r84dh6bmpfhz04jvjiyxdrrfqpi62ab38z8zq81ibkm7d") (y #t)))

(define-public crate-wasmrepl-0.2.0 (c (n "wasmrepl") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "wast") (r "^65.0.0") (d #t) (k 0)))) (h "0j7959yg11lxyyg4izzalpcs86xldd148mvfczd0jw3qk84i69q8") (y #t)))

(define-public crate-wasmrepl-0.3.0 (c (n "wasmrepl") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "wast") (r "^66.0.2") (d #t) (k 0)))) (h "1hqg0c2nj0h9dhhzh2kl2s59vmgsi69h3f0pg9yr5fzjf3ql6if6") (y #t)))

(define-public crate-wasmrepl-0.4.0 (c (n "wasmrepl") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)) (d (n "rustyline-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "wast") (r "^66.0.2") (d #t) (k 0)))) (h "0ilk8fn4y1g7d0sq8zwgc2ba24v95hmc5pd1yg35qnczgkn2n1wd")))

