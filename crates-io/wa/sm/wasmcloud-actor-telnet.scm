(define-module (crates-io wa sm wasmcloud-actor-telnet) #:use-module (crates-io))

(define-public crate-wasmcloud-actor-telnet-0.1.0 (c (n "wasmcloud-actor-telnet") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wapc-guest") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "1dnaz373f9262x8gvhzc6i8pf4fbx2cpzmls555mh05wsfrajpry") (f (quote (("guest" "wapc-guest"))))))

(define-public crate-wasmcloud-actor-telnet-0.1.1 (c (n "wasmcloud-actor-telnet") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (f (quote ("std" "serde"))) (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wapc-guest") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "12078n9y5nppihny8lh7l27q2y28vxx4agmc1pjwha2ym1qc9ibh") (f (quote (("guest" "wapc-guest"))))))

(define-public crate-wasmcloud-actor-telnet-0.1.2 (c (n "wasmcloud-actor-telnet") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wapc-guest") (r "^0.4.0") (o #t) (d #t) (k 0)))) (h "0sq5n7gcslhp9bybmzxr1pl6kbwf6p22s6s3zry6y0d6wnlf4y48") (f (quote (("guest" "wapc-guest"))))))

(define-public crate-wasmcloud-actor-telnet-0.2.0 (c (n "wasmcloud-actor-telnet") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wapc-guest") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "wasmcloud-actor-core") (r "^0.2.2") (f (quote ("guest"))) (d #t) (k 2)))) (h "0nqwr6ryj3d0v6fyzssb1cl11q6dx3r2ky8fxffdrc4qmpmhpzrr") (f (quote (("guest" "wapc-guest"))))))

