(define-module (crates-io wa sm wasmedge-types) #:use-module (crates-io))

(define-public crate-wasmedge-types-0.1.0 (c (n "wasmedge-types") (v "0.1.0") (h "1hms3gbfcl05r51jwd5bbwj8whbpyi4yysa8q51hz15196k80s1s")))

(define-public crate-wasmedge-types-0.1.1 (c (n "wasmedge-types") (v "0.1.1") (d (list (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "1sr3pljgkcl2fv471vg7rsc2d5rc33bia90034558g7r03s1is17")))

(define-public crate-wasmedge-types-0.1.2 (c (n "wasmedge-types") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "1bh1cca9n6szwa4hmjzfp1p2pkqgdwp9gwgfpy3fhvns1p5dkxym")))

(define-public crate-wasmedge-types-0.1.3 (c (n "wasmedge-types") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "1rbkaq8d1z1062iqjf1n5m9ild11wynzrhlzb4s2wy6vcizr2kbz")))

(define-public crate-wasmedge-types-0.2.0 (c (n "wasmedge-types") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "06is31c4qflv2kysmn9f15bm2d5lc4b8ls82lfpzarkpxi3r1asz")))

(define-public crate-wasmedge-types-0.2.1 (c (n "wasmedge-types") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "1a19krgsshfzl134m7aff2y762kbn6xr4kcv8g64whcjq8nzx2zj")))

(define-public crate-wasmedge-types-0.3.0 (c (n "wasmedge-types") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "1nw25v14nz4j7iv7cf4pq8mpdmbkrdffpj2r3lkwvvssvs70rfj4")))

(define-public crate-wasmedge-types-0.3.1 (c (n "wasmedge-types") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "0bajdkxqwphkr3nwpzhbpakna41aqam944047arpcm76liszw8rk")))

(define-public crate-wasmedge-types-0.4.0 (c (n "wasmedge-types") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "12g4frg66l9g1wxa7ig0dwqgh0ld1wasl4f5rm72i2px5xq66rxw") (y #t)))

(define-public crate-wasmedge-types-0.4.1 (c (n "wasmedge-types") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "0f83zg2jh5lvn2kizv1vrj8ryrvpz9sj80dbg5rjfikps9zzbcda")))

(define-public crate-wasmedge-types-0.4.2 (c (n "wasmedge-types") (v "0.4.2") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "17mxrfh99abqsqkvf9rnwdp8jq9b70lpz39qdy36vj3z30d7yi1v")))

(define-public crate-wasmedge-types-0.4.3 (c (n "wasmedge-types") (v "0.4.3") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "1977iswir0gsp97081fy3jawipj2jii5n29ls2h81wy09ylijpc0")))

(define-public crate-wasmedge-types-0.4.4 (c (n "wasmedge-types") (v "0.4.4") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "0lpx1r603wmvg5qk5yd60icnzsskh8abrvlv72xi7ig2xf57qkid")))

(define-public crate-wasmedge-types-0.5.0 (c (n "wasmedge-types") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 0)))) (h "0pvcnkifzmx83w3ywdkg47k420n0rk4rrym82ch8swakh9agr2vw")))

