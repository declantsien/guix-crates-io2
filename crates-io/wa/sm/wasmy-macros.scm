(define-module (crates-io wa sm wasmy-macros) #:use-module (crates-io))

(define-public crate-wasmy-macros-0.2.0 (c (n "wasmy-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rfgqzl0dpxxd0lp32ddpkacwx0w7wlv06yw6wpx6syab5q1y9k4") (y #t)))

(define-public crate-wasmy-macros-0.2.2 (c (n "wasmy-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "100425x0jcjkihyxwfg2sadhp224fiq8dqqslj4y1bs4mdg88bkn")))

(define-public crate-wasmy-macros-0.3.0 (c (n "wasmy-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y9l3fl00rr97nra07v8x7xb4295yc5n85iin254nmfj54pns5h1")))

(define-public crate-wasmy-macros-0.3.1 (c (n "wasmy-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f9d8826rs1a5w9pa6i1ck8mhscdkmh90fzaj62wg75hn3ck61qp")))

(define-public crate-wasmy-macros-0.3.2 (c (n "wasmy-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0chwgjlw2xhwkdyljkcz8bc1frs132bs3yy3h5c7lm7g9kak6z20")))

(define-public crate-wasmy-macros-0.3.3 (c (n "wasmy-macros") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pz06iw7h923h8alsdnrs5hdxsxqs2smbp9nn5zf2dah2yczrrkq")))

(define-public crate-wasmy-macros-0.4.0 (c (n "wasmy-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05ziad2j7xg281hkfmyljydx44asipm86nhmz9a75lbfp4j8cs7z")))

(define-public crate-wasmy-macros-0.4.1 (c (n "wasmy-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cs33j7pvdppxrankfx3ha1x6n060az30i1blk25sq25ph08w20b")))

(define-public crate-wasmy-macros-0.4.2 (c (n "wasmy-macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "16yf697104nnl186vmciqfbdpw9gw0wfh9dibihdbiz0bijrhzds")))

(define-public crate-wasmy-macros-0.5.0 (c (n "wasmy-macros") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c33w7vv9446hdkfw9q3wb1nky02hn0fvm4hkxpixi61k5h9gzgl")))

(define-public crate-wasmy-macros-0.5.1 (c (n "wasmy-macros") (v "0.5.1") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f3bcdmqz24gqjgfacq8kc1nxkvvnac90pmzr6w8q80yzmdh7yj9")))

(define-public crate-wasmy-macros-0.5.5 (c (n "wasmy-macros") (v "0.5.5") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mbpgm6msbw1iadnpy7s7yxl4j2dwy4bg71za8mg3sml0zpmpa2v")))

(define-public crate-wasmy-macros-0.5.6 (c (n "wasmy-macros") (v "0.5.6") (d (list (d (n "proc-macro2") (r "^1.0.7") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "130sq9ianfxaaxmmz75bgpwxq10m9pskqq8chfv9mgi425bw2z8g")))

