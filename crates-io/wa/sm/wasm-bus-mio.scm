(define-module (crates-io wa sm wasm-bus-mio) #:use-module (crates-io))

(define-public crate-wasm-bus-mio-1.0.0 (c (n "wasm-bus-mio") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bus") (r "^1") (f (quote ("rt" "macros"))) (k 0)))) (h "0rianfg5hbr0kidblr922yg28h7xzm1ca6lks1ag09iiyis9dc3x") (f (quote (("frontend" "wasm-bus/syscalls") ("default" "frontend") ("backend"))))))

