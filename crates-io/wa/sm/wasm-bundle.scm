(define-module (crates-io wa sm wasm-bundle) #:use-module (crates-io))

(define-public crate-wasm-bundle-0.1.0 (c (n "wasm-bundle") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "cargo-lock") (r "^8.0.3") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.3") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qppp398gdvfp50v6ds71rbmn9jx115mrbaz4yrij75j7hb00ixx")))

(define-public crate-wasm-bundle-0.2.0 (c (n "wasm-bundle") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "cargo-lock") (r "^8.0.3") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.3") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xywnykh5lg4kpzz02w7psp6xahzb7fzy3s854s1lmrvdkg14qxy")))

(define-public crate-wasm-bundle-0.3.0 (c (n "wasm-bundle") (v "0.3.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "cargo-lock") (r "^8.0.3") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.15.3") (d #t) (k 0)) (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r31rz1nfi5vcj9c4qiyx5g67bprjkd4db4c5xcac0h33splcq8g")))

