(define-module (crates-io wa sm wasmer-engine-x) #:use-module (crates-io))

(define-public crate-wasmer-engine-x-2.4.1 (c (n "wasmer-engine-x") (v "2.4.1") (d (list (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "enumset") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.2") (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "=2.4.1") (d #t) (k 0) (p "wasmer-compiler-x")) (d (n "wasmer-types") (r "=2.4.1") (d #t) (k 0) (p "wasmer-types-x")) (d (n "wasmer-vm") (r "=2.4.1") (d #t) (k 0) (p "wasmer-vm-x")))) (h "04lya7rcz2d63v0y87iz542bll4p23qpp9mar3f0y8brwafv2c2g")))

