(define-module (crates-io wa sm wasm-nn) #:use-module (crates-io))

(define-public crate-wasm-nn-0.1.0 (c (n "wasm-nn") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0qh0cck75wzl2b6hjlfqpmdrdn59f12gj1kj4ayihr39hwflihi8") (f (quote (("default" "console_error_panic_hook"))))))

