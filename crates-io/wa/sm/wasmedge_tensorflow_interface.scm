(define-module (crates-io wa sm wasmedge_tensorflow_interface) #:use-module (crates-io))

(define-public crate-wasmedge_tensorflow_interface-0.2.0 (c (n "wasmedge_tensorflow_interface") (v "0.2.0") (h "1k6rxnbr554hfj2bpkc1mkykh3vnm84khrzah4a90ch56irm7aak")))

(define-public crate-wasmedge_tensorflow_interface-0.2.1 (c (n "wasmedge_tensorflow_interface") (v "0.2.1") (h "1626z4zx28cpfyfyff9myy3pz2xgh3sqlzlaa9zlvwpg6cavqm5x")))

(define-public crate-wasmedge_tensorflow_interface-0.2.2 (c (n "wasmedge_tensorflow_interface") (v "0.2.2") (h "0h3plv1jqrinb1bx1r8wkgl3yc9mw20d7mipjx33x667cf91379z")))

(define-public crate-wasmedge_tensorflow_interface-0.3.0 (c (n "wasmedge_tensorflow_interface") (v "0.3.0") (h "08k14n2zwazaj2h6h8ha145ykh9cia6q6hdnnyrb1x0q1l9f3i9a")))

