(define-module (crates-io wa sm wasmer-derive-asml-fork) #:use-module (crates-io))

(define-public crate-wasmer-derive-asml-fork-0.0.0 (c (n "wasmer-derive-asml-fork") (v "0.0.0") (d (list (d (n "compiletest_rs") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1yxjnbiv6g99zsykzmy7a9y9ninaifgva2xii7gcqgwf4kn4a55b")))

(define-public crate-wasmer-derive-asml-fork-1.0.2 (c (n "wasmer-derive-asml-fork") (v "1.0.2") (d (list (d (n "compiletest_rs") (r "^0.5") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasmer") (r "^0.0.0") (d #t) (k 2) (p "wasmer-asml-fork")))) (h "18f8h1m9swz5hddzyz1krizl4vxpbqzv8j5jpy0x4xmkl4bj4w4h")))

(define-public crate-wasmer-derive-asml-fork-2.0.0 (c (n "wasmer-derive-asml-fork") (v "2.0.0") (d (list (d (n "compiletest_rs") (r "^0.6") (d #t) (k 2)) (d (n "proc-macro-error") (r "^1.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "wasmer") (r "^2.0.0") (d #t) (k 2) (p "wasmer-asml-fork")))) (h "0lga3vhqii6dcfzl6n7q9xs0g49jgbacnp18n1rzwqgpfjlp2qgs")))

