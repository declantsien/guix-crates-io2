(define-module (crates-io wa sm wasmtime-component-util) #:use-module (crates-io))

(define-public crate-wasmtime-component-util-0.40.0 (c (n "wasmtime-component-util") (v "0.40.0") (h "0c16xb1s2klrzdqar117kx4csixmd3rldhvlh65q52d7qa2hrl49")))

(define-public crate-wasmtime-component-util-0.40.1 (c (n "wasmtime-component-util") (v "0.40.1") (h "1cp1n5jk9dp3xf707dy7qka5y1gwziraf25bfjjjmmi2czjs1qcj")))

(define-public crate-wasmtime-component-util-1.0.0 (c (n "wasmtime-component-util") (v "1.0.0") (h "0rz9f3f1v96pwdb9mjdwgw0whzc7pkfm0rjzx5074ci9x290vj8w")))

(define-public crate-wasmtime-component-util-1.0.1 (c (n "wasmtime-component-util") (v "1.0.1") (h "10kyp9q87443h61y3z8d3r063d70c9mjdnnpvk6ialsqjfgi554j")))

(define-public crate-wasmtime-component-util-2.0.0 (c (n "wasmtime-component-util") (v "2.0.0") (h "19fnhv3nfjad2v3z3s8vbk92bvswp1slajc09npk6snfj0c67wfb")))

(define-public crate-wasmtime-component-util-2.0.1 (c (n "wasmtime-component-util") (v "2.0.1") (h "1zxxzwmbav0nprab9y8wljja7kzh7m6n56ir6p6pzzgavwq49msv")))

(define-public crate-wasmtime-component-util-2.0.2 (c (n "wasmtime-component-util") (v "2.0.2") (h "0pdd073mgyww689hx0i7wkzhhrab2n5zyi8mnwy398x73fv3dx1z")))

(define-public crate-wasmtime-component-util-1.0.2 (c (n "wasmtime-component-util") (v "1.0.2") (h "06lh3bl1q3gf3m8gx5l7nmjk3lj395shh1mm7374ap32j91kwkvf")))

(define-public crate-wasmtime-component-util-3.0.0 (c (n "wasmtime-component-util") (v "3.0.0") (h "0hc7ib9c3yjf7f1f7pm20a9g4cyvr59i5y9yzxhpgggsmc0lnq2q")))

(define-public crate-wasmtime-component-util-3.0.1 (c (n "wasmtime-component-util") (v "3.0.1") (h "1w90jf8wsg217gjrca5dwh6zj14f0y4kk0rkyfdf3vmfyriscahy")))

(define-public crate-wasmtime-component-util-4.0.0 (c (n "wasmtime-component-util") (v "4.0.0") (h "1y1pi7vw7nhid5si49azwfjnq1h3115a366m0fgkz0q6q61fhsm8")))

(define-public crate-wasmtime-component-util-5.0.0 (c (n "wasmtime-component-util") (v "5.0.0") (h "10wklaw6wvb2nq6qi0piyfg09czysi88ml9dhklc7jk5wz187184")))

(define-public crate-wasmtime-component-util-6.0.0 (c (n "wasmtime-component-util") (v "6.0.0") (h "1pxzyp2v1bz5nky5j1g809qvgln991mr2nww1fv0cvmg70cxcgba")))

(define-public crate-wasmtime-component-util-5.0.1 (c (n "wasmtime-component-util") (v "5.0.1") (h "08axnff5zgi7m1zp5gcz886gi93cd7d4nqz8niyxxk8h2cx0w3pz")))

(define-public crate-wasmtime-component-util-6.0.1 (c (n "wasmtime-component-util") (v "6.0.1") (h "09204d584xa3r81paa7z5l4qqhcnnkw3g013f5ba98rb9dgafpcj")))

(define-public crate-wasmtime-component-util-4.0.1 (c (n "wasmtime-component-util") (v "4.0.1") (h "03pk25zrk11a6g7a74bqbpcwjp1dwx6n5p09psfldzbp3fnfqyzw")))

(define-public crate-wasmtime-component-util-7.0.0 (c (n "wasmtime-component-util") (v "7.0.0") (h "15zbxbszrj3gl64pqxp03izwdzq0vkf45rhh4dl3zkn132fg9k12")))

(define-public crate-wasmtime-component-util-8.0.0 (c (n "wasmtime-component-util") (v "8.0.0") (h "07f3f1xaqv5irszb1i33sqx898y5m729xq41j15x9zlrii0qk74y")))

(define-public crate-wasmtime-component-util-6.0.2 (c (n "wasmtime-component-util") (v "6.0.2") (h "1qr08c5szd200dmfkdgm3s27rmfwxzr1rlnzp7jikbamq394ywgg")))

(define-public crate-wasmtime-component-util-8.0.1 (c (n "wasmtime-component-util") (v "8.0.1") (h "1hh0j0nc9p44mg4mlhcjd3gmhf9k5qcz09lgp1r9vim3ljkjrq3l")))

(define-public crate-wasmtime-component-util-7.0.1 (c (n "wasmtime-component-util") (v "7.0.1") (h "0bmgwmm85l82q0idr6yq5g9h0ripc9zmwpg06yhrgl586gky1h6x")))

(define-public crate-wasmtime-component-util-9.0.0 (c (n "wasmtime-component-util") (v "9.0.0") (h "1490rvjnv5kakgbn6mcsdibx3gsfjkwm5glnljyyn53g1wmnr4yw")))

(define-public crate-wasmtime-component-util-9.0.1 (c (n "wasmtime-component-util") (v "9.0.1") (h "0naf4nagxpgi0k5hxzv5i5mivmc53x7la1mjz9lvqmvd8ydkpg3a")))

(define-public crate-wasmtime-component-util-9.0.2 (c (n "wasmtime-component-util") (v "9.0.2") (h "02i0kq30k8jn4ndr3036y73ka2q82gsymmxdfsknj45596wl4gmy")))

(define-public crate-wasmtime-component-util-9.0.3 (c (n "wasmtime-component-util") (v "9.0.3") (h "18a73x6f840yr6q2kh1l5i5p5h0dw8256790pk81p6iwqz57dzys")))

(define-public crate-wasmtime-component-util-9.0.4 (c (n "wasmtime-component-util") (v "9.0.4") (h "1lfi29qsp76xam3q3sycs736d4v4vall2iv8kn6843yqfyg8za3z")))

(define-public crate-wasmtime-component-util-10.0.0 (c (n "wasmtime-component-util") (v "10.0.0") (h "11jj2g5sx42q5gh67jl23ackwrv9k9y3q83vlrq805y9fxgq1h1j")))

(define-public crate-wasmtime-component-util-10.0.1 (c (n "wasmtime-component-util") (v "10.0.1") (h "097n3dm9bs9ya5h6cb8c5fmfnppssq19p5vlws011w2yxhsm22pj")))

(define-public crate-wasmtime-component-util-11.0.0 (c (n "wasmtime-component-util") (v "11.0.0") (h "0ziin5fjdhfl5fcpq1fzx2m1xcdswxci9v4iyc1s70xmhlavf5bw")))

(define-public crate-wasmtime-component-util-11.0.1 (c (n "wasmtime-component-util") (v "11.0.1") (h "17w4xaw807ag9r8ghfikda1wxf08fdjbf95m5xz8iy1bq9kgwa09")))

(define-public crate-wasmtime-component-util-12.0.0 (c (n "wasmtime-component-util") (v "12.0.0") (h "1cha14b1yg6nca0islp91khvmn7g7ll6a6zm7bc7fpc91cdvad80")))

(define-public crate-wasmtime-component-util-12.0.1 (c (n "wasmtime-component-util") (v "12.0.1") (h "1z9lzrm3s6nk42ql6mq71zmhpkb6ld7krna33j2ccikarvwjfhxm")))

(define-public crate-wasmtime-component-util-10.0.2 (c (n "wasmtime-component-util") (v "10.0.2") (h "0l8sgggzjh0qn047a6kq3ww0afvb76iqkjvjjk35vy94r2pzwlqi")))

(define-public crate-wasmtime-component-util-11.0.2 (c (n "wasmtime-component-util") (v "11.0.2") (h "0w6vb900b2r34k7pn7apqy0kxrhvm47aspdzaal2mklfdlf6pg9i")))

(define-public crate-wasmtime-component-util-12.0.2 (c (n "wasmtime-component-util") (v "12.0.2") (h "1869j8zz4k3c0sh8n3gl2pgmm4gjl0l1jq1r9v9dqly7b6y23x3l")))

(define-public crate-wasmtime-component-util-13.0.0 (c (n "wasmtime-component-util") (v "13.0.0") (h "0b0kw7ic0a0sbrw74d8cm6j956445s3xafmqq06vdxhrpxvn2hr1")))

(define-public crate-wasmtime-component-util-14.0.0 (c (n "wasmtime-component-util") (v "14.0.0") (h "1kjw2c7h95dvw7z4572x8h5wbwnd1sd7gy7fpwan3d1q0qw2q5gd")))

(define-public crate-wasmtime-component-util-14.0.1 (c (n "wasmtime-component-util") (v "14.0.1") (h "1zgcdwnvfdh9z8ii7q7wljrbnirsp5r4mgj28hrqyygqxn4x10n1")))

(define-public crate-wasmtime-component-util-13.0.1 (c (n "wasmtime-component-util") (v "13.0.1") (h "1lx1ph731mj00zprj0833zqfsjyp5i40ngfr4wm33gcl9d36jzby")))

(define-public crate-wasmtime-component-util-14.0.2 (c (n "wasmtime-component-util") (v "14.0.2") (h "0m29fx2cz62x8pk0cp3yigkq4s9qddj27c2dzpkrvxhf3b9gd3xi")))

(define-public crate-wasmtime-component-util-14.0.3 (c (n "wasmtime-component-util") (v "14.0.3") (h "0560b3n79g02hy6dcfji578mh5favazww63ap9qlk67p0nkj1jrk")))

(define-public crate-wasmtime-component-util-14.0.4 (c (n "wasmtime-component-util") (v "14.0.4") (h "14q0f8ab2npjn77jh1k3nnjg3740q1rq2fhcxrp117h6vyl434cz")))

(define-public crate-wasmtime-component-util-15.0.0 (c (n "wasmtime-component-util") (v "15.0.0") (h "0gh2f92v941icc9nkxcv2z6v0bqcgnb7xas6jjjnmlvyk8qz319y")))

(define-public crate-wasmtime-component-util-15.0.1 (c (n "wasmtime-component-util") (v "15.0.1") (h "0ryg3sjmyaq1n1f876237jr5bnnnzakfkmily1rwn1k0g8bjlj85")))

(define-public crate-wasmtime-component-util-16.0.0 (c (n "wasmtime-component-util") (v "16.0.0") (h "17gdvgjb0ll4jqid074nf0i2pipbyj2hmh2lycc4h6nj31nd11wc")))

(define-public crate-wasmtime-component-util-17.0.0 (c (n "wasmtime-component-util") (v "17.0.0") (h "01l9xg4w5khgig640435n6x46s9nwjl13lz6psj9ldj41h61c2ky")))

(define-public crate-wasmtime-component-util-17.0.1 (c (n "wasmtime-component-util") (v "17.0.1") (h "18cmvw5p4jwvrbharm5m9xbdw8w9z3mkfiwy3wz5iiasmnvp5w4a")))

(define-public crate-wasmtime-component-util-18.0.0 (c (n "wasmtime-component-util") (v "18.0.0") (h "1rb7r5567j8mdilqdnjv3vi7l8g7fy5kih4qc8v76cm5g4mc9ks6")))

(define-public crate-wasmtime-component-util-18.0.1 (c (n "wasmtime-component-util") (v "18.0.1") (h "1b1skbkk9fq1jn212f0hqjmpi1xrkkpwsqiiq1gzjka8kxhp57rc")))

(define-public crate-wasmtime-component-util-17.0.2 (c (n "wasmtime-component-util") (v "17.0.2") (h "13k8nmw0l7w6ldak67mmjvks036ygbfswbvn54zqqgmz6mw63089")))

(define-public crate-wasmtime-component-util-18.0.2 (c (n "wasmtime-component-util") (v "18.0.2") (h "127zkk2anyjv2bmy9g9f37h3r6365jb0d58m8bn71p6d6326jv8x")))

(define-public crate-wasmtime-component-util-18.0.3 (c (n "wasmtime-component-util") (v "18.0.3") (h "0xr7ypmva710b4yrply4g84262hj431kpa0djnry0zh2k3nf5sl1")))

(define-public crate-wasmtime-component-util-19.0.0 (c (n "wasmtime-component-util") (v "19.0.0") (h "00g54hgwqy4gixyc9v3fia3jwhnp6brk8dmb6biy3yp2cszyffmb")))

(define-public crate-wasmtime-component-util-19.0.1 (c (n "wasmtime-component-util") (v "19.0.1") (h "0x7ghw3hsgvgj7n77ybhrbyaxxrm3wh4r7ayv817d3sml1mlkbc2")))

(define-public crate-wasmtime-component-util-18.0.4 (c (n "wasmtime-component-util") (v "18.0.4") (h "11qffgy7g71s5bnfw48ckbk2q744yirdf9c5z51rq0xdjz60ga8a")))

(define-public crate-wasmtime-component-util-19.0.2 (c (n "wasmtime-component-util") (v "19.0.2") (h "09cizbzb1y11sa7z13wi13ljmf2mixihg7bb9g98zcn0xg0pvl8d")))

(define-public crate-wasmtime-component-util-17.0.3 (c (n "wasmtime-component-util") (v "17.0.3") (h "01wqkq7yyl37b4ralc1n06g78ac250k5mm75g4f0d7vcyqaq3b85")))

(define-public crate-wasmtime-component-util-20.0.0 (c (n "wasmtime-component-util") (v "20.0.0") (h "01q376fdfrpryb3xbr1nhmk854yjl6f5b240z4xxmiq49vdhnn3q")))

(define-public crate-wasmtime-component-util-20.0.1 (c (n "wasmtime-component-util") (v "20.0.1") (h "058sf6mr5x4wyf1j4k2jvzjpxbn7ylkcq0zz80i4iqy345m5xmj4")))

(define-public crate-wasmtime-component-util-20.0.2 (c (n "wasmtime-component-util") (v "20.0.2") (h "1w3lvwn4q3fqnmpcv9y7vqmzbfz0x25z21d1n8fbw5sxw6ws2fbq")))

(define-public crate-wasmtime-component-util-21.0.0 (c (n "wasmtime-component-util") (v "21.0.0") (h "1vadd7b3af8nd16brw0j9vqv93vdxx02zczlydqk1ni4gybq3k2j")))

(define-public crate-wasmtime-component-util-21.0.1 (c (n "wasmtime-component-util") (v "21.0.1") (h "0d3i1236m1glpmn3lfcg3zq5n4cnibi5p4ihfg3219nf10gn3bpp")))

