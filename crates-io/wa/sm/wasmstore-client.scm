(define-module (crates-io wa sm wasmstore-client) #:use-module (crates-io))

(define-public crate-wasmstore-client-0.1.0 (c (n "wasmstore-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0xwlbsdw1s03gcr4mxc3jnprv5pmcclw2jfmg14i6bm2s83801ba")))

(define-public crate-wasmstore-client-0.2.0 (c (n "wasmstore-client") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ggnv07n6wfqpbrird58lrcg6zaxfd84kj0myrlwgk1dfki80jmd")))

(define-public crate-wasmstore-client-0.3.0 (c (n "wasmstore-client") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0grd8lg2n78yf6dlfwjbjpdyadag6c60f9niq6ndx4saj12d4irw")))

