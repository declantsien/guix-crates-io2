(define-module (crates-io wa sm wasm4fun-core) #:use-module (crates-io))

(define-public crate-wasm4fun-core-0.1.0-dev (c (n "wasm4fun-core") (v "0.1.0-dev") (h "13brcgfinbabrbkc9qq0v9n2iiwvirxpnhygh4lgwc7ja7by760z") (y #t)))

(define-public crate-wasm4fun-core-0.1.0 (c (n "wasm4fun-core") (v "0.1.0") (h "18w8wdrblpzrqd22sy1mhq23478b57agijkna0ilm1kxcv53wr74")))

