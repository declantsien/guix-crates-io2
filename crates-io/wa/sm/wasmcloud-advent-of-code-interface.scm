(define-module (crates-io wa sm wasmcloud-advent-of-code-interface) #:use-module (crates-io))

(define-public crate-wasmcloud-advent-of-code-interface-0.1.0 (c (n "wasmcloud-advent-of-code-interface") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "base64") (r "^0.13") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.11") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.6") (d #t) (k 1)))) (h "0vk769gf7fyw3hrphldy21bdlx9nbx73c47pkv6a9v9lrfd4zw4r")))

