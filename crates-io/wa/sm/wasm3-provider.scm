(define-module (crates-io wa sm wasm3-provider) #:use-module (crates-io))

(define-public crate-wasm3-provider-0.0.1 (c (n "wasm3-provider") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wapc") (r "^0.10.0") (d #t) (k 0)) (d (n "wasm3") (r "^0.1.3") (f (quote ("build-bindgen"))) (d #t) (k 0)))) (h "0w734v1jgs5ia9qw32zm94hvd2ajb8a27n9p3r8rhh6wxlpcmpzr")))

(define-public crate-wasm3-provider-0.0.2 (c (n "wasm3-provider") (v "0.0.2") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wapc") (r "^0.10.1") (d #t) (k 0)) (d (n "wasm3") (r "^0.1.3") (f (quote ("build-bindgen"))) (d #t) (k 0)))) (h "01xj1awcb9rm3n76z0ns2hnry56n4qhlcscd11c1380qlzpvdrql")))

(define-public crate-wasm3-provider-0.0.3 (c (n "wasm3-provider") (v "0.0.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wapc") (r "^0.10.1") (d #t) (k 0)) (d (n "wasm3") (r "^0.1.3") (f (quote ("build-bindgen"))) (d #t) (k 0)))) (h "0i4qnha408gd566s1jsjxswblb57qhw1g7ngjd2gcvvk620kiy0c")))

(define-public crate-wasm3-provider-1.0.0-alpha.0 (c (n "wasm3-provider") (v "1.0.0-alpha.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wapc") (r "^1.0.0-alpha.0") (d #t) (k 0)) (d (n "wasm3") (r "^0.3.1") (f (quote ("build-bindgen"))) (d #t) (k 0)))) (h "01i15xp4qczkgdaixpv392ygw4fvpj4pk8cjgynz0b9g9626j841")))

(define-public crate-wasm3-provider-1.0.0 (c (n "wasm3-provider") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wapc") (r "^1.0.0-alpha.0") (d #t) (k 0)) (d (n "wasm3") (r "^0.3.1") (f (quote ("build-bindgen"))) (d #t) (k 0)))) (h "0dp9ifcwlf2mvr9yk0bnhv75d9nfk4nbyfcj5pjans315vw2sixb")))

(define-public crate-wasm3-provider-1.1.0 (c (n "wasm3-provider") (v "1.1.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wapc") (r "^1.1.0") (d #t) (k 0)) (d (n "wasm3") (r "^0.3.1") (f (quote ("build-bindgen"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 2)))) (h "012djxavnlc9fg4ww5zybbi87qhmliydjcw0k5na35kgn6bxlvpy")))

