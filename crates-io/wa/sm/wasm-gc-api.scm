(define-module (crates-io wa sm wasm-gc-api) #:use-module (crates-io))

(define-public crate-wasm-gc-api-0.1.0 (c (n "wasm-gc-api") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.23") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "1pqvgvarrg0d5zxn40dmc4sswfh6pjwd9c5hgc8hkbp1qhpk41ab")))

(define-public crate-wasm-gc-api-0.1.1 (c (n "wasm-gc-api") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.27") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "0k88ar90g51fbzscb3j0ha6cxwnypgwn105a7hr0v0n2zpvlvqvd")))

(define-public crate-wasm-gc-api-0.1.2 (c (n "wasm-gc-api") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.27") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "0w523phr978j8rmssybwsxxnn3j2jal96w1ra9vgb1jz4z4vziry")))

(define-public crate-wasm-gc-api-0.1.3 (c (n "wasm-gc-api") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.27.5") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "0r2785wbxbp05gxj15fdb87fwrzwcw7ryiqn9gc0j1gr37a716d4")))

(define-public crate-wasm-gc-api-0.1.4 (c (n "wasm-gc-api") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.28") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "0zsailq8rfzqdnw7p9c0c8amns0y50h8iy49i5hwj1piic2xh940")))

(define-public crate-wasm-gc-api-0.1.5 (c (n "wasm-gc-api") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.30") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "1rpzsl0gqrzddrl8jy9v97kx72b2nkn5rcjh2n2rkfybcmalwh17")))

(define-public crate-wasm-gc-api-0.1.6 (c (n "wasm-gc-api") (v "0.1.6") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "1jjqzgbhhh428jlhgf1kjmsd5np5az5vlysi5189098r7szi42f8")))

(define-public crate-wasm-gc-api-0.1.7 (c (n "wasm-gc-api") (v "0.1.7") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "1162yicnlwk58384jg3qxhfwg2mmy1d9aic3854y1rgg1dapv5zm")))

(define-public crate-wasm-gc-api-0.1.8 (c (n "wasm-gc-api") (v "0.1.8") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "1ys0kwg1yipbkyn6r3x725mf07qxhiv28g9z6ms9z66fb80l4nla")))

(define-public crate-wasm-gc-api-0.1.9 (c (n "wasm-gc-api") (v "0.1.9") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)))) (h "1q5qv2xkhljc6lcamgwgzmhs2c6d0yyihhwzgssmhjr8ckrwpbns")))

(define-public crate-wasm-gc-api-0.1.10 (c (n "wasm-gc-api") (v "0.1.10") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)))) (h "159nd33rpqz2ncrv2b3jd4ifgfh94i7fzcdq780iwhn0v9n4dypz")))

(define-public crate-wasm-gc-api-0.1.11 (c (n "wasm-gc-api") (v "0.1.11") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.32") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)))) (h "1fc6444a3cq9dk8gq84nlix512q8m5hm7mcggx7c3rn7ns8jdhyh")))

