(define-module (crates-io wa sm wasmtime_serde_guest_macro) #:use-module (crates-io))

(define-public crate-wasmtime_serde_guest_macro-0.1.0 (c (n "wasmtime_serde_guest_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)))) (h "02dr4xnjj2gnkhp7g20hkcinanyniv1rajywn9gz1m0yh2vsz4zw")))

