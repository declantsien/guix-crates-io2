(define-module (crates-io wa sm wasmium_errors) #:use-module (crates-io))

(define-public crate-wasmium_errors-1.0.1 (c (n "wasmium_errors") (v "1.0.1") (h "1cpd95v99x1hnaif5asd62pghf3dc5riypipzclh7j9sh1ry3bv1")))

(define-public crate-wasmium_errors-1.0.2 (c (n "wasmium_errors") (v "1.0.2") (h "0zfp8b0ryjy18q7rrzxbzwqd898sb8gy9fw86zqy0dj4w1q52fw7")))

(define-public crate-wasmium_errors-1.0.3 (c (n "wasmium_errors") (v "1.0.3") (h "12p7a8x8qfc0daibfp25jm7l47y7fijfn0yira8svcq35dl5qdq0")))

