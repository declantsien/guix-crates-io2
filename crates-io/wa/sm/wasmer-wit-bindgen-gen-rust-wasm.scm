(define-module (crates-io wa sm wasmer-wit-bindgen-gen-rust-wasm) #:use-module (crates-io))

(define-public crate-wasmer-wit-bindgen-gen-rust-wasm-0.1.0 (c (n "wasmer-wit-bindgen-gen-rust-wasm") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wasmer-wit-bindgen-gen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmer-wit-bindgen-gen-rust") (r "^0.1.0") (d #t) (k 0)))) (h "0yyhbjm35z851rqi9lwjcbi9w25a4zn4d9ipq42y9h6753zc98cf")))

(define-public crate-wasmer-wit-bindgen-gen-rust-wasm-0.1.1 (c (n "wasmer-wit-bindgen-gen-rust-wasm") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wasmer-wit-bindgen-gen-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmer-wit-bindgen-gen-rust") (r "^0.1.0") (d #t) (k 0)))) (h "1z5swyz3cksn4688647p3pgxd59gbrf8l59yw501hngfrxl52psh")))

