(define-module (crates-io wa sm wasmtime-lightbeam) #:use-module (crates-io))

(define-public crate-wasmtime-lightbeam-0.20.0 (c (n "wasmtime-lightbeam") (v "0.20.0") (d (list (d (n "cranelift-codegen") (r "^0.67.0") (d #t) (k 0)) (d (n "lightbeam") (r "^0.20.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.59") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.20.0") (d #t) (k 0)))) (h "0lmx5nn0hk8ramc8z62f5ikr7rz0p54pvbsbddq9k6f9w1001cwx")))

(define-public crate-wasmtime-lightbeam-0.21.0 (c (n "wasmtime-lightbeam") (v "0.21.0") (d (list (d (n "cranelift-codegen") (r "^0.68.0") (d #t) (k 0)) (d (n "lightbeam") (r "^0.21.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.65") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.21.0") (d #t) (k 0)))) (h "0ff0pmr8xygxp275yn1wc3m3jgqzysn7hhhqficvqdmz38f917hj")))

(define-public crate-wasmtime-lightbeam-0.22.0 (c (n "wasmtime-lightbeam") (v "0.22.0") (d (list (d (n "cranelift-codegen") (r "^0.69.0") (d #t) (k 0)) (d (n "lightbeam") (r "^0.22.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.71") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.22.0") (d #t) (k 0)))) (h "0z50x2z0y9z3va04gysh1m37ly0gvn0vw20axg9w2lwspgjnga3l")))

(define-public crate-wasmtime-lightbeam-0.23.0 (c (n "wasmtime-lightbeam") (v "0.23.0") (d (list (d (n "cranelift-codegen") (r "^0.70.0") (d #t) (k 0)) (d (n "lightbeam") (r "^0.23.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.73") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.23.0") (d #t) (k 0)))) (h "0gkzd2f0kxgmrzhk9v56pnxdagd3wbhym9dyddnrzaq96yjwki3b")))

(define-public crate-wasmtime-lightbeam-0.24.0 (c (n "wasmtime-lightbeam") (v "0.24.0") (d (list (d (n "cranelift-codegen") (r "^0.71.0") (d #t) (k 0)) (d (n "lightbeam") (r "^0.24.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.76") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.24.0") (d #t) (k 0)))) (h "1bi04n0hrz2hqlpgd607fmlc9b2bj0f51gnw0xd53qj48w1g6in8")))

(define-public crate-wasmtime-lightbeam-0.25.0 (c (n "wasmtime-lightbeam") (v "0.25.0") (d (list (d (n "cranelift-codegen") (r "^0.72.0") (d #t) (k 0)) (d (n "lightbeam") (r "^0.25.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.76") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.25.0") (d #t) (k 0)))) (h "0afvf2ww90s9g5d1w8sh3ll2m468d2cbq44rn7h99zcs4yy2dqf9")))

(define-public crate-wasmtime-lightbeam-0.26.0 (c (n "wasmtime-lightbeam") (v "0.26.0") (d (list (d (n "cranelift-codegen") (r "^0.73.0") (d #t) (k 0)) (d (n "lightbeam") (r "^0.26.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.77") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.26.0") (d #t) (k 0)))) (h "0igh94jf9jymjml46lwkc76p1vm868rgb0mqxy0402wskycbx540")))

(define-public crate-wasmtime-lightbeam-0.27.0 (c (n "wasmtime-lightbeam") (v "0.27.0") (d (list (d (n "cranelift-codegen") (r "^0.74.0") (d #t) (k 0)) (d (n "lightbeam") (r "^0.27.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.78") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.27.0") (d #t) (k 0)))) (h "03vy84jr3727lhwvyrkqlgziwi7dk0llkgnia8vixzlvvlrhnlrf")))

(define-public crate-wasmtime-lightbeam-0.26.1 (c (n "wasmtime-lightbeam") (v "0.26.1") (d (list (d (n "cranelift-codegen") (r "^0.73.1") (d #t) (k 0)) (d (n "lightbeam") (r "^0.26.1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.77") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.26.1") (d #t) (k 0)))) (h "0w82dj1lwq9vfq4sw66g9asjaizjizhk83gk9qiamkrwyqi190ar")))

(define-public crate-wasmtime-lightbeam-0.28.0 (c (n "wasmtime-lightbeam") (v "0.28.0") (d (list (d (n "cranelift-codegen") (r "^0.75.0") (d #t) (k 0)) (d (n "lightbeam") (r "^0.28.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.78") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.28.0") (d #t) (k 0)))) (h "1ppchxv68ivf7bq26pin3psmaa716xpvvy34ij1p2n1vz05588f0")))

(define-public crate-wasmtime-lightbeam-0.29.0 (c (n "wasmtime-lightbeam") (v "0.29.0") (d (list (d (n "cranelift-codegen") (r "^0.76.0") (d #t) (k 0)) (d (n "lightbeam") (r "^0.29.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.79") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.29.0") (d #t) (k 0)))) (h "1bnnwbxw1cji52plxbp6aqgfvn95wkayr3cniasld4y1w9pxn40s")))

(define-public crate-wasmtime-lightbeam-0.30.0 (c (n "wasmtime-lightbeam") (v "0.30.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.77.0") (d #t) (k 0)) (d (n "gimli") (r "^0.25") (d #t) (k 0)) (d (n "lightbeam") (r "^0.30.0") (d #t) (k 0)) (d (n "object") (r "^0.26.0") (k 0)) (d (n "target-lexicon") (r "^0.12") (d #t) (k 0)) (d (n "wasmparser") (r "^0.80") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.30.0") (d #t) (k 0)))) (h "1zl2l4dmx21gak6wk060i81055cajy4ylphbszq351idmsbqrywp")))

