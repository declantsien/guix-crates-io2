(define-module (crates-io wa sm wasm-mt-pool) #:use-module (crates-io))

(define-public crate-wasm-mt-pool-0.1.0 (c (n "wasm-mt-pool") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "wasm-mt") (r "^0.1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "0jpianc51m2f1hschz9bzk03k5lp33zah9cc2wrwn2fdaq7r3gmg")))

(define-public crate-wasm-mt-pool-0.1.1 (c (n "wasm-mt-pool") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "wasm-mt") (r "^0.1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "0scsk88ymb9bd3accarh38qnhfklphcfdv1a63hdan9pdkf6h1hf")))

(define-public crate-wasm-mt-pool-0.1.2 (c (n "wasm-mt-pool") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "wasm-mt") (r "^0.1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "06nlbqv5f9y266xk8nzw3dcvvaplpzylkcjybrx163k2vmcr34cz")))

