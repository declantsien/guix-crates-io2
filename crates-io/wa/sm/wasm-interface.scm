(define-module (crates-io wa sm wasm-interface) #:use-module (crates-io))

(define-public crate-wasm-interface-0.0.0 (c (n "wasm-interface") (v "0.0.0") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "wabt") (r "^0.7") (d #t) (k 2)) (d (n "wasmparser") (r "^0.32") (o #t) (d #t) (k 0)))) (h "0247apspdc9vfdx1mr47qfwk7hfqjacbz2s74nh7bk013z4xd2if") (f (quote (("validation" "wasmparser") ("default" "validation"))))))

(define-public crate-wasm-interface-0.0.1 (c (n "wasm-interface") (v "0.0.1") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "wabt") (r "^0.7") (d #t) (k 2)) (d (n "wasmparser") (r "^0.32") (o #t) (d #t) (k 0)))) (h "1ffg66f7cfjq7fngn2azxzl463610wvglf2y16hxjq76phnzs9cs") (f (quote (("validation" "wasmparser") ("default" "validation"))))))

(define-public crate-wasm-interface-0.0.2 (c (n "wasm-interface") (v "0.0.2") (d (list (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "wabt") (r "^0.7") (d #t) (k 2)) (d (n "wasmparser") (r "^0.32") (o #t) (d #t) (k 0)))) (h "0j08s24hvj9qy1dcbq77xvkql3gh1n1k5dkzk6h1qc082g5dxd4x") (f (quote (("validation" "wasmparser") ("default" "validation"))))))

(define-public crate-wasm-interface-0.1.0 (c (n "wasm-interface") (v "0.1.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)) (d (n "wabt") (r "^0.7") (d #t) (k 2)) (d (n "wasmparser") (r "^0.32") (o #t) (d #t) (k 0)))) (h "0h3by09g9wad5ppsq3hj43w6w9025a3gpi7acdchhl1k2al46nih") (f (quote (("validation" "wasmparser") ("default" "validation"))))))

