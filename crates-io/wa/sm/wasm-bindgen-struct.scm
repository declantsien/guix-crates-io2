(define-module (crates-io wa sm wasm-bindgen-struct) #:use-module (crates-io))

(define-public crate-wasm-bindgen-struct-0.1.0 (c (n "wasm-bindgen-struct") (v "0.1.0") (d (list (d (n "attribute-derive") (r "^0.6") (d #t) (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (k 2)) (d (n "prettyplease") (r "^0.2") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)) (d (n "termdiff") (r "^3") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 2)) (d (n "wasm-bindgen-futures") (r "^0.4") (d #t) (k 2)))) (h "0dp3jj9zxffa6hlg8qk9aav3lnaa25z2v364d9hv4zmggdahm4vb")))

