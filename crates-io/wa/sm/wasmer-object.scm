(define-module (crates-io wa sm wasmer-object) #:use-module (crates-io))

(define-public crate-wasmer-object-1.0.0-alpha01.0 (c (n "wasmer-object") (v "1.0.0-alpha01.0") (d (list (d (n "object") (r "^0.19") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.0-alpha01.0") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.0-alpha01.0") (d #t) (k 0)))) (h "1kymfmbnccbp8rsyi41z2s50qy69qx24kdggdiyrq1b1l9p475jj")))

(define-public crate-wasmer-object-1.0.0-alpha02.0 (c (n "wasmer-object") (v "1.0.0-alpha02.0") (d (list (d (n "object") (r "^0.19") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.0-alpha02.0") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.0-alpha02.0") (d #t) (k 0)))) (h "0s1lxac87yp1m95d8xl6k8k91221pgg62phjgrx1z2ncyzjs8gv5")))

(define-public crate-wasmer-object-1.0.0-alpha3 (c (n "wasmer-object") (v "1.0.0-alpha3") (d (list (d (n "object") (r "^0.19") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.0-alpha3") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.0-alpha3") (d #t) (k 0)))) (h "0klzv11gbwdklpgrj1pcjhvgq9r5pw0jppyv6rw47f2xkvb15sfh")))

(define-public crate-wasmer-object-1.0.0-alpha4 (c (n "wasmer-object") (v "1.0.0-alpha4") (d (list (d (n "object") (r "^0.19") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.0-alpha4") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.0-alpha4") (d #t) (k 0)))) (h "1p53hk8bnf3gclhb9zwr9pbxjli7v1dixivaf27m9zmarj5dkw16")))

(define-public crate-wasmer-object-1.0.0-alpha5 (c (n "wasmer-object") (v "1.0.0-alpha5") (d (list (d (n "object") (r "^0.21") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.0-alpha5") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.0-alpha5") (d #t) (k 0)))) (h "0akds27siqgay2vglq0azdr2pdivm9y3h4r468pc6xb4ll2l58yy")))

(define-public crate-wasmer-object-1.0.0-beta1 (c (n "wasmer-object") (v "1.0.0-beta1") (d (list (d (n "object") (r "^0.22") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.0-beta1") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.0-beta1") (d #t) (k 0)))) (h "1ksbz3ywnbkpbwa1hc8cc7bkx4xhwhjhbzsg0ffbfzgizxxr7bs8")))

(define-public crate-wasmer-object-1.0.0-beta2 (c (n "wasmer-object") (v "1.0.0-beta2") (d (list (d (n "object") (r "^0.22") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.0-beta2") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.0-beta2") (d #t) (k 0)))) (h "135dvf53i1hdv2rb8jkh0iff7pqzyq3rbfzbv53lh6f55v00ppdb")))

(define-public crate-wasmer-object-1.0.0-rc1 (c (n "wasmer-object") (v "1.0.0-rc1") (d (list (d (n "object") (r "^0.22") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.0-rc1") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.0-rc1") (d #t) (k 0)))) (h "02gks6vqfarg28bw7l23lmbrk57fp56yj3fkidcb6wrd0l1wyb72")))

(define-public crate-wasmer-object-1.0.0 (c (n "wasmer-object") (v "1.0.0") (d (list (d (n "object") (r "^0.22") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.0") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.0") (d #t) (k 0)))) (h "0m34wamfd8vnkq9csac14agixzzwmcnhji8hqvn5lxy77vkhgq06")))

(define-public crate-wasmer-object-1.0.1 (c (n "wasmer-object") (v "1.0.1") (d (list (d (n "object") (r "^0.22") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.1") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.1") (d #t) (k 0)))) (h "12jz67rjkjg4zx6yb1agnqcl7926yqdc2q14cvl0z5898cxa14dr")))

(define-public crate-wasmer-object-1.0.2 (c (n "wasmer-object") (v "1.0.2") (d (list (d (n "object") (r "^0.22") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^1.0.2") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^1.0.2") (d #t) (k 0)))) (h "0ywsfq5dy1dixs85m26ysrqwizp5ic8pw3fkxkmq3zw25g0y1y5b")))

(define-public crate-wasmer-object-2.0.0-rc1 (c (n "wasmer-object") (v "2.0.0-rc1") (d (list (d (n "object") (r "^0.24") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^2.0.0-rc1") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^2.0.0-rc1") (d #t) (k 0)))) (h "170v2px6b6yq0hsdm2bnnniqh6vlzxma4d344k0lwd6dmvkmnna6")))

(define-public crate-wasmer-object-2.0.0-rc2 (c (n "wasmer-object") (v "2.0.0-rc2") (d (list (d (n "object") (r "^0.24") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^2.0.0-rc2") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^2.0.0-rc2") (d #t) (k 0)))) (h "0j5dw4caj2rg9clvsrifs2fcyayyv031h6an3yw0kif6ssw80ng9")))

(define-public crate-wasmer-object-2.0.0 (c (n "wasmer-object") (v "2.0.0") (d (list (d (n "object") (r "^0.25") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^2.0.0") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^2.0.0") (d #t) (k 0)))) (h "05cyfgc7yhf7q7pksryrj9n0dyzc3ga1a72h093l9hczg62wjhf5")))

(define-public crate-wasmer-object-2.1.0 (c (n "wasmer-object") (v "2.1.0") (d (list (d (n "object") (r "^0.27") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^2.1.0") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^2.1.0") (d #t) (k 0)))) (h "1n1j6m6ydlpm9cn6r279lc6xb1cv251i0y5zb7n6xw3d7sp42k59")))

(define-public crate-wasmer-object-2.1.1 (c (n "wasmer-object") (v "2.1.1") (d (list (d (n "object") (r "^0.27") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "^2.1.1") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "^2.1.1") (d #t) (k 0)))) (h "1xvjrcnp2r2nxh01v8vd0gg9kk8rgm0q8hn2awhkpp1v9x773m63")))

(define-public crate-wasmer-object-2.2.0-rc1 (c (n "wasmer-object") (v "2.2.0-rc1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "=2.2.0-rc1") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "=2.2.0-rc1") (d #t) (k 0)))) (h "187r1x25acsf8xpqxyxnn0pvmncicnix74wlk5xp8qp0wlw6jqpm")))

(define-public crate-wasmer-object-2.2.0-rc2 (c (n "wasmer-object") (v "2.2.0-rc2") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "=2.2.0-rc2") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "=2.2.0-rc2") (d #t) (k 0)))) (h "041ap0117i107qzlnv84drx8grsdaq9whwjccxjfz254lsz8mf7z")))

(define-public crate-wasmer-object-2.2.0 (c (n "wasmer-object") (v "2.2.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "=2.2.0") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "=2.2.0") (d #t) (k 0)))) (h "0h4h4am8d5c0q0gqxmwf6cw62yy6z4p112921y2bv619b42l030x")))

(define-public crate-wasmer-object-2.2.1 (c (n "wasmer-object") (v "2.2.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "=2.2.1") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "=2.2.1") (d #t) (k 0)))) (h "1dg0ss47ig1ahnhbnzz765x2s6gki0dfw9qdb1chw1bp52n1iki4")))

(define-public crate-wasmer-object-2.3.0 (c (n "wasmer-object") (v "2.3.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "=2.3.0") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "=2.3.0") (d #t) (k 0)))) (h "12zsm0iqdb14xgn2rf9nh1445ikm264nygrha7sfqi1szwsi70wd")))

(define-public crate-wasmer-object-3.0.0-alpha.3 (c (n "wasmer-object") (v "3.0.0-alpha.3") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "=3.0.0-alpha.3") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "=3.0.0-alpha.3") (d #t) (k 0)))) (h "1xhrmp955azb3jdldgpmrfd0cyr28jmvwcjfq5mrad0zzrhwid6j")))

(define-public crate-wasmer-object-3.0.0-alpha.4 (c (n "wasmer-object") (v "3.0.0-alpha.4") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "=3.0.0-alpha.4") (f (quote ("std" "translator"))) (k 0)) (d (n "wasmer-types") (r "=3.0.0-alpha.4") (d #t) (k 0)))) (h "0ri9y74yhwwmri42nhcphq90kdmf6nirxdrip9kjpcpqx8h1jr1b")))

(define-public crate-wasmer-object-3.0.0-beta (c (n "wasmer-object") (v "3.0.0-beta") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.0.0-beta") (d #t) (k 0)))) (h "1yh8h6vqqnrx3w2n798z9hlmnc1dx1qryl6n6w71mbf78h7lssrp")))

(define-public crate-wasmer-object-3.0.0-beta.2 (c (n "wasmer-object") (v "3.0.0-beta.2") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.0.0-beta.2") (d #t) (k 0)))) (h "0cvnzwqmjgckib9h7f4fzp37hf6516hmks72fxqmagyxjf1igb5z")))

(define-public crate-wasmer-object-3.0.0-rc.1 (c (n "wasmer-object") (v "3.0.0-rc.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.0.0-rc.1") (d #t) (k 0)))) (h "01d3jn2na5wdfavb84i3zwgi366135san75rv6aisyrmf6s68jkr")))

(define-public crate-wasmer-object-3.0.0-rc.2 (c (n "wasmer-object") (v "3.0.0-rc.2") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.0.0-rc.2") (d #t) (k 0)))) (h "0j8hs7g1cc2vc1r6rgz20zq5xzp600jv9gpswq7w01mf41vb5i14")))

(define-public crate-wasmer-object-3.0.0-rc.3 (c (n "wasmer-object") (v "3.0.0-rc.3") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.0.0-rc.3") (d #t) (k 0)))) (h "0y4ji2cffbcnp80ajhxjv30jsafgn21m9936bbijsgjjwg3b7vb0")))

(define-public crate-wasmer-object-3.0.0-rc.4 (c (n "wasmer-object") (v "3.0.0-rc.4") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.0.0-rc.4") (d #t) (k 0)))) (h "0q33cdbccq6ygvs2bqzgcmksq9lkw18486khrdly9c7n25f2ijzb")))

(define-public crate-wasmer-object-3.0.0 (c (n "wasmer-object") (v "3.0.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.0.0") (d #t) (k 0)))) (h "17j8yhn9ccplh2xws85v00dkifsi9az7iyhzhh18vjmzfx4maq3k")))

(define-public crate-wasmer-object-3.0.1 (c (n "wasmer-object") (v "3.0.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.0.1") (d #t) (k 0)))) (h "1w5nphb7rf2c0jdy18znw4zw0w7c9a01ja9ci3zivsmk38940kym")))

(define-public crate-wasmer-object-3.0.2 (c (n "wasmer-object") (v "3.0.2") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.0.2") (d #t) (k 0)))) (h "00dxcaixksyz3jnkypcsgpkx8dmd0xa7sx5fnjnwl9symcqadcz6")))

(define-public crate-wasmer-object-3.1.0 (c (n "wasmer-object") (v "3.1.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.1.0") (d #t) (k 0)))) (h "0ng8phfsj1d2za04xqw68rb65az42lmjycv347x45k6k05hxmg42")))

(define-public crate-wasmer-object-3.2.0-alpha.1 (c (n "wasmer-object") (v "3.2.0-alpha.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.2.0-alpha.1") (d #t) (k 0)))) (h "050a06h91s6pf1q1mwn9q7w0jlvni98fszqfvwxqjihp4iia63h2")))

(define-public crate-wasmer-object-3.0.3 (c (n "wasmer-object") (v "3.0.3") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.0.3") (d #t) (k 0)))) (h "1a059gbfsn3d8an8bd69gp71y3hv4aldg9z2j4zzc3ak77rgs04y")))

(define-public crate-wasmer-object-3.1.1 (c (n "wasmer-object") (v "3.1.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.1.1") (d #t) (k 0)))) (h "1ha6qn4mzhn32y2kxv3zbxs0z14vnfsiryfwhc4fw282mcl41c71")))

(define-public crate-wasmer-object-3.2.0-beta.1 (c (n "wasmer-object") (v "3.2.0-beta.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.2.0-beta.1") (d #t) (k 0)))) (h "16bfxqkcl2b60sabq6dnzf3l1wqbfvcnk78276ixciilw7xry21j")))

(define-public crate-wasmer-object-3.2.0-beta.2 (c (n "wasmer-object") (v "3.2.0-beta.2") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.2.0-beta.2") (d #t) (k 0)))) (h "0733h5xanpha5ljhvqxia0y07nw70f05cg3wdhrlz59nf8z6p582")))

(define-public crate-wasmer-object-3.2.0 (c (n "wasmer-object") (v "3.2.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.2.0") (d #t) (k 0)))) (h "0apclcdqd8hd7zbgpqqg9nv0hdi23718zskqq4p3m2qcmhr0svjg")))

(define-public crate-wasmer-object-3.2.1 (c (n "wasmer-object") (v "3.2.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.2.1") (d #t) (k 0)))) (h "087vfk0llqw4ga3fp5ws7gncc16vwsbz7vb92l3rrdda7dii4dzd")))

(define-public crate-wasmer-object-3.3.0 (c (n "wasmer-object") (v "3.3.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=3.3.0") (d #t) (k 0)))) (h "1bja4is4whdzfrldgzksh92mxfhzmh22ikmzprrj23yx9zh2ashv")))

(define-public crate-wasmer-object-4.0.0-alpha.1 (c (n "wasmer-object") (v "4.0.0-alpha.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.0.0-alpha.1") (d #t) (k 0)))) (h "1xh5fsdkwjrjbzgj330xgiidp94gip2vja5cmlncj8nmzbrbrx6r")))

(define-public crate-wasmer-object-4.0.0-beta.1 (c (n "wasmer-object") (v "4.0.0-beta.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.0.0-beta.1") (d #t) (k 0)))) (h "01x3fq6s80sk6d1bm6xliq60cs1mxps0pbrjslnkqrxps7m7hnla") (r "1.67")))

(define-public crate-wasmer-object-4.0.0-beta.2 (c (n "wasmer-object") (v "4.0.0-beta.2") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.0.0-beta.2") (d #t) (k 0)))) (h "117sa9kpbjzvmz7d7ifd5rpqs13rm5gr6gwy48076x99a5hnhpa4") (r "1.67")))

(define-public crate-wasmer-object-4.0.0-beta.3 (c (n "wasmer-object") (v "4.0.0-beta.3") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.0.0-beta.3") (d #t) (k 0)))) (h "039zdphv4p2040bnhjqknp6d3j5icyk23zl0z24v3r0icn770qhp") (r "1.67")))

(define-public crate-wasmer-object-4.0.0 (c (n "wasmer-object") (v "4.0.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.0.0") (d #t) (k 0)))) (h "0my9wkkjn7s0vabj4f0p3lg8fcdnbn44a46i4a5ajgrigsz5va5x") (r "1.67")))

(define-public crate-wasmer-object-4.1.0 (c (n "wasmer-object") (v "4.1.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.1.0") (d #t) (k 0)))) (h "1ck447c9hxzj9p9zinf6z2yzgg9dc9pkphb2cqpm3f3a22q5s2va") (r "1.67")))

(define-public crate-wasmer-object-4.1.1 (c (n "wasmer-object") (v "4.1.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.1.1") (d #t) (k 0)))) (h "06bn3wgwab1i0mmagqfi9ph69psz36l4rpswnkfqgw6kfzj108l5") (r "1.67")))

(define-public crate-wasmer-object-4.1.2 (c (n "wasmer-object") (v "4.1.2") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.1.2") (d #t) (k 0)))) (h "01bm2iv806mdjx8kfnfz72gi5pnabl0mhjfn5ik2czmw6h9vhyxy") (r "1.67")))

(define-public crate-wasmer-object-4.2.0 (c (n "wasmer-object") (v "4.2.0") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.2.0") (d #t) (k 0)))) (h "0p1d8v4bdlcvrkvzbdamxa2gs1vpzz5i1n7vs71q9836h0ndmpi0") (r "1.67")))

(define-public crate-wasmer-object-4.2.1 (c (n "wasmer-object") (v "4.2.1") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.2.1") (d #t) (k 0)))) (h "0lxsxi1lh3ix051lp9f3cyicbrr3xaahzr83c4wg1wrilwisly0p") (r "1.70")))

(define-public crate-wasmer-object-4.2.2 (c (n "wasmer-object") (v "4.2.2") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.2.2") (d #t) (k 0)))) (h "1d3r1mpk72dlgdjzbh7dcrczng8jnaxqr1i44d7sckb3qfnvxyb4") (r "1.70")))

(define-public crate-wasmer-object-4.2.3 (c (n "wasmer-object") (v "4.2.3") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.2.3") (d #t) (k 0)))) (h "1lh40dhncdh71i53xvi5pbanxmvlic7jdfz4xgc0dby2nrg2dfvs") (r "1.70")))

(define-public crate-wasmer-object-4.2.4 (c (n "wasmer-object") (v "4.2.4") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.2.4") (d #t) (k 0)))) (h "02vclbbh1d0vqn9bfmdzwb7kzjwjfpg2syfcqrk55q82yvfpdp36") (r "1.70")))

(define-public crate-wasmer-object-4.2.5 (c (n "wasmer-object") (v "4.2.5") (d (list (d (n "object") (r "^0.28.3") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.2.5") (d #t) (k 0)))) (h "1g02227qjq4xwplvd5jsp334kxxk63q2gvmycw83raagcf5rgzp7") (r "1.70")))

(define-public crate-wasmer-object-4.2.6 (c (n "wasmer-object") (v "4.2.6") (d (list (d (n "object") (r "^0.29.0") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.2.6") (d #t) (k 0)))) (h "05cbd599iwyk3pqip6w75a7vczmd9p9fb7wf5rjqsasdcdakypn7") (r "1.73")))

(define-public crate-wasmer-object-4.2.7 (c (n "wasmer-object") (v "4.2.7") (d (list (d (n "object") (r "^0.29.0") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.2.7") (d #t) (k 0)))) (h "0r71sy7h827mh8p7psya61b6fjzr6c7xg3prrn956s0j6yaz87kg") (r "1.73")))

(define-public crate-wasmer-object-4.2.8 (c (n "wasmer-object") (v "4.2.8") (d (list (d (n "object") (r "^0.29.0") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.2.8") (d #t) (k 0)))) (h "0m4m7q5w8d5m1piwql4zrzlj5v2di1qpw1fl5pvhf56r1c132vxl") (r "1.73")))

(define-public crate-wasmer-object-4.3.0-alpha.1 (c (n "wasmer-object") (v "4.3.0-alpha.1") (d (list (d (n "object") (r "^0.29.0") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.3.0-alpha.1") (d #t) (k 0)))) (h "1wi8fhh0gvmm6y6zk84p7jycdskcs8dmhv70icgrn9hn1wgvyh4k") (r "1.73")))

(define-public crate-wasmer-object-4.3.0-beta.1 (c (n "wasmer-object") (v "4.3.0-beta.1") (d (list (d (n "object") (r "^0.29.0") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.3.0-beta.1") (d #t) (k 0)))) (h "1ymvdaw12dmb2j20wag7qd8l76wwp3z5yb5p1mg11da18pwpgz7q") (r "1.73")))

(define-public crate-wasmer-object-4.3.0 (c (n "wasmer-object") (v "4.3.0") (d (list (d (n "object") (r "^0.29.0") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.3.0") (d #t) (k 0)))) (h "1yhzcncafxx6jhkf2x9dck5jkixsngqa3khzngky9a00h6hx63yl") (r "1.74")))

(define-public crate-wasmer-object-4.3.1 (c (n "wasmer-object") (v "4.3.1") (d (list (d (n "object") (r "^0.29.0") (f (quote ("write"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=4.3.1") (d #t) (k 0)))) (h "01ci1vnsg0msz2pzymq6vjnipybp5kfsd9dknqdlz0qdjfd2c3sp") (r "1.74")))

