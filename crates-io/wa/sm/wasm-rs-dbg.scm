(define-module (crates-io wa sm wasm-rs-dbg) #:use-module (crates-io))

(define-public crate-wasm-rs-dbg-0.1.0 (c (n "wasm-rs-dbg") (v "0.1.0") (d (list (d (n "web-sys") (r "^0.3.47") (f (quote ("console"))) (d #t) (k 0)))) (h "0nqs056f9s3ckcl9zymg88v9v419k4acaqh4jknnlvp86h77ydb8") (f (quote (("default" "console-debug") ("console-warn") ("console-trace") ("console-log") ("console-info") ("console-error") ("console-debug"))))))

(define-public crate-wasm-rs-dbg-0.1.1 (c (n "wasm-rs-dbg") (v "0.1.1") (d (list (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (k 2)) (d (n "web-sys") (r "^0.3.47") (f (quote ("console"))) (d #t) (k 0)))) (h "1aqdks1wnkh62hskbp9nrj9qvxlr461biif2xlhh9jw435y8p1l5") (f (quote (("default" "console-debug") ("console-warn") ("console-trace") ("console-log") ("console-info") ("console-error") ("console-debug"))))))

(define-public crate-wasm-rs-dbg-0.1.2 (c (n "wasm-rs-dbg") (v "0.1.2") (d (list (d (n "wasm-bindgen-test") (r "^0.2") (d #t) (t "wasm32-unknown-unknown") (k 2)) (d (n "web-sys") (r "^0.3.47") (f (quote ("console"))) (d #t) (t "wasm32-unknown-unknown") (k 0)))) (h "1x33x8kg44k902qdw803wwwxm0aq3bs2j844vgqmrjkqqi5gxrb1") (f (quote (("default" "console-debug") ("console-warn") ("console-trace") ("console-log") ("console-info") ("console-error") ("console-debug"))))))

