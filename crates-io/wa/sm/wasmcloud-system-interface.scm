(define-module (crates-io wa sm wasmcloud-system-interface) #:use-module (crates-io))

(define-public crate-wasmcloud-system-interface-0.1.0 (c (n "wasmcloud-system-interface") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "frodobuf") (r "^0.1") (d #t) (k 0)) (d (n "frodobuf") (r "^0.1") (d #t) (k 1)) (d (n "midl-parser") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "17h01gf0dm2qq61s55hdb7b5l4fwa8m9w96avqk2fymzr2js69m0")))

(define-public crate-wasmcloud-system-interface-0.1.1 (c (n "wasmcloud-system-interface") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "frodobuf") (r "^0.1") (d #t) (k 0)) (d (n "frodobuf") (r "^0.1") (d #t) (k 1)) (d (n "midl-parser") (r "^0.1") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "0k7fgvcax2kq65m0m8nrgzxxmc01v0yyxgk51ff1w5j1x6xjzkpr")))

