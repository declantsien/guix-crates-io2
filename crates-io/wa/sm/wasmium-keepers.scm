(define-module (crates-io wa sm wasmium-keepers) #:use-module (crates-io))

(define-public crate-wasmium-keepers-0.2.0 (c (n "wasmium-keepers") (v "0.2.0") (d (list (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "wasmium-utilities") (r "^0.2.0") (d #t) (k 0)))) (h "1912f64xcpl6xm66shh7iiqngbmq620613lk35lkdnzr3af4vaqa")))

