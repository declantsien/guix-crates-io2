(define-module (crates-io wa sm wasmblock) #:use-module (crates-io))

(define-public crate-wasmblock-0.0.1 (c (n "wasmblock") (v "0.0.1") (h "0zn7di8r50c95bw5ifkmrz8sa1xpl0aq3w5m7dcrwrjxh4gixnk6")))

(define-public crate-wasmblock-0.0.2 (c (n "wasmblock") (v "0.0.2") (h "1jj5h2la6x6b0qhvb3bbyn7gi2rx7cwsq7q0p7lh0rf7gy9rmrkk")))

(define-public crate-wasmblock-0.0.3 (c (n "wasmblock") (v "0.0.3") (h "1kp6ni4k2c2m4vwwvdz6g8ybi7l0k7p3lr2mcgqbbh6ykpbhjhqq")))

(define-public crate-wasmblock-0.0.5 (c (n "wasmblock") (v "0.0.5") (h "1bidx0ikmnrn5p0p325sxf0dflm068is11p7dw5sjc55i7kb4d9b")))

(define-public crate-wasmblock-0.0.6 (c (n "wasmblock") (v "0.0.6") (h "07f1hpi8kqgn9acgwbzh2yjw2pq8qp4vx6n6axaff9c42mc3s04w")))

(define-public crate-wasmblock-0.0.7 (c (n "wasmblock") (v "0.0.7") (h "0676kgdz42cidhif0m87ymc8rfiai02jd0yy61cf90bvhb5ab4n3")))

(define-public crate-wasmblock-0.0.8 (c (n "wasmblock") (v "0.0.8") (h "167x7p3x45jwxdwx4nbn84hvzrqbpvlfv867pgapbqk78iv0whic")))

(define-public crate-wasmblock-0.0.9 (c (n "wasmblock") (v "0.0.9") (h "1z3hsr4b9hdp3y0a7v1bivf1rhj92n3c6vl9djscxvympq9kyz2s")))

(define-public crate-wasmblock-0.0.10 (c (n "wasmblock") (v "0.0.10") (h "08xyr7gdfgmrl7rrdz7myr6kvb3l0q35z2jynkq5i0j51d929s2g")))

(define-public crate-wasmblock-0.0.11 (c (n "wasmblock") (v "0.0.11") (h "067bnv94r51b3ixmdwww3lsszjdasa1mh1s6zyj1jyzg047wh0zr")))

(define-public crate-wasmblock-0.0.12 (c (n "wasmblock") (v "0.0.12") (h "0rd1z68zrki9fadpknf75zbaw32adfqi0w7y7xkkhayswql3znbq")))

(define-public crate-wasmblock-0.0.13 (c (n "wasmblock") (v "0.0.13") (h "00n6dkn27lx0x67fgz0w4cklyn2zd2qaqka63bx99mhahyr16lha")))

(define-public crate-wasmblock-0.0.14 (c (n "wasmblock") (v "0.0.14") (h "0vjcq8jvbcvy8sjif4l67aqg6jx5zv73qyf2fsdm7ygj0h8y21bk")))

(define-public crate-wasmblock-0.0.15 (c (n "wasmblock") (v "0.0.15") (h "1gyvsnl1mxp58nnkr74892mznnkf1dd70cdiprvnqsn38p39v837")))

