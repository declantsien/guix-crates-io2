(define-module (crates-io wa sm wasmer-wit-bindgen-gen-rust) #:use-module (crates-io))

(define-public crate-wasmer-wit-bindgen-gen-rust-0.1.0 (c (n "wasmer-wit-bindgen-gen-rust") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "wasmer-wit-bindgen-gen-core") (r "^0.1.0") (d #t) (k 0)))) (h "1vi3cdyvaabswpnry62n3q0d9yzw5yhr3a8qi50h6fw7hxixmbb3")))

(define-public crate-wasmer-wit-bindgen-gen-rust-0.1.1 (c (n "wasmer-wit-bindgen-gen-rust") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "wasmer-wit-bindgen-gen-core") (r "^0.1.1") (d #t) (k 0)))) (h "0frwkhhn1r0lq6wb12p581jgrdd98cj336yc03qjp1498mycx2s3")))

