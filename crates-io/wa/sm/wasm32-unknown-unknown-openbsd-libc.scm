(define-module (crates-io wa sm wasm32-unknown-unknown-openbsd-libc) #:use-module (crates-io))

(define-public crate-wasm32-unknown-unknown-openbsd-libc-0.1.0 (c (n "wasm32-unknown-unknown-openbsd-libc") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1879i9a2hgdn3b200n8pbz7csglp1yi83brsr1s9zjpsbv9xiwka")))

(define-public crate-wasm32-unknown-unknown-openbsd-libc-0.2.0 (c (n "wasm32-unknown-unknown-openbsd-libc") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "131sr9pn0vilvm2ip50v3b1csbiwyvw7ari7jjbi0paghi4fxazn") (l "wasm32-unknown-unknown-openbsd-libc")))

