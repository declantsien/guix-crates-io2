(define-module (crates-io wa sm wasmer-inline-c-macro) #:use-module (crates-io))

(define-public crate-wasmer-inline-c-macro-0.1.6 (c (n "wasmer-inline-c-macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "1l9nrl3c5lz1n6kgnhwj2bh1s6xd95pnsd3fj18nr0qn87gq3185")))

