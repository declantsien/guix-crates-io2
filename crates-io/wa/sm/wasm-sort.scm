(define-module (crates-io wa sm wasm-sort) #:use-module (crates-io))

(define-public crate-wasm-sort-0.1.0 (c (n "wasm-sort") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "167qfar4q9frp066zqylxqv674lkfnpcjlqvwp9mza1bxmg3lvwl")))

(define-public crate-wasm-sort-0.1.1 (c (n "wasm-sort") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "01q5hmbxx7gcah2zlrkr5fa8drygfwb517d80bhrv2sdzvsipz8x")))

(define-public crate-wasm-sort-0.2.0 (c (n "wasm-sort") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0mnhgsygv7p8373l9ras00fx0a8vr5j4jc8cac02vca1kh2b5nhk")))

(define-public crate-wasm-sort-0.2.1 (c (n "wasm-sort") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1xzcx5zl9ik8v137r0v5h8qbj1lijyh8wbqwfa1l73zkd00jsnvn")))

(define-public crate-wasm-sort-0.2.2 (c (n "wasm-sort") (v "0.2.2") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0nk4qac77gjbknnljl3g28lzx63z9wi9pxf7d5bfq2vkmdx08p06")))

(define-public crate-wasm-sort-0.3.0 (c (n "wasm-sort") (v "0.3.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0npqbypjijqz3waqmih8schxadnj280ay85qy2wpw46jli16f166")))

(define-public crate-wasm-sort-0.4.0 (c (n "wasm-sort") (v "0.4.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "02rsl49yn2kg2hc8vxfsl70aiz5n4x168clmc5zwrgfs18jf33qn")))

(define-public crate-wasm-sort-0.4.1 (c (n "wasm-sort") (v "0.4.1") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1w6vkpl7zg04q1p6is0zm86g80d0gy1dnp7wa7xvsr8h5dwj1g8p")))

(define-public crate-wasm-sort-0.5.0 (c (n "wasm-sort") (v "0.5.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "10sh04s4sc0b6jy45c3yi72km6p0jigpsmij2qjbbvkinfdms52n")))

(define-public crate-wasm-sort-0.6.0 (c (n "wasm-sort") (v "0.6.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "sort") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "039dcl6vxppc231bd2jgfswlhssb2aw13qyv595nwbp7gg3v3713")))

(define-public crate-wasm-sort-0.7.0 (c (n "wasm-sort") (v "0.7.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "sort") (r "^0.5.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1lb1aan8k730b27dhlnbhqkg4mhiwr1r8sqszqj6hmk8d6csvq3y")))

(define-public crate-wasm-sort-0.8.0 (c (n "wasm-sort") (v "0.8.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "sort") (r "^0.6.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "1980xhfv583nvrs6dhdb1n015p363lxha0hhaz137kw5h4cq9yyy")))

(define-public crate-wasm-sort-0.9.0 (c (n "wasm-sort") (v "0.9.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "sort") (r "^0.7") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "17lf790jzgh8wzbxq21k6n47264m1diappi50zsdg4i99h8zjj93")))

(define-public crate-wasm-sort-0.10.0 (c (n "wasm-sort") (v "0.10.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "sort") (r "^0.8.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4") (d #t) (k 0)))) (h "0g6flray8ln1gsd6wmnhv85ncf843gn202wwzrh799fy3lpnyb4q")))

