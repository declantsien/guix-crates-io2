(define-module (crates-io wa sm wasm-deploy-derive) #:use-module (crates-io))

(define-public crate-wasm-deploy-derive-0.3.0 (c (n "wasm-deploy-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "16yznvnga8vy2amcnrjshaz19vdnb4wn1gi2irjx079w5zpp9wcd")))

(define-public crate-wasm-deploy-derive-0.4.0 (c (n "wasm-deploy-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1hwk90j5xh6x3bs6kw6hswgzr11dz1nalm2m7mgd8siwz0vd14p1")))

(define-public crate-wasm-deploy-derive-0.5.0-alpha (c (n "wasm-deploy-derive") (v "0.5.0-alpha") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "063hf0dsb8l83kkfs5jjf1jr362rp905p7kdif1z4yvi1vsbf52q")))

(define-public crate-wasm-deploy-derive-0.5.0-alpha.1 (c (n "wasm-deploy-derive") (v "0.5.0-alpha.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "06vaf36p8lcikr4bclpp0c7ghfkyhvhsw2nvfgh9pjym79jms281")))

(define-public crate-wasm-deploy-derive-0.5.0 (c (n "wasm-deploy-derive") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "07a7z9vrr0dgixrzx770p6n83bzaq6mgdr2cb14cgarv55czzyps")))

