(define-module (crates-io wa sm wasm-wrapper-gen-impl) #:use-module (crates-io))

(define-public crate-wasm-wrapper-gen-impl-0.0.1 (c (n "wasm-wrapper-gen-impl") (v "0.0.1") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasm-wrapper-gen-shared") (r "^0.0.1") (d #t) (k 0)))) (h "16ggm34f4h4qjg7hic4cbd6bx892rgmmi8chrdwndkhacklq2ign")))

(define-public crate-wasm-wrapper-gen-impl-0.0.2 (c (n "wasm-wrapper-gen-impl") (v "0.0.2") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasm-wrapper-gen-shared") (r "^0.0.2") (d #t) (k 0)))) (h "195b16d5sjkb51sfw8hbz093si6l4wmmwpz30gslxgm2236sj43i")))

(define-public crate-wasm-wrapper-gen-impl-0.0.3 (c (n "wasm-wrapper-gen-impl") (v "0.0.3") (d (list (d (n "arrayvec") (r "^0.4") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)) (d (n "wasm-wrapper-gen-shared") (r "^0.0.3") (d #t) (k 0)))) (h "184d7qrsr3kf4c86pb7p6d5wgkh2z9dwmnlpampnr701lwqhlc15")))

