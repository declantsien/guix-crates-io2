(define-module (crates-io wa sm wasmprinter) #:use-module (crates-io))

(define-public crate-wasmprinter-0.1.0 (c (n "wasmprinter") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.42.1") (d #t) (k 0)) (d (n "wast") (r "^3.0.3") (d #t) (k 2)) (d (n "wat") (r "^1.0.4") (d #t) (k 2)))) (h "0i5d4dxqajd22vlxk9dy6905xr3x2qfdi2qvhaj90fqsp1m60prj")))

(define-public crate-wasmprinter-0.2.0 (c (n "wasmprinter") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.42.1") (d #t) (k 0)) (d (n "wast") (r "^3.0.3") (d #t) (k 2)) (d (n "wat") (r "^1.0.4") (d #t) (k 2)))) (h "0v60aqaw5jg5d42qiqwxxl9nq3f0d0jzy4h5rj3lsgshj0c1s449")))

(define-public crate-wasmprinter-0.2.1 (c (n "wasmprinter") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.48.1") (d #t) (k 0)))) (h "1wsg093gysy0ngshwcjwil151dxgwdmgv2graj6j19hcaqm6p2m4")))

(define-public crate-wasmprinter-0.2.2 (c (n "wasmprinter") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.50.0") (d #t) (k 0)) (d (n "wast") (r "^7.0.0") (d #t) (k 2)) (d (n "wat") (r "^1.0.8") (d #t) (k 2)))) (h "1h1207ivr7zmk9dnzg8zi426pmgbv9cf92lqgdhwnfx2wc9p69qk")))

(define-public crate-wasmprinter-0.2.3 (c (n "wasmprinter") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.51.0") (d #t) (k 0)) (d (n "wast") (r "^7.0.0") (d #t) (k 2)) (d (n "wat") (r "^1.0.8") (d #t) (k 2)))) (h "0ns7dhx64gd6w0wb7h93v9dc8qvcwvfbywjlfw8yxz4mbga27m4b")))

(define-public crate-wasmprinter-0.2.4 (c (n "wasmprinter") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.52.0") (d #t) (k 0)) (d (n "wast") (r "^15.0.0") (d #t) (k 2)) (d (n "wat") (r "^1.0.8") (d #t) (k 2)))) (h "0kq43y6q5g6z7n36fcq168rjx7amjkqy81z03hgbz0pqn4barhr1")))

(define-public crate-wasmprinter-0.2.5 (c (n "wasmprinter") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.55.0") (d #t) (k 0)) (d (n "wast") (r "^17.0.0") (d #t) (k 2)) (d (n "wat") (r "^1.0.8") (d #t) (k 2)))) (h "0m04clvp2d4lk7hp05fz221pqmvnsarlfsxn1ncfxi8y208a6fy9")))

(define-public crate-wasmprinter-0.2.6 (c (n "wasmprinter") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.59") (d #t) (k 0)))) (h "04pi13qrc5mfpdzfclcvdslk5b21npllr9b6ydnf26qbigmm2i9k")))

(define-public crate-wasmprinter-0.2.7 (c (n "wasmprinter") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.60") (d #t) (k 0)))) (h "0xiqqjjgrivw0x3d1q0vm8jd6rxkpiij1cki7ps0vl2xm3dkdj7c")))

(define-public crate-wasmprinter-0.2.8 (c (n "wasmprinter") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.61") (d #t) (k 0)))) (h "1jyrn5sb2a4nxpn3gbdxxh1bzl666dp3w1vj0r313c95cxk5vwby")))

(define-public crate-wasmprinter-0.2.9 (c (n "wasmprinter") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.62") (d #t) (k 0)))) (h "154sh68k9vcdny7lalwg8xwimdbsjg0dc2f82rgw3qa5f47y3jdd")))

(define-public crate-wasmprinter-0.2.10 (c (n "wasmprinter") (v "0.2.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.63") (d #t) (k 0)))) (h "0rljvm3zizzarkcbp8pxyrcd1s3kkywqslxn7pips47vhqpfl7zx")))

(define-public crate-wasmprinter-0.2.11 (c (n "wasmprinter") (v "0.2.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.64") (d #t) (k 0)))) (h "18r0rc5n8j80n9anikxbwy1x8hy190hl9mng630m72h7k4fvwlfj")))

(define-public crate-wasmprinter-0.2.12 (c (n "wasmprinter") (v "0.2.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.65") (d #t) (k 0)))) (h "1lwkkym6j7hp46f7lcdigmf91lj9s1bjpjnanwycfz9hmmwpxs0g")))

(define-public crate-wasmprinter-0.2.13 (c (n "wasmprinter") (v "0.2.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.66") (d #t) (k 0)))) (h "0hjj7xz78ygqsydinw6h4bx2msl23yrj0ih1qvx2kw5ikvp0nn4v")))

(define-public crate-wasmprinter-0.2.14 (c (n "wasmprinter") (v "0.2.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.67") (d #t) (k 0)))) (h "1mg6g8f8p3yh5mhhapirisv46kc1rv981875jxil76qgkagqr0p8")))

(define-public crate-wasmprinter-0.2.15 (c (n "wasmprinter") (v "0.2.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.68") (d #t) (k 0)))) (h "19mwi03kppqs17y0q129kgndvr0rkg9nh6dnd2sv3z4wy2sp76pk")))

(define-public crate-wasmprinter-0.2.16 (c (n "wasmprinter") (v "0.2.16") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.69") (d #t) (k 0)))) (h "1xj7jl7kk800baivc8mhmj1761pblqg8cpmlqfjs4hdzqpshd86v")))

(define-public crate-wasmprinter-0.2.17 (c (n "wasmprinter") (v "0.2.17") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.70") (d #t) (k 0)))) (h "0670sgawnjy8i98a4vjwvvy6fw6prcqq7hh9kkz2fzp1vhj2p6zq")))

(define-public crate-wasmprinter-0.2.18 (c (n "wasmprinter") (v "0.2.18") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.71") (d #t) (k 0)))) (h "17vj1r40krkwn4a6j8miqsx03syivxp9fdpcacy7y0qhqrkxn585")))

(define-public crate-wasmprinter-0.2.19 (c (n "wasmprinter") (v "0.2.19") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.72") (d #t) (k 0)))) (h "1idcxw9dm6fh04p2j5g3ff8fldm2qwh3gamayncvi01vdrc3khd0")))

(define-public crate-wasmprinter-0.2.20 (c (n "wasmprinter") (v "0.2.20") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.73") (d #t) (k 0)))) (h "03djg08c7k2k1nnnr4fq2i9v87jhc7vhpinjgshlghqcwq9snry4")))

(define-public crate-wasmprinter-0.2.21 (c (n "wasmprinter") (v "0.2.21") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.73") (d #t) (k 0)))) (h "0biry98j14dy4rhlsiql5impl1bh3jf8gdla9z5aaj9v5f7ppxpd")))

(define-public crate-wasmprinter-0.2.22 (c (n "wasmprinter") (v "0.2.22") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.74") (d #t) (k 0)))) (h "1xl76jlgnf3yhmxc4wwfj11g6mq10z4kspiwqy17waibd9cfr7jy")))

(define-public crate-wasmprinter-0.2.23 (c (n "wasmprinter") (v "0.2.23") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.75") (d #t) (k 0)))) (h "0qb9sdxndwwnbi5y2livn15flah6pi4l1v4jc16pg9hz6hjg35px")))

(define-public crate-wasmprinter-0.2.24 (c (n "wasmprinter") (v "0.2.24") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.76") (d #t) (k 0)))) (h "11jn3qs53mzx45gbkqiyqj7n5jw7549wkgfihh4q4643p52yj17w")))

(define-public crate-wasmprinter-0.2.25 (c (n "wasmprinter") (v "0.2.25") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.77") (d #t) (k 0)))) (h "0dhqjafv968wa8zmnsz6vm3kv774c9pywlwrg1cmp1k0rcg4m9nh")))

(define-public crate-wasmprinter-0.2.26 (c (n "wasmprinter") (v "0.2.26") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.78") (d #t) (k 0)))) (h "11gd2rlyg31gci2rfhmhmnbzp71brcraa84kcvjc4407qyacikic")))

(define-public crate-wasmprinter-0.2.27 (c (n "wasmprinter") (v "0.2.27") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.79") (d #t) (k 0)))) (h "0vwkp797jjzf4jvhvrxpn9yk6w834glyp2wvpa3klrcv1h06avzy")))

(define-public crate-wasmprinter-0.2.28 (c (n "wasmprinter") (v "0.2.28") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.80") (d #t) (k 0)))) (h "0lh4djbn1zxfj2z39rd6iq6pwdnjsxbq6cky0fdhx4xrk4llz3ml")))

(define-public crate-wasmprinter-0.2.29 (c (n "wasmprinter") (v "0.2.29") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.80") (d #t) (k 0)))) (h "0xm1zw5swnnv9zkqw7r2dfdwrvmj6mc5agfbkdwxnd6w1s0ihbzn")))

(define-public crate-wasmprinter-0.2.30 (c (n "wasmprinter") (v "0.2.30") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.80") (d #t) (k 0)))) (h "1rvi7vnidj12m9rpp1rzsmcrd825mwsn1p6ddawzfs2alz8ar0xz")))

(define-public crate-wasmprinter-0.2.31 (c (n "wasmprinter") (v "0.2.31") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.81") (d #t) (k 0)))) (h "08xjv72rm9ib9ijpnka5nwfzgfahx9yv6skpgh9q6hd73fjx82m0")))

(define-public crate-wasmprinter-0.2.32 (c (n "wasmprinter") (v "0.2.32") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.82.0") (d #t) (k 0)))) (h "13asy5744dxyx8fkfkhdsamapjk8ylhvq6450czkch1mbsfiwzkd")))

(define-public crate-wasmprinter-0.2.33 (c (n "wasmprinter") (v "0.2.33") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.83.0") (d #t) (k 0)))) (h "0n7bqhpkmpz4389wnfnmlccxy1dl2i2jf40r8amh6znancpq4wzr")))

(define-public crate-wasmprinter-0.2.34 (c (n "wasmprinter") (v "0.2.34") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.84.0") (d #t) (k 0)))) (h "0rfq268csqdv5ljd9bbw7vav4d8pyssl040mr973l637cp4v6wlv")))

(define-public crate-wasmprinter-0.2.35 (c (n "wasmprinter") (v "0.2.35") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.85.0") (d #t) (k 0)))) (h "0ccw6rwmpwfm716pnvzqbiq4ykh2frma48vp7w2qv257l8s4ciga")))

(define-public crate-wasmprinter-0.2.36 (c (n "wasmprinter") (v "0.2.36") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.86.0") (d #t) (k 0)))) (h "0zk90r98vlw5l4lczlhrhzcvh10z77rvg6kwvld7gnkqa90wlk5a")))

(define-public crate-wasmprinter-0.2.37 (c (n "wasmprinter") (v "0.2.37") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.87.0") (d #t) (k 0)))) (h "11yv2v2wg4izx2zbh5ab5801wqk0mfi7q8lz9ica2spcb8fxw2sm")))

(define-public crate-wasmprinter-0.2.38 (c (n "wasmprinter") (v "0.2.38") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.88.0") (d #t) (k 0)))) (h "08acm9s05n9w4lhqvcglyn06hvcsayvjh7ikzbfi2lm235ppiwh4")))

(define-public crate-wasmprinter-0.2.39 (c (n "wasmprinter") (v "0.2.39") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.89.0") (d #t) (k 0)))) (h "1ijqp02mn6yg7pkfysnr2s25m3hiii712n4m93dabj3cypi5x7ma")))

(define-public crate-wasmprinter-0.2.40 (c (n "wasmprinter") (v "0.2.40") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.91.0") (d #t) (k 0)))) (h "1j4qwn0jqq3s91h424n7p8nfbsq89h19qwcrcb7bxi3kyqf97dc5")))

(define-public crate-wasmprinter-0.2.41 (c (n "wasmprinter") (v "0.2.41") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "rayon") (r "^1.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.92.0") (d #t) (k 0)))) (h "02xirk48g07abnl6y0qwy6bhx06m88d6iqd0baw14jhr4zn78hya")))

(define-public crate-wasmprinter-0.2.42 (c (n "wasmprinter") (v "0.2.42") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.93.0") (d #t) (k 0)))) (h "01cgr86vhv5js1gl3hkzmglm0nvclaprqzjvlmm9qclml1mhk7vc")))

(define-public crate-wasmprinter-0.2.43 (c (n "wasmprinter") (v "0.2.43") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.94.0") (d #t) (k 0)))) (h "02fj43w295jc74pkgag8jz4lq0cdgazbjcihv5cwq9k5kvdks2cw")))

(define-public crate-wasmprinter-0.2.44 (c (n "wasmprinter") (v "0.2.44") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.95.0") (d #t) (k 0)))) (h "1sjf41xgw29a80gcig90z5yb221wylznjrlf6cmlnyn2kh7m095f")))

(define-public crate-wasmprinter-0.2.45 (c (n "wasmprinter") (v "0.2.45") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.96.0") (d #t) (k 0)))) (h "0h2h6dnx3k65p6ghkna7zyp4gac4y2wmvqm1ji7pz15c5jmf2i9h")))

(define-public crate-wasmprinter-0.2.46 (c (n "wasmprinter") (v "0.2.46") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.97.0") (d #t) (k 0)))) (h "0hkvl3src8chjymq536xv7k1f6zm6ghsida1r71vx9s7ks9clp2r")))

(define-public crate-wasmprinter-0.2.47 (c (n "wasmprinter") (v "0.2.47") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.98.0") (d #t) (k 0)))) (h "1apmkmqal9w288jpcj40yj089sw9qw77wrf2jpzqzh2j2sdw3pkb") (y #t)))

(define-public crate-wasmprinter-0.2.48 (c (n "wasmprinter") (v "0.2.48") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.98.1") (d #t) (k 0)))) (h "11ds2mm7mh134hgc0jhr8ql1icr446zl9q9h8fnlnpndhbrlja9j")))

(define-public crate-wasmprinter-0.2.49 (c (n "wasmprinter") (v "0.2.49") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.99.0") (d #t) (k 0)))) (h "188fwjcwmg43kdf671arch9jar65v59731vakbx594qzj3zkvh97")))

(define-public crate-wasmprinter-0.2.50 (c (n "wasmprinter") (v "0.2.50") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.100.0") (d #t) (k 0)))) (h "1sy4r2ldp0ssr8zv7dmkmw9sjs6wdj2z48dvj6gsw00yrgbzglxj")))

(define-public crate-wasmprinter-0.2.51 (c (n "wasmprinter") (v "0.2.51") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.101.0") (d #t) (k 0)))) (h "1v3xmm8il3s0vcsrixkh1qyh5sspz6k6hz4yd2nvqm30h6vs1zmb")))

(define-public crate-wasmprinter-0.2.52 (c (n "wasmprinter") (v "0.2.52") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.101.1") (d #t) (k 0)))) (h "0iznrl88xwqc1f4276i484agafcirmg1i3iqknaarjmpp4vjwgq0")))

(define-public crate-wasmprinter-0.2.53 (c (n "wasmprinter") (v "0.2.53") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.102.0") (d #t) (k 0)))) (h "13qip64sf1hbpw2ss4r4ybxwy18agi4zld8hx7r7ji2fdra4v92s")))

(define-public crate-wasmprinter-0.2.54 (c (n "wasmprinter") (v "0.2.54") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.102.0") (d #t) (k 0)))) (h "18qwpszpif0a7sv7y4bibxga1ffvzsjjc700pyi11l1n73k7mh9d")))

(define-public crate-wasmprinter-0.2.55 (c (n "wasmprinter") (v "0.2.55") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.103.0") (d #t) (k 0)))) (h "1x6cg7ya7zzrl271l55amg6l7lib5wnz8xggl8awdypfsylzvgji")))

(define-public crate-wasmprinter-0.2.56 (c (n "wasmprinter") (v "0.2.56") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.104.0") (d #t) (k 0)))) (h "12mybffyj6kq03z7fxmy2fz8cqgp6mw4a2gbdxfwsdslbm8a47bk")))

(define-public crate-wasmprinter-0.2.57 (c (n "wasmprinter") (v "0.2.57") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.105.0") (d #t) (k 0)))) (h "1n505w900lmgh864pll0943dpji9bz79hxqdgxinb83lgbnybc2h")))

(define-public crate-wasmprinter-0.2.58 (c (n "wasmprinter") (v "0.2.58") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.106.0") (d #t) (k 0)))) (h "0haqb6xnxhrwsvdqh9pbwjrmjicyd3cssqzhykg9m6dml2ch4liw")))

(define-public crate-wasmprinter-0.2.59 (c (n "wasmprinter") (v "0.2.59") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.107.0") (d #t) (k 0)))) (h "1v9z7ays0zc2h7arwxgnq2w4vix1yg7n4g4gfrvs7g2ap0q0p5nc")))

(define-public crate-wasmprinter-0.2.60 (c (n "wasmprinter") (v "0.2.60") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.108.0") (d #t) (k 0)))) (h "14rrl8adxach4ynwqn0mrkshys3y4ir407zfikjhv6rxzq4vjv5p")))

(define-public crate-wasmprinter-0.2.61 (c (n "wasmprinter") (v "0.2.61") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.109.0") (d #t) (k 0)))) (h "0jhsrcr47sj0d01yn80brhc85da9iwjcqcskin0nf48ni93dq1nz")))

(define-public crate-wasmprinter-0.2.62 (c (n "wasmprinter") (v "0.2.62") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.110.0") (d #t) (k 0)))) (h "0zwyzw8j2r0rngxilin71ij02v8jy5bp98cqnpj89acn9pni5ka2")))

(define-public crate-wasmprinter-0.2.63 (c (n "wasmprinter") (v "0.2.63") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.111.0") (d #t) (k 0)))) (h "19h7zrrqykhl9g4l8q0gjkfmmlvcwd82p47f1379v4s1sd0wrf7f")))

(define-public crate-wasmprinter-0.2.64 (c (n "wasmprinter") (v "0.2.64") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.112.0") (d #t) (k 0)))) (h "0bvwbr5p98yfzmb7c2cnv07c3jx0jh8zypjh1mw4pk9n424zbp9l")))

(define-public crate-wasmprinter-0.2.65 (c (n "wasmprinter") (v "0.2.65") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.113.0") (d #t) (k 0)))) (h "025s82pl3ay5dqzrb2zbfsx855vn86vz4sq7cvqg9aaskqjwaisw")))

(define-public crate-wasmprinter-0.2.66 (c (n "wasmprinter") (v "0.2.66") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.113.1") (d #t) (k 0)))) (h "1ip3x3kbq13hz6d11wxsg0lpg3qap9fg2pcs3qqybkl8iy0mwbmb")))

(define-public crate-wasmprinter-0.2.67 (c (n "wasmprinter") (v "0.2.67") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.113.2") (d #t) (k 0)))) (h "13v4m7nspz4shxrdm8m7jy2whh9z7kx90gxryhxpb7hlhxamlqgn")))

(define-public crate-wasmprinter-0.2.68 (c (n "wasmprinter") (v "0.2.68") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.113.3") (d #t) (k 0)))) (h "1hsch9nxak0smgwflindvk4166bwqz5f47z9jrc9hvp7iiqk0w2k")))

(define-public crate-wasmprinter-0.2.69 (c (n "wasmprinter") (v "0.2.69") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.114.0") (d #t) (k 0)))) (h "1n7kyar4f5liliwa593hc24a68wsxk2v9rd5mhxq4rzlgwgi1994")))

(define-public crate-wasmprinter-0.2.70 (c (n "wasmprinter") (v "0.2.70") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.115.0") (d #t) (k 0)))) (h "0plvamyz31a94vrls6dg7cgvynldr16zx85zi88cgjawpjlmhi77")))

(define-public crate-wasmprinter-0.2.71 (c (n "wasmprinter") (v "0.2.71") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.116.0") (d #t) (k 0)))) (h "0zkc3dwizwji8qx3drvji33db648qwraq7zcphc9b4qgl852d64g")))

(define-public crate-wasmprinter-0.2.72 (c (n "wasmprinter") (v "0.2.72") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.116.1") (d #t) (k 0)))) (h "0m1vnrbhky073z4ambbhilznvllgzk1phzg98306x47irpq4vzws")))

(define-public crate-wasmprinter-0.2.73 (c (n "wasmprinter") (v "0.2.73") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.117.0") (d #t) (k 0)))) (h "0jswdkchz9na5fq1lizx8qxz415l905a2y9zij31qihbf4sdnkvs")))

(define-public crate-wasmprinter-0.2.74 (c (n "wasmprinter") (v "0.2.74") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.118.0") (d #t) (k 0)))) (h "05312ak63fcrh9rnsdca1rp9wbz06kf0pw2xdb02avb3wr3a19v1")))

(define-public crate-wasmprinter-0.2.75 (c (n "wasmprinter") (v "0.2.75") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.118.1") (d #t) (k 0)))) (h "132nrv26ax8ya4hyw186p6b1w9xhwvmhr1y0b9qzq12956w7w0ix")))

(define-public crate-wasmprinter-0.2.76 (c (n "wasmprinter") (v "0.2.76") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.119.0") (d #t) (k 0)))) (h "1lm81s8gn0zghhvk2xh5fic618pm01qy2rg3fdalw1vjadsaghna")))

(define-public crate-wasmprinter-0.2.77 (c (n "wasmprinter") (v "0.2.77") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.120.0") (d #t) (k 0)))) (h "1jqdw9ghcqrxziihwrdyv4rkmhhcjs46jyjkl3z6ac8bxfarlf6q")))

(define-public crate-wasmprinter-0.2.78 (c (n "wasmprinter") (v "0.2.78") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.121.0") (d #t) (k 0)))) (h "0yjpmmhmdf6cj10pb2x80iljndhxxd91v2lryv9n9p4zql9jrqq5")))

(define-public crate-wasmprinter-0.2.79 (c (n "wasmprinter") (v "0.2.79") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.121.1") (d #t) (k 0)))) (h "1w2wai693z9dxv4l4mhfd6fcfslv78pjp49x1cznbrpj5296m9xl")))

(define-public crate-wasmprinter-0.2.80 (c (n "wasmprinter") (v "0.2.80") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.121.2") (d #t) (k 0)))) (h "1dw8559vdz6g8864rg4ggi88cm0kf7mygyavgkdzxzdpls33krv0")))

(define-public crate-wasmprinter-0.200.0 (c (n "wasmprinter") (v "0.200.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.200.0") (d #t) (k 0)))) (h "1aq67q7qxzmprp8mf43bkcmjpdnj1cdx8fybbib2grz4vjq57l9b")))

(define-public crate-wasmprinter-0.201.0 (c (n "wasmprinter") (v "0.201.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.201.0") (d #t) (k 0)))) (h "0rf2q8lggfwpkrwy8qhsrfzzd1ch0z0f6y4sf84bl1i7f3d6czm6")))

(define-public crate-wasmprinter-0.202.0 (c (n "wasmprinter") (v "0.202.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.202.0") (d #t) (k 0)))) (h "0ynswb0wmls5fq2fr6p2l58mcpvl4d0lszvq5r8gkvl5hr8cj75b")))

(define-public crate-wasmprinter-0.203.0 (c (n "wasmprinter") (v "0.203.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.203.0") (d #t) (k 0)))) (h "1gc7315shcwm428dzh7b9pk2iafnl8jivyj0bkd2wrwz1s93w53l")))

(define-public crate-wasmprinter-0.204.0 (c (n "wasmprinter") (v "0.204.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.204.0") (d #t) (k 0)))) (h "0a1ikirv10cv0f1r9w6r60fzz7n52zi9asm1nsfganw524qnlgnf")))

(define-public crate-wasmprinter-0.205.0 (c (n "wasmprinter") (v "0.205.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.205.0") (d #t) (k 0)))) (h "1a3xla939kx5xka09iy1sp5pg4x8yj33v4dy18f4p39lph01xkgd")))

(define-public crate-wasmprinter-0.206.0 (c (n "wasmprinter") (v "0.206.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.206.0") (f (quote ("std"))) (d #t) (k 0)))) (h "1c7k44dpb9afzqyl9ycqbvw167dfk7a5q190k4n7jyf4kiplbv1x")))

(define-public crate-wasmprinter-0.207.0 (c (n "wasmprinter") (v "0.207.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.207.0") (f (quote ("std" "std"))) (k 0)))) (h "05cic6n0fgcwwqljb2q30m2yhrjpvgck8hvbiqh61d5b9mxqlbcw")))

(define-public crate-wasmprinter-0.208.0 (c (n "wasmprinter") (v "0.208.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.208.0") (f (quote ("std" "std"))) (k 0)))) (h "0xb92vz9yhxq4byi2kw03w7kv8w6ifc0r95m8l75q6b06b2v2fjf") (r "1.76.0")))

(define-public crate-wasmprinter-0.208.1 (c (n "wasmprinter") (v "0.208.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.208.1") (f (quote ("std" "std"))) (k 0)))) (h "0br0dy5hymqnkhhi7fv4yinr96crmq08b4qp8rlnq7l2wjnbs007") (r "1.76.0")))

(define-public crate-wasmprinter-0.209.0 (c (n "wasmprinter") (v "0.209.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.209.0") (f (quote ("std" "std"))) (k 0)))) (h "0fp5c755ng2941ah8fcpb52f2llf3qsmbfam0rqd0cs7r53f9gw9") (r "1.76.0")))

(define-public crate-wasmprinter-0.209.1 (c (n "wasmprinter") (v "0.209.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "diff") (r "^0.1") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)) (d (n "tempfile") (r "^3.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.209.1") (f (quote ("std" "std"))) (k 0)))) (h "0nb87bpm4xpz0aa1gqdfcjkw5y4ryaym6p626dxwiix8xbk8mjnf") (r "1.76.0")))

