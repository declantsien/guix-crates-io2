(define-module (crates-io wa sm wasmi_arena) #:use-module (crates-io))

(define-public crate-wasmi_arena-0.1.0 (c (n "wasmi_arena") (v "0.1.0") (h "02pf1pscrpnlhz28qg9q8dzswsh3dl1lfyxzy2lz6h8bpff3gsm1") (f (quote (("std") ("default" "std"))))))

(define-public crate-wasmi_arena-0.2.0 (c (n "wasmi_arena") (v "0.2.0") (h "0n2g84x8sjqi3v3zrhkwn0wwd7a0fc4z2xf6qyczz8qbx1s84hkr") (f (quote (("std") ("default" "std"))))))

(define-public crate-wasmi_arena-0.3.0 (c (n "wasmi_arena") (v "0.3.0") (h "1z0fimcpzgsqysq6my6gvv1fpl47nh6q97111f5mr3dvkp082mfg") (f (quote (("std") ("default" "std"))))))

(define-public crate-wasmi_arena-0.4.0 (c (n "wasmi_arena") (v "0.4.0") (h "0s6l4l2q7rwavf2a6ddbfy4ngv6ri5ap8hs8dn2c3yhkwhsiy720") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-wasmi_arena-0.4.1 (c (n "wasmi_arena") (v "0.4.1") (h "0wvhfah2ccvhl4vwycbncfcnb78ndgbkac3v56n0qms4prrpyjhh") (f (quote (("std") ("default" "std"))))))

(define-public crate-wasmi_arena-0.5.0 (c (n "wasmi_arena") (v "0.5.0") (h "0svcdnl4qf3h29dcffnwpdc3328yxiw5n00yk8x5faprv1v6hs0g") (f (quote (("std") ("default" "std"))))))

