(define-module (crates-io wa sm wasm-logger) #:use-module (crates-io))

(define-public crate-wasm-logger-0.1.0 (c (n "wasm-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "0vyrqqa0h5aldy553fhyrkh855lpgcn379lsfxw5gpk6jjsgbyag")))

(define-public crate-wasm-logger-0.1.1 (c (n "wasm-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "16ss9njgxpv8fzzpfzjm8b9lp0d4anv4z7f2a4kmzn8gg1pxv5dc")))

(define-public crate-wasm-logger-0.1.2 (c (n "wasm-logger") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "13kw0w0yg5jcbhddayi9a1k36d6s1l3pz4w6cl4588n90300rgmi")))

(define-public crate-wasm-logger-0.1.3 (c (n "wasm-logger") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "0y26kkgn4jvykmsgy73r69gyka8m3xrm4binn2hpd5g5dfi2yg3j")))

(define-public crate-wasm-logger-0.1.4 (c (n "wasm-logger") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "0d9qn78dkdcsnv81gd7b7wwdgkabv3mhlll90fgsx5z4wnwqgdgb")))

(define-public crate-wasm-logger-0.1.5 (c (n "wasm-logger") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "0anarcsf954br3h5m3vyxgi7nbiq75s5vngka8akd9i5qlgr13qw")))

(define-public crate-wasm-logger-0.2.0 (c (n "wasm-logger") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "064pn4x4rhpkmjavzz5gs848wpd6bwwid44c0vrch1mkdfk4jih7")))

