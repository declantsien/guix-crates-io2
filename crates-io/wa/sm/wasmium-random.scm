(define-module (crates-io wa sm wasmium-random) #:use-module (crates-io))

(define-public crate-wasmium-random-1.0.0 (c (n "wasmium-random") (v "1.0.0") (d (list (d (n "nanorand") (r "^0.6.1") (f (quote ("chacha"))) (d #t) (k 0)))) (h "18xwx11vpchnj91bhhj0y6c6fa5ai87ckkk9l7h21908nr0was28")))

