(define-module (crates-io wa sm wasmtime-wit-bindgen) #:use-module (crates-io))

(define-public crate-wasmtime-wit-bindgen-5.0.0 (c (n "wasmtime-wit-bindgen") (v "5.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.3") (d #t) (k 0)))) (h "0ljni6mp2abzdj27hf371rp7ysbhy8qngj50pys8av8ppik1y6zm")))

(define-public crate-wasmtime-wit-bindgen-6.0.0 (c (n "wasmtime-wit-bindgen") (v "6.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.6.0") (d #t) (k 0)))) (h "1zxkx91zvrcb85r0q84vcfrnv2qwppn84f7z0jgb45d1rxc1rswj")))

(define-public crate-wasmtime-wit-bindgen-5.0.1 (c (n "wasmtime-wit-bindgen") (v "5.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.3") (d #t) (k 0)))) (h "01z01bkwg768x3prj9vzzw7z0h2i564rcmgk8nvlhnh4fg06cxfr")))

(define-public crate-wasmtime-wit-bindgen-6.0.1 (c (n "wasmtime-wit-bindgen") (v "6.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.6.0") (d #t) (k 0)))) (h "1sk7j52dkm8lnkw53why3s87wjmxwsycff12y09jgzdlwn4jhp68")))

(define-public crate-wasmtime-wit-bindgen-7.0.0 (c (n "wasmtime-wit-bindgen") (v "7.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.6.1") (d #t) (k 0)))) (h "07a28q2jiaacql8dq9g71wy7i1yqbzfgv64hi8yl8vwa7hzzjb7x")))

(define-public crate-wasmtime-wit-bindgen-8.0.0 (c (n "wasmtime-wit-bindgen") (v "8.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.6.4") (d #t) (k 0)))) (h "1xcklr76x30mdzf6ci1j85b2p42yxagg8scia2j3s6xchc4xnvvr")))

(define-public crate-wasmtime-wit-bindgen-6.0.2 (c (n "wasmtime-wit-bindgen") (v "6.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.6.0") (d #t) (k 0)))) (h "0qya35nhs1xhlp357g6m21hbpr8mn2w60k18kpglms2h57s9kb5x")))

(define-public crate-wasmtime-wit-bindgen-8.0.1 (c (n "wasmtime-wit-bindgen") (v "8.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.6.4") (d #t) (k 0)))) (h "1pgp3y3xwjrd5n55vnm4n45gq1b6vjhgcgx5jaldl6jd576bjgcq")))

(define-public crate-wasmtime-wit-bindgen-7.0.1 (c (n "wasmtime-wit-bindgen") (v "7.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.6.1") (d #t) (k 0)))) (h "150hq4y92b5dwbw936fv1ph7nlayfjdprzgwsh3swr0azwsjl7qf")))

(define-public crate-wasmtime-wit-bindgen-9.0.0 (c (n "wasmtime-wit-bindgen") (v "9.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.7.0") (d #t) (k 0)))) (h "1ygq6i8dql91g22mdhapgnrpys1bfdr0ic8sygw8a598nrqj50zs")))

(define-public crate-wasmtime-wit-bindgen-9.0.1 (c (n "wasmtime-wit-bindgen") (v "9.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.7.0") (d #t) (k 0)))) (h "00lhdrcb305liyah94m7rrazdg2rlwqlrcdhraz5llqr1xw42srz")))

(define-public crate-wasmtime-wit-bindgen-9.0.2 (c (n "wasmtime-wit-bindgen") (v "9.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.7.0") (d #t) (k 0)))) (h "1fyi8hjjr9j3cg68hbxza4xgvqd326082iix0s34dchd9g8qz7p9")))

(define-public crate-wasmtime-wit-bindgen-9.0.3 (c (n "wasmtime-wit-bindgen") (v "9.0.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.7.0") (d #t) (k 0)))) (h "07mwma8cf35p83s7k6vwpbba9qdmvgd2c0hnh0ib7schqbkybzjs")))

(define-public crate-wasmtime-wit-bindgen-9.0.4 (c (n "wasmtime-wit-bindgen") (v "9.0.4") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.7.0") (d #t) (k 0)))) (h "1arpdpalbiv3h93f6nwfy6b25ir464yvx82kmqsjnqawrhb0s7s2")))

(define-public crate-wasmtime-wit-bindgen-10.0.0 (c (n "wasmtime-wit-bindgen") (v "10.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.8.0") (d #t) (k 0)))) (h "0bj0naldii105jn9arc1yf2ljicgi2p2mjhlq64j148cqnsbs9fl")))

(define-public crate-wasmtime-wit-bindgen-10.0.1 (c (n "wasmtime-wit-bindgen") (v "10.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.8.0") (d #t) (k 0)))) (h "12k1pir7ch3ig63ngmn3f2bl4b0x9m3q7njw6kg41lx4cq24ncyk")))

(define-public crate-wasmtime-wit-bindgen-11.0.0 (c (n "wasmtime-wit-bindgen") (v "11.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.8.0") (d #t) (k 0)))) (h "10msbgksj5d85qppay0fpq1i6dsa6jh2lsh9kgf2snp20hp77mms")))

(define-public crate-wasmtime-wit-bindgen-11.0.1 (c (n "wasmtime-wit-bindgen") (v "11.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.8.0") (d #t) (k 0)))) (h "0x6qk269sr39fznfsh7yf6srr5xfm2fkhgcmrnkzmmd2lq2s1944")))

(define-public crate-wasmtime-wit-bindgen-12.0.0 (c (n "wasmtime-wit-bindgen") (v "12.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.9.2") (d #t) (k 0)))) (h "16q0xx0bzbjlzwr76gahvgzxrydqdlx5hais2i6mxj6ljjqcxagb")))

(define-public crate-wasmtime-wit-bindgen-12.0.1 (c (n "wasmtime-wit-bindgen") (v "12.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.9.2") (d #t) (k 0)))) (h "0z92cdmg4s851l2a13lqp4difah47xxhgra1hpqjrqxsvsm5rmdv")))

(define-public crate-wasmtime-wit-bindgen-10.0.2 (c (n "wasmtime-wit-bindgen") (v "10.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.8.0") (d #t) (k 0)))) (h "17754mv3pa6bgxcrcq1gxwhshmb4q5aaxdjkf3v4x7nhzdz4057i")))

(define-public crate-wasmtime-wit-bindgen-11.0.2 (c (n "wasmtime-wit-bindgen") (v "11.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "wit-parser") (r "^0.8.0") (d #t) (k 0)))) (h "1fqv66rwma17lw0danclldq7sxabszn5kp7hc2z1m2z6ngj8vndf")))

(define-public crate-wasmtime-wit-bindgen-12.0.2 (c (n "wasmtime-wit-bindgen") (v "12.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.9.2") (d #t) (k 0)))) (h "0w9sc0lyyqdwpxyw6cs676qcpfn3gkwsx1wrvmnahszm4040sxql")))

(define-public crate-wasmtime-wit-bindgen-13.0.0 (c (n "wasmtime-wit-bindgen") (v "13.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.0") (d #t) (k 0)))) (h "1ygvg53dwbmsp19l71y0g5kpdqdyvmk36r4k7qwwr29gh943s032")))

(define-public crate-wasmtime-wit-bindgen-14.0.0 (c (n "wasmtime-wit-bindgen") (v "14.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.12.1") (d #t) (k 0)))) (h "0v58llykn4lnjxfglayrv0gva5wc7kanydh5k5kg32qyh4nmg6cv")))

(define-public crate-wasmtime-wit-bindgen-14.0.1 (c (n "wasmtime-wit-bindgen") (v "14.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.12.1") (d #t) (k 0)))) (h "1z0gpc6avk3w4mr063hhwzk44xv7rblqv19q8nzkll5ggzng6whb")))

(define-public crate-wasmtime-wit-bindgen-13.0.1 (c (n "wasmtime-wit-bindgen") (v "13.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.11.0") (d #t) (k 0)))) (h "163ip0xjzvwhqsy1pll90fsh5ydicj72zfmjl338v9kpizakph6w")))

(define-public crate-wasmtime-wit-bindgen-14.0.2 (c (n "wasmtime-wit-bindgen") (v "14.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.12.1") (d #t) (k 0)))) (h "0pkg6nx1xc8bpcq43qnmfglz9cbv3yfk2kignwx9mz8s0860cwpn")))

(define-public crate-wasmtime-wit-bindgen-14.0.3 (c (n "wasmtime-wit-bindgen") (v "14.0.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.12.1") (d #t) (k 0)))) (h "0nh28xnqmg5m56cfllsw0v7lggyqhy4266yg1cmz9dwpvm2829fv")))

(define-public crate-wasmtime-wit-bindgen-14.0.4 (c (n "wasmtime-wit-bindgen") (v "14.0.4") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.12.1") (d #t) (k 0)))) (h "1xrvphsdsc7wnkf5yx1i3612a0nc1m5vlnxdf8layxixa6klq8cx")))

(define-public crate-wasmtime-wit-bindgen-15.0.0 (c (n "wasmtime-wit-bindgen") (v "15.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "11gi2c7p932r1d7hnb2sa3q6bfy6a0xk54djhpkc0l7jpdxnqy21")))

(define-public crate-wasmtime-wit-bindgen-15.0.1 (c (n "wasmtime-wit-bindgen") (v "15.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "0vr1flfxk0ivy72gbr4yr1lsg89bfzkny0i1m8vns38c7pylv02b")))

(define-public crate-wasmtime-wit-bindgen-16.0.0 (c (n "wasmtime-wit-bindgen") (v "16.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "0aq6d385lnglaf1a9vp01g0a8k9spvanx23fflj069whlvab4a7k")))

(define-public crate-wasmtime-wit-bindgen-17.0.0 (c (n "wasmtime-wit-bindgen") (v "17.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "14vqn1lsjd85mx431ckmh0jnl811s8msv46xmyal9izxk1cngra1")))

(define-public crate-wasmtime-wit-bindgen-17.0.1 (c (n "wasmtime-wit-bindgen") (v "17.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "0yy2bsq9n63q2iij1jix4mydc6wwlray1zsv2dhlkkriq29brcvb")))

(define-public crate-wasmtime-wit-bindgen-18.0.0 (c (n "wasmtime-wit-bindgen") (v "18.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.1") (d #t) (k 0)))) (h "1k03ldx4ckv8xp2wbcav8an61vcz20dckszqxirs9m0i445md3f5")))

(define-public crate-wasmtime-wit-bindgen-18.0.1 (c (n "wasmtime-wit-bindgen") (v "18.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.1") (d #t) (k 0)))) (h "01wm72c8kywqla4xxn925dm88m2sjqhvmabdls7269m5w1a9fvpz")))

(define-public crate-wasmtime-wit-bindgen-17.0.2 (c (n "wasmtime-wit-bindgen") (v "17.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "1n68ymnap1pxrqhgfmjwsvmqnbw109in2v25bp6jnh6i3fv4kcc1")))

(define-public crate-wasmtime-wit-bindgen-18.0.2 (c (n "wasmtime-wit-bindgen") (v "18.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.1") (d #t) (k 0)))) (h "14bg8n4xs7gfkmnpbqxfl3cmm28dxil9jnmj2wk60icyhqv7qrya")))

(define-public crate-wasmtime-wit-bindgen-18.0.3 (c (n "wasmtime-wit-bindgen") (v "18.0.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.1") (d #t) (k 0)))) (h "14lbk00251ix1kk5pdky8131ai5l3vhz1xk94s00fzm2h5kwgwps")))

(define-public crate-wasmtime-wit-bindgen-19.0.0 (c (n "wasmtime-wit-bindgen") (v "19.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "0f6d2qjzbh2qyfn7a5hfjnb2zz1nsqk23gfia2ghjv7v02c6qcln")))

(define-public crate-wasmtime-wit-bindgen-19.0.1 (c (n "wasmtime-wit-bindgen") (v "19.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "11wmylc7bmcllx19c85w4klnk29d88p4n27adggif1pqsbpxv4x5")))

(define-public crate-wasmtime-wit-bindgen-18.0.4 (c (n "wasmtime-wit-bindgen") (v "18.0.4") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.1") (d #t) (k 0)))) (h "1xb5hchdzbhxj16lm403iq817fd2fzm3nwp92q8ahjz0zrngqib9")))

(define-public crate-wasmtime-wit-bindgen-19.0.2 (c (n "wasmtime-wit-bindgen") (v "19.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.201.0") (d #t) (k 0)))) (h "0wmga6v328gzsn5ljaxzgl5pp4xgyfzlir836r4xgv85hly2aykw")))

(define-public crate-wasmtime-wit-bindgen-17.0.3 (c (n "wasmtime-wit-bindgen") (v "17.0.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.13.0") (d #t) (k 0)))) (h "1f16hq0l81dj98bgrwlb2ysv5va1hy9kz2p13wkp1bddv8dbnpxg")))

(define-public crate-wasmtime-wit-bindgen-20.0.0 (c (n "wasmtime-wit-bindgen") (v "20.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.202.0") (d #t) (k 0)))) (h "0yj8xbr5498i6ld85g3ssws30b2mx8iw665pz80amx0g7fxwfnsr")))

(define-public crate-wasmtime-wit-bindgen-20.0.1 (c (n "wasmtime-wit-bindgen") (v "20.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.202.0") (d #t) (k 0)))) (h "01fw4zv3zyfr34p1bpcxm9xilx4zsc3vqz4wmwc6ksrqmy1hq7ad")))

(define-public crate-wasmtime-wit-bindgen-20.0.2 (c (n "wasmtime-wit-bindgen") (v "20.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (d #t) (k 0)) (d (n "wit-parser") (r "^0.202.0") (d #t) (k 0)))) (h "15kscpm5kfkbgiz3q26vfsfqzransvdlwgvfg566c0dvzc60r101")))

(define-public crate-wasmtime-wit-bindgen-21.0.0 (c (n "wasmtime-wit-bindgen") (v "21.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (k 0)) (d (n "wit-parser") (r "^0.207.0") (d #t) (k 0)))) (h "1vjmshk5a0v8pivlmqqyhazmjj6rzx3diz5r6k1phkas4vyf9iah") (f (quote (("std"))))))

(define-public crate-wasmtime-wit-bindgen-21.0.1 (c (n "wasmtime-wit-bindgen") (v "21.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "indexmap") (r "^2.0.0") (k 0)) (d (n "wit-parser") (r "^0.207.0") (d #t) (k 0)))) (h "187x2pcmdfm4mjvwqk7h0krnv5mjpcnlzyxmlcmdwa4wwqnaadn9") (f (quote (("std"))))))

