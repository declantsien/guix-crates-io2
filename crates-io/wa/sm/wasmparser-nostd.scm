(define-module (crates-io wa sm wasmparser-nostd) #:use-module (crates-io))

(define-public crate-wasmparser-nostd-0.82.0 (c (n "wasmparser-nostd") (v "0.82.0") (d (list (d (n "no-std-compat") (r "^0.4.1") (f (quote ("compat_hash" "compat_sync" "alloc"))) (k 0)))) (h "1zfkz0pz2qy4c9b0fv6irxfvki2sz954nyap6m8icpri7xzbcrf0") (f (quote (("std" "no-std-compat/std") ("deterministic") ("default" "std"))))))

(define-public crate-wasmparser-nostd-0.83.0 (c (n "wasmparser-nostd") (v "0.83.0") (h "06sp14plgih22jxmz1899mkax89p0lyiar6sydzwxmgyb085z92q") (f (quote (("std") ("deterministic") ("default" "std"))))))

(define-public crate-wasmparser-nostd-0.87.0 (c (n "wasmparser-nostd") (v "0.87.0") (d (list (d (n "indexmap") (r "^0.1.0") (k 0) (p "indexmap-nostd")))) (h "1mbp3i8m10iypgsdpdjwk9hbqm66b6c218idbfh9xcdvfvclvjnw") (f (quote (("std" "indexmap/std") ("deterministic") ("default" "std"))))))

(define-public crate-wasmparser-nostd-0.90.0 (c (n "wasmparser-nostd") (v "0.90.0") (d (list (d (n "indexmap") (r "^0.4.0") (k 0) (p "indexmap-nostd")))) (h "19l4gkyp2xvgwjkabym0nz9rjw7ldh2z4b9q867h66sj9jzlzacj") (f (quote (("std" "indexmap/std") ("deterministic") ("default" "std"))))))

(define-public crate-wasmparser-nostd-0.91.0 (c (n "wasmparser-nostd") (v "0.91.0") (d (list (d (n "indexmap") (r "^0.4.0") (k 0) (p "indexmap-nostd")))) (h "12ri9v0rwhrnki77dfl4dmfnmwcaz7kdiwf0wxdgsax6nl8g6dww") (f (quote (("std" "indexmap/std") ("deterministic") ("default" "std"))))))

(define-public crate-wasmparser-nostd-0.99.0 (c (n "wasmparser-nostd") (v "0.99.0") (d (list (d (n "indexmap") (r "^0.4.0") (k 0) (p "indexmap-nostd")))) (h "031s6qjbcpn2x6ndjxcsczd1ib8msb2ccv9c5qqq4688hq95lpax") (f (quote (("std" "indexmap/std") ("default" "std"))))))

(define-public crate-wasmparser-nostd-0.100.0 (c (n "wasmparser-nostd") (v "0.100.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^0.4.0") (k 0) (p "indexmap-nostd")) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1dvdcx9sv9r48jzjb3cnyimyc4q58jf2f81s9in56ifpy7ajqsjn") (f (quote (("std" "indexmap/std") ("default" "std")))) (y #t)))

(define-public crate-wasmparser-nostd-0.100.1 (c (n "wasmparser-nostd") (v "0.100.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^0.4.0") (k 0) (p "indexmap-nostd")) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "0907ksqa6n535c258j1hlxpmv7q3g9cancsq73yin8h362wclmwi") (f (quote (("std" "indexmap/std") ("default" "std"))))))

(define-public crate-wasmparser-nostd-0.100.2 (c (n "wasmparser-nostd") (v "0.100.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "indexmap") (r "^0.4.0") (k 0) (p "indexmap-nostd")) (d (n "once_cell") (r "^1.13.0") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 2)))) (h "1ak4bi9k9jb223xw7mlxkgim6lp7m8bwfqhlpfa4ll7kjpz1b86m") (f (quote (("std" "indexmap/std") ("default" "std"))))))

