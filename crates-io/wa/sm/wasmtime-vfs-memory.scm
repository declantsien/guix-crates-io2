(define-module (crates-io wa sm wasmtime-vfs-memory) #:use-module (crates-io))

(define-public crate-wasmtime-vfs-memory-0.1.0 (c (n "wasmtime-vfs-memory") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("sync"))) (k 0)) (d (n "wasi-common") (r "^2.0.0") (d #t) (k 0)) (d (n "wasmtime-vfs-ledger") (r "^0.1.0") (d #t) (k 0)))) (h "0nbkc8yc50s7j10vwq8hsyqzzwylrlp0fxapv7zgd0p74c2a6blc")))

