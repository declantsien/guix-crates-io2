(define-module (crates-io wa sm wasmcloud-interface-surrealdb) #:use-module (crates-io))

(define-public crate-wasmcloud-interface-surrealdb-0.1.0 (c (n "wasmcloud-interface-surrealdb") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.13.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7.0") (d #t) (k 1)))) (h "1s9yqvvn9zh5qh9n271ly8sh3sp4j6806a0f6fwy1ssn8z72y0xs")))

