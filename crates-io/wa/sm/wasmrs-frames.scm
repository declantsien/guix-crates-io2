(define-module (crates-io wa sm wasmrs-frames) #:use-module (crates-io))

(define-public crate-wasmrs-frames-0.3.0 (c (n "wasmrs-frames") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0s1p60m2l1048jrp58anrpcvb7l70khl7g1zq9a5az7aaacl8m07")))

(define-public crate-wasmrs-frames-0.4.0 (c (n "wasmrs-frames") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0j9r5a26x8kyg6adj08lcm2wcbw29y7laph9q6m6lpj63mdm779z")))

(define-public crate-wasmrs-frames-0.5.0 (c (n "wasmrs-frames") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "05izxxhfnfv8s6mwc7s8scg46g3gckf5djii3k3vaahdngm1wj7v") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.6.0 (c (n "wasmrs-frames") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1rlb93rf3ykm7hqwc73kxwxz807x9q2wkdj9flgxhwc1qyxz8c50") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.7.0 (c (n "wasmrs-frames") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0mvqpvv23rj4j4xw84dhg4h4bav2kz1yh7j1bi8v7bin2l9nmchz") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.8.0 (c (n "wasmrs-frames") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0lrnkf6cqk1q51lxjwybcvz6lbfpiq00m6j6f88rhm931f6rwry6") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.9.0 (c (n "wasmrs-frames") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1370ainvrpb69z07szg7gvlxb3rn2i190icbga8gshlhxvhas706") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.10.0 (c (n "wasmrs-frames") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "03rjm0zv1nnzkj7sdbckpr7xs7ikd8kn2q7h1w1vc3rjzvadjpsk") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.11.0 (c (n "wasmrs-frames") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1chsw5pdd6ifx8k1v2rn38k1g8hqm2yn88kh8qnfl6vc6avcakxc") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.12.0 (c (n "wasmrs-frames") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1skq4z5ydvbnmk0adbmivnazn2ajlsncnws2xa00hf59vli9k6j6") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.12.1 (c (n "wasmrs-frames") (v "0.12.1") (d (list (d (n "anyhow") (r "^1.0") (k 2)) (d (n "bytes") (r "^1.2") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1mdc2213qy7aavd7s8c1iwgd6glcdcjrkh5nvwhxfjimpczahjsv") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.13.0 (c (n "wasmrs-frames") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (k 2)) (d (n "bytes") (r "^1.2") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0v4m2n4djfpgd7s4267s8ykfxgb9vjwm4xyv10nhzyq834m4azzz") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.14.0 (c (n "wasmrs-frames") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0") (k 2)) (d (n "bytes") (r "^1.2") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0k7pgy2i0fxw4k8l5x2q4ggr0vyfj6c8rfdyj58ln8zy94y78yak") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.15.0 (c (n "wasmrs-frames") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0") (k 2)) (d (n "bytes") (r "^1.2") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1myfa1vmnyb7sjq6hbb7gk6fqaz45djzzw8bb0cyzw71hv7if2fz") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.16.0 (c (n "wasmrs-frames") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0") (k 2)) (d (n "bytes") (r "^1.2") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0v5ndww94sfwlk78vnfgwya252g533pcvf10qy0nq9fglvla9w1b") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.16.1 (c (n "wasmrs-frames") (v "0.16.1") (d (list (d (n "anyhow") (r "^1.0") (k 2)) (d (n "bytes") (r "^1.2") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0zxp5yniq8hvg473cz6195fksrza779n5pa4m23lzir6wlp7l0ry") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.17.0 (c (n "wasmrs-frames") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "bytes") (r "^1.2") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "0qg7jaxahr0r773shskw7w0r5sdiz0n55qxv2py0v27w3s38n716") (f (quote (("derive_serde" "serde/derive") ("default"))))))

(define-public crate-wasmrs-frames-0.17.1 (c (n "wasmrs-frames") (v "0.17.1") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 2)) (d (n "bytes") (r "^1.2") (k 0)) (d (n "serde") (r "^1") (o #t) (k 0)))) (h "1hv9dcv8fq4i8bm6pbj8xlfby9p3rys3x4ag7pa37ixmrxbcq764") (f (quote (("derive_serde" "serde/derive") ("default"))))))

