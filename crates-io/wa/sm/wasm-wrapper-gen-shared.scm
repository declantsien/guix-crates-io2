(define-module (crates-io wa sm wasm-wrapper-gen-shared) #:use-module (crates-io))

(define-public crate-wasm-wrapper-gen-shared-0.0.1 (c (n "wasm-wrapper-gen-shared") (v "0.0.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0m43vs6vyay19h6m622fmlxr6s9ys7d1j0wrzh7hwjayg7w9vy5p")))

(define-public crate-wasm-wrapper-gen-shared-0.0.2 (c (n "wasm-wrapper-gen-shared") (v "0.0.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "10ym475dsqi9r0yqdr7cqms8w3x7sa8c3fw6r29z56fd8i9n8cr1")))

(define-public crate-wasm-wrapper-gen-shared-0.0.3 (c (n "wasm-wrapper-gen-shared") (v "0.0.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^0.11") (f (quote ("full"))) (d #t) (k 0)))) (h "0lk3dsih14i9wff9dp8r8q59jzlr2z3g1bfs6qqwvzv71kpmpcvq")))

