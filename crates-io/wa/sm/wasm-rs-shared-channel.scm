(define-module (crates-io wa sm wasm-rs-shared-channel) #:use-module (crates-io))

(define-public crate-wasm-rs-shared-channel-0.1.0 (c (n "wasm-rs-shared-channel") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (o #t) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.47") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.70") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.20") (d #t) (k 2)) (d (n "wasm-rs-dbg") (r "^0.1.0") (d #t) (k 2)))) (h "12rp15dszn4103f31s7lvslxf3w86ls102s7zcsg3xaibidw6mf5") (f (quote (("serde-bincode" "bincode") ("default" "serde" "serde-bincode"))))))

