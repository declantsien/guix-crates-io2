(define-module (crates-io wa sm wasmtime-asm-macros) #:use-module (crates-io))

(define-public crate-wasmtime-asm-macros-0.40.0 (c (n "wasmtime-asm-macros") (v "0.40.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "07rgxq9wl27kwmijzb9svxbp3wc6rnz9h3piq64vc210pbbp0s2k")))

(define-public crate-wasmtime-asm-macros-0.40.1 (c (n "wasmtime-asm-macros") (v "0.40.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "0xypw9dphkzgf52zpkpgs251iqqsqfy2lrdy6yma5nflznhsvm7y")))

(define-public crate-wasmtime-asm-macros-1.0.0 (c (n "wasmtime-asm-macros") (v "1.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1y6dvqlzanf7jk6gm1a3hblqr12i7wgcjni7s752i1gnvb3bskgf")))

(define-public crate-wasmtime-asm-macros-1.0.1 (c (n "wasmtime-asm-macros") (v "1.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "0y1i19ka7xfqw6j0ndz275sfs34cdz790abksffz2jvlzvmkvgrr")))

(define-public crate-wasmtime-asm-macros-2.0.0 (c (n "wasmtime-asm-macros") (v "2.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "10p4v5sm745gqk5pn8m95az5xahy9qmfrc4s9szxj2lly1gm8dnv")))

(define-public crate-wasmtime-asm-macros-2.0.1 (c (n "wasmtime-asm-macros") (v "2.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1mynzhl3s1snl2ilfbi574wnjd6ww27slgyx6n7x0gpzy6dppwa1")))

(define-public crate-wasmtime-asm-macros-2.0.2 (c (n "wasmtime-asm-macros") (v "2.0.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1whvbz3g1p6012jjvahqj3wyc8a6j3nk308kaxck262jdps7qcny")))

(define-public crate-wasmtime-asm-macros-1.0.2 (c (n "wasmtime-asm-macros") (v "1.0.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "15z5751lyvm6gsnk8fc32b8zdl9mbzpb5xy8mp8j74lbzkfn7pj5")))

(define-public crate-wasmtime-asm-macros-3.0.0 (c (n "wasmtime-asm-macros") (v "3.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1r4nqiibnc2l5l9q2nfys19sqnsjpprcsi3pshv6p1dyafvr8riv")))

(define-public crate-wasmtime-asm-macros-3.0.1 (c (n "wasmtime-asm-macros") (v "3.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "0kxf67nknkgah3w96g7i9r3qz5dj9yj8yx6fvis6xfbrdhzmh0d2")))

(define-public crate-wasmtime-asm-macros-4.0.0 (c (n "wasmtime-asm-macros") (v "4.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "0d4kwzdkp1bv5qr5p7icxiy0g2q4qjb5klw4dsl7niphhrj21xf1")))

(define-public crate-wasmtime-asm-macros-5.0.0 (c (n "wasmtime-asm-macros") (v "5.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1ns2yfma11rzv7s1xpjnrrg552qw6l4chr02n83wyxkd4p5v3bn0")))

(define-public crate-wasmtime-asm-macros-6.0.0 (c (n "wasmtime-asm-macros") (v "6.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "0p7kvgvklkzmbnx599qdfllp6p3b0m5nsvv622wmg0ma7rk5r035")))

(define-public crate-wasmtime-asm-macros-5.0.1 (c (n "wasmtime-asm-macros") (v "5.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "0mwdpr725jv5amnmbgl3s05xjaix07ln8prr7i2v5mwwsz25vjqj")))

(define-public crate-wasmtime-asm-macros-6.0.1 (c (n "wasmtime-asm-macros") (v "6.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "12jbchsqmdhp8vyy8f142kwx2jc6wjkg6sa4jgd3500rdkjablwv")))

(define-public crate-wasmtime-asm-macros-4.0.1 (c (n "wasmtime-asm-macros") (v "4.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1n40kkz1g14vz0h3lsf4ail6dipg1acd9zbbnh3nknvmivnd598a")))

(define-public crate-wasmtime-asm-macros-7.0.0 (c (n "wasmtime-asm-macros") (v "7.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "0f4vxvalbxy61542vplj1casisg4x9ywi1k10kr4mx3ablbx8qxd")))

(define-public crate-wasmtime-asm-macros-8.0.0 (c (n "wasmtime-asm-macros") (v "8.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1qf10hszavmkckfdp5pwmvnj2bx9ldsiqzgi6ii97ma7cdqs9ayd")))

(define-public crate-wasmtime-asm-macros-6.0.2 (c (n "wasmtime-asm-macros") (v "6.0.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1k49201sxrd0pvl5lmadsdnxpbyvm3yp436gn67ksbvxdqjaf1s4")))

(define-public crate-wasmtime-asm-macros-8.0.1 (c (n "wasmtime-asm-macros") (v "8.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "03c1gxczcrfld7x17b2r15xlnpwd82afk7gnvczgmm2cq6kxmffk")))

(define-public crate-wasmtime-asm-macros-7.0.1 (c (n "wasmtime-asm-macros") (v "7.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "14i9ncrblj3wsb814rx96j0p8gvzfamncd1f7rx80ssgf2d8by86")))

(define-public crate-wasmtime-asm-macros-9.0.0 (c (n "wasmtime-asm-macros") (v "9.0.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1pf1s47vi6j1plryngqaw3c724hb0qs8a0r30611py4frpmvrjw7")))

(define-public crate-wasmtime-asm-macros-9.0.1 (c (n "wasmtime-asm-macros") (v "9.0.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "139q11ghcc6b24f83c28fjmd7vcn8bwsj92g4gaii2279bpnfjmc")))

(define-public crate-wasmtime-asm-macros-9.0.2 (c (n "wasmtime-asm-macros") (v "9.0.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1m77v5xxnr72akxx4zlqykqgqd1yfvi3591cv2qfmfi15kmlblv4")))

(define-public crate-wasmtime-asm-macros-9.0.3 (c (n "wasmtime-asm-macros") (v "9.0.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "1nhr9y5v4i99x0zndn72aay9qhbmkf3m2k76vgn7x5fa87nr30x1")))

(define-public crate-wasmtime-asm-macros-9.0.4 (c (n "wasmtime-asm-macros") (v "9.0.4") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)))) (h "0k1h9lk3c8mjfj4qx6c81d4z8wc8z2nak4dhlf1h96z79k176g6k")))

(define-public crate-wasmtime-asm-macros-10.0.0 (c (n "wasmtime-asm-macros") (v "10.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1qvj13s37jis3i6dmvj81gspvdy3lrga9343m4zrq649cfjg36gq")))

(define-public crate-wasmtime-asm-macros-10.0.1 (c (n "wasmtime-asm-macros") (v "10.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "14lvfwy1n4qqw5bl82795pgza3ad1jv71m0aph4xkqkc6ppnci18")))

(define-public crate-wasmtime-asm-macros-11.0.0 (c (n "wasmtime-asm-macros") (v "11.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0x6mlik2v2w0q0dcpgvvzcxww001wd7q3md7i496b5lmrzcggdk3")))

(define-public crate-wasmtime-asm-macros-11.0.1 (c (n "wasmtime-asm-macros") (v "11.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "16rzbjnyq6aqqix3hbx1sdm9v3pdrm75fqlc4cjr43k1lkyzp0hg")))

(define-public crate-wasmtime-asm-macros-12.0.0 (c (n "wasmtime-asm-macros") (v "12.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0wbbp7wrmwdqly0gdrmxs1x8k9j77kblyxcs5z7364766w5878dd")))

(define-public crate-wasmtime-asm-macros-12.0.1 (c (n "wasmtime-asm-macros") (v "12.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "12w0k4x23dliggvi2ikbxzmbfh8p85byy6xmlw4dsr3grsfkycc2")))

(define-public crate-wasmtime-asm-macros-10.0.2 (c (n "wasmtime-asm-macros") (v "10.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0svh7s61sijnx0llbl1ippvn389g49lkrav219hzgdd9c5b8xcnj")))

(define-public crate-wasmtime-asm-macros-11.0.2 (c (n "wasmtime-asm-macros") (v "11.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1n0w0gqqw7l1lnhazfx3acgxl5rvd419xfs0h4x2gpxkiyb6wv77")))

(define-public crate-wasmtime-asm-macros-12.0.2 (c (n "wasmtime-asm-macros") (v "12.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0pa4dl1qs5pvmqa2j493856cyl0vv1wyxwi0w4b0081iv1l4zn4n")))

(define-public crate-wasmtime-asm-macros-13.0.0 (c (n "wasmtime-asm-macros") (v "13.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0bwxh3mwpr7q3m7cv84r3fmd0qghl3h8yb33wmznigbica7hzbsk")))

(define-public crate-wasmtime-asm-macros-14.0.0 (c (n "wasmtime-asm-macros") (v "14.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1yls0b0bb9h98ifi0jhdrkg3rrcyazxv7ii08xdyr48jv9b8c0lf")))

(define-public crate-wasmtime-asm-macros-14.0.1 (c (n "wasmtime-asm-macros") (v "14.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "04b388j7q24fygwvby82bsy80c5ps0zpfh65y9hdi5rqps004xx6")))

(define-public crate-wasmtime-asm-macros-13.0.1 (c (n "wasmtime-asm-macros") (v "13.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1pvawzlggn80h9g5ridmvg0hclpvkzzh8y401kkzpv1njabfa4a7")))

(define-public crate-wasmtime-asm-macros-14.0.2 (c (n "wasmtime-asm-macros") (v "14.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1jjwwqlwl0xwwrzd5nn5qddgwn4xn0w1zzyxnrqk918mhi6999a5")))

(define-public crate-wasmtime-asm-macros-14.0.3 (c (n "wasmtime-asm-macros") (v "14.0.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0a28dsvnbym6i5jixjfgayyi4nwg9nn6j65qnr53n806sbwaxdxp")))

(define-public crate-wasmtime-asm-macros-14.0.4 (c (n "wasmtime-asm-macros") (v "14.0.4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1rlmbn35bp8pp6v3xbmprg7xbd4j63983hfp8dxai7b8np04p62l")))

(define-public crate-wasmtime-asm-macros-15.0.0 (c (n "wasmtime-asm-macros") (v "15.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0pyn6m2vnxynyd9mh50584fpzvzxbnpffi36ww48xsdkggld10f9")))

(define-public crate-wasmtime-asm-macros-15.0.1 (c (n "wasmtime-asm-macros") (v "15.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0iwbk3fyy4ddgda6wjm4v6b2zzbv6pj8vid4w0ym0lnz2nxsibdy")))

(define-public crate-wasmtime-asm-macros-16.0.0 (c (n "wasmtime-asm-macros") (v "16.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1ffbgicckbfqff6j0bfasdmjbvisnwhz43c247csr8hm7qaaj2v6")))

(define-public crate-wasmtime-asm-macros-17.0.0 (c (n "wasmtime-asm-macros") (v "17.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0wlm8ss8d0cgfpfy635ifvkvjjrfdhqwggdwldaid9mlx5182a1p")))

(define-public crate-wasmtime-asm-macros-17.0.1 (c (n "wasmtime-asm-macros") (v "17.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "13g2nrzzd7vpw3as7ka3lha8mpciwd3fjwmnczwi9v3asks5wjf1")))

(define-public crate-wasmtime-asm-macros-18.0.0 (c (n "wasmtime-asm-macros") (v "18.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0dpxhz9l392qs9dvhljfphxpcxs5j2878jjxay4776mq5jg86zdk")))

(define-public crate-wasmtime-asm-macros-18.0.1 (c (n "wasmtime-asm-macros") (v "18.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0lla8p1cbynx7v4yplgmzcr2la6q01gkgknzgs68avsw8xdkkmqr")))

(define-public crate-wasmtime-asm-macros-17.0.2 (c (n "wasmtime-asm-macros") (v "17.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0x8lyc2h9g35mnswbi1dvrgwhh2lqymm3zk2cnfl1jlwxkgipvfk")))

(define-public crate-wasmtime-asm-macros-18.0.2 (c (n "wasmtime-asm-macros") (v "18.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0hypzsx4ssqzcldx440jzslblfdm2b4ig8562a2a3cqqqwlx7fc6")))

(define-public crate-wasmtime-asm-macros-18.0.3 (c (n "wasmtime-asm-macros") (v "18.0.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "13zffpi0bx1hafy7bbcm3jfm3hs6yjvn83ivxqscw6lmx8ng031v")))

(define-public crate-wasmtime-asm-macros-19.0.0 (c (n "wasmtime-asm-macros") (v "19.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "04c2zm4i43hkbkrbkryb4v8mlgp50d3d8s7xnnhcsasl537vzk8n")))

(define-public crate-wasmtime-asm-macros-19.0.1 (c (n "wasmtime-asm-macros") (v "19.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "02dhc89hqcidbb459kh6qagalsmzhil98g0l31gkhs9dm642vlp8")))

(define-public crate-wasmtime-asm-macros-18.0.4 (c (n "wasmtime-asm-macros") (v "18.0.4") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "07ln15wgixxcw3l5wqzka7ll11hbir5ll354haanc37kkdm2sac6")))

(define-public crate-wasmtime-asm-macros-19.0.2 (c (n "wasmtime-asm-macros") (v "19.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1brxqwb8zdgrcqg16si5anmnphbz6sbglw6ajnq6y4raw2caa2hi")))

(define-public crate-wasmtime-asm-macros-17.0.3 (c (n "wasmtime-asm-macros") (v "17.0.3") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1z8p50i5rcxl0nbf49zn43raccw2jxzq7z5s8jqf7lvdasxh1lnf")))

(define-public crate-wasmtime-asm-macros-20.0.0 (c (n "wasmtime-asm-macros") (v "20.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1a8wvxxhdl80fdpxc3kxgm8nkr4ncm8ck2c94ah3wkq0f96fjpk2")))

(define-public crate-wasmtime-asm-macros-20.0.1 (c (n "wasmtime-asm-macros") (v "20.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1a3k9xrzkcr32rhl4a1074fcah543qyc9hqa8rwy453m2jpcwb65")))

(define-public crate-wasmtime-asm-macros-20.0.2 (c (n "wasmtime-asm-macros") (v "20.0.2") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0vnjk7rbrmifm1lvvbygp6lgv6gr88mdbqv7mgzc0mcb9k9c85bm")))

(define-public crate-wasmtime-asm-macros-21.0.0 (c (n "wasmtime-asm-macros") (v "21.0.0") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "0pzbv7sz0wvv9hwriy9vkfj73dnimyfxlgr4y5drj235ia18f7w2")))

(define-public crate-wasmtime-asm-macros-21.0.1 (c (n "wasmtime-asm-macros") (v "21.0.1") (d (list (d (n "cfg-if") (r "^1.0") (d #t) (k 0)))) (h "1ldj2nmjxzbppj6d1dj5k4hys9n48z3x6q1xq1cg59vlr5wqdvkd")))

