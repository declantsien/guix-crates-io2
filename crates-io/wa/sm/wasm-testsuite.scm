(define-module (crates-io wa sm wasm-testsuite) #:use-module (crates-io))

(define-public crate-wasm-testsuite-0.1.0 (c (n "wasm-testsuite") (v "0.1.0") (d (list (d (n "rust-embed") (r "^8.1.0") (f (quote ("include-exclude"))) (d #t) (k 0)))) (h "0z29ssih0szyiwsrvn3as8zfri5mq2w94h4c699rjil5gp3gbmi0")))

(define-public crate-wasm-testsuite-0.2.0 (c (n "wasm-testsuite") (v "0.2.0") (d (list (d (n "rust-embed") (r "^8.1.0") (f (quote ("include-exclude"))) (d #t) (k 0)))) (h "1gw9a9msj851zxjqdcfbagqh8xm7ys5n6ivbanbh48p9jmadv2vc")))

(define-public crate-wasm-testsuite-0.2.1 (c (n "wasm-testsuite") (v "0.2.1") (d (list (d (n "rust-embed") (r "^8.1.0") (f (quote ("include-exclude"))) (d #t) (k 0)))) (h "0l6azkvbd13rrbh4h4gxnx7sc1j9m6akgx505dgq7hnljb3i2br2")))

(define-public crate-wasm-testsuite-0.2.2 (c (n "wasm-testsuite") (v "0.2.2") (d (list (d (n "rust-embed") (r "^8.1.0") (f (quote ("include-exclude"))) (d #t) (k 0)))) (h "0ir2qwdn2sgf2skgnijkhr8hsfwy152xwv3kfr0pzc5cpyj6pb7l")))

