(define-module (crates-io wa sm wasmy-abi) #:use-module (crates-io))

(define-public crate-wasmy-abi-0.2.0 (c (n "wasmy-abi") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.2.0") (d #t) (k 0)))) (h "1wl3mb2scd8n5zhhq406jrz8sam18l0qyvdzyad03bn21080bilg") (y #t)))

(define-public crate-wasmy-abi-0.2.1 (c (n "wasmy-abi") (v "0.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.2.0") (d #t) (k 0)))) (h "0qdvhmgmp637a65vf3pzmmi1k42qlsg2vnh1vhb3k9kigkzvj36m") (y #t)))

(define-public crate-wasmy-abi-0.2.2 (c (n "wasmy-abi") (v "0.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.2.2") (d #t) (k 0)))) (h "0gfl4g89hap55qsj9as3fwphxd941mi47q711jgzqqynyr3j7bvm")))

(define-public crate-wasmy-abi-0.2.3 (c (n "wasmy-abi") (v "0.2.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.2.2") (d #t) (k 0)))) (h "1is8849x2ikii5gzyqjjzjxkcm94xqgrm5anamwgxr2phlrgp1k0")))

(define-public crate-wasmy-abi-0.3.0 (c (n "wasmy-abi") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.3.0") (d #t) (k 0)))) (h "05f3s22gyy0w855dyamx8lpgs7zjxmk41mir3792f5xkkk2iw10v")))

(define-public crate-wasmy-abi-0.3.1 (c (n "wasmy-abi") (v "0.3.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.3.1") (d #t) (k 0)))) (h "1gb1ycf1rnxlqkhjij78lhg6z32rxjjqzhyh6jkysz6x16ml4pn9")))

(define-public crate-wasmy-abi-0.3.2 (c (n "wasmy-abi") (v "0.3.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.3.2") (d #t) (k 0)))) (h "1p2ackypdwj1y2bcz4mjy543iabgw649crvq8ksfir85qk28yjpa")))

(define-public crate-wasmy-abi-0.3.3 (c (n "wasmy-abi") (v "0.3.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.3.3") (d #t) (k 0)))) (h "0cbqlcfxyklrm91zxnrkd9zjjcvrd3100kgrrwg83p82g65xax6w")))

(define-public crate-wasmy-abi-0.4.0 (c (n "wasmy-abi") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.4.0") (d #t) (k 0)))) (h "06gy2p9sjbld8wd65bz60bx21nc05fzbkrq7hhjjsnf55gkvp0c5")))

(define-public crate-wasmy-abi-0.4.1 (c (n "wasmy-abi") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.4.1") (d #t) (k 0)))) (h "04pcyabllxp6wh1h4r188s1rnpmvfrbln7d5l7apwr2wiqkj2gda")))

(define-public crate-wasmy-abi-0.4.2 (c (n "wasmy-abi") (v "0.4.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.4.2") (d #t) (k 0)))) (h "15j6w2cxq3dd8qxwiiq4m9mdmk09gm0y7xm9hf1qv722vns09mk2")))

(define-public crate-wasmy-abi-0.5.0 (c (n "wasmy-abi") (v "0.5.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.5.0") (d #t) (k 0)))) (h "0d5sz42qaml96zz6s82dsrwl92ljllarzpq6hcgw6lhfb5wiaa0s")))

(define-public crate-wasmy-abi-0.5.1 (c (n "wasmy-abi") (v "0.5.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-macros") (r "^0.5.1") (d #t) (k 0)))) (h "15n13mf8crd89i5iik16sh6lnibhbh30kjg5i2dzhk64z66jd7vf")))

(define-public crate-wasmy-abi-0.5.5 (c (n "wasmy-abi") (v "0.5.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmer") (r "^2.0.0") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "wasmer-wasi") (r "^2.0.0") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "wasmy-macros") (r "^0.5.5") (d #t) (k 0)))) (h "172c5xxp2qn51czdp8zvzfihwdb3hpmwcm7xv1az8qsj1fm693xh")))

(define-public crate-wasmy-abi-0.5.6 (c (n "wasmy-abi") (v "0.5.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmer") (r "^2.0.0") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "wasmer-wasi") (r "^2.0.0") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "wasmy-macros") (r "^0.5.6") (d #t) (k 0)))) (h "14v2c42c0lkjb2q4m3qr7gi8zd51v79qfwm4a78c89yh0lwxyipg")))

