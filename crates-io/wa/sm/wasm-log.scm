(define-module (crates-io wa sm wasm-log) #:use-module (crates-io))

(define-public crate-wasm-log-0.3.0 (c (n "wasm-log") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "1wvanic08z380kkcri4bx83pk21cbayka9jckinp1f5216h6afz7") (y #t)))

(define-public crate-wasm-log-0.3.1 (c (n "wasm-log") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("console"))) (d #t) (k 0)))) (h "10az5a8dg6si9sk8a0xyxkb7ghgij7kkszpp6x080919fqm5pyfy")))

