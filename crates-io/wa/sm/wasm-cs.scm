(define-module (crates-io wa sm wasm-cs) #:use-module (crates-io))

(define-public crate-wasm-cs-1.0.0 (c (n "wasm-cs") (v "1.0.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3.0") (d #t) (k 0)) (d (n "wasm-gen") (r "^0.1.4") (d #t) (k 0)) (d (n "wasmparser") (r "^0.116") (d #t) (k 0)))) (h "0viypbc2d7bz0jx3xbdnacz3crdzwfdisyywj32m6lqf51fym0f9") (r "1.73")))

