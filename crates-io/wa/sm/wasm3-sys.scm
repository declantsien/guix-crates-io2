(define-module (crates-io wa sm wasm3-sys) #:use-module (crates-io))

(define-public crate-wasm3-sys-0.1.0 (c (n "wasm3-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1dbab32qjyinpk7p4vz5nhxyhiw5g5vb8dpgcsvqj6shndbp5x3x") (f (quote (("wasi") ("use-32bit-slots")))) (l "wasm3")))

(define-public crate-wasm3-sys-0.1.1 (c (n "wasm3-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "1kr64nw4lbxg34j9qhrq036fslpxssfqf8g56rczf0z8h1a7w6xc") (f (quote (("wasi") ("use-32bit-slots") ("build-bindgen" "bindgen")))) (l "wasm3")))

(define-public crate-wasm3-sys-0.1.2 (c (n "wasm3-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "00khi8f87smipp2m9kxhah1q67h3qx9v14639g7wvj2h42sxvn7y") (f (quote (("wasi") ("use-32bit-slots") ("build-bindgen" "bindgen")))) (l "wasm3")))

(define-public crate-wasm3-sys-0.3.0 (c (n "wasm3-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "shlex") (r "^0.1.1") (d #t) (k 1)))) (h "1arbqn5yz7ygmhayd46dvv9d4p8kvkivljsx4x9pgzhzpw85skjs") (f (quote (("wasi") ("use-32bit-slots") ("build-bindgen" "bindgen")))) (l "wasm3")))

