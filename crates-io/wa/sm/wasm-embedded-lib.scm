(define-module (crates-io wa sm wasm-embedded-lib) #:use-module (crates-io))

(define-public crate-wasm-embedded-lib-0.1.0 (c (n "wasm-embedded-lib") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1i8ivxyjdd32gdhzc62ysqcf6n77imkb2qim3y6zcyxsfww04mna")))

