(define-module (crates-io wa sm wasmium-utilities) #:use-module (crates-io))

(define-public crate-wasmium-utilities-0.1.0 (c (n "wasmium-utilities") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("u64_backend"))) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("u64_backend"))) (k 2)) (d (n "wasmium_errors") (r "^1.0.2") (d #t) (k 0)))) (h "1m151xzz7j2czximm4z7dpqfyzdd40f799af0dkrymlyc67b2x1y")))

(define-public crate-wasmium-utilities-0.2.0 (c (n "wasmium-utilities") (v "0.2.0") (d (list (d (n "bs58") (r "^0.4.0") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("u64_backend"))) (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("u64_backend"))) (k 2)) (d (n "wasmium_errors") (r "^1.0.2") (d #t) (k 0)))) (h "0m0h5p2z3fwkwmip7ac6pgcf4lirq4rcaz446vw7n77aql1q4qmk")))

