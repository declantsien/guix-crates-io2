(define-module (crates-io wa sm wasm-reader-traits) #:use-module (crates-io))

(define-public crate-wasm-reader-traits-0.1.0 (c (n "wasm-reader-traits") (v "0.1.0") (h "1a9yp5xwn7m910pps6156zq990zm8n532vld0z8w4vsvc65a9pxj") (f (quote (("nightly"))))))

(define-public crate-wasm-reader-traits-0.1.1 (c (n "wasm-reader-traits") (v "0.1.1") (d (list (d (n "frunk_core") (r "^0.3") (d #t) (k 0)))) (h "0camxflrfxnjjbjfhhsxmr6vzvcgjna6n9yi0g8xil212xj1pjr1") (f (quote (("nightly"))))))

