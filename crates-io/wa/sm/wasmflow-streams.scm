(define-module (crates-io wa sm wasmflow-streams) #:use-module (crates-io))

(define-public crate-wasmflow-streams-0.10.0-beta.4 (c (n "wasmflow-streams") (v "0.10.0-beta.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "test-log") (r "^0.2.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "wasmflow-packet") (r "^0.10.0-beta.4") (d #t) (k 0)))) (h "1kaik69006fdk3jn626k3j6vi70dhnyg21f811gxk3ifp1n9yxv5")))

(define-public crate-wasmflow-streams-0.10.0 (c (n "wasmflow-streams") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "test-log") (r "^0.2.8") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "wasmflow-packet") (r "^0.10.0-beta.4") (d #t) (k 0)))) (h "16awcyisxsh1fav7390cw8f9k1q84m9lgl5zr07rdxcfp6n81cwa")))

