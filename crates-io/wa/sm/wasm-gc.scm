(define-module (crates-io wa sm wasm-gc) #:use-module (crates-io))

(define-public crate-wasm-gc-0.1.0 (c (n "wasm-gc") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.5") (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "14adjrlmkkk4lbnsidxpvgvf726m4w87ssmhnwp17xz1mmaq5y1c")))

(define-public crate-wasm-gc-0.1.1 (c (n "wasm-gc") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.5") (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "1704qh11faba76l3d0g9nng0bkx8n6jbwp13b8ka6gw1vh8w4h88")))

(define-public crate-wasm-gc-0.1.6 (c (n "wasm-gc") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.5") (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "03diw6mbhl8gihbqzw161a74b5a9kqi3k7923mbkca5smiiijagq")))

