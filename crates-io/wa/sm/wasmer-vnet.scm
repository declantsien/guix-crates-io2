(define-module (crates-io wa sm wasmer-vnet) #:use-module (crates-io))

(define-public crate-wasmer-vnet-3.0.0-alpha.4 (c (n "wasmer-vnet") (v "3.0.0-alpha.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-alpha.4") (k 0)))) (h "1k9gyairyhfm6qnkycy631c6pwnr4rjqq5m2wm91w2zy9qwcxaf7") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.0.0-beta (c (n "wasmer-vnet") (v "3.0.0-beta") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-beta") (k 0)))) (h "1h4k2941dwdfa5ifipgylnq8yikg4b9l3h7c3c52kwv5h6msl76i") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.0.0-beta.2 (c (n "wasmer-vnet") (v "3.0.0-beta.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-beta.2") (k 0)))) (h "0b2z4ygm94z9hikzw12mpfnk979khzmy2x8bb5yryqw20bd8p02v") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.0.0-rc.1 (c (n "wasmer-vnet") (v "3.0.0-rc.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-rc.1") (k 0)))) (h "1w944vxmqb14svgsi3yywgfvgxvrnyx4c94clsxkyyyxjkyjg8g2") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.0.0-rc.2 (c (n "wasmer-vnet") (v "3.0.0-rc.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-rc.2") (k 0)))) (h "1y1qaaarajh92sgfl5x0izld70z830izw4l649rp9wa15iz0bj0x") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.0.0-rc.3 (c (n "wasmer-vnet") (v "3.0.0-rc.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-rc.3") (k 0)))) (h "0i26i0rzsjg30mr44l8xb3ip6lj2xqmph5pjq4rg9cjkzp3snfnf") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.0.0-rc.4 (c (n "wasmer-vnet") (v "3.0.0-rc.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-rc.4") (k 0)))) (h "12d2q839kyz1k6y9g12m1bjcwxa4lqz9pvlql3g37g8d7ndg8nd9") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.0.0 (c (n "wasmer-vnet") (v "3.0.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0") (k 0)))) (h "0szdy9328ark83a46rd2j1xbrchflyr0hz0s7nn0dwbrylrnf9zs") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.0.1 (c (n "wasmer-vnet") (v "3.0.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.1") (k 0)))) (h "1mzh37jikag3lnm02xgxscm1hnwlan492s70v6kwv3rdqhajw5yx") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.0.2 (c (n "wasmer-vnet") (v "3.0.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.2") (k 0)))) (h "033g39sf2zmfbp20x9shimalb7lgajxd21lw5vilvl1j7043i0lq") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.1.0 (c (n "wasmer-vnet") (v "3.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.1.0") (k 0)))) (h "0w3jyax095ya1x1xsyi369msd5h1dqyzg5fykgc3gfpn5j8949wh") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.2.0-alpha.1 (c (n "wasmer-vnet") (v "3.2.0-alpha.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.2.0-alpha.1") (k 0)))) (h "1n96hjz7rhclh921ba2ip8lniviwdw3736m5p63byfy74iqsc6xc") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.0.3 (c (n "wasmer-vnet") (v "3.0.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.3") (k 0)))) (h "0kkgn1x1ll6119kvpl40vjr70d18n3sn1imibwgdkrfpf968armv") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vnet-3.1.1 (c (n "wasmer-vnet") (v "3.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.1.1") (k 0)))) (h "1hdlglp0qx8l51sj3hhjfi4g6vm7v2carpyw3f821inc90xzxi4v") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

