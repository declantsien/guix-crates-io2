(define-module (crates-io wa sm wasm-quicksort-example) #:use-module (crates-io))

(define-public crate-wasm-quicksort-example-0.0.1 (c (n "wasm-quicksort-example") (v "0.0.1") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tsify") (r "^0.4.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (k 2)))) (h "0vpgvwkhjxjzr2jmn18w736mc6y3m3csc76s6lidiqx6i2ils61b") (f (quote (("default" "console_error_panic_hook"))))))

