(define-module (crates-io wa sm wasm_remapper) #:use-module (crates-io))

(define-public crate-wasm_remapper-0.1.0 (c (n "wasm_remapper") (v "0.1.0") (d (list (d (n "parity-wasm") (r "^0.41.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1dz9j6bqmxq6md5mi9l1viv4iiq3rzkrkijbjdd72pz63wh2r9m9")))

