(define-module (crates-io wa sm wasmer-emscripten-asml-fork) #:use-module (crates-io))

(define-public crate-wasmer-emscripten-asml-fork-1.0.2 (c (n "wasmer-emscripten-asml-fork") (v "1.0.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.1") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "^1.0.2") (k 0) (p "wasmer-asml-fork")))) (h "0vjrym4g5cjacww6kjblpgr581z2i4z5mziw9qfq48ja145wykyk")))

