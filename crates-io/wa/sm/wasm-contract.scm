(define-module (crates-io wa sm wasm-contract) #:use-module (crates-io))

(define-public crate-wasm-contract-0.0.0 (c (n "wasm-contract") (v "0.0.0") (d (list (d (n "nom") (r "^5.0.0-beta2") (d #t) (k 0)) (d (n "wabt") (r "^0.7") (d #t) (k 2)) (d (n "wasmparser") (r "^0.30") (o #t) (d #t) (k 0)))) (h "1pkqa6dbnvwr2y9h6pamm5myagsjb6p80fc86njx8v08r1pl0hsb") (f (quote (("validation" "wasmparser") ("default" "validation"))))))

