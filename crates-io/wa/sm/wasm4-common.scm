(define-module (crates-io wa sm wasm4-common) #:use-module (crates-io))

(define-public crate-wasm4-common-0.1.0 (c (n "wasm4-common") (v "0.1.0") (h "0rjhbgcqr5xkpagy9cp3a5lr6n9hw6njkbidhr00jsjfjxvgmlhk")))

(define-public crate-wasm4-common-0.1.1 (c (n "wasm4-common") (v "0.1.1") (h "0bc5ny1hsdw9dz90il0xd6y2ldv9kf84rdghcw5lcdf35s95qdjd")))

(define-public crate-wasm4-common-0.1.2 (c (n "wasm4-common") (v "0.1.2") (h "0j469xiqa0var54ai9j642sim8vh4w51jgng79wwv3l7i4z3gvmm")))

(define-public crate-wasm4-common-0.1.3 (c (n "wasm4-common") (v "0.1.3") (h "1irp3h02x7vxg0m07vvz3izj1g2z6wjw7b8sslx00zn25hc7lwnh")))

(define-public crate-wasm4-common-0.1.4 (c (n "wasm4-common") (v "0.1.4") (h "1gs098wp1y4alpj5hnvmrdv8rkccwwr8m6xms8vfpbm6jcpmvr1s")))

