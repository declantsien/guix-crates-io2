(define-module (crates-io wa sm wasm-builder-runner) #:use-module (crates-io))

(define-public crate-wasm-builder-runner-1.0.5 (c (n "wasm-builder-runner") (v "1.0.5") (h "063m8lm1f2jyypw26cr4d2dm819gj67x51abwikj9gd4plh6mpa8")))

(define-public crate-wasm-builder-runner-1.0.6 (c (n "wasm-builder-runner") (v "1.0.6") (h "0jjyjsnaswsprpq737ns9rzkqysk2jx381pb4pll3k5fia0k9jh0")))

