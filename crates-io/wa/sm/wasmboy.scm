(define-module (crates-io wa sm wasmboy) #:use-module (crates-io))

(define-public crate-wasmboy-0.0.1 (c (n "wasmboy") (v "0.0.1") (h "1h18qsjh44jdzlb10n2257dcvqgrh35z56zx294qnsppv9x18faq")))

(define-public crate-wasmboy-0.0.2 (c (n "wasmboy") (v "0.0.2") (h "1z28cg0anp5hpr6jd4ll3rqgg0dxvj5pbkidychgn7ssdm7vqg5y")))

