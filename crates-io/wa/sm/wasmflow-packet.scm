(define-module (crates-io wa sm wasmflow-packet) #:use-module (crates-io))

(define-public crate-wasmflow-packet-0.10.0-beta.4 (c (n "wasmflow-packet") (v "0.10.0-beta.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "wasmflow-codec") (r "^0.10.0-beta.4") (d #t) (k 0)))) (h "0lj7wh42cvfn6hza67lla6fn6ig5a88x411z426g7zbp8kjnipa0") (f (quote (("v0") ("default") ("all" "v0"))))))

(define-public crate-wasmflow-packet-0.10.0 (c (n "wasmflow-packet") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "wasmflow-codec") (r "^0.10.0-beta.4") (d #t) (k 0)))) (h "0wiq27jv7xqk9kvmwwrkqbnyc9c3irvw7ichagyy22ap98yb7sqg") (f (quote (("v0") ("default") ("all" "v0"))))))

