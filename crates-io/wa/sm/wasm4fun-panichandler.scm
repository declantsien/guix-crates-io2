(define-module (crates-io wa sm wasm4fun-panichandler) #:use-module (crates-io))

(define-public crate-wasm4fun-panichandler-0.1.0 (c (n "wasm4fun-panichandler") (v "0.1.0") (d (list (d (n "wasm4fun-core") (r "^0.1.0") (d #t) (k 0)))) (h "099azdxdld0is2hjzlz1689f0bl2f13j879wil9p028jkf0qm39d") (f (quote (("nosound") ("default"))))))

