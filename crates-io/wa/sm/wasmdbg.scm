(define-module (crates-io wa sm wasmdbg) #:use-module (crates-io))

(define-public crate-wasmdbg-0.1.0 (c (n "wasmdbg") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3") (d #t) (k 0)) (d (n "linefeed") (r "^0.6") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.40") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "shellexpand") (r "^1.0") (d #t) (k 0)) (d (n "terminal_size") (r "^0.1") (d #t) (k 0)) (d (n "wasmi-validation") (r "^0.2") (d #t) (k 0)))) (h "03y042dsanik4q931w511hvw3l94m75sn79v53sfqwv64nd8amzb")))

