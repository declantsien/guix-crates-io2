(define-module (crates-io wa sm wasm-test-runner) #:use-module (crates-io))

(define-public crate-wasm-test-runner-0.1.0 (c (n "wasm-test-runner") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "wasmer") (r "^4.0.0") (f (quote ("singlepass"))) (k 0)))) (h "02lxbmmyvx5mzdx4akkggypjzm5l1dbsdx5gdk8f5244527s487i")))

(define-public crate-wasm-test-runner-0.1.1 (c (n "wasm-test-runner") (v "0.1.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "wasmer") (r "^4.0.0") (f (quote ("singlepass"))) (k 0)))) (h "0lifwy9hsfz638v4nhd4w4z2dycy1p7h3c247baimnhdkagjafji")))

(define-public crate-wasm-test-runner-0.1.2 (c (n "wasm-test-runner") (v "0.1.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.171") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "wasmer") (r "^4.0.0") (f (quote ("singlepass"))) (k 0)))) (h "1myri39wfhp57klrbqq1zy1c3q84mkpawl3q6ivjp50lqbjsn4kq")))

