(define-module (crates-io wa sm wasm-stopwatch) #:use-module (crates-io))

(define-public crate-wasm-stopwatch-0.1.0 (c (n "wasm-stopwatch") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.8") (f (quote ("Window" "Performance"))) (d #t) (k 0)))) (h "1y227mq9r5vd0b5bdf3q1zwn8gxzglyw3651wmifkp0k55v53hlw")))

(define-public crate-wasm-stopwatch-0.2.0 (c (n "wasm-stopwatch") (v "0.2.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "time") (r "^0.2.22") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "web-sys") (r "^0.3.45") (f (quote ("Window" "Performance"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1h91v3gg4lryr146p00vf5sik8bj797i3f92zr6jk41g0q4zvw4j")))

(define-public crate-wasm-stopwatch-0.2.1 (c (n "wasm-stopwatch") (v "0.2.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "time") (r "^0.3.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "web-sys") (r "^0.3.55") (f (quote ("Window" "Performance"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1p3jvafjffvv5qp83xal6yy2lkkv22byybrpsh26md4db970sva3")))

