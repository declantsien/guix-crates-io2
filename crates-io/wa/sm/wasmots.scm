(define-module (crates-io wa sm wasmots) #:use-module (crates-io))

(define-public crate-wasmots-0.1.0 (c (n "wasmots") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3") (d #t) (k 0)))) (h "1vwdxmymqcccxkacyid2az8aglr8bj34m0jz0flcwip7dgf5v62a")))

