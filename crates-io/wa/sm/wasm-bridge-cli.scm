(define-module (crates-io wa sm wasm-bridge-cli) #:use-module (crates-io))

(define-public crate-wasm-bridge-cli-0.1.2 (c (n "wasm-bridge-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (k 0)))) (h "1nsijwwasz6hrvw7a5bsg7nkjr6pj4yq0qpb4m51qr10hj58pzr6")))

(define-public crate-wasm-bridge-cli-0.1.3 (c (n "wasm-bridge-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (k 0)))) (h "02r0kdwx6ilypi4x09gvpg69msrb85s8qm37dj36xgxz8mw6qxj0")))

(define-public crate-wasm-bridge-cli-0.1.4 (c (n "wasm-bridge-cli") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("deflate"))) (k 0)))) (h "1r2icych3ykkbpkhg3r26i5cn51f2iias55gbma79486wmyh89yj")))

(define-public crate-wasm-bridge-cli-0.1.5 (c (n "wasm-bridge-cli") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("deflate"))) (k 0)))) (h "1fy5040bqrk5lq8ljcsvfm850rn3njsfa9zhhbmw7s4xl0qggm08")))

(define-public crate-wasm-bridge-cli-0.1.6 (c (n "wasm-bridge-cli") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (f (quote ("deflate"))) (k 0)))) (h "07nxpxiy7w3pzyfwqjiixh2iyfg760hclswvq00536d844dr57mk")))

(define-public crate-wasm-bridge-cli-0.2.0 (c (n "wasm-bridge-cli") (v "0.2.0") (h "00m1vw4i391k7amqr0wk7gyhnby3jm8277b7v27h123pnwpyqqa9")))

