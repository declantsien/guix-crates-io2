(define-module (crates-io wa sm wasmium-hash-id) #:use-module (crates-io))

(define-public crate-wasmium-hash-id-1.0.0 (c (n "wasmium-hash-id") (v "1.0.0") (d (list (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "borsh") (r "^0.9.3") (d #t) (k 0)) (d (n "nanorand") (r "^0.7.0") (f (quote ("chacha"))) (o #t) (d #t) (k 0)) (d (n "tai64") (r "^4.0.0") (d #t) (k 0)))) (h "0730lk5vrv59gi5nc2w5zbdm2wka96a33i1ywz8174074rklqr4d") (f (quote (("random_id" "nanorand"))))))

