(define-module (crates-io wa sm wasmots-client) #:use-module (crates-io))

(define-public crate-wasmots-client-0.1.0 (c (n "wasmots-client") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "ureq") (r "^2.0") (d #t) (k 0)) (d (n "wasmer") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-wasi") (r "^1.0") (d #t) (k 0)))) (h "1m30cm07n0xmvgc8mrh6k1fh5cf84j4ccxiznz1q1jfaxxzmf52l")))

