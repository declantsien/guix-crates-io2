(define-module (crates-io wa sm wasm-wrapper-gen) #:use-module (crates-io))

(define-public crate-wasm-wrapper-gen-0.0.1 (c (n "wasm-wrapper-gen") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "wasm-wrapper-gen-impl") (r "^0.0.1") (d #t) (k 0)))) (h "13agbsa65admvjb2issbgrxy5ak147igjxkc6qa41xml6gn3ldsl")))

(define-public crate-wasm-wrapper-gen-0.0.2 (c (n "wasm-wrapper-gen") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "wasm-wrapper-gen-impl") (r "^0.0.2") (d #t) (k 0)))) (h "1mjrkx8nl86xdpp46qgsgm5k6nfpmmn3bi5p5qr009m872kq5qz6")))

(define-public crate-wasm-wrapper-gen-0.0.3 (c (n "wasm-wrapper-gen") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)) (d (n "wasm-wrapper-gen-impl") (r "^0.0.3") (d #t) (k 0)))) (h "09992kql0zc3xk185g231a9p4dg1svd0898dwv5zi489s4dc3l9h")))

