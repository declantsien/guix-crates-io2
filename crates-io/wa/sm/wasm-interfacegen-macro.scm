(define-module (crates-io wa sm wasm-interfacegen-macro) #:use-module (crates-io))

(define-public crate-wasm-interfacegen-macro-0.1.0 (c (n "wasm-interfacegen-macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.92") (f (quote ("extra-traits" "printing"))) (d #t) (k 0)))) (h "1nnr2fxvpckksxr7qh90jdk9ld2ilrar7gihzygnm10gsw10jdvi")))

