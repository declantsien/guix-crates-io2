(define-module (crates-io wa sm wasmio-aws-types) #:use-module (crates-io))

(define-public crate-wasmio-aws-types-0.0.1 (c (n "wasmio-aws-types") (v "0.0.1") (d (list (d (n "axum") (r "=0.7") (f (quote ("tokio" "json"))) (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "=1.35") (k 0)))) (h "05xljsyv2sjcf92v5jddwzqyjs8416mchlq24cgf4ic11zpakp82")))

(define-public crate-wasmio-aws-types-0.0.2 (c (n "wasmio-aws-types") (v "0.0.2") (d (list (d (n "axum") (r "=0.7") (f (quote ("tokio" "json"))) (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.13") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "=1.35") (k 0)))) (h "0nh7nk0xrxd64xd0wqz06fzmznd3d73ikkh1p02jj9gqwch2w4h4")))

