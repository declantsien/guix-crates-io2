(define-module (crates-io wa sm wasm-feature-detect-sys) #:use-module (crates-io))

(define-public crate-wasm-feature-detect-sys-0.1.0 (c (n "wasm-feature-detect-sys") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.32") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.55") (f (quote ("strict-macro"))) (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.5") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.5") (d #t) (k 2)))) (h "1ii1wk2rn1szx9488azxz0qpxpxp1j85zxqkqnddp3bnzp9sjlcz")))

