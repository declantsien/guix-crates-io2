(define-module (crates-io wa sm wasmbuild) #:use-module (crates-io))

(define-public crate-wasmbuild-0.0.0 (c (n "wasmbuild") (v "0.0.0") (d (list (d (n "wasm-bindgen") (r "=0.2.81") (d #t) (k 0)))) (h "1crv0rdx4knmq4d8vhlqvwl7yf60f9l8yxz60nx4cjrb31xx6a8h")))

(define-public crate-wasmbuild-0.0.2 (c (n "wasmbuild") (v "0.0.2") (d (list (d (n "wasm-bindgen") (r "=0.2.81") (d #t) (k 0)))) (h "0v0s470dqhym6gjaiw7igqz1s1gr763ajs6rgwmmjpb7kz0zyq3s")))

(define-public crate-wasmbuild-0.0.3 (c (n "wasmbuild") (v "0.0.3") (d (list (d (n "wasm-bindgen") (r "=0.2.81") (d #t) (k 0)))) (h "0v157ax98fh4swk8c8f68sh5bm30hlcf4zs83sysmja63z9c05sg")))

