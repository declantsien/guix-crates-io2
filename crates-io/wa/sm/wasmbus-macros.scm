(define-module (crates-io wa sm wasmbus-macros) #:use-module (crates-io))

(define-public crate-wasmbus-macros-0.1.0 (c (n "wasmbus-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hmbsbwcwwncvix63ydw9f2gh4im5hxbwynplsbz3p2p63c3g73b")))

(define-public crate-wasmbus-macros-0.1.1 (c (n "wasmbus-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1vl687mrfq0aygp93q6fkvxsw7sk5sq4gmfhfqlbavmwcldxag3n")))

(define-public crate-wasmbus-macros-0.1.2 (c (n "wasmbus-macros") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0hg8pad7f5lzzhl9c0y0ra9mhlzijnf0a8cy8vr77375ff25988f")))

(define-public crate-wasmbus-macros-0.1.3 (c (n "wasmbus-macros") (v "0.1.3") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1i4pf82ibs2bwjnrb1y0lrfn071glkwr6naav9l6icfy50jfgcbq")))

(define-public crate-wasmbus-macros-0.1.4 (c (n "wasmbus-macros") (v "0.1.4") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "000bss8kvvzvxnv1dkr0mdnaafc5py4f9lwz0bkn25ag3v84m4s1")))

(define-public crate-wasmbus-macros-0.1.5 (c (n "wasmbus-macros") (v "0.1.5") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "099ic3pzlls0nz889iprhmwf7gpggyxl08z1bj2naj64mcldrz22")))

(define-public crate-wasmbus-macros-0.1.6 (c (n "wasmbus-macros") (v "0.1.6") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0gga2f912s1ni3l32iw6ms553giq4pmdwppmasyyh95r2gmaswg6")))

(define-public crate-wasmbus-macros-0.1.7 (c (n "wasmbus-macros") (v "0.1.7") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10jij2m7kz4plqzm0vkxd18fxj0ybq0fypb7rxk87q6ihbdh6r1y")))

(define-public crate-wasmbus-macros-0.1.8 (c (n "wasmbus-macros") (v "0.1.8") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1fk1kzqpzdmwybq808plwjk7bqf5yyb4rh4z41bjp8gfxp4a6k65")))

(define-public crate-wasmbus-macros-0.1.9 (c (n "wasmbus-macros") (v "0.1.9") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "057wklfp7gxd99yacxl8mpijbxvd47gg7y6g1gfz2m8h9wv4cidc")))

(define-public crate-wasmbus-macros-0.1.10 (c (n "wasmbus-macros") (v "0.1.10") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xmjrnabs45ivd73qackw1jqh99363yqxpf8vi7v2dmr1cbnqarh")))

(define-public crate-wasmbus-macros-0.1.11 (c (n "wasmbus-macros") (v "0.1.11") (d (list (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05y8k9wvnkxmknigr00biz258s80c3pq82p03nagwcwj99c394d2")))

