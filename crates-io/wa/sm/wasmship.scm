(define-module (crates-io wa sm wasmship) #:use-module (crates-io))

(define-public crate-wasmship-0.0.0 (c (n "wasmship") (v "0.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "wasmtime") (r "^0.31.0") (o #t) (d #t) (k 0)))) (h "1dhw7wrhvbgb9hbgsbss1qq4s2nb3p5b06x0qhcfcknmrr7n0nli") (f (quote (("rt_wasmtime" "wasmtime") ("default" "rt_wasmtime")))) (y #t)))

