(define-module (crates-io wa sm wasm4fun-logo) #:use-module (crates-io))

(define-public crate-wasm4fun-logo-0.1.0 (c (n "wasm4fun-logo") (v "0.1.0") (d (list (d (n "wasm4fun-graphics") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-sound") (r "^0.1.0") (d #t) (k 0)))) (h "051c05jy226p3d0lk43bsl8fd3ir2gabwfv2yjgg05rqlkv9fmdw") (f (quote (("nosound") ("default"))))))

