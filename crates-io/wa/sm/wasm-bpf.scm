(define-module (crates-io wa sm wasm-bpf) #:use-module (crates-io))

(define-public crate-wasm-bpf-0.1.0 (c (n "wasm-bpf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flexi_logger") (r "^0.25.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wasm-bpf-rs") (r "^0.2.1") (d #t) (k 0)))) (h "03pzqfb3ip8gnjrk9bfq8kgmll0zqpigwpksvgmxl1dpd8vcagjs")))

(define-public crate-wasm-bpf-0.2.0 (c (n "wasm-bpf") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "flexi_logger") (r "^0.25.1") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wasm-bpf-rs") (r "^0.3.1") (d #t) (k 0)))) (h "10754pfli2j33mz6awlh5pv30qjfp74i3vfkw78sfbxz5a90kspq")))

