(define-module (crates-io wa sm wasmbind-monaco) #:use-module (crates-io))

(define-public crate-wasmbind-monaco-0.1.0 (c (n "wasmbind-monaco") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlElement"))) (d #t) (k 0)))) (h "18zywl6j60492aszcgw9vr9b8jzr87j3fdfsaa4y53rgpij043sr")))

(define-public crate-wasmbind-monaco-0.2.0 (c (n "wasmbind-monaco") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 2)) (d (n "web-sys") (r "^0.3") (f (quote ("HtmlElement"))) (d #t) (k 0)))) (h "0nsxfbcqh9av7dvc60vzqss57nlmjwzjd8ispbybcbklskn7qarl")))

