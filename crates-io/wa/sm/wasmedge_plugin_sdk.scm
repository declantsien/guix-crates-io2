(define-module (crates-io wa sm wasmedge_plugin_sdk) #:use-module (crates-io))

(define-public crate-wasmedge_plugin_sdk-0.1.0 (c (n "wasmedge_plugin_sdk") (v "0.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmedge_sys_ffi") (r "^0.11.2") (d #t) (k 0)))) (h "1awi8xnncifd4vvrxbz4c34xib85f063b41iq4na2axk3z2jxbaj") (f (quote (("wasi") ("plugin") ("embedded" "wasi") ("default" "plugin"))))))

(define-public crate-wasmedge_plugin_sdk-0.2.0 (c (n "wasmedge_plugin_sdk") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmedge_sys_ffi") (r "^0.13.0") (d #t) (k 0)))) (h "0rcrk25lmir9afza0yraz5p18v61xzzj19fdv8jff8wq75cnfzxg") (f (quote (("wasi") ("plugin") ("default" "plugin"))))))

