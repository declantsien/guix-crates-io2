(define-module (crates-io wa sm wasm-message-passing-3nweb) #:use-module (crates-io))

(define-public crate-wasm-message-passing-3nweb-0.1.0 (c (n "wasm-message-passing-3nweb") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)))) (h "142c8g763fmgqk1n5yhc2zyh3s7r2nki0pk3rz6fh9w1d7h7qkxb")))

(define-public crate-wasm-message-passing-3nweb-0.1.1 (c (n "wasm-message-passing-3nweb") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)))) (h "05l4sxb096g0il0gk5am5wn9gl0ai9nh24lryd8gcj9ij0v7zdfg")))

(define-public crate-wasm-message-passing-3nweb-0.1.2 (c (n "wasm-message-passing-3nweb") (v "0.1.2") (d (list (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)))) (h "0ksy7sib6njzkk5zrkb660nwxf8zr2gnbin62icc2nq0ih92jk63")))

(define-public crate-wasm-message-passing-3nweb-0.2.0 (c (n "wasm-message-passing-3nweb") (v "0.2.0") (d (list (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)))) (h "1djld2pi7k8gjq866b70d8i1knp2np8hqf90kzx688jdamhab9li")))

