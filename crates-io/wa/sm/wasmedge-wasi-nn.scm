(define-module (crates-io wa sm wasmedge-wasi-nn) #:use-module (crates-io))

(define-public crate-wasmedge-wasi-nn-0.7.0 (c (n "wasmedge-wasi-nn") (v "0.7.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0bbxjv7ygdwh4600f9ymg4p5h2cz1zpdwbgd79rf98pqwsmfn732")))

(define-public crate-wasmedge-wasi-nn-0.7.1 (c (n "wasmedge-wasi-nn") (v "0.7.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1k1n4gacgh8l0g7z6qs1hh8gynwf3i569f4g9iw3hbj0ay3n122c")))

