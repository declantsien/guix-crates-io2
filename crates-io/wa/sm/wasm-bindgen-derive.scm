(define-module (crates-io wa sm wasm-bindgen-derive) #:use-module (crates-io))

(define-public crate-wasm-bindgen-derive-0.1.0 (c (n "wasm-bindgen-derive") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.51") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1pn44dgpv1gb4y100h66jm3aql2v74rxra9mjmkpng814w0pxr5q")))

(define-public crate-wasm-bindgen-derive-0.1.1 (c (n "wasm-bindgen-derive") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.51") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.79") (d #t) (k 0)))) (h "0da4qgzaaj1n768s9h8l0sgslx2p77m5a9hhglmsgnhfjf31hc67")))

(define-public crate-wasm-bindgen-derive-0.2.0 (c (n "wasm-bindgen-derive") (v "0.2.0") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.85") (d #t) (k 0)))) (h "147vhwrhvfs2072jgc4ifwqnd94qnvif3c52adq0sfhnzx9i6id3") (r "1.65.0")))

(define-public crate-wasm-bindgen-derive-0.2.1 (c (n "wasm-bindgen-derive") (v "0.2.1") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.85") (d #t) (k 0)) (d (n "wasm-bindgen-derive-macro") (r "^0.2.1") (d #t) (k 0)))) (h "0z8pvxxrfdpk73pfmdma5y8dcdvknlzjh48jg229by5kzy5nray1") (r "1.65.0")))

(define-public crate-wasm-bindgen-derive-0.3.0 (c (n "wasm-bindgen-derive") (v "0.3.0") (d (list (d (n "js-sys") (r "^0.3.55") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.85") (d #t) (k 0)) (d (n "wasm-bindgen-derive-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0fn98bvyzpaihkl82c4b7z8kanqki4f32z168dyv5nh8fjwgvf4a") (r "1.74.0")))

