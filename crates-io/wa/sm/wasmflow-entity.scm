(define-module (crates-io wa sm wasmflow-entity) #:use-module (crates-io))

(define-public crate-wasmflow-entity-0.10.0-beta.0 (c (n "wasmflow-entity") (v "0.10.0-beta.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "wasmflow-macros") (r "^0.10.0-beta.0") (d #t) (k 0)))) (h "0a57k0rjxhzcs2yapmdlhmh50zwxfhx2baffpfkyc7zr8q549v36")))

(define-public crate-wasmflow-entity-0.10.0-beta.4 (c (n "wasmflow-entity") (v "0.10.0-beta.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "wasmflow-macros") (r "^0.10.0-beta.4") (d #t) (k 0)))) (h "1s2bp0j4yx5w01c9xk4rybvv5vslpx8l91jvn1np3vqhlys7b0nv")))

(define-public crate-wasmflow-entity-0.10.0 (c (n "wasmflow-entity") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "wasmflow-macros") (r "^0.10.0-beta.4") (d #t) (k 0)))) (h "1rrvsxgd4ycl9l8580q166227vcszd2mszrv14494y5fnmqflkjd")))

