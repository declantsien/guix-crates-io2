(define-module (crates-io wa sm wasm_sync) #:use-module (crates-io))

(define-public crate-wasm_sync-0.1.0 (c (n "wasm_sync") (v "0.1.0") (d (list (d (n "web-sys") (r "^0.3.24") (f (quote ("Window"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "0vzp1hs5iidqrwq2k0bc9rrbfb5x5hlkgpm81hsk8n3cab1b68jg")))

(define-public crate-wasm_sync-0.1.1 (c (n "wasm_sync") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.67") (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.24") (f (quote ("Window"))) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1nxrsriilxfx6bzhq97jraaiyp349blrpcn7xqlb0b0h0bra5717") (y #t)))

(define-public crate-wasm_sync-0.1.2 (c (n "wasm_sync") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3.67") (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2.90") (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3.24") (f (quote ("Window"))) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1smhpgk5jiir089y5w6y2rgqq9aqzrbxlb4x1vzl3v3zvv561wyg")))

