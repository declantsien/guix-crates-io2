(define-module (crates-io wa sm wasm-bus-macros) #:use-module (crates-io))

(define-public crate-wasm-bus-macros-0.1.0 (c (n "wasm-bus-macros") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)) (d (n "wasm-bus-types") (r "^0.1") (d #t) (k 0)))) (h "1zc9vij0ymv21anv5f8rj0fjhnribagrkn56p36zb5jxhlwa6vwr")))

(define-public crate-wasm-bus-macros-1.0.0 (c (n "wasm-bus-macros") (v "1.0.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)) (d (n "wasm-bus-types") (r "^1") (d #t) (k 0)))) (h "082ifi6n7wacg324f898ifypcpvkx8l4388kvgni0qh7mlwi0lvz")))

(define-public crate-wasm-bus-macros-1.1.0 (c (n "wasm-bus-macros") (v "1.1.0") (d (list (d (n "convert_case") (r "^0.5") (d #t) (k 0)) (d (n "derivative") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "parsing" "printing"))) (d #t) (k 0)) (d (n "wasm-bus-types") (r "^1") (d #t) (k 0)))) (h "042ka7zd156sn3wrlfr215lpkpakqf2ndhrq1w305lcqqsw4ljfh")))

