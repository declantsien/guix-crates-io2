(define-module (crates-io wa sm wasm_terminal_2048) #:use-module (crates-io))

(define-public crate-wasm_terminal_2048-0.1.0 (c (n "wasm_terminal_2048") (v "0.1.0") (d (list (d (n "getrandom") (r "^0.2.0") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.0") (f (quote ("std_rng"))) (d #t) (k 0)) (d (n "transpose") (r "^0.2.0") (d #t) (k 0)))) (h "1sypwki07b3qxz0c9rv1dvlbb2avr5fw8ydgz5zh3a3kj0a03m0d")))

