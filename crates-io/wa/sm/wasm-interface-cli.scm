(define-module (crates-io wa sm wasm-interface-cli) #:use-module (crates-io))

(define-public crate-wasm-interface-cli-0.0.0 (c (n "wasm-interface-cli") (v "0.0.0") (d (list (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-interface") (r "^0.0") (d #t) (k 0)))) (h "0krvgg4gdqfcmw0398lqzwhxc6k0ib7ffy62d9sm2qzsywsl233z")))

(define-public crate-wasm-interface-cli-0.0.1 (c (n "wasm-interface-cli") (v "0.0.1") (d (list (d (n "structopt") (r "^0.2.15") (d #t) (k 0)) (d (n "wasm-interface") (r "^0.0") (d #t) (k 0)))) (h "1ypxnkb2gsiarz0nv2znvwh0caljrgn28dz1pyn03p3lli5zcpfk")))

