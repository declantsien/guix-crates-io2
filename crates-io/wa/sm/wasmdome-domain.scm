(define-module (crates-io wa sm wasmdome-domain) #:use-module (crates-io))

(define-public crate-wasmdome-domain-0.0.1 (c (n "wasmdome-domain") (v "0.0.1") (d (list (d (n "eventsourcing") (r "^0.1.4") (d #t) (k 0)) (d (n "eventsourcing-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "122r32dy3bzgvw7i3knw3z40nzzm3l9gj30w5h5i8dwzifbz52vs")))

(define-public crate-wasmdome-domain-0.0.2 (c (n "wasmdome-domain") (v "0.0.2") (d (list (d (n "eventsourcing") (r "^0.1.4") (d #t) (k 0)) (d (n "eventsourcing-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vm0xzcd7mvp4df2pg40hs52bjf9ggbdxbki1vr0b63lp3c0vsgp")))

(define-public crate-wasmdome-domain-0.0.3 (c (n "wasmdome-domain") (v "0.0.3") (d (list (d (n "eventsourcing") (r "^0.1.5") (d #t) (k 0)) (d (n "eventsourcing-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "06jdliisqnj6d5jnqliqxlvg92hs8rflwmizwk2g0wqybmlidzhg")))

(define-public crate-wasmdome-domain-0.0.4 (c (n "wasmdome-domain") (v "0.0.4") (d (list (d (n "eventsourcing") (r "^0.1.5") (d #t) (k 0)) (d (n "eventsourcing-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0bd1in5lc7rngbxgfabxm10kfkc342rsb0yp4hij4jd7m0ybw6n5")))

(define-public crate-wasmdome-domain-0.0.6 (c (n "wasmdome-domain") (v "0.0.6") (d (list (d (n "eventsourcing") (r "^0.1.5") (d #t) (k 0)) (d (n "eventsourcing-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1cd346x5qbn6bz1v3mvqkzvwl8nfkgcqfbbavfmsii5jg45l17wv")))

(define-public crate-wasmdome-domain-0.0.7 (c (n "wasmdome-domain") (v "0.0.7") (d (list (d (n "eventsourcing") (r "^0.1.5") (d #t) (k 0)) (d (n "eventsourcing-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ldafh949na2bxyhhwi9qcpkwrbrfqzgs8hipqpjh30f0apwibs5")))

