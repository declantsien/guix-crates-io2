(define-module (crates-io wa sm wasm-bus-types) #:use-module (crates-io))

(define-public crate-wasm-bus-types-0.1.0 (c (n "wasm-bus-types") (v "0.1.0") (h "01zik84m2f1jfrqs9922h4i032b6g4vqw0y1n8w5h59gan6y4z20")))

(define-public crate-wasm-bus-types-1.0.0 (c (n "wasm-bus-types") (v "1.0.0") (h "0v8472jb5dyj491xginddlqispmg739dww647p6cm0qdiaazyj8x")))

(define-public crate-wasm-bus-types-1.1.0 (c (n "wasm-bus-types") (v "1.1.0") (d (list (d (n "bincode") (r "^1") (o #t) (d #t) (k 0)) (d (n "rkyv") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1s4ilk2wqgvsclkyh12n22jyfgsqs5rqfxl8jaxk4h22dhazsp1v") (f (quote (("enable_yaml" "serde_yaml") ("enable_xml" "serde-xml-rs") ("enable_rkyv" "rkyv") ("enable_mpack" "rmp-serde") ("enable_json" "serde_json") ("enable_bincode" "bincode") ("default" "enable_bincode" "enable_mpack" "enable_json" "enable_yaml" "enable_xml"))))))

