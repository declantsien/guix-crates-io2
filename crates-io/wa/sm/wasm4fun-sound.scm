(define-module (crates-io wa sm wasm4fun-sound) #:use-module (crates-io))

(define-public crate-wasm4fun-sound-0.1.0 (c (n "wasm4fun-sound") (v "0.1.0") (d (list (d (n "wasm4fun-core") (r "^0.1.0") (d #t) (k 0)))) (h "10w9rhcpkyl13iwlfwza9fq6c6gcwmkpw18y9ad1cnzz9yhqm4w4") (f (quote (("nosound") ("default"))))))

