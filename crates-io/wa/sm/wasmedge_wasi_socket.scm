(define-module (crates-io wa sm wasmedge_wasi_socket) #:use-module (crates-io))

(define-public crate-wasmedge_wasi_socket-0.1.0 (c (n "wasmedge_wasi_socket") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.3") (d #t) (k 0)))) (h "1gz1iv2q5vrvvhz5cv0fnl9wfgsb4vkib4wn682a2kin9hj08p17") (f (quote (("std"))))))

(define-public crate-wasmedge_wasi_socket-0.2.0 (c (n "wasmedge_wasi_socket") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.3") (d #t) (k 0)))) (h "132fyawrzm1qfwbd7yg05a5wih3aaxvh46h7ikhkhz23qsl846pv") (f (quote (("std"))))))

(define-public crate-wasmedge_wasi_socket-0.3.0 (c (n "wasmedge_wasi_socket") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.3") (d #t) (k 0)))) (h "1nhwy2kzmqpxap6p5mkvvsjr6zvr70bpf44616anv5qhq9f865xw") (f (quote (("std")))) (y #t)))

(define-public crate-wasmedge_wasi_socket-0.3.1 (c (n "wasmedge_wasi_socket") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.3") (d #t) (k 0)))) (h "1mb41dayvdpmgij5fldyka404nnbsyn3345p3m1qhs7bcgk0ywfn") (y #t)))

(define-public crate-wasmedge_wasi_socket-0.3.2 (c (n "wasmedge_wasi_socket") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.3") (d #t) (k 0)))) (h "0ikfymp80nb2b6qmc0maksszpjgwkh2wg959a06y78v09zfz71ni")))

(define-public crate-wasmedge_wasi_socket-0.3.3 (c (n "wasmedge_wasi_socket") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.3") (d #t) (k 0)))) (h "0i034l42ps7w740wdrmpnsmd2sk5znmvs4fb43mw54n3hjap6hgb")))

(define-public crate-wasmedge_wasi_socket-0.4.0 (c (n "wasmedge_wasi_socket") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "libc") (r "^0.2.3") (d #t) (k 0)) (d (n "tokio") (r "^1.18.1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "0gbs41n8q66ska0rw2m4v6w7ln7r3dvr3klbrllx5dr0xywgc6bf")))

(define-public crate-wasmedge_wasi_socket-0.4.1 (c (n "wasmedge_wasi_socket") (v "0.4.1") (d (list (d (n "libc") (r "^0.2.3") (d #t) (k 0)))) (h "13r68xf80y3k3ixvmydcyhllprdfrg0n3vim7markk8jxj8b6dxr") (f (quote (("wasmedge_0_9") ("wasmedge_0_10") ("default" "wasmedge_0_10"))))))

(define-public crate-wasmedge_wasi_socket-0.4.2 (c (n "wasmedge_wasi_socket") (v "0.4.2") (d (list (d (n "libc") (r "^0.2.3") (d #t) (k 0)))) (h "1gq8nq6abv4bh0by8pz8fdk4c1pndvsx2rcvsbiw5qnp14qfki1g") (f (quote (("wasmedge_0_9") ("wasmedge_0_10") ("wasi_poll") ("default" "wasmedge_0_10"))))))

(define-public crate-wasmedge_wasi_socket-0.4.3 (c (n "wasmedge_wasi_socket") (v "0.4.3") (d (list (d (n "libc") (r "^0.2.3") (d #t) (k 0)))) (h "17r645sdfablg8an2ij54m4f406na75fp71xp6fxlsir3cf19lff") (f (quote (("wasmedge_0_9") ("wasmedge_0_10") ("wasi_poll") ("epoll") ("default" "wasmedge_0_10"))))))

(define-public crate-wasmedge_wasi_socket-0.5.0 (c (n "wasmedge_wasi_socket") (v "0.5.0") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0ka84zhqjnjmg36j852pqvcbyak94md1bvnmki69s7fddjhpiv4i") (f (quote (("wasmedge_0_12") ("wasi_poll") ("epoll") ("default" "built-in-dns" "wasmedge_0_12") ("built-in-dns"))))))

(define-public crate-wasmedge_wasi_socket-0.5.1 (c (n "wasmedge_wasi_socket") (v "0.5.1") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "07xr3ip8cpnvkmh7lhl9wl6jm3z4nd9ki0ywrg6fzx8fd4blq2lh") (f (quote (("wasmedge_0_12") ("wasi_poll") ("epoll") ("default" "built-in-dns" "wasmedge_0_12") ("built-in-dns"))))))

(define-public crate-wasmedge_wasi_socket-0.5.2 (c (n "wasmedge_wasi_socket") (v "0.5.2") (d (list (d (n "bytes") (r "^1.4.0") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g1fcqc1lmncj4ajrs0xwhcxdh3g3l7n7s8a99cpaxjdjv3p0chg") (f (quote (("wasmedge_0_12") ("wasi_poll") ("epoll") ("default" "wasmedge_0_12") ("built-in-dns"))))))

(define-public crate-wasmedge_wasi_socket-0.5.3 (c (n "wasmedge_wasi_socket") (v "0.5.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1d1hjl88pya2b6z54rafwmdjgnp4yrq77xjvkp5jjaaswfbqgd3p") (f (quote (("wasmedge_0_12") ("wasi_poll") ("epoll") ("default" "wasmedge_0_12") ("built-in-dns"))))))

(define-public crate-wasmedge_wasi_socket-0.5.4 (c (n "wasmedge_wasi_socket") (v "0.5.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "dns-parser") (r "^0.8.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0a8797sychnnz6ifmrh2k7037amych358kib07xl11brd0h14izc") (f (quote (("wasi_poll") ("epoll") ("default"))))))

