(define-module (crates-io wa sm wasmer-cache-asml-fork) #:use-module (crates-io))

(define-public crate-wasmer-cache-asml-fork-1.0.2 (c (n "wasmer-cache-asml-fork") (v "1.0.2") (d (list (d (n "blake3") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^1.0.2") (k 0) (p "wasmer-asml-fork")))) (h "01nsgcx7bb4ai46wbfn46izpprf0f213061p9md9bwzicf7b4vyl")))

