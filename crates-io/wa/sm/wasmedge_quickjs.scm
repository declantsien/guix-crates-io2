(define-module (crates-io wa sm wasmedge_quickjs) #:use-module (crates-io))

(define-public crate-wasmedge_quickjs-0.2.0 (c (n "wasmedge_quickjs") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.2") (d #t) (k 0)) (d (n "image") (r "^0.23.0") (f (quote ("jpeg" "png"))) (k 0)) (d (n "imageproc") (r "^0.22.0") (d #t) (k 0)) (d (n "wasmedge_http_req") (r "^0.8.1") (d #t) (k 0)))) (h "15bgjr76idnf3n0jg7q39cn9ninql4bhz3llxnjq00rzhdgzym5h") (f (quote (("tensorflow" "img") ("img") ("http") ("default" "http"))))))

