(define-module (crates-io wa sm wasm-bindgen-downcast-macros) #:use-module (crates-io))

(define-public crate-wasm-bindgen-downcast-macros-0.1.0 (c (n "wasm-bindgen-downcast-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1r2dqg117km4nyh1hkbkjryy5ir0fp3997fk0nz2csr1q167crkr") (r "1.64")))

(define-public crate-wasm-bindgen-downcast-macros-0.1.1 (c (n "wasm-bindgen-downcast-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "13zk8l5qligf53ah2my2gqn15z61wd25s18qy7zcxkn7hzx0q0n5") (r "1.63")))

