(define-module (crates-io wa sm wasm4fun) #:use-module (crates-io))

(define-public crate-wasm4fun-0.1.0 (c (n "wasm4fun") (v "0.1.0") (d (list (d (n "wasm4fun-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-fmt") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-graphics") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-input") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-log") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-logo") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-panichandler") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-random") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-sound") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-storage") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-time") (r "^0.1.0") (d #t) (k 0)))) (h "1l78q0vw40d87x8c8cbm7qwrdpa1la1ll0i1b10pdib94hd9hkg3")))

