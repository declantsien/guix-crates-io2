(define-module (crates-io wa sm wasm-bindgen-test-crate-b) #:use-module (crates-io))

(define-public crate-wasm-bindgen-test-crate-b-0.1.0 (c (n "wasm-bindgen-test-crate-b") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "16p3gx9vhngdf236zxx2qijqx5sq0lid25j8wy6j522ybxs4vbh8")))

