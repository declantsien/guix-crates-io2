(define-module (crates-io wa sm wasm_keyboard) #:use-module (crates-io))

(define-public crate-wasm_keyboard-0.1.0 (c (n "wasm_keyboard") (v "0.1.0") (d (list (d (n "uievents-code") (r "^0.1.2") (f (quote ("enum"))) (d #t) (k 0)) (d (n "wasm_keyboard_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("KeyboardEvent"))) (d #t) (k 0)))) (h "0sxyvaqxwsbqiy4g9w73l3zvc7x6kb59gk3nid9nzim5qkdavvpg") (f (quote (("keypress" "wasm_keyboard_macros/keypress")))) (r "1.56.1")))

(define-public crate-wasm_keyboard-0.1.1 (c (n "wasm_keyboard") (v "0.1.1") (d (list (d (n "uievents-code") (r "^0.1.2") (f (quote ("enum"))) (d #t) (k 0)) (d (n "wasm_keyboard_macros") (r "^0.1") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.61") (f (quote ("KeyboardEvent"))) (d #t) (k 0)))) (h "0mxaix9vpabfvgsa9wvynf9y2iansgqdna5b7ja40y83aj036gnd") (f (quote (("keypress" "wasm_keyboard_macros/keypress")))) (r "1.56.1")))

