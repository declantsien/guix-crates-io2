(define-module (crates-io wa sm wasm-run-proc-macro) #:use-module (crates-io))

(define-public crate-wasm-run-proc-macro-0.1.0 (c (n "wasm-run-proc-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "18gvycdg9vjldg13fnsa28nip7z2r3f6panz3ah2nc3n8kxhivz2") (f (quote (("serve"))))))

(define-public crate-wasm-run-proc-macro-0.2.1 (c (n "wasm-run-proc-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "0p03kh5c2mac1xhpw8lmych4r0qckwnc1mw48y7dsr8mfmrhz3hz") (f (quote (("serve"))))))

(define-public crate-wasm-run-proc-macro-0.3.0 (c (n "wasm-run-proc-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1xkcghzqkmrfwbhwz6nmmgl8xyrc3gq9xrnw3dhj03yd2q5cq27b") (f (quote (("serve"))))))

(define-public crate-wasm-run-proc-macro-0.4.0 (c (n "wasm-run-proc-macro") (v "0.4.0") (d (list (d (n "cargo_metadata") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "13xijby4h52a3zfc3qswra1zbknw4kq1aqhfhhrv0pkmnpdhw3xi") (f (quote (("serve"))))))

(define-public crate-wasm-run-proc-macro-0.5.0 (c (n "wasm-run-proc-macro") (v "0.5.0") (d (list (d (n "cargo_metadata") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "09s7ala7pbp0w1xp9k9krl6phna081y39ivpryk36yz5s8f1p684") (f (quote (("serve"))))))

(define-public crate-wasm-run-proc-macro-0.6.1 (c (n "wasm-run-proc-macro") (v "0.6.1") (d (list (d (n "cargo_metadata") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1vd6xxn8kv61h85cj2y8rnfp498x00mg6vys4f32z4fvd331j0qz") (f (quote (("serve"))))))

(define-public crate-wasm-run-proc-macro-0.7.0 (c (n "wasm-run-proc-macro") (v "0.7.0") (d (list (d (n "cargo_metadata") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "0vlhiwn6d13kxrla62r28pb21m7s1hjr4fsy2axilxk7wz5hg3jx") (f (quote (("serve"))))))

(define-public crate-wasm-run-proc-macro-0.8.0 (c (n "wasm-run-proc-macro") (v "0.8.0") (d (list (d (n "cargo_metadata") (r "^0.12.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "syn") (r "^1.0.53") (f (quote ("full"))) (d #t) (k 0)))) (h "1r5g83jliq900m3z0nglxzrwf24f9x770fwqx792mzj3cv238grx") (f (quote (("serve"))))))

