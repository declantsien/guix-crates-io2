(define-module (crates-io wa sm wasm_membrane_host) #:use-module (crates-io))

(define-public crate-wasm_membrane_host-0.1.0 (c (n "wasm_membrane_host") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasmer") (r "^2.0.0") (d #t) (k 0)))) (h "0l97i5s6z4kp3cvsq0p9w6ccvyz711gzr1jwg5im1q9rh7c2zc2k")))

(define-public crate-wasm_membrane_host-0.2.0 (c (n "wasm_membrane_host") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasmer") (r "^2.1.1") (d #t) (k 0)))) (h "1r1qfwqs3aadm4hcq4cx1fjixqyzbaahrbqcn3xdfimhihvq5c4s")))

