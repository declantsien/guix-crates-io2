(define-module (crates-io wa sm wasmtime-vfs-file) #:use-module (crates-io))

(define-public crate-wasmtime-vfs-file-0.1.0 (c (n "wasmtime-vfs-file") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "wasi-common") (r "^2.0.0") (d #t) (k 0)) (d (n "wasmtime-vfs-ledger") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmtime-vfs-memory") (r "^0.1.0") (d #t) (k 0)))) (h "017sdamnwchapnc1rwy0xqbjndxby2kzh7m8hrj3abjx1198njlh")))

