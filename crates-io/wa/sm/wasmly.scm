(define-module (crates-io wa sm wasmly) #:use-module (crates-io))

(define-public crate-wasmly-0.0.1 (c (n "wasmly") (v "0.0.1") (h "1m973gdi83li5d48s8z1dz8b4sjyw2nmg15zsfmswv5afqanz70p")))

(define-public crate-wasmly-0.0.2 (c (n "wasmly") (v "0.0.2") (h "02nbkcrpm5lhks3hh8cpfj9bv7rbnb5j7mf4bxzgp75blv4wns44")))

(define-public crate-wasmly-0.0.3 (c (n "wasmly") (v "0.0.3") (h "1y80q3m679niz3gxivxdcyj5x6lkq7dv7v0q2x7kpamxlz71aa6l")))

(define-public crate-wasmly-0.0.4 (c (n "wasmly") (v "0.0.4") (h "10vwxcyqdzhql4w7lvjidr08bnrkwj63v0k9arssvz9wdwmx00ap")))

(define-public crate-wasmly-0.0.5 (c (n "wasmly") (v "0.0.5") (h "0wnd9x7czxysrhp39qfrqx75jmfl4l9g2acp4rc6dfpgfw7dj7f8")))

(define-public crate-wasmly-0.0.6 (c (n "wasmly") (v "0.0.6") (h "0q1szfp22dxss437cb2vmb26hvb0r1qh0l14v8f7rfgqwk9m82hh")))

(define-public crate-wasmly-0.0.7 (c (n "wasmly") (v "0.0.7") (h "0kmfybissy8m1znmzcwb4hnjdljf81w7bh6wj7h1y3fs65wkada5")))

(define-public crate-wasmly-0.0.8 (c (n "wasmly") (v "0.0.8") (h "0qvk2m0bsvnjabq6kni5j2v5wgc6wcrmnf4kbbmivx9fypnkbmhj")))

(define-public crate-wasmly-0.0.9 (c (n "wasmly") (v "0.0.9") (h "0dgypvr9hb733b3lfq6wwxwiij8n5i8450h6lb83yriazpgnklmm")))

(define-public crate-wasmly-0.0.10 (c (n "wasmly") (v "0.0.10") (h "1iywjg7jrbhb0mcs250zyicaa128rabi98iz69lrqwkbb02d0ixj")))

(define-public crate-wasmly-0.0.11 (c (n "wasmly") (v "0.0.11") (h "0nlwr5zdfjvzy6jyl1ayq3dczcndryzmfmqh8jyx31m0xj605dfx")))

(define-public crate-wasmly-0.0.12 (c (n "wasmly") (v "0.0.12") (h "10kgzm65bmagf1901gp35glj5h80bx8kcfv3lxvvln6rsy2j5b5n")))

(define-public crate-wasmly-0.0.13 (c (n "wasmly") (v "0.0.13") (h "14spk06wax182pxm19248s23qs1qqh3367y91mjciyw8q58370a4")))

(define-public crate-wasmly-0.1.0 (c (n "wasmly") (v "0.1.0") (h "1m82qlsv3jpggmfk7dynnz9infwdc69869jrnxhgxzvx4gxim7qz")))

(define-public crate-wasmly-0.1.1 (c (n "wasmly") (v "0.1.1") (h "0vvsvpqlgydqdxsb4qaaw9nf7cf49a31437dblq0vpglym80n3q7")))

(define-public crate-wasmly-0.1.2 (c (n "wasmly") (v "0.1.2") (h "0y5lkb6sjs7mh47hhqpx8v3bwh6lfydj7y0fkpww4kqin00xb0i5")))

(define-public crate-wasmly-0.1.3 (c (n "wasmly") (v "0.1.3") (h "065x73qa6j4h68yhvgnm8qaani4rn992nn16m40193k3jkf3y3lm")))

(define-public crate-wasmly-0.1.4 (c (n "wasmly") (v "0.1.4") (h "0ilb5w4rh1dqr6iadfrsdqhzbb9nvrkhf3wh2fcsdl601ja5g3lh")))

(define-public crate-wasmly-0.1.5 (c (n "wasmly") (v "0.1.5") (h "0rgc5s279p57lkzbng6qbkrckl8zskz78jp7d16y3p6ilhj9aan5")))

(define-public crate-wasmly-0.1.6 (c (n "wasmly") (v "0.1.6") (h "1hzjg8q4hs35qxc0sv0hgcv2kh616kw784rxzak18w278rl8m8sh")))

(define-public crate-wasmly-0.1.7 (c (n "wasmly") (v "0.1.7") (h "0250fzbz02066rzb1ca308x7804j09sz441xvyjqlfw6l63vf0q6")))

(define-public crate-wasmly-0.2.0 (c (n "wasmly") (v "0.2.0") (h "15kzcag6bpi33i64k0vmq6jka0v42jjbqpyjf0wymzk364390948")))

