(define-module (crates-io wa sm wasmer-bus-time) #:use-module (crates-io))

(define-public crate-wasmer-bus-time-1.1.0 (c (n "wasmer-bus-time") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros" "time"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "wasmer-bus") (r "^1") (f (quote ("rt" "macros"))) (k 0)))) (h "1y210gdafdflb5l4x83569g578d2pxr3skj1nmsvac25crqh5wya") (f (quote (("default"))))))

