(define-module (crates-io wa sm wasm-dom) #:use-module (crates-io))

(define-public crate-wasm-dom-0.1.0 (c (n "wasm-dom") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "Element" "WebSocket" "Window"))) (d #t) (k 0)))) (h "019x53fz7gzpkq8plb85ky97py9m2f7vym4wq8ans8zv789ng5r2")))

(define-public crate-wasm-dom-1.0.0 (c (n "wasm-dom") (v "1.0.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Document" "Element" "WebSocket" "Window" "Location"))) (d #t) (k 0)))) (h "0298animlh9j1cd6gvjzqj00y7r37mmgvhp2nillbc8s9g5wwpbv")))

