(define-module (crates-io wa sm wasmut-wasm) #:use-module (crates-io))

(define-public crate-wasmut-wasm-0.46.0 (c (n "wasmut-wasm") (v "0.46.0") (d (list (d (n "time") (r "^0.3") (d #t) (k 2)))) (h "0yv1fbhj1w4pvddq9bn3cmvv8gxgk9g5szidw1d0ryxd86xh4nm6") (f (quote (("std") ("simd") ("sign_ext") ("reduced-stack-buffer") ("offsets") ("multi_value") ("default" "std") ("bulk") ("atomics")))) (r "1.56.1")))

