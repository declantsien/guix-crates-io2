(define-module (crates-io wa sm wasm-bus-webgl) #:use-module (crates-io))

(define-public crate-wasm-bus-webgl-1.0.0 (c (n "wasm-bus-webgl") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bus") (r "^1") (f (quote ("rt" "macros"))) (k 0)))) (h "00y1jwxx75qspsyw53nhjh6vwkcxw9ckq1mddinalavi74hdkaxv") (f (quote (("frontend" "wasm-bus/syscalls") ("default" "frontend") ("backend"))))))

(define-public crate-wasm-bus-webgl-1.0.1 (c (n "wasm-bus-webgl") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bus") (r "^1") (f (quote ("rt" "macros"))) (k 0)))) (h "0xzqfahi50p0a72d94hcjp4kbmcb33j7ai2sx3irv6wm4nkqyniy") (f (quote (("frontend" "wasm-bus/syscalls") ("default" "frontend") ("backend"))))))

(define-public crate-wasm-bus-webgl-1.0.2 (c (n "wasm-bus-webgl") (v "1.0.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bus") (r "^1") (f (quote ("rt" "macros"))) (k 0)))) (h "1kgw2wiv9sphw9wjzqv93iwrbvxrbzqhnfhzmanrxlbfqhmpm77g") (f (quote (("frontend" "wasm-bus/syscalls") ("default" "frontend") ("backend"))))))

(define-public crate-wasm-bus-webgl-1.0.3 (c (n "wasm-bus-webgl") (v "1.0.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bus") (r "^1") (f (quote ("rt" "macros"))) (k 0)))) (h "1bz97jl8xsl294gmwrbgfp8fg5vb851prxl6xqdyain9v65xnhm0") (f (quote (("frontend" "wasm-bus/syscalls") ("default" "frontend") ("backend"))))))

