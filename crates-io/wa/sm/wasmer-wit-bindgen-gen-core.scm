(define-module (crates-io wa sm wasmer-wit-bindgen-gen-core) #:use-module (crates-io))

(define-public crate-wasmer-wit-bindgen-gen-core-0.1.0 (c (n "wasmer-wit-bindgen-gen-core") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmer-wit-parser") (r "^0.1.0") (d #t) (k 0)))) (h "104c0jmjv9xpq1p2s80bpnzq0k0cqb7m1srjn5yn3il698cchkps")))

(define-public crate-wasmer-wit-bindgen-gen-core-0.1.1 (c (n "wasmer-wit-bindgen-gen-core") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmer-wit-parser") (r "^0.1.1") (d #t) (k 0)))) (h "1zjl8224fdbsxxjkn26jjn1y4gq90gkc1hhx2mg1zmp5bazab2pz")))

