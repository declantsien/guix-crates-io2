(define-module (crates-io wa sm wasm-module-name) #:use-module (crates-io))

(define-public crate-wasm-module-name-0.1.0 (c (n "wasm-module-name") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.38.0") (d #t) (k 0)))) (h "1qphpl0sjz2hj5c5z0w97lzl7y9n0llml8frd8ws2bsvphy7qk2f")))

(define-public crate-wasm-module-name-0.1.1 (c (n "wasm-module-name") (v "0.1.1") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.38.0") (d #t) (k 0)))) (h "18pd7pcfvgzccx29kzkfaqj8gvs1np42zpiy6m1rz0hx8y7mzpas")))

