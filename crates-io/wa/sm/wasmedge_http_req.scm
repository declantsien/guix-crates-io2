(define-module (crates-io wa sm wasmedge_http_req) #:use-module (crates-io))

(define-public crate-wasmedge_http_req-0.8.1 (c (n "wasmedge_http_req") (v "0.8.1") (d (list (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "wasmedge_wasi_socket") (r "^0.1.0") (d #t) (k 0)))) (h "01ix4z58c3vgnjgpl3s0rayh318d8f3157v9wiyj1nplnwkma5yx")))

(define-public crate-wasmedge_http_req-0.9.0 (c (n "wasmedge_http_req") (v "0.9.0") (d (list (d (n "unicase") (r "^2.6") (d #t) (k 0)) (d (n "wasmedge_wasi_socket") (r "^0.4.0") (d #t) (k 0)))) (h "1l5dc1r2jc45nizcd2d27y6kj9lsrs7zyzby06rz2lq2n7nr1pm0")))

