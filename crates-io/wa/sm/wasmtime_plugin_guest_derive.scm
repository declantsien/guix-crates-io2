(define-module (crates-io wa sm wasmtime_plugin_guest_derive) #:use-module (crates-io))

(define-public crate-wasmtime_plugin_guest_derive-0.1.0 (c (n "wasmtime_plugin_guest_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0jph5c29vyaqn5qgjphqa0xjg01fwzykdzyqdpknsdm58vr1cc6h")))

(define-public crate-wasmtime_plugin_guest_derive-0.1.1 (c (n "wasmtime_plugin_guest_derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ncgjmlgycnjxlldyjpgwwpxq1f3vp3j9hvwg8hj2yzrigrw2zgh")))

