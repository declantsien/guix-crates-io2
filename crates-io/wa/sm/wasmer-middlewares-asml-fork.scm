(define-module (crates-io wa sm wasmer-middlewares-asml-fork) #:use-module (crates-io))

(define-public crate-wasmer-middlewares-asml-fork-1.0.2 (c (n "wasmer-middlewares-asml-fork") (v "1.0.2") (d (list (d (n "wasmer") (r "^1.0.2") (d #t) (k 0) (p "wasmer-asml-fork")) (d (n "wasmer-types") (r "^1.0.2") (d #t) (k 0)) (d (n "wasmer-vm") (r "^1.0.2") (d #t) (k 0)))) (h "0i79gdkz1bdygifa0jhkfyqx3s791khsbzrnvm6cvkq1imq92j7h")))

