(define-module (crates-io wa sm wasm-snip) #:use-module (crates-io))

(define-public crate-wasm-snip-0.1.0 (c (n "wasm-snip") (v "0.1.0") (d (list (d (n "clap") (r "^2.29.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.19.0") (d #t) (k 0)))) (h "1nk0symbnc610d8h21lcnk96by6mnc8q0f04ll2hih5k8h91f9zb") (f (quote (("exe" "clap") ("default" "exe"))))))

(define-public crate-wasm-snip-0.1.1 (c (n "wasm-snip") (v "0.1.1") (d (list (d (n "clap") (r "^2.29.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.27.0") (d #t) (k 0)))) (h "09k3zh5pyz31n70zwy0vsbz8fkb6dp24hvjscm1djdgzdw056cji") (f (quote (("exe" "clap") ("default" "exe"))))))

(define-public crate-wasm-snip-0.1.2 (c (n "wasm-snip") (v "0.1.2") (d (list (d (n "clap") (r "^2.29.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.27.0") (d #t) (k 0)))) (h "1jn6gibb0haxavvjdj8a8r1wf5lw8rlgg55imib6k3x7v2nxk1hh") (f (quote (("exe" "clap") ("default" "exe"))))))

(define-public crate-wasm-snip-0.1.3 (c (n "wasm-snip") (v "0.1.3") (d (list (d (n "clap") (r "^2.29.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.27.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.10") (d #t) (k 0)))) (h "10iav4kpk4af3xag9fzzz4jssm5q28jjdag146m3kj0v75diahlq") (f (quote (("exe" "clap") ("default" "exe"))))))

(define-public crate-wasm-snip-0.1.4 (c (n "wasm-snip") (v "0.1.4") (d (list (d (n "assert_cmd") (r "^0.11.0") (d #t) (k 2)) (d (n "clap") (r "^2.32.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.4.0") (d #t) (k 0)))) (h "010nh9vkq60sgl3mdw8hdkabc7msnla68050443ink688lhri44y") (f (quote (("exe" "clap") ("default" "exe"))))))

(define-public crate-wasm-snip-0.1.5 (c (n "wasm-snip") (v "0.1.5") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.6") (d #t) (k 0)) (d (n "walrus") (r "^0.6.0") (d #t) (k 0)))) (h "0dasl1zja0kvzzl521gc7pqql76l2rdzx1167mqlv4fsmg2m5gq0") (f (quote (("exe" "clap") ("default" "exe"))))))

(define-public crate-wasm-snip-0.2.0 (c (n "wasm-snip") (v "0.2.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.0") (d #t) (k 0)) (d (n "walrus") (r "^0.8.0") (d #t) (k 0)))) (h "058m1yjydb6jh8pgwrdpf0855i33v1fk52drfiqya3vigjhqkxap") (f (quote (("exe" "clap") ("default" "exe"))))))

(define-public crate-wasm-snip-0.3.0 (c (n "wasm-snip") (v "0.3.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "walrus") (r "^0.11.0") (f (quote ("parallel"))) (d #t) (k 0)))) (h "1gl48q5f972y49n20aqnxxyfrxsmgqijnv030hfmw9q1fb4rc4ws") (f (quote (("exe" "clap") ("default" "exe"))))))

(define-public crate-wasm-snip-0.4.0 (c (n "wasm-snip") (v "0.4.0") (d (list (d (n "assert_cmd") (r "^0.11.1") (d #t) (k 2)) (d (n "clap") (r "^2.33.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "rayon") (r "^1.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "walrus") (r "^0.12.0") (f (quote ("parallel"))) (d #t) (k 0)))) (h "0i0w21ahg8x1nwqswkpzqh8g1ayxx5ypjgwvrm1gwp4pw26462v0") (f (quote (("exe" "clap") ("default" "exe"))))))

