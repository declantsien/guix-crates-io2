(define-module (crates-io wa sm wasm-componentize) #:use-module (crates-io))

(define-public crate-wasm-componentize-0.0.1 (c (n "wasm-componentize") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "clap") (r "^3.2.7") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.16.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.90.0") (d #t) (k 0)))) (h "1s4xr13vn3zrvchsh8bk0n6zw1lfp9c8l0q48f5kq5ba0yax9nzk") (f (quote (("default") ("cli" "clap"))))))

