(define-module (crates-io wa sm wasmedge-bindgen-host) #:use-module (crates-io))

(define-public crate-wasmedge-bindgen-host-0.1.0 (c (n "wasmedge-bindgen-host") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "wasmedge-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0i83b936pb8a45b37wz4mlb6wbdj5zizd9l1dzfxk5ds7rrf8bn5")))

(define-public crate-wasmedge-bindgen-host-0.1.1 (c (n "wasmedge-bindgen-host") (v "0.1.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "wasmedge-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1gkzy7iqvf3ak8c0fp9nidgfj9id93s8wwh0ah32sdg482f12wm7")))

(define-public crate-wasmedge-bindgen-host-0.2.0 (c (n "wasmedge-bindgen-host") (v "0.2.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "wasmedge-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmedge-types") (r "^0.1.3") (d #t) (k 0)))) (h "0pfxs78zp875cr449nqpdcxqa2aa63yymqqfnkk778rfcr5khbb6")))

(define-public crate-wasmedge-bindgen-host-0.2.1 (c (n "wasmedge-bindgen-host") (v "0.2.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "wasmedge-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmedge-types") (r "^0.1.3") (d #t) (k 0)))) (h "05qghwk107xbcxih1qmnidx0pya5r001cv7b79yh2v75f22gy0sw")))

(define-public crate-wasmedge-bindgen-host-0.3.0 (c (n "wasmedge-bindgen-host") (v "0.3.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "wasmedge-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmedge-types") (r "^0.1.3") (d #t) (k 0)))) (h "0n42hfb6vcbiah95vn0irs4fimg46njhb4f0wrik08085rq9xg6m")))

(define-public crate-wasmedge-bindgen-host-0.4.0 (c (n "wasmedge-bindgen-host") (v "0.4.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "wasmedge-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmedge-types") (r "^0.1.3") (d #t) (k 0)))) (h "0m5xh792dinnviv3va55iba92h1qfmfpgg2hrhiq47mhdvsjim1n")))

(define-public crate-wasmedge-bindgen-host-0.4.1 (c (n "wasmedge-bindgen-host") (v "0.4.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "wasmedge-sys") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmedge-types") (r "^0.1.3") (d #t) (k 0)))) (h "0bcw3s9ij6xzqdlvny2773qwyiad7d8nzpkh48zqxak7fwmxq7wk")))

