(define-module (crates-io wa sm wasm_bindgen_helper_macros) #:use-module (crates-io))

(define-public crate-wasm_bindgen_helper_macros-0.0.1 (c (n "wasm_bindgen_helper_macros") (v "0.0.1") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 2)))) (h "1ldj95lf8bzaylr3sxh4y73g2l83idsg6309j3cwz3rmfvkws25y")))

(define-public crate-wasm_bindgen_helper_macros-0.0.2 (c (n "wasm_bindgen_helper_macros") (v "0.0.2") (d (list (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 2)))) (h "1w0n9fkbv14ya17rxvdnz9l0zc3qfz9a8zn8i8m34czzafa5sraj")))

(define-public crate-wasm_bindgen_helper_macros-0.0.3 (c (n "wasm_bindgen_helper_macros") (v "0.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 2)))) (h "0srvxpsk4gmh2mq3byzcniqj1s25mad824i1jhlyky21l0hv7pdj") (y #t)))

(define-public crate-wasm_bindgen_helper_macros-0.0.4 (c (n "wasm_bindgen_helper_macros") (v "0.0.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 2)))) (h "1hwjhbscjjxq22x38rxj20xlg19w3vvkihdqlcxb4lh3jcfqyil8")))

