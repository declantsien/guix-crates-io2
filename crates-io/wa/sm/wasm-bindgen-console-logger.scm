(define-module (crates-io wa sm wasm-bindgen-console-logger) #:use-module (crates-io))

(define-public crate-wasm-bindgen-console-logger-0.1.0 (c (n "wasm-bindgen-console-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.29") (d #t) (k 0)))) (h "1d8fwp77h00jnz6dgbkzc0z55xw8af43y0mcaammmvvkjjl7a4aa")))

(define-public crate-wasm-bindgen-console-logger-0.1.1 (c (n "wasm-bindgen-console-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.33") (d #t) (k 0)))) (h "1vc506dhrk2yl0snkcn45s5adndq9wj7ipxb7awbbxzswxss4c3m")))

