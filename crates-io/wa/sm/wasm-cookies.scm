(define-module (crates-io wa sm wasm-cookies) #:use-module (crates-io))

(define-public crate-wasm-cookies-0.1.0 (c (n "wasm-cookies") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "urlencoding") (r "^1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "HtmlDocument"))) (d #t) (k 0)))) (h "1yk2caa54c1pfhcfgvfqhhkxnijdj6sy1rw9fnf0y9gbd821skmx")))

(define-public crate-wasm-cookies-0.2.0 (c (n "wasm-cookies") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "urlencoding") (r "^1.1") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "HtmlDocument"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1hsn8dhr5z6iyj90h2yyrqvl153b9ss95rfj4inwynpma50w3clc")))

(define-public crate-wasm-cookies-0.2.1 (c (n "wasm-cookies") (v "0.2.1") (d (list (d (n "urlencoding") (r "^1.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("Window" "HtmlDocument"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1gkk97bnryds0bncc18w8hihqyzs7q2j0xkm1qgl5picsr7pqavr")))

