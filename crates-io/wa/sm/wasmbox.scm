(define-module (crates-io wa sm wasmbox) #:use-module (crates-io))

(define-public crate-wasmbox-0.1.0 (c (n "wasmbox") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "wasmbox-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0yhgmc0wrvkbijaf4ks58b2qadm6qp6l0rqavds48vjd19iwxg0i")))

(define-public crate-wasmbox-0.1.1 (c (n "wasmbox") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 2)) (d (n "async-trait") (r "^0.1.53") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "wasmbox-macro") (r "^0.1.1") (d #t) (k 0)))) (h "01qk8p4698fcirihzyghb5hh06n8xmxdabbvjaw65a4072d8fml1")))

