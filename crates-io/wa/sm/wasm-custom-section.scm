(define-module (crates-io wa sm wasm-custom-section) #:use-module (crates-io))

(define-public crate-wasm-custom-section-0.1.0 (c (n "wasm-custom-section") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "wasm-gen") (r "^0.1.4") (d #t) (k 0)))) (h "0s7yp40gdps4gq6vg3h18ba85flk6mwrwkp7nr26aqaxj4mxjnsl")))

(define-public crate-wasm-custom-section-0.1.1 (c (n "wasm-custom-section") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "pretty-hex") (r "^0.2.1") (d #t) (k 0)) (d (n "wasm-gen") (r "^0.1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.83") (d #t) (k 0)))) (h "1zgjw9frqzrrrg358zgwj4c2ql4dp5psilga9fx6jlf8496bqxf4")))

