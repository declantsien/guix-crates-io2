(define-module (crates-io wa sm wasm-tracing-allocator) #:use-module (crates-io))

(define-public crate-wasm-tracing-allocator-0.1.0 (c (n "wasm-tracing-allocator") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.43") (d #t) (k 0)))) (h "0fpscrngmsajq72jhd4vavr1fiz0fs06fywjc911as66dv8bv92m")))

(define-public crate-wasm-tracing-allocator-0.1.1 (c (n "wasm-tracing-allocator") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.43") (d #t) (k 0)))) (h "1bk5r9s2nmv9am6jw7vcpfms7cl6kff800zysw5cwx9m8q7qnqq0")))

