(define-module (crates-io wa sm wasm_keyboard_macros) #:use-module (crates-io))

(define-public crate-wasm_keyboard_macros-0.1.0 (c (n "wasm_keyboard_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "uievents-code") (r "^0.1.2") (f (quote ("enum"))) (d #t) (k 0)))) (h "09nigbkh3l0yhw3y2h5fwg0zrbm9jicjp3147ab8dvj5pfhrzcsr") (f (quote (("keypress")))) (r "1.56.1")))

(define-public crate-wasm_keyboard_macros-0.1.1 (c (n "wasm_keyboard_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full"))) (d #t) (k 0)) (d (n "uievents-code") (r "^0.1.2") (f (quote ("enum"))) (d #t) (k 0)))) (h "1qqblcy4sqm0v8qn619sji90i2w5nwlc1x0kqns5n8lvs7lsmipr") (f (quote (("keypress")))) (r "1.56.1")))

