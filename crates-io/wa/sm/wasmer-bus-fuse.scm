(define-module (crates-io wa sm wasmer-bus-fuse) #:use-module (crates-io))

(define-public crate-wasmer-bus-fuse-1.1.0 (c (n "wasmer-bus-fuse") (v "1.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync" "macros"))) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 2)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "wasmer-bus") (r "^1") (f (quote ("rt" "macros"))) (k 0)))) (h "1adfq3hffs7cz8bdm3mw3kjlrvbfkd4lc2rzrdq2zqhj5zgsmzmy") (f (quote (("default") ("backend"))))))

