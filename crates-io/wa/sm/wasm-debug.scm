(define-module (crates-io wa sm wasm-debug) #:use-module (crates-io))

(define-public crate-wasm-debug-0.1.0 (c (n "wasm-debug") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.52.0") (d #t) (k 0)) (d (n "faerie") (r "^0.13.0") (d #t) (k 0)) (d (n "gimli") (r "^0.20.0") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9.0") (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)) (d (n "wasmparser") (r "^0.39.2") (d #t) (k 0)))) (h "1q5fjgw517ndmqz4vr4w6jqc7fc5aypdq43qpqljws5grv60x146")))

(define-public crate-wasm-debug-0.2.0 (c (n "wasm-debug") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.59.0") (d #t) (k 0)) (d (n "faerie") (r "^0.14.0") (d #t) (k 0)) (d (n "gimli") (r "^0.20.0") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2.1") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.10.0") (k 0)) (d (n "thiserror") (r "^1.0.4") (d #t) (k 0)) (d (n "wasmparser") (r "^0.51.4") (d #t) (k 0)))) (h "01kmi8xlr9flib64ckmzvw6c6qnyj9252wxq0aki3nvjh4b31r0d")))

