(define-module (crates-io wa sm wasm-bindgen-wasm-conventions) #:use-module (crates-io))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.51 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.51") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "walrus") (r "^0.12.0") (d #t) (k 0)))) (h "0a29192h6i5fc174ggr3i96mll9p5nn465j02vpbnvkqij4nnl3v")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.52 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.52") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "walrus") (r "^0.12.0") (d #t) (k 0)))) (h "1335hj7yd4iyy8b7fpsa9zbnx3kkds91k3csp7n0xdvq1cm9ka5r")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.53 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.53") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "walrus") (r "^0.12.0") (d #t) (k 0)))) (h "11y60wqvyxig7v8ri33nh1av7g8br0qab41i5a4pz0xm53p3hjvb")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.54 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.54") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.13.0") (d #t) (k 0)))) (h "1skk1ydxx28mykkmpch3s6dsg57vbf0gdbiv6bylgjn8snw7ifsq")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.55 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.55") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.13.0") (d #t) (k 0)))) (h "1kr3afwjj6l4piqd13hlvmk74mnbsmpfx9g49dv0zlq111d9ppsg")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.56 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.56") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)))) (h "0sgn6xwgj1lwrrhqc7ighksd6p82z5himrghjl7fkla0qigd7c05")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.57 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.57") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)))) (h "1mqb1blz2n7wr103jhvny4pqr5cppb82hbgg5i97p8i658mya2ym")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.58 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.58") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)))) (h "1bgdp2z14mck7khvbcy5asznrja30wa216j12bgvnvcrxjf37sv1")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.59 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.59") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)))) (h "05llnkbyv2qa7g54rqx5r0yy3y57iy49p0i911mpbgvz7yjg46y6")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.60 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.60") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)))) (h "0swf01gj9cwhvy8pijkvpzxmd64jjxa81333kjakq25kmapa715g")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.61 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.61") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)))) (h "14dkp5vwz0y6igwvafh25mvalfvil74jg4qghn4w67ccdddmr124")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.62 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.62") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)))) (h "0hd5gh3c6b9q49049v06660garrrw96jyqpc3bydrg242hri5xbm")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.63 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.63") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.17.0") (d #t) (k 0)))) (h "1q503v0yw32da9sq900mydx5nam7zp5kr072lnl260y9dfb8anp0")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.64 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.64") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.17.0") (d #t) (k 0)))) (h "149zi0rwph8ny5dkbhvpkwaa2p93vfxwb2rjsz8xkybb3hi3c3dg")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.65 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.65") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)))) (h "0p6hniycx2dclxqq14ddsvr0qfzmik6x7jh67jagsq6ibjyjbmpp")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.66 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.66") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)))) (h "0psn3zkdn7b4xnc81cw3bjyjz9kp2ad9dfk9y9r6papqc7fsxshb")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.67 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.67") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)))) (h "0r6qklsrd93hhblvg536y3fvkpwa2dzdxpcv4ahvr0194spp8b96")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.68 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.68") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)))) (h "12a2m43myxqmvbwbcwdkp16f7vig918jc9d9pwpd6j4rd12c8km1")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.69 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.69") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)))) (h "0rdbnzkadb0ssn2bqnb4qy3lznlhrys9zhy1irw3idsnc1jpggfs")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.70 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.70") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)))) (h "1vn70sl8kkia74gd8saip22ajcgmcsk2cpf2dg2zm9x12fh5c07p")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.71 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.71") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)))) (h "013c7as6k0qa01j0q3svc0313k9j0i8nmnxhp7paqzwlfll2s9z7")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.72 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.72") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)))) (h "0s5s4370dkixbk6pgd0sq85phrrdxhqw0krlr8p24ybwzzjxglsv")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.73 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.73") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)))) (h "1zahbdy6x3g8rgmm3440vyi44pl4557q4c992m1z3xf1x2vqkd69")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.74 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.74") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "0waqz2f86q4vdvyvbyffcf4zw4fy9ga67v85v1qlpxv7l9l8anfv")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.75 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.75") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "1i8wcjkl2bgxi86ydaxixgwrdqqc0jnpw3a82r87rls9fg3673zn")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.76 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.76") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "0cv90qzz75ia58fkid7kpw8gx5f0kcw9hgrmw37s9zyvwjfr9jif")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.77 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.77") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "112790qqjf6aidsbixhmk889ij80fy47kc7di7zd5wr5775dim4v")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.78 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.78") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "0wx18a039vsf3x77h051nfzx2a2q9s96lc92zc9p6s3jnf8wf6l1")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.79 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.79") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "0w779gm3hf4n7fsvr58f0zjffw44bqkwwixg9akb1fyd10dbbg3g")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.80 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.80") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "11mn5lcqjz9mwihkk6vfganfi0mb7smzb7im71fxsq2gjk3fksqg")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.81 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.81") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "060d1zp7zpr77yrgayjsk5pjsbbkgvhz60v9gpsp3578iyiyr860")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.82 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.82") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "08vsz6g8h7gjspaiz87052ias0ya5flrmxb0p811mm5ab5nfql7m")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.83 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.83") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "00lhvy3siggf9gj0pfv5vnbyxxrnbnxvng7j7xy3pxqjgwhsp9dl")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.84 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.84") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "0ss60jfajsn3fmvjhwarv10367sj2dmzdz800v4lbna8a0rdxxfq")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.85 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.85") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "1swp72bpfk614mvvb26pa0bjcrqar7bvzmkjmmlhfjlsdn2prrzk")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.86 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.86") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "1a4lm7ml1yk1pmgcgl2jmigj7vx7sk9j625l9jch014klwksvrc4")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.87 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.87") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "0yylziynbh132rh7sq8wc6v6zhdf92cm2k529j1flhahzyqnb70f") (r "1.56")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.88 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.88") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)))) (h "05n824krfnm7a4js0657av7my0lanv7rvamq42aj6dy8fp0jph9d") (r "1.57")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.89 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.89") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.20.2") (d #t) (k 0)))) (h "0izhbkmwiz1d1pcgnsikr295sv7zrddxdziv5vywc8ybd63zivda") (r "1.57")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.90 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.90") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.20.2") (d #t) (k 0)))) (h "1483kdd8gwl967sb3b0l9d2pfgnlggnb5jflrynixhysrkc3xwck") (r "1.57")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.91 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.91") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.20.2") (d #t) (k 0)))) (h "1kdr3v1vvcflsjf4gfd7yxx300lkjkk7dwd0pl4rch10d0znassf") (r "1.57")))

(define-public crate-wasm-bindgen-wasm-conventions-0.2.92 (c (n "wasm-bindgen-wasm-conventions") (v "0.2.92") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "walrus") (r "^0.20.2") (d #t) (k 0)))) (h "0xdmsfdypj45v8k8gvw9n1y4gswb5rgkmnv0h9v7c3l1gdhf614c") (r "1.57")))

