(define-module (crates-io wa sm wasm-ast) #:use-module (crates-io))

(define-public crate-wasm-ast-0.0.1 (c (n "wasm-ast") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "184640wpqh4nv9srbb9zwhy1jyzm3nwak5jf9c6l7p93k4kqvwaj")))

(define-public crate-wasm-ast-0.0.2 (c (n "wasm-ast") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0n157sdldyjvbcgkvfp583zbqcgjhqlp3xbblfg8k81i5za8b18k")))

(define-public crate-wasm-ast-0.0.3 (c (n "wasm-ast") (v "0.0.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "0zc3cmycr6vfnfk5rmbarchazhzcir32xn6g3vi40aaa0y8ki6v1")))

(define-public crate-wasm-ast-0.0.4 (c (n "wasm-ast") (v "0.0.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0mm4pnshh0x2fygik12lhilyl0bmy68h5kpk94pws7w8hl0r53hx")))

(define-public crate-wasm-ast-0.0.5 (c (n "wasm-ast") (v "0.0.5") (d (list (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wat") (r "^1") (o #t) (d #t) (k 0)))) (h "082mlhfp3zqaxbj15fb340brmm8n58dha10vswh18s2n5x96h4hp") (f (quote (("text" "parser" "wat") ("parser" "nom") ("default"))))))

(define-public crate-wasm-ast-0.0.6 (c (n "wasm-ast") (v "0.0.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmtime") (r "^0.30.0") (d #t) (k 2)) (d (n "wat") (r "^1") (o #t) (d #t) (k 0)))) (h "1wznzwwq63w08k4sbjsrw2ivpc6rw32nh1q19r2fgaxih3d9s1jy") (f (quote (("text" "parser" "wat") ("parser" "nom") ("full" "emitter" "text") ("emitter") ("default"))))))

(define-public crate-wasm-ast-0.1.0 (c (n "wasm-ast") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "nom") (r "^7") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmtime") (r "^0.30.0") (d #t) (k 2)) (d (n "wat") (r "^1") (o #t) (d #t) (k 0)))) (h "01agcbzgidjqphsmga98dzg8m01avwyzknlwfw611zqf8i6djn4j") (f (quote (("text" "parser" "wat") ("parser" "nom") ("full" "emitter" "text") ("emitter") ("default"))))))

