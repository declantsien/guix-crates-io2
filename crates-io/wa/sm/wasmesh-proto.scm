(define-module (crates-io wa sm wasmesh-proto) #:use-module (crates-io))

(define-public crate-wasmesh-proto-0.2.0 (c (n "wasmesh-proto") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.14.9") (f (quote ("full"))) (d #t) (k 0)) (d (n "protobuf") (r "^2") (f (quote ("with-bytes"))) (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "wasmy-abi") (r "^0.2.2") (d #t) (k 0)))) (h "11cgfnhd05cribi6ykaf7gf5gggg3akr9djhymaarg6i3nm7i1g1")))

