(define-module (crates-io wa sm wasmtime-c-api-macros) #:use-module (crates-io))

(define-public crate-wasmtime-c-api-macros-18.0.1 (c (n "wasmtime-c-api-macros") (v "18.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "12g7j7r6nsf77m6x43hx6bclfi218lzjp7xk087nkzllf8rllk46")))

(define-public crate-wasmtime-c-api-macros-18.0.2 (c (n "wasmtime-c-api-macros") (v "18.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1gil5bz3dq124ybaj212zh01y548d0rklfk5xy0zwbd5xdqilmrf")))

(define-public crate-wasmtime-c-api-macros-18.0.3 (c (n "wasmtime-c-api-macros") (v "18.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1h4428v7b4vvmdba9lqirf7qnn1xh47x6qkn10yxbdj5yxirwm6r")))

(define-public crate-wasmtime-c-api-macros-19.0.0 (c (n "wasmtime-c-api-macros") (v "19.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "05alf38jj2scbsya2pviqzax0798xrzpbarlhjmig2x21w2i5zhc")))

(define-public crate-wasmtime-c-api-macros-19.0.1 (c (n "wasmtime-c-api-macros") (v "19.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0v1bn8fpvfjxgkxjvhiw9zy493779qabdmsngp8qjaqzg9v6i1ll")))

(define-public crate-wasmtime-c-api-macros-18.0.4 (c (n "wasmtime-c-api-macros") (v "18.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0n48dj8a25358y82kgfap2m8y83sn55fd30ay9xhbs0xxk35p0gz")))

(define-public crate-wasmtime-c-api-macros-19.0.2 (c (n "wasmtime-c-api-macros") (v "19.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1y434cdnmjyqs1k58zq1v8j5vnh3q76f3n000m5s8iz2h9pdjdmb")))

(define-public crate-wasmtime-c-api-macros-20.0.0 (c (n "wasmtime-c-api-macros") (v "20.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0y3lwqrz0vmxfvzryaic8fhs9ics9y0a51349vzz1y2jxsh5ywha")))

(define-public crate-wasmtime-c-api-macros-20.0.1 (c (n "wasmtime-c-api-macros") (v "20.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0vggxq472z76rbqp94zyh1z8nni9d2hdisqzihp821zjiinsliy1")))

(define-public crate-wasmtime-c-api-macros-20.0.2 (c (n "wasmtime-c-api-macros") (v "20.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "03y75k762ggqiihq0cjhijm27zbsb84ija3cmb2zxrlqjy6bqyqb")))

(define-public crate-wasmtime-c-api-macros-21.0.0 (c (n "wasmtime-c-api-macros") (v "21.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0i6mkmdd6q30axlzrfc4pl9mgj6zvx7lkc982l8y65xg04ifykzn")))

(define-public crate-wasmtime-c-api-macros-21.0.1 (c (n "wasmtime-c-api-macros") (v "21.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1swmjimr63s8w924s2whws1w3fifk35yj01a4v3dkkn92zwr4jyp")))

