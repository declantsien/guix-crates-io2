(define-module (crates-io wa sm wasmtime-wast) #:use-module (crates-io))

(define-public crate-wasmtime-wast-0.2.0 (c (n "wasmtime-wast") (v "0.2.0") (d (list (d (n "cranelift-codegen") (r "^0.40.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.40.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "cranelift-wasm") (r "^0.40.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (k 0)) (d (n "failure_derive") (r "^0.1.3") (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)) (d (n "wabt") (r "^0.9.1") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmtime-jit") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmtime-runtime") (r "^0.2.0") (d #t) (k 0)))) (h "1d2ils71kwahlk4m4p8byc6iprigsvsz8mk3pyl94kb0z0qxxn4p")))

(define-public crate-wasmtime-wast-0.3.0 (c (n "wasmtime-wast") (v "0.3.0") (d (list (d (n "cranelift-codegen") (r "^0.40.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.40.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "cranelift-wasm") (r "^0.40.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.3") (k 0)) (d (n "failure_derive") (r "^0.1.3") (k 0)) (d (n "target-lexicon") (r "^0.4.0") (d #t) (k 0)) (d (n "wabt") (r "^0.9.1") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.3.0") (d #t) (k 0)) (d (n "wasmtime-jit") (r "^0.3.0") (d #t) (k 0)) (d (n "wasmtime-runtime") (r "^0.3.0") (d #t) (k 0)))) (h "1a4zwjsx4sl8avslwq3k3rknsma89mc3n64xvs7gsvxhyjq761xq")))

(define-public crate-wasmtime-wast-0.7.0 (c (n "wasmtime-wast") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.50.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.50.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "cranelift-wasm") (r "^0.50.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9.0") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmtime-jit") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmtime-runtime") (r "^0.7.0") (d #t) (k 0)) (d (n "wast") (r "^3.0.0") (d #t) (k 0)))) (h "0djyk8siqf7ck29vv37zbl1z31f6br6z7p6n2jga4s2cxzdjm9xy") (f (quote (("lightbeam"))))))

(define-public crate-wasmtime-wast-0.8.0 (c (n "wasmtime-wast") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "cranelift-codegen") (r "^0.50.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "cranelift-entity") (r "^0.50.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "cranelift-wasm") (r "^0.50.0") (f (quote ("enable-serde"))) (d #t) (k 0)) (d (n "target-lexicon") (r "^0.9.0") (d #t) (k 0)) (d (n "wasmtime-environ") (r "^0.8.0") (d #t) (k 0)) (d (n "wasmtime-jit") (r "^0.8.0") (d #t) (k 0)) (d (n "wasmtime-runtime") (r "^0.8.0") (d #t) (k 0)) (d (n "wast") (r "^3.0.0") (d #t) (k 0)))) (h "0qi8w7pw4yr2fv7h9x1ligf5nnw2n56fa2i9p0vzcc9dnl1nbsps") (f (quote (("lightbeam"))))))

(define-public crate-wasmtime-wast-0.9.0 (c (n "wasmtime-wast") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.9.0") (d #t) (k 0)) (d (n "wast") (r "^5.0.1") (d #t) (k 0)))) (h "0m11nv1yscihczhbxms51l4c21c09dxrwxzq2302spb37mv55msm") (f (quote (("lightbeam"))))))

(define-public crate-wasmtime-wast-0.10.0 (c (n "wasmtime-wast") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.10.0") (d #t) (k 0)) (d (n "wast") (r "^6.0.0") (d #t) (k 0)))) (h "1hmfdc20aa0g2mlhhxcc7nvi34ixmiadxm6bcbx3jfr5zh0ds9zx") (f (quote (("lightbeam"))))))

(define-public crate-wasmtime-wast-0.11.0 (c (n "wasmtime-wast") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.11.0") (d #t) (k 0)) (d (n "wast") (r "^7.0.0") (d #t) (k 0)))) (h "07s6s6ndphb3bidyq22af3fq8l5pfy08vrz7a2w8mf4vxlbnyc9w") (f (quote (("lightbeam"))))))

(define-public crate-wasmtime-wast-0.12.0 (c (n "wasmtime-wast") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.12.0") (d #t) (k 0)) (d (n "wast") (r "^9.0.0") (d #t) (k 0)))) (h "0saqf6ibi2dii4nc9f0paqzyxjxfhz217884q76yx12kklzlcng2") (f (quote (("lightbeam"))))))

(define-public crate-wasmtime-wast-0.13.0 (c (n "wasmtime-wast") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.13.0") (k 0)) (d (n "wast") (r "^11.0.0") (d #t) (k 0)))) (h "02jzc7z5c4alm5ahkj00iw0fsk3vzc3jryfds7a208prsx676pdz") (f (quote (("lightbeam" "wasmtime/lightbeam"))))))

(define-public crate-wasmtime-wast-0.15.0 (c (n "wasmtime-wast") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.15.0") (k 0)) (d (n "wast") (r "^11.0.0") (d #t) (k 0)))) (h "02g0il02slvygspdsnhzfy2cvdz65p6gysmpvfll2f22m718aa0j") (f (quote (("lightbeam" "wasmtime/lightbeam"))))))

(define-public crate-wasmtime-wast-0.16.0 (c (n "wasmtime-wast") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.16.0") (k 0)) (d (n "wast") (r "^14.0.0") (d #t) (k 0)))) (h "041gy9r1m1vqwxlj1zk7ndb8dyiw77p2yd927pm4bafm36257nns") (f (quote (("lightbeam" "wasmtime/lightbeam"))))))

(define-public crate-wasmtime-wast-0.17.0 (c (n "wasmtime-wast") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.17.0") (k 0)) (d (n "wast") (r "^17.0.0") (d #t) (k 0)))) (h "1hllyd2md5gnm9cgd517dfs2q26rxbcrj2nv00ny4scs39ncsiw9") (f (quote (("lightbeam" "wasmtime/lightbeam"))))))

(define-public crate-wasmtime-wast-0.18.0 (c (n "wasmtime-wast") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.18.0") (k 0)) (d (n "wast") (r "^17.0.0") (d #t) (k 0)))) (h "19hcazgz5gxa52nrzbywrs7ydpkgy7b9m3jkqb3sg9lv4s52dsng") (f (quote (("lightbeam" "wasmtime/lightbeam"))))))

(define-public crate-wasmtime-wast-0.19.0 (c (n "wasmtime-wast") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.19.0") (k 0)) (d (n "wast") (r "^21.0.0") (d #t) (k 0)))) (h "16l885cfsr4kz242d3rzshf0pgjjbj27a2qwmvmyg6v72jfn38f4") (f (quote (("lightbeam" "wasmtime/lightbeam"))))))

(define-public crate-wasmtime-wast-0.20.0 (c (n "wasmtime-wast") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.20.0") (k 0)) (d (n "wast") (r "^22.0.0") (d #t) (k 0)))) (h "046i78z6ahymm88ah9sg157343772spflaicwlcibq2cvvfrl0kf")))

(define-public crate-wasmtime-wast-0.21.0 (c (n "wasmtime-wast") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.21.0") (k 0)) (d (n "wast") (r "^26.0.1") (d #t) (k 0)))) (h "1gcpakbkn3fh8qwq98g4jqn643p84dhnpq6rn7rh715a7sap95mj")))

(define-public crate-wasmtime-wast-0.22.0 (c (n "wasmtime-wast") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.22.0") (k 0)) (d (n "wast") (r "^29.0.0") (d #t) (k 0)))) (h "1hv4kg13mhbi2nngssqhm3fk9pi425za9b7s0dhg215m10l0wa52")))

(define-public crate-wasmtime-wast-0.23.0 (c (n "wasmtime-wast") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.23.0") (k 0)) (d (n "wast") (r "^32.0.0") (d #t) (k 0)))) (h "10rb67c78rfj5bn5r08bz81mz4bpdfw9h9s8p3sfbac9dlg4i0xs")))

(define-public crate-wasmtime-wast-0.24.0 (c (n "wasmtime-wast") (v "0.24.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.24.0") (k 0)) (d (n "wast") (r "^35.0.0") (d #t) (k 0)))) (h "1cfwvdvxaa84jz91f2al49xl6f3wn4smkwqhgxpq1l39iaid089v")))

(define-public crate-wasmtime-wast-0.25.0 (c (n "wasmtime-wast") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.25.0") (k 0)) (d (n "wast") (r "^35.0.0") (d #t) (k 0)))) (h "0g3g1nbzrbw3rd5v8hzz2vy5bxxddzyd9klr4dpcfgyj10n05fp2")))

(define-public crate-wasmtime-wast-0.26.0 (c (n "wasmtime-wast") (v "0.26.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.26.0") (k 0)) (d (n "wast") (r "^35.0.1") (d #t) (k 0)))) (h "03pkc3yh2k15w5j0ad5576h9l5ggsd3v4rnlhwpkvcjigqi3c2pj")))

(define-public crate-wasmtime-wast-0.27.0 (c (n "wasmtime-wast") (v "0.27.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.27.0") (k 0)) (d (n "wast") (r "^35.0.2") (d #t) (k 0)))) (h "1mwfl18xg3gyv3qc4gw0daqk1ki30ig4kiidm2lg8wyc3n5k6ca7")))

(define-public crate-wasmtime-wast-0.26.1 (c (n "wasmtime-wast") (v "0.26.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.26.1") (k 0)) (d (n "wast") (r "^35.0.1") (d #t) (k 0)))) (h "1cipk8jgmd42d4c2dlpl5zhl5xsn28250n9ga7yyy9mjiga9ljs9")))

(define-public crate-wasmtime-wast-0.28.0 (c (n "wasmtime-wast") (v "0.28.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.28.0") (k 0)) (d (n "wast") (r "^35.0.2") (d #t) (k 0)))) (h "04vk84v6pvmiwyzr8wbiffks8v8cy0n586n6wfd7jx1gkwv5zdaz")))

(define-public crate-wasmtime-wast-0.29.0 (c (n "wasmtime-wast") (v "0.29.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.29.0") (k 0)) (d (n "wast") (r "^36.0.0") (d #t) (k 0)))) (h "0fgjqjpj843w26xl1qh4qk18wrvpzm4zlld0vr2x8pmpcxcdkzvi")))

(define-public crate-wasmtime-wast-0.30.0 (c (n "wasmtime-wast") (v "0.30.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.30.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^37.0.0") (d #t) (k 0)))) (h "1l7zxim26nv5mi8ln3zsg17axifn1r808wgskyqj64b4fgg455m2")))

(define-public crate-wasmtime-wast-0.31.0 (c (n "wasmtime-wast") (v "0.31.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.31.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^38.0.0") (d #t) (k 0)))) (h "1a0ljgf5v4rkd23qkkgmzvkx5gr9n0rxjknhw3ydvciprp7bcwhb")))

(define-public crate-wasmtime-wast-0.32.0 (c (n "wasmtime-wast") (v "0.32.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.32.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^38.0.0") (d #t) (k 0)))) (h "0qi7j663m6v1vg4rsmjlhdrc19b8y9bgfizc5gjs2fji0y7yd036")))

(define-public crate-wasmtime-wast-0.32.1 (c (n "wasmtime-wast") (v "0.32.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.32.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^38.0.0") (d #t) (k 0)))) (h "06yyh2n2hna29kzw4jdpmymdwm74i5iz1pa5l0dhza5pq2zzr2s4")))

(define-public crate-wasmtime-wast-0.33.0 (c (n "wasmtime-wast") (v "0.33.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.33.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^38.0.0") (d #t) (k 0)))) (h "073h63xgfv800pzsigi0m1gk70pjrj7n6vzm03pgka0i2xqsm3in")))

(define-public crate-wasmtime-wast-0.34.0 (c (n "wasmtime-wast") (v "0.34.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.34.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^39.0.0") (d #t) (k 0)))) (h "06sh22d895wsa9sqyak98afpfj1fr465cyx3x876s0nd4i4bda5h")))

(define-public crate-wasmtime-wast-0.34.1 (c (n "wasmtime-wast") (v "0.34.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.34.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^39.0.0") (d #t) (k 0)))) (h "0lcb64spj6gi1argz3zhmgbdy9fckppg8gj77qsk1hkwy7bygfdj")))

(define-public crate-wasmtime-wast-0.33.1 (c (n "wasmtime-wast") (v "0.33.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.33.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^38.0.0") (d #t) (k 0)))) (h "0zy0bi81s5l8z82070scnp22vb88i78bpg21g80x5b8d8yn8nv9z")))

(define-public crate-wasmtime-wast-0.35.0 (c (n "wasmtime-wast") (v "0.35.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.35.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^39.0.0") (d #t) (k 0)))) (h "0d1maxgi2cbq6czgxjin619lfdfcp1wy5vpxdll8pwb2bw5sifjm")))

(define-public crate-wasmtime-wast-0.35.1 (c (n "wasmtime-wast") (v "0.35.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.35.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^39.0.0") (d #t) (k 0)))) (h "043kwa5zv6c9y0p37kchpgbhv04pzc0ig460brqvqxmh4p2wld8n")))

(define-public crate-wasmtime-wast-0.35.2 (c (n "wasmtime-wast") (v "0.35.2") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.35.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^39.0.0") (d #t) (k 0)))) (h "1l0dhrx0x4apz7py6hhvsjzycqnrd0nlzzkwgjsllx5sljhq58sf")))

(define-public crate-wasmtime-wast-0.34.2 (c (n "wasmtime-wast") (v "0.34.2") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.34.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^39.0.0") (d #t) (k 0)))) (h "0fc27sr2f9r654r6w8ms5yn0hj431jmqzafjirpjyxzipws5pysx")))

(define-public crate-wasmtime-wast-0.35.3 (c (n "wasmtime-wast") (v "0.35.3") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.35.3") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^39.0.0") (d #t) (k 0)))) (h "18k788912r17agmsnsb8rfw7zrafq7mvbrhdky5nhbnza6lzvdy5")))

(define-public crate-wasmtime-wast-0.36.0 (c (n "wasmtime-wast") (v "0.36.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.36.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^39.0.0") (d #t) (k 0)))) (h "1d3j1b7wn0ip2s05ggivlvnb6m4mrw5c41fgqjynkq46590vnx94")))

(define-public crate-wasmtime-wast-0.37.0 (c (n "wasmtime-wast") (v "0.37.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.37.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^40.0.0") (d #t) (k 0)))) (h "1qm43gj24vhd2aiz5l3lr3cdy16s572dwliqlz5bk5mlxskr60ql")))

(define-public crate-wasmtime-wast-0.38.0 (c (n "wasmtime-wast") (v "0.38.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.38.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^41.0.0") (d #t) (k 0)))) (h "0ran32dmwwdxbg7n4xlf3x2rl0n2z5wyn4gpv16pkzdi5kal1bcp") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-0.38.1 (c (n "wasmtime-wast") (v "0.38.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.38.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^41.0.0") (d #t) (k 0)))) (h "0930wsv69pnh7yag67fplg71vig4wkg7s943hw3ag9r5x7b5aljs") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-0.39.0 (c (n "wasmtime-wast") (v "0.39.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.39.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^42.0.0") (d #t) (k 0)))) (h "1vfaghhg548y7b9q2ngm0y1m79hwvjnf088jh8496fcmvsnv1c04") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-0.38.2 (c (n "wasmtime-wast") (v "0.38.2") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.38.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^41.0.0") (d #t) (k 0)))) (h "1yd52k0wyhy0h7ac9vvpfkq21yxxlncirgdy4msjq75dvz5ilsh0") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-0.38.3 (c (n "wasmtime-wast") (v "0.38.3") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.38.3") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^41.0.0") (d #t) (k 0)))) (h "0y9x3jppiq2nzzp2p7l0rjq2srpkhrkcbq4mp7fzjsnj19j652kk") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-0.39.1 (c (n "wasmtime-wast") (v "0.39.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.39.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^42.0.0") (d #t) (k 0)))) (h "0rnk9x2ld9k8982ddcm1zph1q4dizbhprmpsijrv7fk4lak9wcr3") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-0.40.0 (c (n "wasmtime-wast") (v "0.40.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.40.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^45.0.0") (d #t) (k 0)))) (h "0gnpx9xvdgh6iyxackhzb3nkfggx6pk6nk1y0zlj3r8l00dghw16") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-0.40.1 (c (n "wasmtime-wast") (v "0.40.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.40.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^45.0.0") (d #t) (k 0)))) (h "0i1b1ivsn9y579p4if73bqb1gvwpjyrihcqmwfxsjqw3gxpfzhrb") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-1.0.0 (c (n "wasmtime-wast") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasmtime") (r "^1.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^46.0.0") (d #t) (k 0)))) (h "0zkrjbhzhiw3ax19j1m3im1c1yjygfw6fs11rhwagbb8i38kgm7i") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-1.0.1 (c (n "wasmtime-wast") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasmtime") (r "^1.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^46.0.0") (d #t) (k 0)))) (h "18y9lzij03mrmfslgjirz3dc8gkkzd65dxdkbgnia5gs718h1fjc") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-2.0.0 (c (n "wasmtime-wast") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^2.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^47.0.1") (d #t) (k 0)))) (h "160j3c427hg26vgzdwbj8x57s60g8fx8qa2yl670hz2n46rh03kh") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-2.0.1 (c (n "wasmtime-wast") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^2.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^47.0.1") (d #t) (k 0)))) (h "0wbwd7f07sfhyyl7085vfsx3dyxwc7gr8kf29fc5sgma4s4fbnab") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-2.0.2 (c (n "wasmtime-wast") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^2.0.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^47.0.1") (d #t) (k 0)))) (h "0cqn1qf4xps1v5c6698sskppricj5ikyy7813883qv2n455kl3yl") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-1.0.2 (c (n "wasmtime-wast") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "wasmtime") (r "^1.0.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^46.0.0") (d #t) (k 0)))) (h "1vd9gwj1yfxprk2rkmnrk6baxznn5bkydvzalsf1mnz1a51nnxnc") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-3.0.0 (c (n "wasmtime-wast") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^3.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^48.0.0") (d #t) (k 0)))) (h "1h83xv3wk3cpyy8k8lhba1k1lab4x5564s5n3vh92xl0gzx42wgp") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-3.0.1 (c (n "wasmtime-wast") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^3.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^48.0.0") (d #t) (k 0)))) (h "02xc4d4gixrnxd3qjb29q3v6gqh9xmacq5swh3m1qbv0l66v6qc4") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-4.0.0 (c (n "wasmtime-wast") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^4.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^50.0.0") (d #t) (k 0)))) (h "0vslpyff8xbaz7vnf2412jg5qwg30v2lz4i19mvm2vv5q27lyjms") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-5.0.0 (c (n "wasmtime-wast") (v "5.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^5.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^50.0.0") (d #t) (k 0)))) (h "02ysf1fcx2psgf175m98ymqkdxzrm6raz25nss5zfzd6fnkwh9aa") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-6.0.0 (c (n "wasmtime-wast") (v "6.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^6.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^53.0.0") (d #t) (k 0)))) (h "0di10kib8shvrvlbv0gn4zk1pwzzm4f4754qrdv38k0j4407zmj4") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-5.0.1 (c (n "wasmtime-wast") (v "5.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^5.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^50.0.0") (d #t) (k 0)))) (h "15v8x1g4mnc0ndda5n50khqd5d0rd8cmmzxayxk1byqxscaajzlc") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-4.0.1 (c (n "wasmtime-wast") (v "4.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^4.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^50.0.0") (d #t) (k 0)))) (h "1xzhikxvxkrhhk4n280285hlh9ji3na6zfgg5pr1j8h5xf8hnb9a") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-6.0.1 (c (n "wasmtime-wast") (v "6.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^6.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^53.0.0") (d #t) (k 0)))) (h "1p777ir07x4sw6nbcqjfz1r3pn3jxgz9ma9bqcmig4969b91g2kj") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-7.0.0 (c (n "wasmtime-wast") (v "7.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^7.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^53.0.0") (d #t) (k 0)))) (h "1v1bbd4jw9abswlm3ybayylyvhfr2bnx8i5nz3cgapy5v67j57lp") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-8.0.0 (c (n "wasmtime-wast") (v "8.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^8.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^55.0.0") (d #t) (k 0)))) (h "0dwl8kj6v42rqr9ib99m8qxhwfvyfwdpph76dc2d6r2c91bx6i9b") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-6.0.2 (c (n "wasmtime-wast") (v "6.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^6.0.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^53.0.0") (d #t) (k 0)))) (h "1mva3ygl0dysx7z5kimrizbgpxi7vif08pimgkds5i6kd8yc6vk8") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-8.0.1 (c (n "wasmtime-wast") (v "8.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^8.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^55.0.0") (d #t) (k 0)))) (h "1lsk5iplfshfpig3zd6vb23bdxrgy3bgcmwwq7i07k4fh7y80f2x") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-7.0.1 (c (n "wasmtime-wast") (v "7.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^7.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^53.0.0") (d #t) (k 0)))) (h "1b33r4qhq2gpsqg5smhafr4z6x67bln449300g1fdr4rcwkqipxh") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-9.0.0 (c (n "wasmtime-wast") (v "9.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^9.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^56.0.0") (d #t) (k 0)))) (h "0rlb83157vs7c8rlsyh6bjxjipcfzbv7r1jljlrhndi74xp77wf2") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-9.0.1 (c (n "wasmtime-wast") (v "9.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^9.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^56.0.0") (d #t) (k 0)))) (h "058p2xy89gb52p56v96rqp4jpl3h75c34rlvgbkc0hnmb55a9f9p") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-9.0.2 (c (n "wasmtime-wast") (v "9.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^9.0.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^56.0.0") (d #t) (k 0)))) (h "03rcs3xa4gx7sxzx17i5f70x3sp5qk0gxgivb25bndpzqaxd9gph") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-9.0.3 (c (n "wasmtime-wast") (v "9.0.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^9.0.3") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^56.0.0") (d #t) (k 0)))) (h "0q8ia4s5cf7mp9i2dyrajlvhmkmmqk3r5ncranhyg87l5c9ks4qg") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-9.0.4 (c (n "wasmtime-wast") (v "9.0.4") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^9.0.4") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^56.0.0") (d #t) (k 0)))) (h "1p4gkmspd6gxnsh1nlbs3niqmsg9l146csmif206j5rcvwm8d8k8") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-10.0.0 (c (n "wasmtime-wast") (v "10.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^10.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^60.0.0") (d #t) (k 0)))) (h "1i4y158b6xl7hdb7rh3hrc96bdf8zi96zpkb5c9xfhdzgw0qlz0g") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-10.0.1 (c (n "wasmtime-wast") (v "10.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^10.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^60.0.0") (d #t) (k 0)))) (h "0xazlxzbjf36xwa2d26c5d7z213ds55917dvqiyw8yivvd0mgjdz") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-11.0.0 (c (n "wasmtime-wast") (v "11.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^11.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^60.0.0") (d #t) (k 0)))) (h "1fnzp5nvxg9h4zkr1zfz2anr8pcmx6b8f6brsfhgjcflx6hsak6l") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-11.0.1 (c (n "wasmtime-wast") (v "11.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^11.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^60.0.0") (d #t) (k 0)))) (h "05gs8zbh89xagx08q2jib4lwc4sx2fqlw8fbdvj86wip7q402mjm") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-12.0.0 (c (n "wasmtime-wast") (v "12.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^12.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^62.0.1") (d #t) (k 0)))) (h "1siwq1n92jzd9dgxwk91dzgs7nngb2db9x0i492dl8dq8hngi0qv") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-12.0.1 (c (n "wasmtime-wast") (v "12.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^12.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^62.0.1") (d #t) (k 0)))) (h "0k1dzjszsp8xbxdb8vkn594il5ylvkhzj6gqkkfw33c9jhf1c346") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-10.0.2 (c (n "wasmtime-wast") (v "10.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^10.0.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^60.0.0") (d #t) (k 0)))) (h "1v8mbqpbsdahk6hxdpn6nxfkd45hlpkvbp89fhb6bm92kw96dj88") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-11.0.2 (c (n "wasmtime-wast") (v "11.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^11.0.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^60.0.0") (d #t) (k 0)))) (h "1n7kdgaqwy5aragfxn4a6gncyhdkv0cv35z8230djnanpj0rxxyv") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-12.0.2 (c (n "wasmtime-wast") (v "12.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^12.0.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^62.0.1") (d #t) (k 0)))) (h "08dvzq9lb74zpwxjxd31jsh0vjabjd06hhaqn7y6zq25n3a3s25f") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-13.0.0 (c (n "wasmtime-wast") (v "13.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^13.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^64.0.0") (d #t) (k 0)))) (h "12zxbsx9i9nn3qangrl1psvajfvgvgi6bwjq144p4jnwv2qk5f64") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-14.0.0 (c (n "wasmtime-wast") (v "14.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^14.0.0") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^66.0.2") (d #t) (k 0)))) (h "0rw03w25r468985j8r9w9pr3p0j39yqkgwgahhrsvcjzsgnkg7s9") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-14.0.1 (c (n "wasmtime-wast") (v "14.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^14.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^66.0.2") (d #t) (k 0)))) (h "0sy761pp7qg43k6hbi24ipajw52q157vv30zyyhx2dmpp99k8xma") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-13.0.1 (c (n "wasmtime-wast") (v "13.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^13.0.1") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^64.0.0") (d #t) (k 0)))) (h "1yfsvwrdq8cy8fv2prmbh9f3ibkq3s9phzda351ijrlp8psjd3wf") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-14.0.2 (c (n "wasmtime-wast") (v "14.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^14.0.2") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^66.0.2") (d #t) (k 0)))) (h "104npjcx3p458mydqv3a5kzhan0zfh35w7f3jx3ida243w9xmqwx") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-14.0.3 (c (n "wasmtime-wast") (v "14.0.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^14.0.3") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^66.0.2") (d #t) (k 0)))) (h "05gkw15pvhnicv27d47x4dfww6qdsyyp7njmxqlzyzx4vkhj81ai") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-14.0.4 (c (n "wasmtime-wast") (v "14.0.4") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^14.0.4") (f (quote ("cranelift"))) (k 0)) (d (n "wast") (r "^66.0.2") (d #t) (k 0)))) (h "1l002di4i9nsa97l22dpj2s406fn2yk3hsfn12j48zb2j3zj62lc") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-15.0.0 (c (n "wasmtime-wast") (v "15.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^15.0.0") (f (quote ("cranelift" "wat"))) (k 0)) (d (n "wast") (r "^67.0.1") (d #t) (k 0)))) (h "0w4axy3ignvzw77a7f3g8mj8kbqj5b3y2gdaz0wsbb6s5xn47659") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-15.0.1 (c (n "wasmtime-wast") (v "15.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^15.0.1") (f (quote ("cranelift" "wat"))) (k 0)) (d (n "wast") (r "^67.0.1") (d #t) (k 0)))) (h "0qp4v3blcv15wil2z4qfssp77x0dpz0ziwf64yclxl4hzwlkpss4") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-16.0.0 (c (n "wasmtime-wast") (v "16.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^16.0.0") (f (quote ("cranelift" "wat"))) (k 0)) (d (n "wast") (r "^69.0.1") (d #t) (k 0)))) (h "16bxn1wyh5svznkl1gkci1ngvhih89m76gs3nbnv4vn1hgfcwyj5") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-17.0.0 (c (n "wasmtime-wast") (v "17.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^17.0.0") (f (quote ("cranelift" "wat"))) (k 0)) (d (n "wast") (r "^69.0.1") (d #t) (k 0)))) (h "0hpx1a7pfls2h3x3j07mfq06jpkraj5ivc4mdq0vyi7gn7f14h9x") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-17.0.1 (c (n "wasmtime-wast") (v "17.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^17.0.1") (f (quote ("cranelift" "wat"))) (k 0)) (d (n "wast") (r "^69.0.1") (d #t) (k 0)))) (h "0iw3rm1vb08vqrm1gwzkl9pmayc8magxs2h3037cfgw2b4dlmwyc") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-18.0.0 (c (n "wasmtime-wast") (v "18.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^18.0.0") (f (quote ("cranelift" "wat" "runtime"))) (k 0)) (d (n "wast") (r "^70.0.2") (d #t) (k 0)))) (h "0z17ya9k1jra6y5mfrzll9b4g6gjcilsysd9mbi5l439bv6lbzka") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-18.0.1 (c (n "wasmtime-wast") (v "18.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^18.0.1") (f (quote ("cranelift" "wat" "runtime"))) (k 0)) (d (n "wast") (r "^70.0.2") (d #t) (k 0)))) (h "1ccshlnqv8l4dndjy90hhx9hcanknlad4jkz257fbl2rhj348jmv") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-17.0.2 (c (n "wasmtime-wast") (v "17.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^17.0.2") (f (quote ("cranelift" "wat"))) (k 0)) (d (n "wast") (r "^69.0.1") (d #t) (k 0)))) (h "077qflg2hnnadrqvn95nkjixis60jmj4xwx9q8b3q0zhnanz5gs1") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-18.0.2 (c (n "wasmtime-wast") (v "18.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^18.0.2") (f (quote ("cranelift" "wat" "runtime"))) (k 0)) (d (n "wast") (r "^70.0.2") (d #t) (k 0)))) (h "0y8bbbzixyv4q2lqb56g2p07rxillnh79ngwdl4fga3s8ji0lcnx") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-18.0.3 (c (n "wasmtime-wast") (v "18.0.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^18.0.3") (f (quote ("cranelift" "wat" "runtime"))) (k 0)) (d (n "wast") (r "^70.0.2") (d #t) (k 0)))) (h "000mlwyszqnyg62cp4h6lhz5sfxqicf0jzl6cbn0p4sp48ps85ry") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-19.0.0 (c (n "wasmtime-wast") (v "19.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^19.0.0") (f (quote ("cranelift" "wat" "runtime" "gc"))) (k 0)) (d (n "wast") (r "^201.0.0") (d #t) (k 0)))) (h "015a00v12lwkzzfgzrc73gj8arp017f6z2ypazbjpdbg74h1s0lv") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-19.0.1 (c (n "wasmtime-wast") (v "19.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^19.0.1") (f (quote ("cranelift" "wat" "runtime" "gc"))) (k 0)) (d (n "wast") (r "^201.0.0") (d #t) (k 0)))) (h "0hmpj7d3diik1hwci7xxj8zbmyl54h3da2pzjkzbys1lvccwg2lk") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-18.0.4 (c (n "wasmtime-wast") (v "18.0.4") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^18.0.4") (f (quote ("cranelift" "wat" "runtime"))) (k 0)) (d (n "wast") (r "^70.0.2") (d #t) (k 0)))) (h "0y8srpcafx194y8znza9zdrwkzzj3gw651pbdlsraph3kc15qsrp") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-19.0.2 (c (n "wasmtime-wast") (v "19.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^19.0.2") (f (quote ("cranelift" "wat" "runtime" "gc"))) (k 0)) (d (n "wast") (r "^201.0.0") (d #t) (k 0)))) (h "0hfg0wd0slpq2byqwbhwzpip94lkxy505h6lcxn8xin1jb2xxijv") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-17.0.3 (c (n "wasmtime-wast") (v "17.0.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^17.0.3") (f (quote ("cranelift" "wat"))) (k 0)) (d (n "wast") (r "^69.0.1") (d #t) (k 0)))) (h "1fwx3s5a25n0k5729vgy0dklnr8h29kaqx51n7rkzjq1l5hkj0m3") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-20.0.0 (c (n "wasmtime-wast") (v "20.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^20.0.0") (f (quote ("cranelift" "wat" "runtime" "gc"))) (k 0)) (d (n "wast") (r "^202.0.0") (d #t) (k 0)))) (h "1ifz2pfjkkil3yix963hyrj7xafqnv6vfhwlmk2ab4nirpmlsna7") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-20.0.1 (c (n "wasmtime-wast") (v "20.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^20.0.1") (f (quote ("cranelift" "wat" "runtime" "gc"))) (k 0)) (d (n "wast") (r "^202.0.0") (d #t) (k 0)))) (h "1r5kbfdlh14jaa9km4yc281sq3xjl14g5vhviw2b6f74vl837hz1") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-20.0.2 (c (n "wasmtime-wast") (v "20.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^20.0.2") (f (quote ("cranelift" "wat" "runtime" "gc"))) (k 0)) (d (n "wast") (r "^202.0.0") (d #t) (k 0)))) (h "16a56rijxfzvipfyvdah0dz9i6fih798pgdp2bpyd0ckj3sjv8kw") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-21.0.0 (c (n "wasmtime-wast") (v "21.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^21.0.0") (f (quote ("cranelift" "wat" "runtime" "gc"))) (k 0)) (d (n "wast") (r "^207.0.0") (d #t) (k 0)))) (h "0pga6656r98g6hk4gpm2f4hayp3fgwnxdfz86hb4mbd48czygqa4") (f (quote (("component-model" "wasmtime/component-model"))))))

(define-public crate-wasmtime-wast-21.0.1 (c (n "wasmtime-wast") (v "21.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (k 0)) (d (n "log") (r "^0.4.8") (k 0)) (d (n "wasmtime") (r "^21.0.1") (f (quote ("cranelift" "wat" "runtime" "gc"))) (k 0)) (d (n "wast") (r "^207.0.0") (d #t) (k 0)))) (h "0j2g3ks8m2bmppy8ml9jhj114rn8zyb8mx2i3a7m15phajvpgn99") (f (quote (("component-model" "wasmtime/component-model"))))))

