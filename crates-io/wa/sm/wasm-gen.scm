(define-module (crates-io wa sm wasm-gen) #:use-module (crates-io))

(define-public crate-wasm-gen-0.1.0 (c (n "wasm-gen") (v "0.1.0") (d (list (d (n "leb128") (r "^0.2.1") (d #t) (k 0)))) (h "004jd5bk37pvr1hrfvqacjcnypjmxbvghxgdv2rz7bdky7spwb36")))

(define-public crate-wasm-gen-0.1.1 (c (n "wasm-gen") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)))) (h "0w49n8fpjn91dbrw1zpd83w4f4n0x170fm6z28i7bkyvkyqva4g2")))

(define-public crate-wasm-gen-0.1.2 (c (n "wasm-gen") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)))) (h "1q7sizmcf60m3019y36b25kkqxp7jjrlsxfy63x04yz7326sm6wi")))

(define-public crate-wasm-gen-0.1.3 (c (n "wasm-gen") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5") (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)))) (h "168yag30phv7i6zaxx6il0h1x2j5bab4qnqdxpcfj9vva8w5s83c")))

(define-public crate-wasm-gen-0.1.4 (c (n "wasm-gen") (v "0.1.4") (d (list (d (n "byteorder") (r "^0.5") (k 0)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)))) (h "1frfhjg7w354cj694rlxh3fkmb1wm9zhyca2awvb79q5213b2m5q")))

