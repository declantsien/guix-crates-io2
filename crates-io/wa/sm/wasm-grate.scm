(define-module (crates-io wa sm wasm-grate) #:use-module (crates-io))

(define-public crate-wasm-grate-0.1.22 (c (n "wasm-grate") (v "0.1.22") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.2") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.110.2") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.141.5") (d #t) (k 0)))) (h "1fb48fhb1ynis7wrw0dhyzbmhznqyg7935cmhda4hfw74gbm67pq")))

(define-public crate-wasm-grate-0.1.23 (c (n "wasm-grate") (v "0.1.23") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.2") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.110.2") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.141.5") (d #t) (k 0)))) (h "155h04jk2ybp5f250irk6z7cv31ivhbhbclmz459hsiim66hsw04")))

(define-public crate-wasm-grate-0.2.0 (c (n "wasm-grate") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.2") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.110.2") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.141.5") (d #t) (k 0)))) (h "0b4kbkj0y4hn5i8pxzdl7z13zjw8x54gwdgdw5z60ykschlsmdss")))

(define-public crate-wasm-grate-0.2.1 (c (n "wasm-grate") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.2") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.110.2") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.141.5") (d #t) (k 0)))) (h "11gmdpq3flrljnw5snr8pxar9n25yhdiasaz860a06zmnz4m0qlb")))

(define-public crate-wasm-grate-0.2.2 (c (n "wasm-grate") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.2") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.110.2") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.141.5") (d #t) (k 0)))) (h "1mf2q989rj5mb72r3ghgfnsw7d1apn7mqbwk5b262p6q956l3qsq")))

(define-public crate-wasm-grate-0.3.0 (c (n "wasm-grate") (v "0.3.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.2") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.110.2") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.141.5") (d #t) (k 0)) (d (n "swc_ecma_visit") (r "^0.96.7") (d #t) (k 0)))) (h "0xp6c145h4xws87dvj0n11v7zayx1l0l2hqvj9f32kxdni7yzw5g")))

(define-public crate-wasm-grate-0.3.1 (c (n "wasm-grate") (v "0.3.1") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.2") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.110.2") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.141.5") (d #t) (k 0)) (d (n "swc_ecma_visit") (r "^0.96.7") (d #t) (k 0)))) (h "08sh5lysq8hk7v9n6cbis024i5kn7rn2qgp2bq9ah5yqvmz637nb")))

(define-public crate-wasm-grate-0.3.2 (c (n "wasm-grate") (v "0.3.2") (d (list (d (n "clap") (r "^4.4.8") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "swc_common") (r "^0.33.8") (d #t) (k 0)) (d (n "swc_ecma_ast") (r "^0.110.9") (d #t) (k 0)) (d (n "swc_ecma_parser") (r "^0.141.21") (d #t) (k 0)) (d (n "swc_ecma_visit") (r "^0.96.9") (d #t) (k 0)))) (h "15iwdl2ns41ydc68s8wwq6m38nc88c81ri8fx6ljsazq3272mdiq")))

