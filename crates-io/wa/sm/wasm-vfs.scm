(define-module (crates-io wa sm wasm-vfs) #:use-module (crates-io))

(define-public crate-wasm-vfs-0.0.1 (c (n "wasm-vfs") (v "0.0.1") (h "1xniipvwajhlvm1950s90a74n1msjfahfl9d6pfmg6q518q1miqd")))

(define-public crate-wasm-vfs-0.0.2 (c (n "wasm-vfs") (v "0.0.2") (h "1yk6246w2qhc9n3ib6syic1mgf58dncw2n49bqdi5p9ri8n1md3x")))

(define-public crate-wasm-vfs-0.0.3 (c (n "wasm-vfs") (v "0.0.3") (h "04jwydd47bk06g6lmh751kx02py71n2fk154dglgns2za1cvdsdv")))

(define-public crate-wasm-vfs-0.0.4 (c (n "wasm-vfs") (v "0.0.4") (h "1mgjn0abgzlcazym346ldkqy05v3wimvh7dqsk6j6bgn981jflnd")))

(define-public crate-wasm-vfs-0.0.5 (c (n "wasm-vfs") (v "0.0.5") (h "1xn3n5klmf5v60ipsq6mdqw2i4lvk0bpvxzs35mbv6bywxg3i2xw")))

(define-public crate-wasm-vfs-0.0.6 (c (n "wasm-vfs") (v "0.0.6") (h "004n1g08wf9w3lsymin9cikcr0m6wyqlj1368wgc3qiyczclx6l4")))

(define-public crate-wasm-vfs-0.0.7 (c (n "wasm-vfs") (v "0.0.7") (h "0g43kdlpf0ngn9dcdfz39amy9dwz1g1gm4vbablp3kxn2yb3lg79")))

(define-public crate-wasm-vfs-0.0.8 (c (n "wasm-vfs") (v "0.0.8") (h "00y67s93m7l1fcsg0zgh2vsxdsfb8a8xbhlkq90b2dnhzqya1bpj")))

(define-public crate-wasm-vfs-0.0.9 (c (n "wasm-vfs") (v "0.0.9") (h "0a3zgapz8c9lm0f6zwisllf441r232bfd4sfqkdhl7nxmlqbnm3k")))

(define-public crate-wasm-vfs-0.1.0 (c (n "wasm-vfs") (v "0.1.0") (h "0nqd7jxk1yc38x4jg4vgymb5jqml6fyriqd5la24dzgj59f1v6ca")))

(define-public crate-wasm-vfs-0.1.1 (c (n "wasm-vfs") (v "0.1.1") (h "06a4srjmhlsaslh4a7fby3x491agcn8qv5q3v9j9kr6yv3nqcdn2")))

