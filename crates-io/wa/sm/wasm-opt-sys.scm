(define-module (crates-io wa sm wasm-opt-sys) #:use-module (crates-io))

(define-public crate-wasm-opt-sys-0.0.0 (c (n "wasm-opt-sys") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "094fmcrv7nfh4m6xr9nx605sx1a6gmivghhfabzsijryx3v3ampd")))

(define-public crate-wasm-opt-sys-0.0.1-preview.1 (c (n "wasm-opt-sys") (v "0.0.1-preview.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "1cbc5slidl4a3l0avppp14kqga6d4dsq3wvzc7msbxvmkb4s456f")))

(define-public crate-wasm-opt-sys-0.0.1-preview.2 (c (n "wasm-opt-sys") (v "0.0.1-preview.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "06jji2hhvp309blnwzsl3573mcqp5x5kkci8yxfqm33nx99y4908")))

(define-public crate-wasm-opt-sys-0.0.1-preview.3 (c (n "wasm-opt-sys") (v "0.0.1-preview.3") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "1xq0d3sf53rbd2jhjnrafhddz8r2z1bqgwh31rdwxzvmq9vgrym1")))

(define-public crate-wasm-opt-sys-0.110.0-beta.1 (c (n "wasm-opt-sys") (v "0.110.0-beta.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "0kyd9wzv2bvwzfm9jxwhknwqnnsmmm6mhilyrjbxsq96g7gqcgpd")))

(define-public crate-wasm-opt-sys-0.110.0-beta.2 (c (n "wasm-opt-sys") (v "0.110.0-beta.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "1jzxl5jw51amx77l0690nb7d5f8pxfwzh2iqckgdbf3zz98i99jp")))

(define-public crate-wasm-opt-sys-0.110.0 (c (n "wasm-opt-sys") (v "0.110.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "16jgdnqbm6b23ba4d5bbak4mxf2sa2qnfzrm9y1r69hm8zii10fw")))

(define-public crate-wasm-opt-sys-0.110.1 (c (n "wasm-opt-sys") (v "0.110.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0.72") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.72") (d #t) (k 1)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "1ckky5ga8y9awqv5n47vanl6psz7x7363y020rj5l8c6jyf06zcd") (l "binaryen")))

(define-public crate-wasm-opt-sys-0.110.2 (c (n "wasm-opt-sys") (v "0.110.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0.79") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.79") (d #t) (k 1)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "0hb9w2iwlb8d70rargnyynfvzgdgas6lj7qlzliarrm44aisakzc") (l "binaryen")))

(define-public crate-wasm-opt-sys-0.111.0 (c (n "wasm-opt-sys") (v "0.111.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0.79") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.79") (d #t) (k 1)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "19sn53fpkd6zrmy2rvk78rxir6fqk652xygddivsjf17aj5y4cj4") (l "binaryen")))

(define-public crate-wasm-opt-sys-0.112.0 (c (n "wasm-opt-sys") (v "0.112.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0.79") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.79") (d #t) (k 1)))) (h "056x6ii9msijd4arrr5rsijihj1bl4k81gfhqsrp247gd17rw6a0") (l "binaryen")))

(define-public crate-wasm-opt-sys-0.113.0 (c (n "wasm-opt-sys") (v "0.113.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0.79") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.79") (d #t) (k 1)))) (h "0w138xp9178gcwnfcbcfhx30k8hl2680ncja2m9ds322sff0cza9") (l "binaryen")))

(define-public crate-wasm-opt-sys-0.114.0 (c (n "wasm-opt-sys") (v "0.114.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0.79") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.79") (d #t) (k 1)))) (h "0scv1rvhmgga40a71i2hhh4kjw788gnd58xvhshkm50jra3kca5p") (l "binaryen")))

(define-public crate-wasm-opt-sys-0.114.1 (c (n "wasm-opt-sys") (v "0.114.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0.79") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.79") (d #t) (k 1)))) (h "1ygl3zpnmsw4cd8rwk5g3i8yv5rs8fvwivzsqjc41mhrlnn2mz9f") (y #t) (l "binaryen")))

(define-public crate-wasm-opt-sys-0.114.2 (c (n "wasm-opt-sys") (v "0.114.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0.79") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.79") (d #t) (k 1)))) (h "19030ybx2hl3274h7yaiaxc1bpklgqqkgykrnxgdds6jpqbziwin") (f (quote (("dwarf") ("default")))) (l "binaryen")))

(define-public crate-wasm-opt-sys-0.116.0 (c (n "wasm-opt-sys") (v "0.116.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cxx") (r "^1.0.79") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.79") (d #t) (k 1)))) (h "1glcp779agbymrvqyzdl3k90pa6vyafw53vivg5xls679mbcw74a") (f (quote (("dwarf") ("default" "dwarf")))) (l "binaryen")))

