(define-module (crates-io wa sm wasmdev_macro) #:use-module (crates-io))

(define-public crate-wasmdev_macro-0.1.1 (c (n "wasmdev_macro") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "wasmdev_server") (r "^0.1.1") (d #t) (k 0)))) (h "07dwvrz8an528w54hnyp0ah5yi9i74bdhjkzs4pfhw4xdcyr3pz6")))

(define-public crate-wasmdev_macro-0.1.2 (c (n "wasmdev_macro") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "wasmdev_server") (r "^0.1.2") (d #t) (k 0)))) (h "06wz2a6ybl0zdzl8mrwzisfimml3kz3xfzl1hx8g7fj9nfqlhpci")))

(define-public crate-wasmdev_macro-0.1.3 (c (n "wasmdev_macro") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "wasmdev_server") (r "^0.1.3") (d #t) (k 0)))) (h "11c3nm1pwbnzcg6jv0j1gfazqci0a2mzlmk0130vbv5w91njzhgv")))

(define-public crate-wasmdev_macro-0.1.4 (c (n "wasmdev_macro") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "wasmdev_server") (r "^0.1.4") (d #t) (k 0)))) (h "1701mx5y43yiazmsrih8cg5mv4i1nmrsmw7c6ik49dhp5f82brpj")))

(define-public crate-wasmdev_macro-0.1.5 (c (n "wasmdev_macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "wasmdev_server") (r "^0.1.5") (d #t) (k 0)))) (h "0v0aam9dw5601gfyx99icrqsp6rxbfx6jnm32a301yaphymzvhg6")))

(define-public crate-wasmdev_macro-0.1.6 (c (n "wasmdev_macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "wasmdev_server") (r "^0.1.6") (d #t) (k 0)))) (h "0vmaljfivwjvnjljc8jaav7gw9z9kjh4sdfjkxrc2k4s61lww5vy")))

(define-public crate-wasmdev_macro-0.1.7 (c (n "wasmdev_macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "wasmdev_core") (r "^0.1.7") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1zwbvd3ybp7xdzlar5jcdmgnn3id5k9m5x3a9gjl9z4f25914inh") (f (quote (("nightly" "wasmdev_core/nightly"))))))

