(define-module (crates-io wa sm wasm-split) #:use-module (crates-io))

(define-public crate-wasm-split-0.1.0 (c (n "wasm-split") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.38.0") (d #t) (k 0)))) (h "079wczq16ax1b86lmiz8hwhcnnpylpximwhbms65fz92wpx32dzi")))

