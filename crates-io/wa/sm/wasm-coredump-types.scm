(define-module (crates-io wa sm wasm-coredump-types) #:use-module (crates-io))

(define-public crate-wasm-coredump-types-0.1.10 (c (n "wasm-coredump-types") (v "0.1.10") (h "0643yn60j1yf0q24a33k4szik1avfvl5k0rrafcjrqbfsc2fj357")))

(define-public crate-wasm-coredump-types-0.1.11 (c (n "wasm-coredump-types") (v "0.1.11") (h "1z1r4maqcdpx6v9vmi5x8fi5vw2x44msq8qya4h4rr11l0lmgqqh")))

(define-public crate-wasm-coredump-types-0.1.12 (c (n "wasm-coredump-types") (v "0.1.12") (h "0r6hyxgwgrzwbpnhvinr7a7vbwqkp25rhqa2xfgialihldkf16il")))

(define-public crate-wasm-coredump-types-0.1.13 (c (n "wasm-coredump-types") (v "0.1.13") (h "02423lvq5dd3b2gj6zssw87c57qn5xhhdphcivjjl99ywnxafdsq")))

(define-public crate-wasm-coredump-types-0.1.14 (c (n "wasm-coredump-types") (v "0.1.14") (h "13wbml5gw95n269jxwszhimmgff2g5ybaq5hajc8kla2n7fsighw")))

(define-public crate-wasm-coredump-types-0.1.15 (c (n "wasm-coredump-types") (v "0.1.15") (h "0nv1mqq7cy1iwrdx7cgbglcmf6nqbgriiv236rrz8y6mn33s2fvb")))

(define-public crate-wasm-coredump-types-0.1.16 (c (n "wasm-coredump-types") (v "0.1.16") (h "07rkjq0b2z32shdp3pkrkfazi6idjicagqyr6v3l36777cln1z0n")))

(define-public crate-wasm-coredump-types-0.1.17 (c (n "wasm-coredump-types") (v "0.1.17") (h "1n2q876b5142g0d7xvia299j36yzfbj2a9qf7khjvwfvli0s63rs")))

(define-public crate-wasm-coredump-types-0.1.18 (c (n "wasm-coredump-types") (v "0.1.18") (h "05435m3v1hsihzvcfrx1lpkr5psab3igw4980wvc9fv61q8mysr8")))

(define-public crate-wasm-coredump-types-0.1.19 (c (n "wasm-coredump-types") (v "0.1.19") (h "0gw58h8miy00y90lk9h26y5wsxfkv6yk6km3v2gy6s44nxiq9678")))

(define-public crate-wasm-coredump-types-0.1.20 (c (n "wasm-coredump-types") (v "0.1.20") (h "1ziasg4x1y7qqd9bqvb65fpbl6cry9bwgshbyz9gjwb4fkxhrh8n")))

(define-public crate-wasm-coredump-types-0.1.21 (c (n "wasm-coredump-types") (v "0.1.21") (h "0ffwwzyslddikynrm5wgmq0skjhssj317a35qb7scn8zxi5a519a")))

(define-public crate-wasm-coredump-types-0.1.22 (c (n "wasm-coredump-types") (v "0.1.22") (h "0dpkfbyhqhzap11mfdmwsyaldl4njd416ni784dlcg4h0yc3sxnj")))

