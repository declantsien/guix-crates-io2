(define-module (crates-io wa sm wasmtime-versioned-export-macros) #:use-module (crates-io))

(define-public crate-wasmtime-versioned-export-macros-12.0.0 (c (n "wasmtime-versioned-export-macros") (v "12.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1z43kjz1wwpv66ly9ly962lmkr7sw7mhcgz0z7ky0cfzkqkp883b")))

(define-public crate-wasmtime-versioned-export-macros-12.0.1 (c (n "wasmtime-versioned-export-macros") (v "12.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "00sphw3kp5gl4jl8h4956h2xmdqwic6dbmr7pj2zi3mddkx3djir")))

(define-public crate-wasmtime-versioned-export-macros-12.0.2 (c (n "wasmtime-versioned-export-macros") (v "12.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0l2hz39zps5g06zaxlr3av8h82yifni0grimg28c8xg87sxzjyna")))

(define-public crate-wasmtime-versioned-export-macros-13.0.0 (c (n "wasmtime-versioned-export-macros") (v "13.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0hq6qsaap4pq51wahxa15s6dbfg3ckvjl4fwxjs9cvfxrdr791zy")))

(define-public crate-wasmtime-versioned-export-macros-14.0.0 (c (n "wasmtime-versioned-export-macros") (v "14.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "10a9z2rz92kcbsclh2snxr7p1c5r41x0xakpibs12kizc4plyw27")))

(define-public crate-wasmtime-versioned-export-macros-14.0.1 (c (n "wasmtime-versioned-export-macros") (v "14.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "17ghradq5aiz5kq3vza1cn4rdc9g37kk42wah264s8smwr7w38ay")))

(define-public crate-wasmtime-versioned-export-macros-13.0.1 (c (n "wasmtime-versioned-export-macros") (v "13.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0lifd23synfqiyl04nn81ssdcy2gkvni8pvkmfmd92kdqdbwaqk3")))

(define-public crate-wasmtime-versioned-export-macros-14.0.2 (c (n "wasmtime-versioned-export-macros") (v "14.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0qwnh1jjqq5fkpb39scxq139fl321ksrf7vr7jm8sh1vmwi086bk")))

(define-public crate-wasmtime-versioned-export-macros-14.0.3 (c (n "wasmtime-versioned-export-macros") (v "14.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1rhnmafcy5b91qx10jf78jws8d576ws0vmz1xcgsadwxppnv1v1b")))

(define-public crate-wasmtime-versioned-export-macros-14.0.4 (c (n "wasmtime-versioned-export-macros") (v "14.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1lwxj4skznm5rcd06622wg4csh9mr53xd6mv6rncl4g7fmd5gd89")))

(define-public crate-wasmtime-versioned-export-macros-15.0.0 (c (n "wasmtime-versioned-export-macros") (v "15.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0rd3596gbm7ypwvrf4237iaqdqf9hlf1pyqa33xpa8pixjw7nd4s")))

(define-public crate-wasmtime-versioned-export-macros-15.0.1 (c (n "wasmtime-versioned-export-macros") (v "15.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0cc2fdyb108cgjwg2bz62bj5zd025myax86rx6l2mzcvszw523zm")))

(define-public crate-wasmtime-versioned-export-macros-16.0.0 (c (n "wasmtime-versioned-export-macros") (v "16.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1nqhs8cnwff4vgi5wrczcxiaxllda7ipl9zls0fw51wjjmjmap7m")))

(define-public crate-wasmtime-versioned-export-macros-17.0.0 (c (n "wasmtime-versioned-export-macros") (v "17.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1hmwxkg34hvachd4yajph7sm9jrb8pgv3n446xs4azp67xrj5lzs")))

(define-public crate-wasmtime-versioned-export-macros-17.0.1 (c (n "wasmtime-versioned-export-macros") (v "17.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1qg5js4fn6lvl6wxs9f5k84z4si528g7xd65h1fmvjg4sikf23z1")))

(define-public crate-wasmtime-versioned-export-macros-18.0.0 (c (n "wasmtime-versioned-export-macros") (v "18.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1czj7rr0sk3kwb8bk2ifv3nabc19fzljdsvlap677zdgw02a2ihf")))

(define-public crate-wasmtime-versioned-export-macros-18.0.1 (c (n "wasmtime-versioned-export-macros") (v "18.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "10n3sklj2y3nrfgsykw463qb6b0w29ycygjf31yl5a9ndjbh00vb")))

(define-public crate-wasmtime-versioned-export-macros-17.0.2 (c (n "wasmtime-versioned-export-macros") (v "17.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1zz01l1xayrgsjjhbk1zi3z2xpryq0gjvm194cbiqiwlk2ca72z7")))

(define-public crate-wasmtime-versioned-export-macros-18.0.2 (c (n "wasmtime-versioned-export-macros") (v "18.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "190dkb4r4slfh3afw92xwidcwgmli4g9srws2maml0y7kzm2xwwf")))

(define-public crate-wasmtime-versioned-export-macros-18.0.3 (c (n "wasmtime-versioned-export-macros") (v "18.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "18a7zihkrrz9cwxrpbdbiqp5ils0bq6cjjhxrzw44v10dmbdgf2a")))

(define-public crate-wasmtime-versioned-export-macros-19.0.0 (c (n "wasmtime-versioned-export-macros") (v "19.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1z7xdz78lb9lwxbb2961pbhyzm80d8ps6g9hqvaafb8305zrcvbd")))

(define-public crate-wasmtime-versioned-export-macros-19.0.1 (c (n "wasmtime-versioned-export-macros") (v "19.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0wm7n1953p45aqns7fnvjvj56v3wxn7v0biy05kidjk1584irn7i")))

(define-public crate-wasmtime-versioned-export-macros-18.0.4 (c (n "wasmtime-versioned-export-macros") (v "18.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "13cl2g409pxhml78cm1d5gamjc29lgg0ald4kldlfjmsvmsw36ak")))

(define-public crate-wasmtime-versioned-export-macros-19.0.2 (c (n "wasmtime-versioned-export-macros") (v "19.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "199d38wm8nsgmv6m06bj1i00mjn4a1fjksb8w2g1lnrm29fgmapz")))

(define-public crate-wasmtime-versioned-export-macros-17.0.3 (c (n "wasmtime-versioned-export-macros") (v "17.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1fyyqb49fz1zap9vrmja4zds220z71453jfk269zma3sb3784ip0")))

(define-public crate-wasmtime-versioned-export-macros-20.0.0 (c (n "wasmtime-versioned-export-macros") (v "20.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0kv86h8csq4hzgxbnwml3a1raxmcq9qpsm9x45ss3chdjbidjqc5")))

(define-public crate-wasmtime-versioned-export-macros-20.0.1 (c (n "wasmtime-versioned-export-macros") (v "20.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0700yjvfmq4bk7vaqqi3lvnd6x00l0xm6a77jjdqag6dl7q22lxq")))

(define-public crate-wasmtime-versioned-export-macros-20.0.2 (c (n "wasmtime-versioned-export-macros") (v "20.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1b7k45bvykiiv0lq8866l0daxa2l4nsbh0zh0g4ibafz5x170fqd")))

(define-public crate-wasmtime-versioned-export-macros-21.0.0 (c (n "wasmtime-versioned-export-macros") (v "21.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1m7hxz3faj0906nqcmvv8fsw99p8bzl2cw2wh8sy4s38nn647cw9")))

(define-public crate-wasmtime-versioned-export-macros-21.0.1 (c (n "wasmtime-versioned-export-macros") (v "21.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0iyx7kk0i679pjn4rscjgkybgwvynij8bqrfaa2jmnzkzrdxrknl")))

