(define-module (crates-io wa sm wasm-gc-capi) #:use-module (crates-io))

(define-public crate-wasm-gc-capi-0.0.0 (c (n "wasm-gc-capi") (v "0.0.0") (h "1x99h6xf03wrwfv49n09i0ihnrgxb48lll73qwcgpw5pwhc89a0f")))

(define-public crate-wasm-gc-capi-0.1.0 (c (n "wasm-gc-capi") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "tetsy-wasm-gc-api") (r "^0.1.11") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (o #t) (d #t) (k 0)))) (h "04shnwnfsbf81s7aw5aqzyl77l8ddhh8byjf80k3r5kj31jn1m42")))

