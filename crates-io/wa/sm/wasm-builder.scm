(define-module (crates-io wa sm wasm-builder) #:use-module (crates-io))

(define-public crate-wasm-builder-0.0.0 (c (n "wasm-builder") (v "0.0.0") (h "0p1rhmf33076rzmc8mr6a6viaw7sf0793zra41zg8ymhz5fkkv5s") (y #t)))

(define-public crate-wasm-builder-3.0.0 (c (n "wasm-builder") (v "3.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "build-helper") (r "^0.1.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tetsy-wasm-gc-api") (r "^0.1.11") (d #t) (k 0)) (d (n "toml") (r "^0.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1wq5wvyrpwmqz4vg813rrzwavbjzn9935izvqrgkmjb44cdsz64n")))

(define-public crate-wasm-builder-1.0.9 (c (n "wasm-builder") (v "1.0.9") (d (list (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "build-helper") (r "^0.1.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.9.0") (d #t) (k 0)) (d (n "fs2") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.2.9") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1.11") (d #t) (k 0)))) (h "15s1lhb717glcn4df9jd0mzafhl97i74f6h6yprdw3rnbhw10pn8")))

(define-public crate-wasm-builder-3.0.1 (c (n "wasm-builder") (v "3.0.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "build-helper") (r "^0.1.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tetsy-wasm-gc-api") (r "^0.1.11") (d #t) (k 0)) (d (n "toml") (r "^0.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "1k8q2b454a28kcrdzks53yj4zc515s0zsa7pqm9zl0ij1klifjx6")))

(define-public crate-wasm-builder-2.1.2 (c (n "wasm-builder") (v "2.1.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "atty") (r "^0.2.13") (d #t) (k 0)) (d (n "build-helper") (r "^0.1.1") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.12.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "tetsy-wasm-gc-api") (r "^0.1.11") (d #t) (k 0)) (d (n "toml") (r "^0.5.4") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.1") (d #t) (k 0)))) (h "0nd0g2gwp113a9i1ff6ncch0gvnndx4s7my5npc9fb1flfi688gh")))

