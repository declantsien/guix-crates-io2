(define-module (crates-io wa sm wasmer-wast-asml-fork) #:use-module (crates-io))

(define-public crate-wasmer-wast-asml-fork-1.0.2 (c (n "wasmer-wast-asml-fork") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "^1.0.2") (k 0) (p "wasmer-asml-fork")) (d (n "wasmer-wasi") (r "^1.0.2") (d #t) (k 0) (p "wasmer-wasi-asml-fork")) (d (n "wast") (r "^24.0") (d #t) (k 0)))) (h "1ylqzvizhkj4ji5m8myxcgcidpjvp6qnh8rbw1jb6yqxkp5az1gy") (f (quote (("wat" "wasmer/wat") ("default" "wat"))))))

