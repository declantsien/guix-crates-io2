(define-module (crates-io wa sm wasmi_runtime_layer) #:use-module (crates-io))

(define-public crate-wasmi_runtime_layer-0.31.0 (c (n "wasmi_runtime_layer") (v "0.31.0") (d (list (d (n "anyhow") (r "^1.0") (f (quote ("std"))) (k 0)) (d (n "ref-cast") (r "^1.0") (k 0)) (d (n "smallvec") (r "^1.11") (k 0)) (d (n "wasm_runtime_layer") (r "^0.4.0") (d #t) (k 0)) (d (n "wasmi") (r "^0.31.0") (f (quote ("std"))) (k 0)))) (h "1lvdq4pann5qq5bn23jxzg7cgf7vhadr7v3acz8bd9c0jxcg0jhd")))

