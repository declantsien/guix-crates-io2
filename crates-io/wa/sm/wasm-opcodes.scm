(define-module (crates-io wa sm wasm-opcodes) #:use-module (crates-io))

(define-public crate-wasm-opcodes-0.115.0 (c (n "wasm-opcodes") (v "0.115.0") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.115.0") (d #t) (k 0)))) (h "02hqkdsqgq78k8vdj1cy11vfz453pw82p1894fhb7w7z71mkgipj") (y #t)))

(define-public crate-wasm-opcodes-0.115.1 (c (n "wasm-opcodes") (v "0.115.1") (d (list (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "wasmparser") (r "^0.115.0") (d #t) (k 0)))) (h "1sd1zda562x5xj0a20s51w3rpcigxhldqadpnalzs13rqiy7rydk")))

