(define-module (crates-io wa sm wasmos-macro) #:use-module (crates-io))

(define-public crate-wasmos-macro-0.0.1 (c (n "wasmos-macro") (v "0.0.1") (d (list (d (n "field_types") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xfww8yq71i80cmmr5p7jx5mg1dxmz6ilkx3wwhwh94aalaixlpk")))

