(define-module (crates-io wa sm wasm-runner) #:use-module (crates-io))

(define-public crate-wasm-runner-0.1.0 (c (n "wasm-runner") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "0cr22ga21h1q1qdmm7xfx513kvmcv90l0i2ihwb6hx8v0kddj6cj")))

(define-public crate-wasm-runner-0.2.0 (c (n "wasm-runner") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "0p0fhbpq434cz16k8an9fyz37vykzbbibmy0pamyjb0ypycc91rl")))

(define-public crate-wasm-runner-0.2.1 (c (n "wasm-runner") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "1f3kaqwjiwv19k21xl6ricxan589lr0v7sj91pwcpwanc1hrd697")))

(define-public crate-wasm-runner-0.3.0 (c (n "wasm-runner") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "1gwxy8fi4f9525n4wpavxy2y181lhrg1jxapgfnxqp2y0n30xx8r")))

(define-public crate-wasm-runner-0.3.1 (c (n "wasm-runner") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 0)))) (h "1kvqa69d2pnc8qxwj18p4qg7rrf8q85dhsf6rnkrqf7mvx72iy7n")))

