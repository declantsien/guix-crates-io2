(define-module (crates-io wa sm wasmflow-output) #:use-module (crates-io))

(define-public crate-wasmflow-output-0.10.0-beta.4 (c (n "wasmflow-output") (v "0.10.0-beta.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "wasmflow-packet") (r "^0.10.0-beta.4") (d #t) (k 0)) (d (n "wasmflow-streams") (r "^0.10.0-beta.4") (d #t) (k 0)) (d (n "wasmflow-transport") (r "^0.10.0-beta.4") (f (quote ("async"))) (d #t) (k 0)))) (h "1n3v1998hdjmfq2dab9mpc3bp7y40b9n196kzxm1gm1b4v22rxjm")))

(define-public crate-wasmflow-output-0.10.0 (c (n "wasmflow-output") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "wasmflow-packet") (r "^0.10.0-beta.4") (d #t) (k 0)) (d (n "wasmflow-streams") (r "^0.10.0-beta.4") (d #t) (k 0)) (d (n "wasmflow-transport") (r "^0.10.0-beta.4") (f (quote ("async"))) (d #t) (k 0)))) (h "1rxjvch5d3r1s086zbxy96adldvrv347srjhw8mcyr3ql149b80p")))

