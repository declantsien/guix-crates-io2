(define-module (crates-io wa sm wasm-compiler) #:use-module (crates-io))

(define-public crate-wasm-compiler-0.0.0 (c (n "wasm-compiler") (v "0.0.0") (d (list (d (n "cstring") (r ">=0.1.1, <0.2.0") (d #t) (k 0)))) (h "0yx348gd2r2wxaxz4rm7bjjq2dnz64ls90wmk591sly51dwap554")))

