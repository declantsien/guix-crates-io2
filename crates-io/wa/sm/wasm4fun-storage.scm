(define-module (crates-io wa sm wasm4fun-storage) #:use-module (crates-io))

(define-public crate-wasm4fun-storage-0.1.0 (c (n "wasm4fun-storage") (v "0.1.0") (d (list (d (n "postcard") (r "^0.7") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "wasm4fun-core") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm4fun-log") (r "^0.1.0") (k 0)))) (h "086qrm03v8bc90n20j0sngskqi845fj06x3cmxsi42nfjq7ypgwy") (f (quote (("storage" "serde" "postcard") ("default"))))))

