(define-module (crates-io wa sm wasm-bindgen-gc) #:use-module (crates-io))

(define-public crate-wasm-bindgen-gc-0.2.25 (c (n "wasm-bindgen-gc") (v "0.2.25") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.34") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)))) (h "0fcynj5yy70wzf40vqb189ihdfiizadwsasi3r1fc5zcpyykwd47")))

(define-public crate-wasm-bindgen-gc-0.2.26 (c (n "wasm-bindgen-gc") (v "0.2.26") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)))) (h "0ngqkx2qh7j7c0gfyj5lpzwr1xgiyqxlwfb6r1h94hdaxpcqip48")))

(define-public crate-wasm-bindgen-gc-0.2.27 (c (n "wasm-bindgen-gc") (v "0.2.27") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)))) (h "1cw71390br0xhsyanqpi1zk3m8b6x2b9nnizg6h6n80pry99kmgv")))

(define-public crate-wasm-bindgen-gc-0.2.28 (c (n "wasm-bindgen-gc") (v "0.2.28") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 2)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)))) (h "1nsc7a46m9f52hd62hw2zy76c7rwnd1j5rvxxvd5b8ihbsq1kdcd")))

(define-public crate-wasm-bindgen-gc-0.2.29 (c (n "wasm-bindgen-gc") (v "0.2.29") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 2)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)))) (h "00z13ch9fy01asbfigvnpnd6ghcm0hkykp5s3spfgm1pjmhc38f8")))

(define-public crate-wasm-bindgen-gc-0.2.30 (c (n "wasm-bindgen-gc") (v "0.2.30") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 2)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)))) (h "1bqvpsndm81xb00sp8gg049qb7kgc7a8rkhx37zk49gwbz3na7jl")))

(define-public crate-wasm-bindgen-gc-0.2.31 (c (n "wasm-bindgen-gc") (v "0.2.31") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 2)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)))) (h "0a8w16bzwj9kwb71n4qg04gpz8ij99hk89ccy6zlmkwdkj751cf5")))

(define-public crate-wasm-bindgen-gc-0.2.32 (c (n "wasm-bindgen-gc") (v "0.2.32") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 2)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)))) (h "1jq8r5d6jz0h7iqmikn5pxhsjg5vx7mgww3zakq7pvvav2b481k6")))

(define-public crate-wasm-bindgen-gc-0.2.33 (c (n "wasm-bindgen-gc") (v "0.2.33") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 2)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)))) (h "1f1318aaw6ayyqf7jk2synia9sasdfafqc5mgcykjj6qpavdf417")))

(define-public crate-wasm-bindgen-gc-0.2.34 (c (n "wasm-bindgen-gc") (v "0.2.34") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.36") (d #t) (k 0)) (d (n "rayon") (r "^1.0.2") (d #t) (k 2)) (d (n "rustc-demangle") (r "^0.1.9") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)))) (h "1ywrf37k6n8vjrpc6cidpp70syzsdygmmk4zjhfwnxyfnnwcizs4")))

