(define-module (crates-io wa sm wasmedge-bindgen-macro) #:use-module (crates-io))

(define-public crate-wasmedge-bindgen-macro-0.1.0 (c (n "wasmedge-bindgen-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "15ix09jyxvhzdsxvzvz06ipxpkmd7h7fsvgsdm6w81xjqwbj96wy")))

(define-public crate-wasmedge-bindgen-macro-0.1.7 (c (n "wasmedge-bindgen-macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cg8nay2kj62nmqc6w115ws07yghwdar8cg0xl9b09dgc32dwn7z")))

(define-public crate-wasmedge-bindgen-macro-0.1.8 (c (n "wasmedge-bindgen-macro") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qwa01cxxafvhmz4d47w9mh3d96hv3cc9lnf300lvzbds84xgsps")))

(define-public crate-wasmedge-bindgen-macro-0.1.11 (c (n "wasmedge-bindgen-macro") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1aqi220i8skf0rfy5xry490v5fazlfpz8zpp7agrndwf6lhxrl70")))

(define-public crate-wasmedge-bindgen-macro-0.1.12 (c (n "wasmedge-bindgen-macro") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bl0jmfp2sq203gy6ijss94cs9gdfiimr5czky5f1bk1sm89ayy7")))

(define-public crate-wasmedge-bindgen-macro-0.1.13 (c (n "wasmedge-bindgen-macro") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "03mrkm3m2bzl9k8i4xldqgn9pfl08cmfpcjmfkqw3dnldr4nkx9y")))

(define-public crate-wasmedge-bindgen-macro-0.1.14 (c (n "wasmedge-bindgen-macro") (v "0.1.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bfvi5s8qff9nkvsnfgk2l77jj0b9v9jbdr208dznlfvy7c091bw")))

(define-public crate-wasmedge-bindgen-macro-0.4.0 (c (n "wasmedge-bindgen-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bq1w9fxvms9ds6m197qw11s27r6f170jvzfnp9mmh17ndnq1n4k")))

(define-public crate-wasmedge-bindgen-macro-0.4.1 (c (n "wasmedge-bindgen-macro") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0cyzkwssy6b6f9a99c5j5j08xda20kvr0bxkng0sl0mxa0kq3zm9")))

