(define-module (crates-io wa sm wasm_mouse) #:use-module (crates-io))

(define-public crate-wasm_mouse-0.1.0 (c (n "wasm_mouse") (v "0.1.0") (d (list (d (n "bitmask-enum") (r "^2.1.0") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("MouseEvent"))) (o #t) (d #t) (k 0)))) (h "0zgf8kmjwxcakw8r477a9i8yx1cwdwznk7niir76y0nlj6l0bwxw") (f (quote (("default" "web-sys")))) (s 2) (e (quote (("web-sys" "dep:web-sys")))) (r "1.56.1")))

