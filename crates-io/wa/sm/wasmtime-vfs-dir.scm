(define-module (crates-io wa sm wasmtime-vfs-dir) #:use-module (crates-io))

(define-public crate-wasmtime-vfs-dir-0.1.0 (c (n "wasmtime-vfs-dir") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt"))) (k 2)) (d (n "wasi-common") (r "^2.0.0") (d #t) (k 0)) (d (n "wasmtime-vfs-file") (r "^0.1.0") (d #t) (k 2)) (d (n "wasmtime-vfs-ledger") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmtime-vfs-memory") (r "^0.1.0") (d #t) (k 0)))) (h "1m5w0ysiagjxl7dyk1dy478d5qx8dpx9cd8hw1hl8l8dfpmv41i8")))

(define-public crate-wasmtime-vfs-dir-0.1.1 (c (n "wasmtime-vfs-dir") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.51") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros" "rt"))) (k 2)) (d (n "wasi-common") (r "^2.0.0") (d #t) (k 0)) (d (n "wasmtime-vfs-file") (r "^0.1.0") (d #t) (k 2)) (d (n "wasmtime-vfs-ledger") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmtime-vfs-memory") (r "^0.1.0") (d #t) (k 0)))) (h "1k28w49px4vln9rxnqiy33299r9krycgim3qi7ibg09r6i5p7m3r")))

