(define-module (crates-io wa sm wasmpng) #:use-module (crates-io))

(define-public crate-wasmpng-0.1.0 (c (n "wasmpng") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "1vcr64xmp1lgi8h1ssnx993y8m2zhd63n9pyqlgrf83i50fd5mm9")))

(define-public crate-wasmpng-0.1.1 (c (n "wasmpng") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "lodepng") (r "^3") (d #t) (k 0)) (d (n "zopfli") (r "^0.5") (d #t) (k 0)))) (h "0g03827r14pq126v0b9mq45ii8iq7rc9fxacdkyi38kc3xxnvfd6")))

(define-public crate-wasmpng-0.1.2 (c (n "wasmpng") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "lodepng") (r "^3") (d #t) (k 0)) (d (n "zopfli") (r "^0.7") (d #t) (k 0)))) (h "0kx5zw883g4m6qa1kcjba6lap1dy1rybdfc34vysmyacn43kvj2n")))

(define-public crate-wasmpng-0.1.3 (c (n "wasmpng") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "lodepng") (r "^3") (d #t) (k 0)) (d (n "zopfli") (r "^0.7") (d #t) (k 0)))) (h "1qj5rhlvzzaafsnsc9kn6kav4cjrjcr2aay03jsnrkp7fv1rk9by")))

