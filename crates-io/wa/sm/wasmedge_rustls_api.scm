(define-module (crates-io wa sm wasmedge_rustls_api) #:use-module (crates-io))

(define-public crate-wasmedge_rustls_api-0.1.0 (c (n "wasmedge_rustls_api") (v "0.1.0") (d (list (d (n "tokio_wasi") (r "^1") (o #t) (d #t) (k 0)))) (h "1jxjj1csb39405p42v31ywhn4by3cack0kcwr07radgbh4gikfzi") (f (quote (("tokio_async" "tokio_wasi") ("default"))))))

(define-public crate-wasmedge_rustls_api-0.1.1 (c (n "wasmedge_rustls_api") (v "0.1.1") (d (list (d (n "tokio_wasi") (r "^1") (o #t) (d #t) (k 0)))) (h "1l33mvlzh0qk8wflffabijg011bcmbvgjmyxh4wigh4kd031y1k5") (f (quote (("tokio_async" "tokio_wasi") ("default"))))))

