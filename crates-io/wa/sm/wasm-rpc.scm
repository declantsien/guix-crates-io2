(define-module (crates-io wa sm wasm-rpc) #:use-module (crates-io))

(define-public crate-wasm-rpc-0.1.0 (c (n "wasm-rpc") (v "0.1.0") (d (list (d (n "cbor-no-std") (r "^0.1.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1vzg30z4iaywf6856ax1rfsn5glraf0lqm3vbl5sldfzy9226cl2")))

(define-public crate-wasm-rpc-0.1.1 (c (n "wasm-rpc") (v "0.1.1") (d (list (d (n "cbor-no-std") (r "^0.1.0") (f (quote ("no_std"))) (d #t) (k 0)))) (h "01ssyn7y4279flim4imnj1hz1d4s1cmiil1lkq9pq3g0lvblvn3p")))

(define-public crate-wasm-rpc-0.1.2 (c (n "wasm-rpc") (v "0.1.2") (d (list (d (n "cbor-lite") (r "^0.1.1") (d #t) (k 0)))) (h "0kxp2nidj3jivy52jgwlz5zlyrfkq1b0mw5wn2fdpf1pxdb6abys")))

(define-public crate-wasm-rpc-0.1.3 (c (n "wasm-rpc") (v "0.1.3") (d (list (d (n "cbor-lite") (r "^0.1.1") (d #t) (k 0)))) (h "1mvhm1wvda3gvi13az3jrxhvvwnsh4y5h0gcam7dc5v5sd2lrk16")))

(define-public crate-wasm-rpc-0.2.1 (c (n "wasm-rpc") (v "0.2.1") (d (list (d (n "cbor-no-std") (r "^0.3.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "1fsh9mzsjk8prh6giql10fdhfwgl8n3mfvciaxgw1ibvnmavqrfy")))

(define-public crate-wasm-rpc-0.2.2 (c (n "wasm-rpc") (v "0.2.2") (d (list (d (n "cbor-no-std") (r "^0.3.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "0vkh4lz4hpc8vpprplgjqjmk9gm7l610cc0vz5blqz7yf78p5xg1")))

(define-public crate-wasm-rpc-0.2.3 (c (n "wasm-rpc") (v "0.2.3") (d (list (d (n "cbor-no-std") (r "^0.3.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "0dzjipkgzv2wxib7id73aq7gcq4ikkzc7vnmzh5i7p1z3hqj94rr")))

(define-public crate-wasm-rpc-0.2.4 (c (n "wasm-rpc") (v "0.2.4") (d (list (d (n "cbor-no-std") (r "^0.3.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "0488qx46nd6c0h0rrbk77wwcf59ajfys1j3256yp7dy4p7ll3404")))

(define-public crate-wasm-rpc-0.2.5 (c (n "wasm-rpc") (v "0.2.5") (d (list (d (n "cbor-no-std") (r "^0.3.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "0c5bv2i5nhhrbqlvx0sjhirx68ss2jp19qdk9ldccfz9gljsbvas")))

(define-public crate-wasm-rpc-0.2.6 (c (n "wasm-rpc") (v "0.2.6") (d (list (d (n "cbor-no-std") (r "^0.3.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "05gwfw16jcgx6hikxd4wq53klrclsmara16vlg9dgw8204kh8anq")))

(define-public crate-wasm-rpc-0.2.7 (c (n "wasm-rpc") (v "0.2.7") (d (list (d (n "cbor-no-std") (r "^0.3.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "0rkmkkc44v7hjy1x8jvih65sm5alasdgy76wwlrnfd26xc05kfbp")))

(define-public crate-wasm-rpc-0.2.8 (c (n "wasm-rpc") (v "0.2.8") (d (list (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "070yyd6q1jmd52ddwsw5kb0zwhbyqy3pb0qg74ivsgxakgdpzcix")))

(define-public crate-wasm-rpc-0.2.9 (c (n "wasm-rpc") (v "0.2.9") (d (list (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "0mfa8zkn1lcqm75c669dab8n97f6lnl9bpfqw163zyfx2c1pp5qp")))

(define-public crate-wasm-rpc-0.2.10 (c (n "wasm-rpc") (v "0.2.10") (d (list (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "048i5vx6nr8d99wzrs5rh4y579zlrdjzzhqbivvr2vfhlgffks8y")))

(define-public crate-wasm-rpc-0.2.11 (c (n "wasm-rpc") (v "0.2.11") (d (list (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "1cd6pyd7fgz3g83762gdkvrdjrxfrp1xv6pf4h27jghah484py9q")))

(define-public crate-wasm-rpc-0.2.12 (c (n "wasm-rpc") (v "0.2.12") (d (list (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "1lv055q3xyglqgahzdcfzdws9bys3hmyw7mqqbfnw3f6x33gf7vg")))

(define-public crate-wasm-rpc-0.2.13 (c (n "wasm-rpc") (v "0.2.13") (d (list (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "1izwj2d6y1kds65kf66p4zrbhddky2ma86hnhpsdcr7kvj5d86r2")))

(define-public crate-wasm-rpc-0.2.14 (c (n "wasm-rpc") (v "0.2.14") (d (list (d (n "serde_cbor") (r "^0.9.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.2") (d #t) (k 0)))) (h "0jp2an9118kn6v7dbpz8fhh48vzg1axbvh29wx5ji0nfgd3ppc6h")))

(define-public crate-wasm-rpc-0.2.15 (c (n "wasm-rpc") (v "0.2.15") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "1amas35qqcajippp15y7hy1acbi1hrzsjzkn966gb9fff3dr50j6")))

(define-public crate-wasm-rpc-0.2.16 (c (n "wasm-rpc") (v "0.2.16") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "00zcnsxfnqj7i22144c5q1p4wf4zmk21y29sd57mmyvqygdd99z4")))

(define-public crate-wasm-rpc-0.2.17 (c (n "wasm-rpc") (v "0.2.17") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "1r15vkq3p19gq6014749b3pybd9xmi8jr1cvym8nwvmwzk7g1bbf")))

(define-public crate-wasm-rpc-0.2.18 (c (n "wasm-rpc") (v "0.2.18") (d (list (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.3") (d #t) (k 0)) (d (n "serde_cbor") (r "^0.11.1") (d #t) (k 0)))) (h "1giz7b7ihc3inj6zpsklc28417q8bh2g1m3mz60bn6sav3q8kyy0")))

