(define-module (crates-io wa sm wasmlib) #:use-module (crates-io))

(define-public crate-wasmlib-0.1.0 (c (n "wasmlib") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (k 2)))) (h "17cl06va330pmf2sq83c2b8nxf5xvasi4xdxp92yqjnxbdwi8mbr") (f (quote (("default" "console_error_panic_hook"))))))

