(define-module (crates-io wa sm wasmer-artifact) #:use-module (crates-io))

(define-public crate-wasmer-artifact-2.3.0 (c (n "wasmer-artifact") (v "2.3.0") (d (list (d (n "enumset") (r "^1.0") (d #t) (k 0)) (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wasmer-compiler") (r "=2.3.0") (d #t) (k 0)) (d (n "wasmer-types") (r "=2.3.0") (d #t) (k 0)))) (h "09g3ghphyl2y9sb5il1zwcsshs55m2dqhi8fmk98l7cwq8l99bvs")))

