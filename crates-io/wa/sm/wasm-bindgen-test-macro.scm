(define-module (crates-io wa sm wasm-bindgen-test-macro) #:use-module (crates-io))

(define-public crate-wasm-bindgen-test-macro-0.2.15 (c (n "wasm-bindgen-test-macro") (v "0.2.15") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1s1wk7bg106pjswv0gnx2s7s86vyb69s91i9cpnm4nzyx5zzaqj3")))

(define-public crate-wasm-bindgen-test-macro-0.2.16 (c (n "wasm-bindgen-test-macro") (v "0.2.16") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "00kn91wqgs96dq9kq6dgqyn4p6vvfcllkyflhfjnvxsf3d6ardzi")))

(define-public crate-wasm-bindgen-test-macro-0.2.17 (c (n "wasm-bindgen-test-macro") (v "0.2.17") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0dj98kkibqsys0903rzcmjcb2426i0pl98p3l8q33yn22kqlmmgj")))

(define-public crate-wasm-bindgen-test-macro-0.2.18 (c (n "wasm-bindgen-test-macro") (v "0.2.18") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1vaaxybwjhw20fw4iz4ligh76m0yq78syq3wq9by5aiw73janckm")))

(define-public crate-wasm-bindgen-test-macro-0.2.19 (c (n "wasm-bindgen-test-macro") (v "0.2.19") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "125j6issjbw45z2m9h5va4ddn452idsrqrii34dsfwh0sr8p3qys")))

(define-public crate-wasm-bindgen-test-macro-0.2.20 (c (n "wasm-bindgen-test-macro") (v "0.2.20") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "01mnpjhi93kg98389ah7njapcig07gkk27n992rrjwxp43g3j6k8")))

(define-public crate-wasm-bindgen-test-macro-0.2.21 (c (n "wasm-bindgen-test-macro") (v "0.2.21") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1ivdagda92h6hmb4m0z043g9fjqmgga385d3r7mi493b7cjdwqlj")))

(define-public crate-wasm-bindgen-test-macro-0.2.22 (c (n "wasm-bindgen-test-macro") (v "0.2.22") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1lj7sk7k736ysz3wwa8w69b3ln6gr6dxmpldzjrc0rm0m2rari8m")))

(define-public crate-wasm-bindgen-test-macro-0.2.23 (c (n "wasm-bindgen-test-macro") (v "0.2.23") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1zqhnfp135ydn7568aiyh3y9qg8h8c6p4cga3g5rbssdcj48c3hh")))

(define-public crate-wasm-bindgen-test-macro-0.2.24 (c (n "wasm-bindgen-test-macro") (v "0.2.24") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0kivx5j1068mlxy6r93l47ng2qzdarjdk7gy9lrydhxksqziwh95")))

(define-public crate-wasm-bindgen-test-macro-0.2.25 (c (n "wasm-bindgen-test-macro") (v "0.2.25") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1kjr4hpqxag3zdhgbj1wqvbjdpc5vjkliwvqf4i8jbz8sbki7fcp")))

(define-public crate-wasm-bindgen-test-macro-0.2.26 (c (n "wasm-bindgen-test-macro") (v "0.2.26") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0q5v8xj02alwn2siisiimk5ppfnhnxvrkg38kbc8ivs41znbcfji")))

(define-public crate-wasm-bindgen-test-macro-0.2.27 (c (n "wasm-bindgen-test-macro") (v "0.2.27") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1lmyj4ycn5rpjhsl075c3bjs7s06bf9dlqjwc20m2qnxq0z58s00")))

(define-public crate-wasm-bindgen-test-macro-0.2.28 (c (n "wasm-bindgen-test-macro") (v "0.2.28") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0nya7n2i0agqam520vxir4bs5nyifbvnjxv4n289gg5sq7d1sd6p")))

(define-public crate-wasm-bindgen-test-macro-0.2.29 (c (n "wasm-bindgen-test-macro") (v "0.2.29") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1a3wmcyw18q096jr4v4gh4bv16jvja6ndkiksd8yz0yas7z232jk")))

(define-public crate-wasm-bindgen-test-macro-0.2.30 (c (n "wasm-bindgen-test-macro") (v "0.2.30") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0jlqwirmj8vig3vh14w9w20kv1xf6wc4rc7ps3jdmqag9fm3nj10")))

(define-public crate-wasm-bindgen-test-macro-0.2.31 (c (n "wasm-bindgen-test-macro") (v "0.2.31") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0x66ckaf679xsfb3drmh15ip8b9kjfvkg2kwdhkzjgbla9zhvs0l")))

(define-public crate-wasm-bindgen-test-macro-0.2.32 (c (n "wasm-bindgen-test-macro") (v "0.2.32") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "00ipcialmcb67yyiaw5zyw9ifisqcl8k5088zyk23w0yz3fqsjbv")))

(define-public crate-wasm-bindgen-test-macro-0.2.33 (c (n "wasm-bindgen-test-macro") (v "0.2.33") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1wpbhnp3z1bq4jzr77zf2ny8pn5rncr57x1ph84lpha8izdfm5qs")))

(define-public crate-wasm-bindgen-test-macro-0.2.34 (c (n "wasm-bindgen-test-macro") (v "0.2.34") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1m8anxar7klsagv01jskdqn32xwmkrwizfzgjcz308z19xd1y80j")))

(define-public crate-wasm-bindgen-test-macro-0.2.35 (c (n "wasm-bindgen-test-macro") (v "0.2.35") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0c72isaj0sr77lhppkycb70v4pdhzfc3s2zynmvjfvwxyq10ahlk")))

(define-public crate-wasm-bindgen-test-macro-0.2.36 (c (n "wasm-bindgen-test-macro") (v "0.2.36") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0yrj3i59mjn395gb7h5gg6p29c47l06yva24nzm1jnq79spv3zp1")))

(define-public crate-wasm-bindgen-test-macro-0.2.37 (c (n "wasm-bindgen-test-macro") (v "0.2.37") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0banhz31nlb0l1sr19n0xjvz8bv91v4sirvaskbsjcwxlf36xdlj")))

(define-public crate-wasm-bindgen-test-macro-0.2.38 (c (n "wasm-bindgen-test-macro") (v "0.2.38") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1qirvfw95cvv29r8nlh0xxvjn52llfx8lxw53hnp03fl4xzzcysx")))

(define-public crate-wasm-bindgen-test-macro-0.2.39 (c (n "wasm-bindgen-test-macro") (v "0.2.39") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0glpv0i1fc275q46ivn525z13d7xnnragy7iai15c6imk6bp7v69")))

(define-public crate-wasm-bindgen-test-macro-0.2.40 (c (n "wasm-bindgen-test-macro") (v "0.2.40") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0cp9cv413rig5z1fvncwqz8mj5vmsbxq0m5hkpbrsrfk3gqgsk8i")))

(define-public crate-wasm-bindgen-test-macro-0.2.41 (c (n "wasm-bindgen-test-macro") (v "0.2.41") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0w4qqfy7v64x9cjxki5amrchchpalab0hyadfbdm3mkpci7h2ni6")))

(define-public crate-wasm-bindgen-test-macro-0.2.42 (c (n "wasm-bindgen-test-macro") (v "0.2.42") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1q8s04w3gi8jbcb74pwdj55l37arnv9rpyvqkkcyraf9yd87qizb")))

(define-public crate-wasm-bindgen-test-macro-0.2.43 (c (n "wasm-bindgen-test-macro") (v "0.2.43") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "03bjmv8kh60yyh9369kiyyvg62sd6gpzb7p6ax31d7cg7jn5sknq")))

(define-public crate-wasm-bindgen-test-macro-0.2.44 (c (n "wasm-bindgen-test-macro") (v "0.2.44") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0ggw5c2qypb7zkzzdq0hp7swiasnhjpira1m81249mf2s78psm89")))

(define-public crate-wasm-bindgen-test-macro-0.2.45 (c (n "wasm-bindgen-test-macro") (v "0.2.45") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0mbk417m3znzj9fdk6zs4ifgl0dxhq4hipadc5rip3diqp3fmh75")))

(define-public crate-wasm-bindgen-test-macro-0.2.46 (c (n "wasm-bindgen-test-macro") (v "0.2.46") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0rhj9splpnbyghm2xrmm82bhsbrwddzf4pzwbri24p0zhq3qb6ai")))

(define-public crate-wasm-bindgen-test-macro-0.2.47 (c (n "wasm-bindgen-test-macro") (v "0.2.47") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1lzkpsswb6hx3iax81211va8ygrclz669hxjr19gbfcilmm86l0f")))

(define-public crate-wasm-bindgen-test-macro-0.2.48 (c (n "wasm-bindgen-test-macro") (v "0.2.48") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "0n28mr6vncf1k1qr2b5bvfxq4jvqkjdzq0z0ab6w2f5d6v8q3q3l")))

(define-public crate-wasm-bindgen-test-macro-0.2.49 (c (n "wasm-bindgen-test-macro") (v "0.2.49") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "1zn6nw87q1ciklkffid4zmsqmlwk4rjd07fc4v09spb5d94yb1xa")))

(define-public crate-wasm-bindgen-test-macro-0.2.50 (c (n "wasm-bindgen-test-macro") (v "0.2.50") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)))) (h "19bvmw8mqlwh6wkbzgs3cnlkywrv8q2kkqggz6y0p158930xm287")))

(define-public crate-wasm-bindgen-test-macro-0.3.1 (c (n "wasm-bindgen-test-macro") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1gli5ifp140hlygn2x7x3hmlxac2kzcsiiqs0sz1830ccc9302az")))

(define-public crate-wasm-bindgen-test-macro-0.3.2 (c (n "wasm-bindgen-test-macro") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1i9r8nmg0gp6y5i4nwg52zfy2s88wr431famq6wgr9zrm6k9gblk")))

(define-public crate-wasm-bindgen-test-macro-0.3.3 (c (n "wasm-bindgen-test-macro") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0j3ns5349d7z0sdn43a0vmhc0djwxj9h65i4f2sgkf5g4z49ixll")))

(define-public crate-wasm-bindgen-test-macro-0.3.4 (c (n "wasm-bindgen-test-macro") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0p0fwf4i663bwg6cscwncx0bww3f1c0a0agaz9nx75jsaa4p0pxy")))

(define-public crate-wasm-bindgen-test-macro-0.3.5 (c (n "wasm-bindgen-test-macro") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0bq2pqc5digd5s8wh45xm7adlvdmm6lxp5yvbkmg03qxs9qcypcw")))

(define-public crate-wasm-bindgen-test-macro-0.3.6 (c (n "wasm-bindgen-test-macro") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "00ba351627yp2amcp0zq50w4i07jac8ng503xjyjlxz05m3bp12v")))

(define-public crate-wasm-bindgen-test-macro-0.3.7 (c (n "wasm-bindgen-test-macro") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1205pbigq6vqr2jmbff7zvx4rkzdjhbf0kw9043xsdif1909s4nh")))

(define-public crate-wasm-bindgen-test-macro-0.3.8 (c (n "wasm-bindgen-test-macro") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0kybf3shpp8ysz4v4j259d7vad9kw5bs4i4dlfrs895bhdp7m0wp")))

(define-public crate-wasm-bindgen-test-macro-0.3.9 (c (n "wasm-bindgen-test-macro") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0nfx65823cg2f0izja8hmpdx9kfyh8cp3d8irz82n6fwrs6ryi29")))

(define-public crate-wasm-bindgen-test-macro-0.3.10 (c (n "wasm-bindgen-test-macro") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0rw4i98s2xhmr8nm8ycw11ir4zybxiadhvmvnhgppam2g36qcbyg")))

(define-public crate-wasm-bindgen-test-macro-0.3.11 (c (n "wasm-bindgen-test-macro") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "09c0s8vai7qfqmf092k34wjswhr7gyd0yf74cghn5i2m13pqf1qa")))

(define-public crate-wasm-bindgen-test-macro-0.3.12 (c (n "wasm-bindgen-test-macro") (v "0.3.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "09rnc2j5pcdhmwyw7j151dq2iah7m4pfcsd0xwjsw6vwa39chn63")))

(define-public crate-wasm-bindgen-test-macro-0.3.13 (c (n "wasm-bindgen-test-macro") (v "0.3.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1isa6avsbgprq899y60iqc226dvwrkz7ghc8w6j9rh8i7w4ihbic")))

(define-public crate-wasm-bindgen-test-macro-0.3.14 (c (n "wasm-bindgen-test-macro") (v "0.3.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1z226v40hj4830nilz350gqln6wr8y88pfhxv24wvr4i19j04g9l")))

(define-public crate-wasm-bindgen-test-macro-0.3.15 (c (n "wasm-bindgen-test-macro") (v "0.3.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0bwgr5f1yag7ylfzhl6b3spnljgh06ripakw5r75b8pskw8g2ars")))

(define-public crate-wasm-bindgen-test-macro-0.3.16 (c (n "wasm-bindgen-test-macro") (v "0.3.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1p773a1zp69fxgzpmirf77i2bhdqxjm48xixz5ncrm7slzxn6j8m")))

(define-public crate-wasm-bindgen-test-macro-0.3.17 (c (n "wasm-bindgen-test-macro") (v "0.3.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1f5k2afi41nracl9mh51kpv30mfcp9s51z4iklvqkfjcx3l1qqai")))

(define-public crate-wasm-bindgen-test-macro-0.3.18 (c (n "wasm-bindgen-test-macro") (v "0.3.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "11d5w6wdjd2if1n7jrjvhgi52pn094m51nxpn65fwfblprkrryz8")))

(define-public crate-wasm-bindgen-test-macro-0.3.19 (c (n "wasm-bindgen-test-macro") (v "0.3.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "12s3h3g1f81afv0rk8idgw2mylgh5q6a30wy5yxc4940p537pq17")))

(define-public crate-wasm-bindgen-test-macro-0.3.20 (c (n "wasm-bindgen-test-macro") (v "0.3.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "16y1ny7nwv8cdwf6pg6yk0mp8aqc9p168ijlslqwf035y0rrq6f3")))

(define-public crate-wasm-bindgen-test-macro-0.3.21 (c (n "wasm-bindgen-test-macro") (v "0.3.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1m5pcjkj72a61vzzlx6dkgwwi015rkkjklyhrpv2vncmyh109x23")))

(define-public crate-wasm-bindgen-test-macro-0.3.22 (c (n "wasm-bindgen-test-macro") (v "0.3.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0db7ls4fkl88sq1zlxqzwakrzxrh4m2sf9gwg16gdnik76yw19hh")))

(define-public crate-wasm-bindgen-test-macro-0.3.23 (c (n "wasm-bindgen-test-macro") (v "0.3.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1dm8d13sn3hg5923m3gn95015zsg84ip5j15ky75h95zz6l56qga")))

(define-public crate-wasm-bindgen-test-macro-0.3.24 (c (n "wasm-bindgen-test-macro") (v "0.3.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "10p7s3zg8pinmb99yi5sgrnqbgf69h821xwbv7q1wm7kdky46ifx")))

(define-public crate-wasm-bindgen-test-macro-0.3.25 (c (n "wasm-bindgen-test-macro") (v "0.3.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "155imfqaf8jlwfcyyxxv7d0c3zysa3wvx0a3lgrr4ns3z1f82n9q")))

(define-public crate-wasm-bindgen-test-macro-0.3.26 (c (n "wasm-bindgen-test-macro") (v "0.3.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0mpbgrws77qdaxrw98jgkkqm8f1ph62n1r1cvm2bi1qfim9iwmiv")))

(define-public crate-wasm-bindgen-test-macro-0.3.27 (c (n "wasm-bindgen-test-macro") (v "0.3.27") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1m1vfk2pg2mxwvf9qn5r18cpx5zji6k4lz3lp9q797z33md6fj3q")))

(define-public crate-wasm-bindgen-test-macro-0.3.28 (c (n "wasm-bindgen-test-macro") (v "0.3.28") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "124ng2j16jf8lfdgq1lg83c7zka42fzizdaddnl9dsyz52bgf1k0")))

(define-public crate-wasm-bindgen-test-macro-0.3.29 (c (n "wasm-bindgen-test-macro") (v "0.3.29") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1pckh9wjah0shwlcjq90xn9yzqg75x8iaykmm5lkprxm9pa61rfh")))

(define-public crate-wasm-bindgen-test-macro-0.3.30 (c (n "wasm-bindgen-test-macro") (v "0.3.30") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0k3v7fn5ndx9njckfys7bmkm3zi7jgfb6pya46ir6q6ch9pmlz57")))

(define-public crate-wasm-bindgen-test-macro-0.3.31 (c (n "wasm-bindgen-test-macro") (v "0.3.31") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0qv4bfyn96hwz4i51qkpkigzgm0rq4phds9amkyclwryyd5mkbc8")))

(define-public crate-wasm-bindgen-test-macro-0.3.32 (c (n "wasm-bindgen-test-macro") (v "0.3.32") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0mhdl64m6zm4sx3zsw2j1kbh56bac88bwl16q7v3q2p90dmd6l31")))

(define-public crate-wasm-bindgen-test-macro-0.3.33 (c (n "wasm-bindgen-test-macro") (v "0.3.33") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "19wwlh39bhka7lmzbxs0whzcpwf440al1kw2kx670vq1zhyxm0s6")))

(define-public crate-wasm-bindgen-test-macro-0.3.34 (c (n "wasm-bindgen-test-macro") (v "0.3.34") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1s9vnkrmmq5fiv0j0ifvbs7q5ffgxwbg1r0zcryp3cdkwsd7ad07")))

(define-public crate-wasm-bindgen-test-macro-0.3.35 (c (n "wasm-bindgen-test-macro") (v "0.3.35") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1llrdfprbgcab2l8816d5rihf7q5mi0lk71s9w6c1skllnwskfhx")))

(define-public crate-wasm-bindgen-test-macro-0.3.36 (c (n "wasm-bindgen-test-macro") (v "0.3.36") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "18vbjgg7xcn0wrvhy7bjwganb6pm2ai4y0ffpkkmhjbw5yniz37i")))

(define-public crate-wasm-bindgen-test-macro-0.3.37 (c (n "wasm-bindgen-test-macro") (v "0.3.37") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0xd5245xixdhj33nf6xbzg85vrmjv5vhw0hf2gnk0sc3ikfr7fgc") (r "1.56")))

(define-public crate-wasm-bindgen-test-macro-0.3.38 (c (n "wasm-bindgen-test-macro") (v "0.3.38") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "derive" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0dfpvrsl5rx5hb1d01ix4ys9ww6xqb7figp66zx69dvbfnmwngs9") (r "1.57")))

(define-public crate-wasm-bindgen-test-macro-0.3.39 (c (n "wasm-bindgen-test-macro") (v "0.3.39") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "derive" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "12gyjjrz873hrxxv0mhrzyjyprxjvwy12kczs2gh76lc83slaikr") (r "1.57")))

(define-public crate-wasm-bindgen-test-macro-0.3.40 (c (n "wasm-bindgen-test-macro") (v "0.3.40") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "derive" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0a1rr50wbni3cfk2xk131vdc9bp42jd74b002qknv9jxzvmjl1vh") (r "1.57")))

(define-public crate-wasm-bindgen-test-macro-0.3.41 (c (n "wasm-bindgen-test-macro") (v "0.3.41") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "derive" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "12bgbvygyi04d1gcrgl7w7m94mn7is59f7ds5cqmfs30a1sin8d5") (r "1.57")))

(define-public crate-wasm-bindgen-test-macro-0.3.42 (c (n "wasm-bindgen-test-macro") (v "0.3.42") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro" "derive" "printing"))) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1w3ypw6b0ffyyx0w83mlb4bw1jmjgza9kdxyjk5h6bhs6lwrgy5p") (r "1.57")))

