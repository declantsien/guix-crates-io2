(define-module (crates-io wa sm wasm-coredump-encoder) #:use-module (crates-io))

(define-public crate-wasm-coredump-encoder-0.1.10 (c (n "wasm-coredump-encoder") (v "0.1.10") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.10") (d #t) (k 0)))) (h "1pfc91g9hmnw4q975f9qch2paz2szqr859h93x23j2crgfz34w50")))

(define-public crate-wasm-coredump-encoder-0.1.11 (c (n "wasm-coredump-encoder") (v "0.1.11") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.11") (d #t) (k 0)))) (h "0047bk2df0v1z1sd9mgyr754frny6zry49qh1xbn70x8yg6rj31g")))

(define-public crate-wasm-coredump-encoder-0.1.12 (c (n "wasm-coredump-encoder") (v "0.1.12") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.12") (d #t) (k 0)))) (h "1cv59h23vsb3iwbd4mrkdagd6aky7mfknjvlppfm8glyk3lw0mdq")))

(define-public crate-wasm-coredump-encoder-0.1.14 (c (n "wasm-coredump-encoder") (v "0.1.14") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.14") (d #t) (k 0)))) (h "1dlf5ayy2nfng7n695hxcijy698l6pvrb1gw46zac4qyx56n4c01")))

(define-public crate-wasm-coredump-encoder-0.1.15 (c (n "wasm-coredump-encoder") (v "0.1.15") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.15") (d #t) (k 0)))) (h "0iv79k1cchwjhcjp4ckwnvvm9wydggkyjplfmbf7wm1d6lg90y3b")))

(define-public crate-wasm-coredump-encoder-0.1.16 (c (n "wasm-coredump-encoder") (v "0.1.16") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.16") (d #t) (k 0)))) (h "1rgrwnvc74nxwr4096m8xmbif4zibh0hy7kh0694wsb7zmjmc718")))

(define-public crate-wasm-coredump-encoder-0.1.17 (c (n "wasm-coredump-encoder") (v "0.1.17") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.17") (d #t) (k 0)))) (h "02ik7hc0cfw62dghgcxqdacnwmq28m2p2mng788pq2ppf8v4fms8")))

(define-public crate-wasm-coredump-encoder-0.1.18 (c (n "wasm-coredump-encoder") (v "0.1.18") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.18") (d #t) (k 0)))) (h "1p9k30g9kd6gkx3xxy5fjpaxm6rrwqr82j3fh65ml4p0pl76pj60")))

(define-public crate-wasm-coredump-encoder-0.1.19 (c (n "wasm-coredump-encoder") (v "0.1.19") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.19") (d #t) (k 0)))) (h "19rbm68v8m00hsvp8r13sjwqqz6ii91zf8bx013h67354vgy1bwa")))

(define-public crate-wasm-coredump-encoder-0.1.20 (c (n "wasm-coredump-encoder") (v "0.1.20") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.20") (d #t) (k 0)))) (h "0hrh58bpxkm23i0h2i4m0v206nwylqdxjf6116854zr5xq888xjc")))

(define-public crate-wasm-coredump-encoder-0.1.21 (c (n "wasm-coredump-encoder") (v "0.1.21") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.21") (d #t) (k 0)))) (h "0xlfqzmghcr76zz6q92vjzkps9zxf3fc8rn347a93jhdqwfspn2x")))

(define-public crate-wasm-coredump-encoder-0.1.22 (c (n "wasm-coredump-encoder") (v "0.1.22") (d (list (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.22") (d #t) (k 0)))) (h "08sn5kfg9fcclnxvjjzc8v38dh09ssqdgrzwjgh0qwh4wv7nrwzn")))

