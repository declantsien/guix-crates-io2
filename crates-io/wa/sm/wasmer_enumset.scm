(define-module (crates-io wa sm wasmer_enumset) #:use-module (crates-io))

(define-public crate-wasmer_enumset-1.0.1 (c (n "wasmer_enumset") (v "1.0.1") (d (list (d (n "bincode") (r "^1.0") (f (quote ("i128"))) (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (k 0)) (d (n "rustversion") (r "^1.0.2") (d #t) (k 2)) (d (n "serde2") (r "^1.0.91") (o #t) (k 0) (p "serde")) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.24") (d #t) (k 2)) (d (n "wasmer_enumset_derive") (r "^0.5.0") (d #t) (k 0)))) (h "1n5jakimap6mmrqws7mfd657sx5v3gxldpzhvybgsiyjyz0qq26g") (f (quote (("serde" "serde2" "wasmer_enumset_derive/serde"))))))

