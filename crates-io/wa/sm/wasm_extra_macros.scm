(define-module (crates-io wa sm wasm_extra_macros) #:use-module (crates-io))

(define-public crate-wasm_extra_macros-0.1.0 (c (n "wasm_extra_macros") (v "0.1.0") (d (list (d (n "derive-syn-parse") (r "^0.1.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0q1zgq8pq305p5259hxbc95i5az1rbm455h4chhmmcqx2kz8ki99") (r "1.56.1")))

(define-public crate-wasm_extra_macros-0.1.1 (c (n "wasm_extra_macros") (v "0.1.1") (d (list (d (n "derive-syn-parse") (r "^0.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0hmi8hcifr9bikyknyzb4m304ni52br3rh8djjfkcqr2zs5difgd") (r "1.56.1")))

