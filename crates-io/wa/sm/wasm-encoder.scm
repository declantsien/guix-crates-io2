(define-module (crates-io wa sm wasm-encoder) #:use-module (crates-io))

(define-public crate-wasm-encoder-0.1.0 (c (n "wasm-encoder") (v "0.1.0") (d (list (d (n "leb128") (r ">=0.2.4, <0.3.0") (d #t) (k 0)))) (h "1jxar99z58kdm6g326bq1a7lgbcqci72gk951c4y1y8wb1y1p2a9")))

(define-public crate-wasm-encoder-0.2.0 (c (n "wasm-encoder") (v "0.2.0") (d (list (d (n "leb128") (r "^0.2.4") (d #t) (k 0)))) (h "1qd520mgd0k7f31r2wphqlfvblvxl246b8bpwjb4zf08kvwym2gd")))

(define-public crate-wasm-encoder-0.3.0 (c (n "wasm-encoder") (v "0.3.0") (d (list (d (n "leb128") (r "^0.2.4") (d #t) (k 0)))) (h "05swy1f32yyxqcig7zqmkvixnh0db676ah3dmvny0kf5vfhr4lfq")))

(define-public crate-wasm-encoder-0.3.1 (c (n "wasm-encoder") (v "0.3.1") (d (list (d (n "leb128") (r "^0.2.4") (d #t) (k 0)))) (h "0w2n16qqakla9ajcq02jxhrgx4jrig79b770hlxvgpsdnzwmq7ny")))

(define-public crate-wasm-encoder-0.4.0 (c (n "wasm-encoder") (v "0.4.0") (d (list (d (n "leb128") (r "^0.2.4") (d #t) (k 0)))) (h "1ial0sf6c2a42y7nxmmlny1cqnd10914bbkraijscjj6y4nacpy7")))

(define-public crate-wasm-encoder-0.4.1 (c (n "wasm-encoder") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "11kgin093ssabp2dyj2fi2jygyffma3cyz9i12ra899b9yfr9d2i")))

(define-public crate-wasm-encoder-0.5.0 (c (n "wasm-encoder") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0rlf3s5wpry16xzixvr2x71lr6nn8iawxmxc1rgw1kgl84szpcvj")))

(define-public crate-wasm-encoder-0.6.0 (c (n "wasm-encoder") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1gfrdrkcz3c1pcik7xzaflvn4gn417swsma0ih0g0x4cqrscraic")))

(define-public crate-wasm-encoder-0.7.0 (c (n "wasm-encoder") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0w17vn63qpxn8n7x8qgc0wxk13dk5v40iq6dgs04jj274dybvks9")))

(define-public crate-wasm-encoder-0.8.0 (c (n "wasm-encoder") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1c4vq8bdca4innh0g3wz9fnhryac3v0sd5m6b1mc0v7468b3a36v")))

(define-public crate-wasm-encoder-0.9.0 (c (n "wasm-encoder") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1y537cjwj9am6nspyj0bp0cdki79gqkmn5im25dxi684bvlbk4ax")))

(define-public crate-wasm-encoder-0.10.0 (c (n "wasm-encoder") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1awhs8bm07mj6225hmy35mzijx78n4q9nz430z272vy4bzs9p7da")))

(define-public crate-wasm-encoder-0.11.0 (c (n "wasm-encoder") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0lrpizdyz2rkw39wmh7771gyx5k45w2hzbj4gqnzjyhl378khld3")))

(define-public crate-wasm-encoder-0.12.0 (c (n "wasm-encoder") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1akcsfbjghi1f1mvznwhnfaindclgdr6rpm9ihsqk9lnn2avjirb")))

(define-public crate-wasm-encoder-0.13.0 (c (n "wasm-encoder") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1skmk8v935lwj1mh3dsgr1ridnr11r38jxc97npddzx5cxrc3w1i")))

(define-public crate-wasm-encoder-0.14.0 (c (n "wasm-encoder") (v "0.14.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0q9fxpwzdpcz7rj6hhcchbdylws1g1kfvk9cpjk3gf79gzl6hq7p")))

(define-public crate-wasm-encoder-0.15.0 (c (n "wasm-encoder") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0szsdhxd33dqfcjs7iyh38lxcwprz539yfplidz0xsxdzljzs1c9")))

(define-public crate-wasm-encoder-0.16.0 (c (n "wasm-encoder") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1xwjb0v83dy8i8aidnzll39nd1zyniqas4pcjxynjwdfvakwahyl")))

(define-public crate-wasm-encoder-0.17.0 (c (n "wasm-encoder") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1vlnkk2g6rl8zzk7d0aqv6f6sdj8wncd5r4ajq85ppm6f0fafz3y")))

(define-public crate-wasm-encoder-0.18.0 (c (n "wasm-encoder") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0b0yb2425zkc824a1iyz5qggkbxrs15pw6vhbk22q6b1bn6wjjn6")))

(define-public crate-wasm-encoder-0.19.0 (c (n "wasm-encoder") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0pwv3lhh1i7qj0w9nnjhdl9qdxs7ax4b0bm6d80kawzax246x0f5")))

(define-public crate-wasm-encoder-0.19.1 (c (n "wasm-encoder") (v "0.19.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0hbcy1l5zk2vvv6jpk4x295d1cp72hds1x680gmd85kaa6mws94l")))

(define-public crate-wasm-encoder-0.20.0 (c (n "wasm-encoder") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "050605gjh5f34acav4rndhjjy9nnmcx2491wb758rvd6cq52wqq5")))

(define-public crate-wasm-encoder-0.21.0 (c (n "wasm-encoder") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "02863sb82p2q130ls7xaw1md00p0fd2vpplp69h32mrjggkjzar9")))

(define-public crate-wasm-encoder-0.22.0 (c (n "wasm-encoder") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "04085x3xpg9siq8q17qpqzjqjlhri7y1md58q5d3bpsbw7h6n4pg")))

(define-public crate-wasm-encoder-0.22.1 (c (n "wasm-encoder") (v "0.22.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1kcsgah4n56iwpnm6g9p7vx62ih54vxw77fi3lgk3nf2rirl4n4s")))

(define-public crate-wasm-encoder-0.23.0 (c (n "wasm-encoder") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0a1lapg23hwwpcn2dyzqb826gvn3wqrnvn44fn76qhwmj304nghw")))

(define-public crate-wasm-encoder-0.24.0 (c (n "wasm-encoder") (v "0.24.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1fh9vn8baqbmhhdr6nz9kjqp8qf1ndvajmx4nj07190lsss56ibh")))

(define-public crate-wasm-encoder-0.24.1 (c (n "wasm-encoder") (v "0.24.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1ibfjjz803v9j1ygbcyzjiwnh4hcqjn1kz9dw4f9046r4xidbxv8")))

(define-public crate-wasm-encoder-0.25.0 (c (n "wasm-encoder") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1dx26l0w6jqkywg6la6n24xy37afmpfpwlmgfr6wkvh99wy8bzsf")))

(define-public crate-wasm-encoder-0.26.0 (c (n "wasm-encoder") (v "0.26.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1vw0kmccww33jxdqy50mm0m245rw3crpaybfy6nrisqarmphnpfh")))

(define-public crate-wasm-encoder-0.27.0 (c (n "wasm-encoder") (v "0.27.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "10f5v7fgz3k0bjb29ifyh74ariddb32cycip3mlr1dwxf3f56w77")))

(define-public crate-wasm-encoder-0.28.0 (c (n "wasm-encoder") (v "0.28.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "05z5bf8sms34cqm1bjvxcmmbkm423bd04jkr4nj1vqsh9m34zjc3")))

(define-public crate-wasm-encoder-0.29.0 (c (n "wasm-encoder") (v "0.29.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1098j622k5mgcjqfdgdr3j3pqdxq81jk3gir59hz7szajayivi0q")))

(define-public crate-wasm-encoder-0.30.0 (c (n "wasm-encoder") (v "0.30.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1a1n0kw48qgl7sq50v4nddk5ldvp4lvh3hxcb17z9jq4irvyky5j")))

(define-public crate-wasm-encoder-0.31.0 (c (n "wasm-encoder") (v "0.31.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0aiapisfb4cadjwb29swnrg5a5rivfpb40llcxrvizvmlnsd38q6")))

(define-public crate-wasm-encoder-0.31.1 (c (n "wasm-encoder") (v "0.31.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "05nyajlmqgvyq7ynqhs1hbsmkafkjrj4dyqszygkklgyx8h3yxj1")))

(define-public crate-wasm-encoder-0.32.0 (c (n "wasm-encoder") (v "0.32.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1xz5bbn48a3v5bcqz2rdsqsr5mh1jkr95qpy93myl5jr460lx9hv")))

(define-public crate-wasm-encoder-0.33.0 (c (n "wasm-encoder") (v "0.33.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0sx081yhwp89gas7m4y23sd91b08y0f6n95l0gh36q65gzm8mazl")))

(define-public crate-wasm-encoder-0.33.1 (c (n "wasm-encoder") (v "0.33.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1bl3j6rf6qas24i50p2wji6kwsyhq3xnq47d9gswilsk79rf17dk")))

(define-public crate-wasm-encoder-0.33.2 (c (n "wasm-encoder") (v "0.33.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0v08xavdihr25mdl9qjzzmx49wbllrhlpnx8qcjlhgibcy4hq61l")))

(define-public crate-wasm-encoder-0.34.0 (c (n "wasm-encoder") (v "0.34.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.114.0") (d #t) (k 0)))) (h "060b1m4682zr8cjl1brgmn8cd317nw5rb4d57p69xa129ycm8ylw")))

(define-public crate-wasm-encoder-0.34.1 (c (n "wasm-encoder") (v "0.34.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.114.0") (o #t) (d #t) (k 0)))) (h "17fza22mw6zxk4w3rqzb34h190yqzq9cgjl09spx2biydbh98jpi")))

(define-public crate-wasm-encoder-0.35.0 (c (n "wasm-encoder") (v "0.35.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.115.0") (o #t) (d #t) (k 0)))) (h "1jha2lmr5m7rs9b95da7ip3xsfqzjmwmag2794yhv9xhnnhhpacw")))

(define-public crate-wasm-encoder-0.36.0 (c (n "wasm-encoder") (v "0.36.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.116.0") (o #t) (d #t) (k 0)))) (h "0sbna5ccfhhxs6vhzykx5v9phgyx1ch03h03bdkyr642yi1bjf4j")))

(define-public crate-wasm-encoder-0.36.1 (c (n "wasm-encoder") (v "0.36.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.116.0") (o #t) (d #t) (k 0)))) (h "0f4dc32w5r7ac1ixhzj0xr5d54dls2xvycd89zgihygq1gi0pbjk")))

(define-public crate-wasm-encoder-0.36.2 (c (n "wasm-encoder") (v "0.36.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.116.1") (o #t) (d #t) (k 0)))) (h "08flkhlrsxzq9s0p1ywz41kv2q5gw93wmzvnjya6ni7jyidn8aw2")))

(define-public crate-wasm-encoder-0.37.0 (c (n "wasm-encoder") (v "0.37.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.117.0") (o #t) (d #t) (k 0)))) (h "0gg4cp15vz7wlc9lr43lsxqqpynn3jfbx85hyphbx7dn824mw4vx")))

(define-public crate-wasm-encoder-0.38.0 (c (n "wasm-encoder") (v "0.38.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.118.0") (o #t) (d #t) (k 0)))) (h "0ni33badq2nj3fj5lkbpb1zy12myf5mlpbkjvfxxm8rkz5fvq2bv")))

(define-public crate-wasm-encoder-0.38.1 (c (n "wasm-encoder") (v "0.38.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.118.1") (o #t) (d #t) (k 0)))) (h "0gwzfwals53swmp18n4mwaqx9jldrgy4647xw97pz76yhhcbblha")))

(define-public crate-wasm-encoder-0.39.0 (c (n "wasm-encoder") (v "0.39.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.119.0") (o #t) (d #t) (k 0)))) (h "07agnf0dsx9g1whsycm39m88cl7lcbqrlypm2a2j6q2743b9a50i")))

(define-public crate-wasm-encoder-0.40.0 (c (n "wasm-encoder") (v "0.40.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.120.0") (o #t) (d #t) (k 0)))) (h "09nq03bp95cwg9qqx82s65z6ciqf7dcs1336x28awsc92rjfnqni")))

(define-public crate-wasm-encoder-0.41.0 (c (n "wasm-encoder") (v "0.41.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.121.0") (o #t) (d #t) (k 0)))) (h "0wd06181w3ip6jp5ag3lz37apgsn3yqymnsyzckpsqw8cdywm6z0")))

(define-public crate-wasm-encoder-0.41.1 (c (n "wasm-encoder") (v "0.41.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.121.1") (o #t) (d #t) (k 0)))) (h "137nzjjja1js3ncas6z8r31bari3msnlv339216dm3fl4gk4vqbc")))

(define-public crate-wasm-encoder-0.41.2 (c (n "wasm-encoder") (v "0.41.2") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.121.2") (o #t) (d #t) (k 0)))) (h "1bl7a871fw3dwrii2swqcn9x1g4hi8c98dfjvs6r13riv2jrfbwp")))

(define-public crate-wasm-encoder-0.200.0 (c (n "wasm-encoder") (v "0.200.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.200.0") (o #t) (d #t) (k 0)))) (h "1fc4mcrj73ygd4zn67pqymn51iyvxvgm1f4mc2m7ipdxiw6gpqxr")))

(define-public crate-wasm-encoder-0.201.0 (c (n "wasm-encoder") (v "0.201.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.201.0") (o #t) (d #t) (k 0)))) (h "06lgfkb0r1dw0hc1mqgnrkg935h1qb668gq1kf0hc07n3mrx5ixr")))

(define-public crate-wasm-encoder-0.202.0 (c (n "wasm-encoder") (v "0.202.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.202.0") (o #t) (d #t) (k 0)))) (h "0yp655s6w759hl117nvdz5gpyixdng5rhsliq6iplpvzb8v0dldz")))

(define-public crate-wasm-encoder-0.203.0 (c (n "wasm-encoder") (v "0.203.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.203.0") (o #t) (d #t) (k 0)))) (h "0l6v8b56yypwk83p2yqciz5vb17j58pc89m9gbal74cx1mmb9qw7")))

(define-public crate-wasm-encoder-0.204.0 (c (n "wasm-encoder") (v "0.204.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.204.0") (o #t) (d #t) (k 0)))) (h "0l8arc6d3bnxd8gcf6jh8wgfb0zhvckd4af7bqrw1pyqnklvs32h")))

(define-public crate-wasm-encoder-0.205.0 (c (n "wasm-encoder") (v "0.205.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.205.0") (o #t) (d #t) (k 0)))) (h "0p0figngs07q176i1ghr01qkf34lspxpw2pv3hyg6r6iccsmpsch")))

(define-public crate-wasm-encoder-0.206.0 (c (n "wasm-encoder") (v "0.206.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.206.0") (o #t) (d #t) (k 0)))) (h "07ivvjs6k6rfpcv2hmwws39xg749hpk0p9w0dl4rkw9p24p32nfp")))

(define-public crate-wasm-encoder-0.207.0 (c (n "wasm-encoder") (v "0.207.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.207.0") (f (quote ("std"))) (o #t) (k 0)))) (h "05zynckhybg946r2hrwzy753c0nzf3vf5nvs2pcy1bmfndpk15nr")))

(define-public crate-wasm-encoder-0.208.0 (c (n "wasm-encoder") (v "0.208.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.208.0") (f (quote ("std"))) (o #t) (k 0)))) (h "0wgfhv13ag6by9ai0kgvr7rjs31gpfadvrxjjsqawml0hcwzph5v") (r "1.76.0")))

(define-public crate-wasm-encoder-0.208.1 (c (n "wasm-encoder") (v "0.208.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.208.1") (f (quote ("std"))) (o #t) (k 0)))) (h "05zscgyd8q9wp45mr3m65jgk3jqjg4lc5v20ir3mixgp897fh9b4") (r "1.76.0")))

(define-public crate-wasm-encoder-0.209.0 (c (n "wasm-encoder") (v "0.209.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.209.0") (f (quote ("std"))) (o #t) (k 0)))) (h "0gb06q1qsvxpf7q9ndway5y708akmirfvx551dwjavjssg6144c2") (r "1.76.0")))

(define-public crate-wasm-encoder-0.209.1 (c (n "wasm-encoder") (v "0.209.1") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 2)) (d (n "leb128") (r "^0.2.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "wasmparser") (r "^0.209.1") (f (quote ("std"))) (o #t) (k 0)))) (h "19xjaf6myzaxyfj3vhbjrf9p4ngy25z8pm3brqrfgnl2d0rhajkv") (r "1.76.0")))

