(define-module (crates-io wa sm wasm-graph) #:use-module (crates-io))

(define-public crate-wasm-graph-0.1.0 (c (n "wasm-graph") (v "0.1.0") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "parity_wasm") (r "^0.40") (d #t) (k 0) (p "parity-wasm")) (d (n "wabt") (r "^0.9") (d #t) (k 2)))) (h "0p7lwh0hc0i6x6f2snly5bzr437s4605rd81w0yrbarfrgh27mmv")))

(define-public crate-wasm-graph-0.1.1 (c (n "wasm-graph") (v "0.1.1") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "parity_wasm") (r "^0.40") (d #t) (k 0) (p "parity-wasm")) (d (n "wabt") (r "^0.9") (d #t) (k 2)))) (h "1yxvkrv1xcfq2c9xbamv9p95ydjz77g2nxw35b7z7lh0r6csf8fw")))

(define-public crate-wasm-graph-0.1.2 (c (n "wasm-graph") (v "0.1.2") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "parity_wasm") (r "^0.40") (d #t) (k 0) (p "parity-wasm")) (d (n "wabt") (r "^0.9") (d #t) (k 2)))) (h "0cxppv2b0kfdwkj9b5zxk4pargnpkq7sik9ln7wjp4vwflnd8971")))

(define-public crate-wasm-graph-0.2.0 (c (n "wasm-graph") (v "0.2.0") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "parity_wasm") (r "^0.45") (d #t) (k 0) (p "parity-wasm")) (d (n "wabt") (r "^0.9") (d #t) (k 2)))) (h "0h65p45hp7m7vkgaq2khm9kqcpvb605zkvxri590ydnv7alibn1p")))

(define-public crate-wasm-graph-0.2.1 (c (n "wasm-graph") (v "0.2.1") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "parity_wasm") (r "^0.45") (d #t) (k 0) (p "parity-wasm")) (d (n "wabt") (r "^0.9") (d #t) (k 2)))) (h "1g0nmnjxldg3bwsk0ln7g8ia58gxpim4ngya4q0klxwa5mkd6f1w")))

(define-public crate-wasm-graph-0.2.2 (c (n "wasm-graph") (v "0.2.2") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "parity_wasm") (r "^0.45") (d #t) (k 0) (p "parity-wasm")) (d (n "wabt") (r "^0.9") (d #t) (k 2)))) (h "0lwx078n05qw38s2l9f361dkcjr5z193j87zysg723x3kyybm06z")))

(define-public crate-wasm-graph-0.2.3 (c (n "wasm-graph") (v "0.2.3") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "parity_wasm") (r "^0.45") (d #t) (k 0) (p "parity-wasm")) (d (n "wabt") (r "^0.9") (d #t) (k 2)))) (h "0h8qv20cg5cg0k50lc52ad41mxq3dcim4zwg92xp20wyikid9cmd")))

(define-public crate-wasm-graph-0.2.4 (c (n "wasm-graph") (v "0.2.4") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "parity_wasm") (r "^0.45") (d #t) (k 0) (p "parity-wasm")) (d (n "wabt") (r "^0.9") (d #t) (k 2)))) (h "1gr46smr696xwwsh52g7aavn60ry5ykdhnxmrvjqlldr7i3c3xha")))

(define-public crate-wasm-graph-0.2.5 (c (n "wasm-graph") (v "0.2.5") (d (list (d (n "indoc") (r "^0.3") (d #t) (k 2)) (d (n "parity_wasm") (r "^0.45") (d #t) (k 0) (p "parity-wasm")) (d (n "wabt") (r "^0.9") (d #t) (k 2)))) (h "0x5dvgxjp79wsa2rd7yac8j2sikvvc6sg1si0m8jzay92qiqa206")))

