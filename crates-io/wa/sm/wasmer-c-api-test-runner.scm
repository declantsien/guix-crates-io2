(define-module (crates-io wa sm wasmer-c-api-test-runner) #:use-module (crates-io))

(define-public crate-wasmer-c-api-test-runner-3.0.0-beta.2 (c (n "wasmer-c-api-test-runner") (v "3.0.0-beta.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)))) (h "0w2sizi28jay80z88bi7pjyicrxylxdsrlvskidpj9inanfa2s55")))

(define-public crate-wasmer-c-api-test-runner-3.0.0-rc.1 (c (n "wasmer-c-api-test-runner") (v "3.0.0-rc.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0q6zipark2kimvbg7aix77cslbpf32jj80hdfkh6hfwz82417pxd")))

(define-public crate-wasmer-c-api-test-runner-3.0.0-rc.2 (c (n "wasmer-c-api-test-runner") (v "3.0.0-rc.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "07bv77x9i0pk6r4ljypnpkrcafvh5z16x9vpndg96k6by1qzl27m")))

(define-public crate-wasmer-c-api-test-runner-3.0.0-rc.3 (c (n "wasmer-c-api-test-runner") (v "3.0.0-rc.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1s5jpmqnq3plbv074zms4syg7hscabmhq70j48sgnbg81sxz0am7")))

(define-public crate-wasmer-c-api-test-runner-3.0.0-rc.4 (c (n "wasmer-c-api-test-runner") (v "3.0.0-rc.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1m4cxp7ndvlpan864apndxhr7awvw26c4zmzp5q8vphvhhrp22gm")))

(define-public crate-wasmer-c-api-test-runner-3.0.0 (c (n "wasmer-c-api-test-runner") (v "3.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "026va1lxw1zzg88z8kpmw7pn2b8n8594xgn3kgxf8aj2lzczdifq")))

(define-public crate-wasmer-c-api-test-runner-3.0.1 (c (n "wasmer-c-api-test-runner") (v "3.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "15yz4qln35zald54cnv7r5y17wzawkkzf947cfh3llpq9lay190s")))

(define-public crate-wasmer-c-api-test-runner-3.0.2 (c (n "wasmer-c-api-test-runner") (v "3.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1zhmq9zmc80ffhzm5yspcm8rqw8hnx7kys4ir6sd9d0fgws8na6c")))

(define-public crate-wasmer-c-api-test-runner-3.1.0 (c (n "wasmer-c-api-test-runner") (v "3.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0nsaxc02vz0vcw3an7fjh6xg501kvk540yl1xbmbzjlaj276yhxd")))

(define-public crate-wasmer-c-api-test-runner-3.2.0-alpha.1 (c (n "wasmer-c-api-test-runner") (v "3.2.0-alpha.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0yldmhz66ycxb2y3cdgx1y4lnrbmx7c3fhvclf375nqgzgv3wmxr")))

(define-public crate-wasmer-c-api-test-runner-3.0.3 (c (n "wasmer-c-api-test-runner") (v "3.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0n5s960fms3ycxna3kd2nzpf4ca1mbg7glrn8bwh8cj3pz0vv1j8")))

(define-public crate-wasmer-c-api-test-runner-3.1.1 (c (n "wasmer-c-api-test-runner") (v "3.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0s911r6npgm0ml5bpg9zz5j3yjp5ngwc6yhr1pqmh8077qphhn65")))

(define-public crate-wasmer-c-api-test-runner-3.2.0-beta.1 (c (n "wasmer-c-api-test-runner") (v "3.2.0-beta.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0g2hb4kdrvy94r75ijj64y77if1nzbzvy55k0kdk2hq9pc77rv9b")))

(define-public crate-wasmer-c-api-test-runner-3.2.0-beta.2 (c (n "wasmer-c-api-test-runner") (v "3.2.0-beta.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1l2wcfrnjdsyszcak50nvr0r69jhfgy4w69xijz2wg37p9xk7ilf")))

(define-public crate-wasmer-c-api-test-runner-3.2.0 (c (n "wasmer-c-api-test-runner") (v "3.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0kw1hpw28p4hqhh8mpy7xdjhci751r5abnra4h83mbfjgv9nafy1")))

(define-public crate-wasmer-c-api-test-runner-3.2.1 (c (n "wasmer-c-api-test-runner") (v "3.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "017cd75js6j300rapn8mmpglfw7sy21wxyiap5g12nhfqj98m49s")))

(define-public crate-wasmer-c-api-test-runner-3.3.0 (c (n "wasmer-c-api-test-runner") (v "3.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "06nw1qfw9xslqwk5sl31ynxx7k47srrr3w0rdq2m8pdqvgkbdnrw")))

(define-public crate-wasmer-c-api-test-runner-4.0.0-alpha.1 (c (n "wasmer-c-api-test-runner") (v "4.0.0-alpha.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0hlra5hzkrxk71isn733vnclad1rglfi7z7daaw0z3132nlvxzgw")))

(define-public crate-wasmer-c-api-test-runner-4.0.0-beta.1 (c (n "wasmer-c-api-test-runner") (v "4.0.0-beta.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0cdjy1h4m085cwwzqj8kcyzkgjx8b22383pydzs5n0121hyqx00x")))

(define-public crate-wasmer-c-api-test-runner-4.0.0-beta.2 (c (n "wasmer-c-api-test-runner") (v "4.0.0-beta.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0avl7hc8c18z75lgm3c4g7378szpak80ci3cic52wa18v1sja7ck")))

(define-public crate-wasmer-c-api-test-runner-4.0.0-beta.3 (c (n "wasmer-c-api-test-runner") (v "4.0.0-beta.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "064yx2xq0l1s3rm9cqwzhr67d7zy52pdfd98ncmj6f825lhqjplg")))

(define-public crate-wasmer-c-api-test-runner-4.0.0 (c (n "wasmer-c-api-test-runner") (v "4.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "110vm428lrxlcv3hn70dp7gcgjk3zmijpawcmwvncvnr3rnf7965")))

(define-public crate-wasmer-c-api-test-runner-4.1.0 (c (n "wasmer-c-api-test-runner") (v "4.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1i0a993zx306p83k5z5pakwif3cs6rh4hln7l4a6q5fiwxf8iph6")))

(define-public crate-wasmer-c-api-test-runner-4.1.1 (c (n "wasmer-c-api-test-runner") (v "4.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0rpp7gpsc2yj004q3xw3bgmgk420d2q7bfhpkwnfnjd5x87dl4rz")))

(define-public crate-wasmer-c-api-test-runner-4.1.2 (c (n "wasmer-c-api-test-runner") (v "4.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "02j8jamq8j5pr6kjv8pnggq2jca5mjxv682g9xhcjwmvjdazx4i5")))

(define-public crate-wasmer-c-api-test-runner-4.2.0 (c (n "wasmer-c-api-test-runner") (v "4.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1hqijdhxpiid19ifa16ni1frvcm0bi5xynp5wrlywii5032wjjks")))

(define-public crate-wasmer-c-api-test-runner-4.2.1 (c (n "wasmer-c-api-test-runner") (v "4.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1ha2m4f8rlw3slflr0nizyyg1009fhwxfnsmhb5kbvp8wpq1lapp")))

(define-public crate-wasmer-c-api-test-runner-4.2.2 (c (n "wasmer-c-api-test-runner") (v "4.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0kmdp5h2xqn3j8vw293i5raz6j4305nr8rki7rmp4hwmpgfrjb2a")))

(define-public crate-wasmer-c-api-test-runner-4.2.3 (c (n "wasmer-c-api-test-runner") (v "4.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0dqnjhsbbw3kvx4k9hm49d1kbysm6y3gkrchjs6pw6hfvdnny929")))

(define-public crate-wasmer-c-api-test-runner-4.2.4 (c (n "wasmer-c-api-test-runner") (v "4.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1qq2ar7kf7y8296ynxcw8am0gvhwzws1chxnlnj33zrqvgm8svdl")))

(define-public crate-wasmer-c-api-test-runner-4.2.5 (c (n "wasmer-c-api-test-runner") (v "4.2.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1kg009x1ij8n5vn5n0x8pwa5rihvphxd9svss77k1bx1iy7anfvh")))

(define-public crate-wasmer-c-api-test-runner-4.2.6 (c (n "wasmer-c-api-test-runner") (v "4.2.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1aacrk3wbixk8w77khdranfdbi88i4fciy3lf5gcnzaprjs7433x")))

(define-public crate-wasmer-c-api-test-runner-4.2.7 (c (n "wasmer-c-api-test-runner") (v "4.2.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0ibakixhpp1yys4cxwd757k6yyk95qgl4c9ww08wl8fdwynzqbfq")))

(define-public crate-wasmer-c-api-test-runner-4.2.8 (c (n "wasmer-c-api-test-runner") (v "4.2.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "0ckiwgsawbz9q7r86dpf5brkicpn203nl04n0lk02gjm7xylidp8")))

(define-public crate-wasmer-c-api-test-runner-4.3.0-alpha.1 (c (n "wasmer-c-api-test-runner") (v "4.3.0-alpha.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1npi1yqmmiwvaq7ww9n4yfaas0w79jnrsgn6fkm0jvhp74rps88b")))

(define-public crate-wasmer-c-api-test-runner-4.3.0-beta.1 (c (n "wasmer-c-api-test-runner") (v "4.3.0-beta.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1ix0pvwdmlss9nh3jh18lm1gpjq0k13i77aqihvl4bfvav21dvbv")))

(define-public crate-wasmer-c-api-test-runner-4.3.0 (c (n "wasmer-c-api-test-runner") (v "4.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "1jq5p18mchls7fxik3ynigl9qf12mbhb21frd9s0wfaicap56078")))

(define-public crate-wasmer-c-api-test-runner-4.3.1 (c (n "wasmer-c-api-test-runner") (v "4.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)))) (h "03dv1js0dgjk10sjnhalk8n6rnyyffph1913vxwl01pynh292j87")))

