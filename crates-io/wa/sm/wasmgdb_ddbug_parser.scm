(define-module (crates-io wa sm wasmgdb_ddbug_parser) #:use-module (crates-io))

(define-public crate-wasmgdb_ddbug_parser-0.3.1 (c (n "wasmgdb_ddbug_parser") (v "0.3.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "gimli") (r "^0.26") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "object") (r "^0.29.0") (f (quote ("wasm"))) (d #t) (k 0)))) (h "001slm2fq010ln29jwdz13x2rbmlv0wkckk7fd40rz0vx6jvdgnc") (f (quote (("default"))))))

(define-public crate-wasmgdb_ddbug_parser-0.3.2 (c (n "wasmgdb_ddbug_parser") (v "0.3.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "gimli") (r "^0.26") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "object") (r "^0.29.0") (f (quote ("wasm"))) (d #t) (k 0)))) (h "11dq1yh7yc0hjn065rbl4c0r6r61qd24v5jaifn28h5gvs5kgg06") (f (quote (("default"))))))

(define-public crate-wasmgdb_ddbug_parser-0.3.3 (c (n "wasmgdb_ddbug_parser") (v "0.3.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "gimli") (r "^0.26") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "object") (r "^0.29.0") (f (quote ("wasm"))) (d #t) (k 0)))) (h "0lzib8r6v9xsala25zhhs9wn7awg1g4v8300jzib33mylrbv1vq4") (f (quote (("default"))))))

