(define-module (crates-io wa sm wasm_bindgen_duck_type_macro_impl) #:use-module (crates-io))

(define-public crate-wasm_bindgen_duck_type_macro_impl-0.1.0 (c (n "wasm_bindgen_duck_type_macro_impl") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "009gv19dpcgp2wfrkl0qzqjhr83kvwkcba7gfyn22mxdlbdk44kx")))

