(define-module (crates-io wa sm wasm3) #:use-module (crates-io))

(define-public crate-wasm3-0.1.0 (c (n "wasm3") (v "0.1.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "wasm3-sys")) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "15m2li8yi424zkd03rfc3gsg4b46m8cm3rdyg65cg6sxcvmzh81x") (f (quote (("wasi" "ffi/wasi") ("use-32bit-slots" "ffi/use-32bit-slots") ("std") ("default" "wasi" "std" "use-32bit-slots"))))))

(define-public crate-wasm3-0.1.1 (c (n "wasm3") (v "0.1.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "wasm3-sys")) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1vyvlvnf273xmx0vbqkx3pqfj63f59x736sh8rhycbsag6xfd3y9") (f (quote (("wasi" "ffi/wasi") ("use-32bit-slots" "ffi/use-32bit-slots") ("std") ("default" "wasi" "std" "use-32bit-slots") ("build-bindgen" "ffi/build-bindgen"))))))

(define-public crate-wasm3-0.1.2 (c (n "wasm3") (v "0.1.2") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ffi") (r "^0.1.2") (d #t) (k 0) (p "wasm3-sys")) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0hnzf5w6rnjrwqyn2a16h349c4s3z0f1nhza30px198d049ypaf1") (f (quote (("wasi" "ffi/wasi") ("use-32bit-slots" "ffi/use-32bit-slots") ("std") ("default" "wasi" "std" "use-32bit-slots") ("build-bindgen" "ffi/build-bindgen"))))))

(define-public crate-wasm3-0.1.3 (c (n "wasm3") (v "0.1.3") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ffi") (r "^0.1.2") (d #t) (k 0) (p "wasm3-sys")) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "14kbpdrwscc20r8i4sybq5jlsb64k26mq665fczvpm2rvygzryg5") (f (quote (("wasi" "ffi/wasi") ("use-32bit-slots" "ffi/use-32bit-slots") ("std") ("default" "wasi" "std" "use-32bit-slots") ("build-bindgen" "ffi/build-bindgen"))))))

(define-public crate-wasm3-0.2.0 (c (n "wasm3") (v "0.2.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ffi") (r "^0.1.2") (d #t) (k 0) (p "wasm3-sys")) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0vicbgivzryhby76ry96m5d5wl866bxr4a5y1c6sh089j7gbsfd1") (f (quote (("wasi" "ffi/wasi") ("use-32bit-slots" "ffi/use-32bit-slots") ("std") ("default" "wasi" "std" "use-32bit-slots") ("build-bindgen" "ffi/build-bindgen"))))))

(define-public crate-wasm3-0.3.0 (c (n "wasm3") (v "0.3.0") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ffi") (r "^0.1.2") (d #t) (k 0) (p "wasm3-sys")) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1wz3b9kbclfm5q9mawnhk7pvwv24i6k3bsdzw5qkhjjqpdkwf3qp") (f (quote (("wasi" "ffi/wasi") ("use-32bit-slots" "ffi/use-32bit-slots") ("std") ("default" "wasi" "std" "use-32bit-slots") ("build-bindgen" "ffi/build-bindgen"))))))

(define-public crate-wasm3-0.3.1 (c (n "wasm3") (v "0.3.1") (d (list (d (n "cty") (r "^0.2") (d #t) (k 0)) (d (n "ffi") (r "^0.3.0") (d #t) (k 0) (p "wasm3-sys")) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "125zic0wv7hfcjnafyp9wdy8y2xl385vnas3993vx6cy8jbxwzfd") (f (quote (("wasi" "ffi/wasi") ("use-32bit-slots" "ffi/use-32bit-slots") ("std") ("default" "wasi" "std" "use-32bit-slots") ("build-bindgen" "ffi/build-bindgen"))))))

