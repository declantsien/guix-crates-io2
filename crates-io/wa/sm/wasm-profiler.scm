(define-module (crates-io wa sm wasm-profiler) #:use-module (crates-io))

(define-public crate-wasm-profiler-0.1.0 (c (n "wasm-profiler") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.39") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "198kzynvfhxhaym35wigznl7w1jxy97z3ykyr7k8g3izh96jbyfm")))

