(define-module (crates-io wa sm wasmflow-boundary) #:use-module (crates-io))

(define-public crate-wasmflow-boundary-0.10.0-beta.4 (c (n "wasmflow-boundary") (v "0.10.0-beta.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasmflow-codec") (r "^0.10.0-beta.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasmflow-invocation") (r "^0.10.0-beta.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasmflow-packet") (r "^0.10.0-beta.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "0za37zj1l34pyqq7yqxwp64752r2h6q4qjbra0gcd9n8yr3b60m8") (f (quote (("v1"))))))

(define-public crate-wasmflow-boundary-0.10.0 (c (n "wasmflow-boundary") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasmflow-codec") (r "^0.10.0-beta.4") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasmflow-invocation") (r "^0.10.0-beta.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasmflow-packet") (r "^0.10.0-beta.4") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "01hxca4q1sisx2b952xc95awvnjayzw75y5w8xxjcinqkihhfqq9") (f (quote (("v1"))))))

