(define-module (crates-io wa sm wasmcloud-interface-timing) #:use-module (crates-io))

(define-public crate-wasmcloud-interface-timing-0.1.0 (c (n "wasmcloud-interface-timing") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.13.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7.0") (d #t) (k 1)))) (h "12xgh8dy8zxkv6kv8k9k6i3j57bhs9diynj18vd50q93a065jdrw")))

(define-public crate-wasmcloud-interface-timing-0.1.1 (c (n "wasmcloud-interface-timing") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.13.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7.0") (d #t) (k 1)))) (h "0vsmygpp9frf86rk4pnvnfmk25nanjkd38g4y4b1s413z7rrgvxd")))

(define-public crate-wasmcloud-interface-timing-0.1.2 (c (n "wasmcloud-interface-timing") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.13.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7.0") (d #t) (k 1)))) (h "1p6xjxis9kcgi1zb78mw1fr3dgf2wjh07pyplwdfq6xjkzpzqxip")))

