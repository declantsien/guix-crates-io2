(define-module (crates-io wa sm wasm-loader) #:use-module (crates-io))

(define-public crate-wasm-loader-0.13.0 (c (n "wasm-loader") (v "0.13.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "jsonrpsee") (r "^0.3.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "multibase") (r "^0.9") (d #t) (k 0)) (d (n "multihash") (r "^0.14") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sp-maybe-compressed-blob") (r "^4.0.0-dev") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("full"))) (d #t) (k 0)))) (h "05v8cjlb6c0b8l0wvxp9qqsrzs2zbll8w4n9n2q856mvrjp4x6jq")))

