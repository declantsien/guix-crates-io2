(define-module (crates-io wa sm wasmtime-rust) #:use-module (crates-io))

(define-public crate-wasmtime-rust-0.2.0 (c (n "wasmtime-rust") (v "0.2.0") (d (list (d (n "cranelift-codegen") (r "^0.40.0") (d #t) (k 0)) (d (n "cranelift-native") (r "^0.40.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime-interface-types") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmtime-jit") (r "^0.2.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.2.0") (d #t) (k 0)))) (h "1ssfq82xr940dicjmra5ianc4asr075zfi22isb05ks19a1arghx")))

(define-public crate-wasmtime-rust-0.3.0 (c (n "wasmtime-rust") (v "0.3.0") (d (list (d (n "cranelift-codegen") (r "^0.40.0") (d #t) (k 0)) (d (n "cranelift-native") (r "^0.40.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime-interface-types") (r "^0.3.0") (d #t) (k 0)) (d (n "wasmtime-jit") (r "^0.3.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.3.0") (d #t) (k 0)))) (h "0sbi07dqzfahlvlpa2lr909sq6z814lgjcfdlhvdzv3v316qa4za")))

(define-public crate-wasmtime-rust-0.7.0 (c (n "wasmtime-rust") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmtime-interface-types") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmtime-jit") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.7.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.7.0") (d #t) (k 0)))) (h "0wm92ydhv94jsnxrxaqf1rxcqxxbl227m4vmyh2szm5d6hlx5538")))

(define-public crate-wasmtime-rust-0.8.0 (c (n "wasmtime-rust") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.8.0") (d #t) (k 0)) (d (n "wasmtime-interface-types") (r "^0.8.0") (d #t) (k 0)) (d (n "wasmtime-jit") (r "^0.8.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.8.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.8.0") (d #t) (k 0)))) (h "02nmdg657hlfa3qpla67psz0lbiv6fqgixc9j8aw1nkg9h87bb7d")))

(define-public crate-wasmtime-rust-0.9.0 (c (n "wasmtime-rust") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.9.0") (d #t) (k 0)) (d (n "wasmtime-interface-types") (r "^0.9.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.9.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.9.0") (d #t) (k 0)))) (h "1290ych0nn7ym4l6q8hav4sb2qai7ggljc5xvhs6sfphlf7nxs4y")))

(define-public crate-wasmtime-rust-0.10.0 (c (n "wasmtime-rust") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.10.0") (d #t) (k 0)) (d (n "wasmtime-interface-types") (r "^0.10.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.10.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.10.0") (d #t) (k 0)))) (h "193hfzjwxmfs0pnldcchxxc8yxc2cjjf4plscdbql1h43ym5mmg3")))

(define-public crate-wasmtime-rust-0.11.0 (c (n "wasmtime-rust") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.11.0") (d #t) (k 0)) (d (n "wasmtime-interface-types") (r "^0.11.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.11.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.11.0") (d #t) (k 0)))) (h "1ylzzjipl0c236swk1g2073sy0nfbva3gjg9ki1ql9xvghpwf6vn")))

(define-public crate-wasmtime-rust-0.12.0 (c (n "wasmtime-rust") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.12.0") (d #t) (k 0)) (d (n "wasmtime-interface-types") (r "^0.12.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.12.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.12.0") (d #t) (k 0)))) (h "04lbjagdv6fxia88yy7nmcnj2rqylxhn0nx4xnlcxbxv2icq2jpx")))

(define-public crate-wasmtime-rust-0.13.0 (c (n "wasmtime-rust") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.13.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.13.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.13.0") (d #t) (k 0)))) (h "0sxa0yb48klany4sclssf7r5kf8isbm75md74nj8yix7mqwvwm46")))

(define-public crate-wasmtime-rust-0.15.0 (c (n "wasmtime-rust") (v "0.15.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.15.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.15.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.15.0") (d #t) (k 0)))) (h "0pv37p6sgkx9diwgwkpcjsmlh1j92s0xw13450w31agnlsrkjlax")))

(define-public crate-wasmtime-rust-0.16.0 (c (n "wasmtime-rust") (v "0.16.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.16.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.16.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.16.0") (d #t) (k 0)))) (h "0vngy399ca183jmb920p44gvpig6zvqa88spmgslms1bh3q2msaj")))

(define-public crate-wasmtime-rust-0.17.0 (c (n "wasmtime-rust") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.17.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.17.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.17.0") (d #t) (k 0)))) (h "0im8bca5rg2i47j2f53gg5i42qdw1hzjidmrs9fn49ii87parr4g")))

(define-public crate-wasmtime-rust-0.18.0 (c (n "wasmtime-rust") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.18.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.18.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.18.0") (d #t) (k 0)))) (h "0mmkiw5rlywy0nbvjdhzjq8rl391cqpxh5kr04qvksb3q4s7gi9j")))

(define-public crate-wasmtime-rust-0.19.0 (c (n "wasmtime-rust") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.19.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.19.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.19.0") (d #t) (k 0)))) (h "0730mi8w6wpfw8km5c3gj0myzlf9k2h78b8c4l25cnn7xb56zw41")))

(define-public crate-wasmtime-rust-0.20.0 (c (n "wasmtime-rust") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.20.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.20.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.20.0") (d #t) (k 0)))) (h "0sdlaqzl42l834lrl3j35plwafkvbjqqzfy6s1qkdji17rn3qzxr")))

(define-public crate-wasmtime-rust-0.21.0 (c (n "wasmtime-rust") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.21.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.21.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.21.0") (d #t) (k 0)))) (h "166fi7jh0410qbikm2y6zaa8iz46mh5cwbmgxdfdxxhadhmzyw4n")))

(define-public crate-wasmtime-rust-0.22.0 (c (n "wasmtime-rust") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.22.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.22.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.22.0") (d #t) (k 0)))) (h "1ylvjanvzd7vkfp7s9mywc6h8wx16mjh6bxcdzn7a1bqy8mz5ys5")))

(define-public crate-wasmtime-rust-0.23.0 (c (n "wasmtime-rust") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.23.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.23.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.23.0") (d #t) (k 0)))) (h "1v4d1smw2ll6sdfrgm8yf7xhhfxvmjfq1jq5kfis2gkgjfrykhrg")))

(define-public crate-wasmtime-rust-0.24.0 (c (n "wasmtime-rust") (v "0.24.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.24.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.24.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.24.0") (d #t) (k 0)))) (h "1l01ajffncip1784ybq99a9pycghkj54m3vww3hck826h97fmch4")))

(define-public crate-wasmtime-rust-0.25.0 (c (n "wasmtime-rust") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.25.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.25.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.25.0") (d #t) (k 0)))) (h "1wxax87xzmp0hpmxyifq2dcz5fp62b1qbn3jwdpv7ll9id2xzbgj")))

(define-public crate-wasmtime-rust-0.26.0 (c (n "wasmtime-rust") (v "0.26.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.26.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.26.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.26.0") (d #t) (k 0)))) (h "13isqs1r4psh7zmpsy5kqaba0gip71a1wj7qm4ks1hgpblkfyi2q")))

(define-public crate-wasmtime-rust-0.27.0 (c (n "wasmtime-rust") (v "0.27.0") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.27.0") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.27.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.27.0") (d #t) (k 0)))) (h "19mrp2p8gzz7d7g1f9r5px7fxw14v2a9h0b6wyaylii9s9rivhdg")))

(define-public crate-wasmtime-rust-0.26.1 (c (n "wasmtime-rust") (v "0.26.1") (d (list (d (n "anyhow") (r "^1.0.19") (d #t) (k 0)) (d (n "wasmtime") (r "^0.26.1") (d #t) (k 0)) (d (n "wasmtime-rust-macro") (r "^0.26.1") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^0.26.1") (d #t) (k 0)))) (h "0prh3ljp7qlf0yh593l44prhnwqf6f1fy35iidl8g9ijmzngwn9i")))

