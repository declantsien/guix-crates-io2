(define-module (crates-io wa sm wasm_promise) #:use-module (crates-io))

(define-public crate-wasm_promise-0.1.0 (c (n "wasm_promise") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.32") (d #t) (k 0)))) (h "0d7j10k8n5i2wcmwp04pv941bav3c2kjf07fiylygf8bys4mpbzv") (y #t)))

(define-public crate-wasm_promise-0.1.1 (c (n "wasm_promise") (v "0.1.1") (d (list (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.32") (d #t) (k 0)))) (h "1ni5m9chirgy9z3p6nxqkw9q8cz85ij673v2n83vp76w9i6x8098") (y #t)))

(define-public crate-wasm_promise-0.1.2 (c (n "wasm_promise") (v "0.1.2") (d (list (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.32") (d #t) (k 0)))) (h "0alpq4n4y5r4gmkr34c2lq20f19zwcscaf50l9xkh0d0ikqgcz63")))

(define-public crate-wasm_promise-0.1.3 (c (n "wasm_promise") (v "0.1.3") (d (list (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.32") (d #t) (k 0)))) (h "0qa5a810sccbw1k79rm6y3lpggn7p4385yh7zr8fzy8h7hqbjc37")))

(define-public crate-wasm_promise-0.1.4 (c (n "wasm_promise") (v "0.1.4") (d (list (d (n "js-sys") (r "^0.3.59") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.82") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.32") (d #t) (k 0)))) (h "1ylj1nj7xkyb95scnw74v1jv29bgcv9vzd3dnxxjvzxbal7sf9yv")))

