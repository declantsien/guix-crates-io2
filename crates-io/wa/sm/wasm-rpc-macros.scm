(define-module (crates-io wa sm wasm-rpc-macros) #:use-module (crates-io))

(define-public crate-wasm-rpc-macros-0.1.0 (c (n "wasm-rpc-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.9") (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0g5vkp0kj8ik1zw142l2xp221im1ya4n4y72npiij737xhwlfr27")))

(define-public crate-wasm-rpc-macros-0.1.1 (c (n "wasm-rpc-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.9") (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (f (quote ("full"))) (d #t) (k 0)))) (h "120z1gvxl3kddw3hwn2dgwf9jbxvy6h9bzx9zvjj6x0ggzrqdafa")))

(define-public crate-wasm-rpc-macros-0.2.6 (c (n "wasm-rpc-macros") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^0.4.9") (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0w4p4jlpqzfrzg7983s6rkyip8sk9q2wdp3npa16gfr164mhcyqc")))

(define-public crate-wasm-rpc-macros-0.2.7 (c (n "wasm-rpc-macros") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^0.4.9") (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (f (quote ("full"))) (d #t) (k 0)))) (h "00l35cyj4y7r4wdgxl90svnqp5d4i4p40wyzh3za2w0lgycd5h5a")))

(define-public crate-wasm-rpc-macros-0.2.8 (c (n "wasm-rpc-macros") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^0.4.9") (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (f (quote ("full"))) (d #t) (k 0)))) (h "16mmhp8bjwbmf6cvwmpq6q29czh9s6d58j2z4lf0j0a04vc44ijp")))

(define-public crate-wasm-rpc-macros-0.2.9 (c (n "wasm-rpc-macros") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^0.4.9") (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0akaf5ih8vqhm8jfb8smjzav7bd0jh72dmq4drlbg4ipbzkls5d1")))

(define-public crate-wasm-rpc-macros-0.2.10 (c (n "wasm-rpc-macros") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^0.4.9") (k 0)) (d (n "quote") (r "^0.6.4") (d #t) (k 0)) (d (n "syn") (r "^0.14.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0gfsfy4prnxnincwmv2zfndsszzbgkaxvk1ppdsd1m9npw7ij3ci")))

(define-public crate-wasm-rpc-macros-0.2.12 (c (n "wasm-rpc-macros") (v "0.2.12") (d (list (d (n "proc-macro2") (r "^0.4.30") (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.38") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1225k2m4zhij1lb0w2f9qfq3ifr55586ly5ap7yqz9ys7nfak1qa")))

(define-public crate-wasm-rpc-macros-0.2.13 (c (n "wasm-rpc-macros") (v "0.2.13") (d (list (d (n "proc-macro2") (r "^0.4.30") (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.38") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1gbpvcvpxdhyj77da68p3kl2g46cv8c4v3rmdyiv17p0z1fd2m3m")))

(define-public crate-wasm-rpc-macros-0.2.14 (c (n "wasm-rpc-macros") (v "0.2.14") (d (list (d (n "proc-macro2") (r "^0.4.30") (k 0)) (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.38") (f (quote ("full" "visit"))) (d #t) (k 0)))) (h "1c6rrp2xd0yn4m966mbaskq6ai2afciqj8n0lr8jpqv7cacnss81")))

(define-public crate-wasm-rpc-macros-0.2.15 (c (n "wasm-rpc-macros") (v "0.2.15") (d (list (d (n "proc-macro2") (r "^1.0.18") (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0rx19z1s8dw9d42afcyirl65340sphxg8m5qxcgwrq8k5r63nzwn")))

(define-public crate-wasm-rpc-macros-0.2.16 (c (n "wasm-rpc-macros") (v "0.2.16") (d (list (d (n "proc-macro2") (r "^1.0.18") (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "1b7x62rnr6z5a2m1gfk7wgavjzk3j5pkcpyyp3ivkgdwpgy5qpph")))

(define-public crate-wasm-rpc-macros-0.2.17 (c (n "wasm-rpc-macros") (v "0.2.17") (d (list (d (n "proc-macro2") (r "^1.0.18") (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "16vql0nrgsf07y6b9izg8616ccq9jr6f32s8906vlhhrp7lahj7y")))

(define-public crate-wasm-rpc-macros-0.2.18 (c (n "wasm-rpc-macros") (v "0.2.18") (d (list (d (n "proc-macro2") (r "^1.0.18") (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.33") (f (quote ("full" "visit" "extra-traits"))) (d #t) (k 0)))) (h "0ncnyykjjy9gnqs73ykk9a7r1cd14rd9g5b2yzbwwsmw7f6hnb8m")))

