(define-module (crates-io wa sm wasmer-types-x) #:use-module (crates-io))

(define-public crate-wasmer-types-x-2.4.1 (c (n "wasmer-types-x") (v "2.4.1") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "rkyv") (r "^0.7.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19jbpcgmwv1d9xlpdhx0w9qj968bl2kmq5q0n8hm4wdrvgjcfa6p") (f (quote (("std") ("experimental-reference-types-extern-ref") ("default" "std") ("core"))))))

