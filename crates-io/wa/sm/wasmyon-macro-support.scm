(define-module (crates-io wa sm wasmyon-macro-support) #:use-module (crates-io))

(define-public crate-wasmyon-macro-support-0.1.0 (c (n "wasmyon-macro-support") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rq6v6kbczwz6xcc201a1p8468a8b3nkyr3gnpcwz0cldqq1bfni")))

(define-public crate-wasmyon-macro-support-0.1.1 (c (n "wasmyon-macro-support") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "03vdcvij7qrgbv3zvpxypq1m7qcd202izpmy27vcazjs8mfm1xpj")))

