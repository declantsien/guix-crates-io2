(define-module (crates-io wa sm wasmfmt) #:use-module (crates-io))

(define-public crate-wasmfmt-0.1.0 (c (n "wasmfmt") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "wast") (r "^30.0") (d #t) (k 0)))) (h "0pq5i82nlb9y2zj7khdpxya2vwfw06a6wz4fj9dl7855bxx9ks4p")))

(define-public crate-wasmfmt-0.1.1 (c (n "wasmfmt") (v "0.1.1") (d (list (d (n "assert_matches") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "wast") (r "^30.0") (d #t) (k 0)))) (h "1hr6hjjcghn0bfd1i6544dgqx1hn8k2w7hwp58biprl8h2c8sbws")))

(define-public crate-wasmfmt-0.2.0 (c (n "wasmfmt") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "wast") (r "^32.0") (d #t) (k 0)))) (h "1xhxllsf0i43nisbbknw6gjqnzhb9w8mafhbav78zzwnya1fvzz6")))

(define-public crate-wasmfmt-0.2.1 (c (n "wasmfmt") (v "0.2.1") (d (list (d (n "assert_matches") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "wast") (r "^32.0") (d #t) (k 0)))) (h "0k9g5qi0p55nyb1fsj81vzg7g7ca3yr9dx1c1hhjand6nn6q9vzw")))

(define-public crate-wasmfmt-0.2.2 (c (n "wasmfmt") (v "0.2.2") (d (list (d (n "assert_matches") (r "^1.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^0.6") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "wast") (r "^32.0") (d #t) (k 0)))) (h "0m0cfvfn0r7yyipx5rpziwlg6bysd3pmqw1sf90ksnakkx2isyji")))

(define-public crate-wasmfmt-0.2.3 (c (n "wasmfmt") (v "0.2.3") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "wast") (r "^32.0") (d #t) (k 0)))) (h "048mjky0d423yibz78i46wvjwlsx1ahwvrjnhh72zx6jzwfyx4r6")))

(define-public crate-wasmfmt-0.2.4 (c (n "wasmfmt") (v "0.2.4") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "unindent") (r "^0.1") (d #t) (k 2)) (d (n "wast") (r "^46.0") (d #t) (k 0)))) (h "0m61pcjdgi559l5ismrkr4rq7jp2gv4l7a4i8cmidpvp18dvgwvm")))

