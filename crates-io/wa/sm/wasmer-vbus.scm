(define-module (crates-io wa sm wasmer-vbus) #:use-module (crates-io))

(define-public crate-wasmer-vbus-3.0.0-alpha.4 (c (n "wasmer-vbus") (v "3.0.0-alpha.4") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-alpha.4") (k 0)))) (h "0plbphf3b0ka5ga7lacjg9nk2iff6kwy50czdslgvj08vynvl13w") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.0.0-beta (c (n "wasmer-vbus") (v "3.0.0-beta") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)) (d (n "slab") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "typetag") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-beta") (k 0)))) (h "1vwb1xqgllzvj832xr4g5nhp80662ln1wmk0ngncp1ck0lgwlmjm") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.0.0-beta.2 (c (n "wasmer-vbus") (v "3.0.0-beta.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-beta.2") (k 0)))) (h "0phl76qg4y02g9vmzg705ya5npy8c29scbpz6ncrvr3m401xhv8h") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.0.0-rc.1 (c (n "wasmer-vbus") (v "3.0.0-rc.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-rc.1") (k 0)))) (h "1r70k99040w00h0ynx75lxgwf8wf8y6w49p57jaj2bg38kjkv1xk") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.0.0-rc.2 (c (n "wasmer-vbus") (v "3.0.0-rc.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-rc.2") (k 0)))) (h "0fp6y4lygjiadjrcd0vw104z860jrizxcrk21m3yazqbzfvb4rsa") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.0.0-rc.3 (c (n "wasmer-vbus") (v "3.0.0-rc.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-rc.3") (k 0)))) (h "0bj1pvw4cqgr2xp2pip2219745l9r6vl8i087x474nn4qmxbqs3s") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.0.0-rc.4 (c (n "wasmer-vbus") (v "3.0.0-rc.4") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0-rc.4") (k 0)))) (h "00d31cblq5nysv1yb7nxkr3gz8n93qx07gxkqkihkw0ihx3xh5xq") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.0.0 (c (n "wasmer-vbus") (v "3.0.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.0") (k 0)))) (h "16ds8im6lgl21vxb7w1h3pmssdig510mh26qrs7lklds5pilgf2z") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.0.1 (c (n "wasmer-vbus") (v "3.0.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.1") (k 0)))) (h "093pxyn1ig2185q063llrq8gxkd5z0w9k5jhh177ppi0x02nbk4p") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.0.2 (c (n "wasmer-vbus") (v "3.0.2") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.2") (k 0)))) (h "0ii3c9v6015v5617i3233llg39krdzhjjhz0m5iyn5zm5y74z6n4") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.1.0 (c (n "wasmer-vbus") (v "3.1.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.1.0") (k 0)))) (h "066gishfbrk9ljcz7pa9rhk2vs9d0apr7fsnfrwsjxjk8y26w5zc") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.2.0-alpha.1 (c (n "wasmer-vbus") (v "3.2.0-alpha.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.2.0-alpha.1") (k 0)))) (h "00284h2w4mwjrw5a820sjyc11a65zli68nzn377k09d07syk8l0p") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.0.3 (c (n "wasmer-vbus") (v "3.0.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.0.3") (k 0)))) (h "1h0v30wd63ln7vcb4ba85dcd6kw5yw50kjnfq48ncqhrcnzqpdnq") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

(define-public crate-wasmer-vbus-3.1.1 (c (n "wasmer-vbus") (v "3.1.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer-vfs") (r "=3.1.1") (k 0)))) (h "1nybkmk9m0x1gxvg7v05a15ryq0797gmz8z39l46i77hp5v2zd3j") (f (quote (("mem_fs" "wasmer-vfs/mem-fs") ("host_fs" "wasmer-vfs/host-fs") ("default" "mem_fs"))))))

