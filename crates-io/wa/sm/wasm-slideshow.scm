(define-module (crates-io wa sm wasm-slideshow) #:use-module (crates-io))

(define-public crate-wasm-slideshow-0.1.0 (c (n "wasm-slideshow") (v "0.1.0") (d (list (d (n "js-sys") (r "^0.3.25") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.48") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.25") (f (quote ("Document" "Element" "HtmlElement" "Node" "NodeList" "Window" "CssStyleDeclaration" "EventTarget" "EventListener" "UiEvent" "DomTokenList"))) (d #t) (k 0)))) (h "1k170w67p48w4vcr4ibdlhylq3vpp4awd84nn58nnjssq2zics1r")))

