(define-module (crates-io wa sm wasmcloud-actor-core-derive) #:use-module (crates-io))

(define-public crate-wasmcloud-actor-core-derive-0.1.0 (c (n "wasmcloud-actor-core-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "13xh8rq8srpb17nxp2619ddnzbnl98bhxxjwxw99hcxm9vflggj8")))

