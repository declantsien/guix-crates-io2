(define-module (crates-io wa sm wasm-tensorflow-tfjs-core) #:use-module (crates-io))

(define-public crate-wasm-tensorflow-tfjs-core-0.1.0 (c (n "wasm-tensorflow-tfjs-core") (v "0.1.0") (d (list (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.39") (d #t) (k 0)))) (h "1gm3mhj5kf9xmgxw3xc1cixy00czi0jy2k6vg99842kf6099j8ja")))

(define-public crate-wasm-tensorflow-tfjs-core-0.1.1 (c (n "wasm-tensorflow-tfjs-core") (v "0.1.1") (d (list (d (n "wasm-bindgen") (r "^0.2.89") (d #t) (k 0)) (d (n "wasm-bindgen-futures") (r "^0.4.39") (d #t) (k 0)))) (h "0f6lyggig7wcyj90mxh6p8cv1jfc1drjbfcwbdbylrbxdgscql9w")))

