(define-module (crates-io wa sm wasmer-cache-near) #:use-module (crates-io))

(define-public crate-wasmer-cache-near-1.0.1 (c (n "wasmer-cache-near") (v "1.0.1") (d (list (d (n "blake3") (r "^0.3") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasmer") (r "^1.0.1") (k 0) (p "wasmer-near")))) (h "12x561k596p1bbhwkqph5phc9gbjk9prjsg6fs8nyjni8g2dnlql")))

