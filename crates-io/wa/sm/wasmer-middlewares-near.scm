(define-module (crates-io wa sm wasmer-middlewares-near) #:use-module (crates-io))

(define-public crate-wasmer-middlewares-near-1.0.1 (c (n "wasmer-middlewares-near") (v "1.0.1") (d (list (d (n "wasmer") (r "^1.0.1") (d #t) (k 0) (p "wasmer-near")) (d (n "wasmer-types") (r "^1.0.1") (d #t) (k 0) (p "wasmer-types-near")) (d (n "wasmer-vm") (r "^1.0.1") (d #t) (k 0) (p "wasmer-vm-near")))) (h "0k8gpkbm6nf98lrp4p6g0vx7fnnzxc5q6yjp4nls7ixlh718kpv6")))

