(define-module (crates-io wa sm wasmer-emscripten-near) #:use-module (crates-io))

(define-public crate-wasmer-emscripten-near-1.0.1 (c (n "wasmer-emscripten-near") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "getrandom") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "^1.0.1") (k 0) (p "wasmer-near")))) (h "0vavk6w390zqb5kkhq92q8khpi88rkmxdd53rks43z8zr4k35ri6")))

