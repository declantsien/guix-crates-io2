(define-module (crates-io wa sm wasmlite) #:use-module (crates-io))

(define-public crate-wasmlite-0.0.1 (c (n "wasmlite") (v "0.0.1") (d (list (d (n "wasmlite-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "wasmlite-utils") (r "^0.0.1") (d #t) (k 0)))) (h "00d0hcqzlpy1l4r6gy6y9n9223k042dp9mv841qpfbnkg4qp8xwk") (f (quote (("debug"))))))

