(define-module (crates-io wa sm wasm-embedded-hal) #:use-module (crates-io))

(define-public crate-wasm-embedded-hal-0.1.0 (c (n "wasm-embedded-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^1.0.0-alpha.4") (d #t) (k 0)))) (h "1k76hqyfaznw2xz0nyjqk1x3xhzf39jv3adj7xh6amrszk41acar") (f (quote (("tests"))))))

(define-public crate-wasm-embedded-hal-0.4.0 (c (n "wasm-embedded-hal") (v "0.4.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)))) (h "0wy1hvwq58wbn6d57vhrrx9vqklk1ah7kdwds9gbmihqx4cbhm5r") (f (quote (("tests" "test_spi" "test_i2c" "test_gpio" "test_uart") ("test_uart") ("test_spi") ("test_i2c") ("test_gpio"))))))

