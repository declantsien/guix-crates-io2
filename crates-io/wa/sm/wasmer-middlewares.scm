(define-module (crates-io wa sm wasmer-middlewares) #:use-module (crates-io))

(define-public crate-wasmer-middlewares-1.0.0-beta2 (c (n "wasmer-middlewares") (v "1.0.0-beta2") (d (list (d (n "wasmer") (r "^1.0.0-beta2") (d #t) (k 0)) (d (n "wasmer-types") (r "^1.0.0-beta2") (d #t) (k 0)) (d (n "wasmer-vm") (r "^1.0.0-beta2") (d #t) (k 0)))) (h "0vd5qirgri5pmlf0kaahbaxh248y5jkix6g2dknzfnk7d4siksm2")))

(define-public crate-wasmer-middlewares-1.0.0-rc1 (c (n "wasmer-middlewares") (v "1.0.0-rc1") (d (list (d (n "wasmer") (r "^1.0.0-rc1") (d #t) (k 0)) (d (n "wasmer-types") (r "^1.0.0-rc1") (d #t) (k 0)) (d (n "wasmer-vm") (r "^1.0.0-rc1") (d #t) (k 0)))) (h "11gdm0d7jl2s60238x49kznngic2aw3mydb49hcp4lr5n8gg5awg")))

(define-public crate-wasmer-middlewares-1.0.0 (c (n "wasmer-middlewares") (v "1.0.0") (d (list (d (n "wasmer") (r "^1.0.0") (d #t) (k 0)) (d (n "wasmer-types") (r "^1.0.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "^1.0.0") (d #t) (k 0)))) (h "0vfchajkp4g8cl5yzr6lfyxwrww1kj4vrcnr12ydqq9af3z5zvjf")))

(define-public crate-wasmer-middlewares-1.0.1 (c (n "wasmer-middlewares") (v "1.0.1") (d (list (d (n "wasmer") (r "^1.0.1") (d #t) (k 0)) (d (n "wasmer-types") (r "^1.0.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "^1.0.1") (d #t) (k 0)))) (h "0310spv0j9ylg2dnpm2v2jffp6yqp09xsvmlrpn7pras7yxlg4wz")))

(define-public crate-wasmer-middlewares-1.0.2 (c (n "wasmer-middlewares") (v "1.0.2") (d (list (d (n "wasmer") (r "^1.0.2") (d #t) (k 0)) (d (n "wasmer-types") (r "^1.0.2") (d #t) (k 0)) (d (n "wasmer-vm") (r "^1.0.2") (d #t) (k 0)))) (h "1i63b7vzdv6afk6yc3zw45iplww63y6pl4vxvcvg8g3kq3iawysl")))

(define-public crate-wasmer-middlewares-2.0.0-rc1 (c (n "wasmer-middlewares") (v "2.0.0-rc1") (d (list (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "^2.0.0-rc1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer-types") (r "^2.0.0-rc1") (d #t) (k 0)) (d (n "wasmer-vm") (r "^2.0.0-rc1") (d #t) (k 0)))) (h "1knwjy7az6qwnigg0q7g82whgm98nd13lhn76wn031i0myf6xby0")))

(define-public crate-wasmer-middlewares-2.0.0-rc2 (c (n "wasmer-middlewares") (v "2.0.0-rc2") (d (list (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "^2.0.0-rc2") (f (quote ("compiler"))) (k 0)) (d (n "wasmer-types") (r "^2.0.0-rc2") (d #t) (k 0)) (d (n "wasmer-vm") (r "^2.0.0-rc2") (d #t) (k 0)))) (h "0x2mgzc9vy48jbkypprladgn529m0xvqk44hhh4ixpfqqb129c8s")))

(define-public crate-wasmer-middlewares-2.0.0 (c (n "wasmer-middlewares") (v "2.0.0") (d (list (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "^2.0.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "^2.0.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "^2.0.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "^2.0.0") (d #t) (k 0)))) (h "0psjzx5xdg6q05w84rnkzjxfz0b1nwxanzks7y4m1j345mrb9llm")))

(define-public crate-wasmer-middlewares-2.1.0 (c (n "wasmer-middlewares") (v "2.1.0") (d (list (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "^2.1.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "^2.1.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "^2.1.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "^2.1.0") (d #t) (k 0)))) (h "19nrzpzk3y53hfm9alzjg2gfxj3sqz2pg31wzyi39f6x1qlsxlf8")))

(define-public crate-wasmer-middlewares-2.1.1 (c (n "wasmer-middlewares") (v "2.1.1") (d (list (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "^2.1.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "^2.1.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "^2.1.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "^2.1.1") (d #t) (k 0)))) (h "1wdz9xpnr82ccrqh0z96csjwsis8ryf89ds2c6jlr2k0m2yp66n6")))

(define-public crate-wasmer-middlewares-2.2.0-rc1 (c (n "wasmer-middlewares") (v "2.2.0-rc1") (d (list (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "=2.2.0-rc1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=2.2.0-rc1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=2.2.0-rc1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=2.2.0-rc1") (d #t) (k 0)))) (h "0bzy91a0pckm5ipp8zrrri4n7kk26frrvjy4psxbp6444x60sxyc")))

(define-public crate-wasmer-middlewares-2.2.0-rc2 (c (n "wasmer-middlewares") (v "2.2.0-rc2") (d (list (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "=2.2.0-rc2") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=2.2.0-rc2") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=2.2.0-rc2") (d #t) (k 0)) (d (n "wasmer-vm") (r "=2.2.0-rc2") (d #t) (k 0)))) (h "08j51a1fx8gji5azwydv4ci5fk6g2k6m33zlymb4xhaq41d8va89")))

(define-public crate-wasmer-middlewares-2.2.0 (c (n "wasmer-middlewares") (v "2.2.0") (d (list (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "=2.2.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=2.2.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=2.2.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "=2.2.0") (d #t) (k 0)))) (h "15ycmhg927zbygi2ca73x45y2ds2pd1vcxsrjfp3gw32vll6b0dy")))

(define-public crate-wasmer-middlewares-2.2.1 (c (n "wasmer-middlewares") (v "2.2.1") (d (list (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "=2.2.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=2.2.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=2.2.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=2.2.1") (d #t) (k 0)))) (h "11xq7b07bnfwlcgi48fccd7v8bicd04p95m8qds4ckv82pdpb5v5")))

(define-public crate-wasmer-middlewares-2.3.0 (c (n "wasmer-middlewares") (v "2.3.0") (d (list (d (n "loupe") (r "^0.1") (d #t) (k 0)) (d (n "wasmer") (r "=2.3.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=2.3.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=2.3.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "=2.3.0") (d #t) (k 0)))) (h "0fpdx6a7hr5jk49da5bfn6r5qixq699xnz006wx20drgxlw290fp")))

(define-public crate-wasmer-middlewares-3.0.0-alpha.2 (c (n "wasmer-middlewares") (v "3.0.0-alpha.2") (d (list (d (n "wasmer") (r "=3.0.0-alpha.2") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.0-alpha.2") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.0-alpha.2") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.0-alpha.2") (d #t) (k 0)))) (h "0qyyrbph2qk128rza69i0phmysdhc5hhdzrk1vwjgb3sn4l4yma8")))

(define-public crate-wasmer-middlewares-3.0.0-alpha.3 (c (n "wasmer-middlewares") (v "3.0.0-alpha.3") (d (list (d (n "wasmer") (r "=3.0.0-alpha.3") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.0-alpha.3") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.0-alpha.3") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.0-alpha.3") (d #t) (k 0)))) (h "1f4nmhk1c476059fkvvijrhng45k4jikwmqaxscw6pn70g5fqx37")))

(define-public crate-wasmer-middlewares-3.0.0-alpha.4 (c (n "wasmer-middlewares") (v "3.0.0-alpha.4") (d (list (d (n "wasmer") (r "=3.0.0-alpha.4") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.0-alpha.4") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.0-alpha.4") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.0-alpha.4") (d #t) (k 0)))) (h "1kdg6vqkh5w8ayx2y6r4hy5wc4l1m38aixw0pa23r5nlc3mpffr8")))

(define-public crate-wasmer-middlewares-3.0.0-beta (c (n "wasmer-middlewares") (v "3.0.0-beta") (d (list (d (n "wasmer") (r "=3.0.0-beta") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.0-beta") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.0-beta") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.0-beta") (d #t) (k 0)))) (h "16f4isvi9ra98n1bz1n4xjlfqk63s9g52rl7y66ww7wiiw1n4pfw")))

(define-public crate-wasmer-middlewares-3.0.0-beta.2 (c (n "wasmer-middlewares") (v "3.0.0-beta.2") (d (list (d (n "wasmer") (r "=3.0.0-beta.2") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.0-beta.2") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.0-beta.2") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.0-beta.2") (d #t) (k 0)))) (h "03xvhw7j4wxr5n4v6x5n276x8i89h75mgx0sayd0r655wfk1y3qr")))

(define-public crate-wasmer-middlewares-3.0.0-rc.1 (c (n "wasmer-middlewares") (v "3.0.0-rc.1") (d (list (d (n "wasmer") (r "=3.0.0-rc.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.0-rc.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.0-rc.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.0-rc.1") (d #t) (k 0)))) (h "1zc31hm0g523kv63vnqwqxbjz5d3n51d39fcw0d2ai15kgbq382d")))

(define-public crate-wasmer-middlewares-3.0.0-rc.2 (c (n "wasmer-middlewares") (v "3.0.0-rc.2") (d (list (d (n "wasmer") (r "=3.0.0-rc.2") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.0-rc.2") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.0-rc.2") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.0-rc.2") (d #t) (k 0)))) (h "1lmxjj018dl3a7h8vjb2w9d6jrcd2vjmfdws160zw6cfbvfhxx99")))

(define-public crate-wasmer-middlewares-3.0.0-rc.3 (c (n "wasmer-middlewares") (v "3.0.0-rc.3") (d (list (d (n "wasmer") (r "=3.0.0-rc.3") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.0-rc.3") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.0-rc.3") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.0-rc.3") (d #t) (k 0)))) (h "0dhwf10ipnfc1rgnj42rhma8yyhrmdq2sdqdzjc90l7wbgshhg6j")))

(define-public crate-wasmer-middlewares-3.0.0-rc.4 (c (n "wasmer-middlewares") (v "3.0.0-rc.4") (d (list (d (n "wasmer") (r "=3.0.0-rc.4") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.0-rc.4") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.0-rc.4") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.0-rc.4") (d #t) (k 0)))) (h "0bci870ymnazqdizacrjchiqivn6xkwzrg62r9xvz1113fvcfdcz")))

(define-public crate-wasmer-middlewares-3.0.0 (c (n "wasmer-middlewares") (v "3.0.0") (d (list (d (n "wasmer") (r "=3.0.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.0") (d #t) (k 0)))) (h "1smxpfh93lsgjvxl1c4nsnk5grd5s6dr5xsjblmyds7yd9f9jzsl")))

(define-public crate-wasmer-middlewares-3.0.1 (c (n "wasmer-middlewares") (v "3.0.1") (d (list (d (n "wasmer") (r "=3.0.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.1") (d #t) (k 0)))) (h "0db0868x7nrlaqh05gyka0zdini9jp5aj4aa350lq8w4100fqgv4")))

(define-public crate-wasmer-middlewares-3.0.2 (c (n "wasmer-middlewares") (v "3.0.2") (d (list (d (n "wasmer") (r "=3.0.2") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.2") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.2") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.2") (d #t) (k 0)))) (h "1l821y0h3d705jc09hn1n8lb9jvsmngbfc5h7yarm0a98xfcl46h")))

(define-public crate-wasmer-middlewares-3.1.0 (c (n "wasmer-middlewares") (v "3.1.0") (d (list (d (n "wasmer") (r "=3.1.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.1.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.1.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.1.0") (d #t) (k 0)))) (h "0ad737hpfzqjjmbsikzrbzv3hkylapnmsa9gd9h14lhb17mjkgiy")))

(define-public crate-wasmer-middlewares-3.2.0-alpha.1 (c (n "wasmer-middlewares") (v "3.2.0-alpha.1") (d (list (d (n "wasmer") (r "=3.2.0-alpha.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.2.0-alpha.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.2.0-alpha.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.2.0-alpha.1") (d #t) (k 0)))) (h "0283xq4zm5ny8jvpsh1sbllw2071b8407qdc92zq9506ks88pdcc")))

(define-public crate-wasmer-middlewares-3.0.3 (c (n "wasmer-middlewares") (v "3.0.3") (d (list (d (n "wasmer") (r "=3.0.3") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.0.3") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.0.3") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.0.3") (d #t) (k 0)))) (h "102qsnfa7kcw99fvqiq4zn0894qml4fy965h1rfyrksi9srm8y09")))

(define-public crate-wasmer-middlewares-3.1.1 (c (n "wasmer-middlewares") (v "3.1.1") (d (list (d (n "wasmer") (r "=3.1.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.1.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.1.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.1.1") (d #t) (k 0)))) (h "03x1nly6yyc9g8xcgccrrhzja4ksxfi8w5g0cg6pyj007m2b5xy3")))

(define-public crate-wasmer-middlewares-3.2.0-beta.1 (c (n "wasmer-middlewares") (v "3.2.0-beta.1") (d (list (d (n "wasmer") (r "=3.2.0-beta.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.2.0-beta.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.2.0-beta.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.2.0-beta.1") (d #t) (k 0)))) (h "046z7lw9jrgbm3nshbj4kcrb4f2c2hlyskf1azfh8hsnixhybmnx")))

(define-public crate-wasmer-middlewares-3.2.0-beta.2 (c (n "wasmer-middlewares") (v "3.2.0-beta.2") (d (list (d (n "wasmer") (r "=3.2.0-beta.2") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.2.0-beta.2") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.2.0-beta.2") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.2.0-beta.2") (d #t) (k 0)))) (h "1a7034yznzcdnh8v715wy0b95rrnlg16iickl4xlz1bs2cqil2cd")))

(define-public crate-wasmer-middlewares-3.2.0 (c (n "wasmer-middlewares") (v "3.2.0") (d (list (d (n "wasmer") (r "=3.2.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.2.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.2.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.2.0") (d #t) (k 0)))) (h "0fqixn35iir5dpkmqxsbvf5czldgjww5040whnlv1cyz5v2y8sa1")))

(define-public crate-wasmer-middlewares-3.2.1 (c (n "wasmer-middlewares") (v "3.2.1") (d (list (d (n "wasmer") (r "=3.2.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.2.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.2.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.2.1") (d #t) (k 0)))) (h "1z8iv8k1yif0371nnwhx72qlkslnlyxffcnlyv40mnxv1yf93igk")))

(define-public crate-wasmer-middlewares-3.3.0 (c (n "wasmer-middlewares") (v "3.3.0") (d (list (d (n "wasmer") (r "=3.3.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=3.3.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=3.3.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "=3.3.0") (d #t) (k 0)))) (h "05na4hclgdcrs0gy1c8ig3bsp7n0rjv7903nxcdi2d8hh49q00ly")))

(define-public crate-wasmer-middlewares-4.0.0-alpha.1 (c (n "wasmer-middlewares") (v "4.0.0-alpha.1") (d (list (d (n "wasmer") (r "=4.0.0-alpha.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.0.0-alpha.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.0.0-alpha.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.0.0-alpha.1") (d #t) (k 0)))) (h "0v0sg2s0z05b2yavsfmpw9wcj9rv337y61wx6hfzmq2fspamsmk3")))

(define-public crate-wasmer-middlewares-4.0.0-beta.1 (c (n "wasmer-middlewares") (v "4.0.0-beta.1") (d (list (d (n "wasmer") (r "=4.0.0-beta.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.0.0-beta.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.0.0-beta.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.0.0-beta.1") (d #t) (k 0)))) (h "1xmrpy6bz9qd7gshibmnpnn73lhkyb1gsq7ra6skn2y13yjf9bzs") (r "1.67")))

(define-public crate-wasmer-middlewares-4.0.0-beta.2 (c (n "wasmer-middlewares") (v "4.0.0-beta.2") (d (list (d (n "wasmer") (r "=4.0.0-beta.2") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.0.0-beta.2") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.0.0-beta.2") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.0.0-beta.2") (d #t) (k 0)))) (h "07qrghqn2y384x1yhhklsjfp0spziwlbjjimrmd13i6jm9z9pynf") (r "1.67")))

(define-public crate-wasmer-middlewares-4.0.0-beta.3 (c (n "wasmer-middlewares") (v "4.0.0-beta.3") (d (list (d (n "wasmer") (r "=4.0.0-beta.3") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.0.0-beta.3") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.0.0-beta.3") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.0.0-beta.3") (d #t) (k 0)))) (h "1id5849kbrpgrdbzh6h8kq2im2f7q6wxpix4xw84780k0rcxj7i7") (r "1.67")))

(define-public crate-wasmer-middlewares-4.0.0 (c (n "wasmer-middlewares") (v "4.0.0") (d (list (d (n "wasmer") (r "=4.0.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.0.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.0.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.0.0") (d #t) (k 0)))) (h "1p5dwhp4qaiq9v163xflm230l8phq050j7gwlwhwybhi0bzcmj1v") (r "1.67")))

(define-public crate-wasmer-middlewares-4.1.0 (c (n "wasmer-middlewares") (v "4.1.0") (d (list (d (n "wasmer") (r "=4.1.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.1.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.1.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.1.0") (d #t) (k 0)))) (h "1kn8yy0sg6w7szcfkavf0kl8g4534acqgj9h2s81mn92nsks51yf") (r "1.67")))

(define-public crate-wasmer-middlewares-4.1.1 (c (n "wasmer-middlewares") (v "4.1.1") (d (list (d (n "wasmer") (r "=4.1.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.1.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.1.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.1.1") (d #t) (k 0)))) (h "1qb56hdm9zgxf7fph4rsyz4z8ic3g41l40dl52dxas3nyc4wdhf3") (r "1.67")))

(define-public crate-wasmer-middlewares-4.1.2 (c (n "wasmer-middlewares") (v "4.1.2") (d (list (d (n "wasmer") (r "=4.1.2") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.1.2") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.1.2") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.1.2") (d #t) (k 0)))) (h "07z8kz5ixa2s1a63y40fr700wv6fd90df0lksvqi259fivxhiz5c") (r "1.67")))

(define-public crate-wasmer-middlewares-4.2.0 (c (n "wasmer-middlewares") (v "4.2.0") (d (list (d (n "wasmer") (r "=4.2.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.2.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.2.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.2.0") (d #t) (k 0)))) (h "0xnd4vk5d7wh9ry5z444lc0yb2fillsqvdpdh6ls93jak71kc1jq") (r "1.67")))

(define-public crate-wasmer-middlewares-4.2.1 (c (n "wasmer-middlewares") (v "4.2.1") (d (list (d (n "wasmer") (r "=4.2.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.2.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.2.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.2.1") (d #t) (k 0)))) (h "156whbb77gv1lq35hcm98kj9g90gfcla7ax81mbhphzsrp5d3fi8") (r "1.70")))

(define-public crate-wasmer-middlewares-4.2.2 (c (n "wasmer-middlewares") (v "4.2.2") (d (list (d (n "wasmer") (r "=4.2.2") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.2.2") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.2.2") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.2.2") (d #t) (k 0)))) (h "0c6sz9bz3mb7yfskvfzwr4iqc76hp998zax8h5n67y591rybid7f") (r "1.70")))

(define-public crate-wasmer-middlewares-4.2.3 (c (n "wasmer-middlewares") (v "4.2.3") (d (list (d (n "wasmer") (r "=4.2.3") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.2.3") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.2.3") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.2.3") (d #t) (k 0)))) (h "1235mr70jsygvczfxrxkzar5n4nw71w1jwrqzz7ml56vla0ygk9d") (r "1.70")))

(define-public crate-wasmer-middlewares-4.2.4 (c (n "wasmer-middlewares") (v "4.2.4") (d (list (d (n "wasmer") (r "=4.2.4") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.2.4") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.2.4") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.2.4") (d #t) (k 0)))) (h "11r1g3vgzsjgpkyk1j731ppn5r8aj8s1z1f8fra35ddpfrzz5m36") (r "1.70")))

(define-public crate-wasmer-middlewares-4.2.5 (c (n "wasmer-middlewares") (v "4.2.5") (d (list (d (n "wasmer") (r "=4.2.5") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.2.5") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.2.5") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.2.5") (d #t) (k 0)))) (h "0xwyipp9823c1c8iahc79z6axsb6757jpf4x50lmkgw13xx39cda") (r "1.70")))

(define-public crate-wasmer-middlewares-4.2.6 (c (n "wasmer-middlewares") (v "4.2.6") (d (list (d (n "wasmer") (r "=4.2.6") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.2.6") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.2.6") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.2.6") (d #t) (k 0)))) (h "0nphwyi2zqxjpiv5a7hfz0s6l9wqsxqh4v2f170wbhc5q4wysih3") (r "1.73")))

(define-public crate-wasmer-middlewares-4.2.8 (c (n "wasmer-middlewares") (v "4.2.8") (d (list (d (n "wasmer") (r "=4.2.8") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.2.8") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.2.8") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.2.8") (d #t) (k 0)))) (h "01sja16cjf79amjp6igia1kkq6dlvm48gf9xzkz78ipx9mbmsp1b") (r "1.73")))

(define-public crate-wasmer-middlewares-4.3.0-alpha.1 (c (n "wasmer-middlewares") (v "4.3.0-alpha.1") (d (list (d (n "wasmer") (r "=4.3.0-alpha.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.3.0-alpha.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.3.0-alpha.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.3.0-alpha.1") (d #t) (k 0)))) (h "15d3yb1pkj9l817g3miwsnmsl9nz3fpbj3qhci0z2myw2j6d1y71") (r "1.73")))

(define-public crate-wasmer-middlewares-4.3.0-beta.1 (c (n "wasmer-middlewares") (v "4.3.0-beta.1") (d (list (d (n "wasmer") (r "=4.3.0-beta.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.3.0-beta.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.3.0-beta.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.3.0-beta.1") (d #t) (k 0)))) (h "01h3jgzbxdi0iikzlmbvyrl18ljc8ca14ljl2aiiqqz9wc4cqd9i") (r "1.73")))

(define-public crate-wasmer-middlewares-4.3.0 (c (n "wasmer-middlewares") (v "4.3.0") (d (list (d (n "wasmer") (r "=4.3.0") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.3.0") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.3.0") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.3.0") (d #t) (k 0)))) (h "1gdz758xk42xbq24pnfhj8fm1vdxmahc4blic5x9lx0l8ip5yhqq") (r "1.74")))

(define-public crate-wasmer-middlewares-4.3.1 (c (n "wasmer-middlewares") (v "4.3.1") (d (list (d (n "wasmer") (r "=4.3.1") (f (quote ("compiler"))) (k 0)) (d (n "wasmer") (r "=4.3.1") (f (quote ("compiler"))) (d #t) (k 2)) (d (n "wasmer-types") (r "=4.3.1") (d #t) (k 0)) (d (n "wasmer-vm") (r "=4.3.1") (d #t) (k 0)))) (h "0zp7d8jimbp4pwqzi2d5i4sky4apsw6ih4b1fbaqyivscrjbwzb6") (r "1.74")))

