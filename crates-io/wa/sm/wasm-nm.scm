(define-module (crates-io wa sm wasm-nm) #:use-module (crates-io))

(define-public crate-wasm-nm-0.1.0 (c (n "wasm-nm") (v "0.1.0") (d (list (d (n "clap") (r "^2.29.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.17.0") (d #t) (k 0)))) (h "057blxyaj8icxi09r1jakwz0p9i31c5gmxafkriinpq6gqr5fmgz") (f (quote (("exe" "clap") ("default" "exe"))))))

(define-public crate-wasm-nm-0.2.0 (c (n "wasm-nm") (v "0.2.0") (d (list (d (n "clap") (r "^2.29.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.19.0") (d #t) (k 0)))) (h "1czbxk335zbjnbgmj2fap2gpsnccbcn7bcyy7p8yj8136s248266") (f (quote (("exe" "clap") ("default" "exe"))))))

(define-public crate-wasm-nm-0.2.1 (c (n "wasm-nm") (v "0.2.1") (d (list (d (n "clap") (r "^2.29.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.19.0") (d #t) (k 0)))) (h "0ah6b885a1qzyqd6kb7skcj052dxaj7yx0gryz0kwfhp9xr5kkxq") (f (quote (("exe" "clap") ("default" "exe"))))))

