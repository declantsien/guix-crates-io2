(define-module (crates-io wa sm wasmi-validation) #:use-module (crates-io))

(define-public crate-wasmi-validation-0.1.0 (c (n "wasmi-validation") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (o #t) (d #t) (k 0)) (d (n "parity-wasm") (r "^0.31") (k 0)))) (h "1wrkx9bg92gg7p7gpkbcj4h2ka22dsbs3h3rmqi8agjb8j902f5b") (f (quote (("std" "parity-wasm/std") ("default" "std") ("core" "hashbrown/nightly"))))))

(define-public crate-wasmi-validation-0.2.0 (c (n "wasmi-validation") (v "0.2.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "parity-wasm") (r "^0.40.1") (k 0)))) (h "1241khry3mw3xl8xjdfpgvi32mci86bskn7pqygn6vpm7mp3bh3b") (f (quote (("std" "parity-wasm/std") ("default" "std") ("core"))))))

(define-public crate-wasmi-validation-0.3.0 (c (n "wasmi-validation") (v "0.3.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "parity-wasm") (r "^0.41.0") (k 0)))) (h "14sd4q5lzyas42ixsp56498hf6f0rzsf50cj16b3b9sb0sbway7a") (f (quote (("std" "parity-wasm/std") ("default" "std") ("core"))))))

(define-public crate-wasmi-validation-0.4.0 (c (n "wasmi-validation") (v "0.4.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "parity-wasm") (r "^0.42.0") (k 0)))) (h "1f04snbxmc7z9wvhgqdwi15fg15zxrh0nlzgxx4bxn4n0y38xsx2") (f (quote (("std" "parity-wasm/std") ("default" "std") ("core"))))))

(define-public crate-wasmi-validation-0.4.1 (c (n "wasmi-validation") (v "0.4.1") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "parity-wasm") (r "^0.42.0") (k 0)))) (h "0dvrdppm7nybrzqh53bg7vps5j824xsq1qnaxc4zq660svn46lqn") (f (quote (("std" "parity-wasm/std") ("default" "std") ("core"))))))

(define-public crate-wasmi-validation-0.4.2 (c (n "wasmi-validation") (v "0.4.2") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "parity-wasm") (r "^0.45.0") (k 0)))) (h "0fxn07p1yhyd2sjyzi2a9sl9vb5d88lggyxkdg3kqrybncnqlcvl") (f (quote (("std" "parity-wasm/std") ("default" "std")))) (y #t)))

(define-public crate-wasmi-validation-0.5.0 (c (n "wasmi-validation") (v "0.5.0") (d (list (d (n "assert_matches") (r "^1.1") (d #t) (k 2)) (d (n "parity-wasm") (r "^0.45.0") (k 0)))) (h "0yw61v785f9l5a5sv84sfjcg1h3lmdgmvv96m7jl437zs5m43zwi") (f (quote (("std" "parity-wasm/std") ("default" "std"))))))

