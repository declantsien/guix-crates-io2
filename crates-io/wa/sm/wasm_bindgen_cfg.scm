(define-module (crates-io wa sm wasm_bindgen_cfg) #:use-module (crates-io))

(define-public crate-wasm_bindgen_cfg-0.1.0 (c (n "wasm_bindgen_cfg") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0f3viz1xx8vv4kq0yr4p5vy6gw0pb03knbvrz22wwrqqrdspjjjz")))

(define-public crate-wasm_bindgen_cfg-0.2.0 (c (n "wasm_bindgen_cfg") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "11wl37nxj7f5ynwc735sdrj62gk2fr2vh50fmj0h360bnxq0sk3k")))

