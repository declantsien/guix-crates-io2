(define-module (crates-io wa sm wasm-bindgen-shared) #:use-module (crates-io))

(define-public crate-wasm-bindgen-shared-0.1.0 (c (n "wasm-bindgen-shared") (v "0.1.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0bcr8w8ly6xna6fzv7hh1xhxswh4sx97hpynhsp7p7dla7lq4qay")))

(define-public crate-wasm-bindgen-shared-0.1.1 (c (n "wasm-bindgen-shared") (v "0.1.1") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0ssnx0jp4cz0y3d54mh3fl3prlnjsn18ag8ll8gfrw96m0qgfz4b")))

(define-public crate-wasm-bindgen-shared-0.2.0 (c (n "wasm-bindgen-shared") (v "0.2.0") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1fqh2hd51s5njdc5cbpid13vyj1fcr0rm29bzr7kpvinzs6i4q3j")))

(define-public crate-wasm-bindgen-shared-0.2.1 (c (n "wasm-bindgen-shared") (v "0.2.1") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0yphwmpj7b1kl5hi5ramqf74xp7wvjx1ssyrylb8yrfcs8rvn1y3")))

(define-public crate-wasm-bindgen-shared-0.2.2 (c (n "wasm-bindgen-shared") (v "0.2.2") (d (list (d (n "fnv") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1aqcpmfhfszlxjlndqrz5va1x71xfq2khv8kq7jji8k5bdrzlb6a")))

(define-public crate-wasm-bindgen-shared-0.2.3 (c (n "wasm-bindgen-shared") (v "0.2.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "153xp9irfbvbyzg9cag4khw4mynxi92fd5gpgms5bx4c6vy7926m")))

(define-public crate-wasm-bindgen-shared-0.2.4 (c (n "wasm-bindgen-shared") (v "0.2.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0drnvvcdxrwjydh839sq3i9v3sianrmx88x9akbv4g449rx0b8mb")))

(define-public crate-wasm-bindgen-shared-0.2.5 (c (n "wasm-bindgen-shared") (v "0.2.5") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "19vfs6hn1zb8kbppjnzma8a4inq086whlx4v1nsaql8jkrr9ryx3")))

(define-public crate-wasm-bindgen-shared-0.2.6 (c (n "wasm-bindgen-shared") (v "0.2.6") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0vpkigfx7fmc5g41q89ggjrfh1am0v4a4mm4ncskx8bqfh5h1lfv")))

(define-public crate-wasm-bindgen-shared-0.2.7 (c (n "wasm-bindgen-shared") (v "0.2.7") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1nzzjj67z6bk87pyambrcz2kp1wwqzwkzmkp41dkqgab602260d1")))

(define-public crate-wasm-bindgen-shared-0.2.8 (c (n "wasm-bindgen-shared") (v "0.2.8") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "16v7xbyq8mq8yqwm0vql0p5idwc8xbas4pzbrx3k50v6vrnm4ad6")))

(define-public crate-wasm-bindgen-shared-0.2.9 (c (n "wasm-bindgen-shared") (v "0.2.9") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0abpxbmnrkv7zqrn1nh1nz2g6vpgyjmzvav75cva6y84csk19c7l")))

(define-public crate-wasm-bindgen-shared-0.2.10 (c (n "wasm-bindgen-shared") (v "0.2.10") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "03n0bik9w1m03yj6x3fjzs9zwbiql4gjdplcvqbd0hjcdrmwnjr4")))

(define-public crate-wasm-bindgen-shared-0.2.11 (c (n "wasm-bindgen-shared") (v "0.2.11") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "11995d99sjnd7rwn05p2px78dskck4v66ghkr3pb4vrmm693kz0j")))

(define-public crate-wasm-bindgen-shared-0.2.12 (c (n "wasm-bindgen-shared") (v "0.2.12") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0ndf99zg4g62q08f8sg3qkpdfl0g1870x9kbr0ygsng6w0fpyvf9")))

(define-public crate-wasm-bindgen-shared-0.2.13 (c (n "wasm-bindgen-shared") (v "0.2.13") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1gv3isc0h026pzjdzm2xfd134z403dbcw376flyfkmv4wisj6422")))

(define-public crate-wasm-bindgen-shared-0.2.14 (c (n "wasm-bindgen-shared") (v "0.2.14") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1qqj86xpj2aqan288lmg522364mbsp7n9lblpjxab51hilpd5jv3")))

(define-public crate-wasm-bindgen-shared-0.2.15 (c (n "wasm-bindgen-shared") (v "0.2.15") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1ckjrg4v55bmgf0qyzbvffwzslcy2ig0xp4a3g7pfx5i60zqfigl")))

(define-public crate-wasm-bindgen-shared-0.2.16 (c (n "wasm-bindgen-shared") (v "0.2.16") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1c1g9kk07jbm57rn9mq9kf2znnfsvlkd2wgh7434bifsp68akgi2")))

(define-public crate-wasm-bindgen-shared-0.2.17 (c (n "wasm-bindgen-shared") (v "0.2.17") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "14zcbdhwvqig85asfbkdlgna66b7wvvgn4cr996sg252j13pnr0r")))

(define-public crate-wasm-bindgen-shared-0.2.18 (c (n "wasm-bindgen-shared") (v "0.2.18") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1sb26njnikwln5yasv5vfmncaqdx3hsh18ww7lcblyc3fmx2aakv")))

(define-public crate-wasm-bindgen-shared-0.2.19 (c (n "wasm-bindgen-shared") (v "0.2.19") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "14yxig2zapivsibwm33vxrdvdk1b2spxpza1m1mx4i8jbvcmmm0k")))

(define-public crate-wasm-bindgen-shared-0.2.20 (c (n "wasm-bindgen-shared") (v "0.2.20") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0h07p6a9v4l2i0rdkpnd7jfx6j2a5xi6k9pfhm0z8lcvsy7bd0f4")))

(define-public crate-wasm-bindgen-shared-0.2.21 (c (n "wasm-bindgen-shared") (v "0.2.21") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1kq1nj76ili9yf2fvqnwbpa1zxcpjav90cyb1fi94js8a5i2lfwf")))

(define-public crate-wasm-bindgen-shared-0.2.22 (c (n "wasm-bindgen-shared") (v "0.2.22") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0c84gnqdq3lnsa8ba4y97palnmr2aaqv4yxf2m4b8yrkr8cjqgv6")))

(define-public crate-wasm-bindgen-shared-0.2.23 (c (n "wasm-bindgen-shared") (v "0.2.23") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0vymaz1sd1xd1dnvflsn4j3955ikvyczfgk9z8xq723gvfpan8vb")))

(define-public crate-wasm-bindgen-shared-0.2.24 (c (n "wasm-bindgen-shared") (v "0.2.24") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1qlah6z2n81vigzry977p0y41biw8ppkd0z25vlwimgpq7569f9z")))

(define-public crate-wasm-bindgen-shared-0.2.25 (c (n "wasm-bindgen-shared") (v "0.2.25") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0w04wawqqvgcdkjh3pvsxw6gpqxq3jcmrn7gwmnkgmibnj6bf6dp")))

(define-public crate-wasm-bindgen-shared-0.2.26 (c (n "wasm-bindgen-shared") (v "0.2.26") (h "11g5a3i2vbzyfjikdmmbchnqb9wsj2cd4k7krg3hhgzxg9h62xgk")))

(define-public crate-wasm-bindgen-shared-0.2.27 (c (n "wasm-bindgen-shared") (v "0.2.27") (h "0y7bq11gv0gv3ar3xpqbnlszzl3c1ilapn0y2v7alnywvcpcn7r5")))

(define-public crate-wasm-bindgen-shared-0.2.28 (c (n "wasm-bindgen-shared") (v "0.2.28") (h "1z7cym3k5s886qhhmxd0dph3jx6r0ixiknzqm8ar1q4132dfff0f")))

(define-public crate-wasm-bindgen-shared-0.2.29 (c (n "wasm-bindgen-shared") (v "0.2.29") (h "0qm1i7c49if6j0x0lhgmfz5wn6avpq5x4ygjjrgh4iaq9zgkf6m7")))

(define-public crate-wasm-bindgen-shared-0.2.30 (c (n "wasm-bindgen-shared") (v "0.2.30") (h "05bxjmz4mk9ki53l2glkbwa652p6kzmxn7l7x8ar8djd6wm9kja4")))

(define-public crate-wasm-bindgen-shared-0.2.31 (c (n "wasm-bindgen-shared") (v "0.2.31") (h "1j006hr4bfwkapm73a7qhznyvlhs3jl9nj6lmgla1disdgvrgilq")))

(define-public crate-wasm-bindgen-shared-0.2.32 (c (n "wasm-bindgen-shared") (v "0.2.32") (h "0qyrw9wyxnk8vzil6jm4pg3m49bz3f7j57zgnfdz2wywaww6xix6")))

(define-public crate-wasm-bindgen-shared-0.2.33 (c (n "wasm-bindgen-shared") (v "0.2.33") (h "0fpw2skhgph47lgj71m6rn8gv1v1say2hzgifrpxndicnkanqh4w")))

(define-public crate-wasm-bindgen-shared-0.2.34 (c (n "wasm-bindgen-shared") (v "0.2.34") (h "0vrkab02d517lqa7yb22mj6xpp0apxnhz5hpgwzrpsw5wgdkxcbp")))

(define-public crate-wasm-bindgen-shared-0.2.35 (c (n "wasm-bindgen-shared") (v "0.2.35") (h "1874wgjb0kikckjr5cjs4ndfa04h7yhwwffi8r2783x5rni1n5qs")))

(define-public crate-wasm-bindgen-shared-0.2.36 (c (n "wasm-bindgen-shared") (v "0.2.36") (h "0bx0pgrlqfcvfl7wn3rdyj6hkl4fn7aalci6lac0fpd74i5k34fi")))

(define-public crate-wasm-bindgen-shared-0.2.37 (c (n "wasm-bindgen-shared") (v "0.2.37") (h "1gwvzm6da2wc067f6msiq2i2vdg25p4bl1fv6rv46lcxjmdmqc2r")))

(define-public crate-wasm-bindgen-shared-0.2.38 (c (n "wasm-bindgen-shared") (v "0.2.38") (h "1ifcaph1xcfvh53vf2cbgjlq48zrzfzn90jh9vypz26ik1qh8lkg")))

(define-public crate-wasm-bindgen-shared-0.2.39 (c (n "wasm-bindgen-shared") (v "0.2.39") (h "0w0882i5hph18vml5234h69bf9g24z8gzr7hacxwrpn7v91pdxbh")))

(define-public crate-wasm-bindgen-shared-0.2.40 (c (n "wasm-bindgen-shared") (v "0.2.40") (h "0wk6n82w3icq7i1l8mxii3fcvkzlwavvfj8gnnb4sgigdyvc6mqd") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.41 (c (n "wasm-bindgen-shared") (v "0.2.41") (h "091g022bsn38w13z68qy4dczspj7in5pgcphcwmfdwv8xxxa7rls") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.42 (c (n "wasm-bindgen-shared") (v "0.2.42") (h "1zn9kapb6kiiszd6xnic5cc12nxchhd0c03lxmn7n58sdq934v1j") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.43 (c (n "wasm-bindgen-shared") (v "0.2.43") (h "0r2a585xa9irjkj0b58xfq7ih6sq8vf4nindvi4pmgzpszs4j69j") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.44 (c (n "wasm-bindgen-shared") (v "0.2.44") (h "1cv5r8bdq74awb0qm7ha0284pmfhrn0ra2hk6hg5zhpzys7gf5sn") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.45 (c (n "wasm-bindgen-shared") (v "0.2.45") (h "19pl6p95kpw5s28xvi9cahrr1l9kb38n0z5d57ifaybsgvh830y0") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.46 (c (n "wasm-bindgen-shared") (v "0.2.46") (h "02lcvmyssm7396yrpz6icjbpqgrd41h0d7xl9m318jm38cj2jr4q") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.47 (c (n "wasm-bindgen-shared") (v "0.2.47") (h "1d0kg65wjrcqjxb21r9jgmhf3fli2h1312srhx545z8cngfidphm") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.48 (c (n "wasm-bindgen-shared") (v "0.2.48") (h "08rnfhjyk0f6liv8n4rdsvhx7r02glkhcbj2lp9lcbkbfpad9hnr") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.49 (c (n "wasm-bindgen-shared") (v "0.2.49") (h "0qrmcxsr1aq02v96p2w80lmiiyl09j920sra7w5p2w0dpddn4hli") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.50 (c (n "wasm-bindgen-shared") (v "0.2.50") (h "0z826ky62159jrgn0dpbnmdsjigx8m002pn1138zikbh6nyc27yl") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.51 (c (n "wasm-bindgen-shared") (v "0.2.51") (h "1l9iyx6jp2y49i0bk4klivrxkjs9b076dv5mv26h7bx7hvlizml3") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.52 (c (n "wasm-bindgen-shared") (v "0.2.52") (h "0niwdkkgrbl7nfpyalx9b6q9jvalfs1d4h6lbapxr35p646jn9qy") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.53 (c (n "wasm-bindgen-shared") (v "0.2.53") (h "0s5d91p8q02srs49bk68b1ijgnq3zc9sasryvvrjwrwrmmkzx4vl") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.54 (c (n "wasm-bindgen-shared") (v "0.2.54") (h "0fcw2ybjxplbmj9krbm1f53dih9lz2hhznhj5fn7zzdz1dzidrwa") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.55 (c (n "wasm-bindgen-shared") (v "0.2.55") (h "0gdmid85jr3yy34xfdnz99rwla27g70csj8xbnwqk1dysgb7h2ya") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.56 (c (n "wasm-bindgen-shared") (v "0.2.56") (h "0w4sqh0br5j1zsix8k3dyzdlsxiyfbzyzf28md75bcq6hm7rh1yr") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.57 (c (n "wasm-bindgen-shared") (v "0.2.57") (h "042qks1qlph8w0qnckh7llswc20z21k4f709gc0yialmwf5113nx") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.58 (c (n "wasm-bindgen-shared") (v "0.2.58") (h "00cnbabf4k9bahb217vkilmjwqwzpwp112vlvgfw1x19r4gydrzm") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.59 (c (n "wasm-bindgen-shared") (v "0.2.59") (h "1n6xr9x5sxqml3680qlbzxfhgka4xf83plfllcch4nzja6nkd7zw") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.60 (c (n "wasm-bindgen-shared") (v "0.2.60") (h "0ffn4152w8n629f29lwjgj3adiyixvdbff3mld49gisssbknzxys") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.61 (c (n "wasm-bindgen-shared") (v "0.2.61") (h "0a8x4cig1s4cs7z2m872sd0qnlwpwk1csmm3irpwl2zx3rl7pxvi") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.62 (c (n "wasm-bindgen-shared") (v "0.2.62") (h "1baqpg0riw5yysyln0a8c1j4fjyr5nmglyjh2vish5ww24b2j759") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.63 (c (n "wasm-bindgen-shared") (v "0.2.63") (h "1k8saav8a80pxcr2dyr9rn9r8a0fvirxlbimdzdz9njq7abikfn9") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.64 (c (n "wasm-bindgen-shared") (v "0.2.64") (h "1bkrpl9qlxvr9pbfq5xrkcyydm9lw914bm35sxpx0bb3dkm90yvz") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.65 (c (n "wasm-bindgen-shared") (v "0.2.65") (h "11synnxxsdn5894sgvzllcf8smfc7f7w6y0chrirsm7d1lic1dkj") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.66 (c (n "wasm-bindgen-shared") (v "0.2.66") (h "0djj29cs7k90yngsbd70snmahzmkjar5bm7gm142f9kjd0jmrq32") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.67 (c (n "wasm-bindgen-shared") (v "0.2.67") (h "14h0wy003w4gzxzmv4b49p8hzc8805b95dy465chwc9l1rc65cck") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.68 (c (n "wasm-bindgen-shared") (v "0.2.68") (h "01w3w2dxi2i16l8r5j9331wkdmhvim36m2fyphzpv38h8lqrlr0x") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.69 (c (n "wasm-bindgen-shared") (v "0.2.69") (h "0n3ir6gq27np22l6m96y342a6fphk1pkbzbfqx6g364kgzfi2y3y") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.70 (c (n "wasm-bindgen-shared") (v "0.2.70") (h "0r7xlkba8r5gfpp4w8bi9gfym08ybf5b6qirq4ajvq1sjkj4ajfx") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.71 (c (n "wasm-bindgen-shared") (v "0.2.71") (h "1cc3cc7lr1051avi24jwadh91kaandzq9991ybsp3p929328wvvx") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.72 (c (n "wasm-bindgen-shared") (v "0.2.72") (h "1yjzil4mndy1bzvm1xr1pbmw86ianjzhp9pa8sin0jgvjr38y55p") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.73 (c (n "wasm-bindgen-shared") (v "0.2.73") (h "12a4kq5wvy3znkqlqn6ib25ips1k9apxjpknpca3s8xacsp479fr") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.74 (c (n "wasm-bindgen-shared") (v "0.2.74") (h "0ksbnhjzvnsmzc1n18bs98swnrzrwxjrpx3clrsyv3pip1vgikyp") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.75 (c (n "wasm-bindgen-shared") (v "0.2.75") (h "1hmwidww6s80kgr3k9jr389s8jzsg8frdpjfbza655ih79s4l31f") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.76 (c (n "wasm-bindgen-shared") (v "0.2.76") (h "0aaa3zyfvppzzflifd7a069mjivp7zj7gz89bzxa2x2mhid0gnxc") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.77 (c (n "wasm-bindgen-shared") (v "0.2.77") (h "1d72hyf2h7rzrjbzmb6dn2gvprmyn2vkbahyiwm7bs58qxvf6yf2") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.78 (c (n "wasm-bindgen-shared") (v "0.2.78") (h "1k27dc57h0brx5ish4dwmzibyif7m9lfagvph1a7s0ygi4kj6dq2") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.79 (c (n "wasm-bindgen-shared") (v "0.2.79") (h "18h67l9b9jn06iw9r2p7bh9i0brh24lilcp4f26f4f24bh1qv59x") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.80 (c (n "wasm-bindgen-shared") (v "0.2.80") (h "0i37ljck1c6l4xypx042ybjcm2lb3xfdjs4lk96rdrfy63svfm6m") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.81 (c (n "wasm-bindgen-shared") (v "0.2.81") (h "1gk1abf8w1bjhhr1a7x1lbj0zadhjd6rrksaxickcpwyv4dr32ba") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.82 (c (n "wasm-bindgen-shared") (v "0.2.82") (h "02hlb8hvfxzvgbqdhc2fh20xrb027sraacb5zyai1mf7sc5xv635") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.83 (c (n "wasm-bindgen-shared") (v "0.2.83") (h "0zzz9xfi3fp2n5ihhlq8ws7674a2ir2frvsd1d7yr4sxad2w0f0w") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.84 (c (n "wasm-bindgen-shared") (v "0.2.84") (h "0pcvk1c97r1pprzfaxxn359r0wqg5bm33ylbwgjh8f4cwbvzwih0") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.85 (c (n "wasm-bindgen-shared") (v "0.2.85") (h "1swi4sz4bd6jb00ddpy3345p1b3rz2pxykijph8xg97sra9da0d9") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.86 (c (n "wasm-bindgen-shared") (v "0.2.86") (h "14ra4j13kqjpyxz82ndzf8wlpwnbkdzjvvig934iz7a00m1mp7gd") (l "wasm_bindgen")))

(define-public crate-wasm-bindgen-shared-0.2.87 (c (n "wasm-bindgen-shared") (v "0.2.87") (h "18bmjwvfyhvlq49nzw6mgiyx4ys350vps4cmx5gvzckh91dd0sna") (l "wasm_bindgen") (r "1.56")))

(define-public crate-wasm-bindgen-shared-0.2.88 (c (n "wasm-bindgen-shared") (v "0.2.88") (h "02vmw2rzsla1qm0zgfng4kqz52xn8k54v8ads4g1macv09fnq10d") (l "wasm_bindgen") (r "1.57")))

(define-public crate-wasm-bindgen-shared-0.2.89 (c (n "wasm-bindgen-shared") (v "0.2.89") (h "17s5rppad113c6ggkaq8c3cg7a3zz15i78wxcg6mcl1n15iv7fbs") (l "wasm_bindgen") (r "1.57")))

(define-public crate-wasm-bindgen-shared-0.2.90 (c (n "wasm-bindgen-shared") (v "0.2.90") (h "0av0m0shdg1jxhf66ymjbq03m0qb7ypm297glndm7mri3hxl34ad") (l "wasm_bindgen") (r "1.57")))

(define-public crate-wasm-bindgen-shared-0.2.91 (c (n "wasm-bindgen-shared") (v "0.2.91") (h "0f4qmjv57ppwi4xpdxgcd77vz9vmvlrnybg8dj430hzhvk96n62g") (l "wasm_bindgen") (r "1.57")))

(define-public crate-wasm-bindgen-shared-0.2.92 (c (n "wasm-bindgen-shared") (v "0.2.92") (h "15kyavsrna2cvy30kg03va257fraf9x00ny554vxngvpyaa0q6dg") (l "wasm_bindgen") (r "1.57")))

