(define-module (crates-io wa sm wasmer-experimental-io-devices-lib) #:use-module (crates-io))

(define-public crate-wasmer-experimental-io-devices-lib-0.1.0 (c (n "wasmer-experimental-io-devices-lib") (v "0.1.0") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0z32y9q5w62q06839h9a556wa7v8ck124r47jczf01zgl9sr6ml1")))

(define-public crate-wasmer-experimental-io-devices-lib-0.1.1 (c (n "wasmer-experimental-io-devices-lib") (v "0.1.1") (d (list (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hq4q9qhsgvyx6sv7va41r2ljq56j5c5fwmhdij5ind6b3s5m5bh")))

