(define-module (crates-io wa sm wasmtime-wasi-crypto) #:use-module (crates-io))

(define-public crate-wasmtime-wasi-crypto-0.23.0 (c (n "wasmtime-wasi-crypto") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.23.0") (k 0)) (d (n "wasmtime-wiggle") (r "^0.23.0") (d #t) (k 0)) (d (n "wiggle") (r "^0.23.0") (d #t) (k 0)))) (h "12jrqrc3s6vdk81c77r3m1lf5y7xva68pa1niasj55lw31076a4h")))

(define-public crate-wasmtime-wasi-crypto-0.24.0 (c (n "wasmtime-wasi-crypto") (v "0.24.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.24.0") (k 0)) (d (n "wasmtime-wiggle") (r "^0.24.0") (d #t) (k 0)) (d (n "wiggle") (r "^0.24.0") (d #t) (k 0)))) (h "1kz75f26j6vpimsibznw0hyqdkf118p8qxyl1d6ph36lgsi2pbnw")))

(define-public crate-wasmtime-wasi-crypto-0.25.0 (c (n "wasmtime-wasi-crypto") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.25.0") (k 0)) (d (n "wasmtime-wiggle") (r "^0.25.0") (d #t) (k 0)) (d (n "wiggle") (r "^0.25.0") (d #t) (k 0)))) (h "18nbhxk83rayz1hi6rhy9z91dc1273j7n5djylnjkpy7q56r0h8d")))

(define-public crate-wasmtime-wasi-crypto-0.26.0 (c (n "wasmtime-wasi-crypto") (v "0.26.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.26.0") (k 0)) (d (n "wasmtime-wiggle") (r "^0.26.0") (d #t) (k 0)) (d (n "wiggle") (r "^0.26.0") (d #t) (k 0)))) (h "0d7ri2m5gh296wn0lzl7gakwq4argxvjw2djp5ynmdgyiy1qlzis")))

(define-public crate-wasmtime-wasi-crypto-0.27.0 (c (n "wasmtime-wasi-crypto") (v "0.27.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.27.0") (k 0)) (d (n "wasmtime-wiggle") (r "^0.27.0") (d #t) (k 0)) (d (n "wiggle") (r "^0.27.0") (d #t) (k 0)))) (h "1grjlhk8q506wia0n63jidpn9204k984n0gsbi722hy6qp2vmr8s")))

(define-public crate-wasmtime-wasi-crypto-0.26.1 (c (n "wasmtime-wasi-crypto") (v "0.26.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.26.1") (k 0)) (d (n "wasmtime-wiggle") (r "^0.26.1") (d #t) (k 0)) (d (n "wiggle") (r "^0.26.1") (d #t) (k 0)))) (h "0ylfhd7n6irb57dp3ilcg9lcb8m226017132lypv238my36xx8l3")))

(define-public crate-wasmtime-wasi-crypto-0.28.0 (c (n "wasmtime-wasi-crypto") (v "0.28.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.28.0") (k 0)) (d (n "wiggle") (r "^0.28.0") (d #t) (k 0)))) (h "0ywi57kpn7aip99bhr6dj5mx3vwr45qf2k49s8a9y36g0i1fn48h")))

(define-public crate-wasmtime-wasi-crypto-0.29.0 (c (n "wasmtime-wasi-crypto") (v "0.29.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.29.0") (k 0)) (d (n "wiggle") (r "^0.29.0") (d #t) (k 0)))) (h "1gnm65gd3h2nblfh21h4xn0lc2mxls3sb23h2fsiwfa1hspmj7nl")))

(define-public crate-wasmtime-wasi-crypto-0.30.0 (c (n "wasmtime-wasi-crypto") (v "0.30.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.4") (d #t) (k 0)) (d (n "wasmtime") (r "^0.30.0") (k 0)) (d (n "wiggle") (r "^0.30.0") (d #t) (k 0)))) (h "18zac2927br5qd7bbdjrwmwwz6gxj1bgxx7kmhqxqdfhq7028q0m")))

(define-public crate-wasmtime-wasi-crypto-0.31.0 (c (n "wasmtime-wasi-crypto") (v "0.31.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.31.0") (k 0)) (d (n "wiggle") (r "=0.31.0") (d #t) (k 0)))) (h "0gyljp3l7xn0qnbhphwa7pq5898yvp0m07zkaf47wv4gbrpf0rqs")))

(define-public crate-wasmtime-wasi-crypto-0.32.0 (c (n "wasmtime-wasi-crypto") (v "0.32.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.32.0") (k 0)) (d (n "wiggle") (r "=0.32.0") (d #t) (k 0)))) (h "0yifr433iyasnlbcmqs1hz536cn80kwiri4jhiq0pg0b26v1x262")))

(define-public crate-wasmtime-wasi-crypto-0.32.1 (c (n "wasmtime-wasi-crypto") (v "0.32.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.32.1") (k 0)) (d (n "wiggle") (r "=0.32.1") (d #t) (k 0)))) (h "048hlx5wcnjqw7pzfzagxw2rwhl119nikcrdqqxm46g5qlhfsx9s")))

(define-public crate-wasmtime-wasi-crypto-0.33.0 (c (n "wasmtime-wasi-crypto") (v "0.33.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.33.0") (k 0)) (d (n "wiggle") (r "=0.33.0") (d #t) (k 0)))) (h "1v2nmwb4yn9059vcm8cakg5p59b52xsqaprrwv2n0qfsvfxnimda")))

(define-public crate-wasmtime-wasi-crypto-0.34.0 (c (n "wasmtime-wasi-crypto") (v "0.34.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.34.0") (k 0)) (d (n "wiggle") (r "=0.34.0") (d #t) (k 0)))) (h "11gh0pf43s7xgs7xbfkzx4zbiqhr2igybly812fpxvkpz3zg3p92")))

(define-public crate-wasmtime-wasi-crypto-0.34.1 (c (n "wasmtime-wasi-crypto") (v "0.34.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.34.1") (k 0)) (d (n "wiggle") (r "=0.34.1") (d #t) (k 0)))) (h "0wjrdvcmvz97l9rsaixr2jp3wr33h9h2yzy1qqy6h257apzq9lni")))

(define-public crate-wasmtime-wasi-crypto-0.33.1 (c (n "wasmtime-wasi-crypto") (v "0.33.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.33.1") (k 0)) (d (n "wiggle") (r "=0.33.1") (d #t) (k 0)))) (h "0svx5hziddhannw0zp2rk1pqyckaa7mjqgdy4s64rk9rkaiw1y5s")))

(define-public crate-wasmtime-wasi-crypto-0.35.0 (c (n "wasmtime-wasi-crypto") (v "0.35.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.35.0") (k 0)) (d (n "wiggle") (r "=0.35.0") (d #t) (k 0)))) (h "0gb2xylwkfsyhrdwlri16h5mi0g5n7brsp34cwajiixj6sxgssr2")))

(define-public crate-wasmtime-wasi-crypto-0.35.1 (c (n "wasmtime-wasi-crypto") (v "0.35.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.35.1") (k 0)) (d (n "wiggle") (r "=0.35.1") (d #t) (k 0)))) (h "1xcjcmqz8yhdc5674pqkwn497dwmv6jxv9wg95r6j3x0i9lvbxhh")))

(define-public crate-wasmtime-wasi-crypto-0.35.2 (c (n "wasmtime-wasi-crypto") (v "0.35.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.35.2") (k 0)) (d (n "wiggle") (r "=0.35.2") (d #t) (k 0)))) (h "1qprv93r7krbxnbv04n8ncp9y33hi4f2xxg465ivsd1iiamd5vbv")))

(define-public crate-wasmtime-wasi-crypto-0.34.2 (c (n "wasmtime-wasi-crypto") (v "0.34.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.34.2") (k 0)) (d (n "wiggle") (r "=0.34.2") (d #t) (k 0)))) (h "10r5882kv56q0zkxbjjyj8qdkvyi5fcjgx4vb7rd52jnwarbiy18")))

(define-public crate-wasmtime-wasi-crypto-0.35.3 (c (n "wasmtime-wasi-crypto") (v "0.35.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.35.3") (k 0)) (d (n "wiggle") (r "=0.35.3") (d #t) (k 0)))) (h "1n9xj0vh0rqcmiqfpywxj7mn91x8bsp38k4fjbjsb283zzbhrhc2")))

(define-public crate-wasmtime-wasi-crypto-0.36.0 (c (n "wasmtime-wasi-crypto") (v "0.36.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.36.0") (k 0)) (d (n "wiggle") (r "=0.36.0") (d #t) (k 0)))) (h "1hhv1zgxixjyyap5nplh8bqb09zcvavxmd30ddpf2c69ds9m1mkg")))

(define-public crate-wasmtime-wasi-crypto-0.37.0 (c (n "wasmtime-wasi-crypto") (v "0.37.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.37.0") (k 0)) (d (n "wiggle") (r "=0.37.0") (d #t) (k 0)))) (h "1fc4f6wk6arw3s62970siip1wicbrs5qp3i8k8jpgialmfmv5agg")))

(define-public crate-wasmtime-wasi-crypto-0.38.0 (c (n "wasmtime-wasi-crypto") (v "0.38.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.38.0") (k 0)) (d (n "wiggle") (r "=0.38.0") (d #t) (k 0)))) (h "0k1pvfzmp2blivgax81d0b327xv8dgzbqgn78sfav6xp500a25ji")))

(define-public crate-wasmtime-wasi-crypto-0.38.1 (c (n "wasmtime-wasi-crypto") (v "0.38.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.38.1") (k 0)) (d (n "wiggle") (r "=0.38.1") (d #t) (k 0)))) (h "0qqnc2n53mma5vc6nxiacdl56vg1c09d7zanqwmqjkwp1qna45px")))

(define-public crate-wasmtime-wasi-crypto-0.39.0 (c (n "wasmtime-wasi-crypto") (v "0.39.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.39.0") (k 0)) (d (n "wiggle") (r "=0.39.0") (d #t) (k 0)))) (h "1gng425y7bbxbdk77g7hwzxp2nmvz6yp2qrj70w8kl315h42pm6p")))

(define-public crate-wasmtime-wasi-crypto-0.38.2 (c (n "wasmtime-wasi-crypto") (v "0.38.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.38.2") (k 0)) (d (n "wiggle") (r "=0.38.2") (d #t) (k 0)))) (h "12yfhs5ab4g5jj018wvzvnlqzdir7fzx8d9j7px9mr7ixpl354zf")))

(define-public crate-wasmtime-wasi-crypto-0.38.3 (c (n "wasmtime-wasi-crypto") (v "0.38.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.38.3") (k 0)) (d (n "wiggle") (r "=0.38.3") (d #t) (k 0)))) (h "0613csmiri1al19y5mqr0xhc7kc86w87xviv099791g5ka9zgnwq")))

(define-public crate-wasmtime-wasi-crypto-0.39.1 (c (n "wasmtime-wasi-crypto") (v "0.39.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.39.1") (k 0)) (d (n "wiggle") (r "=0.39.1") (d #t) (k 0)))) (h "0g74xiv8bm8pa8sf7qjv3kgz9s2r46vmch15wh46kdnqyv6xrkl2")))

(define-public crate-wasmtime-wasi-crypto-0.40.0 (c (n "wasmtime-wasi-crypto") (v "0.40.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.40.0") (k 0)) (d (n "wiggle") (r "=0.40.0") (d #t) (k 0)))) (h "01bgwx49cfw4s91a2jx9vxbsg43wvv8z2xhbw872imyvbzwz3v9x")))

(define-public crate-wasmtime-wasi-crypto-0.40.1 (c (n "wasmtime-wasi-crypto") (v "0.40.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^0.40.1") (k 0)) (d (n "wiggle") (r "=0.40.1") (d #t) (k 0)))) (h "1yh75zn1jgblbv66f3phaw8jaq2r0q67yy8fdf0rk983h6j88yyx")))

(define-public crate-wasmtime-wasi-crypto-1.0.0 (c (n "wasmtime-wasi-crypto") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^1.0.0") (k 0)) (d (n "wiggle") (r "=1.0.0") (d #t) (k 0)))) (h "05yil31glmyr44iz0nyaisd0sf27d3sic299fd00c6njlnrm8sf6")))

(define-public crate-wasmtime-wasi-crypto-1.0.1 (c (n "wasmtime-wasi-crypto") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^1.0.1") (k 0)) (d (n "wiggle") (r "=1.0.1") (d #t) (k 0)))) (h "1a1snzrq4v6mk45x3pz30n53pc1zvddpicyzkhjv050iqv6j60hg")))

(define-public crate-wasmtime-wasi-crypto-2.0.0 (c (n "wasmtime-wasi-crypto") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^2.0.0") (k 0)) (d (n "wiggle") (r "=2.0.0") (f (quote ("wasmtime_integration"))) (k 0)))) (h "0v5nl1xf7920hzda8xd46axml0jbiizfw76sr8hz7m1wk5ajcqnf")))

(define-public crate-wasmtime-wasi-crypto-2.0.1 (c (n "wasmtime-wasi-crypto") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^2.0.1") (k 0)) (d (n "wiggle") (r "=2.0.1") (f (quote ("wasmtime_integration"))) (k 0)))) (h "1al2yw0mc0p12gnaqpsbn7wvljbn9r73csm0cq1jra25f3m77w34")))

(define-public crate-wasmtime-wasi-crypto-2.0.2 (c (n "wasmtime-wasi-crypto") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^2.0.2") (k 0)) (d (n "wiggle") (r "=2.0.2") (f (quote ("wasmtime_integration"))) (k 0)))) (h "14n91pcbbdybw3wsvl30g0wjy6bgyvmrw7archhzsk9809zzxsd2")))

(define-public crate-wasmtime-wasi-crypto-1.0.2 (c (n "wasmtime-wasi-crypto") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^1.0.2") (k 0)) (d (n "wiggle") (r "=1.0.2") (d #t) (k 0)))) (h "1gi8nw2x5qnknz5kiib71ijsi78d1w1nyy1102lz1ci5jf6v07bm")))

(define-public crate-wasmtime-wasi-crypto-3.0.0 (c (n "wasmtime-wasi-crypto") (v "3.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^3.0.0") (k 0)) (d (n "wiggle") (r "=3.0.0") (k 0)))) (h "12w6db8yzmgnalldvj8asp2b7kyghw3g42iacg1dm9avad0lmbps")))

(define-public crate-wasmtime-wasi-crypto-3.0.1 (c (n "wasmtime-wasi-crypto") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^3.0.1") (k 0)) (d (n "wiggle") (r "=3.0.1") (k 0)))) (h "1nb3dryav9x6knqzj7whm1g1kzghl6hnyvff0is7ian7h90y8saf")))

(define-public crate-wasmtime-wasi-crypto-4.0.0 (c (n "wasmtime-wasi-crypto") (v "4.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^4.0.0") (k 0)) (d (n "wiggle") (r "=4.0.0") (k 0)))) (h "03p1w5wvvgzck8s3c0fi5q2wk767z391h7ca23ij0q5r0h2drq9n")))

(define-public crate-wasmtime-wasi-crypto-5.0.0 (c (n "wasmtime-wasi-crypto") (v "5.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^5.0.0") (k 0)) (d (n "wiggle") (r "=5.0.0") (k 0)))) (h "16iklfpigcfkvka7idqx1b8xi6l3q85kgcfsr01lky70n57rbcja")))

(define-public crate-wasmtime-wasi-crypto-6.0.0 (c (n "wasmtime-wasi-crypto") (v "6.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^6.0.0") (k 0)) (d (n "wiggle") (r "=6.0.0") (k 0)))) (h "1gh8q11glwy4v2aryv0cmhimca2cai39cv0rqj9kbxcr55njqj8c")))

(define-public crate-wasmtime-wasi-crypto-5.0.1 (c (n "wasmtime-wasi-crypto") (v "5.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^5.0.1") (k 0)) (d (n "wiggle") (r "=5.0.1") (k 0)))) (h "0d51q3p9c3sf76pnisxgckk03nlgc62rxxvqln34gxyvbnv4l2rg")))

(define-public crate-wasmtime-wasi-crypto-4.0.1 (c (n "wasmtime-wasi-crypto") (v "4.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^4.0.1") (k 0)) (d (n "wiggle") (r "=4.0.1") (k 0)))) (h "14bwwnfmc2mnv8hdqn05xbs04y46igfjjgp39a7vrxragv3bhn2c")))

(define-public crate-wasmtime-wasi-crypto-6.0.1 (c (n "wasmtime-wasi-crypto") (v "6.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^6.0.1") (k 0)) (d (n "wiggle") (r "=6.0.1") (k 0)))) (h "128zj2bc0sbk16c607lcvrg3jx53fv57s99ynxldjskhaljk3imn")))

(define-public crate-wasmtime-wasi-crypto-7.0.0 (c (n "wasmtime-wasi-crypto") (v "7.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^7.0.0") (k 0)) (d (n "wiggle") (r "=7.0.0") (k 0)))) (h "0gva3765y4342qsri1jr50h9xgfxzxs0nfd4pm9a6s64ybshzrn4")))

(define-public crate-wasmtime-wasi-crypto-8.0.0 (c (n "wasmtime-wasi-crypto") (v "8.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^8.0.0") (k 0)) (d (n "wiggle") (r "=8.0.0") (k 0)))) (h "1lqyqvk7xm33f7f0gcc2728sciz1xpz0xj9ak7sdpbapmf7nzn7d")))

(define-public crate-wasmtime-wasi-crypto-6.0.2 (c (n "wasmtime-wasi-crypto") (v "6.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^6.0.2") (k 0)) (d (n "wiggle") (r "=6.0.2") (k 0)))) (h "0srf1360n60f7wd68zxvdzx82f1fzg8dvf75kp02d474w2zvbz5f")))

(define-public crate-wasmtime-wasi-crypto-8.0.1 (c (n "wasmtime-wasi-crypto") (v "8.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^8.0.1") (k 0)) (d (n "wiggle") (r "=8.0.1") (k 0)))) (h "02bljak6fpkjn0pnbl5245lf9nvnmb813cc2avr4p2f7dyhjpcvb")))

(define-public crate-wasmtime-wasi-crypto-7.0.1 (c (n "wasmtime-wasi-crypto") (v "7.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^7.0.1") (k 0)) (d (n "wiggle") (r "=7.0.1") (k 0)))) (h "04va25q44q8vdmsil4nd74xqa8ki5f2z1j56qcnawj96hxv5c7z6")))

(define-public crate-wasmtime-wasi-crypto-9.0.0 (c (n "wasmtime-wasi-crypto") (v "9.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^9.0.0") (k 0)) (d (n "wiggle") (r "=9.0.0") (k 0)))) (h "1mq6nhxwcwg4d10mqa8k7yd9z1nw5cdvdqq6fy2n6ji4hcnliav4")))

(define-public crate-wasmtime-wasi-crypto-9.0.1 (c (n "wasmtime-wasi-crypto") (v "9.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^9.0.1") (k 0)) (d (n "wiggle") (r "=9.0.1") (k 0)))) (h "11kfh3dc321w1mc7axpd385gd2lyvh85xxify9vsmdzv063jndhc")))

(define-public crate-wasmtime-wasi-crypto-9.0.2 (c (n "wasmtime-wasi-crypto") (v "9.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^9.0.2") (k 0)) (d (n "wiggle") (r "=9.0.2") (k 0)))) (h "0d2izvhfn0fmy51yjbx75nqkdbvvcz5sdw3mlhl9cw19xh3r9z29")))

(define-public crate-wasmtime-wasi-crypto-9.0.3 (c (n "wasmtime-wasi-crypto") (v "9.0.3") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^9.0.3") (k 0)) (d (n "wiggle") (r "=9.0.3") (k 0)))) (h "1hskvx6q69jd8lr8jcni2ln6c21mafnfw4mqajsxfxanbab3xb2q")))

(define-public crate-wasmtime-wasi-crypto-9.0.4 (c (n "wasmtime-wasi-crypto") (v "9.0.4") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^9.0.4") (k 0)) (d (n "wiggle") (r "=9.0.4") (k 0)))) (h "1x9fw4893i72zranjd7bjd33l3sda5hnj8xdxhxw848ihc7w0bcs")))

(define-public crate-wasmtime-wasi-crypto-10.0.0 (c (n "wasmtime-wasi-crypto") (v "10.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^10.0.0") (k 0)) (d (n "wiggle") (r "=10.0.0") (k 0)))) (h "1wdk840a475azyqvhjrsa8acdv54j0ch7q3zdcf1c2pqqmzg9lds")))

(define-public crate-wasmtime-wasi-crypto-10.0.1 (c (n "wasmtime-wasi-crypto") (v "10.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^10.0.1") (k 0)) (d (n "wiggle") (r "=10.0.1") (k 0)))) (h "1ik0x46zjxk6cpg7fa99nxmnssa8hldz1m0787979n99m57nc3l1")))

(define-public crate-wasmtime-wasi-crypto-11.0.0 (c (n "wasmtime-wasi-crypto") (v "11.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^11.0.0") (k 0)) (d (n "wiggle") (r "=11.0.0") (k 0)))) (h "10jy6sw0zib4cb3y5g6asg0yfwjbbf9a79y3mjg14nwm7n20y2fq")))

(define-public crate-wasmtime-wasi-crypto-11.0.1 (c (n "wasmtime-wasi-crypto") (v "11.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^11.0.1") (k 0)) (d (n "wiggle") (r "=11.0.1") (k 0)))) (h "1cikjwcsdwvxi7gp88igkgradfzx6dzksdhsfp2dsxn622qyzbzi")))

(define-public crate-wasmtime-wasi-crypto-12.0.0 (c (n "wasmtime-wasi-crypto") (v "12.0.0") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^12.0.0") (k 0)) (d (n "wiggle") (r "=12.0.0") (k 0)))) (h "1rharrrzrmcqvg9h34f28lhvhknjyrwcp9d5yi7wj3qgq9y1cxrz")))

(define-public crate-wasmtime-wasi-crypto-12.0.1 (c (n "wasmtime-wasi-crypto") (v "12.0.1") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^12.0.1") (k 0)) (d (n "wiggle") (r "=12.0.1") (k 0)))) (h "1ags31vamkqmrrlzd9s9rqmgbish2qbsr6s8zl4ydgjbz6i38rqd")))

(define-public crate-wasmtime-wasi-crypto-10.0.2 (c (n "wasmtime-wasi-crypto") (v "10.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^10.0.2") (k 0)) (d (n "wiggle") (r "=10.0.2") (k 0)))) (h "0xj0r9ali018z3600lcj458hmx1sk5zvgffv15gfi6fgpyafq616")))

(define-public crate-wasmtime-wasi-crypto-11.0.2 (c (n "wasmtime-wasi-crypto") (v "11.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^11.0.2") (k 0)) (d (n "wiggle") (r "=11.0.2") (k 0)))) (h "1d05vb3yr0w3l5nfx2gn02ngan4g6vn7z9anpykg1p7ign8mlg6p")))

(define-public crate-wasmtime-wasi-crypto-12.0.2 (c (n "wasmtime-wasi-crypto") (v "12.0.2") (d (list (d (n "anyhow") (r "^1.0.22") (d #t) (k 0)) (d (n "wasi-crypto") (r "^0.1.5") (d #t) (k 0)) (d (n "wasmtime") (r "^12.0.2") (k 0)) (d (n "wiggle") (r "=12.0.2") (k 0)))) (h "09lsxsjq80a18imrrrmkqpfpb6m67v8r128dycmv2gxsa0qx00h3")))

