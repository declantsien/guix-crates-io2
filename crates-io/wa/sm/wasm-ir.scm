(define-module (crates-io wa sm wasm-ir) #:use-module (crates-io))

(define-public crate-wasm-ir-0.1.0 (c (n "wasm-ir") (v "0.1.0") (h "0a0sdlsmgdck07m4b83alafs9h8l8xzym529anjkh16ik93xa93k") (y #t)))

(define-public crate-wasm-ir-0.1.1 (c (n "wasm-ir") (v "0.1.1") (h "1laxqmsg2l2nwvdp7vz395rljcvrk88yvdcy5lp2a32n55i8w8vm") (y #t)))

(define-public crate-wasm-ir-0.1.2 (c (n "wasm-ir") (v "0.1.2") (h "131a5qjxpnic6cnk0fxzbcy6k3injzfd3srkn3pp1lfjzm4jrnq3") (y #t)))

(define-public crate-wasm-ir-0.1.3 (c (n "wasm-ir") (v "0.1.3") (h "16ismjdf0clsa17x57hq6shnmzjbl3p2ibkhkf37gqqy0jd298kz") (y #t)))

(define-public crate-wasm-ir-0.1.4 (c (n "wasm-ir") (v "0.1.4") (h "15mdv8cmhqjda0nlc8mlnpapzikiybjmacrfzj6k5g4w4pvdqkyn") (y #t)))

(define-public crate-wasm-ir-0.1.5 (c (n "wasm-ir") (v "0.1.5") (h "12z4qdlnjx1ygvn9y2g9p8ci7c3dvwgmafwayfz6spvlxgyzllxz") (y #t)))

(define-public crate-wasm-ir-0.1.6 (c (n "wasm-ir") (v "0.1.6") (d (list (d (n "cap-std") (r "^0.24.1") (d #t) (k 2)) (d (n "wasmtime") (r "^0.34.1") (d #t) (k 2)) (d (n "wasmtime-wasi") (r "^0.34.1") (d #t) (k 2)))) (h "169pqy4g93rmjlz3rxh8q5vjyv1fskhj0bpnj6khb2kh50ini2gk") (y #t)))

(define-public crate-wasm-ir-0.1.7 (c (n "wasm-ir") (v "0.1.7") (d (list (d (n "cap-std") (r "^0.24.1") (d #t) (k 2)) (d (n "wasmtime") (r "^0.34.1") (d #t) (k 2)) (d (n "wasmtime-wasi") (r "^0.34.1") (d #t) (k 2)))) (h "0m6jvnz0jq8dm7zvfnr27q1l5nna3kzcxlm4kgih5z7brgkkfrkx") (y #t)))

(define-public crate-wasm-ir-0.1.8 (c (n "wasm-ir") (v "0.1.8") (d (list (d (n "cap-std") (r "^0.24.1") (d #t) (k 2)) (d (n "wasmtime") (r "^0.34.1") (d #t) (k 2)) (d (n "wasmtime-wasi") (r "^0.34.1") (d #t) (k 2)))) (h "16f3hcblih6yr5pd1gpfx5iyh5h8mrabqhskbkzflm5sbb2kd35f") (y #t)))

(define-public crate-wasm-ir-0.1.9 (c (n "wasm-ir") (v "0.1.9") (d (list (d (n "cap-std") (r "^0.24.1") (d #t) (k 2)) (d (n "wasmtime") (r "^0.34.1") (d #t) (k 2)) (d (n "wasmtime-wasi") (r "^0.34.1") (d #t) (k 2)))) (h "0grcvi02ndnhmrgdhc3zgymq2374ksdvrvnmml8s7hnbkcd0pfg9") (y #t)))

