(define-module (crates-io wa sm wasmdev_core) #:use-module (crates-io))

(define-public crate-wasmdev_core-0.1.7 (c (n "wasmdev_core") (v "0.1.7") (d (list (d (n "minify-js") (r "^0.5.6") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "wasm-bindgen-cli-support") (r "^0.2.87") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "xshell") (r "^0.2.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)))) (h "1gavg724a5wsgqijrg6q66nnn3nd5dq356kc1phh0625b6v6bzm8") (f (quote (("nightly"))))))

