(define-module (crates-io wa sm wasmparser-dump) #:use-module (crates-io))

(define-public crate-wasmparser-dump-0.1.0 (c (n "wasmparser-dump") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.82.0") (d #t) (k 0)))) (h "1b92sch3rw136kmnqwj91kmynqr1lfbrhzqw1q817na8pbkpszgm")))

(define-public crate-wasmparser-dump-0.1.1 (c (n "wasmparser-dump") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.83.0") (d #t) (k 0)))) (h "07xp2bijz1py0lcx71w982hdf98r3bnykd40hy6r3jpzhp0mfnh8")))

(define-public crate-wasmparser-dump-0.1.2 (c (n "wasmparser-dump") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.84.0") (d #t) (k 0)))) (h "1drvlxlirvbz0qy4awbdlgbjj52jfrbd3i0y527d6m8q17isg2yz")))

(define-public crate-wasmparser-dump-0.1.3 (c (n "wasmparser-dump") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.85.0") (d #t) (k 0)))) (h "07c1a4iknij7nl6vddrqj8y3880w2q2cq247ml0168miqmbcl2ac")))

(define-public crate-wasmparser-dump-0.1.4 (c (n "wasmparser-dump") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.86.0") (d #t) (k 0)))) (h "1yh82aj32a2h4wljyaqx3p98q5aa9z0x1m3aklnp6g6bbs2rghj5")))

(define-public crate-wasmparser-dump-0.1.5 (c (n "wasmparser-dump") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.87.0") (d #t) (k 0)))) (h "1dfs19sdsk84gx6jgh3n4qr5s4gk15qn21p95n3lxkzcxl2ki8f1")))

(define-public crate-wasmparser-dump-0.1.6 (c (n "wasmparser-dump") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.88.0") (d #t) (k 0)))) (h "13qg9vr9cl4v0bgi21i5icn2l8y3msb51bsmfy75dgcby8vldvwb")))

(define-public crate-wasmparser-dump-0.1.7 (c (n "wasmparser-dump") (v "0.1.7") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.89.0") (d #t) (k 0)))) (h "0plrhav1wpin140rbgwdzdxf9b71afhzczjkrnlyl0fnsjgfcgc8")))

(define-public crate-wasmparser-dump-0.1.8 (c (n "wasmparser-dump") (v "0.1.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.91.0") (d #t) (k 0)))) (h "1jgwqjkbgp84rj000154lw3g6mpfv1fgvn3ml6b7kbkq10qd6ics")))

(define-public crate-wasmparser-dump-0.1.9 (c (n "wasmparser-dump") (v "0.1.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wasmparser") (r "^0.92.0") (d #t) (k 0)))) (h "0dld6g83vkc7xqzj8d1q77c9mzjc0dda4rmriarjx6m63crayv5q")))

(define-public crate-wasmparser-dump-0.1.10 (c (n "wasmparser-dump") (v "0.1.10") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "wasmparser") (r "^0.93.0") (d #t) (k 0)))) (h "15jqnzxcfq6jzy6k6w4jr97lcfc56cfid474d535slcvic7y7wiy")))

(define-public crate-wasmparser-dump-0.1.11 (c (n "wasmparser-dump") (v "0.1.11") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "wasmparser") (r "^0.94.0") (d #t) (k 0)))) (h "02ngkghp5kk3gima6xnxvshml0llzw6avk9z64625vsnk6ip4id3")))

(define-public crate-wasmparser-dump-0.1.12 (c (n "wasmparser-dump") (v "0.1.12") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "wasmparser") (r "^0.95.0") (d #t) (k 0)))) (h "1hng5579yd4d1w5mslzxxcnin15ffl8dvn1hafssnvl5xvx5j8hj")))

(define-public crate-wasmparser-dump-0.1.13 (c (n "wasmparser-dump") (v "0.1.13") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "wasmparser") (r "^0.97.0") (d #t) (k 0)))) (h "1lfwb7vw2hmjagzalgnjsv2f9id2g6i418s31h1pw1b7x65nxr19")))

(define-public crate-wasmparser-dump-0.1.14 (c (n "wasmparser-dump") (v "0.1.14") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "wasmparser") (r "^0.98.0") (d #t) (k 0)))) (h "1r6q6gj3234kidz2v19n84x0yrcfy6drpk02ifzz00njcij90ik0") (y #t)))

(define-public crate-wasmparser-dump-0.1.15 (c (n "wasmparser-dump") (v "0.1.15") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "wasmparser") (r "^0.98.1") (d #t) (k 0)))) (h "062hjas7ywb29s5z3lzxqmnd6iihb983fsksf5jy8rzhn7h274b5")))

(define-public crate-wasmparser-dump-0.1.16 (c (n "wasmparser-dump") (v "0.1.16") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "wasmparser") (r "^0.99.0") (d #t) (k 0)))) (h "1m118dd2j3546ndk52vdkpl1hc3f65vj50jn0bfsvyy38f47ajsl")))

