(define-module (crates-io wa sm wasmedge_storage_interface) #:use-module (crates-io))

(define-public crate-wasmedge_storage_interface-0.2.0 (c (n "wasmedge_storage_interface") (v "0.2.0") (d (list (d (n "bincode") (r "^1.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.105") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialize_deserialize_u8_i32") (r "^0.1") (d #t) (k 0)))) (h "1z4nqqr4x1mj5zh4001kv0wkqkk750h50pifz4lp73ahc5iyizv6")))

