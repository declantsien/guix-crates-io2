(define-module (crates-io wa sm wasmflow-codec) #:use-module (crates-io))

(define-public crate-wasmflow-codec-0.10.0-beta.0 (c (n "wasmflow-codec") (v "0.10.0-beta.0") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15hj1a80q0y12v3225j9n9ihax7770ivi4hdqxydvymdm0vdy6y5")))

(define-public crate-wasmflow-codec-0.10.0-beta.4 (c (n "wasmflow-codec") (v "0.10.0-beta.4") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "196lw4nsgd02514qd27v2ny4hgpsm7pbafdwvdjc8zfxxjmfy9n1")))

(define-public crate-wasmflow-codec-0.10.0 (c (n "wasmflow-codec") (v "0.10.0") (d (list (d (n "rmp-serde") (r "^1.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-value") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0jdlvh993ignr88hfxcwgxvzw2jahmznpncpm33mmxz74z1ga3q1")))

