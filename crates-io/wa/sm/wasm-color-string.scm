(define-module (crates-io wa sm wasm-color-string) #:use-module (crates-io))

(define-public crate-wasm-color-string-0.1.0 (c (n "wasm-color-string") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (d #t) (k 0)))) (h "0jc6ys3fqz6l9gdb4hdyjyzya4frq7pwli692zvixm2gx4k78mbw")))

