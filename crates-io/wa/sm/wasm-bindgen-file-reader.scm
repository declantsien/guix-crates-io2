(define-module (crates-io wa sm wasm-bindgen-file-reader) #:use-module (crates-io))

(define-public crate-wasm-bindgen-file-reader-1.0.0 (c (n "wasm-bindgen-file-reader") (v "1.0.0") (d (list (d (n "js-sys") (r "^0.3") (d #t) (k 0)) (d (n "web-sys") (r "^0.3") (f (quote ("File" "FileReaderSync"))) (d #t) (k 0)))) (h "0m0gpijyy02wxy5hpbirgjsjmji5canvji193fm81npgy9san1s6")))

