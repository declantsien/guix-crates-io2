(define-module (crates-io wa sm wasmtime-rust-macro) #:use-module (crates-io))

(define-public crate-wasmtime-rust-macro-0.2.0 (c (n "wasmtime-rust-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01z33j783k1i4inaa4wb8xp79h4j526pymp563zjc7mmrff08f1q")))

(define-public crate-wasmtime-rust-macro-0.3.0 (c (n "wasmtime-rust-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0jb955ghqj06mx9bp092f4019nils72vbjm6fxfdyn4x4irclqyk")))

(define-public crate-wasmtime-rust-macro-0.7.0 (c (n "wasmtime-rust-macro") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h73pg1b86x382z5a4syw3b965bq7l5ihcr9aq43bzyzcnvv7a8z")))

(define-public crate-wasmtime-rust-macro-0.8.0 (c (n "wasmtime-rust-macro") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0m3k6l7yjcx37d4by33sib1f6c6fx7q2mnqh4g5vknqj39xalasd")))

(define-public crate-wasmtime-rust-macro-0.9.0 (c (n "wasmtime-rust-macro") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1bqkj93rmaj68b743pf31sl1bhamicg9ryj98d89qqbs7m3jqmhx")))

(define-public crate-wasmtime-rust-macro-0.10.0 (c (n "wasmtime-rust-macro") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02g5slvj2klr02d4zydzsslrbp79mpxnxrwrcvv2y4fi31h3lys0")))

(define-public crate-wasmtime-rust-macro-0.11.0 (c (n "wasmtime-rust-macro") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1kbn6v7j1fqznvls7hzsn76apv7n18g9d5330hp1fa8jpk61xa6z")))

(define-public crate-wasmtime-rust-macro-0.12.0 (c (n "wasmtime-rust-macro") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hx43zl408ilbgz5r22a2p94rbih7xx03pa8s1pr9d4fbrlxmjh7")))

(define-public crate-wasmtime-rust-macro-0.13.0 (c (n "wasmtime-rust-macro") (v "0.13.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0d1ka34fw6l0265v33w831bxlp24xw7kzrwh53jh9qlp43cmxbmf")))

(define-public crate-wasmtime-rust-macro-0.15.0 (c (n "wasmtime-rust-macro") (v "0.15.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1gaixhh0x320b91ldrn5qlaci98g73j3qwdl9izmnd212s0pr3as")))

(define-public crate-wasmtime-rust-macro-0.16.0 (c (n "wasmtime-rust-macro") (v "0.16.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nk44dkq2n7kb4is73mx42a8wazl1dasmg4192rjd7v9fqkr70zx")))

(define-public crate-wasmtime-rust-macro-0.17.0 (c (n "wasmtime-rust-macro") (v "0.17.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1yhk6w1r5r3af4aikmypg57x6df4jnklwc0pjkjjlv9cn0avcsaj")))

(define-public crate-wasmtime-rust-macro-0.18.0 (c (n "wasmtime-rust-macro") (v "0.18.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1a0xh7wrq8s6f53j74kvkh885f7413qxigz42v7llad3793w3hpb")))

(define-public crate-wasmtime-rust-macro-0.19.0 (c (n "wasmtime-rust-macro") (v "0.19.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1lr99fyl1jdya9djbm7qqjhhnm928zdk53fp9xa8rzi3jhilncgd")))

(define-public crate-wasmtime-rust-macro-0.20.0 (c (n "wasmtime-rust-macro") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1wg59ylvzx0qdp4fd1dw8c9xyhlq8czy18kc5qlbmxhbr1ak5v8p")))

(define-public crate-wasmtime-rust-macro-0.21.0 (c (n "wasmtime-rust-macro") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1d8ny2q33y6zalgljpl54025riyynbbcyh6jirkay5vqcrc4gfkw")))

(define-public crate-wasmtime-rust-macro-0.22.0 (c (n "wasmtime-rust-macro") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0mwkd23d7p4c3055bqzdwfz8x8ah3jw8nda689w1nn72jvd25w08")))

(define-public crate-wasmtime-rust-macro-0.23.0 (c (n "wasmtime-rust-macro") (v "0.23.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0w48lb7cx6dqx7ixb7a9dya60rf6g0xalax67kn4jp2xlijr01v6")))

(define-public crate-wasmtime-rust-macro-0.24.0 (c (n "wasmtime-rust-macro") (v "0.24.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kd093ryham635q4mgjw2whkxbdi1sc9pa0bkgnyr9nxj7aq48cv")))

(define-public crate-wasmtime-rust-macro-0.25.0 (c (n "wasmtime-rust-macro") (v "0.25.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "01d2ckngni53xk60vhw383lhqxyhsp624sjd639f3ljic8h07cb9")))

(define-public crate-wasmtime-rust-macro-0.26.0 (c (n "wasmtime-rust-macro") (v "0.26.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1c84lhjf74rbigka4gq2nb5rlwxwgss08gy3pv1hd60ryz9dn1xi")))

(define-public crate-wasmtime-rust-macro-0.27.0 (c (n "wasmtime-rust-macro") (v "0.27.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qvjmd8rhiayy97j5xxxdi6baysa9siwpcvm2gpyiyk2g9cj0frk")))

(define-public crate-wasmtime-rust-macro-0.26.1 (c (n "wasmtime-rust-macro") (v "0.26.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08qvjrj50zbh6nywkizz2g0p8vqqfnh7qq2qfgxbic2083c6gwqm")))

