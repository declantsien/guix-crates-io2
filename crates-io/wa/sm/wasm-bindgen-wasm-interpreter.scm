(define-module (crates-io wa sm wasm-bindgen-wasm-interpreter) #:use-module (crates-io))

(define-public crate-wasm-bindgen-wasm-interpreter-0.0.0 (c (n "wasm-bindgen-wasm-interpreter") (v "0.0.0") (d (list (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "10qv8gr7jrxq9z1jd4yg49a6mk0lhmcvi37q8bdlz4wrchza1g4a")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.18 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.18") (d (list (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0l10qw820ab6az6kacl6q4j74j4ar517y3r8pr08l7r7hf0y5i6y")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.19 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.19") (d (list (d (n "parity-wasm") (r "^0.31") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1w96i1w1c4dk0d8lzirx9k8lg3d2f2hs11p133rqindyp7mzcg5v")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.20 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.20") (d (list (d (n "parity-wasm") (r "^0.32") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0wzaq1qdm3fw729vhfh9c2c7dwl7nmq5h0fwjplif73islvlgj1r")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.21 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.21") (d (list (d (n "parity-wasm") (r "^0.32") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0pyfnfiphglb88rpdk6d1dxgvlc71scs8mg6ca2frn55g69z009i")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.22 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.22") (d (list (d (n "parity-wasm") (r "^0.32") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0kqjcxwgn6058d6k9ilxx1fhvjvb5xx2wgxrfhma5zmbjhia6vq5")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.23 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.23") (d (list (d (n "parity-wasm") (r "^0.32") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "03klw51fgkf317gqxavc7pnf6gbis7gxsfzi2prxyxam6ffm7z8w")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.24 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.24") (d (list (d (n "parity-wasm") (r "^0.32") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "13b30fcv2b7h74xncxl24a12kw30b4nc5f0myawz5y3xd1aqc0h9")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.25 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.25") (d (list (d (n "parity-wasm") (r "^0.34") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0f41li6bdb3lgix9vr6zvs0p3zb4a3vh01mwz03s9p1z4vqplzdh")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.26 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.26") (d (list (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1xpbrs1ddfczs70ry1dazcxyfyc3iimjb5s2w9j4d0v61w3nixvl")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.27 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.27") (d (list (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1yw0hy2r1i52636k51g43db31lcrpbj9d0y0p6w3jmf2ad9cj9qf")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.28 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.28") (d (list (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0h25gn39xbx9h297jya8i24xx24s27pkdxdcv0iwp9knvc2970jg")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.29 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.29") (d (list (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1spzzivpginbig3qzp5mc0w6131462rn76hkmdybx48xisqq8fsq")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.30 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.30") (d (list (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "12ksj3dbkzjnyf71mp90wsir1km70pgqsbn286ixppz5xj2f7x3i")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.31 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.31") (d (list (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "03ngsr8g2zpyipk56mpzx3fih87dmhxpa4zz0j6wayzv5znk60h6")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.32 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.32") (d (list (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "126g57zirdnps3qvhjlnbsg7lvamixjnh97jbidsbd8ziq87972i")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.33 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.33") (d (list (d (n "parity-wasm") (r "^0.35") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1f78paalz94c1c0y480m0989jaffxdi9vwa3b5a7ilziqgrsxjxz")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.34 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.34") (d (list (d (n "parity-wasm") (r "^0.36") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0mbwrpv3hprbkzcad1q9ry37hzqxnyj23579bwnk823z1hin4k8v")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.35 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.35") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.1") (d #t) (k 0)))) (h "1vshi33ymmiw1rgh5ilhv65w3ghfdn4zp0rvppx6zw1hq7w6hgdn")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.36 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.36") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.1") (d #t) (k 0)))) (h "1sckvi6n532pnv8xvh7xq4zb73n70f8pfc752v4adpnl7p23w5r2")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.37 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.37") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.2") (d #t) (k 0)))) (h "1q0acxj1lqpyms1xnqhbrg8b6j58g3kgrils5qjr20y36x2scan1")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.38 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.38") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.4") (d #t) (k 0)))) (h "0c264ipmwzpyd3pq0qvi9pgcklcr2pjsikc3k23f5q6n3lf629pq")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.39 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.39") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.5") (d #t) (k 0)))) (h "0l2fck0d4jfn32zkzqi5qv8kbj7dyjdcdsxw7wypi42ac1fb807k")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.40 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.40") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.5") (d #t) (k 0)))) (h "01rza66x42p5cfmhwyfml14248c2qlxf0sbyaryb89g41ihaqjif")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.41 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.41") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.5") (d #t) (k 0)))) (h "05sf2kj0bzkpcpmh28blg4rlpshwi8qgsa3yhyp03zz6x9zy9dhr")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.42 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.42") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.5") (d #t) (k 0)))) (h "038djchd1rl07awpk6428j8flwd3dnc8p20rd9y78qb0d5gs8k4s")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.43 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.43") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.5") (d #t) (k 0)))) (h "0y5d1sfrisq88yph89b12yjhn844yrqmvxl1gpz9rbsq4a1h6799")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.44 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.44") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.6") (d #t) (k 0)))) (h "0h30rv27w5cs0bd0i8n5xnb964k2wnydazsmz9c4j53v73i460vr")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.45 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.45") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.7.0") (d #t) (k 0)))) (h "1bh8zihivw2i87nb5hdi9pzvycglv4bgpzn181mfl49j7l6clxq7")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.46 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.46") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.8.0") (d #t) (k 0)))) (h "1p3y8v036kvpsrfx7qjja5f2hxddhcx1qqp9q6i7ixzbvkym2ly7")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.47 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.47") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.8.0") (d #t) (k 0)))) (h "0c970k0cqgjqyl03nlpca5kw77jyl8l9h4pd1k9qicbihdmn5dzb")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.48 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.48") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.8.0") (d #t) (k 0)))) (h "1rnz1fwq31aw7ilpgp8fc3zn5rv1n0nmwp2zwy2iq46a63q77z3w")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.49 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.49") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.11.0") (d #t) (k 0)))) (h "0shg9c91xw73nlz33c0f5hykdab2wrb1cyiiaqswjh492z7y4rk9")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.50 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.50") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.11.0") (d #t) (k 0)))) (h "1dh0dijpkr6kfxglvy9k3a80cjmn6x6i8zq5jkk0mhpck839ad5g")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.51 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.51") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.12.0") (d #t) (k 0)))) (h "0nvvimi2sfj8i74bpdr6wdsccqvi776qs76jvaav3z3aza2ncybq")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.52 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.52") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.12.0") (d #t) (k 0)))) (h "0miyi6f5sqlznmydimmrm92f4p5i41j11a9l4wk147zr92xr3qik")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.53 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.53") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.12.0") (d #t) (k 0)))) (h "0s92ryinf96a07sfbznyn1h66vikmyn76xvh8nv20v4i2n85gxxg")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.54 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.54") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.13.0") (d #t) (k 0)))) (h "12dccvwmav8ij7yyr51cnz7vpldj6v8i5z8wvyvwai79p2b8kcsd")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.55 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.55") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.13.0") (d #t) (k 0)))) (h "0h7qyg1yxfzjzkl2nfcj3hw2634vs4aglv11lmkapandpr373y4v")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.56 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.56") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "12gpk91y4s3acnalrlh9dl3lw2q9210z4rixqrbsfbfkggr86d8g")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.57 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.57") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0affsp89h563jaldi7balfm7vnhfh948dli4yf7j68zalwlrxlhi")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.58 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.58") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1i88yw28f0rcp06lwkffny2aizr4cr73qmznp7qv3wipl5qdz1b0")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.59 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.59") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1csr7mlal68svcd5s2f4h7cb69mhyfaj14yzmxnmyhhlvkw1lrnj")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.60 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.60") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0bkwwpqnl4aya20x08kj4akzag9slkwb8jgipfnynm9f0vgibk42")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.61 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.61") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1kfhjfn4lsnki1h3wa1w80c9scgql4d0acv4pmy6ds4b2i5ng2jj")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.62 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.62") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.14.0") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1j1fizmn4d1241y0s8zm1jzv1bgyf6356kx5hq60m1l2dfwmkjqs")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.63 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.63") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.17.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.63") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1d0aml236pny5njnd0nm1y8cdhicscy2084m72nrprxsm5xyxql2")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.64 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.64") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.17.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.64") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1alsvy5dd3q9xpbzbl6svcqqr2zsnrbw24bndr88103j7c9mfi5r")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.65 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.65") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.65") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1h5z0m7z8y6d4zmr5c51sgrya9gbxd5k9h18kq50ckha5skr2k35")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.66 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.66") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.66") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0k37ma5k6m5lrx8bf9lngdnv46njlr2v1nr8rwk387hl23s1nqlq")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.67 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.67") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.67") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0fzh1dcdmlg1dp6b4ig4x5f2iy8an11h5k6nmcrrp2qm6rr1yrb0")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.68 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.68") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.68") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "06sbg1684j1jgwgrjyi4cnlwbi5a8fmbna56ckqvlpdhl8w84zp4")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.69 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.69") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.69") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "125sc0jpw40m8gg155w043mdmlq9x8yzfz4wcykkm0q79j61vnf9")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.70 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.70") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.70") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1g5cls7rfkagya6sfdxjf9gfg1y4gvhm403pj23rgdll55d7fz4m")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.71 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.71") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.71") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0nd5377ffvxvk44c895x351nzwfy975af4yvk3b9j4d8v043ckd0")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.72 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.72") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.72") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1ism6y5m3l74rp55g250q9hx0hsjhmwzd2x5hspnyhlj56zrwywz")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.73 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.73") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.18.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.73") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0cvvcc4xf8ipbxw9hsm3rk4w9c90sigp5204ysk7c0nxmfrihdd0")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.74 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.74") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.74") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "15hanxcg72qbr7l79ly5cjvirgkzn7bk7a6sgfwivmsbrq5h2xk0")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.75 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.75") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.75") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1qpg64fcmvxk492l80bm4hjqmxqhrwclvibimv7sf0rl4yajmw22")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.76 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.76") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.76") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "08ddvm3pry7y19i5bgfknkcd079p2jk9rka86hdvaqyg634sjpd6")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.77 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.77") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.77") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0j0a0ad7ayh99gfnr844qswz943c6g9njdrxycd3hyx56ph4yizj")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.78 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.78") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.78") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0zqv84656g6a0ldx8ifg6cbgv48r1qy66sk8b1chnc29kn464r37")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.79 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.79") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.79") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "19vpwhs0qajckglx9m34m2xrf3zlmz5x5lc0jl1cwkrk1306fw1y")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.80 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.80") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.80") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "094x8j0sfn2xp0pbcsh7yxrf1iba51vi4cjmmj87hlfb6h5f1qq3")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.81 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.81") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.81") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "11lbzg4pb1p3padnqwabb248rml33zpm7f5v684q8dqiyyvry70b")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.82 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.82") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.82") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0ha194flghzg7dfk2xnxjfcsfclqx780jx3gj1daqf7shk1nywww")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.83 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.83") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.83") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "14fdd7i6kpyacq13hsa2xiaazswfl2kjf791q6vqjs287dvnrfwg")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.84 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.84") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.84") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0a0iyc0jdk9pnvgqvghn7i2vi1d1vai348i86rs10fifjr2dz5gn")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.85 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.85") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.85") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "11i0q0fk08la0mkys6rcpxzkjrn0wq8iyxcchs1wwy2dcxng1i9m")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.86 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.86") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.86") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1lz0g36pfa1k0p939vxs9ry2q6fxh6rw4y1d8blz683v7c3015g6")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.87 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.87") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.87") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "07mamxmv4441rxmidl9r00f18x4af82p5wv68vabb2kk41i7jp3w") (r "1.56")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.88 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.88") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.19.0") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.88") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "03wqq0a6f91pyjap9dmrv6i1s2kdhw5dkrd7c0qlwyrpi2mi0mba") (r "1.57")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.89 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.89") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.20.2") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.89") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0lgrvbhqbkdbc87hc9hy42ndx4r90n3fq235svk2420yvwwr19x8") (r "1.57")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.90 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.90") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.20.2") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.90") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "14yqhmaw48qbsnw824xwzc3z4m4225719hap80yhd8vj2wkjla64") (r "1.57")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.91 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.91") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.20.2") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.91") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "1hbkx4cnc8ikxknyv49zgfiidjfap0c78090gy4by7bhb8cl0ab8") (r "1.57")))

(define-public crate-wasm-bindgen-wasm-interpreter-0.2.92 (c (n "wasm-bindgen-wasm-interpreter") (v "0.2.92") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "walrus") (r "^0.20.2") (d #t) (k 0)) (d (n "wasm-bindgen-wasm-conventions") (r "^0.2.92") (d #t) (k 0)) (d (n "wat") (r "^1.0") (d #t) (k 2)))) (h "0flvx2m0dxdlyrng8qllx02dwsbsp572ahynnhza6hw27icndacy") (r "1.57")))

