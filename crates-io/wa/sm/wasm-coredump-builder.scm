(define-module (crates-io wa sm wasm-coredump-builder) #:use-module (crates-io))

(define-public crate-wasm-coredump-builder-0.1.9 (c (n "wasm-coredump-builder") (v "0.1.9") (d (list (d (n "core-wasm-ast") (r "^0.1.9") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.9") (d #t) (k 0)))) (h "1xdpk84ldj2687d7klid53a9fzwqf5ca3zgbrx4g77nin1m5njhm")))

(define-public crate-wasm-coredump-builder-0.1.10 (c (n "wasm-coredump-builder") (v "0.1.10") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.10") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.10") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)))) (h "1fap8zjlwy9c75621nk095cpgxby3kmg3a73n6c08d19ib5hdfvc")))

(define-public crate-wasm-coredump-builder-0.1.11 (c (n "wasm-coredump-builder") (v "0.1.11") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.11") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.11") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)))) (h "0241w6ybbmbdbp4253n16lay9mbp7vlhpwk3fwz9ma4vbkrq108m")))

(define-public crate-wasm-coredump-builder-0.1.12 (c (n "wasm-coredump-builder") (v "0.1.12") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.12") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.12") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)))) (h "1vcaq05297jf6icsh92yqnbps2x4j6i4kf4sll1l3b2ji5yrm6hq")))

(define-public crate-wasm-coredump-builder-0.1.14 (c (n "wasm-coredump-builder") (v "0.1.14") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.14") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.14") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.14") (d #t) (k 2)))) (h "1d987ripgf49gmbqiqfashnc9dw72vr3l9cd6v3axwcs9nmddsvy")))

(define-public crate-wasm-coredump-builder-0.1.15 (c (n "wasm-coredump-builder") (v "0.1.15") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.15") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.15") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.15") (d #t) (k 2)))) (h "00hmil6v01wgpv5jvwqqcgvfbmjawb16ax8rq7h4yz67cw60bdxw")))

(define-public crate-wasm-coredump-builder-0.1.16 (c (n "wasm-coredump-builder") (v "0.1.16") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.16") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.16") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.16") (d #t) (k 2)))) (h "0lyw1q25p8fcxni8k2vdqs9fyl3zh73xqjvzxmzak1h5lnbvm2pn")))

(define-public crate-wasm-coredump-builder-0.1.17 (c (n "wasm-coredump-builder") (v "0.1.17") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.17") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.17") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.17") (d #t) (k 2)))) (h "0p1acr2bvrkm40gm9imm6kqa8h7p3qj3s956s9c61yzr704pj088")))

(define-public crate-wasm-coredump-builder-0.1.18 (c (n "wasm-coredump-builder") (v "0.1.18") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.18") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.18") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.18") (d #t) (k 2)))) (h "0yg0ljc4g7s9wgk6v1c0lqk3ia1m47myvr4flm5brazdx2608m0a")))

(define-public crate-wasm-coredump-builder-0.1.19 (c (n "wasm-coredump-builder") (v "0.1.19") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.19") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.19") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.19") (d #t) (k 2)))) (h "00n9f7f5nfc52l69mifirf9clkdabira2q5gmagr5kj2xyamz54m")))

(define-public crate-wasm-coredump-builder-0.1.20 (c (n "wasm-coredump-builder") (v "0.1.20") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.20") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.20") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.20") (d #t) (k 2)))) (h "0cx7676028yl7ijvw23v8alqxyg2zl7in7waq965l1r2vxvmp926")))

(define-public crate-wasm-coredump-builder-0.1.21 (c (n "wasm-coredump-builder") (v "0.1.21") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.21") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.21") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.21") (d #t) (k 2)))) (h "0ay5q4l4cj13jhh4x3izdxrf03w6qa2j3693znx5a33xyy828jaj")))

(define-public crate-wasm-coredump-builder-0.1.22 (c (n "wasm-coredump-builder") (v "0.1.22") (d (list (d (n "wasm-coredump-encoder") (r "^0.1.22") (d #t) (k 0)) (d (n "wasm-coredump-types") (r "^0.1.22") (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.23.0") (d #t) (k 0)) (d (n "wasm-printer") (r "^0.1.22") (d #t) (k 2)))) (h "0n3l7sg1fvn9zhxrasmaskvx71zrrfhidfj616k30i8f68mjdjii")))

