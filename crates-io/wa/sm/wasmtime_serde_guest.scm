(define-module (crates-io wa sm wasmtime_serde_guest) #:use-module (crates-io))

(define-public crate-wasmtime_serde_guest-0.1.0 (c (n "wasmtime_serde_guest") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "wasmtime_serde_guest_macro") (r "^0.1.0") (d #t) (k 0)))) (h "02zxh37sdwqxlr4w5vhd4v4l8wb4kj79697j507rdfxksvakf1l0")))

