(define-module (crates-io wa sm wasmedge-macro) #:use-module (crates-io))

(define-public crate-wasmedge-macro-0.1.0 (c (n "wasmedge-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jb6kmhh79kjszgi26x676895834pmfcpxliswj0by5996arppx3")))

(define-public crate-wasmedge-macro-0.2.0 (c (n "wasmedge-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1fbyb3fxl0gsmldwna84lps9zgslinbj4mrba6jx07vy0ywizpbb")))

(define-public crate-wasmedge-macro-0.2.1 (c (n "wasmedge-macro") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0wzddr9c8d5b0d8qangbkxcgsp25s16k1szq8ih8hyhiinqmw23l")))

(define-public crate-wasmedge-macro-0.3.0 (c (n "wasmedge-macro") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "09x0fasw5k7g6xsgb86mrfarxbsr6jnqijy95zg028rxgnd9l5bg")))

(define-public crate-wasmedge-macro-0.4.0 (c (n "wasmedge-macro") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0mdnkqy9i8m01lgfq1zc7a108valdklqbahlrkh0a9zxgz0qaa9p")))

(define-public crate-wasmedge-macro-0.5.0 (c (n "wasmedge-macro") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "02h7ij97ndbsgpgpd5bzgqzsyfz1jmj62pkm8jx1rvd8p39wn5xd")))

(define-public crate-wasmedge-macro-0.6.0 (c (n "wasmedge-macro") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0q5x64227n510w2xzlaqxr8vfb7xgqi8zzcqin49iks9lgh36wyk")))

(define-public crate-wasmedge-macro-0.6.1 (c (n "wasmedge-macro") (v "0.6.1") (d (list (d (n "proc-macro2") (r "^1.0.43") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14ka83i27xk248dg8ayv8lhdkz7xs6ncjyxgd9xwi6lfm2ahvs6v")))

