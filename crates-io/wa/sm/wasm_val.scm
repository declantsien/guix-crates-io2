(define-module (crates-io wa sm wasm_val) #:use-module (crates-io))

(define-public crate-wasm_val-0.1.0 (c (n "wasm_val") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "0x0kfjs2xikrpj2b61chpzmk89zwdkf18yfxw35qg4ysgl0i67n1")))

(define-public crate-wasm_val-0.1.1 (c (n "wasm_val") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "1v7xr7xvcgpi76z8avaji7mhcma8jwv3kfk65nw9vp0v7j3kc322")))

(define-public crate-wasm_val-0.1.2 (c (n "wasm_val") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "14daqq42frlm1c5f0bs6g0svs7x74q9b4xqfagyjclm7jasbw25g")))

(define-public crate-wasm_val-0.2.0 (c (n "wasm_val") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "1sxzia3q125r9f7400x3knl3hh7z7l2zvmi2d4k863ijyfyd5pj0")))

(define-public crate-wasm_val-0.2.1 (c (n "wasm_val") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "1zyq3v4wphjszvhvglmvygqa3jcl13rg4sf0rdpwdpx5rcdm2jls")))

(define-public crate-wasm_val-0.3.0 (c (n "wasm_val") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "1s218h336x7fmz3wd2kvnbqkkn22ncsdhz5y8zqh6mhj73c32i1n")))

(define-public crate-wasm_val-0.3.1 (c (n "wasm_val") (v "0.3.1") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "0bdqk9vjm7sr4kyq8lzqfxy0llg0nk6c4svs1mli6ax0g7hbkkwn")))

(define-public crate-wasm_val-0.3.2 (c (n "wasm_val") (v "0.3.2") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "1s8bd9p9idg6aaqnyvp50asdzv62fc3jip5wd7amqqxzvx1bgip5")))

(define-public crate-wasm_val-0.3.3 (c (n "wasm_val") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "17h08k37aq9d3s7kxqwkvxjp9qsvihqdh3l2nrxga1bpr7xiba6c")))

(define-public crate-wasm_val-0.3.4 (c (n "wasm_val") (v "0.3.4") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "03szzyljk8cncdw3kxlw7hicr21svl7r5wfbhqw6ida0d2l2bm3w")))

(define-public crate-wasm_val-0.3.5 (c (n "wasm_val") (v "0.3.5") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.5") (d #t) (k 0)))) (h "1m03r34h3g3dh0v25cqsl598q87h45rxv70a4hr0qzbp4ng7gacc")))

