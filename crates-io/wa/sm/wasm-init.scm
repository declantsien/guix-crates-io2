(define-module (crates-io wa sm wasm-init) #:use-module (crates-io))

(define-public crate-wasm-init-0.1.0 (c (n "wasm-init") (v "0.1.0") (d (list (d (n "gensym") (r "^0.1") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "paste") (r "^1") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "094m380f0zsvfmg916rv40cms8csw6aij9ja2pv3x994rkyhsb9x")))

(define-public crate-wasm-init-0.2.0 (c (n "wasm-init") (v "0.2.0") (d (list (d (n "gensym") (r "^0.1") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "paste") (r "^1") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "1yzj9fdmygnmi4pvqpb849f1lv5zj0jzrwl61fgwai18aizifw5v") (f (quote (("auto-init"))))))

(define-public crate-wasm-init-0.2.1 (c (n "wasm-init") (v "0.2.1") (d (list (d (n "gensym") (r "^0.1") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "paste") (r "^1") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "1l3kcmlk8l1dszh8hasihdhrbn8bgf31d5zk328i70dy3jq42q1c") (f (quote (("auto-init"))))))

(define-public crate-wasm-init-0.2.2 (c (n "wasm-init") (v "0.2.2") (d (list (d (n "gensym") (r "^0.1") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "js-sys") (r "^0.3") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "1nypcyhhj7aijvh60gyafpxyvvagwdz61q3yldfk826dw3v9m67y") (f (quote (("auto-init"))))))

