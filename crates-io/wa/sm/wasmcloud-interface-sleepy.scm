(define-module (crates-io wa sm wasmcloud-interface-sleepy) #:use-module (crates-io))

(define-public crate-wasmcloud-interface-sleepy-0.1.0 (c (n "wasmcloud-interface-sleepy") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.12.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7.0") (d #t) (k 1)))) (h "18kgn7857yf4dww0pm06fi7mk76q52d026gj51y033ra7ch9rsaz")))

(define-public crate-wasmcloud-interface-sleepy-0.1.1 (c (n "wasmcloud-interface-sleepy") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.12.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7.0") (d #t) (k 1)))) (h "15lzmhpz9vw4xjx2wmcqsmqbw179iwn3fscvacna2n9y7nbx4df7")))

(define-public crate-wasmcloud-interface-sleepy-0.1.2 (c (n "wasmcloud-interface-sleepy") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.12.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7.0") (d #t) (k 1)))) (h "004p5lk6kv24pp9ifscrbpy4sc7l02g0cnmka0x04mmsc5wxn6vr")))

(define-public crate-wasmcloud-interface-sleepy-0.1.3 (c (n "wasmcloud-interface-sleepy") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.12.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7.0") (d #t) (k 1)))) (h "1jiaydncjrq90m6vyp1p7a0fp54xnbjagc61l4hf13w87mpnraxg")))

(define-public crate-wasmcloud-interface-sleepy-0.1.4 (c (n "wasmcloud-interface-sleepy") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.12.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7.0") (d #t) (k 1)))) (h "04avcmz9kkg3fwdxc4g0wifbh259r20hgy9pc2xapk35b82g7p2p")))

(define-public crate-wasmcloud-interface-sleepy-0.1.5 (c (n "wasmcloud-interface-sleepy") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1.67") (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.5") (d #t) (k 0)) (d (n "wasmbus-rpc") (r "^0.13.0") (d #t) (k 0)) (d (n "weld-codegen") (r "^0.7.0") (d #t) (k 1)))) (h "1nbc44i11xa6s4v43nag0g23n58b8jfssf3zwwfcdh7mkh3ck6hc")))

