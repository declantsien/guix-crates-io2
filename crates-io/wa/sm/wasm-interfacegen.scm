(define-module (crates-io wa sm wasm-interfacegen) #:use-module (crates-io))

(define-public crate-wasm-interfacegen-0.1.0 (c (n "wasm-interfacegen") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (d #t) (k 0)) (d (n "wasm-interfacegen-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "wasm-interfacegen-macro-support") (r "^0.1.0") (d #t) (k 0)))) (h "1ggv4g3f3kyjpvf82jxa4rdn7xgzzjxknzah42xda72llvipd8ll")))

