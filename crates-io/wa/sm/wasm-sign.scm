(define-module (crates-io wa sm wasm-sign) #:use-module (crates-io))

(define-public crate-wasm-sign-0.1.0 (c (n "wasm-sign") (v "0.1.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.3") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.24.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "0jqspw6zgypaz418rpdh79bfc697az3kxx083aklair528rdbyy7")))

(define-public crate-wasm-sign-0.2.0 (c (n "wasm-sign") (v "0.2.0") (d (list (d (n "base64") (r "^0.9") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "openssl") (r "^0.10.3") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.24.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.5") (d #t) (k 0)))) (h "08jm5daw3wmmbz7f6v7sy816qbny8my9z451jvax3prx5r0lwa8r")))

