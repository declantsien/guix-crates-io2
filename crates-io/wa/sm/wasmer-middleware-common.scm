(define-module (crates-io wa sm wasmer-middleware-common) #:use-module (crates-io))

(define-public crate-wasmer-middleware-common-0.7.0 (c (n "wasmer-middleware-common") (v "0.7.0") (d (list (d (n "wasmer-runtime-core") (r "^0.7.0") (d #t) (k 0)))) (h "19kvxzx44hg3j1srac5ll276i4lzhv2njpdacsnfgxdgk7gl599r")))

(define-public crate-wasmer-middleware-common-0.8.0 (c (n "wasmer-middleware-common") (v "0.8.0") (d (list (d (n "wasmer-runtime-core") (r "^0.8.0") (d #t) (k 0)))) (h "0805zy2m1xxmc7qfhlqh0ab4jrlhq8d4avdynazn2r8xg2k21xas")))

(define-public crate-wasmer-middleware-common-0.9.0 (c (n "wasmer-middleware-common") (v "0.9.0") (d (list (d (n "wasmer-runtime-core") (r "^0.9.0") (d #t) (k 0)))) (h "1jygm2xw0hfn6mkvjzwrid97bfk10xax8396jvzcr4bfcv04g1g4")))

(define-public crate-wasmer-middleware-common-0.10.0 (c (n "wasmer-middleware-common") (v "0.10.0") (d (list (d (n "wasmer-runtime-core") (r "^0.10.0") (d #t) (k 0)))) (h "13qrcmglm76qs2bkszfpmv4jwq5dq4jg21bfmck59amrfzb157nv")))

(define-public crate-wasmer-middleware-common-0.10.1 (c (n "wasmer-middleware-common") (v "0.10.1") (d (list (d (n "wasmer-runtime-core") (r "^0.10.1") (d #t) (k 0)))) (h "0l1v8cg9xhwmvrqm6f938a96xkqwwz4ai0p1icba425jc91kl0g7")))

(define-public crate-wasmer-middleware-common-0.10.2 (c (n "wasmer-middleware-common") (v "0.10.2") (d (list (d (n "wasmer-runtime-core") (r "^0.10.2") (d #t) (k 0)))) (h "1iab24kak1aq7mki1wrdn030bm8xmajngz55s2pkgzwfga37lwnm")))

(define-public crate-wasmer-middleware-common-0.11.0 (c (n "wasmer-middleware-common") (v "0.11.0") (d (list (d (n "wasmer-runtime-core") (r "^0.11.0") (d #t) (k 0)))) (h "0j3m7aaa3q8hcg05ky380xbj4hwi9aarxhi1fvmmc1k5qg5wx650")))

(define-public crate-wasmer-middleware-common-0.12.0 (c (n "wasmer-middleware-common") (v "0.12.0") (d (list (d (n "wasmer-runtime-core") (r "^0.12.0") (d #t) (k 0)))) (h "1ivy98f0ml6nlflcx1pi68vp8s67sg9cvr6iib0pcn66fclqcahy")))

(define-public crate-wasmer-middleware-common-0.13.0 (c (n "wasmer-middleware-common") (v "0.13.0") (d (list (d (n "wasmer-runtime-core") (r "^0.13.0") (d #t) (k 0)))) (h "0hfrmf5y05sggyl95h4zsa5bz1mlzkxh2hr9xirk5ndj2cxl9nxi")))

(define-public crate-wasmer-middleware-common-0.13.1 (c (n "wasmer-middleware-common") (v "0.13.1") (d (list (d (n "wasmer-runtime-core") (r "^0.13.1") (d #t) (k 0)))) (h "1cr1bi80svqi522kd587hlfbk8a4m8x1ps4qxrmmbj9cqfzkm3ad")))

(define-public crate-wasmer-middleware-common-0.14.0 (c (n "wasmer-middleware-common") (v "0.14.0") (d (list (d (n "wasmer-runtime-core") (r "^0.14.0") (d #t) (k 0)))) (h "18xjqnyslywjznlkzdwb9lb48w9cngk41ylyj2ghllhd61yb2yai")))

(define-public crate-wasmer-middleware-common-0.14.1 (c (n "wasmer-middleware-common") (v "0.14.1") (d (list (d (n "wasmer-runtime-core") (r "^0.14.1") (d #t) (k 0)))) (h "1dd2q82jahnzr5wa8cj22y6ws0nayj6gpb2qc663m3ayc5n5x894")))

(define-public crate-wasmer-middleware-common-0.15.0 (c (n "wasmer-middleware-common") (v "0.15.0") (d (list (d (n "wasmer-runtime-core") (r "^0.15.0") (d #t) (k 0)))) (h "1vpp4vv7r5b8mgfpf3akfsmw1svqi23vyylap3jj6ial7ck6dxhm")))

(define-public crate-wasmer-middleware-common-0.16.0 (c (n "wasmer-middleware-common") (v "0.16.0") (d (list (d (n "wasmer-runtime-core") (r "^0.16.0") (d #t) (k 0)))) (h "0pcb00q6cis43m6kw49x3n18riaan3ramjv2q83bw45w1fn7qbyi")))

(define-public crate-wasmer-middleware-common-0.16.1 (c (n "wasmer-middleware-common") (v "0.16.1") (d (list (d (n "wasmer-runtime-core") (r "^0.16.1") (d #t) (k 0)))) (h "0y8vfizkqa46c6mimqyfdyljsgl6v780z72p0g4l0zk2ca8xxk90")))

(define-public crate-wasmer-middleware-common-0.16.2 (c (n "wasmer-middleware-common") (v "0.16.2") (d (list (d (n "wasmer-runtime-core") (r "^0.16.2") (d #t) (k 0)))) (h "1l3abz13pal4gm8znfl9hm0j4ia4ap50286f2byk91gfy9ssa457")))

(define-public crate-wasmer-middleware-common-0.17.0 (c (n "wasmer-middleware-common") (v "0.17.0") (d (list (d (n "wasmer-runtime-core") (r "^0.17.0") (d #t) (k 0)))) (h "11ycaqhd4vs80h205vcv71vywrgsw3zlh9j42d9bwpxjhs0hd57x")))

(define-public crate-wasmer-middleware-common-0.17.1 (c (n "wasmer-middleware-common") (v "0.17.1") (d (list (d (n "wasmer-runtime-core") (r "^0.17.1") (d #t) (k 0)))) (h "13gh7v12qb3ilmay9f7j008fl3f8j5sk96hxmya0j42dl4jhirsn")))

