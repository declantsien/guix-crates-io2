(define-module (crates-io wa sm wasmer_enumset_derive) #:use-module (crates-io))

(define-public crate-wasmer_enumset_derive-0.5.0 (c (n "wasmer_enumset_derive") (v "0.5.0") (d (list (d (n "darling") (r "^0.10.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "124ajn7s837r2zb0vbyryrbgwv1dxf2kyvdg1ch984g1k0nv7ld8") (f (quote (("serde"))))))

