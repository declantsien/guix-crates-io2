(define-module (crates-io wa sm wasmbin-derive) #:use-module (crates-io))

(define-public crate-wasmbin-derive-0.1.0 (c (n "wasmbin-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "07qg2bj9jfh209175rpv15m22pz3h1swdak94k06pd6wcn6jnlv2") (f (quote (("nightly"))))))

(define-public crate-wasmbin-derive-0.2.0 (c (n "wasmbin-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0simrcsda8p8bxlzy8y6gl411sbf947as62ny39v047iwcgvz1rg")))

(define-public crate-wasmbin-derive-0.2.1 (c (n "wasmbin-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1kxzdpyay8nbwlnjazwnwwygxmm84b9hffxq5d8jgggg1hvkw0dr")))

(define-public crate-wasmbin-derive-0.2.2 (c (n "wasmbin-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "02zfy3fa7jf60nv16qxvcyggsmghw7483y7sq60nrjd0cr01d0bx")))

(define-public crate-wasmbin-derive-0.2.3 (c (n "wasmbin-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (d #t) (k 0)) (d (n "synstructure") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1nvl51ppdcarn5gw5p96z7q5pgm6l793d3wzjc35l2vr9j3byq9r")))

