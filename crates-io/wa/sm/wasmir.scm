(define-module (crates-io wa sm wasmir) #:use-module (crates-io))

(define-public crate-wasmir-0.1.0 (c (n "wasmir") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "0ncvk8x0gpqripdd59m4cvs4ff8c4i275rh4bdhq3pxc6cx285xa")))

(define-public crate-wasmir-0.1.1 (c (n "wasmir") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "0pnxvnn8lig2vc7gnsashssd6vy30hvph0r8n323r5hg12jmx9gl")))

(define-public crate-wasmir-0.1.2 (c (n "wasmir") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "197j7gjxfnh0darf7g58904vhl5m0jp7mv7r5cj6sqwvf8py1wq4")))

(define-public crate-wasmir-0.1.3 (c (n "wasmir") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "1nzn0097xcy1r6k7lqfva4mrd25lkdiqb5ajiandb38h1yzpmr41")))

(define-public crate-wasmir-0.1.4 (c (n "wasmir") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "0dwbr8g5n34jzzgzpayb8d9yg0l4ihr9pn35dfk8jmv68s14wk48")))

(define-public crate-wasmir-0.1.5 (c (n "wasmir") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "0rpw71x6fy2glzh8ppk2i4kyqvahcplgsrcdq6bp5px8ii089dfk")))

(define-public crate-wasmir-0.1.6 (c (n "wasmir") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "12ijpx04dq28wpnp6j60ns6d8g1lfna0blppq5n1a6xw16z54w4c")))

(define-public crate-wasmir-0.1.7 (c (n "wasmir") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "1whri2fkql1gd211h1bjazkwjghgi5vdsqh9kjrdwsw69dqsmr55")))

(define-public crate-wasmir-0.1.8 (c (n "wasmir") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "12m01knbs8ihrdj9zjwdxfz4sab4fsc349nkhwgksrdgpliwj49s")))

(define-public crate-wasmir-0.1.9 (c (n "wasmir") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "0byahq7mw76nr3lnf56cnfpp455mr2n55dlf3p3ikj5y47nmbvhc")))

(define-public crate-wasmir-0.1.11 (c (n "wasmir") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "114k1252mznjvg6kasbwrmdlayn89y4k5qkc0rk4niq7rdzr647v")))

(define-public crate-wasmir-0.1.12 (c (n "wasmir") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "15xc8593jnbqvczr1r4i2dknxlwk73c0qlzhh4dg3z87a0hcwi7j")))

(define-public crate-wasmir-0.1.13 (c (n "wasmir") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (f (quote ("full" "extra-traits"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.74") (d #t) (k 0)))) (h "1z4xy4hv9q2inqg8awc6lvrdw1mnkr8aawgxq8lz4pjglcp7ryqj")))

