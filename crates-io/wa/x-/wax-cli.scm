(define-module (crates-io wa x- wax-cli) #:use-module (crates-io))

(define-public crate-wax-cli-0.1.0 (c (n "wax-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1x2qjwqmqqmrw000jyqx5vfgi2ihl91qnwvywpz2rwl30f97yilx")))

(define-public crate-wax-cli-0.1.1 (c (n "wax-cli") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0gc2x3ys14wisc91jzay4kbxlhvyqpqkvsfgxl64aalgvq556rjc")))

(define-public crate-wax-cli-0.1.2 (c (n "wax-cli") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0gr7hrd7nr0j7jdxvzry7gpriafwarw6n8g1ylz8z47da3q4p5z0")))

(define-public crate-wax-cli-0.1.3 (c (n "wax-cli") (v "0.1.3") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1qgbps59nb6cq4mxy72h0skchhkf0ji88q39hp8dyfck6wqfnmjv")))

(define-public crate-wax-cli-0.1.4 (c (n "wax-cli") (v "0.1.4") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1dm0502n7laqvzdhi4v1xw7jajijhr40hbkwzjgvnnd1lhfcpbbv")))

(define-public crate-wax-cli-0.1.5 (c (n "wax-cli") (v "0.1.5") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0ir037w7wz80vxz0jwgnh25dna3sisxw4rph41hfnlq2s80b02cq")))

(define-public crate-wax-cli-0.1.6 (c (n "wax-cli") (v "0.1.6") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0yyrr78i1d3fs32433krd6c4jignq6sacyz3j0rdgvp3jr2wbrmy")))

(define-public crate-wax-cli-0.1.7 (c (n "wax-cli") (v "0.1.7") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1mdbqhfckvxwlnjnwm33q4d759lwk5h0f77816d3nr1fsi3gazx1")))

(define-public crate-wax-cli-0.1.8 (c (n "wax-cli") (v "0.1.8") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1km46lxzdp3ar9clffgwc2440qi98g2cnj4aj9qrcrsd4x8bchfv")))

(define-public crate-wax-cli-0.1.9 (c (n "wax-cli") (v "0.1.9") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1945dxyr0xljjjplkzkysfzqy8ihdlv5fka0y9wf587xhxc5p8v9")))

(define-public crate-wax-cli-0.1.10 (c (n "wax-cli") (v "0.1.10") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "1qwxx7ph8m6ns317z2dv0r5wrsjj552p4j3krx5g27zz3sxnx6j3")))

(define-public crate-wax-cli-0.2.0 (c (n "wax-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "minify-html") (r "^0.10.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)))) (h "0wki5bs1kl3y6jjgvjdf2axvx82ncalz7p7d1ldsx50smpcza2nk")))

(define-public crate-wax-cli-0.2.1 (c (n "wax-cli") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "minify-html") (r "^0.10.3") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.9.2") (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (d #t) (k 0)))) (h "0xza298126hfwxqwwib6vga6rqmac1nxv24jsj7yqcblyjhsi1dw")))

