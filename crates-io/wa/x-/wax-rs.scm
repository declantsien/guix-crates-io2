(define-module (crates-io wa x- wax-rs) #:use-module (crates-io))

(define-public crate-wax-rs-0.0.1 (c (n "wax-rs") (v "0.0.1") (h "1q7chvwv8znnrdqs42w18cvqgbiglm9pg3r9pm48b7s71qmx33q1") (y #t)))

(define-public crate-wax-rs-0.0.2 (c (n "wax-rs") (v "0.0.2") (h "1fy97zr9igyjqclqxr27w0lk444l6j462iy6gccra0bljy74pjnh")))

