(define-module (crates-io wa lc walc_model) #:use-module (crates-io))

(define-public crate-walc_model-0.1.0 (c (n "walc_model") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vpqw6ra1ibzx96mhy6wc4jmhsfizgsq4a7c24b0mrdfhhg0ccsm")))

(define-public crate-walc_model-0.1.1 (c (n "walc_model") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ka6cbmd39nkbbnk15fd2c3hdmxs2qmjr52bz2988pkjwz8ivgif")))

