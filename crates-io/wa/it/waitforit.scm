(define-module (crates-io wa it waitforit) #:use-module (crates-io))

(define-public crate-waitforit-0.1.0 (c (n "waitforit") (v "0.1.0") (d (list (d (n "ureq") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.2.0") (o #t) (d #t) (k 0)))) (h "0mlwfmaaf3yid583bg9wz98d8pppyr7w4fa75m963h60p9h7gw83") (f (quote (("http" "ureq" "url") ("default" "http"))))))

