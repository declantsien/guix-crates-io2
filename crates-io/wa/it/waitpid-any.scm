(define-module (crates-io wa it waitpid-any) #:use-module (crates-io))

(define-public crate-waitpid-any-0.1.0 (c (n "waitpid-any") (v "0.1.0") (d (list (d (n "rustix") (r "^0.38.3") (f (quote ("event" "process" "std"))) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02416pcq4sc1jf8ljg4vscdbbl8b3hwcb5kv2wln9vcgqc66iwyn") (r "1.63")))

(define-public crate-waitpid-any-0.2.0 (c (n "waitpid-any") (v "0.2.0") (d (list (d (n "rustix") (r "^0.38.3") (f (quote ("event" "process" "std"))) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.48.0") (f (quote ("Win32_Foundation" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0cs0y4b6g0lqd8kdcph8dp24jpd1fsaqfwjvj2gkbz24c32w7y2l") (r "1.63")))

(define-public crate-waitpid-any-0.2.1 (c (n "waitpid-any") (v "0.2.1") (d (list (d (n "rustix") (r "^0.38") (f (quote ("event" "process" "std"))) (t "cfg(unix)") (k 0)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "04kwil15p63qqs1rjvrgzddmx8ms4c90rpqxqvjqckf5jdy1b281") (r "1.63")))

