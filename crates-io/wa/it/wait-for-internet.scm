(define-module (crates-io wa it wait-for-internet) #:use-module (crates-io))

(define-public crate-wait-for-internet-0.1.8 (c (n "wait-for-internet") (v "0.1.8") (d (list (d (n "online") (r "^4.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0kmvr82ygnf06ldg6yknysar3dbk6jb9r7pqagrn1d7iyjrxmima")))

(define-public crate-wait-for-internet-0.1.9 (c (n "wait-for-internet") (v "0.1.9") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "online") (r "^4.0.0") (d #t) (k 0)))) (h "05s40ck4i6izhd5r1a1gjplmr56ncnq5gmqs73cha0wpinwghr6p")))

