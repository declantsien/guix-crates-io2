(define-module (crates-io wa it wait-on) #:use-module (crates-io))

(define-public crate-wait-on-0.0.1 (c (n "wait-on") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("std" "derive" "env"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "1fmp2yy7bp8h6f521a2qjy9ryk2vb9d6pbjrlmrsl1r3603092j9")))

(define-public crate-wait-on-0.0.2 (c (n "wait-on") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("std" "derive" "env"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "rt-multi-thread" "macros" "net"))) (d #t) (k 0)))) (h "172fqrmb43v3nivjkgigwxmab1xsibixg9bhpcs3ywl7922069dg")))

(define-public crate-wait-on-0.0.3 (c (n "wait-on") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("std" "derive" "env"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "rt-multi-thread" "macros" "net"))) (d #t) (k 0)))) (h "1cwiz1p4l46inix43c51v5gmk2cckij04457kik8sgahiridhi0v")))

(define-public crate-wait-on-0.0.4 (c (n "wait-on") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("std" "derive" "env"))) (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("io-util" "rt-multi-thread" "macros" "net"))) (d #t) (k 0)))) (h "06k0q0gwzrxagc65bzy3kkhg24qbv065s21kxpbwlbm0aidc8gmf")))

