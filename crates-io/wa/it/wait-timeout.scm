(define-module (crates-io wa it wait-timeout) #:use-module (crates-io))

(define-public crate-wait-timeout-0.1.0 (c (n "wait-timeout") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "0whyzhxwfkl8hgvcyzdjxlna2f4hjbjnc9c4ms60avd6xfk4gb9h")))

(define-public crate-wait-timeout-0.1.1 (c (n "wait-timeout") (v "0.1.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1g3ybw9nqvbhkkkb9096y49yp7vhrjfvgznr06pyx549jls3wfw1") (y #t)))

(define-public crate-wait-timeout-0.1.2 (c (n "wait-timeout") (v "0.1.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "kernel32-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "01p6x0r7wlqdp7hjk35vxqzks91ymlxgnzjc8pig73s16g12z36g")))

(define-public crate-wait-timeout-0.1.3 (c (n "wait-timeout") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 2)))) (h "01adc97zr1f2caq0jm65xi1f78lpjvhqnbl038kgazvhlm7b92iy")))

(define-public crate-wait-timeout-0.1.4 (c (n "wait-timeout") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01klarf4clq91hliial69ra1n9syfqjkns5ca5ak5bxvlkpc5jir")))

(define-public crate-wait-timeout-0.1.5 (c (n "wait-timeout") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jckn2am7m6q1mlmwvwifqglf5r1d2aqnixn7nck25c039sbzwxr")))

(define-public crate-wait-timeout-0.2.0 (c (n "wait-timeout") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)))) (h "1xpkk0j5l9pfmjfh1pi0i89invlavfrd9av5xp0zhxgb29dhy84z")))

