(define-module (crates-io wa it wait-until) #:use-module (crates-io))

(define-public crate-wait-until-1.0.0 (c (n "wait-until") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "async-std") (r "^1.10.0") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "parse_duration") (r "^2.1.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "19b27kpc60870ahvrfbl6pmr0h1slih9s11591r94yc5sw9gxrqp")))

