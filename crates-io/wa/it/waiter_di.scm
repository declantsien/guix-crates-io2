(define-module (crates-io wa it waiter_di) #:use-module (crates-io))

(define-public crate-waiter_di-1.0.0 (c (n "waiter_di") (v "1.0.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.0.0") (d #t) (k 0)))) (h "1lrz7sdk3bx889br8rf6g8rv3q5l92vlyxqy6c9jn16bxq8dp1rk")))

(define-public crate-waiter_di-1.1.0 (c (n "waiter_di") (v "1.1.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.0.0") (d #t) (k 0)))) (h "18jyyv9nyfnyzgv45z5w28syi1sbdvdlnixk6nw9ik9zlzzgc14q")))

(define-public crate-waiter_di-1.1.1 (c (n "waiter_di") (v "1.1.1") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.1.1") (d #t) (k 0)))) (h "1nmd4hi03q50mhj3f9pjl45wayxxfhjyl2hj41phdm7fy4iv5qrz")))

(define-public crate-waiter_di-1.1.2 (c (n "waiter_di") (v "1.1.2") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.1.1") (d #t) (k 0)))) (h "1qy3n9rnllki7y1vb3d3i6k4a3pxyp3hxjrlxdcramk9jxh1vni5")))

(define-public crate-waiter_di-1.1.3 (c (n "waiter_di") (v "1.1.3") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.1.3") (d #t) (k 0)))) (h "1vbiasgwya1gcx09zdna03bn0sn6y8js932pnz9l6n582f090bhd")))

(define-public crate-waiter_di-1.1.4 (c (n "waiter_di") (v "1.1.4") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.1.4") (d #t) (k 0)))) (h "08a547dk0x5496msn72c6q7l820xmy6lqka2qfl5d0p0xnvx1ib5")))

(define-public crate-waiter_di-1.1.5 (c (n "waiter_di") (v "1.1.5") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.1.5") (d #t) (k 0)))) (h "1rsk7fxqbxqmzj2qgmnchmk3x6mg0gg7ki94jsadp2wxs4r25nmr")))

(define-public crate-waiter_di-1.1.6 (c (n "waiter_di") (v "1.1.6") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.1.6") (d #t) (k 0)))) (h "1fjy7g276186hc2rip7g8cw5nnnsrw59ypqhhwfv8sys53v0ps07")))

(define-public crate-waiter_di-1.2.0 (c (n "waiter_di") (v "1.2.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.2.0") (d #t) (k 0)))) (h "14kmnj28bilzb1l0kvvhw7rawp8ihb66bvlncy7d1qa32hp0q1cg")))

(define-public crate-waiter_di-1.2.1 (c (n "waiter_di") (v "1.2.1") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.2.1") (d #t) (k 0)))) (h "0c41j3dkz6czh54kc0wy09f6zmr9npfbwffj0dskk478258yklyb")))

(define-public crate-waiter_di-1.2.2 (c (n "waiter_di") (v "1.2.2") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.2.2") (d #t) (k 0)))) (h "0j3if6zd714xqjyl9ik5j8bclnl485nc9291ccvjcwwgr0m46h7y")))

(define-public crate-waiter_di-1.2.3 (c (n "waiter_di") (v "1.2.3") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.2.2") (d #t) (k 0)))) (h "1kmx40f7dbdr3k9f24l3ykrljclxa166jw9qm6fis24i65ybwmdv")))

(define-public crate-waiter_di-1.2.4 (c (n "waiter_di") (v "1.2.4") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.2.2") (d #t) (k 0)))) (h "0ivcp2axdxapmxhjmjr4g62n5acv4m0lcnfd3lm7ljbls9bwxgjj")))

(define-public crate-waiter_di-1.3.0 (c (n "waiter_di") (v "1.3.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.3.0") (d #t) (k 0)))) (h "0yikrs442rb50j7sxj3j5ln1l95s96z5dxv9kp5gvhhpgjahiv26")))

(define-public crate-waiter_di-1.4.0 (c (n "waiter_di") (v "1.4.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.4.0") (d #t) (k 0)))) (h "09nh038iks710x5ngqgs2awsihzdga6vyw6ljayw8ypwvy0zm3gj")))

(define-public crate-waiter_di-1.5.0 (c (n "waiter_di") (v "1.5.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.5.0") (d #t) (k 0)))) (h "1whmmvb21gdqfmxvilirxylhzzr6gfv8dxrbwr4csg4h5csszn3v")))

(define-public crate-waiter_di-1.6.0 (c (n "waiter_di") (v "1.6.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.6.0") (d #t) (k 0)))) (h "1qzbpa5rblza44h2bvj0sc5zhpw579x84d2ngkzy4vfmwdip8rpl") (f (quote (("async" "waiter_codegen/async"))))))

(define-public crate-waiter_di-1.6.1 (c (n "waiter_di") (v "1.6.1") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.6.1") (d #t) (k 0)))) (h "1wy82yfn35ajb766ydkgn1zg8mmycqhnvz4njdkx7m0a97i6kj97") (f (quote (("async" "waiter_codegen/async"))))))

(define-public crate-waiter_di-1.6.2 (c (n "waiter_di") (v "1.6.2") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.6.1") (d #t) (k 0)))) (h "04rp1vh7wpm64ky64shwn619m641pqivk5lh8i7mhzd7hyslrlla") (f (quote (("async" "waiter_codegen/async"))))))

(define-public crate-waiter_di-1.6.3 (c (n "waiter_di") (v "1.6.3") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.6.3") (d #t) (k 0)))) (h "1yjbhl171p2rfc5yj6n9v73jrz1qivjg39na9n5n3lmg9i348xyf") (f (quote (("async" "waiter_codegen/async"))))))

(define-public crate-waiter_di-1.6.4 (c (n "waiter_di") (v "1.6.4") (d (list (d (n "config") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.11, <0.5.0") (d #t) (k 0)) (d (n "regex") (r ">=1.3.9, <2.0.0") (d #t) (k 0)) (d (n "serde") (r ">=1.0.115, <2.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r ">=1.6.4, <2.0.0") (d #t) (k 0)))) (h "0sxl3vp9rmni7hfj2fdbh1n9cgmw3vr6mi2nz4pgc5gkch0n1m34") (f (quote (("async" "waiter_codegen/async"))))))

(define-public crate-waiter_di-1.6.5 (c (n "waiter_di") (v "1.6.5") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.6.5") (d #t) (k 0)))) (h "0v73zv9djljk7zn75lh0krsy3b97pfglz7p4mpf10z2p0vpk2a1y") (f (quote (("async" "waiter_codegen/async"))))))

(define-public crate-waiter_di-1.6.6 (c (n "waiter_di") (v "1.6.6") (d (list (d (n "config") (r "^0.14.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "waiter_codegen") (r "^1.6.6") (d #t) (k 0)))) (h "1hqh6zvksd2iw6hd14hpm33278bbz7a73bgzx5v5fjmj1cbkph56") (f (quote (("async" "waiter_codegen/async"))))))

