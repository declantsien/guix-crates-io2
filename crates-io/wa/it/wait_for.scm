(define-module (crates-io wa it wait_for) #:use-module (crates-io))

(define-public crate-wait_for-0.1.0 (c (n "wait_for") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "10djz6yrw13rgx4l3an4mcb45mp4abzczfnv2hv2gl2as8506d9s")))

(define-public crate-wait_for-0.1.1 (c (n "wait_for") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)))) (h "1ckhdsfmz846sqzdlg05shk351330z2c2r3v4ksz6m60mfy0lw3i")))

