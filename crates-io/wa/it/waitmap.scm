(define-module (crates-io wa it waitmap) #:use-module (crates-io))

(define-public crate-waitmap-1.0.0 (c (n "waitmap") (v "1.0.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^3.7.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1f5y5nij7fdj4s9d62mgqkjpmmbwin9m93aadsjnfdy3lwl0hyzz")))

(define-public crate-waitmap-1.0.1 (c (n "waitmap") (v "1.0.1") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^3.7.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0yhfq998b898q77lvylj6kq8k9bcc4p449lpd70g00d9w0n3pqm7")))

(define-public crate-waitmap-1.0.2 (c (n "waitmap") (v "1.0.2") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^3.7.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "1ksq1wcjhncszy4pxyds2p8gpbv7ih803pwlyfyczxq9svprc94v")))

(define-public crate-waitmap-1.1.0 (c (n "waitmap") (v "1.1.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("unstable"))) (d #t) (k 2)) (d (n "dashmap") (r "^3.7.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.2.0") (d #t) (k 0)))) (h "0aymjvsxni2d0n5xs001g0h3mccjwx5rl4xy4zqbk85rnq8icj98")))

