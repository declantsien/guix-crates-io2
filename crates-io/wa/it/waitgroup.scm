(define-module (crates-io wa it waitgroup) #:use-module (crates-io))

(define-public crate-waitgroup-0.1.0 (c (n "waitgroup") (v "0.1.0") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)))) (h "1pymf5jv2sjqrxjrrkkcrk3s66yb6myjkdb0yacg1sq3jr2dwrg5")))

(define-public crate-waitgroup-0.1.1 (c (n "waitgroup") (v "0.1.1") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-core") (r "^0.3.4") (d #t) (k 0)))) (h "1w6025gr19bm6xkimns6z0v16h8nsnsrf02vqca9jx9x86ilfv9h")))

(define-public crate-waitgroup-0.1.2 (c (n "waitgroup") (v "0.1.2") (d (list (d (n "async-std") (r "^1.5.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "atomic-waker") (r "^1.0") (d #t) (k 0)))) (h "14ljyy4b921y41qbghgg75745g7l883d3y8009n7wil3lw001xfi")))

