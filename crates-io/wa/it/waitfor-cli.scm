(define-module (crates-io wa it waitfor-cli) #:use-module (crates-io))

(define-public crate-waitfor-cli-0.1.0 (c (n "waitfor-cli") (v "0.1.0") (d (list (d (n "num-format") (r "^0.4") (f (quote ("with-system-locale"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "02jmz7bi142vpj54waqqfl07ni7nppwvc6yhxz7hg8fsxaj0r77x")))

(define-public crate-waitfor-cli-0.1.1 (c (n "waitfor-cli") (v "0.1.1") (d (list (d (n "num-format") (r "^0.4") (f (quote ("with-system-locale"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "098ch3sc4w66j3sr1dnaj4r3ry8s9nf6vqx6qnq6qirhz0b25a5k")))

