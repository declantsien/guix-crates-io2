(define-module (crates-io wa it waitlist) #:use-module (crates-io))

(define-public crate-waitlist-0.1.0 (c (n "waitlist") (v "0.1.0") (d (list (d (n "futures-task") (r "^0.3") (d #t) (k 2)))) (h "0viibrzxy7j8ax9bmiwidaixbmafc5r83a7gi1dqr5rhz46mf6x3")))

(define-public crate-waitlist-0.1.1 (c (n "waitlist") (v "0.1.1") (d (list (d (n "futures-task") (r "^0.3") (d #t) (k 2)))) (h "08hqysc4f1s4zbmj0w1rmf48r8xns4824wd6scxa1r0gfn4rhbby")))

