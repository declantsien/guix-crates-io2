(define-module (crates-io wa it wait4) #:use-module (crates-io))

(define-public crate-wait4-0.1.0 (c (n "wait4") (v "0.1.0") (d (list (d (n "human_format") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi" "minwindef" "winnt"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19di3l9ijyxhm512fckfm442qgm03nv44zql3zi8p4pi0zc1jg5k")))

(define-public crate-wait4-0.1.1 (c (n "wait4") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "human_format") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi" "minwindef" "winnt" "psapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "16bqvmkrc16pyap21w7ypis0ncmk37mysgqppgxr836qzyv30haf")))

(define-public crate-wait4-0.1.2 (c (n "wait4") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "human_format") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi" "minwindef" "winnt" "psapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0av10g4cvvwcs4s3whlr99nwzw8sp3d4j6ir1hjvbphljhf117am")))

(define-public crate-wait4-0.1.3 (c (n "wait4") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "human_format") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi" "minwindef" "winnt" "psapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1sk8qpjbrs42vv2n50ll6h5c8hkz67l4ah1103jx47wl32kwzbg1")))

