(define-module (crates-io wa it wait_not_await) #:use-module (crates-io))

(define-public crate-wait_not_await-0.1.0 (c (n "wait_not_await") (v "0.1.0") (h "0pswya3w2w3rjx1sih1v9mkm1xlnhhqjvnhkcpgkcnhbvh1sdgrz")))

(define-public crate-wait_not_await-0.2.0 (c (n "wait_not_await") (v "0.2.0") (h "15ibl3zw638wmwfz8v0c93p6xdp5xdcs4fd1mdyhfcjybrw11bc5")))

(define-public crate-wait_not_await-0.2.1 (c (n "wait_not_await") (v "0.2.1") (h "0qlx991w8d43xnyylv2nv83pkgjclhdcxwzhal5z1qk4bx1ymbk5")))

