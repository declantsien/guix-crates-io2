(define-module (crates-io wa it wait_file_created) #:use-module (crates-io))

(define-public crate-wait_file_created-0.1.0 (c (n "wait_file_created") (v "0.1.0") (d (list (d (n "inotify") (r "^0.9.3") (k 0)) (d (n "mktemp") (r "^0.4.1") (d #t) (k 2)))) (h "0flyzw4cyaa2a2r11b5xv970l3gang5ssw9sc9ackm25hwl9b2kb")))

