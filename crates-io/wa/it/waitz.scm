(define-module (crates-io wa it waitz) #:use-module (crates-io))

(define-public crate-waitz-0.1.0 (c (n "waitz") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1zm7zpcx0i90q1aa8kvzccqfq9xlh4b886i2cddm58jsfqhilszi")))

(define-public crate-waitz-0.2.0 (c (n "waitz") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "0xpzixj415xwgj4vkdbd3iqm9idqmhgjj96x9bz4dm541mn0qsah")))

(define-public crate-waitz-0.2.1 (c (n "waitz") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1vigrr7zc9xfqf383hihyp38a087wldr2xp1zbgcj84qxrhsfz47")))

(define-public crate-waitz-0.2.2 (c (n "waitz") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1c2ajad0if1acd8k4np64kas4769hmnzs34j7h7f5l7fpf7m9z21")))

(define-public crate-waitz-0.3.0 (c (n "waitz") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)))) (h "1r1z1izv6i4jnnx1wa7mb7shpf616ix0qcm7lv0pfdx9s6bhpm6z")))

