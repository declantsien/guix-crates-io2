(define-module (crates-io wa it waiter) #:use-module (crates-io))

(define-public crate-waiter-0.1.0 (c (n "waiter") (v "0.1.0") (h "0qkf1n2ykq672afm41c3dg2yvifmwj9mjbl5fwy7ari5byf962vz")))

(define-public crate-waiter-0.1.1 (c (n "waiter") (v "0.1.1") (h "0yiikj2zsbks1hcv9hl3bp1dc6vz4h4wklqfpbd26dc7j41b3ixz")))

(define-public crate-waiter-0.2.0 (c (n "waiter") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("time"))) (k 0)))) (h "10gw2hjnn9ggkb06spl8rc6ckyy0sfcxyjrm5641pvqj6j00a52s")))

