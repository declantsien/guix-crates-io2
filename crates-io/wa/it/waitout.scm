(define-module (crates-io wa it waitout) #:use-module (crates-io))

(define-public crate-waitout-0.1.0 (c (n "waitout") (v "0.1.0") (h "0nw4has4h2zr15ahlxsihsi0cblgysiiky7nc1ch6cxkd28kqwr5")))

(define-public crate-waitout-0.1.1 (c (n "waitout") (v "0.1.1") (h "0l674g0829gfx83x328dbqhb9igfpg2lz00d5wqvrcyrplwqzd56")))

