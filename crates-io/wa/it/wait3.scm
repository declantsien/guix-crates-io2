(define-module (crates-io wa it wait3) #:use-module (crates-io))

(define-public crate-wait3-0.1.0 (c (n "wait3") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1r4y8rhc8s5a4x2rg6bd0qmal2bb55plgwl86d357xi1ibwdwz6f")))

(define-public crate-wait3-0.1.1 (c (n "wait3") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "12a6n40nkxk7cl750qwvnxclg4fb3s26wdi36h9y79y1mv029dsw")))

(define-public crate-wait3-0.3.1 (c (n "wait3") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1gw2abi4hg0qbr9p06lkra4md0i9ri0zjxz16d5hnd8pai74x0s8")))

(define-public crate-wait3-0.3.3 (c (n "wait3") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "08jj86gmgxdf7yvcazvdwhi5n543gmfxwdl3gi8rcr47bi47sim7")))

(define-public crate-wait3-0.3.4 (c (n "wait3") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1mrcdjpgn90akm18agjim3ap97131a5s2d2szyx4s3215cliij7f")))

(define-public crate-wait3-0.4.0 (c (n "wait3") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (f (quote ("use_std"))) (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)))) (h "1qy0hq6pxjvxqd7h3ring6vqdq88pcyyjwdzcsii30nwp6x2b9vy")))

