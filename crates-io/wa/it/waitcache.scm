(define-module (crates-io wa it waitcache) #:use-module (crates-io))

(define-public crate-waitcache-0.1.0 (c (n "waitcache") (v "0.1.0") (d (list (d (n "dashmap") (r "^3") (d #t) (k 0)) (d (n "waitcell") (r "^0.3") (d #t) (k 0)))) (h "0k4vyvdkkp9fa604hqyimry84pl7b9ii7gz607jlv08k57gflg4d") (y #t)))

(define-public crate-waitcache-0.1.1 (c (n "waitcache") (v "0.1.1") (d (list (d (n "dashmap") (r ">=3, <5") (d #t) (k 0)) (d (n "waitcell") (r "~0.3.1") (d #t) (k 0)))) (h "1nqcgbk30g77bh3pqnpif422lfzh0786p8az4zpqacs0381aqk27") (y #t)))

(define-public crate-waitcache-0.1.2 (c (n "waitcache") (v "0.1.2") (d (list (d (n "dashmap") (r ">=3, <5") (d #t) (k 0)) (d (n "waitcell") (r "~0.4.0") (d #t) (k 0)))) (h "1vcmxa2052zal1cbg0rd2xrrkbspwnaqlv8x8hak767k2d5wfyiw")))

(define-public crate-waitcache-0.1.3 (c (n "waitcache") (v "0.1.3") (d (list (d (n "dashmap") (r ">=3, <6") (d #t) (k 0)) (d (n "waitcell") (r "~0.4.0") (d #t) (k 0)))) (h "1n7kdyk0sxgzspm0i5v9m2sqpk23cfzh98bcibnkz32gf89z5yzj")))

