(define-module (crates-io wa it waiter_codegen) #:use-module (crates-io))

(define-public crate-waiter_codegen-1.0.0 (c (n "waiter_codegen") (v "1.0.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "12haa7bmwrmzkngv63zyinp40a9l0ci3xpyk80ms681qq9ixwika")))

(define-public crate-waiter_codegen-1.1.0 (c (n "waiter_codegen") (v "1.1.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "19kzp16m29xr2vyp1k6hcayy9irznv5dncpqynj818185vmhh0id")))

(define-public crate-waiter_codegen-1.1.1 (c (n "waiter_codegen") (v "1.1.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "063dx8wlgmcmbxyzxsfc7s2cmaw2r44vwv3k3hfggcnkrwc0ik7a")))

(define-public crate-waiter_codegen-1.1.3 (c (n "waiter_codegen") (v "1.1.3") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0jv5gwyzmn7fgz88v0vj4ag7drr1nzjabfiz8vxjfy7n6zw4l8pn")))

(define-public crate-waiter_codegen-1.1.4 (c (n "waiter_codegen") (v "1.1.4") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0b7g919di4xzvgnjczrwhsxhr20x8lmynzprq76swbqm597440jd")))

(define-public crate-waiter_codegen-1.1.5 (c (n "waiter_codegen") (v "1.1.5") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "06j3xz7akn853shjh193cwwgfaydq458r4hj5kl34dzgvqs6y0ws")))

(define-public crate-waiter_codegen-1.1.6 (c (n "waiter_codegen") (v "1.1.6") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "165yyaalkfkai93dan9aq8w1afvsi42sj3igd05b8p9ng1zaw3i1")))

(define-public crate-waiter_codegen-1.2.0 (c (n "waiter_codegen") (v "1.2.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "12yyzkr6f1cfagysxnn8mr7jhlgpx6ylnypza02ps9rjs83a97f0")))

(define-public crate-waiter_codegen-1.2.1 (c (n "waiter_codegen") (v "1.2.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1gw4hcqz5b81wqkra2pasxgjmqwzx2yd5hg19jdajnm57545zdvw")))

(define-public crate-waiter_codegen-1.2.2 (c (n "waiter_codegen") (v "1.2.2") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0nvpszsdw8phpy0smxh6ixfllsi6mr78dv9mi21ns7qbpdjfqsnn")))

(define-public crate-waiter_codegen-1.3.0 (c (n "waiter_codegen") (v "1.3.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0nrj8z75ph12c3vh8c3bkwwjp3n901zzjfsaj64bax95amb7azrk")))

(define-public crate-waiter_codegen-1.4.0 (c (n "waiter_codegen") (v "1.4.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1zri01ih6qq6mb3h6957wbzhfn4gq9sdvagmwy67p00na0x7ibxx")))

(define-public crate-waiter_codegen-1.5.0 (c (n "waiter_codegen") (v "1.5.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1p9s1jshg8m23rxhlq325mv35k19q60b6x3ig9rbvk7cvqdpmdy2")))

(define-public crate-waiter_codegen-1.6.0 (c (n "waiter_codegen") (v "1.6.0") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "02v7silfspk3bsly9h89ckqm782ndfg0nbjd88g68g68487318xk") (f (quote (("async"))))))

(define-public crate-waiter_codegen-1.6.1 (c (n "waiter_codegen") (v "1.6.1") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "00dmr9r56gylwn2psmbbiswn7b3b7bh7kl7mfb0zmmsnig2v27bc") (f (quote (("async"))))))

(define-public crate-waiter_codegen-1.6.3 (c (n "waiter_codegen") (v "1.6.3") (d (list (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "07x86aa5lr50bc3nqlvlcb9m2gckriqs7dvh2psq4imvhkmgnih6") (f (quote (("async"))))))

(define-public crate-waiter_codegen-1.6.4 (c (n "waiter_codegen") (v "1.6.4") (d (list (d (n "quote") (r ">=1.0.7, <2.0.0") (d #t) (k 0)) (d (n "regex") (r ">=1.3.9, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.48, <2.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0nw3scv5841a3ci0kl3qrr6afmik5dl7kw0n6lrqbyaga1qfl37d") (f (quote (("async"))))))

(define-public crate-waiter_codegen-1.6.5 (c (n "waiter_codegen") (v "1.6.5") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "1rdqmzyg79vmsdw95xy4pcz2am9lhn046bzkk3xa33j9c9iy3xvq") (f (quote (("async"))))))

(define-public crate-waiter_codegen-1.6.6 (c (n "waiter_codegen") (v "1.6.6") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "14vc0rx2slryj8zvfcvrhnszpzwp56l87cia576gjr8x5gwimhvw") (f (quote (("async"))))))

