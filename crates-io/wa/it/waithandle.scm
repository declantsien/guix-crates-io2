(define-module (crates-io wa it waithandle) #:use-module (crates-io))

(define-public crate-waithandle-0.1.0 (c (n "waithandle") (v "0.1.0") (h "1pbjz1qf2s9391c6xqiklcpxi09dbx9ry7cwz0g8sna7fm86w3wx")))

(define-public crate-waithandle-0.2.0 (c (n "waithandle") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "1ckkixwg9b1gz6acvm5fq1w61yjnh195bslhr8hl9sbzifwzkb1g")))

(define-public crate-waithandle-0.3.0 (c (n "waithandle") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "0wzhqf1gvqv2h7il1ai7vdm3mvb65fc49x178rpqib70g6vkvl7f")))

(define-public crate-waithandle-0.4.0 (c (n "waithandle") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "01n2mi33x67rjqk81j32lpzc6ia06jm2nza4vlw613i618s613q1")))

