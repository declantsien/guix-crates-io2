(define-module (crates-io wa it wait_group) #:use-module (crates-io))

(define-public crate-wait_group-0.1.0 (c (n "wait_group") (v "0.1.0") (h "1jdks0pgq0lbmsa161ihsmpn37vczkcgm4nd01gfkdbddjdwpnwv") (y #t)))

(define-public crate-wait_group-0.1.1 (c (n "wait_group") (v "0.1.1") (h "09h1bvigbsmfyd6s12f4h9cas75yqlb9wy8qlg49jmhb00za89ca") (y #t)))

(define-public crate-wait_group-0.1.2 (c (n "wait_group") (v "0.1.2") (h "1hmc381yly5kp194gg8rfmqqdqh1k79nk2pkcncijxc8aqrsc1yc")))

(define-public crate-wait_group-0.1.3 (c (n "wait_group") (v "0.1.3") (h "1vj2xf38n1shj3sswb6j10g6xw612jk24yi298bmh5qm4afqfa1c")))

(define-public crate-wait_group-0.1.4 (c (n "wait_group") (v "0.1.4") (h "1889lnyb69w6kknbzpb3w4j596in99405sv4z0x5jahjmkdcyc9b")))

