(define-module (crates-io wa it wait-list) #:use-module (crates-io))

(define-public crate-wait-list-0.1.0 (c (n "wait-list") (v "0.1.0") (d (list (d (n "lock_api_04_crate") (r "^0.4.7") (o #t) (d #t) (k 0) (p "lock_api")) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 2)) (d (n "pinned-aliasable") (r "^0.1.2") (d #t) (k 0)))) (h "0bfxd5ssnvfsi7svyb9fff60vvcpni0qln5ns55c7vvgs109zcyv") (f (quote (("std" "alloc") ("lock_api_04" "lock_api_04_crate") ("alloc")))) (r "1.56.0")))

(define-public crate-wait-list-0.1.1 (c (n "wait-list") (v "0.1.1") (d (list (d (n "lock_api_04_crate") (r "^0.4.7") (o #t) (d #t) (k 0) (p "lock_api")) (d (n "loom_05_crate") (r "^0.5.4") (o #t) (d #t) (k 0) (p "loom")) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 2)) (d (n "pinned-aliasable") (r "^0.1.3") (d #t) (k 0)))) (h "10610zinv9c3vz0cr4lkvdmjh7hrk6fh24gq7706bk3mb5xk5m6c") (f (quote (("std" "alloc") ("loom_05" "loom_05_crate") ("lock_api_04" "lock_api_04_crate") ("alloc")))) (r "1.56.0")))

