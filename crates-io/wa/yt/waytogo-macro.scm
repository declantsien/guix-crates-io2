(define-module (crates-io wa yt waytogo-macro) #:use-module (crates-io))

(define-public crate-waytogo-macro-0.1.0 (c (n "waytogo-macro") (v "0.1.0") (d (list (d (n "enum-as-inner") (r "^0.5") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quick-xml") (r "^0.23") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rust-format") (r "^0.3") (f (quote ("post_process" "token_stream"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "proc-macro"))) (d #t) (k 0)))) (h "0kl7vy18r3dac4agza5mdy3mfw1yj65shp0ivcfgvr58gl58aw17")))

