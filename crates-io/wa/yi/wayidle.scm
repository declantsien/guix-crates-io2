(define-module (crates-io wa yi wayidle) #:use-module (crates-io))

(define-public crate-wayidle-0.1.0 (c (n "wayidle") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.10.4") (d #t) (k 0)) (d (n "wayrs-protocols") (r "^0.10.5") (f (quote ("ext-idle-notify-v1"))) (d #t) (k 0)) (d (n "wayrs-utils") (r "^0.6.0") (f (quote ("seats"))) (d #t) (k 0)))) (h "1s1z90yc6pnrgsxj32lfwfxv2h1lhczgba1p81k8z7y9hh8lz6ch")))

(define-public crate-wayidle-0.1.1 (c (n "wayidle") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "lexopt") (r "^0.3.0") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.10.4") (d #t) (k 0)) (d (n "wayrs-protocols") (r "^0.10.5") (f (quote ("ext-idle-notify-v1"))) (d #t) (k 0)) (d (n "wayrs-utils") (r "^0.6.0") (f (quote ("seats"))) (d #t) (k 0)))) (h "05i1yxv0z3c6ffrkm46qaha36x3kw9k3lzaifvl31jpm4p4n9rjy")))

