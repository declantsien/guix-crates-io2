(define-module (crates-io wa mr wamr-sys) #:use-module (crates-io))

(define-public crate-wamr-sys-0.1.0 (c (n "wamr-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1a7nbi8hfdx3i5v6sswdz4yvz6dm22l2m547bd505va9328mbjny") (l "iwasm")))

(define-public crate-wamr-sys-0.1.1 (c (n "wamr-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1l210gcb4n17k8v3a47yk4lqlz7mrf66s3d27q05fyy8ng61k1bz") (l "iwasm")))

