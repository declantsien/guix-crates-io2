(define-module (crates-io wa ge wagen) #:use-module (crates-io))

(define-public crate-wagen-0.1.0 (c (n "wagen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "extism") (r "^1") (o #t) (d #t) (k 0)) (d (n "extism-manifest") (r "^1") (o #t) (d #t) (k 0)) (d (n "wasm-encoder") (r "^0.200.0") (f (quote ("wasmparser"))) (d #t) (k 0)) (d (n "wasmparser") (r "^0.200.0") (d #t) (k 0)) (d (n "wasmtime") (r ">=16.0.0, <19.0.0") (o #t) (d #t) (k 0)))) (h "0ndcqqqxzcbhc5n2ngp12kl4y6c15y9gc5pm1vhdi5w5la25i288") (f (quote (("default")))) (s 2) (e (quote (("extism" "dep:extism" "dep:extism-manifest"))))))

