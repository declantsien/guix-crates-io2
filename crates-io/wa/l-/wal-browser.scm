(define-module (crates-io wa l- wal-browser) #:use-module (crates-io))

(define-public crate-wal-browser-0.1.0 (c (n "wal-browser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "05gyplh4i7546w0r1f3av61pylqafa17m03cmlkrwcahm1xp134f")))

(define-public crate-wal-browser-0.1.1 (c (n "wal-browser") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1i8lfh546khmy3c13ysyr5apgblwgivf5dib331m0jsam3sm5g8v")))

(define-public crate-wal-browser-0.1.2 (c (n "wal-browser") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0alq0nl3h6l73h84cc6skvjrraxddfcddqv0dj719x8gcyz27gsh")))

(define-public crate-wal-browser-0.1.3 (c (n "wal-browser") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1sh4bl4z8067l44fqxw7lcrvcd5mp0pcjpn8b4378piipk4gswsi")))

