(define-module (crates-io wa l- wal-rs) #:use-module (crates-io))

(define-public crate-wal-rs-0.1.0 (c (n "wal-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "1qqjmvx353r4xfi7kf1j87r9bqn12s4wb0q2mvvbbmazkh1mb299")))

(define-public crate-wal-rs-1.0.0 (c (n "wal-rs") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "03ny8gcl01sxqv7133cg6p6l8qii5ylz0hkn4gx5g36xj1pkfl9i")))

(define-public crate-wal-rs-1.0.1 (c (n "wal-rs") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0g2vdmgi6gc628b423xxzig1v2vmml3rk0hm62xm8m7p7ifn9k00")))

(define-public crate-wal-rs-1.0.2 (c (n "wal-rs") (v "1.0.2") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "crc") (r "^1.8") (d #t) (k 0)) (d (n "fs2") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 2)))) (h "0m0a7fmx9j4gxlrdbrxx1i5qvm6pjprc89d2qg5ylhl62rakp548")))

