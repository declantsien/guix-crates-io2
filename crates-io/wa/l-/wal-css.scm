(define-module (crates-io wa l- wal-css) #:use-module (crates-io))

(define-public crate-wal-css-0.1.0 (c (n "wal-css") (v "0.1.0") (d (list (d (n "gloo") (r "^0.10.0") (f (quote ("utils"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Document" "Element" "Node" "Text" "Window"))) (d #t) (k 0)))) (h "182gs3glvvzk493bzibs6ig2j6qa6xdnrc7ki2w2klgppvay1wpv")))

(define-public crate-wal-css-0.1.1 (c (n "wal-css") (v "0.1.1") (d (list (d (n "gloo") (r "^0.10.0") (f (quote ("utils"))) (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("Document" "Element" "Node" "Text" "Window"))) (d #t) (k 0)))) (h "19awnk43w6p2ngxyrdx1hcfyl4k76jypnkw7dbrjipwfxfvhcmz5")))

