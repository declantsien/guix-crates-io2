(define-module (crates-io wa yr wayrs-scanner) #:use-module (crates-io))

(define-public crate-wayrs-scanner-0.1.0 (c (n "wayrs-scanner") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "008c5yyv59gajkjssmdwzj19l50imzvg0a5h82zjg6y38c2fkhl8") (y #t)))

(define-public crate-wayrs-scanner-0.1.1 (c (n "wayrs-scanner") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0k0170fklgi19wj223jr6w5im2sj8qy21m6an3wfqgjslrc1zsxd") (y #t)))

(define-public crate-wayrs-scanner-0.1.2 (c (n "wayrs-scanner") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "14c4jjlbc4rgm9h89fbqzygm58xjdwdink509hd2ym9gyqaq4d8k")))

(define-public crate-wayrs-scanner-0.2.0 (c (n "wayrs-scanner") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.26") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "05xmh2k80v2yvkzfzfklcd9xhgjfi6fg1imdzip3y7hnc5b12mrr")))

(define-public crate-wayrs-scanner-0.3.0 (c (n "wayrs-scanner") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1vz7zl2y5h4pxqiafynxs1ir0x91y2pff16a9rs7i6z888p1r4qc")))

(define-public crate-wayrs-scanner-0.4.0 (c (n "wayrs-scanner") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0f998d8h2iarrna3yn8gxw5shsypik01yccib1s5vkgqg1zp1mwj")))

(define-public crate-wayrs-scanner-0.6.0 (c (n "wayrs-scanner") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0g5dsqic21vl3j283jda0dzpndr0hkj059lasy9s3a5argrki13q")))

(define-public crate-wayrs-scanner-0.7.0 (c (n "wayrs-scanner") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0jsi4rb1s3dc0carzjri9bwxrfpg8dvpfdf47igb9khcpcdvly0g")))

(define-public crate-wayrs-scanner-0.8.0 (c (n "wayrs-scanner") (v "0.8.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1461nq6avaysvi1sj8fq8lgw267vfadn6yvapwz5b3xhh8iryzln")))

(define-public crate-wayrs-scanner-0.9.0 (c (n "wayrs-scanner") (v "0.9.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0kyjp3y18xprw373qfgxrjmf8zrswydy0zvp7mn0jaa75vsl1g4p")))

(define-public crate-wayrs-scanner-0.10.0 (c (n "wayrs-scanner") (v "0.10.0") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0qfydcqkj447h2fxyr9n4hqj1xlb006l8l7x4rhqaqwqfjgjsvxn")))

(define-public crate-wayrs-scanner-0.10.1 (c (n "wayrs-scanner") (v "0.10.1") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0a006gpvwndhyc321vnkh0f0vrgjm3xl5yaldvq5lx57zm90319d")))

(define-public crate-wayrs-scanner-0.10.2 (c (n "wayrs-scanner") (v "0.10.2") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0hccjyjcwaj8n6qk4gkgln6xgxh9p0bf97k1gxkzdwhins9wbjmr") (y #t)))

(define-public crate-wayrs-scanner-0.10.3 (c (n "wayrs-scanner") (v "0.10.3") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.28") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "148nnzx6mqf26898xr62zbgjvh5ckkxvy6d1g3245chh0109r6ja")))

(define-public crate-wayrs-scanner-0.10.4 (c (n "wayrs-scanner") (v "0.10.4") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "04c9344syy4vdvfj3ldlv19v1g1riyh141mppj6bk2c5kpv2vqxi") (r "1.65")))

(define-public crate-wayrs-scanner-0.10.5 (c (n "wayrs-scanner") (v "0.10.5") (d (list (d (n "convert_case") (r "^0.6") (d #t) (k 0)) (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "16bs0l747609jdqsxkb9hynawbs9r6m461jdsasfh4wxxkisarw2") (r "1.65")))

(define-public crate-wayrs-scanner-0.11.0 (c (n "wayrs-scanner") (v "0.11.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "085hmlh8ycapiz9ia7wxa6i51blzadp1dkb1jmlyn78h6jd4db8s") (r "1.65")))

(define-public crate-wayrs-scanner-0.11.1 (c (n "wayrs-scanner") (v "0.11.1") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1nlzljlyw3nqv0b7m89d3h7812ypizmhq1056gnqgkp17zj81c0k") (r "1.65")))

(define-public crate-wayrs-scanner-0.12.0 (c (n "wayrs-scanner") (v "0.12.0") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1qggcdhbnssg0s4rn67w6krwv00wbn2sjmxk540jp0mwmzlcvnzr") (r "1.65")))

(define-public crate-wayrs-scanner-0.12.1 (c (n "wayrs-scanner") (v "0.12.1") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0476j9lmf0xpg4fh63yicz4zhga5w3m8ywy7dgin5x5qsrr9v4cz") (r "1.66")))

(define-public crate-wayrs-scanner-0.12.2 (c (n "wayrs-scanner") (v "0.12.2") (d (list (d (n "proc-macro-crate") (r "^1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "19x9w1vz3nlc7wkjdqdyhhqj6cq13lrkh23dj3vyikd3glaz9rnp") (r "1.66")))

(define-public crate-wayrs-scanner-0.12.3 (c (n "wayrs-scanner") (v "0.12.3") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0kmsm4326062iri8bkv05xcf7vbybqp1cn1yfh4vz2qzsjl7jrr7") (r "1.66")))

(define-public crate-wayrs-scanner-0.12.4 (c (n "wayrs-scanner") (v "0.12.4") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "0l5ywra9wim5vlrwwjh3zdyq5acymxq628dw03dhdybbcfhvwrhz") (r "1.67")))

(define-public crate-wayrs-scanner-0.12.5 (c (n "wayrs-scanner") (v "0.12.5") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1iq6dbhdgk5lh1814352979hl6fz7dm2f1cv997p16nrnwvyb9m7") (r "1.67")))

(define-public crate-wayrs-scanner-0.12.6 (c (n "wayrs-scanner") (v "0.12.6") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "17y5q32lcw88mpfq24g5sqg9ssm5szank3w8sln7klw5f3dzlql4") (r "1.67")))

(define-public crate-wayrs-scanner-0.12.7 (c (n "wayrs-scanner") (v "0.12.7") (d (list (d (n "proc-macro-crate") (r "^2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)))) (h "1v3q0i2zca6zir8r8m1s6f6d4z69wvkw8p7nx150sxi4fxccf3hc") (r "1.67")))

(define-public crate-wayrs-scanner-0.13.0 (c (n "wayrs-scanner") (v "0.13.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "wayrs-proto-parser") (r "^1.0") (d #t) (k 0)))) (h "1jccs59ip3213gj45fafqm7cmg01mwza1ap79mhicsb9q3wnpgwp") (r "1.67")))

(define-public crate-wayrs-scanner-0.13.1 (c (n "wayrs-scanner") (v "0.13.1") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "wayrs-proto-parser") (r "^1.0") (d #t) (k 0)))) (h "15hmwy5rw52j70in9q3ik1kgaj5g3dkn2hq8k3ad6srqayqig7f9") (r "1.67")))

(define-public crate-wayrs-scanner-0.13.2 (c (n "wayrs-scanner") (v "0.13.2") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "wayrs-proto-parser") (r "^2.0") (d #t) (k 0)))) (h "07xzg36rnnsb4z4rd82r2mk3y05vg1ssfwrry2kd4yz395sx91z3") (r "1.67")))

(define-public crate-wayrs-scanner-0.14.0 (c (n "wayrs-scanner") (v "0.14.0") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "wayrs-proto-parser") (r "^2.0") (d #t) (k 0)))) (h "1l716fx3bg0yfz71jyvmr2dxhca349fzgvywc1igmv8jqwdircjr") (r "1.72")))

(define-public crate-wayrs-scanner-0.14.1 (c (n "wayrs-scanner") (v "0.14.1") (d (list (d (n "proc-macro-crate") (r "^3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("parsing" "proc-macro"))) (k 0)) (d (n "wayrs-proto-parser") (r "^2.0") (d #t) (k 0)))) (h "0z9d5wn2yh3yj53xywqwlcs3hib4gf7kxsfq1sdmdxsflgy0bcbj") (r "1.72")))

