(define-module (crates-io wa yr wayrs-cursor) #:use-module (crates-io))

(define-public crate-wayrs-cursor-0.4.0-beta.0 (c (n "wayrs-cursor") (v "0.4.0-beta.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.3.0") (d #t) (k 0)) (d (n "wayrs-shm-alloc") (r "^0.4.0-beta.0") (d #t) (k 0)) (d (n "xcursor") (r "^0.3") (d #t) (k 0)))) (h "1ghc2y84xgqg9jf9cs1li45q00y8k6k9zwx9bccgzipld5wrz1y1") (y #t)))

