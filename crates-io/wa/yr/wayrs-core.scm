(define-module (crates-io wa yr wayrs-core) #:use-module (crates-io))

(define-public crate-wayrs-core-0.1.0 (c (n "wayrs-core") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "011cldy42bshz3bj7lqzdvahbmnfap3n7y319rqnzn9hvby74yfr") (r "1.69")))

(define-public crate-wayrs-core-1.0.0 (c (n "wayrs-core") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cj0w5xvqzvr9dqvzyrbpg2wlvhrjyznvrsmfhv3yhhjqm2snga4") (r "1.72")))

(define-public crate-wayrs-core-1.0.1 (c (n "wayrs-core") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0g8rh99x5yflz4ifa40c3m5jyy1nwsxng2s04lwns9nyv8fflal8") (r "1.72")))

(define-public crate-wayrs-core-1.0.2 (c (n "wayrs-core") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "10q8khdxkp3brqjj800g87qa7a5d0yw15zx35msdynibwk3mjk29") (r "1.72")))

