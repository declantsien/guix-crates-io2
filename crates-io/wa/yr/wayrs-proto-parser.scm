(define-module (crates-io wa yr wayrs-proto-parser) #:use-module (crates-io))

(define-public crate-wayrs-proto-parser-1.0.0 (c (n "wayrs-proto-parser") (v "1.0.0") (d (list (d (n "quick-xml") (r "^0.31") (d #t) (k 0)))) (h "0pc7vryhwvkdznz3rmkwkg785lklyqz682ycgxrd69h9z1lz9hxd") (r "1.67")))

(define-public crate-wayrs-proto-parser-1.0.1 (c (n "wayrs-proto-parser") (v "1.0.1") (d (list (d (n "quick-xml") (r "^0.31") (d #t) (k 0)))) (h "036w98qyjscm0rls6nwfx6hyl5zg90y3j0bcrkx459r23xs66srx") (r "1.67")))

(define-public crate-wayrs-proto-parser-2.0.0 (c (n "wayrs-proto-parser") (v "2.0.0") (d (list (d (n "quick-xml") (r "^0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1a9gb0iv6rm1z2n4isfb9x05pv6wlgn23hljb9s038m4dgmyn1hl") (r "1.67")))

(define-public crate-wayrs-proto-parser-2.0.1 (c (n "wayrs-proto-parser") (v "2.0.1") (d (list (d (n "quick-xml") (r "^0.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "136bmn2nj97z7nbfbgg01gv0sy9dx79bdcjyc6hqdpln38sddbyk") (r "1.72")))

