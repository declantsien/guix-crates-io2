(define-module (crates-io wa yr wayrs-shm-alloc) #:use-module (crates-io))

(define-public crate-wayrs-shm-alloc-0.1.0 (c (n "wayrs-shm-alloc") (v "0.1.0") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "shmemfdrs") (r "^0.1") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.1.0") (d #t) (k 0)))) (h "0vxi842jsvlc1j3k4lxyzn5vqxh4r481xnl0gv0h46dq3g9lhi8j") (y #t)))

(define-public crate-wayrs-shm-alloc-0.2.0 (c (n "wayrs-shm-alloc") (v "0.2.0") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "shmemfdrs") (r "^0.1") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.2.0") (d #t) (k 0)))) (h "0y1cgi85glpd1q2yybliapwpxs94lzwhb07a4p88p3ml3z2i2f3y") (y #t)))

(define-public crate-wayrs-shm-alloc-0.3.0 (c (n "wayrs-shm-alloc") (v "0.3.0") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "shmemfdrs") (r "^0.1") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.3.0") (d #t) (k 0)))) (h "0r6zhkb1vm022f79bdkz9iznc5a3d77vgyhr6b21zgk3crrp977b") (y #t)))

(define-public crate-wayrs-shm-alloc-0.4.0-beta.0 (c (n "wayrs-shm-alloc") (v "0.4.0-beta.0") (d (list (d (n "memmap2") (r "^0.5") (d #t) (k 0)) (d (n "shmemfdrs") (r "^0.1") (d #t) (k 0)) (d (n "wayrs-client") (r "^0.3.0") (d #t) (k 0)))) (h "0ml88svyqvs7k5jvqs5l03n56x3jbyvdmqrr7g1k28pr08d95jls") (y #t)))

