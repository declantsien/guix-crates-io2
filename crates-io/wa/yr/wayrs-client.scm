(define-module (crates-io wa yr wayrs-client) #:use-module (crates-io))

(define-public crate-wayrs-client-0.1.0 (c (n "wayrs-client") (v "0.1.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.1.0") (d #t) (k 0)))) (h "16nf3352jdw096vq4zykdwrk6144aq20vixzg3ybh671p5qk4n0f")))

(define-public crate-wayrs-client-0.2.0 (c (n "wayrs-client") (v "0.2.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.2.0") (d #t) (k 0)))) (h "0q7kjrgw2h5lj421aki8ds1bxbmj9b7rq6dziycy7a3hbr0n1vjl") (y #t)))

(define-public crate-wayrs-client-0.2.1 (c (n "wayrs-client") (v "0.2.1") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.2.0") (d #t) (k 0)))) (h "1b3y8br19fxqhryi4yc312vmbsa9bqjc0rjkm8i0mphsl17mwwrn")))

(define-public crate-wayrs-client-0.3.0 (c (n "wayrs-client") (v "0.3.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.3.0") (d #t) (k 0)))) (h "1w0yp5g35m386kd4v15n192ypvxwprilv6imjzcrs0h16rqd8h8a")))

(define-public crate-wayrs-client-0.3.1 (c (n "wayrs-client") (v "0.3.1") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.3.0") (d #t) (k 0)))) (h "0cnlg14jjwnwjwyg0v57fxsx8hbg2y5pznv8mjv6c3gj1x917g9b")))

(define-public crate-wayrs-client-0.4.0 (c (n "wayrs-client") (v "0.4.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.4") (d #t) (k 0)))) (h "1lsfw3sah5dhvzk8yzadhjqiqi703ng9kgj9ragpvb686qa6434l") (y #t)))

(define-public crate-wayrs-client-0.4.1 (c (n "wayrs-client") (v "0.4.1") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.4") (d #t) (k 0)))) (h "12vfxmydgz5wlnd70pp8frqv637zildv1ghardiasqdsiliv7dbq")))

(define-public crate-wayrs-client-0.5.0 (c (n "wayrs-client") (v "0.5.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.4") (d #t) (k 0)))) (h "01r758h2kcw6js2kf8g1y23hzvn3bhr8ph42kiagc2sc3b08kfcx")))

(define-public crate-wayrs-client-0.6.0 (c (n "wayrs-client") (v "0.6.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.6") (d #t) (k 0)))) (h "1a1ncd336ksndvpxnb7l1czclzkwwzkqvd0g1hxhb6sy8drsqmrr")))

(define-public crate-wayrs-client-0.7.0 (c (n "wayrs-client") (v "0.7.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.7") (d #t) (k 0)))) (h "0smrz737sprzzkvph3dg2s99k231s0d04dn1bvi8vfyh7ffkyhgd")))

(define-public crate-wayrs-client-0.8.0 (c (n "wayrs-client") (v "0.8.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.8") (d #t) (k 0)))) (h "1sack4pkaikzkax5dlf5m2rip3rcsrv9f6wgwr5ljgvdiryi52h9") (y #t)))

(define-public crate-wayrs-client-0.9.0 (c (n "wayrs-client") (v "0.9.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.9") (d #t) (k 0)))) (h "0b89x9mfpiln7xz0wsxiqqbs7sfzjkq2k17q4269jq2xhsdm1jks") (y #t)))

(define-public crate-wayrs-client-0.10.0 (c (n "wayrs-client") (v "0.10.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.10") (d #t) (k 0)))) (h "13pjs31v08jb1c6dc5cpj3pqb41kfq2dixax0y8if7p5v0a3gmqz") (y #t)))

(define-public crate-wayrs-client-0.10.1 (c (n "wayrs-client") (v "0.10.1") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.10") (d #t) (k 0)))) (h "14axkq6a7561wsial8x14b2zsyxwf66jn5nn5gszcqlpii1w3zva") (y #t)))

(define-public crate-wayrs-client-0.10.2 (c (n "wayrs-client") (v "0.10.2") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.10") (d #t) (k 0)))) (h "14sbjh0cf8danq6kvlrjg25zhmsyqibqzjjk5njnw2v8b0gql3d0")))

(define-public crate-wayrs-client-0.10.3 (c (n "wayrs-client") (v "0.10.3") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.10") (d #t) (k 0)))) (h "0lpmdq2y0hmkykybs2g9w4132r098n782z2ykxi5rcmpzaqrxwkd")))

(define-public crate-wayrs-client-0.10.4 (c (n "wayrs-client") (v "0.10.4") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.10.2") (d #t) (k 0)))) (h "0av9hkqc9pggcn12jhszphx67kym4csj3ikcqb86jdd6wbbg9zwf")))

(define-public crate-wayrs-client-0.10.5 (c (n "wayrs-client") (v "0.10.5") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.10.4") (d #t) (k 0)))) (h "05gpgvy6rnqfz510kimxglvyrzgz4nqfvjfylgicglp1fp2qjvsb") (r "1.65")))

(define-public crate-wayrs-client-0.11.0 (c (n "wayrs-client") (v "0.11.0") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.11") (d #t) (k 0)))) (h "1rqa7qxdnf1arm0is2m05q62dx5ak5gscagx4nv6lzlaji0m5hv4") (r "1.65")))

(define-public crate-wayrs-client-0.11.1 (c (n "wayrs-client") (v "0.11.1") (d (list (d (n "nix") (r "^0.26") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.11") (d #t) (k 0)))) (h "0c8ax8bcy07wy5x86jyp9jp8mcacnfa0wri2lh89v5wkiy2cacxn") (r "1.65")))

(define-public crate-wayrs-client-0.12.0 (c (n "wayrs-client") (v "0.12.0") (d (list (d (n "nix") (r "^0.27") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.12") (d #t) (k 0)))) (h "0xpsgcjqgv7s519ibdpm375nsbkrmx3n7y0zgvszj096hcpvmb74") (r "1.65")))

(define-public crate-wayrs-client-0.12.1 (c (n "wayrs-client") (v "0.12.1") (d (list (d (n "nix") (r "^0.27") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.12") (d #t) (k 0)))) (h "187vrmgs8zmbpmbgw5j2b8k77ln6jq0ldb2m934v0y7pxknyngdy") (r "1.66")))

(define-public crate-wayrs-client-0.12.2 (c (n "wayrs-client") (v "0.12.2") (d (list (d (n "nix") (r "^0.27") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.12") (d #t) (k 0)))) (h "0k8wd8msz2bscl8w1j2gn6kcaiwdsby2gnqcql67mrsbb8bjy75v") (r "1.66")))

(define-public crate-wayrs-client-0.12.3 (c (n "wayrs-client") (v "0.12.3") (d (list (d (n "nix") (r "^0.27") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.12") (d #t) (k 0)))) (h "14bwizni5knyw4db2wg21kiypxqfk32xq159xhgqgn1aj9crsg6c") (r "1.67")))

(define-public crate-wayrs-client-0.12.4 (c (n "wayrs-client") (v "0.12.4") (d (list (d (n "nix") (r "^0.27") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.12") (d #t) (k 0)))) (h "0ajfmflzks8hpq8gzxgp990qlgl0g053i8840yahmmw0jjb57c43") (r "1.67")))

(define-public crate-wayrs-client-1.0.0 (c (n "wayrs-client") (v "1.0.0") (d (list (d (n "nix") (r "^0.27") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.12") (d #t) (k 0)))) (h "07xsl86pg9wdhr3q0kl6mm5hrygjnw19i79z1g2qvjkp40hnb1dm") (r "1.67")))

(define-public crate-wayrs-client-1.0.1 (c (n "wayrs-client") (v "1.0.1") (d (list (d (n "nix") (r "^0.27") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.12") (d #t) (k 0)))) (h "0vqwbmf10makkm45iy1v1hnwzzdb836ci87rn7r0z0wlbvkgfr9p") (r "1.67")))

(define-public crate-wayrs-client-1.0.2 (c (n "wayrs-client") (v "1.0.2") (d (list (d (n "nix") (r "^0.27") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.13") (d #t) (k 0)))) (h "18i19b7z4wp0mjwn256ps3pdfk47lx45h0cx8wkjlv7akkgzxnj7") (r "1.67")))

(define-public crate-wayrs-client-1.0.3 (c (n "wayrs-client") (v "1.0.3") (d (list (d (n "nix") (r "^0.28") (f (quote ("socket" "uio"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-scanner") (r "^0.13") (d #t) (k 0)))) (h "1biv8g9lywbd5bkpnh3wwcgx8vwscci1rckiyj35s2i63a24nf5f") (r "1.69")))

(define-public crate-wayrs-client-1.1.0 (c (n "wayrs-client") (v "1.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-core") (r "^1.0") (d #t) (k 0)) (d (n "wayrs-scanner") (r "^0.14") (d #t) (k 0)))) (h "13g57nxcpwl504vs82smwca209wgi3jvp5jcy0dfaar3a15rpzn2") (r "1.72")))

(define-public crate-wayrs-client-1.1.1 (c (n "wayrs-client") (v "1.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net"))) (o #t) (k 0)) (d (n "wayrs-core") (r "^1.0") (d #t) (k 0)) (d (n "wayrs-scanner") (r "^0.14") (d #t) (k 0)))) (h "0hb440f5d1hwy8kzhavx7543zw336fzkphmd3ffwa4yj6hnn8dwj") (r "1.72")))

