(define-module (crates-io wa rd ward) #:use-module (crates-io))

(define-public crate-ward-1.0.0 (c (n "ward") (v "1.0.0") (h "13xl910bwdqvxrwz6ifpkgn57pi5g5rhv9szm2z81kmyc55jhrhx")))

(define-public crate-ward-1.0.1 (c (n "ward") (v "1.0.1") (h "16w77nj9mgqmkryzrsmgfs592dv863yq9p97brfvccr2s547h4fk")))

(define-public crate-ward-1.0.2 (c (n "ward") (v "1.0.2") (h "0yz369wnrd7dmbyahmv64xclv115ci8dcwcxd90n093gihk7cww1")))

(define-public crate-ward-1.1.0 (c (n "ward") (v "1.1.0") (h "1ynzraz5mpfsygkvlbjz5j3wlan2325r34ybm2dlj62gj8q0jph8")))

(define-public crate-ward-2.0.0 (c (n "ward") (v "2.0.0") (h "020bdd4vcd18n6wc89ij9f92h722bs5fy7chcwcvxc7fh12ky4wl")))

(define-public crate-ward-2.1.0 (c (n "ward") (v "2.1.0") (h "07mn4wqh3cpgihir128jcfczrqrfxn1181ih2mx31q86sq4xdg0w")))

