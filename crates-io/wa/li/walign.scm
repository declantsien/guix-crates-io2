(define-module (crates-io wa li walign) #:use-module (crates-io))

(define-public crate-walign-0.1.0 (c (n "walign") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1xmxlsj8av83kdnnma17gpymfa9xs0q0ixsmspz4v464vnm1vm6r")))

(define-public crate-walign-0.1.1 (c (n "walign") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0jw254hjm598js04516qvmi1ph7xa1zznycnzrsikck4n5m8ymbx")))

(define-public crate-walign-0.1.2 (c (n "walign") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0pc492268n495p3avpd2w5wlq4wrvfqc9d3y3ibjydhifn8rdk99")))

(define-public crate-walign-0.1.3 (c (n "walign") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1x9qyblly9l1qajjny1kvi39bdk46x5zir24xx7hdppwc007kcd8")))

(define-public crate-walign-0.1.4 (c (n "walign") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1qp45ziy66yrzikn8yr2wq5yrbrazrvmd29gw97ka79fqdjmnw8z")))

(define-public crate-walign-0.1.5 (c (n "walign") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0a084i78lvw8xky5sdk2nv627syyxmdg5s83c3d4cd2by2cmy79h")))

(define-public crate-walign-0.1.6 (c (n "walign") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1mffhgi4a1lbvgm46222srn4ipdffzwrib7hghwq0iv91jlx3xbk")))

