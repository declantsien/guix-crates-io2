(define-module (crates-io wa nt wantora) #:use-module (crates-io))

(define-public crate-wantora-0.1.0 (c (n "wantora") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "notify-debouncer-full") (r "^0.3.1") (d #t) (k 0)) (d (n "text_placeholder") (r "^0.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "1wyxjsp0d3xpcnh55mn3w60d9nrdlqri6l3g5kf2mmw7vwppsn6a")))

(define-public crate-wantora-0.1.1 (c (n "wantora") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "notify-debouncer-full") (r "^0.3.1") (d #t) (k 0)) (d (n "text_placeholder") (r "^0.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "1828jvrmv1qkgpqf77fldqp6fvrpx7sj6b3kay554g0ndg7qs36s")))

(define-public crate-wantora-0.1.2 (c (n "wantora") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "notify-debouncer-full") (r "^0.3.1") (d #t) (k 0)) (d (n "text_placeholder") (r "^0.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.7.0") (f (quote ("v4"))) (d #t) (k 2)))) (h "07j0drcp9xjk2raqj8b8lg32k80z94k0nv3mrl58wyk7bh7s4k8y")))

