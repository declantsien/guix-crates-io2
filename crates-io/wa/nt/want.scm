(define-module (crates-io wa nt want) #:use-module (crates-io))

(define-public crate-want-0.0.0 (c (n "want") (v "0.0.0") (h "1vyshi083nxaasiyfxaysdmpcqqnqjdgdp1rzfi3cbi3nx4y0fgj")))

(define-public crate-want-0.0.1 (c (n "want") (v "0.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "try-lock") (r "^0.1") (d #t) (k 0)))) (h "1h8x25gac1m3ssr01z70rw2mjkfp6iahy9krjxyjjvhyi1ady6fn")))

(define-public crate-want-0.0.2 (c (n "want") (v "0.0.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "try-lock") (r "^0.1") (d #t) (k 0)))) (h "13lp28xm3h992x3q622gfhgfk278j7szy5rbvaf47ng4fkx13jxr")))

(define-public crate-want-0.1.0 (c (n "want") (v "0.1.0") (d (list (d (n "futures") (r "^0.2") (d #t) (k 2)) (d (n "futures-core") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "try-lock") (r "^0.1") (d #t) (k 0)))) (h "0fp96bfprd2c7xvlpkq2px3dp05syk7p59haqapvkqwm38ybrgj7") (y #t)))

(define-public crate-want-0.0.3 (c (n "want") (v "0.0.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "try-lock") (r "^0.1") (d #t) (k 0)))) (h "1229z0djv0cd7isd7rv2l2v4z485w19jrl3jzbqk6myr4lv1c0nw")))

(define-public crate-want-0.0.4 (c (n "want") (v "0.0.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "try-lock") (r "^0.1") (d #t) (k 0)))) (h "1l9mbh4a0r2m3s8nckhy1vz9qm6lxsswlgxpimf4pyjkcyb9spd0")))

(define-public crate-want-0.0.5 (c (n "want") (v "0.0.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "try-lock") (r "^0.2") (d #t) (k 0)))) (h "0j937ydgkya10qzljp4hyc1jxsvxq92byvfna14x73p1jfay1zrg")))

(define-public crate-want-0.0.6 (c (n "want") (v "0.0.6") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "try-lock") (r "^0.2") (d #t) (k 0)))) (h "1cznvlky1zbxhwlajdi0kvwq3ma8wsmalaf51j1vip9hbx3n8x3r")))

(define-public crate-want-0.2.0 (c (n "want") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "try-lock") (r "^0.2") (d #t) (k 0)))) (h "0c52g7b4hhj033jc56sx9z3krivyciz0hlblixq2gc448zx5wfdn")))

(define-public crate-want-0.3.0 (c (n "want") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-executor") (r "^0.2.0-alpha.2") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.2.0-alpha.2") (d #t) (k 2)) (d (n "try-lock") (r "^0.2") (d #t) (k 0)))) (h "181b2zmwfq389x9n2g1n37cvcvvdand832zz6v8i1l8wrdlaks0w")))

(define-public crate-want-0.3.1 (c (n "want") (v "0.3.1") (d (list (d (n "tokio-executor") (r "^0.2.0-alpha.2") (d #t) (k 2)) (d (n "tokio-sync") (r "^0.2.0-alpha.2") (d #t) (k 2)) (d (n "try-lock") (r "^0.2.4") (d #t) (k 0)))) (h "03hbfrnvqqdchb5kgxyavb9jabwza0dmh2vw5kg0dq8rxl57d9xz")))

