(define-module (crates-io wa ts watson) #:use-module (crates-io))

(define-public crate-watson-0.0.0 (c (n "watson") (v "0.0.0") (d (list (d (n "executor") (r "^0") (d #t) (k 0)) (d (n "malloc") (r "^0.0.1") (d #t) (k 0)) (d (n "nom") (r "^5") (f (quote ("lexical"))) (k 0)) (d (n "webassembly") (r "^0") (d #t) (k 0)))) (h "1kww8h51nnbzvivs4al1rpv3k87s3s9xik2da0wimy4by9nxsa9r")))

(define-public crate-watson-0.0.2 (c (n "watson") (v "0.0.2") (d (list (d (n "nom") (r "^5.1.1") (f (quote ("alloc" "lexical"))) (k 0)) (d (n "webassembly") (r "^0.3.1") (d #t) (k 0)))) (h "1fifgvrb06zx51q88vb1czc7aaw29jnr5q3lgw19vbhvvkxpjgf1")))

(define-public crate-watson-0.0.3 (c (n "watson") (v "0.0.3") (d (list (d (n "webassembly") (r "^0.3.1") (d #t) (k 0)))) (h "1z4gd9va8pmn7whxmv1wvfsgm3xq8cqavb51yfgcb0a0h6f1rxlx")))

(define-public crate-watson-0.0.4 (c (n "watson") (v "0.0.4") (d (list (d (n "webassembly") (r "^0.3.1") (d #t) (k 0)))) (h "0f2v2y1amazfgps4dxm03x7w3ggpwxjv96hwypd01vf1gsvqvzm4")))

(define-public crate-watson-0.0.5 (c (n "watson") (v "0.0.5") (d (list (d (n "webassembly") (r "^0.3.1") (d #t) (k 0)))) (h "0668sfwn7csvpcrfb269yskx18walras3961r780r6d6d2qv538m")))

(define-public crate-watson-0.0.6 (c (n "watson") (v "0.0.6") (d (list (d (n "webassembly") (r "^0.3.1") (d #t) (k 0)))) (h "0pphwf76iy37l19qzxnldjb5x26y3fdbfd5l62v0ji9dnlw2jv2q")))

(define-public crate-watson-0.0.7 (c (n "watson") (v "0.0.7") (d (list (d (n "webassembly") (r "^0.3.1") (d #t) (k 0)))) (h "03qgfdcwmsc54wh2rrjxilm0fib3lwsf1gwwvd8yawy95jn4zq48")))

(define-public crate-watson-0.0.8 (c (n "watson") (v "0.0.8") (d (list (d (n "webassembly") (r "^0.3.1") (d #t) (k 0)))) (h "09cygbkzg8d042aivd725pd54qmmh34bmqmwkc007f7hg2allpcb")))

(define-public crate-watson-0.0.9 (c (n "watson") (v "0.0.9") (d (list (d (n "webassembly") (r "^0.3.1") (d #t) (k 0)))) (h "1whvd29pn5z4km3pw1a920mvnnl4n9ndjy9rw28m0g3gdjrcn61m")))

(define-public crate-watson-0.0.10 (c (n "watson") (v "0.0.10") (d (list (d (n "webassembly") (r "^0.3.1") (d #t) (k 0)))) (h "12dkzkhx7vhgv1vxp4vq28nzm1mlbw83xacm1bw8zsv6bl7v568v")))

(define-public crate-watson-0.1.0 (c (n "watson") (v "0.1.0") (d (list (d (n "webassembly") (r "^0.3.1") (d #t) (k 0)))) (h "0gxdzbi6wb38chbx22l0hhqipb0k4yvqcz85vfn87gjv5wf5msb0")))

(define-public crate-watson-0.2.0 (c (n "watson") (v "0.2.0") (d (list (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "1qhffbw3mmvni2s9gsl5i5xparyw1qs9q61bjsysys99nfghmy55")))

(define-public crate-watson-0.3.0 (c (n "watson") (v "0.3.0") (d (list (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "1affpk8ai94aswklb6gxx9abmsnz2zir9v8syh8awdlacs92dh0g")))

(define-public crate-watson-0.3.1 (c (n "watson") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "0bdnxgl7833wqkmsx2biyn80ycp26lz3n384bx46vbg01yjzyh0z")))

(define-public crate-watson-0.3.2 (c (n "watson") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "12xfba5s1w5vgv8ipxv123rj0x1j14v4zj4b58sbmhb32ml5xppv")))

(define-public crate-watson-0.3.3 (c (n "watson") (v "0.3.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (k 0)) (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "03i6zrmslw8dlz5027cfkhnfa49kkjgyxjl9shq1jfwgl8asf9sb")))

(define-public crate-watson-0.4.0 (c (n "watson") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "0x7p7pzldvifkqlp0f9216rbg4lqiwylpllw556nrk3fkbkpccl0")))

(define-public crate-watson-0.4.1 (c (n "watson") (v "0.4.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "1vylclyf4xc7v7i15q267nhmx5a81cdr24bz9vqxn0n4g2npxshw")))

(define-public crate-watson-0.5.0 (c (n "watson") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "06x8ni4hhqsrpw5x3j1yykivya13dffxj3lvnf3mr3a2047grnbg")))

(define-public crate-watson-0.6.0 (c (n "watson") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "1pqvz0ida576plhx1spg5xjfk2i19npmcx3r30nhilrl7x5waw4s")))

(define-public crate-watson-0.7.0 (c (n "watson") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "0q9gzf0mcpak2z7rnqrbdgn1zbj65y7a1n6i8sxbp0vb9x59g3wf")))

(define-public crate-watson-0.8.0 (c (n "watson") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "webassembly") (r "^0.5.0") (d #t) (k 0)))) (h "0cdfxyn3a6ppnza9k57mk54lb52x3w9gq4kr8fl2slaph5ik2f6i")))

(define-public crate-watson-0.9.0 (c (n "watson") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("alloc" "derive"))) (k 0)) (d (n "webassembly") (r "^0.8.2") (d #t) (k 0)))) (h "1lzc9ssgxkifrxhh7kzjxf2a820p88rn252z8mpladnapw36w0rm")))

(define-public crate-watson-0.9.1 (c (n "watson") (v "0.9.1") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("alloc" "derive"))) (k 0)) (d (n "webassembly") (r "^0.8.2") (d #t) (k 0)))) (h "02fv6zfj9yiwbh75nxjs9k4n6z533rkha147bl5lazjc2yb2a6jd")))

(define-public crate-watson-0.9.2 (c (n "watson") (v "0.9.2") (d (list (d (n "serde") (r "^1.0.116") (f (quote ("alloc" "derive"))) (k 0)) (d (n "spin") (r "^0.5.2") (d #t) (k 0)) (d (n "webassembly") (r "^0.8.2") (d #t) (k 0)))) (h "1nvgrjcfd2rd7c4x2sy9l4zg6f6hg0v34c9rlfpp4k0s507ahqpg")))

