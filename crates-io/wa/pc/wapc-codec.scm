(define-module (crates-io wa pc wapc-codec) #:use-module (crates-io))

(define-public crate-wapc-codec-1.0.0-alpha.0 (c (n "wapc-codec") (v "1.0.0-alpha.0") (d (list (d (n "rmp-serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "067nrggb83051lq51zf85jjjmqmx7dlgibb327cg7lrp5hy87jaj") (f (quote (("messagepack" "rmp-serde") ("default" "messagepack"))))))

(define-public crate-wapc-codec-1.0.0 (c (n "wapc-codec") (v "1.0.0") (d (list (d (n "rmp-serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1lpwi4r22xnlq309rh1sc0i1pnchz67kdkjndnabw57ibv0mnz2l") (f (quote (("messagepack" "rmp-serde") ("default" "messagepack"))))))

(define-public crate-wapc-codec-1.1.0 (c (n "wapc-codec") (v "1.1.0") (d (list (d (n "rmp-serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)))) (h "1mhk3lvqa6p3wwf9qn60bzygbk1wfjab67w7hpik0psci9xjhciz") (f (quote (("messagepack" "rmp-serde") ("default" "messagepack"))))))

