(define-module (crates-io wa pc wapc-guest) #:use-module (crates-io))

(define-public crate-wapc-guest-0.1.0 (c (n "wapc-guest") (v "0.1.0") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "wascap-codec") (r "^0.1.1") (d #t) (k 0)))) (h "13885d87zqicia7j4ihscq22zfxmlh8rw3ghykclfhz9csh2b87k") (y #t)))

(define-public crate-wapc-guest-0.1.1 (c (n "wapc-guest") (v "0.1.1") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0i8px3fdvg33qcrw9wydnmx6448qiv21mc9qnmibxskrmllhhjz5")))

(define-public crate-wapc-guest-0.2.0 (c (n "wapc-guest") (v "0.2.0") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "1qgjbiw4c50srv51hc4z00xn7s1x51fccy1kzlrd0w0pd8500pvf")))

(define-public crate-wapc-guest-0.2.1 (c (n "wapc-guest") (v "0.2.1") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0jm6wc7x6g7ic4dyzvsflkbijyjx23407ysc03hqc4nw1z1db5fi")))

(define-public crate-wapc-guest-0.3.0 (c (n "wapc-guest") (v "0.3.0") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)))) (h "0avd1ns13mib4fgy21kfnyxc913wfbk12gl6d08q68dkk28jh13c")))

(define-public crate-wapc-guest-0.3.1 (c (n "wapc-guest") (v "0.3.1") (h "1q3mlmq0j7xskb07w7kwg5b23ws56cy2y6r175q7py531qq7sdmm")))

(define-public crate-wapc-guest-0.3.2 (c (n "wapc-guest") (v "0.3.2") (h "1ff2kk1p03v13wrk1kna99q38jy6fym2x56c00jlwzk78w09lvyh")))

(define-public crate-wapc-guest-0.4.0 (c (n "wapc-guest") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "02zh2l9xa33idaxz09pych8fhp7jjdpr6y3jg7d8wwdrg3bxkjs7")))

(define-public crate-wapc-guest-1.0.0-alpha.0 (c (n "wapc-guest") (v "1.0.0-alpha.0") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "wapc-codec") (r "^1.0.0-alpha.0") (o #t) (d #t) (k 0)))) (h "1m4psq9ydv8zs9xjsb4k7fwsv8mwj8k89cbn8nc9llgrxkk3v284") (f (quote (("default") ("codec" "wapc-codec"))))))

(define-public crate-wapc-guest-1.0.0 (c (n "wapc-guest") (v "1.0.0") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "wapc-codec") (r "^1.0.0-alpha.0") (o #t) (d #t) (k 0)))) (h "0laq12m86vqjs28qkqfvf32ajjmjn95l50xjm2fxnivn3vymrmai") (f (quote (("default") ("codec" "wapc-codec"))))))

(define-public crate-wapc-guest-1.0.2 (c (n "wapc-guest") (v "1.0.2") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "wapc-codec") (r "^1.0.0-alpha.0") (o #t) (d #t) (k 0)))) (h "094gs8idgncadbwbkp6h9n3jxjg88a5fqv4brd0njsy0dszja5f3") (f (quote (("default") ("codec" "wapc-codec"))))))

(define-public crate-wapc-guest-1.1.0 (c (n "wapc-guest") (v "1.1.0") (d (list (d (n "once_cell") (r "^1.9") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "wapc-codec") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "1340y8zvcbq982zfz563mil5ljzdklpgwilsaq04df1pvff63dsp") (f (quote (("default") ("codec" "wapc-codec"))))))

