(define-module (crates-io wa ta wataash_utils) #:use-module (crates-io))

(define-public crate-wataash_utils-0.1.0 (c (n "wataash_utils") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "16zb4r7h05n4bwpn8l6yx0dzcqlpqbq07cacc2cbr4qxcpb7za2q")))

(define-public crate-wataash_utils-0.1.1 (c (n "wataash_utils") (v "0.1.1") (d (list (d (n "dirs") (r "^3.0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)))) (h "1kywikk916vxz9jc7wjmx4q5z41hpsk6pvwniy5i3qbnzz4lydm3")))

