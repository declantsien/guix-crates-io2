(define-module (crates-io wa po wapo-env) #:use-module (crates-io))

(define-public crate-wapo-env-0.1.0 (c (n "wapo-env") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "scale") (r "^3.6.5") (f (quote ("derive" "std"))) (k 0) (p "parity-scale-codec")) (d (n "tinyvec") (r "^1.5.1") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "wapo-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "wiggle") (r "^19.0.1") (o #t) (d #t) (k 0)))) (h "0r97njfk0jd76nqdr24l2ia96r5bv2f0c9064zvfy2yl57fygcvs") (f (quote (("host" "wiggle") ("default" "host"))))))

