(define-module (crates-io wa po wapo-macro) #:use-module (crates-io))

(define-public crate-wapo-macro-0.1.0 (c (n "wapo-macro") (v "0.1.0") (d (list (d (n "heck") (r "^0.5.0") (d #t) (k 0)) (d (n "insta") (r "^1.14.0") (d #t) (k 2)) (d (n "proc-macro-crate") (r "^3.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "rustfmt-snippet") (r "^0.1.1") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full" "visit-mut" "extra-traits" "parsing"))) (d #t) (k 0)))) (h "0f9g2asmkmhs4m3wf96wa1f4ifcjj4bnl4wmg3k3l91nqdg4l0fz")))

