(define-module (crates-io wa vp wavpack) #:use-module (crates-io))

(define-public crate-wavpack-0.1.0 (c (n "wavpack") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "libc") (r "^0.2.101") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "wavpack-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1i2lcv2h2f4lpn6qzw8szfl44hjaq4r658y8aia2f6v3lssrdmly")))

(define-public crate-wavpack-0.2.0 (c (n "wavpack") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "wavpack-sys")) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0nnfdx5hq8bn3rbm3sh5kp74fbz71wa8a8izx9s290wbcn4kb61j")))

(define-public crate-wavpack-0.2.1 (c (n "wavpack") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "ffi") (r "^0.1.1") (d #t) (k 0) (p "wavpack-sys")) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1l2629p4nq7ihrwah1q015pfsp889mxdsdj8h71al5mf9pqa9w04")))

(define-public crate-wavpack-0.2.2 (c (n "wavpack") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "ffi") (r "^0.1.1") (d #t) (k 0) (p "wavpack-sys")) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0vfcyichb5hzcg06ngfmvx3l2jc5vr6d3zykbkh77k08gmrkyldb")))

(define-public crate-wavpack-0.2.3 (c (n "wavpack") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "ffi") (r "^0.1.2") (d #t) (k 0) (p "wavpack-sys")) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "011nv6kv3wi6yg569zw1216k923gac2ib9qqpwwbag81y2dwg6py")))

(define-public crate-wavpack-0.2.4 (c (n "wavpack") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "ffi") (r "^0.1.2") (d #t) (k 0) (p "wavpack-sys")) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0r2k8qimal5sf6mk1p7kmc4jzhjhh2wr50lwrjpf2sbnpadl37v3")))

(define-public crate-wavpack-0.2.5 (c (n "wavpack") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "ffi") (r "^0.1.3") (d #t) (k 0) (p "wavpack-sys")) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "0p81l8cvsriaxikd7gdn6cvp9ydnj9gdi5g0z204hxqd8r6whdxh")))

(define-public crate-wavpack-0.3.0 (c (n "wavpack") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "ffi") (r "^0.1.4") (d #t) (k 0) (p "wavpack-sys")) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)))) (h "1nx2yy7hlm450mdcvnhzc6ml892yww0bjz22xi3m14h546sqads1")))

(define-public crate-wavpack-0.4.0 (c (n "wavpack") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.29") (d #t) (k 0)) (d (n "wavpack-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0yhl446dqrw7p5fw78r135jhrvnrgklz7ibd5qf0lryv5w0nbp3y")))

