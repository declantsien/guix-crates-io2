(define-module (crates-io wa vp wavpack-sys) #:use-module (crates-io))

(define-public crate-wavpack-sys-0.1.0 (c (n "wavpack-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)))) (h "1zflxm397wn7xjhac7q9myfwqj22wsbgcmsnz9da61hzlf78f4yn")))

(define-public crate-wavpack-sys-0.1.1 (c (n "wavpack-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0zifsbam9s2z29m5rw4wrh8g40jwqgnp18p6nb527ic8lxsbiz2q")))

(define-public crate-wavpack-sys-0.1.2 (c (n "wavpack-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1f6w1av67ds9l8g6v2n432559mp22020ds94vm903h3y3rgac00k")))

(define-public crate-wavpack-sys-0.1.3 (c (n "wavpack-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1dwgam85yl2fk63f9fkhwzgrscak34c2dqfn08aplw62hckipwky")))

(define-public crate-wavpack-sys-0.1.4 (c (n "wavpack-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.69.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1l26ypgmn89mp897a4qanwfsys0px4jrjw2bnzfsdp7vd20h0fjq")))

(define-public crate-wavpack-sys-0.4.0 (c (n "wavpack-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.69.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0xwcwn8si2x81r49x95ascp44xl7ls6isff6wjg7yvkqlyz4xdpf")))

