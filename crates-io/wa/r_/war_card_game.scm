(define-module (crates-io wa r_ war_card_game) #:use-module (crates-io))

(define-public crate-war_card_game-0.1.0 (c (n "war_card_game") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enum-iterator") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "11rbqfyc8ksfa6x4n4srqb9ikp5yjryzx0n4c6k74lz43l1n5bid")))

(define-public crate-war_card_game-0.2.0 (c (n "war_card_game") (v "0.2.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enum-iterator") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0jqgjwh5hkgpnx8lfw3d346djsia21cscvjj42pky4knqr19pqb4")))

(define-public crate-war_card_game-0.2.1 (c (n "war_card_game") (v "0.2.1") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enum-iterator") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0dqy8ivn22ws7qlam25i5kqxq29yi80qpk8rdnvvcdp7xld4nc2z") (y #t)))

(define-public crate-war_card_game-0.2.2 (c (n "war_card_game") (v "0.2.2") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enum-iterator") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1d2jrmym6k7rs8zz0ka1xcbna1025clnzskks5hpqlmc4iyz4078") (y #t)))

(define-public crate-war_card_game-0.2.3 (c (n "war_card_game") (v "0.2.3") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enum-iterator") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1k2xdp69kl4fz4k688gskg58753amjq2cxxbdax9hm4c92v48lm0") (y #t)))

(define-public crate-war_card_game-0.2.4 (c (n "war_card_game") (v "0.2.4") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enum-iterator") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0g116z7226i54w7ma00n4kfl0qq9hnf7a5rpmspdah6hj5pip7qa")))

(define-public crate-war_card_game-0.3.0 (c (n "war_card_game") (v "0.3.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enum-iterator") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g9h13nqa16kyg6mw0hi3abks6cjfjmxm49zhwpxd55mhj2kyyz2")))

(define-public crate-war_card_game-0.3.1 (c (n "war_card_game") (v "0.3.1") (d (list (d (n "clap") (r "^4.2.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "enum-iterator") (r "^1.3.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "14zqp66akc9ykx9hsww7n08mv7gbnsdvhd3svxpypasn3q4ycs1m")))

