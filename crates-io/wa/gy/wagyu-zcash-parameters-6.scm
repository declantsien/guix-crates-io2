(define-module (crates-io wa gy wagyu-zcash-parameters-6) #:use-module (crates-io))

(define-public crate-wagyu-zcash-parameters-6-0.1.0 (c (n "wagyu-zcash-parameters-6") (v "0.1.0") (h "1jaq8rzzsq0bbgcwf47clagp4bwbcagp4378pg09zsp25i46im2a")))

(define-public crate-wagyu-zcash-parameters-6-0.2.0 (c (n "wagyu-zcash-parameters-6") (v "0.2.0") (h "1qgbzs2yibb3zkf2kv7pcwa475d637r31mwwiqcqygnwiakxbdm7")))

