(define-module (crates-io wa gy wagyu-zcash-parameters) #:use-module (crates-io))

(define-public crate-wagyu-zcash-parameters-0.1.0 (c (n "wagyu-zcash-parameters") (v "0.1.0") (d (list (d (n "wagyu-zcash-parameters-1") (r "^0.1.0") (d #t) (k 0)) (d (n "wagyu-zcash-parameters-2") (r "^0.1.0") (d #t) (k 0)) (d (n "wagyu-zcash-parameters-3") (r "^0.1.0") (d #t) (k 0)) (d (n "wagyu-zcash-parameters-4") (r "^0.1.0") (d #t) (k 0)) (d (n "wagyu-zcash-parameters-5") (r "^0.1.0") (d #t) (k 0)) (d (n "wagyu-zcash-parameters-6") (r "^0.1.0") (d #t) (k 0)))) (h "12yhv8m4zpvcm1kzlx472f62hfli0g1nwv3r3pxpn7kx9sc66mwm")))

(define-public crate-wagyu-zcash-parameters-0.2.0 (c (n "wagyu-zcash-parameters") (v "0.2.0") (d (list (d (n "wagyu-zcash-parameters-1") (r "^0.2.0") (d #t) (k 0)) (d (n "wagyu-zcash-parameters-2") (r "^0.2.0") (d #t) (k 0)) (d (n "wagyu-zcash-parameters-3") (r "^0.2.0") (d #t) (k 0)) (d (n "wagyu-zcash-parameters-4") (r "^0.2.0") (d #t) (k 0)) (d (n "wagyu-zcash-parameters-5") (r "^0.2.0") (d #t) (k 0)) (d (n "wagyu-zcash-parameters-6") (r "^0.2.0") (d #t) (k 0)))) (h "1fz2vfav6w0b23a6aglb5n5p7vrk9j9h1w4829jlwdsqhri09jb1")))

