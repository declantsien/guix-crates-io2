(define-module (crates-io wa gy wagyu-zcash-parameters-3) #:use-module (crates-io))

(define-public crate-wagyu-zcash-parameters-3-0.1.0 (c (n "wagyu-zcash-parameters-3") (v "0.1.0") (h "04kmk39y79q8d5kwww1kd9v0lccwwygp9h17xqsgn6qk6gqy66hz")))

(define-public crate-wagyu-zcash-parameters-3-0.2.0 (c (n "wagyu-zcash-parameters-3") (v "0.2.0") (h "0iv1bpkpndibn0qpi9k26jzm74h6hhcfjs7f6043rycgjlp1xnhl")))

