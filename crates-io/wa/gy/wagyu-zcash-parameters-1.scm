(define-module (crates-io wa gy wagyu-zcash-parameters-1) #:use-module (crates-io))

(define-public crate-wagyu-zcash-parameters-1-0.1.0 (c (n "wagyu-zcash-parameters-1") (v "0.1.0") (h "1wd6vjj0xbmik6rr6rlzv4zjcwpd77fhml72da7ach0kjp1pkw9h")))

(define-public crate-wagyu-zcash-parameters-1-0.2.0 (c (n "wagyu-zcash-parameters-1") (v "0.2.0") (h "1a2xd6sqplz0z1786vsfrvv8p82l1dr6l3f65223yz82pchjxgwh")))

