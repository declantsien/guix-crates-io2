(define-module (crates-io wa gy wagyu-zcash-parameters-2) #:use-module (crates-io))

(define-public crate-wagyu-zcash-parameters-2-0.1.0 (c (n "wagyu-zcash-parameters-2") (v "0.1.0") (h "08gv95i18mp4h6dc9r7i3irhq42zzv5jk0gvyqm0h0l9ysfa624y")))

(define-public crate-wagyu-zcash-parameters-2-0.2.0 (c (n "wagyu-zcash-parameters-2") (v "0.2.0") (h "0b5sr5n086wly85kjpw1fgy1dyqhx16yjxnljn4w8k77a4pan5m6")))

