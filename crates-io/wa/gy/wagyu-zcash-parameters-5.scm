(define-module (crates-io wa gy wagyu-zcash-parameters-5) #:use-module (crates-io))

(define-public crate-wagyu-zcash-parameters-5-0.1.0 (c (n "wagyu-zcash-parameters-5") (v "0.1.0") (h "1a0p4raaihsalv1xcyh4rhhv4m6z2711fgq9wir3x8vjrnkyxbc0")))

(define-public crate-wagyu-zcash-parameters-5-0.2.0 (c (n "wagyu-zcash-parameters-5") (v "0.2.0") (h "17cnwkppkmh849bcap5b37n14gjaaw14ywqvmqrc027661mr3ziz")))

