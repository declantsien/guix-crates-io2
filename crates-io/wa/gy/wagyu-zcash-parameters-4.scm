(define-module (crates-io wa gy wagyu-zcash-parameters-4) #:use-module (crates-io))

(define-public crate-wagyu-zcash-parameters-4-0.1.0 (c (n "wagyu-zcash-parameters-4") (v "0.1.0") (h "0g5j69nxs5vr74q7a50y6y97ynrjb71rb01zwkbxxz55z9z6k7q4")))

(define-public crate-wagyu-zcash-parameters-4-0.2.0 (c (n "wagyu-zcash-parameters-4") (v "0.2.0") (h "1mm6x5cbjghhwsk598h4n89h7ccbgl2x3dgzcs30w1x20gpswn7h")))

