(define-module (crates-io wa si wasi-ext) #:use-module (crates-io))

(define-public crate-wasi-ext-0.1.0 (c (n "wasi-ext") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (o #t) (k 0)) (d (n "wasi") (r "^0.13") (k 0)))) (h "1jv9pgqgrv920hjmxfa35rpxnizn2ya5kykraq26idvx2nijbwfn") (f (quote (("std" "wasi/std") ("default" "std"))))))

