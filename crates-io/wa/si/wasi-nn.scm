(define-module (crates-io wa si wasi-nn) #:use-module (crates-io))

(define-public crate-wasi-nn-0.1.0 (c (n "wasi-nn") (v "0.1.0") (h "15l9am0jyxw898119a12ymwk1ddpdrjafr1gw0lw2gcrxp6rm40c")))

(define-public crate-wasi-nn-0.2.0 (c (n "wasi-nn") (v "0.2.0") (h "0rbw6r6sbsfp5s3xk7i9rjx907wcjic2pscyjzdfzjmnjqd6pgh2")))

(define-public crate-wasi-nn-0.2.1 (c (n "wasi-nn") (v "0.2.1") (h "1y6c6lmnq2mf9swis9ma6yyp9lwnnyh62w0g01dval18sc4mcwc0")))

(define-public crate-wasi-nn-0.3.0 (c (n "wasi-nn") (v "0.3.0") (h "1zgxfkqdphz3dznmivn2pvypmz5jkagifr9gsm0f5wd21agsl80q")))

(define-public crate-wasi-nn-0.4.0 (c (n "wasi-nn") (v "0.4.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zc1prg2icpag5kch6442x8rbdqgj3wisjw384zq7w2ypz9l2ida")))

(define-public crate-wasi-nn-0.5.0 (c (n "wasi-nn") (v "0.5.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0lkmb6rz6d7hvgknrr8r3q1l3r73yp1in3p9habg38ycy281pl03")))

(define-public crate-wasi-nn-0.6.0 (c (n "wasi-nn") (v "0.6.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0h4glvrldkq22sfrgfqdqk9xw5yp52dibyq0j9fm2wasq0y6hcbh")))

