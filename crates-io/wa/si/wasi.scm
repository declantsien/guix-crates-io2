(define-module (crates-io wa si wasi) #:use-module (crates-io))

(define-public crate-wasi-0.0.0 (c (n "wasi") (v "0.0.0") (h "0q4v8j9df57cdcr35xq6abrzfpnpsbb7mcmqq5l8sji6zd5l1p2f")))

(define-public crate-wasi-0.3.0 (c (n "wasi") (v "0.3.0") (h "12va3z1c3wk3r36430ir962j0wd4mygcr4rh6lw2zhzs4vrnchxl")))

(define-public crate-wasi-0.4.0 (c (n "wasi") (v "0.4.0") (h "1vgv948xcv232hwcjnm2v0w8igki78y7v1s7w2c49vgjykbd25vj")))

(define-public crate-wasi-0.5.0 (c (n "wasi") (v "0.5.0") (h "1ir3pd4phdfml0cbziw9bqp7mnk0vfp9biy8bh25lln6raml4m7x")))

(define-public crate-wasi-0.6.0 (c (n "wasi") (v "0.6.0") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "0iaifj4p7yahazpz5af4f8zbv3g0afhj7q416nbxifc59malakik") (f (quote (("rustc-dep-of-std" "compiler_builtins" "core"))))))

(define-public crate-wasi-0.7.0 (c (n "wasi") (v "0.7.0") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03apg3sa4hjn6xwa4pkyvzjiscya51wyrygadgxwdg8lrvj3r75q") (f (quote (("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "alloc") ("alloc"))))))

(define-public crate-wasi-0.9.0+wasi-snapshot-preview1 (c (n "wasi") (v "0.9.0+wasi-snapshot-preview1") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)))) (h "06g5v3vrdapfzvfq662cij7v8a1flwr2my45nnncdv2galrdzkfc") (f (quote (("std") ("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "std"))))))

(define-public crate-wasi-0.10.0+wasi-snapshot-preview1 (c (n "wasi") (v "0.10.0+wasi-snapshot-preview1") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07y3l8mzfzzz4cj09c8y90yak4hpsi9g7pllyzpr6xvwrabka50s") (f (quote (("std") ("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "std"))))))

(define-public crate-wasi-0.10.1+wasi-snapshot-preview1 (c (n "wasi") (v "0.10.1+wasi-snapshot-preview1") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1affhqhp91s1hsxxakdvp60j0mnbgbkjafrpr95wdib3151c7ilk") (f (quote (("std") ("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "std"))))))

(define-public crate-wasi-0.10.2+wasi-snapshot-preview1 (c (n "wasi") (v "0.10.2+wasi-snapshot-preview1") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ii7nff4y1mpcrxzzvbpgxm7a1nn3szjf1n21jnx37c2g6dbsvzx") (f (quote (("std") ("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "std"))))))

(define-public crate-wasi-0.10.3+wasi-snapshot-preview1 (c (n "wasi") (v "0.10.3+wasi-snapshot-preview1") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)))) (h "03iw4kid8y2nmshd793bsnpp2nqp24aak1rpak3v0w7ilf2f78j6") (f (quote (("std") ("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "std")))) (y #t)))

(define-public crate-wasi-0.11.0+wasi-snapshot-preview1 (c (n "wasi") (v "0.11.0+wasi-snapshot-preview1") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)))) (h "08z4hxwkpdpalxjps1ai9y7ihin26y9f476i53dv98v45gkqg3cw") (f (quote (("std") ("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "std"))))))

(define-public crate-wasi-0.12.0+wasi-0.2.0 (c (n "wasi") (v "0.12.0+wasi-0.2.0") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.16") (f (quote ("realloc"))) (k 0)))) (h "1vvrhmndniigdrjzzh68ysicp7gz2xg855sr4yfpsvmk47ixfza5") (f (quote (("std") ("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "std"))))))

(define-public crate-wasi-0.12.1+wasi-0.2.0 (c (n "wasi") (v "0.12.1+wasi-0.2.0") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.19.1") (f (quote ("realloc"))) (k 0)))) (h "0yn6c5smlng07xx25z1wf3n9lp2dl2w9g7iz3daqaz9vww1ly9xg") (f (quote (("std") ("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "std"))))))

(define-public crate-wasi-0.13.0+wasi-0.2.0 (c (n "wasi") (v "0.13.0+wasi-0.2.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wit-bindgen-rt") (r "^0.21.0") (d #t) (k 0)))) (h "0621m5nnzd4hrbs8hr390njmyjnkg4nwfw1vfji5gffh94sdfb35") (f (quote (("std") ("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "std"))))))

(define-public crate-wasi-0.13.1+wasi-0.2.0 (c (n "wasi") (v "0.13.1+wasi-0.2.0") (d (list (d (n "compiler_builtins") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "rustc-std-workspace-alloc") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "wit-bindgen-rt") (r "^0.24.0") (f (quote ("bitflags"))) (d #t) (k 0)))) (h "1y0czq00if5dl154gwbpmfl7dnc2wnihpak17sdbizj5c71x2hrg") (f (quote (("std") ("rustc-dep-of-std" "compiler_builtins" "core" "rustc-std-workspace-alloc") ("default" "std"))))))

