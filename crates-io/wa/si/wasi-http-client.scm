(define-module (crates-io wa si wasi-http-client) #:use-module (crates-io))

(define-public crate-wasi-http-client-0.1.0 (c (n "wasi-http-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wasi") (r "^0.13.0") (d #t) (k 0)))) (h "0mqajk1hkf6yfy563cilcy8bymcnpysaypl0mxgv9sn8yglvxksw")))

(define-public crate-wasi-http-client-0.1.1 (c (n "wasi-http-client") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wasi") (r "^0.13.0") (d #t) (k 0)))) (h "1dl8girs0hgys3vx74fcgi4ri6f6w95i95vri15z9hymfssal63v")))

(define-public crate-wasi-http-client-0.2.0 (c (n "wasi-http-client") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (o #t) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wasi") (r "^0.13.0") (d #t) (k 0)))) (h "0v8akcljwbb28blp2wksviy9aic9gyrxxbpbsz4sni4jpxlhnsvs") (s 2) (e (quote (("json" "dep:serde_json"))))))

(define-public crate-wasi-http-client-0.2.1 (c (n "wasi-http-client") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "serde") (r "^1.0.201") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (o #t) (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wasi") (r "^0.13.0") (d #t) (k 0)))) (h "02bgqdymbv7pwsc4g95rj78xp0jqnc5ba5m8k3pjjndkmcml98x9") (s 2) (e (quote (("json" "dep:serde_json"))))))

