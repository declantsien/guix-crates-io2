(define-module (crates-io wa si wasi-preview2-prototype) #:use-module (crates-io))

(define-public crate-wasi-preview2-prototype-0.0.0 (c (n "wasi-preview2-prototype") (v "0.0.0") (h "029wv0qh22mdlvqd0n59cpwqsk3ffjf6wigxsqg5gmi9xkci9gd3")))

(define-public crate-wasi-preview2-prototype-0.0.1 (c (n "wasi-preview2-prototype") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.4.0") (f (quote ("macros" "realloc"))) (k 0)))) (h "0svh1bjz2himvslfpfb6d09jfr5i2jm4c8a5wn5fvkgv7ydq497m") (f (quote (("default")))) (s 2) (e (quote (("http-client" "dep:anyhow" "dep:bytes" "dep:http"))))))

(define-public crate-wasi-preview2-prototype-0.0.2 (c (n "wasi-preview2-prototype") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.9.0") (f (quote ("macros"))) (k 0)))) (h "0r9zk2543rh5qg748ricm5dpiln2bw680n11jxckzwp6qm99gcjy") (f (quote (("default")))) (s 2) (e (quote (("http-client" "dep:anyhow" "dep:bytes" "dep:http"))))))

(define-public crate-wasi-preview2-prototype-0.0.3 (c (n "wasi-preview2-prototype") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "http") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.9.0") (f (quote ("macros"))) (k 0)))) (h "0x8v1zgzj0qckm097hb68aanhwqwzy8gw5nrz162jfg7f8imm5fg") (f (quote (("default")))) (s 2) (e (quote (("http-client" "dep:anyhow" "dep:bytes" "dep:http"))))))

