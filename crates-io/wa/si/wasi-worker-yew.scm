(define-module (crates-io wa si wasi-worker-yew) #:use-module (crates-io))

(define-public crate-wasi-worker-yew-0.4.0 (c (n "wasi-worker-yew") (v "0.4.0") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "wasi-worker") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.10") (d #t) (k 0)))) (h "0x8fypk2z7wv3pr7ygz6gbnkcx8ppyw01k6n89ai8k8d6zadi8qj")))

(define-public crate-wasi-worker-yew-0.4.1 (c (n "wasi-worker-yew") (v "0.4.1") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "wasi-worker") (r "^0.4") (d #t) (k 0)) (d (n "yew") (r "^0.10") (d #t) (k 0)))) (h "07iryi7y9j9drf8hag2qd2qhd2l666r1f765rqbicvc05si7qgw3")))

(define-public crate-wasi-worker-yew-0.5.0 (c (n "wasi-worker-yew") (v "0.5.0") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "wasi-worker") (r "^0.5") (d #t) (k 0)) (d (n "yew") (r "^0.10") (d #t) (k 0)))) (h "19jhkxh426mgwl6spyggj812fhc0vrm9ms3hpjp0ca7bdaf04szr")))

(define-public crate-wasi-worker-yew-0.5.1 (c (n "wasi-worker-yew") (v "0.5.1") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "wasi-worker") (r "^0.5") (d #t) (k 0)) (d (n "yew") (r "^0.11") (d #t) (k 0)))) (h "17j153a9jbm3wzwcq6i15rwn4fnh7ybdg4kb3h80m422kh3p0pq5")))

(define-public crate-wasi-worker-yew-0.5.2 (c (n "wasi-worker-yew") (v "0.5.2") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "wasi-worker") (r "^0.5") (d #t) (k 0)) (d (n "yew") (r "^0.11") (d #t) (k 0)))) (h "0q34wp57mjdn99p2gjwbw2xw4v3q3s1vhbrkghh5pxajzhshfcrn")))

