(define-module (crates-io wa si wasi-experimental-http) #:use-module (crates-io))

(define-public crate-wasi-experimental-http-0.1.0 (c (n "wasi-experimental-http") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0a7077mdyqpg8b8axrlhs3fhf44jlfhwqbb3fdy3kw4i6bz81irz")))

(define-public crate-wasi-experimental-http-0.1.1 (c (n "wasi-experimental-http") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "07zmbjf6vcdqzh0a83djy2xwbhl7xszxy83k2b591qgszwn8zry0")))

(define-public crate-wasi-experimental-http-0.2.0 (c (n "wasi-experimental-http") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "16fymqvb4k07fmnnsv7jcazc0dpzdv8a2c6xbs3v9rar0fmd8lkq")))

(define-public crate-wasi-experimental-http-0.3.0 (c (n "wasi-experimental-http") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "0dqm9xnx0cnj57m3gli4vxmbjk30ch85r9v5b9r789qvgvxsk6s2")))

(define-public crate-wasi-experimental-http-0.4.0 (c (n "wasi-experimental-http") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "04g68dm6660zipm5pbn4xm3vvzric9f6hv6dkv7q85ipgfk36v9f")))

(define-public crate-wasi-experimental-http-0.5.0 (c (n "wasi-experimental-http") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "00i9bildmgplpxlsqc404m1q4dnpgczzz6rkp2rfzjmgwn7ifi46")))

(define-public crate-wasi-experimental-http-0.6.0 (c (n "wasi-experimental-http") (v "0.6.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "11ycr9s04s9cm9rzjkl1yyyap2844rgrx9pa6nrf6qzwmdjf5fs7")))

(define-public crate-wasi-experimental-http-0.7.0 (c (n "wasi-experimental-http") (v "0.7.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "0ms9q0id614l0vgdxrcbyq93n0nhb8yxwpwagmjjjmic6fl672ca")))

(define-public crate-wasi-experimental-http-0.8.0 (c (n "wasi-experimental-http") (v "0.8.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "1pwddrpzpkjnbqx0dm8334brsdjky992d1ailwpgfxqaqym3a3xw")))

(define-public crate-wasi-experimental-http-0.9.0 (c (n "wasi-experimental-http") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "1f5ppx2lrgnbzv7man1qnd7d1xpl74jwab3bpxlg78ayhvks3irc")))

(define-public crate-wasi-experimental-http-0.10.0 (c (n "wasi-experimental-http") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)))) (h "0yfw51dbqvpw9xqx0aizfjpan59f77r8hxhvd3a7rvs1whf001cq")))

