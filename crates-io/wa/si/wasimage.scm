(define-module (crates-io wa si wasimage) #:use-module (crates-io))

(define-public crate-wasimage-0.1.0 (c (n "wasimage") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "png") (r "^0.17") (d #t) (k 0)))) (h "1pffxyjqmisrliw5bvrqjl93kass9yc3fhpkj721zhy6h3x857g7")))

(define-public crate-wasimage-0.1.1 (c (n "wasimage") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "argh") (r "^0.1") (d #t) (k 0)) (d (n "fs-err") (r "^2") (d #t) (k 0)) (d (n "lodepng") (r "^3") (d #t) (k 0)) (d (n "zopfli") (r "^0.5") (d #t) (k 0)))) (h "103szvvrbylw4vv1q6fhmdx0wa6v0g2bnbld8l6w9r2d4xy9k4c2")))

