(define-module (crates-io wa si wasi-worker-cli) #:use-module (crates-io))

(define-public crate-wasi-worker-cli-0.1.0 (c (n "wasi-worker-cli") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "wasi-worker") (r "^0.3") (d #t) (k 0)))) (h "12vsb6v1iigmdvg9qsgwy8zdl0bajiy88ks8jb1wwq6ilqz4a4nm")))

(define-public crate-wasi-worker-cli-0.1.2 (c (n "wasi-worker-cli") (v "0.1.2") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "wasi-worker") (r "^0.3") (d #t) (k 0)))) (h "1wf8ha138bnjw17pypkf3v34ilycx2nxm4a8339zqhjcaxw0sc5n")))

(define-public crate-wasi-worker-cli-0.2.0 (c (n "wasi-worker-cli") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)) (d (n "wasi-worker") (r "^0.3") (d #t) (k 0)))) (h "1jsl4q2mn92w1y9gp3250v6hdw4z0fhk0x4ixw85gjgxhdn3n0kb")))

(define-public crate-wasi-worker-cli-0.2.1 (c (n "wasi-worker-cli") (v "0.2.1") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)) (d (n "wasi-worker") (r "^0.3") (d #t) (k 0)))) (h "079fhzycidwh41l6kgvn8wb8j1dxqvx6lixknhxsagwvv93igrg1")))

(define-public crate-wasi-worker-cli-0.2.2 (c (n "wasi-worker-cli") (v "0.2.2") (d (list (d (n "dir-diff") (r "^0.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)))) (h "0k4f9p69zyhazdkc8s82r5d4fj4jhc579v9l03j789f1v4c08d7m")))

(define-public crate-wasi-worker-cli-0.2.3 (c (n "wasi-worker-cli") (v "0.2.3") (d (list (d (n "dir-diff") (r "^0.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)))) (h "1dn7rsqpyfh5il1hj1x5i726lbn3lv8n2bmfz8fhj7jrn2winp2l")))

(define-public crate-wasi-worker-cli-0.3.1 (c (n "wasi-worker-cli") (v "0.3.1") (d (list (d (n "dir-diff") (r "^0.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)))) (h "0z7cp5zk6761mrd4q1kkw5i3hmy01jsdfyga5nh1hx0jacdris2c")))

(define-public crate-wasi-worker-cli-0.3.2 (c (n "wasi-worker-cli") (v "0.3.2") (d (list (d (n "dir-diff") (r "^0.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "1hl2jdnj471nqg74xz2jphfvwg4k2y9793ap41i658jdpamvqsbf")))

(define-public crate-wasi-worker-cli-0.3.3 (c (n "wasi-worker-cli") (v "0.3.3") (d (list (d (n "dir-diff") (r "^0.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "1y5lvp84njq6vnczi8b25ckwgq43nm4y62xaq4y10948x2qf32ky")))

(define-public crate-wasi-worker-cli-0.4.0 (c (n "wasi-worker-cli") (v "0.4.0") (d (list (d (n "dir-diff") (r "^0.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "0dprwjdq46phww03qz3ri47x067i7q1fa8isk4bgizbmzr6j98hb")))

(define-public crate-wasi-worker-cli-0.4.1 (c (n "wasi-worker-cli") (v "0.4.1") (d (list (d (n "dir-diff") (r "^0.3") (d #t) (k 2)) (d (n "fs_extra") (r "^1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "000yhwvcm852wymc106b76vmvxjg2wm0s59sc04jhaplyvx1ah2s")))

(define-public crate-wasi-worker-cli-0.4.4 (c (n "wasi-worker-cli") (v "0.4.4") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "10sfq4m79hr2jdrh3arbp0lmab89h7ijkbxl8rb9vjmnhx9ygh61")))

(define-public crate-wasi-worker-cli-0.4.5 (c (n "wasi-worker-cli") (v "0.4.5") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "1c44cx9fsfzcrzzbb0i2x1zif5rs4yhzniqbpsvc3rrvgzxs0q88")))

(define-public crate-wasi-worker-cli-0.5.0 (c (n "wasi-worker-cli") (v "0.5.0") (d (list (d (n "fs_extra") (r "^1.1") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "toml_edit") (r "^0.1") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "0h33nyi2xlmix1pxjlqn5cykifq0q6dxsm0bfzmpgxsh8j1xyg00")))

(define-public crate-wasi-worker-cli-0.6.0 (c (n "wasi-worker-cli") (v "0.6.0") (d (list (d (n "fs_extra") (r "^1.2") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (k 0)) (d (n "toml_edit") (r "^0.2") (d #t) (k 0)) (d (n "wasm-gc-api") (r "^0.1") (d #t) (k 0)))) (h "0b2701xdl2wx4hx6nnp45zzy6pcx0cizvkzazpb5l9kzjr0wlln8")))

