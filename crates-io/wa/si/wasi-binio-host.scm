(define-module (crates-io wa si wasi-binio-host) #:use-module (crates-io))

(define-public crate-wasi-binio-host-0.1.0 (c (n "wasi-binio-host") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasi_binio_shared_mods") (r "^0.1.0") (d #t) (k 0)) (d (n "wasmtime") (r "^0.15.0") (d #t) (k 0)))) (h "12wx5wnxc9hk4pnhmijxfc363ygqskysrs4szkcmzph1wvw3vsfm")))

