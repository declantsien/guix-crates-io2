(define-module (crates-io wa si wasi-update) #:use-module (crates-io))

(define-public crate-wasi-update-0.1.0 (c (n "wasi-update") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("std" "cargo" "wrap_help" "string"))) (d #t) (k 0)) (d (n "ct-codecs") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wit-component") (r "^0.200") (d #t) (k 0)))) (h "17dji8vhjvaaiqf7gmia5ihlhwc56dqd0a8mkf7wk7w402yckm14")))

(define-public crate-wasi-update-0.1.1 (c (n "wasi-update") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("std" "cargo" "wrap_help" "string"))) (d #t) (k 0)) (d (n "ct-codecs") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wit-component") (r "^0.200") (d #t) (k 0)))) (h "0ida242w4v09a370yxpklx04vbsnlw93fvwx80fk6g42k4laq0hr")))

(define-public crate-wasi-update-0.1.2 (c (n "wasi-update") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("std" "cargo" "wrap_help" "string"))) (d #t) (k 0)) (d (n "ct-codecs") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wit-component") (r "^0.205.0") (d #t) (k 0) (p "wit-component-update")))) (h "0g2f4vdajh86g3q8788lbxjpm9cs74gaqybwvbgg6chi9dfyli50")))

(define-public crate-wasi-update-0.1.5 (c (n "wasi-update") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("std" "cargo" "wrap_help" "string"))) (d #t) (k 0)) (d (n "ct-codecs") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wit-component") (r "^0.205.0") (d #t) (k 0) (p "wit-component-update")))) (h "0bg0099569sgxijqxpl2qsnk8fg86zys1cfsv5d4gas3nqi55v76")))

