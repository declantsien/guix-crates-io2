(define-module (crates-io wa si wasi-common-lucet) #:use-module (crates-io))

(define-public crate-wasi-common-lucet-0.1.0 (c (n "wasi-common-lucet") (v "0.1.0") (d (list (d (n "lucet-runtime") (r "^0.1.1") (d #t) (k 0)) (d (n "wasi-common") (r "^0.2") (d #t) (k 0)))) (h "1hj9i3q7xg4v70v5g1q5jn6swg152zmysqvvpjrj1rsygq4wj7k9")))

(define-public crate-wasi-common-lucet-0.1.1 (c (n "wasi-common-lucet") (v "0.1.1") (d (list (d (n "lucet-runtime") (r "^0.1.1") (d #t) (k 0)) (d (n "wasi-common") (r "^0.2") (d #t) (k 0)))) (h "0gziwgxb919f9j3npf6hs4sjs7ghw9y7i1lahcvxihzbbswx9y8y")))

(define-public crate-wasi-common-lucet-0.1.2 (c (n "wasi-common-lucet") (v "0.1.2") (d (list (d (n "lucet-runtime") (r "^0.1.1") (d #t) (k 0)) (d (n "wasi-common") (r "^0.2") (d #t) (k 0)))) (h "1s2rfji79wywrqx75ab22i7irmp44d5p634svjsl0p1hmzidlwr3")))

(define-public crate-wasi-common-lucet-0.1.3 (c (n "wasi-common-lucet") (v "0.1.3") (d (list (d (n "lucet-runtime") (r "^0.1.1") (d #t) (k 0)) (d (n "wasi-common") (r "^0.2") (d #t) (k 0)))) (h "19d6wap69is6s85l203nwg1vy8vqila56rpkigifr8aw8ms1w2nh")))

(define-public crate-wasi-common-lucet-0.1.4 (c (n "wasi-common-lucet") (v "0.1.4") (d (list (d (n "lucet-runtime") (r "^0.1.1") (d #t) (k 0)) (d (n "wasi-common") (r "^0.2") (d #t) (k 0)))) (h "13n3dby45j76lwszky5dz7mcr1pjp57x23rwfbpdb4cmsx27ifvx")))

(define-public crate-wasi-common-lucet-0.1.5 (c (n "wasi-common-lucet") (v "0.1.5") (d (list (d (n "lucet-runtime") (r "^0.2") (d #t) (k 0)) (d (n "wasi-common") (r "^0") (d #t) (k 0)))) (h "1yn8xg1fhqgdl942w5gzw30sgsalrrrdkpbx4l96bidrzj6myans")))

(define-public crate-wasi-common-lucet-0.1.6 (c (n "wasi-common-lucet") (v "0.1.6") (d (list (d (n "lucet-runtime-internals") (r "^0.2.1") (d #t) (k 0)) (d (n "wasi-common") (r "^0.2") (d #t) (k 0)))) (h "0cxcdsq1na03j1cdmh63z58k69wb8j2621500sz3wxkczw9ks644")))

(define-public crate-wasi-common-lucet-0.1.7 (c (n "wasi-common-lucet") (v "0.1.7") (d (list (d (n "lucet-runtime-internals") (r "^0.3") (d #t) (k 0)) (d (n "wasi-common") (r "^0.2") (d #t) (k 0)))) (h "0g30ffw0zbppi954d2si3nz762vnf96yrhidqg54fdz8xdfx9dxa")))

(define-public crate-wasi-common-lucet-0.2.0 (c (n "wasi-common-lucet") (v "0.2.0") (d (list (d (n "lucet-runtime-internals") (r "^0.3") (d #t) (k 0)) (d (n "wasi-common") (r "^0.2") (d #t) (k 0)))) (h "1w5ajsjxkg0snr8gfhxyvj0imqssaf1jbfndw20qy8jbs6l9vw4i")))

(define-public crate-wasi-common-lucet-0.3.0 (c (n "wasi-common-lucet") (v "0.3.0") (d (list (d (n "lucet-runtime-internals") (r "^0.4") (d #t) (k 0)) (d (n "wasi-common") (r "^0.2") (d #t) (k 0)))) (h "0v99q3jmy5qf0f9j92zah51qfw3l8y6029xb60k9micpjgfd6kja")))

(define-public crate-wasi-common-lucet-0.4.0 (c (n "wasi-common-lucet") (v "0.4.0") (d (list (d (n "lucet-runtime") (r "^0.5") (d #t) (k 0)) (d (n "lucet-runtime-internals") (r "^0.5") (d #t) (k 0)) (d (n "wasi-common") (r "^0.4") (d #t) (k 0)))) (h "03rh187jb04qw2pyrwmf2nc3l6v4bfpbyh7kcnldjf8sk40q940m")))

(define-public crate-wasi-common-lucet-0.4.1 (c (n "wasi-common-lucet") (v "0.4.1") (d (list (d (n "lucet-runtime") (r "^0.5") (d #t) (k 0)) (d (n "lucet-runtime-internals") (r "^0.5") (d #t) (k 0)) (d (n "wasi-common") (r "^0.8") (d #t) (k 0)))) (h "0dkii5i5rqq8f7hpcxj6xdcpsl2m977w6snh67jj53gbk92s2val")))

(define-public crate-wasi-common-lucet-0.4.2 (c (n "wasi-common-lucet") (v "0.4.2") (d (list (d (n "lucet-runtime") (r "^0.6") (d #t) (k 0)) (d (n "lucet-runtime-internals") (r "^0.6") (d #t) (k 0)) (d (n "wasi-common") (r "^0.8") (d #t) (k 0)))) (h "0ilgw31dfbaqmfkfkddam7hvyir1hi86jhfnq4kw3dy7l8n69nb1")))

