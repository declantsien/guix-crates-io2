(define-module (crates-io wa si wasi-worker) #:use-module (crates-io))

(define-public crate-wasi-worker-0.3.0 (c (n "wasi-worker") (v "0.3.0") (h "0wbaqjxcmy4jj6mq00dg5i3gx7rcd0wx26f3077s6nff6kwpgf23")))

(define-public crate-wasi-worker-0.3.1 (c (n "wasi-worker") (v "0.3.1") (h "189hwv20hiay57mz8cs6arf03qxznqdn9d10dw4fknihqmjcqmrj")))

(define-public crate-wasi-worker-0.3.2 (c (n "wasi-worker") (v "0.3.2") (h "1cvlmplqa8nlda15sljm49j5cp5fpyzs0bq4g4x9840laac7m82l")))

(define-public crate-wasi-worker-0.3.3 (c (n "wasi-worker") (v "0.3.3") (h "1sppbxb4wl3qh4z43rwxl37j1crylvd1sbls2av9y06z9j81xqia")))

(define-public crate-wasi-worker-0.3.4 (c (n "wasi-worker") (v "0.3.4") (h "0zkx6nf1cvfxmw80ggd94kpl6jm4hql01q4sqisam2yjdn5i5zgw")))

(define-public crate-wasi-worker-0.3.5 (c (n "wasi-worker") (v "0.3.5") (h "186hgqq2lgs7vdqv5r5573q40c5mdkdijxw47rqgibh136r9sgxr")))

(define-public crate-wasi-worker-0.3.6 (c (n "wasi-worker") (v "0.3.6") (h "1axki2l65qkv5k6a9nb14k4c92ipr07iqh2fsji1vyibiv49ikp3")))

(define-public crate-wasi-worker-0.4.0 (c (n "wasi-worker") (v "0.4.0") (h "1mklw2ki8sd62538hhaqwwrs88vcsds5ns88s19sb1q7zavjfwfs")))

(define-public crate-wasi-worker-0.5.0 (c (n "wasi-worker") (v "0.5.0") (h "0552kcxc03v4zzbw8dwb5y1nx4j54mi0zmdpkcc2sfr8nqh3mvwj")))

