(define-module (crates-io wa si wasi-async-runtime) #:use-module (crates-io))

(define-public crate-wasi-async-runtime-0.1.0 (c (n "wasi-async-runtime") (v "0.1.0") (d (list (d (n "futures-concurrency") (r "^7.4.0") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wasi") (r "^0.12.1") (d #t) (k 0)))) (h "18z35a7agrh4wnj8fy9dp6znxy4783280s6anx6jccp3xb70xc7q")))

(define-public crate-wasi-async-runtime-0.1.1 (c (n "wasi-async-runtime") (v "0.1.1") (d (list (d (n "futures-concurrency") (r "^7.4.0") (d #t) (k 2)) (d (n "slab") (r "^0.4.9") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)) (d (n "wasi") (r "^0.12.1") (d #t) (k 0)))) (h "11yw2znqaqnk32zrcm4ws48105lg1r04i5k3gch97b4kiy8wxg03")))

(define-public crate-wasi-async-runtime-0.1.2 (c (n "wasi-async-runtime") (v "0.1.2") (d (list (d (n "futures-concurrency") (r "^7.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14.3") (d #t) (k 0)) (d (n "slab") (r "^0.4.9") (k 0)) (d (n "wasi") (r "^0.12.1") (d #t) (k 0)))) (h "08p007lh4py1sp42z9dxkax4k2dyfsbp9r6qasm7idb9k3wbgw4x") (f (quote (("std") ("default" "std"))))))

