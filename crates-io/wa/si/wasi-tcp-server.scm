(define-module (crates-io wa si wasi-tcp-server) #:use-module (crates-io))

(define-public crate-wasi-tcp-server-0.1.0 (c (n "wasi-tcp-server") (v "0.1.0") (d (list (d (n "wasmedge_wasi_socket") (r "^0.4") (d #t) (k 0)))) (h "0x1winzncjkjjr8iqflw176dsg71zcasf4bkp0dwf87r0nzkpg6r")))

(define-public crate-wasi-tcp-server-0.1.1 (c (n "wasi-tcp-server") (v "0.1.1") (d (list (d (n "wasmedge_wasi_socket") (r "^0.4") (d #t) (k 0)))) (h "0d5isisjymkz6mqcqv2sgk1zsf2s13q1n5q9w8364r9jns2wz3v3")))

