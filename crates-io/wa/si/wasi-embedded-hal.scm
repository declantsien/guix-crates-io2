(define-module (crates-io wa si wasi-embedded-hal) #:use-module (crates-io))

(define-public crate-wasi-embedded-hal-0.0.1 (c (n "wasi-embedded-hal") (v "0.0.1") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "lol_alloc") (r "^0.4.1") (d #t) (k 0)) (d (n "wit-bindgen-rt") (r "^0.22.0") (d #t) (k 0)))) (h "1nzh4ck7qzh9nn5qp51609h1b44kpyzq5wnwfrszr3k3lh3rmghj") (f (quote (("use_std" "use_alloc") ("use_alloc") ("default" "use_std"))))))

(define-public crate-wasi-embedded-hal-0.2.0 (c (n "wasi-embedded-hal") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0") (d #t) (k 0)) (d (n "lol_alloc") (r "^0.4.1") (d #t) (k 0)) (d (n "wit-bindgen-rt") (r "^0.22.0") (d #t) (k 0)))) (h "044d7y2jdc57180y5wwjkavwngcal39q9l4nr5frfqfgz2i50vvd") (f (quote (("use_std" "use_alloc") ("use_alloc") ("default" "use_std"))))))

