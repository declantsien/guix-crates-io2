(define-module (crates-io wa si wasi-crypto-guest) #:use-module (crates-io))

(define-public crate-wasi-crypto-guest-0.1.3 (c (n "wasi-crypto-guest") (v "0.1.3") (d (list (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)))) (h "1qfjn7hrhrxylplml4vjlqf7701ajkj04iy2mxcdcvl8bvknhfgs")))

(define-public crate-wasi-crypto-guest-0.1.4 (c (n "wasi-crypto-guest") (v "0.1.4") (d (list (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)))) (h "0w10mgfkxk4fs9a47wv5wwjjpxr92p2vr26lpi48yz6z8x50rnb0")))

