(define-module (crates-io wa si wasi-version) #:use-module (crates-io))

(define-public crate-wasi-version-0.1.0 (c (n "wasi-version") (v "0.1.0") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "wasm-interface") (r "^0.1.0") (d #t) (k 0)))) (h "021g42c8i88q2593rsl69chmy2dfs9cjp4fg5jy26xcgmwjzwgfq")))

