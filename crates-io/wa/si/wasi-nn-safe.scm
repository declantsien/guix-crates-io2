(define-module (crates-io wa si wasi-nn-safe) #:use-module (crates-io))

(define-public crate-wasi-nn-safe-0.0.0 (c (n "wasi-nn-safe") (v "0.0.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasi-nn") (r "^0.3.0") (d #t) (k 2)))) (h "0s3zkarkv7svnnmjg7ypv53hawa6bk35dmkhjihmz5qqnhqkr8nx") (y #t)))

(define-public crate-wasi-nn-safe-0.0.1 (c (n "wasi-nn-safe") (v "0.0.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasi-nn") (r "^0.3.0") (d #t) (t "wasm32-wasi") (k 2)))) (h "1fidvxw7w5r7dfd3a38cx0hdik0mmnyhhby3cb0i9iygy7qpjrg9") (y #t)))

(define-public crate-wasi-nn-safe-0.0.2 (c (n "wasi-nn-safe") (v "0.0.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasi-nn") (r "^0.3.0") (d #t) (t "wasm32-wasi") (k 2)))) (h "0x9kkjw8xdkvry42nxd7k251a061zyb7shzy93yr3kwzd16hsz0m") (y #t)))

(define-public crate-wasi-nn-safe-0.0.3 (c (n "wasi-nn-safe") (v "0.0.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasi-nn") (r "^0.3.0") (d #t) (t "wasm32-wasi") (k 2)))) (h "1rm1sdviz6b0q1bcr64gxz6gph6kbdl0mah8bnna5cylkplvpl7k") (y #t)))

(define-public crate-wasi-nn-safe-0.0.4 (c (n "wasi-nn-safe") (v "0.0.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasi-nn") (r "^0.3.0") (d #t) (t "wasm32-wasi") (k 2)))) (h "1rcvjg7bf0csqcqyqj9q20kv7hw7z67gasaphdihfrh9gm1rv467") (y #t)))

(define-public crate-wasi-nn-safe-0.0.5 (c (n "wasi-nn-safe") (v "0.0.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasi-nn") (r "^0.3.0") (d #t) (t "wasm32-wasi") (k 2)))) (h "1m5n9wlkjk21yxbznza40lw4ni661ffwmb1pmmvcpkbpaimgbc1k")))

(define-public crate-wasi-nn-safe-0.1.0 (c (n "wasi-nn-safe") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "wasi-nn") (r "^0.3.0") (d #t) (t "wasm32-wasi") (k 2)))) (h "06r9fwvqqb1gr5gvg1x82iznrhxxch44mwjkzkwhiw8vhyrwxc8a")))

