(define-module (crates-io wa si wasi-rng) #:use-module (crates-io))

(define-public crate-wasi-rng-0.1.0 (c (n "wasi-rng") (v "0.1.0") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "wasi") (r "^0.9") (d #t) (k 0)))) (h "06gq7mpklfjhkr2wl7xkxayzpinysbjpcks1p7f5swifk6ny6vc2")))

(define-public crate-wasi-rng-0.1.1 (c (n "wasi-rng") (v "0.1.1") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "wasi") (r "^0.9") (d #t) (k 0)))) (h "1i6izx27rgk5vmyfpi7i9hrc1nwcxib41fd8baq9war3n0za505f")))

(define-public crate-wasi-rng-0.1.2 (c (n "wasi-rng") (v "0.1.2") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "wasi") (r "^0.9") (d #t) (k 0)))) (h "0ah2zv6bwskdlfnrhms35ldq3zqzliji8siyn5vm3yz3p44w30zb")))

(define-public crate-wasi-rng-0.1.3 (c (n "wasi-rng") (v "0.1.3") (d (list (d (n "rand_core") (r "^0.5") (d #t) (k 0)) (d (n "wasi") (r "^0.9") (d #t) (k 0)))) (h "153mbka8k3pvh1rr8bc6yqcym975kf7l9l402ky4aybbpyrgpasx") (f (quote (("std" "wasi/std") ("default" "std"))))))

