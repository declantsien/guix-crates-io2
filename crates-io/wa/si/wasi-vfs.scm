(define-module (crates-io wa si wasi-vfs) #:use-module (crates-io))

(define-public crate-wasi-vfs-0.1.1 (c (n "wasi-vfs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "wasi") (r "^0.11.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1m2r83f2s5pdqk48ax7kr1rbrfidy4nyriq9a9zx2f0qq8g8wss2") (f (quote (("trace-syscall") ("module-linking" "wee_alloc") ("legacy-wasi-libc"))))))

(define-public crate-wasi-vfs-0.2.0 (c (n "wasi-vfs") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "wasi") (r "^0.11.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0mfn2p8qzdcgyhhw2kwv0nahs1zjy8s2pa5fwkr2n3my7wy46fgm") (f (quote (("trace-syscall") ("module-linking" "wee_alloc") ("legacy-wasi-libc"))))))

(define-public crate-wasi-vfs-0.3.0 (c (n "wasi-vfs") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "wasi") (r "^0.11.0") (d #t) (k 0)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "174sl6piqwp9myq0xxscbk6adpmm6c04lblp67llmvwqjq1pyzmh") (f (quote (("trace-syscall") ("module-linking" "wee_alloc") ("legacy-wasi-libc"))))))

