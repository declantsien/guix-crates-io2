(define-module (crates-io wa si wasi-logger) #:use-module (crates-io))

(define-public crate-wasi-logger-0.1.0 (c (n "wasi-logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "wit-bindgen") (r "^0.24") (f (quote ("macros"))) (k 0)))) (h "0z2y7xq646cipsyln8x2bygw652gnzlm2i7crzq5xvr54va0islh") (f (quote (("kv" "log/kv")))) (r "1.65")))

(define-public crate-wasi-logger-0.1.1 (c (n "wasi-logger") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "wit-bindgen") (r "^0.24") (f (quote ("macros"))) (k 0)))) (h "1228admy6xw366y9v9plgiix11ik2fwk62a9lqnfaykkvz8kzml3") (f (quote (("kv" "log/kv")))) (r "1.65")))

(define-public crate-wasi-logger-0.1.2 (c (n "wasi-logger") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (k 0)) (d (n "wit-bindgen") (r "^0.24") (f (quote ("macros"))) (k 0)))) (h "0jzpjfkxg2s9s53f1rbw538vsf5wdl5gd8a7wzr6xngmnw0m5ajq") (f (quote (("kv" "log/kv")))) (r "1.65")))

