(define-module (crates-io wa si wasi-binio-wasm) #:use-module (crates-io))

(define-public crate-wasi-binio-wasm-0.1.0 (c (n "wasi-binio-wasm") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasi_binio_shared_mods") (r "^0.1.0") (d #t) (k 0)))) (h "0msnny3wjlbs34q90y34xgg7hi6kwirsz3ifm5rxjsmbipfgjys4")))

(define-public crate-wasi-binio-wasm-0.1.1 (c (n "wasi-binio-wasm") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasi_binio_shared_mods") (r "^0.1.0") (d #t) (k 0)))) (h "1vqpdqk8a815jfgb8qdi8l4a6k9pynwjd98by8sdq7dhgh33959x")))

