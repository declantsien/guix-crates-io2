(define-module (crates-io wa si wasi-core) #:use-module (crates-io))

(define-public crate-wasi-core-0.0.0 (c (n "wasi-core") (v "0.0.0") (h "0xqlb3b4g7hvqm0vnn8xs09bp27hz5kfb6mjz363alrjmgar9rqr")))

(define-public crate-wasi-core-0.1.0 (c (n "wasi-core") (v "0.1.0") (h "1gss61xas04fg68412ywfj47cs77acz8himnli469yyirknbl7dh")))

(define-public crate-wasi-core-0.2.0 (c (n "wasi-core") (v "0.2.0") (h "1n68sj9wzz62wijzx3bvqahpkg1mr8c5j5wb2s5jpcyv14jnbjdn")))

