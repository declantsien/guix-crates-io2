(define-module (crates-io wa si wasi-common-cbindgen) #:use-module (crates-io))

(define-public crate-wasi-common-cbindgen-0.1.0 (c (n "wasi-common-cbindgen") (v "0.1.0") (d (list (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "0vh1gyvhrx4c2a1f37qszvl39qm43142mcdc972ygxx31l9cyf26")))

(define-public crate-wasi-common-cbindgen-0.2.0 (c (n "wasi-common-cbindgen") (v "0.2.0") (d (list (d (n "quote") (r "^0.6.12") (d #t) (k 0)) (d (n "syn") (r "^0.15.34") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "08l7yrary9k1izxrlg0a18d2jx01yf7lhci64f818f94i4i0ayy4")))

(define-public crate-wasi-common-cbindgen-0.3.0 (c (n "wasi-common-cbindgen") (v "0.3.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "15xlm0a05pcw3d4347dy3hb6arliipxvxbzavlf7gjvhc1ss2j4q")))

(define-public crate-wasi-common-cbindgen-0.4.0 (c (n "wasi-common-cbindgen") (v "0.4.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "1mwnz2ygaq4qw1rll7vyr9hf82c3gsnzyj3s1mkc58awby48l722")))

(define-public crate-wasi-common-cbindgen-0.5.0 (c (n "wasi-common-cbindgen") (v "0.5.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "10f1acqv104d8c3biix2z20932pv8l85qqbjn77q4k8xxhgphrmw")))

(define-public crate-wasi-common-cbindgen-0.6.0 (c (n "wasi-common-cbindgen") (v "0.6.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "0nif5knvh2hr0v39wdc9gzr0h8bgz1r9bdahlb6xwq6ljc9fdkcx")))

(define-public crate-wasi-common-cbindgen-0.7.0 (c (n "wasi-common-cbindgen") (v "0.7.0") (d (list (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "1kk3a9mhnsjclq5nrw1c2bhf2815yhcwikwyb8p499vj5shrkazc")))

(define-public crate-wasi-common-cbindgen-0.8.0 (c (n "wasi-common-cbindgen") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "0lf4ni690f0h2ri4nshaj1qjm6jc1lai00m6mmb73zq0jc6j21b5")))

(define-public crate-wasi-common-cbindgen-0.9.0 (c (n "wasi-common-cbindgen") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "1v2bps89gwp2miqnl5nb1xgfk82rjbv477brpnk9qvw23z3kjg5z")))

(define-public crate-wasi-common-cbindgen-0.10.0 (c (n "wasi-common-cbindgen") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.6") (d #t) (k 0)) (d (n "quote") (r "^1.0.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.4") (d #t) (k 2)))) (h "0bq2jqx503p6cr48z952v8jlzxphrkfbcq098pk4r6ylp2hg85lq")))

