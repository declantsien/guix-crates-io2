(define-module (crates-io wa si wasi-print) #:use-module (crates-io))

(define-public crate-wasi-print-0.2.0 (c (n "wasi-print") (v "0.2.0") (d (list (d (n "compiler_builtins") (r "^0.1") (f (quote ("mem"))) (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (k 0)) (d (n "wasi") (r "^0.11") (k 0)))) (h "1cyfb5v1gdl146g7cdwv72s785kyfpplnn526kr4g5jbpx7c96ci")))

(define-public crate-wasi-print-0.2.1 (c (n "wasi-print") (v "0.2.1") (d (list (d (n "compiler_builtins") (r "^0.1") (f (quote ("mem"))) (o #t) (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (k 0)) (d (n "wasi") (r "^0.11") (k 0)))) (h "1k82c8q0l5ry7di2py3cg0ns3xg78islmw999hr7ihvcbpf9hr82") (f (quote (("print" "dlmalloc" "compiler_builtins") ("panic-handler") ("default" "print" "panic-handler"))))))

(define-public crate-wasi-print-0.2.2 (c (n "wasi-print") (v "0.2.2") (d (list (d (n "compiler_builtins") (r "^0.1") (f (quote ("mem"))) (o #t) (k 0)) (d (n "dlmalloc") (r "^0.2") (f (quote ("global"))) (o #t) (k 0)) (d (n "wasi") (r "^0.11") (k 0)))) (h "1as79a72xggbg7z569fgppkd6qdkim3zmsds904lvpxaq47anwp0") (f (quote (("print" "dlmalloc" "compiler_builtins") ("panic-handler") ("default" "print" "panic-handler"))))))

