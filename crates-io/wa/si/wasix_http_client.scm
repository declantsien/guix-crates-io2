(define-module (crates-io wa si wasix_http_client) #:use-module (crates-io))

(define-public crate-wasix_http_client-0.1.0 (c (n "wasix_http_client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "wai-bindgen-rust") (r "^0.2.2") (d #t) (k 0)))) (h "1knlrl5a39f98z1ia06kh81fri4swlg83my4h30dgm6b6kg7qdhr")))

(define-public crate-wasix_http_client-0.2.0 (c (n "wasix_http_client") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "wai-bindgen-rust") (r "^0.2.2") (d #t) (k 0)))) (h "1p0wrf0v62y8v3ql8z15i09s0l2b04p58jyga08aj2lv49lf26z3") (r "1.67")))

(define-public crate-wasix_http_client-0.3.0 (c (n "wasix_http_client") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "wai-bindgen-rust") (r "^0.2.2") (d #t) (k 0)))) (h "00k8s4rfdkjc644cqr1s1rhyvf310gx4qsrjvkam9n3k9p76wvfg") (r "1.67")))

(define-public crate-wasix_http_client-0.4.0 (c (n "wasix_http_client") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.66") (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "wai-bindgen-rust") (r "^0.2.2") (d #t) (k 0)))) (h "0wqzis0izd5spw15innpjznn186n5pfc0vpnmpgfbpf6h66qznqj") (r "1.67")))

