(define-module (crates-io wa tt watt) #:use-module (crates-io))

(define-public crate-watt-0.0.0 (c (n "watt") (v "0.0.0") (h "13lwzmdga5v280gmjyhh7nghnv94d5lgm9p3hjs75pr47caf35a1") (y #t)))

(define-public crate-watt-0.1.0 (c (n "watt") (v "0.1.0") (h "02cab5drzvg0v25ksxagk3717h6psld7bqdryn6lj5jpdl34swp0")))

(define-public crate-watt-0.1.1 (c (n "watt") (v "0.1.1") (h "1p29c8c7p4a1ywcx53s49n3f9h0vb2w50s2vwwmakmyzkacf188j")))

(define-public crate-watt-0.1.2 (c (n "watt") (v "0.1.2") (h "00fh56zh69l8dk99z0y10v9q54n6b68yvn569kih988v5f45daca")))

(define-public crate-watt-0.1.3 (c (n "watt") (v "0.1.3") (h "1zysinjrlqbbsh48fvszqqqn90n6lc3mafz3099x9kak91zkwnw8")))

(define-public crate-watt-0.2.0 (c (n "watt") (v "0.2.0") (h "1p5hqjdzswsfmaz73x042ad82ka6dns2sw2q2vvva1nghyjp7yji")))

(define-public crate-watt-0.2.1 (c (n "watt") (v "0.2.1") (h "0m553x759qq36hwkvpc07blf4jkh7fydfqzjn873n86k7xr7plwf")))

(define-public crate-watt-0.3.0 (c (n "watt") (v "0.3.0") (h "1mr5p1rslpa9xwd4jv8yqzs8dz31ihipwyqdqc7lqqfhmhycfgxa")))

(define-public crate-watt-0.4.0 (c (n "watt") (v "0.4.0") (h "0j7px01lh6aqyfrv6lglkp42x8bh4k4klva95xpq0k8rpracf9nn")))

(define-public crate-watt-0.4.1 (c (n "watt") (v "0.4.1") (h "1kvv64kfzjhnlqgc0n6llh16r1281yisnz9cs03g9smkf3ry6vxy")))

(define-public crate-watt-0.4.2 (c (n "watt") (v "0.4.2") (h "1q2zk6nhwqas7lj2iyxfczyfcgpnj5nfvrm3dryx17l94dy2kz3d")))

(define-public crate-watt-0.4.3 (c (n "watt") (v "0.4.3") (h "1lj80ybnbw3sj9j3jca9snfn7pl6i4a2bl84zp1skvaa2ay0j7w0")))

(define-public crate-watt-0.4.4 (c (n "watt") (v "0.4.4") (h "1ikw9lx19fh21qr0v8anpz3kczavz0b684k9bsa7015r8v0vll6s") (y #t)))

(define-public crate-watt-0.3.1 (c (n "watt") (v "0.3.1") (h "1jyshr9qcbbr0b1vbvgw61ssz4nyxsdbxwf9rqjc02kqyzyg3z7d")))

(define-public crate-watt-0.4.5 (c (n "watt") (v "0.4.5") (h "04v2khmyfhsbzawann4n8q8cggrz8hf8kxz7gqhnaaj2b47n81lp") (y #t)))

(define-public crate-watt-0.4.6 (c (n "watt") (v "0.4.6") (h "1k4d2pskqqgyafzax5ly1pqfvaffq2rrd0xp8m1dlr0848dhpzhs")))

(define-public crate-watt-0.5.0 (c (n "watt") (v "0.5.0") (h "12djvzyjbdw07yszyb69376x8dysfdq7prg85lkx6554pla27gxk")))

