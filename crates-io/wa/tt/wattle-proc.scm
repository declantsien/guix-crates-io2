(define-module (crates-io wa tt wattle-proc) #:use-module (crates-io))

(define-public crate-wattle-proc-0.1.0 (c (n "wattle-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)) (d (n "wattle-field") (r "^0.1.1") (d #t) (k 0)))) (h "0y2d6b6i2095nqs0slhrp8kwczdacjpwg7s044lc7mkahbchb9xy")))

(define-public crate-wattle-proc-0.1.1 (c (n "wattle-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)) (d (n "wattle-field") (r "^0.1.2") (d #t) (k 0)))) (h "195j09w0hx64lpwcdbhnrb14vyivjla9nsd592fkinszjph026kz")))

(define-public crate-wattle-proc-0.1.2 (c (n "wattle-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)) (d (n "wattle-field") (r "^0.1.3") (d #t) (k 0)))) (h "19hmg3jr5c95q3b3gh03jm233v0pvgy5a6kqwnn5h1avh8z3bcmw") (y #t)))

(define-public crate-wattle-proc-0.1.3 (c (n "wattle-proc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)) (d (n "wattle-field") (r "^0.1.3") (d #t) (k 0)))) (h "071hfdrgm0k3mz1mwkwzn4n5a0qyl0sda6x14510jazfga19yr8c")))

(define-public crate-wattle-proc-0.1.4 (c (n "wattle-proc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1580hwzgh384j9psrz7f7pjb1bvqfrs8nmnaryxlv1zclk5amaij")))

(define-public crate-wattle-proc-0.1.5 (c (n "wattle-proc") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1k9p3pdh7ds069ysw4hr3d0k1am6f7kcnrdhb18lv00rzn34v5q9")))

(define-public crate-wattle-proc-0.1.6 (c (n "wattle-proc") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.32") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (d #t) (k 0)))) (h "1vbx23h7c03364d4v561kx97ypvvf91abwj844pjp7zykn7qrgk1")))

