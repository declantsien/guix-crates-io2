(define-module (crates-io wa tt wattbar) #:use-module (crates-io))

(define-public crate-wattbar-0.1.0 (c (n "wattbar") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "calloop") (r "^0.9.3") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)) (d (n "smithay-client-toolkit") (r "^0.15") (f (quote ("calloop"))) (k 0)) (d (n "upower_dbus") (r "^0.2.0") (d #t) (k 0)) (d (n "wayland-client") (r "^0.29") (d #t) (k 0)) (d (n "wayland-protocols") (r "^0.29") (d #t) (k 0)) (d (n "zbus") (r "^2.2.0") (d #t) (k 0)))) (h "1cz473rkr0w2m96ijdd622ccv33bwdy5p1as4rdqinvkqm8p9j4x")))

