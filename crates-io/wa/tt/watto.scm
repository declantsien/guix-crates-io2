(define-module (crates-io wa tt watto) #:use-module (crates-io))

(define-public crate-watto-0.1.0 (c (n "watto") (v "0.1.0") (d (list (d (n "leb128") (r "^0.2.5") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.33") (o #t) (d #t) (k 0)))) (h "0awlpq4khplpxjlkyrdz14nwji8d4s1bnzh458l48wa1bqqvaik7") (f (quote (("writer" "std") ("strings" "std" "leb128" "thiserror") ("std"))))))

