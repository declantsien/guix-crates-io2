(define-module (crates-io wa tt wattle-field) #:use-module (crates-io))

(define-public crate-wattle-field-0.1.0 (c (n "wattle-field") (v "0.1.0") (h "04wgq9r6bhbp9gld7qabcf47350n53nji0wni9il48ph87cpsc8w") (y #t)))

(define-public crate-wattle-field-0.1.1 (c (n "wattle-field") (v "0.1.1") (h "0pqrl1x90a5dw26hs01cxyp30s7gxw1l46hkiqnqxnm5ahh20g9i") (y #t)))

(define-public crate-wattle-field-0.1.2 (c (n "wattle-field") (v "0.1.2") (h "13kl38xydy1315mwdby5p0h93ki9p3xksfd9x30w2v8d50cpp1f5") (y #t)))

(define-public crate-wattle-field-0.1.3 (c (n "wattle-field") (v "0.1.3") (h "1xbwh7rwv9yd0kr3wqv9fgjfkh7rb5agnlhbrgrdn8dkksl8jv2m") (y #t)))

