(define-module (crates-io wa tt wattle) #:use-module (crates-io))

(define-public crate-wattle-0.0.1 (c (n "wattle") (v "0.0.1") (d (list (d (n "url") (r "^2.4.0") (o #t) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (o #t) (d #t) (k 0)))) (h "1pnaci4nx6l3wfxd34c9q6iga3c1a9bs7sbgish1clnp8x4jb4p8") (f (quote (("web_warp" "warp" "url")))) (y #t)))

(define-public crate-wattle-0.0.2 (c (n "wattle") (v "0.0.2") (d (list (d (n "calamine") (r "^0.21.2") (o #t) (d #t) (k 0)) (d (n "mysql") (r "^24.0.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (o #t) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "xlsxwriter") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "06dxzsl0hyjbsns2296nwcc5wjyw4mkwv7pjv7ahlfdm0144bavb") (f (quote (("web_warp" "warp" "url") ("sql" "mysql") ("excel" "xlsxwriter" "calamine")))) (y #t)))

(define-public crate-wattle-0.0.4 (c (n "wattle") (v "0.0.4") (d (list (d (n "mysql") (r "^24.0.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (o #t) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "wattle-field") (r "^0.1.3") (o #t) (d #t) (k 0)) (d (n "wattle-proc") (r "^0.1.3") (o #t) (d #t) (k 0)))) (h "0g0zywzps58mj2s26rpajqpmkr4dfnhkl0y8xgjnp35z7770xaad") (f (quote (("web_warp" "warp" "url") ("sql" "mysql" "wattle-proc" "wattle-field")))) (y #t)))

(define-public crate-wattle-0.0.5 (c (n "wattle") (v "0.0.5") (d (list (d (n "mysql") (r "^24.0.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (o #t) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "wattle-proc") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "0bmxnmjc5mdps43s1vl8ghc06jv2gzf8j1ds146zhfwzy5bf484c") (f (quote (("web_warp" "warp" "url") ("sql" "mysql" "wattle-proc")))) (y #t)))

(define-public crate-wattle-0.0.7 (c (n "wattle") (v "0.0.7") (d (list (d (n "mysql") (r "^24.0.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (o #t) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "wattle-proc") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "10s8jwzc09wwiw7lpq2yr3a9n5y2yg4lsydf9dgmqpf4ggf1ndf2") (f (quote (("web_warp" "warp" "url") ("sql" "mysql" "wattle-proc")))) (y #t)))

(define-public crate-wattle-0.0.8 (c (n "wattle") (v "0.0.8") (d (list (d (n "mysql") (r "^24.0.0") (o #t) (d #t) (k 0)) (d (n "url") (r "^2.4.0") (o #t) (d #t) (k 0)) (d (n "warp") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "wattle-proc") (r "^0.1.6") (o #t) (d #t) (k 0)))) (h "0ar0gxfqliixgg00q96nz9l5l6581hhmbx67qydj6xirl4afqcyj") (f (quote (("web_warp" "warp" "url") ("sql" "mysql" "wattle-proc")))) (y #t)))

(define-public crate-wattle-0.0.9 (c (n "wattle") (v "0.0.9") (d (list (d (n "mysql") (r "^24.0.0") (d #t) (k 0)) (d (n "mysql_common") (r "^0.30.6") (d #t) (k 0)))) (h "1gj3wr5w2jka613cczp6n43zi25jwm690r5vsdrmma14i4qaia2i")))

