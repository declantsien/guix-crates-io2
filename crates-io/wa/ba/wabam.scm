(define-module (crates-io wa ba wabam) #:use-module (crates-io))

(define-public crate-wabam-0.1.0 (c (n "wabam") (v "0.1.0") (h "0m1abdmsy2rj4yiak7kj93l8q3k053amwgfl80q0kp1m35yx3h2k") (y #t)))

(define-public crate-wabam-0.1.1 (c (n "wabam") (v "0.1.1") (h "10mj4sw9j9hwl8k8hpjspnyjg3jldlk3jxwj8zlxfidn6cb1g8m1") (y #t)))

(define-public crate-wabam-0.1.2 (c (n "wabam") (v "0.1.2") (h "0clscl1ggsnzymzdpxp78xmx578svjc6zijirlrxzwwjaxla1a5f")))

(define-public crate-wabam-0.2.0 (c (n "wabam") (v "0.2.0") (h "138igfnfv4mjk06kqxzykc758lj7q8pa40clp7cbb32w3kc6kvdw")))

(define-public crate-wabam-0.3.0 (c (n "wabam") (v "0.3.0") (h "1a4j62hanrcgjia93d9l5zm2lk53r9igrldyvfikdjbldclgv9ig")))

