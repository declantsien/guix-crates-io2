(define-module (crates-io wa in wain-exec) #:use-module (crates-io))

(define-public crate-wain-exec-0.1.0 (c (n "wain-exec") (v "0.1.0") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)) (d (n "wain-syntax-text") (r "^0.1") (d #t) (k 2)) (d (n "wain-validate") (r "^0.1") (d #t) (k 2)))) (h "09744n6n6j18r5vj4czsaa8sf8i2y3p6ql9zrflzpjfbmbfp5dqw")))

(define-public crate-wain-exec-0.1.1 (c (n "wain-exec") (v "0.1.1") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)) (d (n "wain-syntax-text") (r "^0.1") (d #t) (k 2)) (d (n "wain-validate") (r "^0.1") (d #t) (k 2)))) (h "0vi8bkz7vll0pslp4h2rn5alxgdxs3f5lzdd529gcyf3cvll0a0m")))

(define-public crate-wain-exec-0.1.2 (c (n "wain-exec") (v "0.1.2") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)) (d (n "wain-syntax-text") (r "^0.1") (d #t) (k 2)) (d (n "wain-validate") (r "^0.1") (d #t) (k 2)))) (h "055xfmi1hy6325c1vpz46gb8ybjs6y9cayasbm9j0j0ajkc45dnz")))

(define-public crate-wain-exec-0.2.0 (c (n "wain-exec") (v "0.2.0") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)) (d (n "wain-syntax-text") (r "^0.1") (d #t) (k 2)) (d (n "wain-validate") (r "^0.1") (d #t) (k 2)))) (h "1cm5nzz1ci47ig3hvf9aq8w8275g4ly5vm07sg79rmfdbj5kns64")))

(define-public crate-wain-exec-0.2.1 (c (n "wain-exec") (v "0.2.1") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)) (d (n "wain-syntax-text") (r "^0.1") (d #t) (k 2)) (d (n "wain-validate") (r "^0.1") (d #t) (k 2)))) (h "1r07q3p7nyfvqmva6jk80lypwpazx526klr7p17swnhr482gc8xi")))

(define-public crate-wain-exec-0.2.2 (c (n "wain-exec") (v "0.2.2") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)) (d (n "wain-syntax-text") (r "^0.1") (d #t) (k 2)) (d (n "wain-validate") (r "^0.1") (d #t) (k 2)))) (h "0k23vz5h3axgfz4x5ngxs18ifbcir6r7vbp9gz2nmns8ccmxgyln")))

(define-public crate-wain-exec-0.3.0 (c (n "wain-exec") (v "0.3.0") (d (list (d (n "wain-ast") (r "^0.2") (d #t) (k 0)) (d (n "wain-syntax-text") (r "^0.2") (d #t) (k 2)) (d (n "wain-validate") (r "^0.1") (d #t) (k 2)))) (h "0c6dm92g5vljspdjs5ly4a34rfcr9x6ibd6fmbbbr953x74a9c6s")))

(define-public crate-wain-exec-0.3.1 (c (n "wain-exec") (v "0.3.1") (d (list (d (n "wain-ast") (r "^0.2") (d #t) (k 0)) (d (n "wain-syntax-text") (r "^0.2") (d #t) (k 2)) (d (n "wain-validate") (r "^0.1") (d #t) (k 2)))) (h "1916382s7xbry3xqzinzsai8rnkfyibw1zdxh7p6dhz4y29z2l36")))

