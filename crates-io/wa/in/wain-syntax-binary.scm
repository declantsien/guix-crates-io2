(define-module (crates-io wa in wain-syntax-binary) #:use-module (crates-io))

(define-public crate-wain-syntax-binary-0.1.0 (c (n "wain-syntax-binary") (v "0.1.0") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)))) (h "12na4wx2iy7jh9rqkm2xsy4fx2gycprh2d7421scrlaky34by6im")))

(define-public crate-wain-syntax-binary-0.1.1 (c (n "wain-syntax-binary") (v "0.1.1") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)))) (h "04qc7kf6jqxq26999n9gpdfcsh1j8qf781rxbzsi3384gk5n7ggn")))

(define-public crate-wain-syntax-binary-0.1.2 (c (n "wain-syntax-binary") (v "0.1.2") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)))) (h "08mfcsml1rv90y612bsl88swrfkbs3dxf7qq9iznzrza309hgml5")))

(define-public crate-wain-syntax-binary-0.1.3 (c (n "wain-syntax-binary") (v "0.1.3") (d (list (d (n "wain-ast") (r "^0.2") (d #t) (k 0)))) (h "1zb69aaxbyrgfp3jdz70w16s6mn12fgpkw0vblpamkvg1nzhr0a7")))

(define-public crate-wain-syntax-binary-0.1.4 (c (n "wain-syntax-binary") (v "0.1.4") (d (list (d (n "wain-ast") (r "^0.2") (d #t) (k 0)))) (h "1z2z7q3fj1qjk5hma608sall39irv30flynp2xq52rlbax0wmyjn")))

(define-public crate-wain-syntax-binary-0.1.5 (c (n "wain-syntax-binary") (v "0.1.5") (d (list (d (n "wain-ast") (r "^0.2") (d #t) (k 0)))) (h "1qc015b3nrf3nyxk8i87ygkpld602w6n6y4x0y0fx1974xjppyrq")))

