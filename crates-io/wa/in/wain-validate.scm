(define-module (crates-io wa in wain-validate) #:use-module (crates-io))

(define-public crate-wain-validate-0.1.0 (c (n "wain-validate") (v "0.1.0") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)))) (h "03drb6r4ha97f3rfmm2v166zb1z16vbwznbkxbg5b874jixh5rwy")))

(define-public crate-wain-validate-0.1.1 (c (n "wain-validate") (v "0.1.1") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)))) (h "0rhxql7vrg13riy72aqmkcvl8iqb2069jddkilv8ha2wn5wn8rcl")))

(define-public crate-wain-validate-0.1.2 (c (n "wain-validate") (v "0.1.2") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)))) (h "0j43fwflxldg52bz9qr3m47lckclbkv4pwi0kljw9agkzxxb1d1q")))

(define-public crate-wain-validate-0.1.3 (c (n "wain-validate") (v "0.1.3") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)))) (h "0jsldadlfjk5ll9hrlvn0qkrbs8cv4193qdwf0r00hnkvgf1vlaz")))

(define-public crate-wain-validate-0.1.4 (c (n "wain-validate") (v "0.1.4") (d (list (d (n "wain-ast") (r "^0.2") (d #t) (k 0)))) (h "1aza79a6g1hcnip0ijqz1dnia16nw7q8zvny6pcn1ks5mdhlf9c7")))

(define-public crate-wain-validate-0.1.5 (c (n "wain-validate") (v "0.1.5") (d (list (d (n "wain-ast") (r "^0.2") (d #t) (k 0)))) (h "0f3lp5x2p64763czr704wr0yfr6x0gd78w3skyckx94wqlncrbqn")))

