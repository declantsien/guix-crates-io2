(define-module (crates-io wa in wain-ast) #:use-module (crates-io))

(define-public crate-wain-ast-0.0.0 (c (n "wain-ast") (v "0.0.0") (h "1z75kp4pqicp2b95szy1z8dlj1f5f9x7b8886r233s1g934258hs")))

(define-public crate-wain-ast-0.1.0 (c (n "wain-ast") (v "0.1.0") (h "1m6m6ap34m637bsrh40kb8zh3wks3ph67zf6a23gzfnb82gn76fa")))

(define-public crate-wain-ast-0.1.1 (c (n "wain-ast") (v "0.1.1") (h "1l205vd03188lbzq82xzvw5wmsfxh95zp84fz2qb99k8mf8rayly")))

(define-public crate-wain-ast-0.1.2 (c (n "wain-ast") (v "0.1.2") (h "01qlc2rqhnamjcaqw73jkzlkz0vkimjbxwxga1i7bd2l85cdi5hr")))

(define-public crate-wain-ast-0.1.3 (c (n "wain-ast") (v "0.1.3") (h "1ml35pii726qzbrzcjwv6gmrxl6m8p05ki5kp3inakgqh08aw7g2")))

(define-public crate-wain-ast-0.2.0 (c (n "wain-ast") (v "0.2.0") (h "113fnza73mb5zkswb96q17k0fy6scm4j8zj1h159bp1fj6kj8a7p")))

(define-public crate-wain-ast-0.2.1 (c (n "wain-ast") (v "0.2.1") (h "0k3pyf9dqzszpiskrla9lq1vr6fisbm50myv8g1v8n0gi849gh99")))

(define-public crate-wain-ast-0.2.2 (c (n "wain-ast") (v "0.2.2") (h "0mv2n1qnq81nx7h6d29xrhq3r0c5bgbmzgwbmkajax3wpr5y53z6")))

