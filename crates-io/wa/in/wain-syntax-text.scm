(define-module (crates-io wa in wain-syntax-text) #:use-module (crates-io))

(define-public crate-wain-syntax-text-0.1.0 (c (n "wain-syntax-text") (v "0.1.0") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)))) (h "0ziinay6qrf1ya5c9aqrwmnr7a56r9hhzgvh6vhasrna4x14madb")))

(define-public crate-wain-syntax-text-0.1.1 (c (n "wain-syntax-text") (v "0.1.1") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)))) (h "1lya3l30lc6y8vf5418n04ag7nwi9z91amly1rdc91q0sg9yaww5")))

(define-public crate-wain-syntax-text-0.1.2 (c (n "wain-syntax-text") (v "0.1.2") (d (list (d (n "wain-ast") (r "^0.1") (d #t) (k 0)))) (h "04hwz7yxzybrlm88h268ipznhz29ikqlwla9373wzpk4dn36w951")))

(define-public crate-wain-syntax-text-0.2.0 (c (n "wain-syntax-text") (v "0.2.0") (d (list (d (n "wain-ast") (r "^0.2") (d #t) (k 0)))) (h "0mm41227f065krrrw2b9ll047m89q3ckmgcar3vgh7grv9y0hy7k")))

(define-public crate-wain-syntax-text-0.2.1 (c (n "wain-syntax-text") (v "0.2.1") (d (list (d (n "wain-ast") (r "^0.2") (d #t) (k 0)))) (h "06l8ls7mxlvdh4grkcgamih0i7qa05ljiryzh59bff35bvi7g4k9")))

