(define-module (crates-io wa sh wash-syscall) #:use-module (crates-io))

(define-public crate-wash-syscall-0.0.0 (c (n "wash-syscall") (v "0.0.0") (h "0cp9vcy8zj0hvrwwmdxqlvpq7hsm9idwhpb06agryrpzi1cl5bdl")))

(define-public crate-wash-syscall-0.0.1 (c (n "wash-syscall") (v "0.0.1") (h "15r5fh3jrif9m3p8bkhn5bw20lw52z68qg82k7fsxdi53jrcypkg")))

