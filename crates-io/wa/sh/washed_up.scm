(define-module (crates-io wa sh washed_up) #:use-module (crates-io))

(define-public crate-washed_up-0.1.0 (c (n "washed_up") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "coroutine") (r "*") (d #t) (k 0)) (d (n "log") (r "^0.3.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "uuid") (r "^0.1") (d #t) (k 0)))) (h "15b12n7ralxxwcisjmflzc8xv52v528j6bqy32wwcvdxz4ii5rif")))

