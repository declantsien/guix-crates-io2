(define-module (crates-io wa mb wambo) #:use-module (crates-io))

(define-public crate-wambo-0.1.0 (c (n "wambo") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0zhslihx79hj0ph4k90d8wnx1q15275iklgqpdxg22xsr4l6v2ww")))

(define-public crate-wambo-0.1.1 (c (n "wambo") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "00zifa1jr2r50r4yxfg8h4dcnbd95sppp1fm16wr5k4ih76gyj74")))

(define-public crate-wambo-0.1.2 (c (n "wambo") (v "0.1.2") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1qx6m3cvs6ryjshnzwds4ngkw43brgd3hs4sbhwcvkywm5h9m7yl")))

(define-public crate-wambo-0.1.3 (c (n "wambo") (v "0.1.3") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1zqskbv8ni6z84k7m2x3i1fsnwsmwpr4zvkck90c2jkry1nrcvnv")))

(define-public crate-wambo-0.1.4 (c (n "wambo") (v "0.1.4") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0ggarbnjwfj3dggnwvg654nixkfwpz6gqc71bjafs9jzkzadgfaf")))

(define-public crate-wambo-0.1.5 (c (n "wambo") (v "0.1.5") (d (list (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "08q5vla70ywhhk7746xb1wz5w35bmnjszrdyyyynl4dinv7wkw82")))

(define-public crate-wambo-0.2.0 (c (n "wambo") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1qqlx02n4zh9la5z5j71zwn4wdzlal6pkjr4pnj511gaarbla3d6")))

(define-public crate-wambo-0.2.1 (c (n "wambo") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "14nd3zrzl62ky9c16hm49gsi5wbzlvfyj5ppnm9jj5fx0nj7pcf1")))

(define-public crate-wambo-0.2.2 (c (n "wambo") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0fmagiq5i36kykc3gd8mzi3009p9wgkxcs4wrfnbx0jgahdzgdnm")))

(define-public crate-wambo-0.2.3 (c (n "wambo") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "12rgyihappi8kr0sjpkvjns0zcsk8hg567c99mjs4xw4x6n0g4qc")))

(define-public crate-wambo-0.2.4 (c (n "wambo") (v "0.2.4") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0bdh96bizrw0i2c9pmwvfi67b2lm8dql1w67sdmlxwhkifs9kr2v")))

(define-public crate-wambo-0.2.5 (c (n "wambo") (v "0.2.5") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0b25crn90jrawgivmkx6a1mbks8y2dyn6dcv2xrbb0f8x855wcz1")))

(define-public crate-wambo-0.2.6 (c (n "wambo") (v "0.2.6") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0kvd005iapk7lw8h1sgrjjk1d02abncnrqjvhvhbawxlsax6x1gv") (y #t)))

(define-public crate-wambo-0.2.7 (c (n "wambo") (v "0.2.7") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1jdvvfjp1mhibwn9mwyxxf2xkzj5a6m5wqk4vvlhxxxph2frj1ly")))

(define-public crate-wambo-0.2.8 (c (n "wambo") (v "0.2.8") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0nadk2dcaa51p0dhly63bc7rfgbkq5g2krvmwl9jszrlcfr0aqb4")))

(define-public crate-wambo-0.2.9 (c (n "wambo") (v "0.2.9") (d (list (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "18zg4ry4nm83qx6vfhnfi1l2pjr5nvlzbqaq3k0l5cphbdww4cnf")))

(define-public crate-wambo-0.2.10 (c (n "wambo") (v "0.2.10") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "1l9qdxn5b3wfqad836x5h2gfsc9vkrwmv0xs4djj2mxi3abzx978")))

(define-public crate-wambo-0.2.11 (c (n "wambo") (v "0.2.11") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (d #t) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^1.4.3") (d #t) (k 0)))) (h "18y0s1scbvsagrd14c3f8bx8gar888mdg44kwc8wphy3434k9grl")))

(define-public crate-wambo-0.2.12 (c (n "wambo") (v "0.2.12") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.11") (f (quote ("display"))) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 0)))) (h "1ymyghgcj5rkhp3xbi0q3p4460xg9s6k3f1ld0vd50rbq8aa88lp")))

(define-public crate-wambo-0.3.0 (c (n "wambo") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0199z91y7lqzmlwgs92zjghkz0hn0m1gw22982kahf7qg5rbp30q")))

(define-public crate-wambo-0.3.1 (c (n "wambo") (v "0.3.1") (d (list (d (n "crossterm") (r "^0.25") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (f (quote ("display"))) (k 0)) (d (n "fraction_list_fmt_align") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "tui") (r "^0.19") (d #t) (k 0)))) (h "0i97ipgkma0rcbgnh955m8mk5hm3bkfwgmflcra434s7habzd833")))

