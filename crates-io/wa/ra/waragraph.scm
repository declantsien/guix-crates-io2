(define-module (crates-io wa ra waragraph) #:use-module (crates-io))

(define-public crate-waragraph-0.0.1 (c (n "waragraph") (v "0.0.1") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "internment") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nalgebra") (r "^0.30") (d #t) (k 0)) (d (n "nalgebra-sparse") (r "^0.6") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "thunderdome") (r "^0.5") (d #t) (k 0)))) (h "1cq8v0ps5dl9ia9w49ribfz2sagpbswpqpk2qbcbj6xl9jg0bw4k")))

