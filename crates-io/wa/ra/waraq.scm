(define-module (crates-io wa ra waraq) #:use-module (crates-io))

(define-public crate-waraq-0.1.0 (c (n "waraq") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (f (quote ("jpeg" "png"))) (k 0)) (d (n "infer") (r "^0.11.0") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "resize") (r "^0.7.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8.35") (d #t) (k 0)) (d (n "x11rb") (r "^0.11.1") (f (quote ("image"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0jixdmsdd5bd7kn53ykzvzpx1yd7zqrfzapjz6w96r93yiivqygz")))

(define-public crate-waraq-0.1.1 (c (n "waraq") (v "0.1.1") (d (list (d (n "image") (r "^0.24.5") (f (quote ("jpeg" "png"))) (k 0)) (d (n "infer") (r "^0.11.0") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "resize") (r "^0.7.4") (d #t) (k 0)) (d (n "rgb") (r "^0.8.35") (d #t) (k 0)) (d (n "x11rb") (r "^0.11.1") (f (quote ("image"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0c9jd0sbxchgxkvwkll7yd1wqhiy0bixhzxnbwf9g89yq2qcl0mr")))

(define-public crate-waraq-0.2.1 (c (n "waraq") (v "0.2.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "windows") (r "^0.48.0") (f (quote ("Win32_UI_WindowsAndMessaging" "Win32_Foundation"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "x11rb") (r "^0.12.0") (f (quote ("image"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "02mbfmlk1scjyvwhr1wjvimdf4jdhjqgw33d5r66zb3q46isjnnh")))

