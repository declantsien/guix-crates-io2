(define-module (crates-io wa sc wascc-telnet) #:use-module (crates-io))

(define-public crate-wascc-telnet-0.1.0 (c (n "wascc-telnet") (v "0.1.0") (d (list (d (n "ansi-escapes") (r "^0.1.0") (d #t) (k 0)) (d (n "crossbeam") (r "^0.7.3") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.111") (d #t) (k 0)) (d (n "telnet") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)) (d (n "wascc-codec") (r "^0.7.0") (d #t) (k 0)))) (h "17sz2x45h2z9zgilwvdx16d865jy6jhblk9hp74zb3yxpx2n7yq0") (f (quote (("static_plugin"))))))

