(define-module (crates-io wa sc wasccgraph-common) #:use-module (crates-io))

(define-public crate-wasccgraph-common-0.0.1 (c (n "wasccgraph-common") (v "0.0.1") (d (list (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)))) (h "0f9a9990yif3ysav4yi47gflgi1frhsp8ym55rhd329n6qly1lvi")))

(define-public crate-wasccgraph-common-0.0.2 (c (n "wasccgraph-common") (v "0.0.2") (d (list (d (n "serde") (r "^1.0.110") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.110") (d #t) (k 0)))) (h "0ici5darzi837x2c52dpx20ar3vssxijqdps3fxw7a84yy8isg7a")))

