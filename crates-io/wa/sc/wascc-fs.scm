(define-module (crates-io wa sc wascc-fs) #:use-module (crates-io))

(define-public crate-wascc-fs-0.0.4 (c (n "wascc-fs") (v "0.0.4") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.6.0") (d #t) (k 0)))) (h "19gfbn1nxq90p2d20ydhb33wipy4vpri0849fayrwhnkyg46lmb7") (f (quote (("static_plugin"))))))

(define-public crate-wascc-fs-0.0.5 (c (n "wascc-fs") (v "0.0.5") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.6.0") (d #t) (k 0)))) (h "146x7azd7arvl6v7frcyqmxgnh9pn3zm9p22r1k2z4yjss444k5v") (f (quote (("static_plugin"))))))

(define-public crate-wascc-fs-0.1.0 (c (n "wascc-fs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.7.0") (d #t) (k 0)))) (h "16hr5xb6627kf1c84rqp7gs7nns76aljnqdl2q1c9z22w6vmrk5h") (f (quote (("static_plugin"))))))

(define-public crate-wascc-fs-0.1.1 (c (n "wascc-fs") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.7.2") (d #t) (k 0)))) (h "1sn04xpphbljswlfs98fhbmka8bl5b5k2zpawxrqpvl1yqyjc24r") (f (quote (("static_plugin"))))))

(define-public crate-wascc-fs-0.1.2 (c (n "wascc-fs") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.8.1") (d #t) (k 0)))) (h "1avrbk3gb85sjhaw7jxp77w7hk9x08snr44agzi1xgjag94v82v1") (f (quote (("static_plugin"))))))

