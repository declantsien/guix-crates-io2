(define-module (crates-io wa sc wasccgraph-actor) #:use-module (crates-io))

(define-public crate-wasccgraph-actor-0.0.1 (c (n "wasccgraph-actor") (v "0.0.1") (d (list (d (n "wascc-actor") (r "^0.7.1") (d #t) (k 0)) (d (n "wasccgraph-common") (r "^0.0.1") (d #t) (k 0)))) (h "07dymib2fpzcdgn3pjb504r2gq6lh1d2fp9lnfvawr3hpabyp1j1")))

(define-public crate-wasccgraph-actor-0.0.2 (c (n "wasccgraph-actor") (v "0.0.2") (d (list (d (n "wascc-actor") (r "^0.7.1") (d #t) (k 0)) (d (n "wasccgraph-common") (r "^0.0.2") (d #t) (k 0)))) (h "184rgdvqxiq25d69g5pdzsr2k71d1wk7md1ry34zgk5wyjxvhdsk")))

