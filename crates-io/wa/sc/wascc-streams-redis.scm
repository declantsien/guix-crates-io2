(define-module (crates-io wa sc wascc-streams-redis) #:use-module (crates-io))

(define-public crate-wascc-streams-redis-0.2.0 (c (n "wascc-streams-redis") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "redis-streams") (r "^0.1.0") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.6.0") (d #t) (k 0)))) (h "0jkbzpa9sglm49wjx7ybr61lfcdv14fs2h9rgl13p8vswyyi4ica") (f (quote (("static_plugin"))))))

(define-public crate-wascc-streams-redis-0.3.0 (c (n "wascc-streams-redis") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "redis-streams") (r "^0.1.1") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.7.0") (d #t) (k 0)))) (h "1rhs7yram8ndmmqy1hwfqsjvqq63sg696r5145sznfz3ip2dvh21") (f (quote (("static_plugin"))))))

