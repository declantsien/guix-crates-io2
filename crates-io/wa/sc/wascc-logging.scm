(define-module (crates-io wa sc wascc-logging) #:use-module (crates-io))

(define-public crate-wascc-logging-0.6.0 (c (n "wascc-logging") (v "0.6.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.6.0") (d #t) (k 0)))) (h "1kp11dwspvbh0dnp67n5ab7hx7yfz3jwdaxyaya6xabh8l6vwqf6") (f (quote (("static_plugin"))))))

(define-public crate-wascc-logging-0.7.0 (c (n "wascc-logging") (v "0.7.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.7.0") (d #t) (k 0)))) (h "0hmjhl56x41251xqb8000sq7wc3nfjmd6h0mrar3203jk1rf8gp4") (f (quote (("static_plugin"))))))

(define-public crate-wascc-logging-0.8.0 (c (n "wascc-logging") (v "0.8.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.8.0") (d #t) (k 0)))) (h "1m3b3z3xc8qn2s6r8gz755d2324rni60734lglzvnzrd72hvazfw") (f (quote (("static_plugin"))))))

