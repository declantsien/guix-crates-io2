(define-module (crates-io wa sc wascap-guest) #:use-module (crates-io))

(define-public crate-wascap-guest-0.0.1 (c (n "wascap-guest") (v "0.0.1") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "wascap-codec") (r "^0.0.1") (d #t) (k 0)))) (h "17y6xlwzs96v81x28968aplmfdpmljdj7rkmmvw10i3xiz98j7lw") (y #t)))

(define-public crate-wascap-guest-0.0.2 (c (n "wascap-guest") (v "0.0.2") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "wascap-codec") (r "^0.0.2") (d #t) (k 0)))) (h "0glny4g2g5vxf114l5xlh7cg3yk3mdns0zbpz1zijjlqdmsgrp5w") (y #t)))

(define-public crate-wascap-guest-0.0.3 (c (n "wascap-guest") (v "0.0.3") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "wascap-codec") (r "^0.0.2") (d #t) (k 0)))) (h "1izr2ymdvkvyjr068wspgwynkwf0v28c4acx6yma92y5nv7wvj2y") (y #t)))

(define-public crate-wascap-guest-0.0.4 (c (n "wascap-guest") (v "0.0.4") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "wascap-codec") (r "^0.0.2") (d #t) (k 0)))) (h "1ql24mp4in5cx9a32aq5073cd57hx6pkl8cja3ci9wdav980l47m") (y #t)))

(define-public crate-wascap-guest-0.1.0 (c (n "wascap-guest") (v "0.1.0") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "wascap-codec") (r "^0.1.0") (d #t) (k 0)))) (h "1xpgig4fycq2apf20rxwsig5kqpb90wlb7733w0swn4bzafsdnwq") (y #t)))

(define-public crate-wascap-guest-0.1.1 (c (n "wascap-guest") (v "0.1.1") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "wascap-codec") (r "^0.1.1") (d #t) (k 0)))) (h "0i4alfc83r3mnl1aql4wa6gbj9zsa7j70b8ji3g6lpmmiadzz5di") (y #t)))

(define-public crate-wascap-guest-0.2.0 (c (n "wascap-guest") (v "0.2.0") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.98") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "wascap-codec") (r "^0.1.1") (d #t) (k 0)))) (h "0nr1q7gnrc8qylncjfm054d37nxlzqxf8d4yqssbww01ma5k1zf6") (y #t)))

(define-public crate-wascap-guest-0.2.1 (c (n "wascap-guest") (v "0.2.1") (d (list (d (n "prost") (r "^0.5.0") (d #t) (k 0)) (d (n "prost-types") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "wapc-guest") (r "^0.1.1") (d #t) (k 0)) (d (n "wascap-codec") (r "^0.1.1") (d #t) (k 0)))) (h "1r8hmyh6xirf3dha0g4rs6z01y1fp7p2nlj7n2b6dmjlzx2w8xxl") (y #t)))

