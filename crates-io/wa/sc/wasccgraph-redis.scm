(define-module (crates-io wa sc wasccgraph-redis) #:use-module (crates-io))

(define-public crate-wasccgraph-redis-0.0.1 (c (n "wasccgraph-redis") (v "0.0.1") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "redis") (r "^0.15.1") (d #t) (k 0)) (d (n "redisgraph") (r "^0.4.0") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.6.0") (d #t) (k 0)) (d (n "wasccgraph-common") (r "^0.0.1") (d #t) (k 0)))) (h "1dl3xl6palkmdgahpzp4ar7c7jf80hcz24gzmy6c5m1gn8b5h54v") (f (quote (("static_plugin"))))))

(define-public crate-wasccgraph-redis-0.1.0 (c (n "wasccgraph-redis") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "redis") (r "^0.16.0") (d #t) (k 0)) (d (n "redisgraph") (r "^0.5.0") (d #t) (k 0)) (d (n "wascc-codec") (r "^0.7.0") (d #t) (k 0)) (d (n "wasccgraph-common") (r "^0.0.2") (d #t) (k 0)))) (h "0va129kppm7fnwvihlqb8fx54aqvaqwm6id1i1i113akfkw1bplh") (f (quote (("static_plugin"))))))

