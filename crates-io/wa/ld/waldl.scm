(define-module (crates-io wa ld waldl) #:use-module (crates-io))

(define-public crate-waldl-0.1.0 (c (n "waldl") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r48nm10v6vyifjhsdyil65s8znpv6929pj7lvc6v76wiyzbpibd")))

(define-public crate-waldl-0.1.2 (c (n "waldl") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1cjldzqz6mkfc4gi32vjwmvnfacr7dz2s7pna8ak1ppzxmd6n0dw")))

(define-public crate-waldl-0.2.0 (c (n "waldl") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03cj11mdbbs5ck7vc98vqpsdax2b181p06qgy6ads350iwy13prl")))

(define-public crate-waldl-0.2.1 (c (n "waldl") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1k672xx7rwyl3xx27q55h2892j0wmpz4mdshvnc3c12kxs8gdcd0")))

