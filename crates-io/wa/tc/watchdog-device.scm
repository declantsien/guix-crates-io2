(define-module (crates-io wa tc watchdog-device) #:use-module (crates-io))

(define-public crate-watchdog-device-0.1.0 (c (n "watchdog-device") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.24.1") (d #t) (k 0)))) (h "1iilwd60kn0k6rp2jm34wsr6ykbx53pikdr3cvsmrl5agslyxcb4")))

(define-public crate-watchdog-device-0.2.0 (c (n "watchdog-device") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.151") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "12snhah08cn61l3d9iipxvh1k3gvy54kc9q672zwh1wwnng5rbwx")))

