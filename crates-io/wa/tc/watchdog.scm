(define-module (crates-io wa tc watchdog) #:use-module (crates-io))

(define-public crate-watchdog-0.1.0 (c (n "watchdog") (v "0.1.0") (d (list (d (n "assert_fs") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.16") (d #t) (k 0)))) (h "0lc9z8a94yr8160q4hmj6k1phmjdk1cpnjgd2638iza7hn60gz12")))

(define-public crate-watchdog-0.2.0 (c (n "watchdog") (v "0.2.0") (d (list (d (n "assert_fs") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.16") (d #t) (k 0)))) (h "1xrkfr4b00m8dgdijrrq3skmj135sny5dp2v34xsrvayqvlp2rr7")))

(define-public crate-watchdog-0.2.1 (c (n "watchdog") (v "0.2.1") (d (list (d (n "assert_fs") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.16") (d #t) (k 0)))) (h "1f0bymlsys4x147g60fsmqqcjxl63306p3iv80qx4skl1sk23ggv")))

(define-public crate-watchdog-0.2.2 (c (n "watchdog") (v "0.2.2") (d (list (d (n "assert_fs") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.16") (d #t) (k 0)))) (h "0vrba51j8yk3zxpk5by6il7liy47khvjv0fs395rvcjvd44l7h76")))

(define-public crate-watchdog-0.2.3 (c (n "watchdog") (v "0.2.3") (d (list (d (n "assert_fs") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.16") (d #t) (k 0)))) (h "1agq0hza7bvbwxc53hdk4n2vkfcvpc3b6xgg9j399bfb73va5ma9")))

(define-public crate-watchdog-0.2.5 (c (n "watchdog") (v "0.2.5") (d (list (d (n "assert_fs") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.16") (d #t) (k 0)))) (h "07gxlv67qq326y6qilnq7bzgydxvb409vmxjq6d0m2gzd812bcmf")))

(define-public crate-watchdog-0.2.6 (c (n "watchdog") (v "0.2.6") (d (list (d (n "assert_fs") (r "^0.9.0") (d #t) (k 0)) (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "globset") (r "^0.4.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.6") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.16") (d #t) (k 0)))) (h "1ka2bkc8lqzn35acx68l9qj0bvixn726g0c93zwfdr3jw9l9hdda")))

