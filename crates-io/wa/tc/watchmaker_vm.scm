(define-module (crates-io wa tc watchmaker_vm) #:use-module (crates-io))

(define-public crate-watchmaker_vm-1.0.0 (c (n "watchmaker_vm") (v "1.0.0") (d (list (d (n "half") (r "^1.8.2") (d #t) (k 0)))) (h "02k5zciaz0755r78ks0jb2rgpzvgb3kcn79136v5q69fvglpygzn")))

(define-public crate-watchmaker_vm-1.0.1 (c (n "watchmaker_vm") (v "1.0.1") (d (list (d (n "half") (r "^1.8.2") (d #t) (k 0)))) (h "1pmj7i38pqs5i9w0hy3lnyw61n8qa8p9klrx998x2kgxbg7brax6")))

