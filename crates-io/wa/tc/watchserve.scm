(define-module (crates-io wa tc watchserve) #:use-module (crates-io))

(define-public crate-watchserve-0.1.0 (c (n "watchserve") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("color"))) (d #t) (k 0)) (d (n "futures-batch") (r "^0.6") (d #t) (k 0)) (d (n "http") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "notify") (r "^5.0.0-pre.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-stream") (r "^0.1") (d #t) (k 0)) (d (n "which") (r "^4") (d #t) (k 0)))) (h "1iln8dv4h504gmjhas4zbpa1s3nb43ng643k461b3s4ckc08z57r") (y #t)))

