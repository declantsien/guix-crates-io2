(define-module (crates-io wa tc watchdo) #:use-module (crates-io))

(define-public crate-watchdo-0.1.0 (c (n "watchdo") (v "0.1.0") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "ignore") (r "^0.4.10") (d #t) (k 0)) (d (n "mockall") (r "= 0.5.1") (d #t) (k 2)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.11") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "0s0wf0g0x5a057h5102j7mbqknyhj9k1wadi3srz5ykz9ybkj50f") (f (quote (("strict"))))))

(define-public crate-watchdo-0.1.1 (c (n "watchdo") (v "0.1.1") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "ignore") (r "^0.4.10") (d #t) (k 0)) (d (n "mockall") (r "= 0.5.1") (d #t) (k 2)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.1.11") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "19hprhai5pwq6z5pvkd827g33rff71chxxbclrvxavf7mxs2897g") (f (quote (("strict"))))))

(define-public crate-watchdo-0.1.2 (c (n "watchdo") (v "0.1.2") (d (list (d (n "colored") (r "^1.8") (d #t) (k 0)) (d (n "ignore") (r "^0.4.10") (d #t) (k 0)) (d (n "mockall") (r "= 0.5.1") (d #t) (k 2)) (d (n "notify") (r "^4.0.15") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "subprocess") (r "^0.2.3") (d #t) (k 0)) (d (n "term_size") (r "^0.3.1") (d #t) (k 0)))) (h "0r1g3c761iy57gq8hhgd3ysc2h32w6pan0y42j6sl0yvigqa8wv5") (f (quote (("strict"))))))

