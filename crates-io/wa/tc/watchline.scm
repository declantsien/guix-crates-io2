(define-module (crates-io wa tc watchline) #:use-module (crates-io))

(define-public crate-watchline-0.1.0 (c (n "watchline") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.19") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "simple-eyre") (r "^0.3.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "17xga76h1wpf6bfc0njs5rxcx3swriydml9h3r1am581j3585468")))

