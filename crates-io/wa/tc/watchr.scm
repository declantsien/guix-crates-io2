(define-module (crates-io wa tc watchr) #:use-module (crates-io))

(define-public crate-watchr-0.1.0 (c (n "watchr") (v "0.1.0") (h "050g5vjrvahr417xrzk8pcg69rxvsj3nk8sp6f46yh4l935dbj87")))

(define-public crate-watchr-0.2.0 (c (n "watchr") (v "0.2.0") (d (list (d (n "clap") (r "^3.0.0-rc.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "073gwsgpvc9vi27lgma815yfsffq1zfmx6fgv0srp0cln4wnal4p")))

(define-public crate-watchr-0.3.0 (c (n "watchr") (v "0.3.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)))) (h "0krrkm6a8k2z64kbspcfi3py489sy0mpw4i1rncfvb2jfyai14d2")))

