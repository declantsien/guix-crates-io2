(define-module (crates-io wa tc watchmen) #:use-module (crates-io))

(define-public crate-watchmen-0.0.1 (c (n "watchmen") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "configparser") (r "^3.0.2") (d #t) (k 0)) (d (n "dirs") (r "^5") (d #t) (k 0)) (d (n "regex") (r "^1.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0") (d #t) (k 0)))) (h "1snagrpzxm2my6kw22slybfngwy93r91vbswci3lgiw6anjvykc4")))

