(define-module (crates-io wa tc watchface) #:use-module (crates-io))

(define-public crate-watchface-0.1.0 (c (n "watchface") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.2.0") (d #t) (k 2)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)))) (h "17rcs00p5vv24ncs95sd2rpjraxk5c03bywxhpayd7svzn05hnm2")))

(define-public crate-watchface-0.2.0 (c (n "watchface") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.2.0") (d #t) (k 2)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)))) (h "1awygz4b8wzw0a4d61371dcq34f7i4b5gc286c9nv5g9sc4iwi5d")))

(define-public crate-watchface-0.3.0 (c (n "watchface") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6.2") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.2.0") (d #t) (k 2)) (d (n "embedded-layout") (r "^0.1.0") (d #t) (k 0)) (d (n "heapless") (r "^0.5.5") (d #t) (k 0)))) (h "0mahigsm5y5bdhzih7019a2zdrl47d3zgqb0d06ysiakgj2v2p9p")))

(define-public crate-watchface-0.3.1 (c (n "watchface") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.2") (d #t) (k 2)) (d (n "embedded-layout") (r "^0.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "1c3lgksy4iaxv5ifn9dni3lwbbwg25gbkbhmlhzamq73n97l1x9j") (f (quote (("std" "chrono") ("default" "std"))))))

(define-public crate-watchface-0.3.2 (c (n "watchface") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.6") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.2") (d #t) (k 2)) (d (n "embedded-layout") (r "^0.1") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "0kqwqqcb7m02qvmi5dbv7dlagzxbis5pghfg97ma5lahwsz00nrk") (f (quote (("std" "chrono") ("default" "std"))))))

(define-public crate-watchface-0.4.0 (c (n "watchface") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "embedded-graphics") (r "^0.7") (d #t) (k 0)) (d (n "embedded-graphics-simulator") (r "^0.3") (d #t) (k 2)) (d (n "embedded-layout") (r "^0.2") (d #t) (k 0)) (d (n "heapless") (r "^0.5") (d #t) (k 0)))) (h "1zsh364hwdwmzb3br2kz30cpi9d3l53jbng39l6rc60138ibc43v") (f (quote (("std" "chrono") ("default" "std"))))))

