(define-module (crates-io wa tc watchboi) #:use-module (crates-io))

(define-public crate-watchboi-0.1.0 (c (n "watchboi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termcolor") (r "^1.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-core"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "tungstenite") (r "^0.11") (d #t) (k 0)))) (h "0ppqqadxs81bq4llbkb3zl2mrf1ka3c22zd6rlzzvpw8f1ddjl6w") (y #t)))

(define-public crate-watchboi-0.1.1 (c (n "watchboi") (v "0.1.1") (h "1mp39s69lhz3ynpqc2xb6manwak7pbb1asa4n5a7djp2wxy30jm9")))

