(define-module (crates-io wa tc watchhn) #:use-module (crates-io))

(define-public crate-watchhn-0.1.0 (c (n "watchhn") (v "0.1.0") (d (list (d (n "async-recursion") (r "^0.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0j7fl08gaa32bj1p74hz8jbvswbasg7di51njblm9qkg5b1y4lv5")))

