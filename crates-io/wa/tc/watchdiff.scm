(define-module (crates-io wa tc watchdiff) #:use-module (crates-io))

(define-public crate-watchdiff-0.1.0 (c (n "watchdiff") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0xrcxbbwi3k5qljyqn47da9l4hj3mw975g1givz34h48bbvcg3ll")))

(define-public crate-watchdiff-0.1.1 (c (n "watchdiff") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1c2clc8f4x63dnk64hc6fjrri2wyjwc9xvjjvxj6bjhjhh6c86lh")))

(define-public crate-watchdiff-0.2.0 (c (n "watchdiff") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "12lmjamvzj2j3y5bm8xmm5zkymy0309j66hja3iphbyvbr3wy5cl")))

(define-public crate-watchdiff-0.2.1 (c (n "watchdiff") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "0pzv7lz568blwpz46jva1yn8vlnxzm39103xr0myp0cxra4qmibs")))

(define-public crate-watchdiff-0.2.2 (c (n "watchdiff") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1v611sgw02ifs134rx72wcn4b3by2y2bx1y67b46vm3cma0i76yr")))

