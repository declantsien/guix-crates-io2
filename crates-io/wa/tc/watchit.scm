(define-module (crates-io wa tc watchit) #:use-module (crates-io))

(define-public crate-watchit-0.1.0 (c (n "watchit") (v "0.1.0") (d (list (d (n "notify") (r "^6.1.1") (d #t) (k 0)) (d (n "notify-debouncer-full") (r "^0.3.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0w5pv33xk6yq818nwbbg2ii4nilr88ywvbcyrqkrc10039yawjh6")))

