(define-module (crates-io wa tc watch) #:use-module (crates-io))

(define-public crate-watch-0.1.0 (c (n "watch") (v "0.1.0") (h "1kdj7v6h9bs3z659vav84a7g51y8sw7chscpjr759iw1k58bqgk5") (y #t)))

(define-public crate-watch-0.2.0 (c (n "watch") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1b1rsk59rar9xlpxmgmrdzf5z7c67yp4qh5kgfnvkbp4rkp4f958")))

(define-public crate-watch-0.2.1 (c (n "watch") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)))) (h "14diqy5yvcnz0rgq81lwh8mk9qppwfycdic3n8xp5ng1z824s52l")))

(define-public crate-watch-0.2.2 (c (n "watch") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0ffk8a93avk9l7h9v2l3gi9yc2kacag1y8cbn39x0jshbzcs3cj0")))

(define-public crate-watch-0.2.3 (c (n "watch") (v "0.2.3") (d (list (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)))) (h "0rawzzjb6p1xbm5rwqjz32n9h6cp25bnk8nhvas3cq3c5zx91lsy")))

