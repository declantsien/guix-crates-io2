(define-module (crates-io wa tc watchable) #:use-module (crates-io))

(define-public crate-watchable-0.0.0-reserve.0 (c (n "watchable") (v "0.0.0-reserve.0") (h "04zp71p892nlwys3qzyh85a6gaxplv47gcsbxjzigq3mmmgzlayy")))

(define-public crate-watchable-0.1.0 (c (n "watchable") (v "0.1.0") (d (list (d (n "event-listener") (r "^2.5.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (k 2)))) (h "1wkqzjawr0jndckv8x17837nsnsaxdc6chcyy9y511v2rnrdkwiz") (r "1.58")))

(define-public crate-watchable-1.0.0 (c (n "watchable") (v "1.0.0") (d (list (d (n "event-listener") (r "^2.5.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (k 2)))) (h "1lfjmb8pghfaanywx8lkx71cfn9msx9bl70k9sdd9icv92ns2m0m") (r "1.58")))

(define-public crate-watchable-1.1.0 (c (n "watchable") (v "1.1.0") (d (list (d (n "event-listener") (r "^2.5.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (k 2)))) (h "079wciy6r2cbbagqlx96h0ydl1rsa7qv2gl678b1bqkj9kpb6ydp") (r "1.58")))

(define-public crate-watchable-1.1.1 (c (n "watchable") (v "1.1.1") (d (list (d (n "event-listener") (r "^2.5.2") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (k 2)))) (h "0qlam9mh1avssw6km58xvrbc64a2bcq8l6pcxfzaph5kmyxd147z") (r "1.58")))

(define-public crate-watchable-1.1.2 (c (n "watchable") (v "1.1.2") (d (list (d (n "event-listener") (r "^4.0.1") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.21") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tokio") (r "^1.16.1") (f (quote ("macros" "rt" "rt-multi-thread" "time"))) (k 2)))) (h "0ndjnfjj8z7lwvarmp96912v8b8gnvicva90a6bba5hrc4pjmd25") (r "1.64")))

