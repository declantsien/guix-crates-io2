(define-module (crates-io wa tc watc) #:use-module (crates-io))

(define-public crate-watc-0.0.0 (c (n "watc") (v "0.0.0") (d (list (d (n "wasm-compiler") (r ">=0.0.0, <1.0.0") (d #t) (k 0)) (d (n "wat") (r ">=1.0.28, <2.0.0") (d #t) (k 0)))) (h "0whd4nicy071yihyn4llpqqm7mv7v1d1j9s5lmcybwnczwjcivc7")))

(define-public crate-watc-0.0.1 (c (n "watc") (v "0.0.1") (d (list (d (n "wasm-compiler") (r ">=0.0.0, <1.0.0") (d #t) (k 0)) (d (n "wat") (r ">=1.0.28, <2.0.0") (d #t) (k 0)))) (h "0raczj4bvkgz5lcvgpa4vk6sdk40r6gp121l867j99vj9njml3rj")))

