(define-module (crates-io wa tc watchmaker) #:use-module (crates-io))

(define-public crate-watchmaker-0.1.0 (c (n "watchmaker") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "1bwj4x8viginl0gvpii75043kvaj94hszpzfa6iy59s5i31shzh3")))

(define-public crate-watchmaker-1.0.0 (c (n "watchmaker") (v "1.0.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)))) (h "013cvhdnz8bgvvishrpz56f873gwa5pl6gj8v7wlzkkwn15jc7cv")))

(define-public crate-watchmaker-2.0.0 (c (n "watchmaker") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.2") (d #t) (k 0)))) (h "1c24ayvb8zjylsfwzgsv9ywb242nifwl38z4k9r92hjys23fcvx5")))

