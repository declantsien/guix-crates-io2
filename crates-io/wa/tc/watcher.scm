(define-module (crates-io wa tc watcher) #:use-module (crates-io))

(define-public crate-watcher-0.0.1 (c (n "watcher") (v "0.0.1") (d (list (d (n "gitignore") (r "^1.0") (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)) (d (n "walkdir") (r "^2.3") (d #t) (k 0)))) (h "0kdqd00pkcbyxbpngaqfw404gx57f58a833y90av5sp20ylqc0ll")))

