(define-module (crates-io wa tc watch-rs) #:use-module (crates-io))

(define-public crate-watch-rs-0.1.0 (c (n "watch-rs") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1gmrz8bxcvzg7wnsfh1r9jmy7kz8342dz824lkcyrr9jbs04g6ac") (y #t)))

(define-public crate-watch-rs-0.1.1 (c (n "watch-rs") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1s5wz2naxjr0ps14f8vmmv5j5vqih76kxbh1daflma2vlj3wdc2g") (y #t)))

(define-public crate-watch-rs-0.1.2 (c (n "watch-rs") (v "0.1.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1yv2rarvn2fp00ykb0533wzmpjsq4ry9zpxy42pndix9jsprq31d") (y #t)))

(define-public crate-watch-rs-0.1.3 (c (n "watch-rs") (v "0.1.3") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0khil5yymwcwcwjmpybgpyhbdnllbls6jbg2iyy8392rvly546if") (y #t)))

(define-public crate-watch-rs-0.1.4 (c (n "watch-rs") (v "0.1.4") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0j97fhj8r4nsyvdbdy08i6q2h35ssxqg0skpixhm90ahb93hck4a")))

(define-public crate-watch-rs-0.1.5 (c (n "watch-rs") (v "0.1.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0x97azwal16ljc9bckghvdk0zw20ag1yya85liz3n79iyryhc8yb")))

