(define-module (crates-io wa tc watchman) #:use-module (crates-io))

(define-public crate-watchman-1.3.0 (c (n "watchman") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "directories") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.98") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.40") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)) (d (n "sysinfo") (r "^0.9.1") (d #t) (k 0)))) (h "0vhrxrb997sy0cqb0fbvx8km1nrn7snmzj5jpxib30h3pbjn2xn8")))

