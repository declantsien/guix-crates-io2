(define-module (crates-io wa rm warmer) #:use-module (crates-io))

(define-public crate-warmer-0.1.0 (c (n "warmer") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "11qvc4ks67n1rs0m9qfx0sxlp73pdkyjqv8dnbi00177npypndzv") (y #t) (r "1.71")))

(define-public crate-warmer-0.1.1 (c (n "warmer") (v "0.1.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "1a7xfnvcky4p1hldz4z961z9mpajfsbz5gynpzgqfyfhyz9lbl1w") (r "1.72")))

(define-public crate-warmer-0.1.2 (c (n "warmer") (v "0.1.2") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.2.3") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "url") (r "^2") (d #t) (k 0)))) (h "0d6vngqal05yh5dq5shq6z5v1wcvc08q3hpza6f84cnhp94mjxr4") (r "1.72")))

