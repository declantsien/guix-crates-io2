(define-module (crates-io wa rm warmth) #:use-module (crates-io))

(define-public crate-warmth-0.0.0 (c (n "warmth") (v "0.0.0") (h "104bgg74k0py6miw2faa534qlm4hgz2fdcd5k7w1ix4wka994rpb") (y #t)))

(define-public crate-warmth-0.1.0 (c (n "warmth") (v "0.1.0") (d (list (d (n "booties") (r "^0.1.0") (d #t) (k 0)) (d (n "earmuffs") (r "^0.1.0") (d #t) (k 0)) (d (n "fluff") (r "^0.1.0") (d #t) (k 0)) (d (n "parka") (r "^0.1.0") (d #t) (k 0)) (d (n "snowgoggles") (r "^0.1.0") (d #t) (k 0)))) (h "1vd3c06gbnylnb57yxpx7bwjaa0ia39i3g0dnhnnq3xhfhzasd0m") (y #t)))

(define-public crate-warmth-0.2.0 (c (n "warmth") (v "0.2.0") (d (list (d (n "booties") (r "^0.1.0") (d #t) (k 0)) (d (n "earmuffs") (r "^0.1.0") (d #t) (k 0)) (d (n "fluff") (r "^0.1.0") (d #t) (k 0)) (d (n "parka") (r "^0.1.0") (d #t) (k 0)) (d (n "snowgoggles") (r "^0.1.0") (d #t) (k 0)))) (h "1rlbk94y54ry9n1ax6rn8f513k6ili00kjg9ha3q38214zan4xl9") (y #t)))

