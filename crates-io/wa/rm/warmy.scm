(define-module (crates-io wa rm warmy) #:use-module (crates-io))

(define-public crate-warmy-0.1.0 (c (n "warmy") (v "0.1.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1b688gvmjdvayqzv7975lg6bbhz2p3vm19wa65vcbq97qrgf182p")))

(define-public crate-warmy-0.2.0 (c (n "warmy") (v "0.2.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0h1wpkrkqp077z5w0y5yp5cnh9gsc26vvla0p02cw6cznb33cqp6")))

(define-public crate-warmy-0.3.0 (c (n "warmy") (v "0.3.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0vpm067qgidj7c6qfxh6anzym9ph5p5bvr2l5lh0x8w0j10rpl2k")))

(define-public crate-warmy-0.4.0 (c (n "warmy") (v "0.4.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "155bcqa284mzlmq0wqlkrlxzww6j56dyhhq75dm703s3pnrf94kk")))

(define-public crate-warmy-0.5.0 (c (n "warmy") (v "0.5.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "122gl9lqfnwlqpwn4pb9w9gj6amd4h06vpii6ryq8axrsdby3040")))

(define-public crate-warmy-0.5.1 (c (n "warmy") (v "0.5.1") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0haaw10g1l9dzs3ak5y61s6ik3qzw5ibha39915kx47pnb7fsdz4")))

(define-public crate-warmy-0.5.2 (c (n "warmy") (v "0.5.2") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0afa1fcijg0yp6rv4nb3178pqr0vbrviik9hrdp9c8v2llg0jypk")))

(define-public crate-warmy-0.6.0 (c (n "warmy") (v "0.6.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0bbk2x2vfcnxmaxp4rp4fbdza8bg280gnf65xpgg5h61225hnv5g")))

(define-public crate-warmy-0.6.1 (c (n "warmy") (v "0.6.1") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "16r8v52h2j9y74f72ff9dpmsk3az5s29k6pbvzcsfxlksm60p63n")))

(define-public crate-warmy-0.7.0 (c (n "warmy") (v "0.7.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1zai399jvq6qqprks2aiyg29bzf5qgb72kiy2b5g1lls1m356isq")))

(define-public crate-warmy-0.7.1 (c (n "warmy") (v "0.7.1") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "04i155x8hvdc7dakx2yz5jzj4whcirrdd84pabnfm7a6kl40izfa")))

(define-public crate-warmy-0.7.2 (c (n "warmy") (v "0.7.2") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0a8rc903akf92lwsm1yyy41c8xv6f52scxvn1xb85lzbflc8fz3z")))

(define-public crate-warmy-0.7.3 (c (n "warmy") (v "0.7.3") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "08vriz3mdf8m85ywdzkbvv0szf4q0kq7x126h5dsc2rfw4s3xij7")))

(define-public crate-warmy-0.8.0 (c (n "warmy") (v "0.8.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0624b8c7hjaqgawrybm3cfyb0minwg1b0m33l70179aghbr75kn9")))

(define-public crate-warmy-0.9.0 (c (n "warmy") (v "0.9.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1lxgr20qw3ca7f0a9h3ifa5l2q0iqq4w78d217k9jfhpxry1iz05")))

(define-public crate-warmy-0.10.0 (c (n "warmy") (v "0.10.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0biamxnwj0p1a6rgwlx5ld4skkqjv3nimv7rppbz9ilwmcrvn2ky")))

(define-public crate-warmy-0.11.0 (c (n "warmy") (v "0.11.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1df51iayzz73anb3yq52895q8fkq8q4h1hrs89qfsgwq44jhg2h6")))

(define-public crate-warmy-0.11.1 (c (n "warmy") (v "0.11.1") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "1z6qdm4ym0akg3i6cvq3m3qnb1bj96i9zy5jhqd6aff3qryd5ya2") (f (quote (("json" "serde" "serde_json") ("default" "json"))))))

(define-public crate-warmy-0.11.2 (c (n "warmy") (v "0.11.2") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0mrkfflwqky03wi0155c5kbvqvg2shqfla4w0xdyd1p17a0lfbn1") (f (quote (("json" "serde" "serde_json") ("default" "json") ("arc"))))))

(define-public crate-warmy-0.11.3 (c (n "warmy") (v "0.11.3") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 2)))) (h "0ivw0fkmwb6b4cd5n3hwnaiybpng1vzr2kkddy6kkypwblpfk2j2") (f (quote (("json" "serde" "serde_json") ("default" "json") ("arc"))))))

(define-public crate-warmy-0.12.0 (c (n "warmy") (v "0.12.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "044bj16mkgmxzg4z11l8yp54wr9hr1p7wj2zajlnhv7mnby18hi3") (f (quote (("toml-impl" "serde" "toml") ("json" "serde" "serde_json") ("default" "json") ("arc"))))))

(define-public crate-warmy-0.13.0 (c (n "warmy") (v "0.13.0") (d (list (d (n "any-cache") (r "^0.2") (d #t) (k 0)) (d (n "notify") (r "^4.0.3") (d #t) (k 0)) (d (n "ron") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "toml") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "06afgpxa9q2piyzafhfz11533fb5vbk0xr07v4nkhvyk0hh2gvw0") (f (quote (("toml-impl" "serde" "toml") ("ron-impl" "serde" "ron") ("json" "serde" "serde_json") ("default" "json") ("arc"))))))

