(define-module (crates-io wa rm warm-fs) #:use-module (crates-io))

(define-public crate-warm-fs-0.1.0 (c (n "warm-fs") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "indicatif") (r "^0.16") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "threadpool") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1n77lrjr6qi9q8g419lc9pv9rmh0pzyh1dq4rkyp9b5fk4kzz50g")))

(define-public crate-warm-fs-0.2.0 (c (n "warm-fs") (v "0.2.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "indicatif") (r "^0.16") (d #t) (k 2)) (d (n "rusty-hook") (r "^0.11") (d #t) (k 2)) (d (n "threadpool") (r "^1") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0dwsz8m23msrdf03cz5fsx4z92cvi0vk0hjdjkqcmvvfjirns6wl")))

