(define-module (crates-io wa if waifu2x) #:use-module (crates-io))

(define-public crate-waifu2x-0.0.0 (c (n "waifu2x") (v "0.0.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.24") (d #t) (k 0)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.86") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)) (d (n "tract") (r "^0.18.1") (d #t) (k 0)) (d (n "tract-onnx") (r "^0.18.1") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "065w6kjs7rkq19lmyk6c8iz4czrq5gblfz7iv5bzm7kmy68ixmfs") (f (quote (("default"))))))

(define-public crate-waifu2x-0.1.0 (c (n "waifu2x") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (d #t) (k 0)) (d (n "safetensors") (r "^0.2.8") (d #t) (k 0)) (d (n "tch") (r "^0.10.3") (d #t) (k 0)))) (h "1wrd9lcw9f7q8s579d146br3vb78vnmd84jzigr9rbxsghxnm1y3") (f (quote (("default"))))))

