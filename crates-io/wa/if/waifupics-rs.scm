(define-module (crates-io wa if waifupics-rs) #:use-module (crates-io))

(define-public crate-waifupics-rs-0.1.0 (c (n "waifupics-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "1sijg521lv4qj6pid4accrw7dd12px7wcfn0n51xj6qiv2sckixp")))

(define-public crate-waifupics-rs-0.1.1 (c (n "waifupics-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)))) (h "0n01lpmpqj8h6irrwhzc8drqc2r8v0jfavys00yaipzgs760v4rm") (f (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default-tls" "reqwest/default-tls") ("default" "default-tls"))))))

