(define-module (crates-io wa if waifu-vault-client) #:use-module (crates-io))

(define-public crate-waifu-vault-client-0.1.0 (c (n "waifu-vault-client") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "multipart" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1g9dxbpi97fqrzcb89wis14pmj00cpg9yrl7kcckyyal730x2qf2")))

(define-public crate-waifu-vault-client-0.2.0 (c (n "waifu-vault-client") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "multipart" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0qa6dnjibd1mvh893mjhrbw3kzfhzinawhm7filna60r9m470kxw")))

(define-public crate-waifu-vault-client-0.3.0 (c (n "waifu-vault-client") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "waifuvault") (r "^0.1.3") (d #t) (k 0)))) (h "0xlkmh4rflf0vpvc73k3a0l6ifi8nxfawgbqrw8i6j6ja0p296kx")))

