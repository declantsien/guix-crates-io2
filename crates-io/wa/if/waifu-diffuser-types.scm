(define-module (crates-io wa if waifu-diffuser-types) #:use-module (crates-io))

(define-public crate-waifu-diffuser-types-0.0.0 (c (n "waifu-diffuser-types") (v "0.0.0") (d (list (d (n "diagnostic-quick") (r "^0.3.6") (d #t) (k 0)) (d (n "diffusers") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json5") (r "^0.1.0") (d #t) (k 0)) (d (n "sevenz-rust") (r "^0.2.0") (f (quote ("compress"))) (d #t) (k 0)))) (h "0b8fyvhb0gcs8zhgjqj5r0lap4ybj8mrz6ihqmyljfmhiczf22dr") (f (quote (("default"))))))

