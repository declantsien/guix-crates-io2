(define-module (crates-io wa if waifai) #:use-module (crates-io))

(define-public crate-waifai-0.0.0 (c (n "waifai") (v "0.0.0") (h "0yzs8jqqr1nwg97vf9d1k5299ch776d8c5srrdkclc5086z9pn01")))

(define-public crate-waifai-0.1.0 (c (n "waifai") (v "0.1.0") (h "0d0a6w343f80v9br4f1hzbsmb1w08khr1i0nifpmca7kdvildp5r") (r "1.56.1")))

(define-public crate-waifai-0.1.1 (c (n "waifai") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "085jq4b4vgi0cxaj4ih4nk77wkww23r5ldf85csi2lk742jg32gf") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "serde/derive")))) (r "1.60.0")))

(define-public crate-waifai-0.1.2 (c (n "waifai") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "19n92p12kzrmlg0z23vagj6fkm489bjzzx7frvkb0pin1dyyx3mc") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "serde/derive")))) (r "1.60.0")))

(define-public crate-waifai-0.1.3 (c (n "waifai") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)))) (h "07h20vvq5xx8244509l94gvizaw3va2j3fj6vgqm7m4qa3zc6m9r") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "serde/derive")))) (r "1.60.0")))

