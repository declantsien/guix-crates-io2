(define-module (crates-io ob i- obi-derive-internal) #:use-module (crates-io))

(define-public crate-obi-derive-internal-0.0.1 (c (n "obi-derive-internal") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1yx754drbr6ivdlmiyix0k6icw5ggniva0clgb2afnm77h0qnfn6")))

(define-public crate-obi-derive-internal-0.0.2 (c (n "obi-derive-internal") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0.62") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "17fhn0g3f336hl479ms0ny4ygwsa213c81n1li3qlmfpzzyn81mh")))

