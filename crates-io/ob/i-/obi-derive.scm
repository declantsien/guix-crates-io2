(define-module (crates-io ob i- obi-derive) #:use-module (crates-io))

(define-public crate-obi-derive-0.0.1 (c (n "obi-derive") (v "0.0.1") (d (list (d (n "obi-derive-internal") (r "^0.0.1") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "19c4ky485j5gkpg3n0brxxnk1ax3gz7v32xn0qqsqdqyqfqik22g")))

(define-public crate-obi-derive-0.0.2 (c (n "obi-derive") (v "0.0.2") (d (list (d (n "obi-derive-internal") (r "^0.0.2") (d #t) (k 0)) (d (n "quote") (r "^0.3.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.62") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "0ppjkp2sgb3i52imf9xx507zc2ns2n7aznkxcl71zgx60glr7sjm")))

