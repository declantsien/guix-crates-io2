(define-module (crates-io ob ji objid) #:use-module (crates-io))

(define-public crate-objid-0.1.0 (c (n "objid") (v "0.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0gq52plk4vynli7gqcgagmqm1glbgncrg11svmbwh0yyxgi6ylqd")))

(define-public crate-objid-0.1.1 (c (n "objid") (v "0.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0sf5s7q7q8fkigldzaisqpdfmnlm8caisfs7ivssa7gpy496pyls")))

(define-public crate-objid-0.1.2 (c (n "objid") (v "0.1.2") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1w04mdl2w8x8b0bbh1jfzlsqnsm5x0rrskcsc4a8k050kaa6pggg")))

(define-public crate-objid-0.1.3 (c (n "objid") (v "0.1.3") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "17wlw96rwz3hsx15zh3nc4d5d0s73ga3gh7r7rnwgly5l53n3hsz")))

(define-public crate-objid-0.1.4 (c (n "objid") (v "0.1.4") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1b013nhb6a7jjn3nld1wzglhdkrcwzjq1zc2237pd7rznqkw577q")))

(define-public crate-objid-0.1.5 (c (n "objid") (v "0.1.5") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0faygp7b260x98lc7mxwpzcrvyid4hrww3f8xs230b1zbc2ynklk")))

(define-public crate-objid-0.1.9 (c (n "objid") (v "0.1.9") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0bzdbhpnr3z9r91ifaygw9k74r8v13d6ycrg13ig8v0yw1zd9lza")))

(define-public crate-objid-0.1.10 (c (n "objid") (v "0.1.10") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1q7jdkm2dq1pcckwi7v8izkls7gfxcsc80dxh3myn7100dv4dprc")))

(define-public crate-objid-0.1.11 (c (n "objid") (v "0.1.11") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "10wga8zp6w1mnfphqh9flj42jinchcyblpdfgb5yk2ixn7k0ih6v")))

