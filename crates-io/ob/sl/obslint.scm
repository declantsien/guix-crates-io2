(define-module (crates-io ob sl obslint) #:use-module (crates-io))

(define-public crate-obslint-0.1.0 (c (n "obslint") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^0.7") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "paw") (r "^1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (f (quote ("paw"))) (k 0)) (d (n "test-case") (r "^1") (d #t) (k 2)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0jbqidsjl1mhagpzyhk9j1z613f7ar34s64wzm044ajx9azw5kxc")))

