(define-module (crates-io ob jc objc2-finder-sync) #:use-module (crates-io))

(define-public crate-objc2-finder-sync-0.0.0 (c (n "objc2-finder-sync") (v "0.0.0") (h "15czyhixjlgfmxakl9gv8rw9akbphn1jvd9ah7fi4hwslz27ggg1")))

(define-public crate-objc2-finder-sync-0.2.2 (c (n "objc2-finder-sync") (v "0.2.2") (d (list (d (n "block2") (r "^0.5.1") (o #t) (k 0)) (d (n "objc2") (r "^0.5.2") (k 0)) (d (n "objc2-app-kit") (r "^0.2.2") (f (quote ("NSImage" "NSMenu"))) (o #t) (k 0)) (d (n "objc2-foundation") (r "^0.2.2") (f (quote ("NSArray" "NSData" "NSDate" "NSDictionary" "NSError" "NSExtensionContext" "NSExtensionRequestHandling" "NSFileManager" "NSSet" "NSString" "NSURL" "NSXPCConnection"))) (k 0)))) (h "0f1a9vvv7ygbqsqsrsaai7jqb9h7z62ij3bdg346xzhdj9rxamzv") (f (quote (("default" "std") ("all" "block2" "objc2-app-kit")))) (s 2) (e (quote (("std" "alloc" "block2?/std" "objc2/std" "objc2-app-kit?/std" "objc2-foundation/std") ("objc2-app-kit" "dep:objc2-app-kit") ("block2" "dep:block2" "objc2-app-kit?/block2" "objc2-foundation/block2") ("alloc" "block2?/alloc" "objc2/alloc" "objc2-app-kit?/alloc" "objc2-foundation/alloc")))) (r "1.60")))

