(define-module (crates-io ob jc objc2-ad-services) #:use-module (crates-io))

(define-public crate-objc2-ad-services-0.2.0 (c (n "objc2-ad-services") (v "0.2.0") (d (list (d (n "objc2") (r "^0.5.1") (f (quote ("apple"))) (k 0)) (d (n "objc2-foundation") (r "^0.2.0") (d #t) (k 0)))) (h "0l7jix1vlznqh313jjx4c24hwzsmdghd43mgn2wld6c26ih43wgi") (f (quote (("unstable-docsrs") ("std" "alloc" "objc2/std") ("default" "std") ("alloc" "objc2/alloc") ("all" "AAAttribution") ("AAAttribution" "objc2-foundation/NSError" "objc2-foundation/NSString")))) (r "1.60")))

(define-public crate-objc2-ad-services-0.2.2 (c (n "objc2-ad-services") (v "0.2.2") (d (list (d (n "objc2") (r "^0.5.2") (k 0)) (d (n "objc2-foundation") (r "^0.2.2") (k 0)))) (h "1nb0rpk1ckz8lhvfv8dnfsvm44ais42f4jyfk23asfqdh8yhdjkw") (f (quote (("std" "alloc" "objc2/std" "objc2-foundation/std") ("default" "std") ("alloc" "objc2/alloc" "objc2-foundation/alloc") ("all" "AAAttribution") ("AAAttribution" "objc2-foundation/NSError" "objc2-foundation/NSString")))) (r "1.60")))

