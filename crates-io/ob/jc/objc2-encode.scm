(define-module (crates-io ob jc objc2-encode) #:use-module (crates-io))

(define-public crate-objc2-encode-1.1.0 (c (n "objc2-encode") (v "1.1.0") (h "0c5775b9cx12vw98s3lcyf8g9648ralz5fi57np8b5w07x1qzq5w")))

(define-public crate-objc2-encode-2.0.0-alpha.0 (c (n "objc2-encode") (v "2.0.0-alpha.0") (d (list (d (n "objc") (r "^0.2.7") (d #t) (k 2)))) (h "18vxkqswqkr0hdr7045s7kj9xsvbygpihskb2cdspcdkq976z0di")))

(define-public crate-objc2-encode-2.0.0-alpha.1 (c (n "objc2-encode") (v "2.0.0-alpha.1") (d (list (d (n "objc") (r "^0.2.7") (d #t) (k 2)))) (h "0j104qzqdlgrx0rdx59mfpprmjliyi23iaq1ajixlfqn9rdd6zhs")))

(define-public crate-objc2-encode-2.0.0-beta.0 (c (n "objc2-encode") (v "2.0.0-beta.0") (h "042i1rjzhwsqzx5lajkc39wqpy5kbh7k3gb2szxvr529fdsgylxg")))

(define-public crate-objc2-encode-2.0.0-beta.1 (c (n "objc2-encode") (v "2.0.0-beta.1") (h "0xb4b3mr104nx5q5yhf2h4pyr4aphjxg71m4f763nylcd3b1kp2x")))

(define-public crate-objc2-encode-2.0.0-beta.2 (c (n "objc2-encode") (v "2.0.0-beta.2") (h "0pg341h403ij9qazabv8wig3672x6v4qfb13h6a0a1j95dql8wxi")))

(define-public crate-objc2-encode-2.0.0-pre.0 (c (n "objc2-encode") (v "2.0.0-pre.0") (h "1si1wdadhffpkvzhxylrs62i7c4zibyrxiryby6gdfdm3ycgaigk")))

(define-public crate-objc2-encode-2.0.0-pre.1 (c (n "objc2-encode") (v "2.0.0-pre.1") (h "113dxpkzbihhyg4g3gy6xnp35zjd1d62hhp531s5gnjmv9ss94q7") (f (quote (("unstable-c-unwind") ("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-objc2-encode-2.0.0-pre.2 (c (n "objc2-encode") (v "2.0.0-pre.2") (d (list (d (n "objc-sys") (r "=0.2.0-beta.2") (k 0)))) (h "04h5wns3hxmc9g652hr9xqzrijs4ij9sdnlgc0ha202v050srz5b") (f (quote (("unstable-c-unwind") ("std" "alloc" "objc-sys/std") ("gnustep-2-1" "gnustep-2-0" "objc-sys/gnustep-2-1") ("gnustep-2-0" "gnustep-1-9" "objc-sys/gnustep-2-0") ("gnustep-1-9" "gnustep-1-8" "objc-sys/gnustep-1-9") ("gnustep-1-8" "gnustep-1-7" "objc-sys/gnustep-1-8") ("gnustep-1-7" "objc-sys/gnustep-1-7") ("default" "std" "apple") ("apple" "objc-sys/apple") ("alloc" "objc-sys/alloc"))))))

(define-public crate-objc2-encode-2.0.0-pre.3 (c (n "objc2-encode") (v "2.0.0-pre.3") (d (list (d (n "objc-sys") (r "=0.2.0-beta.3") (k 0)))) (h "0im8bll58p9x9jqnswb745wg9g7i1l01sp74bm55whcw0kaf84i6") (f (quote (("unstable-c-unwind") ("std" "alloc" "objc-sys/std") ("gnustep-2-1" "gnustep-2-0" "objc-sys/gnustep-2-1") ("gnustep-2-0" "gnustep-1-9" "objc-sys/gnustep-2-0") ("gnustep-1-9" "gnustep-1-8" "objc-sys/gnustep-1-9") ("gnustep-1-8" "gnustep-1-7" "objc-sys/gnustep-1-8") ("gnustep-1-7" "objc-sys/gnustep-1-7") ("default" "std" "apple") ("apple" "objc-sys/apple") ("alloc" "objc-sys/alloc"))))))

(define-public crate-objc2-encode-2.0.0-pre.4 (c (n "objc2-encode") (v "2.0.0-pre.4") (h "14i1dgs96p92462zmvqknjkmkval4wmiwqwjwf3llic6nybp53wg") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-objc2-encode-2.0.0 (c (n "objc2-encode") (v "2.0.0") (h "07h95b9480zh86xp5128l8ls5sqifl7yck7vb5n7rc3rs6qpqlfy") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-objc2-encode-3.0.0 (c (n "objc2-encode") (v "3.0.0") (h "0rknhkcnyj4qv1pzqp5j8l80726phz8fcxpsbpz9nhmg6xdq8yfh") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-objc2-encode-4.0.0 (c (n "objc2-encode") (v "4.0.0") (h "0yb38n8w1a2wys958bdwdz332vcfmiwq93gkhij7dq6d0mjnmw1g") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-objc2-encode-4.0.1 (c (n "objc2-encode") (v "4.0.1") (h "02lmq1ll4697zpb61dw80kgr847mdb6h4a96n6nwihjc7sk8src8") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.60")))

(define-public crate-objc2-encode-4.0.2 (c (n "objc2-encode") (v "4.0.2") (h "1fpcmsl0ggwncwjfrs81ga710yc59bczdd1hfydns63svxacprb9") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t) (r "1.60")))

(define-public crate-objc2-encode-4.0.3 (c (n "objc2-encode") (v "4.0.3") (h "1y7hjg4k828zhn4fjnbidrz3vzw4llk9ldy92drj47ydjc9yg4bq") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.60")))

