(define-module (crates-io ob jc objc2-proc-macros) #:use-module (crates-io))

(define-public crate-objc2-proc-macros-0.0.0 (c (n "objc2-proc-macros") (v "0.0.0") (h "151blslk8j07lf00j7kx969cif7mjgfjjqscbpzpglldxf2fjs5x") (f (quote (("gnustep-2-1" "gnustep-2-0") ("gnustep-2-0" "gnustep-1-9") ("gnustep-1-9" "gnustep-1-8") ("gnustep-1-8" "gnustep-1-7") ("gnustep-1-7") ("default" "apple") ("apple"))))))

(define-public crate-objc2-proc-macros-0.1.0 (c (n "objc2-proc-macros") (v "0.1.0") (h "0gffxsc273wbkcl8068ngc40xdlsp4fpdkl0kmbrbd528a7smz9x") (f (quote (("gnustep-2-1" "gnustep-2-0") ("gnustep-2-0" "gnustep-1-9") ("gnustep-1-9" "gnustep-1-8") ("gnustep-1-8" "gnustep-1-7") ("gnustep-1-7") ("default" "apple") ("apple"))))))

(define-public crate-objc2-proc-macros-0.1.1 (c (n "objc2-proc-macros") (v "0.1.1") (h "07j3snswvj6532x32zgn4llc2xaf31rj4iw18n6dsrf2p0jvh1xr") (f (quote (("gnustep-2-1" "gnustep-2-0") ("gnustep-2-0" "gnustep-1-9") ("gnustep-1-9" "gnustep-1-8") ("gnustep-1-8" "gnustep-1-7") ("gnustep-1-7") ("default" "apple") ("apple")))) (r "1.60")))

(define-public crate-objc2-proc-macros-0.1.2 (c (n "objc2-proc-macros") (v "0.1.2") (h "07bni8yn64hdaixczsfvv0jshv8xha0jhlpdxdml57szyaiw3i5f") (f (quote (("gnustep-2-1" "gnustep-2-0") ("gnustep-2-0" "gnustep-1-9") ("gnustep-1-9" "gnustep-1-8") ("gnustep-1-8" "gnustep-1-7") ("gnustep-1-7") ("default") ("apple")))) (y #t) (r "1.60")))

(define-public crate-objc2-proc-macros-0.1.3 (c (n "objc2-proc-macros") (v "0.1.3") (h "1w335fj58k76z94d242xq18qkj7iw082lpy3kxnisaa5r7q4aaa6") (f (quote (("gnustep-2-1" "gnustep-2-0") ("gnustep-2-0" "gnustep-1-9") ("gnustep-1-9" "gnustep-1-8") ("gnustep-1-8" "gnustep-1-7") ("gnustep-1-7") ("default") ("apple")))) (r "1.60")))

