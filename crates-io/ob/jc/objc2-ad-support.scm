(define-module (crates-io ob jc objc2-ad-support) #:use-module (crates-io))

(define-public crate-objc2-ad-support-0.2.0 (c (n "objc2-ad-support") (v "0.2.0") (d (list (d (n "objc2") (r "^0.5.1") (f (quote ("apple"))) (k 0)) (d (n "objc2-foundation") (r "^0.2.0") (d #t) (k 0)))) (h "074m24f0awsz4krx1rc35l9wshy011921yg8yjwg7dm9rihbgivg") (f (quote (("unstable-docsrs") ("std" "alloc" "objc2/std") ("default" "std") ("alloc" "objc2/alloc") ("all" "ASIdentifierManager") ("ASIdentifierManager" "objc2-foundation/NSUUID")))) (r "1.60")))

(define-public crate-objc2-ad-support-0.2.2 (c (n "objc2-ad-support") (v "0.2.2") (d (list (d (n "objc2") (r "^0.5.2") (k 0)) (d (n "objc2-foundation") (r "^0.2.2") (k 0)))) (h "1gsq5dqls3bvqx7817brllgsgnbykl3mq96gi21lr8hsqgzi33pa") (f (quote (("std" "alloc" "objc2/std" "objc2-foundation/std") ("default" "std") ("alloc" "objc2/alloc" "objc2-foundation/alloc") ("all" "ASIdentifierManager") ("ASIdentifierManager" "objc2-foundation/NSUUID")))) (r "1.60")))

