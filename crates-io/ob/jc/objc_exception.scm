(define-module (crates-io ob jc objc_exception) #:use-module (crates-io))

(define-public crate-objc_exception-0.0.1 (c (n "objc_exception") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0rn9ibnbkig129mc0p5dsbkq00dxdlcwyjpcn4b6q6q91jdf4p89")))

(define-public crate-objc_exception-0.0.2 (c (n "objc_exception") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r ">= 0.1, < 0.3") (d #t) (k 0)))) (h "02a22wqcx0hkbnsin7fzgpvvhw8ynv0sks2l4ra1wmk556k8axar")))

(define-public crate-objc_exception-0.1.0 (c (n "objc_exception") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1hmcmj7j40giqqzv18mmp9lfr1iin08z6v3c3s8g7h3v7zcfpv68")))

(define-public crate-objc_exception-0.1.1 (c (n "objc_exception") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1p00damjvy4nbfmrc90d9kbdygycrk76kq1s8v9k1hm35ydd5309")))

(define-public crate-objc_exception-0.1.2 (c (n "objc_exception") (v "0.1.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "191cmdmlypp6piw67y4m8y5swlxf5w0ss8n1lk5xd2l1ans0z5xd")))

