(define-module (crates-io ob jc objc-foundation) #:use-module (crates-io))

(define-public crate-objc-foundation-0.0.1 (c (n "objc-foundation") (v "0.0.1") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.0") (d #t) (k 0)))) (h "0d2046n8q14x2kxkaiv2xvrj67fyfxzfaaza5m4fqlkcd7h7829s")))

(define-public crate-objc-foundation-0.0.2 (c (n "objc-foundation") (v "0.0.2") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.0") (d #t) (k 0)))) (h "01iwychzrqwi56fhp3j9pdxaqqzr6gnszf6b6l57llja1cxcpy4k")))

(define-public crate-objc-foundation-0.0.3 (c (n "objc-foundation") (v "0.0.3") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.0") (d #t) (k 0)))) (h "1va236l736xiz5csfxyv89aqjzi45jm5dwd9x3zym3rq99rn5h7z")))

(define-public crate-objc-foundation-0.0.4 (c (n "objc-foundation") (v "0.0.4") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.1") (d #t) (k 0)) (d (n "objc_id") (r "^0.0") (d #t) (k 0)))) (h "1vsdwv0bhsbyb8fdf0wcqhqfm6z57r8wvl2gj91i2hf0k4n865np")))

(define-public crate-objc-foundation-0.1.0 (c (n "objc-foundation") (v "0.1.0") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc_id") (r "^0.1") (d #t) (k 0)))) (h "129h5i79aarw7d9dbfy933cmadvi5sr9k6kshisqh25nwjz69qr5")))

(define-public crate-objc-foundation-0.1.1 (c (n "objc-foundation") (v "0.1.1") (d (list (d (n "block") (r "^0.1") (d #t) (k 0)) (d (n "objc") (r "^0.2") (d #t) (k 0)) (d (n "objc_id") (r "^0.1") (d #t) (k 0)))) (h "1y9bwb3m5fdq7w7i4bnds067dhm4qxv4m1mbg9y61j9nkrjipp8s")))

