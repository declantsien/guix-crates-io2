(define-module (crates-io ob jc objc2-symbols) #:use-module (crates-io))

(define-public crate-objc2-symbols-0.0.0 (c (n "objc2-symbols") (v "0.0.0") (h "0bygh3nk4asnbydfw0nd10arnca7w7d03g2ipd43ycjyafg5i7wi")))

(define-public crate-objc2-symbols-0.2.2 (c (n "objc2-symbols") (v "0.2.2") (d (list (d (n "objc2") (r "^0.5.2") (k 0)) (d (n "objc2-foundation") (r "^0.2.2") (k 0)))) (h "1p04hjkxan18g2b7h9n2n8xxsvazapv2h6mfmmdk06zc7pz4ws0a") (f (quote (("std" "alloc" "objc2/std" "objc2-foundation/std") ("default" "std") ("alloc" "objc2/alloc" "objc2-foundation/alloc") ("all" "NSSymbolEffect") ("NSSymbolEffect" "objc2-foundation/NSObject")))) (r "1.60")))

