(define-module (crates-io ob jc objc2-system-extensions) #:use-module (crates-io))

(define-public crate-objc2-system-extensions-0.0.0 (c (n "objc2-system-extensions") (v "0.0.0") (h "1jl5q77cbsi4bv704hq51jps29bxzncicjy41gbvwrj8g7r4jsab")))

(define-public crate-objc2-system-extensions-0.2.2 (c (n "objc2-system-extensions") (v "0.2.2") (d (list (d (n "objc2") (r "^0.5.2") (k 0)) (d (n "objc2-foundation") (r "^0.2.2") (f (quote ("NSArray" "NSError" "NSString" "NSURL"))) (k 0)))) (h "1mx1jy08v4v33d4x855mhfw81vzh3mg4zaf3ywqwjs8b9yybg5cm") (f (quote (("std" "alloc" "objc2/std" "objc2-foundation/std") ("default" "std") ("alloc" "objc2/alloc" "objc2-foundation/alloc") ("all")))) (r "1.60")))

