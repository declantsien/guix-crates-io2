(define-module (crates-io ob jc objc2-app-tracking-transparency) #:use-module (crates-io))

(define-public crate-objc2-app-tracking-transparency-0.0.0 (c (n "objc2-app-tracking-transparency") (v "0.0.0") (h "00bqc690qg0vfr8yxjbp5lyghqbhnxqg99fdhskad6m4zg1s44l0")))

(define-public crate-objc2-app-tracking-transparency-0.2.2 (c (n "objc2-app-tracking-transparency") (v "0.2.2") (d (list (d (n "block2") (r "^0.5.1") (o #t) (k 0)) (d (n "objc2") (r "^0.5.2") (k 0)))) (h "187y34l1wd5mvqik6a242k9p5wg8mr0bcxyqpd2s6ycq93jszgr3") (f (quote (("default" "std") ("all" "ATTrackingManager" "block2") ("ATTrackingManager")))) (s 2) (e (quote (("std" "alloc" "block2?/std" "objc2/std") ("block2" "dep:block2") ("alloc" "block2?/alloc" "objc2/alloc")))) (r "1.60")))

