(define-module (crates-io ob jc objc_test_utils) #:use-module (crates-io))

(define-public crate-objc_test_utils-0.0.1 (c (n "objc_test_utils") (v "0.0.1") (d (list (d (n "gcc") (r "^0.1") (d #t) (k 1)))) (h "1qwmcvhrrb7asrzzvsy7m40yalppwf4p19lvblxj9nmarji4pzmn")))

(define-public crate-objc_test_utils-0.0.2 (c (n "objc_test_utils") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "09rckmp5h9bbns08xzicdlk7y5lxj2ygbg3yqk1cszfnzd5n8kzx")))

