(define-module (crates-io ob jc objc) #:use-module (crates-io))

(define-public crate-objc-0.0.1 (c (n "objc") (v "0.0.1") (h "1c753vx7vfjd0kznjpg5kcmb0bh0dr9k2m9ns8n1y4gcpa4a1l41")))

(define-public crate-objc-0.0.2 (c (n "objc") (v "0.0.2") (d (list (d (n "objc_test_utils") (r "^0.0") (d #t) (k 0)))) (h "1ljfhqmdlaf94ykan8waqjd24c2jma1plsdg7bcn8mlq0i77i77z") (y #t)))

(define-public crate-objc-0.0.3 (c (n "objc") (v "0.0.3") (d (list (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "0jbc403k4wwsc2i40w7sd98cz504aiv5sz5817x9hn9kdw4l29l3")))

(define-public crate-objc-0.0.4 (c (n "objc") (v "0.0.4") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "0f55kyca1rfcjkvblp5yz3y3mnpfvlzmq59k73lsxcwmnv1vj9gj")))

(define-public crate-objc-0.0.5 (c (n "objc") (v "0.0.5") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "01i64gnvw9abc81pawarwr9aj5lzrpf8dygwn5h1wxwqan5ljgap")))

(define-public crate-objc-0.0.6 (c (n "objc") (v "0.0.6") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "1lw8wazq0v1bd355m2z2b9pavi2z65r175l1il8yrvz94n0iy4h2") (f (quote (("verify_message_encode") ("default" "verify_message_encode"))))))

(define-public crate-objc-0.0.7 (c (n "objc") (v "0.0.7") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "075955x01bjdlmp7qvknz1kdr4j94m104sbabnz92sag8ykaghdr") (f (quote (("verify_message_encode") ("default" "verify_message_encode"))))))

(define-public crate-objc-0.0.8 (c (n "objc") (v "0.0.8") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "196cgar3lqa23n5f0w3415j6cj2zxbvpc6gswlpw72mr9a9n0bc9")))

(define-public crate-objc-0.1.0 (c (n "objc") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "1f6hsj0rlyl94m2sa53zmafh5m9nj5hsjqih1l90rb1amy5vmv8n")))

(define-public crate-objc-0.1.1 (c (n "objc") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "1i69sfqyx723qpz2jdcy3x3kd7msbnjiy78xgnkx108jsk7bwsan")))

(define-public crate-objc-0.1.2 (c (n "objc") (v "0.1.2") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "02fl0jhjjdm81q4shbfq5hphrkdznsynp6adazhy4g5fz0xnxdlx")))

(define-public crate-objc-0.1.3 (c (n "objc") (v "0.1.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_test_utils") (r "^0.0") (d #t) (k 2)))) (h "1yym2bzhp3dhyxmcba7mwdq18cvggl8b3zd97b2qwrb52zv6bn3a")))

(define-public crate-objc-0.1.4 (c (n "objc") (v "0.1.4") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)))) (h "0rnncdakqq02a4k77m2zasy2nbhlnq37r180gs1bm5rj0c2yqkzr")))

(define-public crate-objc-0.1.5 (c (n "objc") (v "0.1.5") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)))) (h "14qfim0viifpb2xcs66x50mi976n9xwxspbkwa06pxg37ifq3x4l")))

(define-public crate-objc-0.1.6 (c (n "objc") (v "0.1.6") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1xxvr3916bj8981jx13d3mkmj6jmbyhad3ljxahaksb1q5yniprh") (f (quote (("exception" "objc_exception"))))))

(define-public crate-objc-0.1.7 (c (n "objc") (v "0.1.7") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0cara4c1fsh6q97nbzljh9n1x4n77dyhx50kv6q68g1jg7yx2p12") (f (quote (("exception" "objc_exception"))))))

(define-public crate-objc-0.1.8 (c (n "objc") (v "0.1.8") (d (list (d (n "libc") (r ">= 0.1, < 0.3") (d #t) (k 0)) (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0mj00d6fzdn518ryq4r1r32njgvgg1yri8n7by2rh4q3r1zgscsr") (f (quote (("exception" "objc_exception"))))))

(define-public crate-objc-0.2.0 (c (n "objc") (v "0.2.0") (d (list (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.1") (o #t) (d #t) (k 0)))) (h "08xks4bax6f2hq7n4yzrlgl4rvly7prla26mlbdr8n51p7may8yv") (f (quote (("verify_message") ("exception" "objc_exception"))))))

(define-public crate-objc-0.2.1 (c (n "objc") (v "0.2.1") (d (list (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1fjzz9qzicfr6fwah60jsv8fjr7maw2hzyka8wafwyydbam134vw") (f (quote (("verify_message") ("exception" "objc_exception"))))))

(define-public crate-objc-0.2.2 (c (n "objc") (v "0.2.2") (d (list (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0bncws8635v3rm9naxgwxhd22zvhi6rfmk2132dp9xnfgbrk0zw7") (f (quote (("verify_message") ("exception" "objc_exception"))))))

(define-public crate-objc-0.2.3 (c (n "objc") (v "0.2.3") (d (list (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0v7pz5m4qy8h5dyfn6absq21v75671ymcsra4a5fv9g2hjwimzaz") (f (quote (("verify_message") ("exception" "objc_exception"))))))

(define-public crate-objc-0.2.4 (c (n "objc") (v "0.2.4") (d (list (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0myvzqq54aq3k09ifa4r5xh1h9fiqf6ykpzy54fnifz7w9j8vq9x") (f (quote (("verify_message") ("exception" "objc_exception"))))))

(define-public crate-objc-0.2.5 (c (n "objc") (v "0.2.5") (d (list (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0rvkh188dgjr5s60kjs23a8pd59msfjl819a2bib2qakzq7ancwq") (f (quote (("verify_message") ("exception" "objc_exception"))))))

(define-public crate-objc-0.2.6 (c (n "objc") (v "0.2.6") (d (list (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.1") (o #t) (d #t) (k 0)))) (h "03ar7qxhailxgb0zi5lszv7fhwl6b1xkas5y4m8wy1vyng90zlii") (f (quote (("verify_message") ("exception" "objc_exception"))))))

(define-public crate-objc-0.2.7 (c (n "objc") (v "0.2.7") (d (list (d (n "malloc_buf") (r "^0.0") (d #t) (k 0)) (d (n "objc_exception") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1cbpf6kz8a244nn1qzl3xyhmp05gsg4n313c9m3567625d3innwi") (f (quote (("verify_message") ("exception" "objc_exception"))))))

