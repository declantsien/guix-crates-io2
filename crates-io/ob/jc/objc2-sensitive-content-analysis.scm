(define-module (crates-io ob jc objc2-sensitive-content-analysis) #:use-module (crates-io))

(define-public crate-objc2-sensitive-content-analysis-0.0.0 (c (n "objc2-sensitive-content-analysis") (v "0.0.0") (h "0k49assic7c1i96z9p45p4f18dzyy4bm696w7ddsb1grx6z140p4")))

(define-public crate-objc2-sensitive-content-analysis-0.2.2 (c (n "objc2-sensitive-content-analysis") (v "0.2.2") (d (list (d (n "block2") (r "^0.5.1") (o #t) (k 0)) (d (n "objc2") (r "^0.5.2") (k 0)) (d (n "objc2-foundation") (r "^0.2.2") (k 0)))) (h "0q5xga40fg56dhvksd74ri9yxfixn7rjrsc4ydz1il5qs0s6zvci") (f (quote (("default" "std") ("all" "SCSensitivityAnalyzer" "block2") ("SCSensitivityAnalyzer" "objc2-foundation/NSError" "objc2-foundation/NSProgress" "objc2-foundation/NSURL")))) (s 2) (e (quote (("std" "alloc" "block2?/std" "objc2/std" "objc2-foundation/std") ("block2" "dep:block2" "objc2-foundation/block2") ("alloc" "block2?/alloc" "objc2/alloc" "objc2-foundation/alloc")))) (r "1.60")))

