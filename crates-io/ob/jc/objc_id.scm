(define-module (crates-io ob jc objc_id) #:use-module (crates-io))

(define-public crate-objc_id-0.0.1 (c (n "objc_id") (v "0.0.1") (d (list (d (n "objc") (r "^0.1") (d #t) (k 0)))) (h "14ss9wlv6ns2rjhs4k45bfp154gl2j8bybr6fq01nd8jclsijva1")))

(define-public crate-objc_id-0.0.2 (c (n "objc_id") (v "0.0.2") (d (list (d (n "objc") (r "^0.1") (d #t) (k 0)))) (h "0grsr37iglqzi22zh3plicz83gmgi44qi8y13x1migpz65swfrb0")))

(define-public crate-objc_id-0.1.0 (c (n "objc_id") (v "0.1.0") (d (list (d (n "objc") (r "^0.2") (d #t) (k 0)))) (h "15x2r3l33q6zwz9v8vfy3386bi72lc9l3k3wbys2swjdqshhlwz4")))

(define-public crate-objc_id-0.1.1 (c (n "objc_id") (v "0.1.1") (d (list (d (n "objc") (r "^0.2.4") (d #t) (k 0)))) (h "0fq71hnp2sdblaighjc82yrac3adfmqzhpr11irhvdfp9gdlsbf9")))

