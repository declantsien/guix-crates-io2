(define-module (crates-io ob jc objc-encode) #:use-module (crates-io))

(define-public crate-objc-encode-0.0.1 (c (n "objc-encode") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wfgglfvjgibdpnq4xyxw3phbdl1r1k4mjh9yj5z7r5kflk2h6v6")))

(define-public crate-objc-encode-0.0.2 (c (n "objc-encode") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17iqmdxnlznvh990nd7hl0zfkj5k85ahbk5jk528ggvm8mssl2hy")))

(define-public crate-objc-encode-0.0.3 (c (n "objc-encode") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sp16i0y029648wlq9gb28zdfzzscp1nnybihnm8vb9xdkld17py")))

(define-public crate-objc-encode-1.0.0 (c (n "objc-encode") (v "1.0.0") (h "0jqdx2z5i91fzp88yfkdngypxainx97hizf0nspfjmsbp494z6mf")))

(define-public crate-objc-encode-1.1.0 (c (n "objc-encode") (v "1.1.0") (h "08nq3s75wqm8s6fm2c6pyvn793fc5agfpbyiji57vb49sg8gsd4a")))

