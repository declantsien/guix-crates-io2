(define-module (crates-io ob st obstack) #:use-module (crates-io))

(define-public crate-obstack-0.1.0 (c (n "obstack") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "1zrdf9iykk9jabdxmz2qsf3p74h0id3nmnw558s476h8wh7x2cx0")))

(define-public crate-obstack-0.1.1 (c (n "obstack") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "0ncb2dns7whyrwmyhssi63dcyk39lxq9nyf5inssq5i8h01b5m55")))

(define-public crate-obstack-0.1.2 (c (n "obstack") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "1az1zgp4am5kdivlz6kiyaiml65amfzdrwmi4wbnag4ynjra64xq")))

(define-public crate-obstack-0.1.3 (c (n "obstack") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "0c48jnnw7na9yphpm7kbychhmyb5di7276amccyavp3s4n63ww6l")))

(define-public crate-obstack-0.1.4 (c (n "obstack") (v "0.1.4") (d (list (d (n "bencher") (r "^0.1.4") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "1kabk9yh44b143d4ncj76ph0dij0kmyx22vbmvif2cf3sxn0l6pd")))

