(define-module (crates-io ob st obstruct-macros) #:use-module (crates-io))

(define-public crate-obstruct-macros-0.1.0 (c (n "obstruct-macros") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full" "parsing" "printing" "extra-traits"))) (d #t) (k 0)))) (h "00irir9c8q5hpvyqafhdj9by8m014rlh0ar3wlrhgssdlblq8q0h")))

