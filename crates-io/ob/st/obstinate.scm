(define-module (crates-io ob st obstinate) #:use-module (crates-io))

(define-public crate-obstinate-0.1.0 (c (n "obstinate") (v "0.1.0") (h "1ymnvjkyv7rkdkmdfsg5ig1kichcqm68wbgm5gn9lf65ya5zmisd") (y #t)))

(define-public crate-obstinate-0.1.1 (c (n "obstinate") (v "0.1.1") (d (list (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "object_store") (r "^0.6.1") (d #t) (k 0)))) (h "1dwc8622m4sz0s0nl7k3yi6z8w2asbzig70nfvvc72cwxl2kr921") (f (quote (("http" "async" "object_store/http") ("gcp" "async" "object_store/gcp") ("azure" "async" "object_store/azure") ("aws" "async" "object_store/aws") ("async")))) (y #t)))

(define-public crate-obstinate-0.1.2 (c (n "obstinate") (v "0.1.2") (d (list (d (n "memmap2") (r "^0.7.1") (d #t) (k 0)) (d (n "object_store") (r "^0.6.1") (d #t) (k 0)))) (h "19455w4c9ak5chzrc56m6xzmnwxydwa3xg1lw2ibh6z7f82mixhp") (f (quote (("http" "async" "object_store/http") ("gcp" "async" "object_store/gcp") ("azure" "async" "object_store/azure") ("aws" "async" "object_store/aws") ("async")))) (y #t)))

