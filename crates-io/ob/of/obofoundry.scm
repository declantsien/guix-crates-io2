(define-module (crates-io ob of obofoundry) #:use-module (crates-io))

(define-public crate-obofoundry-0.1.0 (c (n "obofoundry") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "0ifgk338zkkv2c6jzf50ysl9y82vkkyadc7cywhmxh4gxj9m8p5k")))

(define-public crate-obofoundry-0.1.1 (c (n "obofoundry") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "1qj4lann0djlsdwqqwbssj8yb5dmqqk8h936zx8i1224fdpzq1cl") (f (quote (("dox") ("default"))))))

(define-public crate-obofoundry-0.1.2 (c (n "obofoundry") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "0vs55m7501lsjp3rc2pfq9nfjw6prb0lahnl5b0d061miy6yr66h")))

(define-public crate-obofoundry-0.2.0 (c (n "obofoundry") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "1n3z7jx8qgg2asy2qmwvsc2d9bmjqbpvrdm1x5qy0c8h8ssmqhhg")))

(define-public crate-obofoundry-0.3.0 (c (n "obofoundry") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "0208blghm6d95xifsnnnhhh3rvhngglpf3wkkcdiqyqjsw1dsv5i")))

(define-public crate-obofoundry-0.4.0 (c (n "obofoundry") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "0793zy68127s5rafhnxp1b42rwd44n53v974y49f5n2idr2vqxvn")))

(define-public crate-obofoundry-0.5.0 (c (n "obofoundry") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "07ilc7kn56fgvh417igrr4sy94lmqmdis5k84x9agv8w78wxh1pw")))

(define-public crate-obofoundry-0.6.0 (c (n "obofoundry") (v "0.6.0") (d (list (d (n "reqwest") (r "^0.10") (f (quote ("blocking"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1q6r5w626qb79mci9vnrv7270qivydmv9bmq4ddbd119v6xa7b3x")))

(define-public crate-obofoundry-0.7.0 (c (n "obofoundry") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "ureq") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "06sw50rz5qcxh9l5jqyqyglr5w1sxj9xlwbhbs9qci0rrkpk75xw")))

(define-public crate-obofoundry-0.8.0 (c (n "obofoundry") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "ureq") (r "^1.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1fxlgzx3mabvlf2xyklz9xkxnily767v9bjd314adyab5mp87f77")))

(define-public crate-obofoundry-0.8.1 (c (n "obofoundry") (v "0.8.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "ureq") (r "^2.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0sbbp9hks33sv78sp8d0jxjphd6pf5clqi1p7nr05a4qwvyibh8r")))

(define-public crate-obofoundry-0.8.2 (c (n "obofoundry") (v "0.8.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "ureq") (r "^2.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "17m52i9diwix07v8jfx9skwm2dmjay6d62w1mgm1kyhc7fpn79da")))

(define-public crate-obofoundry-0.8.3 (c (n "obofoundry") (v "0.8.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "ureq") (r "^2.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1ncdspm6020nxz3ci91ybml75bplcyaznwdg4m78dn5d6lyk7h0v")))

(define-public crate-obofoundry-0.9.0 (c (n "obofoundry") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)) (d (n "ureq") (r "^2.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1sl80dx4xh60wxpgra717975wlagxvni6cyhkxfbkgy8wrqgvnn9")))

(define-public crate-obofoundry-0.10.0 (c (n "obofoundry") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 2)) (d (n "ureq") (r "^2.0") (d #t) (k 2)) (d (n "url") (r "^2.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "1pa9fakk17mm8vza92n4w24pmya2w7k5ahhh78lvgllxfnijywl8")))

