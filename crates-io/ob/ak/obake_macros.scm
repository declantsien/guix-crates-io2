(define-module (crates-io ob ak obake_macros) #:use-module (crates-io))

(define-public crate-obake_macros-1.0.0 (c (n "obake_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02qs23gcfsl8qj8i1sj72i7gvc0r884s77z7c7pz0ykbm9mh5q90")))

(define-public crate-obake_macros-1.0.1 (c (n "obake_macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "03y6affgrfr33sznha208a5ya0qavv1w1jd8c3ncrm4zbl5fq83f")))

(define-public crate-obake_macros-1.0.2 (c (n "obake_macros") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1w6p88kazvnppwz3zklkpwrbsz5wx88wn1by7z17crk2gcf755q2")))

(define-public crate-obake_macros-1.0.3 (c (n "obake_macros") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1nvm6492r1fc8vr8kadw2pj57mxc5zjlhdxgi59dbg0wlxc4016l")))

(define-public crate-obake_macros-1.0.4 (c (n "obake_macros") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12wizpg6ywgdlv7p50gas516gbs6qxm8q1k3704dn20d34an36rl") (f (quote (("serde") ("default"))))))

(define-public crate-obake_macros-1.0.5 (c (n "obake_macros") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p67gki4jjb2zv541r3k1yssz7lvg42rrxi9vjgpm1hvn276n1a7") (f (quote (("serde") ("default"))))))

