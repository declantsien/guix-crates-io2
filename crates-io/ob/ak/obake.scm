(define-module (crates-io ob ak obake) #:use-module (crates-io))

(define-public crate-obake-1.0.0 (c (n "obake") (v "1.0.0") (d (list (d (n "obake_macros") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0z3d4n3i3lmfm50sx2w1dh7bdq6rdxpyxdqy2z4h7bsb4xb9ff8x")))

(define-public crate-obake-1.0.1 (c (n "obake") (v "1.0.1") (d (list (d (n "obake_macros") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0rxlqqwccc5nnjpdiq2dlrws6bmz2qx00405lldzr5rf2xx9z33d")))

(define-public crate-obake-1.0.2 (c (n "obake") (v "1.0.2") (d (list (d (n "obake_macros") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0am0xhw005zxmq9l887nywvl616hgkif9km9rxbwbm9wnfs5vx1s")))

(define-public crate-obake-1.0.3 (c (n "obake") (v "1.0.3") (d (list (d (n "obake_macros") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0bm4f9bzswhrn7lnv8akagxnjrs8k1sjrrgvx0bv2k6634x9ivjq")))

(define-public crate-obake-1.0.4 (c (n "obake") (v "1.0.4") (d (list (d (n "obake_macros") (r "^1.0") (d #t) (k 0)) (d (n "obake_macros") (r "^1.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1z99grj8vifapwpy56nz3687q02w59mj8ay7613alxbi8dj698pd") (f (quote (("serde" "obake_macros/serde") ("default"))))))

(define-public crate-obake-1.0.5 (c (n "obake") (v "1.0.5") (d (list (d (n "obake_macros") (r "^1.0") (d #t) (k 0)) (d (n "obake_macros") (r "^1.0") (f (quote ("serde"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "06q9vr65x0ghb3higzyxq831d4k5d1rqnri3mby3nwg5bd7x65n0") (f (quote (("serde" "obake_macros/serde") ("default"))))))

