(define-module (crates-io ob d2 obd2) #:use-module (crates-io))

(define-public crate-obd2-0.1.0 (c (n "obd2") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "ftdi") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ns2i0n35dp9xf16m24bsixfsx831pqpq5nng10qc1vj6mixvdf2")))

(define-public crate-obd2-0.2.0-pre1 (c (n "obd2") (v "0.2.0-pre1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "ftdi") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ldklca06rm0bjkvr6457mpljini0jnaw2ajs3q92qkgqj1pxmk4")))

(define-public crate-obd2-0.2.0-pre2 (c (n "obd2") (v "0.2.0-pre2") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "ftdi") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "1ngja2yr4za8bqa1asihjhzkcs4jy9dpd7gmx8xfzb12sm1b8nr1")))

(define-public crate-obd2-0.2.0-pre3 (c (n "obd2") (v "0.2.0-pre3") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "ftdi") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.15") (d #t) (k 0)))) (h "13nz5bb0gqq88zql7faki007c7b1amkcimrhpjw3bnn3p8iij3bz")))

