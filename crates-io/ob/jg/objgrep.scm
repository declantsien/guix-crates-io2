(define-module (crates-io ob jg objgrep) #:use-module (crates-io))

(define-public crate-objgrep-1.0.0 (c (n "objgrep") (v "1.0.0") (d (list (d (n "PrintLib") (r "^1.5.2") (d #t) (k 0)) (d (n "capstone") (r "^0.12.0") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "object") (r "^0.34.0") (d #t) (k 0)) (d (n "pad") (r "^0.1.6") (d #t) (k 0)))) (h "1nyrbwpp5fzjycw39pj785wkva704k21ygcx11rzccz2x9hkhsnd")))

