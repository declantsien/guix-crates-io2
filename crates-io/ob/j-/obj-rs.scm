(define-module (crates-io ob j- obj-rs) #:use-module (crates-io))

(define-public crate-obj-rs-0.0.1 (c (n "obj-rs") (v "0.0.1") (h "1fwds7rss5x6dfb1ap17ra5l1bhay2wxv4p01n56hxsjnjyhq1s3")))

(define-public crate-obj-rs-0.0.2 (c (n "obj-rs") (v "0.0.2") (h "0h6m4xya5sn351rw3njwi2dymmiwy0r8bwww31v203zn3295f4jd")))

(define-public crate-obj-rs-0.0.3 (c (n "obj-rs") (v "0.0.3") (d (list (d (n "bear") (r "^0.1.1") (d #t) (k 2)))) (h "0xa4vkfqjj1v53ly8wz7ddqzjiszp05zgfgbfp7f47cg5qd55713")))

(define-public crate-obj-rs-0.0.4 (c (n "obj-rs") (v "0.0.4") (d (list (d (n "bear") (r "^0.1.1") (d #t) (k 2)))) (h "17yan21jv9ab6ppgc3m7q7cnyvza39hjmk1qpf32amh80lj7zn8x")))

(define-public crate-obj-rs-0.0.5 (c (n "obj-rs") (v "0.0.5") (h "1shz8bkkkvjcp6hl733jpy3vxvwszvklnk3jvjlcb1h4w47jms9q")))

(define-public crate-obj-rs-0.1.0 (c (n "obj-rs") (v "0.1.0") (h "0b2cn2nj60658kgz07dzlawx3xnf8s1n5z0gl1lrs6f91dcmniid")))

(define-public crate-obj-rs-0.1.1 (c (n "obj-rs") (v "0.1.1") (h "0k23wsaiayjb75b5rhv5qbb73a9b2h22c233f7d7mb0c2y1i2gvh")))

(define-public crate-obj-rs-0.2.0 (c (n "obj-rs") (v "0.2.0") (h "1mblrixp234vlrpsvdvsbfw9p3sblgfbq6bs8vlw7n2yr7i02qa3")))

(define-public crate-obj-rs-0.2.1 (c (n "obj-rs") (v "0.2.1") (h "066d5hwgpfys32nz0lmb3rvvji9dvn983p76vvg9c3sgziv37rgd")))

(define-public crate-obj-rs-0.2.2 (c (n "obj-rs") (v "0.2.2") (h "0dris5jxsi4rwnyy536383hsf045j6lns650dk0kbwgnf5nrly41")))

(define-public crate-obj-rs-0.3.0 (c (n "obj-rs") (v "0.3.0") (h "0iagwr4f9pqqr3f0q3ff4glp67i2nbvjc26sssnna587isar0bv3")))

(define-public crate-obj-rs-0.4.0 (c (n "obj-rs") (v "0.4.0") (h "0g5lx5gdxmgxhgl1w2wlk2ng9fjlq74h4ghg36y3bnl0v3zlj29p")))

(define-public crate-obj-rs-0.4.1 (c (n "obj-rs") (v "0.4.1") (h "118zwh9sjc1yw31s3q240j3afnydhz297dixcpcw11lahhjn5ikm")))

(define-public crate-obj-rs-0.4.2 (c (n "obj-rs") (v "0.4.2") (h "1mskrd5zfld856nygbsgnifbdrlas68dw86n0iqfmmf70pvsm8zn")))

(define-public crate-obj-rs-0.4.3 (c (n "obj-rs") (v "0.4.3") (d (list (d (n "glium") (r "*") (o #t) (k 0)))) (h "1hb856nprgy9i69ppq5m63fkv3in0kqyqlmnv6pi61zi7cbvjnbg") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.4 (c (n "obj-rs") (v "0.4.4") (d (list (d (n "glium") (r "*") (o #t) (k 0)))) (h "19qic3iv7s63vg7ay5gaar9qbvjvif46g8cjrh7w1xhcjkydqlh3") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.5 (c (n "obj-rs") (v "0.4.5") (d (list (d (n "glium") (r "*") (o #t) (k 0)))) (h "0q9xyc4xh1gmr1zzr9rcq2yf5vg4ymixg4iwx7aci3xp2zdhaq3i") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.6 (c (n "obj-rs") (v "0.4.6") (d (list (d (n "glium") (r "*") (o #t) (k 0)))) (h "1jijkb7haakrvc4qwfy05bgcixm1p094qck933565l8bbrp9bxrn") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.7 (c (n "obj-rs") (v "0.4.7") (d (list (d (n "glium") (r "*") (o #t) (k 0)))) (h "1c3p8fpwqrqq123m6b160zqpr9ff5n38jkc8siyla0bxb240fply") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.8 (c (n "obj-rs") (v "0.4.8") (d (list (d (n "glium") (r "*") (o #t) (k 0)))) (h "06rlg353ica1kw3axxrq0k54rd7islahnznm72qd7h07gdrhlsvv") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.9 (c (n "obj-rs") (v "0.4.9") (d (list (d (n "glium") (r "*") (o #t) (k 0)))) (h "0gkxnfalz6qbbr6cj7y56d0wm9c9cis985hwbbjmi8320kr280hg") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.10 (c (n "obj-rs") (v "0.4.10") (d (list (d (n "glium") (r "^0.8.2") (o #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "1b6v4mf4l6q54zmf1x317z78yfsz40rfwjcvpp91vdpym88cx384") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.11 (c (n "obj-rs") (v "0.4.11") (d (list (d (n "glium") (r "^0.8.2") (o #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "0z15n1v3cd4lrvqaqqc37srv4c6iq05m5pjflsh2aykd4xc0j8v5") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.12 (c (n "obj-rs") (v "0.4.12") (d (list (d (n "glium") (r "^0.8.2") (o #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "0pwc9ivs8hfpg3n0idzckvlqjqgq4a8vz1l7qw5php499viv1a02") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.13 (c (n "obj-rs") (v "0.4.13") (d (list (d (n "glium") (r "^0.8.2") (o #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "1dldg6r7sxm9wd4c1sil3l3pg9696xcyr9f5azkiqdj6wv5pv7nn") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.14 (c (n "obj-rs") (v "0.4.14") (d (list (d (n "glium") (r "^0.8.2") (o #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "1482ksn33wx40iv4056vqb35ksybq3gbacjwgzy870jpc75g4510") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.15 (c (n "obj-rs") (v "0.4.15") (d (list (d (n "glium") (r "^0.8.2") (o #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.3.0") (d #t) (k 0)))) (h "114wc2lviazcdxykx1wa5x91b1n0zzf5cwigckkvr0kdiy6vrqvz") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.16 (c (n "obj-rs") (v "0.4.16") (d (list (d (n "glium") (r "^0.14.0") (o #t) (k 0)) (d (n "glium") (r "^0.14.0") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "01c8p6x98yf30pnkgm0gk5jgnbaf6fzrcy9461l71yzpypghpah6") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.17 (c (n "obj-rs") (v "0.4.17") (d (list (d (n "glium") (r "^0.14.0") (o #t) (k 0)) (d (n "glium") (r "^0.14.0") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "1k12mjcs9dgcs8ijfvfsvkdlvyc3352v0sdyklcklbxwvqi7rkr8") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.18 (c (n "obj-rs") (v "0.4.18") (d (list (d (n "glium") (r "^0.14.0") (o #t) (k 0)) (d (n "glium") (r "^0.14.0") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "0qnxpqbyb29xqlskrsrsi9ncrl6br7h6gi1iq8sii3n70bpylxq9") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.19 (c (n "obj-rs") (v "0.4.19") (d (list (d (n "glium") (r "^0.14.0") (o #t) (k 0)) (d (n "glium") (r "^0.14.0") (d #t) (k 2)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "1jhmjlryx7xw8sxxsxid7ykn9mxw3calbpzs0y2c0rljbh9r7f79") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4.20 (c (n "obj-rs") (v "0.4.20") (d (list (d (n "glium") (r "^0.14.0") (o #t) (k 0)) (d (n "glium") (r "^0.14.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "03f5r41n5bhbasvs70vrg9pmx8ji0k3y3diyy9p9djw1r9qrggm8") (f (quote (("glium-support" "glium")))) (y #t)))

(define-public crate-obj-rs-0.4.21 (c (n "obj-rs") (v "0.4.21") (d (list (d (n "glium") (r "^0.22.0") (o #t) (k 0)) (d (n "glium") (r "^0.22.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "1yxhi77j80lc7c694y50p92gbp7mx68zyppnhb7z2kvsipdk4285") (f (quote (("glium-support" "glium")))) (y #t)))

(define-public crate-obj-rs-0.5.0 (c (n "obj-rs") (v "0.5.0") (d (list (d (n "glium") (r "^0.22.0") (o #t) (k 0)) (d (n "glium") (r "^0.22.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "1cm3mywyhdwq8kddn0nm863zvgha9d8aj18y8652nwglyi5qpwry") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.6.0 (c (n "obj-rs") (v "0.6.0") (d (list (d (n "glium") (r "^0.26.0") (o #t) (k 0)) (d (n "glium") (r "^0.26.0") (d #t) (k 2)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "0acnw94rk7b9v74c6wqc7ch3g56pjs1qx6fhbci1ylk7kw4cng6f") (f (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.6.1 (c (n "obj-rs") (v "0.6.1") (d (list (d (n "glium") (r "^0.26.0") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.8.2") (d #t) (k 0)))) (h "1ppp9mkfcb5gy3fkh42b678bmqj94c70ynlw408kir88jhz0f95w") (f (quote (("glium-support" "glium") ("default" "serde"))))))

(define-public crate-obj-rs-0.6.2 (c (n "obj-rs") (v "0.6.2") (d (list (d (n "glium") (r "^0.26.0") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.8.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "0zl4lxk3i7af2pg8k2kmv6ddpc1h8gski5sk592bydh65cjdsdyg") (f (quote (("glium-support" "glium") ("default" "serde"))))))

(define-public crate-obj-rs-0.6.3 (c (n "obj-rs") (v "0.6.3") (d (list (d (n "glium") (r ">=0.26.0, <0.30.0") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.8.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "0k713aywfhr3m1ni7i528pvlxvjki1aaj507rgv1jcw3qmb5zyr1") (f (quote (("glium-support" "glium") ("default" "serde"))))))

(define-public crate-obj-rs-0.6.4 (c (n "obj-rs") (v "0.6.4") (d (list (d (n "glium") (r ">=0.26.0, <0.30.0") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vec_map") (r "^0.8.2") (d #t) (k 0)) (d (n "vulkano") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1qnp80q7cpjmiq6mdxz9ggwzlcllm5sh75f90xvrxgmbh3cfyv99") (f (quote (("glium-support" "glium") ("default" "serde"))))))

(define-public crate-obj-rs-0.7.0 (c (n "obj-rs") (v "0.7.0") (d (list (d (n "glium") (r ">=0.26.0, <0.30.0") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vulkano") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1d0xrd9syim9j5aag3wdxqihyar88yl3y7a5173abnrkaij2y2n9") (f (quote (("glium-support" "glium") ("default" "serde"))))))

(define-public crate-obj-rs-0.7.1 (c (n "obj-rs") (v "0.7.1") (d (list (d (n "bytemuck") (r "^1") (o #t) (d #t) (k 0)) (d (n "glium") (r ">=0.26.0, <0.33.0") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "vulkano") (r ">=0.19.0, <0.33.0") (o #t) (d #t) (k 0)))) (h "18qxxblh9m6a7ipbmq0pa0wzzlsbbbmj1y2371zdqnm670jih0m5") (f (quote (("glium-support" "glium") ("default" "serde")))) (s 2) (e (quote (("vulkano" "dep:vulkano" "dep:bytemuck") ("serde" "dep:serde") ("glium" "dep:glium"))))))

