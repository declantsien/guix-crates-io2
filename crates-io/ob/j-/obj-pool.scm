(define-module (crates-io ob j- obj-pool) #:use-module (crates-io))

(define-public crate-obj-pool-0.1.0 (c (n "obj-pool") (v "0.1.0") (d (list (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "012yf48rfn0ac4mm1y5xwvfa1x9wn9qv8qa1jjmj1b0pzcsrqkp6")))

(define-public crate-obj-pool-0.2.0 (c (n "obj-pool") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "0lv09xx0d5h58ngacs1c091qah0rbxpfpdl3v66pdij08vsbghm8")))

(define-public crate-obj-pool-0.3.0 (c (n "obj-pool") (v "0.3.0") (d (list (d (n "optional") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "167fnj39b0a3pv2cpi7jn5fraxqycahwl9qgqksprdq5lr2j6cbp")))

(define-public crate-obj-pool-0.3.1 (c (n "obj-pool") (v "0.3.1") (d (list (d (n "optional") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "1hd6j1fipznn8z6xdlyjilhdqm1dsf7zijwd8laq8pqi4107p6cf")))

(define-public crate-obj-pool-0.4.0 (c (n "obj-pool") (v "0.4.0") (d (list (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "0md1al5nyk1693yxjsk87a5y5m442vim7f3il312l9fh60cx3crc")))

(define-public crate-obj-pool-0.4.1 (c (n "obj-pool") (v "0.4.1") (d (list (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "1q3g9r5mdqj7330a3ii65xdz2qyqlj5v8iyqj2s79960fmavdjwg") (f (quote (("serde_support" "serde") ("default"))))))

(define-public crate-obj-pool-0.4.2 (c (n "obj-pool") (v "0.4.2") (d (list (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "0xajgw3zgycfzs9xwll8nfh0x27iz8dbp410l1mc9pk26xnhlkfj") (f (quote (("serde_support" "serde") ("default"))))))

(define-public crate-obj-pool-0.4.3 (c (n "obj-pool") (v "0.4.3") (d (list (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "1h9pn9aid8wqikf5hzghbqwlamc322bvwgf8gpy6cpnvf5y3yynd") (f (quote (("serde_support" "serde" "optional/serde") ("default"))))))

(define-public crate-obj-pool-0.4.4 (c (n "obj-pool") (v "0.4.4") (d (list (d (n "const-random") (r "^0.1.8") (d #t) (k 0)) (d (n "optional") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "1lkgxp0vh52z1jrl7g76hhhhp85dhvzn78lknip7wq1x5vj1hg8j") (f (quote (("serde_support" "serde" "optional/serde") ("default"))))))

(define-public crate-obj-pool-0.5.0 (c (n "obj-pool") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "0cr8ahahn1acagf6f0w7jp3hjd4ijkdhp795myxai5ryp3ll5rix") (f (quote (("serde_support" "serde") ("default"))))))

(define-public crate-obj-pool-0.5.1 (c (n "obj-pool") (v "0.5.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "04ifz0rymrak8j9wbw4hxl8z3hy8lrmi9k1bg0ayzvycgzvkqqxj") (f (quote (("serde_support" "serde") ("default"))))))

