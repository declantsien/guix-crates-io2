(define-module (crates-io ob j- obj-exporter) #:use-module (crates-io))

(define-public crate-obj-exporter-0.1.0 (c (n "obj-exporter") (v "0.1.0") (d (list (d (n "itertools") (r "^0.6.1") (d #t) (k 0)) (d (n "wavefront_obj") (r "^5.1.0") (d #t) (k 0)))) (h "06wqv4b1pb0vic16plk0jqwwhx3fcrxlicdi0hr80rvm4psprxjs")))

(define-public crate-obj-exporter-0.2.0 (c (n "obj-exporter") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "wavefront_obj") (r "^5.1.0") (d #t) (k 0)))) (h "0978pvrnrgzrkhx0z13hxk13r65cqk29hv7bak4k1dbvz86lccyi")))

