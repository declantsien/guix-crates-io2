(define-module (crates-io ob se observe-macro) #:use-module (crates-io))

(define-public crate-observe-macro-0.1.0 (c (n "observe-macro") (v "0.1.0") (d (list (d (n "darling") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s3mra46lbxn4ycsvnsydanr740pshx8d6q1cahr5jcbna3nsacd")))

