(define-module (crates-io ob se observable-rs) #:use-module (crates-io))

(define-public crate-observable-rs-0.0.1 (c (n "observable-rs") (v "0.0.1") (h "1s8fzdhncniyk4j4m44agjgg796glnldhyf3a2aka3lq0sjq4zgj")))

(define-public crate-observable-rs-0.0.2 (c (n "observable-rs") (v "0.0.2") (h "0xzcxdpxznv1c3c69adjx3jynvhy7912qh9sfwj65pf41qrlh87p")))

(define-public crate-observable-rs-0.1.0 (c (n "observable-rs") (v "0.1.0") (h "13djrhd3v6y59hrrrnx5b87ijjhrh4b8bznryj9q4w54s3ibkk2w")))

(define-public crate-observable-rs-0.2.0 (c (n "observable-rs") (v "0.2.0") (h "14l68fcg4vinkqi9vlzqxqdrnmaqxwkx8gp2fw2h58zpcdrd5j5c")))

