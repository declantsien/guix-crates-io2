(define-module (crates-io ob se observatory) #:use-module (crates-io))

(define-public crate-observatory-0.1.0 (c (n "observatory") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)))) (h "18ybgay4692jfvb1w61vcrpphcyharpfcwcdx068bf8bk4cz7907")))

(define-public crate-observatory-0.1.1 (c (n "observatory") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)))) (h "1v238w8b6fiaamp2ak4g28m1f2pl9ap7fflxdhic7h1djld4svqx")))

(define-public crate-observatory-0.1.2 (c (n "observatory") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)))) (h "16za9yzrk9i7l2rm61b2sxfpzsr1k2fzrdvx8afq4iw3marhr9bn")))

(define-public crate-observatory-0.1.3 (c (n "observatory") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)))) (h "1n1shd5x6vdbjfbibbcckvgg5s29dibs3fv9fjinavysh3ihld12")))

(define-public crate-observatory-0.1.4 (c (n "observatory") (v "0.1.4") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)))) (h "14w8lyicmkilhcwmfwla0j0glmprrnrrvjih4q6i3caib2qdmdij")))

(define-public crate-observatory-0.1.5 (c (n "observatory") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)))) (h "1x7yn8zh5fz00spm1ikiq7vfdp3xdsm8dbg8k0k46wcw8r1ihkbf")))

(define-public crate-observatory-0.1.6 (c (n "observatory") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)))) (h "0q3vngcwd0a897a7mmdlmygh6i1pkdi2r2kkswdkcy5gp3nqkl1x")))

(define-public crate-observatory-0.1.7 (c (n "observatory") (v "0.1.7") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8.3") (d #t) (k 0)))) (h "0f623nd696063n7rhg32vymidp01dz899da5a1bh57h8fxb6plb7")))

