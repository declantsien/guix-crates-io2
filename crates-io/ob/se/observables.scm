(define-module (crates-io ob se observables) #:use-module (crates-io))

(define-public crate-observables-0.1.0 (c (n "observables") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.7.1") (o #t) (d #t) (k 0)) (d (n "futures-signals") (r "^0.3.30") (o #t) (d #t) (k 0)) (d (n "pin-project-lite") (r "^0.2.9") (d #t) (k 0)) (d (n "waker-fn") (r "^1.1.0") (o #t) (d #t) (k 0)))) (h "0w27iaka3hvwxkks8mpv97k9x93sja6gznbwkq0n2fqirphbywy7") (f (quote (("default" "futures-signals" "async-channel")))) (s 2) (e (quote (("futures-signals" "dep:futures-signals" "dep:waker-fn") ("async-channel" "dep:async-channel" "dep:waker-fn"))))))

