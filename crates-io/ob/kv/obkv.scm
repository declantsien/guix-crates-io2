(define-module (crates-io ob kv obkv) #:use-module (crates-io))

(define-public crate-obkv-0.1.0 (c (n "obkv") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "071iv95mk4fq2abm5d7ilgiqgfbw3xwav3rf1wjyg0sx2caa4c6f")))

(define-public crate-obkv-0.1.1 (c (n "obkv") (v "0.1.1") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "1s28zb0nj2nnc48lqhcjny4c668swarsancj6kzdlfigmahabn6x")))

(define-public crate-obkv-0.2.0 (c (n "obkv") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "11d3mc0v4qydckk3qn3n65kk4z47zpdpya513lmbanwfgk6li7pn")))

(define-public crate-obkv-0.2.1 (c (n "obkv") (v "0.2.1") (d (list (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "100hv36g93irj8l3d50w87hhxjmfmbmm7l48zwwwcmk089192ibc")))

