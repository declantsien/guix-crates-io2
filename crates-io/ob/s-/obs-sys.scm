(define-module (crates-io ob s- obs-sys) #:use-module (crates-io))

(define-public crate-obs-sys-0.1.0 (c (n "obs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "00wy1j9akf9sqz1akfhcrbc3qig6p1gfd3bjyh07sjrknpjzn7mg")))

(define-public crate-obs-sys-0.1.1 (c (n "obs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1kl2mq6i4grmf3lw6npbf01v4qzl2zg6q4qv2wyif5a93h35mnr0")))

(define-public crate-obs-sys-0.1.2 (c (n "obs-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1sw0vsmzdl1jbm97ycaqznpzyjby24ygc1r1svvh1jvh7y23ndaq")))

(define-public crate-obs-sys-0.1.3 (c (n "obs-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0g89b283xwglbf8jzjg1yiwfdhvp9brc446z1h697713dgzwp983")))

(define-public crate-obs-sys-0.2.0 (c (n "obs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "regex") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "shellexpand") (r "^2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(windows)") (k 1)))) (h "172alknl0qp4k5sjmj87zkbnwz3a5skay8w4wygpjmzs49vnbgzg") (l "obs")))

(define-public crate-obs-sys-0.2.1 (c (n "obs-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "regex") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "shellexpand") (r "^2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(windows)") (k 1)))) (h "0wsc2scnl9bh0n221qnfii6k7n71gd8ww4mdl1ypa70qcar82lmz") (l "obs")))

(define-public crate-obs-sys-0.3.0 (c (n "obs-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "regex") (r "^1.0") (d #t) (t "cfg(windows)") (k 1)) (d (n "shellexpand") (r "^2.0") (d #t) (t "cfg(target_os = \"macos\")") (k 1)) (d (n "winreg") (r "^0.5") (d #t) (t "cfg(windows)") (k 1)))) (h "0098kn81paqq22i8i208ja3b3l5w4nhs5lq5n0gcm8zj5qpnkixw") (l "obs")))

