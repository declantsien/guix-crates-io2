(define-module (crates-io ob s- obs-do) #:use-module (crates-io))

(define-public crate-obs-do-0.1.0 (c (n "obs-do") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "obws") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "16610k46fksgbyb8akzyrxwq2jpih6qgyfncv57zlipgqmyc4lcy")))

(define-public crate-obs-do-0.1.1 (c (n "obs-do") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "obws") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1zhrahlm0hgwj1ggzdpmrj35simzg4s68igax5n7v1yz2hp6zbdm")))

(define-public crate-obs-do-0.1.2 (c (n "obs-do") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "obws") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1pxw49nsk336k7n5knfr0v0c4a9ziy4mn7f08rfw16s6wl14vv21")))

