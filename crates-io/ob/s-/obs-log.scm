(define-module (crates-io ob s- obs-log) #:use-module (crates-io))

(define-public crate-obs-log-0.1.0 (c (n "obs-log") (v "0.1.0") (d (list (d (n "libobs-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "02p30v7zhzf9ky62p61ddy1san9js6ap5dc10ghd97crbhi70iha")))

(define-public crate-obs-log-0.1.1 (c (n "obs-log") (v "0.1.1") (d (list (d (n "libobs-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (f (quote ("std"))) (d #t) (k 0)))) (h "1xx1q8snnzyjnq4yif0cwa3207h21ijnc1xjs6k37yszqrnixzys")))

