(define-module (crates-io ob s- obs-countdown) #:use-module (crates-io))

(define-public crate-obs-countdown-0.0.1 (c (n "obs-countdown") (v "0.0.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winnow") (r "^0.6.5") (d #t) (k 0)))) (h "02g96hwff3x5jm5h8pm08v977r6ym5q7hynwc0y7qpgrsci8h4aj")))

(define-public crate-obs-countdown-0.0.2 (c (n "obs-countdown") (v "0.0.2") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winnow") (r "^0.6.5") (d #t) (k 0)))) (h "00vfbi9cvkpwnvckc50ckqxx8fqc052w007i0himr0100qrwkpkh")))

(define-public crate-obs-countdown-0.0.3 (c (n "obs-countdown") (v "0.0.3") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winnow") (r "^0.6.5") (d #t) (k 0)))) (h "1arzqqjnzl3bmjjz7x4qj96blrjs9n1x42fgaprsmjpp0pwn3hkr")))

(define-public crate-obs-countdown-0.0.4 (c (n "obs-countdown") (v "0.0.4") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winnow") (r "^0.6.5") (d #t) (k 0)))) (h "17a2h86h61jp7iiv4fbxgkj0wk38ad8fzjaggx6mcmkvgrjk4dys")))

