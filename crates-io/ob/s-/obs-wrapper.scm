(define-module (crates-io ob s- obs-wrapper) #:use-module (crates-io))

(define-public crate-obs-wrapper-0.1.0 (c (n "obs-wrapper") (v "0.1.0") (d (list (d (n "obs-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0fkzcbzak5y5przbdanph5wq7gcf88m42agg7k1q7pdmxzpx7wv3")))

(define-public crate-obs-wrapper-0.1.1 (c (n "obs-wrapper") (v "0.1.1") (d (list (d (n "obs-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0gq1v1gz7lnpc24q0n3drywqjafrj3qdfclxrns7c7fqz5k4s4fq")))

(define-public crate-obs-wrapper-0.1.2 (c (n "obs-wrapper") (v "0.1.2") (d (list (d (n "obs-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)) (d (n "xcb-util") (r "^0.3.0") (f (quote ("ewmh"))) (d #t) (k 0)))) (h "0vi43nfrw0jvhmibh4h9qw44gajg64cms7w6drbd00pxrvpvpx48")))

(define-public crate-obs-wrapper-0.1.3 (c (n "obs-wrapper") (v "0.1.3") (d (list (d (n "obs-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1vfj9v1dwzy789fgnn8r67c7b2rii3mdpfjrpnlw7x1ljfqpmvnn")))

(define-public crate-obs-wrapper-0.1.4 (c (n "obs-wrapper") (v "0.1.4") (d (list (d (n "obs-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1fx7l4rcy682pjsxw9i92js19jzmc5kfiy7bpkksh8qc10wvvbc4")))

(define-public crate-obs-wrapper-0.1.5 (c (n "obs-wrapper") (v "0.1.5") (d (list (d (n "obs-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0r0ivr4b2v65wba0n870y6lisw8fmrr0s83b7vja251s0xc3af61")))

(define-public crate-obs-wrapper-0.1.6 (c (n "obs-wrapper") (v "0.1.6") (d (list (d (n "obs-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "127l9859msmd8mzh7bf2yf6v2p8lqzk1nz01rmfxx3245j6acb7w")))

(define-public crate-obs-wrapper-0.2.0 (c (n "obs-wrapper") (v "0.2.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "obs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0xk19v3hba6zf73pzza9vmhh7hrn8miiasnqsfg01pny1ir735ic")))

(define-public crate-obs-wrapper-0.2.2 (c (n "obs-wrapper") (v "0.2.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "obs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0ypqzz14w5bzi184zigq1yq7mw1cx9xaw13r583dp8s7znpdi068")))

(define-public crate-obs-wrapper-0.2.3 (c (n "obs-wrapper") (v "0.2.3") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "obs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "17ghsw1ikv9kpf7bf378dzhlj96gr88srikrg1qhcilgcz3zwjkk")))

(define-public crate-obs-wrapper-0.3.0 (c (n "obs-wrapper") (v "0.3.0") (d (list (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "obs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "16784y0mmicagg3hb65jw7rqdxhncr3mnsivk4issl1klg59k0zf")))

(define-public crate-obs-wrapper-0.3.1 (c (n "obs-wrapper") (v "0.3.1") (d (list (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "obs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1wngfd3p85af63y0xm84z46a2srv2xps10shk3r2pxq79jn62zmz")))

(define-public crate-obs-wrapper-0.3.2 (c (n "obs-wrapper") (v "0.3.2") (d (list (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "obs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1904z945snn469670xvi0qdj4xxl88kf59hn42dg8m4fs9i92ny9")))

(define-public crate-obs-wrapper-0.3.3 (c (n "obs-wrapper") (v "0.3.3") (d (list (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "obs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "153r5cqxpvbi3vhm05hanlsw1v7myff8lh4694hbjaybw6vfc2k7")))

(define-public crate-obs-wrapper-0.3.4 (c (n "obs-wrapper") (v "0.3.4") (d (list (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "obs-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0am4p0l0bnwhlqy7krcc7i6z0f2g7vqvqc4a9qhbmj5l38v0vx5p")))

(define-public crate-obs-wrapper-0.4.0 (c (n "obs-wrapper") (v "0.4.0") (d (list (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "obs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "0r0zwqly9zz8jfnlm5ayipjkxbsq6f4zj52d343lmvbwh1bb6g52")))

(define-public crate-obs-wrapper-0.4.1 (c (n "obs-wrapper") (v "0.4.1") (d (list (d (n "log") (r "^0.4.11") (f (quote ("std"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "obs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "1dn184dc18yc7qp4x407hz0vvjdir6rn9g8hkir42l1fy1md22ci")))

