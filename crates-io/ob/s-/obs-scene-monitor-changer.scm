(define-module (crates-io ob s- obs-scene-monitor-changer) #:use-module (crates-io))

(define-public crate-obs-scene-monitor-changer-0.1.0 (c (n "obs-scene-monitor-changer") (v "0.1.0") (d (list (d (n "enigo") (r "^0.0.14") (d #t) (k 0)) (d (n "notify") (r "^5.0.0") (f (quote ("serde" "macos_kqueue"))) (k 0)) (d (n "obws") (r "^0.10.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.148") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.22.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)))) (h "17wl69rw7l3fq2mqqip7f19gz1ygf0nnnq0ngsb59a16hzymw62p") (r "1.62.1")))

