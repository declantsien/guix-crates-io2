(define-module (crates-io ob je objecthash) #:use-module (crates-io))

(define-public crate-objecthash-0.1.0 (c (n "objecthash") (v "0.1.0") (d (list (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 2)))) (h "0i2r6xxcbbahnhyg3y1xz4hrb3xfivhc23z5jg578n9fvg7kz8hz") (f (quote (("objecthash-ring") ("default"))))))

(define-public crate-objecthash-0.1.1 (c (n "objecthash") (v "0.1.1") (d (list (d (n "ring") (r ">= 0.2") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 2)) (d (n "unicode-normalization") (r ">= 0.1.2") (d #t) (k 0)))) (h "023c5mifdr5y9mvg0ih91g6sjcra5ii3qh54gmc03gd0yz7glfqb") (f (quote (("octet-strings") ("objecthash-ring" "ring") ("default" "objecthash-ring"))))))

(define-public crate-objecthash-0.2.0 (c (n "objecthash") (v "0.2.0") (d (list (d (n "ring") (r ">= 0.2") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 2)) (d (n "unicode-normalization") (r ">= 0.1.2") (d #t) (k 0)))) (h "05jcwmnyiw2gl57nnl8vcr70587s6p7djbxfhn0p9q1fi4bjrkv1") (f (quote (("octet-strings") ("objecthash-ring" "ring") ("default" "objecthash-ring"))))))

(define-public crate-objecthash-0.2.1 (c (n "objecthash") (v "0.2.1") (d (list (d (n "ring") (r ">= 0.2") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 2)) (d (n "unicode-normalization") (r ">= 0.1.2") (d #t) (k 0)))) (h "0pjyd1f07ag7pc2grlghllr1jz4h65g190yqqd4j3zkibzmc27zn") (f (quote (("octet-strings") ("objecthash-ring" "ring") ("default" "objecthash-ring"))))))

(define-public crate-objecthash-0.2.2 (c (n "objecthash") (v "0.2.2") (d (list (d (n "ring") (r ">= 0.2") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 2)) (d (n "unicode-normalization") (r ">= 0.1.2") (d #t) (k 0)))) (h "1a5ckp5db9kkl9lr24f9jr1ld4b1nfzky662jzm4fnwph3yh4wa6") (f (quote (("octet-strings") ("objecthash-ring" "ring") ("default" "objecthash-ring"))))))

(define-public crate-objecthash-0.3.0 (c (n "objecthash") (v "0.3.0") (d (list (d (n "ring") (r ">= 0.2") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 2)) (d (n "unicode-normalization") (r ">= 0.1.2") (d #t) (k 0)))) (h "1x2mkp81fk233mviqmg6s7gc4l1dl806dyrrw3rsnm4np50sy1gp") (f (quote (("octet-strings") ("objecthash-ring" "ring") ("default" "objecthash-ring"))))))

(define-public crate-objecthash-0.4.0 (c (n "objecthash") (v "0.4.0") (d (list (d (n "ring") (r ">= 0.2") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 2)) (d (n "unicode-normalization") (r ">= 0.1.2") (d #t) (k 0)))) (h "02l3wlgfx07p0za4wc4j6j8ywg6y8248s4p2ir2qg051p8dxrsa9") (f (quote (("octet-strings") ("objecthash-ring" "ring") ("default" "objecthash-ring"))))))

(define-public crate-objecthash-0.4.1 (c (n "objecthash") (v "0.4.1") (d (list (d (n "ring") (r ">= 0.2") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r ">= 0.3.19") (d #t) (k 2)) (d (n "unicode-normalization") (r ">= 0.1.2") (d #t) (k 0)))) (h "0qkiladlaa1v7jsdr9zanqd4w9phpg2wakphs8qdix4llkyrdp0l") (f (quote (("octet-strings") ("objecthash-ring" "ring") ("default" "objecthash-ring"))))))

