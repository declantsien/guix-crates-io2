(define-module (crates-io ob je object-provider) #:use-module (crates-io))

(define-public crate-object-provider-0.1.0 (c (n "object-provider") (v "0.1.0") (h "1m5ipk3d4964vajvxq1klc21gz0pyrpa8pc47gn3l05zv5igfvjd")))

(define-public crate-object-provider-0.2.0 (c (n "object-provider") (v "0.2.0") (h "1jl0yzhzss13z6nsvvs1l2j5b4sipqqcl0zclfrlv2fbnq648iyf")))

