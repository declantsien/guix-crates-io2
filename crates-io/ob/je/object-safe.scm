(define-module (crates-io ob je object-safe) #:use-module (crates-io))

(define-public crate-object-safe-0.1.0 (c (n "object-safe") (v "0.1.0") (h "0lmw76sdh6sc0s9pbmd3rnsh46kazc3006pxsp05dakbyr2761ak")))

(define-public crate-object-safe-0.2.0 (c (n "object-safe") (v "0.2.0") (h "0a8g4p4l31pbzin8xfqsw1kkqlc2c742xl8aqdavv3izh7vbn5fy")))

(define-public crate-object-safe-0.3.0 (c (n "object-safe") (v "0.3.0") (h "0kssbvyzylkn74avqc946kbdsp6y9ibrwp82vvg9hgiygbh2v515")))

(define-public crate-object-safe-0.3.1 (c (n "object-safe") (v "0.3.1") (h "1jn23ifx6awy2hh2dkc1jdrswp2ajdim063zj85dsykpavkz1sg2")))

