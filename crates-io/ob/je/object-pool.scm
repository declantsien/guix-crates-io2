(define-module (crates-io ob je object-pool) #:use-module (crates-io))

(define-public crate-object-pool-0.1.0 (c (n "object-pool") (v "0.1.0") (h "0z85galxp4yc7gh8bapimvn73gx8hzad7n1lf72xrqlvhbxq45qw")))

(define-public crate-object-pool-0.1.1 (c (n "object-pool") (v "0.1.1") (h "16z5gvkrhfyg8vwkxvwmvj6r7db6n86q5hgav0mq55fcbqly3rwg")))

(define-public crate-object-pool-0.1.2 (c (n "object-pool") (v "0.1.2") (h "10zbzh3ccnj815wwcg1f3ix73iz33rs60lgqpc98m7kixzs6b0m0")))

(define-public crate-object-pool-0.1.3 (c (n "object-pool") (v "0.1.3") (h "08ljqyrwsv0dm8y36qis80l35ccbr6qxq9krq78h7h5j78ml2ig2")))

(define-public crate-object-pool-0.1.6 (c (n "object-pool") (v "0.1.6") (h "1fbp966di1080blbp0kg57ykhw1bn3hwbfzn6papzc2kxr44fap5")))

(define-public crate-object-pool-0.2.0 (c (n "object-pool") (v "0.2.0") (d (list (d (n "replace_with") (r "^0.1") (d #t) (k 0)))) (h "0lyq1m5pv727hca13qpj68arxgs9zspajwmya0bk2h1hybcxw3il")))

(define-public crate-object-pool-0.2.1 (c (n "object-pool") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.3") (d #t) (k 2)))) (h "1f7nh6g12vl5lfmdnjadv9d3mfzjgzc7wxpsy21ryinafg381fl2")))

(define-public crate-object-pool-0.2.2 (c (n "object-pool") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.3") (d #t) (k 2)))) (h "0jcdp60kmzifk44m9w6c7pb2b7cjka4da9dqncv1af95h65f02xw")))

(define-public crate-object-pool-0.2.3 (c (n "object-pool") (v "0.2.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.3") (d #t) (k 2)))) (h "14hk9p70cx9sfmcis3nzvswgzgv4i00sjxkk2xiiw9rqhnkqqwwf")))

(define-public crate-object-pool-0.3.0 (c (n "object-pool") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "08rq8n2qlsgwji3nz5w0964kkxhxxq5czy4wn0c262sy82nyi2ra")))

(define-public crate-object-pool-0.3.1 (c (n "object-pool") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.3") (d #t) (k 2)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "1g64x90j6g1qz0gl9gnmij19x1k05z558dhwj4gfs8i7c0lrcvb5")))

(define-public crate-object-pool-0.4.0 (c (n "object-pool") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "0msldd87izvb9gidky7v9wbvh0ln3i27a8yci8af97d42hkf8i20")))

(define-public crate-object-pool-0.4.1 (c (n "object-pool") (v "0.4.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)))) (h "1q94rcc52n526664z1ac6r2z958ayddk6iimya2al2frabnq7psp")))

(define-public crate-object-pool-0.4.2 (c (n "object-pool") (v "0.4.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0402cq5k461n231aprngwcz0f97qqqh4j12k8ymif66zqxxl6msf")))

(define-public crate-object-pool-0.4.3 (c (n "object-pool") (v "0.4.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "046qwprqggb23g3z9lxdsx672cy4abc1iys9y4fmdppqrylnx136")))

(define-public crate-object-pool-0.4.4 (c (n "object-pool") (v "0.4.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0sfdf86hgd8siw2m8m7hpcqg8b2gbb3adqc1dd7jd1fipxbnp3dg")))

(define-public crate-object-pool-0.5.0 (c (n "object-pool") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "crossbeam-epoch") (r "^0.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0ybqz8s076189igw3b7hs6fnwl62509m5hpriryqx2yjd488mcnn")))

(define-public crate-object-pool-0.5.1 (c (n "object-pool") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0lbj14z09c4lhgszqkwaiirgmld457njsxyjy4kk0k740h7hfvjl")))

(define-public crate-object-pool-0.5.2 (c (n "object-pool") (v "0.5.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "15l4bppg719j3nw79grwfa3lc96icl5g4p5xaflwyiyxfk83clc8")))

(define-public crate-object-pool-0.5.3 (c (n "object-pool") (v "0.5.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0v3fcfwl45w4jk3sivjwaxi4lk8r3955vrcpng55hixlswchfa2p")))

(define-public crate-object-pool-0.5.4 (c (n "object-pool") (v "0.5.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "criterion-plot") (r "^0.4") (d #t) (k 2)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0s4d6bap4b4fazz4izgqvrc6si4fdrbz2fdr09hci7nhjrqkx6pf")))

