(define-module (crates-io ob je objekt) #:use-module (crates-io))

(define-public crate-objekt-0.1.0 (c (n "objekt") (v "0.1.0") (h "1f0spzwv5dg4w78r9j7gqn0m71q2p7z211a71965iyb7fg5dyy6a")))

(define-public crate-objekt-0.1.1 (c (n "objekt") (v "0.1.1") (h "1x1r5l5f0ls9xnqjd2cv1xafvg152359pngqgbrxcmpd71dmhckx")))

(define-public crate-objekt-0.1.2 (c (n "objekt") (v "0.1.2") (h "0g6cb6m1y18xnd5sin3smcrgvwfjwm3qykkm8ypa95xd7npa6s90")))

(define-public crate-objekt-0.2.0 (c (n "objekt") (v "0.2.0") (h "19lhidq63prmbwx0ycckgn00hhqlmbap31310y76c7ilq2i126li")))

