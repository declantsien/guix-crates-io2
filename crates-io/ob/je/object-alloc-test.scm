(define-module (crates-io ob je object-alloc-test) #:use-module (crates-io))

(define-public crate-object-alloc-test-0.1.0 (c (n "object-alloc-test") (v "0.1.0") (d (list (d (n "interpolate_idents") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "object-alloc") (r "^0.1.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "twox-hash") (r "^1.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "11w8kp4k459mkgl2n2qcg09qilkhmd8bzxr76yd1hkvmblcx6bai")))

