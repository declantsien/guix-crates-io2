(define-module (crates-io ob je object-chain) #:use-module (crates-io))

(define-public crate-object-chain-0.1.0 (c (n "object-chain") (v "0.1.0") (h "1gx6cwql82q3kx43203zygn4xalbk1bbpsm1788g7a05a6bmvbjj")))

(define-public crate-object-chain-0.1.1 (c (n "object-chain") (v "0.1.1") (h "0y4rprxlh2gxr10nw2nfmj4z27pj4hbg3mi9k3ybp3qqq5l0cyzc")))

(define-public crate-object-chain-0.1.2 (c (n "object-chain") (v "0.1.2") (h "1xykszpwsy4sxbh57p7iyrmgh93kpgr7vk6p5lxp1wsds6s6qm7b")))

(define-public crate-object-chain-0.1.3 (c (n "object-chain") (v "0.1.3") (h "1pc4jy8hmnqnqffi1hz7v3827k974xn00mcrnzvk0m8gicajdbs1")))

