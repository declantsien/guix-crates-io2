(define-module (crates-io ob je objekt-clonable) #:use-module (crates-io))

(define-public crate-objekt-clonable-0.2.0 (c (n "objekt-clonable") (v "0.2.0") (d (list (d (n "objekt") (r "^0.1.2") (d #t) (k 0)) (d (n "objekt-clonable-impl") (r "^0.2.0") (d #t) (k 0)))) (h "0m6niq4gynhvmnbmx6ksm8jyhiwkqpg4wf6zhqh2a8wwcsjjs58y")))

(define-public crate-objekt-clonable-0.2.1 (c (n "objekt-clonable") (v "0.2.1") (d (list (d (n "objekt") (r "^0.1.2") (d #t) (k 0)) (d (n "objekt-clonable-impl") (r "^0.2.1") (d #t) (k 0)))) (h "116z5zmcncsky4ps15p4x3nnzay77a9rhppmr6zsm0z5p0aim12w")))

(define-public crate-objekt-clonable-0.2.2 (c (n "objekt-clonable") (v "0.2.2") (d (list (d (n "objekt") (r "^0.1.2") (d #t) (k 0)) (d (n "objekt-clonable-impl") (r "^0.2.2") (d #t) (k 0)))) (h "1k7xvlrknacppkcrsjjnpr7j9nbmx2idlqv165j4kb636kk5f56p")))

