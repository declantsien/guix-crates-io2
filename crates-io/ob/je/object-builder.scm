(define-module (crates-io ob je object-builder) #:use-module (crates-io))

(define-public crate-object-builder-0.1.0-rc1 (c (n "object-builder") (v "0.1.0-rc1") (d (list (d (n "clap") (r "3.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "serde_yaml") (r "0.8.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "00zwqsldisnd6g7mn8fm1vvgywagnl3ip9l76wn40x09fsd1l6d0")))

(define-public crate-object-builder-1.0.0 (c (n "object-builder") (v "1.0.0") (d (list (d (n "clap") (r "3.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "serde_yaml") (r "0.8.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "1vdb6x9z52gxc9xm5j9r1f0am8682hv7r4b8phbfvrkiw10rr389")))

(define-public crate-object-builder-1.1.0 (c (n "object-builder") (v "1.1.0") (d (list (d (n "clap") (r "3.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)) (d (n "serde_yaml") (r "0.8.*") (d #t) (k 0)) (d (n "toml") (r "0.5.*") (d #t) (k 0)))) (h "0ra7sj3bxxcvlydjcp3pizl7bgd61a07idvjb9ga8ibpc6vqw2c0")))

(define-public crate-object-builder-1.1.1 (c (n "object-builder") (v "1.1.1") (d (list (d (n "clap") (r "^3.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0kzmlkqrw9gg05cgw7l5ppwvpm9xdnxxvh1cm6kx71vn5akqwa73")))

