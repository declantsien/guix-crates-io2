(define-module (crates-io ob je objekt-clonable-impl) #:use-module (crates-io))

(define-public crate-objekt-clonable-impl-0.2.0 (c (n "objekt-clonable-impl") (v "0.2.0") (d (list (d (n "objekt") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "027ds31hc73dc9q3s86mmgl4l12jhar34vly5idlfa71d1y6cnca")))

(define-public crate-objekt-clonable-impl-0.2.1 (c (n "objekt-clonable-impl") (v "0.2.1") (d (list (d (n "objekt") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1agim1x38mgjn3ymv2rndlzixhq1wjm15pgm4hyq7q2wv59pgpvs")))

(define-public crate-objekt-clonable-impl-0.2.2 (c (n "objekt-clonable-impl") (v "0.2.2") (d (list (d (n "objekt") (r "^0.1.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4.24") (d #t) (k 0)) (d (n "quote") (r "^0.6.10") (d #t) (k 0)) (d (n "syn") (r "^0.15.18") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1jb0a7s5kz3gj8n8cvnflkjvjz7d457ci22cji15sp3k6i6qs86y")))

