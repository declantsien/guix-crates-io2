(define-module (crates-io ob je object-id) #:use-module (crates-io))

(define-public crate-object-id-0.1.0 (c (n "object-id") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "1gc5yca38y1zmaq1zp657jia65ay802nka1nnmxwiq2q1q22nb4w")))

(define-public crate-object-id-0.1.1 (c (n "object-id") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "0j6lqm5vpgs66f71m3apx9kb4yy3skw8dlk3r02hyr51vpr638gx")))

(define-public crate-object-id-0.1.2 (c (n "object-id") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "1rab7jhb3iqhbrw8imf2rzinjn55jp673iw2y83rfl6g65nknhqn")))

(define-public crate-object-id-0.1.3 (c (n "object-id") (v "0.1.3") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "1cvvmj1h948vk2rgvvnfvibg7195gy390xmbmh2h1m4h1x03kk72")))

(define-public crate-object-id-0.1.4 (c (n "object-id") (v "0.1.4") (d (list (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "1abha0n4l44zvm47np0pxbg86pc75bww1bs20i9ahn9rsqfbv1y5")))

