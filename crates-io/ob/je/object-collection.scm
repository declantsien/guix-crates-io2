(define-module (crates-io ob je object-collection) #:use-module (crates-io))

(define-public crate-object-collection-0.0.1 (c (n "object-collection") (v "0.0.1") (h "11kj48f3lcwizr2nyb0nsngqr19hav8zw28npf49xkkflr8x0n14")))

(define-public crate-object-collection-0.0.2 (c (n "object-collection") (v "0.0.2") (h "1qlyv2anvkdn1ihz87cd6sxmzmkjsfspgwh1r5qcbql7852j30ch")))

(define-public crate-object-collection-0.0.3 (c (n "object-collection") (v "0.0.3") (h "1gs2nzsllmzgsazh6yx118lys53qx42ikfj64ygs90fl22fssmjh")))

(define-public crate-object-collection-0.0.4 (c (n "object-collection") (v "0.0.4") (h "1mfzjg4mxpg8f9g0dc1sbhw024v08pffw93760aca928jxsx60rr")))

(define-public crate-object-collection-0.0.5 (c (n "object-collection") (v "0.0.5") (h "03hjfxg2239lf8cfmzsr9vqhjdvlzyg9axrikw0v4s3k4zkmrm5g")))

