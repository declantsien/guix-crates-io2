(define-module (crates-io ob je object-space) #:use-module (crates-io))

(define-public crate-object-space-0.1.0 (c (n "object-space") (v "0.1.0") (d (list (d (n "chashmap") (r "^2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 2)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "009a3n9a9ghdngn7jfg22255bdlhkpp6192lf16n9vqwlwh8dqja")))

(define-public crate-object-space-0.1.1 (c (n "object-space") (v "0.1.1") (d (list (d (n "chashmap") (r "^2.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "image") (r "^0.18") (d #t) (k 2)) (d (n "indexmap") (r "^1.0") (d #t) (k 0)) (d (n "ordered-float") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "08dsc1prhrkmdnzl6janv1lrz1a0hkjhhg7jjc63y4idjdr84g62")))

