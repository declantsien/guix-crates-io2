(define-module (crates-io ob je objectify) #:use-module (crates-io))

(define-public crate-objectify-0.1.0 (c (n "objectify") (v "0.1.0") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)))) (h "0blvxnzipxa82gvhyfg4y068g1jvxxrmfk8w24m99hjkw02c2r0f")))

(define-public crate-objectify-0.1.1 (c (n "objectify") (v "0.1.1") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10.0") (d #t) (k 0)))) (h "1a4cv93c3rgbxvnac30m9acrfddpv19q5p2wmcbimczgaz4kv3fy")))

(define-public crate-objectify-0.2.2 (c (n "objectify") (v "0.2.2") (d (list (d (n "object") (r "^0.30.3") (d #t) (k 0)))) (h "11s4jirzbj23wj96kikpdk4ygnlkkv3wgqp3ynxzkkz1jfmvicsl")))

