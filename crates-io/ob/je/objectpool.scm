(define-module (crates-io ob je objectpool) #:use-module (crates-io))

(define-public crate-objectpool-0.0.0 (c (n "objectpool") (v "0.0.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "crossbeam-queue") (r "^0.3") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0ylkgiqhqraa0lvdp9qblhqlx5n1mjklhksm70db9awmvyg9lnvd") (f (quote (("std" "crossbeam-queue/default") ("default" "std")))) (y #t) (r "1.56")))

(define-public crate-objectpool-0.1.0 (c (n "objectpool") (v "0.1.0") (d (list (d (n "crossbeam-queue") (r "^0.3") (k 0)) (d (n "loom") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1hjbp02njdnr59lpp16j0y3ghd0aj9nb78ww2p06lq38lnx6pad4") (f (quote (("std" "crossbeam-queue/default") ("default" "std") ("alloc" "crossbeam-queue/alloc")))) (r "1.56")))

