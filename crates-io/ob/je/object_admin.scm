(define-module (crates-io ob je object_admin) #:use-module (crates-io))

(define-public crate-object_admin-0.1.0 (c (n "object_admin") (v "0.1.0") (d (list (d (n "headless_batch_engine") (r "^0.1.0") (d #t) (k 0)) (d (n "headless_common") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.155") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "180wfc4y6wn4d8qrrsmy0f7ygnbl9zp8plr9qbf8bsx34bs41fq4") (r "1.68.0")))

