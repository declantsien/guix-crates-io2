(define-module (crates-io ob je object-type) #:use-module (crates-io))

(define-public crate-object-type-0.1.0 (c (n "object-type") (v "0.1.0") (h "14cglg38xpljimv9ac50qk402f6dvbffprsns0hnnl156kwfq06g") (y #t)))

(define-public crate-object-type-0.1.1 (c (n "object-type") (v "0.1.1") (h "13q1zhhh8liilrhshknw48r40l417lqxlv766liv989r19ynnn5k")))

