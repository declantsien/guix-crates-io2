(define-module (crates-io ob je objectify-bin) #:use-module (crates-io))

(define-public crate-objectify-bin-0.2.3 (c (n "objectify-bin") (v "0.2.3") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "comfy-table") (r "^6.1.4") (d #t) (k 0)) (d (n "heck") (r "^0.4.1") (d #t) (k 0)) (d (n "objectify") (r "^0.2") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^11.1.0") (d #t) (k 0)))) (h "0vmlm4q8n0x2h6cxbszcssmzkx2r00m77r047xawm5bav7ayhp7d")))

