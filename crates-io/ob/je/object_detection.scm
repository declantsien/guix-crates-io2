(define-module (crates-io ob je object_detection) #:use-module (crates-io))

(define-public crate-object_detection-0.1.0 (c (n "object_detection") (v "0.1.0") (d (list (d (n "carrot_common_interface") (r "^0.1.0") (d #t) (k 0)) (d (n "carrot_image_utils") (r "^0.1.10") (d #t) (k 0)) (d (n "carrot_utils") (r "^0.1.7") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)))) (h "0csqgdal3g2nv9zhp1k1gjid1a9djwlckdxpnlbqf2fby21dbkrv")))

(define-public crate-object_detection-0.1.1 (c (n "object_detection") (v "0.1.1") (d (list (d (n "carrot_common_interface") (r "^0.1.0") (d #t) (k 0)) (d (n "carrot_image_utils") (r "^0.1.10") (d #t) (k 0)) (d (n "carrot_utils") (r "^0.1.7") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)))) (h "0ipia0zsqs0bzdjgabn1f7j6ignb0v1v9j0z1sm6lj2977x541wq")))

(define-public crate-object_detection-0.1.2 (c (n "object_detection") (v "0.1.2") (d (list (d (n "carrot_common_interface") (r "^0.1.0") (d #t) (k 0)) (d (n "carrot_image_utils") (r "^0.1.10") (d #t) (k 0)) (d (n "carrot_utils") (r "^0.1.7") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "imageproc") (r "^0.23") (d #t) (k 0)))) (h "0ig981k0rifiy15yyj5arsgz72x1j3k7xkfdc7b0zlzpli2wqri9")))

