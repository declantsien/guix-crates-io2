(define-module (crates-io ob je object-query) #:use-module (crates-io))

(define-public crate-object-query-0.1.0 (c (n "object-query") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0sbpg789xb0d7na6z8p4g3gawwz1szxyj5v57gr1gm5zn7z4pc12") (f (quote (("json" "serde_json") ("default" "json"))))))

(define-public crate-object-query-0.1.1 (c (n "object-query") (v "0.1.1") (d (list (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0rr156qpyd7d0g1hrdyzjidf8b0y4h43rb2v8hs1lsl4yjj8jwms") (f (quote (("json" "serde_json") ("default" "json"))))))

(define-public crate-object-query-0.1.2 (c (n "object-query") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1pn3r9z7hzs5i2b2500kzf9jnxbgf8rrgn16hnp3d9x2zpjpgkcs") (f (quote (("json" "serde_json") ("default" "json"))))))

(define-public crate-object-query-0.1.3 (c (n "object-query") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0gyf15lgzsvhj5fyl8gacsjcc6ylmzljycx7wv3fkhfdgf15mp69") (f (quote (("std" "serde_json/std") ("json" "serde_json") ("default" "std" "json"))))))

(define-public crate-object-query-0.1.4 (c (n "object-query") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "1d06vzhblilsmv811jz057x45fixwcs14w55mzsxjk29p5m9gwg5") (f (quote (("std" "serde_json/std") ("json" "serde_json") ("default" "std" "json"))))))

(define-public crate-object-query-0.1.5 (c (n "object-query") (v "0.1.5") (d (list (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0ib1jidnbkxvfqd9bhf6hd7hbrxc8jidczh6ca31ans9m0ikix9q") (f (quote (("std" "serde_json/std") ("json" "serde_json") ("default" "std" "json"))))))

(define-public crate-object-query-0.1.6 (c (n "object-query") (v "0.1.6") (d (list (d (n "serde_json") (r "^1.0") (f (quote ("alloc"))) (o #t) (k 0)))) (h "16rq5x6ihbdj3iv9cfdrgwrky6sh214nd1s2vwjgspz9a48npng8") (f (quote (("std" "serde_json/std") ("json" "serde_json") ("default" "std" "json"))))))

