(define-module (crates-io ob je object-alloc) #:use-module (crates-io))

(define-public crate-object-alloc-0.1.0 (c (n "object-alloc") (v "0.1.0") (h "0x859c5hvgph6lzp7z3jmhsvdjxwxizcy3hsdlflrql1rky2ny52")))

(define-public crate-object-alloc-0.2.0 (c (n "object-alloc") (v "0.2.0") (h "1qpp4ki94x1c064xb362w28s9psg82aqzb1bg42byzawv3cdhm4d")))

