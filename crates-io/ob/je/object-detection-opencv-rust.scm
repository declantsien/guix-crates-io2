(define-module (crates-io ob je object-detection-opencv-rust) #:use-module (crates-io))

(define-public crate-object-detection-opencv-rust-0.1.0 (c (n "object-detection-opencv-rust") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "opencv") (r "^0.66") (f (quote ("dnn" "imgcodecs" "imgproc"))) (k 0)))) (h "1fngrk8lrrx099k185kfbz3ycnf9ax4w2w0aiyfgpm9f0843khrn")))

