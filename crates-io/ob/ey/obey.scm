(define-module (crates-io ob ey obey) #:use-module (crates-io))

(define-public crate-obey-0.1.0 (c (n "obey") (v "0.1.0") (h "0n610v86mhyq48g53rsq13qxzfpx7873c5dwpb28bhc9pfhy30rm")))

(define-public crate-obey-0.1.1 (c (n "obey") (v "0.1.1") (h "13qv0wi69f421ppl3bmq5s7gc0fv9bk4swb57b7bf3wiy8mn64w3")))

