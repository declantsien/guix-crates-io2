(define-module (crates-io ob si obsidian2neorg) #:use-module (crates-io))

(define-public crate-obsidian2neorg-0.1.0 (c (n "obsidian2neorg") (v "0.1.0") (d (list (d (n "clap") (r "^4.4") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "1sj1wwrw0gq5f8i4d86dh2fgwpm2byrc7gkmplvmjlmjxdrpihy6")))

(define-public crate-obsidian2neorg-0.1.1 (c (n "obsidian2neorg") (v "0.1.1") (d (list (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)))) (h "0m184crr07livgiwb1z1a21200qp6xn1an403r5vi0ks310nbfnd")))

