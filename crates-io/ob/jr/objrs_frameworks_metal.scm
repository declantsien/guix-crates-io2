(define-module (crates-io ob jr objrs_frameworks_metal) #:use-module (crates-io))

(define-public crate-objrs_frameworks_metal-0.0.1 (c (n "objrs_frameworks_metal") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "objrs") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_foundation") (r "^0.0.1") (d #t) (k 0)))) (h "16rw2r8f43cp7in4r14jy9hc0mjjdr64dm3sw5i5mfj3jxnznv13")))

(define-public crate-objrs_frameworks_metal-0.0.2 (c (n "objrs_frameworks_metal") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "objrs") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_foundation") (r "^0.0.2") (d #t) (k 0)))) (h "05qh0i6k37hxqqbld0nzbkhjdx95nr181s34ks2yw3f853vss2pm")))

