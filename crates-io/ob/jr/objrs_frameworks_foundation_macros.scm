(define-module (crates-io ob jr objrs_frameworks_foundation_macros) #:use-module (crates-io))

(define-public crate-objrs_frameworks_foundation_macros-0.0.0 (c (n "objrs_frameworks_foundation_macros") (v "0.0.0") (d (list (d (n "literalext") (r "^0.2.0") (f (quote ("proc-macro"))) (k 0)))) (h "1fi8lzzlc9xz3r3j2lyidfdz9npbpqrglsm0jjlmxg47kvg0i8r9")))

(define-public crate-objrs_frameworks_foundation_macros-0.0.1 (c (n "objrs_frameworks_foundation_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "0s6cx4yr8aavm6gqv92j46mw4zb09rnack3p4ahw6k8j242qqg69")))

(define-public crate-objrs_frameworks_foundation_macros-0.0.2 (c (n "objrs_frameworks_foundation_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "181gflxbwqrhs7f1d2pzrj6rq84py6ij160vx8s2s6pad24n4952")))

