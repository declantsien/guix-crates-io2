(define-module (crates-io ob jr objrs_frameworks_app_kit) #:use-module (crates-io))

(define-public crate-objrs_frameworks_app_kit-0.0.1 (c (n "objrs_frameworks_app_kit") (v "0.0.1") (d (list (d (n "objrs") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_core_graphics") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_foundation") (r "^0.0.1") (d #t) (k 0)))) (h "0civcq53g59jjgbrg4fcf60hp7q1vh7xvk30w4pqyglbx46dld14")))

(define-public crate-objrs_frameworks_app_kit-0.0.2 (c (n "objrs_frameworks_app_kit") (v "0.0.2") (d (list (d (n "objrs") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_core_graphics") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_foundation") (r "^0.0.2") (d #t) (k 0)))) (h "1cd2rxm0id7wsswa431i0b1y9ysrzmmny6a87yvm18jn5qr1kv7s")))

