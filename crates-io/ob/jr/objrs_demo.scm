(define-module (crates-io ob jr objrs_demo) #:use-module (crates-io))

(define-public crate-objrs_demo-0.0.0 (c (n "objrs_demo") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "objrs") (r "^0.0.0") (d #t) (k 0)) (d (n "objrs_frameworks_foundation") (r "^0.0.0") (d #t) (k 0)))) (h "1ns4c4v767a7qhzsmvxwblrm88hy1dy54x6kxbhh7brr8nvizlfl")))

(define-public crate-objrs_demo-0.0.1 (c (n "objrs_demo") (v "0.0.1") (d (list (d (n "objrs") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_app_kit") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_core_graphics") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_foundation") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_metal") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_metal_kit") (r "^0.0.1") (d #t) (k 0)))) (h "0a7k6imwl14h1aa2b57gz0nk9bwxnhl0bvavy0cvyz6kmi6vqxkj")))

(define-public crate-objrs_demo-0.0.2 (c (n "objrs_demo") (v "0.0.2") (d (list (d (n "objrs") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_app_kit") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_core_graphics") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_foundation") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_metal") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_metal_kit") (r "^0.0.2") (d #t) (k 0)))) (h "1vcc3zmdsdk094hivrypy8023qmj8xab9a7p48zwbmly4hdwbc7c")))

