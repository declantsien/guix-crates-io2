(define-module (crates-io ob jr objrs_frameworks_metal_kit) #:use-module (crates-io))

(define-public crate-objrs_frameworks_metal_kit-0.0.1 (c (n "objrs_frameworks_metal_kit") (v "0.0.1") (d (list (d (n "objrs") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_app_kit") (r "^0.0.1") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objrs_frameworks_core_graphics") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_metal") (r "^0.0.1") (d #t) (k 0)))) (h "0yvgk68rbb66rldcc22k8v29wbc9nxbnksisf3jzqp89zkijnj0z")))

(define-public crate-objrs_frameworks_metal_kit-0.0.2 (c (n "objrs_frameworks_metal_kit") (v "0.0.2") (d (list (d (n "objrs") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_app_kit") (r "^0.0.2") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "objrs_frameworks_core_graphics") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_metal") (r "^0.0.2") (d #t) (k 0)))) (h "0c9bhwvq6wslx3mkns100yrj1q5raw3adsf5h1hkshm155zdwnpw")))

