(define-module (crates-io ob jr objrs_runtime) #:use-module (crates-io))

(define-public crate-objrs_runtime-0.0.0 (c (n "objrs_runtime") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0x2ik66jj94wsasdl7jp4iqa3nc5rpr5i57kg7dmlwa5x72nkyck")))

(define-public crate-objrs_runtime-0.0.1 (c (n "objrs_runtime") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0ap7jl9h6gn8pvvk5p3dsz73q0alkl6qaykznz6s2sriglf7wll0")))

(define-public crate-objrs_runtime-0.0.2 (c (n "objrs_runtime") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0hydlgr3h531hravij9z0jv1a78gslgr1jkzs3ssd2ysf6awnq4i")))

