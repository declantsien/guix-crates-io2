(define-module (crates-io ob jr objrs_frameworks_foundation) #:use-module (crates-io))

(define-public crate-objrs_frameworks_foundation-0.0.0 (c (n "objrs_frameworks_foundation") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "objrs") (r "^0.0.0") (d #t) (k 0)) (d (n "objrs_frameworks_foundation_macros") (r "^0.0.0") (d #t) (k 0)))) (h "1m4cdcb15l339qi79m3v0904whjsv802xiklm2kgmkx31f5q6q4z")))

(define-public crate-objrs_frameworks_foundation-0.0.1 (c (n "objrs_frameworks_foundation") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "objrs") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_frameworks_foundation_macros") (r "^0.0.1") (d #t) (k 0)))) (h "16ybrkjp4si1aziq24lhxcd2km744dbpsvbrcvrhp5i2li877bkl")))

(define-public crate-objrs_frameworks_foundation-0.0.2 (c (n "objrs_frameworks_foundation") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "objrs") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_frameworks_foundation_macros") (r "^0.0.2") (d #t) (k 0)))) (h "095j5x0gr63iikvrhb6n64hs33cx92pa4ysij1rz565gibwngilk")))

