(define-module (crates-io ob jr objrs_frameworks_core_graphics) #:use-module (crates-io))

(define-public crate-objrs_frameworks_core_graphics-0.0.1 (c (n "objrs_frameworks_core_graphics") (v "0.0.1") (d (list (d (n "objrs") (r "^0.0.1") (d #t) (k 0)))) (h "02bgx4i164dq7bgj618379sqmkd15l10lrh8a9y6kingydsm2cgx")))

(define-public crate-objrs_frameworks_core_graphics-0.0.2 (c (n "objrs_frameworks_core_graphics") (v "0.0.2") (d (list (d (n "objrs") (r "^0.0.2") (d #t) (k 0)))) (h "0w9svkgnzw6aja2lkf28bawbd50xpqqhqdw3x47yws19r8a7w6fv")))

