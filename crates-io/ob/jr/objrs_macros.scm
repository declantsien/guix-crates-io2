(define-module (crates-io ob jr objrs_macros) #:use-module (crates-io))

(define-public crate-objrs_macros-0.0.0 (c (n "objrs_macros") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full"))) (d #t) (k 0)))) (h "0p8q77nw5cjpsclwf06w7ljqvpyiyj7lx5v4z5vdch8kqrvf8nsn")))

(define-public crate-objrs_macros-0.0.1 (c (n "objrs_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "1398jcgb77aky3lbnjs55wrcvd451lddrrs7j9vayqkm1ayfbhii")))

(define-public crate-objrs_macros-0.0.2 (c (n "objrs_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "visit-mut" "extra-traits"))) (d #t) (k 0)))) (h "0697vrnxjnplyydr9n6p2xw5f16am3f29bgphavb7f0jp0c85jpz")))

