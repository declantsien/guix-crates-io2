(define-module (crates-io ob jr objrs) #:use-module (crates-io))

(define-public crate-objrs-0.0.0 (c (n "objrs") (v "0.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "objrs_macros") (r "^0.0.0") (d #t) (k 0)) (d (n "objrs_runtime") (r "^0.0.0") (d #t) (k 0)))) (h "08959s4ph09rj9pyxjwhlxbaizv7qysh7pxybp4hkpdqvf5vwcrl") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-objrs-0.0.1 (c (n "objrs") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "objrs_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "objrs_runtime") (r "^0.0.1") (d #t) (k 0)))) (h "0rjq4gwgaf0dsxawbwmrbk4xi0j2wrmdsvgwdhyb49yn47x7x0cf") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-objrs-0.0.2 (c (n "objrs") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "objrs_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "objrs_runtime") (r "^0.0.2") (d #t) (k 0)))) (h "11636hdnx23076gxxxyfynhp3kshgga4cyd0i99qz0fm56r9hr85") (f (quote (("use_std") ("default" "use_std"))))))

