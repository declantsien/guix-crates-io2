(define-module (crates-io ob sc obscura) #:use-module (crates-io))

(define-public crate-obscura-0.0.1 (c (n "obscura") (v "0.0.1") (h "09j6xm1m01g343b00phv7kw9d89nccpb66mjk5iwkibhnlxiw22v")))

(define-public crate-obscura-0.0.2 (c (n "obscura") (v "0.0.2") (h "0j9yb7scbfb98drwl6gkp6fjq6waycsyy6givy8i64lqmgaaqq09")))

