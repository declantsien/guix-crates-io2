(define-module (crates-io ob fu obfuscate-integer) #:use-module (crates-io))

(define-public crate-obfuscate-integer-0.1.0 (c (n "obfuscate-integer") (v "0.1.0") (h "0ysx2cy9qq6hd6pr23wmjfchnp2ymghpif00iy59cc346bzhqgr7")))

(define-public crate-obfuscate-integer-0.1.1 (c (n "obfuscate-integer") (v "0.1.1") (h "02pl2q5vk0kgmfvj3i0gh95lkjdw7s3n0zabmkkp8s2pd5mcr8qz") (f (quote (("forbid-unsafe") ("default")))) (y #t)))

(define-public crate-obfuscate-integer-0.1.2 (c (n "obfuscate-integer") (v "0.1.2") (h "1k79dv59vjsm8935325rp6dajgg466fwkhaf2j8bc1w3kr9an3y2") (f (quote (("forbid-unsafe") ("default"))))))

