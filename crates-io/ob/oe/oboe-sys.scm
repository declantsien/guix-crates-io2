(define-module (crates-io ob oe oboe-sys) #:use-module (crates-io))

(define-public crate-oboe-sys-0.1.0 (c (n "oboe-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.11") (o #t) (d #t) (k 1)))) (h "1gcadhsbv1kj56v15i75jbsj7q3phmyb0cq5vvj08z9rj0g8azxd") (f (quote (("static-link") ("rustdoc") ("generate-bindings" "bindgen" "git2") ("compile-library" "cmake" "git2"))))))

(define-public crate-oboe-sys-0.2.0 (c (n "oboe-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (d #t) (k 1)))) (h "1q9x777i5g6y4r1qkam68bqbpyr9xqhg2gxqv939zjr61iigrai4") (f (quote (("test") ("shared-link") ("rustdoc") ("generate-bindings" "bindgen") ("compile-library" "cmake"))))))

(define-public crate-oboe-sys-0.2.1 (c (n "oboe-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.55") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (d #t) (k 1)))) (h "0r7swd5l2fs3xi4qmmfi1ja2fal7pwlv4j4fz11ygk730w7wr6av") (f (quote (("test") ("shared-link") ("rustdoc") ("generate-bindings" "bindgen") ("compile-library" "cmake"))))))

(define-public crate-oboe-sys-0.3.0 (c (n "oboe-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.55") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.2") (d #t) (k 1)))) (h "10lvp65p7jl7ksivqpvyv7c53wb2bacmrv4y317f7aqfc18pmzv8") (f (quote (("test") ("shared-link") ("generate-bindings" "bindgen") ("compile-library" "cmake"))))))

(define-public crate-oboe-sys-0.4.0 (c (n "oboe-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.56") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.3") (o #t) (d #t) (k 1)))) (h "00zipv93in4z1ixw9cg9xk7qw21f7yz7jbqpz86q5z42s1j941py") (f (quote (("test") ("shared-stdcxx") ("shared-link") ("generate-bindings" "bindgen") ("fetch-prebuilt" "fetch_unroll"))))))

(define-public crate-oboe-sys-0.4.2 (c (n "oboe-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.58") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.3") (o #t) (d #t) (k 1)))) (h "17nagb55pfj03avqfzmrsm1p74736kf2c98qip07kxd3ba1n93mq") (f (quote (("test") ("shared-stdcxx") ("shared-link") ("generate-bindings" "bindgen") ("fetch-prebuilt" "fetch_unroll"))))))

(define-public crate-oboe-sys-0.4.3 (c (n "oboe-sys") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.3") (o #t) (d #t) (k 1)))) (h "118m30apnyigid35k2g6ahcz11dy89nm11drnnwa3vvf1jsak67v") (f (quote (("test") ("shared-stdcxx") ("shared-link") ("generate-bindings" "bindgen") ("fetch-prebuilt" "fetch_unroll")))) (y #t)))

(define-public crate-oboe-sys-0.4.4 (c (n "oboe-sys") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1p7ad6ia0vfm8akrb9knr87swhqf8rzzda2s4z4fvamhbap4509k") (f (quote (("test") ("shared-stdcxx") ("shared-link") ("generate-bindings" "bindgen") ("fetch-prebuilt" "fetch_unroll"))))))

(define-public crate-oboe-sys-0.4.5 (c (n "oboe-sys") (v "0.4.5") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1gcl494yy880h2gfgsbdd32g2h0s1n94v58j5hil9mrf6yvsnw1k") (f (quote (("test") ("shared-stdcxx") ("shared-link") ("generate-bindings" "bindgen") ("fetch-prebuilt" "fetch_unroll"))))))

(define-public crate-oboe-sys-0.5.0 (c (n "oboe-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1wmmsyfbw6jipr7rxka7yc949b2i5c5njw6gvp7x665pgxg1ai3z") (f (quote (("test") ("shared-stdcxx") ("shared-link") ("generate-bindings" "bindgen") ("fetch-prebuilt" "fetch_unroll"))))))

(define-public crate-oboe-sys-0.6.0 (c (n "oboe-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1c7ig6gv9206v8xx3q1plzjxgxs3b931n9m0d3mg3cdbrcbk4l86") (f (quote (("test") ("shared-stdcxx") ("shared-link") ("generate-bindings" "bindgen") ("fetch-prebuilt" "fetch_unroll"))))))

(define-public crate-oboe-sys-0.6.1 (c (n "oboe-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "fetch_unroll") (r "^0.3") (o #t) (d #t) (k 1)))) (h "17g7yb4kk6bakc4rhv1izfcqjgqhpkasgq6gf20nc79b9adb12vc") (f (quote (("test") ("shared-stdcxx") ("shared-link") ("generate-bindings" "bindgen") ("fetch-prebuilt" "fetch_unroll"))))))

