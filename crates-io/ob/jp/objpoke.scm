(define-module (crates-io ob jp objpoke) #:use-module (crates-io))

(define-public crate-objpoke-0.1.0 (c (n "objpoke") (v "0.1.0") (d (list (d (n "eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "goblin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0qdmi53108hm4g0s7hjc1n0vl4slpncacywglz6f61bv0j2h1pgx")))

(define-public crate-objpoke-0.2.0 (c (n "objpoke") (v "0.2.0") (d (list (d (n "eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "goblin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "19p18z51ixf0v25yx3n342c144hs79naflglki36lra0g9zs82w0")))

(define-public crate-objpoke-0.2.1 (c (n "objpoke") (v "0.2.1") (d (list (d (n "eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "goblin") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "19wy1q9mpp5g19hb26x4g0hc12k18v580cb1rrab12f9ly8zjf42")))

(define-public crate-objpoke-0.2.3 (c (n "objpoke") (v "0.2.3") (d (list (d (n "eyre") (r "^0.6.3") (d #t) (k 0)) (d (n "goblin") (r "^0.4.3") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "13xpcw34y8f4zckbiyml6z1yljbg8v5w1679i6fxz968dbpcpxpj")))

(define-public crate-objpoke-0.2.4 (c (n "objpoke") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.56") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "goblin") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "scroll") (r "^0.11") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "15gp3h015l8xqwraiay273pblx2ckvi9lkjm45ic5a98iabgf0ss")))

(define-public crate-objpoke-0.3.0 (c (n "objpoke") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.56") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "goblin") (r "=0.4.1") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "scroll") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0c0ca2jj0ivjrlcwjgm8xp4g9r0z5875w1i7533qpqp43j8x49pj")))

