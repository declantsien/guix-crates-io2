(define-module (crates-io ob jp objpool) #:use-module (crates-io))

(define-public crate-objpool-0.1.0 (c (n "objpool") (v "0.1.0") (h "04rv8n57kksvbsw5c2rci6f81prbpljs8jccnj9va7vi0j7hhdlh")))

(define-public crate-objpool-0.2.0 (c (n "objpool") (v "0.2.0") (h "16pk9hgljr82bybk0m7mcdvsqaqblc8pbxsbfy4nzrsg0hqnb74l")))

