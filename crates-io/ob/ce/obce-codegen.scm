(define-module (crates-io ob ce obce-codegen) #:use-module (crates-io))

(define-public crate-obce-codegen-0.1.0 (c (n "obce-codegen") (v "0.1.0") (d (list (d (n "blake2") (r "^0.10") (d #t) (k 0)) (d (n "darling") (r "^0.14") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("parsing" "full" "visit" "extra-traits"))) (d #t) (k 0)) (d (n "tuple") (r "^0.5") (d #t) (k 0)))) (h "14fr09hdjp75665qisavnkwcss7vn4nzhxl1sn1bgwilhss3j1vm") (f (quote (("std") ("default" "std"))))))

