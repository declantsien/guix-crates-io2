(define-module (crates-io ob fs obfstr) #:use-module (crates-io))

(define-public crate-obfstr-0.1.0 (c (n "obfstr") (v "0.1.0") (d (list (d (n "obfstr-impl") (r "^0.1.0") (d #t) (k 0)))) (h "06ybr7pg0r86whczq1kl4k6nggf1g8q9a4j3lc6sf2fdkzawv1c3") (f (quote (("unsafe_static_str") ("rand" "obfstr-impl/rand") ("default" "rand"))))))

(define-public crate-obfstr-0.1.1 (c (n "obfstr") (v "0.1.1") (d (list (d (n "obfstr-impl") (r "^0.1.1") (d #t) (k 0)))) (h "17w9rgx31mf382y2izjhvksn953w1sw09bq1q7zqxgvbf9x6gpjg") (f (quote (("unsafe_static_str") ("rand" "obfstr-impl/rand") ("default" "rand"))))))

(define-public crate-obfstr-0.2.0 (c (n "obfstr") (v "0.2.0") (h "0f6b1p8x552k5ph384vzpdns63f2zb0mhv1jqgwry0ik0apcsf88") (f (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2.1 (c (n "obfstr") (v "0.2.1") (h "0ash27mz2l1fzlvdxyq22hg556m5nhjanj59lchjbajj57nv7fgn") (f (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2.2 (c (n "obfstr") (v "0.2.2") (h "0jgiz6y9my6nnn9k4qhsi19wgmm7cd0avm3x0m4kssrj46pjlb4c") (f (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2.3 (c (n "obfstr") (v "0.2.3") (h "10dhik130j2qpcia4hqq4dbgd128zmpabhm8mav5pfp84n0p6gph") (f (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2.4 (c (n "obfstr") (v "0.2.4") (h "09yk87w6bl6mq6ybvjr15bzv4bqv059qqign4g721z69lf402cmp") (f (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2.5 (c (n "obfstr") (v "0.2.5") (h "1h9bjp2g8vv5jfrqx8a4cz5z9r1lcy9k8m606cdgb7q18001nckh") (f (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2.6 (c (n "obfstr") (v "0.2.6") (h "0l4ixd3adsa0pfkiawzgc1mnyg5485hn7i1b6wcgk3568271vl5b") (f (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.3.0 (c (n "obfstr") (v "0.3.0") (h "0y5azgk8ds43m1sd5pc4hhakxzxkfcfscl24y8gsbvwdznxjqavv")))

(define-public crate-obfstr-0.4.0 (c (n "obfstr") (v "0.4.0") (h "0h527nz3563mlbljbbp42jkh4xf5gg4ziy583ql8xszh7dpbssdr")))

(define-public crate-obfstr-0.4.1 (c (n "obfstr") (v "0.4.1") (h "0hn582zzf97hlsz5gfmlzf51inx2kphzv0s59y1yczgdzj12k8s3")))

(define-public crate-obfstr-0.4.2 (c (n "obfstr") (v "0.4.2") (h "0ic8f9jrc5iylkyk9bjm138mkdap173mbpfvl3v1w94z1gnx1gbj") (y #t)))

(define-public crate-obfstr-0.4.3 (c (n "obfstr") (v "0.4.3") (h "1p2ihfqrsi12sqaf1i87m27vdiphzsbyydrqs6k11jbcp1wjkfp3")))

