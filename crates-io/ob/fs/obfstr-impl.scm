(define-module (crates-io ob fs obfstr-impl) #:use-module (crates-io))

(define-public crate-obfstr-impl-0.1.0 (c (n "obfstr-impl") (v "0.1.0") (d (list (d (n "rand") (r "^0.6") (o #t) (d #t) (k 0)))) (h "19krc8xwxp2895m2zvcs97l26mrk76b8i5xhjjaxc615bz5qx96q")))

(define-public crate-obfstr-impl-0.1.1 (c (n "obfstr-impl") (v "0.1.1") (d (list (d (n "rand") (r "^0.6") (o #t) (d #t) (k 0)))) (h "1r35gvbdv4zw3m82y7fh2szyjhc71z2hnx4zvfjsdphpra5gyxfr")))

