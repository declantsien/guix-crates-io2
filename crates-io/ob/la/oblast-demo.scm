(define-module (crates-io ob la oblast-demo) #:use-module (crates-io))

(define-public crate-oblast-demo-0.1.0 (c (n "oblast-demo") (v "0.1.0") (d (list (d (n "blst") (r "^0.3.10") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.4") (d #t) (k 0)))) (h "0hq1znl6k22zsacnv57236pvgi0c99yax8mw8ljx60ljnxqrwisn")))

