(define-module (crates-io ob ex obex) #:use-module (crates-io))

(define-public crate-obex-0.1.0 (c (n "obex") (v "0.1.0") (d (list (d (n "git2") (r "^0.13.11") (d #t) (k 0)))) (h "1i1q5vndixi9avhqqih76l9phhb80x0lgk0xbdab20ard1mbccm1")))

(define-public crate-obex-0.2.0 (c (n "obex") (v "0.2.0") (d (list (d (n "git2") (r "^0.13.11") (d #t) (k 0)))) (h "1fsfh4y032qy1jwxlkg3c9j5kh8m5wk6xb3k1hxnpg9fpnvzh092")))

