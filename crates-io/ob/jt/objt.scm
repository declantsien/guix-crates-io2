(define-module (crates-io ob jt objt) #:use-module (crates-io))

(define-public crate-objt-0.1.0 (c (n "objt") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.26") (d #t) (k 2)) (d (n "tracing") (r "^0.1.36") (d #t) (k 2)))) (h "00vgfxqvib3yydw2za0p8y42hsl4l7c3hbhkjz0010x9vdrdw2xx") (f (quote (("default") ("debug_borrows"))))))

