(define-module (crates-io ob li oblivious-transfer) #:use-module (crates-io))

(define-public crate-oblivious-transfer-0.0.1 (c (n "oblivious-transfer") (v "0.0.1") (d (list (d (n "aes-ctr") (r "^0.3.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "curve25519-dalek") (r "^2.0.0") (f (quote ("serde" "alloc"))) (d #t) (k 0)) (d (n "digest") (r "^0.8.0") (d #t) (k 0)) (d (n "generic-array") (r "^0.12.0") (d #t) (k 0)) (d (n "packed_simd") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rand_core") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha3") (r "^0.8.1") (d #t) (k 2)))) (h "0p5pg62iz0salkbrqnpb4jkfmik6pzkgldwn7l0ilixj11vag4mk")))

