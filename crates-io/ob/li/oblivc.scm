(define-module (crates-io ob li oblivc) #:use-module (crates-io))

(define-public crate-oblivc-0.1.0 (c (n "oblivc") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libobliv-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libobliv-sys") (r "^0.1.2") (d #t) (k 1)))) (h "1fjc4vlncfhjqvqjv91dlw9la0kf3qxnngv8wciq3c7ai05xs047")))

(define-public crate-oblivc-0.1.1 (c (n "oblivc") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libobliv-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "libobliv-sys") (r "^0.1.3") (d #t) (k 1)))) (h "0ps3zbsyhw2ngfdghr5krb75d7yp7grcpil9jd1adqwlzl21w1k6")))

(define-public crate-oblivc-0.1.2 (c (n "oblivc") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "libobliv-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "libobliv-sys") (r "^0.1.5") (d #t) (k 1)))) (h "1qqb9vkyql6j616fmp7plglsg0skc61b8jp4p6s5mrw9j5d4bgis")))

