(define-module (crates-io ob li obliviate) #:use-module (crates-io))

(define-public crate-obliviate-0.1.0 (c (n "obliviate") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "humansize") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "0zpgscz3x3hbdmh19csih1x9sj94rdda24j9dw7yavmmd8wwdnd8")))

(define-public crate-obliviate-0.1.1 (c (n "obliviate") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "humansize") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "18jhk7fimaqffw4ykrn1vyzflpfbl9hwc6y7q21xxx966zlxqdyc")))

(define-public crate-obliviate-0.1.2 (c (n "obliviate") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "humansize") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1zq6wnc9s0ickvksycdz45rgxa9hyri0sm1h146bxblfnfhfgxf4")))

(define-public crate-obliviate-0.1.3 (c (n "obliviate") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "humansize") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "0v73rb8xl1pwgvx88sqcna50ii1jibkxx58hv80mqf2g436937qv")))

