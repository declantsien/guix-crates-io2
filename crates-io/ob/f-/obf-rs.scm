(define-module (crates-io ob f- obf-rs) #:use-module (crates-io))

(define-public crate-obf-rs-0.1.0 (c (n "obf-rs") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.9.4") (d #t) (k 0)))) (h "0jd7bmfhwlcwlw74syzrqc87l64mhz8icr9fvzadnw0d077887s1")))

(define-public crate-obf-rs-0.1.1 (c (n "obf-rs") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.9.4") (d #t) (k 0)))) (h "14lbfz4qkql5y72w802pvh2airby13rjlmqm1cq8dcprryqmg5lk")))

