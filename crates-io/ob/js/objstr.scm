(define-module (crates-io ob js objstr) #:use-module (crates-io))

(define-public crate-objstr-0.1.0 (c (n "objstr") (v "0.1.0") (h "05cda696a30xklg48ss0vmq9p80nrsz4srnbqp7bxqf17l8hyayd")))

(define-public crate-objstr-0.1.1 (c (n "objstr") (v "0.1.1") (h "0f12gi0qgi8i0mmmmmz4zhkrmfmrdadaq1ypgf0pffk9rm6xrw56")))

