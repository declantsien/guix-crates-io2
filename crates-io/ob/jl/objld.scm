(define-module (crates-io ob jl objld) #:use-module (crates-io))

(define-public crate-objld-0.2.0 (c (n "objld") (v "0.2.0") (d (list (d (n "nom") (r "7.1.*") (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "1r4c0z6zj6kk56snihwpnkkfyyxy3acr00rqc060dqbsskwpf5ip")))

(define-public crate-objld-0.3.0 (c (n "objld") (v "0.3.0") (d (list (d (n "nom") (r "7.1.*") (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "1s9k4n7xx0fa0czzmvdm6m6bzafcjpjmwkah75b70b4f7hmjfga6")))

(define-public crate-objld-0.4.0 (c (n "objld") (v "0.4.0") (d (list (d (n "nom") (r "7.1.*") (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "1589bq7dipinjdglk33kwy3la9m9q3li5hpfkvc55fc0nal79gbb")))

(define-public crate-objld-0.5.0 (c (n "objld") (v "0.5.0") (d (list (d (n "nom") (r "7.1.*") (d #t) (k 0)) (d (n "rayon") (r "1.5.*") (d #t) (k 0)))) (h "0xwv9p417xjwzb2200795x4hf0xalnmsk15rxkbyb2pvchylbwwg")))

