(define-module (crates-io bg pq bgpq3) #:use-module (crates-io))

(define-public crate-bgpq3-0.1.0-alpha.1 (c (n "bgpq3") (v "0.1.0-alpha.1") (d (list (d (n "ipnetwork") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c2hhhxj7d66b26qmp5415hg157360z9gvpfkxpmc9n6n5pkr0bl")))

(define-public crate-bgpq3-0.1.0-alpha.2 (c (n "bgpq3") (v "0.1.0-alpha.2") (d (list (d (n "ipnetwork") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("process"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1635961v2vv8rq8p0im3g3biq50fq1z4zlyz0pdbbbqb90l35q5j")))

(define-public crate-bgpq3-0.1.0-alpha.3 (c (n "bgpq3") (v "0.1.0-alpha.3") (d (list (d (n "ipnetwork") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("process"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1mq6y4i20qd5vy8ajbyshw16x2r3sh4943gf6265xgb78wsnsins")))

(define-public crate-bgpq3-0.1.0 (c (n "bgpq3") (v "0.1.0") (d (list (d (n "ipnetwork") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("process"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.3.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0jfrbqg44rbygqh7kvcy1pv1dp8qknrz1lpn7vaipcs8dd5w906r")))

