(define-module (crates-io bg pv bgpview-cli) #:use-module (crates-io))

(define-public crate-bgpview-cli-0.1.0 (c (n "bgpview-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "httparse") (r "^1") (d #t) (k 0)) (d (n "maplit") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "percent-encoding") (r "^2") (d #t) (k 0)) (d (n "rustls") (r "^0.18") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "webpki") (r "^0.21") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.20") (d #t) (k 0)))) (h "027v5z040qhdka7vag20szbyb4cnzjgcvq745x2nxav646igaz2n")))

