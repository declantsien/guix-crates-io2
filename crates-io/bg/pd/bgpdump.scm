(define-module (crates-io bg pd bgpdump) #:use-module (crates-io))

(define-public crate-bgpdump-0.0.0 (c (n "bgpdump") (v "0.0.0") (d (list (d (n "byteorder") (r "^1.2.7") (f (quote ("i128"))) (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 2)))) (h "14485r3wac1ljjg2d1xy31ig9kahkcqbl2apgany7x9kn8m0vmr6")))

