(define-module (crates-io bg pf bgpfu-lib) #:use-module (crates-io))

(define-public crate-bgpfu-lib-0.1.0-alpha.2 (c (n "bgpfu-lib") (v "0.1.0-alpha.2") (d (list (d (n "generic-ip") (r "^0.1.0-rc.2") (d #t) (k 0)) (d (n "irrc") (r "^0.1.0-rc.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rpsl") (r "^0.1.0-rc.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1x5kcl1bbw1avwj1wpi4n6mv6pgqp77qlkjgkinxnm9wli4vvg73")))

(define-public crate-bgpfu-lib-0.1.0-rc.1 (c (n "bgpfu-lib") (v "0.1.0-rc.1") (d (list (d (n "generic-ip") (r "^0.1.1") (d #t) (k 0)) (d (n "irrc") (r "^0.1") (d #t) (k 0)) (d (n "rpsl") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "17yw5h8hy6xgv74bc9gac3bkrmgkjmf4sr7dgqfnvq72cy58g8cf") (r "1.75")))

(define-public crate-bgpfu-lib-0.1.0-rc.2 (c (n "bgpfu-lib") (v "0.1.0-rc.2") (d (list (d (n "generic-ip") (r "^0.1.1") (d #t) (k 0)) (d (n "irrc") (r "^0.1") (d #t) (k 0)) (d (n "rpsl") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1nx9wqrckwrk12cadv2iaw58wsf72h9pj4f2gxzjrrhq3h5p24k5") (r "1.75")))

(define-public crate-bgpfu-lib-0.1.0 (c (n "bgpfu-lib") (v "0.1.0") (d (list (d (n "generic-ip") (r "^0.1.1") (d #t) (k 0)) (d (n "irrc") (r "^0.1") (d #t) (k 0)) (d (n "rpsl") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0vjyk0vrdx0a9chn4a2vk49sqpgvh7nzch8x6kf7xq1k5g5044j1") (r "1.75")))

