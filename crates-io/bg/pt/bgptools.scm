(define-module (crates-io bg pt bgptools) #:use-module (crates-io))

(define-public crate-bgptools-0.0.1 (c (n "bgptools") (v "0.0.1") (h "0fh9c3yxggxqyd0fclbh3qwpczll0337liwx1w9bsgivkhhav04s")))

(define-public crate-bgptools-0.0.2 (c (n "bgptools") (v "0.0.2") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "162rw2alhirwfw5ql2r4q7h186jykr303vrz094w40lnw2513zz8")))

(define-public crate-bgptools-0.0.3 (c (n "bgptools") (v "0.0.3") (d (list (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0vdsb88dnvlmndsn1m0sj6m8cdmlnzi46in03zql2hyhj52yj6qj")))

(define-public crate-bgptools-0.1.0 (c (n "bgptools") (v "0.1.0") (d (list (d (n "mrt") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1p1g2bgz0fgr738chba26ivx45scnpi8nj5vx7m3q10dpkx8k3bp")))

(define-public crate-bgptools-0.1.1 (c (n "bgptools") (v "0.1.1") (d (list (d (n "mrt") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "17zxaqsxvgpmgnq2l08rdrnn39s98mar57sfcyrklzgwm20pxsbb")))

