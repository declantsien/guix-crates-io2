(define-module (crates-io bg zf bgzf) #:use-module (crates-io))

(define-public crate-bgzf-0.2.0 (c (n "bgzf") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "libdeflater") (r "^0.7.5") (d #t) (k 0)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "16j8anmfnk1bbwimwcggdq11qaynmc6g9zb9kjb95b0hnnnlj23r")))

