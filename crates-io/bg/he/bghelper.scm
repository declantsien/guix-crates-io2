(define-module (crates-io bg he bghelper) #:use-module (crates-io))

(define-public crate-bghelper-2.0.0 (c (n "bghelper") (v "2.0.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)))) (h "0iavgyhcwy394n9hc01077gmlgkx40jklmy6my8lf6bhmxqv9bhl")))

