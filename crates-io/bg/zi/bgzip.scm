(define-module (crates-io bg zi bgzip) #:use-module (crates-io))

(define-public crate-bgzip-0.0.1 (c (n "bgzip") (v "0.0.1") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "11cbr4nv4q4k29bp3v7ay61r5ymjkbwxy9gbqsjd2bd8r8z1bfnc")))

(define-public crate-bgzip-0.0.2 (c (n "bgzip") (v "0.0.2") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1xbcrac50mw0hjv01gj6l59i1mvdighfbnrlc3f1sc13c47bbph9")))

(define-public crate-bgzip-0.0.3 (c (n "bgzip") (v "0.0.3") (d (list (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "1jdl3a9xbhmysn4qcvghzy1g9v25wivbpv6mxsys75blwxq8birj")))

(define-public crate-bgzip-0.1.0 (c (n "bgzip") (v "0.1.0") (d (list (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 0)))) (h "0kr36b2rhhs6awzjym230zadlsxxaww183zx9q66sfp7kwz13zxd")))

(define-public crate-bgzip-0.2.0 (c (n "bgzip") (v "0.2.0") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1n3divw5xf3hmzxrzsr6l1qxivcyvdmbpxvli3lks3l94bgm7r4s")))

(define-public crate-bgzip-0.2.1 (c (n "bgzip") (v "0.2.1") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kssq4hp8csg27rhggabpfiyn9xp5rh5b8al63dghk11vqs7hk5j")))

(define-public crate-bgzip-0.2.2 (c (n "bgzip") (v "0.2.2") (d (list (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1izsmvhmd6fcybkygy3zpm7vp0lnxfrf2mjvyyyrplg9334f93ml")))

(define-public crate-bgzip-0.3.0 (c (n "bgzip") (v "0.3.0") (d (list (d (n "flate2") (r "^1") (o #t) (k 0)) (d (n "libdeflater") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "0w10jhwb9asxx9ba3zixkb65z2m8ycwwkw7xzxy5ya6c2nrv0ifd") (f (quote (("zlib-ng-compat" "flate2/zlib-ng-compat" "flate2") ("zlib-ng" "flate2/zlib-ng" "flate2") ("zlib" "flate2/zlib" "flate2") ("rust_backend" "flate2/rust_backend" "flate2") ("default" "rust_backend" "log" "rayon") ("cloudflare_zlib" "flate2/cloudflare_zlib" "flate2")))) (s 2) (e (quote (("rayon" "dep:rayon") ("log" "dep:log") ("libdeflater" "dep:libdeflater") ("flate2" "dep:flate2"))))))

(define-public crate-bgzip-0.3.1 (c (n "bgzip") (v "0.3.1") (d (list (d (n "flate2") (r "^1") (o #t) (k 0)) (d (n "libdeflater") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "rayon") (r "^1.6.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive" "wrap_help"))) (d #t) (k 2)) (d (n "csv") (r "^1") (d #t) (k 2)) (d (n "flate2") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_pcg") (r "^0.3.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)))) (h "16zr2nclis3kgz0jxi7ayyk510ar5dvyfpf03fazajmn1ycdhkxn") (f (quote (("zlib-ng-compat" "flate2/zlib-ng-compat" "flate2") ("zlib-ng" "flate2/zlib-ng" "flate2") ("zlib" "flate2/zlib" "flate2") ("rust_backend" "flate2/rust_backend" "flate2") ("default" "rust_backend" "log" "rayon") ("cloudflare_zlib" "flate2/cloudflare_zlib" "flate2")))) (s 2) (e (quote (("rayon" "dep:rayon") ("log" "dep:log") ("libdeflater" "dep:libdeflater") ("flate2" "dep:flate2"))))))

