(define-module (crates-io bg p- bgp-models) #:use-module (crates-io))

(define-public crate-bgp-models-0.1.0 (c (n "bgp-models") (v "0.1.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "06gmshpp4bmmfhs5143gysz5kyk1swm3b6m7ifmr2cc8b11yasrp")))

(define-public crate-bgp-models-0.1.1 (c (n "bgp-models") (v "0.1.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "122bh88x0bchlsmnmm1gxcxa1cc7kp55bqx8qxfz8735dki86shm")))

(define-public crate-bgp-models-0.1.2 (c (n "bgp-models") (v "0.1.2") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "02h1sw2j6vf35l8k4vn186qp0bpnrad7akbaq097sqfm2n0qaif6")))

(define-public crate-bgp-models-0.1.3 (c (n "bgp-models") (v "0.1.3") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "04w1y94fg2023rpsfpk36njhdd4qrfkl2f1i2z8p72nazsdnm9y7")))

(define-public crate-bgp-models-0.1.4 (c (n "bgp-models") (v "0.1.4") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "02xzw8rwqpa9r4s4gliwdm6i4nkanfybq9fla0rpxp68az2zgm23")))

(define-public crate-bgp-models-0.1.5 (c (n "bgp-models") (v "0.1.5") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0g763n66hk9bqpl9didr9j190wpr2zz8fadh410h7p72qjjjnx4s")))

(define-public crate-bgp-models-0.1.6 (c (n "bgp-models") (v "0.1.6") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1znq9n3bkb1ynvmbnzagsv6lxqx1h20pkvv4zzz36dcarl4j5mkx")))

(define-public crate-bgp-models-0.2.0 (c (n "bgp-models") (v "0.2.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1vsann7l7qwb3gr9xs89rqkmpbbf47nf2bngfb2wgdx3lmbw9937")))

(define-public crate-bgp-models-0.2.1 (c (n "bgp-models") (v "0.2.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1kdfcwz0zqhhbqnriwcmr14y2g6804avi4nwzza7y86gkx46zh1y")))

(define-public crate-bgp-models-0.2.2 (c (n "bgp-models") (v "0.2.2") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0735ll4whasm4h3rxvdlspd6af2649g2bcfpsjfxa6l0m3y4p7g0")))

(define-public crate-bgp-models-0.2.3 (c (n "bgp-models") (v "0.2.3") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0lwjpvlamixvjnirn9czm8616ymfy2hr5y7a2bgd6ia1pi2fcf8p")))

(define-public crate-bgp-models-0.3.0 (c (n "bgp-models") (v "0.3.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1j62aznl8bxmcmjqwsy2bmwq3r9r0ic4z0bbvnl6bl6dbdmixjmn")))

(define-public crate-bgp-models-0.3.1 (c (n "bgp-models") (v "0.3.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "02bpiyr4i19dw5lhgbr109rmdbhfdccji3lynfc5mxg4aa5h6hfi")))

(define-public crate-bgp-models-0.3.2 (c (n "bgp-models") (v "0.3.2") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1hky52v7f9ap07q7rvw768i374ryylk4sislc1p5djnd2bj03df9")))

(define-public crate-bgp-models-0.3.3 (c (n "bgp-models") (v "0.3.3") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0n9kc731h7f0rxrnkv9aqa04z7br6w9b9k42kc2w7a8ybfhchvha")))

(define-public crate-bgp-models-0.3.4 (c (n "bgp-models") (v "0.3.4") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "18xd6x0l06pjh8rjgcv61rab6v1s067l75p8da78birs1wilc2l8")))

(define-public crate-bgp-models-0.3.5 (c (n "bgp-models") (v "0.3.5") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1ahg64w6x0sbccxba1y2d37g4yisyv28lbjwaq3l3rm5lvw12ahw")))

(define-public crate-bgp-models-0.4.0 (c (n "bgp-models") (v "0.4.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0i04sybi9bw8f1l9xlwwycsg851xfay6fxyjdcaicn1rz1a8v5mh")))

(define-public crate-bgp-models-0.4.1 (c (n "bgp-models") (v "0.4.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "07r8wqba1ipg07ma3438yrii2a37ll0q07ni342sfr1h9ba7wpaz")))

(define-public crate-bgp-models-0.5.0-rc.1 (c (n "bgp-models") (v "0.5.0-rc.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vxjhw0p7fjpcaaw1lrr5bw5p2rn60na6i40k7m6qz2jx8nhk5m7")))

(define-public crate-bgp-models-0.5.0-rc.2 (c (n "bgp-models") (v "0.5.0-rc.2") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mic3qi7hs3han4hdqhhw8ycs4lmf56vyflr9zngcbyvhwnxmwqy")))

(define-public crate-bgp-models-0.5.0 (c (n "bgp-models") (v "0.5.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "02rjsmjf19945l7lnr1y087vgp6yjj54p5bsl375cn5f6sb16jg9")))

(define-public crate-bgp-models-0.6.0-rc.1 (c (n "bgp-models") (v "0.6.0-rc.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zh98njdcrwcknva74awcqgbxkxpyg5bq8qcxjvchfzxp4rfv70p")))

(define-public crate-bgp-models-0.6.0-rc.2 (c (n "bgp-models") (v "0.6.0-rc.2") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kb5h4a3dinrd7cigi8j0iz6g1m8gqia8qmqn4gqr4k68yk9135j")))

(define-public crate-bgp-models-0.6.0-rc.3 (c (n "bgp-models") (v "0.6.0-rc.3") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bhyhrww5mqy4pwrpaxia9ma919gmzs612cavx2l70rzg6m5vph7")))

(define-public crate-bgp-models-0.6.0 (c (n "bgp-models") (v "0.6.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "092wq2jsazl506pnkwaazv02wzf7hh8mj0gksh2d5cgnz5rjccvk")))

(define-public crate-bgp-models-0.6.1 (c (n "bgp-models") (v "0.6.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ppz1di45820wpmlg8qwjxkp0zzpj8m21sizi8zjpfzzs6gzz49k")))

(define-public crate-bgp-models-0.6.2 (c (n "bgp-models") (v "0.6.2") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1z1h11j1dmj7xsd0dmsm86mp0nv8g5j3h3rw19g6c6330q8rwfzz")))

(define-public crate-bgp-models-0.6.3 (c (n "bgp-models") (v "0.6.3") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vyxhxiw07gbd716c81zqr52jm8hpi9kr59glylpbsfs71x5zy8s")))

(define-public crate-bgp-models-0.7.0-alpha.1 (c (n "bgp-models") (v "0.7.0-alpha.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "14wgv1vmp6z55hyia3c0g4bvfl7s41rz0wa8yck037ya3i1gxx8z")))

(define-public crate-bgp-models-0.7.0-alpha.2 (c (n "bgp-models") (v "0.7.0-alpha.2") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "08pp6jirc1c23nsm99kvp5ilgigw0k0n1w35j9d66p5hyhncchj2")))

(define-public crate-bgp-models-0.7.0-alpha.3 (c (n "bgp-models") (v "0.7.0-alpha.3") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zjy1q89r29gnzn302vvm2c0lw4vl239wyfgka8if51pfm10107k")))

(define-public crate-bgp-models-0.7.0-alpha.4 (c (n "bgp-models") (v "0.7.0-alpha.4") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rh2bs789lvgalgivxwr4g644mkagqrnnrya0h4gbmbq3vwg6wa5")))

(define-public crate-bgp-models-0.7.0 (c (n "bgp-models") (v "0.7.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l3spkfar08jxw4sbnij3ji9ybr5qp2npmgw4vnb7rh41c54pcdd")))

(define-public crate-bgp-models-0.7.1 (c (n "bgp-models") (v "0.7.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "090wy17mwjv818qmqrbjj84skgv404yak6vhsjf46f33d71zy4db") (y #t)))

(define-public crate-bgp-models-0.8.0 (c (n "bgp-models") (v "0.8.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnetwork") (r "^0.18") (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "18x3kifp62v8bwxqd7rgm15mp2mai2kvznfl3r63g7vi2jlr8l2l")))

(define-public crate-bgp-models-0.9.0-alpha-1 (c (n "bgp-models") (v "0.9.0-alpha-1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0mmmn4nkfcqaiynxq3kgsgs1zwkhfzxqvp1s1jsm6d52jbrrgkwj")))

(define-public crate-bgp-models-0.9.0 (c (n "bgp-models") (v "0.9.0") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "042ggh3sfkzjn9xkm1071m9sqvr81wva6zbj2jsqsafwj22k463f")))

(define-public crate-bgp-models-0.9.1 (c (n "bgp-models") (v "0.9.1") (d (list (d (n "enum-primitive-derive") (r "^0.2") (d #t) (k 0)) (d (n "ipnet") (r "^2.7") (d #t) (k 0)) (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1md07w4rz226avl3vxsln6xdpsagw8r17g8s3xwqgzsxxg2lzzi7")))

