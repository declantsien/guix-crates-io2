(define-module (crates-io bg p- bgp-rs) #:use-module (crates-io))

(define-public crate-bgp-rs-0.1.0 (c (n "bgp-rs") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "mrt-rs") (r "^0.3") (d #t) (k 2)))) (h "027s3dw1z4g4kz2gjrkvz878dg0gbyhzbpqgx24qnmw9mrbyxqdh")))

(define-public crate-bgp-rs-0.2.0 (c (n "bgp-rs") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "mrt-rs") (r "^1.1.0") (d #t) (k 2)))) (h "1k0vcf2dyllw9v8ycfl519pqn0xbjnrz615pn3d3sr3cdw6xa5b3")))

(define-public crate-bgp-rs-0.3.0 (c (n "bgp-rs") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.3.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "mrt-rs") (r "^1.1.0") (d #t) (k 2)))) (h "04jsxf928pdimng41d75lgcw0giwlyzaj2xkiqr30mjyv57v8pnp")))

(define-public crate-bgp-rs-0.4.0 (c (n "bgp-rs") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "mrt-rs") (r "^1.1.0") (d #t) (k 2)))) (h "0ny5kc5il8n8q4bqm1pbljy21px8y08rc275zqz4sl5ilvd9nqi7")))

(define-public crate-bgp-rs-0.4.1 (c (n "bgp-rs") (v "0.4.1") (d (list (d (n "byteorder") (r "^1.3.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "mrt-rs") (r "^1.1.0") (d #t) (k 2)))) (h "0qr2kwvhqqc56nv6cgf4mfwjjw4n04vppcizn7m0jl09lv6p6vsf")))

(define-public crate-bgp-rs-0.5.0 (c (n "bgp-rs") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.3.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "etherparse") (r "^0.8.0") (d #t) (k 2)) (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "mrt-rs") (r "^1.1.0") (d #t) (k 2)) (d (n "pcap-file") (r "^0.10.0") (d #t) (k 2)))) (h "0m937whdknl9kxzl9dfkc3rj14r0yjg3brifqjdxd00bfh5csf07")))

(define-public crate-bgp-rs-0.6.0 (c (n "bgp-rs") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2") (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.3.1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "etherparse") (r "^0.9.0") (d #t) (k 2)) (d (n "libflate") (r "^0.1") (d #t) (k 2)) (d (n "mrt-rs") (r "^2.0.0") (d #t) (k 2)) (d (n "pcap-file") (r "^1.1") (d #t) (k 2)) (d (n "twoway") (r "^0.2.0") (d #t) (k 2)))) (h "1dhvzxa8lp1pg5dwwr2rsw6gadlwpkqs4iw3cwz234x8b92rs83c") (f (quote (("flowspec" "bitflags") ("default"))))))

