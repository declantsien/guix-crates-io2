(define-module (crates-io bg ps bgpstream) #:use-module (crates-io))

(define-public crate-bgpstream-0.1.1 (c (n "bgpstream") (v "0.1.1") (d (list (d (n "bgpstream-sys") (r "^0.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1xx2vvnmqnmzsqlfznn9ww6cbab6pyzsdqibsp9pznfig7r9gz3j")))

(define-public crate-bgpstream-0.1.2 (c (n "bgpstream") (v "0.1.2") (d (list (d (n "bgpstream-sys") (r "^0.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19wk88ddzfyy8q0j7pmh2a954ypk4x51jwrqyf1w2366jlalgi7f")))

(define-public crate-bgpstream-0.2.0 (c (n "bgpstream") (v "0.2.0") (d (list (d (n "bgpstream-sys") (r "^0.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19lam75j95rmrikh9aq4nlvm1dl55sfll7y7ndpk1pz62yrhi8l6")))

(define-public crate-bgpstream-0.2.1 (c (n "bgpstream") (v "0.2.1") (d (list (d (n "bgpstream-sys") (r "^0.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hp17145672kxly3xvxk01g4h149vwwqf11y28alivab893dswil")))

(define-public crate-bgpstream-0.2.2 (c (n "bgpstream") (v "0.2.2") (d (list (d (n "bgpstream-sys") (r "^0.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1rq8q5v1m1m4d5fymvp98663zzaw4xbgaxn8hpxcbfq94q4mc3sz")))

