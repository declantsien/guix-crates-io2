(define-module (crates-io bg ps bgpsim-macros) #:use-module (crates-io))

(define-public crate-bgpsim-macros-0.11.0 (c (n "bgpsim-macros") (v "0.11.0") (d (list (d (n "ipnet") (r "^2.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.49") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0lg8h04dpj97yll0jx7il1pdlyf8565jvm62ng5xfnprkk16vf7i")))

