(define-module (crates-io bg fx bgfx-sys) #:use-module (crates-io))

(define-public crate-bgfx-sys-0.1.0 (c (n "bgfx-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1ynlkj146zhivjfxzn1ijwgffykyiyral4k2nrzc8hk4c44vg557") (f (quote (("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.2.0 (c (n "bgfx-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0ip817i84j6hqj8gvl1b2i3bhpv2y7m29776v84rygf1zc842qrr") (f (quote (("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.3.0 (c (n "bgfx-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0b7qayc6i3zrkyi1m7kf95kc3ivd7wpbxl64ggfz6qxcr7yk7691") (f (quote (("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.4.0 (c (n "bgfx-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0dqd4jii7wprq3v2hqj69zvpbsz1s55h6bv6c68yzaxynlpy9acf") (f (quote (("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.5.0 (c (n "bgfx-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1vr1shvmdc5kxc5aqi2dzfsy63hvjfl6n5jc46fy7gjhn1y99lmv") (f (quote (("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.6.0 (c (n "bgfx-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1ijyjbbfmj8q9nk4q8irbyz0f4p3y9bd8khxhdw6rbyqyp8hs6k4") (f (quote (("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.7.0 (c (n "bgfx-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1x3g0mv2b86lqffls1x59hl90fg7dzglznnf4lia4ywaygn97kp0") (f (quote (("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.8.0 (c (n "bgfx-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1k5lvv1rczbh1nbqpp33hw295bdh014bac7r1s1a1pirrs4ffs2a") (f (quote (("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.9.0 (c (n "bgfx-sys") (v "0.9.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0xd7z37m4zcfsq7fp3m1vfjlpah159y7k3lf5v1shd3dw36hx3k4") (f (quote (("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.10.0 (c (n "bgfx-sys") (v "0.10.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0x2z8gxlyv69ns82q2rjnhp99g8gaa770yxrq75zgbimfl7j755b") (f (quote (("bgfx-single-threaded") ("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.11.0 (c (n "bgfx-sys") (v "0.11.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "14r08qj65scrkwkn5xijnsj7yb8cpzs52k6ra9pfrniwm3wg4k5r") (f (quote (("bgfx-single-threaded") ("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.12.0 (c (n "bgfx-sys") (v "0.12.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0705whmqxsfs9j46ishqjfa1kmlyi7yj1ksj3n9f5m5zpx6hn5iy") (f (quote (("bgfx-single-threaded") ("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.13.0 (c (n "bgfx-sys") (v "0.13.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "16vzswg80pd06xwl3aqwwrpy0pikvpdq7qr23flz85h8pylaz0j0") (f (quote (("bgfx-single-threaded") ("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.14.0 (c (n "bgfx-sys") (v "0.14.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "19nfbbvvsap0mv2xwn1n623xv5pajjv27397gg5s65fw6acayhcr") (f (quote (("bgfx-single-threaded") ("bgfx-debug"))))))

(define-public crate-bgfx-sys-0.15.0 (c (n "bgfx-sys") (v "0.15.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0gwcs6w8plc4n7nxm2xrdqlcx75axvxp2cki6z1ajllr189pkd9k") (f (quote (("bgfx-single-threaded") ("bgfx-debug"))))))

