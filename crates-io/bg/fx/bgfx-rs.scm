(define-module (crates-io bg fx bgfx-rs) #:use-module (crates-io))

(define-public crate-bgfx-rs-0.1.0 (c (n "bgfx-rs") (v "0.1.0") (d (list (d (n "bgfx-sys") (r "^0.1") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0rm186f9d7wlwlfipxg300d7qr7fcr1a7n9yk76rd7dcd4v6ibds")))

(define-public crate-bgfx-rs-0.2.0 (c (n "bgfx-rs") (v "0.2.0") (d (list (d (n "bgfx-sys") (r "^0.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1kpyyryx81ax950jypilgaic1mpj68csccz8va52r861mkjqj70r")))

(define-public crate-bgfx-rs-0.3.0 (c (n "bgfx-rs") (v "0.3.0") (d (list (d (n "bgfx-sys") (r "^0.2") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0x257f9qsk5xb1fwhgfmpixiqjg8rjyxdj5ndlzw345q6dh5pri8")))

(define-public crate-bgfx-rs-0.4.0 (c (n "bgfx-rs") (v "0.4.0") (d (list (d (n "bgfx-sys") (r "^0.3") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1l5fnlkgc5n06s1dbpydyivqblxxvi63c7p6c0as4406wwhc87w8")))

(define-public crate-bgfx-rs-0.5.0 (c (n "bgfx-rs") (v "0.5.0") (d (list (d (n "bgfx-sys") (r "^0.3") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1vnd92dyjbi7zbycz47l5n4nhnsnz9csybp9jbiwk5lbqy6ffjdf")))

(define-public crate-bgfx-rs-0.6.0 (c (n "bgfx-rs") (v "0.6.0") (d (list (d (n "bgfx-sys") (r "^0.4") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0ia8hbaqlpii73nlkdli47ilr91wn81w4rikcgx1cxsfbrm8ybbs")))

(define-public crate-bgfx-rs-0.7.0 (c (n "bgfx-rs") (v "0.7.0") (d (list (d (n "bgfx-sys") (r "^0.5") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1zc14bfpd2q7ilj2lih458zhpwp189jqw17b82zdnwz61cshrmwr")))

(define-public crate-bgfx-rs-0.8.0 (c (n "bgfx-rs") (v "0.8.0") (d (list (d (n "bgfx-sys") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1bkf9qzm405pnq27as1kfasa8c7gsr02h6jaba8zq1mxcfscqh1y")))

(define-public crate-bgfx-rs-0.9.0 (c (n "bgfx-rs") (v "0.9.0") (d (list (d (n "bgfx-sys") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "18vs25qsxrapq016ndw7vgbzqfarj07a4ljixwjdiyng26f4qa1n")))

(define-public crate-bgfx-rs-0.10.0 (c (n "bgfx-rs") (v "0.10.0") (d (list (d (n "bgfx-sys") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0a7xx22faf162h7cvhcmdwxr7n3jqj5mys3r95ydksf1w6555nka")))

(define-public crate-bgfx-rs-0.11.0 (c (n "bgfx-rs") (v "0.11.0") (d (list (d (n "bgfx-sys") (r "^0.7") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1lhj86swidb5fw8w9kdbqnyw50vasbi0zj0zr62k4g0r2mbbiq08")))

(define-public crate-bgfx-rs-0.12.0 (c (n "bgfx-rs") (v "0.12.0") (d (list (d (n "bgfx-sys") (r "^0.8") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "14p4crcxlzpjf9sx8amsf2mgk15sn4ya37xgf2rq7bvih7gi8g54")))

(define-public crate-bgfx-rs-0.13.0 (c (n "bgfx-rs") (v "0.13.0") (d (list (d (n "bgfx-sys") (r "^0.9") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0zn8bsf4qvna962lx0cpvdcimjhbfhjyfhrh6hnplinfww7by1mk")))

(define-public crate-bgfx-rs-0.14.0 (c (n "bgfx-rs") (v "0.14.0") (d (list (d (n "bgfx-sys") (r "^0.9") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "0xvfb5scg74vaxz775jx7di5sq21zymisswj40dk1sdhjcjd3ava")))

(define-public crate-bgfx-rs-0.15.0 (c (n "bgfx-rs") (v "0.15.0") (d (list (d (n "bgfx-sys") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "18bhfsb3qi3dh49c5pd5p1xxr6v2kp5m0fj2mdc0rg8k4l25lv24")))

(define-public crate-bgfx-rs-0.16.0 (c (n "bgfx-rs") (v "0.16.0") (d (list (d (n "bgfx-sys") (r "^0.11") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1cr6zlfi5knwzzxqipniqq76ba98q7vg9samdwmv3zhdjya8jfzr")))

(define-public crate-bgfx-rs-0.17.0 (c (n "bgfx-rs") (v "0.17.0") (d (list (d (n "bgfx-sys") (r "^0.11") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.41") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.3") (d #t) (k 2)))) (h "1px4g3pqdp1k5xjr7z0vd38y86spyvvxdsprmzfxkar93cp9bcj2")))

(define-public crate-bgfx-rs-0.18.0 (c (n "bgfx-rs") (v "0.18.0") (d (list (d (n "bgfx-sys") (r "^0.12") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.51") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 2)) (d (n "winit") (r "^0.28.1") (d #t) (k 2)))) (h "0q64jnl36069x2zwjgqvzcxyrnmf7ifwp4ygqxq9c54anqsp5yqq")))

(define-public crate-bgfx-rs-0.19.0 (c (n "bgfx-rs") (v "0.19.0") (d (list (d (n "bgfx-sys") (r "^0.13") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.51") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 2)) (d (n "winit") (r "^0.28.1") (d #t) (k 2)))) (h "1rbhcixklh389gzv2s1vzxf2d3sqk9waw9zlcwhjpaikmqk365m7")))

(define-public crate-bgfx-rs-0.20.0 (c (n "bgfx-rs") (v "0.20.0") (d (list (d (n "bgfx-sys") (r "^0.15") (d #t) (k 0)) (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cfixed-string") (r "^1.0") (d #t) (k 0)) (d (n "glam") (r "^0.15") (d #t) (k 2)) (d (n "glfw") (r "^0.51") (d #t) (k 2)) (d (n "raw-window-handle") (r "^0.5") (d #t) (k 2)) (d (n "winit") (r "^0.28.1") (d #t) (k 2)))) (h "13ds91c4dynhlam1w4a7d6vqn4ljfnqyp4ap15b4a5j1yg7qsrqz")))

