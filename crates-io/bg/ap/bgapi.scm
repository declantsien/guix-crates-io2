(define-module (crates-io bg ap bgapi) #:use-module (crates-io))

(define-public crate-bgapi-0.0.0 (c (n "bgapi") (v "0.0.0") (h "0npjzrv0vw6ppbqfq3qcywhbjynvs5jx29gzhc0lcz9143c41rg8")))

(define-public crate-bgapi-0.0.1 (c (n "bgapi") (v "0.0.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "10kciivq9b93wzks82ppss7m816ns7kx5c66vkjsd81pfmplsaap")))

(define-public crate-bgapi-0.0.2 (c (n "bgapi") (v "0.0.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)))) (h "11w6zgi3swfcpdb8dm4naw8wm9zxjirplbnlnbzg5qdjjni0rm2a")))

(define-public crate-bgapi-0.0.3 (c (n "bgapi") (v "0.0.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)))) (h "16y0k92a8d1y6x9iy6vmwai413lqws2rnqfamg0zx8j1fmwyvx89")))

(define-public crate-bgapi-0.0.4 (c (n "bgapi") (v "0.0.4") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)))) (h "10kc46k304giqpyrw8fzrjh873dz1y4h3h5977295qpxdps360wf")))

(define-public crate-bgapi-0.0.5 (c (n "bgapi") (v "0.0.5") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)))) (h "0clrp1ya7vg04cm3lkqgnixv30pi3ak6ri80glk2dy9b2gp9g2fz")))

(define-public crate-bgapi-0.0.6 (c (n "bgapi") (v "0.0.6") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)))) (h "0vnhd9rwc1jxm9w08nkrdnbmwydcaifg148l3bwlxb7g9vlrvnc7")))

(define-public crate-bgapi-0.0.7 (c (n "bgapi") (v "0.0.7") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)))) (h "00q6p21w7hcjx241f9j7xs4dqk1alrvs0v18vd3iw4fyfp5cnj70")))

(define-public crate-bgapi-0.0.8 (c (n "bgapi") (v "0.0.8") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)))) (h "0jr4j4lm6jaglg847px5449ywyq7ffi3wxsvbvibgn51vm6nbd51")))

(define-public crate-bgapi-0.0.9 (c (n "bgapi") (v "0.0.9") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)))) (h "0a8h7xpf7nld4c2kqll93mrq02sw9z0kvqqxz4wra77wksjgjqxp")))

(define-public crate-bgapi-0.0.10 (c (n "bgapi") (v "0.0.10") (d (list (d (n "bytes") (r "^0.4.12") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "spmc") (r "^0.2.2") (d #t) (k 0)))) (h "02mx7z32l2pi4f257i3v2xrjxnih9g96fgb4kn5dq7qn8ldlpv43")))

