(define-module (crates-io bg mr bgmrank-cli) #:use-module (crates-io))

(define-public crate-bgmrank-cli-0.1.0 (c (n "bgmrank-cli") (v "0.1.0") (d (list (d (n "enum-set") (r "^0.0.6") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "libbgmrank") (r "^0.0.1") (d #t) (k 0)))) (h "1i8i6g5q5zddmb48xr8yi0v2gkyv6lrbb9a9gjw6rbd40zrnm0sq") (y #t)))

