(define-module (crates-io bg jk bgjk) #:use-module (crates-io))

(define-public crate-bgjk-0.1.0 (c (n "bgjk") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "0rg4045bkgrzqggdrnkfm3z5a2p0m7pbi9cl4b5mkjjkaf6n7b67") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-bgjk-1.0.0 (c (n "bgjk") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "02wfrc5fhli4cnyvqm3si20xnmnl26a9pq09xlja1jgnpxqkgryw") (f (quote (("dev" "clippy") ("default"))))))

(define-public crate-bgjk-1.0.1 (c (n "bgjk") (v "1.0.1") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)))) (h "1jg78jjgvkfvcml004r1c7xzyd8i4qjlpqpa59vwd0p93lg308q4") (f (quote (("dev" "clippy") ("default"))))))

