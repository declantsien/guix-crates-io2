(define-module (crates-io jk l- jkl-lang) #:use-module (crates-io))

(define-public crate-jkl-lang-0.1.5 (c (n "jkl-lang") (v "0.1.5") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fylyb2k8wcp1lh6f2d7f542xbfmhlp1a3ripjr9wa81l74fpfnf")))

(define-public crate-jkl-lang-0.1.51 (c (n "jkl-lang") (v "0.1.51") (d (list (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "194vnz0vh9syggy9rxnas25dwclqhwzbmcx1frnizzg804rkn543")))

