(define-module (crates-io jk ce jkcenum_derive) #:use-module (crates-io))

(define-public crate-jkcenum_derive-0.2.0 (c (n "jkcenum_derive") (v "0.2.0") (d (list (d (n "virtue") (r "^0.0") (d #t) (k 0)))) (h "03f1fhxvb5y68kqknd3i7abn18b8lsw7pbj0plwqjy05a1dcba1a") (r "1.73")))

(define-public crate-jkcenum_derive-0.2.1 (c (n "jkcenum_derive") (v "0.2.1") (d (list (d (n "virtue") (r "^0.0") (d #t) (k 0)))) (h "0hk4f2xwxw7r2lyq8db487r4s9d7zkm3sy72yvl2zgcyllgkbpi6")))

