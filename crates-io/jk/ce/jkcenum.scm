(define-module (crates-io jk ce jkcenum) #:use-module (crates-io))

(define-public crate-jkcenum-0.2.0 (c (n "jkcenum") (v "0.2.0") (d (list (d (n "jkcenum_derive") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "jkcenum_derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kahjpgx13l31n1s13f4ybcqh9693d023kx3agscb5mb5221ka2f") (f (quote (("derive" "jkcenum_derive")))) (r "1.73")))

(define-public crate-jkcenum-0.2.1 (c (n "jkcenum") (v "0.2.1") (d (list (d (n "jkcenum_derive") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "jkcenum_derive") (r "^0.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1mzc8qb3xkkh431khg6ivh606dmfpznichbiqzi5xclmd67gm56a") (f (quote (("derive" "jkcenum_derive"))))))

