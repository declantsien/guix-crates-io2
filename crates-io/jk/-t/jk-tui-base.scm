(define-module (crates-io jk -t jk-tui-base) #:use-module (crates-io))

(define-public crate-jk-tui-base-0.0.1 (c (n "jk-tui-base") (v "0.0.1") (d (list (d (n "crossterm") (r "^0.26.1") (f (quote ("event-stream" "serde" "futures-core"))) (d #t) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0xxns7xqphb4dizc7fwmna4j018fzzfmrh77l2ld1xkd9vq1hihh")))

