(define-module (crates-io ci vs civs) #:use-module (crates-io))

(define-public crate-civs-0.1.1 (c (n "civs") (v "0.1.1") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bng6s8w5rk7f60y2pwy8cyq2x5amgmr71vv9vdj8nplra4np4ps") (f (quote (("debug"))))))

(define-public crate-civs-0.1.2 (c (n "civs") (v "0.1.2") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1k4gfx0biaa2alafmzh7w5man7sahr6w09kgng65813f58mwmhlg") (f (quote (("debug"))))))

