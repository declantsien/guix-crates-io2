(define-module (crates-io ci sa cisat) #:use-module (crates-io))

(define-public crate-cisat-0.1.0 (c (n "cisat") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "trussx") (r "^0.1.3") (d #t) (k 0)))) (h "14v55w5d83ni635i4d8x4jjslyp5cazs57zisp23qiqv9fjhxxhi")))

(define-public crate-cisat-0.1.1 (c (n "cisat") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "trussx") (r "^0.1.3") (d #t) (k 0)))) (h "0abcxkppj5w33807ilgl0rp9dy0f8r7w2c35hw5vdpc54r1354pv")))

(define-public crate-cisat-0.1.2 (c (n "cisat") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "trussx") (r "^0.1.3") (d #t) (k 0)))) (h "12qcd1823qlacw4dp2g5gvfw54q5vdgs4p4fj6cdrsg0jmwrn75b")))

(define-public crate-cisat-0.1.3 (c (n "cisat") (v "0.1.3") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "trussx") (r "^0.1.3") (d #t) (k 0)))) (h "1r9qqnyv0idaffi3y30a40jm9hx5v73indyhg5s54cn3gc4182aj")))

(define-public crate-cisat-0.1.4 (c (n "cisat") (v "0.1.4") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "trussx") (r "^0.1.3") (d #t) (k 0)))) (h "06ngy1mjr9lkdm5kddqpvd0vb8ji1lw0abjam9ibmq1ycjmwh7kp")))

(define-public crate-cisat-0.1.5 (c (n "cisat") (v "0.1.5") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "trussx") (r "^0.1.3") (d #t) (k 0)))) (h "1izg5q68ys76cfd9q13ng1zbajlcf2llflmmqx81r6pi686vms46")))

(define-public crate-cisat-0.1.6 (c (n "cisat") (v "0.1.6") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "trussx") (r "^0.1.3") (d #t) (k 0)))) (h "0d2xj9m0rlazgnkk71i6ns10880xihfmhf54rn6dr20dw6msdwby")))

(define-public crate-cisat-0.2.0 (c (n "cisat") (v "0.2.0") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "trussx") (r "^0.1.3") (d #t) (k 0)))) (h "09xsl4a275ff6x52l5a3b5fvj32kabfl0dw5b27bradf2wqc2khr")))

(define-public crate-cisat-0.2.1 (c (n "cisat") (v "0.2.1") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "strum") (r "^0.20") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "trussx") (r "^0.1.3") (d #t) (k 0)))) (h "1w5f3kd19fd1rji8mjbh5q7dcz9jjq0kxn4m4lk8j3r0fcflsynz")))

(define-public crate-cisat-0.2.2 (c (n "cisat") (v "0.2.2") (d (list (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)) (d (n "trussx") (r "^0.1.3") (d #t) (k 0)))) (h "0yzsyn65swkqvn8b5m9jyjkc7ixi0dxjvjbwzmw29ynffna8c2d4")))

