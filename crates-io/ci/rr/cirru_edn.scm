(define-module (crates-io ci rr cirru_edn) #:use-module (crates-io))

(define-public crate-cirru_edn-0.0.1 (c (n "cirru_edn") (v "0.0.1") (d (list (d (n "cirru_parser") (r "^0.0.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0m77n5fkl776m8vcca8vhn6iii81h3jw9am8wvkb6yh4c7rf591z")))

(define-public crate-cirru_edn-0.0.2 (c (n "cirru_edn") (v "0.0.2") (d (list (d (n "cirru_parser") (r "^0.0.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0nnvmzps2k65ra0zbpdzrav5mqr01bxk0mhs1gsgzxf4jinps3hg")))

(define-public crate-cirru_edn-0.0.3 (c (n "cirru_edn") (v "0.0.3") (d (list (d (n "cirru_parser") (r "^0.0.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1ra7wi9j6brk3dqv0b33jh69x4s9v29pwqs3ac23p9xjrqxxldvc")))

(define-public crate-cirru_edn-0.0.4 (c (n "cirru_edn") (v "0.0.4") (d (list (d (n "cirru_parser") (r "^0.0.8") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0kqrshx392bj8aia33hjg8pyflfzyw6j8c9389znlk7m6l023cis")))

(define-public crate-cirru_edn-0.0.5 (c (n "cirru_edn") (v "0.0.5") (d (list (d (n "cirru_parser") (r "^0.0.9") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1s6k3sqxgmm7n8v6sxbbvnavl19gc2r60r9as99pflas3w86lscg")))

(define-public crate-cirru_edn-0.1.0 (c (n "cirru_edn") (v "0.1.0") (d (list (d (n "cirru_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1mbpfwwm3qd83653vgjkwj3xi82agfxag3bilfg7046fpbpd7gzn")))

(define-public crate-cirru_edn-0.1.1 (c (n "cirru_edn") (v "0.1.1") (d (list (d (n "cirru_parser") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "03abm6ya53595a661prd156jb2dnmvir5rmv6l1g931aprf8wvgf")))

(define-public crate-cirru_edn-0.1.2 (c (n "cirru_edn") (v "0.1.2") (d (list (d (n "cirru_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1pl48a2wx52a4hjgyhzk50l4dk7hkd96kchc9svqa19sw37vgscd")))

(define-public crate-cirru_edn-0.1.3 (c (n "cirru_edn") (v "0.1.3") (d (list (d (n "cirru_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "19zp3hx86qb5085c8500f6hjb8q6yb7p0pkq1bzka9fs592ircp5")))

(define-public crate-cirru_edn-0.1.4 (c (n "cirru_edn") (v "0.1.4") (d (list (d (n "cirru_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1hqp8gwjzasbm4yy2jvz2s6z9ghj8ix0db9skcqwxk71k8q8vqi3")))

(define-public crate-cirru_edn-0.1.5 (c (n "cirru_edn") (v "0.1.5") (d (list (d (n "cirru_parser") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1pd77j53ms20ghbs2mhkbn4pqxp4z5s8dz0d1faa7q8ayprs8pvd")))

(define-public crate-cirru_edn-0.1.6 (c (n "cirru_edn") (v "0.1.6") (d (list (d (n "cirru_parser") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0qdk2h5z1ap544jr6fijhhk03spzl3333hcic3q1fwqc8bza306m")))

(define-public crate-cirru_edn-0.1.7 (c (n "cirru_edn") (v "0.1.7") (d (list (d (n "cirru_parser") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "16qswmdsh15i6d5lbqn4iqrkvyah5mc3mkjmry9nfyh3n04hhihg")))

(define-public crate-cirru_edn-0.1.8 (c (n "cirru_edn") (v "0.1.8") (d (list (d (n "cirru_parser") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1nymilakphrivb19w2pwd8lrip6dld201hii6018npbgn5diipzj")))

(define-public crate-cirru_edn-0.1.9 (c (n "cirru_edn") (v "0.1.9") (d (list (d (n "cirru_parser") (r "^0.1.6") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1hjssn8isrssrsrc6iswfj9pbwaj74d9c3pns8axsm9k7d4x1jhl")))

(define-public crate-cirru_edn-0.1.10 (c (n "cirru_edn") (v "0.1.10") (d (list (d (n "cirru_parser") (r "^0.1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0k1j3rmranj73zf08y6x629135xb8d8j7jj4x8ra6al2rygkgs4c")))

(define-public crate-cirru_edn-0.1.11 (c (n "cirru_edn") (v "0.1.11") (d (list (d (n "cirru_parser") (r "^0.1.8") (d #t) (k 0)))) (h "0pj0z50335k4x0rs6qvm0hd38kqaznzjd9lky5cvgyr7yrkd8csw")))

(define-public crate-cirru_edn-0.1.12 (c (n "cirru_edn") (v "0.1.12") (d (list (d (n "cirru_parser") (r "^0.1.8") (d #t) (k 0)))) (h "1bnsbgwszw827x1r6q0fgqvddrmcbmwf4vzfjym5gpclsyv56myy")))

(define-public crate-cirru_edn-0.2.0-a1 (c (n "cirru_edn") (v "0.2.0-a1") (d (list (d (n "cirru_parser") (r "^0.1.8") (d #t) (k 0)))) (h "0i7yfk919lk3ky22jgyccis9h46jl957f3wijwkjd68dhkpk1lrp")))

(define-public crate-cirru_edn-0.2.0 (c (n "cirru_edn") (v "0.2.0") (d (list (d (n "cirru_parser") (r "^0.1.8") (d #t) (k 0)))) (h "0nxlrahymbv01nj6zycq8hwxc8p1418pzpsmpd4d4qpqn5hsqz1c")))

(define-public crate-cirru_edn-0.2.1 (c (n "cirru_edn") (v "0.2.1") (d (list (d (n "cirru_parser") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.3.0") (d #t) (k 0)))) (h "1f4gxfqlhgf80hs82fvvqfrb52g27sykphn3ijs8si567dq6aja7")))

(define-public crate-cirru_edn-0.2.2 (c (n "cirru_edn") (v "0.2.2") (d (list (d (n "cirru_parser") (r "^0.1.8") (d #t) (k 0)) (d (n "hex") (r "^0.3.0") (d #t) (k 0)))) (h "0p9j9xsl5x37byd6jwmqji75w1c7nrg78q7vdcqn42m46jbrcw23")))

(define-public crate-cirru_edn-0.2.3 (c (n "cirru_edn") (v "0.2.3") (d (list (d (n "cirru_parser") (r "^0.1.10") (d #t) (k 0)) (d (n "hex") (r "^0.3.0") (d #t) (k 0)))) (h "1fbpj9161l2gdvmmybird1d0bv66q94mc2vy3sr2nm16wkgg76z3")))

(define-public crate-cirru_edn-0.2.4 (c (n "cirru_edn") (v "0.2.4") (d (list (d (n "cirru_parser") (r "^0.1.10") (d #t) (k 0)) (d (n "hex") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "041wvs22rjig50cff5afgxw4xnypfkci4qlpsm7i74fwv43gy29y")))

(define-public crate-cirru_edn-0.2.5 (c (n "cirru_edn") (v "0.2.5") (d (list (d (n "cirru_parser") (r "^0.1.10") (d #t) (k 0)) (d (n "hex") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0c849qiaar05njd0aw6ghsgy8ns6zf6wj3iik7qx7b3l6nmz8f97")))

(define-public crate-cirru_edn-0.2.6 (c (n "cirru_edn") (v "0.2.6") (d (list (d (n "cirru_parser") (r "^0.1.10") (d #t) (k 0)) (d (n "hex") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0d1dz4kabx20kg31342rcpjnmgfq3v1nw78p3012ibsr5mff8158")))

(define-public crate-cirru_edn-0.2.7 (c (n "cirru_edn") (v "0.2.7") (d (list (d (n "cirru_parser") (r "^0.1.12") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0gxcdkxkb59va4xmck6084kk5cs2f99n0wc0716bcsa5dswbwqz6")))

(define-public crate-cirru_edn-0.2.8 (c (n "cirru_edn") (v "0.2.8") (d (list (d (n "cirru_parser") (r "^0.1.12") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "04y6h9r79c7b0gr5cx1ppb9zjf5lglln6zg943w0899v8dvax228")))

(define-public crate-cirru_edn-0.2.9 (c (n "cirru_edn") (v "0.2.9") (d (list (d (n "cirru_parser") (r "^0.1.14") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0sjv7689qpmfva6xa3avmramz941zkqgf5spaan9k4d936lglpqz")))

(define-public crate-cirru_edn-0.2.10 (c (n "cirru_edn") (v "0.2.10") (d (list (d (n "cirru_parser") (r "^0.1.15") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0j4gla5mq7ijg86k0wphzc6a4vqs1vyv7kpq5j0y0hxs5cyxh7cd")))

(define-public crate-cirru_edn-0.2.11 (c (n "cirru_edn") (v "0.2.11") (d (list (d (n "cirru_parser") (r "^0.1.16") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0jjnwfngpcm3ywdkv6qns94cpr0cllsgkxqdiwg7kvn2ii7k7vm0")))

(define-public crate-cirru_edn-0.2.12 (c (n "cirru_edn") (v "0.2.12") (d (list (d (n "cirru_parser") (r "^0.1.17") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0vxabqiw7g9pyfa0l11gdgcnymfhnhvnylfz1daj2kglxcl2cbh1")))

(define-public crate-cirru_edn-0.2.13 (c (n "cirru_edn") (v "0.2.13") (d (list (d (n "cirru_parser") (r "^0.1.18") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "13wim5nr44ck3w1bwv0x3kyvbjpg8sl3cnyvamddn4089ala3aj9")))

(define-public crate-cirru_edn-0.2.14 (c (n "cirru_edn") (v "0.2.14") (d (list (d (n "cirru_parser") (r "^0.1.18") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0q3l3b6nw26iw8rzhjqshmk01dsb35z9h7fk614qgn1bpl97s8lj")))

(define-public crate-cirru_edn-0.2.15 (c (n "cirru_edn") (v "0.2.15") (d (list (d (n "cirru_parser") (r "^0.1.18") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1fc9fm68l9191q4mfbapks3gw0nzj069jcmbbaz7h2v6x6bvsxs8")))

(define-public crate-cirru_edn-0.2.16 (c (n "cirru_edn") (v "0.2.16") (d (list (d (n "cirru_parser") (r "^0.1.18") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "14kd51yv72spn14ppfyhcxjqkhvnlf96cgg4xxb6niikr7jbyv5j")))

(define-public crate-cirru_edn-0.2.17 (c (n "cirru_edn") (v "0.2.17") (d (list (d (n "cirru_parser") (r "^0.1.18") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ds2m0pjw63mg80i2m6m8pff3xjv01swpijxdx2g2ljjwpqy1sly")))

(define-public crate-cirru_edn-0.2.18-a1 (c (n "cirru_edn") (v "0.2.18-a1") (d (list (d (n "cirru_parser") (r "^0.1.20") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "15aljjx7aigagiv7d2wc24xqx47zj4k90zpj1pl5rivdxs7fhdqm")))

(define-public crate-cirru_edn-0.2.18-a2 (c (n "cirru_edn") (v "0.2.18-a2") (d (list (d (n "cirru_parser") (r "^0.1.20") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1lgqfc66vg6s6qajqhrhx898x9wbj5kfnr9rgmrz2jnwgw3j558z")))

(define-public crate-cirru_edn-0.2.18-a3 (c (n "cirru_edn") (v "0.2.18-a3") (d (list (d (n "cirru_parser") (r "^0.1.20") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gkq5ibswh8gimx9f2z0nh2ls9h7383dassgxnh24daq6zh26mfn")))

(define-public crate-cirru_edn-0.2.18 (c (n "cirru_edn") (v "0.2.18") (d (list (d (n "cirru_parser") (r "^0.1.21") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0xx99vp1c33gmkapwg9pz0gaiym8cvm4k0c1g05fczqjplgjkmw9")))

(define-public crate-cirru_edn-0.2.19 (c (n "cirru_edn") (v "0.2.19") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0jrbs9ykfjlajx8mdz0kl54b2987lr7kwq3wl9icagln3q8rcq4p")))

(define-public crate-cirru_edn-0.2.20 (c (n "cirru_edn") (v "0.2.20") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1gbfsqvkrws2kykfi4ysr6czmqidxb7fmbjd7ih07fb7vyihnqsc")))

(define-public crate-cirru_edn-0.2.21 (c (n "cirru_edn") (v "0.2.21") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0ma9crr9cxhyg9i8mb7wdniw33b8dvqnx2yf5mp0ni3ygpffmj33")))

(define-public crate-cirru_edn-0.3.0 (c (n "cirru_edn") (v "0.3.0") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1q1wawx9l67c75px8dhd6cvsm0d1233a4s5zpm31nnvx1906wj3f")))

(define-public crate-cirru_edn-0.3.1 (c (n "cirru_edn") (v "0.3.1") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1rj8zc40731x4myfkzmb667w1a6l3shlqdm8a20147pb2x10j658")))

(define-public crate-cirru_edn-0.4.0 (c (n "cirru_edn") (v "0.4.0") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0hzxif3bk7gvljk9hip1v05cbh64lg9vjya8zfbqmqk4mafqpxrp")))

(define-public crate-cirru_edn-0.4.1 (c (n "cirru_edn") (v "0.4.1") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0qj5r2z512m5ii7lz57wbai3d9wji9y4769rm4kkkkwlbxi155kr")))

(define-public crate-cirru_edn-0.4.2 (c (n "cirru_edn") (v "0.4.2") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0w1wxvrw2ic48vg3kcwfwcbzl9g7cny36x7dilg4f25p7ibbi6ak")))

(define-public crate-cirru_edn-0.5.0-a1 (c (n "cirru_edn") (v "0.5.0-a1") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "02sx1rzyan3647lr7z70zb53fwn4j1445jka7a93vhm8bhwmjzpz")))

(define-public crate-cirru_edn-0.5.0-a2 (c (n "cirru_edn") (v "0.5.0-a2") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1myayyyasinbvypnshn32gj65msjyj5kxi01z7rbivp7xf37w5nd")))

(define-public crate-cirru_edn-0.5.0-a3 (c (n "cirru_edn") (v "0.5.0-a3") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0qyzxm4rr8rx06bzj4498vp9vxpvba54ki2ls7d7bb897zzwjvr1")))

(define-public crate-cirru_edn-0.5.0-a4 (c (n "cirru_edn") (v "0.5.0-a4") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "04blwv00gxhknxnbvlqq66c6pmj4mxzsibn035zv9ljm2zqpdgqp")))

(define-public crate-cirru_edn-0.5.0-a5 (c (n "cirru_edn") (v "0.5.0-a5") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "01ydzj2mr4y01nzmjbqm4ky00ib3d53wy1hbg05j1c602pyb7jmi")))

(define-public crate-cirru_edn-0.5.0 (c (n "cirru_edn") (v "0.5.0") (d (list (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0i7dc6arv4rnwn1i5wnik8yg6q47r4fkdvlx7w10jx4zwn7d6dca")))

(define-public crate-cirru_edn-0.5.1 (c (n "cirru_edn") (v "0.5.1") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "04g6b3shrm9brs27vdh3mvmcwp2nvm868m105j6pwgc8cdmyr99z")))

(define-public crate-cirru_edn-0.5.2 (c (n "cirru_edn") (v "0.5.2") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1sympv4f43hv87lpgy8yscv3vbpw5fl8lym74q6vnaxmi5dw41vd")))

(define-public crate-cirru_edn-0.5.3 (c (n "cirru_edn") (v "0.5.3") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1j22379nvyldsw632zvj4rqwd447fa501jb1qj78zn2c7c00zd84")))

(define-public crate-cirru_edn-0.5.4 (c (n "cirru_edn") (v "0.5.4") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1dsxbzhyajbqwc8l2sxhg8apq5rfxfy3wcknri8yqp07hxyvdvcg")))

(define-public crate-cirru_edn-0.5.5 (c (n "cirru_edn") (v "0.5.5") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "18j8078c74c50xf644dnc6v8hrnm1888dk16qmm65mlk46if4jmb")))

(define-public crate-cirru_edn-0.6.0 (c (n "cirru_edn") (v "0.6.0") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "0.1.*") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "11r3381q3nz7722hnacqwfdb8m2azj6si8skwzbqn1hllwzgri1j")))

(define-public crate-cirru_edn-0.6.1 (c (n "cirru_edn") (v "0.6.1") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1yrmvzbi0z37dch7clgn47gfzaq3c7ckgg48y2gpim2n6j80b511")))

(define-public crate-cirru_edn-0.6.2 (c (n "cirru_edn") (v "0.6.2") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "11whp1cmpcf8g2jab279myypwid1sjhpyc2jfp3x1920byyl7lq8")))

(define-public crate-cirru_edn-0.6.3 (c (n "cirru_edn") (v "0.6.3") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0p7hs0zr0rgzllxlhrpldcq3w83skjl4c3vj4zvic7bayv83br5l")))

(define-public crate-cirru_edn-0.6.4-a1 (c (n "cirru_edn") (v "0.6.4-a1") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0mbhhl356qmyyq5xsvi7vcqdvkh1qaaqymcvkldrs0amwklwiy53")))

(define-public crate-cirru_edn-0.6.4-a2 (c (n "cirru_edn") (v "0.6.4-a2") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0f6qi5zfpy9imqr0pw2c184bavygpjrlyfiga054ha1xas7qggnd")))

(define-public crate-cirru_edn-0.6.4-a3 (c (n "cirru_edn") (v "0.6.4-a3") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0n2r2k66agka6mqnjpckj36mxxj8w096gh2bqqq702b9yzcljrwv")))

(define-public crate-cirru_edn-0.6.4-a4 (c (n "cirru_edn") (v "0.6.4-a4") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1vwclgipcn6v15bzq6ji8szfb744qp8iav2gwsx2cd3va0pr2l6b")))

(define-public crate-cirru_edn-0.6.4-a5 (c (n "cirru_edn") (v "0.6.4-a5") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1s79ah3d2fa19jnwngw0nyiaqi2njhgv0fivl5nrlinfp3ppc1sh")))

(define-public crate-cirru_edn-0.6.5 (c (n "cirru_edn") (v "0.6.5") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0k96d82pp7aixrp3bxzxi7yba70wc092is5zky9n5ykda51z8ang")))

(define-public crate-cirru_edn-0.6.6 (c (n "cirru_edn") (v "0.6.6") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1xbd61fd4bdp177rxkk34jgzggzppkzyvb01z8gq17nrw636brsi")))

(define-public crate-cirru_edn-0.6.7 (c (n "cirru_edn") (v "0.6.7") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "05zjqy8m8660k58snr0ay0mkxn0kr85lwj926ylfy1sm3qygq50c")))

(define-public crate-cirru_edn-0.6.8 (c (n "cirru_edn") (v "0.6.8") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.29") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1281hk1vxfal2mrsgybr2xi9w5vcngm1sdx17s48054yjcy8maml")))

(define-public crate-cirru_edn-0.6.9 (c (n "cirru_edn") (v "0.6.9") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "cirru_parser") (r "^0.1.30") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0x19kfy21j24ghlv7ls35d55j6dpbvgcbfbw3y67bizhb01lazlx")))

