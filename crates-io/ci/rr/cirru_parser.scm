(define-module (crates-io ci rr cirru_parser) #:use-module (crates-io))

(define-public crate-cirru_parser-0.0.1 (c (n "cirru_parser") (v "0.0.1") (d (list (d (n "modulo") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0hi6nb5nzbgi7ivi2y78p41q6g7iyryc7fqmjlfmilhl01c6pih2")))

(define-public crate-cirru_parser-0.0.2 (c (n "cirru_parser") (v "0.0.2") (d (list (d (n "modulo") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "12d5yfp82iipbcn0i9fngw1r1mbnx20lh330hk10sqv5r11x691w")))

(define-public crate-cirru_parser-0.0.3 (c (n "cirru_parser") (v "0.0.3") (d (list (d (n "modulo") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0085kl1izbdy0fg1lm47inz5cr4vyhjvkfvvwg9sd94lxcjyrlvd")))

(define-public crate-cirru_parser-0.0.4 (c (n "cirru_parser") (v "0.0.4") (d (list (d (n "modulo") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "17xxnnxk590rv5szaxn4d3v0gk46jzx273c30bh35g8xkqzn0iq8")))

(define-public crate-cirru_parser-0.0.5 (c (n "cirru_parser") (v "0.0.5") (d (list (d (n "modulo") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0gcvl2q09rwz57bqmqjax38nnqqn3z4kvz495gqm86lqzghfdqxw")))

(define-public crate-cirru_parser-0.0.6 (c (n "cirru_parser") (v "0.0.6") (d (list (d (n "modulo") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1ip96d9r6gkwkvvs61icyvrqdl91392c0qa7lzhazdkd3g0g2261")))

(define-public crate-cirru_parser-0.0.7 (c (n "cirru_parser") (v "0.0.7") (d (list (d (n "modulo") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1ig11qrjb9plkg7n4akd5rb6ybjh877rq4pa75nsl1kxiqfz3fcx")))

(define-public crate-cirru_parser-0.0.8 (c (n "cirru_parser") (v "0.0.8") (d (list (d (n "modulo") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1mf9nw17q3d6hjc1al0x96pn98513syk83lpyfwxwirxk7v8cf26")))

(define-public crate-cirru_parser-0.0.9 (c (n "cirru_parser") (v "0.0.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "modulo") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1lg7zlsbh5z1vlbmjnnywwpc77zg8pwqxk1072136lfx8lykp5zn")))

(define-public crate-cirru_parser-0.1.0 (c (n "cirru_parser") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "modulo") (r "^0.1.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0ww3hfb9bassxx0ckjlsvg9ccjz1ixrkxp555pc16f809zn1jwwi")))

(define-public crate-cirru_parser-0.1.1 (c (n "cirru_parser") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0d7ccci6gr9z8d80yjb96nq3gq1ibv1k2bryhckgvskkfv83kmk5")))

(define-public crate-cirru_parser-0.1.2 (c (n "cirru_parser") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1g1g0s4d0wv6066irwvibq3jaryrm0xcwg2jwvwfc2yw9lankspi")))

(define-public crate-cirru_parser-0.1.3 (c (n "cirru_parser") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1hg5ks6kkp4lzxjkgm10063mdy4qldsgb3m904jlh87xyxhz0pw0")))

(define-public crate-cirru_parser-0.1.4 (c (n "cirru_parser") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "0dd6p1kqlnxm51qfq8pp95zd81h24y70gz2y8x4z3y364jbir1kr")))

(define-public crate-cirru_parser-0.1.5 (c (n "cirru_parser") (v "0.1.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2") (d #t) (k 0)))) (h "1w4w5i0s7xvchj6abz4rsv9rccln8hbp0bkbwiz22za6m24wdzda")))

(define-public crate-cirru_parser-0.1.6 (c (n "cirru_parser") (v "0.1.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "04iygsj0yi0i7cms5kdn1di5bk34p05k6pa5sg90dj87z8lhcwz9")))

(define-public crate-cirru_parser-0.1.7 (c (n "cirru_parser") (v "0.1.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "1pknmg26lg3dfrsb1sn488lp9pf7jmlmar80smdx09yax1fdawj1")))

(define-public crate-cirru_parser-0.1.8 (c (n "cirru_parser") (v "0.1.8") (d (list (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "18yc8hl5md4sc4dm0p34hxzx34ccwq4ypprjl1a7vq6c7hj7f82v")))

(define-public crate-cirru_parser-0.1.9 (c (n "cirru_parser") (v "0.1.9") (d (list (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "0vjcic5608kgfmgv9nxiz9m5v93ajk4ykhgbzd6viaxbr7aj5vly")))

(define-public crate-cirru_parser-0.1.10 (c (n "cirru_parser") (v "0.1.10") (d (list (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "09zaqsv4azwbh37mw6972ackjc59c08fy81jsbiafxwhn55hwbdm")))

(define-public crate-cirru_parser-0.1.12 (c (n "cirru_parser") (v "0.1.12") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "01csaz779ii9iddaqq4qpnd5bp5702r6j4mdralka0fn3rqgwi3k")))

(define-public crate-cirru_parser-0.1.13 (c (n "cirru_parser") (v "0.1.13") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)))) (h "1hvjakfsp9wngxl51fsavcaxbd57h0jpy1d1yy4iki7lhn3rkjrz")))

(define-public crate-cirru_parser-0.1.14 (c (n "cirru_parser") (v "0.1.14") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 2)))) (h "07b35kpmp9gphbmvj0h42zsh07gmqxlycgi9cfrnwws8ll7c7clb")))

(define-public crate-cirru_parser-0.1.15 (c (n "cirru_parser") (v "0.1.15") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 2)))) (h "0gaf2qclg1lrjbcnibix3774kdj36y652pnfs4vydpjif5l1dzi3")))

(define-public crate-cirru_parser-0.1.16 (c (n "cirru_parser") (v "0.1.16") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.69") (d #t) (k 2)))) (h "1pkg87f86ni07sdhlzmlpgr3zxyl18z3xdsrims93i0vpq8lw07m")))

(define-public crate-cirru_parser-0.1.17 (c (n "cirru_parser") (v "0.1.17") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 2)))) (h "0hr7pi7pqarj67kk6lxb52fp5abwrki6zkd5in6wlrniysddy71d")))

(define-public crate-cirru_parser-0.1.18 (c (n "cirru_parser") (v "0.1.18") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.131") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (o #t) (d #t) (k 0)))) (h "1km6zfwiz9kgb13ix2irmmiiy1fkpbqyashnw3k42n6lpa68z0gl") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.19-a1 (c (n "cirru_parser") (v "0.1.19-a1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "1nywddsly7h04b7dakdyb4n5bqlqk9185pjx5gkrj80nxmhmbp60") (f (quote (("use-json" "serde_json") ("default" "serde"))))))

(define-public crate-cirru_parser-0.1.19-a2 (c (n "cirru_parser") (v "0.1.19-a2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "0fsj8lhp2ib00ngf5jg4bm67gf5mm2b151i882ca7rj2qdvkj6vv") (f (quote (("use-json" "serde_json") ("default" "serde"))))))

(define-public crate-cirru_parser-0.1.20 (c (n "cirru_parser") (v "0.1.20") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "0p0m9i6kaln2ax1arfd256xs03ppjnhnvfcxgagh80k95z5fgvf4") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.21 (c (n "cirru_parser") (v "0.1.21") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "193s15kzas5i0w7ki4yx3z9snnla5zn7f915y7fyl2c9ggbgb0jz") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.22 (c (n "cirru_parser") (v "0.1.22") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "1r8ri3mflgp5wj3sffyqkfdq42in2flqfin0w0cvfw8295m6s8rr") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.23 (c (n "cirru_parser") (v "0.1.23") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.137") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.81") (o #t) (d #t) (k 0)))) (h "1f7qg5rf23rcfxiwzj7x1yrqng5jaa36qpxvgwl8pr41a4yf2myv") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.24 (c (n "cirru_parser") (v "0.1.24") (d (list (d (n "criterion") (r "^0.3.6") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (o #t) (d #t) (k 0)))) (h "1al140nx1kbjw2lr0g5qf6j13vxln35nqvmncyrcmrxpmwnjr50c") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.25 (c (n "cirru_parser") (v "0.1.25") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)))) (h "1176hxrzysw8c029izkdqrwlwf6n5xl81smg3hnwgly90j13i5qj") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.26 (c (n "cirru_parser") (v "0.1.26") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)))) (h "0g75dzj5y3l8d76k45vfjki7wf0ldzpj275gdgim0byrnfc076mf") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.27 (c (n "cirru_parser") (v "0.1.27") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)))) (h "05igrxlz5bwhir0kg3cdhrjwwh3jr38yf1l8r2sx4f4hca9bd1q2") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.28 (c (n "cirru_parser") (v "0.1.28") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)))) (h "1brfr3v7sr2rr1bn2iyy7amxhj99fpcd47ilafrf5pd6p5vd1lks") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.29 (c (n "cirru_parser") (v "0.1.29") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (o #t) (d #t) (k 0)))) (h "0y4a04x2np0s7fvqzkvgaj3vkpr02ahiycjbr7w73m3qazhlfqn9") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

(define-public crate-cirru_parser-0.1.30 (c (n "cirru_parser") (v "0.1.30") (d (list (d (n "bincode") (r "^2.0.0-rc.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.202") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.117") (o #t) (d #t) (k 0)))) (h "0nwl9a8cpira9w7d8bkzpapkwxnpdlg0pdkbp7i6vpbmxid1wkdv") (f (quote (("use-serde" "serde" "serde_json") ("default"))))))

