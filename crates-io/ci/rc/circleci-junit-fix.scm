(define-module (crates-io ci rc circleci-junit-fix) #:use-module (crates-io))

(define-public crate-circleci-junit-fix-0.1.0 (c (n "circleci-junit-fix") (v "0.1.0") (d (list (d (n "memchr") (r "^2.4") (d #t) (k 0)) (d (n "quick-xml") (r "^0.22") (d #t) (k 0)))) (h "0rf248f1rkzxcpa53ici0zsms0c1npnijqcm2qlc27lj1zqharfc")))

(define-public crate-circleci-junit-fix-0.2.0 (c (n "circleci-junit-fix") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "1j7ggg61p0cvyfk6l0gnji947smq376897x6pgj1p2isjhvp96j9")))

(define-public crate-circleci-junit-fix-0.2.1 (c (n "circleci-junit-fix") (v "0.2.1") (d (list (d (n "quick-xml") (r "^0.23") (d #t) (k 0)))) (h "14qr32bszh3px41apxyxg3j6vk66agr5xbd4xmy7mj23f456vw4p")))

