(define-module (crates-io ci rc circularing) #:use-module (crates-io))

(define-public crate-circularing-1.0.0 (c (n "circularing") (v "1.0.0") (d (list (d (n "const-assert") (r "^1.0.1") (d #t) (k 0)) (d (n "likeness") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0m9l4qgpfshrzsnhq8cl1vzby0xlci1dy57khqkd3wrl9df5pphv")))

(define-public crate-circularing-1.1.0 (c (n "circularing") (v "1.1.0") (d (list (d (n "const-assert") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "likeness") (r "^1.0.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "0lip45jlb56k2j4krlxlg0zinlg3simb7ngx4a33mhd9g3kahp1f") (f (quote (("nightly" "const-assert"))))))

(define-public crate-circularing-1.2.0 (c (n "circularing") (v "1.2.0") (d (list (d (n "const-assert") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "likeness") (r "^1.1.0") (d #t) (k 0)))) (h "13pvjhqf4gcfdw455q4ca92wnqh8js04p5v0snh5hb8735d5xbl3") (f (quote (("nightly" "const-assert"))))))

(define-public crate-circularing-1.2.1 (c (n "circularing") (v "1.2.1") (d (list (d (n "const-assert") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "likeness") (r "^1.1.0") (d #t) (k 0)))) (h "0qk2apnfppfpm93w0kyihpj45c9as3mmwiqww1lwvl9gjjsh55dw") (f (quote (("nightly" "const-assert")))) (y #t)))

(define-public crate-circularing-1.2.2 (c (n "circularing") (v "1.2.2") (d (list (d (n "const-assert") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "likeness") (r "^1.1.0") (d #t) (k 0)))) (h "132rsndcc9s5kdvx0k0s6vrm7a4gdbb8n95a33rda9n2xd20d6gf") (f (quote (("nightly" "const-assert"))))))

(define-public crate-circularing-2.0.0 (c (n "circularing") (v "2.0.0") (d (list (d (n "const-assert") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "likeness") (r "^1.1.0") (d #t) (k 0)))) (h "1x9ilm4y849gd2nk1mfgk9a5df1q9sfzl22948rck62qaaq2ilkb") (f (quote (("nightly" "const-assert"))))))

(define-public crate-circularing-2.2.0 (c (n "circularing") (v "2.2.0") (d (list (d (n "const-assert") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "likeness") (r "^1.1.0") (d #t) (k 0)))) (h "1zzrmc0z4zsizwk3l5l60zjqbl6vsn9zaniw45v6v1nach1gmm4w") (f (quote (("nightly" "const-assert"))))))

(define-public crate-circularing-2.2.1 (c (n "circularing") (v "2.2.1") (d (list (d (n "const-assert") (r "^1.0.1") (o #t) (d #t) (k 0)) (d (n "likeness") (r "^1.1.0") (d #t) (k 0)))) (h "0c3vrd0s3kwpzbfxjg0g4h8fi6bgkrriqpxbd7dkgb5c8pjzbv0n") (f (quote (("nightly" "const-assert"))))))

