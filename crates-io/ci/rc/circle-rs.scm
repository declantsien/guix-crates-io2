(define-module (crates-io ci rc circle-rs) #:use-module (crates-io))

(define-public crate-circle-rs-0.1.0 (c (n "circle-rs") (v "0.1.0") (h "0r7zxp99wcj27dg11143z2gknf1bpvgna72l50bhdr4ggb92x7hh")))

(define-public crate-circle-rs-0.2.0 (c (n "circle-rs") (v "0.2.0") (h "1613c7j706lz7c05inira2qnagkh830m1nvnrwi795mj063773m2")))

(define-public crate-circle-rs-0.3.0 (c (n "circle-rs") (v "0.3.0") (h "0sxiz9lixj9df0l0l4y5hq9p16mciqrs617g0z0rj2x2rjls2wrk")))

(define-public crate-circle-rs-0.3.1 (c (n "circle-rs") (v "0.3.1") (h "17i76927z5hsarcvy9maxk595g72x2h4j6rkx0ylcm4silw7pqwm")))

(define-public crate-circle-rs-0.3.2 (c (n "circle-rs") (v "0.3.2") (h "0fh4cb9w1g0hnnqhg64ric99xzkv0h8a57i7z8143kjir8a7qm7z")))

(define-public crate-circle-rs-0.3.3 (c (n "circle-rs") (v "0.3.3") (d (list (d (n "termion") (r "^1.5.3") (o #t) (d #t) (k 0)))) (h "1f5qk7w6af9a2njlbvgn7az5m7vpadqlhvbbxj4xgp0vmwzd9jm9") (f (quote (("end" "termion") ("default"))))))

