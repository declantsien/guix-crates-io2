(define-module (crates-io ci rc circle) #:use-module (crates-io))

(define-public crate-circle-0.0.0 (c (n "circle") (v "0.0.0") (h "139r7p7yn0249vjcyb4dh652z8hlgxqiikqr1rs0qx39b3sl7l3i") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-circle-0.1.0 (c (n "circle") (v "0.1.0") (h "06sx8bkpy0hlzd8yk7xh6fwx6jqhn8lg56z8qlw6ycwcl7bdbqpf") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-circle-0.1.1 (c (n "circle") (v "0.1.1") (h "0ymw620xmb4hj99xz3hri7ksrvsjy8dg9bn73cg478yl022wwx3s") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-circle-0.1.2 (c (n "circle") (v "0.1.2") (h "00w9c47br3a578ikxkk7navgkjdmvn1d0yj7lkjgg9cpkvkdfk48") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-circle-0.1.3 (c (n "circle") (v "0.1.3") (h "1gvzyx3wnn0hlzzaqslfvkl2vmgls4rjai742naaj5bh4kzm0jjj") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-circle-0.1.4 (c (n "circle") (v "0.1.4") (h "03dk76jql8ngfyw578246ivmjfhf0f7zkxmnz6x8q474qb3k05w9") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-circle-0.1.5 (c (n "circle") (v "0.1.5") (h "1fpwm0fd6pgwjw3q1wyvsgvsh2c935qgx95sx715yp0549fi11yx") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-circle-0.1.6 (c (n "circle") (v "0.1.6") (h "0mkhkwpz2sdq4f6fj7500y1zmz23kk83vm3qq1yp3chg5flwj0gg") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-circle-0.1.7 (c (n "circle") (v "0.1.7") (h "0silvxgnkx043zg2prf298y6p0xlqj9ffjfnx4l1yb6wkvx2myqc") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-circle-0.1.8 (c (n "circle") (v "0.1.8") (d (list (d (n "wolfram_wxf") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0rv438x83wch5727562x23dm0sqcb9c0ybh03pzh9vabqlfiaqli") (f (quote (("f64") ("f32") ("default" "f32"))))))

(define-public crate-circle-0.1.9 (c (n "circle") (v "0.1.9") (d (list (d (n "serde") (r "1.0.*") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0b8qcx6ivwkk2pfblxm837i2kf5nhfh943mhya7g8834rss4ai61") (f (quote (("f64") ("f32") ("default" "f32"))))))

