(define-module (crates-io ci rc circular_vec) #:use-module (crates-io))

(define-public crate-circular_vec-0.1.0 (c (n "circular_vec") (v "0.1.0") (h "1p4rg5m1inh34j6vykn6w0p44ayp9dz22zf8czlw0v4i9bmxn2jc")))

(define-public crate-circular_vec-0.1.1 (c (n "circular_vec") (v "0.1.1") (h "1fkwarjfxfgz3xgllgzxiakspsial8fig618dw5f4zsdln2kvs9v")))

