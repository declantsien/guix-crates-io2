(define-module (crates-io ci rc circle_area) #:use-module (crates-io))

(define-public crate-Circle_Area-0.1.0 (c (n "Circle_Area") (v "0.1.0") (h "0162jfglmypbad9y4655545j8dkgnqzh8c6iv2hcwcv6axi054ww") (y #t)))

(define-public crate-Circle_Area-0.1.1 (c (n "Circle_Area") (v "0.1.1") (h "0cjvch8891wm26bnggc66xp4wifsvvpmfrd4w1vfbmjj9aqkr51n") (y #t)))

(define-public crate-Circle_Area-0.1.2 (c (n "Circle_Area") (v "0.1.2") (h "08rjhxf9nmyrqvqa3ayryip16cvkb8hpgkjj1nhkqcrs3qhffmw4") (y #t)))

(define-public crate-Circle_Area-0.1.3 (c (n "Circle_Area") (v "0.1.3") (h "03b27axs3bcq49kq3sfsr9bh3adkp7zllzm4qwr267n02yn9jbx5")))

(define-public crate-Circle_Area-0.1.4 (c (n "Circle_Area") (v "0.1.4") (h "0pg4qlr79g16znzgf1sxk1yzr3yjj5gvsns7yzqvs0z6z82d1hn9")))

