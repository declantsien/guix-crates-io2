(define-module (crates-io ci rc circom-lsp-type-analysis) #:use-module (crates-io))

(define-public crate-circom-lsp-type-analysis-2.1.5 (c (n "circom-lsp-type-analysis") (v "2.1.5") (d (list (d (n "num-bigint-dig") (r "^0.6.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "program_structure") (r "^2.1.5") (d #t) (k 0) (p "circom-lsp-program-structure")))) (h "0sf7xdl8lbakcw8hhyqcr09alf48h74cdgsj2wvvyq9lf6dycyfd")))

