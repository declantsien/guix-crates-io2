(define-module (crates-io ci rc circadia) #:use-module (crates-io))

(define-public crate-circadia-0.0.1 (c (n "circadia") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)))) (h "1b74pv7fb0a5vg0dcc9xxi8bxr2x9kjmk8zma2f2zh7zkchzwzqp")))

(define-public crate-circadia-0.0.2 (c (n "circadia") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)))) (h "0ycbiwzz8sp7n2y3jpxxrx62qranwskhx54q3vqa13dyiyxgchs9")))

(define-public crate-circadia-0.1.0 (c (n "circadia") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)))) (h "1gsblp2bkbnpgn091c68khnb9xw6k1vb1r95li8cxvdh136ibcnc")))

