(define-module (crates-io ci rc circular-array) #:use-module (crates-io))

(define-public crate-circular-array-0.1.0 (c (n "circular-array") (v "0.1.0") (h "1hs3pga45ay3d058l688s4akq8i7ap5mpmx6m2azfp4v11nm03vb")))

(define-public crate-circular-array-0.2.0 (c (n "circular-array") (v "0.2.0") (h "1d3ypaz8pg073rbl15k9r57rgam1d4lalgzlx62mpm34nc5h33c5")))

(define-public crate-circular-array-0.2.1 (c (n "circular-array") (v "0.2.1") (h "0f6vmc8g8d9h277mwadhkchwplgs7b1rkp5c3v011v6g55bw87j5")))

(define-public crate-circular-array-0.2.2 (c (n "circular-array") (v "0.2.2") (h "1z8mjnfrr3gdvaxglxq472p5mzm04mrj9vls2idvgp7czflysni7")))

