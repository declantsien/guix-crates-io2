(define-module (crates-io ci rc circular) #:use-module (crates-io))

(define-public crate-circular-0.1.0 (c (n "circular") (v "0.1.0") (h "1l0y6iqbs777mm16g9icv15gfbxxb944yvr7i17iimkq3vaqcc9h")))

(define-public crate-circular-0.2.0 (c (n "circular") (v "0.2.0") (h "0niqsmq2jkhpk70zwv72r2s6yp2z9067sdrk30rblkhkzylmx542")))

(define-public crate-circular-0.3.0 (c (n "circular") (v "0.3.0") (h "1slb0zd1xj0fjd0ql86zs57l1xbn5ywsyj1d8397bcvc1yg27z5h")))

