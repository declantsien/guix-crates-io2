(define-module (crates-io ci rc circular-queue) #:use-module (crates-io))

(define-public crate-circular-queue-0.1.0 (c (n "circular-queue") (v "0.1.0") (h "0zy4wkqfi53dmh55j6x1vzgq8pgmx2vw1cb5ix0mwp9iy47w2hda")))

(define-public crate-circular-queue-0.1.1 (c (n "circular-queue") (v "0.1.1") (h "06v739qb6pkr4baik5psw8nxh6py8y3rdnzymivcmx71rg5yzijb")))

(define-public crate-circular-queue-0.1.2 (c (n "circular-queue") (v "0.1.2") (h "0y3vydynspkphw46r8ln7mjm79k5if71svdmcv5psvd8chkifdfd")))

(define-public crate-circular-queue-0.2.0 (c (n "circular-queue") (v "0.2.0") (h "0iamzrm5da1klw4bh7gkdvdx4453blp17l0qbzbji5s1s1h3kriv")))

(define-public crate-circular-queue-0.2.1 (c (n "circular-queue") (v "0.2.1") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1wfxy06pc52s6adk307pqc5v7a6p5qnbbrjrx7djcrmx4zav4y7h")))

(define-public crate-circular-queue-0.2.2 (c (n "circular-queue") (v "0.2.2") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1dvvm1h1c7fy4hvv99xmi7cbi83447gcfv9kccpibm43fjhrqswr")))

(define-public crate-circular-queue-0.2.3 (c (n "circular-queue") (v "0.2.3") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0pvqbc4g3kxc48njjcrs6kw2kx2nrccbfvn99q1256syfddv77vc")))

(define-public crate-circular-queue-0.2.4 (c (n "circular-queue") (v "0.2.4") (d (list (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0yjwcjbj9f16qipcddifdqizjwfv81v411wny5gzrii1xnpas2hv")))

(define-public crate-circular-queue-0.2.5 (c (n "circular-queue") (v "0.2.5") (d (list (d (n "bincode") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "0hm5h6rr52gs47lgc37b487ks7lzzxf358r9pbfccgbj57bmqq7d") (f (quote (("serde_support_test" "serde_support" "serde_test" "serde_json" "bincode") ("serde_support" "serde") ("default"))))))

(define-public crate-circular-queue-0.2.6 (c (n "circular-queue") (v "0.2.6") (d (list (d (n "bincode") (r "^1.2.1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 1)))) (h "1dar5jr3hmn5hmwscj6jk55mcmw9nmgy779kvc83lx0wmmz34d0x") (f (quote (("serde_support_test" "serde_support" "serde_test" "serde_json" "bincode") ("serde_support" "serde") ("default"))))))

