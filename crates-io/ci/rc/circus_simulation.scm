(define-module (crates-io ci rc circus_simulation) #:use-module (crates-io))

(define-public crate-circus_simulation-0.0.1 (c (n "circus_simulation") (v "0.0.1") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)))) (h "0p4fla4bv0a9ryhrm77ardsqx6v319200q722xdaxhyaxqq78752")))

