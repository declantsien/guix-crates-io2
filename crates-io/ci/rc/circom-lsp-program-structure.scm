(define-module (crates-io ci rc circom-lsp-program-structure) #:use-module (crates-io))

(define-public crate-circom-lsp-program-structure-2.1.5 (c (n "circom-lsp-program-structure") (v "2.1.5") (d (list (d (n "codespan") (r "^0.9.0") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.9.0") (d #t) (k 0)) (d (n "num-bigint-dig") (r "^0.6.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.91") (d #t) (k 0)))) (h "1gan3h5jdvmff0x7djghbdc836xwkxn9m1wvqadmi5dqb7zdin9z")))

