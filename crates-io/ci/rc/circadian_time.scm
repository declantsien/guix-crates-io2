(define-module (crates-io ci rc circadian_time) #:use-module (crates-io))

(define-public crate-circadian_time-0.0.1 (c (n "circadian_time") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "circadia") (r "^0.0.1") (d #t) (k 0)))) (h "1n0zz1qhh8p22qk5vffz8m0j5psh5yq7lam330y6jryv3rs543kz")))

(define-public crate-circadian_time-0.0.2 (c (n "circadian_time") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "circadia") (r "^0.0.1") (d #t) (k 0)))) (h "1qyrsv2yf8l6ji3r01p1lafgdmp90nl5qhg6nlyqcz7s1mrmvc9m")))

(define-public crate-circadian_time-0.0.3 (c (n "circadian_time") (v "0.0.3") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "circadia") (r "^0.0.2") (d #t) (k 0)))) (h "0v3i88xa8bxz5dxcg2wrjzkcc5rfpwvl0xb6dcnsj0y1xhz2blmw")))

