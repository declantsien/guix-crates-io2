(define-module (crates-io ci rc circbuf) #:use-module (crates-io))

(define-public crate-circbuf-0.1.0 (c (n "circbuf") (v "0.1.0") (d (list (d (n "vecio") (r "^0.1.0") (d #t) (k 0)))) (h "061gr644vi5h6irma2290fpdjl35f3darjajdgwwvcrn0mz3xcxh")))

(define-public crate-circbuf-0.1.1 (c (n "circbuf") (v "0.1.1") (d (list (d (n "vecio") (r "^0.1.0") (d #t) (k 0)))) (h "11cpdfdp9ziwkd8r1mkjy45yj13canvndh4x6gpand1j2ya70c1q")))

(define-public crate-circbuf-0.1.2 (c (n "circbuf") (v "0.1.2") (d (list (d (n "vecio") (r "^0.1.0") (d #t) (k 0)))) (h "0w8i11q1s2bp1njmjdnk826bk9sch9ficmkgvhwcxjcl7xjja7hn")))

(define-public crate-circbuf-0.1.3 (c (n "circbuf") (v "0.1.3") (d (list (d (n "vecio") (r "^0.1.0") (d #t) (k 2)))) (h "0fbmk8qcb02j6av5q5l3mhwsxszmlph5iqyy22y7avryrxbsaxxx") (f (quote (("nightly"))))))

(define-public crate-circbuf-0.1.4 (c (n "circbuf") (v "0.1.4") (d (list (d (n "vecio") (r "^0.1.0") (d #t) (k 2)))) (h "134z4ffaq1mqmb75si6dy0lbgd6dg6m2r0r1ifhy91nqjps9flj2") (f (quote (("nightly"))))))

(define-public crate-circbuf-0.2.0 (c (n "circbuf") (v "0.2.0") (d (list (d (n "bytes_rs") (r "^0.5") (o #t) (d #t) (k 0) (p "bytes")) (d (n "vecio") (r "^0.1.0") (d #t) (k 2)))) (h "1133kg6pmilsw6hij5xcybwhfrrwqlpjc09v2imnchphq45nfkxc") (f (quote (("nightly") ("bytes" "bytes_rs"))))))

(define-public crate-circbuf-0.2.1 (c (n "circbuf") (v "0.2.1") (d (list (d (n "bytes") (r "^1.4.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 2)) (d (n "vecio") (r "^0.1.0") (d #t) (k 2)))) (h "16ikqvmj8nnvpk3dmp0lgphyldwfjsyjjgmzwab4a8cyzd1gg6fr") (s 2) (e (quote (("bytes" "dep:bytes")))) (r "1.56")))

