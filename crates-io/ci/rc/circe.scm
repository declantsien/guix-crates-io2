(define-module (crates-io ci rc circe) #:use-module (crates-io))

(define-public crate-circe-0.1.0 (c (n "circe") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "0w2gkm2ldyhmlphaa9x7c6hc66sxx82h5nchhml4qk1dbi2lx9hb")))

(define-public crate-circe-0.1.1 (c (n "circe") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "1m43qwb9barw1dz1w6j18skdwg7n9gfx35za0pqbp8k9j3jzvfip")))

(define-public crate-circe-0.1.2 (c (n "circe") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "11hxzqnfz86phlbkmc6p8k832x1938w5lhixb24jcg4iwkxnrpi1")))

(define-public crate-circe-0.1.3 (c (n "circe") (v "0.1.3") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1jlx5ggs9gzgxfsyrd5g663igbdhijh9pam9jimadry2r2mw9clf")))

(define-public crate-circe-0.1.4 (c (n "circe") (v "0.1.4") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0hmwl3l9lwzn9rfjqz5nsds3wzazp7lqyk2spdp2s1cgqkhs1aq9") (f (quote (("tls" "native-tls") ("default" "tls") ("debug"))))))

(define-public crate-circe-0.1.5 (c (n "circe") (v "0.1.5") (d (list (d (n "native-tls") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5") (o #t) (d #t) (k 0)))) (h "1nf47qi466vaz6d28fi2ch8gkq6q8qaf4ng5rgv3nsfv3n7zsv9y") (f (quote (("toml_support" "toml" "serde" "serde_derive") ("tls" "native-tls") ("default" "tls") ("debug"))))))

