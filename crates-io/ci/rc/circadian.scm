(define-module (crates-io ci rc circadian) #:use-module (crates-io))

(define-public crate-circadian-0.1.0 (c (n "circadian") (v "0.1.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "1nabhp6n9czgl9g2hj2i12wl2x0a2z0lk4xcjrgqbc92vqv94fsb")))

(define-public crate-circadian-0.3.0 (c (n "circadian") (v "0.3.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "1gcfjba49hr852hlww2pas8d14alvbs5g6v4vxw11srzawvrs78v")))

(define-public crate-circadian-0.4.0 (c (n "circadian") (v "0.4.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "0snzk80x3zhiflr33yy2ld0bcj07b44b30xaky8mdznz85j9qgjn")))

(define-public crate-circadian-0.5.0 (c (n "circadian") (v "0.5.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "0vxplmkfshp2yzsw0xd8cw1c712dbdjdiz1pzsa1bbjkzqc8f7bb")))

(define-public crate-circadian-0.6.0 (c (n "circadian") (v "0.6.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "0f4sh5lh09n0qhzsd5rsi45x5iycz4ahlc67wqwm1cvsa12x1mk0")))

(define-public crate-circadian-0.7.0 (c (n "circadian") (v "0.7.0") (d (list (d (n "clap") (r "^2.27.1") (d #t) (k 0)) (d (n "glob") (r "^0.2.11") (d #t) (k 0)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)) (d (n "time") (r "^0.1.38") (d #t) (k 0)))) (h "0z5jxnkn63ihrdnygg32jngzp92g5bd793yssjsfgxb1rh5712hf")))

(define-public crate-circadian-0.8.1 (c (n "circadian") (v "0.8.1") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "time") (r "^0.1.45") (d #t) (k 0)))) (h "1h1vxaf32l93cwb1l1ypz0j8mrx70d128ixhdz0gqc1la69pw7ir")))

(define-public crate-circadian-0.8.2 (c (n "circadian") (v "0.8.2") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "time") (r "^0.1.45") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "0nxqwbg613cniabxb35w86zcf78095bbv8dqa70jhv56y01q912h")))

(define-public crate-circadian-0.8.3 (c (n "circadian") (v "0.8.3") (d (list (d (n "clap") (r "^3.2.23") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "rust-ini") (r "^0.13.0") (d #t) (k 0)) (d (n "time") (r "^0.1.45") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "06s65f778zmzdcyzvmczxlxyrf3aa4pz7jil4dh3szg2srsdhz5j")))

(define-public crate-circadian-0.8.4 (c (n "circadian") (v "0.8.4") (d (list (d (n "clap") (r "^4.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "rust-ini") (r "^0.18.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("macros" "parsing" "local-offset"))) (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "1q7zmxzqs534vklgwyj5j8wbw0fh6lqayxgl7zl261nf7gl8adj1")))

