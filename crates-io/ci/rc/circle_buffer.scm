(define-module (crates-io ci rc circle_buffer) #:use-module (crates-io))

(define-public crate-circle_buffer-0.1.0 (c (n "circle_buffer") (v "0.1.0") (h "0k5bzmnh9c4gk06qd6l17n6ic4mlyfdjn61g4h2ad0xpgck7rwm4")))

(define-public crate-circle_buffer-0.1.1 (c (n "circle_buffer") (v "0.1.1") (h "0ngndsxdkyyz5sjc7w5gfk6c0mrvi4v241y1vff1akiwwj1l0vcp")))

(define-public crate-circle_buffer-0.1.2 (c (n "circle_buffer") (v "0.1.2") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "0xlbq4dgr40lb968fh85lxxhw0jxi96nxzqy9rdps3677m5vyw6h")))

(define-public crate-circle_buffer-0.1.3 (c (n "circle_buffer") (v "0.1.3") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 0)))) (h "1jir657vqbajkr0b08g232v931x59n2dzrs0wwrfjpvap81dyyr1")))

