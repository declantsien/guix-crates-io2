(define-module (crates-io ci rc circus) #:use-module (crates-io))

(define-public crate-circus-0.1.0 (c (n "circus") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xir7zpw5529vc5wqhhb4z82s0s3xnj50jyvsllpxf9bv4cybj17")))

(define-public crate-circus-0.1.1 (c (n "circus") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1afh3h3xhz42kslx734m2k68wxqs0y2qa1b3yizygkygvd8l1jlg")))

