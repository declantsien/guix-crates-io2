(define-module (crates-io ci rc circus_test) #:use-module (crates-io))

(define-public crate-circus_test-0.0.1 (c (n "circus_test") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1aiynphkjgpg2vpspp4fqp304qrvak5kck7d7hih66vj5vmanqq0")))

(define-public crate-circus_test-0.1.0 (c (n "circus_test") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.94") (f (quote ("full"))) (d #t) (k 0)))) (h "0rb8kwwxmk5i1d68134nybd6ndp46f6l10q5yv49jbw5g2gdlp5h")))

