(define-module (crates-io ci rc circle-packer) #:use-module (crates-io))

(define-public crate-circle-packer-0.1.0 (c (n "circle-packer") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2.1") (d #t) (k 2)))) (h "14acfpkgp8cqa947k83qn1b5py7py8bf68nkc8drr574pcygrzy8")))

(define-public crate-circle-packer-0.1.1 (c (n "circle-packer") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-test") (r "^0.2.1") (d #t) (k 2)))) (h "1qv4z494jdy9lkvd7056219nqf83hhbbmcriv9y9mih9mwdndmmy")))

