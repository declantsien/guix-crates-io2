(define-module (crates-io ci rc circular-buffer) #:use-module (crates-io))

(define-public crate-circular-buffer-0.1.0 (c (n "circular-buffer") (v "0.1.0") (d (list (d (n "drop-tracker") (r "^0.1") (d #t) (k 2)))) (h "0i1mlklp7hyxc5kxa5jpa4qaz9si5jkzix7ls00y2j4qp58x5fp8") (f (quote (("use_std") ("default" "use_std"))))))

(define-public crate-circular-buffer-0.1.1 (c (n "circular-buffer") (v "0.1.1") (d (list (d (n "drop-tracker") (r "^0.1") (d #t) (k 2)))) (h "0igkgax052hh7prs36mfy809vz3w31nd1b8mb7qmqspppf1zm1sq") (f (quote (("use_std") ("unstable") ("default" "use_std")))) (r "1.59")))

(define-public crate-circular-buffer-0.1.2 (c (n "circular-buffer") (v "0.1.2") (d (list (d (n "drop-tracker") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0zh48dmmwrkxgaban54y72x96pn0bpxwa9gi29lgd93mwpgsvj0i") (f (quote (("use_std") ("unstable") ("default" "use_std")))) (r "1.59")))

(define-public crate-circular-buffer-0.1.3 (c (n "circular-buffer") (v "0.1.3") (d (list (d (n "drop-tracker") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1f4anyavc90w9pn1zj5bxq8mw6q9i5zpisysa9w9gvfklm6mrys9") (f (quote (("use_std") ("unstable") ("default" "use_std")))) (r "1.59")))

(define-public crate-circular-buffer-0.1.4 (c (n "circular-buffer") (v "0.1.4") (d (list (d (n "drop-tracker") (r "^0.1") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1hxzrza498wz8df468bbn5ldvjd3ii9y2xhchr2ap73h427d4wxn") (f (quote (("use_std") ("unstable") ("default" "use_std")))) (r "1.59")))

(define-public crate-circular-buffer-0.1.5 (c (n "circular-buffer") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "drop-tracker") (r "^0.1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "189620xpl839jrwzfnawbaj1aj5xj8haf2hdcppmgjq48dqv616q") (f (quote (("use_std") ("unstable") ("default" "use_std")))) (r "1.59")))

(define-public crate-circular-buffer-0.1.6 (c (n "circular-buffer") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "drop-tracker") (r "^0.1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0kx8jvrg7gyrpvgd60fsivx0z0hqwhbcxphb70n7m801mlv9vnmq") (f (quote (("use_std") ("unstable") ("default" "use_std")))) (r "1.59")))

(define-public crate-circular-buffer-0.1.7 (c (n "circular-buffer") (v "0.1.7") (d (list (d (n "criterion") (r "^0.3") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "drop-tracker") (r "^0.1.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "04rkyjh7iivl5ndqsvq8w4i8a1km6xlzgqzmglxw9rsa0237b66s") (f (quote (("use_std" "alloc") ("unstable") ("default" "use_std") ("alloc")))) (r "1.65")))

