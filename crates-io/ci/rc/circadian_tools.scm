(define-module (crates-io ci rc circadian_tools) #:use-module (crates-io))

(define-public crate-circadian_tools-0.1.0 (c (n "circadian_tools") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "02qqbvi8k0x6sagdr1rh6fbg7f4cq48clb5dbf2vml7w2mnj6ri1") (y #t)))

(define-public crate-circadian_tools-0.1.1 (c (n "circadian_tools") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "0f9ran0306wzlkz950j41ndsw9h59g6vbspcgr8phag7pcnaxwfp") (y #t)))

(define-public crate-circadian_tools-0.1.2 (c (n "circadian_tools") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "1j6bsd2b1jxn0fv63szyz9b4w5k6h46sgk8czg07phvp3jgmbfr1") (y #t)))

(define-public crate-circadian_tools-0.2.0 (c (n "circadian_tools") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.23") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "0vx49nbdhff0hibf20afzacfyhs59fxn49202kicis2jx5jlr4ik") (f (quote (("default" "chrono"))))))

(define-public crate-circadian_tools-0.2.1 (c (n "circadian_tools") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.23") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "1d4vbaj13147fg00w3aqik6fqbflav57n72w0bb2a0cbcdsksc5g") (f (quote (("default" "chrono"))))))

(define-public crate-circadian_tools-0.2.2 (c (n "circadian_tools") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.23") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 2)))) (h "0ns91yb52hivkwhcx31cd1qdrj9nd0iy8pi3xb6jckh8b9yy5cib") (f (quote (("default" "chrono"))))))

