(define-module (crates-io ci rc circomspect-circom-algebra) #:use-module (crates-io))

(define-public crate-circomspect-circom-algebra-2.0.0 (c (n "circomspect-circom-algebra") (v "2.0.0") (d (list (d (n "num-bigint-dig") (r "^0.6.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0id1k49wrhq39bd8scq8809h0gh3nw53xgg80i583p9c1f6fyxzk")))

(define-public crate-circomspect-circom-algebra-2.0.1 (c (n "circomspect-circom-algebra") (v "2.0.1") (d (list (d (n "num-bigint-dig") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19grd2s788v9h4lpqif1b14an4s6vby58270a15ws1f8kky5w1fc")))

(define-public crate-circomspect-circom-algebra-2.0.2 (c (n "circomspect-circom-algebra") (v "2.0.2") (d (list (d (n "num-bigint-dig") (r "^0.8") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0cq9q2zj7nyjxnhp7p7dlwh2mxbx9z7gahi9n7gpgwxyxs93il47") (r "1.65")))

