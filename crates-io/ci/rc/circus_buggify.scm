(define-module (crates-io ci rc circus_buggify) #:use-module (crates-io))

(define-public crate-circus_buggify-0.1.0 (c (n "circus_buggify") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.10.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.34") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.11") (d #t) (k 2)))) (h "17jqqzn4a508bg97s9dy11xxc2rjc9r4zqdrqzpws9pbd1wfawnk") (r "1.56")))

