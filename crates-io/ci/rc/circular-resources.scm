(define-module (crates-io ci rc circular-resources) #:use-module (crates-io))

(define-public crate-circular-resources-0.0.1 (c (n "circular-resources") (v "0.0.1") (h "04zj4fxdagn36v66r6166k9f0qii831za4606z9jq5gd4zpmsh64")))

(define-public crate-circular-resources-0.0.2 (c (n "circular-resources") (v "0.0.2") (h "162cfa7rxw9bkjgq46vka3xp3pkkmk80j9xixccsvq9wqgnkdihj")))

(define-public crate-circular-resources-0.1.0 (c (n "circular-resources") (v "0.1.0") (h "0sraa0qna73x55c2ih8w7hh0g4bafc74r9fdihqha3dar8hqpjkf")))

(define-public crate-circular-resources-0.1.1 (c (n "circular-resources") (v "0.1.1") (h "0x5s364l6k0h3cvnp88qdxw4gwidd52aksg0wl1d0q6x07lkfdx6")))

(define-public crate-circular-resources-0.2.0 (c (n "circular-resources") (v "0.2.0") (h "0jnirqvcw4aza7jm3ahwl9gq189sg1sksyhrzh7s16gkg24vc5cn")))

(define-public crate-circular-resources-0.2.1 (c (n "circular-resources") (v "0.2.1") (h "0n7nxrpz9sw8k7ba5srkknsfywc8chpk2ydz5inmp7r9qp6wzqm0")))

(define-public crate-circular-resources-0.2.2 (c (n "circular-resources") (v "0.2.2") (h "1h35dhc39rdhziikb4lcdcbyjx7602yrcpgnri2l2ymcrj9vz50n")))

(define-public crate-circular-resources-0.3.0 (c (n "circular-resources") (v "0.3.0") (h "1yfqq2hnm90hkprdcy3r72i19ixlgdvv0r9pmiw8zkrc2ff1zhnd")))

(define-public crate-circular-resources-0.3.1 (c (n "circular-resources") (v "0.3.1") (h "1fgw76236ikfg9jr2h830mq8cl3w5g488hwzf333fqqki5k8wsmm")))

(define-public crate-circular-resources-0.3.2 (c (n "circular-resources") (v "0.3.2") (h "0rbacvaizl0fbqspyj570k3pi7cmakiihlsbp4rwjvyik9h72mzg")))

(define-public crate-circular-resources-0.4.0 (c (n "circular-resources") (v "0.4.0") (h "1nzhy5aq81ijiplksvy9i3ipxpqvggx887h5id1d809jhif420q2")))

(define-public crate-circular-resources-0.4.1 (c (n "circular-resources") (v "0.4.1") (h "0g5c6g2n219zyga3d8msciw2fpbpsp9fyylxhh1q6a70kkd0spm9")))

(define-public crate-circular-resources-0.4.2 (c (n "circular-resources") (v "0.4.2") (h "0gvj4d8iggsc9d3h2972jl7697w4hv2mma343l99qmwk3wvmcbrb")))

(define-public crate-circular-resources-0.4.3 (c (n "circular-resources") (v "0.4.3") (h "0zj4y8wbg8l5h3w6jrfvv719laapzd7px8xx6v7ymid0jwacsby9")))

(define-public crate-circular-resources-0.4.4 (c (n "circular-resources") (v "0.4.4") (h "1b3qm6d6my7zrnbx88wiz63brlksrav1p35nxc4kc5bvknmjjxad")))

(define-public crate-circular-resources-0.4.5 (c (n "circular-resources") (v "0.4.5") (h "0bz97wp2y7ljkh36bj7jv531g9p7i1p06ii0m9jz5hr4fp6pxzd7")))

(define-public crate-circular-resources-0.5.0 (c (n "circular-resources") (v "0.5.0") (h "1kns94d1qajwxhz7axc3sw16lm3mvas3sj5fr5x3xxhsh3m016r0")))

(define-public crate-circular-resources-0.5.1 (c (n "circular-resources") (v "0.5.1") (h "0q3jmh6awvyxq7qp3n72frvl55hmnwhva0jws2vawfh38xmgqdpc")))

(define-public crate-circular-resources-0.6.0 (c (n "circular-resources") (v "0.6.0") (h "0dk80kvf1arlhxdp8s4jq11s9silpxv6rx56pahrl0imsw67xy4p")))

(define-public crate-circular-resources-0.6.1 (c (n "circular-resources") (v "0.6.1") (h "0wx3klccb2c0qv3jj256h77w9l7bifbbzffwk3rq9jdngipv3jvj")))

