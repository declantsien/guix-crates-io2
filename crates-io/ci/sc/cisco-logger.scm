(define-module (crates-io ci sc cisco-logger) #:use-module (crates-io))

(define-public crate-cisco-logger-0.1.0 (c (n "cisco-logger") (v "0.1.0") (d (list (d (n "paris") (r "^1.5.8") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.3") (d #t) (k 0)) (d (n "syslog_loose") (r "^0.16.0") (d #t) (k 0)))) (h "0fp8xki4864fqhxb90nwby41z8jsfqrnmf2xn12452li61968yzz")))

(define-public crate-cisco-logger-0.1.1 (c (n "cisco-logger") (v "0.1.1") (d (list (d (n "paris") (r "^1.5.8") (d #t) (k 0)) (d (n "rust-ini") (r "^0.10.3") (d #t) (k 0)) (d (n "syslog_loose") (r "^0.16.0") (d #t) (k 0)))) (h "0knngwacfppzakh1h6cjrp51166p557sbpy7ychi6q5cqndc68r6")))

