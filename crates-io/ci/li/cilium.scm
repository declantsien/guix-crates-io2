(define-module (crates-io ci li cilium) #:use-module (crates-io))

(define-public crate-cilium-0.0.0 (c (n "cilium") (v "0.0.0") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "bumpalo") (r "^3.16.0") (f (quote ("collections"))) (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "memmap2") (r "^0.9.4") (o #t) (d #t) (k 0)) (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "owning_ref") (r "^0.4.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.15") (d #t) (k 0)) (d (n "rust_search") (r "^2.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.8.0") (d #t) (k 0)))) (h "1v2lp6a5qn4h7y1hmqg1lvx6a620jhj4fyxyn4inc2zn520mzr76")))

