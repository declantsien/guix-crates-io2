(define-module (crates-io ci -t ci-testing-kbartush) #:use-module (crates-io))

(define-public crate-ci-testing-kbartush-0.1.0 (c (n "ci-testing-kbartush") (v "0.1.0") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "06yr2k1ahkqrzbvrg2a1xs35cisvf0yn7dg8c369lzzwbal1fzi3")))

(define-public crate-ci-testing-kbartush-1.5.5 (c (n "ci-testing-kbartush") (v "1.5.5") (d (list (d (n "json") (r "^0.12.0") (d #t) (k 0)) (d (n "xdr-rs-serialize") (r "^0.1.0-alpha") (d #t) (k 0)) (d (n "xdr-rs-serialize-derive") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "1bbmdll68xr09w3msmp23fp29pfrh1gyzdg952z2dvsrkdj5lm41")))

