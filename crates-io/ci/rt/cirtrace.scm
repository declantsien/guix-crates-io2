(define-module (crates-io ci rt cirtrace) #:use-module (crates-io))

(define-public crate-cirtrace-0.1.0 (c (n "cirtrace") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.3") (d #t) (k 0)) (d (n "ssh2") (r "^0.5.0") (d #t) (k 0)))) (h "10q04xdnhqkhn2ldp9aamb71m0ww4369s7vxnkn472rf2flvpp8p")))

(define-public crate-cirtrace-0.1.1 (c (n "cirtrace") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.5.0") (d #t) (k 0)))) (h "1957mqr4xp064jwf3wn8410j7c1dsgz4zvf0c40zb8nqjzppcn8q")))

(define-public crate-cirtrace-0.1.2 (c (n "cirtrace") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.5.0") (d #t) (k 0)))) (h "0n2pa5nj8gda2sjlnds6iily2caxmad7qk0f27kmpcfrgq8r0nyi")))

(define-public crate-cirtrace-0.1.5 (c (n "cirtrace") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.5.0") (d #t) (k 0)))) (h "1x97j2pzkdf2d4dwmylrhv02h7k753mfnllsj05v3ygfccxv7660")))

(define-public crate-cirtrace-0.1.6 (c (n "cirtrace") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.5.0") (d #t) (k 0)))) (h "0glvbmy6ajg0js5554yn71w4hjgsafsr10qxbay8ibb9b063n8w2")))

(define-public crate-cirtrace-0.1.7 (c (n "cirtrace") (v "0.1.7") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.5.0") (d #t) (k 0)))) (h "04x5bzzhyrhgb9rfwapn4kcni2cbvy87chpdmvcdicadbnqyfw7p")))

(define-public crate-cirtrace-0.1.8 (c (n "cirtrace") (v "0.1.8") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "ssh2") (r "^0.5.0") (d #t) (k 0)))) (h "1dpfzasahvc651qjl3nfzvy80hxrj92dyb30v7j8sm716jcvjllw")))

(define-public crate-cirtrace-0.1.10 (c (n "cirtrace") (v "0.1.10") (d (list (d (n "clap") (r "^2.33.0") (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "ssh2") (r "^0.5.0") (d #t) (k 0)))) (h "123bsnyw31660yp3rxdn29wyisayd3wpygdr4kc08r93zwx91pfx")))

