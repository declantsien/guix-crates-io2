(define-module (crates-io ci nd cinder) #:use-module (crates-io))

(define-public crate-cinder-0.0.1 (c (n "cinder") (v "0.0.1") (d (list (d (n "job_scheduler") (r "^1.0") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (d #t) (k 0)))) (h "0ypg5xkvxi4fi9p8b54v0wjsn4ij1xlqx7fl511hijvz0z522znh")))

(define-public crate-cinder-0.0.2 (c (n "cinder") (v "0.0.2") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.14.1") (d #t) (k 0)) (d (n "job_scheduler") (r "^1.0") (d #t) (k 0)) (d (n "postgres") (r "^0.15") (d #t) (k 0)) (d (n "rust-embed") (r "^5.1.0") (d #t) (k 0)))) (h "0bydd9c2y3iqxmrkd2zjjnzxf7l37p9d0lpwsrfhkyqx9ja9x0bh")))

