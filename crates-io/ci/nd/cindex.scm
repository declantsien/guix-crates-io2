(define-module (crates-io ci nd cindex) #:use-module (crates-io))

(define-public crate-cindex-0.1.0 (c (n "cindex") (v "0.1.0") (d (list (d (n "csv") (r "^1.1.6") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1b1qpszzhn487sjs5vlnh4f11fg8n5xkhbgizis6jw48ygnyjkd7")))

(define-public crate-cindex-0.1.1 (c (n "cindex") (v "0.1.1") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0jkf03p23nazdx54lv82pd7l53i5vai1xl3bbdlwxbkicl16yq0y") (f (quote (("default" "rayon"))))))

(define-public crate-cindex-0.1.2 (c (n "cindex") (v "0.1.2") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "13c8gmw4gzkvs4bss6aiamwqab2bxxcvl1r2f9mc7rpa0jq93509") (f (quote (("default" "rayon"))))))

(define-public crate-cindex-0.1.3 (c (n "cindex") (v "0.1.3") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0k31llxg2x7rlhgzpp6viw6341axjpkrvvr795lacy61rf5zbngb") (f (quote (("default" "rayon"))))))

(define-public crate-cindex-0.2.0 (c (n "cindex") (v "0.2.0") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "08zl136j1001qqd94x23r9sjrlkwvlayq9d2zanbcik04ch7y3j0") (f (quote (("default" "rayon"))))))

(define-public crate-cindex-0.3.0 (c (n "cindex") (v "0.3.0") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0byyv9y4ncnfacnjdwxzw8nlhsb8m7xiaydlawm368xxbinkrd02") (f (quote (("default" "rayon"))))))

(define-public crate-cindex-0.3.1 (c (n "cindex") (v "0.3.1") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0kqwkvwg4gilk8bnz60pkfjpyn8l2vbggpv8isyl94k04qwfwax5") (f (quote (("default" "rayon"))))))

(define-public crate-cindex-0.3.2 (c (n "cindex") (v "0.3.2") (d (list (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0bxrhj3ygbq291jgkb1hyh934q8wckl1v2553a8hvyfyn0k9819q") (f (quote (("default" "rayon"))))))

(define-public crate-cindex-0.3.3 (c (n "cindex") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "04vq8jkfdi1zg40kg8r61l2qlgpsbymzpqnmdwiqci7kdk62rhmz") (f (quote (("default" "rayon"))))))

(define-public crate-cindex-0.4.0 (c (n "cindex") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "indexmap") (r "^1.6.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1q7p3q8hn634l38ip8rn7kzfpyv28jq73wyfahkmyg50wdpq0h34") (f (quote (("default" "rayon"))))))

(define-public crate-cindex-0.4.1 (c (n "cindex") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "dcsv") (r "^0.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "1yf18qc7k12b0sxs0gbfqqzzy8blwsmw9h8k4rgrk7jvnivpncl5")))

(define-public crate-cindex-0.5.0 (c (n "cindex") (v "0.5.0") (d (list (d (n "dcsv") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "0kj2n874aax2lbjq49s88wrrmjzw1sq82q4ypvdhzdmzc9hii9hy")))

(define-public crate-cindex-0.5.1 (c (n "cindex") (v "0.5.1") (d (list (d (n "dcsv") (r "^0.2.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (d #t) (k 0)))) (h "02k83lk53p7c6sr4i56n509nfv6b9b1670lnjji7s9qs7pcw2j54")))

(define-public crate-cindex-0.5.2 (c (n "cindex") (v "0.5.2") (d (list (d (n "dcsv") (r "^0.3.3-rc2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "102v1b0xzvvc99fz22piy71s44c6wnsc5c9vlvryvrdj4yswsk5w")))

(define-public crate-cindex-0.6.0-beta.1 (c (n "cindex") (v "0.6.0-beta.1") (d (list (d (n "dcsv") (r "^0.3.4-beta.2") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)))) (h "08h4v0n8i41r0cc8p2mf9w8k6wva0g11i1lg55md88p2fwbdpjax")))

