(define-module (crates-io ci nn cinner) #:use-module (crates-io))

(define-public crate-cinner-0.1.0 (c (n "cinner") (v "0.1.0") (h "169n5rd0n25j2vvkddgc6bhzs4ij9n9qn633didhh4j8vrrscpwn")))

(define-public crate-cinner-0.1.1 (c (n "cinner") (v "0.1.1") (h "04fzqi35pz7n4ad2amnkb70x3477qranpbmfl91434jki32442g0")))

(define-public crate-cinner-0.1.2 (c (n "cinner") (v "0.1.2") (h "14ac9pwdsxgjxfg56ihw5rc6dqsvfhf8i0wg3zpha99nq96xk0mp")))

