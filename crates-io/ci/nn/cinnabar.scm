(define-module (crates-io ci nn cinnabar) #:use-module (crates-io))

(define-public crate-cinnabar-0.1.0 (c (n "cinnabar") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)))) (h "09wwkvlilbrhwk2l00ar66mpnmwd04wkk6zd6jivm0r70h0r6nzy")))

(define-public crate-cinnabar-0.2.0 (c (n "cinnabar") (v "0.2.0") (d (list (d (n "itertools") (r "^0.10.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.0.0") (d #t) (k 2)))) (h "1d787bykw3hq70d5aw0wpyz7iihn5jkxsjq3vb5vp47c0pzpxapx")))

