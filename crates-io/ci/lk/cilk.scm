(define-module (crates-io ci lk cilk) #:use-module (crates-io))

(define-public crate-cilk-0.2.0 (c (n "cilk") (v "0.2.0") (d (list (d (n "defs") (r "^0.1.0") (d #t) (k 0)) (d (n "dynasm") (r "^0.3.2") (d #t) (k 0)) (d (n "dynasmrt") (r "^0.3.1") (d #t) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "14s4jnripiaagnaycj0zjrsh9gxz568x7ybvr8hma8psfd2ikxcv") (f (quote (("x86_64") ("riscv64"))))))

(define-public crate-cilk-0.2.1 (c (n "cilk") (v "0.2.1") (d (list (d (n "defs") (r "^0.1.0") (d #t) (k 0)) (d (n "dynasm") (r "^0.3.2") (d #t) (k 0)) (d (n "dynasmrt") (r "^0.3.1") (d #t) (k 0)) (d (n "id-arena") (r "^2.2.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1kiwis2m5p43g6hy8x40agd63r8y3xci8j7lj1cyxz4lpi2h93w0") (f (quote (("x86_64") ("riscv64"))))))

