(define-module (crates-io ci ri cirilica) #:use-module (crates-io))

(define-public crate-cirilica-0.6.0 (c (n "cirilica") (v "0.6.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0nhq7lra1bbbawqy01knq511r3712v5cmymjr9sp2ni9r9s02b8n") (f (quote (("default" "console_error_panic_hook")))) (y #t)))

(define-public crate-cirilica-0.8.0 (c (n "cirilica") (v "0.8.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0dz7dlqq6xsql1zpb1cjhk0hj1rch40xpqnbpl6c7v69gw0wd91g") (f (quote (("default" "console_error_panic_hook")))) (y #t)))

(define-public crate-cirilica-0.9.0 (c (n "cirilica") (v "0.9.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0a97rygm06i1gn4p2bxag9bbwwnxa1rlyk04d3rz8bnn7di36yl9") (f (quote (("default" "console_error_panic_hook")))) (y #t)))

(define-public crate-cirilica-0.10.0 (c (n "cirilica") (v "0.10.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.63") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.13") (d #t) (k 2)) (d (n "wee_alloc") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0qjppvpms5ywkadzznx4pmp5yay2vjh2vi25aqg08ccx9y7dy48n") (f (quote (("default" "console_error_panic_hook"))))))

