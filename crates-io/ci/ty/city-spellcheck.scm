(define-module (crates-io ci ty city-spellcheck) #:use-module (crates-io))

(define-public crate-city-spellcheck-0.1.0 (c (n "city-spellcheck") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2.7") (d #t) (k 2)) (d (n "distance") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "redis") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "sift4") (r "^0.1.2") (d #t) (k 0)))) (h "1j3f8ggwz4szz8m4aaf8yyzwzfk54k9iv8dbffwwchkprs328vgp")))

(define-public crate-city-spellcheck-0.1.1 (c (n "city-spellcheck") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2.7") (d #t) (k 2)) (d (n "distance") (r "^0.4.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "redis") (r "^0.9.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.84") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.84") (d #t) (k 0)) (d (n "sift4") (r "^0.1.2") (d #t) (k 0)))) (h "17dzkzmj8k64ahkpqrlzyflj0msjbvinrv3mflysg0brp51fbb0j")))

