(define-module (crates-io ci ty cityhash) #:use-module (crates-io))

(define-public crate-cityhash-0.1.0 (c (n "cityhash") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bbv3sx8kifdjyy800mhwb4m3q470j15d3flrp6m7xcj5qvrgw55")))

(define-public crate-cityhash-0.1.1 (c (n "cityhash") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "clap") (r "^2.33.0") (f (quote ("yaml" "wrap_help"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nk33im4wsgqdw5gk6lzm2f36py7a16lb3xw122fm533bcpbiv67")))

