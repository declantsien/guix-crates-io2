(define-module (crates-io ci ty city_time_zone_sqlite) #:use-module (crates-io))

(define-public crate-city_time_zone_sqlite-0.1.0 (c (n "city_time_zone_sqlite") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "diesel") (r "^1.1.0") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "unidecode") (r "^0.3.0") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "015hi9a4nsl51x9q3r746hlzpyiwxd16xnq9b2kbiai4v1w0x9my")))

