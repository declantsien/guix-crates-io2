(define-module (crates-io ci ty cityhash-rs) #:use-module (crates-io))

(define-public crate-cityhash-rs-1.0.0 (c (n "cityhash-rs") (v "1.0.0") (h "0pd3jxprwvpklpnq9d24assa1flfcblf5xch3kz8g5ni6yl2xcs7")))

(define-public crate-cityhash-rs-1.0.1 (c (n "cityhash-rs") (v "1.0.1") (h "0kf31pjx5is18sf6ffyh5hs0jg3hgr5jn0hkpn23n0236s8ik9wk")))

