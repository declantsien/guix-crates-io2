(define-module (crates-io ci ty cityhasher) #:use-module (crates-io))

(define-public crate-cityhasher-0.1.0 (c (n "cityhasher") (v "0.1.0") (h "14h9qhgv82pvrwc7154hn4dpyl9c4cqfjxxfri642hjgx74kgayf") (f (quote (("std") ("disable-bounds-checking") ("default" "disable-bounds-checking" "std")))) (r "1.60.0")))

