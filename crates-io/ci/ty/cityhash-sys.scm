(define-module (crates-io ci ty cityhash-sys) #:use-module (crates-io))

(define-public crate-cityhash-sys-0.1.0 (c (n "cityhash-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0yzkn06czs5zda6sw4cjhrhjx9k9h4xlxnw4na8qngphi1xik96p") (y #t)))

(define-public crate-cityhash-sys-0.2.0 (c (n "cityhash-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "132yfbzc8667kbv7bfw0wxxq2g8lwiqwxdnyjcjzx3p32y4vkby8") (y #t)))

(define-public crate-cityhash-sys-0.3.0 (c (n "cityhash-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "18v1mqvwg5mj3wyh5vf49qwgwisimg27sivzbfsqjw0xd6sran37") (y #t)))

(define-public crate-cityhash-sys-0.4.0 (c (n "cityhash-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0h5v8217ymr9c6bbidj5xr4f9lh6pjbvwcjvd7lf226ds5q0cqkp") (y #t)))

(define-public crate-cityhash-sys-0.5.0 (c (n "cityhash-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1vcdlvcc2pmn5p97sib2m9dj8ljqkdjg5xpd96a2zknha0q13bvp") (y #t)))

(define-public crate-cityhash-sys-1.0.0 (c (n "cityhash-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "01p96aspml0wggvvzwl0yravncpy2lhyvjrxqqxrgd052bpi867c") (y #t)))

(define-public crate-cityhash-sys-1.0.1 (c (n "cityhash-sys") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0h9lqsq6028208f507bag6ki2z4rn045z5nrm2ys8bh5150z1gq8")))

(define-public crate-cityhash-sys-1.0.2 (c (n "cityhash-sys") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1zhclzgncc2qpl5wfaqy65fg5df6dfasga3lblc5f95nryjvw4dj")))

(define-public crate-cityhash-sys-1.0.3 (c (n "cityhash-sys") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15x4kgghcwfxvl4zch5jln2ml447qin6a31ii7n09l93bm261zxq") (y #t)))

(define-public crate-cityhash-sys-1.0.4 (c (n "cityhash-sys") (v "1.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "025gvxxhi14lnzl8mppa9iqiw1fjkp4bl5k9r6l4havmpmghq5al") (y #t)))

(define-public crate-cityhash-sys-1.0.5 (c (n "cityhash-sys") (v "1.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "015kxy64scgwi2bzrqxd2cllp3ksmd3lffv1wgpdvqbds1q202d5")))

