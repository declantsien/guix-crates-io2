(define-module (crates-io ci se ciseaux_client) #:use-module (crates-io))

(define-public crate-ciseaux_client-0.1.0 (c (n "ciseaux_client") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12") (d #t) (k 0)) (d (n "redis") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 0)))) (h "0f62v76yyzpjymp55vq0m54fxfy3yxrs8i9jg6glya0zx47d8s3f")))

(define-public crate-ciseaux_client-0.1.1 (c (n "ciseaux_client") (v "0.1.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12") (d #t) (k 0)) (d (n "redis") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync"))) (d #t) (k 0)))) (h "1f7zjvr3yk6s2hl0r7c80cz7lhc3x2di6scl8i03yxlmng2snvll")))

(define-public crate-ciseaux_client-0.2.0 (c (n "ciseaux_client") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12") (d #t) (k 0)) (d (n "redis") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "time"))) (d #t) (k 0)))) (h "1z427s2dvrc3gafs8djvkd1y4dl5vd9jnqkzwww52qz06pab0zzy")))

(define-public crate-ciseaux_client-0.2.1 (c (n "ciseaux_client") (v "0.2.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.12") (d #t) (k 0)) (d (n "redis") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "time"))) (d #t) (k 0)))) (h "09i8grn5jkmswp8sivir86s15f9dmcncwha6xw9vpfih2wmgx458")))

(define-public crate-ciseaux_client-0.3.0 (c (n "ciseaux_client") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "redis") (r "^0.15") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "macros"))) (d #t) (k 2)))) (h "1p0xib7mhy3dl95gizxwvvx371p615dr1rgj9l7igigrxgmwxwbv")))

(define-public crate-ciseaux_client-0.3.1 (c (n "ciseaux_client") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "redis") (r "^0.16") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("sync" "time"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-core" "macros"))) (d #t) (k 2)))) (h "02n5ffvjkg7qb5d58sxz3bix7c4w6v3cqfw2xmq5cvh1hay3r8mn")))

