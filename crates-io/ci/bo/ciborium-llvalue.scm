(define-module (crates-io ci bo ciborium-llvalue) #:use-module (crates-io))

(define-public crate-ciborium-llvalue-0.1.0 (c (n "ciborium-llvalue") (v "0.1.0") (d (list (d (n "ciborium-iovalue") (r "^0.1.0") (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "19l7sgma4wbnswmcdrkgdv3hy5rw63m4rmjpmm7qqhh2n4lbl3rk") (f (quote (("std" "alloc") ("alloc")))) (y #t)))

