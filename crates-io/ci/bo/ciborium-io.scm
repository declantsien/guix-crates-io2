(define-module (crates-io ci bo ciborium-io) #:use-module (crates-io))

(define-public crate-ciborium-io-0.2.0 (c (n "ciborium-io") (v "0.2.0") (h "0sdkk7l7pqi2nsbm9c6g8im1gb1qdd83l25ja9xwhg07mx9yfv9l") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-ciborium-io-0.2.1 (c (n "ciborium-io") (v "0.2.1") (h "0mi6ci27lpz3azksxrvgzl9jc4a3dfr20pjx7y2nkcrjalbikyfd") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-ciborium-io-0.2.2 (c (n "ciborium-io") (v "0.2.2") (h "0my7s5g24hvp1rs1zd1cxapz94inrvqpdf1rslrvxj8618gfmbq5") (f (quote (("std" "alloc") ("alloc")))) (r "1.58")))

