(define-module (crates-io ci bo ciborium-ll) #:use-module (crates-io))

(define-public crate-ciborium-ll-0.2.0 (c (n "ciborium-ll") (v "0.2.0") (d (list (d (n "ciborium-io") (r "^0.2.0") (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "06ygqh33k3hp9r9mma43gf189b6cyq62clk65f4w1q54nni30c11") (f (quote (("std" "alloc") ("alloc"))))))

(define-public crate-ciborium-ll-0.2.1 (c (n "ciborium-ll") (v "0.2.1") (d (list (d (n "ciborium-io") (r "^0.2.1") (d #t) (k 0)) (d (n "half") (r "^1.6") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "0az2vabamfk75m74ylgf6nzqgqgma5yf25bc1ripfg09ri7a5yny") (f (quote (("std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-ciborium-ll-0.2.2 (c (n "ciborium-ll") (v "0.2.2") (d (list (d (n "ciborium-io") (r "^0.2.2") (d #t) (k 0)) (d (n "half") (r "^2.2") (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)))) (h "1n8g4j5rwkfs3rzfi6g1p7ngmz6m5yxsksryzf5k72ll7mjknrjp") (f (quote (("std" "alloc" "half/std") ("alloc")))) (r "1.58")))

