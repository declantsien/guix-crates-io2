(define-module (crates-io ci bo ciboriumvalue) #:use-module (crates-io))

(define-public crate-ciboriumvalue-0.1.0 (c (n "ciboriumvalue") (v "0.1.0") (d (list (d (n "ciborium-iovalue") (r "^0.1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "ciborium-llvalue") (r "^0.1.0") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rstest") (r "^0.6") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "serde_bytes") (r "^0.11") (d #t) (k 2)))) (h "0j0hcgjnqyf5g4slnz4fsfq8xqgh9klcq5l0syvfw51m5qva9lw6") (f (quote (("std" "ciborium-iovalue/std" "serde/std") ("default" "std")))) (y #t)))

