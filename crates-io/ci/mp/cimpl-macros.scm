(define-module (crates-io ci mp cimpl-macros) #:use-module (crates-io))

(define-public crate-cimpl-macros-0.1.0 (c (n "cimpl-macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0x0j3bpa6nhh4sn6i2awb5ppf6q99w25c3gw6a5zgx6a2gwvy7ip") (y #t)))

(define-public crate-cimpl-macros-0.2.0 (c (n "cimpl-macros") (v "0.2.0") (h "1rws02n0j3j0z8cs2b3l81433y0w9saq87fdvlv1kdsfqiis4r2n") (y #t)))

