(define-module (crates-io ci mp cimpl) #:use-module (crates-io))

(define-public crate-cimpl-0.1.0 (c (n "cimpl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cimpl-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vs2q1mr9gh4drscgs6bg7jp8llmx32zx6zzpg01sbm3m7iip2ps") (f (quote (("default" "pretty")))) (y #t) (s 2) (e (quote (("pretty" "dep:prettyplease"))))))

(define-public crate-cimpl-0.1.1 (c (n "cimpl") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "cimpl-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "prettyplease") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.70") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.41") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0n93wz339gcmjv898wrhqfkkkr98rnvd9w7zi6lq0f3w7lvahqf5") (f (quote (("default" "pretty")))) (y #t) (s 2) (e (quote (("pretty" "dep:prettyplease"))))))

(define-public crate-cimpl-0.2.0 (c (n "cimpl") (v "0.2.0") (h "0nkyglxq077m4ky6i2nkfrlmb9gm5sx007d1wg38k3nis8y8g1zd") (y #t)))

