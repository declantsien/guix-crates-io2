(define-module (crates-io ci oq cioqlsbmiv3xeugi6xqrx411qeizwwadh7c) #:use-module (crates-io))

(define-public crate-cioqLsBmIV3xEUGI6XQRx411QEIZwwaDh7c-0.1.9 (c (n "cioqLsBmIV3xEUGI6XQRx411QEIZwwaDh7c") (v "0.1.9") (d (list (d (n "anchor-lang") (r "^0.24.2") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.24.2") (d #t) (k 0)) (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.7.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "030c3bg91biwin409vrnszaxbg6613xmfvq8yh2s08r62fc35y87")))

