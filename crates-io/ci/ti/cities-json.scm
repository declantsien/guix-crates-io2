(define-module (crates-io ci ti cities-json) #:use-module (crates-io))

(define-public crate-cities-json-0.1.1 (c (n "cities-json") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1afsb9prax7q9ai1bgy68ddz7cpa2bk45m7rb20l11b5m9l0h3ra")))

(define-public crate-cities-json-0.3.1 (c (n "cities-json") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sn71y1n66fwn56rgyg365npbnmsv4pw8wkvcfynswjip2n83jkf")))

(define-public crate-cities-json-0.3.2 (c (n "cities-json") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jgs54fgjci1g8ssrnd4rbi4p3qhmb4y2s0b5cywclhyk9npb21j")))

(define-public crate-cities-json-0.3.3 (c (n "cities-json") (v "0.3.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1c114aj4c3mbilb9lppy4agc4ic3kb2p90qgifl7dir9p8g8ql52")))

(define-public crate-cities-json-0.3.4 (c (n "cities-json") (v "0.3.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w6f55al6vnw1f8xh86lhvql0fg492fazb0mznbl1zjzkca57pgw")))

(define-public crate-cities-json-0.3.5 (c (n "cities-json") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ahild6hhhpdjhmzbrgy2dix4wvb6y70n581x677bbrg1jdh6pwf")))

(define-public crate-cities-json-0.3.6 (c (n "cities-json") (v "0.3.6") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v9xgxhnv6swf8bf9f84v4il4q4y851hlh2dz2mhc6jk1ifqldms")))

(define-public crate-cities-json-0.3.7 (c (n "cities-json") (v "0.3.7") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1lkr1pqnf0h0vjanm5rhzq1b2dwcis0jxligvj0bkqnilzcqjyjg")))

(define-public crate-cities-json-0.3.8 (c (n "cities-json") (v "0.3.8") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "00mvyy0is0pl4bvdzhwdlgd6lfzis1j75ad6hha026xc9h145d08")))

(define-public crate-cities-json-0.3.9 (c (n "cities-json") (v "0.3.9") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1izas1z7s9733ppb2ry7s6sd0m5gdnxqphlvz422fkk0dig6gkmh")))

(define-public crate-cities-json-0.4.0 (c (n "cities-json") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13w9kxh1mqwphmxq3p27pqdpspc1kqpixv7rb9ca1786m2302wnl")))

(define-public crate-cities-json-0.5.0 (c (n "cities-json") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0cdxfh5lfq1wzjw47w1x9fvz9llplsbhys12w6kwcachkwc748z5")))

(define-public crate-cities-json-0.5.1 (c (n "cities-json") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xglgnb9cszzvjr86xjn70576yj4iry01lvxwp9yp08amms6af94")))

(define-public crate-cities-json-0.5.3 (c (n "cities-json") (v "0.5.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "094jkn91k0kha7d8k5f5gr33ylzxll9hfgl7lnh4li7y7wkcc7lp")))

(define-public crate-cities-json-0.5.4 (c (n "cities-json") (v "0.5.4") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ia51cq3vl8yj29pkq1fa395v2av2zzwmdal03c3wprmjiqvlxi1")))

(define-public crate-cities-json-0.5.5 (c (n "cities-json") (v "0.5.5") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0rhybmr4h4zh5f25ga9aq6gny5ywarklybjxsjdiy81yq979k0mi")))

