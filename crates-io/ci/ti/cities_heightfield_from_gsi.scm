(define-module (crates-io ci ti cities_heightfield_from_gsi) #:use-module (crates-io))

(define-public crate-cities_heightfield_from_gsi-1.0.0 (c (n "cities_heightfield_from_gsi") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "resize") (r "^0.3.0") (d #t) (k 0)))) (h "1frj46qagxvp24ycpsn0a59wh2w0jbb6bdwgsym23kly3jjx12mb")))

(define-public crate-cities_heightfield_from_gsi-1.0.1 (c (n "cities_heightfield_from_gsi") (v "1.0.1") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "resize") (r "^0.3.0") (d #t) (k 0)))) (h "102rbzgkgxwipxjli1hkfz5j961f82hg5zlqr2bhqssa048m1z03")))

(define-public crate-cities_heightfield_from_gsi-1.1.0 (c (n "cities_heightfield_from_gsi") (v "1.1.0") (d (list (d (n "getopts") (r "^0.2.18") (d #t) (k 0)) (d (n "image") (r "^0.19.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "resize") (r "^0.3.0") (d #t) (k 0)))) (h "1gf4r340ls789ihcsbnhaavb6yj5q5gzc61ndlsv33sdq1cn4r1q")))

