(define-module (crates-io ci ti cities) #:use-module (crates-io))

(define-public crate-cities-0.1.0 (c (n "cities") (v "0.1.0") (h "0yhsmbdkkwqzgxrk1pjln2jy4hwhmywvkpm0fzgbmd8fca8fs9gh")))

(define-public crate-cities-0.2.0 (c (n "cities") (v "0.2.0") (h "1hp4hrhzdldqzx02w7vqzl9fh5ic92374bpvsg1a8vs32lhyr2zf")))

