(define-module (crates-io ci ti citi) #:use-module (crates-io))

(define-public crate-citi-0.1.0 (c (n "citi") (v "0.1.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1hacpn3mc6iz0d0n5hickl4bpgapp3m8bfbwsny1fkh48v1mvklr")))

(define-public crate-citi-0.2.0 (c (n "citi") (v "0.2.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0i33cqilysy39j4jrjch586plz5zh3wqjxnk9vrg66anzwhynyfg")))

