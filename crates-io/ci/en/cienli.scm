(define-module (crates-io ci en cienli) #:use-module (crates-io))

(define-public crate-cienli-0.1.0 (c (n "cienli") (v "0.1.0") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1w20n0v2an3bdxl48737j7dgc6p05538ahsz32v4i3pjzm2ysqwr") (y #t)))

(define-public crate-cienli-0.1.1 (c (n "cienli") (v "0.1.1") (d (list (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "1nslbimrrk8mq4fnwlcb44n90fxl0x54qxvhzw0hxblfp1d6s8q0")))

(define-public crate-cienli-0.2.0 (c (n "cienli") (v "0.2.0") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "17j6waayl0m3v9kc7vcq2rv29lz0h64w187vqlx1fg64j476ybip")))

(define-public crate-cienli-0.3.0 (c (n "cienli") (v "0.3.0") (d (list (d (n "num-integer") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)))) (h "0wr8cq1ah55s3lpqai43ccz6zr1ak7jx625zpyv2f1h922p5xqbm")))

(define-public crate-cienli-0.3.1 (c (n "cienli") (v "0.3.1") (d (list (d (n "num-integer") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.5.4") (o #t) (d #t) (k 0)))) (h "1brzwzpybhhjnp211xa777bc387m9gx5dcxnrp6wzi0kbvjdzfi7") (f (quote (("xor") ("vigenere") ("scytale") ("rot") ("polybius_square") ("default" "affine" "atbash" "bacon" "caesar" "polybius_square" "rot" "scytale" "vigenere" "xor") ("caesar") ("atbash")))) (s 2) (e (quote (("bacon" "dep:regex") ("affine" "dep:num-integer"))))))

