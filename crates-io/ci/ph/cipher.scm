(define-module (crates-io ci ph cipher) #:use-module (crates-io))

(define-public crate-cipher-0.1.0 (c (n "cipher") (v "0.1.0") (h "07i950ysmx9jbwmk1fan1cb84klbn7fwiq3sl89cpc7asdpbdzrw") (y #t)))

(define-public crate-cipher-0.1.1 (c (n "cipher") (v "0.1.1") (h "1c64sgxw96fg11q15mcf60lmq5v1pizgmskz2zd7gxwc9258h3hy") (y #t)))

(define-public crate-cipher-0.2.0 (c (n "cipher") (v "0.2.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "13spiy5d8h175js83na70bw4j2fvf818r8mjiy2n0z17lx3rx5ag") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-cipher-0.2.1 (c (n "cipher") (v "0.2.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0nfnal8bkp0rvsscs3slqrf45rx26qf9nqspaarh50aqwm59bxx1") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-cipher-0.2.2 (c (n "cipher") (v "0.2.2") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1hvzmcy1lf2dfxm97zvfx8cyc3d3wcnn5v0v1aq2c03g8vbr5ym9") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-cipher-0.2.3 (c (n "cipher") (v "0.2.3") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0ldvgi0snkb7s5pdms2jig6b6bnr9p48q9g3g92ps5ifzvksa7c9") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-cipher-0.2.4 (c (n "cipher") (v "0.2.4") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0ijfs9c1hbb2cpd1s61xska4x5mln33picsbi9qn9cbfyk77yy86") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-cipher-0.2.5 (c (n "cipher") (v "0.2.5") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "00b8imbmdg7zdrbaczlivmdfdy09xldg95wl4iijl15xgjcfgy0j") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-cipher-0.3.0-pre (c (n "cipher") (v "0.3.0-pre") (d (list (d (n "blobby") (r ">=0.3.0, <0.4.0") (o #t) (d #t) (k 0)) (d (n "generic-array") (r ">=0.14.0, <0.15.0") (d #t) (k 0)))) (h "1ghb1ji4rzwnri2m0wfq13782lybf2l3rz4221j78vlcyba8p122") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-cipher-0.3.0-pre.2 (c (n "cipher") (v "0.3.0-pre.2") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1vxzgyci001d3sybdx4km0j9v7cfsxcj0jdrj3p55ib1rb6i7vpd") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-cipher-0.3.0-pre.3 (c (n "cipher") (v "0.3.0-pre.3") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0ssckvm6cy69gz989hx0inbsqma3mb4rdyxp3g0sv5qlpbwylrz0") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-cipher-0.3.0-pre.4 (c (n "cipher") (v "0.3.0-pre.4") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "0scy8h6qy7krn7vk9yypjqi6gibdrrgdsrh3lhmy3v2i8b1m7nz1") (f (quote (("std") ("dev" "blobby")))) (y #t)))

(define-public crate-cipher-0.3.0-pre.5 (c (n "cipher") (v "0.3.0-pre.5") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "block-buffer") (r "=0.10.0-pre.4") (f (quote ("block-padding"))) (o #t) (d #t) (k 0)) (d (n "crypto-common") (r "=0.1.0-pre") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (o #t) (d #t) (k 0)))) (h "0828ff8d9r9xknb8n1hbr107npca9liyi4bzfaj7cpl16hzyivgb") (f (quote (("std" "crypto-common/std") ("mode_wrapper" "block-buffer") ("dev" "blobby") ("default" "mode_wrapper")))) (y #t)))

(define-public crate-cipher-0.3.0 (c (n "cipher") (v "0.3.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)))) (h "1dyzsv0c84rgz98d5glnhsz4320wl24x3bq511vnyf0mxir21rby") (f (quote (("std") ("dev" "blobby"))))))

(define-public crate-cipher-0.4.0 (c (n "cipher") (v "0.4.0") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crypto-common") (r "^0.1.2") (d #t) (k 0)) (d (n "inout") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (o #t) (k 0)))) (h "1q61gaqmm8na9m0v506ri2ighs0h1bbhzny93crirhw2pv4yiwx4") (f (quote (("std" "crypto-common/std" "inout/std") ("rand_core" "crypto-common/rand_core") ("dev" "blobby") ("block-padding" "inout/block-padding")))) (r "1.56")))

(define-public crate-cipher-0.4.1 (c (n "cipher") (v "0.4.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crypto-common") (r "^0.1.3") (d #t) (k 0)) (d (n "inout") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (o #t) (k 0)))) (h "08ya4xipidsljd3ip52fh6b89gx99bxmcs87kcnnp5wlrqd7x7lg") (f (quote (("std" "alloc" "crypto-common/std" "inout/std") ("rand_core" "crypto-common/rand_core") ("dev" "blobby") ("default" "alloc") ("block-padding" "inout/block-padding") ("alloc")))) (y #t) (r "1.56")))

(define-public crate-cipher-0.4.2 (c (n "cipher") (v "0.4.2") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crypto-common") (r "^0.1.3") (d #t) (k 0)) (d (n "inout") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (o #t) (k 0)))) (h "134dv3vv474m7y7wgjlxxvkd5q2hbaf5k73y4wxbfmqmvfcd4nq3") (f (quote (("std" "alloc" "crypto-common/std" "inout/std") ("rand_core" "crypto-common/rand_core") ("dev" "blobby") ("default" "alloc") ("block-padding" "inout/block-padding") ("alloc")))) (y #t) (r "1.56")))

(define-public crate-cipher-0.4.3 (c (n "cipher") (v "0.4.3") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crypto-common") (r "^0.1.3") (d #t) (k 0)) (d (n "inout") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (o #t) (k 0)))) (h "17mmmqaalirdx7bpdhrgzp1sd392zm08mjrr24cjr57pz1q351yi") (f (quote (("std" "alloc" "crypto-common/std" "inout/std") ("rand_core" "crypto-common/rand_core") ("dev" "blobby") ("block-padding" "inout/block-padding") ("alloc")))) (r "1.56")))

(define-public crate-cipher-0.4.4 (c (n "cipher") (v "0.4.4") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crypto-common") (r "^0.1.6") (d #t) (k 0)) (d (n "inout") (r "^0.1") (d #t) (k 0)) (d (n "zeroize") (r "^1.5") (o #t) (k 0)))) (h "1b9x9agg67xq5nq879z66ni4l08m6m3hqcshk37d4is4ysd3ngvp") (f (quote (("std" "alloc" "crypto-common/std" "inout/std") ("rand_core" "crypto-common/rand_core") ("dev" "blobby") ("block-padding" "inout/block-padding") ("alloc")))) (r "1.56")))

(define-public crate-cipher-0.5.0-pre.1 (c (n "cipher") (v "0.5.0-pre.1") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crypto-common") (r "=0.2.0-pre.3") (d #t) (k 0)) (d (n "inout") (r "=0.2.0-pre.3") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (k 0)))) (h "1kghmr1jzjanqmvvaqyb10bvdm5jzv32h9yii6dknjdprsi3iqqm") (f (quote (("std" "alloc" "crypto-common/std" "inout/std") ("rand_core" "crypto-common/rand_core") ("dev" "blobby") ("block-padding" "inout/block-padding") ("alloc")))) (r "1.65")))

(define-public crate-cipher-0.5.0-pre.2 (c (n "cipher") (v "0.5.0-pre.2") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crypto-common") (r "=0.2.0-pre.4") (d #t) (k 0)) (d (n "inout") (r "=0.2.0-pre.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (k 0)))) (h "167yqy35jqsnb64vm6p10vkb15hafdiigrwk85h1ih77s9hrwqa0") (f (quote (("std" "alloc" "crypto-common/std" "inout/std") ("rand_core" "crypto-common/rand_core") ("dev" "blobby") ("block-padding" "inout/block-padding") ("alloc")))) (r "1.65")))

(define-public crate-cipher-0.5.0-pre.3 (c (n "cipher") (v "0.5.0-pre.3") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crypto-common") (r "=0.2.0-pre.5") (d #t) (k 0)) (d (n "inout") (r "=0.2.0-pre.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (k 0)))) (h "0imfwyyg79if655vy9niwiamaa8m9whjq3w0086rm38k14m91p2f") (f (quote (("std" "alloc" "crypto-common/std" "inout/std") ("rand_core" "crypto-common/rand_core") ("dev" "blobby") ("block-padding" "inout/block-padding") ("alloc")))) (r "1.65")))

(define-public crate-cipher-0.5.0-pre.4 (c (n "cipher") (v "0.5.0-pre.4") (d (list (d (n "blobby") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "crypto-common") (r "=0.2.0-pre.5") (d #t) (k 0)) (d (n "inout") (r "=0.2.0-pre.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.7") (o #t) (k 0)))) (h "07vczczcvfi2yidl1dmsv2chz93nfn0pra0qi0q0xkffhn3skyw4") (f (quote (("std" "alloc" "crypto-common/std" "inout/std") ("rand_core" "crypto-common/rand_core") ("dev" "blobby") ("block-padding" "inout/block-padding") ("alloc")))) (r "1.65")))

