(define-module (crates-io ci ph cipher_magma) #:use-module (crates-io))

(define-public crate-cipher_magma-0.2.2 (c (n "cipher_magma") (v "0.2.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1lq0pl0haic5mym676fp0ykjsya3hdjks6ksry32nyz99yybplb3")))

(define-public crate-cipher_magma-0.2.3 (c (n "cipher_magma") (v "0.2.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0bf6xaj1q03a89vzddrivf5byihvw0d70j6p36v8408s9ndwfp1p")))

(define-public crate-cipher_magma-0.2.4 (c (n "cipher_magma") (v "0.2.4") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0p5dm3x0m98g795ay864ch906iscgy91dh6s2d2m751wjj8yvmbr")))

(define-public crate-cipher_magma-0.3.0 (c (n "cipher_magma") (v "0.3.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "12rh1dn4yd5xkqla1v6qzhihblxga9cijmwakprlfi5j6lb4jpln")))

(define-public crate-cipher_magma-0.3.1 (c (n "cipher_magma") (v "0.3.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1f1prrqlyycrm9wak8raiiakvrkjlvmjd1kfv1dww405i4sjihij")))

(define-public crate-cipher_magma-0.4.0 (c (n "cipher_magma") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0sbjw9ikmil5qrwz82z9nq9xb8dyyaz5hs5v6inb1dj4b6k2f5p1")))

(define-public crate-cipher_magma-0.4.1 (c (n "cipher_magma") (v "0.4.1") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "1rcjf5cjnkfdf0fjkksmdl4gs5agan0xx9jga3pr5896fspya5bj")))

(define-public crate-cipher_magma-0.4.2 (c (n "cipher_magma") (v "0.4.2") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "18dl1khfpx1x0r7wwnz5d0gakk342lxm85acb84qrbzqas506dkv")))

(define-public crate-cipher_magma-0.4.3 (c (n "cipher_magma") (v "0.4.3") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)))) (h "0nrzhc3fi1nssm2m1039r65mmkps330r0rhw73q64jdcr110r1i6")))

(define-public crate-cipher_magma-0.5.0 (c (n "cipher_magma") (v "0.5.0") (h "07jjzww15ccl7xbpi8i1w24mn79h4mzc73p0ypb6hjqspbivz0v7")))

(define-public crate-cipher_magma-0.5.1 (c (n "cipher_magma") (v "0.5.1") (h "150mlnkcjxyazazvx5fg5xmf8h67121mzwcsnnd83via3d2x32rr")))

(define-public crate-cipher_magma-0.5.2 (c (n "cipher_magma") (v "0.5.2") (h "05q8q70sbbb46s7fdkc5iizjb6flxbf9imxxc5h2ir7yf59x5pkn")))

(define-public crate-cipher_magma-0.6.0 (c (n "cipher_magma") (v "0.6.0") (d (list (d (n "crypto_vectors") (r "^0.1") (d #t) (k 2)))) (h "00s26f5l5z431c62iv0v62pr576pv6z6lvrxjydh7hh235f2xshf")))

(define-public crate-cipher_magma-0.6.1 (c (n "cipher_magma") (v "0.6.1") (d (list (d (n "crypto_vectors") (r "^0.1") (d #t) (k 2)))) (h "0jrjrzv9w5n0p0vzqp7qbzs1p013hpg06lih9fsprr6ld93jkq5v")))

(define-public crate-cipher_magma-0.6.2 (c (n "cipher_magma") (v "0.6.2") (d (list (d (n "crypto_vectors") (r "^0.1") (d #t) (k 2)))) (h "1xn2lq6qxr17bdy3spg507swksmlmplpbvyfw6gnnh0gnr3kl2cx")))

(define-public crate-cipher_magma-0.6.3 (c (n "cipher_magma") (v "0.6.3") (d (list (d (n "crypto_vectors") (r "^0.1") (d #t) (k 2)))) (h "1k2965x5c4778mfd3lvm25ci8zc5pqzf1yd67xi9k197qhh4kdvl")))

(define-public crate-cipher_magma-0.7.0 (c (n "cipher_magma") (v "0.7.0") (d (list (d (n "crypto_vectors") (r "^0.1") (d #t) (k 2)))) (h "1iigb0v4ydbx4lvwzkg1j6fn8fjwqnha75iiqj57fspj5w1jih17")))

(define-public crate-cipher_magma-0.7.1 (c (n "cipher_magma") (v "0.7.1") (d (list (d (n "crypto_vectors") (r "^0.1") (d #t) (k 2)))) (h "0l0k9wjp3yhzpjv1bpa9bif4clkyn24z4rx4wmj6f9h6gi997xyg")))

(define-public crate-cipher_magma-0.8.0 (c (n "cipher_magma") (v "0.8.0") (d (list (d (n "crypto_vectors") (r "^0.1") (d #t) (k 2)))) (h "0cpkspvc95vgsys343rrmwir0yqp2b05iymfh4dxpqpcarsm7ywx")))

(define-public crate-cipher_magma-0.8.1 (c (n "cipher_magma") (v "0.8.1") (d (list (d (n "crypto_vectors") (r "^0.1") (d #t) (k 2)))) (h "18y42k7p1wnb94h53va2ksksfy397km80g39zlv0q48ik8hfr7li")))

(define-public crate-cipher_magma-0.8.2 (c (n "cipher_magma") (v "0.8.2") (d (list (d (n "crypto_vectors") (r "^0.1") (d #t) (k 2)))) (h "1qxf2sq4dlf8z4g02bwvyfiz6d7siyrwbdjcfrw1n7l44li7vwm7")))

(define-public crate-cipher_magma-0.8.3 (c (n "cipher_magma") (v "0.8.3") (d (list (d (n "crypto_vectors") (r "^0.1") (d #t) (k 2)))) (h "0qsjkkgk95mq52nrw3pddqc2d0amarfzp69wxliaj06fjf11lprf")))

