(define-module (crates-io ci ph cipherstash-grpc-wasm) #:use-module (crates-io))

(define-public crate-cipherstash-grpc-wasm-0.1.0-beta.0 (c (n "cipherstash-grpc-wasm") (v "0.1.0-beta.0") (d (list (d (n "grpc-web-client") (r "^0.1.2") (f (quote ("web-worker"))) (d #t) (k 0) (p "cipherstash-grpc-web-client")) (d (n "js-sys") (r "^0.3") (k 0)) (d (n "prost") (r "^0.9") (k 0)) (d (n "tonic") (r "^0.6") (f (quote ("codegen" "prost"))) (k 0)) (d (n "tonic-build") (r "^0.6") (f (quote ("prost"))) (k 1)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3") (k 0)))) (h "1bvd2d1h0wgp79lhqsv0m9jqifr6nnf7sm8l7dcmwd4bw6wxpdgb") (y #t)))

