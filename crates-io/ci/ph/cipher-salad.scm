(define-module (crates-io ci ph cipher-salad) #:use-module (crates-io))

(define-public crate-cipher-salad-0.1.0 (c (n "cipher-salad") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.9") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.9") (d #t) (k 2)) (d (n "unicode-segmentation") (r "^1.7.1") (d #t) (k 0)))) (h "16zz5l31nhfs614pgk4n59xi8qfsmk915d2fpifc11im6kjzbz5h")))

