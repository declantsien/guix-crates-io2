(define-module (crates-io ci ph cipher_password) #:use-module (crates-io))

(define-public crate-cipher_password-0.1.0 (c (n "cipher_password") (v "0.1.0") (d (list (d (n "clap") (r "^2.32.0") (d #t) (k 0)) (d (n "pwhash") (r "^0.3.0") (d #t) (k 0)))) (h "1jafq6kc4afg91k1biwbcs22mg1alcgmys790bfz129a0zfia31q")))

