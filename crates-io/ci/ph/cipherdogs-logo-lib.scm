(define-module (crates-io ci ph cipherdogs-logo-lib) #:use-module (crates-io))

(define-public crate-cipherdogs-logo-lib-0.1.0 (c (n "cipherdogs-logo-lib") (v "0.1.0") (d (list (d (n "vga") (r "^0.2.4") (d #t) (k 0)) (d (n "vga-figures") (r "^0.2.0") (d #t) (k 0)))) (h "1mp559v9mllc5qcr2cs5qvfhfzy0wy0b2k1iasph8bd81zf9vl50")))

