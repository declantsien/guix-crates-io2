(define-module (crates-io ci ph cipher-crypt) #:use-module (crates-io))

(define-public crate-cipher-crypt-0.1.0 (c (n "cipher-crypt") (v "0.1.0") (h "1qbgqbws6kaxi7sia1v8vfv2sxlyg01x7shfazqkrr9r7r0iw21m")))

(define-public crate-cipher-crypt-0.2.0 (c (n "cipher-crypt") (v "0.2.0") (h "10rbr5c475s2bdxlwraqdx2g23r9z8dxfaamld23fs540l55vamx")))

(define-public crate-cipher-crypt-0.2.1 (c (n "cipher-crypt") (v "0.2.1") (h "08v49xq42pdw7n3pr353r90663p0cr8j00s3bjg9rn5cy01ll564")))

(define-public crate-cipher-crypt-0.3.0 (c (n "cipher-crypt") (v "0.3.0") (h "1z7dybchif4k0ghcmb6kk9jbsq7vdy75g1gb53x4gbw2qd64xlz5")))

(define-public crate-cipher-crypt-0.4.0 (c (n "cipher-crypt") (v "0.4.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "1xfbdj8rsy9cy6vhpcjzh5vagkdzcjalrzh1rillg8fb8cbpg5qz")))

(define-public crate-cipher-crypt-0.4.1 (c (n "cipher-crypt") (v "0.4.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "1s7q2yn0i50ziflsxs22lydpylly6vvzsrj86h9xqvgfn1r5l4al")))

(define-public crate-cipher-crypt-0.5.0 (c (n "cipher-crypt") (v "0.5.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "0dv7y9zkcfqz1agf4jm5a887afc5fh4yj079v4v1l6acpqixg0hn")))

(define-public crate-cipher-crypt-0.6.0 (c (n "cipher-crypt") (v "0.6.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "1ybcqj686qp35n8s0m26hibilgjmz91jz40m90fgh089iksv83is")))

(define-public crate-cipher-crypt-0.6.1 (c (n "cipher-crypt") (v "0.6.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "0ck9qcgbrb1vh52ski6nzray8gbaicf91qxhmylgvq1nypmmdj09")))

(define-public crate-cipher-crypt-0.7.0 (c (n "cipher-crypt") (v "0.7.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "0qd5knir9nbmvbv6xrmkhh1bwinf304rad53jh8rgsk16rbpviqf")))

(define-public crate-cipher-crypt-0.8.0 (c (n "cipher-crypt") (v "0.8.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "1fcwcbzf9knd8qhqr15zj9fa69nx9lbh18bxfr87rc3m43r1msm5")))

(define-public crate-cipher-crypt-0.9.0 (c (n "cipher-crypt") (v "0.9.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "0glaf7vl7m3b70h71h83kll4vhlk9nj8jrv1106rcf3595i4cx5x")))

(define-public crate-cipher-crypt-0.10.0 (c (n "cipher-crypt") (v "0.10.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "0m6k184nhl11wfgq3g3lpdyh1n5z61y0njmj04r1y53cv7nlwkxr")))

(define-public crate-cipher-crypt-0.11.0 (c (n "cipher-crypt") (v "0.11.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "0hs6751l57da6fa86rf1k85kviphgrkr4ji3vk3zxc6j7zv2yinp")))

(define-public crate-cipher-crypt-0.12.0 (c (n "cipher-crypt") (v "0.12.0") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "1644yany31dbvzw068g1b2xy79yzf35f4i4acaxsd80l0njrgi9g")))

(define-public crate-cipher-crypt-0.12.1 (c (n "cipher-crypt") (v "0.12.1") (d (list (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "1hazvr3fwihgzis7lz2n315l25s062i53y6xq119mdj5cy288i4d")))

(define-public crate-cipher-crypt-0.13.0 (c (n "cipher-crypt") (v "0.13.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lipsum") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "0ldb656h3j4p2325a4gml64w0yjrd29kx8bfpjp9nd3cpc5sac8y")))

(define-public crate-cipher-crypt-0.14.0 (c (n "cipher-crypt") (v "0.14.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lipsum") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "1jdd38ghqmb3wka5xqgmsvrfrys6ifs1x7hyl4s56dhnarcrfy6b")))

(define-public crate-cipher-crypt-0.14.1 (c (n "cipher-crypt") (v "0.14.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lipsum") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "1la6f2sslfcjfz0x0f6zhywnfrm1w3rxffr3xmdm9fi35phij41h")))

(define-public crate-cipher-crypt-0.15.0 (c (n "cipher-crypt") (v "0.15.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lipsum") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "0ahd81vnk19avdsad5s61g60wfrk1b72zr54i8m8yb7cgnrlidp3")))

(define-public crate-cipher-crypt-0.15.1 (c (n "cipher-crypt") (v "0.15.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lipsum") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "1najdiv7cd28mqa3bhfniksyv1mkyg6gahx2l30q37730igsiy0z")))

(define-public crate-cipher-crypt-0.16.0 (c (n "cipher-crypt") (v "0.16.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lipsum") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "10n8xqn6a4qp41fz6c68jivnd64wjgivqx2bpcma8rb1qm6cam81")))

(define-public crate-cipher-crypt-0.17.0 (c (n "cipher-crypt") (v "0.17.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lipsum") (r "^0.4") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "0icpifkdvz3ghngc4s7cv4j6sh4vsbqvsmi391k8ygakxmwxyspn")))

(define-public crate-cipher-crypt-0.18.0 (c (n "cipher-crypt") (v "0.18.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "lipsum") (r "^0.6") (d #t) (k 0)) (d (n "maplit") (r "^1.0.1") (d #t) (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4") (d #t) (k 0)))) (h "17x8s98i295ym539mw8cg2xp48ww6gwzc1k4fjm3d6d8fjab0il3")))

