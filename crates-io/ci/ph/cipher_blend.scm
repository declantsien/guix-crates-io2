(define-module (crates-io ci ph cipher_blend) #:use-module (crates-io))

(define-public crate-cipher_blend-0.1.0 (c (n "cipher_blend") (v "0.1.0") (h "0iijmkpkc5spgzdllargly48p2cm1bxgfk64lwi1ra5jpjs418i0")))

(define-public crate-cipher_blend-0.1.1 (c (n "cipher_blend") (v "0.1.1") (h "1rf98zwpwrjg66w3ja155ych3ip74gcsa6hrxvd5ch7lpqrhb8ba")))

