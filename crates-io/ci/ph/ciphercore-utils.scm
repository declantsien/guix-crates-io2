(define-module (crates-io ci ph ciphercore-utils) #:use-module (crates-io))

(define-public crate-ciphercore-utils-0.1.0 (c (n "ciphercore-utils") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1san2qv1zlk4wil1adk4j7lccaxn3gpq1gkhqaszz51nldvhz9p5") (f (quote (("nightly-features") ("default")))) (y #t)))

(define-public crate-ciphercore-utils-0.1.1 (c (n "ciphercore-utils") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "03ir57bf7dzlrja0vh0rm4f4asf8hgbjy512g9aiffdlqgphxxfy") (f (quote (("nightly-features") ("default"))))))

(define-public crate-ciphercore-utils-0.1.2 (c (n "ciphercore-utils") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "1fkvzc28i5g8a1vzilaqvpm5zlmrnhj3k5d49pgbwzff4lw0jn5z") (f (quote (("nightly-features") ("default"))))))

(define-public crate-ciphercore-utils-0.1.3 (c (n "ciphercore-utils") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "19lxp8xa630b4nsdssyh44jlp2x3995fg9fdnnlb4xa8gxzz8bn1") (f (quote (("default"))))))

(define-public crate-ciphercore-utils-0.2.0 (c (n "ciphercore-utils") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "1d5x5mfy9wa8k7iyx16f1mk8xh2fij8ls7w9icaxklvgzy6bkkgy") (f (quote (("default"))))))

(define-public crate-ciphercore-utils-0.3.0 (c (n "ciphercore-utils") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)))) (h "03lny4fgmh5drr7sw2mmq641ryr5wys7k3yz0bcya9kn45w4swni") (f (quote (("default"))))))

