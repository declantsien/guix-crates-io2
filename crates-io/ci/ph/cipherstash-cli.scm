(define-module (crates-io ci ph cipherstash-cli) #:use-module (crates-io))

(define-public crate-cipherstash-cli-0.1.0 (c (n "cipherstash-cli") (v "0.1.0") (h "12xvfw2idp537g1zng0l999qb3chgq3aqda12dj5aydz5mpnm1xd") (y #t)))

(define-public crate-cipherstash-cli-0.3.0 (c (n "cipherstash-cli") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.58") (d #t) (k 0)) (d (n "cipherstash-client") (r "^0.6.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.2") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "open") (r "^3.0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("full"))) (d #t) (k 0)))) (h "17rsp47xxhx87wbahvzan8477wqiy8hl259cw4kx627wkc52rqc1")))

