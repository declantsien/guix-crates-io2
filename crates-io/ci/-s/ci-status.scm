(define-module (crates-io ci -s ci-status) #:use-module (crates-io))

(define-public crate-ci-status-0.1.0 (c (n "ci-status") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.10.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.33.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pwi9iccc6p9ss7igddfrxnwafrxi6m4lixvkh38cl33lybqiljv")))

