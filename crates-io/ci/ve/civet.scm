(define-module (crates-io ci ve civet) #:use-module (crates-io))

(define-public crate-civet-0.1.0 (c (n "civet") (v "0.1.0") (d (list (d (n "civet-sys") (r "*") (d #t) (k 0)) (d (n "conduit") (r "*") (d #t) (k 0)) (d (n "route-recognizer") (r "*") (d #t) (k 0)) (d (n "semver") (r "*") (d #t) (k 0)))) (h "09nnz8jdw4xkzalyya6fs94nrnkwfn75d3fc4jzq32jc54gk6myd")))

(define-public crate-civet-0.1.1 (c (n "civet") (v "0.1.1") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "19r54ll0ywdqif29vf52qb5d4y0m39wlh7y0khxis1klipirndad")))

(define-public crate-civet-0.1.2 (c (n "civet") (v "0.1.2") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0j3fj3k78ksj7vnp4r3i4f3s43b4bd7dm6wgkicrwqjrmb7xygam")))

(define-public crate-civet-0.1.3 (c (n "civet") (v "0.1.3") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "03h6fryxp0ldl5976nhlqawxaipajsyy91kbfgw6n1xl3dbv8qfv")))

(define-public crate-civet-0.1.4 (c (n "civet") (v "0.1.4") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0213nn29lxja7ybrgjixjaicplalm3lg9xj30lqbh6imzgqp9sya")))

(define-public crate-civet-0.1.5 (c (n "civet") (v "0.1.5") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "09cm2ac30krbkbh4qq6h0rm55159x8cyma78dknkx9i2wf90cp1c")))

(define-public crate-civet-0.1.6 (c (n "civet") (v "0.1.6") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1pgnq7kbiqx96hhx8nnxf635zq9kyircia10hgd5zr3mls30l00h")))

(define-public crate-civet-0.1.7 (c (n "civet") (v "0.1.7") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.5.0") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0ycjlskf4mylm4i1adr3f7xn2jjznw3j4yf6ram241nk8s8pj6ah")))

(define-public crate-civet-0.1.8 (c (n "civet") (v "0.1.8") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1cljn0n4k52dbbqga0daf92pf9zjllrpq7wmirc3kh89g056cjvg")))

(define-public crate-civet-0.1.9 (c (n "civet") (v "0.1.9") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0jyr884g359pxdzc6bq33kw1d7n0xsrav6yrswrk9y7n61nissc7")))

(define-public crate-civet-0.1.10 (c (n "civet") (v "0.1.10") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1ljh93cbg6zvgqyjvg55apjs80cisq9ybzak7j7i4lmp1fr67r72")))

(define-public crate-civet-0.1.11 (c (n "civet") (v "0.1.11") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0hvd3my5295macwsdj33hgyg2n473rsvjw8q89xd9aclnxjl5fbp")))

(define-public crate-civet-0.1.12 (c (n "civet") (v "0.1.12") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1n5smb9dmx4bmfsnnihi1hqf0i1lkpxclylnlkgzj69mphbnhzwh")))

(define-public crate-civet-0.1.13 (c (n "civet") (v "0.1.13") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.6") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1gigg63x1i2hnyg0y4ls6bsbjk9rv0jldq2594viazdwq9bcrpi1")))

(define-public crate-civet-0.7.0 (c (n "civet") (v "0.7.0") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0w66nsx66cf5c7bgmj1wcjxk7i3zr1lzmkg9fxjnx0jpm0wfkhny")))

(define-public crate-civet-0.7.1 (c (n "civet") (v "0.7.1") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0z7qnxj95dvmk2lgcqfcqk5gh99qxzybk1azwkjdx8n05s1ippy7")))

(define-public crate-civet-0.7.2 (c (n "civet") (v "0.7.2") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "14czky3mqka5mznas1jd51l1kg4qrwzdbs8fx34r0k8mb7d7ldnd")))

(define-public crate-civet-0.7.3 (c (n "civet") (v "0.7.3") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0yr5ncv0faxgx876niiiyfaghd8wg9d0bvylfb3b1d5aa4hn4z41")))

(define-public crate-civet-0.7.4 (c (n "civet") (v "0.7.4") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "06xd6fzbp1c7w5bmp73m4pzj870vxlzh1wx12gpj23gicaw2rksc")))

(define-public crate-civet-0.7.5 (c (n "civet") (v "0.7.5") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "0zy1s84nxxxm09603a5ji80mvndvsj9m9ysm2pvqnikxfh1172w1")))

(define-public crate-civet-0.7.6 (c (n "civet") (v "0.7.6") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "06zkrlzmlcv1j6r1wyilrhirawcmq49kdhqj61lbmppl0xyp9sj5")))

(define-public crate-civet-0.8.0 (c (n "civet") (v "0.8.0") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.1.0") (d #t) (k 0)))) (h "1sp25nzfx4rr4lpi8zpfhv4i44q4qibvbgxzwfnrz62qm37s0dw9")))

(define-public crate-civet-0.8.1 (c (n "civet") (v "0.8.1") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.2.0") (d #t) (k 0)))) (h "0yyfihbsd0fg880lmz2x42zqpikdizca2ha0vai24ny84q99zgis")))

(define-public crate-civet-0.8.2 (c (n "civet") (v "0.8.2") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.2.0") (d #t) (k 0)))) (h "1nm9sqjzza4xgivfnr7w1pk8182j59dr1k8h8hdvvs8qh70czlcq")))

(define-public crate-civet-0.8.3 (c (n "civet") (v "0.8.3") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.2.0") (d #t) (k 0)))) (h "05dk9q4jn57l27b2ajxxwmn43jwnppv4l8hcsva6ai847av1cj3m")))

(define-public crate-civet-0.9.0 (c (n "civet") (v "0.9.0") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "09n9m0rm9f5fyyr21ymrhzsm0pfywaja0f56d0hr3pqjpdiql5fq")))

(define-public crate-civet-0.9.1 (c (n "civet") (v "0.9.1") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "03qqprja5f14j3jzxyzjbk40x57knp7c79nhsgjgjnvsfspyfqv2")))

(define-public crate-civet-0.10.0 (c (n "civet") (v "0.10.0") (d (list (d (n "civet-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "089jp6ghhqhm2cb28pli6pn1l3kj71vma2fz97d2sp62865ggpqq") (y #t)))

(define-public crate-civet-0.11.0 (c (n "civet") (v "0.11.0") (d (list (d (n "civet-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "conduit") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.5.0") (d #t) (k 0)))) (h "1zf3dkz04lmczv20677ya0i281629yagdgw92422cip0rrsg7vy9")))

(define-public crate-civet-0.12.0-alpha.0 (c (n "civet") (v "0.12.0-alpha.0") (d (list (d (n "civet-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "0mqa31084kl9p04gs6db081pn1rxspxch2vsx1iqk9djrsblqf79")))

(define-public crate-civet-0.12.0-alpha.1 (c (n "civet") (v "0.12.0-alpha.1") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "0lnrx055sr43vrj0x9aa0r0s4k94505m90ifmdra2bbm352y0njz")))

(define-public crate-civet-0.12.0-alpha.2 (c (n "civet") (v "0.12.0-alpha.2") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)))) (h "0f2vsa9qzik78nlhdwmcyix2q42c81nrsqw8zq7p9wqramn7ibmn")))

(define-public crate-civet-0.12.0-alpha.3 (c (n "civet") (v "0.12.0-alpha.3") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)))) (h "1793cba49s6c6vswj04vl1s257hq1bcxj831mkgn2ffhinjf2q7k")))

(define-public crate-civet-0.12.0-alpha.4 (c (n "civet") (v "0.12.0-alpha.4") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)))) (h "0yjqak88m4a3f3v6xw1jdapbyp6s8px7a95l4n8vh23r26hakpra")))

(define-public crate-civet-0.12.0-alpha.5 (c (n "civet") (v "0.12.0-alpha.5") (d (list (d (n "civet-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "conduit") (r "^0.9.0-alpha.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "route-recognizer") (r "^0.1.0") (d #t) (k 2)))) (h "1lfn7fm5silvn97i6vx595v6bbq7hw9dadma4qzgy7a04s1f2f3v")))

