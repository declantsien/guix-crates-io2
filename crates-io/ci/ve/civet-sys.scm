(define-module (crates-io ci ve civet-sys) #:use-module (crates-io))

(define-public crate-civet-sys-0.0.1 (c (n "civet-sys") (v "0.0.1") (h "1jkysbl914spazz45v1x4b0v26ibb6dqcvjpgbh0kkfryc2c82nd")))

(define-public crate-civet-sys-0.1.0 (c (n "civet-sys") (v "0.1.0") (h "1ghnrbpffg2swd4v3l05rzxji3r47m9a5z2c0xjvgla101bk42rm")))

(define-public crate-civet-sys-0.1.1 (c (n "civet-sys") (v "0.1.1") (h "1hvwfyzpf7r4rb5p7d2kccrhs1z75m8qwwrk2vbllhdmps9i7cic")))

(define-public crate-civet-sys-0.1.2 (c (n "civet-sys") (v "0.1.2") (h "1l8y2dj97y6j76krawf9hwdqg6bzvvwad0w8x160yxvhx738lgmr")))

(define-public crate-civet-sys-0.1.3 (c (n "civet-sys") (v "0.1.3") (h "0r87mj0s7qx4phzk41dnj7qjrfbf0kgwcvx3lb0m1bk9xpqy6ghm")))

(define-public crate-civet-sys-0.1.4 (c (n "civet-sys") (v "0.1.4") (h "0djr5hvm0wdd7b3w21z5886kp16x6szx9q9mrf1pk2zj5cvib3cm")))

(define-public crate-civet-sys-0.2.0 (c (n "civet-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1iyq4rwyjawg5zyal44ay260wwgk11an5hrn8xqkl1gvysfsllnh") (y #t)))

(define-public crate-civet-sys-0.3.0 (c (n "civet-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "18n4341w4wa4vwkxiwhsxy1i3y6mw9i8xf78zq8l58hm4vg10sc7") (l "civet")))

