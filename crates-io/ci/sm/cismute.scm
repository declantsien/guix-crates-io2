(define-module (crates-io ci sm cismute) #:use-module (crates-io))

(define-public crate-cismute-0.1.0 (c (n "cismute") (v "0.1.0") (h "0p1pzh1072knwagm57dxvfjdv31vkvsgcyiczrimnvf8drqv92s4") (f (quote (("switch"))))))

(define-public crate-cismute-0.1.1 (c (n "cismute") (v "0.1.1") (h "1mz0kkxxji86fdbbzpmcfj61czh6fn4arb7rnd260vj9f5lmswzh") (f (quote (("switch"))))))

(define-public crate-cismute-0.1.2 (c (n "cismute") (v "0.1.2") (h "1jpa11b41a5qr7zsw2x21y2xwk4p7vg2ymq3bifs4ydiyxbwq411") (f (quote (("switch"))))))

