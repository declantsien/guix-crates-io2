(define-module (crates-io ci -g ci-generate) #:use-module (crates-io))

(define-public crate-ci-generate-0.5.3 (c (n "ci-generate") (v "0.5.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "figment") (r "^0.10") (f (quote ("toml"))) (d #t) (k 0)) (d (n "license") (r "^3.1") (d #t) (k 0)) (d (n "minijinja") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0309b19w060y7i6c0x5ihkbgsygb7aycgkwyc9pn0bfiyf3f7lhh")))

