(define-module (crates-io ci p_ cip_rust) #:use-module (crates-io))

(define-public crate-cip_rust-0.9.0 (c (n "cip_rust") (v "0.9.0") (h "1c6282l2cc06y8kf9iizs2dghwb00mjy3sbanq56agpv2y6hl3i0")))

(define-public crate-cip_rust-0.9.1 (c (n "cip_rust") (v "0.9.1") (h "152hpyv8dvs591gf8ahl9ld69hrv1bnigqmlf80yqrz7j5fp8qas")))

(define-public crate-cip_rust-0.9.2 (c (n "cip_rust") (v "0.9.2") (h "1nph4x0554iaw49y56j4j5vl9x6p9jf2hrsdg8i0xjih9p8gs0n1")))

(define-public crate-cip_rust-0.9.3 (c (n "cip_rust") (v "0.9.3") (h "0iikly33as7y3mvp1vfh82vmxaxri246yvzk3pzf7rvwhhpqy1bm")))

(define-public crate-cip_rust-0.9.4 (c (n "cip_rust") (v "0.9.4") (h "0hjg60bi6dvhy60vdr7yvb8yg5s7hizwgg5p34fb79bsqcf70gi9")))

(define-public crate-cip_rust-0.9.5 (c (n "cip_rust") (v "0.9.5") (h "0jscsq9iyj0smfn6bz2pa2rhg7n0fy37a8qaa498vpk7ammbbalm")))

(define-public crate-cip_rust-0.9.6 (c (n "cip_rust") (v "0.9.6") (h "1cd1asjcnzs3f7h1i6swjpxr24ck89pcdy8969kj9yqwz0vbyap4")))

(define-public crate-cip_rust-0.9.7 (c (n "cip_rust") (v "0.9.7") (h "0bxxglflr9qdzyylpbmb6xfjsa4qmwwrf546grwcqwn74dzflxx3")))

(define-public crate-cip_rust-0.9.8 (c (n "cip_rust") (v "0.9.8") (h "062ky8fx4qbpffz16p630njmj107l6nhh9kdy0sai8y7asfn5kb3")))

