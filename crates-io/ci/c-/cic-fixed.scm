(define-module (crates-io ci c- cic-fixed) #:use-module (crates-io))

(define-public crate-cic-fixed-0.4.0 (c (n "cic-fixed") (v "0.4.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1d2yl4vs6lh3mr0hgdq8f3ymaqgc57my9gc1zi50dy62029268sd") (r "1.68.2")))

(define-public crate-cic-fixed-0.4.1 (c (n "cic-fixed") (v "0.4.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "1dls29wlv002g3cx93as2lds508qnw52n3scm677ikj05vg1acgd") (r "1.68.2")))

(define-public crate-cic-fixed-0.5.1 (c (n "cic-fixed") (v "0.5.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "08q7i9h9y2smndpg61p637bvw8q9cmclnak8ahv2nwn2kbs30jvs") (r "1.68.2")))

