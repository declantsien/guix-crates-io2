(define-module (crates-io ci ll cilly) #:use-module (crates-io))

(define-public crate-cilly-0.1.0 (c (n "cilly") (v "0.1.0") (d (list (d (n "ar") (r "^0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "postcard") (r "^1.0.6") (f (quote ("use-std"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)))) (h "06diwb1620sxy8chd9pvy5hb2s5carxsy13nysh5vj708d0shziy")))

