(define-module (crates-io ci me cimetrics-rs) #:use-module (crates-io))

(define-public crate-cimetrics-rs-0.1.0 (c (n "cimetrics-rs") (v "0.1.0") (h "174fhbgmmbh7y0j4j7h8vsmpgqmwfmcspv2w6rd5rr8mblkjjlmi")))

(define-public crate-cimetrics-rs-0.2.0 (c (n "cimetrics-rs") (v "0.2.0") (h "19znwzsibihllvs4008n60ilr6b63pjiswr31chlj0mx89zhi5vn")))

