(define-module (crates-io ci de ciderver) #:use-module (crates-io))

(define-public crate-ciderver-0.1.1 (c (n "ciderver") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "semver") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "06k7a6q3ra20wbvj7ggnz63mjja49jydcn8a44snl314apsgw69r")))

