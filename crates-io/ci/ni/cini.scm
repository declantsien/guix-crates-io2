(define-module (crates-io ci ni cini) #:use-module (crates-io))

(define-public crate-cini-0.1.0 (c (n "cini") (v "0.1.0") (h "0c79ydqm2z72rj64zk52j6k6n63m2sis6zqxfr2ljpb20ha3xwz4")))

(define-public crate-cini-0.1.1 (c (n "cini") (v "0.1.1") (h "1qj88nq63mli191p1gg713kyb0w1slswcgpz2dgdhyrx83b4qpw9")))

(define-public crate-cini-1.0.0 (c (n "cini") (v "1.0.0") (h "1wk7yryi572ijd4ykjs90qsrsmg2lwxhwrmaw5n1kcd7p7sx2a46")))

