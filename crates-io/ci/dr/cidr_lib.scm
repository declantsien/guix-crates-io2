(define-module (crates-io ci dr cidr_lib) #:use-module (crates-io))

(define-public crate-cidr_lib-0.1.0 (c (n "cidr_lib") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0599cm1aha2flb3bbcqbjbfrq4khl5cvdmyig22fzpij391ifidm") (s 2) (e (quote (("from_str" "dep:regex"))))))

(define-public crate-cidr_lib-0.1.1 (c (n "cidr_lib") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 2)) (d (n "regex") (r "^1.10.4") (o #t) (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0mralyzbw81hifnqp1cbfh388q5ai81ijizrqj5971d6vndx8bmc") (s 2) (e (quote (("from_str" "dep:regex"))))))

