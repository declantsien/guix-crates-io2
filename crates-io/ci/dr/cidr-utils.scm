(define-module (crates-io ci dr cidr-utils) #:use-module (crates-io))

(define-public crate-cidr-utils-0.1.0 (c (n "cidr-utils") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "008h2lsixp4x96pwvspa1nkdkh5w1ksnav9bv9xcmwb9p0qkpg6f")))

(define-public crate-cidr-utils-0.1.1 (c (n "cidr-utils") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "1qbimccldsavdagkipx4fjpfjy209ral9rghvlhbzql5dxapazrp")))

(define-public crate-cidr-utils-0.1.2 (c (n "cidr-utils") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "1m15a6z944r1f3ag5f9ml5bfnxhd1yizlplb6i0x5zbm26a4lsrs")))

(define-public crate-cidr-utils-0.2.0 (c (n "cidr-utils") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "04qy9xwwgdz4rmwsx5pgw5qmkvg4av5qdlrql7s4rylk9qcpd7l0")))

(define-public crate-cidr-utils-0.3.0 (c (n "cidr-utils") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "1wi3hspyz2s5ncnz91fb5f4c9kl9ysj1z2bm2ksqgzp0rlf8r5zz")))

(define-public crate-cidr-utils-0.3.1 (c (n "cidr-utils") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "1ix5054wvw3vlz02s2n8k7anmh98m32zm6f80ki5zjrj12gn0152")))

(define-public crate-cidr-utils-0.3.2 (c (n "cidr-utils") (v "0.3.2") (d (list (d (n "debug-helper") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "0c8bc4ycwclamga7k6bqkw66b6r52rpfs914affpdfllrvl6i430")))

(define-public crate-cidr-utils-0.3.3 (c (n "cidr-utils") (v "0.3.3") (d (list (d (n "debug-helper") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "1nm07bdzqahim6k7z844gsyggdslvj7wjsqaryl57sri16757kck")))

(define-public crate-cidr-utils-0.3.4 (c (n "cidr-utils") (v "0.3.4") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "08gv038bp22hwp9dk5iw4cmx5pqgc353kniyasav9d0mnhn84jal")))

(define-public crate-cidr-utils-0.3.5 (c (n "cidr-utils") (v "0.3.5") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "1jn3pv434q8gbd1cn31j71icjsgkcd4ds0nq7nv04fpj83vp4fpk")))

(define-public crate-cidr-utils-0.3.6 (c (n "cidr-utils") (v "0.3.6") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "1gb5ghkvjhax2vf68w2svvxf1vyknw4vs3bvjj4d8gcc8g9vh1x5")))

(define-public crate-cidr-utils-0.3.7 (c (n "cidr-utils") (v "0.3.7") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "1qmsamjdfa0np4g15xhjfskh3q9dn4hq4pq7sldrqv6p050wbv99")))

(define-public crate-cidr-utils-0.3.8 (c (n "cidr-utils") (v "0.3.8") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "1qk3mdzfagp20scmxy26nag4j7vn1gm3bgvcbkiw9skd282paygg")))

(define-public crate-cidr-utils-0.4.0 (c (n "cidr-utils") (v "0.4.0") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.1.5") (d #t) (k 0)))) (h "0g0hnl81qkxlmav0l3jd93vd2zc4lrivp0z2dsbdz8gnpz8ccv08")))

(define-public crate-cidr-utils-0.4.1 (c (n "cidr-utils") (v "0.4.1") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vjnk7zj5y64az1i58rqsxgd15miv63nmggrkhd8baiwy1xsn4nb")))

(define-public crate-cidr-utils-0.4.2 (c (n "cidr-utils") (v "0.4.2") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "03gmqfm7g5f7hbhhrj5fflsg5mf5s6zyhb8r6yl65xkqvgnmsg54")))

(define-public crate-cidr-utils-0.4.3 (c (n "cidr-utils") (v "0.4.3") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "enum-ordinalize") (r "^3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kvq4hndz1s0v342kjmvy5l4mkjk507r7wa9ijnz1n42vyvrbbqx")))

(define-public crate-cidr-utils-0.5.0 (c (n "cidr-utils") (v "0.5.0") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "007y3jgd1jdxncf3gwkvjq243bwl6qyjinn08inkyy1w5wvvdxw9")))

(define-public crate-cidr-utils-0.5.1 (c (n "cidr-utils") (v "0.5.1") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0p1k87iqd1201miiv4v07k110hzkxc4kwr86fqblp8nw20l249mr")))

(define-public crate-cidr-utils-0.5.2 (c (n "cidr-utils") (v "0.5.2") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1psrfmsark4vxv60dpk9wrb6baak1scn09md5l3jvf9c8m90i2w9")))

(define-public crate-cidr-utils-0.5.3 (c (n "cidr-utils") (v "0.5.3") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11wxvhgbpqbr2zb5nr1g56y5ixxbhvpsqwf3967im5ngci638awb")))

(define-public crate-cidr-utils-0.5.4 (c (n "cidr-utils") (v "0.5.4") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0kwc0ccjl02xbjj21gn6rpfn54cqwyzzqlq9ssikbjjw3vw6y3rr")))

(define-public crate-cidr-utils-0.5.5 (c (n "cidr-utils") (v "0.5.5") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0lbfh1y9azdpnirvalkdcj3fyqvjnis4kjhbhdz8w2k37aa78b1f")))

(define-public crate-cidr-utils-0.5.6 (c (n "cidr-utils") (v "0.5.6") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1ysl8nkvccdsbz5znbmq2bvapac7bpw82viynfnf06g14bzpxqxf")))

(define-public crate-cidr-utils-0.5.7 (c (n "cidr-utils") (v "0.5.7") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0ywwm1naxyipd789kjhpn4m7qv3f2yi6r5kmds3jkp4m8gclj34s")))

(define-public crate-cidr-utils-0.5.8 (c (n "cidr-utils") (v "0.5.8") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0r13sxn7afhfwhcxfsj2shakfj6hdljiy9j4gl6swzyvsr9jjlkq")))

(define-public crate-cidr-utils-0.5.9 (c (n "cidr-utils") (v "0.5.9") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1l2fc5ij5jdrk40jnczia6pza19c7n6il31xjlivan3yyrfmnp9m")))

(define-public crate-cidr-utils-0.5.10 (c (n "cidr-utils") (v "0.5.10") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0750jbxvdbyyxcqnzsw438158r9drs2g077ymx9r9lv193q3dypx")))

(define-public crate-cidr-utils-0.5.11 (c (n "cidr-utils") (v "0.5.11") (d (list (d (n "debug-helper") (r "^0.3") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "1wdbyr4xg15amp99i7cxyysp2q7rvmiwssnyhfldcikikc8zf593") (r "1.58")))

(define-public crate-cidr-utils-0.6.0 (c (n "cidr-utils") (v "0.6.0") (d (list (d (n "cidr") (r "^0.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vl85719vwc51ahq2g87iay20lihqlq74xrbp0qcjc2675dpnavc") (f (quote (("separator" "combiner" "iterator") ("iterator") ("default" "iterator" "combiner" "separator") ("combiner")))) (r "1.67")))

(define-public crate-cidr-utils-0.6.1 (c (n "cidr-utils") (v "0.6.1") (d (list (d (n "cidr") (r "^0.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0gpl4z9zrknkl5nippq7d8250mils6qmk8haa8mcrhn2f3xskh15") (f (quote (("separator" "combiner" "iterator") ("iterator") ("default" "iterator" "combiner" "separator") ("combiner")))) (r "1.67")))

