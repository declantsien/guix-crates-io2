(define-module (crates-io ci dr cidrr) #:use-module (crates-io))

(define-public crate-cidrr-0.1.0 (c (n "cidrr") (v "0.1.0") (d (list (d (n "docopt") (r "^0.6.67") (d #t) (k 0)))) (h "1l3m47xq6r1vzjhjrv2daz5pv5mdwmb6qykh0zqsib6cjywnm7bb")))

(define-public crate-cidrr-0.1.1 (c (n "cidrr") (v "0.1.1") (d (list (d (n "docopt") (r "^0.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1kri13903i5z2vf7f4x89bqv5jg9b29wq96q4w78n15dfia0vsxp")))

