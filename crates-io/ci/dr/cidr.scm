(define-module (crates-io ci dr cidr) #:use-module (crates-io))

(define-public crate-cidr-0.0.1 (c (n "cidr") (v "0.0.1") (h "0xf82rb567sx37k65ykay9ik8i55wwiddv8cmz3mdzlc82vimh3w")))

(define-public crate-cidr-0.0.2 (c (n "cidr") (v "0.0.2") (h "01hl7kpnszn8zb45vq30pvx19z41q0ip1q2bfvdvgqiia364m1bh")))

(define-public crate-cidr-0.0.3 (c (n "cidr") (v "0.0.3") (d (list (d (n "bitstring") (r "^0.1.0") (d #t) (k 0)))) (h "0ywpcgkwbzh6j27w7bigz95hl46xxzjcvk3mdmyhjpc0x1lj06ph")))

(define-public crate-cidr-0.0.4 (c (n "cidr") (v "0.0.4") (d (list (d (n "bitstring") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.27") (d #t) (k 2)))) (h "14qiidbsyi5l6irxa9hmxhkqflw2hipgn58iiyr9i0bcsl7k9xqb") (f (quote (("default" "serde"))))))

(define-public crate-cidr-0.1.0 (c (n "cidr") (v "0.1.0") (d (list (d (n "bincode") (r "^1.0.1") (d #t) (k 2)) (d (n "bitstring") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.27") (d #t) (k 2)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "00dxh7nqkyipds7090x1fgdm2sbl7gdhrx57cwcdrf2v4w7wz89d") (f (quote (("default" "serde"))))))

(define-public crate-cidr-0.1.1 (c (n "cidr") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "bitstring") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.27") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1hcp67kq6c0bz7kqd5qypkr0pj24cgmnspkaabfysa1j0mi6qcf6") (f (quote (("default" "serde"))))))

(define-public crate-cidr-0.2.0 (c (n "cidr") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "bitstring") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.27") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1vihw0cgih0n4w1vpm3s1l2jd6jc8y1zrklggmnxx9z07kfdm8vy") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-cidr-0.2.1 (c (n "cidr") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "bitstring") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.27") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0jy3i2hzd9pf9gabjarnmj4xdshzssm3hq144d2sibdi573wq2rh") (f (quote (("std") ("default" "std"))))))

(define-public crate-cidr-0.2.2 (c (n "cidr") (v "0.2.2") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 2)) (d (n "bitstring") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0.27") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1n78hgs3h8hk62l35afhyx699fmj3qvd911p3sm9lk55xf9v064d") (f (quote (("std") ("default" "std"))))))

