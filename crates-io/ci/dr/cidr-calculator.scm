(define-module (crates-io ci dr cidr-calculator) #:use-module (crates-io))

(define-public crate-cidr-calculator-0.1.0 (c (n "cidr-calculator") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.4") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (d #t) (k 0)))) (h "1105kzr0lg5cgckni28k7g7walpdy9n02bxyb3z9d1lkr1a7vzdc")))

(define-public crate-cidr-calculator-0.2.0 (c (n "cidr-calculator") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "im") (r "^15.1.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.4") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.4") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (o #t) (d #t) (k 0)))) (h "17csjq6fsk2rw9zrnycrr0n00lcl67w5d1qqizs5zws25g4khd58") (f (quote (("default" "cli")))) (s 2) (e (quote (("cli" "dep:rustyline" "dep:clap"))))))

