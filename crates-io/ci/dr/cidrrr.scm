(define-module (crates-io ci dr cidrrr) #:use-module (crates-io))

(define-public crate-cidrrr-2024.3.21 (c (n "cidrrr") (v "2024.3.21") (d (list (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "ipnet") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rq81fzb5ncx7mdk30q2jjk0dz5xc4aarhb81gx63vbd7ahxslhp")))

(define-public crate-cidrrr-2024.3.22 (c (n "cidrrr") (v "2024.3.22") (d (list (d (n "clap") (r "^4") (f (quote ("cargo" "derive"))) (d #t) (k 0)) (d (n "ipnet") (r "^2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "08z4q1nj498d1zhzq885iz3v816yd694y8wmfrfspwnr702xvayb")))

