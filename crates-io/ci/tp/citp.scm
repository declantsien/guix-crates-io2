(define-module (crates-io ci tp citp) #:use-module (crates-io))

(define-public crate-citp-0.1.0 (c (n "citp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)))) (h "1hxkvzraamhcqqa7qcpmxry1zyvxyqsjgwvn10d7mp0f9659g9i5")))

(define-public crate-citp-0.2.1 (c (n "citp") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.2.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 2)) (d (n "socket2") (r "^0.5.3") (d #t) (k 2)) (d (n "ucs2") (r "^0.3.2") (d #t) (k 0)))) (h "0s0l1sncbd55sj2p7hfyf9sixicfnbwmw7yf826pcv1jnawdk7b1")))

