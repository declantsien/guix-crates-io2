(define-module (crates-io ci st cistring) #:use-module (crates-io))

(define-public crate-cistring-0.1.0 (c (n "cistring") (v "0.1.0") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0haq07203s6ahkyxmay5dz7xg8cg5fnp8sb1cwwmixmlnklf15s7") (f (quote (("default"))))))

(define-public crate-cistring-0.1.1 (c (n "cistring") (v "0.1.1") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1wn4wk4x2a6aml97a7sndgwmp1g9018cd0643wfmh8rijjbirs7i") (f (quote (("default"))))))

