(define-module (crates-io ci te citeworks-cli) #:use-module (crates-io))

(define-public crate-citeworks-cli-0.1.0 (c (n "citeworks-cli") (v "0.1.0") (d (list (d (n "citeworks-cff") (r "^0.1.0") (d #t) (k 0)) (d (n "citeworks-csl") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "miette") (r "^5.3.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.6") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1bxwcsr18rqnywlslx3zjb6xh6rg2i7y4qz847ayqck72zgppaby") (r "1.63.0")))

(define-public crate-citeworks-cli-0.1.1 (c (n "citeworks-cli") (v "0.1.1") (d (list (d (n "citeworks-cff") (r "^0.1.1") (d #t) (k 0)) (d (n "citeworks-csl") (r "^0.3.0") (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "miette") (r "^5.3.0") (f (quote ("fancy"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.6") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "07b68n1r9gyrcs6cbqfkcmqfpa5bl9kpm486jzr9l6p65pc8mhqc") (r "1.59.0")))

