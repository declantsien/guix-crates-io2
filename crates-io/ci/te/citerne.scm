(define-module (crates-io ci te citerne) #:use-module (crates-io))

(define-public crate-citerne-0.1.0 (c (n "citerne") (v "0.1.0") (d (list (d (n "citerne-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "diesel") (r "^2") (d #t) (k 0)) (d (n "diesel_migrations") (r "^2") (d #t) (k 0)) (d (n "testcontainers") (r "^0.14") (d #t) (k 0)))) (h "1a8mhs7qmfvk7n48wkxbcvhhcy6dqawnfb88jmrpsmih58ifqm3d") (f (quote (("postgres" "diesel/postgres" "citerne-derive/postgres") ("default" "postgres")))) (y #t)))

