(define-module (crates-io ci te citeworks-csl) #:use-module (crates-io))

(define-public crate-citeworks-csl-0.1.0 (c (n "citeworks-csl") (v "0.1.0") (d (list (d (n "decorum") (r "^0.3.1") (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)))) (h "1lapamphifcm729f42s1pgv17qy541ypy6zdcjsqdl2bds2zwr00") (r "1.63.0")))

(define-public crate-citeworks-csl-0.2.0 (c (n "citeworks-csl") (v "0.2.0") (d (list (d (n "decorum") (r "^0.3.1") (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "1xab4wx16457b0yazsly653j8mj5mbymxkjqabj1xhq0iyvfi06k") (r "1.63.0")))

(define-public crate-citeworks-csl-0.3.0 (c (n "citeworks-csl") (v "0.3.0") (d (list (d (n "decorum") (r "^0.3.1") (k 0)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "0cxjl2ficag618g9akhbmm1xl6iaiv9jwrsqh6d8579290jbi5d8") (r "1.59.0")))

