(define-module (crates-io ci te cite) #:use-module (crates-io))

(define-public crate-cite-0.1.0 (c (n "cite") (v "0.1.0") (d (list (d (n "arguments") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 0)))) (h "0a7gppjhik4qz2y2fyv3wkgqwvxjsz2sz0lvy9mlwy839mgdvjam")))

(define-public crate-cite-0.2.0 (c (n "cite") (v "0.2.0") (d (list (d (n "arguments") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 0)))) (h "14xca321v0jnrhkiphg46hbwp19i2hx8fp4h4as4z6imx7c694y0")))

(define-public crate-cite-0.3.0 (c (n "cite") (v "0.3.0") (d (list (d (n "arguments") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 0)))) (h "0q9ipinnz6qphfg6an8z8dbq2azhm11f8qyl8ryxprb3lvf9f5ws")))

