(define-module (crates-io ci te citeworks-cff) #:use-module (crates-io))

(define-public crate-citeworks-cff-0.1.0 (c (n "citeworks-cff") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "semver") (r "^1.0.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)) (d (n "spdx") (r "^0.8.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0x34b2c42im9wyrqnlzwiv0c6zkkla27xhh61nddmk25m4gwzmrn") (r "1.63.0")))

(define-public crate-citeworks-cff-0.1.1 (c (n "citeworks-cff") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "semver") (r "^1.0.13") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.4") (d #t) (k 0)) (d (n "spdx") (r "^0.8.1") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1kz1m4zv7cba1g34i581yl6b7mv56mmh909yk0w2wn9mf08rixxd") (r "1.59.0")))

