(define-module (crates-io ci te citerne-derive) #:use-module (crates-io))

(define-public crate-citerne-derive-0.1.0 (c (n "citerne-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1n8knh1dh2rdh4kpjq7fs0riqkp8diw0bl0153y0n635mldgpdiq") (f (quote (("postgres") ("mysql") ("default" "postgres"))))))

