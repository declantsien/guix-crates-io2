(define-module (crates-io ci ta cita-snappy) #:use-module (crates-io))

(define-public crate-cita-snappy-0.1.0 (c (n "cita-snappy") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.101") (d #t) (k 0)))) (h "049f978r0ndvn0s2p3gisl3m6gks2jk4zcanmka8yi0bkx7gd7f8")))

(define-public crate-cita-snappy-0.1.1 (c (n "cita-snappy") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1q9fg702mjzs5vc3i9vshiykrqk23vqkqim8khb9qdqm7r98a6al")))

(define-public crate-cita-snappy-0.1.2 (c (n "cita-snappy") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rpwbs234m8z69s2ji6967yajs0w4cdb3jx3dqfb0m43wz4bwaa1")))

(define-public crate-cita-snappy-0.1.3 (c (n "cita-snappy") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0knma134615fbd9zhafx241x009sf1b96rxz0an4wkfiacgxcv4j")))

(define-public crate-cita-snappy-0.1.4 (c (n "cita-snappy") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "003apbyx0rr7hdw5giv075f0fl517aqzds8q6qyb0i7bi3ix0z64") (y #t)))

(define-public crate-cita-snappy-0.1.5 (c (n "cita-snappy") (v "0.1.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i9q9ia8y9cjg4dw331n0viyrwpbk3syqhx6vsvk6c4mvavp68j1")))

