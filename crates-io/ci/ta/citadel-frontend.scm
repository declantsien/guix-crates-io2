(define-module (crates-io ci ta citadel-frontend) #:use-module (crates-io))

(define-public crate-citadel-frontend-0.0.1 (c (n "citadel-frontend") (v "0.0.1") (d (list (d (n "clutils") (r "^0.0.9") (d #t) (k 0)))) (h "08k94kvw5aa0r6rgnx1fcrqsgznmwbgq757m6r44832spcnrnpb5")))

(define-public crate-citadel-frontend-0.0.10 (c (n "citadel-frontend") (v "0.0.10") (d (list (d (n "clutils") (r "^0.0.9") (d #t) (k 0)))) (h "12mizvs0mmab0067nwfgbznzc4vsc27g4lz65xmwwiqzagw2in6d")))

(define-public crate-citadel-frontend-0.0.11 (c (n "citadel-frontend") (v "0.0.11") (h "11y1if820di52zc4xmi2f8nlx0k8dc06wg1qz8y31ac40ix6g996")))

