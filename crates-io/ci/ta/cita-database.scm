(define-module (crates-io ci ta cita-database) #:use-module (crates-io))

(define-public crate-cita-database-0.1.0 (c (n "cita-database") (v "0.1.0") (d (list (d (n "cita-logger") (r "^0.1.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.12.2") (d #t) (k 0)))) (h "06p2q3sf82qk283nq6dsywak4hax7wfmjqxfxc93n4n9102c9hxl")))

(define-public crate-cita-database-0.1.1 (c (n "cita-database") (v "0.1.1") (d (list (d (n "cita-logger") (r "^0.1.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15") (d #t) (k 0)))) (h "18i3wfikw1ynrlqcbqxsdss2qm298jnwkvi9lhlhz27ndqm78ii9")))

(define-public crate-cita-database-0.2.0 (c (n "cita-database") (v "0.2.0") (d (list (d (n "cita-logger") (r "^0.1.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.18.0") (d #t) (k 0)))) (h "1m0cd7vw76bydqjjs7q6c5ff1p4iafcxjhjapq9dvqis9bk9hqx6")))

(define-public crate-cita-database-0.1.2 (c (n "cita-database") (v "0.1.2") (d (list (d (n "cmb-logger") (r "^0.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.15") (d #t) (k 0)))) (h "1gi7jsmj8z0qxryzah4l442bh6y7i7xbazkfrph0nmy7s1mb0rcf")))

(define-public crate-cita-database-0.3.0 (c (n "cita-database") (v "0.3.0") (d (list (d (n "cita-logger") (r "^0.1.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.19.0") (d #t) (k 0)))) (h "0l0hj7r3w01fshx85i93qmhg87imk5vrpq65d6ds0q6qpwb9p2xw")))

(define-public crate-cita-database-0.4.0 (c (n "cita-database") (v "0.4.0") (d (list (d (n "cita-logger") (r "^0.1.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.20.0") (d #t) (k 0)))) (h "1lankypda97d1v70bf6i56lgjjglcqbp2lll4cwhcki836av08b3")))

(define-public crate-cita-database-0.5.0 (c (n "cita-database") (v "0.5.0") (d (list (d (n "cita-logger") (r "^0.1.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)))) (h "1sg25bl102arrc9dnm7lwl1argjjjyiz96rz3k8i3gjpp9p0mx9r")))

(define-public crate-cita-database-0.5.1 (c (n "cita-database") (v "0.5.1") (d (list (d (n "cita-logger") (r "^0.1.1") (d #t) (k 0)) (d (n "rocksdb") (r "^0.21.0") (d #t) (k 0)))) (h "181z6597p6j5bwnwqyfcr5kpmaxw6knkf1s5yv3zf8r3nzj9amwx")))

