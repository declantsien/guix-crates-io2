(define-module (crates-io ci ta cita-evm) #:use-module (crates-io))

(define-public crate-cita-evm-0.1.0 (c (n "cita-evm") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0c08rfbagzl8r53ld133gdf4nkf3lrdsf6m1sqv549lbi8xw3q3i")))

(define-public crate-cita-evm-0.1.1 (c (n "cita-evm") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08nx1qw1swdhgzqb9cqmrgwnqd27yjv6lqc8jnb83lkgq6549z5y")))

(define-public crate-cita-evm-0.1.2 (c (n "cita-evm") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ffxd5v4gw4p125zyalpf1y2m4bgbxxqwzdvc6p8piyid0lja910")))

(define-public crate-cita-evm-0.1.3 (c (n "cita-evm") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1ywy7lkdcy630y55n4wl65a8dvyc9g1hhy1y1bihp801b4v1yl6z")))

(define-public crate-cita-evm-0.1.4 (c (n "cita-evm") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1v99lc78ii9829qnzv35p0mk55ri9liwiahzfp9ipy7w2z1vfhlb")))

(define-public crate-cita-evm-0.1.5 (c (n "cita-evm") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mzf2s1wl0vpvxsbr1asfmldh3fja46cg0vwaipgfqgcwwb21h7z")))

(define-public crate-cita-evm-0.1.6 (c (n "cita-evm") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "ethereum-types") (r "^0.4") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "keccak-hash") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1z539f9llljs0f48ac15mlfv5ybgyyd3gmf638nf2x48jb0ks9nz")))

