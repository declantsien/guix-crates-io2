(define-module (crates-io ci ta cita-util) #:use-module (crates-io))

(define-public crate-cita-util-0.1.0 (c (n "cita-util") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "backtrace") (r "^0.3") (d #t) (k 0)) (d (n "cita-logger") (r "^0.1.0") (d #t) (k 0)) (d (n "cita-types") (r "^0.1") (d #t) (k 0)) (d (n "git2") (r "^0.13") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "12s2f69r00dzhnag6vcscwzmg9ya26m8sisvmq83zf9j87bw1mc6")))

