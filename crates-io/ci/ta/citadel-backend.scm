(define-module (crates-io ci ta citadel-backend) #:use-module (crates-io))

(define-public crate-citadel-backend-0.0.1 (c (n "citadel-backend") (v "0.0.1") (d (list (d (n "citadel-frontend") (r "^0.0.1") (d #t) (k 0)))) (h "09gvz1lvdy8mcyfrckag41l84z70zx1vsp2b1rpr9619l59db8fz")))

(define-public crate-citadel-backend-0.0.11 (c (n "citadel-backend") (v "0.0.11") (d (list (d (n "bumpalo") (r "^3.16.0") (d #t) (k 0)) (d (n "citadel-frontend") (r "^0.0.11") (d #t) (k 0)))) (h "0nrgfisng13lq8jylcrc6f2v524lpasj0ny01d5kbkv4w2r0aay3")))

