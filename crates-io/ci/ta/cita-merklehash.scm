(define-module (crates-io ci ta cita-merklehash) #:use-module (crates-io))

(define-public crate-cita-merklehash-0.1.0 (c (n "cita-merklehash") (v "0.1.0") (d (list (d (n "cita-types") (r "^0.1") (d #t) (k 0)) (d (n "hashable") (r "^0.1") (d #t) (k 0) (p "cita-hashable")) (d (n "rlp") (r "^0.5.1") (d #t) (k 0)) (d (n "static_merkle_tree") (r "^1.1.0") (d #t) (k 0)))) (h "0zl84wa7gjaihpr7yp6iyp2rjirrwr4mzpszxhiff6bc33qzill4") (f (quote (("sm3hash" "hashable/sm3hash") ("sha3hash" "hashable/sha3hash") ("default") ("blake2bhash" "hashable/blake2bhash"))))))

