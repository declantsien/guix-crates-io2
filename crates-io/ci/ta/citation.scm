(define-module (crates-io ci ta citation) #:use-module (crates-io))

(define-public crate-citation-0.0.0 (c (n "citation") (v "0.0.0") (h "0g83vz2csn8c6yy7915m1i2p2xy3h7qi48cdjm1lbz17sja00ll2") (y #t)))

(define-public crate-citation-0.1.0 (c (n "citation") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "1zp5mdcbz66yb0pwvdyq9rwyvvc0140rkhk8p0fn7r632ljwhd8x")))

