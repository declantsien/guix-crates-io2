(define-module (crates-io ci ta citadel-middleend) #:use-module (crates-io))

(define-public crate-citadel-middleend-0.0.1 (c (n "citadel-middleend") (v "0.0.1") (d (list (d (n "citadel-frontend") (r "^0.0.1") (d #t) (k 0)))) (h "0ayj5vfijfk5sys1sadayaqc53xy6n0rja70wm6cg58l74njaqj9")))

(define-public crate-citadel-middleend-0.0.11 (c (n "citadel-middleend") (v "0.0.11") (d (list (d (n "citadel-frontend") (r "^0.0.11") (d #t) (k 0)))) (h "029sb1i12ciai1sn5nifbx1z2d4n5zam9rqz2faymgqadvj7y8az")))

