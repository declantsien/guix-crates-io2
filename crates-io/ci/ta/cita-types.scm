(define-module (crates-io ci ta cita-types) #:use-module (crates-io))

(define-public crate-cita-types-0.1.0 (c (n "cita-types") (v "0.1.0") (d (list (d (n "ethereum-types") (r "^0.12") (d #t) (k 0)) (d (n "plain_hasher") (r "^0.2") (d #t) (k 0)))) (h "1a9c7n88qy1qr2wfx6h4vgdi4ppmwc3acilk29v49xcfjvn71n52")))

(define-public crate-cita-types-0.1.1 (c (n "cita-types") (v "0.1.1") (d (list (d (n "ethereum-types") (r "^0.13") (d #t) (k 0)) (d (n "plain_hasher") (r "^0.2") (d #t) (k 0)))) (h "00ijxkmk20vggv861lb817zg6db8k5rv7iijj3zmq9pz7aw4kgsw")))

(define-public crate-cita-types-0.1.2 (c (n "cita-types") (v "0.1.2") (d (list (d (n "ethereum-types") (r "^0.14") (d #t) (k 0)) (d (n "plain_hasher") (r "^0.2") (d #t) (k 0)))) (h "0450y9xj9qy362ms6zdyk6qaw5llsh2jzsaqi68263lfiyhi73v3")))

