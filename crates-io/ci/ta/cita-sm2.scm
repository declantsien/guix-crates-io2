(define-module (crates-io ci ta cita-sm2) #:use-module (crates-io))

(define-public crate-cita-sm2-0.1.0 (c (n "cita-sm2") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "cita-crypto-trait") (r "^0.1") (d #t) (k 0)) (d (n "cita-types") (r "^0.1") (d #t) (k 0)) (d (n "hashable") (r "^0.1") (d #t) (k 0) (p "cita-hashable")) (d (n "libsm") (r "^0.4") (d #t) (k 0)) (d (n "rlp") (r "^0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0rmkiw14r59n8vy9q38hkl78zyj2bwwgbilv3yhiqf5r9zb78j60") (f (quote (("sm3hash" "hashable/sm3hash") ("sha3hash" "hashable/sha3hash") ("default") ("blake2bhash" "hashable/blake2bhash"))))))

