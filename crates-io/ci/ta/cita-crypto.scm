(define-module (crates-io ci ta cita-crypto) #:use-module (crates-io))

(define-public crate-cita-crypto-0.1.0 (c (n "cita-crypto") (v "0.1.0") (d (list (d (n "cita-crypto-trait") (r "^0.1") (d #t) (k 0)) (d (n "cita-ed25519") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cita-secp256k1") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "cita-sm2") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1x7q31k2hp9282icf4w4fsfysgzg65n1f78nnzqlcxlmxxznz897") (f (quote (("sm2" "cita-sm2" "cita-sm2/sm3hash") ("secp256k1" "cita-secp256k1" "cita-secp256k1/sha3hash") ("ed25519" "cita-ed25519" "cita-ed25519/blake2bhash") ("default"))))))

