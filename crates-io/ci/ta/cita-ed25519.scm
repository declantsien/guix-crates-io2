(define-module (crates-io ci ta cita-ed25519) #:use-module (crates-io))

(define-public crate-cita-ed25519-0.1.0 (c (n "cita-ed25519") (v "0.1.0") (d (list (d (n "bincode") (r "^0.8.0") (d #t) (k 2)) (d (n "cita-crypto-trait") (r "^0.1") (d #t) (k 0)) (d (n "cita-types") (r "^0.1") (d #t) (k 0)) (d (n "hashable") (r "^0.1") (d #t) (k 0) (p "cita-hashable")) (d (n "rlp") (r "^0.5") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "0kxml8z3gwjhfs2q1dxlyrmjld3rcjg4vrlbbq58lwbl1am5kf6j") (f (quote (("sm3hash" "hashable/sm3hash") ("sha3hash" "hashable/sha3hash") ("default") ("blake2bhash" "hashable/blake2bhash"))))))

