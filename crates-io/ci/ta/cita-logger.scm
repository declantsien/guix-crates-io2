(define-module (crates-io ci ta cita-logger) #:use-module (crates-io))

(define-public crate-cita-logger-0.1.0 (c (n "cita-logger") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)))) (h "08dm89r350bvz0lhspczxqpfzb06carmnawgh1qsprivngvcq3sw")))

(define-public crate-cita-logger-0.1.1 (c (n "cita-logger") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.9") (d #t) (k 0)) (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1") (d #t) (k 0)))) (h "0asfmpz6qr89dilpgs0g4wm755pvpwpl7vvadjhymkn3k4bx8a1z")))

(define-public crate-cita-logger-0.1.2 (c (n "cita-logger") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^1.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)))) (h "0fqvzmpax3y47amhg9b4ks7j8g36xck5k5d7gnpcrnsfmry4zlxq")))

