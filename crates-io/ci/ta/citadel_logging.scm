(define-module (crates-io ci ta citadel_logging) #:use-module (crates-io))

(define-public crate-citadel_logging-0.1.0 (c (n "citadel_logging") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "18g0adhggms7b77n8ykw3n9jraw3pbcdnbi3vp3kfsfidxl70d5m")))

(define-public crate-citadel_logging-0.3.0 (c (n "citadel_logging") (v "0.3.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1j6mjcq2dhylvs5dbf0jnyr9npqpghg0nhy93i3ajqja7s6rhipy")))

(define-public crate-citadel_logging-0.4.0 (c (n "citadel_logging") (v "0.4.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1z6whjffhwd4a96rw99hihcvbrj29pzg3cli1h0paakhx106y750")))

(define-public crate-citadel_logging-0.5.0 (c (n "citadel_logging") (v "0.5.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0c8p0qnp964mya60xnk6ab768hgwdyqb02f5czivnzdwnjlc7n25")))

(define-public crate-citadel_logging-0.7.0 (c (n "citadel_logging") (v "0.7.0") (d (list (d (n "log") (r "^0.4.17") (k 0)) (d (n "tracing") (r "^0.1.37") (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "10mdbk84f7n18rzkmyddddz1xwvap6bnwdk5m13zr6kw80p5w84g")))

(define-public crate-citadel_logging-0.8.0 (c (n "citadel_logging") (v "0.8.0") (d (list (d (n "tracing") (r "^0.1.37") (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "0djn4cz867pwkxcbfrv0wpyl1x6v62brh9c3w1gffk6430kf4qjf")))

