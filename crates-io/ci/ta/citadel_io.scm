(define-module (crates-io ci ta citadel_io) #:use-module (crates-io))

(define-public crate-citadel_io-0.3.0 (c (n "citadel_io") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12.1") (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("net"))) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "ws_stream_wasm") (r "^0.7.3") (d #t) (t "cfg(target_family = \"wasm\")") (k 0)))) (h "0hhpxf6p8256n6fingn8w76zx12v5hc5dgxgf338adlmvq23q3p2") (f (quote (("wasm") ("std") ("default" "std"))))))

(define-public crate-citadel_io-0.4.0 (c (n "citadel_io") (v "0.4.0") (d (list (d (n "parking_lot") (r "^0.12.1") (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "tokio") (r "^1.24") (f (quote ("net" "rt"))) (k 0)))) (h "00mcs9d2y8sxk8ln8s274mv9syvqp209f75b8pahhz70l9fgr5v7") (f (quote (("wasm") ("std") ("default" "std") ("deadlock-detection" "parking_lot/deadlock_detection"))))))

(define-public crate-citadel_io-0.5.0 (c (n "citadel_io") (v "0.5.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("net" "rt"))) (k 0)))) (h "0bxvsdan3037kji0ycbfxx69v0znxp7kpd1kjdx3p06rm18fal98") (f (quote (("wasm") ("std") ("default" "std") ("deadlock-detection" "parking_lot/deadlock_detection"))))))

(define-public crate-citadel_io-0.6.0 (c (n "citadel_io") (v "0.6.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("net" "rt"))) (k 0)))) (h "19cnkhip64j1kvf6nh8mpj4zarz297h5dj6wnxa5bjil63x70gln") (f (quote (("wasm") ("std") ("default" "std") ("deadlock-detection" "parking_lot/deadlock_detection"))))))

(define-public crate-citadel_io-0.7.0 (c (n "citadel_io") (v "0.7.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (t "cfg(not(target_family = \"wasm\"))") (k 0)) (d (n "tokio") (r "^1.28") (f (quote ("net" "rt"))) (k 0)))) (h "1s3ybicgcnkx4nm5hqcyrnwpic8gxn4v7ijc8nh46n146ykrqzjj") (f (quote (("wasm") ("std") ("default" "std") ("deadlock-detection" "parking_lot/deadlock_detection"))))))

(define-public crate-citadel_io-0.8.0 (c (n "citadel_io") (v "0.8.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (t "cfg(not(target_family=\"wasm\"))") (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("net" "rt"))) (d #t) (k 0)))) (h "08cpalkzdw8j3nh5kma4gxa9jgwilzxnblnagq8c2rkrfwvv911b") (f (quote (("wasm") ("std") ("default" "std") ("deadlock-detection" "parking_lot/deadlock_detection"))))))

