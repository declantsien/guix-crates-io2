(define-module (crates-io ci ta cita-hashable) #:use-module (crates-io))

(define-public crate-cita-hashable-0.1.0 (c (n "cita-hashable") (v "0.1.0") (d (list (d (n "blake2b_simd") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "cita-types") (r "^0.1") (d #t) (k 0)) (d (n "libsm") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "tiny-keccak") (r "^1.4") (o #t) (d #t) (k 0)))) (h "00nb3kwkirmicygls6jmmi5w27ylykr63chdlxjhyi60yd2bm8wi") (f (quote (("sm3hash" "libsm") ("sha3hash" "tiny-keccak") ("default") ("blake2bhash" "blake2b_simd"))))))

