(define-module (crates-io ci ta citadel-api) #:use-module (crates-io))

(define-public crate-citadel-api-0.0.1 (c (n "citadel-api") (v "0.0.1") (d (list (d (n "citadel-backend") (r "^0.0.1") (d #t) (k 0)) (d (n "citadel-frontend") (r "^0.0.1") (d #t) (k 0)) (d (n "citadel-middleend") (r "^0.0.1") (d #t) (k 0)))) (h "1i9c4y30i35svlnjmhq1vvw7jkip6rl3dr4m5cjwlg0j1lhsbxx2")))

(define-public crate-citadel-api-0.0.11 (c (n "citadel-api") (v "0.0.11") (d (list (d (n "citadel-backend") (r "^0.0.11") (d #t) (k 0)) (d (n "citadel-frontend") (r "^0.0.11") (d #t) (k 0)) (d (n "citadel-middleend") (r "^0.0.11") (d #t) (k 0)))) (h "16fkam4cm66lhf7k1vl593b81bg5w0cqm05prhd51fn15is9llda")))

