(define-module (crates-io ci ta citadel-crud) #:use-module (crates-io))

(define-public crate-citadel-crud-0.1.0 (c (n "citadel-crud") (v "0.1.0") (d (list (d (n "diesel") (r "^1.0.0-beta1") (f (quote ("sqlite"))) (d #t) (k 0)) (d (n "diesel_infer_schema") (r "^1.0.0-beta1") (f (quote ("sqlite"))) (d #t) (k 0)))) (h "1z1allxjgknjdqix7j406ipwr298ir02shmpyy89rzkrmwsmmqcl")))

(define-public crate-citadel-crud-0.1.1 (c (n "citadel-crud") (v "0.1.1") (d (list (d (n "rusqlite") (r "^0.13.0") (d #t) (k 0)))) (h "0gm1y4p996l3hm5f9qzp0nvm5xv7gmkd9p05nsy035qzjq69azry")))

