(define-module (crates-io ci d_ cid_fork_rlay) #:use-module (crates-io))

(define-public crate-cid_fork_rlay-0.3.1 (c (n "cid_fork_rlay") (v "0.3.1") (d (list (d (n "integer-encoding") (r "~1.0.3") (d #t) (k 0)) (d (n "multibase") (r "~0.6.0") (d #t) (k 0)) (d (n "multihash") (r "~0.8.0") (d #t) (k 0)))) (h "154lwcg3ri4jh1l3mlpyzc2wj2hy25ljqm7anh7ibxys19agixzj")))

