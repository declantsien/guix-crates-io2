(define-module (crates-io ci tr citrus-cli) #:use-module (crates-io))

(define-public crate-citrus-cli-0.1.0 (c (n "citrus-cli") (v "0.1.0") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "16ryxj4yjckdv04brkfbv3rvakfp4ip80ldf72cxwnz4y0kgk226")))

(define-public crate-citrus-cli-0.1.1 (c (n "citrus-cli") (v "0.1.1") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "0r5v7nbsr09h9n960klb08f4yq5sncd71kk488hcb1qz8vmkq4qd")))

(define-public crate-citrus-cli-0.1.2 (c (n "citrus-cli") (v "0.1.2") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)))) (h "19dvq5inwrpjbdw913hqdiagisvj77f3qfzqian7ydq3rvj87pdx")))

(define-public crate-citrus-cli-0.1.3 (c (n "citrus-cli") (v "0.1.3") (d (list (d (n "clap") (r "^3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "1icalds4dxg0zryg8k1f1kfzd8zps0p3k5g3zs81lq4avl128438")))

