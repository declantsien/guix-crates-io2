(define-module (crates-io ci tr citrus) #:use-module (crates-io))

(define-public crate-citrus-0.5.0 (c (n "citrus") (v "0.5.0") (d (list (d (n "c3") (r "^0.6.1") (d #t) (k 0)) (d (n "file") (r "^1.1.1") (d #t) (k 2)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "00f5hrygcnrz7mx2sfify2mf64bk8m3w6qn06fkynbra3s92qr8z") (y #t)))

(define-public crate-citrus-0.6.3 (c (n "citrus") (v "0.6.3") (d (list (d (n "c3") (r "^0.7.0") (d #t) (k 0)) (d (n "file") (r "^1.1.1") (d #t) (k 2)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "1kp6pkn42djzq1rg191fb2m3jpc29dvgdnwbasx352891rw7lbmz") (y #t)))

(define-public crate-citrus-0.8.0 (c (n "citrus") (v "0.8.0") (d (list (d (n "c3") (r "^0.9.0") (d #t) (k 0)) (d (n "file") (r "^1.1.1") (d #t) (k 2)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "16qf6wzj4v9qij69v45fica62lyw7xrgjdlhaaplir6xys4wy8va")))

(define-public crate-citrus-0.10.2 (c (n "citrus") (v "0.10.2") (d (list (d (n "c3") (r "^0.11.1") (d #t) (k 0)) (d (n "syntex_syntax") (r "^0.59.1") (d #t) (k 0)))) (h "15jsrmji89pbg2afbsr6md084kca0cmmd6ib8d14r3d79bjwrd88")))

