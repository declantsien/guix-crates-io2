(define-module (crates-io ci tr citrus-cas) #:use-module (crates-io))

(define-public crate-citrus-cas-0.0.0 (c (n "citrus-cas") (v "0.0.0") (h "0sk4fharwmh57l2gnahcaf73q7pf1iz0c1hz1j9vrvpw1gs964g4")))

(define-public crate-citrus-cas-0.0.1 (c (n "citrus-cas") (v "0.0.1") (h "1x2cmpb4jikjv9dlc59gm7r0q50qwp72gwvfhf057zfl69bbm89x")))

