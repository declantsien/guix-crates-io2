(define-module (crates-io ci tr citra-scripting) #:use-module (crates-io))

(define-public crate-citra-scripting-0.1.0 (c (n "citra-scripting") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "zmq") (r "^0.8.2") (d #t) (k 0)))) (h "0d94rv54k9jiwa2hax9xqk7nfx5qv8ybhmbvzvpsrnhmp8fdw2g2") (y #t)))

(define-public crate-citra-scripting-0.1.1 (c (n "citra-scripting") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.5.5") (d #t) (k 0)) (d (n "zmq") (r "^0.8.2") (d #t) (k 0)))) (h "0f4s40hva5b7n0dc1vcxblw2y0z2dcy8v7md8vqv1h0gfs7d0vym") (y #t)))

