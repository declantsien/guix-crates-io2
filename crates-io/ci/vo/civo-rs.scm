(define-module (crates-io ci vo civo-rs) #:use-module (crates-io))

(define-public crate-civo-rs-0.1.0 (c (n "civo-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "1nvr3mdbb0n5c09xz54w94pz16sa5wbd1zzyfwh4hhj1878h949y")))

