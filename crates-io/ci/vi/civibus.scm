(define-module (crates-io ci vi civibus) #:use-module (crates-io))

(define-public crate-civibus-0.1.0 (c (n "civibus") (v "0.1.0") (d (list (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "tectonic") (r "^0.12.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "051bdsn3igsg2hdnvd3ggqv1qv5c7mjw836bc5sj2iiip6p356il")))

(define-public crate-civibus-0.1.1 (c (n "civibus") (v "0.1.1") (d (list (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "tectonic") (r "^0.12.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "0caqghf9ynkyjs4279w73wdq90mcwfvaww8fp27n3z5w4qgz09w6")))

(define-public crate-civibus-0.1.2 (c (n "civibus") (v "0.1.2") (d (list (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "tectonic") (r "^0.12.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "06d3r9zr9vb79p3631kz8n95rafdnx0rc31an5gsrfhivakg3s6j")))

(define-public crate-civibus-0.2.0 (c (n "civibus") (v "0.2.0") (d (list (d (n "inquire") (r "^0.5.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.152") (d #t) (k 0)) (d (n "tectonic") (r "^0.12.0") (d #t) (k 0)) (d (n "toml") (r "^0.5.10") (d #t) (k 0)))) (h "1yn3f32m63qlva2p42m64g22bm6pkplnlhh0vrgrvrl6bcp4117q")))

