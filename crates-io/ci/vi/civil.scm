(define-module (crates-io ci vi civil) #:use-module (crates-io))

(define-public crate-civil-0.0.1 (c (n "civil") (v "0.0.1") (h "1wpjg2rhff27lbvmphpfdyg8zk21m0nspr6hvy2f6g3fmr9vjxrl")))

(define-public crate-civil-0.0.2 (c (n "civil") (v "0.0.2") (h "1kfjqclmy3dg247lxff76s4737g2hpgh96g7d6klv8x6g750f1na")))

(define-public crate-civil-0.0.3 (c (n "civil") (v "0.0.3") (h "1sjm38qaqxhh6ml0ln0vp2dngdb1hvrhgqfymp574brsrlvpkfcl")))

(define-public crate-civil-0.1.0 (c (n "civil") (v "0.1.0") (h "1wa6dm8w9yx809ayxylcknl9h6fdbasan1f30kfvnv1hqiwrq85r")))

(define-public crate-civil-0.1.1 (c (n "civil") (v "0.1.1") (h "0kd16ifcavsrrxwvzc17zydb0r6zprkkmk491j4pqzf7dz51w8l4")))

(define-public crate-civil-0.1.2 (c (n "civil") (v "0.1.2") (h "083rp7mglx7girza59wqhw06wi2ssxwl2b3sa9s6xl7d8kkd1q2q")))

(define-public crate-civil-0.1.3 (c (n "civil") (v "0.1.3") (h "168i2ksk0178xhn9mw8dwj5xaf9xgnbf8p8kp4dkzngg2x6s6sys")))

(define-public crate-civil-0.1.4 (c (n "civil") (v "0.1.4") (h "0dljxca3r1pnav2fw51pbm2b0hqzi7b2w80vd942r1r0rbzrb8b0")))

(define-public crate-civil-0.1.5 (c (n "civil") (v "0.1.5") (h "1z6sjmz9f3m21rg22az82wv0bk9gpbfrzxgg9z1vfk6rfv9i6am7")))

(define-public crate-civil-0.1.6 (c (n "civil") (v "0.1.6") (h "08zjyvw765a872f9iz2vm45z7wy09jsap31wrbgm9w4smszhpbzx")))

(define-public crate-civil-0.1.7 (c (n "civil") (v "0.1.7") (h "1v360a811cihrd2ih00i2k7n4prwz73nphjjibyn4axl8gxr586l")))

