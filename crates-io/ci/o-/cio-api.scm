(define-module (crates-io ci o- cio-api) #:use-module (crates-io))

(define-public crate-cio-api-0.1.0 (c (n "cio-api") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a368mapbwszli2x2myh1x52cv3bmm7ihh5sxdxqj6xk2bbm3h5w")))

(define-public crate-cio-api-0.1.1 (c (n "cio-api") (v "0.1.1") (d (list (d (n "airtable-api") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.0.11") (d #t) (k 0)) (d (n "google-drive") (r "^0.1.0") (d #t) (k 0)) (d (n "html2text") (r "^0.1") (d #t) (k 0)) (d (n "pandoc") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vj204g2p26slv42szqml0dn8rwnqhrifkyrghq8vcdz4scjzjds")))

(define-public crate-cio-api-0.1.2 (c (n "cio-api") (v "0.1.2") (d (list (d (n "airtable-api") (r "^0.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "chrono-humanize") (r "^0.0.11") (d #t) (k 0)) (d (n "google-drive") (r "^0.1.0") (d #t) (k 0)) (d (n "html2text") (r "^0.1") (d #t) (k 0)) (d (n "pandoc") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z4bg07gisispzilijnlj8c1gjmrwv0d1dr69bgrlpr4fp7p75vy")))

