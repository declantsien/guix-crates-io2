(define-module (crates-io hi lo hilog-sys) #:use-module (crates-io))

(define-public crate-hilog-sys-0.1.0 (c (n "hilog-sys") (v "0.1.0") (h "11hmm2vgpd1kr9p765jgfhgpl3gsshsc0fayzdkixpamvhym9aph")))

(define-public crate-hilog-sys-0.1.1 (c (n "hilog-sys") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "17mn8qd45mqlsngkr61b3d4gxp72lgzl7jfcymd0p9rlhmgf7q0d") (s 2) (e (quote (("log" "dep:log"))))))

