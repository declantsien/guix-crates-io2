(define-module (crates-io hi lo hilog) #:use-module (crates-io))

(define-public crate-hilog-0.1.0 (c (n "hilog") (v "0.1.0") (d (list (d (n "env_filter") (r "^0.1.0") (d #t) (k 0)) (d (n "hilog-sys") (r "^0.1.1") (f (quote ("log"))) (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1l3s5qjda5pa22240f27v2f4g5mm1fs0b3z8haf214z28nklcmhd") (r "1.77.0")))

