(define-module (crates-io hi lo hilog-binding) #:use-module (crates-io))

(define-public crate-hilog-binding-0.0.1 (c (n "hilog-binding") (v "0.0.1") (h "1mn0y388shh8cjzpgc8qb5kq4k60cm1c4wmrwra0ag2gzikzfq9z")))

(define-public crate-hilog-binding-0.0.2 (c (n "hilog-binding") (v "0.0.2") (h "1zjf1zsb088cc2i9yjslcqzzrm7hkczr32339g6rak20i34p9vbg")))

(define-public crate-hilog-binding-0.0.3 (c (n "hilog-binding") (v "0.0.3") (h "0apfslpgckbxklglh5j5ln5z21wkq8qxhm643ismwb5agsir86yq")))

(define-public crate-hilog-binding-0.0.4 (c (n "hilog-binding") (v "0.0.4") (h "1j0zfp6vqdh97298a0pq0md5pdh32niq3xck6w9zv36vfgqzhn5z")))

