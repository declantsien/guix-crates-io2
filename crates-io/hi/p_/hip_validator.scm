(define-module (crates-io hi p_ hip_validator) #:use-module (crates-io))

(define-public crate-hip_validator-1.0.0 (c (n "hip_validator") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "assert_cmd") (r "^1.0.7") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0.3") (d #t) (k 2)) (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "iso-8601") (r "^0.4.1") (d #t) (k 0)) (d (n "predicates") (r "^2.0.0") (d #t) (k 2)) (d (n "project-root") (r "^0.2.2") (d #t) (k 2)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.21") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 2)))) (h "0smk1hgzxpgjf46mb9pp3k61r8xs7y068lhwk075c9vvk0gj65i7")))

