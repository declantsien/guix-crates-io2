(define-module (crates-io hi li hilite) #:use-module (crates-io))

(define-public crate-hilite-1.0.0 (c (n "hilite") (v "1.0.0") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "04zy4q211fi24smfjlxnfggmm9xmzvbk48pl8p4qixhsf9jv6hpk")))

(define-public crate-hilite-1.0.1 (c (n "hilite") (v "1.0.1") (d (list (d (n "getopts") (r "^0.2.14") (d #t) (k 0)))) (h "0h8c8v9cql3jp1sqk95cbvsmz56cwwarvb13h9x9a8bakgpyx8cl")))

