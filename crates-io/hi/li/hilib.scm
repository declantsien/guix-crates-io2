(define-module (crates-io hi li hilib) #:use-module (crates-io))

(define-public crate-hilib-0.1.0 (c (n "hilib") (v "0.1.0") (d (list (d (n "dmidecode-rs") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "smbios-lib") (r "^0.9.1") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (d #t) (k 0)))) (h "1sikzi2hbf0naaayc71svdvzz8gz27mq6js6byscbgqlrwyndsd4") (y #t)))

