(define-module (crates-io hi da hidapi-sys) #:use-module (crates-io))

(define-public crate-hidapi-sys-0.1.0 (c (n "hidapi-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0w0yh81q3zzh7ac5752qvw5l5782vmxczqjigifkl6gqmhff9rwv") (f (quote (("static") ("build" "static"))))))

(define-public crate-hidapi-sys-0.1.1 (c (n "hidapi-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "13mw9jk86grmc7bikfk1g8wxasvnz5978fkzlnpv7ck34ij9wz2a") (f (quote (("static") ("build" "static"))))))

(define-public crate-hidapi-sys-0.1.2 (c (n "hidapi-sys") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0l7046p5w2dxg56y0rfs4q4cvf39p4pzmqaj7978l2ji08l9m0k3") (f (quote (("static") ("build" "static"))))))

(define-public crate-hidapi-sys-0.1.3 (c (n "hidapi-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0nrx42d2xqb0faayvam62llj31mfhzahca71n9v0nmn20bncqs85") (f (quote (("static") ("build" "static"))))))

(define-public crate-hidapi-sys-0.1.4 (c (n "hidapi-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gylvirk15zw464h4jk658y50a1sh6qwfh7zf52rzjn7mq8992nx") (f (quote (("static") ("build" "static"))))))

