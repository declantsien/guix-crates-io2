(define-module (crates-io hi da hidapi-alt-for-hidapi-issue-127) #:use-module (crates-io))

(define-public crate-hidapi-alt-for-hidapi-issue-127-1.2.1 (c (n "hidapi-alt-for-hidapi-issue-127") (v "1.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0d9dzq4frrd835ilvcl9cr9p2idws5ff2dassf4im25z09wnsmdg") (f (quote (("linux-static-libusb") ("linux-static-hidraw") ("linux-shared-libusb") ("linux-shared-hidraw") ("default" "linux-static-libusb")))) (l "hidapi")))

