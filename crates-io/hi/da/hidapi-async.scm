(define-module (crates-io hi da hidapi-async) #:use-module (crates-io))

(define-public crate-hidapi-async-0.1.0 (c (n "hidapi-async") (v "0.1.0") (d (list (d (n "async-std") (r "^1.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "hidapi") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1yffv31lvhqxpi7xzw0dx26y57sb55hrzsxz9jfvrx5a0wi1qpzx")))

