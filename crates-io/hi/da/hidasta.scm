(define-module (crates-io hi da hidasta) #:use-module (crates-io))

(define-public crate-hidasta-0.1.0 (c (n "hidasta") (v "0.1.0") (d (list (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "mio") (r "^0.7") (f (quote ("os-poll" "uds"))) (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "slab") (r "^0.4") (d #t) (k 0)))) (h "0ci8nwp8nxg0pxx3kxi74knnrfl3qsjfh4lww6gr7ny2kz4kyjif")))

