(define-module (crates-io hi da hidapi_rust) #:use-module (crates-io))

(define-public crate-hidapi_rust-0.0.1 (c (n "hidapi_rust") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "07fzaqg2vx43gdh9g0r6x6nmd0494gdnhi37gh9dai3y0ah6vrjb") (y #t)))

(define-public crate-hidapi_rust-0.0.2 (c (n "hidapi_rust") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1r75g8gj1pyq1krnhqxv298r3yfn94dfwvg9bg088iwa2n4xdbqn") (y #t)))

(define-public crate-hidapi_rust-0.0.3 (c (n "hidapi_rust") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1m6x1qxxpzwpkw86x63sfq5dm1vix8r60jvdk4bxrxca0j6y70r6") (y #t)))

(define-public crate-hidapi_rust-0.1.0 (c (n "hidapi_rust") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "081g0s7iddxdw8x483kj3m2gm870fpfh4zfylyp9nrapkdsb311p") (y #t)))

(define-public crate-hidapi_rust-0.1.1 (c (n "hidapi_rust") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1lf40mi0m9w1kfcyb05m6a65yjq8cifzzdp9h14lirq4rlcw5hsi") (y #t)))

(define-public crate-hidapi_rust-0.1.2 (c (n "hidapi_rust") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "01xcfzwy5ybnr4gg4xvzpplcw7hzv178h0ivw8ahsd9n2dx0943b") (y #t)))

(define-public crate-hidapi_rust-0.1.3 (c (n "hidapi_rust") (v "0.1.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1.8") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0v7spy1lym2f53mlqq5fbh76l67ph95k6p5jkm0d613430kfk446") (y #t)))

