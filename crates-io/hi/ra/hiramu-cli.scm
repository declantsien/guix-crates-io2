(define-module (crates-io hi ra hiramu-cli) #:use-module (crates-io))

(define-public crate-hiramu-cli-0.1.0 (c (n "hiramu-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "hiramu") (r "^0.1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)))) (h "0qxg63aml199xs06dfs8ibrzkjv6sls7cy2vknp34a9vkp0wc58j")))

(define-public crate-hiramu-cli-0.1.13 (c (n "hiramu-cli") (v "0.1.13") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "hiramu") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)))) (h "1zjwr2znahhkh6fk9pn5nf085iqnha87wv37bzbkkn0ik6bl968j")))

(define-public crate-hiramu-cli-0.1.14 (c (n "hiramu-cli") (v "0.1.14") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "hiramu") (r "^0.1.14") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "09iqmg0py081d4khfds64qg3wxn600hnwvp6ggi9xfcjw7lgfmw9")))

(define-public crate-hiramu-cli-0.1.15 (c (n "hiramu-cli") (v "0.1.15") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "hiramu") (r "^0.1.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1rhn4vw72zcb09scvdj7imr314gd1m3976vwp00y98g7j41ly609") (y #t)))

(define-public crate-hiramu-cli-0.1.16 (c (n "hiramu-cli") (v "0.1.16") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "hiramu") (r "^0.1.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0m0sf5979xd7qzy16bhf69y5px24xkhdl4xga8s1q4wbhf2183ji") (y #t)))

(define-public crate-hiramu-cli-0.1.23 (c (n "hiramu-cli") (v "0.1.23") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "futures-util") (r "^0.3.30") (d #t) (k 0)) (d (n "hiramu") (r "^0.1.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0afy9l59m8gsngk4dwxxpp3p7lpfy59kj2mjjv9z4rd97s63jxaz")))

