(define-module (crates-io hi dd hiddev) #:use-module (crates-io))

(define-public crate-hiddev-0.1.0 (c (n "hiddev") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "hiddev-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 0)))) (h "036cx09jlbdf31iqc1xm73g9iid1f5fbiccngznslnhvsnqx80v3")))

(define-public crate-hiddev-0.1.1 (c (n "hiddev") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "hiddev-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 0)))) (h "1bzy5vrlhy26cf56rxld7ff5zpx2kvymmdjsmb4rl770yy7683sk")))

