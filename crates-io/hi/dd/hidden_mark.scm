(define-module (crates-io hi dd hidden_mark) #:use-module (crates-io))

(define-public crate-hidden_mark-0.1.0 (c (n "hidden_mark") (v "0.1.0") (d (list (d (n "text-blind-watermark") (r "^0.1.0") (d #t) (k 0)))) (h "1rsnvy7ixbql6kzry8k1fd05qwmhm9z0khdfp5kcccmjlpjl2hrf") (y #t)))

(define-public crate-hidden_mark-0.1.2 (c (n "hidden_mark") (v "0.1.2") (d (list (d (n "calamine") (r "^0.23.1") (d #t) (k 0)) (d (n "text-blind-watermark") (r "^0.1.1") (d #t) (k 0)) (d (n "xlsxwriter") (r "^0.6.0") (d #t) (k 0)))) (h "1pzgraqzad0389xvn2jy8dvnj9k12dy4qhz6n1iipd3q66pdh1j2") (y #t)))

(define-public crate-hidden_mark-0.1.3 (c (n "hidden_mark") (v "0.1.3") (d (list (d (n "calamine") (r "^0.23.1") (d #t) (k 0)) (d (n "text-blind-watermark") (r "^0.1.1") (d #t) (k 0)) (d (n "xlsxwriter") (r "^0.6.0") (d #t) (k 0)))) (h "09sfmbli5vym2lxyxb8bgcr3000qm4240r1szsdal1ymxcqs6pvh") (y #t)))

(define-public crate-hidden_mark-0.1.4 (c (n "hidden_mark") (v "0.1.4") (d (list (d (n "calamine") (r "^0.23.1") (d #t) (k 0)) (d (n "text-blind-watermark") (r "^0.1.1") (d #t) (k 0)) (d (n "xlsxwriter") (r "^0.6.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1psb4jkmn8f02dq4f7qbqf7dwlpa7cpisgi9qqi4vh2dg65mz59r") (y #t)))

(define-public crate-hidden_mark-0.1.5 (c (n "hidden_mark") (v "0.1.5") (d (list (d (n "calamine") (r "^0.23.1") (d #t) (k 0)) (d (n "text-blind-watermark") (r "^0.1.1") (d #t) (k 0)) (d (n "xlsxwriter") (r "^0.6.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0whdd8rlcivx00h7d4swvfpcmhdwysn0lk5cg7jpyy9av9xsppqg") (y #t)))

(define-public crate-hidden_mark-0.1.6 (c (n "hidden_mark") (v "0.1.6") (d (list (d (n "calamine") (r "^0.23.1") (d #t) (k 0)) (d (n "lopdf") (r "^0.32.0") (d #t) (k 0)) (d (n "text-blind-watermark") (r "^0.1.1") (d #t) (k 0)) (d (n "xlsxwriter") (r "^0.6.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0yqm1nkp87gh5va1y582893nlm4m30j18p6a0254j38lxk7519i4") (y #t)))

(define-public crate-hidden_mark-0.2.0 (c (n "hidden_mark") (v "0.2.0") (d (list (d (n "lopdf") (r "^0.32.0") (d #t) (k 0)) (d (n "text-blind-watermark") (r "^0.1.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1954rgq7m8kplgvqmlb920skxyp204fvnq1m2dg7xvws93vw4c8d") (y #t)))

(define-public crate-hidden_mark-0.2.1 (c (n "hidden_mark") (v "0.2.1") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "lopdf") (r "^0.32.0") (d #t) (k 0)) (d (n "text-blind-watermark") (r "^0.1.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "189x9yfk1zrd596yncwdxn11ljhr2xa5pyh2qh0plwh42f87cc01") (y #t)))

(define-public crate-hidden_mark-0.2.2 (c (n "hidden_mark") (v "0.2.2") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "lopdf") (r "^0.32.0") (d #t) (k 0)) (d (n "text-blind-watermark") (r "^0.1.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "05qr60h4mv62xwh17xh415klbacv174jc8x207lxbw3v4hs1h8kg") (y #t)))

(define-public crate-hidden_mark-0.2.3 (c (n "hidden_mark") (v "0.2.3") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "lopdf") (r "^0.32.0") (d #t) (k 0)) (d (n "text-blind-watermark") (r "^0.1.2") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "19rg71j5x3cq4sipri9d9aydr2axl4dd8bgfbxh4wy170926zf04") (y #t)))

(define-public crate-hidden_mark-0.2.4 (c (n "hidden_mark") (v "0.2.4") (d (list (d (n "image") (r "^0.24.8") (d #t) (k 0)) (d (n "lopdf") (r "^0.32.0") (d #t) (k 0)) (d (n "text-blind-watermark") (r "^0.1.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1aii7kb6rxk63bkmx194fyr92s53gfm893hawh8ml7afz465927k") (y #t)))

(define-public crate-hidden_mark-0.2.5 (c (n "hidden_mark") (v "0.2.5") (h "00sdd90087zc9blxkraylrzifx1vl5gqqp2nf0xxw2h2par44j3q") (y #t)))

