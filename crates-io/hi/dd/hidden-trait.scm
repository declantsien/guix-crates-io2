(define-module (crates-io hi dd hidden-trait) #:use-module (crates-io))

(define-public crate-hidden-trait-0.1.0 (c (n "hidden-trait") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0x7phwhh2vymawjrk7bb6wnar559ifj3ca1bzzd7k694rjc334g3")))

(define-public crate-hidden-trait-0.1.1 (c (n "hidden-trait") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rp08zqr38r3di34gn6qhlryzq8sbpjh0xg00x6nzd54q6ik8pwn")))

(define-public crate-hidden-trait-0.1.2 (c (n "hidden-trait") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pv0i3h0qn0hn0q8r655jv9wn2ckpsgx1rq7xjdq9b1q0j2rxvb8")))

