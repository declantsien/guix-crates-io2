(define-module (crates-io hi dd hidden-median) #:use-module (crates-io))

(define-public crate-hidden-median-0.1.0 (c (n "hidden-median") (v "0.1.0") (h "0q3v9qgxns6anvkk87vrbwpb9rri5jb2sg60bl1i1hg9b6kg1pkn")))

(define-public crate-hidden-median-0.1.1 (c (n "hidden-median") (v "0.1.1") (h "1lxx4askl9yyh9gf2db897mnk4cjqr13ra32j56r6pg1k785dg9x")))

(define-public crate-hidden-median-0.1.2 (c (n "hidden-median") (v "0.1.2") (h "1lcdcjj61rk7mdf84wjb640nf2j0f19avvgmj31fnjgm2ql8f3bq")))

(define-public crate-hidden-median-0.1.3 (c (n "hidden-median") (v "0.1.3") (h "13cz6cffigw9n6cvnad0xl1dw33ns2n3bkp27ljygb1wirmizpkw")))

