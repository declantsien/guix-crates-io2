(define-module (crates-io hi dd hiddev-sys) #:use-module (crates-io))

(define-public crate-hiddev-sys-0.1.0 (c (n "hiddev-sys") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)))) (h "1nxyj78w7q9889xlk735fvj3nlwjcvrkr0czgsbycwk4ky4yxhg9")))

(define-public crate-hiddev-sys-0.1.1 (c (n "hiddev-sys") (v "0.1.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)))) (h "0xl3hfcglmr23da2980qi6mdw7vnw6pykmkcy4ihxlilgf2icafi")))

(define-public crate-hiddev-sys-0.1.2 (c (n "hiddev-sys") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 0)))) (h "0hxz76099iymvv99wr2rfs09wpc2fsrmg3vb5f8rrcrp7901sxdy")))

(define-public crate-hiddev-sys-0.1.3 (c (n "hiddev-sys") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.9") (d #t) (k 0)) (d (n "test-log") (r "^0.2.11") (d #t) (k 0)))) (h "03bym5ff20pi3wgpl2qn9q3f0mik06v12xz1qfg1a9wfy1l7834y")))

