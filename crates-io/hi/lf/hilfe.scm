(define-module (crates-io hi lf hilfe) #:use-module (crates-io))

(define-public crate-hilfe-0.1.0 (c (n "hilfe") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "openai") (r "^1.0.0-alpha.14") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "os_info") (r "^3.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0cm1z46nrn6i0prsqsprrxhn54hx1m18avqswn05rqvzaiagbz8w")))

(define-public crate-hilfe-0.1.1 (c (n "hilfe") (v "0.1.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "openai") (r "^1.0.0-alpha.14") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "os_info") (r "^3.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "10hgnqlxqm2sf308vq465d20x0gsnkkp5ys26b2f6xm15i9w60rs")))

(define-public crate-hilfe-0.2.0 (c (n "hilfe") (v "0.2.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "openai") (r "^1.0.0-alpha.14") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "os_info") (r "^3.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0dvs86v8w8jhyykczm9facqmxg3b0j8zgnb5jsxvzj2ms3d9x98b")))

(define-public crate-hilfe-0.2.1 (c (n "hilfe") (v "0.2.1") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "openai") (r "^1.0.0-alpha.14") (f (quote ("rustls"))) (d #t) (k 0)) (d (n "os_info") (r "^3.8.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1haf3s3vzrghd86mv3jcbnrdz2d7554lkx5mq2r7ahjc28fgyj5c")))

