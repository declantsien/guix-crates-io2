(define-module (crates-io hi st histogram) #:use-module (crates-io))

(define-public crate-histogram-0.1.2 (c (n "histogram") (v "0.1.2") (h "0hyxhs8x0mfyfgrcx8rg6k0h5iszmv3lpy3hy7hn57zmir36sxjh")))

(define-public crate-histogram-0.1.3 (c (n "histogram") (v "0.1.3") (h "1va3pf2p8kgzqqnih5c0fvz0y8dddi20lxqxk0hajbjjkimad4a8")))

(define-public crate-histogram-0.1.4 (c (n "histogram") (v "0.1.4") (h "1nqx959c3gxfvp08lm7wab9f4ggs6wamq3gqwwpbdpp83zmsv2qh")))

(define-public crate-histogram-0.1.5 (c (n "histogram") (v "0.1.5") (h "12fnms54i5b16hc6ip2k5fqvc0i8s079ar16lrg613m2y5isj513")))

(define-public crate-histogram-0.1.6 (c (n "histogram") (v "0.1.6") (h "147g9b7bf2hk74xj1i5m67kja6qadad833ag4g6s7padzxpjnyhh")))

(define-public crate-histogram-0.2.0 (c (n "histogram") (v "0.2.0") (h "0hghr7zh8iixlglqdia3pip1a0xprgrfyzrq8abc4frn1spvzc4s")))

(define-public crate-histogram-0.2.1 (c (n "histogram") (v "0.2.1") (h "1x76fzj101wqkq97pxcv7s1h1kj7xr58akf4301f4vsqxj9ifb8b")))

(define-public crate-histogram-0.2.2 (c (n "histogram") (v "0.2.2") (h "037xb6wkldph4b0k6nd41jq0kb2wa4vpxq8r210paxns4inzd2hg")))

(define-public crate-histogram-0.3.0 (c (n "histogram") (v "0.3.0") (h "0wsvxladny7pzzlicfn0s1lrjy5a15kzbwwj31rwali2hn2awg32")))

(define-public crate-histogram-0.3.1 (c (n "histogram") (v "0.3.1") (h "0x4x6sdk9fxpyy8hqrr9ccsli62ib7fsnbm4l1hq7fzqqanc4qib")))

(define-public crate-histogram-0.3.2 (c (n "histogram") (v "0.3.2") (h "0hp5fzll41dcq76n89m8zifqbjr337y79xx5izj4bjjmgc7hnkdk")))

(define-public crate-histogram-0.3.3 (c (n "histogram") (v "0.3.3") (h "0bhshlix8h8fdfy7fflp4m8crbmx9gf8s5vlns7npcixf449rwwr")))

(define-public crate-histogram-0.3.4 (c (n "histogram") (v "0.3.4") (h "10qaa9pbpr0l5bvjnnivc3j7m59qcqy5g0k8bixx7s36sgm94x8k")))

(define-public crate-histogram-0.3.5 (c (n "histogram") (v "0.3.5") (h "0zirqrybgin0hkmcnngvkigxns82galvx99xcql0rkcgxvx31wl0")))

(define-public crate-histogram-0.3.6 (c (n "histogram") (v "0.3.6") (h "1bhahvimg01l6d6pshiv4g82sxbygs9mgg7796gw25mxqcxlx089")))

(define-public crate-histogram-0.4.0 (c (n "histogram") (v "0.4.0") (h "1wc7pwjcyrv57074rskzxgfscch0x5a6w465364wf84k9q8dvy3h")))

(define-public crate-histogram-0.5.0 (c (n "histogram") (v "0.5.0") (h "1kykis2b06nmpc2jwqm7lsmrgncfylqpvm420wcb8d1maayw700k")))

(define-public crate-histogram-0.5.1 (c (n "histogram") (v "0.5.1") (h "1s2b651m4jdmg0jpwjxrhsm6gmrra8p5bxp5k6jyyj714c095xyh")))

(define-public crate-histogram-0.6.0 (c (n "histogram") (v "0.6.0") (h "18yxgyc4l6s98hhzli4065n54m4zhg6s0nvgkf77fp95zq6ajvys")))

(define-public crate-histogram-0.6.1 (c (n "histogram") (v "0.6.1") (h "1b07z5q9mn0b802hj469syahsyxzn8hxfvmahw7my2gh4a3y6vzc")))

(define-public crate-histogram-0.6.2 (c (n "histogram") (v "0.6.2") (h "0q1hw8k7svclhcvpy8k61nmwbna3cvj6p4c5aww9dij6bbivrjch")))

(define-public crate-histogram-0.6.3 (c (n "histogram") (v "0.6.3") (h "1c89yk6a3mq84wrrzz0mdvgp91vlbk8ydpk85j92lj62vqsx2rg0")))

(define-public crate-histogram-0.6.4 (c (n "histogram") (v "0.6.4") (h "0n9f5wmr68dqz753gvsr0kp83kiafm7xnc0qf4j18760vqbnacwb")))

(define-public crate-histogram-0.6.5 (c (n "histogram") (v "0.6.5") (h "05xkra69ylxbr2nrcmwhq7mdkcsh88xwwvldmd3vrqzhyz97vsfi")))

(define-public crate-histogram-0.6.6 (c (n "histogram") (v "0.6.6") (h "178i7v1dpsnlsqzjn4ls2h6mapyk26z937m295gpv6pwbj6xf156")))

(define-public crate-histogram-0.6.7 (c (n "histogram") (v "0.6.7") (h "1r9dnhdkshplzqx6v307y4zw9jh8054mx41fb7ac8nf90a2dlqk1")))

(define-public crate-histogram-0.6.8 (c (n "histogram") (v "0.6.8") (h "17fcykfd47x7s5bldwxk62173xbayxxfm12kd0drdjn1ji0frp0v")))

(define-public crate-histogram-0.6.9 (c (n "histogram") (v "0.6.9") (h "0s9660nyacb5dgb5qxzgbf6lxrki1fmmgcalwm38c2r9rcn8ijqj")))

(define-public crate-histogram-0.7.0 (c (n "histogram") (v "0.7.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "05ph937kpwaslfnmgwrzvkr2l5c1mr0g2fdm4gdqmg1pw9m0cgli")))

(define-public crate-histogram-0.7.1 (c (n "histogram") (v "0.7.1") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0mdx5qxlrdk9jdijrk1jlgy0wm899spanrlh4vxi621ywhgdpkgj")))

(define-public crate-histogram-0.7.2 (c (n "histogram") (v "0.7.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0xq1h8cv3dwwya8pvwhazck8ibnfwvrx2zbyhib31pgzhaczk7sc")))

(define-public crate-histogram-0.7.3 (c (n "histogram") (v "0.7.3") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "08q9cz7y5z3mmlx73ixjdqxl80gcf616ig1pa3nxs8bvmss8p5yh")))

(define-public crate-histogram-0.7.4 (c (n "histogram") (v "0.7.4") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "02xq0k7qryxan5k2xzqp01n66hrbhpanwfwhr31da6cn48vx2wz6") (f (quote (("serde-serialize" "serde") ("default" "serde-serialize"))))))

(define-public crate-histogram-0.8.0 (c (n "histogram") (v "0.8.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0y06zmxx5i0wdipv50h0q823x84mjhx676r6vk73qi7027459glb")))

(define-public crate-histogram-0.8.1 (c (n "histogram") (v "0.8.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0sara897s79713srwig1mrd8swjmrvmrbr12vc97sznq0dihkcl5")))

(define-public crate-histogram-0.8.2 (c (n "histogram") (v "0.8.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0c3bi5kn3n1gynbmw7hf9j9fmp1ss3mgmznk2pgnf5c2mzl3klq0") (f (quote (("serde-serialize" "serde"))))))

(define-public crate-histogram-0.8.3 (c (n "histogram") (v "0.8.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0i7i95sd2ixc95nbdi3w6wmmmm0gjmhjii6g4kx3z12hxz60kk3j") (f (quote (("serde-serialize" "serde"))))))

(define-public crate-histogram-0.8.4 (c (n "histogram") (v "0.8.4") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0nbi8iy4bgf0ay0dsp36dlq2x3j1rrr425w1shfiz3azmg45j3yy") (f (quote (("serde-serialize" "serde")))) (s 2) (e (quote (("schemars" "dep:schemars" "serde-serialize"))))))

(define-public crate-histogram-0.9.0 (c (n "histogram") (v "0.9.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0kbd1vjacm1r0ls1gnmnk38wamn1wcwrqxam24dcz24ki63r9vp5") (s 2) (e (quote (("serde" "dep:serde") ("schemars" "dep:schemars" "serde"))))))

(define-public crate-histogram-0.9.1 (c (n "histogram") (v "0.9.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0x8ixfg00nnq1zp0akhdc0qfjykdybid8dkq28p6cqwaxf846qsb") (s 2) (e (quote (("serde" "dep:serde") ("schemars" "dep:schemars" "serde"))))))

(define-public crate-histogram-0.10.0 (c (n "histogram") (v "0.10.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1f8fydc4mgm99m12gsxn6fwmhcd3qzzjiwb2azkifax3fpfvvlzl") (s 2) (e (quote (("serde" "dep:serde") ("schemars" "dep:schemars" "serde"))))))

(define-public crate-histogram-0.10.1 (c (n "histogram") (v "0.10.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "schemars") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "0b5ylvw5w8ncdyzma88xjy2xjpya5wrkfa89y9lail19lswirn2v") (s 2) (e (quote (("serde" "dep:serde") ("schemars" "dep:schemars" "serde"))))))

