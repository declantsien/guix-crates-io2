(define-module (crates-io hi st hist-cli) #:use-module (crates-io))

(define-public crate-hist-cli-0.1.0 (c (n "hist-cli") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0m1960dkma3sspzx0xlsmkfdny52k15k8znq3ibd1yafmmsv5dy8")))

(define-public crate-hist-cli-0.2.0 (c (n "hist-cli") (v "0.2.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1i4py4xjfz24cc2hskm14cs5h3byg8ngz1a9sca20d8l6np9ysg6")))

(define-public crate-hist-cli-0.3.0 (c (n "hist-cli") (v "0.3.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0hnzyr7994wqqx6diz35cw2afdmmwh4702fnm4ggbz154b9lwg2p")))

(define-public crate-hist-cli-0.3.1 (c (n "hist-cli") (v "0.3.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1c0zsf8806m8g1vcarva8xl8gfm9yjhqiwzc75rwhp6f96bfca54")))

(define-public crate-hist-cli-0.4.0 (c (n "hist-cli") (v "0.4.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textplots") (r "^0.6") (d #t) (k 0)))) (h "0hik0s1y2p3cxxzn8xgs7fp6f35zf4kpz5cgjz479jw65kyvyd6w")))

(define-public crate-hist-cli-0.4.1 (c (n "hist-cli") (v "0.4.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textplots") (r "^0.8") (d #t) (k 0)))) (h "0xji5ml2lv9wd02jlphpqa7jgr09zzj8zxq4ilw8b7nkhciskx9r")))

(define-public crate-hist-cli-0.4.2 (c (n "hist-cli") (v "0.4.2") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textplots") (r "^0.8") (d #t) (k 0)))) (h "13byxq7vm8papm3bgjbqj3khyk0r22m1ssfq6vz84f5mgy05xwdi")))

(define-public crate-hist-cli-0.4.3 (c (n "hist-cli") (v "0.4.3") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textplots") (r "^0.8") (d #t) (k 0)))) (h "1ib1nc948jcanr5qs7qxwyangf5jn1w2128vbfak5gbzai1bi4w3")))

(define-public crate-hist-cli-0.4.4 (c (n "hist-cli") (v "0.4.4") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textplots") (r "^0.8") (d #t) (k 0)))) (h "1n48l3331k8xn5770cy19lxqfkm946w2fmzxgwp3ffswcclmsssm")))

(define-public crate-hist-cli-0.4.5 (c (n "hist-cli") (v "0.4.5") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textplots") (r "^0.8") (d #t) (k 0)))) (h "1c9c1bmjbfl9dkyhqz7j3jan5q98fkg5k9b1aqq5dw132hs0q5cx")))

(define-public crate-hist-cli-0.4.6 (c (n "hist-cli") (v "0.4.6") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "plotters") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textplots") (r "^0.8") (d #t) (k 0)))) (h "0gw7dc7w8lg8wm9xmjn1qsjxy9g97i5qzif47vibsk03cxydx42d")))

