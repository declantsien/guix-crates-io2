(define-module (crates-io hi st histodu) #:use-module (crates-io))

(define-public crate-histodu-0.1.0 (c (n "histodu") (v "0.1.0") (d (list (d (n "bytesize") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (o #t) (d #t) (k 1)) (d (n "clap_complete") (r "^4") (o #t) (d #t) (k 1)) (d (n "hdrhistogram") (r "^7") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)) (d (n "scoped-tls") (r "^1") (d #t) (k 0)))) (h "0ssi2bbidy0a5236wxnya5s1c3ykz6vkr3mrgfcy78pm9zzk50pr") (f (quote (("default")))) (s 2) (e (quote (("completion" "dep:clap" "dep:clap_complete")))) (r "1.70")))

