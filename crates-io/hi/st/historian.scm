(define-module (crates-io hi st historian) #:use-module (crates-io))

(define-public crate-historian-1.0.0 (c (n "historian") (v "1.0.0") (d (list (d (n "coco") (r "^0.2") (d #t) (k 0)))) (h "0k6sf2jvhk6ri5l1alcd03m3bpvxczxvagqrnpwqhdxxa9izs8jh")))

(define-public crate-historian-1.0.1 (c (n "historian") (v "1.0.1") (d (list (d (n "coco") (r "^0.2") (d #t) (k 0)))) (h "03kzm85bqfz68v66dimjz17a48l4vn7a1a6c0y4rlr55nmfxzgcg")))

(define-public crate-historian-2.0.0 (c (n "historian") (v "2.0.0") (d (list (d (n "coco") (r "^0.2") (d #t) (k 0)))) (h "1a44zh0r00ck87lrvjk0nz6ndhr25wy12zyz8ry67avzl9mnm04f")))

(define-public crate-historian-2.0.1 (c (n "historian") (v "2.0.1") (d (list (d (n "coco") (r "^0.2") (d #t) (k 0)))) (h "0wyzl3fnapqm0acnmcl7ap41qwl26b6zjinn0lbnjaggl6mi9vf2")))

(define-public crate-historian-2.0.2 (c (n "historian") (v "2.0.2") (d (list (d (n "coco") (r "^0.2") (d #t) (k 0)))) (h "0x7bbp5dm6iahvyv1b6sla9q9v43s60fkwsbmd592x3y29f017hc")))

(define-public crate-historian-3.0.0 (c (n "historian") (v "3.0.0") (d (list (d (n "coco") (r "^0.2") (d #t) (k 0)))) (h "18aff4h9q1k78q55cv58ndpyri21g267p3xqja4n3f09dikm812p")))

(define-public crate-historian-3.0.2 (c (n "historian") (v "3.0.2") (d (list (d (n "coco") (r "^0.2") (d #t) (k 0)))) (h "0dnpbysk8yr6vxgqln2hy9s1rrb3ghvd50jkwbpcy98dcbsbr9ax")))

(define-public crate-historian-3.0.3 (c (n "historian") (v "3.0.3") (d (list (d (n "coco") (r "^0.2") (d #t) (k 0)))) (h "0mp3blr1yjiz32yi7scchl2926vvqf87ayqkz3smsfzqk6770dw7")))

(define-public crate-historian-3.0.4 (c (n "historian") (v "3.0.4") (d (list (d (n "coco") (r "^0.2") (d #t) (k 0)))) (h "01z7p96d1a003akr55m8fmsspvkbdms1wcvrzg6w4lvdg6509f66") (f (quote (("default") ("bypass"))))))

(define-public crate-historian-3.0.6 (c (n "historian") (v "3.0.6") (d (list (d (n "crossbeam-epoch") (r "^0.7") (d #t) (k 0)))) (h "0lpw8jvpq1iy8zfq4nrpz1ixz5h9ixcqxa0sv2smnxs4dkln21h8") (f (quote (("default") ("bypass"))))))

(define-public crate-historian-3.0.7 (c (n "historian") (v "3.0.7") (d (list (d (n "crossbeam-epoch") (r "^0.3") (d #t) (k 0)))) (h "1782y0q4mpffxrixzl3zh9bg7hqqbgn6wv0nariqs0x6j3lp1zdw") (f (quote (("default") ("bypass"))))))

(define-public crate-historian-3.0.8 (c (n "historian") (v "3.0.8") (d (list (d (n "crossbeam-epoch") (r "^0.7") (d #t) (k 0)))) (h "11jphsk6lrgiv32w4933iqr48bb780na0zpyg1m4vnmqbvlfp47w") (f (quote (("default") ("bypass"))))))

(define-public crate-historian-3.0.9 (c (n "historian") (v "3.0.9") (d (list (d (n "sled_sync") (r "^0.2") (d #t) (k 0)))) (h "0dmkggymz04q8d6i24bknbnch9cf452ry588vjmybshymqbb1sfb") (f (quote (("default") ("bypass"))))))

(define-public crate-historian-3.0.10 (c (n "historian") (v "3.0.10") (d (list (d (n "sled_sync") (r "^0.2") (d #t) (k 0)))) (h "0hhm38kwmrky1d67adifhygd5gzpxa40q8bxlq0cfa1vfhahr0jf") (f (quote (("default") ("bypass"))))))

(define-public crate-historian-3.0.11 (c (n "historian") (v "3.0.11") (d (list (d (n "sled_sync") (r "^0.2.2") (d #t) (k 0)))) (h "0s6vnvwvgbz090v8gh2mzjaw9ccqs3qw5ywvhpn13ipa3y5d8691") (f (quote (("default") ("bypass"))))))

(define-public crate-historian-4.0.1 (c (n "historian") (v "4.0.1") (h "197nygii46madmk679fi4clmivbik6m8sjls16zx3gjr7hjlavdh")))

(define-public crate-historian-4.0.2 (c (n "historian") (v "4.0.2") (h "080wzpkry2h1z4ss90w33d2cbqssh86yyn63g2faxfiyx2n1wixf")))

(define-public crate-historian-4.0.3 (c (n "historian") (v "4.0.3") (h "174qvyvv3ibwq8i9d14ak44y9bqgjwbqhjgil519ddjnnxp42lzn") (f (quote (("disable") ("default"))))))

(define-public crate-historian-4.0.4 (c (n "historian") (v "4.0.4") (h "0mvnj4j07f270xiblzg0z0lw2amfxcz3clckpbxjv760ngylxrya") (f (quote (("single_threaded") ("disable") ("default"))))))

