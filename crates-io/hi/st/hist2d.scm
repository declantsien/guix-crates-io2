(define-module (crates-io hi st hist2d) #:use-module (crates-io))

(define-public crate-hist2d-0.1.0 (c (n "hist2d") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "127jzwab0n4qazq9a4bb368qi6r8n5qakj980frsicw3vgmc3i84")))

(define-public crate-hist2d-0.1.1 (c (n "hist2d") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1l87qfcl0ir9qryx97h1zcpzbny2kpvcwi4cr69arq6d16bprsqz")))

(define-public crate-hist2d-0.1.2 (c (n "hist2d") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.0") (f (quote ("yaml"))) (d #t) (k 0)))) (h "1478irh1vhs51872cp1g15kg8sdgkr9fwcx1nqdjy03v0bxfms1y")))

