(define-module (crates-io hi st histo) #:use-module (crates-io))

(define-public crate-histo-0.1.0 (c (n "histo") (v "0.1.0") (d (list (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)))) (h "0n724l9j6l0d3mpimi5lhzb80fvlmn1izd2k0gjyic8n9cc9ahas")))

(define-public crate-histo-1.0.0 (c (n "histo") (v "1.0.0") (d (list (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)))) (h "0v6znd33clam2b37rhn2pldd39l61605s1ivxzpjwdygi8f6mad6")))

