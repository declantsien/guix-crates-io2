(define-module (crates-io hi st histlog) #:use-module (crates-io))

(define-public crate-histlog-0.1.4 (c (n "histlog") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "hdrhistogram") (r "^6") (d #t) (k 0)))) (h "1lcawcqr13cwdckzqadm4zawj78aw1wms4x96igqzk82y0ljwncq")))

(define-public crate-histlog-0.1.5 (c (n "histlog") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "hdrhistogram") (r "^6") (d #t) (k 0)))) (h "0piz9ld47vfxxyjj771p9zr5f26qr7s0134qdjw40a32x6wd0j4b")))

(define-public crate-histlog-0.1.6 (c (n "histlog") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "hdrhistogram") (r "^6") (d #t) (k 0)))) (h "1a6iadn3ryaz3qql6r0m8q525md578w2qdydmdfdpbkn2yfcnxa5")))

(define-public crate-histlog-0.1.7 (c (n "histlog") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "hdrhistogram") (r "^6") (d #t) (k 0)))) (h "1qy9inv65i8p04hai7gdc6xbdbghv19rll7q521fnkinynssslp4")))

(define-public crate-histlog-1.0.0 (c (n "histlog") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 0)) (d (n "hdrhistogram") (r "^6") (d #t) (k 0)))) (h "1pg2aalkxmwx9xm4gyjib00pn5ag68ljlj76bwywf5d4i2wnn85f")))

(define-public crate-histlog-2.0.0-rc.1 (c (n "histlog") (v "2.0.0-rc.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.4") (d #t) (k 0)) (d (n "minstant") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1bmcg4y67hjw454fiizb2hm53j52lvfyxg1mih5lvq6scp5m3vmp") (f (quote (("default" "minstant"))))))

(define-public crate-histlog-2.0.0-rc.2 (c (n "histlog") (v "2.0.0-rc.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.4") (d #t) (k 0)) (d (n "minstant") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1ikhr3yfyyhy7pnvpb47dvj6b07l1m7hyy0nvg82fkbhd7l92nlq") (f (quote (("default" "minstant"))))))

(define-public crate-histlog-2.0.0-rc.3 (c (n "histlog") (v "2.0.0-rc.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.4") (d #t) (k 0)) (d (n "minstant") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "16xqx89bi3lf4qdvyw9475j1yflzhkpzr5aj2ivvkyxdv30ns4n7") (f (quote (("default" "minstant"))))))

(define-public crate-histlog-2.0.0-rc.4 (c (n "histlog") (v "2.0.0-rc.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.4") (d #t) (k 0)) (d (n "minstant") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0ghl8lyw1bf3ipfvsiz13gxx8irb8fh5zbfzmb9av81zms2hl0v2") (f (quote (("default" "minstant"))))))

(define-public crate-histlog-2.0.0-rc.5 (c (n "histlog") (v "2.0.0-rc.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.4") (d #t) (k 0)) (d (n "minstant") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0rpqlg3jhpkqgzyly942scny5lraaa4f513w01g9iaakiyvyf3q9") (f (quote (("default" "minstant"))))))

(define-public crate-histlog-2.0.0-rc.6 (c (n "histlog") (v "2.0.0-rc.6") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.4") (d #t) (k 0)) (d (n "minstant") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "0im7whmxvdllp0224isa1cgsc36xrbj3252qc1bkzyzldqv5axcw") (f (quote (("default" "minstant"))))))

(define-public crate-histlog-2.0.0-rc.7 (c (n "histlog") (v "2.0.0-rc.7") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.4") (d #t) (k 0)) (d (n "minstant") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1m9b8v3vcpwxqfpyggzw4vj510mqnzc0ab2lzppbk0pnm2nfbm7v") (f (quote (("default" "minstant"))))))

(define-public crate-histlog-2.0.0 (c (n "histlog") (v "2.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.4") (d #t) (k 0)) (d (n "minstant") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "1jlc4y9117569x278sf7vmvzb12np5kl5ib0kx4p8l63yaaiwry2") (f (quote (("default" "minstant"))))))

(define-public crate-histlog-2.1.0 (c (n "histlog") (v "2.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.4") (d #t) (k 0)) (d (n "minstant") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "145aa6b6cjjkzllv53xv446nllhmaidkrmd09l1cwm9xsq1r5395") (f (quote (("default" "minstant"))))))

(define-public crate-histlog-2.1.1 (c (n "histlog") (v "2.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "hdrhistogram") (r "^7.5.4") (d #t) (k 0)) (d (n "minstant") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "smol_str") (r "^0.2.0") (o #t) (d #t) (k 0)))) (h "105z2shqlsihw9iqvdfw01xdkkrkpcyz2carw9z90g77nsnvnamg") (f (quote (("default" "minstant"))))))

