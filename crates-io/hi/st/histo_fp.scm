(define-module (crates-io hi st histo_fp) #:use-module (crates-io))

(define-public crate-histo_fp-0.2.0 (c (n "histo_fp") (v "0.2.0") (d (list (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)))) (h "18r70z238nryaxdh73w73d7maml9sl5px6wng5101giv6w07ps28")))

(define-public crate-histo_fp-0.2.1 (c (n "histo_fp") (v "0.2.1") (d (list (d (n "noisy_float") (r "^0.1.12") (d #t) (k 0)) (d (n "quickcheck") (r "^0.4.1") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "streaming-stats") (r "^0.1.28") (d #t) (k 0)))) (h "1csp6fjx8wixmg174j3iq6dya3bcdg45by5kmq7qv2f3z4qcgf7k")))

