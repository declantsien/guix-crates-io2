(define-module (crates-io hi st histongram) #:use-module (crates-io))

(define-public crate-histongram-0.1.0 (c (n "histongram") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fxhash") (r "^0.2.1") (o #t) (d #t) (k 0)) (d (n "insta") (r "^1.20.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "0m2rcrwdyyxjy03nhkzg13pabxdnpi0y78fdlynmn1af98yyvyyn") (f (quote (("default" "fxhash")))) (s 2) (e (quote (("serde" "dep:serde") ("fxhash" "dep:fxhash"))))))

(define-public crate-histongram-0.2.0 (c (n "histongram") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.0") (d #t) (k 2)) (d (n "compact_str") (r "^0.6.1") (d #t) (k 2)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "hashbrown") (r "^0.12.3") (d #t) (k 0)) (d (n "insta") (r "^1.20.0") (f (quote ("ron"))) (d #t) (k 2)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 2)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)))) (h "06p75lnbv65l3i62jbbg8qsmh6y8bpl3kjb3jsp5zm79lw4h08l5") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "hashbrown/serde"))))))

