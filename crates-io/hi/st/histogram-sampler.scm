(define-module (crates-io hi st histogram-sampler) #:use-module (crates-io))

(define-public crate-histogram-sampler-0.1.0 (c (n "histogram-sampler") (v "0.1.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0xk3rqfawl12xk7xsl07rlg5i2p0g8nrym3jl5rii72m4d7ccyb5")))

(define-public crate-histogram-sampler-0.1.1 (c (n "histogram-sampler") (v "0.1.1") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "13rlra03r9klzz7dq9p8242502wmw4p2jm7cc1qzvbr2bq9d4yf4")))

(define-public crate-histogram-sampler-0.1.2 (c (n "histogram-sampler") (v "0.1.2") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "0iy6fry302nyrlk8q1n45f2kxz3s7ylk43879nk2zyx00lmlkzgi")))

(define-public crate-histogram-sampler-0.1.3 (c (n "histogram-sampler") (v "0.1.3") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1x7mk24m8np6v9qhz7lsz8f3s82zarw97f2kqcvl0dy7kqzq1cgb")))

(define-public crate-histogram-sampler-0.2.0 (c (n "histogram-sampler") (v "0.2.0") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "15r7s5fx9051wbs8x3kk3mwjahfb7xbsvw3s1lhcaq8zr4v2mzxh")))

(define-public crate-histogram-sampler-0.2.1 (c (n "histogram-sampler") (v "0.2.1") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1bhxw6yn5xg8bzpw4d4f5kqdzwkxf4nj3pv7f1zndr4r34yhap5m")))

(define-public crate-histogram-sampler-0.3.0 (c (n "histogram-sampler") (v "0.3.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "1jbbrr9z3sq3rl8q22cvvlf915iin2gadx0pdlf1rqxi2dklcpyk")))

(define-public crate-histogram-sampler-0.4.0 (c (n "histogram-sampler") (v "0.4.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0rfmmxinb6kkaijas9ssbrl4xsh97fwpsmna412sj88m5dg079h0")))

(define-public crate-histogram-sampler-0.5.0 (c (n "histogram-sampler") (v "0.5.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0yy5ik4b16avjhwvbzyiv26pz7xm9hciljls98ni0p4bq4y0ga7p")))

