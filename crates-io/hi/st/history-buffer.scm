(define-module (crates-io hi st history-buffer) #:use-module (crates-io))

(define-public crate-history-buffer-0.1.0 (c (n "history-buffer") (v "0.1.0") (h "0symfdsgrd1mmbsncny54155203smv8zv6sa68sw06bl2yxhnh77")))

(define-public crate-history-buffer-0.1.1 (c (n "history-buffer") (v "0.1.1") (h "0ml2il5lv6vkblvc2wnrds4n1jd0889w39qxwrygxsq0mkgc6fj0")))

(define-public crate-history-buffer-0.1.2 (c (n "history-buffer") (v "0.1.2") (h "0a1kf383mhzj936fxy2pgl0h7gyjq6wm2bxxxqql9925an40kbcd")))

