(define-module (crates-io hi ni hinix) #:use-module (crates-io))

(define-public crate-hinix-0.1.0 (c (n "hinix") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.11") (d #t) (k 0)))) (h "0hs82hz6njcp69y57a0pi734kq5x63n5vdyr7hbcvn4nkf3w0y14")))

(define-public crate-hinix-0.2.0 (c (n "hinix") (v "0.2.0") (d (list (d (n "nix") (r "^0.23") (d #t) (k 0)))) (h "1v5xw6ik8q4a695zfih47mri6x1pj8cfixdzpczcksz3ahcnklpm")))

(define-public crate-hinix-0.2.1 (c (n "hinix") (v "0.2.1") (d (list (d (n "clap") (r "^2.34") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "07gvqkmbfj9hhsd7d1fi8l6n5kky5kzflpgmjl9264dj68p3qmhg") (f (quote (("utils" "clap") ("default"))))))

