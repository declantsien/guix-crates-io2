(define-module (crates-io hi ta hitaki) #:use-module (crates-io))

(define-public crate-hitaki-0.0.90 (c (n "hitaki") (v "0.0.90") (d (list (d (n "ffi") (r "^0.0.90") (d #t) (k 0) (p "hitaki-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "1n2fa2hhb78kbk1sgswxwmk5ggdnmiqv5qw2rg1pazzm298jnxgk") (y #t)))

(define-public crate-hitaki-0.0.91 (c (n "hitaki") (v "0.0.91") (d (list (d (n "ffi") (r "^0.0.91") (d #t) (k 0) (p "hitaki-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "0j2w2ik32grdhdqcyvwywhl0ldhbvy4hn29kbi6cyy6845m4hi79") (f (quote (("dox" "ffi/dox" "glib/dox")))) (y #t)))

(define-public crate-hitaki-0.2.0 (c (n "hitaki") (v "0.2.0") (d (list (d (n "ffi") (r "^0.2.0") (d #t) (k 0) (p "hitaki-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "1x4j054fj3a1w095cb43iircdkkryigmrf25nxr0fnirqm0n6bs8") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-hitaki-0.3.0 (c (n "hitaki") (v "0.3.0") (d (list (d (n "ffi") (r "^0.3") (d #t) (k 0) (p "hitaki-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "0c79cnics39lfj2ll2kp8hxj7ba7xk8c9aackz5m5spyc7b8falb") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-hitaki-0.4.0 (c (n "hitaki") (v "0.4.0") (d (list (d (n "ffi") (r "^0.4") (d #t) (k 0) (p "hitaki-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "08k51q29cnf22hnzglb2357a3kdp8yaah5z2zkbaa8syv9gamc6z")))

(define-public crate-hitaki-0.5.0 (c (n "hitaki") (v "0.5.0") (d (list (d (n "ffi") (r "^0.5") (d #t) (k 0) (p "hitaki-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "028hbh41fnlvqvabxckahc6i8nm89hdlxvq0wh65z5yi5v6n0np0")))

