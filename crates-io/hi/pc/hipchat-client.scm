(define-module (crates-io hi pc hipchat-client) #:use-module (crates-io))

(define-public crate-hipchat-client-0.1.1 (c (n "hipchat-client") (v "0.1.1") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^0.6") (d #t) (k 0)) (d (n "serde_json") (r "^0.6") (d #t) (k 0)) (d (n "serde_macros") (r "^0.6") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0pzqhsfcbqkjrhxgmc234lk0lxzvr0dr2wv9hj4qlw1ny2b1qgsz")))

(define-public crate-hipchat-client-0.2.0 (c (n "hipchat-client") (v "0.2.0") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^0.5") (d #t) (k 0)))) (h "0f5ircgr00b3rhwwzajdr0clbhy8b356ddvhhmnfb4a7gi1kq23v")))

(define-public crate-hipchat-client-0.3.0 (c (n "hipchat-client") (v "0.3.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "15ryck6s65zx12h9nk74nw83qk4dqzpdnnmzp7g0iy5mf2mg83da")))

(define-public crate-hipchat-client-0.4.0 (c (n "hipchat-client") (v "0.4.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "155qs56fhx837j3imw4i07nqr5h09f9ymx920lbj34swhkrhzr75")))

(define-public crate-hipchat-client-0.5.0 (c (n "hipchat-client") (v "0.5.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "hyper-native-tls") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "13lj9y9101ifrmfzr8aszx0f55kicnp6drkncgcj9nr012d78mxx")))

