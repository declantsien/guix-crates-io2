(define-module (crates-io hi eu hieu_dummy_rust_lib) #:use-module (crates-io))

(define-public crate-hieu_dummy_rust_lib-0.1.0 (c (n "hieu_dummy_rust_lib") (v "0.1.0") (h "10frz8ckfcl7rvj74v3d98c7m2wc6p556jv6qln983vc25pv4ss1")))

(define-public crate-hieu_dummy_rust_lib-0.2.0 (c (n "hieu_dummy_rust_lib") (v "0.2.0") (h "1qbbqnr0cigkc64psyqs257wpdl07ixsvqmlbnhn2nh1rsx99q1m")))

(define-public crate-hieu_dummy_rust_lib-0.3.0 (c (n "hieu_dummy_rust_lib") (v "0.3.0") (h "1vravd90wh5zcibdkndb671z03rm4dd6w4l0mk561ac0qzz0f6m2")))

(define-public crate-hieu_dummy_rust_lib-0.4.0 (c (n "hieu_dummy_rust_lib") (v "0.4.0") (h "1r3vx6zv498k4amykm846d3ffdsaqzk2a0rin9phwxyg5irq67rb")))

(define-public crate-hieu_dummy_rust_lib-0.5.0 (c (n "hieu_dummy_rust_lib") (v "0.5.0") (h "19y0s456bmcv4fxvf4bf3vqjfvrspi39cp89xcb3kkfz031h6kng")))

(define-public crate-hieu_dummy_rust_lib-0.6.0 (c (n "hieu_dummy_rust_lib") (v "0.6.0") (h "1w825hn90msk22xynp3lp7xva6gcgfywjfjwrrvkggnriikv3zi5")))

(define-public crate-hieu_dummy_rust_lib-0.7.0 (c (n "hieu_dummy_rust_lib") (v "0.7.0") (h "020yrilh98qmrrx2iwfpbkap559b38i7q3h8yd8dvdk4hamz4hd3")))

