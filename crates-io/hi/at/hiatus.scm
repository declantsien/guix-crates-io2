(define-module (crates-io hi at hiatus) #:use-module (crates-io))

(define-public crate-hiatus-0.1.0 (c (n "hiatus") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)))) (h "1ii964870q3xn7g87y8bk55n6ial02w3bjx05cmv561w4hvbkv2h")))

(define-public crate-hiatus-0.1.1 (c (n "hiatus") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.0") (d #t) (k 0)))) (h "1f69m9l3mr13940ff7kzz05x0davba0gb9057wbci58wzcqggbgh")))

