(define-module (crates-io hi ve hive-rust) #:use-module (crates-io))

(define-public crate-hive-rust-0.0.1 (c (n "hive-rust") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "thrift") (r "^0.17.0") (d #t) (k 0)))) (h "1qg2xpgnbicd8fxghy0k6xhqibr1khkqfzspab6ffg97ls0r9774")))

