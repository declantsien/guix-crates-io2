(define-module (crates-io hi ve hive_metastore) #:use-module (crates-io))

(define-public crate-hive_metastore-0.0.1 (c (n "hive_metastore") (v "0.0.1") (d (list (d (n "thrift") (r "^0.18.1") (d #t) (k 0) (p "tent-thrift")))) (h "020i0khqpgnw9blnhgigc6bhr4wxldbzr40h2vn7snm399l7mcn3")))

(define-public crate-hive_metastore-0.0.2 (c (n "hive_metastore") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "pilota") (r "^0.10") (d #t) (k 0)) (d (n "volo") (r "^0.9") (d #t) (k 0)) (d (n "volo-thrift") (r "^0.9") (d #t) (k 0)))) (h "0v99qr6c4zdn0kh9jvqrfsv7njl5sivzygnj936l2fxnnd49wc3j") (r "1.75")))

