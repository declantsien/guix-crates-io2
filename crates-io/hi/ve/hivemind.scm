(define-module (crates-io hi ve hivemind) #:use-module (crates-io))

(define-public crate-hivemind-0.0.0 (c (n "hivemind") (v "0.0.0") (d (list (d (n "clap") (r "^3.0.0-beta.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "hyper") (r "^0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tonic") (r "^0.3") (d #t) (k 0)) (d (n "tonic-build") (r "^0.3") (d #t) (k 1)))) (h "1xfi13hrvwb9hgch1pjxs275hhz50xyy6hcarfsjriqnvi7990br")))

