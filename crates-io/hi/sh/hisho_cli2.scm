(define-module (crates-io hi sh hisho_cli2) #:use-module (crates-io))

(define-public crate-hisho_cli2-1.1.0 (c (n "hisho_cli2") (v "1.1.0") (d (list (d (n "clap") (r "^4.4.7") (d #t) (k 0)) (d (n "hisho_core") (r "^1.1.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1.0.0") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 0)))) (h "185wp7vir346nngpd7h0bvvhdlf097jaiyfjyqsvnglg0wd1yizk") (r "1.73.0")))

