(define-module (crates-io hi to hitotp) #:use-module (crates-io))

(define-public crate-hitotp-0.1.0 (c (n "hitotp") (v "0.1.0") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "hictor") (r "^0.1") (d #t) (k 0)) (d (n "hiopt") (r "^0.1") (d #t) (k 0)))) (h "1p79aaqywz7d8w8l8fhvrf7ydv53lhn8rmmrvsvd83bi47wn6875")))

(define-public crate-hitotp-0.1.1 (c (n "hitotp") (v "0.1.1") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "hictor") (r "^0.1") (d #t) (k 0)) (d (n "hiopt") (r "^0.1") (d #t) (k 0)))) (h "1i7q1qpk99xblf5x6dj48jr1ghk9w1i7snqnrnxc71w92b9y913y")))

(define-public crate-hitotp-0.1.2 (c (n "hitotp") (v "0.1.2") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "hictor") (r "^0.1") (d #t) (k 0)) (d (n "hiopt") (r "^0.1") (d #t) (k 0)))) (h "1xsln5d88xcr7pkvpw81iskh650ffi8j86g81qdqz2vdwbn15xgv")))

(define-public crate-hitotp-0.1.3 (c (n "hitotp") (v "0.1.3") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "hictor") (r "^0.1") (d #t) (k 0)) (d (n "hiopt") (r "^0.1") (d #t) (k 0)))) (h "1c76vrg9vf564g2fna5g7gspblq1j81nkipsmw7ifsdgiwd0xyz8")))

(define-public crate-hitotp-0.1.4 (c (n "hitotp") (v "0.1.4") (d (list (d (n "data-encoding") (r "^2.4.0") (d #t) (k 0)) (d (n "hictor") (r "^0.1") (d #t) (k 0)) (d (n "hiopt") (r "^0.1") (d #t) (k 0)))) (h "1dff1jz4jxl5iv2plfmz34fndqa8ph9my05bfbp62p7m399ncwp7")))

