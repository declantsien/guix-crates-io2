(define-module (crates-io hi to hitori) #:use-module (crates-io))

(define-public crate-hitori-0.1.0 (c (n "hitori") (v "0.1.0") (d (list (d (n "hitori-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "07blnjxslgj4zq9ass9fvwx0b51syc3q3pkz9hc24si3mbgjamgc") (f (quote (("macros" "hitori-macros") ("default" "box" "macros")))) (s 2) (e (quote (("find-hitori" "hitori-macros?/proc-macro-crate") ("box" "hitori-macros?/box"))))))

(define-public crate-hitori-0.1.1 (c (n "hitori") (v "0.1.1") (d (list (d (n "hitori-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "12nyhxl3b3l0v5vn2rnviqd1lb987lh4jai8vfqr92f91hw45gy3") (f (quote (("macros" "hitori-macros") ("default" "box" "macros")))) (s 2) (e (quote (("find-hitori" "hitori-macros?/proc-macro-crate") ("box" "hitori-macros?/box"))))))

(define-public crate-hitori-0.1.2 (c (n "hitori") (v "0.1.2") (d (list (d (n "hitori-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1wdwh3m77kwmvi6k66lwiafdwwv9xprfcr4zr43pksywgajfxyxv") (f (quote (("macros" "hitori-macros") ("default" "box" "macros")))) (s 2) (e (quote (("find-hitori" "hitori-macros?/proc-macro-crate") ("box" "hitori-macros?/box"))))))

(define-public crate-hitori-0.1.3 (c (n "hitori") (v "0.1.3") (d (list (d (n "hitori-macros") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1a5wlg84h8ipyjic7ybfpgsw0y3mv0vla392f2ma07y358jh27vy") (f (quote (("macros" "hitori-macros") ("default" "box" "macros")))) (s 2) (e (quote (("find-hitori" "hitori-macros?/proc-macro-crate") ("box" "hitori-macros?/box"))))))

(define-public crate-hitori-0.1.4 (c (n "hitori") (v "0.1.4") (d (list (d (n "hitori-macros") (r "=0.1.4") (o #t) (d #t) (k 0)))) (h "1i0ljzcrhfi0z7fcnkjad7mwxfp0sdxynrsv0ffv3j6s3g77558k") (f (quote (("macros" "hitori-macros") ("default" "box" "macros")))) (s 2) (e (quote (("find-hitori" "hitori-macros?/proc-macro-crate") ("box" "hitori-macros?/box"))))))

(define-public crate-hitori-0.1.5 (c (n "hitori") (v "0.1.5") (d (list (d (n "hitori-macros") (r "=0.1.5") (o #t) (d #t) (k 0)))) (h "1c7vr6vgf3shb2c2l2b9jhr6q5ql0p6k6ikqzglb048sqj84pw8p") (f (quote (("macros" "hitori-macros") ("default" "box" "macros")))) (s 2) (e (quote (("find-hitori" "hitori-macros?/proc-macro-crate") ("box" "hitori-macros?/box"))))))

(define-public crate-hitori-0.2.0 (c (n "hitori") (v "0.2.0") (d (list (d (n "hitori-macros") (r "=0.2.0") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 2)))) (h "07hdwv6xh57nxjv12k7y6avabirnr22awln7ql4fagphn07fq7s5") (f (quote (("macros" "hitori-macros") ("examples") ("default" "alloc" "macros") ("alloc")))) (s 2) (e (quote (("find-hitori" "hitori-macros?/proc-macro-crate")))) (r "1.56")))

(define-public crate-hitori-0.2.1 (c (n "hitori") (v "0.2.1") (d (list (d (n "hitori-macros") (r "=0.2.1") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 2)))) (h "1pq3x9djyk2ld2850k0n2ixizrllcyz6h1p9f8pb3hmhpjrwmn3z") (f (quote (("macros" "hitori-macros") ("examples") ("default" "alloc" "macros") ("alloc")))) (s 2) (e (quote (("find-hitori" "hitori-macros?/proc-macro-crate")))) (r "1.56")))

(define-public crate-hitori-0.2.2 (c (n "hitori") (v "0.2.2") (d (list (d (n "hitori-macros") (r "=0.2.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 2)))) (h "0n60qz0llqgggycxa21w9w2yldfbd7sb38s30xk0ql5cjhqa1n1w") (f (quote (("macros" "hitori-macros") ("examples") ("default" "alloc" "macros") ("alloc")))) (s 2) (e (quote (("find-hitori" "hitori-macros?/proc-macro-crate")))) (r "1.56")))

(define-public crate-hitori-0.2.3 (c (n "hitori") (v "0.2.3") (d (list (d (n "hitori-macros") (r "=0.2.3") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 2)))) (h "1g8cg9fdw918b9c7xniayk7jkqnh9rcg9zpy3zx8y31psfgip4q3") (f (quote (("macros" "hitori-macros") ("default" "alloc" "macros") ("alloc")))) (s 2) (e (quote (("find-hitori" "hitori-macros?/find-hitori")))) (r "1.64")))

