(define-module (crates-io hi yo hiyori) #:use-module (crates-io))

(define-public crate-hiyori-0.1.0 (c (n "hiyori") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.7") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.73") (d #t) (k 0)))) (h "14q502zb31csfybkqh27p9q54fvcrs79kqrqz7j6anx1538ibkgf")))

