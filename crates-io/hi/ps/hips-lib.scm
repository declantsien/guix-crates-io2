(define-module (crates-io hi ps hips-lib) #:use-module (crates-io))

(define-public crate-hips-lib-0.1.0 (c (n "hips-lib") (v "0.1.0") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)))) (h "1cikz9apaa1c9g80xpf2szpjbv7kcvb18pbjapb7lky4cgyf4sia") (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-hips-lib-0.2.0 (c (n "hips-lib") (v "0.2.0") (d (list (d (n "image") (r "^0.24.5") (o #t) (d #t) (k 0)))) (h "1n26nr76h2k56w6rkdjfimc3pk5d83sbrqp5x570w7q4wmhp0vzj") (s 2) (e (quote (("image" "dep:image"))))))

