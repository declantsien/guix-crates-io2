(define-module (crates-io hi tr hitree) #:use-module (crates-io))

(define-public crate-hitree-0.1.0 (c (n "hitree") (v "0.1.0") (h "1c0fim3zi3yx0wd2mv0gacx0qg2d5yx7igxjlmwpfqwii79qjrnm") (y #t) (r "1.60")))

(define-public crate-hitree-0.1.1 (c (n "hitree") (v "0.1.1") (h "104jvs1phws4wnnrm130ihk4rca19vkq4xiv8xwv2g1zc8a1x62d") (r "1.60")))

(define-public crate-hitree-0.1.2 (c (n "hitree") (v "0.1.2") (h "02m0zr7466mj3gqjvk6abp3ac9hdnmb1jvfw279rcw2albp6hjqf") (r "1.60")))

(define-public crate-hitree-0.1.3 (c (n "hitree") (v "0.1.3") (h "0wjdkzd4g2md9084ml6yr044lmvnapzg14fpw2cz1237gf7h2mvm") (r "1.60")))

(define-public crate-hitree-0.1.4 (c (n "hitree") (v "0.1.4") (h "0f2spn52i1az8ssnvqdl25z0gv079wirb0rlq4c6skcfw50sdqy7") (r "1.60")))

