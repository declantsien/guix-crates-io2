(define-module (crates-io hi tr hitrace-macro) #:use-module (crates-io))

(define-public crate-hitrace-macro-0.1.0 (c (n "hitrace-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.81") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("parsing" "full"))) (d #t) (k 0)))) (h "0g9di1c05vsz99fpkdf5ffs54pvasqg43l1scs1j2lb524xyh7yh")))

