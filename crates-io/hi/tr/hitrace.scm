(define-module (crates-io hi tr hitrace) #:use-module (crates-io))

(define-public crate-hitrace-0.1.0 (c (n "hitrace") (v "0.1.0") (d (list (d (n "hitrace-sys") (r "^0.1") (d #t) (t "cfg(target_env = \"ohos\")") (k 0)))) (h "0hgvvkrbv70b5rwfks9ryzrsmrlxxn1ixj1zzrcyyz9lz09ln0qa")))

(define-public crate-hitrace-0.1.1 (c (n "hitrace") (v "0.1.1") (d (list (d (n "hitrace-sys") (r "^0.1") (d #t) (t "cfg(target_env = \"ohos\")") (k 0)))) (h "1wa0cnjd7kpj5khxg9rb1nc043v58y7q4p0k5akhgk20mx2633qf")))

(define-public crate-hitrace-0.1.2 (c (n "hitrace") (v "0.1.2") (d (list (d (n "hitrace-sys") (r "^0.1") (d #t) (t "cfg(target_env = \"ohos\")") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1gjcvswyf154d85y9rcqx88zl4gk3rn1xra8cvdlnl0hhjnp86kb")))

(define-public crate-hitrace-0.1.3 (c (n "hitrace") (v "0.1.3") (d (list (d (n "hitrace-sys") (r "^0.1") (d #t) (t "cfg(target_env = \"ohos\")") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0abknlhfplskr9y11w9xixd2phwvx101pivm8433ipg8l1k1xfy0")))

(define-public crate-hitrace-0.1.4 (c (n "hitrace") (v "0.1.4") (d (list (d (n "hitrace-sys") (r "^0.1") (d #t) (t "cfg(target_env = \"ohos\")") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0vigmwx7p83r6yqmi576n9vz8r38nniznhqih7mylchbygk0lb7r") (f (quote (("max_level_off"))))))

