(define-module (crates-io hi tr hitrace-sys) #:use-module (crates-io))

(define-public crate-hitrace-sys-0.1.0 (c (n "hitrace-sys") (v "0.1.0") (h "06ynfbvykvhsnpbbzh40agj1r8kap882qma338778mq7lkjmf3cx")))

(define-public crate-hitrace-sys-0.1.1 (c (n "hitrace-sys") (v "0.1.1") (h "0fhw5cvjv4i9rm3nnvq64pvh9d9rjbkr0pfbz3ixz245wp9lyl1d")))

