(define-module (crates-io hi bp hibp) #:use-module (crates-io))

(define-public crate-hibp-0.1.0 (c (n "hibp") (v "0.1.0") (d (list (d (n "mockito") (r "^0.23") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "0d18s7p8wdxqyfxwn68h7wvv355r6sjq8zc7n9n5fdr8zkggk3pm")))

(define-public crate-hibp-0.1.1 (c (n "hibp") (v "0.1.1") (d (list (d (n "mockito") (r "^0.23") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 0)))) (h "1rjys59ljqn44y8d6w5z1c46ap4xfsxf1q4m1vhw5glsq8ii0x34")))

