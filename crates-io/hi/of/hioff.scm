(define-module (crates-io hi of hioff) #:use-module (crates-io))

(define-public crate-hioff-0.1.0 (c (n "hioff") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qy5k5jh9988g0m5qrwiqmmcv6mn143b49vl6ra82j7bvpsxhz5j")))

(define-public crate-hioff-0.1.1 (c (n "hioff") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1s2yypr8krrvwfylsf6g52fhxdh3sw8ckcipsl870rh6vfdgmc0v")))

