(define-module (crates-io hi rp hirpdag_hashconsing) #:use-module (crates-io))

(define-public crate-hirpdag_hashconsing-0.1.0 (c (n "hirpdag_hashconsing") (v "0.1.0") (d (list (d (n "array-init") (r "^1.0.0") (d #t) (k 0)) (d (n "weak-table") (r "^0.3.0") (d #t) (k 0)))) (h "00ddi03frqcaxw2rdmr1ipmw4hji7qp07qfj9j442dj783h0fkly")))

(define-public crate-hirpdag_hashconsing-0.1.1 (c (n "hirpdag_hashconsing") (v "0.1.1") (d (list (d (n "array-init") (r "^1.0.0") (d #t) (k 0)) (d (n "weak-table") (r "^0.3.0") (d #t) (k 0)))) (h "07f0l1dvq6rhfqzv6km2kss9hixr1kqjrfja44bvhj1psl156waq")))

