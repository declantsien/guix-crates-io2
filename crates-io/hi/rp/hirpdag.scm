(define-module (crates-io hi rp hirpdag) #:use-module (crates-io))

(define-public crate-hirpdag-0.1.0 (c (n "hirpdag") (v "0.1.0") (d (list (d (n "hirpdag_derive") (r "^0.1.0") (d #t) (k 0)) (d (n "hirpdag_hashconsing") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "17c6ln48h9wxi22x4wjfdv2igwdkr5azg9wi0jrw76lwzwk2fy0m")))

(define-public crate-hirpdag-0.1.1 (c (n "hirpdag") (v "0.1.1") (d (list (d (n "hirpdag_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "hirpdag_hashconsing") (r "^0.1.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "115ypa0xd07jc62jp6ha5q5gkffbbpf9l3d90glj2z193gi0gk7b")))

