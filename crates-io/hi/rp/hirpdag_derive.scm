(define-module (crates-io hi rp hirpdag_derive) #:use-module (crates-io))

(define-public crate-hirpdag_derive-0.1.0 (c (n "hirpdag_derive") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1fdgvw3g2xxkkp59s7pn83q7w93vmzjmhg9q2zljcq43c8bmxaxx")))

(define-public crate-hirpdag_derive-0.1.1 (c (n "hirpdag_derive") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)))) (h "1ym24j1ksp4fwmxlbc1s20z9r7wm4psvd4kx86z34p2kg1vgbgdn")))

