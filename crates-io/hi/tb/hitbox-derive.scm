(define-module (crates-io hi tb hitbox-derive) #:use-module (crates-io))

(define-public crate-hitbox-derive-0.1.0 (c (n "hitbox-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nx8h64akx6f7fcxi1cbma8kbjf0rwbb3lyn697w4cm339j7cskw") (y #t)))

(define-public crate-hitbox-derive-0.1.1 (c (n "hitbox-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jkl4a323ayagbs4nw91i71v0v0w9882pmzpb9sgifkq2pkb3ryj")))

