(define-module (crates-io hi tb hitbox-backend) #:use-module (crates-io))

(define-public crate-hitbox-backend-0.1.0 (c (n "hitbox-backend") (v "0.1.0") (d (list (d (n "actix") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1qq8q3j23lf7xysidpyf59d8x5jg8sp8l93jr7qq76pjg0c6cc02") (y #t)))

(define-public crate-hitbox-backend-0.1.1 (c (n "hitbox-backend") (v "0.1.1") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "02j6cfr7dh2ran4v8w64bdjll38w7vlhg9hs9b93capm7dv85a46")))

