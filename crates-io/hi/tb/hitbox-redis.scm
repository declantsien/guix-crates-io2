(define-module (crates-io hi tb hitbox-redis) #:use-module (crates-io))

(define-public crate-hitbox-redis-0.1.0 (c (n "hitbox-redis") (v "0.1.0") (d (list (d (n "actix") (r "^0.11") (d #t) (k 0)) (d (n "actix-rt") (r "^2") (d #t) (k 0)) (d (n "actix_derive") (r "^0.6") (d #t) (k 0)) (d (n "hitbox-backend") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.20") (f (quote ("tokio-comp" "connection-manager"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (d #t) (k 2)))) (h "12h8nxmgpqx4n9m96dpzzq5rmrisrdxzf795484p8x6v1fli2gkp") (y #t)))

(define-public crate-hitbox-redis-0.1.1 (c (n "hitbox-redis") (v "0.1.1") (d (list (d (n "actix") (r "^0.10") (d #t) (k 0)) (d (n "actix-rt") (r "^1.1") (d #t) (k 2)) (d (n "actix_derive") (r "^0.5") (d #t) (k 0)) (d (n "hitbox-backend") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "redis") (r "^0.17") (f (quote ("tokio-rt-core" "connection-manager"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("time"))) (d #t) (k 2)))) (h "0hwhgpf60g0d2vpd106him0d5bbqi1h1hzgk1bsc8c3h4xmjciph")))

