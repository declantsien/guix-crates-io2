(define-module (crates-io hi nd hindley-milner) #:use-module (crates-io))

(define-public crate-hindley-milner-0.1.0 (c (n "hindley-milner") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "maplit") (r "^0.1.4") (d #t) (k 0)))) (h "0rw3a8waz08fw50h5pqv9rmnbgaxim28c7bss67sab6c0py4b2dr")))

