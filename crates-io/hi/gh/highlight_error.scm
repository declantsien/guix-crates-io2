(define-module (crates-io hi gh highlight_error) #:use-module (crates-io))

(define-public crate-highlight_error-0.1.0 (c (n "highlight_error") (v "0.1.0") (h "03vg9fz3kzw4qsw9iszpaw6j2j3n0y98x472nxfi3vlsjfr18zli")))

(define-public crate-highlight_error-0.1.1 (c (n "highlight_error") (v "0.1.1") (h "00zgm8hpl5dm27d4vfjwincva8cm16jidwxrwarbdmv0as01i7l0")))

