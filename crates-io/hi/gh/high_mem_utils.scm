(define-module (crates-io hi gh high_mem_utils) #:use-module (crates-io))

(define-public crate-high_mem_utils-0.1.0 (c (n "high_mem_utils") (v "0.1.0") (h "0yjj8qv45f0jlv4pbz50h7dc466mhyxj3m1p18y2vq546pbw0vg1")))

(define-public crate-high_mem_utils-0.1.1 (c (n "high_mem_utils") (v "0.1.1") (h "082znpvfg8s7md93ar4qdg7f7mz1jq7m7653iklx6xfbwqypk0wn") (y #t)))

(define-public crate-high_mem_utils-0.2.0 (c (n "high_mem_utils") (v "0.2.0") (h "019pffnlaj8j2j3ii27z8a7bqydri069qqs93nznp305ny28dx3k")))

(define-public crate-high_mem_utils-0.2.1 (c (n "high_mem_utils") (v "0.2.1") (h "14ixl4sd25gr6xny02p5b0ga9kphp18q5ksws3yrkxx04flk3x72")))

(define-public crate-high_mem_utils-0.2.2 (c (n "high_mem_utils") (v "0.2.2") (h "13wyi8p1mifxabpvh8xds6bx1irv8li2nxj79k86irlz0zwyyjjg")))

(define-public crate-high_mem_utils-0.2.3 (c (n "high_mem_utils") (v "0.2.3") (h "01lfjl08kjbylbq60vvffi16j6q5bnywcls6qnh9f68c3a8j24c7")))

(define-public crate-high_mem_utils-0.2.4 (c (n "high_mem_utils") (v "0.2.4") (h "024a946mkgb7f9ihc9h7f270562f1nf834rw739dv8ksr64zkb44")))

(define-public crate-high_mem_utils-0.2.5 (c (n "high_mem_utils") (v "0.2.5") (h "19zq003qkp4s4khqfx5fz2rdxhjdz0104bj6j8p472sb0sjlyqll")))

(define-public crate-high_mem_utils-0.2.6 (c (n "high_mem_utils") (v "0.2.6") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13yk37pixdzbvv4ppinm84x8wwzgqcc9zh02w45ni3f02f5mripm") (f (quote (("serde_support" "serde"))))))

(define-public crate-high_mem_utils-0.2.7 (c (n "high_mem_utils") (v "0.2.7") (d (list (d (n "fast_new_type") (r "^0.1") (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "003n1d9szr4d4m9iyawz99jnz1qs2i2zr1gq5ym9xfj33ravi2ly") (f (quote (("serde_support" "serde"))))))

