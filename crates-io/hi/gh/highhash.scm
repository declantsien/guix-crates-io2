(define-module (crates-io hi gh highhash) #:use-module (crates-io))

(define-public crate-highhash-0.1.0 (c (n "highhash") (v "0.1.0") (h "0alwmab06k7kzasaq365fhslc794r3xs6adry0im0grr6warah3f")))

(define-public crate-highhash-0.1.1 (c (n "highhash") (v "0.1.1") (h "108l852wp3yiz6j9la1ppb3m4aa4lsybl49livyin9bsbwzngqcr")))

(define-public crate-highhash-0.2.0 (c (n "highhash") (v "0.2.0") (d (list (d (n "fasthash") (r "^0.4") (d #t) (t "cfg(any(target_arch = \"x86\", target_arch = \"x86_64\"))") (k 2)))) (h "0snz5lqm2mib6qlf087q5xpgi4sdkknlwjjq9d86zd26il40i2r0")))

