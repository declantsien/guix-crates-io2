(define-module (crates-io hi gh higher_order_point) #:use-module (crates-io))

(define-public crate-higher_order_point-0.1.0 (c (n "higher_order_point") (v "0.1.0") (d (list (d (n "advancedresearch-higher_order_core") (r "^0.1.1") (d #t) (k 0)) (d (n "camera_controllers") (r "^0.31.0") (d #t) (k 2)) (d (n "piston") (r "^0.49.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.34.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.68.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.63.0") (d #t) (k 2)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 2)))) (h "0vd65rh4lifs0qlv7gi4vsdmvf524fzrh2y6jxrs1zhfnrk0r0am")))

(define-public crate-higher_order_point-0.1.1 (c (n "higher_order_point") (v "0.1.1") (d (list (d (n "advancedresearch-higher_order_core") (r "^0.1.1") (d #t) (k 0)) (d (n "camera_controllers") (r "^0.31.0") (d #t) (k 2)) (d (n "piston") (r "^0.49.0") (d #t) (k 2)) (d (n "piston2d-graphics") (r "^0.34.0") (d #t) (k 2)) (d (n "piston2d-opengl_graphics") (r "^0.68.0") (d #t) (k 2)) (d (n "pistoncore-sdl2_window") (r "^0.63.0") (d #t) (k 2)) (d (n "vecmath") (r "^1.0.0") (d #t) (k 2)))) (h "19zm4p5l7hlmc0pjkmv0q7q8mvhpl47bgk0bm0ir1pygd00aizbb")))

