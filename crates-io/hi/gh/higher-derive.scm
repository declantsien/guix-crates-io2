(define-module (crates-io hi gh higher-derive) #:use-module (crates-io))

(define-public crate-higher-derive-0.1.0 (c (n "higher-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "0g16vrwyyfzgy2dghgq18yffn560v2nzqhvy4i33wkmim1qaiwah")))

(define-public crate-higher-derive-0.1.1 (c (n "higher-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.11") (d #t) (k 0)) (d (n "syn") (r "^0.15.26") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "175fwy9knxfsc6v7nfk31z0j86l5ydsd0x8616fcvh6w7byrf3mv")))

(define-public crate-higher-derive-0.2.0 (c (n "higher-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.50") (d #t) (k 0)) (d (n "quote") (r "^1.0.23") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("derive" "extra-traits"))) (d #t) (k 0)))) (h "04g5hzws9j2g59c5vw7sszwhc6fs58hh4qgrydx50985fd7ifz9d")))

