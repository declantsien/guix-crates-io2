(define-module (crates-io hi gh higher-order-closure) #:use-module (crates-io))

(define-public crate-higher-order-closure-0.0.1 (c (n "higher-order-closure") (v "0.0.1") (h "15hpb888z1f96idn3hhy2sib9lfd035xp0jvnaxysr08xk9r3dd8") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

(define-public crate-higher-order-closure-0.0.2 (c (n "higher-order-closure") (v "0.0.2") (h "15b36g0njwcw9n8dycazr93bxj07fdpc3kn4l3igfxf2505z4qj8") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

(define-public crate-higher-order-closure-0.0.3 (c (n "higher-order-closure") (v "0.0.3") (h "1kfvnhcz7z42f1r0cnr8lxccgrpa584ba1nipi1vmx6jf68s4j06") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

(define-public crate-higher-order-closure-0.0.4 (c (n "higher-order-closure") (v "0.0.4") (h "1gahpkm2v2rvk9gs6c68j6nc205984w5jiqxhm70b4f7hyy4iwrr") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

(define-public crate-higher-order-closure-0.0.5 (c (n "higher-order-closure") (v "0.0.5") (h "12bc46mpmls9nbjzm4bi2l9y61499flgr3bisz5m9iq7lf3hsicc") (f (quote (("ui-tests" "better-docs") ("better-docs"))))))

