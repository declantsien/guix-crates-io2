(define-module (crates-io hi gh highdash) #:use-module (crates-io))

(define-public crate-highdash-0.0.0 (c (n "highdash") (v "0.0.0") (h "1ydaw5wgbrjcv4kc2g5vwq7bdrak8gcq34c6br2sy3pf3a2mckfi")))

(define-public crate-highdash-0.0.1 (c (n "highdash") (v "0.0.1") (h "19fn27fjv5zin9rrflx97k45jy07q47sxnwn23s6pzshm3957mrc")))

