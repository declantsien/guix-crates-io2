(define-module (crates-io hi gh highlighter) #:use-module (crates-io))

(define-public crate-highlighter-0.1.0-alpha (c (n "highlighter") (v "0.1.0-alpha") (d (list (d (n "highlighter-core") (r "^0.1.0-alpha") (d #t) (k 0)))) (h "1g803iv9ds205lffbazc4fvdjvhzc20w07vmja6fwkys841ag35m")))

(define-public crate-highlighter-0.1.1-alpha (c (n "highlighter") (v "0.1.1-alpha") (d (list (d (n "highlighter-core") (r "^0.1.1-alpha") (d #t) (k 0)) (d (n "highlighter-languages") (r "^0.1.1-alpha") (d #t) (k 0)) (d (n "highlighter-target-html") (r "^0.1.1-alpha") (o #t) (d #t) (k 0)))) (h "12jc27fhgwxa3mx70jv2gs38fw182n3sx30prjs9nsfbfqnvbs1r") (f (quote (("target-html" "highlighter-target-html") ("default" "all-languages" "target-html") ("brainheck" "highlighter-languages/brainheck") ("all-languages" "brainheck"))))))

