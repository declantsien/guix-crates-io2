(define-module (crates-io hi gh hightorrent) #:use-module (crates-io))

(define-public crate-hightorrent-0.1.0 (c (n "hightorrent") (v "0.1.0") (d (list (d (n "bt_bencode") (r "^0.7") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha1") (r "^0.10") (d #t) (k 0)) (d (n "sha256") (r "^1.1") (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "1933k34ni24dchmwxm9hfkzcvva2frbkk0jj2hiz3d5nw0wwvsm8") (r "1.64")))

