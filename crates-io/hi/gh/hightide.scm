(define-module (crates-io hi gh hightide) #:use-module (crates-io))

(define-public crate-hightide-0.1.0 (c (n "hightide") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "hyperx") (r "^1.1.0") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "0q5l0gkvm2xw3c40gwqmhpbdgwwf6azgwd3pgbiw33ad1fmm8fx2")))

(define-public crate-hightide-0.1.1 (c (n "hightide") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "hyperx") (r "^1.1.0") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "0yflrp36ikh952kys1prd5mymv7whpb3iqbfz84czfqhxpp2s8n4")))

(define-public crate-hightide-0.1.2 (c (n "hightide") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "hyperx") (r "^1.1.0") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "1r2srm92hkcy79bpas59icsaqp18s2rf75wcnym61s4fnxgbz41a")))

(define-public crate-hightide-0.1.3 (c (n "hightide") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1.40") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 0)) (d (n "hyperx") (r "^1.1.0") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "1nsj4lj7k5dl374c2k8ydb4wxfh8hyfcghxcwsp7a3yh9cl5mfz0")))

