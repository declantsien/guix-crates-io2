(define-module (crates-io hi gh highlight-assertions) #:use-module (crates-io))

(define-public crate-highlight-assertions-0.1.7 (c (n "highlight-assertions") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libloading") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.17") (d #t) (k 0)) (d (n "regex") (r "^1.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tree-sitter") (r "^0.20.9") (d #t) (k 0)))) (h "1532mvq3fx0ixfx70c6bb6lcj5vl3qi075ga9wa9dyhanghgd854")))

