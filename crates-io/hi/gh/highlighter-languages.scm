(define-module (crates-io hi gh highlighter-languages) #:use-module (crates-io))

(define-public crate-highlighter-languages-0.1.1-alpha (c (n "highlighter-languages") (v "0.1.1-alpha") (d (list (d (n "highlighter-core") (r "^0.1.1-alpha") (d #t) (k 0)))) (h "1xz9n7lb60691bn8vq01jy9jqm4m5hsgfk8n588wpwwh14fvzry1") (f (quote (("brainheck") ("all-languages" "brainheck"))))))

