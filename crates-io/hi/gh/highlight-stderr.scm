(define-module (crates-io hi gh highlight-stderr) #:use-module (crates-io))

(define-public crate-highlight-stderr-0.1.0 (c (n "highlight-stderr") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "colorparse") (r "^2.0.1") (d #t) (k 0)) (d (n "io-mux") (r "^0.1") (d #t) (k 0)))) (h "07j5a2gpap7mvjfvd02g26gbxg4nsg0d013m986whypdbcz67smk")))

(define-public crate-highlight-stderr-0.2.0 (c (n "highlight-stderr") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "colorparse") (r "^2.0.1") (d #t) (k 0)) (d (n "io-mux") (r "^1") (d #t) (k 0)))) (h "000xa1b4ry7qgl6k6rmds2w8qsl6njjgx9c929568mmb341523zb") (y #t)))

(define-public crate-highlight-stderr-0.2.1 (c (n "highlight-stderr") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.26") (d #t) (k 0)) (d (n "colorparse") (r "^2.0.1") (d #t) (k 0)) (d (n "io-mux") (r "^1.0.1") (d #t) (k 0)))) (h "0w3d2bpb7bc1a218dmcxa5jsszfxc1yrjcj22pvd0f7r08py6klr")))

(define-public crate-highlight-stderr-0.3.0 (c (n "highlight-stderr") (v "0.3.0") (d (list (d (n "anstyle-git") (r "^1") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "io-mux") (r "^2.1.0") (d #t) (k 0)))) (h "1far2ypa12q3ggi6yfdzcng360zl9rk25iw4lxsr91m6g8m8ac1r")))

