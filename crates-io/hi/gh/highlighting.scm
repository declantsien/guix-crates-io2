(define-module (crates-io hi gh highlighting) #:use-module (crates-io))

(define-public crate-highlighting-0.1.0 (c (n "highlighting") (v "0.1.0") (d (list (d (n "tree-sitter-highlight") (r "^0.20.1") (d #t) (k 0)))) (h "00psak8ylw0kijkkjxhhhzhs282b4hj01n4zgrd53bg64zqiqcl3")))

(define-public crate-highlighting-0.1.1 (c (n "highlighting") (v "0.1.1") (d (list (d (n "tree-sitter-highlight") (r "^0.20.1") (d #t) (k 0)))) (h "1n99pvfd2bhs7y24ybswsqjx3mdlqbgwm49j3mdgwh69h0simh70")))

(define-public crate-highlighting-0.1.2 (c (n "highlighting") (v "0.1.2") (d (list (d (n "tree-sitter-highlight") (r "^0.20.1") (d #t) (k 0)))) (h "1xr9h4gsragbbbgycilyv311ivzi0yn4zahsbn6gm2y07038a9im")))

