(define-module (crates-io hi gh highlighter-core) #:use-module (crates-io))

(define-public crate-highlighter-core-0.1.0-alpha (c (n "highlighter-core") (v "0.1.0-alpha") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1pbjvhcca4bl13zj5r77xgq4979038n3b4y88kyvb0k8z90mxqhz")))

(define-public crate-highlighter-core-0.1.1-alpha (c (n "highlighter-core") (v "0.1.1-alpha") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1m928sbzbxcxxp83apldv2v97517cpwlwdhahl1g4z3ak4lx5i0b")))

