(define-module (crates-io hi gh highlight) #:use-module (crates-io))

(define-public crate-highlight-0.0.0 (c (n "highlight") (v "0.0.0") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)))) (h "183rk79m19h0bn9r4xp01a49waaxqg00718fxpp50n5haxi1mjvm")))

(define-public crate-highlight-0.0.1 (c (n "highlight") (v "0.0.1") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)))) (h "009qy6xipl8vyfc19glkmz8yla955z5290x33fb80p60kinczxjv")))

(define-public crate-highlight-0.0.2 (c (n "highlight") (v "0.0.2") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)))) (h "1hnmpi9m0k77amn48nxvglzx716nbi36ll5pc4pi45bfsywrv3d8")))

(define-public crate-highlight-0.0.3 (c (n "highlight") (v "0.0.3") (d (list (d (n "anystring") (r "^0.0") (d #t) (k 0)))) (h "1whv925m7l6b6fk1qyrj18ibaaf141gbmpv6drvl989g4n90bcd9")))

