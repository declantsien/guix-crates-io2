(define-module (crates-io hi gh higher_order_functions) #:use-module (crates-io))

(define-public crate-higher_order_functions-0.1.0 (c (n "higher_order_functions") (v "0.1.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "07wsjh27pm31brl9ff5b0a6dr6w1hin9qprcljcplhcp2kh3xywx") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-higher_order_functions-0.1.1 (c (n "higher_order_functions") (v "0.1.1") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "08zydmjz16fvhfz8i568s7vg2cy6hafyapz1077hf1k489wrc0i5") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-higher_order_functions-0.1.2 (c (n "higher_order_functions") (v "0.1.2") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "17rmrs5ka9xlwks7frz0ladc5x1zznmsm38qbaq9slcc5gp3lmj2") (f (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-higher_order_functions-0.2.0 (c (n "higher_order_functions") (v "0.2.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1drl1943cakmxianc8x914jrl861avndhrszgl9d4vc2qhylkdgh") (f (quote (("std") ("default" "std") ("alloc"))))))

