(define-module (crates-io hi gh hightime) #:use-module (crates-io))

(define-public crate-hightime-0.1.0 (c (n "hightime") (v "0.1.0") (h "0p5sx0v8ksa34y7d21cwd4k5yk8jh1l4m17z0r404fynji1yq079")))

(define-public crate-hightime-0.1.1 (c (n "hightime") (v "0.1.1") (h "17mlhqd172ny5kq073blavkihmjyz51zmsqlixldhllbd5z06v7y")))

(define-public crate-hightime-0.1.2 (c (n "hightime") (v "0.1.2") (h "0ih8xqhzlsn4h1karic63a7vhbsa0xdl07l3gdf0w4sm0jcxxfiv") (f (quote (("unstable") ("std"))))))

