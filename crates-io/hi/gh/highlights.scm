(define-module (crates-io hi gh highlights) #:use-module (crates-io))

(define-public crate-highlights-0.1.0 (c (n "highlights") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "0bcf1r75gcqnrarf7g18x4c7iwr2ycpsywzmzw2w3mzw95h5bz7g")))

(define-public crate-highlights-0.2.0 (c (n "highlights") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)))) (h "1yay8g3k01xxb3jjk8krs1bcpx2r1a8ng37p107p1w9kp224c2p6")))

