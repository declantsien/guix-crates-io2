(define-module (crates-io hi gh higher-kinded-types) #:use-module (crates-io))

(define-public crate-higher-kinded-types-0.1.0-rc1 (c (n "higher-kinded-types") (v "0.1.0-rc1") (d (list (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "1j0ybdwbhs9f21gwc0xplv3id9i4lv2rrlc9k2gjnl63gwi9ynzw") (f (quote (("ui-tests" "better-docs" "fn_traits") ("fn_traits") ("docs-rs" "better-docs" "fn_traits") ("default") ("better-docs"))))))

(define-public crate-higher-kinded-types-0.1.0-rc2 (c (n "higher-kinded-types") (v "0.1.0-rc2") (d (list (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "1l31qyiy07x25fcg9vlxbg1gqcrns26fpj2za3215vw93df1ilhs") (f (quote (("ui-tests" "better-docs" "fn_traits") ("fn_traits") ("docs-rs" "better-docs" "fn_traits") ("default") ("better-docs"))))))

(define-public crate-higher-kinded-types-0.1.0-rc3 (c (n "higher-kinded-types") (v "0.1.0-rc3") (d (list (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "0bbjk8hv5mk1w1z38yjlwvvcl208346zswj7f1jak5jiq4izkh6n") (f (quote (("ui-tests" "better-docs" "fn_traits") ("fn_traits") ("docs-rs" "better-docs" "fn_traits") ("default") ("better-docs"))))))

(define-public crate-higher-kinded-types-0.1.0-rc4 (c (n "higher-kinded-types") (v "0.1.0-rc4") (d (list (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "08kx8q9jd7a9c86n58yycq10wca81dlm60gc2hlpcmc8srg6vcqz") (f (quote (("ui-tests" "better-docs" "fn_traits") ("fn_traits") ("docs-rs" "better-docs" "fn_traits") ("default") ("better-docs"))))))

(define-public crate-higher-kinded-types-0.1.0-rc5 (c (n "higher-kinded-types") (v "0.1.0-rc5") (d (list (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "0bmv9z9gszybqcj6fskw1rd325rs3jf15x71bjfv6ng32ipxh60a") (f (quote (("ui-tests" "better-docs" "fn_traits") ("fn_traits") ("docs-rs" "better-docs" "fn_traits") ("default") ("better-docs"))))))

(define-public crate-higher-kinded-types-0.1.0 (c (n "higher-kinded-types") (v "0.1.0") (d (list (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "0vch36n2m3g1vmzm38nsizj0smvdkkb77n1ymcad2cl8r7ggc09b") (f (quote (("ui-tests" "better-docs" "fn_traits") ("fn_traits") ("docs-rs" "better-docs" "fn_traits") ("default") ("better-docs"))))))

(define-public crate-higher-kinded-types-0.2.0-rc1 (c (n "higher-kinded-types") (v "0.2.0-rc1") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 0)) (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0v5kjlpkc9l61ayrjgbm38ki322kzys8lcffiiq2vyrvys6c74lk") (f (quote (("ui-tests" "better-docs" "fn_traits") ("fn_traits") ("docs-rs" "better-docs" "fn_traits") ("default") ("better-docs"))))))

(define-public crate-higher-kinded-types-0.1.1 (c (n "higher-kinded-types") (v "0.1.1") (d (list (d (n "never-say-never") (r "^6.6.666") (d #t) (k 0)))) (h "13k7qqri4l2rrmjq9dr0bldc7ilwjzqsb42wc044i3cb9iaqa6an") (f (quote (("ui-tests" "better-docs" "fn_traits") ("fn_traits") ("docs-rs" "better-docs" "fn_traits") ("default") ("better-docs"))))))

