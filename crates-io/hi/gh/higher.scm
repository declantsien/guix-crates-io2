(define-module (crates-io hi gh higher) #:use-module (crates-io))

(define-public crate-higher-0.1.0 (c (n "higher") (v "0.1.0") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)))) (h "0j1vmvx29wy2bsanhzqqv1d2i55wh8yb8z08sspk1gk1y1ch35nv")))

(define-public crate-higher-0.1.1 (c (n "higher") (v "0.1.1") (d (list (d (n "proptest") (r "^0.9.1") (d #t) (k 2)))) (h "0cdpjb8rnkc1b22i9c70d55r7kjzvn8hf955lhyyj04xpgvwkxz7")))

(define-public crate-higher-0.2.0 (c (n "higher") (v "0.2.0") (d (list (d (n "futures") (r "^0.3.25") (f (quote ("thread-pool"))) (o #t) (d #t) (k 0)) (d (n "higher-derive") (r "^0.2.0") (d #t) (k 0)))) (h "1m222mxk2fd4bc355bxs3v8j7c0s2qa07nfc5r6lwvs09k3d9wg2") (f (quote (("test" "std") ("std") ("io" "std" "futures") ("effect" "futures") ("default" "std" "effect" "io")))) (r "1.65")))

