(define-module (crates-io hi gh highwayhash) #:use-module (crates-io))

(define-public crate-highwayhash-0.0.10 (c (n "highwayhash") (v "0.0.10") (h "0ddyj88iw0l5b5q0wkj8l6riqm08yzycd49hfjiha17lzacad8lr") (y #t)))

(define-public crate-highwayhash-0.0.11 (c (n "highwayhash") (v "0.0.11") (h "1xhhrhjwhclnph9fahzgm0djgwvhaif88amjh24drdx5dirl616l")))

(define-public crate-highwayhash-0.0.12 (c (n "highwayhash") (v "0.0.12") (h "12qgfb2qq7mdilvnkw0480yhg1bbhrrnsnf33lx0nsggnpna2scp")))

(define-public crate-highwayhash-0.0.13 (c (n "highwayhash") (v "0.0.13") (h "1nnby57sjxwlyxmmlkd7g6rzgr2350h0374akxpqkcgxhm8y4qd9")))

(define-public crate-highwayhash-0.0.14 (c (n "highwayhash") (v "0.0.14") (h "1aywaz72di1ikx2xdm65672pn92bfwxr7mid4sk9klvms7zjm8xv")))

