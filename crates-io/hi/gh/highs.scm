(define-module (crates-io hi gh highs) #:use-module (crates-io))

(define-public crate-highs-0.0.0 (c (n "highs") (v "0.0.0") (d (list (d (n "highs-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1b4yx43pc376w96lrqa5p0dkv9q9xm4kxm0b1why3lgnq6qfl2p8")))

(define-public crate-highs-0.0.1 (c (n "highs") (v "0.0.1") (d (list (d (n "highs-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qflw2sr2hk70ixjsf55g01hkjk404zx04wc0hdh7v65ri06xm1p")))

(define-public crate-highs-0.0.3 (c (n "highs") (v "0.0.3") (d (list (d (n "highs-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0xsgj90wf24xybpf8sj1ir6kw8yysw3l8py33r8hr8gz9s5w74mb")))

(define-public crate-highs-0.0.4 (c (n "highs") (v "0.0.4") (d (list (d (n "highs-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0li03bqkb7i3y7y4vlwhvrcxkaiyawpybzw2782di56cjngv549i")))

(define-public crate-highs-0.1.1 (c (n "highs") (v "0.1.1") (d (list (d (n "highs-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fw39a4ic0ar8qqsp2xihaqvyy5lm6sppvmr9qkqp52g8i657vwq")))

(define-public crate-highs-0.1.2 (c (n "highs") (v "0.1.2") (d (list (d (n "highs-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1268wmzmd9mw2xdcmkmcqjq86da0mk8kykih1fjzpk1rw7921ybh")))

(define-public crate-highs-0.2.0 (c (n "highs") (v "0.2.0") (d (list (d (n "highs-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pivxchmji6gqv04jhbff34ci0p57qw2fsgwj4sbsfw7lyhkrz9q")))

(define-public crate-highs-0.3.0 (c (n "highs") (v "0.3.0") (d (list (d (n "highs-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1x2d4i36xc1pmhkvn0i5rv5sg44w4h5w9dp3prl9ah0kiqjh7786")))

(define-public crate-highs-0.3.1 (c (n "highs") (v "0.3.1") (d (list (d (n "highs-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "03laqqxldbqr11apymr83sqklfvwasv6mqjimpxs0kzah4sj0zkb")))

(define-public crate-highs-0.3.2 (c (n "highs") (v "0.3.2") (d (list (d (n "highs-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qps618z3lbnmd01zv722cxhdxcp46zgwb5s59a0dlkb93lrgk6i")))

(define-public crate-highs-0.3.4 (c (n "highs") (v "0.3.4") (d (list (d (n "highs-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0sp6jyzca06d3gpf4a8dagqgwirlyi9zh8xhcnjqf2lgmhy38jba")))

(define-public crate-highs-0.3.5 (c (n "highs") (v "0.3.5") (d (list (d (n "highs-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02fxb1nd5n2c81s9rxydk6gc7d64962f5agpism2rixm79f7k5w3")))

(define-public crate-highs-0.4.0 (c (n "highs") (v "0.4.0") (d (list (d (n "highs-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0g2ic7jnm26n205c0flic1wd2dgfdifa134ng8w6cvpznv1r3p7m")))

(define-public crate-highs-0.5.0 (c (n "highs") (v "0.5.0") (d (list (d (n "highs-sys") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fbn5dyph70ng315mhb335vzffq0vp1jp5vsxhz12a6s2gqb7qdd")))

(define-public crate-highs-0.5.1 (c (n "highs") (v "0.5.1") (d (list (d (n "highs-sys") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0hmkdw9ldab90fwbpw3rmgd0frllqbvwbwczmy1bbfz5cvsj7nw1")))

(define-public crate-highs-0.6.0 (c (n "highs") (v "0.6.0") (d (list (d (n "highs-sys") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1li40zj8drh5qvzcf0xs4n1in9915mzq2r9ih0a9vsn8870zwsqv")))

(define-public crate-highs-0.6.1 (c (n "highs") (v "0.6.1") (d (list (d (n "highs-sys") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1fl2pdmlyijw5ij7lzsfnwxbnp893bs8fmi3kd2dm1i8hjl5z7w2")))

(define-public crate-highs-1.2.1 (c (n "highs") (v "1.2.1") (d (list (d (n "highs-sys") (r "^1.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0pxj5ndr367pmbp0h6mcdq064wjk7nh4n8xc9ky3b6midwv0kai0")))

(define-public crate-highs-1.2.2 (c (n "highs") (v "1.2.2") (d (list (d (n "highs-sys") (r "^1.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1kbqshdzksdvmh13f0yhjhshmpl4mcg5da2ac31ciwif5i5zi39q")))

(define-public crate-highs-1.5.0 (c (n "highs") (v "1.5.0") (d (list (d (n "highs-sys") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1jdq2adykcl3i8cdc3xjf3j5mp3bdc4f973rz4ybv4pkk89ac0fz")))

(define-public crate-highs-1.5.1 (c (n "highs") (v "1.5.1") (d (list (d (n "highs-sys") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "1r9g24x8ri7k04wl513p8hrlcqjbchjz7bh2aqls0a69h71584v2")))

(define-public crate-highs-1.6.0 (c (n "highs") (v "1.6.0") (d (list (d (n "highs-sys") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "11dh7akwp9xhknirn8gkaxcx844iadaml2kb4ww475wr9p3ii3a9")))

(define-public crate-highs-1.6.1 (c (n "highs") (v "1.6.1") (d (list (d (n "highs-sys") (r "^1.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)))) (h "0z3fq4iaax6h6kajkv12f7zd1phcpf4rvk9mbgddwhidkifwqd3w")))

