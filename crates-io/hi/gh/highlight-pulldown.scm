(define-module (crates-io hi gh highlight-pulldown) #:use-module (crates-io))

(define-public crate-highlight-pulldown-0.1.0 (c (n "highlight-pulldown") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "04chrxsgzs2vp5y8y2h9wgcyi7jf644bsrdl9j65sd8mkb6s2sin")))

(define-public crate-highlight-pulldown-0.1.1 (c (n "highlight-pulldown") (v "0.1.1") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0c2x7m9zglq53107ygpdm13pshlq88ddns47k5895r6r8nf1hc6n")))

(define-public crate-highlight-pulldown-0.2.0 (c (n "highlight-pulldown") (v "0.2.0") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0ndi0ax11fayi05r3id1bs970x1z20lhf5f9mzibxffbvyiyj2hk")))

(define-public crate-highlight-pulldown-0.2.1 (c (n "highlight-pulldown") (v "0.2.1") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "0qwykinnr1228wfvhpwxi1gax9kly7wwhg7mfp4kgzchv72sagc8")))

(define-public crate-highlight-pulldown-0.2.2 (c (n "highlight-pulldown") (v "0.2.2") (d (list (d (n "pulldown-cmark") (r "^0.9.3") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 0)))) (h "1mm5zyn10ahhp6w15h89bxlawmw3hjpd0xq7fywf3mf71cslkdhy")))

