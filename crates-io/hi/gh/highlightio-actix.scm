(define-module (crates-io hi gh highlightio-actix) #:use-module (crates-io))

(define-public crate-highlightio-actix-1.0.0 (c (n "highlightio-actix") (v "1.0.0") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "highlightio") (r "^1.0.0") (f (quote ("tokio-current-thread"))) (k 0)) (d (n "opentelemetry") (r "^0.21") (f (quote ("trace"))) (d #t) (k 0)) (d (n "opentelemetry-semantic-conventions") (r "^0.13") (d #t) (k 0)))) (h "12xrakp7nbir04l40cjsfcd7j601vkm9afkikbf6l8vpbxr9jz5f")))

(define-public crate-highlightio-actix-1.0.2 (c (n "highlightio-actix") (v "1.0.2") (d (list (d (n "actix-web") (r "^4") (k 0)) (d (n "futures-util") (r "^0.3") (d #t) (k 0)) (d (n "highlightio") (r "^1.0.0") (f (quote ("tokio-current-thread"))) (k 0)) (d (n "opentelemetry") (r "^0.21") (f (quote ("trace"))) (d #t) (k 0)) (d (n "opentelemetry-semantic-conventions") (r "^0.13") (d #t) (k 0)))) (h "0jqsa7b7mhzv9yqpzgjiqw0r049zzb05c3aaz3c6hd15dcxkcr4l")))

