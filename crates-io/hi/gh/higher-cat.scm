(define-module (crates-io hi gh higher-cat) #:use-module (crates-io))

(define-public crate-higher-cat-0.1.0 (c (n "higher-cat") (v "0.1.0") (d (list (d (n "higher") (r "^0.1.0") (d #t) (k 0)) (d (n "higher-derive") (r "^0.1.0") (d #t) (k 2)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)))) (h "0hxd4gb9qh2xgkrkaflgbp50bj8ikg5k5bhqik8xg3h4ssw9vrim")))

(define-public crate-higher-cat-0.1.1 (c (n "higher-cat") (v "0.1.1") (d (list (d (n "higher") (r "^0.1.1") (d #t) (k 0)) (d (n "higher-derive") (r "^0.1.1") (d #t) (k 2)) (d (n "proptest") (r "^0.9.1") (d #t) (k 2)))) (h "135iwhcihs75a7p72shl7y4mv4cyc9pgnv1gv88dj5kyhcg3gjs7")))

