(define-module (crates-io hi lb hilbert_index) #:use-module (crates-io))

(define-public crate-hilbert_index-0.1.0 (c (n "hilbert_index") (v "0.1.0") (h "0kj12f46axj7arriqav2rlis6m468nvsp40dji86rddpa50xan1x")))

(define-public crate-hilbert_index-0.2.0 (c (n "hilbert_index") (v "0.2.0") (h "0876zkiyr3az59g10cdg6jsh334nxgnajfi2msdlwkw8aqfa9jmh")))

