(define-module (crates-io hi lb hilbert) #:use-module (crates-io))

(define-public crate-hilbert-0.1.0 (c (n "hilbert") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 0)))) (h "128mzh443x3j34c6brfdqqxn36wxakbkjiwjfi23x645bgl71inr")))

(define-public crate-hilbert-0.1.1 (c (n "hilbert") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 0)))) (h "16ys6bnnlxjdndvaqgpvv6g21j6r78zddwfgdbbnk53q7b2px89b")))

(define-public crate-hilbert-0.1.2 (c (n "hilbert") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "spectral") (r "^0.6.0") (d #t) (k 0)))) (h "1y6az4r6xr6kk6b201d98wblvzl34s7ynf2f4sm3ba832s08738p")))

