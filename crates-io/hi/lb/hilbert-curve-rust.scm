(define-module (crates-io hi lb hilbert-curve-rust) #:use-module (crates-io))

(define-public crate-hilbert-curve-rust-0.1.0 (c (n "hilbert-curve-rust") (v "0.1.0") (h "07m9f2w5asrgv2qzfcr3havyz4vfasickgi7044ig5xfhfxnz9za")))

(define-public crate-hilbert-curve-rust-0.1.2 (c (n "hilbert-curve-rust") (v "0.1.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "fast_hilbert") (r "^1.0.1") (d #t) (k 2)) (d (n "hilbert") (r "^0.1.1") (d #t) (k 2)) (d (n "hilbert_2d") (r "^1.0.0") (d #t) (k 2)) (d (n "hilbert_curve") (r "^0.2.0") (d #t) (k 2)))) (h "1gjh3nzkvvmja80iif1875gi9hkc2gbp1sr2ljzfqb07pdxfrlrj")))

