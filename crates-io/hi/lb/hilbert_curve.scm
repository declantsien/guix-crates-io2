(define-module (crates-io hi lb hilbert_curve) #:use-module (crates-io))

(define-public crate-hilbert_curve-0.1.0 (c (n "hilbert_curve") (v "0.1.0") (h "192nnyj1dmk1hz9shq57s90dvxxzsbkhhd7ngr6fk4cc6y7kkd2m")))

(define-public crate-hilbert_curve-0.2.0 (c (n "hilbert_curve") (v "0.2.0") (h "1nbrfwixlnhj7w4b03x7ink1nb2zycrn3mhjiarsxjdcwb4gr601")))

