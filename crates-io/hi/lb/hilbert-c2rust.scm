(define-module (crates-io hi lb hilbert-c2rust) #:use-module (crates-io))

(define-public crate-hilbert-c2rust-0.1.0 (c (n "hilbert-c2rust") (v "0.1.0") (h "10fcnbbhgpmp6hq97m2f1rrbjp6c4v4xy2j4swgbcmr5l7zxrykw")))

(define-public crate-hilbert-c2rust-0.1.1 (c (n "hilbert-c2rust") (v "0.1.1") (h "0d7h4q7wbi1k3ppr860z5jca6l1q12bchc8xfcz6rff6ih7wi662")))

(define-public crate-hilbert-c2rust-0.1.2 (c (n "hilbert-c2rust") (v "0.1.2") (h "0h2f9c5frq9ky2175l0m8pn84bvbrvh7y3b88wv3x4jsscjjh9sz")))

(define-public crate-hilbert-c2rust-0.1.3 (c (n "hilbert-c2rust") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1wbx42idi9cczklv0hp39nrrm65dmxpvfhb91lass9d68a1la1cn")))

(define-public crate-hilbert-c2rust-0.1.4 (c (n "hilbert-c2rust") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1v423yicxmyjjymja8ppmddjhg95glhkq9pjxd3n1zmzz8lci3n3")))

(define-public crate-hilbert-c2rust-0.1.5 (c (n "hilbert-c2rust") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.51") (d #t) (k 0)))) (h "1gzcfms89nhbz7pdm56gidgf1rax3h5m2aiyhp0pqqccy84rw15i")))

