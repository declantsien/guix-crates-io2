(define-module (crates-io hi lb hilbert_transform) #:use-module (crates-io))

(define-public crate-hilbert_transform-0.1.0 (c (n "hilbert_transform") (v "0.1.0") (d (list (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0s9l7gdpfv54bb3fsm8ir4kfbaahmfl5is4kcgcbdxvgqij092wn")))

(define-public crate-hilbert_transform-0.1.1 (c (n "hilbert_transform") (v "0.1.1") (d (list (d (n "rustfft") (r "^6.1.0") (d #t) (k 0)))) (h "0a9hkcr4pj1qhpa7mx7bkfi2kak1k4n89bvsw3pln80qax5rwkw5")))

