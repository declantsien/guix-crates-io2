(define-module (crates-io hi lb hilbert_image_to_sound) #:use-module (crates-io))

(define-public crate-hilbert_image_to_sound-0.1.0 (c (n "hilbert_image_to_sound") (v "0.1.0") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "hilbert_curve") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.0") (d #t) (k 0)))) (h "1z6nmmf1xnhdgw705hnadcp4v4fmyv88q6s3ykzj3w5kld1yyaa8")))

(define-public crate-hilbert_image_to_sound-0.1.1 (c (n "hilbert_image_to_sound") (v "0.1.1") (d (list (d (n "cpal") (r "^0.11.0") (d #t) (k 0)) (d (n "hilbert_curve") (r "^0.2.0") (d #t) (k 0)) (d (n "image") (r "^0.23.0") (d #t) (k 0)))) (h "1khs6a1gdhz2qq8f1z713rfl31iack9z854gvf9amqi9dzkb9b88")))

