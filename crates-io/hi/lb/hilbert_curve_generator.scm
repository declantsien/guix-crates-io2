(define-module (crates-io hi lb hilbert_curve_generator) #:use-module (crates-io))

(define-public crate-hilbert_curve_generator-0.1.0 (c (n "hilbert_curve_generator") (v "0.1.0") (h "0c23vpi4bhh32mm7idxlamz255sq9ap8hmll5vh2n88bwv07x2fm")))

(define-public crate-hilbert_curve_generator-0.1.1 (c (n "hilbert_curve_generator") (v "0.1.1") (h "08jsp0l0ifhz98ka7xz016adz51lp7y13n1rs83dkpr5an81rqsn")))

(define-public crate-hilbert_curve_generator-0.1.2 (c (n "hilbert_curve_generator") (v "0.1.2") (h "04szp2jvpkph7na1z63x11k6sqr6pxnwhh9wgfgb0a75xh3ip0v5")))

