(define-module (crates-io hi lb hilbert_2d) #:use-module (crates-io))

(define-public crate-hilbert_2d-1.0.0 (c (n "hilbert_2d") (v "1.0.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "palette") (r "^0.5.0") (d #t) (k 2)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "1xqis4rdndrsqy2hny6m5byaiqxdyrdg8lrax7jxbwj08jyg3991")))

(define-public crate-hilbert_2d-1.1.0 (c (n "hilbert_2d") (v "1.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1.0") (d #t) (k 2)) (d (n "palette") (r "^0.5.0") (d #t) (k 2)) (d (n "plotlib") (r "^0.5.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.1") (d #t) (k 2)))) (h "0p5yz3wjfbp7jlngl8868f59d2jznvvcf0f76npk85xi8bh82pvh")))

