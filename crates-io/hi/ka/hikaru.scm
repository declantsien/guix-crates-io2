(define-module (crates-io hi ka hikaru) #:use-module (crates-io))

(define-public crate-hikaru-0.1.0 (c (n "hikaru") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1khps7i62hz69zxjgnawq9m41r9qgya265n3kvh7f8rn5br605j5")))

(define-public crate-hikaru-0.1.1 (c (n "hikaru") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14ljp9xjmhpk36bjvrnix80y455l9567grcj624lkz15p6kdcljm")))

(define-public crate-hikaru-0.1.2 (c (n "hikaru") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1aypfmp56kckrgx8cpjbw545f7yzcbpd3595mfginw2alj5fcd1q")))

