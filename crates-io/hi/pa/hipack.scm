(define-module (crates-io hi pa hipack) #:use-module (crates-io))

(define-public crate-hipack-0.1.0 (c (n "hipack") (v "0.1.0") (h "1iz5db7zbbjxj8h7i23jhx4lc4vq6h1mblksl1jn0mzfcwy4r3nr")))

(define-public crate-hipack-0.1.1 (c (n "hipack") (v "0.1.1") (h "11if2bz6wvdvp6fj6iyb06glxb2anb1kqki3fgabyr80f3blg1p2")))

