(define-module (crates-io hi po hipool) #:use-module (crates-io))

(define-public crate-hipool-0.1.0 (c (n "hipool") (v "0.1.0") (d (list (d (n "hierr") (r "^0.1") (d #t) (k 0)))) (h "1ilhg5lizajjpf3659mz43w42pzz0q4larv3c88hkiz6q2jpa478")))

(define-public crate-hipool-0.1.1 (c (n "hipool") (v "0.1.1") (d (list (d (n "hierr") (r "^0.1") (d #t) (k 0)))) (h "1qrfajsi3mwg0l9a2qgbmdf5s6nmv38j7w2ayfg8hw6vay8znjgi")))

