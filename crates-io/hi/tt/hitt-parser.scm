(define-module (crates-io hi tt hitt-parser) #:use-module (crates-io))

(define-public crate-hitt-parser-0.0.1 (c (n "hitt-parser") (v "0.0.1") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)))) (h "12482djakaxs37drfr2aj1san0hwy921cpsn067q6y2afh8ggmiw") (r "1.70.0")))

(define-public crate-hitt-parser-0.0.2 (c (n "hitt-parser") (v "0.0.2") (d (list (d (n "http") (r "^0.2.11") (d #t) (k 0)))) (h "1k937634hnj8qb65kb5lkckxdqvpmhl5wq4nx0gg22814w066j8s") (r "1.70.0")))

(define-public crate-hitt-parser-0.0.3 (c (n "hitt-parser") (v "0.0.3") (d (list (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "13s7c7zlq12x327dkfdihyk582ww0j07pjlci5nlmd7nbn6j6c2b") (r "1.70.0")))

(define-public crate-hitt-parser-0.0.4 (c (n "hitt-parser") (v "0.0.4") (d (list (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "126p1pz7akcbfz7ciifynq5g6agcrjm5kg7z4wp21gam14dqjb7q") (r "1.70.0")))

(define-public crate-hitt-parser-0.0.5 (c (n "hitt-parser") (v "0.0.5") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "1v6plxbqkr514hgw7cirbxs63xqqxixlkipzal7hdjlappam9wkv") (r "1.74.0")))

(define-public crate-hitt-parser-0.0.6 (c (n "hitt-parser") (v "0.0.6") (d (list (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 2)))) (h "107k3n8dcjyy1wvf5qmwc3cpxk1shxhmsnvdgl67743vd0gvfkdx") (r "1.74.0")))

