(define-module (crates-io hi tt hitt-request) #:use-module (crates-io))

(define-public crate-hitt-request-0.0.1 (c (n "hitt-request") (v "0.0.1") (d (list (d (n "hitt-parser") (r "^0.0.1") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "08paj8dgzi4cdma4sy7k7n7xsypb3kvlda1wqhv3kinb4n6cy5yk") (r "1.70.0")))

(define-public crate-hitt-request-0.0.2 (c (n "hitt-request") (v "0.0.2") (d (list (d (n "hitt-parser") (r "^0.0.2") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "196456wy7lxhxqdd0xzp4lswcqwgk6jzzfsqr2yp57qy8chrl4dq") (r "1.70.0")))

(define-public crate-hitt-request-0.0.3 (c (n "hitt-request") (v "0.0.3") (d (list (d (n "hitt-parser") (r "^0.0.3") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1j7hdvhwaadips9fvf8yqwjlcg37szag1cs5k8qpsmvfjgasvsza") (r "1.70.0")))

(define-public crate-hitt-request-0.0.4 (c (n "hitt-request") (v "0.0.4") (d (list (d (n "hitt-parser") (r "^0.0.4") (d #t) (k 0)) (d (n "http") (r "^0.2.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1jz0rcmjbsx06933mrma2di5y5zy2ba2jc37rwnid3dbkawd1ic5") (r "1.70.0")))

(define-public crate-hitt-request-0.0.5 (c (n "hitt-request") (v "0.0.5") (d (list (d (n "hitt-parser") (r "^0.0.5") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.2") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "05gbv1vawmcikvff4i2h38b9brpnvfzjk6v8rqs50hiyhsk4v39h") (r "1.74.0")))

(define-public crate-hitt-request-0.0.6 (c (n "hitt-request") (v "0.0.6") (d (list (d (n "hitt-parser") (r "^0.0.6") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("fs" "macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1p7jnvx8b9wr7arkwai3qmmwkpx0cqwl2mj02n91wf3bynnf3g6s") (r "1.74.0")))

