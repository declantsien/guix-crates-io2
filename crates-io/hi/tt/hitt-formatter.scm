(define-module (crates-io hi tt hitt-formatter) #:use-module (crates-io))

(define-public crate-hitt-formatter-0.0.1 (c (n "hitt-formatter") (v "0.0.1") (d (list (d (n "jsonformat") (r "^2.0.0") (d #t) (k 0)))) (h "1xma0x2gs7y5aj55m7ldn13gwainiaz5nldjf2r2rch45hz60wy1") (r "1.70.0")))

(define-public crate-hitt-formatter-0.0.2 (c (n "hitt-formatter") (v "0.0.2") (d (list (d (n "jsonformat") (r "^2.0.0") (d #t) (k 0)))) (h "0fj1f0p5c0vf80mj6crlqhb5xzklf5l4jvqx9rdha84sjfsxwnm8") (r "1.70.0")))

(define-public crate-hitt-formatter-0.0.3 (c (n "hitt-formatter") (v "0.0.3") (d (list (d (n "jsonformat") (r "^2.0.0") (d #t) (k 0)))) (h "1pgag65ppfyml6klardbk7cp2x5skknjsbfgdbs9npj1fdkxnv64") (r "1.70.0")))

(define-public crate-hitt-formatter-0.0.4 (c (n "hitt-formatter") (v "0.0.4") (d (list (d (n "jsonformat") (r "^2.0.0") (d #t) (k 0)))) (h "09fjq77jngcjb1sg6zpy70b4ain3b8rxjyrj4z4n88p7ymiyvj0b") (r "1.70.0")))

(define-public crate-hitt-formatter-0.0.5 (c (n "hitt-formatter") (v "0.0.5") (d (list (d (n "jsonformat") (r "^2.0.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "0gl334f780zq5y6jnn93vdknnr153csz6bh4fnl9pv0ldy097281") (r "1.74.0")))

(define-public crate-hitt-formatter-0.0.6 (c (n "hitt-formatter") (v "0.0.6") (d (list (d (n "jsonformat") (r "^2.0.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.17") (d #t) (k 0)))) (h "13b6nwzizsq3h1pi56rqyv5zkvk5s50r8lriwi31sj9g1a8ifdda") (r "1.74.0")))

