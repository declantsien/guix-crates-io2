(define-module (crates-io hi nt hinted) #:use-module (crates-io))

(define-public crate-hinted-0.0.0 (c (n "hinted") (v "0.0.0") (h "1wzclvak3446x09353zs6h0hfgrksvcsn1wzrrrn69ai6llcdx6l")))

(define-public crate-hinted-0.0.1 (c (n "hinted") (v "0.0.1") (h "0sdsfx8by5cg2y5rqq677xly7sgw0r4sk7dmq9dpr4g97vfbnrcr") (f (quote (("nightly"))))))

