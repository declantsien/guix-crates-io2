(define-module (crates-io hi -a hi-apns) #:use-module (crates-io))

(define-public crate-hi-apns-0.0.1 (c (n "hi-apns") (v "0.0.1") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "157hr752bxcfwnycd4c0ykqw22dqz0960njk9aqswj2n2pmmfzyk")))

(define-public crate-hi-apns-0.0.2 (c (n "hi-apns") (v "0.0.2") (d (list (d (n "curl") (r "^0.4.44") (d #t) (k 0)) (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json" "native-tls-vendored"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "uuid") (r "^1.1") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "07lykk3y0vwlc1r7zqzs8lg0lxfn238drh0zgizg87kc7y4abihr")))

