(define-module (crates-io hi -a hi-auth) #:use-module (crates-io))

(define-public crate-hi-auth-0.0.1 (c (n "hi-auth") (v "0.0.1") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "oauth2") (r "^4.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0ljsc8rp9fwg8r4gm5h61bw60m74xf4zjbd599rmm9133zhp5m75")))

(define-public crate-hi-auth-0.0.2 (c (n "hi-auth") (v "0.0.2") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "oauth2") (r "^4.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "05m96lna82s5y949rhbyq8w5ddc0yljymp73hf0lbz2qjjg3iyy6")))

(define-public crate-hi-auth-0.0.3 (c (n "hi-auth") (v "0.0.3") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "oauth2") (r "^4.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "02m3vazi9frvn8fq0jrdapxz2wfx9sbgdgqc7ahdpvxgmgz6yfn6")))

(define-public crate-hi-auth-0.0.4 (c (n "hi-auth") (v "0.0.4") (d (list (d (n "async-trait") (r "^0.1.64") (d #t) (k 0)) (d (n "oauth2") (r "^4.3.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 2)))) (h "026q07zrh0d2yhzvv5gljz38ha8vxazga77wxngy83kdm28iwagd")))

