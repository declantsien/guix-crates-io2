(define-module (crates-io hi er hiero_pack) #:use-module (crates-io))

(define-public crate-hiero_pack-0.1.0 (c (n "hiero_pack") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fia69rbmr1ymhf2rzxl375v6jy9nm3790ssnrb65m3r7bbhsrbd") (f (quote (("default" "console") ("console" "clap"))))))

(define-public crate-hiero_pack-0.1.1 (c (n "hiero_pack") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (f (quote ("derive"))) (d #t) (k 0)))) (h "141brbzyrl7axri0gyfxqqyrdrg5wh25kdc4d5l7h9122mqg8x3l") (f (quote (("default" "console") ("console" "clap"))))))

(define-public crate-hiero_pack-0.1.3 (c (n "hiero_pack") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (o #t) (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 0)) (d (n "serde") (r "^1.0.63") (f (quote ("derive"))) (d #t) (k 0)))) (h "1pcjwj0aw7ab0k05fxnaip1zb34y4lp19gc5nsxjvrz1gzhdglw1") (f (quote (("default" "console") ("console" "clap"))))))

