(define-module (crates-io hi er hierr) #:use-module (crates-io))

(define-public crate-hierr-0.1.0 (c (n "hierr") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "0lhmsnq190pali57frmc15izjcmdj13x9mw9f89pj7sni1fd13qx")))

(define-public crate-hierr-0.1.1 (c (n "hierr") (v "0.1.1") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)))) (h "1ngkphs9rwgknvs7lrhf7wwgk0mpvspjzd67pp93mgyfr7c6k90f")))

