(define-module (crates-io hi er hierarchical_pathfinding) #:use-module (crates-io))

(define-public crate-hierarchical_pathfinding-0.1.0 (c (n "hierarchical_pathfinding") (v "0.1.0") (h "1q6r0jlnzcxazy9z4h1ybfrs49n4xigzvchp7h8jj1bq8h1mmd3p")))

(define-public crate-hierarchical_pathfinding-0.2.0 (c (n "hierarchical_pathfinding") (v "0.2.0") (h "1icxyzg892v3jchl25f43c4n2hh2568f67k8dcmix7yr2fdsp1s9")))

(define-public crate-hierarchical_pathfinding-0.3.0 (c (n "hierarchical_pathfinding") (v "0.3.0") (h "1gk3d1wvjf49hgn6a71w3lmxwk7hr5ncxdcbbfawfrhwx1k3gcik")))

(define-public crate-hierarchical_pathfinding-0.3.5 (c (n "hierarchical_pathfinding") (v "0.3.5") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "1w6l3i6hbpniwwd1z9qgrpgcmn6rrr04yx1j9173sh8a16n5ygyi")))

(define-public crate-hierarchical_pathfinding-0.3.6 (c (n "hierarchical_pathfinding") (v "0.3.6") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)))) (h "06yn3z26wmis2bgc013zndci8y3lsyish38rkf1ysbi609nw9vjx")))

(define-public crate-hierarchical_pathfinding-0.4.0 (c (n "hierarchical_pathfinding") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "0lq5wvi56ssddjqqv9c7xqg6srndz8q066dnshx3xfngv6haaawd") (y #t)))

(define-public crate-hierarchical_pathfinding-0.4.1 (c (n "hierarchical_pathfinding") (v "0.4.1") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "19nc1hv43c83m1zs2n6pi4pnmnrc70pdd734bwfhr2wsljynfy6c")))

(define-public crate-hierarchical_pathfinding-0.4.2 (c (n "hierarchical_pathfinding") (v "0.4.2") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)))) (h "1l5533x4sy73hr1wr2af5qh5j3k2xy1gzqbf74if7q929gsqaga5")))

(define-public crate-hierarchical_pathfinding-0.4.3 (c (n "hierarchical_pathfinding") (v "0.4.3") (d (list (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8") (f (quote ("std_rng"))) (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "0imwwhjql58bp6cf91rii4nzgxv9zv77dpvl4hjqhsbifi963sk1") (f (quote (("default" "rayon"))))))

(define-public crate-hierarchical_pathfinding-0.5.0 (c (n "hierarchical_pathfinding") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "hashbrown") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "nanorand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)))) (h "1cn9zarvd5xg5yyy2rvrwmnv0a2s7diqyqk7csd7123xi48h8kbb") (f (quote (("parallel" "rayon" "hashbrown/rayon") ("default" "parallel"))))))

