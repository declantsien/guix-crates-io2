(define-module (crates-io hi er hier) #:use-module (crates-io))

(define-public crate-hier-0.1.0 (c (n "hier") (v "0.1.0") (d (list (d (n "graphviz-rust") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (f (quote ("invocation"))) (d #t) (k 0)))) (h "1znlyadi2jwr4njx7sb6mlnxypp771d6zsghwc40xc80jiav6q69") (s 2) (e (quote (("graph" "dep:graphviz-rust"))))))

(define-public crate-hier-0.2.0 (c (n "hier") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.4.2") (d #t) (k 0)) (d (n "graphviz-rust") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "phf") (r "^0.11.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0avqq6k5jqbqbkbsz7a2a3zsxh9g91kskwizq3qg2n7kmwym2izw") (f (quote (("invocation" "jni/invocation") ("defaults")))) (s 2) (e (quote (("graph" "dep:graphviz-rust"))))))

