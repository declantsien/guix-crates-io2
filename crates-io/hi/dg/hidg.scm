(define-module (crates-io hi dg hidg) #:use-module (crates-io))

(define-public crate-hidg-0.1.0 (c (n "hidg") (v "0.1.0") (d (list (d (n "hidg-core") (r "^0.1.0") (k 0)))) (h "08qj9qjgpvrgd6fp612g046jqn4ahmkf67z8v7scpp9avpy69p9w") (f (quote (("serde" "hidg-core/serde") ("phf" "hidg-core/phf") ("mouse" "hidg-core/mouse") ("keyboard" "hidg-core/keyboard") ("fromstr" "hidg-core/fromstr") ("either" "hidg-core/either") ("display" "hidg-core/display") ("default" "fromstr" "display" "phf" "keyboard" "mouse"))))))

(define-public crate-hidg-0.1.1 (c (n "hidg") (v "0.1.1") (d (list (d (n "hidg-core") (r "^0.1.1") (k 0)))) (h "0naqz9d1p3sm15yikbsr2dl2gcalbyrvkxfbxxz4ykznbydw02iy") (f (quote (("serde" "hidg-core/serde") ("phf" "hidg-core/phf") ("mouse" "hidg-core/mouse") ("keyboard" "hidg-core/keyboard") ("fromstr" "hidg-core/fromstr") ("either" "hidg-core/either") ("display" "hidg-core/display") ("default" "fromstr" "display" "phf" "keyboard" "mouse"))))))

(define-public crate-hidg-0.2.0 (c (n "hidg") (v "0.2.0") (d (list (d (n "hidg-core") (r "^0.2") (k 0)))) (h "1h42bvy9zcr00skaqsq7vkz4ndyz2ykgpxrgc923kljipvr4nlh1") (f (quote (("serde" "hidg-core/serde") ("phf" "hidg-core/phf") ("mouse" "hidg-core/mouse") ("keyboard" "hidg-core/keyboard") ("fromstr" "hidg-core/fromstr") ("either" "hidg-core/either") ("display" "hidg-core/display") ("default" "fromstr" "display" "phf" "keyboard" "mouse"))))))

