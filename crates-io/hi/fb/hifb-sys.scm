(define-module (crates-io hi fb hifb-sys) #:use-module (crates-io))

(define-public crate-hifb-sys-0.1.0 (c (n "hifb-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1l8rzjrs0nb9afj78xcf4kmm689mhi8wdvpgsdihzkw890qpjnf0") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default")))) (y #t)))

(define-public crate-hifb-sys-0.1.1 (c (n "hifb-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "04g5qiqkds1dypjh98lxqxnayad48xvf87l5lhvbwwnha29pfa6a") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hifb-sys-0.1.2 (c (n "hifb-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "0ckpjyivvk1vm7ylrjarw90mwnf04grmm7y8dg9c3gqlz1ijq369") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hifb-sys-0.1.3 (c (n "hifb-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1b5rkhaahc7gnmzi2195nxg24p3wf1dfj3qy7pb513wmga7a9iyr") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hifb-sys-0.1.4 (c (n "hifb-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1hxhpjn9x09q1a7jlc9y2k71w8hy5rphkafi9g2bhcyfzbvavn8r") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hifb-sys-0.1.5 (c (n "hifb-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1xchmn251g705mw05w96c0cnmbgywbih2c7hg8d9x9wijxrs829b") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hifb-sys-0.1.6 (c (n "hifb-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "0zfrs4zb8k1j55q410xbgxjzbds1k2lf9flm40bldhkw1myf3rg9") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hifb-sys-0.1.7 (c (n "hifb-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1d3g6xr17ra15mapq88bx2xcwcg7y645abd3fk66czyyhgi7d9s7") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hifb-sys-0.1.8 (c (n "hifb-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1fls9i6lmv0nm7iq6q4svs3518hd99kgrm55hhby9yjnssffvsmv") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hifb-sys-0.1.9 (c (n "hifb-sys") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1irbpk8w04cpzm0g34wr8cdik3qkjx0zgv62rasvigvfhbm4a4nb") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hifb-sys-0.1.10 (c (n "hifb-sys") (v "0.1.10") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1hrwsgbm4nyddpxln1hjxfdb19pdgpdl5l0hlp4npj83ycl5ijaj") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hifb-sys-0.1.11 (c (n "hifb-sys") (v "0.1.11") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1rkvmx05sdad42wgx4kp6g9y7mx72rcjhqa38m976r5gw9z6i2gh") (f (quote (("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

