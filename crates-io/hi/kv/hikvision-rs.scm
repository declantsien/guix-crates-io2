(define-module (crates-io hi kv hikvision-rs) #:use-module (crates-io))

(define-public crate-hikvision-rs-0.0.0 (c (n "hikvision-rs") (v "0.0.0") (d (list (d (n "reqwest") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "09f2445g08s8bgn5hhwfgsc6nvcz602ggdbyqprhypnhhprzs14l") (y #t)))

(define-public crate-hikvision-rs-0.0.1 (c (n "hikvision-rs") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0r52sksmfxnfygzmhz67qjckq0246fg6x7h4mv5k2ll4d06bpzjn") (y #t)))

(define-public crate-hikvision-rs-0.0.2 (c (n "hikvision-rs") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0bv8kwaxl93dbp6ysgf0708hb882z9hjm9s8561fjmbfxmrnamjs") (y #t)))

(define-public crate-hikvision-rs-0.0.3 (c (n "hikvision-rs") (v "0.0.3") (d (list (d (n "reqwest") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ixnmihqal4px3pzcxqjn4f3lgg56057rlfidn1zdrigsa1fjv6x") (y #t)))

(define-public crate-hikvision-rs-0.0.4 (c (n "hikvision-rs") (v "0.0.4") (d (list (d (n "reqwest") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10w5ai6fjrp3xwrcyfq7g6p2w2mfzlqsjckkgmnh9r8d8yfv11cm") (y #t)))

(define-public crate-hikvision-rs-0.0.5 (c (n "hikvision-rs") (v "0.0.5") (d (list (d (n "reqwest") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12jn0ni43m24fy8kyjphg725y41bfrzjbxi8w9jmp2s4jhhxbvsx")))

(define-public crate-hikvision-rs-0.0.6 (c (n "hikvision-rs") (v "0.0.6") (d (list (d (n "reqwest") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1pbajs53vww299c3ppqbvxwbp2lhmx2dq1bvx0wp8xfmwpahww3y")))

(define-public crate-hikvision-rs-0.0.7 (c (n "hikvision-rs") (v "0.0.7") (d (list (d (n "reqwest") (r "^0.12.0") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1h8ayvpg7vjf6d5fa0nb6m2v8l8xmxldbhkj85ih02yjc5p81ar1")))

