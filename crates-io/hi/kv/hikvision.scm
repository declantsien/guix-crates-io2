(define-module (crates-io hi kv hikvision) #:use-module (crates-io))

(define-public crate-hikvision-0.1.0 (c (n "hikvision") (v "0.1.0") (d (list (d (n "e-utils") (r "^0.3") (f (quote ("algorithm" "macros" "ui"))) (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "080sj77wh8arm9iqklqcpipga3jln4gfkm4w60jn535hcg4sfbp2") (f (quote (("net") ("mvs") ("default")))) (r "1.76.0")))

(define-public crate-hikvision-0.1.1 (c (n "hikvision") (v "0.1.1") (d (list (d (n "e-utils") (r "^0.3") (f (quote ("algorithm" "macros" "ui"))) (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "078jlhw1310fb9bbd8lz5qd9wsxxg4gm12slymki96phi6lfjwq4") (f (quote (("net") ("mvs") ("default")))) (y #t) (r "1.76.0")))

(define-public crate-hikvision-0.1.2 (c (n "hikvision") (v "0.1.2") (d (list (d (n "e-utils") (r "^0.3") (f (quote ("algorithm" "macros" "ui"))) (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1f2ird1vs7hjkk8j1hg9s8afsqdvakyyzqiwv6fx3jqpy11xrk5g") (f (quote (("net") ("mvs") ("default")))) (y #t) (r "1.76.0")))

(define-public crate-hikvision-0.1.4 (c (n "hikvision") (v "0.1.4") (d (list (d (n "e-utils") (r "^0.3") (f (quote ("algorithm" "macros" "ui"))) (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nan38fd7m3gb5ikkzz5lsf2l2rwnx6bk2zzkcbhrqq7vfh6kf66") (f (quote (("net") ("mvs") ("default")))) (y #t) (r "1.76.0")))

(define-public crate-hikvision-0.1.5 (c (n "hikvision") (v "0.1.5") (d (list (d (n "e-utils") (r "^0.3") (f (quote ("algorithm" "macros" "ui"))) (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pbmcahrg4ysc300dkdp1glqf4hssmphz4ksqyi9iy5b7l56qy7c") (f (quote (("net") ("mvs") ("default" "net" "mvs")))) (y #t) (r "1.76.0")))

(define-public crate-hikvision-0.1.6 (c (n "hikvision") (v "0.1.6") (d (list (d (n "e-utils") (r "^0.3") (f (quote ("algorithm" "macros"))) (d #t) (k 0)) (d (n "libloading") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z30b97lwh7azbz1a7h53zvlh6hbfmqlmhy409n5mlvdmpi3h3sb") (f (quote (("ui" "e-utils/ui") ("net") ("mvs") ("default")))) (r "1.76.0")))

