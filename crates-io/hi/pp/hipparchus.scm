(define-module (crates-io hi pp hipparchus) #:use-module (crates-io))

(define-public crate-hipparchus-0.1.2 (c (n "hipparchus") (v "0.1.2") (d (list (d (n "hipparchus-az") (r "^0.1") (d #t) (k 0)) (d (n "hipparchus-geo") (r "^0.1") (d #t) (k 0)) (d (n "hipparchus-mean") (r "^0.1") (d #t) (k 0)) (d (n "hipparchus-metrics") (r "^0.1") (d #t) (k 0)) (d (n "hipparchus-seq") (r "^0.1") (d #t) (k 0)))) (h "14fgfdaxgv0djc174iawlpag3lhsl9jlaiz98fd5ygfphv5bzcn6") (r "1.70.0")))

