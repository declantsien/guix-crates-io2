(define-module (crates-io hi pp hipparchus-geo) #:use-module (crates-io))

(define-public crate-hipparchus-geo-0.1.2 (c (n "hipparchus-geo") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hipparchus-az") (r "^0.1") (d #t) (k 0)) (d (n "hipparchus-mean") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0mqhd6wy4lp5dxgf4ak4qhazr05y886ic51rrhp74rm66j6pw76q") (r "1.70.0")))

