(define-module (crates-io hi pp hipparchus-seq) #:use-module (crates-io))

(define-public crate-hipparchus-seq-0.1.2 (c (n "hipparchus-seq") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.4.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "057y5smdyby1vibahk800k4lqzr5sn31lvsxyymy4pg646kmvw03") (r "1.70.0")))

