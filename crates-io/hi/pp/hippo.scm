(define-module (crates-io hi pp hippo) #:use-module (crates-io))

(define-public crate-hippo-0.1.1 (c (n "hippo") (v "0.1.1") (d (list (d (n "hippo-macro") (r "^0.2.0") (d #t) (k 0)) (d (n "hippo-shared") (r "^0.1.0") (d #t) (k 0)))) (h "0amnr2kbsr1gs2llg8a2h6wkflj1h9j6m4n8h31ngcz4hmhf19kc")))

