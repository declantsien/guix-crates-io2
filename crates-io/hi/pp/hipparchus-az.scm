(define-module (crates-io hi pp hipparchus-az) #:use-module (crates-io))

(define-public crate-hipparchus-az-0.1.2 (c (n "hipparchus-az") (v "0.1.2") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hipparchus-mean") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "1zn9afkyyxbryxjsbjhjbnd0brlmsvjgrslw64qw2dclz3jpnvzb") (r "1.70.0")))

