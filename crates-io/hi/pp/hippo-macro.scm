(define-module (crates-io hi pp hippo-macro) #:use-module (crates-io))

(define-public crate-hippo-macro-0.2.1 (c (n "hippo-macro") (v "0.2.1") (d (list (d (n "hippo-shared") (r "^0.1.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "09mq3gljn193np9rin0rcsk6vw4nhhln4ama10yyynzrjv3wsjix")))

