(define-module (crates-io hi pp hippo-shared) #:use-module (crates-io))

(define-public crate-hippo-shared-0.1.1 (c (n "hippo-shared") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "toml") (r "^0.5.0") (d #t) (k 0)))) (h "130p0lfgvhgm4qv39k5vkj9z6abhbflklgyj7r41pa34i9waam34")))

