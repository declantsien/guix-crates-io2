(define-module (crates-io hi pp hippotat-macros) #:use-module (crates-io))

(define-public crate-hippotat-macros-0.0.1 (c (n "hippotat-macros") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "006m1hh3pv2bzw5mvfm41fp3myvafn9g4v2785mdr4dgnzn983g7")))

(define-public crate-hippotat-macros-1.0.0 (c (n "hippotat-macros") (v "1.0.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dsf2k67y5n31ph44s1qz5mscll364mcrm1r4npjxsvw60pswkak")))

(define-public crate-hippotat-macros-1.1.0 (c (n "hippotat-macros") (v "1.1.0") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "131vmfg8a97bg4mk7mg94wlns2bja6dyf8ixv8jwmw92r2d06ka7") (y #t)))

(define-public crate-hippotat-macros-1.1.1 (c (n "hippotat-macros") (v "1.1.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dz8xzd1iqibhcws80dg1fwgm3z5infv2xzqql9zywpaggrzm8qa")))

(define-public crate-hippotat-macros-1.1.3 (c (n "hippotat-macros") (v "1.1.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "101msrjj6jhc0ymg9kxmvc8j0ra00h1brld6g6dib2g746i437m7")))

(define-public crate-hippotat-macros-1.1.6 (c (n "hippotat-macros") (v "1.1.6") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02a3q15xkrdpj3nbn5w9djc1hi7p2fx3r2rxih3vf5ckncpv1749")))

(define-public crate-hippotat-macros-1.1.7 (c (n "hippotat-macros") (v "1.1.7") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "10vqxriw2gwp93a4aqsbn9h49i07fr9ipmp2is6jzay0fg63yfzx")))

(define-public crate-hippotat-macros-1.1.8 (c (n "hippotat-macros") (v "1.1.8") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "05jw11gxfkagynz7wq0828mg1vdcsfhl22fihljc24s5iyjcp53f")))

(define-public crate-hippotat-macros-1.1.9 (c (n "hippotat-macros") (v "1.1.9") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1015xzrspkwgm0c4iryks5abkqsybbg1rgh9il19sa04dfjkg175")))

(define-public crate-hippotat-macros-1.1.10 (c (n "hippotat-macros") (v "1.1.10") (d (list (d (n "itertools") (r ">=0.10.1, <0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0viz1qfs4gsvjqzqbbf81pxbcj3hm2gidx3wil5gm6ss226r4qxf")))

(define-public crate-hippotat-macros-1.1.11 (c (n "hippotat-macros") (v "1.1.11") (d (list (d (n "itertools") (r ">=0.10.1, <0.13") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zj0fkm5vn2kp19r725g1b48m2850x4acnbg3dxxbi9v7wikfyw5")))

