(define-module (crates-io hi pp hippo-openapi) #:use-module (crates-io))

(define-public crate-hippo-openapi-0.1.0 (c (n "hippo-openapi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0ljd3qi0hky96w9v9djfa0mi34az983szg2ghrk4dd3cwn3qzckz")))

(define-public crate-hippo-openapi-0.2.0 (c (n "hippo-openapi") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "02l9p0xwiixkig3ra67bfbcgxvhc984r4rccswl0ybd37jqymqgw")))

(define-public crate-hippo-openapi-0.4.0 (c (n "hippo-openapi") (v "0.4.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1wicr4625wvjd2ljla9xy5lprafy34dyygrni1x1lq6yyjl4xpfc")))

(define-public crate-hippo-openapi-0.5.0 (c (n "hippo-openapi") (v "0.5.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1gvj1z4nla1m4gpp2l6jhiqdd1dnb26w7y7i3bsgb786iz8p17w1")))

(define-public crate-hippo-openapi-0.7.0 (c (n "hippo-openapi") (v "0.7.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "1mvs0wgl8dnwmd3z12wdmhfk4vzpjka1dxw45147kvh8j416wh3s")))

(define-public crate-hippo-openapi-0.8.0 (c (n "hippo-openapi") (v "0.8.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0qrj2yprgj1r16qqh8n92gh1nkm0nlhm16pil91kga80zbsgfv0n")))

(define-public crate-hippo-openapi-0.9.0 (c (n "hippo-openapi") (v "0.9.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "19avfid9ybrv6iq018r7kfwb319xmvwydqp3m2gyfns72q2s7ygd")))

(define-public crate-hippo-openapi-0.9.1 (c (n "hippo-openapi") (v "0.9.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "18bnjlc90bfma3293fgiq3s5ryjkcwxh3xp62k6bqc000iqkf8fq")))

(define-public crate-hippo-openapi-0.10.0 (c (n "hippo-openapi") (v "0.10.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0lgv2imcz0ngb8hdrvnpvscxrlpsg8xlapyrqbvkn8gpc937a4i3")))

(define-public crate-hippo-openapi-0.10.1 (c (n "hippo-openapi") (v "0.10.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0c6iigh1h6mv1cjbsc00v1gi25gagg96y7x6x0krgm7q7wviaay0")))

