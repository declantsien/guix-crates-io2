(define-module (crates-io hi pp hipparchus-metrics) #:use-module (crates-io))

(define-public crate-hipparchus-metrics-0.1.2 (c (n "hipparchus-metrics") (v "0.1.2") (d (list (d (n "float-cmp") (r "^0.9.0") (d #t) (k 0)) (d (n "hipparchus-mean") (r "^0.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "rstest") (r "^0.18.2") (d #t) (k 2)))) (h "0h1hqj45qc2xypw84y0iggaaa7lc1l34cbf0v4nnhars9r67ayz0") (r "1.70.0")))

