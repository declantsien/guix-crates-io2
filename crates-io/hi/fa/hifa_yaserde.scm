(define-module (crates-io hi fa hifa_yaserde) #:use-module (crates-io))

(define-public crate-hifa_yaserde-0.9.2 (c (n "hifa_yaserde") (v "0.9.2") (d (list (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "hifa_yaserde_derive") (r "^0.9.2") (o #t) (d #t) (k 0)) (d (n "hifa_yaserde_derive") (r "^0.9.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0jxkny8p1ny6jgnkj01c4c19m2kxjgd54l3fs7hrnzjcdwvi4h59")))

(define-public crate-hifa_yaserde-0.9.3 (c (n "hifa_yaserde") (v "0.9.3") (d (list (d (n "env_logger") (r "^0.11.0") (d #t) (k 2)) (d (n "hifa_yaserde_derive") (r "^0.9.3") (o #t) (d #t) (k 0)) (d (n "hifa_yaserde_derive") (r "^0.9.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "1s4xvw8lh4fxv8l9hx5za1gmpn46zrl0mr6g52l7pzjws4w90jn8")))

