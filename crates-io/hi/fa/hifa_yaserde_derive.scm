(define-module (crates-io hi fa hifa_yaserde_derive) #:use-module (crates-io))

(define-public crate-hifa_yaserde_derive-0.9.2 (c (n "hifa_yaserde_derive") (v "0.9.2") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "0207fsrinvfxfxk30s13wzgwbw4dd5ndxnzwzcws7lbri0wbf5fa")))

(define-public crate-hifa_yaserde_derive-0.9.3 (c (n "hifa_yaserde_derive") (v "0.9.3") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0") (d #t) (k 0)) (d (n "quote") (r "~1.0") (d #t) (k 0)) (d (n "syn") (r "~1.0") (f (quote ("visit" "extra-traits"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.3") (d #t) (k 0)))) (h "14y7l4fj21bnf5z2lka3r8fj44axg7bn7b3xnm2zmmai4n17jqgz")))

