(define-module (crates-io hi te hitesh_me) #:use-module (crates-io))

(define-public crate-hitesh_me-0.1.0 (c (n "hitesh_me") (v "0.1.0") (h "096cm2x5v97i2mdllgg6r26zmhvnpn2vsm15mdnbg3l4pzc38yfq")))

(define-public crate-hitesh_me-0.1.1 (c (n "hitesh_me") (v "0.1.1") (h "0f8xg3bhb13wvdjmjmhx3v1wqhcbshndch9xrx8yyfxm9n22f3y1")))

(define-public crate-hitesh_me-0.1.2 (c (n "hitesh_me") (v "0.1.2") (d (list (d (n "cargo-readme") (r "^3.3.1") (d #t) (k 2)))) (h "1l5sxq2kxmf2p884fhniqsghnmib13xlcikg1lg72mgzhs2ql6w4")))

