(define-module (crates-io hi -d hi-doc) #:use-module (crates-io))

(define-public crate-hi-doc-0.1.0 (c (n "hi-doc") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "random_color") (r "^0.8.0") (d #t) (k 0)) (d (n "range-map") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.13.1") (d #t) (k 0)))) (h "1z509pdsdhvhvp84fn4wnxy4cxw8qbwm8kgryrxh5vi161zn333l")))

(define-public crate-hi-doc-0.1.1 (c (n "hi-doc") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "random_color") (r "^0.8.0") (d #t) (k 0)) (d (n "range-map") (r "^0.2.0") (d #t) (k 0)) (d (n "smallvec") (r "^1.13.2") (d #t) (k 0)))) (h "01p0nh3qrw9lf1zivxhdfbhkgyivmlxq6mwmxxl02dz1kc60lffj")))

