(define-module (crates-io hi -j hi-jira2) #:use-module (crates-io))

(define-public crate-hi-jira2-0.0.1 (c (n "hi-jira2") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "0zjmqc0mfqqryknjw73n3hkgsq2sagn3hmlw7p0lc4aq3ridfblg")))

