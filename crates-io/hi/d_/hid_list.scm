(define-module (crates-io hi d_ hid_list) #:use-module (crates-io))

(define-public crate-hid_list-0.1.0 (c (n "hid_list") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("hidclass" "debug" "hidsdi" "setupapi" "impl-default" "fileapi" "winnt" "winbase" "handleapi" "hidpi"))) (d #t) (k 0)))) (h "09mss38mk6lzxsafsq15m0j12bh6hakdfzzzvlkqxid6bxihqgry")))

(define-public crate-hid_list-0.1.1 (c (n "hid_list") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("hidclass" "debug" "hidsdi" "setupapi" "impl-default" "fileapi" "winnt" "winbase" "handleapi" "hidpi"))) (d #t) (k 0)))) (h "01fy8ls8x9lxnnyh3drvqrsrgbppg0i2rkwzfai2vgwp53bzh6iv")))

(define-public crate-hid_list-0.1.2 (c (n "hid_list") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "winapi") (r "^0.3.8") (f (quote ("debug" "fileapi" "hidclass" "hidsdi" "handleapi" "hidpi" "impl-default" "setupapi" "winnt" "winbase"))) (d #t) (k 0)))) (h "1iv5bzg856dz12kp3slzqwvngs3sqrbbi24cfs1phz9f657pakd6")))

