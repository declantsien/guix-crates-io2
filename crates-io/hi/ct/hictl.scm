(define-module (crates-io hi ct hictl) #:use-module (crates-io))

(define-public crate-hictl-0.1.0 (c (n "hictl") (v "0.1.0") (d (list (d (n "hilib") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("macros" "rt-multi-thread" "full"))) (d #t) (k 0)))) (h "1l2rc60wai9mh1dm3qch8kwj8wk8nh0bqp3nc5hyz4smxsssy0vp") (y #t)))

