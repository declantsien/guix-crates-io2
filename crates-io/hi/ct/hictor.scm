(define-module (crates-io hi ct hictor) #:use-module (crates-io))

(define-public crate-hictor-0.1.0 (c (n "hictor") (v "0.1.0") (h "09709lc12yprmdfy6s64hnb1hqwi3f7400h0f6677754p36amgzi")))

(define-public crate-hictor-0.1.1 (c (n "hictor") (v "0.1.1") (h "1l9ji23bkh48jn0y19k65xa1xskcyap0q8mvdx7fwb9l19bp6xbg")))

(define-public crate-hictor-0.1.2 (c (n "hictor") (v "0.1.2") (h "1rwz3vz6gwywf3h47x05a8l4f33swls8rlbyvyyri2jiv33v6ivi")))

(define-public crate-hictor-0.1.3 (c (n "hictor") (v "0.1.3") (h "0d5z80bbkybznvqjf28pw30baqzcg6hrzaxji895pfh1pl85n5na")))

(define-public crate-hictor-0.1.4 (c (n "hictor") (v "0.1.4") (h "126namz8pm5xv6dlg65wqn6yvzf1l8r2zs0734lbnsv1vbbwl2sj")))

(define-public crate-hictor-0.1.5 (c (n "hictor") (v "0.1.5") (h "0f42ld00np5v8rkzqi2nwzg86jb95ms13yws85bgqja9kxlgqsbw")))

(define-public crate-hictor-0.1.6 (c (n "hictor") (v "0.1.6") (h "0xz8b9zar38mzwrzyp90dxx3k4nsiyf6w4nnqkmp53nr3azr8irl")))

