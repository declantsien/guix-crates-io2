(define-module (crates-io hi na hinawa) #:use-module (crates-io))

(define-public crate-hinawa-0.0.90 (c (n "hinawa") (v "0.0.90") (d (list (d (n "ffi") (r "^0.0") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qpjpngh9spz6s6wrn1fmb4s49mg5mfd2273ih82ciygvkszg06b") (y #t)))

(define-public crate-hinawa-0.0.91 (c (n "hinawa") (v "0.0.91") (d (list (d (n "ffi") (r "^0.0.91") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1w0xwaxkdxa0lvs3x7983m9mp2vfc6zw5lsjfdza8lalw3sk68fq") (f (quote (("dox" "ffi/dox" "glib/dox")))) (y #t)))

(define-public crate-hinawa-0.7.0 (c (n "hinawa") (v "0.7.0") (d (list (d (n "ffi") (r "^0.7.0") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13lmi1wzcgrqm3dx0dfv70wwds0czjyaz4j84dpxjb7wz1fih8gx") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-hinawa-0.8.0 (c (n "hinawa") (v "0.8.0") (d (list (d (n "ffi") (r "^0.8.0") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18gc7z452vis8q3vsiirmw961wsgik9iyv914m7ivkyz54ac48qp") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-hinawa-0.8.1 (c (n "hinawa") (v "0.8.1") (d (list (d (n "ffi") (r "^0.8.1") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "023n8mv52b4haxkz9c41zg299q0fd77aqkvv2syr8xc00m564sv4") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-hinawa-0.7.1 (c (n "hinawa") (v "0.7.1") (d (list (d (n "ffi") (r "^0.7.1") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05nkkgr2mva9g77247xxnscfjig2iln03ms7f78a8349xavhwdv7") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-hinawa-0.9.0 (c (n "hinawa") (v "0.9.0") (d (list (d (n "ffi") (r "^0.9.0") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05cqg727fnhvnj6rzn6rw5bjjcmy870vbk3fi2w77ybnwfvfqqap") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-hinawa-0.9.1 (c (n "hinawa") (v "0.9.1") (d (list (d (n "ffi") (r "^0.9.1") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0d4zr9r081fj43ysl6qvahm21azsr12m3ml7j37d13jayv7yqy4f") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-hinawa-0.0.92 (c (n "hinawa") (v "0.0.92") (d (list (d (n "ffi") (r "^0.0.92") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "046bh9cr75alf3arai6ip7w7f4a6zar93ix86i51qmffkxvv0jyk") (y #t)))

(define-public crate-hinawa-0.10.0 (c (n "hinawa") (v "0.10.0") (d (list (d (n "ffi") (r "^0.10.0") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c4z0c9313zmqbjm2vwvymvx3dfvimq99w9s14g8awilq7q7h84a")))

(define-public crate-hinawa-0.11.0 (c (n "hinawa") (v "0.11.0") (d (list (d (n "ffi") (r "^0.11.0") (d #t) (k 0) (p "hinawa-sys")) (d (n "gir-format-check") (r "^0.1") (d #t) (k 2)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fkfrplix5z6g9w7gb15i3pr279nxxxf2i0j5r0fya1brf53sjk8")))

