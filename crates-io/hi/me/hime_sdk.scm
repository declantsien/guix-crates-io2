(define-module (crates-io hi me hime_sdk) #:use-module (crates-io))

(define-public crate-hime_sdk-4.0.0 (c (n "hime_sdk") (v "4.0.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "hime_redist") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1n3l98qvjl5mdb0p5qqfxf44hrq51wj487fna9bjxjvf5fyyr1q3") (f (quote (("print_errors" "ansi_term") ("default"))))))

(define-public crate-hime_sdk-4.1.0 (c (n "hime_sdk") (v "4.1.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "hime_redist") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1kymy1na8pi5zds1qbvw93m89pbs25xxf3i55268p9ndqmf6iz9a") (f (quote (("print_errors" "ansi_term") ("default"))))))

(define-public crate-hime_sdk-4.2.0 (c (n "hime_sdk") (v "4.2.0") (d (list (d (n "ansi_term") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "hime_redist") (r "^4.0.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "19db00lbhjyim2xg7mj6b2dl11mnksc9s5h9r5h28x767xk07m83") (f (quote (("print_errors" "ansi_term") ("default"))))))

(define-public crate-hime_sdk-4.2.2 (c (n "hime_sdk") (v "4.2.2") (d (list (d (n "hime_redist") (r "^4.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "miette") (r "^5.6") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "11ly5vmsrmzvf41h3si2gnik60fvdx6cxjc62gc97lk6cz9i47fn") (f (quote (("print_errors" "miette") ("default"))))))

(define-public crate-hime_sdk-4.2.4 (c (n "hime_sdk") (v "4.2.4") (d (list (d (n "hime_redist") (r "^4.2.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "miette") (r "^5.6") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1cj0j0n84sh3brfpj965av3368sph7v2z5nw7pk46qq6w4dr77v6") (f (quote (("print_errors" "miette") ("default"))))))

(define-public crate-hime_sdk-4.3.0 (c (n "hime_sdk") (v "4.3.0") (d (list (d (n "hime_redist") (r "^4.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "miette") (r "^5.6") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "0dppxwjn088qmdc7mxb649i2g8laan4458i8wmvw54hri3b0y5zl") (f (quote (("print_errors" "miette") ("default"))))))

