(define-module (crates-io hi me himetake) #:use-module (crates-io))

(define-public crate-himetake-0.1.0-alpha1 (c (n "himetake") (v "0.1.0-alpha1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 0)) (d (n "kusabira") (r "^0.1.0-alpha0") (d #t) (k 0)) (d (n "kusabira") (r "^0.1.0-alpha0") (d #t) (k 1)) (d (n "system-deps") (r "^6.1") (d #t) (k 1)))) (h "0cpbfsnvy5irv9b6lh7qlrc7swvkg74n6az97jpf59rpv6k3kc1f")))

(define-public crate-himetake-0.1.0 (c (n "himetake") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 0)) (d (n "kusabira") (r "^0.1") (d #t) (k 0)) (d (n "kusabira") (r "^0.1") (d #t) (k 1)) (d (n "system-deps") (r "^6.1") (d #t) (k 1)))) (h "1pi2h9lb18jw6nb13misa61pdqzsyidx7bzlcnv3k19gabdrm4cm")))

(define-public crate-himetake-0.1.1 (c (n "himetake") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 0)) (d (n "kusabira") (r "^0.1") (d #t) (k 0)) (d (n "kusabira") (r "^0.1") (d #t) (k 1)) (d (n "system-deps") (r "^6.1") (d #t) (k 1)))) (h "0lafd3x7ih2gsxl48y105l2ml58kwcfzaywxszv3cnppqps1zic7")))

