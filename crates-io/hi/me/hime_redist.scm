(define-module (crates-io hi me hime_redist) #:use-module (crates-io))

(define-public crate-hime_redist-3.3.0 (c (n "hime_redist") (v "3.3.0") (h "19rma7mkix5rmxgizbcqsrb446j60bd39z5xkifyj0njalx8r7sg")))

(define-public crate-hime_redist-3.3.1 (c (n "hime_redist") (v "3.3.1") (h "0nvywwh8rzk2ckihbi2g50zbv9fqacv6l72afklmgmx4ysnqkc19")))

(define-public crate-hime_redist-3.3.2 (c (n "hime_redist") (v "3.3.2") (h "0m89s5bx77qshk1k8fg6f03mm9hzy535hp840wqr22cy18l0qzcb")))

(define-public crate-hime_redist-3.4.0 (c (n "hime_redist") (v "3.4.0") (h "1a76k8dd11ar9mzri4lzbv19brbjk13z5pyp2hy1j3ybh5s67ayz")))

(define-public crate-hime_redist-3.4.1 (c (n "hime_redist") (v "3.4.1") (h "15sbnrhah7piwm53s6962xxzcpwjz8884bmsqpch9q30shdps00b")))

(define-public crate-hime_redist-3.5.0 (c (n "hime_redist") (v "3.5.0") (h "17w7h5fbqx4jvfqzwi7j0xrvm2fwixcwfcfflwkl9wk5jjzikwlh")))

(define-public crate-hime_redist-3.5.1 (c (n "hime_redist") (v "3.5.1") (h "17fqm35kl74ymwkx7llmhc5jbkpvq8aw9yk7igzd98krxxcfnylq")))

(define-public crate-hime_redist-4.0.0 (c (n "hime_redist") (v "4.0.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1x8mdknwz4xzcrg0pibr45qsg28n8pnr91sfy84h281ki4wv74yc")))

(define-public crate-hime_redist-4.1.1 (c (n "hime_redist") (v "4.1.1") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "06v82wm0b8hxkb1bh059za3y26r8g4pkhli8k5a1vf34apm1r2ln") (y #t)))

(define-public crate-hime_redist-4.1.0 (c (n "hime_redist") (v "4.1.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0ii93fd71whcv86dlc446mdycmdhlvp9h4hxcsy6h18f7kbpwiaa")))

(define-public crate-hime_redist-4.1.2 (c (n "hime_redist") (v "4.1.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1kkiqc4bkh82j0sf1sxxawdmxqb8mccpag2x0ybywczam0jzc5wr")))

(define-public crate-hime_redist-4.2.0 (c (n "hime_redist") (v "4.2.0") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "09k847vlgwhnnzrjb5bycs62vvjc4ly48zwgns7idcm07igvrkhv")))

(define-public crate-hime_redist-4.2.2 (c (n "hime_redist") (v "4.2.2") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1bg700jnb2mzvq3w0k4458803fvs88k47hrhdxv9kdlprmdpi4hb")))

(define-public crate-hime_redist-4.2.3 (c (n "hime_redist") (v "4.2.3") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1zbyvmkp3cfxxd2yjdbjbgw4h5p1447h1l9h13km47jjrzb0lns9")))

(define-public crate-hime_redist-4.2.4 (c (n "hime_redist") (v "4.2.4") (d (list (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "1j55bmibb1zpcb2r9vqxpcga94msx38sixx24z2jf898qgybvrpx") (f (quote (("default") ("debug"))))))

(define-public crate-hime_redist-4.3.0 (c (n "hime_redist") (v "4.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive" "alloc"))) (k 0)))) (h "05gaawdvnmdp9x5lw6rsfby510jqlaa7rfr5i3vswgiwf4gj2crf") (f (quote (("std" "serde/std") ("default" "std") ("debug"))))))

