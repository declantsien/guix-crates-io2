(define-module (crates-io hi fi hifive1) #:use-module (crates-io))

(define-public crate-hifive1-0.3.0 (c (n "hifive1") (v "0.3.0") (d (list (d (n "e310x-hal") (r "^0.3.0") (d #t) (k 0)) (d (n "riscv") (r "^0.4.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.4.0") (d #t) (k 0)))) (h "1q4wdqhgy1nc94mlwcwd9q5ipgrr929c6gx6an3gzshfv0242y62") (f (quote (("board-lofive") ("board-hifive1"))))))

(define-public crate-hifive1-0.4.0 (c (n "hifive1") (v "0.4.0") (d (list (d (n "e310x-hal") (r "^0.4.0") (d #t) (k 0)))) (h "1xm3yqzgpb9navz4sdr7cb6jfbfavix9cc2yccinwkrmyism99cq") (f (quote (("board-lofive") ("board-hifive1"))))))

(define-public crate-hifive1-0.5.0 (c (n "hifive1") (v "0.5.0") (d (list (d (n "e310x-hal") (r "^0.5.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)))) (h "11n2w8ngvg0vy3xd1f2xjmw26s35abjaldp4ypy447pxd94fnvl6") (f (quote (("board-lofive") ("board-hifive1-revb" "e310x-hal/g002") ("board-hifive1"))))))

(define-public crate-hifive1-0.6.0 (c (n "hifive1") (v "0.6.0") (d (list (d (n "e310x-hal") (r "^0.6.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.2") (d #t) (k 0)))) (h "1lb8glncf8cm4hz97rbg4j5bc2vxzskwci6snf8ywh70x7bcs326") (f (quote (("board-lofive") ("board-hifive1-revb" "e310x-hal/g002") ("board-hifive1"))))))

(define-public crate-hifive1-0.7.0 (c (n "hifive1") (v "0.7.0") (d (list (d (n "e310x-hal") (r "^0.7.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.2") (d #t) (k 0)))) (h "13vvsmahv58vv2y31d2g7pqfxzx1gs948c6x9m40xkcpy3zzw73b") (f (quote (("board-lofive") ("board-hifive1-revb" "e310x-hal/g002") ("board-hifive1"))))))

(define-public crate-hifive1-0.8.0 (c (n "hifive1") (v "0.8.0") (d (list (d (n "e310x-hal") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)))) (h "01masqd0h53ny4vp6cqq3y7gp25l1i23s2m1bk1l8snf9lmvj24l") (f (quote (("board-lofive") ("board-hifive1-revb" "e310x-hal/g002") ("board-hifive1"))))))

(define-public crate-hifive1-0.8.1 (c (n "hifive1") (v "0.8.1") (d (list (d (n "e310x-hal") (r "^0.8.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)))) (h "0074drga5ing50c1gfh9lxk84jnpa90gb6bh6k8wfcjj634gacb4") (f (quote (("board-lofive-r1" "e310x-hal/g002") ("board-lofive") ("board-hifive1-revb" "e310x-hal/g002") ("board-hifive1"))))))

(define-public crate-hifive1-0.9.0 (c (n "hifive1") (v "0.9.0") (d (list (d (n "e310x-hal") (r "^0.9.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)))) (h "11qp3qcqdzpd3xjpn10c1369k3jyyml5j4gndlbymc4k7ilwszlr") (f (quote (("board-lofive-r1" "e310x-hal/g002") ("board-lofive") ("board-hifive1-revb" "e310x-hal/g002") ("board-hifive1"))))))

(define-public crate-hifive1-0.9.1 (c (n "hifive1") (v "0.9.1") (d (list (d (n "e310x-hal") (r "^0.9.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.2") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)))) (h "1v7gz37n9068flkyfwkbl4bpwmhasjjji2lpbnw4pjzxqwxa23w4") (f (quote (("board-lofive-r1" "e310x-hal/g002") ("board-lofive") ("board-hifive1-revb" "e310x-hal/g002") ("board-hifive1"))))))

(define-public crate-hifive1-0.10.0 (c (n "hifive1") (v "0.10.0") (d (list (d (n "e310x-hal") (r "^0.9.1") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.6.0") (d #t) (k 0)))) (h "0c47n9iiz6a04vkfrsbqsx2kbabkhl0a8vcb1hdqmm1r1w56cgwz") (f (quote (("board-redv" "e310x-hal/g002") ("board-lofive-r1" "e310x-hal/g002") ("board-lofive") ("board-hifive1-revb" "e310x-hal/g002") ("board-hifive1"))))))

(define-public crate-hifive1-0.11.0 (c (n "hifive1") (v "0.11.0") (d (list (d (n "e310x-hal") (r "^0.9.3") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "1x724lz549jblf45kda4hljz44r8y0c1cls4d0983h867krqimbf") (f (quote (("board-redv" "e310x-hal/g002") ("board-lofive-r1" "e310x-hal/g002") ("board-lofive") ("board-hifive1-revb" "e310x-hal/g002") ("board-hifive1")))) (r "1.59")))

(define-public crate-hifive1-0.12.0 (c (n "hifive1") (v "0.12.0") (d (list (d (n "e310x-hal") (r "^0.11.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.10.1") (d #t) (k 0)))) (h "16hff8rn2sp3zyw1s8rx2ia3idrxzx5fcndv0pqnybf54n9dp5mz") (f (quote (("board-redv" "e310x-hal/g002") ("board-lofive-r1" "e310x-hal/g002") ("board-lofive") ("board-hifive1-revb" "e310x-hal/g002") ("board-hifive1")))) (r "1.59")))

