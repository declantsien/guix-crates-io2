(define-module (crates-io hi fi hifijson) #:use-module (crates-io))

(define-public crate-hifijson-0.1.0 (c (n "hifijson") (v "0.1.0") (d (list (d (n "memmap2") (r "^0.5.8") (d #t) (k 2)))) (h "1c0z2s80lgrrkxh2yswmbg17smdkn1lpda4ls3yk9n3516dyh2r9") (f (quote (("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-hifijson-0.1.1 (c (n "hifijson") (v "0.1.1") (d (list (d (n "memmap2") (r "^0.5.8") (d #t) (k 2)))) (h "1b4g6x883ibw5iri3psky75braz0c6lmnf3vg71zz9jq0ay7rj58") (f (quote (("std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-hifijson-0.2.0 (c (n "hifijson") (v "0.2.0") (d (list (d (n "memmap2") (r "^0.5.8") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)))) (h "0jpgrmxpf2884sr75wxawwbwscxn35d14nda98mdvrikqd0npvw5") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.56")))

(define-public crate-hifijson-0.2.1 (c (n "hifijson") (v "0.2.1") (d (list (d (n "memmap2") (r "^0.9") (d #t) (k 2)) (d (n "serde") (r "^1.0.152") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.92") (f (quote ("arbitrary_precision"))) (d #t) (k 2)))) (h "0347da7gzgsdr7divxbp01m6n1wrpjmjivj9947czzjdrf5ldbhq") (f (quote (("std") ("default" "std" "alloc") ("alloc")))) (r "1.56")))

