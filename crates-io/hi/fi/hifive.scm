(define-module (crates-io hi fi hifive) #:use-module (crates-io))

(define-public crate-hifive-0.2.0 (c (n "hifive") (v "0.2.0") (d (list (d (n "e310x-hal") (r "^0.2.0") (f (quote ("pll" "hfxosc" "lfaltclk"))) (d #t) (k 0)) (d (n "riscv") (r "^0.3.0") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.3.0") (d #t) (k 0)))) (h "11f974jy7rd3q36k799afnl9zrrpg4l0c291m8dc3rxxnm3kd8rx")))

