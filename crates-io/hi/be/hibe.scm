(define-module (crates-io hi be hibe) #:use-module (crates-io))

(define-public crate-hibe-1.0.0 (c (n "hibe") (v "1.0.0") (d (list (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)))) (h "1hwqw4w02lx5zp3sdp2di1bhd7v9n9i5xd7qbdass4c2r3ygzkhg")))

(define-public crate-hibe-1.2.1 (c (n "hibe") (v "1.2.1") (d (list (d (n "clap") (r "^4.0.18") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_logger") (r "^4.0.0") (d #t) (k 0)))) (h "1b47dhpzl5b6ddz59pn6qcxdgdja986j79y953ay6dbmxhhar1xn")))

