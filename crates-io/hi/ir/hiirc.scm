(define-module (crates-io hi ir hiirc) #:use-module (crates-io))

(define-public crate-hiirc-0.1.0 (c (n "hiirc") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "loirc") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 2)))) (h "0w4cbaps9lcpvmd5ajhkp3mvh9mzp83622w55is3hl84jgzr9l19")))

(define-public crate-hiirc-0.2.0 (c (n "hiirc") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "loirc") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 2)))) (h "05cq0dl5zxrf8m7rsqn1rql2fbh98mrbf1nvcgfnjg3i2q9apxz8")))

(define-public crate-hiirc-0.2.1 (c (n "hiirc") (v "0.2.1") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "loirc") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 2)))) (h "00s5krsdb2c879km82i52iv01nacakizf5wjfah201jf0pihvqlx")))

(define-public crate-hiirc-0.3.0 (c (n "hiirc") (v "0.3.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "loirc") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 2)))) (h "1drmiajb28npmldgjqsdq046g0s297yzqpjk1487a95qj3alxkfx")))

(define-public crate-hiirc-0.4.0 (c (n "hiirc") (v "0.4.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "loirc") (r "^0.1.4") (d #t) (k 0)) (d (n "time") (r "^0.1.32") (d #t) (k 2)))) (h "1sfah7nbpyhiiyqn17v6ql90im3r2p2x0z2r5bb7lc2src2d2mv9")))

(define-public crate-hiirc-0.4.1 (c (n "hiirc") (v "0.4.1") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "loirc") (r "^0.2") (d #t) (k 0)))) (h "0qaccbg24xgksdn127bfxlkhj8020n5awz2ila9rmyknggdpfkqv")))

(define-public crate-hiirc-0.5.0 (c (n "hiirc") (v "0.5.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "loirc") (r "^0.2") (d #t) (k 0)))) (h "144ajw77nkz9k6imp0076svx961cwz99rq01qijr2d6mba1sgj1n")))

