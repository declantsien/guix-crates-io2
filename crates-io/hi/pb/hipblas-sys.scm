(define-module (crates-io hi pb hipblas-sys) #:use-module (crates-io))

(define-public crate-hipblas-sys-0.1.0 (c (n "hipblas-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.74") (d #t) (k 0)))) (h "160wlk4wxh29s89gviyis15vbwjng50hz15yx1k3vfsbxlv69f1p") (l "hipblas")))

