(define-module (crates-io hi dr hidraw-sys) #:use-module (crates-io))

(define-public crate-hidraw-sys-0.0.1 (c (n "hidraw-sys") (v "0.0.1") (d (list (d (n "ioctl-macro") (r "^0.0.1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4.0") (d #t) (k 1)))) (h "1w1cb039ssmrrnwgp5ngw0w0bmjw5abzibrx5pw4xfiniv2mw0pd") (f (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default"))))))

(define-public crate-hidraw-sys-0.0.2 (c (n "hidraw-sys") (v "0.0.2") (d (list (d (n "ioctl-macro") (r "^0.0.2") (d #t) (k 0)))) (h "0m4bxgy3cdcl645094gab9c29agwryjd3m99xiwvav5bpx3awhzc") (f (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default")))) (r "1.64.0")))

(define-public crate-hidraw-sys-0.0.3 (c (n "hidraw-sys") (v "0.0.3") (d (list (d (n "ioctl-macro") (r "^0.0.2") (d #t) (k 0)))) (h "0nb060i0h0rlpcx7x96kc1kn6k38prp2sbh3gi3dl5h1c35sjras") (f (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default")))) (r "1.64.0")))

(define-public crate-hidraw-sys-0.0.4 (c (n "hidraw-sys") (v "0.0.4") (d (list (d (n "ioctl-macro") (r "^0.0.2") (d #t) (k 0)))) (h "0k4b93az8qwg63yw49p5qk2lrg48dxmrd5farwdyzikammm880nk") (f (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default")))) (r "1.64.0")))

(define-public crate-hidraw-sys-0.0.5 (c (n "hidraw-sys") (v "0.0.5") (d (list (d (n "ioctl-macro") (r "^0.0.2") (d #t) (k 0)))) (h "1sbx10dxmi7lpiww1apj73005vkm50b4wdn3ixdkvgl00q1mx0c9") (f (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default")))) (y #t) (r "1.64.0")))

(define-public crate-hidraw-sys-0.0.6 (c (n "hidraw-sys") (v "0.0.6") (d (list (d (n "ioctl-macro") (r "^0.0.2") (d #t) (k 0)))) (h "0abm8y0wdwzlj3gfy06k96xrpbrw7c54b40z5bk194rpkfgbmmpf") (f (quote (("wrappers" "ioctl-macro/extern_fn") ("extra_traits") ("default")))) (r "1.64.0")))

