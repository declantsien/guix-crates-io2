(define-module (crates-io hi dr hidraw) #:use-module (crates-io))

(define-public crate-hidraw-0.0.1 (c (n "hidraw") (v "0.0.1") (d (list (d (n "ctrlc") (r "^3") (d #t) (k 2)) (d (n "hidraw-sys") (r "^0.0.1") (f (quote ("extra_traits" "wrappers"))) (d #t) (k 0)))) (h "1qi8mfpyhdwbcn5yh80sqv1r59cnjm1gmz8lp2880as4k2hag4s4")))

(define-public crate-hidraw-0.0.2 (c (n "hidraw") (v "0.0.2") (d (list (d (n "hidraw-sys") (r "^0.0.2") (f (quote ("extra_traits" "wrappers"))) (d #t) (k 0)))) (h "0gm3qbhvb110nqgr4x09p8n0lyrinc7bn2zz57kbx7v01nw63lzy") (r "1.64.0")))

(define-public crate-hidraw-0.0.3 (c (n "hidraw") (v "0.0.3") (d (list (d (n "hidraw-sys") (r "^0.0.3") (f (quote ("extra_traits" "wrappers"))) (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "0x7hb61hk72cnk2zz51ribz8gmsi7y8lscgk5qzh1kc02nrkb3bx") (f (quote (("unsafe_ioctl") ("default")))) (r "1.64.0")))

(define-public crate-hidraw-0.0.4 (c (n "hidraw") (v "0.0.4") (d (list (d (n "hidraw-sys") (r "^0.0.3") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1yp2c3sg1h6l7bx814zq4j81b5p8vxhyppzadcibysrx31px1si6") (f (quote (("unsafe_reports") ("default")))) (r "1.64.0")))

(define-public crate-hidraw-0.0.5 (c (n "hidraw") (v "0.0.5") (d (list (d (n "hidraw-sys") (r "^0.0.5") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1b3726d86c5cmp5842dxidcirza8x44ypm5hqalax4rd90cmasap") (f (quote (("unsafe_reports") ("default")))) (y #t) (r "1.64.0")))

(define-public crate-hidraw-0.0.6 (c (n "hidraw") (v "0.0.6") (d (list (d (n "hidraw-sys") (r "^0.0.6") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "1b44f098az5b4na2qhjqlhjwk276h7s9aa7pg3gk07cdzc017vrm") (f (quote (("unsafe_reports") ("default")))) (y #t) (r "1.64.0")))

(define-public crate-hidraw-0.0.7 (c (n "hidraw") (v "0.0.7") (d (list (d (n "hidraw-sys") (r "^0.0.6") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)))) (h "11x57calzswh6z96kwzvqj1fh6sqsdp8d1r3b3bv7gcrnkqv67cv") (f (quote (("unsafe_reports") ("default")))) (r "1.64.0")))

