(define-module (crates-io hi dr hidreport) #:use-module (crates-io))

(define-public crate-hidreport-0.1.0 (c (n "hidreport") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0zwvfk9b8qvkj8z6ikbrnd1j8bplxlgqw7bgpygqr3kbnkqpv72c")))

(define-public crate-hidreport-0.2.1 (c (n "hidreport") (v "0.2.1") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0k58qcsm1f8xbrpb1ly3qnjnfgjb5d19m7lkysjmg69c5gviknv0")))

(define-public crate-hidreport-0.3.0 (c (n "hidreport") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1s9gw2168vz7dlyhp5ppgzqwc148fpg0ji9cpx3z9pnaf1qbai18")))

