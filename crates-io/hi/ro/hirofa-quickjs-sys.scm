(define-module (crates-io hi ro hirofa-quickjs-sys) #:use-module (crates-io))

(define-public crate-hirofa-quickjs-sys-0.2.0 (c (n "hirofa-quickjs-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "17wd20wminnaivrhii2pawc5wg5qcc8f7qir63lkvv554lbnkwbj") (f (quote (("default" "bellard") ("bellard"))))))

(define-public crate-hirofa-quickjs-sys-0.3.0 (c (n "hirofa-quickjs-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "1110m9ppcnghspgblwa7s17kbg1c0xiba9g680qnhkihmm0xmqjn") (f (quote (("quickjs-ng") ("default" "bellard") ("bellard"))))))

(define-public crate-hirofa-quickjs-sys-0.4.0 (c (n "hirofa-quickjs-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.66") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "0ydpdp24h6w78ivc3lfm77n3sq7nyp8paj745ikdl494z81c5ny0") (f (quote (("quickjs-ng") ("default" "quickjs-ng") ("bellard"))))))

