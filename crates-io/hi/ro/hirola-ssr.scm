(define-module (crates-io hi ro hirola-ssr) #:use-module (crates-io))

(define-public crate-hirola-ssr-0.4.0-beta.0 (c (n "hirola-ssr") (v "0.4.0-beta.0") (d (list (d (n "hirola-core") (r "^0.4.0-beta.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.39") (d #t) (k 2)))) (h "0sdl8n3xwbjwn77m1i80wky42z40zml9h2p6xgmjqykci7h273y1")))

(define-public crate-hirola-ssr-0.4.0 (c (n "hirola-ssr") (v "0.4.0") (d (list (d (n "hirola-core") (r "^0.4.0") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.39") (d #t) (k 2)))) (h "1ymiwjd6q669r7cj159rszkcgh6nbdvchkqj9gjgcpbbz6917nvd")))

