(define-module (crates-io hi vm hivm2) #:use-module (crates-io))

(define-public crate-hivm2-0.0.1 (c (n "hivm2") (v "0.0.1") (d (list (d (n "nom") (r "~1.0.0") (d #t) (k 0)))) (h "0bnbq8v2faxnvvi8jd0cm1sdpn6x6vfhrjx88aybpa0s6fig4gcc")))

(define-public crate-hivm2-0.0.2 (c (n "hivm2") (v "0.0.2") (d (list (d (n "nom") (r "~1.0.0") (d #t) (k 0)))) (h "0b4lidqdxxlmmhcc7v3jbb7qsvmya3w55nf09x1j6s6m9mgi47xj")))

