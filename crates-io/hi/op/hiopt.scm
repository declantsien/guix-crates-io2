(define-module (crates-io hi op hiopt) #:use-module (crates-io))

(define-public crate-hiopt-0.1.0 (c (n "hiopt") (v "0.1.0") (d (list (d (n "hictor") (r "^0.1") (d #t) (k 2)))) (h "0psw0vp58s2xiiwvk73ab123hqi47zlq8zkv5k4gh6rwlpr3a39i")))

(define-public crate-hiopt-0.1.1 (c (n "hiopt") (v "0.1.1") (d (list (d (n "hictor") (r "^0.1") (d #t) (k 2)))) (h "06vjndixsb8znp919rbji4nvg6j0dc2sn4d0pxqbfz2rdcik3sbg")))

(define-public crate-hiopt-0.1.2 (c (n "hiopt") (v "0.1.2") (d (list (d (n "hictor") (r "^0.1") (d #t) (k 0)) (d (n "hictor") (r "^0.1") (d #t) (k 2)))) (h "171r8g7fdm8l87xm8nms97xjcjpzyfb8rg3cd6s9k8002hqv7djc")))

(define-public crate-hiopt-0.1.3 (c (n "hiopt") (v "0.1.3") (d (list (d (n "hictor") (r "^0.1") (d #t) (k 0)) (d (n "hictor") (r "^0.1") (d #t) (k 2)))) (h "09zf9a3xvcwmfgy4zkcmx6dc25y48hpkjxhy9zw0616yqji401ai")))

(define-public crate-hiopt-0.1.4 (c (n "hiopt") (v "0.1.4") (d (list (d (n "hictor") (r "^0.1") (d #t) (k 0)) (d (n "hictor") (r "^0.1") (d #t) (k 2)))) (h "12wva41fmfjhl7zzm4yx5gg8pgn2ihxyamhpn6nf6n9f18bl337v")))

(define-public crate-hiopt-0.1.5 (c (n "hiopt") (v "0.1.5") (d (list (d (n "hictor") (r "^0.1") (d #t) (k 0)) (d (n "hictor") (r "^0.1") (d #t) (k 2)))) (h "16jb7brqzd38v5j4na6h1nc2cpvpm5y84wzg4mfv3cgjfddga5fc")))

(define-public crate-hiopt-0.1.6 (c (n "hiopt") (v "0.1.6") (d (list (d (n "hictor") (r "^0.1") (d #t) (k 0)) (d (n "hictor") (r "^0.1") (d #t) (k 2)))) (h "0km5ndfh3lv60k50j5fpbpc7nr1kfb724n8s4hx1hhfrkrrffrqv")))

(define-public crate-hiopt-0.1.7 (c (n "hiopt") (v "0.1.7") (d (list (d (n "hictor") (r "^0.1") (d #t) (k 0)))) (h "1qvmk7jm7cymm6b5zhs848l9aci74ggdyrm538g7kzkbjsicyjif")))

