(define-module (crates-io hi d- hid-and-seek) #:use-module (crates-io))

(define-public crate-hid-and-seek-0.1.0 (c (n "hid-and-seek") (v "0.1.0") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1nyknsa2jmln8z85af25p9hm9wps3ghhqx588ygxrg7fj1nh4dmm") (y #t)))

(define-public crate-hid-and-seek-0.1.1 (c (n "hid-and-seek") (v "0.1.1") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1lyz0wf7n6sp7w4ynxsr9z98bg4zgvgg6w69sxv3m0h67grgw2xx") (y #t)))

(define-public crate-hid-and-seek-0.1.2 (c (n "hid-and-seek") (v "0.1.2") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "10wslwlfl31cr4d8cdwy8vsa2zbx14k16dlmpd54wwz5zr5ivr30") (y #t)))

(define-public crate-hid-and-seek-0.1.3 (c (n "hid-and-seek") (v "0.1.3") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0fr58prafyafbbvf2x0kncz1497kr0s01hbb3vjfl7gsqmlxp7sw") (y #t)))

(define-public crate-hid-and-seek-0.1.4 (c (n "hid-and-seek") (v "0.1.4") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "06yj2fdgicmnp4nw2l6rlg0dvm23fs6w15p5ir1q5dyripq4g1gd") (y #t)))

(define-public crate-hid-and-seek-0.1.5 (c (n "hid-and-seek") (v "0.1.5") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "1j0s7xq5g3a8idx82andai2mqdhg8akylk7fb0xwb8cggcyjd0ia") (y #t)))

(define-public crate-hid-and-seek-0.1.6 (c (n "hid-and-seek") (v "0.1.6") (d (list (d (n "ron") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "0mbz04rbm5xwahl6j1vnjkgqm562c75gp037lz256wcs24v61zzz") (y #t)))

