(define-module (crates-io hi d- hid-cli) #:use-module (crates-io))

(define-public crate-hid-cli-0.1.0 (c (n "hid-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.33") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "hidapi") (r "^1.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "simplelog") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.20") (d #t) (k 0)))) (h "1j8pf2cs9psy2fsbjilqb2ivx2mqvplh9libfvsh8173zwn7sm8w")))

