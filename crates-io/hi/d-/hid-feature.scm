(define-module (crates-io hi d- hid-feature) #:use-module (crates-io))

(define-public crate-hid-feature-0.1.0 (c (n "hid-feature") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.86") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hidraw") (r "^0.0.7") (f (quote ("unsafe_reports"))) (d #t) (k 0)) (d (n "hidreport") (r "^0.3.0") (d #t) (k 0)) (d (n "hut") (r "^0.2.0") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0.0") (f (quote ("supports-colors"))) (d #t) (k 0)))) (h "1kg5swmdpngkfg722gi53qg9l0sgm3m6hhn1r8d84xdyp6cw9g7y")))

