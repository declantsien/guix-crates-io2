(define-module (crates-io hi d- hid-replay) #:use-module (crates-io))

(define-public crate-hid-replay-0.1.0 (c (n "hid-replay") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "glob") (r "^0.3.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "nix") (r "^0.28.0") (d #t) (k 0)) (d (n "uhid-virt") (r "^0.0.7") (d #t) (k 0)))) (h "1d2my6bpl0l4plkpv82jq79mbxl6jpgy0ssv3nl2d7iaql8rgm5s")))

