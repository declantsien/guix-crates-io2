(define-module (crates-io hi d- hid-api-rs) #:use-module (crates-io))

(define-public crate-hid-api-rs-1.0.2 (c (n "hid-api-rs") (v "1.0.2") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "0psk8g6b489y0sc8vfm018qqj7na8prvmizrpvjp0l37zc7ywipm")))

(define-public crate-hid-api-rs-1.0.3 (c (n "hid-api-rs") (v "1.0.3") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "1rmqp2wmrrkcmdhfyfh7pf9lg39g8cx3jmwvkvncmfv2v6sng6av")))

(define-public crate-hid-api-rs-1.1.3 (c (n "hid-api-rs") (v "1.1.3") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)))) (h "040794hywp4jnyf3x8k6jdg8r4b1nq8scl19h428hb105j1s6amf")))

(define-public crate-hid-api-rs-2.0.0 (c (n "hid-api-rs") (v "2.0.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)))) (h "0c5bji22rjr01q8d4i57clydwc1cfisvz04m6683f0g5q17vdcxq")))

(define-public crate-hid-api-rs-2.0.1 (c (n "hid-api-rs") (v "2.0.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)))) (h "14irijfblrql2cr51zyhsikgpfhwq5fn47cdjqkd3f1irp7v5drz")))

(define-public crate-hid-api-rs-3.0.0 (c (n "hid-api-rs") (v "3.0.0") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)))) (h "188qzdkcvk5ayylsbmlkhjqwd5igs9g4d0abh3951p539k294vrj")))

(define-public crate-hid-api-rs-3.0.1 (c (n "hid-api-rs") (v "3.0.1") (d (list (d (n "bitvec") (r "^1.0.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "strum") (r "^0.25.0") (d #t) (k 0)) (d (n "strum_macros") (r "^0.25.3") (d #t) (k 0)))) (h "07kpjh8ws8mn49pshcnf8crsjz124mjbgy0y335qq9jhb4vay7a1")))

