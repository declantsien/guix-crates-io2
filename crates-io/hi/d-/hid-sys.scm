(define-module (crates-io hi d- hid-sys) #:use-module (crates-io))

(define-public crate-hid-sys-0.0.1 (c (n "hid-sys") (v "0.0.1") (d (list (d (n "winapi") (r "*") (d #t) (k 0)))) (h "0my1wrv6mb2sq2h3f0lzg1z5bmazl5zmx6b012kz7j4a4384i7yx")))

(define-public crate-hid-sys-0.2.0 (c (n "hid-sys") (v "0.2.0") (d (list (d (n "winapi") (r "^0.2.5") (d #t) (k 0)) (d (n "winapi-build") (r "^0.1.1") (d #t) (k 1)))) (h "1i5bcgnx3ma4ani1phkmzkssgsb0215d16dhckab0iimibsy6a23")))

