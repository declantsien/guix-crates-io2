(define-module (crates-io hi bi hibitset) #:use-module (crates-io))

(define-public crate-hibitset-0.1.0 (c (n "hibitset") (v "0.1.0") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)))) (h "137fsa4x7pfbdmssfmhwx2msw7aspx8xd7gqncnmg8fizhn831vv")))

(define-public crate-hibitset-0.1.1 (c (n "hibitset") (v "0.1.1") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)))) (h "1lpw413igc27p7kyprv4vmnfbappvaxws6q34bslgvaymyn2ck5b")))

(define-public crate-hibitset-0.1.2 (c (n "hibitset") (v "0.1.2") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^0.7") (f (quote ("unstable"))) (o #t) (d #t) (k 0)))) (h "1gsxxzak2g5garyrlbmfsvijra1qlpf4fdfv00kkg10xrhc7sbnh") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.1.3 (c (n "hibitset") (v "0.1.3") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^0.7.1") (o #t) (d #t) (k 0)))) (h "09s9lrw3idsqlm8f6m3sk6xn1dzm5iycaw19j85gs9m08ri1q2ns") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.2.0 (c (n "hibitset") (v "0.2.0") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "1jm2wyi9a6icpzbwybdn0w7x7z5d6qwrnfhqh26bfqvhbxmn96n0") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.3.0 (c (n "hibitset") (v "0.3.0") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "057pdm2yaqp3zz98pm8ix07a6agbddkxsw16ypq4g521w9b99mqk") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.3.1 (c (n "hibitset") (v "0.3.1") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "12prh7dm6wvq39yifb3ycsk58zd143fckf5k9pmg4yqd2npcha6l") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.3.2 (c (n "hibitset") (v "0.3.2") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "113x63wcz0q34r6rwywh4lzp10jbzhykg3vfb4jibms3qbiri2dp") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.4.0 (c (n "hibitset") (v "0.4.0") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "0bf2rp7l0v2vnn1yq9rws8lg35kbbn26y7b0mlc537lki3awkcsi") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.4.1 (c (n "hibitset") (v "0.4.1") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^0.8.2") (o #t) (d #t) (k 0)))) (h "08zzqmn4bn3rgxi1g5c0a94nr002l62m9zxv0zxvb2617bmyy3aa") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.5.0 (c (n "hibitset") (v "0.5.0") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "10msjhlzkismxhjpnin2b5vihv5qf9ps5mm4y7pigs8bpzpv57qy") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.5.1 (c (n "hibitset") (v "0.5.1") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0n9z3k2m170x7q32pnsx94sr4g0vn6h9vbr2lzgsqivv5nr8k55q") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.5.2 (c (n "hibitset") (v "0.5.2") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "03h7asdswqr3rbs5faix1x2gk3hgkqm4i95bcw97w7qmxbqcnxc8") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.5.3 (c (n "hibitset") (v "0.5.3") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "0japzizxdxrin858z2ghh1516hhn1yfycbgrcmkrr5dfi5fxm5d7") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.5.4 (c (n "hibitset") (v "0.5.4") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1l4jb5xd5xvmrymd1b4ajwv9p88ppyr78a2pqwk3j39fyf4bq9v5") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.6.0 (c (n "hibitset") (v "0.6.0") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1") (o #t) (d #t) (k 0)))) (h "1nxzx1y6rrzza4lxz6sgniia8ld8715gkd4d31xpddz96fd149pb") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.6.1 (c (n "hibitset") (v "0.6.1") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (o #t) (d #t) (k 0)))) (h "04k13g1anz33s590h7dz0jgajfb4ys85m333lfxgjli46hvp7n1w") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.6.2 (c (n "hibitset") (v "0.6.2") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rayon") (r "^1.1") (o #t) (d #t) (k 0)))) (h "13a0dyma3y7n316j611n4knpk6x6s3r4i0696px8kzppv4pjkrs7") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.6.3 (c (n "hibitset") (v "0.6.3") (d (list (d (n "atom") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "1cb4dpjbhlg88s6ac16c4fymqffpip9c8ls2s6kmji542s1vp8ck") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

(define-public crate-hibitset-0.6.4 (c (n "hibitset") (v "0.6.4") (d (list (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (o #t) (d #t) (k 0)))) (h "08812bg7is6cn07dhfk5l8aiw8f4vcx1cr8d6dh8x58clv7ybvgk") (f (quote (("parallel" "rayon") ("default" "parallel"))))))

