(define-module (crates-io hi -n hi-nvim-rs) #:use-module (crates-io))

(define-public crate-hi-nvim-rs-0.1.0 (c (n "hi-nvim-rs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "palette") (r "^0.7.6") (d #t) (k 0)) (d (n "palette-gamut-mapping") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0q83sd61ix1jk222p023w4wks81mwf00vq0ywcqxsdbhvygdzdsr")))

