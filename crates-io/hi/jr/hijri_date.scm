(define-module (crates-io hi jr hijri_date) #:use-module (crates-io))

(define-public crate-hijri_date-0.1.0 (c (n "hijri_date") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)))) (h "0h3znv6m12170kawzyxrmjp8ggc5af1y69hqpkd2k21ddby69nd2")))

(define-public crate-hijri_date-0.1.1 (c (n "hijri_date") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)))) (h "1055i9vbj2r40k0fqd3wg61jp8s5z03b1xa844nbrnff5v6n8vfq")))

(define-public crate-hijri_date-0.1.2 (c (n "hijri_date") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)))) (h "1255l84fq3l0ljwwb3c5r7z30vwjl70n51a89pzdv1anr5z2s07n")))

(define-public crate-hijri_date-0.1.3 (c (n "hijri_date") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)))) (h "1x3id3wqv73j3pjh4ra75a2p28nx10rw6zb3cq8qjgz7kqa9a2g4")))

(define-public crate-hijri_date-0.1.4 (c (n "hijri_date") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)))) (h "1cns5wkkf2m6m5h9ikhqd8iv45nmx4srnnqbig5ygnvqbxri88wy")))

(define-public crate-hijri_date-0.1.5 (c (n "hijri_date") (v "0.1.5") (d (list (d (n "arabic_reshaper") (r "^0.1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)))) (h "1xx7gghn6fkgp0bcjazrj09c4l132cmxn0w8ynz3wr9rbmsshpzn")))

(define-public crate-hijri_date-0.1.6 (c (n "hijri_date") (v "0.1.6") (d (list (d (n "arabic_reshaper") (r "^0.1.4") (d #t) (k 0)) (d (n "chrono") (r "^0.4.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0.2") (d #t) (k 0)))) (h "0al39bwkfwxv4swl4aap3d08gq572g6am3k7pgz1d7wwkdcab4f2")))

(define-public crate-hijri_date-0.2.0 (c (n "hijri_date") (v "0.2.0") (d (list (d (n "arabic_reshaper") (r "^0.1.6") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)))) (h "001zgry6p60sqm454n0wl2nx6b16b76dv5cvkwh6xqinhy8y8h4n")))

(define-public crate-hijri_date-0.2.1 (c (n "hijri_date") (v "0.2.1") (d (list (d (n "arabic_reshaper") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)))) (h "1dbwk9gnbzahmqxnzw11br2imdzv2l0gl7xpxkiwccl32kxyp51l")))

(define-public crate-hijri_date-0.2.2 (c (n "hijri_date") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)))) (h "0x0n96pif63zak393l9l5iham0dckfry8g9qikwcza4aaxkpmbb7")))

(define-public crate-hijri_date-0.3.0 (c (n "hijri_date") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)))) (h "0nidmjzdfhpmzvxl68b0vjcd14x0lspa18rg61dz8l7hgrzlss64")))

(define-public crate-hijri_date-0.3.1 (c (n "hijri_date") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)))) (h "10vhibm8vdsba6rm2yxcjzn6bbhl8r2vs1hdgxvc9kklb86bkrxn")))

(define-public crate-hijri_date-0.3.2 (c (n "hijri_date") (v "0.3.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)))) (h "028hwx182zslglsm5iphrpkdqsrdailq2p1nk318c6syxiiaxj4l")))

(define-public crate-hijri_date-0.4.0 (c (n "hijri_date") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0q621dsr58nrpfwasz28wfpgi5pkxjygipiza2g224z9rf3n0gf4")))

(define-public crate-hijri_date-0.4.1 (c (n "hijri_date") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)))) (h "1jc6hnyqkns9l5sjdljpc1q4m4064nlgyysi53681l8b798lr4vk")))

(define-public crate-hijri_date-0.5.0 (c (n "hijri_date") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "14x33s7mm4zqsj3drk2r1021623c7mr74iw8aa2wb6qww9ck5s84")))

(define-public crate-hijri_date-0.5.1 (c (n "hijri_date") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "00jn4c2lnzdhmwl9hphgpm6dcwms0lij54l4frnw1k4b1h7vp1x3")))

