(define-module (crates-io hi p- hip-runtime-sys) #:use-module (crates-io))

(define-public crate-hip-runtime-sys-0.1.0 (c (n "hip-runtime-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.73") (d #t) (k 0)))) (h "0npn0lc9safan607dkpdjbfsyl5vhcwdbqgnwvcrsygzrxa5s6lh") (l "amdhip64")))

