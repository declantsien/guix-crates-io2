(define-module (crates-io hi p- hip-sys) #:use-module (crates-io))

(define-public crate-hip-sys-0.1.1 (c (n "hip-sys") (v "0.1.1") (d (list (d (n "hip-runtime-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "hipblas-sys") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "11s2l1wz2jfal3kk0g2dz0mzzqkhmadr97h323y9w95nk2gn56gk") (f (quote (("blas" "hipblas-sys")))) (s 2) (e (quote (("bindgen" "hip-runtime-sys/bindgen" "hipblas-sys?/bindgen")))) (r "1.60")))

