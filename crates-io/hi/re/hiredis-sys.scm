(define-module (crates-io hi re hiredis-sys) #:use-module (crates-io))

(define-public crate-hiredis-sys-0.0.1 (c (n "hiredis-sys") (v "0.0.1") (h "1csfqn6h9h5prgg33p76gmp0s444skndnb6s69iccgdi5qh2l49l")))

(define-public crate-hiredis-sys-0.1.0 (c (n "hiredis-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0d6rrxlv81sakx2swvfb9inj57bdbqmfbqhxn148kjm0sq1sb8sr")))

(define-public crate-hiredis-sys-0.1.1 (c (n "hiredis-sys") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0a00p3hmsy3bps2ylsm7bbdp4zbribqffyc1lsfkjvc4iyxsricy")))

(define-public crate-hiredis-sys-0.2.0 (c (n "hiredis-sys") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1w2aya6ycy25z1x9amal10m773z0rxs9g5pzfb65kab03k6v0cv1")))

(define-public crate-hiredis-sys-0.2.1 (c (n "hiredis-sys") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "14dbrpai0a947hscjm6xil9qwi18jr9vs6kcmam2pqqkxkxj6x6m")))

(define-public crate-hiredis-sys-0.3.0 (c (n "hiredis-sys") (v "0.3.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0a667c3226ki3b0vkrnk50l9d21xxp07y122p1jbqbgryf2q0pnj")))

(define-public crate-hiredis-sys-0.3.1 (c (n "hiredis-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l0qv6m7mafqcl6jfjsgibmzrz3j195ybnc8bl974nfh28j5sr43")))

(define-public crate-hiredis-sys-0.3.2 (c (n "hiredis-sys") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01w452frmxq7k64jw4x6fj1g28lph7v0y9c91yzfaa8f607d579r")))

(define-public crate-hiredis-sys-0.4.0 (c (n "hiredis-sys") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yfaan5n4c5h8mxy5ihbf24lvp3zcywj1q3hm9ya4hahn44ll04l")))

(define-public crate-hiredis-sys-0.4.1 (c (n "hiredis-sys") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0n59i8gibwpc0wb2w5xsxfxpwac3dfwc0wxxg54jvyizd9c5hr13")))

