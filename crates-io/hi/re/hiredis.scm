(define-module (crates-io hi re hiredis) #:use-module (crates-io))

(define-public crate-hiredis-0.0.1 (c (n "hiredis") (v "0.0.1") (h "1f91zrjvd8r8x2dckdy4nyx8pvxkj95p8vwlllmffxgji5x3wnn1")))

(define-public crate-hiredis-0.1.0 (c (n "hiredis") (v "0.1.0") (d (list (d (n "hiredis-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1fpjj03z6dm4ls5fkbbjwwrzki49j2falx598b227s3fi9vwach1")))

(define-public crate-hiredis-0.1.1 (c (n "hiredis") (v "0.1.1") (d (list (d (n "hiredis-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1fkshja4pb69zb8fszldm5ac03wj94g96n256m1gblhwcvwz36m4")))

(define-public crate-hiredis-0.1.2 (c (n "hiredis") (v "0.1.2") (d (list (d (n "hiredis-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1sycny4b1q6aiwvjcv472z4059nnj4289y5rdc6q1v86mydxb9p3")))

(define-public crate-hiredis-0.1.3 (c (n "hiredis") (v "0.1.3") (d (list (d (n "hiredis-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1gp60h08z12z3x1nq10mnfj744wwp3ha89f17q7c6ykrkvmlp3ng")))

(define-public crate-hiredis-0.2.0 (c (n "hiredis") (v "0.2.0") (d (list (d (n "hiredis-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1il4094wl5d84cbvl7nvw53qdbgv16wndq021zrlv4pq756ijn1i")))

(define-public crate-hiredis-0.2.1 (c (n "hiredis") (v "0.2.1") (d (list (d (n "hiredis-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "03zfpifsah9xyvx9qk897861xk1rhjrgjsv3b5zkmpc36m5dv71q")))

(define-public crate-hiredis-0.2.2 (c (n "hiredis") (v "0.2.2") (d (list (d (n "hiredis-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1zw9rppvg7r781sz14vw17w83f4z29d9pai8s3rvmmmjyig7qvxs")))

(define-public crate-hiredis-0.2.3 (c (n "hiredis") (v "0.2.3") (d (list (d (n "hiredis-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1nb3a1hgifz1sj41l4c8df4a6q8z5d9i4i0937bjvq9jkcdc6zdq")))

(define-public crate-hiredis-0.2.4 (c (n "hiredis") (v "0.2.4") (d (list (d (n "hiredis-sys") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q0msh3j94562w7ygh82cj8drkafi2a12spchvwhw27m2ajnlj54")))

(define-public crate-hiredis-0.3.0 (c (n "hiredis") (v "0.3.0") (d (list (d (n "hiredis-sys") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05rsw6qzwl6jpmzqx9v0krk635hgp5yk0dyfhigrgccq68kcp2hn")))

(define-public crate-hiredis-0.3.1 (c (n "hiredis") (v "0.3.1") (d (list (d (n "hiredis-sys") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lshh2cjf38a21sfh4gkyh4j8rix9x2vv4pkicz37a5ql5knqnmj")))

