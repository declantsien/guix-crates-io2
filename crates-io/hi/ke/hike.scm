(define-module (crates-io hi ke hike) #:use-module (crates-io))

(define-public crate-hike-0.1.0 (c (n "hike") (v "0.1.0") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "030xnjwrwwcfglizv0nwq7k3lhqsppili8gy3xzcwdv552mba4sj")))

(define-public crate-hike-0.1.1 (c (n "hike") (v "0.1.1") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0y5csgwdqz2vv2bvi8b9444fpqrxgg84sy871957zi87rq7f0qr8")))

(define-public crate-hike-0.1.2 (c (n "hike") (v "0.1.2") (d (list (d (n "docopt") (r "^0.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10qyxj39zalp0fy5hnxxjxkr85p3a460f48a6ksdyinlmxpgpv7d")))

