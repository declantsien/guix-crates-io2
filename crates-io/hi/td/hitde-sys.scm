(define-module (crates-io hi td hitde-sys) #:use-module (crates-io))

(define-public crate-hitde-sys-0.1.0 (c (n "hitde-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "0fjf50ga1sn34mccq9vawy3dh9wixwfnjw7i7xpzx3fpbqrvjvy2") (f (quote (("static-link") ("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default" "hi3559av100"))))))

(define-public crate-hitde-sys-0.1.1 (c (n "hitde-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "170dg2jh37p5jv1q1l1xr34h566gcgcwij0z10lmihskdp29qvwy") (f (quote (("static-link") ("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hitde-sys-0.1.2 (c (n "hitde-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1f8p982p6qv2pfx0sqrv9jr2phklnp7slpmaynypp1570am65yqm") (f (quote (("static-link") ("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hitde-sys-0.1.3 (c (n "hitde-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "0ph1xp7b4ninni3l6j7yri47y3gxi6zjpx0s4x4dx99phs8bg7ar") (f (quote (("static-link") ("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hitde-sys-0.1.4 (c (n "hitde-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1z402mgmm5awkmxksxzr9h7g8s27fxshawdk18shby66p9yx5mxj") (f (quote (("static-link") ("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hitde-sys-0.1.5 (c (n "hitde-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "108lpzfhx8531vlb0l79nivxr37nqc68r1m90q27mgyk2x901gd9") (f (quote (("static-link") ("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hitde-sys-0.1.6 (c (n "hitde-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "01nw6ndi53qk6k4cyaylzh06xgvv9251sdwgg59m3k6gc2jqy0z3") (f (quote (("static-link") ("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hitde-sys-0.1.7 (c (n "hitde-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "pavo-traits") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "0dhwfyridmil37p64xzpglb97cjnzpszzcp2p351zs5bipr3zx0a") (f (quote (("static-link") ("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hitde-sys-0.1.8 (c (n "hitde-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)) (d (n "pavo-traits") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "0kr27480x5p1zwyj39dagaqxh1qp40x0lwcvs3wql6la3skiklsp") (f (quote (("static-link") ("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

(define-public crate-hitde-sys-0.1.9 (c (n "hitde-sys") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pavo-traits") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "0zhjfwjnibbwgahbyk959hvcf2vln1ahp0dbwxpw6gfgn5gq059y") (f (quote (("static-link") ("hi3559av100") ("hi3531v100") ("hi3519av100") ("hi3518ev300") ("hi3518ev200") ("hi3516ev300") ("hi3516ev200") ("default"))))))

