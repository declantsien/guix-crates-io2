(define-module (crates-io hi de hide_my_email) #:use-module (crates-io))

(define-public crate-hide_my_email-0.1.1 (c (n "hide_my_email") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f2dqi9l7g1rhdymb65kap43mfm094yxdc6w73gc0cs3qr8zlqia")))

(define-public crate-hide_my_email-0.1.2 (c (n "hide_my_email") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1r2jhbmpcvfv9fw6ks5bkqnmaba9kpvmcnxrj6qs686wbn5zzr3s")))

(define-public crate-hide_my_email-0.1.3 (c (n "hide_my_email") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("cookies" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1276ccq7x4ap5w0fvygp3xffcvjnr78yydy2cz9zqcn61aw6bhrp")))

