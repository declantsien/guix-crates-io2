(define-module (crates-io hi fm hifmt) #:use-module (crates-io))

(define-public crate-hifmt-0.1.3 (c (n "hifmt") (v "0.1.3") (d (list (d (n "hifmt-macros") (r "^0.2.1") (d #t) (k 0)))) (h "12nypk4n7z2rh4glb5r3vif10plxa1w8avvffwg4lh901cj90dn2")))

(define-public crate-hifmt-0.1.4 (c (n "hifmt") (v "0.1.4") (d (list (d (n "hifmt-macros") (r "^0.2.2") (d #t) (k 0)))) (h "0492yymhbd3201lb7w2mdpdgpr0pzsmz19mh6f3rvy1g95hrw6bk")))

