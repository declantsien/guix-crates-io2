(define-module (crates-io hi fm hifmt-macros) #:use-module (crates-io))

(define-public crate-hifmt-macros-0.2.1 (c (n "hifmt-macros") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "03pdf11kx0lxi8qswc2fbig6wj903wafg155inmqyfzg8b66h2bg")))

(define-public crate-hifmt-macros-0.2.2 (c (n "hifmt-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1ldxkdfzd8cwgrrws5899p4qk95v7wv2qxjn164af27dmcdzmxpg")))

