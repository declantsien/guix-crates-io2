(define-module (crates-io hi co hicollections) #:use-module (crates-io))

(define-public crate-hicollections-0.1.0 (c (n "hicollections") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hipool") (r "^0.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1z36rbzwwl88v8db9ib58yyyc0sikah5h7ri0w2y0hbm8qrxf70x")))

(define-public crate-hicollections-0.1.1 (c (n "hicollections") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hipool") (r "^0.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0di6696szsg0q9rgqqygyjmssd1vhh7x297sxpmzksyhbdfv7vwd")))

(define-public crate-hicollections-0.1.2 (c (n "hicollections") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hipool") (r "^0.1") (d #t) (k 0)) (d (n "iai") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "05dza8k3r364wsfcj4m2hcpvydixkhgc0fl284dwlrak4p151yzf")))

