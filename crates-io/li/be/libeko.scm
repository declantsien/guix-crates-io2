(define-module (crates-io li be libeko) #:use-module (crates-io))

(define-public crate-libeko-0.1.0 (c (n "libeko") (v "0.1.0") (d (list (d (n "curl") (r "^0.4.11") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1niq3vnk7gvc82ysr3m5g6v7kggxk6szr72s46v3zjrp40pdbcyi")))

(define-public crate-libeko-0.1.1 (c (n "libeko") (v "0.1.1") (d (list (d (n "curl") (r "^0.4.11") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "getset") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^0.6") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0kxpl1g3n3vd1ry23qgkfzvhccl5y4qc16dic7x3mp7h660hclwc")))

(define-public crate-libeko-0.1.2 (c (n "libeko") (v "0.1.2") (d (list (d (n "curl") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1j9sfnsspj6a8rifcpbggwhhrlgmy5fc566ni75gs2db15kdqwjw")))

(define-public crate-libeko-0.1.3 (c (n "libeko") (v "0.1.3") (d (list (d (n "curl") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0afvd6zz3qvspzhnw47gp4bzrkarlbmys6vh2avyymavvckda43q")))

(define-public crate-libeko-0.1.4 (c (n "libeko") (v "0.1.4") (d (list (d (n "curl") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0bzlzdckz0n50knd79ki86c0gd2j9vq634vqrggnb87ism2xhy91")))

(define-public crate-libeko-0.1.5 (c (n "libeko") (v "0.1.5") (d (list (d (n "curl") (r "^0") (d #t) (k 0)) (d (n "error-chain") (r "^0") (d #t) (k 0)) (d (n "getset") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1fqfwcnpgb15x23nv915j6h0r7znbibhk70n7pbzcin3vxqx2f9x")))

