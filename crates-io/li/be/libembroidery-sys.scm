(define-module (crates-io li be libembroidery-sys) #:use-module (crates-io))

(define-public crate-libembroidery-sys-0.1.0 (c (n "libembroidery-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vfdfb9z9lk2g8y30m1bz9bj4al66qwsrli78jqv4lq5034k6g7y")))

(define-public crate-libembroidery-sys-0.1.1 (c (n "libembroidery-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f8g6ndks1ipdd22y7h240l518ddc3j0pvpfvr4pjzj74v3y8djj")))

(define-public crate-libembroidery-sys-0.1.2 (c (n "libembroidery-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nicy86gapbvkxibq9l26gwiyfd1piqhjipxk34l47204f6cg4x8")))

