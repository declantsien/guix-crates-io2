(define-module (crates-io li be libevdev-sys) #:use-module (crates-io))

(define-public crate-libevdev-sys-0.1.0 (c (n "libevdev-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "1q6gbfmbv6hsm9bjfzpnia5bi76myp38lxszgdl42zggdn7n3bsc")))

(define-public crate-libevdev-sys-0.1.1 (c (n "libevdev-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.14") (d #t) (k 0)))) (h "07q19qb65rchdv4p6gpfm93ha745k92420jh93ssika3jgsxm99d")))

