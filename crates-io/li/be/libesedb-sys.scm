(define-module (crates-io li be libesedb-sys) #:use-module (crates-io))

(define-public crate-libesedb-sys-0.1.0 (c (n "libesedb-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0hfpj99qmarnzd2wmff6k8076bdzy0qs9k0li7bf1agm4x9gwzmb") (l "esedb")))

(define-public crate-libesedb-sys-0.1.1 (c (n "libesedb-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "092hgzldn7zwkzdy3spphn0x5m147v09xn9gyz11sipdl895z3hb") (l "esedb")))

(define-public crate-libesedb-sys-0.1.2 (c (n "libesedb-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "0vd2vmazqd50v8d9jhfax4nsy85mhzsrah7hxj541v58b7b1ydm0") (l "esedb")))

(define-public crate-libesedb-sys-0.2.0 (c (n "libesedb-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1xd86d0cj0mm4icb9nx9qb9450iq3zrbw2d2a44dg0mqlxz0d9gp") (l "esedb")))

