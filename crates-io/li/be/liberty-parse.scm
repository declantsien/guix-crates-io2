(define-module (crates-io li be liberty-parse) #:use-module (crates-io))

(define-public crate-liberty-parse-0.1.0 (c (n "liberty-parse") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "nom") (r "^5.0.0") (d #t) (k 0)))) (h "0jgg5vmzwz48k1w53f2gcdbqh7y4jk352h9gw4p17lq4vyz0f7mq")))

