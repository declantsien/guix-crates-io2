(define-module (crates-io li be libelf-sys) #:use-module (crates-io))

(define-public crate-libelf-sys-0.0.1 (c (n "libelf-sys") (v "0.0.1") (h "0hp9b5sww88hdr39qv2wmqa9ss9yyvkvw3gr62w66q063n2xhdl1")))

(define-public crate-libelf-sys-0.0.2 (c (n "libelf-sys") (v "0.0.2") (d (list (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1ablh30l5jf6k3wxyrq2cw30cmxva0k2pflhxdbf1x65yvn5zyhl")))

(define-public crate-libelf-sys-0.0.3 (c (n "libelf-sys") (v "0.0.3") (d (list (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1hv4m0h88lljvfjp3v04ixlwgzhvfdn35g9734pihv89477ml0ki") (y #t)))

(define-public crate-libelf-sys-0.0.4 (c (n "libelf-sys") (v "0.0.4") (d (list (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0zgbxbsdm021rzmxgxjm99as4b5vv0q3bimcxk03xfij7frbrv2x")))

(define-public crate-libelf-sys-0.0.5 (c (n "libelf-sys") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0hhkr7kkgaz3fvcpxa09nbirwj27cpnf6wkr0ld0l2ayd0jfrbs0")))

(define-public crate-libelf-sys-0.0.6 (c (n "libelf-sys") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1qbr6wla06y58jbclmymvpkgvqa1m4ws5i3ac5hgjwg3pqnx7w7c") (l "elf")))

(define-public crate-libelf-sys-0.1.0 (c (n "libelf-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.15") (d #t) (k 1)))) (h "1j6z5fr2b4v66s1abwk49jq6jh2bp2l7879mrz2gn86d73lfwwhk") (l "elf")))

