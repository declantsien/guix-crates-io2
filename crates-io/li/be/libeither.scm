(define-module (crates-io li be libeither) #:use-module (crates-io))

(define-public crate-libeither-0.1.0 (c (n "libeither") (v "0.1.0") (d (list (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0") (d #t) (k 2)))) (h "0pmsaw3hrpm29iz52dz2cxs0rcnfhfqb24dl6hg14mgk67fafc5l") (f (quote (("unstable") ("default"))))))

(define-public crate-libeither-0.1.1 (c (n "libeither") (v "0.1.1") (d (list (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0") (d #t) (k 2)))) (h "1v99dibd7w5nw7cg3nqgdybcw2rrbcw0pay9lsdry6a792ajl6a2") (f (quote (("unstable") ("default"))))))

(define-public crate-libeither-0.2.0 (c (n "libeither") (v "0.2.0") (d (list (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0") (d #t) (k 2)))) (h "0mh09vwsvpkfxvnxvhhdvzrlwygmv8s23rwv2asmwiadfcs0hd7m") (f (quote (("unstable") ("default"))))))

(define-public crate-libeither-0.3.0 (c (n "libeither") (v "0.3.0") (d (list (d (n "failure") (r "^0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0") (d #t) (k 2)))) (h "1pqkccr5w5362zx9l6i15ijqvwcdkyya994sgk9m1gh729xd1jiz") (f (quote (("unstable") ("default" "serde"))))))

(define-public crate-libeither-0.4.0 (c (n "libeither") (v "0.4.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_derive") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "toml") (r "^0") (d #t) (k 2)))) (h "18gzsz4wb0j4dzgb11vl6amccdy347vr01sghv7bq7m2glq4lpdk") (f (quote (("unstable") ("serialization" "serde" "serde_derive") ("default" "serialization"))))))

(define-public crate-libeither-0.5.0 (c (n "libeither") (v "0.5.0") (d (list (d (n "rustversion") (r "^1.0.9") (d #t) (k 1)) (d (n "serde") (r "^1.0.149") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "toml") (r "^0.5.9") (d #t) (k 2)))) (h "1clfm813fqh62f0ixahkk1pnvhn0c3y57qh3nfs6rkn38x9rajdw") (f (quote (("unstable") ("serialization" "serde") ("default" "serialization"))))))

