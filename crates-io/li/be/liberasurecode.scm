(define-module (crates-io li be liberasurecode) #:use-module (crates-io))

(define-public crate-liberasurecode-1.0.0 (c (n "liberasurecode") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lz1xsd92q9mq2b2bx13brxba34jg783r5wlkjjb9nw8ylxbnlqz")))

(define-public crate-liberasurecode-1.0.1 (c (n "liberasurecode") (v "1.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1i0a9id45af4121gky26b70hghlqajz0z0kbfzgd605m1m8v9fic") (l "erasurecode")))

(define-public crate-liberasurecode-1.0.2 (c (n "liberasurecode") (v "1.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r2b0l0qslvm0ilh0ggz2yg0c1vz1myjx66d205yqdm3dcak76hg") (l "erasurecode")))

(define-public crate-liberasurecode-1.0.3 (c (n "liberasurecode") (v "1.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09c33784wppl27dxj998wdw16qvi7jrhw95b6d9py6ml9cllz6sv") (l "erasurecode")))

