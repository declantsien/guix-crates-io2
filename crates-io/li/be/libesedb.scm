(define-module (crates-io li be libesedb) #:use-module (crates-io))

(define-public crate-libesedb-0.1.0 (c (n "libesedb") (v "0.1.0") (d (list (d (n "libesedb-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0xfclmaqfnnbx319dyjyrhaz2xpfdnh97hhslqzfslfrhp67nwdq")))

(define-public crate-libesedb-0.1.1 (c (n "libesedb") (v "0.1.1") (d (list (d (n "libesedb-sys") (r "^0.1.1") (d #t) (k 0)))) (h "05jics3n4viyc9nqm25aw0rbf6242744mj29zk46hd18fchxbhjc")))

(define-public crate-libesedb-0.1.2 (c (n "libesedb") (v "0.1.2") (d (list (d (n "libesedb-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1g7fr1xfdd1lfafmf8iypg5xq9gciw3r01zgz52knkw6kqzx5j65")))

(define-public crate-libesedb-0.1.3 (c (n "libesedb") (v "0.1.3") (d (list (d (n "libesedb-sys") (r "^0.1.2") (d #t) (k 0)))) (h "16pdi4xln4lpxgrb9kkw2g9pa01y5zgjrjnm6z1div0zw745xrm1")))

(define-public crate-libesedb-0.1.4 (c (n "libesedb") (v "0.1.4") (d (list (d (n "libesedb-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "18bsfvwd2ysws7miwiz2mc92xk80wz8qyk8j7645fys7iw370vqm")))

(define-public crate-libesedb-0.2.0 (c (n "libesedb") (v "0.2.0") (d (list (d (n "libesedb-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "06q1jglm7kf0pl2k4l6jmckgf3ixcs9v3bdgfg8nbz7ifijdz699")))

(define-public crate-libesedb-0.2.1 (c (n "libesedb") (v "0.2.1") (d (list (d (n "libesedb-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1xy6pd2vsbf3s93n6abqxbplrxdd7zyscps1mfvf6cdqqjjpim4y")))

(define-public crate-libesedb-0.2.2 (c (n "libesedb") (v "0.2.2") (d (list (d (n "libesedb-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "0kb5h1sv5cgnxsa18szdz8h6d9vm4r1bc0k1789szzcih8zhnfzp")))

(define-public crate-libesedb-0.2.3 (c (n "libesedb") (v "0.2.3") (d (list (d (n "libesedb-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "0z8p109s5wq1jm47zv1s6r7j3zxk4bz12w7bmlbiygxv7fhp0v50")))

(define-public crate-libesedb-0.2.4 (c (n "libesedb") (v "0.2.4") (d (list (d (n "libesedb-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "1brjak29nh15q5n7gisnrdq7qxh7mxgpm1n01yizgqkcb1008ls0")))

(define-public crate-libesedb-0.2.5 (c (n "libesedb") (v "0.2.5") (d (list (d (n "libesedb-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "time") (r "^0.3.20") (f (quote ("formatting" "macros"))) (d #t) (k 0)))) (h "068mcxk9j9d3h8vbkhxgv401hz23k6bdb0m5y9mg2hiv3kfclp1n")))

