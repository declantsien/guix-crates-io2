(define-module (crates-io li be liberty-parser) #:use-module (crates-io))

(define-public crate-liberty-parser-0.1.1 (c (n "liberty-parser") (v "0.1.1") (d (list (d (n "compact_str") (r "^0.6.1") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (d #t) (k 0)) (d (n "readonly") (r "^0.2.2") (d #t) (k 0)) (d (n "strum") (r "^0.24") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)))) (h "01xc35hbp5d325wysylkmpwvrlndf0m223gdb051s1bnvhl68z1s")))

