(define-module (crates-io li be libelf) #:use-module (crates-io))

(define-public crate-libelf-0.0.1 (c (n "libelf") (v "0.0.1") (d (list (d (n "libelf-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1930sxrxb1m9bw13nxlvqznbyb9da8ry7bv0dj6yr2ka8gmqxs7l")))

(define-public crate-libelf-0.0.2 (c (n "libelf") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "libelf-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1fqy5m074zfqn5lqqs4azcw20905a243s0kv6gblswn36lg9xxg4")))

(define-public crate-libelf-0.0.3 (c (n "libelf") (v "0.0.3") (d (list (d (n "libelf-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1kv6dc7whwq83bzjf2jhcbkp4cfdiinvg2ypcblaykwf00fcs1zc") (y #t)))

(define-public crate-libelf-0.0.4 (c (n "libelf") (v "0.0.4") (d (list (d (n "libelf-sys") (r "^0.0.4") (d #t) (k 0)))) (h "1m6vnbgpv38m76lcyycn0jhzpq76chsr7sayplqq1143pms0aif9")))

(define-public crate-libelf-0.0.5 (c (n "libelf") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libelf-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1xg1s5hb9qz9zsc95x2qnjczmiq8cxikczcd9qcqg1q5yc5avbv6")))

(define-public crate-libelf-0.0.6 (c (n "libelf") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libelf-sys") (r "^0.0.6") (d #t) (k 0)))) (h "0m1h20zdbdfp21ll98kijpnbgfldx0ga7z9ar2w1hc0kbmkx4pvr")))

(define-public crate-libelf-0.1.0 (c (n "libelf") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libelf-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0h89kdrh9ym3ayb7hdzpgq1s6qypn2n2mq3003nhn0bwvc5isbbr")))

