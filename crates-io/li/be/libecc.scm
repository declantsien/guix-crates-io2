(define-module (crates-io li be libecc) #:use-module (crates-io))

(define-public crate-libecc-0.1.0 (c (n "libecc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)))) (h "0vx0l4z060kgm6hrm4375y0wirkp0842y79zq74na00b8sz00kdl")))

(define-public crate-libecc-0.2.0 (c (n "libecc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0q5vlx1xj2x8ldpa97qp5q9amfmwxv800n88195i5b7hylaa3s3q")))

(define-public crate-libecc-0.2.1 (c (n "libecc") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 2)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.17.0") (f (quote ("rt" "macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "1z30mg0csd31cn67lggi2gwsnmnpz1p6a0487f2c402pbw84vbml")))

(define-public crate-libecc-0.2.2 (c (n "libecc") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.57") (d #t) (k 0)) (d (n "bitvec") (r "^1.0.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "phf") (r "^0.10.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "tokio") (r "^1.18.2") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tokio") (r "^1.18.2") (f (quote ("rt" "macros" "rt-multi-thread" "time"))) (d #t) (k 2)))) (h "0irdw467v2hx0g9823z1vvw6bm1fxmd0mg5r9kj74flcswkfvd1a")))

