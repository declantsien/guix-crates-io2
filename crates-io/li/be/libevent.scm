(define-module (crates-io li be libevent) #:use-module (crates-io))

(define-public crate-libevent-0.0.0 (c (n "libevent") (v "0.0.0") (h "054hbsmqpcrv707b92fyc8fqmwcijf1w5gq60jhxnna8qvdqplkx")))

(define-public crate-libevent-0.1.0 (c (n "libevent") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libevent-sys") (r "^0.2") (k 0)))) (h "1zqx2n9877g3qrww6chm9fms3g6ncwvgald6n4h17d7mx1y3h80g") (f (quote (("verbose_build" "libevent-sys/verbose_build") ("threading" "libevent-sys/threading") ("static" "libevent-sys/static") ("pkgconfig" "libevent-sys/pkgconfig") ("openssl_bundled" "libevent-sys/openssl_bundled" "threading") ("openssl" "libevent-sys/openssl") ("default" "pkgconfig" "openssl" "threading" "buildtime_bindgen") ("bundled" "static" "libevent-sys/bundled") ("buildtime_bindgen" "libevent-sys/buildtime_bindgen"))))))

