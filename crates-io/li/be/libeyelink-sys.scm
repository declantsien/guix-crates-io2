(define-module (crates-io li be libeyelink-sys) #:use-module (crates-io))

(define-public crate-libeyelink-sys-0.1.0 (c (n "libeyelink-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "11k3rld37iddz724nh6jhldsgfcng2sx1a4lj40z6vr866xdbwz1") (l "eyelink")))

(define-public crate-libeyelink-sys-0.1.1 (c (n "libeyelink-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.72") (d #t) (k 0)))) (h "1y55ibg0mcqyvvmp401l7x61ml6byjj6ri0wjnjb4zp1pmrc81q5") (f (quote (("sdl-graphics")))) (y #t) (l "eyelink")))

(define-public crate-libeyelink-sys-0.1.2 (c (n "libeyelink-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0xngsjcsmgpz75d0flfdgwij649cvwd264i2yp12w5davdr9xqqh") (f (quote (("sdl-graphics")))) (l "eyelink")))

(define-public crate-libeyelink-sys-0.1.3 (c (n "libeyelink-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0qb6hp7qag4c5q3yw7zmx9xipg02i77qspbjn74kb4rcxhr7hyy4") (f (quote (("sdl-graphics")))) (l "eyelink")))

(define-public crate-libeyelink-sys-0.1.4 (c (n "libeyelink-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1l27p0cahvcw3kkkl6bff0nqwscxhblp4fqn5ng2jmi0ybhi758b") (f (quote (("sdl-graphics")))) (l "eyelink")))

