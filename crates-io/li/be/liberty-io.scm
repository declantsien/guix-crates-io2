(define-module (crates-io li be liberty-io) #:use-module (crates-io))

(define-public crate-liberty-io-0.0.2 (c (n "liberty-io") (v "0.0.2") (d (list (d (n "byteorder") (r "1.3.*") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libreda-db") (r "^0.0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1g75midyf9h3skj34v5ysms6crasy02myg14iyf3l1m2y38ip9ln")))

