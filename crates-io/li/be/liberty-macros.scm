(define-module (crates-io li be liberty-macros) #:use-module (crates-io))

(define-public crate-liberty-macros-0.1.0 (c (n "liberty-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z30i4k283c6n8vq1xdr9ph9asrwavjk67qzlsnsvpb5abxmwdbg")))

(define-public crate-liberty-macros-0.2.0 (c (n "liberty-macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "051h3kxz7mk2zdps7p9whw4jidpxd6i3cwmlfpwvg1wdnpvlhmyn")))

(define-public crate-liberty-macros-0.2.2 (c (n "liberty-macros") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02lng1yzyv52qxxihkzj2zr5flc0ri1vyzqmh2zjf67dv3i8y9g9")))

(define-public crate-liberty-macros-0.2.3 (c (n "liberty-macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "14avhd6jkxszgbyhidd8ip5swrmmd5sl4iaqz53pb7igmcp7kshb")))

(define-public crate-liberty-macros-0.2.4 (c (n "liberty-macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1x5qvhpnqgskr2v3dq660mqcb9b8zcar77dabpjv4if5bdqi0kpz")))

(define-public crate-liberty-macros-0.3.0 (c (n "liberty-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0qrjryhrrv286225gfj6mz06wgqin0kdn3jm6szh9g5p9v2qlgkm")))

(define-public crate-liberty-macros-0.3.1 (c (n "liberty-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0dws34v8nvd2ac6gp4g1pbs2z8ha61xnh2knqpwcbbbglq9myqvm")))

(define-public crate-liberty-macros-0.4.0 (c (n "liberty-macros") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "02jgfajyabrnw3h6p9zk5vscjnqzywnylx2zp68yph6qycy9yirb")))

(define-public crate-liberty-macros-0.4.1 (c (n "liberty-macros") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1qgnhi02ai9af8s1p0vd2r66jmqxdxhn9wzpbknkjnb0ci2f89bc")))

(define-public crate-liberty-macros-0.4.2 (c (n "liberty-macros") (v "0.4.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0a38b7rb7pdp7c9svvv5i3sh4m4qfgprkgdl31y6a934y2pzbnl1")))

(define-public crate-liberty-macros-0.4.3 (c (n "liberty-macros") (v "0.4.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1w7dnxn7rp642zjibgnckd7vq8dycrnnmvxglisgnaz00pkyyjzd")))

(define-public crate-liberty-macros-0.4.4 (c (n "liberty-macros") (v "0.4.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1348a92kikfmx1k9arqdbfhsqc1kaf34mivizhphv2717immimms")))

(define-public crate-liberty-macros-0.4.5 (c (n "liberty-macros") (v "0.4.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "19za8m9hwf5md931yds72ryv962p070pl0sd61rdqx9dp690zr8r")))

(define-public crate-liberty-macros-0.4.7 (c (n "liberty-macros") (v "0.4.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0zd4pyx9fpgd9i9k206l3y0n4r2xvidf8zx8kn278ds220lkgrw3")))

(define-public crate-liberty-macros-0.4.8 (c (n "liberty-macros") (v "0.4.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.15") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1zy0id31f8rjvpmkngplf8ggvg09gsmnf57inn6b54lvy906qw3y")))

