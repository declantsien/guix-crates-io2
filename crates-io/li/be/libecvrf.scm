(define-module (crates-io li be libecvrf) #:use-module (crates-io))

(define-public crate-libecvrf-1.0.1 (c (n "libecvrf") (v "1.0.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (k 0)))) (h "1wlwy0lxfd1rrnh3gnx0h7pka08b4p62blfwd798d7i53wz4wyb5")))

(define-public crate-libecvrf-1.1.1 (c (n "libecvrf") (v "1.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (k 0)))) (h "1hbwyv1j3bi4l1n3wlv2hbn3145shfwk4pv1z64891h3l1cdbbvb") (f (quote (("std") ("no_std") ("default" "no_std"))))))

(define-public crate-libecvrf-1.1.2 (c (n "libecvrf") (v "1.1.2") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "libsecp256k1") (r "^0.7.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tiny-keccak") (r "^2.0.2") (f (quote ("keccak"))) (k 0)))) (h "1icn8601cwk91r18mhaj0pzdl66xwil04pnbgv6rm95w3ak97i5a") (f (quote (("std") ("no_std") ("default" "no_std"))))))

