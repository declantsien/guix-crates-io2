(define-module (crates-io li be libe2fs-sys) #:use-module (crates-io))

(define-public crate-libe2fs-sys-0.1.0 (c (n "libe2fs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1nbpqbyll4bkp9iamab3jyc9kngs7amv6ghjkrgzwhd8rk3h0484") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.0 (c (n "libe2fs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1qrqhp78gc6xfa55v7zhg7gw0lsbi5gnbn7gsvxxi0mgcdcd45k7") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.2 (c (n "libe2fs-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0r6wk83g9z3hknpmvmn5izhnk2mmlr0jhsxczjmhyyhv3n9i9qjq") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.3 (c (n "libe2fs-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1hdpn13psys5v1bwjij73ccjliagl8yzrzm8m6hrzqq5qss0ivzb") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.4 (c (n "libe2fs-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0wqqg1n5304qicipikw9jn4gq5flilb3rcb5v6gjgalnbs4mlxpw") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.5 (c (n "libe2fs-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1bnpxnw52g55ibg2j7rsy4fhjiqbza1ml5prww1qh6jsawmal71j") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.10 (c (n "libe2fs-sys") (v "0.2.10") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1d056vkfvwizdg6byz43qkzd8qqanfpv47jw70sq0dvdcylysy25") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.11 (c (n "libe2fs-sys") (v "0.2.11") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0ck5716n6985fwmgynf66x75nh2p9xhsj3vq69k5y8plg9bk7da5") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.12 (c (n "libe2fs-sys") (v "0.2.12") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "049lskvs9ws45i207kxml6r9mhddv8nmhvr29ngkz2w60whxcvql") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.13 (c (n "libe2fs-sys") (v "0.2.13") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1ndxm1qirs5phkj90161x2dlyh045wjc0sggz6bssws4ibp98alj") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.14 (c (n "libe2fs-sys") (v "0.2.14") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1ij8nrgnj851wgcr69bwm95mwa7mbzlsah7i10w5j23xqpzmsriz") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.15 (c (n "libe2fs-sys") (v "0.2.15") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0710i4cz09djblp4c0ivr9b6zfymq0l9fj05j2bgk7rhhkncivzj") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.16 (c (n "libe2fs-sys") (v "0.2.16") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1q6xgrx6409dfgkr4giasa97cnjg7v9n851z28gwvgqxlkp0d9yf") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.17 (c (n "libe2fs-sys") (v "0.2.17") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1l2iz00c2zpbyvc8adlch737as678219i3nwm73yzm0cpychkz1j") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.18 (c (n "libe2fs-sys") (v "0.2.18") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1imvjk97g2v5ah5s5sx3zpdp51gwhpyxkp9735fnxd08cfni8a2g") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.19 (c (n "libe2fs-sys") (v "0.2.19") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "07m1a3zgzkx3g2namryssvn2bq2qj440q65wq65dkpd7kv0fpsyh") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.20 (c (n "libe2fs-sys") (v "0.2.20") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "09myhqsn2115h5gci89vwzkg17rli01lijgf2afrbj9mggph58j1") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.21 (c (n "libe2fs-sys") (v "0.2.21") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0rd448jn9w9a4xl3n3plvgm3r5jayhgw94yfpzbpbz7f1a3jmcn3") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.22 (c (n "libe2fs-sys") (v "0.2.22") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1rbhz9y8c16bw5f6557qbzhscykcq0dp42d89d6c5jrz9d5gixpb") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.23 (c (n "libe2fs-sys") (v "0.2.23") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0vsg0h7lhjy37ph6w3gzqcll3lpzfwx358zla6a9w4qw4vll2bx3") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.24 (c (n "libe2fs-sys") (v "0.2.24") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1dr12vdrkak2wg68r6jkm84vh782jxqxmyx00yhchwv693ck6pym") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.25 (c (n "libe2fs-sys") (v "0.2.25") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1hxa5v8p21c2xi7imqa5znl084vdxp9fn1rnc4490z25zms19arx") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.2.26 (c (n "libe2fs-sys") (v "0.2.26") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "198jnirrh5zbz2498mjalyaip4gzmypd9hpgs4ais23im0wsqiay") (l "ext2fs")))

(define-public crate-libe2fs-sys-0.3.0 (c (n "libe2fs-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1l8l16j179ykmiv7k2dp9qy1alg65fmcf7h3r704zj380gb4ifkj") (l "ext2fs")))

