(define-module (crates-io li be libexif-wrapper) #:use-module (crates-io))

(define-public crate-libexif-wrapper-0.1.0 (c (n "libexif-wrapper") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "0ryp8bwpd15ab4g21iy1jb331q3am3y3wwd5l3n39iah592fq2mw") (l "exif")))

(define-public crate-libexif-wrapper-0.1.1 (c (n "libexif-wrapper") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "0g254avb54r71f64y1dxgq94qd3c856kq0vv9ryhjrpyybqhrq10") (l "exif")))

(define-public crate-libexif-wrapper-0.1.2 (c (n "libexif-wrapper") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "0iddgpghaphl1i5k8rxpzzmpw7nci39y5mr0lak73lapjw4nfc1h") (l "exif")))

