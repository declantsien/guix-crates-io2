(define-module (crates-io li be libecdsautil-sys) #:use-module (crates-io))

(define-public crate-libecdsautil-sys-0.1.0 (c (n "libecdsautil-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0lcp1h05y6i4xim1935q821ay97xd0bczl7papdjs3l5xz24dr97") (l "ecdsautil")))

