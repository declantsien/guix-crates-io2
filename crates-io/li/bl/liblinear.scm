(define-module (crates-io li bl liblinear) #:use-module (crates-io))

(define-public crate-liblinear-0.1.0 (c (n "liblinear") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "parsnip") (r "^0.3.0") (d #t) (k 0)))) (h "06ls6nlclx4cqhmq77ygwkfa63zfgmsiq2hgvs9f6d9mrmx7llgn") (l "liblinear.a")))

(define-public crate-liblinear-0.1.1 (c (n "liblinear") (v "0.1.1") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "parsnip") (r "^0.3.0") (d #t) (k 0)))) (h "0q2p9w7ij26vq0g40msjsyvpsvd9q3m9pvqhmkwpn1khl5lqa1bf") (l "liblinear.a")))

(define-public crate-liblinear-1.0.0 (c (n "liblinear") (v "1.0.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "cc") (r "^1.0.46") (d #t) (k 1)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "parsnip") (r "^0.3.0") (d #t) (k 0)))) (h "1kbbq4b1l0p3psm0i71vyvkgyky9i9mscxgm8kkli0qzkkygbkf8") (l "liblinear.a")))

