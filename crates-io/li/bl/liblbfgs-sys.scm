(define-module (crates-io li bl liblbfgs-sys) #:use-module (crates-io))

(define-public crate-liblbfgs-sys-0.1.0 (c (n "liblbfgs-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "04v4xsvfsrisfzlgmjfpic1gwy53azfq5s2x3s3f72jvih0zi0la") (l "lbfgs")))

(define-public crate-liblbfgs-sys-0.1.1 (c (n "liblbfgs-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "18fydpbhsjqwmwpa368pqjzc0y4fwwm06xr17slrf1d611s1sglg") (l "lbfgs")))

(define-public crate-liblbfgs-sys-0.1.2 (c (n "liblbfgs-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "006xis192cwp8rg9rlsqyw2j3rjk7qq0p51g1iqb5c7d0y9wgig5") (l "lbfgs")))

