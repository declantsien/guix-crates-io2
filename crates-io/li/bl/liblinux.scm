(define-module (crates-io li bl liblinux) #:use-module (crates-io))

(define-public crate-liblinux-0.1.0 (c (n "liblinux") (v "0.1.0") (h "1l2j5v49b3lvxk40ks9m8caz8a76ywj116kmkc3nry6v3fx1k5w1")))

(define-public crate-liblinux-0.1.1 (c (n "liblinux") (v "0.1.1") (h "1yg89sgn61gbswhlfrxv0wvw4jja6qk88c5r4fjfaqsjzxx1jjv8")))

(define-public crate-liblinux-0.2.0 (c (n "liblinux") (v "0.2.0") (h "1rsj0gs6936fbnqrf56fl5g6238sx7ij6ai4whav23xx8brd0nq9")))

