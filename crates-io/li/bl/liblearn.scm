(define-module (crates-io li bl liblearn) #:use-module (crates-io))

(define-public crate-liblearn-0.1.0 (c (n "liblearn") (v "0.1.0") (h "0q1d7bg6rzs5r31g41dpvwbwibjifvp66ljm2i76z261nnm70zlf")))

(define-public crate-liblearn-0.1.1 (c (n "liblearn") (v "0.1.1") (h "0jnd96jnx3kdzx3y8ncqrx1xz3g5aj4fmwlcpx9zkdwqg1p1ji60")))

