(define-module (crates-io li bl liblife) #:use-module (crates-io))

(define-public crate-liblife-0.1.0 (c (n "liblife") (v "0.1.0") (d (list (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "1mawyd606d44iqqcrl6aymh1q6xac47xhs9z19whmlwi7zrb15pd")))

(define-public crate-liblife-0.2.0 (c (n "liblife") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)))) (h "0jyzbsk0ciqfvm0igr3sfcvggsyvpz0yg7zghhvpbd5ncf6y8589")))

