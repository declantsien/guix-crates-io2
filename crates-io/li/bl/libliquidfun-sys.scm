(define-module (crates-io li bl libliquidfun-sys) #:use-module (crates-io))

(define-public crate-libliquidfun-sys-0.1.0 (c (n "libliquidfun-sys") (v "0.1.0") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "cxx") (r "^1.0.109") (d #t) (k 0)))) (h "02q6ykjmmiig7fm0mrknf1cs6rzcl3i6ja5lpm69lnna7cmnc8c2")))

(define-public crate-libliquidfun-sys-0.2.0 (c (n "libliquidfun-sys") (v "0.2.0") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "cxx") (r "^1.0.110") (d #t) (k 0)))) (h "1ixwlcqxwnf4jxb945ydfkb9wxcf5k9jgl611vci4ibflpw63v2p")))

(define-public crate-libliquidfun-sys-0.3.0 (c (n "libliquidfun-sys") (v "0.3.0") (d (list (d (n "autocxx") (r "^0.26.0") (d #t) (k 0)) (d (n "autocxx-build") (r "^0.26.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "cxx") (r "^1.0.110") (d #t) (k 0)))) (h "0n235l1is9xd9cpb3x1i884ipxxiql7xy54gjbhinvfbdjkdyd3w")))

