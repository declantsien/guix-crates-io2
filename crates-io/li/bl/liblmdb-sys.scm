(define-module (crates-io li bl liblmdb-sys) #:use-module (crates-io))

(define-public crate-liblmdb-sys-0.0.1 (c (n "liblmdb-sys") (v "0.0.1") (h "05nd57s3a8v4smys184p84hxnswl17siwmlrwkn92p3zrz01swpd")))

(define-public crate-liblmdb-sys-0.0.2 (c (n "liblmdb-sys") (v "0.0.2") (h "1s8fgq26g3p0fhrxi483pbmnx7rk2530qzgdgzaa3qjmhdv2bq5d") (y #t)))

(define-public crate-liblmdb-sys-0.0.3 (c (n "liblmdb-sys") (v "0.0.3") (h "0d5ykiy67k42q9hdjm92yr8w9gnvb3nvggalxlsnfdhpsrps81wa")))

(define-public crate-liblmdb-sys-0.0.4 (c (n "liblmdb-sys") (v "0.0.4") (h "0yvakjfhcrxk1p28fgjsfw4lhymvji6dmxhg9037zdk09hgpq3rs")))

(define-public crate-liblmdb-sys-0.0.5 (c (n "liblmdb-sys") (v "0.0.5") (h "1ji73j8lpwsa530acg80hicfddxdjq28lb9npb0az7mp5027k7za")))

(define-public crate-liblmdb-sys-0.0.7 (c (n "liblmdb-sys") (v "0.0.7") (h "1gmix7xv9rljr90x6kqyvzrcanak25gxkiv3dkxlv9428v7ddnfw")))

(define-public crate-liblmdb-sys-0.1.0 (c (n "liblmdb-sys") (v "0.1.0") (h "1fxddkyl7s2vxgrdsai8i5rad30j28gaja7gwb7cpy9n62zaijlp")))

(define-public crate-liblmdb-sys-0.1.1 (c (n "liblmdb-sys") (v "0.1.1") (h "1h1sgay2cyq5aa0qr3q1hf3spkxq2sgaldi1s49xhhwmd0jkml0y")))

(define-public crate-liblmdb-sys-0.1.2 (c (n "liblmdb-sys") (v "0.1.2") (h "0150hax3k0h5nmhanr8gq20hxalxwr7ckdvhz8ah2jixikhrs980")))

(define-public crate-liblmdb-sys-0.1.3 (c (n "liblmdb-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0195lfa1qcbznayxvhn9y6yvw1sbb3rq1bdrpn6ilmmb554sfiw8")))

(define-public crate-liblmdb-sys-0.1.4 (c (n "liblmdb-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "0qblwis85njgxgjd5iqykg0znqcfplaaq7h7s3dmgrk7dp05pj4b")))

(define-public crate-liblmdb-sys-0.1.5 (c (n "liblmdb-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "01j1qfji05rhasbmwf3kkvb74qhnzj02pwi6nn97cbh9qq59chag")))

(define-public crate-liblmdb-sys-0.1.6 (c (n "liblmdb-sys") (v "0.1.6") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "15rqbigm6lgkp0nrd0g4gwmpvhw0qygv71f6l3rc7hl7di6sf440")))

(define-public crate-liblmdb-sys-0.1.7 (c (n "liblmdb-sys") (v "0.1.7") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1bj782dnqypblhfagycdadpl4ih8qka45sp1vficyzhwsysvkvhf")))

(define-public crate-liblmdb-sys-0.1.8 (c (n "liblmdb-sys") (v "0.1.8") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1w0dpb0fvhscipkjwhddyfppsmzmbmmk334nbl7jyqpsfcxm8si3")))

(define-public crate-liblmdb-sys-0.1.9 (c (n "liblmdb-sys") (v "0.1.9") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "03afhsw7yzgzwxslbv7d4hz16fb9jrlzipl7r5ixlg7kb1znjx9y")))

(define-public crate-liblmdb-sys-0.2.0 (c (n "liblmdb-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qhkm6zwla014q9q2cdv8cyl0izw72c5a2jh35nxbmbbbkh34ih1")))

(define-public crate-liblmdb-sys-0.2.1 (c (n "liblmdb-sys") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xr6b57myck3pfm9nck6pn3h07b7f8ygg3bnkxkvdyc3z7f0fvdm")))

(define-public crate-liblmdb-sys-0.2.2 (c (n "liblmdb-sys") (v "0.2.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j9f9l6fbaaqfbvfmb9rd2b9acqjyh7pn1ma3bv0pxl0lnikivgy")))

