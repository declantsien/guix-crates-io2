(define-module (crates-io li bl liblinkspace) #:use-module (crates-io))

(define-public crate-liblinkspace-0.1.3 (c (n "liblinkspace") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "linkspace-common") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1d7kl1sw7ck7dajyc9bpa8zpb2a8a3zzax7zq8729b8aq8gxnhk4") (y #t)))

(define-public crate-liblinkspace-0.1.0 (c (n "liblinkspace") (v "0.1.0") (h "03dmnnq1c9vfy6i3mam08yy3pc325i2djfxix9n7fbwinypsmq8p") (y #t)))

