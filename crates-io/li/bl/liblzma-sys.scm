(define-module (crates-io li bl liblzma-sys) #:use-module (crates-io))

(define-public crate-liblzma-sys-0.1.20 (c (n "liblzma-sys") (v "0.1.20") (d (list (d (n "cc") (r "^1.0.34") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "117ylvmm7a72lbf5l9dkq34m6jbqj8vy7q466s1j60zdnbzwm4vq") (f (quote (("static")))) (l "lzma")))

(define-public crate-liblzma-sys-0.1.21 (c (n "liblzma-sys") (v "0.1.21") (d (list (d (n "cc") (r "^1.0.34") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0zq65wl80kvfh8csdqf50qn44zkzd7zamfyc14g948wzyi7v1mdy") (f (quote (("static")))) (y #t) (l "lzma")))

(define-public crate-liblzma-sys-0.1.22 (c (n "liblzma-sys") (v "0.1.22") (d (list (d (n "cc") (r "^1.0.34") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0vxl1l1bsfiwdsphpvpdhli6znzkx8gsj1afr2lsn997i4ab9qbj") (f (quote (("threading") ("static") ("default" "threading")))) (l "lzma")))

(define-public crate-liblzma-sys-0.1.23 (c (n "liblzma-sys") (v "0.1.23") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "09gqhhwaiwcmwbxcy21q0xcxrm307iyk2f0a29ad9gajr7bsy4fw") (f (quote (("threading") ("static") ("default" "threading")))) (l "lzma")))

(define-public crate-liblzma-sys-0.1.24 (c (n "liblzma-sys") (v "0.1.24") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0vcdnmnd2wmz5iqp8lr9ra6br1s5b2yhjamsjp2mi6328hlkclnz") (f (quote (("threading" "parallel") ("static") ("parallel") ("default" "parallel") ("bindgen")))) (l "lzma")))

(define-public crate-liblzma-sys-0.2.0 (c (n "liblzma-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0lj7x55q3jn0dd15fhv7i4all3wc1s4v7k3jah22cz69p8bly8pv") (f (quote (("static") ("parallel") ("default" "bindgen") ("bindgen")))) (l "lzma")))

(define-public crate-liblzma-sys-0.2.1 (c (n "liblzma-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1nik77bm919k0113h5r8cdjykrwaws6f3psjx85hlmib40i96big") (f (quote (("wasm" "static" "bindgen") ("static") ("parallel") ("default" "bindgen") ("bindgen")))) (l "lzma")))

(define-public crate-liblzma-sys-0.2.2 (c (n "liblzma-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0108aqr52a0w9l09d5wfy44m2nr11csgddjg4cmjpak9kikv3dl2") (f (quote (("wasm" "static" "bindgen") ("static") ("parallel") ("default" "bindgen") ("bindgen")))) (l "lzma")))

(define-public crate-liblzma-sys-0.2.3 (c (n "liblzma-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0s5whirv1ygad770jg1s7ryxw300r297jgzk04zjs1rv9ar5ni8b") (f (quote (("wasm" "static" "bindgen") ("static") ("parallel") ("default" "bindgen") ("bindgen")))) (l "lzma")))

(define-public crate-liblzma-sys-0.2.4 (c (n "liblzma-sys") (v "0.2.4") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1ilp1k433rsslm1nf2li2h8xxscplfysqgdfph7sssv2gwxx22rl") (f (quote (("wasm" "static" "bindgen") ("static") ("parallel") ("default" "bindgen") ("bindgen")))) (l "lzma")))

(define-public crate-liblzma-sys-0.2.5 (c (n "liblzma-sys") (v "0.2.5") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "15shlsc3smk8riz0bp0k32xlya5n729r0w0mshazdy68z6jsp6my") (f (quote (("wasm" "static" "bindgen") ("static") ("parallel") ("default" "bindgen") ("bindgen")))) (l "lzma")))

(define-public crate-liblzma-sys-0.3.0 (c (n "liblzma-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0sy8n9qkvviwlll5mn204gwwalm790yi60hkgci4h9xb2ap284dc") (f (quote (("wasm" "static" "bindgen") ("static") ("parallel") ("default" "bindgen") ("bindgen")))) (y #t) (l "lzma")))

(define-public crate-liblzma-sys-0.3.1 (c (n "liblzma-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0vfa6l2sxhdmd9x7c6wn4xi3nnrxa86shspn7cybd73p2a4q8680") (f (quote (("wasm" "static" "bindgen") ("static") ("parallel") ("default" "bindgen") ("bindgen")))) (y #t) (l "lzma")))

(define-public crate-liblzma-sys-0.3.2 (c (n "liblzma-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "09pvyzv6njjilwm1v8273sjqdandl68iy5jl472gwl5p5h41q3s8") (f (quote (("wasm" "static" "bindgen") ("static") ("parallel") ("default" "bindgen") ("bindgen")))) (y #t) (l "lzma")))

(define-public crate-liblzma-sys-0.3.3 (c (n "liblzma-sys") (v "0.3.3") (d (list (d (n "cc") (r "^1.0.49") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0jkvskvbx5fvbl605d5msmgqh8als08xvmqy9017y5fv4f5m55r8") (f (quote (("wasm" "static" "bindgen") ("static") ("parallel") ("default" "bindgen") ("bindgen")))) (l "lzma")))

