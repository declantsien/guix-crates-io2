(define-module (crates-io li bl liblinear-rs) #:use-module (crates-io))

(define-public crate-liblinear-rs-0.1.0 (c (n "liblinear-rs") (v "0.1.0") (d (list (d (n "approx") (r "^0.3.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "parsnip") (r "^0.3.0") (d #t) (k 0)))) (h "0xzs1vd59h22gm81hdi3sjry2wjcfg77n81dbp0haax1gbydma52") (y #t) (l "liblinear.a")))

