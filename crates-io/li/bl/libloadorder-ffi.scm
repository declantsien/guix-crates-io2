(define-module (crates-io li bl libloadorder-ffi) #:use-module (crates-io))

(define-public crate-libloadorder-ffi-10.0.0 (c (n "libloadorder-ffi") (v "10.0.0") (d (list (d (n "cbindgen") (r "^0.1.23") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^10.0.0") (d #t) (k 0)))) (h "0vba8fi0z67v2bbcx6pprm4bdj26hm6x4sybz0pp39a9v5mlr912") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-10.0.1 (c (n "libloadorder-ffi") (v "10.0.1") (d (list (d (n "cbindgen") (r "^0.1.23") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^10.0.1") (d #t) (k 0)))) (h "1is9nfsfbqmksfi4l09rphhrvwn5amizrm96xk5dzw4pznavz1f6") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-10.0.2 (c (n "libloadorder-ffi") (v "10.0.2") (d (list (d (n "cbindgen") (r "^0.1.23") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^10.0.2") (d #t) (k 0)))) (h "0avfybw74qlnjh2pp3k914mjaj3q59agdsbmzq9iwi4ycafp81nd") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-10.0.3 (c (n "libloadorder-ffi") (v "10.0.3") (d (list (d (n "cbindgen") (r "^0.1.23") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^10.0.3") (d #t) (k 0)))) (h "1g0hmbfbjbl02cvxlsm5l7dfxl37k688q1fwa461l9cras335ll9") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-10.0.4 (c (n "libloadorder-ffi") (v "10.0.4") (d (list (d (n "cbindgen") (r "^0.1.23") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^10.0.4") (d #t) (k 0)))) (h "1c8cv63797wcnmzi22188igcgys87zi5l22hkplvlmwfr7nmvhgh") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-10.1.0 (c (n "libloadorder-ffi") (v "10.1.0") (d (list (d (n "cbindgen") (r "^0.4.3") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^10.1.0") (d #t) (k 0)))) (h "1vz9x1snf1gg3af6jz9icrl3k0487xiraqgzsv96zj30g1i1js76") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-10.1.1 (c (n "libloadorder-ffi") (v "10.1.1") (d (list (d (n "cbindgen") (r "^0.4.3") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^10.1.1") (d #t) (k 0)))) (h "059zd20p9mwfjwcp5zrrv2mam1id3srgjxyrcc4hya64lds5hmci") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-11.0.0 (c (n "libloadorder-ffi") (v "11.0.0") (d (list (d (n "cbindgen") (r "^0.4.3") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^11.0.0") (d #t) (k 0)))) (h "06jgfikf6g01s5s6dql9x9yfv0ybllfk3q1i42iw8czq7csj2gkz") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-11.0.1 (c (n "libloadorder-ffi") (v "11.0.1") (d (list (d (n "cbindgen") (r "^0.4.3") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^11.0.1") (d #t) (k 0)))) (h "0cl05xlb40mgkyd7sdn9mdr3k1kmq7dbi4924x6sfwyar5zrkzf9") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-11.0.2 (c (n "libloadorder-ffi") (v "11.0.2") (d (list (d (n "cbindgen") (r "^0.5.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^11.0.2") (d #t) (k 0)))) (h "032kw05bkzfg1r8lbvz83kkb4ijl7dl2284f7x18xshbp53w85m4") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-11.1.0 (c (n "libloadorder-ffi") (v "11.1.0") (d (list (d (n "cbindgen") (r "^0.5.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^11.1.0") (d #t) (k 0)))) (h "0vcq9l0sfcll1shvfppyhn0qy0s5clmqpdka5n3lslzdjblifayv") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-11.2.0 (c (n "libloadorder-ffi") (v "11.2.0") (d (list (d (n "cbindgen") (r "^0.5.2") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^11.2.0") (d #t) (k 0)))) (h "1bhdjgzjlg9w7nd2v71hk8libkdxf2qi1q91s382ay0540qxlkak") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-11.2.1 (c (n "libloadorder-ffi") (v "11.2.1") (d (list (d (n "cbindgen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^11.2.1") (d #t) (k 0)))) (h "1bcbfarl3cpnydxchr668cyqqv9x92ylmdhf7n14j29zyf5irzxd") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-11.2.2 (c (n "libloadorder-ffi") (v "11.2.2") (d (list (d (n "cbindgen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^11.2.2") (d #t) (k 0)))) (h "0y0mfq8bxrry3daa07m0qnwls34kc58vnnj11lgzxxahyzzrpww6") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-11.3.0 (c (n "libloadorder-ffi") (v "11.3.0") (d (list (d (n "cbindgen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^11.2.3") (d #t) (k 0)))) (h "1mzhzlfb6lk97qc99k9g0b3iqw6dnq5fcpv86psi57anrwy0nrvi") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-11.4.0 (c (n "libloadorder-ffi") (v "11.4.0") (d (list (d (n "cbindgen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^11.4.0") (d #t) (k 0)))) (h "1xlhlyh0kca57h5rpfn5cnq0dpv308zilk4rdw99z6c7b0s8hgsn") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

(define-public crate-libloadorder-ffi-11.4.1 (c (n "libloadorder-ffi") (v "11.4.1") (d (list (d (n "cbindgen") (r "^0.6") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloadorder") (r "^11.4.1") (d #t) (k 0)))) (h "0m2n10fg1jvfq4crabgbxs42jzl55a5pvhfvy0znqnf4k6mxp973") (f (quote (("ffi-headers" "cbindgen") ("default"))))))

