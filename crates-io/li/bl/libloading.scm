(define-module (crates-io li bl libloading) #:use-module (crates-io))

(define-public crate-libloading-0.2.0 (c (n "libloading") (v "0.2.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "075xhm0pc7fjcfzxsq4lpqfzbgpl7qph6rb1fcaw5hszmyh1xqg2")))

(define-public crate-libloading-0.2.1 (c (n "libloading") (v "0.2.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "17wv32kkvdpnsvicc6dg6i7chsbp6rjka5ys4fnw78bsiykrdwlv")))

(define-public crate-libloading-0.2.2 (c (n "libloading") (v "0.2.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "002hn5nrlip35js6blwb0am0bza38wkj3ycl5q2ciw4a840c78nz")))

(define-public crate-libloading-0.2.3 (c (n "libloading") (v "0.2.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "target_build_utils") (r "^0.1") (d #t) (k 1)) (d (n "winapi") (r "^0.2") (d #t) (k 0)))) (h "1w05s4m096jxyk92xdrc9nsqxmd2nzw7f54h4s88qrxix841757c")))

(define-public crate-libloading-0.2.4 (c (n "libloading") (v "0.2.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "target_build_utils") (r "^0.1") (d #t) (k 1)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1wk5sl27gg7693rj49rj1aky6h3y6ygql134jzqwf9wsxqvjdszc")))

(define-public crate-libloading-0.3.0 (c (n "libloading") (v "0.3.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "target_build_utils") (r "^0.1") (d #t) (k 1)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1hqg20cqwxmdy8n5pgldk2gfa0vnyhkhn15511xg71ybhg9c6s90")))

(define-public crate-libloading-0.3.1 (c (n "libloading") (v "0.3.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "target_build_utils") (r "^0.1") (d #t) (k 1)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "16fb6s507pxyljpwg93px9q34wn66nfv1lmx1pz3s5nqds66m0c4")))

(define-public crate-libloading-0.3.2 (c (n "libloading") (v "0.3.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "target_build_utils") (r "^0.3") (d #t) (k 1)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "13wxy6qhhj43474hiqhl4q74hsk84cy4k4x4ffh7pxn12jkka67x")))

(define-public crate-libloading-0.3.3 (c (n "libloading") (v "0.3.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "target_build_utils") (r "^0.3") (d #t) (k 1)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "15awjpbz897dv6nc6akj3gph945l8jdvqg0arkrxk66ar35kq5wp") (y #t)))

(define-public crate-libloading-0.3.4 (c (n "libloading") (v "0.3.4") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "target_build_utils") (r "^0.3") (d #t) (k 1)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0risz19rllhdc0d7nkpwkf4pcbjjgg1iim0kkmzb6kkp874hl0ha")))

(define-public crate-libloading-0.4.0 (c (n "libloading") (v "0.4.0") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "152ipp7kl8l7g7nzd5d6qm7nvfwjjs82qnbag0x51sdkpqagi6dy")))

(define-public crate-libloading-0.4.1 (c (n "libloading") (v "0.4.1") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1ii344cq4x70v96s8mi554dpjxyh1chgsh4cgdf7ljqmp4n9nsbb")))

(define-public crate-libloading-0.4.2 (c (n "libloading") (v "0.4.2") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "071vprpivk8rq9hq5yp7nr1p453pspqa7yzm07msx9sbk9m954iz")))

(define-public crate-libloading-0.4.3 (c (n "libloading") (v "0.4.3") (d (list (d (n "kernel32-sys") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1fbr96fpzzqxqwzpl74d8ami38vdpga4c1fk2w65v5ppx0yhff7x")))

(define-public crate-libloading-0.5.0 (c (n "libloading") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1hl8bgw54qc9ffg1fykkj9km5wgin2bhilc3rli5i36bsxhdcflw")))

(define-public crate-libloading-0.5.1 (c (n "libloading") (v "0.5.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0682nn0g7zias4dcgsrk0vlrpvlav70fbcqq85yf48qqnn12ysd5")))

(define-public crate-libloading-0.5.2 (c (n "libloading") (v "0.5.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0lyply8rcqc8agajzxs7bq6ivba9dnn1i68kgb9z2flnfjh13cgj")))

(define-public crate-libloading-0.6.0 (c (n "libloading") (v "0.6.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1i7dxflzjdxwpqznhyxzb7vnnn1x7w2k7hv574kz0mxlzwcrm5rc")))

(define-public crate-libloading-0.6.1 (c (n "libloading") (v "0.6.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1c00fq0hnfbx8v0ilhx5901ycbkv3nw4pjacrddbdggmj2vm2krw")))

(define-public crate-libloading-0.6.2 (c (n "libloading") (v "0.6.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "07671b8b8s7w7kj65cja3l31gkdl1r9cfy2wy1fw8w7hd7kvib9c")))

(define-public crate-libloading-0.6.3 (c (n "libloading") (v "0.6.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1ygliqa518jjxwa5ih4b2f8m984ib596vxmjb28pa5lb8zqdhhr4")))

(define-public crate-libloading-0.6.4 (c (n "libloading") (v "0.6.4") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "188pjyhchf2m9a92ggqndilm1s7l5v3a9lww2dnpyxbz9wwcjmrm")))

(define-public crate-libloading-0.6.5 (c (n "libloading") (v "0.6.5") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1c7xhvvf7mzxyyd1gsq0byj72znrafa1v2531n6n9hkfw07hi40h")))

(define-public crate-libloading-0.6.6 (c (n "libloading") (v "0.6.6") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1g0nkdbf3mk34bn80aanj3d48d18f2jzfrsqi77y7drnm3gpndp9")))

(define-public crate-libloading-0.6.7 (c (n "libloading") (v "0.6.7") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "10wq4a4jkman8k6y0v2cw3d38y1h3rw6d2in5klggm8jg90k46im")))

(define-public crate-libloading-0.7.0 (c (n "libloading") (v "0.7.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sidr67nsa693mqrqgk2np3bkqni0778yk147xncspy171jdk13g")))

(define-public crate-libloading-0.7.1 (c (n "libloading") (v "0.7.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1w3wc1y2z9z7w1xmf7xwdsjy9cvnrqzppqadblyflas02mnh7ky0")))

(define-public crate-libloading-0.7.2 (c (n "libloading") (v "0.7.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ljbc0pmxq4y0x393cn10f92xd5pcddax6v151qrp5zcd7b07qmg") (r "1.40.0")))

(define-public crate-libloading-0.7.3 (c (n "libloading") (v "0.7.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1pfdmf0scimadifda0wyg9swalr2pahwd5fjmvvfjxd7z41hzg7g") (r "1.40.0")))

(define-public crate-libloading-0.7.4 (c (n "libloading") (v "0.7.4") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "winapi") (r "^0.3") (f (quote ("errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "17wbccnjvhjd9ibh019xcd8kjvqws8lqgq86lqkpbgig7gyq0wxn") (r "1.40.0")))

(define-public crate-libloading-0.8.0 (c (n "libloading") (v "0.8.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_LibraryLoader"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1jyq4bzq1n3c7rmblcwnd0xvxr553zmrikr83ch0arbpjn7k306m") (r "1.48.0")))

(define-public crate-libloading-0.8.1 (c (n "libloading") (v "0.8.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "windows-sys") (r "^0.48") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_LibraryLoader"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0q812zvfag4m803ak640znl6cf8ngdd0ilzky498r6pwvmvbcwf5") (r "1.48.0")))

(define-public crate-libloading-0.8.2 (c (n "libloading") (v "0.8.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation"))) (d #t) (k 2)) (d (n "windows-targets") (r ">=0.48, <0.53") (d #t) (t "cfg(windows)") (k 0)))) (h "0r016gr85prhr9plcs1y89d4s76jc97pvkk04xjs5wzrigxmmaic") (r "1.56.0")))

(define-public crate-libloading-0.8.3 (c (n "libloading") (v "0.8.3") (d (list (d (n "cfg-if") (r "^1") (d #t) (t "cfg(unix)") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "static_assertions") (r "^1.1") (d #t) (k 2)) (d (n "windows-sys") (r "^0.52") (f (quote ("Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 2)) (d (n "windows-targets") (r ">=0.48, <0.53") (d #t) (t "cfg(windows)") (k 0)))) (h "06awqx9glr3i7mcs6csscr8d6dbd9rrk6yglilmdmsmhns7ijahc") (r "1.56.0")))

