(define-module (crates-io li bl liblbfgs) #:use-module (crates-io))

(define-public crate-liblbfgs-0.0.15 (c (n "liblbfgs") (v "0.0.15") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vecfx") (r "^0.0.8") (d #t) (k 2)))) (h "0bigknv2a0mjh1fvai1hm7fp15fyldxxc3xyclayj41dv0cch0nh")))

(define-public crate-liblbfgs-0.0.16 (c (n "liblbfgs") (v "0.0.16") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "approx") (r "^0.3") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vecfx") (r "^0.1") (d #t) (k 2)))) (h "12bwdkinh0hiykvgxphvwfisbp03xb1pc5nm4zbz3glzp21ilxm4")))

(define-public crate-liblbfgs-0.1.0 (c (n "liblbfgs") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "vecfx") (r "^0.1") (d #t) (k 2)))) (h "1p9gy672g54dhliyk41dlzsx508phhqzafbbfmj7sm9602dh070z")))

