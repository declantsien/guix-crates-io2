(define-module (crates-io li bl liblmod) #:use-module (crates-io))

(define-public crate-liblmod-0.1.1 (c (n "liblmod") (v "0.1.1") (h "04a7j6waxafwak0afjn4zxnkkr002wbl4h3k634sv5h7pcxrh9r2")))

(define-public crate-liblmod-0.2.0 (c (n "liblmod") (v "0.2.0") (h "0z12mcbz9g7lrg2w5a83nwiw9mfw1ssibqb80ywbalq07xfmh8dj")))

