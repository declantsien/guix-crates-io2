(define-module (crates-io li bl liblet) #:use-module (crates-io))

(define-public crate-liblet-0.1.0 (c (n "liblet") (v "0.1.0") (d (list (d (n "itertools") (r "^0.9.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "1cdg7326ljymxb816ihf67p4n9gmblapn59dcwnm6m8imm5grgdh")))

(define-public crate-liblet-0.1.1 (c (n "liblet") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "0lini0iwdy02slf5p79xmha7m97hyi1cy6fzfnjr0ksgz70lmn06")))

(define-public crate-liblet-0.1.2 (c (n "liblet") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "0j6sa3rkpqvdahkq5a0ivpr5gljbbyq3qy0gsy8nz4p903hqf2kq")))

(define-public crate-liblet-0.2.0 (c (n "liblet") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.0") (d #t) (k 2)))) (h "0mmc7l8fly2mv5kw91mdh6357q9dpri704mdcq8ad3ldiym6a5hq")))

