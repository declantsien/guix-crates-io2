(define-module (crates-io li bl libloc) #:use-module (crates-io))

(define-public crate-libloc-0.1.0 (c (n "libloc") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "ipnet") (r "^2.9.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.153") (d #t) (k 2)) (d (n "memmap2") (r "^0.9.4") (f (quote ("stable_deref_trait"))) (d #t) (k 0)) (d (n "yoke") (r "^0.7.3") (d #t) (k 0)) (d (n "yoke-derive") (r "^0.7.3") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.32") (d #t) (k 0)) (d (n "zerocopy-derive") (r "^0.7.32") (d #t) (k 0)))) (h "0icipkw3ggbvpfgbay1260dz3ay78v7pdf1i01v0ays5w714swd2")))

