(define-module (crates-io li bl liblapack-sys) #:use-module (crates-io))

(define-public crate-liblapack-sys-0.0.1 (c (n "liblapack-sys") (v "0.0.1") (h "1641gcwbjba7q8wiy6fgrdkm8l8vmj2czb54v97zvf8v1yz9i5wc")))

(define-public crate-liblapack-sys-0.0.2 (c (n "liblapack-sys") (v "0.0.2") (d (list (d (n "libblas-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1p1v2h3air030s00a3ddy24728lx9wd1x15i3wmbqfr018nvvgsl")))

(define-public crate-liblapack-sys-0.0.3 (c (n "liblapack-sys") (v "0.0.3") (d (list (d (n "libblas-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0v8b21mkgnkgf0pc1apbnpbc8x500m9z9gjiijzima1fsypwj79f")))

(define-public crate-liblapack-sys-0.0.4 (c (n "liblapack-sys") (v "0.0.4") (d (list (d (n "libblas-sys") (r "^0.0.4") (d #t) (k 0)))) (h "17xwn6q80i3qiayxf4z7hamrz84ybf3qk7r24d3aqplq5wb7hjhd")))

(define-public crate-liblapack-sys-0.0.5 (c (n "liblapack-sys") (v "0.0.5") (d (list (d (n "libblas-sys") (r "^0.0.5") (d #t) (k 0)))) (h "19h84lvlmsas2cr2wdjmvx9wkyp22qnq29zzrjn5yhndi5rgz3d8")))

(define-public crate-liblapack-sys-0.0.6 (c (n "liblapack-sys") (v "0.0.6") (d (list (d (n "libblas-sys") (r "^0.0.6") (d #t) (k 0)))) (h "05dafzr0n5y1rvd0al5napjmp5i8bfg3zsg5b3bl45rkm14xqbs3")))

(define-public crate-liblapack-sys-0.0.7 (c (n "liblapack-sys") (v "0.0.7") (d (list (d (n "libblas-sys") (r "^0.0.7") (d #t) (k 0)))) (h "1ljnkdsdzjqb3p9pdb61rcjgjnhwrw9vdv5a07kc4z6vfj2n9vml")))

(define-public crate-liblapack-sys-0.0.8 (c (n "liblapack-sys") (v "0.0.8") (d (list (d (n "libblas-sys") (r "^0.0.8") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "09bj5h48ngsjxp0cfak85l8249x5pb7z9ixnfpylhcmhplxsvg92")))

(define-public crate-liblapack-sys-0.0.12 (c (n "liblapack-sys") (v "0.0.12") (d (list (d (n "libblas-sys") (r "^0.0.12") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0x4b8pmsj5k34a7pxfny669p2b9q6w278c9csdmhbjwmjp6akysv")))

(define-public crate-liblapack-sys-0.0.13 (c (n "liblapack-sys") (v "0.0.13") (d (list (d (n "libblas-sys") (r "^0.0.13") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0alrzgc1i957p014vf7z9mary20gxdrizqk64396b6q68d9i29x1")))

(define-public crate-liblapack-sys-0.0.14 (c (n "liblapack-sys") (v "0.0.14") (d (list (d (n "libblas-sys") (r "^0.0.14") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0yr5dlya9cvdab88cwlj7idd28ag5yhjdkif37r956w6slahsb0m")))

(define-public crate-liblapack-sys-0.0.15 (c (n "liblapack-sys") (v "0.0.15") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "netlib-blas-provider") (r "*") (o #t) (d #t) (k 0)) (d (n "openblas-blas-provider") (r "*") (o #t) (d #t) (k 0)))) (h "0ikgwg6vcyjjypz9g5wfrknjv9hy0gscy1c1n2szgplxpdpj0bzg") (f (quote (("openblas" "openblas-blas-provider") ("netlib" "netlib-blas-provider") ("default" "openblas"))))))

(define-public crate-liblapack-sys-0.0.16 (c (n "liblapack-sys") (v "0.0.16") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "netlib-blas-provider") (r "*") (o #t) (d #t) (k 0)) (d (n "openblas-blas-provider") (r "*") (o #t) (d #t) (k 0)))) (h "07x9ylj54nhcqbanb5m7gdpmvxrmyx4vda4z5jnawp3v5wsk9fdq") (f (quote (("openblas" "openblas-blas-provider") ("netlib" "netlib-blas-provider") ("default" "openblas"))))))

(define-public crate-liblapack-sys-0.0.17 (c (n "liblapack-sys") (v "0.0.17") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "netlib-provider") (r "*") (o #t) (d #t) (k 0)) (d (n "openblas-provider") (r "*") (o #t) (d #t) (k 0)))) (h "0p6ajkvvdzrvi74j2ndqzwsjiylshnyxva2l3kh2ic35qw8maxm1") (f (quote (("openblas" "openblas-provider") ("netlib" "netlib-provider") ("default" "openblas"))))))

(define-public crate-liblapack-sys-0.1.0 (c (n "liblapack-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "netlib-provider") (r "*") (o #t) (d #t) (k 0)) (d (n "openblas-provider") (r "*") (o #t) (d #t) (k 0)))) (h "1vkdfrlif7f526c0ccsppi6qqv0760jbkgxmpds21b9225wj9ahg") (f (quote (("openblas" "openblas-provider") ("netlib" "netlib-provider") ("default" "openblas"))))))

(define-public crate-liblapack-sys-0.2.0 (c (n "liblapack-sys") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "netlib-provider") (r "*") (o #t) (d #t) (k 0)) (d (n "openblas-provider") (r "*") (o #t) (d #t) (k 0)))) (h "1hpyb5x0fqd9sdlzi76hrzfy0vly6z39pzrznd340iq1myznwzyr") (f (quote (("openblas" "openblas-provider") ("netlib" "netlib-provider") ("default" "openblas"))))))

