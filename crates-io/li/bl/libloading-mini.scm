(define-module (crates-io li bl libloading-mini) #:use-module (crates-io))

(define-public crate-libloading-mini-0.1.0 (c (n "libloading-mini") (v "0.1.0") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0maq311h3zclsgv1n4y4h7jrq1mkg7g6130f27rxdic48aqd2in5")))

(define-public crate-libloading-mini-0.1.1 (c (n "libloading-mini") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0dijj92bgcslwwizgwkc79xcvj8qwpd1k02nqn4pkml6mm2jca77") (f (quote (("default") ("debug_symbols"))))))

(define-public crate-libloading-mini-0.1.2 (c (n "libloading-mini") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("winerror" "errhandlingapi" "libloaderapi"))) (t "cfg(windows)") (k 0)))) (h "0yzg25l5fc74n7ppzwmq3i3l0ian9dcc8ff6rq9znnviq0s9j819") (f (quote (("default") ("debug_symbols"))))))

