(define-module (crates-io li bl libloader) #:use-module (crates-io))

(define-public crate-libloader-0.1.0 (c (n "libloader") (v "0.1.0") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1qk84iqcwcg7fi84fkid2zfw7gsxlvv6f94fn2195hq22xhc5r0i")))

(define-public crate-libloader-0.1.1 (c (n "libloader") (v "0.1.1") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "1c68fchim70v2z774v7s2m363j0b8a6h65malba6dprs5910yjgs")))

(define-public crate-libloader-0.1.2 (c (n "libloader") (v "0.1.2") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0d7si9qpwn6vymhb4x5khwm81wm547l9fgf17j9dmfvag47cm3iw")))

(define-public crate-libloader-0.1.3 (c (n "libloader") (v "0.1.3") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "099abidb791w0ngd0xwgha19kk2y25ggg9gpgwzj1mmxrlck7i0s")))

(define-public crate-libloader-0.1.4 (c (n "libloader") (v "0.1.4") (d (list (d (n "libloading") (r "^0.7") (d #t) (k 0)))) (h "0ww06aq4qadsiap7j40k1v6v36cpiiz28gbsjdrvjvh3ccyyffad")))

