(define-module (crates-io li tu litua) #:use-module (crates-io))

(define-public crate-litua-1.0.0 (c (n "litua") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mlua") (r "^0.7") (f (quote ("lua54" "vendored"))) (d #t) (k 0)))) (h "00f4k0r5mh9lkahxj19fimkwlndqgs5akzh2m6pd72ki47mcil3v")))

(define-public crate-litua-1.1.0 (c (n "litua") (v "1.1.0") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mlua") (r "^0.7") (f (quote ("lua54" "vendored"))) (d #t) (k 0)))) (h "1mv00x8i55aldwkzb679455234p2k6mi5ny68z4bpijqybda6k80")))

(define-public crate-litua-1.1.1 (c (n "litua") (v "1.1.1") (d (list (d (n "clap") (r "^4.0.27") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mlua") (r "^0.7") (f (quote ("lua54" "vendored"))) (d #t) (k 0)))) (h "165wks37yr3gxvxcz8m7f6bvyyrmi296nv0804awf3kcm51randh")))

(define-public crate-litua-2.0.0 (c (n "litua") (v "2.0.0") (d (list (d (n "clap") (r "^4.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "mlua") (r "^0.8.8") (f (quote ("lua54" "vendored"))) (d #t) (k 0)))) (h "0vzklycas43b81zm4vpc84i234hyiw1i528bwa05mdiqfl0x8n6d")))

