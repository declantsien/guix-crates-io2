(define-module (crates-io li tu liturgical) #:use-module (crates-io))

(define-public crate-liturgical-0.1.0 (c (n "liturgical") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0lkqxm8k9lk0rdi00kyn7bzxfcc0m69bs8xjrx554kxflkb9vhi6")))

(define-public crate-liturgical-0.2.0 (c (n "liturgical") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1ncgw24f3dpd99y4ddg4yrizrg1imw4k3qvrq5lyawvi4lk9r6qa")))

