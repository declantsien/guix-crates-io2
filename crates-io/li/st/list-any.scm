(define-module (crates-io li st list-any) #:use-module (crates-io))

(define-public crate-list-any-0.1.0 (c (n "list-any") (v "0.1.0") (h "0gyhv9rsrsyqm1a969q8vhl9j4ya7hxzzz0g14md7llvgv8q131y") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-list-any-0.2.0 (c (n "list-any") (v "0.2.0") (h "0761x55am79niik8fmvnbfg6im12d4dp89xxgd6rxzff3w8ilvmv") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-list-any-0.2.1 (c (n "list-any") (v "0.2.1") (h "0rb1rbvk48351kgxcj7q8c3dhj2mdzsy8jl84bnnpiwcaaqd3lck") (f (quote (("default" "alloc") ("alloc"))))))

