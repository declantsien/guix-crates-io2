(define-module (crates-io li st list_builder) #:use-module (crates-io))

(define-public crate-list_builder-0.0.1 (c (n "list_builder") (v "0.0.1") (h "10cv038llch1ab9xhnrm7q5clw4swldzsrvjrd17wyk59pd6ascc")))

(define-public crate-list_builder-0.0.2 (c (n "list_builder") (v "0.0.2") (h "13sng0vz8d7hkrnmgvhq3d3z9zl6akr7hk3na7ziww1wcx5fcbhq")))

