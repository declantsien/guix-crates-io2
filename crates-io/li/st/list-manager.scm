(define-module (crates-io li st list-manager) #:use-module (crates-io))

(define-public crate-list-manager-0.1.0 (c (n "list-manager") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30.0") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (d #t) (k 0)) (d (n "users") (r "^0.11.0") (d #t) (k 0)))) (h "07v9vqb7q96d036pg286h57pgb1s5z6j2pvb5shxx7yxdfc12xjn")))

