(define-module (crates-io li st lists) #:use-module (crates-io))

(define-public crate-lists-1.0.0 (c (n "lists") (v "1.0.0") (h "0sy9mfji6kyf3wqir3lbp388wq4y1v3znj6a7b667kkhnbfb1yyz") (y #t)))

(define-public crate-lists-1.0.1 (c (n "lists") (v "1.0.1") (h "053b8ji16y637qgibvx5glgkc77s794nzrgxn5z1zvnsdl2gfdl6") (y #t)))

(define-public crate-lists-1.0.2 (c (n "lists") (v "1.0.2") (h "0lz9nk6d1zv863dszfh1zjk0x7lhg6fwv3iqpnac0hlm0sh4asna") (y #t)))

(define-public crate-lists-1.0.3 (c (n "lists") (v "1.0.3") (h "1jsy59fj069xxh5nlmn72ajngirbf3fy6gfqryk2f747c8xaasrk") (y #t)))

(define-public crate-lists-1.0.4 (c (n "lists") (v "1.0.4") (h "11cz1zad5vw6rdyxnh53l6slv7vancfvdissz9h3l55c3syaswsr") (y #t)))

(define-public crate-lists-1.1.0 (c (n "lists") (v "1.1.0") (h "1ww1wnc540blm4g7npbqvqc4rpvra12nvs1dhqcyhy33nf9smxgv") (y #t)))

(define-public crate-lists-1.2.0 (c (n "lists") (v "1.2.0") (h "1fdvvk7vkb90pwv6pj9f5fa7c6fr971dr4lrr23m59x5mcq36fm4") (y #t)))

(define-public crate-lists-1.3.0 (c (n "lists") (v "1.3.0") (h "0afra7xgif84c23drzgr3inxh8yi8gi89fdlzajy7qkzjf20k7z3") (y #t)))

(define-public crate-lists-1.4.0 (c (n "lists") (v "1.4.0") (h "1nkqr2rbxx2jnm4qadnr1ywj0f1kd34zgqrcsp2yma40gp502kj7") (y #t)))

(define-public crate-lists-1.5.0 (c (n "lists") (v "1.5.0") (h "0i9kjrnvnzlj50zpvl2z2najnmggi0z2jy8jsj7zc3ciavkwkl4s") (y #t)))

(define-public crate-lists-1.5.1 (c (n "lists") (v "1.5.1") (h "1d3hq9cjn3w12rwcdfgjfns64ixpcvj7q9iihkk97plgvda5myc5") (y #t)))

(define-public crate-lists-2.0.0 (c (n "lists") (v "2.0.0") (h "0myqhjjkikdgah83h68zbzi5zddqpckk6dcjg7jhgbxp9g9ls2r4") (y #t)))

(define-public crate-lists-2.1.0 (c (n "lists") (v "2.1.0") (h "1wvlpy89kns872rj6ck6csnvwc1zx4rn7z0ll5ywfi3a4fmgdl5r") (y #t)))

(define-public crate-lists-2.2.0 (c (n "lists") (v "2.2.0") (h "12hpjdfdi8x811sp9f53lxhm7a9kz2j2n8mfz7rb1an8vbl5k230") (y #t)))

(define-public crate-lists-2.2.1 (c (n "lists") (v "2.2.1") (h "0v7k7pca0vixfn60zbbswn0ds3my2qw2zywz3fs7nrjigwpwdnpz") (y #t)))

(define-public crate-lists-2.3.0 (c (n "lists") (v "2.3.0") (h "0kr1pb8ffrzgh8z8xd4yjhy5bjk5g3056gvmhdafhj1khll6ggi7") (y #t)))

