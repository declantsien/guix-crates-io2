(define-module (crates-io li st list-modules) #:use-module (crates-io))

(define-public crate-list-modules-0.1.0 (c (n "list-modules") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "008xprjp9a5ksqwwlcmf9p1yi3mc08lznhyar7vc43hb815235b9") (y #t)))

(define-public crate-list-modules-0.1.1 (c (n "list-modules") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "037pf57phzdzgawrnfxk8a3j3ln4fkkg5zdxa6mazahip06jfy3g") (y #t)))

(define-public crate-list-modules-0.1.2 (c (n "list-modules") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0f15alsr267mpra0n8g8wwby84c8c1ndcwkayn91bb564arc8plx") (y #t)))

(define-public crate-list-modules-0.1.3 (c (n "list-modules") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0yml2lbj2qyfkgkr0k3vz05pjkmmbmjlgdv5xyrwzyi74k9bnarm")))

(define-public crate-list-modules-0.1.4 (c (n "list-modules") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "08il0lb4w7l2lsxli148rd1s41r51qxryrifv6fi83kwjpv2q3y4")))

(define-public crate-list-modules-0.1.5 (c (n "list-modules") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1bz4ir1hyq3jfm9s897c5jjrp4z8vaihfz1v3ajpar2lhlcdakm7")))

(define-public crate-list-modules-0.1.6 (c (n "list-modules") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0rhs7m1b6mryqgy3295pkx1dxysykhzv64cw0im9sby8dn2l3ymz")))

