(define-module (crates-io li st listenfd) #:use-module (crates-io))

(define-public crate-listenfd-0.1.0 (c (n "listenfd") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0fmg1l2dbp7wfcz1pqdsp0qk1fhvb052h1mkxpsfh2qnwxjy0b3p")))

(define-public crate-listenfd-0.1.1 (c (n "listenfd") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1d1a0bindr59bli85gfyk25xac7c8ii6b7g7yiac0gwmxgb863gc")))

(define-public crate-listenfd-0.2.0 (c (n "listenfd") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1jzaz6x10slc308njvzb4l0xbzalqzbkqlaiax26l2xwsj40p6ja")))

(define-public crate-listenfd-0.3.0 (c (n "listenfd") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.6.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("winsock2" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "09gl0ifmi0jbq9d6wrx2rp85sa1x76nlx0v0dya2q5a8fl1d8ipb")))

(define-public crate-listenfd-0.3.1 (c (n "listenfd") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.6.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("winsock2" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1147fxx303cvrbxvvvg065f5jrfqdja5q81r9iar4l9lprh5m9mc")))

(define-public crate-listenfd-0.3.2 (c (n "listenfd") (v "0.3.2") (d (list (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.6.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("winsock2" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0c61ggx00c1kglq9agnq34g0dza5pb6vm21lhkwzxyn6l53fgan4") (y #t)))

(define-public crate-listenfd-0.3.3 (c (n "listenfd") (v "0.3.3") (d (list (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.6.3") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("winsock2" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "188cprmlnqmgwy7kfxwqsp9jrqapgr1a5w4jqn0xxqpj6bkmh8a9")))

(define-public crate-listenfd-0.3.4 (c (n "listenfd") (v "0.3.4") (d (list (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("winsock2" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1n7b43yryxv5p1i4npdc29mnnza56pif103061qziwghhg187bbj")))

(define-public crate-listenfd-0.3.5 (c (n "listenfd") (v "0.3.5") (d (list (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^0.8.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("winsock2" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "19vrdqv1qg47gm9kwd4ffxv6sxn1jkka6vil053n5adq5i7537l0")))

(define-public crate-listenfd-0.4.0 (c (n "listenfd") (v "0.4.0") (d (list (d (n "hyper") (r "^0.14.16") (d #t) (k 2)) (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "warp") (r "^0.3.2") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("winsock2" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0apj7d29wmxncc4nqra35q8ywyh471ad9pyqzr61w5fvl56wl1gy")))

(define-public crate-listenfd-0.5.0 (c (n "listenfd") (v "0.5.0") (d (list (d (n "hyper") (r "^0.14.16") (d #t) (k 2)) (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^0.8.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "warp") (r "^0.3.2") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("winsock2" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0m5s8adggcr56dziswlwhm63bqrjxyh4mcxis0phhpwzbpri8ay0")))

(define-public crate-listenfd-1.0.0 (c (n "listenfd") (v "1.0.0") (d (list (d (n "hyper") (r "^0.14.16") (d #t) (k 2)) (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "uuid") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "warp") (r "^0.3.2") (d #t) (k 2)) (d (n "winapi") (r "^0.3.4") (f (quote ("winsock2" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1219y7fnvg73883j6c8z8s1ji9mx8cgyf5hfnya1swzn1z0grr0l")))

(define-public crate-listenfd-1.0.1 (c (n "listenfd") (v "1.0.1") (d (list (d (n "hyper") (r "^0.14.16") (d #t) (k 2)) (d (n "tokio") (r "^1.15.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "warp") (r "^0.3.2") (d #t) (k 2)) (d (n "libc") (r "^0.2.40") (d #t) (t "cfg(not(windows))") (k 0)) (d (n "uuid") (r "^1.0.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "winapi") (r "^0.3.4") (f (quote ("winsock2" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "15jc2xgrp4j6508s5f9d9gq6w1ssgv2mvc5b3795jqnrmiih8l70")))

