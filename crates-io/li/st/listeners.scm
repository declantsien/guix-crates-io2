(define-module (crates-io li st listeners) #:use-module (crates-io))

(define-public crate-listeners-0.1.0 (c (n "listeners") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rustix") (r "^0.38.31") (f (quote ("fs"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_ToolHelp"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0075nb2wdblc4qcl8032zqckavh6g2llxkd7d5qbfwxl645nlbwn")))

(define-public crate-listeners-0.2.0 (c (n "listeners") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.5.0") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "rustix") (r "^0.38.31") (f (quote ("fs"))) (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "windows") (r "^0.54.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_ToolHelp"))) (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1x4h78a0q5slqh6f22h3bx56wk2cibcs3inydxn8n8qnx96r2fas")))

