(define-module (crates-io li st listload) #:use-module (crates-io))

(define-public crate-listload-0.1.0 (c (n "listload") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("cargo" "derive" "env" "wrap_help"))) (d #t) (k 0)) (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "downloader") (r "^0.2.7") (f (quote ("rustls-tls"))) (k 0)) (d (n "indoc") (r "^2.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.99") (d #t) (k 0)) (d (n "toml") (r "^0.7.5") (d #t) (k 0)) (d (n "url") (r "^2.4.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "0h18hdv28hz24i7c7j8fdha0z0wk90nibd76y5169bkbf5x6aaqn")))

