(define-module (crates-io li st list_flattener) #:use-module (crates-io))

(define-public crate-list_flattener-0.1.0 (c (n "list_flattener") (v "0.1.0") (d (list (d (n "parse_int") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "03gsm8dyw28sqn5ajq0rpgxx52k0d56nxgj5i7i09far3f1h4sjw")))

(define-public crate-list_flattener-0.1.1 (c (n "list_flattener") (v "0.1.1") (d (list (d (n "parse_int") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1vsj1y5xfk2y9i4366kl68ylfjn590s0x47ffv3p00ks078syz36")))

(define-public crate-list_flattener-0.2.0 (c (n "list_flattener") (v "0.2.0") (d (list (d (n "afl") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "parse_int") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "1l12b2wcpmcxhmw69nm8i8cn9i2m74j383jn219wilya9kbf818w") (f (quote (("fuzz" "afl") ("default"))))))

(define-public crate-list_flattener-0.3.0 (c (n "list_flattener") (v "0.3.0") (d (list (d (n "afl") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "parse_int") (r "^0.4") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "13g3zxzfylb25xyhlrz3wsqcq0nls464x9fvb314mnz99080hnxq") (f (quote (("fuzz" "afl") ("default"))))))

