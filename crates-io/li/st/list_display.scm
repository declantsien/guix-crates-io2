(define-module (crates-io li st list_display) #:use-module (crates-io))

(define-public crate-list_display-0.1.0 (c (n "list_display") (v "0.1.0") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1ajc1nz5pnsgf6gjnkninfvsipr0rshwf24xzryfhkz21bm5l9qk") (y #t)))

(define-public crate-list_display-0.1.1 (c (n "list_display") (v "0.1.1") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13fqw4y2s5kp1l4ikbz9dvbdc5brz07jl62dxh73wbj5hk49ahhl")))

