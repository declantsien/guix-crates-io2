(define-module (crates-io li st list-mod) #:use-module (crates-io))

(define-public crate-list-mod-0.1.0 (c (n "list-mod") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "188mar5p8lnyr8qpznpnzr4ng6rnc12mngm49xxqblk6ihywv54n")))

(define-public crate-list-mod-0.1.1 (c (n "list-mod") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1mlb13l1yfld3misyf9ciwkl8v1q50mpi0n8bdxy83xhlp9bw09x")))

(define-public crate-list-mod-0.1.2 (c (n "list-mod") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "05mym7wrvkqc7as1ajncbld414z5j0dmvwpdzhszz0545lklbi7x")))

(define-public crate-list-mod-0.1.3 (c (n "list-mod") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0qikgw8gg10x648zv07dwyy5vp0q4q87biibbdyh7y31mc7w42bd")))

(define-public crate-list-mod-0.1.4 (c (n "list-mod") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1cm1dq1mazhb9ya77jai59pjbbdpzcan2kkhwkdidwzk5vd32fnx")))

(define-public crate-list-mod-0.1.5 (c (n "list-mod") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1qzl2pjslcvz2p1nkmdmv4v9mfp0cb53j46z5alqfda7w1lk2dwl")))

(define-public crate-list-mod-0.1.6 (c (n "list-mod") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "149dk32p0a5d0ji5vnk46m17npqfrybcwfm9qgsrfy65w9p78vwm")))

(define-public crate-list-mod-0.1.7 (c (n "list-mod") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1yfizpi9glp8mycgmmj9xrvysmzkblyr4a34gm9nbkrqvi9z19sz")))

(define-public crate-list-mod-0.1.8 (c (n "list-mod") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "12cm38l4igp709292bv74x3scyyf0qgf5d843zv40vsi2bc8dx7j")))

(define-public crate-list-mod-0.1.9 (c (n "list-mod") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ijfzr0p4fxswm3isdhjmyv20pjgy8hr4dfr19znlgmmkk0zhlgi") (y #t)))

