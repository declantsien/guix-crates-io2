(define-module (crates-io li st listenbrainz-rust) #:use-module (crates-io))

(define-public crate-listenbrainz-rust-0.1.0 (c (n "listenbrainz-rust") (v "0.1.0") (d (list (d (n "regex") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "18a77pz0064qxqxi42mlr8qgr69cjpcac4hvpl01gh8bcmg4sklg")))

(define-public crate-listenbrainz-rust-0.2.0 (c (n "listenbrainz-rust") (v "0.2.0") (d (list (d (n "regex") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1z5ab02zp02p5r532j6bkcfrfpmrnbs31ljn9ls2ldh9xxqjc9qq")))

(define-public crate-listenbrainz-rust-0.2.1 (c (n "listenbrainz-rust") (v "0.2.1") (d (list (d (n "regex") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1drgidgazfhbv0lzsyzqi832jid08ijbqhy47wd40a6kba5zpmnp")))

(define-public crate-listenbrainz-rust-0.2.2 (c (n "listenbrainz-rust") (v "0.2.2") (d (list (d (n "regex") (r "^1.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0l22sfk2zfm5i21w0q2rlyayzza5g39in79gf0g16ix1ky1agc2x")))

