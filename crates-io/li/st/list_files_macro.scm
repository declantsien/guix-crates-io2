(define-module (crates-io li st list_files_macro) #:use-module (crates-io))

(define-public crate-list_files_macro-0.1.0 (c (n "list_files_macro") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.99") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "166kl2qi960pd5z8dj5i86d866mjnm9jp80lv2hhk9cvbnx6dzvf")))

