(define-module (crates-io li st list_compression) #:use-module (crates-io))

(define-public crate-list_compression-0.1.0 (c (n "list_compression") (v "0.1.0") (h "1br9ldrfwz8zd5r17bml6irm5w8fvwa24ww030q9nm5zhwn49lhr") (y #t)))

(define-public crate-list_compression-0.1.1 (c (n "list_compression") (v "0.1.1") (h "15ass45014hknkg4ngrgi5qh80hgfp4lrw0ga0042nh1zlsjg3i6")))

(define-public crate-list_compression-0.1.2 (c (n "list_compression") (v "0.1.2") (h "19jmgwz816x9lcddx794db5hg05j8qv8pchmgfhxjgcxmsipxg11") (y #t)))

(define-public crate-list_compression-0.1.3 (c (n "list_compression") (v "0.1.3") (h "0wzcrn88sf2vmnjf0lldl4j1gd4cq5xfjw2z4yjx339x0cqwbqz5") (y #t)))

