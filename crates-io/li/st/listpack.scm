(define-module (crates-io li st listpack) #:use-module (crates-io))

(define-public crate-listpack-0.1.0 (c (n "listpack") (v "0.1.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gvxl3qy0snabcjl1nd5pwsvmrx3rl4g9vjqd6a515l853pylyz9")))

(define-public crate-listpack-0.1.1 (c (n "listpack") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m48frks0vffqgcg9xs3vn7l9hsbxi6vsvhj03qvfcxx20kvhxxg")))

(define-public crate-listpack-0.1.2 (c (n "listpack") (v "0.1.2") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1swpmgwjk56z5500gv8x2rmrmfm23cr3i671rq8kma5v8zwbp8h5")))

(define-public crate-listpack-0.1.3 (c (n "listpack") (v "0.1.3") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fzh87vcld7r8786b60m8cwb60i2nj4pmn842y7ail3fxwgdlki1")))

(define-public crate-listpack-0.1.4 (c (n "listpack") (v "0.1.4") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hq73qw5gdqvlba33qssrcrzvx9379c5dm16qhhddi1ph2rql0g5")))

(define-public crate-listpack-0.1.5 (c (n "listpack") (v "0.1.5") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nkl0bww2pbb0r9xyxqhi1f6xd2syixqipwf1088ngwrn1l4lkir")))

(define-public crate-listpack-0.1.6 (c (n "listpack") (v "0.1.6") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1m70xdh5c7dbavnkq4b5gabycmsypb8zhv9pd8pa7pkwbb905biz")))

