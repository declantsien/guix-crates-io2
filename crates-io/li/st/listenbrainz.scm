(define-module (crates-io li st listenbrainz) #:use-module (crates-io))

(define-public crate-listenbrainz-0.1.0 (c (n "listenbrainz") (v "0.1.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("json"))) (d #t) (k 0)))) (h "1ybsj0396c9am6aa3yk3xj8wdz7ydr5sbwzcbghviygi7w16fxxd")))

(define-public crate-listenbrainz-0.2.0 (c (n "listenbrainz") (v "0.2.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("json"))) (d #t) (k 0)))) (h "08g0fxninhh84hxgn8b9rpb14samwmx3rxxl88jn3sj8d96hb3g8")))

(define-public crate-listenbrainz-0.3.0 (c (n "listenbrainz") (v "0.3.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (f (quote ("json"))) (d #t) (k 0)))) (h "0v8d0qbzazzk226jai9v6wvxhrnvpsy7v9ml5mzavljbva9w90bq")))

(define-public crate-listenbrainz-0.4.0 (c (n "listenbrainz") (v "0.4.0") (d (list (d (n "attohttpc") (r "^0.17") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1ghf8cf8d0kxgafbiy59kvb05bnwk5hcabw5k2ds3q71i60wcxp2")))

(define-public crate-listenbrainz-0.4.1 (c (n "listenbrainz") (v "0.4.1") (d (list (d (n "attohttpc") (r "^0.17") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "14q85a1wvcp9wsd9vfsdncfnx0fbhdaw9zpx044h0lz893jgjs0y")))

(define-public crate-listenbrainz-0.4.2 (c (n "listenbrainz") (v "0.4.2") (d (list (d (n "attohttpc") (r "^0.17") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0y1d9bbmmdjfgapiir03776z8rgkc2yqy6j83k7bam96xbk9190g")))

(define-public crate-listenbrainz-0.4.3 (c (n "listenbrainz") (v "0.4.3") (d (list (d (n "attohttpc") (r "^0.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0hvscar5d6dpj2d08606nzbyi6qr042dk1lnin5y9n031nhp40pl")))

(define-public crate-listenbrainz-0.5.0 (c (n "listenbrainz") (v "0.5.0") (d (list (d (n "attohttpc") (r "^0.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "008lisn4nrbb8xczk6q7h3jf88531lzqg6dclkqg6ksmz82n447q")))

(define-public crate-listenbrainz-0.6.0 (c (n "listenbrainz") (v "0.6.0") (d (list (d (n "attohttpc") (r "^0.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1x2dh3mdz4vhhmal175rld8cnq8ppxmpckgdlfkynpa8r1nxkzyd")))

(define-public crate-listenbrainz-0.7.0 (c (n "listenbrainz") (v "0.7.0") (d (list (d (n "attohttpc") (r "^0.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1rdycxr69cj5qlbl04f8hxagf2xiv8q8zwmwbw6v3sq0cvlk3rlz")))

