(define-module (crates-io li st list-program) #:use-module (crates-io))

(define-public crate-list-program-0.0.1 (c (n "list-program") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "0xs4rcbyfqxkran14wx026sbvm9vv66d99va72xqkgjsc1985klr") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-list-program-0.0.2 (c (n "list-program") (v "0.0.2") (d (list (d (n "anchor-lang") (r "^0.19.0") (d #t) (k 0)))) (h "0nmw08dbph5cw4rig7dzn0p3jqan75mrmgfjsjxl8maihwc000rw") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

(define-public crate-list-program-0.0.3 (c (n "list-program") (v "0.0.3") (d (list (d (n "anchor-lang") (r "^0.20.1") (d #t) (k 0)))) (h "0ncpjaq6zilyyw9rqs5h1x4f5yr9sk6svg4xjhqpq6dsw1vg832b") (f (quote (("no-entrypoint") ("cpi" "no-entrypoint"))))))

