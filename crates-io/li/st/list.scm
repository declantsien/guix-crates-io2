(define-module (crates-io li st list) #:use-module (crates-io))

(define-public crate-list-0.1.0 (c (n "list") (v "0.1.0") (h "13pglk38340kk90c7f03j253gd828p34rlj2rb5hksfccqi82bip")))

(define-public crate-list-0.1.1 (c (n "list") (v "0.1.1") (h "1fgrklmy2j8ckw6661biajix8yycbklnazbpz5y783nmv35zd468")))

(define-public crate-list-0.1.2 (c (n "list") (v "0.1.2") (h "09abjcxldn627v92qahrdxiar9qdqpc2232dar9n29zqj58nxbz5")))

(define-public crate-list-0.1.3 (c (n "list") (v "0.1.3") (h "020l8cz05a996m017rlfh9g6b0hj8y3lmr54pmhj25kc043ilbaj")))

