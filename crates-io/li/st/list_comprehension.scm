(define-module (crates-io li st list_comprehension) #:use-module (crates-io))

(define-public crate-list_comprehension-0.1.0 (c (n "list_comprehension") (v "0.1.0") (h "0c17x0yk40sazslwdmm4fn4inzwchhf8avvs3k1w5f66ac2za00j")))

(define-public crate-list_comprehension-0.1.1 (c (n "list_comprehension") (v "0.1.1") (h "18nfdhc8hqz0iyzhly90qy3wnap88b2w48djbpbh9z6yad7d16fl")))

(define-public crate-list_comprehension-0.1.2 (c (n "list_comprehension") (v "0.1.2") (h "0743i5rc8smjsdp76ak5y9j295vidicsvmgb4gm4crhn9jli7r62") (y #t)))

(define-public crate-list_comprehension-0.1.3 (c (n "list_comprehension") (v "0.1.3") (h "0l72snmfavjwvgw62nmb3yaddf9m47p0h0vyhg690d8apk8iis1r")))

(define-public crate-list_comprehension-0.1.4 (c (n "list_comprehension") (v "0.1.4") (h "0mf8bcxwvs1b6s8krmqdqrvzpd8y9h3hqvswvmfima3rfkcrr42c")))

(define-public crate-list_comprehension-0.1.5 (c (n "list_comprehension") (v "0.1.5") (h "1p9k8b4f1yy2iadn3yacagi4cxy7g2641qib24jfh9p30sx3954l")))

(define-public crate-list_comprehension-0.2.0 (c (n "list_comprehension") (v "0.2.0") (d (list (d (n "genawaiter") (r "^0.99.1") (o #t) (k 0)))) (h "0zfq9gvyyp54pqxg7v0070inb5vxnj6dv6881s4phrvwpyk4azmy") (f (quote (("lazy_comp" "genawaiter") ("default" "lazy_comp"))))))

