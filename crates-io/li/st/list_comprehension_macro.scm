(define-module (crates-io li st list_comprehension_macro) #:use-module (crates-io))

(define-public crate-list_comprehension_macro-0.1.0 (c (n "list_comprehension_macro") (v "0.1.0") (h "0ydw0qgafiyci3zvlj6i44mivkgb8kggkinh5cad83svfj1skqzr")))

(define-public crate-list_comprehension_macro-0.1.1 (c (n "list_comprehension_macro") (v "0.1.1") (h "113ywwcp87din2hqr2b8kcvcyf8nmx2m1vabkv9vikmdvhv98sqz")))

