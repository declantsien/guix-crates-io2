(define-module (crates-io li st listree) #:use-module (crates-io))

(define-public crate-listree-0.1.0 (c (n "listree") (v "0.1.0") (h "1dwmc6x627lr1bvijz7vwlwv6cdw073gr1axgn2vqc9msl8dydlk")))

(define-public crate-listree-0.1.1 (c (n "listree") (v "0.1.1") (h "0b40mxvlfaby149lq93b83zkwacpk3sx730dl66qr0jmnvqr1zaq")))

(define-public crate-listree-0.2.0 (c (n "listree") (v "0.2.0") (h "0hn1j9h9zqi84hkraa208ahdf2wqwxh3y4lpqmqw6swjjk3lim4j")))

