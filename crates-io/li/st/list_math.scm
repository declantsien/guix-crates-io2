(define-module (crates-io li st list_math) #:use-module (crates-io))

(define-public crate-list_math-0.1.0 (c (n "list_math") (v "0.1.0") (h "0lbsh40zvlc5pmyigiaz0l8296finfp4qivispgisz2ws9kfrb0g")))

(define-public crate-list_math-0.2.0 (c (n "list_math") (v "0.2.0") (h "05ni2d84kh192ndchxfiad0j0fjpbz7h6ij0n95rz6bgdgacj2cy")))

(define-public crate-list_math-0.2.1 (c (n "list_math") (v "0.2.1") (h "0mdkfavmgsplml7f9l5s9x3zcrkiman950xmqv7zflhi3fpbkshl")))

(define-public crate-list_math-0.3.0 (c (n "list_math") (v "0.3.0") (h "1f6kdxq2425adbba1s8m4g4ffs7nlw8zdg47v76c8fj0hh7q71zz")))

(define-public crate-list_math-0.3.1 (c (n "list_math") (v "0.3.1") (h "0aqw7rwscpi1irw97nrsjydnd2p3n3cm25ak65zz289njaqnzr8v")))

(define-public crate-list_math-0.3.2 (c (n "list_math") (v "0.3.2") (h "0naphk1lnbbm5jgam0vvkpad3f9dxdk86x695q8i8ll0azj1cbcz")))

(define-public crate-list_math-0.3.3 (c (n "list_math") (v "0.3.3") (h "05jalgggvsj34sxybhibkss10q5ayb0r9z470gw94vp8yr2ngd0m")))

