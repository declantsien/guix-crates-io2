(define-module (crates-io li st list-rs) #:use-module (crates-io))

(define-public crate-list-rs-0.1.0 (c (n "list-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled" "chrono"))) (d #t) (k 0)))) (h "1x227qn70r1xrxmf7vh40v080s81cbynlw0qqlwbihg5vn2x263w")))

(define-public crate-list-rs-0.1.1 (c (n "list-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29.0") (f (quote ("bundled" "chrono"))) (d #t) (k 0)))) (h "1hgw2srf4988s8kkf5yz35f56782w8b7vkkfsdscixwxwbx5ln3z")))

