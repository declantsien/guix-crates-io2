(define-module (crates-io li lj liljag_gps_crate) #:use-module (crates-io))

(define-public crate-liljag_gps_crate-0.1.0 (c (n "liljag_gps_crate") (v "0.1.0") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "1vwcqi2zx8k11pwrihzvnxhkavz6xmwd4yxkmzvqjywkc0aa3fd0")))

(define-public crate-liljag_gps_crate-0.1.1 (c (n "liljag_gps_crate") (v "0.1.1") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0x7nj3szjjm6i8gnj853iin6fap4i5diz8paqmb1ya6qsawnidmk")))

(define-public crate-liljag_gps_crate-0.1.2 (c (n "liljag_gps_crate") (v "0.1.2") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0hc1dlpyb5a5fhxwavb3z7g6c6msrykkg5m8mgd9pamf6372fvk6")))

(define-public crate-liljag_gps_crate-0.1.3 (c (n "liljag_gps_crate") (v "0.1.3") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0n63zqmw0iga9swzi00q8hgha8msvr69xxa5l7zi40bd13rvwq8f")))

(define-public crate-liljag_gps_crate-0.1.4 (c (n "liljag_gps_crate") (v "0.1.4") (d (list (d (n "rpi_embedded") (r "^0.1.0") (d #t) (k 0)))) (h "0yr70kihrmcjw98mmy3fqj18h23q3zhinwc50nprzh8r86h2n9jv")))

