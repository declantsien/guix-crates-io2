(define-module (crates-io li pm lipmaa-link) #:use-module (crates-io))

(define-public crate-lipmaa-link-0.1.0 (c (n "lipmaa-link") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1vl1yg61946g8spa1wd1s0zzb439y30srnfy6mnkfl3yi6pfr0vc")))

(define-public crate-lipmaa-link-0.1.1 (c (n "lipmaa-link") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1ybfkhwa2dwk1x6dhc8gxkmxvffhv0108mfr0ms4wjh6v6fyxyyq")))

(define-public crate-lipmaa-link-0.2.0 (c (n "lipmaa-link") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "00xdqw6fqzjh4n56w5mld0c46mfnga1anh9sr4d1vklj8ps29wzl")))

(define-public crate-lipmaa-link-0.2.1 (c (n "lipmaa-link") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0lqql1814vn52zpnn7xs27s5660apjjrxagbaq15rgghlfzqkmyz") (f (quote (("std") ("default" "std"))))))

(define-public crate-lipmaa-link-0.2.2 (c (n "lipmaa-link") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1jzv2rnb6my5s9gq9q91788lvk46xsfkrvwn5f6gc6jczggg1x9n") (f (quote (("std") ("default" "std"))))))

