(define-module (crates-io li bd libdivsufsort-rs) #:use-module (crates-io))

(define-public crate-libdivsufsort-rs-0.1.0 (c (n "libdivsufsort-rs") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "15jj8zypslyfsmgmz40x0fs35b4sz373cii5jqpq7ffm03ybi12q")))

(define-public crate-libdivsufsort-rs-0.1.1 (c (n "libdivsufsort-rs") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1ych4fpfgpvgjsxphq37anwv3m65rbjwr2wkprkqv7pa80jrymrd")))

(define-public crate-libdivsufsort-rs-0.1.2 (c (n "libdivsufsort-rs") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.49") (d #t) (k 1)))) (h "1yz31dlr0akb3xkbvj39s2nmkjp84fx95hs3pv3859bj0kgdbris") (r "1.57.0")))

