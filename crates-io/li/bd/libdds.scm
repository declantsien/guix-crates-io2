(define-module (crates-io li bd libdds) #:use-module (crates-io))

(define-public crate-libdds-1.0.0 (c (n "libdds") (v "1.0.0") (h "14xk41sgr8xihrzj46dqr0hkzidbyzwl4ay74kx74mszdi4bmpf6") (y #t)))

(define-public crate-libdds-1.1.0 (c (n "libdds") (v "1.1.0") (d (list (d (n "bitflags") (r "^2.4.0") (d #t) (k 0)))) (h "04cil34q8y3pf5q5h0xfbp0m622y68nf7j131j4m545hgs25hyys")))

