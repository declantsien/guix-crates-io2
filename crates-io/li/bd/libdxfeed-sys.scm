(define-module (crates-io li bd libdxfeed-sys) #:use-module (crates-io))

(define-public crate-libdxfeed-sys-0.1.0 (c (n "libdxfeed-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "06b1bzv7d7rlfmss9rrskij92lnpf9csjkjd30hkapsw0f25829h")))

(define-public crate-libdxfeed-sys-0.1.1 (c (n "libdxfeed-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m8lv5qy7pz1p0nfcp79lmmdsb88gzxifpx82fgfajk3s6sj8jp1") (l "DXFeed")))

(define-public crate-libdxfeed-sys-0.1.2 (c (n "libdxfeed-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kj8xqq0iqb9hsgsny2kvpx2yzrp6j91v6vg76bdbh1wq057fm4d") (l "DXFeed")))

(define-public crate-libdxfeed-sys-0.1.3 (c (n "libdxfeed-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pkcqsmyws7f98axqgnv2gmnbpv7i86zyw3nj2wr70fls9gf26g3") (l "DXFeed")))

(define-public crate-libdxfeed-sys-0.1.4 (c (n "libdxfeed-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1r7y4hwk0mbnwdq5cmv6nmrkfyv94c1s4ybfdrf94mamxi5y6dnq") (l "DXFeed")))

(define-public crate-libdxfeed-sys-0.1.5 (c (n "libdxfeed-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1jazcs19g4yrlvc2bzm29wgabmkwmz5wqr0xzvz0a8m833acskpk") (l "DXFeed") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-libdxfeed-sys-0.1.8 (c (n "libdxfeed-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.49") (d #t) (k 1)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0pmapckbxgmw0yvx85jyksxcpgww11akfs7drsfqspjwr9vrw9b5") (l "DXFeed") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-libdxfeed-sys-0.2.0 (c (n "libdxfeed-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1b12b47p6b11r17mk4lfy9hny4ani2m3yba2d6sdaymaf0qq0vnq") (l "DXFeed") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-libdxfeed-sys-0.2.1 (c (n "libdxfeed-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0qzqy37inllx0dd63nidggnpxfv31vfls02z1wm5aslwc9lbh1x9") (l "DXFeed") (s 2) (e (quote (("serde" "dep:serde"))))))

