(define-module (crates-io li bd libde265) #:use-module (crates-io))

(define-public crate-libde265-0.1.0 (c (n "libde265") (v "0.1.0") (d (list (d (n "libde265-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1dck3n2zd82wqahrwr74ywh3v9w90nmw2m1xqal7xn00if053raq") (f (quote (("system" "libde265-sys/system") ("static" "libde265-sys/static") ("encoder" "libde265-sys/encoder") ("default" "static"))))))

(define-public crate-libde265-0.1.1 (c (n "libde265") (v "0.1.1") (d (list (d (n "libde265-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1fc5rhdx3kkxn9p9wl04hzrdmawbhk8k5wkffmm11bwybj2y3rk7") (f (quote (("system" "libde265-sys/system") ("static" "libde265-sys/static") ("encoder" "libde265-sys/encoder") ("default" "static"))))))

