(define-module (crates-io li bd libdvb-rs) #:use-module (crates-io))

(define-public crate-libdvb-rs-0.4.0 (c (n "libdvb-rs") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "derive_more") (r "^0.99.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mgj6qff4fd7kv525kbyk8nxifh0baaxv6nsgdm4c2vp0cv62xqm")))

(define-public crate-libdvb-rs-0.4.1 (c (n "libdvb-rs") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "1mlvchczscqrkmxcvnv1l77r7c5238vc5pdy8xih6y9r94w45ssg")))

(define-public crate-libdvb-rs-0.4.2 (c (n "libdvb-rs") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vab08k6xmb914aipv31hb73d48125fb7j5jsszlgq3mlvgk1wys")))

(define-public crate-libdvb-rs-0.4.3 (c (n "libdvb-rs") (v "0.4.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x6kv2xrab6jvpza4k5wgg25hlfwygka2hshfnl9p7c57p0kjs8x")))

(define-public crate-libdvb-rs-0.4.5 (c (n "libdvb-rs") (v "0.4.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.2") (d #t) (k 0)) (d (n "nix") (r "^0.23") (d #t) (k 0)) (d (n "strum") (r "^0.23") (f (quote ("derive"))) (d #t) (k 0)))) (h "1f3qbwir3dabcymabgdz7y2ak7vcqqydk0ni0pn7avg200xhbqlr")))

