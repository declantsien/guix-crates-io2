(define-module (crates-io li bd libdw) #:use-module (crates-io))

(define-public crate-libdw-0.0.1 (c (n "libdw") (v "0.0.1") (d (list (d (n "libdw-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0ipjnxhmrpairrnpzmknjv5j14kf1gsfnl6j503fhml635rw0yv3")))

(define-public crate-libdw-0.0.2 (c (n "libdw") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "libelf") (r "^0.0.2") (d #t) (k 0)))) (h "1v98j8kg4nsbirww8skwc1mmw91r0d3bk941ax0imyyn8mxxnpxj")))

(define-public crate-libdw-0.0.4 (c (n "libdw") (v "0.0.4") (d (list (d (n "libdw-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libelf") (r "^0.0.4") (d #t) (k 0)))) (h "0kvddda2cjw3km2c2jlnb2g8n29s55p5p4na7wy5kqxbnj5jrpj4")))

(define-public crate-libdw-0.0.5 (c (n "libdw") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libelf") (r "^0.0.5") (d #t) (k 0)))) (h "0vr5hs1dqcxxgsrfk49wq5c4k37p5drikifmyn9gjdc6hnrv56lk")))

(define-public crate-libdw-0.0.6 (c (n "libdw") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "libelf") (r "^0.0.6") (d #t) (k 0)))) (h "074h63crjj0lp2ymrw3nhss211mx03z5161yr7k98wplng1xbxz0")))

(define-public crate-libdw-0.1.0 (c (n "libdw") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libelf") (r "^0.1.0") (d #t) (k 0)))) (h "0wzqramxz6rkgqzi6qnh702pq6rh9ijxi5v0vzzh881iinw41g6z")))

