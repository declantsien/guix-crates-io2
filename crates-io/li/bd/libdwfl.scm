(define-module (crates-io li bd libdwfl) #:use-module (crates-io))

(define-public crate-libdwfl-0.0.1 (c (n "libdwfl") (v "0.0.1") (d (list (d (n "libdw-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0qp6hh0hvr51cj8595pvf0ha58bn60hgffc79h8rd27ghlj95mds")))

(define-public crate-libdwfl-0.0.2 (c (n "libdwfl") (v "0.0.2") (d (list (d (n "libdw-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0pbay34wc851nrjykhzqlkawm1v01skxsy99vqjalcd65dzhy2hq")))

(define-public crate-libdwfl-0.0.4 (c (n "libdwfl") (v "0.0.4") (d (list (d (n "libdw") (r "^0.0.4") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.4") (d #t) (k 0)))) (h "1a43s3zrxrbnr1b6glks1x8afp1v9g87sa2cfqb5pq6r9rrq1ias")))

(define-public crate-libdwfl-0.0.5 (c (n "libdwfl") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libdw") (r "^0.0.5") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1zq3kybx8fxsgjbrcdd8x7y4qhi8miyc9mwxcjl6v0b07l47sdhx")))

(define-public crate-libdwfl-0.0.6 (c (n "libdwfl") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libdw") (r "^0.0.6") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.6") (d #t) (k 0)))) (h "1nrr3fvdnd9n5lpk5y65n5yb8qn0s9nj70x39syc2w02y8qq59s4")))

(define-public crate-libdwfl-0.1.0 (c (n "libdwfl") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libdw") (r "^0.1.0") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1xbaa9mm8q17ffqik7hna1r1n4bn0rzblqgi850dzlm2wqa5sk37")))

