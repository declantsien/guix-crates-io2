(define-module (crates-io li bd libdw-sys) #:use-module (crates-io))

(define-public crate-libdw-sys-0.0.1 (c (n "libdw-sys") (v "0.0.1") (h "0iygm7gz9kwhmm0hrx0s4aja1xxa1psaiqhanajav3ywxhb88f9d")))

(define-public crate-libdw-sys-0.0.2 (c (n "libdw-sys") (v "0.0.2") (d (list (d (n "libelf-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0dxmsdnlw5i4s5m43gbdlfibg6asv6kc9d60a5b46ldcnivsmbfr")))

(define-public crate-libdw-sys-0.0.3 (c (n "libdw-sys") (v "0.0.3") (d (list (d (n "libelf-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "16flghqssz6jgqw2w5xgs5fh16l1v1xr2d9w88ya7rsa7z37lqjs") (y #t)))

(define-public crate-libdw-sys-0.0.4 (c (n "libdw-sys") (v "0.0.4") (d (list (d (n "libelf-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "09dsacr77f78qavgylalwdx42fcqxsdczrb8hs7h7f8zxyhqg1rn")))

(define-public crate-libdw-sys-0.0.5 (c (n "libdw-sys") (v "0.0.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libelf-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1qr67bvpyna0iq0ba8rfja4v2aircxglqvhq6lg48hj43c4vp2m7")))

(define-public crate-libdw-sys-0.0.6 (c (n "libdw-sys") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libelf-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1xb8x9d6v70i70ic7kwxligf0k2j03nhd43d5380wcr1wi66pxhs") (l "dw")))

(define-public crate-libdw-sys-0.1.0 (c (n "libdw-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libelf-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.15") (d #t) (k 1)))) (h "17jxh5n8hcmxpbp64g1dsja8pnqxq3x6xgvkh9cq3l626vji4axw") (l "dw")))

