(define-module (crates-io li bd libdatachannel-sys) #:use-module (crates-io))

(define-public crate-libdatachannel-sys-0.20.1 (c (n "libdatachannel-sys") (v "0.20.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cpp_build") (r "^0.5") (d #t) (k 1)) (d (n "openssl-src") (r "^300") (d #t) (k 1)))) (h "1kxyx9dipkgg075ia3k2ji7gj9p21i11khfs33jzrwqzllvh2rzd")))

