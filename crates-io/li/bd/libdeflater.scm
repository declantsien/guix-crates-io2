(define-module (crates-io li bd libdeflater) #:use-module (crates-io))

(define-public crate-libdeflater-0.1.0 (c (n "libdeflater") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^0.2.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5.3") (d #t) (k 2)))) (h "1hkbig7in7hrba71sngvs59m5y7a89ghc9vkr2d49sj45icbs65w")))

(define-public crate-libdeflater-0.1.1 (c (n "libdeflater") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^0.2.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5.3") (d #t) (k 2)))) (h "12wg9n6yki1lx98lackywa1bdmcc6biclp77ghcrh8zx9x1cldkr")))

(define-public crate-libdeflater-0.1.2 (c (n "libdeflater") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^0.2.5") (d #t) (k 2)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 2)) (d (n "toml") (r "^0.5.3") (d #t) (k 2)))) (h "0idf1jiqycvi2zppliij7ccgjyl00fmlxdldaa4w2ihpvj5as3vv")))

(define-public crate-libdeflater-0.2.0 (c (n "libdeflater") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)))) (h "0ih2x22qpxsm9nq7d9hjdjz5r61kz02xv8fwysr6b61n2f5s1p36")))

(define-public crate-libdeflater-0.3.0 (c (n "libdeflater") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)))) (h "0wyvq9qrlkfvj4wpgn61v0j69sr2k7rpfj7zhfbb446k24brgnwq") (l "libdeflate")))

(define-public crate-libdeflater-0.5.0 (c (n "libdeflater") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.5.0") (d #t) (k 0)))) (h "1wh17bw59vj3bsf7xyq9nyja6anjsjir2z9d1r3nvwlisy00k0d4")))

(define-public crate-libdeflater-0.6.0 (c (n "libdeflater") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0m6nyjqmpk74d47n26cyia7rl61hds5klwzghkd522cpacxdkvck") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.7.0 (c (n "libdeflater") (v "0.7.0") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "flate2") (r ">=1.0.11, <2.0.0") (d #t) (k 2)) (d (n "libdeflate-sys") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "05gvfdal816lvmsfz9rfc0lbsg3x3hccbd6r63y9q8a9iv4g12jv") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.7.1 (c (n "libdeflater") (v "0.7.1") (d (list (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "flate2") (r ">=1.0.11, <2.0.0") (d #t) (k 2)) (d (n "libdeflate-sys") (r ">=0.7.0, <0.8.0") (d #t) (k 0)))) (h "03zn7ikg329zmsc47yag1wlvl08ps53cq635npvrp4jlar34ghfc") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.7.2 (c (n "libdeflater") (v "0.7.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.7.0") (d #t) (k 0)))) (h "14llpskss34ccxh4zdkh414irm2h5v2vrapjrwh314hhv7q0100w") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.7.3 (c (n "libdeflater") (v "0.7.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0h0mvv0s5gwlc3ry2z3fh0qf0b2a1cfq3rlfkrq68yr5461hq761") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.7.4 (c (n "libdeflater") (v "0.7.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.7.0") (d #t) (k 0)))) (h "065bsgmd66bj4wg32jip2dwq74kq07w3bghhwhqd23c8f7q70axk") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.7.5 (c (n "libdeflater") (v "0.7.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.7.5") (d #t) (k 0)))) (h "1zjjcgb5pv59g6mx7avbs05ar7rdrvi45kw2gbmk64qqzlaw3w0m") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.8.0 (c (n "libdeflater") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0kvcwy2ic7mk03bjl2hchv142ri3rs1nqc8k2q7y274bipnxgw0j") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.9.0 (c (n "libdeflater") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.9.0") (d #t) (k 0)))) (h "0ikgyqybfqj1vsjq06n8jgdzj849kn8ngvbkichn8bxs0w5kh3ai") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.10.0 (c (n "libdeflater") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.10.0") (d #t) (k 0)))) (h "0nrfng8fmhydwnf6j6jig25sr9r70j0ipbf7mm5qd7n41sbbfmp6") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.11.0 (c (n "libdeflater") (v "0.11.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.11.0") (d #t) (k 0)))) (h "0385hpai7fsnpfvxd3hki43v1cj8w6z1cb2rn8wd6vq4dam8bqnq") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.12.0 (c (n "libdeflater") (v "0.12.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.12.0") (d #t) (k 0)))) (h "0cbrdvwhilvmk919izkp5bqgwfa7b8nj2ar9gp67nb345wl667k7") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.13.0 (c (n "libdeflater") (v "0.13.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.13.0") (d #t) (k 0)))) (h "0bjdyb5p8bx5jj2jsmaq1lr86bkzb7qsnykkr32gmjnxibgyi47a") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-0.14.0 (c (n "libdeflater") (v "0.14.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^0.14.0") (d #t) (k 0)))) (h "1hqr5aa6gld6wxfzk4rl9318gmhjsnrxfzjalbial56v5nc0zcsq") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-1.19.0 (c (n "libdeflater") (v "1.19.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^1.19.0") (d #t) (k 0)))) (h "0zdbiqq2fmjakwk7d1xwnakjnxzaba9rz18k9clyql13cqpv4c9s") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-1.19.1 (c (n "libdeflater") (v "1.19.1") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^1.19.1") (d #t) (k 0)))) (h "1h3ddpqg1qsmky5lpwzq0w6algh3lfn5bn2z009wbyrvsw1170mf") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-1.19.2 (c (n "libdeflater") (v "1.19.2") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^1.19.2") (d #t) (k 0)))) (h "09xv27nrdv2ylk9nfzhkf1lh3adnwiiafams9xs9xw8w8iwkzr3a") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc"))))))

(define-public crate-libdeflater-1.19.3 (c (n "libdeflater") (v "1.19.3") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^1.19.3") (d #t) (k 0)))) (h "00xxwh9x43fh7jb4cvi3mr0cyv2csjz7q45cnbi24pqysddrhni6") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc") ("dynamic" "libdeflate-sys/dynamic"))))))

(define-public crate-libdeflater-1.20.0 (c (n "libdeflater") (v "1.20.0") (d (list (d (n "adler32") (r "^1.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "flate2") (r "^1.0.11") (d #t) (k 2)) (d (n "libdeflate-sys") (r "^1.20.0") (d #t) (k 0)))) (h "05nhvv27g0kj9xj3g8ipq8xavqsh0inpj7bg86iy03h1ywj69zcd") (f (quote (("use_rust_alloc") ("freestanding" "libdeflate-sys/freestanding" "use_rust_alloc") ("dynamic" "libdeflate-sys/dynamic"))))))

