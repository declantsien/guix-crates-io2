(define-module (crates-io li bd libdice) #:use-module (crates-io))

(define-public crate-libdice-0.1.0 (c (n "libdice") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "pest") (r "^2.7.10") (d #t) (k 0)) (d (n "pest_derive") (r "^2.7.10") (d #t) (k 0)))) (h "0hd53j83njy6xjv9kchs8sdz7qmfmycv9xkf2ss75sj7qv430hib")))

