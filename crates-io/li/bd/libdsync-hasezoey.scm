(define-module (crates-io li bd libdsync-hasezoey) #:use-module (crates-io))

(define-public crate-libdsync-hasezoey-0.1.0 (c (n "libdsync-hasezoey") (v "0.1.0") (d (list (d (n "Inflector") (r "^0.11.4") (d #t) (k 0)) (d (n "indoc") (r "^2.0.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1jivmdydnbbmarw1z0dlfrd1bb59525kppa2bq749p164l25i3x0") (f (quote (("tsync") ("default" "tsync" "backtrace") ("backtrace") ("async"))))))

(define-public crate-libdsync-hasezoey-0.2.0 (c (n "libdsync-hasezoey") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits" "full"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b2dnwis79xv3bjc9xn1rvkyjh1db9g4i39ys4gc5qs8hyy04y5l") (f (quote (("tsync") ("default" "tsync" "backtrace") ("backtrace") ("async"))))))

