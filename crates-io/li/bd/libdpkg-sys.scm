(define-module (crates-io li bd libdpkg-sys) #:use-module (crates-io))

(define-public crate-libdpkg-sys-0.1.0+1.19 (c (n "libdpkg-sys") (v "0.1.0+1.19") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "1kvz34l3x0hlvr5m9bab1c9hdqv4ljmjdcfaxzz9vkh6bv0qg5nr") (l "dpkg") (r "1.56")))

(define-public crate-libdpkg-sys-0.1.1+1.19 (c (n "libdpkg-sys") (v "0.1.1+1.19") (d (list (d (n "bindgen") (r "^0.61") (d #t) (k 1)) (d (n "system-deps") (r "^6.0") (d #t) (k 1)))) (h "05x34ijzrmqky68khaa9j6nvpk54mr1qqq88v8q139vkld0y01zw") (l "dpkg") (r "1.56")))

