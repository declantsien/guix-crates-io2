(define-module (crates-io li bd libdaw) #:use-module (crates-io))

(define-public crate-libdaw-0.1.0 (c (n "libdaw") (v "0.1.0") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)))) (h "1frni7wwak1rc1sqd4rhxpla6l5mywnxxs61y6p9pbg147l6ngq1")))

(define-public crate-libdaw-0.2.0 (c (n "libdaw") (v "0.2.0") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)))) (h "16byajp9agm16crx65n0183j17yi7isvk031j3a27sqvl6j87ish")))

