(define-module (crates-io li bd libdeflate-sys) #:use-module (crates-io))

(define-public crate-libdeflate-sys-0.5.0 (c (n "libdeflate-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "07ychzvz9xrqs92frs3zpny0l8jp3sndzqpl7zhv6kdqhzx9xqr1") (l "libdeflate")))

(define-public crate-libdeflate-sys-0.6.0 (c (n "libdeflate-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0w8nyccj5b42l1lvyv1svmxb2svgqvbh8v0n8rdcby7bl211anrg") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.7.0 (c (n "libdeflate-sys") (v "0.7.0") (d (list (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)))) (h "0y40if0yhs7wqa5lpxx2ky10xpl9ik3lv2r38wxfw5x4rjhzvj2z") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.7.1 (c (n "libdeflate-sys") (v "0.7.1") (d (list (d (n "cc") (r ">=1.0.0, <2.0.0") (d #t) (k 1)))) (h "15sv2w5i6s86m757s5cw62qbhapik7rw7p4nh9md0p88wx5zm5aa") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.7.2 (c (n "libdeflate-sys") (v "0.7.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "11gck4lidblfw56vxvf8rdz2c910k5bangv4ls5aas7hy7sgff6y") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.7.3 (c (n "libdeflate-sys") (v "0.7.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1r14pwnwgvb0ws3cg4jhl1sm8wc5zsjwr78ll7c302jinpvirj09") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.7.4 (c (n "libdeflate-sys") (v "0.7.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0d003wi3xdlm88i0f85i1psl2ry4f12i4lay2abavv0wfi4viq3w") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.7.5 (c (n "libdeflate-sys") (v "0.7.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0a5pkrxa7zcb0nab2j48dn6j8d8g1pjvz196c308wrax6dpazkjc") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.8.0 (c (n "libdeflate-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0vawbfw86xaphmr1qxk7mr2asrx2bf93zbc707d8d0adk7qn4sr1") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.9.0 (c (n "libdeflate-sys") (v "0.9.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1amsa390nxmhv7xxfl212hrcsprjfhnb57aq9dsylwwj24shqfhn") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.10.0 (c (n "libdeflate-sys") (v "0.10.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "13c9ld3sjnn19a9sqib0im3h5x0f55fg7910p8k881gzjaqsbbs3") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.11.0 (c (n "libdeflate-sys") (v "0.11.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0xs2wdly73ar7r9qyrbwnkwjjz4wd92kv5h78cdxfrsbp2v88ryb") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.12.0 (c (n "libdeflate-sys") (v "0.12.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "11a7nn3p73vw3cnra36vz7mz60wx9jzhzwwji1hbmql5gy0v1xz1") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.13.0 (c (n "libdeflate-sys") (v "0.13.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0pn7f21ywgzd3fkxg385xb9l537sz097frnwz304pna1xcmh80f3") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-0.14.0 (c (n "libdeflate-sys") (v "0.14.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1j6xax4racrz90f2nz51k9vljcf9np7cwfmg2axdgry176n3f901") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-1.19.0 (c (n "libdeflate-sys") (v "1.19.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1hhwgzlpbw2nafylnjrhbyh7s6q9fb2cglf3xxcia30hhmzim4k7") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-1.19.1 (c (n "libdeflate-sys") (v "1.19.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "04775l6vhcm82c8hfrji31w3db0xnqnppl95hhxk04ncm48vvjdn") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-1.19.2 (c (n "libdeflate-sys") (v "1.19.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "17b7ndw3hk6zni6w41sl374cd6xmjpcc92gizmd9k0s0l5840rdp") (f (quote (("freestanding")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-1.19.3 (c (n "libdeflate-sys") (v "1.19.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "0xzi0c9dl9jdzs758fvxmb9wmsqjm0a4b3ygxz2fhvncr1vam76c") (f (quote (("freestanding") ("dynamic" "pkg-config")))) (l "libdeflate")))

(define-public crate-libdeflate-sys-1.20.0 (c (n "libdeflate-sys") (v "1.20.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (o #t) (d #t) (k 1)))) (h "1g3p34012372cdr5bb7zmh27axsp77plpvn7166b9g2pj9zs37k6") (f (quote (("freestanding") ("dynamic" "pkg-config")))) (l "libdeflate")))

