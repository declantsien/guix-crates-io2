(define-module (crates-io li bd libdrm-sweet) #:use-module (crates-io))

(define-public crate-libdrm-sweet-0.1.1 (c (n "libdrm-sweet") (v "0.1.1") (d (list (d (n "libdrm-sys") (r "^0.2.0") (f (quote ("version_2_4_58"))) (d #t) (k 0)))) (h "171yj3x2jmfifbii4jdlqv3vbphgv815q2cfwzh56zgi5hdshv2l")))

(define-public crate-libdrm-sweet-0.1.2 (c (n "libdrm-sweet") (v "0.1.2") (d (list (d (n "libdrm-sys") (r "^0.2.0") (f (quote ("version_2_4_67"))) (d #t) (k 0)))) (h "1xnj5pkqvqg1sdjzzrsdnfdfd8c30x72avaq8sjir5j39fl8g949")))

