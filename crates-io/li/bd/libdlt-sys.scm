(define-module (crates-io li bd libdlt-sys) #:use-module (crates-io))

(define-public crate-libdlt-sys-0.1.0 (c (n "libdlt-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1bwh6fakaf5r35n6szpgmq5r0brlzkh84dam6pvkvbaw227mlz6f") (l "dlt")))

(define-public crate-libdlt-sys-0.1.1 (c (n "libdlt-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0sw1mv4iyn6lh8i3rms5ljpqy21yh3idhywplgqlsxjdh6w13i7b") (l "dlt")))

