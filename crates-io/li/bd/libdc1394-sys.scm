(define-module (crates-io li bd libdc1394-sys) #:use-module (crates-io))

(define-public crate-libdc1394-sys-0.1.0 (c (n "libdc1394-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "0gb825s1rk9jn3s6kyqqnnpl1q4ksvkhr19l68x2yj7y8iy2cn18")))

(define-public crate-libdc1394-sys-0.2.0 (c (n "libdc1394-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "libusb-sys") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0am6rays3nx52911xl9w5zxc7izyqybasc0zmlkk8sxx9ni2gvqj")))

(define-public crate-libdc1394-sys-0.2.1 (c (n "libdc1394-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)) (d (n "libusb-sys") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pdlw6jj3diyz7jb940cv2svgawkdz7c2lky15sa85xpi5d6d1a4")))

(define-public crate-libdc1394-sys-0.2.2 (c (n "libdc1394-sys") (v "0.2.2") (d (list (d (n "libusb-sys") (r "^0.2") (d #t) (k 0)))) (h "1p5lnbwhdz6yryha2y7j9w13fz4p8hqbqswvx1d0ydynbjzmskjh") (l "dc1394-2")))

