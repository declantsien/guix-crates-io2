(define-module (crates-io li bd libdrm_amdgpu_sys) #:use-module (crates-io))

(define-public crate-libdrm_amdgpu_sys-0.1.0 (c (n "libdrm_amdgpu_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "1zc10vq1lwic60whmwxzlcrw3y5q3hz2g80csy8480k1zqadcpby")))

(define-public crate-libdrm_amdgpu_sys-0.1.1 (c (n "libdrm_amdgpu_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "1cpf0nwf265qq5914hjqv8mbriyf5s82hn3xjxrmxr7xc418dqb5") (f (quote (("std") ("default" "std") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.1.2 (c (n "libdrm_amdgpu_sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "0jjwg1wcxwskpwa7mb4wzmqdn2mcs1qcv5a80s4msl6ljkgr8zx5") (f (quote (("std") ("default" "std") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.1.4 (c (n "libdrm_amdgpu_sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "1841zg8llxy1zscvpkqgpvgnx0jsqa67zyxvm6m27bc7vhh2gfpv") (f (quote (("std") ("default" "std") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.1.5 (c (n "libdrm_amdgpu_sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "02gy4l570nmpa75nxypbg6v27yjq4vrizkci5hpjb5d4g440n36y") (f (quote (("std") ("default" "std") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.1.6 (c (n "libdrm_amdgpu_sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.59.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3.25") (o #t) (d #t) (k 1)))) (h "1fvn86ii2mi1z7bz2k0gcp38mbmw43s2xw9raq42ii28k0442r4f") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.2.0 (c (n "libdrm_amdgpu_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1xrgxrbknpxdybrklqilbfyzdr0i1wx8mrks3rlf82f9w4i1a22x") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.2.1 (c (n "libdrm_amdgpu_sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0nv9a3b5zxnk8pcnwrzpcai95q6czjr0xdj78igg18c11pkbbrcb") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.2.2 (c (n "libdrm_amdgpu_sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1ih7rvx9m9591fbhmhvl9sb296d6pczfr70cygpdblcmrac0psj2") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.3.0 (c (n "libdrm_amdgpu_sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "16m1d30f1645jcg8kk5sapzgb312zwih1pwfpdwm37anyrs2ii6r") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.4.0 (c (n "libdrm_amdgpu_sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1xwnyj40f7i1brb1nk0kqzk1yp7vf25hgw9z7k9wwi0j0xpb2gl0") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.4.1 (c (n "libdrm_amdgpu_sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0lmjh5wg4gnmag3qvrfihhsdl3is53zaargp72l9kpikld1apbbb") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.5.0 (c (n "libdrm_amdgpu_sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1d90fmwwm6k26ym32xcnv1gaw3j8gxwh9sxb64im3spa40480lhz") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.6.0 (c (n "libdrm_amdgpu_sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "07g6mj1yz3fb2fdc827prd2sj6wqyplsr0skgdnqmmk40jrmdbhh") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.7.0 (c (n "libdrm_amdgpu_sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1hsk7knsncqdsn1yb265qz2yg58rm4b7xah1f5irg3sil95bxx4k") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libdrm_amdgpu_sys-0.7.1 (c (n "libdrm_amdgpu_sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0nkgk0xmy54b1k6drxny3n95i1y635cibq9jl604s541dkjzndx1") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config")))) (l "drm, drm_amdgpu")))

(define-public crate-libdrm_amdgpu_sys-0.7.2 (c (n "libdrm_amdgpu_sys") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "1ws2nalcrdg49vlhzaivvbfqyhiwkbpf6hjashn16mszdqk1s6gv") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config")))) (l "drm, drm_amdgpu")))

(define-public crate-libdrm_amdgpu_sys-0.7.3 (c (n "libdrm_amdgpu_sys") (v "0.7.3") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "00m8mnhvnwzml0hhnxa1kmzrr2gvmwzvqmm8k7b2p1rb04cwbzxd") (f (quote (("std") ("link-drm") ("default" "std" "link-drm") ("buildtime_bindgen" "bindgen" "pkg-config")))) (l "drm, drm_amdgpu")))

