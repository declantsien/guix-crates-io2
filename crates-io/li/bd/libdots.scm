(define-module (crates-io li bd libdots) #:use-module (crates-io))

(define-public crate-libdots-0.1.0 (c (n "libdots") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0xkc084vihbzlar5yi46qi85gndrdzlprg2dyr8nl7i2aljcsfb8")))

(define-public crate-libdots-0.1.1 (c (n "libdots") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1gvkj2bsym610fivpnh095pki5rjcn72kwdm4k7bw5hkd7gdz0dk")))

(define-public crate-libdots-0.2.0 (c (n "libdots") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0a2a6mxi28axilq2909hhvrysb0n2zwv0q0wsclqqa9ldm58ndrw")))

(define-public crate-libdots-0.3.0 (c (n "libdots") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "uuid") (r "^1.3.2") (d #t) (k 0)))) (h "1s053pzh8jbcfil700xqcsdpx4rx1r810f1vn75dn5q9gj3685mq")))

(define-public crate-libdots-0.3.1 (c (n "libdots") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "uuid") (r "^1.3.2") (d #t) (k 0)))) (h "043xan99b0qy79bw2d0a001c0jrpf9pxd6b7ch0sxhs13bq7j9v3")))

