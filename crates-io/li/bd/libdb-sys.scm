(define-module (crates-io li bd libdb-sys) #:use-module (crates-io))

(define-public crate-libdb-sys-0.1.0 (c (n "libdb-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "0dr3w0217jmldaqzbri978x3yqhjmi57pqnvazhqmqbzwyz84n53") (f (quote (("v5_3") ("v4_8") ("default")))) (l "db")))

(define-public crate-libdb-sys-0.1.1 (c (n "libdb-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)))) (h "1nfj792ydjvslygndc5jm5nbpj3ksa27s6xwc7zj8fki7cpgb03c") (f (quote (("v5_3") ("v4_8") ("default")))) (l "db")))

