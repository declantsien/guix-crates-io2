(define-module (crates-io li bd libdbus-sys) #:use-module (crates-io))

(define-public crate-libdbus-sys-0.1.0 (c (n "libdbus-sys") (v "0.1.0") (d (list (d (n "metadeps") (r "^1") (d #t) (k 1)))) (h "1q9w6lycwsw7wy2ch34yix2rwk93bwvjwlh77hhpmsgijrhlx42m")))

(define-public crate-libdbus-sys-0.1.1 (c (n "libdbus-sys") (v "0.1.1") (d (list (d (n "metadeps") (r "^1") (d #t) (k 1)))) (h "0sjvdqzbcki0gy9fnpm3kbci569nax46i9z06l57i6s65rfwjr77")))

(define-public crate-libdbus-sys-0.1.2 (c (n "libdbus-sys") (v "0.1.2") (d (list (d (n "metadeps") (r "^1") (d #t) (k 1)))) (h "1y9537yg8s0fnv0mjh0bppryb913yaqd62vzvgb8mgbvg5ck53p9")))

(define-public crate-libdbus-sys-0.1.3 (c (n "libdbus-sys") (v "0.1.3") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18c3gvdvv9dcy8jx33azcafxls3qb40i5y8k0fsjq18794kzj847")))

(define-public crate-libdbus-sys-0.1.4 (c (n "libdbus-sys") (v "0.1.4") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "126v84w1w4vymgrn0fr40mkvwrv94yc7g7y5q6nlm5ka2l383iwr") (l "dbus")))

(define-public crate-libdbus-sys-0.1.5 (c (n "libdbus-sys") (v "0.1.5") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "044j33bjl7b59c38m31cjvcq09yq6dcvnp90c94hzl2q6ab8ijqq") (l "dbus")))

(define-public crate-libdbus-sys-0.2.0 (c (n "libdbus-sys") (v "0.2.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mzb9sidl0zwpkznfj7l9y8j4c0f99cmxb49p9igwz3rpsrjqf39") (l "dbus")))

(define-public crate-libdbus-sys-0.2.1 (c (n "libdbus-sys") (v "0.2.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1w06ycq2mw8zfp9j73macgdl8d2881bnxbzdyyxys90ljyya64nw") (l "dbus")))

(define-public crate-libdbus-sys-0.2.2 (c (n "libdbus-sys") (v "0.2.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ss51n616qr36jw34kxvh3m5m6sd7l499xcg7bpj62chmnvvb1f1") (l "dbus")))

(define-public crate-libdbus-sys-0.2.3 (c (n "libdbus-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0.78") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0g7vspa53xd84bz5brwn5qyg0x03xacxhakjvji60klv1bczjr12") (f (quote (("vendored" "cc") ("default" "pkg-config")))) (l "dbus")))

(define-public crate-libdbus-sys-0.2.4 (c (n "libdbus-sys") (v "0.2.4") (d (list (d (n "cc") (r "^1.0.78") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "182qbyd48qcb4sn51hqksbgqq2cvyn1fdr8ahif85jz1a7kpm3cz") (f (quote (("vendored" "cc") ("default" "pkg-config")))) (l "dbus")))

(define-public crate-libdbus-sys-0.2.5 (c (n "libdbus-sys") (v "0.2.5") (d (list (d (n "cc") (r "^1.0.78") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0wjw93q6ckrn8qdrxzdi02f0ma9g7nnlpgkrkcll1mjhnw95a206") (f (quote (("vendored" "cc") ("default" "pkg-config")))) (l "dbus")))

