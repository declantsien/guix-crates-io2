(define-module (crates-io li bd libdwelf) #:use-module (crates-io))

(define-public crate-libdwelf-0.0.1 (c (n "libdwelf") (v "0.0.1") (d (list (d (n "libdw-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1krrkvq424ki4l5lvmkaxplhp3yin2bcylscm4mdv0yvryl0nvxd")))

(define-public crate-libdwelf-0.0.2 (c (n "libdwelf") (v "0.0.2") (d (list (d (n "libdw-sys") (r "^0.0.2") (d #t) (k 0)))) (h "1kqmi63219qbllm3b9qfq9mk3ivqqr7fwfn5n4c6f1vsh6hy57p9")))

(define-public crate-libdwelf-0.0.4 (c (n "libdwelf") (v "0.0.4") (d (list (d (n "libdw") (r "^0.0.4") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.4") (d #t) (k 0)) (d (n "libelf") (r "^0.0.4") (d #t) (k 0)))) (h "0x188mpi95nlj4sjv6kyl6rkhxqrcv9b30kchwdax1aqbb0c276y")))

(define-public crate-libdwelf-0.0.5 (c (n "libdwelf") (v "0.0.5") (d (list (d (n "libdw") (r "^0.0.5") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.5") (d #t) (k 0)) (d (n "libelf") (r "^0.0.5") (d #t) (k 0)))) (h "05cv4i06cv87dagwylkibpbxbgdfghjchjn7j8x71s15wr2vlv7x")))

(define-public crate-libdwelf-0.0.6 (c (n "libdwelf") (v "0.0.6") (d (list (d (n "libdw") (r "^0.0.6") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.0.6") (d #t) (k 0)) (d (n "libelf") (r "^0.0.6") (d #t) (k 0)))) (h "0fa8b2k59l5wmlg8ayq8vv865skhxh4ifj8acc0jpi3jiwgr8dh0")))

(define-public crate-libdwelf-0.1.0 (c (n "libdwelf") (v "0.1.0") (d (list (d (n "libdw") (r "^0.1.0") (d #t) (k 0)) (d (n "libdw-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libelf") (r "^0.1.0") (d #t) (k 0)))) (h "11lsi8zm8zdvmqgndb0plfsl5f5kx3s68bai0412vvjs0q32dmic")))

