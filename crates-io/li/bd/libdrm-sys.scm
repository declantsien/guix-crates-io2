(define-module (crates-io li bd libdrm-sys) #:use-module (crates-io))

(define-public crate-libdrm-sys-0.1.4 (c (n "libdrm-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0410sh7fr70yx4j9bh32x9bx1kpmgm3qyyv5jjharhzvnqgsj6vq")))

(define-public crate-libdrm-sys-0.1.5 (c (n "libdrm-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12izdr8mbr9j9h44cbp096iwcsgdxaqc2w8p5bxlqr3lc9mw0c11")))

(define-public crate-libdrm-sys-0.2.0 (c (n "libdrm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0w3f7za081q5qg0gb09d9qxjh9h71iviw37n7vakgx3zarmz0mk3") (f (quote (("version_2_4_67" "version_2_4_58") ("version_2_4_58") ("default" "version_2_4_58"))))))

(define-public crate-libdrm-sys-0.2.1 (c (n "libdrm-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.29") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0a4kv4vvv0wpi0icdn9sin6f640sba0anz4ws00a2xw0girval89") (f (quote (("version_2_4_67" "version_2_4_58") ("version_2_4_58") ("default" "version_2_4_58"))))))

