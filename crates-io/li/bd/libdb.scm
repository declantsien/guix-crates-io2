(define-module (crates-io li bd libdb) #:use-module (crates-io))

(define-public crate-libdb-0.1.0 (c (n "libdb") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "libdb-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "01zv2x5dnx5rzr1716q2x583zhz7lkgd2jnp7ng7rvxzs5hjkpj5") (f (quote (("v5_3" "libdb-sys/v5_3") ("v4_8" "libdb-sys/v4_8"))))))

(define-public crate-libdb-0.1.1 (c (n "libdb") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "libdb-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)))) (h "1mrvrza8dqwgb7yk6lxkg9bdqkd9q9q302ic742ra41b915klwm9") (f (quote (("v5_3" "libdb-sys/v5_3") ("v4_8" "libdb-sys/v4_8"))))))

