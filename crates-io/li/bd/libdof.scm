(define-module (crates-io li bd libdof) #:use-module (crates-io))

(define-public crate-libdof-0.1.0 (c (n "libdof") (v "0.1.0") (h "033nmp4hz5jlz8y0zynychckj4sswzgzhb4j357igqb5hmcjv947")))

(define-public crate-libdof-0.2.0 (c (n "libdof") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1gy1yp49jqagy383y5b67ckcw7dmxcskdmajf6ap8xkv2hp2d665")))

(define-public crate-libdof-0.3.0 (c (n "libdof") (v "0.3.0") (d (list (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0my8nsmy7hh97m2djzfgzw50flfzzf8i1ry3kzcdhd1qxrx1yl1v")))

(define-public crate-libdof-0.5.0 (c (n "libdof") (v "0.5.0") (d (list (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "1k0fjj07fgirii3j0g0gzhj5gdy4bc9q02v220zpqb4p5kd9fdv5")))

(define-public crate-libdof-0.5.1 (c (n "libdof") (v "0.5.1") (d (list (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "01xpx17bsski3dmhwlayf8snhkgyf0jx1hy3jw0nwl67f853rkki")))

(define-public crate-libdof-0.6.0 (c (n "libdof") (v "0.6.0") (d (list (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "0w20hzwpfzh63saighp10gx75za6xjfsap4n79z8r2jm9949kz73")))

(define-public crate-libdof-0.7.0 (c (n "libdof") (v "0.7.0") (d (list (d (n "serde") (r "^1.0.153") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "serde_with") (r "^2.2.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.39") (d #t) (k 0)))) (h "175cycfb30ij3s7x11zd7ki83yjmvwm8a2rdczdq5mbsckfszxrb")))

(define-public crate-libdof-0.8.0 (c (n "libdof") (v "0.8.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ii9y7hj6s7gm4vq0rs6vmk8dszx862l80n2j9gi60p08hi2bmfv")))

(define-public crate-libdof-0.8.1 (c (n "libdof") (v "0.8.1") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1p1ja7yfrrgbf92071vdixihbflq3ra01v0d2vfsj31vqvyxw6vc")))

(define-public crate-libdof-0.9.0 (c (n "libdof") (v "0.9.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "01ccr5i5nf36b4pjhl1cw3bgih4073kiih7ya0596lnvgn1y7mmd")))

(define-public crate-libdof-0.10.0 (c (n "libdof") (v "0.10.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "13zc5m00mj5g0yqg4kg86ayz7wjxyq17l3dswlhv35xxncmaralp")))

(define-public crate-libdof-0.11.0 (c (n "libdof") (v "0.11.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "10dm6w5ja7rjpn8vahfzk1rqzqsw0ys6b8r68mbvf7w834kaa7d9")))

(define-public crate-libdof-0.12.0 (c (n "libdof") (v "0.12.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0s4yqjvwx1xbw6kxk6fi75c1a4ciwdwbksx7llbdhp5da08m7wjh")))

(define-public crate-libdof-0.13.0 (c (n "libdof") (v "0.13.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1mqcf4hzkaf5wj6fci4r1wlkw70n6p964c3v3ggcqdnqkqfyjw1x")))

(define-public crate-libdof-0.13.1 (c (n "libdof") (v "0.13.1") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0l6ajz4a30mmyl3qfj1scqnqddfxfc9jaqc1761n84b2zawdpd5a")))

(define-public crate-libdof-0.13.2 (c (n "libdof") (v "0.13.2") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1n3h87pfxldq11c1751n3cy42baprh5jdf5lyqsh9hyd8jdr5csd")))

(define-public crate-libdof-0.13.3 (c (n "libdof") (v "0.13.3") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "07hw4135n4f4w7cswqnz7fj0hkpkqpsfjgs4cz1bpj0ps1d9gvx6")))

(define-public crate-libdof-0.14.0 (c (n "libdof") (v "0.14.0") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0gv41dl1ah5wxqr33xb2ya37kz26bj6470x7qdw2xpf91w0460qp")))

(define-public crate-libdof-0.14.1 (c (n "libdof") (v "0.14.1") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "10ps3k9l15w8h0a5nizbby8dlwmgw33xld6y5zfdhjjadds6dz3n")))

(define-public crate-libdof-0.14.2 (c (n "libdof") (v "0.14.2") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0wyrj6q3nvfh7ghdgw1qcs909q0annygam4yjf2ch4nizirjdzml")))

(define-public crate-libdof-0.14.3 (c (n "libdof") (v "0.14.3") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0g1g50jv14xs9fk7jnvy632y3x85vq44cix8isar824525s3cdgx")))

(define-public crate-libdof-0.14.4 (c (n "libdof") (v "0.14.4") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0qg7n7359py1n50im6nc6dcb6qb0xddd7rfzqdhmy8krk4q2s3cf")))

(define-public crate-libdof-0.14.5 (c (n "libdof") (v "0.14.5") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0szp4iy63ckrr41lbkalnfrigaq0m3sa8nsrsgk81sb7p7hk85c5")))

(define-public crate-libdof-0.14.6 (c (n "libdof") (v "0.14.6") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0q4yyijhawlpbrw2pm1rlxhdz9vag51f9k8i22523l94cgnmp1zm")))

(define-public crate-libdof-0.14.7 (c (n "libdof") (v "0.14.7") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0xf6ksy47xycjk2lcqbcqrg40kd4lras26rb0zlc49avijsn2arv")))

(define-public crate-libdof-0.14.8 (c (n "libdof") (v "0.14.8") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1qfv1j4ak3l64sx98akgn3qr5rx8457znlqcd2rxawy7v5q0kl8z")))

(define-public crate-libdof-0.14.9 (c (n "libdof") (v "0.14.9") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1ppfskyl05fj2p3y2vxpjcypj0mb4nbr7iz15f3s7ay1n9w7mvpa")))

(define-public crate-libdof-0.14.10 (c (n "libdof") (v "0.14.10") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "033cicc22d45nqq5dsga1fhfjsxjqf7jsmzp6413fssm6ksq82xk")))

(define-public crate-libdof-0.14.11 (c (n "libdof") (v "0.14.11") (d (list (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.4.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "15gpq5q3a4l8p76cpn6f817xpr1wx206403dx2w8ai8pw7ggy69w")))

(define-public crate-libdof-0.14.12 (c (n "libdof") (v "0.14.12") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_with") (r "^3.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "14prh8s744cqz2msgky34dn9m0m2mp9hm5skspzd6anbcq45qv59")))

(define-public crate-libdof-0.15.0 (c (n "libdof") (v "0.15.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "serde_with") (r "^3.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "10w9jg9f4n9bm8pcfwgvr668w0l6h7px9g2xlzy02ww5040zp6qg")))

(define-public crate-libdof-0.15.1 (c (n "libdof") (v "0.15.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_with") (r "^3.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d9c8mdxy57qyva77hpz6zi54jsr2jsksqsxw6vxkgjr9lmicysh")))

(define-public crate-libdof-0.16.0 (c (n "libdof") (v "0.16.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "serde_with") (r "^3.6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s8y1bp8hnxksf9xhz2jgj9yh118nf1ym5wf5rnmnkg4jlw7a7k7")))

