(define-module (crates-io li bd libdx) #:use-module (crates-io))

(define-public crate-libdx-0.1.0 (c (n "libdx") (v "0.1.0") (h "03qlanaqn631rbkv0wvpydfz2lw1lpb9a29vdzzxjjfjymglwaya")))

(define-public crate-libdx-0.1.1 (c (n "libdx") (v "0.1.1") (h "05mmbzx06pb7lgwy7n2qh9hr7dq92dh3qb8dbgvn755k85cq969d")))

(define-public crate-libdx-0.2.0 (c (n "libdx") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0wgmf5d816xmn01fg8xi59zfdr7n6smzi498ddk4yfl6isxlzhr8")))

(define-public crate-libdx-0.3.0 (c (n "libdx") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "06avv7agymcg98w16jdg6qgkzhwg04xyijfhbldsvqd4646rppb7")))

(define-public crate-libdx-0.4.0 (c (n "libdx") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0s3rlgyj9s80blhcbvh41qzyqkfpz64nc0ggnd7kjzvib6vmv8m5")))

(define-public crate-libdx-0.4.1 (c (n "libdx") (v "0.4.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "02fjiaxkn1jajn24aklccvllgpw1686pmayxjq22r495s7bccdah")))

(define-public crate-libdx-1.0.0 (c (n "libdx") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0pqq9dnhmxybaswf4lms2l0vj2rgfy1cxcw9h2b6k3qnr9vqvnmx")))

(define-public crate-libdx-2.0.0 (c (n "libdx") (v "2.0.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "145v4ill0k7ph2jlfmvbcj4n4w9s2833z6h8xcgqs7bybyss7wp4") (y #t)))

(define-public crate-libdx-2.1.0 (c (n "libdx") (v "2.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1g8hyz18lljivbi6w8hq648dnb5fbyxwkbxqaszsdffchj250jl7")))

(define-public crate-libdx-2.1.1 (c (n "libdx") (v "2.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "130v2rdp9081lix8mwdssaxfzgjhnsh8b5d0nixy10mdgbpk1143")))

(define-public crate-libdx-2.1.2 (c (n "libdx") (v "2.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0qjrw23x1d12ajxmldmfzirpfwyydjsgrb6s9bgvbl3sbmmmn8bm")))

