(define-module (crates-io li bd libdpdk-sys) #:use-module (crates-io))

(define-public crate-libdpdk-sys-0.1.0 (c (n "libdpdk-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "1kp9fg0ckfgpis0g49rm7vvpavbbk8lvrg5da4v57wv50hqa5cqd")))

