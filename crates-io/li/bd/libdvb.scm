(define-module (crates-io li bd libdvb) #:use-module (crates-io))

(define-public crate-libdvb-0.2.1 (c (n "libdvb") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nz9ffll8xb0rk7dx68cijr7h4ffv7aq2jn8bl7fyh4z16a2gqrd")))

(define-public crate-libdvb-0.2.2 (c (n "libdvb") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ibjyxy1spmw0x1rl2p2pwrlradk2qmhmqxf9a531lgd0c9wr1zv")))

(define-public crate-libdvb-0.3.0 (c (n "libdvb") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)))) (h "115n1vvwfmb1kiy36magnjzkj0dwqyy966871ig4l0f1mwb5m969")))

