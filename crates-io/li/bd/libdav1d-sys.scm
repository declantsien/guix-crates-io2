(define-module (crates-io li bd libdav1d-sys) #:use-module (crates-io))

(define-public crate-libdav1d-sys-0.1.0 (c (n "libdav1d-sys") (v "0.1.0") (h "1akpqpr6daczlscyr1nrxrxivl8hm6h0qiy112d004k1yicfsycw") (l "dav1d")))

(define-public crate-libdav1d-sys-0.1.1 (c (n "libdav1d-sys") (v "0.1.1") (h "06yfd8d6ih0kpb2p6di6qlfwldww5rc04m6q4w423i53x1vgr565") (l "dav1d")))

(define-public crate-libdav1d-sys-0.2.0 (c (n "libdav1d-sys") (v "0.2.0") (h "1ydv9l3s255hwpw0nsdyw1isz599jm8kh8b10pj3vbgg98m9rmn5") (l "dav1d")))

(define-public crate-libdav1d-sys-0.3.0 (c (n "libdav1d-sys") (v "0.3.0") (h "04frz9rgzqn102i4ykalp6c1hlakjl4xk7ix0xv0znxxv761wj7h") (l "dav1d")))

(define-public crate-libdav1d-sys-0.4.0 (c (n "libdav1d-sys") (v "0.4.0") (h "07wawdjjp7qz6cxkkknr2syxzczjawx8447c499522ay4p2fxmsc") (l "dav1d")))

(define-public crate-libdav1d-sys-0.4.1 (c (n "libdav1d-sys") (v "0.4.1") (h "0vjqwmhhnvnmw7bbilgqhvl61knpmj5b3r6w6iakncz8x1zgy8jp") (l "dav1d")))

(define-public crate-libdav1d-sys-0.5.0 (c (n "libdav1d-sys") (v "0.5.0") (h "1kqz0zify2m4nmlp4d5jm0s119g5cc80gyhjvdgfd6mna77w0j94") (l "dav1d")))

(define-public crate-libdav1d-sys-0.5.1 (c (n "libdav1d-sys") (v "0.5.1") (h "1hn0hq7xcvxf53mg2rnx48pnym8q4pdi87rkyjg7m9vkj6zasl3f") (y #t) (l "dav1d")))

(define-public crate-libdav1d-sys-0.5.2 (c (n "libdav1d-sys") (v "0.5.2") (h "04mzk3920fhg2jz9jc19lm7arl6a3vixvzmpkwpjg45k218xwcn2") (y #t) (l "dav1d")))

(define-public crate-libdav1d-sys-0.5.3-alpha.1 (c (n "libdav1d-sys") (v "0.5.3-alpha.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f8y5cnv3p45dg26fdv6ivn9wwisn8c5wsb2f65wg06i5qrv04hq") (l "dav1d")))

(define-public crate-libdav1d-sys-0.5.3-alpha.2 (c (n "libdav1d-sys") (v "0.5.3-alpha.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r1pcyjbs30lh1lv3ra4ybrcl9pkhz662yk16wj6jg1lq323yd27") (l "dav1d")))

(define-public crate-libdav1d-sys-0.5.3 (c (n "libdav1d-sys") (v "0.5.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0z2b32nski0l87vcyyv7wnwfvfd1cq9kbqvwr5jkmcyv1gz5a7f2") (l "dav1d")))

(define-public crate-libdav1d-sys-0.6.0 (c (n "libdav1d-sys") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wh5jgdm33ld6djxsc7cmwd1ifqys145zlbsf8516n625lscrj8j") (l "dav1d")))

(define-public crate-libdav1d-sys-0.7.0+libdav1d.1.4.0 (c (n "libdav1d-sys") (v "0.7.0+libdav1d.1.4.0") (d (list (d (n "bindgen") (r "^0.69.4") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wfa59llf18klfbhz8brg8d5p7plrdac9jb74nkarxz0vhn95pmh") (l "dav1d") (s 2) (e (quote (("generate" "dep:bindgen"))))))

