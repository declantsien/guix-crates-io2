(define-module (crates-io li bd libdivide) #:use-module (crates-io))

(define-public crate-libdivide-0.4.0 (c (n "libdivide") (v "0.4.0") (d (list (d (n "num-integer") (r "^0.1.44") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "0a48540aajvlnw8dkyl5pbmmg9pnadl15i7apd0l28i8svd9lal1")))

