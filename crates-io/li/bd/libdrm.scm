(define-module (crates-io li bd libdrm) #:use-module (crates-io))

(define-public crate-libdrm-0.0.2 (c (n "libdrm") (v "0.0.2") (d (list (d (n "drm-sys") (r "^0.0.3") (d #t) (k 0)) (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "smallvec") (r "^0.3.1") (d #t) (k 0)))) (h "0b1lpqa345b9piy7qr9lzysj4yqyjl8yr32vqin558kh0n6xssf8")))

