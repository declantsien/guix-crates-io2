(define-module (crates-io li bd libdatachannel) #:use-module (crates-io))

(define-public crate-libdatachannel-0.1.0+0.20.1 (c (n "libdatachannel") (v "0.1.0+0.20.1") (d (list (d (n "libdatachannel-sys") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0zwdlhzg0ak0j8g7sqcz6aq20lj0xm08y1dil61xi8s4zg170bqy")))

(define-public crate-libdatachannel-0.2.0+0.20.1 (c (n "libdatachannel") (v "0.2.0+0.20.1") (d (list (d (n "libdatachannel-sys") (r "^0.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "test-log") (r "^0.2") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0k38h7f9a1sw6gilkac97qxkk1f6gasfwcpms50j9sdysl2wvxji")))

