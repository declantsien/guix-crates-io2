(define-module (crates-io li bd libddsc-sys) #:use-module (crates-io))

(define-public crate-libddsc-sys-0.1.0 (c (n "libddsc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "1cc4x3k1i7vh9f6hg7gbq3grk4s85q4hyfrryyjl1bds1md6wjck") (l "ddsc")))

(define-public crate-libddsc-sys-0.1.2 (c (n "libddsc-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "13c7yq4hcnv1ii9nfa6sdvmzv4vxxs5n4hf2swvyjaw3f46p9amc") (l "ddsc")))

