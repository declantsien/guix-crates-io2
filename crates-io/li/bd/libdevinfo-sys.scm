(define-module (crates-io li bd libdevinfo-sys) #:use-module (crates-io))

(define-public crate-libdevinfo-sys-1.0.0 (c (n "libdevinfo-sys") (v "1.0.0") (h "0p6pn23n9dzwya3lnxap64m1k984yk77nkz93hpxgpvlx7n00hm8") (f (quote (("private") ("default"))))))

(define-public crate-libdevinfo-sys-1.1.0 (c (n "libdevinfo-sys") (v "1.1.0") (h "1hffcfkjggihgmyg40xi5bbf45vdvgrhddczjmfljygjv9ajjbg9") (f (quote (("private") ("default"))))))

