(define-module (crates-io li bd libdt-macros) #:use-module (crates-io))

(define-public crate-libdt-macros-1.0.0 (c (n "libdt-macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11lifycz93957ivgxm0iv5v0qsz5sqdgy0mkk31h1kc4w6p44b9p")))

(define-public crate-libdt-macros-2.0.0 (c (n "libdt-macros") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0ayvlr3fwwawvy07zc7fi4mbd6jcn1hgc2zj76v7dix4sk79bf87")))

(define-public crate-libdt-macros-2.0.1 (c (n "libdt-macros") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "109lzvqyh4kb92dsb1vr9fyyayn89jm17csnnpqd94q1a0qjrl4k")))

(define-public crate-libdt-macros-2.0.2 (c (n "libdt-macros") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1rc6snsqid05dcfyjr26cskzrnqa68kmvra42zd3rbn4wck2qm8m")))

