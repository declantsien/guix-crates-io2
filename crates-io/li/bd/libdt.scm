(define-module (crates-io li bd libdt) #:use-module (crates-io))

(define-public crate-libdt-2.0.0 (c (n "libdt") (v "2.0.0") (d (list (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "libdt-macros") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "libdt-macros") (r "^2.0.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0rf887nf4k08lwvc15qxcw4kxjq5602nq1094f1mpfnha68a7hv7") (f (quote (("macros" "libdt-macros")))) (y #t)))

(define-public crate-libdt-2.0.1 (c (n "libdt") (v "2.0.1") (d (list (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "libdt-macros") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "libdt-macros") (r "^2.0.0") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0bv1nvwxg6zqhhjgn51ss4b1xjjfz88m1w4s8grpf7n3kbah5haj") (f (quote (("macros" "libdt-macros")))) (y #t)))

(define-public crate-libdt-2.0.2 (c (n "libdt") (v "2.0.2") (d (list (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "libdt-macros") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "libdt-macros") (r "^2.0.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0p1yq4zybr9ijjny56b5p03k19kwkmkhgmpjrl8x9rmjmzb59rpk") (f (quote (("macros" "libdt-macros")))) (y #t)))

(define-public crate-libdt-2.0.3 (c (n "libdt") (v "2.0.3") (d (list (d (n "float_eq") (r "^1.0.1") (d #t) (k 2)) (d (n "libdt-macros") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "libdt-macros") (r "^2.0.2") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0nhr1s2idwjmpgchqdzmwvmqdyw3hb15hx8sqv4an94sk6ck7cnh") (f (quote (("macros" "libdt-macros"))))))

