(define-module (crates-io li by libyubihsm) #:use-module (crates-io))

(define-public crate-libyubihsm-0.1.0 (c (n "libyubihsm") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "06ych9pcs9ixrd04vnkfyz2n51yfq37yy63k7ld6bxx631ziak40")))

(define-public crate-libyubihsm-0.2.0 (c (n "libyubihsm") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "0l5gl9ybf9b7l6x72kvpkybdayj2y7mg7h1v9rjhsdgf7h2bxjzx")))

(define-public crate-libyubihsm-0.2.1 (c (n "libyubihsm") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.32") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)))) (h "1f6z0mlbj99h2jp2yv7wl6ypkji1c3sm6g87v2knc9yjlcp304mx")))

