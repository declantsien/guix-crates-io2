(define-module (crates-io li by libykpers-sys) #:use-module (crates-io))

(define-public crate-libykpers-sys-0.1.0 (c (n "libykpers-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "089z7cnk75cx4fggix4rdhfpdc5aqrs2x0pscmrbg322pp45aykb")))

(define-public crate-libykpers-sys-0.2.0-beta1 (c (n "libykpers-sys") (v "0.2.0-beta1") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1lw0ik6yxji9bwrwm9li6j5d1zlfg0kh861h2gg30im4dlkxxg48") (l "ykpers-1")))

(define-public crate-libykpers-sys-0.3.0 (c (n "libykpers-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.21") (d #t) (k 1)))) (h "08fy0mg7hc9w19g2g3k0sqwy07cl5dl2i46cap1510lh6nmvw49z") (l "ykpers-1")))

(define-public crate-libykpers-sys-0.3.1 (c (n "libykpers-sys") (v "0.3.1") (d (list (d (n "libc") (r "^0.2.104") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.21") (d #t) (k 1)))) (h "0r1h0yly2aj106xz8sj9sk3rrbcjg7ca3m3r621msp4sd5gajcw0") (l "ykpers-1")))

