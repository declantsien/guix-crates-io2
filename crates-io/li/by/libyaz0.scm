(define-module (crates-io li by libyaz0) #:use-module (crates-io))

(define-public crate-libyaz0-0.1.0 (c (n "libyaz0") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5.1") (d #t) (k 2)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1k0q71w1502bka00sjw04hvjjsl23sy0xd532ly7rbnzwav6cfh6")))

