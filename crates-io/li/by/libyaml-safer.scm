(define-module (crates-io li by libyaml-safer) #:use-module (crates-io))

(define-public crate-libyaml-safer-0.1.0 (c (n "libyaml-safer") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "unsafe-libyaml") (r "^0.2.10") (d #t) (k 2)))) (h "1sw2agv49rfwa8wsvgj2f49fij0nmhwq1blhjpvbm1ws5z5q8xmf") (r "1.70")))

(define-public crate-libyaml-safer-0.1.1 (c (n "libyaml-safer") (v "0.1.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.0") (d #t) (k 2)) (d (n "unsafe-libyaml") (r "^0.2.10") (d #t) (k 2)))) (h "1mjvbw9i078w4i5hvp93jdmlklmicpivrlbykcf8aa48l3529zrc") (r "1.70")))

