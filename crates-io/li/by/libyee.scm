(define-module (crates-io li by libyee) #:use-module (crates-io))

(define-public crate-libyee-0.0.1 (c (n "libyee") (v "0.0.1") (d (list (d (n "enum-iterator") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0nh57qqnwl1za3ff43638pzi9difhsg844cxdh5bxl0anpd3zyv2")))

(define-public crate-libyee-0.0.2 (c (n "libyee") (v "0.0.2") (d (list (d (n "enum-iterator") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "09mixc96prb5h0glayb4y4zbvhxdb30h2jpwcgyzy22cgy98jwvd")))

