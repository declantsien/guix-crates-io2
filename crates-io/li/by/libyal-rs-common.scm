(define-module (crates-io li by libyal-rs-common) #:use-module (crates-io))

(define-public crate-libyal-rs-common-0.2.0 (c (n "libyal-rs-common") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1lglfd8gkcpkyl65swx92ybxw1dgjp1irc1d40jikng8pv5avkm6") (f (quote (("default"))))))

(define-public crate-libyal-rs-common-0.2.1 (c (n "libyal-rs-common") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1dn18jlnx4mnyykrqhalp5yww47d76vjjxj43wqn5za4acd4gjh2") (f (quote (("default"))))))

(define-public crate-libyal-rs-common-0.2.2 (c (n "libyal-rs-common") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1m7bls2qlcrd6bhhmhl1z5j05wlq1vvgw8v0an317846yhgzhb7c") (f (quote (("default")))) (y #t)))

(define-public crate-libyal-rs-common-0.2.3 (c (n "libyal-rs-common") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1d2qi6xza56mm08qw5va5y3vbh4ws6nwmal8f121vjka2mky7jck") (f (quote (("default"))))))

(define-public crate-libyal-rs-common-0.2.4 (c (n "libyal-rs-common") (v "0.2.4") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1ijkb8h0dr6z4h8qvd675008rgwbfbpak740pmgxdq6va63c64w6") (f (quote (("default"))))))

(define-public crate-libyal-rs-common-0.2.5 (c (n "libyal-rs-common") (v "0.2.5") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.6.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)))) (h "1jz3w4pgn5yf8g0j6w9jsjqx8sfzhyg0fj3l2mxik216mpikjf1p") (f (quote (("default"))))))

