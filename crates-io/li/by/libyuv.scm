(define-module (crates-io li by libyuv) #:use-module (crates-io))

(define-public crate-libyuv-0.1.0 (c (n "libyuv") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "15axd85snf4r5d1zdn4nfcz1c3axmk2m495gbbf7j4al9lydixy1")))

(define-public crate-libyuv-0.1.1 (c (n "libyuv") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "1fc9z7mgh9s9z266gswndz4bsmpkwh9la04nd6nf4wrvckv1r4gy")))

(define-public crate-libyuv-0.1.2 (c (n "libyuv") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.121") (d #t) (k 0)))) (h "1jwyfw40fj1bcdzfx0n8jnp9c1xl1a2dsbdpdijbmqp77mfkz7la")))

