(define-module (crates-io li no linoleum) #:use-module (crates-io))

(define-public crate-linoleum-2.0.0 (c (n "linoleum") (v "2.0.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "1q8zn5hffjjpsbzjwpgicd02n2d15wsv84kq0fn5m5v4f4f1lpas")))

(define-public crate-linoleum-2.2.0 (c (n "linoleum") (v "2.2.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "1vil4ymq3lji4cwi5rkd78qf72bf9nx8a5a2h528h5p53gmb6yh5")))

(define-public crate-linoleum-2.2.1 (c (n "linoleum") (v "2.2.1") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "1chib0hgqll147nk3gn7jrfv9sxksk7khdlcnxd7byza8ql8r7sb")))

(define-public crate-linoleum-2.3.0 (c (n "linoleum") (v "2.3.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "strip-ansi-escapes") (r "^0.2.0") (d #t) (k 0)))) (h "0nnj4pjq0da0zgvifvq87qmka2grf3ph3h05km5cf4ac8dhgh98g")))

(define-public crate-linoleum-2.4.0 (c (n "linoleum") (v "2.4.0") (d (list (d (n "crossterm") (r "^0.27") (d #t) (k 0)) (d (n "regex-lite") (r "^0.1.0") (d #t) (k 0)))) (h "1gk0cj4lypyxsqcvmni55p5k9zvrn0pswr4rvv31zawsm54m66iw")))

(define-public crate-linoleum-2.4.1 (c (n "linoleum") (v "2.4.1") (d (list (d (n "antsy") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "17r97q2965miiflqsig8d44c7vidgnxdn59f291h8hl3lkp34d2m")))

(define-public crate-linoleum-2.4.2 (c (n "linoleum") (v "2.4.2") (d (list (d (n "antsy") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "1hvygnlvbz4z3aqs4qykyy94qcsv03hsaawn1jldqsjkcwwwkl3z")))

(define-public crate-linoleum-2.5.0 (c (n "linoleum") (v "2.5.0") (d (list (d (n "antsy") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "12hmi9yxjryrdwxp42g9m6zbk9ygms1y6kphg0fshlhiw929ynq3")))

(define-public crate-linoleum-2.5.1 (c (n "linoleum") (v "2.5.1") (d (list (d (n "antsy") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "0znvf41aisg7cl0gfxxbghpy2l0wjpskcww45jbp1k7q5lh97f5z")))

(define-public crate-linoleum-2.5.2 (c (n "linoleum") (v "2.5.2") (d (list (d (n "antsy") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "01firhiwxhcindchz2pbih35ggypyz9b4hzgpp7d015v1x1a3a4c")))

(define-public crate-linoleum-3.0.0 (c (n "linoleum") (v "3.0.0") (d (list (d (n "antsy") (r "^1.0.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27") (d #t) (k 0)))) (h "1ynq6n2m1z433ry75k3cds4n1f4glxm6pxd2k3xnjf037psbfwkm")))

