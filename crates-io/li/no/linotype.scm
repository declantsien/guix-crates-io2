(define-module (crates-io li no linotype) #:use-module (crates-io))

(define-public crate-linotype-0.0.1 (c (n "linotype") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "higher-order-closure") (r "^0.0.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)) (d (n "tap") (r "^1.0.1") (d #t) (k 0)) (d (n "typed-arena") (r "^2.0.1") (k 0)) (d (n "version-sync") (r "^0.9.3") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.28") (d #t) (k 2)))) (h "07ifkba5jm9ikanh8v81r40jnza7vm0a3cl0zdb9bmwcri5mspr6") (f (quote (("std"))))))

