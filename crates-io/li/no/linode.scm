(define-module (crates-io li no linode) #:use-module (crates-io))

(define-public crate-linode-0.0.1 (c (n "linode") (v "0.0.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "svix-ksuid") (r "^0.8.0") (d #t) (k 0)))) (h "1vcmwrd577009jfc1lc95812rgw928wf1qxpcz0c14zxyf1v3mm8")))

