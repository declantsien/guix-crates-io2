(define-module (crates-io li no lino) #:use-module (crates-io))

(define-public crate-lino-0.1.0 (c (n "lino") (v "0.1.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)))) (h "0bxknf43f7m7lg4jp3faf0gxa1s162pbdpc046g092dskqb0hhjg")))

(define-public crate-lino-0.1.1 (c (n "lino") (v "0.1.1") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)))) (h "0z5q8i20fda7yavpaj2876k4f804bb0ak1zr1zzhfdky0bn6jl3q")))

(define-public crate-lino-0.2.0 (c (n "lino") (v "0.2.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)))) (h "1zzinrdyj152fd9whm1pi90lv7a93bzk5gmvfdd0fm75g3y0dng5")))

(define-public crate-lino-0.2.1 (c (n "lino") (v "0.2.1") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)))) (h "0n2afbixxkdlnk74lz5kj6h2nxsmn738567iaq8nbdwlg4abv93a")))

(define-public crate-lino-0.3.0 (c (n "lino") (v "0.3.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)))) (h "13c5y3bxw493zd6i4m0f7vza52xqa000k9cbz2jk2f2ksxn1pxjm")))

(define-public crate-lino-0.4.0 (c (n "lino") (v "0.4.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "015bhm2nkdhhmnz26wxi5cxgcywm8bf0nv6lwdff3nf60cv5787y")))

(define-public crate-lino-0.4.1 (c (n "lino") (v "0.4.1") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.18.2") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "1xjpbbmqsv99l81s1dy5yll7hsih8pag2hq5khfw4d9gbyzikzhg")))

(define-public crate-lino-0.4.2 (c (n "lino") (v "0.4.2") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "1qf5sbqhsfyyp9vk8h6m8vh8z2mqgbf13lr8k6n9smh0k9551dg2")))

(define-public crate-lino-0.4.3 (c (n "lino") (v "0.4.3") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "11h6lb74d6g7mfq3ig4g557c77zf7ym0584v0m0k602qxcl30mrj")))

(define-public crate-lino-0.4.4 (c (n "lino") (v "0.4.4") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "1iwzz8hz0p730zmgrlxbrr9lbmhbgqkm4ychd9gvgqbjw1mgv6nh")))

(define-public crate-lino-0.4.5 (c (n "lino") (v "0.4.5") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "0ys3lmgvkb71a55ach2p92w8pfaf9svdf4z3xnbn9qp5aimwffk8")))

(define-public crate-lino-0.4.6 (c (n "lino") (v "0.4.6") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "1hm22xj570ql2h768xl118ccj1dpmkl980a40hr0wgq20hc1j1y4")))

(define-public crate-lino-0.4.7 (c (n "lino") (v "0.4.7") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "18z755rzk9q8jhfnpjc7l4y70hhir2syg9wjl539wx9fcsb6czr1")))

(define-public crate-lino-0.4.8 (c (n "lino") (v "0.4.8") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "0zc6lmz4ds77sgmg8lby087n0d34v1g94d9dfh0dqg2l23dwisa6")))

(define-public crate-lino-0.4.9 (c (n "lino") (v "0.4.9") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "1lyh8qnc3hk5kybsfyfyh0l3pkw3cyjpg1hrf93m63rr2l25ghgm")))

(define-public crate-lino-0.5.0 (c (n "lino") (v "0.5.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "03nx5vmw0w9fbxfr8gjam813wgh3rd0zm1gr7mh4j2wl18m9hgwz")))

(define-public crate-lino-0.5.1 (c (n "lino") (v "0.5.1") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "0qy45w1rhaxyjhhy1685f624fn0yp5ixnl1k5ysgfpgm8c2ii15s")))

(define-public crate-lino-0.5.2 (c (n "lino") (v "0.5.2") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "0xh3xvd0n7adaly1fmhibww1cxdgxczb77cnv3q6d61zm8wn1n2c")))

(define-public crate-lino-0.5.3 (c (n "lino") (v "0.5.3") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "0y5255g3a17rjlmbgn375wb0qaq7xsy4069cm6fsf4lkljnhdcmg")))

(define-public crate-lino-0.6.0 (c (n "lino") (v "0.6.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "1arvjhx3id2a20zl1rvcqjz8xzr37sw81yqnb4lgvd14bz2zrg5y")))

(define-public crate-lino-0.7.0 (c (n "lino") (v "0.7.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "0xwa2nimpscnw0fd5r96rbqjk245b2gik7zacp2q9h8bc9jgvn9j")))

(define-public crate-lino-0.7.1 (c (n "lino") (v "0.7.1") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "0d8s02jfwx95rmd8j98mipc4siw7gig1bf052svlmkm0mysnlxsh")))

(define-public crate-lino-0.8.0 (c (n "lino") (v "0.8.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "120zgv57h0ibq5m4axpx7krfq5863l5zgqcrqyabvbb0ykblsavv")))

(define-public crate-lino-0.8.1 (c (n "lino") (v "0.8.1") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "0xkmvy72srx4a7vc86bjw8d3yhr0b1b9mflw9s2dnkpkn9yk9zz7")))

(define-public crate-lino-0.8.2 (c (n "lino") (v "0.8.2") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)))) (h "17hwnr1ijvxy5ww38cdmvpdybwicmc88kgh7xfb386kwd906n6kg")))

(define-public crate-lino-0.9.0 (c (n "lino") (v "0.9.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "02x43a54y4fmi1np2ywr2vc1v14i152vzix46acwq2p4954935wg")))

(define-public crate-lino-0.9.1 (c (n "lino") (v "0.9.1") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1l6rmdc65npr2vy5hl8in8f2mmwy1q0pjd2fpzyidk2jkfjx5w1s")))

(define-public crate-lino-0.9.2 (c (n "lino") (v "0.9.2") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (f (quote ("default-onig"))) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "00hwb3kh7vv3dzamgpnickgdgrd8d0yaxigfx9d2apakx0krhh7b")))

(define-public crate-lino-0.10.0 (c (n "lino") (v "0.10.0") (d (list (d (n "copypasta") (r "^0.7.1") (d #t) (k 0)) (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "ctrlc") (r "^3.1.7") (d #t) (k 0)) (d (n "syntect") (r "^4.5.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "11fziq4x9csx61h5p7qwq61g726aybialgg3v94s9i2mhihcq643")))

