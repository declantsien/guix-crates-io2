(define-module (crates-io li no linode-api) #:use-module (crates-io))

(define-public crate-linode-api-0.0.0 (c (n "linode-api") (v "0.0.0") (h "115f800jkh8i75l76n9i8sdnzin9157qcrixwgidj7f1ilzfv9fs")))

(define-public crate-linode-api-0.0.1 (c (n "linode-api") (v "0.0.1") (d (list (d (n "http") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.3") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "url") (r "^2") (k 0)) (d (n "wrapping-macro") (r "^0.2") (k 0)))) (h "05d7lj0pj990qfy7mxzrbxx82fnj9s1cbkhdrdhg2nbqllfb01l8")))

(define-public crate-linode-api-0.1.0 (c (n "linode-api") (v "0.1.0") (d (list (d (n "http-api-client") (r "^0.2") (k 0)) (d (n "once_cell") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde") (r "^1") (f (quote ("std" "derive"))) (k 0)) (d (n "serde-enum-str") (r "^0.3") (k 0)) (d (n "serde_json") (r "^1") (f (quote ("std"))) (k 0)) (d (n "serde_qs") (r "^0.12") (k 0)) (d (n "url") (r "^2") (k 0)) (d (n "wrapping-macro") (r "^0.2") (f (quote ("alloc"))) (k 0)))) (h "1xqswv4j61r5x30pyrvs74mp48syb91sjkzqmh4kwg0fdmyr3d2w")))

