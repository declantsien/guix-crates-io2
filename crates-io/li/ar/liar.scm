(define-module (crates-io li ar liar) #:use-module (crates-io))

(define-public crate-liar-0.1.0 (c (n "liar") (v "0.1.0") (d (list (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)))) (h "12lwpvi1mljdgckikfvfxrcdw60qczshn89g9bsc0lx2yymlxlyw")))

(define-public crate-liar-0.2.0 (c (n "liar") (v "0.2.0") (d (list (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)))) (h "1n251vawvq9f2a0yv8pxkcvq5i2q23cjavxj2jk99kdxrgxrcm40") (f (quote (("std") ("default" "std")))) (y #t)))

(define-public crate-liar-0.3.0 (c (n "liar") (v "0.3.0") (d (list (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)))) (h "0ichsinicns5z3jck5zkdacbr7wxzy520bvsr9l3fsjibd6r43p4") (f (quote (("std") ("default" "std"))))))

(define-public crate-liar-0.4.0 (c (n "liar") (v "0.4.0") (d (list (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)))) (h "1s0w574c4rssxwy4lxlk9n8fxxhzyhacbbnmfnb6112z0vcic6qh") (f (quote (("std") ("default" "std"))))))

(define-public crate-liar-0.5.0 (c (n "liar") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (t "cfg(unix)") (k 0)) (d (n "rustc_version") (r "^0.2.1") (d #t) (k 1)))) (h "1766wmkk0c2mnspv1xs7h101xaziixylpxvjqnwchacwc7618432") (f (quote (("std") ("default" "std" "libc"))))))

