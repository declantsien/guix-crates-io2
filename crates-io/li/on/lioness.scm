(define-module (crates-io li on lioness) #:use-module (crates-io))

(define-public crate-lioness-0.1.0 (c (n "lioness") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "1czzv58yh0xngvybbcgkcql51chwzsh08mmsndk4d59f415pafn4") (y #t)))

(define-public crate-lioness-0.1.1 (c (n "lioness") (v "0.1.1") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 2)))) (h "05h9fq9k7dg2pwp8g0ydqg46wk53smhcw2j60xzpxa2w24dmlfdz")))

(define-public crate-lioness-0.1.2 (c (n "lioness") (v "0.1.2") (d (list (d (n "arrayref") (r "^0.3.4") (d #t) (k 0)) (d (n "blake2") (r "^0.8.0") (d #t) (k 0)) (d (n "chacha") (r "^0.3.0") (d #t) (k 0)) (d (n "hex") (r "^0.3.1") (d #t) (k 2)) (d (n "keystream") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.5.0") (d #t) (k 2)))) (h "1fdcm7ikvbwgfn5y50ifvxry4x8d6dw124jpjif44b54ddq2dsaa")))

