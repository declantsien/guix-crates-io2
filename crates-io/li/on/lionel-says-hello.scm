(define-module (crates-io li on lionel-says-hello) #:use-module (crates-io))

(define-public crate-lionel-says-hello-0.1.0 (c (n "lionel-says-hello") (v "0.1.0") (h "0sgi5m47qr4hshzcdpa2ffnvkjygi339iwl9fx43sdcn2l64ah15")))

(define-public crate-lionel-says-hello-0.1.1 (c (n "lionel-says-hello") (v "0.1.1") (h "0r4xjg891jz53ix90gkil9v334b4syyp99b734xlf15grwfk66i6")))

