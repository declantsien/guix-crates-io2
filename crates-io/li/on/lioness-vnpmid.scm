(define-module (crates-io li on lioness-vnpmid) #:use-module (crates-io))

(define-public crate-lioness-vnpmid-0.1.0 (c (n "lioness-vnpmid") (v "0.1.0") (d (list (d (n "arrayref") (r "^0.3.6") (d #t) (k 0)) (d (n "blake2") (r "^0.10.4") (d #t) (k 0)) (d (n "blake3") (r "^1.3.1") (d #t) (k 0)) (d (n "chacha20") (r "^0.9.0") (d #t) (k 0)) (d (n "digest") (r "^0.10.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "09dihamhjl704cgiqsawlgmylwblm3fmab2jxqk7finxbn0mbb6q") (y #t)))

