(define-module (crates-io li on lionfish) #:use-module (crates-io))

(define-public crate-lionfish-0.1.0 (c (n "lionfish") (v "0.1.0") (h "0hxvfk5qpz7h97dk0mffzdnbvfvrvms5jdi1h43rqkd78chw72vx")))

(define-public crate-lionfish-0.1.1 (c (n "lionfish") (v "0.1.1") (h "0v3s7mfysb8lzwkpava18961gi2q6v0m88skkncxjyjksgl6jhyr")))

