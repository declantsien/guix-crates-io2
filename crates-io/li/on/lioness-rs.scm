(define-module (crates-io li on lioness-rs) #:use-module (crates-io))

(define-public crate-lioness-rs-0.1.0 (c (n "lioness-rs") (v "0.1.0") (d (list (d (n "blake3") (r "^1.0.0") (f (quote ("traits-preview"))) (d #t) (k 2)) (d (n "chacha20") (r "^0.7.2") (d #t) (k 2)) (d (n "cipher") (r "^0.3.0") (d #t) (k 0)) (d (n "crypto-mac") (r "^0.11.1") (d #t) (k 0)) (d (n "digest") (r "^0.10.0-pre.3") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "generic-array") (r "^0.14") (f (quote ("more_lengths"))) (d #t) (k 2)))) (h "0l4hlw6zzxifkv8lparpvyxdi8fidb58h4j6cvxvm89qfklis7fl") (f (quote (("block-cipher"))))))

