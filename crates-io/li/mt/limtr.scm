(define-module (crates-io li mt limtr) #:use-module (crates-io))

(define-public crate-limtr-0.1.7 (c (n "limtr") (v "0.1.7") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("sync" "rt-multi-thread" "test-util" "macros"))) (o #t) (d #t) (k 0)))) (h "1a465dg4zqdqm6b5y376q5zxyvksr99yz29rxd4lz0lfcjdhn2d1") (f (quote (("default" "async") ("async" "tokio"))))))

(define-public crate-limtr-0.2.0 (c (n "limtr") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "indexmap") (r "^2.0.2") (f (quote ("rayon"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 2)) (d (n "tokio") (r "^1.29.1") (f (quote ("sync" "rt-multi-thread" "test-util" "macros"))) (o #t) (d #t) (k 0)))) (h "1sy4jx3z09fa4bfrmrmqyq9wgag8ydpdmbd8m1ghawhf664x0rqw") (f (quote (("default" "async") ("async" "tokio"))))))

