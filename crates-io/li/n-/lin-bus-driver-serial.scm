(define-module (crates-io li n- lin-bus-driver-serial) #:use-module (crates-io))

(define-public crate-lin-bus-driver-serial-0.1.0 (c (n "lin-bus-driver-serial") (v "0.1.0") (d (list (d (n "lin-bus") (r "^0.1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "1n7xca3w9bdvszrmmn2p666qc7g8pyjql4garphwfyrmjb225c2g")))

(define-public crate-lin-bus-driver-serial-0.1.1 (c (n "lin-bus-driver-serial") (v "0.1.1") (d (list (d (n "lin-bus") (r "^0.1") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "1mmpnb904463ig1ifbw7ci7crb3fsaj3va5b59wqsbwv2g2i4n0n")))

(define-public crate-lin-bus-driver-serial-0.2.0 (c (n "lin-bus-driver-serial") (v "0.2.0") (d (list (d (n "lin-bus") (r "^0.2") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "18lyrdngynaidqp3fwx1l4lh5wvh05yfic8y38cw7pms0xw097ah")))

(define-public crate-lin-bus-driver-serial-0.3.0 (c (n "lin-bus-driver-serial") (v "0.3.0") (d (list (d (n "lin-bus") (r "^0.3") (d #t) (k 0)) (d (n "serial") (r "^0.4") (d #t) (k 0)))) (h "1818r0gkf3qz9jfvy32a1w1arwz4is8algqfar9cyh0h2kd323cj")))

