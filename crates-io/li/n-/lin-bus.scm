(define-module (crates-io li n- lin-bus) #:use-module (crates-io))

(define-public crate-lin-bus-0.1.0 (c (n "lin-bus") (v "0.1.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "15l12848k0nspwp69gp59441s032q7yq3blv0xrzja1azwvqcjd8")))

(define-public crate-lin-bus-0.1.1 (c (n "lin-bus") (v "0.1.1") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "140mrqm8aacxngcxsj0mskmhwa8cns5i6gwdxs0dnm6qs7r7iw8n")))

(define-public crate-lin-bus-0.2.0 (c (n "lin-bus") (v "0.2.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0af3xn4c62aqhzsxilqbd773rq0p8v8g2bg80cpmyph3ck8mwgmd")))

(define-public crate-lin-bus-0.2.1 (c (n "lin-bus") (v "0.2.1") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "0g9w8wvyi49kj6pyrv1jb2ij90kpi55sdfq2fkvbdjy622hbwj4d")))

(define-public crate-lin-bus-0.3.0 (c (n "lin-bus") (v "0.3.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1hc8m74rjpsm2g8wy6wk2gxh7mx1mzl4r6nsc5s5y466pgnhxr1d")))

(define-public crate-lin-bus-0.3.1 (c (n "lin-bus") (v "0.3.1") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1qmqla8i83rfs15vj0sg9lvg51zf3s5h9hn7daa4ygxqdam95w6k")))

(define-public crate-lin-bus-0.3.2 (c (n "lin-bus") (v "0.3.2") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1byjy0k2cqygh2azxwc1sr30hsik35zamkm7ha8wn913lbxm2n69")))

(define-public crate-lin-bus-0.4.0 (c (n "lin-bus") (v "0.4.0") (d (list (d (n "bitfield") (r "^0.13") (d #t) (k 0)) (d (n "byteorder") (r "^1.0") (k 0)) (d (n "num-traits") (r "^0.2") (k 0)))) (h "1f4splrfwdkfnksk6p59y2y3l1iacgx9rw7nl8l7hn32q3rd7b9j")))

