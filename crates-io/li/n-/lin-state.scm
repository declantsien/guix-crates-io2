(define-module (crates-io li n- lin-state) #:use-module (crates-io))

(define-public crate-lin-state-0.0.1 (c (n "lin-state") (v "0.0.1") (d (list (d (n "lin-state-macros") (r "^0.0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.29") (f (quote ("sync" "rt" "time"))) (o #t) (d #t) (k 0)))) (h "055dyv24rbrxckfwhin1ll0mslh7m66bbn1whjpvg7m3vmnzz2kz") (f (quote (("default" "macros")))) (s 2) (e (quote (("tokio" "dep:tokio") ("macros" "dep:lin-state-macros"))))))

