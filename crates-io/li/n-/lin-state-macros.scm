(define-module (crates-io li n- lin-state-macros) #:use-module (crates-io))

(define-public crate-lin-state-macros-0.0.1 (c (n "lin-state-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0482vly9nyjhpfp6x0mcj4gb0ccvvv8xikj9pc9r7vrv9cdbig1y")))

