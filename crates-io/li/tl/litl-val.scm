(define-module (crates-io li tl litl-val) #:use-module (crates-io))

(define-public crate-litl-val-0.1.0 (c (n "litl-val") (v "0.1.0") (d (list (d (n "deepsize") (r "^0.2.0") (d #t) (k 0)) (d (n "nanval") (r "^0.2.0") (d #t) (k 0) (p "litl-nanval")) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1pgnyvszc2vkxqpm2v2y0xk14cp0x7fnvjwid7nrf089wr2p76hj")))

(define-public crate-litl-val-0.2.0 (c (n "litl-val") (v "0.2.0") (d (list (d (n "deepsize") (r "^0.2.0") (d #t) (k 0)) (d (n "nanval") (r "^0.2.0") (d #t) (k 0) (p "litl-nanval")) (d (n "ordered-float") (r "^3.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0pqhynaa0545vaqqkiaz8b9yp5wvjysgx80phpnda0p94zjihl09")))

