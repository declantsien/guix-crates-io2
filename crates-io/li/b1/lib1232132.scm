(define-module (crates-io li b1 lib1232132) #:use-module (crates-io))

(define-public crate-lib1232132-1.0.0 (c (n "lib1232132") (v "1.0.0") (h "0s9a9n8lddx2r0h09c2nnbp117388r6f13xpdi2ccvkanq3xkj29")))

(define-public crate-lib1232132-1.0.2 (c (n "lib1232132") (v "1.0.2") (h "0ic7r2qrrzsim1jwadh2zrx6qlpy43lgqwyqmzzz7ww4v7anwjgb")))

(define-public crate-lib1232132-1.0.1 (c (n "lib1232132") (v "1.0.1") (h "07d71wq8gnxqjbns4wk71xdbh6qskmy3vk7l1wagdal0pmavhfra")))

