(define-module (crates-io li b1 lib1testingdep) #:use-module (crates-io))

(define-public crate-lib1testingDep-0.1.0 (c (n "lib1testingDep") (v "0.1.0") (d (list (d (n "lib2") (r "^0.1.0") (d #t) (k 0)))) (h "0yivzpil22q49hpckd13z33vmx490xkf8fd3aq5ffjr19k3d77xs")))

(define-public crate-lib1testingDep-0.2.0 (c (n "lib1testingDep") (v "0.2.0") (h "1ycv3bqmx2lr2w2pxwf43c2w9m7vky0xlbz1bl0nhhvicn61hc4d") (y #t)))

