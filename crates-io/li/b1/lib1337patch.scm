(define-module (crates-io li b1 lib1337patch) #:use-module (crates-io))

(define-public crate-lib1337patch-0.1.0 (c (n "lib1337patch") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)))) (h "1q351bia45wjq24rlya0pz3sclysd2171fyb2y6cc3pjmn8nv1zi") (y #t)))

(define-public crate-lib1337patch-0.1.1 (c (n "lib1337patch") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)))) (h "1xp6f7dmfqw5j7fi3jdd788fzfnwydkcfwpwvbz9ggil3w3iiglp") (y #t)))

(define-public crate-lib1337patch-0.1.2 (c (n "lib1337patch") (v "0.1.2") (d (list (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)))) (h "06jxhp5snk7ql62ka7mlx6npxzx4vw97vpl7ryhq7m6pgmvpp9n9") (y #t)))

(define-public crate-lib1337patch-0.2.0 (c (n "lib1337patch") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)))) (h "0lb99pk8ahvmz6xc72a6mqw8fj2r96dq008yiwz17l250b92np2q") (y #t)))

(define-public crate-lib1337patch-0.2.1 (c (n "lib1337patch") (v "0.2.1") (d (list (d (n "tempfile") (r "^3.7.1") (d #t) (k 2)))) (h "1jvqvksc4lzci6acbpcfni3km8hl4x9apfqk7mh80m35z61h1m9m") (y #t)))

(define-public crate-lib1337patch-0.2.2 (c (n "lib1337patch") (v "0.2.2") (d (list (d (n "tempfile") (r "^3.8") (d #t) (k 2)))) (h "0lysl7dn01gz32z5rr60imzwiwggk0h1q0li7klqq5sayvqcas8a")))

