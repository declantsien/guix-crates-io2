(define-module (crates-io li b3 lib3d6) #:use-module (crates-io))

(define-public crate-lib3d6-0.0.1 (c (n "lib3d6") (v "0.0.1") (h "0bhni4ymjq5n8yd6l3c4kq1hrwq67yh0f7lxw1970aim6jlsvrxp")))

(define-public crate-lib3d6-0.0.2 (c (n "lib3d6") (v "0.0.2") (h "0b3p97bv89gq860aqq5sdjc0a048b60wnjvga8xqsaijl2hg3287")))

(define-public crate-lib3d6-0.0.3 (c (n "lib3d6") (v "0.0.3") (h "0lqga7aiywxgv21ddsvmyhwsv18jwkv9xi1lbqiq2px18nl6z5xw")))

(define-public crate-lib3d6-0.0.4 (c (n "lib3d6") (v "0.0.4") (h "0z7lvkq6kvg8pnx0vmkwvszznbn8rzy35cfzjkz697wapyibrdrh")))

