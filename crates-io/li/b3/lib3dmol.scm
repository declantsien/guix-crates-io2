(define-module (crates-io li b3 lib3dmol) #:use-module (crates-io))

(define-public crate-lib3dmol-0.3.0 (c (n "lib3dmol") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "02xgw3hxliiwky844aszy301yjcplrdvcw36qfymz8j2br0a5xkk")))

(define-public crate-lib3dmol-0.3.1 (c (n "lib3dmol") (v "0.3.1") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0phskfaizffqwvwv36zx0m553hgplrw5pqih23k52n918lajjrmz")))

(define-public crate-lib3dmol-0.3.2 (c (n "lib3dmol") (v "0.3.2") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "0hfkwmkh6zd6swd7n2ih4ikm5bxqinambb62jc8gbjcllpblnv7i")))

(define-public crate-lib3dmol-0.4.0 (c (n "lib3dmol") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)))) (h "1izhk1hcabhff9ybzal29awsnfclrw0s6ha4mssy1m0gz75rb0cq")))

