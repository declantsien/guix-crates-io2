(define-module (crates-io li b3 lib3-babs-devops-test) #:use-module (crates-io))

(define-public crate-lib3-babs-devops-test-0.1.0 (c (n "lib3-babs-devops-test") (v "0.1.0") (h "1ckk0rypficjnv5s63k2sx26kfyn7fx20vvykzbmsj8zfv0l3pa5")))

(define-public crate-lib3-babs-devops-test-0.1.1 (c (n "lib3-babs-devops-test") (v "0.1.1") (h "0rynycb5qqw66g1r3y2kl60ll23dkmpr5anq1prxnj6zcd26n5pa")))

(define-public crate-lib3-babs-devops-test-0.1.2 (c (n "lib3-babs-devops-test") (v "0.1.2") (h "1ppb5z2vc0mfm495v6p8my63yfwvq24vmk70hc7lfjw3im0lj02n")))

