(define-module (crates-io li b3 lib3h_p2p_protocol) #:use-module (crates-io))

(define-public crate-lib3h_p2p_protocol-0.0.5 (c (n "lib3h_p2p_protocol") (v "0.0.5") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "00x7iykkm365i06scw0v9xvmvxrqbwq0bn0bgmn74m8q9ihkf4a6")))

(define-public crate-lib3h_p2p_protocol-0.0.7 (c (n "lib3h_p2p_protocol") (v "0.0.7") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "15w267iziz3ax0pawaxknk87s0ylmkh6grz774726jqvzi3pcz25")))

(define-public crate-lib3h_p2p_protocol-0.0.8 (c (n "lib3h_p2p_protocol") (v "0.0.8") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "15w1zvbv0r5fp887w600k6bd54x4c0irg5c0hxg8l3qmny86bgql")))

(define-public crate-lib3h_p2p_protocol-0.0.9 (c (n "lib3h_p2p_protocol") (v "0.0.9") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "0p5mk1k9jwj5jqmzykxrx45xxxmhr890f5aji8hlkxq3xqz407mj")))

(define-public crate-lib3h_p2p_protocol-0.0.10 (c (n "lib3h_p2p_protocol") (v "0.0.10") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "00c47bjyq4j5i0x6rzmw82cm2h7h96286xb5cz9vcy7y4pyqij9w")))

(define-public crate-lib3h_p2p_protocol-0.0.13 (c (n "lib3h_p2p_protocol") (v "0.0.13") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "088rv1r0i1vzs8dl11ravgsmp85sh43j925znx59k41j5wbxl7sr")))

(define-public crate-lib3h_p2p_protocol-0.0.16 (c (n "lib3h_p2p_protocol") (v "0.0.16") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "09dy0fszj4bbngmfyy538wy6wz8ij4gcw2ny0kvsg9hjys676vx1")))

(define-public crate-lib3h_p2p_protocol-0.0.17 (c (n "lib3h_p2p_protocol") (v "0.0.17") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "1gbcm04pwq1bn0ryv1dk4ldrvd9jbi6mn2lb5hsqd0ax0ii58iiy")))

(define-public crate-lib3h_p2p_protocol-0.0.18 (c (n "lib3h_p2p_protocol") (v "0.0.18") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "1129xfw7y1qhmq6hd9slbw7apw32if1qa33yhip1wj311xmm01g4")))

(define-public crate-lib3h_p2p_protocol-0.0.19 (c (n "lib3h_p2p_protocol") (v "0.0.19") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "088pb5w36rhm617pz791div5qbiw036vk53m6g8fnph69f0ha4j4")))

(define-public crate-lib3h_p2p_protocol-0.0.20 (c (n "lib3h_p2p_protocol") (v "0.0.20") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "1lxbrkfrsfczz8776hr4arcbjisv7fma4hahjaammkqpif4ac689")))

(define-public crate-lib3h_p2p_protocol-0.0.21 (c (n "lib3h_p2p_protocol") (v "0.0.21") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "15sx5nv4jr5wam4wscwk6j7m91m5sws81h9d2nnykkxdi4rxn1zs")))

(define-public crate-lib3h_p2p_protocol-0.0.22 (c (n "lib3h_p2p_protocol") (v "0.0.22") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "0gv15zxag75zyfwwa4269vzqz67s57cpjwgmcwkpgwbkmjk4p61f")))

(define-public crate-lib3h_p2p_protocol-0.0.23 (c (n "lib3h_p2p_protocol") (v "0.0.23") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "0sqwx449sy89nqarx3ahzl2f2jaj9s860vldmyvwvz8mnbpdj2wr")))

(define-public crate-lib3h_p2p_protocol-0.0.24 (c (n "lib3h_p2p_protocol") (v "0.0.24") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "0i97aliy3y2hvyw2mkpfsphv12gaqp061fqly4ba08hm1zp5269f")))

(define-public crate-lib3h_p2p_protocol-0.0.25 (c (n "lib3h_p2p_protocol") (v "0.0.25") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "1v6xpwkhfq0m0nd7myx8lzyk2ccakba4xnmkz695s8xfys4jykc9")))

(define-public crate-lib3h_p2p_protocol-0.0.26 (c (n "lib3h_p2p_protocol") (v "0.0.26") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "15zkjhfyshcs181qrfv3jwadqf6ak3chrv89h5r56vqpr3ija14z")))

(define-public crate-lib3h_p2p_protocol-0.0.28 (c (n "lib3h_p2p_protocol") (v "0.0.28") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "1ah8isjv6b6fw2q32j3znzpgibv6c71j7in2g119ym67dx16knh9")))

(define-public crate-lib3h_p2p_protocol-0.0.30 (c (n "lib3h_p2p_protocol") (v "0.0.30") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "1rd0zvm4k7k77mhw8f469a7vj062qb3drilf71galr95nvy5yyr7")))

(define-public crate-lib3h_p2p_protocol-0.0.31 (c (n "lib3h_p2p_protocol") (v "0.0.31") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "1i7p7g2s2hclxp0fppsm87fccw234wxxf2i9j9yfifiwz4svqm4r")))

(define-public crate-lib3h_p2p_protocol-0.0.32 (c (n "lib3h_p2p_protocol") (v "0.0.32") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "1x17klp0k5fvmx03xvvl7dr6ly4divh85bb3ic0m0qsf022yfqm6")))

(define-public crate-lib3h_p2p_protocol-0.0.33 (c (n "lib3h_p2p_protocol") (v "0.0.33") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "1b52g6phq2hdvh784pn2cf5lmr08bgi11fyc213xxx9slw9xyvfj")))

(define-public crate-lib3h_p2p_protocol-0.0.34 (c (n "lib3h_p2p_protocol") (v "0.0.34") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "05pfnpw6aaxkq8m83ibc58dywdhl34riqyyl3ijxf24x5ncynn9k")))

(define-public crate-lib3h_p2p_protocol-0.0.35 (c (n "lib3h_p2p_protocol") (v "0.0.35") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (d #t) (k 0)))) (h "1d5w5ks5w2v0qgypcvjm2byb7ri7yzwlr0xfp3m15ccc78rbwkns")))

(define-public crate-lib3h_p2p_protocol-0.0.36 (c (n "lib3h_p2p_protocol") (v "0.0.36") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.100") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.100") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.47") (d #t) (k 0)))) (h "1rc320bi7y3srq1r6vm7gyj1jcagcbqplvnrlwgwfxamda716az7")))

(define-public crate-lib3h_p2p_protocol-0.0.37 (c (n "lib3h_p2p_protocol") (v "0.0.37") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.47") (d #t) (k 0)))) (h "01bi92qwq3ls1nhs3ix70hqnyhb55xwavnlrzjm4cnh0wpivp1wh")))

(define-public crate-lib3h_p2p_protocol-0.0.38 (c (n "lib3h_p2p_protocol") (v "0.0.38") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.47") (d #t) (k 0)))) (h "0xp9ppsaasb997wwpfbd368ydaw8kyc2z59kymdb6z6ss1dhxqqn")))

(define-public crate-lib3h_p2p_protocol-0.0.39 (c (n "lib3h_p2p_protocol") (v "0.0.39") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.47") (d #t) (k 0)))) (h "11qfh317nai4cya2cglg1zzr1iv0ddz6gzjrx985vcab9ljqc49k")))

(define-public crate-lib3h_p2p_protocol-0.0.40 (c (n "lib3h_p2p_protocol") (v "0.0.40") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.47") (d #t) (k 0)))) (h "0x7xddp2r60kh6fyz5x170gs7hv0i9ic7462h63f8y8m49q274lh")))

(define-public crate-lib3h_p2p_protocol-0.0.41 (c (n "lib3h_p2p_protocol") (v "0.0.41") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.47") (d #t) (k 0)))) (h "0vwb2nfn9fwbmx9rh791wnch171aawmd9q6x51z59iaswnz7xyzn")))

(define-public crate-lib3h_p2p_protocol-0.0.42 (c (n "lib3h_p2p_protocol") (v "0.0.42") (d (list (d (n "capnp") (r "= 0.10.0") (d #t) (k 0)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.47") (d #t) (k 0)))) (h "06fxlzmspng3b5h7fws7ag2zxl0apylny2bpnk9rxw8hy8slbzzr")))

