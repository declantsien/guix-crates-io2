(define-module (crates-io li b3 lib3h_crypto_api) #:use-module (crates-io))

(define-public crate-lib3h_crypto_api-0.0.1-alpha1 (c (n "lib3h_crypto_api") (v "0.0.1-alpha1") (d (list (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)))) (h "0rkfz7vsjxfwgrhbn3zfr987nlaz2pzv0jzkyfklan5dm5vzw0ia")))

(define-public crate-lib3h_crypto_api-0.0.2-alpha1 (c (n "lib3h_crypto_api") (v "0.0.2-alpha1") (d (list (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)))) (h "0xz6ggq638znannpylc3159z6w089vkapwhy6h2vwrw0v3ffr5s2")))

(define-public crate-lib3h_crypto_api-0.0.3-alpha1 (c (n "lib3h_crypto_api") (v "0.0.3-alpha1") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)))) (h "1rnzq6lrj0frqi474farsv9ym3856ywb49640ay7ck2r97fn8rmh")))

(define-public crate-lib3h_crypto_api-0.0.4-alpha1 (c (n "lib3h_crypto_api") (v "0.0.4-alpha1") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "05y91wnkgphxfvhkwd0jxghr4pbg9r7q5n4sijd909irhz5i6g56")))

(define-public crate-lib3h_crypto_api-0.0.5 (c (n "lib3h_crypto_api") (v "0.0.5") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0cdx45zq51vinmzmw3q6n7wlcbg1pyz1igm64b0fx8cl8a2q1vmk")))

(define-public crate-lib3h_crypto_api-0.0.6 (c (n "lib3h_crypto_api") (v "0.0.6") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "15asrczvw374cjqnvr0b4rrgd9y1hy29sgv0g556rij42wmxjdqx")))

(define-public crate-lib3h_crypto_api-0.0.7 (c (n "lib3h_crypto_api") (v "0.0.7") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "1217yg9cm1ls8993lwpdhw9ccbhra2x2h5y2sarw4ns0g6fkmy5b")))

(define-public crate-lib3h_crypto_api-0.0.8 (c (n "lib3h_crypto_api") (v "0.0.8") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0pabads93hf4g743j4spy5ibsb5wgx32shm78z7czg35nvvz495k")))

(define-public crate-lib3h_crypto_api-0.0.9 (c (n "lib3h_crypto_api") (v "0.0.9") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "05fl10viqlnsphysdza0ki8yicff8v5kr5xzhwbl21wj1xbcnjz0")))

(define-public crate-lib3h_crypto_api-0.0.10 (c (n "lib3h_crypto_api") (v "0.0.10") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0rhj61c7zxi4gvzllmj6kwy3lxqhk12jq32sy6cppa9vk31yrnjw")))

(define-public crate-lib3h_crypto_api-0.0.13 (c (n "lib3h_crypto_api") (v "0.0.13") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0qzpcyiphx3vdyc2c6q6bk4wwi1nf5hsw0c73cxv3chna8c3bi0k")))

(define-public crate-lib3h_crypto_api-0.0.14 (c (n "lib3h_crypto_api") (v "0.0.14") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0xaiflicdfabmbs91lm31ayr4r7a8z1ym23bscbwvnzmq2979ndi")))

(define-public crate-lib3h_crypto_api-0.0.15 (c (n "lib3h_crypto_api") (v "0.0.15") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "135ljcw3059g75mjmbczkq1kmkhbiqf95l8vkwy3sv6451jh6qh7")))

(define-public crate-lib3h_crypto_api-0.0.16 (c (n "lib3h_crypto_api") (v "0.0.16") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0as2kpgf4p0nfpxvinccf2j299dr1sh8kra3gs4zcr562bb4sm0k")))

(define-public crate-lib3h_crypto_api-0.0.17 (c (n "lib3h_crypto_api") (v "0.0.17") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "1zbg3q1hr3vqvylryszinx68wjrqqhh4x4mzrbzgwhwzhnmx84wc")))

(define-public crate-lib3h_crypto_api-0.0.18 (c (n "lib3h_crypto_api") (v "0.0.18") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0q9qgvmb1mklj50x4n8r0b4kc6wifriifsi8gl9vxnp3np8nnx30")))

(define-public crate-lib3h_crypto_api-0.0.19 (c (n "lib3h_crypto_api") (v "0.0.19") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "120mwsfvkl01821v0xcrygsmf7dzw7s6n3bcfifrfsxl748yhd9x")))

(define-public crate-lib3h_crypto_api-0.0.20 (c (n "lib3h_crypto_api") (v "0.0.20") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0hwzflyjs6hwxl5hm2g0dmv9jsb67p7wgvg8iwwh1f1k6q57qcbb")))

(define-public crate-lib3h_crypto_api-0.0.21 (c (n "lib3h_crypto_api") (v "0.0.21") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0i589zqqzn8iv41bx7aydnk5shrbbq2zq6g52q6w1b0mrj5hw2wl")))

(define-public crate-lib3h_crypto_api-0.0.22 (c (n "lib3h_crypto_api") (v "0.0.22") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0cjqp4w7gzxpn1lcbiqlmla6xki8cy09fb59jhxd45gb7f19n8bq")))

(define-public crate-lib3h_crypto_api-0.0.23 (c (n "lib3h_crypto_api") (v "0.0.23") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "04j62j1ry9lsxq948ac3z87cg3jr4jc4q3gcvlk7kbfab0brszcy")))

(define-public crate-lib3h_crypto_api-0.0.24 (c (n "lib3h_crypto_api") (v "0.0.24") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0p1w0grll6s6gqqj31b9c7qx8v63j22gwfdzmmkkyz6zcx0a580r")))

(define-public crate-lib3h_crypto_api-0.0.25 (c (n "lib3h_crypto_api") (v "0.0.25") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "1liqsm295nqk95cpp928cml45zc5yh8xbkgmzna1yayhv3i66x77")))

(define-public crate-lib3h_crypto_api-0.0.26 (c (n "lib3h_crypto_api") (v "0.0.26") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "08k511mak6bjz1v8h5s4vbi2grzixlpv83hm4zc7nxpxj26y56kd")))

(define-public crate-lib3h_crypto_api-0.0.27 (c (n "lib3h_crypto_api") (v "0.0.27") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "009pkdi95c0485968k17jckpq20phm1jha1l71pkz5yya2i5hx3i")))

(define-public crate-lib3h_crypto_api-0.0.28 (c (n "lib3h_crypto_api") (v "0.0.28") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "1y8lkhm1yl2p5gdl5mz7k6njyc54wi2xdxjbv0bgc0x685zgy2gj")))

(define-public crate-lib3h_crypto_api-0.0.30 (c (n "lib3h_crypto_api") (v "0.0.30") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "168a16rmlka2bzp599pqngjp1fycbs8drrkvq27m2wsr6327z7yq")))

(define-public crate-lib3h_crypto_api-0.0.31 (c (n "lib3h_crypto_api") (v "0.0.31") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "1mn4ylixb6m0z097abraimdk433dcz87arc5gpnpqisb4zbphch8")))

(define-public crate-lib3h_crypto_api-0.0.32 (c (n "lib3h_crypto_api") (v "0.0.32") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "1niqcg0hwbjv1fm2453wi30dzwwhchx0dq40q8d20mph4807dg6m")))

(define-public crate-lib3h_crypto_api-0.0.33 (c (n "lib3h_crypto_api") (v "0.0.33") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0f3hgr3h511f3zlnz78q4aqv4rkwv4pw24mzrb3g3235kl9c7jb3")))

(define-public crate-lib3h_crypto_api-0.0.34 (c (n "lib3h_crypto_api") (v "0.0.34") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "01bdqg50qr86g7mwakp7x7hn62lmjbbpigg59589rljfky3p7mka")))

(define-public crate-lib3h_crypto_api-0.0.35 (c (n "lib3h_crypto_api") (v "0.0.35") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.89") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "1i9ddgmjdm8cscaw40zrvhcr1a6xzijhz09jl3bfbkxh9lav8p0n")))

(define-public crate-lib3h_crypto_api-0.0.36 (c (n "lib3h_crypto_api") (v "0.0.36") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.100") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.100") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0x2lf5k4nadydrwijxd878dfcq3x9zkl39nk1m2z5i7glk57ikhm")))

(define-public crate-lib3h_crypto_api-0.0.37 (c (n "lib3h_crypto_api") (v "0.0.37") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0r0az5azvldfhk50bi4l9cjjcimm22xhj2ii9amdvb99sdd14zb6")))

(define-public crate-lib3h_crypto_api-0.0.38 (c (n "lib3h_crypto_api") (v "0.0.38") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0pkjr109zvv0jmjdb4jh90jk9hghdca5pf7mbkrgc6iyxyw3icpm")))

(define-public crate-lib3h_crypto_api-0.0.39 (c (n "lib3h_crypto_api") (v "0.0.39") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "14p4d024hb4bbc7syvyx4hh55r1nbmcvrnxg4hbmadzmmzrpd8ln")))

(define-public crate-lib3h_crypto_api-0.0.40 (c (n "lib3h_crypto_api") (v "0.0.40") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "0l9rdlh566w1jw5l3scq907lsz0v1nrvi043qrs994a4rdl3hpfp")))

(define-public crate-lib3h_crypto_api-0.0.41 (c (n "lib3h_crypto_api") (v "0.0.41") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "1ha8vg1svjb2dsngk7x1ajf7nd6c4b7s5lnwg8nqmzh162sjz6ia")))

(define-public crate-lib3h_crypto_api-0.0.42 (c (n "lib3h_crypto_api") (v "0.0.42") (d (list (d (n "rand") (r "= 0.6.5") (d #t) (k 2)) (d (n "serde") (r "= 1.0.104") (d #t) (k 0)) (d (n "serde_derive") (r "= 1.0.104") (d #t) (k 0)) (d (n "sha2") (r "= 0.8.0") (d #t) (k 2)) (d (n "zeroize") (r "= 0.9.2") (d #t) (k 0)))) (h "1910k0nm6dn6b44nwmj4wyiazx2jlpd8ksizmn5c27i7zhm5gq6q")))

