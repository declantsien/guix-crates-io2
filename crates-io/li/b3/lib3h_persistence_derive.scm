(define-module (crates-io li b3 lib3h_persistence_derive) #:use-module (crates-io))

(define-public crate-lib3h_persistence_derive-0.0.1-alpha1 (c (n "lib3h_persistence_derive") (v "0.0.1-alpha1") (d (list (d (n "quote") (r "= 0.6.11") (d #t) (k 0)) (d (n "serde") (r "= 1.0.89") (d #t) (k 0)) (d (n "serde_json") (r "= 1.0.39") (f (quote ("preserve_order"))) (d #t) (k 0)) (d (n "syn") (r "= 0.15.31") (d #t) (k 0)))) (h "03wicld9nbx8yxx0rf4kgz4hrzf9r46a8gww1bdbpsvi7k5kcajm")))

