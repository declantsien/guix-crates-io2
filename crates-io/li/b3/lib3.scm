(define-module (crates-io li b3 lib3) #:use-module (crates-io))

(define-public crate-lib3-0.1.0 (c (n "lib3") (v "0.1.0") (h "150q9npj5ml5rhd5c0y5c1wimlpx89r6apcbjd0cdh3shxsndd49")))

(define-public crate-lib3-0.1.1 (c (n "lib3") (v "0.1.1") (h "1i88m5hkh5hy8cdq6kwlg4qbwqalycia0af1d0bwhrsin61jbnrv")))

