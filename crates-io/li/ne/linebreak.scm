(define-module (crates-io li ne linebreak) #:use-module (crates-io))

(define-public crate-linebreak-0.1.0 (c (n "linebreak") (v "0.1.0") (d (list (d (n "icu") (r "^1.4.0") (d #t) (k 0)))) (h "1rmlzjxbn6qpfmaidffph82fww8cbzl1s3xf063y19ksci66f21c") (r "1.67.1")))

(define-public crate-linebreak-0.2.0 (c (n "linebreak") (v "0.2.0") (d (list (d (n "icu") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.53") (f (quote ("Win32_Foundation" "Win32_System_Console"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0bpbnjwj6r2p92rffajvyff9gng7rpfap4p1vwf1wbszazyxh6s5") (r "1.67.1")))

