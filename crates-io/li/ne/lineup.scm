(define-module (crates-io li ne lineup) #:use-module (crates-io))

(define-public crate-lineup-0.9.0 (c (n "lineup") (v "0.9.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)))) (h "019ai389hd2n1fgl3awik7h7zzjscrmjgxhhywm5g85lr9wf46aj")))

(define-public crate-lineup-0.9.1 (c (n "lineup") (v "0.9.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)))) (h "11p2lg0wh9cd0lj506gx9fs08001637ibri42qmawnv78dw5yw9f")))

(define-public crate-lineup-0.10.0 (c (n "lineup") (v "0.10.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)))) (h "17zp5hqlcq8xn1jm752ikab07bnnmzzwqifdvbhv8ikj7nbqyj88") (y #t)))

(define-public crate-lineup-0.10.1 (c (n "lineup") (v "0.10.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)))) (h "1vi5dbm1ji0gh5i25iz9dymsk60hbf0lv5si36l93l2m302mkyia")))

(define-public crate-lineup-0.2.0 (c (n "lineup") (v "0.2.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)))) (h "1l0idjlx1jmx5zi0l64gza7wrnap8zwpfgk0ymrx27clcbs7nbfb") (y #t)))

(define-public crate-lineup-0.2.1 (c (n "lineup") (v "0.2.1") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)))) (h "12z2qk0nqr9rv66m71k6lpcara0ad6hqn8j36p2yv6gmcfg7n7px")))

(define-public crate-lineup-1.0.0 (c (n "lineup") (v "1.0.0") (d (list (d (n "clap") (r "^4.0.18") (f (quote ("derive" "cargo"))) (d #t) (k 0)) (d (n "derive-new") (r "^0.5.9") (d #t) (k 0)) (d (n "derive_builder") (r "^0.11.2") (d #t) (k 0)))) (h "1mjldwd4m1wsbcnpgi9bb7bai8kz1rk661w1ap3xq8rjb305sc77")))

