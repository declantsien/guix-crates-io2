(define-module (crates-io li ne linenoise) #:use-module (crates-io))

(define-public crate-linenoise-0.0.1 (c (n "linenoise") (v "0.0.1") (d (list (d (n "linenoise-sys") (r "~1.0.0") (d #t) (k 0)))) (h "0gn75yjjf2dfn2z2cf059rmwlz3s2wry8i32ml9z577qwj0vvwc0")))

(define-public crate-linenoise-0.0.2 (c (n "linenoise") (v "0.0.2") (d (list (d (n "linenoise-sys") (r "~1.0.0") (d #t) (k 0)))) (h "0axsv8xk8iq7y8f5mqdc15q5kw7n8d0xqg6wwgc7x6bvli4r6grg")))

(define-public crate-linenoise-0.0.3 (c (n "linenoise") (v "0.0.3") (d (list (d (n "linenoise-sys") (r "~1.0.0") (d #t) (k 0)))) (h "1ybl43hn62d2nnwxpw1jc6d6qvxrpkk5a9146mjxbg3wbffa1qlh")))

