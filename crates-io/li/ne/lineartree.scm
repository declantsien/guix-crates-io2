(define-module (crates-io li ne lineartree) #:use-module (crates-io))

(define-public crate-lineartree-0.1.0 (c (n "lineartree") (v "0.1.0") (h "0fi9kzr7a9wa99xkmfifzx2sws6zdbkkln4afkk2y3z6rf5nbl38")))

(define-public crate-lineartree-0.1.1 (c (n "lineartree") (v "0.1.1") (h "1vjrppl6gzzvak3lvns6p880dm6d80giz5s3h6dizr6pd9shd6mc")))

