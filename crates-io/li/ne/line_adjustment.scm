(define-module (crates-io li ne line_adjustment) #:use-module (crates-io))

(define-public crate-line_adjustment-0.1.0 (c (n "line_adjustment") (v "0.1.0") (h "1148rygd4spm8ll5av6jaxwai8cj1n45079ylfp8ls7dv7ifs98m")))

(define-public crate-line_adjustment-0.1.1 (c (n "line_adjustment") (v "0.1.1") (h "0awnvi73625pgdl0qsh8zhjnl2az8lwdlzzsx9fjj9giymsgfn57")))

