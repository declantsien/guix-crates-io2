(define-module (crates-io li ne linear-hashtbl) #:use-module (crates-io))

(define-public crate-linear-hashtbl-0.1.0 (c (n "linear-hashtbl") (v "0.1.0") (d (list (d (n "allocator-api2") (r "^0.2") (f (quote ("alloc"))) (o #t) (k 0)))) (h "1n8xwlnzgf6wgpiy7lnqb1971bdpa3xg89vzqjxqwg4cw12szi5j") (f (quote (("default")))) (s 2) (e (quote (("nightly" "allocator-api2?/nightly") ("allocator-api2" "dep:allocator-api2"))))))

