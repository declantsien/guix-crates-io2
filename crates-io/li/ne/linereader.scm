(define-module (crates-io li ne linereader) #:use-module (crates-io))

(define-public crate-linereader-0.3.0 (c (n "linereader") (v "0.3.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.6.0") (d #t) (k 2)))) (h "0sj0n14h3wd37jiw6sf4pjwarpz3367ixpz1l23k1gyiwsy1v7gj")))

(define-public crate-linereader-0.4.0 (c (n "linereader") (v "0.4.0") (d (list (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "rand") (r "^0.7.0") (d #t) (k 2)))) (h "07fxs1r8ljj01hvknm6xrid5hw14wb31985c35amfmq3hskgw8fr")))

