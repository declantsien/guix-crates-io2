(define-module (crates-io li ne linear_model_allen) #:use-module (crates-io))

(define-public crate-linear_model_allen-0.2.1 (c (n "linear_model_allen") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "hound") (r "^3.4.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "lewton") (r "^0.10.2") (d #t) (k 2)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cgq0p4kri5zgkc0dp8413g6y0agmkkbh3id34gz1frkii3yar09")))

