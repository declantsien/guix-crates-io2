(define-module (crates-io li ne linera-wit-bindgen-core) #:use-module (crates-io))

(define-public crate-linera-wit-bindgen-core-0.2.0 (c (n "linera-wit-bindgen-core") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "wit-parser") (r "^0.2.0") (d #t) (k 0) (p "linera-wit-parser")))) (h "11g4j6za1vq0jcm81kwm8fwid3bm5j2vjyrj68yalkixr31r8j10")))

