(define-module (crates-io li ne line-index) #:use-module (crates-io))

(define-public crate-line-index-0.1.0-pre.1 (c (n "line-index") (v "0.1.0-pre.1") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1kj5yyi8mjq1n22l5nms9gg0l01nzyl41yfl25g79h8hjxv9db9c")))

(define-public crate-line-index-0.1.0 (c (n "line-index") (v "0.1.0") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.0") (d #t) (k 0)))) (h "1b57n7f9kxp4ys34r4kfdmwsjaqgh8b69vlyy3jblq8fwni9xrby")))

(define-public crate-line-index-0.1.1 (c (n "line-index") (v "0.1.1") (d (list (d (n "nohash-hasher") (r "^0.2.0") (d #t) (k 0)) (d (n "text-size") (r "^1.1.1") (d #t) (k 0)))) (h "02jqal6pyrh455avf1lcsdpi7nypsykzs66250wniqka6yaigmk7")))

