(define-module (crates-io li ne linebased) #:use-module (crates-io))

(define-public crate-linebased-0.1.0 (c (n "linebased") (v "0.1.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)))) (h "1bd9j3mpk603r3q7vcz8js6psil4izxhd39j056207c9wb4453fb")))

(define-public crate-linebased-0.2.0 (c (n "linebased") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)))) (h "0cfjghqi172k48nya50010pdqw0314crq04mq8rvac81bzh94wgc")))

(define-public crate-linebased-0.2.1 (c (n "linebased") (v "0.2.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)))) (h "0imhm4q5rgxri0pkhayzq665047nyhacc2zqwn30040r4smlwgk1")))

(define-public crate-linebased-0.3.0 (c (n "linebased") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)))) (h "1qcg0r1cp86vxwhimzyll5sjvfyyfqjyxa8v03mqzgdrfgxv95jh")))

(define-public crate-linebased-0.3.1 (c (n "linebased") (v "0.3.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)))) (h "1h9gr6f3ymm6jh73kbcx35naddkrcqrn18liiywdkpnwdl9xyi5b")))

(define-public crate-linebased-0.4.0 (c (n "linebased") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "sync" "rt-core" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "0jq40y4ya48mp23x6c5rf0l8wn67b4bhmqy35sfq2g31xsrndyqh")))

(define-public crate-linebased-0.4.1 (c (n "linebased") (v "0.4.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "sync" "rt-core" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "0jkcb84z4ia9514g5amhh6jwp0aiz92cvkhlphng1h3li38xc1jj")))

(define-public crate-linebased-0.4.2 (c (n "linebased") (v "0.4.2") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "sync" "rt-core" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "186m3nqpiv3lf59bbbkkxcgpayc2wvkhlnwq215ihsk3c4v6r007")))

(define-public crate-linebased-0.4.3 (c (n "linebased") (v "0.4.3") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("tcp" "sync" "rt-core" "io-util" "blocking"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros" "rt-threaded"))) (d #t) (k 2)))) (h "02lymrrad6mv1i65s78py32b58r9y436krskqsqcr6h8ij8zgsh7")))

(define-public crate-linebased-0.5.0 (c (n "linebased") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "sync" "rt" "io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0zj8l4hly9ivb9g715vsw3m3y2izwawp4lib2zmqisn0bqkw7ph0")))

