(define-module (crates-io li ne line-span) #:use-module (crates-io))

(define-public crate-line-span-0.1.0 (c (n "line-span") (v "0.1.0") (h "07bmhgkb5rc8hv58zd8jrhz9hjwy4034yc1ndh26wwa98x5caqlp")))

(define-public crate-line-span-0.1.1 (c (n "line-span") (v "0.1.1") (h "0gxbvyrv17jdn8yjc05m41c40a681vlfck5al31w0p0i3jr26bkc")))

(define-public crate-line-span-0.1.2 (c (n "line-span") (v "0.1.2") (h "0ckqiygcfjsajnam7ar82xyncnq80vrwibcscm0lq2lkrlin74sq")))

(define-public crate-line-span-0.1.3 (c (n "line-span") (v "0.1.3") (h "0awiv9a5nr0nq955xz8j1lrlz91kparzf2l57jl76vbzlc2fcvc0") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-line-span-0.1.4 (c (n "line-span") (v "0.1.4") (h "02s7vkjrfc3yf1vjqzri67kk7by7jasrmafl30bs184b3h1n69ya") (f (quote (("default" "alloc") ("alloc"))))))

(define-public crate-line-span-0.1.5 (c (n "line-span") (v "0.1.5") (h "12j07vn0vnchlvlrh3cc3320jaz07sggcj42l6f0j0365wxi5z19") (f (quote (("default" "alloc") ("alloc"))))))

