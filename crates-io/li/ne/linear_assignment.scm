(define-module (crates-io li ne linear_assignment) #:use-module (crates-io))

(define-public crate-linear_assignment-0.0.1 (c (n "linear_assignment") (v "0.0.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.2.14") (f (quote ("quickcheck" "arbitrary"))) (d #t) (k 2)) (d (n "num") (r "^0.1.24") (d #t) (k 0)) (d (n "quickcheck") (r "^0.2.18") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2.18") (d #t) (k 2)) (d (n "rand") (r "*") (d #t) (k 2)))) (h "0ilzx4202q5nx3cjppbmx1mqp1pn2wnfs89mrwm9lz9nvxv3dim3")))

