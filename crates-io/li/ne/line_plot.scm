(define-module (crates-io li ne line_plot) #:use-module (crates-io))

(define-public crate-line_plot-0.1.0 (c (n "line_plot") (v "0.1.0") (d (list (d (n "piston2d-graphics") (r "^0.21.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.64.0") (d #t) (k 0)))) (h "1jr7fpwf6dcaivzzndwbinxyyyqcf3q2aadlakdbl13dwfkn165m")))

(define-public crate-line_plot-0.1.1 (c (n "line_plot") (v "0.1.1") (d (list (d (n "piston2d-graphics") (r "^0.21.1") (d #t) (k 0)) (d (n "piston_window") (r "^0.64.0") (d #t) (k 0)))) (h "13m7q2gsr4vly5zfpljryj2dyp5dd0007i9axj2nbg53nxxiflvj")))

(define-public crate-line_plot-0.1.2 (c (n "line_plot") (v "0.1.2") (d (list (d (n "gl") (r "^0.6.1") (d #t) (k 0)) (d (n "glhelper") (r "^0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.29.1") (d #t) (k 0)))) (h "04hfj7l5x5wqgf0kglhsjsyz257ba965c401rn3d7v3ndca60hsd")))

(define-public crate-line_plot-0.1.3 (c (n "line_plot") (v "0.1.3") (d (list (d (n "gl") (r "^0.6.1") (d #t) (k 0)) (d (n "glhelper") (r "^0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.29.1") (d #t) (k 0)))) (h "0s7zx809km8irgjp4k77c5g6dx8k7dgljmap9slfi902mhfca07j")))

(define-public crate-line_plot-0.1.4 (c (n "line_plot") (v "0.1.4") (d (list (d (n "gl") (r "^0.6.1") (d #t) (k 0)) (d (n "glhelper") (r "^0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.29.1") (d #t) (k 0)))) (h "039h98biqx4y7y52f49xws8jm3bpw0sv9xl7mjjslcjy74yhpsjz")))

(define-public crate-line_plot-0.1.5 (c (n "line_plot") (v "0.1.5") (d (list (d (n "gl") (r "^0.6.1") (d #t) (k 0)) (d (n "glhelper") (r "^0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.29.1") (d #t) (k 0)))) (h "1wg88d0b1fsqmlia6xb974dx15bzyw2z051wcp0nyc67slcdxkv1")))

(define-public crate-line_plot-0.1.6 (c (n "line_plot") (v "0.1.6") (d (list (d (n "gl") (r "^0.6.1") (d #t) (k 0)) (d (n "glhelper") (r "^0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12.2") (d #t) (k 0)) (d (n "sdl2") (r "^0.29.1") (d #t) (k 0)))) (h "1a2chfadjr6rk3chmvjx3k7q465rm7sjwlnzxysrris3g3d0gs7f")))

(define-public crate-line_plot-0.1.7 (c (n "line_plot") (v "0.1.7") (d (list (d (n "gl") (r "^0.6.1") (d #t) (k 0)) (d (n "glhelper") (r "^0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "^0.12.3") (d #t) (k 0)) (d (n "sdl2") (r "^0.29.1") (d #t) (k 0)))) (h "0qbqdaa52rr4lb6lg7pc89kdqna09rjyzcb5qkm4qx7j6nfr1pv0")))

