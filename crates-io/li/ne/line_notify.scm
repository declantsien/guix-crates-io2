(define-module (crates-io li ne line_notify) #:use-module (crates-io))

(define-public crate-line_notify-0.1.0 (c (n "line_notify") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "0aiz641ms2g0bvzvaj2px95y057j8k5g4qs44xhwkhwfd7g5gics")))

(define-public crate-line_notify-0.1.1 (c (n "line_notify") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1kds2yr0mmfm9lgdf1pli84370xcrkmk40f8hxqqyahvxhdzdy05")))

(define-public crate-line_notify-0.2.0 (c (n "line_notify") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1bmhlwz53v9y8lwlz5ajvhm9qxx3kqv64bqhxz4wsb1s8jpq3n27")))

(define-public crate-line_notify-0.2.1 (c (n "line_notify") (v "0.2.1") (d (list (d (n "reqwest") (r "^0.11.24") (f (quote ("multipart"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vybf9n75brjfsrlyyaahm2yxawxxvlz3bi82lkchfk46y1g6zaa")))

