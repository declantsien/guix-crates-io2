(define-module (crates-io li ne line_botty) #:use-module (crates-io))

(define-public crate-line_botty-0.1.0 (c (n "line_botty") (v "0.1.0") (d (list (d (n "copperline") (r "^0.3.0") (d #t) (k 0)) (d (n "docopt") (r "^0.6.86") (d #t) (k 0)) (d (n "git2") (r "^0.6.3") (d #t) (k 0)) (d (n "json_flex") (r "^0.3.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)))) (h "000rr1k1rwilai97gf9nhfv5wgivxr7j0szigiwyna1arrg0p2gc")))

