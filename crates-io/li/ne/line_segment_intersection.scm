(define-module (crates-io li ne line_segment_intersection) #:use-module (crates-io))

(define-public crate-line_segment_intersection-0.1.0 (c (n "line_segment_intersection") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0sys7qyxq9dss8aqk7m1n9vsxlcbmc4hbwh5szmy7a2p03n3sgxf") (y #t)))

(define-public crate-line_segment_intersection-0.1.1 (c (n "line_segment_intersection") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1pdq5dawa7vjwnwcl6g5harlmjd5jy2266fxqklw2n0249izdzms") (y #t)))

(define-public crate-line_segment_intersection-0.2.0 (c (n "line_segment_intersection") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "163f2x328ff4i8pzzf5mkxmg0y1jrpfx6k1bl73wsgah9x3039vc") (y #t)))

