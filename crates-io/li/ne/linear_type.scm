(define-module (crates-io li ne linear_type) #:use-module (crates-io))

(define-public crate-linear_type-0.1.0 (c (n "linear_type") (v "0.1.0") (h "1dx3330mlqgc2db1ir1k2niavw25yaf39igy746jxi47mvhypy44")))

(define-public crate-linear_type-0.2.0 (c (n "linear_type") (v "0.2.0") (d (list (d (n "no-panic") (r "^0.1.26") (o #t) (d #t) (k 0)))) (h "09nz9ilsla00z99kd14s13v1i2nmp1snfnxs6is2crp0ayv8gi5p") (s 2) (e (quote (("compile_error" "dep:no-panic"))))))

(define-public crate-linear_type-0.3.0 (c (n "linear_type") (v "0.3.0") (d (list (d (n "no-panic") (r "^0.1.26") (o #t) (d #t) (k 0)))) (h "0spls5c1fqbvhbcr2kz8dljx723ybr3rlvinw0q82d5nxygsdp8w") (s 2) (e (quote (("compile_error" "dep:no-panic"))))))

(define-public crate-linear_type-0.3.1 (c (n "linear_type") (v "0.3.1") (d (list (d (n "no-panic") (r "^0.1.26") (o #t) (d #t) (k 0)))) (h "1vvvscy2rlbaab42zrfwp4isb53zlkbdzgmxi64iycdmpiwkb20r") (s 2) (e (quote (("compile_error" "dep:no-panic"))))))

(define-public crate-linear_type-0.4.0 (c (n "linear_type") (v "0.4.0") (d (list (d (n "no-panic") (r "^0.1.26") (o #t) (d #t) (k 0)))) (h "09l5dr308nkcml622i1p6j4hrq3fdd92jbsc94dxp89v16r1s6zy") (s 2) (e (quote (("compile_error" "dep:no-panic"))))))

(define-public crate-linear_type-0.5.0 (c (n "linear_type") (v "0.5.0") (d (list (d (n "no-panic") (r "^0.1.26") (o #t) (d #t) (k 0)))) (h "0dgkqrdqpccfgm0l4d5rcbqgab86p02wjhhrsghg0fhmqidkmrgf") (s 2) (e (quote (("compile_error" "dep:no-panic"))))))

