(define-module (crates-io li ne lines_from_file) #:use-module (crates-io))

(define-public crate-lines_from_file-0.1.0 (c (n "lines_from_file") (v "0.1.0") (h "1ai6596lvkjgbdxzrppjpfpfxj46zybahb474xf47pv85ri48330")))

(define-public crate-lines_from_file-0.1.1 (c (n "lines_from_file") (v "0.1.1") (h "0jwxj6gp77kkf77faxlxqam24kp599l6w8hvnswcas6b6wfxx14n")))

(define-public crate-lines_from_file-0.1.2 (c (n "lines_from_file") (v "0.1.2") (h "127c3pfhrnfh782iljxna16a4pvc5grjdar1241d6ib9w4jdd5vm")))

