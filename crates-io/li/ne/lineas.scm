(define-module (crates-io li ne lineas) #:use-module (crates-io))

(define-public crate-lineas-0.1.0 (c (n "lineas") (v "0.1.0") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)))) (h "0ypgrdvr028lwyws3fglsahgpg5k85jrx432r6mxvnafskvygp1n")))

(define-public crate-lineas-0.1.1 (c (n "lineas") (v "0.1.1") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "plotters") (r "^0.3.3") (o #t) (d #t) (k 0)))) (h "0qk6n69dlh53d5ahk4011r33biwq7n0vz5qh70zmivn6h4ljma0z") (f (quote (("plotting" "plotters"))))))

