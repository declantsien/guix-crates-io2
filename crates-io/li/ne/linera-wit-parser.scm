(define-module (crates-io li ne linera-wit-parser) #:use-module (crates-io))

(define-public crate-linera-wit-parser-0.2.0 (c (n "linera-wit-parser") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "id-arena") (r "^2") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.8") (k 0)) (d (n "rayon") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "unicode-normalization") (r "^0.1.19") (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.2") (d #t) (k 0)) (d (n "wast") (r "^33") (o #t) (k 0)))) (h "16im5h3ky11qr49ax9k3hjryxp28hhzyrdj339jn2xp8n75xghfm")))

