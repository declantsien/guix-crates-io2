(define-module (crates-io li ne line_diff) #:use-module (crates-io))

(define-public crate-line_diff-0.1.0 (c (n "line_diff") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0byzrpchbz827pkldsigjxzfvpa6agh69lmldx556ijg54hwqsa2")))

(define-public crate-line_diff-0.2.0 (c (n "line_diff") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0df8lqxihy8z49a9ljsrnrn6wvg3lqfcnh38qrc1y6vbg01xih34")))

(define-public crate-line_diff-0.3.0 (c (n "line_diff") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0lcwwvw8726b79whqsj7slf25lqfshppjjd7zx9kpj7z65g5j9kr")))

(define-public crate-line_diff-0.4.0 (c (n "line_diff") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0krj4wyslmyqlzcddhylz75254h95l6mazfllnpjzhv0ng223dgd")))

(define-public crate-line_diff-0.5.0 (c (n "line_diff") (v "0.5.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0dvbm1f87f8wfya15vibs1m7z4mmd2nlhgacza15vi04slxamaiz")))

(define-public crate-line_diff-0.5.1 (c (n "line_diff") (v "0.5.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)))) (h "0rkrgh4sk3cxkjhar9k7wax9pnn7kzzvcg5hz6ll4zjh0kv6zynz")))

(define-public crate-line_diff-0.6.0 (c (n "line_diff") (v "0.6.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "0fjk7x709ikajwnjp0gv844rlqyb4wjkiv5wrvrl1h43fms34nmz")))

(define-public crate-line_diff-0.6.1 (c (n "line_diff") (v "0.6.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)))) (h "1qd6l0cxjjapydr91dhhjh5cl1208jx21kmv5zzkmc50i7bysj4p")))

(define-public crate-line_diff-0.7.0 (c (n "line_diff") (v "0.7.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.8") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (f (quote ("term_size"))) (d #t) (k 0)))) (h "1crlm7s3rsb76f1226aivz4zwl7yd2d50ywbsdk3sav9zj1094zm")))

(define-public crate-line_diff-0.8.0 (c (n "line_diff") (v "0.8.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (f (quote ("term_size"))) (d #t) (k 0)))) (h "1pxm0rfsxxl06d85xqyq02c70wqy83jfzcap30cbl3lbkr52v5m2")))

(define-public crate-line_diff-0.9.0 (c (n "line_diff") (v "0.9.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (f (quote ("term_size"))) (d #t) (k 0)))) (h "1p4wvlmjchn69b3z46iryhm61j4a63jqqb73cvxik0p0kqmn3588")))

(define-public crate-line_diff-0.10.0 (c (n "line_diff") (v "0.10.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.12") (d #t) (k 0)) (d (n "textwrap") (r "^0.11") (f (quote ("term_size"))) (d #t) (k 0)))) (h "16dzcwfhp6pkcfkbkqf5i45kwmh22w61li64lmvn0gs8yrzwgnbw")))

(define-public crate-line_diff-0.11.0 (c (n "line_diff") (v "0.11.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.12") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0cbqfh11pr1i7lycgyf8nrw5hjcmd5msl5cgirmc85acyc0idnnf")))

(define-public crate-line_diff-0.12.0 (c (n "line_diff") (v "0.12.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.13") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "1d19iq6xh5srlqgsr1z07l7s5dm6s39pxvic9zyp1r334ipapxh1")))

(define-public crate-line_diff-0.13.0 (c (n "line_diff") (v "0.13.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.14") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0bv0zq918aq7j70cnxnwym1awsdkhcga4wkzlm8r1c0hh99369z3")))

(define-public crate-line_diff-0.13.1 (c (n "line_diff") (v "0.13.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.9") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "01gqlk1w71kk30i5yd4z4m8i2zr7mry0ymz8mansavyi8zxnzyr4")))

(define-public crate-line_diff-0.13.2 (c (n "line_diff") (v "0.13.2") (d (list (d (n "difference") (r "^2.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "textwrap") (r "^0.16") (f (quote ("terminal_size"))) (d #t) (k 0)))) (h "0n7qixwgdl6c49rr29ghy86x4f3wrajrjw00slpmkdhjnd1g7vn7")))

