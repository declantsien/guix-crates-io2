(define-module (crates-io li ne linear_algebra) #:use-module (crates-io))

(define-public crate-linear_algebra-0.1.0 (c (n "linear_algebra") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "17raxijc6npp3fj16v6skd9cg04q20w4c9wwxcza2y8zp1pnmcnk")))

(define-public crate-linear_algebra-0.1.1 (c (n "linear_algebra") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "07bhs23gfs391blgqg45j871in7xa5qivr9wil48j2dfjwdn0k1y")))

(define-public crate-linear_algebra-0.1.2 (c (n "linear_algebra") (v "0.1.2") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "1h3qv4ag4ffl2pjpyxgys6ysnn7vakzvyw439srfq371ar3l5vk0")))

(define-public crate-linear_algebra-0.1.3 (c (n "linear_algebra") (v "0.1.3") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "1008hz670a7873r3dyz9aqqhlgh1ry77ahcqwibryp98nx9gvr49")))

(define-public crate-linear_algebra-0.1.4 (c (n "linear_algebra") (v "0.1.4") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "0r8appmy50hc9kl8pgxyxswn32wq5kzvz14i8gh5a6qkqwq8z8lp")))

(define-public crate-linear_algebra-0.1.5 (c (n "linear_algebra") (v "0.1.5") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "095vknc6cic5i0y6bvz9wlymm38ps08ci1mhbdi1lsnlrmjvlq2a")))

(define-public crate-linear_algebra-0.1.6 (c (n "linear_algebra") (v "0.1.6") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "1xy6j08ryqlsiflzcvgb3jvd50kfqd8w9j2lik9ll9i1zmaaz1jf")))

(define-public crate-linear_algebra-0.1.7 (c (n "linear_algebra") (v "0.1.7") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "0cmbgaw7p50qarqrhs9k2h24zd2fnb9l2hv99q5fmfr6qkzrdk45")))

(define-public crate-linear_algebra-0.1.8 (c (n "linear_algebra") (v "0.1.8") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "0sz15kwza6yq0i1zj6z6gz383q7hijwm9hrk9civcd71n46an4dw")))

(define-public crate-linear_algebra-0.1.9 (c (n "linear_algebra") (v "0.1.9") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "1zm0zm8cjsdwa1y8fs2jkkhlaxzk7bf0kgvyg3z7kc0bkp0dj3p4")))

(define-public crate-linear_algebra-1.0.0 (c (n "linear_algebra") (v "1.0.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "1rq8ircgaayjn38g5b9xw167lx8b0bsg5isnj22da25drrmii6qr")))

(define-public crate-linear_algebra-1.0.1 (c (n "linear_algebra") (v "1.0.1") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "0f38kzd07i45kyiy9471a6dnvc7j9yh64sbwg5mzx5k0ryyvh6yz")))

(define-public crate-linear_algebra-1.0.2 (c (n "linear_algebra") (v "1.0.2") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "0b0ca9wkxcpk5g4f2vgrsji9f2n8j6fk8344qx17sn3li0adc5k8")))

(define-public crate-linear_algebra-1.0.3 (c (n "linear_algebra") (v "1.0.3") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "026b4akvip50c7r2r7cd1cnjp2qbfawxxznfsshfyd1kly5dzka8")))

(define-public crate-linear_algebra-2.0.0 (c (n "linear_algebra") (v "2.0.0") (d (list (d (n "assert_approx_eq") (r "^1.0.0") (d #t) (k 0)))) (h "0mvmj57hxdvnllp21k578b5ma43c5a4ywvbkwp053l8syfq2js96")))

