(define-module (crates-io li ne line-straddler) #:use-module (crates-io))

(define-public crate-line-straddler-0.1.0 (c (n "line-straddler") (v "0.1.0") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "1bwc1cqlrp3r5zgr9hg55qqrcya4mb1v8k8sia0m9ccw34ssq6q0") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.40")))

(define-public crate-line-straddler-0.1.1 (c (n "line-straddler") (v "0.1.1") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "1q53hci7kihv8wms4v2g9z8qgi2prglzps3mbiifksxjn7p5a8q1") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.40")))

(define-public crate-line-straddler-0.2.0 (c (n "line-straddler") (v "0.2.0") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "1g0xgiscfqa8mwcsdbci631qlvif5zkh0vqkyk5ham8i55iqcvv4") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.40")))

(define-public crate-line-straddler-0.2.1 (c (n "line-straddler") (v "0.2.1") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0ksrslrjp6m1z0labxglp16ppi3w7p6hbn770r2552570vp5nfa8") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.40")))

(define-public crate-line-straddler-0.2.2 (c (n "line-straddler") (v "0.2.2") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "0vib011dl0vmmzpbyig1zwj06swn44454lnkn0yzd7llbx1nb5nj") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.63")))

(define-public crate-line-straddler-0.2.3 (c (n "line-straddler") (v "0.2.3") (d (list (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "libm") (r "^0.2.6") (o #t) (d #t) (k 0)))) (h "107zk0pc2y6d3jcn76q0c5i1x06w2cyprnzfpd899irmlwv6g2p1") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (r "1.63")))

