(define-module (crates-io li ne linera-wit-bindgen-guest-rust) #:use-module (crates-io))

(define-public crate-linera-wit-bindgen-guest-rust-0.2.0 (c (n "linera-wit-bindgen-guest-rust") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "wit-bindgen-guest-rust-macro") (r "^0.2.0") (o #t) (d #t) (k 0) (p "linera-wit-bindgen-guest-rust-macro")))) (h "0rr0qm3ycla3n338wlrn0818ixvz92n8f9v5g3y7bvphhbxzby67") (f (quote (("macros" "wit-bindgen-guest-rust-macro") ("default" "macros"))))))

