(define-module (crates-io li ne linear_sdk) #:use-module (crates-io))

(define-public crate-linear_sdk-0.0.1 (c (n "linear_sdk") (v "0.0.1") (d (list (d (n "graphql_client") (r "^0.11") (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (k 0)) (d (n "pem") (r "^1.1") (o #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (k 0)) (d (n "ring") (r "^0.16") (o #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.3") (d #t) (k 0)))) (h "061z9fy90yrgf4ir0gmrz8ih08qkvlz997rqgzgh987cvyflr4mb") (f (quote (("rustls-tls" "reqwest/rustls-tls" "ring" "pem") ("native-tls" "reqwest/default-tls" "openssl") ("default" "rustls-tls"))))))

