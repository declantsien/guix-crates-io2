(define-module (crates-io li ne linear_search_trie) #:use-module (crates-io))

(define-public crate-linear_search_trie-0.1.0 (c (n "linear_search_trie") (v "0.1.0") (h "0n42z4y32969jqsrk1bd5r000nmn1lygx2k731144vygzycnqnna")))

(define-public crate-linear_search_trie-0.1.1 (c (n "linear_search_trie") (v "0.1.1") (h "1vnsi00vcbqkk1jf021ra09jr7jb2m9w9zdrawsldm8mc5lqzi7g")))

(define-public crate-linear_search_trie-0.1.2 (c (n "linear_search_trie") (v "0.1.2") (h "0h44vzxd4m2fasnxyncfxfq42n3299bh2z0wy14nrpsv61xkbzhf")))

