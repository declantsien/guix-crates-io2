(define-module (crates-io li ne linera-sdk-derive) #:use-module (crates-io))

(define-public crate-linera-sdk-derive-0.0.1 (c (n "linera-sdk-derive") (v "0.0.1") (h "1h1yay9m2a9b0xcsd5m49akfv47bs3yvqh8k8x3xwnvq0ifkpby1") (y #t)))

(define-public crate-linera-sdk-derive-0.3.0 (c (n "linera-sdk-derive") (v "0.3.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1z6rjlrdwpjgi758r2qxisbg82q2yzni0npy9qrbyd8jwnvrp3rx") (y #t)))

(define-public crate-linera-sdk-derive-0.4.0 (c (n "linera-sdk-derive") (v "0.4.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1xlm0r51apxqb2n2vyb6cg9ddrgb2l0818bp3mk01za1kfayfxda") (y #t)))

(define-public crate-linera-sdk-derive-0.4.1 (c (n "linera-sdk-derive") (v "0.4.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "033xm232i91rzjj4zwfvndw109m4g0ln7wzbi8qyn71mm56ih1r4")))

(define-public crate-linera-sdk-derive-0.5.0 (c (n "linera-sdk-derive") (v "0.5.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bbbw7509z59jpym40z1kzq0jk4b2dy13vjqgavlgypc4anmiimk")))

(define-public crate-linera-sdk-derive-0.6.0 (c (n "linera-sdk-derive") (v "0.6.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "18vxdds786jxnmf4wj70rijxkxdd0yiszhl3j80ilbr462ambkbc")))

(define-public crate-linera-sdk-derive-0.7.0 (c (n "linera-sdk-derive") (v "0.7.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lw204niyg1j7zbfyzsbjqa4241cw5c4ria0wfcpzw5zrab572bm")))

(define-public crate-linera-sdk-derive-0.7.1 (c (n "linera-sdk-derive") (v "0.7.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cc2rbcd8wpmvndvyn6lnl4h7z3m1pcdh49bna9w5c45w9gd8634")))

(define-public crate-linera-sdk-derive-0.8.0 (c (n "linera-sdk-derive") (v "0.8.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1m8zgg7r35c63jl42xg3820pxj050sf36dlcx96fljs7hf0p3rah")))

(define-public crate-linera-sdk-derive-0.9.0 (c (n "linera-sdk-derive") (v "0.9.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qj6ajkj6vh6bvz7033xwrrch38z8zh4b07p8741pmg6vzbmwlp3")))

(define-public crate-linera-sdk-derive-0.10.0 (c (n "linera-sdk-derive") (v "0.10.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "12pz4fgsaky5nnbnpw94cya53p6kf5dx794l7q8qwlbkwlwaaswj")))

(define-public crate-linera-sdk-derive-0.10.1 (c (n "linera-sdk-derive") (v "0.10.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "059lyqir8ahi0z0lfzp05lm6s9c9s4gqys4lcngr2djybjz3h8il")))

(define-public crate-linera-sdk-derive-0.11.0 (c (n "linera-sdk-derive") (v "0.11.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "19mq10056ygik0cj4kz8pqpy2j6wwd27z0wxybsmliqw0yhb1bim")))

(define-public crate-linera-sdk-derive-0.10.2 (c (n "linera-sdk-derive") (v "0.10.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0jdi398z8dxhpl5idnsi86m6nbydfqy3a4l77jm3mpycmihfqk3r")))

(define-public crate-linera-sdk-derive-0.10.3 (c (n "linera-sdk-derive") (v "0.10.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gnx1avjg25ha2gy9bbxdr7mn5i4iq32cysnn2pap9q793n0c1v5")))

(define-public crate-linera-sdk-derive-0.11.1 (c (n "linera-sdk-derive") (v "0.11.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "071bxvdkjwz43kkq57v1qxzkgzxk83r115izfap2y3gxlrgjp2d5")))

(define-public crate-linera-sdk-derive-0.10.4 (c (n "linera-sdk-derive") (v "0.10.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0irkg9d9gwml7qkyxvl5b3v9hgra0m2vmqkg9kz55vc8rz6mxmsb")))

(define-public crate-linera-sdk-derive-0.11.2 (c (n "linera-sdk-derive") (v "0.11.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bd7z8hv7iqpfbkyflh8p2b0qixvrk8j27azh4aqckvngkpar2d0")))

(define-public crate-linera-sdk-derive-0.11.3 (c (n "linera-sdk-derive") (v "0.11.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1nn16spmslqc9bcfpsl6mwllgnlm7npbl4xv0rry0vvr79y4gwwg")))

(define-public crate-linera-sdk-derive-0.10.5 (c (n "linera-sdk-derive") (v "0.10.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.52") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qxrmg170jrzcrzvsh5406mr6arn46xvlfkmjankwnyvqcxdwzim")))

