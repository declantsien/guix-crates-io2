(define-module (crates-io li ne linearalgebra) #:use-module (crates-io))

(define-public crate-linearalgebra-0.1.0 (c (n "linearalgebra") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0a1cm68n5zykbd8b7vhf4bhs186hi0xj60wiqhzf1dpyh1jqf6hx")))

(define-public crate-linearalgebra-0.1.1 (c (n "linearalgebra") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03w9x3g3wv5104d6677aqqn174xm09g8qwpijrdnbsr624aw7157")))

(define-public crate-linearalgebra-0.1.2 (c (n "linearalgebra") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0piq26klnw5v6i9x2m82j9ngqa65a2bf9nbrqq86zc6b0m1c1xrf")))

(define-public crate-linearalgebra-0.1.3 (c (n "linearalgebra") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "01zs396x1cf4hq6hm5iqsm4kxg0g88zkqfbxlak78gwfns01fg2s")))

(define-public crate-linearalgebra-0.1.4 (c (n "linearalgebra") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0alrz8ypsl659a86s80k8lbwbjialkb08zil3lwdlfw3fn7yr9y9")))

(define-public crate-linearalgebra-0.1.5 (c (n "linearalgebra") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "00dh75z37w68m7b9dbv08maid9l0zfbl01c9jlkn0729sq60khkf")))

(define-public crate-linearalgebra-0.1.6 (c (n "linearalgebra") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1bq7cv8wfc4aid0philc8s6ji6g1fvciigx2wr9z52zd5ask654x")))

(define-public crate-linearalgebra-0.1.7 (c (n "linearalgebra") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "0903a21yj5bf7h92n130i35k79wpjpc4jk92a02f4klq0853730n")))

(define-public crate-linearalgebra-0.1.8 (c (n "linearalgebra") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "0cw5zln7br23r1mza4p60mi0l0c1i7jl0fxg8iga66c8h32a0dyp")))

(define-public crate-linearalgebra-0.1.9 (c (n "linearalgebra") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "0s3kvr4r6krh9qs6ibz3gn2n8ywz3fi5m5wh0c75891i16z52lcr")))

(define-public crate-linearalgebra-0.2.0 (c (n "linearalgebra") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "1wbkcmlh0flq4033ck1fcc6jf87s5rl6wdax1f09dbnc9hsfmvqj") (f (quote (("simd"))))))

(define-public crate-linearalgebra-0.2.1 (c (n "linearalgebra") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rayon") (r "^1.7") (d #t) (k 0)))) (h "1gfzrbydkdkdn0dl8jbghinm1f8ql5vkxa7da98p3z6kx06nfpcx") (f (quote (("simd"))))))

