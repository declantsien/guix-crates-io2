(define-module (crates-io li ne lines) #:use-module (crates-io))

(define-public crate-lines-0.0.1 (c (n "lines") (v "0.0.1") (h "1mx17py2hlzx0q7r4sf1y14645n6gmvwl27nbznknj3v3grhbgzq")))

(define-public crate-lines-0.0.2 (c (n "lines") (v "0.0.2") (h "1z11zl4xvjn594l2hglphpcv1kr8cgayspqirvk92lp5fk09wz7s")))

(define-public crate-lines-0.0.3 (c (n "lines") (v "0.0.3") (d (list (d (n "memchr") (r "*") (d #t) (k 0)))) (h "04kk6ahkl2mglam18dfr4gq956l0haya6jrhgf41q9z1df35yh0c")))

(define-public crate-lines-0.0.4 (c (n "lines") (v "0.0.4") (d (list (d (n "memchr") (r "^0.1") (d #t) (k 0)))) (h "04iv0lj0rwpacs2hwd9sa0pwh25g10f505kb8jfv9cfip4qana67")))

(define-public crate-lines-0.0.5 (c (n "lines") (v "0.0.5") (d (list (d (n "memchr") (r "^1.0") (d #t) (k 0)))) (h "0nkjldr41mbmwv90rqlzcy9vxmmcxr7xqd52y2r8pjcdhcm51j6f")))

(define-public crate-lines-0.0.6 (c (n "lines") (v "0.0.6") (d (list (d (n "memchr") (r "^1.0") (d #t) (k 0)))) (h "18kwxs887h45xp32sjvx927f5z05vgfx29szn1x0yas5i5lhp7nr")))

