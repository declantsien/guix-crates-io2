(define-module (crates-io li ne lineindex) #:use-module (crates-io))

(define-public crate-lineindex-0.1.0 (c (n "lineindex") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "10yv3mlwvax0gs9rh3207yaaqfiwnnjgxn2f8lm74d1npjp8f417")))

(define-public crate-lineindex-0.1.1 (c (n "lineindex") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)))) (h "0n8ba4vb4qa9livlqxp2n7kxyb6w6n0dar24i8pp8wl9n356ckar")))

