(define-module (crates-io li ne line-col) #:use-module (crates-io))

(define-public crate-line-col-0.1.0 (c (n "line-col") (v "0.1.0") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "1ygsbfy42hllxmql3w9gpjiqh2rfd68vnwxi1ylk34h8s9sp10dx") (f (quote (("grapheme-clusters" "unicode-segmentation") ("default"))))))

(define-public crate-line-col-0.1.1 (c (n "line-col") (v "0.1.1") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "1ac45n4g13m6yrnvpvwd569s3jjcxqjja22gxxnz1hl574ahmvc6") (f (quote (("grapheme-clusters" "unicode-segmentation") ("default"))))))

(define-public crate-line-col-0.2.0 (c (n "line-col") (v "0.2.0") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "1pfdz48zv6iyaic3sxp52hp43ny6y5pzc5di36farwhrlwqivc99") (f (quote (("grapheme-clusters" "unicode-segmentation") ("default"))))))

(define-public crate-line-col-0.2.1 (c (n "line-col") (v "0.2.1") (d (list (d (n "unicode-segmentation") (r "^1.6.0") (o #t) (d #t) (k 0)))) (h "1nvqjxn2kkxkph3qdlkc5xq1m2ygla4l0saga778sp2vp3vcsscy") (f (quote (("grapheme-clusters" "unicode-segmentation") ("default"))))))

