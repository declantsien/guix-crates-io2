(define-module (crates-io li ne linebuffer) #:use-module (crates-io))

(define-public crate-linebuffer-0.1.0 (c (n "linebuffer") (v "0.1.0") (d (list (d (n "arraydeque") (r "^0.4") (f (quote ("use_generic_array"))) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "0nlxz16p3k028wandhykirrlc6zpv0gigwxfq1bmmjsji8psrnvj")))

(define-public crate-linebuffer-0.1.1 (c (n "linebuffer") (v "0.1.1") (d (list (d (n "arraydeque") (r "^0.4") (f (quote ("use_generic_array"))) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "1cniha6mizhx6hc2vnlknhp5769dgvbw5c1l9yw342wryi323bpm")))

(define-public crate-linebuffer-0.1.2 (c (n "linebuffer") (v "0.1.2") (d (list (d (n "arraydeque") (r "^0.4") (f (quote ("use_generic_array"))) (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "08fyvnz462bwmyr2x324gc0crsgha3kj3qicwpwkz247g9lfhzfs")))

