(define-module (crates-io li ne linearify) #:use-module (crates-io))

(define-public crate-linearify-0.1.0 (c (n "linearify") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "0xl9r7394sai8xqnv31c88rhy462xrk4ianshkd6wgx7sp8hr3f3")))

(define-public crate-linearify-0.1.1 (c (n "linearify") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "1qcx2546v7inkwss4sassjirx5ifhicybv9pc2hm3fkhllv8zrbr")))

(define-public crate-linearify-0.1.2 (c (n "linearify") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "0lbq0i8hqwwzcz56kp05ggngnwc6cf25zdscdk1pm58h0ymadyd7")))

(define-public crate-linearify-0.1.3 (c (n "linearify") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "08c8qywa3y1gk78vci8jc1y4n0ry9ni0029625xbbi49jd1g0r55")))

(define-public crate-linearify-0.1.4 (c (n "linearify") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "0kgwavxkdxxkid3sq8gk03sw84v6f3harwlpbidv1253z0xw14z1")))

(define-public crate-linearify-0.1.5 (c (n "linearify") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "1hjyzyaxwz7n3c5hbzc4dykjr5naxhx3vb8sg6j4bwszbf4v5hh4")))

(define-public crate-linearify-0.1.6 (c (n "linearify") (v "0.1.6") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "zstd") (r "^0.12.3") (d #t) (k 0)))) (h "1b058954lxl4s1l7cm3j3clrmqaw0ka5qbghxwa6464dk12r4c24")))

