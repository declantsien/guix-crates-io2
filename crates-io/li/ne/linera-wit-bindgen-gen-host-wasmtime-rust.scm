(define-module (crates-io li ne linera-wit-bindgen-gen-host-wasmtime-rust) #:use-module (crates-io))

(define-public crate-linera-wit-bindgen-gen-host-wasmtime-rust-0.2.0 (c (n "linera-wit-bindgen-gen-host-wasmtime-rust") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wasmtime") (r "^1.0") (d #t) (k 2)) (d (n "wasmtime-wasi") (r "^1.0") (d #t) (k 2)) (d (n "wit-bindgen-core") (r "^0.2.0") (d #t) (k 0) (p "linera-wit-bindgen-core")) (d (n "wit-bindgen-gen-rust-lib") (r "^0.2.0") (d #t) (k 0) (p "linera-wit-bindgen-gen-rust-lib")))) (h "13g0jzdl93b8s068wccsv680sz12qbm4hf13gd2hpmvi7c9xinwm")))

