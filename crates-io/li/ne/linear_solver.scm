(define-module (crates-io li ne linear_solver) #:use-module (crates-io))

(define-public crate-linear_solver-0.1.0 (c (n "linear_solver") (v "0.1.0") (d (list (d (n "bloom") (r "^0.3.2") (d #t) (k 0)))) (h "1rkr6725flrkb6iqqs9s1nr991ylad75s6sb9p3gzb2yh1cdjdcq")))

(define-public crate-linear_solver-0.2.0 (c (n "linear_solver") (v "0.2.0") (d (list (d (n "bloom") (r "^0.3.2") (d #t) (k 0)))) (h "08slf346kj7qvphcisfc96f47d4x866rni71qpg7w4dv3p3mbd9m")))

(define-public crate-linear_solver-0.2.1 (c (n "linear_solver") (v "0.2.1") (d (list (d (n "bloom") (r "^0.3.2") (d #t) (k 0)))) (h "118yvkbzhn6zf2qnyaa2r4myz6807j0bgmsd4als02h61d4jl41f")))

(define-public crate-linear_solver-0.2.2 (c (n "linear_solver") (v "0.2.2") (d (list (d (n "cuckoofilter") (r "^0.3.2") (d #t) (k 0)))) (h "18kscc0h7mjb3m6llzrhqrpx7457s2gacmkdl1bykn0vr99549bc")))

