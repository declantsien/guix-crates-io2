(define-module (crates-io li ne lineselect) #:use-module (crates-io))

(define-public crate-lineselect-0.1.0 (c (n "lineselect") (v "0.1.0") (d (list (d (n "clap") (r "^4.3.11") (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (d #t) (k 0)))) (h "166l5kkn6vwqbmhvj0vcgm6lnmpkihp0sn3x00q95sjljx89jk76")))

