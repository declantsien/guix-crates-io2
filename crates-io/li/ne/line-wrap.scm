(define-module (crates-io li ne line-wrap) #:use-module (crates-io))

(define-public crate-line-wrap-0.1.0 (c (n "line-wrap") (v "0.1.0") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "safemem") (r "^0.3") (d #t) (k 0)))) (h "0pn00i043qjbi6zb3fm66hdgw6qac9b6l3x3ifkmk2irmdph44xn")))

(define-public crate-line-wrap-0.1.1 (c (n "line-wrap") (v "0.1.1") (d (list (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "safemem") (r "^0.3") (d #t) (k 0)))) (h "1ffa2whkyh9mwvdlpk6v8pjkg8p8mlzyjfymq5adll9a18sl80zk")))

(define-public crate-line-wrap-0.2.0 (c (n "line-wrap") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "03p7265v8al6fmj04hlx8kyavmq131mi3lcqp0gx4c6j9b9c86yx") (r "1.37.0")))

