(define-module (crates-io li ne line_membership) #:use-module (crates-io))

(define-public crate-line_membership-0.0.1 (c (n "line_membership") (v "0.0.1") (d (list (d (n "base64") (r "~0.7.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http") (r "~0.2") (d #t) (k 0)) (d (n "hyper") (r "~0.14") (f (quote ("full"))) (d #t) (k 0)) (d (n "hyper-tls") (r "~0.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "16pqzaw69ha6ds2c0ii2i68x29wsll0cy2mnvn6fcknw6sg1bfc2")))

