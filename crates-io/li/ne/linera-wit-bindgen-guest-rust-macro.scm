(define-module (crates-io li ne linera-wit-bindgen-guest-rust-macro) #:use-module (crates-io))

(define-public crate-linera-wit-bindgen-guest-rust-macro-0.2.0 (c (n "linera-wit-bindgen-guest-rust-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.2") (d #t) (k 0) (p "linera-wit-bindgen-core")) (d (n "wit-bindgen-gen-guest-rust") (r "^0.2") (d #t) (k 0) (p "linera-wit-bindgen-gen-guest-rust")))) (h "1g8p0vqsmphl1iwlgbc8wz2syp0cf7hagq4x9ay641axm5vi100g")))

