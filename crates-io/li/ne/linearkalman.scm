(define-module (crates-io li ne linearkalman) #:use-module (crates-io))

(define-public crate-linearkalman-0.1.0 (c (n "linearkalman") (v "0.1.0") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0b98f6bdzyxkb50xyg8wx4aw1r1g7xcbb4npjn9y8dw69qjcayjp")))

(define-public crate-linearkalman-0.1.1 (c (n "linearkalman") (v "0.1.1") (d (list (d (n "csv") (r "^0.14") (d #t) (k 0)) (d (n "rulinalg") (r "^0.4.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "092g5wznxqfwdywm3md1hm5fwl840kqb9i9k8874yiv3xwwam6ng")))

(define-public crate-linearkalman-0.1.2 (c (n "linearkalman") (v "0.1.2") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.1") (d #t) (k 0)))) (h "0smb3qrvldxjhk0vq04h2lvck81mvbxym51mxb1ivd9lgsvlnrmg")))

(define-public crate-linearkalman-0.1.3 (c (n "linearkalman") (v "0.1.3") (d (list (d (n "csv") (r "^0.14") (d #t) (k 2)) (d (n "rulinalg") (r "^0.4.1") (d #t) (k 0)))) (h "1i83xzjs2a759zg5zmxn0dgnzax36sz0xk7y4yfbn1pc7yrj12yw")))

