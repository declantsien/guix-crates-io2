(define-module (crates-io li ne line_drawing) #:use-module (crates-io))

(define-public crate-line_drawing-0.1.0 (c (n "line_drawing") (v "0.1.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)))) (h "01ia8laynxq93wqrlkw1v27p3lf6xnnz4mjja7q6dg2fg42l8cqr")))

(define-public crate-line_drawing-0.1.1 (c (n "line_drawing") (v "0.1.1") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 2)))) (h "192b748ibbn29az04gc1h7jrszbjr1vax0mma2k7g8asrmaj4d02")))

(define-public crate-line_drawing-0.1.2 (c (n "line_drawing") (v "0.1.2") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 2)))) (h "15il4g7vgxjvrchaw5b8gdihrmzdbdh9yrn4zpv8namfk1xkwabq")))

(define-public crate-line_drawing-0.2.0 (c (n "line_drawing") (v "0.2.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 2)))) (h "1a33v2glkyf1ml0v3kyrf5iy1fb5vlxqs0lhxggwb04nsb9gk07w")))

(define-public crate-line_drawing-0.3.0 (c (n "line_drawing") (v "0.3.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "0knl9bd2ijg0a8z8v24n25p9a3bp7a957nm6mkanl4zgxgp5am92")))

(define-public crate-line_drawing-0.4.0 (c (n "line_drawing") (v "0.4.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 0)) (d (n "image") (r "^0.15.0") (d #t) (k 2)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "087z5f63cybaz697cwqdsn7fdwrhw2phmv1m70h8d93l7afaz45d")))

(define-public crate-line_drawing-0.5.0 (c (n "line_drawing") (v "0.5.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "image") (r "^0.15.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "04168x7glfds23nkk4isbl2vx0v1kviclphsgw2k7ah71f7xfcxb")))

(define-public crate-line_drawing-0.5.1 (c (n "line_drawing") (v "0.5.1") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "image") (r "^0.15.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "1mjd5fv5pkdlqyrp5zpymiy3nk7bxajcnva6bsiq4z39qf58haf3")))

(define-public crate-line_drawing-0.5.2 (c (n "line_drawing") (v "0.5.2") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "image") (r "^0.15.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.3.16") (d #t) (k 2)))) (h "1x5f28f6mzay5y9136sb4rjbwq264h4d5xzn8pgbimwylk8acgy9")))

(define-public crate-line_drawing-0.5.3 (c (n "line_drawing") (v "0.5.3") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "image") (r "^0.17.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "0fvd5i2zsmpqrghlmzxch87g5a5bnjnd09lzv6z86xzr19iqig9v")))

(define-public crate-line_drawing-0.6.0 (c (n "line_drawing") (v "0.6.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "image") (r "^0.17.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.3.17") (d #t) (k 2)))) (h "1s0rr7f7k762z02c9z2gzpnabhffig0nvcyiy54ck5frl8fbr0fm")))

(define-public crate-line_drawing-0.7.0 (c (n "line_drawing") (v "0.7.0") (d (list (d (n "bresenham") (r "^0") (d #t) (k 2)) (d (n "image") (r "^0") (d #t) (k 2)) (d (n "num-traits") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 2)))) (h "1fcna7hq1g1kkkqy07hydscx5d2zgb6gskz3vnsvsif8h8ysvisw")))

(define-public crate-line_drawing-0.8.0 (c (n "line_drawing") (v "0.8.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "image") (r "^0.17.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.1.40") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "1chawny039jkj0lj6abkbykfbhk5wwilshn60fqh4c288bjh46gq")))

(define-public crate-line_drawing-0.8.1 (c (n "line_drawing") (v "0.8.1") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (f (quote ("png"))) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "18a940s3mv8w5prpb99sdxykzhrvzrnymw3hvd7wisnkgbr11jqm")))

(define-public crate-line_drawing-1.0.0 (c (n "line_drawing") (v "1.0.0") (d (list (d (n "bresenham") (r "^0.1.1") (d #t) (k 2)) (d (n "image") (r "^0.23.14") (f (quote ("png"))) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "078xnqy1i0gb3vdc7idm6i00kscym685x6a9h5n3x2h02fiph51x")))

