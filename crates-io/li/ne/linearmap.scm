(define-module (crates-io li ne linearmap) #:use-module (crates-io))

(define-public crate-linearmap-0.0.1 (c (n "linearmap") (v "0.0.1") (h "0nj9zsdpcfwssmwh4kkq3z81p8y8aa87avp9bg22kp0c1v5xskcj")))

(define-public crate-linearmap-0.0.2 (c (n "linearmap") (v "0.0.2") (h "0cri8bqpz5zg6n6vqa59m6c70dj0hy138szyw721j2d3lq08ir4m")))

(define-public crate-linearmap-0.0.3 (c (n "linearmap") (v "0.0.3") (h "1z5lzpmhnbcpbvcpp40xvh1amwr37622whip5mx9xd5ccw9hahki")))

