(define-module (crates-io li ne linera-wit-bindgen-gen-guest-rust) #:use-module (crates-io))

(define-public crate-linera-wit-bindgen-gen-guest-rust-0.2.0 (c (n "linera-wit-bindgen-gen-guest-rust") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.2.0") (d #t) (k 0) (p "linera-wit-bindgen-core")) (d (n "wit-bindgen-gen-rust-lib") (r "^0.2.0") (d #t) (k 0) (p "linera-wit-bindgen-gen-rust-lib")))) (h "1i7b8g2pz0rdkbgj6zbz917ip1xhabxix20qxisxcrmmjl4bwz7c")))

