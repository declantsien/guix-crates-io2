(define-module (crates-io li ne linera-wit-bindgen-gen-rust-lib) #:use-module (crates-io))

(define-public crate-linera-wit-bindgen-gen-rust-lib-0.2.0 (c (n "linera-wit-bindgen-gen-rust-lib") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.2") (d #t) (k 0) (p "linera-wit-bindgen-core")))) (h "0rqp37xaxs002xanq1s50b7jzpb3jrxzj7rmsvqhjp932n0h6bkj")))

