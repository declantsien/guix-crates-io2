(define-module (crates-io li ne linewrapper) #:use-module (crates-io))

(define-public crate-linewrapper-0.1.1 (c (n "linewrapper") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.7") (d #t) (t "cfg(target_os = \"android\")") (k 0)) (d (n "textwrap") (r "^0.14.2") (d #t) (t "cfg(target_os = \"android\")") (k 0)))) (h "1sd6i63z2h8kcjpmw4ygqw8kci5zb5h3zhk36rbzndlpr9y5jvdj")))

