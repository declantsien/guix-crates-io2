(define-module (crates-io li ne line-numbers) #:use-module (crates-io))

(define-public crate-line-numbers-0.1.0 (c (n "line-numbers") (v "0.1.0") (h "0qrqwcz1pqxxrxx1i84wwp753zdhcr6l9w483xj4c50zvwww3m03") (r "1.58.0")))

(define-public crate-line-numbers-0.2.0 (c (n "line-numbers") (v "0.2.0") (h "0j3dg6qdnkijn23i3aaa3wy6mh74x8xq6b5zzm5fq9qdbllxbgwc") (r "1.58.0")))

(define-public crate-line-numbers-0.2.1 (c (n "line-numbers") (v "0.2.1") (h "0wl6akd5pq4q8m8r2ljp84f7zg47a9663h6yb5nka3j13b8h40l9") (r "1.58.0")))

(define-public crate-line-numbers-0.2.2 (c (n "line-numbers") (v "0.2.2") (h "101pnnswgwxy9a1wnhvm1s2yngnnhpmbs9c82ncqcfxnbqqpafkr") (r "1.58.0")))

(define-public crate-line-numbers-0.3.0 (c (n "line-numbers") (v "0.3.0") (h "0p531c7w7viilf7q1k277zmy8ihl8064vx0r7jm5z02ki03ga9bv") (r "1.58.0")))

