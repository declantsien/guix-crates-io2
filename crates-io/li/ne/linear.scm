(define-module (crates-io li ne linear) #:use-module (crates-io))

(define-public crate-linear-0.0.1 (c (n "linear") (v "0.0.1") (h "0r4cl92am1g7r0wy00p67dhkvny9j0vy2347bdjf3x44zpjmzwig")))

(define-public crate-linear-0.1.0 (c (n "linear") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "*") (d #t) (k 0)) (d (n "lapack") (r "*") (d #t) (k 0)))) (h "1y62nmjhc18azalhynifyk9r3mj05z34ra0hq6y56fbkk590v5i2")))

(define-public crate-linear-0.1.1 (c (n "linear") (v "0.1.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "blas") (r "*") (d #t) (k 0)) (d (n "lapack") (r "*") (d #t) (k 0)))) (h "184bnb3fy7h3m2d4s3ml7zq8w3js8h1vjvcz0iwd275ddxs65k19")))

(define-public crate-linear-0.2.0 (c (n "linear") (v "0.2.0") (h "0cly43sxdiqn70599fhlvz99hkx5kinhcygypr2d8s4z2i67lshv")))

