(define-module (crates-io li ne line) #:use-module (crates-io))

(define-public crate-line-0.1.0 (c (n "line") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1dhyszib52fz258wdq192j5lh5x70zam19sd0w6lbvydpysk85z5")))

(define-public crate-line-0.1.1 (c (n "line") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0hjax9hv7rancgrkh12k48lyha4l2c9n5qalg1mjf0v2hnij5pgc")))

(define-public crate-line-0.1.2 (c (n "line") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0595ij2wdn1fv0ncbf4sj1bqsr2xhsmwdrvxikjbvhzwdsg3dsmz")))

(define-public crate-line-0.1.3 (c (n "line") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1f0mm22yfs5vhf3fyzzlaqns791n7bbjmr2xiz0i6rh9sr09hfxd")))

(define-public crate-line-0.1.4 (c (n "line") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0x6jpnw5989kdipswvg05wscsr463k581lkj04iwrdx6sbc8pr20")))

(define-public crate-line-0.1.6 (c (n "line") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0bkln2dh82h66khicia7dj5l7wxa0z69z50709sf2dhdidzfy4x9")))

(define-public crate-line-0.1.7 (c (n "line") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0qc9idfs42zqhmn1xvr069hmzhq5amnal8vmzn81j0ic5333dnr8")))

(define-public crate-line-0.1.8 (c (n "line") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1w2r3d2bh4r25axji774yrq9fqci6bfxv3d8dw9q6vq72hbaw0ss")))

(define-public crate-line-0.1.9 (c (n "line") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1a4x89g2k97whiz9lymzkxz11lgm8g095yyrsp5y4720nnzch8km")))

(define-public crate-line-0.1.10 (c (n "line") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0ajss381bl39czn7gc7kzv8na8g9x635w0ikcv0yknm3i61zx1bl")))

(define-public crate-line-0.1.11 (c (n "line") (v "0.1.11") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1cfldr9srnakgwcrqjs3jhwsgpj4fdhypa9lay6m8dqn34pcnm44")))

(define-public crate-line-0.1.12 (c (n "line") (v "0.1.12") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "1fwmjfsysk50icbqlsn6cjwvf506dgxsqawycc5gw5q04a056kh3")))

(define-public crate-line-0.1.13 (c (n "line") (v "0.1.13") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "084q4qk5dqh3fpzc2q505nacw38jxzjh20bph9yl7rz09gn2ig0n")))

(define-public crate-line-0.1.14 (c (n "line") (v "0.1.14") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0q7l3s9815wxy22w13aym5fnjnfgkq74v6ilk702zzn6605l3nm3")))

(define-public crate-line-0.1.15 (c (n "line") (v "0.1.15") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "utf8parse") (r "^0.1") (d #t) (t "cfg(unix)") (k 0)))) (h "0pissvrl5398701zlfd22w51ca32vhw83vbsl58a330hr4w5ra04")))

