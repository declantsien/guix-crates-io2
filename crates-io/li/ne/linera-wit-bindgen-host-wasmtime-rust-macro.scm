(define-module (crates-io li ne linera-wit-bindgen-host-wasmtime-rust-macro) #:use-module (crates-io))

(define-public crate-linera-wit-bindgen-host-wasmtime-rust-macro-0.2.0 (c (n "linera-wit-bindgen-host-wasmtime-rust-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.2") (d #t) (k 0) (p "linera-wit-bindgen-core")) (d (n "wit-bindgen-gen-host-wasmtime-rust") (r "^0.2") (d #t) (k 0) (p "linera-wit-bindgen-gen-host-wasmtime-rust")))) (h "1kvjzbr4mjpk3ys8bvjzjhhsyzs7xp2zyzs85ghag5diyzklh5nm") (f (quote (("tracing") ("async"))))))

