(define-module (crates-io li ne line-login-api) #:use-module (crates-io))

(define-public crate-line-login-api-0.1.1 (c (n "line-login-api") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "~1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "~1") (d #t) (k 0)) (d (n "tokio") (r "~1") (f (quote ("full"))) (d #t) (k 2)))) (h "1nzm8n85bvcpj70w7x1vhajbzlnxndw57xl7wpkm9x2y0pwyg62b")))

