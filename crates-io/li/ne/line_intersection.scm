(define-module (crates-io li ne line_intersection) #:use-module (crates-io))

(define-public crate-line_intersection-0.3.0 (c (n "line_intersection") (v "0.3.0") (d (list (d (n "geo") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1q31sagppcrwcfzfqm8kk1kmq2kh1jq5ckphj66ckqz7la1n5957")))

(define-public crate-line_intersection-0.4.0 (c (n "line_intersection") (v "0.4.0") (d (list (d (n "geo") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0b08c7dmp3hnzs77yycsigf12ahmsd6vfnnhppl6hmr124lig00w")))

