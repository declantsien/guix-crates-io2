(define-module (crates-io li ne linear-map) #:use-module (crates-io))

(define-public crate-linear-map-0.0.2 (c (n "linear-map") (v "0.0.2") (h "046vig74vmnd1hjks1a34z021qlyw5v2xgpk09dz7yh8v5pgjv0d") (f (quote (("nightly"))))))

(define-public crate-linear-map-0.0.3 (c (n "linear-map") (v "0.0.3") (h "0q8d219fhl2rjsziwd86q7dhhpmdlhnm6s859p12j5q5xv4fhj86") (f (quote (("nightly"))))))

(define-public crate-linear-map-0.0.4 (c (n "linear-map") (v "0.0.4") (h "15gwyhk2z1i4f4yhvg8d2nsjbajs173vbnhagbfy1ww65499fzav") (f (quote (("nightly"))))))

(define-public crate-linear-map-1.0.0 (c (n "linear-map") (v "1.0.0") (h "0qnv82si7q2bl1iw1dsrk5n9h4nkg20i2svcjfnm0g5m8sism8f7") (f (quote (("nightly"))))))

(define-public crate-linear-map-1.1.0 (c (n "linear-map") (v "1.1.0") (d (list (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.7") (o #t) (d #t) (k 0)))) (h "1kv4rh4lah44gf9bs0i01av9g7rnlizbqc5489z06n591hm7v54g") (f (quote (("serde_impl" "serde" "serde_test") ("nightly"))))))

(define-public crate-linear-map-1.2.0 (c (n "linear-map") (v "1.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1vh3sczl4xb5asdlpafdf3y4g9bp63fgs8y2a2sjgmcsn7v21bmz") (f (quote (("serde_impl" "serde" "serde_test") ("nightly"))))))

