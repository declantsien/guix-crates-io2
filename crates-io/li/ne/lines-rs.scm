(define-module (crates-io li ne lines-rs) #:use-module (crates-io))

(define-public crate-lines-rs-0.1.0 (c (n "lines-rs") (v "0.1.0") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1rdazxxg50c7bzbaylfm73n5kyrzhqzv72li826qjsnrk44v99gf")))

(define-public crate-lines-rs-0.1.1 (c (n "lines-rs") (v "0.1.1") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0hkqn06miyapk5afwhbjmh32wcz827lbc24lyrr3v7zr227dg3f0")))

(define-public crate-lines-rs-0.1.2 (c (n "lines-rs") (v "0.1.2") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0giwnrckpy8llrg8fy7ryq8br6f213scx35fcxqmkg1zgyj48awj")))

(define-public crate-lines-rs-0.1.3 (c (n "lines-rs") (v "0.1.3") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0smkz0sgbk5vxyydjiq265ww09ib5nh2vdqxvkl63qnpxbi8rlgd")))

(define-public crate-lines-rs-0.1.4 (c (n "lines-rs") (v "0.1.4") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "0rqnskc5x92ml3mjqk4chfaqwa25az22s8qj8hgflgf01pb6cslr")))

(define-public crate-lines-rs-0.1.5 (c (n "lines-rs") (v "0.1.5") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1wd4jnyfx0ypamh0qh1xz3ssw2r90yaramgg5bvkbybaqiryckav")))

(define-public crate-lines-rs-0.1.6 (c (n "lines-rs") (v "0.1.6") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "04r0sym20b9jkp816wa7jpihsasm40gm1hq17swlkiwf10wrmns9")))

(define-public crate-lines-rs-0.1.7 (c (n "lines-rs") (v "0.1.7") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "17blcin2fb0q9vx06i5wmydrp1hj6n5gp0w9h9zy80rrpmvbdwkg")))

(define-public crate-lines-rs-0.1.8 (c (n "lines-rs") (v "0.1.8") (d (list (d (n "bytecount") (r "^0.6.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "rayon") (r "^1.5.0") (d #t) (k 0)))) (h "1mdjxw10hx1qyj3wqza1v31l2hizfdwabyfpirn68kmmjg09djs1")))

