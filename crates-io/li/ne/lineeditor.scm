(define-module (crates-io li ne lineeditor) #:use-module (crates-io))

(define-public crate-lineeditor-0.1.0 (c (n "lineeditor") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "0nldpxx1kzgzvzrsj29l09y83k8r001iclj3416rb9gi951ch4z3")))

(define-public crate-lineeditor-0.2.0 (c (n "lineeditor") (v "0.2.0") (d (list (d (n "clipboard") (r "^0.5.0") (d #t) (k 0)) (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)))) (h "1mbxcq51906y7w39imr4fsv2ipbw3l1yzxh38w9x2m03dpw59r4w")))

