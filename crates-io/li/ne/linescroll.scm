(define-module (crates-io li ne linescroll) #:use-module (crates-io))

(define-public crate-linescroll-0.1.0 (c (n "linescroll") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.25") (d #t) (k 0)))) (h "1swrkqdj380i0900a2ypham572maqavx23bz6plyjyfihzay255s")))

(define-public crate-linescroll-0.1.1 (c (n "linescroll") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "027hqdnmcy52bx8y5dgn4dmhwn7lvliamsdbg99gg58365jb12lm")))

(define-public crate-linescroll-0.1.2 (c (n "linescroll") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "0s95qp718xf8qgf46wm9cinv2kx1sj0r94ahnjl8rsqmwv02l82i")))

(define-public crate-linescroll-0.2.0 (c (n "linescroll") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "17zl9iysqjjyn02q4sncz2gzkza4nss64ap1762isdpfzfqbkz38")))

