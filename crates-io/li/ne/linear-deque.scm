(define-module (crates-io li ne linear-deque) #:use-module (crates-io))

(define-public crate-linear-deque-0.1.0 (c (n "linear-deque") (v "0.1.0") (h "0c3rl9qyx1nabn5sibsif4xjv42pb57hr9nhli25ymg90qpc997k")))

(define-public crate-linear-deque-0.1.1 (c (n "linear-deque") (v "0.1.1") (h "0i05c7v7y3253n5q3k98a0j1bk53pfmynr587pfg8538bgiscxpb")))

(define-public crate-linear-deque-0.1.2 (c (n "linear-deque") (v "0.1.2") (h "0pxrh43n8mkajij6z2f88ff2fs32r1nysdbhwhxrgpwk8j72zxgv")))

