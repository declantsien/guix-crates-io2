(define-module (crates-io li ne linera-wit-bindgen-host-wasmer-rust-macro) #:use-module (crates-io))

(define-public crate-linera-wit-bindgen-host-wasmer-rust-macro-0.2.0 (c (n "linera-wit-bindgen-host-wasmer-rust-macro") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "wit-bindgen-core") (r "^0.2") (d #t) (k 0) (p "linera-wit-bindgen-core")) (d (n "wit-bindgen-gen-host-wasmer-rust") (r "^0.2") (d #t) (k 0) (p "linera-wit-bindgen-gen-host-wasmer-rust")))) (h "1rhvpa3rv88x2ldl5m31syfj8dy1n5i40ca46fd01nq5p6yj0aii") (f (quote (("tracing") ("async"))))))

