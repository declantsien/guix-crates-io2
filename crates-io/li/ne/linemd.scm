(define-module (crates-io li ne linemd) #:use-module (crates-io))

(define-public crate-linemd-0.1.0 (c (n "linemd") (v "0.1.0") (h "0m8gf9n0zw3r18nbaxmzaka6aqycr30l1b39fd3ymbk6akg62kjn")))

(define-public crate-linemd-0.1.1 (c (n "linemd") (v "0.1.1") (h "1bpg6nramqhxnb8vyinp2r0pyadkpgcaiyfhs2v0siv1rsa56460")))

(define-public crate-linemd-0.2.0 (c (n "linemd") (v "0.2.0") (h "1xcm84pvyxvxr21siga24hvnmsd1lymag9b24icfr0p8hsj1zg7r") (f (quote (("svg") ("html") ("default" "html"))))))

(define-public crate-linemd-0.2.1 (c (n "linemd") (v "0.2.1") (h "0lwv7xv35ch9140rqs6rs9nr4vhcqa4k7kr1wmnkivv3ysdg4fkh") (f (quote (("svg") ("html") ("default" "html"))))))

(define-public crate-linemd-0.3.0 (c (n "linemd") (v "0.3.0") (h "1xav9ps7mq1nv5bvlx3mxrqxfb41dshnkr7spvbqy0lp7hrbz8vp") (f (quote (("svg") ("html") ("default" "html"))))))

(define-public crate-linemd-0.4.0 (c (n "linemd") (v "0.4.0") (h "03m3f3k8dc077qlvqw00v9cvhg8ai0k93gzwzc2l8vf3cwjncj03") (f (quote (("svg") ("html") ("default" "html"))))))

