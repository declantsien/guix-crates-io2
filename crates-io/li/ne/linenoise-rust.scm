(define-module (crates-io li ne linenoise-rust) #:use-module (crates-io))

(define-public crate-linenoise-rust-0.0.2 (c (n "linenoise-rust") (v "0.0.2") (d (list (d (n "gcc") (r "~0.1.0") (d #t) (k 1)))) (h "0kckaxid97jjldy4irp8f2adh5p3kr81gp819m92pz5li98i86zk")))

(define-public crate-linenoise-rust-0.0.3 (c (n "linenoise-rust") (v "0.0.3") (d (list (d (n "gcc") (r "~0.1.0") (d #t) (k 1)))) (h "1ivfvc865017bl4vc8sr13yh1bxqqgq1cmfji2kyw75bz95az92s")))

(define-public crate-linenoise-rust-0.1.2 (c (n "linenoise-rust") (v "0.1.2") (d (list (d (n "gcc") (r "~0.1.0") (d #t) (k 1)))) (h "072zlicf1hzdjn799gq6fwgqmxr9f4cw7a8cdc6zw0i2cdk3fd8i")))

(define-public crate-linenoise-rust-0.1.3 (c (n "linenoise-rust") (v "0.1.3") (d (list (d (n "gcc") (r "~0.1.0") (d #t) (k 1)))) (h "1iszri98i447h51fwx7y794lwlfy6fi7nyxphlwxmki7c6nbnxnh")))

(define-public crate-linenoise-rust-0.1.4 (c (n "linenoise-rust") (v "0.1.4") (d (list (d (n "gcc") (r "~0.1.0") (d #t) (k 1)))) (h "1qnzfv2fy2nwssg2372y4553hr3zj1z49qa6n6yyv5iys0d8m53p")))

(define-public crate-linenoise-rust-0.1.5 (c (n "linenoise-rust") (v "0.1.5") (d (list (d (n "gcc") (r "~0.1.0") (d #t) (k 1)))) (h "1f9ap9rwj29lisvqi9n6yd49mbyqy57zjv52dyk5429j6x4cv78m")))

(define-public crate-linenoise-rust-0.1.6 (c (n "linenoise-rust") (v "0.1.6") (d (list (d (n "gcc") (r "~0.1.0") (d #t) (k 1)))) (h "1aicr1c99dpbfwlsfhm91jvskzv9jm9ypzamcqxj4yj60g35h7vr")))

(define-public crate-linenoise-rust-0.1.7 (c (n "linenoise-rust") (v "0.1.7") (d (list (d (n "gcc") (r "~0.1.0") (d #t) (k 1)))) (h "0v23fvcb9pl3b22378cl1nd4bdzamd6f82xy24p17684hzrsgg9r")))

(define-public crate-linenoise-rust-0.1.8 (c (n "linenoise-rust") (v "0.1.8") (d (list (d (n "gcc") (r "~0.1.0") (d #t) (k 1)))) (h "00klva4a57ylgi1zq0zjlxl5iamqln9837qh0k35k72sdgr4bvqv")))

(define-public crate-linenoise-rust-0.1.9 (c (n "linenoise-rust") (v "0.1.9") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1f3pm7hkq417vwjmkj806q27mmlnzbm3bn7hd8pvr7x5cj28hdqf")))

(define-public crate-linenoise-rust-0.1.10 (c (n "linenoise-rust") (v "0.1.10") (d (list (d (n "gcc") (r "*") (d #t) (k 1)))) (h "1gscwz9wf3f6kbsj73rqby3nh3h1mwfd7m0pj6ilvkl33r71j81f")))

(define-public crate-linenoise-rust-0.1.11 (c (n "linenoise-rust") (v "0.1.11") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1xfmsnn9pkpr728hcg6wb06lsccswkcx43sf0chxglv8yygl9bi8")))

(define-public crate-linenoise-rust-0.1.12 (c (n "linenoise-rust") (v "0.1.12") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0fvb1iwp19y5l296l1f5jvpr68kr6pwla6i2kz5gdyxm55adj75j")))

(define-public crate-linenoise-rust-0.1.13 (c (n "linenoise-rust") (v "0.1.13") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0x45qlvjc969p7a730i3ig79wjl0yhyx0h08a3w259nldbhydnlq")))

(define-public crate-linenoise-rust-0.1.14 (c (n "linenoise-rust") (v "0.1.14") (d (list (d (n "gcc") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1f32vcvj3i0iyizj3cham84bdc8hpsdw2nk4zrcdhlxhajj837cv")))

(define-public crate-linenoise-rust-0.1.15 (c (n "linenoise-rust") (v "0.1.15") (d (list (d (n "gcc") (r "~0.3.22") (d #t) (k 1)) (d (n "libc") (r "~0.2.7") (d #t) (k 0)))) (h "15vyg5islhp3z9jhx7wwzyr1zrqbcfvad4ghd56rj3wq28spkc51")))

(define-public crate-linenoise-rust-0.2.0 (c (n "linenoise-rust") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.24") (d #t) (k 1)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "1cmpfwly9zqy8lqs4q711dyzh6m7swv70ggyagqyxgm97c8lw7g3")))

(define-public crate-linenoise-rust-0.2.1 (c (n "linenoise-rust") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3.24") (d #t) (k 1)) (d (n "libc") (r "^0.2.7") (d #t) (k 0)))) (h "0lp1rblbmbglkzjx01cs9x4q96zxdfbaiqg7nxry7nm4c1d47hkn")))

