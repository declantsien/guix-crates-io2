(define-module (crates-io li ne linefreq) #:use-module (crates-io))

(define-public crate-linefreq-0.1.0 (c (n "linefreq") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i4d5smkn4glpya174w95a05bamxhfxfpjc187sdb0j1xyr70d90")))

(define-public crate-linefreq-0.2.0 (c (n "linefreq") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zw91h4w5j612v8r1n7iwy2iql0mda1v2mblkkjh1nhfb9awgr45")))

