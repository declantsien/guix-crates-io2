(define-module (crates-io li ne lines-tally) #:use-module (crates-io))

(define-public crate-lines-tally-0.1.0 (c (n "lines-tally") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0h8195laag62hs24zv23qjv2m0f20v8zbhqsvj9z25dfx3hxs7bh")))

(define-public crate-lines-tally-0.1.1 (c (n "lines-tally") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.11.0") (d #t) (k 0)) (d (n "ignore") (r "^0.4.20") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "13893vs7v1bk3vyw948yafwm52i18s5i1w9jr04pg5z0j6npw95y")))

