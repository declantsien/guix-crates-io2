(define-module (crates-io li ne linera-wit-bindgen-gen-host-wasmer-rust) #:use-module (crates-io))

(define-public crate-linera-wit-bindgen-gen-host-wasmer-rust-0.2.0 (c (n "linera-wit-bindgen-gen-host-wasmer-rust") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (k 0)) (d (n "wasmer") (r "^3.0.0-rc.2") (d #t) (k 2)) (d (n "wasmer-wasi") (r "^3.0.0-rc.2") (d #t) (k 2)) (d (n "wit-bindgen-core") (r "^0.2.0") (d #t) (k 0) (p "linera-wit-bindgen-core")) (d (n "wit-bindgen-gen-rust-lib") (r "^0.2.0") (d #t) (k 0) (p "linera-wit-bindgen-gen-rust-lib")))) (h "09q9nyqdax7xkqzy8qlcsp6c9m6z039bsg8ci8x3i419mypk3mcm")))

