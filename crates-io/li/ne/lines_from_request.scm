(define-module (crates-io li ne lines_from_request) #:use-module (crates-io))

(define-public crate-lines_from_request-0.1.0 (c (n "lines_from_request") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)))) (h "0fc62q2drpvrc5vzfdxh70g942l845jlv37ag489li1svcgf17r0")))

(define-public crate-lines_from_request-0.2.0 (c (n "lines_from_request") (v "0.2.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)))) (h "1ic140vgq5plqsjkislhgkr6yyxxhj4b37vhh5drj5f5djkazlab")))

(define-public crate-lines_from_request-0.3.0 (c (n "lines_from_request") (v "0.3.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13") (d #t) (k 0)))) (h "0qqgpl45qkxinlaa5ap8jd2qnw6y255vx2rr4fq5z988i25pr0g4")))

(define-public crate-lines_from_request-0.3.1 (c (n "lines_from_request") (v "0.3.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("stream"))) (d #t) (k 0)))) (h "1r8vxhfxqz0rdyzqj5kmpbdzbzkgb691c1xz7rkimmnv2q9gr3cc")))

