(define-module (crates-io li ne lineq) #:use-module (crates-io))

(define-public crate-lineq-0.1.0 (c (n "lineq") (v "0.1.0") (h "0ffn85azcsywzxrhiinaj49jl6n8hmd2iha70vmsm8pk81kxf85w")))

(define-public crate-lineq-0.1.1 (c (n "lineq") (v "0.1.1") (h "0yy25y4rsa1iydqm7dis8mgibrvs5imx4spvn0m0nrbgqv17ymdc") (y #t)))

(define-public crate-lineq-0.1.2 (c (n "lineq") (v "0.1.2") (h "1jrg0xg3vk5pcgcdrmg9pjp8y6glwfj1na7bhb8zd0sgdv517ch6")))

