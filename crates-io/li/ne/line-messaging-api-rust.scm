(define-module (crates-io li ne line-messaging-api-rust) #:use-module (crates-io))

(define-public crate-line-messaging-api-rust-0.1.0 (c (n "line-messaging-api-rust") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "0xb1q8n2fi8nrsg0axdgkwq433vlmspbm92n2ifn2w4jnch9b5bf")))

(define-public crate-line-messaging-api-rust-0.1.1 (c (n "line-messaging-api-rust") (v "0.1.1") (d (list (d (n "base64") (r "^0.9.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.8.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)))) (h "068wc04v6bz0d7xml3mmlhjsbznnk5ainfznm29ybibfxcqnhics")))

