(define-module (crates-io li ne linear_collections) #:use-module (crates-io))

(define-public crate-linear_collections-0.1.0 (c (n "linear_collections") (v "0.1.0") (d (list (d (n "serde") (r "1.*") (f (quote ("std"))) (o #t) (k 0)))) (h "1x5iqpr7mv3fh13qf3pypwcl58j2wg6mlc7im5frimfczksdg6p5") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-linear_collections-0.2.0 (c (n "linear_collections") (v "0.2.0") (d (list (d (n "serde") (r "1.*") (f (quote ("std"))) (o #t) (k 0)))) (h "0addlgi8ginqdcpvl6mrjqg8p1j3x7bi263xp696rnxnm7r36c9x") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-linear_collections-0.2.1 (c (n "linear_collections") (v "0.2.1") (d (list (d (n "serde") (r "1.*") (f (quote ("std"))) (o #t) (k 0)))) (h "0ki0kzzhvg4knmbhr2rzfhm02nk61qx5ma8f5rd8iybk54zsy08d") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-linear_collections-0.2.2 (c (n "linear_collections") (v "0.2.2") (d (list (d (n "serde") (r "1.*") (f (quote ("std"))) (o #t) (k 0)))) (h "12c0mjlvss420znrm2p9klxm6pl9h0cisq820gs3jnhal25vzwf4") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-linear_collections-0.3.0 (c (n "linear_collections") (v "0.3.0") (d (list (d (n "linear_collections_macros") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "1.*") (f (quote ("std"))) (o #t) (k 0)) (d (n "serde_test") (r "^1.0.176") (o #t) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.96") (d #t) (k 2)))) (h "00kmbf5md29fsfzzik41wnzsa8s17dml8m66fwvwg4v8jhbki0g2") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde" "dep:serde_test") ("macros" "dep:linear_collections_macros"))))))

