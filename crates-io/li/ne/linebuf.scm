(define-module (crates-io li ne linebuf) #:use-module (crates-io))

(define-public crate-linebuf-0.0.1 (c (n "linebuf") (v "0.0.1") (d (list (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "117vadjzycp0kq5icxlgrrbqka8fm93rfwhkkakxz2lfqzw6kw0j")))

(define-public crate-linebuf-0.0.2 (c (n "linebuf") (v "0.0.2") (d (list (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "1rsaibwxg9zj2zhk3j7kyy81sgzc1ywl33gcpkqylda57j642ib0")))

