(define-module (crates-io li ne lineup_rust) #:use-module (crates-io))

(define-public crate-lineup_rust-0.1.0 (c (n "lineup_rust") (v "0.1.0") (h "10797cv6m3mjhr9ch5b0qx6hsbmqpa0kpfjmfj1x8jv2fghqgcaj")))

(define-public crate-lineup_rust-0.1.5 (c (n "lineup_rust") (v "0.1.5") (h "15issi77p65b7ffj22wx7z28gwggqfi3dm4vml7v275fais48n65")))

