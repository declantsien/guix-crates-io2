(define-module (crates-io li bu libunwind-rs) #:use-module (crates-io))

(define-public crate-libunwind-rs-0.1.0 (c (n "libunwind-rs") (v "0.1.0") (d (list (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libunwind-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0mynawr5xv3blcg346a6l8gcnl83xj75q9jgsvarw2x9ajps4hm4")))

(define-public crate-libunwind-rs-0.1.1 (c (n "libunwind-rs") (v "0.1.1") (d (list (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libunwind-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0fibqgs7xdd9vl7ygbqkpf44djmz94z0yjpqhkz6v2cv9r6qqfci")))

(define-public crate-libunwind-rs-0.1.2 (c (n "libunwind-rs") (v "0.1.2") (d (list (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libunwind-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0nkrs5dyx81cqcwxyhlgwy7ny4pkpdpv1yf9961861jwr8njb2xm")))

(define-public crate-libunwind-rs-0.2.0 (c (n "libunwind-rs") (v "0.2.0") (d (list (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libunwind-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1131s9rvn1kg0ccg38k376584jwsc0f34635wf7nkpcs3kpx5f7a") (f (quote (("ptrace" "libunwind-sys/ptrace"))))))

(define-public crate-libunwind-rs-0.3.0 (c (n "libunwind-rs") (v "0.3.0") (d (list (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libunwind-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "19z6gjbfi028gj385g9df2xhsa8192lv1vn0s6g1lnnmjqi22209") (f (quote (("ptrace" "libunwind-sys/ptrace"))))))

(define-public crate-libunwind-rs-0.3.1 (c (n "libunwind-rs") (v "0.3.1") (d (list (d (n "foreign-types") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libunwind-sys") (r "^0.5.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1snd5d3l6j18dgkj0gk3kx6xm21s7ph8r7cn8zivgrrg530g5j40") (f (quote (("ptrace" "libunwind-sys/ptrace"))))))

