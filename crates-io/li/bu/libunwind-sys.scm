(define-module (crates-io li bu libunwind-sys) #:use-module (crates-io))

(define-public crate-libunwind-sys-0.1.0 (c (n "libunwind-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0b0h8igdj0mpzrvdj2jnsy47zz240f0sh6kcw0xc1ki28rdf4pxx") (y #t)))

(define-public crate-libunwind-sys-0.1.1 (c (n "libunwind-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0swhj1r7ci09fljs4n1v5xx1wkdhf9r8jyv7xz9ir9c5ibrng16l") (y #t)))

(define-public crate-libunwind-sys-0.1.2 (c (n "libunwind-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "1wwv6cxl3kp6db7c313c12xh0zf4xhxdaa7d7hjbncx09k5wih9r")))

(define-public crate-libunwind-sys-0.1.3 (c (n "libunwind-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "1v477plyj6y5p9d71dqfmm4vzprqkbgqamqhv2k78v5i42hpqj2d")))

(define-public crate-libunwind-sys-0.2.0 (c (n "libunwind-sys") (v "0.2.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0f5vkpbwrkvzdhv9gv89mw88g724ph5zva6m11ddynbjp2rjvrbd")))

(define-public crate-libunwind-sys-0.2.1 (c (n "libunwind-sys") (v "0.2.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "16apvdw58lirsiqrsni2w8wxifm7zbbpn9r40mdb4pklvddl70jg")))

(define-public crate-libunwind-sys-0.3.0 (c (n "libunwind-sys") (v "0.3.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "19l9bpnah47gxrmljpjh14dr09r3l4n3jk9yqcb7p8hh2kgjv79f")))

(define-public crate-libunwind-sys-0.4.0 (c (n "libunwind-sys") (v "0.4.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "03i431wrgwgfh9yfh71rdn9nrqa3jdwpnzspp07zfwcs1glr75a3") (f (quote (("ptrace"))))))

(define-public crate-libunwind-sys-0.5.0 (c (n "libunwind-sys") (v "0.5.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0cl7s2yhv68wl3206wvv13xqmhqkgmdcms0jyr8701lc7lbicj40") (f (quote (("ptrace"))))))

(define-public crate-libunwind-sys-0.5.1 (c (n "libunwind-sys") (v "0.5.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0ccfhkdbkqy13rcrk6bd02i71zblmxpxkjwr4g14ylgqmr6xz39q") (f (quote (("ptrace"))))))

