(define-module (crates-io li bu libudis86-sys) #:use-module (crates-io))

(define-public crate-libudis86-sys-0.1.0 (c (n "libudis86-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.10") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "0ri9ir82035nj14cwil05rjhyspvm5scnkaim015m7pd8zk7ivsy")))

(define-public crate-libudis86-sys-0.2.0 (c (n "libudis86-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.10") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "1ljfjr17vgzwhibwyv4xljnr2ap0ivxzdxi4vav82zzjlcayizkm")))

(define-public crate-libudis86-sys-0.2.1 (c (n "libudis86-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.58") (d #t) (k 0)))) (h "1c5612mp4p8j3301awzf7i1wsmy9v4ijkpb4mk0r1z0vvffvz6qk")))

