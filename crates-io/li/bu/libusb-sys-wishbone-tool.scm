(define-module (crates-io li bu libusb-sys-wishbone-tool) #:use-module (crates-io))

(define-public crate-libusb-sys-wishbone-tool-0.2.6 (c (n "libusb-sys-wishbone-tool") (v "0.2.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06mvv41i4sapgx2fwidxy17mxkb4n7bq04hw7wyywgs5wm3wqw97") (l "usb-1.0")))

