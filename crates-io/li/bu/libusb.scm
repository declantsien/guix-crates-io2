(define-module (crates-io li bu libusb) #:use-module (crates-io))

(define-public crate-libusb-0.0.1 (c (n "libusb") (v "0.0.1") (d (list (d (n "libusb-sys") (r "^0.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.2.0") (d #t) (k 2)) (d (n "regex") (r "^0.1.18") (d #t) (k 2)))) (h "0s6sa6pd0sz8ffngdc53xm94rhmnha11j14v9zp5hig11pqmw1kc")))

(define-public crate-libusb-0.0.2 (c (n "libusb") (v "0.0.2") (d (list (d (n "libusb-sys") (r "^0.0.2") (d #t) (k 0)) (d (n "regex") (r "^0.1.18") (d #t) (k 2)))) (h "00dkwr0c848xpp5vcplf0jj2x491p5p7l5ykpw0rwyqqhnjyskdx")))

(define-public crate-libusb-0.1.0 (c (n "libusb") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.26") (d #t) (k 2)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "06s23mlgizd9g37ny46x9b2i74ffxcv2srf5185ca38jkrb58rp0")))

(define-public crate-libusb-0.1.1 (c (n "libusb") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.26") (d #t) (k 2)) (d (n "time") (r "^0.1.25") (d #t) (k 0)))) (h "1ppinmdm68ac3hmx041q55xjcqjxp6k7fi919rfchhkl5nimbxn8")))

(define-public crate-libusb-0.2.0 (c (n "libusb") (v "0.2.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.26") (d #t) (k 2)))) (h "0ydl84yzq7pl2np24icj078r69f7aikr2j5991xgklqlzz4jda5f") (y #t)))

(define-public crate-libusb-0.2.1 (c (n "libusb") (v "0.2.1") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.26") (d #t) (k 2)))) (h "1gm4lr8ihnxnpv9fmjfgcfl0481b8cbg20w4vrbn34b12ys26s6v")))

(define-public crate-libusb-0.2.2 (c (n "libusb") (v "0.2.2") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^0.1.26") (d #t) (k 2)))) (h "17d8x16f4kvc0r8dzzmqwr6yynx3z9z0naqdwmiza1vf4lhbd3pk")))

(define-public crate-libusb-0.3.0 (c (n "libusb") (v "0.3.0") (d (list (d (n "bit-set") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.26") (d #t) (k 2)))) (h "0lxa70vv0w350kcjkip4jsbmn32f3xpg4v6d9vg57glwjbfhv6az")))

