(define-module (crates-io li bu liburing) #:use-module (crates-io))

(define-public crate-liburing-0.0.1 (c (n "liburing") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1m15b3mwkskxbq422mykls1vzgyfmn38hq0y53mx1q3c5yyxxrfh")))

(define-public crate-liburing-0.0.2 (c (n "liburing") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1vslhpi8a5ybikjz9zcdkimgy5j3y7an4db9kwb4f0hymn7x6wqa")))

