(define-module (crates-io li bu libusb-src) #:use-module (crates-io))

(define-public crate-libusb-src-1.0.26 (c (n "libusb-src") (v "1.0.26") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 1)))) (h "19fr6lqqbbl7cnm0x8kz7h69yv6jlhl7jxgs5xmp0p9a8jlng3z8") (y #t)))

(define-public crate-libusb-src-1.26.0 (c (n "libusb-src") (v "1.26.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 1)))) (h "1q0yd49xfhslhym05i6995v5328pndr2jfqz7p1m8m52y5cf596f") (y #t)))

(define-public crate-libusb-src-1.26.1 (c (n "libusb-src") (v "1.26.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 1)))) (h "1lmrw614cpcvajqbygsb3dbfnryddlfinhc4mdsx7vlsba5hh1fh") (y #t)))

(define-public crate-libusb-src-1.26.2 (c (n "libusb-src") (v "1.26.2") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 1)))) (h "1qdlj5a66d1gy57v0m4mrz1kn0rcqj8cil9xgadh6bjlxkznggi9")))

(define-public crate-libusb-src-1.27.0+1.0.26 (c (n "libusb-src") (v "1.27.0+1.0.26") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.9") (d #t) (k 1)))) (h "07zkfav3csi3h5z53ifcfgdbzmpw5iav6y55kql7byv4wjxhphjz")))

