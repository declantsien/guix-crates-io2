(define-module (crates-io li bu libusbmuxd-sys) #:use-module (crates-io))

(define-public crate-libusbmuxd-sys-0.1.0 (c (n "libusbmuxd-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0mcvy11qzd1i5r72w7qh8fk9gsal9l46ab1ndm84ychrdp8cs2xq") (l "usbmuxd")))

(define-public crate-libusbmuxd-sys-0.1.1 (c (n "libusbmuxd-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1bs966hbinqkps4xhpfa8gf37xckn19gzckidlbm7k9ms4awcqfn") (l "usbmuxd")))

