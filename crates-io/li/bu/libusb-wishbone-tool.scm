(define-module (crates-io li bu libusb-wishbone-tool) #:use-module (crates-io))

(define-public crate-libusb-wishbone-tool-0.3.1 (c (n "libusb-wishbone-tool") (v "0.3.1") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb-sys-wishbone-tool") (r "^0.2.5") (d #t) (k 0)) (d (n "regex") (r "^0.1.26") (d #t) (k 2)))) (h "0kvjwm18bb49k44s5n8ks64aw7phibr9b80g9sxqmlra6l883n6v")))

