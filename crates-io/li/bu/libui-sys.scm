(define-module (crates-io li bu libui-sys) #:use-module (crates-io))

(define-public crate-libui-sys-0.1.0 (c (n "libui-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "embed-resource") (r "^1.2") (d #t) (k 1)))) (h "0lp6ypphvpiml0m6yvncyrfhnk7h8hin86q8k2dxw63nk13ywbq1") (f (quote (("static"))))))

(define-public crate-libui-sys-0.1.1 (c (n "libui-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "embed-resource") (r "^1.2") (d #t) (k 1)))) (h "17b4jbwkdrdpm6n0a9n1q69r45crqcia3sb5w4sdh0dk7vc0z010") (f (quote (("static"))))))

(define-public crate-libui-sys-0.1.2 (c (n "libui-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "embed-resource") (r "^1.2") (d #t) (k 1)))) (h "1805pk56y8f47hqwsmvw215p7cizrhiikhfvlgh3j3spjibjcccw") (f (quote (("static"))))))

(define-public crate-libui-sys-0.1.3 (c (n "libui-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "embed-resource") (r "^1.2") (d #t) (k 1)))) (h "13bxc80lnsvkrq1k0cd1cpgfkb4kyd4pnccmvf07rgmkmpxy4m0b") (f (quote (("static"))))))

(define-public crate-libui-sys-0.1.4 (c (n "libui-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "embed-resource") (r "^1.2") (d #t) (k 1)))) (h "1ilbxi3hq93srbbsai56cs8hafiwfs0lwrncg7xhgyp0ag5g7a9g") (f (quote (("static"))))))

(define-public crate-libui-sys-0.1.5 (c (n "libui-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "embed-resource") (r "^1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0acq7vkr8vf1njgkwcy1zdg8s0h645zji4qi1yj9552ayvlhbp1c") (f (quote (("static"))))))

(define-public crate-libui-sys-0.1.6 (c (n "libui-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "embed-resource") (r "^1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qsyjp1b7c2i7p18zyrz44wfcashgmvj2hlvfk1rqm48ga7c2a2k") (f (quote (("static"))))))

(define-public crate-libui-sys-0.1.7 (c (n "libui-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "embed-resource") (r "^1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "00hag3xkllnwkbybs28vxl5wbg69gh51cl95hkdvllxwyxd02l05") (f (quote (("static"))))))

(define-public crate-libui-sys-0.1.8 (c (n "libui-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "> 0.49, < 1") (d #t) (k 1)) (d (n "embed-resource") (r "^1.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1s73ln5cnq4hr1pb8l87hr1nca5gp2ql1y4p1y97cj9ffs2dxg6k") (f (quote (("static"))))))

(define-public crate-libui-sys-0.1.9 (c (n "libui-sys") (v "0.1.9") (d (list (d (n "bindgen") (r "> 0.49, < 1") (d #t) (k 1)) (d (n "embed-resource") (r "^1.3") (d #t) (k 1)) (d (n "find-winsdk") (r "^0.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1c63559wzbibl22zx8mxajb5lwzj068hqr9sj1fc547xzb3m4bqb") (f (quote (("static"))))))

