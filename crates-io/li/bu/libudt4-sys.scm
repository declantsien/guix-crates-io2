(define-module (crates-io li bu libudt4-sys) #:use-module (crates-io))

(define-public crate-libudt4-sys-0.1.0 (c (n "libudt4-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "057f98gmwcryswz4shm86qsjvy4wcy9niawb55r7j1dw8mkv5g81")))

(define-public crate-libudt4-sys-0.1.1 (c (n "libudt4-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1zqqpgyjc1rn7pihng6gzlbm5w93rsabxlp1g5vl4w2n7ghk80ww")))

(define-public crate-libudt4-sys-0.2.0 (c (n "libudt4-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "05vfvdja0rqdxr6kky7lvajzn62cvg66amnal9aw5pqw32wvvmsk")))

