(define-module (crates-io li bu liburing2-sys) #:use-module (crates-io))

(define-public crate-liburing2-sys-0.1.0+f4e42a5 (c (n "liburing2-sys") (v "0.1.0+f4e42a5") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "10rp4yhjzsvnkrd7bljkr78gb42mqhdyimg7s6ygn5f6i6cb0j6i") (f (quote (("lto")))) (l "uring-ffi")))

(define-public crate-liburing2-sys-0.2.0+f4e42a5 (c (n "liburing2-sys") (v "0.2.0+f4e42a5") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "10774jdw4v7x9khi8hwxm3vl7zycbz0gbmfd4v18g929xn2bwk05") (f (quote (("lto")))) (l "uring-ffi")))

(define-public crate-liburing2-sys-0.3.0+f4e42a5 (c (n "liburing2-sys") (v "0.3.0+f4e42a5") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "0wvkjqgjdbcmsb0246qk8c1ggy649fnw0ri3rblqcldj16jci49b") (f (quote (("lto")))) (l "uring-ffi")))

