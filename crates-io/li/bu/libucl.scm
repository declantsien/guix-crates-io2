(define-module (crates-io li bu libucl) #:use-module (crates-io))

(define-public crate-libucl-0.2.0 (c (n "libucl") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libucl-bind") (r "^0.2.0") (d #t) (k 0)))) (h "1gc4paqsxj44b3wfac3p4p437pihfbqaz7xphqxs2jm05pzh9fnj") (f (quote (("unstable") ("default"))))))

(define-public crate-libucl-0.2.1 (c (n "libucl") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libucl-bind") (r "^0.2.0") (d #t) (k 0)))) (h "06fgdf0gv3zygzkwq4dw14mr1y3ffc60y5r73xgsw7w2h3anlmxv") (f (quote (("unstable") ("default"))))))

(define-public crate-libucl-0.2.2 (c (n "libucl") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libucl-bind") (r "^0.2.1") (d #t) (k 0)))) (h "1ykam691w6jfkhnhffj32ik4wkdzdldkqlw9qnj6x9jxvgc811da")))

(define-public crate-libucl-0.2.3 (c (n "libucl") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "libucl-bind") (r "^0.2.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1ghlqnnf5iigdl3dwp0dip9hp7ym5m4591yh4vldyvybqfmcwb1g")))

