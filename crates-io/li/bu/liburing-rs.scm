(define-module (crates-io li bu liburing-rs) #:use-module (crates-io))

(define-public crate-liburing-rs-0.1.0 (c (n "liburing-rs") (v "0.1.0") (d (list (d (n "libc") (r "^0") (d #t) (k 0)))) (h "0d8pcv5qv2m1md5nfndp2ax11i6kn6vwya8qi2aqlcmqp6y22g7m")))

(define-public crate-liburing-rs-0.1.1 (c (n "liburing-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0") (d #t) (k 0)))) (h "0sf0nf502kvvmarawg8h77k2hydy3332v71jwjdwjzmd1vcpd46y")))

(define-public crate-liburing-rs-0.1.2 (c (n "liburing-rs") (v "0.1.2") (d (list (d (n "atomic") (r "^0") (d #t) (k 0)) (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "bytemuck") (r "^1") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0") (d #t) (k 0)))) (h "077icjw4s1vhwml7n67hziwv7a6lm4wv84yxrl2fspahnm754mg9")))

