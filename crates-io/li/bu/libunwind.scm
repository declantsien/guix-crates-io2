(define-module (crates-io li bu libunwind) #:use-module (crates-io))

(define-public crate-libunwind-1.3.2 (c (n "libunwind") (v "1.3.2") (h "1zcjydbxpzlyd0an46xllwcafdkrm2vv5kx9wnv05xqb36wjn5vv")))

(define-public crate-libunwind-1.3.3 (c (n "libunwind") (v "1.3.3") (h "1b37rhz52fw00dykskwirhvh3p5m2qzyimvhkjvm9s3w1avkjrhc")))

