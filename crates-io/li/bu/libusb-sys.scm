(define-module (crates-io li bu libusb-sys) #:use-module (crates-io))

(define-public crate-libusb-sys-0.0.1 (c (n "libusb-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "^0.3.0") (d #t) (k 1)))) (h "1ypzc7jvld9r7jn86h0wm9b9px6psj8pqd5vv3rcz6qvwxrlghk8")))

(define-public crate-libusb-sys-0.0.2 (c (n "libusb-sys") (v "0.0.2") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "10wb82nmih0r2nrwrpwi4bkr1lhda5vfdl2fbvypj2wls2wdnkbn")))

(define-public crate-libusb-sys-0.1.0 (c (n "libusb-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "13bhyxv99k1lac6d90fr39lkvvlysq7lpzfhbv4xqkz2j39ndy0b")))

(define-public crate-libusb-sys-0.1.1 (c (n "libusb-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0kzpw412vv260x0hp1r7djknxk2igcjjw7spws51fsraj786ihxf")))

(define-public crate-libusb-sys-0.2.0 (c (n "libusb-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1p46bpacn9n1yh05z6s5k2r62509m7zydxw2wlb71ybv1wn0w01g")))

(define-public crate-libusb-sys-0.2.1 (c (n "libusb-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1967kf8m29cs895vm5xadmqc1bqfi69ihk0zpfv3d5zc8xgi7s9z")))

(define-public crate-libusb-sys-0.2.2 (c (n "libusb-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1afh64n7f19v902ngdhlklj5b1m0hyyyvqbij8pvy4smvc9am387")))

(define-public crate-libusb-sys-0.2.3 (c (n "libusb-sys") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0rypgad8cj591jvrwb78d60fmhrrabwlxxcjwv9lmmk34mcbclsc")))

