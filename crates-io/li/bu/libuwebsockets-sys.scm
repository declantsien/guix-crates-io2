(define-module (crates-io li bu libuwebsockets-sys) #:use-module (crates-io))

(define-public crate-libuwebsockets-sys-0.0.1 (c (n "libuwebsockets-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1bz5irwnk8p3k3k1dzpg40pb242hlpxijkb2ld5aqsynfla42h04") (f (quote (("uws_vendored")))) (y #t) (l "uwebsockets,uv,z,c++,ssl,crypto")))

(define-public crate-libuwebsockets-sys-0.0.2 (c (n "libuwebsockets-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1ap5jzzwvw3h8ma2wvnw00x1k6d16x8z26lxddjzfims5ggdfjf9") (f (quote (("uws_vendored")))) (y #t) (l "uwebsockets,uv,z,c++,ssl,crypto")))

(define-public crate-libuwebsockets-sys-0.0.3 (c (n "libuwebsockets-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "092xlb9fs4cafp9vypj7vrn7kjbvi9gv9idxd9wqk6zl4i39vkpv") (f (quote (("uws_vendored")))) (y #t) (l "uwebsockets,uv,z,ssl,crypto")))

(define-public crate-libuwebsockets-sys-0.0.5 (c (n "libuwebsockets-sys") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1b4i23l65fsri3p5mydsnl13hrx278480w35iadc0z90xwch24ry") (f (quote (("uws_vendored")))) (y #t) (l "uwebsockets,uv,z,ssl,crypto")))

(define-public crate-libuwebsockets-sys-0.0.6 (c (n "libuwebsockets-sys") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "18axc810gwbipmmyx1853zys2smz734anx4c60x2hy1ban2hldah") (f (quote (("uws_vendored")))) (l "uwebsockets,uv,z,ssl,crypto")))

(define-public crate-libuwebsockets-sys-0.0.7 (c (n "libuwebsockets-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "1hv23mhg4xvsvk61hk2v5vyrc6v4z8q34dr7nhh0wfkk2dr1424y") (f (quote (("uws_vendored")))) (l "uwebsockets")))

(define-public crate-libuwebsockets-sys-0.0.8 (c (n "libuwebsockets-sys") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "1qj6s6wf5i8cvhhlli8xvad10l2laq038v7dw3ilv86y4jaipj2r") (f (quote (("uws_vendored")))) (l "uwebsockets")))

(define-public crate-libuwebsockets-sys-0.0.9 (c (n "libuwebsockets-sys") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "0j041gpmaqzyyq0lcsk526y1jw3kjl65siv4s7zh0ypd05hzgsvz") (f (quote (("uws_vendored")))) (l "uwebsockets")))

(define-public crate-libuwebsockets-sys-0.0.10 (c (n "libuwebsockets-sys") (v "0.0.10") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)))) (h "0wv8g201lpjkghg09145pmz8yxssqdv57gk1dsi6ikcwqap3n7nm") (f (quote (("uws_vendored")))) (l "uwebsockets")))

