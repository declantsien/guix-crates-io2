(define-module (crates-io li bu libusbk-sys) #:use-module (crates-io))

(define-public crate-libusbk-sys-0.1.0 (c (n "libusbk-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "0vsf35zp3a62lf919n6wzwlwmn2451an4b0i85mrqycwi9ymjaxy") (f (quote (("vendored"))))))

(define-public crate-libusbk-sys-0.1.1 (c (n "libusbk-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "13l9z22avl26arvayahwl97hdaprg7hl8xywnzxnfr6mpl9sxqwa") (f (quote (("vendored"))))))

(define-public crate-libusbk-sys-0.1.2 (c (n "libusbk-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "1bdhy54pp0xf2r8qyf1yp0akryp1swirlipcri7p2ckjzjwimfsr") (f (quote (("vendored")))) (l "libusbk")))

(define-public crate-libusbk-sys-0.1.3 (c (n "libusbk-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2.15") (d #t) (k 1)))) (h "1imx3p2znrda5ypa1l6slrr8cr1yv8z7j9w4gqh3mg6v06sk42qi") (f (quote (("vendored")))) (l "libusbk")))

