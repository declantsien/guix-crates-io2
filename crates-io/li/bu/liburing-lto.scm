(define-module (crates-io li bu liburing-lto) #:use-module (crates-io))

(define-public crate-liburing-lto-0.1.0 (c (n "liburing-lto") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "0bn3zp97shshsjr8crp09j75zy1cw32gg4iwjmm3w7nr6nhbj0cr") (y #t)))

(define-public crate-liburing-lto-0.1.1 (c (n "liburing-lto") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1hrv5x69d8a61b6s2bqzmxais1172xx3hqipacswffa3pfyrbq8g") (y #t)))

(define-public crate-liburing-lto-0.2.0 (c (n "liburing-lto") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)))) (h "1jq1226ml14pvpv41aj66xi2ndcnpdpzzzrg128ahsdv8c9s9rdv") (f (quote (("lto")))) (y #t)))

