(define-module (crates-io li bu libunicorn-sys) #:use-module (crates-io))

(define-public crate-libunicorn-sys-0.4.0 (c (n "libunicorn-sys") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)))) (h "0wan8bkqngjb444kgsx8jsj55inpqlk1xca16nzjfsyqrfr09smm")))

(define-public crate-libunicorn-sys-0.6.0 (c (n "libunicorn-sys") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "os_type") (r "^0.5.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1krbkv1inw1lb2rcd3gmggwqk5mm5zw1sk5l2d9rmmm5a8xhindj")))

(define-public crate-libunicorn-sys-0.7.0 (c (n "libunicorn-sys") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "build-helper") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "os_type") (r "^0.6") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0mmjkwk852z0qi8wjd87yz6ym786r1x4nrxnry4lk65n30h3s643")))

(define-public crate-libunicorn-sys-0.8.0 (c (n "libunicorn-sys") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "build-helper") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "os_type") (r "^0.6") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jrcrqjq2j38ijyy8y1c2dlnfxykfnmwf28xfx5iw5m2hscikwrb")))

(define-public crate-libunicorn-sys-0.9.0 (c (n "libunicorn-sys") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "build-helper") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "os_type") (r "^2.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1wd0yc31wsw53hxm5y0zsmzl2vm78k3isn9qbkg2n5y96dxl9pml") (f (quote (("std")))) (l "unicorn")))

(define-public crate-libunicorn-sys-0.9.1 (c (n "libunicorn-sys") (v "0.9.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "build-helper") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "os_type") (r "^2.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0srs4mja2kfpwa10ym3jngg5grn9rqcgviisxpmhmzivhc5h0nxg") (f (quote (("std")))) (l "unicorn")))

