(define-module (crates-io li bu libuv) #:use-module (crates-io))

(define-public crate-libuv-1.0.0 (c (n "libuv") (v "1.0.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.37.2") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "1fmijj36aydk4db9r4sbp6ay9dsqck4cnfq2a5dmlgw15kc6lq71")))

(define-public crate-libuv-1.1.0 (c (n "libuv") (v "1.1.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.38.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "19h36nc56i7x68gznbxp044jizsikfr6f2f71l1qys7wsk5hky9r")))

(define-public crate-libuv-1.1.1 (c (n "libuv") (v "1.1.1") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "clang-sys") (r "=0.28.0") (d #t) (k 0)) (d (n "libuv-sys2") (r "=1.38.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "1wrrhyin4j0kkr20d02ycmlkc39w8iy0azpq0v7c065p50r6znzc")))

(define-public crate-libuv-1.1.2 (c (n "libuv") (v "1.1.2") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.38.1") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "1f58s9i197zhwy5jig5nzn5k29zjh3k0z444285lkrpzfv3r1lnv")))

(define-public crate-libuv-1.1.3 (c (n "libuv") (v "1.1.3") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.39.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "0k8vfsyvlb68ml30s7x8azva8rz7pn7irjmvyzdlp137gsrxfq6w")))

(define-public crate-libuv-2.0.0 (c (n "libuv") (v "2.0.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.40.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "0skjgxzzhsai55wh8ba89677qq0vix2r3idq806n7vxs8xjaa6ql")))

(define-public crate-libuv-2.0.1 (c (n "libuv") (v "2.0.1") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.40.1") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "0i4lwjcn2p9s29c0rajq9r19g8sa706ahvgqs966ca4sbgzj9n2k") (f (quote (("skip-pkg-config" "libuv-sys2/skip-pkg-config"))))))

(define-public crate-libuv-2.1.0 (c (n "libuv") (v "2.1.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.41.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "13hmr9sf31l53iyvccafqwdg4x23rsvm2x6xbdsc9m1n8vcgr7vy") (f (quote (("skip-pkg-config" "libuv-sys2/skip-pkg-config"))))))

(define-public crate-libuv-2.2.0 (c (n "libuv") (v "2.2.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.41.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "17lb82s3czrh7lfml71fljb1yjzj6zz9vsyap9jm6wp34cw4ihy5") (f (quote (("skip-pkg-config" "libuv-sys2/skip-pkg-config"))))))

(define-public crate-libuv-2.3.0 (c (n "libuv") (v "2.3.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.42.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "0ncicwlzldddgma5s6alnjmr67c9anfdpfpdcgmqnh961ijq5g5s") (f (quote (("skip-pkg-config" "libuv-sys2/skip-pkg-config"))))))

(define-public crate-libuv-2.4.0 (c (n "libuv") (v "2.4.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.43.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "0xrb3fvfixkmq1hng9qi3j2p3xl440qzyqzp9mwj7qq71j08rg1b") (f (quote (("skip-pkg-config" "libuv-sys2/skip-pkg-config"))))))

(define-public crate-libuv-2.5.0 (c (n "libuv") (v "2.5.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.44.2") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "15cyfrjr34d4jaxll6lrrzqhfg10jmqzbpngrzvnilvpkz78j29x") (f (quote (("skip-pkg-config" "libuv-sys2/skip-pkg-config"))))))

(define-public crate-libuv-2.6.0 (c (n "libuv") (v "2.6.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.45.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "0hhr4i4hvl21rw44y6vf5i02brixg062d2cwfw7m7apf9fj058mn") (f (quote (("skip-pkg-config" "libuv-sys2/skip-pkg-config"))))))

(define-public crate-libuv-2.7.0 (c (n "libuv") (v "2.7.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.46.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "0hnqm5ygpcqb7n7mkhgqnga1lb9ji6b8am9ly31k9l7zcyfv8k8h") (f (quote (("skip-pkg-config" "libuv-sys2/skip-pkg-config"))))))

(define-public crate-libuv-2.8.0 (c (n "libuv") (v "2.8.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.47.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "04zwis6yiz1s0rw8qc926yg54amzjml31ibwil95lq1awcb78dym") (f (quote (("skip-pkg-config" "libuv-sys2/skip-pkg-config"))))))

(define-public crate-libuv-2.9.0 (c (n "libuv") (v "2.9.0") (d (list (d (n "bitflags") (r "~1.2.1") (d #t) (k 0)) (d (n "libuv-sys2") (r "~1.48.0") (d #t) (k 0)) (d (n "rand") (r "~0.7.3") (d #t) (k 2)))) (h "0gwlp7nyv8b81csbdf8ycnlfyac8l82042hrh84brw49gikfr5qv") (f (quote (("skip-pkg-config" "libuv-sys2/skip-pkg-config"))))))

