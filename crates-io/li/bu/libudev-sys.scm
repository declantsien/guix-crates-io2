(define-module (crates-io li bu libudev-sys) #:use-module (crates-io))

(define-public crate-libudev-sys-0.1.0 (c (n "libudev-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0s5xh28jh5znlvm0qp88d9l090n5ir46ckws9pymynw49j73616r")))

(define-public crate-libudev-sys-0.1.1 (c (n "libudev-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0nkw9a7dq7f3gpggvqy89j8rsi5k02gm6hbark7rbawkif082kwy")))

(define-public crate-libudev-sys-0.1.2 (c (n "libudev-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0bxphcxm9plidvm9i2s6ghpi2bi1qpvkg8m7vg4cil594mpl2hv4")))

(define-public crate-libudev-sys-0.1.3 (c (n "libudev-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0mck1gi2pd4l3s10pv0nvbz312hhazfb9jgbhhqxqrm2gws1x6i4")))

(define-public crate-libudev-sys-0.1.4 (c (n "libudev-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "09236fdzlx9l0dlrsc6xx21v5x8flpfm3d5rjq9jr5ivlas6k11w")))

