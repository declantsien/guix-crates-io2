(define-module (crates-io li bu libui) #:use-module (crates-io))

(define-public crate-libui-0.0.0 (c (n "libui") (v "0.0.0") (h "02aqa1jjgwg6hs2rkdpx7nkan5mxn87kzqfsmjll65ris2zyis0z")))

(define-public crate-libui-0.1.0 (c (n "libui") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libui-ffi") (r "^0.1.0") (d #t) (k 0)))) (h "0md2g05dm4j7w14mc1w9wcw1yz657r14g6rmccjw8g7wnf99sbn9")))

(define-public crate-libui-0.2.0 (c (n "libui") (v "0.2.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libui-ffi") (r "^0.2.0") (d #t) (k 0)))) (h "1gv659dd8g04iwxgc6dl2mnclrpzqmcp6zch5ylvflcly3nx5nq1")))

(define-public crate-libui-0.3.0 (c (n "libui") (v "0.3.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libui-ffi") (r "^0.3.0") (d #t) (k 0)))) (h "1p1w4vcq2m4kqljrs1n5wzc57mkbifvclzf92m8l7pxijkg37xgj")))

