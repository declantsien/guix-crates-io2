(define-module (crates-io li bu libui-ffi) #:use-module (crates-io))

(define-public crate-libui-ffi-0.0.0 (c (n "libui-ffi") (v "0.0.0") (h "1mqq91mfj3l3lszwzzj0gxc0s3hzwpiv0z5c5lqhcgz205qds769")))

(define-public crate-libui-ffi-0.1.0 (c (n "libui-ffi") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embed-resource") (r "^1.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1badds6maqafj70q0vr9k26j84mnac911qypibd075dcjr4z85vl") (f (quote (("fetch") ("default" "fetch" "build") ("build")))) (l "ui")))

(define-public crate-libui-ffi-0.2.0 (c (n "libui-ffi") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embed-resource") (r "^1.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1vvzbnx0i41njn0bq6wijn624i0fazdf3d5ldl7azmi0lsnb4j53") (f (quote (("fetch") ("default" "fetch" "build") ("build")))) (l "ui")))

(define-public crate-libui-ffi-0.3.0 (c (n "libui-ffi") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "embed-resource") (r "^2.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1si7zrsj74zwmzgjsqbr5biixbklgc6a94ik5cc4zaip6m4qqgjv") (f (quote (("fetch") ("default" "fetch" "build") ("build")))) (l "ui")))

