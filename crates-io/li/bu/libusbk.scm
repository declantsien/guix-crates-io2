(define-module (crates-io li bu libusbk) #:use-module (crates-io))

(define-public crate-libusbk-0.1.0 (c (n "libusbk") (v "0.1.0") (d (list (d (n "libusbk-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("errhandlingapi" "ioapiset"))) (d #t) (k 0)))) (h "1fd9kcpa0fhlki8bskw7av3wazfhm2vxs79h8r9nix0a5vll5xy3") (f (quote (("vendored" "libusbk-sys/vendored"))))))

(define-public crate-libusbk-0.1.1 (c (n "libusbk") (v "0.1.1") (d (list (d (n "libusbk-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("errhandlingapi" "ioapiset"))) (d #t) (k 0)))) (h "0lwsvwpwglnq1xppnfarm1sqm6qr6qik07q06f3izhhfmiadli35") (f (quote (("vendored" "libusbk-sys/vendored"))))))

(define-public crate-libusbk-0.1.2 (c (n "libusbk") (v "0.1.2") (d (list (d (n "libusbk-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("errhandlingapi" "ioapiset"))) (d #t) (k 0)))) (h "05qfagdi4r3p24zhrqpagh554vnwn4rdmxz5p4x712lc2gky6s8n") (f (quote (("vendored" "libusbk-sys/vendored"))))))

