(define-module (crates-io li bu libusbgx-sys) #:use-module (crates-io))

(define-public crate-libusbgx-sys-0.1.0 (c (n "libusbgx-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)))) (h "00dh641ckzmbn99bszw3n2wv7cky9z1gh9pv6gyr4gyibisz82gc")))

(define-public crate-libusbgx-sys-0.1.1 (c (n "libusbgx-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0056r7p1g9s6pqs9wdnx1ii7yn50mcib0h1bgw25abs2di7n5zrb")))

