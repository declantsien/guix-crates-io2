(define-module (crates-io li bu liburing-sys) #:use-module (crates-io))

(define-public crate-liburing-sys-0.1.0 (c (n "liburing-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "0px1w6avpmviv0ww1i95ww1xrdiyzw5nvpsrjxcjdcnh9z9hg86r")))

(define-public crate-liburing-sys-0.1.1 (c (n "liburing-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "12x7rhl16k8gf4qfab3sx0fydd835xcsinb0cc0fpc2s67jcbg6n")))

(define-public crate-liburing-sys-0.1.2 (c (n "liburing-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0b7sbrsbkyqn4yh19hfgzmz35ag7adcaqq96vs9rzb1n9wimiydk")))

