(define-module (crates-io li bu libucl-sys) #:use-module (crates-io))

(define-public crate-libucl-sys-0.1.0 (c (n "libucl-sys") (v "0.1.0") (d (list (d (n "curl-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1sasbbdqsmnhz4ar18akmx68ag93hkmmxxi46zddl17y6ivgi03m")))

(define-public crate-libucl-sys-0.1.1 (c (n "libucl-sys") (v "0.1.1") (d (list (d (n "curl-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0fqh9mckxqsgch5ga7yfp9s9dgwbl1634daqwbnk3y6w1wjlvbkm")))

(define-public crate-libucl-sys-0.1.2 (c (n "libucl-sys") (v "0.1.2") (d (list (d (n "curl-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0nijnxglif6h5yar7mx4h04y5a7nz0ggawjq5b8gdw1pa7pfpxwg")))

(define-public crate-libucl-sys-0.1.3 (c (n "libucl-sys") (v "0.1.3") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "curl-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1shwvyvh4fhmvm6sh9vwaglj9vqii6mr7aqqx39bvgjxhsqsgkhq")))

(define-public crate-libucl-sys-0.1.4 (c (n "libucl-sys") (v "0.1.4") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "curl-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "openssl-sys") (r "*") (d #t) (k 0)))) (h "0bdwsrmvjkasdzavz02bwij6sdnv67q82032lq49mlrhxd1m1gxg")))

(define-public crate-libucl-sys-0.1.5 (c (n "libucl-sys") (v "0.1.5") (d (list (d (n "bitflags") (r "*") (d #t) (k 0)) (d (n "curl-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "1l9vdq48krmsxwhis5bzvazji03mknhb94kmxry893jqpd0vi1g3")))

