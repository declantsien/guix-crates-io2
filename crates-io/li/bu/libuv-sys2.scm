(define-module (crates-io li bu libuv-sys2) #:use-module (crates-io))

(define-public crate-libuv-sys2-1.30.1 (c (n "libuv-sys2") (v "1.30.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "085w5p2pcvjs1mm0qag16ns95byc4icanvmm2yazs8kv86w978kh") (l "uv")))

(define-public crate-libuv-sys2-1.31.1 (c (n "libuv-sys2") (v "1.31.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "12mb25cdkwcq6127v9br2s91aphxgd1cm8djfigrnrzn8glhngzd") (l "uv")))

(define-public crate-libuv-sys2-1.32.1 (c (n "libuv-sys2") (v "1.32.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1pvck200xb898lkhh0y5l128n6w08lzl29d5qrwal4jr4495j9j7") (l "uv")))

(define-public crate-libuv-sys2-1.33.2 (c (n "libuv-sys2") (v "1.33.2") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0r1ncm09y91i1mqx6iac35xyaid8a2sg6sffpc4zryi23pq7n84s") (l "uv")))

(define-public crate-libuv-sys2-1.34.4 (c (n "libuv-sys2") (v "1.34.4") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1izglyaaa89h8s1yjl0jyfqj1p65140j2mzwflbf30pr5bgnxkr5") (l "uv")))

(define-public crate-libuv-sys2-1.35.0 (c (n "libuv-sys2") (v "1.35.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "14gza15rmbz6x9hwavz2pmw9wj7xq36cm6czvb1045glx76y3jih") (l "uv")))

(define-public crate-libuv-sys2-1.35.1 (c (n "libuv-sys2") (v "1.35.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "nt_version") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1k93p8sbh6hhzw8j3qfxvay1qxv0fnkcia4ic6g47c99maj50zja") (l "uv")))

(define-public crate-libuv-sys2-1.35.2 (c (n "libuv-sys2") (v "1.35.2") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "nt_version") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0lbd25ac99pxy6hw1dfc60b79dsjsr73k3bx9cfayzd2nzbvc82p") (l "uv")))

(define-public crate-libuv-sys2-1.35.3 (c (n "libuv-sys2") (v "1.35.3") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "nt_version") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1ranfg5pmh0663vcxd43fqxbg8jcxfnnadn5i164w9bglzxi9ad9") (l "uv")))

(define-public crate-libuv-sys2-1.36.0 (c (n "libuv-sys2") (v "1.36.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "nt_version") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0nfp6a02b5id3g4wa90nspvbgg5qjplzw1vbx5d8d75lnqn6ajwk") (l "uv")))

(define-public crate-libuv-sys2-1.36.1 (c (n "libuv-sys2") (v "1.36.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "nt_version") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1x760kcq8f8znjc1jkikydwdaqmwdyqvxi5579p1l0b54xrygjpd") (l "uv")))

(define-public crate-libuv-sys2-1.37.0 (c (n "libuv-sys2") (v "1.37.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "nt_version") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1hlvaazpwbsqgg9qbfd0m8f64pvhkvrih8xinnsljnc6zp9g2imm") (l "uv")))

(define-public crate-libuv-sys2-1.37.1 (c (n "libuv-sys2") (v "1.37.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "nt_version") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1yhaqhh1aq93iaw2i9clkf5rcr5kfgn3jwbllqxhpijwzfnirabi") (l "uv")))

(define-public crate-libuv-sys2-1.37.2 (c (n "libuv-sys2") (v "1.37.2") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "nt_version") (r "^0.1.3") (d #t) (t "cfg(windows)") (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1bfvnd38nfrnx8y42zgffy4fk3bv7syhn6n8rhgnhl890jv8nj73") (l "uv")))

(define-public crate-libuv-sys2-1.38.0 (c (n "libuv-sys2") (v "1.38.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "15ijmj5iyd42wzpkxwlgz8ip030lf8laivibk6plkb64fyxg2im1") (l "uv")))

(define-public crate-libuv-sys2-1.38.1 (c (n "libuv-sys2") (v "1.38.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "08jipa7rr6nivl2ccp3wx44w9q8icldgcwycvyx8bvplfq12zpq2") (l "uv")))

(define-public crate-libuv-sys2-1.39.0 (c (n "libuv-sys2") (v "1.39.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "07fpa1h1l35cvc1ab1py2qjk0jrida4h8xmnfkgby2d7rrilhqjg") (l "uv")))

(define-public crate-libuv-sys2-1.40.0 (c (n "libuv-sys2") (v "1.40.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "040vh4abli8vjmwqgsvl44vm24sasgn29p2g86smizpgq3vrlfzl") (l "uv")))

(define-public crate-libuv-sys2-1.40.1 (c (n "libuv-sys2") (v "1.40.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1p62fq8nv2bv24y4amdgzx73pgnig2h2mzha4n755jqpnjw384pw") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.41.0 (c (n "libuv-sys2") (v "1.41.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1g4xrz41irrzab3a9pamsf8hbpcism92mnlr5yscsgish4v67dqd") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.41.1 (c (n "libuv-sys2") (v "1.41.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0fwjyk8y7p31ip2riin762ywf49hb8rp2i43b8909fvkjagvw8f3") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.42.0 (c (n "libuv-sys2") (v "1.42.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "127hl9fwn3ji96ixbw4546zspjn9la9s1v76zkamghyvy8dw36q9") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.43.0 (c (n "libuv-sys2") (v "1.43.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0vi763k9f18nm3gzjp780hrighv9gnb9k5fiafpl006pfjm5k9wd") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.44.0 (c (n "libuv-sys2") (v "1.44.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1wl2zc565qbdi18xc9s51dyhnnvir057gl1q29g88rp764q7hn2i") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.44.1 (c (n "libuv-sys2") (v "1.44.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "06h1pig4waay5c71c5scvsmrvlaxis6jaj4nkq4r5dd4gqs47ahr") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.44.2 (c (n "libuv-sys2") (v "1.44.2") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0sdl3k0kzcnifc637nnn3a4qlabiladj6r94yllb23qnjhqdpv42") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.45.0 (c (n "libuv-sys2") (v "1.45.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0j5a576j5rl1b64a2fgkhyafm2fwgx4jz54z8pyjl3xhmk5x66k7") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.46.0 (c (n "libuv-sys2") (v "1.46.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0h39z2q89r6iipc1hihdk1ff7ls11rn89r56z5kk339khxslyn7j") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.46.1 (c (n "libuv-sys2") (v "1.46.1") (d (list (d (n "bindgen") (r "^0.68.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0lv2b3inds7kwrv3pjvy3slgkh2rmh4f1vx87jl0mmznymgq5cv8") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.47.0 (c (n "libuv-sys2") (v "1.47.0") (d (list (d (n "bindgen") (r "^0.68.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1b8m03p7hxdhbzbcf8am880h5l82k7b2ilzqgars7dilsqx3f179") (f (quote (("skip-pkg-config")))) (l "uv")))

(define-public crate-libuv-sys2-1.48.0 (c (n "libuv-sys2") (v "1.48.0") (d (list (d (n "bindgen") (r "^0.68.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0hzkgpi5r35xlfwcx9vxyzwvm148xz964rz79haqlsd542if29b1") (f (quote (("skip-pkg-config")))) (l "uv")))

