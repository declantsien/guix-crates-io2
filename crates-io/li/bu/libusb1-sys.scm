(define-module (crates-io li bu libusb1-sys) #:use-module (crates-io))

(define-public crate-libusb1-sys-0.3.0 (c (n "libusb1-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1gjnxry1cx6yr6ryhk8rk9qhp010qxighi5bc318321nv9xhr97c")))

(define-public crate-libusb1-sys-0.3.1 (c (n "libusb1-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0ldr223cnxwgppwlcyrnwmk0z0b395cpvb82g32206dd3kidpl9d")))

(define-public crate-libusb1-sys-0.3.2 (c (n "libusb1-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "18lwvpchyybdscl4fnaqm08wa55gch4c56abjifr4wz7695hh12r")))

(define-public crate-libusb1-sys-0.3.3 (c (n "libusb1-sys") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1qwdzrvr8dz8ndnjm4rvr5q3fax97p41f0gjaw74fg3y8pf07cwx")))

(define-public crate-libusb1-sys-0.3.4 (c (n "libusb1-sys") (v "0.3.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0i6szbqi02hr97h953r4ra4v57mq45w6z1l1c2dl38jl1xn0plij")))

(define-public crate-libusb1-sys-0.3.5 (c (n "libusb1-sys") (v "0.3.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1ydn1ycaanngx1vrrh5vfr2p7vk55wmm0pkgcdyv5bpmfr3727g0") (f (quote (("vendored"))))))

(define-public crate-libusb1-sys-0.3.6 (c (n "libusb1-sys") (v "0.3.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0d2bg6yygd7a2mhrrx8ldrl0ngy9dyz360g4lfli30nw3q9qycg0") (f (quote (("vendored"))))))

(define-public crate-libusb1-sys-0.3.7 (c (n "libusb1-sys") (v "0.3.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0vpqm19198yd2mpw8q3x0jlz6n1swqyzgrppkskk7wmn8vadvnbi") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.4.0 (c (n "libusb1-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "196xvimmqsgz2gq7kjjmrb64gi1h6sdhw90i39iphi8k1ji8x2js") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.4.1 (c (n "libusb1-sys") (v "0.4.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0ychlnx7n4fcpjpjy4l9yrx6d4wm0iska5hsgpg3frf5wa12lp6a") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.4.2 (c (n "libusb1-sys") (v "0.4.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0f9z484s73qgmcqhz4c07aq0sx5rzq2skcxaq2bwq0qp2qqfj0hz") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.4.3 (c (n "libusb1-sys") (v "0.4.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "11f6l1cpfjvjqy9dq93q510qxhp2hmr3lnl6l211m4y8pn2q6fsy") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.5.0-beta.1 (c (n "libusb1-sys") (v "0.5.0-beta.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "117yp4z8zhj268sdnz2nir743jws2v0c080x9x2p5gjzrbvipnf9") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.4.4 (c (n "libusb1-sys") (v "0.4.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libflate") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "00rp9w0vr7wf1xrpw3b388larvbihycch9l51dnpc91a229ic95y") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.5.0 (c (n "libusb1-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0gq27za2av9gvdz1pgwlzaw3bflyhlxj0inlqp31cs5yig88jbp2") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.6.0 (c (n "libusb1-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "09rhhy750wrrv81h7kq5qzswl3j7bqzmmv2ad0cqx62dimz2nxxq") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.6.1 (c (n "libusb1-sys") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0fwqi4639w6x33pyqga4myn7vzqw2d65kkblwf9y1zcl4s6yn6pa") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.6.2 (c (n "libusb1-sys") (v "0.6.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1kh9d0hghi1whz1pqi7q01zjbrjm1pcinhib94xmma2s224v1ykd") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.6.3 (c (n "libusb1-sys") (v "0.6.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0nj5s2vy9dmcknw15w51b1xgwdcllvs6gpjxyfnjhmgbg7lnpbj3") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.6.4 (c (n "libusb1-sys") (v "0.6.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "09sznaf1lkahb6rfz2j0zbrcm2viz1d1wl8qlk4z4ia2rspy5l7r") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.6.5 (c (n "libusb1-sys") (v "0.6.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "05wg7nv56rm9ik3bnr6cy7h2yywy8v7aylf8hybjw21r5z7bmxhp") (f (quote (("vendored")))) (l "usb-1.0")))

(define-public crate-libusb1-sys-0.7.0 (c (n "libusb1-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_os = \"windows\")") (k 1)))) (h "03yfx469d1ldpw2h21hy322f5a0h1ahlgy4s6yjipzy4gbg0l1fs") (f (quote (("vendored")))) (l "usb-1.0")))

