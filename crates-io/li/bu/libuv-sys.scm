(define-module (crates-io li bu libuv-sys) #:use-module (crates-io))

(define-public crate-libuv-sys-0.1.0 (c (n "libuv-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0gkz8rkvw3mmhy4i0pcvv51cia7pd0l7f160qh3b2x6mk0zykf5y")))

