(define-module (crates-io li bu libucl-bind) #:use-module (crates-io))

(define-public crate-libucl-bind-0.2.0 (c (n "libucl-bind") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "00a9wazxhkqi6xc9dwcvlbc54j87dxw30p3xdck8hxjfzgb4pgqp")))

(define-public crate-libucl-bind-0.2.1 (c (n "libucl-bind") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "0i6f5ljkjiy2vzsjn4w5zk9dngdljaqkawwlvzrgnm505w4c8c59")))

(define-public crate-libucl-bind-0.2.2 (c (n "libucl-bind") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libc") (r "^0.2.66") (d #t) (k 0)))) (h "1mm1i4j2z3yfrm3rgcdd3zlzk5ydnf7vrhaj4rrzjirs19ffv7hi")))

