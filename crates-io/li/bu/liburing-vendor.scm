(define-module (crates-io li bu liburing-vendor) #:use-module (crates-io))

(define-public crate-liburing-vendor-0.0.1 (c (n "liburing-vendor") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "06bdsx7fzib89l5c6rmdkh86zplpqfkvph9if5xz4xxpcrplkrw8")))

(define-public crate-liburing-vendor-0.0.2 (c (n "liburing-vendor") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "0x7fgvm78drv08j85c78pkig68833mgab3pi1nc24him0dbrbckv")))

(define-public crate-liburing-vendor-0.0.3 (c (n "liburing-vendor") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "16banp0a3a80cgfpdp634c6nh27h7jqn6spzlzlj8jqjra1mhdn7")))

(define-public crate-liburing-vendor-0.0.4 (c (n "liburing-vendor") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "05ysxqiznxdfr05bvljjcfs3cb0b2m0kf4n3pg5ymcfc5iqg41rw")))

(define-public crate-liburing-vendor-0.0.5 (c (n "liburing-vendor") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)))) (h "0dab0fjnyv0pqs7i5vriwcb0y7yf4bn5ynxnh95nq1qj4g12hr2d")))

