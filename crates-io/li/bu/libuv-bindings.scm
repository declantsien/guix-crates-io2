(define-module (crates-io li bu libuv-bindings) #:use-module (crates-io))

(define-public crate-libuv-bindings-0.2.0 (c (n "libuv-bindings") (v "0.2.0") (d (list (d (n "libuv-sys2") (r "^1.44.2") (d #t) (k 0)) (d (n "luajit-bindings") (r "^0.2.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.15") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0l3xbnl4y7hhwwfss3jc5c7a5zcka22vh8arsqgnjy0y7ycy2wxs")))

