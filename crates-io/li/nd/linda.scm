(define-module (crates-io li nd linda) #:use-module (crates-io))

(define-public crate-linda-0.1.0 (c (n "linda") (v "0.1.0") (d (list (d (n "http") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "139nk0025y92gsv1x6z2l3s16xz88d3fxy41nq54k8lzb2xmybyc")))

(define-public crate-linda-0.1.1 (c (n "linda") (v "0.1.1") (d (list (d (n "http") (r "^0.1.18") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "0v3dzfm3g652zklwypnifcfn13c6rn7gi3lkyrhdajlny67w7ml9")))

(define-public crate-linda-0.1.2 (c (n "linda") (v "0.1.2") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1k77figysbzjm654c09w361zcw2nxb8rzyv7d5ln3qwr0hpmm538")))

(define-public crate-linda-0.2.0 (c (n "linda") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "17dcqrjqjyfcvck1nlgpx730zh5x5sfb1y8l9a3jn7nn0sm18wf2")))

