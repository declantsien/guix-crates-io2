(define-module (crates-io li nd lindenmayer) #:use-module (crates-io))

(define-public crate-lindenmayer-0.1.0 (c (n "lindenmayer") (v "0.1.0") (d (list (d (n "glam") (r "^0.22.0") (d #t) (k 0)))) (h "187r0bkzax8dzhqrplizg1k5wbgwp4rqmpg3g0khk9brmik29f6d")))

(define-public crate-lindenmayer-0.2.0 (c (n "lindenmayer") (v "0.2.0") (d (list (d (n "glam") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "1dd2a88q04q8xz97hxqxfns1mnd2aklh8c7fhl5in3hl9pg0kyb0")))

(define-public crate-lindenmayer-0.3.0 (c (n "lindenmayer") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6.0") (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1.0") (d #t) (k 0)))) (h "0y22nllayk10x94v2iqk7ryljwd9bvnzdas0a77g3f6wlvjfdjjh")))

