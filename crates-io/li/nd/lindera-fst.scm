(define-module (crates-io li nd lindera-fst) #:use-module (crates-io))

(define-public crate-lindera-fst-0.1.0 (c (n "lindera-fst") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "levenshtein_automata") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.4") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1") (d #t) (k 0)))) (h "16bza3inv796sbzfj8qpzx50241axzal1gk580zlxp3q5570yvvs")))

(define-public crate-lindera-fst-0.1.1 (c (n "lindera-fst") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "fnv") (r "^1.0.5") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 2)) (d (n "levenshtein_automata") (r "^0.1") (d #t) (k 0)) (d (n "proptest") (r "^0.9.4") (d #t) (k 2)) (d (n "quickcheck") (r "^0.7") (k 2)) (d (n "rand") (r "^0.5") (d #t) (k 2)) (d (n "regex-syntax") (r "^0.4") (d #t) (k 0)) (d (n "utf8-ranges") (r "^1") (d #t) (k 0)))) (h "1q2a52q0m9y8pr2wi5434z2m41cr5wizlzi25p6rd4k7lry8l2d6")))

