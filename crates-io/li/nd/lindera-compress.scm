(define-module (crates-io li nd lindera-compress) #:use-module (crates-io))

(define-public crate-lindera-compress-0.9.0 (c (n "lindera-compress") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.9.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0x9z5mndfvf3q2ap3w52aw94x1pz09r2jp99v1xii9cs47sc33hh")))

(define-public crate-lindera-compress-0.9.1 (c (n "lindera-compress") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.9.1") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1127faj8az58cvcx0ap9ifxibamjmnrjd0y87w9yimyj5qlxrrw2")))

(define-public crate-lindera-compress-0.10.0 (c (n "lindera-compress") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.10.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1y2aclnl00zxph8cvs0v5rnbcpim3x2adj0gqfzrm6vcan7c4d1p")))

(define-public crate-lindera-compress-0.11.0 (c (n "lindera-compress") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.11.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0sb11zqc2r2hacylxsf404y81v6dpi8a831crq3n1aakyhqcskvz")))

(define-public crate-lindera-compress-0.11.1 (c (n "lindera-compress") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.11.1") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0s0nqmmlsr87422f87b9aib3vnfk4xf5r4dl67zrlnk961f0zx1h")))

(define-public crate-lindera-compress-0.12.0 (c (n "lindera-compress") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.12.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1kwpsg6d3skb763bshjs3js27q8ww6i926rspqj9234kkz04j1y8")))

(define-public crate-lindera-compress-0.12.1 (c (n "lindera-compress") (v "0.12.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.12.1") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1h7p7ra3sbr3qqrna24sif7jzwpac54if3czfxh164kam1idpwv7")))

(define-public crate-lindera-compress-0.12.2 (c (n "lindera-compress") (v "0.12.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.12.2") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1x3gmmfcw791j625pf2ai8vcmcypxhrl2dm2riaqvcqplkwnp7fw")))

(define-public crate-lindera-compress-0.12.3 (c (n "lindera-compress") (v "0.12.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.12.3") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0jd1y6p9ivwi70s3r8fkc1ln2015g513csf3p21lvf2qqb7xfagw")))

(define-public crate-lindera-compress-0.12.4 (c (n "lindera-compress") (v "0.12.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.12.4") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1bfp98ig2xbqxbrzylgi5zd4skx1slxd0813xrcjb66j0wxjazsl")))

(define-public crate-lindera-compress-0.12.5 (c (n "lindera-compress") (v "0.12.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.12.5") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "09v1abssklq0anrpm8m6byyibqbvi6whgdwz0ig5cyby59ca87k0")))

(define-public crate-lindera-compress-0.12.6 (c (n "lindera-compress") (v "0.12.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.12.6") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1671d9wcvb2hpxm19rpq929fdwcjwxnvlanzd3yhz8yfihrnyd7r")))

(define-public crate-lindera-compress-0.13.0 (c (n "lindera-compress") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.13.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1661a6rb0cxm396shdaz7i03z40viqqpx4sgkdhi21zaanzxbhyw")))

(define-public crate-lindera-compress-0.13.1 (c (n "lindera-compress") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.13.1") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0ls1g3mpkf5s8li78gwmbxix70hwphym8i18pn7f56wmjz5iqc23")))

(define-public crate-lindera-compress-0.13.2 (c (n "lindera-compress") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.13.2") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0xivdjjs1iqw9r5503r1mf0h84zgp49b4vk59rjqsairqfy2k9ir")))

(define-public crate-lindera-compress-0.13.3 (c (n "lindera-compress") (v "0.13.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.13.3") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "12a3l8050fcnwaqhf8rrjna6cw5amn4k0h1mrgaq1nyls6ymswyy")))

(define-public crate-lindera-compress-0.13.4 (c (n "lindera-compress") (v "0.13.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.13.4") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5") (d #t) (t "cfg(not(windows))") (k 0)))) (h "06g855shcvn9whwrfyvh2148ayq7znncvxz3d96cr7pmx5fn32r1")))

(define-public crate-lindera-compress-0.13.5 (c (n "lindera-compress") (v "0.13.5") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.13.5") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "04lg05pvgq3g6lnbbph796jign3l8mvp1ladzj36m2ya1xcm3903")))

(define-public crate-lindera-compress-0.17.0 (c (n "lindera-compress") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.17.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "1ggk6na3fhbvl9y1pkcnlz8p22rpc84a4bbyn1yyg2w2dsqcc1ic")))

(define-public crate-lindera-compress-0.18.0 (c (n "lindera-compress") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.18.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rust-lzma") (r "^0.5.1") (d #t) (t "cfg(not(windows))") (k 0)))) (h "0qdlgs0qzg6vh345wnfx9wx552mxc467ml91jml9kvmzbl4mslgp")))

(define-public crate-lindera-compress-0.19.0 (c (n "lindera-compress") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "198ryvnfyg6yr3wbmqk8bbbl9q5agnh1zxsz32yza1gqla9f4gix")))

(define-public crate-lindera-compress-0.20.0 (c (n "lindera-compress") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.20.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0d0s96pkk82gyzw1ffvahcdr1yyblz50amvpr6a11c4a4as9klmp")))

(define-public crate-lindera-compress-0.21.0 (c (n "lindera-compress") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "09dif7lc2xg81y2cw3dwavg3y42n7f6m2kn9ra0j6pk2wlyyml58")))

(define-public crate-lindera-compress-0.22.0 (c (n "lindera-compress") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.22.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "138y9zbawifzi6f89zq02r9ipvvwj9dj1zgsg930nzb7j6mgbsqp")))

(define-public crate-lindera-compress-0.22.1 (c (n "lindera-compress") (v "0.22.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.22.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "10pb317kimsnr88c0r0vk14yp5hr5calgjyjygmrq9ygrv0rl9w0")))

(define-public crate-lindera-compress-0.23.0 (c (n "lindera-compress") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.23.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0dwrhrjbaij2cbrpxxkj1i1qdfhlc7pdz450g1l3yl4mb6zrd4gd")))

(define-public crate-lindera-compress-0.24.0 (c (n "lindera-compress") (v "0.24.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.24.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18cspvjd6z052y4qs3z8anwkc27djm5j19g3l8zgzjs8amrhk05y")))

(define-public crate-lindera-compress-0.25.0 (c (n "lindera-compress") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.25.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0yjja2y4044qn2a238vw0q2jb0km75j77hnidsxjnd9nfzk9jblg")))

(define-public crate-lindera-compress-0.26.0 (c (n "lindera-compress") (v "0.26.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.26.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1pk573kgnpc7na21w4027q3wfz0isck0m3wvw2s9s5g1qndk7pvs")))

(define-public crate-lindera-compress-0.27.0 (c (n "lindera-compress") (v "0.27.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0navsrbfcdljb28i20vkyfmx6ghkar84msl5xsf8mgmwb42lcazp")))

(define-public crate-lindera-compress-0.27.1 (c (n "lindera-compress") (v "0.27.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.27.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "18k3mwn9367lpvgifkanjdp4alx1n24fapayza0rigjysm9ybws9")))

(define-public crate-lindera-compress-0.27.2 (c (n "lindera-compress") (v "0.27.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.27.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1khd8bng3s6f6v3a2q02z10w6qr6f84qdgwifvh1kz2gpp9bf9qr")))

(define-public crate-lindera-compress-0.28.0 (c (n "lindera-compress") (v "0.28.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.28.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0pfscg91kbgd37i3zmcci3v26anb2r54h67la69s9czkj5815nil")))

(define-public crate-lindera-compress-0.29.0 (c (n "lindera-compress") (v "0.29.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.29.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1f6pkzba727csx54rp6m69chwqgcf2ilsw1qpr9vnxh9laycksg2")))

(define-public crate-lindera-compress-0.30.0 (c (n "lindera-compress") (v "0.30.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.30.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0055g108dfgn5k7ia1z888sb5mmj1m0hqakwszkiby9kyjy3qdij")))

(define-public crate-lindera-compress-0.31.0 (c (n "lindera-compress") (v "0.31.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "lindera-decompress") (r "^0.31.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0v6ar5ccnc2cvw88rq0r0fpqz379m5y51g9w73jgj3ypb2lggyah")))

