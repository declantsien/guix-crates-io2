(define-module (crates-io li nd lindel) #:use-module (crates-io))

(define-public crate-lindel-0.1.0 (c (n "lindel") (v "0.1.0") (d (list (d (n "morton-encoding") (r "^2") (k 0)) (d (n "nalgebra") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "14kx68jc4rywrffb2m8dsq9w24ph5r5xj3i3bpa15rs89vf8vjas") (f (quote (("std") ("nalg" "nalgebra") ("default" "std"))))))

(define-public crate-lindel-0.1.1 (c (n "lindel") (v "0.1.1") (d (list (d (n "morton-encoding") (r "^2") (k 0)) (d (n "nalgebra") (r "^0.25") (o #t) (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1759mj4yarvdpg5fi39fqy74g6pa3rnn844s7ny0nf6104lvljg0") (f (quote (("std") ("nalg" "nalgebra") ("default" "std"))))))

