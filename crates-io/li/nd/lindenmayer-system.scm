(define-module (crates-io li nd lindenmayer-system) #:use-module (crates-io))

(define-public crate-lindenmayer-system-0.1.0 (c (n "lindenmayer-system") (v "0.1.0") (d (list (d (n "expression") (r "^0.3.0") (d #t) (k 0)) (d (n "expression-num") (r "^0.1.0") (d #t) (k 2)) (d (n "turtle") (r "^0.1.1") (d #t) (k 0)))) (h "1dxfm6qjvxw147izkz1qxl72dyyx4nqa711s94gsvazmnblfkxk4")))

(define-public crate-lindenmayer-system-0.2.0 (c (n "lindenmayer-system") (v "0.2.0") (d (list (d (n "expression") (r "^0.3.0") (d #t) (k 0)) (d (n "expression-num") (r "^0.1.0") (d #t) (k 2)) (d (n "turtle") (r "^0.1.1") (d #t) (k 0)))) (h "0bi43gc7qs9cy0d1xx3g0a2bdl1br2mmcmz74cjkk2iiidpsd5fb")))

(define-public crate-lindenmayer-system-0.3.0 (c (n "lindenmayer-system") (v "0.3.0") (d (list (d (n "expression") (r "^0.3.0") (d #t) (k 0)) (d (n "expression-num") (r "^0.1.0") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "turtle") (r "^0.1.1") (d #t) (k 0)))) (h "1my99sd667axhc8ksrzp07l02g0l0qyv59bjas51l7222b47zbqh")))

(define-public crate-lindenmayer-system-0.3.1 (c (n "lindenmayer-system") (v "0.3.1") (d (list (d (n "expression") (r "^0.3.4") (d #t) (k 0)) (d (n "expression-num") (r "^0.2.3") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "turtle-graphics") (r "^0.1.2") (d #t) (k 0)))) (h "1i0phz93n30w14anlcdsi4vlhzcx9vsklgzl4719gmzds0d49if3")))

