(define-module (crates-io li nd lindenmayer_graphic) #:use-module (crates-io))

(define-public crate-lindenmayer_graphic-0.1.0 (c (n "lindenmayer_graphic") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "turtle") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "turtle-svg") (r "^0.1.1") (d #t) (k 0)))) (h "0w511yzmnvgypsbsm68w65rs4qdq13kj8cj6wwsbn5wqibmrqv6d")))

(define-public crate-lindenmayer_graphic-0.1.1 (c (n "lindenmayer_graphic") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "turtle") (r "^1.0.0-rc.3") (d #t) (k 0)) (d (n "turtle-svg") (r "^0.1.1") (d #t) (k 0)) (d (n "weighted_rand") (r "^0.3.2") (d #t) (k 0)))) (h "0jp9cv961bpiffcly350vapdz55pajbffmr8mb51m03l3yaklah4")))

