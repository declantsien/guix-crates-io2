(define-module (crates-io li nd lindera-decompress) #:use-module (crates-io))

(define-public crate-lindera-decompress-0.9.0 (c (n "lindera-decompress") (v "0.9.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pfx2ar4hxsl2v0b6haqsrjb9r6rfl36x40mwj8axivn56wrpx7v")))

(define-public crate-lindera-decompress-0.9.1 (c (n "lindera-decompress") (v "0.9.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fv2viq1z9wnrxq9rj143hpvibalgw263sygf9vw23rqvrc54rpz")))

(define-public crate-lindera-decompress-0.10.0 (c (n "lindera-decompress") (v "0.10.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kf2aawk45d4pkvkgyvf59yvi9if2wsinx34m17wi53xixi3v3pw")))

(define-public crate-lindera-decompress-0.11.0 (c (n "lindera-decompress") (v "0.11.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rp1yc75niwvhhm19c26whliqkj37psy3d1z4hhzm9na49xhc1fy")))

(define-public crate-lindera-decompress-0.11.1 (c (n "lindera-decompress") (v "0.11.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xvrjc97shq22yx4gp4d0nndbh122nr4f4lm5zxxrq4kwppij0zs")))

(define-public crate-lindera-decompress-0.12.0 (c (n "lindera-decompress") (v "0.12.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l1jc7z3p62v25gwlnnlxqcv4gvg75syp2ni638q03kjsgyd7yn0")))

(define-public crate-lindera-decompress-0.12.1 (c (n "lindera-decompress") (v "0.12.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bincode") (r "^1.3") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02j4gv6nyd4wyw1q409mal0rlsad0ihc2ajiaw24crxpfyxc5dlg")))

(define-public crate-lindera-decompress-0.12.2 (c (n "lindera-decompress") (v "0.12.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v9z2l865nz109sxq4rzfm4lsgcbkmbp3z7jyabjmqhasdb52rpy")))

(define-public crate-lindera-decompress-0.12.3 (c (n "lindera-decompress") (v "0.12.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0f7s2bg3wb7kcvgjf42s15spbwjanj8r4a12pydxcklpk56blfp9")))

(define-public crate-lindera-decompress-0.12.4 (c (n "lindera-decompress") (v "0.12.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "14kabiv5slyb9xddv8562q19fjwhw9vsdcdcmama21q09dp0a7vh")))

(define-public crate-lindera-decompress-0.12.5 (c (n "lindera-decompress") (v "0.12.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1y05v1xi5lb5x4583sknq27mwsifa6ffbpykz2d3i8a7l7cw4x7f")))

(define-public crate-lindera-decompress-0.12.6 (c (n "lindera-decompress") (v "0.12.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0jpmazhkyrp2zyxfj3jakbj79w2rdiwq7pq5g12w4glpr0myczl1")))

(define-public crate-lindera-decompress-0.13.0 (c (n "lindera-decompress") (v "0.13.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01391b9q2cq6wb48rvga4w8jl5bafdz7nql3bfjxjpcg20lmvf0g")))

(define-public crate-lindera-decompress-0.13.1 (c (n "lindera-decompress") (v "0.13.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zgb8qvkphjm51b12bvfhpi47v8fvjqhdjz3hxpazkfxrbzwa4kk")))

(define-public crate-lindera-decompress-0.13.2 (c (n "lindera-decompress") (v "0.13.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kxgk06qk57xasps14rnkizas463529rs8rs283r5vgs9adazid9")))

(define-public crate-lindera-decompress-0.13.3 (c (n "lindera-decompress") (v "0.13.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nl01nr7s3gshp57q82k53ik23lmn46i7m50vk22kmsz23y1zvcf")))

(define-public crate-lindera-decompress-0.13.4 (c (n "lindera-decompress") (v "0.13.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i1slp3xs1dwni9w37s26n7zr2bzk2qcpf5wk5x944zdrm880sxr")))

(define-public crate-lindera-decompress-0.13.5 (c (n "lindera-decompress") (v "0.13.5") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i37kmr72cr1s99nw5z32j9w85rzpq7gyr7frmb142iim67g77bg")))

(define-public crate-lindera-decompress-0.17.0 (c (n "lindera-decompress") (v "0.17.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hwdw09ndcxjr0kdw1438w20lz49fzd6c2nva9jgm77q50cb9b9b")))

(define-public crate-lindera-decompress-0.18.0 (c (n "lindera-decompress") (v "0.18.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "lzma-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (f (quote ("derive"))) (d #t) (k 0)))) (h "17nz3dj67phdnc2fljsr8irrf7l3352515wzvarzyp5hxcrw13zn")))

(define-public crate-lindera-decompress-0.19.0 (c (n "lindera-decompress") (v "0.19.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hlc0cbcq2nsrldjjlg6xq85hiqq4yjihqjbkkbhmdpbd939iac8")))

(define-public crate-lindera-decompress-0.20.0 (c (n "lindera-decompress") (v "0.20.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.151") (f (quote ("derive"))) (d #t) (k 0)))) (h "11h45zm64clrvb6ndzsaiyb3s02ay49bnb0k7r6az1i34ylnl4s4")))

(define-public crate-lindera-decompress-0.21.0 (c (n "lindera-decompress") (v "0.21.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zdfvy4s00kabkzw2wij3vlizr1amkspm2db0mim0k3maka1n42j")))

(define-public crate-lindera-decompress-0.22.0 (c (n "lindera-decompress") (v "0.22.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "19mvvv6cmhw481bh64byvy3y8jrn31gcf5vrsvijjp4pgmdvll9k")))

(define-public crate-lindera-decompress-0.22.1 (c (n "lindera-decompress") (v "0.22.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1d6kc4n97dnphs5g7xkqxs51qalk6k56xn8i6z84mz6k4vy5h0p8")))

(define-public crate-lindera-decompress-0.23.0 (c (n "lindera-decompress") (v "0.23.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "18pl7f4s4pcgcy0lza4l8hcid8xphmhslwnk4qncxhqbxyk3zrm6")))

(define-public crate-lindera-decompress-0.24.0 (c (n "lindera-decompress") (v "0.24.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x7d4g0002cxm2q9qmxh2xrpx6qqv2dd97i6qfmlc62xqqyibbqn")))

(define-public crate-lindera-decompress-0.25.0 (c (n "lindera-decompress") (v "0.25.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1imk2scjz419s4x2qzpajla9ilp8is39p2y8b5zrvi33pdm40xkl")))

(define-public crate-lindera-decompress-0.26.0 (c (n "lindera-decompress") (v "0.26.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ry1gnq9j1c05950rmg1vgphbx1b41c8mcg5yav2mzrh14m8nzqc")))

(define-public crate-lindera-decompress-0.27.0 (c (n "lindera-decompress") (v "0.27.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qf0vj0kd81zb3fl8grikzam7nrhfxsvsf3krhphg656ip6zmc8z")))

(define-public crate-lindera-decompress-0.27.1 (c (n "lindera-decompress") (v "0.27.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "16sjrcn2sn1pr5liixm8i1s61z1gsvzpw6zp4ssjr5hwwq8a8xz0")))

(define-public crate-lindera-decompress-0.27.2 (c (n "lindera-decompress") (v "0.27.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "flate2") (r "^1.0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0.167") (f (quote ("derive"))) (d #t) (k 0)))) (h "0nwyqyn1ilr22srsa4rwbv6r9fmpk78m21y4y27xqrrs6b9bi0kv")))

(define-public crate-lindera-decompress-0.28.0 (c (n "lindera-decompress") (v "0.28.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n1007g4g5c2x6y99dgvdhksgbb9vwx9ahpwzdaglkybk5n4gx4q")))

(define-public crate-lindera-decompress-0.29.0 (c (n "lindera-decompress") (v "0.29.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fnljwdkp69kx8w20rahvyfk6sgrsnin5q2cl8hypwzkn9ac1pxk")))

(define-public crate-lindera-decompress-0.30.0 (c (n "lindera-decompress") (v "0.30.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pi85iclm9h684mi078pkmzdkaccq5wp1ii9l6wnbdpqyr2n6h0f")))

(define-public crate-lindera-decompress-0.31.0 (c (n "lindera-decompress") (v "0.31.0") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "flate2") (r "^1.0.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)))) (h "04ibmax7d2icsjldn2g6djpsrwvrw3pvb9cz5pw4asyxnb4h0d6m")))

