(define-module (crates-io li kh likh) #:use-module (crates-io))

(define-public crate-likh-0.0.1 (c (n "likh") (v "0.0.1") (h "0rgn0j4zh6prariinq65xkq3v24xfm265n1n36650knn02nyn905")))

(define-public crate-likh-0.1.0 (c (n "likh") (v "0.1.0") (h "1mw3b7wm2354795h1w3v7zfs9lz9ly9z3ps9fk283wqz17p8igzp")))

(define-public crate-likh-0.1.1 (c (n "likh") (v "0.1.1") (h "05rfy1imvvbk97y7hmlw8p7j0mhkibwasrpgpdhvldxvq159r41d")))

(define-public crate-likh-0.1.2 (c (n "likh") (v "0.1.2") (h "1s4w9iswxik2niw65qh8rkqqg2k4bg4pb9nr0fii9bpma8bc3zx0")))

(define-public crate-likh-0.1.3 (c (n "likh") (v "0.1.3") (h "0mf11hi8razx3acmb71a58zk623x88arskmaj3wsa5w60s27dzmk")))

(define-public crate-likh-0.1.4 (c (n "likh") (v "0.1.4") (h "1qnm7f0jh7yrcmvmcnkh6ih1779b3crc0bcxbigp3ylhwqd3ax35")))

(define-public crate-likh-0.1.5 (c (n "likh") (v "0.1.5") (h "0x3fg4xzh3m1dapwlbsa0kadabdvpic51m016q01zsgbdi84fycs")))

