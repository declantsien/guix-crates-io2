(define-module (crates-io li mo limonite) #:use-module (crates-io))

(define-public crate-limonite-0.0.0 (c (n "limonite") (v "0.0.0") (d (list (d (n "liquid") (r "^0.1.1") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.2.2") (d #t) (k 0)))) (h "0agbjvil79k4a0kb2gdsx481ga0666g1w68dbvs4wfbrfzp80l3j")))

(define-public crate-limonite-0.1.0 (c (n "limonite") (v "0.1.0") (d (list (d (n "clap") (r "^1.5.5") (d #t) (k 0)) (d (n "git2") (r "^0.3") (d #t) (k 1)) (d (n "liquid") (r "^0.2.0") (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)) (d (n "uuid") (r "^0.1.18") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.2.2") (d #t) (k 0)))) (h "13y1686z000dxdi1p8l8r849ff59jvyi5znn7fa1qcxfbync03ws")))

(define-public crate-limonite-0.2.1 (c (n "limonite") (v "0.2.1") (d (list (d (n "clap") (r "^1.5.5") (d #t) (k 0)) (d (n "git2") (r "^0.3") (d #t) (k 1)) (d (n "handlebars") (r "^0.13.0") (f (quote ("serde_type"))) (d #t) (k 0)) (d (n "pulldown-cmark") (r "^0.0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1.41") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.34") (d #t) (k 0)) (d (n "serde") (r "^0.6.10") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.6.10") (d #t) (k 1)) (d (n "serde_json") (r "^0.6.0") (d #t) (k 0)) (d (n "syntex") (r "^0.26.0") (d #t) (k 1)) (d (n "uuid") (r "^0.1.18") (d #t) (k 0)) (d (n "walkdir") (r "^0.1.5") (d #t) (k 0)) (d (n "yaml-rust") (r "^0.2.2") (d #t) (k 0)))) (h "1bqza7m98il3ihws03k44s48izs03b3yzr61hc6wfay0pkpzzyx7")))

