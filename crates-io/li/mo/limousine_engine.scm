(define-module (crates-io li mo limousine_engine) #:use-module (crates-io))

(define-public crate-limousine_engine-0.1.0 (c (n "limousine_engine") (v "0.1.0") (d (list (d (n "limousine_core") (r "^0.1.0") (d #t) (k 0)) (d (n "limousine_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0hwwnmhw243zikif5avka4smbw8vwiz04di0ncicdnnan8zf0nl5") (y #t)))

(define-public crate-limousine_engine-0.1.1 (c (n "limousine_engine") (v "0.1.1") (d (list (d (n "limousine_core") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "limousine_derive") (r "^0.1.0") (d #t) (k 0)))) (h "0lf70nvg23vcd2pj4502f4a86hchf145m6kczh2lfv60liyxkxl2") (s 2) (e (quote (("standard" "limousine_core?/standard") ("default" "dep:limousine_core"))))))

(define-public crate-limousine_engine-0.1.2 (c (n "limousine_engine") (v "0.1.2") (d (list (d (n "limousine_core") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "limousine_derive") (r "^0.1.0") (d #t) (k 0)))) (h "13ks7pmjdl2f9l6i1ilvhhhzcdycfyhxm2s2lgklixc7gp381gwj") (s 2) (e (quote (("standard" "limousine_core?/standard") ("default" "dep:limousine_core"))))))

(define-public crate-limousine_engine-0.2.0 (c (n "limousine_engine") (v "0.2.0") (d (list (d (n "limousine_core") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "limousine_derive") (r "^0.2.0") (d #t) (k 0)))) (h "19hhj6r97yzp4i3yxhgx5dkgfpc30qpvmq2cn65hvv0p98a763zk") (y #t) (s 2) (e (quote (("standard" "limousine_core?/standard") ("default" "dep:limousine_core"))))))

(define-public crate-limousine_engine-0.2.1 (c (n "limousine_engine") (v "0.2.1") (d (list (d (n "limousine_core") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "limousine_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1di35zr00sj4mw2lgd1n1i18z5bw49hhgja5fwvdn3104qma6aiq") (y #t) (s 2) (e (quote (("standard" "limousine_core?/standard") ("default" "dep:limousine_core"))))))

(define-public crate-limousine_engine-0.2.2 (c (n "limousine_engine") (v "0.2.2") (d (list (d (n "limousine_core") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "limousine_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1z0d97ji7b5vz3cpcwvrbl0f15fadi1m4jr4qj6dzjcw5ilpiwag") (y #t) (s 2) (e (quote (("standard" "limousine_core?/standard") ("default" "dep:limousine_core"))))))

(define-public crate-limousine_engine-0.2.3 (c (n "limousine_engine") (v "0.2.3") (d (list (d (n "limousine_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "limousine_derive") (r "^0.2") (d #t) (k 0)))) (h "0nprday1xpahvgm518260v2v2c70hwj0yrwnhbqw8lifih3lkkpq") (y #t) (s 2) (e (quote (("standard" "limousine_core?/standard") ("default" "dep:limousine_core"))))))

(define-public crate-limousine_engine-0.2.4 (c (n "limousine_engine") (v "0.2.4") (d (list (d (n "limousine_core") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "limousine_derive") (r "^0.2") (d #t) (k 0)))) (h "0h278xm0qihph3ryh7jfpjdhplm9hqi4nd28ci8vpz0zkjww5r5w") (s 2) (e (quote (("standard" "limousine_core?/standard") ("default" "dep:limousine_core"))))))

(define-public crate-limousine_engine-0.3.0 (c (n "limousine_engine") (v "0.3.0") (d (list (d (n "limousine_core") (r "^0.3") (d #t) (k 0)) (d (n "limousine_derive") (r "^0.3") (d #t) (k 0)))) (h "1b3nad3gwp726vphaqr56iy9jkyjw9yygnr48qfhy9xzwfdacj2f")))

(define-public crate-limousine_engine-0.3.1 (c (n "limousine_engine") (v "0.3.1") (d (list (d (n "limousine_core") (r "^0.3") (d #t) (k 0)) (d (n "limousine_derive") (r "^0.3") (d #t) (k 0)))) (h "13jj0cxbrsz4cj3q3hcjj77c11qzw5m8dfsvsxr4pzny266zhzyl")))

(define-public crate-limousine_engine-0.3.2 (c (n "limousine_engine") (v "0.3.2") (d (list (d (n "limousine_core") (r "^0.3") (d #t) (k 0)) (d (n "limousine_derive") (r "^0.3") (d #t) (k 0)))) (h "0nqy0m8vkb4f3pa1zy4lx1rvzjina2p4337g489awjh6vnwglk06")))

(define-public crate-limousine_engine-0.3.3 (c (n "limousine_engine") (v "0.3.3") (d (list (d (n "limousine_core") (r "^0.3") (d #t) (k 0)) (d (n "limousine_derive") (r "^0.3") (d #t) (k 0)))) (h "0ry9fz8s667s7ipqvvn0hf3i1ac1wb2my2cv5180zhwbwsylgl7r")))

(define-public crate-limousine_engine-0.3.4 (c (n "limousine_engine") (v "0.3.4") (d (list (d (n "limousine_core") (r "^0.3.4") (d #t) (k 0)) (d (n "limousine_derive") (r "^0.3.4") (d #t) (k 0)))) (h "16nkpa9s03il4lhzgmssdsacipfayxzhpadfci727yv68izqpixz")))

