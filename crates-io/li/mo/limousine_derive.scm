(define-module (crates-io li mo limousine_derive) #:use-module (crates-io))

(define-public crate-limousine_derive-0.1.0 (c (n "limousine_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iw2s293snvpnnala3dilhfhxlrj38g3p0zgg48ylh5071pfb6jr")))

(define-public crate-limousine_derive-0.2.0 (c (n "limousine_derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0iyzc8y6gq3iyjf9dibww7dipas668yx8zbh462xpxvgi0bn86x9")))

(define-public crate-limousine_derive-0.3.0 (c (n "limousine_derive") (v "0.3.0") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "19jpff2lkk233ydzyvn1sjwaznmqmsnnm0mnhdrw5i1j1cvypn5s")))

(define-public crate-limousine_derive-0.3.3 (c (n "limousine_derive") (v "0.3.3") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1y4q85h9nz3y6j74h6yr8lcxnmhh5kpk0n9j3sh7bmcvpxl5gvby")))

(define-public crate-limousine_derive-0.3.4 (c (n "limousine_derive") (v "0.3.4") (d (list (d (n "base64") (r "^0.22.0") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1ca6ryafmvckzzwqr2q0mhxfpnwrr9yw5m8j20ic5lzsy6l84czv")))

