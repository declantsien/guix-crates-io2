(define-module (crates-io li np linprog) #:use-module (crates-io))

(define-public crate-linprog-0.3.0 (c (n "linprog") (v "0.3.0") (d (list (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "1630c3c2cjpiysy052m9z4bq50xjp99b8g3x1r8w4mhfdd2ppglq") (y #t)))

(define-public crate-linprog-0.3.1 (c (n "linprog") (v "0.3.1") (d (list (d (n "uuid") (r "^0.4") (f (quote ("v4"))) (d #t) (k 0)))) (h "0i9pcjcjkbp5rmfjsa2ysklyr29l8v834lv4rrka29xj875bmhrf")))

