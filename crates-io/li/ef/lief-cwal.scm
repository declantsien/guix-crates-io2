(define-module (crates-io li ef lief-cwal) #:use-module (crates-io))

(define-public crate-lief-cwal-0.1.0 (c (n "lief-cwal") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "image") (r "^0.24.3") (f (quote ("ico"))) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "lief-cwal-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "picky") (r "^7.0.0-rc.3") (f (quote ("wincert" "x509" "pkcs7"))) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.32") (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (f (quote ("v4"))) (k 2)) (d (n "widestring") (r "^1.0.2") (f (quote ("alloc"))) (k 0)))) (h "1ab286vkl34hql3qa1v8lw3vhsd7cdpyps1pwazjbk40r2n2hj6p")))

