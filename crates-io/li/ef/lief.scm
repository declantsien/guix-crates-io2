(define-module (crates-io li ef lief) #:use-module (crates-io))

(define-public crate-lief-0.0.1 (c (n "lief") (v "0.0.1") (h "1i2imwd254664zpgg0ik5l94frnccrysni70b82l1cwhidnpgvzd")))

(define-public crate-lief-0.0.2 (c (n "lief") (v "0.0.2") (h "1a81590ys32yr7l0daxxkw3pms04db2ayc3r1clfw3zr60wa371p")))

