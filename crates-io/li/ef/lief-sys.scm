(define-module (crates-io li ef lief-sys) #:use-module (crates-io))

(define-public crate-lief-sys-0.11.5 (c (n "lief-sys") (v "0.11.5") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0kr74ly05rfjwyp1qnq7rr4zwy2s8ph7kmajpkbncrfmiwm61a1l") (y #t)))

(define-public crate-lief-sys-0.0.1 (c (n "lief-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0bqqimf54mfkxwa6w4f6n9lgn4xfkrymlfw4dhlhmk0qbirsp4h9")))

