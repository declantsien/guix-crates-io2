(define-module (crates-io li gm ligma) #:use-module (crates-io))

(define-public crate-ligma-0.69.420 (c (n "ligma") (v "0.69.420") (h "1rxpbv150bp9gj7vk47gyxakycmmsshixx118m683ym9b54bc4ra")))

(define-public crate-ligma-0.420.69 (c (n "ligma") (v "0.420.69") (h "0z9qi0hlh7mc95cwhcl8g33r5df138fk630v87bmbc3dvd7izrgh")))

