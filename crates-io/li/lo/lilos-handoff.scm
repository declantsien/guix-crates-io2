(define-module (crates-io li lo lilos-handoff) #:use-module (crates-io))

(define-public crate-lilos-handoff-1.0.0-pre.1 (c (n "lilos-handoff") (v "1.0.0-pre.1") (d (list (d (n "lilos") (r "^1.0.0-pre.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (k 0)))) (h "0n6xkmcp2w6x7xq2rlg4zlqmxxr9kdif3wnrjyfjw50qga2gw19r") (r "1.69")))

(define-public crate-lilos-handoff-1.0.0-pre.2 (c (n "lilos-handoff") (v "1.0.0-pre.2") (d (list (d (n "lilos") (r "^1.0.0-pre.3") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (k 0)))) (h "12lsg2yzfrf17q0jlj95nssm86qw17qmqn4fwblspc5kf5708ij1") (r "1.69")))

(define-public crate-lilos-handoff-1.0.0 (c (n "lilos-handoff") (v "1.0.0") (d (list (d (n "lilos") (r "^1.0.0") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (k 0)))) (h "0x0dhfsjxih5ja6v0vvnxw0gd7qvbz0r84j75w5ayfqlwqp40abi") (r "1.69")))

(define-public crate-lilos-handoff-1.0.1 (c (n "lilos-handoff") (v "1.0.1") (d (list (d (n "lilos") (r "^1.0.2") (k 0)) (d (n "scopeguard") (r "^1.2.0") (k 0)))) (h "1im1ngyyxjrr13i1kpclpplc0ypigrd5b0v0bif6ql1ihmam42w8") (r "1.69")))

