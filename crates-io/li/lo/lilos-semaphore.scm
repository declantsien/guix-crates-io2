(define-module (crates-io li lo lilos-semaphore) #:use-module (crates-io))

(define-public crate-lilos-semaphore-0.1.0 (c (n "lilos-semaphore") (v "0.1.0") (d (list (d (n "lilos") (r "^1.0.2") (k 0)) (d (n "pin-project") (r "^1.1.5") (d #t) (k 0)))) (h "0v6qhq251nssvpbdhji39sq3hv7znwll5wyj4zm3kr5igxnbz11k") (r "1.69")))

(define-public crate-lilos-semaphore-0.2.0 (c (n "lilos-semaphore") (v "0.2.0") (d (list (d (n "lilos") (r "^1.1.0") (k 0)) (d (n "lilos-list") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.5") (d #t) (k 0)))) (h "196rib80kz18rfc03iv9g4164ns2khapb8c0dd4aphzkv0zva7y9") (r "1.69")))

