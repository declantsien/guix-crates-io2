(define-module (crates-io li lo lilos-rwlock) #:use-module (crates-io))

(define-public crate-lilos-rwlock-0.1.0 (c (n "lilos-rwlock") (v "0.1.0") (d (list (d (n "lilos") (r "^1.1.0") (k 0)) (d (n "pin-project") (r "^1.1.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (k 0)))) (h "16syrbf63704d3n10zamkk8kjvjgrbbpd48jzxfd9vx4mlc10k58") (r "1.69")))

(define-public crate-lilos-rwlock-0.2.0 (c (n "lilos-rwlock") (v "0.2.0") (d (list (d (n "lilos") (r "^1.1.0") (k 0)) (d (n "lilos-list") (r "^0.1.0") (d #t) (k 0)) (d (n "pin-project") (r "^1.1.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.2.0") (k 0)))) (h "0rs4p523f7wlxfqicky0w8vq0g1xhn2776kjzlx5ycxf2c4i6zrx") (r "1.69")))

