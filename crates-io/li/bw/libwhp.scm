(define-module (crates-io li bw libwhp) #:use-module (crates-io))

(define-public crate-libwhp-0.1.0 (c (n "libwhp") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gvmj7ixx33hghnhnmaggdwjcrxpfg11d79yv43igk83650bdj9p") (y #t)))

(define-public crate-libwhp-0.1.1 (c (n "libwhp") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "052zs9h7qnvfkmsj6bjbpawarnnxpsf34qaa4dc1m3vph7hq4hw9") (y #t)))

(define-public crate-libwhp-0.1.2 (c (n "libwhp") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wny4f8fsa1jnmgbk5imjma82l13bx0rlb5jyapkf50l3cffz2cc")))

(define-public crate-libwhp-0.1.3 (c (n "libwhp") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1fal4ymz01srl6bj8lnk98mdik3d0223v8rspa72jgvay2g1w82n")))

