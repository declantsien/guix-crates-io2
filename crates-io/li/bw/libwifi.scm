(define-module (crates-io li bw libwifi) #:use-module (crates-io))

(define-public crate-libwifi-0.1.0 (c (n "libwifi") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bd7vbwhvacnz4p8gbxi8abf2k4z9gfgl3pv0p73hpqglzs8257j")))

(define-public crate-libwifi-0.2.0 (c (n "libwifi") (v "0.2.0") (d (list (d (n "libwifi_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0w50hv4j1jp8w68dmgvj2wk58gqjn59i18kdwy8p3p432nbjqxfb")))

(define-public crate-libwifi-0.3.0 (c (n "libwifi") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "libwifi_macros") (r "^0.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "strum_macros") (r "^0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1bszdkzbxnzwvyh11m1vac948fsdvqwvy5gjc70rl553ai0p0q57")))

(define-public crate-libwifi-0.3.1 (c (n "libwifi") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3") (d #t) (k 2)) (d (n "enum_dispatch") (r "^0.3") (d #t) (k 0)) (d (n "libwifi_macros") (r "^0.0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "strum_macros") (r "^0.24") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01rbf3amcm5hlcfhmpqz6j62warmxkjb0ki2pdfzvhq77hyd1q4w")))

