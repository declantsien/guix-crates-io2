(define-module (crates-io li bw libwfa) #:use-module (crates-io))

(define-public crate-libwfa-0.1.0 (c (n "libwfa") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0rd5i805z8f3mhzvi6zl7in5a7cin1ybg2knb6g0y8pnbkn7vg5x")))

(define-public crate-libwfa-0.1.1 (c (n "libwfa") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hhq7j9qypnw1pk4j0lzlyf6b4gxz1yiqx6i3axs782ypgqn5f6d")))

(define-public crate-libwfa-0.1.2 (c (n "libwfa") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pj0hnjjlsd2rd5bsvq6hbwrgcxkm57j2yabpgsx14n2gra8970d")))

