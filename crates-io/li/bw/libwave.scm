(define-module (crates-io li bw libwave) #:use-module (crates-io))

(define-public crate-libwave-0.1.0 (c (n "libwave") (v "0.1.0") (d (list (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "0cfwgf24bclr4gsfarll7v3frv5dyz5wpcj7nx346svp9070v0jq")))

(define-public crate-libwave-0.1.1 (c (n "libwave") (v "0.1.1") (d (list (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "1zsvr6fax5h408gwrj32js2x8j6n8ippxm781g4lvnxk52bdyidc")))

(define-public crate-libwave-0.1.2 (c (n "libwave") (v "0.1.2") (d (list (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "1y8fl1g6lr3dln0xq1abp4c0ji7ypqchdrqmy7fymgbblw6d1xmi")))

