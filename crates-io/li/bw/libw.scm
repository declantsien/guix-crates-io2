(define-module (crates-io li bw libw) #:use-module (crates-io))

(define-public crate-libw-0.0.1 (c (n "libw") (v "0.0.1") (d (list (d (n "wasi") (r "^0.9.0") (d #t) (k 0)))) (h "1c7daxi1md3ja5haw0dmspyq7myspj3cvdrgxidi575j4sdr73fy")))

(define-public crate-libw-0.0.2 (c (n "libw") (v "0.0.2") (d (list (d (n "wasi") (r "^0.9.0") (d #t) (k 0)))) (h "10j3jgcy6hcwqv15xd0jxrmwvxsvxnsky7rkx2z8xl8ir2sjkbbl")))

(define-public crate-libw-0.0.3 (c (n "libw") (v "0.0.3") (d (list (d (n "wasi") (r "^0.9.0") (d #t) (k 0)))) (h "1yd60wb51m4rlbxvyfhm7q61mc00q3k9qka8hgz0djbr94bk9lcy")))

(define-public crate-libw-0.1.0 (c (n "libw") (v "0.1.0") (d (list (d (n "wasi") (r "^0.9.0") (d #t) (k 0)))) (h "0zl7jasy6l207b37wizbxar2qw9az21v8z9g318fhr4m30y4pg5s")))

(define-public crate-libw-0.1.1 (c (n "libw") (v "0.1.1") (d (list (d (n "wasi") (r "^0.9.0") (d #t) (k 0)))) (h "1b7nbl2bzl9pyvnrif47npc43322805haanszjg4d2pqp3cxbpdy")))

(define-public crate-libw-0.1.2 (c (n "libw") (v "0.1.2") (d (list (d (n "wasi") (r "^0.9.0") (d #t) (k 0)))) (h "04m1dcmh7ja58kn0fr8p9fnjy693gdwgg53x6nklw7vwbvaywy14")))

(define-public crate-libw-0.1.3 (c (n "libw") (v "0.1.3") (d (list (d (n "wasi") (r "^0.9.0") (d #t) (k 0)))) (h "1lkd47jz208ad2q2bh9b2sk5aahc1dm4hbz8ajv2c68yhnfnwch5")))

(define-public crate-libw-0.1.4 (c (n "libw") (v "0.1.4") (d (list (d (n "wasi") (r "^0.9.0") (d #t) (k 0)))) (h "1kpczm2b7qyawkzqxcmx6ixrhkfw2mdl1rrv59ql4x3plqkvy55m")))

(define-public crate-libw-0.1.5 (c (n "libw") (v "0.1.5") (d (list (d (n "wasi") (r "^0.9.0") (d #t) (k 0)))) (h "1zyrj609r72mqk8hpm0aasi2shsb8iklmy7y6g4221kz6jfzg9cl")))

(define-public crate-libw-0.1.6 (c (n "libw") (v "0.1.6") (d (list (d (n "wasi") (r "^0.9.0") (d #t) (k 0)))) (h "000lgi2fglqgipfyb20cvy74iqccmggznxrv4wnig15l1y6rdvwh")))

