(define-module (crates-io li bw libwren-sys) #:use-module (crates-io))

(define-public crate-libwren-sys-0.1.0 (c (n "libwren-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "13sfx0d4vmpw79mzhhm5rcckqfdjscmh3h8pazg3am68wsa5j5p5")))

