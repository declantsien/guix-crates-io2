(define-module (crates-io li bw libwispha) #:use-module (crates-io))

(define-public crate-libwispha-0.1.0 (c (n "libwispha") (v "0.1.0") (d (list (d (n "maplit") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0p652q9hypc64di2v8pfp2k0j4b0syld5azrjp29qy8vdm1h8q6s")))

