(define-module (crates-io li bw libwebp) #:use-module (crates-io))

(define-public crate-libwebp-0.1.0 (c (n "libwebp") (v "0.1.0") (d (list (d (n "libwebp-sys2") (r "^0.1.0") (d #t) (k 0)))) (h "11k0psxgm86nbdd2ffhiws9iwf6s3l2z2nk2vb482vyi5ks5h36s") (f (quote (("static" "libwebp-sys2/static") ("mux" "libwebp-sys2/mux") ("extern-types" "libwebp-sys2/extern-types") ("demux" "libwebp-sys2/demux") ("default") ("__doc_cfg" "1_1" "demux" "mux") ("1_1" "libwebp-sys2/1_1" "0_6") ("0_6" "libwebp-sys2/0_6" "0_5") ("0_5" "libwebp-sys2/0_5"))))))

(define-public crate-libwebp-0.1.1 (c (n "libwebp") (v "0.1.1") (d (list (d (n "libwebp-sys2") (r "^0.1.0") (d #t) (k 0)))) (h "02miaark83ira8abij2dbfa0ilx43xy5q2lslcnwyndvpw1k8lrp") (f (quote (("static" "libwebp-sys2/static") ("mux" "libwebp-sys2/mux") ("extern-types" "libwebp-sys2/extern-types") ("demux" "libwebp-sys2/demux") ("default") ("__doc_cfg" "1_1" "demux" "mux") ("1_1" "libwebp-sys2/1_1" "0_6") ("0_6" "libwebp-sys2/0_6" "0_5") ("0_5" "libwebp-sys2/0_5"))))))

(define-public crate-libwebp-0.1.2 (c (n "libwebp") (v "0.1.2") (d (list (d (n "libwebp-sys2") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 2)))) (h "1x3l3b7w455jdb5wn75rfgfjqbcp3wil45pyv4l2ansvz612lyf7") (f (quote (("static" "libwebp-sys2/static") ("mux" "libwebp-sys2/mux") ("extern-types" "libwebp-sys2/extern-types") ("demux" "libwebp-sys2/demux") ("default") ("__doc_cfg" "1_1" "demux" "mux") ("1_1" "libwebp-sys2/1_1" "0_6") ("0_6" "libwebp-sys2/0_6" "0_5") ("0_5" "libwebp-sys2/0_5"))))))

