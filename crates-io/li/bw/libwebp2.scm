(define-module (crates-io li bw libwebp2) #:use-module (crates-io))

(define-public crate-libwebp2-0.0.1 (c (n "libwebp2") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 1)))) (h "046pmh1qpq5zvjk8v0gysqn8pjh72m185zgvaram7d1zyd8gjc3i") (y #t)))

(define-public crate-libwebp2-0.0.2 (c (n "libwebp2") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 1)))) (h "13si5ksp7hg1vgpgchkng5jddrj3czxl6p14ac826m71sk554r44") (f (quote (("download")))) (y #t)))

(define-public crate-libwebp2-0.0.3 (c (n "libwebp2") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)) (d (n "zip") (r "^0.6") (d #t) (k 1)))) (h "018yn2wm38rsyxhrd8zdfwhv6541d8ayy4jn1dvx066afzjas0vh") (f (quote (("download")))) (y #t)))

(define-public crate-libwebp2-0.1.0 (c (n "libwebp2") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls" "blocking"))) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)) (d (n "zip") (r "^0.6") (d #t) (k 1)))) (h "0kymv3hmq1l94bdrm4bgmhnil6sljdq4maw03yb0kh5059hh4ywc") (f (quote (("download")))) (y #t)))

