(define-module (crates-io li bw libwebp-image) #:use-module (crates-io))

(define-public crate-libwebp-image-0.1.0 (c (n "libwebp-image") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "libwebp") (r "^0.1.0") (d #t) (k 0)))) (h "1kfjdw505vq60xv8w9ddm0mn5fvay5s6bircj8zq4n6smwnrbhdz") (f (quote (("libwebp-1_1" "libwebp/1_1" "libwebp-0_6") ("libwebp-0_6" "libwebp/0_6" "libwebp-0_5") ("libwebp-0_5" "libwebp/0_5") ("default"))))))

(define-public crate-libwebp-image-0.1.1 (c (n "libwebp-image") (v "0.1.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "image") (r "^0.23.12") (d #t) (k 0)) (d (n "libwebp") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1cj0ig256lirmxinnfbzda52ks91an7r5vsbf8xv80qr2fxbjp1n") (f (quote (("libwebp-1_1" "libwebp/1_1" "libwebp-0_6") ("libwebp-0_6" "libwebp/0_6" "libwebp-0_5") ("libwebp-0_5" "libwebp/0_5") ("default"))))))

(define-public crate-libwebp-image-0.2.0 (c (n "libwebp-image") (v "0.2.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "image") (r "^0.23.12") (k 0)) (d (n "libwebp") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1qzkwhczn5rz0gv7wn9k0s96glyd2h5pysq638g1ii814i68f27p") (f (quote (("libwebp-1_1" "libwebp/1_1" "libwebp-0_6") ("libwebp-0_6" "libwebp/0_6" "libwebp-0_5") ("libwebp-0_5" "libwebp/0_5") ("default"))))))

(define-public crate-libwebp-image-0.3.0 (c (n "libwebp-image") (v "0.3.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "image") (r "^0.24.0") (k 0)) (d (n "libwebp") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0bzp3fpdgfn9qn64v34l3dyr0pqyd7cjcx6yaz019pf3yzbalbgp") (f (quote (("libwebp-1_1" "libwebp/1_1" "libwebp-0_6") ("libwebp-0_6" "libwebp/0_6" "libwebp-0_5") ("libwebp-0_5" "libwebp/0_5") ("default")))) (r "1.56")))

