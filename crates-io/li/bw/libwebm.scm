(define-module (crates-io li bw libwebm) #:use-module (crates-io))

(define-public crate-libwebm-0.1.0 (c (n "libwebm") (v "0.1.0") (d (list (d (n "enum-primitive-derive") (r "^0.1.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 0)))) (h "1c3f6k0wz1rvknyh3lwpkhg1qj82d3g4zr8y1945agg9xay0mxl4")))

