(define-module (crates-io li bw libwgetj) #:use-module (crates-io))

(define-public crate-libwgetj-0.1.0 (c (n "libwgetj") (v "0.1.0") (d (list (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)))) (h "0ixyd81xv4004zwi91bgx4h1ndqp8wnd2c887cxl1xzwn90ii544")))

(define-public crate-libwgetj-0.1.1 (c (n "libwgetj") (v "0.1.1") (d (list (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 1)))) (h "1jb3rmicy02laffjvywq2i1z2nb1ahb3zj44m6nx9k8iffv1sm9k")))

(define-public crate-libwgetj-0.1.2 (c (n "libwgetj") (v "0.1.2") (d (list (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "vergen") (r "*") (d #t) (k 1)))) (h "1vn6wryz6nl44v5xhyrrllliykkgpkp8rgmxsps83nx9852awpvn")))

(define-public crate-libwgetj-0.1.3 (c (n "libwgetj") (v "0.1.3") (d (list (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "vergen") (r "*") (d #t) (k 1)))) (h "0g0g3m0l55fbq4jbmd8k05vdk11wwjvkh1y2ms7zn9r0nb3dv2yq")))

(define-public crate-libwgetj-0.1.4 (c (n "libwgetj") (v "0.1.4") (d (list (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "vergen") (r "*") (d #t) (k 1)))) (h "1ldyxy1hwnq4v7xgi3cmrq35lkwiq77a7nizgvyngqr60lvngyid")))

(define-public crate-libwgetj-0.1.5 (c (n "libwgetj") (v "0.1.5") (d (list (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "vergen") (r "*") (d #t) (k 1)))) (h "1q84l5jihyhs1cizxbx2gdq7qjx49jdf6zdx617czvcl5q0mwwn1")))

(define-public crate-libwgetj-0.1.6 (c (n "libwgetj") (v "0.1.6") (d (list (d (n "commandext") (r "*") (d #t) (k 0)) (d (n "vergen") (r "*") (d #t) (k 1)))) (h "051yvss7ks1n03p6l15avyz0gcp935cgvm682rf954j0r9y9z5dl")))

(define-public crate-libwgetj-0.1.7 (c (n "libwgetj") (v "0.1.7") (d (list (d (n "clippy") (r "~0.0.30") (o #t) (d #t) (k 0)) (d (n "commandext") (r "~0.0.7") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "0lqf3adhgn7bzisg0qbraa9iv5a38wdga1bkkdrhxn5473qwb5r8") (f (quote (("default"))))))

(define-public crate-libwgetj-0.1.8 (c (n "libwgetj") (v "0.1.8") (d (list (d (n "clippy") (r "~0.0.30") (o #t) (d #t) (k 0)) (d (n "commandext") (r "~0.0.7") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "0my54zbvif071qcd8ca9b8pcjckn8hr2x2fzdbmdf6vn6d5qjsqw") (f (quote (("default"))))))

(define-public crate-libwgetj-0.1.10 (c (n "libwgetj") (v "0.1.10") (d (list (d (n "clippy") (r "~0.0.30") (o #t) (d #t) (k 0)) (d (n "commandext") (r "~0.1.0") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "159r001nsk8swrbg13p272c5gmf80c4r5wa40sin9dniidznf449") (f (quote (("default"))))))

(define-public crate-libwgetj-0.1.11 (c (n "libwgetj") (v "0.1.11") (d (list (d (n "clippy") (r "~0.0.30") (o #t) (d #t) (k 0)) (d (n "commandext") (r "~0.1.0") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "0lrp5h952qykqkdrzyks0m7p2nk4fwl4gv44xyaa9hbwbqd06j77") (f (quote (("default"))))))

(define-public crate-libwgetj-1.0.0 (c (n "libwgetj") (v "1.0.0") (d (list (d (n "clippy") (r "~0.0.30") (o #t) (d #t) (k 0)) (d (n "commandext") (r "~0.1.0") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "1gipkq55ihdpzijfhmcn9aszfvyfjn0mh2vkpx0fdxqm7477p46s") (f (quote (("default"))))))

(define-public crate-libwgetj-1.0.1 (c (n "libwgetj") (v "1.0.1") (d (list (d (n "clippy") (r "~0.0.30") (o #t) (d #t) (k 0)) (d (n "commandext") (r "~0.1.0") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "1pmq1drmv587cg691qdn3hkyp3fbp26s43rsvlb0vxb5gdn22c1w") (f (quote (("default"))))))

(define-public crate-libwgetj-1.0.2 (c (n "libwgetj") (v "1.0.2") (d (list (d (n "clippy") (r "~0.0") (o #t) (d #t) (k 0)) (d (n "commandext") (r "~0.1.0") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "1crr6ihsl24grrgpqd1yj3h9z6mmzvqw3q9p48dvycvkcsvqidxr") (f (quote (("lint" "clippy") ("default"))))))

(define-public crate-libwgetj-1.0.3 (c (n "libwgetj") (v "1.0.3") (d (list (d (n "clippy") (r "~0.0") (o #t) (d #t) (k 0)) (d (n "commandext") (r "~0.1.0") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "0scw2y0ybmdm9x6i77zz65y0z186d8yb38k6yxcbhww469l99pbh") (f (quote (("lint" "clippy") ("default"))))))

(define-public crate-libwgetj-1.0.4 (c (n "libwgetj") (v "1.0.4") (d (list (d (n "clippy") (r "~0.0") (o #t) (d #t) (k 0)) (d (n "commandext") (r "~0.1.0") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "15kzk2582y05cag03ly8aw7gwjdnwwgabc52jjlwxfhap6j38aws") (f (quote (("lint" "clippy") ("default"))))))

(define-public crate-libwgetj-1.0.5 (c (n "libwgetj") (v "1.0.5") (d (list (d (n "clippy") (r "~0.0") (o #t) (d #t) (k 0)) (d (n "commandext") (r "~0.1.0") (d #t) (k 0)) (d (n "log") (r "~0.3.4") (d #t) (k 0)) (d (n "vergen") (r "~0.1.0") (d #t) (k 1)))) (h "071cgmkqim14py5vkyb8ay8fdn1ns2skxfy10fdnvmk7znifz2sm") (f (quote (("lint" "clippy") ("default"))))))

