(define-module (crates-io li bw libwifi_macros) #:use-module (crates-io))

(define-public crate-libwifi_macros-0.0.1 (c (n "libwifi_macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "15bim1qysyqv772cf050ax8s0cjlmimvjl9v6gf1f0bwgswacp0l")))

(define-public crate-libwifi_macros-0.0.2 (c (n "libwifi_macros") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "0jbw35cy6vrpfb1yxx0wp66fpavinn5c7flm84ipd6m31csgjp1z")))

