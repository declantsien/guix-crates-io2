(define-module (crates-io li bw libwmctl) #:use-module (crates-io))

(define-public crate-libwmctl-0.0.39 (c (n "libwmctl") (v "0.0.39") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "x11rb") (r "^0.9.0") (d #t) (k 0)))) (h "068i6vii3a26g6k867drh8nyqfgm770zsrrx6ckv9p6rklir2rbh")))

(define-public crate-libwmctl-0.0.40 (c (n "libwmctl") (v "0.0.40") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "x11rb") (r "^0.9.0") (d #t) (k 0)))) (h "04apgvqdzjbqir29fwxn668x9r0lnvy24fg5aywiwyjyxdk6j8yq")))

(define-public crate-libwmctl-0.0.45 (c (n "libwmctl") (v "0.0.45") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "x11rb") (r "^0.9.0") (d #t) (k 0)))) (h "0n4685hm38r6ibkncgn6239ggcnpxm22h5617afj6lsllylp1p1a")))

