(define-module (crates-io li bw libwhen) #:use-module (crates-io))

(define-public crate-libwhen-0.4.0 (c (n "libwhen") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "chrono-tz") (r "^0.6.1") (d #t) (k 0)) (d (n "localzone") (r "^0.2.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l5qs7dsx0rk3zk5fd2irbsxfzqr2zs56rp87129pn1rslckaygp")))

