(define-module (crates-io li bw libwmata) #:use-module (crates-io))

(define-public crate-libwmata-0.1.0 (c (n "libwmata") (v "0.1.0") (d (list (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "059h9zpxvh8wx48vji457ywz17xhnggwqs9n0chqzwwy1h0kmc6v")))

(define-public crate-libwmata-0.1.1 (c (n "libwmata") (v "0.1.1") (d (list (d (n "phf") (r "^0.8.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.9.24") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (d #t) (k 0)))) (h "07jm93nmadghx8p5r97n9345sqa935jjzwyvx02glkrah0acvzna")))

