(define-module (crates-io li bw libwavm-sys) #:use-module (crates-io))

(define-public crate-libwavm-sys-0.1.0 (c (n "libwavm-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "070265wwqiv8a4v15lg0y5d6pf8rfsd4kjlw1djmxi985icw9g0k") (l "wavm")))

(define-public crate-libwavm-sys-0.1.1 (c (n "libwavm-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "1aa9q01lc6am06bhx6k1x1ds6j7lr3mzys1153my5abpbqbxyp7p") (l "WAVM")))

(define-public crate-libwavm-sys-0.1.2 (c (n "libwavm-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "0daybyh5gzzc1v9v6dldgm02px1vfms1apj5ibsy2rzn5jb41173") (l "WAVM")))

(define-public crate-libwavm-sys-0.1.3 (c (n "libwavm-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "1035nf95afd924p27b8rcirn4fqbjzgmi2h7h4d8r1q4sj9w6mng") (l "WAVM")))

(define-public crate-libwavm-sys-0.1.4 (c (n "libwavm-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.126") (d #t) (k 0)))) (h "1s9vqf25rwk8ajyj0y3xfan952nr5xvxynfkry813aw2l5k9b42l") (l "WAVM")))

