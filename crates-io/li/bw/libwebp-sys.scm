(define-module (crates-io li bw libwebp-sys) #:use-module (crates-io))

(define-public crate-libwebp-sys-0.1.0 (c (n "libwebp-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1phhlyz86qx7z32bq4mmkpdlxr79yyimwnc94i17xs8naky9hwn6")))

(define-public crate-libwebp-sys-0.2.0 (c (n "libwebp-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "1xif4zc8s8m808vswryi84v3b04n1myd54grss7s4dcbfdjc0w1y") (f (quote (("parallel" "cc/parallel") ("default" "parallel"))))))

(define-public crate-libwebp-sys-0.3.0 (c (n "libwebp-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "05m8qw34zfj35rlfd0dkwa12xpa9yi6jirbf7ix4mxpq579h76xk") (f (quote (("parallel" "cc/parallel") ("default" "parallel"))))))

(define-public crate-libwebp-sys-0.3.2 (c (n "libwebp-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "0adk6yjnidw49g14lc0nan1qv4a88n4hi74h3l8gamhs5937m8nl") (f (quote (("parallel" "cc/parallel") ("default" "parallel"))))))

(define-public crate-libwebp-sys-0.4.0 (c (n "libwebp-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "0vc16gxpq4wvl9yfiz1plypl69djrcm1rgpwfx7a5nnybwd5r3va") (f (quote (("sse41") ("parallel" "cc/parallel") ("neon") ("default" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.4.1 (c (n "libwebp-sys") (v "0.4.1") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "0mg4xsv8jmfdm14w8qh21vl5cs3vlcdxg3ycy1sy7cvly9hkaasi") (f (quote (("sse41") ("parallel" "cc/parallel") ("neon") ("default" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.4.2 (c (n "libwebp-sys") (v "0.4.2") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "1gvjaqhjpzdskx8x4q1lfgw24jnbjgkx4s6dxpkkg2d2ba4d37s3") (f (quote (("sse41") ("parallel" "cc/parallel") ("neon") ("default" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.5.0 (c (n "libwebp-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "1ymalbk544c64wbn07rsfmqvv72zzqxcbgcnfykgdr1qsw3jynlh") (f (quote (("sse41") ("parallel" "cc/parallel") ("neon") ("default" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.6.0 (c (n "libwebp-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.25") (d #t) (k 1)))) (h "0i9h7mrfz5iyh3rvkwakd5k35m893wvkd3gy9d4g9g0ysh34rx2a") (f (quote (("sse41") ("parallel" "cc/parallel") ("neon") ("default" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.7.0 (c (n "libwebp-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "15l4cmvkyy6ssa0akmn44ikh0g2gpzcnw0ybjxr0pqzp65rr23nw") (f (quote (("sse41") ("parallel" "cc/parallel") ("neon") ("default" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.7.1 (c (n "libwebp-sys") (v "0.7.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "0psnxl5l2dj1hyr5a0dgv59jl000gj5dcbf3x90gmyyhqxnir446") (f (quote (("sse41") ("parallel" "cc/parallel") ("neon") ("default" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.8.0 (c (n "libwebp-sys") (v "0.8.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "1q9xr5ad3drg22h41qgh56q5sg32nr2m0kzckl9dkszmhm65lcyy") (f (quote (("sse41") ("parallel" "cc/parallel") ("neon") ("default" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.9.0 (c (n "libwebp-sys") (v "0.9.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "0f6nh9f9q01q48cplm7hdfrp50q10i6m5zryj4fnbilk4qlm6p62") (f (quote (("std") ("sse41") ("parallel" "cc/parallel") ("neon") ("default" "std" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.9.1 (c (n "libwebp-sys") (v "0.9.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "0n4f82pkd0aljv48bcfzwabkzmmjd1vi8nbfqwwbblan8nypjajw") (f (quote (("std") ("sse41") ("parallel" "cc/parallel") ("neon") ("default" "std" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.9.2 (c (n "libwebp-sys") (v "0.9.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "08yqb4vd9rrx53i3xswygxp723k19vvwqr11m9c01vxcy1v1xpx5") (f (quote (("std") ("sse41") ("parallel" "cc/parallel") ("neon") ("default" "std" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.9.3 (c (n "libwebp-sys") (v "0.9.3") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "03v3mwzm10w8ha429230m99dr0x1gy788yd71zmfv7pclf7b150c") (f (quote (("std") ("sse41") ("parallel" "cc/parallel") ("neon") ("default" "std" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.9.4 (c (n "libwebp-sys") (v "0.9.4") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "0g7fr2753gkjk9wr2qk4bwlf6bhss8jdfp1kwsp54ka4z6hg039y") (f (quote (("std") ("sse41") ("parallel" "cc/parallel") ("neon") ("default" "std" "parallel" "neon") ("avx2"))))))

(define-public crate-libwebp-sys-0.9.5 (c (n "libwebp-sys") (v "0.9.5") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (k 1)))) (h "0f582b0fipsa5jxb2kkyv3dr7pl80zz43j5srhmnvv9i9xh6p6w2") (f (quote (("std") ("sse41") ("parallel" "cc/parallel") ("neon") ("default" "std" "parallel" "neon") ("avx2"))))))

