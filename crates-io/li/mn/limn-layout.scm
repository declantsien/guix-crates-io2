(define-module (crates-io li mn limn-layout) #:use-module (crates-io))

(define-public crate-limn-layout-0.0.1 (c (n "limn-layout") (v "0.0.1") (d (list (d (n "cassowary") (r "^0.3.0") (d #t) (k 0)) (d (n "euclid") (r "^0.15.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "maplit") (r "^0.1.4") (d #t) (k 2)))) (h "11fqcaxjvn6gx7b39b4pmyx9xawq4yiqlagzqfs046m4mw8l60f2")))

