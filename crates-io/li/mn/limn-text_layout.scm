(define-module (crates-io li mn limn-text_layout) #:use-module (crates-io))

(define-public crate-limn-text_layout-0.0.1 (c (n "limn-text_layout") (v "0.0.1") (d (list (d (n "euclid") (r "^0.15.1") (d #t) (k 0)) (d (n "rusttype") (r "^0.2.1") (d #t) (k 0)))) (h "1dxqvhga8vpbdyzz2vh6y5wwalh5vqy5hpfspqgcn90xcra4p1j4")))

