(define-module (crates-io li bb libblackbody) #:use-module (crates-io))

(define-public crate-libblackbody-0.1.0 (c (n "libblackbody") (v "0.1.0") (d (list (d (n "flyr") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "tiff") (r "^0.5.0") (d #t) (k 0)))) (h "0rryxvb6m1wr43lcd71rmbm26l16p1ar6vmwp11c1amz5id881g0")))

(define-public crate-libblackbody-0.2.0 (c (n "libblackbody") (v "0.2.0") (d (list (d (n "flyr") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "tiff") (r "^0.5.0") (d #t) (k 0)))) (h "0gpi4wbxy814n6zlnnb52l5hvwjsx5zwn3m8zjnwcgxvcgi42nlc")))

(define-public crate-libblackbody-0.3.0 (c (n "libblackbody") (v "0.3.0") (d (list (d (n "flyr") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "tiff") (r "^0.5.0") (d #t) (k 0)))) (h "0q46cv5p48z0mr9vqhymvgyzlsza55pcxfqzbjd594cjraq1k11l")))

(define-public crate-libblackbody-0.3.1 (c (n "libblackbody") (v "0.3.1") (d (list (d (n "flyr") (r "^0.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "tiff") (r "^0.5.0") (d #t) (k 0)))) (h "0pydrd67kil5rldrgn1ys5hghwk7mx98giddx98zy72qnwrb6phb")))

(define-public crate-libblackbody-0.4.0 (c (n "libblackbody") (v "0.4.0") (d (list (d (n "flyr") (r "^0.4.0") (d #t) (k 0)) (d (n "image") (r "^0.23.1") (f (quote ("png" "tiff"))) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "tiff") (r "^0.5.0") (d #t) (k 0)))) (h "0f8ai85a0s463jqnci48q8qhyij2g5zmjdaws8ri7brxjh401rcb")))

(define-public crate-libblackbody-0.5.0 (c (n "libblackbody") (v "0.5.0") (d (list (d (n "flyr") (r "^0.4.0") (d #t) (k 0)) (d (n "image") (r "^0.23.1") (f (quote ("png"))) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "tiff") (r "^0.6.0") (d #t) (k 0)))) (h "1pq7s99250l0h0n5d4jb5ka714wqq0ri850c9k9ihmnqkilb8ymr")))

