(define-module (crates-io li bb libbfio-sys) #:use-module (crates-io))

(define-public crate-libbfio-sys-0.2.0 (c (n "libbfio-sys") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.0") (d #t) (k 1)))) (h "1hcwgdv209mrh2kji8vmn4vyi65j700cdsjvy4s2a4has5pyxpjr") (f (quote (("dynamic_link") ("default")))) (l "libbfio")))

(define-public crate-libbfio-sys-0.2.1 (c (n "libbfio-sys") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.1") (d #t) (k 1)))) (h "0yrm9m3vgcgscg33sagyjsc5187c2gddqchfl85flnpwl2jp7xi8") (f (quote (("dynamic_link") ("default"))))))

(define-public crate-libbfio-sys-0.2.2 (c (n "libbfio-sys") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.1") (d #t) (k 1)))) (h "0ixgr0a7v7anyq3d72fiin8gsbhlkk4scs2xmyk2lcngp521ymi8") (f (quote (("dynamic_link") ("default")))) (y #t)))

(define-public crate-libbfio-sys-0.2.3 (c (n "libbfio-sys") (v "0.2.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.1") (d #t) (k 1)))) (h "0wdbcgbl2m6ma9d6v74x0ywkj3h0dszllvrf0qpajr0fx0bh36dj") (f (quote (("dynamic_link") ("default"))))))

(define-public crate-libbfio-sys-0.2.4 (c (n "libbfio-sys") (v "0.2.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.4") (d #t) (k 1)))) (h "0w8vyni9g6dll63jfnxz3bqar3yr148rzk80s1lxw2cjns9dv600") (f (quote (("dynamic_link") ("default"))))))

(define-public crate-libbfio-sys-0.2.5 (c (n "libbfio-sys") (v "0.2.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.5") (d #t) (k 1)))) (h "1y8zajfr5ilw4rb904yj50p8wpwamsi2c3w70av37a7m9cas23dz") (f (quote (("dynamic_link") ("default"))))))

