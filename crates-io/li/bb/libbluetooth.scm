(define-module (crates-io li bb libbluetooth) #:use-module (crates-io))

(define-public crate-libbluetooth-0.1.0 (c (n "libbluetooth") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.13.0") (d #t) (k 0)))) (h "1gr8a4jfljswg6v9kkcbdli8lfkjzwxjwxz8lj32a5086n5nl7ji") (f (quote (("impl-default") ("impl-debug") ("debug" "impl-debug"))))))

