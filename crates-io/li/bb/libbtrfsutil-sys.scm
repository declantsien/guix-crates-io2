(define-module (crates-io li bb libbtrfsutil-sys) #:use-module (crates-io))

(define-public crate-libbtrfsutil-sys-0.1.0 (c (n "libbtrfsutil-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (f (quote ("logging" "runtime" "which-rustfmt"))) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08hf2zv8dr350nk8gfxr68zykwhimryjsbyfv7xjgg0v654saba4") (l "btrfsutil")))

(define-public crate-libbtrfsutil-sys-0.2.0 (c (n "libbtrfsutil-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("logging" "runtime" "which-rustfmt"))) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1z5n3g2q60aj4ha1v2xlp2qvgpb63jx3qnz172m3zadcjl6qciaa") (l "btrfsutil")))

(define-public crate-libbtrfsutil-sys-0.2.1 (c (n "libbtrfsutil-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("logging" "runtime" "which-rustfmt"))) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ckdmj6imvimy4y42v7myl7fwifhdldqw3941cgpwn7bg981s926") (l "btrfsutil")))

