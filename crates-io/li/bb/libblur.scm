(define-module (crates-io li bb libblur) #:use-module (crates-io))

(define-public crate-libblur-0.9.0 (c (n "libblur") (v "0.9.0") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "0dv648dwkhhkkyhv8bkwhp61gg22zszgn64wipi2w6cbzzzz59xj")))

(define-public crate-libblur-0.9.1 (c (n "libblur") (v "0.9.1") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "11pjnm31mbsgls1c8y9iy7fy3f7a2hz21c1324zmf8ry9k6ghzjj")))

(define-public crate-libblur-0.9.2 (c (n "libblur") (v "0.9.2") (d (list (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "17pzxjqh7spx1bqd2zmqijs20gczlb4k99hgb83xjxx1356zqmyb")))

(define-public crate-libblur-0.9.3 (c (n "libblur") (v "0.9.3") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "122120sxccikf6mrj6fm9i8v5n4jdq465h4kyacf4pkm1pk4zffa")))

(define-public crate-libblur-0.9.4 (c (n "libblur") (v "0.9.4") (d (list (d (n "half") (r "^2.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "0cy1h70kfz5vvhykb89gv01ws273z0hpnyn7nvj2r081sz7kaqja")))

