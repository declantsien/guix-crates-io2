(define-module (crates-io li bb libbgmrank) #:use-module (crates-io))

(define-public crate-libbgmrank-0.0.1 (c (n "libbgmrank") (v "0.0.1") (d (list (d (n "enum-set") (r "^0.0.6") (d #t) (k 0)) (d (n "html5ever-atoms") (r "^0.1.2") (d #t) (k 0)) (d (n "kuchiki") (r "^0.4") (f (quote ("hyper"))) (d #t) (k 0)) (d (n "selectors") (r "^0.14") (d #t) (k 0)))) (h "1jc8d43k4yd8l731ivimzpdzyxvllbgwfrbr01hy9zpsaf7c2c8j") (y #t)))

