(define-module (crates-io li bb libbf) #:use-module (crates-io))

(define-public crate-libbf-0.1.0 (c (n "libbf") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03cz2l9z0jcjg3cz2mspkd8arfh0fgz1wvck9mrmnbjn37drryvv") (f (quote (("predefined" "bf" "ook") ("ook") ("default") ("bf") ("all" "predefined"))))))

(define-public crate-libbf-0.1.1 (c (n "libbf") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vil9ygiva0qn7qr02ll67fzra6l63ay81dyp59x28d98lni1v7h") (f (quote (("predefined" "bf" "ook") ("ook") ("default") ("bf") ("all" "predefined"))))))

(define-public crate-libbf-0.1.2 (c (n "libbf") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0ppm653514424yyq58ij6fxrk1zrw91hn0ylwnl5y8r88g4wy81p") (f (quote (("predefined" "bf" "ook") ("ook") ("default") ("bf") ("all" "predefined"))))))

(define-public crate-libbf-0.1.3 (c (n "libbf") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0iijx4q508b9zl4pzccy6sxij38bx875xz3ir85adrzhsxpxnhkv") (f (quote (("predefined" "bf" "ook") ("ook") ("default") ("bf") ("all" "predefined"))))))

