(define-module (crates-io li bb libblkid-rs-sys) #:use-module (crates-io))

(define-public crate-libblkid-rs-sys-0.1.0 (c (n "libblkid-rs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "1rgnli6abcrhd3fzv3bd4alpp0xbri5p0dx4pfxbw0vhylbbp35m")))

(define-public crate-libblkid-rs-sys-0.1.1 (c (n "libblkid-rs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "11z0iqlnqlcm83z1h8cnfsfp11pqmbpiggv2j7vw4r1c0s2nvbap")))

(define-public crate-libblkid-rs-sys-0.1.2 (c (n "libblkid-rs-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "1zl856lwhlasax9q79j6dc5kk1gfc3ag24xs19xcc9rg4q7jbm17")))

(define-public crate-libblkid-rs-sys-0.1.3 (c (n "libblkid-rs-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "0fglk4rgv3kdkfbr4aq7hidbhpadms26ky95mv3360wyzws5qqhy")))

(define-public crate-libblkid-rs-sys-0.1.4 (c (n "libblkid-rs-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.59.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "15kgnbvpkh2dijlb6hwsdrr0s1xp3xkiy1a3q29b8mcw14gw6x40")))

(define-public crate-libblkid-rs-sys-0.1.5 (c (n "libblkid-rs-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "1hss67ks8szb443ynd8idygfiny9fav0yr6qndr5d3x2sqi27vjj") (r "1.66.1")))

(define-public crate-libblkid-rs-sys-0.2.0 (c (n "libblkid-rs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "0fp272m3h564kn2cli2h33p0hmml6zqsyggwn8zjdbrggc36hc0n") (r "1.71.1")))

(define-public crate-libblkid-rs-sys-0.3.0 (c (n "libblkid-rs-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "1n3800677w96p0dxnrg9a759zwhry3g1fs0jqlwlsxhxrkxa9qnq") (r "1.71.1")))

