(define-module (crates-io li bb libbytestat) #:use-module (crates-io))

(define-public crate-libbytestat-0.0.1 (c (n "libbytestat") (v "0.0.1") (h "04h3s9r3hp2xhaqjxq4dfj90bb9ywqgpmgq9dvs3xdv7yxpz8v60")))

(define-public crate-libbytestat-0.0.2 (c (n "libbytestat") (v "0.0.2") (h "1l86dgqdaibfabbl207n1fd2rmw8a6zmgapxhhw22rr7isdj716y")))

(define-public crate-libbytestat-0.0.3 (c (n "libbytestat") (v "0.0.3") (h "0cy5mm25iczy6p83wnfyx4bmyg3xval9g51iaibg0p4l2ckz2ci3")))

