(define-module (crates-io li bb libbaz-test) #:use-module (crates-io))

(define-public crate-libbaz-test-0.1.0 (c (n "libbaz-test") (v "0.1.0") (h "0k3myljp1sl1fxz2iarkdjss4kji69d3sv48cnx3ajzz1r0m7g02") (y #t)))

(define-public crate-libbaz-test-0.2.0 (c (n "libbaz-test") (v "0.2.0") (h "05cq57hnl00cwa67iwqxra8rm99wy8hfjbyg8jnln1nk4sc2zv18")))

