(define-module (crates-io li bb libbpf-sys) #:use-module (crates-io))

(define-public crate-libbpf-sys-0.0.0 (c (n "libbpf-sys") (v "0.0.0") (h "0qdafi820719dcnfjkggyrkdrj5pvx19d92crfjfc3ax5xzpb90l") (y #t)))

(define-public crate-libbpf-sys-0.0.6-1 (c (n "libbpf-sys") (v "0.0.6-1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)))) (h "1wi07zqyyj7i5m8bgr526xkw9ngfq6zmaf678m6aigcj4cy7z3da") (y #t) (l "bpf")))

(define-public crate-libbpf-sys-0.0.6-2 (c (n "libbpf-sys") (v "0.0.6-2") (h "1rvihh8b3y8ay82vazg72q5z2lra5f7agkznfr472mp8yfs229yz") (y #t) (l "bpf")))

(define-public crate-libbpf-sys-0.0.6-3 (c (n "libbpf-sys") (v "0.0.6-3") (h "1d8n69jwr0af860i44yw7bhk7882cgmz9licsrh2s8797yg1q6yz")))

(define-public crate-libbpf-sys-0.0.6-4 (c (n "libbpf-sys") (v "0.0.6-4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1h2vk5rphd8b9jcfw6g528h04ygspjppra9p1lkgvxsnh4yf2vid")))

(define-public crate-libbpf-sys-0.0.6-5 (c (n "libbpf-sys") (v "0.0.6-5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1q70fj47j00lqrpdkwwacs5sscbl5w4spm90b2bl8g0xxh4msp0f")))

(define-public crate-libbpf-sys-0.0.6-6 (c (n "libbpf-sys") (v "0.0.6-6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "00hbf8ibyzz5dvxi3agwhcg61b61a337kidjz2k1aki7kby842k5")))

(define-public crate-libbpf-sys-0.0.6-7 (c (n "libbpf-sys") (v "0.0.6-7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1swdsfsxphl925jsg5x75jc1y5p3q2bjj8dmlxcwjc3a43ssvbhw")))

(define-public crate-libbpf-sys-0.0.6-8 (c (n "libbpf-sys") (v "0.0.6-8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0wm0m2n0g9643hxif34f4cdjza8mqg3nnszl74fcmhfffw8v2h82")))

(define-public crate-libbpf-sys-0.0.7-1 (c (n "libbpf-sys") (v "0.0.7-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0w9qwh7jdh57la2fdq3n6bmi531zvmqvv20g9syyvxb98srpsz6y")))

(define-public crate-libbpf-sys-0.0.8-1 (c (n "libbpf-sys") (v "0.0.8-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0y4ib0qx2k78hfnp3alziz89zx6p4xxhsksc49q9ysykpchgv8a9")))

(define-public crate-libbpf-sys-0.0.8-2 (c (n "libbpf-sys") (v "0.0.8-2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1a9j38jxllr2zdp2mi7anyasn7ry0z0222xk9w7zl2klzl1mgvf5")))

(define-public crate-libbpf-sys-0.0.8-3 (c (n "libbpf-sys") (v "0.0.8-3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1kmnjymvldmisz0z7phr65n1fnw1w2f9brghsf792d6y8nkjwfjj")))

(define-public crate-libbpf-sys-0.0.9-1 (c (n "libbpf-sys") (v "0.0.9-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0sp1nkh0q6w535maxbivpj54z4rq2fvxgbgpwjmrij27fl59z1b0")))

(define-public crate-libbpf-sys-0.0.9-2 (c (n "libbpf-sys") (v "0.0.9-2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1gzp97s6186fr548ixb6nsjs35qiswgz68yc53pkfa8aisy7dc7v") (l "libbpf")))

(define-public crate-libbpf-sys-0.1.0-1 (c (n "libbpf-sys") (v "0.1.0-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "09mvygzh729g5hpfviskw9cgx3xaw5lw8v6v601547xijz5afx64") (l "libbpf")))

(define-public crate-libbpf-sys-0.2.0-1 (c (n "libbpf-sys") (v "0.2.0-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "083aa707gj1x59d5gy4vlj7ypnplpq9yq3am89blwfwnzfd2bibp") (y #t) (l "libbpf")))

(define-public crate-libbpf-sys-0.2.0-2 (c (n "libbpf-sys") (v "0.2.0-2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "182b8jcgd505yy882563mfyz00w0jvci81af5bs1zxibq9rsr2bj") (l "libbpf")))

(define-public crate-libbpf-sys-0.2.0-3 (c (n "libbpf-sys") (v "0.2.0-3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "056bs7qfz5bpxg78s6rzinw9i2n19rggc7k0bqbc0r8l73baw4z4") (l "libbpf")))

(define-public crate-libbpf-sys-0.3.0-1 (c (n "libbpf-sys") (v "0.3.0-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0s9rqwlj90x0nvapaj9ckzyxwys70l4z8cd08ds5ffgb1p4pxd9w") (l "libbpf")))

(define-public crate-libbpf-sys-0.3.0-2 (c (n "libbpf-sys") (v "0.3.0-2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1zibmp7k0ahcc0z2br96sm0sr7raf1dlxvxkysxnxkvy2xqp5aby") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.4.0-1 (c (n "libbpf-sys") (v "0.4.0-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0k5c0wpgn7z26w2s4g0ny0k0i12vsi6gb67v5a41jg38kg8yklqp") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.4.0-2 (c (n "libbpf-sys") (v "0.4.0-2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1lpnq3v2qll8b7iwshnh33707krpqdd2vwvnpblxnf6zbmpadchw") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.5.0-1 (c (n "libbpf-sys") (v "0.5.0-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1na19qd71m6c5v5cjnh0fks6qa7bzvpdl299306xvwjnzxw8cl04") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.5.0-2 (c (n "libbpf-sys") (v "0.5.0-2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0alfrjga80mp9iqwy2rkqxdcwv2smhgk2fdf8abxmkwyd4wj764p") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.6.0-1 (c (n "libbpf-sys") (v "0.6.0-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0s06mxryvzb7i513xzv7ghxlk6rwlsh7245lnn92lvs26w3l1kg2") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.6.1-1 (c (n "libbpf-sys") (v "0.6.1-1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18j49n63myrqsfy4wld66y7lx0kh9yh6462157mdv8857svby70v") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.6.1-2 (c (n "libbpf-sys") (v "0.6.1-2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "031zh2j5nc01w3r0sbb56a8m4mr1i09dva4f1qyqpmnzap06wx6z") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.6.2+v0.6.1 (c (n "libbpf-sys") (v "0.6.2+v0.6.1") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0sad3dza3jccjvkz1n7ly21d0bi7g6jqbspyccx7xf72xaxiab0w") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.7.0+v0.7.0 (c (n "libbpf-sys") (v "0.7.0+v0.7.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1d4fpl1cpymyw2yhmrlv4b12kablwq3661s5dgv5dxw70fqv6y8f") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.7.1+v0.7.0 (c (n "libbpf-sys") (v "0.7.1+v0.7.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0s62shzi310kxxpdqqjg81b1qi78r2yjpj29xrh1a67rkvrjgy2q") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.8.0+v0.8.0 (c (n "libbpf-sys") (v "0.8.0+v0.8.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "107xwpvclv8h03xcwzx3cyfbp4xkicc1m4mr7ms5alzwl4q5s698") (f (quote (("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.8.1+v0.8.0 (c (n "libbpf-sys") (v "0.8.1+v0.8.0") (d (list (d (n "bindgen") (r "^0.59") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "188li699hymj3jpl8dil77224s9yxz4cciqy9lbmm6z4d24jfzjc") (f (quote (("static") ("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.8.2+v0.8.1 (c (n "libbpf-sys") (v "0.8.2+v0.8.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1qx50h8w5f3pf4dhl14bzhcrxqzqhjqazkal0w4rszicmmwgs8ds") (f (quote (("static") ("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-0.8.3+v0.8.1 (c (n "libbpf-sys") (v "0.8.3+v0.8.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0301ma0hxwa45nccxkz0cr7pk3wxy1x7pgxmlc61x9lzbdb5p9ri") (f (quote (("static") ("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-1.0.0+v1.0.0 (c (n "libbpf-sys") (v "1.0.0+v1.0.0") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dkjawnhhh0s0yndx9wi840db96anzf62sgzpkn7h9px49sbbhqs") (f (quote (("static") ("novendor")))) (y #t) (l "libbpf")))

(define-public crate-libbpf-sys-1.0.1+v1.0.0 (c (n "libbpf-sys") (v "1.0.1+v1.0.0") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1cgy43yazzwjb1yf8pcvf06qz5z5v9w3w4gqdla1c37jvh0zms2x") (f (quote (("static") ("novendor")))) (y #t) (l "libbpf")))

(define-public crate-libbpf-sys-1.0.2+v1.0.1 (c (n "libbpf-sys") (v "1.0.2+v1.0.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1c5b96y0vkk455nd97009hpvf0cvss4ax4gm72p2i6y77m7p037j") (f (quote (("static") ("novendor")))) (y #t) (l "libbpf")))

(define-public crate-libbpf-sys-1.0.3+v1.0.1 (c (n "libbpf-sys") (v "1.0.3+v1.0.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0906zkx8dm0cicvyxih1wax8jsd4wvcgj1qvpwpfrxyq9dj26v9p") (f (quote (("static") ("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-1.0.4+v1.0.1 (c (n "libbpf-sys") (v "1.0.4+v1.0.1") (d (list (d (n "bindgen") (r "^0.60") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "04qbgxwgjnklirgg79w4mzsa11mri76k2lsr0dx6vl3ns5g7i5b2") (f (quote (("static") ("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-1.1.0+v1.1.0 (c (n "libbpf-sys") (v "1.1.0+v1.1.0") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0pgpsiyd6rz3wixjir3hb2qjq4gzgdaa3zy73f2fyppfin15i0p8") (f (quote (("static") ("novendor")))) (y #t) (l "libbpf")))

(define-public crate-libbpf-sys-1.1.1+v1.1.0 (c (n "libbpf-sys") (v "1.1.1+v1.1.0") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0s08jywq3ckqjwfs6mwzpia9lp24825sx738ixm9j91qa5sgq2wz") (f (quote (("static") ("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-1.1.0+v1.1.1 (c (n "libbpf-sys") (v "1.1.0+v1.1.1") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1kjx8qiwws1bw5zp19z1b6wm0abi7qn1pkm9ngjjy1dq2r3izbzb") (f (quote (("static") ("novendor")))) (y #t) (l "libbpf")))

(define-public crate-libbpf-sys-1.2.0+v1.2.0 (c (n "libbpf-sys") (v "1.2.0+v1.2.0") (d (list (d (n "bindgen") (r "^0.66") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "nix") (r "^0.26.2") (d #t) (k 1)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1ghzq1psf6m68j7k4xd60zihh4vnpqrk5an9p5xk4f5lkwcz6sv1") (f (quote (("vendored" "static") ("static") ("novendor")))) (y #t) (l "libbpf")))

(define-public crate-libbpf-sys-1.2.1+v1.2.0 (c (n "libbpf-sys") (v "1.2.1+v1.2.0") (d (list (d (n "bindgen") (r "^0.66") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "nix") (r "^0.26.2") (d #t) (k 1)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "12xd17bp44398z965w4gwvnp9bahfwjf9h5v7sk2r9w2281b9bbm") (f (quote (("vendored" "static") ("static") ("novendor")))) (l "libbpf")))

(define-public crate-libbpf-sys-1.3.0+v1.3.0 (c (n "libbpf-sys") (v "1.3.0+v1.3.0") (d (list (d (n "bindgen") (r "^0.69.2") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "nix") (r "^0.27.1") (f (quote ("fs"))) (k 1)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "00g8y6h0cz6n7p1sghqz2xkcfn5rwnj01y1hd6vxfbcgzs3qkrmn") (f (quote (("vendored-zlib" "static-zlib") ("vendored-libelf" "static-libelf") ("vendored-libbpf" "static-libbpf") ("vendored" "vendored-libbpf" "vendored-libelf" "vendored-zlib") ("static-zlib" "static-libbpf") ("static-libelf" "static-libbpf") ("static-libbpf") ("static" "static-libbpf" "static-libelf" "static-zlib") ("novendor") ("default" "vendored-libbpf")))) (l "libbpf")))

(define-public crate-libbpf-sys-1.4.0+v1.4.0 (c (n "libbpf-sys") (v "1.4.0+v1.4.0") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "nix") (r "^0.27.1") (f (quote ("fs"))) (k 1)) (d (n "num_cpus") (r "^1.16.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "0n72jdf8fl14yq485yjg23ldc4gz8mn4d4pxjbada6il6mnmzvm4") (f (quote (("vendored-zlib" "static-zlib") ("vendored-libelf" "static-libelf") ("vendored-libbpf" "static-libbpf") ("vendored" "vendored-libbpf" "vendored-libelf" "vendored-zlib") ("static-zlib" "static-libbpf") ("static-libelf" "static-libbpf") ("static-libbpf") ("static" "static-libbpf" "static-libelf" "static-zlib") ("novendor") ("default" "vendored-libbpf") ("bindgen-source" "bindgen")))) (l "libbpf")))

(define-public crate-libbpf-sys-1.4.1+v1.4.0 (c (n "libbpf-sys") (v "1.4.1+v1.4.0") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "nix") (r "^0.27.1") (f (quote ("fs"))) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1xrdrs4gc8x8vf49pv8f31insy0vlgjc2gq7vwfrd7nb2lwj6icd") (f (quote (("vendored-zlib" "static-zlib") ("vendored-libelf" "static-libelf") ("vendored-libbpf" "static-libbpf") ("vendored" "vendored-libbpf" "vendored-libelf" "vendored-zlib") ("static-zlib" "static-libbpf") ("static-libelf" "static-libbpf") ("static-libbpf") ("static" "static-libbpf" "static-libelf" "static-zlib") ("novendor") ("default" "vendored-libbpf") ("bindgen-source" "bindgen")))) (l "libbpf")))

(define-public crate-libbpf-sys-1.4.2+v1.4.2 (c (n "libbpf-sys") (v "1.4.2+v1.4.2") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.90") (d #t) (k 1)) (d (n "nix") (r "^0.28.0") (f (quote ("fs"))) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1v9rncfc5hmh0dd9yrxz8cc14hmqfah438ilcgwn8rr13g1cq2w0") (f (quote (("vendored-zlib" "static-zlib") ("vendored-libelf" "static-libelf") ("vendored-libbpf" "static-libbpf") ("vendored" "vendored-libbpf" "vendored-libelf" "vendored-zlib") ("static-zlib" "static-libbpf") ("static-libelf" "static-libbpf") ("static-libbpf") ("static" "static-libbpf" "static-libelf" "static-zlib") ("novendor") ("default" "vendored-libbpf") ("bindgen-source" "bindgen")))) (l "libbpf")))

