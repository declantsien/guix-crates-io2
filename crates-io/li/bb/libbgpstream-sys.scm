(define-module (crates-io li bb libbgpstream-sys) #:use-module (crates-io))

(define-public crate-libbgpstream-sys-0.0.1 (c (n "libbgpstream-sys") (v "0.0.1") (h "0zn2gknwrcg5wzsj6n118zlp90nvva1vyd7v7afpr2x3jjdh5c6f")))

(define-public crate-libbgpstream-sys-0.1.0 (c (n "libbgpstream-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "07677y826hisy1r9xz1i3890b5j222kc5s5y3frhzjwcgxjhqja2")))

(define-public crate-libbgpstream-sys-0.1.1 (c (n "libbgpstream-sys") (v "0.1.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)))) (h "0vbb0cvkvmvckj1bh0w3d2j0yj27k3hhqq0zmnsvqqfpyfkksqjd")))

(define-public crate-libbgpstream-sys-0.1.2 (c (n "libbgpstream-sys") (v "0.1.2") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "rdkafka-sys") (r "^4.5.0") (d #t) (k 0)) (d (n "rdkafka-sys") (r "^4.5.0") (d #t) (k 1)))) (h "0c5mc4shw5dcnk0ivqsnlws37vxwb9abkd0i3kbybcwl04vmx39f")))

(define-public crate-libbgpstream-sys-0.2.0 (c (n "libbgpstream-sys") (v "0.2.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "rdkafka-sys") (r "^4.5.0") (d #t) (k 0)) (d (n "rdkafka-sys") (r "^4.5.0") (d #t) (k 1)) (d (n "wandio-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "wandio-sys") (r "^0.2.0") (d #t) (k 1)))) (h "1drnmrca143zcb4fzmqs8m25018y73zkka9b59mf5q29sg0xgd45")))

(define-public crate-libbgpstream-sys-0.2.1 (c (n "libbgpstream-sys") (v "0.2.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "rdkafka-sys") (r "^4.5.0") (d #t) (k 0)) (d (n "rdkafka-sys") (r "^4.5.0") (d #t) (k 1)) (d (n "wandio-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "wandio-sys") (r "^0.2.0") (d #t) (k 1)))) (h "04lklhns9c2fas6d2jkm0yidfhb0jv0p8ssshmdigm8h1sa7ialn")))

