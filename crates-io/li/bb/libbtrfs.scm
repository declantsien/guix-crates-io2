(define-module (crates-io li bb libbtrfs) #:use-module (crates-io))

(define-public crate-libbtrfs-0.0.1 (c (n "libbtrfs") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "08llml412r5dms2zzd30zi2r3zw11m1f4cfxqlyb8wkszmaziksi")))

(define-public crate-libbtrfs-0.0.2 (c (n "libbtrfs") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "0wabsp98nblm8nr5s3skzirpf35jyglas3bxckkq9mjhn3zbbxbd")))

(define-public crate-libbtrfs-0.0.5 (c (n "libbtrfs") (v "0.0.5") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "1zzsn8z814kb0z9rb4zfc0390wwxz8m2khlxm6xbaqh2jrsiyy4n")))

(define-public crate-libbtrfs-0.0.10 (c (n "libbtrfs") (v "0.0.10") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "14i65r0z5k0phnhh5fn9h4yxvld10a9f7l4kagbivxk7zxv2d06s")))

(define-public crate-libbtrfs-0.0.10-1 (c (n "libbtrfs") (v "0.0.10-1") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "1d5p1bkvb1x4yg0yxzr1822jvdncf3fni5g9s91kr7vbjywdr5zv")))

(define-public crate-libbtrfs-0.0.11 (c (n "libbtrfs") (v "0.0.11") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "1by3p45z7nj264691mjkr7zabn05g3pfmxqk92rzprwlxx0xnvc8")))

(define-public crate-libbtrfs-0.0.12 (c (n "libbtrfs") (v "0.0.12") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "uuid") (r "^1.3.0") (d #t) (k 0)))) (h "14jm9xs9kk9rks02pwm400jqzajgswpq13f8646rf882dxmi7dql")))

