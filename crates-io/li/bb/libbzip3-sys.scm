(define-module (crates-io li bb libbzip3-sys) #:use-module (crates-io))

(define-public crate-libbzip3-sys-0.1.0 (c (n "libbzip3-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)))) (h "1lzra63jawf20azgix9q3q2kh7c2sa1bwk1vpbja9ps2pmmki08a")))

(define-public crate-libbzip3-sys-0.2.0 (c (n "libbzip3-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.7.1") (d #t) (k 1)))) (h "1yjvgmr2kz3idf1v7wy4v3vmhpfzd4w826913mpaqpz9mh0j4lqx") (f (quote (("bundled")))) (y #t)))

(define-public crate-libbzip3-sys-0.2.1 (c (n "libbzip3-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.7.1") (d #t) (k 1)))) (h "139gdkv43dfnfpp71bsg25bb7k93gkm5p8xli60kxpc9l99ar7rr") (f (quote (("bundled")))) (y #t)))

(define-public crate-libbzip3-sys-0.2.2 (c (n "libbzip3-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.7.1") (d #t) (k 1)))) (h "0r8njpaxxkiv5lf3f59w7yh77fxmjgv2c6dzagqjr9b0nxnl7p6f") (f (quote (("bundled"))))))

(define-public crate-libbzip3-sys-0.3.0 (c (n "libbzip3-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.7.1") (d #t) (k 1)))) (h "11kpqgyd622kmqsjjbgq16y8h27vg8yhv48c9jjygdgpq0zw41yn") (f (quote (("bundled")))) (y #t)))

(define-public crate-libbzip3-sys-0.3.1+1.3.1 (c (n "libbzip3-sys") (v "0.3.1+1.3.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.7.1") (d #t) (k 1)))) (h "1x4ava9cnq4glhkbs7i8ajvzqzpql3y5hi9b5krjsjm2gifs25hy") (f (quote (("bundled"))))))

(define-public crate-libbzip3-sys-0.3.2+1.3.2 (c (n "libbzip3-sys") (v "0.3.2+1.3.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.7.1") (d #t) (k 1)))) (h "1qx0vvbgawh85hnrrl7q3rpdys0gr34cwczw9naiw0qpihbvbl0x") (f (quote (("bundled"))))))

(define-public crate-libbzip3-sys-0.3.3+1.3.2 (c (n "libbzip3-sys") (v "0.3.3+1.3.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.7.1") (d #t) (k 1)))) (h "1w8i4x68fdg0bkinyxlc535f91fna79pacvay40hcm1s6y9z3chz") (f (quote (("bundled"))))))

(define-public crate-libbzip3-sys-0.4.0+1.4.0 (c (n "libbzip3-sys") (v "0.4.0+1.4.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.78") (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "regex") (r "^1.7.1") (d #t) (k 1)))) (h "0ssx5f16hhif8zjdd1fz4cwn1cwjslwcbssm2xs7l93g2i6hw6h0") (f (quote (("bundled"))))))

