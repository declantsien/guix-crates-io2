(define-module (crates-io li bb libbtrfsutil) #:use-module (crates-io))

(define-public crate-libbtrfsutil-0.1.0 (c (n "libbtrfsutil") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^1.2.1") (d #t) (k 0) (p "btrfsutil-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1g1c0l0wmdf1kniziisvqlgi2cwnh4kvfyykwqnd0krdv6rrafgq")))

(define-public crate-libbtrfsutil-0.2.0 (c (n "libbtrfsutil") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^1.2.1") (d #t) (k 0) (p "btrfsutil-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "1dfvsd5q7h8cs2gqq57as3jirlbnm0a9g1v9krp12si011z9p07h")))

(define-public crate-libbtrfsutil-0.3.0 (c (n "libbtrfsutil") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^1.2.1") (d #t) (k 0) (p "btrfsutil-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0cm95x7df5snn60knwk1bnvyhnyd46alkcb6jssb04hqj8zrf6xi")))

(define-public crate-libbtrfsutil-0.4.0 (c (n "libbtrfsutil") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "libbtrfsutil-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "045h5wafbddfx4g47wgc8yrq8kgby5y1a80wzkd4xp0l30srpvq7")))

(define-public crate-libbtrfsutil-0.5.0 (c (n "libbtrfsutil") (v "0.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "libbtrfsutil-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0d5hmm69lzdwxsnd5x6hzkkzj8ds6gxpybvxvpkjdg00hafk74v4")))

(define-public crate-libbtrfsutil-0.5.1 (c (n "libbtrfsutil") (v "0.5.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "libbtrfsutil-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1bcam5q6l4ksi1ipd4h86754x0f4862j0xgj4552cnrm8ym2v6is")))

(define-public crate-libbtrfsutil-0.5.2 (c (n "libbtrfsutil") (v "0.5.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "libbtrfsutil-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0zdih57yj8iyvccfq4pskj6iyhyhrmnyp4rgpdxfa543z8zcn2xm")))

(define-public crate-libbtrfsutil-0.6.0 (c (n "libbtrfsutil") (v "0.6.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "libbtrfsutil-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1mbhvh6n2cjx91khpg12jksh4y9s665qbixyxxvbw6qimmsg4ldf")))

(define-public crate-libbtrfsutil-0.7.0 (c (n "libbtrfsutil") (v "0.7.0") (d (list (d (n "ffi") (r "^0.2.0") (d #t) (k 0) (p "libbtrfsutil-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0y4vwxd7bbilwpqzx2pgwffz0jpa6vsv422sbxx74qyy3xrcxcfp")))

(define-public crate-libbtrfsutil-0.7.1 (c (n "libbtrfsutil") (v "0.7.1") (d (list (d (n "ffi") (r "^0.2.0") (d #t) (k 0) (p "libbtrfsutil-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "10avhn0wp59q2jmahc5vkli753cwxkr7s259mxngpjvkw3a2lkzf")))

