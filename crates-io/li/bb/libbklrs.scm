(define-module (crates-io li bb libbklrs) #:use-module (crates-io))

(define-public crate-libbklrs-0.1.0 (c (n "libbklrs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.33") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.3") (d #t) (k 2)))) (h "114slqjn3l0nd44xxrcams4zkkmq8ka2yij6hfmn153qah46ky50")))

