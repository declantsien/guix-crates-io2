(define-module (crates-io li bb libblkid-rs) #:use-module (crates-io))

(define-public crate-libblkid-rs-0.1.0 (c (n "libblkid-rs") (v "0.1.0") (d (list (d (n "either") (r "^1.5") (d #t) (k 0)) (d (n "libblkid-rs-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (d #t) (k 0)))) (h "0nmg5gcjp1jk1sffmj3l3krvjgmvsj319hxm952mlfahcjnqq7mc") (f (quote (("deprecated") ("default"))))))

(define-public crate-libblkid-rs-0.1.1 (c (n "libblkid-rs") (v "0.1.1") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "libblkid-rs-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "uuid") (r "^0.8.0") (d #t) (k 0)))) (h "1p0q67r5g96d995p4wx3ivf9v6sf53si2rnsg8sk36xph0wd296g") (f (quote (("deprecated") ("default"))))))

(define-public crate-libblkid-rs-0.1.2 (c (n "libblkid-rs") (v "0.1.2") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "libblkid-rs-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "uuid") (r "^0.8.0") (d #t) (k 0)))) (h "110vmkp2ncaky757v5f0wjqf7m2ihddr6nkc8bg97sdcy7k44x0q") (f (quote (("deprecated") ("default")))) (y #t)))

(define-public crate-libblkid-rs-0.2.0 (c (n "libblkid-rs") (v "0.2.0") (d (list (d (n "either") (r "^1.5.0") (d #t) (k 0)) (d (n "libblkid-rs-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "uuid") (r "^0.8.0") (d #t) (k 0)))) (h "1aiznb871yr7qsn8xy6mgsxvnli3fjkn849l49zllxdlh5qwpnh4") (f (quote (("deprecated") ("default"))))))

(define-public crate-libblkid-rs-0.3.0 (c (n "libblkid-rs") (v "0.3.0") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "libblkid-rs-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (d #t) (k 0)))) (h "0h5h1i02gfd7baixr90h8qsyjh1iaay6qmhv8f29y324gqnl16ny") (f (quote (("deprecated") ("default")))) (r "1.62.1")))

(define-public crate-libblkid-rs-0.3.1 (c (n "libblkid-rs") (v "0.3.1") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "libblkid-rs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (d #t) (k 0)))) (h "0qx8s6sa2qcg3v0hmpg378bibz0qq4jvc7zrmw4m46p11mygshqb") (f (quote (("deprecated") ("default")))) (r "1.71.1")))

(define-public crate-libblkid-rs-0.3.2 (c (n "libblkid-rs") (v "0.3.2") (d (list (d (n "either") (r "^1.6.1") (d #t) (k 0)) (d (n "libblkid-rs-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.121") (d #t) (k 0)) (d (n "uuid") (r "^1.0.0") (d #t) (k 0)))) (h "1wmq1hj0d2584h9bzz0bx6z4zp8sllp71gnm9fcp58z7l9zzdff4") (f (quote (("deprecated") ("default")))) (r "1.71.1")))

