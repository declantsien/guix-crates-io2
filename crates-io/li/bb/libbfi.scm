(define-module (crates-io li bb libbfi) #:use-module (crates-io))

(define-public crate-libbfi-1.2.1 (c (n "libbfi") (v "1.2.1") (h "1aqalzw3i2j9lj8acap00pc78cllq6xfb6sdxcmv5am81ak4d2bq") (y #t)))

(define-public crate-libbfi-2.0.0 (c (n "libbfi") (v "2.0.0") (h "0pvk3zlf54lyh1q4n25hrcakgpsdafc0ly1p3m1qjx7qccxxki78") (y #t)))

(define-public crate-libbfi-3.0.0 (c (n "libbfi") (v "3.0.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0y03ldpnk7gfcga513i8kf8j1w0ir4ahzgx8jxklq4ii6igzmj1k")))

(define-public crate-libbfi-3.0.1 (c (n "libbfi") (v "3.0.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1q4jx1n4ck8l5v1v4q4kp4357bzwlvdp4p3x5airc8cgg7w9m43j")))

(define-public crate-libbfi-4.0.0 (c (n "libbfi") (v "4.0.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "00zpdnfshfnfcchbdf4xxz37hrphmk68i9nrp5a2f4fxlrwgnh36")))

(define-public crate-libbfi-4.1.0 (c (n "libbfi") (v "4.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1sif02yg2csk9psz0ypzs0r03bqzkm1lhrppyqpmh00ph0vqmx3q")))

(define-public crate-libbfi-4.2.0 (c (n "libbfi") (v "4.2.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "132ymysfg8glb1mgqmyg3i2qd4lds6q5mmbr4dh6vrcprmajr7w1")))

(define-public crate-libbfi-5.0.0 (c (n "libbfi") (v "5.0.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0yk7gczzb12gysyqg3iigvyv3adp4c8cs4fb8mz5lz6s9mll2mzx")))

(define-public crate-libbfi-6.0.0 (c (n "libbfi") (v "6.0.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "1anr517c6j3fpncric6l1m0rabc0mdx3czb72f6qrwdss8f1rmqs")))

(define-public crate-libbfi-6.0.1 (c (n "libbfi") (v "6.0.1") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "10vbmqbbyhz463nxah6l74z2w79vrxyr5rahahs8g07vmcb1cyhg")))

