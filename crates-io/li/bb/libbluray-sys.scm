(define-module (crates-io li bb libbluray-sys) #:use-module (crates-io))

(define-public crate-libbluray-sys-1.0.0 (c (n "libbluray-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)))) (h "0fswimd2dy6jg00jw8d5xz7fq603lnwyl4m8asxs245q386a8c3f") (f (quote (("default" "aacs" "bdplus") ("bdplus") ("aacs"))))))

(define-public crate-libbluray-sys-1.0.1 (c (n "libbluray-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.68.0") (d #t) (k 1)))) (h "10yp6nysxp0b0m5cd0ipdnjzdw6h01q43md1bz0hjp7njb4kzb3q") (f (quote (("default" "aacs" "bdplus") ("bdplus") ("aacs"))))))

