(define-module (crates-io li bb libbklr) #:use-module (crates-io))

(define-public crate-libbklr-0.0.1 (c (n "libbklr") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "1p3jhmmxv20j1alszfcq0qlsfzxp3qdahbhhy7fqb8j0ca2xjv5j") (y #t)))

(define-public crate-libbklr-0.0.2 (c (n "libbklr") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11.20") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)))) (h "180ii4pr0kf9q9m7b998w1hqahfsmsnhsmi23dv840h4vz95i75a") (y #t)))

