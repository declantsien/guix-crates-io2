(define-module (crates-io li bb libblockchain) #:use-module (crates-io))

(define-public crate-libblockchain-0.0.1 (c (n "libblockchain") (v "0.0.1") (h "0l8is1y3vy1qixyk7vbaj3cmjllsvnljxsv41vwph2jkskygb5ca")))

(define-public crate-libblockchain-0.0.2 (c (n "libblockchain") (v "0.0.2") (d (list (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0sf68vxj9xc6y1hbhxbjd0zfx25mnx6irgk8b60hvg2s92k708jg")))

(define-public crate-libblockchain-0.0.3 (c (n "libblockchain") (v "0.0.3") (d (list (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1cy61xlqkw6y6n6gk3asln3r0fr7c28cbfwdmnmk2ih4l797d62n")))

(define-public crate-libblockchain-0.0.4 (c (n "libblockchain") (v "0.0.4") (d (list (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "0z6rfbwi59blgrsxy8szjxqpzlxf89mw1nmfyhf56mf16jcjm4cf")))

(define-public crate-libblockchain-0.0.5 (c (n "libblockchain") (v "0.0.5") (d (list (d (n "time") (r "^0.1.37") (d #t) (k 0)))) (h "1gqav1726fali9ycryh928y9a4lwpdk84fvqlgy4w1armikyir7p")))

(define-public crate-libblockchain-0.1.0 (c (n "libblockchain") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)))) (h "0wnaxsv3l6mcxbf1wb4r1c4g296dp6jyfrqypga1vpxg5jl8dsnf")))

(define-public crate-libblockchain-0.1.1 (c (n "libblockchain") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)))) (h "11570ji1wyhnjg0f6i72m52ypxgchwmfl643dqqwn15cvr05z0np")))

(define-public crate-libblockchain-0.1.2 (c (n "libblockchain") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.0") (d #t) (k 0)))) (h "170qazfabxczmy5h6hqhvc6b4j7w6shrjq8hr5phx8bh9hwmr5bh")))

