(define-module (crates-io li bb libbruteforce) #:use-module (crates-io))

(define-public crate-libbruteforce-1.0.0 (c (n "libbruteforce") (v "1.0.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "1dkbr7ypk0cnq5ks5kywqypjvinc2i1m608bscvbgfcmy5jm8akp")))

(define-public crate-libbruteforce-1.0.1 (c (n "libbruteforce") (v "1.0.1") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0c1xlnnwc0wcgbxclcb032pb0c6hwi364pwbfgl6q9dcp4ina8yl")))

(define-public crate-libbruteforce-2.0.0 (c (n "libbruteforce") (v "2.0.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "0iwjsw0knpny0zx7qfrm4qv118jsms2ddsxc9hpc7xr1kj8gci7w")))

(define-public crate-libbruteforce-2.1.0 (c (n "libbruteforce") (v "2.1.0") (d (list (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.11.1") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.1") (d #t) (k 0)))) (h "049k6chnq3q8gxv4qfshk8pvbgxlrcica4dgh2jd2zf9s6xgpqjs")))

(define-public crate-libbruteforce-2.2.0 (c (n "libbruteforce") (v "2.2.0") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)))) (h "07pazfiky6msxp3322af3dd4ff9clz3z5q87rm5pd6gra6kp397v") (y #t)))

(define-public crate-libbruteforce-2.2.1 (c (n "libbruteforce") (v "2.2.1") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)))) (h "0lwaj5wfiq6lfvp0m8iybjnpjfc8d8p08bqp4nzsbkmma5hs3dsj") (y #t)))

(define-public crate-libbruteforce-2.2.2 (c (n "libbruteforce") (v "2.2.2") (d (list (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.0") (d #t) (k 0)) (d (n "sha1") (r "^0.6.0") (d #t) (k 0)) (d (n "sha2") (r "^0.8.2") (d #t) (k 0)))) (h "0cm9rnicxlbjcdjfbwd89ypw5wpxsryik1swf88ydha58bfchiq0")))

(define-public crate-libbruteforce-3.0.0 (c (n "libbruteforce") (v "3.0.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.9") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha-1") (r "^0.9") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)) (d (n "simple_logger") (r "^1.11") (d #t) (k 2)))) (h "14p4blj00jkc33y7gysvclf6z7gvwmgj0hm5pq295y943n78vgzd")))

(define-public crate-libbruteforce-3.0.1 (c (n "libbruteforce") (v "3.0.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)))) (h "0w1857dgv5sjg3bb7l9c92rxicb3sl42ljd29ssjjk9b583skwqd")))

(define-public crate-libbruteforce-4.0.0 (c (n "libbruteforce") (v "4.0.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)))) (h "0jl9f9lljaasi8r1gvaz57ryjfh9cdgchwvdlmqcf8hwx7jc46kp")))

(define-public crate-libbruteforce-4.0.1 (c (n "libbruteforce") (v "4.0.1") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "sha-1") (r "^0.10") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "simple_logger") (r "^2.1") (d #t) (k 2)))) (h "1blz4c6xphzkbklkcw7pdywzvypz8ic3v4dnc40gaqv72lv5lk6k")))

