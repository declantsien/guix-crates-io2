(define-module (crates-io li dr lidrs) #:use-module (crates-io))

(define-public crate-lidrs-0.1.0 (c (n "lidrs") (v "0.1.0") (d (list (d (n "approx") (r "0.5.*") (d #t) (k 2)) (d (n "num_enum") (r "0.5.*") (d #t) (k 0)) (d (n "property") (r "0.3.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (d #t) (k 0)))) (h "1kyq7nlw9lc8rfb76m70gpdjkafahxparaqs0rs4554h6yiz0bsm")))

(define-public crate-lidrs-0.2.0 (c (n "lidrs") (v "0.2.0") (d (list (d (n "approx") (r "0.5.*") (d #t) (k 2)) (d (n "num_enum") (r "0.5.*") (d #t) (k 0)) (d (n "property") (r "0.3.*") (d #t) (k 0)) (d (n "regex") (r "1.5.*") (d #t) (k 0)))) (h "166lbajnga6bl136qwhjr2b78kk424xia87bscxknya0qsc7b9x9")))

(define-public crate-lidrs-0.2.1 (c (n "lidrs") (v "0.2.1") (d (list (d (n "approx") (r "0.5.*") (d #t) (k 2)) (d (n "num_enum") (r "0.5.*") (d #t) (k 0)) (d (n "property") (r "0.3.*") (d #t) (k 0)) (d (n "regex") (r "1.6.*") (d #t) (k 0)))) (h "10k1x3gimvv5j8fqdhn4d1dk0hcgcp8n9i35x82q5janpcv3m8c0")))

