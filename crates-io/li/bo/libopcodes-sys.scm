(define-module (crates-io li bo libopcodes-sys) #:use-module (crates-io))

(define-public crate-libopcodes-sys-0.1.0 (c (n "libopcodes-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0.81") (d #t) (k 1)))) (h "0bsc5ify9lbm4vsap3yb6bfp3zcv8jfjq3s8hsrbmywsfb8vly8p") (l "opcodes")))

(define-public crate-libopcodes-sys-0.1.1 (c (n "libopcodes-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1.0.81") (d #t) (k 1)))) (h "1a02v77gsj12fp2mhphz60zcqs4ibci3b6015a5pnbnxcawy4v8j") (l "opcodes")))

