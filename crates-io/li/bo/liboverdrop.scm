(define-module (crates-io li bo liboverdrop) #:use-module (crates-io))

(define-public crate-liboverdrop-0.0.1 (c (n "liboverdrop") (v "0.0.1") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "0zlfmmk4xkrszyvkznwf40rx1fq8929iicfjg5irj5lpqkxdn1yx")))

(define-public crate-liboverdrop-0.0.2 (c (n "liboverdrop") (v "0.0.2") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1fhr5vd3zb67z3i02m8wnaqq1vwaxi28fyl541s6gm5aqmvcr2rs")))

(define-public crate-liboverdrop-0.0.4 (c (n "liboverdrop") (v "0.0.4") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "17cghvajvvg4aflq3pyfn96fjskp5b1nd16wr5c2x9yyyc37y2zh") (r "1.56.0")))

(define-public crate-liboverdrop-0.1.0 (c (n "liboverdrop") (v "0.1.0") (d (list (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "13hz61r7ccxjgbv3k1l1lf6c569s908h2474pcplx0qjflykgr88") (r "1.56.0")))

