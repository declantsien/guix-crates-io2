(define-module (crates-io li bo liboscore) #:use-module (crates-io))

(define-public crate-liboscore-0.1.0 (c (n "liboscore") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cbindgen") (r "^0.24") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "coap-message") (r "^0.2") (d #t) (k 0)) (d (n "coap-message-utils") (r "^0.2.2") (d #t) (k 0)) (d (n "liboscore-cryptobackend") (r "^0.1.0") (f (quote ("chacha20poly1305" "aes-gcm" "aes-ccm"))) (d #t) (k 0)) (d (n "liboscore-msgbackend") (r "^0.1.0") (d #t) (k 0)) (d (n "pretty-hex") (r "^0.3") (k 0)))) (h "0gny0kk3zqcbq35hfg7fanlfs00mln9mxa6vn6cg45m626k86ivi") (l "liboscore")))

