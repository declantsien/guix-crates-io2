(define-module (crates-io li bo libopenxg) #:use-module (crates-io))

(define-public crate-libopenxg-0.1.0 (c (n "libopenxg") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls"))) (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)))) (h "1ns3irbcmdiliadx02dzkfha9pxvv42pbmwmwa1mg6r0vm0nm438")))

(define-public crate-libopenxg-0.1.1 (c (n "libopenxg") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls"))) (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)))) (h "02jgv18ykzdlvprp4nn0354845mgsm00llzlc59hlg5ck16zn24k")))

(define-public crate-libopenxg-0.1.2 (c (n "libopenxg") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls"))) (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)))) (h "0yvnk0bm70b1hn9sqjdn602f04mr2m61wcjsji7cc2bbf8wx1axw")))

(define-public crate-libopenxg-0.1.3 (c (n "libopenxg") (v "0.1.3") (d (list (d (n "const_format") (r "^0.2.30") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls"))) (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)))) (h "01c752f0l66glzzpv7dylbbyiyh71clwpgwbmfqczwgfl14z7syy")))

