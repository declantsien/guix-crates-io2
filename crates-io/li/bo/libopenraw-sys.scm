(define-module (crates-io li bo libopenraw-sys) #:use-module (crates-io))

(define-public crate-libopenraw-sys-0.1.0 (c (n "libopenraw-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "03pcp0cxjf3i3z9l6k0aj6rygzikw8y0c3khbbjnipwnrqvyqflf")))

(define-public crate-libopenraw-sys-0.1.1 (c (n "libopenraw-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0yqw2fvrgv6pl72x52j467b7lwlcxbk7485qrf68q09kylh3wybx")))

(define-public crate-libopenraw-sys-0.1.2 (c (n "libopenraw-sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pmfyiivvi7a6wzj988i7fr7iyiy8afxzlrs3myxb6wgfh8spwki")))

