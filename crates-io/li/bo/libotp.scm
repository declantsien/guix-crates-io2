(define-module (crates-io li bo libotp) #:use-module (crates-io))

(define-public crate-libotp-0.1.0 (c (n "libotp") (v "0.1.0") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 0)))) (h "1gdnwa5jgr5jnxbpz9pp90xq0r1y4vdzbf1n69rig5gsc2lhadpc") (y #t)))

(define-public crate-libotp-0.1.1 (c (n "libotp") (v "0.1.1") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 0)))) (h "14v98lihbbzb87541l97rqf0rrqncbsx2wwi0ggncf4102vw2z37")))

(define-public crate-libotp-0.1.2 (c (n "libotp") (v "0.1.2") (d (list (d (n "base32") (r "^0.3.1") (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 0)))) (h "02bi4k88dfbhy24bazhijmlkys4i6yxdiqh6k9014hs7kl9hgnbg")))

(define-public crate-libotp-0.1.3 (c (n "libotp") (v "0.1.3") (d (list (d (n "binascii") (r "^0.1.1") (d #t) (k 0)) (d (n "ring") (r "^0.13.2") (d #t) (k 0)))) (h "1bnp0hgrgmw9fxqqv204f54qghv0gzq8nk71y8yqa2pk6fvwxs9l")))

(define-public crate-libotp-0.1.4 (c (n "libotp") (v "0.1.4") (d (list (d (n "binascii") (r "^0.1") (d #t) (k 0)) (d (n "ring") (r "^0.14") (d #t) (k 0)))) (h "1kw7qrfaxl00ly7pzrm19q61966g820k95i396s5124c9wbsnhmz")))

