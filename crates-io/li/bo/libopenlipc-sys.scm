(define-module (crates-io li bo libopenlipc-sys) #:use-module (crates-io))

(define-public crate-libopenlipc-sys-0.1.0 (c (n "libopenlipc-sys") (v "0.1.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)))) (h "1arm1wsywfpzi170r5l53liirh57gfh07i6mfb719fc8qw9ilyk6")))

(define-public crate-libopenlipc-sys-0.1.1 (c (n "libopenlipc-sys") (v "0.1.1") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)))) (h "055nwcss7bnp1402lsipmb85lz1bp3j7shdbcjgzd8mckz7rlgbj")))

(define-public crate-libopenlipc-sys-0.1.2 (c (n "libopenlipc-sys") (v "0.1.2") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)))) (h "0msqal7i2z2rngjksb1g1wz23bm10xpmpfqsfpjkdl0nxxf2r8xw")))

(define-public crate-libopenlipc-sys-0.1.3 (c (n "libopenlipc-sys") (v "0.1.3") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)))) (h "15jwzgj5y7dk0rzfmgmsxnspxqlwid17bsj7amj7kv0wb9v35jiz")))

