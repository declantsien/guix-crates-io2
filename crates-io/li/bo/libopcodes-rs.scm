(define-module (crates-io li bo libopcodes-rs) #:use-module (crates-io))

(define-public crate-libopcodes-rs-0.1.0 (c (n "libopcodes-rs") (v "0.1.0") (d (list (d (n "libopcodes-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1f4jigl53fkv9vqyzzmn67b9iph216hi08hwmrr4zxxlqimlirjb")))

(define-public crate-libopcodes-rs-0.1.1 (c (n "libopcodes-rs") (v "0.1.1") (d (list (d (n "libopcodes-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0iskhyc32zsrjf8l13yzn1cbf77bmqyy4rld4f40zgkrai349f32")))

