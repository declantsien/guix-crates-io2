(define-module (crates-io li bo libopencm3_sys) #:use-module (crates-io))

(define-public crate-libopencm3_sys-0.0.1 (c (n "libopencm3_sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0li9mzq72pcglfprvd42bk0in32khyp75sw739v9spvgfacx19pz") (y #t)))

(define-public crate-libopencm3_sys-0.0.2 (c (n "libopencm3_sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1v1nkqzakw22zfwgajdn9h8w7ynzhpxym8raklrl5m97hmmlifr0")))

(define-public crate-libopencm3_sys-0.0.3 (c (n "libopencm3_sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ld89wgshrlvv5291kpazvni0cc6g8j0m1jawym0wamn5xhhjnc6")))

