(define-module (crates-io li bo libobs-sys) #:use-module (crates-io))

(define-public crate-libobs-sys-0.1.0 (c (n "libobs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19zfws1j7ws1xn2l4xsmv2szlq85slh91s9dk9lzq5kgaxs4l2my") (l "obs")))

