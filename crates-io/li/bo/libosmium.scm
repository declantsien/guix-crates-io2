(define-module (crates-io li bo libosmium) #:use-module (crates-io))

(define-public crate-libosmium-0.1.0 (c (n "libosmium") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0x7wy6hli6103zri2y9z4hmpqsl1x5jmda7aybv39a8cdqb8ka87") (l "z")))

(define-public crate-libosmium-0.2.0 (c (n "libosmium") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "100s1kb5j2qbc7846h2r6p0y5hzxm787aj1jiyjaq9wbsw2svbfl") (l "z")))

(define-public crate-libosmium-0.2.1 (c (n "libosmium") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "182hhd9his3793p1x49zmxxa47nhm46rc44g7bj6yg4sv8v60wib") (l "z")))

(define-public crate-libosmium-0.3.0 (c (n "libosmium") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0q4kmy1p4dg87sjz1mr6rj6l57lnv14khsljdsv6lwkpc3kmd1m7") (l "z")))

(define-public crate-libosmium-0.3.1 (c (n "libosmium") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "serde") (r "~1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1g820ccipby35k4ilcbg7r115vbgn3kaj032n7a4fzmlndbihf55") (l "z")))

