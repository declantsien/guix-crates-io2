(define-module (crates-io li bo libocc) #:use-module (crates-io))

(define-public crate-libocc-0.0.0 (c (n "libocc") (v "0.0.0") (h "0w4m33ghahin840aaql1f8x6d86n3rq955xs4kpl4b4054k8gsy4")))

(define-public crate-libocc-0.1.0 (c (n "libocc") (v "0.1.0") (h "1vib7d8l1rmillsixci9qlkilw7pjig33i07jdwa1qn5d92bfdbl")))

(define-public crate-libocc-0.1.1 (c (n "libocc") (v "0.1.1") (h "0wg1bzds3wdiskfjg1v2w8p40rqv49fch87id6s9sl2i8ki8xxgv")))

(define-public crate-libocc-0.1.2 (c (n "libocc") (v "0.1.2") (h "1lk131h9ydyv5yilawv9dfdnshkz7r21wana048z3p45jwppcvb2")))

(define-public crate-libocc-0.1.3 (c (n "libocc") (v "0.1.3") (h "1x0z1d6znc6fpy998748jdmlnkvka5naib8yc1yl2dj7fpl2xli3")))

(define-public crate-libocc-0.2.0 (c (n "libocc") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0h7a3zksnvqmc9x91wc1s3h2qjiidqdqkpk91ls9mrxssgn9yg6s")))

(define-public crate-libocc-0.3.0 (c (n "libocc") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "12ylm3nr0zk61gr6jwrk2mv1wjcq05fq7j0icllgiy8ryrmv3yyq")))

(define-public crate-libocc-0.4.0 (c (n "libocc") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "0nmimxgwl2a844gg4jkv4zsdfnnn2s4b771hkmypwvfd825ygvnc")))

(define-public crate-libocc-0.5.0 (c (n "libocc") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 2)))) (h "02l4ir5xc83jnaf1v60v4ygmhkzda132ipxj650qga0g2gs2ffps")))

