(define-module (crates-io li bo libosdp-sys) #:use-module (crates-io))

(define-public crate-libosdp-sys-0.1.0 (c (n "libosdp-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0yyhl0ss34nkbjjyp1f50dsyrd05vmhfaq6c4bfcvq55lj9ykj3l")))

(define-public crate-libosdp-sys-0.1.1 (c (n "libosdp-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1r4bdwqa1dq0h0z37n7sl9623a61n7ifiz2zr4irm9f6g4rralkx")))

(define-public crate-libosdp-sys-0.1.2 (c (n "libosdp-sys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1rph5gsp9676kkb58id23dha66bbiq7bn68y1x7zv6mnn4f8p24s") (f (quote (("skip_mark_byte") ("packet_trace") ("data_trace"))))))

(define-public crate-libosdp-sys-0.1.3 (c (n "libosdp-sys") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1mar9wisb4ffry695xxfp376zk4q74r6gahkxhg9swqm33zhfcdd") (f (quote (("skip_mark_byte") ("packet_trace") ("data_trace"))))))

(define-public crate-libosdp-sys-0.1.4 (c (n "libosdp-sys") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "1yv0hm43d9aw7lfdrpb3ig7xbiy5xmbq2n33xhnhqfc8i57m9pmb") (f (quote (("skip_mark_byte") ("packet_trace") ("data_trace"))))))

(define-public crate-libosdp-sys-0.1.5 (c (n "libosdp-sys") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "11r8klqcpad0vwggnpyj6gbvszic0gb16myj861g5kwp9855f77l") (f (quote (("skip_mark_byte") ("packet_trace") ("data_trace"))))))

(define-public crate-libosdp-sys-3.0.1 (c (n "libosdp-sys") (v "3.0.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "034md5fac9vryh3kvlkzg3fj1jrvs67q2321qf7iggvwsz52mv3x") (f (quote (("skip_mark_byte") ("packet_trace") ("data_trace"))))))

(define-public crate-libosdp-sys-3.0.2 (c (n "libosdp-sys") (v "3.0.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0lvbvn738nic4vb00kdm9r2ravpr842i29z1rl2grnjc6v3ygh4i") (f (quote (("skip_mark_byte") ("packet_trace") ("data_trace"))))))

(define-public crate-libosdp-sys-3.0.3 (c (n "libosdp-sys") (v "3.0.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0z69k3q3lxf2f9q2k1vg2ckvql4bgjpm5hwlscbxq914s5i02dpn") (f (quote (("skip_mark_byte") ("packet_trace") ("data_trace"))))))

(define-public crate-libosdp-sys-3.0.4 (c (n "libosdp-sys") (v "3.0.4") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "15yzf8i2fmp86a6pfybl2ymffj0g76rn035nlib1bjdarfpkgmx4") (f (quote (("skip_mark_byte") ("packet_trace") ("data_trace"))))))

(define-public crate-libosdp-sys-3.0.5 (c (n "libosdp-sys") (v "3.0.5") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0wjqfb89941rzwxfzy38wwss65b1hmsxly6qg8yi7gab2qpw9jh2") (f (quote (("skip_mark_byte") ("packet_trace") ("data_trace"))))))

(define-public crate-libosdp-sys-3.0.6 (c (n "libosdp-sys") (v "3.0.6") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 1)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "build-target") (r "^0.4.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)))) (h "0z3b1cnl6k7f64ad3w1640vscp3c36nnlpvhh1wh7acyy6fq2sbh") (f (quote (("skip_mark_byte") ("packet_trace") ("data_trace"))))))

