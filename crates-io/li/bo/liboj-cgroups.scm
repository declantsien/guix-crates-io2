(define-module (crates-io li bo liboj-cgroups) #:use-module (crates-io))

(define-public crate-liboj-cgroups-0.1.0 (c (n "liboj-cgroups") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "031nnwm0rhrwb8m6lmgnff6mwiyq2xay9cf0zlvw6c4cg01ib138")))

