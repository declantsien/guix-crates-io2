(define-module (crates-io li bo libosrs) #:use-module (crates-io))

(define-public crate-libosrs-0.1.0 (c (n "libosrs") (v "0.1.0") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1f52vqfbz533wqnf9p7dapcj1mshpnd851jlln5r1iq1qlb7lyjp")))

(define-public crate-libosrs-0.1.1 (c (n "libosrs") (v "0.1.1") (d (list (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "09x6k722piyvxyrb744ahz8lscgjap0912i48bg5wiil6nvmiva7")))

(define-public crate-libosrs-0.1.2 (c (n "libosrs") (v "0.1.2") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1b1mk1dzfwvq075kqpcnmipi6js7cizn0j42jg9x9wr9h8lamdc2")))

(define-public crate-libosrs-0.1.3 (c (n "libosrs") (v "0.1.3") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qd7vh45cpxl0mkabv45knicga5hnpjrphggqpmh002vmblb06dq")))

(define-public crate-libosrs-0.1.4 (c (n "libosrs") (v "0.1.4") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1svq0avwmaf78awlrllwhkj67pr5s4sxd8nwa1aj353y726qskxw")))

