(define-module (crates-io li bo liboci-cli) #:use-module (crates-io))

(define-public crate-liboci-cli-0.0.3 (c (n "liboci-cli") (v "0.0.3") (d (list (d (n "clap") (r "^3.1.7") (f (quote ("std" "suggestions" "derive" "cargo"))) (k 0)))) (h "1kp89c3dbk3j6h6mbdilhnc5mwpdklldggq1kc2j2lapnqlsjnn2")))

(define-public crate-liboci-cli-0.0.4 (c (n "liboci-cli") (v "0.0.4") (d (list (d (n "clap") (r "^3.2.23") (f (quote ("std" "suggestions" "derive" "cargo"))) (k 0)))) (h "1934f9hfqlsdzksmflj5mk9dbrndaa24984akhfl4d3s4hbpfw3g")))

(define-public crate-liboci-cli-0.0.5 (c (n "liboci-cli") (v "0.0.5") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("std" "suggestions" "derive" "cargo" "help" "usage" "error-context"))) (k 0)))) (h "1a102vd2yqfmzn99vz6ji3j41ypxinyqy83hzm22zgij6z4nzb7j")))

(define-public crate-liboci-cli-0.1.0 (c (n "liboci-cli") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("std" "suggestions" "derive" "cargo" "help" "usage" "error-context"))) (k 0)))) (h "015sa80z3mvkk066bk8b0dbvqz1lb8fwddsipmbwk4s978z43a4j")))

(define-public crate-liboci-cli-0.2.0 (c (n "liboci-cli") (v "0.2.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("std" "suggestions" "derive" "cargo" "help" "usage" "error-context"))) (k 0)))) (h "07has3j7i0snajkdv493md5lmzl9inisd7w34lq3b674rjbn8dsm")))

(define-public crate-liboci-cli-0.3.0 (c (n "liboci-cli") (v "0.3.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("std" "suggestions" "derive" "cargo" "help" "usage" "error-context"))) (k 0)))) (h "1shjkpsvr5xa69gq2avj014h4wg88np82lcicigcdzyrgkwh0jbh")))

(define-public crate-liboci-cli-0.3.1 (c (n "liboci-cli") (v "0.3.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("std" "suggestions" "derive" "cargo" "help" "usage" "error-context"))) (k 0)))) (h "1np1gjkvjb9p4vk0dwhw8p2w9c697csqc8qfj54q48k9pd91v5ry")))

(define-public crate-liboci-cli-0.3.2 (c (n "liboci-cli") (v "0.3.2") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("std" "suggestions" "derive" "cargo" "help" "usage" "error-context"))) (k 0)))) (h "1z9z5bn4pn3fjh7dcmjxnly5z4yyl1rzpc4623dja38qnym8vyy2")))

(define-public crate-liboci-cli-0.3.3 (c (n "liboci-cli") (v "0.3.3") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("std" "suggestions" "derive" "cargo" "help" "usage" "error-context"))) (k 0)))) (h "0bsdpqijxhqqv7i41zjiknadwv9qplfzxbwgvx6pp3d54cbr1bq3")))

