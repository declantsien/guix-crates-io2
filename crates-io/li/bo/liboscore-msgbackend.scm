(define-module (crates-io li bo liboscore-msgbackend) #:use-module (crates-io))

(define-public crate-liboscore-msgbackend-0.1.0 (c (n "liboscore-msgbackend") (v "0.1.0") (d (list (d (n "coap-message") (r "^0.2.3") (d #t) (k 0)) (d (n "coap-message-utils") (r "^0.2.2") (d #t) (k 0)))) (h "179hrp24hd4vr8sw8x6sgd0hby90kay5sg26znvg9hd2mq8qb4fp") (f (quote (("alloc" "coap-message/alloc"))))))

