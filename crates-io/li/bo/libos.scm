(define-module (crates-io li bo libos) #:use-module (crates-io))

(define-public crate-libos-0.1.0 (c (n "libos") (v "0.1.0") (d (list (d (n "bootloader") (r "^0.6.4") (f (quote ("map_physical_memory"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.7.5") (d #t) (k 0)))) (h "1whalm0dd7f9d4nh1hsf57ms49znazz9m4c4l2px1sn3wi4j02ib") (y #t)))

(define-public crate-libos-0.2.0 (c (n "libos") (v "0.2.0") (d (list (d (n "bootloader") (r "^0.6.4") (f (quote ("map_physical_memory"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.7.5") (d #t) (k 0)))) (h "1p751k2p058znn4kiz8nr08q9d93a6pm8za86cbrabfnbk08igpf") (y #t)))

(define-public crate-libos-0.1.1 (c (n "libos") (v "0.1.1") (d (list (d (n "bootloader") (r "^0.6.4") (f (quote ("map_physical_memory"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.7.5") (d #t) (k 0)))) (h "1zg8i7g9fgp8rprzkgr8p5a67ajgrvbzm14bb0xxlppwlgpkygv7") (y #t)))

(define-public crate-libos-0.1.2 (c (n "libos") (v "0.1.2") (d (list (d (n "bootloader") (r "^0.6.4") (f (quote ("map_physical_memory"))) (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (f (quote ("spin_no_std"))) (d #t) (k 0)) (d (n "x86_64") (r "^0.7.5") (d #t) (k 0)))) (h "0ypk32v57rwgdh95cgxpyfpxgmk1kjb3r7k4mp9nb5vs5pwxizjx")))

