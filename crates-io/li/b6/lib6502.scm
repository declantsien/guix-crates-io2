(define-module (crates-io li b6 lib6502) #:use-module (crates-io))

(define-public crate-lib6502-0.1.1 (c (n "lib6502") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)))) (h "1hp5zlx7nlayl772kgyinrskyi35ni4lldxqzbs88zvnvxjpl1g0")))

(define-public crate-lib6502-0.1.2 (c (n "lib6502") (v "0.1.2") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)))) (h "0cn6q1p2m1sc928csgkpj78yvz13c4zbbhry3ah6qrvn1j4vz6s9")))

(define-public crate-lib6502-0.2.0 (c (n "lib6502") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)))) (h "020spi4fvkjry7gm7bdy6nzgh8fyqclxpiasabmziaj731s4wnzx") (y #t)))

(define-public crate-lib6502-0.2.2 (c (n "lib6502") (v "0.2.2") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)))) (h "1a91nzwxig3bd021m9gkbwm7j5f1viwy6mgnx0xxywdwsj971jm3")))

