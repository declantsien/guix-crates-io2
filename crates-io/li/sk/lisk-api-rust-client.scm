(define-module (crates-io li sk lisk-api-rust-client) #:use-module (crates-io))

(define-public crate-lisk-api-rust-client-0.1.0 (c (n "lisk-api-rust-client") (v "0.1.0") (d (list (d (n "mockito") (r "^0.17.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.90") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1nnzigwmpcipwjw4agz66k808zd9gfamblgmkzl1ig57qyax5dzz")))

