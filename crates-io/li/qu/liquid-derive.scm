(define-module (crates-io li qu liquid-derive) #:use-module (crates-io))

(define-public crate-liquid-derive-0.19.0 (c (n "liquid-derive") (v "0.19.0") (d (list (d (n "liquid-compiler") (r "^0.19") (d #t) (k 0)) (d (n "liquid-error") (r "^0.19") (d #t) (k 0)) (d (n "liquid-interpreter") (r "^0.19") (d #t) (k 0)) (d (n "liquid-value") (r "^0.19") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "proc-quote") (r "^0.2") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "062j2kgdgijag4xq7yah4x34j6blk9mc65h374wcz9yi1pfsl3sv")))

(define-public crate-liquid-derive-0.20.0-alpha.2 (c (n "liquid-derive") (v "0.20.0-alpha.2") (d (list (d (n "liquid-compiler") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-interpreter") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s7h02nq75imm3aqs9s95cwsqfsj5x6649fidlrn6zddnn0fzm3f")))

(define-public crate-liquid-derive-0.20.0-alpha.3 (c (n "liquid-derive") (v "0.20.0-alpha.3") (d (list (d (n "liquid-compiler") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-interpreter") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "10fgp8v5ffp7xykhy864xz6yzr72c1lw8ziqn7lwjpy77305mciw")))

(define-public crate-liquid-derive-0.20.0-alpha.4 (c (n "liquid-derive") (v "0.20.0-alpha.4") (d (list (d (n "liquid-compiler") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-interpreter") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r8wxr9g798gzm8vrgdvb0ns8p8xjy441ban0sp2dl9nmydmcvgn")))

(define-public crate-liquid-derive-0.20.0-alpha.5 (c (n "liquid-derive") (v "0.20.0-alpha.5") (d (list (d (n "liquid-compiler") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-interpreter") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "14jlgsd2jzqf2yy00pd72c1mm16r351ws8657iwmnrg27bpjhk60")))

(define-public crate-liquid-derive-0.20.0-alpha.6 (c (n "liquid-derive") (v "0.20.0-alpha.6") (d (list (d (n "liquid-compiler") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-interpreter") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1hblpivssjsjbzv9dxk9gpwp3ahj3l2y6a50fgvxxwidzm1fa9dw")))

(define-public crate-liquid-derive-0.20.0-alpha.7 (c (n "liquid-derive") (v "0.20.0-alpha.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pdm9wig2h47sxwn8a0j3bqa0pldsmhwn1qwcnh8zy3h7ykyjllg")))

(define-public crate-liquid-derive-0.20.0-alpha.8 (c (n "liquid-derive") (v "0.20.0-alpha.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0s5vn9vcibqkyikimccis6qcsaq76p46gghl6g5szlcb1552ysl1")))

(define-public crate-liquid-derive-0.20.0 (c (n "liquid-derive") (v "0.20.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0cx17xa2fifrq9m9s110ifi13sg6jp6awniybg34kg1x8j6v9z76")))

(define-public crate-liquid-derive-0.20.1 (c (n "liquid-derive") (v "0.20.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1c0w16kidrnddqjbyk56mwvmszrig18085q5apy5v7h1gzrkbvyg")))

(define-public crate-liquid-derive-0.21.0 (c (n "liquid-derive") (v "0.21.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0aya5bj7paang7vdypb76fsi5v2cqvx9hy13xhdh8w22p6qyjf61")))

(define-public crate-liquid-derive-0.21.1 (c (n "liquid-derive") (v "0.21.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wg7hhh0zlgg6pvhk3nmqqarbml3zzs5a6a7cl4xffqbznbh7vfm")))

(define-public crate-liquid-derive-0.22.0 (c (n "liquid-derive") (v "0.22.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jcbbkq5pk38jpk4skwklj58h6g2fba8wxv92f0jd962nyc8ch48")))

(define-public crate-liquid-derive-0.23.0 (c (n "liquid-derive") (v "0.23.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "084gbk8i5zryjz5k6w6a81d0wyb94x2f8pi7d5x3bmz0vy7f2nkh")))

(define-public crate-liquid-derive-0.23.1 (c (n "liquid-derive") (v "0.23.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02hza3ksl6gpmc3xwsk5h8j5ynd2ll6kp483fvhaz88df1bf8435")))

(define-public crate-liquid-derive-0.24.0 (c (n "liquid-derive") (v "0.24.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0mha305h4f7dg71alsspr9azs5bzda9aw8x41afqczmsi8182bfq")))

(define-public crate-liquid-derive-0.25.0 (c (n "liquid-derive") (v "0.25.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0418yf6dnhkbb0w2s7s7yg11yl5mc1gdx7jskyd6qi53s5xigvwb")))

(define-public crate-liquid-derive-0.25.1 (c (n "liquid-derive") (v "0.25.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "00qplayfkzail9dhmgkvk77mxif9f4fx9gy90hxkc9g0fcs8fy3m")))

(define-public crate-liquid-derive-0.26.0 (c (n "liquid-derive") (v "0.26.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0sd4ynwg00lg6nl8bmc80ywy9hx7zbdsqxrhhcrq8ghhbws58r4j")))

(define-public crate-liquid-derive-0.26.1 (c (n "liquid-derive") (v "0.26.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0irzxlk29psslr87s18d7y6d4gbf99l2a0gjsqxj7532p7gnl731") (r "1.60.0")))

(define-public crate-liquid-derive-0.26.2 (c (n "liquid-derive") (v "0.26.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "proc-quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zpjl3n43ply9n1jp99nfdzp5zrx3gy1kj4zaydmhl6llg2r8j41") (r "1.65.0")))

(define-public crate-liquid-derive-0.26.3 (c (n "liquid-derive") (v "0.26.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "01h463p1p344wxikps30l4rflzk9940qi9d234kyp06kbx1dlf14") (r "1.65.0")))

(define-public crate-liquid-derive-0.26.4 (c (n "liquid-derive") (v "0.26.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.28") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0887ccqm50ddj0d9n74ls6bm3m7p5mzdyjqm0cw7l9dlkcdb8bzw") (r "1.65.0")))

