(define-module (crates-io li qu liquid-interpreter) #:use-module (crates-io))

(define-public crate-liquid-interpreter-0.16.0 (c (n "liquid-interpreter") (v "0.16.0") (d (list (d (n "itertools") (r "^0.7.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.16.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.16.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "1nwlzk63inz1xinjv3mjl2qf3zg7dbbnrkkkd2ndbmcrdvhf3dbc")))

(define-public crate-liquid-interpreter-0.16.1 (c (n "liquid-interpreter") (v "0.16.1") (d (list (d (n "itertools") (r "^0.7.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.16.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.16.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "0zxvpbkgbcb6r9bkcslsk4v6wzjv2yq0c1fzkdlqql2qrbqc00jn")))

(define-public crate-liquid-interpreter-0.17.0 (c (n "liquid-interpreter") (v "0.17.0") (d (list (d (n "itertools") (r "^0.7.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.16") (d #t) (k 0)) (d (n "liquid-value") (r "^0.17") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "164vgnqny59smz1mm8mwkff0cv10dxhxs1am4sddifzyf8p0xqha")))

(define-public crate-liquid-interpreter-0.17.1 (c (n "liquid-interpreter") (v "0.17.1") (d (list (d (n "itertools") (r "^0.7.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.16") (d #t) (k 0)) (d (n "liquid-value") (r "^0.17") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "02fs3w68lhmgd4i1cjadl94ppv5fy7dda1a06z4l9fs0bx378gxz")))

(define-public crate-liquid-interpreter-0.18.0 (c (n "liquid-interpreter") (v "0.18.0") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.7.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.18") (d #t) (k 0)) (d (n "liquid-value") (r "^0.18") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "128a66d4sa7p2vxsi81x8kj85lij5b9y380aci2bda687hxifdlm")))

(define-public crate-liquid-interpreter-0.18.1 (c (n "liquid-interpreter") (v "0.18.1") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.18") (d #t) (k 0)) (d (n "liquid-value") (r "^0.18") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "0yza8fxn74cpjfcvff859sfri472cfn489p4g52s71wbil77xh0c")))

(define-public crate-liquid-interpreter-0.19.0 (c (n "liquid-interpreter") (v "0.19.0") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.19") (d #t) (k 0)) (d (n "liquid-value") (r "^0.19") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "0gn62z4439z5398rg31igvpwi8fjmcavpk190yxi0qnbp08h93lr")))

(define-public crate-liquid-interpreter-0.20.0-alpha.2 (c (n "liquid-interpreter") (v "0.20.0-alpha.2") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "kstring") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "03iydmw5ypq7nawpal408v77fblckrp0yj3hc7s20y215pwwvknp")))

(define-public crate-liquid-interpreter-0.20.0-alpha.3 (c (n "liquid-interpreter") (v "0.20.0-alpha.3") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "kstring") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "005wg7cvsc4s8x4g73bad7y6g504fgi3609znhi240nk2wxa6b4y")))

(define-public crate-liquid-interpreter-0.20.0-alpha.4 (c (n "liquid-interpreter") (v "0.20.0-alpha.4") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "kstring") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "11r532w33sy2r14fbxfnr0y9apamykabcx6lkhj29vfxv614ms89")))

(define-public crate-liquid-interpreter-0.20.0-alpha.5 (c (n "liquid-interpreter") (v "0.20.0-alpha.5") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "kstring") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "1higcr8z1hy0g0q6l2d15drk70x8wlqcfj5szasm6m82s2xvbrgf")))

(define-public crate-liquid-interpreter-0.20.0-alpha.6 (c (n "liquid-interpreter") (v "0.20.0-alpha.6") (d (list (d (n "anymap") (r "^0.12") (d #t) (k 0)) (d (n "itertools") (r "^0.8.0") (d #t) (k 0)) (d (n "kstring") (r "^0.1.0-alpha.0") (d #t) (k 0)) (d (n "liquid-error") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "liquid-value") (r "^0.20.0-alpha.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 2)))) (h "1znwws23z3vsq5h6apy3dws94m9cmvbh47awghwdyzv4djdm7wgc")))

