(define-module (crates-io li qu liquid_crystal) #:use-module (crates-io))

(define-public crate-liquid_crystal-0.1.0 (c (n "liquid_crystal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)))) (h "0rcmq4lmq5axc5jf3zw589ga4rhyrw8lacms2mxz9m0pbkr15z7a")))

(define-public crate-liquid_crystal-0.1.1 (c (n "liquid_crystal") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1s5lzqagdymdvix7zm5p7sz6m09l4c4mhl05fnc5j8ik5ab3jbnr")))

(define-public crate-liquid_crystal-0.1.2 (c (n "liquid_crystal") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "11avsf7n96dhrj5gyx641i1rmfljppw68raf307lqhp7i3zw02ll")))

(define-public crate-liquid_crystal-0.1.4 (c (n "liquid_crystal") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "0dp96nn9bylk2ffw4lcmy4ssfx5pqr3nvhvgk63qjz50c36d3qvj")))

(define-public crate-liquid_crystal-0.1.5 (c (n "liquid_crystal") (v "0.1.5") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1zcvxiq8fimnc5v1hkzygq0083cd2xjyfgq8lz2bi2q6bj7z8nyg")))

(define-public crate-liquid_crystal-0.1.6 (c (n "liquid_crystal") (v "0.1.6") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)))) (h "1xp34r1hj42w0hx7q5ln09n5n0amrqv9cyvrp0343g8gala1pqym")))

(define-public crate-liquid_crystal-0.2.0 (c (n "liquid_crystal") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^1.0") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1s9il22ya4r9038xqh6v27sf9w4zlslyk4haywhgqhdwjh3nxdnq") (s 2) (e (quote (("async" "dep:embedded-hal-async"))))))

