(define-module (crates-io li qu liquid-filter-reverse-string) #:use-module (crates-io))

(define-public crate-liquid-filter-reverse-string-0.1.0 (c (n "liquid-filter-reverse-string") (v "0.1.0") (d (list (d (n "liquid") (r "^0.26") (d #t) (k 0)) (d (n "liquid-core") (r "^0.26") (d #t) (k 0)))) (h "006221rn6573sk2l03837sxvxzqcnmc6c6zk6pcnpaaj2cw7bxwf")))

(define-public crate-liquid-filter-reverse-string-0.1.1 (c (n "liquid-filter-reverse-string") (v "0.1.1") (d (list (d (n "liquid") (r "^0.26") (d #t) (k 0)) (d (n "liquid-core") (r "^0.26") (d #t) (k 0)))) (h "1pqra9885di9vqmznjj8mfbly6s7ha6l65az9wsrlvx2l9dz47vg")))

