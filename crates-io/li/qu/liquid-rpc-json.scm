(define-module (crates-io li qu liquid-rpc-json) #:use-module (crates-io))

(define-public crate-liquid-rpc-json-0.1.0 (c (n "liquid-rpc-json") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.18") (f (quote ("use-serde"))) (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "bitcoincore-rpc") (r "^0.7") (d #t) (k 0)) (d (n "elements") (r "^0.7.0") (f (quote ("serde-feature"))) (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.12") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0q7fp2kllg9xgn8slz5x867zyxqfcp5v848nczihbc4s12kvk9hb")))

(define-public crate-liquid-rpc-json-0.2.0 (c (n "liquid-rpc-json") (v "0.2.0") (d (list (d (n "bitcoincore-rpc") (r "^0.11") (d #t) (k 0)) (d (n "elements") (r "^0.12.0") (f (quote ("serde-feature"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1s93mpy8f4ga39j7vwxsh20c72nk9m4560jw9prz5gd74hrf7szj")))

