(define-module (crates-io li qu liquid-error) #:use-module (crates-io))

(define-public crate-liquid-error-0.16.0 (c (n "liquid-error") (v "0.16.0") (h "19ryaqixvj9k6ff1sc1rd8kz8l0wqaj1s69jq63p6x5b9h3vc9gr")))

(define-public crate-liquid-error-0.16.1 (c (n "liquid-error") (v "0.16.1") (h "0r69zg54jgkj599g8pcm8lf001kjfqxjy4bijck3hifd6lmxl5mw")))

(define-public crate-liquid-error-0.18.0 (c (n "liquid-error") (v "0.18.0") (h "089pxc1vh1dw1cv2k8g35r0sdjw0xiby4qdxkij210kcfqsawk32")))

(define-public crate-liquid-error-0.18.1 (c (n "liquid-error") (v "0.18.1") (h "16wnx2ldxwp5kq4j1gdhk5j5b82cjap2fdlmgf6ihyv8f4hry4bn")))

(define-public crate-liquid-error-0.19.0 (c (n "liquid-error") (v "0.19.0") (h "10fljkxnsvi4q38bhknl3djxivy343jxghk7rxyyh8i3h58hhp3x")))

(define-public crate-liquid-error-0.20.0-alpha.2 (c (n "liquid-error") (v "0.20.0-alpha.2") (d (list (d (n "kstring") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "0j6x5mbb9fy6sa68y85wbj9yy7dnzgyi36kxazd63sdhdc5hms71")))

(define-public crate-liquid-error-0.20.0-alpha.3 (c (n "liquid-error") (v "0.20.0-alpha.3") (d (list (d (n "kstring") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "16qjlp9b7bb36dw3axjwiz8jvwpjd8ahbkw6jap1min2bgp4jpsb")))

(define-public crate-liquid-error-0.20.0-alpha.4 (c (n "liquid-error") (v "0.20.0-alpha.4") (d (list (d (n "kstring") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "19zl9va06c4njzmpw5rqiliwrv87fch0hgdd7abkyxcdhkykx039")))

(define-public crate-liquid-error-0.20.0-alpha.5 (c (n "liquid-error") (v "0.20.0-alpha.5") (d (list (d (n "kstring") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "1njxc0j8f39lwps7pak0nklq0vfp2rcdrcc3vdisqwaawjqf06di")))

(define-public crate-liquid-error-0.20.0-alpha.6 (c (n "liquid-error") (v "0.20.0-alpha.6") (d (list (d (n "kstring") (r "^0.1.0-alpha.0") (d #t) (k 0)))) (h "1c7lbcx5ypdq0z7nwm8ycxblay0lf7jizslcicbr63nb8accfgk6")))

