(define-module (crates-io li qu liquidfun) #:use-module (crates-io))

(define-public crate-liquidfun-0.1.0 (c (n "liquidfun") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mkw8a3ppz6rzmk7zvfgbslpv0rc3lz8fdr1ifwqgspqyfmfx60y")))

(define-public crate-liquidfun-0.2.0 (c (n "liquidfun") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ma1pmia98amn8ma5n3gz1fpa59zwwhkiv36nrsfn0vhyjkg98hq")))

(define-public crate-liquidfun-0.3.0 (c (n "liquidfun") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m8hb5vpaf4dd74h6kp8fzzjdr9iw1sj23gqvxmazj6cfkd6d6pd")))

(define-public crate-liquidfun-0.4.0 (c (n "liquidfun") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11zj5fkimmzd0a88zl9i8p2cdy1gr7j106whidsmlklrlk2s3s85")))

(define-public crate-liquidfun-0.5.0 (c (n "liquidfun") (v "0.5.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hqq3sidqrc23xngdg72rfadvsrsqi60rb550qplrq530mzb7jq1")))

(define-public crate-liquidfun-0.6.0 (c (n "liquidfun") (v "0.6.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11n28359zqgslb3dpvcpj8j103hfwmgsjdrphwyaj931i7ml7wf7")))

(define-public crate-liquidfun-0.7.0 (c (n "liquidfun") (v "0.7.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "116b5w7sra42ll5syan3lzdb6rzdzdgp8g6lsfn6cq2wixi9918l")))

(define-public crate-liquidfun-0.8.0 (c (n "liquidfun") (v "0.8.0") (d (list (d (n "bitflags") (r "^0.3") (d #t) (k 0)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jb8vy21qbnj247fzk96jppjyagpmvih20q4mb3rhj2l8fsv542q")))

