(define-module (crates-io li qu liquid-chain) #:use-module (crates-io))

(define-public crate-liquid-chain-0.1.0 (c (n "liquid-chain") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0gibw407x6kiqf61fkkk3ard466gq5xqqqf0phi0a6z93s4axg8l")))

