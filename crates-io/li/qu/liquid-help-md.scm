(define-module (crates-io li qu liquid-help-md) #:use-module (crates-io))

(define-public crate-liquid-help-md-0.20.0-alpha.2 (c (n "liquid-help-md") (v "0.20.0-alpha.2") (d (list (d (n "liquid-core") (r "^0.20.0-alpha.0") (d #t) (k 0)))) (h "1ig8gakzfpjslmh78f23iiznydvx0fkvjksdwzgx6gdz687n3ga2")))

(define-public crate-liquid-help-md-0.20.0-alpha.3 (c (n "liquid-help-md") (v "0.20.0-alpha.3") (d (list (d (n "liquid-core") (r "^0.20.0-alpha.0") (d #t) (k 0)))) (h "0c4kkrb2f772qwvzzsjk9kgf0p1filzyf1ysw3bj80hwki7342jx")))

(define-public crate-liquid-help-md-0.20.0-alpha.4 (c (n "liquid-help-md") (v "0.20.0-alpha.4") (d (list (d (n "liquid-core") (r "^0.20.0-alpha.0") (d #t) (k 0)))) (h "1989vm85v6gazvrbin5p8h3wm0p1swsgx428bhy37asfipcbypyg")))

(define-public crate-liquid-help-md-0.20.0-alpha.5 (c (n "liquid-help-md") (v "0.20.0-alpha.5") (d (list (d (n "liquid-core") (r "^0.20.0-alpha.0") (d #t) (k 0)))) (h "0ip4zy18i3396j21iycp2lcnsz81bm5l45r36hi3vfby13vgjhqz")))

(define-public crate-liquid-help-md-0.20.0-alpha.6 (c (n "liquid-help-md") (v "0.20.0-alpha.6") (d (list (d (n "liquid-core") (r "^0.20.0-alpha.0") (d #t) (k 0)))) (h "0lgk5c68z6knvbma85gq4c1f66r0asin822lgwn59j68vy46smqh")))

(define-public crate-liquid-help-md-0.20.0-alpha.7 (c (n "liquid-help-md") (v "0.20.0-alpha.7") (d (list (d (n "liquid-core") (r "^0.20.0-alpha.0") (d #t) (k 0)))) (h "0i8bs7s0cid49ba8w5czvvp9llj958vkrhs3vf64m83ry63avv6x")))

(define-public crate-liquid-help-md-0.20.0-alpha.8 (c (n "liquid-help-md") (v "0.20.0-alpha.8") (d (list (d (n "liquid-core") (r "^0.20.0-alpha.0") (d #t) (k 0)))) (h "0zargplbx79bcnrzpcmb0mkfh8ikfqp3vm0mqjbj4cwfdxq449sw")))

(define-public crate-liquid-help-md-0.20.0 (c (n "liquid-help-md") (v "0.20.0") (d (list (d (n "liquid-core") (r "^0.20.0-alpha.0") (d #t) (k 0)))) (h "1lcn9j04r9m17xh1yjsjw4balahhxxi3i5rbbq3w8n616z7m1bcw")))

(define-public crate-liquid-help-md-0.21.0 (c (n "liquid-help-md") (v "0.21.0") (d (list (d (n "liquid-core") (r "^0.21.0") (d #t) (k 0)))) (h "0hrhbm1s613ybzgdjh877qd42pqncpdd7fjyc0wz7r8aiz70v1hl")))

(define-public crate-liquid-help-md-0.21.1 (c (n "liquid-help-md") (v "0.21.1") (d (list (d (n "liquid-core") (r "^0.21.0") (d #t) (k 0)))) (h "03c3yddy95nidljprsfdaxbn3345a4pvsqn0q1rnd86dsx6mqzfk")))

(define-public crate-liquid-help-md-0.22.0 (c (n "liquid-help-md") (v "0.22.0") (d (list (d (n "liquid-core") (r "^0.22.0") (d #t) (k 0)))) (h "1h0a8y730xsh0imb0r2yylm4dz827lwv5w72kdp3lnsm5rzg1rjp")))

(define-public crate-liquid-help-md-0.23.0 (c (n "liquid-help-md") (v "0.23.0") (d (list (d (n "liquid-core") (r "^0.23.0") (d #t) (k 0)))) (h "1x698ij4fwxj7gj6lpbp9xsmw3h4fwk36dsym9higdw351g626s5")))

(define-public crate-liquid-help-md-0.23.1 (c (n "liquid-help-md") (v "0.23.1") (d (list (d (n "liquid-core") (r "^0.23.0") (d #t) (k 0)))) (h "1j2p7afdkzm30db7fhwg8m8zmbivlz3svj0z03kgp73qy3jd2vx1")))

(define-public crate-liquid-help-md-0.24.0 (c (n "liquid-help-md") (v "0.24.0") (d (list (d (n "liquid-core") (r "^0.24.0") (d #t) (k 0)))) (h "091i5hd2qpkfr83wcbz84p2n0ys8nh51nrwvlrrqa3ja84lvjkws")))

(define-public crate-liquid-help-md-0.25.0 (c (n "liquid-help-md") (v "0.25.0") (d (list (d (n "liquid-core") (r "^0.25.0") (d #t) (k 0)))) (h "05nwvkifyndg3zvakphjwv9g7myr1sm73l17grbznv5rvx25l30z")))

(define-public crate-liquid-help-md-0.25.1 (c (n "liquid-help-md") (v "0.25.1") (d (list (d (n "liquid-core") (r "^0.25.0") (d #t) (k 0)))) (h "0320hlrxjd70krpbam2s6v77zxk5hms4rpyxzjsfhj0xs8jk4zfz")))

(define-public crate-liquid-help-md-0.26.0 (c (n "liquid-help-md") (v "0.26.0") (d (list (d (n "liquid-core") (r "^0.26.0") (d #t) (k 0)))) (h "1zp0j7ivwyz7njmw5qd66i79d5z93dd07dfqx3af619lzmh9as65")))

(define-public crate-liquid-help-md-0.26.1 (c (n "liquid-help-md") (v "0.26.1") (d (list (d (n "liquid-core") (r "^0.26.1") (d #t) (k 0)))) (h "07m8m7dwmiqlw7m2dsym224ws1m52l9iffp9gw7fpn07j3p52nq5") (r "1.60.0")))

(define-public crate-liquid-help-md-0.26.2 (c (n "liquid-help-md") (v "0.26.2") (d (list (d (n "liquid-core") (r "^0.26.2") (d #t) (k 0)))) (h "0kayzjcb7s30vlp6bwhmcgiwzikd9hv6fm2kkxbh0g25jpnn7d88") (r "1.65.0")))

(define-public crate-liquid-help-md-0.26.3 (c (n "liquid-help-md") (v "0.26.3") (d (list (d (n "liquid-core") (r "^0.26.3") (d #t) (k 0)))) (h "0q60jyw7hxk1lncrzw7xn0f8wl8yp39ijipagb5xk7rlwqjy1xmp") (r "1.65.0")))

