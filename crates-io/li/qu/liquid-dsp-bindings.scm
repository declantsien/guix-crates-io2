(define-module (crates-io li qu liquid-dsp-bindings) #:use-module (crates-io))

(define-public crate-liquid-dsp-bindings-0.1.0 (c (n "liquid-dsp-bindings") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)))) (h "1dj8wb5jyhj6liq9r5ivykja6p7s5piiigdvhashfvc973fdyad1")))

(define-public crate-liquid-dsp-bindings-0.1.1 (c (n "liquid-dsp-bindings") (v "0.1.1") (d (list (d (n "liquid-dsp-bindings-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0x10mldlgshdp1iiwi72nqg95m4392jrrkr1325wxag59zygvph3")))

