(define-module (crates-io li qu liquidity_check) #:use-module (crates-io))

(define-public crate-liquidity_check-0.1.0 (c (n "liquidity_check") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "rusty-money") (r "^0.4.1") (d #t) (k 0)))) (h "1ds2mxa7gwql91a2ya7biikis1nwrgiv3yv5k57sib2p4gmpj0mk")))

