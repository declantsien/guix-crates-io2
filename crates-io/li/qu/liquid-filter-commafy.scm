(define-module (crates-io li qu liquid-filter-commafy) #:use-module (crates-io))

(define-public crate-liquid-filter-commafy-0.1.1 (c (n "liquid-filter-commafy") (v "0.1.1") (d (list (d (n "liquid") (r "^0.26") (d #t) (k 0)) (d (n "liquid-core") (r "^0.26") (d #t) (k 0)))) (h "1rw3aii55syxh0qrpw7x3v7sa8mi8w0w0dq3b8gymq68yiarql6x")))

