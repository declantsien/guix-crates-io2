(define-module (crates-io li qu liquidity-helper) #:use-module (crates-io))

(define-public crate-liquidity-helper-0.1.0 (c (n "liquidity-helper") (v "0.1.0") (d (list (d (n "apollo-cw-asset") (r "^0.1.0") (d #t) (k 0)) (d (n "apollo-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.2.1") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.2.1") (d #t) (k 0)) (d (n "cw20") (r "^1.0.1") (d #t) (k 0)) (d (n "schemars") (r "^0.8.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (k 0)))) (h "13qd4n68pdmzc3rfzgdh48mjb126mzhrvlrlxjdk2ph2a05smjvg") (r "1.64.0")))

(define-public crate-liquidity-helper-0.2.0 (c (n "liquidity-helper") (v "0.2.0") (d (list (d (n "apollo-cw-asset") (r "^0.1.2") (d #t) (k 0)) (d (n "apollo-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.3.2") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.3.2") (d #t) (k 0)) (d (n "cw20") (r "^1.1.0") (d #t) (k 0)))) (h "1sb2ab92qc3mbwmk3nyww04lfivmx8qfx86bzklmmnd51bn6yn6v") (r "1.64.0")))

(define-public crate-liquidity-helper-0.2.1 (c (n "liquidity-helper") (v "0.2.1") (d (list (d (n "apollo-cw-asset") (r "^0.1.2") (d #t) (k 0)) (d (n "apollo-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw20") (r "^1.1.0") (d #t) (k 0)))) (h "1fizgm73i1id14gw2p8diwvmvngq6vnsw8rl8hx1x7ndv3fqfa9j") (r "1.64.0")))

(define-public crate-liquidity-helper-0.2.2 (c (n "liquidity-helper") (v "0.2.2") (d (list (d (n "apollo-cw-asset") (r "^0.1.2") (d #t) (k 0)) (d (n "apollo-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw20") (r "^1.1.0") (d #t) (k 0)))) (h "0s10gmbqk7axjmv012965cmpw7cv0kxqjf3zz3zrh1j136yyzgj9") (r "1.64.0")))

(define-public crate-liquidity-helper-0.2.3-rc.1 (c (n "liquidity-helper") (v "0.2.3-rc.1") (d (list (d (n "apollo-cw-asset") (r "^0.1.2") (d #t) (k 0)) (d (n "apollo-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw20") (r "^1.1.0") (d #t) (k 0)))) (h "04gqa5cwgz183w43rx2rnf2kmdv4xglr3wzfajacpd0crdil8dkr") (r "1.64.0")))

(define-public crate-liquidity-helper-0.2.3 (c (n "liquidity-helper") (v "0.2.3") (d (list (d (n "apollo-cw-asset") (r "^0.1.2") (d #t) (k 0)) (d (n "apollo-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw20") (r "^1.1.0") (d #t) (k 0)))) (h "03ywh1j5r9qf7zqkq46fld91v7fzg4dh3wpiiw4y14mszqc3s42f") (y #t) (r "1.64.0")))

(define-public crate-liquidity-helper-0.3.0 (c (n "liquidity-helper") (v "0.3.0") (d (list (d (n "apollo-cw-asset") (r "^0.1.2") (d #t) (k 0)) (d (n "apollo-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw20") (r "^1.1.0") (d #t) (k 0)))) (h "1sm0ba5723rzs7q0znwfncm1z81dqgslaigqvwsnq03nw73saimq") (r "1.64.0")))

(define-public crate-liquidity-helper-0.3.2 (c (n "liquidity-helper") (v "0.3.2") (d (list (d (n "apollo-cw-asset") (r "^0.1.2") (d #t) (k 0)) (d (n "apollo-utils") (r "^0.1.1") (d #t) (k 0)) (d (n "cosmwasm-schema") (r "^1.4.0") (d #t) (k 0)) (d (n "cosmwasm-std") (r "^1.4.0") (d #t) (k 0)) (d (n "cw20") (r "^1.1.0") (d #t) (k 0)))) (h "10ap397ixs79d38x0kyric5fsyn2dmm99w6h2z1xfmv0h97wiv72") (r "1.64.0")))

