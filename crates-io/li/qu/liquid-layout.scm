(define-module (crates-io li qu liquid-layout) #:use-module (crates-io))

(define-public crate-liquid-layout-0.1.0 (c (n "liquid-layout") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "fraction") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "z3") (r "^0.10") (d #t) (k 0)))) (h "159p9y3n3cw0iyhif83b4yffwkrhn1x8kbxv7snv1hji9sh36w3y")))

(define-public crate-liquid-layout-0.1.1 (c (n "liquid-layout") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bumpalo") (r "^3") (d #t) (k 0)) (d (n "fraction") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "z3") (r "^0.10") (d #t) (k 0)))) (h "03dyna4d27qj46a6rrzxgr6m486bli2b6vz3ksv9bvag1vargngx")))

