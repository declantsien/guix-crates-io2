(define-module (crates-io li qu liquid-heck) #:use-module (crates-io))

(define-public crate-liquid-heck-0.1.0 (c (n "liquid-heck") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "liquid") (r "^0.26") (d #t) (k 2)) (d (n "liquid-core") (r "^0.26") (d #t) (k 0)) (d (n "liquid-derive") (r "^0.26") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "12kxdsz75lpw2fy6wzjygar0v6jmfn9qhgjflq8j7mmr32bddacm") (s 2) (e (quote (("tracing" "dep:tracing"))))))

