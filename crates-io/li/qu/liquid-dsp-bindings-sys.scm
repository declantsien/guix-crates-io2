(define-module (crates-io li qu liquid-dsp-bindings-sys) #:use-module (crates-io))

(define-public crate-liquid-dsp-bindings-sys-0.1.0 (c (n "liquid-dsp-bindings-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)))) (h "1d4zg8bb0xnrckczchy4mnfsg3hxvj412hg7b68wzjrddz16mqv4")))

