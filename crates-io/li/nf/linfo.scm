(define-module (crates-io li nf linfo) #:use-module (crates-io))

(define-public crate-linfo-0.1.0 (c (n "linfo") (v "0.1.0") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (o #t) (d #t) (k 0)))) (h "1si2zczxid3fa2lk501h7cvaklh0k8v16yaa5nrs277bfzxakdjc") (f (quote (("std") ("default" "std"))))))

(define-public crate-linfo-0.1.1 (c (n "linfo") (v "0.1.1") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15x5llzzvjlqq4bvkgpc0j1mj6vrar7hk14nkxc9ic4gn2xavjxc") (f (quote (("std") ("default" "std"))))))

(define-public crate-linfo-0.1.2 (c (n "linfo") (v "0.1.2") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "15h9jl2cmdyc8pvhd9x5mwf5xnc5pgbgqm77c12j3fw17gmkf5yh") (f (quote (("std") ("default" "std"))))))

(define-public crate-linfo-0.1.3 (c (n "linfo") (v "0.1.3") (d (list (d (n "phf") (r "^0.8") (f (quote ("macros"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16fibdnpxgfn0y984958nfyz1rpc862sl0xdlq0hh4x7q2yyyczm") (f (quote (("std") ("default" "std"))))))

