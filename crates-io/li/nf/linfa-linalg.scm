(define-module (crates-io li nf linfa-linalg) #:use-module (crates-io))

(define-public crate-linfa-linalg-0.1.0 (c (n "linfa-linalg") (v "0.1.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-rand") (r "^0.14") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "rand") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "rand_xoshiro") (r "^0.6") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "10mcfr8k0syjxj9g14v8iv3lr25j7c0nfw49cwrnv1y884mmdrsn") (f (quote (("iterative" "rand") ("default" "iterative"))))))

