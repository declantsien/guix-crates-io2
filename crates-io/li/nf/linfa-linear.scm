(define-module (crates-io li nf linfa-linear) #:use-module (crates-io))

(define-public crate-linfa-linear-0.2.1 (c (n "linfa-linear") (v "0.2.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "argmin") (r "^0.3.1") (f (quote ("ndarrayl"))) (d #t) (k 0)) (d (n "csv") (r "^1.1") (d #t) (k 2)) (d (n "flate2") (r "^1.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (f (quote ("blas" "approx"))) (d #t) (k 0)) (d (n "ndarray-csv") (r "^0.4") (d #t) (k 2)) (d (n "ndarray-linalg") (r "^0.12") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "openblas-src") (r "^0.9") (f (quote ("system"))) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)))) (h "03k8bihrgg6kfvhfirv31kzw0kmasqmr2rn1nq6y15bdkwsbxvbs")))

(define-public crate-linfa-linear-0.3.0 (c (n "linfa-linear") (v "0.3.0") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "argmin") (r "^0.3.1") (f (quote ("ndarrayl"))) (d #t) (k 0)) (d (n "linfa") (r "^0.3.0") (d #t) (k 0)) (d (n "linfa-datasets") (r "^0.3.0") (f (quote ("diabetes"))) (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (f (quote ("blas" "approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.12") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)))) (h "1algxlsm9k89l0cd4bzjpw0j2blxzislk3k7jzm6mjf1rlk7s95w")))

(define-public crate-linfa-linear-0.3.1 (c (n "linfa-linear") (v "0.3.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 2)) (d (n "argmin") (r "^0.3.1") (f (quote ("ndarrayl"))) (d #t) (k 0)) (d (n "linfa") (r "^0.3.1") (d #t) (k 0)) (d (n "linfa-datasets") (r "^0.3.1") (f (quote ("diabetes"))) (d #t) (k 2)) (d (n "ndarray") (r "^0.13") (f (quote ("blas" "approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.12") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0i5iz6mdxsi33dz8p017b417g02dwh1r7alnx4w9hacg2c3sjp8m")))

(define-public crate-linfa-linear-0.4.0 (c (n "linfa-linear") (v "0.4.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "argmin") (r "^0.4") (f (quote ("ndarrayl"))) (d #t) (k 0)) (d (n "linfa") (r "^0.4.0") (d #t) (k 0)) (d (n "linfa-datasets") (r "^0.4.0") (f (quote ("diabetes"))) (d #t) (k 2)) (d (n "ndarray") (r "^0.14") (f (quote ("blas" "approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.13") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0d514k280xcbih2wc30z4id4l9csm6sdyy2nxsrdapb1lhwij0m3")))

(define-public crate-linfa-linear-0.5.0 (c (n "linfa-linear") (v "0.5.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "argmin") (r "^0.4.6") (f (quote ("ndarrayl"))) (d #t) (k 0)) (d (n "linfa") (r "^0.5.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "linfa-datasets") (r "^0.5.0") (f (quote ("diabetes"))) (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("blas" "approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dp6v4128yi12id7kc74x791jxw0k94wj64rhm0zw1bb4as4cjhg")))

(define-public crate-linfa-linear-0.5.1 (c (n "linfa-linear") (v "0.5.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "argmin") (r "^0.4.6") (f (quote ("ndarrayl"))) (d #t) (k 0)) (d (n "linfa") (r "^0.5.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "linfa-datasets") (r "^0.5.1") (f (quote ("diabetes"))) (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (f (quote ("blas" "approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14") (d #t) (k 0)) (d (n "ndarray-stats") (r "^0.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01c6dsdrbsmj0m0b48ssa7algvkjfp6pr5av65f5sm8mdx9bxsdl")))

(define-public crate-linfa-linear-0.6.0 (c (n "linfa-linear") (v "0.6.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "argmin") (r "^0.4.6") (f (quote ("ndarray" "ndarray-rand"))) (d #t) (k 0)) (d (n "linfa") (r "^0.6.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "linfa-datasets") (r "^0.6.0") (f (quote ("diabetes"))) (d #t) (k 2)) (d (n "linfa-linalg") (r "^0.1") (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.14") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "147gl3fqagp59famvkqi7vh3grcivwmavqk8bg38nnfdnbqz7gz8") (f (quote (("blas" "ndarray-linalg" "linfa/ndarray-linalg" "argmin/ndarray-linalg"))))))

(define-public crate-linfa-linear-0.6.1 (c (n "linfa-linear") (v "0.6.1") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "argmin") (r "^0.4.6") (f (quote ("ndarray" "ndarray-rand"))) (d #t) (k 0)) (d (n "linfa") (r "^0.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "linfa-datasets") (r "^0.6.1") (f (quote ("diabetes"))) (d #t) (k 2)) (d (n "linfa-linalg") (r "^0.1") (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ifz4jq6n371j3djkbcnaimwwff5kv6r5ma6xv380gygddn7apsg") (f (quote (("blas" "ndarray-linalg" "linfa/ndarray-linalg" "argmin/ndarray-linalg"))))))

(define-public crate-linfa-linear-0.7.0 (c (n "linfa-linear") (v "0.7.0") (d (list (d (n "approx") (r "^0.4") (d #t) (k 2)) (d (n "argmin") (r "^0.8.1") (k 0)) (d (n "argmin-math") (r "^0.3") (f (quote ("ndarray_v0_15-nolinalg"))) (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "linfa") (r "^0.7.0") (d #t) (k 0)) (d (n "linfa") (r "^0.7.0") (f (quote ("benchmarks"))) (d #t) (k 2)) (d (n "linfa-datasets") (r "^0.7.0") (f (quote ("diabetes"))) (d #t) (k 2)) (d (n "linfa-linalg") (r "^0.1") (k 0)) (d (n "ndarray") (r "^0.15") (f (quote ("approx"))) (d #t) (k 0)) (d (n "ndarray-linalg") (r "^0.15") (o #t) (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde_crate") (r "^1.0") (f (quote ("std" "derive"))) (o #t) (k 0) (p "serde")) (d (n "statrs") (r "^0.16.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pz26jvpan1vn7y9xbgjip4zbkj582dzrmn90da0zjw3j8d91qvd") (f (quote (("serde" "serde_crate" "linfa/serde" "ndarray/serde" "argmin/serde1") ("blas" "ndarray-linalg" "linfa/ndarray-linalg"))))))

