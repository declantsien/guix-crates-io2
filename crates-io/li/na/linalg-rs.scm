(define-module (crates-io li na linalg-rs) #:use-module (crates-io))

(define-public crate-linalg-rs-1.1.2 (c (n "linalg-rs") (v "1.1.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "itertools") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)))) (h "09sxcbc95787cmw1lrc6hxqjrcb0shkipnfxdss6j8b61aiwddy7")))

