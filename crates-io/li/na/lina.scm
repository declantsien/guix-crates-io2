(define-module (crates-io li na lina) #:use-module (crates-io))

(define-public crate-lina-0.1.0 (c (n "lina") (v "0.1.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1iczjk4hwgm0300m744rn8mzsqzzwh05w676dkdj36acbbqwm5g2")))

(define-public crate-lina-0.1.1 (c (n "lina") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0jfiz7jsyjqw2p2ipkknxc45537bigj0f959iahwbgjz3z8y74xg")))

(define-public crate-lina-0.1.2 (c (n "lina") (v "0.1.2") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "08pyvjdhjdfpc08bw6bi4ia5m0wykvmhdc5wnfkmv27x49bglf8n")))

(define-public crate-lina-0.1.3 (c (n "lina") (v "0.1.3") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0fkplfm5br99lzxl1cdv8x264w67x17a1bff17wlyvp2vpaark85")))

(define-public crate-lina-0.1.4 (c (n "lina") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "104nxwn9ngq0wzvwfq00vj7jnyhrby8s54avpdbdscp1rrdjdvrd") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-lina-0.1.5 (c (n "lina") (v "0.1.5") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "0l6b1vrhyzmxk4kczd4hsnhy1hdydycxrwc3i278khyq4m559sds") (f (quote (("nightly") ("default" "nightly"))))))

(define-public crate-lina-0.2.0 (c (n "lina") (v "0.2.0") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1rxm3amjfj2k41h9x0n41v172ip5w6hjd69zvr3xdp52hg608vgq")))

(define-public crate-lina-0.2.1 (c (n "lina") (v "0.2.1") (d (list (d (n "bytemuck") (r "^1.7.2") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "1r1hjfy8bylcsg5wvl98z0qdmxb8h75agz5kiaa3dmy0k2ncnsyd")))

