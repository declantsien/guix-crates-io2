(define-module (crates-io li na linal) #:use-module (crates-io))

(define-public crate-linal-0.1.0 (c (n "linal") (v "0.1.0") (h "1ll2w0n0pzd6rwhzqfmdh0qwx1a0lawfbhnjv20m4v1k0jwiwqdv")))

(define-public crate-linal-0.2.0 (c (n "linal") (v "0.2.0") (h "07ckrqmzq0brzsg1r0vbqds2zgjkxk03klaidnfl6qr9kdvqnab9")))

(define-public crate-linal-0.2.3 (c (n "linal") (v "0.2.3") (h "038jqzlbc4pnwjc99k62zg7b8l372lbgirn9mcps84kqjl0fm51b")))

(define-public crate-linal-0.2.4 (c (n "linal") (v "0.2.4") (h "1hzw2g94k5kpz6mhljwgc7rs8hv4k79j20gl9n8dhsxl7757mnyp")))

(define-public crate-linal-0.2.5 (c (n "linal") (v "0.2.5") (h "1z0xy8pazsq1xfxik5ayl7wh9j5nagwvnwy405jjzlf8nvr0q502")))

