(define-module (crates-io li ns linspacers) #:use-module (crates-io))

(define-public crate-linspacers-0.1.0 (c (n "linspacers") (v "0.1.0") (h "1dmqg0dhdm2g4p7d263lq7k9bvwblmz8xpz5y5vv0dzvrf232lva")))

(define-public crate-linspacers-0.1.1 (c (n "linspacers") (v "0.1.1") (h "1da95sw1i51ak91vw2x4zwghxm3x99a1vy7yrmlv0798hxn7jz8l")))

