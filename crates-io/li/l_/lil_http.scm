(define-module (crates-io li l_ lil_http) #:use-module (crates-io))

(define-public crate-lil_http-0.1.0 (c (n "lil_http") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)))) (h "070333zqmdv9akkykgv3xyjk6wr8ya9fg4sap7v8200wii5sg07g")))

(define-public crate-lil_http-0.1.1 (c (n "lil_http") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "06a1limlwppkb24h36lndp717rz38868jndv6dahdgqcpik0z1i2")))

(define-public crate-lil_http-0.1.2 (c (n "lil_http") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.13") (f (quote ("json"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serial_test") (r "^0.10.0") (d #t) (k 2)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tokio-test") (r "^0.4.2") (d #t) (k 2)))) (h "1mzgp6v3fr0pyviglx58ql5c125k8vad01xwvwpawj4ka8y61lvw")))

