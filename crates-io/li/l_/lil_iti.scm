(define-module (crates-io li l_ lil_iti) #:use-module (crates-io))

(define-public crate-lil_iti-0.1.0 (c (n "lil_iti") (v "0.1.0") (d (list (d (n "powershell_script") (r "^1.1.0") (d #t) (k 0)))) (h "0dc2ca1bfg023q88pgdnx3m72m69q84p9g0kwgf3gf7ikihb7w63")))

(define-public crate-lil_iti-0.1.1 (c (n "lil_iti") (v "0.1.1") (d (list (d (n "powershell_script") (r "^1.1.0") (d #t) (k 0)))) (h "1h62f2rfyh4bbvc2bn7c25c4m72yn54lnfa8hdmmkyvy772drs98")))

(define-public crate-lil_iti-0.1.2 (c (n "lil_iti") (v "0.1.2") (d (list (d (n "powershell_script") (r "^1.1.0") (d #t) (k 0)))) (h "1pl9rkc3wrq8i1xy0r3slxzxka34vvxdkar2wihvrzdpq8x266id")))

(define-public crate-lil_iti-0.1.3 (c (n "lil_iti") (v "0.1.3") (d (list (d (n "powershell_script") (r "^1.1.0") (d #t) (k 0)))) (h "1dgdaksg4fzjay13w94ma2384ld8zfgj65cm1amk4c596kv15qb3")))

