(define-module (crates-io li bh libhdf5-sys) #:use-module (crates-io))

(define-public crate-libhdf5-sys-0.1.0 (c (n "libhdf5-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "1ipsqb2sxm2pz18qpwb4kz4li2rc6jcg6jckvm7kci7iabdvzghf")))

(define-public crate-libhdf5-sys-0.1.1 (c (n "libhdf5-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "03q5h0l8nldxmg7x9904d9klx7mj8pjhr0akkvrmz8j9nk2fmzg1")))

(define-public crate-libhdf5-sys-0.2.0 (c (n "libhdf5-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libhdf5-lib") (r "^0.2.0") (d #t) (k 0)) (d (n "libhdf5-lib") (r "^0.2.0") (d #t) (k 1)))) (h "0mklrfy3wxnbhw8vc00xiwcqy9hwsp34aaaj6fjy7bjv1h37nair")))

