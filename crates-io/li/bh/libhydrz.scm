(define-module (crates-io li bh libhydrz) #:use-module (crates-io))

(define-public crate-libhydrz-0.1.0 (c (n "libhydrz") (v "0.1.0") (d (list (d (n "minicbor") (r "^0.20.0") (f (quote ("alloc" "std" "derive"))) (d #t) (k 0)))) (h "1n8ypm0q7iwrwl5z6r85s3d4idmhq0im3s8srf9rpb826bj3vhlv")))

