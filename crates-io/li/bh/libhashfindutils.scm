(define-module (crates-io li bh libhashfindutils) #:use-module (crates-io))

(define-public crate-libhashfindutils-0.1.0 (c (n "libhashfindutils") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "home") (r "^0.5.5") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (f (quote ("user"))) (k 0)))) (h "1pd01yd1nng99ji0pbrqm2nv3145l53p7ci75mvq8588v429b4rc")))

