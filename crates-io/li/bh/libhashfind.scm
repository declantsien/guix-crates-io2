(define-module (crates-io li bh libhashfind) #:use-module (crates-io))

(define-public crate-libhashfind-0.1.0 (c (n "libhashfind") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.71") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "libhashfindutils") (r "^0.1.0") (d #t) (k 0)))) (h "0xic1jndff0z4xq230cc5kavf2gx44xj02r6sk6q9w1vi7s239i8")))

