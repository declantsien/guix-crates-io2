(define-module (crates-io li bh libharu-sys) #:use-module (crates-io))

(define-public crate-libharu-sys-0.0.1 (c (n "libharu-sys") (v "0.0.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "image") (r "^0.19.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.42") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1c6d01d6zsp0dg83m8sk41bjdafah0vnq06067m1idinlpc496lv") (l "haru")))

