(define-module (crates-io li bh libheif-rs) #:use-module (crates-io))

(define-public crate-libheif-rs-0.1.0 (c (n "libheif-rs") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1nd9qw55hc07nrq0lk1fq1l2f7mapwxp4yxcd114bwg0w62h2aap")))

(define-public crate-libheif-rs-0.2.0 (c (n "libheif-rs") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.4.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "17wmq2jnva5ksnnm97xbmhrc8cx0fnlx774arzg484pp19scpvfk")))

(define-public crate-libheif-rs-0.2.1 (c (n "libheif-rs") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.4.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1r20m22rslpq9d5hc7nhyswl7fpjpi18b4zp53drnbmqj4n1p1d2")))

(define-public crate-libheif-rs-0.3.0 (c (n "libheif-rs") (v "0.3.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.4.1") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0fb14a5w2d2vzg319yi5456iwrf2684nj1xfbrp980n64d57bqnr")))

(define-public crate-libheif-rs-0.4.0 (c (n "libheif-rs") (v "0.4.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1gj2qzk1afvazb3kdy7yqamfmkpdrs32b6rmgxm0l4jvpv92xq3j")))

(define-public crate-libheif-rs-0.5.0 (c (n "libheif-rs") (v "0.5.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.4.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0hc3xd1m029q88rmbcimr237h2hccpfz5wvsjmndr08pdcwj30yk")))

(define-public crate-libheif-rs-0.6.0 (c (n "libheif-rs") (v "0.6.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.4.2") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "16hlisb9yxcsi51r9sryljbsrdr2sicj8qk2hnxkv0d9y6lkfwan")))

(define-public crate-libheif-rs-0.7.0 (c (n "libheif-rs") (v "0.7.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0pkwq2chfb0mix97pzjn0n38bdx1aj6swv1xiqgcr8jxph75jj75")))

(define-public crate-libheif-rs-0.8.0 (c (n "libheif-rs") (v "0.8.0") (d (list (d (n "kamadak-exif") (r "^0.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ypxpmah70pa9yk6gj2w38533c766wbdxfyl7fb1ihgn95gvxlgw")))

(define-public crate-libheif-rs-0.9.0 (c (n "libheif-rs") (v "0.9.0") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.5") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1gndx5gx3sjb83cs7xb7g268a2ixvk3ij01mf8287y5c6z227n2p")))

(define-public crate-libheif-rs-0.9.1 (c (n "libheif-rs") (v "0.9.1") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "enumn") (r "^0.1.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0nc76zjsmx09b6rnxlxzk6d0mf1pyz9acjdkb8ygy6wdjw70pirc")))

(define-public crate-libheif-rs-0.9.2 (c (n "libheif-rs") (v "0.9.2") (d (list (d (n "doc-comment") (r "^0.3.1") (d #t) (k 2)) (d (n "enumn") (r "^0.1.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.5") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "1yf2gi3qsfbkbkigkbczmfm2zbysrrjxk3rndm2idxglm0zdr4fj")))

(define-public crate-libheif-rs-0.10.0 (c (n "libheif-rs") (v "0.10.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "enumn") (r "^0.1.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.2") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.8.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "0v2kw2rpw9rswjqn1sr94aagr91i589kn93hgvnra4w76aiiywmc")))

(define-public crate-libheif-rs-0.11.0 (c (n "libheif-rs") (v "0.11.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "enumn") (r "^0.1.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.2") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.8.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 2)))) (h "04v4pq76swlqmpxjhic0hih0xbddb2m3gj6j6avnr6wiww4vxhpv")))

(define-public crate-libheif-rs-0.12.0 (c (n "libheif-rs") (v "0.12.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "enumn") (r "^0.1.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "13fniv0gasqrqv9m25hkcwcv3sj9wrhbglf2qvzjp62m3znisj68")))

(define-public crate-libheif-rs-0.13.0 (c (n "libheif-rs") (v "0.13.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "enumn") (r "^0.1.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0py0x2a10298rac8bwj72i4wx5kfm96nysq01gacdscv8mrng9hj")))

(define-public crate-libheif-rs-0.13.1 (c (n "libheif-rs") (v "0.13.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "enumn") (r "^0.1.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "19rd3zair0wf0pl9b2462az5ryxry5sh92q06vma5vpslsbv9qfp")))

(define-public crate-libheif-rs-0.14.0 (c (n "libheif-rs") (v "0.14.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "enumn") (r "^0.1.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.3") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.11.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1g3gmw7g2i0jzsljll4qhm5cmxrdkq9vvgmgng1wgaxc9hq2yiv6")))

(define-public crate-libheif-rs-0.15.0 (c (n "libheif-rs") (v "0.15.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "enumn") (r "^0.1.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.4") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.12.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1xs9y8pjqzkn4p7xb5sawfy2nbfq0a5l7g8b4hnlz4qbgs7yja4s")))

(define-public crate-libheif-rs-0.15.1 (c (n "libheif-rs") (v "0.15.1") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "enumn") (r "^0.1.5") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.4") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.12.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0mhhyak12ysnv9f6s0mp99i8z15z7qv5x9z28va1777w059qpxgd")))

(define-public crate-libheif-rs-0.16.0 (c (n "libheif-rs") (v "0.16.0") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.14.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0hgykggvxv4c6b90dpsgmi5zafzwrjsmlz87lxzwx688lj9l4chb")))

(define-public crate-libheif-rs-0.17.0 (c (n "libheif-rs") (v "0.17.0") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.14.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0jj4p82v0yybkq67gb1kcz99x5sy70vmjvxp7qmjds8kx3jd0gs9")))

(define-public crate-libheif-rs-0.18.0 (c (n "libheif-rs") (v "0.18.0") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libheif-sys") (r "^1.14.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1k8cmmrfbh2hjwa0gwz3piphkdn65dk395fsxr70ink95l6rpl1p")))

(define-public crate-libheif-rs-0.19.0 (c (n "libheif-rs") (v "0.19.0") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libheif-sys") (r "^1.14.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1i7mg03nsfs2a69fhpwqfsagk4ziagrh6h3hdjil0pnqpc92rjvz")))

(define-public crate-libheif-rs-0.19.1 (c (n "libheif-rs") (v "0.19.1") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libheif-sys") (r "^1.14.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0j5pdblpk2dhdgr5zab8139pl79anqrrs8d6phf6341cbp2s2kin")))

(define-public crate-libheif-rs-0.19.2 (c (n "libheif-rs") (v "0.19.2") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.2") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libheif-sys") (r "^1.14.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "07q3w32p2b2ak3nmiwzlrl4bwc0wi7x6qm04jdck9wv942ycx7vl")))

(define-public crate-libheif-rs-0.20.0 (c (n "libheif-rs") (v "0.20.0") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libheif-sys") (r "^1.14") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1lwsmgqfslwwfmiqp7adi284v7jsj3wxa3x2ryp1a65bnbndxajq")))

(define-public crate-libheif-rs-0.21.0 (c (n "libheif-rs") (v "0.21.0") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libheif-sys") (r "^1.16") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0rsps065k6rnai76lkzrw8zxwx06bb8gfd04swbi5cijp2wxcjvm")))

(define-public crate-libheif-rs-0.22.0 (c (n "libheif-rs") (v "0.22.0") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libheif-sys") (r "^2") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1k9aablpi20md8yc012mb8w0kk193a7rzh5kbxa7pwblqyllyr3z") (f (quote (("use-bindgen" "libheif-sys/use-bindgen") ("default" "use-bindgen"))))))

(define-public crate-libheif-rs-1.0.0 (c (n "libheif-rs") (v "1.0.0") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libheif-sys") (r "^2.1") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0w5aw4ffnhnz3f3c7yzgbc75hz0fplyb72gynrv6cz91jxb76rpw") (f (quote (("use-bindgen" "libheif-sys/use-bindgen") ("default" "use-bindgen"))))))

(define-public crate-libheif-rs-1.0.1 (c (n "libheif-rs") (v "1.0.1") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libheif-sys") (r "^2.1") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "05x0awr69aqb125pm7w2x2sgvdzf3skbykj4yx5f1c324idndf6f") (f (quote (("use-bindgen" "libheif-sys/use-bindgen") ("default" "use-bindgen"))))))

(define-public crate-libheif-rs-1.0.2 (c (n "libheif-rs") (v "1.0.2") (d (list (d (n "enumn") (r "^0.1") (d #t) (k 0)) (d (n "four-cc") (r "^0.3") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libheif-sys") (r "^2.1") (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "01xrrv1is5i9capxvx3aj45q1vdw2a31rwbhf1q841wp1h52rhs0") (f (quote (("use-bindgen" "libheif-sys/use-bindgen") ("default" "use-bindgen"))))))

