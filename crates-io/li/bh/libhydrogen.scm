(define-module (crates-io li bh libhydrogen) #:use-module (crates-io))

(define-public crate-libhydrogen-0.1.0 (c (n "libhydrogen") (v "0.1.0") (d (list (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "failure_derive") (r "~0.1") (d #t) (k 0)) (d (n "libhydrogen-sys") (r "~0.6.2") (d #t) (k 0)))) (h "10yicjszckxfddwj7r3v3bywjksipnms3wpxyjnmmwa9axbzgi0w")))

(define-public crate-libhydrogen-0.1.1 (c (n "libhydrogen") (v "0.1.1") (d (list (d (n "failure") (r "~0.1") (d #t) (k 0)) (d (n "failure_derive") (r "~0.1") (d #t) (k 0)) (d (n "libhydrogen-sys") (r "~0.7") (d #t) (k 0)))) (h "1r2yq1wa7pqpizrxd3bvjv392l117hf9rigsjbadrk0ikzxx8rb1")))

(define-public crate-libhydrogen-0.1.2 (c (n "libhydrogen") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libhydrogen-sys") (r "^0.7.2") (d #t) (k 0)))) (h "1b1j7dndab7nrbfkw3992nv8wmq6w0di830j43viabwlh185bm7s")))

(define-public crate-libhydrogen-0.1.4 (c (n "libhydrogen") (v "0.1.4") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libhydrogen-sys") (r "^0.7.3") (d #t) (k 0)))) (h "1agqln7jy4ffdw6c149w595lnmmqzllxkpbnmv77mlg33dr7nbva")))

(define-public crate-libhydrogen-0.1.5 (c (n "libhydrogen") (v "0.1.5") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libhydrogen-sys") (r "^0.7.3") (d #t) (k 0)))) (h "04z4kpbyn4irj5bbl1j1ncdkyjv85a0nj8m7m5nkcdvqa77bfipy")))

(define-public crate-libhydrogen-0.2.0 (c (n "libhydrogen") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libhydrogen-sys") (r "^0.8.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gwmq8f4458v8c2q2zr1xf23iaxxqdl9bb01dd7yx799lfhvxr0k")))

(define-public crate-libhydrogen-0.2.1 (c (n "libhydrogen") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libhydrogen-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04l80fzfay80sd4dl9csbwjyys8kgz3f5bk5b24ighjaymdiyra6")))

(define-public crate-libhydrogen-0.3.0 (c (n "libhydrogen") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libhydrogen-sys") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1af48na0xlmx9b1d490zphz77jjwz89pfa3dl0c0lyqvc68n8nls")))

(define-public crate-libhydrogen-0.4.1 (c (n "libhydrogen") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libhydrogen-sys") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cc9bh3cmryvkgw756fn077r8pqfp34378swx0q7c5h0qdmd6yn2")))

(define-public crate-libhydrogen-0.4.2 (c (n "libhydrogen") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "libhydrogen-sys") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11hpgb3cjxdlyw7khv0jbcxihcl1w6gi7n417p2yhqgilm6ky9g5")))

