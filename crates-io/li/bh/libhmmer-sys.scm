(define-module (crates-io li bh libhmmer-sys) #:use-module (crates-io))

(define-public crate-libhmmer-sys-0.1.0 (c (n "libhmmer-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "01756pr3rnhynmyhma7fngvr07mm4ddbifa6qpp8lbr4wwkpnx4h") (l "hmmer")))

(define-public crate-libhmmer-sys-0.2.0 (c (n "libhmmer-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "07s60b2nh7qn946a323y73lfjhry94rv9yymj1pcg3mjpc4qw1nm") (l "hmmer")))

(define-public crate-libhmmer-sys-0.3.0 (c (n "libhmmer-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "1pvm9ww46rypsh4sy5q1vpxwg9pcqdq6x9bmp6nvh7a0h8sz4027") (l "hmmer")))

(define-public crate-libhmmer-sys-0.4.0 (c (n "libhmmer-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "fs_extra") (r "^1.2.0") (d #t) (k 1)) (d (n "num_cpus") (r "^1.0") (d #t) (k 1)))) (h "1nb423n3cmqx5jhivbs6avnrkikvrj157fhmddk93x60jw5r36ls") (l "hmmer")))

