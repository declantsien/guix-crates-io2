(define-module (crates-io li bh libhydrogen-sys) #:use-module (crates-io))

(define-public crate-libhydrogen-sys-0.1.0 (c (n "libhydrogen-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "gcc") (r "~0") (d #t) (k 1)))) (h "1j6yiq1gh0927fq9yg0hcjjzx6cqlb1v9wam52brh68grxvxa229")))

(define-public crate-libhydrogen-sys-0.2.0 (c (n "libhydrogen-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "gcc") (r "~0") (d #t) (k 1)))) (h "036i1zqgxznqbvpnysadkx50vzw3cm8wkmiwdh1adgr1yxymd1yz")))

(define-public crate-libhydrogen-sys-0.3.0 (c (n "libhydrogen-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "gcc") (r "~0") (d #t) (k 1)))) (h "14zk7fhj2m4n5r2cvdsdn5djb2y3kpzi8plj4zpb5fbrwz1x4c6m")))

(define-public crate-libhydrogen-sys-0.4.0 (c (n "libhydrogen-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "gcc") (r "~0") (d #t) (k 1)))) (h "0lwarxa7blsr3sz8ifzb73wlvnv32zs8y997xjnzyw7nabm66wyv")))

(define-public crate-libhydrogen-sys-0.5.0 (c (n "libhydrogen-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "gcc") (r "~0") (d #t) (k 1)))) (h "02w9skr66prixg1lmylgcgacf8sf8wjqb861axgxpp4pzr5w8acc")))

(define-public crate-libhydrogen-sys-0.6.0 (c (n "libhydrogen-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "gcc") (r "~0") (d #t) (k 1)))) (h "0vyrwvccwz8ik8wl32dhgxf5z2b1kk01k6agwiqy9f1vyfl3pf53")))

(define-public crate-libhydrogen-sys-0.6.1 (c (n "libhydrogen-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "gcc") (r "~0") (d #t) (k 1)))) (h "144df2hjdz03lxpmbr44cg12x4wdrc4rbjy2d07mxixlbjmpmxbl")))

(define-public crate-libhydrogen-sys-0.6.2 (c (n "libhydrogen-sys") (v "0.6.2") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "gcc") (r "~0") (d #t) (k 1)))) (h "02px6qs7asjic0yzw6cr15gl9d6x50rax7h7nhvq613wn05mvccn")))

(define-public crate-libhydrogen-sys-0.7.0 (c (n "libhydrogen-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "gcc") (r "~0") (d #t) (k 1)))) (h "1w53w480arw1b8r82jzvhibi54108slswy5f16rj9sv2zfy8hs49")))

(define-public crate-libhydrogen-sys-0.7.1 (c (n "libhydrogen-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "~0") (d #t) (k 1)) (d (n "gcc") (r "~0") (d #t) (k 1)))) (h "0c86lyw6zx2s7ki76b4y8rd9dcc6idrx4pxyncf9rdlh5jkqvhiz")))

(define-public crate-libhydrogen-sys-0.7.2 (c (n "libhydrogen-sys") (v "0.7.2") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0q08mj6mhrq8k3qsb2xj61iq82745rk1yl88kk5lm3l4bm3cyg28")))

(define-public crate-libhydrogen-sys-0.7.3 (c (n "libhydrogen-sys") (v "0.7.3") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "10fqa7nsz5zl92v7i501s0jrrsm02s7cjs1p53k7d0377p72f5j1")))

(define-public crate-libhydrogen-sys-0.8.0 (c (n "libhydrogen-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1z9svhgh5k0yhfd2fj12xr6zgrxnj9irc18wma7hr84hn7phprni")))

(define-public crate-libhydrogen-sys-0.8.1 (c (n "libhydrogen-sys") (v "0.8.1") (d (list (d (n "bindgen") (r ">= 0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1j94vl3n0cznr5j4qdmfbf25s3rwxwsv9pgf392rrvajqvscq5c4")))

(define-public crate-libhydrogen-sys-0.8.2 (c (n "libhydrogen-sys") (v "0.8.2") (d (list (d (n "bindgen") (r ">= 0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0p6czcacmajh4yjrnh1mjl7m3n0sklv050xdgr8340i8z77b43rr")))

(define-public crate-libhydrogen-sys-0.9.0 (c (n "libhydrogen-sys") (v "0.9.0") (d (list (d (n "bindgen") (r ">=0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "18lgmyisimscr368ygf48ik55qdnfh8d49vrq12ww4f345hl4n6y")))

(define-public crate-libhydrogen-sys-0.9.1 (c (n "libhydrogen-sys") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0sqnqk1laln2v9581rl2mv2faq4dldpsfb7ksda2y2z4am8v68xl")))

(define-public crate-libhydrogen-sys-0.9.2 (c (n "libhydrogen-sys") (v "0.9.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04rhvsjc0qn83015ah8h4i9khm78mz9nl0f42y1km3rw6wsxg1s0")))

(define-public crate-libhydrogen-sys-0.9.3 (c (n "libhydrogen-sys") (v "0.9.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "16cfhcq94d02v736kc4fs2psarj820949wqm96f6sqmdyzy5nbaj")))

