(define-module (crates-io li bh libhdf5-lib) #:use-module (crates-io))

(define-public crate-libhdf5-lib-0.1.0 (c (n "libhdf5-lib") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0lfqnffm7pk2ydc2g6vpr5s0r5s1n0p3k1qcwarfqrca4zv6mhav")))

(define-public crate-libhdf5-lib-0.2.0 (c (n "libhdf5-lib") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "15i8ihqz0vbbjxv2izr264d6kg3r21hfpm2wdkn1r58f4i6vvqdr")))

