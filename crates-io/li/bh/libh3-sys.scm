(define-module (crates-io li bh libh3-sys) #:use-module (crates-io))

(define-public crate-libh3-sys-0.1.0 (c (n "libh3-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0f3rf3d58px93dvik9z13200nvwgx9y18hkg4xcrld8yrv0zz0hh") (l "h3")))

(define-public crate-libh3-sys-0.1.1 (c (n "libh3-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0zy5v06nxgmr7p9805rh23zbhgq8ajh41bhyz6l576nznmmfjs5v") (l "h3")))

(define-public crate-libh3-sys-0.1.2 (c (n "libh3-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dy9bq7v5h6cprmdvav1cp262r92n5i6lk6h5cwc52abjr0ha93q") (l "h3")))

(define-public crate-libh3-sys-0.1.3 (c (n "libh3-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dfa4ab7j86hwjfv84a4kfpbad4npydv01kyi179v5jv6kw15r8i") (l "h3")))

