(define-module (crates-io li bh libheif-sys) #:use-module (crates-io))

(define-public crate-libheif-sys-1.4.0 (c (n "libheif-sys") (v "1.4.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "0c05qqw6nnc5wpsqpz97q0fkpi2jdly531fwl28n53ma1z7agnjs") (l "heif")))

(define-public crate-libheif-sys-1.4.1 (c (n "libheif-sys") (v "1.4.1") (d (list (d (n "bindgen") (r "^0.49") (o #t) (d #t) (k 1)))) (h "0hznxzfbgazkqbdhjns403y962m4kc6slgpk4nj8jq4pz07cpjbm") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.4.2 (c (n "libheif-sys") (v "1.4.2") (d (list (d (n "bindgen") (r "^0.50") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.60") (d #t) (k 0)))) (h "0a73hc7d6j81x273c4s8lsnyl5zramng9f934avksa6pnk0r3bhh") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.5.0 (c (n "libheif-sys") (v "1.5.0") (d (list (d (n "bindgen") (r "^0.51") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "09iz2rssjax1g509w5i2d08c9i5rj7i52lj806p5ic8mh4sy15x9") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.6.0 (c (n "libheif-sys") (v "1.6.0") (d (list (d (n "bindgen") (r "^0.51") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "139k68law9h3wps6fvxkwy0ikn5j3w4hk8g0xb70isy42hbg5ldd") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.8.0 (c (n "libheif-sys") (v "1.8.0") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)))) (h "166vdbai7rlpgf2lwcxsysxsn9nmjqihm3lyrcxhs92pfhynq858") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.8.1 (c (n "libheif-sys") (v "1.8.1") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)))) (h "0js1mf1v13qb5ny80c3mjigjm5rri9pw6p66s3h0xjwpafs1qspn") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.9.0 (c (n "libheif-sys") (v "1.9.0") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)))) (h "0k5zbva28ay3qva7a1an1fh22rc8ga6qnrc59fb2p04ba1m5jbl4") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.10.0 (c (n "libheif-sys") (v "1.10.0") (d (list (d (n "bindgen") (r "^0.56.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "14sm3wy6v3308qhch1a4lgyj18zlc2qimwghdb0hkxp3x8w0lpn5") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.11.0 (c (n "libheif-sys") (v "1.11.0") (d (list (d (n "bindgen") (r "^0.56.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)))) (h "03qnmw2idvlkfn86ham9k6plk9r900zivgm5i9iqkgaqkjfb0nwl") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.12.0 (c (n "libheif-sys") (v "1.12.0") (d (list (d (n "bindgen") (r "^0.58.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (d #t) (k 0)))) (h "0sjkp2k0vpryklpfxyjr3460d9ggc3f0vj5a6sb3prczg4y6nhn9") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.14.2 (c (n "libheif-sys") (v "1.14.2") (d (list (d (n "bindgen") (r "^0.63.0") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qzcrs42ahh4jzyr0hf8b019rw235hbzg1465rnhzq8s2njbgy3a") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.14.3 (c (n "libheif-sys") (v "1.14.3") (d (list (d (n "bindgen") (r "^0.65") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.15") (d #t) (k 1)))) (h "1pbglvp80vic1mif8khnhhxd2sz98qgr2fkclfl0p7bgrzpga2fk") (f (quote (("use-bindgen" "bindgen")))) (l "heif")))

(define-public crate-libheif-sys-1.14.4 (c (n "libheif-sys") (v "1.14.4") (d (list (d (n "bindgen") (r "^0.65") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "0hwni1pi2qmf328iqaqls6wr97vprhxlhyd1qlsb5scy6m0paz5i") (l "heif") (s 2) (e (quote (("use-bindgen" "dep:bindgen"))))))

(define-public crate-libheif-sys-1.16.0 (c (n "libheif-sys") (v "1.16.0") (d (list (d (n "bindgen") (r "^0.66") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1y8maykfwxfslc23hps4861b1ajvrjlmf31ssxfdqv4rdqahgzqc") (l "heif") (s 2) (e (quote (("use-bindgen" "dep:bindgen"))))))

(define-public crate-libheif-sys-1.16.1 (c (n "libheif-sys") (v "1.16.1") (d (list (d (n "bindgen") (r "^0.66") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "0gymx6013yzgda7j8lyd21q85brwy04l06x6w39xcwmmimcnpap8") (l "heif") (s 2) (e (quote (("use-bindgen" "dep:bindgen"))))))

(define-public crate-libheif-sys-1.16.2 (c (n "libheif-sys") (v "1.16.2") (d (list (d (n "bindgen") (r "^0.66") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "072mw23fv5nb2xmx7xa4g2ciafwvalfxqrms3wwr4n5rrqbrdv1g") (l "heif") (s 2) (e (quote (("use-bindgen" "dep:bindgen"))))))

(define-public crate-libheif-sys-2.0.0+1.16.2 (c (n "libheif-sys") (v "2.0.0+1.16.2") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)) (d (n "walkdir") (r "^2") (d #t) (t "cfg(windows)") (k 1)))) (h "1arswwmf7wx9ias28scs7a6w57h41wm8kazi5x9fjhl03csclj2d") (f (quote (("default" "use-bindgen")))) (l "heif") (s 2) (e (quote (("use-bindgen" "dep:bindgen"))))))

(define-public crate-libheif-sys-2.0.1+1.16.2 (c (n "libheif-sys") (v "2.0.1+1.16.2") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)) (d (n "walkdir") (r "^2") (d #t) (t "cfg(windows)") (k 1)))) (h "0nwl2w419jflkpmsq7w954njs259bx7jx5qs0hj8y8n3kczpdq3x") (f (quote (("default" "use-bindgen")))) (l "heif") (s 2) (e (quote (("use-bindgen" "dep:bindgen"))))))

(define-public crate-libheif-sys-2.1.0+1.17.4 (c (n "libheif-sys") (v "2.1.0+1.17.4") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)) (d (n "walkdir") (r "^2") (d #t) (t "cfg(windows)") (k 1)))) (h "08nd4h9253qll196qgg1s5ykplqch4briw3mv17x5hzmkjzxbf81") (f (quote (("default" "use-bindgen")))) (l "heif") (s 2) (e (quote (("use-bindgen" "dep:bindgen"))))))

(define-public crate-libheif-sys-2.1.1+1.17.4 (c (n "libheif-sys") (v "2.1.1+1.17.4") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)) (d (n "walkdir") (r "^2") (d #t) (t "cfg(windows)") (k 1)))) (h "1y3iivwsd7lbssdyryzryhmmbpryijhhjkrlc6x2mazk3sz2j2zn") (f (quote (("default" "use-bindgen")))) (l "heif") (s 2) (e (quote (("use-bindgen" "dep:bindgen"))))))

