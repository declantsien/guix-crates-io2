(define-module (crates-io li bh libharu) #:use-module (crates-io))

(define-public crate-libharu-0.1.2 (c (n "libharu") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 2)) (d (n "libharu-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1ja64wcx4cyzjgjkwyzvld43095rsxr1xlzhgpn0clpbrphhswvw")))

(define-public crate-libharu-0.1.4 (c (n "libharu") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 2)) (d (n "libharu-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0igf43mby9hhkc9swp5bgsa8xpwis2wwc5vv7jwm3h79adggywiy")))

(define-public crate-libharu-0.1.5 (c (n "libharu") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.44") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.28") (d #t) (k 2)) (d (n "libharu-sys") (r "^0.0.1") (d #t) (k 0)))) (h "12l1k6lf067zx210yg944g69f46pxn4r2m82lwzmxc3251iv8lhg")))

