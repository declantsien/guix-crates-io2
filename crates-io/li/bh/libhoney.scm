(define-module (crates-io li bh libhoney) #:use-module (crates-io))

(define-public crate-libhoney-0.0.1 (c (n "libhoney") (v "0.0.1") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1j83cnfndvy8ls48an0wg7w2nwrdcwhlsqcmbmag3249zaddc1ml")))

(define-public crate-libhoney-0.0.2 (c (n "libhoney") (v "0.0.2") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1qchx1iw581hqivyk0i7p64j98dfwd74ki9zkj39qf53xqabrjng")))

(define-public crate-libhoney-0.0.3 (c (n "libhoney") (v "0.0.3") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "1089nnyqnbpx879wq6v8kv02qisd305cg8w3i53avvdg70rybvh9")))

(define-public crate-libhoney-0.0.4 (c (n "libhoney") (v "0.0.4") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "09wwrn9pfj55yxmc7w6w1fnngkbmzszx1pp09ig8f5qihyvna03n")))

(define-public crate-libhoney-0.0.5 (c (n "libhoney") (v "0.0.5") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "hyper-tls") (r "^0.1.2") (d #t) (k 0)) (d (n "maplit") (r "^1.0.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.5") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)))) (h "0plmnyrm48ghlrs71lsj54sqgq80b5zqvlw3fgnbbc7lzdmlp8jf")))

