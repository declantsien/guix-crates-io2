(define-module (crates-io li bh libharu_ng) #:use-module (crates-io))

(define-public crate-libharu_ng-0.0.1+1 (c (n "libharu_ng") (v "0.0.1+1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "1sh43xiji0bnpxwdqak2mk66rnll2fmy5r3mfx449wgar6dlwrq1") (y #t)))

(define-public crate-libharu_ng-0.0.1-2 (c (n "libharu_ng") (v "0.0.1-2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "1g58c508z642wfw76yi2m4y1l1nsp61s1sdsiyjkbhgg5qn6gjqa") (y #t)))

(define-public crate-libharu_ng-0.0.2 (c (n "libharu_ng") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0dd1bnd0y7a6i0b85najrq17q0pp0jhcz0v573wyb60nfaz4ygcc") (y #t)))

(define-public crate-libharu_ng-0.0.3 (c (n "libharu_ng") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "12g019b49fzvrrj5fr3bw0ghanqjvzw2j0r5z25hh08n3jvgppc1") (y #t)))

(define-public crate-libharu_ng-0.0.4-pr1 (c (n "libharu_ng") (v "0.0.4-pr1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "1pp45s0b3zxh7l2sga9i9iamnngzpkmm2z67nqrb9ka3mnslpcbx") (y #t)))

(define-public crate-libharu_ng-0.0.5 (c (n "libharu_ng") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0zzpz00j2agiafwpq5l7rv4v0yl5qmbx46lgjkjmkixbw512a9jr") (y #t)))

(define-public crate-libharu_ng-0.0.6 (c (n "libharu_ng") (v "0.0.6") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0x10vh42fcvk138hbrzy8d3p97rv7jrrczk36vigmz0z0xjvcsgz") (y #t)))

(define-public crate-libharu_ng-0.0.7 (c (n "libharu_ng") (v "0.0.7") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0da7jjyzpgi8nsac7h8fdbaxjny8b4mmnqcj3c9sngj2scfyf9bx") (y #t)))

(define-public crate-libharu_ng-0.0.8 (c (n "libharu_ng") (v "0.0.8") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0hrjmjz47mmv8sa1s42fb8chw8bm74pjchasn0yrsd3m0v1zinpv") (y #t)))

(define-public crate-libharu_ng-0.0.9 (c (n "libharu_ng") (v "0.0.9") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)))) (h "0l0hjnxy1xfbr5bk7fd81j1498higafq71blg7z0r5skhsl0bxl0") (y #t)))

(define-public crate-libharu_ng-0.0.12 (c (n "libharu_ng") (v "0.0.12") (h "1qbrlcc0pxw84aa9xm4ibqa5hdk5yzpf64bxzrs8fkkarrba3zkb") (y #t)))

(define-public crate-libharu_ng-0.0.13 (c (n "libharu_ng") (v "0.0.13") (h "0kl4mhhbrx8b4g89q2g33ygmx2fz2038ba8rcx2wg83zqwmyx71f") (y #t)))

(define-public crate-libharu_ng-0.0.14 (c (n "libharu_ng") (v "0.0.14") (h "1da5bigh3kiv599pj7v1mn65vl0zyz6yq8h1ry8sjr9m8qzsh98f") (y #t)))

(define-public crate-libharu_ng-0.0.15 (c (n "libharu_ng") (v "0.0.15") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "14znna3s5k473wcykkizw52fnk1p67q78zsfzil0fjnm356aqmqn") (y #t)))

(define-public crate-libharu_ng-0.0.16 (c (n "libharu_ng") (v "0.0.16") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06mhxcy8g22hksrd8advl5ra1b1p29v7d2vw7ly1myfmr0jdqr13") (y #t)))

(define-public crate-libharu_ng-0.0.18 (c (n "libharu_ng") (v "0.0.18") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1vw35vmk0677s8swgmgaf6n8pc0nikiwldv2zwnnixjvj2g4kwa4") (y #t)))

(define-public crate-libharu_ng-0.0.19 (c (n "libharu_ng") (v "0.0.19") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "100y6dbsl4d7qiqfrd5inla2n2j84hfwd09a9bpdbap7ks5vzb49") (y #t)))

(define-public crate-libharu_ng-0.0.20 (c (n "libharu_ng") (v "0.0.20") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1f7a8pjv0vmv4aj0pqab2s3jzrjbcll4b7hh490p1zyy4x9dqqah") (y #t)))

(define-public crate-libharu_ng-0.0.21 (c (n "libharu_ng") (v "0.0.21") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06gd0jm20fisd39dqb9hdb0g5qvxq1yly3q6chz94rhp4kjb95w8") (y #t)))

(define-public crate-libharu_ng-0.0.23 (c (n "libharu_ng") (v "0.0.23") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "13hr5dbszvip72h2862xmfqkv6y5i4mw14fmj4jp3qxaw9ijn9d1") (y #t)))

(define-public crate-libharu_ng-0.0.25 (c (n "libharu_ng") (v "0.0.25") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "0sh1jaac87q0ag0iry87qbjz65pmsampp409wjr638zq0an437vf") (y #t)))

(define-public crate-libharu_ng-0.0.26 (c (n "libharu_ng") (v "0.0.26") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "0079ynhbqfpljdkz7j9wa37ciakdv6sy7wxlbhlqjn0ac0vizzdi") (y #t)))

(define-public crate-libharu_ng-0.1.0 (c (n "libharu_ng") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "030bg26ga6936pah5ckk8f25fv24avqabmgj414i13j0ghnm0pkj")))

(define-public crate-libharu_ng-0.1.1 (c (n "libharu_ng") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "1q2pxzz2p7lbrjim49z7mb2sa9lkk14di3l293b61k50nm1g52rs")))

(define-public crate-libharu_ng-0.1.2 (c (n "libharu_ng") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "17mw1kv6ifiwcb6ywrf5bx0n6zjljnjgfk35gpnwn68mhxcvyb7i")))

(define-public crate-libharu_ng-0.1.3 (c (n "libharu_ng") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "00g6w14xr5xwsbn0z1g6r7qrvjx9bqrd7adzrp5xjhim4zkx8gip")))

(define-public crate-libharu_ng-0.1.4 (c (n "libharu_ng") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "1s8f924jinl34zmb2qkqrncncvbk0af0lhabfkr2c6hxm4cpdj4y")))

(define-public crate-libharu_ng-0.1.5 (c (n "libharu_ng") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "16dx3jcfl645x7qidiqwq5c4jb74wv2iz5j9fdvbbgwc03w567ah")))

(define-public crate-libharu_ng-0.1.6 (c (n "libharu_ng") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "1y9qgzcj53792p17dr8arx8nyfjbi1la8gy5ynwf78q5gk07a2vf")))

(define-public crate-libharu_ng-0.1.8 (c (n "libharu_ng") (v "0.1.8") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)))) (h "13pakhls74padi2i94vkdv05sygywjbhh74nfv22am15crp5by2h")))

(define-public crate-libharu_ng-0.1.9 (c (n "libharu_ng") (v "0.1.9") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0craj8jlkhyn126x6c6rb96ari8qb61qi1m7h3hvgmmab21p1waw")))

(define-public crate-libharu_ng-0.1.10 (c (n "libharu_ng") (v "0.1.10") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "09fgfpk4342yxrxcnw2x76xnb0rq26kj2xwg4g9dn1qknbcv95jv")))

(define-public crate-libharu_ng-1.0.0 (c (n "libharu_ng") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0lk1xdbmn47b0hjb06w4kbvcgn23lwi02qlp7b58dfgx2ngg98g8")))

(define-public crate-libharu_ng-1.0.1 (c (n "libharu_ng") (v "1.0.1") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1d083r884b0b7paj2vnjg29cxr9ks9rcz279hmybrykc5vzlrwp6")))

(define-public crate-libharu_ng-1.0.2 (c (n "libharu_ng") (v "1.0.2") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1vmlcjsx9h6yp19knzymlns62s7gx5lklrif1fp889qcxxcdl59k")))

(define-public crate-libharu_ng-1.0.3 (c (n "libharu_ng") (v "1.0.3") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "07b725wpbgypi6lyn6a2p14a8avgha7p4bn01xsby8811qigj4c9")))

(define-public crate-libharu_ng-1.0.4 (c (n "libharu_ng") (v "1.0.4") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1c0nxkmqj28azx3v9yyphyx63pr9zpqbqkp50m486gaznas7l8dw")))

(define-public crate-libharu_ng-1.0.5 (c (n "libharu_ng") (v "1.0.5") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1kfy9lj3zql0y012gxrrb1c1lx7i6x3q98i2ws83jmja0q4mnk2x")))

(define-public crate-libharu_ng-1.0.6 (c (n "libharu_ng") (v "1.0.6") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1sdb79k46xjyilpb83wc1190fwy4gsjjgq27yk43pr4q88frm62r")))

(define-public crate-libharu_ng-1.0.7 (c (n "libharu_ng") (v "1.0.7") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1n94v4dvcmkn40daczyn4lysgsbw7vz798djbj4m41xas8n7h7cv")))

(define-public crate-libharu_ng-1.0.8 (c (n "libharu_ng") (v "1.0.8") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "02n1x165kq3m88nagcp02w0nbd9vvvini0463dqw9lfhi6xl8yc8")))

