(define-module (crates-io li bh libhook) #:use-module (crates-io))

(define-public crate-libhook-0.0.1 (c (n "libhook") (v "0.0.1") (d (list (d (n "base-coroutine") (r "^0.0.1") (d #t) (k 0)) (d (n "cbindgen") (r "^0.24.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "rustversion") (r "^1.0.6") (d #t) (k 1)) (d (n "timer-utils") (r "^0.0.1") (d #t) (k 0)))) (h "0kx1jdh5vckfgaxcpgx5xddwsw3anpb8p5mlbnyln1jpsx34p626")))

(define-public crate-libhook-0.0.2 (c (n "libhook") (v "0.0.2") (d (list (d (n "base-coroutine") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "timer-utils") (r "^0.0.1") (d #t) (k 0)))) (h "0p8jgcbphyc8i6v3lld1qar5l56p49n4pnhy2k3xzn0p6hv5bivq")))

(define-public crate-libhook-0.0.3 (c (n "libhook") (v "0.0.3") (d (list (d (n "base-coroutine") (r "^0.0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.119") (d #t) (k 0)) (d (n "timer-utils") (r "^0.0.1") (d #t) (k 0)))) (h "1lp0w9lwzm9g1nbmfj601xb0q3iqq4sjn33xl6nvsm48d8ki6nfg")))

(define-public crate-libhook-0.1.0 (c (n "libhook") (v "0.1.0") (d (list (d (n "base-coroutine") (r "^0.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.138") (d #t) (k 0)) (d (n "num_cpus") (r "^1.14.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "timer-utils") (r "^0.0.3") (d #t) (k 0)) (d (n "detour2") (r "^0.9.0") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows-sys") (r "^0.42.0") (f (quote ("Win32_Foundation" "Win32_System_Diagnostics_Debug" "Win32_System_Threading" "Win32_Security" "Win32_System_LibraryLoader" "Win32_System_SystemServices"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0rd7k49msm4dcl4hq08m0ab102msyl1x9w5q7r2idi3fnxn99cdk")))

