(define-module (crates-io li bh libhumancode) #:use-module (crates-io))

(define-public crate-libhumancode-1.0.0 (c (n "libhumancode") (v "1.0.0") (d (list (d (n "libzbase32") (r "^1.1.0") (k 0)) (d (n "reed-solomon-32") (r "^1.0.0") (k 0)))) (h "079h6qrzck5hj16cy6kx0q6m9li0sfvdnvmjv1n25n62bi4d2rhj") (f (quote (("std" "libzbase32/std" "reed-solomon-32/std") ("default" "std"))))))

(define-public crate-libhumancode-2.0.0 (c (n "libhumancode") (v "2.0.0") (d (list (d (n "libzbase32") (r "^2.0.0") (k 0)) (d (n "reed-solomon-32") (r "^2.0.0") (k 0)))) (h "1g50g6plnd0injc9pd292z4av1am0j39m0cdnsgy72f2fxbkk3kh") (f (quote (("std" "libzbase32/std" "reed-solomon-32/std") ("default" "std"))))))

(define-public crate-libhumancode-2.0.1 (c (n "libhumancode") (v "2.0.1") (d (list (d (n "libzbase32") (r "^2.0.0") (k 0)) (d (n "reed-solomon-32") (r "^2.0.0") (k 0)))) (h "11vqd96jyrhi52yv9990p5gik42gzy2djbfamdrv12nbh6mf4dw4") (f (quote (("std" "libzbase32/std" "reed-solomon-32/std") ("default" "std"))))))

