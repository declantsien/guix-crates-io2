(define-module (crates-io li bh libhermesabi-sys) #:use-module (crates-io))

(define-public crate-libhermesabi-sys-0.1.0 (c (n "libhermesabi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "git2") (r "^0.18.2") (d #t) (k 1)))) (h "052ws2w29f2s10c1z6lxb3k5wwn51ry6wgf40gi0mj1svz485aam") (y #t)))

(define-public crate-libhermesabi-sys-0.1.1 (c (n "libhermesabi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "11yjkkgspladii8mxqvql507ba29vs6v6nsw0fndwyhrbqmjnrqj") (y #t)))

