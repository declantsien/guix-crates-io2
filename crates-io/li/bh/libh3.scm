(define-module (crates-io li bh libh3) #:use-module (crates-io))

(define-public crate-libh3-0.1.0 (c (n "libh3") (v "0.1.0") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "libh3-sys") (r "^0.1.1") (d #t) (k 0)))) (h "02mdz94gv5m6250ahfhina7f67wdw16n5rr01341j6dj1cnzyd9i")))

(define-public crate-libh3-0.1.1 (c (n "libh3") (v "0.1.1") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "libh3-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0iqxnw9cbxfamc26p75bzbffvmklv2b5amlhhg5h0p1jba3fy8zf")))

(define-public crate-libh3-0.1.2 (c (n "libh3") (v "0.1.2") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "libh3-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0rvppm7b92djcnm42k5sqg7rdy9sw68ar4lp8ckzj1bh0lyxj8gs")))

(define-public crate-libh3-0.1.3 (c (n "libh3") (v "0.1.3") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "libh3-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0h7b15ll227bm02wfmgggg0ij4jrx747q5vvnbgkq0fbi6l53w01")))

(define-public crate-libh3-0.1.4 (c (n "libh3") (v "0.1.4") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "libh3-sys") (r "^0.1.1") (d #t) (k 0)))) (h "03aaqwhqs1qjv2dld9qmv95x50x9r7kwqc9a9wd8x87aw5nq74fn")))

(define-public crate-libh3-0.1.5 (c (n "libh3") (v "0.1.5") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "libh3-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0d11y5nzj7wbcd062wp0mxi5nrv68p5w75z05z801sn1aqakdgnj")))

(define-public crate-libh3-0.1.6 (c (n "libh3") (v "0.1.6") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "libh3-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1pl30dcvjm7rlw5zjmly3n7gc4h1rm3g68rawkca38dlavrwq7ps")))

(define-public crate-libh3-0.1.7 (c (n "libh3") (v "0.1.7") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "libh3-sys") (r "^0.1.3") (d #t) (k 0)))) (h "18d6kgspa90r0mr1j0lylp4a28r6s22dwr4ahhfxzzc1c460l9bk")))

(define-public crate-libh3-0.1.8 (c (n "libh3") (v "0.1.8") (d (list (d (n "float-cmp") (r "^0.6.0") (d #t) (k 0)) (d (n "libh3-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1ri3rvifxaqvqpi31cn5sf12pnbjnhfgbg55ym406yahrxlwcf0f")))

