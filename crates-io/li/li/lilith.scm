(define-module (crates-io li li lilith) #:use-module (crates-io))

(define-public crate-lilith-0.1.0 (c (n "lilith") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.19.0") (d #t) (k 0)) (d (n "raw-window-handle") (r "^0.3.1") (d #t) (k 0)) (d (n "slotmap") (r "^0.4.0") (d #t) (k 0)) (d (n "wgpu") (r "^0.4.0") (d #t) (k 0)) (d (n "winit") (r "^0.20.0-alpha5") (d #t) (k 2)))) (h "1yawxbv7y85mdwlsmi2g532i7nzpk1x8w8r7n3a1rqdqdfznhdnr")))

