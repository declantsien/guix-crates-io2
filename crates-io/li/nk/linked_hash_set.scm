(define-module (crates-io li nk linked_hash_set) #:use-module (crates-io))

(define-public crate-linked_hash_set-0.1.0 (c (n "linked_hash_set") (v "0.1.0") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "0wpi267kfc997z94y4z9n0k8j9p68z2axcialh7yi201w3pg2zjf")))

(define-public crate-linked_hash_set-0.1.1 (c (n "linked_hash_set") (v "0.1.1") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "16dqx6nzh2irdrcrvamxw44z9f6n9b7ygbl9xw109m7vik5dzz6c")))

(define-public crate-linked_hash_set-0.1.2 (c (n "linked_hash_set") (v "0.1.2") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "1qk3ndcmb2cnq16yrp1n4y4a4kzycrk3bi0lfbphzr9ds663laxm")))

(define-public crate-linked_hash_set-0.1.3 (c (n "linked_hash_set") (v "0.1.3") (d (list (d (n "linked-hash-map") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "177zaga6mn0srr3rq91pphq6ig85d8gf2nsfghplzsxvqz292z1w")))

(define-public crate-linked_hash_set-0.1.4 (c (n "linked_hash_set") (v "0.1.4") (d (list (d (n "linked-hash-map") (r "^0.5.3") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "124m7wiz9ah7ah58ckai413mzfglh3y1nz64qy1s676qlinnq627")))

