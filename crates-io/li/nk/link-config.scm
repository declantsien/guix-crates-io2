(define-module (crates-io li nk link-config) #:use-module (crates-io))

(define-public crate-link-config-0.0.1 (c (n "link-config") (v "0.0.1") (h "0kkhbm4xs4i3wciwdvb2wn8fn2p0j92xapl8j7xk5kxrx9k4xw40") (y #t)))

(define-public crate-link-config-0.0.2 (c (n "link-config") (v "0.0.2") (h "1ipxwlf97bfrj4wlc2hcbj8shx6dxx2sr5h8g2h0r98d0p9mpkh3") (y #t)))

(define-public crate-link-config-0.1.0 (c (n "link-config") (v "0.1.0") (h "10q9rfq1kh1vn1jl0mmgk0as0w11d4ffmsljsqr35w9zb1g46pr0") (y #t)))

(define-public crate-link-config-0.1.1 (c (n "link-config") (v "0.1.1") (h "0d8vixywrm1zg8wyal6hd32c1j28h6rxg0zlwywmzlp4154gwz9z") (y #t)))

