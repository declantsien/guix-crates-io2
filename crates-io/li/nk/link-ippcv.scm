(define-module (crates-io li nk link-ippcv) #:use-module (crates-io))

(define-public crate-link-ippcv-0.1.0 (c (n "link-ippcv") (v "0.1.0") (d (list (d (n "ipp-sys-build-help") (r "^0.1") (d #t) (k 1)) (d (n "link-ippcore") (r "^0.1") (d #t) (k 0)))) (h "0j6533z6ss3yrwanlf55rszwh1f63cvd80q1dzzfm8g9m0a6s1zx")))

(define-public crate-link-ippcv-0.1.1 (c (n "link-ippcv") (v "0.1.1") (d (list (d (n "ipp-sys-build-help") (r "^0.1") (d #t) (k 1)) (d (n "link-ippcore") (r "^0.1") (d #t) (k 0)) (d (n "link-ipps") (r "^0.1") (d #t) (k 0)))) (h "0g5vz9lq3sdgy910z7v5sawx7wclgy0wvf884jxxw2jlkm80zjsg")))

(define-public crate-link-ippcv-0.1.2 (c (n "link-ippcv") (v "0.1.2") (d (list (d (n "ipp-sys-build-help") (r "^0.1.2") (d #t) (k 1)) (d (n "link-ippcore") (r "^0.1") (d #t) (k 0)) (d (n "link-ipps") (r "^0.1") (d #t) (k 0)))) (h "0625giri6r8z2piwxahmslnfl1ci28lyin5l1rq6cc9znwprpk98") (l "libippcv")))

