(define-module (crates-io li nk linkmap) #:use-module (crates-io))

(define-public crate-linkmap-0.1.0 (c (n "linkmap") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.6.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.6.0") (d #t) (k 0)))) (h "09sycw5wb9fqhyap08zmjzkcw0ajlcvsxwsrrynab29r3r9qcjha")))

(define-public crate-linkmap-0.1.1 (c (n "linkmap") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 0)) (d (n "symbolic-common") (r "^8.6.0") (d #t) (k 0)) (d (n "symbolic-demangle") (r "^8.6.0") (d #t) (k 0)))) (h "059rlji7ajsj61rs6vp6x1npff6wbyc87yrbccw3xqdark216kb0")))

