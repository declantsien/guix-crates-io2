(define-module (crates-io li nk linkchecker) #:use-module (crates-io))

(define-public crate-linkchecker-0.1.1 (c (n "linkchecker") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "colored") (r "^1.8.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ntest") (r "^0.3.1") (d #t) (k 2)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "simplelog") (r "^0.7.3") (d #t) (k 0)) (d (n "url") (r "^2.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1k4zg225d163c4j6gam6b83cbdkvwbi8x1ak0n0190pmvqy969bg")))

(define-public crate-linkchecker-0.2.0 (c (n "linkchecker") (v "0.2.0") (h "0ascvwgg6l8lbry8zrpkhnnqjgf6f1a7qdxhkc7083s9b1gs89rr")))

