(define-module (crates-io li nk linked-hash-map-rs) #:use-module (crates-io))

(define-public crate-linked-hash-map-rs-0.1.0 (c (n "linked-hash-map-rs") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 2)))) (h "03yf9zxcq879ji19mi172sb1kjkazbwpd60azjqh0irvfi7wrnpw")))

(define-public crate-linked-hash-map-rs-0.1.1 (c (n "linked-hash-map-rs") (v "0.1.1") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 2)))) (h "08x5s20h97jyb91rvfs44qz8j4rka4mdlzivp28zwbsy1rs9j8q6")))

(define-public crate-linked-hash-map-rs-0.1.2 (c (n "linked-hash-map-rs") (v "0.1.2") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0n0yxzh9jwjf5vc8iskkjzw4jw7crv62vkr3zzj0ixg2s6w8h4zp") (f (quote (("default"))))))

(define-public crate-linked-hash-map-rs-0.1.3 (c (n "linked-hash-map-rs") (v "0.1.3") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "13d2nsfg89wcvph82gxjz39fr0v8dmy7kqlafclwzrqv6m1rgzn2") (f (quote (("default"))))))

(define-public crate-linked-hash-map-rs-0.1.4 (c (n "linked-hash-map-rs") (v "0.1.4") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1v5ccfd2910m0496dwv81chxf9vsbav5advl2hmr52jqqq17s8vg") (f (quote (("default"))))))

(define-public crate-linked-hash-map-rs-0.1.5 (c (n "linked-hash-map-rs") (v "0.1.5") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0wydrklrk9bw75p8wqsr2wrk576jasr6lgsghcfjd9z0fmvn8d1x") (f (quote (("default"))))))

(define-public crate-linked-hash-map-rs-0.1.6 (c (n "linked-hash-map-rs") (v "0.1.6") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1fg9vixdgjyghdrkqph08xly41vqv9l58dsv80vax626hn8zl7q7") (f (quote (("default"))))))

(define-public crate-linked-hash-map-rs-0.1.7 (c (n "linked-hash-map-rs") (v "0.1.7") (d (list (d (n "ahash") (r "^0.7.4") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "linked-hash-map") (r "^0.5.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0a00b68f66s4wmhhz8zzw8lpfahihqmksiyxy7zlj0zcck4pgd6z") (f (quote (("default"))))))

