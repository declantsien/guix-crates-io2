(define-module (crates-io li nk linkbot) #:use-module (crates-io))

(define-public crate-linkbot-0.0.1 (c (n "linkbot") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "0sy7bf0pqbki6y53i3v4k67v43zkg7crqllxxzjarcwpb7wx2i6d")))

(define-public crate-linkbot-0.0.2 (c (n "linkbot") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "0y8h5nv9m2h73wgkyqd70aka8hqpyf2kw7fjqdiihz3ym66d8f7v")))

