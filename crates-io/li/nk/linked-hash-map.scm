(define-module (crates-io li nk linked-hash-map) #:use-module (crates-io))

(define-public crate-linked-hash-map-0.0.1 (c (n "linked-hash-map") (v "0.0.1") (h "17zwyqvf5k2wnya4bwrpcf86ps74dcin434fac05wbk46hs757va")))

(define-public crate-linked-hash-map-0.0.2 (c (n "linked-hash-map") (v "0.0.2") (h "1r926bppxfzlby2r6x90s0cb611nag52jikvb4i8adsh8h1pwv4y")))

(define-public crate-linked-hash-map-0.0.3 (c (n "linked-hash-map") (v "0.0.3") (h "0ymir5jk050bqgqpywswm6l690bx2frdq7h2rwwwpj4afnnqshrw")))

(define-public crate-linked-hash-map-0.0.4 (c (n "linked-hash-map") (v "0.0.4") (h "05xz9awnhnw3p2wp8pxbf93scvky76a6xnhjc6yq0xbhf7vwqg94")))

(define-public crate-linked-hash-map-0.0.5 (c (n "linked-hash-map") (v "0.0.5") (h "0ishxyi30isfllc3gjdb929dpmh6bf4kzsaq9wxrqb99xw4vvjw5")))

(define-public crate-linked-hash-map-0.0.6 (c (n "linked-hash-map") (v "0.0.6") (h "0nyj88bf7xsbmgv4swn5m1iddwlfkgs9h0p5xg0biy3lblvb4y0b")))

(define-public crate-linked-hash-map-0.0.7 (c (n "linked-hash-map") (v "0.0.7") (h "12miiifp2c874a3wngb1gmyiqyb4yysdahf45rirjymrxv9i5p9b")))

(define-public crate-linked-hash-map-0.0.8 (c (n "linked-hash-map") (v "0.0.8") (h "08kpx47y8yj7i77pmw4aabd46dh3l6ixrqb6g6rhnklcqykbay5b") (y #t)))

(define-public crate-linked-hash-map-0.0.9 (c (n "linked-hash-map") (v "0.0.9") (h "0yfxr2z2w1a8zwnx0l4dhqk3pfs232v59kyc47wzv6g9m8xzzxw3") (f (quote (("nightly"))))))

(define-public crate-linked-hash-map-0.0.10 (c (n "linked-hash-map") (v "0.0.10") (d (list (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (o #t) (d #t) (k 0)))) (h "07538wamqkqsbrsmv49n5dhxndcx82jh453vab6hjhw81hg9cvpj") (f (quote (("serde_impl" "serde" "serde_json") ("nightly"))))))

(define-public crate-linked-hash-map-0.0.11 (c (n "linked-hash-map") (v "0.0.11") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0dq7gyi2z7nlrh728a5ka5xvihgxcpx48vmikz4gpwvns083mp7z") (f (quote (("serde_impl" "serde" "serde_json") ("nightly"))))))

(define-public crate-linked-hash-map-0.1.0 (c (n "linked-hash-map") (v "0.1.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0ia69q39whl1nsd0zcbm787pkd8shka4dr7baz2g1c55bi0iavxz") (f (quote (("serde_impl" "serde" "serde_test") ("nightly"))))))

(define-public crate-linked-hash-map-0.2.0 (c (n "linked-hash-map") (v "0.2.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0dhrck2fgifzpim1mi4ssmkrqgiw604a453ja3jcqiq85ararb13") (f (quote (("serde_impl" "serde" "serde_test") ("nightly")))) (y #t)))

(define-public crate-linked-hash-map-0.2.1 (c (n "linked-hash-map") (v "0.2.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.7") (o #t) (d #t) (k 0)))) (h "0j2ybcr4sbfcsmwvl2yy2hd8jswx8xzd3x01lkl7xfdyvbh5i8dx") (f (quote (("serde_impl" "serde" "serde_test") ("nightly"))))))

(define-public crate-linked-hash-map-0.3.0 (c (n "linked-hash-map") (v "0.3.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1kaf95grvfqchxn8pl0854g8ab0fzl56217hndhhhz5qqm2j09kd") (f (quote (("serde_impl" "serde" "serde_test") ("nightly"))))))

(define-public crate-linked-hash-map-0.4.0 (c (n "linked-hash-map") (v "0.4.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1cjz1j6jc4mbgajpi7xzsv89ja3d57jknrw1dvnlnvcjzs7jz6j3") (f (quote (("serde_impl" "serde" "serde_test") ("nightly"))))))

(define-public crate-linked-hash-map-0.4.1 (c (n "linked-hash-map") (v "0.4.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0sw83vsr7c8vsxa1hd2pfbvbqgf7f8g5nqgbgcgxsvii2cdhsrlv") (f (quote (("serde_impl" "serde" "serde_test") ("nightly"))))))

(define-public crate-linked-hash-map-0.4.2 (c (n "linked-hash-map") (v "0.4.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "heapsize") (r "^0.3.9") (o #t) (d #t) (k 0)) (d (n "serde") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^0.9") (o #t) (d #t) (k 0)))) (h "0fd958y02ggwpa2246kmjky9xmnww7vxg0ik3rxgy23hgwlyqq3q") (f (quote (("serde_impl" "serde" "serde_test") ("nightly") ("heapsize_impl" "heapsize"))))))

(define-public crate-linked-hash-map-0.5.0 (c (n "linked-hash-map") (v "0.5.0") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1gxlj8bbc00drfnl7d3kpgys9v6q9pci80cvan3banv1g02anaid") (f (quote (("serde_impl" "serde" "serde_test") ("nightly") ("heapsize_impl" "heapsize"))))))

(define-public crate-linked-hash-map-0.5.1 (c (n "linked-hash-map") (v "0.5.1") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "17pc0g6qcb8w7slj2nvzlplzdp7jyb64wz460mixgkf7bc13kyvh") (f (quote (("serde_impl" "serde" "serde_test") ("nightly") ("heapsize_impl" "heapsize"))))))

(define-public crate-linked-hash-map-0.5.2 (c (n "linked-hash-map") (v "0.5.2") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "10qgbvh00q36ql0jh00rxh2jlq6qvl11n6mig0cvkpf4xf5bd4df") (f (quote (("serde_impl" "serde" "serde_test") ("nightly") ("heapsize_impl" "heapsize"))))))

(define-public crate-linked-hash-map-0.5.3 (c (n "linked-hash-map") (v "0.5.3") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0jih3za0p1mywlnwcakc462q1byk6z8vnrzdm36hg6cxk7asdmcd") (f (quote (("serde_impl" "serde" "serde_test") ("nightly") ("heapsize_impl" "heapsize"))))))

(define-public crate-linked-hash-map-0.5.4 (c (n "linked-hash-map") (v "0.5.4") (d (list (d (n "clippy") (r "0.*") (o #t) (d #t) (k 0)) (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1ww8zsraqnvrsknd315481185igwkx5n14xnhq5i8216z65b7fbz") (f (quote (("serde_impl" "serde" "serde_test") ("nightly") ("heapsize_impl" "heapsize"))))))

(define-public crate-linked-hash-map-0.5.5 (c (n "linked-hash-map") (v "0.5.5") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1190w3a3pp978hdzb4sr781i488jmz7myax1zhz5lkrffmdj08jy") (f (quote (("serde_impl" "serde") ("nightly") ("heapsize_impl" "heapsize"))))))

(define-public crate-linked-hash-map-0.5.6 (c (n "linked-hash-map") (v "0.5.6") (d (list (d (n "heapsize") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "03vpgw7x507g524nx5i1jf5dl8k3kv0fzg8v3ip6qqwbpkqww5q7") (f (quote (("serde_impl" "serde") ("nightly") ("heapsize_impl" "heapsize"))))))

