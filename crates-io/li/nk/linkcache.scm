(define-module (crates-io li nk linkcache) #:use-module (crates-io))

(define-public crate-linkcache-0.1.0 (c (n "linkcache") (v "0.1.0") (d (list (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "filetime") (r "^0.2.23") (d #t) (k 0)) (d (n "image") (r "^0.25") (d #t) (k 0)) (d (n "itertools") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "regex") (r "^1.10") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31.0") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "sublime_fuzzy") (r "^0.7.0") (d #t) (k 0)) (d (n "tantivy") (r "^0.22") (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 2)))) (h "0fx4m8b5jvp36z2c50nwmj3xacc13y3mrm2rfaj98cr7xcsmf3pp")))

