(define-module (crates-io li nk linked-list) #:use-module (crates-io))

(define-public crate-linked-list-0.0.1 (c (n "linked-list") (v "0.0.1") (h "0cfs7mawiljs0mfrwjzh91abb3a1mcq169yhavv37h2av1g78f9i")))

(define-public crate-linked-list-0.0.2 (c (n "linked-list") (v "0.0.2") (h "12izjpadiq9zl03sm9nwphkivxz3cr7a9ilhgn511ndws03wpflk")))

(define-public crate-linked-list-0.0.3 (c (n "linked-list") (v "0.0.3") (d (list (d (n "rand") (r "*") (d #t) (k 2)))) (h "01jzlv3vvz4950jg4d6g9ck0s0syn0p09d9iyzqnkp23j2bcznm4") (f (quote (("nightly"))))))

