(define-module (crates-io li nk linkspace-cli) #:use-module (crates-io))

(define-public crate-linkspace-cli-0.1.3 (c (n "linkspace-cli") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "liblinkspace") (r "^0.1.3") (d #t) (k 0)) (d (n "linkspace-common") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.14") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1i9gcpp7da8vnnfaxy6yj9cdig228yg1283pxirrsxi8kkx1iy01") (y #t)))

(define-public crate-linkspace-cli-0.1.4 (c (n "linkspace-cli") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "linkspace") (r "^0.1.4") (d #t) (k 0)) (d (n "linkspace-common") (r "^0.1.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.14") (f (quote ("env-filter"))) (d #t) (k 0)))) (h "1adh0zyh437kw72jr500jnx4ns2dh1mz11d8clja06hvjmi93aj4") (y #t)))

