(define-module (crates-io li nk link-cplusplus) #:use-module (crates-io))

(define-public crate-link-cplusplus-1.0.0 (c (n "link-cplusplus") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0dhvp9ja1djmvpqzp1zr38lz6r5m6209fhr0aak6qvwnv5jrzysd") (f (quote (("nothing") ("libstdcxx") ("libcxx") ("default"))))))

(define-public crate-link-cplusplus-1.0.1 (c (n "link-cplusplus") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "15bja24hd9cwxn6q7hkvchsbfpkgi7bs6f3lc4qbk769npbxk332") (f (quote (("nothing") ("libstdcxx") ("libcxx") ("default"))))))

(define-public crate-link-cplusplus-1.0.2 (c (n "link-cplusplus") (v "1.0.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1x8pn99p1vcmaxckzgkfb1a7h5xmwbj0c4ij7q786gm69s0v6qzm") (f (quote (("nothing") ("libstdcxx") ("libcxx") ("default"))))))

(define-public crate-link-cplusplus-1.0.3 (c (n "link-cplusplus") (v "1.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "168zrs79j88z01vyi1k1g5bj7nilkrnf2ygnsn2slyfwzyw62b9p") (f (quote (("nothing") ("libstdcxx" "libstdc++") ("libstdc++") ("libcxx" "libc++") ("libc++") ("default"))))))

(define-public crate-link-cplusplus-1.0.4 (c (n "link-cplusplus") (v "1.0.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0m7365ig7r88x7b4gkzj5m7b6wiq42pi1ign7mvyq63jr22sfspr") (f (quote (("nothing") ("libstdcxx" "libstdc++") ("libstdc++") ("libcxx" "libc++") ("libc++") ("default")))) (l "cplusplus")))

(define-public crate-link-cplusplus-1.0.5 (c (n "link-cplusplus") (v "1.0.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1lgjbqfkg0hyq4xmb12aaxxp947g6qbgm2hbc7f5cda7gp9fq6wg") (f (quote (("nothing") ("libstdcxx" "libstdc++") ("libstdc++") ("libcxx" "libc++") ("libc++") ("default")))) (l "cplusplus")))

(define-public crate-link-cplusplus-1.0.6 (c (n "link-cplusplus") (v "1.0.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1j68vchys9338wj07aa5nmhrhyzvbm3j9ixr70wzdwx2gg6y5jpq") (f (quote (("nothing") ("libstdcxx" "libstdc++") ("libstdc++") ("libcxx" "libc++") ("libc++") ("default")))) (l "cplusplus") (r "1.34")))

(define-public crate-link-cplusplus-1.0.7 (c (n "link-cplusplus") (v "1.0.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0sgkbj8x50md3i10rpq9spi9yqyb2z0hcv65bfy6y169jrxsnwlj") (f (quote (("nothing") ("libstdcxx" "libstdc++") ("libstdc++") ("libcxx" "libc++") ("libc++") ("default")))) (l "cplusplus") (r "1.34")))

(define-public crate-link-cplusplus-1.0.8 (c (n "link-cplusplus") (v "1.0.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1x84vvg7gn94x9zrvd67602h3ricmhlv19cpl2alzhqkqz4hglpc") (f (quote (("nothing") ("libstdcxx" "libstdc++") ("libstdc++") ("libcxx" "libc++") ("libc++") ("default")))) (l "cplusplus") (r "1.34")))

(define-public crate-link-cplusplus-1.0.9 (c (n "link-cplusplus") (v "1.0.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1jc56xf51f3fn5hvppsj9c2pa0fxm7k79xs90a5s58qvgrphq94x") (f (quote (("nothing") ("libstdcxx" "libstdc++") ("libstdc++") ("libcxx" "libc++") ("libc++") ("default")))) (l "cplusplus") (r "1.34")))

