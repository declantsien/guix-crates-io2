(define-module (crates-io li nk linkedbytes) #:use-module (crates-io))

(define-public crate-linkedbytes-0.0.0 (c (n "linkedbytes") (v "0.0.0") (h "086248ba1fyjpk5sc7cj30qx2l8b43ln507zkrdbjlqgzdjfanps")))

(define-public crate-linkedbytes-0.1.0 (c (n "linkedbytes") (v "0.1.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "19k11n3kvp764i2hzvhi41g6wdv2y2rs8q9vj4p53v9q8gwjjdc2")))

(define-public crate-linkedbytes-0.1.1 (c (n "linkedbytes") (v "0.1.1") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)))) (h "1djn5zbd1w0khvnqwlidl9c1d12zp6jwxn7fgm6mgnr6lv7qdf9r")))

(define-public crate-linkedbytes-0.1.2 (c (n "linkedbytes") (v "0.1.2") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "05x5g4l9pcxsr6f0gf7m4v646hfw986rphr7zbmav3zm6iap2d7g")))

(define-public crate-linkedbytes-0.1.3 (c (n "linkedbytes") (v "0.1.3") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1afdq3sp2yvp4xi0w9ikx0dz2x48l410xasxwclrlzshb1z8y1yq")))

(define-public crate-linkedbytes-0.1.4 (c (n "linkedbytes") (v "0.1.4") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "smol_str") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "035z0d1pgryizagpklnp6axa9r10xa7w318s9cj303zq9i4h61vf") (f (quote (("smolstr" "smol_str"))))))

(define-public crate-linkedbytes-0.1.5 (c (n "linkedbytes") (v "0.1.5") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "faststr") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "02fv4xs89dimrr8nb0q7bd5whan7cikrw8dl2kfiilvr1c29gdhy")))

(define-public crate-linkedbytes-0.1.6 (c (n "linkedbytes") (v "0.1.6") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "faststr") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "0vqqckzkbs3zap94qgbngas9dgc5nlkfp2x2kb8k2ml7my7vs7jg")))

(define-public crate-linkedbytes-0.1.7 (c (n "linkedbytes") (v "0.1.7") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "038r15fd7xlpyc7d6jblcd41xcrvv372rvf7a59x81l9sgp9d890")))

(define-public crate-linkedbytes-0.1.8 (c (n "linkedbytes") (v "0.1.8") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "faststr") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("io-util"))) (d #t) (k 0)))) (h "1k90z69wqp108y8s6qxs1xsnskd3xp1p3z252dy9hm7mwfr40xj8")))

