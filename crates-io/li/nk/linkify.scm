(define-module (crates-io li nk linkify) #:use-module (crates-io))

(define-public crate-linkify-0.1.0 (c (n "linkify") (v "0.1.0") (d (list (d (n "memchr") (r "^1.0.1") (d #t) (k 0)))) (h "0xy5knlayx43xwlc1rmsjs2rp3w19z82l7qpl2yax7904wkj8chg")))

(define-public crate-linkify-0.1.1 (c (n "linkify") (v "0.1.1") (d (list (d (n "memchr") (r "^1.0.1") (d #t) (k 0)))) (h "1svahwr2a2navq4h604xs7f1x2haiak31kq5357gzxaivlbfwr0c")))

(define-public crate-linkify-0.1.2 (c (n "linkify") (v "0.1.2") (d (list (d (n "memchr") (r "^1.0.1") (d #t) (k 0)))) (h "1v97v07sc9smrk10sya0ajmvrjhs5wlzf55wd72831nj8a5qqwk7")))

(define-public crate-linkify-0.2.0 (c (n "linkify") (v "0.2.0") (d (list (d (n "memchr") (r "^1.0.1") (d #t) (k 0)))) (h "00f5s06rh085n0pg2d0rspfky46jrv9vqjzcxykrra1gzb9608cq")))

(define-public crate-linkify-0.3.0 (c (n "linkify") (v "0.3.0") (d (list (d (n "memchr") (r "^1.0.1") (d #t) (k 0)))) (h "0k5zi4vfpaxn6pm13l3gk1swvbrxddi2s84mpjh4d5xsyjfc5b0c")))

(define-public crate-linkify-0.3.1 (c (n "linkify") (v "0.3.1") (d (list (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "0pyyh503xjj0rz6s2fpi77d92j0307pjn9v133f9442adyf47scw")))

(define-public crate-linkify-0.4.0 (c (n "linkify") (v "0.4.0") (d (list (d (n "memchr") (r "^2.0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)))) (h "15i0q81vrhm4asskacy2z83fyj09ivcff0km82gwbli4vlkib583")))

(define-public crate-linkify-0.5.0 (c (n "linkify") (v "0.5.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "1yqx9m5w1ssvm7zh4n5xfi1xmqy8gl2viahqqkpyxmx65drrvmbq")))

(define-public crate-linkify-0.6.0 (c (n "linkify") (v "0.6.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "0zwscjqh5i1s5mdiz84qrcdhc30k9f6jjsn64gv1vs0k7hf951hr")))

(define-public crate-linkify-0.7.0 (c (n "linkify") (v "0.7.0") (d (list (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "1y99xcmy6793m6x3z2qcyiw3dfrdvza3n659lmlv7kpzq7yjin04")))

(define-public crate-linkify-0.8.0 (c (n "linkify") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "1s253ywn5w3xs1cq36rh32h7c9qbwalrjjbpqgikmahmv5kddg6c")))

(define-public crate-linkify-0.8.1 (c (n "linkify") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "13acb2d3jyhbdwr7h83s3z6w1499wkyfhlkgkk1k3g6hnxz9dn98")))

(define-public crate-linkify-0.9.0 (c (n "linkify") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "memchr") (r "^2.0.1") (d #t) (k 0)) (d (n "plotters-backend") (r "=0.3.2") (d #t) (k 2)))) (h "1xfgz0lh1rbirzi28rnl0378w967jwhkd4v0qqmi2n430225ipcn")))

(define-public crate-linkify-0.10.0 (c (n "linkify") (v "0.10.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "memchr") (r "^2.0.1") (d #t) (k 0)))) (h "107px7dxzzjk645im0g5gciscg8lw2jy4z1sg3nfk0f5a9ns7pzi")))

