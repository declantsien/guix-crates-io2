(define-module (crates-io li nk linker-set) #:use-module (crates-io))

(define-public crate-linker-set-0.1.0 (c (n "linker-set") (v "0.1.0") (d (list (d (n "linker-set-proc") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "14vg5g0rf7fcf2c7rhw18nyzwmzwz5r4jf5s78bq26x9xqpx17a7")))

(define-public crate-linker-set-0.2.0 (c (n "linker-set") (v "0.2.0") (d (list (d (n "linker-set-proc") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^1.0.14") (d #t) (k 0)))) (h "0qg7q5k46jhzpbmh1kazvdbc8alg40321qy5wsq1zw83z285kpxn")))

