(define-module (crates-io li nk linkedlist) #:use-module (crates-io))

(define-public crate-linkedlist-0.1.0 (c (n "linkedlist") (v "0.1.0") (h "1ig4vjj3arbg16sbfg4dyrzij3l9whb561j0c1bdjkjvvxyc74s8")))

(define-public crate-linkedlist-0.1.1 (c (n "linkedlist") (v "0.1.1") (h "0xywwxkggvbwlxbppa9af3a5af9l8rcqr1ms27wsjhcdlcl0j7vj")))

