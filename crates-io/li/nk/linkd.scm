(define-module (crates-io li nk linkd) #:use-module (crates-io))

(define-public crate-linkd-0.1.0 (c (n "linkd") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "09x5dy072d4nq9jsrnvznk0y7gyr2xzx4gh637f31baq9q9yrq8s")))

(define-public crate-linkd-0.1.1 (c (n "linkd") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "1khgs20yfl6bz1hyapqi6n74q7zs98b9y61a9k5iahhnnd57pp0z")))

(define-public crate-linkd-0.1.2 (c (n "linkd") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "05jzb96c4n8921ynagi95w8fzxfxc0rdmv4k6f13h9fmnf1fkg0x")))

(define-public crate-linkd-0.1.3 (c (n "linkd") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "sync" "macros"))) (d #t) (k 0)))) (h "1b5130395i1yl9bzjmcgh30g8bq7kvnrsapbgzkf7vzxmpzkzh8g")))

