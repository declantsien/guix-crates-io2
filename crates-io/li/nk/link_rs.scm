(define-module (crates-io li nk link_rs) #:use-module (crates-io))

(define-public crate-link_rs-0.1.0 (c (n "link_rs") (v "0.1.0") (d (list (d (n "actix-web") (r "^4.0") (d #t) (k 2)) (d (n "harsh") (r "^0.2.2") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "12al4s3awclscdhy1izahdmizf4mwz3b9xdf33yylw8w1bxzqka3") (f (quote (("default" "qrcode")))) (s 2) (e (quote (("qrcode" "dep:qrcode"))))))

(define-public crate-link_rs-0.1.1 (c (n "link_rs") (v "0.1.1") (d (list (d (n "actix-web") (r "^4.0") (d #t) (k 2)) (d (n "harsh") (r "^0.2.2") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "0ylykji2l6mp9h3k9vhnmrr9mkkkdwxrd56vs1f5ir5p349vx2fh") (f (quote (("default" "qrcode")))) (s 2) (e (quote (("qrcode" "dep:qrcode"))))))

(define-public crate-link_rs-0.1.2 (c (n "link_rs") (v "0.1.2") (d (list (d (n "actix-web") (r "^4.0") (d #t) (k 2)) (d (n "harsh") (r "^0.2.2") (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (o #t) (d #t) (k 0)) (d (n "qrcode") (r "^0.12.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.81") (d #t) (k 2)))) (h "19ns2hlwvbvlghlggz99waxm1nxxhn19x3c61h482nai7dqgq87i") (f (quote (("default" "qrcode")))) (s 2) (e (quote (("qrcode" "dep:qrcode"))))))

