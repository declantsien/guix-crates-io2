(define-module (crates-io li nk link-ipps) #:use-module (crates-io))

(define-public crate-link-ipps-0.1.0 (c (n "link-ipps") (v "0.1.0") (d (list (d (n "ipp-sys-build-help") (r "^0.1") (d #t) (k 1)) (d (n "link-ippcore") (r "^0.1") (d #t) (k 0)))) (h "0zrh90cvf7ribb5mry9x8zhx5q8nljggc1zzqrp9ivb0d2iqj561")))

(define-public crate-link-ipps-0.1.1 (c (n "link-ipps") (v "0.1.1") (d (list (d (n "ipp-sys-build-help") (r "^0.1.2") (d #t) (k 1)) (d (n "link-ippcore") (r "^0.1") (d #t) (k 0)))) (h "194fhk3g2p1bfb1fix8jnp0rydszzq4m30bg4czzy5vrdff0r1im") (l "libipps")))

(define-public crate-link-ipps-0.1.2 (c (n "link-ipps") (v "0.1.2") (d (list (d (n "ipp-sys-build-help") (r "^0.1.2") (d #t) (k 1)) (d (n "link-ippcore") (r "^0.1") (d #t) (k 0)) (d (n "link-ippvm") (r "^0.1") (d #t) (k 0)))) (h "1c0q8nd87ggjbh2pvc74p5jhw2dczrds1ywj48pb37qhv4yb19hl") (l "libipps")))

