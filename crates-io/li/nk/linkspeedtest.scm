(define-module (crates-io li nk linkspeedtest) #:use-module (crates-io))

(define-public crate-linkspeedtest-0.1.0 (c (n "linkspeedtest") (v "0.1.0") (h "07kv21ghs0cqlkz2xzbddv6iqiaw3swb12frbw73s63k732in2sd")))

(define-public crate-linkspeedtest-0.1.1 (c (n "linkspeedtest") (v "0.1.1") (h "1id0hxmh2hsyys0wjcdz4w3xjh6d2n1fj6s4yjlilmq2vq9yy742")))

(define-public crate-linkspeedtest-0.1.2 (c (n "linkspeedtest") (v "0.1.2") (h "1lam6n1d27cfmpdkv070mcsggjki791p6d558dflsfi6a5fy32yf")))

(define-public crate-linkspeedtest-0.1.3 (c (n "linkspeedtest") (v "0.1.3") (h "0hz3mxzcjww5gv1lpzy76yf83gniz3mhdbx2n86iy5jxsvw8mmwp")))

(define-public crate-linkspeedtest-0.1.4 (c (n "linkspeedtest") (v "0.1.4") (h "1mhvny8cq1xm0nfhfx4jbn8zpd5m8frsn4w7vzrnaipkgg1fx9hp")))

(define-public crate-linkspeedtest-0.1.5 (c (n "linkspeedtest") (v "0.1.5") (h "15gqjnr3ldnsg8lydiw6qgkmw06f5bbcmpyd59691c18mrshr4wj")))

(define-public crate-linkspeedtest-0.1.6 (c (n "linkspeedtest") (v "0.1.6") (h "1gl1km9jky4krnmif8008y9vgajkzydzkfibz1mb2kchpcpwl2rp")))

(define-public crate-linkspeedtest-0.1.7 (c (n "linkspeedtest") (v "0.1.7") (h "1bnbrzgahkxh53gp15nw7q870pxwrzdmanrp6ria85xv1hwpqrkb")))

(define-public crate-linkspeedtest-0.2.7 (c (n "linkspeedtest") (v "0.2.7") (h "19fiz2ga3g7h5ad5icqpq4bcw1cws7b00qlfi74sh9h2s319wq79")))

(define-public crate-linkspeedtest-0.2.8 (c (n "linkspeedtest") (v "0.2.8") (h "0h69n2yvqdqw3fbig47bn13w937h54ay58ydjybkfdcn29wjpmqx")))

