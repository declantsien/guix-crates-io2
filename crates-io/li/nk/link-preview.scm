(define-module (crates-io li nk link-preview) #:use-module (crates-io))

(define-public crate-link-preview-0.0.1 (c (n "link-preview") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11.4") (o #t) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "13h1w1g2h7hfqikw2k1v47h8i87gr428yz3aaqzh93n18xgh0di8") (f (quote (("fetch" "reqwest"))))))

(define-public crate-link-preview-0.0.2 (c (n "link-preview") (v "0.0.2") (d (list (d (n "reqwest") (r "^0.11.4") (o #t) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "0cllqhnpshkz531qnw9xqn1zhfw1vgpr9x57288cn16zn2wvmpwr") (f (quote (("fetch" "reqwest"))))))

(define-public crate-link-preview-0.0.3 (c (n "link-preview") (v "0.0.3") (d (list (d (n "regex") (r "^1.5.4") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (o #t) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)) (d (n "tokio") (r "^1.9.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "04kg5qb8cl0w065lmv0548lxai4nad1lgf27fhpghv0cg0zjq5dv") (f (quote (("fetch" "reqwest"))))))

(define-public crate-link-preview-0.1.0 (c (n "link-preview") (v "0.1.0") (d (list (d (n "regex") (r "^1.6.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.12") (o #t) (d #t) (k 0)) (d (n "scraper") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2.3.1") (d #t) (k 0)))) (h "00x069kxsd8hmyggwhkm7iby4xjd9h90whmzqlxplq1la73swxga") (f (quote (("fetch" "reqwest"))))))

(define-public crate-link-preview-0.1.1 (c (n "link-preview") (v "0.1.1") (d (list (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (o #t) (d #t) (k 0)) (d (n "scraper") (r "^0.16.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.44") (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "url") (r "^2.4.0") (d #t) (k 0)))) (h "1v79yngv6xk39g6ap2m0i5rx0pcrhs3xxazzq98vagmgjh61fx47") (f (quote (("fetch" "reqwest"))))))

