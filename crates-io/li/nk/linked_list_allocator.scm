(define-module (crates-io li nk linked_list_allocator) #:use-module (crates-io))

(define-public crate-linked_list_allocator-0.1.0 (c (n "linked_list_allocator") (v "0.1.0") (h "1l81a3ay4ghrvqykk4nq7hyv3kbl6izyv1ddiis3n0hjpnqhvmc7")))

(define-public crate-linked_list_allocator-0.1.1 (c (n "linked_list_allocator") (v "0.1.1") (h "0gxxm0l6mry75466620yfkmssfrig3lfvfbpqjykws2279idlz7p")))

(define-public crate-linked_list_allocator-0.1.2 (c (n "linked_list_allocator") (v "0.1.2") (h "1ln6clgn2p30q75gdbfxyy781kj5qj6hjc5gy0ra4yam4s5pdndm")))

(define-public crate-linked_list_allocator-0.1.3 (c (n "linked_list_allocator") (v "0.1.3") (h "0s8gpr5yp4zjqz8kbqp8kqdgzxv2d893m8wjwlx0gv8xm8lg38sf")))

(define-public crate-linked_list_allocator-0.2.0 (c (n "linked_list_allocator") (v "0.2.0") (h "0c70gwqpp305yfmr2wgcd3ryy6xaaykaklplnqx8nk1wqh1cxwpx")))

(define-public crate-linked_list_allocator-0.2.1 (c (n "linked_list_allocator") (v "0.2.1") (h "0gh9d0bba2731a4xya255f5gx1jq036pjc3l2g3yzcwjjbjqiif1")))

(define-public crate-linked_list_allocator-0.2.2 (c (n "linked_list_allocator") (v "0.2.2") (h "0bh541755v9f0sic4bhx17bxv7kv7hxpvwsx2rv4kyryqmw61g2q")))

(define-public crate-linked_list_allocator-0.2.3 (c (n "linked_list_allocator") (v "0.2.3") (h "111kl4v3fcbv347j626w6d1bwj1zdmxdd5d0bjr4zg01b5x3411m")))

(define-public crate-linked_list_allocator-0.2.4 (c (n "linked_list_allocator") (v "0.2.4") (h "003lkqr4kgby5f422ys7p7i2ilqgab8vdvbswm09qc1ig7g31yxi")))

(define-public crate-linked_list_allocator-0.2.5 (c (n "linked_list_allocator") (v "0.2.5") (h "1vh41ac0i93cj7z3w7800yn22b6y4b2vq3yy0m718z8p4nvrj4xd")))

(define-public crate-linked_list_allocator-0.2.6 (c (n "linked_list_allocator") (v "0.2.6") (h "1k7ij982684bixyamlwck2714s6yf0ln16p16572dw8plhapxnv3")))

(define-public crate-linked_list_allocator-0.2.7 (c (n "linked_list_allocator") (v "0.2.7") (h "0g4mqr52pjqf5xci489r696p46fpv0k6w8f4gq1f7aw1w45b09a0")))

(define-public crate-linked_list_allocator-0.3.0 (c (n "linked_list_allocator") (v "0.3.0") (h "1ydnxi9gj3023f67v4zvjqh0ypacblwn3421lkfyr08dls9sjv2p")))

(define-public crate-linked_list_allocator-0.4.0 (c (n "linked_list_allocator") (v "0.4.0") (d (list (d (n "spin") (r "^0.4.5") (d #t) (k 0)))) (h "14cdv7w044s3zfdx0m2i283h804v19h8gginjxf6sp4jw0cf965l") (y #t)))

(define-public crate-linked_list_allocator-0.4.1 (c (n "linked_list_allocator") (v "0.4.1") (d (list (d (n "spin") (r "^0.4.5") (d #t) (k 0)))) (h "0f74jj52zgijcp8nhz6gwsdn0ls6g99j1blkrjmadv594my5g7h1")))

(define-public crate-linked_list_allocator-0.4.2 (c (n "linked_list_allocator") (v "0.4.2") (d (list (d (n "spin") (r "^0.4.5") (d #t) (k 0)))) (h "1zca131fb8pz0zp95l1vxpmr8cqs337z66d589rcpnlz51l8m6qx")))

(define-public crate-linked_list_allocator-0.4.3 (c (n "linked_list_allocator") (v "0.4.3") (d (list (d (n "spin") (r "^0.4.5") (d #t) (k 0)))) (h "0frf7wbysk2ffk7gdiqry19jyzlw0adlx6cd1inssdh6da1s1xsm")))

(define-public crate-linked_list_allocator-0.5.0 (c (n "linked_list_allocator") (v "0.5.0") (d (list (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0hl5vch6kxbvlii9hki30nzmhl78771n6dpms34i3rnfcwqhlhm6") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-linked_list_allocator-0.6.0 (c (n "linked_list_allocator") (v "0.6.0") (d (list (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "1lz0xh46m0h08rvb7iawkh11i2srkzzk0fbigkpcnk466fz16llj") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-linked_list_allocator-0.6.1 (c (n "linked_list_allocator") (v "0.6.1") (d (list (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "0n1j98540svbsi80ddx80dnpkaa7mlsc114nnm8wy0d3vzmfxqqy") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-linked_list_allocator-0.6.2 (c (n "linked_list_allocator") (v "0.6.2") (d (list (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "14rv9s1ikjsnay4z23afy6lj96gbqzrm8pzn54i1fcc48ci33zy8") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-linked_list_allocator-0.6.3 (c (n "linked_list_allocator") (v "0.6.3") (d (list (d (n "spin") (r "^0.4.5") (o #t) (d #t) (k 0)))) (h "13b7q5kp503gdag6017nwgd36h71lmm1n8vjrs8hizi7333mfpb5") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-linked_list_allocator-0.6.4 (c (n "linked_list_allocator") (v "0.6.4") (d (list (d (n "spin") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "1shb7rs2d6b8nqzrrmb7cp2ma44vprbvx9dmgkp6ka4ssb0lwca7") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-linked_list_allocator-0.6.5 (c (n "linked_list_allocator") (v "0.6.5") (d (list (d (n "spin") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "097v8343513yhv4gvbnhy99qd58yp5lrwm5fwbyq02z2iinmww7k") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-linked_list_allocator-0.6.6 (c (n "linked_list_allocator") (v "0.6.6") (d (list (d (n "spin") (r "^0.5.0") (o #t) (d #t) (k 0)))) (h "0qn382rwwhfplvl8lhnl75xkvayynpjj9qg9jzhhw9fhz91impj7") (f (quote (("use_spin" "spin") ("default" "use_spin"))))))

(define-public crate-linked_list_allocator-0.7.0 (c (n "linked_list_allocator") (v "0.7.0") (d (list (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "0shkf015b9wq5rnmmjg323kp0xz67y81arrgyymr06847xcr26a1") (f (quote (("use_spin" "spinning_top") ("default" "use_spin"))))))

(define-public crate-linked_list_allocator-0.8.0 (c (n "linked_list_allocator") (v "0.8.0") (d (list (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "0k420082kvrdk25s381bv1xw3yzgmkv54a72zfd43r6z6721iihq") (f (quote (("use_spin" "spinning_top") ("default" "use_spin") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.1 (c (n "linked_list_allocator") (v "0.8.1") (d (list (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "0vv2fbgn1lznfjw3sy2x6q83lvzmp9w9p35yrpyx0nf64flaw9aq") (f (quote (("use_spin" "spinning_top") ("default" "use_spin") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.2 (c (n "linked_list_allocator") (v "0.8.2") (d (list (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "1r0pahk1wxkzijfrfbbw8h5ixk4l1x2adb4774xgvjzc9fjhw1y1") (f (quote (("use_spin" "spinning_top") ("default" "use_spin") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.3 (c (n "linked_list_allocator") (v "0.8.3") (d (list (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "17pi4gcykyrq4pds4782rbh09kpn8kap1ya3pd80k1acvl0hbdnn") (f (quote (("use_spin" "spinning_top") ("default" "use_spin") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.4 (c (n "linked_list_allocator") (v "0.8.4") (d (list (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "0gfpxw07ffjim92qzphijxyhi8bw6kim4mzcdk1790qf7k0lc3p7") (f (quote (("use_spin" "spinning_top") ("default" "use_spin") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.5 (c (n "linked_list_allocator") (v "0.8.5") (d (list (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "140zkpws4sxdgxbklwis6lpspx9wjkhjy18rzgp00zba2pk2c2v6") (f (quote (("use_spin" "spinning_top") ("default" "use_spin") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.6 (c (n "linked_list_allocator") (v "0.8.6") (d (list (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "167l1x54r6kxklnrfv8lclg70wdjnf16i2504wd3si83wiw5cml4") (f (quote (("use_spin" "spinning_top") ("default" "use_spin") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.7 (c (n "linked_list_allocator") (v "0.8.7") (d (list (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "1fjb5yx3z727j6ws1r65bd7hbg6vxqqa7zj40bj9kqnzgm5kqk1s") (f (quote (("use_spin" "spinning_top") ("default" "use_spin" "const_mut_refs") ("const_mut_refs") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.8 (c (n "linked_list_allocator") (v "0.8.8") (d (list (d (n "spinning_top") (r "^0.1.0") (f (quote ("nightly"))) (o #t) (d #t) (k 0)))) (h "16d3f12nl9rc65d02nzq75w8y02619yg473z3h6jx4xzyggncrsf") (f (quote (("use_spin" "spinning_top") ("default" "use_spin" "const_mut_refs") ("const_mut_refs") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.9 (c (n "linked_list_allocator") (v "0.8.9") (d (list (d (n "spinning_top") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "03ffv9jzddmj4d5abizbvv48qf62rnhdimgmfwpqq5l4y5lvfv2k") (f (quote (("use_spin" "spinning_top") ("default" "use_spin" "const_mut_refs") ("const_mut_refs" "spinning_top/nightly") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.10 (c (n "linked_list_allocator") (v "0.8.10") (d (list (d (n "spinning_top") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1gpbwlwpbpn09aglcsw87ss9vr80bmjwk68zjkx2i4bp0r45yqfb") (f (quote (("use_spin" "spinning_top") ("default" "use_spin" "const_mut_refs") ("const_mut_refs" "spinning_top/nightly") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.8.11 (c (n "linked_list_allocator") (v "0.8.11") (d (list (d (n "spinning_top") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "094wc067g789zxf3kakm3wbqby5y2zd109ajg65njq0qvfgdsal2") (f (quote (("use_spin_nightly" "use_spin" "spinning_top/nightly" "const_mut_refs") ("use_spin" "spinning_top") ("default" "use_spin_nightly") ("const_mut_refs") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.9.0 (c (n "linked_list_allocator") (v "0.9.0") (d (list (d (n "spinning_top") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0s7asysb1f926283rgc04bgl79gqg46b5l32d44idakhflh2bdyh") (f (quote (("use_spin_nightly" "use_spin" "spinning_top/nightly" "const_mut_refs") ("use_spin" "spinning_top") ("default" "use_spin_nightly") ("const_mut_refs") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.9.1 (c (n "linked_list_allocator") (v "0.9.1") (d (list (d (n "spinning_top") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0jlg2svr3fikzm8c1jpl111wz6y5fk6ssh237jar3cj61rsf372l") (f (quote (("use_spin_nightly" "use_spin" "spinning_top/nightly" "const_mut_refs") ("use_spin" "spinning_top") ("default" "use_spin_nightly") ("const_mut_refs") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.10.0 (c (n "linked_list_allocator") (v "0.10.0") (d (list (d (n "spinning_top") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1afjrdbczyy614ddnfkan9c3mwwhvh25ykbs5k4f00xk4fzh0b92") (f (quote (("use_spin_nightly" "use_spin" "spinning_top/nightly" "const_mut_refs") ("use_spin" "spinning_top") ("default" "use_spin_nightly") ("const_mut_refs") ("alloc_ref")))) (y #t)))

(define-public crate-linked_list_allocator-0.10.1 (c (n "linked_list_allocator") (v "0.10.1") (d (list (d (n "spinning_top") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "0jnrh6h3zw472c2bnhr2yrh2m6jmbn6wh29i60j2fqyv574knv33") (f (quote (("use_spin_nightly" "use_spin" "spinning_top/nightly" "const_mut_refs") ("use_spin" "spinning_top") ("default" "use_spin_nightly") ("const_mut_refs") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.10.2 (c (n "linked_list_allocator") (v "0.10.2") (d (list (d (n "spinning_top") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "04lc5fwikslgkg41fia93mppkrzkmv93p016zfiy866i9kb0i2g9") (f (quote (("use_spin_nightly" "use_spin" "spinning_top/nightly" "const_mut_refs") ("use_spin" "spinning_top") ("default" "use_spin_nightly") ("const_mut_refs") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.10.3 (c (n "linked_list_allocator") (v "0.10.3") (d (list (d (n "spinning_top") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "1hl32sqly15m73qa3ga7zz3glix1sxdgx5839q7f9b1s53ka139f") (f (quote (("use_spin_nightly" "use_spin" "spinning_top/nightly" "const_mut_refs") ("use_spin" "spinning_top") ("default" "use_spin_nightly") ("const_mut_refs") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.10.4 (c (n "linked_list_allocator") (v "0.10.4") (d (list (d (n "spinning_top") (r "^0.2.3") (o #t) (d #t) (k 0)))) (h "03h3jmdliasdd3gga1djz2yscs2rdbfb4lxhl51sxyr5s9cz48p3") (f (quote (("use_spin_nightly" "use_spin" "spinning_top/nightly" "const_mut_refs") ("use_spin" "spinning_top") ("default" "use_spin_nightly") ("const_mut_refs") ("alloc_ref"))))))

(define-public crate-linked_list_allocator-0.10.5 (c (n "linked_list_allocator") (v "0.10.5") (d (list (d (n "spinning_top") (r "^0.2.5") (o #t) (d #t) (k 0)))) (h "11k2dv6v5kq45kbvahll434f9iwfw0vsyaycp76q3vh5ahzldyls") (f (quote (("use_spin_nightly" "use_spin") ("use_spin" "spinning_top") ("default" "use_spin") ("const_mut_refs") ("alloc_ref")))) (r "1.61")))

