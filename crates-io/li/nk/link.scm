(define-module (crates-io li nk link) #:use-module (crates-io))

(define-public crate-link-0.1.0 (c (n "link") (v "0.1.0") (d (list (d (n "field-offset") (r "^0.1") (d #t) (k 0)))) (h "1pgcpbbh41x8v2n8717ql1w780zzblc4r7yljypgs95610wsjczs") (y #t)))

(define-public crate-link-0.1.1 (c (n "link") (v "0.1.1") (h "14nxh7vc8vhgjjwf9n8cl5wxmb7mlkb3z1zj5hlc34vh01s5j6lr")))

