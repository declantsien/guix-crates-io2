(define-module (crates-io li nk linked-data-derive) #:use-module (crates-io))

(define-public crate-linked-data-derive-0.1.0 (c (n "linked-data-derive") (v "0.1.0") (d (list (d (n "iref") (r "^3.0") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "static-iref") (r "^3.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.47") (d #t) (k 0)))) (h "1vpvc9bh71d8xbc3f0xf34w82imyw66jss2rzzfc4isvcq1ijyl8") (r "1.71.1")))

