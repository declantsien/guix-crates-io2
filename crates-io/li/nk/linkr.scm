(define-module (crates-io li nk linkr) #:use-module (crates-io))

(define-public crate-linkr-0.1.0 (c (n "linkr") (v "0.1.0") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.0") (f (quote ("diesel_postgres_pool"))) (k 0)) (d (n "url") (r "^1.7.2") (d #t) (k 0)))) (h "0ddz4jhv7gznyyddgbhi244sn8y3pvd1f238m3khv11h4dqwd9sv") (y #t)))

(define-public crate-linkr-0.1.1 (c (n "linkr") (v "0.1.1") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.0") (f (quote ("diesel_postgres_pool"))) (k 0)))) (h "1m16w4r3qw76qffmlnanmj6829g9cb01m2c4v6q9qxphw7hc21v3") (y #t)))

(define-public crate-linkr-0.1.2 (c (n "linkr") (v "0.1.2") (d (list (d (n "diesel") (r "^1.0.0") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.0") (f (quote ("diesel_postgres_pool"))) (k 0)))) (h "1919lbc50aknvf846f5cr9n7an0jwwi7lrw0piaq5hl6vr6fazw8") (y #t)))

(define-public crate-linkr-0.2.0 (c (n "linkr") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "diesel") (r "^1.0.0") (f (quote ("postgres" "chrono"))) (d #t) (k 0)) (d (n "diesel_migrations") (r "^1.4.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.13.0") (d #t) (k 0)) (d (n "hmac") (r "^0.7.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "rocket") (r "^0.4.0") (d #t) (k 0)) (d (n "rocket_contrib") (r "^0.4.0") (f (quote ("diesel_postgres_pool"))) (d #t) (k 0)) (d (n "sha2") (r "^0.8.0") (d #t) (k 0)))) (h "0ymshqd9lbgz2jl27k9pg8gx1ybpnlcfl438b0g5azp28h05l6gf") (y #t)))

