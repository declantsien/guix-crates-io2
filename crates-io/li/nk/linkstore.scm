(define-module (crates-io li nk linkstore) #:use-module (crates-io))

(define-public crate-linkstore-0.1.0 (c (n "linkstore") (v "0.1.0") (d (list (d (n "goblin") (r "^0") (d #t) (k 0)) (d (n "libloading") (r "^0") (d #t) (k 2)) (d (n "ouroboros") (r "^0") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "18apjgf3pid2mi5s45nw10rbkgwawcdhyacy814k3fcbz6dak7a6")))

(define-public crate-linkstore-1.0.1 (c (n "linkstore") (v "1.0.1") (d (list (d (n "goblin") (r "^0.6") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 2)) (d (n "ouroboros") (r "^0.15") (d #t) (k 0)) (d (n "sealed") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1fn83w91v49cwnfznf5xi0s803kayfnjniijqhvp93b2asa85ghi") (f (quote (("store") ("embedder") ("docs") ("default" "embedder" "store"))))))

(define-public crate-linkstore-1.0.2 (c (n "linkstore") (v "1.0.2") (d (list (d (n "goblin") (r "^0.6") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 2)) (d (n "ouroboros") (r "^0.15") (d #t) (k 0)) (d (n "sealed") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "19z1c9dc80cpgmbki4xka4g1pvivi8y53pqaqgjjz1yjiqy8xszg") (f (quote (("store") ("embedder") ("default" "embedder" "store"))))))

