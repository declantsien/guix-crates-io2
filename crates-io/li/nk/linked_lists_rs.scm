(define-module (crates-io li nk linked_lists_rs) #:use-module (crates-io))

(define-public crate-linked_lists_rs-0.1.0 (c (n "linked_lists_rs") (v "0.1.0") (h "1n69hqgp07ry46j6y4wdfch9jpvdxvs83v39h18lsp56df7p7xls")))

(define-public crate-linked_lists_rs-0.1.1 (c (n "linked_lists_rs") (v "0.1.1") (h "06bdbb3f4mryx3gwyf4ws6ricc74vr8wk251r547mifdmqvrr7d0")))

(define-public crate-linked_lists_rs-0.1.2 (c (n "linked_lists_rs") (v "0.1.2") (h "04llgm8216rx1928pklx2pj1bqixba1qncq59ym9ivb5hmx72a59")))

