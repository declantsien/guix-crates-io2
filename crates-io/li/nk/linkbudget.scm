(define-module (crates-io li nk linkbudget) #:use-module (crates-io))

(define-public crate-linkbudget-0.0.6 (c (n "linkbudget") (v "0.0.6") (h "1d1n7sl8hmjbcyngamw0ppna1mvrnzwiimp6f996y0rlsiafgnyj")))

(define-public crate-linkbudget-0.0.7 (c (n "linkbudget") (v "0.0.7") (h "0i0xshzdv9bwnnsqhxiipc2ssns1kl2m2frcqkk0kiz8sbwsl2kh")))

(define-public crate-linkbudget-0.0.8 (c (n "linkbudget") (v "0.0.8") (h "09didzs8cjp878x3w7qjq666ln6imsdhhg26x23cc966b7l1vkjj")))

(define-public crate-linkbudget-0.0.9 (c (n "linkbudget") (v "0.0.9") (h "1m0l37cv6jkrkplxkzmii32wpx4idrgpahnf24m3lgck4symz3yb")))

(define-public crate-linkbudget-0.0.11 (c (n "linkbudget") (v "0.0.11") (h "1f2jzdm8iywa45wwqx5a0h65k3qr7mk733axs9jmd9vv9whc2r94")))

