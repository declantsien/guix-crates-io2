(define-module (crates-io li nk linked_lists) #:use-module (crates-io))

(define-public crate-linked_lists-0.1.0 (c (n "linked_lists") (v "0.1.0") (h "0nd6pm2hfdidijhxaww7diif42iazkgs5fz8cj99kr1vl318640m") (f (quote (("stack"))))))

(define-public crate-linked_lists-0.1.1 (c (n "linked_lists") (v "0.1.1") (h "1pxrpdzq2gy7s08ai5cjzf8bvi4vpp21dzc6gcc7mb7hn1dgvkfh") (f (quote (("stack"))))))

(define-public crate-linked_lists-0.1.2 (c (n "linked_lists") (v "0.1.2") (h "0p9k6wq4n7zh7zj8cf4zcq55pjis988i87811lyw087pvi9akzdb") (f (quote (("stack"))))))

(define-public crate-linked_lists-0.1.3 (c (n "linked_lists") (v "0.1.3") (h "0rsycljna80370km27g963d6jai7h3gwy64v181fg08p28yc1x2x") (f (quote (("stack"))))))

(define-public crate-linked_lists-0.1.4 (c (n "linked_lists") (v "0.1.4") (h "04ycihc1xiyly6s68z07jy279rpjs7n36j70xmdp7r8c5iqg0ig3") (f (quote (("stack"))))))

(define-public crate-linked_lists-0.1.5 (c (n "linked_lists") (v "0.1.5") (h "032cvdqcrbhfall1zl7zfia91fz8vgbn6ip2rh4l7fhv1073c7r9") (f (quote (("stack") ("persistent_arc") ("persistent"))))))

(define-public crate-linked_lists-0.1.6 (c (n "linked_lists") (v "0.1.6") (h "19f4xiq1f4qyp5j4y8xym0lpxizb17adr41rc5lbmn8dds8nlp2r") (f (quote (("stack") ("persistent_arc") ("persistent"))))))

(define-public crate-linked_lists-0.1.7 (c (n "linked_lists") (v "0.1.7") (h "0j55ml0dwl6845swd8md8igsr9jdqy2bxssc73wxzs7kc0mbx58s") (f (quote (("stack") ("persistent_arc") ("persistent"))))))

