(define-module (crates-io li nk linkme) #:use-module (crates-io))

(define-public crate-linkme-0.1.0 (c (n "linkme") (v "0.1.0") (d (list (d (n "linkme-impl") (r "^0.1") (d #t) (k 0)))) (h "0kdimpnqfilgcnmy4pvbdnsmqms6a5bmcm91bixq263fphhbxgyn")))

(define-public crate-linkme-0.1.1 (c (n "linkme") (v "0.1.1") (d (list (d (n "linkme-impl") (r "= 0.1.1") (d #t) (k 0)))) (h "16jm4vxww9ahpllmspj6mxi2rxrq3kg2hhfxxd5kbjxpis3iqg3b")))

(define-public crate-linkme-0.1.2 (c (n "linkme") (v "0.1.2") (d (list (d (n "linkme-impl") (r "= 0.1.2") (d #t) (k 0)))) (h "0n7l5fiaijxwag85322vgw5j4a2a2dxb99xldrq4dw9hi801wax1")))

(define-public crate-linkme-0.1.3 (c (n "linkme") (v "0.1.3") (d (list (d (n "linkme-impl") (r "= 0.1.3") (d #t) (k 0)))) (h "0amlmb1paf3np09zkjgcl2n02ki7ynq0iz01x23w43haznps411x")))

(define-public crate-linkme-0.1.4 (c (n "linkme") (v "0.1.4") (d (list (d (n "linkme-impl") (r "= 0.1.4") (d #t) (k 0)))) (h "02nqfaz2r42hs7s958pp223vwa0mxmlfalwis9c5c3p02fs83pvg")))

(define-public crate-linkme-0.1.5 (c (n "linkme") (v "0.1.5") (d (list (d (n "linkme-impl") (r "= 0.1.5") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0hk60wl8xj7nq9f5402pfrjw8hkh217hxzhd8b0jbr0m54m9i1nk")))

(define-public crate-linkme-0.1.6 (c (n "linkme") (v "0.1.6") (d (list (d (n "linkme-impl") (r "= 0.1.6") (d #t) (k 0)) (d (n "rustversion") (r "^0.1") (d #t) (k 2)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0dmc8xlcvjrncj0hnk32rh7sxpxcgjinc1w70mwm0kf1z77f39hk")))

(define-public crate-linkme-0.2.0 (c (n "linkme") (v "0.2.0") (d (list (d (n "linkme-impl") (r "= 0.2.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0a75sviffiix5v8cjvaf9wqg902hjvk9245mf9fdnqf77q0mcs4l")))

(define-public crate-linkme-0.2.1 (c (n "linkme") (v "0.2.1") (d (list (d (n "linkme-impl") (r "= 0.2.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "132ab9jkprg2s1qv3an0x4qlrypfi24nxcrz333ffrwl0c3944kr")))

(define-public crate-linkme-0.2.2 (c (n "linkme") (v "0.2.2") (d (list (d (n "linkme-impl") (r "=0.2.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0x69s8dx8wsyp0k42h5sak8lgbapzr93prhsqhpqz4385b30ajfg")))

(define-public crate-linkme-0.2.3 (c (n "linkme") (v "0.2.3") (d (list (d (n "linkme-impl") (r "=0.2.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1hwppwk55l74h8g0zyczryz4a7n3j9l9q9phy3n9a20kgcjqysrz")))

(define-public crate-linkme-0.2.4 (c (n "linkme") (v "0.2.4") (d (list (d (n "linkme-impl") (r "=0.2.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0mf5w0c5j7s8pscdw1sfwiarjr6avcwkdsgc1k757ra7xb1li3dg")))

(define-public crate-linkme-0.2.5 (c (n "linkme") (v "0.2.5") (d (list (d (n "linkme-impl") (r "=0.2.5") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0cqq9yzkicpnjx57x12wkmghg2j4qbjblm0rzv1yprrhgpq1279l")))

(define-public crate-linkme-0.2.6 (c (n "linkme") (v "0.2.6") (d (list (d (n "linkme-impl") (r "=0.2.6") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1yzp5chggf4qn1x06y6zq9sdf1r9x5pmhcy32s6nhk7vvr4abxq4")))

(define-public crate-linkme-0.2.7 (c (n "linkme") (v "0.2.7") (d (list (d (n "linkme-impl") (r "=0.2.7") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1x49h7hvql838j50mg2af7hv4ziqyafni73v3sa4a78p0nbl1vg6")))

(define-public crate-linkme-0.2.8 (c (n "linkme") (v "0.2.8") (d (list (d (n "linkme-impl") (r "=0.2.8") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0z225px5szv9ymmxicfzxrhprgzv2yj5338whlhqph44ilzm6mxa") (r "1.31")))

(define-public crate-linkme-0.2.9 (c (n "linkme") (v "0.2.9") (d (list (d (n "linkme-impl") (r "=0.2.9") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0vc343mafr3jn2vgsh5jcrkp9rqj9qj6c4pc5wrqi47xcvwhvw9x") (r "1.31")))

(define-public crate-linkme-0.2.10 (c (n "linkme") (v "0.2.10") (d (list (d (n "linkme-impl") (r "=0.2.10") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0b6095nivdypdjbrsyk5llq6zjx7g1rd2vyrr8hxqd4rdcasvm7d") (r "1.31")))

(define-public crate-linkme-0.3.0 (c (n "linkme") (v "0.3.0") (d (list (d (n "linkme-impl") (r "=0.3.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ayisglq052x2q0am6d2g14bsx87kdrga3wh3sk2l5g1jqb2ilw4") (r "1.62")))

(define-public crate-linkme-0.3.1 (c (n "linkme") (v "0.3.1") (d (list (d (n "linkme-impl") (r "=0.3.1") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "01i4p255yl5a4dkq3innw36w4nhjssg7lnbvdzn79vm2k27v1qf0") (r "1.62")))

(define-public crate-linkme-0.3.2 (c (n "linkme") (v "0.3.2") (d (list (d (n "linkme-impl") (r "=0.3.2") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0z102f45xcl0mhn1gsyz1higr0yacacaq1cvignzba29sjl5fz9m") (r "1.62")))

(define-public crate-linkme-0.3.3 (c (n "linkme") (v "0.3.3") (d (list (d (n "linkme-impl") (r "=0.3.3") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0bkkrzflbi3spqxyrj3bixpfpln5gn1zk5784qg9cx0s8ibcbz7i") (r "1.62")))

(define-public crate-linkme-0.3.4 (c (n "linkme") (v "0.3.4") (d (list (d (n "linkme-impl") (r "=0.3.4") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0zfc8jiz3diirmmvkkjdmix5njkj8xxglsqmq5p008pam7l499l7") (r "1.62")))

(define-public crate-linkme-0.3.5 (c (n "linkme") (v "0.3.5") (d (list (d (n "linkme-impl") (r "=0.3.5") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0vkpcpdpq95sahfg2r5l9y6nr71i2g0yzg4q2fgc1jmaxrvn5dfa") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.6 (c (n "linkme") (v "0.3.6") (d (list (d (n "linkme-impl") (r "=0.3.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0jwnvf2pbw8pxzbv3mh731fpwlw1dhfp06ghcz1f451m50rzfpn2") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.7 (c (n "linkme") (v "0.3.7") (d (list (d (n "linkme-impl") (r "=0.3.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1y3s0brp263q6la3i7b9iyymgmhr2cavv1cdy5y5brw2y5pb1g12") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.8 (c (n "linkme") (v "0.3.8") (d (list (d (n "linkme-impl") (r "=0.3.8") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0q2jlkbl61js19iyzl5b1vpib8s9p7r439ssy6lcq6yscw4v7hng") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.9 (c (n "linkme") (v "0.3.9") (d (list (d (n "linkme-impl") (r "=0.3.9") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "04zhm562zzfa6xlv5nmfy32w4gjhjcf7jg4aqwr88aidibfkx2mg") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.10 (c (n "linkme") (v "0.3.10") (d (list (d (n "linkme-impl") (r "=0.3.10") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1n1lsshzka34jb46nmx425km6jmlpdnhk88h11gppfvfzhp31wwp") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.11 (c (n "linkme") (v "0.3.11") (d (list (d (n "linkme-impl") (r "=0.3.11") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "05p2v8c9vc51y8pa01padyygm6wv28amg7mwxzzc3g8wqafvsw5g") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.12 (c (n "linkme") (v "0.3.12") (d (list (d (n "linkme-impl") (r "=0.3.12") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1zzc6g1kv1qin1na7mrm1w0dsixx78lqjy8g6xrw1q242cysfl5a") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.13 (c (n "linkme") (v "0.3.13") (d (list (d (n "linkme-impl") (r "=0.3.13") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0yclmcs2sa9ll0sv0sbklcnjskk7hdqc35zrs326mz7vphx2g5qx") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.14 (c (n "linkme") (v "0.3.14") (d (list (d (n "linkme-impl") (r "=0.3.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1j48cfzxlpkfxrsmn4jpd3zxln4si24w6086fpynyibard6vpix6") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.15 (c (n "linkme") (v "0.3.15") (d (list (d (n "linkme-impl") (r "=0.3.15") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0fy3if678xlnd692aqq6igqfw9ipcj07lyms2ianpd2vmmk8754z") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.16 (c (n "linkme") (v "0.3.16") (d (list (d (n "linkme-impl") (r "=0.3.16") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "038121h2m26f5njd0c1z1qa2yz15sli5n5918cn088r4kmzp1maa") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.17 (c (n "linkme") (v "0.3.17") (d (list (d (n "linkme-impl") (r "=0.3.17") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0276wri3rpc3qlwhdnda5h3jz054zx687bg9z1x71yag8vljxvci") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.18 (c (n "linkme") (v "0.3.18") (d (list (d (n "linkme-impl") (r "=0.3.18") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1adz5x9fid4sxy0q7vi4p01zj89qd2wvwh42x1d3r3d8kjxv1rmi") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.19 (c (n "linkme") (v "0.3.19") (d (list (d (n "linkme-impl") (r "=0.3.19") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0y1m9b8vb00ggr5z3yabqnvqrrghdd4nqwpav5g0xw0ay2jjzkbk") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.20 (c (n "linkme") (v "0.3.20") (d (list (d (n "linkme-impl") (r "=0.3.20") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "09q4b0qkwb7g26zjfhkyc10kgxypvsg23qgf7h56wl8xisp8mbnk") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.21 (c (n "linkme") (v "0.3.21") (d (list (d (n "linkme-impl") (r "=0.3.21") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "08xy2a1ivi9aqxrn9pfy6m99hgr0pqxs2dgz403vf5wpgl5m2yj3") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.22 (c (n "linkme") (v "0.3.22") (d (list (d (n "linkme-impl") (r "=0.3.22") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0c5l45b2mirxsig8d5p2z48a0wfmsmddnklm0m3qcn6y6dmaslwb") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.23 (c (n "linkme") (v "0.3.23") (d (list (d (n "linkme-impl") (r "=0.3.23") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1lz3wballvaiay37l9h7ghv39njx773rqblxznkhln4pq1m82y0s") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.24 (c (n "linkme") (v "0.3.24") (d (list (d (n "linkme-impl") (r "=0.3.24") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1p9kz6aql2kxcvrapxfl42kz36888jx9k5kn03xn9qyslrvn78bw") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.25 (c (n "linkme") (v "0.3.25") (d (list (d (n "linkme-impl") (r "=0.3.25") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1fax9g6cdk6aq92bhp21zzjinvclaphib85rkxc6kn4vvvhgwb5v") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

(define-public crate-linkme-0.3.26 (c (n "linkme") (v "0.3.26") (d (list (d (n "linkme-impl") (r "=0.3.26") (d #t) (k 0)) (d (n "once_cell") (r "^1.16") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "0gv2f6slhddajjz9sz63xxghsgv76cdcjw4piyn6ha77pypj4cl3") (f (quote (("used_linker" "linkme-impl/used_linker")))) (r "1.62")))

