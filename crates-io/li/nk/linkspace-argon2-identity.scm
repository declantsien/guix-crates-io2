(define-module (crates-io li nk linkspace-argon2-identity) #:use-module (crates-io))

(define-public crate-linkspace-argon2-identity-0.1.0 (c (n "linkspace-argon2-identity") (v "0.1.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "linkspace-cryptography") (r "^0.1.0") (d #t) (k 0)) (d (n "rust-argon2") (r "^1.0.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "1qk1jvl4rh0q5bny5p5l3q6cw275ay8jzav5cwf7y01wc68hk0iz") (y #t)))

