(define-module (crates-io li nk linkk) #:use-module (crates-io))

(define-public crate-linkk-0.1.0 (c (n "linkk") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0chs2iingmi11iyp9jlvy17hd31m3dr7wpq56lmims9zyqx2kgiy") (f (quote (("default")))) (y #t)))

(define-public crate-linkk-0.1.1 (c (n "linkk") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0myb2b5xyqfqz3ni7xwh3b3ngmwbj41i2i1fj6npyxdlkbrqgcr1") (f (quote (("default")))) (y #t)))

(define-public crate-linkk-0.1.2 (c (n "linkk") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1k23wwzlnp03zskb3z0d4h7vyh6fhw8zl01cc0mzqb95cn8yc2dp") (f (quote (("default"))))))

