(define-module (crates-io li nk link_args) #:use-module (crates-io))

(define-public crate-link_args-0.4.0 (c (n "link_args") (v "0.4.0") (h "1xwsv9almxdlfdqkx1jkjksr10blqan2xcjrqi3yrxr9sr1c5d5s")))

(define-public crate-link_args-0.4.1 (c (n "link_args") (v "0.4.1") (h "1xh2b359b6fi8adsdvf6v51hdxi339w2pa95q4jhf6gm5bm3icys")))

(define-public crate-link_args-0.5.0 (c (n "link_args") (v "0.5.0") (h "0wb9a8dxz70alj1z0xm17cml5ixw70xx9cbyri4lqi43qw1a9qqj")))

(define-public crate-link_args-0.5.1 (c (n "link_args") (v "0.5.1") (h "0dzix0hshl23jrgrmw8a3p169z1q9ky347h2x4xfzvw0sa15w1vf")))

(define-public crate-link_args-0.6.0 (c (n "link_args") (v "0.6.0") (h "16z8sm0bdhdfmdnznbkg75x5q166r6vvcpkssam9lk32fbj22xrc")))

