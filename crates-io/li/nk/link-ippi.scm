(define-module (crates-io li nk link-ippi) #:use-module (crates-io))

(define-public crate-link-ippi-0.1.0 (c (n "link-ippi") (v "0.1.0") (d (list (d (n "ipp-sys-build-help") (r "^0.1") (d #t) (k 1)) (d (n "link-ippcore") (r "^0.1") (d #t) (k 0)))) (h "0j129r47bwv6i08z884jm39a7lbwh61rbdnwiv93ybcrifxjmxp5")))

(define-public crate-link-ippi-0.1.1 (c (n "link-ippi") (v "0.1.1") (d (list (d (n "ipp-sys-build-help") (r "^0.1") (d #t) (k 1)) (d (n "link-ippcore") (r "^0.1") (d #t) (k 0)) (d (n "link-ipps") (r "^0.1") (d #t) (k 0)))) (h "0i7pr7nsmvk5lsi107l4pzjkqafcpidpj1qnprksar0v5ccg6qw5")))

(define-public crate-link-ippi-0.1.2 (c (n "link-ippi") (v "0.1.2") (d (list (d (n "ipp-sys-build-help") (r "^0.1.2") (d #t) (k 1)) (d (n "link-ippcore") (r "^0.1") (d #t) (k 0)) (d (n "link-ipps") (r "^0.1") (d #t) (k 0)))) (h "0aw96b6wim6dm2db60194z5aq7h28klpwzhjfyhhdc0hv536ch6x") (l "libippi")))

