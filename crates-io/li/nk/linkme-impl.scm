(define-module (crates-io li nk linkme-impl) #:use-module (crates-io))

(define-public crate-linkme-impl-0.1.0 (c (n "linkme-impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1fqkcaml4akcp6c5ba4z9wvhjy1wcp7p7z0cb1bzi5191vbdj4x9")))

(define-public crate-linkme-impl-0.1.1 (c (n "linkme-impl") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "1wgax0caaqnvqsajf8n8lb09y08m3p60lzr0vcgkrr96k58vkyyz")))

(define-public crate-linkme-impl-0.1.2 (c (n "linkme-impl") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17as9l1kv1lrdszm7p3aqgzc507sg7d420vid1pkzz48d19v7jwx")))

(define-public crate-linkme-impl-0.1.3 (c (n "linkme-impl") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0398m73ybibzp6xp4cc9jrc0l1fs23bgpsh7k592070inf12dwma")))

(define-public crate-linkme-impl-0.1.4 (c (n "linkme-impl") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "17665sn7bikrqyjyfwn5yp5kv8gpz5wczznc6w1ncarwhff7010x")))

(define-public crate-linkme-impl-0.1.5 (c (n "linkme-impl") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1j53dlyra108z7biyw0mb4q3wnlib110qp7x7bmixaivj6z2fhgi")))

(define-public crate-linkme-impl-0.1.6 (c (n "linkme-impl") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06x0qa32v859znlyvb52ih46gs4xbbaav8albdzq5cc8z8k0bhw8")))

(define-public crate-linkme-impl-0.2.0 (c (n "linkme-impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bs7z1mgcji041lqwrpf2c9b7l92bkkf7ij1jcyp6g4qc3pkvjkh")))

(define-public crate-linkme-impl-0.2.1 (c (n "linkme-impl") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qq6asnmpy1f5l8jk3191spca6b6g7k3gwrswd77l19q7i276c42")))

(define-public crate-linkme-impl-0.2.2 (c (n "linkme-impl") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1i1wh0j3akzvaxz90q3fpd4n7jhfkd0zkcdhc35qwf303dak5v26")))

(define-public crate-linkme-impl-0.2.3 (c (n "linkme-impl") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lp1cwg1lxxfs75iq4pmrzhkr6wrhvnbhja4vs91hl7njn4zdq8n")))

(define-public crate-linkme-impl-0.2.4 (c (n "linkme-impl") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jks3xx3hw333kd6sshmsdm67n71fd8lr474bxnd7zwn59m1ma44")))

(define-public crate-linkme-impl-0.2.5 (c (n "linkme-impl") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04diz7613hra57syfd48drygxhqasd6iiai9113kdrnb4i0l132m")))

(define-public crate-linkme-impl-0.2.6 (c (n "linkme-impl") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1b1486wasvfgc722d1p77irfn1zqpdihfjvxkgi060bmjhcc9z5y")))

(define-public crate-linkme-impl-0.2.7 (c (n "linkme-impl") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "09c6p4zg4h9fwz2gldbhfmamvvpxhnghl47f0m8k9a3v9dxl6wxq")))

(define-public crate-linkme-impl-0.2.8 (c (n "linkme-impl") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ncx2z3l1lkaannz0nvcnh0baxqlmn5f026ydva35sgwsw3qgwiv") (r "1.31")))

(define-public crate-linkme-impl-0.2.9 (c (n "linkme-impl") (v "0.2.9") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0j9ivfpk2smdw1wr14lianr3yvwb37shqh3hiqz0vfkscyx7yn2x") (r "1.31")))

(define-public crate-linkme-impl-0.2.10 (c (n "linkme-impl") (v "0.2.10") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1n488hbilqdljklvs4macnlx6b3lixg15adbpn61drrd0z3rvzbk") (r "1.31")))

(define-public crate-linkme-impl-0.3.0 (c (n "linkme-impl") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06yhsf8yivq72v6k9h8iia6ip613bxnniciwv4ql3d9w81x9k282") (r "1.62")))

(define-public crate-linkme-impl-0.3.1 (c (n "linkme-impl") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05ma60vljkdxk395g5i3igqf77isjkccmjqshzjrld2k47b99lv3") (r "1.62")))

(define-public crate-linkme-impl-0.3.2 (c (n "linkme-impl") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "04n156qsccgs0243nxrrrxknla6gca5nrybjh3qjx3c7mkl71lfs") (r "1.62")))

(define-public crate-linkme-impl-0.3.3 (c (n "linkme-impl") (v "0.3.3") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1agycf60fw0zr0lp4nh99rwza9v7s0slir580sr8apk7w64hk9lf") (r "1.62")))

(define-public crate-linkme-impl-0.3.4 (c (n "linkme-impl") (v "0.3.4") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d0ackykhblzz8as6yrf8mzqf1db5m3vlglpswr4niiiq67x5zi9") (r "1.62")))

(define-public crate-linkme-impl-0.3.5 (c (n "linkme-impl") (v "0.3.5") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rq58vxzzqy35qgxvhsblfaghqf6la2lg88j31sjr6zrlpjphjv2") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.6 (c (n "linkme-impl") (v "0.3.6") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "097dsd09ljdpcd25zhxld7qy8jrvc4xgs4lkx5qdjmjicpcyz0ia") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.7 (c (n "linkme-impl") (v "0.3.7") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d80cz2cdzbb6hm94ppwg0ljsqp9pbfgb733v6s5xi1124f03wl3") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.8 (c (n "linkme-impl") (v "0.3.8") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ajxsgi8r1anhnab2gx1izl94431ahi0m1f2gh6skx9lnwizhh54") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.9 (c (n "linkme-impl") (v "0.3.9") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "066z2q40mf1kvqnxqjc162g97c8zp85bbdgkqbhlm1653w8f1068") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.10 (c (n "linkme-impl") (v "0.3.10") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "09m402hlvq3nxq6cp4kswgqfn436p4skc86a798hhnn882zpg6i7") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.11 (c (n "linkme-impl") (v "0.3.11") (d (list (d (n "proc-macro2") (r "^1.0.2") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1q8d4kdwi60k6xq0f308wwgq5ga05lynjx4wp4r7yrqlsl9j29j2") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.12 (c (n "linkme-impl") (v "0.3.12") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1nv6zibfp1ag2hhdvfvbzrkvb5iaj0pb8dnp7na1njwanicc3gwh") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.13 (c (n "linkme-impl") (v "0.3.13") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "00yc6wzpfwcxqbpv8fcz1jkr41sm60szpri9qdzjnx0h0wr9p0bn") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.14 (c (n "linkme-impl") (v "0.3.14") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0632k3cr5wa2hixja69n01kld816zwl041r368bsjk60nbyxwnhx") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.15 (c (n "linkme-impl") (v "0.3.15") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1w7fyj60i5xsd1b0vsv8ls5inar5m4lw6vs6yc7wkp3kmn646a5w") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.16 (c (n "linkme-impl") (v "0.3.16") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1fbdjbslv1j12pcn1bh1rnhpi58i6z3xvc82rd3nm688pr256fml") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.17 (c (n "linkme-impl") (v "0.3.17") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "15j3v4pnkfwbgzmddpwm1133w540fm729h66rgy15m89n5s5j4ms") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.18 (c (n "linkme-impl") (v "0.3.18") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "05phi8lq9wvfxmhxpyvwsq0f3dgkw4qr9irnpb37wqbsalggdcxk") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.19 (c (n "linkme-impl") (v "0.3.19") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0pmq6rqacxzqj0sih8l4mippq33q2r4pdy1s2q73x2xz9x7s6hsv") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.20 (c (n "linkme-impl") (v "0.3.20") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0r3jgddbl5nl5m2dkxvbkjxk5l2y8i2a5qia4fd70zp3gdv3s25d") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.21 (c (n "linkme-impl") (v "0.3.21") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0g8fbib6fnnpcrqvfrvwydchbb55brkh1lkkh7yh5xdj7wldq2fy") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.22 (c (n "linkme-impl") (v "0.3.22") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1zv65brjj9v74prh8r5143i9xdx67gx91c5dqzybdaclijhl5r84") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.23 (c (n "linkme-impl") (v "0.3.23") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1c4kxw461mkn9rygyxzj5czdzq0dwz1m0ni8vbipzgzqcjjj747f") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.24 (c (n "linkme-impl") (v "0.3.24") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1icn6sxd8zcjz05zw98nxwi9ny6369k462n505aj3y2bw0fwhnhy") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.25 (c (n "linkme-impl") (v "0.3.25") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1y4rvivq9xbl70f6lr0kgvh7c07kxznyg3xacjsbfaasvjj5gwdd") (f (quote (("used_linker")))) (r "1.62")))

(define-public crate-linkme-impl-0.3.26 (c (n "linkme-impl") (v "0.3.26") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "021z534lx5v0d019abvj3ylbnrrrf941hbncawajg8xy5nlxxw1r") (f (quote (("used_linker")))) (r "1.62")))

