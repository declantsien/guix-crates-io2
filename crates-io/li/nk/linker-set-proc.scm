(define-module (crates-io li nk linker-set-proc) #:use-module (crates-io))

(define-public crate-linker-set-proc-0.1.0 (c (n "linker-set-proc") (v "0.1.0") (d (list (d (n "paste") (r "^1.0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.55") (f (quote ("full"))) (d #t) (k 0)))) (h "0v4l9isnrc7r4wzhr0vi5y79yzk58maxrkjam0i7b52zba4c5pq7")))

