(define-module (crates-io li nk linkmobility) #:use-module (crates-io))

(define-public crate-linkmobility-0.1.0 (c (n "linkmobility") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)))) (h "17i88kszbxgzvjxcldqmz4qxnp8p101cm6lb7y8pcq1bdcjww7li")))

