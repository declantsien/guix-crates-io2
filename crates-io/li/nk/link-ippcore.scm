(define-module (crates-io li nk link-ippcore) #:use-module (crates-io))

(define-public crate-link-ippcore-0.1.0 (c (n "link-ippcore") (v "0.1.0") (d (list (d (n "ipp-sys-build-help") (r "^0.1") (d #t) (k 1)))) (h "0dm2xm7gg9a4zgs2jfj0lf9ifza4d5flcil1g39qy84jby83j11l")))

(define-public crate-link-ippcore-0.1.1 (c (n "link-ippcore") (v "0.1.1") (d (list (d (n "ipp-sys-build-help") (r "^0.1.2") (d #t) (k 1)))) (h "0lfc87j0wwcb4cxasrz02pw6r37caak4287hm4ap0lp2z34pwj9l") (l "libippcore")))

