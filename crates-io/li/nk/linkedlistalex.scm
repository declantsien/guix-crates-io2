(define-module (crates-io li nk linkedlistalex) #:use-module (crates-io))

(define-public crate-LinkedListAlex-0.1.0 (c (n "LinkedListAlex") (v "0.1.0") (h "0kjq1jyha82k6n50ir0m6bfz8q6qr0q7pigcx38v30iydz6527xr") (y #t) (r "1.68.0")))

(define-public crate-LinkedListAlex-0.1.1 (c (n "LinkedListAlex") (v "0.1.1") (h "1l8g0l1smdqhji1zf4gw7ipzs1wpr32brs73dvn173dh3nhgsa1m") (y #t) (r "1.68.0")))

(define-public crate-LinkedListAlex-0.1.2 (c (n "LinkedListAlex") (v "0.1.2") (h "06044cl1zkmcrh9zy3j0wszy48v9g4xxrmiqsdb9bdimzwl1296c") (y #t) (r "1.68.0")))

(define-public crate-LinkedListAlex-0.1.3 (c (n "LinkedListAlex") (v "0.1.3") (h "1x8y6pwy901aapjmnqwggx6dv96yhcm5nzxmp330krxiy82481j5") (y #t) (r "1.68.0")))

