(define-module (crates-io li nk linkspace) #:use-module (crates-io))

(define-public crate-linkspace-0.1.0 (c (n "linkspace") (v "0.1.0") (h "0qz41r2hcl2mysnc3cgam1fagz39xcah9gx1hvaqqx82qya80112") (y #t)))

(define-public crate-linkspace-0.1.4 (c (n "linkspace") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "backtrace") (r "^0.3.67") (d #t) (k 0)) (d (n "linkspace-common") (r "^0.1.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "16idcrgjwzpk3agiizgl8xarixh7faxlwzzlc6rfzbzj1qcy92ng") (y #t)))

(define-public crate-linkspace-0.3.0 (c (n "linkspace") (v "0.3.0") (h "0ha3gbq64dzvcdfwx3da8blrqjb5xv1mssvlr5gdv12y6sl12397")))

