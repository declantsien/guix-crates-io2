(define-module (crates-io li nk linked_list_c) #:use-module (crates-io))

(define-public crate-linked_list_c-0.0.1 (c (n "linked_list_c") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)))) (h "157927id75z1s8vs9caglyl9chkk993xfk6dgk7rnmr3l9nkgv24")))

(define-public crate-linked_list_c-0.1.0 (c (n "linked_list_c") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "linked_list_c_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0n41yy0ahf039yjs1gg216v1ln9k0q275nir05pc00n5y0vignv0") (f (quote (("default")))) (s 2) (e (quote (("derive" "dep:linked_list_c_derive"))))))

(define-public crate-linked_list_c-0.1.1 (c (n "linked_list_c") (v "0.1.1") (d (list (d (n "linked_list_c_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)))) (h "1jd81r0bdz46i9s9jzkk6sczvcvqx8l5hwvpxn2wx8ii3s7w3nvk") (f (quote (("default")))) (s 2) (e (quote (("derive" "dep:linked_list_c_derive"))))))

(define-public crate-linked_list_c-0.1.2 (c (n "linked_list_c") (v "0.1.2") (d (list (d (n "linked_list_c_derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)))) (h "1fs8fgwc765ayyp5fqydlls97026cq1993vabszf0jd81x1q0zzz") (f (quote (("default")))) (s 2) (e (quote (("derive" "dep:linked_list_c_derive"))))))

