(define-module (crates-io li nk linked-buffer) #:use-module (crates-io))

(define-public crate-linked-buffer-0.0.1 (c (n "linked-buffer") (v "0.0.1") (h "01i3adyiipvm1nv3rld6p741bga7ds5wp7ymyhsfn4zj0j4079j9") (y #t)))

(define-public crate-linked-buffer-0.0.2 (c (n "linked-buffer") (v "0.0.2") (d (list (d (n "bytes") (r "^1") (f (quote ("std"))) (k 0)) (d (n "libc") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "monoio") (r "^0.0.4") (o #t) (k 0)))) (h "0gbqspqq3ldnsv20gw7020h5na6qswigwy7mk2r7mbyqvz7jf90n") (f (quote (("monoio-vec" "monoio" "libc") ("default" "monoio-vec"))))))

