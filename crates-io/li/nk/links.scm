(define-module (crates-io li nk links) #:use-module (crates-io))

(define-public crate-links-0.0.0 (c (n "links") (v "0.0.0") (h "1b2iw0sjjg37043cy1aldgr5p3rp4jsfnp17154rl249x2fqhp02")))

(define-public crate-links-0.1.0 (c (n "links") (v "0.1.0") (d (list (d (n "links_html") (r "^0.1.0") (d #t) (k 0)) (d (n "links_js") (r "^0.1.0") (d #t) (k 0)))) (h "0lw0rbrsfnk7cbpa3pxqndwaii66bjls6pjzdn4kqlvyzynvs3zc")))

