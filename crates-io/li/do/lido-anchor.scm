(define-module (crates-io li do lido-anchor) #:use-module (crates-io))

(define-public crate-lido-anchor-0.0.1 (c (n "lido-anchor") (v "0.0.1") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "lido") (r "=1.1.0-patch.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0q32nw3i3v48j42l34lkhp65cv3kw8kbw8zp18zy0gl8ir0znmn1") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-lido-anchor-0.1.0 (c (n "lido-anchor") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "lido") (r "=1.1.0-patch.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0xkxkg1z2r6i6v4mw1vknhy71gbjfdwpnw8qifb627b3njd2fqvi") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-lido-anchor-0.1.1 (c (n "lido-anchor") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.17.0") (d #t) (k 0)) (d (n "borsh") (r "^0.9.1") (d #t) (k 0)) (d (n "lido") (r "=1.1.0-patch.1") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1v1c8ldbjbd489593y5cr96l86si2j74zjwij86b0b4xxfk62xzm") (f (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

