(define-module (crates-io li fx lifx-core) #:use-module (crates-io))

(define-public crate-lifx-core-0.1.0 (c (n "lifx-core") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)))) (h "1cq8ffap1dfbr1dbc43357qpml8acyyzxjwckq72fr7n6i8fzys4")))

(define-public crate-lifx-core-0.2.0 (c (n "lifx-core") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.2") (d #t) (k 0)))) (h "14c6achdbamm3zinzv5avg6k3d8j4pkn8akizm5dj7m2pxw4lbh0")))

(define-public crate-lifx-core-0.3.0 (c (n "lifx-core") (v "0.3.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m7kj4ndwq1n4y8xn798rd7xczzfgcakqmfdx8g49ym53ph5nczb")))

(define-public crate-lifx-core-0.3.1 (c (n "lifx-core") (v "0.3.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l7y9ck2vfyg7rdrzidnhdw0kx4q1qqi24l63r65r478vwmqd3r3")))

(define-public crate-lifx-core-0.4.0 (c (n "lifx-core") (v "0.4.0") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0iwlsyv5vm7wpa6q0cxzdaxkiqmi6jbr51xws9y7pppn54c422hb")))

