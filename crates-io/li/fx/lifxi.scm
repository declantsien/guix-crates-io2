(define-module (crates-io li fx lifxi) #:use-module (crates-io))

(define-public crate-lifxi-0.1.0 (c (n "lifxi") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "13cl0qax2zn9l2v5la2gai68i394rg5qwq4y5cp3xpkppc92aha4")))

(define-public crate-lifxi-0.1.1 (c (n "lifxi") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.9.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.82") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.82") (d #t) (k 0)))) (h "053x63dkdrvr6541lch7fxfmfn3r5nakqvradfv576zcik5i2yx2")))

