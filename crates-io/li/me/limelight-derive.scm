(define-module (crates-io li me limelight-derive) #:use-module (crates-io))

(define-public crate-limelight-derive-0.1.0 (c (n "limelight-derive") (v "0.1.0") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0ksimif6yii1b1l6hy1bp59shxw0ar14s5ff43iswy6nxdmjhfkm")))

(define-public crate-limelight-derive-0.1.1 (c (n "limelight-derive") (v "0.1.1") (d (list (d (n "inflections") (r "^1.1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0jz4qyikfcr9yjsqj50n70z9ks9f849sh8kplvxdan7b1pf8ws3y")))

