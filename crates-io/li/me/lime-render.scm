(define-module (crates-io li me lime-render) #:use-module (crates-io))

(define-public crate-lime-render-0.1.0 (c (n "lime-render") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "specs") (r "= 0.11.0-alpha5") (d #t) (k 0)) (d (n "vulkano") (r "^0.9.0") (d #t) (k 0)) (d (n "vulkano-shader-derive") (r "^0.9.0") (d #t) (k 0)) (d (n "vulkano-win") (r "^0.9.0") (d #t) (k 0)) (d (n "winit") (r "^0.11.0") (d #t) (k 0)))) (h "0pxvf4yv9ch1k40nn2cvkigbwi3wrvd9ziwfsbxd6j8l6in6zsgd")))

