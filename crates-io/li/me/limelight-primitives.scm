(define-module (crates-io li me limelight-primitives) #:use-module (crates-io))

(define-public crate-limelight-primitives-0.1.3 (c (n "limelight-primitives") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "bytemuck") (r "^1.7.2") (d #t) (k 0)) (d (n "limelight") (r "^0.1.3") (d #t) (k 0)) (d (n "palette") (r "^0.6.0") (d #t) (k 0)))) (h "12fq3cd61936ia6knam7xkwdd53xpxg96854iv5g5hxrp3wwl36y")))

