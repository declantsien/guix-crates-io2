(define-module (crates-io li me lime-main-loop) #:use-module (crates-io))

(define-public crate-lime-main-loop-0.1.0 (c (n "lime-main-loop") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)) (d (n "winit") (r "^0.11.0") (d #t) (k 0)))) (h "1phwi0ra89klqpxns76r8gmgvlcij04cgdcld6sq0jdwn9a7gadp")))

