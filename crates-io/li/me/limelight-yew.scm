(define-module (crates-io li me limelight-yew) #:use-module (crates-io))

(define-public crate-limelight-yew-0.1.0 (c (n "limelight-yew") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.51") (d #t) (k 0)) (d (n "gloo-events") (r "^0.1.1") (d #t) (k 0)) (d (n "gloo-render") (r "^0.1.0") (d #t) (k 0)) (d (n "limelight") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.55") (f (quote ("Window" "HtmlCanvasElement"))) (d #t) (k 0)) (d (n "yew") (r "^0.19.3") (d #t) (k 0)))) (h "1fxl6pvi5qkzr9kmwbhdghmqhny5qv0c066xs1001s1sp13zq2wk")))

