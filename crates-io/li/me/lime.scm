(define-module (crates-io li me lime) #:use-module (crates-io))

(define-public crate-lime-0.1.0 (c (n "lime") (v "0.1.0") (d (list (d (n "lime-main-loop") (r "^0.1.0") (d #t) (k 0)) (d (n "lime-render") (r "^0.1.0") (d #t) (k 0)) (d (n "lime-ui") (r "^0.1.0") (d #t) (k 0)))) (h "05lmkibwfq8yr2wzw29ncxz4imidmwxhflkzdw2a0r37p7q3n46v")))

