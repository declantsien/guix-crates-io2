(define-module (crates-io li t- lit-html) #:use-module (crates-io))

(define-public crate-lit-html-0.0.0 (c (n "lit-html") (v "0.0.0") (d (list (d (n "globals") (r "^1") (d #t) (k 0)) (d (n "js") (r "^0.2") (d #t) (k 0)))) (h "138rvwqzr5c08h6190s4b13wcbky81yn7506db1xphqrgqj5yh7c")))

(define-public crate-lit-html-0.0.1 (c (n "lit-html") (v "0.0.1") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js") (r "^0.2.3") (d #t) (k 0)) (d (n "lit-html-macro") (r "^0") (d #t) (k 0)))) (h "1gxhj6581llg5f3kjb43dz38lh04n5asl73q2dips9mv8s0n5gma")))

(define-public crate-lit-html-0.0.2 (c (n "lit-html") (v "0.0.2") (d (list (d (n "globals") (r "^1.0.1") (d #t) (k 0)) (d (n "js") (r "^0.2.4") (d #t) (k 0)) (d (n "lit-html-macro") (r "^0") (d #t) (k 0)))) (h "0bb93nxq992nfcck2z740nma7kyn83ccz2sn2wvmm5vddyr459xl")))

(define-public crate-lit-html-0.1.0 (c (n "lit-html") (v "0.1.0") (d (list (d (n "js") (r "^0") (d #t) (k 0)))) (h "0ycqnmvnv8lmc3var23spdw0ybva383cxjkykiilb807gpbcn5jw")))

(define-public crate-lit-html-0.1.1 (c (n "lit-html") (v "0.1.1") (d (list (d (n "js") (r "^0") (d #t) (k 0)))) (h "1zcqzlrmv3ywaczpydkxl6w63nhad59bscbqy1nfvskmppf4hlvr")))

(define-public crate-lit-html-0.1.2 (c (n "lit-html") (v "0.1.2") (d (list (d (n "js") (r "^0") (d #t) (k 0)))) (h "1a3f08scxdkylp2nmm392ajiljgqzxmsjbaw8306cbjw2cs748gx")))

