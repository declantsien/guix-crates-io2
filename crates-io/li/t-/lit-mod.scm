(define-module (crates-io li t- lit-mod) #:use-module (crates-io))

(define-public crate-lit-mod-0.1.0 (c (n "lit-mod") (v "0.1.0") (d (list (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1q0cgh76wwsizbjmf83fbv9lin8m2v40m074k4s4q0mz9dr7w781") (r "1.64")))

(define-public crate-lit-mod-0.2.0 (c (n "lit-mod") (v "0.2.0") (d (list (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0adwlsvdqzl1iyqy4kl9xis0ghpycy3mzi9s2g4my54na0xjpbr2") (r "1.64")))

