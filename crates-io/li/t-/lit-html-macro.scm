(define-module (crates-io li t- lit-html-macro) #:use-module (crates-io))

(define-public crate-lit-html-macro-0.0.0 (c (n "lit-html-macro") (v "0.0.0") (d (list (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0qw6c76x45v040ykmcwa5nxqdfj6bhy8czbp70js61pvx28gg9ra")))

(define-public crate-lit-html-macro-0.0.1 (c (n "lit-html-macro") (v "0.0.1") (d (list (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0schdqlghnk1ggdpmw1s3ilpbvpj25xsdm9vm1fy1a65mrf8a3cc")))

