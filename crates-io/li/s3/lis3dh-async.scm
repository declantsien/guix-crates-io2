(define-module (crates-io li s3 lis3dh-async) #:use-module (crates-io))

(define-public crate-lis3dh-async-0.5.0 (c (n "lis3dh-async") (v "0.5.0") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "num_enum") (r "~0.5") (k 0)))) (h "0davhjiwwldmy05lx2zayxv39bxqxlkv2vx2gmgzp8b2j27rf97v") (r "1.65")))

(define-public crate-lis3dh-async-0.5.1 (c (n "lis3dh-async") (v "0.5.1") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.1.0-alpha.3") (d #t) (k 0)) (d (n "num_enum") (r "~0.5") (k 0)))) (h "04x3xbfcc7664v41cjc68cgvsr1fg25r65w398v0m1k843ycpvcj") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.65")))

(define-public crate-lis3dh-async-0.6.0 (c (n "lis3dh-async") (v "0.6.0") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "num_enum") (r "~0.5") (k 0)))) (h "18d4326vzfcj8fsnzpl8jk3wm35gzhynpc3fbpnk43h2wjaprhxa") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.65")))

(define-public crate-lis3dh-async-0.6.1 (c (n "lis3dh-async") (v "0.6.1") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^1.0.0-alpha.9") (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.2.0-alpha.0") (d #t) (k 0)) (d (n "num_enum") (r "~0.5") (k 0)))) (h "13c6zf5cbc9yd7dl8fgfrkgckvaigsli5kfn75b79pqnv66fav4n") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.65")))

(define-public crate-lis3dh-async-0.7.0 (c (n "lis3dh-async") (v "0.7.0") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^0.2.0-alpha.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (k 0)))) (h "1859iwxi48270wq8k0w7imm8lqvvh9jajwjz17n3rvsb5k950q16") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.65")))

(define-public crate-lis3dh-async-0.8.0 (c (n "lis3dh-async") (v "0.8.0") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0-rc.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (k 0)))) (h "0kw319pxp1c76mik8s4h9lp0kk6ajpisi4nfnlgc0fzyq9rklxp4") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.65")))

(define-public crate-lis3dh-async-0.9.0 (c (n "lis3dh-async") (v "0.9.0") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (k 0)))) (h "00gsy2s5kyq6y3s5ldql12ci01lqkkcfdlpi8rcx1gah639iaih2") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.65")))

(define-public crate-lis3dh-async-0.9.1 (c (n "lis3dh-async") (v "0.9.1") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (k 0)))) (h "1fqdz8z719s01rqd7dvzzzfrw21cmrp100v8wch5zncnlkxsi086") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.65")))

(define-public crate-lis3dh-async-0.9.2 (c (n "lis3dh-async") (v "0.9.2") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "defmt") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "embedded-hal-async") (r "^1.0.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (k 0)))) (h "0x3cjfmasc9sc38vwihcryc7qsdk3wxw05f8k7cwj6h51fda2k09") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt")))) (r "1.65")))

