(define-module (crates-io li s3 lis3dh-spi) #:use-module (crates-io))

(define-public crate-lis3dh-spi-0.0.1 (c (n "lis3dh-spi") (v "0.0.1") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (f (quote ("vector"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1x58qdva8mfh57d8mf4lwshfqwcynr026ghhh1vbslbqy6l5wwqs") (y #t)))

(define-public crate-lis3dh-spi-0.0.2 (c (n "lis3dh-spi") (v "0.0.2") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (f (quote ("vector"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1ca1zy6rqqkva542lc8xfpwfz33k003xl4rz8ydfyf300rjfafzg")))

(define-public crate-lis3dh-spi-0.0.3 (c (n "lis3dh-spi") (v "0.0.3") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (f (quote ("vector"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "00jh4ij12v3vxy4ksxjsgfbr0y8xfs4w9n56ghl43dlfjw3576df")))

(define-public crate-lis3dh-spi-0.0.4 (c (n "lis3dh-spi") (v "0.0.4") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (f (quote ("vector"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "1mz11bba9bcllnyhhmnslhj10my3yvlgp43xld6s2pq02x4gwfs0")))

(define-public crate-lis3dh-spi-0.0.5 (c (n "lis3dh-spi") (v "0.0.5") (d (list (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "micromath") (r "^2.0.0") (f (quote ("vector"))) (d #t) (k 0)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (f (quote ("libm"))) (k 0)))) (h "0g9k2glqcn6dsimmxpql8420wap4dv05hy4dv4nfw4jgwz1rpn54")))

