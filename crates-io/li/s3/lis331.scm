(define-module (crates-io li s3 lis331) #:use-module (crates-io))

(define-public crate-lis331-0.1.1 (c (n "lis331") (v "0.1.1") (d (list (d (n "accelerometer") (r "~0.12") (d #t) (k 0)) (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "embedded-hal") (r "~0.2") (d #t) (k 0)) (d (n "embuild") (r "^0.28") (d #t) (k 1)) (d (n "esp-idf-hal") (r "^0.36.0") (d #t) (k 0)) (d (n "esp-idf-sys") (r "^0.31") (f (quote ("binstart"))) (d #t) (k 0)) (d (n "num_enum") (r "~0.5") (k 0)))) (h "13i6b6knal3g1qajn85i4dhdcvrikwdy65rs5hifh57pn27yzpid") (f (quote (("pio" "esp-idf-sys/pio"))))))

