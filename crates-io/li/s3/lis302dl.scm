(define-module (crates-io li s3 lis302dl) #:use-module (crates-io))

(define-public crate-lis302dl-0.1.0 (c (n "lis302dl") (v "0.1.0") (d (list (d (n "accelerometer") (r "^0.11.0") (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)))) (h "1c6kkks70qgc97q1r3nmy8pc91038g4gr9z9r2csfwxflk7263kb")))

