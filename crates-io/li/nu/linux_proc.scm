(define-module (crates-io li nu linux_proc) #:use-module (crates-io))

(define-public crate-linux_proc-0.1.0 (c (n "linux_proc") (v "0.1.0") (d (list (d (n "quicli") (r "^0.3") (d #t) (k 2)))) (h "0pppii273r6kj2kfdwr4aq20cg9wslkq7ryh6b1ssmcrwg6794wg")))

(define-public crate-linux_proc-0.1.1 (c (n "linux_proc") (v "0.1.1") (d (list (d (n "quicli") (r "^0.3") (d #t) (k 2)))) (h "11iwmj17bxprsfgmwbim072b9bqnxdampd2nv0a113s8mf8daa3v")))

