(define-module (crates-io li nu linuxkit-utils) #:use-module (crates-io))

(define-public crate-linuxkit-utils-0.1.0 (c (n "linuxkit-utils") (v "0.1.0") (d (list (d (n "envsubst") (r "^0.2") (d #t) (k 0)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1gz7n3cp8sgvj88807zczp3snxcyyjypnfj1rz9b783l3w87crn0")))

