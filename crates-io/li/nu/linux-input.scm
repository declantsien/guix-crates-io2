(define-module (crates-io li nu linux-input) #:use-module (crates-io))

(define-public crate-linux-input-0.1.0 (c (n "linux-input") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.17") (d #t) (k 0)))) (h "1vwhwgzysb423mc9jnn75adpbh15ckjg0i6dgajxd6q9gswdby66")))

