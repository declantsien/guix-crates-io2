(define-module (crates-io li nu linux-keyutils) #:use-module (crates-io))

(define-public crate-linux-keyutils-0.1.0 (c (n "linux-keyutils") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("std" "derive"))) (k 2)) (d (n "libc") (r "^0.2.132") (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 2)))) (h "1qf6a9xjf9klxi0a5pw5dxljx55xbgl3r49dp57vb19qlwla8ibd") (f (quote (("std") ("default"))))))

(define-public crate-linux-keyutils-0.1.1 (c (n "linux-keyutils") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("std" "derive"))) (k 2)) (d (n "libc") (r "^0.2.132") (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 2)))) (h "1mlp45byvh8jwsj3x0jr0fg2w36487lp2178i7caafyw4vmba2df") (f (quote (("std") ("default"))))))

(define-public crate-linux-keyutils-0.1.2 (c (n "linux-keyutils") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("std" "derive"))) (k 2)) (d (n "libc") (r "^0.2.132") (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 2)))) (h "1d6spg815i4h3yzj62j8q2h5hnyvyirgng6fdivvvh21dvkr874c") (f (quote (("std") ("default"))))))

(define-public crate-linux-keyutils-0.2.0 (c (n "linux-keyutils") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("std" "derive"))) (k 2)) (d (n "libc") (r "^0.2.132") (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 2)))) (h "1aaj7r38cxj1g5if702hd0s00cqhvslmbc96igqra92h1wrb2d5q") (f (quote (("std") ("default"))))))

(define-public crate-linux-keyutils-0.2.1 (c (n "linux-keyutils") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("std" "derive"))) (k 2)) (d (n "libc") (r "^0.2.132") (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 2)))) (h "0jpd9x9i7jj96a9mjbw2pypnfa6jng0d61xgyzhldggkrj9r4clv") (f (quote (("std") ("default"))))))

(define-public crate-linux-keyutils-0.2.2 (c (n "linux-keyutils") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("std" "derive"))) (k 2)) (d (n "libc") (r "^0.2.132") (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 2)))) (h "06l38mjj888k4jr4fnkzk9gm8igw64rci799cdsy16kk42k55i0h") (f (quote (("std") ("default"))))))

(define-public crate-linux-keyutils-0.2.3 (c (n "linux-keyutils") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "clap") (r "^3.2.22") (f (quote ("std" "derive"))) (k 2)) (d (n "libc") (r "^0.2.132") (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 2)))) (h "0jxq2bsrr80diyh8h4a0ls6yaljhyvj6ha2qmfshn7fxyrkvn9rz") (f (quote (("std") ("default"))))))

(define-public crate-linux-keyutils-0.2.4 (c (n "linux-keyutils") (v "0.2.4") (d (list (d (n "bitflags") (r "^2.4") (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("std" "derive"))) (k 2)) (d (n "libc") (r "^0.2.132") (k 0)) (d (n "zeroize") (r "^1.5.7") (d #t) (k 2)))) (h "13nipvk2mzk76y7yfsqwnwsqk21x6xy8fkmqz5is99fqbzn4j7kn") (f (quote (("std" "bitflags/std") ("default"))))))

