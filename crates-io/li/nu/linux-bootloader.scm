(define-module (crates-io li nu linux-bootloader) #:use-module (crates-io))

(define-public crate-linux-bootloader-0.0.1 (c (n "linux-bootloader") (v "0.0.1") (d (list (d (n "bitflags") (r "^2.3.3") (d #t) (k 0)) (d (n "goblin") (r "^0.6.1") (f (quote ("pe64" "alloc"))) (k 0)) (d (n "log") (r "^0.4.19") (f (quote ("max_level_info" "release_max_level_warn"))) (k 0)) (d (n "sha1_smol") (r "^1.0.0") (d #t) (k 0)) (d (n "uefi") (r "^0.24.0") (f (quote ("alloc" "global_allocator"))) (k 0)))) (h "0i4kw5as2x4zg5hsh193n0bqqqnkgk13ss3d8dpxdr9nhyzc918f")))

