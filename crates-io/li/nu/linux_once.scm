(define-module (crates-io li nu linux_once) #:use-module (crates-io))

(define-public crate-linux_once-0.1.0 (c (n "linux_once") (v "0.1.0") (d (list (d (n "linux-futex") (r "^0.1.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "029ii3n86lgi3z85gjhr2n79v4bwv68kq72b9hidr9f1xcdnj9xy") (f (quote (("bench"))))))

(define-public crate-linux_once-0.1.1 (c (n "linux_once") (v "0.1.1") (d (list (d (n "linux-futex") (r "^0.1.1") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "10c4xg445v04qmd8ccmdkp723fnaz5ywfa57sbn4w2xhnbywkkaa") (f (quote (("bench"))))))

