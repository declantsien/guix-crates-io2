(define-module (crates-io li nu linux) #:use-module (crates-io))

(define-public crate-linux-0.0.0-rc (c (n "linux") (v "0.0.0-rc") (h "0ijs4ffdc79igsg0a154jjf6203mxsd62d7svjqxj11hc2wmigkg")))

(define-public crate-linux-0.0.1 (c (n "linux") (v "0.0.1") (d (list (d (n "linux-macros") (r "^0.0.1") (d #t) (k 0)) (d (n "memchr") (r "^2.2") (k 0)))) (h "0ljn72ydgsj6rmr77rwqqymq5yr4krz0wwdacn6b5jk90lvb02mb") (f (quote (("std") ("retry-interrupted") ("default" "std" "retry-interrupted"))))))

