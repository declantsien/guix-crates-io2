(define-module (crates-io li nu linux-bzimage-builder) #:use-module (crates-io))

(define-public crate-linux-bzimage-builder-0.1.0 (c (n "linux-bzimage-builder") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "xmas-elf") (r "^0.9.1") (d #t) (k 0)))) (h "0q7fv8pjnmadipyd5gr9bvfkws6hz3ngv4ka5k2dvkfcnpki7m2r")))

