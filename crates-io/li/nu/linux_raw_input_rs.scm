(define-module (crates-io li nu linux_raw_input_rs) #:use-module (crates-io))

(define-public crate-linux_raw_input_rs-0.1.0 (c (n "linux_raw_input_rs") (v "0.1.0") (h "1kqa9g4nppi8jj0zqh8fbg8yhg7l55401yagdm0y6pfwy3glg3l4")))

(define-public crate-linux_raw_input_rs-0.1.1 (c (n "linux_raw_input_rs") (v "0.1.1") (h "0mwpwd9v2wns8syvwncih89ln7rkk0knvdpsrkln6v2v9xrfssqv")))

(define-public crate-linux_raw_input_rs-0.1.2 (c (n "linux_raw_input_rs") (v "0.1.2") (h "0ragqynfbmrfs46frf49ms3jif6ngs2n90hs88x4r2d97dnwq16d")))

(define-public crate-linux_raw_input_rs-0.1.3 (c (n "linux_raw_input_rs") (v "0.1.3") (h "0xf1mp0c0j7r16h83i0i7llb07vf12q8i8ynvbpwq90d0zmrpfym")))

(define-public crate-linux_raw_input_rs-0.1.5 (c (n "linux_raw_input_rs") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)))) (h "01c9wsjikglil4g11wn697msxnhbkwmh5hba7m75p0qgkgc6xary")))

(define-public crate-linux_raw_input_rs-0.1.6 (c (n "linux_raw_input_rs") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.8") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.8") (d #t) (k 0)))) (h "06wbcs39c0avas8f9hl0402497415cs6b0cxd73mcp7zih92s04w")))

