(define-module (crates-io li nu linux-embedded-hal) #:use-module (crates-io))

(define-public crate-linux-embedded-hal-0.1.0 (c (n "linux-embedded-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.1") (d #t) (k 0)))) (h "0afra0ihywp07kcqsafwixyl1b0350xvz6lc9n15vvkl07jj5df5")))

(define-public crate-linux-embedded-hal-0.1.1 (c (n "linux-embedded-hal") (v "0.1.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.1.0") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.1") (d #t) (k 0)))) (h "0bh4j4imsa0mr3rc8hlspn9ii19dsn8nnpzbmpj171c2zjjlb948")))

(define-public crate-linux-embedded-hal-0.2.0 (c (n "linux-embedded-hal") (v "0.2.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.1") (d #t) (k 0)))) (h "18z1qzcjgkkzdaadly7p3jvx90pdcb6rqf3gjpgzcf13zd2n5x3z")))

(define-public crate-linux-embedded-hal-0.2.1 (c (n "linux-embedded-hal") (v "0.2.1") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "i2cdev") (r "^0.3.1") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.1") (d #t) (k 0)))) (h "0g4ks29ihc62pi34cglgih3igadi3d5f686sy3nkv0yxa8jfi8p9")))

(define-public crate-linux-embedded-hal-0.2.2 (c (n "linux-embedded-hal") (v "0.2.2") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.0") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.1") (d #t) (k 0)) (d (n "spidev") (r "^0.3.0") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.1") (d #t) (k 0)))) (h "117vr35fkvmqf1xrb2bni9l4d80vgfly3hwinnpj41nbxv09cl3r")))

(define-public crate-linux-embedded-hal-0.3.0 (c (n "linux-embedded-hal") (v "0.3.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "^0.2.3") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.3") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "openpty") (r "^0.1.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-unix") (r "^0.4.0") (d #t) (k 0)) (d (n "spidev") (r "^0.4") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5") (d #t) (k 0)))) (h "0b84mgr7wkf7s1wy9iricwb36f03wp78mvghr6dr8bdv5672sjz0")))

(define-public crate-linux-embedded-hal-0.4.0-alpha.0 (c (n "linux-embedded-hal") (v "0.4.0-alpha.0") (d (list (d (n "cast") (r "^0.2.2") (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.4") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "i2cdev") (r "^0.4.3") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "openpty") (r "^0.1.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-unix") (r "^0.4.0") (d #t) (k 0)) (d (n "spidev") (r "^0.4") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5") (o #t) (d #t) (k 0)))) (h "12ha5m8kb1288jk1mr98gcb6wdim2nb0i9q52hca67453m1lld6g") (f (quote (("gpio_sysfs" "sysfs_gpio") ("gpio_cdev" "gpio-cdev") ("default" "gpio_cdev" "gpio_sysfs"))))))

(define-public crate-linux-embedded-hal-0.3.1 (c (n "linux-embedded-hal") (v "0.3.1") (d (list (d (n "cast") (r "^0.3") (k 0)) (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "i2cdev") (r "^0.5") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "openpty") (r "^0.1.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-unix") (r "^0.4.0") (d #t) (k 0)) (d (n "spidev") (r "^0.5") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "09jiasdbj7j2jq7vsbn2sbbj3jv7hlgn1xask96bkb0rmwal4a5b") (f (quote (("gpio_sysfs" "sysfs_gpio") ("gpio_cdev" "gpio-cdev") ("default" "gpio_cdev" "gpio_sysfs")))) (y #t)))

(define-public crate-linux-embedded-hal-0.4.0-alpha.1 (c (n "linux-embedded-hal") (v "0.4.0-alpha.1") (d (list (d (n "cast") (r "^0.3") (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.5") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "i2cdev") (r "^0.5") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "openpty") (r "^0.1.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-unix") (r "^0.4.0") (d #t) (k 0)) (d (n "spidev") (r "^0.5") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6") (o #t) (d #t) (k 0)))) (h "095j8dpj65y02arkrgrh7iczjbn5fyxscwrzq8c5zj2agsaam8km") (f (quote (("gpio_sysfs" "sysfs_gpio") ("gpio_cdev" "gpio-cdev") ("default" "gpio_cdev" "gpio_sysfs") ("async-tokio" "gpio-cdev/async-tokio"))))))

(define-public crate-linux-embedded-hal-0.3.2 (c (n "linux-embedded-hal") (v "0.3.2") (d (list (d (n "cast") (r "^0.3") (k 0)) (d (n "embedded-hal") (r "^0.2.6") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "i2cdev") (r "^0.5") (d #t) (k 0)) (d (n "nb") (r "^0.1.3") (d #t) (k 0)) (d (n "openpty") (r "^0.1.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-unix") (r "^0.4.0") (d #t) (k 0)) (d (n "spidev") (r "^0.5") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "void") (r "^1") (d #t) (k 0)))) (h "0pcip70gvgjh6p4cy8xryaqy24lqraraxw9pc5p79jshmii1g1k1") (f (quote (("gpio_sysfs" "sysfs_gpio") ("gpio_cdev" "gpio-cdev") ("default" "gpio_cdev" "gpio_sysfs"))))))

(define-public crate-linux-embedded-hal-0.4.0-alpha.2 (c (n "linux-embedded-hal") (v "0.4.0-alpha.2") (d (list (d (n "cast") (r "^0.3") (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.7") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "i2cdev") (r "^0.5.1") (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "openpty") (r "^0.2.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-unix") (r "^0.4.0") (d #t) (k 0)) (d (n "spidev") (r "^0.5.1") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "1gjmmxkqyqdvf6dp0afjahpr8kllgv5q7clc6ak3r6yjyp77zxjr") (f (quote (("gpio_sysfs" "sysfs_gpio") ("gpio_cdev" "gpio-cdev") ("default" "gpio_cdev" "gpio_sysfs") ("async-tokio" "gpio-cdev/async-tokio"))))))

(define-public crate-linux-embedded-hal-0.4.0-alpha.3 (c (n "linux-embedded-hal") (v "0.4.0-alpha.3") (d (list (d (n "cast") (r "^0.3") (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "i2cdev") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.23.1") (d #t) (k 0)) (d (n "openpty") (r "^0.2.0") (d #t) (k 2)) (d (n "serial-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serial-unix") (r "^0.4.0") (d #t) (k 0)) (d (n "spidev") (r "^0.5.1") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "07ycaaz8rip7gwibzfvlh599fkbk2664xdfaz94ap4fang6w7ri6") (f (quote (("spi" "spidev") ("i2c" "i2cdev") ("gpio_sysfs" "sysfs_gpio") ("gpio_cdev" "gpio-cdev") ("default" "gpio_cdev" "gpio_sysfs" "i2c" "spi") ("async-tokio" "gpio-cdev/async-tokio"))))))

(define-public crate-linux-embedded-hal-0.4.0-alpha.4 (c (n "linux-embedded-hal") (v "0.4.0-alpha.4") (d (list (d (n "cast") (r "^0.3") (k 0)) (d (n "embedded-hal") (r "=1.0.0-rc.3") (d #t) (k 0)) (d (n "embedded-hal-nb") (r "=1.0.0-rc.3") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "i2cdev") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)) (d (n "openpty") (r "^0.2.0") (d #t) (k 2)) (d (n "serialport") (r "^4.2.0") (k 0)) (d (n "spidev") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "01z8y1v9zwk0na4x76n1jy2p87l4hwj4svia5ssp32p6b8b5372x") (f (quote (("spi" "spidev") ("i2c" "i2cdev") ("gpio_sysfs" "sysfs_gpio") ("gpio_cdev" "gpio-cdev") ("default" "gpio_cdev" "gpio_sysfs" "i2c" "spi") ("async-tokio" "gpio-cdev/async-tokio"))))))

(define-public crate-linux-embedded-hal-0.4.0 (c (n "linux-embedded-hal") (v "0.4.0") (d (list (d (n "cast") (r "^0.3") (k 0)) (d (n "embedded-hal") (r "^1") (d #t) (k 0)) (d (n "embedded-hal-nb") (r "^1") (d #t) (k 0)) (d (n "gpio-cdev") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "i2cdev") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "nb") (r "^1") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)) (d (n "openpty") (r "^0.2.0") (d #t) (k 2)) (d (n "serialport") (r "^4.2.0") (k 0)) (d (n "spidev") (r "^0.6.0") (o #t) (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.6.1") (o #t) (d #t) (k 0)))) (h "00ps9i2kb9w153gmjsph4yf46j4ji1lwgwf6cq7q3dp14svndmd8") (f (quote (("spi" "spidev") ("i2c" "i2cdev") ("gpio_sysfs" "sysfs_gpio") ("gpio_cdev" "gpio-cdev") ("default" "gpio_cdev" "gpio_sysfs" "i2c" "spi") ("async-tokio" "gpio-cdev/async-tokio"))))))

