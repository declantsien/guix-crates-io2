(define-module (crates-io li nu linux_max6675) #:use-module (crates-io))

(define-public crate-linux_max6675-0.1.0 (c (n "linux_max6675") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "spidev") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "1ca5z5xlyshg6aahl7rzxycl3kfigkb018gdlxy1plh8zxwxx2jq")))

(define-public crate-linux_max6675-0.1.1 (c (n "linux_max6675") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "spidev") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "16mg4r16vj6zfpgqnv1lr3bl0fawn4nq5q1095lq50ln18rhifda")))

(define-public crate-linux_max6675-0.2.0 (c (n "linux_max6675") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "simmer") (r "^0.2") (d #t) (k 0)) (d (n "spidev") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "0ns313dnpzsbl5b30bk38x8hg2cvsw6igaciz3pki4f06nqvicpz")))

(define-public crate-linux_max6675-0.2.1 (c (n "linux_max6675") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "approx_eq") (r "^0.1.8") (d #t) (k 2)) (d (n "simmer") (r "^0.3") (d #t) (k 0)) (d (n "spidev") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 2)))) (h "1c86n9gfywvqp44immib3gr6ayb73ha9jy4m21ias6hz1df3i4mi")))

