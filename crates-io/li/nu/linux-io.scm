(define-module (crates-io li nu linux-io) #:use-module (crates-io))

(define-public crate-linux-io-0.1.0 (c (n "linux-io") (v "0.1.0") (d (list (d (n "linux-unsafe") (r "^0.2.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1dwzrb4dm347c066csj4pf84r6rl4df1mcwx4n9lfhaxksyp3ajk") (f (quote (("std") ("default" "std"))))))

(define-public crate-linux-io-0.2.0 (c (n "linux-io") (v "0.2.0") (d (list (d (n "linux-unsafe") (r "^0.5.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "0a66ch3wasyjnq1h93b0gqybqvn9rqjh24byjwbaij2vblp7zlgq") (f (quote (("std") ("default" "std"))))))

(define-public crate-linux-io-0.3.0 (c (n "linux-io") (v "0.3.0") (d (list (d (n "linux-unsafe") (r "^0.6.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "170yhd0w0bfjj540ramm09wvcgmpamr5dns2pc7pzn7ic7pvghqn") (f (quote (("std") ("default" "std"))))))

(define-public crate-linux-io-0.4.0 (c (n "linux-io") (v "0.4.0") (d (list (d (n "linux-unsafe") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1phhilj8yifw3ll3hjyy0mhswpl9r74p8b6cqrf4k5wll8i66mgp") (f (quote (("std") ("default" "std"))))))

(define-public crate-linux-io-0.5.0 (c (n "linux-io") (v "0.5.0") (d (list (d (n "linux-unsafe") (r "^0.8.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "04vpw4mjr6qndajw907gl56wlznp2pba2n8bavrl1wi7mhdgi27x") (f (quote (("std") ("default" "std"))))))

(define-public crate-linux-io-0.6.0 (c (n "linux-io") (v "0.6.0") (d (list (d (n "linux-unsafe") (r "^0.9.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 2)))) (h "1092p2zc3fzkd6nf5zv6jdjjsrvjjgvcmxw1faz61kww4c79vsmc") (f (quote (("std") ("default" "std"))))))

