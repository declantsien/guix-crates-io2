(define-module (crates-io li nu linux-nvme-sys) #:use-module (crates-io))

(define-public crate-linux-nvme-sys-0.1.0 (c (n "linux-nvme-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)))) (h "040mz7gj0kjqf2jg37j75cj4mw1j37rcxvyzjacmaidpijakysm3")))

(define-public crate-linux-nvme-sys-0.2.0 (c (n "linux-nvme-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)))) (h "10q8wr8g4vj9x5l05rcgscyz3ffz312m9pfwvcpjn2f15chawf5c")))

(define-public crate-linux-nvme-sys-0.3.0 (c (n "linux-nvme-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "nix") (r "^0.15") (d #t) (k 0)))) (h "1yn29gmhr4x95gji9ymvh0r9rcj1g2dq3fcbnsc7kj0bhjc5g150")))

(define-public crate-linux-nvme-sys-0.4.0 (c (n "linux-nvme-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "nix") (r "^0.26") (d #t) (k 0)))) (h "1wasvgrl7wvq4qxqykcndqzbjbn4i80dr23p54a11pmw7219p72d")))

