(define-module (crates-io li nu linux-raw-vdso) #:use-module (crates-io))

(define-public crate-linux-raw-vdso-0.1.0 (c (n "linux-raw-vdso") (v "0.1.0") (h "1rgchc22al5kkrfsqrrm0bk10d1lnl0mf8xv5bbk0zvb03mmxpvf")))

(define-public crate-linux-raw-vdso-0.1.1 (c (n "linux-raw-vdso") (v "0.1.1") (h "1fls3r01fs2716qr87mi7mpmgj59zp5r2rxd0nyg2jmn1zlqbr7r")))

(define-public crate-linux-raw-vdso-0.1.2 (c (n "linux-raw-vdso") (v "0.1.2") (h "0k8159mqzjhwnw4820rw8vb66v37zyqjfid7rmy1s0m6bwlac6c6")))

(define-public crate-linux-raw-vdso-0.1.3 (c (n "linux-raw-vdso") (v "0.1.3") (h "048l9kyln9y21w2kv8pkasgjkjp8rkj5n8hky7qf1gdllq6afms3")))

(define-public crate-linux-raw-vdso-0.1.4 (c (n "linux-raw-vdso") (v "0.1.4") (h "1csxicxxrwki5sj06m0q4hc0ymykdnfl3ffi7was6rhr6zzn8l8j")))

