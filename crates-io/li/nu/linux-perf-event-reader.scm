(define-module (crates-io li nu linux-perf-event-reader) #:use-module (crates-io))

(define-public crate-linux-perf-event-reader-0.1.0 (c (n "linux-perf-event-reader") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "038mg95br3pj7f7j3b98zhrz4n3ycd5hh8bgbq4x0nlj38jil22a")))

(define-public crate-linux-perf-event-reader-0.2.0 (c (n "linux-perf-event-reader") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0mq8zzyd79bf5ak3w02ih5hc4rfgb4awjvz77d9sswmci53ysic9")))

(define-public crate-linux-perf-event-reader-0.3.0 (c (n "linux-perf-event-reader") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1p8lc2bhkcls2n88vlw5w6ldjsqcaaq0wlpcmvivvcq17yx476l4")))

(define-public crate-linux-perf-event-reader-0.4.0 (c (n "linux-perf-event-reader") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0772q911sqps89b6543kz9kdzcnxrldhs6drggyz5xxkrs2bl74b")))

(define-public crate-linux-perf-event-reader-0.5.0 (c (n "linux-perf-event-reader") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "14a2mlmr5spgxzac9vwci7kxaf49gyh9w1yxa2mi8wni1z3p0pdv")))

(define-public crate-linux-perf-event-reader-0.6.0 (c (n "linux-perf-event-reader") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0vzrpcgffj2bvs6al1vb1pvqn4h1fplqiv6l68vkl73gv44krwx3")))

(define-public crate-linux-perf-event-reader-0.7.0 (c (n "linux-perf-event-reader") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "06hrrvkpmw5d3s8z8m9xlp7xkqnq165a7g54hpqgkfmrwzk7p2yh")))

(define-public crate-linux-perf-event-reader-0.8.0 (c (n "linux-perf-event-reader") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13kq6lj0d9gamzk2iajicaksinwy2hvfa3f30c19g3wvwdj03hs1")))

(define-public crate-linux-perf-event-reader-0.8.1 (c (n "linux-perf-event-reader") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0g91lfjjbipsw0azxzgz5wkplcdppxa62667lfxcdc53g41qp60i") (y #t)))

(define-public crate-linux-perf-event-reader-0.9.0 (c (n "linux-perf-event-reader") (v "0.9.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "142sbkan8d8fwaz7s7fs8wwcmis5yv2r4ma6r4wyb8v04jskhfg9")))

(define-public crate-linux-perf-event-reader-0.10.0 (c (n "linux-perf-event-reader") (v "0.10.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "memchr") (r "^2.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "11qnzcplwl03rng625awsh9wapy2vqf4m7njkc1dn07ixhilc1j1")))

