(define-module (crates-io li nu linux-video) #:use-module (crates-io))

(define-public crate-linux-video-0.1.0 (c (n "linux-video") (v "0.1.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "linux-video-core") (r "^0.1.0") (k 0)))) (h "1cpximm938m5j35xv33z8aqy3m2clkmdcw961fc9l7dxr38n3mhq") (f (quote (("thread-safe" "linux-video-core/thread-safe") ("test-vivid") ("serde" "linux-video-core/serde") ("full" "clap" "serde" "drm-fourcc" "dcv-color-primitives") ("drm-fourcc" "linux-video-core/drm-fourcc") ("default") ("dcv-color-primitives" "linux-video-core/dcv-color-primitives") ("clap" "linux-video-core/clap"))))))

(define-public crate-linux-video-0.1.1 (c (n "linux-video") (v "0.1.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "linux-video-core") (r "^0.1.1") (k 0)))) (h "0i4bqfgldhq4gkixrly1pcwmzgyf96dy33kynnm2pm8xjnv97hdw") (f (quote (("thread-safe" "linux-video-core/thread-safe") ("test-vivid") ("serde" "linux-video-core/serde") ("full" "clap" "serde" "drm-fourcc" "dcv-color-primitives") ("drm-fourcc" "linux-video-core/drm-fourcc") ("default") ("dcv-color-primitives" "linux-video-core/dcv-color-primitives") ("clap" "linux-video-core/clap"))))))

