(define-module (crates-io li nu linux-io-uring-sys) #:use-module (crates-io))

(define-public crate-linux-io-uring-sys-0.0.1 (c (n "linux-io-uring-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bc9diql18gbnya2yp4skl0qnay3p2ms9ng8shmiicixmnpc9p4n")))

(define-public crate-linux-io-uring-sys-0.0.2 (c (n "linux-io-uring-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.51") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10bb9bpa5152askcf1k6rz8yld4x6p0iwc1kpnm69f42gvkmkbkr") (f (quote (("overwrite" "bindgen"))))))

(define-public crate-linux-io-uring-sys-0.0.3 (c (n "linux-io-uring-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.51") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ii354amzvi5snxs0i9jq4606pf9gwbwvdb07mkmh4gj95nlbadv") (f (quote (("overwrite" "bindgen"))))))

(define-public crate-linux-io-uring-sys-0.0.4 (c (n "linux-io-uring-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.52") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14x103bqyj97fgs4qyqzp6a4phaa1b7jb0v8fxdbfbfj665vhqmj") (f (quote (("overwrite" "bindgen"))))))

