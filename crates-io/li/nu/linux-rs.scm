(define-module (crates-io li nu linux-rs) #:use-module (crates-io))

(define-public crate-linux-rs-0.0.1 (c (n "linux-rs") (v "0.0.1") (h "1mdpsh20s4q286fk92y375j023ivlkr4yd0fllyz2cyqiqld105p")))

(define-public crate-linux-rs-0.0.2 (c (n "linux-rs") (v "0.0.2") (h "1nfbq73c1as0dpcnlwj78s5nmzl5ljjdzf3z39aa1vs8ff7nzlbw") (f (quote (("CONFIG_TRACE_IRQFLAGS"))))))

