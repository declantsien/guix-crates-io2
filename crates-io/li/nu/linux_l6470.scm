(define-module (crates-io li nu linux_l6470) #:use-module (crates-io))

(define-public crate-linux_l6470-0.1.0 (c (n "linux_l6470") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "spidev") (r "^0.3") (d #t) (k 0)) (d (n "sysfs_gpio") (r "^0.5.3") (d #t) (k 0)))) (h "1w481xillvvc0r83v7hz6cm48ak611pr83bb34c8ih85l4xmlcvn")))

