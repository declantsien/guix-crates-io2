(define-module (crates-io li nu linurgy) #:use-module (crates-io))

(define-public crate-linurgy-0.1.0 (c (n "linurgy") (v "0.1.0") (h "1nk588w8w5sdxr93zg1fiharbs4mvvax0fgnz743x7cmf4415f6j")))

(define-public crate-linurgy-0.1.1 (c (n "linurgy") (v "0.1.1") (h "1f86nvkcnkl88izv4liqvcq94w1aihpn0qfqnk1hg5x0gx6crw3i")))

(define-public crate-linurgy-0.1.2 (c (n "linurgy") (v "0.1.2") (h "05bfggp8dr8zhcyndc21s3p2m49hqxvsngwdav26ra2rhwxwdq5r")))

(define-public crate-linurgy-0.2.1 (c (n "linurgy") (v "0.2.1") (h "0nzl32df7cr6nwr47vz748km1bsxyw20a0mdnjsa912vacx0lf1h")))

(define-public crate-linurgy-0.3.0 (c (n "linurgy") (v "0.3.0") (h "14fzjmv7wwdm0506yvd7bmr1h49mifyfsypwkf9dcnnzlzsdmi3r")))

(define-public crate-linurgy-0.4.0 (c (n "linurgy") (v "0.4.0") (h "1apkqyi20y4clxfs6zac4p49ff8wfjr9d739p82rf4kvjn6cdp3f")))

(define-public crate-linurgy-0.4.1 (c (n "linurgy") (v "0.4.1") (h "1d31z7r7faba5dqf0fl9v6mlafl87w5183p0qldrqqj5as0964w7")))

(define-public crate-linurgy-0.5.0 (c (n "linurgy") (v "0.5.0") (h "0np37s4pxm65920584l99v2jvmf7bv3bllkr57f3mym3w8a4zk7k")))

(define-public crate-linurgy-0.6.0 (c (n "linurgy") (v "0.6.0") (h "1gls7ln7ywq90vbw0wv5h1i2pk508sh0myg6d3zkwlqmjdr5xnnl")))

