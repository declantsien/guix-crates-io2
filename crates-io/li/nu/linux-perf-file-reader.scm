(define-module (crates-io li nu linux-perf-file-reader) #:use-module (crates-io))

(define-public crate-linux-perf-file-reader-0.1.0 (c (n "linux-perf-file-reader") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.8") (d #t) (k 0)) (d (n "env_logger") (r "^0.4") (d #t) (k 2)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "041qjjlfdf9jis9bla5i0sqa27r32y3jqfz2dsw6kacqj983m6na")))

(define-public crate-linux-perf-file-reader-0.2.0 (c (n "linux-perf-file-reader") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "error-chain") (r "^0.12.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1znagb0jq0125r91m2riq9b5py1zd79qgq50hxbg0b5bmv28j9k2")))

