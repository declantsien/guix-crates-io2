(define-module (crates-io li nu linux-api-sys) #:use-module (crates-io))

(define-public crate-linux-api-sys-0.0.0 (c (n "linux-api-sys") (v "0.0.0") (d (list (d (n "linux-api") (r ">= 0.0.3") (d #t) (k 0)))) (h "191dy45k5anpm1nfxnsl6brpmcny875w7jx2l8928h72by8fxmw2")))

(define-public crate-linux-api-sys-0.0.1 (c (n "linux-api-sys") (v "0.0.1") (d (list (d (n "linux-api") (r ">= 0.0.4") (d #t) (k 0)))) (h "0zhlzp82vh23yzw38jnjl166kw6kn567nbnvx62rsbikdz2xbpnf")))

