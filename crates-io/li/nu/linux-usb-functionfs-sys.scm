(define-module (crates-io li nu linux-usb-functionfs-sys) #:use-module (crates-io))

(define-public crate-linux-usb-functionfs-sys-0.1.0 (c (n "linux-usb-functionfs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.1") (d #t) (k 1)))) (h "052ljl4l651s7fc211ns6414lijgbcxm8lggp14xiadnpxq21vzk")))

(define-public crate-linux-usb-functionfs-sys-0.1.1 (c (n "linux-usb-functionfs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0rp9i9ax3871mbzrn9zabgxx0r7qi70lkb5k9d0ry5s6vrbc3pbf")))

