(define-module (crates-io li nu linuxtrack-sys) #:use-module (crates-io))

(define-public crate-linuxtrack-sys-1.0.0 (c (n "linuxtrack-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0w54wkgszskwnv5s4jr63l10jxc6sqm44njqfn8gvxmhhjnr2x0j")))

(define-public crate-linuxtrack-sys-1.0.1 (c (n "linuxtrack-sys") (v "1.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "156wfhsp60n4z930ddyfxwsz9kdmhriy79xkzd8xj6v0515qwydg")))

