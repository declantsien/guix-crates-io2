(define-module (crates-io li nu linux_details) #:use-module (crates-io))

(define-public crate-linux_details-0.1.0 (c (n "linux_details") (v "0.1.0") (d (list (d (n "os_info") (r "^3.4.0") (d #t) (k 0)))) (h "01kb4jlx7bbraa74458jlvxdx70xfqag1phvqrsr73c2lmrzinml")))

(define-public crate-linux_details-0.1.1 (c (n "linux_details") (v "0.1.1") (d (list (d (n "enum-iterator") (r "^1.1.2") (d #t) (k 0)) (d (n "linux_details_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "os_info") (r "^3.4.0") (d #t) (k 0)))) (h "17i2z9lqspv5lap695jvzvynianl0h9fnvr0xdp2rfp9zyp51xxl")))

