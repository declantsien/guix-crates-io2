(define-module (crates-io li nu linux_details_macros) #:use-module (crates-io))

(define-public crate-linux_details_macros-0.1.0 (c (n "linux_details_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0nvcn5dsarvybdcfpgagj89mrgy3vvnrvlxdy4j5q8k4v57kcyis")))

