(define-module (crates-io li nu linux_tuples_client) #:use-module (crates-io))

(define-public crate-linux_tuples_client-0.1.0 (c (n "linux_tuples_client") (v "0.1.0") (h "0y057lnwiv34j18pjpf6k8yjr1hpkk3595raqghzqy76h3sly3hn")))

(define-public crate-linux_tuples_client-0.1.1 (c (n "linux_tuples_client") (v "0.1.1") (h "0v81n1agv4z8ikyjj26p6k0jdvjif3mz54bvlzh2xcmvdfqf3ppd")))

