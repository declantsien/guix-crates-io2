(define-module (crates-io li nu linuxcnc-hal) #:use-module (crates-io))

(define-public crate-linuxcnc-hal-0.1.0 (c (n "linuxcnc-hal") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "linuxcnc-hal-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "0pk6d6vf6i8vw9il67nqnjzzpp2w7w12clvsybjf3wnm06chjay0")))

(define-public crate-linuxcnc-hal-0.1.1 (c (n "linuxcnc-hal") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "linuxcnc-hal-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "1rmr8141ynqf1mkvdlldigd01rkhv42p0h9kzllvyl3cfq7n5a1n")))

(define-public crate-linuxcnc-hal-0.1.2 (c (n "linuxcnc-hal") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "linuxcnc-hal-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.9") (d #t) (k 0)))) (h "19f6996pmkz46af487lisjspvpwbnyinfs1ahr6srkgby7vwqvl7")))

(define-public crate-linuxcnc-hal-0.1.3 (c (n "linuxcnc-hal") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "linuxcnc-hal-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "signal-hook") (r "^0.1.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)))) (h "0vpkl56g9r4552kg0npn2kh8xldcmjig5kk9nq7ma5050sxy4383")))

(define-public crate-linuxcnc-hal-0.2.0 (c (n "linuxcnc-hal") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.66") (d #t) (k 0)) (d (n "linuxcnc-hal-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 2)) (d (n "signal-hook") (r "^0.1.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.10") (d #t) (k 0)))) (h "1c6mpggil00rmd5ixddl2yvw9bjiqq11071xa7yl0j8f2ci87i69")))

(define-public crate-linuxcnc-hal-0.3.0 (c (n "linuxcnc-hal") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "linuxcnc-hal-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rtapi-logger") (r "^0.2.0") (d #t) (k 2)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "0xbfah7pcv008qsbn9dsp7m1qzazj9ly4488zvp3jsy30ja0cwms")))

