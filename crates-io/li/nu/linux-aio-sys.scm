(define-module (crates-io li nu linux-aio-sys) #:use-module (crates-io))

(define-public crate-linux-aio-sys-0.1.0 (c (n "linux-aio-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)))) (h "16hig8i4ffwaybgs8p4w1iki5gkmi1p5rs4b9sym9fswsh618dhx")))

