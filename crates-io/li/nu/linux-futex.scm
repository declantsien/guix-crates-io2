(define-module (crates-io li nu linux-futex) #:use-module (crates-io))

(define-public crate-linux-futex-0.1.0 (c (n "linux-futex") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "0q9dqbkp58b0saqssk6x0r0jv3909rmcz41prs08754iw3mlsywj")))

(define-public crate-linux-futex-0.1.1 (c (n "linux-futex") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "1nm8f9y1p46b3vjwf0bbzxwc6zb6xx4a05r9xzkzh6xf0aa0z5ng")))

(define-public crate-linux-futex-0.1.2 (c (n "linux-futex") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.67") (d #t) (k 0)))) (h "1vb5hwdxhm194hspj1s27gm6cfs4bslflvrhh9pwk0qdlsvgyf59")))

(define-public crate-linux-futex-0.2.0 (c (n "linux-futex") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "17dwy3226ggi9dqjzaryb6v1vlsv0y6z4asr9nmi68yqn9s56bi2")))

