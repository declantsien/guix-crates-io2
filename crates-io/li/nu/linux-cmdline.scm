(define-module (crates-io li nu linux-cmdline) #:use-module (crates-io))

(define-public crate-linux-cmdline-0.1.0 (c (n "linux-cmdline") (v "0.1.0") (h "0zcvdi3hi3xcs9d1zrmya1im7bx5qhywxkl571glmr06br5dq31z")))

(define-public crate-linux-cmdline-0.1.1 (c (n "linux-cmdline") (v "0.1.1") (h "0iw7s1bpc80ka65kx6q1z85iqjnaqfhcg2cikb9sivp2ldnc0pxx")))

(define-public crate-linux-cmdline-0.1.2 (c (n "linux-cmdline") (v "0.1.2") (h "073ijlcwk51c8mkphq86i1p2b3njzc77bzh00r91b006i49rzgki")))

