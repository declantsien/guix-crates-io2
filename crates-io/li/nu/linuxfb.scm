(define-module (crates-io li nu linuxfb) #:use-module (crates-io))

(define-public crate-linuxfb-0.1.0 (c (n "linuxfb") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "1p165kdmz56da8yjhakcgsydrybbklqw3dz42ymh5q798xphidqz")))

(define-public crate-linuxfb-0.1.1 (c (n "linuxfb") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "0ycy33rnnqqkbmfa2hdmja37r7n81iipqkrdx4kq4jffq5gwg5j1")))

(define-public crate-linuxfb-0.1.2 (c (n "linuxfb") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "041gjifighyxcyznr1vjlyjdz8yxav8liqfc5lp524rfab09rsxs")))

(define-public crate-linuxfb-0.2.0 (c (n "linuxfb") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.69") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "0ch7sd6ykjl5yz6g9kkwwly31a3dkbn98j181l7xb16plrb4gdg0")))

(define-public crate-linuxfb-0.2.1 (c (n "linuxfb") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "0s3cfp1clmipn6gxj8q3z4k3zcv6n874xrbwbid3dsh923yf18kv")))

(define-public crate-linuxfb-0.3.0 (c (n "linuxfb") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "0zd57k3zxs1mghrky9wc1y9fbm9dzlrm60czds0qwi0yv1v0gw36")))

(define-public crate-linuxfb-0.3.1 (c (n "linuxfb") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.148") (d #t) (k 0)) (d (n "memmap") (r "^0.7.0") (d #t) (k 0)))) (h "1v0bdmh266a8jsr4a5w1v89mayx5ry9vl85zlcv6pn3d0q9z6lby")))

