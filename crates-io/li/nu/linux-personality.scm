(define-module (crates-io li nu linux-personality) #:use-module (crates-io))

(define-public crate-linux-personality-1.0.0 (c (n "linux-personality") (v "1.0.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14ys7nhbkmw83gnjfckrl6hg3gqydqirp3m9y19xnmbmaxvakh9k")))

