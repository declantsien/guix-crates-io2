(define-module (crates-io li nu linux-kcapi) #:use-module (crates-io))

(define-public crate-linux-kcapi-0.1.0 (c (n "linux-kcapi") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.19") (d #t) (k 0)) (d (n "rand_core") (r "^0.6.1") (f (quote ("alloc"))) (o #t) (k 0)))) (h "0fry6w7cd37rz7yfp43hq5cbayivd3a1z9rzbqsi6xjwp3ghvpw7") (f (quote (("rand_trait" "rand_core") ("default" "rand_trait"))))))

(define-public crate-linux-kcapi-0.1.1 (c (n "linux-kcapi") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "nix") (r "^0.26") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (f (quote ("alloc"))) (o #t) (k 0)))) (h "11q8fqs932gaan7fv8qyi24rdi7dngm6xm16glw6bp1sbna9vxhd") (f (quote (("rand_trait" "rand_core") ("default" "rand_trait"))))))

