(define-module (crates-io li nu linux-macros) #:use-module (crates-io))

(define-public crate-linux-macros-0.0.1 (c (n "linux-macros") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (d #t) (k 0)))) (h "0dvz5vs27ic9v5d11rp6mzqpxh63j322y0kv62k96bl95lwzdal2")))

