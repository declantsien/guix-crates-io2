(define-module (crates-io li nu linux-errno) #:use-module (crates-io))

(define-public crate-linux-errno-1.0.0 (c (n "linux-errno") (v "1.0.0") (d (list (d (n "posix-errno") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "1zgns947hr60lcnf96llfk682w1pf3iq9q7l08gjgf6565qsa3vf") (f (quote (("default" "posix-traits")))) (s 2) (e (quote (("posix-traits" "dep:posix-errno"))))))

(define-public crate-linux-errno-1.0.1 (c (n "linux-errno") (v "1.0.1") (d (list (d (n "posix-errno") (r "^1.0.1") (o #t) (d #t) (k 0)))) (h "08x573dl0cdrk4s52a3x4nzzmv5gpm55s4820y5wycjwb4rwqfjq") (f (quote (("default" "posix-traits")))) (s 2) (e (quote (("posix-traits" "dep:posix-errno"))))))

