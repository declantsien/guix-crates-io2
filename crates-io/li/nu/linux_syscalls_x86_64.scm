(define-module (crates-io li nu linux_syscalls_x86_64) #:use-module (crates-io))

(define-public crate-linux_syscalls_x86_64-0.1.0 (c (n "linux_syscalls_x86_64") (v "0.1.0") (h "0xv3n8il47kw619skk0frp4mgizb68zjizi1kqhdsv3xyz11cgz0") (y #t)))

(define-public crate-linux_syscalls_x86_64-0.0.1 (c (n "linux_syscalls_x86_64") (v "0.0.1") (h "0yyxw87fqrf00d8fsr9sxngv5apfsl8z24w33xb5jx024694qhdr") (y #t)))

