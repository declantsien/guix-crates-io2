(define-module (crates-io li nu linux-kvm) #:use-module (crates-io))

(define-public crate-linux-kvm-0.1.0 (c (n "linux-kvm") (v "0.1.0") (d (list (d (n "linux-io") (r "^0.5.0") (d #t) (k 0)) (d (n "linux-unsafe") (r "^0.8.0") (d #t) (k 0)))) (h "0v0hywvv2nmp348gagspqkw6g3cmdgsn9qs4s47mac69gzzwg7bf")))

(define-public crate-linux-kvm-0.2.0 (c (n "linux-kvm") (v "0.2.0") (d (list (d (n "linux-io") (r "^0.6.0") (d #t) (k 0)) (d (n "linux-unsafe") (r "^0.9.0") (d #t) (k 0)))) (h "0xl8x8scqnns7npfymag6xijxma46dnmj3ybzvr6f8acbl5xpzf8")))

