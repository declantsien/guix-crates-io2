(define-module (crates-io li nu linuxvideo) #:use-module (crates-io))

(define-public crate-linuxvideo-0.0.0 (c (n "linuxvideo") (v "0.0.0") (h "12fva7n8hlda39qlfrgcgpp91kmyks5lhmjd454klg15fsckyyz1")))

(define-public crate-linuxvideo-0.1.0 (c (n "linuxvideo") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "1ym5fdfbf7bv8iym7md39l2rb5xicar07nj2yg1zw3dc6kkf8fbq")))

(define-public crate-linuxvideo-0.1.1 (c (n "linuxvideo") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.25.0") (d #t) (k 0)))) (h "11wg5fsrlv1x4nz176cig221lj5c2zppbqlyd7vssrx0ipfl9anl")))

(define-public crate-linuxvideo-0.2.0 (c (n "linuxvideo") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)))) (h "1ykbrhkw41p5mmq3ziqnjgv9i56gjjf5scz7d28i34ib2q8l8s4m")))

(define-public crate-linuxvideo-0.3.0 (c (n "linuxvideo") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)))) (h "1rdnswh0ilrbk0rhcx0gzyb54slya0mnxdrrr0dhf1izikx2a562")))

(define-public crate-linuxvideo-0.3.1 (c (n "linuxvideo") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)))) (h "0fcx21zwmwxm9gfb9rilxwz2hs8bsjsrc2h9cpkm2vvig5dnxwn8")))

(define-public crate-linuxvideo-0.3.2 (c (n "linuxvideo") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)))) (h "1312nwadl9hzw1gxdlka673cdhza5ndvkm355m609ranc0jp3dky")))

(define-public crate-linuxvideo-0.3.3 (c (n "linuxvideo") (v "0.3.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (k 2)) (d (n "itertools") (r "^0.10.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "nix") (r "^0.26.1") (d #t) (k 0)))) (h "0j2b45azdd6ln0dxpfzys302isc3amiv44m4knpqw3djx8bzqyp9")))

