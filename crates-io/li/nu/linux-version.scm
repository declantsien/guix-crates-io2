(define-module (crates-io li nu linux-version) #:use-module (crates-io))

(define-public crate-linux-version-0.1.0 (c (n "linux-version") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "1la4waqbspii5b5shqp4fn7c9yxr8ng3p8qgmjr73x0w2z46cnf8")))

(define-public crate-linux-version-0.1.1 (c (n "linux-version") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51") (k 1)) (d (n "nix") (r "^0.17") (d #t) (k 0)) (d (n "semver") (r "^0.9") (d #t) (k 0)))) (h "0adfygybmg7zgcclri0m8p9w8xyvhybfcfi9jdfi9brskphrw3jm")))

