(define-module (crates-io li nu linux-ipc) #:use-module (crates-io))

(define-public crate-linux-ipc-0.1.0 (c (n "linux-ipc") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (d #t) (k 0)))) (h "1knmj5j22w5l8iavq1cxljh0lkgad8vsd89knqjp71fil1m1c0lx")))

