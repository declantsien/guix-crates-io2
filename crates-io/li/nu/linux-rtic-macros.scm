(define-module (crates-io li nu linux-rtic-macros) #:use-module (crates-io))

(define-public crate-linux-rtic-macros-0.1.0 (c (n "linux-rtic-macros") (v "0.1.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-alpha.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "07q7yapin6hpk7azm1lp49ja72c8if3xwzvlqwvysswpa07d8qni")))

(define-public crate-linux-rtic-macros-0.1.1 (c (n "linux-rtic-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rtic-syntax") (r "^0.5.0-alpha.4") (d #t) (k 0)) (d (n "syn") (r "^1") (d #t) (k 0)))) (h "1w398awyf0v54jsmsqyif89i3ram47xj9wxq1wv52ylgw532k3k6")))

