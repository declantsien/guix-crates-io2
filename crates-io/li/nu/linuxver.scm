(define-module (crates-io li nu linuxver) #:use-module (crates-io))

(define-public crate-linuxver-0.1.0 (c (n "linuxver") (v "0.1.0") (d (list (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "semver") (r "^0.4.0") (d #t) (k 0)))) (h "1aznipff07y65d0ci01br1m6k9081nxvxjd6m1xf49rrgr3ys6rp")))

