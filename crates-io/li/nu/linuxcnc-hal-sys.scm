(define-module (crates-io li nu linuxcnc-hal-sys) #:use-module (crates-io))

(define-public crate-linuxcnc-hal-sys-0.1.0 (c (n "linuxcnc-hal-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "0n23dsk5zck0vp8413ziz8qy4cb0xp7pix52qv85n4q7ak94gd1a")))

(define-public crate-linuxcnc-hal-sys-0.1.1 (c (n "linuxcnc-hal-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "14vcg30srl47irnqi3z8l2i0j0cj5kc5cwyiziprhrrdq5yc7n98")))

(define-public crate-linuxcnc-hal-sys-0.1.2 (c (n "linuxcnc-hal-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "01sh5p92c44bnia40cmqcj2v7qah2a4awdaiqbmv0gv5vfg7jbr3")))

(define-public crate-linuxcnc-hal-sys-0.1.3 (c (n "linuxcnc-hal-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1kfg8iabh86sm9z18wzdd6vxnnwa7za4fxamqwfgd2r7b6i0y1hm")))

(define-public crate-linuxcnc-hal-sys-0.1.4 (c (n "linuxcnc-hal-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)))) (h "1jpbijqlvgi19c04hzgwia0ywl14sqv9v4x6h87qw1jk25aw8nqw") (l "linuxcnchal")))

(define-public crate-linuxcnc-hal-sys-0.1.5 (c (n "linuxcnc-hal-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)))) (h "0w0786z4b59p80l618pk5cridfgb1nigfz028kd60zjyggp62dnk")))

(define-public crate-linuxcnc-hal-sys-0.1.6 (c (n "linuxcnc-hal-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "signal-hook") (r "^0.1.13") (d #t) (k 2)))) (h "1ndaf3kgka382pk5zn62bf6sqqm6x87szh762zgd8rx2m55iw503")))

(define-public crate-linuxcnc-hal-sys-0.1.7 (c (n "linuxcnc-hal-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "signal-hook") (r "^0.1.13") (d #t) (k 2)))) (h "1hs84r5w2gfpy1n2dcmsgpbs37wnb0n5nd0hn1v3m7aj1nfslh3a")))

(define-public crate-linuxcnc-hal-sys-0.2.0 (c (n "linuxcnc-hal-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.50") (d #t) (k 1)) (d (n "signal-hook") (r "^0.1.13") (d #t) (k 2)))) (h "0n1fyi8dli5mri9mbyrn1yp4c5qkdxj765gdb2v5wrj81mi3ly33")))

(define-public crate-linuxcnc-hal-sys-0.3.0 (c (n "linuxcnc-hal-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.14") (d #t) (k 2)))) (h "1dm6gzwqv08j5lc8l08y3qbw6iif1dq9rr1kx7a1cajm56r6apgw")))

