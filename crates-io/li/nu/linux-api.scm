(define-module (crates-io li nu linux-api) #:use-module (crates-io))

(define-public crate-linux-api-0.0.1 (c (n "linux-api") (v "0.0.1") (h "1h1ki9smql9ws1izpddwn0427x4gmi9ar44bzc7rn3rw5n4m38q6")))

(define-public crate-linux-api-0.0.2 (c (n "linux-api") (v "0.0.2") (d (list (d (n "time-sys") (r "*") (d #t) (k 2)))) (h "0g6zan1rwiqjhkj2hn4i9ini4pzhx651dnjlpijh3sysi23h5lms")))

(define-public crate-linux-api-0.0.3 (c (n "linux-api") (v "0.0.3") (d (list (d (n "time-sys") (r "*") (d #t) (k 2)))) (h "1rxp40mmz6w6v16q3zd51qwcm9yaii8j8nlg81dhv575zimbys04")))

(define-public crate-linux-api-0.0.4 (c (n "linux-api") (v "0.0.4") (d (list (d (n "linux-api-exit") (r "*") (d #t) (k 2)) (d (n "linux-api-sys") (r "*") (d #t) (k 2)) (d (n "time-sys") (r "*") (d #t) (k 2)))) (h "0fqp62hk3wnj10l55q9b35v6ks5qpkpf979f2ndpkmzg2ky3cd87")))

(define-public crate-linux-api-0.0.5 (c (n "linux-api") (v "0.0.5") (d (list (d (n "linux-api-exit") (r "*") (d #t) (k 2)) (d (n "linux-api-sys") (r "*") (d #t) (k 2)) (d (n "time-sys") (r "*") (d #t) (k 2)))) (h "1k9j9v1yd49zysbdn33l5c528kh0327958znrl6p6nm5im93a4bz")))

