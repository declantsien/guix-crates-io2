(define-module (crates-io li nu linux-sys) #:use-module (crates-io))

(define-public crate-linux-sys-0.0.0 (c (n "linux-sys") (v "0.0.0") (h "078anicqgjwaj9gxzs5w8kkdm52p1yvv41b2sgzqiymhchdynd5b")))

(define-public crate-linux-sys-0.0.1-pre+5.4.0 (c (n "linux-sys") (v "0.0.1-pre+5.4.0") (d (list (d (n "bindgen") (r "^0.52") (f (quote ("which-rustfmt"))) (k 1)) (d (n "cty") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1rw5gbxsr681l8y85064gw1vsry04zkpkyli8prl75aa1f3p4746")))

