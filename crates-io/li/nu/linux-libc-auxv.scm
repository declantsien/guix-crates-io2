(define-module (crates-io li nu linux-libc-auxv) #:use-module (crates-io))

(define-public crate-linux-libc-auxv-0.1.0 (c (n "linux-libc-auxv") (v "0.1.0") (d (list (d (n "enum-iterator") (r "^0.7") (d #t) (k 0)))) (h "0gp1pkl7zcyk0s5bc5xrcqclqnbfs3l884j89qq5xzngyn8pnrfr")))

(define-public crate-linux-libc-auxv-0.2.0 (c (n "linux-libc-auxv") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "enum-iterator") (r "^0.7") (d #t) (k 0)))) (h "1fwflxhrfwzagsgqd3xjgmjy2q3grlpn7g3kkr119rwcpsy531z0")))

(define-public crate-linux-libc-auxv-0.2.1 (c (n "linux-libc-auxv") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "enum-iterator") (r "^0.7") (d #t) (k 0)))) (h "0kz5194jn68pkhhy8hcldxcmv5s3x4m1splpyqifrrcaqd557v21")))

