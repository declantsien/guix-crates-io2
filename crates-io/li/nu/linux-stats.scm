(define-module (crates-io li nu linux-stats) #:use-module (crates-io))

(define-public crate-linux-stats-0.1.0 (c (n "linux-stats") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1yhdjh67vhvc8c4678i8a2kn2nk74f301gql1pk398a47fa4hn1k")))

(define-public crate-linux-stats-0.2.0 (c (n "linux-stats") (v "0.2.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1h1h417rq310r34ngva3k0c5jlvqc9krsq26zsn6v162ibcxcjm6")))

(define-public crate-linux-stats-0.3.0 (c (n "linux-stats") (v "0.3.0") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1zilsrhjpnhh60l9ab1aaqs3izpjmfbarvfinqjzk1yy5nyswzp1")))

(define-public crate-linux-stats-0.3.1 (c (n "linux-stats") (v "0.3.1") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "num") (r "^0.1.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0zn56isnsldrw5w84kkyqix0w7w4qd6hrj7r6bd5bvnw8vmkwzxh")))

(define-public crate-linux-stats-0.3.2 (c (n "linux-stats") (v "0.3.2") (d (list (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "1m28pjrkv4mnrmibrq54bglnsf73b6501cam0gris8l1yr8i0f2m")))

