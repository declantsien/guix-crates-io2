(define-module (crates-io li nu linux-sed-opal-sys) #:use-module (crates-io))

(define-public crate-linux-sed-opal-sys-0.1.0 (c (n "linux-sed-opal-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65") (f (quote ("which-rustfmt"))) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "1wi4zicdvyxhf1pp8f01g5bvdv9rcsnypjwfiylln4lc13j5xi4k") (f (quote (("linux_6_6" "linux_6_4") ("linux_6_4" "linux_6_1") ("linux_6_1")))) (y #t)))

(define-public crate-linux-sed-opal-sys-0.1.1 (c (n "linux-sed-opal-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65") (f (quote ("which-rustfmt"))) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "nix") (r "^0.27") (f (quote ("ioctl"))) (d #t) (k 0)))) (h "0rv715l65f98ac7sjg3l28cpajv1047fj82hd0pvza8m039kvf6c") (f (quote (("linux_6_6" "linux_6_4") ("linux_6_4" "linux_6_1") ("linux_6_1"))))))

