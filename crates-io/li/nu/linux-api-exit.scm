(define-module (crates-io li nu linux-api-exit) #:use-module (crates-io))

(define-public crate-linux-api-exit-0.0.0 (c (n "linux-api-exit") (v "0.0.0") (d (list (d (n "linux-api") (r ">= 0.0.3") (d #t) (k 0)))) (h "1avnnvgahjxiz1iq1w3pnk0fpji8c197qx7i94hirf4z0cy14zi1")))

(define-public crate-linux-api-exit-0.1.0 (c (n "linux-api-exit") (v "0.1.0") (d (list (d (n "linux-api") (r ">= 0.0.4") (d #t) (k 0)))) (h "04xh2bc5yapcdqnwp69wqdwcwjx0x86q85c61qx8l3ldn7d58d45")))

(define-public crate-linux-api-exit-0.1.1 (c (n "linux-api-exit") (v "0.1.1") (d (list (d (n "linux-api") (r ">= 0.0.4") (d #t) (k 0)))) (h "12ks3bqyvfpqm4p4vjs6ry4xlkid67nnss57drxqkk7mm5jvv6n1")))

