(define-module (crates-io li nu linux-loader) #:use-module (crates-io))

(define-public crate-linux-loader-0.1.0 (c (n "linux-loader") (v "0.1.0") (h "08gb2pfx2s8ic3b70k1w37wr82s871gsjrsqpkh068sz159lr1gx")))

(define-public crate-linux-loader-0.2.0 (c (n "linux-loader") (v "0.2.0") (d (list (d (n "criterion") (r "=0.3.0") (d #t) (k 2)) (d (n "vm-memory") (r ">=0.2.0") (d #t) (k 0)))) (h "1vcl33z9781vlps2lic134b79wrp0a2rkrmp7qr6b3xbjsn55rza") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage")))) (y #t)))

(define-public crate-linux-loader-0.3.0 (c (n "linux-loader") (v "0.3.0") (d (list (d (n "criterion") (r "=0.3.0") (d #t) (k 2)) (d (n "vm-memory") (r ">=0.2.0") (d #t) (k 0)) (d (n "vm-memory") (r ">=0.2.0") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "0nbl8bijx0c6g9gnljiq68lxhicb5355bi7fkznw3wmhfn1cq6f8") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage"))))))

(define-public crate-linux-loader-0.4.0 (c (n "linux-loader") (v "0.4.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "vm-memory") (r ">=0.6.0") (d #t) (k 0)) (d (n "vm-memory") (r ">=0.6.0") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "027sabz5dpyzy15h8id8dvil54vbx9j1lc3asmmklh08714pfpla") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage")))) (y #t)))

(define-public crate-linux-loader-0.5.0 (c (n "linux-loader") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "vm-memory") (r "^0.9.0") (d #t) (k 0)) (d (n "vm-memory") (r "^0.9.0") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "095av5kxx3sfrdzip4xsr7dkci5f225cc6wmmdi9dglrrhjszzl5") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage")))) (y #t)))

(define-public crate-linux-loader-0.6.0 (c (n "linux-loader") (v "0.6.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "vm-memory") (r "^0.9.0") (d #t) (k 0)) (d (n "vm-memory") (r "^0.9.0") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "0s71bwqz727aifz0wi0fqjm8vacy0n8a6hqa9g1lw0yavq9gk8k2") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage"))))))

(define-public crate-linux-loader-0.7.0 (c (n "linux-loader") (v "0.7.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "vm-memory") (r "^0.9.0") (d #t) (k 0)) (d (n "vm-memory") (r "^0.9.0") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "1zmx2qy93gf9iqi4pr4p1q5q5s8mp2acjhw0l89pg6npqxh2697q") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage"))))))

(define-public crate-linux-loader-0.8.0 (c (n "linux-loader") (v "0.8.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "vm-memory") (r "^0.10.0") (d #t) (k 0)) (d (n "vm-memory") (r "^0.10.0") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "1jw1gl358s9m4wnrqpisl4pgppdqss9f9vc8qbksh0q084mqhc0w") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage"))))))

(define-public crate-linux-loader-0.8.1 (c (n "linux-loader") (v "0.8.1") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "vm-memory") (r "^0.10.0") (d #t) (k 0)) (d (n "vm-memory") (r "^0.10.0") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "0gz8bpcv7wlz001g918xrwl05pad02807imvyqccjb5mzgdrs9dr") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage"))))))

(define-public crate-linux-loader-0.9.0 (c (n "linux-loader") (v "0.9.0") (d (list (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "vm-memory") (r "^0.11.0") (d #t) (k 0)) (d (n "vm-memory") (r "^0.11.0") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "021fyx7dimyj9g3v1m690w4mxw8bvqkll6ivr8g792g151xxnfld") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage"))))))

(define-public crate-linux-loader-0.9.1 (c (n "linux-loader") (v "0.9.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "vm-memory") (r "^0.12.2") (d #t) (k 0)) (d (n "vm-memory") (r "^0.12.0") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "1kzxxxr4zj6kg4rnaahs8yhma3k03gpdfgm97zw72280r0jsgdhx") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage"))))))

(define-public crate-linux-loader-0.10.0 (c (n "linux-loader") (v "0.10.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "vm-memory") (r "^0.13.1") (d #t) (k 0)) (d (n "vm-memory") (r "^0.13.1") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "0d6bm76v9ligg1h5zgl7y5bpgkiciwpwg0mn580n98dkhldm6ahk") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage"))))))

(define-public crate-linux-loader-0.11.0 (c (n "linux-loader") (v "0.11.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "vm-memory") (r "^0.14.0") (d #t) (k 0)) (d (n "vm-memory") (r "^0.14.0") (f (quote ("backend-mmap"))) (d #t) (k 2)))) (h "10vs4s4myz5cb57r5n74hdv7f37zki897r8amzpqsnpja8sdss7b") (f (quote (("pe") ("elf") ("default" "elf" "pe") ("bzimage"))))))

