(define-module (crates-io li nu linux-file-info) #:use-module (crates-io))

(define-public crate-linux-file-info-0.1.0 (c (n "linux-file-info") (v "0.1.0") (h "17711gzkpkvq5m5y9qdl1isqr1qadmi425y6w153z9lv6yzyawsv")))

(define-public crate-linux-file-info-0.1.1 (c (n "linux-file-info") (v "0.1.1") (h "1vxm1hyh7n7q814bq0saxhya41ricb5rnarv6jczkbw03x803ggc")))

(define-public crate-linux-file-info-0.2.0 (c (n "linux-file-info") (v "0.2.0") (h "12r6z5grkviadr2xlw8ga16ax76ak2lf4zv31xbxpk99jglmz28y")))

(define-public crate-linux-file-info-0.3.0 (c (n "linux-file-info") (v "0.3.0") (h "1a5ap3qyynmn8cz30dbv0crvd32zfd3xvvzgnk3nanb3bf3xywms")))

