(define-module (crates-io li fe lifecycle_msgs) #:use-module (crates-io))

(define-public crate-lifecycle_msgs-1.2.1 (c (n "lifecycle_msgs") (v "1.2.1") (d (list (d (n "rosidl_runtime_rs") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde-big-array") (r "^0.5.1") (o #t) (d #t) (k 0)))) (h "12rn9r3h4knwhn2ayig7zyspr1xg0zb95rdyqmd6r8sd2ia1pn4a") (y #t) (s 2) (e (quote (("serde" "dep:serde" "dep:serde-big-array" "rosidl_runtime_rs/serde"))))))

