(define-module (crates-io li fe lifers-ascii) #:use-module (crates-io))

(define-public crate-lifers-ascii-0.1.0 (c (n "lifers-ascii") (v "0.1.0") (d (list (d (n "lifers") (r "^0.1.0") (d #t) (k 0)))) (h "0w7zijw0dvm2xc2br3kn0ggmyz5hfbxcl16s2qlw19qj4pzk51dj")))

(define-public crate-lifers-ascii-0.1.1 (c (n "lifers-ascii") (v "0.1.1") (d (list (d (n "lifers") (r "^0.2.0") (d #t) (k 0)))) (h "1ngf6f1vkhjgvq8mk9qgc1qh5lf49kwrkz42kdry6zf5kwzvkc42")))

