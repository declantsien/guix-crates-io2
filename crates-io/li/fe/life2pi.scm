(define-module (crates-io li fe life2pi) #:use-module (crates-io))

(define-public crate-life2pi-0.1.0 (c (n "life2pi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1xij0pgwasasny76yg470apy2axjp5b7bq04mjpm8lmir3v8yswr")))

(define-public crate-life2pi-0.1.1 (c (n "life2pi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.159") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1.27.0") (f (quote ("full"))) (d #t) (k 0)))) (h "15i9hkcvdq0r7qmajyfz54jn8krplqkhvavswsm4wvmxk085f40c")))

