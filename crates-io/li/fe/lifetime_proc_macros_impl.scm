(define-module (crates-io li fe lifetime_proc_macros_impl) #:use-module (crates-io))

(define-public crate-lifetime_proc_macros_impl-0.1.0-test1 (c (n "lifetime_proc_macros_impl") (v "0.1.0-test1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "1m8cmqmsd9n1fbqzxl58qwwgsfpsr89ydbqd6nni8a7d50ql1y07")))

(define-public crate-lifetime_proc_macros_impl-0.1.0 (c (n "lifetime_proc_macros_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("visit"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 2)))) (h "0jsic0y7y0iwxk1029q9yg9lhgd04msrc5y0cj4j9xjmilrpb6qx")))

