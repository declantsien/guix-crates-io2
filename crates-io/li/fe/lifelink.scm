(define-module (crates-io li fe lifelink) #:use-module (crates-io))

(define-public crate-lifelink-0.1.0 (c (n "lifelink") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "03pqr5jl3rw6cdxg35rqrk5vmbgcfg5qv5dcvj8b79l96vhli9fg") (f (quote (("nightly"))))))

(define-public crate-lifelink-0.1.1 (c (n "lifelink") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "0jjgxyj8d0wsg2dfqwwqvgvkydnscv0wvbvcf1kk6ib1k0d9jsys") (f (quote (("nightly"))))))

(define-public crate-lifelink-0.2.0 (c (n "lifelink") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)))) (h "1zj2l6mxnn1az98qpwq2xjan402qxkwy5m6cqr60rqmwxnxvl70k") (f (quote (("nightly"))))))

