(define-module (crates-io li fe lifeguard) #:use-module (crates-io))

(define-public crate-lifeguard-0.0.1 (c (n "lifeguard") (v "0.0.1") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1vyd5lcijrmnwllypknsg2m3jv8spp8y49i467lpiiqhdj8yw5sa")))

(define-public crate-lifeguard-0.0.2 (c (n "lifeguard") (v "0.0.2") (h "06xbcrsr325cabyl38shh1amm84zr8w1jl4bahqh65xha6pd89bj")))

(define-public crate-lifeguard-0.0.3 (c (n "lifeguard") (v "0.0.3") (h "043124n9avrifhm4l1h2b47x0q87ycyz3v6yn4wnn5kxhy0ivixb")))

(define-public crate-lifeguard-0.1.0 (c (n "lifeguard") (v "0.1.0") (h "1m8jhsvi4g26diikqfr9xdhkgikrr31vn6n6xm1l4jksg3bksfv0")))

(define-public crate-lifeguard-0.2.0 (c (n "lifeguard") (v "0.2.0") (h "1knc2pl3flw6hrwmzklm92cxj0jm949iqc4zrznj3kvm7jl6bpxi")))

(define-public crate-lifeguard-0.3.0 (c (n "lifeguard") (v "0.3.0") (h "0d9ihmlj424wmd5dl65wmj5zxz5i6k01b95j6db3a3jpbzc8mnys")))

(define-public crate-lifeguard-0.4.0 (c (n "lifeguard") (v "0.4.0") (h "1vrqr2r56c9l7a12w8bg7pic909vqpw3pschm64p08frm4b85a4b")))

(define-public crate-lifeguard-0.5.0 (c (n "lifeguard") (v "0.5.0") (h "19b4w21pzlxdbp2llm1j569idk3m39j26sszq1p3f8zx63i47a98")))

(define-public crate-lifeguard-0.5.1 (c (n "lifeguard") (v "0.5.1") (h "1sddn50z8qbgdc2vk792hq9gf8vc8irwimkvafh9p35751dzv54y")))

(define-public crate-lifeguard-0.5.2 (c (n "lifeguard") (v "0.5.2") (h "1lr11r81hsyl8al22y8ccniljsshjn59jrgb5nqh6bjdjv4chihi")))

(define-public crate-lifeguard-0.6.0 (c (n "lifeguard") (v "0.6.0") (h "0ps0xi4h0l8ixfdk5mnms4a12r7wc73qd8klgdrhi6hzsl9hbq7f")))

(define-public crate-lifeguard-0.6.1 (c (n "lifeguard") (v "0.6.1") (h "09iwwy7888i1kfvbrgwpl4xv3pwsz1fbzx54djs3gnvmszdr9gl9")))

