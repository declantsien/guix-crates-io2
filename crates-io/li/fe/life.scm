(define-module (crates-io li fe life) #:use-module (crates-io))

(define-public crate-life-0.1.0 (c (n "life") (v "0.1.0") (d (list (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "1np3g71rvxsz625cl2fp509dkz80j4ipjy7kkgi19brl37mlqjzb")))

(define-public crate-life-0.1.2 (c (n "life") (v "0.1.2") (d (list (d (n "term") (r "^0.2") (d #t) (k 0)))) (h "0ic3yk9slx5ivs6l9hra1vab5ccj4p9d18w19258kx3sdfbpfh2z")))

(define-public crate-life-0.1.3 (c (n "life") (v "0.1.3") (d (list (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1kz345gfihk1l32agvmh5x1as3n1n67xjm4dlhxz30i36n6vhk4c")))

(define-public crate-life-0.1.4 (c (n "life") (v "0.1.4") (d (list (d (n "term") (r "^0.4") (d #t) (k 0)))) (h "1j9sdm2kifyq6c26ljig9hzvd90qla2zw2hbzxws779cf4nv4grk")))

(define-public crate-life-0.1.5 (c (n "life") (v "0.1.5") (d (list (d (n "term") (r "^0.5") (d #t) (k 0)))) (h "17h4mnnkxnyvaz0s3wpzar73ppc5qq6yjj771va13y115jwgl027")))

