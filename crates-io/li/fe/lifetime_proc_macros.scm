(define-module (crates-io li fe lifetime_proc_macros) #:use-module (crates-io))

(define-public crate-lifetime_proc_macros-0.1.0-test1 (c (n "lifetime_proc_macros") (v "0.1.0-test1") (d (list (d (n "lifetime_proc_macros_impl") (r "=0.1.0-test1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y0yg0fjgmx12rhpf0193hqi385sld0ygby22fahsk4d3qpzyf7z")))

(define-public crate-lifetime_proc_macros-0.1.0 (c (n "lifetime_proc_macros") (v "0.1.0") (d (list (d (n "lifetime_proc_macros_impl") (r "=0.1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02pk7n47hw7b83di6f8fpnax88m4cfch3j847s9sxapiznzhhc6b")))

