(define-module (crates-io li fe lifelog) #:use-module (crates-io))

(define-public crate-lifelog-0.1.0 (c (n "lifelog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (d #t) (k 0)) (d (n "cursive") (r "^0.19.0") (f (quote ("crossterm-backend" "toml"))) (k 0)) (d (n "cursive_calendar_view") (r "^0.8.0") (d #t) (k 0)) (d (n "etcetera") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)))) (h "018z9wiapwnwia9w1g9v5ny5yg0fl899d8f1wmfgzanws8yq4j9g")))

