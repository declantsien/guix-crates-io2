(define-module (crates-io li fe life-backend) #:use-module (crates-io))

(define-public crate-life-backend-0.0.0 (c (n "life-backend") (v "0.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "num-iter") (r "^0.1.43") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0nl3vrac025czpv8gzyscg3nv9jm7sq5986a90h0sm25kb5ck6sl") (r "1.65.0")))

(define-public crate-life-backend-0.0.1 (c (n "life-backend") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "num-iter") (r "^0.1.43") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "1ikz3sp8xlc3zwflmg5bxgb57bff4v9z7x1yjfsb2v3y7jv7y8ys") (r "1.65.0")))

(define-public crate-life-backend-0.0.2 (c (n "life-backend") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.8") (f (quote ("derive"))) (d #t) (k 2)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "num-iter") (r "^0.1.43") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "03cg07anj3qci08rrhfm7kbxvkf6zdm0nwvzgnvxy7nlq6nwsm51") (r "1.65.0")))

