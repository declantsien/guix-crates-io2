(define-module (crates-io li fe lifetime) #:use-module (crates-io))

(define-public crate-lifetime-0.1.0-beta1 (c (n "lifetime") (v "0.1.0-beta1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)))) (h "0476mjfh2r98g4w09p6d7kasksj7ys96ybfhhnhvsllkg4a1qr01") (f (quote (("unstable"))))))

(define-public crate-lifetime-0.1.0-test1 (c (n "lifetime") (v "0.1.0-test1") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "lifetime_proc_macros") (r "=0.1.0-test1") (o #t) (d #t) (k 0)))) (h "0n8hrqgnc80ck17rlc9i9cp4r70sy0mfdnivz7rzxxr7gmi4dhgm") (f (quote (("unstable") ("macros" "lifetime_proc_macros") ("default" "alloc") ("alloc"))))))

(define-public crate-lifetime-0.1.0 (c (n "lifetime") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5") (d #t) (k 2)) (d (n "lifetime_proc_macros") (r "=0.1.0") (o #t) (d #t) (k 0)))) (h "13llpailgypnq85mg14cxyv3bnwv9nw8x3r0zj8w198k6jdw76hb") (f (quote (("unstable") ("macros" "lifetime_proc_macros") ("default" "alloc") ("alloc"))))))

