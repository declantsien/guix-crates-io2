(define-module (crates-io li fe lifering) #:use-module (crates-io))

(define-public crate-lifering-0.1.0 (c (n "lifering") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "056kcmgpn005kizyqm60syycskss9yc7yl21s0yafw0di2mkni4l")))

(define-public crate-lifering-0.1.1 (c (n "lifering") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0rdzz0ssr9k0yd8bzryayc5afvhakc6a30z4y0m8hw8nc6nracvh")))

(define-public crate-lifering-0.2.0 (c (n "lifering") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0r8944zp6qba59x13ircisxpn9p144d79yrxh0mkn7x9vc3plb03") (y #t)))

(define-public crate-lifering-0.3.0 (c (n "lifering") (v "0.3.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "0k9xsikfcx4bc0287038vxcnnqhbjp29zxcw62bgikgsjls7pd63")))

(define-public crate-lifering-0.4.0 (c (n "lifering") (v "0.4.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)))) (h "07k2kgdxc6jpriqc2gblsv9i4406jhr884s3k232wqpq84ynvgq5")))

