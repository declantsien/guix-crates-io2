(define-module (crates-io li fe lifetime-tracer) #:use-module (crates-io))

(define-public crate-lifetime-tracer-0.1.0 (c (n "lifetime-tracer") (v "0.1.0") (d (list (d (n "tracing") (r "^0.1") (d #t) (k 0)) (d (n "tracing-futures") (r "^0.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 0)))) (h "19fh2p5h75ga6drl7xlghbzmdbm8fbydzay662q3vsf4gz9ivx8j")))

