(define-module (crates-io li fe lifetime-extender) #:use-module (crates-io))

(define-public crate-lifetime-extender-0.1.0 (c (n "lifetime-extender") (v "0.1.0") (h "0mm5xn767xyfps71wh5i1wkcqgdyy83n18lbbcw0qb7l6iyrqqsz") (r "1.72")))

(define-public crate-lifetime-extender-0.1.1 (c (n "lifetime-extender") (v "0.1.1") (h "10acimnjvgxm3cmd1v8w8cw3j49lbps045d1yzpk04m5i6vrhgis") (f (quote (("test-fake-static-borrow-mut") ("test-fake-static-borrow") ("test-borrow-mut") ("test-borrow") ("i-can-do-whatever-the-fxxk-i-want" "test-borrow" "test-borrow-mut" "test-fake-static-borrow" "test-fake-static-borrow-mut")))) (r "1.72")))

