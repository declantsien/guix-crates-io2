(define-module (crates-io li fe lifetime-thread) #:use-module (crates-io))

(define-public crate-lifetime-thread-0.1.0 (c (n "lifetime-thread") (v "0.1.0") (d (list (d (n "crossbeam-epoch") (r "^0.8") (d #t) (k 0)))) (h "1zy89qy2m28k8hazzb56jxc26lkpl5j4c6xrryfplhac23q14dp6") (y #t)))

(define-public crate-lifetime-thread-0.1.1 (c (n "lifetime-thread") (v "0.1.1") (d (list (d (n "crossbeam-epoch") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "185y943xqvs75c1rlfqimvvnbx79cwn1vzamv916jakxh1ngzbvc") (y #t)))

(define-public crate-lifetime-thread-0.2.0 (c (n "lifetime-thread") (v "0.2.0") (d (list (d (n "async-std") (r "^1.6") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "11pnb9cb5wjr7vrlq0ijk339p5c1193qci7c94r7vh4x5ak2v4s3") (y #t)))

(define-public crate-lifetime-thread-0.2.1 (c (n "lifetime-thread") (v "0.2.1") (d (list (d (n "async-std") (r "^1.6") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1sx698mkf1yw9m1sgvd0rgrg36cj50dv2gb1qn0iyxxa5glvh121") (y #t)))

(define-public crate-lifetime-thread-0.2.2 (c (n "lifetime-thread") (v "0.2.2") (d (list (d (n "async-std") (r "^1.6") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1xcbg40qw3s9c6p4pif61v5qm1sjdrkhkc93930f9m1krkya85i6")))

(define-public crate-lifetime-thread-0.2.3 (c (n "lifetime-thread") (v "0.2.3") (d (list (d (n "async-std") (r "^1.6") (f (quote ("unstable" "attributes"))) (d #t) (k 0)) (d (n "crossbeam-epoch") (r "^0.8") (d #t) (k 0)))) (h "0p3wl2d4vq0j5qzj9v1i6abx848mymz6iqnqkw35b48znl51i752")))

(define-public crate-lifetime-thread-0.3.0 (c (n "lifetime-thread") (v "0.3.0") (d (list (d (n "crossbeam-epoch") (r "^0.9") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ag8j20m3i4mr9sagh7f4hkwfypaka2azi0gmh9rr6n5x513qp6f")))

