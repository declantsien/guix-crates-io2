(define-module (crates-io li fe lifeless) #:use-module (crates-io))

(define-public crate-lifeless-0.1.0 (c (n "lifeless") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)))) (h "01x16fv1676dpkafk9arqni9ay5xpds5vfbxk8znhxjgmvpasxv9") (y #t)))

(define-public crate-lifeless-0.1.1 (c (n "lifeless") (v "0.1.1") (d (list (d (n "derive_more") (r "^0.99.17") (d #t) (k 0)) (d (n "heapless") (r "^0.8.0") (d #t) (k 0)))) (h "0qk8b79xl1rqn5fpzx0q3z2ckd5klndsmj53rj2h6m3x2q2ni2fk")))

(define-public crate-lifeless-0.2.0 (c (n "lifeless") (v "0.2.0") (h "0a4qlzswrjj48bic1avd1lvgnrv75shbqjya5ijyw6jhfdfim08n")))

