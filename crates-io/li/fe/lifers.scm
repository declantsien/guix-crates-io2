(define-module (crates-io li fe lifers) #:use-module (crates-io))

(define-public crate-lifers-0.1.0 (c (n "lifers") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "0rhx6vd9f2ghbbrawclshhy9bs38scgd8fqcnprxmq5hchr4h34y")))

(define-public crate-lifers-0.2.0 (c (n "lifers") (v "0.2.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)))) (h "194809xg2ib253d02jz6yv510j5h69p61pbbk2j8qp92lyal5cla")))

