(define-module (crates-io li bj libjit-sys) #:use-module (crates-io))

(define-public crate-libjit-sys-0.1.0 (c (n "libjit-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1jcgrl0rgz7f9d30hcxrsc9wkcnfwmrr9wrx8rsip6mqgk60sfjb")))

(define-public crate-libjit-sys-0.1.1 (c (n "libjit-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1shvn1zjq4ppqqnl1ymh8wjyi5zi06yp5gqr54zn9y6h2slilplv")))

(define-public crate-libjit-sys-0.1.2 (c (n "libjit-sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0p2cpicda86z9wysrzl8hj1syj2054v0808hymrcr7k1pzi69xry")))

(define-public crate-libjit-sys-0.1.3 (c (n "libjit-sys") (v "0.1.3") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1c5iqjlfy4rx25shf5y78gr925m4vkv38w056m3d2rz98219lhr2")))

(define-public crate-libjit-sys-0.1.4 (c (n "libjit-sys") (v "0.1.4") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "11fnqqahm8xxddzaisp33d7vdlaaqvs6ql51a5qm5b7xqwvvkgwb")))

(define-public crate-libjit-sys-0.1.5 (c (n "libjit-sys") (v "0.1.5") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0v0vz5q743fs4g4wfzki399cn9j8s0px62bgy73v8glby395xdas")))

(define-public crate-libjit-sys-0.1.6 (c (n "libjit-sys") (v "0.1.6") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "10l96zwixf1q2ami2mp8bmlprkzscr9ar79hy40227ak0kf2fc41")))

(define-public crate-libjit-sys-0.1.8 (c (n "libjit-sys") (v "0.1.8") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "00kq241ja5cmj56yhrdj2ry6ipsspg4zv3qg5gd740wnvxbkm610")))

(define-public crate-libjit-sys-0.1.9 (c (n "libjit-sys") (v "0.1.9") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1s9ihz0i0igy3dy02rfygf7rxv715z36gjxwiayjlw9rk5y2h77a")))

(define-public crate-libjit-sys-0.1.10 (c (n "libjit-sys") (v "0.1.10") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1l6b54ig2nlqmb355afigdn0px9z1gzinzvxp8shgqg7bbphgcjp")))

(define-public crate-libjit-sys-0.1.11 (c (n "libjit-sys") (v "0.1.11") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0jsxx8z26lkpgylf575lw5dq84w4zmi3yz0r3n8l129py9vh40np")))

(define-public crate-libjit-sys-0.1.12 (c (n "libjit-sys") (v "0.1.12") (d (list (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1d8gswqcp052i1d9xb7xfkqirqdl1h685mah4c3p168s0sqxm7vh")))

(define-public crate-libjit-sys-0.2.1 (c (n "libjit-sys") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0mmzhgg4sk0hgmbp9b9x6m9zr21xicjc7r1df3lm35vv2syxp72f")))

