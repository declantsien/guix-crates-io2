(define-module (crates-io li bj libjwt) #:use-module (crates-io))

(define-public crate-libjwt-0.1.0 (c (n "libjwt") (v "0.1.0") (d (list (d (n "base64") (r "^0.9.0") (d #t) (k 0)) (d (n "error-chain") (r "^0.11.0") (d #t) (k 0)) (d (n "openssl") (r "^0.10.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0gn5a7dhjnjq22waf0wgyin6qffwxnbnwvsdcgigyka59wbq3wng") (f (quote (("default"))))))

