(define-module (crates-io li bj libjpegturbo-sys) #:use-module (crates-io))

(define-public crate-libjpegturbo-sys-0.0.1 (c (n "libjpegturbo-sys") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "18py5ygqydax2kcyh0kv94hq7si3rykv59v0npgkpa6c9g7l0adz")))

(define-public crate-libjpegturbo-sys-0.0.2 (c (n "libjpegturbo-sys") (v "0.0.2") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0b53y0si4q23wwql117sfim3r9by3p3c9gpz0r3n425xphyzaxvk")))

