(define-module (crates-io li bj libjuice-sys) #:use-module (crates-io))

(define-public crate-libjuice-sys-0.9.7 (c (n "libjuice-sys") (v "0.9.7") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0cy7v4bndi19rbw85zjz9b86c7m2kijx2pnhxyqhd7c4rl4w4krv")))

