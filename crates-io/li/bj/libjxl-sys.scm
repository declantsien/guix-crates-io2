(define-module (crates-io li bj libjxl-sys) #:use-module (crates-io))

(define-public crate-libjxl-sys-0.2.0 (c (n "libjxl-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.2.5") (d #t) (k 1)))) (h "0pwflj9fj60cr413kswv0lgfq3har6brmsq9s8fx7ykhmcd6p7z1") (l "jxl")))

(define-public crate-libjxl-sys-0.2.1 (c (n "libjxl-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.2.5") (d #t) (k 1)))) (h "10c0bh0cc3gir709hhg00yb4bp0wp0czf468dwnrq7z7sswsyl92") (l "jxl")))

(define-public crate-libjxl-sys-0.2.2 (c (n "libjxl-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.2.6") (d #t) (k 1)))) (h "0bx8h70fmzvk6n8s02v467zk8vvyczwx9dvx5f6g3dvns0l1qch8") (l "jxl")))

(define-public crate-libjxl-sys-0.3.0 (c (n "libjxl-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.3.0") (d #t) (k 1)))) (h "0ljn9gbxb36xnwfwww3fd1ccq634j3i4ddq54b2md9nbq18n3b85") (l "jxl")))

(define-public crate-libjxl-sys-0.3.1 (c (n "libjxl-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.3.0") (d #t) (k 1)))) (h "05hjhp4i64klwgl8zdjljsfvzrlgnldl7vpsylgd9am7x03mjb9a") (l "jxl")))

(define-public crate-libjxl-sys-0.3.2 (c (n "libjxl-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.3.1") (k 1)))) (h "0lwhl6hq6z23w6mi4v106dqifg7ks2hgfgm9kidr9r3h0hz306fx") (l "jxl")))

(define-public crate-libjxl-sys-0.3.3 (c (n "libjxl-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.3.2") (k 1)))) (h "1a7xyr4bl3hfqp473h890zy0ppqhkx1x8b7v22bza3lak945086z") (l "jxl")))

(define-public crate-libjxl-sys-0.3.4 (c (n "libjxl-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.3.2") (k 1)))) (h "0l1ckrb3mpl2h8fx04id5zmfmhzwzmfxjl89h3cv25k2063y1y5i") (l "jxl")))

(define-public crate-libjxl-sys-0.3.5 (c (n "libjxl-sys") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.3.4") (k 1)))) (h "1flbd10kpgq2v1bx3klkjpp6rpi9316cpx2nihhn877ihl85zh7z") (l "jxl")))

(define-public crate-libjxl-sys-0.3.6 (c (n "libjxl-sys") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.3.5") (k 1)))) (h "0knd74dlmmgfqgv82hfvs7p0hirgfww2lvv3d7r9r2jfxbwl8q11") (l "jxl")))

(define-public crate-libjxl-sys-0.7.0 (c (n "libjxl-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.7.0") (k 1)))) (h "17529bhcqamv4iz1rzk611valbcfjg2lhbmsh7y42w5lnkl20l3s") (l "jxl")))

(define-public crate-libjxl-sys-0.7.1 (c (n "libjxl-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.7.3") (k 1)))) (h "1hbyg1rnaiqhfjjjj0dxm0xph3m38b7h7px7cpwbppljds3hvf84") (l "jxl")))

(define-public crate-libjxl-sys-0.7.2 (c (n "libjxl-sys") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "libjxl-src") (r "^0.7.6") (k 1)))) (h "1258vld9qksh7rcix9k393b1pbkqdz0bgh7xw8mg7szar12lfv9m") (l "jxl")))

