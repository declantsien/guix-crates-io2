(define-module (crates-io li bj libjess) #:use-module (crates-io))

(define-public crate-libjess-0.1.0 (c (n "libjess") (v "0.1.0") (h "00kj0114v0wdzwwhxc2ypx42vky53zjpbv2xm0vs4y4a7p7cbq2y") (f (quote (("fs") ("default" "fs"))))))

(define-public crate-libjess-0.1.1 (c (n "libjess") (v "0.1.1") (h "1k54ilzw2ayhdv50zj9vi6xwd085pibqdqzna02ddqlvl49p9rh6") (f (quote (("fs") ("default" "fs"))))))

