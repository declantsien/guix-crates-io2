(define-module (crates-io li bj libjxl-src) #:use-module (crates-io))

(define-public crate-libjxl-src-0.2.0 (c (n "libjxl-src") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0zgqfyx690zc60vkplz8hp4ivr8ihlg5cwbj5lcr87d2z8b94c7c")))

(define-public crate-libjxl-src-0.2.1 (c (n "libjxl-src") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0cq17abdgf7ysi4bkrqjzcq9hvqsknl3c2j71fmmgmr851isfahg")))

(define-public crate-libjxl-src-0.2.2 (c (n "libjxl-src") (v "0.2.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0hmhb245h7gy5jldhklp0g4d7i0bxilsmx34rw8h49ksaiq8658g")))

(define-public crate-libjxl-src-0.2.3 (c (n "libjxl-src") (v "0.2.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0xckvkrwnqpl2j3n1q5w71y6bd8iahsjj0jy6ak7yacjgs7ikkq8")))

(define-public crate-libjxl-src-0.2.4 (c (n "libjxl-src") (v "0.2.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "10j5jlgmqihkp0cqiwz3sm3vy4xmk8lqb2vx7r0z2wnrj5kgivdb")))

(define-public crate-libjxl-src-0.2.5 (c (n "libjxl-src") (v "0.2.5") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1d92yig1wwsic2wis7wx3lrwbiz4f19k33il0d7cr6y3flf59ywx")))

(define-public crate-libjxl-src-0.2.6 (c (n "libjxl-src") (v "0.2.6") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "18x33c1f9lgrafwlwqffh8y5j7zfrmbsz42xz21mz68hnh94s1ck")))

(define-public crate-libjxl-src-0.3.0 (c (n "libjxl-src") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1zh0jg9kw6f2v7j9r1xrr8dh9pfy7zlprs8rizn2891cn9a2r2m5")))

(define-public crate-libjxl-src-0.3.1 (c (n "libjxl-src") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "100czjlm341grxcpshh0w7rm5468hr42rj9w91v66z3qs40fssrh") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3.2 (c (n "libjxl-src") (v "0.3.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1nd9rsdzicbdngqis818437hb4qk578vl7lhrp99qar22fp9xf6k") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3.3 (c (n "libjxl-src") (v "0.3.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0gd399ykqv8y2n1hb1f168q4kyzq04xpflwv57bqckhscb4iybyl") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3.4 (c (n "libjxl-src") (v "0.3.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0iwjbwqa58p9k9nmbckjl4g6rp0qi6lfmi93flf4xy2cfw2vbqmb") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3.5 (c (n "libjxl-src") (v "0.3.5") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1k5cb8bj292b9z0m6lgdxljz7fds8v9mi5k307pl0ak3d954zklw") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3.6 (c (n "libjxl-src") (v "0.3.6") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "028vlwa7xpy6pm9qx7bx0vhnhaal8glynakzwrja3qyxpgswdvb2") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3.7 (c (n "libjxl-src") (v "0.3.7") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "18dy74ly5j5pdyszqyphnrhrajcrfyrs0v3b67c3wkz5nzc53zzk") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.3.8 (c (n "libjxl-src") (v "0.3.8") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "121vffh32krwjq3iwy1fxb8j9p2mdr0rviw12mjrp0r6b2rhwry1") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7.0 (c (n "libjxl-src") (v "0.7.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0ayy8fg76hv7gf8dgb5wb8891pc5kwzcpbab0pg1z4abhl0mq9gs") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7.1 (c (n "libjxl-src") (v "0.7.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0bv1lnzrny1xwx8r647gakcwsxknjmc4wficsggcmx3w9pdw81r2") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7.2 (c (n "libjxl-src") (v "0.7.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1fmw9s91am173znxx2v559fm9g9360fsa6h7wlcjrvpr7bvg561m") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7.3 (c (n "libjxl-src") (v "0.7.3") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "19z9cf0w6p0rwvqcazvpf4h0fg6z9q4wmh8j868fzp81j7yg91fl") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7.4 (c (n "libjxl-src") (v "0.7.4") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 0)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0wqg3ra5ma787x72zp53yarivfcxpqqiz64ny0nyba1cpyaybnxz") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7.5 (c (n "libjxl-src") (v "0.7.5") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 0)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0fj00dvp1br03hvnv4fmc5r60pm8igxyr01mkq9kdv0vhlwm0kww") (f (quote (("instant-build") ("default" "instant-build"))))))

(define-public crate-libjxl-src-0.7.6 (c (n "libjxl-src") (v "0.7.6") (d (list (d (n "cmake") (r "^0.1.48") (d #t) (k 0)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0x8z666q6x20ag3ismp45l8is3cdnlnsiycifzllcnrv0r096vms") (f (quote (("instant-build") ("default" "instant-build"))))))

