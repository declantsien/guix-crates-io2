(define-module (crates-io li bj libjade-sys) #:use-module (crates-io))

(define-public crate-libjade-sys-0.0.1-dev.1 (c (n "libjade-sys") (v "0.0.1-dev.1") (d (list (d (n "bindgen") (r "^0.64") (d #t) (t "cfg(not(windows))") (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1if8xrhn1psbyhhyr72k1i6v0r8m5f3w8qri1p5wy87mf5ycam4h")))

(define-public crate-libjade-sys-0.0.1 (c (n "libjade-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.64") (d #t) (t "cfg(not(windows))") (k 1)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 1)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "00x1f05jy7qmcvxgwsz2v57q6zcszc2lv4sld87b00vnmsm5hysl")))

(define-public crate-libjade-sys-0.0.2-pre.1 (c (n "libjade-sys") (v "0.0.2-pre.1") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (t "cfg(not(windows))") (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libcrux-platform") (r "=0.0.2-pre.1") (d #t) (k 1)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)))) (h "18sp430a371ihpfzwx6sflcbwcism46cfgvkpvbx0xafb5qdrk4c") (f (quote (("bindings" "bindgen"))))))

(define-public crate-libjade-sys-0.0.2-pre.2 (c (n "libjade-sys") (v "0.0.2-pre.2") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (t "cfg(not(windows))") (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libcrux-platform") (r "=0.0.2-pre.2") (d #t) (k 1)) (d (n "pretty_env_logger") (r "^0.5") (d #t) (k 2)))) (h "13pvizs2hq2pjrlwgdf3w03qgfkdwp8cyv73xdd8zgvnljxj4kgc") (f (quote (("bindings" "bindgen"))))))

