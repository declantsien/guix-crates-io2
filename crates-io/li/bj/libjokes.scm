(define-module (crates-io li bj libjokes) #:use-module (crates-io))

(define-public crate-libjokes-0.1.1 (c (n "libjokes") (v "0.1.1") (d (list (d (n "json") (r ">=0.12.4, <0.13.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.0, <0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0rdk1njm90p95znsx4nv6cp1s72pz2z0jns1qfsyknlx9vkl4iic")))

(define-public crate-libjokes-0.1.2 (c (n "libjokes") (v "0.1.2") (d (list (d (n "json") (r ">=0.12.4, <0.13.0") (d #t) (k 0)) (d (n "reqwest") (r ">=0.10.0, <0.11.0") (f (quote ("blocking" "json"))) (d #t) (k 0)))) (h "0zinhqw0f0y5axp1xf1l52z016ng7d38cpcdr7smwcn4vrf8wiw9")))

