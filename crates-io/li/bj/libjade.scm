(define-module (crates-io li bj libjade) #:use-module (crates-io))

(define-public crate-libjade-0.0.1-dev.1 (c (n "libjade") (v "0.0.1-dev.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "fs_extra") (r "^1.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (k 1)) (d (n "bindgen") (r "^0.64") (d #t) (t "cfg(not(windows))") (k 1)))) (h "0hajcfl8rkixa4a3h2aqhdhqkdw198znli1x6n8k0ab6rnmi9jhj")))

(define-public crate-libjade-0.0.0 (c (n "libjade") (v "0.0.0") (h "1yf1777j7x9pjv1b1fx2c21kgm7ihc6a8y9dbvymg5ap5afxwhpn")))

