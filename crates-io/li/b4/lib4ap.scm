(define-module (crates-io li b4 lib4ap) #:use-module (crates-io))

(define-public crate-lib4ap-0.1.3 (c (n "lib4ap") (v "0.1.3") (d (list (d (n "custom_error") (r "^1.9.2") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)) (d (n "tokio") (r "^1.17.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)))) (h "05yirpax11wxi2ny61y0vyw0s1rdkrxy7xpqnzqnbrv3h9lnn4w5")))

