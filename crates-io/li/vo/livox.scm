(define-module (crates-io li vo livox) #:use-module (crates-io))

(define-public crate-livox-0.1.0 (c (n "livox") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "las") (r "^0.7.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "livox-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "palette") (r "^0.4") (d #t) (k 2)))) (h "1v7dd91avgnlfm3vgjrir9nshxdsmigfpkkqm0q7dvr1n4ik827l")))

(define-public crate-livox-0.1.1 (c (n "livox") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.7") (d #t) (k 2)) (d (n "las") (r "^0.7.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "livox-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "num-derive") (r "^0.2.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.0") (d #t) (k 0)) (d (n "palette") (r "^0.4") (d #t) (k 2)))) (h "18gw7s3wbs8pcmqgzwkczckcgg2wrj713f5jalwmnd8djq9i83aj")))

