(define-module (crates-io li vo livox-sys) #:use-module (crates-io))

(define-public crate-livox-sys-0.1.0 (c (n "livox-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1l60fdxgcb5xffix07jc8vp5abci0j27akz5bvypxpflh75ccnw6") (l "livox_sdk_static")))

(define-public crate-livox-sys-0.1.1 (c (n "livox-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0wnrnyy3gcsirb883zv3ynas6id6fz2q18gv10dd8b28hp6jm4mk") (l "livox_sdk_static")))

