(define-module (crates-io li ri liriodendron) #:use-module (crates-io))

(define-public crate-liriodendron-0.2.0 (c (n "liriodendron") (v "0.2.0") (d (list (d (n "better-panic") (r "^0.2.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.13") (d #t) (k 0)) (d (n "crossterm") (r "^0.17.4") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "image") (r "^0.23.8") (d #t) (k 0)) (d (n "num") (r "^0.3.0") (d #t) (k 0)) (d (n "radix_fmt") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.16") (k 0)) (d (n "tokio-threadpool") (r "^0.1.18") (d #t) (k 0)) (d (n "tui") (r "^0.10.0") (f (quote ("crossterm"))) (k 0)))) (h "15kvq3ip4lwvkdjblbrdw7m0bfq99i57q0wb4qsl5i7y8r8i5rii")))

