(define-module (crates-io li ly lily) #:use-module (crates-io))

(define-public crate-lily-0.1.0 (c (n "lily") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0n5qsi747kxbrw9qhq3nm01qqgqp2wg5m1v1qavz9a4lm2x9nsc9") (y #t)))

(define-public crate-lily-0.1.1 (c (n "lily") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0z5lqg9ghrybyx22yxsn93ybcpdcam00wywaxvnkwzwrvik0zvxd")))

(define-public crate-lily-0.2.0 (c (n "lily") (v "0.2.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "17nvx0sm8xvpai09ph64rynqvdbff5scpskpk4yh80r2kqgdl6va")))

(define-public crate-lily-0.3.0 (c (n "lily") (v "0.3.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0fb29ivk5ls17968y27gid8l94z59y40xncz0anki67m1glv81xf")))

(define-public crate-lily-0.4.0 (c (n "lily") (v "0.4.0") (d (list (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1nl4dbl1iignh6p23ii8m7ka24d0wap5jhx9y9nlxxh6cs28fi5j")))

