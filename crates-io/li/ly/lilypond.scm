(define-module (crates-io li ly lilypond) #:use-module (crates-io))

(define-public crate-lilypond-0.1.0 (c (n "lilypond") (v "0.1.0") (d (list (d (n "fsutils") (r "^0.1.2") (d #t) (k 0)))) (h "0h1h5ii8a483fvig2j1c4jgn03lak3wjma6qd2c262dy4vmmqh8p")))

(define-public crate-lilypond-0.1.1 (c (n "lilypond") (v "0.1.1") (d (list (d (n "fsutils") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1plggnsy4wgn7ccbbjig3ihxl7b0bk29bdqzp3m53ph1gpmiy21d")))

