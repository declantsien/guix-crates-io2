(define-module (crates-io li ce licenses_pro) #:use-module (crates-io))

(define-public crate-licenses_pro-0.1.0 (c (n "licenses_pro") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ring") (r "^0.17.7") (d #t) (k 0)))) (h "0na1ayynihq433r0ysyv59q9346plslbaf60ydfvizqc6q7ycx5x")))

(define-public crate-licenses_pro-0.1.1 (c (n "licenses_pro") (v "0.1.1") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "ring") (r "^0.17.7") (d #t) (k 0)))) (h "1ng3q7v72l9rw8616hwq444qwqkmq2188xqk9ng8cl7b1fsxdsqw")))

(define-public crate-licenses_pro-0.2.0 (c (n "licenses_pro") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.17.7") (d #t) (k 0)))) (h "11i5pgysqvw5bnj5qyzvpbqb33l10yka8lacg82lrj7mms52n2yw") (s 2) (e (quote (("reqwest" "dep:reqwest"))))))

(define-public crate-licenses_pro-0.2.1 (c (n "licenses_pro") (v "0.2.1") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.17.7") (d #t) (k 0)))) (h "1i38f3xqmjrxxx0y4j4aflapzc2gw6nh6nwrqw1fpbly7hsx703q") (s 2) (e (quote (("reqwest" "dep:reqwest"))))))

(define-public crate-licenses_pro-0.3.0 (c (n "licenses_pro") (v "0.3.0") (d (list (d (n "base64") (r "^0.21.7") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("blocking"))) (o #t) (d #t) (k 0)) (d (n "ring") (r "^0.17.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1qh40jphgq4gdz9fn6nfiy7a9673pjzkvc8bii72fbicnh3phb59") (s 2) (e (quote (("reqwest" "dep:reqwest"))))))

