(define-module (crates-io li ce licenser) #:use-module (crates-io))

(define-public crate-licenser-1.0.0 (c (n "licenser") (v "1.0.0") (h "1d7bb2xb47aylm78q5al1nd5nv5fjgjwjgy2ylai75mj7gp8dbii") (y #t)))

(define-public crate-licenser-1.0.1 (c (n "licenser") (v "1.0.1") (h "1df5hg9y0r2xii42lfqn4wkrr08zn8p5773ayrz333byyykxq3sn") (y #t)))

(define-public crate-licenser-1.1.0 (c (n "licenser") (v "1.1.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "05p9h8ghahikrv5hjw7rxgkkqz0zkmxz921116jv4h1mn8jpqqdz") (y #t)))

(define-public crate-licenser-1.2.0 (c (n "licenser") (v "1.2.0") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xvm4xf43562d9dv4p3kn9wd6vx0n8wb6qrx9kkjbq30bkh67wxv") (y #t)))

(define-public crate-licenser-1.2.1 (c (n "licenser") (v "1.2.1") (d (list (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xd5jscgfgsx64l96qdrbnkxv1dw4g7f98p40rn0fbldpipdrymw")))

