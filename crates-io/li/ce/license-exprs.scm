(define-module (crates-io li ce license-exprs) #:use-module (crates-io))

(define-public crate-license-exprs-1.0.0 (c (n "license-exprs") (v "1.0.0") (h "0fjyayvlv1sq1g58ww5zcs92r6870qvqx5y3w966m1hawbs32fa9") (y #t)))

(define-public crate-license-exprs-1.1.0 (c (n "license-exprs") (v "1.1.0") (h "1j016vixxqk2rj7kj0mmdl5qi34ryzbmi8qvillqa0x65dgxfbwg")))

(define-public crate-license-exprs-1.1.2 (c (n "license-exprs") (v "1.1.2") (h "0nni17s24s1zq8dxvfbnmhkar5hfsvk4407v1080a4ksihasnsjx")))

(define-public crate-license-exprs-1.2.0 (c (n "license-exprs") (v "1.2.0") (h "1qj92hq4vdfpla4zqr145dlc8kxraxml2h5a0kkf9yllxsxxgrpp")))

(define-public crate-license-exprs-1.3.0 (c (n "license-exprs") (v "1.3.0") (h "0rhsrhwvpp6ldilbapishcp36ly4b9h7lfbnfnq9np3qz2lp1q90")))

(define-public crate-license-exprs-1.4.0 (c (n "license-exprs") (v "1.4.0") (h "14sm25zb9gdg6264d2dwjrsvnws8hzdhdrgs9ps2x26pqp7shvg6")))

(define-public crate-license-exprs-1.5.0 (c (n "license-exprs") (v "1.5.0") (h "15zisva465acmc6sypayni70lmsf783cb9vhn5mkn5b98vz4wdzb")))

(define-public crate-license-exprs-1.6.0 (c (n "license-exprs") (v "1.6.0") (h "0gfck6gwx8rd7pcc3r0743agzijkdjw40i3wazfnc9wqyfjw6xx5")))

