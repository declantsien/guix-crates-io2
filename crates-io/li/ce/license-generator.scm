(define-module (crates-io li ce license-generator) #:use-module (crates-io))

(define-public crate-license-generator-0.1.0 (c (n "license-generator") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "license") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "16g7fkv1iazhzcgh2fcbcmbp9vmx9p17lv85g7kqpa7qcxbb8yj9")))

(define-public crate-license-generator-0.1.1 (c (n "license-generator") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "license") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "04rfryh6sa0x4q7ijq23vai1sfypnjxc3gb6k4cigdsiv8vddxcw")))

(define-public crate-license-generator-0.1.2 (c (n "license-generator") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "license") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1pnyfmb08fnlb2h8sqc2gi14m3c10jarddmsl2116zmr5cw2lw5i")))

(define-public crate-license-generator-0.1.3 (c (n "license-generator") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "license") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1318zymmz9sdpfmrqxvsfqn7n6iyx7j0wjzhb6x7x25dzpx4qpkq")))

(define-public crate-license-generator-0.1.4 (c (n "license-generator") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "license") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "10c0r147lpg9685hf2pyqyf3hsh1qmj9wbjbfrlnpsnqyc7wqbcd")))

(define-public crate-license-generator-0.2.0 (c (n "license-generator") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "license") (r "^0.7.2") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1p0qkbhnkr2w14dyl9papi4bz6b1930psydk5wws6mpik5dkznc9")))

(define-public crate-license-generator-0.3.1 (c (n "license-generator") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "09gb5p9qm2kwd0r9q4hkmv9r0jlgmx2pqs66bxnjvf3zgv0d2clc")))

(define-public crate-license-generator-0.4.0 (c (n "license-generator") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0vz6yn6fz0g38z220mv7jmqcfadcm6nwy41388s6dahh6yqh98ql")))

(define-public crate-license-generator-0.5.0 (c (n "license-generator") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "01gj4iz1250k9j9yws0yg4igshl5lab9zx9hif611vvqzcsygr29")))

(define-public crate-license-generator-0.6.0 (c (n "license-generator") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1r0l8dw92ljy7fwj1rdl4v843lmrfwlxb966p1qczcfnfy6yccpc")))

(define-public crate-license-generator-0.7.0 (c (n "license-generator") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0a057xnni8llwmmpfdxs5m1jshzk1yl6vma19zdj9609zyq48ixb")))

(define-public crate-license-generator-0.7.1 (c (n "license-generator") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0gm4zrv4x78i12iv054prab8yvjgbx0gq2w69vpfgkd6mr4ihlmd")))

(define-public crate-license-generator-0.8.0 (c (n "license-generator") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "170k6xmq2bsqvjc0x0yd7x5bzlpxd3nbjrwh3yaj1bnwsw0vyslp")))

(define-public crate-license-generator-0.8.1 (c (n "license-generator") (v "0.8.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "1n296rr78439b9cmv87ppblxwyskzyqqcb3fmj3a5qlw7cbn46vd")))

(define-public crate-license-generator-0.8.2 (c (n "license-generator") (v "0.8.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "10z41fa7g00nmp1db7rls11nj05k2bwhj9mssn44p1jzalvgbh6y")))

(define-public crate-license-generator-1.0.0 (c (n "license-generator") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0gbml457c67rgh7dlq2jffpha3wlh0rxckam09bb11ggmmxfp77n")))

(define-public crate-license-generator-1.1.0 (c (n "license-generator") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.2.14") (d #t) (k 0)))) (h "0zhbc5sbwz0628qcginp13irgsjrl9y4p02i39gxd7zj1blgj8wq")))

