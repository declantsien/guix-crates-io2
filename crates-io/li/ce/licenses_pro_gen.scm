(define-module (crates-io li ce licenses_pro_gen) #:use-module (crates-io))

(define-public crate-licenses_pro_gen-0.1.0 (c (n "licenses_pro_gen") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "licenses_pro") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "092786lnxjhn8dnjf3jmz8gs7qnpcb4qfhs4j8jnajh84jxcgzw2")))

(define-public crate-licenses_pro_gen-0.2.0 (c (n "licenses_pro_gen") (v "0.2.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "licenses_pro") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "1gxg7pcxf6apkamg5cgclrnk7iw6cyspicvni8wxhcqq18kisz0d")))

(define-public crate-licenses_pro_gen-0.2.1 (c (n "licenses_pro_gen") (v "0.2.1") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "licenses_pro") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "08frj8gfkgys2a9rfbg72bfxma58nbxfvi2h7xzfajivw85lknyg")))

(define-public crate-licenses_pro_gen-0.2.2 (c (n "licenses_pro_gen") (v "0.2.2") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "licenses_pro") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("serde_derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)))) (h "0il3yqh8d08my3m9jbzs06mvdqb2sc4gfhyf9f33yvzvdwwsl743")))

