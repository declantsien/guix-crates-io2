(define-module (crates-io li ce licensed) #:use-module (crates-io))

(define-public crate-licensed-0.1.0 (c (n "licensed") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "untrusted") (r "^0.5.1") (d #t) (k 0)))) (h "1m3nyi8dsa5kkcpjk3hcaz3jyydhp69nz4hf9ix8r7nvycdndwzl")))

(define-public crate-licensed-0.1.1 (c (n "licensed") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.2") (d #t) (k 0)) (d (n "ring") (r "^0.12.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "untrusted") (r "^0.5.1") (d #t) (k 0)))) (h "0qai7v4jrw2ny9mz79m6g1jp1ci9qx5n9m17x9y111nwzrx7984p")))

