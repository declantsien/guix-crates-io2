(define-module (crates-io li ce licensing) #:use-module (crates-io))

(define-public crate-licensing-0.1.0 (c (n "licensing") (v "0.1.0") (h "0rxh5r05j7i51yl5xiay18yb4514q8n772il5lvxdl5l7vgvvk59") (y #t)))

(define-public crate-licensing-0.1.1 (c (n "licensing") (v "0.1.1") (h "18ixzffg2n84j5xwbgmrm4cy9rrsans3vp9miwajr983lgpl4qbn") (y #t)))

(define-public crate-licensing-0.1.2 (c (n "licensing") (v "0.1.2") (h "08p0airs38vvz9pgfs2fm2ih3ldlsqiyhmzhbiwk2r336ir6mrxs") (y #t)))

(define-public crate-licensing-0.1.3 (c (n "licensing") (v "0.1.3") (h "1hln394hwr7am1hiwr732nyxjfbx9qbhvh4d0080yp903x0g5zbz") (y #t)))

(define-public crate-licensing-0.1.4 (c (n "licensing") (v "0.1.4") (h "01k8hq5ipplvq9fc21zn2spcv53nm1hmzxbl2mdv7lc3cfh3as6m") (y #t)))

(define-public crate-licensing-0.2.0 (c (n "licensing") (v "0.2.0") (d (list (d (n "toml") (r "^0.8.13") (d #t) (k 0)))) (h "0nb0rb7vhmc8mbnkqz393hpmq8d6vz10lji5gzq6rr6alrxn8dwi") (y #t)))

