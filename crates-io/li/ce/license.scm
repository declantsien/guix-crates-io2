(define-module (crates-io li ce license) #:use-module (crates-io))

(define-public crate-license-0.1.0 (c (n "license") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)))) (h "057m7qqwjfda2km5q23i9c5hbcfa770ivya3flrqivhifqlnd8h9")))

(define-public crate-license-0.1.1 (c (n "license") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)))) (h "0rhxaxj9d2xvs7l8gwl5j11vk6pkcphd69yzgmpvjz8hxfzcgq6p")))

(define-public crate-license-0.2.0 (c (n "license") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)))) (h "0jkgspvzq47qs2w861l5zmf9810disybwlrjk3db13wh73z3s2bm")))

(define-public crate-license-0.2.1 (c (n "license") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)))) (h "0s1axzqhah5gwx6i66pdwddhgyk711ffd8nv47idk1kcr3ri5xyx")))

(define-public crate-license-0.2.2 (c (n "license") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)))) (h "0a8jzz12rdx5ny4116ziq54cnh6lk3iilhy7q438dkq6f5b5bdd4")))

(define-public crate-license-0.2.3 (c (n "license") (v "0.2.3") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1ir74bn2xkb2h4cylkv4zna5apr1azjgwdkl6c51v5gvna0nh86d")))

(define-public crate-license-0.2.4 (c (n "license") (v "0.2.4") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1b7llqz16622k95hnfzhaclh97h2bvql69brj33s141v8swx7szv")))

(define-public crate-license-0.2.5 (c (n "license") (v "0.2.5") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0kvly84jcd673fr1xmhmdllrkbw0jw2cz19hb08dfv48g69g20r4")))

(define-public crate-license-0.2.6 (c (n "license") (v "0.2.6") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "19ryrgpfn85rfp9726xh2sg3szq7xg49x4zih1l7f68kx4248yg2")))

(define-public crate-license-0.2.7 (c (n "license") (v "0.2.7") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "0hw25imrx3kxfdps8x729zpa8k37rq88qg5fff7xfcan5111h57l")))

(define-public crate-license-0.2.8 (c (n "license") (v "0.2.8") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "03lw27ksx2di3raz3fj6s8sbw60kkqyw9fn2j3yn560bsdgba2jd")))

(define-public crate-license-0.2.9 (c (n "license") (v "0.2.9") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1qgx2zblk2626br15h2fq5q6k2hm6cp7zhxvymrq7jpd7faqfqa3")))

(define-public crate-license-0.2.10 (c (n "license") (v "0.2.10") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "03jp0lhdcgiaak9wl4imrsld4fyjwr78wmpakvi8xdln58rh0xwh")))

(define-public crate-license-0.2.11 (c (n "license") (v "0.2.11") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1xrzhazkzn2czpf05ry328qqyq4nracx1wcx254gj3q7gy4awzad")))

(define-public crate-license-0.2.12 (c (n "license") (v "0.2.12") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "05vc0l5qdjqphk1kdl295lwk6imzcmng4q31zl2fxi1160avrjd6")))

(define-public crate-license-0.3.0 (c (n "license") (v "0.3.0") (d (list (d (n "aho-corasick") (r "^0.6.3") (d #t) (k 0)) (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)))) (h "1b5x7f18xv4qjkj7dxqdbr73qdj8j8g2v63ppznvny5mb1ikqzk9")))

(define-public crate-license-0.4.0 (c (n "license") (v "0.4.0") (d (list (d (n "aho-corasick") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1z9jz99d23q7ndx1zxviiz516pq1pq14aklj787srx0iwxz4ymxk")))

(define-public crate-license-0.5.0 (c (n "license") (v "0.5.0") (d (list (d (n "aho-corasick") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "1kyg57zz1r80gzhbsxxr5r8c9j791g749ra1fhx1rrm2kfgwsavj")))

(define-public crate-license-0.5.1 (c (n "license") (v "0.5.1") (d (list (d (n "aho-corasick") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "007717vj1fy12vmxcijsqdxb2238ki5rbw4vkis768ll7gi89pfv") (f (quote (("unlicense") ("mpl2") ("mit") ("lgpl3") ("gpl3") ("default" "agpl3" "apache2" "cc01" "gpl3" "lgpl3" "mit" "mpl2" "unlicense") ("cc01") ("apache2") ("agpl3"))))))

(define-public crate-license-0.5.2 (c (n "license") (v "0.5.2") (d (list (d (n "aho-corasick") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)))) (h "0qbbyivbr0j85jz5hi7fd70vd6f6pgglpl0jrgl0c338612d3gb2") (f (quote (("unlicense") ("mpl2") ("mit") ("lgpl3") ("gpl3") ("default" "agpl3" "apache2" "cc01" "gpl3" "lgpl3" "mit" "mpl2" "unlicense") ("cc01") ("apache2") ("agpl3"))))))

(define-public crate-license-0.6.0 (c (n "license") (v "0.6.0") (d (list (d (n "aho-corasick") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1ahacrj9g18c7ckrwsxhd4whg3am30vzzz7p9514adppbixqhydq") (f (quote (("unlicense") ("mpl2") ("mit") ("lgpl3") ("gpl3") ("default" "agpl3" "apache2" "cc01" "gpl3" "lgpl3" "mit" "mpl2" "unlicense") ("cc01") ("apache2") ("agpl3"))))))

(define-public crate-license-0.6.1 (c (n "license") (v "0.6.1") (d (list (d (n "aho-corasick") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "06kkvl5v10bgmjfgpviyr5z2pwfyjpg2fsynmqb45qn3dpy65001") (f (quote (("unlicense") ("mpl2") ("mit") ("lgpl3") ("gpl3") ("default" "agpl3" "apache2" "cc01" "gpl3" "lgpl3" "mit" "mpl2" "unlicense") ("cc01") ("apache2") ("agpl3"))))))

(define-public crate-license-0.7.0 (c (n "license") (v "0.7.0") (d (list (d (n "aho-corasick") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1ns0czpywc8s84gl3nhhxfhj1p3wbz8363lwwvq0hg0cr74m6r7h") (f (quote (("unlicense") ("mpl2") ("mit") ("lgpl3") ("gpl3") ("default" "agpl3" "apache2" "cc01" "gpl3" "lgpl3" "mit" "mpl2" "unlicense") ("cc01") ("apache2") ("agpl3"))))))

(define-public crate-license-0.7.1 (c (n "license") (v "0.7.1") (d (list (d (n "aho-corasick") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "0r8g3jw46kpl0apnvp81ddas5hwci4a88gjyzxz96393vhdg4q5l") (f (quote (("unlicense") ("mpl2") ("mit") ("lgpl3") ("gpl3") ("default" "agpl3" "apache2" "cc01" "gpl3" "lgpl3" "mit" "mpl2" "unlicense") ("cc01") ("apache2") ("agpl3"))))))

(define-public crate-license-0.7.2 (c (n "license") (v "0.7.2") (d (list (d (n "aho-corasick") (r "^0.6") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1") (d #t) (k 0)))) (h "1g18r2kvdy2kpxgigd85fdvz5f00mblpk29dm1gysyvjqb1ggzzm") (f (quote (("unlicense") ("mpl2") ("mit") ("lgpl3") ("gpl3") ("default" "agpl3" "apache2" "cc01" "gpl3" "lgpl3" "mit" "mpl2" "unlicense") ("cc01") ("apache2") ("agpl3"))))))

(define-public crate-license-0.8.0 (c (n "license") (v "0.8.0") (h "0dhw5l2zkgz1f6gc8aqsfadwgf1r5kly73m5ys3j46law6yr55kl")))

(define-public crate-license-0.8.1 (c (n "license") (v "0.8.1") (h "0j2r7iqzy329j15j9l7nhalh4kq5jvl5dgiwx27ajibsknk70jnf")))

(define-public crate-license-0.9.0 (c (n "license") (v "0.9.0") (h "0pq5z5dab0ili2vnls3xnrhymng7pfg7bw68gzc0ya378m224gif")))

(define-public crate-license-0.10.0 (c (n "license") (v "0.10.0") (h "0jm47jwrvbq5fzjn12rqhfp89pn7nq57pxgc0r4bd8nvmjb6lc2a")))

(define-public crate-license-0.10.1 (c (n "license") (v "0.10.1") (h "1029fhmfymx0ld37619ajcw97j4qd75739xs2f9xcksfvwb0sg4i")))

(define-public crate-license-0.10.2 (c (n "license") (v "0.10.2") (h "19a3k3whgsfr5jhvdqqws8z2qxbackmal7mxrdqpwya1qhbasjzr")))

(define-public crate-license-1.0.0 (c (n "license") (v "1.0.0") (h "05kkdsk3y7glhiak3s22f7jivyj9ncbrba1lv2gw0misy4xh3ypx")))

(define-public crate-license-1.0.1 (c (n "license") (v "1.0.1") (h "0lks0xs8rd345n53cxp7dsmfdki6ykvdjzjcp3l89fr5fswmzaay")))

(define-public crate-license-1.0.2 (c (n "license") (v "1.0.2") (h "03zsw9435y9v5wwm89gpmdff40sf8mmdd68narwlq7j9pnncqhlc")))

(define-public crate-license-1.1.0 (c (n "license") (v "1.1.0") (h "1ajk5fsyz22z4hbj8fq2672aa5isha7vvrh6bvc9scg0y12rf7a2")))

(define-public crate-license-1.1.1 (c (n "license") (v "1.1.1") (h "0sj4jcsidp33w5k5jmys7h26r91pzl21q9dkvi8j5frippdvydmf")))

(define-public crate-license-1.1.2 (c (n "license") (v "1.1.2") (h "1n789r99pdwfi7y96ja1mkr8c2xkpv4sw4f405k469frbrk1qfkc")))

(define-public crate-license-1.1.3 (c (n "license") (v "1.1.3") (h "0vvaybjp3da17fbnj1rwhzn1c2gvkbx92wypcm1pkk8br0zss3d5")))

(define-public crate-license-1.1.4 (c (n "license") (v "1.1.4") (h "11avm0n90nv5yq1qi306947sjawrh89rrs1kw3r79kpgdsybsiqh")))

(define-public crate-license-1.1.5 (c (n "license") (v "1.1.5") (h "1ivzibivaa2pdh4ylndxvd0ri5xj5jzy9vwaq3grbziqx66sggb7")))

(define-public crate-license-1.1.6 (c (n "license") (v "1.1.6") (h "0n3dx09djkd06wixciwx20wy56bff7hzjnmqn2bclgcip7xsgyg1")))

(define-public crate-license-1.1.7 (c (n "license") (v "1.1.7") (h "02hxq8frcbkg2g3ly8wnb102jwkhynwkgl8flcriaz28rhrcpsvv")))

(define-public crate-license-1.1.8 (c (n "license") (v "1.1.8") (h "0ybmf5lqvkq0khpi57f0i5myiimb7vgfsjsiq1r3gvkrl3v6dlmw")))

(define-public crate-license-1.1.9 (c (n "license") (v "1.1.9") (h "080x34y18h5cwqpfym3dvl4dkbnsnpz8y4f52zqisv8vq37ibnbw")))

(define-public crate-license-1.1.10 (c (n "license") (v "1.1.10") (h "167v6qhhgxzn15ajlb63655kib09si9ab12l0rrwk27ipf1ijvdg")))

(define-public crate-license-1.2.0 (c (n "license") (v "1.2.0") (h "12w556999n31vk83zpw42dknjaylpysjm1ly9w3mq40026nqpmd1")))

(define-public crate-license-2.0.0 (c (n "license") (v "2.0.0") (d (list (d (n "reword") (r "^6.4") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "0c25qd2jxcpz1b17nn2n990fv7gmc4xm5fsg4xmb61nvvsbsawh8")))

(define-public crate-license-2.0.1 (c (n "license") (v "2.0.1") (d (list (d (n "reword") (r "^6.4") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "19y74cn0g45w48giqkfx43q6r812xr5pxbbcgf0lybs0zgkv13na")))

(define-public crate-license-2.0.2 (c (n "license") (v "2.0.2") (d (list (d (n "reword") (r "^6") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "0sih7xkv8m4yj3wil63zlzg61bdadcwmdridd6cx7012qdcnaljs") (f (quote (("offline") ("default"))))))

(define-public crate-license-2.0.3 (c (n "license") (v "2.0.3") (d (list (d (n "reword") (r "^6") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "1vd7ycfi7dlqvhfmbxdzjkpw3xvwqzfwm8ki2lgyk81vc37vmv3g") (f (quote (("offline") ("default"))))))

(define-public crate-license-3.0.0 (c (n "license") (v "3.0.0") (d (list (d (n "reword") (r "^6") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "0m74xhp7mdl76pnb47gsmmfxiy0sd167mqvv9zq8ppqhdhdq27qf") (f (quote (("offline") ("default"))))))

(define-public crate-license-3.1.0 (c (n "license") (v "3.1.0") (d (list (d (n "reword") (r "^7") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "075iqb8npzrl5mj07266aj7ldflqx80my6fj29pxn5ffb5y3l7k1") (f (quote (("offline") ("default"))))))

(define-public crate-license-3.1.1 (c (n "license") (v "3.1.1") (d (list (d (n "reword") (r "^7") (d #t) (k 1)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "0ilril6khvfaq8d3hw743kwvzf6s576h6bj0ghr554cl5va1armn") (f (quote (("offline") ("default"))))))

(define-public crate-license-3.2.0 (c (n "license") (v "3.2.0") (d (list (d (n "reword") (r "^7") (d #t) (k 1)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "135dcg448cqvzjmwi4xdpzbb2rh1smbiwasxx430lqhpa4c1i1vp") (f (quote (("offline") ("default"))))))

(define-public crate-license-3.3.0 (c (n "license") (v "3.3.0") (d (list (d (n "reword") (r "^7") (d #t) (k 1)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1qfqml59l2i4ignhdbvcvhxl39bfcahqc35q942akl28f3rb42ki") (f (quote (("offline") ("default"))))))

(define-public crate-license-3.3.1 (c (n "license") (v "3.3.1") (d (list (d (n "reword") (r "^7") (d #t) (k 1)) (d (n "serde") (r "^1") (o #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1r52ydyhgcj5gg1gfd0z747dflfqkj9mhmjnx95ws4qxxq12zfiv") (f (quote (("offline") ("default"))))))

