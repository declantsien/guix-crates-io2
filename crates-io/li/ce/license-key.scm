(define-module (crates-io li ce license-key) #:use-module (crates-io))

(define-public crate-license-key-0.1.0 (c (n "license-key") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)))) (h "1msfi52g7dql2z62kjiag81glq6n5gs2lhs06j0zj602lj521nph")))

