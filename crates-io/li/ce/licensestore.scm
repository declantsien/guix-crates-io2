(define-module (crates-io li ce licensestore) #:use-module (crates-io))

(define-public crate-LicenseStore-0.1.0 (c (n "LicenseStore") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.25.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.25.0") (d #t) (k 0)))) (h "1ldb69fv7hfphxqrzykghr7bwhghxnhcf0vkd62xjn4ydj1lym3d") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

