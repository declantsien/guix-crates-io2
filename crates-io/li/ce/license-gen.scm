(define-module (crates-io li ce license-gen) #:use-module (crates-io))

(define-public crate-license-gen-0.1.0 (c (n "license-gen") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0i777simmgfvai91zw4m69z7jzm513jay92lc5mh37qza7q4100q")))

(define-public crate-license-gen-0.1.1 (c (n "license-gen") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17qbl4pqbz7y432f48vx1phbcw6c1isnn3i5xmxg0bsdjk79r52r")))

(define-public crate-license-gen-0.1.2 (c (n "license-gen") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ch7v3aadyjy8grsm4kyv271dhzq1yz061g0cb3qhm3n4nc5h3rl")))

(define-public crate-license-gen-0.1.3 (c (n "license-gen") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zc6mliqn6cj6cns6gqsg9pq3y53v91h3fw1hdlbcr5lavkb07gr")))

(define-public crate-license-gen-0.1.4 (c (n "license-gen") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zily3nzg74vdd50hjjvdfrqx94kfjzb1185kywimhblghxh9iwg")))

(define-public crate-license-gen-0.1.5 (c (n "license-gen") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "inquire") (r "^0.2.1") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1glyylzhfx87y3c26km4l7h7nxm2ds8x36mm902ylr6w2yy132a8")))

