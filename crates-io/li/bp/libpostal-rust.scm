(define-module (crates-io li bp libpostal-rust) #:use-module (crates-io))

(define-public crate-libpostal-rust-0.1.0 (c (n "libpostal-rust") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libpostal-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "083cf8bj7ifkyivvxdy0i0a0zqp64db0xai4b0xczk33wxb031nw")))

(define-public crate-libpostal-rust-0.1.1 (c (n "libpostal-rust") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libpostal-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "0il5xmxp3cimnlmpsw4k9w9rdjmybmiqg805xpid0gcpanz3akf0")))

