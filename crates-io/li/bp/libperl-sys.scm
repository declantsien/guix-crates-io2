(define-module (crates-io li bp libperl-sys) #:use-module (crates-io))

(define-public crate-libperl-sys-0.1.0 (c (n "libperl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.44") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1crm6gzanzsc5s6nfli8pj66amr0q95xkxbfgbhh9n5w1v0py140") (l "perl")))

(define-public crate-libperl-sys-0.2.0 (c (n "libperl-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.44") (d #t) (k 1)) (d (n "libperl-config") (r "^0.2") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1hk8jhqp42gl93gi7r5i2ja4075vwqjrpijvrwp4ihr8l2rv5fsl") (l "perl")))

