(define-module (crates-io li bp libp2p-rpc) #:use-module (crates-io))

(define-public crate-libp2p-rpc-0.1.0 (c (n "libp2p-rpc") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.6") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p") (r "^0.12") (d #t) (k 0)) (d (n "smallvec") (r "^0.6.10") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-io-timeout") (r "^0.3.1") (d #t) (k 0)))) (h "06xs1yrfvly6brmg7zy415pccrpv0008pkqis14g09q83h75349g")))

