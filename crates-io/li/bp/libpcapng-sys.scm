(define-module (crates-io li bp libpcapng-sys) #:use-module (crates-io))

(define-public crate-libpcapng-sys-0.1.1 (c (n "libpcapng-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1.50") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "0wi12zjmw329i9h2zwp3hb4svzdq4b31821czw90qrzcsqcbhnhp") (f (quote (("static" "cmake") ("default" "static")))) (l "pcapng")))

(define-public crate-libpcapng-sys-0.1.2 (c (n "libpcapng-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.50") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "156igcf1lss610y9dhz01h9bccx9lbpk170a23sjam011rl5z5qb") (f (quote (("static" "cmake") ("default" "static")))) (l "pcapng")))

