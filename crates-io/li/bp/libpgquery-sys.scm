(define-module (crates-io li bp libpgquery-sys) #:use-module (crates-io))

(define-public crate-libpgquery-sys-0.1.0 (c (n "libpgquery-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0a5jnc1kxqam872617raax5n219q2mjfmi0xmk6bkrf4dwa9ab9a")))

