(define-module (crates-io li bp libpermute) #:use-module (crates-io))

(define-public crate-libpermute-1.0.0 (c (n "libpermute") (v "1.0.0") (d (list (d (n "chacha20") (r "^0.7.2") (k 0)) (d (n "sha2") (r "^0.9.5") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "0qf0sigchm063wgd2dxdscdkq9yvrl7y3d1d8ba7pirh6sndddqh") (f (quote (("std" "sha2/std" "chacha20/std") ("default" "std"))))))

(define-public crate-libpermute-1.0.1 (c (n "libpermute") (v "1.0.1") (d (list (d (n "chacha20") (r "^0.7.2") (f (quote ("cipher"))) (k 0)) (d (n "sha2") (r "^0.9.5") (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1crf98xrxq9dpjqygpn3skssxi25n6j9kd3x8zqslmj9wggqvb7a") (f (quote (("std" "sha2/std" "chacha20/std") ("default" "std"))))))

