(define-module (crates-io li bp libpg_query-sys) #:use-module (crates-io))

(define-public crate-libpg_query-sys-0.1.0 (c (n "libpg_query-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0ppp4yww627n9rfvhidmfwhp9gi9ji9lipp0sh5g6yz5a2sapx80") (l "pg_query")))

(define-public crate-libpg_query-sys-0.1.1 (c (n "libpg_query-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0k51sxwarjzix1n8024zq5fq6cvwkk6vwrcm7s2lsi8xzpr96nlk") (l "pg_query")))

(define-public crate-libpg_query-sys-0.1.2 (c (n "libpg_query-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.45.0") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0ynn1g2ikzkhnqaznrsf1rbqv344wx6kz96dw93p3jlbq4bx74y9") (l "pg_query")))

(define-public crate-libpg_query-sys-0.1.3 (c (n "libpg_query-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "1nsr8dqvwnwq2wfgwjc1vhbrprmwalw2mc3w474m2k36rkpk300l") (l "pg_query")))

(define-public crate-libpg_query-sys-0.2.0 (c (n "libpg_query-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "09149n10xrvpnzhsix6pqn8z4pl85zxxb8cvnpkaaywd3f51s9s6") (l "pg_query")))

(define-public crate-libpg_query-sys-0.2.1 (c (n "libpg_query-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "1p09q7aqqy3q7j7c1yg40k47ijj3ig171npkc01fqhwzijaihx64") (l "pg_query")))

(define-public crate-libpg_query-sys-0.3.0 (c (n "libpg_query-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0ikjhp8z5a10wx2whqc16llyk5hw2zg6n8s1176xs1kqm057dkwk") (l "pg_query")))

(define-public crate-libpg_query-sys-0.3.1 (c (n "libpg_query-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0ghycnsgswr1g8z7ds2cw9bl6djnz3apj9wakxhj0qncbx52i3w4") (l "pg_query")))

(define-public crate-libpg_query-sys-0.4.0 (c (n "libpg_query-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "1yifdp7rg4a4g6iapq0g21bdwfpxfkz2773hn55bjg58iqjvl75p") (l "pg_query")))

