(define-module (crates-io li bp libp2p-swarm-derive) #:use-module (crates-io))

(define-public crate-libp2p-swarm-derive-0.22.0 (c (n "libp2p-swarm-derive") (v "0.22.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0wzj5avlhcncjf8i4mpvmxjcwadam91h5nxd3vskjj566smfnr65")))

(define-public crate-libp2p-swarm-derive-0.23.0 (c (n "libp2p-swarm-derive") (v "0.23.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1vaykjf1nc2qgr3fwbm03ixjkshjl217amj0d1knhlgakxlhlnrn")))

(define-public crate-libp2p-swarm-derive-0.24.0 (c (n "libp2p-swarm-derive") (v "0.24.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "176p11p7xi4xhi2wf1ylf3v2qgc3n3d4zddbymlli1gwsh4b735b")))

(define-public crate-libp2p-swarm-derive-0.25.0-rc.1 (c (n "libp2p-swarm-derive") (v "0.25.0-rc.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "12053b9vabmvbaiypj8ljfi9sby4gjwca7xv2cw90avlijw38k8a")))

(define-public crate-libp2p-swarm-derive-0.25.0 (c (n "libp2p-swarm-derive") (v "0.25.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1hh0digkxqw16s0mgmrvcxdd54w6jx3wilfnx72bsfbxf87jjb07")))

(define-public crate-libp2p-swarm-derive-0.26.0 (c (n "libp2p-swarm-derive") (v "0.26.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1ari8gw0ic2c7mdp8klb6kb1b15szns4ybjpjybw6qcvmmykmnbx")))

(define-public crate-libp2p-swarm-derive-0.26.1 (c (n "libp2p-swarm-derive") (v "0.26.1") (d (list (d (n "either") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "045frqgqk93pcifz032cv1850lxv3g8xha07aw5zwfbpsjnd1d1k") (r "1.56.1")))

(define-public crate-libp2p-swarm-derive-0.27.0 (c (n "libp2p-swarm-derive") (v "0.27.0") (d (list (d (n "either") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1a4ggz15cwghpcm1c3qzh5723m78mzg23gzjis4fr11f8yk570ab") (r "1.56.1")))

(define-public crate-libp2p-swarm-derive-0.27.1 (c (n "libp2p-swarm-derive") (v "0.27.1") (d (list (d (n "either") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1my90n9pcw0j9gvfhlkrqfxdzdmmfd98fladbwsn2ddlh26gxwns") (r "1.56.1")))

(define-public crate-libp2p-swarm-derive-0.27.2 (c (n "libp2p-swarm-derive") (v "0.27.2") (d (list (d (n "either") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "19c72r4dx1a93v2g21kwv43z7x33khvkmffbfba38c11d263qsag") (r "1.56.1")))

(define-public crate-libp2p-swarm-derive-0.28.0 (c (n "libp2p-swarm-derive") (v "0.28.0") (d (list (d (n "either") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0xnca034z1idczq2kwibs0q97xlp3psapy42ww69w92pd55scm4z") (r "1.56.1")))

(define-public crate-libp2p-swarm-derive-0.29.0 (c (n "libp2p-swarm-derive") (v "0.29.0") (d (list (d (n "either") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "15nh988cdw5cykxx9qcpjbs9alz5bjm9mn2z6il02h0ylp3amci7") (r "1.56.1")))

(define-public crate-libp2p-swarm-derive-0.30.0 (c (n "libp2p-swarm-derive") (v "0.30.0") (d (list (d (n "either") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0j3ssw3fdip7cfiqp83xyai8jc81ppw8ghzf9qdh2l6iklmn40hz") (r "1.56.1")))

(define-public crate-libp2p-swarm-derive-0.30.1 (c (n "libp2p-swarm-derive") (v "0.30.1") (d (list (d (n "either") (r "^1.6.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 2)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1") (d #t) (k 2)))) (h "0cmsyc3r1bi8yiqmdcmj1nhc7yb4k0cfhh1w043abdd8jx2drvd0") (r "1.56.1")))

(define-public crate-libp2p-swarm-derive-0.30.2 (c (n "libp2p-swarm-derive") (v "0.30.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "11q2bdxihanjvaksblhng1894fgqdhx9xii88zf35fx41nb1w7pn") (y #t) (r "1.60.0")))

(define-public crate-libp2p-swarm-derive-0.31.0 (c (n "libp2p-swarm-derive") (v "0.31.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0064kpkpjha7k1cy0yc9i8iyxd2higzhf36qlr2bsajq4xc7sllx") (r "1.60.0")))

(define-public crate-libp2p-swarm-derive-0.32.0 (c (n "libp2p-swarm-derive") (v "0.32.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0kyw9icl9lqhiv5qr1n9bx5h38zhq3ln3gy7sjnbcjl265hlbfhg") (r "1.60.0")))

(define-public crate-libp2p-swarm-derive-0.33.0 (c (n "libp2p-swarm-derive") (v "0.33.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro-warning") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.18") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0x0xrlrck9ln6qjk74f6p82rgif9fii16v39swv7h37h7lmfrmf4") (r "1.65.0")))

(define-public crate-libp2p-swarm-derive-0.34.0 (c (n "libp2p-swarm-derive") (v "0.34.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1pgjgb6qj2gz52in2qbj4rca7nw3iilag3fs44x460bd8dbx49wv") (r "1.73.0")))

(define-public crate-libp2p-swarm-derive-0.34.1 (c (n "libp2p-swarm-derive") (v "0.34.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0vwbgmh0h8zx7c8psa239jmrcvd3wxp24c8b22kadnng9a5jci5n") (r "1.73.0")))

(define-public crate-libp2p-swarm-derive-0.34.2 (c (n "libp2p-swarm-derive") (v "0.34.2") (d (list (d (n "heck") (r "^0.5") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "16dr17fdm1dq420yg0n2012wban4ic4r93pyzinpnh88v6fypb2x") (r "1.75.0")))

