(define-module (crates-io li bp libp2prs-runtime) #:use-module (crates-io))

(define-public crate-libp2prs-runtime-0.2.1 (c (n "libp2prs-runtime") (v "0.2.1") (d (list (d (n "async-std") (r "^1.8") (f (quote ("default"))) (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "net" "time"))) (o #t) (d #t) (k 0)))) (h "0iqk512rglhycrsapik0d73fmqpkhip6pbcs4n41s76vsvjibibg")))

(define-public crate-libp2prs-runtime-0.2.2 (c (n "libp2prs-runtime") (v "0.2.2") (d (list (d (n "async-std") (r "^1.8") (f (quote ("default"))) (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std" "executor"))) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "net" "time"))) (o #t) (d #t) (k 0)))) (h "03jjach3np4q11rwy0i2qxa0pya02y0gkp5w29hffjq2bakmpfdd")))

(define-public crate-libp2prs-runtime-0.3.0 (c (n "libp2prs-runtime") (v "0.3.0") (d (list (d (n "async-std") (r "^1.8") (f (quote ("default"))) (o #t) (d #t) (k 0)) (d (n "futures") (r "^0.3") (f (quote ("std" "executor"))) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt-multi-thread" "net" "time"))) (o #t) (d #t) (k 0)))) (h "0575axcskj58ny2bq954w6kafl5indg4a2haiiim8hkbni2ijyjq")))

