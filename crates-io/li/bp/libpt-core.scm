(define-module (crates-io li bp libpt-core) #:use-module (crates-io))

(define-public crate-libpt-core-0.1.7 (c (n "libpt-core") (v "0.1.7") (h "145qh86k407wmra2mbs0w4ydy8f2d7lwfqg94zkj4pk7i3zfgx6f")))

(define-public crate-libpt-core-0.3.10 (c (n "libpt-core") (v "0.3.10") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cucumber") (r "^0.20.2") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.10") (d #t) (k 0)))) (h "1jll9x5bzxj87x61jdss0dblmbkdy4hssf7jy9vlgngjzldvskgv")))

(define-public crate-libpt-core-0.3.11-alpha.0 (c (n "libpt-core") (v "0.3.11-alpha.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cucumber") (r "^0.20.2") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.11-alpha.0") (d #t) (k 0)))) (h "0r5czgss4yi2sqll9cq87p52rgpdk2sw9dhb9s5d10sar0l9k49k")))

(define-public crate-libpt-core-0.3.11-alpha.1 (c (n "libpt-core") (v "0.3.11-alpha.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cucumber") (r "^0.20.2") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.11-alpha.1") (d #t) (k 0)))) (h "0z42qnvxlip4sjsm61lbglgs5lql86297rhzmmsm09555xbfascb")))

(define-public crate-libpt-core-0.3.11 (c (n "libpt-core") (v "0.3.11") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cucumber") (r "^0.20.2") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.11") (d #t) (k 0)))) (h "0h3ww5is38ia8v5rkzh7ns73wfp18q1k39970b9g6mpilpaqnnsc")))

(define-public crate-libpt-core-0.4.0-alpha.2 (c (n "libpt-core") (v "0.4.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "cucumber") (r "^0.20.2") (d #t) (k 0)) (d (n "libpt-log") (r "^0.4.0-alpha.2") (d #t) (k 0)))) (h "0wx3jvnf4f9gr51jik44yx20xg79iwk8k8gy55g7j1nj6llfr903")))

(define-public crate-libpt-core-0.4.0 (c (n "libpt-core") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "libpt-log") (r "^0.4.0-alpha.2") (d #t) (k 0)))) (h "1hc1lx3yyghll4hlxrdd0plxns9c3b5li15dqhpak1v2sg5b7vha")))

