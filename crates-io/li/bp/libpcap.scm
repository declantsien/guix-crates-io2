(define-module (crates-io li bp libpcap) #:use-module (crates-io))

(define-public crate-libpcap-0.1.0 (c (n "libpcap") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q4inafsg3xdg1s407xmay2s22p52j541l891j2dw3rr7bgvjp5v")))

(define-public crate-libpcap-0.1.1 (c (n "libpcap") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16gjzqvfj67bpyzxhswky0rzj57vqw4zaajlckl8p5yrjvr3xsr6")))

(define-public crate-libpcap-0.1.2 (c (n "libpcap") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kacxa27zipphhcqnipm1vpp5r39fzngf81p281x9jjhycnsnm3v")))

(define-public crate-libpcap-0.1.3 (c (n "libpcap") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dgk3wp84idwdpskscanng0dym5x2kdk0j37qfsdaqpq1cycwj46")))

(define-public crate-libpcap-0.1.4 (c (n "libpcap") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0296pjimwsiyw1plq27yc20kybypvg536b19gk64z7ydz6lyv0wy")))

(define-public crate-libpcap-0.1.5 (c (n "libpcap") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11kwx575b73cs03xwydyrz8h0010b3587g3jrk9zjk253fdv7mbz")))

(define-public crate-libpcap-0.1.6 (c (n "libpcap") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "197dh7cl2q2alz29s7651bjdfrk5ybrsav7zkq3hdv2dsp2z8pw6")))

(define-public crate-libpcap-0.1.7 (c (n "libpcap") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wqh2pbvnywlzmg0n7yn0rfbb28zccwgpi5y484y102gpwxqp66w")))

