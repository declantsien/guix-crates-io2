(define-module (crates-io li bp libpg_query2-sys) #:use-module (crates-io))

(define-public crate-libpg_query2-sys-0.2.0 (c (n "libpg_query2-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "1711zizfnj75gb5lp3wxwrs68aw4br0irkh9c3y331h8vh9yjm35") (l "pg_query")))

(define-public crate-libpg_query2-sys-0.2.1 (c (n "libpg_query2-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "0cjqp40zhkyzba9vlr4caybqm5d86f4bkyb255kmzj7kzz23f3vc") (l "pg_query")))

(define-public crate-libpg_query2-sys-0.2.2 (c (n "libpg_query2-sys") (v "0.2.2") (h "0lgbbf19drabzy2kv9vml0y7jaybl8p3xny3j7n8y00sj1f2rk5j") (l "pg_query")))

(define-public crate-libpg_query2-sys-0.2.3 (c (n "libpg_query2-sys") (v "0.2.3") (h "09msny4cppgnyarxk5qzxwjysc6pcv9c0wnfb11knjf8xvn130kh") (l "pg_query")))

(define-public crate-libpg_query2-sys-0.3.0 (c (n "libpg_query2-sys") (v "0.3.0") (h "1kzwyh3bvjzv6kkx8lj484aviaxh0l32l8zxxxv8yz9gwx9rm5vx") (l "pg_query")))

