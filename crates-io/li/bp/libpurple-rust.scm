(define-module (crates-io li bp libpurple-rust) #:use-module (crates-io))

(define-public crate-libpurple-rust-0.0.1 (c (n "libpurple-rust") (v "0.0.1") (d (list (d (n "bindgen") (r "~0.21") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 1)) (d (n "pkg-config") (r "~0.3") (d #t) (k 1)))) (h "0yfz75dkgvp22pfg072v6a8ybl4fhd5a8sx6dsqvgdrw11bs8b0r")))

