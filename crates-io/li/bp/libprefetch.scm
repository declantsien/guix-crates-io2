(define-module (crates-io li bp libprefetch) #:use-module (crates-io))

(define-public crate-libprefetch-0.1.0 (c (n "libprefetch") (v "0.1.0") (h "1ws42spffjklhmf4smjz8zmk5ygq47hxclhzqg992jmkn4bmwzhy")))

(define-public crate-libprefetch-0.1.1 (c (n "libprefetch") (v "0.1.1") (h "0rmd186qzcfm6yaqk8b5wqz6lnfwdkcw4zxim2w52zh7011cyxj5")))

