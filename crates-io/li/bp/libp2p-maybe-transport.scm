(define-module (crates-io li bp libp2p-maybe-transport) #:use-module (crates-io))

(define-public crate-libp2p-maybe-transport-0.1.0 (c (n "libp2p-maybe-transport") (v "0.1.0") (d (list (d (n "libp2p") (r "^0.39.1") (k 0)))) (h "00gsvy8i7kznrcb0cdqzymdyfkx0blla6ann0f81x73gr6sbm1yq") (y #t)))

(define-public crate-libp2p-maybe-transport-0.1.1 (c (n "libp2p-maybe-transport") (v "0.1.1") (d (list (d (n "libp2p") (r "^0.39.1") (k 0)))) (h "1cbhfyxdqjdqnins4cwjrjhgwpdb2pjbw7895bykan54yp80vaf3") (y #t)))

(define-public crate-libp2p-maybe-transport-0.1.2 (c (n "libp2p-maybe-transport") (v "0.1.2") (d (list (d (n "libp2p") (r "^0.39.1") (k 0)))) (h "11k9gqm2ild2w923krkfcy9qwqxp8bxhfw3jlzyf90c6w3n4nbw3") (y #t)))

