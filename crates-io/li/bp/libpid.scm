(define-module (crates-io li bp libpid) #:use-module (crates-io))

(define-public crate-libpid-0.1.0 (c (n "libpid") (v "0.1.0") (d (list (d (n "proptest") (r "^1.3.1") (d #t) (k 0)))) (h "0v43h9dv3cizkijai00wjk3j1v0hc1pcp1lc6a4h0hslvk4ndx89")))

(define-public crate-libpid-0.2.0 (c (n "libpid") (v "0.2.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 0)))) (h "0dpdl8in8vwx1vn5y1f4y3dckva7zl8a1nj39716q5jlwh9ykkrv")))

(define-public crate-libpid-0.2.1 (c (n "libpid") (v "0.2.1") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 0)))) (h "18iix0akdracf3kqb3fs6pqz5jls9sqfrgsg71767238hr0bxx7c")))

(define-public crate-libpid-0.2.2 (c (n "libpid") (v "0.2.2") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 0)))) (h "1chfwwqz54ivgs0zn49h18zp0y4wgx2dl707zwz0h1gx1jhjp6im")))

(define-public crate-libpid-0.2.3 (c (n "libpid") (v "0.2.3") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 0)))) (h "0q7h7d6wl70bxkda5kiji1b1pbz6xdq7gapqmlgwrxck8b3ivaam")))

(define-public crate-libpid-0.3.0 (c (n "libpid") (v "0.3.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "test-log") (r "^0.2.14") (d #t) (k 2)))) (h "0bv6m93b25iharx6r71y6j8rhschpv4id7w6ia7vb4p2gyv30zy9")))

(define-public crate-libpid-0.4.0 (c (n "libpid") (v "0.4.0") (d (list (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "test-log") (r "^0.2.14") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "0faki0bl0ycwbihv7fczmjb8nm9dbzfs7hp5mv1jqqdcxdgsx84n")))

(define-public crate-libpid-0.5.0 (c (n "libpid") (v "0.5.0") (d (list (d (n "proptest") (r "^1.3.1") (d #t) (k 2)) (d (n "serial_test") (r "^3.0.0") (d #t) (k 2)) (d (n "test-log") (r "^0.2.14") (d #t) (k 2)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)))) (h "08hp929z53czhwyrx3ypjvci9vagp2bphdp67kp05gr8ba9599f5")))

