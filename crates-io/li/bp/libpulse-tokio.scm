(define-module (crates-io li bp libpulse-tokio) #:use-module (crates-io))

(define-public crate-libpulse-tokio-0.1.0 (c (n "libpulse-tokio") (v "0.1.0") (d (list (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpulse-binding") (r "^2.26") (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "time"))) (d #t) (k 0)))) (h "0hd20q9jykw234gbhnz7avpn5iv2924fsnryhm6mhhqhab2lchwl") (r "1.56")))

(define-public crate-libpulse-tokio-0.1.1 (c (n "libpulse-tokio") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpulse-binding") (r "^2.26") (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "time"))) (d #t) (k 0)))) (h "14x6h4f7cpq9x2qz0vx9576wvkj0984w5jp7vmp4fg8lxzfd3z95") (r "1.64")))

