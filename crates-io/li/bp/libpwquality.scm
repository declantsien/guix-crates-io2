(define-module (crates-io li bp libpwquality) #:use-module (crates-io))

(define-public crate-libpwquality-0.1.0 (c (n "libpwquality") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpwquality-sys") (r "^0.1.0") (k 0)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "04fcjqwcwnq67hh7qkk135n8jpcggffbphy012bfl99zw4ajva8c") (f (quote (("vendored" "libpwquality-sys/vendored") ("default" "vendored") ("crack" "libpwquality-sys/crack"))))))

(define-public crate-libpwquality-0.2.0 (c (n "libpwquality") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpwquality-sys") (r "^0.2.0") (k 0)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "144bcdvmhzmlidcvlnhypymfqva230vzn13nw4pk1c4d14hb4jzh") (f (quote (("crack" "libpwquality-sys/crack"))))))

(define-public crate-libpwquality-0.3.0 (c (n "libpwquality") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpwquality-sys") (r "^0.2.0") (k 0)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "03rkbingdj7igasanjvw5r7dy0r6qj4vzdakhxvdcvs5273if9vp") (f (quote (("crack" "libpwquality-sys/crack")))) (y #t)))

(define-public crate-libpwquality-0.4.0 (c (n "libpwquality") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpwquality-sys") (r "^0.2.0") (k 0)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "0hl7f6xhg3hwwjz3gd2z6jx39jyknj49vblzw10cjb53xh46gkx2") (f (quote (("crack" "libpwquality-sys/crack"))))))

(define-public crate-libpwquality-0.5.0 (c (n "libpwquality") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpwquality-sys") (r "^0.2.0") (k 0)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "1gcldi7w78h35q51ky6ll9lalkgdaj60lpgppqhz8njjfspw52zh") (f (quote (("crack" "libpwquality-sys/crack"))))))

(define-public crate-libpwquality-0.5.1 (c (n "libpwquality") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpwquality-sys") (r "^0.2.0") (k 0)) (d (n "serial_test") (r "^1.0") (d #t) (k 2)))) (h "1yf5j510nj2s2nzawmpqqz81fxizpvvavp4dl5ndgzhbhz4h1kh6") (f (quote (("crack" "libpwquality-sys/crack"))))))

(define-public crate-libpwquality-0.6.0 (c (n "libpwquality") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpwquality-sys") (r "^0.3.0") (k 0)) (d (n "serial_test") (r "^1.0") (k 2)))) (h "0iljmq7bd7n4vgzdsqhwb2cj7v77a4h0bmgyvxbj3rjy68a5gj64") (f (quote (("vendored" "libpwquality-sys/vendored"))))))

(define-public crate-libpwquality-0.6.1 (c (n "libpwquality") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpwquality-sys") (r "^0.3.0") (k 0)) (d (n "serial_test") (r "^1.0") (k 2)))) (h "0a7zhq645a2jl1fqjglzf5agmkizmiyf90gkha2xgp2rl47a04np") (f (quote (("vendored" "libpwquality-sys/vendored"))))))

