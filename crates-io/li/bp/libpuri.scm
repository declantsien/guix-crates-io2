(define-module (crates-io li bp libpuri) #:use-module (crates-io))

(define-public crate-libpuri-0.1.0 (c (n "libpuri") (v "0.1.0") (h "0ls9c245xv03cajq111858smyylv9n71njbp1kz44mw0lcg5sdgq")))

(define-public crate-libpuri-0.1.1 (c (n "libpuri") (v "0.1.1") (h "1im2ryda6saxais92q7x5366asypz8k7blaan9px5rszryq3mprn")))

(define-public crate-libpuri-0.1.2 (c (n "libpuri") (v "0.1.2") (h "19byan1viqvrlibkpnhxixgdmh9cx4wz71g68xz5h48pl13rfk9q")))

(define-public crate-libpuri-0.1.3 (c (n "libpuri") (v "0.1.3") (h "1slypjz37v9dch5yn61zrpf0zizcsswz3q2s80j1hi9f05lf88a6")))

(define-public crate-libpuri-0.1.4 (c (n "libpuri") (v "0.1.4") (h "08l2807jjywq90d5imyvjfbjq08w8xm18als989fhvdzfnfarmhs")))

(define-public crate-libpuri-0.1.5 (c (n "libpuri") (v "0.1.5") (h "04dyhkv197x892zs4567l3mgs009jpsawxykh9aiw3nap490i9gl")))

