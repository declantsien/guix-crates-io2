(define-module (crates-io li bp libp2p-fetch) #:use-module (crates-io))

(define-public crate-libp2p-fetch-0.1.0 (c (n "libp2p-fetch") (v "0.1.0") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "libp2p") (r "^0.51") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8") (f (quote ("std"))) (k 0)) (d (n "unsigned-varint") (r "^0.7") (d #t) (k 0)))) (h "0spnns2lzll346fpqhsmq4yvqp1m3fmpfbv0f3kjf4q17sds1jcs")))

