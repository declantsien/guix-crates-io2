(define-module (crates-io li bp libpostal-sys) #:use-module (crates-io))

(define-public crate-libpostal-sys-0.1.0-pre.1 (c (n "libpostal-sys") (v "0.1.0-pre.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0cbw5ahvjavmqkpnzsi8zcnh0w8p0h43b3mppa052j5jss967pc8") (l "postal")))

(define-public crate-libpostal-sys-0.1.0 (c (n "libpostal-sys") (v "0.1.0") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "14ymn9hpgyzwbwqbi3dx8135wpmhvhvp26az75bpxg7b447rg3js") (l "postal")))

(define-public crate-libpostal-sys-0.1.1 (c (n "libpostal-sys") (v "0.1.1") (d (list (d (n "autotools") (r "^0.2") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "02a0kv3vxrrwayk7223f3x0pr157ifbikhpfm3acq8gl5qpza0yq") (l "postal")))

