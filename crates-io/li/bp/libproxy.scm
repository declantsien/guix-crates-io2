(define-module (crates-io li bp libproxy) #:use-module (crates-io))

(define-public crate-libproxy-0.1.0 (c (n "libproxy") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.23") (d #t) (k 0)))) (h "1c8z4d9b4qfk6mgsibbnq1gvlvbgj9zmjkcj24474nar7ki2vpd5")))

(define-public crate-libproxy-0.1.1 (c (n "libproxy") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "1p77l3cghw086w7s8b56phcvsnw6rng6qcphgd07wf4g790075sp")))

