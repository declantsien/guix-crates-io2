(define-module (crates-io li bp libpapi_sys) #:use-module (crates-io))

(define-public crate-libpapi_sys-0.1.0 (c (n "libpapi_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 1)))) (h "0jqj58265h3i2biz7rs7knz3lz3mri0xl6z3khc0c3r0m6f2v449") (l "papi")))

(define-public crate-libpapi_sys-0.1.1 (c (n "libpapi_sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "06kz0wjlkm81bkgs6l3pp168g2639ahqqqkrg1s14czz62lwmc1c") (l "papi")))

(define-public crate-libpapi_sys-0.1.2 (c (n "libpapi_sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "156zgadjpsipdp4zbidmx6dm2qc8z4yhiknk6zmd6vhp4cclngd2") (l "papi")))

(define-public crate-libpapi_sys-0.1.3 (c (n "libpapi_sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "1nf82xjll2k6gwj7vwqkbnzlpj0djz6c70a92yckbjkcnrzaahar") (l "papi")))

(define-public crate-libpapi_sys-0.1.4 (c (n "libpapi_sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "0v1mvvlj8hc3jsh17m2gnqkswcfy6l26j2pl91q3885mhikq8gsc") (l "papi")))

(define-public crate-libpapi_sys-0.1.5 (c (n "libpapi_sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "0sw8jbav29nr3b81q7h54ji7ma3jnn7l7bbwz6mg6x55yml76s4w") (l "papi")))

(define-public crate-libpapi_sys-0.1.6 (c (n "libpapi_sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "1j994jilp0vjnbqgzxyvqjmgknplvi10cyhmz9l7hdf3drmk0xmd") (l "papi")))

(define-public crate-libpapi_sys-0.1.7 (c (n "libpapi_sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "num_cpus") (r "^1.8") (d #t) (k 1)))) (h "0wncf366apbdbynwpqb6hqcs1wqk6l5cxs47r2wa5xvawklkg2w0") (l "papi")))

