(define-module (crates-io li bp libp2p-broadcast) #:use-module (crates-io))

(define-public crate-libp2p-broadcast-0.1.0 (c (n "libp2p-broadcast") (v "0.1.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libp2p") (r "^0.35.1") (k 0)))) (h "1mkyyz5cldyvhhsskjgg1k1mhcs5dhj1gwycdy1gb3ray04cwm7z")))

(define-public crate-libp2p-broadcast-0.2.0 (c (n "libp2p-broadcast") (v "0.2.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "libp2p") (r "^0.36.0") (k 0)))) (h "1mggsfwqsyigdbrgjf4q0y0kqc5vzfbjz65llgbpjdb9wh25rgkl")))

(define-public crate-libp2p-broadcast-0.3.0 (c (n "libp2p-broadcast") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "libp2p") (r "^0.37.0") (k 0)))) (h "1k3i79dq5bsc15a5r3g0m0v0xfi9nja9msnbnv1ahrlibnkq7dwr")))

(define-public crate-libp2p-broadcast-0.3.1 (c (n "libp2p-broadcast") (v "0.3.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "libp2p") (r "^0.37.1") (k 0)))) (h "0fdsdy4xncsa5xkm0vlimz6py8npbyad0zwa6mv78yb7pybprzph")))

(define-public crate-libp2p-broadcast-0.4.0 (c (n "libp2p-broadcast") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "libp2p") (r "^0.38.0") (k 0)))) (h "1s1p7pf4hdadfq495dibgks5zylrrsrajq8yz2szadc7qqwhxjvj")))

(define-public crate-libp2p-broadcast-0.5.0 (c (n "libp2p-broadcast") (v "0.5.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.15") (d #t) (k 0)) (d (n "libp2p") (r "^0.39.1") (k 0)))) (h "0piyxml71n12fv4w3vg2c89sfk4nn86dwn5k71f784lwwcpxflmn")))

(define-public crate-libp2p-broadcast-0.6.0 (c (n "libp2p-broadcast") (v "0.6.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "libp2p") (r "^0.39.1") (k 0)))) (h "0nk84krzlwfwabkr91i6bxvy5bm97c18ydlw9a3nvr46ld82wk9s")))

(define-public crate-libp2p-broadcast-0.7.0 (c (n "libp2p-broadcast") (v "0.7.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "libp2p") (r "^0.41") (k 0)))) (h "1jv6qfq07gi2rjvnd6ma5c161pka00vkkg8sx5ch2bpmdr826jhd")))

(define-public crate-libp2p-broadcast-0.8.0 (c (n "libp2p-broadcast") (v "0.8.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "libp2p") (r "^0.42.1") (k 0)))) (h "1nk77v0aqdvmq159f2xmh44cbbmkr4306vpgb0ih9ccvfr5jdmxm")))

(define-public crate-libp2p-broadcast-0.9.0 (c (n "libp2p-broadcast") (v "0.9.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "libp2p") (r "^0.43.0") (k 0)))) (h "1agn04x8z2zxpm9nrjrvv0iwj27jrjfif2v7ckvzzzckm2fcqgbb")))

(define-public crate-libp2p-broadcast-0.9.1 (c (n "libp2p-broadcast") (v "0.9.1") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "libp2p") (r "^0.43.0") (k 0)))) (h "0kmg9jawawfpp430rlq7a8r6zjvff0bhavkvwh6wpbh29jkpkka4")))

(define-public crate-libp2p-broadcast-0.10.0 (c (n "libp2p-broadcast") (v "0.10.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "libp2p") (r "^0.48.0") (k 0)))) (h "1wc4dwk5pfl0nsfq4vd17pz56a6qkjxv2csrvrj7bxy4fgarwdvz")))

(define-public crate-libp2p-broadcast-0.11.0 (c (n "libp2p-broadcast") (v "0.11.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "libp2p") (r "^0.49.0") (d #t) (k 0)))) (h "15ilj96nsqk90kfyb75dj716aqv78jkga9yjq8smc5prqnmiikdq")))

(define-public crate-libp2p-broadcast-0.12.0 (c (n "libp2p-broadcast") (v "0.12.0") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.21") (d #t) (k 0)) (d (n "libp2p") (r "^0.50.0") (d #t) (k 0)))) (h "1s61fnbvr6rbqnj69h3xys6vjq8ds1pprxzcdbrgyfcr29pf79vw")))

