(define-module (crates-io li bp libpass) #:use-module (crates-io))

(define-public crate-libpass-0.1.0 (c (n "libpass") (v "0.1.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gpgme") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0p7srqxr2avndd9knqvls4xn935ywm6182yyjg09mh76rfkki9ps")))

(define-public crate-libpass-0.1.1 (c (n "libpass") (v "0.1.1") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gpgme") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "16ykwva3fv4rpl651jb32z5i7p1px1r93qbd7jnj914ffj4imhls")))

(define-public crate-libpass-0.1.2 (c (n "libpass") (v "0.1.2") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gpgme") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "11cbyj63dcr5dfx1bwfvkjcyql9dbsgjq2fmh16s4594jmcsrjns")))

(define-public crate-libpass-0.1.3 (c (n "libpass") (v "0.1.3") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gpgme") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "09jj2767y2xmcm4bbhl3x8xgz36338b77jmj8a4g9nysn2iylw6w")))

(define-public crate-libpass-0.1.4 (c (n "libpass") (v "0.1.4") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gpgme") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "09h9xmlph908x096x87s13aph95h2wvraqvkhckf7nxa4d2sl0y4")))

(define-public crate-libpass-0.1.5 (c (n "libpass") (v "0.1.5") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gpgme") (r "^0.10.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "167bry88p142rl61w0g7mqyvqf27qrfzqcyd72k56y6myxbfdhi5")))

(define-public crate-libpass-0.2.0 (c (n "libpass") (v "0.2.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gpgme") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "171ic0dybb35c9nmmdncqfh63hpkag53i8zn4614pk7r06dy99xh")))

(define-public crate-libpass-0.3.0 (c (n "libpass") (v "0.3.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gpgme") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0j4rcn2xpizl7hkbpmx49hr1dm764a2b27f6kk8n95kkw4r6bfqd")))

(define-public crate-libpass-0.4.0 (c (n "libpass") (v "0.4.0") (d (list (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "gpgme") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "15rlv7k5nalmyib9paxh3gk8azxn4p63spbdkdnvxjl1z509s27s")))

