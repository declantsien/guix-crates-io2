(define-module (crates-io li bp libpcp) #:use-module (crates-io))

(define-public crate-libpcp-0.6.2 (c (n "libpcp") (v "0.6.2") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "gcollections") (r "^1.4.0") (d #t) (k 0)) (d (n "intervallum") (r "^1.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "trilean") (r "^1.0.1") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "0vxar732m1f2p5hx4snr4f6w6pnwmj5fxcy4pn84sqp74ys15j3i")))

(define-public crate-libpcp-0.6.3 (c (n "libpcp") (v "0.6.3") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "gcollections") (r "^1.4.0") (d #t) (k 0)) (d (n "intervallum") (r "^1.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "trilean") (r "^1.0.1") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "0yh585drpr5a6jrj3kps29p1x7hfw0mdazzcqzqskvrmf2pbnv0r")))

(define-public crate-libpcp-0.6.4 (c (n "libpcp") (v "0.6.4") (d (list (d (n "bit-set") (r "^0.4.0") (d #t) (k 0)) (d (n "gcollections") (r "^1.4.0") (d #t) (k 0)) (d (n "intervallum") (r "^1.2.0") (d #t) (k 0)) (d (n "num") (r "^0.1.29") (d #t) (k 0)) (d (n "trilean") (r "^1.0.1") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "10g908m6b20ykj7ji84g2pn9cmbyvlcx1svna3wncyknwlbfcigj")))

(define-public crate-libpcp-0.7.0 (c (n "libpcp") (v "0.7.0") (d (list (d (n "bit-set") (r "^0.5.3") (d #t) (k 0)) (d (n "gcollections") (r "^1.4.0") (d #t) (k 0)) (d (n "intervallum") (r "^1.2.0") (d #t) (k 0)) (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "trilean") (r "^1.0.1") (d #t) (k 0)) (d (n "vec_map") (r "^0.6.0") (d #t) (k 0)))) (h "00dx940bzlcd0dk1zvjmhmcskqc4nvabvnb9ja4ppzjfndsi59mw")))

