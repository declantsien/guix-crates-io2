(define-module (crates-io li bp libp2p-pubsub) #:use-module (crates-io))

(define-public crate-libp2p-pubsub-0.1.0 (c (n "libp2p-pubsub") (v "0.1.0") (d (list (d (n "libp2p-pubsub-common") (r "^0.1.0") (d #t) (k 0)) (d (n "libp2p-pubsub-core") (r "^0.1.0") (d #t) (k 0)) (d (n "libp2p-pubsub-floodsub") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1zfiyb6vfvwxmi504hpa8j639j4dil8dk88akp838y1mdnhhi64b") (f (quote (("all" "floodsub")))) (s 2) (e (quote (("floodsub" "dep:libp2p-pubsub-floodsub")))) (r "1.66.0")))

