(define-module (crates-io li bp libpq) #:use-module (crates-io))

(define-public crate-libpq-0.0.0 (c (n "libpq") (v "0.0.0") (d (list (d (n "postgres-types") (r "^0.1") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)))) (h "03rgplj159yaij57cq3f0m7zcndmv2ada3lk3rwrgfkd8l77z64f")))

(define-public crate-libpq-0.1.0 (c (n "libpq") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "postgres-types") (r "^0.1") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 2)))) (h "1rhafb0wj8lsq0qnswlswy2g68krbbssbsj1pr99alsylxmccnw5")))

(define-public crate-libpq-0.2.0 (c (n "libpq") (v "0.2.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)))) (h "0l1m8yf2fiwx5c9jk0v3qrxg2d2w6d86as4bgcj18awcmgfsdrrc")))

(define-public crate-libpq-0.3.0 (c (n "libpq") (v "0.3.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "059fhvdgaraq00rdx1yl78s0b5sc5hiassj5n0a87zracp4zdnm3")))

(define-public crate-libpq-0.4.0 (c (n "libpq") (v "0.4.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "0aisazikccn8hq9jy4i1swqm5g7qhj89hd797j0kf7c4cwgki7nn")))

(define-public crate-libpq-0.5.0 (c (n "libpq") (v "0.5.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1ifpnrygh551r4807r2y8nmad8zcsj9j20ls29b5zfws4qcw3k7r")))

(define-public crate-libpq-0.6.0 (c (n "libpq") (v "0.6.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "0mihgdblb0dc32wicwzx20ac4clm2rjd4jx18fig6nnqvrcm6285")))

(define-public crate-libpq-0.7.0 (c (n "libpq") (v "0.7.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1rc5sdnhdbfzwfsy8ywqhl06mnv8khggxhsgh1ii1in0n0b2waax")))

(define-public crate-libpq-0.8.0 (c (n "libpq") (v "0.8.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1jar9awr3fkhncjhkwm4winfjcgsk55fiwvs46hv03fs2306acak")))

(define-public crate-libpq-0.9.0 (c (n "libpq") (v "0.9.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1yv2jcb6nlv67djkxic0zvndlxisbdcpzh21a71jkyv3r9rn6hv4")))

(define-public crate-libpq-0.10.0 (c (n "libpq") (v "0.10.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "05a86vcbz939bl5imyy28diicbdyl68kdxf3gs6qrzvw91szhvw7")))

(define-public crate-libpq-0.11.0 (c (n "libpq") (v "0.11.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "04xd3pai3pwlb1bsc2x9nvw91dibv9f73jai65ns7kirb15dqz5j")))

(define-public crate-libpq-0.12.0 (c (n "libpq") (v "0.12.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1cihfixff56psn5ijm415kgcr6fnzid6w5kzgn5mcw3d5c0ggfik")))

(define-public crate-libpq-1.0.0 (c (n "libpq") (v "1.0.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1rzh7gjdmp8vg8i09irnmn4l3cnfkz9962n7ps2pp77v7gqcbisi")))

(define-public crate-libpq-1.1.0 (c (n "libpq") (v "1.1.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1f179bikql3py7hn0fvqxwg7vik7ydnyv2bdcfc68j8n64qmm7l5")))

(define-public crate-libpq-1.2.0 (c (n "libpq") (v "1.2.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "017xj6c68jzhbkispkwh5a5kv85v2p014rbq4n1lslqgwx145153")))

(define-public crate-libpq-1.3.0 (c (n "libpq") (v "1.3.0") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "1b7xf2wjx3s0zpvh42rj2ld4dh0vkpm3kldlzk2v7p53h5jyv6i2") (f (quote (("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-1.3.1 (c (n "libpq") (v "1.3.1") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "0b7afsxhprcs3h705g3qgij9klkif60ghzs1d58d0jdwr6ksk4ad") (f (quote (("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-1.3.2 (c (n "libpq") (v "1.3.2") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")))) (h "1wwdp0jgcp2zzkf4n37drdswhs99rwv2954whr4965mjsqyhqv64") (f (quote (("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-1.3.3 (c (n "libpq") (v "1.3.3") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")))) (h "11i88v3wginp7c8ib5rkix2yg3x578iczb6hxf53amawnh1hl0ff") (f (quote (("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-1.4.0 (c (n "libpq") (v "1.4.0") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")))) (h "10m2n6qlk5gh1s6vdzl7jmym03z4n5cj18qh9g2lvi5gcgj8q5nz") (f (quote (("v14" "v13" "bitflags") ("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-2.0.0 (c (n "libpq") (v "2.0.0") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")))) (h "05x1smsg150bzbqmy7jx2n79cggxcay7rg5bp8b5pml3l27lxgva") (f (quote (("v14" "v13" "bitflags") ("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-1.3.4 (c (n "libpq") (v "1.3.4") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")))) (h "0xyxiw9p908k6rix89p0bmg2yv93ciyh0jxm2vj805xlzjqbmfmq") (f (quote (("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-1.4.1 (c (n "libpq") (v "1.4.1") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")))) (h "0sw2qmxcpamz2p4lzcwxjj8lwiakp0vcbbaqql0xfpcb9w343zn2") (f (quote (("v14" "v13" "bitflags") ("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-2.0.1 (c (n "libpq") (v "2.0.1") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")))) (h "0sw0b04gccrcq6a3n4n28b4j1b4nl27p32cjinz0cbb2jlqcg5v2") (f (quote (("v14" "v13" "bitflags") ("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-1.3.5 (c (n "libpq") (v "1.3.5") (d (list (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")))) (h "04vqfd43bm87ggb50cvvsmmdx1njy3wmckjch6fzaingbs5kr0fb") (f (quote (("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-1.4.2 (c (n "libpq") (v "1.4.2") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")))) (h "16bhkkvn1grbi3fsy0i8x561axz33sipmr17hyvk9wpj50ih6qad") (f (quote (("v14" "v13" "bitflags") ("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-2.0.2 (c (n "libpq") (v "2.0.2") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")))) (h "0hd7mvy05xmbd67vis1d5rdx79f7xwh6pxrrh8ggawprkqkwaahh") (f (quote (("v14" "v13" "bitflags") ("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-3.0.0-rc.1 (c (n "libpq") (v "3.0.0-rc.1") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1dic7smh1bc9my4vzcfqi22jv6xdvqi7qp4y8rdsl4l4cpdmi0qi") (f (quote (("v14" "v13" "bitflags") ("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-3.0.0 (c (n "libpq") (v "3.0.0") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "codegen") (r "^0.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.6") (d #t) (k 0) (p "libpq-sys")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s38da7hpn9hi80idjvw7nilija7m8jnxx8m9hkiqs49qy6q5i53") (f (quote (("v14" "v13" "bitflags") ("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-3.1.0 (c (n "libpq") (v "3.1.0") (d (list (d (n "bitflags") (r "^1.3") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.8") (d #t) (k 0) (p "libpq-sys")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0y85lhsqh4z9f7vrgi5yvqwzbmbxbraly4szih1pl7726dr4agva") (f (quote (("v14" "v13" "bitflags") ("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

(define-public crate-libpq-4.0.0 (c (n "libpq") (v "4.0.0") (d (list (d (n "bitflags") (r "^2.0") (o #t) (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pq-sys") (r "^0.8") (d #t) (k 0) (p "libpq-sys")) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s10s5icwcaxjy9pdqs14xacxsq1r27pz0ghbbijv9ll71g2yx5h") (f (quote (("v15" "v14") ("v14" "v13" "bitflags") ("v13" "v12") ("v12" "v11") ("v11") ("default"))))))

