(define-module (crates-io li bp libp3p) #:use-module (crates-io))

(define-public crate-libp3p-0.0.0 (c (n "libp3p") (v "0.0.0") (d (list (d (n "async-std") (r "0.99.*") (d #t) (k 0)) (d (n "balloon") (r "0.0.*") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "mohan") (r "0.0.*") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "schnorr") (r "0.0.*") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "0s7mws7vqwp8p7pchy28linl23g7rv1m219lms2f82h95b9k12ig")))

