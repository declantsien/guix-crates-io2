(define-module (crates-io li bp libpebble) #:use-module (crates-io))

(define-public crate-libpebble-0.1.0 (c (n "libpebble") (v "0.1.0") (h "1lj3cd5jfdk91i1ipqfyn8na659fjyg908m9va0n2pn8hkblky6v") (f (quote (("kernel") ("default"))))))

(define-public crate-libpebble-0.1.1 (c (n "libpebble") (v "0.1.1") (h "1y68jziw33m74bkgn7wzgmizs0c0472sfv73vcqgryfjmgwnamnm") (f (quote (("kernel") ("default"))))))

(define-public crate-libpebble-0.2.0 (c (n "libpebble") (v "0.2.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "pci_types") (r "^0.2") (d #t) (k 0)) (d (n "ptah") (r "^0.1") (o #t) (d #t) (k 0)))) (h "16a5lkmh9vgvdqj5ybzwjqw25pk0l2zvylflyi9dag0098kfyvrf") (f (quote (("can_alloc" "log" "ptah"))))))

(define-public crate-libpebble-0.3.0 (c (n "libpebble") (v "0.3.0") (d (list (d (n "bit_field") (r "^0.10") (d #t) (k 0)) (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "pci_types") (r "^0.2.0") (d #t) (k 0)) (d (n "ptah") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0bxk9pcagb9lfjkd6rx5d5wlwj5zcshjh4i2gkadwwnpg239qari") (f (quote (("rustc-dep-of-std" "core") ("can_alloc" "log" "ptah"))))))

