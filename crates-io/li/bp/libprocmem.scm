(define-module (crates-io li bp libprocmem) #:use-module (crates-io))

(define-public crate-libprocmem-0.1.0 (c (n "libprocmem") (v "0.1.0") (h "195snfmj6clf2vm4xf4anf3y0c1q4mkndzjvk3hi0kzk2aw56qcq")))

(define-public crate-libprocmem-0.1.1 (c (n "libprocmem") (v "0.1.1") (h "0n07y42gb80rkmvrkgvi15hlb4mp2m75qbm7gcihws1w3ziz6bna")))

(define-public crate-libprocmem-0.1.2 (c (n "libprocmem") (v "0.1.2") (h "0snn2f1x7z1z042r4h291bsyx6a6la33y5dq3k8y5n1zw0zjf0i4")))

