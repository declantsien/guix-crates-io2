(define-module (crates-io li bp libpy) #:use-module (crates-io))

(define-public crate-libpy-0.1.0 (c (n "libpy") (v "0.1.0") (h "0vsxb04ph5j6nh88a0b23ybsyp4qlrg7ciq89ag25ba2zv97by3w")))

(define-public crate-libpy-0.2.0 (c (n "libpy") (v "0.2.0") (h "00w1vd5xgp2y29jza9wd9lg8qnk7ihcbhn68ii2yf90ilfkvhb0i")))

(define-public crate-libpy-0.2.1 (c (n "libpy") (v "0.2.1") (h "05by368k9vy6597n3grgc9d9zy8fajv302bw70849vp8475diq9p")))

