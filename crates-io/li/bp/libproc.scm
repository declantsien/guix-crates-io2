(define-module (crates-io li bp libproc) #:use-module (crates-io))

(define-public crate-libproc-0.1.0 (c (n "libproc") (v "0.1.0") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "0qrlf6lf0h5hxf3f9400x2xplmsa0zrp1ygwx1v1bwy0a2kzr5bi")))

(define-public crate-libproc-0.2.0 (c (n "libproc") (v "0.2.0") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "1wax5f8ziacjq7drhydhsfazlhb0q4l413sgxcxi484caa1b1ccg")))

(define-public crate-libproc-0.3.0 (c (n "libproc") (v "0.3.0") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "1jswldf7d0mvwlc8airsdgldlp8xhi02hi107k3615002p5lzh6i")))

(define-public crate-libproc-0.3.1 (c (n "libproc") (v "0.3.1") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "0glkbp9ig4lw92lvqr60g9iy4dwmk1zy3ar2fr8rc8fq54v85aqb")))

(define-public crate-libproc-0.3.2 (c (n "libproc") (v "0.3.2") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "1d7a1cw760rbh0akd8xc312i09wg86jk3kavly33zz59smwkxljq")))

(define-public crate-libproc-0.4.0 (c (n "libproc") (v "0.4.0") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "1wln1ih8h6ij3i24h9k93f01d09bznhfq5l7vn820fhrivycmypw")))

(define-public crate-libproc-0.5.0 (c (n "libproc") (v "0.5.0") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.15") (d #t) (k 0)))) (h "0xidccki5m74sjsniqwk1kcizi7bh8jjrbpdywkshbf1fji1pfr2")))

(define-public crate-libproc-0.6.0 (c (n "libproc") (v "0.6.0") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "06k02b16yk433mksqmrbywyg20f8956q8xxjyjayd0rij16qqjxk")))

(define-public crate-libproc-0.7.0 (c (n "libproc") (v "0.7.0") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0qd92bsgiargw2wqh7nghln67lfc6l3p79b1gxjg5xn573xkdk9j")))

(define-public crate-libproc-0.7.1 (c (n "libproc") (v "0.7.1") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "12jr6sp4xyf97xhabw2v38wx2x8cs7wy0rx73j2qnz4hwk40n8kv")))

(define-public crate-libproc-0.7.2 (c (n "libproc") (v "0.7.2") (d (list (d (n "errno") (r "^0.1.8") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1bb3xk4i95a2xccxb318zvilv289y522ixyrnl84mrk7rya24hds")))

(define-public crate-libproc-0.8.0 (c (n "libproc") (v "0.8.0") (d (list (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1n5ncpr164pgc0kgbawzpk32bm3ki9jpqgsnw1fqkrrgczkrrqps")))

(define-public crate-libproc-0.9.0 (c (n "libproc") (v "0.9.0") (d (list (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0mslwwgjgwc4h20jr18ski99ynwcciq5vyylakini6cw235vsbp7")))

(define-public crate-libproc-0.9.1 (c (n "libproc") (v "0.9.1") (d (list (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "139b51wz3pzz14inkgvlg1kbvv12p9wyz4wcncavxlz2zsz51yqm") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.10.0 (c (n "libproc") (v "0.10.0") (d (list (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1w07njh8bkzi0wg6km6vxywjpybf4cj1rgnlpczmcxj2hcgzqrk4") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.11.0 (c (n "libproc") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0av5qmzrgn7aqhgsy2qc6y9nkr7rhqkn9qim7z6mxx84y9l3i86z") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.11.1 (c (n "libproc") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "09k79hhj328gy4gyhg2jpp67vjv0vwxpnqjnd32s3pd1q33q7yl4") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.12.0 (c (n "libproc") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.59.2") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "1myy7jzwx10lh9pl9ggd986w4ir2nqkmdvk7qhafjp6pap8rly8b") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.13.0 (c (n "libproc") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.64.0") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.2.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "procfs") (r "^0.14.1") (d #t) (t "cfg(target_os = \"linux\")") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (t "cfg(target_os = \"linux\")") (k 2)))) (h "0ksv09124xm2n83b7qml9gbpbjqgc6nfkp1bl8mm9wwgkzrcn64b") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.14.0 (c (n "libproc") (v "0.14.0") (d (list (d (n "bindgen") (r "^0.66.1") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "procfs") (r "^0.15.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\"))") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\"))") (k 2)))) (h "18r1d3dk2mk2cfkw8cckkan027s8dra1amsdlinfbl13vmr0nr82") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.14.1 (c (n "libproc") (v "0.14.1") (d (list (d (n "bindgen") (r "^0.66.1") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "procfs") (r "^0.15.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)))) (h "0vgjjpl05kybzd2zjybcndk9w2z5i6j1bzsjmzhrzdq3w3dn3fhg") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.14.2 (c (n "libproc") (v "0.14.2") (d (list (d (n "bindgen") (r "^0.68.1") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "procfs") (r "^0.15.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)))) (h "1vsahlxjx6gwwvvqgaznv53vnanmnqiiagv286pmq7cxpbmh9412") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.14.3 (c (n "libproc") (v "0.14.3") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "procfs") (r "^0.16.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)))) (h "12mi1bqy9s69hx4fp13d0w9aa1s7hypix1xzy3975xji5ycsx9lz") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.14.4 (c (n "libproc") (v "0.14.4") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "procfs") (r "^0.16.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)))) (h "13ivg6n87kpcwn00ril71ynqmsdpbppyr84cimy80gjizirx33cf") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.14.5 (c (n "libproc") (v "0.14.5") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "procfs") (r "^0.16.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)))) (h "0h43h7vdn0dphgrwnzvqczall7wdkyz1b47rrlgvdbk5h8jsi3q6") (f (quote (("macosx_10_9" "macosx_10_7") ("macosx_10_7") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.14.6 (c (n "libproc") (v "0.14.6") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "procfs") (r "^0.16.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)))) (h "0va3jzmgfj57lf0zfq59acfkid3frs6nvx9xlsnc5jd4g1q4kdlf") (f (quote (("macosx_10_9") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.14.7 (c (n "libproc") (v "0.14.7") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "procfs") (r "^0.16.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)))) (h "0h7a9vidcc2x6r7cck6zhqagqk68b09m4alz0d0p1d248060zlqk") (f (quote (("macosx_10_9") ("default" "macosx_10_9"))))))

(define-public crate-libproc-0.14.8 (c (n "libproc") (v "0.14.8") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime"))) (k 1)) (d (n "errno") (r "^0.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "procfs") (r "^0.16.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"redox\", target_os = \"android\"))") (k 2)))) (h "1fw89a419xfciim290hy11qfmjpi3m247zns55a6g08sbsvs97mf") (f (quote (("macosx_10_9") ("default" "macosx_10_9"))))))

