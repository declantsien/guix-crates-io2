(define-module (crates-io li bp libpacket) #:use-module (crates-io))

(define-public crate-libpacket-0.1.0 (c (n "libpacket") (v "0.1.0") (d (list (d (n "libpacket-core") (r "^0.1.0") (d #t) (k 0)) (d (n "libpacket-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1ix9ijg1vqsycjxxmd1j33ykgym98bhfxb05h482pvyc5vjgiw20")))

(define-public crate-libpacket-0.1.1 (c (n "libpacket") (v "0.1.1") (d (list (d (n "libpacket-core") (r "^0.1.0") (d #t) (k 0)) (d (n "libpacket-derive") (r "^0.1.0") (d #t) (k 0)))) (h "0hlqh0fksff4rhl0kj779wsavf7k9ngw2h4s1vdi9wxifvzd8w3h")))

(define-public crate-libpacket-0.1.2 (c (n "libpacket") (v "0.1.2") (d (list (d (n "libpacket-core") (r "^0.1.0") (d #t) (k 0)) (d (n "libpacket-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1n9521sj93p52k57chqfqj6cc5cimjwwnk0pkmg0myz7bwwwqznq")))

