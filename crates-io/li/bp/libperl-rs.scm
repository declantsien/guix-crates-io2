(define-module (crates-io li bp libperl-rs) #:use-module (crates-io))

(define-public crate-libperl-rs-0.1.0 (c (n "libperl-rs") (v "0.1.0") (d (list (d (n "libperl-sys") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0i3lh4wnpzdv2cv80ml2s94x20clw5qiyayy5dpc117lw4xqwh30")))

(define-public crate-libperl-rs-0.2.0 (c (n "libperl-rs") (v "0.2.0") (d (list (d (n "libperl-config") (r "^0.2") (d #t) (k 1)) (d (n "libperl-sys") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0na3zrprc7ypzly7amvw35sfn4hizjm2nw1fq6l5spx40cvs33iz")))

