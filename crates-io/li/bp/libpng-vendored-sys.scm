(define-module (crates-io li bp libpng-vendored-sys) #:use-module (crates-io))

(define-public crate-libpng-vendored-sys-0.1.0 (c (n "libpng-vendored-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpng-src") (r "^0.2") (d #t) (k 1)) (d (n "libz-sys") (r "=1.1.15") (d #t) (k 0)))) (h "18qm291qjjm2gxy13qvp022vwhca7mh8jnh8p6y50k08i883fdpb")))

(define-public crate-libpng-vendored-sys-0.1.1 (c (n "libpng-vendored-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpng-src") (r "^0.2") (d #t) (k 1)) (d (n "libz-sys") (r "=1.1.15") (d #t) (k 0)))) (h "1xlh3by1i4c9dpr4p6pgqv8s6qr0gsan7k1qaq4482ss2ha3n85f") (l "png")))

(define-public crate-libpng-vendored-sys-0.1.2 (c (n "libpng-vendored-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libpng-src") (r "=0.2.3") (d #t) (k 1)) (d (n "libz-sys") (r "=1.1.16") (o #t) (d #t) (k 0)))) (h "1sd386kf3jinlfrmmdwfxhrrdvxa87aw1bw51a71qdvlf88j5wjd") (f (quote (("link-libz-static" "libz-sys/static") ("default" "link-libz")))) (l "png") (s 2) (e (quote (("link-libz" "dep:libz-sys"))))))

