(define-module (crates-io li bp libpt-bintols) #:use-module (crates-io))

(define-public crate-libpt-bintols-0.1.7 (c (n "libpt-bintols") (v "0.1.7") (d (list (d (n "libpt-core") (r "^0.1.0") (d #t) (k 0)) (d (n "libpt-log") (r "^0.1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "08ir1r44l6ipwa77xw3rz2sylky9qg0rsh80n2zxjpfb34cank8l")))

(define-public crate-libpt-bintols-0.3.10 (c (n "libpt-bintols") (v "0.3.10") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "libpt-core") (r "^0.3.10") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.10") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "19jh6g393lzhm3ckm4mf5hj724p5qhlq70dx6np1nw2ddfdvckmg")))

(define-public crate-libpt-bintols-0.3.11-alpha.0 (c (n "libpt-bintols") (v "0.3.11-alpha.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "libpt-core") (r "^0.3.11-alpha.0") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.11-alpha.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0xgwk64ib7s3d2q8izsbvqy0n0zm3n6gdkd84yxsclqamfjnlgfw")))

(define-public crate-libpt-bintols-0.3.11-alpha.1 (c (n "libpt-bintols") (v "0.3.11-alpha.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "libpt-core") (r "^0.3.11-alpha.1") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.11-alpha.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0821nj3d67vdyzsnrd8lfpq3lj73q61r6nr89x4gfb399xmzbqx4")))

(define-public crate-libpt-bintols-0.3.11 (c (n "libpt-bintols") (v "0.3.11") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "libpt-core") (r "^0.3.11") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.11") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0vbc72h4s4317cfps31fqqwz37vq18555p4vw6z8748yfzq2c95r")))

(define-public crate-libpt-bintols-0.4.0-alpha.2 (c (n "libpt-bintols") (v "0.4.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "libpt-core") (r "^0.4.0-alpha.2") (d #t) (k 0)) (d (n "libpt-log") (r "^0.4.0-alpha.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0n3lm1mqi3w8prhpakibx9x5qinhvjqws4aaq1vgd6gzrwjfp9xq")))

(define-public crate-libpt-bintols-0.4.0 (c (n "libpt-bintols") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "libpt-core") (r "^0.4.0-alpha.2") (d #t) (k 0)) (d (n "libpt-log") (r "^0.4.0-alpha.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "1m8yh1w27398nlxxm7i3ydz7s440ri5djm5s16a0dwkq6m37yr7p")))

(define-public crate-libpt-bintols-0.5.0 (c (n "libpt-bintols") (v "0.5.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "libpt-core") (r "^0.4.0") (d #t) (k 0)) (d (n "libpt-log") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0jfghaw9qgcf5g2s8barbfdqlmgww0m7px99s7y48s5p4fssfgz2")))

(define-public crate-libpt-bintols-0.5.1 (c (n "libpt-bintols") (v "0.5.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "libpt-core") (r "^0.4.0") (d #t) (k 0)) (d (n "libpt-log") (r "^0.4.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0dshgn3n6x10aqkjccfkx77m13zk8mjcay3qx56m0y67rl6rfxkz")))

