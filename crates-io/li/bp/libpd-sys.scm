(define-module (crates-io li bp libpd-sys) #:use-module (crates-io))

(define-public crate-libpd-sys-0.1.0 (c (n "libpd-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1mlmc9fili1vjhip96s3nlnm5iyycsxn8pj5j5prbkd6di7rf7gw")))

(define-public crate-libpd-sys-0.1.1 (c (n "libpd-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1ni11vcm6m2d2nvlhkvbj21jczmmfw4dvflk69i9a7zn079ys95j")))

(define-public crate-libpd-sys-0.1.2 (c (n "libpd-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1f84z3wf66dbv7qgp7yyz9kdslzbd3r6y5ya7z05wsrimb80bqw9")))

(define-public crate-libpd-sys-0.1.3 (c (n "libpd-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "1nxazl050mxn930vc3644wwkd197f7slqbrkiv1mz2gpkpk7j7rf")))

(define-public crate-libpd-sys-0.1.4 (c (n "libpd-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "16vx2fdkg9189k53y4rg7c2bp0x6q98avw2zk24dzxa1va70hk92")))

(define-public crate-libpd-sys-0.1.5 (c (n "libpd-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "14adnwwkybzqvi6wnbhsnz4m3kdpgr8myqlqynsnhhqrajnmzzcz")))

(define-public crate-libpd-sys-0.1.6 (c (n "libpd-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "19abqbjc1559m2wz5xph7y1grvvqaghcnv6sbai66hmk2r2505kl")))

(define-public crate-libpd-sys-0.1.7 (c (n "libpd-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0rpvasjh0fjil658mikn8nkxf29al9l223jysf8nsvwk90v76k59")))

(define-public crate-libpd-sys-0.1.8 (c (n "libpd-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0wwck8i49jccnd7vw1imq8x2sdfp14qbrxxj7dp45w6izlw55igz")))

(define-public crate-libpd-sys-0.1.9 (c (n "libpd-sys") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "13shkpbc0nhrhfm2rzh84zawrxh059q5d54zhdxsawcywpvd88qv")))

(define-public crate-libpd-sys-0.2.0 (c (n "libpd-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)))) (h "0fy7bfqcawsv14qny7j8rk70814p9ms5by2pzq6fzyd6hvix31q5")))

(define-public crate-libpd-sys-0.2.1 (c (n "libpd-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1z4065iwwsgxmaca6if7pf8v5jlw7z2s327hwd43cca08xahif5a")))

