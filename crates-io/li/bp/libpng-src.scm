(define-module (crates-io li bp libpng-src) #:use-module (crates-io))

(define-public crate-libpng-src-0.1.0 (c (n "libpng-src") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0a56jsybfbcm8mx7pqqcycgz7a6zpdqvgkfn2fvh47fgcmg1idm2")))

(define-public crate-libpng-src-0.2.0 (c (n "libpng-src") (v "0.2.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1hzmh0wcvb4xykb1dvg5kk9qsi07ran7mp325vd4vgzdp3p0ixk9") (y #t)))

(define-public crate-libpng-src-0.2.1 (c (n "libpng-src") (v "0.2.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "0xyw2c51mlmpf6v285hiqcsjk6a93qc7770w7y8jl6sa93xqhcqh") (y #t)))

(define-public crate-libpng-src-0.2.2 (c (n "libpng-src") (v "0.2.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1hs5abyd9lnqr3badl1pkxfrpdbb0wmbsbcj9y93vn74w59zyhnp")))

(define-public crate-libpng-src-0.2.3 (c (n "libpng-src") (v "0.2.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "02q9pjg6c1wc233gkpdgvyyrnqxbm77l7wb7wabnxbhnchn2b2b1")))

