(define-module (crates-io li bp libpicnic-sys) #:use-module (crates-io))

(define-public crate-libpicnic-sys-0.1.0+3.0.18 (c (n "libpicnic-sys") (v "0.1.0+3.0.18") (d (list (d (n "cc") (r "^1") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "hex") (r "^0.4") (f (quote ("alloc"))) (k 2)) (d (n "libc") (r "^0.2") (k 0)) (d (n "paste") (r "^1") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (o #t) (d #t) (k 1)))) (h "0i7qm4685lwvn1w66qnhv9nkp69znikhkjg2zcfbr9jwsncqcc3w") (f (quote (("unruh-transform" "picnic") ("picnic3") ("picnic") ("default" "system" "vendored" "picnic" "picnic3")))) (l "picnic") (s 2) (e (quote (("vendored" "dep:cc") ("system" "dep:pkg-config") ("param-bindings" "dep:paste")))) (r "1.60")))

