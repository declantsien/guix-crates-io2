(define-module (crates-io li bp libp2p-remux) #:use-module (crates-io))

(define-public crate-libp2p-remux-0.0.0 (c (n "libp2p-remux") (v "0.0.0") (h "1hky812nzalpwvp2dhpr8qvv7hq9hx53asqazpw164h52wvl23l9") (y #t)))

(define-public crate-libp2p-remux-0.30.1 (c (n "libp2p-remux") (v "0.30.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "remux") (r "^0.8.0") (d #t) (k 0)) (d (n "tetsy-libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09qf5806h5bn2r8sc1gfi9mpl616h6qzir7305mjla62q9r5kicz")))

