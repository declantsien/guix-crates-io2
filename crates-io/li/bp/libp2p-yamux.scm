(define-module (crates-io li bp libp2p-yamux) #:use-module (crates-io))

(define-public crate-libp2p-yamux-0.1.0 (c (n "libp2p-yamux") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.1.1") (d #t) (k 0)))) (h "0s0nq1v0ah3rpcvgz6v3azz57g8m81m0g822645yxhlnrasxxv5i")))

(define-public crate-libp2p-yamux-0.2.0 (c (n "libp2p-yamux") (v "0.2.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.1.1") (d #t) (k 0)))) (h "1c87380jwr4s89jwwmmv9zxq09by2xcxfrkk04niaf60l5lzvd3i")))

(define-public crate-libp2p-yamux-0.3.0 (c (n "libp2p-yamux") (v "0.3.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.1.1") (d #t) (k 0)))) (h "1kwdfnpdhcvc0n1jwxc7dcv4qmi5si3fgv3xxj2pl705fgc2x6ny")))

(define-public crate-libp2p-yamux-0.4.0 (c (n "libp2p-yamux") (v "0.4.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.1.1") (d #t) (k 0)))) (h "04wkzragc882hy264vzj5n0s5f87p80gpihilzsn7ja7wrx1k9nm")))

(define-public crate-libp2p-yamux-0.5.0 (c (n "libp2p-yamux") (v "0.5.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.1.9") (d #t) (k 0)))) (h "0v61bmzs06mf0i1dzdnnp0g7cx7f0gq76rb8jyh6yl83qgi24cjn")))

(define-public crate-libp2p-yamux-0.6.0 (c (n "libp2p-yamux") (v "0.6.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.6.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.1.9") (d #t) (k 0)))) (h "1nbqlismamk3j7a8gqq87ycr4vbc0a58cyz74x3rcsjkrc48vfqd")))

(define-public crate-libp2p-yamux-0.7.0 (c (n "libp2p-yamux") (v "0.7.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.7.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.2.0") (d #t) (k 0)))) (h "0lmg189ijcbqzi7dn796373cbwydan2fvwn9w77blmi0bcdgavsy")))

(define-public crate-libp2p-yamux-0.8.0 (c (n "libp2p-yamux") (v "0.8.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.8.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.2.0") (d #t) (k 0)))) (h "1f69hn4ngyp1v3ihnwg69j98s7zk81i7pwvkprz4xa5yb59icsak")))

(define-public crate-libp2p-yamux-0.9.0 (c (n "libp2p-yamux") (v "0.9.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.2.1") (d #t) (k 0)))) (h "1xi44dsz0rz3nvmxhcqfjxmpgmixa486zrfv08jn5k97y5zzqjxz")))

(define-public crate-libp2p-yamux-0.10.0 (c (n "libp2p-yamux") (v "0.10.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.2.1") (d #t) (k 0)))) (h "1bgsll2lmarjkj1m3p03xdi43fc6kksq99wmfm9shajp3prz2ywh")))

(define-public crate-libp2p-yamux-0.11.0 (c (n "libp2p-yamux") (v "0.11.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.11.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.2.1") (d #t) (k 0)))) (h "1rmzm9085jdjyalrqx097igvki0yidhlv3ilcv4h2ysw0afvh0qw")))

(define-public crate-libp2p-yamux-0.12.0 (c (n "libp2p-yamux") (v "0.12.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.12.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.2.1") (d #t) (k 0)))) (h "05fapynaq0rcgxgpw5dld6dckkmilqx703drvznfw37fr03ysyx3")))

(define-public crate-libp2p-yamux-0.13.0 (c (n "libp2p-yamux") (v "0.13.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.13.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "yamux") (r "^0.2.1") (d #t) (k 0)))) (h "00qhgrjvcx79y22vkjbvp26rc8b9dwlp05vggfaiamgbsryyn4qr")))

(define-public crate-libp2p-yamux-0.14.0-alpha.1 (c (n "libp2p-yamux") (v "0.14.0-alpha.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.14.0-alpha.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4") (d #t) (k 0)))) (h "1y9grz15z0w1a385kf8q7aakl3pbrgiwnp8i1jns1hy7zzm7fcpg")))

(define-public crate-libp2p-yamux-0.15.0 (c (n "libp2p-yamux") (v "0.15.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.15.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4") (d #t) (k 0)))) (h "11idsq1n41gh6ihvzqbayab6zhfgsfmj4xjn58yck8vqmhx5p8hc")))

(define-public crate-libp2p-yamux-0.16.0 (c (n "libp2p-yamux") (v "0.16.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4.2") (d #t) (k 0)))) (h "0zsbibkzk4x1hr7zcqs82rb79z5fzr4ql409xbmcca9w4yksaapp")))

(define-public crate-libp2p-yamux-0.16.2 (c (n "libp2p-yamux") (v "0.16.2") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.16.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4.2") (d #t) (k 0)))) (h "1w9liav89c798x4ncgshi5jvwysqbh91mmv3ydkhxpnc4kg31b4x")))

(define-public crate-libp2p-yamux-0.17.0 (c (n "libp2p-yamux") (v "0.17.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.17.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4.5") (d #t) (k 0)))) (h "1fh1kkdkq4ivvavnnf2gls79l52pbywm2rgv3zw0hyaj4sac84pf")))

(define-public crate-libp2p-yamux-0.18.0 (c (n "libp2p-yamux") (v "0.18.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.18.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4.5") (d #t) (k 0)))) (h "11xbp5w7wq5zwklr9p98xi0zs2i7kc7dqn60qq5iwmzna3m1my82")))

(define-public crate-libp2p-yamux-0.19.0 (c (n "libp2p-yamux") (v "0.19.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.19.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4.5") (d #t) (k 0)))) (h "1chknkdb5db2wkwq7hnpw2aswby5sl8jszz1q08qzrl1i4x5sc0b")))

(define-public crate-libp2p-yamux-0.19.1 (c (n "libp2p-yamux") (v "0.19.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.19.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4.5") (d #t) (k 0)))) (h "1z665ah5j562vd6d36zkx65s8pwz48g8s2zvi9m5ris9bxxkx8wd")))

(define-public crate-libp2p-yamux-0.20.0 (c (n "libp2p-yamux") (v "0.20.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.20.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4.5") (d #t) (k 0)))) (h "16h2f6aqs7zq4a7hjsfn35jdc6ywx6fxyqcvgsfbx96qyzr9pbj6")))

(define-public crate-libp2p-yamux-0.21.0 (c (n "libp2p-yamux") (v "0.21.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.21.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.4.5") (d #t) (k 0)))) (h "1al86y9hbwh7f2ykj08b7c7l3bsw4hj3jqh4yfvfyc759jnrx5mk")))

(define-public crate-libp2p-yamux-0.22.0 (c (n "libp2p-yamux") (v "0.22.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.22.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.5.0") (d #t) (k 0)))) (h "16szvkycva83las7wnkdnix5sn0c7gza5zdp07m1hi49kj5kqni3")))

(define-public crate-libp2p-yamux-0.23.0 (c (n "libp2p-yamux") (v "0.23.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.22.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.6.0") (d #t) (k 0)))) (h "0v6ssrs5d958q1kkcszi4vn33dh70q5y23ni4m00nyysnlggavql")))

(define-public crate-libp2p-yamux-0.24.0 (c (n "libp2p-yamux") (v "0.24.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.22.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.7.0") (d #t) (k 0)))) (h "0h8f3m91vca6mss1hl7nkbbghr4dfhalwk20ymm4s3idsg8y8mjw")))

(define-public crate-libp2p-yamux-0.25.0 (c (n "libp2p-yamux") (v "0.25.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.22.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.0") (d #t) (k 0)))) (h "1z9kdzgjy1fg5p5cv5jdzm4vi5l56qjp30200v2apk9x0jgrn7bq")))

(define-public crate-libp2p-yamux-0.26.0 (c (n "libp2p-yamux") (v "0.26.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.23.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.0") (d #t) (k 0)))) (h "043ikifmrd0f76nc6v70mychambpdc5hn5slx0m8q5ksxyvckh07")))

(define-public crate-libp2p-yamux-0.27.0 (c (n "libp2p-yamux") (v "0.27.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.24.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.0") (d #t) (k 0)))) (h "0j4kalgp1pz0ipjncjbfl91aqvphkb89714580n1cdc5np5rh1qs")))

(define-public crate-libp2p-yamux-0.28.0 (c (n "libp2p-yamux") (v "0.28.0") (d (list (d (n "futures") (r ">=0.3.1, <0.4.0") (d #t) (k 0)) (d (n "libp2p-core") (r ">=0.25.0, <0.26.0") (d #t) (k 0)) (d (n "parking_lot") (r ">=0.11.0, <0.12.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "yamux") (r ">=0.8.0, <0.9.0") (d #t) (k 0)))) (h "1xp5n3scip08ddp4wjpnzzwqxb1lvr48wrbfkb02mr531w0arrrv")))

(define-public crate-libp2p-yamux-0.29.0 (c (n "libp2p-yamux") (v "0.29.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.26.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.0") (d #t) (k 0)))) (h "1w5cbk6zqgl8z3wpry6yl2a0z1yyr3dysch010pphd9c17lrsy9n")))

(define-public crate-libp2p-yamux-0.30.0 (c (n "libp2p-yamux") (v "0.30.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.0") (d #t) (k 0)))) (h "1b8224vql5lj53f6z5gzrcbl3apz9g8s7dpi5lhkbzj0zhkqn2s9")))

(define-public crate-libp2p-yamux-0.30.1 (c (n "libp2p-yamux") (v "0.30.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.27.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.1") (d #t) (k 0)))) (h "0ai3zjdvf1apdxl1mc3rf13szpcl9gxiwsdfysazz1iaaj63a6a8")))

(define-public crate-libp2p-yamux-0.31.0 (c (n "libp2p-yamux") (v "0.31.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.28.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.8.1") (d #t) (k 0)))) (h "0yrvk7aj8m3342cxdldwd20q18ijrkxw5ryiil5gnhs1r5619mln")))

(define-public crate-libp2p-yamux-0.32.0 (c (n "libp2p-yamux") (v "0.32.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.28.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.9.0") (d #t) (k 0)))) (h "1i5b7v8s9frzwc4mwrcqizrwsinid2y82s5dygfb0p3dzhna8pgk")))

(define-public crate-libp2p-yamux-0.33.0 (c (n "libp2p-yamux") (v "0.33.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.29.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.9.0") (d #t) (k 0)))) (h "1gs4s0nb9d76w5m9spmym29xpbxv846bl7mvy0kyvjrpkkfw0k11")))

(define-public crate-libp2p-yamux-0.34.0-rc.1 (c (n "libp2p-yamux") (v "0.34.0-rc.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.30.0-rc.1") (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.9.0") (d #t) (k 0)))) (h "0lbky06fsf8ww0im91jc33rxx5ai85k94kjc2978ayii14arnmpj")))

(define-public crate-libp2p-yamux-0.34.0 (c (n "libp2p-yamux") (v "0.34.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.30.0") (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.9.0") (d #t) (k 0)))) (h "1bw9d1zhan8jvibsrj65gyclqm8d8rzz2gg9wy3p2zc6p2mn4wsf")))

(define-public crate-libp2p-yamux-0.35.0 (c (n "libp2p-yamux") (v "0.35.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.31.0") (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "0di0bvjdbi8ahzv2pix19n32skfcwrqz6k4pbaf9zlkh0v716g85") (r "1.56.1")))

(define-public crate-libp2p-yamux-0.36.0 (c (n "libp2p-yamux") (v "0.36.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.32.0") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "05vm5klldl1p98zn33yi2q6zr3534rvi12cyx0hd0g0ri6yjx45y") (r "1.56.1")))

(define-public crate-libp2p-yamux-0.37.0 (c (n "libp2p-yamux") (v "0.37.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.33.0") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "07rqdxfcw9ii5pnlxpmd46icmx6brfqfn3vjb73pfj6pk9im7rlg") (r "1.56.1")))

(define-public crate-libp2p-yamux-0.38.0 (c (n "libp2p-yamux") (v "0.38.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.34.0") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "0758qw8x69rfljplykkqmpvf4418jcqmc0nw6ch0f1ks463adpn6") (r "1.56.1")))

(define-public crate-libp2p-yamux-0.39.0 (c (n "libp2p-yamux") (v "0.39.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.35.0") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "1simzkd32vnwnqvdrp1p6dlbrc0xmvc4s62rknl7miphy43c278x") (r "1.56.1")))

(define-public crate-libp2p-yamux-0.40.0 (c (n "libp2p-yamux") (v "0.40.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.36.0") (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "0k39aw9c8wkn9kkrx9ygn7jc2x5kycbjkmck5c5kyn1b0kfchkmp") (r "1.56.1")))

(define-public crate-libp2p-yamux-0.41.0 (c (n "libp2p-yamux") (v "0.41.0") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.37.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "0ic81ni3lqj6zfpjcfy1r2429w4609in150kr1zh3b91g84pkw1h") (r "1.56.1")))

(define-public crate-libp2p-yamux-0.41.1 (c (n "libp2p-yamux") (v "0.41.1") (d (list (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.37.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "04kc1my7z4n78gyzmd1sl5iiadddpskak31vdviggi23cpb78s0d") (r "1.56.1")))

(define-public crate-libp2p-yamux-0.42.0 (c (n "libp2p-yamux") (v "0.42.0") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.38.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "0acajb6va5ily4k3d2phadr8flshjj8wf5a9klnn86581955jqsg") (r "1.60.0")))

(define-public crate-libp2p-yamux-0.43.0-alpha (c (n "libp2p-yamux") (v "0.43.0-alpha") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.1") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.38.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.11.0") (d #t) (k 0)))) (h "0v2fp8bhxfzx9h6vmja65xmv0df570674xwxr6qmx5rbxxgfsvhx") (r "1.60.0")))

(define-public crate-libp2p-yamux-0.43.0 (c (n "libp2p-yamux") (v "0.43.0") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.26") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.39.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "1zfpa8wy5kzh7sdmgls9lqixbjd8is09pvm1b9jh121cyy1csj6h") (r "1.60.0")))

(define-public crate-libp2p-yamux-0.43.1 (c (n "libp2p-yamux") (v "0.43.1") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.39.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "1niva7pj48jj33bwss3hx5wdfm2z5y7ddinlhnih09v6a3cj3kad") (r "1.60.0")))

(define-public crate-libp2p-yamux-0.44.0 (c (n "libp2p-yamux") (v "0.44.0") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.40.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.10.0") (d #t) (k 0)))) (h "1c47aah1545y29my38jm1fhrkn28bzf13yyqfvqcc5fynqmb9af0") (r "1.65.0")))

(define-public crate-libp2p-yamux-0.44.1 (c (n "libp2p-yamux") (v "0.44.1") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.40.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "yamux") (r "^0.12") (d #t) (k 0)))) (h "11bazmll9925pvd3d2z6p6bs0g5gxnkf5m6gp7ph0hsch9icpvcf") (r "1.65.0")))

(define-public crate-libp2p-yamux-0.45.0 (c (n "libp2p-yamux") (v "0.45.0") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.41.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "yamux") (r "^0.12") (d #t) (k 0)))) (h "1s5a4f63dj1r1rlc46hcy33zwqs493sffl94rwfdphqvyxw4f7vm") (r "1.73.0")))

(define-public crate-libp2p-yamux-0.45.1 (c (n "libp2p-yamux") (v "0.45.1") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "either") (r "^1") (d #t) (k 0)) (d (n "futures") (r "^0.3.29") (d #t) (k 0)) (d (n "libp2p-core") (r "^0.41.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "yamux012") (r "^0.12.1") (d #t) (k 0) (p "yamux")) (d (n "yamux013") (r "^0.13.1") (d #t) (k 0) (p "yamux")))) (h "1rbi9d1n6jnjz0ddwn9r1af9pmxyfwfl62ym4w4pci4s6i8bw310") (r "1.73.0")))

