(define-module (crates-io li bp libpacstall) #:use-module (crates-io))

(define-public crate-libpacstall-0.1.0 (c (n "libpacstall") (v "0.1.0") (d (list (d (n "figment") (r "^0.10.6") (f (quote ("env" "test" "toml"))) (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "rstest") (r "^0.15.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)))) (h "0v85ppmyayiws3cfnqc7p11dycgv805nklz21jnw3p7idqi2mk60")))

