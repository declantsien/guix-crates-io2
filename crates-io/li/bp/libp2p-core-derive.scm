(define-module (crates-io li bp libp2p-core-derive) #:use-module (crates-io))

(define-public crate-libp2p-core-derive-0.1.0 (c (n "libp2p-core-derive") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "0ci3pa083bh9iinpcnm0mmr4j8qq6mhrycydarfwm4i8gk5a8lik")))

(define-public crate-libp2p-core-derive-0.2.0 (c (n "libp2p-core-derive") (v "0.2.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "1aky9cp3fjq7q0nmzjl80bmq598dkziq1g2lf384bal7y5i24ap5")))

(define-public crate-libp2p-core-derive-0.2.1 (c (n "libp2p-core-derive") (v "0.2.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "129h29wsp972c5vl2d8731ddg2bc8v05dr2v4kk1r34sigmh8kfr")))

(define-public crate-libp2p-core-derive-0.2.2 (c (n "libp2p-core-derive") (v "0.2.2") (d (list (d (n "libp2p") (r "^0.2.2") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "1smk2h21yviijm4amidyc5a154im1ri0zz74gi03y3znydvg124a")))

(define-public crate-libp2p-core-derive-0.3.0 (c (n "libp2p-core-derive") (v "0.3.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "1xjy3nyqlqk1abp9wh0x3h53j07kac623appnjc4g87kvvnijjjd")))

(define-public crate-libp2p-core-derive-0.3.1 (c (n "libp2p-core-derive") (v "0.3.1") (d (list (d (n "libp2p") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "14k0pscxp0x851dadxv2v411jsdnyz2mlkvkc3q0wr53012d23h4")))

(define-public crate-libp2p-core-derive-0.3.2 (c (n "libp2p-core-derive") (v "0.3.2") (d (list (d (n "libp2p") (r "^0.3.0") (d #t) (k 2)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "1gm6crd6icfq731c1lqbwwyhfxdxqmd7aiypc38l11admb43gmi1")))

(define-public crate-libp2p-core-derive-0.4.0 (c (n "libp2p-core-derive") (v "0.4.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "1lyqcx4c9pjvhsj3mr0m9m74fm2yf4l0v7gz38gf82wxcfxz77sy")))

(define-public crate-libp2p-core-derive-0.5.0 (c (n "libp2p-core-derive") (v "0.5.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "16ymvrbl8izm56my8lgvgmgh5zx0hav0643977jprh4kr9sv82p0")))

(define-public crate-libp2p-core-derive-0.6.0 (c (n "libp2p-core-derive") (v "0.6.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "11ljqvkv70qh3jgdyjzi4026lk1ciwinvgc2f2pzvsax7n5aigny")))

(define-public crate-libp2p-core-derive-0.7.0 (c (n "libp2p-core-derive") (v "0.7.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "1cc1dydza2wnrcdfb4wwi5112nyfryyzsaxh23cvw3387c85yxiz")))

(define-public crate-libp2p-core-derive-0.8.0 (c (n "libp2p-core-derive") (v "0.8.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "04mbl159lrjcmbppbdjw5nk1kgk3gf4x6x0sjf4lvpzf7magbxpl")))

(define-public crate-libp2p-core-derive-0.9.0 (c (n "libp2p-core-derive") (v "0.9.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "0rkhfbrr7q67a6mn10rv7hj019w8vsbcxjp0g9vrcv5nmbf5j6l5")))

(define-public crate-libp2p-core-derive-0.10.0 (c (n "libp2p-core-derive") (v "0.10.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)) (d (n "void") (r "^1.0") (d #t) (k 2)))) (h "0ay9p4cgwyq17ghprfk6j288raknfxi6v37xij8bxx1kd7xg0v8y")))

(define-public crate-libp2p-core-derive-0.11.0 (c (n "libp2p-core-derive") (v "0.11.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1h6lmn239bvcyal770aggsh34v0ijhw0vzxbjrhvf9x7vdgf8kwm")))

(define-public crate-libp2p-core-derive-0.12.0 (c (n "libp2p-core-derive") (v "0.12.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "09yv0x64fx5x5hslh1dh0sd02lki00wnbkgbwlbvg5dcgr9b7zxs")))

(define-public crate-libp2p-core-derive-0.13.0 (c (n "libp2p-core-derive") (v "0.13.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15.22") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0vw22986c800a8243ny6bxm54j5q53ciwdbyjqqhzihlmh22gsqy")))

(define-public crate-libp2p-core-derive-0.14.0-alpha.1 (c (n "libp2p-core-derive") (v "0.14.0-alpha.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0hw9va09kavgyv0hbrrx516sgxcbngkg1mdkf5in60aphl0xlx5n")))

(define-public crate-libp2p-core-derive-0.15.0 (c (n "libp2p-core-derive") (v "0.15.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0kh0rydkqf8wnj9jsqfkfhnlp143vfn3bml46ki60xqaprk04ckc")))

(define-public crate-libp2p-core-derive-0.16.0 (c (n "libp2p-core-derive") (v "0.16.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "16za2mdq58czhrgwl6gh217fhywm1ghiv03pbj08ix92splp5m4n")))

(define-public crate-libp2p-core-derive-0.17.0 (c (n "libp2p-core-derive") (v "0.17.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "18v9l03ry4ydq3j0zsai83f743g17p6d3ispmip84ahmbxfv5vm0")))

(define-public crate-libp2p-core-derive-0.18.0 (c (n "libp2p-core-derive") (v "0.18.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1rp2b9la9ly75q1fv47izljr769a6npdacqcnrddna27is2jg49j")))

(define-public crate-libp2p-core-derive-0.19.0 (c (n "libp2p-core-derive") (v "0.19.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0cvh9nd3wm9rd56aly3752j8cvphw40p2dqks6k5gm75pqaxkw37")))

(define-public crate-libp2p-core-derive-0.19.1 (c (n "libp2p-core-derive") (v "0.19.1") (d (list (d (n "libp2p") (r "^0.19.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0w9ib0f5cdps54k4vz5rp2apf4g1dkh9brgxh10dczkkddi4i5gh")))

(define-public crate-libp2p-core-derive-0.20.0 (c (n "libp2p-core-derive") (v "0.20.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0rhzfqfv8ahqadh3hg4bqs39dazbmaf6ciiansh50r2r9rxlybsb")))

(define-public crate-libp2p-core-derive-0.20.1 (c (n "libp2p-core-derive") (v "0.20.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "1szcawp5376fka75x72wwd667mdxjwwq1vfkisw1ncjxp9y4lp2i")))

(define-public crate-libp2p-core-derive-0.20.2 (c (n "libp2p-core-derive") (v "0.20.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.8") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "04r2s9d3m9lp8jlg4srx2nag51ww4q6wva5q0jzi9v6k9hrdjlzp")))

(define-public crate-libp2p-core-derive-0.21.0 (c (n "libp2p-core-derive") (v "0.21.0") (d (list (d (n "quote") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "syn") (r ">=1.0.8, <2.0.0") (f (quote ("clone-impls" "derive" "parsing" "printing" "proc-macro"))) (k 0)))) (h "0mh1pq7v369ac8vmjwa2fwilg8g1y1zwblrfs8w23r2n66a41g7l")))

