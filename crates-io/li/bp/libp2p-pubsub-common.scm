(define-module (crates-io li bp libp2p-pubsub-common) #:use-module (crates-io))

(define-public crate-libp2p-pubsub-common-0.1.0 (c (n "libp2p-pubsub-common") (v "0.1.0") (d (list (d (n "assert_matches") (r "^1.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-ticker") (r "^0.0.3") (d #t) (k 0)) (d (n "hashlink") (r "^0.8.4") (d #t) (k 0)) (d (n "instant") (r "^0.1.12") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 2)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt" "time" "macros"))) (d #t) (k 2)))) (h "0z82xc474qlgvkiccf3h71qinbzldr7mfwa4crhcy7i8nrvqnixx") (r "1.66.0")))

