(define-module (crates-io li bp libpassgen) #:use-module (crates-io))

(define-public crate-libpassgen-1.0.0 (c (n "libpassgen") (v "1.0.0") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0nvk0h7kvh6kwyq80r05558npkrijlicmqcz6d8rzpsh2n81hkna")))

(define-public crate-libpassgen-1.0.1 (c (n "libpassgen") (v "1.0.1") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "1m3vk1jv5dqm1m39s3lyx7gibzjabryjfpl8qcmxycrkay6n7sk7")))

(define-public crate-libpassgen-1.0.2 (c (n "libpassgen") (v "1.0.2") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0db5y87z2pl5s461jpjrq9sc9dfn5kgga5mm1ikvys8xzgvcn2zg")))

(define-public crate-libpassgen-1.0.3 (c (n "libpassgen") (v "1.0.3") (d (list (d (n "indexmap") (r "^1.6") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0gbx868r3zw52fbr8y4w1mkll3iml60slhxl0h64bpypbb64qrcf")))

