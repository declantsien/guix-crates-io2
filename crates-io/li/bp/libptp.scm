(define-module (crates-io li bp libptp) #:use-module (crates-io))

(define-public crate-libptp-0.5.0 (c (n "libptp") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusb") (r "^0.4.0") (d #t) (k 0)))) (h "1hpdnd4z34vv0ilisw7x1fmrqi911k9p6i957zm2aaklg8j2k8h6")))

(define-public crate-libptp-0.6.0 (c (n "libptp") (v "0.6.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusb") (r "^0.5.0") (d #t) (k 0)))) (h "109qj14awgn90zg7l2inyr1q91jmvrylav7dxd5mpgmpgmc071g1")))

(define-public crate-libptp-0.6.1 (c (n "libptp") (v "0.6.1") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusb") (r "^0.5.1") (d #t) (k 0)))) (h "03zzn4yn5d0p9dix669lsq375l5v0fxc51nqd4c5l7xwj81q6awj")))

(define-public crate-libptp-0.6.2 (c (n "libptp") (v "0.6.2") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusb") (r "^0.6") (d #t) (k 0)))) (h "17h7170j7448kqjnzbbvsb44xbfjyqxx5k7ska092py9xmfh966i")))

(define-public crate-libptp-0.6.3 (c (n "libptp") (v "0.6.3") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusb") (r "^0.7") (d #t) (k 0)))) (h "1brz5s0fimhw4q7m11k968z38w2n2apjh1g9xkri55j8j1lgzqkx")))

(define-public crate-libptp-0.6.4 (c (n "libptp") (v "0.6.4") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusb") (r "^0.8") (d #t) (k 0)))) (h "0brsyanrb2rvyr38xk1sciymdmmwj2xr1ky85pzfvh4illpy12qh")))

(define-public crate-libptp-0.6.5 (c (n "libptp") (v "0.6.5") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "1qn4rj2zqdlnsvzs00yajmm5khkd74fadkkbnfnw6ycm5n188srf")))

