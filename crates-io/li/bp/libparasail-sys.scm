(define-module (crates-io li bp libparasail-sys) #:use-module (crates-io))

(define-public crate-libparasail-sys-0.1.0 (c (n "libparasail-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1cq493mw70n69fb5mz0jyddap0lihkk4d0cjl7vghyl7qa5g0l8l") (l "parasail")))

(define-public crate-libparasail-sys-0.1.1 (c (n "libparasail-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "09zvigh2z2bdipfcam5f6xisywyn2gdkk1jsw42bvmx8vz2mpr32") (l "parasail")))

(define-public crate-libparasail-sys-0.1.2 (c (n "libparasail-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "06ficim7y8rmm3q734da896z08yg4hlsz32ilh9daiv4n0m0a7l8") (l "parasail")))

(define-public crate-libparasail-sys-0.1.3 (c (n "libparasail-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "05yrhjd4980pwsq5aaihwr1w32id83623xv93pd860fymsgdsz11") (l "parasail")))

(define-public crate-libparasail-sys-0.1.4 (c (n "libparasail-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1cd84qb5mfhn9q0g4s9vl65qpgvizfcarrh82c3xvlzvs0nxms9r") (l "parasail")))

(define-public crate-libparasail-sys-0.1.5 (c (n "libparasail-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "1xk48ibqr4644ny6h4y0vk9fa73gh1h07x392g81ghyy1b3hzjx0") (l "parasail")))

(define-public crate-libparasail-sys-0.1.6 (c (n "libparasail-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.30") (d #t) (k 1)))) (h "13whwwz1n0bx333gwnp25whzz1vx26s93mamwppq8bf9ar32gzqr") (l "parasail")))

