(define-module (crates-io li bp libpm) #:use-module (crates-io))

(define-public crate-libpm-0.1.0 (c (n "libpm") (v "0.1.0") (d (list (d (n "flate2") (r "^0.2") (d #t) (k 0)) (d (n "git2") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "pgetopts") (r "^0.1") (d #t) (k 0)) (d (n "rpf") (r "^0.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^0.1") (d #t) (k 0)))) (h "1w93xhj1fslwjgck289f2wlhjfgcw2rmqkx0nc328zirc4y1xam3")))

