(define-module (crates-io li bp libphantomfs) #:use-module (crates-io))

(define-public crate-libphantomfs-0.1.0 (c (n "libphantomfs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (f (quote ("derive" "min_const_generics" "zeroable_maybe_uninit" "extern_crate_alloc"))) (d #t) (k 0)) (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "fake-enum") (r "^0.1.4") (d #t) (k 0)) (d (n "nonzero_ext") (r "^0.3.0") (d #t) (k 0)))) (h "0yjqrslrpr0xm44yf31x4pwixwhgl4477pl49b4dmb6j6myj5rc6") (f (quote (("std" "bytemuck/extern_crate_std")))) (r "1.59")))

