(define-module (crates-io li bp libplist) #:use-module (crates-io))

(define-public crate-libplist-0.1.0 (c (n "libplist") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "11lajv7d51w8bvhlyx8mqbh7slk92rym606pa8sfkx06gr5jmd0f")))

