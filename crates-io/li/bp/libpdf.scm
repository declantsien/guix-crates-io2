(define-module (crates-io li bp libpdf) #:use-module (crates-io))

(define-public crate-libpdf-0.1.0 (c (n "libpdf") (v "0.1.0") (h "1fpf4g6qjpb80nl7hkan7rrnhq8p6g19xlhg5m7d0n092h89lifk")))

(define-public crate-libpdf-0.2.0 (c (n "libpdf") (v "0.2.0") (h "1m6dglwlc5s6p94ld6hpd9jlb6i0w4j85k5vr97flz1q1lzl9347")))

(define-public crate-libpdf-0.3.0 (c (n "libpdf") (v "0.3.0") (h "0wd5rlwd1aqd6xlpambkjjwcvm2xl361hx15rmnqrxx1i5cvfcas")))

