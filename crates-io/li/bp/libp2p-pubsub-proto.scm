(define-module (crates-io li bp libp2p-pubsub-proto) #:use-module (crates-io))

(define-public crate-libp2p-pubsub-proto-0.1.0 (c (n "libp2p-pubsub-proto") (v "0.1.0") (d (list (d (n "prost") (r "^0.12.0") (d #t) (k 0)))) (h "1cf8dihfnz8lb44y3j7lrj3z5ggin25y7w08h5xarni3rrvg85k1") (f (quote (("proto-gen")))) (r "1.66.0")))

