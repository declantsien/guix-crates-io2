(define-module (crates-io li bp libp2p-combined-transport) #:use-module (crates-io))

(define-public crate-libp2p-combined-transport-0.1.0 (c (n "libp2p-combined-transport") (v "0.1.0") (d (list (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "libp2p") (r "^0.39.1") (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "17iirc2pd0p9rvpq59648585zgw4qssg50hb89m337rgj58vv4bh")))

(define-public crate-libp2p-combined-transport-0.1.1 (c (n "libp2p-combined-transport") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "libp2p") (r "^0.39.1") (k 0)) (d (n "libp2p") (r "^0.39.1") (f (quote ("tcp-tokio" "websocket" "ping"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1h5hag60cimzammgjf4jdcnb76kr04x077ipqg35cvmq1l1r3fiq")))

(define-public crate-libp2p-combined-transport-0.1.2 (c (n "libp2p-combined-transport") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 2)) (d (n "futures") (r "^0.3.17") (d #t) (k 0)) (d (n "libp2p") (r "^0.39.1") (k 0)) (d (n "libp2p") (r "^0.39.1") (f (quote ("tcp-tokio" "websocket" "ping"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.11.2") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1h22cc9ibqzmb66iiizsxnvlvr8cxb1w0y5yjrrvpwn7133pf7bi")))

