(define-module (crates-io li bp libpq-sys) #:use-module (crates-io))

(define-public crate-libpq-sys-0.5.0 (c (n "libpq-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.0") (o #t) (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0drx91979vng1fsj22ds30cnbfzbsm8r2z4kl9i688vmqfw91vyq") (l "pq")))

(define-public crate-libpq-sys-0.6.0 (c (n "libpq-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1c0v7q6fjjf9hy81z50fajklv7grkabs3dh0qnh4fj3k897bmxmv") (l "pq")))

(define-public crate-libpq-sys-0.7.0 (c (n "libpq-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0vzmbk3mpc0ywf0hr35l5rcnjp1k952abz70f1v3kw5zcbbbm6my") (l "pq")))

(define-public crate-libpq-sys-0.8.0 (c (n "libpq-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "08wamqc9rgl63k4s61jcvgh851qc20lvckjzl5fwh1y20nn61w1f") (l "pq")))

