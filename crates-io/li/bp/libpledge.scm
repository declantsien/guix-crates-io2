(define-module (crates-io li bp libpledge) #:use-module (crates-io))

(define-public crate-libpledge-0.1.0 (c (n "libpledge") (v "0.1.0") (d (list (d (n "bpfvm") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.135") (f (quote ("extra_traits"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 2)) (d (n "nix") (r "^0.25.0") (d #t) (k 2)) (d (n "test-log") (r "^0.2.11") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)))) (h "1v91943072im5ivzbxilc1djh2fxwhw574gd8y2wsflfgh78i713")))

