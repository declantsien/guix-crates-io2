(define-module (crates-io li bp libpcap-rs) #:use-module (crates-io))

(define-public crate-libpcap-rs-0.1.1 (c (n "libpcap-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jkcenum") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0ipzvbkr9wybp48fc07zih4687hjqgg63bvrj2cp6hyg1b3i2b72") (r "1.73")))

(define-public crate-libpcap-rs-0.2.0 (c (n "libpcap-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jkcenum") (r "^0.2.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jppe") (r "^0.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "01cdnaji89a7ah2y8vz3jyjalqqynvky6nj982zsvqpmhs8d96lw") (r "1.56")))

(define-public crate-libpcap-rs-0.2.1 (c (n "libpcap-rs") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "hex") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "jkcenum") (r "^0.2.1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "jppe") (r "^1.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "signal-hook") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0skvyjg9yjsgffdq5xppzgxc9lvkc2z8m3367a7b1njmspnhxwm2") (r "1.56")))

