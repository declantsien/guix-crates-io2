(define-module (crates-io li bp libphonenumber-sys) #:use-module (crates-io))

(define-public crate-libphonenumber-sys-0.1.0 (c (n "libphonenumber-sys") (v "0.1.0") (d (list (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1s1qf6vaimj4g7wcfaa302lk93c1v3prj685w20ns09zjh5ichdz")))

(define-public crate-libphonenumber-sys-0.1.1 (c (n "libphonenumber-sys") (v "0.1.1") (d (list (d (n "cpp_utils") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jznv8n5h4cf7x8qys4zij6f8d8gb3pfdl5w4gs6bhrg2zqgnci1")))

