(define-module (crates-io li bp libpobsd) #:use-module (crates-io))

(define-public crate-libpobsd-0.1.0 (c (n "libpobsd") (v "0.1.0") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0r6y0pp3glbcira9s3l3qncvx9vd8mqdg9kb0c1r1qqkbfllmgg8") (r "1.68.0")))

(define-public crate-libpobsd-0.1.1 (c (n "libpobsd") (v "0.1.1") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1ryzk34d2inwnp0is67xwb9ar725hwadcwy6b55fnjza861p0fmj") (r "1.68.0")))

(define-public crate-libpobsd-0.1.2 (c (n "libpobsd") (v "0.1.2") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ks0w3x3rnr5avkm27phbg1j72v9vmkzdlxbmmgf22vpab5p9iwm") (r "1.68.0")))

(define-public crate-libpobsd-0.1.3 (c (n "libpobsd") (v "0.1.3") (d (list (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0ia37xlq9vc93m2arjdv5rh411lpbzr6wm5ym1kriknqc3hdqwq2") (r "1.68.0")))

(define-public crate-libpobsd-0.2.0 (c (n "libpobsd") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.35") (f (quote ("serde"))) (d #t) (k 0)) (d (n "hash32") (r "^0.3.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.3") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "02ph3367dvxqknyhamnfw8g3abqdp3zi6ljq0q9sxn92lrid262c") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde")))) (r "1.68.0")))

