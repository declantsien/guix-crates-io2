(define-module (crates-io li bp libpwquality-sys) #:use-module (crates-io))

(define-public crate-libpwquality-sys-0.1.0 (c (n "libpwquality-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1s548vvx0ak5lfb0y92p5krqa1p6hmr9jzcjsbkgqmcq3d22sak1") (f (quote (("vendored") ("default" "vendored") ("crack")))) (l "pwquality")))

(define-public crate-libpwquality-sys-0.2.0 (c (n "libpwquality-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1myzq9b70d6fscd09xz27jrd3dhkz3x4fm4xxns8kjwjxc98m9rv") (f (quote (("crack")))) (l "pwquality")))

(define-public crate-libpwquality-sys-0.2.1 (c (n "libpwquality-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1aknkf84685igg0zxr4ycvq8c3kc880vj812vhw9hpn1z475km7y") (f (quote (("crack")))) (l "pwquality")))

(define-public crate-libpwquality-sys-0.3.0 (c (n "libpwquality-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0yq8cm9gg62gai3p10w47zb4w1vcwcypbdp2pqdgcfjkv5jbkb49") (f (quote (("vendored")))) (l "pwquality")))

