(define-module (crates-io li bp libpt-log) #:use-module (crates-io))

(define-public crate-libpt-log-0.1.7 (c (n "libpt-log") (v "0.1.7") (d (list (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "pyo3") (r "^0.19") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0i3rk7z60i6mdcj8a0pcnf2blp8f1kshndzaic5nk4mqpr9w74xg")))

(define-public crate-libpt-log-0.3.10 (c (n "libpt-log") (v "0.3.10") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0g2ban84q2zy77z2xd7kg9cg21ipz7lkhrys9ag6478i4w23ikw4")))

(define-public crate-libpt-log-0.3.11-alpha.0 (c (n "libpt-log") (v "0.3.11-alpha.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "1b4rbzm56skskyb3cy5q4dmy7a0wvywhsb1dfqmp3d8cwi0i4md9")))

(define-public crate-libpt-log-0.3.11-alpha.1 (c (n "libpt-log") (v "0.3.11-alpha.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "00iifk1zfj6iispajkxmadi6k66719dmangb6k5silg3ywm26zjg")))

(define-public crate-libpt-log-0.3.11 (c (n "libpt-log") (v "0.3.11") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "1p9cd1wg95k4apmvwpq2lhpcnyv9xa1jvl8dh3kadakf94lc486l")))

(define-public crate-libpt-log-0.3.12 (c (n "libpt-log") (v "0.3.12") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0dpwlvgjlp98hc8q2750g5cxpf82ly2sgj36pxp3gbh7dwk6shqh")))

(define-public crate-libpt-log-0.4.0-alpha.2 (c (n "libpt-log") (v "0.4.0-alpha.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "10qrzycwdgxp78js3ca4011y7833iikqcxbi7wn8cxd19l4czxcz")))

(define-public crate-libpt-log-0.4.0 (c (n "libpt-log") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "0pgwq5vznr9bn20r7n4fm07bxss8xdjm1h80iz9n5yvddfg2ipdx")))

(define-public crate-libpt-log-0.4.1 (c (n "libpt-log") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "16vs3c61d2dap0bzn79p79kfxj8gaafa1xlgnsjjsrcjrs8hj9wk")))

(define-public crate-libpt-log-0.4.2 (c (n "libpt-log") (v "0.4.2") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "gag") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-appender") (r "^0.2.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "04xcbrm0dml4gpjwqn69491mgwqxsv62fm7igh6fl23m8zcsiw9s")))

