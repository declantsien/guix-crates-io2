(define-module (crates-io li bp libpd-rs) #:use-module (crates-io))

(define-public crate-libpd-rs-0.1.0 (c (n "libpd-rs") (v "0.1.0") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "10hyjwp57fyxiq4ss8iaddi3wrqzq1ycca2vjzdnpk35ygjbjzii")))

(define-public crate-libpd-rs-0.1.1 (c (n "libpd-rs") (v "0.1.1") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1k10h67mdi6hx41m4n49ks0kfdxgqai6hx0r18j3q7fazkfmayxj")))

(define-public crate-libpd-rs-0.1.2 (c (n "libpd-rs") (v "0.1.2") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "06rwbga6h20cfk4s2880p6vbrbnfa4zfcy2f2p7yqn1s7cl90335")))

(define-public crate-libpd-rs-0.1.3 (c (n "libpd-rs") (v "0.1.3") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1ygcxxkanqkkdjziijk3zka7vyqlipilllkmyifznsvhsm12jl8i")))

(define-public crate-libpd-rs-0.1.4 (c (n "libpd-rs") (v "0.1.4") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1608x33sf29a9jjnwkz8slm2z38f9fc2nrk84kg87wmqv0yf6fnv")))

(define-public crate-libpd-rs-0.1.5 (c (n "libpd-rs") (v "0.1.5") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1m2vb1m35s3rj69jbl3pbrv9j3jnq1chw3zka7wi2112mja30584")))

(define-public crate-libpd-rs-0.1.6 (c (n "libpd-rs") (v "0.1.6") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0z607mhwbxmjxkc7gl3l13whc8mypdkalvnd1fv07bf2dpn0xm4l")))

(define-public crate-libpd-rs-0.1.7 (c (n "libpd-rs") (v "0.1.7") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "059qqvrvq8irll9sznj3r7db8j81n7dw379rz3qbphg36z7dg4xk")))

(define-public crate-libpd-rs-0.1.8 (c (n "libpd-rs") (v "0.1.8") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 0)) (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 0)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1nwi3llkhi4ppxygc77fmybd11nin6xppy450fq196c9avkx77dz")))

(define-public crate-libpd-rs-0.1.9 (c (n "libpd-rs") (v "0.1.9") (d (list (d (n "cpal") (r "^0.13") (d #t) (k 2)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.2") (d #t) (k 0)) (d (n "nannou") (r "^0.18") (d #t) (k 2)) (d (n "nannou_audio") (r "^0.18") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "154ddyr21zm8nign5w1p0jf99hr3x79qai5srfwgjz620pr5jnq0")))

(define-public crate-libpd-rs-0.1.10 (c (n "libpd-rs") (v "0.1.10") (d (list (d (n "cpal") (r "^0.15.2") (d #t) (k 2)) (d (n "embed-doc-image") (r "^0.1.4") (d #t) (k 0)) (d (n "libffi") (r "^3.0.0") (d #t) (k 0)) (d (n "libpd-sys") (r "^0.2") (d #t) (k 0)) (d (n "nannou") (r "^0.18") (d #t) (k 2)) (d (n "nannou_audio") (r "^0.18") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "sys-info") (r "^0.9.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1z4wadsl7w9w7vyjl76xg90zxsjn13bcrh8cxyq95vlpx9fk31yi")))

