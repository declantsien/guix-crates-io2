(define-module (crates-io li bp libparted) #:use-module (crates-io))

(define-public crate-libparted-0.1.0 (c (n "libparted") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "failure_derive") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "libparted-sys") (r "^0.1") (d #t) (k 0)))) (h "18kyqnh1xvfy1mx08i0d61jag68gxx49mc2z71zsr9bvmcaxfhb5")))

(define-public crate-libparted-0.1.1 (c (n "libparted") (v "0.1.1") (d (list (d (n "distinst-libparted-sys") (r "^0.1") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "failure_derive") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "1y4x27x15wd31ssh6qmd2za4r2wm0894il8sqbnb1vlh0x9zin49")))

(define-public crate-libparted-0.1.2 (c (n "libparted") (v "0.1.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "failure_derive") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "libparted-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0bq4040x4as15i96s4a0zvzhcsd70hjhbvk094snswfd2swja5m3")))

(define-public crate-libparted-0.1.3 (c (n "libparted") (v "0.1.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 2)) (d (n "failure_derive") (r "^0.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "libparted-sys") (r "^0.1.1") (d #t) (k 0)))) (h "00s72r1x28xmdzgbi0yghfyrpv16im003lapn0h2sgv25r4h2qd4")))

(define-public crate-libparted-0.1.4 (c (n "libparted") (v "0.1.4") (d (list (d (n "failure") (r "^0.1.7") (d #t) (k 2)) (d (n "failure_derive") (r "^0.1.7") (d #t) (k 2)) (d (n "libc") (r "^0.2.68") (d #t) (k 0)) (d (n "libc") (r "^0.2.68") (d #t) (k 2)) (d (n "libparted-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0fhg4854kgiiizm6znp5i3482lm7pn1j06m8958yhwygy2g2cf7c")))

(define-public crate-libparted-0.1.5 (c (n "libparted") (v "0.1.5") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 2)) (d (n "failure_derive") (r "^0.1.8") (d #t) (k 2)) (d (n "libc") (r "^0.2.103") (d #t) (k 0)) (d (n "libc") (r "^0.2.103") (d #t) (k 2)) (d (n "libparted-sys") (r "^0.3.1") (d #t) (k 0)))) (h "1vqhfvwblw8imy81wvp3qkdx2b5ni32xf1jjq7zf806bpmkzgqyk")))

