(define-module (crates-io li bp libpcre-sys) #:use-module (crates-io))

(define-public crate-libpcre-sys-0.1.0 (c (n "libpcre-sys") (v "0.1.0") (d (list (d (n "bzip2") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 1)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "tar") (r ">= 0.2.12") (d #t) (k 1)))) (h "11acfq8fpkz6acgrb73h002dqkv5flixnr8c6c613ah6hyl9z4n4")))

(define-public crate-libpcre-sys-0.1.1 (c (n "libpcre-sys") (v "0.1.1") (d (list (d (n "bzip2") (r "*") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libc") (r "*") (d #t) (k 1)) (d (n "pkg-config") (r "*") (d #t) (k 1)) (d (n "tar") (r "*") (d #t) (k 1)))) (h "1gk35klvbk6lwqsy8m72nml11vg155whinj6ywac22khkrwbkbkc")))

(define-public crate-libpcre-sys-0.2.0 (c (n "libpcre-sys") (v "0.2.0") (d (list (d (n "bzip2") (r "= 0.2.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "= 0.3.1") (d #t) (k 1)))) (h "0iw0zg17f4s06sd1xv1g6p6cnvw9bn6nmzwf86a2yw9icjg4pvsk")))

(define-public crate-libpcre-sys-0.2.1 (c (n "libpcre-sys") (v "0.2.1") (d (list (d (n "bzip2") (r "= 0.2.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "= 0.3.1") (d #t) (k 1)))) (h "0zwiw0kaf71d20vccxynzjbndhxmzwx4cbg87vv3m465vgjzxpbp")))

(define-public crate-libpcre-sys-0.2.2 (c (n "libpcre-sys") (v "0.2.2") (d (list (d (n "bzip2") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)))) (h "0rk65alz6z1l88j08ycf8i31d3ms4ldjybw8a9kzxmlnp8ldvwqg")))

