(define-module (crates-io li bp libparted-sys) #:use-module (crates-io))

(define-public crate-libparted-sys-0.1.0 (c (n "libparted-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "~0.30") (d #t) (k 1)))) (h "0mzs8fw3dxjg7i6awjhjjndjcmfz7lhwdv0mzbnhildcsrx2zjrz")))

(define-public crate-libparted-sys-0.1.1 (c (n "libparted-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "~0.30") (d #t) (k 1)))) (h "0jjjhgmpg3ihpn4v9wphfvhq3fh6k36nwfjjlk5a2lpfg88qyj8n")))

(define-public crate-libparted-sys-0.2.0 (c (n "libparted-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "~0.53") (d #t) (k 1)))) (h "0klijx7pbgmpmnypla96qi6hamy5jdkcim3syg0rn5l82kdqjxza")))

(define-public crate-libparted-sys-0.3.0 (c (n "libparted-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "~0.53") (d #t) (k 1)))) (h "1g839rm1lsacxq2xvks4cm2kfjh8vnr1k19hj9ck801ys9c96vmw")))

(define-public crate-libparted-sys-0.3.1 (c (n "libparted-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "~0.58") (d #t) (k 1)))) (h "1ra9cq2gay5jmdf3w8gwvxy2wvpnxgngyja5iqj6d9a16gvfjw0x")))

(define-public crate-libparted-sys-0.3.2 (c (n "libparted-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "~0.65") (d #t) (k 1)))) (h "10j4l90gvhm8hiafhm2q9q4f3y6qdkw1rbpyhg7r8pzzm2bz3ylj")))

