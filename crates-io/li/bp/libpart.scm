(define-module (crates-io li bp libpart) #:use-module (crates-io))

(define-public crate-libpart-0.1.1 (c (n "libpart") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "checksum") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0gpwcb7hir9vxr3z4fgrlkikinjy0sz6s18736hvxwwh8z77p1pz")))

(define-public crate-libpart-0.1.2 (c (n "libpart") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "checksum") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "1wr4w7hh8jpc28wax4gxxbmrcb8y0f33cnrds5gm0qvcw26ckk5l")))

(define-public crate-libpart-0.1.3 (c (n "libpart") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "checksum") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "0s53s0f3yiy75gb0myx6p9zbnfqkp1hrsd7ckpaq9grsvlrjw9yl")))

(define-public crate-libpart-0.1.4 (c (n "libpart") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "checksum") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.5") (f (quote ("use_std"))) (d #t) (k 0)))) (h "12ihqgyl6lsbn6a7114y16hv1wkx8xdy5yw0whkzv8gflhbspi2b")))

