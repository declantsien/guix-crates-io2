(define-module (crates-io li bp libpt-math) #:use-module (crates-io))

(define-public crate-libpt-math-0.1.7 (c (n "libpt-math") (v "0.1.7") (h "0p1q7pjq9m83aqizjp23fas1k4npx7dc12hbdhyk9flhfspbxpsp")))

(define-public crate-libpt-math-0.3.10 (c (n "libpt-math") (v "0.3.10") (d (list (d (n "libpt-core") (r "^0.3.10") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.10") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1r3kzqvds4bmwzk7i3nnrimy1ggdiyzyf89y9y59ngvswhil7nxf")))

(define-public crate-libpt-math-0.3.11-alpha.1 (c (n "libpt-math") (v "0.3.11-alpha.1") (d (list (d (n "libpt-core") (r "^0.3.11-alpha.1") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.11-alpha.1") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "0n9nrnac27945mgak8s6a47nypd6lywsrnv160pyx138f1xylvy2")))

(define-public crate-libpt-math-0.3.11 (c (n "libpt-math") (v "0.3.11") (d (list (d (n "libpt-core") (r "^0.3.11") (d #t) (k 0)) (d (n "libpt-log") (r "^0.3.11") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.16") (d #t) (k 0)))) (h "1bgdkkf9zxkkrkc1nhch7jix0mdfk22z99dgq8yhjl7pf5y3cmi4")))

