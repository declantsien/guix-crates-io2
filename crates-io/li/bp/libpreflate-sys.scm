(define-module (crates-io li bp libpreflate-sys) #:use-module (crates-io))

(define-public crate-libpreflate-sys-0.1.0 (c (n "libpreflate-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "cxx") (r "^1.0.110") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.110") (d #t) (k 1)))) (h "0ys7zpmycbvpq96789rm8wb9454d1xf9vkcf18b4qbaxmn8zrwwa")))

