(define-module (crates-io li bp libpfm-sys) #:use-module (crates-io))

(define-public crate-libpfm-sys-0.1.0 (c (n "libpfm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "1wj98g2ripgcdcy35xsvvppwj5nyjqmidrdbk8db004yy4lznx8y")))

(define-public crate-libpfm-sys-0.1.1 (c (n "libpfm-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0h2h2yhggx1mnggbjfpw1big5142xnwwrh7ka3vjzg9pywgf71s2")))

