(define-module (crates-io li bs libstaticvec) #:use-module (crates-io))

(define-public crate-libstaticvec-0.1.0 (c (n "libstaticvec") (v "0.1.0") (d (list (d (n "staticvec") (r "^0.6") (f (quote ("repr_c"))) (k 0)))) (h "0dxwlik8jgbr0f0cwv0zik2idyabamk9l0k1l23pdq80zhpnlp8p") (f (quote (("default" "capacity_32") ("capacity_64") ("capacity_512") ("capacity_32") ("capacity_256") ("capacity_128")))) (y #t)))

(define-public crate-libstaticvec-0.2.0 (c (n "libstaticvec") (v "0.2.0") (d (list (d (n "staticvec") (r "^0.6") (f (quote ("repr_c"))) (k 0)))) (h "0nvdbmhxdn3fqdrckz0fjciklwlqk8fxczs8cx1hg99zkxkj952c") (f (quote (("default" "capacity_16") ("capacity_64") ("capacity_512") ("capacity_32") ("capacity_256") ("capacity_16") ("capacity_128")))) (y #t)))

(define-public crate-libstaticvec-0.2.1 (c (n "libstaticvec") (v "0.2.1") (d (list (d (n "staticvec") (r "^0.6") (f (quote ("repr_c"))) (k 0)))) (h "1qry6x3vmawy9bs3kimh2bs888q9d7f2rgwhp68v6xpd950m9gs8") (f (quote (("default" "capacity_16") ("capacity_64") ("capacity_512") ("capacity_32") ("capacity_256") ("capacity_16") ("capacity_128")))) (y #t)))

