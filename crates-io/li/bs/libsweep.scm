(define-module (crates-io li bs libsweep) #:use-module (crates-io))

(define-public crate-libsweep-0.1.0 (c (n "libsweep") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0ipwq8my2d2jqnkzlvcdxjd4xh5w729b4adpad5jw942da7v2w2w")))

(define-public crate-libsweep-0.1.1 (c (n "libsweep") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "1cnyg757p6racdzfwcz5d1fcbvq47rx2rph5k4jy9v7y0qkk7ifa")))

(define-public crate-libsweep-1.0.0 (c (n "libsweep") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "04dszqc5aix0yw66sl9cl824yym06kbsb0di1jl3vdy60kv6h2qb") (y #t)))

(define-public crate-libsweep-1.0.1 (c (n "libsweep") (v "1.0.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "123j7sycw4zwaffw1yf4gyblz2830v8d1r8f7p2x5bkcr3jx13a2")))

