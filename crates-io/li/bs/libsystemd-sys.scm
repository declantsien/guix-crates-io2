(define-module (crates-io li bs libsystemd-sys) #:use-module (crates-io))

(define-public crate-libsystemd-sys-0.0.1 (c (n "libsystemd-sys") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "1cd79325g5lgdmnz045dx1m98w3ykyljid1q34hhc5pypw883krl")))

(define-public crate-libsystemd-sys-0.0.2 (c (n "libsystemd-sys") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "15bjwbvwqzjdwijybgh5czhxwi5jcsw68rh96jac3nyf49zyy9dv")))

(define-public crate-libsystemd-sys-0.0.3 (c (n "libsystemd-sys") (v "0.0.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0b3wzbm2q8b3ig556fd32l02iv9mvv9jb7z8zpir68cgrq8fizcz")))

(define-public crate-libsystemd-sys-0.0.4 (c (n "libsystemd-sys") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0k5rqgf2248fa7chg0npivycca33jbxrim6h5s9n722470vnnplr")))

(define-public crate-libsystemd-sys-0.0.5 (c (n "libsystemd-sys") (v "0.0.5") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "pkg-config") (r "0.*") (d #t) (k 1)))) (h "0g9y4237zfk0hp1jbwci9g11g8s5hv65ymzsyrc74jsx4xpysxax")))

(define-public crate-libsystemd-sys-0.0.6 (c (n "libsystemd-sys") (v "0.0.6") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "pkg-config") (r "0.*") (d #t) (k 1)))) (h "0j3hjws34aq9hwclcc6kradaaqsnrasbxni23bbiwa1cr2sy8cmc") (f (quote (("bus"))))))

(define-public crate-libsystemd-sys-0.0.8 (c (n "libsystemd-sys") (v "0.0.8") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "pkg-config") (r "0.*") (d #t) (k 1)))) (h "0lrvavd2hp9pmmhja4gj2cf7b0j9bp10fiih0fghbcghi95cflpx") (f (quote (("bus"))))))

(define-public crate-libsystemd-sys-0.0.10 (c (n "libsystemd-sys") (v "0.0.10") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "pkg-config") (r "0.*") (d #t) (k 1)))) (h "1b5r15lf4hpid5wgrmjaycldn70f38zkyyi6d0915vpzj3i547w3") (f (quote (("bus"))))))

(define-public crate-libsystemd-sys-0.1.0 (c (n "libsystemd-sys") (v "0.1.0") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "pkg-config") (r "0.*") (d #t) (k 1)))) (h "07a1ww1ha68jv3z8japlg4pfdy5ppmbm1gl2sdzijg0q4r59hpz6") (f (quote (("bus"))))))

(define-public crate-libsystemd-sys-0.2.1 (c (n "libsystemd-sys") (v "0.2.1") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "pkg-config") (r "0.*") (d #t) (k 1)))) (h "1nd0nqqnc63r1ljba8knnvb1svzxmzjbww54kfaf0n3i84ivflg7") (f (quote (("bus"))))))

(define-public crate-libsystemd-sys-0.2.2 (c (n "libsystemd-sys") (v "0.2.2") (d (list (d (n "libc") (r "0.*") (d #t) (k 0)) (d (n "pkg-config") (r "0.*") (d #t) (k 1)))) (h "0sr2vi90sbjhxrg0asnnwh5cpaxrqjis7wd6rfmc7984rmc89ffp") (f (quote (("bus"))))))

(define-public crate-libsystemd-sys-0.5.0 (c (n "libsystemd-sys") (v "0.5.0") (d (list (d (n "build-env") (r "^0.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1dmlnillcp1nbiywfmd4l1mggmzqdp9yf7i086ysm0gfgf6x72nj") (f (quote (("journal") ("default" "bus" "journal") ("bus"))))))

(define-public crate-libsystemd-sys-0.6.0 (c (n "libsystemd-sys") (v "0.6.0") (d (list (d (n "build-env") (r "^0.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0087xdicv8pslbmpxi4vwwcmr2rq6vf4gymdcm4x51hc2kxqd99k") (f (quote (("journal") ("default" "bus" "journal") ("bus"))))))

(define-public crate-libsystemd-sys-0.7.0 (c (n "libsystemd-sys") (v "0.7.0") (d (list (d (n "build-env") (r "^0.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "12g4pfafcrdha75nidmashplbki3938qimhygch3is5ylp1w9d0i") (f (quote (("journal") ("default" "bus" "journal") ("bus"))))))

(define-public crate-libsystemd-sys-0.7.1 (c (n "libsystemd-sys") (v "0.7.1") (d (list (d (n "build-env") (r "^0.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1ifh8j2ylwbdpn90rbndhpj4ys0lmgqljizpvxa3rbsrkbafj354") (f (quote (("journal") ("default" "bus" "journal") ("bus"))))))

(define-public crate-libsystemd-sys-0.8.0 (c (n "libsystemd-sys") (v "0.8.0") (d (list (d (n "build-env") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1f3sxy4v1acl5c6h72ydab2kssnyb849flndrn6sdkff1dcgs0vf") (f (quote (("journal") ("default" "bus" "journal") ("bus"))))))

(define-public crate-libsystemd-sys-0.9.0 (c (n "libsystemd-sys") (v "0.9.0") (d (list (d (n "build-env") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1cw3rp9bfw61sbwi2vqzq6v91zs4wr4bh70y955pam0ssqjrp366") (f (quote (("systemd_v245") ("journal") ("default" "bus" "journal") ("bus"))))))

(define-public crate-libsystemd-sys-0.9.1 (c (n "libsystemd-sys") (v "0.9.1") (d (list (d (n "build-env") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0yls1s8q39i5v5z61ch259kvjib01c1j5wxqkj32w0bclya0zla8") (f (quote (("systemd_v245") ("journal") ("default" "bus" "journal") ("bus"))))))

(define-public crate-libsystemd-sys-0.9.2 (c (n "libsystemd-sys") (v "0.9.2") (d (list (d (n "build-env") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "17rqyzb2995zbw80glvyzrw085myylylrs2qgi4anp0x0a96263i") (f (quote (("systemd_v245") ("journal") ("default" "bus" "journal") ("bus"))))))

(define-public crate-libsystemd-sys-0.9.3 (c (n "libsystemd-sys") (v "0.9.3") (d (list (d (n "build-env") (r "^0.3.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.76") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "06c1g4i2ihg40j5rmka8q6xa8jbiffdj1qmw6jdw5755r9ih227d") (f (quote (("systemd_v245") ("journal") ("default" "bus" "journal") ("bus"))))))

