(define-module (crates-io li bs libsecp256k1-gen-genmult) #:use-module (crates-io))

(define-public crate-libsecp256k1-gen-genmult-0.1.0 (c (n "libsecp256k1-gen-genmult") (v "0.1.0") (d (list (d (n "libsecp256k1-core") (r "^0.1.0") (d #t) (k 0)))) (h "1kxbfnpnkqsnh3zc1n1khnl551ll4vmlf7dbsjkxijhn1iivg03d")))

(define-public crate-libsecp256k1-gen-genmult-0.2.0 (c (n "libsecp256k1-gen-genmult") (v "0.2.0") (d (list (d (n "libsecp256k1-core") (r "^0.2.0") (d #t) (k 0)))) (h "1ffmzyh3d51br25dcjwykbrwfn3chp16469bi7wza40xw8rv9b3n")))

(define-public crate-libsecp256k1-gen-genmult-0.2.1 (c (n "libsecp256k1-gen-genmult") (v "0.2.1") (d (list (d (n "libsecp256k1-core") (r "^0.2.2") (d #t) (k 0)))) (h "0pag9hq2pvisin0iy67q5m0388wf0qmv7gm290famqwmjcagxav7")))

(define-public crate-libsecp256k1-gen-genmult-0.3.0 (c (n "libsecp256k1-gen-genmult") (v "0.3.0") (d (list (d (n "libsecp256k1-core") (r "^0.3.0") (d #t) (k 0)))) (h "0z3gl0x5rpdjrr3mds8620gk0h0qjfccr33f1v2ar7pc5jxddf1x")))

