(define-module (crates-io li bs libsql-shell) #:use-module (crates-io))

(define-public crate-libsql-shell-0.1.0 (c (n "libsql-shell") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0ap85qbk8jm1vmmrv6r8bgy1rbvxay5fdflrlxdif3wc7f3a7rdd") (y #t)))

(define-public crate-libsql-shell-0.1.1 (c (n "libsql-shell") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "clap") (r "^4.1.8") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28.0") (d #t) (k 0)) (d (n "rustyline") (r "^11.0.0") (d #t) (k 0)) (d (n "tabled") (r "^0.10.0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1v70qlz6m27bf6gn3x8qd1844d9l96xsx9di0q0hizlrfi4lm0wh")))

