(define-module (crates-io li bs libsyd) #:use-module (crates-io))

(define-public crate-libsyd-3.0.0-alpha.2 (c (n "libsyd") (v "3.0.0-alpha.2") (d (list (d (n "cbindgen") (r "^0.26") (d #t) (k 1)))) (h "1c0a7zwqa5hkby9xjxcxv1306x62jn5vnqfhpj1zw753pxrprcdm") (y #t) (r "1.65")))

(define-public crate-libsyd-3.0.0-alpha.3 (c (n "libsyd") (v "3.0.0-alpha.3") (d (list (d (n "cbindgen") (r "^0.26") (d #t) (k 1)))) (h "184374d7ryhbhn4arqrz6zn6mgmx7vsbgfc8rlgn6v1g4sqbxj4h") (y #t) (r "1.65")))

(define-public crate-libsyd-3.0.0-alpha.4 (c (n "libsyd") (v "3.0.0-alpha.4") (h "1xm1l6w369v9a2k95lv5wi8jpfz63nvmkmlxr3xyhy1yznzsh848") (y #t) (r "1.65")))

(define-public crate-libsyd-3.0.0-alpha.5 (c (n "libsyd") (v "3.0.0-alpha.5") (h "0v6y9jh8v55wivj5yml1jmvqq41ykx4g01d6akwsra0904hbvppf") (y #t) (r "1.65")))

(define-public crate-libsyd-3.0.0 (c (n "libsyd") (v "3.0.0") (d (list (d (n "itoa") (r "^1.0") (k 0)) (d (n "nix") (r "^0.27") (f (quote ("fs"))) (k 0)))) (h "10a7i6rvrjd7y8l5gk0xlcczmh9dwzn9bkzpkyg67dlw0lnl88l4") (r "1.65")))

(define-public crate-libsyd-3.0.1 (c (n "libsyd") (v "3.0.1") (d (list (d (n "itoa") (r "^1.0") (k 0)) (d (n "nix") (r "^0.27") (f (quote ("fs"))) (k 0)))) (h "1q5mzrx43w890b9259iklwvf81hj5xjn2csxf8j454iisvvg51xg") (r "1.65")))

(define-public crate-libsyd-3.0.2 (c (n "libsyd") (v "3.0.2") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "1z7sji9zqjmblnxphgc6288fcayddwfqzf64b7s70z2glc317765") (r "1.65")))

(define-public crate-libsyd-3.0.3 (c (n "libsyd") (v "3.0.3") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "1mjwbcxp1wfa0y0ml1blwm4qq3vsaivbgxg8smykv2jjgvdrhx58") (r "1.65")))

(define-public crate-libsyd-3.0.4 (c (n "libsyd") (v "3.0.4") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "0cc81mz7h4z7i77l6a65ycdj24d4ap9lkqx78f6rrmd0vjchs1sr") (r "1.65")))

(define-public crate-libsyd-3.0.5 (c (n "libsyd") (v "3.0.5") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "0lyllh2v5k46m9km33b42yxd3dw5hznj54q897rgmpyx8ixblgcq") (r "1.65")))

(define-public crate-libsyd-3.0.6 (c (n "libsyd") (v "3.0.6") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "1824n8j6h4y6pgp6s1s0i3ias64lfv5kjqkv9xzrdlc70awv738a") (r "1.56.1")))

(define-public crate-libsyd-3.0.7 (c (n "libsyd") (v "3.0.7") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "19y16qkilgpwlzfqs7mvyzzca75lx7m7nfjcxvdw9cskziy54isl") (r "1.56.1")))

(define-public crate-libsyd-3.1.0 (c (n "libsyd") (v "3.1.0") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "1jv41i4g4ajn4j6axin5n9ll1rbd1wx2qmchcxbsdpfkmnwr5agf") (r "1.56.1")))

(define-public crate-libsyd-3.1.1 (c (n "libsyd") (v "3.1.1") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "05rzd4gv7wp66ksq3111zfyf9hp0fpw8mfx46dfhl1crdrwpknl0") (r "1.56.1")))

(define-public crate-libsyd-3.1.2 (c (n "libsyd") (v "3.1.2") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "019imyi77m4zi1n02cwpks27y73is0r1i0kgr4gdiinpc45k8xfj") (r "1.56.1")))

(define-public crate-libsyd-3.1.3 (c (n "libsyd") (v "3.1.3") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "1mkzhr1lis3ml82g02pr3r5fqyn33jbr9zab2ajw2w481jwmp0k6") (r "1.56.1")))

(define-public crate-libsyd-3.1.4 (c (n "libsyd") (v "3.1.4") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "0fwlzqc3kl4qc63vd8833z2g33rhdywi1ihb4gjwsvg00b6sgmvg") (r "1.56.1")))

(define-public crate-libsyd-3.1.5 (c (n "libsyd") (v "3.1.5") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "15s2c3r514kwl8k0fakh3n55r2jd24w63iss5gbflc6iamq4gpqy") (r "1.56.1")))

(define-public crate-libsyd-3.2.0 (c (n "libsyd") (v "3.2.0") (d (list (d (n "itoa") (r "^1.0") (k 0)))) (h "05kis98v5wsqr3g3my8x7407h12r2bj93lqq0x1dwmc1fflra044") (r "1.56.1")))

