(define-module (crates-io li bs libsugar) #:use-module (crates-io))

(define-public crate-libsugar-2.4.0 (c (n "libsugar") (v "2.4.0") (d (list (d (n "tuples") (r "^1.4") (o #t) (d #t) (k 0)))) (h "1l0ps6f8wp2xwx5sa7zsm7f5il2dhpfbgy3qwxp4vdd91f5ic9ix") (f (quote (("std") ("side-effect") ("re-exports") ("once_get") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports" "chain_panic" "chain_todo" "tuples" "once_get" "chain_drop") ("combin") ("chain_todo") ("chain_panic") ("chain_drop"))))))

(define-public crate-libsugar-2.4.1 (c (n "libsugar") (v "2.4.1") (d (list (d (n "tuples") (r "^1.4") (o #t) (d #t) (k 0)))) (h "00d84mzr2iz2v047ww6d5693768rr7yffhh6qv4ccmjxh8yyzsq2") (f (quote (("std") ("side-effect") ("re-exports") ("once_get") ("named-into") ("macro-lit") ("default" "std" "combin" "named-into" "macro-lit" "side-effect" "re-exports" "chain_panic" "chain_todo" "tuples" "once_get" "chain_drop") ("combin") ("chain_todo") ("chain_panic") ("chain_drop"))))))

