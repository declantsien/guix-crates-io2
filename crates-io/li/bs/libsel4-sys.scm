(define-module (crates-io li bs libsel4-sys) #:use-module (crates-io))

(define-public crate-libsel4-sys-0.5.1 (c (n "libsel4-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.35") (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fel4-config") (r "^0.3") (d #t) (k 1)) (d (n "rlibc") (r "^1.0") (d #t) (k 0)))) (h "05b83n6i24mlb82ykzcq1zcnmzqyvp7na4zzmzfqqgb1ld6cmz3s") (l "sel4")))

(define-public crate-libsel4-sys-0.5.2 (c (n "libsel4-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.35") (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "fel4-config") (r "^0.3") (d #t) (k 1)) (d (n "rlibc") (r "^1.0") (d #t) (k 0)))) (h "0l4jg47j0729zxkfsybv9qml7cnlj14akjvn802nwr2hwfyga81n") (l "sel4")))

