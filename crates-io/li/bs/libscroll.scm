(define-module (crates-io li bs libscroll) #:use-module (crates-io))

(define-public crate-libscroll-0.1.0 (c (n "libscroll") (v "0.1.0") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "06c1p9sv2h2ak57frywk5zm7i6p0y7ydg5xvbxklc2553f5r1nsv")))

(define-public crate-libscroll-0.1.1 (c (n "libscroll") (v "0.1.1") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1bgpqy1x1hnmlai8d9lpari5208x8y18hkxr16x0sy22mlfgqnrm")))

(define-public crate-libscroll-0.1.2 (c (n "libscroll") (v "0.1.2") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1wwxn4f237wj56ffcxmwkpv9xabf7qq73khyz7myi68mf3agq89g")))

(define-public crate-libscroll-0.1.3 (c (n "libscroll") (v "0.1.3") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "1byxk4i0pg7f81577hs9b4qkgb42j9cfbg1187yihm0j5gccil2h")))

(define-public crate-libscroll-0.1.4 (c (n "libscroll") (v "0.1.4") (d (list (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "150m5q4dh44772ysh0c1izw5pb1yhjnf38yqcn7jndalmhm21gz4")))

(define-public crate-libscroll-0.1.5 (c (n "libscroll") (v "0.1.5") (d (list (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "0404v9j6kn8340q4s8hir5zx9xj2c6s3w0ccc90srb5066whkmvg")))

(define-public crate-libscroll-0.1.6 (c (n "libscroll") (v "0.1.6") (d (list (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)))) (h "176h1jhwifirazjiw736pip78lviw2z5g0czr80dbdf5x0i70hb7")))

(define-public crate-libscroll-0.2.0 (c (n "libscroll") (v "0.2.0") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "tini") (r "^0.1") (d #t) (k 0)))) (h "1in3s675332j012ywkjhhmnw1iigyw3qyq0blg10zs0ly44yj10m")))

(define-public crate-libscroll-0.2.1 (c (n "libscroll") (v "0.2.1") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "tini") (r "^0.1") (d #t) (k 0)))) (h "1psq47cxb3bhm2617m9yk6g25ciq6a031yln46bqycvd9d0jny7i")))

(define-public crate-libscroll-0.2.2 (c (n "libscroll") (v "0.2.2") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "tini") (r "^0.1") (d #t) (k 0)))) (h "1j87rx31n34yyh1sy6sww5n743cm4ff2xrsmvp63hhyzk8xsz1sf")))

(define-public crate-libscroll-0.2.3 (c (n "libscroll") (v "0.2.3") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "tini") (r "^0.1") (d #t) (k 0)))) (h "0falmbirmr296d2n2frl7790ybq5d91rrr0x0f2rsba75qifpdz5")))

(define-public crate-libscroll-0.2.4 (c (n "libscroll") (v "0.2.4") (d (list (d (n "app_dirs") (r "^1.2.1") (d #t) (k 0)) (d (n "flo_curves") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "tini") (r "^0.1") (d #t) (k 0)))) (h "0x4xgy4nxxzgjjn9ycjipvkydrr9m73xc498vkih153kcnrpvkn8")))

