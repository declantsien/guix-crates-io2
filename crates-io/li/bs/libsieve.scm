(define-module (crates-io li bs libsieve) #:use-module (crates-io))

(define-public crate-libsieve-0.1.0 (c (n "libsieve") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "083b1mqg6p7fslw16c5rgx96lw3c08rxjh13mi29mlx87hfpw6jk")))

(define-public crate-libsieve-0.1.1 (c (n "libsieve") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "0qvn01f649yw01y0823fxn37c99g694c182dp0mh3g55h2qlkq8n")))

(define-public crate-libsieve-0.1.2 (c (n "libsieve") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "memmem") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^5.0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "0si60920nwf8zfh545qwav1d9qkampp42iqcr21nlms93a2r8zy4")))

