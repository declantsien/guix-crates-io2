(define-module (crates-io li bs libstat) #:use-module (crates-io))

(define-public crate-libstat-0.1.0 (c (n "libstat") (v "0.1.0") (d (list (d (n "libmath") (r "^0.1") (d #t) (k 0)))) (h "1472y3kx5jjckis2wa9ymgra9ssmkkq8wzbzqpv03z0jidqjvf1z")))

(define-public crate-libstat-0.1.1 (c (n "libstat") (v "0.1.1") (d (list (d (n "libmath") (r "~0.1.3") (d #t) (k 0)))) (h "14q6rz9zc6qchnav8783zy6hxz7j1c4c3bc80b4qxifqknd7a5s8")))

