(define-module (crates-io li bs libstomper) #:use-module (crates-io))

(define-public crate-libstomper-0.1.0 (c (n "libstomper") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "144darh85dmishwgmhv37n6dh833l6cfbiy8vm69r422hkjp33ki")))

(define-public crate-libstomper-0.2.0 (c (n "libstomper") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "18xwwn2apq6hs5bpy1masgm5hlb4yqd6krwcf9s09yg4hgvlqac0")))

