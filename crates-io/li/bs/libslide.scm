(define-module (crates-io li bs libslide) #:use-module (crates-io))

(define-public crate-libslide-0.0.1 (c (n "libslide") (v "0.0.1") (d (list (d (n "criterion") (r "^0.3.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (f (quote ("std"))) (k 0)) (d (n "rug") (r "^1.9.0") (d #t) (k 0)))) (h "1bdjb27kbw7jm94vpna97mh57fn6fkv8ykxbfz2rjy0343vigd3n") (f (quote (("benchmark-internals"))))))

