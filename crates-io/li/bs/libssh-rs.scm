(define-module (crates-io li bs libssh-rs) #:use-module (crates-io))

(define-public crate-libssh-rs-0.1.0 (c (n "libssh-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11wlaxzcnl295zkmggcc6s130hymsjyzi9wds5zfzlgjign5idm2") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.1.1 (c (n "libssh-rs") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h1sx7mqm0fy554jkf0z9qi9hn1aiiiqzv6l98933wywn5bb43f3") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.1.2 (c (n "libssh-rs") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rvj4j8cigs70x5pplfw7b9zjycjgcd9l9iwpqb45wkqn1y6ijn5") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.1.3 (c (n "libssh-rs") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0nsasps85klq02lmblalski9wixnn4kb3qz0g9qriqs46qj4qgzb") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.1.4 (c (n "libssh-rs") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0hif4f7n9hf5ps553pi6j5qnazyjz92m9jpi48pgbmivgj2vf92a") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.1.5 (c (n "libssh-rs") (v "0.1.5") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dk8mrd9l00wxfxba0da3dsg7zcclb5zh6wjvifn42b4yry06yfk") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.1.6 (c (n "libssh-rs") (v "0.1.6") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04jbg45rwkwb26jk6h2y4n41dg1wnygpq6wbvwjd9x50k3dv784x") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.1.7 (c (n "libssh-rs") (v "0.1.7") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "openssl-sys") (r "=0.9.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0s0igd7d9y8ncp86p10ialk5lka4msl54cnzcvpnk95zn37dpd88") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.1.8 (c (n "libssh-rs") (v "0.1.8") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "openssl-sys") (r "=0.9.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15s7xiq1l9wxclxrpxpzh0l12csmvaljafwvk97m9m8zp3bdc6j1") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.1.9 (c (n "libssh-rs") (v "0.1.9") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "openssl-sys") (r "=0.9.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xr9x5r3pimw2qr7a1y1dynraxx2lkyxpd1156mppxshwxmifzfx") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored")))) (y #t)))

(define-public crate-libssh-rs-0.2.0 (c (n "libssh-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "openssl-sys") (r "=0.9.71") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0x5apffl3r4skj7kgbkyy6kk7grla8xk3nhkb4q8wfm47922r2gz") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.2.1 (c (n "libssh-rs") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1imc5ac66pvv0krsd7lg8mpwsfipgvg1ssish7vf2wffla9k1qgw") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.2.2 (c (n "libssh-rs") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "026vncph1i9d91l9jfl5bs4v89bzajn844xqmcl1vdq6zcjf6gzb") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.3.1 (c (n "libssh-rs") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1nriwgrpjhkqi5hnyz2v8nc52r97cg5bl5j7lh8pzdwvlwlbygy1") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.3.2 (c (n "libssh-rs") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h59ajain69lv26cwc5ac1hn43awbc279w362760csymn8bvdr9z") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

(define-public crate-libssh-rs-0.3.3 (c (n "libssh-rs") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libssh-rs-sys") (r "^0.2.4") (d #t) (k 0)) (d (n "openssl-sys") (r "^0.9.93") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "162zar0lz30vxisg339wiwqqq969vkpdbzhw8x93ricdzln6hijd") (f (quote (("vendored-openssl" "libssh-rs-sys/vendored-openssl") ("vendored" "libssh-rs-sys/vendored"))))))

