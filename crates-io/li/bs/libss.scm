(define-module (crates-io li bs libss) #:use-module (crates-io))

(define-public crate-libss-0.1.0 (c (n "libss") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "rand") (r "^0.4.0") (d #t) (k 0)) (d (n "subtle") (r "^2.1.0") (d #t) (k 0)))) (h "046k6ayf3wfkdvzds8id6f67ayn5lqf9xxwisyjmwd1vbkvlb68d")))

