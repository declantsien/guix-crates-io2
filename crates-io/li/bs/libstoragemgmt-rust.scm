(define-module (crates-io li bs libstoragemgmt-rust) #:use-module (crates-io))

(define-public crate-libstoragemgmt-rust-0.0.1 (c (n "libstoragemgmt-rust") (v "0.0.1") (d (list (d (n "rand") (r "^0.3.18") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.16") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.4") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "1drdh845m6gc28fn0xahv9qdpwq9a8p9bv0qb7vx28202phaxjbi")))

