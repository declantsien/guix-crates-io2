(define-module (crates-io li bs libsts) #:use-module (crates-io))

(define-public crate-libsts-0.1.0 (c (n "libsts") (v "0.1.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "fwatch") (r "^0.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0chklcpf3zrzb0v02bwv5iy8q6lkzm0smjmnp9mkjmrzg14pzvjr")))

(define-public crate-libsts-0.2.0 (c (n "libsts") (v "0.2.0") (d (list (d (n "base64") (r "^0.10.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "fwatch") (r "^0.1.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kp53l7s8k6frdgk38q9vxdd9mvwk3l2z46k2b6lxjmk4zcldkwh")))

(define-public crate-libsts-0.3.0 (c (n "libsts") (v "0.3.0") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "fwatch") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0v4acywm8v850n0r48hciqq08crgxrc69fd4060jjisj6makv2gc")))

(define-public crate-libsts-0.3.1 (c (n "libsts") (v "0.3.1") (d (list (d (n "base64") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "fwatch") (r "^0.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12d2wrsk2ack3nmpsavjz0x5zaapqw3dl81zfr4cc3xd0i62xsw8")))

