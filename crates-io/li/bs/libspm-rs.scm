(define-module (crates-io li bs libspm-rs) #:use-module (crates-io))

(define-public crate-libspm-rs-0.0.1 (c (n "libspm-rs") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "0sxv4q1zsvzb2xn097pdzkxipdp4046pgwghyq683pddrscnjjv2")))

(define-public crate-libspm-rs-0.0.2 (c (n "libspm-rs") (v "0.0.2") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "0pp8j93gd06r0p87gp556mwfaacbq0r55gv1w6q208ciainzb81f")))

(define-public crate-libspm-rs-0.0.3 (c (n "libspm-rs") (v "0.0.3") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "1jy4hyh2plqkljavvz0vg6h6w3kv4wasdvca73n632w8s2n468h4")))

(define-public crate-libspm-rs-0.0.4 (c (n "libspm-rs") (v "0.0.4") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)))) (h "0x6qp1r4lc3vhm9a18grg20i6ksz20k772kcgyz35wf3c8gbb5v4")))

