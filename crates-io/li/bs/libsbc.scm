(define-module (crates-io li bs libsbc) #:use-module (crates-io))

(define-public crate-libsbc-0.1.0 (c (n "libsbc") (v "0.1.0") (d (list (d (n "alsa") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "libsbc-sys") (r "^0.1") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.13") (d #t) (k 0)))) (h "1f7ljc9x21laky3rr0fj65ij1j5l9gayqi24wk0lxcq6j2aj3wzg")))

(define-public crate-libsbc-0.1.1 (c (n "libsbc") (v "0.1.1") (d (list (d (n "alsa") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "libsbc-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.13") (d #t) (k 0)))) (h "1cxh320fnb3hsp27nkprz4jacmvl1jn83v6dj8nlb26gwr3042ni")))

(define-public crate-libsbc-0.1.2 (c (n "libsbc") (v "0.1.2") (d (list (d (n "alsa") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "libsbc-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.13") (d #t) (k 0)))) (h "19qngvmy22yp3280ddz827phsm51dhnbw4iwj22g3c3njw49bfqg")))

(define-public crate-libsbc-0.1.3 (c (n "libsbc") (v "0.1.3") (d (list (d (n "alsa") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "libsbc-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.1.13") (d #t) (k 0)))) (h "005i154cfzpy96vqdm785hynycxaw568yg31npl5rs4w0hl00f99")))

(define-public crate-libsbc-0.1.4 (c (n "libsbc") (v "0.1.4") (d (list (d (n "alsa") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "libsbc-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.2") (d #t) (k 0)))) (h "0isy4jfbz4hi3x0mw2hryafrdi23zvzdnf62gkcm6hjj334kng9g")))

(define-public crate-libsbc-0.1.5 (c (n "libsbc") (v "0.1.5") (d (list (d (n "alsa") (r "^0.2.0") (d #t) (k 2)) (d (n "byteorder") (r "^1") (d #t) (k 2)) (d (n "failure") (r "^0.1.2") (d #t) (k 0)) (d (n "libsbc-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "slice-deque") (r "^0.2") (d #t) (k 0)))) (h "06zmifwkpdg6symn6vf7da49f7bmpgsysjrnva2nsxg9h2wzjy7l")))

