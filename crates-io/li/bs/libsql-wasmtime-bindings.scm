(define-module (crates-io li bs libsql-wasmtime-bindings) #:use-module (crates-io))

(define-public crate-libsql-wasmtime-bindings-0.1.0 (c (n "libsql-wasmtime-bindings") (v "0.1.0") (d (list (d (n "wasmtime") (r "^2.0") (d #t) (k 0)))) (h "1b21qmbf8f0a904qswnvcdh4nvjkvrx9n0sbnxnszfabajkqqiy8")))

(define-public crate-libsql-wasmtime-bindings-0.2.0 (c (n "libsql-wasmtime-bindings") (v "0.2.0") (d (list (d (n "wasmtime") (r "^2.0") (d #t) (k 0)))) (h "01zf12f6gz6ccshvm0s4q7c9grd8nkjbqjn0xxjkd9xciv1zv99z")))

(define-public crate-libsql-wasmtime-bindings-0.2.1 (c (n "libsql-wasmtime-bindings") (v "0.2.1") (d (list (d (n "wasmtime") (r "^3.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^3.0") (d #t) (k 0)))) (h "0ndzki6r0bv88fmj429x2k296gzzhwpfl6vykglqal6z95c6zddw")))

(define-public crate-libsql-wasmtime-bindings-0.2.2 (c (n "libsql-wasmtime-bindings") (v "0.2.2") (d (list (d (n "wasmtime") (r "^9.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^9.0") (d #t) (k 0)))) (h "0vvbzfwm166wp9rwy1c7y4ai61vvjz3zhiddkn1q6zz347zr8isw")))

