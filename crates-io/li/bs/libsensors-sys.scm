(define-module (crates-io li bs libsensors-sys) #:use-module (crates-io))

(define-public crate-libsensors-sys-0.1.0 (c (n "libsensors-sys") (v "0.1.0") (h "01wgdps5ikdp0622w59qwwc8wbwvn2373j3v0kfw9qhbwicprsj3")))

(define-public crate-libsensors-sys-0.1.1 (c (n "libsensors-sys") (v "0.1.1") (h "0vgwr0wwbs2a521j0c5dyr2gf1kgxdd94jy3r03wqrx348wch63m")))

(define-public crate-libsensors-sys-0.1.2 (c (n "libsensors-sys") (v "0.1.2") (h "1w2j5gdh49wp1bajyv8baqhsnf1026sn4pdhhd3s6yz6gkpm2ydv")))

(define-public crate-libsensors-sys-0.2.0 (c (n "libsensors-sys") (v "0.2.0") (h "0cii4jbj19d95q6nyczfi3d3s2j3jg4qf7qc8qfxx5jb2v3hqwx1")))

