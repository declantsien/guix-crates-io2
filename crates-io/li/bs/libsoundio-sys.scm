(define-module (crates-io li bs libsoundio-sys) #:use-module (crates-io))

(define-public crate-libsoundio-sys-0.2.0 (c (n "libsoundio-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.2") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0swh513ba27hbccq0fhwh1slgdbgzk647ixd7k8195q31ha88fqg")))

(define-public crate-libsoundio-sys-0.3.0 (c (n "libsoundio-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0zrsv4hsj30p35xy50hgybgrpmkc9c0xnavyg18cjaa0wfg5y6g2") (l "soundio")))

