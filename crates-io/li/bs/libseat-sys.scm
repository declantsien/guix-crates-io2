(define-module (crates-io li bs libseat-sys) #:use-module (crates-io))

(define-public crate-libseat-sys-0.1.0 (c (n "libseat-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "00qi3w850i4fsg5z2i665kc2hwjgb1yfqhh6j52ysw5nbj1piypl") (f (quote (("use_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libseat-sys-0.1.1 (c (n "libseat-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (o #t) (d #t) (k 1)))) (h "15v32f0ss56d721f542rhd8z1yvd5qcvr09wz4cscsi57n1cybxp") (f (quote (("use_bindgen" "bindgen" "pkg-config"))))))

(define-public crate-libseat-sys-0.1.5 (c (n "libseat-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.58.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "151gjic0hlfpxkr33f6yzd2l19pqscr884v7fk2fvnqxd13r9yli") (f (quote (("use_bindgen" "bindgen")))) (y #t)))

(define-public crate-libseat-sys-0.1.6 (c (n "libseat-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.58.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1czx75bilbg8pvkfxwb08vj6rzccydh9azjqk1nkh4rfxnzysva9") (f (quote (("use_bindgen" "bindgen") ("docs_rs")))) (y #t)))

(define-public crate-libseat-sys-0.1.7 (c (n "libseat-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.58.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0d2c7lbcl45rmpwy0nb3z94f6jam4ylxmddky1mis7w70dgcnw9n") (f (quote (("use_bindgen" "bindgen") ("docs_rs"))))))

