(define-module (crates-io li bs libsecp256k1-gen-ecmult) #:use-module (crates-io))

(define-public crate-libsecp256k1-gen-ecmult-0.1.0 (c (n "libsecp256k1-gen-ecmult") (v "0.1.0") (d (list (d (n "libsecp256k1-core") (r "^0.1.0") (d #t) (k 0)))) (h "0pdsqhrz4xiamqyn61mrry2yiyi1whl4x40mblshjr45ah1jn86g")))

(define-public crate-libsecp256k1-gen-ecmult-0.2.0 (c (n "libsecp256k1-gen-ecmult") (v "0.2.0") (d (list (d (n "libsecp256k1-core") (r "^0.2.0") (d #t) (k 0)))) (h "1h694mlx3i639y40l03sm11j8av7ng704ymkhddhjsmvzwk9c8rj")))

(define-public crate-libsecp256k1-gen-ecmult-0.2.1 (c (n "libsecp256k1-gen-ecmult") (v "0.2.1") (d (list (d (n "libsecp256k1-core") (r "^0.2.2") (d #t) (k 0)))) (h "1hy5agkfylvdlfsxm5n07ljx1byyy1cyc1rzm23ar3ykhjsrdayc")))

(define-public crate-libsecp256k1-gen-ecmult-0.3.0 (c (n "libsecp256k1-gen-ecmult") (v "0.3.0") (d (list (d (n "libsecp256k1-core") (r "^0.3.0") (d #t) (k 0)))) (h "02a8rddxan8616rrq5b88hbw9ikz323psfk4fahyi1swql4chf1h")))

