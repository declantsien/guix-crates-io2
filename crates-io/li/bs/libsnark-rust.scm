(define-module (crates-io li bs libsnark-rust) #:use-module (crates-io))

(define-public crate-libsnark-rust-1.1.2 (c (n "libsnark-rust") (v "1.1.2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "zkinterface") (r "^1.1.2") (d #t) (k 0)))) (h "1f2qjzg4rna9jpwcjz9ysrxfans3xwbd9klm11b0pxacg5149m66") (y #t) (l "zkif_gadgetlib")))

