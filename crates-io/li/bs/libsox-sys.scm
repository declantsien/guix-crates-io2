(define-module (crates-io li bs libsox-sys) #:use-module (crates-io))

(define-public crate-libsox-sys-0.1.0 (c (n "libsox-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 2)))) (h "09d9grj6cg1b0d8ajr2friq0j1z24dwh0ixzams7b6dfkgi1qs93")))

(define-public crate-libsox-sys-0.1.1 (c (n "libsox-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.51") (d #t) (k 2)))) (h "016hcslm9hlrlpg5hx111dk9ll8f665lw8krbgpyk73s7kw7i7kx")))

