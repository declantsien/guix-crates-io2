(define-module (crates-io li bs libsocket) #:use-module (crates-io))

(define-public crate-libsocket-0.1.0 (c (n "libsocket") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "1jf61bwzqqv9b08sc6znxvpa4kbm30ii543yxrnxcc044avsgd34")))

(define-public crate-libsocket-0.1.1 (c (n "libsocket") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "errno") (r "^0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "0z4ribksif5l3jvamp5hw9mi4rp37v3f0f0zpqq462j3v3dbxvmd")))

