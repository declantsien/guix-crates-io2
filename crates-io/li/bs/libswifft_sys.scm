(define-module (crates-io li bs libswifft_sys) #:use-module (crates-io))

(define-public crate-libswifft_sys-0.1.0 (c (n "libswifft_sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)))) (h "12kqi8ilb6h9alxz7p2h8pqjwvvfqjl17i5zv3aksz5mdj042346") (l "LibSWIFFT")))

(define-public crate-libswifft_sys-0.2.0 (c (n "libswifft_sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.0") (d #t) (k 1)))) (h "13q57hm2br72vg8skal2iavr5c8pg4csis5blx8p90l5kamqs1w2") (l "LibSWIFFT")))

