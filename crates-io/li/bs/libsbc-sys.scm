(define-module (crates-io li bs libsbc-sys) #:use-module (crates-io))

(define-public crate-libsbc-sys-0.1.0 (c (n "libsbc-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "10za4k7f2didcldqvcf1mdg2q7j8ji2p1nkqsqsqad7r17vxkhwa")))

(define-public crate-libsbc-sys-0.1.1 (c (n "libsbc-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1nywywzgm5x0p27f08fxq6rj1v78k2bacslc8557lyv1kclxpjlf") (f (quote (("source-build") ("default" "source-build"))))))

(define-public crate-libsbc-sys-0.1.2 (c (n "libsbc-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1m01nnyqfsrh0f22cnfsnlxm9zmi7dnbilm20svarzvzhn7nb1i3") (f (quote (("source-build") ("default" "source-build"))))))

