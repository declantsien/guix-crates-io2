(define-module (crates-io li bs libssg) #:use-module (crates-io))

(define-public crate-libssg-0.1.0 (c (n "libssg") (v "0.1.0") (d (list (d (n "handlebars") (r "^3.0.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.47") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v3"))) (d #t) (k 0)))) (h "0aw65lkx9jdkinj40nbkvz81nzk6090m76d4w8m6jjp13xnyn14f")))

(define-public crate-libssg-0.1.1 (c (n "libssg") (v "0.1.1") (d (list (d (n "handlebars") (r "^3.0.1") (f (quote ("dir_source"))) (d #t) (k 0)) (d (n "regex") (r "^1.3.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.100") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.47") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (f (quote ("v3"))) (d #t) (k 0)))) (h "1plg5rvd6qw4kxw2ah4x1vijqlws2v0rxdxd4z8bjy57ljr4vnr9")))

