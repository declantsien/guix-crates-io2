(define-module (crates-io li bs libseminix) #:use-module (crates-io))

(define-public crate-libseminix-0.1.0 (c (n "libseminix") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)))) (h "1rs5cpfcp18bwmjmynr2xik525nyycqkwfrm93s83p85qw1mhs1x") (y #t)))

(define-public crate-libseminix-0.1.1 (c (n "libseminix") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)))) (h "17ki119nwfwhy3wsxshz1m5kjhaz077n4ic5yii6hk79yanq0xz3") (y #t)))

(define-public crate-libseminix-0.1.2 (c (n "libseminix") (v "0.1.2") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)))) (h "1craakhhn5hxrx9m98jqwxiqd1467pn9ca0qwakl5zsxw7bkq30w") (y #t)))

(define-public crate-libseminix-0.1.3 (c (n "libseminix") (v "0.1.3") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)))) (h "0jpccvm86jvzbhya9cq674192i35y9n669xdq6bqznaqqvbgsbzh") (y #t)))

(define-public crate-libseminix-0.1.4 (c (n "libseminix") (v "0.1.4") (d (list (d (n "bitfield") (r "^0.14.0") (d #t) (k 0)) (d (n "bitflags") (r "^2.3.1") (d #t) (k 0)))) (h "1rgr1rl2s2pkn4qsla7km0sklpk17djbprcsafppdz4vravvfad0") (y #t)))

