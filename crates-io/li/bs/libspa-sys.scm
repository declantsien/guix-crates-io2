(define-module (crates-io li bs libspa-sys) #:use-module (crates-io))

(define-public crate-libspa-sys-0.1.0 (c (n "libspa-sys") (v "0.1.0") (d (list (d (n "bindgen") (r ">=0.55.0, <0.56.0") (d #t) (k 1)) (d (n "system-deps") (r ">=2.0.0, <3.0.0") (d #t) (k 1)))) (h "1y6mzg44vwd5raqdyfb9vx7gspxhf7mf3699f4wcdqqrmzyf69p7") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.2.0 (c (n "libspa-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "system-deps") (r "^2.0") (d #t) (k 1)))) (h "06xmawilhs6irb4y5gkbf92156w3p7l273rrriz32i1hfi63xbhm") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.2.1 (c (n "libspa-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "system-deps") (r "^2.0") (d #t) (k 1)))) (h "1apwzs6g2rl7fsa85p5z7w33a87350xq00sjg16r43mwc7ccifgi") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.2.2 (c (n "libspa-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "system-deps") (r "^3.0") (d #t) (k 1)))) (h "0fvp99wpxs2k12izmnjy56xapq2g84qilgxb3fjhk7y2js17czv0") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.3.0 (c (n "libspa-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "system-deps") (r "^3.0") (d #t) (k 1)))) (h "0sa7cwfkhr5syqljlrh6ybixb839b1rfzyhx95gc1bd5ndk1ad8r") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.4.0 (c (n "libspa-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "system-deps") (r "^3.0") (d #t) (k 1)))) (h "0zgv2s432dqvsjc5flc6ssr5sp86kpwnl5w8kj048sz9lkyiiyb2") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.4.1 (c (n "libspa-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "system-deps") (r "^3.0") (d #t) (k 1)))) (h "19pgpqblq8aacd7ggv4m4q6ay6glk16qlh1n730rf2pd5zya40fk") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.5.0 (c (n "libspa-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "0kbdc44y6lam6wircv995kd1dkr7q44v837bg1yhvlfdsqw2x3cy") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.6.0 (c (n "libspa-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.64") (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "0yjcadwv8zas8rk0kin57i7148lfcblilicdm1ydyd15yn45pkvr") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.7.0 (c (n "libspa-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.66") (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "17wpralvak293mi38aijl2dig1bj1b1aggncvj691qr72dvv5v9n") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.7.1 (c (n "libspa-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.66") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "1ard1gyivzy8wbgz6y2y261nbw4s5lbsdmk5awjnwpipsc0wfgz6") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.7.2 (c (n "libspa-sys") (v "0.7.2") (d (list (d (n "bindgen") (r "^0.66") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "0ifkg4k7azfqbz4i1lk31d77nhlc2w36s133yf78a3z7yfihrrxk") (l "libspa-0.2")))

(define-public crate-libspa-sys-0.8.0 (c (n "libspa-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.69") (f (quote ("experimental" "runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "07yh4i5grzbxkchg6dnxlwbdw2wm5jnd7ffbhl77jr0388b9f3dz") (f (quote (("v0_3_65")))) (l "libspa-0.2")))

