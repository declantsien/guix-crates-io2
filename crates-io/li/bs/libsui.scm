(define-module (crates-io li bs libsui) #:use-module (crates-io))

(define-public crate-libsui-0.0.0 (c (n "libsui") (v "0.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v0w6j473n0j6qfaq8l8bkps1wvx573kzzkz61wi3zp3pnh3b0zd")))

(define-public crate-libsui-0.0.1 (c (n "libsui") (v "0.0.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qar2ch60ll8q15dx639h2vhccv4045w9ya6i33rvjvgqpw0fwl9")))

