(define-module (crates-io li bs libsql_bindgen_macros) #:use-module (crates-io))

(define-public crate-libsql_bindgen_macros-0.2.0 (c (n "libsql_bindgen_macros") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vcmcidxcgssypvsq3m73q8nqqavq8qay1q7cf9yqki7q2b43ji8")))

(define-public crate-libsql_bindgen_macros-0.2.3 (c (n "libsql_bindgen_macros") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "056srna2hzr0c1qsdg6kwc6w3sdsrlxw6g1rfisnp2xqx9maj1wg")))

(define-public crate-libsql_bindgen_macros-0.2.4 (c (n "libsql_bindgen_macros") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1inhl91frykxd7dhwv8w6vajrp2yq6sx0kr677gymphbjd547i7f")))

