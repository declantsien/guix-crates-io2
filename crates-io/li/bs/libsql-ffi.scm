(define-module (crates-io li bs libsql-ffi) #:use-module (crates-io))

(define-public crate-libsql-ffi-0.1.0 (c (n "libsql-ffi") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libsql-wasmtime-bindings") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "02ccpv0lm6ih46gy013cm0pww8jicx2g7xzm4afqw7d3d1gbbqh3") (f (quote (("session")))) (s 2) (e (quote (("wasmtime-bindings" "dep:libsql-wasmtime-bindings"))))))

(define-public crate-libsql-ffi-0.2.0 (c (n "libsql-ffi") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libsql-wasmtime-bindings") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "0id4d99q9nfqs29n3mgc2d30m2x159d7jwn4qllkjx9v516aqg8b") (f (quote (("session") ("multiple-ciphers")))) (s 2) (e (quote (("wasmtime-bindings" "dep:libsql-wasmtime-bindings"))))))

(define-public crate-libsql-ffi-0.2.1 (c (n "libsql-ffi") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libsql-wasmtime-bindings") (r "^0.2.1") (o #t) (d #t) (k 0)))) (h "1f90cpi48fkfxgw39yh2wqh581i6m01dfbzviga0dvc5cl60nypn") (f (quote (("session") ("multiple-ciphers")))) (s 2) (e (quote (("wasmtime-bindings" "dep:libsql-wasmtime-bindings"))))))

