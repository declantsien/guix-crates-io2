(define-module (crates-io li bs libsql_bindgen) #:use-module (crates-io))

(define-public crate-libsql_bindgen-0.1.0 (c (n "libsql_bindgen") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "045yp4d01i5x0pyzhn4mzcb4v1q26pz15xvq5zxdljywhvr35zdh")))

(define-public crate-libsql_bindgen-0.1.1 (c (n "libsql_bindgen") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "06q5d3skv22zbqy3f1qnl591b3vfrcpr82nffyb3y23lds57zz5x")))

(define-public crate-libsql_bindgen-0.1.2 (c (n "libsql_bindgen") (v "0.1.2") (d (list (d (n "libsql_wasm_abi") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1hlzclwnjpiqca4wgprkfa0pn972q181k1lg68c4jxfca6k67086")))

(define-public crate-libsql_bindgen-0.1.3 (c (n "libsql_bindgen") (v "0.1.3") (d (list (d (n "libsql_wasm_abi") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "106zaz5hrzv2d9vbamy51bad03m8g96vczwsaqa5vb23a8cynx4x")))

(define-public crate-libsql_bindgen-0.1.4 (c (n "libsql_bindgen") (v "0.1.4") (d (list (d (n "libsql_wasm_abi") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0wgxbljcffwydhqv9jkglvgyswf12d8x428fc00ii9i13mhilcfa")))

(define-public crate-libsql_bindgen-0.1.5 (c (n "libsql_bindgen") (v "0.1.5") (d (list (d (n "libsql_wasm_abi") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1p3vd2867v026fglk6l1l98k5kwvhv4vhk59pra5nrx6nif4jrv8")))

(define-public crate-libsql_bindgen-0.2.0 (c (n "libsql_bindgen") (v "0.2.0") (d (list (d (n "libsql_bindgen_macros") (r "^0") (d #t) (k 0)))) (h "1x8pc1vcx2f38bvdvvf88zkm0rywb8xmjpd12vzwar5vhb7fn50v")))

(define-public crate-libsql_bindgen-0.2.1 (c (n "libsql_bindgen") (v "0.2.1") (d (list (d (n "libsql_bindgen_macros") (r "^0") (d #t) (k 0)))) (h "07imp6w0y05fhk75zh8k9ywbq139bk72s1cnj8sb1fbamr88qkin")))

(define-public crate-libsql_bindgen-0.2.2 (c (n "libsql_bindgen") (v "0.2.2") (d (list (d (n "libsql_bindgen_macros") (r "^0") (d #t) (k 0)))) (h "0gxz55kpabwn5q3s1yh2c9q6wjr7bl6y9ikd1bqwa3m21r6yblp5")))

(define-public crate-libsql_bindgen-0.2.3 (c (n "libsql_bindgen") (v "0.2.3") (d (list (d (n "libsql_bindgen_macros") (r "^0") (d #t) (k 0)))) (h "08vmd6ck7hp3fqp0wnybp33ln7a23x2vslpm0glny7aq6yakwzm8")))

(define-public crate-libsql_bindgen-0.2.4 (c (n "libsql_bindgen") (v "0.2.4") (d (list (d (n "libsql_bindgen_macros") (r "^0.2.4") (d #t) (k 0)))) (h "0fdm9dfcfm4pzfx35xbkpcrh0m26z6i4vr3hb80hrd1dxxsdksyw")))

(define-public crate-libsql_bindgen-0.2.5 (c (n "libsql_bindgen") (v "0.2.5") (d (list (d (n "libsql_bindgen_macros") (r "^0.2.4") (d #t) (k 0)))) (h "0p0azcblvylcsbbv0aikn0d1y938vy0av5cha4k1dg518jmwcnic")))

(define-public crate-libsql_bindgen-0.2.6 (c (n "libsql_bindgen") (v "0.2.6") (d (list (d (n "libsql_bindgen_macros") (r "^0.2.4") (d #t) (k 0)))) (h "1p0560mxic1japw9hdi4mlm2hc9f55r8hkrk1s7rlx92rcviyclm")))

(define-public crate-libsql_bindgen-0.3.0 (c (n "libsql_bindgen") (v "0.3.0") (d (list (d (n "libsql_bindgen_macros") (r "^0.2.4") (d #t) (k 0)))) (h "11b9amg9h28zrlr23w3vcjd4nb2ihqdwcc6fj8xh135wfwxp6g03")))

(define-public crate-libsql_bindgen-0.3.1 (c (n "libsql_bindgen") (v "0.3.1") (d (list (d (n "libsql_bindgen_macros") (r "^0.2.4") (d #t) (k 0)))) (h "1242b36qhx38gx7aw0qwk5j1l1x48bfv3yj1kjyqi7s2j9g5hdbk")))

