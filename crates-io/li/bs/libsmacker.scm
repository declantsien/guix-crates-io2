(define-module (crates-io li bs libsmacker) #:use-module (crates-io))

(define-public crate-libsmacker-0.1.0 (c (n "libsmacker") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "libsmacker-sys") (r "^0") (d #t) (k 0)))) (h "1fpcc0dr5s45finir495ny1xhs2vm540w8y9vvc9237alv6d1yx7")))

(define-public crate-libsmacker-0.1.1 (c (n "libsmacker") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "enum_primitive") (r "^0.1.1") (d #t) (k 0)) (d (n "libsmacker-sys") (r "^0") (d #t) (k 0)))) (h "0kjxkadvmydcijvim49h5b9mgjcdgz8r9q1dm8gkrz7jlg0fwsng")))

