(define-module (crates-io li bs libsyscall-intercept) #:use-module (crates-io))

(define-public crate-libsyscall-intercept-0.1.0 (c (n "libsyscall-intercept") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0an97zfkdrjb0g6cv9a0487kma504k64gnr1p73ld244d1a31y35")))

(define-public crate-libsyscall-intercept-0.1.1 (c (n "libsyscall-intercept") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nwcqq88syqhv9cvyfbqhcarn75gpczir3ndg39rhcymhv82gglj")))

(define-public crate-libsyscall-intercept-0.1.2 (c (n "libsyscall-intercept") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lx76xd3sa4b6gvr8al6vr5abb31pi6387y7m219xy96xjrz9n4g")))

