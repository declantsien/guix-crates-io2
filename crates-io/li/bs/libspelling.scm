(define-module (crates-io li bs libspelling) #:use-module (crates-io))

(define-public crate-libspelling-0.1.0 (c (n "libspelling") (v "0.1.0") (d (list (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "libspelling-sys")) (d (n "gio") (r "^0.18") (d #t) (k 0)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "gtk") (r "^0.7") (d #t) (k 0) (p "gtk4")) (d (n "gtk_source") (r "^0.7") (d #t) (k 0) (p "sourceview5")) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xjyy7xh5snlk89ygfsiq6b3hy8cqn1ybrq7cil9g154dq2n60w5")))

(define-public crate-libspelling-0.2.0 (c (n "libspelling") (v "0.2.0") (d (list (d (n "ffi") (r "^0.2") (d #t) (k 0) (p "libspelling-sys")) (d (n "gio") (r "^0.19") (d #t) (k 0)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "gtk") (r "^0.8") (d #t) (k 0) (p "gtk4")) (d (n "gtk_source") (r "^0.8") (d #t) (k 0) (p "sourceview5")) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bdmc13l3z9qs06ky6nmg0lgjqj04j7a76am3a2qf3dm5wrrkn1q")))

