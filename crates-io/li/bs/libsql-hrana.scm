(define-module (crates-io li bs libsql-hrana) #:use-module (crates-io))

(define-public crate-libsql-hrana-0.1.0 (c (n "libsql-hrana") (v "0.1.0") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0shiwsw0y8cl0qajf08gcwf9dxfnf4s2acvi0s78114fr72mdwj0")))

(define-public crate-libsql-hrana-0.1.1 (c (n "libsql-hrana") (v "0.1.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0fmbzhlhkl06aqid37fgkmfrnmqz76jza81vaakvz7flwrgr42i2")))

