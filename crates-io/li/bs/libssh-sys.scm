(define-module (crates-io li bs libssh-sys) #:use-module (crates-io))

(define-public crate-libssh-sys-0.1.0 (c (n "libssh-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "17inwz9gp3i6y5rdmmpsk6cffwjgijmlkqn63bb7s25wsfsh4gig") (l "ssh")))

(define-public crate-libssh-sys-0.1.1 (c (n "libssh-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1v9psbk07awb48a23gmhziwnxgfbqb82vrcx5wc0wbgwxq46zbln") (l "ssh")))

