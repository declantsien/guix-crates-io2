(define-module (crates-io li bs libslang-sys) #:use-module (crates-io))

(define-public crate-libslang-sys-0.1.0 (c (n "libslang-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "02awhp7nn57iwcz22is3ajdpcg9j9ivqz8ksw2fzvfzqlsy6gmh0") (y #t) (l "slang")))

(define-public crate-libslang-sys-0.1.1 (c (n "libslang-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0zah20scsfzkhqhbvhqs6ybyl2367lcvqvysdv7y89jp11kf8z31") (y #t) (l "slang")))

(define-public crate-libslang-sys-0.1.2 (c (n "libslang-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1iaz6vsn578a6wqkjh3i6g9y1n7x6cjk7s29k7a7pkyyhqm150s9") (y #t) (l "slang")))

(define-public crate-libslang-sys-0.1.3 (c (n "libslang-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0x9kxn8cj0wmx1n592sk84k1myhb9m5j88csqhyc17c0gmzmizzz") (y #t) (l "slang")))

(define-public crate-libslang-sys-0.1.4 (c (n "libslang-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.58") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.72") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "016xwag3h60bfvabqa7y7lcw49hxxbpzsh143gh41jawxk2zrvml") (y #t) (l "slang")))

