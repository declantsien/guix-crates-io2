(define-module (crates-io li bs libstatgrab-sys) #:use-module (crates-io))

(define-public crate-libstatgrab-sys-0.1.0 (c (n "libstatgrab-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12szgyzdml9nd7qy9pj4qg7vxrb4x135mrn99xw00pk4jqc4dn61") (l "libstatgrab")))

(define-public crate-libstatgrab-sys-0.2.0 (c (n "libstatgrab-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19wwar15fa9pf2d74x78fn3lq95jj1h2zxay0nxkqlk7g6rsr6fa") (l "libstatgrab")))

(define-public crate-libstatgrab-sys-0.3.0 (c (n "libstatgrab-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "05fyn3dxwi4p9c75bnpah7pjv9qc28prkx0xn0086b1sraphlc3r") (l "libstatgrab")))

(define-public crate-libstatgrab-sys-1.0.0 (c (n "libstatgrab-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1zsscwkldhmcvnd2jfnnqvs6xvvgrinap86vwxalnk1ld9x69fxj") (l "libstatgrab")))

(define-public crate-libstatgrab-sys-1.0.1 (c (n "libstatgrab-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0zhd8kp2iwwifds79cy2nh3drwr4da450yi0qj4a0vk4j91f6jx4") (l "libstatgrab")))

(define-public crate-libstatgrab-sys-1.0.2 (c (n "libstatgrab-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0b1imflkrir69c5hlh8g4bs1v9g1fphqy61gaajlk61k4vdxykfl") (l "libstatgrab")))

