(define-module (crates-io li bs libsoxr) #:use-module (crates-io))

(define-public crate-libsoxr-0.1.5 (c (n "libsoxr") (v "0.1.5") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1adq22fxd8lwh5ng8vmncw0jzzhsxyywa9lgqgc7wcqfjk20apki")))

(define-public crate-libsoxr-0.1.6 (c (n "libsoxr") (v "0.1.6") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0swgn5x35pml3isd7dcshb6m0fa6rh4y5l32ld31wzdjgksgzv9v")))

(define-public crate-libsoxr-0.2.0 (c (n "libsoxr") (v "0.2.0") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12alvcx75syrbg909lkgjzsskkajwksfarqbkmwfs81a6crjifwl")))

(define-public crate-libsoxr-0.2.1 (c (n "libsoxr") (v "0.2.1") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1n97rlwjh61iqjawy9847xdrc8lbbclsdmmf51lf8w8hm0anvhy4")))

(define-public crate-libsoxr-0.2.2 (c (n "libsoxr") (v "0.2.2") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jdh4q9wi5bahd7hc04x3y1x7djvp8n8y5bk7jgnfr3piavq64js")))

(define-public crate-libsoxr-0.2.3 (c (n "libsoxr") (v "0.2.3") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "01pxsnjr2jdzizp1nnxjp9xz16llhv0adm5alwd91i2kmp96jps5")))

(define-public crate-libsoxr-0.2.4 (c (n "libsoxr") (v "0.2.4") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07wc9lqq82k0jd55sdrd3pkvfxppxlnkm72dhqgfl50gz0jm9fyl")))

(define-public crate-libsoxr-0.2.5 (c (n "libsoxr") (v "0.2.5") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0s0042yajwm4p483jn122hmb7hk2l5xxaps95gwz43avhilsjzg7")))

(define-public crate-libsoxr-0.2.6 (c (n "libsoxr") (v "0.2.6") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09y45kllqi0hmbwhf1fw8ffjx9h9xccyrk4y8xjqdnps8ah7zrqr")))

(define-public crate-libsoxr-0.2.7 (c (n "libsoxr") (v "0.2.7") (d (list (d (n "approx") (r "^0.4.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1hfvxp37983v4lnpbnqz4hmhvgmh14sblq7j9frr3ha754z1w0py")))

(define-public crate-libsoxr-0.2.8 (c (n "libsoxr") (v "0.2.8") (d (list (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11pkybdhdcri357a985da9021ag9mh34kqx9qrkwc8n908z6cvhs")))

(define-public crate-libsoxr-0.2.9 (c (n "libsoxr") (v "0.2.9") (d (list (d (n "bitflags") (r "^2.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsoxr-sys") (r "^0.1") (d #t) (k 0)) (d (n "approx") (r "^0.5.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "058bss6cv202jg4hpfnyx6kfz97y7yb7q5cbbycw3bkjpv8dk5d0")))

