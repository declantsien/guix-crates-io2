(define-module (crates-io li bs libsodium_seeded_prng) #:use-module (crates-io))

(define-public crate-libsodium_seeded_prng-0.1.1 (c (n "libsodium_seeded_prng") (v "0.1.1") (d (list (d (n "clippy") (r "~0.0.76") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "~0.2.1") (d #t) (k 0)) (d (n "libc") (r "~0.2.12") (d #t) (k 0)) (d (n "quick-error") (r "~1.0.0") (d #t) (k 0)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)) (d (n "sodiumoxide") (r "~0.0.10") (d #t) (k 2)) (d (n "unwrap") (r "~1.1.0") (d #t) (k 0)))) (h "0n6i8bw8idwp7lf5my2r02r03anfkq8a9a14i0pz7vmdlck089xn") (y #t)))

