(define-module (crates-io li bs libsbf) #:use-module (crates-io))

(define-public crate-libsbf-0.1.0 (c (n "libsbf") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "binrw") (r "^0.13.3") (d #t) (k 0)))) (h "0ajcj2r1160jg1sh2dq0006lfzm22v2l2g1kcfk6jfg52rglxl10")))

(define-public crate-libsbf-0.1.1 (c (n "libsbf") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 2)))) (h "1madzlq9csp0kqpfvv9s0k61337yyk0vyyp22l0hnk384kqj6y2r")))

(define-public crate-libsbf-0.2.0 (c (n "libsbf") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 2)))) (h "0h0k6sqdsf5p5pz8aj1a80dzby2iwjvblr44sjglhahb8zvqfjrl")))

(define-public crate-libsbf-0.3.0 (c (n "libsbf") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 2)))) (h "1wd3wxqp4402q6rhxy8dqpg1zx7n623grzwf6kllm09vzc5kpfrr")))

(define-public crate-libsbf-0.4.0 (c (n "libsbf") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 2)))) (h "1ym3c0bbnwnkd9s34drq073gy5kz5hhwqihz205naf88qgapspmm")))

(define-public crate-libsbf-0.4.1 (c (n "libsbf") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 2)) (d (n "binrw") (r "^0.13.3") (d #t) (k 0)) (d (n "crc16") (r "^0.4.0") (d #t) (k 2)))) (h "05y502kja8nqw22f1m12jq7hag0lvqnin2hnz5s3j5bvng9f5br5")))

