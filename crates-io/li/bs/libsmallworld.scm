(define-module (crates-io li bs libsmallworld) #:use-module (crates-io))

(define-public crate-libsmallworld-0.1.0 (c (n "libsmallworld") (v "0.1.0") (d (list (d (n "binread") (r "^2.2.0") (d #t) (k 0)) (d (n "enumflags2") (r "^0.7.5") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "phf") (r "^0.11.0") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0parhfj67hly25m7x79varpmv2ywlk86rjsv4lv264gkm5jicw26")))

