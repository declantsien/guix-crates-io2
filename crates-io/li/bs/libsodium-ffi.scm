(define-module (crates-io li bs libsodium-ffi) #:use-module (crates-io))

(define-public crate-libsodium-ffi-0.1.1 (c (n "libsodium-ffi") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0vdydj812ln2yrb9md3m6c5q9v8sh67hf4vzkl09s3z4nccgx7sh")))

(define-public crate-libsodium-ffi-0.1.2 (c (n "libsodium-ffi") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "03aly6hgqazvjyq305jsnn8r2nxqlq2k4qh9i8v472kbyzzh6s0q")))

(define-public crate-libsodium-ffi-0.1.3 (c (n "libsodium-ffi") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06q5bdciwdiq9chsvpi2qj39bizj8lkdf79w3bs3kyskjdbc67d2")))

(define-public crate-libsodium-ffi-0.1.4 (c (n "libsodium-ffi") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "02jabivmnw5pyz5g5wqhf8hq5cqxw98ndycdxdllyqab9vbg97ns")))

(define-public crate-libsodium-ffi-0.1.5 (c (n "libsodium-ffi") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dj3dqigp54f2fxil61gmsh4sgmg5iyv3wqjbla1ksms54g8vdaz")))

(define-public crate-libsodium-ffi-0.1.6 (c (n "libsodium-ffi") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "04ry77zdsyx6k2h23nb1jbnwzciqflks00p1vywf7wmx7d2yapqk")))

(define-public crate-libsodium-ffi-0.1.7 (c (n "libsodium-ffi") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17gndrvknj189zzlj7gxfss7gwf80immwwqq2cvsarr98wylph90")))

(define-public crate-libsodium-ffi-0.1.8 (c (n "libsodium-ffi") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ncndmcfbf8l9q5000iv3hx05zvwxsxlr11wp23yyczjskp2cgxn")))

(define-public crate-libsodium-ffi-0.1.9 (c (n "libsodium-ffi") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ajx4hilrjzbhdjwqc41c4igfd9i0yvcpbcs9w516cgpd54mqzwg")))

(define-public crate-libsodium-ffi-0.1.11 (c (n "libsodium-ffi") (v "0.1.11") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "flate2") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.1") (d #t) (k 1)) (d (n "zip") (r "^0.2") (d #t) (k 1)))) (h "07br4dwa1zirhq92ijyrzs76dsj6lkf3kaxdw7s5j2p314ha1ahj")))

(define-public crate-libsodium-ffi-0.1.12 (c (n "libsodium-ffi") (v "0.1.12") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "flate2") (r "^0.2") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)) (d (n "zip") (r "^0.2") (d #t) (k 1)))) (h "0kpwr8g1f8hy2ag2sqlxjxva51rbf57g567l2br3qzgsqn86dcv8") (l "sodium")))

(define-public crate-libsodium-ffi-0.1.13 (c (n "libsodium-ffi") (v "0.1.13") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "flate2") (r "^0.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)) (d (n "zip") (r "^0.2") (d #t) (k 1)))) (h "164sn5n9q7xy45w5x5cawcr95jivzjkx4l4xm4q2y0n0cli6kq8g") (l "sodium")))

(define-public crate-libsodium-ffi-0.1.14 (c (n "libsodium-ffi") (v "0.1.14") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "flate2") (r "^0.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)) (d (n "zip") (r "^0.2") (d #t) (k 1)))) (h "055hg67n5i8n2hjfryf5n83qr0vr1gw746w06l9ibh3lf5ifcd2i") (y #t) (l "sodium")))

(define-public crate-libsodium-ffi-0.1.15 (c (n "libsodium-ffi") (v "0.1.15") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "flate2") (r "^0.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)) (d (n "zip") (r "^0.2") (d #t) (k 1)))) (h "0basp4gswzy0vrn56zs5jr8sb4rgws731khrfvbjnz4cf5zbd55w") (l "sodium")))

(define-public crate-libsodium-ffi-0.1.16 (c (n "libsodium-ffi") (v "0.1.16") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "flate2") (r "^0.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)) (d (n "zip") (r "^0.2") (d #t) (k 1)))) (h "0r6zxw68pf5cf99snn79z2xg8ngrbldr9kfyqfmsch6gm6cv9igi") (l "sodium")))

(define-public crate-libsodium-ffi-0.1.17 (c (n "libsodium-ffi") (v "0.1.17") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "flate2") (r "^0.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.1") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)) (d (n "zip") (r "^0.2") (d #t) (k 1)))) (h "05cyfhkm7lkhh7cr6xbf3a59xks2p3g66dcp8cfynh3qh2r6y1lh") (l "sodium")))

(define-public crate-libsodium-ffi-0.2.0 (c (n "libsodium-ffi") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.2") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "017m0vak913zm1mg3635x2rrrp4ca23zna57rzkw0lrvpx4sgkdg") (y #t) (l "sodium")))

(define-public crate-libsodium-ffi-0.2.1 (c (n "libsodium-ffi") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.2") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "0rzwgir6mks2rx83935y27q3ml3f2jsfka5d2mz71781s093c6l4") (l "sodium")))

(define-public crate-libsodium-ffi-0.2.2 (c (n "libsodium-ffi") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.2") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "1wwmgypnz0ia78alc3074qcjgh7v4q72hk710bvprvlmn1br4m2k") (l "sodium")))

