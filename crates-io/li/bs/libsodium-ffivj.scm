(define-module (crates-io li bs libsodium-ffivj) #:use-module (crates-io))

(define-public crate-libsodium-ffivj-0.2.3 (c (n "libsodium-ffivj") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "curl") (r "^0.4") (d #t) (k 1)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "unwrap") (r "^1.2") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)) (d (n "zip") (r "^0.6") (d #t) (k 1)))) (h "1qqwr8riln5wz5nvwrrnkwq2rfbqzfmnj5b6mkwj6wvpgbv13pym") (l "sodium")))

