(define-module (crates-io li bs libsdp) #:use-module (crates-io))

(define-public crate-libsdp-0.1.0 (c (n "libsdp") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.1") (d #t) (k 0)))) (h "1gcramxa5qha0mprlb8j959b0faawan33k3q4q9xd43rf0mr0gqv")))

(define-public crate-libsdp-0.1.1 (c (n "libsdp") (v "0.1.1") (d (list (d (n "nom") (r "^5.1.0") (d #t) (k 0)))) (h "16i1qwv92hs5qb7gxl26nzgqdh3sri6sz2ng72w0agg285rhwrih")))

(define-public crate-libsdp-0.1.2 (c (n "libsdp") (v "0.1.2") (d (list (d (n "nom") (r "^6.0.0-alpha1") (d #t) (k 0)))) (h "0kz1h1xq0f2am9bwyndy2rgcg0xx22dkw84r6yxm06a8di8msv0w")))

