(define-module (crates-io li bs libsync) #:use-module (crates-io))

(define-public crate-libsync-0.1.0 (c (n "libsync") (v "0.1.0") (d (list (d (n "crossbeam") (r "0.8.*") (o #t) (d #t) (k 0)) (d (n "delegate") (r "0.*") (d #t) (k 0)) (d (n "futures") (r "0.3.*") (d #t) (k 0)) (d (n "tokio") (r "1.*") (f (quote ("sync" "time"))) (o #t) (d #t) (k 0)))) (h "1mzh414pynb6pm78rbwg46prhrrqs7cgipdrs4sr1fvp64cv4lgk") (f (quote (("std") ("count_waiting_senders_and_receivers"))))))

