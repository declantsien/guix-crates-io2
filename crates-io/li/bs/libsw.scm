(define-module (crates-io li bs libsw) #:use-module (crates-io))

(define-public crate-libsw-0.1.0 (c (n "libsw") (v "0.1.0") (h "10d78badxjz82vq3yjp53g6y0kjbhdc1hfy7xszk7mr4jid7xwwv")))

(define-public crate-libsw-0.1.1 (c (n "libsw") (v "0.1.1") (h "1rr5dv9kwpdlm01n3mmqbh0c1wp0m7fi9jwnsb7gdpvw250g091h")))

(define-public crate-libsw-0.2.0 (c (n "libsw") (v "0.2.0") (h "196wnzpr23r6gk6j3kavjq67yfmp0pk2cpdg27ll2374dwq23x68")))

(define-public crate-libsw-1.0.0 (c (n "libsw") (v "1.0.0") (h "0djdfi6cxvjsxix0bh657x6f2wnvj6rnwwmmr0vbi4pcf3l669xz")))

(define-public crate-libsw-1.1.0 (c (n "libsw") (v "1.1.0") (h "0lh3lzh0xgg8bqwgwkak1gagxdqs0vc6swjidcp2h87y5swjbbkv")))

(define-public crate-libsw-1.2.0 (c (n "libsw") (v "1.2.0") (h "1am8yfxdaghc448015wvdknqipfx95pbmiams02nghm4q3avinj2")))

(define-public crate-libsw-1.3.0 (c (n "libsw") (v "1.3.0") (h "0r5qfa6qfhb4q040qmyh9f65ya8zw86vpkj3zzxd759j72aa8mp9")))

(define-public crate-libsw-1.4.0 (c (n "libsw") (v "1.4.0") (h "1n41nvxh7fmbmchlbc50539civdahbcxn506bjdn9a92ka9xwgq0")))

(define-public crate-libsw-1.5.0 (c (n "libsw") (v "1.5.0") (h "0kf7285v49wgfn5km14wvh4zsspdqpzi41i930wdbq1nk294dg76")))

(define-public crate-libsw-1.6.0 (c (n "libsw") (v "1.6.0") (h "1l4f55gpb0553r39kd207gymaqfvgvz1m4j6zdqrh82d29a7kp2j")))

(define-public crate-libsw-1.7.0 (c (n "libsw") (v "1.7.0") (h "0wk1vmwnk5hxh8xd76c9p8fvwi3zgwvp6z83jmzvggsq636wmfkg") (r "1.58.1")))

(define-public crate-libsw-1.8.0 (c (n "libsw") (v "1.8.0") (h "1gd7v8q9nhyilbb4xc2qf46s4yqy501lgcq6n2br8bsa1hrqbigg") (r "1.58.1")))

(define-public crate-libsw-1.8.1 (c (n "libsw") (v "1.8.1") (h "1d9lkff8fq57kx43fq9p9d6asr75zacg778wwjz00m0gash00m3r") (r "1.58.1")))

(define-public crate-libsw-1.8.2 (c (n "libsw") (v "1.8.2") (h "1ifidij5hpk066vw9b5jjnyhyh2vsbjkki0ha113350y2rd5zw14") (r "1.58.1")))

(define-public crate-libsw-2.0.0 (c (n "libsw") (v "2.0.0") (h "13amggim613n91y2rraiwnmmlvakjil729i9c8mirb54b4s1aiw5") (r "1.58.1")))

(define-public crate-libsw-2.0.1 (c (n "libsw") (v "2.0.1") (h "0qc53nv72abz2pmn2zamdcn7h0ihw1z6hbhm3ccmsdx3iwv2nzbm") (r "1.58.1")))

(define-public crate-libsw-2.0.2 (c (n "libsw") (v "2.0.2") (h "0pby6mw8vw1l7rlvr9mmj4wy5fsmffqmw8qks633ay9papai8h77") (r "1.58.1")))

(define-public crate-libsw-2.1.0 (c (n "libsw") (v "2.1.0") (h "1cw8s5zds3x9d3mi1540914j5yyx241kg9ci037z0jyqkbgnl7pr") (r "1.58.1")))

(define-public crate-libsw-2.1.1 (c (n "libsw") (v "2.1.1") (h "12mc313amsifdaviw4lhgxfw4aniw7sbrqbnxk47wiba2swjnidh") (r "1.58.1")))

(define-public crate-libsw-2.2.0 (c (n "libsw") (v "2.2.0") (h "1jhpqnwvz73zwf1mzpgwiq18al61h3md9pijdqmnx2wqrmrv70cx") (r "1.58.1")))

(define-public crate-libsw-3.0.0 (c (n "libsw") (v "3.0.0") (d (list (d (n "tokio") (r "^1.25.0") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "0pf12w5ds8qnj2gs0l9paji3vrcnqakfqsj9xwpqbhwgvp0pdazn") (f (quote (("std_systemtime" "std") ("std_instant" "std") ("std") ("nightly") ("default" "std_instant" "std_systemtime")))) (s 2) (e (quote (("tokio" "dep:tokio" "std")))) (r "1.61.0")))

(define-public crate-libsw-3.0.1 (c (n "libsw") (v "3.0.1") (d (list (d (n "tokio") (r "^1.25.0") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "0gnfwq522h1kyjz3w083snzh8iab6wab68scb3sqzckd1q7s1mr6") (f (quote (("std_systemtime" "std") ("std_instant" "std") ("std") ("nightly") ("default" "std_instant" "std_systemtime")))) (s 2) (e (quote (("tokio" "dep:tokio" "std")))) (r "1.61.0")))

(define-public crate-libsw-3.1.0 (c (n "libsw") (v "3.1.0") (d (list (d (n "coarsetime") (r "^0.1.22") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.19") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "12c10k9fyc5i3r1gq8bs21qj2s49rqgnyvlxyn9vm3q71z6bj8ch") (f (quote (("std_systemtime" "std") ("std_instant" "std") ("std") ("nightly") ("default" "std_instant" "std_systemtime")))) (s 2) (e (quote (("tokio" "dep:tokio" "std") ("time" "dep:time" "std") ("coarsetime" "dep:coarsetime" "std")))) (r "1.61.0")))

(define-public crate-libsw-3.2.0 (c (n "libsw") (v "3.2.0") (d (list (d (n "coarsetime") (r "^0.1.22") (o #t) (d #t) (k 0)) (d (n "quanta") (r "^0.10.1") (o #t) (d #t) (k 0)) (d (n "time") (r "^0.3.19") (f (quote ("std"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("time"))) (o #t) (d #t) (k 0)))) (h "062qh96qv512dvinkbrlw7yflg5c8hcvgjnrnr272bli9151ysxg") (f (quote (("std_systemtime" "std") ("std_instant" "std") ("std") ("nightly") ("default" "std_instant" "std_systemtime")))) (s 2) (e (quote (("tokio" "dep:tokio" "std") ("time" "dep:time" "std") ("quanta" "dep:quanta" "std") ("coarsetime" "dep:coarsetime" "std")))) (r "1.61.0")))

(define-public crate-libsw-3.2.1 (c (n "libsw") (v "3.2.1") (d (list (d (n "coarsetime") (r "^0.1") (o #t) (k 0)) (d (n "quanta") (r "^0.11") (o #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (o #t) (k 0)))) (h "1kyvg2gpl6niakm7fa098b5j4z8ydzncy4a52595lca0hf7jisv4") (f (quote (("std_systemtime" "std") ("std_instant" "std") ("std") ("nightly") ("default" "std_instant" "std_systemtime")))) (s 2) (e (quote (("tokio" "dep:tokio" "std") ("time" "dep:time" "std") ("quanta" "dep:quanta" "std") ("coarsetime" "dep:coarsetime" "std")))) (r "1.61.0")))

(define-public crate-libsw-3.2.2 (c (n "libsw") (v "3.2.2") (d (list (d (n "coarsetime") (r "^0.1") (o #t) (k 0)) (d (n "quanta") (r "^0.11") (o #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (o #t) (k 0)))) (h "1r6cq2d2hajj3c6jb9vx0rs6q5xq4377a2545lma4kf9x08w13j8") (f (quote (("std_systemtime" "std") ("std_instant" "std") ("std") ("nightly") ("default" "std_instant" "std_systemtime")))) (s 2) (e (quote (("tokio" "dep:tokio" "std") ("time" "dep:time" "std") ("quanta" "dep:quanta" "std") ("coarsetime" "dep:coarsetime" "std")))) (r "1.61.0")))

(define-public crate-libsw-3.2.3 (c (n "libsw") (v "3.2.3") (d (list (d (n "coarsetime") (r "^0.1") (o #t) (k 0)) (d (n "quanta") (r "^0.11") (o #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (o #t) (k 0)))) (h "1rfj2mq5r7968brwg6rqa8ng0yz55azdk3ix2ipdz0b2fgy1mmvk") (f (quote (("std_systemtime" "std") ("std_instant" "std") ("std") ("nightly") ("default" "std_instant" "std_systemtime")))) (s 2) (e (quote (("tokio" "dep:tokio" "std") ("time" "dep:time" "std") ("quanta" "dep:quanta" "std") ("coarsetime" "dep:coarsetime" "std")))) (r "1.61.0")))

(define-public crate-libsw-3.2.4 (c (n "libsw") (v "3.2.4") (d (list (d (n "coarsetime") (r "^0.1") (o #t) (k 0)) (d (n "quanta") (r "^0.11") (o #t) (k 0)) (d (n "time") (r "^0.3") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("time"))) (o #t) (k 0)))) (h "0cqcf34aq374mz4ybllghda0aj4fdafz72zqvjf9i11pf9rxyqfs") (f (quote (("std_systemtime" "std") ("std_instant" "std") ("std") ("nightly") ("default" "std_instant" "std_systemtime")))) (s 2) (e (quote (("tokio" "dep:tokio" "std") ("time" "dep:time" "std") ("quanta" "dep:quanta" "std") ("coarsetime" "dep:coarsetime" "std")))) (r "1.61.0")))

(define-public crate-libsw-3.3.0 (c (n "libsw") (v "3.3.0") (d (list (d (n "coarsetime") (r "~0.1") (o #t) (k 0)) (d (n "quanta") (r "~0.11") (o #t) (k 0)) (d (n "time") (r "~0.3") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "~1") (f (quote ("time"))) (o #t) (k 0)))) (h "1ychw5gy59k8b5civhi81qd58kf4cd95g2s9apw4lqsgs8lxj331") (f (quote (("std_systemtime" "std") ("std_instant" "std") ("std") ("nightly") ("default" "std_instant" "std_systemtime")))) (s 2) (e (quote (("tokio" "dep:tokio" "std") ("time" "dep:time" "std") ("quanta" "dep:quanta" "std") ("coarsetime" "dep:coarsetime" "std")))) (r "1.61.0")))

(define-public crate-libsw-3.3.1 (c (n "libsw") (v "3.3.1") (d (list (d (n "coarsetime") (r "~0.1") (o #t) (k 0)) (d (n "quanta") (r "~0.12") (o #t) (k 0)) (d (n "time") (r "~0.3") (f (quote ("std"))) (o #t) (k 0)) (d (n "tokio") (r "~1") (f (quote ("time"))) (o #t) (k 0)))) (h "1kjgddfs5ampxq0iyi96c4yi0m2k86i9pbfv8516g8gp3r63cwq6") (f (quote (("std_systemtime" "std") ("std_instant" "std") ("std") ("nightly") ("default" "std_instant" "std_systemtime")))) (s 2) (e (quote (("tokio" "dep:tokio" "std") ("time" "dep:time" "std") ("quanta" "dep:quanta" "std") ("coarsetime" "dep:coarsetime" "std")))) (r "1.61.0")))

