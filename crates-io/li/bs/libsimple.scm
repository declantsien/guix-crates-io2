(define-module (crates-io li bs libsimple) #:use-module (crates-io))

(define-public crate-libsimple-0.1.0 (c (n "libsimple") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "rusqlite") (r "^0.31") (f (quote ("load_extension"))) (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled-sqlcipher"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)) (d (n "thiserror") (r "~1.0") (d #t) (k 0)))) (h "0kf39g3hcb407yz54accnmppxvyyhccdw176rka0q9ljbbf4afh0")))

(define-public crate-libsimple-0.2.0 (c (n "libsimple") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rusqlite") (r "^0.31") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled-sqlcipher"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)))) (h "10s2r9q7knj2gfv593l8ksjmpjsfd6cfhf3hc40nkn7nfr3hw283") (f (quote (("jieba") ("default" "jieba")))) (y #t)))

(define-public crate-libsimple-0.2.1 (c (n "libsimple") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rusqlite") (r "^0.31") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled-sqlcipher"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)))) (h "0d0g6gc4l06rnhb55hdb2i2ppbvg48n20nlm3i96bfs9l4y8y4f9") (f (quote (("jieba") ("default" "jieba"))))))

(define-public crate-libsimple-0.2.2 (c (n "libsimple") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rusqlite") (r "^0.31") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled-sqlcipher"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)))) (h "120jbp8j1kjjydiqlycy8vdma2h8dnkavga9p17hd82i0y3zbhpv") (f (quote (("jieba") ("default" "jieba"))))))

(define-public crate-libsimple-0.3.0 (c (n "libsimple") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "rusqlite") (r "^0.31") (d #t) (k 0)) (d (n "rusqlite") (r "^0.31") (f (quote ("bundled-sqlcipher"))) (d #t) (k 2)) (d (n "tempfile") (r "^3.10") (d #t) (k 2)))) (h "0f3xp3f6g9bq251cb5cwq3ggx94c0576bmx7rsykj2hw1zncnr6d") (f (quote (("jieba") ("default" "jieba"))))))

