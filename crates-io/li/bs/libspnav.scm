(define-module (crates-io li bs libspnav) #:use-module (crates-io))

(define-public crate-libspnav-0.1.0 (c (n "libspnav") (v "0.1.0") (d (list (d (n "libspnav-sys") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0zq4v1hlfnngzl20qp3n7qv3islfd8gzxp46yc3l1a9ll6hiqk9i") (f (quote (("serde-serialize" "serde"))))))

