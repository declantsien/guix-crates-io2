(define-module (crates-io li bs libsql-sys) #:use-module (crates-io))

(define-public crate-libsql-sys-0.1.0 (c (n "libsql-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)))) (h "1kaya407gz24xmbznn8hx2gbm2lg1prsf8r8xa5sq27dqh0zabms")))

(define-public crate-libsql-sys-0.1.1 (c (n "libsql-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)))) (h "12hhfpm1wpr2a4fdqdyrqyj6hl0nr6bykg4xxga6qx8fx23p4aba")))

(define-public crate-libsql-sys-0.2.0 (c (n "libsql-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "06f1fhwpjc13m33cvrns4km0ywxrs0jglhh3n2dmlf193hi5z2k7")))

(define-public crate-libsql-sys-0.2.1 (c (n "libsql-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1b10haqfcbqrnzzybr2d5znaa1l7y4s2jhmkq5rpn7v8bhdq0z26")))

(define-public crate-libsql-sys-0.2.2 (c (n "libsql-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "05v7vl6ff0p49jhjaawwvh8i8fd6cqzy4ppxhb9k4db2gkhaxdp4")))

(define-public crate-libsql-sys-0.2.3 (c (n "libsql-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1nx497j0w3hzzrg2i39dl5nphn22lamrwwk0l07lhfigiyc4d8b0")))

(define-public crate-libsql-sys-0.2.4 (c (n "libsql-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "10fmz2jjbdj6ygrh9r3an3vr417v9akrdn2lah16fzdyjd6y537a")))

(define-public crate-libsql-sys-0.2.5 (c (n "libsql-sys") (v "0.2.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "02xig71alw7s62qdb0b5yhgykpybhdqvg49zaa2zhqypyqp67mb2")))

(define-public crate-libsql-sys-0.2.6 (c (n "libsql-sys") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1zk56wvgrd4m84xr6rhzsiaydz8pwmr4jmrcfvid5j3yly0fab5d")))

(define-public crate-libsql-sys-0.2.7 (c (n "libsql-sys") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0cmjj89d3m4cqvrkrqbvdpcl2cnpr8g721n9hk920mj5jxprrf66")))

(define-public crate-libsql-sys-0.2.8 (c (n "libsql-sys") (v "0.2.8") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0qdn91c7cjh17m6l938wikb1hmc2l9k5ravyqxvnhidibfwgzjwk")))

(define-public crate-libsql-sys-0.2.9 (c (n "libsql-sys") (v "0.2.9") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "08qx64mzrpaxb0z309p0pxm07d1y049vkgagsywmmyanqmwfm6z8")))

(define-public crate-libsql-sys-0.2.10 (c (n "libsql-sys") (v "0.2.10") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "06j9dykrpizyzfiksawp9zvj0aynvfwhf5qcjrlwl5p9rx5cigrl")))

(define-public crate-libsql-sys-0.2.11 (c (n "libsql-sys") (v "0.2.11") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0pl47k0gdg58f7mkx38w1z41zqjyx50ps5pyzr7s4bvh66mn3y0a")))

(define-public crate-libsql-sys-0.2.12 (c (n "libsql-sys") (v "0.2.12") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0wnb08bl21bww10qf3av29jm883dy1kf4dh9j7i6fr5lv57fddqc")))

(define-public crate-libsql-sys-0.2.13 (c (n "libsql-sys") (v "0.2.13") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "1ls80qhsi55880975xyd4diwsdjiczgma8ggjn6cikvfkgmx4afr")))

(define-public crate-libsql-sys-0.2.14 (c (n "libsql-sys") (v "0.2.14") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "default-env") (r "^0.1.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0368n28bmfs5smwcpbl556akfy3n4kbi009qfzmmpd5cak2rdzxy")))

(define-public crate-libsql-sys-0.3.0 (c (n "libsql-sys") (v "0.3.0") (d (list (d (n "libsql-ffi") (r "^0.1") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (f (quote ("libsql-experimental" "column_decltype" "load_extension" "modern_sqlite" "trace"))) (o #t) (k 0) (p "libsql-rusqlite")) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.28") (f (quote ("derive"))) (d #t) (k 0)))) (h "0q32c3jvp5k9m9ih572cdqgdmhj60gfcjl62vv3csizabq6y20pg") (f (quote (("wasmtime-bindings" "libsql-ffi/wasmtime-bindings") ("wal") ("unix-excl-vfs") ("default" "api") ("api" "wal")))) (s 2) (e (quote (("rusqlite" "dep:rusqlite"))))))

(define-public crate-libsql-sys-0.4.0 (c (n "libsql-sys") (v "0.4.0") (d (list (d (n "base64") (r "^0.21") (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "libsql-ffi") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "prost") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (f (quote ("libsql-experimental" "column_decltype" "load_extension" "modern_sqlite" "functions" "limits" "trace"))) (o #t) (k 0) (p "libsql-rusqlite")) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.28") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x57mng8vp1c691vwyw6y2dyaablfxzmlda5ganmpmz44lmfyq72") (f (quote (("wasmtime-bindings" "libsql-ffi/wasmtime-bindings") ("wal") ("unix-excl-vfs") ("encryption" "libsql-ffi/multiple-ciphers") ("default" "api") ("api" "wal")))) (s 2) (e (quote (("serde" "dep:serde") ("rusqlite" "dep:rusqlite") ("hrana" "serde" "dep:prost" "dep:base64"))))))

(define-public crate-libsql-sys-0.5.0 (c (n "libsql-sys") (v "0.5.0") (d (list (d (n "bytes") (r "^1.5.0") (d #t) (k 0)) (d (n "libsql-ffi") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (f (quote ("libsql-experimental" "column_decltype" "load_extension" "modern_sqlite" "functions" "limits" "trace"))) (o #t) (k 0) (p "libsql-rusqlite")) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7.28") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lgbdcpiz95nhhpzxhzg104qvrrda08b63nbwsfl4614m1cgfaaq") (f (quote (("wasmtime-bindings" "libsql-ffi/wasmtime-bindings") ("wal") ("unix-excl-vfs") ("encryption" "libsql-ffi/multiple-ciphers") ("default" "api") ("api" "wal")))) (s 2) (e (quote (("rusqlite" "dep:rusqlite"))))))

