(define-module (crates-io li bs libsyslog) #:use-module (crates-io))

(define-public crate-libsyslog-0.0.0 (c (n "libsyslog") (v "0.0.0") (h "0y4hnjyij1ca3p1v3yzzqz7c7lrs8bbylc3p30npkkp3wv89mn2i")))

(define-public crate-libsyslog-0.1.0 (c (n "libsyslog") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.0.0-rc.3") (o #t) (d #t) (k 0)) (d (n "libsyslog-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)))) (h "132qdjbzxm1c04vacygf1vqmm2qvarxjjw5v0d8393j7ixl9kxin") (y #t) (s 2) (e (quote (("bitflags" "dep:bitflags"))))))

(define-public crate-libsyslog-0.1.1 (c (n "libsyslog") (v "0.1.1") (d (list (d (n "bitflags") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "libsyslog-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (f (quote ("std"))) (d #t) (k 0)))) (h "0rq5hkvygbbymxpfcnki4lwl5p3l1awg7hh8b2pnfdcgfx9bls48") (f (quote (("default" "bitflags")))) (s 2) (e (quote (("bitflags" "dep:bitflags"))))))

