(define-module (crates-io li bs libsamplerate-sys) #:use-module (crates-io))

(define-public crate-libsamplerate-sys-0.1.0 (c (n "libsamplerate-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)))) (h "1mhin7vfgdfwmna92rswlhsi8yldmcjij7c6vcrcr30083zjfgw8")))

(define-public crate-libsamplerate-sys-0.1.1 (c (n "libsamplerate-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)))) (h "1w3y16l80iig69fsbxlnk11a5y32prfxc25m56axhcj2h8d42ixf")))

(define-public crate-libsamplerate-sys-0.1.2 (c (n "libsamplerate-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "pathdiff") (r "^0.1") (d #t) (k 1)))) (h "08j0g5d39svvz0c3lm19w6jip4ls0f5gcfzfqlqd63a47s19qjnn")))

(define-public crate-libsamplerate-sys-0.1.3 (c (n "libsamplerate-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "pathdiff") (r "^0.1") (d #t) (k 1)))) (h "0n844y1fqhl9lsnlw7l2gqxwyky8d1cdxq4li1wlma1sqid3zmdj")))

(define-public crate-libsamplerate-sys-0.1.4 (c (n "libsamplerate-sys") (v "0.1.4") (d (list (d (n "all_asserts") (r "^0.1.4") (d #t) (k 2)) (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "pathdiff") (r "^0.1") (d #t) (k 1)))) (h "1zhz1s5hrzivpk8flpz98hqd7wplz705k2wm2ard6ia9vq9x95c9")))

(define-public crate-libsamplerate-sys-0.1.5 (c (n "libsamplerate-sys") (v "0.1.5") (d (list (d (n "all_asserts") (r "^0.1.4") (d #t) (k 2)) (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "pathdiff") (r "^0.1") (d #t) (k 1)))) (h "0whf6ikxjnra8dzbdpx30540c896b55sbmx4kz7r0dd20ly11s4y")))

(define-public crate-libsamplerate-sys-0.1.6 (c (n "libsamplerate-sys") (v "0.1.6") (d (list (d (n "all_asserts") (r "^0.1.4") (d #t) (k 2)) (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "pathdiff") (r "^0.1") (d #t) (k 1)))) (h "0fiy7gscis0vabk75ls8chb0h1a8xi8n0vbkx7gzniryipqscr9i")))

(define-public crate-libsamplerate-sys-0.1.7 (c (n "libsamplerate-sys") (v "0.1.7") (d (list (d (n "all_asserts") (r "^0.1.4") (d #t) (k 2)) (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "pathdiff") (r "^0.1") (d #t) (k 1)))) (h "0jl5x6dprc2jpv1fq2y41zhrq0s9748mbdn16h0fg1hjpdbaw193")))

(define-public crate-libsamplerate-sys-0.1.8 (c (n "libsamplerate-sys") (v "0.1.8") (d (list (d (n "all_asserts") (r "^0.1.4") (d #t) (k 2)) (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1sq94dhajllnahwpr7d0ghkxh9jn24g2pplhgxvgh54j90fmsmv6")))

(define-public crate-libsamplerate-sys-0.1.9 (c (n "libsamplerate-sys") (v "0.1.9") (d (list (d (n "all_asserts") (r "^0.1.4") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1k5qchnv15xw2178cnpr0b3j8idx79d1nid4cai3ic6g9b5b8w29")))

(define-public crate-libsamplerate-sys-0.1.10 (c (n "libsamplerate-sys") (v "0.1.10") (d (list (d (n "all_asserts") (r "^2.2.0") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0lwnxfj9qv2r5r3gaayxz32nccbzvp6h8p14sq67idbmiqjn6ca0")))

(define-public crate-libsamplerate-sys-0.1.11 (c (n "libsamplerate-sys") (v "0.1.11") (d (list (d (n "all_asserts") (r "^2.2.0") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "1b7qs2v5k2k4mc6b4anvlv525fy0scggwgbd89wv8m4xh49p3qmw") (f (quote (("static" "cmake") ("default"))))))

(define-public crate-libsamplerate-sys-0.1.12 (c (n "libsamplerate-sys") (v "0.1.12") (d (list (d (n "all_asserts") (r "^2.2.0") (d #t) (k 2)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "039flz6abgm24rksdbkgbwkw8w1ilra3ncwdv0f2iy3qkwwkp198")))

