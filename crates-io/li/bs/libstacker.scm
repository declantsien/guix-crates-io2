(define-module (crates-io li bs libstacker) #:use-module (crates-io))

(define-public crate-libstacker-0.0.1 (c (n "libstacker") (v "0.0.1") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "opencv") (r "^0.58") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "00baajnar2hqlc9bjzn3rj25nzlbgxarkxjgmv61vkvs7066idfk")))

(define-public crate-libstacker-0.0.2 (c (n "libstacker") (v "0.0.2") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "opencv") (r "^0.58") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0p4kpz0sfi762nzdg672kxm4gfm4jai5wxqs3khhn4i0cq0jsvcf")))

(define-public crate-libstacker-0.0.3 (c (n "libstacker") (v "0.0.3") (d (list (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "opencv") (r "^0.58") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "072b15fswa9lr2qp0ijw7jkxyf75avbz5i8wvdapbggp1myny330")))

(define-public crate-libstacker-0.0.4 (c (n "libstacker") (v "0.0.4") (d (list (d (n "opencv") (r "^0.59") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "076a50z23fgg3690gpxbpw9f4jajsx9v94gyvbizkaw14wph6waj")))

(define-public crate-libstacker-0.0.5 (c (n "libstacker") (v "0.0.5") (d (list (d (n "opencv") (r "^0.59") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11vi33py0lih8ilfxfwgmlrkn7xadgmj94pz78n1sqpx0yazjkxv")))

(define-public crate-libstacker-0.0.6 (c (n "libstacker") (v "0.0.6") (d (list (d (n "opencv") (r "^0.61") (d #t) (k 0)) (d (n "ordered-float") (r "^2.8") (d #t) (k 0)) (d (n "rayon") (r "^1.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1pnllcgcah3a8nhzcakrxw0xsn2arzqd82qi4dgy2hik89izgaiq")))

(define-public crate-libstacker-0.0.7 (c (n "libstacker") (v "0.0.7") (d (list (d (n "opencv") (r "^0.85.3") (f (quote ("features2d"))) (d #t) (k 0)) (d (n "ordered-float") (r "^4.1.1") (d #t) (k 0)) (d (n "rayon") (r "^1.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1c62j9348rhwrn57l1iv2l8gni8k4xj2i7m512wqvgzqna41y9x1") (f (quote (("clang-runtime" "opencv/clang-runtime"))))))

