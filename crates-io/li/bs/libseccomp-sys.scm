(define-module (crates-io li bs libseccomp-sys) #:use-module (crates-io))

(define-public crate-libseccomp-sys-0.1.0 (c (n "libseccomp-sys") (v "0.1.0") (h "174ri0mqnd025n8x4piapd9r1byg9j5lx12lw11l7g0pjz50xbkm")))

(define-public crate-libseccomp-sys-0.1.1 (c (n "libseccomp-sys") (v "0.1.1") (h "0xf4m9r48d5d8lhckdmmlljaaf2074mg33bya1pc2kws4bg156ak")))

(define-public crate-libseccomp-sys-0.2.0 (c (n "libseccomp-sys") (v "0.2.0") (h "08pr45a321x8k59lgvmh1n2nkiz1ala5d2z436qnird6d2fxbb9k")))

(define-public crate-libseccomp-sys-0.2.1 (c (n "libseccomp-sys") (v "0.2.1") (h "0f6iw3qsww1dkrx49wh8vmda198i7galfnvfgjc52wj6mpabnz4s")))

