(define-module (crates-io li bs libsnark) #:use-module (crates-io))

(define-public crate-libsnark-1.1.2 (c (n "libsnark") (v "1.1.2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "zkinterface") (r "^1.1.2") (d #t) (k 0)))) (h "0wzh81x94b9pjnywf0x6gnmlr364cg2giv1wqhx8mkwgpsq2vihs") (y #t) (l "zkif_gadgetlib")))

