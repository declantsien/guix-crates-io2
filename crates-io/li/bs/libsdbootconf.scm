(define-module (crates-io li bs libsdbootconf) #:use-module (crates-io))

(define-public crate-libsdbootconf-0.1.0 (c (n "libsdbootconf") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0rhnfh7gyna1phh8lch0y2q61w4f7p9kqccnr2rhs7hfbwa55ni4")))

(define-public crate-libsdbootconf-0.2.0 (c (n "libsdbootconf") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "066k98s94csa1phpxidkildjl0ghdan5qbw6nf8aaqi2l3kfaxzf")))

(define-public crate-libsdbootconf-0.3.0 (c (n "libsdbootconf") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qkgmbpxxn6fafzs93v10823vixxnrxw0m8bip5zyqpxrywwr770")))

(define-public crate-libsdbootconf-0.3.1 (c (n "libsdbootconf") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "103kkhcynakd455c8mil4vkk52sxig474njiyk15bw4bxdl8wlnf")))

(define-public crate-libsdbootconf-0.3.2 (c (n "libsdbootconf") (v "0.3.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0bs39vm2ix1pphig92hcx8wc937ymsl5lqwhs26978snf1ms5igr")))

(define-public crate-libsdbootconf-0.3.3 (c (n "libsdbootconf") (v "0.3.3") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1zzpfzqn7j755m6fxxjy8ayvcywfnszqah1f5cnd59540lxjxc2b")))

(define-public crate-libsdbootconf-0.3.4 (c (n "libsdbootconf") (v "0.3.4") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12q7a13zq10b0lq1191lqhf3m6dllwl0zlys0dry84mikq98329b")))

(define-public crate-libsdbootconf-0.4.0 (c (n "libsdbootconf") (v "0.4.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pvsb79cvwrj74xq4v3db36m9n20qpxw78x4c2ks5gan3f7hyqfn")))

(define-public crate-libsdbootconf-0.4.1 (c (n "libsdbootconf") (v "0.4.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pkm11bbwnm1nx62ivy8m3m2029d9kcr3zz3avy2rflqkqg7maqd")))

(define-public crate-libsdbootconf-0.5.0 (c (n "libsdbootconf") (v "0.5.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "03r0s3r93vjrf7cm3sgz103nv4dkxlla854qh874cadmacpnmsm6")))

(define-public crate-libsdbootconf-0.6.0 (c (n "libsdbootconf") (v "0.6.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0c23dr78gf7ckcgdypwshcn159348k474j711bzf4g41ha02rn8n")))

(define-public crate-libsdbootconf-0.6.1 (c (n "libsdbootconf") (v "0.6.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0vgwmq19pdrq0hcm8k6hmqkb6916ny6qpjj9y1kk57a5m3z3sm1q")))

(define-public crate-libsdbootconf-0.7.0 (c (n "libsdbootconf") (v "0.7.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12j63xc3i9f5wy6qrwf339j9jnqfnyxfhrib7sa3x3pj0lqi9c72")))

(define-public crate-libsdbootconf-0.8.0 (c (n "libsdbootconf") (v "0.8.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xpkxp0lsxv3day0871r6d5ipnrbmb4z6dis12rh1fp86pck9jww")))

(define-public crate-libsdbootconf-0.8.1 (c (n "libsdbootconf") (v "0.8.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1q1ahyp8sa6vz8pnjl7xinih6i525m8kh3a7kwn21201xly1nh3q")))

(define-public crate-libsdbootconf-0.9.0 (c (n "libsdbootconf") (v "0.9.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0wsbhd0zpwcv2rky0382jrbijm2w5bkgaa4sw9ddg3p2f2rbbnhk")))

(define-public crate-libsdbootconf-0.9.1 (c (n "libsdbootconf") (v "0.9.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0b5yc1lgr61pnr7k38h1wh0z2dx5pp4lzi6zgrxfaskvfbxim9hw")))

(define-public crate-libsdbootconf-0.10.0 (c (n "libsdbootconf") (v "0.10.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1cyb7yx0wy2yfr93rp030y459vngfjpnicgbgyiddwsh66xmnc4z")))

(define-public crate-libsdbootconf-0.10.1 (c (n "libsdbootconf") (v "0.10.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1plsjx390k4vp6nk1m6q7aifyhy8a6xlvm7j0z3ibrldwm7hksc5")))

(define-public crate-libsdbootconf-0.10.2 (c (n "libsdbootconf") (v "0.10.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "15slnsq1qsfzb30ql41wgnqvdpxdk6fnijlyi98vjccafks391sx")))

(define-public crate-libsdbootconf-0.11.0 (c (n "libsdbootconf") (v "0.11.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1plzm4qhz70sn98zv225jsvfcrdc831yh0fzpw4cg5jyakpxisfr")))

(define-public crate-libsdbootconf-0.11.1 (c (n "libsdbootconf") (v "0.11.1") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1s74p453jn17b755aa61mccz5bah4imzjxwnpc5rn23k9rb5254r")))

(define-public crate-libsdbootconf-0.11.2 (c (n "libsdbootconf") (v "0.11.2") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0mbcrsj899l7cm1mx2f7xq9xjx0y6b7wyyh380ssq08i6as6qi13")))

