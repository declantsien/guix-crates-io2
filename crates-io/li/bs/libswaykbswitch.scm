(define-module (crates-io li bs libswaykbswitch) #:use-module (crates-io))

(define-public crate-libswaykbswitch-0.0.1 (c (n "libswaykbswitch") (v "0.0.1") (d (list (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)))) (h "0khkmih3n7s3sycmpcwyh3j4fc8pkwh4rra7r606hrfhm5x2v8vr")))

(define-public crate-libswaykbswitch-0.1.0 (c (n "libswaykbswitch") (v "0.1.0") (d (list (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)))) (h "1kgn74q4l2fd5zp9qlh08rhban29a4aw62z9pl6k89nq5dvjjrw7")))

(define-public crate-libswaykbswitch-0.2.0 (c (n "libswaykbswitch") (v "0.2.0") (d (list (d (n "swayipc") (r "^3.0.1") (d #t) (k 0)))) (h "1ghfbivrib5bdmrmy7cqw74mfl7xvykx0rclv8wbax0i1gdn0xlf")))

