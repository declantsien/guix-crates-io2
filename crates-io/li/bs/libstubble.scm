(define-module (crates-io li bs libstubble) #:use-module (crates-io))

(define-public crate-libstubble-0.3.0 (c (n "libstubble") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "handlebars") (r "^4.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (d #t) (k 0)))) (h "00cl855ks4zsh038fqnchrx54szpp9pf8hlw1l721n4jc0lbh94k")))

(define-public crate-libstubble-0.3.1 (c (n "libstubble") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "handlebars") (r "^4.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (d #t) (k 0)))) (h "19by8wm4dnzckzz0lh2yk67pjjpd1a7y56bgw3ijr49ci0cp1kcd")))

(define-public crate-libstubble-0.3.2 (c (n "libstubble") (v "0.3.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "handlebars") (r "^4.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "slug") (r "^0.1.4") (d #t) (k 0)) (d (n "uuid") (r "^1.5.0") (d #t) (k 0)))) (h "0ppyqfmp2zybnhwa5qy9zvgly32xln3s9aacvg6xnx6q9gxna74p")))

