(define-module (crates-io li bs libsbxf) #:use-module (crates-io))

(define-public crate-libsbxf-0.1.0 (c (n "libsbxf") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.1") (d #t) (k 0)) (d (n "chacha20poly1305") (r "^0.5.1") (d #t) (k 0)) (d (n "ed25519-dalek") (r "^1.0.0-pre.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.114") (f (quote ("derive"))) (d #t) (k 0)))) (h "1yd335p2wcxfnj9n1i85iigxhjhq0j2vhgmpjxmk7x7g2qgvji4f")))

