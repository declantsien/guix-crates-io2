(define-module (crates-io li bs libsydtime) #:use-module (crates-io))

(define-public crate-libsydtime-0.0.1 (c (n "libsydtime") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "1y6l0vp393jjqdz2xkfhgmncvjzgvdhw1j918x7dn5fi6gd3gl4p") (r "1.56.1")))

(define-public crate-libsydtime-0.0.2 (c (n "libsydtime") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "198q64w30sd83i1gy2g7yvqw0llq4m00fl900qn2apbhx6jr9al9") (r "1.56.1")))

(define-public crate-libsydtime-0.0.3 (c (n "libsydtime") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0vc2z162i9s212lq4nc8yxv81hk5f9cxmgzdl2avk79kwbc5vzib") (r "1.56.1")))

