(define-module (crates-io li bs libsql-wasi) #:use-module (crates-io))

(define-public crate-libsql-wasi-0.1.0 (c (n "libsql-wasi") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (d #t) (k 0)) (d (n "wasmtime") (r "^15.0.0") (d #t) (k 0)) (d (n "wasmtime-wasi") (r "^15.0.0") (d #t) (k 0)))) (h "1cdgjazf6aq5rw1c89h4k6zgh000c51dpy3j9rxglhg3gk8f0m4b")))

