(define-module (crates-io li bs libsyslog-sys) #:use-module (crates-io))

(define-public crate-libsyslog-sys-0.0.0 (c (n "libsyslog-sys") (v "0.0.0") (h "0r0ka1dmwy4z12n2sqxv1sv8nd7rmnmxirzzmwh8vx3n6qdyvv5r")))

(define-public crate-libsyslog-sys-0.1.0 (c (n "libsyslog-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0x0xsr8j3q0qpy4j2w9s7zwicl4yac3dmzyj5dq415q9rrnwq6j0")))

