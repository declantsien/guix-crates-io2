(define-module (crates-io li bs libsolv-sys) #:use-module (crates-io))

(define-public crate-libsolv-sys-0.1.0 (c (n "libsolv-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0npzsbjdfy9jyx5w3ng9x1ivb4k8qbghwmvhmqqw8ahqrhfyd7mg")))

(define-public crate-libsolv-sys-0.1.1 (c (n "libsolv-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1yqp9db411xfikn18xk66s1s5bd5vkzq65a82gb6kn87qw47rnsq")))

(define-public crate-libsolv-sys-0.1.2 (c (n "libsolv-sys") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "10siim7i6m3sqxrkg69b3rqm6yrb9jb0m26dqvdbxhg81xk24dr7")))

(define-public crate-libsolv-sys-0.1.3 (c (n "libsolv-sys") (v "0.1.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0z2s9p9d7wngk0mlznakk76ipx0w0zqsny95l96g7jn2wi173z55")))

(define-public crate-libsolv-sys-0.1.4 (c (n "libsolv-sys") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 1)) (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "01vc82pbm5my6a2sfpl6v3wch0bp44p8453q0kgc1ddhcnl3xj9y")))

