(define-module (crates-io li bs libsex) #:use-module (crates-io))

(define-public crate-libsex-0.1.0 (c (n "libsex") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0xbf0b2jqzfc3xiyc9snk98dbcnmpy1cv0265hx01rm8321radvp")))

(define-public crate-libsex-0.1.1 (c (n "libsex") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1ffgvnq61jqp7ks8wj6f0p6f9qm5bci5nmwhr407ivfv9z8fcr9g")))

(define-public crate-libsex-0.1.2 (c (n "libsex") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "06y9x21n8h5r3310lh36lzhq7ygavya0fvl3wamhyaczf42zlvbl")))

