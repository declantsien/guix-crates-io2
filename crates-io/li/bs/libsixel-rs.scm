(define-module (crates-io li bs libsixel-rs) #:use-module (crates-io))

(define-public crate-libsixel-rs-0.1.0 (c (n "libsixel-rs") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "image") (r "^0.24") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qyi2rg71pay39bvz74pzgsblpdm5jpfshm6ippmw9fn65hqqmrg") (f (quote (("std" "image") ("e2e-tests"))))))

