(define-module (crates-io li bs libswifft) #:use-module (crates-io))

(define-public crate-libswifft-0.1.0 (c (n "libswifft") (v "0.1.0") (d (list (d (n "libswifft_sys") (r "^0.1.0") (d #t) (k 0)))) (h "0bjssd7wbic4vwam49q4pjj1nry7nl7chr94bqha4hmv4flny7fk")))

(define-public crate-libswifft-0.2.0 (c (n "libswifft") (v "0.2.0") (d (list (d (n "libswifft_sys") (r "^0.2.0") (d #t) (k 0)))) (h "0f9siil4ns1hv02ldkbkwahzhvrc0n2m0jldj3wnxv4qkqfscm7k")))

