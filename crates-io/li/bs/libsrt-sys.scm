(define-module (crates-io li bs libsrt-sys) #:use-module (crates-io))

(define-public crate-libsrt-sys-1.4.1 (c (n "libsrt-sys") (v "1.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yw11k3vfayglrl66sxf0yvfyz3mh03lkc6sbw6v67n1mvbcfg5y") (l "srt")))

(define-public crate-libsrt-sys-1.4.2 (c (n "libsrt-sys") (v "1.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0v92vgfz0dw8szpr4ply313xlxshq83mkc2hylmxrq3gaqnaavh6") (l "srt")))

(define-public crate-libsrt-sys-1.4.3 (c (n "libsrt-sys") (v "1.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1l3a1fz9afxcgjmykd5ycp0vj3rdnj4vjqah4fl32nji7yf7680l") (l "srt")))

(define-public crate-libsrt-sys-1.4.4 (c (n "libsrt-sys") (v "1.4.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0grajaxswcfd5a1skakbn4glr9s47ajs4rkd7plcac48hx9v67sc") (l "srt")))

(define-public crate-libsrt-sys-1.4.5 (c (n "libsrt-sys") (v "1.4.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mcdwymd9c88c5chzjdflwf2vrsrwxgc8yj72xzhlqafsa9q0fcg") (l "srt")))

(define-public crate-libsrt-sys-1.4.6 (c (n "libsrt-sys") (v "1.4.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0s1vay76qrma420l0wr527ds7waybmgj7i6wk33wqbqm2yw7ja3n") (l "srt")))

(define-public crate-libsrt-sys-1.4.7 (c (n "libsrt-sys") (v "1.4.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jdpz4nzvqxddhkwj2yiilz1cgmy5ajam1780bdqbz8xq3lwk95c") (l "srt")))

(define-public crate-libsrt-sys-1.4.12 (c (n "libsrt-sys") (v "1.4.12") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0z9xi9zanspbfh7v0q4nahqilhs6pzs2rijdhjj7f9w5q3jn3hyy") (l "srt")))

(define-public crate-libsrt-sys-1.4.13 (c (n "libsrt-sys") (v "1.4.13") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0sgv2f07981id9407l6yb2ii5j8n9lxrxk08gjia8plpdq67pixm") (l "srt")))

