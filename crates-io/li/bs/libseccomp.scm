(define-module (crates-io li bs libseccomp) #:use-module (crates-io))

(define-public crate-libseccomp-0.1.0 (c (n "libseccomp") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libseccomp-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "14skr24v9w8zkq3792baab1x4cbgkkzwcj2sakmxz45i2rmv17xw")))

(define-public crate-libseccomp-0.1.1 (c (n "libseccomp") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libseccomp-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0fjys2vqw80pcv8nsydvwgbq28vmfac8yfwyp5wkb45bhjnmb798")))

(define-public crate-libseccomp-0.1.2 (c (n "libseccomp") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libseccomp-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "0n240f4hla6m3whs2k5rm7i5in67fjmavr7ihbkpd9bhl0xl4js4")))

(define-public crate-libseccomp-0.1.3 (c (n "libseccomp") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libseccomp-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "11nvrylsflkdcsmnvhxjpkw6782dkfr7hcbazsng7vkcnsjp3b9n")))

(define-public crate-libseccomp-0.2.0 (c (n "libseccomp") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libseccomp-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1268rygr9l9c15nxka9igcb0bd8aqss43jcjxnrngx294rxi3rmg")))

(define-public crate-libseccomp-0.2.1 (c (n "libseccomp") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libseccomp-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "15jjfgykxkqs29406wkzh1js44zf118f78fg2br18700k7wdj051")))

(define-public crate-libseccomp-0.2.2 (c (n "libseccomp") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libseccomp-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "07sxxfcxz3746mhjnsdlh5zgvg3gqrfn15kdwybj18k0pwd4c6jj")))

(define-public crate-libseccomp-0.2.3 (c (n "libseccomp") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.108") (d #t) (k 0)) (d (n "libseccomp-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "104cnryqm24cp86vx14y54vbr8vjc7mz2zgz8a4sqhjwybxs3ga9")))

(define-public crate-libseccomp-0.3.0 (c (n "libseccomp") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "cfg-if") (r "^1.0.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.108") (d #t) (k 0)) (d (n "libseccomp-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1cz5780csvkfr57p0gf6fxqshacdc4c83dmp0yc0300sk3c7zi91") (f (quote (("const-syscall" "cfg-if"))))))

