(define-module (crates-io li bs libshaiya) #:use-module (crates-io))

(define-public crate-libshaiya-0.0.1 (c (n "libshaiya") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0hzcwmjappdyah2zd68pga0fxhhafmzasx352aas0bhvnd6wnfap")))

(define-public crate-libshaiya-0.0.2 (c (n "libshaiya") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1kckx6nk2zgm0k685wxzkxl61vfvxrjzz7spwglfl9h7zkxj0yym")))

(define-public crate-libshaiya-0.0.3 (c (n "libshaiya") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "10vfaqqsiifxm6rj6mv3hw495v44s57v1asyv95jv6wcimzq1j3h")))

(define-public crate-libshaiya-0.0.4 (c (n "libshaiya") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1l788kap4dmnbfxvarx9kgqlx9z5m9as3mrba0ad1g5y9zhm4vkh")))

