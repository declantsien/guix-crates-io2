(define-module (crates-io li bs libsam-rs) #:use-module (crates-io))

(define-public crate-libsam-rs-0.1.0 (c (n "libsam-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "0ci568c6b19izky2hq70rn891b8bdgccndjylndjbl9dd35na11x")))

(define-public crate-libsam-rs-0.2.0 (c (n "libsam-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "13qmp99mazjq7v9zplsl5kzzzb5r4n9gadz6rk9dakbjp6aqb73r")))

