(define-module (crates-io li bs libscroll-re) #:use-module (crates-io))

(define-public crate-libscroll-re-0.1.0 (c (n "libscroll-re") (v "0.1.0") (d (list (d (n "libscroll") (r "^0.1.4") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "ocaml") (r "^0.8.1") (d #t) (k 0)))) (h "1wfycbjqclkzwb2r791ssvlrijmkw0jfhxi99hv0y25yr64x265m")))

