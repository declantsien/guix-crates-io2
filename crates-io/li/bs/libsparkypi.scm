(define-module (crates-io li bs libsparkypi) #:use-module (crates-io))

(define-public crate-libsparkypi-0.1.0 (c (n "libsparkypi") (v "0.1.0") (d (list (d (n "rppal") (r "^0.14.1") (d #t) (k 0)))) (h "128v4g1nvygpr9b1fnv9sw9y26r0z21348k338dsafjnic202n4w")))

(define-public crate-libsparkypi-0.2.0 (c (n "libsparkypi") (v "0.2.0") (d (list (d (n "rppal") (r "^0.14.1") (d #t) (k 0)))) (h "18r5rinwlx49ba0vhfv9yw6pf08bs1g0w42jg0sdixjdpmwrfszg")))

(define-public crate-libsparkypi-0.2.1 (c (n "libsparkypi") (v "0.2.1") (d (list (d (n "rppal") (r "^0.17.1") (d #t) (k 0)))) (h "1pzjk7aymnxim2pq6g9wwmn84h57dr3rmj7sqdw5mkgiwmhah1rs") (y #t)))

(define-public crate-libsparkypi-0.2.2 (c (n "libsparkypi") (v "0.2.2") (d (list (d (n "rppal") (r "^0.17.1") (d #t) (k 0)))) (h "0alp96h8d8ypqnlc2k52arg3rr22c14ynl1jgnjqnikcvnllwxxl")))

(define-public crate-libsparkypi-0.3.0 (c (n "libsparkypi") (v "0.3.0") (d (list (d (n "gpio-cdev") (r "^0.6.0") (d #t) (k 0)))) (h "0a6b327ycqbjfr548rmfg31i0a07in7gz4xnh1p3k12fy70m82q5")))

