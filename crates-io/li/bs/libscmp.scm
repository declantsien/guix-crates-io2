(define-module (crates-io li bs libscmp) #:use-module (crates-io))

(define-public crate-libscmp-0.1.0 (c (n "libscmp") (v "0.1.0") (d (list (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "0zp3j9akzpnn1rvv76rrvsjqydsx0p731wik5kzm6nzz7ljal6lm") (f (quote (("libseccomp-2-5" "libseccomp-2-4") ("libseccomp-2-4") ("default"))))))

(define-public crate-libscmp-0.2.0 (c (n "libscmp") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03gmcfniqvia1hiwc4ln0d59gw1d7b4rkri8ibxzz76bwdxj270a") (f (quote (("libseccomp-2-5" "libseccomp-2-4") ("libseccomp-2-4") ("default")))) (l "seccomp")))

