(define-module (crates-io li bs libsodium-sys) #:use-module (crates-io))

(define-public crate-libsodium-sys-0.0.1 (c (n "libsodium-sys") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "179hay4n0v8abi8hdacq7ckkhi9hhywi165a7n7dbzx7wvm6dcnw")))

(define-public crate-libsodium-sys-0.0.2 (c (n "libsodium-sys") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1y6ri25yj8df1fvg9wfryx9g59bspq9l0fvcwksrl9yzph4vym6q")))

(define-public crate-libsodium-sys-0.0.3 (c (n "libsodium-sys") (v "0.0.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "052q7nf0cg1hwpy7lnlrymr071a4h1441ms81dhmp4p8i5z764h5")))

(define-public crate-libsodium-sys-0.0.4 (c (n "libsodium-sys") (v "0.0.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1fmz47yi61drzs4qyx5ib7r4jmxlbqiywz5zvpkq8mjvarfmi35d")))

(define-public crate-libsodium-sys-0.0.5 (c (n "libsodium-sys") (v "0.0.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1qaaag0glpcx7r64yda8ydj44i1jbvkpqi4jlqw8wasbk634z8rw")))

(define-public crate-libsodium-sys-0.0.6 (c (n "libsodium-sys") (v "0.0.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "10w4rc7r3l867v1dnhkdqkcckncm7l4wvkb2gl9w4ai5kp0ryksg")))

(define-public crate-libsodium-sys-0.0.7 (c (n "libsodium-sys") (v "0.0.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "19783xbjs88m759b0dphmjjj27rai18f1n486rgwgzcq7r2dks5w")))

(define-public crate-libsodium-sys-0.0.8 (c (n "libsodium-sys") (v "0.0.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "15k0rvpiiwpy6vks3kqadr1gfsbhwdffn1xc957vc01v5giywl90")))

(define-public crate-libsodium-sys-0.0.9 (c (n "libsodium-sys") (v "0.0.9") (d (list (d (n "libc") (r "^0.2.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.6") (d #t) (k 1)))) (h "0k220jvjk759xpdbsbjjf4pa7bizrw3cdnnqh68wwssp8f8mb6pn")))

(define-public crate-libsodium-sys-0.0.10 (c (n "libsodium-sys") (v "0.0.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dln2fnfb19lyb871f0z812h59mgk3xj885wssa4vph7ap2l6l72")))

(define-public crate-libsodium-sys-0.0.11 (c (n "libsodium-sys") (v "0.0.11") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1bdx4c7xqvhndn6bhj9233n1yy42py2fwswfhb1sz7n2436vlvzx")))

(define-public crate-libsodium-sys-0.0.12 (c (n "libsodium-sys") (v "0.0.12") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ipvzg9nwniwbwpkzhn4fhlapsz5f0n50km7dv9cq4866dn9isa4")))

(define-public crate-libsodium-sys-0.0.13 (c (n "libsodium-sys") (v "0.0.13") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1n3kqzhw21w14wn4xnnsx0v53frxbhxfd6m8ymvxmhlyy4p574lf")))

(define-public crate-libsodium-sys-0.0.14 (c (n "libsodium-sys") (v "0.0.14") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1l8s7ikpkpn6ib1rm46jrbc5zm7s8x4fvq4dd7wan5bq0536xg6b")))

(define-public crate-libsodium-sys-0.0.15 (c (n "libsodium-sys") (v "0.0.15") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1y0dahsh1xl1c02a5rzs44i1k3r1wf9rcs71fa10cwhv1yyxdrj5")))

(define-public crate-libsodium-sys-0.0.16 (c (n "libsodium-sys") (v "0.0.16") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hjmdxjz32yq4gxwjg608bi7cf9igilsmsv9lslcli4dxpp1pggw")))

(define-public crate-libsodium-sys-0.1.0 (c (n "libsodium-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.37.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.41") (k 0)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)))) (h "0hs6sr4cvx04sl49ailhg15i13rzkjzvp7fn91b90d8m2mm9v0q1") (l "sodium")))

(define-public crate-libsodium-sys-0.2.0 (c (n "libsodium-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.9") (d #t) (k 1)) (d (n "flate2") (r "^1.0.1") (d #t) (k 1)) (d (n "http_req") (r "^0.2.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.41") (k 0)) (d (n "libc") (r "^0.2.41") (k 1)) (d (n "pkg-config") (r "^0.3.11") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 1)) (d (n "tar") (r "^0.4.15") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "zip") (r "^0.5") (d #t) (k 1)))) (h "1zdxjw9c06kzfm7dan5zlf43x25ydn0lq4lq60sm6qa7gkrvgz9k") (l "sodium")))

(define-public crate-libsodium-sys-0.2.1 (c (n "libsodium-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "http_req") (r "^0.4") (f (quote ("rust-tls"))) (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libflate") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "sha2") (r "^0.8") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "zip") (r "^0.5") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1xa9hc7vqxx7syss78j7ya4nhwpvxmb3m97p625ignq303k7kzq9") (f (quote (("use-pkg-config")))) (l "sodium")))

(define-public crate-libsodium-sys-0.2.2 (c (n "libsodium-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libflate") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "07hpd32hqm3lfswdajpr29dg1kc61cx7j9qgml92dbnxg5d5jafy") (f (quote (("use-pkg-config")))) (l "sodium")))

(define-public crate-libsodium-sys-0.2.3 (c (n "libsodium-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "18yfg1gj9fx0qdjr1ymhkcld2a4zjhg35hb7410n3vsvpl885hxr") (f (quote (("use-pkg-config")))) (y #t) (l "sodium")))

(define-public crate-libsodium-sys-0.2.4 (c (n "libsodium-sys") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0zl66yciq875pq4qwjh5r2lnvdgi2jmkml5jcppiglb6d7ayvv5d") (f (quote (("use-pkg-config")))) (l "sodium")))

(define-public crate-libsodium-sys-0.2.5 (c (n "libsodium-sys") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "libflate") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "tar") (r "^0.4") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0wsy3ycxngxjxb4zwh8q6gdl3l0zihscvypzy2hqzvwh5gqlyd0w") (f (quote (("use-pkg-config")))) (l "sodium")))

(define-public crate-libsodium-sys-0.2.6 (c (n "libsodium-sys") (v "0.2.6") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0n4a5f4vi63lvf4v6xcds20kdiqs8dxpybjz25s90cvvhd7vd1d6") (f (quote (("use-pkg-config")))) (l "sodium")))

(define-public crate-libsodium-sys-0.2.7 (c (n "libsodium-sys") (v "0.2.7") (d (list (d (n "cc") (r "^1.0") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "libc") (r "^0.2") (k 0)) (d (n "libc") (r "^0.2") (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "walkdir") (r "^2") (d #t) (k 1)))) (h "1zcjka23grayr8kjrgbada6vwagp0kkni9m45v0gpbanrn3r6xvb") (f (quote (("use-pkg-config")))) (l "sodium")))

