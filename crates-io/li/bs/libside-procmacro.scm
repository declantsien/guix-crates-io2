(define-module (crates-io li bs libside-procmacro) #:use-module (crates-io))

(define-public crate-libside-procmacro-0.1.0 (c (n "libside-procmacro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.30") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits" "printing"))) (d #t) (k 0)))) (h "0qmsmyhmgx0bay8pxwf6pn089g9gnv87n7gamrwb5ic5rakbk7kz")))

