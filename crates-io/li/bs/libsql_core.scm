(define-module (crates-io li bs libsql_core) #:use-module (crates-io))

(define-public crate-libsql_core-0.0.1 (c (n "libsql_core") (v "0.0.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports" "async" "async_futures"))) (d #t) (k 2)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "libsql-sys") (r "^0") (d #t) (k 0)) (d (n "libsql_replication") (r "^0") (d #t) (k 0)) (d (n "pprof") (r "^0.11.1") (f (quote ("criterion" "flamegraph"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("macros"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.17") (d #t) (k 0)))) (h "19hdwv28dwx0c04pqnk9y10mmp4cdscxpwby16fmli9nkjdidh6r") (y #t)))

