(define-module (crates-io li bs libslirp-sys) #:use-module (crates-io))

(define-public crate-libslirp-sys-4.0.0 (c (n "libslirp-sys") (v "4.0.0") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "047pbsfq4zssdn748j0hiqdgiqn8wd44ly74s8xa0g7hh4bfrwgj") (l "slirp")))

(define-public crate-libslirp-sys-4.0.1 (c (n "libslirp-sys") (v "4.0.1") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1094gfjzqz4l61yy9dlf2y9xpsgi5rsr1yfyd9qa1l4kna378m8z") (l "slirp")))

(define-public crate-libslirp-sys-4.0.2 (c (n "libslirp-sys") (v "4.0.2") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0b25gwd9bv6hgdirnwd0vc48bily7frn37aymskd3i06b97n0aqy") (l "slirp")))

(define-public crate-libslirp-sys-4.2.0 (c (n "libslirp-sys") (v "4.2.0") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1d07v58c3f61yf4rhpflnabibxc0ryrf85gf4lxyx6x735blmr96") (l "slirp")))

(define-public crate-libslirp-sys-4.2.1 (c (n "libslirp-sys") (v "4.2.1") (d (list (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1x658g9bagjvj2krir6fa96scfg10lq07gg0xb3hbympx463fwi7") (l "slirp")))

