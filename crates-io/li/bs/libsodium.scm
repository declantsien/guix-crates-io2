(define-module (crates-io li bs libsodium) #:use-module (crates-io))

(define-public crate-libsodium-0.1.1 (c (n "libsodium") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ylkif2xwkhmlfj4rg94mcm8mjgdj8z297j6papf0li4fcgxi6gr") (y #t)))

