(define-module (crates-io li bs libsixel-sys) #:use-module (crates-io))

(define-public crate-libsixel-sys-0.1.0 (c (n "libsixel-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "18q8cbc43pafa3ij77fm9b260an8dy9dcp07g896c293w0ylzzsk")))

