(define-module (crates-io li bs libsoxr-sys) #:use-module (crates-io))

(define-public crate-libsoxr-sys-0.1.0 (c (n "libsoxr-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17x1qckcckbakk0rddp5q1h5g4kv0zxq70g8dnh4h6yjvp6mil5x")))

(define-public crate-libsoxr-sys-0.1.1 (c (n "libsoxr-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1a5l42z6dqdnzrmwv0mp7ppr4m68pngaa2nr6k2557aly6d40isr")))

(define-public crate-libsoxr-sys-0.1.2 (c (n "libsoxr-sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dca8jxj4hjyvbimq5r7c22ijbpmfgllp3405wb68dxiiz2ywr2l")))

(define-public crate-libsoxr-sys-0.1.3 (c (n "libsoxr-sys") (v "0.1.3") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ia42iqfxr88dfir8w4llq6jbdfi3ps478hhscryaxrkdam6nspy") (l "soxr")))

(define-public crate-libsoxr-sys-0.1.4 (c (n "libsoxr-sys") (v "0.1.4") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1kpvm3g0xiiy4ylky0jppwkg5zl5pyk6y8z3jlp9sxb47f5xrzvw") (l "soxr")))

