(define-module (crates-io li bs libstrophe-sys) #:use-module (crates-io))

(define-public crate-libstrophe-sys-0.0.1 (c (n "libstrophe-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "04mk7l6844i97k21rsfy89vh490947hlj18ax7k111xcdcwcfcam")))

