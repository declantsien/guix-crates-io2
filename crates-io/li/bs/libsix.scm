(define-module (crates-io li bs libsix) #:use-module (crates-io))

(define-public crate-libsix-0.0.1 (c (n "libsix") (v "0.0.1") (h "0c10s2k6zgifkkbl62di74ishyf435b0azgihbwx9y9c7fkpaa3v")))

(define-public crate-libsix-0.0.2 (c (n "libsix") (v "0.0.2") (d (list (d (n "unmem") (r "^0.2.5") (d #t) (k 0)))) (h "0rd16krr61pfi7rp72hsmz6ylqkk2mzc5br5f85iq29lm40sh210")))

