(define-module (crates-io li nt lint-init) #:use-module (crates-io))

(define-public crate-lint-init-0.1.0 (c (n "lint-init") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.10") (d #t) (k 0)) (d (n "relative-path") (r "^1.6.1") (d #t) (k 0)))) (h "1pvd19hvy39mkb04ffpsl7dqmcb2zhyi85nrixqny1gdzafmd9nx")))

(define-public crate-lint-init-0.2.0 (c (n "lint-init") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "clap") (r "^3.0.14") (f (quote ("derive"))) (d #t) (k 0)) (d (n "execute") (r "^0.2.10") (d #t) (k 0)) (d (n "relative-path") (r "^1.6.1") (d #t) (k 0)))) (h "0rw3a8krzip6bbxyd9wd5ksh5spwqbshdg61l4migip2fz054y0q")))

