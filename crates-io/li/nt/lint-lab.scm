(define-module (crates-io li nt lint-lab) #:use-module (crates-io))

(define-public crate-lint-lab-0.0.1 (c (n "lint-lab") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "cargo-lock") (r "^9.0.0") (d #t) (k 0)) (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "prometheus-client") (r "^0.22.2") (d #t) (k 0)) (d (n "semver") (r "^1.0.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "strum") (r "^0.26.2") (f (quote ("derive"))) (d #t) (k 0)))) (h "16f1i3z9g2z35jfgwykpj49daywq8ghjdz3lfivx0g3hhccyybhv")))

