(define-module (crates-io li nt lintd-taskops) #:use-module (crates-io))

(define-public crate-lintd-taskops-0.1.0 (c (n "lintd-taskops") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "xtaskops") (r "^0.4.2") (d #t) (k 0)))) (h "154zi0axs12v24ckdshwrg44mj2g1ry6l2jyln94i6zsvfkhm59n")))

(define-public crate-lintd-taskops-0.1.1 (c (n "lintd-taskops") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.189") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)) (d (n "xtaskops") (r "^0.4.2") (d #t) (k 0)))) (h "0bw459mwgq6gs8jc8vs9myn5v2144qbjnz1bvlh0s0ndmpb6f39v")))

(define-public crate-lintd-taskops-0.1.2 (c (n "lintd-taskops") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.189") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)) (d (n "xtaskops") (r "^0.4.2") (d #t) (k 0)))) (h "0hjjgrmqk637cyn39i9mdlzpfh7jhcqiwjk1rw1a3mdvhjk1xqhm")))

(define-public crate-lintd-taskops-0.1.3 (c (n "lintd-taskops") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "duct") (r "^0.13.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.189") (d #t) (k 0)) (d (n "toml") (r "^0.8.2") (d #t) (k 0)) (d (n "xtaskops") (r "^0.4.2") (d #t) (k 0)))) (h "0pk2w186cmrh2rliyc21hd4mhh72r9q79xfs6fhp5gfmls3rcc16")))

