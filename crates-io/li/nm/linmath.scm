(define-module (crates-io li nm linmath) #:use-module (crates-io))

(define-public crate-linmath-0.0.1 (c (n "linmath") (v "0.0.1") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0lr33fqfvjarypvjzrrfsfzwknws5qr8l8rfljwly6blwlhdspq2")))

(define-public crate-linmath-0.0.2 (c (n "linmath") (v "0.0.2") (d (list (d (n "approx") (r "^0.3.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "003nirbzaiyjq8syqmyic78min0snb3vyv4dsriksqvmk5sw9cpq")))

