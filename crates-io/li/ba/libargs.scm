(define-module (crates-io li ba libargs) #:use-module (crates-io))

(define-public crate-libargs-0.1.0 (c (n "libargs") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)))) (h "09bkdmgq98q9agzjip08s6hi4kwvbhm4s9w7r3i617z8mjadmivm") (f (quote (("force_walk"))))))

(define-public crate-libargs-0.1.1 (c (n "libargs") (v "0.1.1") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)))) (h "1nr8yn8wwishxigl6vfn0v73bnf7ldsxnvwbz9vivl88qd6xj7nf") (f (quote (("force_walk")))) (y #t)))

(define-public crate-libargs-0.1.2 (c (n "libargs") (v "0.1.2") (d (list (d (n "once_cell") (r "^1.2.0") (d #t) (k 0)))) (h "1a7skkqiigx73lgijjnn7wnmb1bv127smfwfkpqa2nzx4mr5gzzj") (f (quote (("force_walk"))))))

