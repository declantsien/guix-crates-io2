(define-module (crates-io li ba libargon2-sys) #:use-module (crates-io))

(define-public crate-libargon2-sys-0.0.0 (c (n "libargon2-sys") (v "0.0.0") (h "0709xkbiicd71g9arj3zwn1r9gf1hs1qsfj5phyl7x5kn9may0my")))

(define-public crate-libargon2-sys-0.1.0 (c (n "libargon2-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g96r377qik8v8y4f91lzg8yghdclagxc4cnsyyq25b4v93cwwp1") (l "argon2")))

(define-public crate-libargon2-sys-0.2.0 (c (n "libargon2-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1c060x6wma1skmal679w50z49cahrh2y09nyy3454sdkh0xkmmw5") (l "argon2")))

