(define-module (crates-io li ba libarc) #:use-module (crates-io))

(define-public crate-libarc-0.1.0 (c (n "libarc") (v "0.1.0") (d (list (d (n "hyper") (r "^0.13.0-alpha.4") (d #t) (k 0)) (d (n "stdweb") (r "^0.4.20") (d #t) (k 0)) (d (n "tokio-async") (r "^0.0.0") (d #t) (k 0)))) (h "1zg13kv4xj6rryg7vjm65ip1xczl4p57x4a5b800hav8jdxiqlkr")))

