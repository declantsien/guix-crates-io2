(define-module (crates-io li ba libatasmart-sys) #:use-module (crates-io))

(define-public crate-libatasmart-sys-0.1.0 (c (n "libatasmart-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0s35myiyf4cd806hc0qwcv1v3fvi2bdn29nn36lw0fw1qrq3s9mg")))

(define-public crate-libatasmart-sys-0.1.1 (c (n "libatasmart-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "0wh9g7yc6i78w7fyaz6q4wlhd48wg6k2x80288vaplbidj3qarbw")))

(define-public crate-libatasmart-sys-0.1.2 (c (n "libatasmart-sys") (v "0.1.2") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "0hg82wzdnm7f24zrv3ycwy298zyc2jpqays67q0sqcrhjxjnpzyy")))

(define-public crate-libatasmart-sys-0.2.0 (c (n "libatasmart-sys") (v "0.2.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)))) (h "19392k3yay1ik0d1ssj37333v6r3j5plni6hvwvxampfsr7388x2")))

