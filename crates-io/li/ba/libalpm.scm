(define-module (crates-io li ba libalpm) #:use-module (crates-io))

(define-public crate-libalpm-0.1.0 (c (n "libalpm") (v "0.1.0") (d (list (d (n "alpm-sys") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "printf") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "1gy8b92w4s3vipbrv7idf3zi494sa9ssrahas2g9cijd96qwmp6j")))

(define-public crate-libalpm-0.1.1 (c (n "libalpm") (v "0.1.1") (d (list (d (n "alpm-sys") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "printf") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "10yiimgf165xmidbq6cr0vhldjf0cyqzi3khb52k9nd52sba197y")))

(define-public crate-libalpm-0.1.2 (c (n "libalpm") (v "0.1.2") (d (list (d (n "alpm-sys") (r "^0.1") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "printf") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "0z28z47drpwvvl07df77hi44hr3s2kq5ir6w5vvdf62nz65g0xqp")))

