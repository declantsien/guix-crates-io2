(define-module (crates-io li ba libabort) #:use-module (crates-io))

(define-public crate-libabort-0.1.0 (c (n "libabort") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "0whbrwqmdzg0apr5ngh236kw5mzydvqw0wdfqd4aplk0gjmw89kw") (f (quote (("std"))))))

(define-public crate-libabort-0.1.2 (c (n "libabort") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "1n78a3svx0ly5g38r011zpz4pwm7qz9ykbyyy69li4xwd17h871x") (f (quote (("std"))))))

(define-public crate-libabort-0.1.3 (c (n "libabort") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (o #t) (k 0)))) (h "0i3n8c34csr83y9kfahyy1gg7m78q4lxjxymrzl4bq9zm9k31q9p") (f (quote (("std"))))))

