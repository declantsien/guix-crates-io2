(define-module (crates-io li ba libarchive3-sys) #:use-module (crates-io))

(define-public crate-libarchive3-sys-0.1.0 (c (n "libarchive3-sys") (v "0.1.0") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1fkglqcqvabazczx4c5dbbs1fvcycvjc1az8ffzsmsrn97pa55c2")))

(define-public crate-libarchive3-sys-0.1.1 (c (n "libarchive3-sys") (v "0.1.1") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1zxifc8vzh7ghknv8ngbyb2aka4i8h697hif2x4q52di5hxrg2aj")))

(define-public crate-libarchive3-sys-0.1.2 (c (n "libarchive3-sys") (v "0.1.2") (d (list (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19qw0gk6vyjdd26yhih42my5fdr076snjcjj0slcg92riypbxlrw")))

