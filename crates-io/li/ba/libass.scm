(define-module (crates-io li ba libass) #:use-module (crates-io))

(define-public crate-libass-0.1.0 (c (n "libass") (v "0.1.0") (d (list (d (n "libass-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.54") (d #t) (k 0)))) (h "01nfmkikacmlmz3nxx6s2hxv3vcj3qm5jnvm9aprvs2q54p7p69b")))

(define-public crate-libass-0.1.1 (c (n "libass") (v "0.1.1") (d (list (d (n "libass-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.54") (d #t) (k 0)))) (h "0p49b9xbh8cwg607f431k3s2kmv7q9ik0y0ai5bd0qd71rv8mf5p") (y #t)))

(define-public crate-libass-0.1.2 (c (n "libass") (v "0.1.2") (d (list (d (n "libass-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.54") (d #t) (k 0)))) (h "0xn0xivgfy32bl4zs53kawj7sq2vvls6ab6qf4hldzbzy29h4hdg")))

(define-public crate-libass-0.1.3 (c (n "libass") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "libass-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.54") (d #t) (k 0)))) (h "0vvirdiwr9njgcx1127g7r69dyns6yy1jwwm49fcd26033mys1gn")))

(define-public crate-libass-0.2.0 (c (n "libass") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.1.0") (d #t) (k 0)) (d (n "libass-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.54") (d #t) (k 0)) (d (n "png") (r "^0.16.7") (d #t) (k 2)))) (h "1v9z3d2m9f9vy157gd3inhkq14mspf1kpygscdb6ndczxf93fll8")))

