(define-module (crates-io li ba libalgobsec-sys) #:use-module (crates-io))

(define-public crate-libalgobsec-sys-0.0.1+1.4.8.0 (c (n "libalgobsec-sys") (v "0.0.1+1.4.8.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "0kf08ynar7spm5nk7s54by6a404nym5ycax4h3nj9hs4b9lx5a8d")))

(define-public crate-libalgobsec-sys-0.0.2+1.4.8.0 (c (n "libalgobsec-sys") (v "0.0.2+1.4.8.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "0zpr2zag64j295ivjs0x15qg1bvckzvl078gdc5bs3jcdhxs5bxw")))

(define-public crate-libalgobsec-sys-0.1.0+1.4.8.0 (c (n "libalgobsec-sys") (v "0.1.0+1.4.8.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "1s6s64bk30x4mmpwzl6k6c0wdzn7q6hd49ywivn9l4q1n50l23h8")))

(define-public crate-libalgobsec-sys-0.2.0+1.4.8.0 (c (n "libalgobsec-sys") (v "0.2.0+1.4.8.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0acw97bl9nla2p5jmvlrw1yc6xpaxzdgvz9h1q024wm8rfhpi9jw")))

(define-public crate-libalgobsec-sys-0.3.0+1.4.8.0 (c (n "libalgobsec-sys") (v "0.3.0+1.4.8.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "03v1b74z787y1kh06kdpmbdixhy3zil8b4q68dzm3qih3krf7wy5") (r "1.54")))

