(define-module (crates-io li ba libaom-sys) #:use-module (crates-io))

(define-public crate-libaom-sys-0.5.0 (c (n "libaom-sys") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0n3g7vi27lz5dzv5dcyw2yb76ka9hk3k23llvy1rl8ihiifsphbd") (f (quote (("__internal_aom_generic_target")))) (l "aom")))

(define-public crate-libaom-sys-0.5.1 (c (n "libaom-sys") (v "0.5.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0k5c3mqz67ary8ajxp60slj1m679wz0nz5xcbbc3z6pqi6l7i81y") (l "aom")))

(define-public crate-libaom-sys-0.6.0 (c (n "libaom-sys") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0jnr2p694xs0lfa6m0mhvdxnq85vmcz5ymi5jybz4kmm62nc6p3q") (l "aom")))

(define-public crate-libaom-sys-0.6.1 (c (n "libaom-sys") (v "0.6.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "16h7h0x7bzzgcxyslyz82h8jsws8wy0vi48ymch39avlrg9c3x0j") (l "aom")))

(define-public crate-libaom-sys-0.8.0 (c (n "libaom-sys") (v "0.8.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1nlpvz0jkfa6md93wk6di0cxp74aw5xmy40p7sh21j59pblm14wy") (l "aom")))

(define-public crate-libaom-sys-0.9.0 (c (n "libaom-sys") (v "0.9.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "06qnn6dvb30sypnkp3drvc5jwgykznf6f6cjk0mpdaaixr150lhy") (l "aom")))

(define-public crate-libaom-sys-0.9.1 (c (n "libaom-sys") (v "0.9.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "0kn20kwh3jgm955s8b19wxchz4f58c7kj6a6mb3c6433m67mdr0z") (l "aom")))

(define-public crate-libaom-sys-0.10.0 (c (n "libaom-sys") (v "0.10.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1g1gybadaplbhmiqa31sqkzb9g43kz0a989dqrn0xf0kx0mla7rp") (y #t) (l "aom")))

(define-public crate-libaom-sys-0.10.1 (c (n "libaom-sys") (v "0.10.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "19xichk2i3qvd7c7bvp5mwbxzfcklm275w8fw1m0i6byhrgzvpij") (l "aom")))

(define-public crate-libaom-sys-0.11.0 (c (n "libaom-sys") (v "0.11.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "18x586iryclrwwv9kn94j42agsrknlsw3w339gf732w0k1sjj5kl") (l "aom")))

(define-public crate-libaom-sys-0.12.0 (c (n "libaom-sys") (v "0.12.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "11rrm311p22ml5163mdn2r8ylaaq0b6g1vrmdmzjyfdz1lax5nnm") (l "aom")))

(define-public crate-libaom-sys-0.12.1 (c (n "libaom-sys") (v "0.12.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "032v27kh6pdgzwixgqwf2af27n8jl1639cfwndn9rr2mmmzpr0dw") (l "aom")))

(define-public crate-libaom-sys-0.13.0 (c (n "libaom-sys") (v "0.13.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "02wfmv29qi40xzhm11xdf68mz44sc4cpivmrzfxwd89fk8dm6j8q") (l "aom")))

(define-public crate-libaom-sys-0.14.0 (c (n "libaom-sys") (v "0.14.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0wfd1xlhbpla92xkjxd19hj77n3x75n7pcnkqdl8j5z57x4l20k1") (l "aom")))

(define-public crate-libaom-sys-0.15.0 (c (n "libaom-sys") (v "0.15.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0l7vwbmy9s0xhcg3dffpj5w966q1p781jgani6nia6cwrr9w5ndh") (l "aom")))

(define-public crate-libaom-sys-0.16.0+libaom.3.8.1 (c (n "libaom-sys") (v "0.16.0+libaom.3.8.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1b2c2zn17sl6adcpffawja3fl7dsp3n6mv5scnnyz3s2zamj7dd1") (l "aom")))

