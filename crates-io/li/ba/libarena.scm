(define-module (crates-io li ba libarena) #:use-module (crates-io))

(define-public crate-libarena-0.1.0 (c (n "libarena") (v "0.1.0") (h "1xvwgdvcrgbdslx1v6vqhadjc1hsslajsyfmsgvr2jkq28kn7hxa")))

(define-public crate-libarena-0.1.1 (c (n "libarena") (v "0.1.1") (h "0sgc9xdfv0zk1nysjg00c5zc3h970p0aym42403xaqj8226qlzpk")))

(define-public crate-libarena-0.1.2 (c (n "libarena") (v "0.1.2") (h "14qalg01yi5j2hj3n4bv0y03dvv9n00mq63pn9zvc9fa52knd709")))

(define-public crate-libarena-0.1.3 (c (n "libarena") (v "0.1.3") (h "0ppvr8d9k9rpd5svz8c0ky1plkp570h2s8z06vm31yzk06l07dps")))

(define-public crate-libarena-0.1.4 (c (n "libarena") (v "0.1.4") (h "08b45wwkwmcnnkw4sp2lhnd42cyx96x0kvxmb5337c5bq60pfjzg")))

(define-public crate-libarena-0.1.5 (c (n "libarena") (v "0.1.5") (h "177fk3qf41w7rh2pbw5wn5knyviqnkhhc6xw41vqmnwr02l9p18a")))

(define-public crate-libarena-0.1.6 (c (n "libarena") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1if5rylahiksm88spvf9p6a81h1sya0iq56q0s3ryc8wlcbn6glx")))

