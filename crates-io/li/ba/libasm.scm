(define-module (crates-io li ba libasm) #:use-module (crates-io))

(define-public crate-libasm-0.1.0 (c (n "libasm") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^0.12.5") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1z6cir2dcxmhcxdp6dyljm86zgy8yi9xwhgpzzai5y5a3b59xick")))

(define-public crate-libasm-0.1.1 (c (n "libasm") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^0.12.5") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "07bmhij1wld0q4m1f9v9gw606f4iwyv5x9ik66k6h3baykik7j58")))

(define-public crate-libasm-0.1.2 (c (n "libasm") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^0.2.1") (d #t) (k 0)) (d (n "syn") (r "^0.12.5") (f (quote ("full" "extra-traits" "visit"))) (d #t) (k 0)))) (h "1zq6wdph1mygxw932pfzmg3qwndkhifs4556x2sbc4caqfhdc1kh")))

