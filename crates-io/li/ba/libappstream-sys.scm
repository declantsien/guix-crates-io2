(define-module (crates-io li ba libappstream-sys) #:use-module (crates-io))

(define-public crate-libappstream-sys-0.1.0 (c (n "libappstream-sys") (v "0.1.0") (d (list (d (n "gio-sys") (r "^0.19") (d #t) (k 0)) (d (n "glib-sys") (r "^0.19") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0wbr2spqfw3ms8xqc6j1x2pvx7yvp20cm2fi8y6anysaw8i74krf") (l "appstream")))

