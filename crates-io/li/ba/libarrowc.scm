(define-module (crates-io li ba libarrowc) #:use-module (crates-io))

(define-public crate-libarrowc-0.0.1 (c (n "libarrowc") (v "0.0.1") (d (list (d (n "arrow-parser") (r "^0.0.2") (d #t) (k 0)) (d (n "ordered-float") (r "^3.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "rustc-hash") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive" "rc"))) (d #t) (k 0)))) (h "153alsy1dc6bpzwkgipi4ll8cn40nb6cyhqbj8hvg8jzqwxazm1i")))

