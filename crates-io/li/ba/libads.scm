(define-module (crates-io li ba libads) #:use-module (crates-io))

(define-public crate-libads-0.1.0-alpha (c (n "libads") (v "0.1.0-alpha") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.0") (d #t) (k 0)) (d (n "select") (r "^0.3.0") (d #t) (k 0)) (d (n "slog") (r "^1.4.1") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.1.0") (d #t) (k 0)))) (h "0xcs5zljqcvksdp93nkw11adhjzz6hqvn7k0vfmwbciv0h4g8waa")))

(define-public crate-libads-0.1.0-beta (c (n "libads") (v "0.1.0-beta") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.0") (d #t) (k 0)) (d (n "select") (r "^0.3.0") (d #t) (k 0)) (d (n "slog") (r "^1.4.1") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.1.0") (d #t) (k 0)))) (h "0d3c98wg33hbjivhwqjjxil4zxifyc2058i2zl2jkqvmzpzlimn6")))

