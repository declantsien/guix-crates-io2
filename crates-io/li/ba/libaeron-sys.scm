(define-module (crates-io li ba libaeron-sys) #:use-module (crates-io))

(define-public crate-libaeron-sys-1.0.0+1.28.0 (c (n "libaeron-sys") (v "1.0.0+1.28.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "1gzbxa50di0c8ldwric56d1ahb4qa19fais4cdx080ghjm2brmnv") (f (quote (("static"))))))

(define-public crate-libaeron-sys-1.0.0+1.28.2 (c (n "libaeron-sys") (v "1.0.0+1.28.2") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "08jmmzqnjzc15wvrrv8y8ppj4k76x9v5hqnkibvjmy56g7cnd4hk") (f (quote (("static"))))))

(define-public crate-libaeron-sys-1.0.0+1.29.0 (c (n "libaeron-sys") (v "1.0.0+1.29.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "04s81kbjnmkl10x9kzk6s6nb47ycxswk0vx0wmc6i6q13sq9pf61") (f (quote (("static"))))))

(define-public crate-libaeron-sys-1.0.0+1.30.0 (c (n "libaeron-sys") (v "1.0.0+1.30.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "11gglxmg86qq2ah5x664vlp4z5223lja9wjv9c3ggsjx7jwir24v") (f (quote (("static"))))))

(define-public crate-libaeron-sys-1.0.0+1.31.0 (c (n "libaeron-sys") (v "1.0.0+1.31.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "1dfm69fmzzg4sb0sikk1w1byjv5zx898frgv703wffk0kagnz82n") (f (quote (("static"))))))

(define-public crate-libaeron-sys-1.0.0+1.31.1 (c (n "libaeron-sys") (v "1.0.0+1.31.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "15c01jggzb2l0vmab4pgd32czzharidqvblnfz0hrjpbmp5qrbbx") (f (quote (("static"))))))

(define-public crate-libaeron-sys-1.0.0+1.40.0 (c (n "libaeron-sys") (v "1.0.0+1.40.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "0426m3c7zyxycvy6lkdj1y0yzgni06zg5yqlxp93nygdy5hy3bzb") (f (quote (("static"))))))

(define-public crate-libaeron-sys-1.42.1 (c (n "libaeron-sys") (v "1.42.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "0xjs5mgbfwhh8a6054j5r4q62qm7qwi2pa2d5jd9m57wbawzn39i") (f (quote (("static"))))))

(define-public crate-libaeron-sys-1.44.1 (c (n "libaeron-sys") (v "1.44.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "1q5z2qdgskqjm675kfnqmlfs7lnlb56dnrnb6x0417qq2pizf1ad") (f (quote (("static"))))))

