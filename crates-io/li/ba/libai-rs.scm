(define-module (crates-io li ba libai-rs) #:use-module (crates-io))

(define-public crate-libai-rs-0.1.0 (c (n "libai-rs") (v "0.1.0") (h "1pw7qsrhhdcfscla7k77l2z1id5ci8nyi0xh04a4lxirz1nn0w0f") (y #t)))

(define-public crate-libai-rs-0.1.1 (c (n "libai-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("with-fuzzy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "148vjapj63fqip8qw633lxd1sw6qfmphlc48517yz24cgrvygp9b") (y #t)))

(define-public crate-libai-rs-0.1.2 (c (n "libai-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("with-fuzzy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vrlsspw8zamlaslh0gnbb0jpvwkxwpzppzl6d3cqv7k6kw6sfcd") (y #t)))

(define-public crate-libai-rs-0.1.4 (c (n "libai-rs") (v "0.1.4") (d (list (d (n "reqwest") (r "^0.11.18") (f (quote ("json"))) (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("with-fuzzy"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.180") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1m6fb41s1x59wjd02j5rx0nb1y5k4mzdxaxlsn64x98pmnbcsybn") (y #t)))

