(define-module (crates-io li ba libafl_derive) #:use-module (crates-io))

(define-public crate-libafl_derive-0.1.0 (c (n "libafl_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0lkp0zgljdgfm6a5iwxlhdg4siagh19cr8mmf7c1pfnbyncswv1f")))

(define-public crate-libafl_derive-0.2.1 (c (n "libafl_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "137y10zdicwgfmjwfwh8gb2mk8yrv9ypgrgs4marmiigrk9dgfq3")))

(define-public crate-libafl_derive-0.3.0 (c (n "libafl_derive") (v "0.3.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0vsbw0xl9bny3iazah67raflh3lq0ddahvapw0c6y1alzf35f0x2")))

(define-public crate-libafl_derive-0.3.1 (c (n "libafl_derive") (v "0.3.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1f3sfsn7zzv9z5s8qr56s2qyg4kvkwqmf5drdjim7ik89l6iy72s")))

(define-public crate-libafl_derive-0.3.2 (c (n "libafl_derive") (v "0.3.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1njhwphfynpchgfs5ym3kblzrajhji1phi99smpwi70vgddnmzr3")))

(define-public crate-libafl_derive-0.4.0 (c (n "libafl_derive") (v "0.4.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07c1xjhhj2zglrg22f6s17r6r454i60j5r8d11r26kbvlx6j8jxb")))

(define-public crate-libafl_derive-0.5.0 (c (n "libafl_derive") (v "0.5.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0j2aymlawgvvn5cck501krdssh7sn8qn7ca8mg98smpha942bwqq")))

(define-public crate-libafl_derive-0.6.0 (c (n "libafl_derive") (v "0.6.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1hsdf31hhiysgpabn15x40paq5hj608zwwxdrzwqcp6ahyy67f5a")))

(define-public crate-libafl_derive-0.6.1 (c (n "libafl_derive") (v "0.6.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c0q3c4d3nrvabsw49s3x4wbfrdkanbcj4brzp48g4p9dg7l36fn")))

(define-public crate-libafl_derive-0.7.0 (c (n "libafl_derive") (v "0.7.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1pny5q9miyfy9f2iwayi5x2096g1k694hhm9w3s6pf0zwaav7mwg")))

(define-public crate-libafl_derive-0.7.1 (c (n "libafl_derive") (v "0.7.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0bqw7vn6rb1754258cvfcwmb1c2pxr1bb7p7szcafhqg5g8xg2yk")))

(define-public crate-libafl_derive-0.8.0 (c (n "libafl_derive") (v "0.8.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1h4dmfhdaxs6s17m5q2k72bncf09i8b3vcjby3f7f5i96nfbbkqs")))

(define-public crate-libafl_derive-0.8.1 (c (n "libafl_derive") (v "0.8.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "11954wz099aqxmypkbqwvb7ckbal94i279nw6xj2v5m7kzrb7xr0")))

(define-public crate-libafl_derive-0.8.2 (c (n "libafl_derive") (v "0.8.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1zrq6q0wgzaylg2asbxvyawna89zddhxw9xi9wnq1xcjcqqyrx6i")))

(define-public crate-libafl_derive-0.9.0 (c (n "libafl_derive") (v "0.9.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1lrvh99w8rinimlf5a7ws9ir8md78z8gx4k497f5zx3zcs5510w1")))

(define-public crate-libafl_derive-0.10.0 (c (n "libafl_derive") (v "0.10.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0qwamw9gbqba619g6mcqhymqy009f285kjq0a13n9nj3yivrmgy2")))

(define-public crate-libafl_derive-0.10.1 (c (n "libafl_derive") (v "0.10.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0k4vd340l3l0xlr5gha2zjq34swidgsczw9sxk20nsk8w58f2q7m")))

(define-public crate-libafl_derive-0.11.0 (c (n "libafl_derive") (v "0.11.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qz8zdvvnf0x0mh0i6m6324hxfxsrpllxxjpd37n5s4ii6x2qzkx")))

(define-public crate-libafl_derive-0.11.1 (c (n "libafl_derive") (v "0.11.1") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0c9348kmdja21307xayc2lqrdl5m9fz1pz5z06hsms326ldhx69k")))

(define-public crate-libafl_derive-0.11.2 (c (n "libafl_derive") (v "0.11.2") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0s3mrdn5w3ynjcjnhc9zbvdc20ck8jpw4ppx2qd741y7viw9i12w")))

(define-public crate-libafl_derive-0.12.0 (c (n "libafl_derive") (v "0.12.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ick3z93i5g8zgf1d5fkmiiys60iwp3l3zib24aiar188ip1j84k")))

