(define-module (crates-io li ba libavif) #:use-module (crates-io))

(define-public crate-libavif-0.1.0 (c (n "libavif") (v "0.1.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.4") (d #t) (k 0)))) (h "1nipw8rcprjc0fm8jdjqk0nh178ll3wf1hj90hqzrw1x5js3z8y9")))

(define-public crate-libavif-0.1.2 (c (n "libavif") (v "0.1.2") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.4.2") (k 0)))) (h "0hcygcrk6azssx91xfcjgnalvinpkkhxmz5882hmlrvp559kb2lv") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom") ("__internal_aom_generic_target" "libavif-sys/__internal_aom_generic_target"))))))

(define-public crate-libavif-0.2.0 (c (n "libavif") (v "0.2.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.5.0") (k 0)))) (h "1a99fv2a1cgkx8q41nj5xrdn8kvfha5xda1w3a7csnbk6bv40g3j") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom") ("__internal_aom_generic_target" "libavif-sys/__internal_aom_generic_target")))) (y #t)))

(define-public crate-libavif-0.2.1 (c (n "libavif") (v "0.2.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.5.0") (k 0)))) (h "0ng0nnymji07jdrxmwb83hjcx76avin1mri71gg3dnkpw5xggrws") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom") ("__internal_aom_generic_target" "libavif-sys/__internal_aom_generic_target"))))))

(define-public crate-libavif-0.3.0 (c (n "libavif") (v "0.3.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.6.0") (k 0)))) (h "1wdz54m5rc87yrh84zcl0a45b5z5nna73grwh5kg5ghmx23kl649") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom") ("__internal_aom_generic_target" "libavif-sys/__internal_aom_generic_target"))))))

(define-public crate-libavif-0.4.0 (c (n "libavif") (v "0.4.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.7.0") (k 0)))) (h "1xmr8s7dvq5s9088z1y48c8xx7snzppmi83gd0pkaj64vkh3l77i") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom"))))))

(define-public crate-libavif-0.5.0 (c (n "libavif") (v "0.5.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.8.0") (k 0)))) (h "13lyw649kbxycsj7nkd2qfwsm2zvqw3qziigk0p3lfr5gywl5l3c") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom"))))))

(define-public crate-libavif-0.6.0 (c (n "libavif") (v "0.6.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.9.0") (k 0)))) (h "1bvqw31gl9nfl19wn9glvl82k3k728gpk3hb26svx3yw369zx04b") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom"))))))

(define-public crate-libavif-0.7.0 (c (n "libavif") (v "0.7.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.10.0") (k 0)))) (h "029mvaxzvjq96znsrvcy6zdsd189dc1pj82kin93k37v0fb7m375") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom"))))))

(define-public crate-libavif-0.7.1 (c (n "libavif") (v "0.7.1") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.11.0") (k 0)))) (h "0wa8p1jpp7913828h1b9h1x64wjlbiczc7yax1x7zv8xr1nmymyh") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom"))))))

(define-public crate-libavif-0.8.0 (c (n "libavif") (v "0.8.0") (d (list (d (n "image") (r "^0.23") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.12.0") (k 0)))) (h "0jbjwab678j02scbyfwgfm5jqpx40s79g9cq35a0zqkzdmsbw5a3") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom"))))))

(define-public crate-libavif-0.9.0 (c (n "libavif") (v "0.9.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.12.0") (k 0)))) (h "03rk61ya2hgcahnr8l93finw5ywhg7sjgd25kca3yzf9nd6rjcyn") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom"))))))

(define-public crate-libavif-0.10.0 (c (n "libavif") (v "0.10.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.13.0") (k 0)))) (h "0f6bh44kdd6rw0rkmsva0pln8vgsqqfhijw7wkcvxf14aqa9lvvy") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom")))) (r "1.56")))

(define-public crate-libavif-0.10.1 (c (n "libavif") (v "0.10.1") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.13.1") (k 0)))) (h "0302gqympkjxxhfy4z9qdi8q88r439p4z0pbralzz2x5ich3ikbc") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom")))) (r "1.56")))

(define-public crate-libavif-0.11.0 (c (n "libavif") (v "0.11.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.14.0") (k 0)))) (h "0y2fq37d4hjsdfr3nw9jyb0ymvdq38zw8c0s5x7cban2wkm5fjy0") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom")))) (r "1.56")))

(define-public crate-libavif-0.12.0 (c (n "libavif") (v "0.12.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.15.0") (k 0)))) (h "0p13fnmmfrwaq9zcbkg4338g5nnl0jrr50p8lpiv61vj7m5kx7ny") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom")))) (r "1.63")))

(define-public crate-libavif-0.13.0 (c (n "libavif") (v "0.13.0") (d (list (d (n "image") (r "^0.24") (d #t) (k 2)) (d (n "libavif-sys") (r "^0.16.0") (k 0)))) (h "0vx615231z643kif0n0x24y1rslilscd0hgyzssa39rq202fn1z3") (f (quote (("default" "codec-dav1d" "codec-rav1e") ("codec-rav1e" "libavif-sys/codec-rav1e") ("codec-dav1d" "libavif-sys/codec-dav1d") ("codec-aom" "libavif-sys/codec-aom")))) (r "1.63")))

