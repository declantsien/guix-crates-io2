(define-module (crates-io li ba libafl_cc) #:use-module (crates-io))

(define-public crate-libafl_cc-0.1.0 (c (n "libafl_cc") (v "0.1.0") (h "0fmjr772xxb77wn4vg080y0pzza6gbgd3zi3rp9xxqj9vns2dxsm")))

(define-public crate-libafl_cc-0.2.1 (c (n "libafl_cc") (v "0.2.1") (h "1ngk8238h5mj9j62b5z38x783ryh2xpxr2qy2z4jkhny9jvnzs4x")))

(define-public crate-libafl_cc-0.3.0 (c (n "libafl_cc") (v "0.3.0") (h "0v8hdi4xg0qvckva6m3g33jalzsmy5nrzibs6q0j4a5m93zdcqaw")))

(define-public crate-libafl_cc-0.3.1 (c (n "libafl_cc") (v "0.3.1") (h "0vrdxrbzvabh3smkzb1wfgjgipw636x18mshrqw0hxx1sgg475l5")))

(define-public crate-libafl_cc-0.3.2 (c (n "libafl_cc") (v "0.3.2") (h "17n8d5h0ckppdd6n2hdf7fhgf6804rr6z5sxyg37hcs4d52zlfqd")))

(define-public crate-libafl_cc-0.4.0 (c (n "libafl_cc") (v "0.4.0") (h "1xjkzfza6hxmp1bh2llqr7dcfjr57pd6kk3gkfxrl7gm667rv6mi")))

(define-public crate-libafl_cc-0.5.0 (c (n "libafl_cc") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0wrwmxb5ih2v7l2yy7rd5wv239n8mmh2f7m1qqygg623n4i6s8y1")))

(define-public crate-libafl_cc-0.6.0 (c (n "libafl_cc") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_os = \"macos\")") (k 1)))) (h "1qia3y4mhikc2rrvwhy86kzrhw96lkw8mb372j1qvc6x7lq0gq5b")))

(define-public crate-libafl_cc-0.6.1 (c (n "libafl_cc") (v "0.6.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)))) (h "1kx7l92xh6z21dgcjk93yaxy1634g0kb9rpg21fnhh7kvhx2hrcs")))

(define-public crate-libafl_cc-0.7.0 (c (n "libafl_cc") (v "0.7.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)))) (h "1jx0id6ssshmklraaizc2agg82c9wdrb5h3v71mszd5v2vpyi3f4")))

(define-public crate-libafl_cc-0.7.1 (c (n "libafl_cc") (v "0.7.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "which") (r "^4.2.2") (d #t) (k 1)))) (h "1bx6g0gwa3nqcadmp3rccy8i8jvkaikxk7z2qvcbn4xw1ljy8n99")))

(define-public crate-libafl_cc-0.8.0 (c (n "libafl_cc") (v "0.8.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "which") (r "^4.2.2") (d #t) (k 1)))) (h "014drhfjkr247npcianm24w16cfpch6xbdkwb43fmyzrjn1yda11")))

(define-public crate-libafl_cc-0.8.1 (c (n "libafl_cc") (v "0.8.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0z227yn29n5jmac8lw2p8fxiy13mpdmw9rqp1gwglffbaf8pm03r")))

(define-public crate-libafl_cc-0.8.2 (c (n "libafl_cc") (v "0.8.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "10m9p7pzv6h189gyjl4i3xqdqk70bfnzjza1bv6j67llpn49jnh0")))

(define-public crate-libafl_cc-0.9.0 (c (n "libafl_cc") (v "0.9.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0zjdv96c5bvnvh0igjirl9i2abd2pd6nf7fgzxhx55cxci3jqp41")))

(define-public crate-libafl_cc-0.10.0 (c (n "libafl_cc") (v "0.10.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "0jfppxyijm2084i1qql87c7p1cymigi2qkqibmgzmgl4ys09c9zx")))

(define-public crate-libafl_cc-0.10.1 (c (n "libafl_cc") (v "0.10.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 1)))) (h "14y2bb69pdq88v0lbhw766vciylw081dsa34n94hbhrrih9adq6h")))

(define-public crate-libafl_cc-0.11.0 (c (n "libafl_cc") (v "0.11.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "03mqv8aarfcq0avdix25n3l1aymg1bv85apj0bg6z7z0jb31c5ip")))

(define-public crate-libafl_cc-0.11.1 (c (n "libafl_cc") (v "0.11.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "0y2lfpsjgrm7b9cbnmcx0ng75h2g300ii2c5vafhr8cmlf86hxji")))

(define-public crate-libafl_cc-0.11.2 (c (n "libafl_cc") (v "0.11.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "112rcffni614n4qwp86l5bnha5rzzsgkwkwmqglgcxp1wcmgm24n")))

(define-public crate-libafl_cc-0.12.0 (c (n "libafl_cc") (v "0.12.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3") (d #t) (t "cfg(target_vendor = \"apple\")") (k 1)) (d (n "serde") (r "^1.0") (f (quote ("alloc" "derive"))) (k 0)) (d (n "which") (r "^4.4") (d #t) (k 1)))) (h "1hb3pz97s9d12miqk37b89g1j2086n4g99088wd5i24qpmnx8im1")))

