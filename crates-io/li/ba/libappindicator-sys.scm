(define-module (crates-io li ba libappindicator-sys) #:use-module (crates-io))

(define-public crate-libappindicator-sys-0.1.0 (c (n "libappindicator-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "gtk-sys") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0vm690gmwljrwvr50c53mjbv4hk2ci3i30qzncgh3mqc2fyyqaci")))

(define-public crate-libappindicator-sys-0.1.1 (c (n "libappindicator-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "gtk-sys") (r "^0.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1nkrhvry5rrasx5nkrapc0ncwyq1xdh23hh62cndjqsvk2dyr9gy")))

(define-public crate-libappindicator-sys-0.1.2 (c (n "libappindicator-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "gtk-sys") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1p429iqww26h56jpd7izx0mkzjy29dxz1wly5v1rmg336j4nn0g6")))

(define-public crate-libappindicator-sys-0.2.0 (c (n "libappindicator-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "gtk-sys") (r "^0.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "19gfs3szypir95rhqmjb3694mmq2pp1702crgsmqa18yfhakxqx0")))

(define-public crate-libappindicator-sys-0.3.0 (c (n "libappindicator-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "gtk-sys") (r "^0.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08ca3qcn3s39cjy78g8nj76p0s8zidsgpvkvpixcz0y79qlw949r")))

(define-public crate-libappindicator-sys-0.4.0 (c (n "libappindicator-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "gtk-sys") (r "^0.9.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "18q7xkgrsg0dlz8ynf3bhcqbyllqgxkgyy1hb77zzn7shvn3hqin")))

(define-public crate-libappindicator-sys-0.5.0 (c (n "libappindicator-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "gtk-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "07b5r3wy5kq4x3zsmgbgmz2645yr5fyy1finkbsj3cbc38hbl6cp")))

(define-public crate-libappindicator-sys-0.6.0 (c (n "libappindicator-sys") (v "0.6.0") (d (list (d (n "gtk-sys") (r "^0.14") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11q9zcl54vymfazx5nfvf3r02ha5g9xdqsyygc9jlb8m065ffr31")))

(define-public crate-libappindicator-sys-0.6.1 (c (n "libappindicator-sys") (v "0.6.1") (d (list (d (n "gtk-sys") (r "^0.14") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "16yhk1522nphawi1cdwrfx0qxh69kpfsmhlwmm0wylfsfi62ayr3")))

(define-public crate-libappindicator-sys-0.7.0 (c (n "libappindicator-sys") (v "0.7.0") (d (list (d (n "gtk-sys") (r "^0.15") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0kyp06jz3hw5d4l6pbybcks2jzfbss387wgk5yzgw4gwbj6cpgch")))

(define-public crate-libappindicator-sys-0.7.1 (c (n "libappindicator-sys") (v "0.7.1") (d (list (d (n "gtk-sys") (r "^0.15") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "168xkg8ng0insa39p8phhxdybm2zsc9lcmdflanm0ybxf8kj4g6q")))

(define-public crate-libappindicator-sys-0.7.2 (c (n "libappindicator-sys") (v "0.7.2") (d (list (d (n "gtk-sys") (r "^0.15") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "0d95ix5dfxk7lpl39rbzgpr9grx65jpmhay5yicahdm7w6d023ja")))

(define-public crate-libappindicator-sys-0.7.3 (c (n "libappindicator-sys") (v "0.7.3") (d (list (d (n "gtk-sys") (r "^0.15") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "1ai651cszjy9jnd1qn0ap0h2fl6mszk92wyf7g6aikkk35lbdczi") (f (quote (("default" "backcompat") ("backcompat"))))))

(define-public crate-libappindicator-sys-0.8.0 (c (n "libappindicator-sys") (v "0.8.0") (d (list (d (n "gtk-sys") (r "^0.16") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "061r2q38k8jj7i3awh25n9b95l52xa1yq095k09rdvlwm2zb5z08") (f (quote (("default" "backcompat") ("backcompat"))))))

(define-public crate-libappindicator-sys-0.9.0 (c (n "libappindicator-sys") (v "0.9.0") (d (list (d (n "gtk-sys") (r "^0.18") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "1bsw2mcxil3zm4zzdir76i7xnaqaq30cd9qpviccrvdb70hwb7kf") (f (quote (("default" "backcompat") ("backcompat"))))))

