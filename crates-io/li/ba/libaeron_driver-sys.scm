(define-module (crates-io li ba libaeron_driver-sys) #:use-module (crates-io))

(define-public crate-libaeron_driver-sys-1.0.0+1.28.0 (c (n "libaeron_driver-sys") (v "1.0.0+1.28.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "0xr5ibb1mvqjf8chylvb47ad3562m56acxpgxm1a9j4jgpf0mx44") (f (quote (("static"))))))

(define-public crate-libaeron_driver-sys-1.0.0+1.28.2 (c (n "libaeron_driver-sys") (v "1.0.0+1.28.2") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "076n2yk9mjdmz1lqxqrxlivdv16m1y1c2d2miz355swj52vsmcpl") (f (quote (("static"))))))

(define-public crate-libaeron_driver-sys-1.0.0+1.29.0 (c (n "libaeron_driver-sys") (v "1.0.0+1.29.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "1r0yx1h2c8a4cvy42c1kifgha1wz9d5d4pbzk3hr1vi82pzgkw25") (f (quote (("static"))))))

(define-public crate-libaeron_driver-sys-1.0.0+1.30.0 (c (n "libaeron_driver-sys") (v "1.0.0+1.30.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "14ivnaa5qjnp6px8myxa8xwx081g0m4jqfj2ni4y0vid7p0wlgy7") (f (quote (("static"))))))

(define-public crate-libaeron_driver-sys-1.0.0+1.31.0 (c (n "libaeron_driver-sys") (v "1.0.0+1.31.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "0frvvahdyi1r0jbnymikgrfph0s4nbmf08237wf8k2czg5shlwsm") (f (quote (("static"))))))

(define-public crate-libaeron_driver-sys-1.0.0+1.31.1 (c (n "libaeron_driver-sys") (v "1.0.0+1.31.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "186a0zn33cijni9prnjx6i6idciyr4w3nh7lvf9djx2907fw649f") (f (quote (("static"))))))

(define-public crate-libaeron_driver-sys-1.0.0+1.40.0 (c (n "libaeron_driver-sys") (v "1.0.0+1.40.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "0sabcg5sc38wv1cis9gbf6nkdkbjar4ldq0wfyn74z20284i39rv") (f (quote (("static"))))))

(define-public crate-libaeron_driver-sys-1.42.1 (c (n "libaeron_driver-sys") (v "1.42.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "0pd632p9dq4wpq8561ics3jz3406gyapfc2mpjrgr05xvwkbi5hr") (f (quote (("static"))))))

(define-public crate-libaeron_driver-sys-1.44.1 (c (n "libaeron_driver-sys") (v "1.44.1") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "dunce") (r "^1.0") (d #t) (k 1)))) (h "0alpq7m5hj3d01dxz90lx9kzxqa5q8pii3vd4w3gc9wqkdjzglm1") (f (quote (("static"))))))

