(define-module (crates-io li ba libamdgpu_top) #:use-module (crates-io))

(define-public crate-libamdgpu_top-0.2.0 (c (n "libamdgpu_top") (v "0.2.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.1.6") (d #t) (k 0)))) (h "0k1kp2hnn12yn93gll8qfqcdc3ysa70j281af99y9lmhminkhxd3")))

(define-public crate-libamdgpu_top-0.2.1 (c (n "libamdgpu_top") (v "0.2.1") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.2.1") (d #t) (k 0)))) (h "0x2jj59q7m5854dn04yqbk11wfkyqmnfis248n2d0ay4f49x8imk")))

(define-public crate-libamdgpu_top-0.2.2 (c (n "libamdgpu_top") (v "0.2.2") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.2.2") (d #t) (k 0)))) (h "183d7iydpkpb8y7c4nhksmr83mjw110csvmx74inm01pq495vjzb")))

(define-public crate-libamdgpu_top-0.2.3 (c (n "libamdgpu_top") (v "0.2.3") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.2.2") (d #t) (k 0)))) (h "1ndwzgcqlg1kp1lw6rb2w4rcffjczz69m0zqr9b3qvgs53rvzg8f")))

(define-public crate-libamdgpu_top-0.3.0 (c (n "libamdgpu_top") (v "0.3.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.2.2") (d #t) (k 0)))) (h "0lg05xqj974ywd615g9l6kglb9wh5hynbfh73kf6m2a2bzmf6xp9")))

(define-public crate-libamdgpu_top-0.3.1 (c (n "libamdgpu_top") (v "0.3.1") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.2.2") (d #t) (k 0)))) (h "1s719ir274xmvg21qkgalvvyqxpl1wyfr2pkq38z2illb7m5nli3")))

(define-public crate-libamdgpu_top-0.4.0 (c (n "libamdgpu_top") (v "0.4.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.3.0") (d #t) (k 0)))) (h "1rp7aj3qi8cj51ajnphwdz13h2l701hvf72m3v86kimfw5pcdpjc")))

(define-public crate-libamdgpu_top-0.5.0 (c (n "libamdgpu_top") (v "0.5.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.4.0") (d #t) (k 0)))) (h "1kdh081jwsvjmymxf1abvs9ck4ywq8j9jr4996nlwnzdgidshlzh")))

(define-public crate-libamdgpu_top-0.6.0 (c (n "libamdgpu_top") (v "0.6.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.4.1") (d #t) (k 0)))) (h "0c59kq4rxhxrszzfzpzmykl4n6x4x911gncrwfja6dp4w8kl9acd")))

(define-public crate-libamdgpu_top-0.6.1 (c (n "libamdgpu_top") (v "0.6.1") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.4.1") (d #t) (k 0)))) (h "1jgfkngq8bzbxhbd7519wrz56z2pknlhv7ka0djmzr8bqpc2vrph")))

(define-public crate-libamdgpu_top-0.7.0 (c (n "libamdgpu_top") (v "0.7.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.5.0") (d #t) (k 0)))) (h "1xkkf5x5qf4ya52kr87k2wkllfi6p9bx2qp8y6516v83fff41004")))

(define-public crate-libamdgpu_top-0.8.0 (c (n "libamdgpu_top") (v "0.8.0") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.7.0") (d #t) (k 0)))) (h "0zs7mff8fj3mfhlz73qxjhnw1rr6ygwfwp61y1dfpdbfz8n15yjn")))

(define-public crate-libamdgpu_top-0.8.2 (c (n "libamdgpu_top") (v "0.8.2") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.7.1") (d #t) (k 0)))) (h "0pvc1nckb3q8rjs7n9lpfhra3h9g73psq3yxg9kwgqmnywzh5hnf")))

(define-public crate-libamdgpu_top-0.8.3 (c (n "libamdgpu_top") (v "0.8.3") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.7.1") (d #t) (k 0)))) (h "05smj4m669bgbldzx2rp918xg92qr9l1lz5srazhx5brak7swd3k")))

(define-public crate-libamdgpu_top-0.8.5 (c (n "libamdgpu_top") (v "0.8.5") (d (list (d (n "anyhow") (r "1.0.*") (d #t) (k 0)) (d (n "libdrm_amdgpu_sys") (r "^0.7.3") (d #t) (k 0)))) (h "1df8hiqjizm75kl2n0hdly08b3fync0rblk0abbvcv69492hz72j")))

