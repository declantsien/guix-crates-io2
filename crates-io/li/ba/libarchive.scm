(define-module (crates-io li ba libarchive) #:use-module (crates-io))

(define-public crate-libarchive-0.1.0 (c (n "libarchive") (v "0.1.0") (d (list (d (n "libarchive3-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)))) (h "0r62nmbiv4qhdvrnxcp8ihlhyrb8ws5zc2y6iacglqj3l0zl6nmy")))

(define-public crate-libarchive-0.1.1 (c (n "libarchive") (v "0.1.1") (d (list (d (n "libarchive3-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r ">= 0.2.0") (d #t) (k 0)))) (h "0569p7hwgjw1k025m1fh74wf8aimlq54mm7m82537bqrrli6p81x")))

