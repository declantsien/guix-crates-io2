(define-module (crates-io li ba libafl_inline_c) #:use-module (crates-io))

(define-public crate-libafl_inline_c-0.1.0 (c (n "libafl_inline_c") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libafl_inline_c_macro") (r "^0.1") (d #t) (k 0)) (d (n "predicates") (r "^2") (d #t) (k 0)) (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)) (d (n "target-lexicon") (r "^0.11") (d #t) (k 0)) (d (n "tempfile") (r "^3.1") (d #t) (k 0)))) (h "1cr4nwhsbzfl2g3lrvjjvz80rjisz4knfq3fia796idnb3fqqday")))

