(define-module (crates-io li ba libacc) #:use-module (crates-io))

(define-public crate-libacc-0.1.0 (c (n "libacc") (v "0.1.0") (d (list (d (n "aes-gcm-siv") (r "^0.11.1") (d #t) (k 0)) (d (n "bson") (r "^2.9.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "0ycw3acxry0b8lg8c3h71kgrf6xfdx811kfrwrar0d6j0gi8j3ph") (r "1.72.0")))

