(define-module (crates-io li ba libatrace) #:use-module (crates-io))

(define-public crate-libatrace-0.1.0 (c (n "libatrace") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.48") (d #t) (k 0)))) (h "0zs14hsrqb05l5j7ly0w1g4pbrj76lpl6613m5llsc5v1rw22dj5")))

