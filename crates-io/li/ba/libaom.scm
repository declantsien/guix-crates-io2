(define-module (crates-io li ba libaom) #:use-module (crates-io))

(define-public crate-libaom-0.2.0 (c (n "libaom") (v "0.2.0") (d (list (d (n "aom-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "av-codec") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "av-data") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)))) (h "0s09v86anih7kpd3mxca74bbv3s42c1qbhb3wmyhiici85zdwyvg") (f (quote (("default" "codec-trait") ("codec-trait" "av-codec") ("build" "aom-sys/build_sources"))))))

(define-public crate-libaom-0.2.1 (c (n "libaom") (v "0.2.1") (d (list (d (n "aom-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "av-codec") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "av-data") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)))) (h "154jlbm6g7ncj6wggcm8940v6c8kmwjqim9xppa17jljsmdm09d4") (f (quote (("default" "codec-trait") ("codec-trait" "av-codec") ("build" "aom-sys/build_sources"))))))

(define-public crate-libaom-0.3.0 (c (n "libaom") (v "0.3.0") (d (list (d (n "aom-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "av-codec") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "av-data") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)))) (h "0qm604drsn5dih701077xds7jw4scl7lysck3y4knqc7xnbgssjc") (f (quote (("default" "codec-trait") ("codec-trait" "av-codec") ("build" "aom-sys/build_sources"))))))

(define-public crate-libaom-0.3.1 (c (n "libaom") (v "0.3.1") (d (list (d (n "aom-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "av-codec") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "av-data") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)))) (h "1lfayr8kaikb12mhvn0ifarcdbrn37v00m88bsl4v8npxg03bah5") (f (quote (("default" "codec-trait") ("codec-trait" "av-codec") ("build" "aom-sys/build_sources"))))))

(define-public crate-libaom-0.3.2 (c (n "libaom") (v "0.3.2") (d (list (d (n "aom-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "av-codec") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "av-data") (r "^0.4.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 2)))) (h "0ixal2pspy5n6nq0f34zvv6v6xscxh8wnn04rx3gb6dpbaas2j1j") (f (quote (("default" "codec-trait") ("codec-trait" "av-codec") ("build" "aom-sys/build_sources"))))))

