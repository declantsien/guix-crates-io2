(define-module (crates-io li ba libalpm-fork) #:use-module (crates-io))

(define-public crate-libalpm-fork-0.1.3 (c (n "libalpm-fork") (v "0.1.3") (d (list (d (n "alpm-sys-fork") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "printf") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "034fk5bsq5bz65dwmljm4fqk5zlnic6d522gr7z1v8i95a0xb915")))

(define-public crate-libalpm-fork-0.1.4 (c (n "libalpm-fork") (v "0.1.4") (d (list (d (n "alpm-sys-fork") (r "^0.1.2") (d #t) (k 0)) (d (n "chrono") (r "^0.3") (d #t) (k 0)) (d (n "curl") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "printf") (r "^0.1") (d #t) (k 0)) (d (n "term") (r "^0.4") (d #t) (k 2)) (d (n "url") (r "^1.4") (d #t) (k 0)))) (h "06lgy726d9aj6xz3yqslaf0kdp0bhmya0bn6i1qg9i0q5n2bkgpx")))

