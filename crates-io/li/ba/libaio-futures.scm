(define-module (crates-io li ba libaio-futures) #:use-module (crates-io))

(define-public crate-libaio-futures-0.1.0 (c (n "libaio-futures") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "08kzk8ry6zicxms1dh890msiir9w8nkbpq7dl18vg2yqqdhmkfwm")))

(define-public crate-libaio-futures-0.1.1 (c (n "libaio-futures") (v "0.1.1") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "0frylmmi4n5bbf7xxcsa61wsknkily7wc20v2mlciraqi7mdd784")))

(define-public crate-libaio-futures-0.1.2 (c (n "libaio-futures") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "1vkfl2hk667x02626mk8bngbqjbagmj7sclkr6az86da0xg9acg8")))

(define-public crate-libaio-futures-0.1.3 (c (n "libaio-futures") (v "0.1.3") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "0k3gf1sydbxaa0h6qskgzy1wkhr851296chqb5w9z1wcps015m1v")))

(define-public crate-libaio-futures-0.1.4 (c (n "libaio-futures") (v "0.1.4") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "0qy53x4bcfn6qz82jz1ndjlhp628lbjrsidsckdgww93qhbsndg4")))

(define-public crate-libaio-futures-0.1.5 (c (n "libaio-futures") (v "0.1.5") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "0f28i61b9gq9mkaf7nwhy831yjpmpi6x3c0knp3fa4r06zj93cdr")))

(define-public crate-libaio-futures-0.1.6 (c (n "libaio-futures") (v "0.1.6") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "0p7sy8wx374yspmhg4hql58z3qkkhjiwy471xr4pbqd05czw5bqh")))

(define-public crate-libaio-futures-0.1.7 (c (n "libaio-futures") (v "0.1.7") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "futures") (r "^0.3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10.2") (d #t) (k 0)))) (h "05ban7ikangmi7ywvzfdn605apx3mwq4c565wjb8464ks1i9f1hd")))

(define-public crate-libaio-futures-0.1.8 (c (n "libaio-futures") (v "0.1.8") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 2)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "0fa19pcz9idy8h8li2wg6xxzr4zbqq8303wxl1azvzrv0jkrxziz") (f (quote (("emulated-failure"))))))

(define-public crate-libaio-futures-0.1.9 (c (n "libaio-futures") (v "0.1.9") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 2)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "11pcq2k17fkp0dw5kbzi9y1zmdnr3n3d7qfk2ar3srq51nbjmzdy") (f (quote (("emulated-failure"))))))

(define-public crate-libaio-futures-0.2.0 (c (n "libaio-futures") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 2)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "1q1g9p759m2cmfi7nadw9nyinbpvw0aa98jjm1d9w2m28f1405h7") (f (quote (("emulated-failure"))))))

(define-public crate-libaio-futures-0.2.1 (c (n "libaio-futures") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 2)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "18w138a6m1qn8fhcnz2wnv5411s3nyqfvah5813h00agkaa8a79f") (f (quote (("emulated-failure"))))))

(define-public crate-libaio-futures-0.2.2 (c (n "libaio-futures") (v "0.2.2") (d (list (d (n "crossbeam-channel") (r "^0.5.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.8") (d #t) (k 2)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11.1") (d #t) (k 0)))) (h "0w9jmcikm7c2qxj2f1mfa49a70pc35zrqd5ixrpvyylfnb9inri3") (f (quote (("emulated-failure"))))))

(define-public crate-libaio-futures-0.2.3 (c (n "libaio-futures") (v "0.2.3") (d (list (d (n "crossbeam-channel") (r "^0.5.6") (d #t) (k 0)) (d (n "futures") (r "^0.3.24") (d #t) (k 2)) (d (n "libc") (r "^0.2.133") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0mfj1hn4sw94ld6rw4ry39h0k9l7i9jciiw8vk7jglxgl4jqnr6q") (f (quote (("emulated-failure"))))))

