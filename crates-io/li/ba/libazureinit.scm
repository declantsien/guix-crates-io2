(define-module (crates-io li ba libazureinit) #:use-module (crates-io))

(define-public crate-libazureinit-0.1.1 (c (n "libazureinit") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.146") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (f (quote ("blocking" "json"))) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde-xml-rs") (r "^0.6.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_xml") (r "^0.9.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.13") (d #t) (k 0)))) (h "1fkqbh6xvhq3yr6z13paxq9b62h0mkv7h4ypkkxjx7d9f5wmfbjx")))

