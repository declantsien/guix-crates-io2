(define-module (crates-io li ba liba) #:use-module (crates-io))

(define-public crate-liba-0.1.0 (c (n "liba") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (d #t) (k 0)))) (h "02nkhm8vrnmnhl0x47adv89hd61g7150a7hqk5zx867dwl82gfyp")))

(define-public crate-liba-0.1.0-rc2 (c (n "liba") (v "0.1.0-rc2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p3fhr8q7q3lp8q9gfd4yrichdn45jvq68vmjn6g9z6wrxavpd4p") (y #t)))

(define-public crate-liba-0.1.1-rc0 (c (n "liba") (v "0.1.1-rc0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ki2prngsp7afnlnjvcyi8lsqsrqlb8ykr1g6zl4kq5h86x496v0") (y #t)))

(define-public crate-liba-0.1.1-rc1 (c (n "liba") (v "0.1.1-rc1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qmrgzkcfwdlbydch6igxr4ld4zbp8pdbp4dmxvwd5k9g2xz13si") (y #t)))

(define-public crate-liba-0.1.1-rc2 (c (n "liba") (v "0.1.1-rc2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bb909kdz89pds0i20pn3k2hid7sy35jjv64l3bvyfydlvs8w4aw") (y #t)))

(define-public crate-liba-0.1.1-rc3 (c (n "liba") (v "0.1.1-rc3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a591735y1hp4y4gwxspkp9lkl8x12ryp4rj0pv61b5i13dyzlyr") (y #t)))

(define-public crate-liba-0.1.1-rc4 (c (n "liba") (v "0.1.1-rc4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f9y6sxxr6ac20f0wiqs7mjapcsbg5j41dx87fd0m7iy1ab34v4r") (y #t)))

(define-public crate-liba-0.1.1-rc5 (c (n "liba") (v "0.1.1-rc5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1v5yjslm5n30131j815lv51fz0029rjmm4p7sib26r3bcldvdxa3") (f (quote (("float")))) (y #t)))

(define-public crate-liba-0.1.1-rc6 (c (n "liba") (v "0.1.1-rc6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "09w5vfj764g9pb0hmxg0265vv6mffilwxg3760mmklb2f9xkaa27") (f (quote (("float")))) (y #t)))

(define-public crate-liba-0.1.1-rc7 (c (n "liba") (v "0.1.1-rc7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1yb07qrpmb26z7xb8f8dxc58plzq09cyv3slin3p6271ayxjqx28") (f (quote (("std") ("float")))) (y #t)))

(define-public crate-liba-0.1.1-rc8 (c (n "liba") (v "0.1.1-rc8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "06pc718q69zqfavj90zas6p8vw3rpn1b1cb29z8s31gasb23s5l2") (f (quote (("std") ("float")))) (y #t)))

(define-public crate-liba-0.1.1-rc9 (c (n "liba") (v "0.1.1-rc9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0vp9vxbh92804jh8id45hg3rzam1xcb9p2arwwq5ihyfpzmrffvc") (f (quote (("std") ("float")))) (y #t)))

(define-public crate-liba-0.1.1 (c (n "liba") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1ycfghsvqfcn3zm0zjjg14chgy47pp1fk8nf70qpdpzq979p4pgq") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.2-rc0 (c (n "liba") (v "0.1.2-rc0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1aifsff01xr9sxbhw49qcqhnylcpn3pc0y4qi01v3qgl1yz4kyfk") (f (quote (("std") ("float")))) (y #t)))

(define-public crate-liba-0.1.2 (c (n "liba") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "131gjahkwq4yi1d6wgxd449i8yjqmrad8pzi1n3j30yyb95a8n6p") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.3 (c (n "liba") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0rpmxx5zsj3d3zs6nrq5965wkdgv4nb21v6ps52l131wc6n4nd4g") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.4-rc.0 (c (n "liba") (v "0.1.4-rc.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1dsqwk1kprjlxdy3zs6lgm77fwv4aybyx27xd8qdnl0g23l7npq9") (f (quote (("std") ("float")))) (y #t)))

(define-public crate-liba-0.1.4-rc.1 (c (n "liba") (v "0.1.4-rc.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0fmsm48cf080iqbm5r2awh0a5h5sc6kxdmaqpx77xsrmvdim4slz") (f (quote (("std") ("float")))) (y #t)))

(define-public crate-liba-0.1.4 (c (n "liba") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "17i71s2cc72wil214ipwa4f0nvp47lx2xjhzqy72qrcbrwky0qh6") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.5-rc.0 (c (n "liba") (v "0.1.5-rc.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1wq425ni6fq40wr1niq4p9r46d6wc4jwg7qnvqc0b6h1jn8myf4w") (f (quote (("std") ("float")))) (y #t)))

(define-public crate-liba-0.1.4-0 (c (n "liba") (v "0.1.4-0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0cqyijbr51z3ij3wnyr4n35wwdqb4jsr1adlfy15j1iwcm65md9s") (f (quote (("std") ("float")))) (y #t)))

(define-public crate-liba-0.1.5 (c (n "liba") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "15ak8rzdr6ll719zipzybgp16rjkrfw074k7ic4nka4rrgb7l0m5") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.6 (c (n "liba") (v "0.1.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0vnhx92zfl537vi5p53l2h91740axb9sfc9h6zjcrpnn9jijrjfm") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.7-a (c (n "liba") (v "0.1.7-a") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0mgbh5igv3sk5ch7nccycaqvvyjkc69wan3cjcqydrijqbsm0pqp") (f (quote (("std") ("float")))) (y #t)))

(define-public crate-liba-0.1.7-b.0 (c (n "liba") (v "0.1.7-b.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "05hhp2bmrpgd75lp32c6k8ql1iim1avfj6drsyj1khyrl1027j88") (f (quote (("std") ("float")))) (y #t)))

(define-public crate-liba-0.1.7 (c (n "liba") (v "0.1.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0k6d3il8w1l90qm8grsq4m1xsdpkn8g1yaaa0isacddymg4k1q72") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.8 (c (n "liba") (v "0.1.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1mcyc3y19vsa68hg3lgcnh32jaba6qb0c1z9lpl7fpkvcsyvkv9i") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.9 (c (n "liba") (v "0.1.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0ij7hcr1x7qnnmw6jhb3lpggvj3k5v2n8clc3cdjln71h8p31yss") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.10 (c (n "liba") (v "0.1.10") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0p1x118qdinnwan89ml3ysjs04whcqx8cx8v9czj6lglh9fw3b32") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.11 (c (n "liba") (v "0.1.11") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1156d57s3awbqymyhb9ykxj7bd7gp8ndsb3gd6j423ylbq6gvxk2") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.12 (c (n "liba") (v "0.1.12") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1nr7hjl00r429l9qnhzf1xihiy397srbchf87bvgr92dpgxk6n6x") (f (quote (("std") ("float"))))))

(define-public crate-liba-0.1.13 (c (n "liba") (v "0.1.13") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)))) (h "041g0569hakx8lw27gr00mpx0b4q3i1za986rx9wvvwia662ya9d") (f (quote (("std") ("static_crt") ("float"))))))

