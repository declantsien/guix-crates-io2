(define-module (crates-io li ba libayatana-appindicator) #:use-module (crates-io))

(define-public crate-libayatana-appindicator-0.1.0 (c (n "libayatana-appindicator") (v "0.1.0") (d (list (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gtk") (r "^0.9.2") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "libayatana-appindicator-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1s7rnpd17fag5wsij08awxfcdj27zydd34bwcq2f2gkjsd7l0npq")))

(define-public crate-libayatana-appindicator-0.1.1 (c (n "libayatana-appindicator") (v "0.1.1") (d (list (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gtk") (r "^0.9.2") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "libayatana-appindicator-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1md38jg4fr6m0jqqb4xd7s2dx6c4vjm5a1cwpdwp72qqjqp9zz5z")))

(define-public crate-libayatana-appindicator-0.1.3 (c (n "libayatana-appindicator") (v "0.1.3") (d (list (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gtk") (r "^0.9.2") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "libayatana-appindicator-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "086wr44ihgkgwn82hwry6ag4q8pvghxmwxpazasg68mbmisflp1c")))

(define-public crate-libayatana-appindicator-0.1.4 (c (n "libayatana-appindicator") (v "0.1.4") (d (list (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gtk") (r "^0.14.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.14.0") (d #t) (k 0)) (d (n "libayatana-appindicator-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0vxq0ydj02jgrwsnsix1911901skisyjvd71h2g5zw75lyk7p4v5")))

(define-public crate-libayatana-appindicator-0.1.6 (c (n "libayatana-appindicator") (v "0.1.6") (d (list (d (n "glib") (r "^0.10.3") (d #t) (k 0)) (d (n "gtk") (r "^0.14.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.14.0") (d #t) (k 0)) (d (n "libayatana-appindicator-sys") (r "^0.1.6") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "00zk5gkli8jqz25af0la47q2aa8y5l8nxaf7ha6iah0a65xpxdi1") (f (quote (("dynamic" "libayatana-appindicator-sys"))))))

(define-public crate-libayatana-appindicator-0.2.0 (c (n "libayatana-appindicator") (v "0.2.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "gtk") (r "^0.15.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.15.0") (d #t) (k 0)) (d (n "libayatana-appindicator-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1i4cq8chijlpvwnsbv63x74hgpnwq57v3qyvnhkwnirr68md4y83") (f (quote (("dynamic" "libayatana-appindicator-sys"))))))

