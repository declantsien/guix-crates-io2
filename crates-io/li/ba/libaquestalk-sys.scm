(define-module (crates-io li ba libaquestalk-sys) #:use-module (crates-io))

(define-public crate-libaquestalk-sys-0.1.0 (c (n "libaquestalk-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 0)))) (h "1ih2ipvmw14rdnaw2hvbxx72xd4wxn0n1mdp2w9v4dbd5kf02slx")))

(define-public crate-libaquestalk-sys-0.1.1 (c (n "libaquestalk-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 0)))) (h "0fwqv1c8f6xdc0azva61xka6x986gvpam48bsqd8iw5j4mzbprmf")))

(define-public crate-libaquestalk-sys-0.1.3 (c (n "libaquestalk-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 0)))) (h "1l0b35hy6b5l57h2dlywdhisp2lcgadypkqx8n4wpmm8lm3hilqi")))

(define-public crate-libaquestalk-sys-0.1.4 (c (n "libaquestalk-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.138") (d #t) (k 0)))) (h "0rdrpb8325asch8q1973x1fvp0gqa5cyqi5gpxkd38dv0w1f7yxa")))

