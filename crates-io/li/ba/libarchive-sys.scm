(define-module (crates-io li ba libarchive-sys) #:use-module (crates-io))

(define-public crate-libarchive-sys-0.0.1 (c (n "libarchive-sys") (v "0.0.1") (h "0dl61qwxbgpdswwwj1m7ajkgzp0qmrxr235ihc9i5gkh8nimgbrd")))

(define-public crate-libarchive-sys-0.0.2 (c (n "libarchive-sys") (v "0.0.2") (d (list (d (n "pkg-config") (r "^0.3.3") (d #t) (k 1)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1a2mmgp3077p251bib209rhnn4n3p21q73z44v3by99iqfvrvfhj")))

