(define-module (crates-io li ba libafl_libfuzzer) #:use-module (crates-io))

(define-public crate-libafl_libfuzzer-0.11.0 (c (n "libafl_libfuzzer") (v "0.11.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.7") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "0zfgm9cwzf58x5pl1b4g8x1kqvl423zrfv6rcspm1yiw3hh50hk3") (f (quote (("introspection") ("arbitrary-derive" "libfuzzer-sys/arbitrary-derive"))))))

(define-public crate-libafl_libfuzzer-0.11.1 (c (n "libafl_libfuzzer") (v "0.11.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.7") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "0qhz3qv634586rjajz4apprqacdzz7i573hnrrd81pzhf9kmy23d") (f (quote (("introspection") ("arbitrary-derive" "libfuzzer-sys/arbitrary-derive"))))))

(define-public crate-libafl_libfuzzer-0.12.0 (c (n "libafl_libfuzzer") (v "0.12.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "document-features") (r "^0.2") (d #t) (k 0)) (d (n "libfuzzer-sys") (r "^0.4.7") (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 1)))) (h "09q99hzzjlzyq9jkbns3fa8q0dlmzw69bimxvqbhpigw3va2ykrp") (f (quote (("rabbit") ("introspection") ("embed-runtime") ("arbitrary-derive" "libfuzzer-sys/arbitrary-derive"))))))

