(define-module (crates-io li ba libaaarg) #:use-module (crates-io))

(define-public crate-libaaarg-0.4.0 (c (n "libaaarg") (v "0.4.0") (d (list (d (n "hound") (r "^3.4.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rodio") (r "^0.15") (d #t) (k 0)))) (h "1ckz079xvpjv2jrxiica28k1fnfjp75m2l04iz95w9bn35cgvc9q")))

