(define-module (crates-io li ba libappindicator) #:use-module (crates-io))

(define-public crate-libappindicator-0.1.0 (c (n "libappindicator") (v "0.1.0") (d (list (d (n "glib") (r "^0.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.3") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "07pzrh7i77sylhk34j89yh3qsrpjm3hzafnwy77jq3z7r5mba7zy")))

(define-public crate-libappindicator-0.2.0 (c (n "libappindicator") (v "0.2.0") (d (list (d (n "glib") (r "^0.1") (d #t) (k 0)) (d (n "gtk") (r "^0.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.3") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0ddvy9vzsnp1y0kvl123gpn205kkywhp132q38z86gn1gzskk101")))

(define-public crate-libappindicator-0.3.0 (c (n "libappindicator") (v "0.3.0") (d (list (d (n "glib") (r "^0.3") (d #t) (k 0)) (d (n "gtk") (r "^0.2") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.4") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "16xphdzr9399pwz45qprx0pmynvaz1z1hjyiz64d4spzlpc04c0n")))

(define-public crate-libappindicator-0.4.0 (c (n "libappindicator") (v "0.4.0") (d (list (d (n "glib") (r "^0.5.0") (d #t) (k 0)) (d (n "gtk") (r "^0.4.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 0)))) (h "1h06bvaq0skc1pm06s8949vn44p8m73jkp4j90xhrpmw0wzq0aa2")))

(define-public crate-libappindicator-0.5.0 (c (n "libappindicator") (v "0.5.0") (d (list (d (n "glib") (r "^0.9.1") (d #t) (k 0)) (d (n "gtk") (r "^0.8.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.9.2") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "00ifxys8kn15fqnl1n52g9ac51vihm49xsywsklx9aai4fyakbjb")))

(define-public crate-libappindicator-0.5.1 (c (n "libappindicator") (v "0.5.1") (d (list (d (n "glib") (r "^0.9.2") (d #t) (k 0)) (d (n "gtk") (r "^0.8.1") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.9.2") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0na0628biyfbdsayi2kqwdfwdhvgmc0h7b9ijlapqyq0al6waccg")))

(define-public crate-libappindicator-0.5.2 (c (n "libappindicator") (v "0.5.2") (d (list (d (n "glib") (r "^0.10.1") (d #t) (k 0)) (d (n "gtk") (r "^0.9.0") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "0dyhb5gh2srhaq8v6nrcds4qsqibkpgkh53lypxf4l1z6mbzrlsj")))

(define-public crate-libappindicator-0.6.0 (c (n "libappindicator") (v "0.6.0") (d (list (d (n "glib") (r "^0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.14") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.14") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bmdihywd2h5jmha6dkr4r94lccvg0lzy9q3qsjhg506rxj3rbqy")))

(define-public crate-libappindicator-0.6.1 (c (n "libappindicator") (v "0.6.1") (d (list (d (n "glib") (r "^0.14") (d #t) (k 0)) (d (n "gtk") (r "^0.14") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.14") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1vmp91549lnpjwbg7p6wslmrq54ma9hb1vvvxdyk71hpy6f8p4l4")))

(define-public crate-libappindicator-0.7.0 (c (n "libappindicator") (v "0.7.0") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "gtk") (r "^0.15") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.15") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1pnpc5n9c75xzsmv2j67n80v18grkvd7apvj0qyrzmc06amrzclp")))

(define-public crate-libappindicator-0.7.1 (c (n "libappindicator") (v "0.7.1") (d (list (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "gtk") (r "^0.15") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.15") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1s279f9g3xw7a6a8240c46v4qsl59ia9wv1hrcj4haq9dnwkqbfv") (f (quote (("default" "backcompat") ("backcompat" "libappindicator-sys/backcompat"))))))

(define-public crate-libappindicator-0.8.0 (c (n "libappindicator") (v "0.8.0") (d (list (d (n "glib") (r "^0.16") (d #t) (k 0)) (d (n "gtk") (r "^0.16") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.16") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0bqs8p5fpdf0h0yyjvr5fj8pg767fx5znvbc61c371dhr7yyvqc9") (f (quote (("default" "backcompat") ("backcompat" "libappindicator-sys/backcompat"))))))

(define-public crate-libappindicator-0.9.0 (c (n "libappindicator") (v "0.9.0") (d (list (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "gtk") (r "^0.18") (d #t) (k 0)) (d (n "gtk-sys") (r "^0.18") (d #t) (k 0)) (d (n "libappindicator-sys") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "02nwjmm5qqbkvzbz4j1dd50xs0ywr0i2l2scwmxcqs680yb9nn03") (f (quote (("default" "backcompat") ("backcompat" "libappindicator-sys/backcompat"))))))

