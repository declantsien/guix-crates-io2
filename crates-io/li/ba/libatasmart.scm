(define-module (crates-io li ba libatasmart) #:use-module (crates-io))

(define-public crate-libatasmart-0.1.0 (c (n "libatasmart") (v "0.1.0") (d (list (d (n "libatasmart-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "0wkmhn39hxr1nyjhmlwpqap2q80llqkc6i9v0x5skihhqqfghw4n")))

(define-public crate-libatasmart-0.1.2 (c (n "libatasmart") (v "0.1.2") (d (list (d (n "libatasmart-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "nix") (r "^0.4.2") (d #t) (k 0)))) (h "1m0iw19cs94sxzhw3qqfv0kbjlbfsk5jc4yxnyqwnl0imnpmb60j")))

(define-public crate-libatasmart-0.1.3 (c (n "libatasmart") (v "0.1.3") (d (list (d (n "libatasmart-sys") (r "~0.1") (d #t) (k 0)) (d (n "nix") (r "~0.8") (d #t) (k 0)))) (h "09sj5r7k944d77x024h3145b8gzpkw05bnvhc6qln594nnwirwkk")))

(define-public crate-libatasmart-0.1.4 (c (n "libatasmart") (v "0.1.4") (d (list (d (n "libatasmart-sys") (r "~0.1") (d #t) (k 0)) (d (n "nix") (r "~0.8") (d #t) (k 0)))) (h "1nypvizb60rwkr9k06lzfh16klhw2idmr0rixnpfypzv0grj1f3m")))

(define-public crate-libatasmart-0.1.5 (c (n "libatasmart") (v "0.1.5") (d (list (d (n "libatasmart-sys") (r "~0.1") (d #t) (k 0)) (d (n "nix") (r "~0.8") (d #t) (k 0)))) (h "0sysmkak3rg741vz0ppdjrszak6iwz705rk21557b3qvqnhw5hg4")))

(define-public crate-libatasmart-0.1.6 (c (n "libatasmart") (v "0.1.6") (d (list (d (n "libatasmart-sys") (r "~0.1") (d #t) (k 0)) (d (n "nix") (r "~0.8") (d #t) (k 0)))) (h "1yax5ygbd231yd91rxakzrq26cpsbvps81mmizrfpqid03xqx9j4")))

(define-public crate-libatasmart-0.1.7 (c (n "libatasmart") (v "0.1.7") (d (list (d (n "libatasmart-sys") (r "~0.1") (d #t) (k 0)) (d (n "nix") (r "~0.8") (d #t) (k 0)))) (h "12rp4gyyj33pav271cz02kq9bks8bqx9d25hl8ks3i3109b02a3c")))

(define-public crate-libatasmart-0.2.0 (c (n "libatasmart") (v "0.2.0") (d (list (d (n "derive-error") (r "^0.0") (d #t) (k 0)) (d (n "libatasmart-sys") (r "~0.1") (d #t) (k 0)) (d (n "nix") (r "~0.8") (d #t) (k 0)))) (h "0y5kdabwc0b4f2jslb1b79h1vfchsn3nndzr24qg7ciayqrbcr4s")))

(define-public crate-libatasmart-0.3.0 (c (n "libatasmart") (v "0.3.0") (d (list (d (n "derive-error") (r "^0.0") (d #t) (k 0)) (d (n "libatasmart-sys") (r "~0.1") (d #t) (k 0)) (d (n "nix") (r "~0.17") (d #t) (k 0)))) (h "1wi1lszs1x374vygf6lzpcz65br8d25vi6jj4nhibjj85pwhf5ab")))

(define-public crate-libatasmart-0.3.1 (c (n "libatasmart") (v "0.3.1") (d (list (d (n "libatasmart-sys") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.26") (d #t) (k 0)))) (h "115jzlw1c4j0azbyh6dz0wivkbn6ahyamrzqyq6sxc59vq8y0kr4")))

