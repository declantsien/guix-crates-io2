(define-module (crates-io li ba libav-sys) #:use-module (crates-io))

(define-public crate-libav-sys-0.1.0 (c (n "libav-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "1yks1p55qi99773n868jyc0lms1dl8hiyi39z1cki44g5pyn728m")))

(define-public crate-libav-sys-0.2.0 (c (n "libav-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "doxygen-rs") (r "^0.3.1") (d #t) (k 1)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1daxg2fwgid9nc7sjyxhlcbx56v421dlg1w0ipp81qn6ak4mk3bw")))

