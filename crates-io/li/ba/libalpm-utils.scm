(define-module (crates-io li ba libalpm-utils) #:use-module (crates-io))

(define-public crate-libalpm-utils-0.0.1 (c (n "libalpm-utils") (v "0.0.1") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "133r95j8sn4j8azf8bynng1lvp5qywblyq39v2ax9g56f8pirs43")))

(define-public crate-libalpm-utils-0.0.2 (c (n "libalpm-utils") (v "0.0.2") (d (list (d (n "nom") (r "^2.0") (d #t) (k 0)))) (h "1fyffjsdsnx4b5226vqy5ihsrjwzjb1ak9d74r6ikb3ff4kc4kbm")))

