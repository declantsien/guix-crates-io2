(define-module (crates-io li ba libary_test) #:use-module (crates-io))

(define-public crate-libary_test-0.1.0 (c (n "libary_test") (v "0.1.0") (h "0bkah24fac63w5hk58nmnvxdn2c8i8cninx9gjar4nn5zwf7887m") (y #t)))

(define-public crate-libary_test-0.1.1 (c (n "libary_test") (v "0.1.1") (h "0b7spmk2qcspf7am0nx7v05rifab62kvad8afh9qq29ac30ggb2j") (y #t)))

(define-public crate-libary_test-0.1.2 (c (n "libary_test") (v "0.1.2") (h "0a5cafz4c6l2lz2x0vnkbsy918raf96zapd19c29zgvr59iilwkm") (y #t)))

