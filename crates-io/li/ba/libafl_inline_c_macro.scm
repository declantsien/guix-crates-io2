(define-module (crates-io li ba libafl_inline_c_macro) #:use-module (crates-io))

(define-public crate-libafl_inline_c_macro-0.1.6 (c (n "libafl_inline_c_macro") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1.0.78") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "1l7b8v5ax6i3jrpdijbys8nmypz84yq1rnm7088ws9yicjqfr95g")))

