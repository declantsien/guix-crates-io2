(define-module (crates-io li ba libaes) #:use-module (crates-io))

(define-public crate-libaes-0.1.0 (c (n "libaes") (v "0.1.0") (h "0mp2d4ldkg13i5nmpzlvz92haiga2j4gk5zyykknnsj1ygh5zn0h")))

(define-public crate-libaes-0.1.1 (c (n "libaes") (v "0.1.1") (h "1lf3kif1w8lsf2xssxfd2zzw87j8qgj844gy0qfgvpz9ybpcbsqy")))

(define-public crate-libaes-0.1.2 (c (n "libaes") (v "0.1.2") (h "1h3bh4cq6dwhix0ffibyi9ccxjx6xz564x600h2njj6hyy53x6qy")))

(define-public crate-libaes-0.2.0 (c (n "libaes") (v "0.2.0") (h "14lkazf9xhw3x3xzdph6f9d8k4m6bjnsjd3n98nc2hpfxxf8gdpb")))

(define-public crate-libaes-0.3.0 (c (n "libaes") (v "0.3.0") (h "0xgprbkhhdg5iz8gmvgg2x426vrcb4nnw3riy173yy5l36rxi3gq")))

(define-public crate-libaes-0.4.0 (c (n "libaes") (v "0.4.0") (h "1axmpi3x2zc3d6zfhyd1hc8rgzib59sxbqfd2kpnf1bzj1abw0zc")))

(define-public crate-libaes-0.5.0 (c (n "libaes") (v "0.5.0") (h "1pgcmkq327mkq70kzgkygn0n6hi7xpxjy2grbbisqds8cn494hvb") (y #t)))

(define-public crate-libaes-0.6.0 (c (n "libaes") (v "0.6.0") (h "1q660qdkyqbfk1zsv6c464h3azpgawdp0wfbijfmhbwip2pxhsdb")))

(define-public crate-libaes-0.6.1 (c (n "libaes") (v "0.6.1") (h "01mlw6mabshp6z9vm5d7ivxpj4z7vic6m809p9vns0zwmydlz2s1")))

(define-public crate-libaes-0.6.2 (c (n "libaes") (v "0.6.2") (h "1f627kjsdigj7ghfh65c47dd039spxly4aaqzci80i5lblxlcz6v")))

(define-public crate-libaes-0.6.3 (c (n "libaes") (v "0.6.3") (h "1w02w33lmdsaflvx821l2766ga2daz3scz3wyk74nq4f0zh6zfsi")))

(define-public crate-libaes-0.6.4 (c (n "libaes") (v "0.6.4") (h "0nzindqbp6gaahyq407hal99vgj690p1y3p02s0g4zvp3ca4w7xv")))

(define-public crate-libaes-0.6.5 (c (n "libaes") (v "0.6.5") (h "0mm6p121c2vhzhrmsaakrd6nwm02a05jpqp0v1mhmi6n63dhjil8")))

(define-public crate-libaes-0.7.0 (c (n "libaes") (v "0.7.0") (h "1drvgng5hpjwnmahdwn3w8g30z62b20vdabjmgsidf09q1h37442")))

