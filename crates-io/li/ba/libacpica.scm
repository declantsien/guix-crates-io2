(define-module (crates-io li ba libacpica) #:use-module (crates-io))

(define-public crate-libacpica-0.0.5 (c (n "libacpica") (v "0.0.5") (h "19wqmf9by9hm57hlr0wgyyk4c8lm6a17a9lp021g385ns333vpdq")))

(define-public crate-libacpica-0.0.6 (c (n "libacpica") (v "0.0.6") (h "07kcd1k3s181a8y0zxzjlm1n10axx8r289411rwp6574w9p0gfdc")))

(define-public crate-libacpica-0.0.7 (c (n "libacpica") (v "0.0.7") (h "1sbzp9497i7mjsg7inv45480nwzrq6gz7vj30bvz981wccwcr1n6")))

(define-public crate-libacpica-0.0.8 (c (n "libacpica") (v "0.0.8") (h "1f2mdd74x042nir6gz84bx114ii2wnib7b0xlz1dys7idh89c1km")))

