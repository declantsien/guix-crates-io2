(define-module (crates-io li ba libappstream) #:use-module (crates-io))

(define-public crate-libappstream-0.1.0 (c (n "libappstream") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "libappstream-sys")) (d (n "gio") (r "^0.19") (d #t) (k 0)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.12") (d #t) (k 0)))) (h "03jg71g1lfs7czs9j917q9x6qbk20xhkw73f0c4ydxqzhx71wxpp")))

