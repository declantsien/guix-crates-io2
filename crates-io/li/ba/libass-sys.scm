(define-module (crates-io li ba libass-sys) #:use-module (crates-io))

(define-public crate-libass-sys-0.1.0 (c (n "libass-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1y6rzq95lz26vhg2zjcxwvnhpl4gb78b9hv66w3a8629cvjx8ilj")))

(define-public crate-libass-sys-0.1.1 (c (n "libass-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)))) (h "1z5298gnhg4wjssilm2swmnfr3gbzsfl9jffbxvszg3gyww3rnfw")))

(define-public crate-libass-sys-0.1.2 (c (n "libass-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "metadeps") (r "^1.1") (d #t) (k 1)))) (h "0afdb8mlf1j30chg3wg221xf3nj8hypwd9rpk99sn09lwjwg0f8k")))

