(define-module (crates-io li ut liutongshuo_decoding_macros) #:use-module (crates-io))

(define-public crate-liutongshuo_decoding_macros-0.1.0 (c (n "liutongshuo_decoding_macros") (v "0.1.0") (d (list (d (n "liutongshuo_decoding_macros_impl") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)))) (h "0dl0ygk9f0zm11qdswcnns03627ww57n96wb0v4nqg7ykp55mb69")))

(define-public crate-liutongshuo_decoding_macros-0.0.1 (c (n "liutongshuo_decoding_macros") (v "0.0.1") (d (list (d (n "liutongshuo_decoding_macros_impl") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.4.0") (d #t) (k 0)))) (h "05a4wxgjh4d3yk2ak9iaw8q86kk2x73c894806dlakgh85xz3w3p")))

