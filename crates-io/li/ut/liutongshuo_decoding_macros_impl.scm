(define-module (crates-io li ut liutongshuo_decoding_macros_impl) #:use-module (crates-io))

(define-public crate-liutongshuo_decoding_macros_impl-0.0.1 (c (n "liutongshuo_decoding_macros_impl") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "02f6c10vmavvaqb2z5q8ycpl4zqwpv0raag3a3ciyg91w6j34y0f")))

(define-public crate-liutongshuo_decoding_macros_impl-0.0.2 (c (n "liutongshuo_decoding_macros_impl") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "1dniniwjp6318b3zdplr85ail4r98yh62yk92bpm2mi8wss0va3k")))

(define-public crate-liutongshuo_decoding_macros_impl-0.0.3 (c (n "liutongshuo_decoding_macros_impl") (v "0.0.3") (d (list (d (n "proc-macro-hack") (r "^0.4") (d #t) (k 0)))) (h "0zi7288nq3a586b3vfp7gqp7c62d51bb5fbzk5bbr5zkx280alqd")))

