(define-module (crates-io li gh light-bounded-vec) #:use-module (crates-io))

(define-public crate-light-bounded-vec-0.1.1 (c (n "light-bounded-vec") (v "0.1.1") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.18.11") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09mlrya4yr2j5fsikh3g5nmr9v1py55r0rfb69fkxqsqzckx4pkw") (f (quote (("solana" "solana-program"))))))

(define-public crate-light-bounded-vec-0.1.4 (c (n "light-bounded-vec") (v "0.1.4") (d (list (d (n "bytemuck") (r "^1.14") (f (quote ("min_const_generics"))) (d #t) (k 0)) (d (n "solana-program") (r "^1.18.11") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1si9jnzdrdrqa0fhh6g1vsxxbvgpblpbnzw4rwzpjn8178jnv5wg") (f (quote (("solana" "solana-program"))))))

