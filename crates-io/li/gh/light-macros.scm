(define-module (crates-io li gh light-macros) #:use-module (crates-io))

(define-public crate-light-macros-0.1.0 (c (n "light-macros") (v "0.1.0") (d (list (d (n "bs58") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1mg1nn5pw71gx4pl61sf2li1n5wy00cdxrg737p7q5dhy4npjcph")))

(define-public crate-light-macros-0.3.0 (c (n "light-macros") (v "0.3.0") (d (list (d (n "anchor-lang") (r "^0.28") (d #t) (k 2)) (d (n "anchor-spl") (r "^0.28") (d #t) (k 2)) (d (n "bs58") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "07bhii5vsf59qvskj0xqxbpam2nfjyvn23k9spdlix5w6ixy703y")))

(define-public crate-light-macros-0.3.1 (c (n "light-macros") (v "0.3.1") (d (list (d (n "anchor-lang") (r "^0.28") (d #t) (k 2)) (d (n "anchor-spl") (r "^0.28") (d #t) (k 2)) (d (n "bs58") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0pdn0jswm5w8xd5sarm5chg2c3j1j4q6fm77id318sdvxlhf2avq")))

(define-public crate-light-macros-0.3.2 (c (n "light-macros") (v "0.3.2") (d (list (d (n "anchor-lang") (r "^0.29") (d #t) (k 2)) (d (n "anchor-spl") (r "^0.29") (d #t) (k 2)) (d (n "bs58") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0vqvrhzhkh10sigccf821v7n3p4z6ng12k9d4phv732w61v8ix1h")))

(define-public crate-light-macros-0.3.5 (c (n "light-macros") (v "0.3.5") (d (list (d (n "anchor-lang") (r "^0.29") (d #t) (k 2)) (d (n "anchor-spl") (r "^0.29") (d #t) (k 2)) (d (n "bs58") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "18mfgncikm83wbpzmnmg0jbj8adhc1cm6myijjxyvxjhngvm9vrz")))

