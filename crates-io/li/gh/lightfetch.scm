(define-module (crates-io li gh lightfetch) #:use-module (crates-io))

(define-public crate-lightfetch-1.0.0 (c (n "lightfetch") (v "1.0.0") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.5") (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)) (d (n "wmi") (r "^0.9.2") (d #t) (k 0)))) (h "04xqkl4qk285p5xnksxq4gj5vn6a9di6s9wrlzk6vfiy672pgmhb")))

(define-public crate-lightfetch-1.0.1 (c (n "lightfetch") (v "1.0.1") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.23.5") (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)) (d (n "wmi") (r "^0.9.2") (d #t) (k 0)))) (h "182g9mdl47gnbjlgdqfnp93iyrszqnzq0b7nsk8xd8f8gxgjyr7a")))

(define-public crate-lightfetch-1.0.2 (c (n "lightfetch") (v "1.0.2") (d (list (d (n "num_cpus") (r "^1.13.1") (d #t) (k 0)) (d (n "sysinfo") (r "^0.24.7") (k 0)) (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.1") (d #t) (k 0)) (d (n "winit") (r "^0.26.1") (d #t) (k 0)) (d (n "wmi") (r "^0.11.0") (d #t) (k 0)))) (h "1a9p3vlq9mbn8308fnzaj6gxp4vz2igr582fdj507cs518hy893k")))

(define-public crate-lightfetch-1.0.3 (c (n "lightfetch") (v "1.0.3") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27.1") (k 0)) (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)) (d (n "wmi") (r "^0.11.4") (d #t) (k 0)))) (h "02i36f0cv4d2qf87m9n44mhrx7l5kphzq9p6jyhc5qbqpkn76njw")))

(define-public crate-lightfetch-1.0.4 (c (n "lightfetch") (v "1.0.4") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "static_vcruntime") (r "^2.0.0") (d #t) (k 1)) (d (n "sysinfo") (r "^0.27.1") (k 0)) (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)) (d (n "wmi") (r "^0.11.4") (d #t) (k 0)))) (h "0916j244v3gcpyjpahiajmxy6sd4pr6gs94kjiyjkdp35nz2b18j") (y #t)))

(define-public crate-lightfetch-1.0.5 (c (n "lightfetch") (v "1.0.5") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "static_vcruntime") (r "^2.0.0") (d #t) (k 1)) (d (n "sysinfo") (r "^0.27.1") (k 0)) (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)) (d (n "wmi") (r "^0.11.4") (d #t) (k 0)))) (h "18lwi624an4dy5kk8i5a8wm9gxvwwl1yzxvbixqwri4hmpf1n7gw")))

(define-public crate-lightfetch-1.0.6 (c (n "lightfetch") (v "1.0.6") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "static_vcruntime") (r "^2.0.0") (d #t) (k 1)) (d (n "sysinfo") (r "^0.27.1") (k 0)) (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)) (d (n "wmi") (r "^0.11.4") (d #t) (k 0)))) (h "1zxcxdwxqjrk6kp0nx0vppr4q1ha8fw79949fmvncwcwcs9pqiqz")))

(define-public crate-lightfetch-1.0.7 (c (n "lightfetch") (v "1.0.7") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "static_vcruntime") (r "^2.0.0") (d #t) (k 1)) (d (n "sysinfo") (r "^0.27.1") (k 0)) (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.2.3") (d #t) (k 0)) (d (n "winit") (r "^0.27.5") (d #t) (k 0)) (d (n "wmi") (r "^0.11.4") (d #t) (k 0)))) (h "1lv53bzkgj7h8rl3r1bpz8han20i6ka1bdfdcybxszxw8sxmdmnr")))

(define-public crate-lightfetch-1.0.8 (c (n "lightfetch") (v "1.0.8") (d (list (d (n "num_cpus") (r "^1.15.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.28.2") (k 0)) (d (n "tiny-ansi") (r "^0.1.0") (d #t) (k 0)) (d (n "whoami") (r "^1.3.0") (d #t) (k 0)) (d (n "winit") (r "^0.28.2") (d #t) (k 0)) (d (n "wmi") (r "^0.12.0") (d #t) (k 0)) (d (n "static_vcruntime") (r "^2.0.0") (d #t) (k 1)))) (h "0pi0zi2qq69z2vgj4vxzfafp7mlymqndg7yqmx9cg2ry8ic7mx6r")))

