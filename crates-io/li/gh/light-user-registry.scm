(define-module (crates-io li gh light-user-registry) #:use-module (crates-io))

(define-public crate-light-user-registry-0.3.0 (c (n "light-user-registry") (v "0.3.0") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.14") (d #t) (k 0)))) (h "1xbg8jqwymxh8cdpn5cpkp9fwcrh6wz5rqwzwgmphi53pfz0s0im") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

