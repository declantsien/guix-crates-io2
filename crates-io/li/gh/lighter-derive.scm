(define-module (crates-io li gh lighter-derive) #:use-module (crates-io))

(define-public crate-lighter-derive-0.1.0 (c (n "lighter-derive") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "04mfdysznpdqbxsbdrlgcnh9sj9b1kv2rv7lkw7bmqjwn83h9kiq") (f (quote (("std") ("default" "std"))))))

