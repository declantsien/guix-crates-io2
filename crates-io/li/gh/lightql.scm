(define-module (crates-io li gh lightql) #:use-module (crates-io))

(define-public crate-lightql-0.1.0 (c (n "lightql") (v "0.1.0") (d (list (d (n "mio_httpc") (r "^0.7.0") (f (quote ("native"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0arq61sm0ynj5xjwf7h0qmwgzyd55fwbf3s8pk8ak87sg29jkaml")))

(define-public crate-lightql-0.2.0 (c (n "lightql") (v "0.2.0") (d (list (d (n "mio_httpc") (r "^0.7.0") (f (quote ("native"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.80") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.33") (d #t) (k 0)))) (h "0warxfjslsc2j04yyhg3wma8l9g7ckg4sjz51xfcxik1zj8s8pdk")))

