(define-module (crates-io li gh light_pencil) #:use-module (crates-io))

(define-public crate-light_pencil-0.4.3 (c (n "light_pencil") (v "0.4.3") (d (list (d (n "formdata") (r "^0.12.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "lazycell") (r "^0.5.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.5") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "1s8c5yqis5cmyn00r9z3n5qvx3ajnhyp2vm1w7j33mv6hm3x9g2f")))

(define-public crate-light_pencil-0.4.4 (c (n "light_pencil") (v "0.4.4") (d (list (d (n "formdata") (r "^0.12.2") (d #t) (k 0)) (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "lazycell") (r "^0.5.1") (d #t) (k 0)) (d (n "mime") (r "^0.3.5") (d #t) (k 0)) (d (n "mime_guess") (r "^1.8.2") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "typemap") (r "^0.3.3") (d #t) (k 0)) (d (n "url") (r "^1.5.1") (d #t) (k 0)))) (h "1q24sv5r7pl2x2c4db7p1z2yqb7ynyrcm6nkxjrgfbwp6apxq4fl")))

