(define-module (crates-io li gh light_binary_merkle_tree) #:use-module (crates-io))

(define-public crate-light_binary_merkle_tree-0.1.0 (c (n "light_binary_merkle_tree") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.174") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.174") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "13z252a2l7qlps3s3966dn692s10pfy035ky5v4wn317bls8qmys")))

