(define-module (crates-io li gh light-merkle-tree) #:use-module (crates-io))

(define-public crate-light-merkle-tree-0.1.0 (c (n "light-merkle-tree") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.26") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)))) (h "012dw46d8z3dvymsrb2nqxy27567dy1cg10qfd9kk97fj9z1sdml") (f (quote (("solana"))))))

(define-public crate-light-merkle-tree-0.1.1 (c (n "light-merkle-tree") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "bytemuck") (r "^1.13.1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 2)))) (h "0accxdad45ddy95ivix7dsjf1pnmwq54ylqab38jy79rx8hk8cyj") (f (quote (("solana"))))))

