(define-module (crates-io li gh lightfield_loader) #:use-module (crates-io))

(define-public crate-lightfield_loader-0.1.0 (c (n "lightfield_loader") (v "0.1.0") (d (list (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "zip") (r "^0.1.17") (d #t) (k 0)))) (h "0cr9qjkqxawdr7m4i4mchkpwr91nrf8vzcfwjma1pgsrp9nzlhhc")))

(define-public crate-lightfield_loader-0.2.0 (c (n "lightfield_loader") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.9.1") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "zip") (r "^0.1.17") (d #t) (k 0)))) (h "0x9qznnm43px1d85mdm2n6iqa5b95garjrgw1s1zjj501fzig1l2")))

(define-public crate-lightfield_loader-0.3.0 (c (n "lightfield_loader") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.9.1") (d #t) (k 0)) (d (n "image") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "zip") (r "^0.1.17") (d #t) (k 0)))) (h "137avyyd4mc717m2hj9hkjcgxqwwpc631acaiy51mcxla2w1ydnj")))

