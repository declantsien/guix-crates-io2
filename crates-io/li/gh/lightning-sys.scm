(define-module (crates-io li gh lightning-sys) #:use-module (crates-io))

(define-public crate-lightning-sys-0.1.0 (c (n "lightning-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)))) (h "0a73qfrr5v9k97lbwc6crcqvn51y6f5k95d7s3x3xsv6807d1lpw")))

(define-public crate-lightning-sys-0.1.1 (c (n "lightning-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "18l27m2rbp8iigai8wypm04f7v2fd2jfr01n9gsb0lr3rc1ag0av")))

(define-public crate-lightning-sys-0.1.2 (c (n "lightning-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0g6kpp09amvkznmk76jg1vmhgn13k580kjhj8cfivad65hwf7dpi")))

(define-public crate-lightning-sys-0.2.0 (c (n "lightning-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "1rpahr1n5sjvvv1qknhbki54hnf06yaw4wh5gl7h6i3fmw8bm7k8")))

(define-public crate-lightning-sys-0.2.1 (c (n "lightning-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.1") (d #t) (k 0)))) (h "0nw5mh2gxfq8wv3cysxy8jmmi474k1sr2a17rnxlyszz6bq54cdy")))

(define-public crate-lightning-sys-0.2.2 (c (n "lightning-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.54.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "paste") (r "^0.1.18") (d #t) (k 0)) (d (n "tt-call") (r "^1.0") (d #t) (k 0)))) (h "19w6kfiwa15fx2y3j5z0j3pzmgnw04k6na1s475dh4m7hm12l1ip")))

