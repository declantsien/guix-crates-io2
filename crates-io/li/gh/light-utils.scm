(define-module (crates-io li gh light-utils) #:use-module (crates-io))

(define-public crate-light-utils-0.1.1 (c (n "light-utils") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "solana-program") (r "^1.18.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1h4l4bjwf5lkf30amg13m11bhpacd9jpdkrxx4nvg2788aqgbq8y")))

(define-public crate-light-utils-0.1.4 (c (n "light-utils") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "solana-program") (r "^1.18.11") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dkrr9z3k12024fkrl01d7mjjxrw8hp7ws56yskpqym9zhfji9r4")))

