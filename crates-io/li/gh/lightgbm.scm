(define-module (crates-io li gh lightgbm) #:use-module (crates-io))

(define-public crate-lightgbm-0.1.0 (c (n "lightgbm") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "lightgbm-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1x3krmqp5vxaqjz3z78fnb0a23b8lmlgw7ippd7j6h80xyqxw0mz")))

(define-public crate-lightgbm-0.1.1 (c (n "lightgbm") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "lightgbm-sys") (r "^0.1.0") (d #t) (k 0)))) (h "03mmpk3p28lf5gchn2hi5nfw7bpahalm23b9z06dh0n9ha9vb2s4")))

(define-public crate-lightgbm-0.1.2 (c (n "lightgbm") (v "0.1.2") (d (list (d (n "derive_builder") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "lightgbm-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1bq3r10mf0h8j0klrpjswr598969cvh4pacwhpy3bsh5im60fl63")))

(define-public crate-lightgbm-0.1.3 (c (n "lightgbm") (v "0.1.3") (d (list (d (n "derive_builder") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "lightgbm-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0cdja67in3wj2mjwykmgq15bvxp7malgwkamaphm89jv5yn2pxm7")))

(define-public crate-lightgbm-0.2.0 (c (n "lightgbm") (v "0.2.0") (d (list (d (n "derive_builder") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "lightgbm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "035bw2ri35r6vx9i8hsz0m4an3msfsagzb3va0w5kjdk1m674ddc")))

(define-public crate-lightgbm-0.2.1 (c (n "lightgbm") (v "0.2.1") (d (list (d (n "derive_builder") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "lightgbm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0c762zfd3ng1lcz9sjzfmgicp1a8172zz6h15s07ghr64w6r04ai")))

(define-public crate-lightgbm-0.2.2 (c (n "lightgbm") (v "0.2.2") (d (list (d (n "derive_builder") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "lightgbm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "1wcblbbsxdc7m35z45cqcx13iwd4nrdzycxzz8h9z3w2qk4zpiz1")))

(define-public crate-lightgbm-0.2.3 (c (n "lightgbm") (v "0.2.3") (d (list (d (n "derive_builder") (r "^0.5.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "lightgbm-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "0hy67bh04rz4xzhj2awccsdnjdc32lm6rifzrbrdax8gpqx0y9al")))

