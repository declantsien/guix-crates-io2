(define-module (crates-io li gh lightway-core) #:use-module (crates-io))

(define-public crate-lightway-core-0.0.1 (c (n "lightway-core") (v "0.0.1") (h "1hf9dhdpm61yhz3shxmv1p2zs95ixisz29jmqn6w0z5xwdbqp27s")))

(define-public crate-lightway-core-0.0.2 (c (n "lightway-core") (v "0.0.2") (h "0gyprvrxg65wbii2c4dwvkcnwjxjirjwz0i0v3apcaphh02dv2ph")))

