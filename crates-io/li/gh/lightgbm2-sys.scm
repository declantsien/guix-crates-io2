(define-module (crates-io li gh lightgbm2-sys) #:use-module (crates-io))

(define-public crate-lightgbm2-sys-0.3.0 (c (n "lightgbm2-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0f75l6h2ip0ixcqwiksksb291n9iqf3wynzsi2xrna7w022y9sny")))

