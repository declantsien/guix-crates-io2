(define-module (crates-io li gh lightningscanner) #:use-module (crates-io))

(define-public crate-lightningscanner-1.0.0 (c (n "lightningscanner") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "elain") (r "^0.3.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5.0") (d #t) (k 2)))) (h "03q496jadrkjd9yyyhmdam0nnrfggspp8xjnyg45hmwl3a1gdsg2")))

(define-public crate-lightningscanner-1.0.1 (c (n "lightningscanner") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "elain") (r "^0.3.0") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5.0") (d #t) (k 2)))) (h "0aljjjq60b0s07n6bf72dnqvl1gr4bfkxbcwgikbqdiqddi75vkx")))

(define-public crate-lightningscanner-1.0.2 (c (n "lightningscanner") (v "1.0.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "elain") (r "^0.3.0") (d #t) (k 0)) (d (n "tinyrand") (r "^0.5.0") (d #t) (k 2)))) (h "12h31xmz3p1js9ah4999l5qfr6ycf7lrkqm1150bp13rjb2p9pyz")))

