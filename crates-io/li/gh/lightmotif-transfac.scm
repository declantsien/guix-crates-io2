(define-module (crates-io li gh lightmotif-transfac) #:use-module (crates-io))

(define-public crate-lightmotif-transfac-0.1.0 (c (n "lightmotif-transfac") (v "0.1.0") (d (list (d (n "lightmotif") (r "^0.1.0") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0pl9iqx6jgfd8fw818ghxzfajrff5dn6c2p00wlp4cfk7rpsf3dv")))

(define-public crate-lightmotif-transfac-0.1.1 (c (n "lightmotif-transfac") (v "0.1.1") (d (list (d (n "lightmotif") (r "^0.1.1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0aaqm945n143cfrqjch5bc2c41qk2yb67k35zsnzlgpvgiv77v8s")))

(define-public crate-lightmotif-transfac-0.2.0 (c (n "lightmotif-transfac") (v "0.2.0") (d (list (d (n "lightmotif") (r "^0.2.0") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "018k2wpyy65lzjd86nxhsl1lwqhzxzzn0v8cvlj9fx27f0cdgqvf")))

(define-public crate-lightmotif-transfac-0.3.0 (c (n "lightmotif-transfac") (v "0.3.0") (d (list (d (n "lightmotif") (r "^0.3.0") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1glmx6mhzrwzdfi5jzjxa1l99w0r5afjl6zca2w84q5m9cgd1p6v")))

(define-public crate-lightmotif-transfac-0.4.0 (c (n "lightmotif-transfac") (v "0.4.0") (d (list (d (n "lightmotif") (r "^0.4.0") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1klhrx7bd717qgvdl413q085hx6sv0srp1dsrjfg21lxkf6v1vg8")))

(define-public crate-lightmotif-transfac-0.5.0 (c (n "lightmotif-transfac") (v "0.5.0") (d (list (d (n "lightmotif") (r "^0.5.0") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1iz85vjar0y82r2f93r412m0ifxpwm8hixc4rip1m41l37jkqxn3") (y #t)))

(define-public crate-lightmotif-transfac-0.5.1 (c (n "lightmotif-transfac") (v "0.5.1") (d (list (d (n "lightmotif") (r "^0.5.1") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "04zmhlj1jzili4if989lzp35n4m3k4gf1haknrccg58bmgwfvh1n")))

(define-public crate-lightmotif-transfac-0.6.0 (c (n "lightmotif-transfac") (v "0.6.0") (d (list (d (n "lightmotif") (r "^0.6.0") (d #t) (k 0)) (d (n "lightmotif-io") (r "^0.6.0") (d #t) (k 0)))) (h "03z6qc7whafcx7ngr6097nssk7gmjm7capr07a5p7gcfg6h6h9w3")))

