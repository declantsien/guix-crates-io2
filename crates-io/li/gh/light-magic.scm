(define-module (crates-io li gh light-magic) #:use-module (crates-io))

(define-public crate-light-magic-0.1.0 (c (n "light-magic") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.62") (d #t) (k 0)))) (h "107kc35m7mfm5vynbb2j1khcsdlx6ngz1w6i4gqxbkdihb4pbgyv") (r "1.60.0")))

