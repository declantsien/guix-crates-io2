(define-module (crates-io li gh lightning-wire-msgs) #:use-module (crates-io))

(define-public crate-lightning-wire-msgs-0.1.0 (c (n "lightning-wire-msgs") (v "0.1.0") (d (list (d (n "lightning-wire-msgs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)))) (h "0k9sz2nsdpihqxzpcqg6a5dv7pbqs7fk7dgd0090kil14h7czsmk")))

(define-public crate-lightning-wire-msgs-0.1.1 (c (n "lightning-wire-msgs") (v "0.1.1") (d (list (d (n "lightning-wire-msgs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)))) (h "0p5din2vsp5fwqid4jcbm823j3svfz2sfrrd4bpbhrwhzc2ddprw")))

(define-public crate-lightning-wire-msgs-0.1.2 (c (n "lightning-wire-msgs") (v "0.1.2") (d (list (d (n "lightning-wire-msgs-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.1") (d #t) (k 0)))) (h "1h9lpna2fr6m224cx7brnlcsr907ypyb8zy0anp801fi2ynymmiq")))

(define-public crate-lightning-wire-msgs-0.2.1 (c (n "lightning-wire-msgs") (v "0.2.1") (d (list (d (n "lightning-wire-msgs-derive") (r "^0.2.3") (d #t) (k 0)))) (h "1ijkizrqf52zk7iw8r2a08cn0p64800d9fqvk45gd8zasman8d2n")))

