(define-module (crates-io li gh lightlog) #:use-module (crates-io))

(define-public crate-lightlog-0.1.0 (c (n "lightlog") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "1llb96m55b8gmkawib98v6ici3ii671vk0b3wvhgnx55rq538wgb")))

(define-public crate-lightlog-0.1.1 (c (n "lightlog") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)))) (h "0mfnhndavpzm8jch52j5djica3lgd2rq61c9lw37n8scbzvzswrm")))

