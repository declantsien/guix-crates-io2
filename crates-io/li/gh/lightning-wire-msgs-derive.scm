(define-module (crates-io li gh lightning-wire-msgs-derive) #:use-module (crates-io))

(define-public crate-lightning-wire-msgs-derive-0.1.0 (c (n "lightning-wire-msgs-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.3") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0pmwbi41yhccdp2imc96ijv96lvfzbnkyyi4i4l9zs6ka8mnk2ww")))

(define-public crate-lightning-wire-msgs-derive-0.1.1 (c (n "lightning-wire-msgs-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0ly7016bd1p7w11ih0a2mcbj6iv6m9xzjff7iswli1y32vsg8bdp")))

(define-public crate-lightning-wire-msgs-derive-0.1.2 (c (n "lightning-wire-msgs-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0xl6vpcxcnxjdxfj6flyfsg4bpv1x3in9wh2pa6safmf9dwha9mr")))

(define-public crate-lightning-wire-msgs-derive-0.2.0 (c (n "lightning-wire-msgs-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1ws80a5b3vh30vvb4zy2b40qzaaf69h2g4gzz7swza8im6pprfxm")))

(define-public crate-lightning-wire-msgs-derive-0.2.1 (c (n "lightning-wire-msgs-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1c371c9v4a32c2clx014scj601ybx6l7xhvhv8pwj6gjb6kb4lgz")))

(define-public crate-lightning-wire-msgs-derive-0.2.2 (c (n "lightning-wire-msgs-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "030gigablvin8ilgiywqp165jc6zv9wyqxyzlrv538zzvfjliqxr")))

(define-public crate-lightning-wire-msgs-derive-0.2.3 (c (n "lightning-wire-msgs-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1cwxhmdqsj0xcf04izbphx7njmiaxyfzixg6pfyqwv42n43b9w9g")))

(define-public crate-lightning-wire-msgs-derive-0.2.4 (c (n "lightning-wire-msgs-derive") (v "0.2.4") (d (list (d (n "lightning-wire-msgs-derive-base") (r "^0.2.4") (d #t) (k 0)))) (h "0zbix3zgv811a3qj7fyr7sp9q36hig0cv5k5r7nkrzwhrfprr2pv")))

(define-public crate-lightning-wire-msgs-derive-0.2.5 (c (n "lightning-wire-msgs-derive") (v "0.2.5") (d (list (d (n "lightning-wire-msgs-derive-base") (r "^0.2.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "0mj1839nrnrk7lsj6qix2bzy3gw3040hmk618q5lza0xpmidylqm")))

(define-public crate-lightning-wire-msgs-derive-0.2.6 (c (n "lightning-wire-msgs-derive") (v "0.2.6") (d (list (d (n "lightning-wire-msgs-derive-base") (r "^0.2.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (d #t) (k 0)))) (h "1pxk72srs8b8ymswmdvyv3ii1vv6qmcrshvsdip0whpn0cz0p3p6")))

