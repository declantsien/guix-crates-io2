(define-module (crates-io li gh light-control) #:use-module (crates-io))

(define-public crate-light-control-2.0.0 (c (n "light-control") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mustache") (r "^0.9") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.6") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "18l6fyfjv57l442dd8ig39vxyjf8k76zd5bjd66hdiblc7adin9z")))

(define-public crate-light-control-2.1.0 (c (n "light-control") (v "2.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mustache") (r "^0.9") (d #t) (k 0)) (d (n "paho-mqtt") (r "^0.6") (f (quote ("bundled"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "1fd1afbm9iypif0wdxl7wracns23d5vvkbgfqwx3d0h54w3awvxq")))

