(define-module (crates-io li gh light_enum) #:use-module (crates-io))

(define-public crate-light_enum-0.1.0 (c (n "light_enum") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (d #t) (k 0)))) (h "13yssvap06an59lr3689z4ylqh804bk8p2mkk59cd5iakqcnf89y")))

(define-public crate-light_enum-0.2.0 (c (n "light_enum") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1ppknmx0zfcihblyf32sgpyvssaa72i4dy29f0bw4aj084k2dh3b")))

(define-public crate-light_enum-0.2.1 (c (n "light_enum") (v "0.2.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1gh9qs1qz1ckja683x40pcrbyx2km85m63pn62bi3cpmp1ri2dqz")))

(define-public crate-light_enum-0.2.2 (c (n "light_enum") (v "0.2.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1crdyz9f7534x7ss4dk3r28lqiwa9ngh5v02q6q48lbvdn2lzdmw")))

