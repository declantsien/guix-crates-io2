(define-module (crates-io li gh light-heap) #:use-module (crates-io))

(define-public crate-light-heap-0.1.1 (c (n "light-heap") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)))) (h "1mnhmjlw0y2ccqxlb8mk11g4jalyrsj9cgvlg5fhmzg3k0q8a814")))

(define-public crate-light-heap-0.1.5 (c (n "light-heap") (v "0.1.5") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)))) (h "1wrz9nr37p5ypwm7v3jg1hg2raan1998gsf0jp2gvnwzzfmmnqpr")))

