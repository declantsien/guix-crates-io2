(define-module (crates-io li gh light-ini) #:use-module (crates-io))

(define-public crate-light-ini-0.1.0 (c (n "light-ini") (v "0.1.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "01fgcc5xih2pl8qjl3ij26qwwbf38pkym690lf966aa0a25kcwqp")))

(define-public crate-light-ini-0.1.1 (c (n "light-ini") (v "0.1.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0n76ck9pl3mj8yavqw43a1cshysrpbjx15750hs6q30inmgnbanw")))

(define-public crate-light-ini-0.1.2 (c (n "light-ini") (v "0.1.2") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "14bmm8sd9ypk288q6jcsdkh02pkzldznpak2mjxd5b1sbqszg6pz")))

(define-public crate-light-ini-0.1.3 (c (n "light-ini") (v "0.1.3") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "00l6w26z5hn17bw41lmnl2ybgds1hp63wysgpxlm9y282k36rr1j")))

(define-public crate-light-ini-0.2.0 (c (n "light-ini") (v "0.2.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "06ayl0pqm6pm9zjbm37w3f8njzac4mbysayiigmhwwaap625v6h5")))

(define-public crate-light-ini-0.2.1 (c (n "light-ini") (v "0.2.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "02da49ax56qx9ycssvp8zqfbjx2syb86145acb4848lmndj2mpnh")))

(define-public crate-light-ini-0.3.0 (c (n "light-ini") (v "0.3.0") (h "01zywk8pz5nqy1w8mmspdga8c6jmlqn252bs2ak1v3yf50p04a22")))

