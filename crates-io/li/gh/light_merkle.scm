(define-module (crates-io li gh light_merkle) #:use-module (crates-io))

(define-public crate-light_merkle-0.1.0 (c (n "light_merkle") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.174") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.174") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)) (d (n "sha3") (r "^0.10.8") (d #t) (k 0)))) (h "1ka37mlaigkkf8my850xb8sz5fivdmxdw3y7853k52b2rb4p52jj")))

