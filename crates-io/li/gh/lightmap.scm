(define-module (crates-io li gh lightmap) #:use-module (crates-io))

(define-public crate-lightmap-0.1.0 (c (n "lightmap") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "fyrox-math") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "uvgen") (r "^0.1.0") (d #t) (k 0)))) (h "1znfq6222l5b57rfrsfcm6hg2w21p3gw2zk9pslv0sqr6mbj48vf") (r "1.72")))

(define-public crate-lightmap-0.1.1 (c (n "lightmap") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7.4") (d #t) (k 0)) (d (n "fxhash") (r "^0.2.1") (d #t) (k 0)) (d (n "fyrox-math") (r "^0.1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "uvgen") (r "^0.1.0") (d #t) (k 0)))) (h "1vhp4azck8cxa317gl4nm84bxlf61f0m785z1pr212an7dgbxgqw") (r "1.72")))

