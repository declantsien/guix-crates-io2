(define-module (crates-io li gh lightning-wire-msgs-derive-base) #:use-module (crates-io))

(define-public crate-lightning-wire-msgs-derive-base-0.2.4 (c (n "lightning-wire-msgs-derive-base") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14x1fswli7kzaw80byd5rd25z1f10zpkpg93f1kgknzcs15lmm0z")))

(define-public crate-lightning-wire-msgs-derive-base-0.2.5 (c (n "lightning-wire-msgs-derive-base") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "14x5iy83jf1665pykb2hs025s8s195bk7apxnrzmnw3hbg9j91vx")))

(define-public crate-lightning-wire-msgs-derive-base-0.2.6 (c (n "lightning-wire-msgs-derive-base") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.1") (d #t) (k 0)) (d (n "syn") (r "^1.0.5") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0sc7ab2zc4ap4vf6zlkxlh1qhg8d5jz7xc72xm7kgsq1sh2xzqj1")))

