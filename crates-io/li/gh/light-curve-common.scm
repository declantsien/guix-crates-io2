(define-module (crates-io li gh light-curve-common) #:use-module (crates-io))

(define-public crate-light-curve-common-0.1.0 (c (n "light-curve-common") (v "0.1.0") (d (list (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0cy1cyrqrvmlm4bc66b46qi6d2mq829v2kg1bq8qf1li0shl03kg")))

(define-public crate-light-curve-common-0.1.1-beta.1 (c (n "light-curve-common") (v "0.1.1-beta.1") (d (list (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "114n19nhldj9wv17zbimyychw8qcvzrv2fmm3kdw62m70970h76y")))

(define-public crate-light-curve-common-0.1.1 (c (n "light-curve-common") (v "0.1.1") (d (list (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "1cnxhgz7l9riwx9awpcx5ba0w6gjixcxvgp96rmxjhgqxh164c92")))

