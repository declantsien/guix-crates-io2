(define-module (crates-io li gh lightmotif-tfmpvalue) #:use-module (crates-io))

(define-public crate-lightmotif-tfmpvalue-0.3.0 (c (n "lightmotif-tfmpvalue") (v "0.3.0") (d (list (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lightmotif") (r "^0.3.0") (d #t) (k 0)))) (h "16n87x9d0qyrqlrzvy2dbhwhqrkk4i6dpls52c00z6054kz2zprc") (f (quote (("default" "fnv"))))))

(define-public crate-lightmotif-tfmpvalue-0.4.0 (c (n "lightmotif-tfmpvalue") (v "0.4.0") (d (list (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lightmotif") (r "^0.4.0") (d #t) (k 0)))) (h "0n2air141mpnyxiv2zj1ljnyxjhra54di1q0925vcn51gxbgd8bg") (f (quote (("default" "fnv"))))))

(define-public crate-lightmotif-tfmpvalue-0.5.0 (c (n "lightmotif-tfmpvalue") (v "0.5.0") (d (list (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lightmotif") (r "^0.5.0") (d #t) (k 0)))) (h "06gji7hpk263z97s45n75jji15vxcgq490hmcqy8y04b3xqfis2p") (f (quote (("default" "fnv")))) (y #t)))

(define-public crate-lightmotif-tfmpvalue-0.5.1 (c (n "lightmotif-tfmpvalue") (v "0.5.1") (d (list (d (n "fnv") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "lightmotif") (r "^0.5.1") (d #t) (k 0)))) (h "0psjzrr3k04x9qxl2xjxnd09c89470jykvmil0pvnhi4avfdab4i") (f (quote (("default" "fnv"))))))

(define-public crate-lightmotif-tfmpvalue-0.6.0 (c (n "lightmotif-tfmpvalue") (v "0.6.0") (d (list (d (n "lightmotif") (r "^0.6.0") (d #t) (k 0)))) (h "08gs5sixq4bagv9hkn4kcs34c5gr9673y4w59n2xjsswna8q70m1")))

