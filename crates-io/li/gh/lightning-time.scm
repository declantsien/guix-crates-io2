(define-module (crates-io li gh lightning-time) #:use-module (crates-io))

(define-public crate-lightning-time-0.1.0 (c (n "lightning-time") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "prisma") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0lww6igxps0fsrq2ignh9rvsppa1zs0n06qfww15wvzpi832hjh7")))

(define-public crate-lightning-time-0.2.0 (c (n "lightning-time") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (k 0)) (d (n "hex") (r "^0.4.3") (k 0)) (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "palette") (r "^0.7.5") (f (quote ("libm"))) (k 0)) (d (n "regex") (r "^1.10.2") (o #t) (d #t) (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)))) (h "1v7nn2yph1h7r16nqrv7hprnlkibzdc1cpna4jsd93d469nc2ynf") (f (quote (("default" "std")))) (s 2) (e (quote (("std" "dep:regex" "palette/default"))))))

