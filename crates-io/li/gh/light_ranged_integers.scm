(define-module (crates-io li gh light_ranged_integers) #:use-module (crates-io))

(define-public crate-light_ranged_integers-0.1.0 (c (n "light_ranged_integers") (v "0.1.0") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0ik04rk73ynkfnwczhbb0x00c1s7pyx18qhd1x7g7lld7dds6gvy")))

(define-public crate-light_ranged_integers-0.1.1 (c (n "light_ranged_integers") (v "0.1.1") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "0h4pvy2m0mj1iaiprdqkv32jxs6d7ac8d8b9063idazxxm8qdgyh")))

(define-public crate-light_ranged_integers-0.1.2 (c (n "light_ranged_integers") (v "0.1.2") (d (list (d (n "paste") (r "^1") (d #t) (k 0)))) (h "15k84w3gdv18qxz5d7dl9lh41dwnz1rbay06xj3bgzq2gy6il2dx")))

(define-public crate-light_ranged_integers-0.1.3 (c (n "light_ranged_integers") (v "0.1.3") (d (list (d (n "deranged") (r "^0.3.11") (d #t) (k 2)) (d (n "paste") (r "^1") (d #t) (k 2)))) (h "1d8isvisgrihqsgddjqpc08ir18w5ki8hrm34djs2lldsz140w6z")))

