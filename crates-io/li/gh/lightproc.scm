(define-module (crates-io li gh lightproc) #:use-module (crates-io))

(define-public crate-lightproc-0.0.0 (c (n "lightproc") (v "0.0.0") (h "157fn8ngy1cmz8hh7ymyhj3lyfc26vr5h3zpqkd1hn32cfa9nv8x")))

(define-public crate-lightproc-0.3.2 (c (n "lightproc") (v "0.3.2") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.19") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "0m8h6f931wypjmxsm4jc52kpfx0fah15pp4gyz85iinnlwwhdvpf")))

(define-public crate-lightproc-0.3.3-alpha.1 (c (n "lightproc") (v "0.3.3-alpha.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.19") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "0smir8aq4ibzrdk60ic3j164szzwjcv67rl8kh78j1wgpr8k3n0i")))

(define-public crate-lightproc-0.3.3 (c (n "lightproc") (v "0.3.3") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.6") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.19") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "114kgdwv32m2nhjil211r3abnqlp7wmaybfng8fiaqw0rq8azzy2")))

(define-public crate-lightproc-0.3.4 (c (n "lightproc") (v "0.3.4") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.19") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "1w5kybgv7q7y2m94wa00gyw2x0bs4y4j6306ffh4vkrykxybi7r3")))

(define-public crate-lightproc-0.3.5-alpha.0 (c (n "lightproc") (v "0.3.5-alpha.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "futures-preview") (r "= 0.3.0-alpha.19") (d #t) (k 2)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0-alpha.4") (d #t) (k 0)))) (h "19fr9cm1dr1c4brhfb3dbzvmz0y43vgflv70drjn1v282m7720b1")))

(define-public crate-lightproc-0.3.5 (c (n "lightproc") (v "0.3.5") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.7") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)))) (h "1q2dhwrccnhpzibml35izksszlv2gfnmz14d0mwxadfsqh1c5xzb")))

(define-public crate-lightproc-0.3.6-alpha.0 (c (n "lightproc") (v "0.3.6-alpha.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 2)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)) (d (n "futures-executor") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 2)) (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)))) (h "03cmly19z2ipn7rv7x083j6daynl3510pmgn1dk3h5f1gyd3d2ra")))

