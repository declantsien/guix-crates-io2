(define-module (crates-io li gh lightpack-derive) #:use-module (crates-io))

(define-public crate-lightpack-derive-0.1.0 (c (n "lightpack-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0yb028km0fc1qdcfz3v7j1favvlc7al0jpbrb5ssgjrvqpypjcch")))

(define-public crate-lightpack-derive-0.1.1 (c (n "lightpack-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0nkfz8d9dr7gl6fkyh6964r6wlwr49i9d110nihzmvy18czc8msv")))

(define-public crate-lightpack-derive-0.2.0 (c (n "lightpack-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "16p4qhdy5khyd500b4hjwbdrrdzvfjsfwj310rbwq40czi9i17hc")))

(define-public crate-lightpack-derive-0.2.1 (c (n "lightpack-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0npqgwv4n7jwbdqic1kplagf8ah99sw2jf05psixvfs9jr6v1wcx")))

(define-public crate-lightpack-derive-0.2.2 (c (n "lightpack-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0wkymbf11hbmlvf28midxy5r5nz7swj1lsp8yn2hignjy9x51wv7")))

(define-public crate-lightpack-derive-0.2.4 (c (n "lightpack-derive") (v "0.2.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0w059g9vqw5yix0xd03s0xmd6yiyglp0vfmxp5537znilws9v2rb")))

(define-public crate-lightpack-derive-0.2.5 (c (n "lightpack-derive") (v "0.2.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0pm0mb13q9rqyvx2hcbxs2ydxlwafcffi6m21w48assa98wpr135")))

(define-public crate-lightpack-derive-0.2.6 (c (n "lightpack-derive") (v "0.2.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "0s39mc4fzivzc4l8rqgzjhci5j58z7rlmh4k5ii4rigarvm16z7q")))

(define-public crate-lightpack-derive-0.2.7 (c (n "lightpack-derive") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1papxq9xvpxdjlklwn10i4d0lgwnnryil015l5pg1sfqd5m3jicm")))

(define-public crate-lightpack-derive-0.2.8 (c (n "lightpack-derive") (v "0.2.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1yyhkxi6vcqmay47gk420y85agam9ypkkmnbipnfn3chmq7kg3gl")))

