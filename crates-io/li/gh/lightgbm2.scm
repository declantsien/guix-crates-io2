(define-module (crates-io li gh lightgbm2) #:use-module (crates-io))

(define-public crate-lightgbm2-0.3.0 (c (n "lightgbm2") (v "0.3.0") (d (list (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)) (d (n "lightgbm2-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "polars") (r "^0.26") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "02bkbdrm8qzfi3dlzrvjmbzk8f0pmj9r650dbfh0x04vr7d4grg8") (f (quote (("default") ("dataframe" "polars"))))))

