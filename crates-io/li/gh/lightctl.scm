(define-module (crates-io li gh lightctl) #:use-module (crates-io))

(define-public crate-lightctl-0.1.0 (c (n "lightctl") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dbus") (r "^0.9.5") (f (quote ("futures"))) (d #t) (k 0)))) (h "18q707jgg1dj7nbnhvgdcs2g9cz78zcx8ajmvjryxbj4qnaqldlx")))

