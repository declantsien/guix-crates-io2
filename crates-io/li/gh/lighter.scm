(define-module (crates-io li gh lighter) #:use-module (crates-io))

(define-public crate-lighter-0.1.0 (c (n "lighter") (v "0.1.0") (d (list (d (n "lighter-derive") (r "^0.1.0") (k 0)))) (h "1i9s3z4jhq4rr3j3ldv404ywb5ra3saj5pkzzqxkdilmvnacwnas") (f (quote (("std") ("default" "std"))))))

