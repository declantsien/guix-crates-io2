(define-module (crates-io li gh light-merkle-tree-reference) #:use-module (crates-io))

(define-public crate-light-merkle-tree-reference-0.1.1 (c (n "light-merkle-tree-reference") (v "0.1.1") (d (list (d (n "light-bounded-vec") (r "^0.1.1") (d #t) (k 0)) (d (n "light-hasher") (r "^0.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1wnhj8izdr0bh96w0pba3xz5q1gpv5a8lj4p3cg0ch4j1mjyb6n1")))

(define-public crate-light-merkle-tree-reference-0.1.4 (c (n "light-merkle-tree-reference") (v "0.1.4") (d (list (d (n "light-bounded-vec") (r "^0.1.4") (d #t) (k 0)) (d (n "light-hasher") (r "^0.1.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12hg9q97i146ngh75fjmll760nh0a895kmk033mqzmw0qwljz1b1")))

