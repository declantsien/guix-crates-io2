(define-module (crates-io li gh lightgbm-sys) #:use-module (crates-io))

(define-public crate-lightgbm-sys-0.1.0 (c (n "lightgbm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "0d84d25baaizrc8bicxh6h5jmifj1wxgi2wi4pfs3gbz69d6nmcc")))

(define-public crate-lightgbm-sys-0.2.0 (c (n "lightgbm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "00mz6r6a75b52jzi6h522hq0v3lwfdxf3aim27knifsqrcfkg6li")))

(define-public crate-lightgbm-sys-0.3.0 (c (n "lightgbm-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.81") (d #t) (k 0)))) (h "0qglfm01xvgg4p3iqvxq7dirl810p835ck8msc6f2sr4la16rdph")))

