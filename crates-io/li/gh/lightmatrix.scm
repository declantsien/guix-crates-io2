(define-module (crates-io li gh lightmatrix) #:use-module (crates-io))

(define-public crate-lightmatrix-0.1.0 (c (n "lightmatrix") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0yc4kxikdgv82vm5xphqy2qr3apr64sjqinzkykiri165a3czd7j")))

(define-public crate-lightmatrix-0.2.0 (c (n "lightmatrix") (v "0.2.0") (d (list (d (n "delegate") (r "^0.6.1") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "10gpxi70ha9nsxynf2vb67c6giahj1xqxidfg4y4my4gv6q5q8j8")))

(define-public crate-lightmatrix-0.3.0 (c (n "lightmatrix") (v "0.3.0") (d (list (d (n "delegate") (r "^0.6.1") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "05xlm6x6sd9b941i7jzdzcll74zzpif1y9985cyd8nfs4j9gx3b3")))

(define-public crate-lightmatrix-0.4.0 (c (n "lightmatrix") (v "0.4.0") (d (list (d (n "delegate") (r "^0.6.1") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "1f54lrqxj2izb4vl0mw4xbklg6a9rhqcyk0s4gx6c3vw1jz5mp0r")))

(define-public crate-lightmatrix-0.5.0 (c (n "lightmatrix") (v "0.5.0") (d (list (d (n "delegate") (r "^0.6.1") (d #t) (k 0)) (d (n "more-asserts") (r "^0.2") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.5") (d #t) (k 0)) (d (n "quickcheck") (r "^1") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.0") (d #t) (k 2)))) (h "0afc0cq7v455hg41852fypi789n2mimxnackzbmm8phjd0zzvb0w")))

