(define-module (crates-io li gh lightmotif) #:use-module (crates-io))

(define-public crate-lightmotif-0.1.0 (c (n "lightmotif") (v "0.1.0") (h "0yp68p7vz3jjp87ymrjyfh0j251b47zmyhhrvqdck3wy46vgqb89")))

(define-public crate-lightmotif-0.1.1 (c (n "lightmotif") (v "0.1.1") (h "0jm4qr0ky892a2zaw96mz4lj23qhwifgbi6505qvrhvlbi8xvcxx")))

(define-public crate-lightmotif-0.2.0 (c (n "lightmotif") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "056mvgg6dn2z4gjpzg5kf9z4sza5c8whkzgmkx8qfiwsmhxbnlvw")))

(define-public crate-lightmotif-0.3.0 (c (n "lightmotif") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "07dnci71ja92v5c8g5kyibcc4vhzp9di2p1qx9arg2sqzx0l03cc")))

(define-public crate-lightmotif-0.4.0 (c (n "lightmotif") (v "0.4.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "1l1yq5p6h8xnc61h695arafgvxrb2navbbb4601hn6yw5rhb2dbx")))

(define-public crate-lightmotif-0.5.0 (c (n "lightmotif") (v "0.5.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "1gn3ydgqcv1hwar4yggd1h8prqljjhgzqwhzm1v44gsnyn2rxp3h") (y #t)))

(define-public crate-lightmotif-0.5.1 (c (n "lightmotif") (v "0.5.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "05pwljsccxyspkh3haxhq7bfa8g27l3cg5vn3rvia8kycrq4nc04")))

(define-public crate-lightmotif-0.6.0 (c (n "lightmotif") (v "0.6.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "typenum") (r "^1.16") (d #t) (k 0)))) (h "07cvyhhcdj2m2izzvf2rbsjzgg3dzf7aqv97srzbkizisr0bjkz2")))

