(define-module (crates-io li gh lightdock) #:use-module (crates-io))

(define-public crate-lightdock-0.3.1 (c (n "lightdock") (v "0.3.1") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "npyz") (r "^0.8.3") (d #t) (k 0)) (d (n "pdbtbx") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0319dy5h7ck1abzf0zyb5pm1f4ci386g0k1gh2w2w1adbidyk027")))

(define-public crate-lightdock-0.3.2 (c (n "lightdock") (v "0.3.2") (d (list (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "npyz") (r "^0.8.3") (d #t) (k 0)) (d (n "pdbtbx") (r "^0.11.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)))) (h "0fcfcflvgdxi266maxajkhvcj61xin32pjfvapvr9afc2jvcn2sg")))

