(define-module (crates-io li gh light-poseidon) #:use-module (crates-io))

(define-public crate-light-poseidon-0.0.1 (c (n "light-poseidon") (v "0.0.1") (d (list (d (n "ark-bn254") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3.0") (d #t) (k 0)))) (h "0jbx92cx8l700kl3ig4rszxy9j7zfk8kcb3nv7zyim1w0awsmym2")))

(define-public crate-light-poseidon-0.0.2 (c (n "light-poseidon") (v "0.0.2") (d (list (d (n "ark-bn254") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0yvb7c145gqinh0jsc48xpicr74r4kyzy8xhmmf3b00m17ircrlj")))

(define-public crate-light-poseidon-0.0.3 (c (n "light-poseidon") (v "0.0.3") (d (list (d (n "ark-bn254") (r "^0.3.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.3.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0kjacyyic6g5bzm0qjnzdkgml26k0lh9546dfizdwazghaypaxzg")))

(define-public crate-light-poseidon-0.0.4 (c (n "light-poseidon") (v "0.0.4") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0d4nayx7ihaivbv0z9gzvrrj1h7h0kjgaq96fwqrp3qmmr7liahj")))

(define-public crate-light-poseidon-0.0.5 (c (n "light-poseidon") (v "0.0.5") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0m3bsiyck4vlkhyfsa3p8jv6hgafqpnzlgxdbfflh09df9yz53n3")))

(define-public crate-light-poseidon-0.0.6 (c (n "light-poseidon") (v "0.0.6") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0396v7hr3ydj5y7hdvzdzx5w4bwgyp6407c6lmpwkj5kd0z9gj20")))

(define-public crate-light-poseidon-0.1.0 (c (n "light-poseidon") (v "0.1.0") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "08z75df4047gj1v6zzjrw2kx61lhky1zr8ymlnf5yzm47jzc87cz")))

(define-public crate-light-poseidon-0.1.1 (c (n "light-poseidon") (v "0.1.1") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1g4hgg67sby40m7qv2hcmny354dr6kdvm9p98lfli4zdwhidv6wl") (f (quote (("fuzz-tests"))))))

(define-public crate-light-poseidon-0.1.2 (c (n "light-poseidon") (v "0.1.2") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1xsi2i0qkrlhksvr32glbmkglggi9mrg25vkripdih6zkj03kd55")))

(define-public crate-light-poseidon-0.1.3 (c (n "light-poseidon") (v "0.1.3") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0qm833wc1hh13k7znibmmwm9vmj8vzhr8qlp3hqgy7qykkznfx65") (y #t)))

(define-public crate-light-poseidon-0.2.0 (c (n "light-poseidon") (v "0.2.0") (d (list (d (n "ark-bn254") (r "^0.4.0") (d #t) (k 0)) (d (n "ark-ff") (r "^0.4.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "num-bigint") (r "^0.4.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1vp7gqnlvdkkhlqjfpyqmhh9s5wiv174n1kqfpmrqm1cfnlqb6iw")))

