(define-module (crates-io li gh light-cli) #:use-module (crates-io))

(define-public crate-light-cli-0.1.0 (c (n "light-cli") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.5.8") (d #t) (t "thumbv7m-none-eabi") (k 2)) (d (n "cortex-m-rt") (r "^0.6.7") (d #t) (t "thumbv7m-none-eabi") (k 2)) (d (n "embedded-hal") (r "^0.2.2") (d #t) (k 0)) (d (n "heapless") (r "^0.4.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "panic-abort") (r "^0.3.1") (d #t) (t "thumbv7m-none-eabi") (k 2)) (d (n "stm32f1xx-hal") (r "^0.2.1") (f (quote ("stm32f103"))) (d #t) (t "thumbv7m-none-eabi") (k 2)))) (h "0wgyl37gf0pbv7kjighjawmis4xi5j9rj3ggif3476yswfj63d02") (f (quote (("doc"))))))

