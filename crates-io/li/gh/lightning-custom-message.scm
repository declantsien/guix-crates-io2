(define-module (crates-io li gh lightning-custom-message) #:use-module (crates-io))

(define-public crate-lightning-custom-message-0.0.1 (c (n "lightning-custom-message") (v "0.0.1") (h "0vgvigmj5ddgqgkj2igkdp3vw07r99anaf50b7r45pk9sq9k8q5a")))

(define-public crate-lightning-custom-message-0.0.114 (c (n "lightning-custom-message") (v "0.0.114") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "lightning") (r "^0.0.114") (d #t) (k 0)))) (h "0zx1jnf46i1b40r8xggqd07an30qyj2fw2w52a7nfmnz4636za3z")))

(define-public crate-lightning-custom-message-0.0.115 (c (n "lightning-custom-message") (v "0.0.115") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "lightning") (r "^0.0.115") (d #t) (k 0)))) (h "0y0pd5fmbw2hgr719bgywc322dfs7n5a32fyf7vl312djbqpixnb")))

(define-public crate-lightning-custom-message-0.0.116-alpha1 (c (n "lightning-custom-message") (v "0.0.116-alpha1") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "lightning") (r "^0.0.116-alpha1") (d #t) (k 0)))) (h "1g20p1b73v0b2gq7dvdprvv8g5s0mpl84lg013snz0xs256jvqia")))

(define-public crate-lightning-custom-message-0.0.116-rc1 (c (n "lightning-custom-message") (v "0.0.116-rc1") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "lightning") (r "^0.0.116-rc1") (d #t) (k 0)))) (h "19gzf3flih7i65k597nkxza312bzhw458vkpn20k66zflgp25fhv")))

(define-public crate-lightning-custom-message-0.0.116 (c (n "lightning-custom-message") (v "0.0.116") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "lightning") (r "^0.0.116") (d #t) (k 0)))) (h "19i6gy9rfi0fg63b6cwl2d7kaxjp9ya65qnvqp12mi9jbzyjkp2p")))

(define-public crate-lightning-custom-message-0.0.117-alpha1 (c (n "lightning-custom-message") (v "0.0.117-alpha1") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "lightning") (r "^0.0.117-alpha1") (d #t) (k 0)))) (h "141x0jm05d570q5y3zaw8vj8hwqn6pw3n3jgd80ryrbpr48zkpjq")))

(define-public crate-lightning-custom-message-0.0.117-alpha2 (c (n "lightning-custom-message") (v "0.0.117-alpha2") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "lightning") (r "^0.0.117-alpha2") (d #t) (k 0)))) (h "0wssbi6jq5xxiwdma48w9xqzqhwxzdcinqp55pjaxvqypv79himx")))

(define-public crate-lightning-custom-message-0.0.117-rc1 (c (n "lightning-custom-message") (v "0.0.117-rc1") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "lightning") (r "^0.0.117-rc1") (d #t) (k 0)))) (h "0iymlmk0n8008l8bii326szbgl8zcpygh7d43wsfd6ga4fjhwcm1")))

(define-public crate-lightning-custom-message-0.0.117 (c (n "lightning-custom-message") (v "0.0.117") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "lightning") (r "^0.0.117") (d #t) (k 0)))) (h "0bv150yp5g3p8lygv5ahls1bgp19i9ixxnq2s1g6gk8a5qyl51wd")))

(define-public crate-lightning-custom-message-0.0.118 (c (n "lightning-custom-message") (v "0.0.118") (d (list (d (n "bitcoin") (r "^0.29.0") (d #t) (k 0)) (d (n "lightning") (r "^0.0.118") (d #t) (k 0)))) (h "06l0a04p1fcax6b7q95h9hi88mb8p23452bqrspfhkjwdl0h3ksj")))

(define-public crate-lightning-custom-message-0.0.119 (c (n "lightning-custom-message") (v "0.0.119") (d (list (d (n "bitcoin") (r "^0.30.2") (d #t) (k 0)) (d (n "lightning") (r "^0.0.119") (d #t) (k 0)))) (h "177cjdv7vy6j3daixp7answcm1ywv48bg3lapbqlin1638rdc014")))

(define-public crate-lightning-custom-message-0.0.120 (c (n "lightning-custom-message") (v "0.0.120") (d (list (d (n "bitcoin") (r "^0.30.2") (d #t) (k 0)) (d (n "lightning") (r "^0.0.120") (d #t) (k 0)))) (h "1vfp4m7899qx55w6x64dpcwxj90v039lnkqb5zv0hzrk3zh7kw9q")))

(define-public crate-lightning-custom-message-0.0.121 (c (n "lightning-custom-message") (v "0.0.121") (d (list (d (n "bitcoin") (r "^0.30.2") (d #t) (k 0)) (d (n "lightning") (r "^0.0.121") (d #t) (k 0)))) (h "16bp312g7ca2gf0hmilj7wplg2lvq78ms59vpnnq1h063bkqwgg8")))

(define-public crate-lightning-custom-message-0.0.122 (c (n "lightning-custom-message") (v "0.0.122") (d (list (d (n "bitcoin") (r "^0.30.2") (d #t) (k 0)) (d (n "lightning") (r "^0.0.122") (d #t) (k 0)))) (h "0y4mr7cgagw81j6rzmc3m27r66l20n9q12q0qazf3zgbba38dpgi")))

(define-public crate-lightning-custom-message-0.0.123-beta (c (n "lightning-custom-message") (v "0.0.123-beta") (d (list (d (n "bitcoin") (r "^0.30.2") (d #t) (k 0)) (d (n "lightning") (r "^0.0.123-beta") (d #t) (k 0)))) (h "0mnrsqp4yj78cwb8b7j9igq0cgryxa7w9bh4ciypsk8kwny6jnfp")))

(define-public crate-lightning-custom-message-0.0.123-rc1 (c (n "lightning-custom-message") (v "0.0.123-rc1") (d (list (d (n "bitcoin") (r "^0.30.2") (d #t) (k 0)) (d (n "lightning") (r "^0.0.123-rc1") (d #t) (k 0)))) (h "0fvpnk52lg7rdrfy0yvqixsbawsmrfvm12p7hz54pwfmk17vg6j1")))

(define-public crate-lightning-custom-message-0.0.123 (c (n "lightning-custom-message") (v "0.0.123") (d (list (d (n "bitcoin") (r "^0.30.2") (d #t) (k 0)) (d (n "lightning") (r "^0.0.123") (d #t) (k 0)))) (h "1ydqbmns23h35dinkw0pz33ffip88i0b491q9j8qk0wkp4zk5az7")))

