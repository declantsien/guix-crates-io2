(define-module (crates-io li gh lightgbm3-sys) #:use-module (crates-io))

(define-public crate-lightgbm3-sys-1.0.0 (c (n "lightgbm3-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1car2qvdwvll7j84vv1rvcj099mdyfykmq73rgwzs9hqxk9i6v6r") (f (quote (("openmp") ("gpu") ("cuda"))))))

(define-public crate-lightgbm3-sys-1.0.1 (c (n "lightgbm3-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yamy6sg1h8bzghxg1jcx797gry0ayzciz4h3pccnqn3sfryq602") (f (quote (("openmp") ("gpu") ("cuda"))))))

(define-public crate-lightgbm3-sys-1.0.2 (c (n "lightgbm3-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0c415fbv1l1sc0m145xk96qmi6jbl3jiphald97i5hqdxaglf08b") (f (quote (("openmp") ("gpu") ("cuda"))))))

