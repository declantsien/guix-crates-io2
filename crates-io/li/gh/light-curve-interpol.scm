(define-module (crates-io li gh light-curve-interpol) #:use-module (crates-io))

(define-public crate-light-curve-interpol-0.1.0 (c (n "light-curve-interpol") (v "0.1.0") (d (list (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "light-curve-common") (r "^0.1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1d9hhsy5v3yc0qp4p5ir7v9l03jr57y5i3vsp57s5j9b1hhn98wg")))

(define-public crate-light-curve-interpol-0.1.1 (c (n "light-curve-interpol") (v "0.1.1") (d (list (d (n "conv") (r "^0.3") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "light-curve-common") (r "^0.1.0") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0ij1z6r64w5g4jbz9d1kzlq86w7fc9y69al53ivxms03mp4bbv11")))

