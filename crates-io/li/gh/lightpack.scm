(define-module (crates-io li gh lightpack) #:use-module (crates-io))

(define-public crate-lightpack-0.1.0 (c (n "lightpack") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "lightpack-derive") (r "^0.1.0") (d #t) (k 0)))) (h "1hplvp09qm9yh2aiiph0vm97wm500xv3jz1819cy2llappi400qn")))

(define-public crate-lightpack-0.1.1 (c (n "lightpack") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "lightpack-derive") (r "^0.1.1") (d #t) (k 0)))) (h "14b4yia8v88aqwh4ijf0jha69vnxb4axlgscx2y9hr4glv5xhn7n")))

(define-public crate-lightpack-0.2.0 (c (n "lightpack") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "lightpack-derive") (r "^0.2.0") (d #t) (k 0)))) (h "13kq2asafyy6q1c6v9rsah41d9gw451krxa02wcack40yfb8hyjz")))

(define-public crate-lightpack-0.2.1 (c (n "lightpack") (v "0.2.1") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "lightpack-derive") (r "^0.2.1") (d #t) (k 0)))) (h "1fnwzkngpg9n5rpxfg428x5g9v18pnvjgkd2dgmmy007swd727mb")))

(define-public crate-lightpack-0.2.2 (c (n "lightpack") (v "0.2.2") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "lightpack-derive") (r "^0.2.2") (d #t) (k 0)))) (h "0whvw15xjn0a22sscm54i2958wm80j2sh0sv170p3slzpbgv4wni")))

(define-public crate-lightpack-0.2.4 (c (n "lightpack") (v "0.2.4") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "lightpack-derive") (r "^0.2.4") (d #t) (k 0)))) (h "0iifi4dh789fl5xgv3dalr1z9rfw3bpifr13k6k4mzrzry5v6f7b")))

(define-public crate-lightpack-0.2.5 (c (n "lightpack") (v "0.2.5") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "lightpack-derive") (r "^0.2.5") (d #t) (k 0)))) (h "0ymgwrb1fzsnjqgbpmzqmlrpyhnphl39r8rpb6fb2zcyb42accf6")))

(define-public crate-lightpack-0.2.6 (c (n "lightpack") (v "0.2.6") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "lightpack-derive") (r "^0.2.6") (d #t) (k 0)))) (h "0f7rcw2rn20sh5jmd79fspvlnb1xllm7niadnc2sp6zbhv2qaa91")))

(define-public crate-lightpack-0.2.7 (c (n "lightpack") (v "0.2.7") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "lightpack-derive") (r "^0.2.7") (d #t) (k 0)))) (h "094bjjx0jh2ns1fs8c50x4zgdji3ra89bl91vw5b71s3jhqwds1z")))

(define-public crate-lightpack-0.2.8 (c (n "lightpack") (v "0.2.8") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "lightpack-derive") (r "^0.2.8") (d #t) (k 0)))) (h "15bqcnv8n9bz9dirvsxidj4md4kbyhyxakp0av9pq275g3qflham")))

