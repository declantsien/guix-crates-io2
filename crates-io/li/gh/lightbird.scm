(define-module (crates-io li gh lightbird) #:use-module (crates-io))

(define-public crate-lightbird-0.1.0 (c (n "lightbird") (v "0.1.0") (h "0fsyi5gnn5zdsl8dq7jxp4imizgwpml9hc77d63wsalpvkamgw2c")))

(define-public crate-lightbird-0.1.1 (c (n "lightbird") (v "0.1.1") (h "0mmgw51dsbh2xljpy2aj7ajg6f7qf8xfv37f9gqm01yl9icwlqy6")))

(define-public crate-lightbird-0.1.2 (c (n "lightbird") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0b60r861qxxb8gqkf13v04ddxlapjzfc0wvmz5hj46kr5bnvb10m")))

(define-public crate-lightbird-0.1.3 (c (n "lightbird") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "01m3zxjzjm7i8rivw43zdydp9qin0nnjda9fv3y1kvpp31dbqgz9")))

(define-public crate-lightbird-0.1.4 (c (n "lightbird") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1zmi84gikg9dfaj8k0iqww7cv4pf0y3b5xcnzjyywvvqr1xbcghj")))

(define-public crate-lightbird-0.1.5 (c (n "lightbird") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1jmp6yy10g0byzhl88cj1hdn51cvxrhp2ng4ys3d1fdlr07v8fqf")))

(define-public crate-lightbird-0.1.6 (c (n "lightbird") (v "0.1.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1k8gw7v2jg90jgxzzxl7hi4g0adfsbd5ydwblp9hh098cld408fa")))

(define-public crate-lightbird-0.1.7 (c (n "lightbird") (v "0.1.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0shzrh32n9lyym7dkadgyh9dgxm3jylphlm8pgngbfdav4pm6z6h")))

(define-public crate-lightbird-0.1.8 (c (n "lightbird") (v "0.1.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "036s6v8c8filp4bwx80mi93vwdiw17vqf4pk9xf5095f5kgfcz12")))

(define-public crate-lightbird-0.1.9 (c (n "lightbird") (v "0.1.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wi8lvmarh74cccq9ykn05aa50cvsmf8csz4ps2x3i4r7zgcr34v")))

(define-public crate-lightbird-0.1.10 (c (n "lightbird") (v "0.1.10") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0syqdm762bf945nbwzr1infm8lz2pi1wb6h2k1m18ngiscy5cr54")))

