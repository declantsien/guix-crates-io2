(define-module (crates-io li gh lightningcss-derive) #:use-module (crates-io))

(define-public crate-lightningcss-derive-1.0.0-alpha.35 (c (n "lightningcss-derive") (v "1.0.0-alpha.35") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1lpa22gzhq0r9ac5nfhvch7hbd5d8qpvgib9l8lwfg9xps7apf41")))

(define-public crate-lightningcss-derive-1.0.0-alpha.36 (c (n "lightningcss-derive") (v "1.0.0-alpha.36") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0sslcsjzkwki64vnxkqv6hsjay7rj285cp9hz7lmz9c5azxjx50c")))

(define-public crate-lightningcss-derive-1.0.0-alpha.37 (c (n "lightningcss-derive") (v "1.0.0-alpha.37") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1c9jz4lqvdabfskcbf84hp7lw0dprfp16412lf4f1pxg102vxbjg")))

(define-public crate-lightningcss-derive-1.0.0-alpha.38 (c (n "lightningcss-derive") (v "1.0.0-alpha.38") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bkdsd36y6gvlm4zhg100kxgy65ghxgr9andwgbf8g7jzjfql86r")))

(define-public crate-lightningcss-derive-1.0.0-alpha.39 (c (n "lightningcss-derive") (v "1.0.0-alpha.39") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0npaf1mmb9apk1x8hh75zrj1mf96z4jw99097zpswgz42d8c1yc0")))

(define-public crate-lightningcss-derive-1.0.0-alpha.40 (c (n "lightningcss-derive") (v "1.0.0-alpha.40") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01x67ywyn0hvpadlhmls2jcc55qnlvn82aprw61kxx1l796fkk0g")))

(define-public crate-lightningcss-derive-1.0.0-alpha.41 (c (n "lightningcss-derive") (v "1.0.0-alpha.41") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0x49j8z00f79d859234lqfr5jndn5jm6hyq0lsxb4fnx0xspxrwq")))

(define-public crate-lightningcss-derive-1.0.0-alpha.42 (c (n "lightningcss-derive") (v "1.0.0-alpha.42") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "15d2r1xbvsdh3inap8z0f9mpq0r1f0pmx99yw4g1zlvr1fgs00lg")))

