(define-module (crates-io li gh light_arena) #:use-module (crates-io))

(define-public crate-light_arena-0.1.0 (c (n "light_arena") (v "0.1.0") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)))) (h "0nqsx3aafa6hdq7srivvi8vdw04lwvkbjdgz32wdralwp2ysaq0m") (f (quote (("unstable" "clippy"))))))

(define-public crate-light_arena-0.1.1 (c (n "light_arena") (v "0.1.1") (d (list (d (n "clippy") (r "^0.0.140") (o #t) (d #t) (k 0)))) (h "1imv6189b95apmcpax2jhifkl0z3j2j2ckn01r88cqsnb0iwx1m8") (f (quote (("unstable" "clippy"))))))

(define-public crate-light_arena-0.1.2 (c (n "light_arena") (v "0.1.2") (d (list (d (n "clippy") (r "^0.0.182") (o #t) (d #t) (k 0)))) (h "12gca607syv3azkqvlx8gyyfmprxiwz2mx17dqs7iiqfqd9m6gpd") (f (quote (("unstable"))))))

(define-public crate-light_arena-1.0.0 (c (n "light_arena") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0.182") (o #t) (d #t) (k 0)))) (h "1g6dwp8sn9h4h379cyhyvskjk8k58cfsfsvblbw10vjy9w1ghyx4") (f (quote (("unstable"))))))

(define-public crate-light_arena-1.0.1 (c (n "light_arena") (v "1.0.1") (d (list (d (n "clippy") (r "^0.0.182") (o #t) (d #t) (k 0)))) (h "16ghxf99kxra1agrdhy0g97kxdhl5rf7iw5h501f6x3rmqhxi9xx") (f (quote (("unstable"))))))

