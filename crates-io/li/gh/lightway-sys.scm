(define-module (crates-io li gh lightway-sys) #:use-module (crates-io))

(define-public crate-lightway-sys-0.1.0 (c (n "lightway-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "wolfssl-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "wolfssl-sys") (r "^0.1.2") (d #t) (k 1)))) (h "1wvh5vhhhr98aa15jp5ilr5y7d5v70802nr2ii1p3g0ihvw8qwjc")))

