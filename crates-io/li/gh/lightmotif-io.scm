(define-module (crates-io li gh lightmotif-io) #:use-module (crates-io))

(define-public crate-lightmotif-io-0.6.0 (c (n "lightmotif-io") (v "0.6.0") (d (list (d (n "lightmotif") (r "^0.6.0") (d #t) (k 0)) (d (n "memchr") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0ks57kqay40k3r920pga1w41qkg55z1jskwcxi3g9pqna37060pd")))

