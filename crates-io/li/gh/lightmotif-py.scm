(define-module (crates-io li gh lightmotif-py) #:use-module (crates-io))

(define-public crate-lightmotif-py-0.1.0 (c (n "lightmotif-py") (v "0.1.0") (d (list (d (n "built") (r "^0.6") (f (quote ("chrono"))) (d #t) (k 1)) (d (n "lightmotif") (r "^0.1.0") (d #t) (k 0)) (d (n "project-root") (r "^0.2.2") (d #t) (k 1)) (d (n "pyo3") (r "^0.18.3") (d #t) (k 0)) (d (n "pyo3-built") (r "^0.4.7") (d #t) (k 0)))) (h "13miw10piw7mmfirbqixsnyc04haz4ffx8pqwaxnwf8si8xq8ykl") (f (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default"))))))

(define-public crate-lightmotif-py-0.1.1 (c (n "lightmotif-py") (v "0.1.1") (d (list (d (n "lightmotif") (r "^0.1.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.3") (d #t) (k 0)))) (h "1c3knx7wk96dyg9wbwbw5m3mz36yd9hnxmlzmhc8a5vnlpgrysmm") (f (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default") ("built"))))))

(define-public crate-lightmotif-py-0.2.0 (c (n "lightmotif-py") (v "0.2.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "lightmotif") (r "^0.2.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.3") (d #t) (k 0)))) (h "049vr16dy1f9zndcff6fy2d9b81db3k93m8i00q5l8zvr80f6z2w") (f (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default") ("built"))))))

(define-public crate-lightmotif-py-0.3.0 (c (n "lightmotif-py") (v "0.3.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "lightmotif") (r "^0.3.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.3") (d #t) (k 0)))) (h "0z34sb0qqagph4ybfdq0180m1fw8f6z87qj37hj0h2zl1vsk1yqk") (f (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default") ("built"))))))

(define-public crate-lightmotif-py-0.4.0 (c (n "lightmotif-py") (v "0.4.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "lightmotif") (r "^0.4.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.3") (d #t) (k 0)))) (h "003yvd1rhn4qmk0qlgs52jhl5f9r04ddhzzlh07ly7y7yf8g0hc0") (f (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default") ("built"))))))

(define-public crate-lightmotif-py-0.5.0 (c (n "lightmotif-py") (v "0.5.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "lightmotif") (r "^0.5.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.3") (d #t) (k 0)))) (h "1dwcrnyvi23dd03qrzs57kjpgch2iwhynsldm36v2aizqskwn8h2") (f (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default") ("built")))) (y #t)))

(define-public crate-lightmotif-py-0.5.1 (c (n "lightmotif-py") (v "0.5.1") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "lightmotif") (r "^0.5.1") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.3") (d #t) (k 0)))) (h "091a1hqc36p5skbqlnvaaz4gf8rinslf2f4k2rhm2cn07irgfz3w") (f (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default") ("built"))))))

(define-public crate-lightmotif-py-0.6.0 (c (n "lightmotif-py") (v "0.6.0") (d (list (d (n "generic-array") (r "^0.14") (d #t) (k 0)) (d (n "lightmotif") (r "^0.6.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.18.3") (d #t) (k 0)))) (h "1kg5mvv4fq68x92yk4z9m4y83ba3xrhgs8gj796c7h37ix5ahkd1") (f (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default") ("built"))))))

