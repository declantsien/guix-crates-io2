(define-module (crates-io li gh lights) #:use-module (crates-io))

(define-public crate-lights-0.1.0 (c (n "lights") (v "0.1.0") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1758jvpfqp5icd8ifp0x5y0ggg88krql14zz7523vnsy6hpd9brz")))

(define-public crate-lights-0.1.1 (c (n "lights") (v "0.1.1") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "1bnxsmiqq7b695xja5v7d79faja1nf4ga8izzbdwfm73df1czjsc")))

(define-public crate-lights-0.1.2 (c (n "lights") (v "0.1.2") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0fpfwz50wy25yhkzrw76w56w8zn1bkd8hq18sj9kbib7y9ah9m8s")))

(define-public crate-lights-0.1.3 (c (n "lights") (v "0.1.3") (d (list (d (n "clap") (r "^2.25.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.9") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.2") (d #t) (k 0)))) (h "0ya98ax79s5p44dryknvbwwhbjr5dmwcggw738i7ix204kp26iy7")))

(define-public crate-lights-0.1.5 (c (n "lights") (v "0.1.5") (d (list (d (n "clap") (r "^2.32") (d #t) (k 0)) (d (n "dirs") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0jq3i224ma81b1v7wnn411vvdc4mk86sa5mgf6zmzz98a28zmd5x")))

