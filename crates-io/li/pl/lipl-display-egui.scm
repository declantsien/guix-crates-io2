(define-module (crates-io li pl lipl-display-egui) #:use-module (crates-io))

(define-public crate-lipl-display-egui-0.1.1 (c (n "lipl-display-egui") (v "0.1.1") (d (list (d (n "eframe") (r "^0.22") (f (quote ("default_fonts" "glow"))) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 0)))) (h "0ff7zmwp62ivjr5l1bx8z5wksql2pk2jhcfy3icbj9sbnzjbvjxk")))

(define-public crate-lipl-display-egui-0.1.2 (c (n "lipl-display-egui") (v "0.1.2") (d (list (d (n "eframe") (r "^0.22") (f (quote ("default_fonts" "glow"))) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 0)))) (h "0hl66b0gcxf6cziay5kvc8174qz3mss2a1f7lfy3lw01h7ay18qy")))

(define-public crate-lipl-display-egui-0.1.3 (c (n "lipl-display-egui") (v "0.1.3") (d (list (d (n "eframe") (r "^0.22") (f (quote ("default_fonts" "glow"))) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 0)))) (h "0ig5g4q94bm3z89rmkm45kq0sizvf5lhay4gkncrzgcdk86qzzah")))

(define-public crate-lipl-display-egui-0.1.4 (c (n "lipl-display-egui") (v "0.1.4") (d (list (d (n "eframe") (r "^0.22") (f (quote ("default_fonts" "glow"))) (k 0)) (d (n "lipl-display-common") (r "^0.2") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 0)))) (h "0rki1vwb8y15lq30q79hzr06jc71nzqhl8m4yg7wx5pshdg6kagp")))

(define-public crate-lipl-display-egui-0.1.5 (c (n "lipl-display-egui") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "eframe") (r "^0.22") (f (quote ("default_fonts" "glow"))) (k 0)) (d (n "lipl-display-common") (r "^0.2.8") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.1.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 0)))) (h "00cr80179dgf6szz15b1pzzc2fgilxmsg1723a24ah0gqqr75zk2")))

(define-public crate-lipl-display-egui-0.3.0 (c (n "lipl-display-egui") (v "0.3.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "eframe") (r "^0.22") (f (quote ("default_fonts" "glow"))) (k 0)) (d (n "lipl-display-common") (r "^0.3.0") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 0)))) (h "0x49f3dx49qcy68l1jyf1s96slqwwksnf3w44mrqqzhxzsspasz4")))

(define-public crate-lipl-display-egui-0.4.0 (c (n "lipl-display-egui") (v "0.4.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "eframe") (r "^0.22") (f (quote ("glow"))) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "lipl-display-common") (r "^0.4.0") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1k1v4rlv96lb4ib8lpvv8a06xaj925klawc01jgafb8qk87pm4fz")))

(define-public crate-lipl-display-egui-0.4.1 (c (n "lipl-display-egui") (v "0.4.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "eframe") (r "^0.23") (f (quote ("glow"))) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 0)) (d (n "lipl-display-common") (r "^0.4.0") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0jpj9wjml4bihdf4la9xklx05nv8yaxcbr18dkg1b420hsphvcjf")))

(define-public crate-lipl-display-egui-0.4.4 (c (n "lipl-display-egui") (v "0.4.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "eframe") (r "^0.26") (f (quote ("glow"))) (k 0)) (d (n "env_logger") (r "^0.11") (d #t) (k 0)) (d (n "lipl-display-common") (r "^0.4") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1qpi7irvdg28b9s5gvbhx20vply1qnr37dw9k1qfwiilk0g9iksp")))

