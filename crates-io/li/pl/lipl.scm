(define-module (crates-io li pl lipl) #:use-module (crates-io))

(define-public crate-lipl-0.1.0 (c (n "lipl") (v "0.1.0") (d (list (d (n "cmd_lib") (r "^1.3.0") (d #t) (k 0)) (d (n "linreg") (r "^0.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "termion") (r "^1.5.3") (d #t) (k 0)) (d (n "tui") (r "^0.6.2") (d #t) (k 0)))) (h "15zcfaamy4k5bmndla1iqi32j6wim6mvhlq93l4viz7v7n7zpszm")))

