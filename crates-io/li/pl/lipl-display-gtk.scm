(define-module (crates-io li pl lipl-display-gtk) #:use-module (crates-io))

(define-public crate-lipl-display-gtk-0.1.2 (c (n "lipl-display-gtk") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glib") (r "^0.18") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1nb4bh49gmrd6h1jjnzcc59dgc614az2dwqdnixhq0rydx07myjz")))

(define-public crate-lipl-display-gtk-0.1.3 (c (n "lipl-display-gtk") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glib") (r "^0.18") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1shyc08x5xir53d68j1bfs79g8qinb5igb88ffxfphy24yf83khf")))

(define-public crate-lipl-display-gtk-0.1.4 (c (n "lipl-display-gtk") (v "0.1.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glib") (r "^0.18") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0mqbalw80gjldzglid8x05gla8l2zbvjmk5f23q4yan9ajysgvxn")))

(define-public crate-lipl-display-gtk-0.1.5 (c (n "lipl-display-gtk") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glib") (r "^0.18") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "11q2bcgb32gb5s9c4f9jn3v85bsgpww3y2iy84b55i5zya555180")))

(define-public crate-lipl-display-gtk-0.1.6 (c (n "lipl-display-gtk") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glib") (r "^0.18") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18") (d #t) (k 0)) (d (n "lipl-display-common") (r "^0.2.8") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.1.20") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08flchadwlhgl353gqh9d3xspsayln3hkn0dipdrhb15mgcbm29y")))

(define-public crate-lipl-display-gtk-0.3.0 (c (n "lipl-display-gtk") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glib") (r "^0.18") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18") (d #t) (k 0)) (d (n "lipl-display-common") (r "^0.3.0") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0nfxarps7xys24mfr7cx7dv4raphbv6hyzab345z4mjb9bh6mxvm")))

(define-public crate-lipl-display-gtk-0.4.0 (c (n "lipl-display-gtk") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glib") (r "^0.18") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18") (d #t) (k 0)) (d (n "lipl-display-common") (r "^0.4.0") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0swf4429p6n1bj5xdpi47lz1y5rq8wvsn3vgfm0h5g11g3qp2ckh")))

(define-public crate-lipl-display-gtk-0.4.4 (c (n "lipl-display-gtk") (v "0.4.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "glib") (r "^0.18") (f (quote ("log"))) (d #t) (k 0)) (d (n "gtk") (r "^0.18") (d #t) (k 0)) (d (n "lipl-display-common") (r "^0.4") (d #t) (k 0)) (d (n "lipl-gatt-bluer") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1iqzfby2x00a48ba0v3p5gv5qrmkjbr2xsvdpm7zq3ns12v2lr35")))

