(define-module (crates-io li pl lipl-display-common) #:use-module (crates-io))

(define-public crate-lipl-display-common-0.2.3 (c (n "lipl-display-common") (v "0.2.3") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1krz2am76kjl4y31jcskr14k61ggjx6mgq0ixch81364qfmjb09y")))

(define-public crate-lipl-display-common-0.2.4 (c (n "lipl-display-common") (v "0.2.4") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0bjwdnydh43x8yrwxcanxzy19k7mm3xhj52z85p1k8j5220ky3gr")))

(define-public crate-lipl-display-common-0.2.5 (c (n "lipl-display-common") (v "0.2.5") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1hwqfi88v3pl0afjnk2gxxvm5hdq28s3mkbg2dd84ja6mcgqzjzh")))

(define-public crate-lipl-display-common-0.2.6 (c (n "lipl-display-common") (v "0.2.6") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0hxp5hyha8xslp2w3na27vn7118nm9yi89jy1wa99nhb7y5n36ck")))

(define-public crate-lipl-display-common-0.2.7 (c (n "lipl-display-common") (v "0.2.7") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1lahls8wb6l9hs2hch7dwjp9h4ka13byw49n5mih96fplx9cqv58")))

(define-public crate-lipl-display-common-0.2.8 (c (n "lipl-display-common") (v "0.2.8") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "05igxzhv2l9pngvfx3n32zc048c7sl7scbq27ayn6gscbhp3mva9")))

(define-public crate-lipl-display-common-0.3.0 (c (n "lipl-display-common") (v "0.3.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "01vvvlgi3y1c15hnl3ksg33bhqbj7c4qi5r3r4w3cxcxljqwsmw9")))

(define-public crate-lipl-display-common-0.4.0 (c (n "lipl-display-common") (v "0.4.0") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0lsn1dm7d1mvw4ymxd6lzx65g88kza40kqfqrf3hd4m8fg7w37v5")))

(define-public crate-lipl-display-common-0.4.4 (c (n "lipl-display-common") (v "0.4.4") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "1rkp4gwbjpnkw2c176kaa4rfhss2dnncl0qn9mjvsa6k13hxpym4")))

(define-public crate-lipl-display-common-0.4.5 (c (n "lipl-display-common") (v "0.4.5") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "uuid") (r "^1") (d #t) (k 0)))) (h "0g0r18l6hhyihsb400v9qln8zlcrhm4zvcx9dwhg5w2p1whp0i2w")))

