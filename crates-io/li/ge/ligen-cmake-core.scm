(define-module (crates-io li ge ligen-cmake-core) #:use-module (crates-io))

(define-public crate-ligen-cmake-core-0.1.0 (c (n "ligen-cmake-core") (v "0.1.0") (d (list (d (n "ligen") (r "^0.1.13") (d #t) (k 0)))) (h "1rdxs4ymxz1bnqrrm81wny92kgk2wv7w2bryzk978pjfvb9gvh6c")))

(define-public crate-ligen-cmake-core-0.1.1 (c (n "ligen-cmake-core") (v "0.1.1") (d (list (d (n "ligen") (r "^0.1.14") (d #t) (k 0)))) (h "1whgna6590y4nqmvnv8xk8il433cyfg8kp9i8wl1vmn3bviyrzsm")))

(define-public crate-ligen-cmake-core-0.1.2 (c (n "ligen-cmake-core") (v "0.1.2") (d (list (d (n "ligen") (r "^0.1.14") (d #t) (k 0)))) (h "1j1al3x5bic23h91d3r37s8f1fn7n0850paggij22irfz3zgm9lr")))

(define-public crate-ligen-cmake-core-0.1.3 (c (n "ligen-cmake-core") (v "0.1.3") (d (list (d (n "ligen") (r "^0.1.14") (d #t) (k 0)))) (h "1rfgkn2zk0mmmhjhk04yg8vl13hmab33xqcwllqmwz12l82cc43z")))

(define-public crate-ligen-cmake-core-0.1.4 (c (n "ligen-cmake-core") (v "0.1.4") (d (list (d (n "ligen") (r "^0.1.14") (d #t) (k 0)))) (h "1cqqjvffpr3habqdfx915sgfbpfk7g0d87rx5dafvqv43kray51b")))

