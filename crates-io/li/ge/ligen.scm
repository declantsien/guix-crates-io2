(define-module (crates-io li ge ligen) #:use-module (crates-io))

(define-public crate-ligen-0.1.0 (c (n "ligen") (v "0.1.0") (h "0v4bnibla4czy6c2x94zzwwhk35mziqwg3w7z2vciyyjxjq0mk5r")))

(define-public crate-ligen-0.1.4 (c (n "ligen") (v "0.1.4") (d (list (d (n "ligen-core") (r "^0.1.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)))) (h "1srywg4zpkwg92phh4qrgra3fkr4yds3h66xx7smbyrs6d7nmb7l")))

(define-public crate-ligen-0.1.6 (c (n "ligen") (v "0.1.6") (d (list (d (n "ligen-core") (r "^0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)))) (h "07vdhjlj7hhm7m892yfq8i1lajq62w3wqwg780bcc74iv6ml0aza")))

(define-public crate-ligen-0.1.7 (c (n "ligen") (v "0.1.7") (d (list (d (n "ligen-core") (r "^0.1.7") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)))) (h "0fismsbgi4plzqins5b4krld26k86fb99xv3ggkrx3hsc5bkrizj")))

(define-public crate-ligen-0.1.8 (c (n "ligen") (v "0.1.8") (d (list (d (n "ligen-core") (r "^0.1.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)))) (h "0fm4js3x525f6xilsj0j76bqgwicm56sgsyc5imzz6j4hw02fxs2")))

(define-public crate-ligen-0.1.9 (c (n "ligen") (v "0.1.9") (d (list (d (n "ligen-core") (r "^0.1.9-alpha.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)))) (h "1qhkv8nawwjc4081am1xhrc0jal09mxyghydhxsp88vfpvjqvk60")))

(define-public crate-ligen-0.1.10 (c (n "ligen") (v "0.1.10") (d (list (d (n "ligen-core") (r "^0.1.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)))) (h "05qhbsbf10ijk9ziq82vim0gq3x1iqhd45dw2l6b4qll0ii1wd1p")))

(define-public crate-ligen-0.1.11 (c (n "ligen") (v "0.1.11") (d (list (d (n "ligen-core") (r "^0.1.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)))) (h "0inai0775ryykk0h4vip7b4pvidyd3w17a2lzhvnmgbxy0zmdkrk")))

(define-public crate-ligen-0.1.12 (c (n "ligen") (v "0.1.12") (d (list (d (n "ligen-core") (r "^0.1.10") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)))) (h "1prb1x2wh1fs7clk0ykb6jhnk691prykhv6q9jxg19iha3975jic")))

(define-public crate-ligen-0.1.13 (c (n "ligen") (v "0.1.13") (d (list (d (n "ligen-core") (r "^0.1.13") (d #t) (k 0)) (d (n "ligen-macro") (r "^0.1.13") (d #t) (k 0)))) (h "19f6lb3nya7canpaz8ykfpw6p9i7cc7i8jyyp4y3s0wi96vwynci")))

(define-public crate-ligen-0.1.14 (c (n "ligen") (v "0.1.14") (d (list (d (n "ligen-core") (r "^0.1.14") (d #t) (k 0)) (d (n "ligen-macro") (r "^0.1.14") (d #t) (k 0)))) (h "1rjd4wla69sa2c0ygbdl6vgxpkqrba55chlm9qzhdf4ikry9vmab")))

(define-public crate-ligen-0.1.15 (c (n "ligen") (v "0.1.15") (d (list (d (n "ligen-core") (r "=0.1.15") (d #t) (k 0)) (d (n "ligen-macro") (r "=0.1.15") (d #t) (k 0)))) (h "1hmyg0s6m1p49h7xrqnqvjq2qnldiq6b5sjhpn35visrb08ccdqd")))

(define-public crate-ligen-0.1.16 (c (n "ligen") (v "0.1.16") (d (list (d (n "ligen-core") (r "^0.1.16") (d #t) (k 0)) (d (n "ligen-macro") (r "^0.1.16") (d #t) (k 0)))) (h "08jam43r7ak1gmxgzw6wjya8r3v90zwjaagyxh6iqw0ka5qc56mp")))

