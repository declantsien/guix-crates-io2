(define-module (crates-io li ge ligen-cmake-macro) #:use-module (crates-io))

(define-public crate-ligen-cmake-macro-0.1.0 (c (n "ligen-cmake-macro") (v "0.1.0") (d (list (d (n "ligen") (r "^0.1.13") (d #t) (k 0)) (d (n "ligen-cmake-core") (r "^0.1.0") (d #t) (k 0)))) (h "1d69j740x7227ak6lngcmacbvsh3v3j2m3rjwwpxkm9d4g52dn1d")))

(define-public crate-ligen-cmake-macro-0.1.1 (c (n "ligen-cmake-macro") (v "0.1.1") (d (list (d (n "ligen") (r "^0.1.14") (d #t) (k 0)) (d (n "ligen-cmake-core") (r "^0.1.0") (d #t) (k 0)))) (h "1jjlbhwsd80s72sz3q8jkn66rm3wr2rqg1cd875j0yyfp3r5znpi")))

(define-public crate-ligen-cmake-macro-0.1.2 (c (n "ligen-cmake-macro") (v "0.1.2") (d (list (d (n "ligen") (r "^0.1.14") (d #t) (k 0)) (d (n "ligen-cmake-core") (r "^0.1.2") (d #t) (k 0)))) (h "1wqsyh1p8b4pjznpzaws2qd8g4qiq3rk4zs52zw7xfq5gvmp0ii2")))

(define-public crate-ligen-cmake-macro-0.1.3 (c (n "ligen-cmake-macro") (v "0.1.3") (d (list (d (n "ligen") (r "^0.1.14") (d #t) (k 0)) (d (n "ligen-cmake-core") (r "^0.1.2") (d #t) (k 0)))) (h "1wp28jv394idzcf31xnfp41a4vbi8nvicrggzpm7jz9dn4f8hjsl")))

(define-public crate-ligen-cmake-macro-0.1.4 (c (n "ligen-cmake-macro") (v "0.1.4") (d (list (d (n "ligen") (r "^0.1.14") (d #t) (k 0)) (d (n "ligen-cmake-core") (r "^0.1.4") (d #t) (k 0)))) (h "091bhh215r8jh922k3pw2hjknqbwfnh61cz3ar0m2bgd5ja3b969")))

