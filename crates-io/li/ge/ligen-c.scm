(define-module (crates-io li ge ligen-c) #:use-module (crates-io))

(define-public crate-ligen-c-0.1.0 (c (n "ligen-c") (v "0.1.0") (d (list (d (n "ligen-c-core") (r "^0.1.0") (d #t) (k 0)) (d (n "ligen-c-macro") (r "^0.1.0") (d #t) (k 0)))) (h "16z3amgl3rdh8qmvjwrgybpxzf0xgnkzi73zp9p4sjb65m0wjxyv")))

(define-public crate-ligen-c-0.1.2 (c (n "ligen-c") (v "0.1.2") (d (list (d (n "ligen-c-core") (r "^0.1.2") (d #t) (k 0)) (d (n "ligen-c-macro") (r "^0.1.2") (d #t) (k 0)))) (h "0zjrpspklv2ccybwnldx3xbv6ll95fq1zrpd8hb7m3ckivsc5xw7")))

