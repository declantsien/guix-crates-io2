(define-module (crates-io li ge ligen-c-core) #:use-module (crates-io))

(define-public crate-ligen-c-core-0.1.0 (c (n "ligen-c-core") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.9.2") (d #t) (k 0)) (d (n "ligen") (r "^0.1.13") (d #t) (k 0)))) (h "0jnfpszbjiy94aak0w9dssaxk6hl8mj0szlxai62prxgibrl99bx")))

(define-public crate-ligen-c-core-0.1.1 (c (n "ligen-c-core") (v "0.1.1") (d (list (d (n "cargo_toml") (r "^0.9.2") (d #t) (k 0)) (d (n "ligen") (r "^0.1.14") (d #t) (k 0)))) (h "1jwqjz4vyar6iz4j8vj6lr1cy4f2mc77jksrlq57nqq0lzy84r7x")))

(define-public crate-ligen-c-core-0.1.2 (c (n "ligen-c-core") (v "0.1.2") (d (list (d (n "ligen") (r "^0.1.14") (d #t) (k 0)))) (h "11vz9qyj0jhf7ccxgiwmkanywr8d576xk56sx430zd7wmf8knz9w")))

