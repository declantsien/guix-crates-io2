(define-module (crates-io li ge ligen-macro) #:use-module (crates-io))

(define-public crate-ligen-macro-0.1.13 (c (n "ligen-macro") (v "0.1.13") (d (list (d (n "ligen-core") (r "^0.1.13") (d #t) (k 0)))) (h "1navyn5yc2kz9ljggvm7p2wvjzicha09gbqqgcg03jnsgifc79ss")))

(define-public crate-ligen-macro-0.1.14 (c (n "ligen-macro") (v "0.1.14") (d (list (d (n "ligen-core") (r "^0.1.14") (d #t) (k 0)))) (h "0nslg985aqxz8vyjq52qz5vzbzsjkhq5fsa9y6ss4ap2wyg3zyz2")))

(define-public crate-ligen-macro-0.1.15 (c (n "ligen-macro") (v "0.1.15") (d (list (d (n "ligen-core") (r "=0.1.15") (d #t) (k 0)))) (h "0sd4qps3wmwypjywjwfq8kqwbzs5c3ghisnj1p0r1vwn2yz56vad")))

(define-public crate-ligen-macro-0.1.16 (c (n "ligen-macro") (v "0.1.16") (d (list (d (n "ligen-core") (r "^0.1.16") (d #t) (k 0)))) (h "185w0kvcvfn7sqa8nfnlpw3ws6nbckp7j77hm54vldpjawka0xk1")))

