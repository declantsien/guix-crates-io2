(define-module (crates-io li ge ligen-c-macro) #:use-module (crates-io))

(define-public crate-ligen-c-macro-0.1.0 (c (n "ligen-c-macro") (v "0.1.0") (d (list (d (n "ligen") (r "^0.1.13") (d #t) (k 0)) (d (n "ligen-c-core") (r "^0.1.0") (d #t) (k 0)))) (h "1d1srxllawdc6aa3kjm7bhn8yrj5816ayb4sn1imspp82qmpkhcj")))

(define-public crate-ligen-c-macro-0.1.2 (c (n "ligen-c-macro") (v "0.1.2") (d (list (d (n "ligen") (r "^0.1.14") (d #t) (k 0)) (d (n "ligen-c-core") (r "^0.1.2") (d #t) (k 0)))) (h "0648z8gsnf23agqrz1jcvrkrpy8d32zfz9jmafbsvr8lg5ih7kl6")))

