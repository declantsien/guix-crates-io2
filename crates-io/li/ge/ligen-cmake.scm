(define-module (crates-io li ge ligen-cmake) #:use-module (crates-io))

(define-public crate-ligen-cmake-0.1.0 (c (n "ligen-cmake") (v "0.1.0") (d (list (d (n "ligen-cmake-core") (r "^0.1.0") (d #t) (k 0)) (d (n "ligen-cmake-macro") (r "^0.1.0") (d #t) (k 0)))) (h "08kpfx7sy4yd2n6sqsn606w3nr03kfsjy9dmkdln5fprkslmv96s")))

(define-public crate-ligen-cmake-0.1.1 (c (n "ligen-cmake") (v "0.1.1") (d (list (d (n "ligen-cmake-core") (r "^0.1.1") (d #t) (k 0)) (d (n "ligen-cmake-macro") (r "^0.1.1") (d #t) (k 0)))) (h "0jszv35i0k6hc580aqma3h6f6cqzj6zkwfcyy66i4bgdp02w8bc5")))

(define-public crate-ligen-cmake-0.1.2 (c (n "ligen-cmake") (v "0.1.2") (d (list (d (n "ligen-cmake-core") (r "^0.1.2") (d #t) (k 0)) (d (n "ligen-cmake-macro") (r "^0.1.2") (d #t) (k 0)))) (h "1laxbgykyfnawzh5h6b209ahvz71n9mh9kn2g3xf2607wqnxnxr6")))

(define-public crate-ligen-cmake-0.1.3 (c (n "ligen-cmake") (v "0.1.3") (d (list (d (n "ligen-cmake-core") (r "^0.1.2") (d #t) (k 0)) (d (n "ligen-cmake-macro") (r "^0.1.2") (d #t) (k 0)))) (h "1cgvwhqswxnzzcsncfbmna6ni7926saaiqb2rb86p7qv5w93jbgb")))

(define-public crate-ligen-cmake-0.1.4 (c (n "ligen-cmake") (v "0.1.4") (d (list (d (n "ligen-cmake-core") (r "^0.1.4") (d #t) (k 0)) (d (n "ligen-cmake-macro") (r "^0.1.4") (d #t) (k 0)))) (h "1f29b345px58lyf0l5lx22d7mcg9cjifjihrphp00fl6m04lrbyp")))

