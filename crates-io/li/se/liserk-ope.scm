(define-module (crates-io li se liserk-ope) #:use-module (crates-io))

(define-public crate-liserk-ope-0.1.0 (c (n "liserk-ope") (v "0.1.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (k 0)))) (h "02p3jjjqmfywlflq06win0v1wrxfima4bmf3hh8xshqyhvwz0ra2")))

(define-public crate-liserk-ope-0.1.1 (c (n "liserk-ope") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (k 0)))) (h "02r4vr0sncbcv50nkzik0s8ncnivi71l1hbmfzjk4i9s6q5195af")))

(define-public crate-liserk-ope-0.2.0 (c (n "liserk-ope") (v "0.2.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 0)) (d (n "block-modes") (r "^0.9.1") (d #t) (k 0)) (d (n "cipher") (r "^0.4.4") (d #t) (k 0)) (d (n "ctr") (r "^0.9.2") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "openssl") (r "^0.10.55") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rug") (r "^1.19.2") (d #t) (k 0)) (d (n "sha2") (r "^0.10.7") (d #t) (k 0)))) (h "015bbvlyfb7hdqm9rypk2rdsnlvsdn2zyaadpqmiwi2xyqqppici")))

