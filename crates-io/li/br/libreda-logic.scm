(define-module (crates-io li br libreda-logic) #:use-module (crates-io))

(define-public crate-libreda-logic-0.0.2 (c (n "libreda-logic") (v "0.0.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fnv") (r "^1.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)))) (h "0f0pri1l2q08fwnc30nnm3rz0ay9lhawvfvqshc8i2b1jd2bzsm4")))

