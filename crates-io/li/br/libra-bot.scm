(define-module (crates-io li br libra-bot) #:use-module (crates-io))

(define-public crate-libra-bot-0.1.0 (c (n "libra-bot") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serenity") (r "^0.8") (d #t) (k 0)))) (h "00c2jwssnlnrzz6yla250mznd6xwm5fx21q28110r6gyplkj1xab")))

(define-public crate-libra-bot-0.1.1 (c (n "libra-bot") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serenity") (r "^0.8") (d #t) (k 0)))) (h "0ynigx8v1s67k2lk47rgdxkk1r9f3xg62yvqg40wlxlwn8s2q2ad")))

(define-public crate-libra-bot-0.1.2 (c (n "libra-bot") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1k519q6wp8s1y4a5li28xfsp6wx0sprpdjy8f6ywc0pxn1frpw10")))

(define-public crate-libra-bot-0.1.3 (c (n "libra-bot") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "158s41qz55ca3ps09yf7p0vfynbrawhrl1vai0wj57a68dddhibl")))

(define-public crate-libra-bot-0.1.4 (c (n "libra-bot") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "0h1ir3w842pw89nsydcpk2f5c6q7p2bqrybn1yxy2yi9dfdhxab5")))

(define-public crate-libra-bot-0.1.5 (c (n "libra-bot") (v "0.1.5") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1q8cdnkwzm6vifcki9qmfdwy6dwihx50cfnxgrlf3lb5rx6v01jb")))

(define-public crate-libra-bot-0.1.6 (c (n "libra-bot") (v "0.1.6") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serenity") (r "^0.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1rfzaylz4g9n94mvjypzl8j83h81fqqhx3d4bp1791mlrjr5cpdv")))

(define-public crate-libra-bot-0.1.7 (c (n "libra-bot") (v "0.1.7") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 0)) (d (n "kankyo") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("small_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serenity") (r "^0.8") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6") (d #t) (k 0)))) (h "1izcfy5799j1jp7bbcqbvv1cs5jvp9x1i6khkrqf52h07ihl43hr")))

