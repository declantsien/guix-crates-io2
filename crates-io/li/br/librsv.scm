(define-module (crates-io li br librsv) #:use-module (crates-io))

(define-public crate-librsv-0.1.0 (c (n "librsv") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "00vjldcl8czf2scmrnvh0cjhg5w4ppxirc1vg9g765f11wl1yniw")))

(define-public crate-librsv-0.1.1 (c (n "librsv") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)))) (h "0rq9nwwxfy7m0cdndz3flhkp35daklrjv7fygx68c2nc0r73xhzl")))

