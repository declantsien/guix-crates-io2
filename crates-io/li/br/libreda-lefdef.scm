(define-module (crates-io li br libreda-lefdef) #:use-module (crates-io))

(define-public crate-libreda-lefdef-0.0.2 (c (n "libreda-lefdef") (v "0.0.2") (d (list (d (n "byteorder") (r "1.3.*") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "libreda-db") (r "^0.0.9") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1cls2p7k43a9zbzd15njad883ikcdy00ygg8zwk2a46r4w286cl7")))

