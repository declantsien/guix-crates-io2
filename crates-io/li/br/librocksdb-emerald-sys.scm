(define-module (crates-io li br librocksdb-emerald-sys) #:use-module (crates-io))

(define-public crate-librocksdb-emerald-sys-1.0.0 (c (n "librocksdb-emerald-sys") (v "1.0.0") (d (list (d (n "const-cstr") (r "^0.2") (d #t) (k 2)) (d (n "gcc") (r "^0.3") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "1r74y176f9r7vmrfik0z777l4kh2jnl7n11hw4kxbqqap46niyqq") (f (quote (("static") ("default" "static"))))))

