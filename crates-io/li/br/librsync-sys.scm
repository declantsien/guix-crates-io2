(define-module (crates-io li br librsync-sys) #:use-module (crates-io))

(define-public crate-librsync-sys-0.1.0 (c (n "librsync-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1hxgdqxg09rpvsjv7xlbqjz87rmcxx5m5898366l5aj586hjpa32")))

(define-public crate-librsync-sys-0.1.1 (c (n "librsync-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16rzpbhq8vhsf6zghd5m8lzxbz1rrpmdzjck5b641mp3s8j6p4mx")))

(define-public crate-librsync-sys-0.1.2 (c (n "librsync-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1m7jvjyl7pks6c490pn3dd0j545w1z1z5j71x6b7c7ylsi8h4has")))

