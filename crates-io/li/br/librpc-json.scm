(define-module (crates-io li br librpc-json) #:use-module (crates-io))

(define-public crate-librpc-json-0.1.2 (c (n "librpc-json") (v "0.1.2") (d (list (d (n "async-timer-rs") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("async_futures" "html_reports"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 2)) (d (n "librpc") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0s3lrhc49bmqmv2vnzdxz6ppd96gbrfip5v8wkm5w5v60n862hlz")))

