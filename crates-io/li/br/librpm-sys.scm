(define-module (crates-io li br librpm-sys) #:use-module (crates-io))

(define-public crate-librpm-sys-0.0.1 (c (n "librpm-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "06r896p2l2cgc49y1v8rxv6wyhkz1651i34vjb70b8f5wm6di8k7") (y #t)))

(define-public crate-librpm-sys-0.0.2 (c (n "librpm-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "0mgr2g2p241aahbhl9vwq4bg3xrpdyyak364xfk65f2r47n5r1pl") (y #t)))

(define-public crate-librpm-sys-0.0.3 (c (n "librpm-sys") (v "0.0.3") (h "191gxdbvfzmk3mi3a9cv9nhabvmaxzp7ky3zr25dbjbhk2c8a1wh") (y #t)))

(define-public crate-librpm-sys-0.1.0 (c (n "librpm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "1bg0p41hdl0jcshmx72fbnmzw4v91dzhkf4zk5jbww54a4lpkyq7")))

(define-public crate-librpm-sys-0.1.1 (c (n "librpm-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "1bs70zkqpwiz7z0yryssrls3rb6gbxcbqa7y7sp90js8j03ygf3b")))

