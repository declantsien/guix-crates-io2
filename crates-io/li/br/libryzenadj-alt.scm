(define-module (crates-io li br libryzenadj-alt) #:use-module (crates-io))

(define-public crate-libryzenadj-alt-0.14.0 (c (n "libryzenadj-alt") (v "0.14.0") (d (list (d (n "errno") (r "^0.3.2") (d #t) (k 0)) (d (n "libryzenadj-sys-alt") (r "^0.14.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 2)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "12qzqydxxpaar81044vzkrfk00rvkagxs57h5r68cyzplxkcra7h")))

