(define-module (crates-io li br librist-rust) #:use-module (crates-io))

(define-public crate-librist-rust-0.1.0 (c (n "librist-rust") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librist-sys") (r "^0.3") (d #t) (k 0)))) (h "0vsc299agslgd5n5d08873fx3zmjj1xkwj81xx2gy2002hmn49ii")))

(define-public crate-librist-rust-0.2.0 (c (n "librist-rust") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librist-sys") (r "^0.4.0") (d #t) (k 0)))) (h "0ik9qp8d7274my0ji74m7bwdzyv43b533i8nwn4j5wjx5117pbjr")))

(define-public crate-librist-rust-0.3.0 (c (n "librist-rust") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librist-sys") (r "^0.5") (d #t) (k 0)))) (h "0a03hd73453w3r3akj92zc00dijdkf4iafpnf98w8lg5rnj8mdlr")))

(define-public crate-librist-rust-0.4.0 (c (n "librist-rust") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librist-sys") (r "^0.6") (d #t) (k 0)))) (h "07y3hk1671vdg7z4fzc7yk5vi4ka9bc05a26i5iblh7ll3m8lbya")))

(define-public crate-librist-rust-0.5.0 (c (n "librist-rust") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librist-sys") (r "^0.7") (d #t) (k 0)))) (h "16lv8hfm4y943dkdc82mya9lzh75ilq088qzpsxks542wgjd3dxw")))

(define-public crate-librist-rust-0.6.0 (c (n "librist-rust") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librist-sys") (r "^0.8") (d #t) (k 0)))) (h "0ivpfwlsgn3rikxrf1c99nlcwzli2xp5fn87imp33b76pyrzx885")))

(define-public crate-librist-rust-0.6.1 (c (n "librist-rust") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librist-sys") (r "^0.8.1") (d #t) (k 0)))) (h "1wmfrz0daadi6h5rdlji6mapmgahddw6c1lnzzxg5q1nc7f0j6wd")))

(define-public crate-librist-rust-0.6.2 (c (n "librist-rust") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librist-sys") (r "^0.8.2") (d #t) (k 0)))) (h "11avqrlfpqq7ivf20q9pzkghqkp5y4pz8cf02xsdi6ynqglz6pi1")))

(define-public crate-librist-rust-0.6.3 (c (n "librist-rust") (v "0.6.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "hexdump") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librist-sys") (r "^0.8.3") (d #t) (k 0)))) (h "1jksgadarpd1smhgxzr6jib9b6aiqbjyzpnljbiyj705barmznbv")))

