(define-module (crates-io li br librustconfig) #:use-module (crates-io))

(define-public crate-librustconfig-0.1.0 (c (n "librustconfig") (v "0.1.0") (d (list (d (n "libconfig-sys") (r "^0.1") (d #t) (k 0)))) (h "098rs1v0y5v5iv5l18grx5s3ga4lqcmhhsh7mgv5xvn8iklkx27h")))

(define-public crate-librustconfig-0.1.1 (c (n "librustconfig") (v "0.1.1") (d (list (d (n "libconfig-sys") (r "^0.1") (d #t) (k 0)))) (h "0gn4qmhf6zd80hp4vzyba7qgm3d0akns1p1ia1f4p6wsrn5i6ddi")))

