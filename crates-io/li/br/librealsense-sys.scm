(define-module (crates-io li br librealsense-sys) #:use-module (crates-io))

(define-public crate-librealsense-sys-0.1.0 (c (n "librealsense-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)))) (h "0apph1v49ibays00fbm1scc89vzcw2dv1ls8lj7c8zi16iraldnl")))

(define-public crate-librealsense-sys-0.1.1 (c (n "librealsense-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)))) (h "1w52qin7n6hf6k32h0y0dy3fshp9hyhm5yc9146zqjdsnwyxid86")))

(define-public crate-librealsense-sys-0.2.0 (c (n "librealsense-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.23") (d #t) (k 1)))) (h "0kchh81xhkv5g6cgjjakxf0vl1b0h6fr2q7r5xwi37b4p5dsjji4")))

