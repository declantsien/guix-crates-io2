(define-module (crates-io li br librpm) #:use-module (crates-io))

(define-public crate-librpm-0.1.0 (c (n "librpm") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "librpm-sys") (r "^0.1") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)))) (h "0yiyf93g9xzqg3r2lwm8lnyz7gir32a0j0x03xwg64jjzbsrjz24")))

(define-public crate-librpm-0.1.1 (c (n "librpm") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "librpm-sys") (r "^0.1") (d #t) (k 0)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)))) (h "0vqc9w0bbh8xpfna0p52vhdhkj0k4rlmlyrss7djg38jgilrswqc")))

