(define-module (crates-io li br libribzip2) #:use-module (crates-io))

(define-public crate-libribzip2-0.3.0 (c (n "libribzip2") (v "0.3.0") (h "14jxvg3ph67718ncpmgkcvrj83jqhnrx9p54wz6d8hzylzgi59dk")))

(define-public crate-libribzip2-0.3.2 (c (n "libribzip2") (v "0.3.2") (h "02acp85wc4i2p9ngw86d1ywrj62ws58pmfc6wj9byds96vqm1x8s")))

(define-public crate-libribzip2-0.4.0 (c (n "libribzip2") (v "0.4.0") (h "02jkhyqxfkgklzqhy6c1zd87915msv4076qgbx5k716lc3m1jvar")))

(define-public crate-libribzip2-0.5.0 (c (n "libribzip2") (v "0.5.0") (h "1kgwv10g2k2sxbqsv42aigwdsw7xvav47612jmfh3gfis4ga5558")))

