(define-module (crates-io li br libra-canonical-serialization) #:use-module (crates-io))

(define-public crate-libra-canonical-serialization-0.0.0 (c (n "libra-canonical-serialization") (v "0.0.0") (h "1x70mgg2ldllq4wlflg1j5dh4rqlgxxn02a0xrx430i5zm438w70")))

(define-public crate-libra-canonical-serialization-0.1.0 (c (n "libra-canonical-serialization") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "proptest") (r "^0.10.1") (d #t) (k 2)) (d (n "proptest-derive") (r "^0.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1w91sqrw8b2n9pcmiaxqs1bkp6vp434l09s120r9d7lk63ddv3bx")))

