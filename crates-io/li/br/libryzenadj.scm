(define-module (crates-io li br libryzenadj) #:use-module (crates-io))

(define-public crate-libryzenadj-0.11.1 (c (n "libryzenadj") (v "0.11.1") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libryzenadj-sys") (r "^0.11.1") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1c9bar1ds7q0bhcd9hsl0qwn7z7qapflx9qc6ay49zxpsyvcm11m")))

(define-public crate-libryzenadj-0.11.2 (c (n "libryzenadj") (v "0.11.2") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libryzenadj-sys") (r "^0.11.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "16pnijp3nqif2r9ypqlnfndyyzgyn1qqbylzrz3jvnlw3f5sarmz")))

(define-public crate-libryzenadj-0.12.0 (c (n "libryzenadj") (v "0.12.0") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libryzenadj-sys") (r "^0.11.2") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1xsmq7z1plwc1jgh8yrr0f3rs8bbx7i2f0cj2fg51nqvp8mii58g")))

(define-public crate-libryzenadj-0.12.1 (c (n "libryzenadj") (v "0.12.1") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libryzenadj-sys") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "1nd2k25s82jdqgl2cskpvqgqhzbyqg0gb01kikjqnrjxww1jmmpg")))

(define-public crate-libryzenadj-0.12.2 (c (n "libryzenadj") (v "0.12.2") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libryzenadj-sys") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0n2mvynwl42sivccx7ygbpxn74cqb01p8dykw24rvs4kgj1vlx32")))

(define-public crate-libryzenadj-0.12.3 (c (n "libryzenadj") (v "0.12.3") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "libryzenadj-sys") (r "^0.12.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 2)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0wgr5aqhp1hbmsphi7sbnp5lrs4cjhsbf9dq84v9wwphpcd5f1d8")))

(define-public crate-libryzenadj-0.13.0 (c (n "libryzenadj") (v "0.13.0") (d (list (d (n "errno") (r "^0.3.2") (d #t) (k 0)) (d (n "libryzenadj-sys") (r "^0.13.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13.1") (d #t) (k 2)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.35") (d #t) (k 0)))) (h "0cpw1qrv4ky2z39bhfz041nzfsdkihysaj2n8dnc0d1jgkqcvg65")))

