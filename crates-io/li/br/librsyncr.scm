(define-module (crates-io li br librsyncr) #:use-module (crates-io))

(define-public crate-librsyncr-0.1.0 (c (n "librsyncr") (v "0.1.0") (d (list (d (n "blake2-rfc") (r "^0.2.18") (f (quote ("simd" "simd_opt"))) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.7") (d #t) (k 2)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)))) (h "0kv2l94xcqj3h3viy8wry4q0grn4xnhry8yar4a71fadya9v0583") (f (quote (("strong_hash"))))))

(define-public crate-librsyncr-0.1.1 (c (n "librsyncr") (v "0.1.1") (d (list (d (n "blake2-rfc") (r "^0.2.18") (f (quote ("simd" "simd_opt"))) (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "gumdrop") (r "^0.7") (d #t) (k 2)) (d (n "rustc-hash") (r "^1") (d #t) (k 0)))) (h "0kvn50241z5hh7b7xg5yppb0k6i3p47abx739apvn1b7db440f3b") (f (quote (("strong_hash"))))))

