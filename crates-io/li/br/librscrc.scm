(define-module (crates-io li br librscrc) #:use-module (crates-io))

(define-public crate-librscrc-0.1.0 (c (n "librscrc") (v "0.1.0") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)))) (h "15z6il7gz97hyk20n6aa8cb8y47xp01p5xlvnkiqk2h4v1sn28kc") (f (quote (("nightly") ("hardware") ("default" "hardware") ("all" "hardware" "nightly"))))))

