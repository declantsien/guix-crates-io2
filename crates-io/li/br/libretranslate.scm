(define-module (crates-io li br libretranslate) #:use-module (crates-io))

(define-public crate-libretranslate-0.1.0 (c (n "libretranslate") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1lh99jh808q58xgphbvizbj0xv732bdhnmlxr52xqcqvygwwbwpf")))

(define-public crate-libretranslate-0.1.1 (c (n "libretranslate") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "03y6k41yzkncs249jz2fanhy90prq3pxlwhgh9kz8sq108gaayzf")))

(define-public crate-libretranslate-0.1.2 (c (n "libretranslate") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "06586by1ahbixc3ikz80g1xqr1zh4p5lx8r1b56xxz3ryyiny4rb")))

(define-public crate-libretranslate-0.1.3 (c (n "libretranslate") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1fdg788nhz9218pryqr2i7mh1jm6yx0dj8mn1gw3gg8hx8d802zp")))

(define-public crate-libretranslate-0.1.4 (c (n "libretranslate") (v "0.1.4") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1dp1mhfg43s34ifibqh43l9gzw4rna270420fm0pmi287djgwq5l")))

(define-public crate-libretranslate-0.1.5 (c (n "libretranslate") (v "0.1.5") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1gj39xs07w1wkq836n3l8nlk0wp2jalmn8mz1is01n64yhizd10y")))

(define-public crate-libretranslate-0.1.6 (c (n "libretranslate") (v "0.1.6") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0j28i919v3s24fzcqglsp6ybm8k5x2i5080jc3051z1l185s1cgg")))

(define-public crate-libretranslate-0.1.7 (c (n "libretranslate") (v "0.1.7") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "1bzfrcjpvhfy3598qb2g95cb5h1wi8lvk50avjlx38i46nynkbm7")))

(define-public crate-libretranslate-0.1.8 (c (n "libretranslate") (v "0.1.8") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0wa659cnyrkn6p8zh07ybwqh474gm5y6bkx5wpfwdxlhww88zcxh")))

(define-public crate-libretranslate-0.1.9 (c (n "libretranslate") (v "0.1.9") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "112y64i8hvlgkrhczlmxg43m8mzqk07h7lds0cwmnqdpywsls762")))

(define-public crate-libretranslate-0.2.0 (c (n "libretranslate") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)))) (h "0wcm2q2dw5hj11d21d0vz4ajj81w1w5lr2cjqx8az592cdwd90jm")))

(define-public crate-libretranslate-0.2.1 (c (n "libretranslate") (v "0.2.1") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "whatlang") (r "^0.11.1") (d #t) (k 0)))) (h "0sjrb96p22c853symghp9rb8kqp3f1vh747pi8xn7lxswxsddv1a")))

(define-public crate-libretranslate-0.2.2 (c (n "libretranslate") (v "0.2.2") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "whatlang") (r "^0.11.1") (d #t) (k 0)))) (h "044j8hx782m2rhfdh9r235hp784l5mg2znd1py0cx6xknj31dwlm")))

(define-public crate-libretranslate-0.2.3 (c (n "libretranslate") (v "0.2.3") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "whatlang") (r "^0.11.1") (d #t) (k 0)))) (h "0b41maknlc72wc4zr5b1iiaqdz2yb4n9c66nnnyr0yvwhhijmfh3")))

(define-public crate-libretranslate-0.2.4 (c (n "libretranslate") (v "0.2.4") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "ureq") (r "^2.0.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "whatlang") (r "^0.11.1") (d #t) (k 0)))) (h "1zl0nrphsz7bxlrmrdbm2p0z2bngr97sk3xldxsnhz3yimgqsf1b")))

(define-public crate-libretranslate-0.2.5 (c (n "libretranslate") (v "0.2.5") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "whatlang") (r "^0.11.1") (d #t) (k 0)))) (h "0va4hmdybc23ws0gqkfl3jfsa034bpzjkn6v406922nyvfj287x5")))

(define-public crate-libretranslate-0.2.6 (c (n "libretranslate") (v "0.2.6") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "whatlang") (r "^0.11.1") (d #t) (k 0)))) (h "0iv8ifz4azsl1abs06y9s6skd0sn3mwh50mcnypdwspvs41804b7")))

(define-public crate-libretranslate-0.2.7 (c (n "libretranslate") (v "0.2.7") (d (list (d (n "futures") (r "^0.3.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "whatlang") (r "^0.11.1") (d #t) (k 0)))) (h "0a3xpw07z6rcl7nhw5vqyw3ph4py027y5azql8lv1lvi3axzvg80")))

(define-public crate-libretranslate-0.2.8 (c (n "libretranslate") (v "0.2.8") (d (list (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "whatlang") (r "^0.12.0") (d #t) (k 0)))) (h "1a5zg7248ps36qpzjmhnd9yksxswxgbcijraqm40bdqsji46g5rm")))

(define-public crate-libretranslate-0.2.9 (c (n "libretranslate") (v "0.2.9") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1372iy4lw5p9x29cx1bq7qjymiqzb0nczgx7ab64cpjsj5crc6ik")))

(define-public crate-libretranslate-0.3.0 (c (n "libretranslate") (v "0.3.0") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)))) (h "15kmdllfr5i60wxvdyrnp2hz4k44zh0qqqkhdcgs2vy1z32an422")))

(define-public crate-libretranslate-0.3.1 (c (n "libretranslate") (v "0.3.1") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1bf5qqs35n1arcdr8x4kwkqiy3ihfck7jyprv6ad8lis01b26nf8")))

(define-public crate-libretranslate-0.4.0 (c (n "libretranslate") (v "0.4.0") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1ligmf2yw14603grqi21shb8npn3ph0v4v48qra0cvml9y39fl1j")))

(define-public crate-libretranslate-0.4.1 (c (n "libretranslate") (v "0.4.1") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "unic-langid") (r "^0.9.0") (d #t) (k 0)))) (h "0d6rb2j86h2wlf2s4d3cs91r78v8wxaizbqdn3rjl4nky4kj8b66")))

(define-public crate-libretranslate-0.5.0 (c (n "libretranslate") (v "0.5.0") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "unic-langid") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0876yl0rhsbfx0kv8wqyjvmy5mp568kc4wbacry0w8myppg3996m") (f (quote (("unicode_langid" "unic-langid"))))))

(define-public crate-libretranslate-0.5.1 (c (n "libretranslate") (v "0.5.1") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "unic-langid") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "03ncz5r7frdp0y60i5xn95hwhr6sc4pfcjdm2a7zxm5w6sag7jf0") (f (quote (("unicode_langid" "unic-langid"))))))

(define-public crate-libretranslate-0.5.2 (c (n "libretranslate") (v "0.5.2") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "surf") (r "^2.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "unic-langid") (r "^0.9.0") (o #t) (d #t) (k 0)))) (h "0ixqqhphcnl05ff05hm9k9kj81aqjh3ld25ghz7giy0xpkvcx0fy") (f (quote (("unicode_langid" "unic-langid"))))))

