(define-module (crates-io li br librpmsign-sys) #:use-module (crates-io))

(define-public crate-librpmsign-sys-0.1.0 (c (n "librpmsign-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "0gk5asz94aia480r2gkp3shg3r7bihc6bnyaq4zgji1yb5b57swp")))

(define-public crate-librpmsign-sys-0.1.1 (c (n "librpmsign-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "1jdlz8kj890qcl4p2cxj207x607qbk74l8xb22bwx2sjrf1mz4b0")))

