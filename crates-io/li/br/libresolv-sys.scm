(define-module (crates-io li br libresolv-sys) #:use-module (crates-io))

(define-public crate-libresolv-sys-0.1.0 (c (n "libresolv-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j4xb0fir76scmiip9y4r2d4k71bjz4298sk7sv9fyh9x4b74zpb")))

(define-public crate-libresolv-sys-0.2.0 (c (n "libresolv-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1clyn1x4qf69drf7zkxsnwrqcisdy5af4rma953p78nvx83d5x0z") (l "resolv")))

(define-public crate-libresolv-sys-0.3.0 (c (n "libresolv-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pip7jlcraq50dl7vhq0sfy0njmlz7zmd1sjvyfaswyyzc0hm01h") (l "resolv")))

