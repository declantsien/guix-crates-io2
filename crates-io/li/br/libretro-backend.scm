(define-module (crates-io li br libretro-backend) #:use-module (crates-io))

(define-public crate-libretro-backend-0.1.0 (c (n "libretro-backend") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libretro-sys") (r "^0.1") (d #t) (k 0)))) (h "04id6dvvzb3jd0r3mjf6lfqxfdk4g91k82sg7x3mi64admvgcsiw")))

(define-public crate-libretro-backend-0.2.0 (c (n "libretro-backend") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libretro-sys") (r "^0.1") (d #t) (k 0)))) (h "194fz72hdiym1kibacgb8akndcnvca2bq7fizwsfs0c4vpsr48lx")))

(define-public crate-libretro-backend-0.2.1 (c (n "libretro-backend") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libretro-sys") (r "^0.1") (d #t) (k 0)))) (h "0vjxx3vc59w22i1kb66gn20lh9jigg6bfgh2i2yjki7fznk72yk6")))

