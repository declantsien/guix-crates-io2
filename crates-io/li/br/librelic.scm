(define-module (crates-io li br librelic) #:use-module (crates-io))

(define-public crate-librelic-0.0.11 (c (n "librelic") (v "0.0.11") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "fungus") (r "^0.1.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "skellige") (r "^0.1.7") (d #t) (k 0)))) (h "0y3wn1ihb1ac804nx3xnmrdyxjc81njwfwamnb3d87860hp47klm")))

(define-public crate-librelic-0.0.14 (c (n "librelic") (v "0.0.14") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 1)) (d (n "fungus") (r "^0.1.19") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "skellige") (r "^0.1.7") (d #t) (k 0)))) (h "1hkq2c7063l3hs33843868nj3xnp6h6vaxhs76nzrf6j0ghimjd5")))

