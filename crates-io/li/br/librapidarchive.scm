(define-module (crates-io li br librapidarchive) #:use-module (crates-io))

(define-public crate-librapidarchive-0.2.0 (c (n "librapidarchive") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(unix)") (k 0)) (d (n "nix") (r "^0.10.0") (d #t) (t "cfg(unix)") (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "pad") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.6.4") (d #t) (k 2)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winbase" "handleapi" "winerror" "aclapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "13gxxkbjgglsg1fk4nywhdrsiim46axcbijjan4gpcf42klp6qm1")))

