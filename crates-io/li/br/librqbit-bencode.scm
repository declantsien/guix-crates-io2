(define-module (crates-io li br librqbit-bencode) #:use-module (crates-io))

(define-public crate-librqbit-bencode-2.2.1 (c (n "librqbit-bencode") (v "2.2.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "buffers") (r "^2.2.1") (d #t) (k 0) (p "librqbit-buffers")) (d (n "clone_to_owned") (r "^2.2.1") (d #t) (k 0) (p "librqbit-clone-to-owned")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha1w") (r "^2.2.1") (k 0) (p "librqbit-sha1-wrapper")))) (h "14l0ibxrwi3sdzvid97sy8cl5wfyn79mwbz6p8zpbnld8rila895") (f (quote (("sha1-system" "sha1w/sha1-system") ("sha1-rust" "sha1w/sha1-rust") ("sha1-openssl" "sha1w/sha1-openssl") ("default" "sha1-system"))))))

(define-public crate-librqbit-bencode-2.2.2 (c (n "librqbit-bencode") (v "2.2.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "buffers") (r "^3.0.0") (d #t) (k 0) (p "librqbit-buffers")) (d (n "clone_to_owned") (r "^2.2.1") (d #t) (k 0) (p "librqbit-clone-to-owned")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha1w") (r "^3.0.0") (k 0) (p "librqbit-sha1-wrapper")))) (h "0hprbbngz3k2hfbnj18lp10il0ycz7wlbi39bschls5rv11jy2bq")))

