(define-module (crates-io li br librqbit-buffers) #:use-module (crates-io))

(define-public crate-librqbit-buffers-2.2.1 (c (n "librqbit-buffers") (v "2.2.1") (d (list (d (n "clone_to_owned") (r "^2.2.1") (d #t) (k 0) (p "librqbit-clone-to-owned")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "004szx2b1pkxqv07v0wg8mlpa5kj94a3k3jmy3nwkpll94mw339p")))

(define-public crate-librqbit-buffers-3.0.0 (c (n "librqbit-buffers") (v "3.0.0") (d (list (d (n "clone_to_owned") (r "^2.2.1") (d #t) (k 0) (p "librqbit-clone-to-owned")) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ghqg4d3sfdff8mkpbl9bbn1s3hgi50jnnc1h3633fr0g5g5pi05")))

