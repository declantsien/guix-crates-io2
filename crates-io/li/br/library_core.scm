(define-module (crates-io li br library_core) #:use-module (crates-io))

(define-public crate-library_core-0.1.0 (c (n "library_core") (v "0.1.0") (h "027fp5y516k1hc3bmpbm4qcrzxl5wxhxmnwg4nqwj1p8k73w1glj")))

(define-public crate-library_core-0.1.1 (c (n "library_core") (v "0.1.1") (h "055g25awg425qxf14s8c494cn24p0544xknq2w73x2wgimndkwaa")))

(define-public crate-library_core-0.1.2 (c (n "library_core") (v "0.1.2") (h "0hdbkb25n7645pynfjddyamf7pl8xvqmpm3iclbdbrp28vwlfywc")))

