(define-module (crates-io li br libredox) #:use-module (crates-io))

(define-public crate-libredox-0.0.1 (c (n "libredox") (v "0.0.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.4") (d #t) (k 0)))) (h "1s2fh4ikpp9xl0lsl01pi0n8pw1q9s3ld452vd8qh1v63v537j45") (f (quote (("scheme" "call") ("default" "scheme" "call") ("call"))))))

(define-public crate-libredox-0.0.2 (c (n "libredox") (v "0.0.2") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.4") (d #t) (k 0)))) (h "01v6pb09j7dl2gnbvzz6zmy2k4zyxjjzvl7wacwjjffqsxajry9s") (f (quote (("scheme" "call") ("default" "call") ("call"))))))

(define-public crate-libredox-0.0.3 (c (n "libredox") (v "0.0.3") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.4") (d #t) (k 0)))) (h "0nmb9j3bgzfd88f2pvx9kgfrifjw1x74dqckqk79akwkdc76aawn") (f (quote (("scheme" "call") ("default" "call") ("call"))))))

(define-public crate-libredox-0.0.4 (c (n "libredox") (v "0.0.4") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.4") (d #t) (k 0)))) (h "1prric705g2wh7d0jl40jcqnb0nsskq6mhb2qpck8d6v6hx23dc6") (f (quote (("std") ("redox_syscall_exports") ("default" "call" "std" "redox_syscall_exports") ("call"))))))

(define-public crate-libredox-0.1.0 (c (n "libredox") (v "0.1.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0h34fndqwlgbvmhn0zb8gbqlas1sx12wrlvfbf9f2s6ggkkxp2hk") (f (quote (("std") ("default" "call" "std" "redox_syscall") ("call"))))))

(define-public crate-libredox-0.1.1 (c (n "libredox") (v "0.1.1") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.4") (o #t) (d #t) (k 0)))) (h "1pyvxk7k2mghhkz96dxnqncp7g48c8v084wmy03psjb2qnkdcx9l") (f (quote (("std") ("default" "call" "std" "redox_syscall") ("call"))))))

(define-public crate-libredox-0.1.2 (c (n "libredox") (v "0.1.2") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.5") (o #t) (d #t) (k 0)))) (h "042fys6pknx2zq6j2nzd196pj80dhlrvp3xyxd90h5ymhncsd6rh") (f (quote (("std") ("default" "call" "std" "redox_syscall") ("call"))))))

(define-public crate-libredox-0.1.3 (c (n "libredox") (v "0.1.3") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "ioslice") (r "^0.6") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "redox_syscall") (r "^0.5") (o #t) (d #t) (k 0)))) (h "139602gzgs0k91zb7dvgj1qh4ynb8g1lbxsswdim18hcb6ykgzy0") (f (quote (("std") ("mkns" "ioslice") ("default" "call" "std" "redox_syscall") ("call"))))))

