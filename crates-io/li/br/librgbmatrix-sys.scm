(define-module (crates-io li br librgbmatrix-sys) #:use-module (crates-io))

(define-public crate-librgbmatrix-sys-0.1.0 (c (n "librgbmatrix-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0gjvmlhhwki8xyv4p14b5ynchk9d7n89yz2wnrs1ibqrar0nbdir")))

(define-public crate-librgbmatrix-sys-0.2.0 (c (n "librgbmatrix-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1a7fznjwip4myn339czmj5rnbqqbp2j32qyjblbg6jjc02py2nar") (y #t)))

(define-public crate-librgbmatrix-sys-0.2.1 (c (n "librgbmatrix-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0fbq7kpkfzz5hhb9njlqd03gf3pkfl96gxj2pqd56y610q1s6rvz")))

(define-public crate-librgbmatrix-sys-0.3.0 (c (n "librgbmatrix-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0dlr2j2nq0ix9khxw1ybi600nxkqvibf3ly28hk0hwibdyy6bp11")))

(define-public crate-librgbmatrix-sys-0.4.0 (c (n "librgbmatrix-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1zfh0360fjdbvj4x8n3a6pyai2g35rdkvsh42z5pshsjjp743c39")))

(define-public crate-librgbmatrix-sys-0.5.0-6-g7343f38 (c (n "librgbmatrix-sys") (v "0.5.0-6-g7343f38") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "16k952fbb2n164b8g7n79n9km9i27v8x9l41q6g2xj1awwq137zz") (y #t)))

(define-public crate-librgbmatrix-sys-0.5.0 (c (n "librgbmatrix-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0d69jhx2k0b9gbgjn7hvcw236p9x8k8f1q29vc8w6jshl1wvzbbz")))

(define-public crate-librgbmatrix-sys-0.6.0 (c (n "librgbmatrix-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0lh2l7b0d4ypcpgp5df6ha3bnz40jpah21z9p9i6lima7v056g91")))

(define-public crate-librgbmatrix-sys-0.7.0 (c (n "librgbmatrix-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "17ban8qxm47n36wcn76bgsvw71aywh4b2fy3zqwpfy695qmq9sy3")))

(define-public crate-librgbmatrix-sys-0.8.0 (c (n "librgbmatrix-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1y47qxx0fk7p3qb2kqpd3a9bpj68siha09llkp593xhdl1snvrps")))

(define-public crate-librgbmatrix-sys-0.9.0 (c (n "librgbmatrix-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0qnp72asppjh456v5bwdi46ygwbnw6ars3i5nfqgchvhrlz1n8wk")))

