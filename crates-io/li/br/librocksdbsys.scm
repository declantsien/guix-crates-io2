(define-module (crates-io li br librocksdbsys) #:use-module (crates-io))

(define-public crate-librocksdbsys-0.1.2 (c (n "librocksdbsys") (v "0.1.2") (d (list (d (n "bzip2-sys") (r "^0.1") (d #t) (k 0)) (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libz-sys") (r "^1.0") (f (quote ("static"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tempdir") (r "^0.3") (d #t) (k 0)) (d (n "zstd-sys") (r "^1.3") (d #t) (k 0)))) (h "0022yljcjy7pkiysnn9gxpv2gybn7ip429h0b592mp10lccv7wq9") (f (quote (("sse") ("portable") ("default")))) (l "rocksdb")))

