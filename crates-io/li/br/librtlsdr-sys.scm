(define-module (crates-io li br librtlsdr-sys) #:use-module (crates-io))

(define-public crate-librtlsdr-sys-0.1.0 (c (n "librtlsdr-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.47.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1b5a5b48f6l5ygkk86pb068nmsinxd8i7n4vwf8nhyc3swjcfyhc") (l "librtlsdr")))

