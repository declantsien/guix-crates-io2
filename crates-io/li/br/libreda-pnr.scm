(define-module (crates-io li br libreda-pnr) #:use-module (crates-io))

(define-public crate-libreda-pnr-0.0.0 (c (n "libreda-pnr") (v "0.0.0") (d (list (d (n "libreda-db") (r "^0.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0yl8pirjjgqcyx51ii510hhn801cm674gnk0q53nl46w6r274735")))

(define-public crate-libreda-pnr-0.0.4 (c (n "libreda-pnr") (v "0.0.4") (d (list (d (n "libreda-db") (r "^0.0.10") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0zf0asvrhy1vyx51k1vfw8y66hrqjxb2ly5pk1gfxs3wajkm6a93")))

