(define-module (crates-io li br libraft) #:use-module (crates-io))

(define-public crate-libraft-0.1.0 (c (n "libraft") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^1.4") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)))) (h "1fn74lz7410hi3x6m4h7y1w30j9dwfzqwhzcz4xx5r42xxnfkmss")))

(define-public crate-libraft-0.1.1 (c (n "libraft") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "protobuf") (r "^1.7") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1n9bd389sxv0hrqz49vcrhssn84fi9c3wqihjg8qsqxnbbjjgzx9")))

