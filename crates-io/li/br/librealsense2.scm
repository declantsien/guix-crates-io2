(define-module (crates-io li br librealsense2) #:use-module (crates-io))

(define-public crate-librealsense2-0.2.0 (c (n "librealsense2") (v "0.2.0") (d (list (d (n "librealsense2-sys") (r "^0.2.0") (d #t) (k 0)))) (h "070c5497hl3xlzdqrfz3mg0b7v9d563yg7ls6dvpd5zfd4l33dfy")))

(define-public crate-librealsense2-0.3.0 (c (n "librealsense2") (v "0.3.0") (d (list (d (n "librealsense2-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1y6lr430gglj25ds71mrazgdgd8d9axmpzc51s48s7dm8hh6rnv3")))

(define-public crate-librealsense2-0.5.0 (c (n "librealsense2") (v "0.5.0") (d (list (d (n "librealsense2-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0jda7hz8wv2nvzypfj17ddliddpys9y26v73764kd7i8slmch64v")))

