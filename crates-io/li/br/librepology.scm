(define-module (crates-io li br librepology) #:use-module (crates-io))

(define-public crate-librepology-0.1.0-alpha.1 (c (n "librepology") (v "0.1.0-alpha.1") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "derive-new") (r "^0.5") (d #t) (k 0)) (d (n "derive_more") (r "^0.14") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "url") (r "^1.7") (d #t) (k 0)) (d (n "url_serde") (r "^0.2") (d #t) (k 0)))) (h "1my0hhsrfk7jmd8973mkl35sk2m43lxycxw3zf5a6krymgij8jyf")))

