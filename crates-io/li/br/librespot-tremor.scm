(define-module (crates-io li br librespot-tremor) #:use-module (crates-io))

(define-public crate-librespot-tremor-0.1.0 (c (n "librespot-tremor") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "16wjwlnhyvhs9iwagm0ak027zvhvxv0qa6vsqc0jw9sd9vfafmdi")))

(define-public crate-librespot-tremor-0.2.0 (c (n "librespot-tremor") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.47") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "ogg-sys") (r "^0.0.9") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1zmld16zawvn7ayrf318lwdr2d7awn4bk9s0d6kpim0mz6zjbxcp")))

