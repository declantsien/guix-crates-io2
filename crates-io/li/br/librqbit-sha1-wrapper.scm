(define-module (crates-io li br librqbit-sha1-wrapper) #:use-module (crates-io))

(define-public crate-librqbit-sha1-wrapper-2.2.1 (c (n "librqbit-sha1-wrapper") (v "2.2.1") (d (list (d (n "crypto-hash") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "openssl") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "sha1") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1zjn0fhxcb46k2ip3cz195kdsj7nmlbq2ds5nvcilxgiw226s0a5") (f (quote (("sha1-system" "crypto-hash") ("sha1-rust" "sha1") ("sha1-openssl" "openssl") ("default" "sha1-system"))))))

(define-public crate-librqbit-sha1-wrapper-3.0.0 (c (n "librqbit-sha1-wrapper") (v "3.0.0") (d (list (d (n "crypto-hash") (r "^0.3") (d #t) (k 0)))) (h "19q1wbwbilgfflq6sjqr3s8qbqnh47hgfy7fd6jskl41jw5smzy7")))

