(define-module (crates-io li br libraildriver) #:use-module (crates-io))

(define-public crate-libraildriver-0.1.0 (c (n "libraildriver") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libraildriver-sys") (r "^0.1") (d #t) (k 0)))) (h "0hab34qfmj6hv00gfbvqnbj06pxmha9ws8qb6dr0kvxcaarrgv02")))

(define-public crate-libraildriver-0.1.1 (c (n "libraildriver") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libraildriver-sys") (r "^0.1") (d #t) (k 0)))) (h "16px4lgyj8h5p17m12kvb8vq1j9nbqyrcyqx5682znbi5c8w9mfg")))

(define-public crate-libraildriver-0.1.2 (c (n "libraildriver") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libraildriver-sys") (r "^0.1") (d #t) (k 0)))) (h "05flfp64ilya8kd304dkzbrdr4ivyjh1f7x44nm3aq7almmq2z1k")))

