(define-module (crates-io li br librador) #:use-module (crates-io))

(define-public crate-librador-0.1.0 (c (n "librador") (v "0.1.0") (d (list (d (n "librador-sys") (r "^0.1") (d #t) (k 0)))) (h "1vj1jy14g2lnljx34p4bv39a2x905r217ynbgv63fgc3aa5hapxs")))

(define-public crate-librador-0.1.1 (c (n "librador") (v "0.1.1") (d (list (d (n "librador-sys") (r "^0.1") (d #t) (k 0)))) (h "0kig17ngg90bd81dv1cwmjr0zqg2sm5xpqhj90sd0s1f9zi209z2")))

