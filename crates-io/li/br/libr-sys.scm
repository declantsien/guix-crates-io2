(define-module (crates-io li br libr-sys) #:use-module (crates-io))

(define-public crate-libR-sys-0.1.0 (c (n "libR-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "07fsrii6li8pd4swdmlb539zfk96nszddvnkzrs3vhp4dnbip5vj")))

(define-public crate-libR-sys-0.1.1 (c (n "libR-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0rp8sclvxlva4ix17h7014glagk37bamm7miapffikfdwqmcl8pa")))

(define-public crate-libR-sys-0.1.2 (c (n "libR-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1il0bji4lqvnna2vk02j8597pri2dgf0aq2h1080id75am03hdsk") (l "R")))

(define-public crate-libR-sys-0.1.3 (c (n "libR-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1a3vjcidrijy3ypdld74c8a89ijacdxqm94lvh8q6ll9h2px5dl7") (l "R")))

(define-public crate-libR-sys-0.1.4 (c (n "libR-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "04x5i08mlpmzmmw28a4rpvwd351cq41fi78bll2awh91q3ybzh57") (l "R")))

(define-public crate-libR-sys-0.1.6 (c (n "libR-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0zph64x5m7pknj58h08dv76zxda687smiykkips6qhvi2prn0zm6") (l "R")))

(define-public crate-libR-sys-0.1.7 (c (n "libR-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0w8icp7wmjzzadsmgab6hz8cpx7c3f2qfaa1pswq0ss43k82p6v4") (l "R")))

(define-public crate-libR-sys-0.1.8 (c (n "libR-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0nrlixb1rnbjln1kwz3dbwy3whrm9n3gi242vs8i30s3r3n2vlwj") (l "R")))

(define-public crate-libR-sys-0.1.9 (c (n "libR-sys") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "185vnf43yg5cc399agr1n1y96ka6fdsnq8dgv5v8s5r01h2iyqdf") (l "R")))

(define-public crate-libR-sys-0.1.10 (c (n "libR-sys") (v "0.1.10") (d (list (d (n "bindgen") (r ">=0.53.2, <0.54.0") (d #t) (k 1)) (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 1)))) (h "0i1482gs7wn7nkj6kr32ifz3qavdgvp36ikirb7r9gcrrgn7jlaz") (l "R")))

(define-public crate-libR-sys-0.2.0 (c (n "libR-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.2") (o #t) (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 1)) (d (n "regex") (r "^1") (o #t) (d #t) (k 1)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 1)))) (h "0hz79l22swsc27rfjdjjprlp67wazm4axg24py32xd5dz7bgmn62") (f (quote (("use-bindgen" "bindgen" "regex")))) (l "R")))

(define-public crate-libR-sys-0.2.1 (c (n "libR-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.53.2") (o #t) (d #t) (k 1)) (d (n "kernel32-sys") (r "^0.2.2") (d #t) (t "cfg(windows)") (k 1)) (d (n "regex") (r "^1") (o #t) (d #t) (k 1)) (d (n "winapi") (r "^0.3") (d #t) (t "cfg(windows)") (k 1)))) (h "1n4s395g03ymx1admj1pzj9ng69ns57bkr6iyfzcrwkq59hqxg8w") (f (quote (("use-bindgen" "bindgen" "regex")))) (l "R")))

(define-public crate-libR-sys-0.2.2 (c (n "libR-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.53.2") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1") (o #t) (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "stringapiset"))) (d #t) (t "cfg(windows)") (k 1)))) (h "02698wd9569blclgw55363ygh6cqr7s7jgk03fccs3zbk1a5xx5x") (f (quote (("use-bindgen" "bindgen" "regex")))) (l "R")))

(define-public crate-libR-sys-0.3.0 (c (n "libR-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.53.2") (o #t) (d #t) (k 1)) (d (n "regex") (r "^1") (o #t) (d #t) (k 1)) (d (n "winapi") (r "^0.3") (f (quote ("consoleapi" "stringapiset"))) (d #t) (t "cfg(windows)") (k 1)))) (h "1113snd54mf6wkrs4862v056dvqqs5lb7dy9nmb23ikrjdlgv5ra") (f (quote (("use-bindgen" "bindgen" "regex")))) (l "R")))

(define-public crate-libR-sys-0.4.0 (c (n "libR-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.63") (o #t) (d #t) (k 1)) (d (n "clang") (r "^2") (f (quote ("runtime"))) (o #t) (d #t) (k 1)) (d (n "regex") (r "^1") (o #t) (d #t) (k 1)))) (h "1pqal370pvn5gpkm5skciy5gkrhc7r0sfrf8dda5z5xhp6bqlwnd") (f (quote (("use-bindgen" "bindgen" "regex" "clang")))) (l "R")))

(define-public crate-libR-sys-0.5.0 (c (n "libR-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "clang") (r "^2") (f (quote ("runtime" "clang_3_7"))) (o #t) (d #t) (k 1)))) (h "1yzipnv4kwznfkbn3m9fnp235dyh2dp2v9zxw89s8z2xlrima2j7") (f (quote (("use-bindgen" "bindgen" "clang")))) (l "R")))

(define-public crate-libR-sys-0.6.0 (c (n "libR-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.64") (o #t) (d #t) (k 1)) (d (n "clang") (r "^2") (f (quote ("runtime" "clang_3_7"))) (o #t) (d #t) (k 1)))) (h "1wczj4s0baxwaf9r6kj357qamn5d4q9pv9pmbnmixxq1l9laljm3") (f (quote (("use-bindgen" "bindgen" "clang")))) (l "R")))

