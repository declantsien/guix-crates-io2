(define-module (crates-io li br library_crate) #:use-module (crates-io))

(define-public crate-library_crate-0.1.0 (c (n "library_crate") (v "0.1.0") (h "0zpvs66wi0n7hs0zbndjay465sgjzrjadd7m7g6ia6sg6ss8b3n0")))

(define-public crate-library_crate-0.1.1 (c (n "library_crate") (v "0.1.1") (h "0whx0nv11x5afmr9n6ly172zbp79xpgb92qlvfj7qcqwb6yi90yz")))

(define-public crate-library_crate-0.1.2 (c (n "library_crate") (v "0.1.2") (h "1haqsqpm93495526miymrpqb993pv1jwpdr9b7vx08707y3y5fa6")))

