(define-module (crates-io li br librist-sys) #:use-module (crates-io))

(define-public crate-librist-sys-0.1.0 (c (n "librist-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0j8in0vjjk4apxxs0wjhwqfb69hjc65ian1ps9yzihdmbwphdjml")))

(define-public crate-librist-sys-0.2.0 (c (n "librist-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jpx1sw3ilw7sg9kaywqbxcv2is0jq50dnmlxkra5i1r760bbywn")))

(define-public crate-librist-sys-0.3.0 (c (n "librist-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1p0da5z3cgxd48q7nshki6chycfvdpxyixkpmqvhzcdys99404vv")))

(define-public crate-librist-sys-0.4.0 (c (n "librist-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cmhrrdvcg4bc5g8dg768l9169nrxxjrljg7354y6j0bzr4aj75a")))

(define-public crate-librist-sys-0.5.0 (c (n "librist-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1mnfjff4hz292iwcbb2nlq0s5lz3bvmygyp2bhxp2y6kcbp2mbkd")))

(define-public crate-librist-sys-0.6.0 (c (n "librist-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rafkyv8jf95pl3jbhwl88kb8m7l1ri4bg54wrgzr11p3v5jir95")))

(define-public crate-librist-sys-0.7.0 (c (n "librist-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1a3ycfnszrk8zfq6m2kp4vgf26m32r2j6dafrb9nx8df5j6mli73")))

(define-public crate-librist-sys-0.8.0 (c (n "librist-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kl034g472a8s5yaays7rcl9y8hag8ryi5yihcfxbcz1sbfq8cpd")))

(define-public crate-librist-sys-0.8.1 (c (n "librist-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hz552c6w70xb1yh748vf3f9cp77mbrzv10rvrds1l9v4j448xvh")))

(define-public crate-librist-sys-0.8.2 (c (n "librist-sys") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11mba2ahm1sp046axq1mqgfmik40a1xqh163c2vpv77jx3mr9zqq")))

(define-public crate-librist-sys-0.8.3 (c (n "librist-sys") (v "0.8.3") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0459wi77m39nck079hrrbkwr8iwkfnm1i7ccw35w7jsgmdlllaq1")))

