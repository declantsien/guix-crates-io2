(define-module (crates-io li br libraw_rs_vendor) #:use-module (crates-io))

(define-public crate-libraw_rs_vendor-1.0.0 (c (n "libraw_rs_vendor") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.82") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)))) (h "0l1q61bhzwfx8a0dw36fb2nhmcp2xn8n0i2ljripnwf7mpkcrs77")))

