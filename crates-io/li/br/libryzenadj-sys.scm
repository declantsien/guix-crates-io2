(define-module (crates-io li br libryzenadj-sys) #:use-module (crates-io))

(define-public crate-libryzenadj-sys-0.11.1 (c (n "libryzenadj-sys") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "patch") (r "^0.6.0") (d #t) (k 1)))) (h "0av8g4z3kcb2i9s45zwrqbdpkn5m5ncx3szk6dbf3hhz5qsrbpy9")))

(define-public crate-libryzenadj-sys-0.11.2 (c (n "libryzenadj-sys") (v "0.11.2") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "patch") (r "^0.6.0") (d #t) (k 1)))) (h "1c23ibps7qxy0qvp7cm47g4dg1hsk47dszhamjdqb395l6sflxrv")))

(define-public crate-libryzenadj-sys-0.12.0 (c (n "libryzenadj-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "patch") (r "^0.7.0") (d #t) (k 1)))) (h "1q9di64aqil7d9lww6v84zx4vfxmvlq29pdlnw6wwip26afz6wiv")))

(define-public crate-libryzenadj-sys-0.13.0 (c (n "libryzenadj-sys") (v "0.13.0") (d (list (d (n "bindgen") (r "^0.66.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "patch") (r "^0.7.0") (d #t) (k 1)))) (h "0fmwz9w4zdgmjhnhd6vma1cs1dij5rm7m86l28p8jklppqhkdpmi")))

