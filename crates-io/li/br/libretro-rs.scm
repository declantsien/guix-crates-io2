(define-module (crates-io li br libretro-rs) #:use-module (crates-io))

(define-public crate-libretro-rs-0.1.0 (c (n "libretro-rs") (v "0.1.0") (d (list (d (n "libretro-sys") (r "^0.1") (d #t) (k 0)))) (h "0a02wl6q4qy16gy4v69p7d0j7in9dj8mcghbqxdlil3ll2qz1khc")))

(define-public crate-libretro-rs-0.1.1 (c (n "libretro-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lmbpaka2l21wh33gzfsv19smzck2qsqzi3ls98mwaj7hx2f99n8")))

(define-public crate-libretro-rs-0.1.2 (c (n "libretro-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1axcdmcss30l8d8dzvwjcbgip7baqri4635dqnxrlbh7mvy7bsrn")))

(define-public crate-libretro-rs-0.1.3 (c (n "libretro-rs") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ckwiygxzwwqm7390mkf8f17nzpz082h9a5fkydgw736hxnshhw5")))

