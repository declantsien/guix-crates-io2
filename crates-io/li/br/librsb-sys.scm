(define-module (crates-io li br librsb-sys) #:use-module (crates-io))

(define-public crate-librsb-sys-0.1.0 (c (n "librsb-sys") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 0)) (d (n "bindgen") (r "^0.59.2") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 1)))) (h "1xh7fw7x1mh8d0kck2andgs3pgvg5z20s78g6y9gjmfb5z003nvm") (f (quote (("doc-only") ("codegen" "bindgen"))))))

(define-public crate-librsb-sys-0.1.1 (c (n "librsb-sys") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.53") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 1)))) (h "0wg9g7gx747fymsxcvaa04q84qgilm5lalisbrgv4xnnh44lpk6h") (f (quote (("doc-only") ("codegen" "bindgen"))))))

