(define-module (crates-io li br libreoffice-rs) #:use-module (crates-io))

(define-public crate-libreoffice-rs-0.1.0 (c (n "libreoffice-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0d8d98gb63bk0fzy04rlky6p1aby8qh44gcygg1ygf0aa086jdcb")))

(define-public crate-libreoffice-rs-0.1.1 (c (n "libreoffice-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "1gq6fij3200gcbmji6ipnkl889g036s09r1kv11q37b0ynv5xxax")))

(define-public crate-libreoffice-rs-0.1.2 (c (n "libreoffice-rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "04rxl0qjqjvxs4v0f5z17pm96d5c80n2d9z5l942qdmqffww0j77")))

(define-public crate-libreoffice-rs-0.2.0 (c (n "libreoffice-rs") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "0br7k27h266jzsxy69gd6x5bbv815awqk5bh2b4lp2s4sfxcz7wc") (f (quote (("unstable") ("default"))))))

(define-public crate-libreoffice-rs-0.2.1 (c (n "libreoffice-rs") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "07pbjnqa4dd9jxl1d1rs3r99wymc12ps3b0qqwgf8y8q6nxm137b") (f (quote (("unstable") ("default"))))))

(define-public crate-libreoffice-rs-0.3.0 (c (n "libreoffice-rs") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "1jcn7y8d3al7y3b5np90arnh38nfphc2n905kiwm2bc0ccm3bidw") (f (quote (("unstable") ("default"))))))

(define-public crate-libreoffice-rs-0.3.1 (c (n "libreoffice-rs") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1kqhf1hk39gra7zblfapk57ms9hnvsyhb9i352jn0jmwf557ybk6") (f (quote (("unstable") ("default"))))))

(define-public crate-libreoffice-rs-0.3.2 (c (n "libreoffice-rs") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1x87qwzv54mc82yv733ilizm1k1kv5v25vpg9812m71sdjazazc9") (f (quote (("unstable") ("default"))))))

(define-public crate-libreoffice-rs-0.3.3 (c (n "libreoffice-rs") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "10v9avg3v4jcyb6rzj4nsb9fsc99bldjkgdl6g35kr2dd4yqmx9p") (f (quote (("unstable") ("default"))))))

