(define-module (crates-io li br libr) #:use-module (crates-io))

(define-public crate-libr-0.1.0 (c (n "libr") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17lyribp20ff09dka8f6iiklawy2k0phgb7firy9j12x0cbf5ndx")))

