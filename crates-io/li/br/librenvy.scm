(define-module (crates-io li br librenvy) #:use-module (crates-io))

(define-public crate-librenvy-0.1.0 (c (n "librenvy") (v "0.1.0") (d (list (d (n "cargo-make") (r "^0.35.13") (d #t) (k 2)))) (h "1zw29wscyax82yr0s4x7pmc87ynk6h69jr5gbk1ghcv8n3ixkrfd")))

(define-public crate-librenvy-0.1.1 (c (n "librenvy") (v "0.1.1") (d (list (d (n "cargo-make") (r "^0.35.13") (d #t) (k 2)))) (h "04gnmfpaqqjqhkyklcsnfyqw24rgxz92i6v26hclqalgi7m2d9ja")))

(define-public crate-librenvy-0.1.2 (c (n "librenvy") (v "0.1.2") (d (list (d (n "cargo-make") (r "^0.35.13") (d #t) (k 2)))) (h "068mp3flkirccc8dn0xq1ihx9b1zda43mq8076ndjlb11jd4s46m")))

(define-public crate-librenvy-0.1.3 (c (n "librenvy") (v "0.1.3") (d (list (d (n "cargo-make") (r "^0.35.13") (d #t) (k 2)))) (h "1mkpj1yrvpjxn1gjwllwpgxx7491rb2da7fm6k4zcxanimyyqg0f")))

(define-public crate-librenvy-0.1.5 (c (n "librenvy") (v "0.1.5") (d (list (d (n "cargo-make") (r "^0.35.13") (d #t) (k 2)))) (h "1fhy0g6r1v0lz5bir4g9ygdh7yl911pnzf7ak8lh2yhkw7dn4xj2")))

(define-public crate-librenvy-0.1.6 (c (n "librenvy") (v "0.1.6") (d (list (d (n "cargo-make") (r "^0.35.13") (d #t) (k 2)))) (h "08d84r0da66nmvpfw52rjsfdbmmnm7vw8zk13sp9zlxbkdfssz0i")))

