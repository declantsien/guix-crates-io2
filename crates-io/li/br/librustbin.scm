(define-module (crates-io li br librustbin) #:use-module (crates-io))

(define-public crate-librustbin-0.1.0 (c (n "librustbin") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.5") (f (quote ("blocking" "multipart"))) (d #t) (k 0)))) (h "0vz00ixl3xmwrp03766gkm2pm2j010prmxiihawiifjhaznlqbjw")))

(define-public crate-librustbin-0.2.0 (c (n "librustbin") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.5") (f (quote ("blocking" "multipart"))) (d #t) (k 0)))) (h "0361d48cxdyfbwn0kk4mcc7rwgm3l32xihsbln4c5n8qs0zszj4m")))

