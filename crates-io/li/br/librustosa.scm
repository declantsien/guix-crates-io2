(define-module (crates-io li br librustosa) #:use-module (crates-io))

(define-public crate-librustosa-0.1.0 (c (n "librustosa") (v "0.1.0") (d (list (d (n "apodize") (r "^1.0") (d #t) (k 0)) (d (n "fftw") (r "^0.7") (f (quote ("system"))) (o #t) (k 0)) (d (n "ndarray") (r "^0.15") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.4") (d #t) (k 0)) (d (n "rustfft") (r "^6.0") (o #t) (d #t) (k 0)) (d (n "strider") (r "^0.1") (d #t) (k 0)))) (h "0qdfzyr2scqkgnkcp92s48zs2bq2fvrxgf12lnxc12645raxalij") (f (quote (("fftrust" "rustfft") ("fftextern" "fftw/system") ("default" "fftrust"))))))

