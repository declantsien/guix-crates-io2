(define-module (crates-io li br librualg) #:use-module (crates-io))

(define-public crate-librualg-0.1.0 (c (n "librualg") (v "0.1.0") (h "1jxdnpm0wx8f9qji9ay7shh9lzkjidr2jr7246x5jcadfz2qhvi2")))

(define-public crate-librualg-0.2.0 (c (n "librualg") (v "0.2.0") (h "0mq1l74jy26rppggq84lv9bm7sm7390lsisxsgxyd39jrjdxcv8w")))

(define-public crate-librualg-0.3.0 (c (n "librualg") (v "0.3.0") (h "0wa74rk5cnlxbh96hdjwmgvab8w6a695q2m0fiq4x006b9kszr8h")))

(define-public crate-librualg-0.4.0 (c (n "librualg") (v "0.4.0") (h "0s2wk9fp4kc86lhld3wy7gxiv0ax7nhm4ib2mh0bac51yin708hx")))

(define-public crate-librualg-0.5.0 (c (n "librualg") (v "0.5.0") (h "08i36x13yvzmcp88acm4hpqp8r5ilid7nlmscp8q9y0z9bk0rd4s")))

(define-public crate-librualg-0.6.0 (c (n "librualg") (v "0.6.0") (h "03bvq95m02224db7hnh0a8qhr5dyr40ap2dpqyc84hb8afr42kjw")))

(define-public crate-librualg-0.7.0 (c (n "librualg") (v "0.7.0") (h "1kbgrbm6jvzamqfqjjhyxhxc6lmi323g7ggxcjma2vmjcflbhlar")))

(define-public crate-librualg-0.8.0 (c (n "librualg") (v "0.8.0") (h "0w4n5gdl90swffzfp5flda7bjn719jm4f1j7ng01smsnqdvap0nl")))

(define-public crate-librualg-0.9.0 (c (n "librualg") (v "0.9.0") (h "1wghqga8117n1vdv6442qddm0vvmxaxqv5kpv6c1lwj5hpypy3gz")))

(define-public crate-librualg-0.10.0 (c (n "librualg") (v "0.10.0") (h "097ky6r4aarvx0ism9y7d7yb1mk53s0a1c4g8qggfqpmgs3ckwl7")))

(define-public crate-librualg-0.11.0 (c (n "librualg") (v "0.11.0") (h "155qkgzyamyb7drvy0vhg549byrj19vw0zax3zs0p8kks0b44mgw")))

(define-public crate-librualg-0.12.0 (c (n "librualg") (v "0.12.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0rayizlm6471d5lv8p2g2zqn1inz5kl7wj2gsr1ccgpj8z8lm754")))

(define-public crate-librualg-0.13.0 (c (n "librualg") (v "0.13.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1aiziy690afsgzm83rfv0njlr94glqjphy0lrz1kfh7i2ig7fxdc")))

(define-public crate-librualg-0.14.0 (c (n "librualg") (v "0.14.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1kjcfcpxr8b4agd7mm9vjnd71zl2k1grx6jh5r88wpva57svv36r")))

(define-public crate-librualg-0.15.0 (c (n "librualg") (v "0.15.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1zgnbk8f9xcjrkcb7cc8qnqw4137lkssvhvabx6ajqihinyvhdag")))

(define-public crate-librualg-0.16.0 (c (n "librualg") (v "0.16.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0cn88d9cwpslirh6w978kmkgpf4nnsjzdais6zdw9m3swldn2c7z")))

(define-public crate-librualg-0.17.0 (c (n "librualg") (v "0.17.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0n5vwzwgsr0wml9rzmx0jhv8p03wazwdcfn5pnjdbih97q82sy99")))

(define-public crate-librualg-0.18.0 (c (n "librualg") (v "0.18.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "14xx07610hz27k83706x1j3qqvbjhrjirkhwpasyaqprarvsn8n3")))

(define-public crate-librualg-0.19.0 (c (n "librualg") (v "0.19.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0lwrg2s7kwfjcivg14d9k457jldb9avlnd1mgf87yg4w09vx7i5l")))

(define-public crate-librualg-0.20.0 (c (n "librualg") (v "0.20.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "01c3hlzzs16jgf0yi6yr51hdlsx9zxk7f55dp71b3k8vdgg5139f")))

(define-public crate-librualg-0.21.0 (c (n "librualg") (v "0.21.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0p9rbbszcrnwzn6k0gw0sc79v25gq0b8fb9ixp7fhdkb741vmbgp")))

(define-public crate-librualg-0.22.0 (c (n "librualg") (v "0.22.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "061rc4yldr0rzrxb0w5f765wxjb3kpymdf8npz022xaa5v9krnl4")))

(define-public crate-librualg-0.22.1 (c (n "librualg") (v "0.22.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16wviqkn6i1nc93c85699g9m85qjdlv6ff0vh3n1vfy20n6hjmdq")))

(define-public crate-librualg-0.23.0 (c (n "librualg") (v "0.23.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1jqm5k2dzf4096cngkkm1fq0kvndrw356j3mbgw959nd8lmci15w")))

(define-public crate-librualg-0.24.0 (c (n "librualg") (v "0.24.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1p41l49sjd9pkmph69mxiw8vwnnaz7gs0i73svdhgd99lxa3kd21")))

(define-public crate-librualg-0.25.0 (c (n "librualg") (v "0.25.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0wk973yvmpifiim5i4y9cipmads92fy9plchbbxijsi3hwsc6gfn")))

(define-public crate-librualg-0.25.1 (c (n "librualg") (v "0.25.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "03pla0chcbnn368mqipq0zpbng1z2pycx8p2pc7cdj65z5dcyqdq")))

(define-public crate-librualg-0.26.0 (c (n "librualg") (v "0.26.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "01pkssr8x4lx5jqz0h8pbglsa39n92fx5v7dl7zv13isygvvxrqz")))

(define-public crate-librualg-0.27.0 (c (n "librualg") (v "0.27.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1h81qqw16i25dm30l2qhr9q3pvhl1xlz0py2sy6vmpmm4v60fjfy")))

(define-public crate-librualg-0.28.0 (c (n "librualg") (v "0.28.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "14j3k0zcqaykv3h62ka2a7snvx0fng5lxbapa2qvyx1fi8ckalwj")))

(define-public crate-librualg-0.29.0 (c (n "librualg") (v "0.29.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ahwfabnha6h8kl588dr8c0w1mxq5qii0rh526n1mxizwgms06g4")))

(define-public crate-librualg-0.29.1 (c (n "librualg") (v "0.29.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0nidi7jrz602f98vv6w3blc3vrljfg05z1clpxnaf1nc2z4bz6yp")))

(define-public crate-librualg-0.30.0 (c (n "librualg") (v "0.30.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1qxvf09zhf7zkyrq06s0bi396z847immh167w11p80xi2dcvg8mx") (y #t)))

