(define-module (crates-io li br libreplica) #:use-module (crates-io))

(define-public crate-libreplica-0.1.0 (c (n "libreplica") (v "0.1.0") (d (list (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 0)))) (h "0hxgajb7mzib7iva3wsxi13khakl93bsyq3bj0829pgw9gz5lprs")))

(define-public crate-libreplica-0.1.1 (c (n "libreplica") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 2)) (d (n "core_affinity") (r "^0.5.10") (d #t) (k 2)) (d (n "ctrlc") (r "^3.2.0") (d #t) (k 0)) (d (n "derivative") (r "^2.2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.16") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "quanta") (r "^0.9.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "simple_logger") (r "^1.13.0") (d #t) (k 2)))) (h "0nl84njh2sbv2xp2gsq3iv96p65hshhgs4k0phxhfgdsqcqnfsfm")))

