(define-module (crates-io li br libryzenadj-sys-alt) #:use-module (crates-io))

(define-public crate-libryzenadj-sys-alt-0.14.0 (c (n "libryzenadj-sys-alt") (v "0.14.0") (d (list (d (n "bindgen") (r "^0.66.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "patch") (r "^0.7.0") (d #t) (k 1)))) (h "0dfxw3v9csszwr76y04fjhya4ii2q8l0dymma477f5i2r4mfd7fb")))

