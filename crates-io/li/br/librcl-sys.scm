(define-module (crates-io li br librcl-sys) #:use-module (crates-io))

(define-public crate-librcl-sys-0.1.0 (c (n "librcl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "1yq9x1pbwcq2pjdvz66a6hy0fdyhrcghxgd97xbcfh6624027bc1") (l "rcl")))

(define-public crate-librcl-sys-0.1.1 (c (n "librcl-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "0ygzqjhb0vhn956l9ba40hwlk4c63fswvmbzwl8iwpipz8rbbnx1") (l "rcl")))

(define-public crate-librcl-sys-0.1.2 (c (n "librcl-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "1h81qw86wz3c7hrg4wdis1iflikr8w0pn6frb0m3hvd7fjgmpgjw") (l "rcl")))

