(define-module (crates-io li br libruta) #:use-module (crates-io))

(define-public crate-libruta-0.1.0 (c (n "libruta") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (f (quote ("cookies"))) (d #t) (k 0)))) (h "1zwq3w5a3n6n6p3byq94xpqmvff0x0wv5f4x60kz9pr7f7kbb721")))

(define-public crate-libruta-0.2.0 (c (n "libruta") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.5") (d #t) (k 0)) (d (n "scraper") (r "^0.18.1") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (f (quote ("cookies"))) (d #t) (k 0)))) (h "1q3n7f9mxg0l24dfqpa41za02v1sq1a8hh4xk7zsjcgllxj4xy3q")))

