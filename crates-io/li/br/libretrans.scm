(define-module (crates-io li br libretrans) #:use-module (crates-io))

(define-public crate-libretrans-0.1.0 (c (n "libretrans") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "libretranslate") (r "^0.1.1") (d #t) (k 0)))) (h "1cc8dlpawril566v1l21b46z759mvkxg8smaah3qjki7mpr66pc6")))

(define-public crate-libretrans-0.1.1 (c (n "libretrans") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libretranslate") (r "^0.1.7") (d #t) (k 0)))) (h "0ia53jzd0qw7hgb5wsrllchk5v2wr2l3mja5sdzyykhmrlj2i46s")))

(define-public crate-libretrans-0.1.2 (c (n "libretrans") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libretranslate") (r "^0.1.8") (d #t) (k 0)))) (h "14hl0cd2485zj3jvga0bpdhvldlcj16ywyrz8vz9kwppfi3rqm9l")))

(define-public crate-libretrans-0.1.3 (c (n "libretrans") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libretranslate") (r "^0.1.8") (d #t) (k 0)))) (h "00vx6w924nrdvq9v2cyxsy451mgrybmjl9pgh2d1sdvxs3igzbi7")))

(define-public crate-libretrans-0.1.4 (c (n "libretrans") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libretranslate") (r "^0.1.8") (d #t) (k 0)))) (h "01mcn6k4gyabzyiq3bgw4rvh1wwqir6q2i0hl3058l6ic49qs2pc")))

(define-public crate-libretrans-0.1.5 (c (n "libretrans") (v "0.1.5") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libretranslate") (r "^0.2.0") (d #t) (k 0)))) (h "0gzb3x2pggvrc0wjqf9vr05r87zbli1qr8p3265wbp2ayhip7pa7")))

(define-public crate-libretrans-0.1.6 (c (n "libretrans") (v "0.1.6") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libretranslate") (r "^0.2.4") (d #t) (k 0)))) (h "1v9jy0qpw4hkx5w1pgyrpvff8v7d3z3fz9fz6nysf4jf316psdl4")))

(define-public crate-libretrans-0.1.7 (c (n "libretrans") (v "0.1.7") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libretranslate") (r "^0.2.9") (d #t) (k 0)))) (h "0d82c1x596b827sr1zfnf8dbfgj7ji7mjsijpm16bvknnz7d6q0f")))

(define-public crate-libretrans-0.1.8 (c (n "libretrans") (v "0.1.8") (d (list (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "libretranslate") (r "^0.4.0") (d #t) (k 0)))) (h "16n40f36bvhv1miyjcmgcz879qsd7r1rqwqdhrm2lm6lgkhp78q9")))

