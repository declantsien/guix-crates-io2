(define-module (crates-io li br librobotcontrol-sys) #:use-module (crates-io))

(define-public crate-librobotcontrol-sys-0.1.0 (c (n "librobotcontrol-sys") (v "0.1.0") (d (list (d (n "c2rust-bitfields") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 2)))) (h "10gdvmlgp3mx093fvvy59jg01wci64vw2n4z8n95afvcai6ab83p")))

(define-public crate-librobotcontrol-sys-0.2.0 (c (n "librobotcontrol-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 2)))) (h "0mqb58dkgg7lqqzvspa1p5bfnvbv5jn0w37f8wjni4pgpzkys1cf")))

(define-public crate-librobotcontrol-sys-0.3.0 (c (n "librobotcontrol-sys") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 2)))) (h "1jxf2d1lahhyiv62hc5168zckgpxdxvshbs5wa0m3d6ivqjn0nxc")))

(define-public crate-librobotcontrol-sys-0.4.0 (c (n "librobotcontrol-sys") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "signal-hook") (r "^0.3.15") (d #t) (k 2)))) (h "07hpg8qjm0z0zw12fj23mr3xmalqc70mvggxmbdghk5zga53ns1q")))

