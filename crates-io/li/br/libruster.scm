(define-module (crates-io li br libruster) #:use-module (crates-io))

(define-public crate-libruster-1.0.0 (c (n "libruster") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1hwdj1w3dh6b1allc4jmxjwb7y67wv6bb0y6zyhg2grk3ngf0h5r")))

(define-public crate-libruster-1.1.1 (c (n "libruster") (v "1.1.1") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0q9vk5yk1rg7wyp4pflm48ch4k45yh0qwfdcjlpv90ff92k5dkmr")))

(define-public crate-libruster-1.2.1 (c (n "libruster") (v "1.2.1") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1qgrwkr0n56dmqw4ki9isg4hd9fdaah47v4bdzrck2g82vbxfzjj")))

(define-public crate-libruster-1.2.2 (c (n "libruster") (v "1.2.2") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "10jkskl14kj7g1f4m4kya50qs4fgzz0x7v2asbv62y3251qx4dq1")))

(define-public crate-libruster-1.2.3 (c (n "libruster") (v "1.2.3") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "00hkxc5i8vkgmfcfbdjh0a0q5ypfpyy9a4ibzcsraafq8v8qc9x3")))

(define-public crate-libruster-1.2.4 (c (n "libruster") (v "1.2.4") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "191mc8hbi2970m3bpv70m0qll4ma5pw03mxdhx1qvdrzbwd8qwd1")))

(define-public crate-libruster-1.2.6 (c (n "libruster") (v "1.2.6") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0y7gppkp9nja2552p6znan18zkwbh9gm4967yxbp13cqa4vvfqam")))

(define-public crate-libruster-1.2.7 (c (n "libruster") (v "1.2.7") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0gp5f9ydc9fgbdmchp97rgwd5gq0xn5ghiwhd73h4hxkklxpfb3l")))

(define-public crate-libruster-1.2.8 (c (n "libruster") (v "1.2.8") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1dkgc77ia1dk53zcb1j65k82mr93isillwsq3h9cqqld4pjlvjig")))

(define-public crate-libruster-1.2.9 (c (n "libruster") (v "1.2.9") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "10hhj71kq06j9dgjdzrb1wb7qgfs76z03lw4b5kplkhjh9s3fyaz")))

(define-public crate-libruster-1.2.10 (c (n "libruster") (v "1.2.10") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "12240l1bm05zsiyb89bri9grvfc4670wfay646dmyhffn908flgw")))

(define-public crate-libruster-1.3.1 (c (n "libruster") (v "1.3.1") (d (list (d (n "env_logger") (r "^0.8.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1p7q228drcalfa5d8y5icqmy379cg79bpicapk85hx7l1512smch")))

