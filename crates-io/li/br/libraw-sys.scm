(define-module (crates-io li br libraw-sys) #:use-module (crates-io))

(define-public crate-libraw-sys-0.1.0 (c (n "libraw-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.19") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1w2wr3v3d0cj2993j8vf3dsfma0vawam5sl1hp8ns14jb9hrvmp7")))

(define-public crate-libraw-sys-0.1.1 (c (n "libraw-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.19") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1jj31f2dp9j197apcz435psarrx3hkvdp67fnqnmd68r8yxqbr9c")))

