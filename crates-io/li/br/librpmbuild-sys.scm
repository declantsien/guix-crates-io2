(define-module (crates-io li br librpmbuild-sys) #:use-module (crates-io))

(define-public crate-librpmbuild-sys-0.1.0 (c (n "librpmbuild-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "00l3v8y16q94vdzx3g4aybdjla83sg1yp5vh152pdxl2sz235257")))

(define-public crate-librpmbuild-sys-0.1.1 (c (n "librpmbuild-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "1j6sjxbvb11w0zfgdk4j8rgzrg5gjsiwpy43nq06ax0qc3zf9c1a")))

