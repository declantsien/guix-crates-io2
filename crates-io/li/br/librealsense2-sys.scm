(define-module (crates-io li br librealsense2-sys) #:use-module (crates-io))

(define-public crate-librealsense2-sys-0.1.0 (c (n "librealsense2-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1qz7gxc0q4jfzcrdr5rwp3ynb64r6l1amk2aakwf1z9sxvqkar1l")))

(define-public crate-librealsense2-sys-0.2.0 (c (n "librealsense2-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.48.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "05nvqw7ysphhiarkhkn89z3ygbqbk50yl83w6qahs71794pbk7il")))

