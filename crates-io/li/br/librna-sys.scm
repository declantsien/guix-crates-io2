(define-module (crates-io li br librna-sys) #:use-module (crates-io))

(define-public crate-librna-sys-0.1.0 (c (n "librna-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "~0.58") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.13") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "1wnlkji57gmx6knnrdswv0d5f1ypl58117y9za94gacs30dghl1h") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

(define-public crate-librna-sys-0.1.1 (c (n "librna-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "~0.58") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.13") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "01jki0mlvsjbimbls4c0169kqpwfn38v3n0z1bdx4my4454r288a") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

(define-public crate-librna-sys-0.1.2 (c (n "librna-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "~0.58") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.13") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "143lvryhfm488s9dh8pjd899vxp9iig2v982hnb9si1sr1lb74xd") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

(define-public crate-librna-sys-0.1.3 (c (n "librna-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "~0.58") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.13") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "1yav6wkjj46md0zl6a9bcj525rm0fmsds34ksl6bvzgp5zvdkv59") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

(define-public crate-librna-sys-0.1.4 (c (n "librna-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "~0.59") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.16") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "1c83q0xknjl2ccp5yyvv6wr2pnwwm5lnf99dr0vyjbw794ckwsmy") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

(define-public crate-librna-sys-0.1.5 (c (n "librna-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "~0.64") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.16") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "187wi6snhmmh8h3kywa2vn3fp059md895m2z8lfbv5ddwhqpj485") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

(define-public crate-librna-sys-0.1.6 (c (n "librna-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "~0.64") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.16") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "18gfqma2gspsc3xm0k87gf1xklvw2skavyj070h99mh0818pcrv9") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

(define-public crate-librna-sys-0.1.7 (c (n "librna-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "~0.64") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.16") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "1pdf686s1rw72bpr5yddhyzcj0b4nvh2znc3caxgpils6m200wpv") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

(define-public crate-librna-sys-0.2.0 (c (n "librna-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "~0.64") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.16") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "16yfzazczi6w2xbgz77nq0zazfh7gd6kywk7731ldcxl2zabm8f5") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

(define-public crate-librna-sys-0.2.1 (c (n "librna-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "~0.64") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.16") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "15h7ah9gd8y4l4015di5f120as08phgilfjzfhxqd13vi0szmawa") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

(define-public crate-librna-sys-0.2.2 (c (n "librna-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "~0.64") (d #t) (k 1)) (d (n "libc") (r "~0.2") (d #t) (k 2)) (d (n "pkg-config") (r "~0.3") (o #t) (d #t) (k 1)) (d (n "pyo3") (r "~0.16") (f (quote ("extension-module"))) (d #t) (k 2)))) (h "0n1axl32pf628si8y7c9wxgal23cx8f4vdvchv40071yiar7xlxb") (f (quote (("default") ("auto" "pkg-config")))) (l "RNA")))

