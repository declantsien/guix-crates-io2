(define-module (crates-io li br librecollect) #:use-module (crates-io))

(define-public crate-librecollect-0.1.0 (c (n "librecollect") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "cron") (r "^0.11") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "11m57c5dhaaql72lh5y4my4ssdkx50kpq910lhpf7ir5djq8l3f0")))

(define-public crate-librecollect-0.2.0 (c (n "librecollect") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "cron") (r "^0.11") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0dapvcpyg3m7gj8q8vrixr2xqkw3l7g5xihvhdcsrg7y487335kz")))

(define-public crate-librecollect-0.3.0 (c (n "librecollect") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "cron") (r "^0.11") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1qp7p7iayz2wz9l9v81413py421f2iadnq839zwvr9qjfqwf8lqw")))

(define-public crate-librecollect-0.3.1 (c (n "librecollect") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "cron") (r "^0.11") (d #t) (k 0)) (d (n "notify-rust") (r "^4") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1k3jpy5wgb4w2141q22nkdbnyiwj1msdhx9q3awsry6xq7s06ll5")))

