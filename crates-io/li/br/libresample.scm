(define-module (crates-io li br libresample) #:use-module (crates-io))

(define-public crate-libresample-0.1.0 (c (n "libresample") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "14ll6pmmqllvf7xvhi9981kv11lxjnzvmlpmpgrw99gyc2nkyarl")))

(define-public crate-libresample-0.1.1 (c (n "libresample") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "13678gi7y80r7wfasnj5dxdw99np2xds5f1apraxlcn0zp4r9ya2")))

