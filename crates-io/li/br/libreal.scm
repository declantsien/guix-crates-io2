(define-module (crates-io li br libreal) #:use-module (crates-io))

(define-public crate-libreal-0.1.0 (c (n "libreal") (v "0.1.0") (h "0cnq29rzmgy0iqbvwbmjw94gyqc0fpk9rql0pkr7vnwncyygpfr3")))

(define-public crate-libreal-0.1.1 (c (n "libreal") (v "0.1.1") (h "1prdrwzjwn7rwc1fy7kf9b4mdfla89a8bq5v70dcz1vi37ijjyqp")))

(define-public crate-libreal-0.2.0 (c (n "libreal") (v "0.2.0") (h "024bhp2sy1kqqbkr4fqckjykf54yfxbcsrf52kk6kn3d3y6p2g8q")))

(define-public crate-libreal-0.2.1 (c (n "libreal") (v "0.2.1") (h "0p2apg9npyc9zl7frflj1qzaim6f4sxfa2bpfbczjmf2mc5ncml9")))

(define-public crate-libreal-0.3.0 (c (n "libreal") (v "0.3.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)))) (h "063iip1fl3i43gmlxb8jb4mcn5b8jdd3pp9iwrnvnfs04nfsxygc")))

(define-public crate-libreal-0.4.0 (c (n "libreal") (v "0.4.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "08gv714vf92hzpcvp4hn0x1azhfagpffvagb968i1smqxb0bgsb0")))

(define-public crate-libreal-0.5.0 (c (n "libreal") (v "0.5.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0wn9d06xkn0dxvcx4rck4ffkwbxgqryx1js635p7xwbxliydhyrq")))

(define-public crate-libreal-0.6.0 (c (n "libreal") (v "0.6.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "07sgxn8ci98iygnkkf8vhdyj64li6wvjvhvqa2iw4347q6r6yixb")))

(define-public crate-libreal-0.6.1 (c (n "libreal") (v "0.6.1") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1g0sks2bngr5r5zb20psi3yln1jd22x6qp5i3ymqm9vv7p2pawpp")))

(define-public crate-libreal-0.6.2 (c (n "libreal") (v "0.6.2") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0ch2cqzg0lgh2g0a1pxzisza6n7s6kpgw93yj41fxdamncd4mycn")))

(define-public crate-libreal-0.6.3 (c (n "libreal") (v "0.6.3") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1sxzgslr3k0sx8kbhpb4z8bgmhznhqasma2z3sk8b132336kh52b")))

(define-public crate-libreal-0.7.0 (c (n "libreal") (v "0.7.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1350qj3kj5za5qpay2v4gz7gsrd9mjv8i8g8siy4h1513yhkvv9x")))

(define-public crate-libreal-0.7.1 (c (n "libreal") (v "0.7.1") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "08pyn300vwcwq76yh8n4kb2gfylmpqimrfa518i5vfk76r63l4w7")))

(define-public crate-libreal-0.8.0 (c (n "libreal") (v "0.8.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0c3gj9blnmsdn1npwm0jdkh3xlgzx149qpfi9nyz58b07aqb036q")))

(define-public crate-libreal-0.9.0 (c (n "libreal") (v "0.9.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "060q7n194j9nfaaxbdjdv6qlzvsdca732sj0w1lrw0z7m1mrq29m")))

(define-public crate-libreal-0.10.0 (c (n "libreal") (v "0.10.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0m5l2wk18a7iamnsrz3dw9m700ijjy3bd84p852vigd7gw7mw7pv")))

(define-public crate-libreal-0.10.1 (c (n "libreal") (v "0.10.1") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1f9q7l7izg6bg0ixpzsw59xn9pjlp0y6p6kyywd8cdwkbwpdk21n")))

(define-public crate-libreal-0.10.2 (c (n "libreal") (v "0.10.2") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0i1jh372da235klsdjx4f5hfzlx2s2rympnkqzg3vl3p94i9dkxl")))

(define-public crate-libreal-0.10.3 (c (n "libreal") (v "0.10.3") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "050rgpxhlcfrqa8crn4pgyb11fkb7zwz391bgz1mwgcp71j6gcmq")))

(define-public crate-libreal-0.10.4 (c (n "libreal") (v "0.10.4") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0jl0yp3731hrwnl31887knwhvg0mr17sshasxrm73cl2g7v5hw31")))

(define-public crate-libreal-0.11.0 (c (n "libreal") (v "0.11.0") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "06ik0sdlvgfghd7zji71xai1si2kflzzjnaxihb2swl4igkl7941")))

(define-public crate-libreal-0.11.2 (c (n "libreal") (v "0.11.2") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "1.2.*") (f (quote ("no_std"))) (d #t) (k 0)))) (h "0lf675l77lm1d16ppadklk0ag8yf5h2jwq2hrv7czg6bknkrk01b")))

(define-public crate-libreal-0.11.3 (c (n "libreal") (v "0.11.3") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "^1.2") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1mv7idikifzaiw2f1q1marlqclx6c3f40phrhxcl90h5wsp7n17z")))

(define-public crate-libreal-0.11.4 (c (n "libreal") (v "0.11.4") (d (list (d (n "quickcheck") (r "0.2.*") (d #t) (k 2)) (d (n "quickcheck_macros") (r "0.2.*") (d #t) (k 2)) (d (n "typenum") (r "^1.2") (f (quote ("no_std"))) (d #t) (k 0)))) (h "1r0fp8chclk3gl1hwh6n5ny84cbxzs0y4v84vb9g4w8wz4i600yj")))

(define-public crate-libreal-0.12.0 (c (n "libreal") (v "0.12.0") (d (list (d (n "quickcheck") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.2") (d #t) (k 2)) (d (n "typenum") (r "^1.2") (f (quote ("no_std"))) (d #t) (k 0)) (d (n "void") (r "^1") (k 0)))) (h "002pnfccj9ac9afvc5ixjza196mw09p25h99xg7lh2simzi1szfn")))

(define-public crate-libreal-0.13.0 (c (n "libreal") (v "0.13.0") (d (list (d (n "containers") (r "^0.1") (d #t) (k 0)) (d (n "i-o") (r "^0.2") (d #t) (k 0)) (d (n "rel") (r "^0.1") (d #t) (k 0)))) (h "1k4ngqbxx7zvl66gd65d3qmsgyd6wfifd34v3v4cda8igma4h562")))

(define-public crate-libreal-0.13.1 (c (n "libreal") (v "0.13.1") (d (list (d (n "containers") (r "^0.1") (d #t) (k 0)) (d (n "i-o") (r "^0.2") (d #t) (k 0)) (d (n "rel") (r "^0.1") (d #t) (k 0)))) (h "0kjy3vqca7dajrg0yd6zwscki8a27zdwvw3z1di7k683saf97grm")))

(define-public crate-libreal-0.14.0 (c (n "libreal") (v "0.14.0") (d (list (d (n "containers") (r "^0.2") (d #t) (k 0)) (d (n "i-o") (r "^0.2") (d #t) (k 0)) (d (n "rel") (r "^0.1") (d #t) (k 0)))) (h "1nb9xbalgj9vwmyw9by24gz635zhd6s21fasps29wg84dqmlhixx")))

(define-public crate-libreal-0.14.1 (c (n "libreal") (v "0.14.1") (d (list (d (n "containers") (r "^0.2") (d #t) (k 0)) (d (n "i-o") (r "^0.2") (d #t) (k 0)) (d (n "rel") (r "^0.1") (d #t) (k 0)))) (h "1yfbq1zdk3sppdd1najkf0cgip5vkhl87ycrgdglzlisy8mln2gy")))

