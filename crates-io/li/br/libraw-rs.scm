(define-module (crates-io li br libraw-rs) #:use-module (crates-io))

(define-public crate-libraw-rs-0.0.1 (c (n "libraw-rs") (v "0.0.1") (d (list (d (n "libraw-rs-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0mlsh6wqxmhzr1j24rq8bplr2p9k4jilal5pp2kg21zmc4g3vn52")))

(define-public crate-libraw-rs-0.0.2 (c (n "libraw-rs") (v "0.0.2") (d (list (d (n "libraw-rs-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0dy52l817xaa5incvxsyhhn6ay77y6h06lf8l24a6hpi1vi5yv3k")))

(define-public crate-libraw-rs-0.0.4 (c (n "libraw-rs") (v "0.0.4") (d (list (d (n "libraw-rs-sys") (r "^0.0.4") (d #t) (k 0)))) (h "0zrqhn4nn9hp15jhkzn6k8as4y12vk3a0w3fkhlhqmkqp2m61v14") (f (quote (("bindgen" "libraw-rs-sys/bindgen"))))))

