(define-module (crates-io li br libredfish) #:use-module (crates-io))

(define-public crate-libredfish-0.1.1 (c (n "libredfish") (v "0.1.1") (d (list (d (n "log") (r "~0.4") (d #t) (k 0)) (d (n "reqwest") (r "~0.11") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "reqwest") (r "~0.11") (f (quote ("blocking" "json"))) (d #t) (k 1)) (d (n "serde") (r "~1.0") (d #t) (k 0)) (d (n "serde_derive") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 0)) (d (n "serde_json") (r "~1.0") (d #t) (k 1)))) (h "1nd8n3w49kv5kd0g32qd9km99l9ga7xbbrw2vih8rlakbscaiw77")))

