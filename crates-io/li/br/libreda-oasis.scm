(define-module (crates-io li br libreda-oasis) #:use-module (crates-io))

(define-public crate-libreda-oasis-0.0.1 (c (n "libreda-oasis") (v "0.0.1") (d (list (d (n "byteorder") (r "1.3.*") (d #t) (k 0)) (d (n "libreda-db") (r "^0.0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "060wxz6b6aqs2lc40wmls442xa0r1v9hkvx3a4lzmij5asqxl597")))

(define-public crate-libreda-oasis-0.0.2 (c (n "libreda-oasis") (v "0.0.2") (d (list (d (n "byteorder") (r "1.3.*") (d #t) (k 0)) (d (n "libreda-db") (r "^0.0.6") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0384xy7vs6i4j45kydiq9rwlxidb639138x6p7nwh9zfdq4svnpp")))

(define-public crate-libreda-oasis-0.0.3 (c (n "libreda-oasis") (v "0.0.3") (d (list (d (n "byteorder") (r "1.3.*") (d #t) (k 0)) (d (n "libreda-db") (r "^0.0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0p8b8dmrv59qm9ngwi1qzylfq11n3rr2hy9by34zxx2bmkn4rmiy")))

