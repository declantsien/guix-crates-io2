(define-module (crates-io li br librespot-protocol) #:use-module (crates-io))

(define-public crate-librespot-protocol-0.0.0 (c (n "librespot-protocol") (v "0.0.0") (h "0kshp14hissbgmv457w0w9ynymgi5i34fl4bk5p8ljvspj3fd4ff")))

(define-public crate-librespot-protocol-0.1.0 (c (n "librespot-protocol") (v "0.1.0") (d (list (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.8.1") (d #t) (k 1)) (d (n "protobuf-codegen-pure") (r "^2.8.1") (d #t) (k 1)))) (h "01cgp7mi5bi7ai7fml9vq9pwsxm1igky62wizjg3lpxdigbgry86")))

(define-public crate-librespot-protocol-0.1.1 (c (n "librespot-protocol") (v "0.1.1") (d (list (d (n "protobuf") (r "^2.8.1") (d #t) (k 0)) (d (n "protobuf-codegen") (r "^2.8.1") (d #t) (k 1)) (d (n "protobuf-codegen-pure") (r "^2.8.1") (d #t) (k 1)))) (h "13fwa8igyy44cg71g0b3irjh390gb9r9m3whbz459z85klq9qs9j")))

(define-public crate-librespot-protocol-0.1.2 (c (n "librespot-protocol") (v "0.1.2") (d (list (d (n "protobuf") (r "~2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "~2.14.0") (d #t) (k 1)) (d (n "protobuf-codegen-pure") (r "~2.14.0") (d #t) (k 1)))) (h "0hzd1ryk0ap9lf3l2q5zj336kamgbscmnic4hidk437db4jypc6g")))

(define-public crate-librespot-protocol-0.1.3 (c (n "librespot-protocol") (v "0.1.3") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "protobuf") (r "~2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "~2.14.0") (d #t) (k 1)) (d (n "protobuf-codegen-pure") (r "~2.14.0") (d #t) (k 1)))) (h "00by2mq73w2r3fp9lsdjbdkwrpdcvlpn0j064mlf715i8bg05hj0")))

(define-public crate-librespot-protocol-0.1.4 (c (n "librespot-protocol") (v "0.1.4") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "protobuf") (r "~2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "~2.14.0") (d #t) (k 1)) (d (n "protobuf-codegen-pure") (r "~2.14.0") (d #t) (k 1)))) (h "15yxkhxmmnv3hkq89nddavw9w8zb1137ppnfzsh8j1mhv9vjkslh") (y #t)))

(define-public crate-librespot-protocol-0.1.5 (c (n "librespot-protocol") (v "0.1.5") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "protobuf") (r "~2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "~2.14.0") (d #t) (k 1)) (d (n "protobuf-codegen-pure") (r "~2.14.0") (d #t) (k 1)))) (h "0d1i9qa2480gmwq4s7jxp10ca6jnhmjcds6kkkxnks28s2m8xhns")))

(define-public crate-librespot-protocol-0.1.6 (c (n "librespot-protocol") (v "0.1.6") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "protobuf") (r "~2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "~2.14.0") (d #t) (k 1)) (d (n "protobuf-codegen-pure") (r "~2.14.0") (d #t) (k 1)))) (h "1n8gr8g4yrjp6i0rbsm7vpq00x9yh3lbjkzy1m5d8sgkkss048yl")))

(define-public crate-librespot-protocol-0.2.0 (c (n "librespot-protocol") (v "0.2.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "protobuf") (r "~2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen") (r "~2.14.0") (d #t) (k 1)) (d (n "protobuf-codegen-pure") (r "~2.14.0") (d #t) (k 1)))) (h "0pa3j2csmimf0l2yfdxp9ijmgm8ngxadim801jrh43xxqgj3nx8w")))

(define-public crate-librespot-protocol-0.3.0 (c (n "librespot-protocol") (v "0.3.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.14.0") (d #t) (k 1)))) (h "1z1r5inl4mprk89r3006cy56bx7jjgyc6k11ycsn9l2ylixz4217")))

(define-public crate-librespot-protocol-0.3.1 (c (n "librespot-protocol") (v "0.3.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.14.0") (d #t) (k 1)))) (h "1r1yrhycxaqd5iismw8wr7y8v4nvmarzbdx5lp550k6b0ndnkcs1")))

(define-public crate-librespot-protocol-0.4.0 (c (n "librespot-protocol") (v "0.4.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.14.0") (d #t) (k 1)))) (h "1l5zwv561pysgi1k5kg5gg6q7rrn2zvc9lajyjq2i5q1ayzbhqqg") (y #t)))

(define-public crate-librespot-protocol-0.4.1 (c (n "librespot-protocol") (v "0.4.1") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.14.0") (d #t) (k 1)))) (h "0paki13qvlii6ykaczd6cv5v7carlr3si9a07ixjmav6m8bdw89f")))

(define-public crate-librespot-protocol-0.4.2 (c (n "librespot-protocol") (v "0.4.2") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 1)) (d (n "protobuf") (r "^2.14.0") (d #t) (k 0)) (d (n "protobuf-codegen-pure") (r "^2.14.0") (d #t) (k 1)))) (h "17xkvhlxfkjh1z79pvq22nrxi99hcxnzafg0pdkymh3a3733lvax")))

