(define-module (crates-io li br libressl-pnacl-sys) #:use-module (crates-io))

(define-public crate-libressl-pnacl-sys-2.0.0 (c (n "libressl-pnacl-sys") (v "2.0.0") (d (list (d (n "pnacl-build-helper") (r "*") (d #t) (k 0)))) (h "1llak9bvjjyc8wn8vzp0g9rp0il24j2zfpf09a9k5icrx8m1q1m3")))

(define-public crate-libressl-pnacl-sys-2.0.2 (c (n "libressl-pnacl-sys") (v "2.0.2") (d (list (d (n "pnacl-build-helper") (r "*") (d #t) (k 1)))) (h "0h90cdqa3138sha6qwcgfrs3326zgwqsh9lk3380snmj7h1wdv37")))

(define-public crate-libressl-pnacl-sys-2.0.3 (c (n "libressl-pnacl-sys") (v "2.0.3") (d (list (d (n "pnacl-build-helper") (r "*") (d #t) (k 1)))) (h "0wgnlz35a58vpvyihi29v3rd5kq0n8d09mdwvsd33679bv0ij9h8") (y #t)))

(define-public crate-libressl-pnacl-sys-2.1.0 (c (n "libressl-pnacl-sys") (v "2.1.0") (d (list (d (n "pnacl-build-helper") (r "*") (d #t) (k 1)))) (h "15b3096npcs5iwv2pnj6rjcbszicw8rphx1fdrr5plk91fvk7ggv") (y #t)))

(define-public crate-libressl-pnacl-sys-2.1.1 (c (n "libressl-pnacl-sys") (v "2.1.1") (d (list (d (n "pnacl-build-helper") (r "*") (d #t) (k 1)))) (h "0lnxhjnmdmprmwjmmjpz8avndsfzd5wir5ph0y43p8zkn2sxd5l9") (y #t)))

(define-public crate-libressl-pnacl-sys-2.1.2 (c (n "libressl-pnacl-sys") (v "2.1.2") (d (list (d (n "pnacl-build-helper") (r "*") (d #t) (k 1)))) (h "03nll58zjl5cf8ss5a8lrh7wyiym4p0cw3drnm7akxazlw2rycww") (y #t)))

(define-public crate-libressl-pnacl-sys-2.1.3 (c (n "libressl-pnacl-sys") (v "2.1.3") (d (list (d (n "pnacl-build-helper") (r "*") (d #t) (k 1)))) (h "14vgm7v36kswp3mvhias7cqkn767hd8sd8wgzb91ap4q4chh7sva")))

(define-public crate-libressl-pnacl-sys-2.1.4 (c (n "libressl-pnacl-sys") (v "2.1.4") (d (list (d (n "pnacl-build-helper") (r "^1.2") (d #t) (k 1)))) (h "0dhywxpaklic8r49gsjq80c7m3bzvxmqh9ymbsf1mnlkiq69pvby")))

(define-public crate-libressl-pnacl-sys-2.1.5 (c (n "libressl-pnacl-sys") (v "2.1.5") (d (list (d (n "pnacl-build-helper") (r "^1.4") (d #t) (k 1)))) (h "0z24mcs4wlv62i3vas2rgykha2g028by9p91v4vzf1xnqqr4pfxf")))

(define-public crate-libressl-pnacl-sys-2.1.6 (c (n "libressl-pnacl-sys") (v "2.1.6") (d (list (d (n "pnacl-build-helper") (r "^1.4") (d #t) (k 1)))) (h "0w9yvgrf591phpr1222j0dj7x1n489v2sihnr8syz8xn3aamih6b")))

