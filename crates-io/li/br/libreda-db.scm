(define-module (crates-io li br libreda-db) #:use-module (crates-io))

(define-public crate-libreda-db-0.0.4 (c (n "libreda-db") (v "0.0.4") (d (list (d (n "genawaiter") (r "^0.99.1") (k 0)) (d (n "iron-shapes") (r "^0.0.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jlhwm8z3mkqd6f1iglh2a9nq0xncklyz4wya6xgyl7gjhdcr81c")))

(define-public crate-libreda-db-0.0.6 (c (n "libreda-db") (v "0.0.6") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "iron-shapes") (r "^0.0.6") (f (quote ("serde"))) (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "15wbxylajdg7dw1vqs34181fd2i57h1kxfwq0slp6lh6pl0sf04r")))

(define-public crate-libreda-db-0.0.8 (c (n "libreda-db") (v "0.0.8") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "iron-shapes") (r "^0.0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "iron-shapes-booleanop") (r "^0.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "1sk6p1cwwmag33g34flrihjprq4sm1772pdxgqlwn678d1dswxjm")))

(define-public crate-libreda-db-0.0.9 (c (n "libreda-db") (v "0.0.9") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "iron-shapes") (r "^0.0.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "iron-shapes-booleanop") (r "^0.0.2") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "000aff714irlm7p7w5pbhg751p3m3mrrpmvjwpl9p4v4f4w9vp69")))

(define-public crate-libreda-db-0.0.10 (c (n "libreda-db") (v "0.0.10") (d (list (d (n "fnv") (r "^1.0.7") (d #t) (k 0)) (d (n "iron-shapes") (r "^0.0.8") (f (quote ("serde"))) (d #t) (k 0)) (d (n "iron-shapes-booleanop") (r "^0.0.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive" "rc"))) (o #t) (d #t) (k 0)))) (h "0341hikp2dbw9q74zi7kj4ycfcq08yw7m4ihr7k3j293k0c3426w")))

