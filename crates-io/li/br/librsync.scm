(define-module (crates-io li br librsync) #:use-module (crates-io))

(define-public crate-librsync-0.1.0 (c (n "librsync") (v "0.1.0") (d (list (d (n "clippy") (r "< 1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librsync-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1qiznqpx3hlx9yl9prp3i2lpz3w1js7k18qpyic1cnq164n061bb") (f (quote (("unstable" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default" "log"))))))

(define-public crate-librsync-0.1.1 (c (n "librsync") (v "0.1.1") (d (list (d (n "clippy") (r "< 1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librsync-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0hppwjd1qn5cy9j8j9l53rrna1nh5wwcbjxb08g8fj1447f7904b") (f (quote (("unstable" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default" "log"))))))

(define-public crate-librsync-0.1.2 (c (n "librsync") (v "0.1.2") (d (list (d (n "clippy") (r "< 1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librsync-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0p8mrxcqj8hz6n1fmphqg34n5jh0q0zkr47h4nz019pdzk46rwb6") (f (quote (("unstable" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default" "log"))))))

(define-public crate-librsync-0.2.0 (c (n "librsync") (v "0.2.0") (d (list (d (n "clippy") (r "< 1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librsync-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1ljic3ynnlf6ngi0n8mcpwg7jqwh4512zw0q4dr7plsj852ab5pf") (f (quote (("unstable" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default" "log"))))))

(define-public crate-librsync-0.2.1 (c (n "librsync") (v "0.2.1") (d (list (d (n "clippy") (r "< 1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librsync-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "133c935in4xacj5pkpcrx3qgadsimfnx5as42sppikm6nyar76gv") (f (quote (("unstable" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default" "log"))))))

(define-public crate-librsync-0.2.2 (c (n "librsync") (v "0.2.2") (d (list (d (n "clippy") (r "<1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librsync-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "0743cgnf31nfqi3zvsrixdym1pi3dn0v0vj1rj6hszp8lbvmh7rb") (f (quote (("unstable" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default" "log"))))))

(define-public crate-librsync-0.2.3 (c (n "librsync") (v "0.2.3") (d (list (d (n "clippy") (r "<1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "librsync-sys") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)))) (h "07qax6szn6qd7ljdffch8kci60p7lknngfx028ssc01qnr7y98ny") (f (quote (("unstable" "lints" "nightly") ("nightly") ("lints" "clippy" "nightly") ("default" "log"))))))

