(define-module (crates-io li br libraw-rs-sys) #:use-module (crates-io))

(define-public crate-libraw-rs-sys-0.0.1+libraw-0.20.0 (c (n "libraw-rs-sys") (v "0.0.1+libraw-0.20.0") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1j4ld5by349w7mk7vaqim39jsf63rvj9vgv6lnd6h4mprx0nx792")))

(define-public crate-libraw-rs-sys-0.0.2+libraw-0.20.0 (c (n "libraw-rs-sys") (v "0.0.2+libraw-0.20.0") (d (list (d (n "bindgen") (r "^0.55.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gv40p89h4xx5sd6a1rd9xsngbrw5i4h52xcc6vz1hb1i11laj1w")))

(define-public crate-libraw-rs-sys-0.0.3+libraw-0.20.1 (c (n "libraw-rs-sys") (v "0.0.3+libraw-0.20.1") (d (list (d (n "bindgen") (r "^0.57") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1yda85xrq7c2lhmvs7lnwq94f39lq18zna4ig5kk2qg9nd74wrhg") (y #t)))

(define-public crate-libraw-rs-sys-0.0.4+libraw-0.20.1 (c (n "libraw-rs-sys") (v "0.0.4+libraw-0.20.1") (d (list (d (n "bindgen") (r "^0.57") (f (quote ("runtime"))) (o #t) (k 1)) (d (n "cc") (r "^1") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k63cldswgclbgjyma673k8af4rv3pw0dzzsxbyl5k04icxll2ds")))

