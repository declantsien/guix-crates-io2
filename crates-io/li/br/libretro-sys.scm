(define-module (crates-io li br libretro-sys) #:use-module (crates-io))

(define-public crate-libretro-sys-0.1.0 (c (n "libretro-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zlzrqr3zy1miavi59hbkp324sbfldqpk4y5mgqq3bal1lx5lihr")))

(define-public crate-libretro-sys-0.1.1 (c (n "libretro-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fdl41fv5ja9bk6impbr2ggw9mcihv9mw3rxvzkcxjyf085hcyr0")))

