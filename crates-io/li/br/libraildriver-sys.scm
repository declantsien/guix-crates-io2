(define-module (crates-io li br libraildriver-sys) #:use-module (crates-io))

(define-public crate-libraildriver-sys-0.1.0 (c (n "libraildriver-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09b73b3a2n018r505hcrksr6msxi6p957mq7nyplz2c74ccjz580")))

(define-public crate-libraildriver-sys-0.1.1 (c (n "libraildriver-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1zfd181jyw2jdbhkbbkn6vq6al940770lzc1v7cfqda2v4nnc8gj")))

(define-public crate-libraildriver-sys-0.1.2 (c (n "libraildriver-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7.3") (d #t) (k 0)))) (h "0a2hzqwc8xc2gkdbvnyrffy2xzi7zbk8ldjsv2168w4mwf1vxll9")))

