(define-module (crates-io li br library_of_babel) #:use-module (crates-io))

(define-public crate-library_of_babel-0.1.1 (c (n "library_of_babel") (v "0.1.1") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "num") (r "~0.3.1") (d #t) (k 0)) (d (n "rand") (r "~0.8.0") (d #t) (k 0)))) (h "1vkv1khi8wggdz63idg590j68j1ivasnn77l4lhxjgva0cjl9v6y") (y #t)))

(define-public crate-library_of_babel-0.1.2 (c (n "library_of_babel") (v "0.1.2") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "num") (r "~0.3.1") (d #t) (k 0)) (d (n "rand") (r "~0.8.0") (d #t) (k 0)))) (h "0srf5qv0bzz9l62g6lbr29ai0h55q1yd4z8qzzab19jm0c9489ib") (y #t)))

(define-public crate-library_of_babel-0.1.3 (c (n "library_of_babel") (v "0.1.3") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "num") (r "~0.3.1") (d #t) (k 0)) (d (n "rand") (r "~0.8.0") (d #t) (k 0)))) (h "0la66zdjsfw0ps6h5b2kz07imhgppkjgwc8m4rpwarw3ip5vgzbr")))

(define-public crate-library_of_babel-0.1.4 (c (n "library_of_babel") (v "0.1.4") (d (list (d (n "clap") (r "~2.33.3") (d #t) (k 0)) (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "num") (r "~0.3.1") (d #t) (k 0)) (d (n "rand") (r "~0.8.0") (d #t) (k 0)))) (h "1jwx0jvjfxaiva06m9wrpk3f9wm7ilz9iq58cl6falaabjc91byb")))

(define-public crate-library_of_babel-0.1.5 (c (n "library_of_babel") (v "0.1.5") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "num") (r "~0.3.1") (d #t) (k 0)) (d (n "rand") (r "~0.8.1") (d #t) (k 0)))) (h "1lbirnzl0jql2z2lnnh6kgdwlirzli0bf9rfi6frhdnn46dhk7lb")))

(define-public crate-library_of_babel-0.1.6 (c (n "library_of_babel") (v "0.1.6") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "num") (r "~0.3.1") (d #t) (k 0)) (d (n "rand") (r "~0.8.3") (d #t) (k 0)))) (h "038apgl6cknb9smv7ac6favlyn3nylkyyv6skg7fvxjpb97x2yy6")))

(define-public crate-library_of_babel-0.2.0 (c (n "library_of_babel") (v "0.2.0") (d (list (d (n "lazy_static") (r "~1.4.0") (d #t) (k 0)) (d (n "num") (r "~0.3.1") (d #t) (k 0)) (d (n "rand") (r "~0.8.3") (d #t) (k 0)))) (h "03kw663fqqxi7vf6nkv62jwq9yla0f2k67jkikbqk2gby3v3aj12")))

(define-public crate-library_of_babel-0.2.1 (c (n "library_of_babel") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)))) (h "1mihvj6kdk8h4jfffkdmvjxwc03ycymhqh1s27gww1dm14hz9cjk")))

(define-public crate-library_of_babel-0.4.0 (c (n "library_of_babel") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1kb5j5sg6zwyijid41bp58vn6cdjjims0chf13qj6xiwfpqgjbyn")))

