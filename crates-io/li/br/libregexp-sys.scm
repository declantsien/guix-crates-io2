(define-module (crates-io li br libregexp-sys) #:use-module (crates-io))

(define-public crate-libregexp-sys-0.0.1 (c (n "libregexp-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1z0p2sdbs0iaq8yyagxk96g2avpv3hk1zhvk48dxnprbzxwlgzgr") (l "libregexp")))

(define-public crate-libregexp-sys-0.0.2 (c (n "libregexp-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1whzi8wmk9nkgj230466vxxi8rdbfic9mj9f73b3bnwphlyrck21") (l "libregexp")))

(define-public crate-libregexp-sys-0.0.3 (c (n "libregexp-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "16q54zf1vm64zq91gqy4pyvf8pi11rhh7rf9nz1prwf1fq9c9br2") (l "libregexp")))

(define-public crate-libregexp-sys-0.0.4 (c (n "libregexp-sys") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0aqyc4kzsj21yivq8z2kps35s70qfpfw1kiwznvgk4yrz231rq1k") (l "libregexp")))

