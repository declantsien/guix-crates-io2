(define-module (crates-io li br librime-sys) #:use-module (crates-io))

(define-public crate-librime-sys-0.3.0 (c (n "librime-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "1gc2iw4z0xah14di80h0vljma7cvs5ps045c67dw80dn3rmxcnbx")))

(define-public crate-librime-sys-0.4.0 (c (n "librime-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "09dyw5c8fh1ngn1g541pnqslp4chgnlw4dfq5gf5fyqiygjx5h52")))

