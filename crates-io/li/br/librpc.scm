(define-module (crates-io li br librpc) #:use-module (crates-io))

(define-public crate-librpc-0.1.0 (c (n "librpc") (v "0.1.0") (h "13cxin324kl92wzh360499a7ydkdw8g45z8j68xmj0jiqvml66y9")))

(define-public crate-librpc-0.1.1 (c (n "librpc") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("async_futures" "html_reports"))) (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)))) (h "0pnybkj04y97s0kwqh06pri5qgn68l7sjbc8b3npxk5j836sbvvi")))

(define-public crate-librpc-0.1.2 (c (n "librpc") (v "0.1.2") (d (list (d (n "async-timer-rs") (r "^0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.4") (f (quote ("async_futures" "html_reports"))) (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "futures-test") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 2)))) (h "1xjaz7z5qjxx4rqhs8dy5jkpcn85g6r9mg2kw9ybs84kj0byjbpj")))

