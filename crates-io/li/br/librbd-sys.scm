(define-module (crates-io li br librbd-sys) #:use-module (crates-io))

(define-public crate-librbd-sys-0.1.0 (c (n "librbd-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.8") (d #t) (k 0)) (d (n "librados-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1gsyw34hvmwkapsxcriy2jzkqig9vvvf4wwzz20gjx6470izpzn4")))

