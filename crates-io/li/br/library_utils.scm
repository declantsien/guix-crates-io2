(define-module (crates-io li br library_utils) #:use-module (crates-io))

(define-public crate-library_utils-0.0.1 (c (n "library_utils") (v "0.0.1") (h "1bb5g1l97ryffisqq6593nmyr8fhd4qz5k45als1lryai7yxd76y") (y #t)))

(define-public crate-library_utils-0.0.2 (c (n "library_utils") (v "0.0.2") (h "06gq9v7kn7cjg7yd9c8ra1a0y7ssnqw0a8li519mdsyrdsf4jlbv")))

