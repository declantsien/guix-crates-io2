(define-module (crates-io li br libreda-structural-verilog) #:use-module (crates-io))

(define-public crate-libreda-structural-verilog-0.0.3 (c (n "libreda-structural-verilog") (v "0.0.3") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lalrpop") (r "^0.19.0") (f (quote ("lexer"))) (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.0") (d #t) (k 0)) (d (n "libreda-db") (r "^0.0.8") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "petgraph") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1db8255sxh4kc3zysqj2s8an099qq624n1mdxg86ipzgnmvybarn")))

