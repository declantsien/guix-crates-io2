(define-module (crates-io li br librdkafka-sys) #:use-module (crates-io))

(define-public crate-librdkafka-sys-0.9.1-0 (c (n "librdkafka-sys") (v "0.9.1-0") (d (list (d (n "num_cpus") (r "^0.2") (d #t) (k 1)))) (h "0ihmmqqz62f9v089srr6a900n9gdqd9dvb9bvz2kpkc7580pn7ri")))

(define-public crate-librdkafka-sys-0.9.1-1 (c (n "librdkafka-sys") (v "0.9.1-1") (d (list (d (n "num_cpus") (r "^0.2") (d #t) (k 1)))) (h "1w3ll0r3jy482wdz6f5i24mr3sjdzyxrpzzv0zql3g3rds2aykdg")))

(define-public crate-librdkafka-sys-0.9.1-2 (c (n "librdkafka-sys") (v "0.9.1-2") (d (list (d (n "num_cpus") (r "^0.2") (d #t) (k 1)))) (h "0ia7vrld11rkl5gac4hjil0bcz0ly5bmpj8g7vrk76z58qf7vvqy")))

(define-public crate-librdkafka-sys-0.9.1-3 (c (n "librdkafka-sys") (v "0.9.1-3") (d (list (d (n "num_cpus") (r "^0.2") (d #t) (k 1)))) (h "0vvywgc9w0pwxjq8x2vasdcdxzqp1ikss3n84c7llih75s37dbai") (f (quote (("zlib") ("ssl") ("sasl" "ssl") ("default" "zlib"))))))

(define-public crate-librdkafka-sys-0.9.2-0 (c (n "librdkafka-sys") (v "0.9.2-0") (d (list (d (n "num_cpus") (r "^0.2") (d #t) (k 1)))) (h "12mclsfd5mq6q30wjwlip3ic9p08sq57b54bqgnaspp38ydmhr6c") (f (quote (("zlib") ("ssl") ("sasl" "ssl") ("default" "zlib"))))))

