(define-module (crates-io li br librunner) #:use-module (crates-io))

(define-public crate-librunner-0.1.0 (c (n "librunner") (v "0.1.0") (h "0aj3sszxknqw42kczxibjdymwqjsasf72nmcszyhc526shg6204d")))

(define-public crate-librunner-0.2.0 (c (n "librunner") (v "0.2.0") (h "18kyp00fwp29x6pxvgzi5cf4zjsgp30a1xf9b8q8i6pg75b5h3xa")))

(define-public crate-librunner-0.2.1 (c (n "librunner") (v "0.2.1") (h "17zyd4gki6jcrimp8bypagpb71sgkj8iyvwwznhbzf1xxl69d6ww")))

(define-public crate-librunner-0.2.2 (c (n "librunner") (v "0.2.2") (h "1vkw0d0kfbw294mv041wcpxl8az6il6cxk11bsl9y6nbgmdh3m5i")))

(define-public crate-librunner-0.3.0 (c (n "librunner") (v "0.3.0") (h "1rb43m58yd2m2c0z0sm1qp1fwlwsfszhkgnhwk343h7szih9064a")))

(define-public crate-librunner-0.4.0 (c (n "librunner") (v "0.4.0") (h "0maxmv4vp1ysglybwvalwm28l28248ws1fq2giydzh5gkd1wfqzf")))

(define-public crate-librunner-0.5.0 (c (n "librunner") (v "0.5.0") (h "1gj967r0yxgpcc4w76k0n8g23fl8rrb524yblqfislnl3q81vvjw")))

(define-public crate-librunner-0.6.0 (c (n "librunner") (v "0.6.0") (h "1m6ms3mzrzz2xh3s97096gnl3z6fy9i85c44lvxz3d0rk75c9vvv")))

(define-public crate-librunner-0.7.0 (c (n "librunner") (v "0.7.0") (h "0lkaaj45mhbjdr6aiakhvmfj9knl4jzjvh8w267b6r1dql229awi")))

