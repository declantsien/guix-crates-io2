(define-module (crates-io li ea liealg) #:use-module (crates-io))

(define-public crate-liealg-0.1.0 (c (n "liealg") (v "0.1.0") (d (list (d (n "approx") (r "^0.5") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)))) (h "1mvl0j391vc0ln1g13i38vk4zg5j3qmghl4ag7l84qgb0yh2wrq6") (y #t)))

(define-public crate-liealg-0.1.1 (c (n "liealg") (v "0.1.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.4") (d #t) (k 0)))) (h "1d1b8vam6yfzss4pjy956z0aw9fn7v57vrhyxg1ci47bniihaqgy") (y #t)))

(define-public crate-liealg-0.2.0 (c (n "liealg") (v "0.2.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "1s0x74lmiqwgg71lcaa460bq7811ydypbb379l1hg6kc1qpzmhln")))

(define-public crate-liealg-0.3.0 (c (n "liealg") (v "0.3.0") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.5") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.18") (d #t) (k 0)))) (h "092c962b1yhhv37xfdgmapayzj85b8aahdpn0gdxi7sbb1jrv376")))

