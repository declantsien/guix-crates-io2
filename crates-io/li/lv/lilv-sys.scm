(define-module (crates-io li lv lilv-sys) #:use-module (crates-io))

(define-public crate-lilv-sys-0.1.0 (c (n "lilv-sys") (v "0.1.0") (h "0yy9wh9n6pcnkpmp9bi48v9dyq352g8sy14xg2ca0vdm6ym10v5g")))

(define-public crate-lilv-sys-0.1.1 (c (n "lilv-sys") (v "0.1.1") (h "0h4yqnkv9jp1dvch7203nmvj8cyhz1zpnx7nxplnk6iss1k6yn2p")))

(define-public crate-lilv-sys-0.1.2 (c (n "lilv-sys") (v "0.1.2") (h "0dylr8vigp7rifr63846bdmcimkdpdk344kdfrsg8ipxm2rjnivd")))

(define-public crate-lilv-sys-0.2.0 (c (n "lilv-sys") (v "0.2.0") (d (list (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)))) (h "0r36gvf3dpx96c45j2dd5mllanbjpjnf9ahdxknnz2ypyn2kry3p")))

(define-public crate-lilv-sys-0.2.1 (c (n "lilv-sys") (v "0.2.1") (d (list (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)))) (h "0g7mb1nnpq098pmf2xyhph1hxyv5kibis5wzh40jvdldvdpnmrdf")))

