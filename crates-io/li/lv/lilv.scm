(define-module (crates-io li lv lilv) #:use-module (crates-io))

(define-public crate-lilv-0.0.1 (c (n "lilv") (v "0.0.1") (d (list (d (n "lilv-sys") (r "^0.2") (d #t) (k 0)) (d (n "lv2-urid") (r "^2") (d #t) (k 2)) (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "04fkrwv8wdqp3bkqz5naic84klqd9bn8ykk8dfn35i52ylm21bzq")))

(define-public crate-lilv-0.0.2 (c (n "lilv") (v "0.0.2") (d (list (d (n "lilv-sys") (r "^0.2") (d #t) (k 0)) (d (n "lv2-urid") (r "^2") (d #t) (k 2)) (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0f86fpri787l56bcispfjlvc7fawk8dzdg7xlh3i9f0mm18aqq68")))

(define-public crate-lilv-0.0.3 (c (n "lilv") (v "0.0.3") (d (list (d (n "lilv-sys") (r "^0.2") (d #t) (k 0)) (d (n "lv2-urid") (r "^2") (d #t) (k 2)) (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hpi5r825b6780njdxmzf9228cp1vg3p4cb143jfdrb1h8l53fpq")))

(define-public crate-lilv-0.0.4 (c (n "lilv") (v "0.0.4") (d (list (d (n "lilv-sys") (r "^0.2") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rzrpkraf94f3icpcy3zx9wnrlddgrnv11mj82q1rbca6ng7530s")))

(define-public crate-lilv-0.0.5 (c (n "lilv") (v "0.0.5") (d (list (d (n "lilv-sys") (r "^0.2") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "00bsmcrmhigxa8drkwjjjry607iarjv7db483mi5l64pdz4dx0bf")))

(define-public crate-lilv-0.0.6 (c (n "lilv") (v "0.0.6") (d (list (d (n "lilv-sys") (r "^0.2") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1bfbaxd7r0a0x9vi10xhizmj9b74wdfxxs3jyq1ip7c4i6561l4c")))

(define-public crate-lilv-0.1.0 (c (n "lilv") (v "0.1.0") (d (list (d (n "lilv-sys") (r "^0.2") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0b5f9g9x2dk90nkplcr3lrm9ndydhl2p5bsrlrhh2k3y3d1vq454")))

(define-public crate-lilv-0.2.0 (c (n "lilv") (v "0.2.0") (d (list (d (n "lilv-sys") (r "^0.2") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0bw69m9slyb5ymski655xrffs73b94whg55jh10hhx2kakg44zi4")))

(define-public crate-lilv-0.2.1 (c (n "lilv") (v "0.2.1") (d (list (d (n "lilv-sys") (r "^0.2") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08qmgg0c68sr85zsx4xwjv06qbvi00ydf9cpwqi38pl0q8h8ljb7")))

(define-public crate-lilv-0.2.2 (c (n "lilv") (v "0.2.2") (d (list (d (n "lilv-sys") (r "^0.2") (d #t) (k 0)) (d (n "lv2_raw") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0yij4pj82lbk4zbk3kj5630yx5y9723qcy3m6y4gb8y79jg5p2xs")))

