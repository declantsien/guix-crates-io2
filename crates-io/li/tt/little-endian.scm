(define-module (crates-io li tt little-endian) #:use-module (crates-io))

(define-public crate-little-endian-0.1.0 (c (n "little-endian") (v "0.1.0") (h "1q6kfq15gi37i05dqcxi21bh856x71dwcjnzmp571zv6gx2ivd5w")))

(define-public crate-little-endian-1.0.0 (c (n "little-endian") (v "1.0.0") (h "12pc6ginvyhh43m9jv5g9j7yrr8wysxjbb7cdq3fkv315022d8x1")))

