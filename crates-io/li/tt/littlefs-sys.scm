(define-module (crates-io li tt littlefs-sys) #:use-module (crates-io))

(define-public crate-littlefs-sys-0.1.0 (c (n "littlefs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "0r8f7j5a73z5m6fdrg7ajpy4ca5sqz4zw94faqxi2nwf4392z1pr")))

(define-public crate-littlefs-sys-0.2.0 (c (n "littlefs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)))) (h "1hcdj5nw1ma0brm6fd4kpi2g33yz3635xmji9mzaj3v02fk48p5n")))

