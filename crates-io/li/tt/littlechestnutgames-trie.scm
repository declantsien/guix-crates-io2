(define-module (crates-io li tt littlechestnutgames-trie) #:use-module (crates-io))

(define-public crate-littlechestnutgames-trie-1.0.0 (c (n "littlechestnutgames-trie") (v "1.0.0") (h "0gp8fax3g1bs6d9d43k1nsxan795689iqkfn4dhgizv416afx4xp")))

(define-public crate-littlechestnutgames-trie-2.0.0 (c (n "littlechestnutgames-trie") (v "2.0.0") (d (list (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "0nyna57612rac3k0irdz56w888zrk97skvhxzvpv0x60y12vfis3")))

(define-public crate-littlechestnutgames-trie-3.0.0 (c (n "littlechestnutgames-trie") (v "3.0.0") (d (list (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1n0a7z97wcyz39vp4pk4gyvcihz57h4jvh7pf2b5axaz5rw8c1j8")))

