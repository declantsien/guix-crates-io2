(define-module (crates-io li tt littlefs2-sys) #:use-module (crates-io))

(define-public crate-littlefs2-sys-0.1.0 (c (n "littlefs2-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "004lr5d5mqhx5c97dsaipaq9ih0d557ighn523gi3m37hxg5408q")))

(define-public crate-littlefs2-sys-0.1.1 (c (n "littlefs2-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "172wnp1pwwr86ri2544rmbhaxi7gdjjml5r2a38fqkd3vahw87mp") (f (quote (("trace") ("assertions"))))))

(define-public crate-littlefs2-sys-0.1.2 (c (n "littlefs2-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0gq7pjk0brpxl59jwq2s7zm90yx8bnxhwikb4gk1ggvqsqx4paia") (f (quote (("trace") ("assertions"))))))

(define-public crate-littlefs2-sys-0.1.3 (c (n "littlefs2-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "1ws36pj0p6nqr6gv4hndzgdqa87gskbjmkdfh5xf25ng6wgwkmrw") (f (quote (("trace") ("assertions"))))))

(define-public crate-littlefs2-sys-0.1.4 (c (n "littlefs2-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "01c9gd6g5lhna31h6sba9gnq05zqdvi3f4ndpfpf0960rlycckc5") (f (quote (("trace") ("assertions"))))))

(define-public crate-littlefs2-sys-0.1.5 (c (n "littlefs2-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0.25") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "0m0rdnkf0r0fmxlc6lsicscnnk54hf9iai3yk2dv206ps1rp1i6k") (f (quote (("trace") ("assertions"))))))

(define-public crate-littlefs2-sys-0.1.6 (c (n "littlefs2-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.56.0") (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "cty") (r "^0.2.1") (d #t) (k 0)))) (h "02xr8pjvnc7y3b7j59ckj137ir9h4f676kvm9096w1caxk551hda") (f (quote (("trace") ("assertions"))))))

(define-public crate-littlefs2-sys-0.1.7 (c (n "littlefs2-sys") (v "0.1.7") (d (list (d (n "cty") (r "^0.2.1") (d #t) (k 0)) (d (n "bindgen") (r "^0.56.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1x8mm5mlbvx2a9x03w6wjdw4srv4wxh641i5cdf439bcbxa4ypn2") (f (quote (("trace") ("assertions"))))))

