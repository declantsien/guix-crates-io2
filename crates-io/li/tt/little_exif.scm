(define-module (crates-io li tt little_exif) #:use-module (crates-io))

(define-public crate-little_exif-0.1.0 (c (n "little_exif") (v "0.1.0") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "0z7khs5hyi3skfzzdwyq0665x3w30n3xaraqdnq424v88gav6hkp") (y #t)))

(define-public crate-little_exif-0.1.1 (c (n "little_exif") (v "0.1.1") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.6.2") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "0fzs2522qxxx2myh55k4mgps9hz376vnzsmqjiaz6n91drqxcxhb") (y #t)))

(define-public crate-little_exif-0.2.0 (c (n "little_exif") (v "0.2.0") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "1nky31smfi210zmicnv4y19zzasm5l3s5wrzjk56icjp5jpx1p4j") (y #t)))

(define-public crate-little_exif-0.2.1 (c (n "little_exif") (v "0.2.1") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "1dyr7jgifa0r8pwmvmns4hz3awyfrpzwjg6byhf5g77h35rp34a3")))

(define-public crate-little_exif-0.3.0 (c (n "little_exif") (v "0.3.0") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "10p32aj1n6vdsz75brmlrslhmrpf9mqwxz3yk8i49adp7qp7si5d") (y #t)))

(define-public crate-little_exif-0.3.1 (c (n "little_exif") (v "0.3.1") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "0mihkr4rdf1820z3f3z9qz97jfw614syac5aygidyvk1rwcpprki") (y #t)))

(define-public crate-little_exif-0.3.2 (c (n "little_exif") (v "0.3.2") (d (list (d (n "crc") (r "^3.0.0") (d #t) (k 0)) (d (n "miniz_oxide") (r "^0.7.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.9") (d #t) (k 0)))) (h "0hdi9xmccyphwgn471lfy7fi15ii00qmy77ammmkvw0gn5xcm2af")))

