(define-module (crates-io li tt little) #:use-module (crates-io))

(define-public crate-little-0.1.0 (c (n "little") (v "0.1.0") (h "0hlm6qajbqf3c344rgyh1047vg81a4n9pyax4rxcvn9gy3h659b3")))

(define-public crate-little-0.1.1 (c (n "little") (v "0.1.1") (h "0iklai8f0mwp6ag60ylg6wzqrzr4sfcih5nwbj89ppxd0c73j495") (f (quote (("nightly"))))))

(define-public crate-little-0.2.0 (c (n "little") (v "0.2.0") (h "1y5qafa1aamfvn0l7lj92zrslla2b6b1vxfg5zqc5jljw16v6xa1") (f (quote (("nightly"))))))

(define-public crate-little-0.2.1 (c (n "little") (v "0.2.1") (h "1d29jh34rwp5rvnpsiqc5cwhcv95i83n3wyccrgccv1cjvb1zq09") (f (quote (("nightly"))))))

(define-public crate-little-0.2.2 (c (n "little") (v "0.2.2") (h "1vj69rsnwd6zwilp6sjfd2gdrwbiw4hz114wwl6ccm3dzjrpjqxn") (f (quote (("nightly"))))))

(define-public crate-little-0.2.3 (c (n "little") (v "0.2.3") (h "01dgsbrzjzih7faq7c5vvm6463hm3cfcpmm7x3hblhzxks6a3n5l") (f (quote (("nightly"))))))

(define-public crate-little-0.2.4 (c (n "little") (v "0.2.4") (h "1a9msv17phy38xb169b80qy9qd3gmgy5qi1igpc9x36ybw7nxacn") (f (quote (("nightly"))))))

(define-public crate-little-0.2.5 (c (n "little") (v "0.2.5") (h "03vgp3x9m6ksjy7hhgar9qy0pwvk0icx8l97dgl4az9kbyilxh3d") (f (quote (("nightly"))))))

(define-public crate-little-0.2.7 (c (n "little") (v "0.2.7") (h "1sgaa2v7azs09jkzzjmhv55xfaamskvw4kx8wsbimw5h709kvln2") (f (quote (("nightly"))))))

(define-public crate-little-0.3.0 (c (n "little") (v "0.3.0") (d (list (d (n "byteorder") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.3") (d #t) (k 2)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0a3h3c1lad7j58ar9ygi0h4gyyy6hlqmwihp1sc1r75yfdvh2b9g") (f (quote (("nightly"))))))

