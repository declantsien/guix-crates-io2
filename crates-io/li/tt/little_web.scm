(define-module (crates-io li tt little_web) #:use-module (crates-io))

(define-public crate-little_web-0.1.0 (c (n "little_web") (v "0.1.0") (d (list (d (n "dyn-clone") (r "^1.0.11") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "02s1zc1crd8lz56x0cv9ylij16bdr7j6jhw2ryq1wnznyrj0rgnb")))

