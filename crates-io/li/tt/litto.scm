(define-module (crates-io li tt litto) #:use-module (crates-io))

(define-public crate-litto-0.1.0 (c (n "litto") (v "0.1.0") (d (list (d (n "gcmodule") (r "^0.2.3") (d #t) (k 0)) (d (n "pest") (r "^2") (o #t) (d #t) (k 0)) (d (n "pest_derive") (r "^2") (o #t) (d #t) (k 0)))) (h "0yc10bwawbm2qiprw2ygg8qx63hblr22vqhx4x9a6v37q1ckhs3h") (f (quote (("tinylang") ("parse" "pest" "pest_derive") ("minilang" "tinylang") ("default" "parse" "tinylang" "minilang"))))))

