(define-module (crates-io li tt little-camera) #:use-module (crates-io))

(define-public crate-little-camera-0.1.0 (c (n "little-camera") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13") (d #t) (k 0)) (d (n "bevy-inspector-egui") (r "^0.22") (d #t) (k 0)))) (h "1psk9q63ahgsf7lmikwpygb4461hl5pwppvzwmzkvm9a869835fp")))

