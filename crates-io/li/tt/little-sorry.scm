(define-module (crates-io li tt little-sorry) #:use-module (crates-io))

(define-public crate-little-sorry-0.1.0 (c (n "little-sorry") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "rand_distr") (r "^0.2.2") (d #t) (k 0)))) (h "1j6dg308v0wf82s5pkyd5jm8n7azhrna5rd8dzms383011yb6dk3")))

(define-public crate-little-sorry-0.3.0 (c (n "little-sorry") (v "0.3.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.17.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "00cmp0n34pscqyaxy0mqz0pv7nk4sfb844f3nplaqqjdjf354p83")))

(define-public crate-little-sorry-0.4.0 (c (n "little-sorry") (v "0.4.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_distr") (r "^0.4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0kb5js1ijdfrmv0ga6i0l0w3b7kwk71iyx2y7wxdcksx5xgnv5cw")))

