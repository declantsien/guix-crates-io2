(define-module (crates-io li tt littletest) #:use-module (crates-io))

(define-public crate-littletest-0.1.0 (c (n "littletest") (v "0.1.0") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "06n6xw2ss6qi8hvi771snl6ny37xyvhc7vf9r5flgc5f058z9fkb")))

(define-public crate-littletest-0.1.1 (c (n "littletest") (v "0.1.1") (d (list (d (n "time") (r "*") (d #t) (k 0)))) (h "1sm9rxj8c1kmzgg4msvq9jh4fgchlbx42wbf9xylal5c2p5gl8wa")))

(define-public crate-littletest-0.2.0 (c (n "littletest") (v "0.2.0") (d (list (d (n "threadpool") (r "*") (f (quote ("scoped-pool"))) (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1ga799bhdqkgdl9h8xgz66m87hs0fyp7cm593zcn14q7fkln4m4r")))

(define-public crate-littletest-0.2.1 (c (n "littletest") (v "0.2.1") (d (list (d (n "rayon") (r "^0.5") (d #t) (k 0)))) (h "1xq40yyyncyh5z3c5pnwwy2pjhjgpvmlp5qfkds42wh365jl4mx4")))

(define-public crate-littletest-0.2.2 (c (n "littletest") (v "0.2.2") (d (list (d (n "rayon") (r "^0.5") (d #t) (k 0)))) (h "1najsv2rjvppbadhvvpcx5pmg9h9xkvivv9xlq25g3bmiaiggfd0")))

