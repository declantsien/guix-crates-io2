(define-module (crates-io li tt little_boxes) #:use-module (crates-io))

(define-public crate-little_boxes-1.2.0 (c (n "little_boxes") (v "1.2.0") (d (list (d (n "docopt") (r "^0.6.66") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0ihzz5wq5mkvk6vfzsyj9m96vmq6r20nz44dh59pf4vj52svwspw")))

(define-public crate-little_boxes-1.4.0 (c (n "little_boxes") (v "1.4.0") (d (list (d (n "docopt") (r "^0.6.66") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "1czn9sn4cazcf175w55jashjmhjyd29mpvj15lq4zra6wisha9h7")))

(define-public crate-little_boxes-1.5.0 (c (n "little_boxes") (v "1.5.0") (d (list (d (n "docopt") (r "^0.6.66") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.15") (d #t) (k 0)))) (h "0ynk0c1l1b3smhmjqf7f3xj8fgvrm8pacr18lpngyg7yg91b8gkh")))

(define-public crate-little_boxes-1.6.0 (c (n "little_boxes") (v "1.6.0") (d (list (d (n "docopt") (r "^0.8.1") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.12") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.12") (d #t) (k 0)))) (h "1k764bxsvhdnnn5g8ynx6m1rbg6ll4v6pxlh4qlkpk8cb1rilfah")))

(define-public crate-little_boxes-1.8.0 (c (n "little_boxes") (v "1.8.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("default" "cargo"))) (d #t) (k 0)) (d (n "clap") (r "^4.3.19") (f (quote ("default" "cargo"))) (d #t) (k 1)) (d (n "clap_mangen") (r "^0.2.12") (d #t) (k 1)) (d (n "strip-ansi-escapes") (r "^0.1.1") (d #t) (k 0)) (d (n "trycmd") (r "^0.14.16") (f (quote ("default"))) (d #t) (k 0)))) (h "0l8j055d0cqpx74gm7fwhzyvxjpq8v9d4sn8gsjxpdm1b82dyd6l")))

