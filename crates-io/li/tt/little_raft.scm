(define-module (crates-io li tt little_raft) #:use-module (crates-io))

(define-public crate-little_raft-0.1.1 (c (n "little_raft") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "timer") (r "^0.1.3") (d #t) (k 0)))) (h "1wqjscw22wkjwzhi9ghyvgcd273a1c57qspv3k3sjb3xx57biasa")))

(define-public crate-little_raft-0.1.2 (c (n "little_raft") (v "0.1.2") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "timer") (r "^0.1.3") (d #t) (k 0)))) (h "16bp5zy3kwz6gk0zjb9p1a048nfiqqsxdsgl52x5zsapv1qgyp36")))

(define-public crate-little_raft-0.1.3 (c (n "little_raft") (v "0.1.3") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "timer") (r "^0.1.3") (d #t) (k 0)))) (h "1973i5nf6lgpfq40i2pqxn6n99rc3vsv78d43rnsp0sa2pm60x3x")))

(define-public crate-little_raft-0.1.4 (c (n "little_raft") (v "0.1.4") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "timer") (r "^0.1.3") (d #t) (k 0)))) (h "1i8dlggfq12d0cib7d7a36vyyyzgfn2dhlgi40b8kyw59zjfbd8n")))

(define-public crate-little_raft-0.1.5 (c (n "little_raft") (v "0.1.5") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "timer") (r "^0.1.3") (d #t) (k 0)))) (h "0579108b01b3nxm8k8dlr82mhnriy5icbhqf6iz5nlvj9ny6g8jm")))

(define-public crate-little_raft-0.1.6 (c (n "little_raft") (v "0.1.6") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "timer") (r "^0.1.3") (d #t) (k 0)))) (h "03rdwinahjrdna7cgr3jhsga5p0wf12kbz2jghhp0qrisdydmssw")))

(define-public crate-little_raft-0.2.0 (c (n "little_raft") (v "0.2.0") (d (list (d (n "bytes") (r "^0.4.7") (d #t) (k 0)) (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "time") (r "^0.1.39") (d #t) (k 0)) (d (n "timer") (r "^0.1.3") (d #t) (k 0)))) (h "1599mcw04skl2dfac8878difni6l770fhx5xwqdkf46427mxb0y2")))

