(define-module (crates-io li tt littlefs) #:use-module (crates-io))

(define-public crate-littlefs-0.1.0 (c (n "littlefs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "littlefs-sys") (r "^0.1") (d #t) (k 0)))) (h "1vdpdl8vqmkbzm8jdcixhlhk1yxfkjf49bhka7wacxyyg7ac74h2")))

(define-public crate-littlefs-0.2.0 (c (n "littlefs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0.4") (d #t) (k 0)) (d (n "cty") (r "^0.1.5") (d #t) (k 0)) (d (n "littlefs-sys") (r "^0.2") (d #t) (k 0)))) (h "154aw7gdicrzqqkhd4y1lfmm0nvmzl3jp4m5cq22w2pw9fh37n0r")))

