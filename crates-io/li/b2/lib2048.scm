(define-module (crates-io li b2 lib2048) #:use-module (crates-io))

(define-public crate-lib2048-0.0.1 (c (n "lib2048") (v "0.0.1") (d (list (d (n "console") (r "^0.6") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "193aj1kyayc5kwrksiwy0h3dqqajpm9m5fv77s6c6gcwvx88mwfz")))

(define-public crate-lib2048-0.0.2 (c (n "lib2048") (v "0.0.2") (d (list (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "04ljkqx81dyv6w4049hssjxx5yjq1f1w1bbyfvf922craag30gvf")))

