(define-module (crates-io li ve live2d) #:use-module (crates-io))

(define-public crate-live2d-0.1.0 (c (n "live2d") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "live2d-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.27") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1j5hn867k6rpxacj6ixbw7gmvvgzf6gp6m6x8pzxv6bym8n1z9x6") (y #t)))

