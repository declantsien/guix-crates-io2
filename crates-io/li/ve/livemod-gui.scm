(define-module (crates-io li ve livemod-gui) #:use-module (crates-io))

(define-public crate-livemod-gui-0.1.0 (c (n "livemod-gui") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "egui") (r "^0.12.0") (d #t) (k 0)) (d (n "egui_glium") (r "^0.12.0") (d #t) (k 0)) (d (n "glium") (r "^0.29.1") (d #t) (k 0)) (d (n "livemod") (r "^0.1") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)))) (h "0dvkrqsahcmk7kq669xid4hif3an4jcgpyfcvsp0gndamgk1j5f7")))

(define-public crate-livemod-gui-0.2.0 (c (n "livemod-gui") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "egui") (r "^0.12.0") (d #t) (k 0)) (d (n "egui_glium") (r "^0.12.0") (d #t) (k 0)) (d (n "glium") (r "^0.29.1") (d #t) (k 0)) (d (n "livemod") (r "^0.2") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)))) (h "1d8miir9ghka2yj18kjx3d4xlzxdkgk0xyc7nv6fzir1s5h8nlr7")))

(define-public crate-livemod-gui-0.3.0 (c (n "livemod-gui") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "egui") (r "^0.12.0") (d #t) (k 0)) (d (n "egui_glium") (r "^0.12.0") (d #t) (k 0)) (d (n "glium") (r "^0.29.1") (d #t) (k 0)) (d (n "livemod") (r "^0.3") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)))) (h "1braj5qjbg6h0kng6bqpvkrcgpjdksz5y3far8vbj106n1z43rkg")))

(define-public crate-livemod-gui-0.4.0 (c (n "livemod-gui") (v "0.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "egui_glium") (r "^0.13") (d #t) (k 0)) (d (n "glium") (r "^0.30") (d #t) (k 0)) (d (n "livemod") (r "^0.4") (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)))) (h "0pf8bbjnasdlxpz3a0m63hnk2dd10qp3r3kvnppd50gg2v9j4xmi")))

(define-public crate-livemod-gui-0.5.0 (c (n "livemod-gui") (v "0.5.0") (d (list (d (n "color-eyre") (r "^0.5") (k 0)) (d (n "egui") (r "^0.13") (d #t) (k 0)) (d (n "egui_glium") (r "^0.13") (d #t) (k 0)) (d (n "glium") (r "^0.30") (d #t) (k 0)) (d (n "hashlink") (r "^0.7") (d #t) (k 0)) (d (n "io_tee") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "livemod") (r "^0.5") (d #t) (k 0)))) (h "0q0ai066nj3j9mfzni653v79brqjzqzqqzab6jzw73mrm0yjbqg5")))

