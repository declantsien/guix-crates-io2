(define-module (crates-io li ve livejq) #:use-module (crates-io))

(define-public crate-livejq-1.0.0 (c (n "livejq") (v "1.0.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17zybc2n4va04gd4s2zmjdyxg09crqwkgfvcgs68hkxfkdgisz3s")))

(define-public crate-livejq-1.0.1 (c (n "livejq") (v "1.0.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1637il06via27bfba23d99vmijmsslwn41m1qrb22i1srmnpm5a9")))

(define-public crate-livejq-1.0.2 (c (n "livejq") (v "1.0.2") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p5ld5yriyfl4545205b9nc6hn7az6fb8bqm6n6kpx8z214f76yr")))

(define-public crate-livejq-1.1.0 (c (n "livejq") (v "1.1.0") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0aslwpd9n9ff2mav4cbyri2s9x31wyf61c40zzr83s908rzb3yvy")))

(define-public crate-livejq-1.1.1 (c (n "livejq") (v "1.1.1") (d (list (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1kf7i1500sv2fhps0zdkfqf021fr8zp683qqlcnd2wiw9xbrg972")))

