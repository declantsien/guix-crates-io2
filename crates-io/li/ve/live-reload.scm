(define-module (crates-io li ve live-reload) #:use-module (crates-io))

(define-public crate-live-reload-0.1.0 (c (n "live-reload") (v "0.1.0") (d (list (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "1jf969nhnx2sni38xdz8gcn1wprb5r6qm7x3krwd3s1adwqyny7f")))

(define-public crate-live-reload-0.2.0 (c (n "live-reload") (v "0.2.0") (d (list (d (n "libloading") (r "^0.4") (d #t) (k 0)) (d (n "notify") (r "^4.0") (d #t) (k 0)))) (h "1rlxizwmydwkmj1dyrc6mni2grs2ns492z4fx22jys7l68803d98")))

