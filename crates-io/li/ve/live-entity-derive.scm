(define-module (crates-io li ve live-entity-derive) #:use-module (crates-io))

(define-public crate-live-entity-derive-0.0.1 (c (n "live-entity-derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0cbg19w68wnafp316qkkafwz5dm1wf9xwxlck08b8hbq9zcdq65w")))

(define-public crate-live-entity-derive-0.0.2 (c (n "live-entity-derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "176dj9w9s5jp43fays95nydwarl39z2k6vis7hpqnbajdiafvhnk") (y #t)))

(define-public crate-live-entity-derive-0.0.3 (c (n "live-entity-derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1x47h0v4r1qz94xgdbialp55w79yrd7j7kjk8wa6f1wbnbcyjkrq") (y #t)))

(define-public crate-live-entity-derive-0.0.4 (c (n "live-entity-derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "07y0c7kpddqsvbb6hvqmmsliqa90kvp028872cb2x1dl3j5dvv9w") (y #t)))

(define-public crate-live-entity-derive-0.0.5 (c (n "live-entity-derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0i51p76xlmynakxqnnva6dklllvxc87hgrl4fiifw96i86ghjqxw") (y #t)))

(define-public crate-live-entity-derive-0.0.6 (c (n "live-entity-derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0mqcinbky99qz3hm921m8jp7jwqsx49m8q3a9drzas7s17n02h2a")))

(define-public crate-live-entity-derive-0.0.7 (c (n "live-entity-derive") (v "0.0.7") (d (list (d (n "proc-macro2") (r "^1.0.67") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "1s20zdqpz8mjkgds0nl5gldk8y71f7mdw40mjxpsfp6qjb5dmnsw")))

