(define-module (crates-io li ve livesplit-auto-splitting) #:use-module (crates-io))

(define-public crate-livesplit-auto-splitting-0.1.0 (c (n "livesplit-auto-splitting") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.45") (k 0)) (d (n "proc-maps") (r "^0.3.0") (k 0)) (d (n "read-process-memory") (r "^0.1.4") (k 0)) (d (n "slotmap") (r "^1.0.2") (k 0)) (d (n "snafu") (r "^0.7.0") (d #t) (k 0)) (d (n "sysinfo") (r "^0.27.0") (f (quote ("multithread"))) (k 0)) (d (n "time") (r "^0.3.3") (k 0)) (d (n "wasmtime") (r "^4.0.0") (f (quote ("cranelift" "parallel-compilation"))) (k 0)) (d (n "wasmtime-wasi") (r "^4.0.0") (o #t) (d #t) (k 0)))) (h "1hap23n24bqd1b9q13wxhn8da2rrj1v5qpfk74mp8l41w66s7c3y") (f (quote (("unstable" "wasmtime-wasi"))))))

