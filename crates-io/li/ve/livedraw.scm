(define-module (crates-io li ve livedraw) #:use-module (crates-io))

(define-public crate-livedraw-0.1.0 (c (n "livedraw") (v "0.1.0") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.12.1") (d #t) (k 0)))) (h "15p5j6a4akdkxym4gy8gsmdyj3m4fxkjzprq3skygq7n6dqsyvcv")))

(define-public crate-livedraw-0.1.1 (c (n "livedraw") (v "0.1.1") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.12.1") (d #t) (k 0)))) (h "1rydx72yrziqy7p7f66bkacgpb41pmf73j8qpcfl2cjq6wjz0s0r")))

(define-public crate-livedraw-0.1.2 (c (n "livedraw") (v "0.1.2") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.12.1") (d #t) (k 0)))) (h "0vfszg9y0bfy741q7g77z6ak1vrp1nkgn5y4v3vd2076gzca6gs2")))

(define-public crate-livedraw-0.2.0 (c (n "livedraw") (v "0.2.0") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.12.1") (d #t) (k 0)))) (h "1qd2k2c3g7fhscd2gg24cymjnh4z0ssxg7varfir281mnshn48l1")))

(define-public crate-livedraw-0.2.1 (c (n "livedraw") (v "0.2.1") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.12.1") (d #t) (k 0)))) (h "1la3amix6gqwjafy7gi2lazvdc2v35wrv0ay030d2g7nhvk4pzyb")))

(define-public crate-livedraw-0.2.2 (c (n "livedraw") (v "0.2.2") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.13") (d #t) (k 0)))) (h "1m4608wri2l4z3pry257granj3mvh5ns02qnydqi5wk2wynai9ic")))

(define-public crate-livedraw-0.2.3 (c (n "livedraw") (v "0.2.3") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.13") (d #t) (k 0)))) (h "1ydfy9v3c014zfibx9v8mjdlawnw6gx6cadj2xg91fzs5fgs41ab")))

(define-public crate-livedraw-0.2.4 (c (n "livedraw") (v "0.2.4") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.13") (d #t) (k 0)))) (h "1825dz7mym6f69cqarvf7f42fymyhj0iacj0f4sxdxf6dzf7qhhq")))

(define-public crate-livedraw-0.2.5 (c (n "livedraw") (v "0.2.5") (d (list (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.13") (d #t) (k 0)))) (h "0lnizcyfv5dq0xlb19a83ry554k3dwqd0yzix4hvk7yywc556qy9")))

(define-public crate-livedraw-0.3.0 (c (n "livedraw") (v "0.3.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "isahc") (r "^1.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "svg") (r "^0.13") (d #t) (k 0)))) (h "0bfszwscinv0md27b7l1q4bi7p31zby1h5qyaavxg953cckyh2ig")))

