(define-module (crates-io li ve livemod-derive) #:use-module (crates-io))

(define-public crate-livemod-derive-0.1.0 (c (n "livemod-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1dhzq6x0kp8z2j7xr96gmiaqlhgjyvass1p5n00qvdjnbhxizf94")))

(define-public crate-livemod-derive-0.2.0 (c (n "livemod-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g4n281bm3jvrppjql97lzis4lrr9hapgn3l2skzfl3qkb5crc2m")))

(define-public crate-livemod-derive-0.3.0 (c (n "livemod-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1d3qdhngyxf5fvg4nvchq4yvv64xigyw1r1hdg2ifibk3j5ik73i")))

(define-public crate-livemod-derive-0.4.0 (c (n "livemod-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1y8b3av78kw66jx81phxnqmsfjwgmzg9hxc6nhx0z95z72vp6zn3")))

(define-public crate-livemod-derive-0.5.0 (c (n "livemod-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1r99xdawxwf63i5ab51srk08x3mzl86j4y0drjw8ls5cjp7cyyvh")))

