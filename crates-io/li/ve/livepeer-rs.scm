(define-module (crates-io li ve livepeer-rs) #:use-module (crates-io))

(define-public crate-livepeer-rs-0.0.1 (c (n "livepeer-rs") (v "0.0.1") (d (list (d (n "async-std") (r "^1.8.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "ffmpeg-next") (r "^5.0.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "surf") (r "^2.3.2") (d #t) (k 0)))) (h "1w0vhndkpkayvfhg6dv1bii19ma6g6pgbi420qg07ahvwj67dhf4")))

