(define-module (crates-io li ve livesplit-title-abbreviations) #:use-module (crates-io))

(define-public crate-livesplit-title-abbreviations-0.1.0 (c (n "livesplit-title-abbreviations") (v "0.1.0") (d (list (d (n "unicase") (r "^2.2.0") (d #t) (k 0)))) (h "0z37w71i2fj6j2bxrlp1xkadpdkq2fm6d4cswirqqifb6rd1hy0g")))

(define-public crate-livesplit-title-abbreviations-0.2.0 (c (n "livesplit-title-abbreviations") (v "0.2.0") (d (list (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "12393fkbgzhg46wcgy2svvwh8032my7nc7qdfm7qb6m1hlqli0qr")))

(define-public crate-livesplit-title-abbreviations-0.3.0 (c (n "livesplit-title-abbreviations") (v "0.3.0") (d (list (d (n "unicase") (r "^2.6.0") (d #t) (k 0)))) (h "0mdanr4px9n8f3aw119cxcxinjflnnn45ki0x4dkvdbl52h1x149")))

