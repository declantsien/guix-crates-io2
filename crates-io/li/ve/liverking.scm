(define-module (crates-io li ve liverking) #:use-module (crates-io))

(define-public crate-liverking-0.1.0 (c (n "liverking") (v "0.1.0") (h "025hzn0165smb73bbspsl7msrxzjn2pdfqi2hzml58zpyyqa6hm8")))

(define-public crate-liverking-0.1.1 (c (n "liverking") (v "0.1.1") (h "01gdnjndy9lxdfrn5c6a87i2lllig5ydqns818f2sq3x2rb8rb0l")))

