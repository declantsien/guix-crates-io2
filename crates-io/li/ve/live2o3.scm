(define-module (crates-io li ve live2o3) #:use-module (crates-io))

(define-public crate-live2o3-0.0.1 (c (n "live2o3") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "rml_rtmp") (r "^0.3.1") (d #t) (k 0)) (d (n "slab") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.20") (f (quote ("full"))) (d #t) (k 0)))) (h "0fqw8nrsrf2qx0xzig4p1caqrizrayv407x5dbljpaw5rwk7m4wh")))

