(define-module (crates-io li ve livesort) #:use-module (crates-io))

(define-public crate-livesort-0.1.0 (c (n "livesort") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0gl778qlyhq6avyprkc2m2x17ani3wmnvnv3qxa7qdvsxnz0npam")))

(define-public crate-livesort-0.2.0 (c (n "livesort") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0sxpvnaaxagsdaf441ls81cbi5jm2kgi871klbmcw4248bsr62ac")))

(define-public crate-livesort-0.3.0 (c (n "livesort") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "1i1xjj4xq28qwswi4j0z4ggjg0nk9i6yggykq19i3ghpqiwml765")))

(define-public crate-livesort-0.4.0 (c (n "livesort") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "09fll1k7nqqvik6a2s4y61hg1586xjp8kambxsl011ayl86s7wdl")))

(define-public crate-livesort-0.4.1 (c (n "livesort") (v "0.4.1") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "liveterm") (r "^0.2.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.21") (d #t) (k 0)))) (h "0dh6lyygdjbj97gd81qz9h9akxf8j2dcgzp5s158hlc8ax6an0f8")))

