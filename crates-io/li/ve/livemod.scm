(define-module (crates-io li ve livemod) #:use-module (crates-io))

(define-public crate-livemod-0.1.0 (c (n "livemod") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "livemod-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0a2mpck1x51q5mqsj7lsjglmyyxni0np7cailpsip6n2bcg5c6kh") (f (quote (("disabled") ("derive" "livemod-derive"))))))

(define-public crate-livemod-0.2.0 (c (n "livemod") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "livemod-derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1l7d0mlxgnwq13qckda7nkxin846z0fym1jbvz4k6nzdnw1s2ryp") (f (quote (("disabled") ("derive" "livemod-derive"))))))

(define-public crate-livemod-0.3.0 (c (n "livemod") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "livemod-derive") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "12cplm1gj556flap4qgr56qrqdhvb8zp79f0q24zmcm53vrms7p8") (f (quote (("disabled") ("derive" "livemod-derive"))))))

(define-public crate-livemod-0.4.0 (c (n "livemod") (v "0.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "livemod-derive") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "nanoserde") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1d757j0bqnj115rccqv9k7081l8ws48hn1j5hlk2lczamh35y78x") (f (quote (("disabled") ("derive" "livemod-derive"))))))

(define-public crate-livemod-0.5.0 (c (n "livemod") (v "0.5.0") (d (list (d (n "hashlink") (r "^0.7") (d #t) (k 0)) (d (n "livemod-derive") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "0jymgl984v7nxynz721ck9l10396rz7i5xgdagsjgscqsv1jnqz5") (f (quote (("disabled") ("derive" "livemod-derive"))))))

