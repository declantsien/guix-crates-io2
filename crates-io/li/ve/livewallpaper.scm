(define-module (crates-io li ve livewallpaper) #:use-module (crates-io))

(define-public crate-livewallpaper-0.1.0 (c (n "livewallpaper") (v "0.1.0") (h "0fcgk538sjsr0567c7kkiswclcq85wd6srh1snlyimm7qs4m8c1w")))

(define-public crate-livewallpaper-0.1.1 (c (n "livewallpaper") (v "0.1.1") (h "101kmzc90j6sdjp9i7cf5zq57snizmgwzxk0da109qxy0lbaahhx")))

