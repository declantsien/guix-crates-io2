(define-module (crates-io li ve liver) #:use-module (crates-io))

(define-public crate-liver-0.1.0 (c (n "liver") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "ws") (r "^0.9") (d #t) (k 0)))) (h "0r5wzi1m69ymvkxy70ggz0749a2qmg4w367sc9k896bk0xdw7af8") (y #t)))

(define-public crate-liver-0.1.1 (c (n "liver") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "hotwatch") (r "^0.4") (d #t) (k 0)) (d (n "rocket") (r "^0.4") (d #t) (k 0)) (d (n "ws") (r "^0.9") (d #t) (k 0)))) (h "01lchx90q790wpal5qf106ck1rnk39kgqni06avpdmmi97i3nhir") (y #t)))

