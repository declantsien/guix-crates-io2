(define-module (crates-io li ve liveterm) #:use-module (crates-io))

(define-public crate-liveterm-0.1.0 (c (n "liveterm") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)))) (h "0557b3nxb8i14x4h6pyz69f2s67s918vbb4hcz5d03k289qigsxp")))

(define-public crate-liveterm-0.2.0 (c (n "liveterm") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.19.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)) (d (n "vte") (r "^0.10.0") (d #t) (k 0)))) (h "04flbhg4h80b1h694sg8rxz015p5rhjhl2kwgzb4f08ghyf9jjs3")))

(define-public crate-liveterm-0.3.0 (c (n "liveterm") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.20.0") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.8") (d #t) (k 0)) (d (n "vte") (r "^0.10.0") (d #t) (k 0)))) (h "1piwnimzv6ggi7zsr9r8kz2qp1x12klsqmxwzgrqipqfb295h98i")))

