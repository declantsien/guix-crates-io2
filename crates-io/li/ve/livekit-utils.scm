(define-module (crates-io li ve livekit-utils) #:use-module (crates-io))

(define-public crate-livekit-utils-0.1.0 (c (n "livekit-utils") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zq93gaqs0hava9q8xv0qvz1plzczpf4l9nqhnjw6xrb4cs8cg3x") (y #t)))

(define-public crate-livekit-utils-0.1.1 (c (n "livekit-utils") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qis6xld7qa72z42dz9pdapad6l5mv6lnymblbs2mlm89nw3yd6q") (y #t)))

