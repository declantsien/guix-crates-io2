(define-module (crates-io li ev lievre) #:use-module (crates-io))

(define-public crate-lievre-0.0.1 (c (n "lievre") (v "0.0.1") (h "0kipj27gwhs5k06p153xfaxvy27nwbssnc219nmjsz9anf9mxk6v")))

(define-public crate-lievre-0.0.2 (c (n "lievre") (v "0.0.2") (h "0smyywrc4l9abxb06g9kmv5w4sywjn2nzkf8w6y0z2dhlzqi0r66")))

(define-public crate-lievre-0.0.3 (c (n "lievre") (v "0.0.3") (h "1dny582avj9xha9klgvav0ixiwvyvbwa1sy9cs7ipir43mllw5mx")))

