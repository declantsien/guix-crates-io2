(define-module (crates-io li si lisi) #:use-module (crates-io))

(define-public crate-lisi-0.2.0 (c (n "lisi") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "asciidoctrine") (r "^0.1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rhai") (r "^1.3") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "topological-sort") (r "^0.2") (d #t) (k 0)))) (h "1rnagj8yl8byli5qk5p8153x2mcm3yb52w6v9a6x0gqij08jp8mq")))

