(define-module (crates-io li ke likeness) #:use-module (crates-io))

(define-public crate-likeness-1.0.0 (c (n "likeness") (v "1.0.0") (h "091dk18s2yrdfpbkikzn1rhpqlp38iwfg6r2vq4dr2ysvb97m5h3")))

(define-public crate-likeness-1.1.0 (c (n "likeness") (v "1.1.0") (h "16xq33624075a8d072cx8rnzz7vvpjaqkxijv7yda4p7904mv85n")))

