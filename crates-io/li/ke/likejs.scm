(define-module (crates-io li ke likejs) #:use-module (crates-io))

(define-public crate-likejs-0.1.0 (c (n "likejs") (v "0.1.0") (h "1sjb30pknacw7z111wl1yzh592v8zq62bxy37c4j0f3n48ic2q0i") (y #t)))

(define-public crate-likejs-0.1.1 (c (n "likejs") (v "0.1.1") (h "1kw2cy413gv82fl9ca7nm5liq4msbavvp87yqx5ikzzky3knzs1n") (y #t)))

(define-public crate-likejs-0.1.2 (c (n "likejs") (v "0.1.2") (h "1qrv6yw8z583bpifn07qs52vwmskdyn3xrjbx6y6nk3mw0j7fp9s")))

