(define-module (crates-io li ke likely_stable) #:use-module (crates-io))

(define-public crate-likely_stable-0.1.0 (c (n "likely_stable") (v "0.1.0") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "00ags6bsxr4w7jl04lrx6iycwcyk4k6raz2dc1mpl89bxm6h6s25") (f (quote (("default") ("check_assembly")))) (y #t)))

(define-public crate-likely_stable-0.1.1 (c (n "likely_stable") (v "0.1.1") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "108w5nqr3s30pp59mva95kqbh07xrlbcjp6ad2w7yiynfvpdj0yp") (f (quote (("default") ("check_assembly")))) (y #t)))

(define-public crate-likely_stable-0.1.2 (c (n "likely_stable") (v "0.1.2") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)))) (h "0sny4jgvix0hyl38d3f210dxppc0gj9w3xniak2851jn3dg21av9") (f (quote (("default") ("check_assembly"))))))

