(define-module (crates-io li ke likemod) #:use-module (crates-io))

(define-public crate-likemod-0.1.0 (c (n "likemod") (v "0.1.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1zdn8pv175d0jnsn1q0y3p1fb2zbd7p7p1w46pjann48i5fzipfr")))

(define-public crate-likemod-0.2.0 (c (n "likemod") (v "0.2.0") (d (list (d (n "errno") (r "^0.2") (d #t) (k 0)) (d (n "error-chain") (r "^0.12") (k 0)) (d (n "futures") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0n2117bjkgjj6fxlvz6q6y6wx6dsc4d2srll9y2fp6hzl71v8qka") (f (quote (("async" "futures" "tokio"))))))

