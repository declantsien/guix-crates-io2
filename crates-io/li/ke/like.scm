(define-module (crates-io li ke like) #:use-module (crates-io))

(define-public crate-like-0.1.0 (c (n "like") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1iq9zfjdqknlszsryqmxcgbgnbjb78znqi9znwvrna97dwxbpbay")))

(define-public crate-like-0.1.1 (c (n "like") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "0hzaag8v7sz1blv792bwa9k9wga6vnkzg2s0cjnb60k07dkcirbb")))

(define-public crate-like-0.1.2 (c (n "like") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "1yxg4km2fwq84znxwdwkcahsnszgg53s2rxnndr0g487jcvf0y4d")))

(define-public crate-like-0.2.0 (c (n "like") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)))) (h "05dfi91cn8a52jhqhnl7jd0dl7db8fpdgcmy79402p1vyblihg9f")))

(define-public crate-like-0.2.1 (c (n "like") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1shng1dkj93k26x73pccig44mvlcpk1vhcdfqfvd6wqi9ik05cdn")))

(define-public crate-like-0.3.0 (c (n "like") (v "0.3.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0a3yi1kysc8aa2cg3m1ynhs2g9rq4jyvi34bijs0isn8ip7q5v2b")))

(define-public crate-like-0.3.1 (c (n "like") (v "0.3.1") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "0w0snb3l0p3im5nhyi8s4m8dwr0lsafw99v36khgm8dinbj82wpw")))

