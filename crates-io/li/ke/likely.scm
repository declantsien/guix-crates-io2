(define-module (crates-io li ke likely) #:use-module (crates-io))

(define-public crate-likely-0.1.0 (c (n "likely") (v "0.1.0") (h "1xdr1gm1aywy9bv19rjpdbidxwcmrslmisgjcyxndh18fm674xm3")))

(define-public crate-likely-0.2.0 (c (n "likely") (v "0.2.0") (h "190cds33apb6w2dgzglcphzh4qz87l23rzm2vbg6608ndwb24dwx")))

