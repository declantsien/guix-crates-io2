(define-module (crates-io li bz libzfs_core-sys) #:use-module (crates-io))

(define-public crate-libzfs_core-sys-0.1.0 (c (n "libzfs_core-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1.2") (d #t) (k 0)))) (h "05g873fjwx4zz4220drxzvzhzfs1fl5vw9xgvx1qnw5px7bkqg0w")))

