(define-module (crates-io li bz libzbase32) #:use-module (crates-io))

(define-public crate-libzbase32-1.0.0 (c (n "libzbase32") (v "1.0.0") (h "0g2lg36wishpk58iz3my9363lmpxvcwg8p33m130z0i7avhndm9c") (f (quote (("std") ("default" "std"))))))

(define-public crate-libzbase32-1.1.0 (c (n "libzbase32") (v "1.1.0") (h "0b2l530rykhca76fnpnz2wbj7ipmw7sbzsqjjgjhpa54c5hvi4k2") (f (quote (("std") ("default" "std"))))))

(define-public crate-libzbase32-2.0.0 (c (n "libzbase32") (v "2.0.0") (h "0wcpql8a9dg2a180fqm37dpzz50sdxx3hx144j0v6b7yaj1gbpgd") (f (quote (("std") ("default" "std"))))))

(define-public crate-libzbase32-2.0.1 (c (n "libzbase32") (v "2.0.1") (h "0l42wn8lmkr3ir1r6xsg7a444xg4bl8833cwd9ajjb0jxvh3f85z") (f (quote (("std") ("default" "std"))))))

