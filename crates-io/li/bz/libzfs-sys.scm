(define-module (crates-io li bz libzfs-sys) #:use-module (crates-io))

(define-public crate-libzfs-sys-0.1.0 (c (n "libzfs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "022j8vpzp219ma4pgzlr2cgn64hzlfdibp2gxm4x5p49f6gdw22f")))

(define-public crate-libzfs-sys-0.2.0 (c (n "libzfs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "02rn1rwvfk227h4q1761bx3lfrj2xyjaircmh7v316av79sx1d5j")))

(define-public crate-libzfs-sys-0.3.0 (c (n "libzfs-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1h7ackwgnl2jd8q2clriv9f8plz81zyjkmjq33yghlz3mbs4230q")))

(define-public crate-libzfs-sys-0.4.0 (c (n "libzfs-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0alv9qnarp56qy7127psfhpd4wy5an2aw7r2z3lk8b7da8i7jgz8")))

(define-public crate-libzfs-sys-0.5.0 (c (n "libzfs-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0xqdqw581mynqxsf47fwvz83dnlcbnblax66ny37zzlc5b21nr83")))

(define-public crate-libzfs-sys-0.5.1 (c (n "libzfs-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ms02nk56mr2ssz59xqqqh6cg8lnchdhqzqy9g593lj7r43cvsdg")))

(define-public crate-libzfs-sys-0.5.2 (c (n "libzfs-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06iid4fs0sz9smc66z8zgqzh38sc9amqxyhz56dy5ifji4hwg276")))

(define-public crate-libzfs-sys-0.5.3 (c (n "libzfs-sys") (v "0.5.3") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "01178r761kg2dzpg715clfhfyivaj9426h79vrbp5izha316bgsz")))

(define-public crate-libzfs-sys-0.5.4 (c (n "libzfs-sys") (v "0.5.4") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0d7h84gbw7xpf2a9j1zsa5kl7yp5nc5qd1dj6j8qdm918sa85n3q")))

(define-public crate-libzfs-sys-0.5.5 (c (n "libzfs-sys") (v "0.5.5") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "028d0kjzb94ys48gxslrqyw6i3pkid9kjqa6vrcls8swf38lxdim")))

(define-public crate-libzfs-sys-0.5.6 (c (n "libzfs-sys") (v "0.5.6") (d (list (d (n "bindgen") (r "^0.36.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "1vgp3iw3np6fsb2098jq8jkinwwb7h1czhq8qc632milm9n793m0") (l "zfs")))

(define-public crate-libzfs-sys-0.5.7 (c (n "libzfs-sys") (v "0.5.7") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "019chgrps10a27g7vc8mangfhj03kvp96yl8q1205nf85wr0crif") (l "zfs")))

(define-public crate-libzfs-sys-0.5.8 (c (n "libzfs-sys") (v "0.5.8") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1kxccjk60lwm3drmpxigwnz8vri7j2aksq2q9myiikpz2d92xzdv") (l "zfs")))

(define-public crate-libzfs-sys-0.5.9 (c (n "libzfs-sys") (v "0.5.9") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0v1iqis7iv2p6w564s31qk0xm5wv77nr68lp966ysx4skar41cri") (l "zfs")))

(define-public crate-libzfs-sys-0.5.10 (c (n "libzfs-sys") (v "0.5.10") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "07g7l52xgw2vi93jcsbr2nnj9dgixa1dng0n1qmwmk7w185gfypi") (l "zfs")))

(define-public crate-libzfs-sys-0.5.11 (c (n "libzfs-sys") (v "0.5.11") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "nvpair-sys") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.15") (d #t) (k 1)))) (h "1k7z1rw407ns5mq93gfrwq0r58xwncfjwsysqp2xbxswvqbsbsg3") (l "zfs")))

