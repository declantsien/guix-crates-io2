(define-module (crates-io li bz libzfs-types) #:use-module (crates-io))

(define-public crate-libzfs-types-0.1.0 (c (n "libzfs-types") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "00fn7agmb677x3r2yw8ssd88hp3xidx7jlg1w88yx5jwrhfbgswy")))

(define-public crate-libzfs-types-0.1.1 (c (n "libzfs-types") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "1jlv6ylqcmfzqdw6rlw54sf0fmgq5w59ifilzgip723if473l7kq")))

(define-public crate-libzfs-types-0.1.2 (c (n "libzfs-types") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "0z33la4hdwngfgyh7jnx93n1vq3k7r75njyc9l1k7dsx3l14217s")))

