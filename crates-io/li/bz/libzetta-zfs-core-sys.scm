(define-module (crates-io li bz libzetta-zfs-core-sys) #:use-module (crates-io))

(define-public crate-libzetta-zfs-core-sys-0.5.1 (c (n "libzetta-zfs-core-sys") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.4") (d #t) (k 0)) (d (n "build-env") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "01w1r739c16h12sigxhcvj47qzfb43mrg45ry44x9wlah2q1i057")))

(define-public crate-libzetta-zfs-core-sys-0.5.2 (c (n "libzetta-zfs-core-sys") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.4") (d #t) (k 0)) (d (n "build-env") (r "^0.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1j6lpyyx66z55iszxrf8q9jsmziwdjzqsvrnsa6xscf7s81xafaq")))

