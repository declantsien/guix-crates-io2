(define-module (crates-io li bz libzip-sys) #:use-module (crates-io))

(define-public crate-libzip-sys-0.1.0 (c (n "libzip-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "06gfx5lp59ynl89kaqvz10iys6l03jb8hrh5bgsgra761a2bnvxl") (l "zip")))

(define-public crate-libzip-sys-0.1.1 (c (n "libzip-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "bzip2-sys") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "libz-sys") (r "^1.0.25") (f (quote ("static"))) (d #t) (k 0)))) (h "0na288m65740md4bvy60y5dz1qgfs5cg7615s1jwqkp3n3dbgm8x") (f (quote (("default" "bzip2-sys") ("bzip2" "bzip2-sys")))) (l "zip")))

