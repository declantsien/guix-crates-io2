(define-module (crates-io li bz libz-sys) #:use-module (crates-io))

(define-public crate-libz-sys-0.0.1 (c (n "libz-sys") (v "0.0.1") (d (list (d (n "pkg-config") (r "*") (d #t) (k 0)))) (h "17s9nlf24klprjzngwl1mc3xvka87j6m4gyms7k920jp53lq90m8") (y #t)))

(define-public crate-libz-sys-0.1.0 (c (n "libz-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.1.0") (d #t) (k 1)))) (h "0bqpi0micj1ycrycafaqvpa2hjq6j177l34ihk5v8fv5xzx1v5jz")))

(define-public crate-libz-sys-0.1.1 (c (n "libz-sys") (v "0.1.1") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ishjh9rwvs8r84yn9zd4mba4sxwqppdxdk8ysgdydj08bjwq8nb")))

(define-public crate-libz-sys-0.1.2 (c (n "libz-sys") (v "0.1.2") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "09f1dmdh6nmmczkpann7l0jgb5ccyi8p8nflymg23p1cw3nzgls6")))

(define-public crate-libz-sys-0.1.3 (c (n "libz-sys") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mrylbhqw3wg06x57azfwvsivzvzgphimxkvp052zpxbsp46f87w") (y #t)))

(define-public crate-libz-sys-0.1.4 (c (n "libz-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "152j3msmxp80kn9vhwb3l2sv2rk4kcvv7j909d916fgvvzgwaj52")))

(define-public crate-libz-sys-0.1.5 (c (n "libz-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1759vy2ph3kk8dl1lqzlk564pk5r12i8yvhivpv9k2na6g4cgqx8")))

(define-public crate-libz-sys-0.1.6 (c (n "libz-sys") (v "0.1.6") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0d8mrzjzhkfl8jghv86x960dra8jpcxhyvyyw89n4hv6hl6pd0am")))

(define-public crate-libz-sys-0.1.7 (c (n "libz-sys") (v "0.1.7") (d (list (d (n "gcc") (r "^0.3.10") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0mn4qgsq8945vyzcpi1dmzsp4hhgsskr5zjqs479ymwhc6ilfz6i")))

(define-public crate-libz-sys-0.1.8 (c (n "libz-sys") (v "0.1.8") (d (list (d (n "gcc") (r "^0.3.10") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1imhi8wbc1kkkr9yrp24k3lhhkyjj2fzx0hvml03snqwms33lk4s")))

(define-public crate-libz-sys-0.1.9 (c (n "libz-sys") (v "0.1.9") (d (list (d (n "gcc") (r "^0.3.10") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06cgj368y09rq86n5imx22n42pzb7jvyhapa8k8rj0adqvd5nmih")))

(define-public crate-libz-sys-1.0.0 (c (n "libz-sys") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3.10") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0mdapxfnz67mlf1sb2si2hqqrk6kmipjnz7f5klnlhgfri0gsf8d")))

(define-public crate-libz-sys-1.0.1 (c (n "libz-sys") (v "1.0.1") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rmpa83hiyhglmbml316fviq8kqsljp1lxcgmgimqw78bv1zvnz6")))

(define-public crate-libz-sys-1.0.2 (c (n "libz-sys") (v "1.0.2") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1zdlz00b87rqang0qvzkxzkqdp1z1rprphpkpdlkal3gh3wm6sxa")))

(define-public crate-libz-sys-1.0.3 (c (n "libz-sys") (v "1.0.3") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1wmz6llj4r8zjvvl373avw9ljg7wj9zih68v07a0kd48a4803nj4")))

(define-public crate-libz-sys-1.0.4 (c (n "libz-sys") (v "1.0.4") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1llfbbfjhmh6va3xz2cf701f1hpw2qw0d3rzhymspcwq0j55lyf9")))

(define-public crate-libz-sys-1.0.5 (c (n "libz-sys") (v "1.0.5") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1kqm5sl9bnb1vkfjii7vrz09d21p0arpsa9iwz0gbyxc2qnxi05b")))

(define-public crate-libz-sys-1.0.6 (c (n "libz-sys") (v "1.0.6") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rhw43xqjnsqk2x96b0w8zvdxifqi38f8k74qck99lmm61vxzwj0")))

(define-public crate-libz-sys-1.0.7 (c (n "libz-sys") (v "1.0.7") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1yarhx68dww4x8dxwh5ff0aihcqch84whgz1482hkibq5wb2sg18")))

(define-public crate-libz-sys-1.0.8 (c (n "libz-sys") (v "1.0.8") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "032psw32rxq6ldp4jplmikiiyz1j5lg9kq9p1cbb1bxzmck5i2y1")))

(define-public crate-libz-sys-1.0.9 (c (n "libz-sys") (v "1.0.9") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1mp9ki223riri9czij4w10slh86vbfppp94l9nlkjynrcs18lzfx")))

(define-public crate-libz-sys-1.0.10 (c (n "libz-sys") (v "1.0.10") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1rl85x045sk5d345hgcahx99plpbdg2a3bx5vjfxig30qah74p4h")))

(define-public crate-libz-sys-1.0.11 (c (n "libz-sys") (v "1.0.11") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "192zwqlkvivcwrnpl193jn445szpahsvf6s4x79qilbcx8n690jm")))

(define-public crate-libz-sys-1.0.12 (c (n "libz-sys") (v "1.0.12") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1s5v0xgw6izz64gzdilxx3n01yzwhsv784fwrmhdm4slayd0j5kn")))

(define-public crate-libz-sys-1.0.13 (c (n "libz-sys") (v "1.0.13") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "034pgvxzgsv37iafgs0lmvd1ifm0bg0zm1xcsn9x71nn8lm93vp5")))

(define-public crate-libz-sys-1.0.14 (c (n "libz-sys") (v "1.0.14") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1rwnb6l78mmjzcq9gq0z8lmm50ha5bnb2dbimlqzajm4apv9a0g7")))

(define-public crate-libz-sys-1.0.15 (c (n "libz-sys") (v "1.0.15") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "06n7ynn5lv7fsrnv881kmnmfv8w4xmggcjydqbbi2928jgc1drvp")))

(define-public crate-libz-sys-1.0.16 (c (n "libz-sys") (v "1.0.16") (d (list (d (n "gcc") (r "^0.3.17") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1n74h8zr4k9jnm1xa9ani6nwijw31d2isp25fib1hlp6ivpn9p9z")))

(define-public crate-libz-sys-1.0.17 (c (n "libz-sys") (v "1.0.17") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1mbb3r2wpvawk2liv1rf0l97c1byv49hx6p07pcz9lnj1xvbrss4")))

(define-public crate-libz-sys-1.0.18 (c (n "libz-sys") (v "1.0.18") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "05hg2jg2chig9n8004s4xnynaf2py4959p6rwgpnxzf6djnkgxw7")))

(define-public crate-libz-sys-1.0.19 (c (n "libz-sys") (v "1.0.19") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0vq7n91gnspq4rhr32khjc28wavrsh7jp59zhazq0fx7vwnsk3fq") (y #t) (l "z")))

(define-public crate-libz-sys-1.0.20 (c (n "libz-sys") (v "1.0.20") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1in735x78xab0ps9j1k0kb5cxab8b24nbv2br7lmzi5kd6lspygm") (l "z")))

(define-public crate-libz-sys-1.0.21 (c (n "libz-sys") (v "1.0.21") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1inh5zf64jhyq9amfi2zzz6fzng9k1a2lwi6ll491spkk3svrq5k") (l "z")))

(define-public crate-libz-sys-1.0.22 (c (n "libz-sys") (v "1.0.22") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "063whgjsgcfwyj00k7jxw4gyx47fqjakv4ai47x5sqyp8d363zv5") (l "z")))

(define-public crate-libz-sys-1.0.23 (c (n "libz-sys") (v "1.0.23") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1av96k41pyxbn1nzd3gczy8nvr5bdj8p2apbwqqaj0m0592cmgf7") (l "z")))

(define-public crate-libz-sys-1.0.24 (c (n "libz-sys") (v "1.0.24") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1062j58jv18q3l1fx36j3llrv9x7ian2aij6wgy4c38aarsgw0a4") (f (quote (("asm")))) (l "z")))

(define-public crate-libz-sys-1.0.25 (c (n "libz-sys") (v "1.0.25") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1gjycyl2283525abks98bhxa4r259m617xfm5z52p3p3c8ry9d9f") (f (quote (("static") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.0.26 (c (n "libz-sys") (v "1.0.26") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0w8b15603kjdplnjfbb3nxzqpi09arj9bncxikd8jcmfdxbrdfcd") (f (quote (("static") ("default" "libc") ("asm")))) (y #t) (l "z")))

(define-public crate-libz-sys-1.0.27 (c (n "libz-sys") (v "1.0.27") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1wh2qgdr73wsqqk29i55bpfznckk8s3zk2s7841j8l6jhd48ka3c") (f (quote (("static") ("default" "libc") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.0 (c (n "libz-sys") (v "1.1.0") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1sasky1swzcjzx9l059f4yfj6kdpwn66r1k1lb7cr1fqim5r4rxg") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.1 (c (n "libz-sys") (v "1.1.1") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1q25cb8vs113si7q2p0innhi8jk0wpq37hqi2wcc219hcmw43cr3") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.2 (c (n "libz-sys") (v "1.1.2") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0mcn8991kpmw320923hlcmci834nkv1qakkcg4w8znq85cci68b0") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.3 (c (n "libz-sys") (v "1.1.3") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0rpcxkccjn8da9114kvl8hrnrkspzsmdn0qcxlix85lwajw3am6y") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.4 (c (n "libz-sys") (v "1.1.4") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0ahyplr2n9v8jwnd1p57fai8azbiwv91by6syz1j39d0g0bgcayz") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.5 (c (n "libz-sys") (v "1.1.5") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0ngqbc6p5b31f2mkq4dlkk4w2dj23nzf4aw291dcnwsn9b6zldbg") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.6 (c (n "libz-sys") (v "1.1.6") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "0bpfakhhzda75vhvcaarsj6a4qafy4jpc4rffpcy3k0hfrfy3rwj") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.7 (c (n "libz-sys") (v "1.1.7") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1kr478mga8sp5dxw34hwv2xn8gx5486dgh9zqqkvzp7cixxmwk0p") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.8 (c (n "libz-sys") (v "1.1.8") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1gqb8nk7j4ngvlcll8plm2fvjwic40p2g4qp20pwry1m74f7c0lp") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.9 (c (n "libz-sys") (v "1.1.9") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1nznp8v01pyb8lx0d4fj53rrzr8cd8yn0bylaqa8fs4mrjg8ivjn") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.10 (c (n "libz-sys") (v "1.1.10") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1p27p9vm1mishki82ik02wpnchpzz54jzm57wvzr5c0yjw0spri4") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.11 (c (n "libz-sys") (v "1.1.11") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "0k1nhq7473ngzq4kjd4jm56yma8q2v8vkl88mlfq5dbarh1787qj") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.12 (c (n "libz-sys") (v "1.1.12") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "0yqahz2m5g44mpgfdy0k53hpfkfs5rfiv3a1y7p766ijbsr3fwfr") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.13 (c (n "libz-sys") (v "1.1.13") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "0rncrk2i3mk5a69pzay7n1ny7a8s3vj3vr4351s1k7nr17fnyljz") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.14 (c (n "libz-sys") (v "1.1.14") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "0l6h7wdvr77614a1jwkm5w4asifpng73rsxs3n18qg2p6zl1fp19") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.15 (c (n "libz-sys") (v "1.1.15") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1xj89rjhk642x8271xr9phj7da7ivwyvd5g8fmb7ma5asgsk2xq3") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.16 (c (n "libz-sys") (v "1.1.16") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.44") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1yd7mnw0h469rbsxfbb0r6czc4q8fabn9jkbiz99a9kbcrg3n52y") (f (quote (("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

(define-public crate-libz-sys-1.1.17 (c (n "libz-sys") (v "1.1.17") (d (list (d (n "cc") (r "^1.0.18") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1w1r89f13sff9mjwx7a1wma0gjj0srkagg1jncslgnfbfl454y00") (f (quote (("zlib-ng-no-cmake-experimental-community-maintained" "libc") ("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (y #t) (l "z")))

(define-public crate-libz-sys-1.1.18 (c (n "libz-sys") (v "1.1.18") (d (list (d (n "cc") (r "^1.0.98") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "0bpqmfzvijbrqs29vphnafjz834lpz6pabbsnf85rqppb9pa4pf1") (f (quote (("zlib-ng-no-cmake-experimental-community-maintained" "libc") ("zlib-ng" "libc" "cmake") ("stock-zlib") ("static") ("default" "libc" "stock-zlib") ("asm")))) (l "z")))

