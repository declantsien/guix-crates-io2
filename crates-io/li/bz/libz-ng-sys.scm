(define-module (crates-io li bz libz-ng-sys) #:use-module (crates-io))

(define-public crate-libz-ng-sys-1.1.7 (c (n "libz-ng-sys") (v "1.1.7") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "1j0xiawwrc39ba9jx759x9byd7wvd1mris39182jqw9bs2v0pfz3") (l "z-ng")))

(define-public crate-libz-ng-sys-1.1.8 (c (n "libz-ng-sys") (v "1.1.8") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "1wp0aya5hh76h1acspvrrsvq2fl0kyb8dpi6wy0zaswnm6bax6a3") (l "z-ng")))

(define-public crate-libz-ng-sys-1.1.9 (c (n "libz-ng-sys") (v "1.1.9") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "1jrw0jqz3c0vc8ybahsa5ibd12dxvvzw2k8mwwpmhfwh6ippas14") (l "z-ng")))

(define-public crate-libz-ng-sys-1.1.10 (c (n "libz-ng-sys") (v "1.1.10") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "12rvykk8j2hn5wkl1h4s3rl8zvfilzqczlb9gj65qcv8h20bcps2") (l "z-ng")))

(define-public crate-libz-ng-sys-1.1.11 (c (n "libz-ng-sys") (v "1.1.11") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "19ndzwgxc7cjx48fraia112b5j8dwnrqbym6jrxq9syb14zi0jky") (l "z-ng")))

(define-public crate-libz-ng-sys-1.1.12 (c (n "libz-ng-sys") (v "1.1.12") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "0qzap9jqlcsij8dccf36x2afaiiqnvv5idwj1zp4csjkflzg9n9x") (l "z-ng")))

(define-public crate-libz-ng-sys-1.1.13 (c (n "libz-ng-sys") (v "1.1.13") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "0ba9j7c91yjbflxrs00j46qbkgs04adn6bgmr51npiz23m4jf730") (l "z-ng")))

(define-public crate-libz-ng-sys-1.1.14 (c (n "libz-ng-sys") (v "1.1.14") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "1ib6s4n3q3fqj46q8y5n8rijmdckh5xv8jrsx92jpbfl5zg7s5c1") (l "z-ng")))

(define-public crate-libz-ng-sys-1.1.15 (c (n "libz-ng-sys") (v "1.1.15") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "libc") (r "^0.2.43") (d #t) (k 0)))) (h "1ic3h0rccfkfy6y7crdwjp7xm38fyznghbb07sb8f9mic7y9wh66") (l "z-ng")))

