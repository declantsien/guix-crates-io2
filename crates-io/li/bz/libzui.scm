(define-module (crates-io li bz libzui) #:use-module (crates-io))

(define-public crate-libzui-0.1.0 (c (n "libzui") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qf0a9qasj245crjrwgj5mvwd8pnib3xh25zwgb2ckap94866wrm") (y #t)))

(define-public crate-libzui-0.0.1 (c (n "libzui") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rip3nzzfvjkcwp7q5yyri6pwmgxqs9md84zxawwl8iskjh131vq") (y #t)))

(define-public crate-libzui-0.0.2 (c (n "libzui") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "190i9qyaan96qcnxq56pj0q99jkn7ii5rmhaz8rkvwjb9k36xxyp")))

(define-public crate-libzui-0.0.3 (c (n "libzui") (v "0.0.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vmza43bqcmcf551s5834f0bgzas6i26d8y52jg4wc5svf4ha9fn")))

