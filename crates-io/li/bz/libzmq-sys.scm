(define-module (crates-io li bz libzmq-sys) #:use-module (crates-io))

(define-public crate-libzmq-sys-0.1.0 (c (n "libzmq-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zeromq-src") (r "= 0.1.4-preview") (d #t) (k 1)))) (h "0lv841m9vkrqr95c9npncglhdij57bhj8g2778625x50vxp9k44b") (y #t) (l "zmq")))

(define-public crate-libzmq-sys-0.1.1 (c (n "libzmq-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zeromq-src") (r "= 0.1.4-preview") (d #t) (k 1)))) (h "1z4c6dnzsxaf38a8ms9pv4i0qlalfj506yrhvvsisa17arj3k2sb") (y #t) (l "zmq")))

(define-public crate-libzmq-sys-0.1.2 (c (n "libzmq-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zeromq-src") (r "= 0.1.6-preview") (d #t) (k 1)))) (h "1gxs3b47zf3r7ijyz46f2pal36q5rp5rxsvmkr149rdh5r2shdhc") (y #t) (l "zmq")))

(define-public crate-libzmq-sys-0.1.3 (c (n "libzmq-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zeromq-src") (r "= 0.1.6-preview") (d #t) (k 1)))) (h "1a4cfzib0jp6a0rsc6v32588rxd20wsjshdgs28dwd2cgzhc935s") (y #t) (l "zmq")))

(define-public crate-libzmq-sys-0.1.4 (c (n "libzmq-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zeromq-src") (r "= 0.1.6-preview") (d #t) (k 1)))) (h "1mkbkv5hi2ksqbm6rj9l6dqijmpa7simimgc2g8kh8mxk0r2vr79") (f (quote (("renew-bindings" "bindgen")))) (y #t) (l "zmq")))

(define-public crate-libzmq-sys-0.1.5 (c (n "libzmq-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zeromq-src") (r "= 0.1.6-preview.1") (d #t) (k 1)))) (h "0hlr3rcksrbhmcxcbwchk1v8sfbnafv6qd42xndv2x81c4jpahq7") (f (quote (("renew-bindings" "bindgen")))) (y #t) (l "zmq")))

(define-public crate-libzmq-sys-0.1.6 (c (n "libzmq-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zeromq-src") (r "^0.1.7") (d #t) (k 1)))) (h "19vhhqij1c6yxmcq6sbc4cwwzfi97b3v1p175p243a9q81x0w1n1") (f (quote (("renew-bindings" "bindgen")))) (l "zmq")))

(define-public crate-libzmq-sys-0.1.7+4.3.2 (c (n "libzmq-sys") (v "0.1.7+4.3.2") (d (list (d (n "bindgen") (r "^0.49.0") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.8") (d #t) (k 2)) (d (n "zeromq-src") (r "^0.1.8") (d #t) (k 1)))) (h "189z7rvh4n6g359170y314lav138py1h6x5wz4alvxzq4irfdcwj") (f (quote (("renew-bindings" "bindgen") ("libsodium" "libsodium-sys")))) (l "zmq")))

(define-public crate-libzmq-sys-0.1.8+4.3.2 (c (n "libzmq-sys") (v "0.1.8+4.3.2") (d (list (d (n "bindgen") (r "^0.53.2") (o #t) (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "zeromq-src") (r "^0.1.10") (d #t) (k 1)))) (h "103bx92jbnzc76w5i587ns2gyizihp871ifhrki65wd3s8n2fq49") (f (quote (("renew-bindings" "bindgen") ("libsodium" "libsodium-sys") ("default" "curve") ("curve")))) (l "zmq")))

