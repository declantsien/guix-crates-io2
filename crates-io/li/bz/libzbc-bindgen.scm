(define-module (crates-io li bz libzbc-bindgen) #:use-module (crates-io))

(define-public crate-libzbc-bindgen-0.1.0 (c (n "libzbc-bindgen") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0s00lypkz3alrb0ykfkn0f90548qhw5b2qw0mc1pkvc8622cnwl8")))

(define-public crate-libzbc-bindgen-0.2.0 (c (n "libzbc-bindgen") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "06dxq7zq6yb8jm6m8q8ipih8sh8dhpspa3c8rk6mh71vh61xhmvk") (l "zbc")))

