(define-module (crates-io li bz libzt) #:use-module (crates-io))

(define-public crate-libzt-0.1.0 (c (n "libzt") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.57") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "0gar7xcxhc72yq5fd1nzcjlhdhdg2yswkv5jib9ifnmk4cm9p2yw")))

(define-public crate-libzt-0.1.1 (c (n "libzt") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "0rsypxgis1ca6aay1l7n923ycvb72d8bcrjw3lxblki14whhcirf")))

(define-public crate-libzt-0.1.2 (c (n "libzt") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "19ycfwb2a8lvm4skia3csi1dj0pag4ag5bh3kaz3pfisi57b9jz0")))

