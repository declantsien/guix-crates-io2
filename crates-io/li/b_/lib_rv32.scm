(define-module (crates-io li b_ lib_rv32) #:use-module (crates-io))

(define-public crate-lib_rv32-0.1.0 (c (n "lib_rv32") (v "0.1.0") (d (list (d (n "clap") (r "2.33.*") (d #t) (k 0)) (d (n "glob") (r "0.3.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1nc7hwhzkry63bv1d1yrsd6arkihim6cc1jl9lia8hvkdqgyqz9p")))

(define-public crate-lib_rv32-0.1.1 (c (n "lib_rv32") (v "0.1.1") (d (list (d (n "clap") (r "2.33.*") (d #t) (k 0)) (d (n "glob") (r "0.3.*") (d #t) (k 0)) (d (n "lazy_static") (r "1.4.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 0)))) (h "1yh1wvnd709kbnab6cj8ijyk163whqsm8smswxglhqgy7416nklf")))

