(define-module (crates-io li b_ lib_opnfi) #:use-module (crates-io))

(define-public crate-lib_opnfi-0.0.1 (c (n "lib_opnfi") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "enumflags2") (r "^0.6.2") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "openssl") (r "^0.10.26") (d #t) (k 0)) (d (n "pnet") (r "^0.23.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.102") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "snap") (r "^0.2.5") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1gri7xk434jgidhyh9c89i4rbfsll0jimky4mlrwkcgc37s5znxa")))

