(define-module (crates-io li b_ lib_blaster) #:use-module (crates-io))

(define-public crate-lib_blaster-0.1.0 (c (n "lib_blaster") (v "0.1.0") (d (list (d (n "pnet_macros_support") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.21.0") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.21.0") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1gicyxwb5a9r6bxddm4zlkdcfr6yxhp28ihh37kspzgc44wil641")))

(define-public crate-lib_blaster-0.1.1 (c (n "lib_blaster") (v "0.1.1") (d (list (d (n "pnet_packet") (r "^0.21") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.21") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "13k9l37j7lbxp4dfhg76s3xdyxyj0108hj5q0qq5f8g538abhy35")))

(define-public crate-lib_blaster-0.1.2 (c (n "lib_blaster") (v "0.1.2") (d (list (d (n "pnet_packet") (r "^0.21") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.21") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1lmajffgsw3y2zcinvxhi5i0n2j70gx8q2ppk468p4b9kk0b7lv8")))

(define-public crate-lib_blaster-0.2.0 (c (n "lib_blaster") (v "0.2.0") (d (list (d (n "pnet_macros_support") (r "^0.21") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.21") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.21") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1rmdxzd0slb89pfs1ij8sw98x9xw5a046zzpflg0bk64y4i0d4hx")))

(define-public crate-lib_blaster-0.3.0 (c (n "lib_blaster") (v "0.3.0") (d (list (d (n "pnet_macros_support") (r "^0.21") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.21") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.21") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "0qqh6270cs6rfvfax0wdggmy2dcnafvd8y37h2r8s1586i5pj99w")))

(define-public crate-lib_blaster-0.3.1 (c (n "lib_blaster") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "pnet_macros_support") (r "^0.21") (d #t) (k 0)) (d (n "pnet_packet") (r "^0.21") (d #t) (k 0)) (d (n "pnet_transport") (r "^0.21") (d #t) (k 0)) (d (n "rand") (r "^0.5") (d #t) (k 0)))) (h "1539h4wkq7gl3xlsbsbjiwlg3146hw7m5x4yfh5h7q4qfclgfzj0")))

