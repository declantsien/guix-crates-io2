(define-module (crates-io li b_ lib_cstars) #:use-module (crates-io))

(define-public crate-lib_cstars-0.1.0 (c (n "lib_cstars") (v "0.1.0") (d (list (d (n "directories") (r "^4.0.0") (d #t) (k 0)) (d (n "figment") (r "^0.10.0") (f (quote ("toml"))) (d #t) (k 0)) (d (n "html_editor") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.4.16") (d #t) (k 0)) (d (n "regex") (r "^1.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("blocking" "cookies"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)))) (h "0x7qqi4qanx8p3j3r3dmfbafihqarp653v49bwa3hi5cdfmsb4xs")))

