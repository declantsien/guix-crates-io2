(define-module (crates-io li b_ lib_manipulation_archive) #:use-module (crates-io))

(define-public crate-lib_manipulation_archive-0.1.0 (c (n "lib_manipulation_archive") (v "0.1.0") (h "1mdqgqk694q3cahmjfbpswiml2lfilvrigfw837yv1r09cp0ypyl") (r "1.59.0")))

(define-public crate-lib_manipulation_archive-0.1.1 (c (n "lib_manipulation_archive") (v "0.1.1") (h "1r6z26a16pnbnvp1bmhs44m11405sran2z1c08mxr4vy8ciajhb9") (r "1.59.0")))

(define-public crate-lib_manipulation_archive-0.2.0 (c (n "lib_manipulation_archive") (v "0.2.0") (h "03jgzn84s07d9qm8wx7yrrm6pjdpn7bsww100gfiq0zpkpb3cksm") (r "1.59.0")))

