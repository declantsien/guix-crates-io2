(define-module (crates-io li b_ lib_maniascript) #:use-module (crates-io))

(define-public crate-lib_maniascript-0.1.0 (c (n "lib_maniascript") (v "0.1.0") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rowan") (r "^0.9.0") (d #t) (k 0)))) (h "0p3ffnmsdvk4n4vlqaxrm6lch986xv6w0wnbfikgsw863hiv8qc6")))

(define-public crate-lib_maniascript-0.1.1 (c (n "lib_maniascript") (v "0.1.1") (d (list (d (n "cbitset") (r "^0.2.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rowan") (r "^0.9.0") (d #t) (k 0)))) (h "06xc1r2m74iy7214ni4br536vs52pp6172xr53dzibgrsslplvn7")))

