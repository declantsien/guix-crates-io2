(define-module (crates-io li b_ lib_annex) #:use-module (crates-io))

(define-public crate-lib_annex-0.1.0 (c (n "lib_annex") (v "0.1.0") (h "0fxy8c2m88k998qr59gldqfd22zwnbl1x0pvjlidri0y1wryqq5q")))

(define-public crate-lib_annex-0.1.1 (c (n "lib_annex") (v "0.1.1") (h "1z2mwcl6mi1x33ymxc6r6ipivn91yk6k1p40qbgrhqg8iyh0cy4p")))

(define-public crate-lib_annex-0.1.2 (c (n "lib_annex") (v "0.1.2") (h "0mrdvm07xx92aq9v74r8v92ran79rwfrnhhfhqq0y2xs1lqv07mb")))

(define-public crate-lib_annex-0.1.3 (c (n "lib_annex") (v "0.1.3") (h "144cvdar8d0w3qzxzfhkmsdb5kk8y5g01bipaylxs0kfy91m2nci")))

(define-public crate-lib_annex-0.1.4 (c (n "lib_annex") (v "0.1.4") (h "1rimaaa32bwrzry3awqhwcb47j71y4pr3v25ffshzmj1m0jn4014")))

