(define-module (crates-io li b_ lib_hello_world) #:use-module (crates-io))

(define-public crate-lib_hello_world-0.1.0 (c (n "lib_hello_world") (v "0.1.0") (h "1dg2x1fds9hd4ffp1fp0s87l0lskblqsi6032dbgn5ay0rcc27i3")))

(define-public crate-lib_hello_world-0.1.1 (c (n "lib_hello_world") (v "0.1.1") (h "14cf0vahz3adp5gdwfg2m573zb900yiww3hgv5imhihzmh7gw7zl")))

(define-public crate-lib_hello_world-0.1.2 (c (n "lib_hello_world") (v "0.1.2") (h "12z6cxcjfm4f6y24613pfv7zm9r5wbf66d094ybn0n7gpz8rh7cq")))

