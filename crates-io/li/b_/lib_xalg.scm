(define-module (crates-io li b_ lib_xalg) #:use-module (crates-io))

(define-public crate-lib_xalg-0.1.0 (c (n "lib_xalg") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "130bs6nxvd0033b1fl5pdq91bz982fx65d61m25bp3nmgvphcbp4")))

(define-public crate-lib_xalg-0.1.1 (c (n "lib_xalg") (v "0.1.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "0jym3qg34hd57mg8rvwrg1y42iby4cljmzghb6sr0digs5hl8nw3")))

(define-public crate-lib_xalg-0.1.2 (c (n "lib_xalg") (v "0.1.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "182bfs4f6w64x1n0h17nil3lfpa3giqzr76nymnl5hjvmbb7qv91")))

(define-public crate-lib_xalg-0.2.0 (c (n "lib_xalg") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)))) (h "06jh5hgsfq19hlvdjkcxp1zhbnj4n2kb7xxvk2wbkidqijjyzqkh")))

(define-public crate-lib_xalg-0.2.1 (c (n "lib_xalg") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "14n3raamaqzfg9ymas2lxycf01yp07wjvq0y0lrpjxw2b5jnngd5")))

(define-public crate-lib_xalg-0.3.0 (c (n "lib_xalg") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "0f72hi2mn4f95bfpf4wy75cwdfb3cf135agybpssm5f9jw15y87p") (y #t)))

(define-public crate-lib_xalg-0.3.1 (c (n "lib_xalg") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("wasm-bindgen"))) (d #t) (k 0)))) (h "1cy0d036ddj8cqf5m5jml9nlnvwmpvl164h6fmfi4mc7gvax9gw7")))

