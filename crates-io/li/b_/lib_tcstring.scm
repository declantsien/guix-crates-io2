(define-module (crates-io li b_ lib_tcstring) #:use-module (crates-io))

(define-public crate-lib_tcstring-0.1.0 (c (n "lib_tcstring") (v "0.1.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0jdzzmnjvlzw4myh2qncjcps8n60gldirg85w8vrrml05fhi0kf8") (f (quote (("default")))) (y #t)))

(define-public crate-lib_tcstring-0.1.1 (c (n "lib_tcstring") (v "0.1.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0yc9jgfp5jcl0b8j47ar249ll6dqyjdjl7l73x1r61nidbvsi7g4") (f (quote (("default")))) (y #t)))

(define-public crate-lib_tcstring-0.1.2 (c (n "lib_tcstring") (v "0.1.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ikrh2v6r1d8nlwzhx78qgg5nkb64jx8lkdcx9gkfcwmw5z77mds") (f (quote (("default"))))))

(define-public crate-lib_tcstring-0.2.0 (c (n "lib_tcstring") (v "0.2.0") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (d #t) (k 1)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1blk856r55d6clxmzg64lhxcbav1rpl2irqy9nksz6wm97mihz08") (f (quote (("default"))))))

(define-public crate-lib_tcstring-0.2.1 (c (n "lib_tcstring") (v "0.2.1") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0paacs6zznlic04l0pg6hr7hkr36vnxk0b1zskwzx4gjj0kkqs83") (f (quote (("default"))))))

(define-public crate-lib_tcstring-0.2.2 (c (n "lib_tcstring") (v "0.2.2") (d (list (d (n "base64") (r "^0.12") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0zsvr8293jd1la5wfrz78rpcjpvljpqgm4mg8ylskcv7j3mfrhbc") (f (quote (("default"))))))

(define-public crate-lib_tcstring-0.2.3 (c (n "lib_tcstring") (v "0.2.3") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1zrfkxqx1wjcbl1kbf5r4pwkcsqralk30kdv7vrs85k9bpmiwqi3") (f (quote (("default"))))))

(define-public crate-lib_tcstring-0.2.4 (c (n "lib_tcstring") (v "0.2.4") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1p7bcnqbkc0kvalx0fc6n66gqbj8kyjblcj97371b02k3viadg1b") (f (quote (("default"))))))

(define-public crate-lib_tcstring-0.3.0 (c (n "lib_tcstring") (v "0.3.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "clippy") (r "^0.0.302") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0268zpim0dbyjjh762ixb0l8vnxz80c6xcg6vblc597pji0xkmd4") (f (quote (("default"))))))

(define-public crate-lib_tcstring-0.4.0 (c (n "lib_tcstring") (v "0.4.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "criterion") (r "^0.3.5") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "098c87a6xi06j5z014rv0aaw9fyl8w5qyx60p47qv1abs3h145lb") (f (quote (("default"))))))

(define-public crate-lib_tcstring-0.4.1 (c (n "lib_tcstring") (v "0.4.1") (d (list (d (n "base64") (r "^0.21") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1fylq5r38q5hmsvnbvg9kcmj1ky3dw6wmkk0pp8q9fz3brfb8w4w") (f (quote (("default"))))))

(define-public crate-lib_tcstring-0.5.0 (c (n "lib_tcstring") (v "0.5.0") (d (list (d (n "base64") (r "^0.22") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "version-sync") (r "^0.9.5") (d #t) (k 2)))) (h "0fybd8kzps8knim1x81klwmp3wqhgc7r7b40gwcx2x7r4xv1dcc4") (f (quote (("default"))))))

