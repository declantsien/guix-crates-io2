(define-module (crates-io li b_ lib_composite) #:use-module (crates-io))

(define-public crate-lib_composite-0.1.0 (c (n "lib_composite") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "1vgm08fikn1jjvg95bclgpwvziv17l5x4v5afmkzsjv9pyx7xhl7")))

(define-public crate-lib_composite-0.1.1 (c (n "lib_composite") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "0m9m2dz8ys0ggg6sd5847gfmkkh9wp9l3vmh8b0i9p200fi13azr")))

(define-public crate-lib_composite-0.1.2 (c (n "lib_composite") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "1wls1kp9ir14my7g5xszwpfckjiq44vpgxgl2q9fkcif1aqq3w27")))

(define-public crate-lib_composite-0.1.3 (c (n "lib_composite") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "1jla0369z6cq63npa3c907a3yc0mbfm2pxvf80n34jqbamj5v6yn")))

(define-public crate-lib_composite-0.1.4 (c (n "lib_composite") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "07wpqccwd7agg234p8706ji7wg9ibwbk75bk62bzjqbk78zc8j8x")))

(define-public crate-lib_composite-0.2.0 (c (n "lib_composite") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "0gx4m493kjg8h4a5fici3yczqghshhmfdq4vriffrpamnwn5r59b")))

(define-public crate-lib_composite-0.2.1 (c (n "lib_composite") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "0zy8s0wkkazd3bc266yxgb45kpq5pxigxz19y0q8vfin1pzrd1vi")))

(define-public crate-lib_composite-0.3.0 (c (n "lib_composite") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "19ffw0w34jcjh502qglhhgkvq1dfjwv52c5xvcmjfyinlh8w8i9h")))

(define-public crate-lib_composite-0.3.1 (c (n "lib_composite") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "09i5vm9cqjba6sdldl0k0pr6wh15qbgmkfx3a0221r7apmqcqh1z")))

(define-public crate-lib_composite-0.3.2 (c (n "lib_composite") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "06sbkn92bcj27vkz73dsa5wcrscdy7alfal79s3bgljz5xvn54a7")))

(define-public crate-lib_composite-0.4.0 (c (n "lib_composite") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "1lixh21kzj30dkl5b7ggbpiypmsryhw8csxc4vl1wramv4zx72dz")))

(define-public crate-lib_composite-0.5.0 (c (n "lib_composite") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "1ra2hdc6zcnrgxjqglic9l4i6k2w282b57mwa77ggqv5gqmrypca")))

(define-public crate-lib_composite-0.6.0 (c (n "lib_composite") (v "0.6.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "0gs6rxjy9ip7vj8d87aqpzh8z5ksfpqxmz9majh9mvvm0bwn8kar")))

(define-public crate-lib_composite-0.7.0 (c (n "lib_composite") (v "0.7.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc-extra") (r "^0.0.15") (d #t) (k 0)))) (h "0w9h7h0gwh6bnfvayd6pfkbmrzlsiq9j16lw19ikdxmiqj6r39l8")))

