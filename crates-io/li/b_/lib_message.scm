(define-module (crates-io li b_ lib_message) #:use-module (crates-io))

(define-public crate-lib_message-0.1.0 (c (n "lib_message") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iyx0xc1bbpsn7s0px2x3c006mfg0a1narm877wf5cr9wsf1ikns")))

(define-public crate-lib_message-0.1.1 (c (n "lib_message") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01f5gj6lwv92pl0ldl9vmm46j60fvc5drq7x737lpg94ywaxjrc3")))

(define-public crate-lib_message-0.1.2 (c (n "lib_message") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "191imwabvp7hrdsxb2cq7n8ag9jyqqr3jrmdsmhnmsrq0maagxqi")))

(define-public crate-lib_message-0.1.3 (c (n "lib_message") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02ppszyn9rzlxmdf53cfbbqcdqqjkw93ch63wb2ms2a8vb90fn29")))

(define-public crate-lib_message-0.1.4 (c (n "lib_message") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cl8i0vqpqy4wxb8bz0y6zf76jkrxc0q1zijnmrjnsffm8y6lz89")))

