(define-module (crates-io li b_ lib_treblecross) #:use-module (crates-io))

(define-public crate-lib_treblecross-0.1.0 (c (n "lib_treblecross") (v "0.1.0") (h "0p466i1yhvl9kmgpv9kqbq91jyifqlmycyylsnnlz2q3rz2n0xxx")))

(define-public crate-lib_treblecross-0.2.0 (c (n "lib_treblecross") (v "0.2.0") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)))) (h "0i5lgwky97sampxam8qw71z12miyv8s3f9m3pbywh7xnd5jqjqrk")))

(define-public crate-lib_treblecross-0.2.1 (c (n "lib_treblecross") (v "0.2.1") (d (list (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)))) (h "1xa17i4xpjj5apj03n1cbipzccp4szql5xj102rfq7m4ipy6msb4")))

