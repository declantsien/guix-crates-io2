(define-module (crates-io li b_ lib_battleship) #:use-module (crates-io))

(define-public crate-lib_battleship-1.0.0 (c (n "lib_battleship") (v "1.0.0") (h "05s4m790hzgfwphlfqy05xrnf2kcf6lba7dm9wg85fxfh18lg0mi")))

(define-public crate-lib_battleship-1.0.1 (c (n "lib_battleship") (v "1.0.1") (h "1dcpbz67qha6q759nhavwqklidsp4x4zwnfv9a62shfgkc8wlp32")))

(define-public crate-lib_battleship-1.0.2 (c (n "lib_battleship") (v "1.0.2") (h "08g531fakmgrm387kws1cb4rlmbzzdzsgf5j97mbb6lwmhr0ixlb")))

(define-public crate-lib_battleship-2.0.0 (c (n "lib_battleship") (v "2.0.0") (h "1a78zmq61hsaxfbsjcz0mmzq4bwsml9v0vx4smqmw8zj62ylim2z")))

(define-public crate-lib_battleship-2.1.0 (c (n "lib_battleship") (v "2.1.0") (h "1rq9d094kxpa17i8kg5jz8myvvvsrb41whya1ghvlkzhg6n487i1")))

