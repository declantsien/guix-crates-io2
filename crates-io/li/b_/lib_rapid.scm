(define-module (crates-io li b_ lib_rapid) #:use-module (crates-io))

(define-public crate-lib_rapid-0.0.1 (c (n "lib_rapid") (v "0.0.1") (d (list (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1j2hnwkrrfik1rh1418b7ynf97qn5cw8w9pjmb6m5wypz5snbama") (y #t)))

(define-public crate-lib_rapid-0.0.11 (c (n "lib_rapid") (v "0.0.11") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zhnq06n6in9qa9s8a0h2gqyiwvj5rah4q6y79zhl7059y1i1l4n")))

(define-public crate-lib_rapid-0.0.12 (c (n "lib_rapid") (v "0.0.12") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p2h1f4g82p73snyq9jl9nbblj1w4vsi50i129072kv8vk8p7izs")))

(define-public crate-lib_rapid-0.0.13 (c (n "lib_rapid") (v "0.0.13") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bwhfvywaf4pg0jrcwj8npdp214rk8azjafshqv23krd6lfp412y")))

(define-public crate-lib_rapid-0.0.14 (c (n "lib_rapid") (v "0.0.14") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0aascry96xnnairss10cyf5wxd57kvszp9a0r9c8307h4yh39fh5") (y #t)))

(define-public crate-lib_rapid-0.0.15 (c (n "lib_rapid") (v "0.0.15") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zbr94sr6974jdg8bm8c5rvp5amydvi0dkxb5sjg11i5n7prs6m9") (y #t)))

(define-public crate-lib_rapid-0.0.16 (c (n "lib_rapid") (v "0.0.16") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "num") (r "^0.2.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0gaj54h73ppgi0bz3f3xwj8102in7y0qdh439jm679yfsk65nwsy") (y #t)))

(define-public crate-lib_rapid-0.0.17 (c (n "lib_rapid") (v "0.0.17") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wc7w4y2bc4p7jdz7gcnnjp4w588k1l0f4b6xki95xmgsxjmdwzj") (y #t)))

(define-public crate-lib_rapid-0.0.18 (c (n "lib_rapid") (v "0.0.18") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0wxqlr1gn06n8pk3604k8lbv968q245imx0n7bfpj2xh4wq2hh5y") (y #t)))

(define-public crate-lib_rapid-0.0.19 (c (n "lib_rapid") (v "0.0.19") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07vgqpjdaqn5zpqw0y892p9f598cb48q235yvl1lk3v3qff71laq")))

(define-public crate-lib_rapid-0.0.20 (c (n "lib_rapid") (v "0.0.20") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hzx09wfrjzybg3fry1rzigjwwk4g4lvpy9cwfskyqd3xc2c6w5r")))

(define-public crate-lib_rapid-0.0.21 (c (n "lib_rapid") (v "0.0.21") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0malcq50l0khi20z1m84zcw28b4yc5n8l41da081g67pqgal4mja")))

(define-public crate-lib_rapid-0.0.22 (c (n "lib_rapid") (v "0.0.22") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0kkcswvizpkc4dvr61azwl9sgj075j28gmyyqvg36jap69wyl9yf")))

(define-public crate-lib_rapid-0.0.23 (c (n "lib_rapid") (v "0.0.23") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wpds7nvbcbc1gg125m33195xjad2mqvxipdsgv01hxh4q72qw1w")))

(define-public crate-lib_rapid-0.0.5 (c (n "lib_rapid") (v "0.0.5") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1j9l5xsplpicc7qj2qgq6wdazqv1kg1n73hkyd7a6ph0ljl9rcng") (y #t)))

(define-public crate-lib_rapid-0.0.50 (c (n "lib_rapid") (v "0.0.50") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1fzn12bqxxsb9wsy4n67im0ada576qhw9w5w2xnh3j7msh5dhvig")))

(define-public crate-lib_rapid-0.0.51 (c (n "lib_rapid") (v "0.0.51") (d (list (d (n "bincode") (r "^1.0") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1556mgn1kkbyzwfbkn3vvgwyqryk0miv2ci8r4hg0syjblz3bni8")))

(define-public crate-lib_rapid-0.0.52 (c (n "lib_rapid") (v "0.0.52") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1svqdljzrnkphvc40gd12xwr241lrpv4qdysfc1nj640x5kpijxs")))

(define-public crate-lib_rapid-0.0.53 (c (n "lib_rapid") (v "0.0.53") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "plain") (r "^0.2.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "16j8ajrg4g4mxf1pdx5085fywsd7p2v6j5yqszky9lbdalqlkizq")))

(define-public crate-lib_rapid-0.1.0 (c (n "lib_rapid") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0hz4gip6vpvlh2r8grih7hxd7sy83nnx8wmlqrg0gff7g46mi6hh")))

(define-public crate-lib_rapid-0.1.1-preview.1 (c (n "lib_rapid") (v "0.1.1-preview.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cmdqb4qx7acig27z30nagc3f4i81drmg1rm87mz1ha55lcaf6kw")))

(define-public crate-lib_rapid-0.1.2 (c (n "lib_rapid") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "160qyw3914g84xvmmdpycgad5vivz40gd9j2pb3klgb6672isvjz")))

(define-public crate-lib_rapid-0.1.3-preview.1 (c (n "lib_rapid") (v "0.1.3-preview.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1983s9d8nvh42aws2l70mbd0255vb0gwbg3b58zfn0crknh4s620")))

(define-public crate-lib_rapid-0.1.3-preview.2 (c (n "lib_rapid") (v "0.1.3-preview.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "number-theory") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x6sccy3l0p7za4s36mvkd3fkfpbm5m97hgcin5hb2jyl7pcnzwa")))

(define-public crate-lib_rapid-0.1.3-preview.3 (c (n "lib_rapid") (v "0.1.3-preview.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "number-theory") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "13wnyyfyvlzw77vgmaqr3wg47barcjl75mjr5acr45acmanm8k5n")))

(define-public crate-lib_rapid-0.1.3 (c (n "lib_rapid") (v "0.1.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "number-theory") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lxnl9lfw4nljs9xdcjw131ada7s7xxf5ahanaqpix8im2gc0pmm")))

(define-public crate-lib_rapid-0.1.4-preview.1 (c (n "lib_rapid") (v "0.1.4-preview.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "number-theory") (r "^0.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1w5r7mjarma0qhpb7p8pgc5j6rl173bvvgcb5kf6y9kpki2ajanp")))

(define-public crate-lib_rapid-0.1.4-preview.2 (c (n "lib_rapid") (v "0.1.4-preview.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "number-theory") (r "^0.0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "03gb998jqppqidk0ll8v8f7rwfx0mzic26kzw296s4zsxj9x8sdm")))

(define-public crate-lib_rapid-0.1.4-preview.3 (c (n "lib_rapid") (v "0.1.4-preview.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p6ya4xaxdw26qig1n145vnfi0l4yyal0387hy4ykkl6pyys4pjm")))

(define-public crate-lib_rapid-0.1.4 (c (n "lib_rapid") (v "0.1.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bbhwi6gyzrxnj1j4hlv2ns91n6plvka2gp59pfhyhs6386sxy0c")))

(define-public crate-lib_rapid-0.1.5-preview.1 (c (n "lib_rapid") (v "0.1.5-preview.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1najcvd4ca2k3q3m4bch0vdws753rvlbd7svd0i8hjvyw05xqqpw")))

(define-public crate-lib_rapid-0.1.5-preview.2 (c (n "lib_rapid") (v "0.1.5-preview.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0mn0kwn2lm1xjfgf1v7vrcvnmkfjmvjdlngxzi03dz24n2rdfv1s")))

(define-public crate-lib_rapid-0.1.5-preview.3 (c (n "lib_rapid") (v "0.1.5-preview.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r1xgs55xdh0wskrx15fs1xrlh6yfps181zda2ckpzx15nqcx3gz")))

(define-public crate-lib_rapid-0.1.5 (c (n "lib_rapid") (v "0.1.5") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "12nvnh0kmwbhm4mn5249zf9i1sgx4s73ydr6s9pmrc7ipfggz3m5")))

(define-public crate-lib_rapid-0.1.6 (c (n "lib_rapid") (v "0.1.6") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1wrgyr0air3c4kv2ifxs4jpzp6zgnk2sg23bqim2swz5pm4ldpdm")))

(define-public crate-lib_rapid-0.1.7 (c (n "lib_rapid") (v "0.1.7") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0szisi1xdxcal9hi32b7yzcy4ksy7y0f8aq48vvdvn4kfhqb1ddm")))

(define-public crate-lib_rapid-0.2.0-preview.1 (c (n "lib_rapid") (v "0.2.0-preview.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0x9k2ay96za9kslw8idcmwhdk0m3m0nfs698iy4cli0xjf0smzrv")))

(define-public crate-lib_rapid-0.2.0 (c (n "lib_rapid") (v "0.2.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zdyqaa3jp4z9rlbyw3xchhjc3x2w71wqapawcy19jgjknvvnd68")))

(define-public crate-lib_rapid-0.2.1-preview.1 (c (n "lib_rapid") (v "0.2.1-preview.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "18bsml08g7ya4v0y27ay7b66qbrkdkxcpm5w9hzkvzdffw42nx5l")))

(define-public crate-lib_rapid-0.2.1-preview.2 (c (n "lib_rapid") (v "0.2.1-preview.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1i87fdqxx6abzz70hrh8nhz7p3127j1s3p13xj9q8fxklcnsr0m7")))

(define-public crate-lib_rapid-0.2.1-preview.3 (c (n "lib_rapid") (v "0.2.1-preview.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ab5zxqf51955jss4mdzin7iz5lsyf67w3z7fy9kwi1caxqxl5sh")))

(define-public crate-lib_rapid-0.2.1-preview.4 (c (n "lib_rapid") (v "0.2.1-preview.4") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pxgc7ky5bs4khwkj3cd8xlpwaxavpnnlly1knd5fw5j3a2ny67p")))

(define-public crate-lib_rapid-0.2.1 (c (n "lib_rapid") (v "0.2.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rayszix0dj0zb0rh3aqcj3wrdjm91y2aywmb9ha3s9yzc62vx5g")))

(define-public crate-lib_rapid-0.3.0 (c (n "lib_rapid") (v "0.3.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0i2j69kagr88b39ra7svya1c7nn69src7kkq8bz1igxiqhk3xziv")))

(define-public crate-lib_rapid-0.3.1-preview.1 (c (n "lib_rapid") (v "0.3.1-preview.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1abx0llr0a02yj79cs6jkxk3ki8p35961pck3i7rfdf31qpi0rrc")))

(define-public crate-lib_rapid-0.3.1 (c (n "lib_rapid") (v "0.3.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "147fkv1c9s7nbjjc30pn3qqj4jppk1pzm6x7i0b2rsiv430pkn7g")))

(define-public crate-lib_rapid-0.4.0-preview.1 (c (n "lib_rapid") (v "0.4.0-preview.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0bxlrpnfph6vrj8m1xnjkmar98fbmnvimasrzc9anrbglfnlrk35")))

(define-public crate-lib_rapid-0.4.0 (c (n "lib_rapid") (v "0.4.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "148xs1v4fv3ghfhwvy5asnbwga56za10vnk3abja65z9casvvmvn")))

(define-public crate-lib_rapid-0.4.1-preview.1 (c (n "lib_rapid") (v "0.4.1-preview.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j0wcavhhrv3bbrdgwglazbv92iqcy6hpv6biln6mivij54hd171")))

(define-public crate-lib_rapid-0.4.1-preview.2 (c (n "lib_rapid") (v "0.4.1-preview.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1m6ji5phgyi7h7rbw0jh63hyf60xvvpc59ccdgjispcs23sks7hb")))

(define-public crate-lib_rapid-0.4.1-preview.3 (c (n "lib_rapid") (v "0.4.1-preview.3") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kvfv2czkh8917m3jnwnm9kyp1c40dd6fly60d1rsdmsrg210s59")))

(define-public crate-lib_rapid-0.4.2 (c (n "lib_rapid") (v "0.4.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0r7djrcvzcjkki5s1xhi85q5b412vpi9sfncp2gk8xm5fzcnv3rk")))

(define-public crate-lib_rapid-0.5.0 (c (n "lib_rapid") (v "0.5.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "04q9h9gxwvz7iffqnxy5ghklavbj4igw2ix9v0k8hqg87yj13jx6")))

(define-public crate-lib_rapid-0.6.0 (c (n "lib_rapid") (v "0.6.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hw4lbxg8czhfv6z24785dgcj9py6accg5g1630bv3cncfrn97a5")))

(define-public crate-lib_rapid-0.7.0 (c (n "lib_rapid") (v "0.7.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bit-vec") (r "^0.6.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)))) (h "040cjrdjzcls2zlirfq86xk96kdlzdb0b8wffjnnwqnsf29kjr9l")))

