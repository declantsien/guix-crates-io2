(define-module (crates-io li b_ lib_code) #:use-module (crates-io))

(define-public crate-lib_code-0.1.0 (c (n "lib_code") (v "0.1.0") (h "1qvwkpksgallrz09pp7d5s4g37knd538r3745p8wrrq30g82fn6f")))

(define-public crate-lib_code-0.1.1 (c (n "lib_code") (v "0.1.1") (h "0y8bwiav2vfv2ab3zivl7wpvw9k0razvlh5d5ww4alhlymg19j67")))

