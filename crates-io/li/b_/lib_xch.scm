(define-module (crates-io li b_ lib_xch) #:use-module (crates-io))

(define-public crate-lib_xch-0.1.0 (c (n "lib_xch") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0ja66wfm0w57ck79syi1f0gskkdn86bkynza3xizs09y46zpzldf")))

(define-public crate-lib_xch-0.1.1 (c (n "lib_xch") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "1pcqzg18xxqs6kc170wql76kmxmspj6nzrwf80pl0glml6jx7ikh")))

(define-public crate-lib_xch-0.2.0 (c (n "lib_xch") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "0ym1ps83niizmci404kp8f30lswdk1k88i22nc3ikp8ykfl3dwqd")))

(define-public crate-lib_xch-0.2.1 (c (n "lib_xch") (v "0.2.1") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1294i6cm2g8dh0rcmzvilvx6vwm295lldjyvkpnw3g8i1ri5134s")))

(define-public crate-lib_xch-0.2.2 (c (n "lib_xch") (v "0.2.2") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "136gsm1s13a7cap8c3i0kzypqdhi1wn3dpf8mibhc8sd632qamsd") (y #t)))

(define-public crate-lib_xch-0.2.3 (c (n "lib_xch") (v "0.2.3") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "17inmfky1hr5pcwh3nny4d03x850a57a7lb9w99gfpbf76ds5s5s")))

(define-public crate-lib_xch-0.2.4 (c (n "lib_xch") (v "0.2.4") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1pibxrhc4na2dzs9n38rchj836mfc6vh47aa9dvfph298vhgryqw")))

(define-public crate-lib_xch-0.2.5 (c (n "lib_xch") (v "0.2.5") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1cd9mmddjbgc4f2hdjx7sz7ymsf8mz0r7h23ysfpgrd38lr5avlx")))

(define-public crate-lib_xch-0.2.6 (c (n "lib_xch") (v "0.2.6") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0lqdaayhayvi9fn8lhwv0mgdhvm7ds088i3rhp9qs7x69qaiwmmf")))

(define-public crate-lib_xch-0.3.0 (c (n "lib_xch") (v "0.3.0") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0bshyr1f9s4zic34krnnad7knah0hd9jfkw0l2nchgw83r74cc7j") (y #t)))

(define-public crate-lib_xch-0.3.1 (c (n "lib_xch") (v "0.3.1") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "046ak01lcmhsf186v8myd7zh947jvimdmaww4vbf5fg2m167qcjn") (y #t)))

(define-public crate-lib_xch-0.3.2 (c (n "lib_xch") (v "0.3.2") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1qsr3h1xlyap1k16vvqs9hx062ynbb2dx5mig93w37kxdc2c7rnf") (y #t)))

(define-public crate-lib_xch-0.3.3 (c (n "lib_xch") (v "0.3.3") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "00j1qspg0fz203h2mwvi695ppn1qfljxz8ibb1bqmldap5nkvvsi") (y #t)))

(define-public crate-lib_xch-0.3.4 (c (n "lib_xch") (v "0.3.4") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0y3c71z7gybmld0pwlxi0gc4s9sifvwzb7i5l2cvvcpywxjpha8a")))

(define-public crate-lib_xch-0.3.5 (c (n "lib_xch") (v "0.3.5") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1xfr5p09zf0aysi0dbp9df1jdyhfna51x7qm1kx8h4gmkv1j5q18")))

(define-public crate-lib_xch-0.3.6 (c (n "lib_xch") (v "0.3.6") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "15xqm7v035rffvskrvgzria4jss1k7rfxyaihg6nqsssywk3izvl")))

(define-public crate-lib_xch-0.3.7 (c (n "lib_xch") (v "0.3.7") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1z3sf2l3h133fj7nidcazdldxn768ckw4dy5i7xrng44kc31r4yr")))

(define-public crate-lib_xch-0.3.8 (c (n "lib_xch") (v "0.3.8") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0sl9zjy38c1j70yn7v94igxah60rl7aj2v6birn473gma3rxjw1i")))

(define-public crate-lib_xch-0.3.9 (c (n "lib_xch") (v "0.3.9") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "10z2dgzd214kiibv4clkgh13c3gwwdis4xx56hp9q7pfrwnd5wc2")))

(define-public crate-lib_xch-0.3.10 (c (n "lib_xch") (v "0.3.10") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "10kc5awpjvksclbdcqv2dvci8xbppg9zv8zw1kj9ckpn95gkjdnv")))

(define-public crate-lib_xch-0.3.11 (c (n "lib_xch") (v "0.3.11") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0c6kwwn9iajqn3jsc402fqnrdsq3gxv0b4bk2zyjsw45s5imz1an")))

(define-public crate-lib_xch-0.4.0 (c (n "lib_xch") (v "0.4.0") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0d98rrb0myrnvdlzvkmnrm56hymsakgb6irxjy2xnhglfvpsxpcd")))

(define-public crate-lib_xch-0.5.0 (c (n "lib_xch") (v "0.5.0") (d (list (d (n "lazy_static") (r "1.*") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1fph6dpvza14nbw2zygbx8ccyvvkxgs21i6cn9cfsjapa8b4cbp7")))

(define-public crate-lib_xch-0.5.1 (c (n "lib_xch") (v "0.5.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "004gihzv27hjv1gwrd7kz7il4d4lq29rzfnldxmd12sl2m40ak0f")))

(define-public crate-lib_xch-0.5.2 (c (n "lib_xch") (v "0.5.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0b04fziqmqmbdf6dccc4qgzy2g8yrbsallhxs7rfrlnv4wb87v9j")))

(define-public crate-lib_xch-0.6.0 (c (n "lib_xch") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0h48qpdzrm7bxzlrq5fhsm2r7c3b27cqg8yl3sgi50cgk75bxwwd")))

(define-public crate-lib_xch-0.6.1 (c (n "lib_xch") (v "0.6.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1qjfij54j2cc32dkxmk6q4lmhzl0lb6cr459wsl6x4lc68z928fw")))

(define-public crate-lib_xch-0.7.0 (c (n "lib_xch") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1qdalrq6n9q2b6h5gr3ggdvvmddnnv8a4vb1xsp398pakhwsyb5b")))

(define-public crate-lib_xch-0.7.1 (c (n "lib_xch") (v "0.7.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "16k4wwim4p2qa7pd5mpnbh2qfl52dyi1lh4ynx9m4lyxndmlxjxx")))

(define-public crate-lib_xch-0.7.2 (c (n "lib_xch") (v "0.7.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1k0aqqkc2hmb3fiwgwhp92im73aqpl3a7c9acil5jjjgwdlcnani")))

(define-public crate-lib_xch-0.8.0 (c (n "lib_xch") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)))) (h "1qp52hnjs9grbr2847gbl26aknqk2kdggxvxgf19xk6j6hw3knwk")))

(define-public crate-lib_xch-0.9.0 (c (n "lib_xch") (v "0.9.0") (d (list (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "15qngavsqxms4fxd06r4vwvgp2x5lx9kbrmyqjm5sb35qrqnkpl1")))

(define-public crate-lib_xch-0.9.1 (c (n "lib_xch") (v "0.9.1") (d (list (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "1vyj8xclivaaahdpglskli09nfg425bi0ag8fbhrdcl5dicg07pd")))

(define-public crate-lib_xch-0.10.0 (c (n "lib_xch") (v "0.10.0") (d (list (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "1k8hwmmzngyckf96jm6gygwhgxnx1wyapdkskf9yv87jf29d0gbm")))

(define-public crate-lib_xch-0.10.1 (c (n "lib_xch") (v "0.10.1") (d (list (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "1y9k889b1q62wckalm635b1qvr5zxdyfjpgl9p60cny8ch2hw9qi")))

(define-public crate-lib_xch-0.10.2 (c (n "lib_xch") (v "0.10.2") (d (list (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^1.0") (d #t) (k 0)) (d (n "pest_derive") (r "^1.0") (d #t) (k 0)))) (h "0q7v9y6xwnyyjs5c4lfqi08wxrr30lpdlwr21by9612kyl78dx9r")))

(define-public crate-lib_xch-0.10.3 (c (n "lib_xch") (v "0.10.3") (d (list (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1yd8c9j8g6payyq1m74n7l8hd2ckmva2d9xc05kkkg7r5ff3xhg6")))

(define-public crate-lib_xch-0.10.4 (c (n "lib_xch") (v "0.10.4") (d (list (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "066cka3axs5r61xd9vqqrj7zvjq3qgmz2l6jhlx4qwv0ji21qwrs")))

(define-public crate-lib_xch-0.11.0 (c (n "lib_xch") (v "0.11.0") (d (list (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0z6d8s9vpsxkhxlczxpa489hb74a0q82a4drmpgyiyxgggzwspyh")))

(define-public crate-lib_xch-0.11.1 (c (n "lib_xch") (v "0.11.1") (d (list (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1c1vxsyinrfwrx3s59h4f4cjr1af7c6r9v8w9ffql8i9885mjvjc")))

(define-public crate-lib_xch-0.11.2 (c (n "lib_xch") (v "0.11.2") (d (list (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0khqvjbwcbqabi7fs0x9hrk5c0y3m24cn5qjrfdvy47qx7hqhr4d")))

(define-public crate-lib_xch-0.12.0 (c (n "lib_xch") (v "0.12.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "0nwya8kl3g10sqyfnl0mr4bna91sksmsw01iirfmjqkxb0xksi39")))

(define-public crate-lib_xch-0.12.1 (c (n "lib_xch") (v "0.12.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "120g570qbvw6qc3rb2jhgliz7kwpjf334rzwnmkay594gi6zbxz1")))

(define-public crate-lib_xch-0.13.0 (c (n "lib_xch") (v "0.13.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "id_tree") (r "^1.3") (d #t) (k 0)) (d (n "nalgebra") (r "^0.16") (d #t) (k 0)) (d (n "num") (r "^0.2") (d #t) (k 0)) (d (n "pest") (r "^2.0") (d #t) (k 0)) (d (n "pest_derive") (r "^2.0") (d #t) (k 0)))) (h "1d2nmrc7jmzcsqnnxln6nc2w3aksn5j9f65nh4y34jmgcl20zk3q")))

