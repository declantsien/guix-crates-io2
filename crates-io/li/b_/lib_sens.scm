(define-module (crates-io li b_ lib_sens) #:use-module (crates-io))

(define-public crate-lib_sens-0.1.0 (c (n "lib_sens") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0w6rbjdpjqmwak0whc2w1zzpq4fp5gnrkg86xrvkk5hq6hx5v254")))

(define-public crate-lib_sens-0.1.1 (c (n "lib_sens") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1h4f6pvc9xi9rhn6dymd60z7fmib4g7scr72m1mb4v4vqf0chpma")))

(define-public crate-lib_sens-0.1.2 (c (n "lib_sens") (v "0.1.2") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0im01a6rp7wxmbrk4gbhn44ysrvydwnh2jdgq7hw708s1dmysl1j")))

(define-public crate-lib_sens-0.1.3 (c (n "lib_sens") (v "0.1.3") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "typetag") (r "^0.1.4") (d #t) (k 0)))) (h "0d9s7m6qipnacpyfh8n9d4kz86m02rkqkhmdb0w4zb91kl1sj1bn")))

(define-public crate-lib_sens-0.1.4 (c (n "lib_sens") (v "0.1.4") (d (list (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "typetag") (r "^0.1.4") (d #t) (k 0)))) (h "0gssyyh5zw5866rii929a0z9zxgrl87x63hsmk6plrq1y9bg0000")))

