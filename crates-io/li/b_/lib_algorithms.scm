(define-module (crates-io li b_ lib_algorithms) #:use-module (crates-io))

(define-public crate-lib_algorithms-0.0.1 (c (n "lib_algorithms") (v "0.0.1") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0fa5bl3904br3lzaw6jbqgjpycc08kysc8lwpbkh45inmirjw0hp") (y #t)))

(define-public crate-lib_algorithms-0.0.2 (c (n "lib_algorithms") (v "0.0.2") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1qg1likbq2xgj045c108cv0h5f2rrvb32c1m4pzdgzhl735b3632") (y #t)))

(define-public crate-lib_algorithms-0.0.3 (c (n "lib_algorithms") (v "0.0.3") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "0n1lr1vx0xw4jmvj6l2kqx27r10qchxyacizjzhrh92wvihskxdw") (y #t)))

(define-public crate-lib_algorithms-0.0.4 (c (n "lib_algorithms") (v "0.0.4") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1xbcbvjfw8k3w3fjjyfgqk0m4nyxyxhxzva96kah33rynkcjj488") (y #t)))

(define-public crate-lib_algorithms-0.0.5 (c (n "lib_algorithms") (v "0.0.5") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1my5cazzxcvzxqz522xyppn98pjp1i6am7wxnmwsyj8lrcmvah1y") (y #t)))

(define-public crate-lib_algorithms-0.0.6 (c (n "lib_algorithms") (v "0.0.6") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1dqgp3blwxgf9cnnp1b6bxjsplr295azms13xvw76lsb3kba6q1s") (y #t)))

(define-public crate-lib_algorithms-0.0.7 (c (n "lib_algorithms") (v "0.0.7") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1bnh11palrdlkqb9vxirnnrvg61v1k3n9vfc6lwak1jz320x081y") (y #t)))

(define-public crate-lib_algorithms-0.0.8 (c (n "lib_algorithms") (v "0.0.8") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "00i2j0d5wxj31609hcblsx1sfhbx39q4x4m6w4kipysc3m6jcqqd") (y #t)))

(define-public crate-lib_algorithms-0.0.9 (c (n "lib_algorithms") (v "0.0.9") (d (list (d (n "num") (r "^0.4") (d #t) (k 0)))) (h "1w6nmwvahy03zfz7mqccd9ga02c5j9fgwrzd5sfl66i83m9rbisa")))

