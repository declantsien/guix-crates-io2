(define-module (crates-io li b_ lib_stacked) #:use-module (crates-io))

(define-public crate-lib_stacked-0.1.0 (c (n "lib_stacked") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)))) (h "1smrdcc0a637k5j62dzh33gx18k5hp3pmpl1d42r79nb5w6jhfl3")))

(define-public crate-lib_stacked-0.1.1 (c (n "lib_stacked") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)))) (h "1gal3hcli4hjyr1wdgf48skgrfskkkxmnzrwjgrm4x513szkfm13")))

(define-public crate-lib_stacked-0.1.2 (c (n "lib_stacked") (v "0.1.2") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (d #t) (k 0)))) (h "1i2v21fqmgj5aksijndxcjaf7fbi6b11rg311kqfpaaddy7d1izf")))

