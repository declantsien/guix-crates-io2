(define-module (crates-io li b_ lib_tcp) #:use-module (crates-io))

(define-public crate-lib_tcp-0.1.0 (c (n "lib_tcp") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "11scdixy0ly8walpkms6p6lk4l1i1x5bz9dmjii7liqbi0lwck7p") (y #t)))

(define-public crate-lib_tcp-0.1.1 (c (n "lib_tcp") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1bg7g1m85z8yrdi2gx8znd0bh5lmx016b155p5msn70yix7k8mfj") (y #t)))

