(define-module (crates-io li b_ lib_config) #:use-module (crates-io))

(define-public crate-lib_config-0.1.0 (c (n "lib_config") (v "0.1.0") (d (list (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1ch91nr8181czlcp0aj25243ys95sik9vf666c6ihjphl3h6fpgl")))

(define-public crate-lib_config-0.1.1 (c (n "lib_config") (v "0.1.1") (d (list (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0936gym3h6pyih5pd0vdj0anamm4ym77h7f7q8bwhcwwb15rn1wg")))

(define-public crate-lib_config-0.2.0 (c (n "lib_config") (v "0.2.0") (d (list (d (n "directories") (r "^5.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0c5czakri180lqm76k2knmn2qds7vqjgrmj9yagg16gibnpb58z7")))

