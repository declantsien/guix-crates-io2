(define-module (crates-io li b_ lib_aoc) #:use-module (crates-io))

(define-public crate-lib_aoc-0.5.0 (c (n "lib_aoc") (v "0.5.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)))) (h "1wavdxmddqj5rw6x0sjb1yzh0jh1rwhc4yy3yyi7xksnq28vfnzz")))

(define-public crate-lib_aoc-0.6.0 (c (n "lib_aoc") (v "0.6.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)))) (h "0i7480rq3lwk57p4ppppjs8l5pa6x46b9l6kxi6v0h8vsx35mzwn")))

(define-public crate-lib_aoc-0.6.1 (c (n "lib_aoc") (v "0.6.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)))) (h "1ivsyk5i7668dh277mqvn85x09lha143hbcx597igbyvglrla9w0") (y #t)))

(define-public crate-lib_aoc-0.6.2 (c (n "lib_aoc") (v "0.6.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)))) (h "06sx5hqamgv8l0zriz6638vanjh5rrwxjrr5nz28z6n2byk1qmyf")))

(define-public crate-lib_aoc-0.7.0 (c (n "lib_aoc") (v "0.7.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)))) (h "1rp0dp23smdl99w937cddsyi5jsri0rn11bkk6z8i5aasfr6dvlc")))

(define-public crate-lib_aoc-0.7.1 (c (n "lib_aoc") (v "0.7.1") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)))) (h "0a4h502dvrbhsss12g4iphrxa518mzif10zzj71ja3da6nnfx62v")))

(define-public crate-lib_aoc-0.7.2 (c (n "lib_aoc") (v "0.7.2") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)))) (h "1qk8ps9p15x001xicsa69g5ag5pg5l7qpqyfjb0nh9z3s40h9ly9")))

(define-public crate-lib_aoc-0.8.0 (c (n "lib_aoc") (v "0.8.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "seq-macro") (r "^0.3.1") (d #t) (k 0)))) (h "1vsr9dcsi1knw9frg9vd0zp6iknpr3yprfsvy403xbgf0937w1kf")))

