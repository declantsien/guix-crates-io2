(define-module (crates-io li b_ lib_demo) #:use-module (crates-io))

(define-public crate-lib_demo-0.1.0 (c (n "lib_demo") (v "0.1.0") (h "0jf23lzvd5ss1wv7pf23mqdi6wr4gb83nqh6c7jlxh3pqigirnjp")))

(define-public crate-lib_demo-0.1.1 (c (n "lib_demo") (v "0.1.1") (h "13qvw9qzvpxf0n85rp73bibi2271csbqdwdzj1a6gcnhcm1d64yi")))

(define-public crate-lib_demo-0.1.3 (c (n "lib_demo") (v "0.1.3") (h "1x0l567zir61wzvnpfs4pz2wgwbnwykjlxhz261cfjyi2nl770xk")))

(define-public crate-lib_demo-0.1.4 (c (n "lib_demo") (v "0.1.4") (h "0yfbrlnrywpn48pyk5151gvzkyj2wqh8wzr9izl7irkszbbix894")))

