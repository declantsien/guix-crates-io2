(define-module (crates-io li b_ lib_malloc_freq) #:use-module (crates-io))

(define-public crate-lib_malloc_freq-0.1.0 (c (n "lib_malloc_freq") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "malloc_freq") (r "^0.1.0") (d #t) (k 0)))) (h "1kpix1awjmphvhicvahiv747920pksvzgfhb1jcrgmpky3shm7nf")))

(define-public crate-lib_malloc_freq-0.1.1 (c (n "lib_malloc_freq") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "malloc_freq") (r "^0.1.1") (d #t) (k 0)))) (h "0iirivr501l100wx4q5pj3vdhjqx4r9kzfin3in3yvbsnij1js8r")))

(define-public crate-lib_malloc_freq-0.1.2 (c (n "lib_malloc_freq") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "malloc_freq") (r "^0.1.2") (d #t) (k 0)))) (h "1m9rsqi1hrb5cvnhicy8awmm23sfxx2s2k0c5d58y1sgcjrr1y7v")))

