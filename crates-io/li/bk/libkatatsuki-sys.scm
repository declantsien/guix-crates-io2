(define-module (crates-io li bk libkatatsuki-sys) #:use-module (crates-io))

(define-public crate-libkatatsuki-sys-0.1.0 (c (n "libkatatsuki-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 1)))) (h "08634a1kizfjpq2q7fm5ldsjbz4iyc39kd8k9drklxmpi58kfwlc") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-0.1.1 (c (n "libkatatsuki-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 1)))) (h "01872nx1p6mbwpvvzv1v777dgddsk616xpkcw4ibrjzd4kpfivfi") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-0.1.2 (c (n "libkatatsuki-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 1)))) (h "06hnkzdxm4h3xza9xyxm4vd2yyfx84xzb3k721g8cyilx29v0qmc") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-0.1.3 (c (n "libkatatsuki-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 1)))) (h "1k96n4nk964k78irjjrhyffq65vw3c9ai6wn3d6m2c20qg1jkp46") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-0.2.0 (c (n "libkatatsuki-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 1)))) (h "1ssg1h2dzgw7rbp1lrhh5cw285l5qwhbrr800c7gvmsl459kn211") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-0.2.1 (c (n "libkatatsuki-sys") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 1)))) (h "1n59s7zm3irxk2pzybhx95hsi60ppwpamd8ms2hyryh12gsza9wl") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-0.2.2 (c (n "libkatatsuki-sys") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 1)))) (h "1f7fdavj5jwc748qsf7jf5d40a9fqq4v1gbsxwigydxg9jwrhpak") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-0.3.0 (c (n "libkatatsuki-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 1)))) (h "0l6l5pw0025fcxr2pq24p9c7l8az2zpcg2bv7rjchg7q4bd8g3b4") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.0 (c (n "libkatatsuki-sys") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wr4l1wcm7qgdpxa3360bl15h2lx5pndiqpv1p4z48f8qgr7jz6j") (y #t) (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.1 (c (n "libkatatsuki-sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xbzjpnbnsnkimnk3f04djanvjfvv6sw103bvvl962b2zvml85cj") (y #t) (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.2 (c (n "libkatatsuki-sys") (v "1.0.2") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jd225f2wgcyfdvyb7k79jhm3bmg8sgx0wvgs93nwh56ilk081k4") (y #t) (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.3 (c (n "libkatatsuki-sys") (v "1.0.3") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1lhclvj4plypf0383bzn1j91slgnbkrdkrp05dsdgjm05mjmfbhy") (y #t) (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.4 (c (n "libkatatsuki-sys") (v "1.0.4") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15j8w8nrhnc9mpwhjsrhckqbc0rfxabc2saswmkc47wkmjm3jqml") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.5 (c (n "libkatatsuki-sys") (v "1.0.5") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gm7aqj1qwrzz2j4x0shxhq40a1i942i783dcijwz19pfjbzj03y") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.7 (c (n "libkatatsuki-sys") (v "1.0.7") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1f50wxhwa9idmqn04nc1zaji6zzyahqqrx6n9qgafwrh6zj23h7g") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.8 (c (n "libkatatsuki-sys") (v "1.0.8") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13di5ysg1aiq6lgdzrww6mmfhkf2k23b60bw7zkx5jg2g9gpmdyd") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.9 (c (n "libkatatsuki-sys") (v "1.0.9") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0848pwvl5m1045saccab5p79hqgjzbnsipyb0rj1r1afwi1i3nv2") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.10 (c (n "libkatatsuki-sys") (v "1.0.10") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cnbvdvw819wi48m8g3cxxsqlalzxx2l1xpq442bjv2y0213rpbi") (l "libkatatsuki")))

(define-public crate-libkatatsuki-sys-1.0.11 (c (n "libkatatsuki-sys") (v "1.0.11") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ml793jdh7vscz8sapxycw9ssb7bl7nm2xvgp4wy8ficbszfz0nh") (l "libkatatsuki")))

