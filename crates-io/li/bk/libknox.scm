(define-module (crates-io li bk libknox) #:use-module (crates-io))

(define-public crate-libknox-0.1.0 (c (n "libknox") (v "0.1.0") (d (list (d (n "gpgme") (r "^0.8") (d #t) (k 0)) (d (n "protobuf") (r "^2.2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1v7pvkc3g1pxxwvslx631s8dp68wipy6lyp8gjzax7n9jy1ppy74") (f (quote (("protobufs"))))))

(define-public crate-libknox-0.1.1 (c (n "libknox") (v "0.1.1") (d (list (d (n "gpgme") (r "^0.8") (d #t) (k 0)) (d (n "protobuf") (r "^2.2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1xq3k32sk2q162zj8p7i5bz06m9xxj01f5h0xi6qqz8mkmpw1mz5") (f (quote (("protobufs"))))))

(define-public crate-libknox-0.2.0 (c (n "libknox") (v "0.2.0") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "gpgme") (r "^0.8") (d #t) (k 0)) (d (n "protobuf") (r "^2.2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "0w6cicfnlys33vz7r4s4w9bm04sawll4shfpxamixx55jcggbk6y") (f (quote (("protobufs"))))))

(define-public crate-libknox-0.2.1 (c (n "libknox") (v "0.2.1") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "gpgme") (r "^0.8") (d #t) (k 0)) (d (n "protobuf") (r "^2.2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "04pzb9hi1w5prpw607hyk8bxf4bg0vrgsl5i0yn2ig7fvbhx0b0s") (f (quote (("protobufs"))))))

(define-public crate-libknox-0.3.0 (c (n "libknox") (v "0.3.0") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "gpgme") (r "^0.8") (d #t) (k 0)) (d (n "oath") (r "^0.10") (d #t) (k 0)) (d (n "protobuf") (r "^2.2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1kklh1ik9bw9maq6w592l8cw8s4s1qxy41hiwiswwnp0pcn3c15k") (f (quote (("protobufs"))))))

(define-public crate-libknox-0.3.1 (c (n "libknox") (v "0.3.1") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "gpgme") (r "^0.8") (d #t) (k 0)) (d (n "oath") (r "^0.10") (d #t) (k 0)) (d (n "protobuf") (r "^2.2") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.0") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1nvdqj8xr80ipgsy0c17c9flrhjyvz4v2a0swaahqg814bsjsqd8") (f (quote (("protobufs"))))))

(define-public crate-libknox-0.3.2 (c (n "libknox") (v "0.3.2") (d (list (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "gpgme") (r "^0.8") (d #t) (k 0)) (d (n "oath") (r "^0.10") (d #t) (k 0)) (d (n "protobuf") (r "^2.10.1") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.10.1") (d #t) (k 1)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 0)))) (h "1yf7008gz9jw4qjjxnpbb3vl5dc0klfqzngnq2lgvl8df8frs2xj") (f (quote (("protobufs"))))))

(define-public crate-libknox-0.4.0 (c (n "libknox") (v "0.4.0") (d (list (d (n "git2") (r "^0.13.5") (d #t) (k 0)) (d (n "gpgme") (r "^0.9.2") (d #t) (k 0)) (d (n "oath") (r "^0.10.2") (d #t) (k 0)) (d (n "protobuf") (r "^2.14") (d #t) (k 0)) (d (n "protoc-rust") (r "^2.14") (d #t) (k 1)) (d (n "uuid") (r "^0.8.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "055wh5v1f1sjp56zrwmyhllr03kydidqsvs7f8qj8s6iz8kkigrs") (f (quote (("protobufs"))))))

