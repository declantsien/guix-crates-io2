(define-module (crates-io li bk libkrb5) #:use-module (crates-io))

(define-public crate-libkrb5-0.0.1 (c (n "libkrb5") (v "0.0.1") (d (list (d (n "libkrb5-sys") (r "^0.0") (d #t) (k 0)))) (h "0nj6ck26f5sbvpl669r4l0wb351wkga9bj67l3dvb827svn6bv2r")))

(define-public crate-libkrb5-0.0.3 (c (n "libkrb5") (v "0.0.3") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libkrb5-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0v3jzz1kiq5b3p7h5h0jwkx3srlbhw5w4g892w6y7dkj38aslggj")))

