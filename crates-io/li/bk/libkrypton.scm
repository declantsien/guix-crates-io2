(define-module (crates-io li bk libkrypton) #:use-module (crates-io))

(define-public crate-libkrypton-0.1.0 (c (n "libkrypton") (v "0.1.0") (h "02j16i10lxknzy0jm6d4bzyraa68j2as7c4fph12idand75scfbx")))

(define-public crate-libkrypton-0.1.1 (c (n "libkrypton") (v "0.1.1") (h "0spx2dh91d4p1qlhjl806i9fr067py4jjv5ayspggqgsgl6bn3lr")))

(define-public crate-libkrypton-0.1.2 (c (n "libkrypton") (v "0.1.2") (h "10i1c1mkcpbbj3cz74bdfspyc22q3w4c1izmdjsza5qivrxn7n7y") (f (quote (("std") ("default" "std"))))))

