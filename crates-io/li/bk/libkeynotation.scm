(define-module (crates-io li bk libkeynotation) #:use-module (crates-io))

(define-public crate-libkeynotation-0.1.1 (c (n "libkeynotation") (v "0.1.1") (d (list (d (n "test-log") (r "^0.2") (f (quote ("trace"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (o #t) (d #t) (k 0)))) (h "0f8yijlffcxaj1cd1w040hlb07v94zh21ypnpv75kajr95pvclf1") (f (quote (("default")))) (s 2) (e (quote (("logging" "dep:tracing" "dep:test-log" "dep:tracing-subscriber"))))))

(define-public crate-libkeynotation-0.1.2 (c (n "libkeynotation") (v "0.1.2") (d (list (d (n "test-log") (r "^0.2") (f (quote ("trace"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (o #t) (d #t) (k 0)))) (h "0mgj7izmmjdx846raz8xlwkmqrca465rxz40vdjj8avw0r1v24bl") (f (quote (("default")))) (s 2) (e (quote (("logging" "dep:tracing" "dep:test-log" "dep:tracing-subscriber"))))))

(define-public crate-libkeynotation-0.2.0 (c (n "libkeynotation") (v "0.2.0") (d (list (d (n "test-log") (r "^0.2") (f (quote ("trace"))) (o #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3") (f (quote ("env-filter"))) (o #t) (d #t) (k 0)))) (h "1kvaq5nq70fvcyj71aj4wh2k9rhmxbrnly3pw5rqsa81474j13j3") (f (quote (("default")))) (s 2) (e (quote (("logging" "dep:tracing" "dep:test-log" "dep:tracing-subscriber"))))))

