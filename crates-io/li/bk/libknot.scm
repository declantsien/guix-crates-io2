(define-module (crates-io li bk libknot) #:use-module (crates-io))

(define-public crate-libknot-0.1.0 (c (n "libknot") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "libknot-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)))) (h "1mkx06gm9nw9i6732gb2l02gi299i9xq69fxn1ryggwd8i2xgwyz")))

(define-public crate-libknot-0.2.0 (c (n "libknot") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.8") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "libknot-sys") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "unix_socket2") (r "^0.5.4") (d #t) (k 0)))) (h "03k7p2kb3qccnmb9nh95xaj5wslcb68hqa7fkwycdhjpahcw9hyl") (f (quote (("native" "libknot-sys") ("async" "futures-util"))))))

(define-public crate-libknot-0.2.1 (c (n "libknot") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.3.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "fallible-iterator") (r "^0.2.0") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.8") (f (quote ("io"))) (o #t) (d #t) (k 0)) (d (n "libknot-sys") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.3") (d #t) (k 0)) (d (n "unix_socket2") (r "^0.5.4") (d #t) (k 0)))) (h "0br8f4gcrs3676p6q0chsipazdxhrmqrhp6fb3kkgq6js86s8i84") (f (quote (("native" "libknot-sys") ("async" "futures-util"))))))

