(define-module (crates-io li bk libknot-sys) #:use-module (crates-io))

(define-public crate-libknot-sys-0.1.0 (c (n "libknot-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)))) (h "0r09j9s8zakvbzrhprmf72d6fc4ijmd934bffa929x993gay213m")))

(define-public crate-libknot-sys-0.1.1 (c (n "libknot-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.37.4") (d #t) (k 1)))) (h "02jwapchizak7578m9ncybc54pmzihdkfbfaj49h9r4i7sx9wf01")))

