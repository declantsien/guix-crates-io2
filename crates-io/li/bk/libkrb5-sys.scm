(define-module (crates-io li bk libkrb5-sys) #:use-module (crates-io))

(define-public crate-libkrb5-sys-0.0.1 (c (n "libkrb5-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0nwavdc34ff0mv8niixjzxdk155liqbz3n5as6jn65lwgxrj9m5p")))

(define-public crate-libkrb5-sys-0.0.2 (c (n "libkrb5-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0lqch4bg2jkyyz7sf39iyi3k68ynbrcy00ym9798m3bwf0zimrqd")))

(define-public crate-libkrb5-sys-0.0.3 (c (n "libkrb5-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "1hlnggw52sw0rp32jbp84amh87s355jshpgl8amj2pdgi4vnzg1y") (l "krb5")))

