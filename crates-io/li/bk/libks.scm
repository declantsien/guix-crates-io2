(define-module (crates-io li bk libks) #:use-module (crates-io))

(define-public crate-libks-0.1.0 (c (n "libks") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0sgc25l7ds1d587x6gzmcc4llhbfns49mny2w0z041vdfmcl0lzv") (y #t)))

(define-public crate-libks-0.1.1 (c (n "libks") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "01j1n22zz6cxx2lxqb5dp0yizg170kybiy4vs50y2vkq7nfk51br") (y #t)))

(define-public crate-libks-0.2.0 (c (n "libks") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0l5b0ql6phfkdhmgc3hpwhc3356z61k0zrqdihb24ji8p8335y6h") (y #t)))

(define-public crate-libks-0.2.1 (c (n "libks") (v "0.2.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "07lf25y97746hf1lj87591qkga83ld7wydv6173hfx4369f53agg") (y #t)))

(define-public crate-libks-0.2.2 (c (n "libks") (v "0.2.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "flate2") (r "^1.0.25") (d #t) (k 0)) (d (n "rust-ini") (r "^0.19.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("serde_derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "1xc160dn3kpmfnpbvmy64hpl889zk2f5rcpsaffqm01fzjj2iz3a") (s 2) (e (quote (("serde" "dep:serde"))))))

