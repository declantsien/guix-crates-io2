(define-module (crates-io li bk libktx-sys) #:use-module (crates-io))

(define-public crate-libktx-sys-0.1.0 (c (n "libktx-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "clang-sys") (r "^1.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "19rnznr30ypn5gfw57rkmjr5m2gv4dj9s899mnbip4rw2zyvw802") (y #t)))

(define-public crate-libktx-sys-0.1.1 (c (n "libktx-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "clang-sys") (r "^1.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "17vij1q6zl7xdyvg3j6cac8msa0h9spk096q47ka34sj18nmisgv")))

(define-public crate-libktx-sys-0.1.2 (c (n "libktx-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "clang-sys") (r "^1.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "17allmz29xsi7xycp37jwdhl0m35d66phkg87vq69fr9dn6ra63i")))

(define-public crate-libktx-sys-0.1.3 (c (n "libktx-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "clang-sys") (r "^1.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0i416im2cz1h6d9njicnnzb3pxyrlx2qmbsdycmk8dg8aqhpzmjk")))

(define-public crate-libktx-sys-0.1.4 (c (n "libktx-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "clang-sys") (r "^1.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1.10.2") (o #t) (d #t) (k 1)))) (h "029wx0fxjxvk6hl3hl0wrfzpxgbj51lpkyig2z5nb05ly75skj9c") (f (quote (("postprocess") ("default") ("bindgen"))))))

(define-public crate-libktx-sys-0.1.5 (c (n "libktx-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "clang-sys") (r "^1.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1.10.2") (o #t) (d #t) (k 1)))) (h "182hm396jwnl1bjd2i6309bn9ly02gz2cbbk6qz4dk1p9bcbxfg0") (f (quote (("postprocess") ("default") ("bindgen"))))))

(define-public crate-libktx-sys-0.1.6 (c (n "libktx-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "clang-sys") (r "^1.2") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "regex") (r "^1.10.2") (o #t) (d #t) (k 1)))) (h "1skcrxqic2jhc2j4qifaas41p3bmq9raaxi9954km09xkaz8ip63") (f (quote (("postprocess") ("default") ("bindgen"))))))

