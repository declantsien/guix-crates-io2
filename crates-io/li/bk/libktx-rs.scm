(define-module (crates-io li bk libktx-rs) #:use-module (crates-io))

(define-public crate-libktx-rs-0.1.0 (c (n "libktx-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libktx-rs-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "libktx-rs-sys") (r "^0.1.40") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "10ljh20bslpns25gxqsfk32mchf9wbx8gy6ssh516ai1yiddafv1") (f (quote (("write" "libktx-rs-sys/write") ("test-images") ("default" "write")))) (y #t)))

(define-public crate-libktx-rs-0.2.0 (c (n "libktx-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libktx-rs-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "libktx-rs-sys") (r "^0.2.40") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1zrlllgs5jplm3c9c6nqx2zwqrzbbrhll4mikv1zikxzv9d4vxxv") (f (quote (("write" "libktx-rs-sys/write") ("test-images") ("default" "write")))) (y #t)))

(define-public crate-libktx-rs-0.2.1 (c (n "libktx-rs") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libktx-rs-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "libktx-rs-sys") (r "^0.2.41") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0fzl90amyv15lnr29kszhmxs4p8ii2148h3910ldikp6hzib291a") (f (quote (("write" "libktx-rs-sys/write") ("test-images") ("default" "write")))) (y #t)))

(define-public crate-libktx-rs-0.2.2 (c (n "libktx-rs") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libktx-rs-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "libktx-rs-sys") (r "^0.2.42") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0p88ilacwhwgscgmgbclwi1f7dcgc0l9pch36c4r5jvr6n5fv2ci") (f (quote (("write" "libktx-rs-sys/write") ("test-images") ("default" "write")))) (y #t)))

(define-public crate-libktx-rs-0.2.3 (c (n "libktx-rs") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libktx-rs-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "libktx-rs-sys") (r "^0.2.43") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1acy2i9861vas0j7cdyhz6y786iz4mhbh9q2fqfdlm75wh974vii") (f (quote (("write" "libktx-rs-sys/write") ("test-images") ("default" "write"))))))

(define-public crate-libktx-rs-0.2.4 (c (n "libktx-rs") (v "0.2.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "libktx-rs-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "libktx-rs-sys") (r "^0.2.44") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0vgbf7cjrz43g1vbakhl5yz8gmzi2c2bm6gxx2szgwm4dgbd3mhf") (f (quote (("write" "libktx-rs-sys/write") ("test-images") ("default" "write"))))))

(define-public crate-libktx-rs-0.3.0 (c (n "libktx-rs") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libktx-rs-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "libktx-rs-sys") (r "^0.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "1a577gzvll8gpdrivybnafrhsmwym0f9lsjwns8i47xqi7h90iif") (f (quote (("write" "libktx-rs-sys/write") ("test-images") ("default" "write"))))))

(define-public crate-libktx-rs-0.3.1 (c (n "libktx-rs") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libktx-rs-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "libktx-rs-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "103sg2afvcf6l1cy8mnwphpyjrhanslmqxyf174sxlfmd8mz0jz9") (f (quote (("write" "libktx-rs-sys/write") ("test-images") ("default" "write"))))))

(define-public crate-libktx-rs-0.3.2 (c (n "libktx-rs") (v "0.3.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libktx-rs-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "libktx-rs-sys") (r "^0.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "0vgg99jcawk26f4ngv5cccg84qyzlc5jmp9ppkcd7s2aysicalni") (f (quote (("write" "libktx-rs-sys/write") ("test-images") ("default" "write"))))))

(define-public crate-libktx-rs-0.3.3 (c (n "libktx-rs") (v "0.3.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "libktx-rs-macros") (r "^0.1.0") (d #t) (k 2)) (d (n "libktx-rs-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)))) (h "035d5hjk90ib07ngi4yqxz5zb7grpwy9bl72r704vvpsdgc414id") (f (quote (("write" "libktx-rs-sys/write") ("test-images") ("default" "write"))))))

