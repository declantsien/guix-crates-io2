(define-module (crates-io li bv libvibrant) #:use-module (crates-io))

(define-public crate-libvibrant-1.0.0 (c (n "libvibrant") (v "1.0.0") (d (list (d (n "libXNVCtrl-sys") (r "^1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xrandr"))) (d #t) (k 0)))) (h "1wrvdpaamfkq3bibzzpd49670q7hi2mhal1psadnlswi1b1akbqc") (y #t)))

(define-public crate-libvibrant-1.0.1 (c (n "libvibrant") (v "1.0.1") (d (list (d (n "libXNVCtrl-sys") (r "^1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xrandr"))) (d #t) (k 0)))) (h "0phr3nf5gfyf0p94bzj6j9rbsvbzy9iq4b00p0sj9pr4r4nsxxaa")))

(define-public crate-libvibrant-1.0.2 (c (n "libvibrant") (v "1.0.2") (d (list (d (n "libXNVCtrl-sys") (r "^1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xrandr"))) (d #t) (k 0)))) (h "1fgqybj6ffbcy9zvh142r0i6g3swga6w63036ah7zgyr692jwdbl")))

(define-public crate-libvibrant-1.0.3 (c (n "libvibrant") (v "1.0.3") (d (list (d (n "libXNVCtrl-sys") (r "^1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xrandr"))) (d #t) (k 0)))) (h "08x9z8jyai19z0k083wjx5bq28b8wgwix8kqlv7bj1dw94bj048c")))

(define-public crate-libvibrant-1.0.4 (c (n "libvibrant") (v "1.0.4") (d (list (d (n "libXNVCtrl-sys") (r "^1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xrandr"))) (d #t) (k 0)))) (h "0bzhn5jwpz87xxmwn8ckjp4yz7an2bb52ringip3znxkcxk4c4nv")))

(define-public crate-libvibrant-1.0.5 (c (n "libvibrant") (v "1.0.5") (d (list (d (n "libXNVCtrl-sys") (r "^1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xrandr"))) (d #t) (k 0)))) (h "1fyja6rn6w3q23dsnx4xj0cvk2m1wd5rrf2bsfxhyc6xs4gskl0r")))

(define-public crate-libvibrant-1.0.6 (c (n "libvibrant") (v "1.0.6") (d (list (d (n "libXNVCtrl-sys") (r "^1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xrandr"))) (d #t) (k 0)))) (h "09kyixkbf3637m610byrl9n7dlfapmh5ni7kybfkxl543j2c3ka4")))

(define-public crate-libvibrant-1.1.0 (c (n "libvibrant") (v "1.1.0") (d (list (d (n "libXNVCtrl-sys") (r "^1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xrandr"))) (d #t) (k 0)))) (h "1njz85rg1ca8v8ssgvh8608j387yzdjh4g1y0g8y8jljjd3mys1s")))

(define-public crate-libvibrant-1.1.1 (c (n "libvibrant") (v "1.1.1") (d (list (d (n "libXNVCtrl-sys") (r "^1.29") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "x11") (r "^2.18") (f (quote ("xrandr"))) (d #t) (k 0)))) (h "05r8kvm9ggw7gr3b5klfxy1nx9x0sbnd9835gdl4snrsppkq4480")))

