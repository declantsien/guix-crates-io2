(define-module (crates-io li bv libvmdk-sys) #:use-module (crates-io))

(define-public crate-libvmdk-sys-0.1.0 (c (n "libvmdk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 2)))) (h "0jj57kn2i9c3n8vhmx6g0gyx8b5765sarbz4lvn0ncli8ipbz0s8")))

