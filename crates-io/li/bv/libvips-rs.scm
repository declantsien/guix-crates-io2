(define-module (crates-io li bv libvips-rs) #:use-module (crates-io))

(define-public crate-libvips-rs-8.14.2 (c (n "libvips-rs") (v "8.14.2") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "14qx313yr179kcb3rrpp681ybgkypfnpplda7k280n9zf0l6l818")))

(define-public crate-libvips-rs-8.14.2-r1 (c (n "libvips-rs") (v "8.14.2-r1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0nk8xf9m6yz1zaa9xk485xxgjdmkrqp0h8bgvva0977rphgs3l0k")))

(define-public crate-libvips-rs-8.15.1 (c (n "libvips-rs") (v "8.15.1") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "06ngm0hiqimmflmvb5r3xzwcryx1zwivbvf5z4yap7rbk8kqkzq2")))

