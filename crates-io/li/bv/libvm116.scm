(define-module (crates-io li bv libvm116) #:use-module (crates-io))

(define-public crate-libvm116-0.1.0 (c (n "libvm116") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hidapi-rusb") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 2)))) (h "1jzawndy4q2j2715ycdivsfmwwr3n266gi1jbwi8hg1gbvy0sn98")))

(define-public crate-libvm116-0.1.1 (c (n "libvm116") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.9") (d #t) (k 2)) (d (n "hidapi-rusb") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 2)))) (h "1l0ncqlyhb7f25sq16bgidziqk0h718bmf6hsxwn6n7r1pka5w2r")))

