(define-module (crates-io li bv libvips-sys) #:use-module (crates-io))

(define-public crate-libvips-sys-0.1.0 (c (n "libvips-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.33") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "152krv093qsi7740r3yvcprc9y3yzrp2dac9q7x7yd5a5c15kbql")))

