(define-module (crates-io li bv libvpx-sys) #:use-module (crates-io))

(define-public crate-libvpx-sys-1.4.0 (c (n "libvpx-sys") (v "1.4.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "pnacl-build-helper") (r "^1.4") (d #t) (k 1)))) (h "1ag2p0pl7j4gxs8dskqq72v6fzl60yrkqlcqj48chv0mklrf653x")))

(define-public crate-libvpx-sys-1.4.1 (c (n "libvpx-sys") (v "1.4.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "pnacl-build-helper") (r "^1.4") (d #t) (k 1)))) (h "030q6f4jy1g72fgf13kf536sz3cffk86dbphpywa7qvnm0dr592m")))

(define-public crate-libvpx-sys-1.4.2 (c (n "libvpx-sys") (v "1.4.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "pnacl-build-helper") (r "^1.4") (d #t) (k 1)))) (h "02r1mq3f1s8hi2sx4wqwy1k3flhw4vm8a0mn0x1zj3bnzlpz3sq6")))

