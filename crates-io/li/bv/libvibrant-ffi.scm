(define-module (crates-io li bv libvibrant-ffi) #:use-module (crates-io))

(define-public crate-libvibrant-ffi-1.1.0 (c (n "libvibrant-ffi") (v "1.1.0") (d (list (d (n "libvibrant") (r "^1.1.0") (d #t) (k 0)))) (h "0w27y8hp32mm00ibag6y5lf8xwgpp0k94manql27hpfdz6nkjgly")))

(define-public crate-libvibrant-ffi-1.1.1 (c (n "libvibrant-ffi") (v "1.1.1") (d (list (d (n "libvibrant") (r "^1.1.1") (d #t) (k 0)))) (h "13ym8i8z1ksbccr3fsymxzsz103smrjr64swldxkl2sslb1ilzb1")))

