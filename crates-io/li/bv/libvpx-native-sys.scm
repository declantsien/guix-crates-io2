(define-module (crates-io li bv libvpx-native-sys) #:use-module (crates-io))

(define-public crate-libvpx-native-sys-2.0.0 (c (n "libvpx-native-sys") (v "2.0.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1layv3jvmp3jb7i7h2m19ysbpq39cx42mwm8bkf8gqr314b21mvg") (y #t)))

(define-public crate-libvpx-native-sys-3.0.0 (c (n "libvpx-native-sys") (v "3.0.0") (d (list (d (n "bindgen") (r "^0.21.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "15bbiqw97x0jswixgv8wg69ilfc3b6q3011d8g5nqr7lyj8s4qy1") (y #t)))

(define-public crate-libvpx-native-sys-3.0.2 (c (n "libvpx-native-sys") (v "3.0.2") (d (list (d (n "bindgen") (r "^0.22.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "0wy1b0nfy7maga87a1vkvmwbwbmd3rgy1jv2w07fijc9yg2w6dv8") (y #t)))

(define-public crate-libvpx-native-sys-3.0.3 (c (n "libvpx-native-sys") (v "3.0.3") (d (list (d (n "bindgen") (r "^0.22.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "16m5v1iay25wfz6gncqbp03q4mzhs02ccwfxyfqbvp2a0gzrddiz") (y #t)))

(define-public crate-libvpx-native-sys-3.0.4 (c (n "libvpx-native-sys") (v "3.0.4") (d (list (d (n "bindgen") (r "^0.25.3") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)))) (h "1ssan61l7fy9llwj48g0ykmm9c030frfkrspx3wffppahs0pgg7d")))

(define-public crate-libvpx-native-sys-4.0.0 (c (n "libvpx-native-sys") (v "4.0.0") (d (list (d (n "bindgen") (r "^0.25.5") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "03459rshn8784nwfm6j3b0a8zipi80l5cniz2cx855295ykzcpka") (f (quote (("generate" "bindgen") ("default"))))))

(define-public crate-libvpx-native-sys-4.0.1 (c (n "libvpx-native-sys") (v "4.0.1") (d (list (d (n "bindgen") (r "^0.25.5") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "1rpfsjnmhs6q9c9wyhgb0db1qnhkzf4n9bmn89112pl5r2gj7x2d") (f (quote (("generate" "bindgen") ("default"))))))

(define-public crate-libvpx-native-sys-4.0.2 (c (n "libvpx-native-sys") (v "4.0.2") (d (list (d (n "bindgen") (r "^0.31.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "0yn7fmf6ngw7zr3z7r6rf3fs7gqn0lhis2qgrhlgx69hpf7dwdaf") (f (quote (("generate" "bindgen") ("default"))))))

(define-public crate-libvpx-native-sys-4.0.3 (c (n "libvpx-native-sys") (v "4.0.3") (d (list (d (n "bindgen") (r "^0.32.3") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "14bzwkbs787ylwg2b2lx09ig5p9x1wz1gh64z1n3y0j6ppzh5cfc") (f (quote (("generate" "bindgen") ("default"))))))

(define-public crate-libvpx-native-sys-4.0.4 (c (n "libvpx-native-sys") (v "4.0.4") (d (list (d (n "bindgen") (r "^0.36.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "19cd9b0d74v5bhjzcdla8a83ghs0hpn53djp9ygsy71m14g23vh1") (f (quote (("generate" "bindgen") ("default")))) (y #t)))

(define-public crate-libvpx-native-sys-4.0.5 (c (n "libvpx-native-sys") (v "4.0.5") (d (list (d (n "bindgen") (r "^0.36.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "0s9z8z9z23cmg2fy6niqhdl3wgpw2mk5apllv5j743hw2rjfgvhf") (f (quote (("generate" "bindgen") ("default"))))))

(define-public crate-libvpx-native-sys-4.0.6 (c (n "libvpx-native-sys") (v "4.0.6") (d (list (d (n "bindgen") (r "^0.36.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "1lpr9a81yn1m40vpwpvkk04aprswmipzm0z0zwfdvbadz9c88mqd") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-libvpx-native-sys-4.0.7 (c (n "libvpx-native-sys") (v "4.0.7") (d (list (d (n "bindgen") (r "^0.36.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.7.0") (d #t) (k 1)))) (h "0p5byz4y04bkpnjx6m05px8zvfk5fd3ac2s89h0wyxb3da2yjzy5") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-libvpx-native-sys-5.0.0 (c (n "libvpx-native-sys") (v "5.0.0") (d (list (d (n "bindgen") (r "^0.53.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.5") (d #t) (k 1)) (d (n "semver-parser") (r "^0.9") (d #t) (k 1)))) (h "0cr9mmh1vms2k77zzh31h60bpgyc22kh1zkai79q7jxpsg8p5b4s") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-libvpx-native-sys-5.0.1 (c (n "libvpx-native-sys") (v "5.0.1") (d (list (d (n "bindgen") (r "^0.55.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.1") (d #t) (k 1)))) (h "04i3wdp3yf23al30v4g2l2s5j69z0mriy1qjww9f79n80zi76sij") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-libvpx-native-sys-5.0.2 (c (n "libvpx-native-sys") (v "5.0.2") (d (list (d (n "bindgen") (r "^0.56.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "0awkj3qzxqxr7z2avvdjmfsvb9b97cyxrghr8kmjdi7f5rhi5cdl") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-libvpx-native-sys-5.0.3 (c (n "libvpx-native-sys") (v "5.0.3") (d (list (d (n "bindgen") (r "^0.58.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "0avh7bplspybsqlm3qvxl0mipl293ayzpxhvjc2lyb60q38x80yw") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-libvpx-native-sys-5.0.4 (c (n "libvpx-native-sys") (v "5.0.4") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "17ximx6mpqa8xydlj19zbzz5cavc2k2hyzhsax7395qsh6x2xjg6") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-libvpx-native-sys-5.0.5 (c (n "libvpx-native-sys") (v "5.0.5") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "1x4p65c92wx4y36is8zi7f4iag377avrh7nmbcnczx3xhmmxjnrf") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-libvpx-native-sys-5.0.6 (c (n "libvpx-native-sys") (v "5.0.6") (d (list (d (n "bindgen") (r "^0.59.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "0007y65cx93nlgs5y1afwrkwnlnif2kwgw6y0ab5wdd8ppfljvbc") (f (quote (("generate" "bindgen") ("default")))) (l "vpx")))

(define-public crate-libvpx-native-sys-5.0.7 (c (n "libvpx-native-sys") (v "5.0.7") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "1bcg7g0h2wv97b8v2ll26y980zwjcsrxnf0ckpvq6aiwh8c4dq1d") (f (quote (("default")))) (l "vpx") (s 2) (e (quote (("generate" "dep:bindgen"))))))

(define-public crate-libvpx-native-sys-5.0.8 (c (n "libvpx-native-sys") (v "5.0.8") (d (list (d (n "bindgen") (r "^0.60.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "1skqqxvvgsp0aay0x2d9sq3a0846zf5hb20dyna3sy0552dpx93p") (f (quote (("default")))) (l "vpx") (s 2) (e (quote (("generate" "dep:bindgen"))))))

(define-public crate-libvpx-native-sys-5.0.9 (c (n "libvpx-native-sys") (v "5.0.9") (d (list (d (n "bindgen") (r "^0.64.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "1pydfy7fdzj1yxlp0b9yppqjbq4wr2fhdnc3qrw7brcr9lwzy9f0") (f (quote (("default")))) (l "vpx") (s 2) (e (quote (("generate" "dep:bindgen"))))))

(define-public crate-libvpx-native-sys-5.0.10 (c (n "libvpx-native-sys") (v "5.0.10") (d (list (d (n "bindgen") (r "^0.65.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "0gc7xsxvn2r4ps71l9dxyaajnnsh4gd177qzzbsmw4f0bbnjsa4l") (f (quote (("default")))) (l "vpx") (s 2) (e (quote (("generate" "dep:bindgen"))))))

(define-public crate-libvpx-native-sys-5.0.11 (c (n "libvpx-native-sys") (v "5.0.11") (d (list (d (n "bindgen") (r "^0.66.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "0qgcap4ckl4sr1k3f92pi456nndgp32gmn2qdf2s26riax0vl3pf") (f (quote (("default")))) (l "vpx") (s 2) (e (quote (("generate" "dep:bindgen"))))))

(define-public crate-libvpx-native-sys-5.0.12 (c (n "libvpx-native-sys") (v "5.0.12") (d (list (d (n "bindgen") (r "^0.68.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "0y5ah3j1529y1wpal8b8rrzfr2a9dn3i1play05vbzz1pacjl1sk") (f (quote (("default")))) (l "vpx") (s 2) (e (quote (("generate" "dep:bindgen"))))))

(define-public crate-libvpx-native-sys-5.0.13 (c (n "libvpx-native-sys") (v "5.0.13") (d (list (d (n "bindgen") (r "^0.69.1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)) (d (n "semver-parser") (r "^0.10.2") (d #t) (k 1)))) (h "0qq4q7820703v8cis3zmrd3lpjlcb8gzbblyx9miwwgkr5k14m4x") (f (quote (("default")))) (l "vpx") (s 2) (e (quote (("generate" "dep:bindgen"))))))

