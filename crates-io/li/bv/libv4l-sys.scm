(define-module (crates-io li bv libv4l-sys) #:use-module (crates-io))

(define-public crate-libv4l-sys-0.1.0 (c (n "libv4l-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hfik2yh7q2xnn7bn9fayp26m8x12clxkj86s5pqgr7k3qwyqn9r")))

(define-public crate-libv4l-sys-0.2.0 (c (n "libv4l-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0wab5b6yqvjgz5za305qh1bmy8lkh75hlpjv7qq96svskxqr2zzc")))

(define-public crate-libv4l-sys-0.2.1 (c (n "libv4l-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1kbcc4bs8frhm3vcmgmbxddifjd71250kpm4lgkv98clzyjq1n30")))

(define-public crate-libv4l-sys-0.2.2 (c (n "libv4l-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jglmhxn5dp7q77cvd6q899zvirmj68q0jj458z3q2pdrjp9a6wl")))

(define-public crate-libv4l-sys-0.3.0 (c (n "libv4l-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1dzqrj2a1hbbm7f57bfi5wqmqkv672f3bkch37yn17wicdss56xg") (y #t)))

(define-public crate-libv4l-sys-0.3.1 (c (n "libv4l-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1q3c1hqjnna7m254fq2bjp9nhw3vr5wwrsv10rlgpic57cc8rdav")))

