(define-module (crates-io li bv libvips) #:use-module (crates-io))

(define-public crate-libvips-8.3.0 (c (n "libvips") (v "8.3.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 1)) (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^3.1") (d #t) (k 1)))) (h "1x432f3xx1kw58s05ffwkjx5rbjq72x52m0yr3dirkvljpngxjnf") (y #t)))

(define-public crate-libvips-1.0.0 (c (n "libvips") (v "1.0.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 1)) (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^3.1") (d #t) (k 1)))) (h "01ix28n1bfy3wc2lmm00fqdhn9r0wdcnjnmyg4f9fvsw7ncgw0d0")))

(define-public crate-libvips-1.1.0 (c (n "libvips") (v "1.1.0") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 1)) (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^3.1") (d #t) (k 1)))) (h "191c2idpv5ik5ma0d44iz8rfkgjrg0g41dcrkz154c1y8vb2ky7f")))

(define-public crate-libvips-1.1.1 (c (n "libvips") (v "1.1.1") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 1)) (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^3.1") (d #t) (k 1)))) (h "18h8c44b8hqlrdsxpc665lyymhm456b7lqsp6s2fwj247bqnf179")))

(define-public crate-libvips-1.1.2 (c (n "libvips") (v "1.1.2") (d (list (d (n "Inflector") (r "^0.11") (d #t) (k 1)) (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)) (d (n "which") (r "^3.1") (d #t) (k 1)))) (h "0mp1fchy967liiwn26aja0q3i9gxamkcwn31vkknxcm3jlbvajl1")))

(define-public crate-libvips-1.1.3 (c (n "libvips") (v "1.1.3") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "10gqwcfgwlgy7qgzfk0b3b70f57c7kl7ikk9vjz35dx2cr7gigyk")))

(define-public crate-libvips-1.1.4 (c (n "libvips") (v "1.1.4") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1asaxzzrvrna5yy668vqs3p5xm4px0208hjcijp2gj25xq0hx4ni")))

(define-public crate-libvips-1.1.5 (c (n "libvips") (v "1.1.5") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1hingsxj0zgah2m3bqr8vf25h596262p8wvsvxmz1jvsg84520wl")))

(define-public crate-libvips-1.2.0 (c (n "libvips") (v "1.2.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "17ikrdi9b4hgm2g38ddc2p5pln2gbclwip6ihdmrxz99bm809p8b")))

(define-public crate-libvips-1.3.0 (c (n "libvips") (v "1.3.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0khadhyzmdbp3by1s69ynpqsnma2bk9gq4mrh8z8y3rc7p4fxc0c")))

(define-public crate-libvips-1.4.0 (c (n "libvips") (v "1.4.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "073v2vrs6qfya724n7k605h64i8gwspyp4ri0py7f1lzh5dalmv4")))

(define-public crate-libvips-1.4.1 (c (n "libvips") (v "1.4.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1wh90whssqq0l4rz487ddmiibj8adq5z7z0d3dxyc56n2brd2gzx")))

(define-public crate-libvips-1.4.2 (c (n "libvips") (v "1.4.2") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0inp3n52lbhn5y16d3nl2gr3jr6mp01gix571gix0qmddvdd8v1m")))

(define-public crate-libvips-1.4.3 (c (n "libvips") (v "1.4.3") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1qmcd4wx4mn0yhm8s7q3hrk59x18cia4f3fpdd1ww8rq5fh78rcw")))

(define-public crate-libvips-1.5.0 (c (n "libvips") (v "1.5.0") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "133jrs5bx0cswv6fxvlspi0xv59c727yxc6cdqq51p0dl6jfxi7k")))

(define-public crate-libvips-1.5.1 (c (n "libvips") (v "1.5.1") (d (list (d (n "num-derive") (r "^0.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "13cyznxs0wjjlcsswa6qzhq7dv779dpn8lqnx5sbrj1l2v6w3vdz")))

(define-public crate-libvips-1.6.0 (c (n "libvips") (v "1.6.0") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0vryjg8qdrmz20664dbpibi2car1x8agfyxhslq2jd04qydxig6s")))

(define-public crate-libvips-1.6.1 (c (n "libvips") (v "1.6.1") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "12sqfq3mxg9yjcssfzh9vpcpgxix1l6wf1pwq995nv9vm44myf2q")))

(define-public crate-libvips-1.7.0 (c (n "libvips") (v "1.7.0") (d (list (d (n "num-derive") (r "^0.4") (d #t) (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "0apwyz8dm9ysxmsry2savf3q0nd12k01pm33c1g5ph2s6s9hp29k")))

