(define-module (crates-io li bv libvopono) #:use-module (crates-io))

(define-public crate-libvopono-0.1.0 (c (n "libvopono") (v "0.1.0") (d (list (d (n "crossbeam") (r ">=0.8.0, <0.9.0") (d #t) (k 0)) (d (n "nix") (r ">=0.19.0, <0.20.0") (d #t) (k 0)) (d (n "thiserror") (r ">=1.0.22, <2.0.0") (d #t) (k 0)))) (h "0yw0myglhqs31wpvq1wjcnd3shlvzzd0dh2f4vzyvfd9p88p3mr7")))

