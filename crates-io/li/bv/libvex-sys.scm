(define-module (crates-io li bv libvex-sys) #:use-module (crates-io))

(define-public crate-libvex-sys-0.1.0 (c (n "libvex-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pqkwl8j5gsg3hwd8q56k45g6la3hfwzaav5dwzkcb9202i3pv41")))

(define-public crate-libvex-sys-0.1.1 (c (n "libvex-sys") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vq5p0bhbgbq2hdpckhbzpd1skvxj5wx4bgvblwapqv6pdqsksqa")))

