(define-module (crates-io li bv libvault_rs) #:use-module (crates-io))

(define-public crate-libvault_rs-0.0.1 (c (n "libvault_rs") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "02n25i1f2k4gpfqjn2hjp15vnynq1ljxl5klr2c8hzg8spmhc2pd")))

(define-public crate-libvault_rs-0.0.2 (c (n "libvault_rs") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rwl2cxyfshldbgnxl4z48iklmar1yxxic1nvf5jhq6yqxvgxg2s")))

(define-public crate-libvault_rs-0.0.3 (c (n "libvault_rs") (v "0.0.3") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v5qa8nvpijaa7khp6912i8ny5pvcnj0s9492cqv0a7qm9v1nlzz")))

(define-public crate-libvault_rs-0.0.4 (c (n "libvault_rs") (v "0.0.4") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1dmvh7pw8dd75gqwrzjpqdf0wpzk1y4lcr83n8kbws6m8aqpvykp")))

(define-public crate-libvault_rs-0.0.5 (c (n "libvault_rs") (v "0.0.5") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.144") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)))) (h "1v335k93p9nkrqj07r0lmqjmrig14haxi5g7qf3w8g9q23vsys51")))

