(define-module (crates-io li bv libvex) #:use-module (crates-io))

(define-public crate-libvex-0.1.0 (c (n "libvex") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "vex-sys") (r "^0.3") (d #t) (k 0)))) (h "0w1fgrzivcfn1g40fy36hxdf5a46sywkfy5452rhy3129cyk7sr5")))

