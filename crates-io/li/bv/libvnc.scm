(define-module (crates-io li bv libvnc) #:use-module (crates-io))

(define-public crate-libvnc-0.1.0 (c (n "libvnc") (v "0.1.0") (d (list (d (n "libvnc-sys") (r "^0.1.1") (d #t) (k 0)))) (h "18w6hblwa2awp3cwjd447bi4mfa0s3n0n1is6yapzz6v84hvfg5c")))

(define-public crate-libvnc-0.1.1 (c (n "libvnc") (v "0.1.1") (d (list (d (n "libvnc-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0wi016vzbb7rvpw3jdjz4js30hahvwdvv7bn3w0x58yikmcnpfns")))

(define-public crate-libvnc-0.1.2 (c (n "libvnc") (v "0.1.2") (d (list (d (n "libvnc-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "1h7pfl7lr6n53hy1wgbi5vlgcq5vdbmalbnc46y52ihpsd9iqksr")))

(define-public crate-libvnc-0.1.3 (c (n "libvnc") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.154") (d #t) (k 2)) (d (n "libvnc-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)))) (h "01bml0zl98gz2lcrlqp5xxs2x31y4sfig64vdmgp26pv7qq1ndph") (f (quote (("pkg" "libvnc-sys/pkg") ("default" "pkg"))))))

