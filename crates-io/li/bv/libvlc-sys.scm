(define-module (crates-io li bv libvlc-sys) #:use-module (crates-io))

(define-public crate-libvlc-sys-0.1.0 (c (n "libvlc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0y9qaa0ppxfqly32s11gg68hx0yq5s7zh0anixml7f4ymz63sws2")))

(define-public crate-libvlc-sys-0.2.0 (c (n "libvlc-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jmbnm0y8asdwhkayhf82llw7wrslhippx627q351ma1bbl2m7mi")))

