(define-module (crates-io li bv libvmaf-sys) #:use-module (crates-io))

(define-public crate-libvmaf-sys-0.1.0 (c (n "libvmaf-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.0.0") (d #t) (k 1)))) (h "0ri76jgvpm7qwpbls6c2h3xf71fmq95i9h52wyyycfxynmjwr8w0")))

(define-public crate-libvmaf-sys-0.1.1 (c (n "libvmaf-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.0.0") (d #t) (k 1)))) (h "1fpxwpvw5sjmgg6l0iw3ziln5cxbz60xcl4b5aawc4f9jywivhf0")))

(define-public crate-libvmaf-sys-0.1.2 (c (n "libvmaf-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.0.0") (d #t) (k 1)))) (h "0n78836v2657jvvygdb93yj1g97yaqvjp9qb24pxfa3hbgj27712")))

(define-public crate-libvmaf-sys-0.1.3 (c (n "libvmaf-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.0.0") (d #t) (k 1)))) (h "07rcdmhdm0mr4vjcdp6nc2jg8y3nvl2ngmppq8lxkilg4h9zhb8s")))

(define-public crate-libvmaf-sys-0.1.4 (c (n "libvmaf-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.0.0") (d #t) (k 1)))) (h "0z8lmr1gq9v66jpil076414ssax6vackc6xy17d2vnmmglldrdp1")))

(define-public crate-libvmaf-sys-0.1.5 (c (n "libvmaf-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.0.1") (d #t) (k 1)))) (h "0r2lyjzqqxdgb9myfjmdsjq4dql1pckfqqqc9mvymna4m63lwd38")))

(define-public crate-libvmaf-sys-0.2.0 (c (n "libvmaf-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.0.1") (d #t) (k 1)))) (h "0qlzhc14v51xvjgqp77xhymyi3hr6r7jwfw3drgqqlhzjr0hniv2")))

(define-public crate-libvmaf-sys-0.2.1 (c (n "libvmaf-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.0.1") (d #t) (k 1)))) (h "1ag671ymri33y0rmcy6p5fppn7yw7axgv0w6708i7c4rsshv484a")))

(define-public crate-libvmaf-sys-0.2.2 (c (n "libvmaf-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.0.1") (d #t) (k 1)))) (h "102c722kq642pp4ckf11k4xrsw8r6zcpdbkr9vp7jc98agr33nbr") (l "vmaf")))

(define-public crate-libvmaf-sys-0.3.0 (c (n "libvmaf-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.1.3") (d #t) (k 1)))) (h "04n3kfk5ak89dfrl2ha5q70iws8s2viijvs8qsrd36gbkvix0y0x") (f (quote (("float") ("avx512")))) (l "vmaf")))

(define-public crate-libvmaf-sys-0.3.1 (c (n "libvmaf-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.1.3") (d #t) (k 1)))) (h "0lfcbsaknhs8g37fp2kwk7cqrld9iq4xwnjv3fzalkzdn3syskvd") (f (quote (("float") ("avx512")))) (l "vmaf")))

(define-public crate-libvmaf-sys-0.3.2 (c (n "libvmaf-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.2.1") (d #t) (k 1)))) (h "0dlarwfq6gppsjnpb6q3xznv8pm0x0fj5k7wsp1lpzich45yjkdh") (f (quote (("float") ("avx512")))) (l "vmaf")))

(define-public crate-libvmaf-sys-0.3.3 (c (n "libvmaf-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.2.1") (d #t) (k 1)))) (h "0lw4h8z908j45g8bxk1799pbs5y6n57jdby9b2qg841p5xf263sn") (f (quote (("float") ("avx512")))) (l "vmaf")))

(define-public crate-libvmaf-sys-0.3.4 (c (n "libvmaf-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.2.2") (d #t) (k 1)))) (h "0mk0vkcxzdpdi9b4yb5ilwswkgkz83ciqagbydlapzrbz1kkyxkm") (f (quote (("float") ("avx512")))) (l "vmaf")))

(define-public crate-libvmaf-sys-0.3.5 (c (n "libvmaf-sys") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.2.2") (d #t) (k 1)))) (h "1fbwyimrscy85y7y0jfhgwgvpk0xa7qaxcx3nv4wnw3sw5a1n8g0") (f (quote (("float") ("avx512")))) (l "vmaf")))

(define-public crate-libvmaf-sys-0.3.6 (c (n "libvmaf-sys") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "meson-next") (r "^1.2.2") (d #t) (k 1)))) (h "1zplgbjk5v189n29k9lvrvknskyig1yq1qbfkc3q3blpl5ckayy1") (f (quote (("float") ("avx512")))) (l "vmaf")))

(define-public crate-libvmaf-sys-0.4.0 (c (n "libvmaf-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "meson-next") (r "^1.2.2") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "18cijkq0salf1hi7kpbyz7zj0x8zk3mccpkxw8jik1iwvg5nh2wr") (f (quote (("float" "build") ("avx512" "build")))) (l "vmaf") (s 2) (e (quote (("build" "dep:meson-next"))))))

(define-public crate-libvmaf-sys-0.4.1 (c (n "libvmaf-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "meson-next") (r "^1.2.2") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "10g7n5fr252v2ra6yp3xv5adghnir9ksjl5d76fmmaifwc8ag7yk") (f (quote (("float" "build") ("default" "build") ("avx512" "build")))) (l "vmaf") (s 2) (e (quote (("build" "dep:meson-next"))))))

(define-public crate-libvmaf-sys-0.4.2 (c (n "libvmaf-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "meson-next") (r "^1.2.2") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1avpcahy14nw43dqwrnzgl5xcy46mnsb84cg829ndms9hkp3w169") (f (quote (("static") ("float" "build") ("default" "build") ("avx512" "build")))) (l "vmaf") (s 2) (e (quote (("build" "dep:meson-next"))))))

(define-public crate-libvmaf-sys-0.4.3 (c (n "libvmaf-sys") (v "0.4.3") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "meson-next") (r "^1.2.2") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0lsg417mdrjvh5y3c04ny1xi2bgwp3p0xqc166pka77wd0ryaapj") (f (quote (("static") ("float" "build") ("default" "build") ("avx512" "build")))) (l "vmaf") (s 2) (e (quote (("build" "dep:meson-next"))))))

(define-public crate-libvmaf-sys-0.4.4 (c (n "libvmaf-sys") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "meson-next") (r "^1.2.2") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "19krvw23xv5z66rwg0xd6d0f6ddx1bpvpljikapaasrcyg2kw1cb") (f (quote (("static") ("float" "build") ("default" "build") ("avx512" "build")))) (l "vmaf") (s 2) (e (quote (("build" "dep:meson-next"))))))

