(define-module (crates-io li bv libvpx) #:use-module (crates-io))

(define-public crate-libvpx-0.1.0 (c (n "libvpx") (v "0.1.0") (d (list (d (n "av-codec") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "av-data") (r "^0.1.0") (d #t) (k 0)) (d (n "vpx-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1iaggnhgmazfcy7qglfd296833zyliv92bmw6xxrk6mzyil91lld") (f (quote (("default" "codec-trait") ("codec-trait" "av-codec"))))))

(define-public crate-libvpx-0.1.1 (c (n "libvpx") (v "0.1.1") (d (list (d (n "av-codec") (r "^0.2.2") (o #t) (d #t) (k 0)) (d (n "av-data") (r "^0.3.0") (d #t) (k 0)) (d (n "vpx-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0h3d4yjn75kiz25b7qv66dgpbwcwig16p13qf906rw8fqniwpcm1") (f (quote (("default" "codec-trait") ("codec-trait" "av-codec"))))))

