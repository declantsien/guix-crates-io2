(define-module (crates-io li bv libva-sys) #:use-module (crates-io))

(define-public crate-libva-sys-0.1.0 (c (n "libva-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0wn12al7gyb74mikwjnhja13nh35la7gyl04wd1fi6xc95ag3ri1") (f (quote (("x11") ("wayland") ("drm") ("default" "drm")))) (l "va")))

(define-public crate-libva-sys-0.1.1 (c (n "libva-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1z22h2gdhvdqbmxc9wk4gi54vaw9mrph9wapwn93ls53a47ira0w") (f (quote (("x11") ("wayland") ("drm") ("default" "drm")))) (l "va")))

(define-public crate-libva-sys-0.1.2 (c (n "libva-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1fp9wap92h2kmjga0g4sz44fsy5rvj1wyhkw6l5697mr5jmcbr29") (f (quote (("x11") ("wayland") ("drm") ("default" "drm")))) (l "va")))

