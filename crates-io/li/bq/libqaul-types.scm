(define-module (crates-io li bq libqaul-types) #:use-module (crates-io))

(define-public crate-libqaul-types-0.1.0 (c (n "libqaul-types") (v "0.1.0") (d (list (d (n "alexandria-tags") (r "^0.2") (d #t) (k 0)) (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "bincode") (r "^1.0") (d #t) (k 2)) (d (n "qrpc-sdk") (r "^0.1") (d #t) (k 0)) (d (n "ratman-identity") (r "^0.6.0") (f (quote ("random"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1dvc3biyx4sq5azm6fb10c9dmswj95w4zmvqgfcmk88a2bprp16g")))

