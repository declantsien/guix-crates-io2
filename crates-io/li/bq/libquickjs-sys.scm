(define-module (crates-io li bq libquickjs-sys) #:use-module (crates-io))

(define-public crate-libquickjs-sys-0.1.0-alpha.1 (c (n "libquickjs-sys") (v "0.1.0-alpha.1") (d (list (d (n "bindgen") (r "^0.50.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "1h6xxaf0hs7wck6dhfj1wkzmygk3xi98hgp53ycvrywnyh5rp1ia") (f (quote (("system" "bindgen") ("default" "bundled") ("bundled" "copy_dir"))))))

(define-public crate-libquickjs-sys-0.1.0-alpha.2 (c (n "libquickjs-sys") (v "0.1.0-alpha.2") (d (list (d (n "bindgen") (r "^0.50.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "14cfj3lqgqjcmfvykpg5xxbv0mlmmn5yl6sp72b15w1lpsmvwm3d") (f (quote (("system" "bindgen") ("default" "bundled") ("bundled" "copy_dir"))))))

(define-public crate-libquickjs-sys-0.1.0 (c (n "libquickjs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.50.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "10fwflycxw7fk9vchpxf9rc6i5az2g6ri5k064jk5rig1qyd9476") (f (quote (("system" "bindgen") ("default" "bundled") ("bundled" "copy_dir"))))))

(define-public crate-libquickjs-sys-0.2.0 (c (n "libquickjs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.50.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "0h3j9qa17cgiyv722flhyq3g7i60pk96j9c2z1dhq3fxf8jv0vg2") (f (quote (("system" "bindgen") ("default" "bundled") ("bundled" "copy_dir"))))))

(define-public crate-libquickjs-sys-0.3.0 (c (n "libquickjs-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.50.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "0nfd3axwpbpnbjz9gv2ibavf0c4fqlybi721s9lfw8fjmaag9184") (f (quote (("system" "bindgen") ("patched") ("default" "bundled") ("bundled" "copy_dir"))))))

(define-public crate-libquickjs-sys-0.4.0 (c (n "libquickjs-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.51.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "1cnqfms61rlxszmqb069vvkz9z801niyq7nmcylg7hdf4pz13da4") (f (quote (("system" "bindgen") ("patched") ("default" "bundled") ("bundled" "copy_dir") ("bignum"))))))

(define-public crate-libquickjs-sys-0.5.0 (c (n "libquickjs-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.51.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "05v0qxhxm2b2fqmbqzxk60r38s373i59cfpb8y4jfmm7b4gay1v2") (f (quote (("system" "bindgen") ("patched") ("default" "bundled") ("bundled" "copy_dir"))))))

(define-public crate-libquickjs-sys-0.6.0 (c (n "libquickjs-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.54.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "0ni3j4nyqj7x87bhha12hadnlsba7ih49sms70laj4ibjyz432cr") (f (quote (("system" "bindgen") ("patched" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

(define-public crate-libquickjs-sys-0.7.0 (c (n "libquickjs-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.54.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "1sag1q8j62r92fvfahswnbhr72cyz8cw03i0npnlkg34lpr3mm1q") (f (quote (("system" "bindgen") ("patched" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

(define-public crate-libquickjs-sys-0.8.0 (c (n "libquickjs-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.54.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "1fpkgmlyaj9jx3qmfrznamsrvlplb2z3nwvgxkxwvsw3x8ma1sfy") (f (quote (("system" "bindgen") ("patched" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

(define-public crate-libquickjs-sys-0.9.0 (c (n "libquickjs-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.54.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "15pf7h6afms69bryamiin87l2n7yp27l5gcm0ap7a6qppplj82rz") (f (quote (("system" "bindgen") ("patched" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

(define-public crate-libquickjs-sys-0.10.0 (c (n "libquickjs-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.57.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "11vx7grj9m9snpyksq6ghdxb4ad7j057mgmf1579n3dss9qx6pls") (f (quote (("system" "bindgen") ("patched" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

