(define-module (crates-io li bq libqoi) #:use-module (crates-io))

(define-public crate-libqoi-0.1.0 (c (n "libqoi") (v "0.1.0") (h "1n5mpcx8lak0whjspadnh4dxzxxam34gis6d7xgw5ykp8r0lmhcw")))

(define-public crate-libqoi-0.1.1 (c (n "libqoi") (v "0.1.1") (h "158nlg1xmgyisz8yhwmd6gvzsgl7bngcq28rl8z4hcazxm1y3mdn")))

(define-public crate-libqoi-0.2.0 (c (n "libqoi") (v "0.2.0") (h "0v8mwxjdp925qb84kayjlb0b64g1pq0q8zja5wz5q1g41favz88c") (y #t)))

(define-public crate-libqoi-0.2.1 (c (n "libqoi") (v "0.2.1") (h "03w0337sc110mc55iqw8lfy19fzlrk59kf2ahw5j8j4vv51imrs8")))

(define-public crate-libqoi-0.3.1 (c (n "libqoi") (v "0.3.1") (h "1316vlyd0jjhin42hrzyiqfafpzbf9h6s4j4bbghb26y7g3wjxsx")))

(define-public crate-libqoi-0.3.2 (c (n "libqoi") (v "0.3.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0xdkvm41arvvjck2gr3fs63h0qrxpjglx359mrjrv5qrvk65lcri")))

