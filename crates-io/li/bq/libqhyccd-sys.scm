(define-module (crates-io li bq libqhyccd-sys) #:use-module (crates-io))

(define-public crate-libqhyccd-sys-0.1.0 (c (n "libqhyccd-sys") (v "0.1.0") (h "1kixbabpgcq007fjdhqif1fsaw1n7z478g83m1gyk0bdigwdpyyn") (f (quote (("vendored")))) (l "qhyccd") (r "1.64.0")))

(define-public crate-libqhyccd-sys-0.1.1 (c (n "libqhyccd-sys") (v "0.1.1") (h "1cxhkpaa6aibsqy0n42mlwf5p7ch986dcyfz5kw3jddr49a5qndw") (f (quote (("vendored")))) (l "qhyccd") (r "1.64.0")))

(define-public crate-libqhyccd-sys-0.1.2 (c (n "libqhyccd-sys") (v "0.1.2") (h "0bha309g0hwcnn35d2amr1lmgqh1rq34zs6gwzyls2y4hi2k12fh") (f (quote (("vendored")))) (l "qhyccd") (r "1.64.0")))

