(define-module (crates-io li bq libquickjspp-sys) #:use-module (crates-io))

(define-public crate-libquickjspp-sys-0.1.0 (c (n "libquickjspp-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "0h315bbmb4g3mzilpffalrbrc3p4dlsi2piqgik1rvky36m5l3xn") (f (quote (("system" "bindgen") ("patched" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

(define-public crate-libquickjspp-sys-0.6.0-rc.0 (c (n "libquickjspp-sys") (v "0.6.0-rc.0") (d (list (d (n "bindgen") (r "^0.64.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "0mjy0cyyh8mllwfrvir9xhv5fg2z6hp8srq4k55h0wvad2w7lsrb") (f (quote (("system" "bindgen") ("patched" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

(define-public crate-libquickjspp-sys-0.6.0-rc.1 (c (n "libquickjspp-sys") (v "0.6.0-rc.1") (d (list (d (n "bindgen") (r "^0.64.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "04wnc9gv7if9bqwv8vhqdf4sspcvq0jyh7zx36rj5vfy9v37kkpy") (f (quote (("system" "bindgen") ("patched" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

