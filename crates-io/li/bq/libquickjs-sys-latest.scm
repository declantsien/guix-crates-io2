(define-module (crates-io li bq libquickjs-sys-latest) #:use-module (crates-io))

(define-public crate-libquickjs-sys-latest-0.10.0 (c (n "libquickjs-sys-latest") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.61.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "0573wjfnax8kvjhpwjj6ic5ml7f2cmwxc9p93dfyrgvg89qmwyl7") (f (quote (("system" "bindgen") ("patched" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir")))) (y #t)))

(define-public crate-libquickjs-sys-latest-0.10.1 (c (n "libquickjs-sys-latest") (v "0.10.1") (d (list (d (n "bindgen") (r "^0.61.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "131bi5zc9gnk47vyi9yzzr64x6p56zr7zmarckfr1znb3ylbnja7") (f (quote (("system" "bindgen") ("patched" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

