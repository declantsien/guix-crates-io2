(define-module (crates-io li bq libquickjs-dtp-sys) #:use-module (crates-io))

(define-public crate-libquickjs-dtp-sys-0.9.0 (c (n "libquickjs-dtp-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.63.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "1b2z9j95xjv5zf34nfylrw25anpd2dv5h1ya1kb36ifaa01axx45") (f (quote (("system" "bindgen") ("patch-dateparser" "bundled") ("patch-bigint" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

(define-public crate-libquickjs-dtp-sys-0.10.0 (c (n "libquickjs-dtp-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.69.0") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (o #t) (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (o #t) (d #t) (k 1)))) (h "15gva3l7j4zmwc5papbn6gd6hwl9j1lmb9r93gpdj0gwkjyxfsdq") (f (quote (("system" "bindgen") ("patch-dateparser" "bundled") ("patch-bigint" "bundled") ("default" "bundled") ("bundled" "cc" "copy_dir"))))))

