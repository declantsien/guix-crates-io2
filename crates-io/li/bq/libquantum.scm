(define-module (crates-io li bq libquantum) #:use-module (crates-io))

(define-public crate-libquantum-0.1.0 (c (n "libquantum") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.20") (d #t) (k 1)))) (h "1ff5kk1w00c7sjplq6ngqcx6m3lgns87y907a1qqshc8qb7hkqr5") (y #t)))

(define-public crate-libquantum-0.1.1 (c (n "libquantum") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.20") (d #t) (k 1)))) (h "0765ig0mxksc1l2cdhbnw41dhy9dfm8pzks2c0365g901bm0cbaz")))

(define-public crate-libquantum-0.1.2 (c (n "libquantum") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.20") (d #t) (k 1)) (d (n "glob") (r "^0.2") (d #t) (k 1)))) (h "1ykxq906grpblrmjn7pnjbql74rfviz893gcmh1aa8qiys79f6h2")))

