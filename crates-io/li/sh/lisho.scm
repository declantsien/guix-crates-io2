(define-module (crates-io li sh lisho) #:use-module (crates-io))

(define-public crate-lisho-0.1.0 (c (n "lisho") (v "0.1.0") (h "13zrqsswm0xgzb76ryx11nl48yk2vpy6l4hfh78g0bqb6jvdz49i")))

(define-public crate-lisho-0.1.1 (c (n "lisho") (v "0.1.1") (h "10rk48rjayh4jh4sh3y5ad7jm8p7zwzn5y918rnv1wbbwxl7yna3")))

(define-public crate-lisho-0.1.4 (c (n "lisho") (v "0.1.4") (h "1ahj1n1dqdw89lw5y6dhjx749xbxbyrddq1rfpjzfr22n1624wrq")))

(define-public crate-lisho-0.1.5 (c (n "lisho") (v "0.1.5") (h "1p443fvh8pv1arsx6pcl1aqkv8j3ybmjl8h8dha1dhwadc24a4ib")))

