(define-module (crates-io li sh lish) #:use-module (crates-io))

(define-public crate-lish-0.1.0 (c (n "lish") (v "0.1.0") (d (list (d (n "liso") (r "^0.8") (d #t) (k 0)) (d (n "shellish_parse") (r "^2.0") (d #t) (k 0)) (d (n "nix") (r "^0.25") (f (quote ("signal"))) (t "cfg(unix)") (k 0)))) (h "0gnws7r6li3ax8inz81fxyzpy177b5x473xwgbzaqjv3kzngcjfq") (f (quote (("no-unix") ("default"))))))

