(define-module (crates-io li dw lidwatch) #:use-module (crates-io))

(define-public crate-lidwatch-0.1.0 (c (n "lidwatch") (v "0.1.0") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "1gl4pkyqpxwjf4wyw8imnd8lacwjf03r6i2fpw39zxncmbfmd4ck") (y #t)))

(define-public crate-lidwatch-0.1.1 (c (n "lidwatch") (v "0.1.1") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "1qjcjs0bf4vk095ifj7kqsbpnzrd1ls44y7vjwwlysg686aaw4gj")))

(define-public crate-lidwatch-0.1.2 (c (n "lidwatch") (v "0.1.2") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "0kncbh3phq4vf5lspcrl1549sqrffg824b4rb3kj55r7wyixc8wq")))

(define-public crate-lidwatch-0.1.3 (c (n "lidwatch") (v "0.1.3") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "1xgi0gkjnk9311gabrkdhl8ic3n86igsilm97zjbx567xv24db7g")))

(define-public crate-lidwatch-0.1.4 (c (n "lidwatch") (v "0.1.4") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "0ldg9qnangmy3598cxx4v2gch8v8x9mjllr9067b8isc7hdmszki")))

(define-public crate-lidwatch-0.1.5 (c (n "lidwatch") (v "0.1.5") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)))) (h "1ighjzlsl4fnzinxk7mnbbl3svgl6fv9i15mapazi7swrapfwn6z")))

(define-public crate-lidwatch-0.1.6 (c (n "lidwatch") (v "0.1.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "evdev") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1ky95nm2v1hc1z8yrdjwbmlisrvcylyd8c1ah22ywa9ph8r5h5h5")))

(define-public crate-lidwatch-0.1.7 (c (n "lidwatch") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "evdev") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1m40b9rs2q8p53djrhyxmx830dvl4wr95ia3f9k9zyri1asd4fqi")))

(define-public crate-lidwatch-1.0.0 (c (n "lidwatch") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "evdev") (r "^0.12") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "032ka2gllvf2smdays6rx4zwr18djgcsiigzfjqzag73v4xrv37a")))

(define-public crate-lidwatch-1.0.1 (c (n "lidwatch") (v "1.0.1") (d (list (d (n "evdev") (r "^0.12") (d #t) (k 0)) (d (n "notify-rust") (r "^4.11") (d #t) (k 0)))) (h "0dwz9cbsx2bjhfwbb61p35b0ywywqq0zxk5dd9g4cm2calqigpg0")))

