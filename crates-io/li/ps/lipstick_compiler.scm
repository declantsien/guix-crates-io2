(define-module (crates-io li ps lipstick_compiler) #:use-module (crates-io))

(define-public crate-lipstick_compiler-0.4.0 (c (n "lipstick_compiler") (v "0.4.0") (d (list (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.32") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "string-interner") (r "^0.12.2") (d #t) (k 0)) (d (n "syn") (r "^1.0.82") (f (quote ("full" "parsing" "printing" "visit"))) (k 0)))) (h "1wvpa5r4amw6app9j7hbasa9plb6kkmsf7aic5a16c0kan4y5lhj") (f (quote (("dev" "syn/extra-traits"))))))

