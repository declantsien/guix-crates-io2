(define-module (crates-io li ps lipsum) #:use-module (crates-io))

(define-public crate-lipsum-0.1.0 (c (n "lipsum") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "012wy3fv9xjl0xlmz98rnx9laps3zvq4kaf8ialgak5m5gpa4lwn")))

(define-public crate-lipsum-0.2.0 (c (n "lipsum") (v "0.2.0") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "19zcdgmvjmmi9vb27f9p5zhlmyq9svaws6z82xmv6zsq5c72cilg")))

(define-public crate-lipsum-0.3.0 (c (n "lipsum") (v "0.3.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1hal592x6g55bavrf4h723xcfp35yn4n1imbhjv00cgs7vrxyww1")))

(define-public crate-lipsum-0.4.0 (c (n "lipsum") (v "0.4.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.3") (d #t) (k 2)))) (h "17vc49g8c4g17smyvwjp15jqf1d7wpjyikz7kgjbjw28r0vk5jxw")))

(define-public crate-lipsum-0.5.0 (c (n "lipsum") (v "0.5.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "version-sync") (r "^0.5") (d #t) (k 2)))) (h "1asxgwqxkw9y9fz4ckyr51417z4vcfi64qp26kyqs5sf6sw38vw2")))

(define-public crate-lipsum-0.6.0 (c (n "lipsum") (v "0.6.0") (d (list (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "rand_xorshift") (r "^0.1") (d #t) (k 2)) (d (n "version-sync") (r "^0.6") (d #t) (k 2)))) (h "0nlxkz8zjxqmbrxqasr36a5fqn2n33cxy11w0x0a0b6mcx04dr2q")))

(define-public crate-lipsum-0.7.0 (c (n "lipsum") (v "0.7.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "174b9z087h36782nsndspg7bq8y9j2wjadqz2js1mdwq16wgrg97")))

(define-public crate-lipsum-0.8.0 (c (n "lipsum") (v "0.8.0") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0sn5k0hgx099x2qdx0xlx8a5b74sfc55qnbyrhnh72baqxqp5vj2")))

(define-public crate-lipsum-0.8.1 (c (n "lipsum") (v "0.8.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1iq4n3bdzayv7dcq9shmim2xkr4n9awypvqigz2f3b7qlr0kzj49")))

(define-public crate-lipsum-0.8.2 (c (n "lipsum") (v "0.8.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "1x39vkjyr23bgm3s9m9rl56q1gj0rvxqjsb6hr2f8dzky531hid8")))

(define-public crate-lipsum-0.9.0 (c (n "lipsum") (v "0.9.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0hd5kjd5y3dy7cpfhbjkjp8p2fld7hh7rqmcb5xfcvxdsbr9wplw")))

(define-public crate-lipsum-0.9.1 (c (n "lipsum") (v "0.9.1") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("alloc"))) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "rand_chacha") (r "^0.3.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0r40mf2cwh4fp9pdfcc1n8hjxw05w7galjvb1z23r5pq38jn0s33") (r "1.56")))

