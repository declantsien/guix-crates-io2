(define-module (crates-io li ps lipsum-cn) #:use-module (crates-io))

(define-public crate-lipsum-cn-1.0.0 (c (n "lipsum-cn") (v "1.0.0") (d (list (d (n "dynfmt") (r "^0.1.5") (f (quote ("curly"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "18xfrngrsjrmn2f3xcxcfw5946jh4sp37zw3gklwngicwk0hgg7p")))

(define-public crate-lipsum-cn-1.0.1 (c (n "lipsum-cn") (v "1.0.1") (d (list (d (n "dynfmt") (r "^0.1.5") (f (quote ("curly"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (f (quote ("small_rng"))) (d #t) (k 0)))) (h "01sbsh0q0bv3k8prv4ms3lkg7ibl0a6jc0ifnl08pylv70kdh14g")))

