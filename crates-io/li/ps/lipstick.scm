(define-module (crates-io li ps lipstick) #:use-module (crates-io))

(define-public crate-lipstick-0.1.0 (c (n "lipstick") (v "0.1.0") (d (list (d (n "lalrpop") (r "^0.19.5") (d #t) (k 1)) (d (n "lalrpop-util") (r "^0.19.5") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0r7c5javl6a79aycf1hbwrqw1yjfd2j1l088jxv1r9vsz63xpsjy")))

(define-public crate-lipstick-0.2.0 (c (n "lipstick") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "codespan-reporting") (r "^0.11.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.26") (f (quote ("span-locations"))) (d #t) (k 0)) (d (n "syn") (r "^1.0.67") (f (quote ("full" "parsing" "printing"))) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "0fcp2kk6yfb6dgw4pj408nhb6iv5n8hijzmml19pkjfajmswfx5d")))

(define-public crate-lipstick-0.4.0 (c (n "lipstick") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "compiler") (r "^0.4.0") (d #t) (k 0) (p "lipstick_compiler")))) (h "0jvb9shkmd39xl5yma360ygqzz5rg7fp6vplwl297f11p7vhhjam") (f (quote (("dev" "compiler/dev"))))))

