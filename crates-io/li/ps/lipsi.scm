(define-module (crates-io li ps lipsi) #:use-module (crates-io))

(define-public crate-lipsi-0.1.0 (c (n "lipsi") (v "0.1.0") (h "0yxqylnwx5qzxw7g8gsgkixb9z5b9p27b9qd2dv2hjpmssfhr0d9")))

(define-public crate-lipsi-1.0.0 (c (n "lipsi") (v "1.0.0") (h "10g5ncmdyzf841wznlkymg8bcd2n971g77f0m66vxawd9d8738nl")))

(define-public crate-lipsi-1.0.1 (c (n "lipsi") (v "1.0.1") (h "1vq369fl5knd5lcacrjndl5y839q0v7y7y8rcfh566kiiy1yk216")))

(define-public crate-lipsi-2.0.0 (c (n "lipsi") (v "2.0.0") (h "0z5xrvgzz4lahby8v4aicxwlbf4s6kc6y1dzxvmwz1swj9pi1brk")))

(define-public crate-lipsi-2.0.1 (c (n "lipsi") (v "2.0.1") (h "1fkijm100nhph1yb8bkin6502ljdvknvz2xqi9ag5cdr4754qicw")))

