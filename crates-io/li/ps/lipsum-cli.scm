(define-module (crates-io li ps lipsum-cli) #:use-module (crates-io))

(define-public crate-lipsum-cli-0.1.0 (c (n "lipsum-cli") (v "0.1.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lipsum") (r "^0.8") (d #t) (k 0)))) (h "1r8k2ygv3m20rl1pjrkhcip2l3s37c6w70sg1w9ky4y3i4m1vlqw") (y #t)))

(define-public crate-lipsum-cli-0.2.0 (c (n "lipsum-cli") (v "0.2.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lipsum") (r "^0.8") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0z2f9gb70iy2k9cm9gp5ncvshjqspgnrrj1f8y5g1z8vm61x8bsv")))

(define-public crate-lipsum-cli-0.2.1 (c (n "lipsum-cli") (v "0.2.1") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^3.1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lipsum") (r "^0.8") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0rgp4sp7phpp2qwvhab67pnad6lfpx5fzzp8rrsz9yzw3qxvv4fk")))

(define-public crate-lipsum-cli-0.2.2 (c (n "lipsum-cli") (v "0.2.2") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lipsum") (r "^0.8") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0wlr35gb41dkz9l3pm56h975gcaiv65azi0dlnlfi2x92wqi2lzl")))

(define-public crate-lipsum-cli-0.3.0 (c (n "lipsum-cli") (v "0.3.0") (d (list (d (n "atty") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "^4.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lipsum") (r "^0.8") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0xd3sfwmr8pxjvz10m0gjmc9xc52q5irbjh2ca81v10yvh75y43n")))

