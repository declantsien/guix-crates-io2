(define-module (crates-io li -a li-async-h1) #:use-module (crates-io))

(define-public crate-li-async-h1-2.3.3 (c (n "li-async-h1") (v "2.3.3") (d (list (d (n "async-channel") (r "^1.5.1") (d #t) (k 0)) (d (n "async-dup") (r "^1.2.2") (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (d #t) (k 0)) (d (n "async-std") (r "^1.7.0") (f (quote ("attributes"))) (d #t) (k 2)) (d (n "futures-core") (r "^0.3.8") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "li-http-types") (r "^2.12.0") (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pin-project") (r "^1.0.2") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)))) (h "0gvpggrpkmvs5rjmwpn52mkq1dhh4inl33a55m7v5r8cp36ypypv")))

