(define-module (crates-io li ff liff) #:use-module (crates-io))

(define-public crate-liff-0.1.0 (c (n "liff") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "levenshtein-diff") (r "^0.2.2") (d #t) (k 2)) (d (n "snap") (r "^1") (d #t) (k 0)) (d (n "uuid-readable-rs") (r "^0.1.5") (d #t) (k 2)))) (h "1wy9rmjgi86cjgw18rhp43a2p7liky3vscb6akda1n181zsnvb1l")))

(define-public crate-liff-0.1.1 (c (n "liff") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "levenshtein-diff") (r "^0.2.2") (d #t) (k 2)) (d (n "snap") (r "^1") (d #t) (k 0)) (d (n "uuid-readable-rs") (r "^0.1.5") (d #t) (k 2)))) (h "0vxlxaqmixpq150k1kigkld0x9yrp9z6ygbsxh5ww9xmgn47bh92")))

