(define-module (crates-io li bc libc-extra) #:use-module (crates-io))

(define-public crate-libc-extra-0.0.1 (c (n "libc-extra") (v "0.0.1") (h "1g1db6070nf2m6wrj3yqrk6kzamb8l9pr67wl6ccfjlfccyfbm7m")))

(define-public crate-libc-extra-0.0.2 (c (n "libc-extra") (v "0.0.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0vdlchc8hmvdbx8iy4jblg7y9bz980yb02fvbhk9z35q65ni99ix")))

(define-public crate-libc-extra-0.0.4 (c (n "libc-extra") (v "0.0.4") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1qb17j9bawnxkpywnj6zh945rk3cmpvix33km6kdb82dccx4l66c")))

(define-public crate-libc-extra-0.0.5 (c (n "libc-extra") (v "0.0.5") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bjgsmqa195kxjq50qfl72a1fv9x5h9af9shwmajyqxvz8f0mj1x")))

(define-public crate-libc-extra-0.0.6 (c (n "libc-extra") (v "0.0.6") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kp0qbfcakg45ysgm3x5vsafb39jy36xklzm46v1pnw2rgnvn4m8")))

(define-public crate-libc-extra-0.0.7 (c (n "libc-extra") (v "0.0.7") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jk8dlck9k792h78vlzmjz2qz0d0ali5pijz930d8xjxm0hhgwqp")))

(define-public crate-libc-extra-0.0.8 (c (n "libc-extra") (v "0.0.8") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m4jzcwjaqlk0da9mxajv42x89vb0msc5p38lacr43d2j1j3cv8y")))

(define-public crate-libc-extra-0.0.9 (c (n "libc-extra") (v "0.0.9") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wc0c72cz44j8vg6gkr1bcsa0vc69rpbzqabgs5y4mnyi24vjrbg")))

(define-public crate-libc-extra-0.0.10 (c (n "libc-extra") (v "0.0.10") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0zii3130bq37r5gkbmbc7ing1481pq24p3nb28rhbrvm8shdjc8i")))

(define-public crate-libc-extra-0.0.11 (c (n "libc-extra") (v "0.0.11") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19fkncmg3gcsrp4bwn09hf7z19laj8wpx6qzc67ir8sxm5f3622d")))

(define-public crate-libc-extra-0.0.12 (c (n "libc-extra") (v "0.0.12") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "018djlxki9b4cqm4xyr9wkyhb0nsv59mwgvn6wvh3vncl5kgfqfd")))

(define-public crate-libc-extra-0.0.13 (c (n "libc-extra") (v "0.0.13") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lxkbdaqbmc3918s6gx0qxllifi0yg95ixg4p654l8vijjvc8afm")))

(define-public crate-libc-extra-0.0.14 (c (n "libc-extra") (v "0.0.14") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "const-cstr-fork") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h8n1a2nvxy9ivzj7zh5zhaf6xwcanka83wps9gprdmrdvr55f44")))

(define-public crate-libc-extra-0.0.15 (c (n "libc-extra") (v "0.0.15") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "const-cstr-fork") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g7zcmy47yqvzz5rw953i63zm5ka8h84451z8f5xx1v20p7nda5f")))

(define-public crate-libc-extra-0.0.16 (c (n "libc-extra") (v "0.0.16") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "const-cstr-fork") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cwz0zli8sxj2qqdp1vflh8qm0fqmbm8azmpi33y55bz0ji7ykvy")))

(define-public crate-libc-extra-0.1.0 (c (n "libc-extra") (v "0.1.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "const-cstr-fork") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18bwljzawdf74kp64l9myl3357lqfgrd5flr9iz5h351zvdx1sg3")))

(define-public crate-libc-extra-0.2.0 (c (n "libc-extra") (v "0.2.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "const-cstr-fork") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gngx243x74fp587ha73z418wdxa23353agf2y444r1l75mwp3in")))

(define-public crate-libc-extra-0.2.1 (c (n "libc-extra") (v "0.2.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "const-cstr-fork") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cv0cg5cw2w3sx29301pahjw32kzbjf5j7vhv9azmlm7ag92kmcp")))

(define-public crate-libc-extra-0.3.0 (c (n "libc-extra") (v "0.3.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "const-cstr-fork") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l5pjr014yqx24k4qz64mwwrvl3642wcgmngfbi8wl5i865acn5h")))

(define-public crate-libc-extra-0.3.1 (c (n "libc-extra") (v "0.3.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "const-cstr-fork") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ha4gs2vd16gx55xqxp3ppyvx38jk7162878y4ahrxj138i9bh1d")))

(define-public crate-libc-extra-0.3.2 (c (n "libc-extra") (v "0.3.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "const-cstr-fork") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ay5n4y0rh04sdanlds8ybh5j9l6gcc0ii1z7h1jzj1rgwcanppi")))

