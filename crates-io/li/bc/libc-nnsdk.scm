(define-module (crates-io li bc libc-nnsdk) #:use-module (crates-io))

(define-public crate-libc-nnsdk-0.0.1 (c (n "libc-nnsdk") (v "0.0.1") (d (list (d (n "rustc-std-workspace-core") (r "^1") (o #t) (d #t) (k 0)))) (h "198xgqbgw3y4jychmilp84fsqp0yjh6rcmzz3bmkqalsbl5gmj64") (f (quote (("rustc-dep-of-std" "rustc-std-workspace-core"))))))

(define-public crate-libc-nnsdk-0.2.0 (c (n "libc-nnsdk") (v "0.2.0") (d (list (d (n "rustc-std-workspace-core") (r "^1") (o #t) (d #t) (k 0)))) (h "0rzqchx4pc0c7rk16052k4dkmyd7qphsz5gfvxshmqfyyszy88v8") (f (quote (("rustc-dep-of-std" "rustc-std-workspace-core"))))))

(define-public crate-libc-nnsdk-0.3.0 (c (n "libc-nnsdk") (v "0.3.0") (d (list (d (n "rustc-std-workspace-core") (r "^1") (o #t) (d #t) (k 0)))) (h "0s4vgvcjfajc38mw38q0wynmyg60vvllrc7yrlppfqg4d532w44r") (f (quote (("rustc-dep-of-std" "rustc-std-workspace-core"))))))

