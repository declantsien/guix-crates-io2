(define-module (crates-io li bc libc_alloc) #:use-module (crates-io))

(define-public crate-libc_alloc-1.0.0 (c (n "libc_alloc") (v "1.0.0") (d (list (d (n "libc") (r "^0.2") (k 0)))) (h "0ihxc087nrm0yy24185rp4fq2a37k35g1mxsj7n4k1qlk3asvkzs")))

(define-public crate-libc_alloc-1.0.1 (c (n "libc_alloc") (v "1.0.1") (h "0bbzsmxk02ggfq5gj8lmg9sgpv1kf42piq1lcg8by50fgh7a0992")))

(define-public crate-libc_alloc-1.0.2 (c (n "libc_alloc") (v "1.0.2") (h "01a36gacl7in3vbainkf8wx40715ndx9npdaraghhcz7ggscjg6s")))

(define-public crate-libc_alloc-1.0.3 (c (n "libc_alloc") (v "1.0.3") (h "1d6sg6x9gl5c6z8f8ina7dywsm4rjrzxfqkslvsafii7b92n47fv")))

(define-public crate-libc_alloc-1.0.4 (c (n "libc_alloc") (v "1.0.4") (h "1sk9pxqk9g3wc5nzqcqppmac99g5i2yx5w1hgr8di43dnr4062ba")))

(define-public crate-libc_alloc-1.0.5 (c (n "libc_alloc") (v "1.0.5") (h "1crzalmj1l6hljsblkj57qqhiijax7d6aigvxz7yi7zli2pflkq0")))

(define-public crate-libc_alloc-1.0.6 (c (n "libc_alloc") (v "1.0.6") (h "0kinj1k052m18p1l6zwrg5c66z2yaqyp9cczgwl270blic1mr0ra") (f (quote (("global"))))))

(define-public crate-libc_alloc-1.0.7 (c (n "libc_alloc") (v "1.0.7") (h "12h9xpma6cll834slkhngisdna2cjs87bpni861nk6dw50lji0bm") (f (quote (("global"))))))

