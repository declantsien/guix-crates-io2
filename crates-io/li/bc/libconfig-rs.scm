(define-module (crates-io li bc libconfig-rs) #:use-module (crates-io))

(define-public crate-libconfig-rs-1.0.0 (c (n "libconfig-rs") (v "1.0.0") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "18ilxmb8bphqvc5mrsqbw1xwpfrqmlhr4rmbk1cn75b619kkbrs3") (y #t)))

(define-public crate-libconfig-rs-1.0.1 (c (n "libconfig-rs") (v "1.0.1") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "085bj2ny5dqrl8bdrgj9wms84sg41kigsydzd1dhjbcz9n15zk12") (y #t)))

(define-public crate-libconfig-rs-1.0.2 (c (n "libconfig-rs") (v "1.0.2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1xzy5vm7s3wrf1yj1pf22bx1qabk1r8dn946yyyf3611ff9pkyv9")))

(define-public crate-libconfig-rs-1.0.3 (c (n "libconfig-rs") (v "1.0.3") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0kpsybp7ck4m0xnwpg1hj7z3c8853x81pfzdzbj48vzf5gay1sjf")))

(define-public crate-libconfig-rs-1.0.4 (c (n "libconfig-rs") (v "1.0.4") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1qy9mgx6wdl9y4gldw9bgrcppz7lbi6895rcjsbwp0k0q6gsdw3h")))

(define-public crate-libconfig-rs-1.0.5 (c (n "libconfig-rs") (v "1.0.5") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "06qm2f315la2l5zi74np1ksi1ibn96g1d2zn1sqrwh00m9ayfac4")))

(define-public crate-libconfig-rs-2.0.0 (c (n "libconfig-rs") (v "2.0.0") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0nlmk94cbbmqvw67l2pnkyvcvdkdhlm7prkad457mxy3s9wapwlb")))

(define-public crate-libconfig-rs-2.0.1 (c (n "libconfig-rs") (v "2.0.1") (d (list (d (n "indexmap") (r "^1") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "16sxsxffwd68xsgbnfvhvb21qcmmxqgd5hqbccfqy21dayrv5yy8")))

(define-public crate-libconfig-rs-3.0.0 (c (n "libconfig-rs") (v "3.0.0") (d (list (d (n "indexmap") (r "^2") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0awxymwg9zd18ll7g6wq2zhj24g2h4jjd7rj21ihnikbhfn69y46")))

