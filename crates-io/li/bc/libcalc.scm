(define-module (crates-io li bc libcalc) #:use-module (crates-io))

(define-public crate-libcalc-0.0.0 (c (n "libcalc") (v "0.0.0") (h "1r801lnn3xjalkvmba0ivdw93j09v28pwzy6gbdsazmmqjk3xaws")))

(define-public crate-libcalc-0.0.1 (c (n "libcalc") (v "0.0.1") (h "1kk1zh45y8x1cdgq4b7wn8gjn2pr0hdb7bnid3agzlcgslry1z15")))

