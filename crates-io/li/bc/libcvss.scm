(define-module (crates-io li bc libcvss) #:use-module (crates-io))

(define-public crate-libcvss-1.0.0-rc.2 (c (n "libcvss") (v "1.0.0-rc.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0zmpp55wvvwy9ck1c2ma3rf2nj8607w1mdghaf1l9ijhx7zdhb5a") (f (quote (("fail-on-warnings"))))))

(define-public crate-libcvss-1.0.0-rc.3 (c (n "libcvss") (v "1.0.0-rc.3") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "19pmk58if4xk1zkwilazljw24dpk2zjca8aby6vbz9qr4y0cnv2k") (f (quote (("fail-on-warnings"))))))

(define-public crate-libcvss-1.0.0 (c (n "libcvss") (v "1.0.0") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gaykm2jabg3acpqdijcrl10ppj5h0vbq48p7fsvfi556yam1ccb") (f (quote (("fail-on-warnings"))))))

(define-public crate-libcvss-1.0.1-rc.1 (c (n "libcvss") (v "1.0.1-rc.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1x0lacr6r7gjsbalq9acwiz38ngp2w5wrsnqbsa0l3mm3iv9y2nl") (f (quote (("fail-on-warnings"))))))

(define-public crate-libcvss-1.0.1-rc.2 (c (n "libcvss") (v "1.0.1-rc.2") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0p453i1z83aqgbxhlzqal4gvy3z3c854ziz4imhakxd8pr2v49bs") (f (quote (("fail-on-warnings"))))))

(define-public crate-libcvss-1.0.1 (c (n "libcvss") (v "1.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fnhyzxprwskrhp5h6v8pmb1dj6mdw3pjfiivkbizrs9jbadqazf") (f (quote (("fail-on-warnings"))))))

