(define-module (crates-io li bc libcrio) #:use-module (crates-io))

(define-public crate-libcrio-0.1.0 (c (n "libcrio") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1cybb9xz6jly0y7i96al3f6j08ydllw7lk8hkavxdvb0fdcqbm68") (y #t)))

(define-public crate-libcrio-0.1.1 (c (n "libcrio") (v "0.1.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1z7x1b41jfg13254gq644wkhy7if58hvnriyc06499d9hjhvkl2l")))

(define-public crate-libcrio-0.1.2 (c (n "libcrio") (v "0.1.2") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0v104wffs79bd663j90xif98nrdg4jn5kprfa7nlk3p1kb0j2k9w")))

(define-public crate-libcrio-0.1.3 (c (n "libcrio") (v "0.1.3") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0mfw77v7zshiabsbqdhj4qc2pwr9ppwbyr0c1ijsxyxwmgqi9bnd")))

(define-public crate-libcrio-0.1.4 (c (n "libcrio") (v "0.1.4") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1cvdlq91zmjz4h2z4kqc9gyf7zdc27pdqmg14xb91frnxwmygxk5")))

(define-public crate-libcrio-0.1.5 (c (n "libcrio") (v "0.1.5") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1lbp24gvb1p941gnv64p87xlac68pv9zncpcskv304v1afip4f70")))

(define-public crate-libcrio-0.2.0 (c (n "libcrio") (v "0.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)))) (h "02vfz6jg0g8sac3wq5qk6jkp832ax1nlaylj642cd6f86zahg4n3")))

(define-public crate-libcrio-1.0.0 (c (n "libcrio") (v "1.0.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)))) (h "0yhg9s2syy8693lgh78nnqr19gnnlhr4nilv26fhbvypa1p09dxg")))

(define-public crate-libcrio-1.1.0 (c (n "libcrio") (v "1.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)))) (h "0w5cnyzr7af9am4zw2v6hws0fwj02inpcwi57wjkfa0jqvb7viwq")))

(define-public crate-libcrio-1.2.0 (c (n "libcrio") (v "1.2.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)))) (h "1nyisx3bva2q6x3aqcj1shqcmhp4isklgmkdni5i03ikbzhnk3ci")))

(define-public crate-libcrio-1.3.0 (c (n "libcrio") (v "1.3.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)))) (h "11wr7f21gw0r3qprkg7grrq7blmy8yv8n1f1arhq0nlrkybf21hf")))

(define-public crate-libcrio-1.3.1 (c (n "libcrio") (v "1.3.1") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)))) (h "0blra4vg0x3s0as4i2yzd9jjlbnrd0rp390azn1q08p2v3k5i0v3")))

(define-public crate-libcrio-1.4.0 (c (n "libcrio") (v "1.4.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)))) (h "19ps2mdn19qzghgfrkhcs8987iy4zjsgskzgw9w4b15iyfhynrhd")))

(define-public crate-libcrio-2.0.0 (c (n "libcrio") (v "2.0.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)))) (h "0pmqwajf8fqm427ag7z6flj7hmp0kd6w5lkdcq0ng0ffmshwd0ad")))

(define-public crate-libcrio-2.1.0 (c (n "libcrio") (v "2.1.0") (d (list (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "serde") (r "^1.0.134") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.76") (d #t) (k 0)))) (h "1hb1km1ywrhal1vb3g04kmaq5py0yzw1hkbm2r87xr6lj34lhqpq")))

