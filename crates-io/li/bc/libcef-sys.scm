(define-module (crates-io li bc libcef-sys) #:use-module (crates-io))

(define-public crate-libcef-sys-1.0.0 (c (n "libcef-sys") (v "1.0.0") (d (list (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "1fsz4jq1fkr7ld579331ikx5wi41k0zxbnr84g3m9wcavk2c7nwg") (f (quote (("dox"))))))

(define-public crate-libcef-sys-1.0.1 (c (n "libcef-sys") (v "1.0.1") (d (list (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "03wcfq818spaf5sr0gncy4xzf1n789nqnqrc1ah1p7qkd2ph5cq3") (f (quote (("dox"))))))

(define-public crate-libcef-sys-117.2.5 (c (n "libcef-sys") (v "117.2.5") (d (list (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "1ayn8bnncd1r25c2x155ns21c3x90ppppxvvslpbdmahbx165nf1") (f (quote (("dox"))))))

(define-public crate-libcef-sys-117.2.6 (c (n "libcef-sys") (v "117.2.6") (d (list (d (n "winit") (r "^0.28") (d #t) (k 2)))) (h "1zzy4yxl8a86cnpp5ksknvvhkgrryq5laimhw588id40h6fnfkhr") (f (quote (("dox"))))))

