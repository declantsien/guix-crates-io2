(define-module (crates-io li bc libcodejam) #:use-module (crates-io))

(define-public crate-libcodejam-0.2.0 (c (n "libcodejam") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.11.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0.1") (d #t) (k 0)))) (h "1gmxkay36ydjhcxabwx4vpiiyn9jhmsk5v910rysp1saxwflnibn")))

