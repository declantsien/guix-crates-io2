(define-module (crates-io li bc libcw) #:use-module (crates-io))

(define-public crate-libcw-0.1.0 (c (n "libcw") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0ff6xc7pyahxsnnz1wjmcd4s2ss4m1xggd7j3gm6p8ppyi8xfy2d") (f (quote (("parser" "regex" "lazy_static") ("mars") ("default" "parser" "mars"))))))

(define-public crate-libcw-0.1.1 (c (n "libcw") (v "0.1.1") (d (list (d (n "lazy_static") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "regex") (r "^0.2") (o #t) (d #t) (k 0)))) (h "1pdymnnsziigac52x6gfrzsc6h6izff836wwhwghc8xd94y6r71v") (f (quote (("parser" "regex" "lazy_static") ("mars") ("default" "parser" "mars"))))))

