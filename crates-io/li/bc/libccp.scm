(define-module (crates-io li bc libccp) #:use-module (crates-io))

(define-public crate-libccp-0.0.1 (c (n "libccp") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1xsfscdykfhgfyklgwjfz2kz0v67h05irphn95v07y0q1avl53qw")))

(define-public crate-libccp-0.0.2 (c (n "libccp") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "09vkf54f0m9rq2nkndq470hpm5zk5aps1c29d5agnadhlajmsb89")))

(define-public crate-libccp-0.0.3 (c (n "libccp") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0idc69f1i0580a5iwza2maqpvq33ilwxlmx37rkcvvvh5j1drhs0")))

(define-public crate-libccp-0.0.4 (c (n "libccp") (v "0.0.4") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "07gs9cvf8xynl4ns9b0k1sszk3bnygxmhmkvk2axvas63clsr33i")))

(define-public crate-libccp-0.0.5 (c (n "libccp") (v "0.0.5") (d (list (d (n "bindgen") (r "^0.43.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0rpar7k6hxavlkmvylw46593gpcswzb1fq2b946c1bf12yzkd5dl")))

(define-public crate-libccp-0.0.6 (c (n "libccp") (v "0.0.6") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0rrrlqvgkwpaiwfdmr3d3gsjac1ifpdh9vyinqzvi0qfr52wb14v")))

(define-public crate-libccp-0.0.7 (c (n "libccp") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0kmbfr0iyqfky67rqc2cphcf30gkl8qk3qj4fy6b3d9mczjqah8q")))

(define-public crate-libccp-0.0.8 (c (n "libccp") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "02yph01b12l04wkcvc2rshwcgby2y3j29v6k5igm8d3bdh5h67ak") (f (quote (("log_warn") ("log_trace") ("log_info") ("log_error") ("log_debug"))))))

(define-public crate-libccp-0.0.9 (c (n "libccp") (v "0.0.9") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0c1llpkpsvgnkkqvbskankbnjqqsdk6lzi35vl9w6n27nfdlnmww") (f (quote (("log_warn") ("log_trace") ("log_info") ("log_error") ("log_debug"))))))

(define-public crate-libccp-0.0.10 (c (n "libccp") (v "0.0.10") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 2)) (d (n "mashup") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0z90hdcffzz23cscgsh4n1hxjn2yayx11zsrw0y4myprzs138cyv") (f (quote (("log_warn") ("log_trace") ("log_info") ("log_error") ("log_debug"))))))

(define-public crate-libccp-0.0.11 (c (n "libccp") (v "0.0.11") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1svsp3i4y7l4pr525vkjshpfd630mwbb422sghlaakq4gm75g6r2") (f (quote (("log_warn") ("log_trace") ("log_info") ("log_error") ("log_debug"))))))

(define-public crate-libccp-0.0.12 (c (n "libccp") (v "0.0.12") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1fvdpax7lpb5727752gqkj7fxc58432qy2v2dpl17m966lhz32f1") (f (quote (("log_warn") ("log_trace") ("log_info") ("log_error") ("log_debug"))))))

(define-public crate-libccp-0.0.13 (c (n "libccp") (v "0.0.13") (d (list (d (n "bindgen") (r "^0.47.0") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "125shy7p8b1xss22hh4mpzvb22g28mjvy8d962l3vd6zg10ijgfq") (f (quote (("log_warn") ("log_trace") ("log_info") ("log_error") ("log_debug"))))))

(define-public crate-libccp-1.0.0 (c (n "libccp") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0b3qcly03lsgrlp8qn3np1k1y372njms655lhy9ip8wvg8r17hc0") (f (quote (("log_warn") ("log_trace") ("log_info") ("log_error") ("log_debug"))))))

(define-public crate-libccp-1.1.0 (c (n "libccp") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "02darz2zrkwb5ybyk5p2lshczs3ifawj9gvq0kby8rf2g51bnrz0") (f (quote (("log_warn") ("log_trace") ("log_info") ("log_error") ("log_debug"))))))

(define-public crate-libccp-1.1.1 (c (n "libccp") (v "1.1.1") (d (list (d (n "bindgen") (r "^0.58") (d #t) (k 1)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1sbz0r2248ckk3jczsh1s2mqyinhvnzs7fxx84jqxp90p88amx21") (f (quote (("log_warn") ("log_trace") ("log_info") ("log_error") ("log_debug"))))))

