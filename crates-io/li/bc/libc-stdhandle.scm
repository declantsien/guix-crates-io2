(define-module (crates-io li bc libc-stdhandle) #:use-module (crates-io))

(define-public crate-libc-stdhandle-0.1.0 (c (n "libc-stdhandle") (v "0.1.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0lbmg42kncl3yjgzx48d2sa3p78w4fmhs9c21dg4r4r8virj9b3d")))

