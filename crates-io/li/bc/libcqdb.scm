(define-module (crates-io li bc libcqdb) #:use-module (crates-io))

(define-public crate-libcqdb-0.1.0 (c (n "libcqdb") (v "0.1.0") (d (list (d (n "cqdb") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 1)))) (h "1d0i9gjhsgiz40h9y8zsnh9ax8gb86x84a47r7d74cidnkngyd97") (l "libcqdb")))

(define-public crate-libcqdb-0.2.0 (c (n "libcqdb") (v "0.2.0") (d (list (d (n "cqdb") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 1)))) (h "00dwmram3ss2zqlf6ihn1b46xp6zgwg1dgp696qks6zbz7nn1581") (l "libcqdb")))

(define-public crate-libcqdb-0.3.0 (c (n "libcqdb") (v "0.3.0") (d (list (d (n "cqdb") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 1)))) (h "0xpv1l8i9dl7a1gsf5njkc4ckr1crhkn8d056gl2xz3rx52yngw5") (l "libcqdb")))

(define-public crate-libcqdb-0.3.1 (c (n "libcqdb") (v "0.3.1") (d (list (d (n "cqdb") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 1)))) (h "055p361a30w65k9ap6jd8rs5q6aibmmc76nq0i5344qdj4yknymb") (l "libcqdb")))

(define-public crate-libcqdb-0.3.2 (c (n "libcqdb") (v "0.3.2") (d (list (d (n "cqdb") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.82") (d #t) (k 0)) (d (n "path-slash") (r "^0.1.4") (d #t) (k 1)))) (h "1qjnkmr84cvw1mcsl59blpgvrfhxyp2swzl1ppl29fpmj6l8jr22") (l "libcqdb")))

