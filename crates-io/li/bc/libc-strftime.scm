(define-module (crates-io li bc libc-strftime) #:use-module (crates-io))

(define-public crate-libc-strftime-0.1.0 (c (n "libc-strftime") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0krnv0pdyjhgdfl2rdi7jng4kgmklp8ij301ccabmq9hm07n30g4")))

(define-public crate-libc-strftime-0.2.0 (c (n "libc-strftime") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kv4cggvi250dwv574j18ppjdx1jzxvf97gz5izh28w67lpv3b9v")))

