(define-module (crates-io li bc libcamera) #:use-module (crates-io))

(define-public crate-libcamera-0.1.0 (c (n "libcamera") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)) (d (n "nix") (r "^0.26.2") (f (quote ("mman"))) (d #t) (k 0)))) (h "0wzpfghxl3hqisfvwbfl7ds76arkx02n16vrv636wads45rp8cd3") (y #t)))

(define-public crate-libcamera-0.2.0 (c (n "libcamera") (v "0.2.0") (d (list (d (n "bitflags") (r "^2.0.0-rc.2") (d #t) (k 0)) (d (n "drm-fourcc") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libcamera-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12yz65jxkzj9jwk0lavhi0wmdqdaa937y58h9g18k71pyxvgsaj5")))

(define-public crate-libcamera-0.2.1 (c (n "libcamera") (v "0.2.1") (d (list (d (n "bitflags") (r "^2.0.0-rc.2") (d #t) (k 0)) (d (n "drm-fourcc") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libcamera-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pf69j72ags7s1jv0agz3vxp5g3amkpkzfv3dfsn1d56gskh9rxm")))

(define-public crate-libcamera-0.2.2 (c (n "libcamera") (v "0.2.2") (d (list (d (n "bitflags") (r "^2.0.0-rc.2") (d #t) (k 0)) (d (n "drm-fourcc") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libcamera-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05cis9gyjk9bxkxmr3i7akzrvarvh5drv26q7375ibmgv80m07wl")))

(define-public crate-libcamera-0.2.3 (c (n "libcamera") (v "0.2.3") (d (list (d (n "bitflags") (r "^2.0.0-rc.2") (d #t) (k 0)) (d (n "drm-fourcc") (r "^2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libcamera-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "num_enum") (r "^0.6.1") (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "05ivkz0c39w9mj5iizx1ji8rnicpkppng0vq5kw2vb7shgh7fk1p")))

