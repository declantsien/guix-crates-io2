(define-module (crates-io li bc libcachebust) #:use-module (crates-io))

(define-public crate-libcachebust-0.2.1 (c (n "libcachebust") (v "0.2.1") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1lcf5lpw4y6z9s648n00g28dlzwj921c3s2jwr4mar9qvjgmw1k8")))

(define-public crate-libcachebust-0.3.0 (c (n "libcachebust") (v "0.3.0") (d (list (d (n "data-encoding") (r "^2.3.2") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "mime_guess") (r "^2.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "0861hcn6pfipk3q7hy087mj1bb6jc3ix64l2x285mj53asr10l76")))

