(define-module (crates-io li bc libcypher-parser-sys) #:use-module (crates-io))

(define-public crate-libcypher-parser-sys-0.6.0 (c (n "libcypher-parser-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1a2k4pn6k5lxccj32mdm4q3b4rfpmy492hm3bw4f3jf3dhmrcip4") (l "cypher-parser")))

(define-public crate-libcypher-parser-sys-0.6.1 (c (n "libcypher-parser-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1m7pihfw1g2zwqjsysispzgd2qavqnfzgjcrak7q8gxfdzk3ay7m") (y #t) (l "cypher-parser")))

(define-public crate-libcypher-parser-sys-0.6.1-1 (c (n "libcypher-parser-sys") (v "0.6.1-1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1pkhimjlfp64ybm1hb4x4wc9dgy0d73hdh0jj9ynb8jfzd80adx7") (l "cypher-parser")))

