(define-module (crates-io li bc libcrypt-rs) #:use-module (crates-io))

(define-public crate-libcrypt-rs-0.1.0 (c (n "libcrypt-rs") (v "0.1.0") (h "1hj86sji99mgkg53w8pzl0n61n24q22pjf2f07wqj9vdfzzbfbzd") (y #t)))

(define-public crate-libcrypt-rs-0.1.1 (c (n "libcrypt-rs") (v "0.1.1") (h "18gslfcy4wywa6731m6gm3as8qdy3wlx6f5cj1drz6zznqml1k3l") (y #t)))

(define-public crate-libcrypt-rs-0.1.2 (c (n "libcrypt-rs") (v "0.1.2") (h "1b498ksmh6gh15ybmsacw3gy3kl9drzfn08b0gn1apgq2b3dgcrr")))

