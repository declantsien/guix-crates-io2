(define-module (crates-io li bc libc_tools) #:use-module (crates-io))

(define-public crate-libc_tools-0.1.0 (c (n "libc_tools") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "03fhxhmdd6abs1p3mcgvnizibklj3prv7w44nx8bjs9ghmyzaa7n")))

(define-public crate-libc_tools-0.1.1 (c (n "libc_tools") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "0a8a4b7500w884kqgyxjp0zdiin1cy86h1w0pbqvi9h4w5xzhimg")))

(define-public crate-libc_tools-0.1.2 (c (n "libc_tools") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "06882f0s5n81w0ffr7xkd8mbkdmf5jk6c0hij5m5q17n61s1xgn6")))

(define-public crate-libc_tools-0.1.3 (c (n "libc_tools") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.98") (d #t) (k 0)))) (h "1dka2cc2kix80hbfyfsdrlxxzag906ylkc8asbj7j38asl6x36g5")))

