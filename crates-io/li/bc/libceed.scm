(define-module (crates-io li bc libceed) #:use-module (crates-io))

(define-public crate-libceed-0.8.0 (c (n "libceed") (v "0.8.0") (d (list (d (n "libceed-sys") (r "^0.8") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "03h50yzzd2hiim02fx5ym44473g6cvp95dk3g6ahly2s3j6crbdj")))

(define-public crate-libceed-0.9.0 (c (n "libceed") (v "0.9.0") (d (list (d (n "libceed-sys") (r "^0.9") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0f654dv7glb0zb0ham459p4pin2wmn33l44b3bb9ib3hhymjyp3g")))

(define-public crate-libceed-0.11.0 (c (n "libceed") (v "0.11.0") (d (list (d (n "katexit") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "libceed-sys") (r "^0.11") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "1wwgqr68r1770miy0yinmii2i6afxwpdabxmjnx446y4wllx89yn") (r "1.56")))

(define-public crate-libceed-0.12.0 (c (n "libceed") (v "0.12.0") (d (list (d (n "katexit") (r "^0.1.1") (o #t) (d #t) (k 0)) (d (n "libceed-sys") (r "^0.12") (d #t) (k 0)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "08a3lrfpjpfl8jim05f3wy5a48m7jj55ybrcjhzpd9d532n98ln6") (r "1.56")))

