(define-module (crates-io li bc libc-spawn) #:use-module (crates-io))

(define-public crate-libc-spawn-0.0.1 (c (n "libc-spawn") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r8248s35cp7n95y2h4w2f7gq8r97jpmkirnjprbh4ds6nw60zaj")))

