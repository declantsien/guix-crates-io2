(define-module (crates-io li bc libcamelliaoss) #:use-module (crates-io))

(define-public crate-libcamelliaoss-1.0.0 (c (n "libcamelliaoss") (v "1.0.0") (d (list (d (n "bytecodec") (r "^0.4") (f (quote ("bincode_codec"))) (d #t) (k 0)) (d (n "fibers") (r "^0.1") (d #t) (k 0)) (d (n "fibers_rpc") (r "^0.3") (d #t) (k 0)) (d (n "futures") (r "^0.1.31") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "trackable") (r "^1.2.0") (f (quote ("serialize"))) (d #t) (k 0)))) (h "05kxrgw49p0ir9q2kigdbk75bgd5vl49fq8d6pd6w4mqh7gd4ybq")))

