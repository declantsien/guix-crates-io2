(define-module (crates-io li bc libcne-ve) #:use-module (crates-io))

(define-public crate-libcne-ve-0.1.0 (c (n "libcne-ve") (v "0.1.0") (d (list (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "scraper") (r "^0.12.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.59") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "tokio-test") (r "^0.2.1") (d #t) (k 2)))) (h "04lf3v14q2c0lrlvq0zdrfbi9kf9hik9dwbydrgzl7kjz4mp0cw5")))

