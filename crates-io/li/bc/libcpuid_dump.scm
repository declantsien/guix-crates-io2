(define-module (crates-io li bc libcpuid_dump) #:use-module (crates-io))

(define-public crate-libcpuid_dump-0.1.0 (c (n "libcpuid_dump") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.34.0") (f (quote ("Win32_Foundation" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1c7vsycz98yaycawka201spfhh0a8sj4xc7427b5x4v1zdzgzz8h") (f (quote (("std") ("default" "std"))))))

(define-public crate-libcpuid_dump-0.1.1 (c (n "libcpuid_dump") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.34") (f (quote ("Win32_Foundation" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1x0a8ijn7v4gfbxpqw5pf97yf1lr3zd972m6cl6qisj19ai789sx") (f (quote (("std") ("default" "std"))))))

(define-public crate-libcpuid_dump-0.1.2 (c (n "libcpuid_dump") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (t "cfg(unix)") (k 0)) (d (n "windows") (r "^0.34") (f (quote ("Win32_Foundation" "Win32_System_Threading"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0d4817x8ycxlc8zxipvxn69v50h6j9da9674yqc31si4qm4nlv0w") (f (quote (("std") ("default" "std"))))))

