(define-module (crates-io li bc libcontract) #:use-module (crates-io))

(define-public crate-libcontract-0.1.0 (c (n "libcontract") (v "0.1.0") (d (list (d (n "contract-sys") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "06gqr8q0kglxz1ym76dwcz3yllnbmz8jjf7scm5ls69f1zb91gc7")))

