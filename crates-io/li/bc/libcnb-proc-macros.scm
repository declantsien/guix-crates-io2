(define-module (crates-io li bc libcnb-proc-macros) #:use-module (crates-io))

(define-public crate-libcnb-proc-macros-0.1.0 (c (n "libcnb-proc-macros") (v "0.1.0") (d (list (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.10") (d #t) (k 0)) (d (n "syn") (r "^1.0.81") (f (quote ("full"))) (d #t) (k 0)))) (h "0h8sykrklglp3a8rlhxcyb648g9r56f51j42ycykl9hw858ysg25") (r "1.56")))

(define-public crate-libcnb-proc-macros-0.1.1 (c (n "libcnb-proc-macros") (v "0.1.1") (d (list (d (n "fancy-regex") (r "^0.7.1") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0syhffi486xs3k27hh8n2ln3pq17lmm1cs36fjjq24q7xrm2lrli") (r "1.56")))

(define-public crate-libcnb-proc-macros-0.2.0 (c (n "libcnb-proc-macros") (v "0.2.0") (d (list (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.8.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.15") (d #t) (k 0)) (d (n "syn") (r "^1.0.86") (f (quote ("full"))) (d #t) (k 0)))) (h "1dgfrxfa4c1m8xvagp7ihj7x5yb824xwyb66c9xzh5vcmiqxq13f") (r "1.58")))

(define-public crate-libcnb-proc-macros-0.2.1 (c (n "libcnb-proc-macros") (v "0.2.1") (d (list (d (n "cargo_metadata") (r "^0.14.2") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.8.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("full"))) (d #t) (k 0)))) (h "15w014fzdwb8lhiknbrqn7gbhmyq40xj6jpzrrpvi2jssd1jlc9g") (r "1.58")))

(define-public crate-libcnb-proc-macros-0.2.2 (c (n "libcnb-proc-macros") (v "0.2.2") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "0xhsr0m5ffhp1pjaq38jbhhzw5r4nic66jkwg25yl6kbksgy3g1m") (r "1.58")))

(define-public crate-libcnb-proc-macros-0.3.0 (c (n "libcnb-proc-macros") (v "0.3.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "12r7zn68r9dskqb72ggd0cgihhfa0v7392hq3gm6ygyzk0akw0i1") (r "1.59")))

(define-public crate-libcnb-proc-macros-0.10.0 (c (n "libcnb-proc-macros") (v "0.10.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "syn") (r "^1.0.98") (f (quote ("full"))) (d #t) (k 0)))) (h "03vijwamhqdhirfac49g1i0v2qhys5r96lhgisl2xxr52p8x567j") (r "1.59")))

(define-public crate-libcnb-proc-macros-0.11.0 (c (n "libcnb-proc-macros") (v "0.11.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("full"))) (d #t) (k 0)))) (h "152fwdahj9dpg6ww7azwbhmjvmbn0dvvvkhcgqy2gm27hvvyv24h") (r "1.64")))

(define-public crate-libcnb-proc-macros-0.11.1 (c (n "libcnb-proc-macros") (v "0.11.1") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("full"))) (d #t) (k 0)))) (h "0cjd4cjgm2mgxixadic2bxb88gnaa287nkm2p6pjz7k2vj8r0ah9") (r "1.64")))

(define-public crate-libcnb-proc-macros-0.11.2 (c (n "libcnb-proc-macros") (v "0.11.2") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("full"))) (d #t) (k 0)))) (h "1ymn6xxbb9ksz3khpsvcs69iaw5dq6p808dbhpav0d7zlfqa0608") (r "1.64")))

(define-public crate-libcnb-proc-macros-0.11.3 (c (n "libcnb-proc-macros") (v "0.11.3") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("full"))) (d #t) (k 0)))) (h "173s7g3aq6lk4245r20nyg1vwhlwy9qay1f9l8p1di845bbnhl1m") (r "1.64")))

(define-public crate-libcnb-proc-macros-0.11.4 (c (n "libcnb-proc-macros") (v "0.11.4") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.10.0") (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("full"))) (d #t) (k 0)))) (h "1giyf2y2av5mygdl7y4h1rnvw2pgnmx7kv713m13hhzwsjgqgcr5") (r "1.64")))

(define-public crate-libcnb-proc-macros-0.11.5 (c (n "libcnb-proc-macros") (v "0.11.5") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.100") (f (quote ("full"))) (d #t) (k 0)))) (h "0kkfgmdlswjjgmy4ldz1xcjahdwbdqp90jy4q20m098m4sicwc2r") (r "1.64")))

(define-public crate-libcnb-proc-macros-0.12.0 (c (n "libcnb-proc-macros") (v "0.12.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0z4ql7y7f3ijq0xbda6qwrqrhrl1yv7mvdgcahfhzl107b4irqy9") (r "1.64")))

(define-public crate-libcnb-proc-macros-0.13.0 (c (n "libcnb-proc-macros") (v "0.13.0") (d (list (d (n "cargo_metadata") (r "^0.15.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^2.0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xyamv872lrdjvigaff4i2l57dn19xsdlymwd98q1lpzccfkrcqs") (r "1.64")))

(define-public crate-libcnb-proc-macros-0.14.0 (c (n "libcnb-proc-macros") (v "0.14.0") (d (list (d (n "cargo_metadata") (r "^0.17.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (f (quote ("full"))) (d #t) (k 0)))) (h "1kyg6wjwspylwg0vwja2hxqrqjy5g05c5fg9chsjr85k732cw5f7") (r "1.64")))

(define-public crate-libcnb-proc-macros-0.15.0 (c (n "libcnb-proc-macros") (v "0.15.0") (d (list (d (n "cargo_metadata") (r "^0.18.0") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.37") (f (quote ("full"))) (d #t) (k 0)))) (h "0k23303xxnahm0z6d0gmq2fl8ydp2fr82ml8lag8j0bl3b3wizsy") (r "1.64")))

(define-public crate-libcnb-proc-macros-0.16.0 (c (n "libcnb-proc-macros") (v "0.16.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.12.0") (f (quote ("std"))) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "0qa6zwhg5da7yligw0f6ysb5413207x320jjw91l1vcjj7vhm56p") (r "1.74")))

(define-public crate-libcnb-proc-macros-0.17.0 (c (n "libcnb-proc-macros") (v "0.17.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.12.0") (f (quote ("std"))) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (f (quote ("full"))) (d #t) (k 0)))) (h "05wwhzk426mfpl9nykw9lkaa1qvd5ysjqgklzmm969vk7bgazw60") (r "1.74")))

(define-public crate-libcnb-proc-macros-0.18.0 (c (n "libcnb-proc-macros") (v "0.18.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13.0") (f (quote ("std"))) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "0nvqychdjzhy9pybdhdb285v0gv5709dqvx96ib3yjflf1r84ibd") (r "1.76")))

(define-public crate-libcnb-proc-macros-0.19.0 (c (n "libcnb-proc-macros") (v "0.19.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13.0") (f (quote ("std"))) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.50") (f (quote ("full"))) (d #t) (k 0)))) (h "02b7s78kpqx1ky2549n75mqxnaxha2dx2hmbd6wv1n2dalxqwc5h") (r "1.76")))

(define-public crate-libcnb-proc-macros-0.20.0 (c (n "libcnb-proc-macros") (v "0.20.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13.0") (f (quote ("std"))) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.58") (f (quote ("full"))) (d #t) (k 0)))) (h "1k8wkbkmykglw4s48wyyh31pm8dy616i6cyk6v0pjzl4cj2hh6r2") (r "1.76")))

(define-public crate-libcnb-proc-macros-0.21.0 (c (n "libcnb-proc-macros") (v "0.21.0") (d (list (d (n "cargo_metadata") (r "^0.18.1") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.13.0") (f (quote ("std"))) (k 0)) (d (n "quote") (r "^1.0.36") (d #t) (k 0)) (d (n "syn") (r "^2.0.60") (f (quote ("full"))) (d #t) (k 0)))) (h "0bs1ppxkp51qrp9x66ilgfmibb0v868naypp56dxcv1sgi3vlfzq") (r "1.76")))

