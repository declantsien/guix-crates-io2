(define-module (crates-io li bc libcoinche) #:use-module (crates-io))

(define-public crate-libcoinche-0.1.0 (c (n "libcoinche") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.12") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.16") (d #t) (k 0)))) (h "1aky4szspdz7y0yr02lzpg6cmf1cqsan57gsacdvm4f9lva9h4wf")))

(define-public crate-libcoinche-0.1.1 (c (n "libcoinche") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "01iwl6k7f12awx57r8h7yjc7ky651d5k7r70l00z6dpghfc10iyz")))

(define-public crate-libcoinche-0.1.2 (c (n "libcoinche") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "01fn7b7zgxbda8yy93m3ky7knqbjxwhvwdfwagm0nqdimdp8c6gz") (f (quote (("use_bench") ("default"))))))

(define-public crate-libcoinche-0.1.3 (c (n "libcoinche") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0sax8bxmv9bxjanvjayj7x1rljhsrsqg862p25kzwh8y6bad688y") (f (quote (("use_bench") ("default"))))))

(define-public crate-libcoinche-0.1.4 (c (n "libcoinche") (v "0.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "10cvgv2mihskj943sxdkw7p912pb0d2yd5ywrc6nydqsk3d2r0ks") (f (quote (("use_bench") ("default"))))))

(define-public crate-libcoinche-0.1.5 (c (n "libcoinche") (v "0.1.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1ajridcs9w77qbrygak17axiwp14l78iyss0mlsiflr0clsyhhgy") (f (quote (("use_bench") ("default"))))))

(define-public crate-libcoinche-0.1.6 (c (n "libcoinche") (v "0.1.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "00bqxdqb263675k6na3av0m3d40vgjp3qhh8ib6i4xjhs63jrir0") (f (quote (("use_bench") ("default"))))))

(define-public crate-libcoinche-0.1.7 (c (n "libcoinche") (v "0.1.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0b8rnx5l1j7icqc3gf8m8p7gccxrkqxg2n28wvgg4aprxj2qx3zw") (f (quote (("use_bench") ("default"))))))

(define-public crate-libcoinche-0.1.8 (c (n "libcoinche") (v "0.1.8") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "13hb9kkl45dfgsf4nl3qvg3vh5xlf0anpdj8gac0qgj3lgd4ff46") (f (quote (("use_bench") ("default"))))))

(define-public crate-libcoinche-0.1.9 (c (n "libcoinche") (v "0.1.9") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "07ngs1575rl61x27sg91dnixsixnhr3xacy7kijp60ylm1h9rcw2") (f (quote (("use_bench") ("default"))))))

(define-public crate-libcoinche-0.2.0 (c (n "libcoinche") (v "0.2.0") (d (list (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "serde") (r "^1.0.6") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.6") (d #t) (k 0)))) (h "07q0i77fg4w07nx96yfiyh0qnrqpaaj71anj5sgxxgvxs1m0iqz5")))

(define-public crate-libcoinche-0.4.0 (c (n "libcoinche") (v "0.4.0") (d (list (d (n "rand") (r "^0.8.5") (f (quote ("std_rng"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)))) (h "1cqlr91iihn1q5qpvwzk5iimh1ycmg8qzjmhg4zxhn0l420yks24")))

