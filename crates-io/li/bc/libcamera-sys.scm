(define-module (crates-io li bc libcamera-sys) #:use-module (crates-io))

(define-public crate-libcamera-sys-0.2.0 (c (n "libcamera-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1iqp2kmvg5v2byc73palfvqwp61jsyha128l81r511ja4jxysgic")))

(define-public crate-libcamera-sys-0.2.1 (c (n "libcamera-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "13x8d2wi0f4xck01hpph8jy2gqxfshcyjnx86nss5gx4xlq0smhj")))

(define-public crate-libcamera-sys-0.2.2 (c (n "libcamera-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "05kpin9rw1460296zmjjf0v0yrwkjpnlw6bjvzj0jna5ra7q1g8z")))

(define-public crate-libcamera-sys-0.2.3 (c (n "libcamera-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0y1j3w8v9jh9c3c8mc3y9nixybx2qaaizcp8icjnflxfhncafsg8")))

