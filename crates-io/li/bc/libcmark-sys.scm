(define-module (crates-io li bc libcmark-sys) #:use-module (crates-io))

(define-public crate-libcmark-sys-0.1.0 (c (n "libcmark-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rbh5jhmw0zbvndndc3bs462vm6d4j0sl2im44j8b9ira1vmhkbn")))

