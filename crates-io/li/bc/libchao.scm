(define-module (crates-io li bc libchao) #:use-module (crates-io))

(define-public crate-libchao-0.1.0 (c (n "libchao") (v "0.1.0") (d (list (d (n "colored") (r "^1.6") (d #t) (k 0)) (d (n "combine") (r "^3.4") (d #t) (k 0)) (d (n "itertools") (r "^0.7") (d #t) (k 0)))) (h "1g00lfqfkn9zjzjgrbkzmn89vbrcc71402vw469dxacjixs7605a")))

