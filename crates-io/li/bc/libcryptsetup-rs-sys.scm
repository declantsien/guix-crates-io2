(define-module (crates-io li bc libcryptsetup-rs-sys) #:use-module (crates-io))

(define-public crate-libcryptsetup-rs-sys-0.1.0 (c (n "libcryptsetup-rs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)))) (h "0dnr7a89d0x0y34lv6cz3waa5pybhdrm2bj8ncnf5ljr2cnqqr5a")))

(define-public crate-libcryptsetup-rs-sys-0.1.1 (c (n "libcryptsetup-rs-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hjmplx07x315bxw643nnspz1hqx083z26i0cbyrkncy2kyrgba1")))

(define-public crate-libcryptsetup-rs-sys-0.1.2 (c (n "libcryptsetup-rs-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "1rwsykmxpc27535gar6w8pfg3xhxx92vxl65yk0rjahyx2c5jq0c")))

(define-public crate-libcryptsetup-rs-sys-0.1.3 (c (n "libcryptsetup-rs-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "1xy2g7mrmj4kj6f0ngjvpnq4wv269ajkpz4si6z277y8ss1zxdqw")))

(define-public crate-libcryptsetup-rs-sys-0.1.4 (c (n "libcryptsetup-rs-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.54.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^0.9") (d #t) (k 1)))) (h "0ki4v1szg0kbncza829x6yz19nk666bjq8sbvfgv77sh8sws4xab")))

(define-public crate-libcryptsetup-rs-sys-0.1.5 (c (n "libcryptsetup-rs-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.57.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "semver") (r "^0.11") (d #t) (k 1)))) (h "1p88hg7wf3mahxpyk6dab83z3d7a64l71gpcjpwcwvpa52c65v8a")))

(define-public crate-libcryptsetup-rs-sys-0.1.6 (c (n "libcryptsetup-rs-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "semver") (r "^1.0.0") (d #t) (k 1)))) (h "1p4q4xxjxs1n0brxd03c4v8h8x3i54l702f1wcry57v74gljbvs1")))

(define-public crate-libcryptsetup-rs-sys-0.2.0 (c (n "libcryptsetup-rs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "semver") (r "^1.0.0") (d #t) (k 1)))) (h "174z4g4glkknl6w2vyijghq9nsjbbfsixqrf16a66p6a1dj6w4qc")))

(define-public crate-libcryptsetup-rs-sys-0.2.1 (c (n "libcryptsetup-rs-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.59.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "semver") (r "^1.0.0") (d #t) (k 1)))) (h "1gmgjxsw6wds09nsllwmdcgddpbaiy6bpv89jf1ir4cr8rj1py8a")))

(define-public crate-libcryptsetup-rs-sys-0.2.2 (c (n "libcryptsetup-rs-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "semver") (r "^1.0.0") (d #t) (k 1)))) (h "0jn2yx0r0r6y1i4kw4ywgvd1r8vhnmqqg65vv6qggm29am4qp3jk") (y #t) (r "1.66.1")))

(define-public crate-libcryptsetup-rs-sys-0.2.3 (c (n "libcryptsetup-rs-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "semver") (r "^1.0.0") (d #t) (k 1)))) (h "02bls6xzayc6jlwgf2920gjamxwazz2msicycjb0j1sy35z4gcw2") (r "1.66.1")))

(define-public crate-libcryptsetup-rs-sys-0.2.4 (c (n "libcryptsetup-rs-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "semver") (r "^1.0.0") (d #t) (k 1)))) (h "1szlv3f4hs90qbrksxcpjsbqd5wr0qv2n614k6ha2w54hg15bhyp") (r "1.66.1")))

(define-public crate-libcryptsetup-rs-sys-0.3.0 (c (n "libcryptsetup-rs-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.68.1") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "semver") (r "^1.0.0") (d #t) (k 1)))) (h "01s7694pailmq2b81z2q8bchh41x8vy41ncmm19x6y2hs2gjkz10") (r "1.71.1")))

(define-public crate-libcryptsetup-rs-sys-0.4.0 (c (n "libcryptsetup-rs-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.69.0") (f (quote ("runtime"))) (k 1)) (d (n "cc") (r "^1.0.45") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)) (d (n "semver") (r "^1.0.0") (d #t) (k 1)))) (h "1lmqcwl7sfp8rgknxsffp6pb9snh9rsih4fir3fsb6nb85rkk2y7") (r "1.71.1")))

