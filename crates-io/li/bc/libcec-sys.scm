(define-module (crates-io li bc libcec-sys) #:use-module (crates-io))

(define-public crate-libcec-sys-0.1.0 (c (n "libcec-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)))) (h "0ix73b46q9cp37zb1dzgavn0y1dhibkni3gpgzqpm2q30z3nnk0k")))

(define-public crate-libcec-sys-0.2.0 (c (n "libcec-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)))) (h "0xp4lcxi54hr9qcamapbmxzihq229b6wc7abi5zm018n2gw8y4jq")))

(define-public crate-libcec-sys-0.2.1 (c (n "libcec-sys") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)))) (h "1sx6jsj0lysw5lgx6qwq8lkwjk4w3vgj72vqfbv4cg94llxr4rvb")))

(define-public crate-libcec-sys-1.0.0 (c (n "libcec-sys") (v "1.0.0") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)))) (h "0q86r9s68k876ydf2mbvv103g2invzxx9fb786icdb0skz7f1xj2")))

(define-public crate-libcec-sys-1.0.2 (c (n "libcec-sys") (v "1.0.2") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "0930ib4kkbaj1783i6lh8qcwyqghcc1yczh6wrbch5q3pm34yp89")))

(define-public crate-libcec-sys-1.0.6 (c (n "libcec-sys") (v "1.0.6") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "0lkhg9w62pznzy1cag0ph1wbxl045q84dz2zp9r0p5xv9dvhw9sh")))

(define-public crate-libcec-sys-1.0.7 (c (n "libcec-sys") (v "1.0.7") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "0rawcspipl3c60xc73zszmba0px4lqf3frp8fdyqkaqllic5p0zk")))

(define-public crate-libcec-sys-1.1.0 (c (n "libcec-sys") (v "1.1.0") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "0gi4w1p0a8h26hr2xmbb0mjy1f6kiyjd4a6d808yfq2hdx3v7wkz")))

(define-public crate-libcec-sys-1.1.1 (c (n "libcec-sys") (v "1.1.1") (d (list (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)))) (h "1pp9djxrwh2k7hmzhnpkbidfn5sms6bfhna8h40aafglaaz2vv6f")))

(define-public crate-libcec-sys-2.0.0 (c (n "libcec-sys") (v "2.0.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "0253w3526iiqng7cnx73clpczb1fhr19fp55gkzcxjz7v81nn6ij") (f (quote (("vendored") ("default")))) (l "cec")))

(define-public crate-libcec-sys-2.0.1 (c (n "libcec-sys") (v "2.0.1") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "1c9fs4wp25fg64pm1ayjmxab6l8ybd6q79cfhwi764gcj80rmmna") (f (quote (("vendored") ("default")))) (l "cec")))

(define-public crate-libcec-sys-3.0.0 (c (n "libcec-sys") (v "3.0.0") (d (list (d (n "cc") (r "^1.0.72") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.24") (d #t) (k 1)))) (h "1ckhs2812fx2fza4aiglkaggxp5flwljcnvi5sm042vh06fg40cc") (f (quote (("vendored") ("default")))) (l "cec")))

(define-public crate-libcec-sys-4.0.0 (c (n "libcec-sys") (v "4.0.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "0pial3dp3j5s8w42f69gfpdf27ygk8m7mnxhqqnb41pv95sl4ns4") (f (quote (("vendored") ("default")))) (l "cec")))

(define-public crate-libcec-sys-4.0.1 (c (n "libcec-sys") (v "4.0.1") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "copy_dir") (r "^0.1.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "1zmspxwqdnxwmdvi3w13dz12iywr5rb4x2vxn6l5m90aryymigbw") (f (quote (("vendored") ("default")))) (l "cec")))

(define-public crate-libcec-sys-4.0.2 (c (n "libcec-sys") (v "4.0.2") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "dircpy") (r "^0.3.14") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "0qjdg5llmgkc0rnxihnmsnzcc5x21sjbgsyndzmf8db4mfcdjh27") (f (quote (("vendored") ("default")))) (l "cec")))

(define-public crate-libcec-sys-4.0.3 (c (n "libcec-sys") (v "4.0.3") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "fs_extra") (r "^1.3.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "1glyv8hsngd0wbfgc26zvd6sg7xm5wj8r24z1i1lvg07g7534hb1") (f (quote (("vendored") ("default")))) (l "cec")))

