(define-module (crates-io li bc libctp-sys) #:use-module (crates-io))

(define-public crate-libctp-sys-0.1.0 (c (n "libctp-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "01lficml3p9rh4zy4xdyd8y38jnjs6fj84z4v3km46y4ql745kc1")))

(define-public crate-libctp-sys-0.1.1 (c (n "libctp-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "024rlfm74419l173c1nbi0s2k8v94dmsimakv7hsnkb0dps5b5qh")))

(define-public crate-libctp-sys-0.1.2 (c (n "libctp-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0hzdaa95vdxigdc8smv8dbbky50mr6prxa98b9ic7ay3633pw3fm")))

(define-public crate-libctp-sys-0.1.3 (c (n "libctp-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1d5nbbfsn3dbibzdzxn3iqq80j7z6i9b59977hr16i1wi4lv2yj6")))

(define-public crate-libctp-sys-0.1.4 (c (n "libctp-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1l0dqp93cm3ly817yy9f0d6jp257fjn8j774aggk51sc4aa8fqgn")))

