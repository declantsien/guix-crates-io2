(define-module (crates-io li bc libcruby-sys) #:use-module (crates-io))

(define-public crate-libcruby-sys-0.5.0-alpha-1 (c (n "libcruby-sys") (v "0.5.0-alpha-1") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "1h9p5gdgd57ki6cf84l6i7ayhr4xm8cwcp4mjwikiq8nhxb2vgby")))

(define-public crate-libcruby-sys-0.5.0-alpha-2 (c (n "libcruby-sys") (v "0.5.0-alpha-2") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "11d2i8qh6j3i7xjxj4iw7rcrj47l2yy30sa57whzfkmg3g0nd04k")))

(define-public crate-libcruby-sys-0.5.0-alpha-4 (c (n "libcruby-sys") (v "0.5.0-alpha-4") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "1rnsh153g1pc75q7g7c3l3rhv1qqr5fz4mkz9hrgf0n84gpqzc4j")))

(define-public crate-libcruby-sys-0.5.0 (c (n "libcruby-sys") (v "0.5.0") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "0l1cl3bh2km3kbg9gz101mk30njdkywld0s28x42zqgbcm8aha48")))

(define-public crate-libcruby-sys-0.6.0 (c (n "libcruby-sys") (v "0.6.0") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "1raay34am9fj9jngvmzlyipx5jnz08yb6241sq6b5wd1blr7dwiv")))

(define-public crate-libcruby-sys-0.6.1 (c (n "libcruby-sys") (v "0.6.1") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "05312i0387scxwzy16yl7wba1qsr36bqjdalpdavqms10rgmgbnr")))

(define-public crate-libcruby-sys-0.6.2 (c (n "libcruby-sys") (v "0.6.2") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "0nymv0grqc7xpzd3269n2fj80gb9n1dw91v8l0f7xcr81v745dfq")))

(define-public crate-libcruby-sys-0.0.1 (c (n "libcruby-sys") (v "0.0.1") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "1s10ryfwcsaah5b0sb1ks5v9b6h1i7ryyzz20s03czam70s8ykrv") (y #t)))

(define-public crate-libcruby-sys-0.6.3 (c (n "libcruby-sys") (v "0.6.3") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "0j79104y49cp3vp100623x90xzsycy8f7afl3qjw3bpdhh41afj4")))

(define-public crate-libcruby-sys-0.6.4 (c (n "libcruby-sys") (v "0.6.4") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "0dbhdrn3whxni40h256k59sk6jfga4qgw3jb5bf5n0q1xxrhahzf")))

(define-public crate-libcruby-sys-0.7.0 (c (n "libcruby-sys") (v "0.7.0") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "03888r975035zpjjp9p7y8c1lkx10zvpfkidigy1kvs133gn4ak9")))

(define-public crate-libcruby-sys-0.7.1 (c (n "libcruby-sys") (v "0.7.1") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "0q4fxifi6qg4p3040mqqzirv26fl0mj0cxyvbrnjvg5j0qw954bh")))

(define-public crate-libcruby-sys-0.7.2 (c (n "libcruby-sys") (v "0.7.2") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "0b18canlam1rxv0nc7pdhdfdx70b9s7k1f0z77yhv3rqkw949nd8")))

(define-public crate-libcruby-sys-0.7.3 (c (n "libcruby-sys") (v "0.7.3") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "0nb0zqldkffnwnrafx6vp8kybqqvjrpy44nx0vbjg4ajxfdzpc78")))

(define-public crate-libcruby-sys-0.7.4 (c (n "libcruby-sys") (v "0.7.4") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "1jp9j21zdm2r4h4h27zajp1hizx9iczkk22i5paian7lgcryl4xr")))

(define-public crate-libcruby-sys-0.7.5 (c (n "libcruby-sys") (v "0.7.5") (d (list (d (n "libc") (r ">= 0.1.0") (d #t) (k 0)))) (h "1h549pl7blqihp973pg0mn6fzb0gh6xnc7gxfrbdbj70vj605xpy")))

