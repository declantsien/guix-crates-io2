(define-module (crates-io li bc libcrux-platform) #:use-module (crates-io))

(define-public crate-libcrux-platform-0.0.2-pre.1 (c (n "libcrux-platform") (v "0.0.2-pre.1") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "1ckmg5qgkzy2xqdkngdqqcmhjcyna65xih7yb15aln3pg6i3l02k")))

(define-public crate-libcrux-platform-0.0.2-pre.2 (c (n "libcrux-platform") (v "0.0.2-pre.2") (d (list (d (n "libc") (r "^0.2.147") (d #t) (k 0)))) (h "041lrz8clbhcaqr1b0mrk3gpkgmrah8isi8rgkqivcclc5k3jzk4")))

