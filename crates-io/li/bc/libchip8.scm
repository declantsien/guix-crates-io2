(define-module (crates-io li bc libchip8) #:use-module (crates-io))

(define-public crate-libchip8-0.1.0 (c (n "libchip8") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 2)) (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 2)))) (h "1nkh1wr8rildibg88wri46pwhybn6g950fgq7gia2s3ap47bj6wv")))

(define-public crate-libchip8-0.1.1 (c (n "libchip8") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 2)) (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 2)))) (h "159dbqg6v6mkk86hi6n3gxvb749dwgk6m6k3bg488xvhi7g3w0pf")))

(define-public crate-libchip8-0.1.2 (c (n "libchip8") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "log4rs") (r "^0.8") (d #t) (k 2)) (d (n "minifb") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)) (d (n "structopt") (r "^0.2") (d #t) (k 2)))) (h "06mwbdkcb6m9mcn1h0sq25psq3qgia21lpzz056j0kkl72fcj3ad")))

