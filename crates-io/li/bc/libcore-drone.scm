(define-module (crates-io li bc libcore-drone) #:use-module (crates-io))

(define-public crate-libcore-drone-0.10.0 (c (n "libcore-drone") (v "0.10.0") (d (list (d (n "drone-core") (r "^0.10.0") (d #t) (k 0)))) (h "1igk918p9nj3ckk57xbxp6xcbn13iaaiyrw0wr15mm68wxqc1hng")))

(define-public crate-libcore-drone-0.11.0 (c (n "libcore-drone") (v "0.11.0") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)))) (h "17fzvhr5b0zc79l41gisisxsjmpr7mhz15vy9y76rcpn9800875f")))

(define-public crate-libcore-drone-0.11.1 (c (n "libcore-drone") (v "0.11.1") (d (list (d (n "drone-core") (r "^0.11.0") (d #t) (k 0)))) (h "13z5453icd0g34pvg9cdhzmqfxcl3k7972p5mpbva64mi4fzfc5h")))

