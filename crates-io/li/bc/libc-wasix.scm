(define-module (crates-io li bc libc-wasix) #:use-module (crates-io))

(define-public crate-libc-wasix-0.2.147 (c (n "libc-wasix") (v "0.2.147") (d (list (d (n "rustc-std-workspace-core") (r "^1.0.0") (o #t) (d #t) (k 0)))) (h "0bw4a5dn2cqw88pn2i3x314hv8cd7kfdm729x7wcxrbr0wrsr1nh") (f (quote (("use_std" "std") ("std") ("rustc-dep-of-std" "align" "rustc-std-workspace-core") ("extra_traits") ("default" "std") ("const-extern-fn") ("align"))))))

