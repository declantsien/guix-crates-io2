(define-module (crates-io li bc libcapstone-sys) #:use-module (crates-io))

(define-public crate-libcapstone-sys-0.3.0 (c (n "libcapstone-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.22.1") (d #t) (k 1)))) (h "0rmng2a8hfwaqbx89dmwf267q2d1c8b3am2p0g97h0qsm6djzcdg")))

(define-public crate-libcapstone-sys-0.3.1 (c (n "libcapstone-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.22.1") (d #t) (k 1)))) (h "0lk241dxkl8xlldn6knb8i3xfxkamzhkqga09y2lcc3pvv981s1h")))

(define-public crate-libcapstone-sys-0.3.2 (c (n "libcapstone-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.22.1") (d #t) (k 1)))) (h "028qwiyi5z7ymggi9bj88mhw4sc8gq76jy7df62i37a4xyvjrwx3")))

