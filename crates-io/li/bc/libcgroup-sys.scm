(define-module (crates-io li bc libcgroup-sys) #:use-module (crates-io))

(define-public crate-libcgroup-sys-0.1.1 (c (n "libcgroup-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0shk8f6a2c2m30gzmn9043zh73qm68kg7cg4kjyz9chs5bjpfvah")))

(define-public crate-libcgroup-sys-0.2.0 (c (n "libcgroup-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "procinfo") (r "^0.2.1") (d #t) (k 2)))) (h "0fmaqslq508i4b204qbamw5669222fa7r8yag8x6fjffbwns9nm6")))

(define-public crate-libcgroup-sys-0.3.0 (c (n "libcgroup-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "procinfo") (r "^0.2") (d #t) (k 2)))) (h "1mkf2c40w04xsz72269s3rj7mryamlb6flf0qa8nag61j3ywhvrf")))

