(define-module (crates-io li bc libcalcver) #:use-module (crates-io))

(define-public crate-libcalcver-0.2.0-beta.2 (c (n "libcalcver") (v "0.2.0-beta.2") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "1yj6f3k2yvsv2jvfa258bnmzpgxrv9nv4avcjq6wdm2zjin5cqai") (y #t)))

(define-public crate-libcalcver-0.2.0-beta.3 (c (n "libcalcver") (v "0.2.0-beta.3") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "18xzb84d1ic3f6nhw8y9lfp5nsxylqj80jgpah00arji0vcd9zzz") (y #t)))

(define-public crate-libcalcver-0.2.0-beta.4 (c (n "libcalcver") (v "0.2.0-beta.4") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0z9vj0mavfsybaj1qx82xgwgriflnsaw9dl0jfad6r6gskxmr1pm") (y #t)))

(define-public crate-libcalcver-0.2.0-beta.5 (c (n "libcalcver") (v "0.2.0-beta.5") (d (list (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)) (d (n "semver") (r "^0.9.0") (d #t) (k 0)))) (h "0fgql7giqvm067j8v5dksnq5d4r7xppmhl0ixh688jh135aj00kc") (y #t)))

