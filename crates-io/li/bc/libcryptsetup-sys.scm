(define-module (crates-io li bc libcryptsetup-sys) #:use-module (crates-io))

(define-public crate-libcryptsetup-sys-0.1.0 (c (n "libcryptsetup-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1pi6kgvn15al8yj0zzpcjcp97slwz84nvf4kh39kh4ff2dmvxf08")))

(define-public crate-libcryptsetup-sys-0.2.0 (c (n "libcryptsetup-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1padjfdpyrqiyryj0zha3znav1f7ggdlqm1zaa5s251mnnayl6ij") (l "cryptsetup")))

(define-public crate-libcryptsetup-sys-0.1.2 (c (n "libcryptsetup-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "15pmhirnkphnlkbm0qsp70jh5jbm9cdsbzs278kfl5kkziyv7jpq") (l "cryptsetup")))

(define-public crate-libcryptsetup-sys-0.3.0-beta1 (c (n "libcryptsetup-sys") (v "0.3.0-beta1") (d (list (d (n "libc") (r "^0.2.70") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "00x6g6c9hpd2mk9qfch1fx3sqi7jsc1apwf1vrf856vp4wcg4a7r") (l "cryptsetup")))

(define-public crate-libcryptsetup-sys-0.3.0 (c (n "libcryptsetup-sys") (v "0.3.0") (d (list (d (n "libc") (r "^0.2.132") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "10nrm9mimgpigk415fn3v5q0gdmjsnn9cirzd75fbvp6wzz7lcg6") (l "cryptsetup")))

