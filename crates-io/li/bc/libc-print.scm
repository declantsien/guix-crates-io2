(define-module (crates-io li bc libc-print) #:use-module (crates-io))

(define-public crate-libc-print-0.1.0 (c (n "libc-print") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "0gd5nyxqwdhcqlm3nkp9ch8cjc8qwarzn0vg8395ff25jbi5kprf")))

(define-public crate-libc-print-0.1.1 (c (n "libc-print") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "0idhli0jpm9xr4ap6dbn20jcb25iragi4an347paqbdkr3b3s6l0")))

(define-public crate-libc-print-0.1.2 (c (n "libc-print") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "1i96jiaifkzpb6ba7r8rn8nszhq17vr52njhjlidr6jivr16q2q3")))

(define-public crate-libc-print-0.1.3 (c (n "libc-print") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "165ykr62m5fqbm47dijdmzxp9rxzz1a6apgxc6wqqmnyblsbl8iq")))

(define-public crate-libc-print-0.1.4 (c (n "libc-print") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "1b15x13cvjqdyg0azab3wk3gvi6n6gs1bgc0aqw9s5cr7f0l6dll")))

(define-public crate-libc-print-0.1.5 (c (n "libc-print") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "1252bwdin63lxr4azs10q5fakv37iazfbnmqc4hwg99lkc59m5gc")))

(define-public crate-libc-print-0.1.6 (c (n "libc-print") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "0pal6a794f15p2dgck1rgkw0ldpsif9hlsyc8pn56w2n24nlgkqq")))

(define-public crate-libc-print-0.1.7 (c (n "libc-print") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.44") (d #t) (k 0)))) (h "1fpgdw9wmwpb89b5qhmmbliy02val8g6q2fcx2hc5lvxxlajirgp")))

(define-public crate-libc-print-0.1.8 (c (n "libc-print") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.62") (d #t) (k 0)))) (h "0jadyzhikrkzgzqqr58q9ffw7wrzl6g0j6jb5bkkvrj9sdgssxv1")))

(define-public crate-libc-print-0.1.9 (c (n "libc-print") (v "0.1.9") (d (list (d (n "libc") (r "^0.2.62") (k 0)))) (h "014yvvw6kq82pjjs4qaysm7qvvwj7nvg8gsvwjg9vsvn9p268ask")))

(define-public crate-libc-print-0.1.10 (c (n "libc-print") (v "0.1.10") (d (list (d (n "libc") (r "^0.2.62") (k 0)))) (h "1yyvkdi7gqkfwa6yj1zjh51yz42mabp55m2yakb866npcnprsqmw")))

(define-public crate-libc-print-0.1.11 (c (n "libc-print") (v "0.1.11") (d (list (d (n "libc") (r "^0.2.62") (k 0)))) (h "1sh4l815w7zxg8w17fvwj63y421sjqxxrdamzwyvg90n6mr70phv")))

(define-public crate-libc-print-0.1.12 (c (n "libc-print") (v "0.1.12") (d (list (d (n "libc") (r "^0.2.62") (k 0)))) (h "07m60jw407rlbf5j13x9qpgpl8lv39l98m9yiqj13kvj8jvjz7qq")))

(define-public crate-libc-print-0.1.13 (c (n "libc-print") (v "0.1.13") (d (list (d (n "libc") (r "^0.2.69") (k 0)))) (h "0cjvz622b9bmf32q3mzmxv9ddxfdla6z2v647v8f3qx7lci9kmji")))

(define-public crate-libc-print-0.1.14 (c (n "libc-print") (v "0.1.14") (d (list (d (n "libc") (r "^0.2.77") (k 0)))) (h "1jm75ckpxj4abgqcz0mh5w6fwqb8l0w6b6d0v6ziqj4dbi9j60z7")))

(define-public crate-libc-print-0.1.15 (c (n "libc-print") (v "0.1.15") (d (list (d (n "libc") (r "^0.2.81") (k 0)))) (h "1zhd57gc8d5i7j2mksmq8w388m84w7h6kl9z13h3fhwvf32bzdb1")))

(define-public crate-libc-print-0.1.16 (c (n "libc-print") (v "0.1.16") (d (list (d (n "libc") (r "^0.2.84") (k 0)))) (h "0cvgmb3gn2fqx9gb7pcyr0bw0kxm28y876qpn3k9ybwn7r2pa7z4")))

(define-public crate-libc-print-0.1.17 (c (n "libc-print") (v "0.1.17") (d (list (d (n "libc") (r "^0.2.101") (k 0)))) (h "1al0fdla92w2xj9wi5y8y3f9jg6nprvzmmlh6q40p1in5svxzh4y")))

(define-public crate-libc-print-0.1.18 (c (n "libc-print") (v "0.1.18") (d (list (d (n "libc") (r "^0.2.120") (k 0)))) (h "0sxzs02n0hk9msvnxxrjyg0rkqydklxv79im2rsa22bym8vzkk2p")))

(define-public crate-libc-print-0.1.19 (c (n "libc-print") (v "0.1.19") (d (list (d (n "libc") (r "^0.2.120") (k 0)))) (h "0vaxk3hms5r01zmjplz45xhpk73kabjxffdhh254c8srd6aci025")))

(define-public crate-libc-print-0.1.20 (c (n "libc-print") (v "0.1.20") (d (list (d (n "libc") (r "^0.2.126") (k 0)))) (h "02gdmj7b5mpnd0ync1b3bz1q1rah7fyrniij3grrd6frxcd4jx55")))

(define-public crate-libc-print-0.1.21 (c (n "libc-print") (v "0.1.21") (d (list (d (n "libc") (r "^0.2.139") (k 0)))) (h "04bcx06gb04v0csgyzsvd1v9wvmv26xwbipmpcblgfnrigasbkh6")))

(define-public crate-libc-print-0.1.22 (c (n "libc-print") (v "0.1.22") (d (list (d (n "libc") (r "^0.2.148") (k 0)))) (h "1x9jh3sx7r2nkhaw32r1xjnv0xh77cxgx2aymzd7kivm44g12zy1")))

(define-public crate-libc-print-0.1.23 (c (n "libc-print") (v "0.1.23") (d (list (d (n "libc") (r "^0.2.148") (k 0)))) (h "1c6lvpciaq5gj82wzgm5y9r119pia11lhdvvypx3b7mlilh619m4")))

