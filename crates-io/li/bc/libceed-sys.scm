(define-module (crates-io li bc libceed-sys) #:use-module (crates-io))

(define-public crate-libceed-sys-0.8.0 (c (n "libceed-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "16cacxp6m9vxzwvp854y8f33y73cdaviq8nsh6m1s6y0b1g7w28q") (f (quote (("system") ("static") ("default" "static")))) (l "libceed-sys")))

(define-public crate-libceed-sys-0.9.0 (c (n "libceed-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "13gp24jfzxx4w5wgfnk9zi2531s29izs6w2rhqjg51q1k6568ffd") (f (quote (("system") ("static") ("default" "static")))) (l "libceed-sys")))

(define-public crate-libceed-sys-0.11.0 (c (n "libceed-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0npjcaxqqzrkf0xhh61z4vnpiwjb2hgn9m9g3mvxwrixxw4i8w11") (f (quote (("system") ("static") ("default" "static")))) (l "libceed-sys")))

(define-public crate-libceed-sys-0.12.0 (c (n "libceed-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.61.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)) (d (n "version-sync") (r "^0.9.2") (d #t) (k 2)))) (h "0z1dy456haal69569wsbjid7cc8dnvpd1sm3lnrhzwlmdr72dygq") (f (quote (("system") ("static") ("default" "static")))) (l "libceed-sys")))

