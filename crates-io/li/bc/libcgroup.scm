(define-module (crates-io li bc libcgroup) #:use-module (crates-io))

(define-public crate-libcgroup-0.1.0 (c (n "libcgroup") (v "0.1.0") (d (list (d (n "libcgroup-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1mbbhp0sjh7wnnx1zz8jn8bxizrvns96x2i85hfkkrwlb8j88a0q")))

(define-public crate-libcgroup-0.2.0 (c (n "libcgroup") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libcgroup-sys") (r "^0.2.0") (d #t) (k 0)))) (h "09973sbqqclfx8gw97p8ka6v722wlnd06r1hc4wm43bkpij1p4rj") (f (quote (("sudo"))))))

(define-public crate-libcgroup-0.3.0 (c (n "libcgroup") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libcgroup-sys") (r "^0.3") (d #t) (k 0)))) (h "116nm10fxf24yq96jyiclx4wj65ll2s96id84abkq4n0iyb73xsj") (f (quote (("sudo"))))))

