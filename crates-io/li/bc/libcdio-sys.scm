(define-module (crates-io li bc libcdio-sys) #:use-module (crates-io))

(define-public crate-libcdio-sys-0.1.0 (c (n "libcdio-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "1yic6ad314n15hh1ivz471gh9cfq7xjnjl6d9hf7ywk0iv9hhr47") (f (quote (("udf") ("iso9660") ("default" "cdio" "iso9660" "udf") ("cdio"))))))

(define-public crate-libcdio-sys-0.1.1 (c (n "libcdio-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.35") (d #t) (k 1)))) (h "0g6npxgqy4bbz5mrjnmkwwj4a3imk7pavlav7l2kqz79dzm0cfv7") (f (quote (("udf") ("iso9660") ("default" "cdio" "iso9660" "udf") ("cdio"))))))

(define-public crate-libcdio-sys-0.2.0 (c (n "libcdio-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.36") (d #t) (k 1)))) (h "09wyyar0n1jm422mvpkpvva3sn8843ql5qd87a8i58kzrihfdhbq") (f (quote (("udf") ("iso9660") ("default" "cdio" "iso9660" "udf") ("cdio"))))))

(define-public crate-libcdio-sys-0.2.2 (c (n "libcdio-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)))) (h "1hk4ddj31bcsv76l5ngf30kzp1wr50fq75xhndir0kj7jg5fab4z") (f (quote (("udf") ("iso9660") ("docsrs-workaround") ("default" "cdio" "iso9660" "udf") ("cdio"))))))

(define-public crate-libcdio-sys-0.3.0 (c (n "libcdio-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)))) (h "19mz4p5xa4wvcy2mhq9awppxg6zxlgbiamlbnv6zpj59y7krcisr") (f (quote (("udf") ("iso9660") ("default" "cdio" "iso9660" "udf") ("cdio"))))))

(define-public crate-libcdio-sys-0.4.0 (c (n "libcdio-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1hlvx3zpw684vrqnrlmqbwf6lycmv9hwb75c1aj84s1b8kwbybpa") (f (quote (("udf") ("iso9660") ("default" "cdio" "iso9660" "udf") ("cdio"))))))

(define-public crate-libcdio-sys-0.4.1 (c (n "libcdio-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "0givniial8w10gdw28pkd3mp5qckjqwh7643s2bd2681c7h88mib") (f (quote (("udf") ("iso9660") ("default" "cdio" "iso9660" "udf") ("cdio"))))))

(define-public crate-libcdio-sys-0.4.2 (c (n "libcdio-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)))) (h "1i4kabzc1p2g7kw54qsk5ah0gws2d5cy9yhrh0l6215dnin7kv2g") (f (quote (("udf") ("iso9660") ("default" "cdio" "iso9660" "udf") ("cdio"))))))

(define-public crate-libcdio-sys-0.5.0 (c (n "libcdio-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)))) (h "1zm1nn1m9zd6a537hvsbaaa4wy8y2k6m5rmm71kgc7s68rd02c27") (f (quote (("udf") ("iso9660") ("default" "cdio" "iso9660" "udf") ("cdio"))))))

