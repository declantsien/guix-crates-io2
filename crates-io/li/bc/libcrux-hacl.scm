(define-module (crates-io li bc libcrux-hacl) #:use-module (crates-io))

(define-public crate-libcrux-hacl-0.0.2-pre.1 (c (n "libcrux-hacl") (v "0.0.2-pre.1") (d (list (d (n "bindgen") (r "^0.68") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libcrux-platform") (r "=0.0.2-pre.1") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1613bzzxvqrmsm39h4l7i66713c0393xx3frmcc1vya6rhmi6wzi") (f (quote (("bindings" "bindgen"))))))

(define-public crate-libcrux-hacl-0.0.2-pre.2 (c (n "libcrux-hacl") (v "0.0.2-pre.2") (d (list (d (n "bindgen") (r "^0.69") (o #t) (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libcrux-platform") (r "=0.0.2-pre.2") (d #t) (k 1)) (d (n "wasm-bindgen-test") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0s0bvcc651825dbicrqpp1krprkvni95ayq700kw5ickwhf5icjj") (f (quote (("bindings" "bindgen"))))))

