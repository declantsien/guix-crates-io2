(define-module (crates-io li bc libcerror-sys) #:use-module (crates-io))

(define-public crate-libcerror-sys-0.2.0 (c (n "libcerror-sys") (v "0.2.0") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "fs_extra") (r "^1.1.0") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.0") (d #t) (k 1)))) (h "0mb147pvzyhib99jj83zdh02nzhghs393zw1j0ialdqbr2mngkrx") (f (quote (("dynamic_link") ("default")))) (l "libcerror")))

(define-public crate-libcerror-sys-0.2.1 (c (n "libcerror-sys") (v "0.2.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.1") (d #t) (k 1)))) (h "12m7dmxi5nk0kwz58x5p91a8ylbr5fvjnp78m35xhnmw4x36cywh") (f (quote (("dynamic_link") ("default"))))))

(define-public crate-libcerror-sys-0.2.2 (c (n "libcerror-sys") (v "0.2.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.1") (d #t) (k 1)))) (h "1waq4qs3wmjzilgfgri07452n8rapqdqvg5zkgca3c5iljpq77mz") (f (quote (("dynamic_link") ("default")))) (y #t)))

(define-public crate-libcerror-sys-0.2.3 (c (n "libcerror-sys") (v "0.2.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.1") (d #t) (k 1)))) (h "14d6ywp0mxyhqnxgpllqhxlmcij8fnww5ylhrr9lrqyr774i1g05") (f (quote (("dynamic_link") ("default"))))))

(define-public crate-libcerror-sys-0.2.4 (c (n "libcerror-sys") (v "0.2.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.4") (d #t) (k 1)))) (h "0r4cb4gbqyxwknd8wvk3zvnvj357cbi94bha12qv67rfdnsa301k") (f (quote (("dynamic_link") ("default"))))))

(define-public crate-libcerror-sys-0.2.5 (c (n "libcerror-sys") (v "0.2.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 1)) (d (n "libyal-rs-common-build") (r "^0.2.5") (d #t) (k 1)))) (h "1k8vqjcysv0kjymnzxbgblgac5dvdahlv0lmydi047hnspvgjiv3") (f (quote (("dynamic_link") ("default"))))))

