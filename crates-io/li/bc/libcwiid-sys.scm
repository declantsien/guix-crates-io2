(define-module (crates-io li bc libcwiid-sys) #:use-module (crates-io))

(define-public crate-libcwiid-sys-0.1.0 (c (n "libcwiid-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0xxy17yiwwc74d99rrdz5vsmckx90d4mc66pcq8g5mnjh71g2ady")))

(define-public crate-libcwiid-sys-0.1.1 (c (n "libcwiid-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1yi7jv8zc3qabbb87n5nr7ig3210vj7kavzjxqh1mpwc3rm1lkgh")))

(define-public crate-libcwiid-sys-0.1.2 (c (n "libcwiid-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0ybq1mgaq1ff1mqbb7hddrxv0lgk5v6p1nsnbnvhc421jf2a9zk2")))

(define-public crate-libcwiid-sys-0.1.3 (c (n "libcwiid-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0f3k120jnhxx3ggbj0f9yzb57kjfvhkg117w30cal2shnid4v158") (y #t)))

(define-public crate-libcwiid-sys-0.1.4 (c (n "libcwiid-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "062mv18s5snxng18d4qmgsjk5p23hzvix7n2s0ycq1b6qxgslwxf")))

(define-public crate-libcwiid-sys-0.1.5 (c (n "libcwiid-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)))) (h "1nr5xw2kpyljwkbbxv81rpxlydyb94xxp44isisx42pv0vpvghsf")))

(define-public crate-libcwiid-sys-0.1.6 (c (n "libcwiid-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.77") (f (quote ("parallel"))) (d #t) (k 1)))) (h "10vdq17176ld30z8cl19lygyx0v0c6qv8fhr0sakkxpj2nkbw1jp")))

(define-public crate-libcwiid-sys-0.1.7 (c (n "libcwiid-sys") (v "0.1.7") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "03x5y3l7z0nvv1gn2ix21mvrh8rk5labcdw0dk2lp1dkzd11k5jq")))

(define-public crate-libcwiid-sys-0.1.8 (c (n "libcwiid-sys") (v "0.1.8") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "11hxy0k6qqjinp9fxaa3s6a2hdgxzmairxs5hgf9pr7aqdiirafx")))

(define-public crate-libcwiid-sys-0.1.9 (c (n "libcwiid-sys") (v "0.1.9") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "0skz02zxrdz2279fxkk1ck62zc161gmg8c7zzjpw9z6wx7dijsri")))

(define-public crate-libcwiid-sys-0.1.10 (c (n "libcwiid-sys") (v "0.1.10") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "1mcwhavk9489kkyss8rmz5rsd0q9vmxcc0f8sgv8i6rjmgl74mm6")))

(define-public crate-libcwiid-sys-0.1.11 (c (n "libcwiid-sys") (v "0.1.11") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "07k5cmf7yhy0knnp8wpvny4r9g30mma24gsvi3x3hr41a9my9kzp")))

(define-public crate-libcwiid-sys-0.1.12 (c (n "libcwiid-sys") (v "0.1.12") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "1cfs15803la68w9inkv38am8mjgz63fdpd9agadf2a8f18jgac9j")))

(define-public crate-libcwiid-sys-0.1.13 (c (n "libcwiid-sys") (v "0.1.13") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "0ryy1s48yf3mi2cynkdv5xn821p238pd5454wwvgmsybllvsalnc")))

(define-public crate-libcwiid-sys-0.1.14 (c (n "libcwiid-sys") (v "0.1.14") (d (list (d (n "cc") (r "^1.0.77") (d #t) (k 1)))) (h "0s9w2ijvb75vp5xq0bqsgww7hsmwm1d7arkvq06lhx4z9m2j8qfl")))

(define-public crate-libcwiid-sys-0.1.15 (c (n "libcwiid-sys") (v "0.1.15") (h "0l5drg5nd0mmfj31s0r6r3nkn8hvx814jj8m5s74ndfk3ny30y79")))

(define-public crate-libcwiid-sys-0.1.16 (c (n "libcwiid-sys") (v "0.1.16") (h "1d4ym6jfi374a10nxnc02dlggkkj7wxpsl7f5xzf6k2dn734j5fb")))

(define-public crate-libcwiid-sys-0.1.17 (c (n "libcwiid-sys") (v "0.1.17") (h "1352966kc6456ck3z1qcvj4vqhkzxnnzw683l9j5fa5c3a5m2jz6")))

(define-public crate-libcwiid-sys-0.1.18 (c (n "libcwiid-sys") (v "0.1.18") (h "1mc9mhvs9zp7l7zsm0z1ggn0h14f3v5dhf8x4wgaccbxvk3cgj2s")))

