(define-module (crates-io li bc libcratesio) #:use-module (crates-io))

(define-public crate-libcratesio-0.0.0 (c (n "libcratesio") (v "0.0.0") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "0iwrfwl02qf0wqhp4khf49s8nbj7shc3nhmwn4sxb9lrc49ixp7c")))

(define-public crate-libcratesio-0.0.1 (c (n "libcratesio") (v "0.0.1") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "16nax3ib3zzynmin69mc8mr25rlv237flf2vrzbd35qh070rvp17")))

(define-public crate-libcratesio-0.0.2 (c (n "libcratesio") (v "0.0.2") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.5") (d #t) (k 0)) (d (n "serde") (r "^0.9") (d #t) (k 0)) (d (n "serde_derive") (r "^0.9") (d #t) (k 0)) (d (n "serde_json") (r "^0.9") (d #t) (k 0)))) (h "1bj062r87mx994ri7zm3pya4hvga5gwdv25clnikrs7xzdlvw555")))

(define-public crate-libcratesio-0.0.3 (c (n "libcratesio") (v "0.0.3") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xbr4yyh76wngmvajy9znxphn55y2rhkq8mg9b5qqw85kfpzkd9w")))

(define-public crate-libcratesio-0.0.4 (c (n "libcratesio") (v "0.0.4") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1awxlwwd836brl5xxd255sqlklibfss82rq9vlbzbs7zmwgqlpyk")))

(define-public crate-libcratesio-0.0.5 (c (n "libcratesio") (v "0.0.5") (d (list (d (n "chrono") (r "^0.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ns8221brf1v059r4symbk0cv9bw39ba8z0pvp3h8ryhy9kirhjm")))

(define-public crate-libcratesio-0.0.6 (c (n "libcratesio") (v "0.0.6") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "10j446pnhl7g94gpbwczsnskc8w9x93ybjf9dni3zs1mk1iik6sx")))

(define-public crate-libcratesio-0.0.7 (c (n "libcratesio") (v "0.0.7") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19m3cdzzg6g1vp61xkqg777i2m4qg4v9zsq7grzwm12n9wnzjvzl")))

(define-public crate-libcratesio-0.0.8 (c (n "libcratesio") (v "0.0.8") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "reqwest") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vhqkv986qq0v0vi9mlcwrfbk6af0hr4r3fy8s0qgb4851m1a3yh")))

