(define-module (crates-io li bc libc-test) #:use-module (crates-io))

(define-public crate-libc-test-0.2.87 (c (n "libc-test") (v "0.2.87") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.87") (k 0)))) (h "0c6cd0nld5hqm7csd6yvn9gj2y6wpilbprj49nr87xfw0hy5g20m") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.88 (c (n "libc-test") (v "0.2.88") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (k 0)))) (h "1f91w0m26ahn3jpwgyhq4l46sdg95m17f7xyxcib6ciw7vqbm46w") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.89 (c (n "libc-test") (v "0.2.89") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (k 0)))) (h "05jhvv1mnvncqivcg8a83hvy2dl7i24243kbjvyr8zwvbkpmzz1q") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.90 (c (n "libc-test") (v "0.2.90") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.88") (k 0)))) (h "1ncgd131p5dx4wiqclvnrylx01nypg21nyim9zpq61y7lpm6p7d7") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.91 (c (n "libc-test") (v "0.2.91") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.91") (k 0)))) (h "12qn5dwfqxlrflva2m65ln634l5d3k82my2pmrn812b0hjcih05i") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.92 (c (n "libc-test") (v "0.2.92") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.92") (k 0)))) (h "0sr14xr5r43p3dhwj7h9xhdk9c476fhglzvpfi98p3g2nkigi39v") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.93 (c (n "libc-test") (v "0.2.93") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.93") (k 0)))) (h "00a04m6ywj9fn35sk3snpjfpicgbkmc9qya8f5vnrkwjm1yvbfd9") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.94 (c (n "libc-test") (v "0.2.94") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.94") (k 0)))) (h "0bpkcbkdnaclfh5wbsn8z7zvbyqd5bgfl0jwbb21c5hzv3qgpwax") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.95 (c (n "libc-test") (v "0.2.95") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.95") (k 0)))) (h "02g0sw9gdmviqqmd0378mp136xigc4wh6n6m5cm2vaahmj3j3hzh") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.96 (c (n "libc-test") (v "0.2.96") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.96") (k 0)))) (h "0lcb4vjrpmvxmz25gkicj6v666ci8gzycr5hc1zlx90m119zq9hq") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.97 (c (n "libc-test") (v "0.2.97") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.97") (k 0)))) (h "0a2dxy5k34cw6zrngwrr5hzg4ahxbdpwsni1kksqlkdpdb9kj58w") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.98 (c (n "libc-test") (v "0.2.98") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.98") (k 0)))) (h "143zrw12vc4li188npjjfdx090c4smpgsigsqbjmnm47rhzvr0qm") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.99 (c (n "libc-test") (v "0.2.99") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.99") (k 0)))) (h "0fga2n35yphvkgkwi8v8d2vqac22h93lqn55dshb6a67bl4zlbf2") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.100 (c (n "libc-test") (v "0.2.100") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.100") (k 0)))) (h "0r52y9sdg2k95cyq61pla7ijsk39wc67wrm8vb754389n7i8lyns") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.102 (c (n "libc-test") (v "0.2.102") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.102") (k 0)))) (h "13azyzhg0gz16h0hl08zswdir3pakfglfk1f6pnc2r35c4jgvi2n") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.104 (c (n "libc-test") (v "0.2.104") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4") (d #t) (k 1)) (d (n "libc") (r "^0.2.104") (k 0)))) (h "1djdcwyn91vdg7chs6z2pakg8qwq6rqf1b1ks72xbj6ghhj12r96") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.105 (c (n "libc-test") (v "0.2.105") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.105") (k 0)))) (h "0lzlcyaqpn74klpn174l87nc3ciz18ymfkm4vy9bzdrngdq752s2") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.107 (c (n "libc-test") (v "0.2.107") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.107") (k 0)))) (h "0kph4fa1905bdiixwvv2s5ycgp1cr6hbb2cymr7k4avr1cm6i7bi") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.108 (c (n "libc-test") (v "0.2.108") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.2") (d #t) (k 1)) (d (n "libc") (r "^0.2.108") (k 0)))) (h "16bg5pskfsj5rsjvp8c3l4rrg7bfv63dmdi0cvqpb2841fvzybxj") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.128 (c (n "libc-test") (v "0.2.128") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.128") (k 0)))) (h "0pc9r0s75rpiq39da3xi1y4znjxyympwazz8cakd2y9x3gwwhy0a") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.129 (c (n "libc-test") (v "0.2.129") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.129") (k 0)))) (h "0aj9j2942kpvafgl013skc5lrsdf7cn3fyg4yzs5kh02233xzx7r") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.132 (c (n "libc-test") (v "0.2.132") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.132") (k 0)))) (h "1v2rr9k371wx04dk73q41makm7wdpqgdi7dpws7h21nzg24kasxx") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.133 (c (n "libc-test") (v "0.2.133") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.133") (k 0)))) (h "013b4hxcj1xi6qccv4b2vafh5sa4g55kjfsnz3ka9qzyhgzw8h4j") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.134 (c (n "libc-test") (v "0.2.134") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.134") (k 0)))) (h "0n6iqh53b607df7n2klbdpp5np7g9q6kcnckkckm67fqr23y1x2c") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.135 (c (n "libc-test") (v "0.2.135") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.135") (k 0)))) (h "13fr45sgdcfpqfgjnck57wvxslhhnf3p4s0vqkk2pf71km37mnqr") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.136 (c (n "libc-test") (v "0.2.136") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.136") (k 0)))) (h "0f1phmf4iirdkhrxyzr1h8lnvi1r3k0y7gps2x3bkil8wxcrd9a7") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.137 (c (n "libc-test") (v "0.2.137") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.137") (k 0)))) (h "0ahximhwx7wij410hfgmwkbinyjl5grj17zq3x00vv3fbmv8fhzz") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.138 (c (n "libc-test") (v "0.2.138") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.138") (k 0)))) (h "03hiiazg68nwjp2jlvh48wr3kfpzyzwrj24zxp6j2x9ckissz8hc") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.139 (c (n "libc-test") (v "0.2.139") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.139") (k 0)))) (h "1xkzkyvqiwcjacynlrnkw3766psmkzlh1xgghy2szwl6020gif98") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.140 (c (n "libc-test") (v "0.2.140") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.140") (k 0)))) (h "1ifk4vnk7gmpd7a7j3ldj6c8x4b27hsg32rvwhylgn6r3c1vk8m1") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.141 (c (n "libc-test") (v "0.2.141") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.141") (k 0)))) (h "1l0mascj17cvqqyrhv8f0hvs42vbxm2wrbnaarlf872dvzk2hhil") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.142 (c (n "libc-test") (v "0.2.142") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.122") (k 0)))) (h "0iap53c629f5h5mpqi164jbl30d9ihp9bpn3y1lsds7hfvx08fs0") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.143 (c (n "libc-test") (v "0.2.143") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.143") (k 0)))) (h "1210bhfjs3qhzjp9jk1zz95mgsmnnw4b7jci5rw80n91c65g4kkk") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.144 (c (n "libc-test") (v "0.2.144") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.144") (k 0)))) (h "122wsdh5lczpbhbw5dcrh7xyfzsn2px727xqszwxmbhm5jdvc37x") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.145 (c (n "libc-test") (v "0.2.145") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.145") (k 0)))) (h "05gc1pfa0nnzmfadigdym1znv0l9c9jrpy8ardf3l1kayp6xmv9j") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.147 (c (n "libc-test") (v "0.2.147") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (k 0)))) (h "1q0b6p9yam8gq14dzsh0l2x0y7sib2lz0nvhy19c993inddjwci0") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.149 (c (n "libc-test") (v "0.2.149") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.149") (k 0)))) (h "02c2b83ak5laifrfr609qwaz593x8wxhyc48f7dzgq3sks8n4s6x") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.150 (c (n "libc-test") (v "0.2.150") (d (list (d (n "cc") (r "^1.0.61") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.150") (k 0)))) (h "0by7fc50kjns4m8xql2z1k5h1p8gp00jbz330wlal3msmf033gry") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.151 (c (n "libc-test") (v "0.2.151") (d (list (d (n "cc") (r "=1.0.83") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.151") (k 0)))) (h "0gi548zdyg8yz3115q91hra7g9gz2nbm2ja56hww44k53b132wd3") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.152 (c (n "libc-test") (v "0.2.152") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.152") (k 0)))) (h "0b7dlhc40rhqvf5cjaffjrh0r8j6jzv2j1yawz3nwhdxisg4hkyv") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.153 (c (n "libc-test") (v "0.2.153") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.153") (k 0)))) (h "1513435dcnkq8jzqxx2y8ag0xrl6ihkclailzq17qnf1736xn6ng") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

(define-public crate-libc-test-0.2.154 (c (n "libc-test") (v "0.2.154") (d (list (d (n "cc") (r "^1.0.83") (d #t) (k 1)) (d (n "ctest2") (r "^0.4.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.154") (k 0)))) (h "021rrm9lac1c2dgvaw5lr82575fbfnz41swkxq4rh7ngkwp6ijkc") (f (quote (("std" "libc/std") ("extra_traits" "libc/extra_traits") ("default" "std") ("align" "libc/align"))))))

