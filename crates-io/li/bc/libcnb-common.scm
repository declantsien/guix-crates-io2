(define-module (crates-io li bc libcnb-common) #:use-module (crates-io))

(define-public crate-libcnb-common-0.15.0 (c (n "libcnb-common") (v "0.15.0") (d (list (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.48") (d #t) (k 0)) (d (n "toml") (r "^0.8.0") (d #t) (k 0)))) (h "0a587bbh6xrz5fr7dijk4rm7jglpv475sa4a3mn3357ji5qcgi2k") (r "1.64")))

(define-public crate-libcnb-common-0.16.0 (c (n "libcnb-common") (v "0.16.0") (d (list (d (n "serde") (r "^1.0.192") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0w06b9anwkw08iiqj27ffs2x0y6lh608bfgmfwqcz99pj6wkhmfh") (r "1.74")))

(define-public crate-libcnb-common-0.17.0 (c (n "libcnb-common") (v "0.17.0") (d (list (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)) (d (n "toml") (r "^0.8.8") (d #t) (k 0)))) (h "0y07g7pmh8sc7mnjrqav14szlrmj9g1ndkkn37zh8c1msiydxzi8") (r "1.74")))

(define-public crate-libcnb-common-0.18.0 (c (n "libcnb-common") (v "0.18.0") (d (list (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "0z89ys8wk65sxmzs388s6mhhsxzhwyk0w4m1ajdz9il6xdkgwjcv") (r "1.76")))

(define-public crate-libcnb-common-0.19.0 (c (n "libcnb-common") (v "0.19.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)) (d (n "toml") (r "^0.8.10") (d #t) (k 0)))) (h "06yazc6g92pgg6aypcllcni6iw48p81f1118w6npdpb2c42z5azk") (r "1.76")))

(define-public crate-libcnb-common-0.20.0 (c (n "libcnb-common") (v "0.20.0") (d (list (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "0smgj2ik3i5md9fcldp5bfc7jpnc16j3s71sl1jd4q1j64x75nzr") (r "1.76")))

(define-public crate-libcnb-common-0.21.0 (c (n "libcnb-common") (v "0.21.0") (d (list (d (n "serde") (r "^1.0.199") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)) (d (n "toml") (r "^0.8.12") (d #t) (k 0)))) (h "1qxncdlzjsm9afas9xn5awx57iyiqzs981j58b5g2l8v71vvsmm3") (r "1.76")))

