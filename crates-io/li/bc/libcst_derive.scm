(define-module (crates-io li bc libcst_derive) #:use-module (crates-io))

(define-public crate-libcst_derive-0.1.0 (c (n "libcst_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "03bwd8qdkj67j14ndv3bms9kcfd7sdgfq1bhrmcg4xx41g2rf0aj")))

(define-public crate-libcst_derive-1.1.0 (c (n "libcst_derive") (v "1.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1sdbb89xdacwfyxxf397lr94aznzn4d3apg6zb3gw2xjh8i2azvd")))

(define-public crate-libcst_derive-1.2.0 (c (n "libcst_derive") (v "1.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "04aci77nlcih7grif1f1x6ry1m39d7nllq9svkmj4r1lv4y2zg8x")))

(define-public crate-libcst_derive-1.3.0 (c (n "libcst_derive") (v "1.3.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0p3q9mppqzzn2qhkn968q8180h49qmzddgb0f9hi30kdnyq0jvr8")))

(define-public crate-libcst_derive-1.3.1 (c (n "libcst_derive") (v "1.3.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1q0vc3fngqffgl8g09cawmv95vjxv2wh23m9lhadx4whspr12l2a")))

(define-public crate-libcst_derive-1.4.0 (c (n "libcst_derive") (v "1.4.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0ys67c3kjgx6dg35pb3f8r0jnzqwhwycn11mlp3d5760g80l1bm2")))

