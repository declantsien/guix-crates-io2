(define-module (crates-io li bc libcprover_rust) #:use-module (crates-io))

(define-public crate-libcprover_rust-0.1.0 (c (n "libcprover_rust") (v "0.1.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1jbms4pmsi4lwmfriyx0wk2ybs416s8b9fd540na006865zwmxj6")))

(define-public crate-libcprover_rust-5.80.0 (c (n "libcprover_rust") (v "5.80.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1cf6xlz0s6qjmziprbqnsnr5rgz5q2nsspmn7qp7hjn9a7vyl1cd")))

(define-public crate-libcprover_rust-5.81.0 (c (n "libcprover_rust") (v "5.81.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1b5vbaj6nmqjgwi54wvs936b9fmw3knsxjy7sysy1xmhhcq66sqr")))

(define-public crate-libcprover_rust-5.82.0 (c (n "libcprover_rust") (v "5.82.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0qxjapz30a5imhkq22b7j7bjbai0y1yka2m1c3x1rr3l5canpmj8")))

(define-public crate-libcprover_rust-5.83.0 (c (n "libcprover_rust") (v "5.83.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1lpyyh0xz9mynv0qmjzlj03zc7ipspggpcx8snad7dnbihq7s8id")))

(define-public crate-libcprover_rust-5.84.0 (c (n "libcprover_rust") (v "5.84.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0ig3mz24s5lws0hzs73kp9acx2ahhvbqqgbi475s2ygwyz1h913q")))

(define-public crate-libcprover_rust-5.85.0 (c (n "libcprover_rust") (v "5.85.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "02x39vgap448z1ib3wjg5j4ss475c2y3zqww3sghwp6bf9chwbqm")))

(define-public crate-libcprover_rust-5.86.0 (c (n "libcprover_rust") (v "5.86.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "152nwws2kcwfbs364pr5qzswz80k3ilp4xsdk2cfj8k74c7azn9b")))

(define-public crate-libcprover_rust-5.87.0 (c (n "libcprover_rust") (v "5.87.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "074bgi4j7hnl10c163035hs6fklgcz8za0wvxlin3ln5liy24852")))

(define-public crate-libcprover_rust-5.88.0 (c (n "libcprover_rust") (v "5.88.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "11kmpzlj44lj9ba1c393rk3c19bnviy22335sl62ixbs2y6w1r5w")))

(define-public crate-libcprover_rust-5.88.1 (c (n "libcprover_rust") (v "5.88.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "01v25hq7iqgcjawsglw72ym3mgv82nn11x7wkgmh3d2g75bxrmzy")))

(define-public crate-libcprover_rust-5.89.0 (c (n "libcprover_rust") (v "5.89.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0v3c16gcfnbzdcyifiz4mjlrla1vkl7ys0xkbpk2b9y5r2w3cslz")))

(define-public crate-libcprover_rust-5.90.0 (c (n "libcprover_rust") (v "5.90.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1ja0y522nxwg5f2a31b0qcg6083aiwk2jiz5b529wc1658pjbzjs")))

(define-public crate-libcprover_rust-5.91.0 (c (n "libcprover_rust") (v "5.91.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1y951ndzl7kh40vgvg2n1pzmg4jcf4l8zb0325326xkcvqpgd4iz")))

(define-public crate-libcprover_rust-5.92.0 (c (n "libcprover_rust") (v "5.92.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0j76z10psslhiwkr469js3q4kqsnpipnplhmd8r05wn2gyiiirj6")))

(define-public crate-libcprover_rust-5.93.0 (c (n "libcprover_rust") (v "5.93.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "0s7z7xjkx4b5qv3lklql9xfdkqi51jxhfkd6cfsczmxphdmwlhnd")))

(define-public crate-libcprover_rust-5.94.0 (c (n "libcprover_rust") (v "5.94.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "1his1nj6v2ydphrmdsby0k58brym0kc58pzbkf1x8yscxgz512k8")))

(define-public crate-libcprover_rust-5.95.1 (c (n "libcprover_rust") (v "5.95.1") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0") (d #t) (k 1)))) (h "03iq1yaw77zd4dsvdj1g8kp1vfw0l94sjqf8p7m0djhjhfz5j2j6")))

