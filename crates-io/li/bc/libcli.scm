(define-module (crates-io li bc libcli) #:use-module (crates-io))

(define-public crate-libcli-0.1.0 (c (n "libcli") (v "0.1.0") (h "05i7mmm0gw0sf8sjpg8x80gsnagiq0i7npj6v2kxx8rby5924fqs")))

(define-public crate-libcli-0.1.1 (c (n "libcli") (v "0.1.1") (h "08q65spahff05m0la3bn7r7lbivmksd4bxkyfb43z4wp8rx6iim5")))

(define-public crate-libcli-0.1.2 (c (n "libcli") (v "0.1.2") (h "07x5sjjdawv7bv8jxsnxl8jra6lfg126sgsbvqvslm6jjxkxgl54")))

(define-public crate-libcli-0.1.3 (c (n "libcli") (v "0.1.3") (h "1aplh5abgkjqcdh7iak0mwkzx7vvrbs21nhifvvr7vqrd6k7x6cf")))

(define-public crate-libcli-0.2.3 (c (n "libcli") (v "0.2.3") (h "19wg1va9zylb6zc7w9w4z2vck7qy0ax7gb5zvj5lc35511ni0jjl")))

(define-public crate-libcli-0.2.4 (c (n "libcli") (v "0.2.4") (h "0dhpi4rldvzczrrlzbxx3agnmrgdswhjvf6f232scf1ff71b9xkg")))

(define-public crate-libcli-0.3.4 (c (n "libcli") (v "0.3.4") (h "047rq5qr93q70aacd38d66p9g6llwzppk87b4g71vf7xhqaipvg5")))

(define-public crate-libcli-0.3.5 (c (n "libcli") (v "0.3.5") (h "1kk5cii57hvcbg3is0a3rc6zvjvv7zjg6cwyqfa13qr3aaznv6j7")))

(define-public crate-libcli-0.3.6 (c (n "libcli") (v "0.3.6") (h "0sgnzms4g2vwvi1hd9d3746cr0kl78qvfxs5n1pngr848n8dpva7")))

(define-public crate-libcli-0.3.7 (c (n "libcli") (v "0.3.7") (h "1nak0avzc502q795648fyna02dq6bqy20m7wa645nx7hr3w3vqxh")))

(define-public crate-libcli-0.3.8 (c (n "libcli") (v "0.3.8") (h "1i68jlrbf9bmvkxmwnz6xl88cr4riddqk90s7avpj5mbzdkq6cvh")))

(define-public crate-libcli-0.3.9 (c (n "libcli") (v "0.3.9") (h "0l33dxfzg026x2fvc8mf243ha8zb4sivhs57r1miznm1yd2fq2si")))

