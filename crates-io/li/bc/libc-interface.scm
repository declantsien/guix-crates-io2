(define-module (crates-io li bc libc-interface) #:use-module (crates-io))

(define-public crate-libc-interface-0.1.0 (c (n "libc-interface") (v "0.1.0") (h "1crwlzgh8v96cjgivradw7i7ii5j0nx3vhbaqk8aiivdlmp609dp") (f (quote (("default") ("align"))))))

(define-public crate-libc-interface-0.1.1 (c (n "libc-interface") (v "0.1.1") (h "0fc0v3r1j4g67j4fgd7s0qiivr3jdkvlj4zwrah2g52mf0cbyd63") (f (quote (("default") ("align"))))))

(define-public crate-libc-interface-0.1.2 (c (n "libc-interface") (v "0.1.2") (h "16313dhw3l9qfnd7855w7fxiia5bka00ics8kjf89q9diybqx4l5") (f (quote (("default") ("align"))))))

