(define-module (crates-io li bc libchisel) #:use-module (crates-io))

(define-public crate-libchisel-0.1.0 (c (n "libchisel") (v "0.1.0") (d (list (d (n "parity-wasm") (r "^0.21") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "1xfnb84ivnb832pn8jbb0k09wxqnd944yj9bn4k8f9k9qzkj8jf4")))

(define-public crate-libchisel-0.2.0 (c (n "libchisel") (v "0.2.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.6") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "0zdpvh77jvl8jyf5z65b9dz95if1ni7yvpgx23y8fabwja0n7wlm")))

(define-public crate-libchisel-0.3.0 (c (n "libchisel") (v "0.3.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.6") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "1bxdm6si8zcrrkjr4jw4b396gf8mgxgck6wfrscblgp6mq6lp980")))

(define-public crate-libchisel-0.4.0 (c (n "libchisel") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.6") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)))) (h "1l8f00dnajdl55zf8a9ms49xml2cy18pipl079z8i24hk8ys66zc")))

(define-public crate-libchisel-0.5.0 (c (n "libchisel") (v "0.5.0") (d (list (d (n "byteorder") (r "^1.2.4") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.35.6") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 0)) (d (n "wabt") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "wasm-snip") (r "^0.2.0") (d #t) (k 0)))) (h "17mp7gpqalpdgs5cllql078jb29xy9ig9fl7fqc5zpklfmpqqxzh") (f (quote (("default"))))))

(define-public crate-libchisel-0.6.0 (c (n "libchisel") (v "0.6.0") (d (list (d (n "binaryen") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "parity-wasm") (r "^0.40.2") (d #t) (k 0)) (d (n "rustc-hex") (r "^1.0") (d #t) (k 2)) (d (n "wasm-snip") (r "^0.2.0") (d #t) (k 0)))) (h "1ig8isnd7iwdbb1n3gl19h3fhxn7jjx7an2gayii3rv21dvqrhck") (f (quote (("default"))))))

