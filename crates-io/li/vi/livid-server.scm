(define-module (crates-io li vi livid-server) #:use-module (crates-io))

(define-public crate-livid-server-0.1.0 (c (n "livid-server") (v "0.1.0") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.11.0") (d #t) (k 0)))) (h "0jkqr9ibmq44wzkvs7spcw762k7j10gyykjx9mqc3g9nqb7z9akl")))

(define-public crate-livid-server-0.2.0 (c (n "livid-server") (v "0.2.0") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.11.0") (d #t) (k 0)))) (h "13zyqym6ggq7v0832wrwrs49w1jfdmsi1j9zdlvs2f4fbhfp9b5k") (y #t)))

(define-public crate-livid-server-0.2.1 (c (n "livid-server") (v "0.2.1") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "tiny_http") (r "^0.11.0") (d #t) (k 0)))) (h "11yljgpq9nf35j8zrgxwjxg89v22m0p35g9z6q60yfzg6hgnhy55")))

(define-public crate-livid-server-0.2.2 (c (n "livid-server") (v "0.2.2") (d (list (d (n "ascii") (r "^1.0.0") (d #t) (k 0)) (d (n "httpdate") (r "^1.0.3") (d #t) (k 0)) (d (n "tiny_http") (r "^0.12.0") (d #t) (k 0)))) (h "0bdrj4lafba6nxx9w4s7fv0w62jpg8rmszzvgg948in7ycmdafv8")))

