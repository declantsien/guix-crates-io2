(define-module (crates-io li vi livid-cli) #:use-module (crates-io))

(define-public crate-livid-cli-0.1.0 (c (n "livid-cli") (v "0.1.0") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1kvn3rrg88w5fmqh4v11i1zbvzf36a3dh7yvbh5i5cij693mqjfh")))

(define-public crate-livid-cli-0.1.1 (c (n "livid-cli") (v "0.1.1") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "11imn10l0bbrddhs72lh1qz0739jh5i1kc1as0p3c6hvar7x91fa")))

(define-public crate-livid-cli-0.1.2 (c (n "livid-cli") (v "0.1.2") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0bawmciligab9bfgqfw0jla22mf665qj3jl5jqg6bk36ng1mfg1a")))

(define-public crate-livid-cli-0.1.3 (c (n "livid-cli") (v "0.1.3") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1i15sfjs66w78cx6b15g9h0j5cqjb1f4z53ms43wird453ynjj2l")))

(define-public crate-livid-cli-0.1.4 (c (n "livid-cli") (v "0.1.4") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0dqc9wbm8iz7q479a9pln9s0bb5006dbl1lblw80kfi0g35qr6pp")))

(define-public crate-livid-cli-0.1.5 (c (n "livid-cli") (v "0.1.5") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0wbggpanyb1p19bgfii3cy6nqjrkgdk1dcw613yixqklwsaf22w8")))

(define-public crate-livid-cli-0.1.6 (c (n "livid-cli") (v "0.1.6") (d (list (d (n "fs_extra") (r "^1.2.0") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0xckvayxvazbxjjs0l3nb0df310ilrssg6bisvb6g230baabnqcn")))

(define-public crate-livid-cli-0.1.7 (c (n "livid-cli") (v "0.1.7") (d (list (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "livid-server") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "17i46xs4r8c58sjmm4sb8ggmhf6wi2jq3i7d2i1yzb63yim73jw5")))

(define-public crate-livid-cli-0.1.8 (c (n "livid-cli") (v "0.1.8") (d (list (d (n "fs_extra") (r "^1.3.0") (d #t) (k 0)) (d (n "livid-server") (r "^0.2") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "0a596xxjkd05f1mjbjn4crsdgpfidb3l3iyfv8kp106xwr84l5b5")))

