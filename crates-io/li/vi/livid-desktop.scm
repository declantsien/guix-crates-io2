(define-module (crates-io li vi livid-desktop) #:use-module (crates-io))

(define-public crate-livid-desktop-0.1.0 (c (n "livid-desktop") (v "0.1.0") (d (list (d (n "fltk-webview-sys") (r "^0.2") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)))) (h "0ah3qdgb4s6n0yn3jfic1ixgsgpbnwxba6s0jaczam2dq8128zg7")))

(define-public crate-livid-desktop-0.1.1 (c (n "livid-desktop") (v "0.1.1") (d (list (d (n "fltk-webview-sys") (r "^0.2") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)))) (h "09pqrl10s3wgd73ajqajvxrnvsjlwiixqj3slbqpw7dawdp0hc9q")))

(define-public crate-livid-desktop-0.1.2 (c (n "livid-desktop") (v "0.1.2") (d (list (d (n "fltk-webview-sys") (r "^0.2.9") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)))) (h "1lfdzlhrazv5i5569453yai67s1l5kch60gy00f65cs6pgxkb1sl")))

(define-public crate-livid-desktop-0.1.3 (c (n "livid-desktop") (v "0.1.3") (d (list (d (n "fltk-webview-sys") (r "^0.2.9") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)))) (h "01m70kbvbsn0mpxzrhcly3xklv6jbkj1id7y553acihzih8vvkxp")))

(define-public crate-livid-desktop-0.1.4 (c (n "livid-desktop") (v "0.1.4") (d (list (d (n "fltk-webview-sys") (r "^0.2.10") (d #t) (k 0)) (d (n "livid-server") (r "^0.1") (d #t) (k 0)))) (h "02bky9b63mgqf4dia8k6n8vbrbdskfwplaa0f8gy19sh1927z734")))

(define-public crate-livid-desktop-0.1.5 (c (n "livid-desktop") (v "0.1.5") (d (list (d (n "fltk-webview-sys") (r "^0.2.10") (d #t) (k 0)) (d (n "livid-server") (r "^0.2") (d #t) (k 0)))) (h "0lvbad5rjhgiy75czdmz9zc1ilg9r93lc2wf78c6qr48fwslklpg")))

(define-public crate-livid-desktop-0.1.6 (c (n "livid-desktop") (v "0.1.6") (d (list (d (n "fltk-webview-sys") (r "^0.2.10") (d #t) (k 0)) (d (n "livid-server") (r "^0.2") (d #t) (k 0)))) (h "1dk9ghxmmkgxs6j4b3qfc56wcz6k955bpv0vccbag7zpafvhi3q5")))

(define-public crate-livid-desktop-0.1.7 (c (n "livid-desktop") (v "0.1.7") (d (list (d (n "livid-server") (r "^0.2") (d #t) (k 0)) (d (n "wv-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1vdsbv80k49587i78jg1pksilrczwqr3ii8jzd5zipd08fpqjad1")))

(define-public crate-livid-desktop-0.1.8 (c (n "livid-desktop") (v "0.1.8") (d (list (d (n "livid-server") (r "^0.2") (d #t) (k 0)) (d (n "wv") (r "^0.1.0") (d #t) (k 0)))) (h "05zip51r6kwdxn5zs578ri1bk7dgcxmwv0xws1gmqj53nhy1q1hz")))

