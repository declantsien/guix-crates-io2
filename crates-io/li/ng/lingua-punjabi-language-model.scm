(define-module (crates-io li ng lingua-punjabi-language-model) #:use-module (crates-io))

(define-public crate-lingua-punjabi-language-model-1.0.0 (c (n "lingua-punjabi-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1mym8jn0cm2x12p2g11vabraag9c419zzzk4fnlbvvpcrj20zjgx")))

(define-public crate-lingua-punjabi-language-model-1.0.1 (c (n "lingua-punjabi-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1l51sa3g5g4k864hhfd064v4kwi8lcsv37nr5kfng8sjvn6qdna1")))

(define-public crate-lingua-punjabi-language-model-1.1.0 (c (n "lingua-punjabi-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0kv0sx5swcfyriz6ljwdadykaymd9i5rdyzpd8vclz5gwrc9wlnc")))

