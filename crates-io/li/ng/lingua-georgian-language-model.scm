(define-module (crates-io li ng lingua-georgian-language-model) #:use-module (crates-io))

(define-public crate-lingua-georgian-language-model-1.0.0 (c (n "lingua-georgian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0j7vrm7cqkzz2zzqpiqmw60adi74zrysbkp5s2lxcdyzd76a04pp")))

(define-public crate-lingua-georgian-language-model-1.0.1 (c (n "lingua-georgian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "12gin3gxwwj2j07dqi1gzsh6jgjg22incdiys9fhj7idnfi2cxb4")))

(define-public crate-lingua-georgian-language-model-1.1.0 (c (n "lingua-georgian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "009g1hf259sc9z9mvm4hiyk3jw54cfsdprxsf7zz6pq62kl2nff7")))

