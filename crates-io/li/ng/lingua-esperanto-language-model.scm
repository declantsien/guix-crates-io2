(define-module (crates-io li ng lingua-esperanto-language-model) #:use-module (crates-io))

(define-public crate-lingua-esperanto-language-model-1.0.0 (c (n "lingua-esperanto-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0ckjg5l1x0rz101q6wi09zj810qx9nim839g4asyhfmfmxd32drv")))

(define-public crate-lingua-esperanto-language-model-1.0.1 (c (n "lingua-esperanto-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "09vpgpv5b5dhlxwlgnhb0v8i5315p0vwj5zsz685ixpfpzsd83mn")))

(define-public crate-lingua-esperanto-language-model-1.1.0 (c (n "lingua-esperanto-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1hjpl613rwj2rrdixcyvq0hph2b7qsqrlv132hkiy5j66c5icxvq")))

