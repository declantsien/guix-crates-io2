(define-module (crates-io li ng lingua-kazakh-language-model) #:use-module (crates-io))

(define-public crate-lingua-kazakh-language-model-1.0.0 (c (n "lingua-kazakh-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "140kw6hiqand0mzrw503g51ifqmsbm9h55kwvd1c656skcy1nfrb")))

(define-public crate-lingua-kazakh-language-model-1.0.1 (c (n "lingua-kazakh-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "11jpnx1smjd4fvr664m3sa9f1qszp5z245bcz0hn4ym7k6vhmni1")))

(define-public crate-lingua-kazakh-language-model-1.1.0 (c (n "lingua-kazakh-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "190dbim6xn9qhn4shly4yjmxdjrfky5x2nks9qydqf0sri4ahr0v")))

