(define-module (crates-io li ng lingua-indonesian-language-model) #:use-module (crates-io))

(define-public crate-lingua-indonesian-language-model-1.0.0 (c (n "lingua-indonesian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0yp7wf7fd1lygymbmwbb8y7zww99n9mngb95kaip2z97d0rgir66")))

(define-public crate-lingua-indonesian-language-model-1.0.1 (c (n "lingua-indonesian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "07l9g173h8ibqlyzhpr3dnl7x9v47z1nm8nzd8ajk2szj45x2kpf")))

(define-public crate-lingua-indonesian-language-model-1.1.0 (c (n "lingua-indonesian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1bfr44nzy7fdvjb7v27swpmpdy56zb5hkl4gn9vavnkagy9vh1nw")))

