(define-module (crates-io li ng lingon-macro) #:use-module (crates-io))

(define-public crate-lingon-macro-0.1.0 (c (n "lingon-macro") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15ks8nshii34q7lcqnl7y6m1ynkiq18v5qizichvbybwbbqqxkzd")))

