(define-module (crates-io li ng lingua-nynorsk-language-model) #:use-module (crates-io))

(define-public crate-lingua-nynorsk-language-model-1.0.0 (c (n "lingua-nynorsk-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "07igynl92znf3ivhwhj04h5r4mmmfvqcrzjm3gim7q3l9riqx1lv")))

(define-public crate-lingua-nynorsk-language-model-1.0.1 (c (n "lingua-nynorsk-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1zjqxz71gnypkiwd1x81x2zd1535vih7dhxqp0zc4wdiv7h82pa4")))

(define-public crate-lingua-nynorsk-language-model-1.1.0 (c (n "lingua-nynorsk-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1afgc4ryy843lcjhs5lvihnv16gk1wh7fxljfv5ll96zc4sv206v")))

