(define-module (crates-io li ng lingua-chinese-language-model) #:use-module (crates-io))

(define-public crate-lingua-chinese-language-model-1.0.0 (c (n "lingua-chinese-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1mc8dhqlbp4mzf8vp6p2q5lnns72wj3x4bh1b7cjq7mnzx3wsdxh")))

(define-public crate-lingua-chinese-language-model-1.0.1 (c (n "lingua-chinese-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "14ihsh5g4yaxcll2p3vakk611dwkv77k2aamy6qw9iv5d7lmy0gn")))

(define-public crate-lingua-chinese-language-model-1.1.0 (c (n "lingua-chinese-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1ipp0cb82sxy6ax9bd8k4wa2gnacq2l7jslvppfzc809iaiy78a5")))

