(define-module (crates-io li ng lingua-bengali-language-model) #:use-module (crates-io))

(define-public crate-lingua-bengali-language-model-1.0.0 (c (n "lingua-bengali-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0q4v4i8jifp05x9h91l23sa0lf3lh0m8hjvnajy6af2y86fiblpg")))

(define-public crate-lingua-bengali-language-model-1.0.1 (c (n "lingua-bengali-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1spyvl9skgsa1bzks2gi7909nipxhirqsfh2hqwji8vamjaj2027")))

(define-public crate-lingua-bengali-language-model-1.1.0 (c (n "lingua-bengali-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "17971l6cag1ypnmb3gw7ifh6j6kqy64223r4xr79hy0cn33z3ygc")))

