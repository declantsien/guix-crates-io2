(define-module (crates-io li ng lingua-malay-language-model) #:use-module (crates-io))

(define-public crate-lingua-malay-language-model-1.0.0 (c (n "lingua-malay-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "07ixainrlww2bh1rs73mzikndxw6c55993yfkdbh90c7yy1h2j3v")))

(define-public crate-lingua-malay-language-model-1.0.1 (c (n "lingua-malay-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1rc3l48jyz9jsxh3fmqwz7q1jf4grzvgl9qm4qxy5g5kr4qckvqm")))

(define-public crate-lingua-malay-language-model-1.1.0 (c (n "lingua-malay-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1an894z8ia837k20w60a7x66gb8sc43by6ffw5ijapl6n9x6n73p")))

