(define-module (crates-io li ng lingua-welsh-language-model) #:use-module (crates-io))

(define-public crate-lingua-welsh-language-model-1.0.0 (c (n "lingua-welsh-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "09i3vk469bqa98pqxxmj76hbknwkk2jcajm1p35ndcxriw305q93")))

(define-public crate-lingua-welsh-language-model-1.0.1 (c (n "lingua-welsh-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1fglz4x0kabca88dps5bs5yzgq9wflfkc6jyh6s1j6fhvmqgh39j")))

(define-public crate-lingua-welsh-language-model-1.1.0 (c (n "lingua-welsh-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1c8rin92jjwfzpa4sjj6a548460bj1cr61nfgvfjyxv67d9pmi07")))

