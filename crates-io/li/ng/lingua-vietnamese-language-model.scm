(define-module (crates-io li ng lingua-vietnamese-language-model) #:use-module (crates-io))

(define-public crate-lingua-vietnamese-language-model-1.0.0 (c (n "lingua-vietnamese-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0q7qm4v4f95mmv1yiw1r7jc6v2s2bavjcr82jb1981cb6gf63mmz")))

(define-public crate-lingua-vietnamese-language-model-1.0.1 (c (n "lingua-vietnamese-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0p7vabx6bzx0bbnzdwwzhyxm4myyailayag607i25ldf6ir0qjd9")))

(define-public crate-lingua-vietnamese-language-model-1.1.0 (c (n "lingua-vietnamese-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0jgx84z5zy573f7ldql54scnilzc5vszmbxzyainm433ybx8ml07")))

