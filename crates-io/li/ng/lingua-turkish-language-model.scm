(define-module (crates-io li ng lingua-turkish-language-model) #:use-module (crates-io))

(define-public crate-lingua-turkish-language-model-1.0.0 (c (n "lingua-turkish-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0bja6svgg9k4djka8ch0ynzzdq3nqcav4jrb8hkiyly8brmix0va")))

(define-public crate-lingua-turkish-language-model-1.0.1 (c (n "lingua-turkish-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0ww3g2a8iak67xq30z49dh8y0w1kh5jxq8i5v3p075ffy2zi71gm")))

(define-public crate-lingua-turkish-language-model-1.1.0 (c (n "lingua-turkish-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "10ddmyrphvzmhfd8r2f0a7iwyfr2q39gv07kz3kq1kllzq97wlbv")))

