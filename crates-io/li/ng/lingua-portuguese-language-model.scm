(define-module (crates-io li ng lingua-portuguese-language-model) #:use-module (crates-io))

(define-public crate-lingua-portuguese-language-model-1.0.0 (c (n "lingua-portuguese-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0pmv5rvl1z6rsky33q08hp33mpdf9rvy808myg4qjwvn760mg71l")))

(define-public crate-lingua-portuguese-language-model-1.0.1 (c (n "lingua-portuguese-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0xysdmrfzqx7ib914ndnapfcx7rr56zpc1xqqkhqnsm98pp09dmz")))

(define-public crate-lingua-portuguese-language-model-1.1.0 (c (n "lingua-portuguese-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "12z5rz7wxi65lg9i32l1pxp4mzy0089q99vn1vhj8pizgcqbxw4x")))

