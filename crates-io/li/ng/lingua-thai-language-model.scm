(define-module (crates-io li ng lingua-thai-language-model) #:use-module (crates-io))

(define-public crate-lingua-thai-language-model-1.0.0 (c (n "lingua-thai-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0ig2gvm8xn203kn73yqbd8p6wpdqvjgaxhjxggr8f41p2cqfqfap")))

(define-public crate-lingua-thai-language-model-1.0.1 (c (n "lingua-thai-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "14ywfh1i568gh9myjlik76faqaqsd19s6a4mhryd71rwsk7ch7f0")))

(define-public crate-lingua-thai-language-model-1.1.0 (c (n "lingua-thai-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0xiwr93dbrv03c1r3zajz8sikzvffn4mcl7rirnrhgf6kwh0jjqr")))

