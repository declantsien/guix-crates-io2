(define-module (crates-io li ng lingua-latvian-language-model) #:use-module (crates-io))

(define-public crate-lingua-latvian-language-model-1.0.0 (c (n "lingua-latvian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1hy3fdhhc8didwrf6pf50c3g3fhb3r8lqcfinrj38s31fpdbclgj")))

(define-public crate-lingua-latvian-language-model-1.0.1 (c (n "lingua-latvian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0vjxd7v4w79ydg6f124vlp71vqiqjl2cin44dfw9jnc4i5y97a31")))

(define-public crate-lingua-latvian-language-model-1.0.2 (c (n "lingua-latvian-language-model") (v "1.0.2") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "07sfa2x2xgpmgmnrczq2589lylgx2c685bycf2y9k4zsaa8b9q2z")))

(define-public crate-lingua-latvian-language-model-1.1.0 (c (n "lingua-latvian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "024grwrrb5hwnz08b2n3m0yha9wkpbbp4l8bn2zwqr6spwdw0v0b")))

