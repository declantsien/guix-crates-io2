(define-module (crates-io li ng lingua-maori-language-model) #:use-module (crates-io))

(define-public crate-lingua-maori-language-model-1.0.0 (c (n "lingua-maori-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1826b7m7b4q5jx1vdwjxxvjxg3bzmpac1s4b5xx0hmgapq341iky")))

(define-public crate-lingua-maori-language-model-1.0.1 (c (n "lingua-maori-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0050vsvvlbh87gk6pn8dhd6saw9ac6ckhq5iqg6x7jzphfzv45z5")))

(define-public crate-lingua-maori-language-model-1.1.0 (c (n "lingua-maori-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "147zi8g9ri566wb1yffavgbcn5gplhmkcppppn4a1yjwdk0s0lyn")))

