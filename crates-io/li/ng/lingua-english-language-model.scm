(define-module (crates-io li ng lingua-english-language-model) #:use-module (crates-io))

(define-public crate-lingua-english-language-model-1.0.0 (c (n "lingua-english-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0kasavzmxn8jrqikvdq1a7apks0mxcvw4h0vaf4qbx5vjxfnrxka")))

(define-public crate-lingua-english-language-model-1.0.1 (c (n "lingua-english-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1cl1746crza9vnj4z6d7nd798ifwxq80x2x0lrzpczzbmy0s4sxa")))

(define-public crate-lingua-english-language-model-1.1.0 (c (n "lingua-english-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1a5q2wc230pm7s3ss8ah4rq72dfs9m4irz5mnlxhh45v79lndl7v")))

