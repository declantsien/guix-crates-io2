(define-module (crates-io li ng lingua-albanian-language-model) #:use-module (crates-io))

(define-public crate-lingua-albanian-language-model-1.0.0 (c (n "lingua-albanian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1nl1v4ffgrf4i7a8zalbnjjvhrnhbwk78hccjf9aj34r5wqy1z13")))

(define-public crate-lingua-albanian-language-model-1.0.1 (c (n "lingua-albanian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1wikw517dpr6iy25n7q3z9gr59hylgaxcgvj3jzwya8j6q0j2crs")))

(define-public crate-lingua-albanian-language-model-1.1.0 (c (n "lingua-albanian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "15hrm9yh6mlb9mjhsra3rqrmwi0w1wg4ip0wpm9psczkd028zxy1")))

