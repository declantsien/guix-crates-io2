(define-module (crates-io li ng lingua-icelandic-language-model) #:use-module (crates-io))

(define-public crate-lingua-icelandic-language-model-1.0.0 (c (n "lingua-icelandic-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1ajiifjj08mnrgkx9a1byy75pif7byn64h9czz670l9bpdk7sscz")))

(define-public crate-lingua-icelandic-language-model-1.0.1 (c (n "lingua-icelandic-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "167yjv9222b5jqh2mp7j548pml8mycgjy7ipc2m8kj9vjhsdjac1")))

(define-public crate-lingua-icelandic-language-model-1.1.0 (c (n "lingua-icelandic-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "17b5mfbyns0c06mp2fpybq7whps4sdc45vzwh6cbr80svxrya5mn")))

