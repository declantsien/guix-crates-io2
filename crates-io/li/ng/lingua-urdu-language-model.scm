(define-module (crates-io li ng lingua-urdu-language-model) #:use-module (crates-io))

(define-public crate-lingua-urdu-language-model-1.0.0 (c (n "lingua-urdu-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1ix44a02bd2ma27i06bkymqg21ilsfyna6yql2i4gd261pa31mjd")))

(define-public crate-lingua-urdu-language-model-1.0.1 (c (n "lingua-urdu-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0ss0cf392qwrfiafwnx2zymx5n0d6z855xwsq3q5r86xf7gf4kcq")))

(define-public crate-lingua-urdu-language-model-1.1.0 (c (n "lingua-urdu-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1x7aa32pwzv2rx6lj1b8ag2p0xr386zz304yp2sh90782n7dp7hv")))

