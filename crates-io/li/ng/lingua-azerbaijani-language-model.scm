(define-module (crates-io li ng lingua-azerbaijani-language-model) #:use-module (crates-io))

(define-public crate-lingua-azerbaijani-language-model-1.0.0 (c (n "lingua-azerbaijani-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "159358227ss7xhchlsgd9jcmhqcz2240bv2hhz0gsgwrgqy6jrda")))

(define-public crate-lingua-azerbaijani-language-model-1.0.1 (c (n "lingua-azerbaijani-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1xilfcm10bzj1wl7amaxndz69141xim9js1lkwgjb7ckxfhk50nn")))

(define-public crate-lingua-azerbaijani-language-model-1.1.0 (c (n "lingua-azerbaijani-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "14j874v94ms0l8jlrfq2lv7xrj4zwx72plfg67dklj6mpsw449wn")))

