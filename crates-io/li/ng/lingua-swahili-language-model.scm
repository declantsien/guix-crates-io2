(define-module (crates-io li ng lingua-swahili-language-model) #:use-module (crates-io))

(define-public crate-lingua-swahili-language-model-1.0.0 (c (n "lingua-swahili-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "05xv3lkdgq4iy5jv2iyxalc9hdhc9mcnss4zkrmdadlsicavgzhx")))

(define-public crate-lingua-swahili-language-model-1.0.1 (c (n "lingua-swahili-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1999rvq70mz189iigqab0hixm0m9hw0pxp1pl4f1y1pkh8991s8r")))

(define-public crate-lingua-swahili-language-model-1.1.0 (c (n "lingua-swahili-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1qsgmwkqqpny18p9xyw3vqcpqhb42b9my2c7mwcyamng0mlj8lgj")))

