(define-module (crates-io li ng lingua-italian-language-model) #:use-module (crates-io))

(define-public crate-lingua-italian-language-model-1.0.0 (c (n "lingua-italian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "195q8hfz662d4va06b61gxc973wxkh7ynsv1dl2h35df9q6887cr")))

(define-public crate-lingua-italian-language-model-1.0.1 (c (n "lingua-italian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1bj1ra0fxmg65nbyiz6kskcpv1gd5fhks5wc6kpkghf9nvmckki4")))

(define-public crate-lingua-italian-language-model-1.1.0 (c (n "lingua-italian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1475hb6d91b0y5pharwyq4m6ay1q72gasdwy84g6hkdc8y016vhc")))

