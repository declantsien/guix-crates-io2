(define-module (crates-io li ng lingua-sotho-language-model) #:use-module (crates-io))

(define-public crate-lingua-sotho-language-model-1.0.0 (c (n "lingua-sotho-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0m8x6bvfsig2z461zvdmdvxyizali7mc2mv95jzrw7sdg4pw7bi6")))

(define-public crate-lingua-sotho-language-model-1.0.1 (c (n "lingua-sotho-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "02ha1ikf2fl4n167gs95c33a4dg67npk9rjwmzk0yac1mkdymqh0")))

(define-public crate-lingua-sotho-language-model-1.1.0 (c (n "lingua-sotho-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "014zv1ckgjqs7dyzpv9lx5fwznnazlykz2rfj8ngccmg3yw4w22q")))

