(define-module (crates-io li ng lingua-bokmal-language-model) #:use-module (crates-io))

(define-public crate-lingua-bokmal-language-model-1.0.0 (c (n "lingua-bokmal-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1hganyfsl6wyx8yrn9mw55bn1xr28drnklhriah4r90fhsmlnqi9")))

(define-public crate-lingua-bokmal-language-model-1.0.1 (c (n "lingua-bokmal-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "160p6mdadm8d7x0j4d67cgb35iq0f6qq8j150mnj8ipifbla4sby")))

(define-public crate-lingua-bokmal-language-model-1.1.0 (c (n "lingua-bokmal-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1fawz88imrsmc7pzp2nb240832bkaz8xkny55li3fvdydgp1b4ia")))

