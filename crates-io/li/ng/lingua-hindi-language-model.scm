(define-module (crates-io li ng lingua-hindi-language-model) #:use-module (crates-io))

(define-public crate-lingua-hindi-language-model-1.0.0 (c (n "lingua-hindi-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1ld2cy0n8lc5rvnrp12i5x8z55a9q281y82jf6cywcc4pwgq4s2g")))

(define-public crate-lingua-hindi-language-model-1.0.1 (c (n "lingua-hindi-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "112spz09fam7dh9sh6g3y5h588jwy22ijqz0n8fbkis7xr098s9h")))

(define-public crate-lingua-hindi-language-model-1.1.0 (c (n "lingua-hindi-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1l2gw9fd7fgd28ypxhsbin65pc5dk15s1myh5rnp137s2kjmjdpn")))

