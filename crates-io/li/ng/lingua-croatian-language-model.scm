(define-module (crates-io li ng lingua-croatian-language-model) #:use-module (crates-io))

(define-public crate-lingua-croatian-language-model-1.0.0 (c (n "lingua-croatian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "119fxyjnl1g07gfdap7h76kz3wl6qn76c8vzf9v0fsqmm8lh84g0")))

(define-public crate-lingua-croatian-language-model-1.0.1 (c (n "lingua-croatian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1r917q90dx97cjybl0yi1x06rr3kg1wd4h5zpj36kvppv8m7mh6i")))

(define-public crate-lingua-croatian-language-model-1.1.0 (c (n "lingua-croatian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0i133h3bxaldf5h7l1f69kh7cddn8ssywys9s917qizss6bsksk6")))

