(define-module (crates-io li ng lingua-polish-language-model) #:use-module (crates-io))

(define-public crate-lingua-polish-language-model-1.0.0 (c (n "lingua-polish-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "073ms53srsj2f32zm1aj02f16a3zxznbzng5952990ph6njyyyvr")))

(define-public crate-lingua-polish-language-model-1.0.1 (c (n "lingua-polish-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "01926gdww1g84sb1dlcnbw2x4901p9yqzk4gb3qrs3hh7b03lrqp")))

(define-public crate-lingua-polish-language-model-1.1.0 (c (n "lingua-polish-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0xgzr7mmw5nljxg7f6phf4dnffxzbixdpmqs239qap8sad8bd67j")))

