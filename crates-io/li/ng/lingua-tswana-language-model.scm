(define-module (crates-io li ng lingua-tswana-language-model) #:use-module (crates-io))

(define-public crate-lingua-tswana-language-model-1.0.0 (c (n "lingua-tswana-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1mhfzy4cw9gl0727bwnqyg05k6mpa3zwryybknw0lwf0kxqqlb65")))

(define-public crate-lingua-tswana-language-model-1.0.1 (c (n "lingua-tswana-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1xyv0vj86xa13b2d2h8a97qhi9qx1f4w3zyhvrmjz185gddj7yqr")))

(define-public crate-lingua-tswana-language-model-1.1.0 (c (n "lingua-tswana-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0dqycv2f691scnf999rdfpv8b4sc2fz0asz9vd4fpx07hl91dc2x")))

