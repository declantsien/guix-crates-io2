(define-module (crates-io li ng lingua-hebrew-language-model) #:use-module (crates-io))

(define-public crate-lingua-hebrew-language-model-1.0.0 (c (n "lingua-hebrew-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1vvjl1ikvf9spqbrb0s72d9hcn3gxp4y2m17f6q17j0l10a9bm4j")))

(define-public crate-lingua-hebrew-language-model-1.0.1 (c (n "lingua-hebrew-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0kpdvkwxgzja7ldp726wmg2wi0pi3mg73xqyzasqn6krp6lsvgi6")))

(define-public crate-lingua-hebrew-language-model-1.1.0 (c (n "lingua-hebrew-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1xjia0iy5mnkbsfrpsilcbix0cn3fbfhlwd9scgbkgr17zxcj4qs")))

