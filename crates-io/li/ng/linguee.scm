(define-module (crates-io li ng linguee) #:use-module (crates-io))

(define-public crate-linguee-0.1.0 (c (n "linguee") (v "0.1.0") (d (list (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (f (quote ("simd"))) (d #t) (k 0)))) (h "1dm9gxn0j11k1f9l09r7jz7pw40qgwr8aayqjhv6b2nf6xlj6sfb")))

(define-public crate-linguee-0.1.1 (c (n "linguee") (v "0.1.1") (d (list (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (f (quote ("simd"))) (d #t) (k 0)))) (h "1ny438d9pz7l70ls9gfaj34qfcdwyy8p05ykm99c3j5lb6sk4bap")))

(define-public crate-linguee-0.1.2 (c (n "linguee") (v "0.1.2") (d (list (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (f (quote ("simd"))) (d #t) (k 0)))) (h "1qrk6qz9b02h0acnk3fh33cxs4sy0js5pm9scy525nc1pc39mixn")))

(define-public crate-linguee-0.1.3 (c (n "linguee") (v "0.1.3") (d (list (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (f (quote ("simd"))) (d #t) (k 0)))) (h "0wk018f6sxsargs0hfwiinnq6g7np4q902fglmdi4rpb583m96qc")))

(define-public crate-linguee-0.1.4 (c (n "linguee") (v "0.1.4") (d (list (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (f (quote ("simd"))) (d #t) (k 0)))) (h "1s5za655ac6a722cg5hhcym4wq4linzp1rjkj4hk4d49050wiydd")))

(define-public crate-linguee-0.1.5 (c (n "linguee") (v "0.1.5") (d (list (d (n "doe") (r "^0.1.25") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (f (quote ("simd"))) (d #t) (k 0)))) (h "1p89i1nlsccifxa9abjyfjknk0c31kf3cspzvbhf6n6fv9bqaygp")))

(define-public crate-linguee-0.1.6 (c (n "linguee") (v "0.1.6") (d (list (d (n "cok") (r "^0.1.7") (d #t) (k 0)) (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (f (quote ("simd"))) (d #t) (k 0)))) (h "0jh2mk50xsi0kzm6f6prnl5kdsm5l43lykla4zhr0pp19ixxr7zp")))

(define-public crate-linguee-0.1.7 (c (n "linguee") (v "0.1.7") (d (list (d (n "cok") (r "^0.1.7") (d #t) (k 0)) (d (n "doe") (r "^0.1.29") (d #t) (k 0)) (d (n "tl") (r "^0.7.7") (f (quote ("simd"))) (d #t) (k 0)) (d (n "tomcat") (r "^0.1.7") (d #t) (k 0)))) (h "0qg6cfrlqbj7ia5rz57afsmmikhpbqi76799c2906yzdmvl5khgl")))

