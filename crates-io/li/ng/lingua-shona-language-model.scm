(define-module (crates-io li ng lingua-shona-language-model) #:use-module (crates-io))

(define-public crate-lingua-shona-language-model-1.0.0 (c (n "lingua-shona-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0bwk5a8mn3mzazc1s69p24l0f54ffjwmnxwfvimdwmqhpc935aqj")))

(define-public crate-lingua-shona-language-model-1.0.1 (c (n "lingua-shona-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1rg1ky3scng9034cf2fi7b3bfbi4c478dawakxx5ph7z4g1g751q")))

(define-public crate-lingua-shona-language-model-1.1.0 (c (n "lingua-shona-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1fsplxhxx5iw2hq5w7yaijwxqlabqcdvwl2vrp3px97j0b2p55sh")))

