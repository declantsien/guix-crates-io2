(define-module (crates-io li ng lingua-bosnian-language-model) #:use-module (crates-io))

(define-public crate-lingua-bosnian-language-model-1.0.0 (c (n "lingua-bosnian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0g8amv1ww8xmfcxqlv64jm0szafbkfhl22ylra8zq5i5svwazmg5")))

(define-public crate-lingua-bosnian-language-model-1.0.1 (c (n "lingua-bosnian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0z0j5i1gqnqx57k88fayxi3031mlyxb1icjrpkp0bj16bkjb1xkc")))

(define-public crate-lingua-bosnian-language-model-1.1.0 (c (n "lingua-bosnian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0fwckns162l498czskhn5d0ja6s3f9zchxhq1sf2skikb7skdy4q")))

