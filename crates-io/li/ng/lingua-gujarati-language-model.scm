(define-module (crates-io li ng lingua-gujarati-language-model) #:use-module (crates-io))

(define-public crate-lingua-gujarati-language-model-1.0.0 (c (n "lingua-gujarati-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0gqax5s74ri0m6ahd63j1ixiggf7crvsc65qlsvn8qgkfprapyby")))

(define-public crate-lingua-gujarati-language-model-1.0.1 (c (n "lingua-gujarati-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "18lkwz2hkaavv0difkfl959jj318lllwi9lh2f73j2zg36giahr3")))

(define-public crate-lingua-gujarati-language-model-1.1.0 (c (n "lingua-gujarati-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "17lhj4xmln27x3bcdpb1s0v1md9zqzg9x307sarlx3yzzw81pq8f")))

