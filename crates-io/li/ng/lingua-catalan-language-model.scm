(define-module (crates-io li ng lingua-catalan-language-model) #:use-module (crates-io))

(define-public crate-lingua-catalan-language-model-1.0.0 (c (n "lingua-catalan-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "17y6m9fcig9kgr9h56li32gcmfck0gkxm6wkza0284m6axb2xbn3")))

(define-public crate-lingua-catalan-language-model-1.0.1 (c (n "lingua-catalan-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1msp2iiv0crn0iffzkjyv8z0x2rssl1vd4dpbdw7n5q4bzlsx6h9")))

(define-public crate-lingua-catalan-language-model-1.1.0 (c (n "lingua-catalan-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "009jjamnzf5sbhgdgmp9l04rhzg8q3mp31fbwhgrc80k7baarnfh")))

