(define-module (crates-io li ng lingua-danish-language-model) #:use-module (crates-io))

(define-public crate-lingua-danish-language-model-1.0.0 (c (n "lingua-danish-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1a7jv03lzmsivsj3rqlsbgbjyyadbzjia8bkk5v5n5cmjyqqdld2")))

(define-public crate-lingua-danish-language-model-1.0.1 (c (n "lingua-danish-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "133jbwf7vkhwgjal9cf3bn98bxvmraiin49dhhqj6mvrikx93bq4")))

(define-public crate-lingua-danish-language-model-1.1.0 (c (n "lingua-danish-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1ljybd6r0zzib4gl5xn1rzzdq4rvvayg7r0s781cz53yw6ylmp0a")))

