(define-module (crates-io li ng lingua-greek-language-model) #:use-module (crates-io))

(define-public crate-lingua-greek-language-model-1.0.0 (c (n "lingua-greek-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "07a0kfw8h4jlrhbbc09qnj2y1lvhn6sb0kx1d6l8bb3gd0h76g4w")))

(define-public crate-lingua-greek-language-model-1.0.1 (c (n "lingua-greek-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "15a0kgkm4gi95fk7ildh88jv38g69cl7vn6ydqcjd4r4sjf233qw")))

(define-public crate-lingua-greek-language-model-1.1.0 (c (n "lingua-greek-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "06v1rdyzb90qidbs0a8ywzwjy1q4jlh7j29nlqkza424ccbml7ry")))

