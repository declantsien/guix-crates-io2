(define-module (crates-io li ng lingua-arabic-language-model) #:use-module (crates-io))

(define-public crate-lingua-arabic-language-model-1.0.0 (c (n "lingua-arabic-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0d7sy4snzp3c6nx6zf7x19ck0fw7jna0fpkv948m9d5vnkhhina6")))

(define-public crate-lingua-arabic-language-model-1.0.1 (c (n "lingua-arabic-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "068g8mc9pg6pslpibnnqllzp505ydix32c31lmdw1swk62sn787y")))

(define-public crate-lingua-arabic-language-model-1.1.0 (c (n "lingua-arabic-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0ka9pxvdaknbhb0c16jr26wwiw7bxy9pdv6ap82hp6980w87bnka")))

