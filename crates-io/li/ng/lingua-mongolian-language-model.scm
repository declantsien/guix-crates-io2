(define-module (crates-io li ng lingua-mongolian-language-model) #:use-module (crates-io))

(define-public crate-lingua-mongolian-language-model-1.0.0 (c (n "lingua-mongolian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "15jd72apsi39hrzv5cv2rp55bxkgcpxdh9354ykfw6aa4d4b1f4z")))

(define-public crate-lingua-mongolian-language-model-1.0.1 (c (n "lingua-mongolian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0l9ry2gnignalg13lshilk6r7jyigvpaf92yxk92a1ahly4nmjl2")))

(define-public crate-lingua-mongolian-language-model-1.1.0 (c (n "lingua-mongolian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1h92a5h2n3r41awc5wz38dghy6x8q2aphy67irflpx0aj81faggm")))

