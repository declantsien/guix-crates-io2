(define-module (crates-io li ng lingua-french-language-model) #:use-module (crates-io))

(define-public crate-lingua-french-language-model-1.0.0 (c (n "lingua-french-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1c0wy1z5mhf3k0dq44q7qlbniy4gdbw5h9aahmnhx6dcvffrjxz1")))

(define-public crate-lingua-french-language-model-1.0.1 (c (n "lingua-french-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "118df3crdqdi0s5d9vjfq0xx7b898k1ndgpa7wv4qsrcbvvkw7dl")))

(define-public crate-lingua-french-language-model-1.1.0 (c (n "lingua-french-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0vw64k3dgj2vvy2d1yvp5gk1r9xiwjp7dr9n01wi5ch9agh6sh5s")))

