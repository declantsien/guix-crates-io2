(define-module (crates-io li ng lingua-finnish-language-model) #:use-module (crates-io))

(define-public crate-lingua-finnish-language-model-1.0.0 (c (n "lingua-finnish-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0vkyyfy1ligyjh7dk6b8255cffs88571a5rym7ldq994fink85cq")))

(define-public crate-lingua-finnish-language-model-1.0.1 (c (n "lingua-finnish-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1ajc6riq6sq593hxdp7v67bcg757cbgf6a92mp1di1mmk7yplrch")))

(define-public crate-lingua-finnish-language-model-1.1.0 (c (n "lingua-finnish-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0h4rgiv8rsi28l99kvj3iq78ipq67qc4rjqni4kfqwqrjcs0yznh")))

