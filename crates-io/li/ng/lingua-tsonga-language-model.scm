(define-module (crates-io li ng lingua-tsonga-language-model) #:use-module (crates-io))

(define-public crate-lingua-tsonga-language-model-1.0.0 (c (n "lingua-tsonga-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1avxv3pp3hc03likln06bjn8xy88ywm8ld28m1ml9y1az5bczh3n")))

(define-public crate-lingua-tsonga-language-model-1.0.1 (c (n "lingua-tsonga-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0hp28lx095nhrd2q6aq2363ykp31d2fhbwykz28yx7bx88vh96fm")))

(define-public crate-lingua-tsonga-language-model-1.1.0 (c (n "lingua-tsonga-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "01rvr17kbnqyg6ylwk1k6fzddflqxdbmcji5j62zsfxvws9h98as")))

