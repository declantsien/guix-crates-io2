(define-module (crates-io li ng lingua-ganda-language-model) #:use-module (crates-io))

(define-public crate-lingua-ganda-language-model-1.0.0 (c (n "lingua-ganda-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0b0ngc309ixgknrzxb7b666lc4jlpmvbddnkqpvjd98lgv1krkg9")))

(define-public crate-lingua-ganda-language-model-1.0.1 (c (n "lingua-ganda-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0swqrqjvg3gxy5zmw2pf9382vyk7h3xd0n2sqdcpmwpxp9vc1d7x")))

(define-public crate-lingua-ganda-language-model-1.1.0 (c (n "lingua-ganda-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "14j75wcf0wyz4nybcv4cb62ihnh4qlh00xys80i7zgs8ddphx2gr")))

