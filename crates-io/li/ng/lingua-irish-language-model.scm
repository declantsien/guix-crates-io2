(define-module (crates-io li ng lingua-irish-language-model) #:use-module (crates-io))

(define-public crate-lingua-irish-language-model-1.0.0 (c (n "lingua-irish-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "125ln5ynvz7m1ncv3kp6y7sd4d0dz951y7z4cimc5901xq24j8h2")))

(define-public crate-lingua-irish-language-model-1.0.1 (c (n "lingua-irish-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1812021xfajkhz761y209lq2crk064035mbis57x5z6h6jgywbsr")))

(define-public crate-lingua-irish-language-model-1.1.0 (c (n "lingua-irish-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "16biyxa817p5s01sdvq4yizwzjvb3vd4vnn7m9il29r7cvmqn9d6")))

