(define-module (crates-io li ng lingua-xhosa-language-model) #:use-module (crates-io))

(define-public crate-lingua-xhosa-language-model-1.0.0 (c (n "lingua-xhosa-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0hsn3xbl8sdrxa671xfh34y390bmwcax38gfjmgkxvjxksrzqx42")))

(define-public crate-lingua-xhosa-language-model-1.0.1 (c (n "lingua-xhosa-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0dp5bfbd52qfn2z6ngv5wkmw1x4l7c3wnl90knjkylndlp8fzxd4")))

(define-public crate-lingua-xhosa-language-model-1.1.0 (c (n "lingua-xhosa-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1yqqm2r1b33w1nq5464gz7psisvybp1gv947dxyaacsrsd8qi92m")))

