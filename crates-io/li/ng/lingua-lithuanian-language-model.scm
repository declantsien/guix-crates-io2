(define-module (crates-io li ng lingua-lithuanian-language-model) #:use-module (crates-io))

(define-public crate-lingua-lithuanian-language-model-1.0.0 (c (n "lingua-lithuanian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1ky7ks1rwwab9vd247vsbjfk69nqp1g9db629z5mi56sydpd2brs")))

(define-public crate-lingua-lithuanian-language-model-1.0.1 (c (n "lingua-lithuanian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1jypc2fwmbyni5x786cr4dg8pk2s5srkp93w791p4pin6skd3kqw")))

(define-public crate-lingua-lithuanian-language-model-1.1.0 (c (n "lingua-lithuanian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "07jb3js4f2c3sbkxrwnyv0dnhi77m8xn68z83xxyfwlg7icqxdm9")))

