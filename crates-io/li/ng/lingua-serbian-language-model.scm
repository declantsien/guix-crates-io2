(define-module (crates-io li ng lingua-serbian-language-model) #:use-module (crates-io))

(define-public crate-lingua-serbian-language-model-1.0.0 (c (n "lingua-serbian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "17ychhv0jq98h7p6ggsqy15m45jn81bsqvaaybf8v77kmcfwyf45")))

(define-public crate-lingua-serbian-language-model-1.0.1 (c (n "lingua-serbian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1d8f9lrgi2ni1b5mhdfafp8zllbcxcgxqz0xf60jw1i39831jkwv")))

(define-public crate-lingua-serbian-language-model-1.1.0 (c (n "lingua-serbian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1nqn0ixrxngs9104bmx4hqw240bdcrrqf3g1m7srp5ip66z83cdp")))

