(define-module (crates-io li ng lingua-estonian-language-model) #:use-module (crates-io))

(define-public crate-lingua-estonian-language-model-1.0.0 (c (n "lingua-estonian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0fvhzprb2ylmvd805lhinff35hrchbxyhf2pl1acvs2010dz24d3")))

(define-public crate-lingua-estonian-language-model-1.0.1 (c (n "lingua-estonian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1k2k6c2hsf171aaa3mw08cf8c3pk7vm7i6y6ksqd7xl1z5k5madj")))

(define-public crate-lingua-estonian-language-model-1.1.0 (c (n "lingua-estonian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0irw3dm4yr8av038vpdsib8k48q3lj9jijyq85f2mj37i9yw09cs")))

