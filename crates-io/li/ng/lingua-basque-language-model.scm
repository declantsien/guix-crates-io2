(define-module (crates-io li ng lingua-basque-language-model) #:use-module (crates-io))

(define-public crate-lingua-basque-language-model-1.0.0 (c (n "lingua-basque-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0npgaji2ig0wlhxa1pqxh9317pbblja4y3bn3w6z3ah8x0nxy88w")))

(define-public crate-lingua-basque-language-model-1.0.1 (c (n "lingua-basque-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1gpg2w1q4y7ykj23nq4b21h079f9k5qdg7vm2ghgrw6xk7vhp3mx")))

(define-public crate-lingua-basque-language-model-1.1.0 (c (n "lingua-basque-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "194pmk2vhb33fa2xxf9cpa3k0wim00xj60a6cadh2g1x432a02ki")))

