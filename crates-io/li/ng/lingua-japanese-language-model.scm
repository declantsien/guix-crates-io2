(define-module (crates-io li ng lingua-japanese-language-model) #:use-module (crates-io))

(define-public crate-lingua-japanese-language-model-1.0.0 (c (n "lingua-japanese-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "15pzl5zzqabs1j6yigcaagj197m3h09klggah90lnmsmklm8na85")))

(define-public crate-lingua-japanese-language-model-1.0.1 (c (n "lingua-japanese-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0a89g0n44lkz81rmgdjiskv6kr5a8z809wp9l49yhxfyw8kbs01x")))

(define-public crate-lingua-japanese-language-model-1.1.0 (c (n "lingua-japanese-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1a01ymnii2yxwis2jyhyn9mjbmzbs097jb6svlw0ig51jsm98jxk")))

