(define-module (crates-io li ng lingua-marathi-language-model) #:use-module (crates-io))

(define-public crate-lingua-marathi-language-model-1.0.0 (c (n "lingua-marathi-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0gnbdrvlp601cgymyl1c8lgvcpjaf44y4zakcmlvnj6azps3ms1l")))

(define-public crate-lingua-marathi-language-model-1.0.1 (c (n "lingua-marathi-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0rwkh0q9df1rp9zgvrwamgfkf4qy6mkx1rjzw321ip8dyjaj8il8")))

(define-public crate-lingua-marathi-language-model-1.1.0 (c (n "lingua-marathi-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "184bivcnvmvkdlij1v154vdw5fygng54q1hwahpg43v7v74438di")))

