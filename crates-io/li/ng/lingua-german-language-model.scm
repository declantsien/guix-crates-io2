(define-module (crates-io li ng lingua-german-language-model) #:use-module (crates-io))

(define-public crate-lingua-german-language-model-1.0.0 (c (n "lingua-german-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1c4ai7frzqwc51qd1fw31aidk3qi7gbh54a0ybijp2fyalqxjzp4")))

(define-public crate-lingua-german-language-model-1.0.1 (c (n "lingua-german-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "05f1zj3kva17w47g6h16qfi5a7kwz088ymmlmw9h69kgj8kbhy5q")))

(define-public crate-lingua-german-language-model-1.1.0 (c (n "lingua-german-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "04qad6zjjq7hkdxxlvcnidlqhpiy6dwnksn02pa14r9bahfh9m0d")))

