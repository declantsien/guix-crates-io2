(define-module (crates-io li ng lingua-russian-language-model) #:use-module (crates-io))

(define-public crate-lingua-russian-language-model-1.0.0 (c (n "lingua-russian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "08jfcd1qgs54w9lgqn4xa3gyc9nyihy92bf1l6bc5a58fik4jv40")))

(define-public crate-lingua-russian-language-model-1.0.1 (c (n "lingua-russian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "11gxy1qvip3yy3v2pn4ap0zi0xq89wc2qqziny62jd2q78v95c72")))

(define-public crate-lingua-russian-language-model-1.1.0 (c (n "lingua-russian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0vk7i92b6naw7scd23wpsilrxpbcq3q2iis303yf83p98dk7vafy")))

