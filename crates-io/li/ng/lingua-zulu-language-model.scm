(define-module (crates-io li ng lingua-zulu-language-model) #:use-module (crates-io))

(define-public crate-lingua-zulu-language-model-1.0.0 (c (n "lingua-zulu-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "00b8f04cr7s9rj69l8vb7yymx6z2yfj3vmj44f8g2qacx59cx5kv")))

(define-public crate-lingua-zulu-language-model-1.0.1 (c (n "lingua-zulu-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "00wl15y5q1gywwqhkas7g4qhk1bm62lnglmn4rv6rdzwx5gvsl27")))

(define-public crate-lingua-zulu-language-model-1.1.0 (c (n "lingua-zulu-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1qmbi67lxyphda2w9dxiqhcngrzwg8lp8a7xgzrpil0jyk3vby33")))

