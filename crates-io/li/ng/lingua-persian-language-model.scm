(define-module (crates-io li ng lingua-persian-language-model) #:use-module (crates-io))

(define-public crate-lingua-persian-language-model-1.0.0 (c (n "lingua-persian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1cgdwp2p17i9zv6mlqjqj2pnywckqna4f3za65ddyazsxx9fm596")))

(define-public crate-lingua-persian-language-model-1.0.1 (c (n "lingua-persian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "01xmyqs39svyff8vplaick1b8li43bzbij6fswddak5j47di2l3r")))

(define-public crate-lingua-persian-language-model-1.1.0 (c (n "lingua-persian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1i36yaafs9qvp6z1p2fka3l0z6zwsqjmk7ydl759mybxmjsajfq4")))

