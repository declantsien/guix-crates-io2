(define-module (crates-io li ng lingua-armenian-language-model) #:use-module (crates-io))

(define-public crate-lingua-armenian-language-model-1.0.0 (c (n "lingua-armenian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1ic2hcxg3j65vb7xzz35p7i0b1qabh2j1q6p33vwnnidsl2hk5c7")))

(define-public crate-lingua-armenian-language-model-1.0.1 (c (n "lingua-armenian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1qvc5lwmdab4v1b7i5p2w6s1c9wlx0fhbxdk28ksq7sm5ashs1nr")))

(define-public crate-lingua-armenian-language-model-1.1.0 (c (n "lingua-armenian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "03xwf86c0q7kkpwk2p1jy1xiv2b4raiy1wnfz5fmqd3650cg89ni")))

