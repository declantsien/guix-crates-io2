(define-module (crates-io li ng lingua-spanish-language-model) #:use-module (crates-io))

(define-public crate-lingua-spanish-language-model-1.0.0 (c (n "lingua-spanish-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0z392n9qggmryhi8s695b58crcc4ppq0j2xfjhsrqhwrb1b7xp1w")))

(define-public crate-lingua-spanish-language-model-1.0.1 (c (n "lingua-spanish-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "07pmfqnij0l1ik6clap0hzqyizy9plwfpl7dw8s6j5r1jr8zz9ag")))

(define-public crate-lingua-spanish-language-model-1.1.0 (c (n "lingua-spanish-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0w30riqiwp6qxjpkcmsiblzdinqq5a14fq70nr9nv7cvvszfjnyl")))

