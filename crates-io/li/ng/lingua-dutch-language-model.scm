(define-module (crates-io li ng lingua-dutch-language-model) #:use-module (crates-io))

(define-public crate-lingua-dutch-language-model-1.0.0 (c (n "lingua-dutch-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0y41s4x9yp095wivp92w5lq0n9rpgl19sfhz7cwiscmkwcvbh4ga")))

(define-public crate-lingua-dutch-language-model-1.0.1 (c (n "lingua-dutch-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1x97kv06874lzxryzgng24n0zp01rnipkib5h5fjng9p0hwk0jmf")))

(define-public crate-lingua-dutch-language-model-1.1.0 (c (n "lingua-dutch-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1f9xb0rspiq5lrc08qrp05izgncilcnd7dcha7c69sd56mbdpwrh")))

