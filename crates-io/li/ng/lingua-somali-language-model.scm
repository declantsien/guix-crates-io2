(define-module (crates-io li ng lingua-somali-language-model) #:use-module (crates-io))

(define-public crate-lingua-somali-language-model-1.0.0 (c (n "lingua-somali-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0asvxsbml4bg1cfbyj33w3drl8gpx9xsphcan373lfgzd4ra5xxg")))

(define-public crate-lingua-somali-language-model-1.0.1 (c (n "lingua-somali-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0azmp83pbp05ji50rn4sqck4byk29zjv7qvn97wl45ld2zf67dfc")))

(define-public crate-lingua-somali-language-model-1.1.0 (c (n "lingua-somali-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1cyh8mkbi9ypnbnflnw18cvfvmbb0v75m65gr7mfbrq1i5psds6x")))

