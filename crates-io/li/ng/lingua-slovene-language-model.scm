(define-module (crates-io li ng lingua-slovene-language-model) #:use-module (crates-io))

(define-public crate-lingua-slovene-language-model-1.0.0 (c (n "lingua-slovene-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0s4s4c56bmgmdr3cbgwjyzcwpnjgcbi4h0xd70mwrmvyicbc5l75")))

(define-public crate-lingua-slovene-language-model-1.0.1 (c (n "lingua-slovene-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "12d6x6adpzaykf00li743lr2np3aqivj1p94cqxzwqgw0hxcz4br")))

(define-public crate-lingua-slovene-language-model-1.1.0 (c (n "lingua-slovene-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1ha9v7ppp01wc0imr31x1nz2cynl90rlkbn7r660dwyd186l3z29")))

