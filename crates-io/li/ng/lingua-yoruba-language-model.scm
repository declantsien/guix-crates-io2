(define-module (crates-io li ng lingua-yoruba-language-model) #:use-module (crates-io))

(define-public crate-lingua-yoruba-language-model-1.0.0 (c (n "lingua-yoruba-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1f8ykcidpdspw68j7a3vxm9r9xbcz6miag8hf6k75iwjbw1bc9y3")))

(define-public crate-lingua-yoruba-language-model-1.0.1 (c (n "lingua-yoruba-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1cxc5ldgwn1c9q5icbwpjb635wc1qcm22nkixf4pmf4yd3wpgnz1")))

(define-public crate-lingua-yoruba-language-model-1.1.0 (c (n "lingua-yoruba-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1vxhyqbp2c6r9493vsd5m4m2kb832mdzbyqq5k975dzz2m93dxjv")))

