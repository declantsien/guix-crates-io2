(define-module (crates-io li ng lingua-belarusian-language-model) #:use-module (crates-io))

(define-public crate-lingua-belarusian-language-model-1.0.0 (c (n "lingua-belarusian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0hxx5q7lygn5rli0c2kid9h66dvnha72bn3i8008y0pjmjvxnjlj")))

(define-public crate-lingua-belarusian-language-model-1.0.1 (c (n "lingua-belarusian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1wddb867yb6h1prg5w1xg4nj8sw6yykvrpfa500vxaazsll628k5")))

(define-public crate-lingua-belarusian-language-model-1.1.0 (c (n "lingua-belarusian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1xc0fp92n4339p0p1h8gci7xccjkp8q1ads9i066yr67431nxdx5")))

