(define-module (crates-io li ng lingua-czech-language-model) #:use-module (crates-io))

(define-public crate-lingua-czech-language-model-1.0.0 (c (n "lingua-czech-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1nw4p3imx8f8a3k7sz3bvl25j48nmac7mma4a15nhqswak86pys6")))

(define-public crate-lingua-czech-language-model-1.0.1 (c (n "lingua-czech-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0vp4231ihdx31bjf1jnshfpmsfiajrsxsm3i4ifpg20xhbnam3fd")))

(define-public crate-lingua-czech-language-model-1.1.0 (c (n "lingua-czech-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1lfibi33vg2wai4h6dmk58rh9qngr0lj1a0dq839np6bal0gb67d")))

