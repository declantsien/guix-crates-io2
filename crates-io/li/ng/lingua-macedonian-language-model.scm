(define-module (crates-io li ng lingua-macedonian-language-model) #:use-module (crates-io))

(define-public crate-lingua-macedonian-language-model-1.0.0 (c (n "lingua-macedonian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0xqvss7brvxcis0k44is9yjizgwbnj5jw83avlcqqgdgii12iqdp")))

(define-public crate-lingua-macedonian-language-model-1.0.1 (c (n "lingua-macedonian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1nw3h514j6yhcgayy7qdvf8d07nx6n0ji5zshcx6hwkc3il0gns5")))

(define-public crate-lingua-macedonian-language-model-1.1.0 (c (n "lingua-macedonian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1zknj87yc24ha6fcag6cbcsy1g1rvr1di07dh0ai6xhpxk28na0y")))

