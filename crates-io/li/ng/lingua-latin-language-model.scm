(define-module (crates-io li ng lingua-latin-language-model) #:use-module (crates-io))

(define-public crate-lingua-latin-language-model-1.0.0 (c (n "lingua-latin-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0dm96cq94c81vmjrd0x1ga30ixsx20g5w6ljqhwr65fd4cg4w9sh")))

(define-public crate-lingua-latin-language-model-1.0.1 (c (n "lingua-latin-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1cflaimh0xaw3rbf6dfx5b82kmjhzb01k0l74wk1l3w4vbkar2y4")))

(define-public crate-lingua-latin-language-model-1.1.0 (c (n "lingua-latin-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1mxl53l4w1vdkgkhq32gq65qfnpvmqsiiav0f4wibbk8gvr242bd")))

