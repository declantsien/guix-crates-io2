(define-module (crates-io li ng lingua-hungarian-language-model) #:use-module (crates-io))

(define-public crate-lingua-hungarian-language-model-1.0.0 (c (n "lingua-hungarian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1n2dj7mf3srz2l069w16h7p1lsxjb8pwa9piqmmz0bd885ccs0v6")))

(define-public crate-lingua-hungarian-language-model-1.0.1 (c (n "lingua-hungarian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "1qlrxbamnsjfzam5pzc20g6icsp6wzc18szifg6hwv24ilgql10v")))

(define-public crate-lingua-hungarian-language-model-1.1.0 (c (n "lingua-hungarian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0vn9v3mr542ys1hc4j3kwhj2g0ls7cmqmvhs9a69fsv9dhkjamc8")))

