(define-module (crates-io li ng lingua-bulgarian-language-model) #:use-module (crates-io))

(define-public crate-lingua-bulgarian-language-model-1.0.0 (c (n "lingua-bulgarian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0lv06hjaps0q68kk593hdnb75rf7w6shd5kpjvy0lbivmk1grgj5")))

(define-public crate-lingua-bulgarian-language-model-1.0.1 (c (n "lingua-bulgarian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "13rjqh9pr7avsjpg61jgbqqq1fhbp8x8jcnqbvsxanvck0g5a159")))

(define-public crate-lingua-bulgarian-language-model-1.1.0 (c (n "lingua-bulgarian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "18d590d3q0fpy7q3hs8l7bdlxn1xz5w42nashcssy6a4nfbr6qzn")))

