(define-module (crates-io li ng lingua-slovak-language-model) #:use-module (crates-io))

(define-public crate-lingua-slovak-language-model-1.0.0 (c (n "lingua-slovak-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "047prafz4a3912ik4pdzsnghfi6f1hmqkh2icvl4cfwyb6nrlk6r")))

(define-public crate-lingua-slovak-language-model-1.0.1 (c (n "lingua-slovak-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0qf5z9pbcinlkgaphi3k2i7dbw4dj4w583p5njijw32y9zgcn30m")))

(define-public crate-lingua-slovak-language-model-1.1.0 (c (n "lingua-slovak-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "0h6wwdmyh2bjfwyzz421lakrhhn2xwqhgj3s6rcrds95cygfkqa4")))

