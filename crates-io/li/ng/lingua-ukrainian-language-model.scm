(define-module (crates-io li ng lingua-ukrainian-language-model) #:use-module (crates-io))

(define-public crate-lingua-ukrainian-language-model-1.0.0 (c (n "lingua-ukrainian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "005bwvsizxa7szv17vm546nj6adl42z5nmlm9ya2s5lkpn2cpr5r")))

(define-public crate-lingua-ukrainian-language-model-1.0.1 (c (n "lingua-ukrainian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "10l76infwp7i73smspfjz8fj39b7cza773j529g2d1n10q4z8fac")))

(define-public crate-lingua-ukrainian-language-model-1.1.0 (c (n "lingua-ukrainian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "06hk3z9a7lydwicm89vd5bdlqv6pk7sfa1rvzrv5zv5psifh7v8l")))

