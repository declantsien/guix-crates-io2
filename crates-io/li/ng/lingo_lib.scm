(define-module (crates-io li ng lingo_lib) #:use-module (crates-io))

(define-public crate-lingo_lib-0.1.0 (c (n "lingo_lib") (v "0.1.0") (d (list (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.3") (d #t) (k 0)))) (h "1ivmpdp3msfqc16sj9kl7wfqg299cd3glf1pk2b65b318xiajv40")))

(define-public crate-lingo_lib-0.2.0 (c (n "lingo_lib") (v "0.2.0") (d (list (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.3") (d #t) (k 0)))) (h "0fl1pdvpjy0a03bqiblb3wwvkf4r5q5kmn9whnzzgdh5ggbi0kgh")))

(define-public crate-lingo_lib-0.2.1 (c (n "lingo_lib") (v "0.2.1") (d (list (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.3") (d #t) (k 0)))) (h "09lxchrh58lff9dcc76ccxsipcyk0pcsrlks0my77z69drrx8l81")))

(define-public crate-lingo_lib-0.2.2 (c (n "lingo_lib") (v "0.2.2") (d (list (d (n "strum") (r "^0.24.1") (d #t) (k 0)) (d (n "strum_macros") (r "^0.24.3") (d #t) (k 0)) (d (n "sys-locale") (r "^0.2.3") (d #t) (k 0)))) (h "1j2z6kz939sa2rdr9m59xrwq31mcmm1vgwfxfxansldpwmwmrclf")))

