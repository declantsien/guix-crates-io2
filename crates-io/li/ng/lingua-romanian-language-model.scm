(define-module (crates-io li ng lingua-romanian-language-model) #:use-module (crates-io))

(define-public crate-lingua-romanian-language-model-1.0.0 (c (n "lingua-romanian-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "0vsxdj8jgx0nd1m9as78vr2j1livx3s450ai3sacl0hvl0fnm4kr")))

(define-public crate-lingua-romanian-language-model-1.0.1 (c (n "lingua-romanian-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0xis8b19kqjsl5fv7ydf2nlgvb5jksvz799c45bfg9rs9w9v8i9n")))

(define-public crate-lingua-romanian-language-model-1.1.0 (c (n "lingua-romanian-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "02rrisvw7q3dg33h4y1wqq0j532hr03yhw61ks96app0v09qqhmh")))

