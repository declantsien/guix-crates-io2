(define-module (crates-io li ng lingua-tagalog-language-model) #:use-module (crates-io))

(define-public crate-lingua-tagalog-language-model-1.0.0 (c (n "lingua-tagalog-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "1ayxkq5i7gp6d8b7yddpvh26ss43mq552q97f2i0nk4q4n6aa1hk")))

(define-public crate-lingua-tagalog-language-model-1.0.1 (c (n "lingua-tagalog-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "0k4fsgxr9ya8ihyrl66laf61g5v521iyv79z4qwlib5id3q8rg4k")))

(define-public crate-lingua-tagalog-language-model-1.1.0 (c (n "lingua-tagalog-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "1c2w9ii3kfphzvgdrcx9a5qnjvyijayxkl06dzsn52kax24vhp6c")))

