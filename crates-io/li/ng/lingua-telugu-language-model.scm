(define-module (crates-io li ng lingua-telugu-language-model) #:use-module (crates-io))

(define-public crate-lingua-telugu-language-model-1.0.0 (c (n "lingua-telugu-language-model") (v "1.0.0") (d (list (d (n "include_dir") (r "^0.6.0") (d #t) (k 0)))) (h "15bcginlx11wfpvvj8wxc4gn16mskca70gg9zi7imgpirkjkaykl")))

(define-public crate-lingua-telugu-language-model-1.0.1 (c (n "lingua-telugu-language-model") (v "1.0.1") (d (list (d (n "include_dir") (r "^0.7.2") (d #t) (k 0)))) (h "11zjrvz53xkpnxs1dx5d2w91jsw83ifixacn1b2m7zvi4s3ssai3")))

(define-public crate-lingua-telugu-language-model-1.1.0 (c (n "lingua-telugu-language-model") (v "1.1.0") (d (list (d (n "include_dir") (r "^0.7.3") (d #t) (k 0)))) (h "01xy3k8cw3j4ndy9f927j7wxvk0cm52gs8203x7fyiy25yszj085")))

