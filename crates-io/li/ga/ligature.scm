(define-module (crates-io li ga ligature) #:use-module (crates-io))

(define-public crate-ligature-0.1.0 (c (n "ligature") (v "0.1.0") (h "1jn02a1ryd3s7wj1ql8iw23ra7xxjbfjp00miqf1w2zrblaj14km")))

(define-public crate-ligature-0.2.0 (c (n "ligature") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1qb61vk52cjkgv857w0nqs6dl7iac7ps9z81f5i3hx18j82dzyaz")))

(define-public crate-ligature-0.3.0 (c (n "ligature") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1gfwlprmg4kz72rrvq7ayk6wvljx9nfvymf1ws6m54rirq7m1fh0")))

(define-public crate-ligature-0.4.0 (c (n "ligature") (v "0.4.0") (d (list (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1vbkc0zrasy54g1h1pjrlgds50yb9ifgqm7n9n29pzpi1n6gzbp7")))

(define-public crate-ligature-0.5.0 (c (n "ligature") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0qhmwrn6n5s07mdjvwbi9s9bgdgn0vaj9qjsx1rv9wshw6py9izv")))

(define-public crate-ligature-0.6.0 (c (n "ligature") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1kx3n7312wwdzqhb2834in4hvfqnwq9fy622h6cfw4zfrx6i6438")))

(define-public crate-ligature-0.7.0 (c (n "ligature") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "03b8hcz3bx6hlcx9bb7rknkbpw41rm4fm89cs5j1s44khinj9756")))

