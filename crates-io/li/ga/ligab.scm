(define-module (crates-io li ga ligab) #:use-module (crates-io))

(define-public crate-ligab-0.1.0 (c (n "ligab") (v "0.1.0") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "http_req_wasi") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0a065f1l49kyggjv9z5389fh48y8wzj39wr78llb8viiqp39h4il")))

(define-public crate-ligab-0.1.1 (c (n "ligab") (v "0.1.1") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "http_req_wasi") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)))) (h "0rzgc1a4sbnn1212h5fjm301ry5yr63ihs0jfffxcllms1z930pl")))

(define-public crate-ligab-0.1.2 (c (n "ligab") (v "0.1.2") (d (list (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "http_req_wasi") (r "^0.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "serde_urlencoded") (r "^0.7.1") (d #t) (k 0)))) (h "1psranj51wijvcbv6bd4z7s024r8g0pfv4fy5jwk0hj6p5zqgccx")))

