(define-module (crates-io li ga ligature-sqlite) #:use-module (crates-io))

(define-public crate-ligature-sqlite-0.1.0 (c (n "ligature-sqlite") (v "0.1.0") (d (list (d (n "ligature") (r "^0.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (d #t) (k 0)))) (h "1id0laz9mcriry5a9idxr72j92rd0lpsy2lgfcp01bv7lvl9p7r4")))

