(define-module (crates-io li ga ligature-repl) #:use-module (crates-io))

(define-public crate-ligature-repl-0.1.0-SNAPSHOT (c (n "ligature-repl") (v "0.1.0-SNAPSHOT") (d (list (d (n "lig") (r "^0.1") (d #t) (k 0)) (d (n "ligature") (r "^0.5") (d #t) (k 0)) (d (n "ligature-in-memory") (r "^0.1") (d #t) (k 0)) (d (n "wander") (r "^0.1") (d #t) (k 0)))) (h "0qk755z3v9jxmb10kycm7blypbc9z08hp26nwnj64kpk9hdq0c9r")))

(define-public crate-ligature-repl-0.1.0 (c (n "ligature-repl") (v "0.1.0") (d (list (d (n "lig") (r "^0.1") (d #t) (k 0)) (d (n "ligature") (r "^0.5") (d #t) (k 0)) (d (n "ligature-in-memory") (r "^0.1") (d #t) (k 0)) (d (n "wander") (r "^0.1") (d #t) (k 0)))) (h "0nlzh2wvww9kkdqqvgmcrsak81ll0wd65jlls3z9vmby4gdwsxd2")))

