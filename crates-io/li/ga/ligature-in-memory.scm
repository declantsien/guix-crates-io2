(define-module (crates-io li ga ligature-in-memory) #:use-module (crates-io))

(define-public crate-ligature-in-memory-0.1.0-SNAPSHOT (c (n "ligature-in-memory") (v "0.1.0-SNAPSHOT") (d (list (d (n "ligature") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.9") (d #t) (k 0)))) (h "0n101v7qz8g6s6acgkck8pxvhsjl8rv5agqym4n4zmn29mmrijcp")))

(define-public crate-ligature-in-memory-0.1.0 (c (n "ligature-in-memory") (v "0.1.0") (d (list (d (n "ligature") (r "^0.5") (d #t) (k 0)) (d (n "rpds") (r "^0.9") (d #t) (k 0)))) (h "1gda6qrrjbb009ha8dcp2c0vhsylzcsz98r62yhwgxi4n8mnlj7a")))

