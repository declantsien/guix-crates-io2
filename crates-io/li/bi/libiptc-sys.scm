(define-module (crates-io li bi libiptc-sys) #:use-module (crates-io))

(define-public crate-libiptc-sys-0.1.0 (c (n "libiptc-sys") (v "0.1.0") (d (list (d (n "capabilities") (r "0.2.*") (d #t) (k 2)) (d (n "libc") (r "0.2.*") (d #t) (k 0)) (d (n "pkg-config") (r "0.3.*") (d #t) (k 1)))) (h "00wycd0k5f5278xrc9jy6g8hfkw7dw1n3wp4lci7c7ffhdnpkjk4")))

