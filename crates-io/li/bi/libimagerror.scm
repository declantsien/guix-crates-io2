(define-module (crates-io li bi libimagerror) #:use-module (crates-io))

(define-public crate-libimagerror-0.2.0 (c (n "libimagerror") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "125c9qis5a4ad2szp15nvhzm00b3p3xk455wg11aflakj4civxyk")))

(define-public crate-libimagerror-0.3.0 (c (n "libimagerror") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1331qyz9d9jclr5i4rizwqij0pi3ckra8kkaq3j5bqbfsf55ml8x")))

(define-public crate-libimagerror-0.4.0 (c (n "libimagerror") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.9") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "1fzrz8n5s39ixnllwbm7pd7gxrgnjdiwf6g1wrygx2g80ps0wzha")))

(define-public crate-libimagerror-0.5.0 (c (n "libimagerror") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)))) (h "0bw8v0fcbm0k8lqah05288s7zx2xjgjn2byfj3wrh3df9rp0z874")))

(define-public crate-libimagerror-0.6.0 (c (n "libimagerror") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0mmckbs1z244vx2x7xf404n370jhkk69v9ar0jp9yvgp1wmm5fy0")))

(define-public crate-libimagerror-0.6.2 (c (n "libimagerror") (v "0.6.2") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "0zsdhmd5157glmp0v022kscj3vlsknbfqw1y4sb4d9x8z9fx01b4")))

(define-public crate-libimagerror-0.6.3 (c (n "libimagerror") (v "0.6.3") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "10fhvkxz4lih59r9rciy0s2x7x7znj7chhyhjrna7jv2p6as99g4")))

(define-public crate-libimagerror-0.6.4 (c (n "libimagerror") (v "0.6.4") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)))) (h "1v6njvbdpxdsbylvqz9bv887dl1hi38gw3j0gfm79qbzgvj975x4")))

(define-public crate-libimagerror-0.7.0 (c (n "libimagerror") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1iqq9rin4majwqvljda7xk9lvsfliqxnb502rhavl5yi4kvd3y7l")))

(define-public crate-libimagerror-0.7.1 (c (n "libimagerror") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1c8fblbpbpanpw9wkvm5jpz02vpc92y8n44xbibzjpmyf44w11np")))

(define-public crate-libimagerror-0.8.0 (c (n "libimagerror") (v "0.8.0") (d (list (d (n "ansi_term") (r "^0.10") (d #t) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0lgrh88flcnis0bc9rwjp0rjvd7ny3947kmxfdr4j9diw5dcyyh9")))

(define-public crate-libimagerror-0.9.0 (c (n "libimagerror") (v "0.9.0") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0sar7rayr92m190sa5j8k2q07mnjpmw020v9y6bmf18gqr4wyf9i")))

(define-public crate-libimagerror-0.9.1 (c (n "libimagerror") (v "0.9.1") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05sk2liv28jh5558jfd51saacfqvn19x2ygm70wqydjh3plapw53")))

(define-public crate-libimagerror-0.9.2 (c (n "libimagerror") (v "0.9.2") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0gavq2nj1i0ljacpl6xjmv7gf3ga2qgichdplkyb9ckvfvgfpz2x")))

(define-public crate-libimagerror-0.9.3 (c (n "libimagerror") (v "0.9.3") (d (list (d (n "ansi_term") (r "^0.11") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0qyrp1acjglm2yrnybkqf14ayhvak14fj580sj8q17hryc3sldqk")))

(define-public crate-libimagerror-0.10.0 (c (n "libimagerror") (v "0.10.0") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "18ijd63lrjhjipg0kafxdy9jq66cp4p9apq4hz32rjhscpp5q20r")))

(define-public crate-libimagerror-0.10.1 (c (n "libimagerror") (v "0.10.1") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)))) (h "1fp3wpsihl58an757l7bbrz2majsslmakcy8nkjbfn9njr02rax3")))

