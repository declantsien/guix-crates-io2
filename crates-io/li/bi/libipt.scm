(define-module (crates-io li bi libipt) #:use-module (crates-io))

(define-public crate-libipt-0.1.0 (c (n "libipt") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libipt-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)))) (h "071k60ffjllrvqrzgmn886gysr7ggwy1mgm2319pk4v0fn1l3vzy")))

(define-public crate-libipt-0.1.1 (c (n "libipt") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libipt-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)))) (h "1fxmiv0mjh6xlb0yz8jih7mqx7yyji79rmvzjd391q49f5myy777")))

(define-public crate-libipt-0.1.2 (c (n "libipt") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libipt-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)))) (h "1ib4rv8q3mjwk4gbig7c150812zyscbyw495aaf1swbwifx6kqra")))

(define-public crate-libipt-0.1.3 (c (n "libipt") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libipt-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)))) (h "1q8j8kh6sx12rp27zhn0rh31rchax0mdmys7f8w50cy4llzjdgq4")))

(define-public crate-libipt-0.1.4 (c (n "libipt") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "libipt-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "num_enum") (r "^0.4.2") (d #t) (k 0)))) (h "144dw0l64qy4180s3rc1qdwac3nb48py1gq4p53wprv8s97r25mh")))

