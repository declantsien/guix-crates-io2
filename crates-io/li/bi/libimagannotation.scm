(define-module (crates-io li bi libimagannotation) #:use-module (crates-io))

(define-public crate-libimagannotation-0.3.0 (c (n "libimagannotation") (v "0.3.0") (d (list (d (n "lazy_static") (r "^0.1.15") (d #t) (k 0)) (d (n "libimagentrylink") (r "^0.3.0") (d #t) (k 0)) (d (n "libimagerror") (r "^0.3.0") (d #t) (k 0)) (d (n "libimagnotes") (r "^0.3.0") (d #t) (k 0)) (d (n "libimagstore") (r "^0.3.0") (d #t) (k 0)) (d (n "libimagutil") (r "^0.3.0") (d #t) (k 0)) (d (n "toml") (r "^0.4") (d #t) (k 0)) (d (n "uuid") (r "^0.3.1") (f (quote ("v4"))) (d #t) (k 0)))) (h "0lnx849jmv69irjzhi04hd2nfg2indl4awnsv4qdid834y7mbdl1")))

