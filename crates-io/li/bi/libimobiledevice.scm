(define-module (crates-io li bi libimobiledevice) #:use-module (crates-io))

(define-public crate-libimobiledevice-0.1.0 (c (n "libimobiledevice") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "chrono") (r "^0.4.11") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 2)) (d (n "libc") (r "^0.2.67") (d #t) (k 0)) (d (n "libplist") (r "^0.1.0") (d #t) (k 0)) (d (n "paste") (r "^0.1.10") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)) (d (n "uuid") (r "^0.5.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0l1rvg53rlpabij7j7jz4gam2n3dqknv0wi5gscb0nxwb8s0cqzm")))

