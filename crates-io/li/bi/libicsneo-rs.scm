(define-module (crates-io li bi libicsneo-rs) #:use-module (crates-io))

(define-public crate-libicsneo-rs-0.1.0 (c (n "libicsneo-rs") (v "0.1.0") (d (list (d (n "libicsneo-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1yzl86872p1z8s9rjg629s9x21zc22v5b9azh8lyqiy3pjmh4zbd") (y #t)))

(define-public crate-libicsneo-rs-0.1.1 (c (n "libicsneo-rs") (v "0.1.1") (d (list (d (n "libicsneo-sys") (r "^0.1.3") (d #t) (k 0)))) (h "17wqcjw0vaycpwz2i6j1kipgpvxsfh3yph9ml3yx6fla7djnji6g") (y #t)))

(define-public crate-libicsneo-rs-0.1.2 (c (n "libicsneo-rs") (v "0.1.2") (d (list (d (n "libicsneo-sys") (r "^0.1.6") (d #t) (k 0)))) (h "0fdnzk6lh2ycsl3dj13fb6dzw35m7w25h7al4k1xgqqxkaxy11m2") (f (quote (("test_zero_devices") ("default" "test_zero_devices")))) (y #t)))

(define-public crate-libicsneo-rs-0.1.3 (c (n "libicsneo-rs") (v "0.1.3") (d (list (d (n "libicsneo-sys") (r "^0.1.9") (d #t) (k 0)))) (h "01zq6ir4y56fbbdn7gks6pg29ybvzczrd30rj5rgc8ryb516xyxs") (f (quote (("test_zero_devices") ("default" "test_zero_devices")))) (y #t)))

(define-public crate-libicsneo-rs-0.1.4 (c (n "libicsneo-rs") (v "0.1.4") (d (list (d (n "libicsneo-sys") (r "^0.1.9") (d #t) (k 0)))) (h "1vwwcnb3r6156pap46rpc55bjbkg2ylspp82nx7h762i0a3mcvvm") (f (quote (("test_zero_devices") ("default" "test_zero_devices")))) (y #t)))

(define-public crate-libicsneo-rs-0.1.5 (c (n "libicsneo-rs") (v "0.1.5") (d (list (d (n "libicsneo-sys") (r "^0.1.14") (d #t) (k 0)) (d (n "pyo3") (r "^0.17.3") (f (quote ("extension-module" "abi3" "abi3-py37" "anyhow" "chrono"))) (o #t) (d #t) (k 0)))) (h "0932a85fyy3qvxnfddi886d007qgsqr5jpxwcfv7r9yx1d97ycgd") (f (quote (("test_zero_devices") ("python" "pyo3") ("default" "test_zero_devices")))) (y #t)))

(define-public crate-libicsneo-rs-0.2.0 (c (n "libicsneo-rs") (v "0.2.0") (d (list (d (n "libicsneo-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0n50hrkr11y9vzxd57xz6si6l8p7amszwmzwq81qd0yrvnclqf8y") (y #t)))

