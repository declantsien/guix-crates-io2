(define-module (crates-io li bi libicmp) #:use-module (crates-io))

(define-public crate-libicmp-0.1.0 (c (n "libicmp") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "01i4928vq2k62psyalra15s47yi9gpb4a8531fngnd9v4a4lbg6v")))

(define-public crate-libicmp-0.1.1 (c (n "libicmp") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0ycaji179k6w3w1abqy5wggfyy0daql1jnkgfsvr2dh8qny0hfkl")))

(define-public crate-libicmp-0.1.2 (c (n "libicmp") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "17iv1mravj9pfpyhhqpf5r853kxzdmr3pdvbkjrc3hp982lhrd9f")))

