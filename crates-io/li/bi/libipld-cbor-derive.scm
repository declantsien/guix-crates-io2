(define-module (crates-io li bi libipld-cbor-derive) #:use-module (crates-io))

(define-public crate-libipld-cbor-derive-0.2.0 (c (n "libipld-cbor-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.12") (d #t) (k 0)) (d (n "quote") (r "^1.0.4") (d #t) (k 0)) (d (n "syn") (r "^1.0.19") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.26") (d #t) (k 2)))) (h "1d90551yyslavd83ciz947w1bm51rhc3q7jay94ciqjjncxrcrcn")))

(define-public crate-libipld-cbor-derive-0.3.0 (c (n "libipld-cbor-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "141fyc56gabd6xy8jsjgp584dx6afib161fgkaqmgshk934x5fly")))

(define-public crate-libipld-cbor-derive-0.4.0 (c (n "libipld-cbor-derive") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0.14") (d #t) (k 0)) (d (n "quote") (r "^1.0.6") (d #t) (k 0)) (d (n "syn") (r "^1.0.23") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.27") (d #t) (k 2)))) (h "0dkbbvh2bs4k950pqxg0ia2blq4l5d2ll31pf7wr6ac54br85i33")))

(define-public crate-libipld-cbor-derive-0.5.0 (c (n "libipld-cbor-derive") (v "0.5.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.40") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.33") (d #t) (k 2)))) (h "0ydh3bxnxxbdky6fmz001i5xl6b8iaw1szk6y0a8cz6fri2bycgw")))

(define-public crate-libipld-cbor-derive-0.6.0 (c (n "libipld-cbor-derive") (v "0.6.0") (d (list (d (n "proc-macro2") (r "^1.0.21") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.41") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.35") (d #t) (k 2)))) (h "10nap5gwqlh3sammgr03iavjv85ykybhfq3i9vxjnbba2gr1ir8i")))

(define-public crate-libipld-cbor-derive-0.7.0 (c (n "libipld-cbor-derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.42") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.35") (d #t) (k 2)))) (h "11nqhblkx0y4kp1n86lrl5dwfj4b3c9nnmiryy3bh5bj9sz9bp1p")))

(define-public crate-libipld-cbor-derive-0.8.0 (c (n "libipld-cbor-derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.53") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.35") (d #t) (k 2)))) (h "0wx4f2y209zdmw0y0ly8fzshir27w5ad96f4pd612gg1h184kpdr")))

(define-public crate-libipld-cbor-derive-0.9.0 (c (n "libipld-cbor-derive") (v "0.9.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "1f28471iaqivf0q5i6628qk9xw88mr5c5ng37svg69q999ds4yp1")))

(define-public crate-libipld-cbor-derive-0.9.1 (c (n "libipld-cbor-derive") (v "0.9.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "0v0a8s39kviaf62lh3axgr1hv329m75yw3akg1rgwhv1jmj5ipry")))

(define-public crate-libipld-cbor-derive-0.9.2 (c (n "libipld-cbor-derive") (v "0.9.2") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.7") (d #t) (k 0)) (d (n "syn") (r "^1.0.54") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.37") (d #t) (k 2)))) (h "0qgs6fwwwmcq4p303laag8smaw1j28jfvr9sn3kz97md02sn1c1s")))

(define-public crate-libipld-cbor-derive-0.10.0 (c (n "libipld-cbor-derive") (v "0.10.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.40") (d #t) (k 2)))) (h "0d79q81hff5jdydk8nqbvcfbfxsnpc3mli576qrd0cz3y0qvv2zc")))

(define-public crate-libipld-cbor-derive-0.11.0 (c (n "libipld-cbor-derive") (v "0.11.0") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.40") (d #t) (k 2)))) (h "0a6r07m4lpk964s43csbgdsn87g8n5wlc0a919pl7g4rh8waydpd")))

(define-public crate-libipld-cbor-derive-0.11.1 (c (n "libipld-cbor-derive") (v "0.11.1") (d (list (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "syn") (r "^1.0.60") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.40") (d #t) (k 2)))) (h "0sjbrawvzdb6awpsg4g24ibmskqayr5ads7krfwascwh6aa6wrqg")))

(define-public crate-libipld-cbor-derive-0.12.0 (c (n "libipld-cbor-derive") (v "0.12.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0kzpih7n4nyi255k471mik67r24qvy8asxxc4jlq04n3vbnmxkw0")))

(define-public crate-libipld-cbor-derive-0.13.0 (c (n "libipld-cbor-derive") (v "0.13.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0mzvvsh2v3nzj8ha5y6q7nsnb7cih2qmq9im8mqqnrhh100n4bdd")))

(define-public crate-libipld-cbor-derive-0.13.1 (c (n "libipld-cbor-derive") (v "0.13.1") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0938grzrc7k11p2xs891n7rvzl0zj4yxdph1w8rjxxpljk81gv5n")))

(define-public crate-libipld-cbor-derive-0.14.0 (c (n "libipld-cbor-derive") (v "0.14.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0xbkjb6sh4606s06g6wi6mrd9l3mzx4b5g2yv6i4f4rs754jzv39")))

(define-public crate-libipld-cbor-derive-0.15.0 (c (n "libipld-cbor-derive") (v "0.15.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "095kpnfyzvz98a8317haj58jfqavxn6411yxzfhn7mpzddmnh5a5")))

(define-public crate-libipld-cbor-derive-0.16.0 (c (n "libipld-cbor-derive") (v "0.16.0") (d (list (d (n "proc-macro-crate") (r "^1.1.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.72") (d #t) (k 0)) (d (n "synstructure") (r "^0.12.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.42") (d #t) (k 2)))) (h "0f5qijp9iz7950awqdhqdhvnl5rfzq52p0d1avj76adp56ks6nvx")))

