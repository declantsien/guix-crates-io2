(define-module (crates-io li bi libinput-sys) #:use-module (crates-io))

(define-public crate-libinput-sys-0.1.0 (c (n "libinput-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)))) (h "1wd6wn97y8ybnvjw8yhihn4fhbc1llg68imvxd14cwnixfnm7hkd")))

(define-public crate-libinput-sys-0.1.1 (c (n "libinput-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.19.0") (d #t) (k 1)))) (h "1dzqv5qxc7sn055jxk78a07b88i78m43iwh9kihc7y81mg0zl9hi")))

(define-public crate-libinput-sys-0.2.0 (c (n "libinput-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.25.0") (d #t) (k 1)))) (h "17l5gmahfwfj426xmkg6hg5hql9ar5s5vaf9zcipksr9kxl48fp7")))

