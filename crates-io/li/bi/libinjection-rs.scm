(define-module (crates-io li bi libinjection-rs) #:use-module (crates-io))

(define-public crate-libinjection-rs-0.2.6 (c (n "libinjection-rs") (v "0.2.6") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "git2") (r "^0.13") (d #t) (k 1)) (d (n "regex") (r "^1.4") (d #t) (k 1)))) (h "10m8pld57r0z46sm8047cp9igz4k0v4j6ax0rh9mzkd5wilxval7")))

(define-public crate-libinjection-rs-0.2.7 (c (n "libinjection-rs") (v "0.2.7") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "git2") (r "^0.13") (d #t) (k 1)) (d (n "regex") (r "^1.4") (d #t) (k 1)))) (h "0y105fclszdy1d1rdwi6im5ng1cr381cr96vxs4dbmk3wmjjqq9m")))

(define-public crate-libinjection-rs-0.2.8 (c (n "libinjection-rs") (v "0.2.8") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "git2") (r "^0.13") (d #t) (k 1)) (d (n "regex") (r "^1.4") (d #t) (k 1)))) (h "02xkva2pg9p371j8vmsjgypnx6kbqxpbkpb1y0dzi49manfvhz3j")))

