(define-module (crates-io li bi libipld-base) #:use-module (crates-io))

(define-public crate-libipld-base-0.1.0 (c (n "libipld-base") (v "0.1.0") (d (list (d (n "cid") (r "^0.4.0") (d #t) (k 0)) (d (n "multihash") (r "^0.10.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.11") (d #t) (k 0)))) (h "0w0rc8ms3cb8hk5j3lbm3pbbjjnxl4ldv8jkrd9pyd9fx92gii78")))

