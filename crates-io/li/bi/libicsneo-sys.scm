(define-module (crates-io li bi libicsneo-sys) #:use-module (crates-io))

(define-public crate-libicsneo-sys-0.1.0 (c (n "libicsneo-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "1k7zks648bh1jarbqz2ygixvba9c5k6gwzybrg478qkhhnbc9lfn")))

(define-public crate-libicsneo-sys-0.1.1 (c (n "libicsneo-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "1zbiwvlns92gr3m655y1bcl9k7xscz8kpqwp69jd2k7ksc752hd8")))

(define-public crate-libicsneo-sys-0.1.2 (c (n "libicsneo-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "1401n5vlzcz98jp9h823xzhy3gwym3qvssi8caxc8r5hwzxvkan7")))

(define-public crate-libicsneo-sys-0.1.3 (c (n "libicsneo-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "17n2r2icv0plh55zgjf8viawli5q6fkgqy9w4mzhnqz3hd41qgdm")))

(define-public crate-libicsneo-sys-0.1.4 (c (n "libicsneo-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "0ap9vlqhq763s4sxg47jal9v6ifhzpbfqbibd3jj7mdck19zkcwb")))

(define-public crate-libicsneo-sys-0.1.5 (c (n "libicsneo-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "1g75k7g9bll14mc2zzjnnb0bx2vdlsbpjzih84sigr50h8grvbgs")))

(define-public crate-libicsneo-sys-0.1.6 (c (n "libicsneo-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "0c5bc8yz467gyjrxfnqjx94haasz24rvs8v4ns5m3bf0rb2ra87n") (f (quote (("default") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.7 (c (n "libicsneo-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "0d810k3n7zzh3diq9l96qqn2kv5r00cbr01a18qansv8gsx7120w") (f (quote (("msvc_parallel_build" "build_libicsneo") ("default" "build_libicsneo" "msvc_parallel_build") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.8 (c (n "libicsneo-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "0fa42ap92pg0agnfv85gx5ylla98dsdjxpq91n8fh3xif5i0yy32") (f (quote (("msvc_parallel_build" "build_libicsneo") ("default" "build_libicsneo" "msvc_parallel_build") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.9 (c (n "libicsneo-sys") (v "0.1.9") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "0bxasq2b435kgdy2f4n6zdkf577mg5c1zz60zgv55d5dav9gni69") (f (quote (("msvc_parallel_build" "build_libicsneo") ("default" "build_libicsneo" "msvc_parallel_build") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.10 (c (n "libicsneo-sys") (v "0.1.10") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)))) (h "1ik26b6kfkrl1i9sar7rlw40w8w0id4513rd36jqv0z9haa14ihj") (f (quote (("msvc_parallel_build" "build_libicsneo") ("default" "build_libicsneo" "msvc_parallel_build") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.11 (c (n "libicsneo-sys") (v "0.1.11") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "1hq5v7sg92sjwixywfnh9py7sz7178w1s6rnxplbdrh26cnzyll9") (f (quote (("ninja" "build_libicsneo") ("default" "build_libicsneo") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.12 (c (n "libicsneo-sys") (v "0.1.12") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "1vnshgnrirwr2n3pqb10fkpypfchj53pdmh0s8bfav54wbpa0l0v") (f (quote (("ninja" "build_libicsneo") ("default" "build_libicsneo" "ninja") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.13 (c (n "libicsneo-sys") (v "0.1.13") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "0kflvchsfgzsw1apa0pdchj93b59dw6c75glb0n9ihv7337m6l5x") (f (quote (("ninja" "build_libicsneo") ("default" "build_libicsneo" "ninja") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.14 (c (n "libicsneo-sys") (v "0.1.14") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "1fgyj6w9612z8qdglj5lgd21m07ddakf7akk2irwrgfkjc6ysngj") (f (quote (("ninja" "build_libicsneo") ("default" "build_libicsneo" "ninja") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.15 (c (n "libicsneo-sys") (v "0.1.15") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "0bqg61dqqf885jgj9kl1na4n94qxxf5klivrdz91qdff04qnbrp9") (f (quote (("ninja" "build_libicsneo") ("default" "build_libicsneo" "ninja") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.16 (c (n "libicsneo-sys") (v "0.1.16") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "1a5m365qcpmzxwq2m6r4p6f321nacwwl77j7p43hlib92f6wi1yd") (f (quote (("ninja" "build_libicsneo") ("default" "build_libicsneo" "ninja") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.17 (c (n "libicsneo-sys") (v "0.1.17") (d (list (d (n "bindgen") (r "0.60.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "1cw9732agx3skwisbld89w6cdagn92hxfyfy8jwmi73zyb36kiky") (f (quote (("ninja" "build_libicsneo") ("default" "build_libicsneo" "ninja") ("build_libicsneo"))))))

(define-public crate-libicsneo-sys-0.1.18 (c (n "libicsneo-sys") (v "0.1.18") (d (list (d (n "bindgen") (r "0.65.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "14y3hd5pa16fm6wfdlp3zi24ri8aqf2kpr9qrgy6wzyqvrmhdlmi") (f (quote (("static") ("default" "static"))))))

(define-public crate-libicsneo-sys-0.1.19 (c (n "libicsneo-sys") (v "0.1.19") (d (list (d (n "bindgen") (r "0.65.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "0z7lya7p2yigj9vdg00dgzsxnj741d1jmd7j8m70ylqwczgxhjlx") (f (quote (("static") ("default" "static"))))))

(define-public crate-libicsneo-sys-0.2.0 (c (n "libicsneo-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "0.65.*") (d #t) (k 1)) (d (n "cmake") (r "^0.1.48") (d #t) (k 1)) (d (n "path-clean") (r "0.1.*") (d #t) (k 1)) (d (n "which") (r "^4.3.0") (d #t) (k 1)))) (h "1r8scms8dyw2bhsdg97q76j3p5jhsg68fl2qjsbx3pq45rxhjd7r") (f (quote (("static") ("default"))))))

