(define-module (crates-io li bi libits-copycat) #:use-module (crates-io))

(define-public crate-libits-copycat-0.2.1 (c (n "libits-copycat") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "07ac612i42wss58hfv195zg2ga6qz7c56g5yx3xwhvjq10ial0b9")))

(define-public crate-libits-copycat-0.2.2 (c (n "libits-copycat") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "1b5i93rv62zkh988c13184c9w23dw5nbpz0xgg360fgmhp4kwdhg")))

(define-public crate-libits-copycat-0.3.0 (c (n "libits-copycat") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "16plkvdpd36zwrpwrfgh9iys4vv9h14zxrs3h7a0w6q9lmbbd0p2")))

(define-public crate-libits-copycat-0.3.1 (c (n "libits-copycat") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "1ps3l05x1mvq1inh1hllbqia31jg54042r4xnq81z46p6jxgjphd")))

(define-public crate-libits-copycat-1.0.0 (c (n "libits-copycat") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "05vfsxdwggwwsi6jpyq16hj6rwx9gxqqw0sbp2f9j0z564g85wmm")))

(define-public crate-libits-copycat-1.0.1 (c (n "libits-copycat") (v "1.0.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "1dy2nb5fi5fa536x2aa7kl6ddyh0yjgf93v1zpml9bsp9q6fmpp7")))

(define-public crate-libits-copycat-1.1.0 (c (n "libits-copycat") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^1.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "01rj79s63jjg2iqd8vz12dci289nrj5a6dbr2fi0lwjp5kfmdp8i")))

(define-public crate-libits-copycat-1.2.0 (c (n "libits-copycat") (v "1.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^1.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "141ldmkbm3a8g4kc2ankpyah87mp8immxbkx1wnkips327midvpp")))

(define-public crate-libits-copycat-1.3.0 (c (n "libits-copycat") (v "1.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^1.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "1l020qik0q37h4nvi9vc57b7s5g0wd8x4sg4n30mhah13b9bbmpc")))

(define-public crate-libits-copycat-1.4.0 (c (n "libits-copycat") (v "1.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "1yv52l93mb49iy2qchrarkw8mkzic1v7cwkys23s294fkbncwqyk")))

(define-public crate-libits-copycat-1.5.0 (c (n "libits-copycat") (v "1.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "libits-client") (r "^1.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "timer") (r "^0.2") (d #t) (k 0)))) (h "1csgk1ir8bj0gmjm526d83hlqpf7jgn8f58dc8r724smf9vv8rpx")))

