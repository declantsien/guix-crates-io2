(define-module (crates-io li bi libipld-macro) #:use-module (crates-io))

(define-public crate-libipld-macro-0.1.0 (c (n "libipld-macro") (v "0.1.0") (d (list (d (n "libipld-base") (r "^0.1.0") (d #t) (k 0)) (d (n "multihash") (r "^0.10.1") (d #t) (k 0)))) (h "1dgb08d7ky0ifxvliii5qvi99sflxrfpzbxp7y1hkscknjrmbg3g")))

(define-public crate-libipld-macro-0.2.0 (c (n "libipld-macro") (v "0.2.0") (d (list (d (n "libipld-core") (r "^0.2.0") (d #t) (k 0)))) (h "1js1mpl04r9fg9yk8f2sp72468yz5fmqhvr7yl35c8089xbdw7bm")))

(define-public crate-libipld-macro-0.3.0 (c (n "libipld-macro") (v "0.3.0") (d (list (d (n "libipld-core") (r "^0.3.0") (d #t) (k 0)))) (h "0wxcybai2x8l5vdj439745spk7q8jhdgk15vyc7y6jh4pz5nbq1l")))

(define-public crate-libipld-macro-0.4.0 (c (n "libipld-macro") (v "0.4.0") (d (list (d (n "libipld-core") (r "^0.4.0") (d #t) (k 0)))) (h "0h385qkm9ri5f8vs73mwiyx1b99gj3mi2ibrkq4bkrs8wswv81hf")))

(define-public crate-libipld-macro-0.5.0 (c (n "libipld-macro") (v "0.5.0") (d (list (d (n "libipld-core") (r "^0.5.0") (d #t) (k 0)))) (h "0yms2zlzg35sbw09pnypklyxjgj5w3gcm8kwl61ia5a44gqxryg8")))

(define-public crate-libipld-macro-0.6.0 (c (n "libipld-macro") (v "0.6.0") (d (list (d (n "libipld-core") (r "^0.6.0") (d #t) (k 0)))) (h "0ipwwallh8rpjj7c1616n062rp6f91ggmgnkh7vay2xzyqcvx5hp")))

(define-public crate-libipld-macro-0.7.0 (c (n "libipld-macro") (v "0.7.0") (d (list (d (n "libipld-core") (r "^0.7.0") (d #t) (k 0)))) (h "1r4pv51yajd2k4452yjc1s0fgr33bzd5iwizcvhig20m80mpdsqi")))

(define-public crate-libipld-macro-0.8.0 (c (n "libipld-macro") (v "0.8.0") (d (list (d (n "libipld-core") (r "^0.8.0") (d #t) (k 0)))) (h "0c9v7valvlq866n5jsnqad3gg3c48nzp4kcdc3yqlmmslkds9jwa")))

(define-public crate-libipld-macro-0.9.0 (c (n "libipld-macro") (v "0.9.0") (d (list (d (n "libipld-core") (r "^0.9.0") (d #t) (k 0)))) (h "1sq0xmv1ayazrm65a03v5mw7w8f3n1n5qmcbhxyqn2zsjz9imcxw")))

(define-public crate-libipld-macro-0.10.0 (c (n "libipld-macro") (v "0.10.0") (d (list (d (n "libipld-core") (r "^0.10.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (f (quote ("blake3"))) (d #t) (k 2)))) (h "0il58csipj2jf944yy0fv2rc4y76yv7if73icicji3g5n6qnlwiy")))

(define-public crate-libipld-macro-0.11.0 (c (n "libipld-macro") (v "0.11.0") (d (list (d (n "libipld-core") (r "^0.11.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (f (quote ("blake3"))) (d #t) (k 2)))) (h "14sixpq1f1gp3bs30yba45i42773463ms88b5jjq1fqagdwxsava")))

(define-public crate-libipld-macro-0.12.0 (c (n "libipld-macro") (v "0.12.0") (d (list (d (n "libipld-core") (r "^0.12.0") (d #t) (k 0)) (d (n "multihash") (r "^0.14.0") (f (quote ("blake3"))) (d #t) (k 2)))) (h "0r548gyszylidqi8brififb243mz2ss5zphm5pkbaz3zfh3kym9d")))

(define-public crate-libipld-macro-0.13.0 (c (n "libipld-macro") (v "0.13.0") (d (list (d (n "libipld-core") (r "^0.13.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (f (quote ("blake3"))) (d #t) (k 2)))) (h "1996ns2xqh9idyqqah68jfgs4iv1jxdzk3yjykma9b08513d9mmh")))

(define-public crate-libipld-macro-0.13.1 (c (n "libipld-macro") (v "0.13.1") (d (list (d (n "libipld-core") (r "^0.13.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (f (quote ("blake3"))) (d #t) (k 2)))) (h "0jl170ghbb1pg0dy524q01amzi2jy90zprfrd82z8bi6aq8cpi66")))

(define-public crate-libipld-macro-0.14.0 (c (n "libipld-macro") (v "0.14.0") (d (list (d (n "libipld-core") (r "^0.14.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (f (quote ("blake3"))) (d #t) (k 2)))) (h "1qi59lkk7zsg8kjfsfcfvxl5mbs5b6gpz49sgjv5jl5fc8ah2b45")))

(define-public crate-libipld-macro-0.15.0 (c (n "libipld-macro") (v "0.15.0") (d (list (d (n "libipld-core") (r "^0.15.0") (d #t) (k 0)) (d (n "multihash") (r "^0.17.0") (f (quote ("blake3"))) (d #t) (k 2)))) (h "1iwhr39li0zj8ijr417mim0isxd7lzpap6prydn7kwjlkvccriyl")))

(define-public crate-libipld-macro-0.16.0 (c (n "libipld-macro") (v "0.16.0") (d (list (d (n "libipld-core") (r "^0.16.0") (d #t) (k 0)) (d (n "multihash") (r "^0.18.0") (f (quote ("blake3"))) (d #t) (k 2)))) (h "1bnlbq5wljba3dmix9p503k322gz3pw2fc1gfbk6m1jg45a1q5vi")))

