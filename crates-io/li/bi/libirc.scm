(define-module (crates-io li bi libirc) #:use-module (crates-io))

(define-public crate-libirc-0.1.0 (c (n "libirc") (v "0.1.0") (h "15yi61yky6lr7c72iv8xrp0y80s0n6bjmw4lx9fxynpbzndalh6l")))

(define-public crate-libirc-0.1.1 (c (n "libirc") (v "0.1.1") (h "1m02wrlg1ihhx06j36yayh30c18rihn9lrpa0nxsqdzi3j8za1w4")))

(define-public crate-libirc-0.1.2 (c (n "libirc") (v "0.1.2") (h "0kn1igjgdhlk5zx22warzc4fp7x79fidfax6qcmgsr9dgvlyixby")))

(define-public crate-libirc-0.1.3 (c (n "libirc") (v "0.1.3") (h "1i3v5hjp4qz4yb2gq4gsj2aa6cl15qilyx11l1vkvwjq86qsc5ay")))

