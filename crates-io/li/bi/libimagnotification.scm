(define-module (crates-io li bi libimagnotification) #:use-module (crates-io))

(define-public crate-libimagnotification-0.4.0 (c (n "libimagnotification") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libimagerror") (r "^0.5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "1dj3ks2a4b6fsynnk9q7zr5vpqx0m3p59mfxs74dbpbjc368hs73") (y #t)))

(define-public crate-libimagnotification-0.5.0 (c (n "libimagnotification") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libimagerror") (r "^0.5.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "1y00bzk8bmvsm2yp4v673q4lxcgfhq0dyq0km70wzmask1wqrlp5")))

(define-public crate-libimagnotification-0.6.0 (c (n "libimagnotification") (v "0.6.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libimagerror") (r "^0.6.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "1pi0c8w8wx8bqs1447zcllyia20dcxfnah45y1mn98mrhldprsfj")))

(define-public crate-libimagnotification-0.6.2 (c (n "libimagnotification") (v "0.6.2") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libimagerror") (r "^0.6.2") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "1w1rprhm5jqq95ccjx2pc110kq49wccdlj2cg4slmzpgg2rc17rv")))

(define-public crate-libimagnotification-0.6.3 (c (n "libimagnotification") (v "0.6.3") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libimagerror") (r "^0.6.3") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "1fklfs2rissvkgz2gvzi76nzrs9ma0a2n48shkmfqybl9x3mpfjw")))

(define-public crate-libimagnotification-0.6.4 (c (n "libimagnotification") (v "0.6.4") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libimagerror") (r "^0.6.4") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "11l1ck2qxzxa375hrcd60cl4q6py0x7m797l58gpcyh817g4a27x")))

(define-public crate-libimagnotification-0.7.0 (c (n "libimagnotification") (v "0.7.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libimagerror") (r "^0.7.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "05fjhn2sns9nrkzlkbdjf4ymqsvhl30sknc1lcf3yxypadr471xp")))

(define-public crate-libimagnotification-0.7.1 (c (n "libimagnotification") (v "0.7.1") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libimagerror") (r "^0.7.1") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "1kjyrqykscjfny5s3g8x865gkd1hlphy5gz91ai2axc1f9vf1j4w")))

(define-public crate-libimagnotification-0.8.0 (c (n "libimagnotification") (v "0.8.0") (d (list (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libimagerror") (r "^0.8.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "1y3nqm99qg17jvdhldibqn0m1bsw4dmw2vhidnccy2kk055qzni4")))

(define-public crate-libimagnotification-0.9.0 (c (n "libimagnotification") (v "0.9.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libimagerror") (r "^0.9.0") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "0qnnc2drflcm7swm478bw69gr5kc6cbk5xk92x81cpxx2i9a179h")))

(define-public crate-libimagnotification-0.9.1 (c (n "libimagnotification") (v "0.9.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libimagerror") (r "^0.9.1") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "17kl7swjllsywk5xly3kpgh0niv4j3pnj0dfkzmm9541k96hcvmn")))

(define-public crate-libimagnotification-0.9.2 (c (n "libimagnotification") (v "0.9.2") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libimagerror") (r "^0.9.1") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "1848g68qkmsp8xsgzzfk5c3w2vdrfbk19jh3fzf3q33wzb3k5kzs")))

(define-public crate-libimagnotification-0.9.3 (c (n "libimagnotification") (v "0.9.3") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "libimagerror") (r "^0.9.3") (d #t) (k 0)) (d (n "notify-rust") (r "^3.4.2") (d #t) (k 0)))) (h "1d4r3gk975yp76wny4wzakc7zd5gvwa2p3lk9nadqavy24v732vh")))

