(define-module (crates-io li bi libipt-sys) #:use-module (crates-io))

(define-public crate-libipt-sys-0.1.0 (c (n "libipt-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0g1ifm39jy82qm9k2n2m1nlhiwlhvxl0if8ccp7a25fzsxij57gi")))

(define-public crate-libipt-sys-0.1.1 (c (n "libipt-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.52.0") (d #t) (k 1)) (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0f7cbq0f8nixbkgckf8kng7bchxlrin35k05d0c4vzf3q6vjhjpr")))

(define-public crate-libipt-sys-0.1.2 (c (n "libipt-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "1narsgqnb3pd2pn55fynbjgmg385vbzdjz8gkhkj6pwqbpj8gwmh")))

(define-public crate-libipt-sys-0.1.3 (c (n "libipt-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0z35823zibvn3clzbqvv5cw4d5477krw4c5dvl28phhndq9ay8js")))

(define-public crate-libipt-sys-0.1.4 (c (n "libipt-sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1.42") (d #t) (k 1)))) (h "0y2cj81mbzn4gm7b6bxw9d1gm6l3whm7k49cyi7pnaqrqz1jm5cl")))

