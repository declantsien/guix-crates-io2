(define-module (crates-io li bi libintern) #:use-module (crates-io))

(define-public crate-libintern-0.1.0 (c (n "libintern") (v "0.1.0") (h "07ndn4l8b83nbx18lv5aqzd28dkwprvnhrhms6013v17xx0m9f7p")))

(define-public crate-libintern-0.2.0 (c (n "libintern") (v "0.2.0") (h "0h5y0az678flw5b6bg76q49am652kqwbs8nl65calv6qbk8i7h9y")))

(define-public crate-libintern-0.3.0 (c (n "libintern") (v "0.3.0") (h "106yab9my3v3a89qynvjybimxf43dyc6rvcyr38m34h7lzmyzpq5")))

(define-public crate-libintern-0.4.0 (c (n "libintern") (v "0.4.0") (h "1zdbi277mjv8zamw10fvay8q109xjcfz5q68z6wz9k7j7mzf8any")))

(define-public crate-libintern-0.5.0 (c (n "libintern") (v "0.5.0") (h "0m5c7crniy6v0ssndavbkkglflqfsxf9y8dk3mdchvwd2s9kplqf")))

(define-public crate-libintern-0.6.0 (c (n "libintern") (v "0.6.0") (h "1490qn69d69fn62j827wagh0hz34yb5mx59rv48jcxixn6kyflf7")))

(define-public crate-libintern-0.7.0 (c (n "libintern") (v "0.7.0") (h "0jis4fpb7iawq98jydqd74q9frljg5aiy8bwwwjwq72pqwv45ccf")))

