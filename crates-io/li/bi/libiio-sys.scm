(define-module (crates-io li bi libiio-sys) #:use-module (crates-io))

(define-public crate-libiio-sys-0.1.0 (c (n "libiio-sys") (v "0.1.0") (h "0k2q0mmaga3pb1a2gzd2n3dnyfsardp4qwv7485ds3j8wzng9qvl") (l "iio")))

(define-public crate-libiio-sys-0.2.0 (c (n "libiio-sys") (v "0.2.0") (h "0fck1m9y5jrkc9y69b6fl2hpn50bd0npy0h0qz46nmpa2ag4dcms") (l "iio")))

(define-public crate-libiio-sys-0.3.0 (c (n "libiio-sys") (v "0.3.0") (h "1wf8ciw2ihb7i9iqivl57qg85csj5146mxnwwjhwczbq4hbxc9zs") (l "iio")))

(define-public crate-libiio-sys-0.3.1 (c (n "libiio-sys") (v "0.3.1") (h "1v14rwlc1ny550jywwnf64592p1b7gll82sm22xlbwxqfdfjav3d") (l "iio")))

