(define-module (crates-io li bi libips) #:use-module (crates-io))

(define-public crate-libips-0.0.1-placeholder (c (n "libips") (v "0.0.1-placeholder") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "1gx1fk8qjhf1wgb3cak5v05aybfkz8gmc6zwn6r8pvgp5wwsxbqy")))

(define-public crate-libips-0.0.2-placeholder (c (n "libips") (v "0.0.2-placeholder") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)))) (h "0rizxd56ab67g330zm83mnk6blp9r9wrji2pm1cg7ny21yscimr4")))

(define-public crate-libips-0.1.0 (c (n "libips") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "maplit") (r "^0.1.6") (d #t) (k 0)) (d (n "object") (r "^0.23.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "0jrshrcqv5qf0ncrc173jkl16bpknfwdka3pl97hb59ibbibxa23")))

(define-public crate-libips-0.1.1 (c (n "libips") (v "0.1.1") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "maplit") (r "^0.1.6") (d #t) (k 0)) (d (n "object") (r "^0.23.0") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.7") (d #t) (k 0)) (d (n "sha2") (r "^0.9.3") (d #t) (k 0)) (d (n "sha3") (r "^0.9.1") (d #t) (k 0)))) (h "0mhsgwm4xyjmcna23hcdhharr3y3yzyxdwlch2rvm5blqfnq6dc0")))

