(define-module (crates-io li bi libinjection) #:use-module (crates-io))

(define-public crate-libinjection-0.1.0 (c (n "libinjection") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "git2") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "11pik6sj3kpidb89hzgw1fw6z8wywgfv3wfzq65jwmxcfgqvmdfd")))

(define-public crate-libinjection-0.1.1 (c (n "libinjection") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "git2") (r "^0.7") (d #t) (k 1)) (d (n "regex") (r "^1.0") (d #t) (k 1)))) (h "0bqyvgrj4ajbhbgr4ahk0vmcf8q3y74gr5b6zz9ndqd6wscivzw0")))

(define-public crate-libinjection-0.2.0 (c (n "libinjection") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "git2") (r "^0.11") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "09ppi9ga0vggq87sglpygy2bbagh1jx91l69qzav8db5zi7rzvfl")))

(define-public crate-libinjection-0.2.1 (c (n "libinjection") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "git2") (r "^0.11") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "14rsw9nfr94b3pc2xwk9052xm52arr295786i8laqjjrvnl1g5w1")))

(define-public crate-libinjection-0.2.2 (c (n "libinjection") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "git2") (r "^0.11") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1xdim5c9j7wwggwb30h7fql5mcd6w25rb9662a6s9nnn0b8hvki4")))

(define-public crate-libinjection-0.2.3 (c (n "libinjection") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "git2") (r "^0.13") (d #t) (k 1)) (d (n "regex") (r "^1.3") (d #t) (k 1)))) (h "1gchgrh4lc4jjfbi20vzdlsshyd1lymzx2l1dl5ww51q0r5bw033")))

(define-public crate-libinjection-0.2.4 (c (n "libinjection") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "git2") (r "^0.13") (d #t) (k 1)) (d (n "regex") (r "^1.4") (d #t) (k 1)))) (h "02i3bigdn7mgwb58sxn1gzfnqpb5vfm2ybwkhfy1hrkfv5m4jqcv")))

(define-public crate-libinjection-0.3.0 (c (n "libinjection") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "git2") (r "^0.15") (d #t) (k 1)))) (h "03qjap2mzhpijdc8fdiqj6icix8x782i0xnp7z8sgv1mahj29kxw")))

(define-public crate-libinjection-0.3.1 (c (n "libinjection") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "git2") (r "^0.15") (d #t) (k 1)))) (h "0fic56nb21ymd2xhjf4xc1hiqdf01xss5fma7h463j7skil8szgv")))

(define-public crate-libinjection-0.3.2 (c (n "libinjection") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "git2") (r "^0.15") (d #t) (k 1)))) (h "0fb0wa40nkq4hncjx3y9vs39i40kskpd20zn2mswh92jwq6akad6")))

(define-public crate-libinjection-0.3.3 (c (n "libinjection") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "git2") (r "^0.18") (d #t) (k 1)))) (h "1xkrr954c2n1kjsxhi017aqb91sfi925q3g6qzrizyw78yfhmcka")))

(define-public crate-libinjection-0.3.4 (c (n "libinjection") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "git2") (r "^0.18") (d #t) (k 1)))) (h "0byfm1yy1402cdfflpg0hvxnabgfxr3nrp8pyqzi05vhv6m20fs9")))

