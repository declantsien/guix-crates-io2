(define-module (crates-io li bi libipld-raw-cbor) #:use-module (crates-io))

(define-public crate-libipld-raw-cbor-0.1.0 (c (n "libipld-raw-cbor") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libipld-core") (r "^0.14.0") (d #t) (k 0)) (d (n "libipld-macro") (r "^0.14.0") (d #t) (k 2)) (d (n "multihash") (r "^0.16.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "07179iki5pxxbcw10amhpwxfyr5drsd5krmdfd840cnwd7idgw1a")))

(define-public crate-libipld-raw-cbor-0.1.1 (c (n "libipld-raw-cbor") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "libipld-core") (r "^0.14.0") (d #t) (k 0)) (d (n "libipld-macro") (r "^0.14.0") (d #t) (k 2)) (d (n "multihash") (r "^0.16.3") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "05dmbv25sqzr05zjaji1pqsabdwzm215zy9vzjxm2qy64m69hdbx")))

