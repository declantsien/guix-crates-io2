(define-module (crates-io li bi libirecovery-sys) #:use-module (crates-io))

(define-public crate-libirecovery-sys-0.1.0 (c (n "libirecovery-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0j4qbsl3kdryzz7k27nacvdibk63xh6bl800s60c7vqkv59lnl6y") (l "irecovery")))

(define-public crate-libirecovery-sys-0.1.1 (c (n "libirecovery-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hsam2br1zlkg632177sgbmm9vqax8n0ccf3ivbgmy9f1rdp36pf") (l "irecovery")))

