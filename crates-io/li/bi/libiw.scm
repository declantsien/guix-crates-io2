(define-module (crates-io li bi libiw) #:use-module (crates-io))

(define-public crate-libiw-0.1.0 (c (n "libiw") (v "0.1.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.57") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.80") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.57") (f (quote ("console"))) (d #t) (k 0)))) (h "0927b8mx1xjzkc039mrhckz9hxgkfq0a0ylhhgd9v0zrqhdlblxc") (y #t)))

(define-public crate-libiw-0.2.0 (c (n "libiw") (v "0.2.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.63") (d #t) (k 0)) (d (n "serde") (r "^1.0.164") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.86") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.63") (f (quote ("console"))) (d #t) (k 0)))) (h "19b8b6hm7pm1w5l877776jsn43pq8ak3j7fdxgxjyw5z3dnq8hx2") (y #t)))

(define-public crate-libiw-0.3.0 (c (n "libiw") (v "0.3.0") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.64") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.87") (f (quote ("serde-serialize"))) (d #t) (k 0)) (d (n "web-sys") (r "^0.3.64") (f (quote ("console"))) (d #t) (k 0)))) (h "13955b2sisn8g8f3xj5c0d3w0r5fdpqz1c0z76gbxpfjrfh6i8in") (y #t)))

