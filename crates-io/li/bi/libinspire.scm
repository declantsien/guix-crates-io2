(define-module (crates-io li bi libinspire) #:use-module (crates-io))

(define-public crate-libinspire-0.1.0-alpha (c (n "libinspire") (v "0.1.0-alpha") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.5.0") (d #t) (k 0)) (d (n "select") (r "^0.3.0") (d #t) (k 0)) (d (n "slog") (r "^1.4.1") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.1.0") (d #t) (k 0)))) (h "0iydzvdm9z6fgamgwf3ypvx6bdpf96si6sgixflk88qacp9rx1w9")))

(define-public crate-libinspire-0.1.0-beta (c (n "libinspire") (v "0.1.0-beta") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "select") (r "^0.3.0") (d #t) (k 0)) (d (n "slog") (r "^1.4.1") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.1.0") (d #t) (k 0)))) (h "08ix6mqpcpjj5hawxw8140mlm3wxzrpl90jcsp411f48zgrrq6jg")))

(define-public crate-libinspire-0.1.0-beta-no-https (c (n "libinspire") (v "0.1.0-beta-no-https") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.7.3") (d #t) (k 0)) (d (n "select") (r "^0.3.0") (d #t) (k 0)) (d (n "slog") (r "^1.4.1") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.1.0") (d #t) (k 0)))) (h "165wyq9djv08b1vvmzzgmygjwwakrj5mslcqfpp5gyhb5h60ar78")))

(define-public crate-libinspire-0.1.0 (c (n "libinspire") (v "0.1.0") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "regex") (r "^0.2.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.8") (d #t) (k 0)) (d (n "select") (r "^0.3.0") (d #t) (k 0)) (d (n "slog") (r "^1.4.1") (d #t) (k 0)) (d (n "slog-stdlog") (r "^1.1.0") (d #t) (k 0)))) (h "1ymh6ymr209rp3y4lfcyxhd7lhz93hnjpig9q5dvgxy32xdr52wi")))

