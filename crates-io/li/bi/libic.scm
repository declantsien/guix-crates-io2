(define-module (crates-io li bi libic) #:use-module (crates-io))

(define-public crate-libic-0.1.0 (c (n "libic") (v "0.1.0") (d (list (d (n "either") (r "^1.6.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "minreq") (r "^2.6.0") (f (quote ("https"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)) (d (n "xmltojson") (r "^0") (d #t) (k 0)))) (h "0nrsaa4g7ibgz5kqddvkmqiqv7z11hilcappvnh6ipg47a56qz97")))

