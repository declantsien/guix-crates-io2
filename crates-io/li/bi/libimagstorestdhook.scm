(define-module (crates-io li bi libimagstorestdhook) #:use-module (crates-io))

(define-public crate-libimagstorestdhook-0.2.0 (c (n "libimagstorestdhook") (v "0.2.0") (d (list (d (n "fs2") (r "^0.2") (d #t) (k 0)) (d (n "git2") (r "^0.4") (d #t) (k 0)) (d (n "libimagentrylink") (r "^0.2.0") (d #t) (k 0)) (d (n "libimagerror") (r "^0.2.0") (d #t) (k 0)) (d (n "libimaginteraction") (r "^0.2.0") (d #t) (k 0)) (d (n "libimagstore") (r "^0.2.0") (d #t) (k 0)) (d (n "libimagutil") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "toml") (r "0.2.*") (d #t) (k 0)))) (h "1q6cd7q73db72akahs1w4a6c6fx6qgv54x94lygzxsp7h6zl8qvz")))

