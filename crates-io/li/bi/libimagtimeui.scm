(define-module (crates-io li bi libimagtimeui) #:use-module (crates-io))

(define-public crate-libimagtimeui-0.2.0 (c (n "libimagtimeui") (v "0.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "lazy_static") (r "^0.1") (d #t) (k 0)) (d (n "libimagerror") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)))) (h "0vaj7mjpg6cizwj8c90ikx8nmbc82b6c3xhkfb8f2qdcq9p2si02")))

(define-public crate-libimagtimeui-0.3.0 (c (n "libimagtimeui") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0822h46d3yhnqxnp5m7vngxz4hv05b07c0y12a9p7pn29q6xzrc5")))

(define-public crate-libimagtimeui-0.4.0 (c (n "libimagtimeui") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "123v89xdw12i3ja1nkdpx37f35c19pn5xp5lsn3pld4p7r4h29la")))

(define-public crate-libimagtimeui-0.5.0 (c (n "libimagtimeui") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0rrki753vpqijqgaclmcs5zd2d9n8dwqazz6ak0ac2835iv29f88")))

(define-public crate-libimagtimeui-0.6.0 (c (n "libimagtimeui") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "07l645xfgdn5p3pw2315wnr86aska6zyy7drk9qxxmz49mf7q1fz")))

(define-public crate-libimagtimeui-0.6.2 (c (n "libimagtimeui") (v "0.6.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0mvj6500qq99ck2ra1ws4kvskm22dksyyrbx8qf039rp3aa72b5l")))

(define-public crate-libimagtimeui-0.6.3 (c (n "libimagtimeui") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0z8f0ksdbg98555fgarq1ah3k0xfnmb0p4wjdjdp1v6ich8w4mqd")))

(define-public crate-libimagtimeui-0.6.4 (c (n "libimagtimeui") (v "0.6.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r ">= 2.17") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1yp0fgdxynqpr83y28l0g4njbsf79437ija98c2aiisb5x396ar7")))

(define-public crate-libimagtimeui-0.7.0 (c (n "libimagtimeui") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1vspvmssici9wk9lwd5bippcvrvk0arlyfc5xa5nm03f544rvzw2")))

(define-public crate-libimagtimeui-0.7.1 (c (n "libimagtimeui") (v "0.7.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1ia83r32xpybv0wk82mr64s01gcad4w9pd5ryj5kc0x5kkkxx1j7")))

(define-public crate-libimagtimeui-0.8.0 (c (n "libimagtimeui") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1l9qwmknn0wpmg90g2zs5ggfa279156akzhrcz3f9hgc26r6hgn6")))

(define-public crate-libimagtimeui-0.9.0 (c (n "libimagtimeui") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "03ki5c7ailc5l0p9n5mn5x57kvb8c32ic8b5bv9k0vw6ikxvzmx2")))

(define-public crate-libimagtimeui-0.9.1 (c (n "libimagtimeui") (v "0.9.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1340r4vjaydmsl0v648nh4vh08ml90jzcsvp1yfd2ydmsv29zl12")))

(define-public crate-libimagtimeui-0.9.2 (c (n "libimagtimeui") (v "0.9.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0y6yv4r5yn2q47fjlrafkff281cazvfv9i2xsd7ig58bh6vbiq9v")))

(define-public crate-libimagtimeui-0.9.3 (c (n "libimagtimeui") (v "0.9.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2.29") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "log") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0kgrmw3a9m8z4zylv6q8b4n6xp7ln6xzq2issk3f5jnjgsq57nwn")))

(define-public crate-libimagtimeui-0.10.0 (c (n "libimagtimeui") (v "0.10.0") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)))) (h "0wsjg9bfzd2fda16dhk43s7k3624ga14f3c5icdpz5v3h3rylkyd")))

(define-public crate-libimagtimeui-0.10.1 (c (n "libimagtimeui") (v "0.10.1") (d (list (d (n "chrono") (r "^0.4.7") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (f (quote ("color" "suggestions" "wrap_help"))) (k 0)) (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)))) (h "1wfygnr40xwn5q2fh5mda0aah7ynpyrcsggdkjnw8xvchccgdfgq")))

