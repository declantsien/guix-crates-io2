(define-module (crates-io li bi libical-sys) #:use-module (crates-io))

(define-public crate-libical-sys-0.1.0 (c (n "libical-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1m22ximk6p8p1y6yrpm2fyw4240541y93r2421wgp9lgfvhnzbz8")))

(define-public crate-libical-sys-0.1.1 (c (n "libical-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0w8429s8imakc2dnappvgm4nvf1nm5yz9cv101rl6mwkfdqpa7v1") (l "ical")))

(define-public crate-libical-sys-0.1.2 (c (n "libical-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1f8y3ngp982fgvpf0cw2f8x47jn9244xscmg0l2dg78lb59nb47g") (l "ical")))

(define-public crate-libical-sys-0.1.3 (c (n "libical-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.43") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18bdx4cgw8fmprhsxn7pdnh08rniqav8ybcbw5n0rw21nfk6wn1i") (l "ical")))

