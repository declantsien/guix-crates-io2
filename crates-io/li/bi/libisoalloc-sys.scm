(define-module (crates-io li bi libisoalloc-sys) #:use-module (crates-io))

(define-public crate-libisoalloc-sys-0.1.0 (c (n "libisoalloc-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0543wb1n3nn4rbc811px02y4x9mihhxh2287rghndrn5ljp2mmm2") (f (quote (("userfaultfd") ("sanity"))))))

(define-public crate-libisoalloc-sys-0.2.0 (c (n "libisoalloc-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r0sy6x1d76wzsjf0k61p0d8v3jxq46va6asa1cjkxhqizsc9lsk") (f (quote (("userfaultfd") ("tagging") ("sanity"))))))

(define-public crate-libisoalloc-sys-0.2.1 (c (n "libisoalloc-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1djmwwms8xw7jb1c2bsr9kxxp8bkxcmjkfrncix13zlz6yqslpr3") (f (quote (("userfaultfd") ("tagging") ("sanity"))))))

(define-public crate-libisoalloc-sys-0.2.2 (c (n "libisoalloc-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "08gwacsaajz6llsi4b0qmr4scks8g0sv6i8mmygwix1n74ivnjxr") (f (quote (("userfaultfd") ("tagging") ("sanity"))))))

(define-public crate-libisoalloc-sys-0.2.3 (c (n "libisoalloc-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11acmrvgl6gfc8qr7dzj1afaik73jip3jigkmd1zisrbmhwqmawa") (f (quote (("userfaultfd") ("tagging") ("sanity"))))))

(define-public crate-libisoalloc-sys-0.2.4 (c (n "libisoalloc-sys") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wbiyiwfw31b26qngyqk3n71dkzfz7dcl5gxvgc9l87xjm2ym6v0") (f (quote (("userfaultfd") ("tagging") ("sanity"))))))

(define-public crate-libisoalloc-sys-0.2.5 (c (n "libisoalloc-sys") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ziciqbmjlw0bcylbifsfw5ciar21qv4k3ysk0irrah1pcggdszp") (f (quote (("userfaultfd") ("tagging") ("sanity") ("neon") ("mte") ("memory_tagging"))))))

(define-public crate-libisoalloc-sys-0.2.6 (c (n "libisoalloc-sys") (v "0.2.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wnpk5wlmqxk0hj7z5vxpq3hz2z4wy0nz30qj1masl22ywq956i3") (f (quote (("userfaultfd") ("tagging") ("sanity") ("neon") ("mte") ("memory_tagging"))))))

(define-public crate-libisoalloc-sys-0.2.7 (c (n "libisoalloc-sys") (v "0.2.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01csjx2bkb90m517m5zwlv8f7k8kpppi6w3hjwgy45wdjfff9ids") (f (quote (("userfaultfd") ("tagging") ("sanity") ("nothread") ("neon") ("mte") ("memory_tagging"))))))

(define-public crate-libisoalloc-sys-0.2.8 (c (n "libisoalloc-sys") (v "0.2.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "1bys1cr367ipbdb25imspm0382xz28vz8gxw3f6bibfyk0pkfw2j") (f (quote (("userfaultfd") ("tagging") ("smallmem") ("sanity") ("nothread") ("neon") ("mte") ("memory_tagging"))))))

(define-public crate-libisoalloc-sys-0.2.9 (c (n "libisoalloc-sys") (v "0.2.9") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 1)))) (h "1j59is46n6jczsqpjbyza4vnh7c6vmxvgi7d2zksd75d0slknsw4") (f (quote (("userfaultfd") ("tagging") ("smallmem") ("sanity") ("nothread") ("neon") ("mte") ("memory_tagging"))))))

