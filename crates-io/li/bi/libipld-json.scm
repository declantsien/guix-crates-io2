(define-module (crates-io li bi libipld-json) #:use-module (crates-io))

(define-public crate-libipld-json-0.2.0 (c (n "libipld-json") (v "0.2.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "libipld-core") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.106") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.52") (d #t) (k 0)))) (h "1zq0cq7k5adwr8bz9h61abn9i8xnl1rcc6mfdivnfw15s618dzz2")))

(define-public crate-libipld-json-0.3.0 (c (n "libipld-json") (v "0.3.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "libipld-core") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1p0i4f5ljic2aajm3ny33k45lc5d97rkplglckycp3jpb6w33r00")))

(define-public crate-libipld-json-0.4.0 (c (n "libipld-json") (v "0.4.0") (d (list (d (n "base64") (r "^0.12.1") (d #t) (k 0)) (d (n "libipld-core") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.110") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.53") (d #t) (k 0)))) (h "1i6yji5kvadqdprgc1zsyr1kbn5daiskr9svgv8vi7vf5cj7iykk")))

(define-public crate-libipld-json-0.5.0 (c (n "libipld-json") (v "0.5.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "libipld-core") (r "^0.5.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0zk9w3ns0kf03dqvxkdhvv2mx7szc2wh7gqp4yrs5d94y98jihbw")))

(define-public crate-libipld-json-0.6.0 (c (n "libipld-json") (v "0.6.0") (d (list (d (n "base64") (r "^0.12.3") (d #t) (k 0)) (d (n "libipld-core") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1kpl5f5pbpvhvha14y4wf685alz5i02ngh6968wa61yim1hbc9i3")))

(define-public crate-libipld-json-0.7.0 (c (n "libipld-json") (v "0.7.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libipld-core") (r "^0.7.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.58") (d #t) (k 0)))) (h "1hkbzyx1hk6lq41bpfy7gcwf3bms83gnmi07yjqb7arvirfpfnxm")))

(define-public crate-libipld-json-0.8.0 (c (n "libipld-json") (v "0.8.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libipld-core") (r "^0.8.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)))) (h "162p5irjpk8rrb4gz0g973irjvmrbk264zld1g1my3xsipg2cvnp")))

(define-public crate-libipld-json-0.9.0 (c (n "libipld-json") (v "0.9.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libipld-core") (r "^0.9.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.118") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.60") (d #t) (k 0)))) (h "18wl6cr9q0g41asj4x208rgh22rxglrlgvz817hm4g6028ips6my")))

(define-public crate-libipld-json-0.10.0 (c (n "libipld-json") (v "0.10.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libipld-core") (r "^0.10.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "06ixrldznk5insvl2lhs6hdgrki0896y8398n9c81nm9sm8i09k0") (f (quote (("unleashed" "libipld-core/unleashed"))))))

(define-public crate-libipld-json-0.11.0 (c (n "libipld-json") (v "0.11.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libipld-core") (r "^0.11.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "19hmbjjyr68sl6q69i2mc4wl1d06hz5ba00saryahgaffqlb7kgn") (f (quote (("unleashed" "libipld-core/unleashed"))))))

(define-public crate-libipld-json-0.11.1 (c (n "libipld-json") (v "0.11.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libipld-core") (r "^0.11.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "12xykizrwcq1m1qsf2a93zj1d3863xwqamx3jxwhqg7l0axfx46q") (f (quote (("unleashed" "libipld-core/unleashed"))))))

(define-public crate-libipld-json-0.11.2 (c (n "libipld-json") (v "0.11.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libipld-core") (r "^0.11.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)))) (h "02p9zm2df80w3nsmqznly8l5l686vxqqlm0q3w4nqyrrvjf52324") (f (quote (("unleashed" "libipld-core/unleashed"))))))

(define-public crate-libipld-json-0.12.0 (c (n "libipld-json") (v "0.12.0") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "libipld-core") (r "^0.12.0") (d #t) (k 0)) (d (n "multihash") (r "^0.14.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1qrljj9i0jyyj4w0mglrsxzqzhiv0qfrb6agfx43ynsswv8adwrw") (f (quote (("unleashed" "libipld-core/unleashed"))))))

(define-public crate-libipld-json-0.13.0 (c (n "libipld-json") (v "0.13.0") (d (list (d (n "libipld-core") (r "^0.13.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "1glyffjw9cy63157yxr2v6y7h7fkzcck702pspjxwfh9l70j68dr")))

(define-public crate-libipld-json-0.13.1 (c (n "libipld-json") (v "0.13.1") (d (list (d (n "libipld-core") (r "^0.13.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "0khg0s78b49lb0y9vzrw89s53s9d0pb2zfcy00hix5k2i5b2fm21")))

(define-public crate-libipld-json-0.14.0 (c (n "libipld-json") (v "0.14.0") (d (list (d (n "libipld-core") (r "^0.14.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "1cv3kn6f8m3x46lbnssznxicfscm78jwx7nxff2dk17hhwd4iahq")))

(define-public crate-libipld-json-0.15.0 (c (n "libipld-json") (v "0.15.0") (d (list (d (n "libipld-core") (r "^0.15.0") (d #t) (k 0)) (d (n "multihash") (r "^0.17.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "0y8ykkvsk2zff5glixrqv751psfgxkvknvfb3w1h397rf1y9sm7w")))

(define-public crate-libipld-json-0.16.0 (c (n "libipld-json") (v "0.16.0") (d (list (d (n "libipld-core") (r "^0.16.0") (d #t) (k 0)) (d (n "multihash") (r "^0.18.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (f (quote ("float_roundtrip"))) (d #t) (k 0)))) (h "073khw2clkfw2m90h0h2md4p0na8s9k4wgf34mxv0iq0jkpnv195")))

