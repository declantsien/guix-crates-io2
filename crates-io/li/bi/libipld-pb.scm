(define-module (crates-io li bi libipld-pb) #:use-module (crates-io))

(define-public crate-libipld-pb-0.2.0 (c (n "libipld-pb") (v "0.2.0") (d (list (d (n "libipld-core") (r "^0.2.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.16") (d #t) (k 0)))) (h "12s7vqm39sknvikhxnl64pm2z86dq3vx70x76ssvgq8qg365irpa")))

(define-public crate-libipld-pb-0.3.0 (c (n "libipld-pb") (v "0.3.0") (d (list (d (n "libipld-core") (r "^0.3.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1gcgpai2iyxn046v7qfyxqfqqsd2by5mnzq3j9a3fh208kp3hwpq")))

(define-public crate-libipld-pb-0.4.0 (c (n "libipld-pb") (v "0.4.0") (d (list (d (n "libipld-core") (r "^0.4.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1qfipaqj38icqmv5n1g12nilphzl1pwcknk2p17vsn0iii3j1dpy")))

(define-public crate-libipld-pb-0.5.0 (c (n "libipld-pb") (v "0.5.0") (d (list (d (n "libipld-core") (r "^0.5.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "1wqz0sk75v1s8l1jr8y1ipfasmximj9xjmbll522zr7kzmf1pzv6")))

(define-public crate-libipld-pb-0.6.0 (c (n "libipld-pb") (v "0.6.0") (d (list (d (n "libipld-core") (r "^0.6.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0v51h9rsg4a8hpdhjpwaapm79vpvyinzrfpjmd128s2n8hcnbyh8")))

(define-public crate-libipld-pb-0.7.0 (c (n "libipld-pb") (v "0.7.0") (d (list (d (n "libipld-core") (r "^0.7.0") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)))) (h "0b6a4jdcx6i2684k9vjq35c9kcl61sla5qnlipx80xf64n8x8035")))

(define-public crate-libipld-pb-0.8.0 (c (n "libipld-pb") (v "0.8.0") (d (list (d (n "libipld-core") (r "^0.8.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.1") (d #t) (k 2)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "11mqq1jlpmhwa48ds4mlvg7fixcvn4ya8ypwwv02a4lq2xvh1rgs")))

(define-public crate-libipld-pb-0.9.0 (c (n "libipld-pb") (v "0.9.0") (d (list (d (n "libipld-core") (r "^0.9.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 2)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1zblwny10hfhhi426mldc7wa4asqk3vksm0ncyridq3v8351g892")))

(define-public crate-libipld-pb-0.10.0 (c (n "libipld-pb") (v "0.10.0") (d (list (d (n "libipld-core") (r "^0.10.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 2)) (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0kdvfr94dz5w6f76szwrkzpwim7ci2xwfv8qzxzyaln0j14q4ry9")))

(define-public crate-libipld-pb-0.11.0 (c (n "libipld-pb") (v "0.11.0") (d (list (d (n "libipld-core") (r "^0.11.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 2)) (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1g4lz2ssvr3dxymbf1gq27hhbhg73p38axrigqh2xrs93s64pxx7")))

(define-public crate-libipld-pb-0.11.1 (c (n "libipld-pb") (v "0.11.1") (d (list (d (n "libipld-core") (r "^0.11.0") (d #t) (k 0)) (d (n "multihash") (r "^0.13.2") (d #t) (k 2)) (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1mbg92hskslmzs30b62x44k6k1qkf3a4nfq93hnxyxx61ikzvfk5")))

(define-public crate-libipld-pb-0.12.0 (c (n "libipld-pb") (v "0.12.0") (d (list (d (n "libipld-core") (r "^0.12.0") (d #t) (k 0)) (d (n "multihash") (r "^0.14.0") (d #t) (k 2)) (d (n "prost") (r "^0.7.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.7.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "09s7ds2y223h9a0bk8h7rp0d68xkikrb6afkfml9rd9bdcsyinqd")))

(define-public crate-libipld-pb-0.13.0 (c (n "libipld-pb") (v "0.13.0") (d (list (d (n "libipld-core") (r "^0.13.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 2)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "059iwirjyla1fx76a7rv25vmq6akx5h983i3wj3dg7hfqh11mbdr")))

(define-public crate-libipld-pb-0.13.1 (c (n "libipld-pb") (v "0.13.1") (d (list (d (n "libipld-core") (r "^0.13.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 2)) (d (n "prost") (r "^0.9.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.9.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "0badhgfzmlk7g35ryxlvwgl3ia2j43r1w222vzlf28zzmq41m1bb")))

(define-public crate-libipld-pb-0.14.0 (c (n "libipld-pb") (v "0.14.0") (d (list (d (n "libipld-core") (r "^0.14.0") (d #t) (k 0)) (d (n "multihash") (r "^0.16.0") (d #t) (k 2)) (d (n "prost") (r "^0.10.0") (d #t) (k 0)) (d (n "prost-build") (r "^0.10.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1h6in65hvlvgfgxrv1n9c8jcwkhdp33ll6va4laq2mwn6i8vw0y0")))

(define-public crate-libipld-pb-0.15.0 (c (n "libipld-pb") (v "0.15.0") (d (list (d (n "libipld-core") (r "^0.15.0") (d #t) (k 0)) (d (n "multihash") (r "^0.17.0") (d #t) (k 2)) (d (n "prost") (r "^0.11.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)))) (h "1jvwq4c8589k6y1qfkpc6p6s495565yf0ingisr82g18gs5yx16x")))

(define-public crate-libipld-pb-0.16.0 (c (n "libipld-pb") (v "0.16.0") (d (list (d (n "bytes") (r "^1.3.0") (d #t) (k 0)) (d (n "libipld-core") (r "^0.16.0") (d #t) (k 0)) (d (n "quick-protobuf") (r "^0.8.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.25") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "multihash") (r "^0.18.0") (d #t) (k 2)))) (h "0vyiypfhx6kydyshmdx4lf12i3a7p8lw8s40mb4mvkf4cvwd1wn3")))

