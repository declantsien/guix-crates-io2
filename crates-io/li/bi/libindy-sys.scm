(define-module (crates-io li bi libindy-sys) #:use-module (crates-io))

(define-public crate-libindy-sys-0.1.7-dev-1 (c (n "libindy-sys") (v "0.1.7-dev-1") (d (list (d (n "bindgen") (r "^0.41.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "049pv3crx2xbxrxbiq7agffpd3jjn6g4b9fg1r91zwc93z0azrc2") (l "indy")))

(define-public crate-libindy-sys-0.1.7-dev-0 (c (n "libindy-sys") (v "0.1.7-dev-0") (d (list (d (n "bindgen") (r "^0.41.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)) (d (n "regex") (r "^1.0.0") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1ap7h87b5dydmg6ncnhc3dw9i2ixala11cn0xqqifqwjiyf84kif") (l "indy")))

