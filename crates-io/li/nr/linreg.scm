(define-module (crates-io li nr linreg) #:use-module (crates-io))

(define-public crate-linreg-0.1.1 (c (n "linreg") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.4") (d #t) (k 0)))) (h "0rvw6vbvyfl989chp6zmc8hlcfa47swjiszg2cmnc2rzcpkxjf4m")))

(define-public crate-linreg-0.1.2 (c (n "linreg") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.4") (d #t) (k 0)))) (h "023fx9x2bfvlb3w94lvjmxwsa1vn0rk0c490g878rhx92993i3bi")))

(define-public crate-linreg-0.1.3 (c (n "linreg") (v "0.1.3") (d (list (d (n "num-traits") (r "^0.2.4") (d #t) (k 0)))) (h "1gix5sgcavc7a3hfjbkb1l9fr9zdzkl9h3650wdafxvcdk006lyq")))

(define-public crate-linreg-0.2.0 (c (n "linreg") (v "0.2.0") (d (list (d (n "displaydoc") (r "^0.1.5") (k 0)) (d (n "num-traits") (r "^0.2.4") (d #t) (k 0)))) (h "0iglzmdddv2w5pwzd9f0iplc4viaxjsf9knvk39bcf4k1cpzvipq")))

