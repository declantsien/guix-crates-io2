(define-module (crates-io li nr linregress) #:use-module (crates-io))

(define-public crate-linregress-0.1.0 (c (n "linregress") (v "0.1.0") (d (list (d (n "failure") (r "~0.1.5") (d #t) (k 0)) (d (n "hashbrown") (r "~0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "~0.17.0") (d #t) (k 0)) (d (n "statrs") (r "~0.10.0") (d #t) (k 0)))) (h "1c018859vhm6xybm1ivbqci4vswyy278rg5n69aalnywb246ccls") (f (quote (("unstable"))))))

(define-public crate-linregress-0.1.1 (c (n "linregress") (v "0.1.1") (d (list (d (n "failure") (r "~0.1.5") (d #t) (k 0)) (d (n "hashbrown") (r "~0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "~0.17.0") (d #t) (k 0)) (d (n "statrs") (r "~0.10.0") (d #t) (k 0)))) (h "08nldsis2q6afk5vlvp201m1v3z4gkik9nzfkhcrv4vd16sing9k") (f (quote (("unstable"))))))

(define-public crate-linregress-0.1.2 (c (n "linregress") (v "0.1.2") (d (list (d (n "failure") (r "~0.1.5") (d #t) (k 0)) (d (n "hashbrown") (r "~0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "~0.17.0") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.10.0") (d #t) (k 0)) (d (n "stdtr") (r "^0.1.0") (d #t) (k 2)))) (h "174ad4r2y6al8rm80mp4d7ckcnp1frsw59wdk4vq3aysr0l7bv72") (f (quote (("unstable"))))))

(define-public crate-linregress-0.1.3 (c (n "linregress") (v "0.1.3") (d (list (d (n "failure") (r "~0.1.5") (d #t) (k 0)) (d (n "hashbrown") (r "~0.1.8") (d #t) (k 0)) (d (n "nalgebra") (r "~0.17.0") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.10.0") (d #t) (k 0)) (d (n "stdtr") (r "~0.1.0") (d #t) (k 2)))) (h "0cfd332g3mxlwi0p7ilr12dapzvxawy4qn4nnchfnf7wqfk5g501") (f (quote (("unstable"))))))

(define-public crate-linregress-0.1.4 (c (n "linregress") (v "0.1.4") (d (list (d (n "failure") (r "~0.1.5") (d #t) (k 0)) (d (n "hashbrown") (r "~0.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "~0.18.0") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.10.0") (d #t) (k 0)) (d (n "stdtr") (r "~0.1.0") (d #t) (k 2)))) (h "18lgqqbk1cklwlla527jy0pnqlz8nyd5j94lkiflp94nwnmvlpcs") (f (quote (("unstable"))))))

(define-public crate-linregress-0.1.5 (c (n "linregress") (v "0.1.5") (d (list (d (n "failure") (r "~0.1.5") (d #t) (k 0)) (d (n "hashbrown") (r "~0.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "~0.18.0") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.10.0") (d #t) (k 0)) (d (n "stdtr") (r "~0.1.0") (d #t) (k 2)))) (h "0pca52fjixvz4xcw59rinfvvp2wjz1ssmrsd5mvalzj1r7c8jr0m") (f (quote (("unstable"))))))

(define-public crate-linregress-0.1.6 (c (n "linregress") (v "0.1.6") (d (list (d (n "failure") (r "~0.1.5") (d #t) (k 0)) (d (n "nalgebra") (r "~0.18.0") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.10.0") (d #t) (k 0)) (d (n "stdtr") (r "~0.1.0") (d #t) (k 2)))) (h "0s7l98m5axxfj25c2yh6z4xpswdp9f3jvv4yd3hwrzzj7wmlnrpr") (f (quote (("unstable"))))))

(define-public crate-linregress-0.1.7 (c (n "linregress") (v "0.1.7") (d (list (d (n "failure") (r "~0.1.5") (d #t) (k 0)) (d (n "nalgebra") (r "~0.18.0") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.10.0") (d #t) (k 0)) (d (n "stdtr") (r "~0.1.0") (d #t) (k 2)))) (h "1jdaq3nlw1indvxbyaqalyhs0lnlv3czmilnq2wywxl5j9pwz44j") (f (quote (("unstable"))))))

(define-public crate-linregress-0.2.0 (c (n "linregress") (v "0.2.0") (d (list (d (n "nalgebra") (r "~0.21.0") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.12.0") (d #t) (k 0)) (d (n "stdtr") (r "~0.1.0") (d #t) (k 2)))) (h "0b08vcy2wwhlrqk0s3jpm474m3qch8vlkkq7hazjb28hdwchbgsd") (f (quote (("unstable"))))))

(define-public crate-linregress-0.3.0 (c (n "linregress") (v "0.3.0") (d (list (d (n "nalgebra") (r "~0.21.0") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.12.0") (d #t) (k 0)) (d (n "stdtr") (r "~0.1.0") (d #t) (k 2)))) (h "1iva0wq0b90qjizvsba9k08c4j2d1y1jrazvj05nygfwb2gpcnk3") (f (quote (("unstable"))))))

(define-public crate-linregress-0.4.0 (c (n "linregress") (v "0.4.0") (d (list (d (n "nalgebra") (r "~0.21.0") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.12.0") (d #t) (k 0)) (d (n "stdtr") (r "~0.1.0") (d #t) (k 2)))) (h "0917m74ia66pci87ndbs5cm3vmjk2d8c7yk1qn0si1c3rjsx82hd") (f (quote (("unstable"))))))

(define-public crate-linregress-0.4.1 (c (n "linregress") (v "0.4.1") (d (list (d (n "nalgebra") (r "~0.25.0") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.13.0") (d #t) (k 0)) (d (n "stdtr") (r "~0.1.0") (d #t) (k 2)))) (h "0h6rsjm1ficgbafbhscrn4m9zb7q57q8idrw49hypgfww7964qdk") (f (quote (("unstable"))))))

(define-public crate-linregress-0.4.2 (c (n "linregress") (v "0.4.2") (d (list (d (n "nalgebra") (r "~0.27.1") (d #t) (k 0)) (d (n "quickcheck") (r "~0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "~0.8.0") (d #t) (k 2)) (d (n "statrs") (r "~0.14.0") (d #t) (k 0)) (d (n "stdtr") (r "~0.2.0") (d #t) (k 2)))) (h "06ix09czxpwahs926cn42riciz15fahajmh6xig2gq933ls7zzxi") (f (quote (("unstable"))))))

(define-public crate-linregress-0.4.3 (c (n "linregress") (v "0.4.3") (d (list (d (n "nalgebra") (r "^0.27.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)) (d (n "stdtr") (r "^0.2.0") (d #t) (k 2)))) (h "15gknpmpr6h3kcambs6fzls4bjp703zpmhk9phqlpjmlmmyl0vhy") (f (quote (("unstable"))))))

(define-public crate-linregress-0.4.4 (c (n "linregress") (v "0.4.4") (d (list (d (n "nalgebra") (r "^0.27.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)) (d (n "stdtr") (r "^0.2.0") (d #t) (k 2)))) (h "1f6ybljgh5lahx1b2qb48hfgp18m0fhbqisjcax1mkaybyl03inn") (f (quote (("unstable"))))))

(define-public crate-linregress-0.5.0-alpha.1 (c (n "linregress") (v "0.5.0-alpha.1") (d (list (d (n "nalgebra") (r "^0.27.1") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.2") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8.0") (d #t) (k 2)) (d (n "statrs") (r "^0.15.0") (d #t) (k 0)) (d (n "stdtr") (r "^0.2.0") (d #t) (k 2)))) (h "0aqdyjzw9rbzmyhdpls1ik7q23yx6pd3hs8wjb89v98silbn8qmh") (f (quote (("unstable"))))))

(define-public crate-linregress-0.5.0 (c (n "linregress") (v "0.5.0") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nalgebra") (r "^0.31.0") (d #t) (k 0)))) (h "03xizzgcvaqskqps4sj81mv7msylqf8s11x9qv3prrlfs5jcmg40")))

(define-public crate-linregress-0.5.1 (c (n "linregress") (v "0.5.1") (d (list (d (n "criterion") (r "^0.3.6") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "nalgebra") (r "^0.32.1") (d #t) (k 0)))) (h "0lpf4irwkvs884f45qyg695ss01mw8xq2s9finrfs5zhz2kial27")))

(define-public crate-linregress-0.5.2 (c (n "linregress") (v "0.5.2") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "tiny-bench") (r "^0.3.0") (d #t) (k 2)))) (h "0icwkk7lrm17rn5hnq66ldm2v5iq3fvsnpwg4r6m914z5bsvbq2d")))

(define-public crate-linregress-0.5.3 (c (n "linregress") (v "0.5.3") (d (list (d (n "nalgebra") (r "^0.32.3") (d #t) (k 0)) (d (n "tiny-bench") (r "^0.3.0") (d #t) (k 2)))) (h "1hm9mv1lfhvmdrrygxjyi9aq99pzhmdj884r3wwndlsqrk74vq2d")))

