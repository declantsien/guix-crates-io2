(define-module (crates-io li bf libftd2xx-cc1101) #:use-module (crates-io))

(define-public crate-libftd2xx-cc1101-0.1.0 (c (n "libftd2xx-cc1101") (v "0.1.0") (d (list (d (n "libftd2xx") (r "~0.31.0") (d #t) (k 0)) (d (n "libftd2xx-cc1101-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "modular-bitfield-msb") (r "~0.11.2") (d #t) (k 0)) (d (n "ringbuffer") (r "~0.7.1") (d #t) (k 0)) (d (n "static_assertions") (r "~1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0rw80ljl83dkicwh829ddv0qxfp3wjvy9ar2y7ax6wr6x3yw5hpj") (f (quote (("static" "libftd2xx/static"))))))

