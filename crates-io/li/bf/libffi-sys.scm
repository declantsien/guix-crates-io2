(define-module (crates-io li bf libffi-sys) #:use-module (crates-io))

(define-public crate-libffi-sys-0.2.0 (c (n "libffi-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "clang-sys") (r "= 0.7.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "1178zsh907fcrckmvchmi8wscyscs8av247jpfrki3jqaavk0m4g")))

(define-public crate-libffi-sys-0.3.0 (c (n "libffi-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "clang-sys") (r "= 0.7.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "1ynr672i1xf47r73s59y5w39h6gi301ilnm1zf8d6vzf33mj7bcm")))

(define-public crate-libffi-sys-0.3.1 (c (n "libffi-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "clang-sys") (r "= 0.7.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "1a3d29cdxzylrdaip2bzl0798rx9379rv4pj4mncwmvbd4wyw6im")))

(define-public crate-libffi-sys-0.3.2 (c (n "libffi-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "clang-sys") (r "= 0.7.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "1pqlkkh00s7c2ivsdh2m0zfbhs5ln0gwhlzfgbb078h4rn76m3g0")))

(define-public crate-libffi-sys-0.3.3 (c (n "libffi-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "clang-sys") (r "= 0.7.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "0yppkgra9swqkizyzsm1h251d15liwdxwn5jh83a9343xksqj68m")))

(define-public crate-libffi-sys-0.3.4 (c (n "libffi-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "clang-sys") (r "^0.7.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "0rcgfvbpmspf51v0hgas2lg0n0gxlji10ifma9i6lwdza29s7l9i")))

(define-public crate-libffi-sys-0.4.0 (c (n "libffi-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "curl") (r "^0.3.0") (d #t) (k 1)))) (h "1xqbp573ih2q0qvb6hvjxrib83ckg8m7d7fad4mngn3k3a77v13l")))

(define-public crate-libffi-sys-0.4.1 (c (n "libffi-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "curl") (r "^0.3.0") (d #t) (k 1)))) (h "1in9q1qw3ymy4gb39f1fk7nib4qc1s9n2dpgkjlrhw8c96kqa4gd")))

(define-public crate-libffi-sys-0.4.2 (c (n "libffi-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "curl") (r "^0.3.0") (d #t) (k 1)))) (h "1azpgxgpmq47xncqg2816lzfb25v7wglqwmc304qsgrlaq6f5npg")))

(define-public crate-libffi-sys-0.4.4 (c (n "libffi-sys") (v "0.4.4") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "curl") (r "^0.3.0") (d #t) (k 1)))) (h "0lhs2mrr8a7d08s29qvvfcld49p17n5hpzm2r8646adrahdk5m03")))

(define-public crate-libffi-sys-0.4.6 (c (n "libffi-sys") (v "0.4.6") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "188i37ypi5d16gwn1agzn495a52z881pyqpb1dypnmrjy0wnrj1i")))

(define-public crate-libffi-sys-0.4.7 (c (n "libffi-sys") (v "0.4.7") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "0kyxqfx0q26302hmxgr70zadrwzx42vz45840sd876prm7vd9fxv")))

(define-public crate-libffi-sys-0.5.0 (c (n "libffi-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "1nbnhr9n33h5fvr9pdw7jww9fr0vhnf36265w90ckdndda0pyzyb")))

(define-public crate-libffi-sys-0.5.1 (c (n "libffi-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "1w440wsixg4bia97kf2bilicnfr2qv8iglh9fgrqhf6nqc1brybv")))

(define-public crate-libffi-sys-0.5.2 (c (n "libffi-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "0jy0wsslk85d9j3xyplh0kmv803x409qcsxipx13s41zmnpmnqvl")))

(define-public crate-libffi-sys-0.5.3 (c (n "libffi-sys") (v "0.5.3") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "0kfqzmbj9wisli2hdx24c8r528974w7dqgyvd8njahlpfzh1wlmm")))

(define-public crate-libffi-sys-0.5.4 (c (n "libffi-sys") (v "0.5.4") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "1mv5hw6s1zqll44m3bfbxbwxs4axbzf1q3d5s10m35wg465337xh")))

(define-public crate-libffi-sys-0.5.5 (c (n "libffi-sys") (v "0.5.5") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "0kkfz1y943pwl4pyp5y21b25ysp6qicbjad5wvkjay2dz6gb942g")))

(define-public crate-libffi-sys-0.6.0 (c (n "libffi-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "0wqnfcsfllqkfxri6jkqnk1rlpznv0ijgzg2mqaaayg5ikicyqy8")))

(define-public crate-libffi-sys-0.6.1 (c (n "libffi-sys") (v "0.6.1") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)))) (h "0547h7r3hj3jd2f20d5lkvg4714r1xgg6kxrxwzh33qhp1c3cg2a") (l "ffi")))

(define-public crate-libffi-sys-0.6.2 (c (n "libffi-sys") (v "0.6.2") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "0l8xa2ymj8iim8q266j3k20c78fdgxmijkbdy50ddhj98xxxk087") (f (quote (("system")))) (l "ffi")))

(define-public crate-libffi-sys-0.6.3 (c (n "libffi-sys") (v "0.6.3") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "0wxk4hiszidzbg62rjk419c2jawca86kpblg45k4p8xvzpbxaxa6") (f (quote (("system")))) (l "ffi")))

(define-public crate-libffi-sys-0.6.4 (c (n "libffi-sys") (v "0.6.4") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "1z19wzpzs6jprkpn34gc10r2lhm6x0mcwggsngr2b5fwwvsan4n0") (f (quote (("system")))) (y #t) (l "ffi")))

(define-public crate-libffi-sys-0.7.0 (c (n "libffi-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.49") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "1s9c3gp78ap6bmbvssnlf6zqdnvan7ks1sf9nwm8pbdq0zcvdlzd") (f (quote (("system")))) (l "ffi")))

(define-public crate-libffi-sys-0.8.0 (c (n "libffi-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "10fvnnkk41hg0lbdk2ag3zmy2qpvki50dcpqnmkdd1cqmq0insf2") (f (quote (("system")))) (l "ffi")))

(define-public crate-libffi-sys-0.9.0 (c (n "libffi-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.51") (d #t) (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "0qgywmq99i0bh2mawn69308c92ax3dvcz29k9v0afn7vqad2fyas") (f (quote (("system")))) (l "ffi")))

(define-public crate-libffi-sys-0.9.1 (c (n "libffi-sys") (v "0.9.1") (d (list (d (n "bindgen") (r "^0.52") (d #t) (k 1)) (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)) (d (n "pkg-config") (r "^0.3.13") (d #t) (k 1)))) (h "01jf9v2gr7lnrcms1grpihavahw394w1gdd39ld79i9pwj6f803g") (f (quote (("system")))) (l "ffi")))

(define-public crate-libffi-sys-1.0.0 (c (n "libffi-sys") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "1v771z9dq67lcqrvfpr6bw2d7z6fjx5z90rnsf0wpq8831987j0z") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-1.1.0 (c (n "libffi-sys") (v "1.1.0") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "02qnsan3f83rv3gzikijv4a4s4mk696xl5j27z50cfqw5wa6av9v") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-1.1.1 (c (n "libffi-sys") (v "1.1.1") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "0kcracirrh95bvbpa0d4qrfxpq9sd8yr1z40nsw34mgwvhsyi4by") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-1.1.2 (c (n "libffi-sys") (v "1.1.2") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)) (d (n "make-cmd") (r "^0.1") (d #t) (t "cfg(not(target_env = \"msvc\"))") (k 1)))) (h "1y6h9d2sj4g58ldafwphimhwab879cxsqxjjn63kv7v0l3rhgzzl") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-1.1.3 (c (n "libffi-sys") (v "1.1.3") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "00nfxy2wmg17qdk7lp6qjl5nsqmxw0d92wppiv2025gk6k3mw87l") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-1.2.0 (c (n "libffi-sys") (v "1.2.0") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1bxjsdch4zrhi7qhbljq1llx2ygc87l3igz4vh015lm021p6hjkf") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-1.3.0 (c (n "libffi-sys") (v "1.3.0") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1imwrmql0l8nc1prkkdd8b511jxbz6cxwh66g8djp5s248wl78vq") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-1.3.1 (c (n "libffi-sys") (v "1.3.1") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "1adyqvp0gyiibgg42p0y5rp01n6xwrd6s2lnkmnv9dk87c9hmsxn") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-1.3.2 (c (n "libffi-sys") (v "1.3.2") (d (list (d (n "cc") (r "^1.0.48") (d #t) (t "cfg(target_env = \"msvc\")") (k 1)))) (h "07s06nrh4wzag676fvzl0yiabp4wlk9902g4nfw4w1n0i3na10vj") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-2.0.0 (c (n "libffi-sys") (v "2.0.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "059f4kyls1zh3w8zxifl4jwczpy1z8c5ck9k4788fywxy2vhchdb") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-2.0.1 (c (n "libffi-sys") (v "2.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0q2avv154v2wpkjdmp3ayzgic3pq4rh6xkiq9k1fksm8wl18vrw4") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-2.1.0 (c (n "libffi-sys") (v "2.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "18wx7payg9ym187axdw34gf1xx0vcq5mzld983hgy9rs0qgg3ihi") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-2.2.0 (c (n "libffi-sys") (v "2.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0x47fh6ib14rrynbvdfx6zm2lj77l2m60h1fdjpj5blbgyv5zlgp") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-2.2.1 (c (n "libffi-sys") (v "2.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1ix7vjwnfz1p07qw34pcif2hh7pp5phakfg8f6bhdz60g1xhcrfw") (f (quote (("system") ("complex")))) (l "ffi")))

(define-public crate-libffi-sys-2.3.0 (c (n "libffi-sys") (v "2.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0g7vpq1pggl43mclblqydmnizp0znlmkq65lh6bm5s2p1hb1aqgk") (f (quote (("system") ("complex")))) (l "ffi")))

