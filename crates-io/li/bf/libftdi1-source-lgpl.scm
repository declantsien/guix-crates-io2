(define-module (crates-io li bf libftdi1-source-lgpl) #:use-module (crates-io))

(define-public crate-libftdi1-source-lgpl-1.4.0 (c (n "libftdi1-source-lgpl") (v "1.4.0") (h "0z8sjqc15sg9n2mj5dxx65n4wwgwcbws97l3b1p26iimlls2m0ws") (l "ftdi1_source")))

(define-public crate-libftdi1-source-lgpl-1.5.0 (c (n "libftdi1-source-lgpl") (v "1.5.0") (h "1cgx15xiv796nadpzw4mzmnqg690dv2gxkmhqmcqzdy7726r24j4") (l "ftdi1_source")))

