(define-module (crates-io li bf libfeedback) #:use-module (crates-io))

(define-public crate-libfeedback-0.0.1 (c (n "libfeedback") (v "0.0.1") (d (list (d (n "ffi") (r "^0.0.1") (d #t) (k 0) (p "libfeedback-sys")) (d (n "gio") (r "^0.19") (f (quote ("v2_70"))) (d #t) (k 0)) (d (n "glib") (r "^0.19") (f (quote ("v2_66"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mrssz8n6jknvmw12varvc8s1mz5chisb8j8b3bcwvs4xkkbkb59") (f (quote (("v0_3" "ffi/v0_3"))))))

