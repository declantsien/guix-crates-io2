(define-module (crates-io li bf libflo) #:use-module (crates-io))

(define-public crate-libflo-0.1.1 (c (n "libflo") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libflo_dynamic_event") (r "^0.1") (d #t) (k 0)) (d (n "libflo_event") (r "^0.1") (d #t) (k 0)) (d (n "libflo_func") (r "^0.1") (d #t) (k 0)) (d (n "libflo_module") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "1pki5vs9qi98ljs47x5crr44lfb075b20qlz6yd4w4sw00gp4g3n")))

(define-public crate-libflo-0.1.2 (c (n "libflo") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libflo_dynamic_event") (r "^0.1") (d #t) (k 0)) (d (n "libflo_event") (r "^0.1") (d #t) (k 0)) (d (n "libflo_func") (r "^0.1") (d #t) (k 0)) (d (n "libflo_module") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)))) (h "0pa8y795fny3dyk9yvrnfgcxprrk00ydqfmbpc8q5jv0vpb7y2s6")))

