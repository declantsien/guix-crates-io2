(define-module (crates-io li bf libfprint-rs) #:use-module (crates-io))

(define-public crate-libfprint-rs-0.1.0 (c (n "libfprint-rs") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "libfprint-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1jwh96cql9v2nhrgjdgf1f3p930qc2nd6j3h71jzjdx60mg9w47d")))

(define-public crate-libfprint-rs-0.1.1 (c (n "libfprint-rs") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "libfprint-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0a8bsp9vsyiwh78ccsrgd7rkq94l0g2iy2nmzv5fg9k3mq9iplq4") (y #t)))

(define-public crate-libfprint-rs-0.1.2 (c (n "libfprint-rs") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libfprint-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "19flwvc87xyi7aav8nk3hxbh5cz46zdir04ay4pxwrqfwrm632az") (y #t)))

(define-public crate-libfprint-rs-0.1.3 (c (n "libfprint-rs") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libfprint-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0rxryfn4306j17ysdlacsfvmpyd7g6gwm8njqa1m4dvnc4xhcncv") (y #t)))

(define-public crate-libfprint-rs-0.1.4 (c (n "libfprint-rs") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libfprint-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "08yvql9i2wnb5brvzwn0hmazag1xjv80p77w36gwm17p6src2ib9") (y #t)))

(define-public crate-libfprint-rs-0.1.5 (c (n "libfprint-rs") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libfprint-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0cb4bsknn3fxp6z3156psmc9pv32302z0dk45h37lc3y4iagr60d") (y #t)))

(define-public crate-libfprint-rs-0.1.6 (c (n "libfprint-rs") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libfprint-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "0s69na1lhnqbvyppgl87ypj6zp3g05dzp3qhgnvvqdp036pchcsp")))

(define-public crate-libfprint-rs-0.1.7 (c (n "libfprint-rs") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libfprint-sys") (r "^0.1.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1mj1h0ca4j8pi90zj2a79s87c7930q20ja1i1z7071v1sd1an0rb") (y #t)))

(define-public crate-libfprint-rs-0.1.8 (c (n "libfprint-rs") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libfprint-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "1qjxf0msd0lvw5nxdj1aacppsc4mn3pjz8w4inff514f8hvadi15")))

(define-public crate-libfprint-rs-0.1.9 (c (n "libfprint-rs") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libfprint-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.27") (d #t) (k 1)))) (h "00209n9fhji99ck08w7x20z1giqkjn2qq9zh5q33xli13pg2yj6d")))

(define-public crate-libfprint-rs-0.1.10 (c (n "libfprint-rs") (v "0.1.10") (d (list (d (n "libfprint-sys") (r "^0.1.7") (d #t) (k 0)))) (h "0y9c54b4kwkr4rbirxjgycamnr23xw26hg5q45zh2mnvpl8hvhdn")))

(define-public crate-libfprint-rs-0.1.11 (c (n "libfprint-rs") (v "0.1.11") (d (list (d (n "libfprint-sys") (r "^0.1.7") (d #t) (k 0)))) (h "065z5bz4dyhhb4pbn4dvrcgpfgxq1mm7vjai6l59sqqbdg04faha")))

(define-public crate-libfprint-rs-0.1.12 (c (n "libfprint-rs") (v "0.1.12") (d (list (d (n "gio") (r "^0.18.1") (d #t) (k 0)) (d (n "glib") (r "^0.18.1") (d #t) (k 0)) (d (n "libfprint-sys") (r "^0.1.8") (d #t) (k 0)))) (h "1hcgappily5f23vv4n9605m6ldf2j3bm7vrmmibcf1skaj9f8pv4")))

(define-public crate-libfprint-rs-0.2.0 (c (n "libfprint-rs") (v "0.2.0") (d (list (d (n "gio") (r "^0.18.1") (d #t) (k 0)) (d (n "glib") (r "^0.18.1") (d #t) (k 0)) (d (n "libfprint-sys") (r "^0.1.8") (d #t) (k 0)))) (h "0qllvnlmslwfnvwn47gsxns1s1ihn1y2d3j4fsa3pnw94g6v22ij")))

(define-public crate-libfprint-rs-0.2.1 (c (n "libfprint-rs") (v "0.2.1") (d (list (d (n "gio") (r "^0.18.1") (d #t) (k 0)) (d (n "glib") (r "^0.18.1") (d #t) (k 0)) (d (n "libfprint-sys") (r "^0.1.8") (d #t) (k 0)))) (h "1fp5s8i9jfbqbv5w1kgfchkjm1f2xvpjqhi32vn7ix8vglsy7mya")))

