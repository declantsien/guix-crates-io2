(define-module (crates-io li bf libflate_lz77) #:use-module (crates-io))

(define-public crate-libflate_lz77-0.1.0 (c (n "libflate_lz77") (v "0.1.0") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 2)))) (h "12bvrzg2kzw14whl39n3c5r1ip2dci95h93aj6vwijrds23b94iv")))

(define-public crate-libflate_lz77-0.1.1 (c (n "libflate_lz77") (v "0.1.1") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 2)))) (h "1vrw0qvwspd4s3qhzqp4d4hnhm1wxxzd2s9dxlzifcnw4rvrv7kk")))

(define-public crate-libflate_lz77-1.0.0 (c (n "libflate_lz77") (v "1.0.0") (d (list (d (n "libflate") (r "^0.1") (d #t) (k 2)))) (h "06xir79gmp97mdnlnjclk5zlzgkf5s6qvwilcd4gq9j9gngz11ij")))

(define-public crate-libflate_lz77-1.1.0 (c (n "libflate_lz77") (v "1.1.0") (d (list (d (n "libflate") (r "^1") (d #t) (k 2)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)))) (h "0nig3z2wjwk27ia066knp633akm00v017vny976sy29l970399rr")))

(define-public crate-libflate_lz77-1.2.0 (c (n "libflate_lz77") (v "1.2.0") (d (list (d (n "core2") (r "^0.4") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "hashbrown") (r "^0.13") (o #t) (d #t) (k 0)) (d (n "libflate") (r "^1") (d #t) (k 2)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)))) (h "1gxc75fb2sk0xgrh3qxvxcx1l93yhmyxn9241r251wl5zj5klbd5") (f (quote (("no_std" "core2" "hashbrown"))))))

(define-public crate-libflate_lz77-2.0.0 (c (n "libflate_lz77") (v "2.0.0") (d (list (d (n "core2") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "hashbrown") (r "^0.13") (d #t) (k 0)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)))) (h "096map4a8xvf3819kgdxbg08nf97nggg9lvrdgn7c5a5ikxm4pxy") (f (quote (("std" "core2/std") ("default" "std"))))))

(define-public crate-libflate_lz77-2.1.0 (c (n "libflate_lz77") (v "2.1.0") (d (list (d (n "core2") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "hashbrown") (r "^0.14") (d #t) (k 0)) (d (n "libflate") (r "^2.0") (k 2)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)))) (h "0gc6h98jwigscasz8vw1vv65b3rismqcbndb8hf6yf4z6qxxgq76") (f (quote (("std" "core2/std" "libflate/std") ("default" "std"))))))

