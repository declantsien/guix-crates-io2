(define-module (crates-io li bf libfdisk) #:use-module (crates-io))

(define-public crate-libfdisk-0.1.3 (c (n "libfdisk") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libfdisk-sys") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.16.1") (d #t) (k 0)))) (h "16888qblxg959s9g2fgf6vgbksijka9x250x6g122r2zqaqckwzb")))

(define-public crate-libfdisk-0.1.4 (c (n "libfdisk") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfdisk-sys") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.21") (d #t) (k 0)))) (h "1fcxwrhqb3bpdr3gnpk9s5k39cslzq6ggwb09zkrldjcr55vsylj")))

(define-public crate-libfdisk-0.1.5 (c (n "libfdisk") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.12") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libfdisk-sys") (r "^0.1") (d #t) (k 0)) (d (n "nix") (r "^0.24") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "08s8yhyip9gdilnjdindq33zvlrcigdnl2qqvlvjw5jxl4a2m550")))

