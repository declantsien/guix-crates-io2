(define-module (crates-io li bf libfuzzer-sys) #:use-module (crates-io))

(define-public crate-libfuzzer-sys-0.1.0 (c (n "libfuzzer-sys") (v "0.1.0") (d (list (d (n "arbitrary") (r "^0.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "16z14mb82p7mg165bd9n4hvmfyh2wlrrhzzmqzb8fyd76cvs90yq") (y #t)))

(define-public crate-libfuzzer-sys-0.1.1 (c (n "libfuzzer-sys") (v "0.1.1") (d (list (d (n "arbitrary") (r "^0.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "12s2zrcf532w3wfw8a0v77wpxykn881h9aixfi9gvlsxsbxjm7md")))

(define-public crate-libfuzzer-sys-0.2.0 (c (n "libfuzzer-sys") (v "0.2.0") (d (list (d (n "arbitrary") (r "^0.3.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0sarprqmcmbgpkwsni0w7jijam04zq1hr69pqr1rzjx96vqf9ycl") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.2.1 (c (n "libfuzzer-sys") (v "0.2.1") (d (list (d (n "arbitrary") (r "^0.3.2") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0dcsy1rfshgl3f0mmd3z52r8s51lj6j5s85yrc5awakspv99r5iy") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.3.0 (c (n "libfuzzer-sys") (v "0.3.0") (d (list (d (n "arbitrary") (r "^0.4.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1bdfrmfhqasnm66plfw8f5779dq9djyh45z37qymsckmgwscl0y1") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.3.1 (c (n "libfuzzer-sys") (v "0.3.1") (d (list (d (n "arbitrary") (r "^0.4.0") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "06sysqikf3ls6kcj7vm2f07hlx5b8ms0hrp4s4l8k849qpy9ly7v") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.3.2 (c (n "libfuzzer-sys") (v "0.3.2") (d (list (d (n "arbitrary") (r "^0.4.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "096grnhaqw99a05700hwmg3irr39gmcw9llv0swk6dg2p2a8fwcd") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.3.3 (c (n "libfuzzer-sys") (v "0.3.3") (d (list (d (n "arbitrary") (r "^0.4.1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1agvmdvawlsywgnr2x5fyaq8xbavj4glznwha5blf9c7wy3amk47") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.3.4 (c (n "libfuzzer-sys") (v "0.3.4") (d (list (d (n "arbitrary") (r "^0.4.6") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1k3s0wj1bb129gqypl2gr76q8mack43ysrd9fznradzlcaml537f") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.3.5 (c (n "libfuzzer-sys") (v "0.3.5") (d (list (d (n "arbitrary") (r "^0.4.6") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1vv42rpvmfr83hlblyrjf8ifilsmc3d5gcznblmghx5jnsj89wgw") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.4.0 (c (n "libfuzzer-sys") (v "0.4.0") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0ihfzp1d7p9smww9w8sxg1zlrhqz94qvfch98jcjyamw6zb7bjc6") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.4.1 (c (n "libfuzzer-sys") (v "0.4.1") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0.20") (d #t) (k 2)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0nqgxy5r8gg2frp01yij75j9p65dgyjdaqv591vavf04crawd7ca") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.4.2 (c (n "libfuzzer-sys") (v "0.4.2") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0.20") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1cyyy436zamxlrvygjlg90c2m6ibvdgfada2n3pxymcbdr5aia9n") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.4.3 (c (n "libfuzzer-sys") (v "0.4.3") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "flate2") (r "^1.0.20") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "07cr9jlbq6y68iw964bbsrr3lx5a09c5ip40cks2v8dnxam48qik") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.4.4 (c (n "libfuzzer-sys") (v "0.4.4") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0n6sh9sq9z6r4m1970hjqa36h4hyqiy5m8bd0vylm08rzs25c65f") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.4.5 (c (n "libfuzzer-sys") (v "0.4.5") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0f306q4cllzlqr9llqayv6d0nmwda1dzvdviv802irly2f8zizy8") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.4.6 (c (n "libfuzzer-sys") (v "0.4.6") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0kmxw78d0palqvdimaknf3r17zx5gprwqv374x5sk845mr89kc5y") (f (quote (("arbitrary-derive" "arbitrary/derive"))))))

(define-public crate-libfuzzer-sys-0.4.7 (c (n "libfuzzer-sys") (v "0.4.7") (d (list (d (n "arbitrary") (r "^1") (d #t) (k 0)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "flate2") (r "^1.0.24") (d #t) (k 2)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1xz70z8q85f80wdrc0m0flck73cqdhj5ajgd7ywg50pbaxazsv59") (f (quote (("link_libfuzzer") ("default" "link_libfuzzer") ("arbitrary-derive" "arbitrary/derive"))))))

