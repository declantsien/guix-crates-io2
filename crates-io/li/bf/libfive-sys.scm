(define-module (crates-io li bf libfive-sys) #:use-module (crates-io))

(define-public crate-libfive-sys-0.1.0 (c (n "libfive-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "1abafxb0icz5x94mqzkdzpiaxn2q11vvxcl0m679fhba0wv4q5vm") (f (quote (("packed_opcodes")))) (y #t)))

(define-public crate-libfive-sys-0.1.1 (c (n "libfive-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.45") (d #t) (k 1)))) (h "01p90yxkgp4yziym82prnvz0h1nhbdcpz0yag7sl9whcvbv89l5q") (f (quote (("packed_opcodes"))))))

(define-public crate-libfive-sys-0.2.0 (c (n "libfive-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0q0jhj966ivs521cplpkh8qg0qlnyfm0k34d1b7inv8vb84mscz7") (f (quote (("packed_opcodes"))))))

(define-public crate-libfive-sys-0.3.0 (c (n "libfive-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1k58v43d1ph4mlnixl6ya1fgn3pblznl1sc2859psiicavfnm1zf") (f (quote (("packed_opcodes"))))))

