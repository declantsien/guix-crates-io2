(define-module (crates-io li bf libffi) #:use-module (crates-io))

(define-public crate-libffi-0.1.0 (c (n "libffi") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "0mihpvd8rysp19bjc25jmgdlwrz9ch1rzi8ja7g1klhl0vjsmmlr")))

(define-public crate-libffi-0.1.1 (c (n "libffi") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "1l3rhw8dyfacc5y0zq66zlr6xp21l708qw2qxz6ykr8pxfxyg25c")))

(define-public crate-libffi-0.1.2 (c (n "libffi") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "186af4yxrpssmnzfwdgqd35mp78van7swsnas7jfyqkqqlnfk400")))

(define-public crate-libffi-0.1.3 (c (n "libffi") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "0mblmx8iy0gflr5x97wnfjy8v9ir0c46jr67lim3fiad9xv2c99j")))

(define-public crate-libffi-0.1.4 (c (n "libffi") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "1f1k25jx9fxy285a2gbl8sg5x0rd45xfgribyfgnzflkbl0pxrhv")))

(define-public crate-libffi-0.1.5 (c (n "libffi") (v "0.1.5") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "1315wx36wy08jjrr9q10lp0hz8pw856lyncmwm5740dscq1z93n2")))

(define-public crate-libffi-0.1.6 (c (n "libffi") (v "0.1.6") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "13jyi83jz6j50jmz0khbbsjpz4fpzxqa74zkky4qks5gjqjis4p2")))

(define-public crate-libffi-0.1.7 (c (n "libffi") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "1q264zhyakhs3prci77rrww3hd5rsiglq0fz3dk3db25vixn205k") (f (quote (("unique"))))))

(define-public crate-libffi-0.1.8 (c (n "libffi") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "0h8d7m0wmna4ysw67qbkas9kn9c2vd2flbv7n75zz4iwxa1zpbid") (f (quote (("unique"))))))

(define-public crate-libffi-0.1.9 (c (n "libffi") (v "0.1.9") (d (list (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "0mrrbaw6y399cxgqjwv144jg3fng31dg7vh6s7cm6dpzbgsn90wb") (f (quote (("unique"))))))

(define-public crate-libffi-0.1.11 (c (n "libffi") (v "0.1.11") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "0jhigxsmv721na7g2r88wc4ccg5bh4h1lshg1hz7rm68s5vy85jx") (f (quote (("unique"))))))

(define-public crate-libffi-0.1.12 (c (n "libffi") (v "0.1.12") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.18") (d #t) (k 1)) (d (n "clang-sys") (r "= 0.7.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.7") (d #t) (k 1)))) (h "0hl2zszv8r7w49c9ppl5in2z7qarrv11rlm75qp4pjhmq7vpq0js") (f (quote (("unique"))))))

(define-public crate-libffi-0.2.0 (c (n "libffi") (v "0.2.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0fa6qrzwayizhf8b8wddzrl806961d70jmzin455md5v9zf26m4v") (f (quote (("unique"))))))

(define-public crate-libffi-0.2.1 (c (n "libffi") (v "0.2.1") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1vssb6b97i1iv03jrk5qb4d9s0qf7w6xajpkm520362kk814k1pj") (f (quote (("unique"))))))

(define-public crate-libffi-0.3.0 (c (n "libffi") (v "0.3.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1c0scfi12bl6rhknh78a8h3k9zcx5sqkrdi81i0fjj2ysjwgxcis") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.3.1 (c (n "libffi") (v "0.3.1") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.3.0") (d #t) (k 0)))) (h "1mpwjw0p9rb52by5pbzq28srb970y1lrf4ypwvzshkb7a5i7f6js") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.3.2 (c (n "libffi") (v "0.3.2") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.3.2") (d #t) (k 0)))) (h "1a76gm6ra7g5x9vfdw917vizgnnfrxdgp7qcpyns36a36s3zyzcm") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.3.3 (c (n "libffi") (v "0.3.3") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.3.2") (d #t) (k 0)))) (h "0qn8vj5fzar0raap55wym0m5975qladmvanbrd7lv906dq2772ja") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.3.4 (c (n "libffi") (v "0.3.4") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.3.4") (d #t) (k 0)))) (h "0yj1aif1b98hwm54yf0xv3s9hmjaj5dk3j0rhcv4x02s3906d8j7") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.4.0 (c (n "libffi") (v "0.4.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.4.0") (d #t) (k 0)))) (h "01bqzfbgx1fhiaqrpcq16pmaykw68h4a95xhq7sdw8s5xg98yh9k") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.4.1 (c (n "libffi") (v "0.4.1") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.4.1") (d #t) (k 0)))) (h "166dnfg3l84rmzjhczhk91rgrnmlz95knr3nch2amvq5w9vc9y4h") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.5.0 (c (n "libffi") (v "0.5.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.5.0") (d #t) (k 0)))) (h "0d5lxqrbivn3xzglprh8i89mvwaksibg94ahi0x2lyb1k0cjpz6r") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.5.2 (c (n "libffi") (v "0.5.2") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.5.2") (d #t) (k 0)))) (h "0w8fl6bdxbyc9f9w9qfxx7qf660fizv3kx3rihxmhprhwq1j0syn") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.5.3 (c (n "libffi") (v "0.5.3") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.5.2") (d #t) (k 0)))) (h "0pfgnk04xs6kbp4g9fic44h5xcli7acb8m6hr0ijj274vgrq2did") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.6.0 (c (n "libffi") (v "0.6.0") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.5.2") (d #t) (k 0)))) (h "1pp5fica3fcn75dqrry39bm1lpchjjqdrf3v9xgd9mls51rldk31") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.6.1 (c (n "libffi") (v "0.6.1") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.5.3") (d #t) (k 0)))) (h "07946gz2n3m2q529wyh68y8lxpphggfzcpg173ai93v6kzgq507z") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.6.2 (c (n "libffi") (v "0.6.2") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1i4svd1h84gh12qpxwvj8j41dyj9k32r85w4k495r9sc4mxbw9pf") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.6.3 (c (n "libffi") (v "0.6.3") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "clippy") (r "^0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.6.0") (d #t) (k 0)))) (h "0r5nss2q8cg64yh0rmvh1inf5a2ljhi0mqj48k5ra2i637sxia5m") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.6.4 (c (n "libffi") (v "0.6.4") (d (list (d (n "abort_on_panic") (r "^1.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.6.0") (d #t) (k 0)))) (h "1cbq8mcgmh4k001ddm0dp32p0d8mwn5qasrbfi6m27qqfg1dmafq") (f (quote (("unique") ("complex"))))))

(define-public crate-libffi-0.7.0 (c (n "libffi") (v "0.7.0") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.7.0") (d #t) (k 0)))) (h "0lzzv72g7y1fjw1p16dfkl1skrc874w6132f60y3ky7w2839fx7y") (f (quote (("complex"))))))

(define-public crate-libffi-0.8.0 (c (n "libffi") (v "0.8.0") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.8.0") (d #t) (k 0)))) (h "1sddmy4yx038x4z26kmlv737a2af09qzmz4dbaqpwa180gsfl0jk") (f (quote (("system" "libffi-sys/system") ("complex"))))))

(define-public crate-libffi-0.9.0 (c (n "libffi") (v "0.9.0") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libffi-sys") (r "^0.9.0") (d #t) (k 0)))) (h "1wn6zqb48dalv6xz5lb8ws8awd57jr33jq8ayqxziiswj9azx3n1") (f (quote (("system" "libffi-sys/system") ("complex"))))))

(define-public crate-libffi-1.0.0 (c (n "libffi") (v "1.0.0") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libffi-sys") (r "^1.0") (d #t) (k 0)))) (h "1pcmlyi5wbj3f69d02a803svjcks03db5xma91rw4l9dw8zgizms") (f (quote (("system" "libffi-sys/system") ("complex"))))))

(define-public crate-libffi-1.0.1 (c (n "libffi") (v "1.0.1") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libffi-sys") (r "^1.1") (d #t) (k 0)))) (h "1bkjcdl02wrw3qp4hbmmnz6zgfww3fjf2ajs8v1rjz6556hbiz7l") (f (quote (("system" "libffi-sys/system") ("complex"))))))

(define-public crate-libffi-2.0.0 (c (n "libffi") (v "2.0.0") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libffi-sys") (r "^1.1") (d #t) (k 0)))) (h "083gps9kj8lc3xx3j4d2plh3b23fj8kfq6dc5j0q83jqc0cm86hs") (f (quote (("system" "libffi-sys/system") ("complex"))))))

(define-public crate-libffi-2.0.1 (c (n "libffi") (v "2.0.1") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libffi-sys") (r "^1.1") (d #t) (k 0)))) (h "1gq4f0l3nrip2g847liy6hjhsn6q8vpmlwandjrs144lv0mva1ab") (f (quote (("system" "libffi-sys/system") ("complex"))))))

(define-public crate-libffi-3.0.0 (c (n "libffi") (v "3.0.0") (d (list (d (n "abort_on_panic") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libffi-sys") (r "^2.0") (d #t) (k 0)))) (h "07dwfvgrqwq0df63r6qgpgrz34gzjpc562l31jyr9vny5lx0j20f") (f (quote (("system" "libffi-sys/system") ("complex"))))))

(define-public crate-libffi-3.0.1 (c (n "libffi") (v "3.0.1") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libffi-sys") (r "^2.0") (d #t) (k 0)))) (h "1rl7s9xd0gmrncs5lg5bckjlkdh2vx0mxbhhg2qs7yqnzcz4ni8y") (f (quote (("system" "libffi-sys/system") ("complex"))))))

(define-public crate-libffi-3.1.0 (c (n "libffi") (v "3.1.0") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libffi-sys") (r "^2.1") (d #t) (k 0)))) (h "0q6g2i07rbwfl2a11zpzi7lpl5flkv1l2dwlhbb3r3s29idnvc3c") (f (quote (("system" "libffi-sys/system") ("complex"))))))

(define-public crate-libffi-3.2.0 (c (n "libffi") (v "3.2.0") (d (list (d (n "libc") (r "^0.2.65") (d #t) (k 0)) (d (n "libffi-sys") (r "^2.2") (d #t) (k 0)))) (h "1qki30sf5vpwqcgl6s04h8px9qjjvqj9fya1qk7dbqs860j6r0nf") (f (quote (("system" "libffi-sys/system") ("complex"))))))

