(define-module (crates-io li bf libflac-sys) #:use-module (crates-io))

(define-public crate-libflac-sys-0.1.0 (c (n "libflac-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "132z905xw2rsd8a230jjlvp4lx5q6jwxsigsknh55l7ibalyq756") (f (quote (("ogg") ("default" "ogg"))))))

(define-public crate-libflac-sys-0.2.0 (c (n "libflac-sys") (v "0.2.0") (d (list (d (n "cmake") (r ">=0.1.45, <0.2.0") (o #t) (d #t) (k 1)) (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "1hr8ak6vc6ci55zrffc88w8l0m35w8wk1vyvz9vqfggjinbs2nxg") (f (quote (("default" "build-flac" "build-ogg") ("build-ogg" "build-flac") ("build-flac" "cmake")))) (l "FLAC")))

(define-public crate-libflac-sys-0.2.1 (c (n "libflac-sys") (v "0.2.1") (d (list (d (n "cmake") (r "^0.1.45") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "038yjahd7d775xvnf9642qafi0wfvqwxlfz8f86zfg0ikgbczgyv") (f (quote (("default" "build-flac" "build-ogg") ("build-ogg" "build-flac") ("build-flac" "cmake")))) (l "FLAC")))

(define-public crate-libflac-sys-0.2.2 (c (n "libflac-sys") (v "0.2.2") (d (list (d (n "cmake") (r "^0.1.45") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1rslkv5niv23j5vh57x5caq5aqhj0sznskc1fs4b58m8w7p7p2im") (f (quote (("default" "build-flac" "build-ogg") ("build-ogg" "build-flac") ("build-flac" "cmake")))) (l "FLAC")))

(define-public crate-libflac-sys-0.3.0 (c (n "libflac-sys") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1.45") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0y1x3mnmxql1cnxg06mh0x6gcn05f5f32248n6jgnb3b3p0gfhwj") (f (quote (("default" "build-flac" "build-ogg") ("build-ogg" "build-flac") ("build-flac" "cmake")))) (l "FLAC")))

(define-public crate-libflac-sys-0.3.1 (c (n "libflac-sys") (v "0.3.1") (d (list (d (n "cmake") (r "^0.1.45") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0nrnf2kabqqk28wkn3kv1c6dmr356sphynhx0m5yizqs5ic9n3av") (f (quote (("default" "build-flac" "build-ogg") ("build-ogg" "build-flac") ("build-flac" "cmake")))) (l "FLAC")))

