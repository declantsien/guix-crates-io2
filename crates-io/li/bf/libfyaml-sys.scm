(define-module (crates-io li bf libfyaml-sys) #:use-module (crates-io))

(define-public crate-libfyaml-sys-0.1.0 (c (n "libfyaml-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d9agr90mc8mfx5sd81njdhq0d3q2df02vbxs8cfq6dbbv6i8j80") (l "libfyaml") (r "1.63")))

(define-public crate-libfyaml-sys-0.1.1 (c (n "libfyaml-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yj54ll1x3jdqci9km3zbx9a7sg1805820lsjj73ib7p33rvgdrj") (l "libfyaml") (r "1.63")))

(define-public crate-libfyaml-sys-0.1.2+fy0.7.12 (c (n "libfyaml-sys") (v "0.1.2+fy0.7.12") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "084abdcgp6irc9gn86f8cya79qphkdk9p8h5gkprir6ymw72wzm1") (l "libfyaml") (r "1.63")))

(define-public crate-libfyaml-sys-0.1.3+fy0.7.12 (c (n "libfyaml-sys") (v "0.1.3+fy0.7.12") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wiryp9sbpcryf5v1i50wd8km3q3bgh51bvhiln7qq849wkrzj63") (l "libfyaml") (r "1.63")))

(define-public crate-libfyaml-sys-0.2.0+fy0.8.0 (c (n "libfyaml-sys") (v "0.2.0+fy0.8.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0skfkpxcw1qjycy0dcw8v6ai3bbn9scp5dd7jmhjbn4wcbl08g1d") (l "libfyaml") (r "1.63")))

(define-public crate-libfyaml-sys-0.2.1+fy0.8.0 (c (n "libfyaml-sys") (v "0.2.1+fy0.8.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "177vg5yf67wa4s0nnc1w19cxbwgn6vbrbcvn3arqarlwf5kgccdc") (l "libfyaml") (r "1.63")))

(define-public crate-libfyaml-sys-0.2.2+fy0.8.0 (c (n "libfyaml-sys") (v "0.2.2+fy0.8.0") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ws579rwjj1lkwgaakbvyx77y5alc51lc5zahz6b9v7kwqlhdbmk") (l "libfyaml") (r "1.63")))

(define-public crate-libfyaml-sys-0.2.3+fy0.8.0 (c (n "libfyaml-sys") (v "0.2.3+fy0.8.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xj72q78lsya0f1b953b4286kxqsi8qvxh7s7rjhmngd2xba20ad") (l "libfyaml") (r "1.63")))

(define-public crate-libfyaml-sys-0.2.4+fy0.8.0 (c (n "libfyaml-sys") (v "0.2.4+fy0.8.0") (d (list (d (n "bindgen") (r "^0.66") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wczdydnnwcfl3zl8iikl5y2my27xfvsys9njh5y4i6fcvc08d4s") (l "libfyaml") (r "1.63")))

(define-public crate-libfyaml-sys-0.2.5+fy0.8.0 (c (n "libfyaml-sys") (v "0.2.5+fy0.8.0") (d (list (d (n "bindgen") (r "^0.68") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vfjcq46376v4fx4dn0kmgjmpjizspga1zrcsjxjak3scfm8aj5k") (l "libfyaml") (r "1.63")))

(define-public crate-libfyaml-sys-0.2.6+fy0.8.0 (c (n "libfyaml-sys") (v "0.2.6+fy0.8.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18r70p0pi67y1nnp0xik9dak74rcvcc8gvy9mp4fp5dx6zibm3k5") (l "libfyaml") (r "1.63")))

