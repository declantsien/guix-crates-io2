(define-module (crates-io li bf libft) #:use-module (crates-io))

(define-public crate-libft-0.1.0 (c (n "libft") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "csv") (r "^1.2.2") (d #t) (k 0)) (d (n "pgn-reader") (r "^0.25.0") (d #t) (k 0)) (d (n "rayon") (r "^1.8.0") (d #t) (k 0)) (d (n "redb") (r "^1.2.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (d #t) (k 0)) (d (n "serde_with") (r "^3.3.0") (d #t) (k 0)) (d (n "shakmaty") (r "^0.26") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1kfapx02f8gq3lmld40q0p09xbsb72zn69kqbisf4ydkdgs656ym")))

