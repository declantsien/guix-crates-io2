(define-module (crates-io li bf libferroxid) #:use-module (crates-io))

(define-public crate-libferroxid-0.1.0 (c (n "libferroxid") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1qarv3rvdl93zbsdxcal8pavjhbg4xcs778hayqjbi8r7l7f0myv")))

(define-public crate-libferroxid-0.1.1 (c (n "libferroxid") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0xg43vwsdm32xci10kiz8q7629nmga0nzxd3rv6ph5v6908mlr00")))

