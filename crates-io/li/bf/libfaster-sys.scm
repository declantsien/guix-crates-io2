(define-module (crates-io li bf libfaster-sys) #:use-module (crates-io))

(define-public crate-libfaster-sys-0.1.0 (c (n "libfaster-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "01zbcc1z5q8rwqzn0c3n1wmdvn1dg4vdr79i3pq14iy591s7hzrq") (l "faster")))

(define-public crate-libfaster-sys-0.2.0 (c (n "libfaster-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18ajfrwhhf95prgkf17hkid54wwv3rdadbxgrgf4c8brv0kyfxk4") (l "faster")))

(define-public crate-libfaster-sys-0.3.0 (c (n "libfaster-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f0bpwlj7wm9c2ys589akf7rkkwz9ivbz647iyzxs7zbjgljs72n") (l "faster")))

(define-public crate-libfaster-sys-0.4.0 (c (n "libfaster-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jwhkia2nxml28na7c0gi8fjdmcxav8ih8hf282bsryw3c86aqbg") (l "faster")))

(define-public crate-libfaster-sys-0.5.0 (c (n "libfaster-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bqpcphccyq7fypvshjc5854zl5z9pkqpa3zcy3430l01ky3x12f") (l "faster")))

(define-public crate-libfaster-sys-0.5.1 (c (n "libfaster-sys") (v "0.5.1") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10v0ax62vf1h36mj16s9mq0xylvdnv16dxl1zgl3crqbj7cd54a4") (l "faster")))

(define-public crate-libfaster-sys-0.5.2 (c (n "libfaster-sys") (v "0.5.2") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "12c7brrbpx4xpwpr94x8z1fkscdhrw32d14k5l9xh4a7aj2vbfkw") (l "faster")))

(define-public crate-libfaster-sys-0.5.3 (c (n "libfaster-sys") (v "0.5.3") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "137jm5wym1jw2vd70f17vd7cmjgw6cdas5jwip4smvgmzg08ylpm") (l "faster")))

(define-public crate-libfaster-sys-0.6.0 (c (n "libfaster-sys") (v "0.6.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w8d6j0nl4pyw9wfcv9544fc0h8nk36lpgmz21y5sbv5b5q6a71q") (l "faster")))

(define-public crate-libfaster-sys-0.7.0 (c (n "libfaster-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03qjpi1c227n2grnv4bj82gr4fx2xyh4r57klmjpw8zjli4v7ma0") (l "faster")))

(define-public crate-libfaster-sys-0.7.1 (c (n "libfaster-sys") (v "0.7.1") (d (list (d (n "bindgen") (r "^0.43.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qalvflrzjsalg3816kd5yqv3r8mibi81nd78kyhsi0l9fx7dind") (l "faster")))

(define-public crate-libfaster-sys-0.8.0 (c (n "libfaster-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09iv8j9r3bx53ns5qyw78zps981bkg88spxqr30n411fbja04z0j") (l "faster")))

(define-public crate-libfaster-sys-0.8.1 (c (n "libfaster-sys") (v "0.8.1") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04j69z2yn46p00jd5s596x0spsj8v191yxvymp5q52yrdpl2qgbw") (l "faster")))

(define-public crate-libfaster-sys-0.8.2 (c (n "libfaster-sys") (v "0.8.2") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dwzhgn2rpjg07zvzqkqwq4hbv0vz8h4hp16815p9jmw63g3ps9k") (y #t) (l "faster")))

(define-public crate-libfaster-sys-0.8.3 (c (n "libfaster-sys") (v "0.8.3") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03spmy1gb2sr9wz8z0nlz9yw9n985m0ykf0bkl5bnmwddn2l3jnz") (l "faster")))

(define-public crate-libfaster-sys-0.8.4 (c (n "libfaster-sys") (v "0.8.4") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1k9gr7aqyavqa35mza2x4dsacpfir069blrkhkdi2p69jmq8im01") (l "faster")))

(define-public crate-libfaster-sys-0.9.0 (c (n "libfaster-sys") (v "0.9.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "02hf1x9v1j1crimmrhxdwdsahyqc1rkxj7y0n36m4wcf72dss2db") (l "faster")))

(define-public crate-libfaster-sys-0.10.0 (c (n "libfaster-sys") (v "0.10.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sz9jykrz2sxa1l5j5ayz190kmyn1pmlmg8n8x9397i796sk3xs2") (l "faster")))

(define-public crate-libfaster-sys-0.11.0 (c (n "libfaster-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.50") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0521qc5yxsippc0fdq9cxyzg8i49z2ig0bc7cbj1hnnvrypcyihh") (l "faster")))

