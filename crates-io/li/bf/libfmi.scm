(define-module (crates-io li bf libfmi) #:use-module (crates-io))

(define-public crate-libfmi-2.0.4 (c (n "libfmi") (v "2.0.4") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "1x21zci7gcbkvwkgi5ygywllsv72vvgjldkkszd1i885mvz3dynm")))

(define-public crate-libfmi-2.1.4 (c (n "libfmi") (v "2.1.4") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "11x0wd28jnlvf7fb3gs6kxy5xav2fxcp3k36xrc1l6sbwrgnq4qv")))

(define-public crate-libfmi-2.2.4 (c (n "libfmi") (v "2.2.4") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libloading") (r "^0.8.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "17b7kk0d8r13v3q2drk5wh9bk5jpgwms9rzh3fvc06h9r9w78j30")))

