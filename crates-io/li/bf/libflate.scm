(define-module (crates-io li bf libflate) #:use-module (crates-io))

(define-public crate-libflate-0.1.0 (c (n "libflate") (v "0.1.0") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.14") (d #t) (k 2)))) (h "0p9i3pm5a8zqggy5dlljnm3pj6396jbrd9fski2zhfw5cdybj8r6")))

(define-public crate-libflate-0.1.1 (c (n "libflate") (v "0.1.1") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.14") (d #t) (k 2)))) (h "1xrcn3rjzw85vn61mghb0aydl0nq3rc72qbg5f4l5c5gjmq7f26l")))

(define-public crate-libflate-0.1.2 (c (n "libflate") (v "0.1.2") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.14") (d #t) (k 2)))) (h "0msh19y9bagsbn17cwlswzbrls6f51vd5b32a85b67v98xc1rsc6")))

(define-public crate-libflate-0.1.3 (c (n "libflate") (v "0.1.3") (d (list (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^2.19") (d #t) (k 2)))) (h "1nn4nw5kv4q99m0rvp2p587qbqqzg42arwgv5hdcpffjf5m68wwn")))

(define-public crate-libflate-0.1.4 (c (n "libflate") (v "0.1.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)))) (h "0hfswb3h1yhlrlkn2xbfjrn169r6vb8xazxvw6d80y6zzf4zab10")))

(define-public crate-libflate-0.1.5 (c (n "libflate") (v "0.1.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)))) (h "0xrs69wcp0kzy88aavgx42mlj80idwjhhqmxfimzj58cd3761am7")))

(define-public crate-libflate-0.1.6 (c (n "libflate") (v "0.1.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)))) (h "1h7nrwq12q6f7s6z21zlkb2dvag3czw09farsvxjiq0myngapl85")))

(define-public crate-libflate-0.1.7 (c (n "libflate") (v "0.1.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)))) (h "1xn11hkwfdbly17kr0r170gxs0fygsyrfbzjhd4f56xw5hsb27sr")))

(define-public crate-libflate-0.1.8 (c (n "libflate") (v "0.1.8") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)))) (h "1zh9daa028906pdk7xa61m0cfxbm4yjd5nxx9ydfw2arwri4gvm2")))

(define-public crate-libflate-0.1.9 (c (n "libflate") (v "0.1.9") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)))) (h "1jgyvh92mhm0xjmr5s65d69q4kkm1vzflik0vqzzc0pjw0cldyjr")))

(define-public crate-libflate-0.1.10 (c (n "libflate") (v "0.1.10") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)))) (h "0aaagr0r7lynjjcfjq0p0y5fmc4v6cgsh8hdlypqa6cqpml5r7mv")))

(define-public crate-libflate-0.1.11 (c (n "libflate") (v "0.1.11") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)))) (h "0c4q6bm00jnypr2sh7f3gdw92ckrkvzk0q9nllqjv08007n09am2")))

(define-public crate-libflate-0.1.12 (c (n "libflate") (v "0.1.12") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "0rqykxhh6j5adx67c8qk0w516zqa5kl5nz75jvlq2sa9zbdbqimf")))

(define-public crate-libflate-0.1.13 (c (n "libflate") (v "0.1.13") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "1yvdzrrhk5pwhna8y1p5mxl76ndxjrbbmhf2y8d6whjaxbrfh2kf")))

(define-public crate-libflate-0.1.14 (c (n "libflate") (v "0.1.14") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "13jfnynw6vn8ir1palsgv7cpzx430qbyjl7fj7mcfs488639nhhs") (y #t)))

(define-public crate-libflate-0.1.15 (c (n "libflate") (v "0.1.15") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "0yj8035gg84868b2wpmacl2p1qzc43fwifgzcas1bnaf1q2422pp") (y #t)))

(define-public crate-libflate-0.1.16 (c (n "libflate") (v "0.1.16") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "165bv8abm9jgjnp8c889xdnhds5d1vmd14znaswikb2vzxx4qjvx") (y #t)))

(define-public crate-libflate-0.1.17 (c (n "libflate") (v "0.1.17") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "18ynm96p67labpbrsdd2f8sy00c1hc2slyff1a3msl7rm45jg1m6") (y #t)))

(define-public crate-libflate-0.1.18 (c (n "libflate") (v "0.1.18") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc") (r "^1") (d #t) (k 0)))) (h "0dp9dsq0nas53s6n1w6150ps62szk9wdan9mmvbqwhwzcv38y4r1") (y #t)))

(define-public crate-libflate-0.1.19 (c (n "libflate") (v "0.1.19") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1") (d #t) (k 0)))) (h "0yxmfj6l4v7ixsgljsj76iv1cf7nxrsysd9wacxhswr3dxysrwxz") (y #t)))

(define-public crate-libflate-0.1.20 (c (n "libflate") (v "0.1.20") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1") (d #t) (k 0)))) (h "18ccfpcagkqspbrricx5d88w2cc3i26n7mw9aqy28w18qpwxvlal") (y #t)))

(define-public crate-libflate-0.1.21 (c (n "libflate") (v "0.1.21") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1") (d #t) (k 0)))) (h "082dj5wb4my4asrzz7g82vy33p45af85s8i49pa5hf9ci8zahikk") (y #t)))

(define-public crate-libflate-0.1.22 (c (n "libflate") (v "0.1.22") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1") (d #t) (k 0)))) (h "0sknagxm084dsd6bkiga4cdpswdlgxgz7pfq20mfh31dnap888y5") (y #t)))

(define-public crate-libflate-0.1.23 (c (n "libflate") (v "0.1.23") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1") (d #t) (k 0)))) (h "05gsbzpc62p25zhsswk1q1br2dj5prvyhgn4v430wvvb36h2m4bn") (y #t)))

(define-public crate-libflate-0.1.24 (c (n "libflate") (v "0.1.24") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1") (d #t) (k 0)))) (h "0847gm3zc9fp2d48wwfiyp956p24k5nh1h7m83943il0szj1xlzx") (y #t)))

(define-public crate-libflate-0.1.25 (c (n "libflate") (v "0.1.25") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1") (d #t) (k 0)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "0a85b3yr2x00vm98vwcgxz37f7dmhyvgh5pr0rr39ahc9dpziilh")))

(define-public crate-libflate-0.1.26 (c (n "libflate") (v "0.1.26") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1") (d #t) (k 0)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "04ilp73hvx6mgya0cy2rk2hj37vrqk6nll1dqp59vdr547v7rja5")))

(define-public crate-libflate-0.1.27 (c (n "libflate") (v "0.1.27") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1") (d #t) (k 0)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)) (d (n "take_mut") (r "^0.2.2") (d #t) (k 0)))) (h "1p8z839c5lpl0g01mf8iglys9lgcjxw6xjw56crhwp8z7gs5s4yr")))

(define-public crate-libflate-1.0.0 (c (n "libflate") (v "1.0.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1") (d #t) (k 0)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)))) (h "0r2xscsw86l14n9i9f0v4v57xgl5mqcy7b1pdm24chx9cywydyx1")))

(define-public crate-libflate-1.0.1 (c (n "libflate") (v "1.0.1") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1.1.1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1") (d #t) (k 0)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)))) (h "09cdksx29ls5pzz7r949jcpb1s37cqw0d2b5ad77z7caj32lwkvq")))

(define-public crate-libflate-1.0.2 (c (n "libflate") (v "1.0.2") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1.1.1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1") (d #t) (k 0)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)))) (h "0jarv5ildsm0ci4prd4gz7fqypifhp9xk34z9w49rchx7q1ckfp9")))

(define-public crate-libflate-1.0.3 (c (n "libflate") (v "1.0.3") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1.1.1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1") (d #t) (k 0)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)))) (h "053rw5b82z6x41h9zkcc31r1jpymhpzyg8sdjxjn6iq6bs3yg79q")))

(define-public crate-libflate-1.0.4 (c (n "libflate") (v "1.0.4") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1.1.1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1") (d #t) (k 0)) (d (n "rle-decode-fast") (r "^1.0.0") (d #t) (k 0)))) (h "05i91rc08x9slyznfkz2mwypgwhky2kgb5080pvflqd7175f52hm")))

(define-public crate-libflate-1.1.0 (c (n "libflate") (v "1.1.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1.1.1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1.1") (d #t) (k 0)))) (h "1qxzc0ms58vbqv8q6gy56gpmd2vrhavj2lb405zhys1zdgiym1vd")))

(define-public crate-libflate-1.1.1 (c (n "libflate") (v "1.1.1") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1.1.1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1.1") (d #t) (k 0)))) (h "07hlzg1zly4dw9s39fv2ik6nfcjpjgx82b5kkf3bafdvdvvlldhn")))

(define-public crate-libflate-1.1.2 (c (n "libflate") (v "1.1.2") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1.1.1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1.1") (d #t) (k 0)))) (h "0pvnyk9xsjd6df9d13zagsfy5d6zicrzwnf4il5kxb0p8x9pxmfj")))

(define-public crate-libflate-1.2.0 (c (n "libflate") (v "1.2.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "crc32fast") (r "^1.1.1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1.1") (d #t) (k 0)))) (h "14rh6zpc7jihszkpw9dy54zshblbxwlzydf6x70cy6z1pjr5lq05")))

(define-public crate-libflate-1.3.0 (c (n "libflate") (v "1.3.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "core2") (r "^0.4") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "crc32fast") (r "^1.1.1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1.1") (k 0)))) (h "1c2h7sha2328vjq27qvqzqh9bx4bprgnm20k6x05nkdxj7vjp0lp") (f (quote (("no_std" "libflate_lz77/no_std" "core2"))))))

(define-public crate-libflate-1.4.0 (c (n "libflate") (v "1.4.0") (d (list (d (n "adler32") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "core2") (r "^0.4") (f (quote ("alloc"))) (o #t) (k 0)) (d (n "crc32fast") (r "^1.1.1") (d #t) (k 0)) (d (n "libflate_lz77") (r "^1.1") (k 0)))) (h "063xw2z477h3vh7j32y0f54a6nbndd7yf7rr5wpsvfw5nrqsxx2z") (f (quote (("no_std" "libflate_lz77/no_std" "core2"))))))

(define-public crate-libflate-2.0.0 (c (n "libflate") (v "2.0.0") (d (list (d (n "adler32") (r "^1") (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "core2") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "crc32fast") (r "^1.1.1") (k 0)) (d (n "dary_heap") (r "^0.3.5") (d #t) (k 0)) (d (n "libflate_lz77") (r "^2.0.0") (k 0)))) (h "1gxjqgbzzn6sdalrd2cfn7zb14c75if3dx3nzz3sz58pmra5czcz") (f (quote (("std" "libflate_lz77/std" "core2/std") ("default" "std"))))))

(define-public crate-libflate-2.1.0 (c (n "libflate") (v "2.1.0") (d (list (d (n "adler32") (r "^1") (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "core2") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "crc32fast") (r "^1.1.1") (k 0)) (d (n "dary_heap") (r "^0.3.5") (d #t) (k 0)) (d (n "libflate_lz77") (r "^2.1.0") (k 0)))) (h "07mj9z89vbhq837q58m4v2nblgsmrn6vrp8w1j8g0kpa2kfdzna5") (f (quote (("std" "libflate_lz77/std" "core2/std") ("default" "std"))))))

