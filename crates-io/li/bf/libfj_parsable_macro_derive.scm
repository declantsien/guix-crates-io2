(define-module (crates-io li bf libfj_parsable_macro_derive) #:use-module (crates-io))

(define-public crate-libfj_parsable_macro_derive-0.5.1 (c (n "libfj_parsable_macro_derive") (v "0.5.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1qls2lpmrlwnlw30r5bdcfyvsrpnw517341splkfzgpd5myns32c")))

(define-public crate-libfj_parsable_macro_derive-0.5.2 (c (n "libfj_parsable_macro_derive") (v "0.5.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0axl0qllll4g2rmlja9kh7s9sfhi0ah3nrnspmxsddhr7v88z8hf")))

(define-public crate-libfj_parsable_macro_derive-0.5.3 (c (n "libfj_parsable_macro_derive") (v "0.5.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05m4bp7ka2bwdhy6ni28lp67qz5g3f0f57dmhsixrrfgs7pyj4jq")))

