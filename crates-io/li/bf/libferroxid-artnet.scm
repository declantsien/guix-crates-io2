(define-module (crates-io li bf libferroxid-artnet) #:use-module (crates-io))

(define-public crate-libferroxid-artnet-0.1.0 (c (n "libferroxid-artnet") (v "0.1.0") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.138") (d #t) (k 0)))) (h "0dzixij875wdq2xr1f6kiqvx2p4g3nsjkcxxb1hgj6qvqgm2m4hr")))

(define-public crate-libferroxid-artnet-0.1.1 (c (n "libferroxid-artnet") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "libferroxid") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.138") (d #t) (k 0)))) (h "1g9sv4a6kfw49ddgly94q0y95m7w2ky13alj26v6xnwjsg77c65c")))

(define-public crate-libferroxid-artnet-0.1.2 (c (n "libferroxid-artnet") (v "0.1.2") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "libferroxid") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.138") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.138") (d #t) (k 0)))) (h "1qyz1hnj9mhgl73dcmkzpgh38zdagvq423qknlpgxj3bkn39djnx")))

