(define-module (crates-io li bf libflo_action_mapper) #:use-module (crates-io))

(define-public crate-libflo_action_mapper-0.1.0 (c (n "libflo_action_mapper") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libflo_module") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1mp6dab2xq4ydif30j38h8y9qjz97n686sf6gvnjx0q03p67ybm3") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

