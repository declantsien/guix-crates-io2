(define-module (crates-io li bf libflo_action_util) #:use-module (crates-io))

(define-public crate-libflo_action_util-0.1.0 (c (n "libflo_action_util") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libflo") (r "^0.1") (d #t) (k 0)) (d (n "libflo_module") (r "^0.1") (d #t) (k 0)) (d (n "number_or_string") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1zwgwaa21db77fsdgls7w822fihb0w8wd057wnc95ynykz5l6s61") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

