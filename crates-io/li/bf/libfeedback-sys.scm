(define-module (crates-io li bf libfeedback-sys) #:use-module (crates-io))

(define-public crate-libfeedback-sys-0.0.1 (c (n "libfeedback-sys") (v "0.0.1") (d (list (d (n "gio-sys") (r "^0.19") (d #t) (k 0)) (d (n "glib-sys") (r "^0.19") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "197vbdmskkmakvhbv0n5mmqc57dn9gnrbgywc7czkpmj9z15bbp6") (f (quote (("v0_3"))))))

