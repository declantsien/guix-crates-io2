(define-module (crates-io li bf libflo_cmdline_host) #:use-module (crates-io))

(define-public crate-libflo_cmdline_host-0.1.1 (c (n "libflo_cmdline_host") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libflo") (r "^0.1") (d #t) (k 0)) (d (n "libflo_dynamic_event") (r "^0.1") (d #t) (k 0)) (d (n "libflo_event") (r "^0.1") (d #t) (k 0)) (d (n "libflo_func") (r "^0.1") (d #t) (k 0)) (d (n "libflo_module") (r "^0.1") (d #t) (k 0)))) (h "0x8g93jrn43p1qxi7822l51svgd3v8rw0cj682pjkbqxdmhx4xjf")))

(define-public crate-libflo_cmdline_host-0.1.2 (c (n "libflo_cmdline_host") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "libflo") (r "^0.1") (d #t) (k 0)) (d (n "libflo_dynamic_event") (r "^0.1") (d #t) (k 0)) (d (n "libflo_event") (r "^0.1") (d #t) (k 0)) (d (n "libflo_func") (r "^0.1") (d #t) (k 0)) (d (n "libflo_module") (r "^0.1") (d #t) (k 0)))) (h "1rdlw0i2w7752ri9nbbb3yjhh2gqxkbdzf7wprw1pg8w5qkdg3ag")))

