(define-module (crates-io li bf libfirm-sys) #:use-module (crates-io))

(define-public crate-libfirm-sys-0.1.0 (c (n "libfirm-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "0sk00591vxfp1cm183c54981balnkdx86kk8mdzqrjbcp5al9bql") (y #t) (l "libfirm")))

(define-public crate-libfirm-sys-0.2.0 (c (n "libfirm-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "158gfr8i2kbp7pyzh6wj1hs4nqyhw6cdc3lx8bbnwgdz1nbcqa87") (l "libfirm")))

