(define-module (crates-io li bf libflo_module) #:use-module (crates-io))

(define-public crate-libflo_module-0.1.0 (c (n "libflo_module") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0dgc2p6fffrw4ngn6ggv2v2j4d6rizklwwx0zsfmjxflcnjhi78c") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-libflo_module-0.1.1 (c (n "libflo_module") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "1h0xq8qgzl43mc50fff0jngcqvrshdlnl9amyvpwhqxzgs34ql8g") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

(define-public crate-libflo_module-0.1.2 (c (n "libflo_module") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^0.8") (d #t) (k 0)) (d (n "serde_codegen") (r "^0.8") (o #t) (d #t) (k 1)) (d (n "serde_json") (r "^0.8") (d #t) (k 0)) (d (n "serde_macros") (r "^0.8") (o #t) (d #t) (k 0)))) (h "0rkv5z2srvwlk025xi5hhlix2iqhk3dg6ijfwzg0wnlwfd7q36xf") (f (quote (("unstable" "serde_macros") ("default" "serde_codegen"))))))

