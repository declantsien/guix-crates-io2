(define-module (crates-io li bf libfa-sys) #:use-module (crates-io))

(define-public crate-libfa-sys-0.1.0 (c (n "libfa-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "*") (d #t) (k 1)))) (h "0a18416aki4y3far7yjnh8pbbqbx0h8hby8dnbaq13130qcdjsyr")))

(define-public crate-libfa-sys-0.1.1 (c (n "libfa-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1c1vqslcc52dm64zr164q1dmbgz72sil92avm2m2vsza1lwcmi9l")))

(define-public crate-libfa-sys-0.1.2 (c (n "libfa-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1wffdmg5acpdli9kn3sypcm1g4sagm17i7anlh870cffxg6053yx")))

(define-public crate-libfa-sys-0.1.3 (c (n "libfa-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1c4va1j0il3cwa6r6l25nmbqj1cwiip64n33slxvbqglacm60zwv")))

