(define-module (crates-io li bf libfive) #:use-module (crates-io))

(define-public crate-libfive-0.1.0 (c (n "libfive") (v "0.1.0") (d (list (d (n "ahash") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "libfive-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "028khsq1ri0l37firgbgbhl3v1w02l8hi4fb1sk1qknz55y523zg") (f (quote (("stdlib") ("packed_opcodes" "libfive-sys/packed_opcodes") ("default" "ahash" "stdlib")))) (y #t)))

(define-public crate-libfive-0.1.1 (c (n "libfive") (v "0.1.1") (d (list (d (n "ahash") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "libfive-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "0kaipnk8c9m1j4a7wcqcyqcqk89hgmwmlyzsbv47wdscf7r6x0h3") (f (quote (("stdlib") ("packed_opcodes" "libfive-sys/packed_opcodes") ("default" "ahash" "stdlib")))) (y #t)))

(define-public crate-libfive-0.1.2 (c (n "libfive") (v "0.1.2") (d (list (d (n "ahash") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "libfive-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "1aydna017sypid90svig526q8l67sccbj438bg89v8fz5asqf4yv") (f (quote (("stdlib") ("packed_opcodes" "libfive-sys/packed_opcodes") ("default" "ahash" "stdlib"))))))

(define-public crate-libfive-0.1.3 (c (n "libfive") (v "0.1.3") (d (list (d (n "ahash") (r "^0.7.4") (o #t) (d #t) (k 0)) (d (n "libfive-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.1") (d #t) (k 0)))) (h "16pdhif5s2v5javh7m6ngqf93xl2qp6ddzv58wxb1brn3wpls8wr") (f (quote (("stdlib") ("packed_opcodes" "libfive-sys/packed_opcodes") ("default" "ahash" "stdlib"))))))

(define-public crate-libfive-0.2.0 (c (n "libfive") (v "0.2.0") (d (list (d (n "ahash") (r "^0.8.7") (o #t) (d #t) (k 0)) (d (n "libfive-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0xnilchbzc6md5h66pffbfyjqs5i8sx6nsjaafp2i1v3sjqbyxny") (f (quote (("stdlib") ("packed_opcodes" "libfive-sys/packed_opcodes") ("default" "ahash" "stdlib")))) (y #t)))

(define-public crate-libfive-0.3.0 (c (n "libfive") (v "0.3.0") (d (list (d (n "ahash") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "libfive-sys") (r "^0.3") (d #t) (k 0)))) (h "13n5331ik9fqq2dlg53yy7bpsfl8n6f9x1a65wrfzl8vrychm7yd") (f (quote (("stdlib") ("packed_opcodes" "libfive-sys/packed_opcodes") ("default" "ahash" "stdlib")))) (y #t)))

(define-public crate-libfive-0.3.1 (c (n "libfive") (v "0.3.1") (d (list (d (n "ahash") (r "^0.8") (o #t) (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "libfive-sys") (r "^0.3") (d #t) (k 0)))) (h "00ncwy4awxx2gy2vqz4db1bhl9qnqab3d35m3r37axz9xc0avin8") (f (quote (("stdlib") ("packed_opcodes" "libfive-sys/packed_opcodes") ("default" "ahash" "stdlib"))))))

