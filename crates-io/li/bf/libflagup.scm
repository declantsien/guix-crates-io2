(define-module (crates-io li bf libflagup) #:use-module (crates-io))

(define-public crate-libflagup-0.0.4 (c (n "libflagup") (v "0.0.4") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zvb93248bqda5c8hkja5zbrs9i7ch3gbdg42w6199wn5ykv4ijy")))

(define-public crate-libflagup-0.0.5 (c (n "libflagup") (v "0.0.5") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0gjqh4a0j3bz0jw6fa3jzanjm9s1ijd39h8laffhhjx7i9vzm6q1")))

(define-public crate-libflagup-0.0.6 (c (n "libflagup") (v "0.0.6") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "19g32wxn8b9ga68qrqyg7q0b9iy1i2j7p13qvgkrvgk2g1mw58nd")))

(define-public crate-libflagup-0.0.8 (c (n "libflagup") (v "0.0.8") (d (list (d (n "console") (r "^0.15.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xrw1999qxq02cfcl3aq4krapq9kc6nr7wnghfcvvdm4b23589c6")))

