(define-module (crates-io li bf libflv) #:use-module (crates-io))

(define-public crate-libflv-0.0.1 (c (n "libflv") (v "0.0.1") (d (list (d (n "byteorder") (r "^1.4.2") (d #t) (k 0)) (d (n "bytes") (r "^1.0.0") (d #t) (k 0)) (d (n "failure") (r "^0.1.1") (d #t) (k 0)) (d (n "hmac") (r "^0.6.2") (d #t) (k 0)) (d (n "networkio") (r "^0.1.23") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "sha2") (r "^0.7.1") (d #t) (k 0)) (d (n "tokio") (r "^1.4.0") (f (quote ("full"))) (k 0)) (d (n "tokio-util") (r "^0.6.5") (f (quote ("codec"))) (d #t) (k 0)))) (h "1dn3359pwqmjlwa3av3qw9id5v54pi52pz7nmp2k8ps9cajgn9k6")))

