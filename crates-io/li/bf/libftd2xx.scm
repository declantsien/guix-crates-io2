(define-module (crates-io li bf libftd2xx) #:use-module (crates-io))

(define-public crate-libftd2xx-0.1.0 (c (n "libftd2xx") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.71") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "034dlnv22xvl25d0w65y0d4q53yk7fh7l99vzxrxjf9n4mbkljmq")))

(define-public crate-libftd2xx-0.1.1 (c (n "libftd2xx") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0px7gh9bp4bc7bk8wgg0l1y8fsd4d16rkjm8qaq5cha456hcbv2c")))

(define-public crate-libftd2xx-0.2.0 (c (n "libftd2xx") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1k4yq78ql63qk88v8ls4k2adddf3nbx562yjsgh1gfy4rdhqqfgr")))

(define-public crate-libftd2xx-0.3.0 (c (n "libftd2xx") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0chyh1byr4xwkadipgw55f7ks4gx2fynipam3xfgbglax2lw4kly")))

(define-public crate-libftd2xx-0.3.1 (c (n "libftd2xx") (v "0.3.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "12q045lf73b725zmlxqgy3x2zfwkpxd7pwnp3kisa9fbrq844d05")))

(define-public crate-libftd2xx-0.3.2 (c (n "libftd2xx") (v "0.3.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0vjcgsmadwm1y4c4ckgqvbaqjxv70a2mi92w7cqkzqn93czgk9jv")))

(define-public crate-libftd2xx-0.4.0 (c (n "libftd2xx") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "^0.1") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0f01yrgqp832h4gn45m5n53ki31crb1z61qa03wv0wxzypalr3pm")))

(define-public crate-libftd2xx-0.5.0 (c (n "libftd2xx") (v "0.5.0") (d (list (d (n "libc") (r "~0.2.72") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "~0.2.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "00bn9yddlkn0nb0wx866af6wj8h165agnql9b248s2wf62qd327p")))

(define-public crate-libftd2xx-0.5.1 (c (n "libftd2xx") (v "0.5.1") (d (list (d (n "libc") (r "~0.2.72") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "~0.2.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0ly0529lghyi08xy1yk7sg1pbljzfps6izv6bb8inskrh008sqk0")))

(define-public crate-libftd2xx-0.6.0 (c (n "libftd2xx") (v "0.6.0") (d (list (d (n "libc") (r "~0.2.72") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "~0.2.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0wqh3837qgylqmq7d07qpd4dxgswqsj92v7ixjy1igykhx5f39y1")))

(define-public crate-libftd2xx-0.7.0 (c (n "libftd2xx") (v "0.7.0") (d (list (d (n "libc") (r "~0.2.73") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "~0.2.0") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "068lfsj971yn22kcdclfh4fkg1gg0paz0djf85kk7zx1gvr9kz23")))

(define-public crate-libftd2xx-0.7.1 (c (n "libftd2xx") (v "0.7.1") (d (list (d (n "libc") (r "~0.2.73") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "~0.2.1") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1652mncp5kmx0pai5i4g31clz3aa3qamw679xfx3abkbmc3wldif")))

(define-public crate-libftd2xx-0.8.0 (c (n "libftd2xx") (v "0.8.0") (d (list (d (n "libftd2xx-ffi") (r "~0.2.2") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "06wzswhmycil0780f2b1g14m76mbjydk6axyps4ga5d0mcbnjb2n")))

(define-public crate-libftd2xx-0.8.1 (c (n "libftd2xx") (v "0.8.1") (d (list (d (n "libftd2xx-ffi") (r "~0.2.2") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1sb64h0nmd97vv53pxnvarqsz5cfznkcwd16c8crqyvj2pwqlkwd")))

(define-public crate-libftd2xx-0.8.2 (c (n "libftd2xx") (v "0.8.2") (d (list (d (n "libftd2xx-ffi") (r "~0.2.2") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1xsizp3p34xjqayrszxis9d85607m2bhhawabipz7h6bfv2b5ia5")))

(define-public crate-libftd2xx-0.9.0 (c (n "libftd2xx") (v "0.9.0") (d (list (d (n "libftd2xx-ffi") (r "~0.2.2") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1qa7zl62whwg7x6nz1jax4arh6b1970rfgd91ahrcd17hdv67f56")))

(define-public crate-libftd2xx-0.9.1 (c (n "libftd2xx") (v "0.9.1") (d (list (d (n "libftd2xx-ffi") (r "~0.2.2") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1d0xxqa4j63k25bw6xdmayhm0kaqw81n1bykk31pzlrnkppbwsvv")))

(define-public crate-libftd2xx-0.10.0 (c (n "libftd2xx") (v "0.10.0") (d (list (d (n "libftd2xx-ffi") (r "~0.2.2") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0nyzg29gmpgc81169ads99vjwrgx6xlcd3x4naqsiv6nv13gd2c2")))

(define-public crate-libftd2xx-0.11.0 (c (n "libftd2xx") (v "0.11.0") (d (list (d (n "libftd2xx-ffi") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "04qgijysbnrsp5dfs54w5fds2d9xw48dn0i7s4m58x162djl7gs8")))

(define-public crate-libftd2xx-0.12.0 (c (n "libftd2xx") (v "0.12.0") (d (list (d (n "libftd2xx-ffi") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0x5rii0lb36k4r77iy5lyp5sfrn4brzysi11s0fnsh4jr85dgd1y")))

(define-public crate-libftd2xx-0.12.1 (c (n "libftd2xx") (v "0.12.1") (d (list (d (n "libftd2xx-ffi") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0w625y0d977s3vn4hfcr85bvj1vcp83dya9pa817wi5h1av0y01v")))

(define-public crate-libftd2xx-0.13.0 (c (n "libftd2xx") (v "0.13.0") (d (list (d (n "libftd2xx-ffi") (r "~0.2.2") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1h5v4a419rh192hhnmfnzizg2pc0s6gp41fq396fgyvmmfx2nyc5")))

(define-public crate-libftd2xx-0.14.0 (c (n "libftd2xx") (v "0.14.0") (d (list (d (n "libftd2xx-ffi") (r "~0.3.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0rx5cmx8rzljvrxygf7lyp30dzxaj3d9h7zk3aqy5nvcym76qjcf")))

(define-public crate-libftd2xx-0.15.0 (c (n "libftd2xx") (v "0.15.0") (d (list (d (n "libftd2xx-ffi") (r "~0.3.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1rbfr2jlqwc396r3nmz5savs5spxk00gigc1aki9w5ywamgs09m7")))

(define-public crate-libftd2xx-0.16.0 (c (n "libftd2xx") (v "0.16.0") (d (list (d (n "libftd2xx-ffi") (r "~0.4.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0c0f8qj6byfb9yl55gajzvixsgn1v3w6103cs6c73r976lpfc1cp")))

(define-public crate-libftd2xx-0.16.1 (c (n "libftd2xx") (v "0.16.1") (d (list (d (n "libftd2xx-ffi") (r "~0.4.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1wyfccp2i5dba3kifx00ny9zc403cnbak17ppi1gms0pnx40ih7b")))

(define-public crate-libftd2xx-0.17.0 (c (n "libftd2xx") (v "0.17.0") (d (list (d (n "libftd2xx-ffi") (r "~0.4.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0b4bzhv27fazlq0a9m0wpm5yzjpy77p2pid74qhm6cskrpnl2azf")))

(define-public crate-libftd2xx-0.18.0 (c (n "libftd2xx") (v "0.18.0") (d (list (d (n "libftd2xx-ffi") (r "~0.4.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1f763x9izpdm1kk5175yalg1kmaakggx2pq0lspk0zdr5xb9iwjs")))

(define-public crate-libftd2xx-0.19.0 (c (n "libftd2xx") (v "0.19.0") (d (list (d (n "libftd2xx-ffi") (r "~0.4.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "02iy8c1fpliqi9ikank1ca9b6hx219czvsslmadl1ccxvzipakfn")))

(define-public crate-libftd2xx-0.20.0 (c (n "libftd2xx") (v "0.20.0") (d (list (d (n "libftd2xx-ffi") (r "~0.4.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0rd2z9z8b462fwhx0k0rbx228nazrgyrxmjksidanv9az4v8fsg4")))

(define-public crate-libftd2xx-0.21.0 (c (n "libftd2xx") (v "0.21.0") (d (list (d (n "libftd2xx-ffi") (r "~0.4.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "05w85bwvxa8g7i5vcfz314qv5gaa89f6s08wjq6zqqwlhj664rlr")))

(define-public crate-libftd2xx-0.21.1 (c (n "libftd2xx") (v "0.21.1") (d (list (d (n "libftd2xx-ffi") (r "~0.4.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "073rppb3hk1a59jp94277w1znmyii9njml0knm5k2q9xnbyikpm4")))

(define-public crate-libftd2xx-0.22.0 (c (n "libftd2xx") (v "0.22.0") (d (list (d (n "libftd2xx-ffi") (r "~0.4.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1pc1rpa03shgab6gc0ywazpp8dq2xkr8igwlsy8l15jkxa0gjxac")))

(define-public crate-libftd2xx-0.23.0 (c (n "libftd2xx") (v "0.23.0") (d (list (d (n "libftd2xx-ffi") (r "~0.4.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "155j90d9flclj1gi6iwhgipbfywy3i33bbc2zdfndwzcixmp5iqc")))

(define-public crate-libftd2xx-0.24.0 (c (n "libftd2xx") (v "0.24.0") (d (list (d (n "libftd2xx-ffi") (r "~0.5.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "~1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "03d2vjy9xx3zxmbwvzc1i6b3bchdh5669p0a61sqy42hliww1k92")))

(define-public crate-libftd2xx-0.24.1 (c (n "libftd2xx") (v "0.24.1") (d (list (d (n "libftd2xx-ffi") (r "~0.5.1") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1ppif04yzzcwisnj6v52rnnhhgxzq32j08k85k7xjddix0pq5pdr")))

(define-public crate-libftd2xx-0.25.0 (c (n "libftd2xx") (v "0.25.0") (d (list (d (n "libftd2xx-ffi") (r "~0.6.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0w7k9lfpgz6fscz98jcfsch3hv3hj6r1l5bh6fsyxf6cyaczvlm2")))

(define-public crate-libftd2xx-0.25.1 (c (n "libftd2xx") (v "0.25.1") (d (list (d (n "libftd2xx-ffi") (r "~0.6.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "144zd38wj2hlb6ar5vm52h8s1569wbpq76xvc175fnlswgxb7pgh")))

(define-public crate-libftd2xx-0.26.0 (c (n "libftd2xx") (v "0.26.0") (d (list (d (n "libftd2xx-ffi") (r "~0.6.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "11jqg8b0fa4hy642ns2ij9dhh9kfgakzmwca1fjknxrvqj2qxfnh")))

(define-public crate-libftd2xx-0.27.0 (c (n "libftd2xx") (v "0.27.0") (d (list (d (n "libftd2xx-ffi") (r "~0.7.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "1bg19ncfg47drbcy92z992cmmx8xgk7xrz4p7a184l9gn21wyd4a") (f (quote (("static" "libftd2xx-ffi/static"))))))

(define-public crate-libftd2xx-0.28.0 (c (n "libftd2xx") (v "0.28.0") (d (list (d (n "libftd2xx-ffi") (r "~0.7.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "02hf0k6n3rgmdmq80pywzy20nra5nlwgmq6ff8jj3hqqbx97dxwd") (f (quote (("static" "libftd2xx-ffi/static"))))))

(define-public crate-libftd2xx-0.29.0 (c (n "libftd2xx") (v "0.29.0") (d (list (d (n "libftd2xx-ffi") (r "~0.7.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "1pcfc1fbzgz69ri8q9nh2mg3bm3gqd70vdcm50l2f0ph5c6i9ns7") (f (quote (("static" "libftd2xx-ffi/static"))))))

(define-public crate-libftd2xx-0.30.0 (c (n "libftd2xx") (v "0.30.0") (d (list (d (n "libftd2xx-ffi") (r "~0.8.0") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "02jmp8ja9j1df9q7adn93waqpd4vylivkzdqrj0wpwsgji3mazih") (f (quote (("static" "libftd2xx-ffi/static"))))))

(define-public crate-libftd2xx-0.31.0 (c (n "libftd2xx") (v "0.31.0") (d (list (d (n "libftd2xx-ffi") (r "~0.8.3") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "12p6hlwwvhmsaf9yglmvs9lp2bf8l7yybp8q77xk4xcil4xbxcyn") (f (quote (("static" "libftd2xx-ffi/static"))))))

(define-public crate-libftd2xx-0.32.0 (c (n "libftd2xx") (v "0.32.0") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "~0.8.3") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "18p4b92pi4jj9nwsj3ybyaikm3lx4s18x42j3gghc008l9lqqkkg") (f (quote (("static" "libftd2xx-ffi/static"))))))

(define-public crate-libftd2xx-0.32.1 (c (n "libftd2xx") (v "0.32.1") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "~0.8.3") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0mr1x2chdlvgld1mq0mqknqf6i7qv5hyxqkwkcyi5jzr3wjwyv5x") (f (quote (("static" "libftd2xx-ffi/static"))))))

(define-public crate-libftd2xx-0.32.2 (c (n "libftd2xx") (v "0.32.2") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "~0.8.5") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "05gaq18swd3ndws9nhk096lqizmjfj8fhsfg2k38hqh9jc7mmgys") (f (quote (("static" "libftd2xx-ffi/static"))))))

(define-public crate-libftd2xx-0.32.3 (c (n "libftd2xx") (v "0.32.3") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "~0.8.5") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "1k1a2v26ic38p2mbg394xwsv253x2bl34b81dak305m6zpzcx37b") (f (quote (("static" "libftd2xx-ffi/static"))))))

(define-public crate-libftd2xx-0.32.4 (c (n "libftd2xx") (v "0.32.4") (d (list (d (n "ftdi-mpsse") (r "^0.1") (d #t) (k 0)) (d (n "libftd2xx-ffi") (r "~0.8.5") (d #t) (k 0)) (d (n "log") (r "~0.4.11") (d #t) (k 0)) (d (n "paste") (r "^1") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "00mkjnr8zqa4dkv6icjkrgpwqyshd4vygn5s4smmz1sjpcf12ag8") (f (quote (("static" "libftd2xx-ffi/static"))))))

