(define-module (crates-io li bf libfetch) #:use-module (crates-io))

(define-public crate-libfetch-0.1.0 (c (n "libfetch") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.124") (d #t) (k 0)) (d (n "sysctl") (r "^0.4.3") (d #t) (k 0)))) (h "02gs9qdiwbjk1l4j9dppns5v8ypmpyknydn2bz4hvj946nay16d0")))

