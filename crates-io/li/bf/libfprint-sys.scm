(define-module (crates-io li bf libfprint-sys) #:use-module (crates-io))

(define-public crate-libfprint-sys-0.1.0 (c (n "libfprint-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1w43gask01g8l5lagchw8j28bx2745895cihm5fikjywajxmlhky")))

(define-public crate-libfprint-sys-0.1.1 (c (n "libfprint-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0zg2wqjh6s2ag9q70z8zyavs6bz5fflcwx29590002yrs61jpjn0") (y #t) (l "libfprint-2")))

(define-public crate-libfprint-sys-0.1.2 (c (n "libfprint-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1l8knxsqsw1v64z7gkf25hsk22yg6s38xn0h6vcisxnm4f7pwbmq") (y #t) (l "libfprint-2")))

(define-public crate-libfprint-sys-0.1.3 (c (n "libfprint-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1084a19760v6zjwjmd3lpd1g1n7xgw98hn8dnbdj7k5ri1w2ma0v") (y #t) (l "libfprint-2")))

(define-public crate-libfprint-sys-0.1.4 (c (n "libfprint-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0yyawzyxyk61ml6knjf1l3h8nnqiqrvvwf48s8in85dpk7c3gj5m") (y #t) (l "libfprint-2")))

(define-public crate-libfprint-sys-0.1.5 (c (n "libfprint-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1mjkz9mi8w9gfakdx0p74r8915i14r14lr4bj5nfmnjgkjk3aaw6") (y #t) (l "libfprint-2")))

(define-public crate-libfprint-sys-0.1.6 (c (n "libfprint-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "0is056d5l1n4b4iz3l3bq65bzzwf0b8ixxq1qrydr17bmycqzwz0") (l "libfprint-2")))

(define-public crate-libfprint-sys-0.1.7 (c (n "libfprint-sys") (v "0.1.7") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1rq945k4szv8ns2rr0swih6w79xy6flnypy3l48w2li8z4np3cqd") (l "libfprint-2")))

(define-public crate-libfprint-sys-0.1.8 (c (n "libfprint-sys") (v "0.1.8") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "13df6k3aw19vkh9758s9ycacf8vpvn8qx0rzdqn1rmgc259ilwiv") (l "libfprint-2")))

