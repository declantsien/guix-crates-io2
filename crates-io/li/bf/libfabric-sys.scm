(define-module (crates-io li bf libfabric-sys) #:use-module (crates-io))

(define-public crate-libfabric-sys-0.0.1 (c (n "libfabric-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rust_c") (r "^0.1.1") (f (quote ("macro"))) (d #t) (k 0)) (d (n "rust_c") (r "^0.1.1") (f (quote ("build"))) (d #t) (k 1)))) (h "1in07k8zilx96y9vx2hig5c3z3rlzw7vjzmq1hyfnw7h4mkdzdh7")))

(define-public crate-libfabric-sys-0.0.2 (c (n "libfabric-sys") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x49rnn6qmj844yjlq8ra252bhw5vhqsb6rxycihw1y36a130vxs")))

