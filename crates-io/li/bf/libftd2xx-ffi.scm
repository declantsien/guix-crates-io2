(define-module (crates-io li bf libftd2xx-ffi) #:use-module (crates-io))

(define-public crate-libftd2xx-ffi-0.1.1 (c (n "libftd2xx-ffi") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "12ww9ad8fhwwgpbsv779sa6cpdyvb0484v7478rvjilj409mcmki")))

(define-public crate-libftd2xx-ffi-0.1.2 (c (n "libftd2xx-ffi") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)))) (h "1yr7fp7pnjgm0rlzaw53d1xic1v4bng74sg8m43nsy83fqc53kq6")))

(define-public crate-libftd2xx-ffi-0.1.3 (c (n "libftd2xx-ffi") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "0vqcvzzl62q2bxz9j3h0zn6iipxb0whm5g620ja3qjm0nryvjw07")))

(define-public crate-libftd2xx-ffi-0.2.0 (c (n "libftd2xx-ffi") (v "0.2.0") (d (list (d (n "bindgen") (r "~0.54.1") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "~0.1.10") (d #t) (k 0)) (d (n "cfg-if") (r "~0.1.10") (d #t) (k 1)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "143r6rmjn7xp15zpabc4syd51d8kq5lhq9x5a54l57kabl7g9rqb")))

(define-public crate-libftd2xx-ffi-0.2.1 (c (n "libftd2xx-ffi") (v "0.2.1") (d (list (d (n "bindgen") (r "~0.54.1") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "~0.1.10") (d #t) (k 0)) (d (n "cfg-if") (r "~0.1.10") (d #t) (k 1)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1dhh6p954v7g9x53r9jc7f0c6vbncw3xjnd5mv4279a9zyxxpi92")))

(define-public crate-libftd2xx-ffi-0.2.2 (c (n "libftd2xx-ffi") (v "0.2.2") (d (list (d (n "bindgen") (r "~0.54.1") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "~0.1.10") (d #t) (k 0)) (d (n "cfg-if") (r "~0.1.10") (d #t) (k 1)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1h2iayfmdp7g24da3lg3lbhmh61vf06rm1ixpxr28qkicw38whwj")))

(define-public crate-libftd2xx-ffi-0.2.3 (c (n "libftd2xx-ffi") (v "0.2.3") (d (list (d (n "bindgen") (r "~0.54.1") (o #t) (d #t) (k 1)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1spcrq5sj2dvgil2sjrc8iwk4hgnvpy3b84z6pns6gbgfiw8pbl6")))

(define-public crate-libftd2xx-ffi-0.3.0 (c (n "libftd2xx-ffi") (v "0.3.0") (d (list (d (n "bindgen") (r "~0.54.1") (o #t) (d #t) (k 1)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "11wr8clp3c4b3784x0n31f0pp9k8sjyw16z5b3dh9xgfcqkf37yf")))

(define-public crate-libftd2xx-ffi-0.4.0 (c (n "libftd2xx-ffi") (v "0.4.0") (d (list (d (n "bindgen") (r "~0.55.1") (o #t) (d #t) (k 1)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "17wssxyfl1yxhryimqlwmkkx80kxkb5qarkyv524i6a4v4mdmxnq")))

(define-public crate-libftd2xx-ffi-0.4.1 (c (n "libftd2xx-ffi") (v "0.4.1") (d (list (d (n "bindgen") (r "~0.56.0") (o #t) (d #t) (k 1)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "15gw1gcgm7j0ijihkpi09256wlcjs1yghhzv9rmwzvakycmm2zzb")))

(define-public crate-libftd2xx-ffi-0.5.0 (c (n "libftd2xx-ffi") (v "0.5.0") (d (list (d (n "bindgen") (r "~0.56.0") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1l00ac1594rrg5igv9blkajfbcv2j9mnb8sdi5f4jih6l7vbqyvx")))

(define-public crate-libftd2xx-ffi-0.5.1 (c (n "libftd2xx-ffi") (v "0.5.1") (d (list (d (n "bindgen") (r "~0.56.0") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "1m2zcpawbz3wh0mrpr8v5rfdd2hbabdv6c08lm6dklv635m6nxq7")))

(define-public crate-libftd2xx-ffi-0.6.0 (c (n "libftd2xx-ffi") (v "0.6.0") (d (list (d (n "bindgen") (r "~0.57.0") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "0qgchbvr23c98pl86blslncrmy8mhavhckziwci5s1j266z8w3pz")))

(define-public crate-libftd2xx-ffi-0.7.0 (c (n "libftd2xx-ffi") (v "0.7.0") (d (list (d (n "bindgen") (r "~0.57.0") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.1") (d #t) (k 2)))) (h "01qf1nn39bl0mp8947jzprnf7khjymdgr9bfa8232lgxwhjfxn01") (f (quote (("static"))))))

(define-public crate-libftd2xx-ffi-0.8.0 (c (n "libftd2xx-ffi") (v "0.8.0") (d (list (d (n "bindgen") (r "~0.58.1") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "1ibszf1sy45sncf725hf2ics2fkl5yx5qr6l7mqz1bwv7q868mym") (f (quote (("static"))))))

(define-public crate-libftd2xx-ffi-0.8.1 (c (n "libftd2xx-ffi") (v "0.8.1") (d (list (d (n "bindgen") (r "~0.58.1") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "024yrvi9ykkncy8ky4vsvrm2s4ngdw1ivvc4vz9zig7jr84nkqby") (f (quote (("static"))))))

(define-public crate-libftd2xx-ffi-0.8.2 (c (n "libftd2xx-ffi") (v "0.8.2") (d (list (d (n "bindgen") (r "~0.58.1") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "1gspvpy9m2j2zvvs5kg3j2j2sbqr5wpb7fbzpda3d8nrlihxywyp") (f (quote (("static"))))))

(define-public crate-libftd2xx-ffi-0.8.3 (c (n "libftd2xx-ffi") (v "0.8.3") (d (list (d (n "bindgen") (r "~0.58.1") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0bbsh7q9hhmgnzrkkdsjzh6npk5inb4hs90zmz6mdqdnmdr0yxz3") (f (quote (("static"))))))

(define-public crate-libftd2xx-ffi-0.8.4 (c (n "libftd2xx-ffi") (v "0.8.4") (d (list (d (n "bindgen") (r "~0.58.1") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0yhh625p61b0clqha0jb2a6614i9qlzzpxdk6jxfbz7hdybgb30y") (f (quote (("static"))))))

(define-public crate-libftd2xx-ffi-0.8.5 (c (n "libftd2xx-ffi") (v "0.8.5") (d (list (d (n "bindgen") (r "~0.60.1") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "0kg6lvxp20nbcc75j9iaddn21p85x2zbn3s0n4rgx3f3fflx2vh2") (f (quote (("static")))) (l "ftd2xx")))

(define-public crate-libftd2xx-ffi-0.8.6 (c (n "libftd2xx-ffi") (v "0.8.6") (d (list (d (n "bindgen") (r "~0.63.0") (o #t) (d #t) (k 1)) (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "version-sync") (r "~0.9.2") (d #t) (k 2)))) (h "1kl94wmr7nz3yzdsdnfg1jz2mrllh0d1sdpiwkh84lm47vsxggj0") (f (quote (("static")))) (l "ftd2xx")))

