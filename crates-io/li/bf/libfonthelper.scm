(define-module (crates-io li bf libfonthelper) #:use-module (crates-io))

(define-public crate-libfonthelper-0.1.2 (c (n "libfonthelper") (v "0.1.2") (d (list (d (n "finder") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1c7ravp7ja78aq2wncpwq3v37vsnxikv2s8mzzwws7arw2syzna2")))

(define-public crate-libfonthelper-0.1.3 (c (n "libfonthelper") (v "0.1.3") (d (list (d (n "finder") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1i9hklq4d96c3l9fvmwa52a9cn84amqac7bv4pgcq5k6817780av")))

(define-public crate-libfonthelper-0.1.4 (c (n "libfonthelper") (v "0.1.4") (d (list (d (n "finder") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)))) (h "1c55jqdkq2iidipdzcafc15g7wbp0r48j2n41zrjxzkln0b34y77")))

(define-public crate-libfonthelper-0.2.0 (c (n "libfonthelper") (v "0.2.0") (d (list (d (n "finder") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.21.0") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1n6xb8zaa85wcwd3n2jmdrw453kmzfq2b5h8xahl2ld8vllzxlxq")))

(define-public crate-libfonthelper-0.2.1 (c (n "libfonthelper") (v "0.2.1") (d (list (d (n "finder") (r "^0.1.1") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1dghmfvs3ssvw0xb6byr52fbz5rhk8jjvmyl4p4ak2rkp87skn5y")))

(define-public crate-libfonthelper-0.2.2 (c (n "libfonthelper") (v "0.2.2") (d (list (d (n "finder") (r "^0.1.5") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1b4b45mr04hsc8zqrk4h5igx65v25lhwhp2aqxfmw2j7j3v7n6w2")))

(define-public crate-libfonthelper-0.2.3 (c (n "libfonthelper") (v "0.2.3") (d (list (d (n "finder") (r "^0.1.5") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0vzn7gdhxbmd91v2x5j15q4ghfnr69y2s7jiphhpys36mvdd2kjl")))

(define-public crate-libfonthelper-0.2.4 (c (n "libfonthelper") (v "0.2.4") (d (list (d (n "finder") (r "^0.1.5") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.21.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "17jxwjdakn6q2iqzgbpg6wy4wz7cbaq20jajjnzbb2zkzr3pv141")))

(define-public crate-libfonthelper-0.2.5 (c (n "libfonthelper") (v "0.2.5") (d (list (d (n "finder") (r "^0.1.5") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.26.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1m1khlz4p6pxfaad0l6rly61bkzwh77q0jdgr9kk9wby6qy9kgv3")))

(define-public crate-libfonthelper-0.2.6 (c (n "libfonthelper") (v "0.2.6") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.26.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0zng1f8amq36nhjl637vm7pn1kymah11b2nvd49hpsbyp08pbi9b")))

(define-public crate-libfonthelper-0.2.7 (c (n "libfonthelper") (v "0.2.7") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.26.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "14l68skj8x46q92cdy9d51h71zi6yj8sdcv08fp9vnrjnzm1yygz")))

(define-public crate-libfonthelper-0.2.8 (c (n "libfonthelper") (v "0.2.8") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "freetype-rs") (r "^0.26.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "1chi4096qs69gszqp088jnjimqm8frff49rajagz1iyy2cwck3l7")))

(define-public crate-libfonthelper-0.2.9-beta.0 (c (n "libfonthelper") (v "0.2.9-beta.0") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.115") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)))) (h "0fn4pdrvmf52yz7mrzw6id79lj40zhnp2nzxsfckf6f81q6vfnm3")))

(define-public crate-libfonthelper-0.3.0 (c (n "libfonthelper") (v "0.3.0") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "font-reader") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1gsgl15cqshbyq1vbbllpv8bxzxxg1jh1a5wr9c95w48s9f0ak9h")))

(define-public crate-libfonthelper-0.3.2 (c (n "libfonthelper") (v "0.3.2") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "font-reader") (r "^0.1.3") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1x595g39ia1i0fdyi5ddkdqq4x3x3mlzmfgjcgknf7ibh4zf43w1")))

(define-public crate-libfonthelper-0.3.3 (c (n "libfonthelper") (v "0.3.3") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "font-reader") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1gzdmbl1cr5hfkwzlcspnddw7miswjrx1ag9bw754f1q4b06hdyj")))

(define-public crate-libfonthelper-0.3.4 (c (n "libfonthelper") (v "0.3.4") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "font-reader") (r "^0.1.5") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1hmhkk4yss5l11ffhxczijxk2yqgh1c7pfazh2b0s9c4m4c4n38l")))

(define-public crate-libfonthelper-0.3.5 (c (n "libfonthelper") (v "0.3.5") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "font-reader") (r "^0.1.6") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1g5gm2lgswx1a16bn3hdg362w2v6mx953f1k2i4kl1xk5gilrk7n")))

(define-public crate-libfonthelper-0.3.6 (c (n "libfonthelper") (v "0.3.6") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "font-reader") (r "^0.1.7") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0qi8phm6vlj6dhzfy7vk5g1f5mkpg5q0k6nzmrcg665chcdcp1vl")))

(define-public crate-libfonthelper-0.3.7 (c (n "libfonthelper") (v "0.3.7") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "font-reader") (r "^0.1.8") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "15icxwx4iqav9rscvirz34mkl6646a2999cc7fpkn5p0x9y1l5am")))

(define-public crate-libfonthelper-0.3.8 (c (n "libfonthelper") (v "0.3.8") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "font-reader") (r "^0.1.9") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "1sxh3v91vz3hpnxzgh6ng138yxig9v6vkwx8mq4lzr0mjbq1dwn6")))

(define-public crate-libfonthelper-0.3.9 (c (n "libfonthelper") (v "0.3.9") (d (list (d (n "finder") (r "^0.1.6") (d #t) (k 0)) (d (n "font-reader") (r "^0.1.10") (d #t) (k 0)) (d (n "regex") (r "^1.3.9") (d #t) (k 0)))) (h "0dg342c2mn6csjisw4qidjshb6z7gmqg7wb9jf3dyrbhm07ml03v")))

