(define-module (crates-io li bf libfar) #:use-module (crates-io))

(define-public crate-libfar-0.1.0 (c (n "libfar") (v "0.1.0") (h "1j074rycjhfdwlxkxlli6cwhscqn47b62cbd45xaj50li6pc0b04") (y #t)))

(define-public crate-libfar-0.1.1 (c (n "libfar") (v "0.1.1") (h "0iszml86fk5s93912wiay5s994if1ilf8ncf19yylhc4d323k0an")))

(define-public crate-libfar-0.2.0 (c (n "libfar") (v "0.2.0") (h "0z1jcy2q6097y74v4rrb4d3mgglz0vj2fkz0vbayyy1qw6k36jxy")))

