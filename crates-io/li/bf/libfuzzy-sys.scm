(define-module (crates-io li bf libfuzzy-sys) #:use-module (crates-io))

(define-public crate-libfuzzy-sys-0.1.0 (c (n "libfuzzy-sys") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.38") (d #t) (k 1)) (d (n "libc") (r "^0.2.17") (d #t) (k 0)))) (h "0hanxqaqb9sb3l7ygb0bldi2g7418z77h1zqz66pwfbx4gkn5s6g")))

(define-public crate-libfuzzy-sys-0.2.0 (c (n "libfuzzy-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2.40") (d #t) (k 0)))) (h "14303hyjyvil6qdk2wax44lz3ldcps7vy6fa1wif4xf9xc86q3c9")))

(define-public crate-libfuzzy-sys-0.3.0 (c (n "libfuzzy-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0fmnm7w2v11w1crjj1d16p8yz9x3hn9dfk749ablkjb5chmywr2i") (l "libfuzzy")))

(define-public crate-libfuzzy-sys-0.4.0 (c (n "libfuzzy-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kmbrl347jll5bcdm0zg9il72r1lw7mzw70yh0glq9qx3h45dbx4") (l "libfuzzy")))

(define-public crate-libfuzzy-sys-0.5.0 (c (n "libfuzzy-sys") (v "0.5.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1npj1ia123m9ic9yq58x1wnik7gvn6rnfkz6gcyjzyaymaldiz0p") (l "libfuzzy")))

(define-public crate-libfuzzy-sys-0.6.0 (c (n "libfuzzy-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k43h9llj0473azz9bjbsihhba31xrfcd93ifnznsl5r2lyfnx27") (l "libfuzzy")))

