(define-module (crates-io li bf libflo_error) #:use-module (crates-io))

(define-public crate-libflo_error-0.1.0 (c (n "libflo_error") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "1jqdiq1y6mj76wfvjmbp41nayv8xdxbzkfncygxbbj9ijmmbgwgh")))

(define-public crate-libflo_error-0.1.1 (c (n "libflo_error") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "1jvyfmzabxg42dd0y2msf06aq909gz7h5qp93nk3j885kkybildc")))

(define-public crate-libflo_error-0.1.2 (c (n "libflo_error") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "1izhavymkif9vpp0h13gzpskqqas2ggyz74hpxzxqbqan5hcxngf")))

