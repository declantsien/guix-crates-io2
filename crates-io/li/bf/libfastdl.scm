(define-module (crates-io li bf libfastdl) #:use-module (crates-io))

(define-public crate-libfastdl-0.1.0 (c (n "libfastdl") (v "0.1.0") (d (list (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1hxpr657svx51faab1s63i6n31sx6krisp9rpr8awh5rkaqfisvd")))

(define-public crate-libfastdl-0.2.0 (c (n "libfastdl") (v "0.2.0") (d (list (d (n "libloading") (r "^0.5") (d #t) (k 0)))) (h "1irhiwydn6wlxnhxlsm1625b5z836wdrnck078gvlrhprs5547pp")))

