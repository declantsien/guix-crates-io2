(define-module (crates-io li bf libffm) #:use-module (crates-io))

(define-public crate-libffm-0.1.0 (c (n "libffm") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0gj4zapd6dhcya7v1vm76gnf6c99pgh08hmlj3ib8076k1ck1sxy") (f (quote (("cli" "structopt"))))))

