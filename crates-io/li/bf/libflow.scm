(define-module (crates-io li bf libflow) #:use-module (crates-io))

(define-public crate-libflow-0.1.0 (c (n "libflow") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "pcap-parser") (r "^0.9.3") (f (quote ("data"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "15f2m9w4mxck5azfwxw11j0nzjs4g3ba9cpgvdi9bhbxhkwyfgaf")))

(define-public crate-libflow-0.1.1 (c (n "libflow") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "pcap-parser") (r "^0.9.3") (f (quote ("data"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "0j2p7nspz9czkfaj277px3qyjvxcl1nqajrfs9zqq48arvij2h5s")))

(define-public crate-libflow-0.1.2 (c (n "libflow") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "pcap-parser") (r "^0.9.3") (f (quote ("data"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "statrs") (r "^0.13.0") (d #t) (k 0)))) (h "1lfc06xn6x20l90byzzc5xkrrgilvcxn1dkhq7jsbrn64b6n9nlj")))

(define-public crate-libflow-0.1.3 (c (n "libflow") (v "0.1.3") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wx9s633kirmivpids140khjkn1iji895m9zcmnjdia8l81z0vlq")))

(define-public crate-libflow-0.1.4 (c (n "libflow") (v "0.1.4") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0g6661k56zyhljakh36gllk6ak2q88kv0mgc8jvf5s594vh96pv0")))

(define-public crate-libflow-0.1.5 (c (n "libflow") (v "0.1.5") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0kpp195fc84mfvlh2hhlf6zwj7wv4x5daykwd3111w7f8z360l0c")))

(define-public crate-libflow-0.2.0 (c (n "libflow") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1l64s7nxrzr0j8l7c6bl766mfw54nb00r8c7jb8wwkz1b57yc4k6")))

(define-public crate-libflow-0.2.1 (c (n "libflow") (v "0.2.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "0h3znhkq4m5mi76gsw1w49c3la3ks9l3zm0j9rnmg7qrs2w6kfr1")))

(define-public crate-libflow-0.3.0 (c (n "libflow") (v "0.3.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1apvp6ziqi5fzakhlibify6vx16dnd44jf4hbxlhsx020ayadg9n")))

(define-public crate-libflow-0.3.1 (c (n "libflow") (v "0.3.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_with") (r "^1.4.0") (f (quote ("json"))) (d #t) (k 0)))) (h "1jixj0627khcqxlbnjxj545m7q2gm19xpkr0f51vgfkaj1djxmpk")))

