(define-module (crates-io li bf libfdisk-sys) #:use-module (crates-io))

(define-public crate-libfdisk-sys-0.1.0 (c (n "libfdisk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.65") (d #t) (k 0)))) (h "0bql4drfrj5n7jlmf41ckn1rjrz9p7ai960kf2fcy67mx5d5g0r4")))

