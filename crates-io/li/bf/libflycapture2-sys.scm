(define-module (crates-io li bf libflycapture2-sys) #:use-module (crates-io))

(define-public crate-libflycapture2-sys-0.1.0 (c (n "libflycapture2-sys") (v "0.1.0") (h "1s22pxygrgkrljlmagpkrfp98ylnzlszipwk9lqrix6nbp5abxji")))

(define-public crate-libflycapture2-sys-0.1.1 (c (n "libflycapture2-sys") (v "0.1.1") (h "1wqlf4ffcx5iangyskj529s35k6grn7ds18v08xq0f5967nsn107")))

