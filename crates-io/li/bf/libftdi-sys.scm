(define-module (crates-io li bf libftdi-sys) #:use-module (crates-io))

(define-public crate-libftdi-sys-1.4.0 (c (n "libftdi-sys") (v "1.4.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "1s41ifvp9j8xwvn1yk7q44pqr9lmdg4axa5ql2n9lxpd4jdk6n9s") (y #t)))

(define-public crate-libftdi-sys-1.4.1 (c (n "libftdi-sys") (v "1.4.1") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)))) (h "0a8bmcsvb08ckb8r58in45hxqfia12p9vg6drwbjzmanvfszscgw") (y #t)))

