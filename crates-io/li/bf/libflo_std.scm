(define-module (crates-io li bf libflo_std) #:use-module (crates-io))

(define-public crate-libflo_std-0.1.0 (c (n "libflo_std") (v "0.1.0") (d (list (d (n "libflo") (r "^0.1") (d #t) (k 0)) (d (n "libflo_error") (r "^0.1") (d #t) (k 0)) (d (n "libflo_event") (r "^0.1") (d #t) (k 0)) (d (n "libflo_func") (r "^0.1") (d #t) (k 0)) (d (n "libflo_module") (r "^0.1") (d #t) (k 0)) (d (n "libflo_type") (r "^0.1") (d #t) (k 0)))) (h "17nhmkkrqkrh58vp2dwpy33hqcrmxvbgarvw4yia6p3jqg83zqi8")))

