(define-module (crates-io li bf libfct4) #:use-module (crates-io))

(define-public crate-libfct4-0.1.0 (c (n "libfct4") (v "0.1.0") (d (list (d (n "bufreaderwriter") (r "^0.1.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "14331059vm8fmlwr2pdfx92r64xh2akh6plfsqpxd912r2zhj5qs")))

(define-public crate-libfct4-0.1.1 (c (n "libfct4") (v "0.1.1") (d (list (d (n "bufreaderwriter") (r "^0.1.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1qdjna44ikg790rfm75mynk8kbz2wacvnfmvplmniv7h9zmi14pz")))

(define-public crate-libfct4-0.1.2 (c (n "libfct4") (v "0.1.2") (d (list (d (n "bufreaderwriter") (r "^0.1.2") (d #t) (k 0)) (d (n "pathdiff") (r "^0.1.0") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "009kmbsf34aynd9iwynkwgd260x5icymgalk0lz4aqzw8c53jvs6")))

