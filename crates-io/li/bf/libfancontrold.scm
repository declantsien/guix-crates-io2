(define-module (crates-io li bf libfancontrold) #:use-module (crates-io))

(define-public crate-libfancontrold-0.1.0 (c (n "libfancontrold") (v "0.1.0") (d (list (d (n "libmedium") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "13naw9rw83aalvskyyalrhdvc9105q37ciclvr0kpdyyxjv7yaga")))

(define-public crate-libfancontrold-0.2.0 (c (n "libfancontrold") (v "0.2.0") (d (list (d (n "libmedium") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1vvh9p7bhpy1y20nnmcpky26b11mrhcb4wzwvmbhg4v35fymbdv1")))

(define-public crate-libfancontrold-0.2.1 (c (n "libfancontrold") (v "0.2.1") (d (list (d (n "libmedium") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0hl04g1vgi6zk2g67hf605kigp38llhw79imn71hw1ys54kij512")))

(define-public crate-libfancontrold-0.2.2 (c (n "libfancontrold") (v "0.2.2") (d (list (d (n "libmedium") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs"))) (o #t) (d #t) (k 0)))) (h "0npy8ym71vsgi6cy68iq2ahri7va2rn6x7lyvxnskw7jy9ffwdzh") (f (quote (("async" "tokio" "libmedium/async"))))))

(define-public crate-libfancontrold-0.3.0 (c (n "libfancontrold") (v "0.3.0") (d (list (d (n "libmedium") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("fs"))) (o #t) (d #t) (k 0)))) (h "1hgq2h0ab89lq65l7x0aicdcc009lc6waa4nyb4jax20jsyjdz9d") (f (quote (("async" "tokio" "libmedium/async"))))))

