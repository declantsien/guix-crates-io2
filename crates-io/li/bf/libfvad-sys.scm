(define-module (crates-io li bf libfvad-sys) #:use-module (crates-io))

(define-public crate-libfvad-sys-1.0.0 (c (n "libfvad-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.56.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.66") (d #t) (k 1)))) (h "1n4lij55vaffxhc9ilp8k9wpjmzf17v7h330c6szlr8cfs4m6fs7") (l "libfvad")))

