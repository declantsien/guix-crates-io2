(define-module (crates-io li mi liminal) #:use-module (crates-io))

(define-public crate-liminal-0.1.0 (c (n "liminal") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0lnsb2srax6jjl70w85afqz6vwmvn1l4ppacfilvagw15m25amya")))

(define-public crate-liminal-0.1.1 (c (n "liminal") (v "0.1.1") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1vqx9qndcckhm16gai2mnkc1nvikmv95s5gsibv6f4p8nylkcp8q")))

(define-public crate-liminal-0.1.2 (c (n "liminal") (v "0.1.2") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1rbcn8y533p08m9d9bqgrkd4wcwh4bn07bl42d7pn22qsi4ydrxh")))

(define-public crate-liminal-0.1.3 (c (n "liminal") (v "0.1.3") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0vswxpvmiyzkr7x2dryl976yd9r5jx2bkrhihm0dzb3bsjbg7wax")))

(define-public crate-liminal-0.1.4 (c (n "liminal") (v "0.1.4") (d (list (d (n "axum") (r "^0.7.5") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_derive") (r "^4.0.0-rc.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.116") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "028kdbg0ffwdb0yqrdjs0ckd341rrfwg8ardkq5niws0nxycnfyg")))

