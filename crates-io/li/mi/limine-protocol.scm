(define-module (crates-io li mi limine-protocol) #:use-module (crates-io))

(define-public crate-limine-protocol-0.1.0 (c (n "limine-protocol") (v "0.1.0") (h "1rcyw4a6gz4g5abh4h0rryyllsfwsdk7zb8cp8k8g8282xiyl477")))

(define-public crate-limine-protocol-0.2.0 (c (n "limine-protocol") (v "0.2.0") (h "0mz8grs4hyqgyhvdn8nvrv7g7xsj5d1lbn3rfq95vgzn2db31faf")))

(define-public crate-limine-protocol-0.2.1 (c (n "limine-protocol") (v "0.2.1") (h "0y28xd7s37n4q1nr9lkbgzzbw0jj1wn67ar0am0z0x3h3y8qlbzy")))

(define-public crate-limine-protocol-0.3.0 (c (n "limine-protocol") (v "0.3.0") (h "003fnm68dg7h805jnjhabc65iw3j8698rw1gmwipgdsx5fhx5nj9")))

(define-public crate-limine-protocol-0.3.1 (c (n "limine-protocol") (v "0.3.1") (h "0h32kdxqsbw34fvvzhvvrw02xr0d01d7kq5rk4qsywg56nm1f3ws")))

(define-public crate-limine-protocol-0.3.2 (c (n "limine-protocol") (v "0.3.2") (h "0ngcm313sccjqzv5fhhw0qdjwjih6fsq8263b4swj5vam6vlkljj")))

(define-public crate-limine-protocol-0.4.0 (c (n "limine-protocol") (v "0.4.0") (h "0rdaarygwd829as1gv9wfa3dklbpv00n5232qadqmj111spfz02w")))

(define-public crate-limine-protocol-0.4.1 (c (n "limine-protocol") (v "0.4.1") (h "1hgzx04l5dml7abd8hf6nkjdcqhixpv1amljybnl38k5bs3qgv1x")))

(define-public crate-limine-protocol-0.4.2 (c (n "limine-protocol") (v "0.4.2") (h "0vnwr0462bzcc59mmypif0immiavn50bs4xmw3qy75qgbahmlf1a")))

(define-public crate-limine-protocol-0.4.3 (c (n "limine-protocol") (v "0.4.3") (h "0nf1i3iablflsd0lwfg0kx37a3sy9l5gfjafq44d4arvx9syfdk7")))

(define-public crate-limine-protocol-0.5.0 (c (n "limine-protocol") (v "0.5.0") (h "14gqdl80g16dz4lirshvdgf60136m3pb9kmzffl3z8j73c15dpqp")))

