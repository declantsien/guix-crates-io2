(define-module (crates-io li mi limine-rs) #:use-module (crates-io))

(define-public crate-limine-rs-0.1.0 (c (n "limine-rs") (v "0.1.0") (h "1h4pgb6f9savrs0kns4fsfdmkvzhx26a9xszbckh3ajy715ck5br")))

(define-public crate-limine-rs-0.1.1 (c (n "limine-rs") (v "0.1.1") (h "05xq9rz6w2nhdwv2vy7yl8ygdbyixc2haf00mqmdigs11hnqrycs")))

(define-public crate-limine-rs-0.1.2 (c (n "limine-rs") (v "0.1.2") (h "05wil2gv6bs7z683slk7kpmki1141alfrm5q5j3v1pkcd4ksh83v")))

