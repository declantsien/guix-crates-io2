(define-module (crates-io li mi limitation-actix-middleware) #:use-module (crates-io))

(define-public crate-limitation-actix-middleware-0.1.0 (c (n "limitation-actix-middleware") (v "0.1.0") (d (list (d (n "actix-web") (r "^1.0.8") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "limitation") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "17qvixyixmy2s5i6brqsynygbfnbbh0g1qqf9gjnbq86c0sivfbw")))

(define-public crate-limitation-actix-middleware-0.1.1 (c (n "limitation-actix-middleware") (v "0.1.1") (d (list (d (n "actix-web") (r "^1.0.8") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "limitation") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "1cc2w03j3rlzm9zpxq5bviinml06pfhc21c3dpb3cy3p67ywh6gi")))

