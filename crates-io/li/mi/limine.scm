(define-module (crates-io li mi limine) #:use-module (crates-io))

(define-public crate-limine-0.1.0 (c (n "limine") (v "0.1.0") (h "10d3n99vfjfq7hlfw9jiyyhjj8jhbmw0s15xyvfwh23xcm7gpf26")))

(define-public crate-limine-0.1.1 (c (n "limine") (v "0.1.1") (h "1prigndz6l0w3y6rahh8a53f3nhn9i2bfwa36gsdcxzzj5xyn71k")))

(define-public crate-limine-0.1.2 (c (n "limine") (v "0.1.2") (h "18m7kjfza5qh69kg4pnxm3iqvl9r24wzmzj4fy60yv7dcxbjaj3c")))

(define-public crate-limine-0.1.3 (c (n "limine") (v "0.1.3") (h "1lsj6ahnn1ziil11cjcib85rpn6xapqsvcap9qk56alqr10jdvsz")))

(define-public crate-limine-0.1.4 (c (n "limine") (v "0.1.4") (d (list (d (n "limine-proc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "0lvsi4nlaq8rdihdndrz3g3n5jrx4j1dkym8pvrm9gvjim093i2k") (f (quote (("requests-section" "limine-proc") ("default"))))))

(define-public crate-limine-0.1.5 (c (n "limine") (v "0.1.5") (d (list (d (n "limine-proc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1fc0yqlpaibr2ap15p1045y7d2gbxy0473lr0n5h5irwkhssqcw4") (f (quote (("requests-section" "limine-proc") ("default"))))))

(define-public crate-limine-0.1.6 (c (n "limine") (v "0.1.6") (d (list (d (n "limine-proc") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "1dcw1ikdv5vn5d9h4ajz6c9f2318wwjppyflla1rz3l4cqid284i") (f (quote (("requests-section" "limine-proc") ("default"))))))

(define-public crate-limine-0.1.7 (c (n "limine") (v "0.1.7") (d (list (d (n "limine-proc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (o #t) (d #t) (k 0)))) (h "0i6c29lh4z6x1545myfc119zsm4qk08i4f1yp3z5npxi61z3h5jq") (f (quote (("requests-section" "limine-proc") ("into-uuid" "uuid") ("default")))) (y #t)))

(define-public crate-limine-0.1.8 (c (n "limine") (v "0.1.8") (d (list (d (n "limine-proc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (o #t) (k 0)))) (h "17arxvrhm8biifymns09sqq42d2x9w0v3rc6g4wb5fh5wlc4dipa") (f (quote (("requests-section" "limine-proc") ("into-uuid" "uuid") ("default"))))))

(define-public crate-limine-0.1.9 (c (n "limine") (v "0.1.9") (d (list (d (n "limine-proc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (o #t) (k 0)))) (h "01gi2cz42azwdag8c2a0b1wk215i483q7afzalvvllqci8aaqiy8") (f (quote (("requests-section" "limine-proc") ("into-uuid" "uuid") ("default"))))))

(define-public crate-limine-0.1.10 (c (n "limine") (v "0.1.10") (d (list (d (n "limine-proc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (o #t) (k 0)))) (h "01skqb8pnimwsbzrrf0x02qqybvhkn5l82bb1r7h9b2n22ahr7ay") (f (quote (("requests-section" "limine-proc") ("into-uuid" "uuid") ("default"))))))

(define-public crate-limine-0.1.11 (c (n "limine") (v "0.1.11") (d (list (d (n "limine-proc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (o #t) (k 0)))) (h "0zpj1vyaxrvncfndc73ra3pv6wd6bpdy2mj50sq8wkh32b2nfp7l") (f (quote (("requests-section" "limine-proc") ("into-uuid" "uuid") ("default"))))))

(define-public crate-limine-0.1.12 (c (n "limine") (v "0.1.12") (d (list (d (n "limine-proc") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^1.1.2") (o #t) (k 0)))) (h "1ppya93hawkir6jd162mnxa8x2q1n1nszyqhyd8g0yrydf7ly0q2") (f (quote (("requests-section" "limine-proc") ("into-uuid" "uuid") ("default"))))))

(define-public crate-limine-0.2.0 (c (n "limine") (v "0.2.0") (d (list (d (n "bitflags") (r "^2") (d #t) (k 0)) (d (n "uuid") (r "^1") (o #t) (k 0)))) (h "1376d3vdqlhknxswz1vaqhjkb3ip5fkwp60hlqbqg7h89pi8fv44") (f (quote (("ipaddr")))) (s 2) (e (quote (("uuid" "dep:uuid"))))))

