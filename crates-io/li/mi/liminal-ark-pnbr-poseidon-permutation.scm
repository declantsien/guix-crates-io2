(define-module (crates-io li mi liminal-ark-pnbr-poseidon-permutation) #:use-module (crates-io))

(define-public crate-liminal-ark-pnbr-poseidon-permutation-0.1.0 (c (n "liminal-ark-pnbr-poseidon-permutation") (v "0.1.0") (d (list (d (n "ark-ff") (r "^0.3") (k 0)) (d (n "ark-std") (r "^0.3.0") (k 0)) (d (n "poseidon-parameters") (r "^0.1.0") (k 0) (p "liminal-ark-pnbr-poseidon-parameters")))) (h "1lvj7fbbrn4wf69pmxs1ygjj8bvmc5n9rc1dxnhhjfx1h2vij16w") (f (quote (("std" "ark-ff/std" "ark-std/std" "poseidon-parameters/std") ("default" "std"))))))

