(define-module (crates-io li mi limithub-code-block-sdk) #:use-module (crates-io))

(define-public crate-limithub-code-block-sdk-0.1.0 (c (n "limithub-code-block-sdk") (v "0.1.0") (d (list (d (n "axum") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)))) (h "0sk6lhwz6y66xc4qdp3gisrg8fcwdqrm32gbzf97afilakn941q0")))

(define-public crate-limithub-code-block-sdk-0.2.0 (c (n "limithub-code-block-sdk") (v "0.2.0") (d (list (d (n "axum") (r "^0.7.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.114") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (d #t) (k 0)))) (h "1jl8brf0ir5hh673hlc06vky1cv7vdkqklwjwkkmlf3qdjsm448g")))

