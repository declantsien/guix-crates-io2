(define-module (crates-io li mi liminal-ark-pnbr-poseidon-parameters) #:use-module (crates-io))

(define-public crate-liminal-ark-pnbr-poseidon-parameters-0.1.0 (c (n "liminal-ark-pnbr-poseidon-parameters") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (k 0)) (d (n "ark-ff") (r "^0.3.0") (k 0)) (d (n "num-integer") (r "^0.1.45") (k 0)) (d (n "ark-ed-on-bls12-377") (r "^0.3") (d #t) (k 2)) (d (n "proptest") (r "^1") (d #t) (k 2)))) (h "1vpjqp1bpd7ip3ka0v552vb7nzrwr88cbf97pln1iwlqmdbdl8w7") (f (quote (("std" "anyhow/std" "ark-ff/std" "num-integer/std"))))))

