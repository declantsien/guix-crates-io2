(define-module (crates-io li mi limits-rs) #:use-module (crates-io))

(define-public crate-limits-rs-0.1.0 (c (n "limits-rs") (v "0.1.0") (d (list (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1.6") (d #t) (k 0)))) (h "12rb9i6dpnapym7y2banpw2hv9c00d5qlwx15hklii3vwl6jhf66")))

(define-public crate-limits-rs-0.2.0 (c (n "limits-rs") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1z5151fikxa62yym76w42k2r5dixv7qs661ibnapv9h7h5kz3lqr")))

