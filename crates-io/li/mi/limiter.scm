(define-module (crates-io li mi limiter) #:use-module (crates-io))

(define-public crate-limiter-0.1.0 (c (n "limiter") (v "0.1.0") (d (list (d (n "iron") (r "^0.3") (d #t) (k 0)))) (h "0hzv006gghwmg3qzknviykh9f4sfqv2ixilzmqnn3z86kncjg15n")))

(define-public crate-limiter-0.1.1 (c (n "limiter") (v "0.1.1") (d (list (d (n "iron") (r "^0.3") (d #t) (k 0)))) (h "083zy0x4lwnzqjgy5anh4q8zqp165kn14ilkxn4yyhjgj03r6xsk")))

(define-public crate-limiter-0.1.2 (c (n "limiter") (v "0.1.2") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)))) (h "0br4c2shk4j2n064fbwq86k3xr440avzigd21qjav11svfpryzky")))

(define-public crate-limiter-0.2.0 (c (n "limiter") (v "0.2.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "04lhxdg99clqw4c4ws9hhn1p8acb4casz8a8myqyig0pyjckq3m8")))

(define-public crate-limiter-0.2.1 (c (n "limiter") (v "0.2.1") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)))) (h "1w1irhccqbqgj5v3z6yfqmji3b0ab418sr16l8m5kpiw0fxvr8wg")))

(define-public crate-limiter-0.3.0 (c (n "limiter") (v "0.3.0") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)))) (h "0ay9mqwdxz6iwapkx602mbpv4dv2czhn57ix9a2wn9lmfphxj0qz")))

(define-public crate-limiter-0.3.1 (c (n "limiter") (v "0.3.1") (d (list (d (n "iron") (r "^0.4") (d #t) (k 0)))) (h "1p77ccqxswp5rr9rm5544a3yrssx8zy6q5h2ixlwzcgdvvdskrj5")))

(define-public crate-limiter-0.3.2 (c (n "limiter") (v "0.3.2") (d (list (d (n "iron") (r "^0.5") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0xhnjdk57c2m7val4pmdszq851arp61ripc420rzava1cfzxiifb")))

(define-public crate-limiter-0.4.0 (c (n "limiter") (v "0.4.0") (d (list (d (n "iron") (r "^0.6.0") (d #t) (k 0)))) (h "17xhgkksqpkc4j7l0qvkdjw2g4w8h7828nvxwlmvs2jifl6jcl4c")))

