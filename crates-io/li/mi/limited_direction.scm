(define-module (crates-io li mi limited_direction) #:use-module (crates-io))

(define-public crate-limited_direction-0.1.0 (c (n "limited_direction") (v "0.1.0") (d (list (d (n "macroquad") (r "^0.3.25") (d #t) (k 2)))) (h "1jawzzv6zfqcvq385x6bnml61g2i0p932p51jmzgh1jazpwfhlgb") (y #t)))

(define-public crate-limited_direction-0.1.1 (c (n "limited_direction") (v "0.1.1") (d (list (d (n "macroquad") (r "^0.3.25") (d #t) (k 2)))) (h "153ccgj1j5y2dwrqj7gfgz22crqag3hpzzj8ynn9j5g9d2jf1gqs") (y #t)))

(define-public crate-limited_direction-0.2.0 (c (n "limited_direction") (v "0.2.0") (d (list (d (n "macroquad") (r "^0.3.25") (d #t) (k 2)))) (h "1wcdvv2b1hz5zhq2gkxyaik6qm0layjyyfs4rmkha2sx31fdpv8b")))

