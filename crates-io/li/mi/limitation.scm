(define-module (crates-io li mi limitation) #:use-module (crates-io))

(define-public crate-limitation-0.1.0 (c (n "limitation") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "redis") (r "^0.13.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 2)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "1sjqd5w0dpkvbca671y09qzf6pz6i01vpkvmg89jccwki3nyng90")))

(define-public crate-limitation-0.1.1 (c (n "limitation") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "futures") (r "^0.1.29") (d #t) (k 0)) (d (n "redis") (r "^0.13.0") (d #t) (k 0)) (d (n "time") (r "^0.1.42") (d #t) (k 0)) (d (n "tokio") (r "^0.1.22") (d #t) (k 2)) (d (n "version-sync") (r "^0.8.1") (d #t) (k 2)))) (h "1m96yxhjpdsmkkll5cdknpw5xivd6gylp6911w2zfc1rjq9m8hby")))

