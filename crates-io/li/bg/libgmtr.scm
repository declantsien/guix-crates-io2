(define-module (crates-io li bg libgmtr) #:use-module (crates-io))

(define-public crate-libgmtr-0.1.0 (c (n "libgmtr") (v "0.1.0") (h "0ir5l8ayl3wrpzj3lgysi1z8j7hlj1a7rzdzsizxmhii336bv6dj")))

(define-public crate-libgmtr-0.1.1 (c (n "libgmtr") (v "0.1.1") (h "03i5bf8xbn09v3p84pc65paikzcr8s7qmcq8xqjnxnva8mzsv948")))

