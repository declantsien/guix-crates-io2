(define-module (crates-io li bg libgpiod) #:use-module (crates-io))

(define-public crate-libgpiod-0.1.0 (c (n "libgpiod") (v "0.1.0") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "intmap") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "libgpiod-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0cc2p316iv07c5rjygr4isaj78mg5g8ymyaif5lzaqvh8d5xz7wy") (r "1.56")))

(define-public crate-libgpiod-0.2.0 (c (n "libgpiod") (v "0.2.0") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "intmap") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "libgpiod-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "01xrc1dq3rjwzfz2r2qlg7pbn8bzvwbv3rqjqblgymg51xn7p4kg") (r "1.60")))

(define-public crate-libgpiod-0.2.1 (c (n "libgpiod") (v "0.2.1") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "intmap") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "libgpiod-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0y8gxryzs3fb1if0pn2z6ipv78v8izxcsx1h3cp9hfvxjrbwq9pp") (f (quote (("vnext")))) (r "1.60")))

(define-public crate-libgpiod-0.2.2 (c (n "libgpiod") (v "0.2.2") (d (list (d (n "errno") (r "^0.2.8") (d #t) (k 0)) (d (n "intmap") (r "^2.0.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.39") (d #t) (k 0)) (d (n "libgpiod-sys") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1j6k032bb10bi0clrjdmqbvn1j771n81qzldxwxcryl2lyvv8a9j") (f (quote (("vnext" "v2_1") ("v2_1" "libgpiod-sys/v2_1")))) (r "1.60")))

