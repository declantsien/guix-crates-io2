(define-module (crates-io li bg libgpg-error-sys) #:use-module (crates-io))

(define-public crate-libgpg-error-sys-0.1.0 (c (n "libgpg-error-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "1kyrpgpawcsv9y0x6jnypc9c0ywys5bbnhr1dfn2visrr3r4amzm")))

(define-public crate-libgpg-error-sys-0.1.1 (c (n "libgpg-error-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)))) (h "09plvnnaz4n20zbghcyaz01g00n67qwaxywp01ws67via75sizws")))

(define-public crate-libgpg-error-sys-0.1.2 (c (n "libgpg-error-sys") (v "0.1.2") (h "1n7c241wlr5md4cjqb706qv393s2fgfbd503782ziaw2wl9pyffl")))

(define-public crate-libgpg-error-sys-0.1.4 (c (n "libgpg-error-sys") (v "0.1.4") (h "0da2r6qrsb1i2d381d7gzbcivxhz1pd4n0wkzcxw3rrb3idmgc0y")))

(define-public crate-libgpg-error-sys-0.1.5 (c (n "libgpg-error-sys") (v "0.1.5") (h "0hji7f0lc97np2jcvjkcj7fl0z0p3mrb41y7y89g34ka2ax4zdiv")))

(define-public crate-libgpg-error-sys-0.1.6 (c (n "libgpg-error-sys") (v "0.1.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0wh75ndbvzch2ni1crrxwvprffivmq192l9x5aqcva14bqx9giqg") (y #t)))

(define-public crate-libgpg-error-sys-0.2.0 (c (n "libgpg-error-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0kwvxzl2zaapcx5pgmyx1ma3ml3sy2jr1bswzlr8hgg8pwxcmvn7")))

(define-public crate-libgpg-error-sys-0.2.1 (c (n "libgpg-error-sys") (v "0.2.1") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0ay5g4kn6c55qsr1wi52qf7asfyf72vqjzjagmg9fhyd6nihjg0m")))

(define-public crate-libgpg-error-sys-0.2.2 (c (n "libgpg-error-sys") (v "0.2.2") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "04rdlvyyfp9bygnd9h4ic91i12zg1nb3hb1jjfmgmddwvj3crkjg")))

(define-public crate-libgpg-error-sys-0.2.3 (c (n "libgpg-error-sys") (v "0.2.3") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1cmybgpsw97hs0vp8p4np22z6801kidi1l2iyymd8j8w7y4q7vxh")))

(define-public crate-libgpg-error-sys-0.3.0 (c (n "libgpg-error-sys") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0gplhqhlp529bcf768ww5qp4czvv069bhb9rjglkxl5jwjhd8nyp")))

(define-public crate-libgpg-error-sys-0.3.1 (c (n "libgpg-error-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "1ynzw542hhpv8kyd0m4j5xz5ynccbhkpzsqgmh5sf4xwbckjvv2c")))

(define-public crate-libgpg-error-sys-0.3.2 (c (n "libgpg-error-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "1jxfdldlsyn7h99xdc9wsm3wsbs20xsn1h7jdbmmpr4a8p4hyf8h")))

(define-public crate-libgpg-error-sys-0.4.0 (c (n "libgpg-error-sys") (v "0.4.0") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "1lnj629c31fh099fqzmr19kykngnd1rp0xwv2k5ac2i8za2cqp8w") (f (quote (("default" "bundled") ("bundled")))) (l "gpg-error")))

(define-public crate-libgpg-error-sys-0.4.1 (c (n "libgpg-error-sys") (v "0.4.1") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)))) (h "02rc7n7x50nchhlm5nm9jqrx9bx48s35cp2wys9cadsgkss2qf4z") (f (quote (("default" "bundled") ("bundled")))) (l "gpg-error")))

(define-public crate-libgpg-error-sys-0.5.0 (c (n "libgpg-error-sys") (v "0.5.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xsz4rdm1fwm7qr50h3pc7p5748k66y0sw4180gqrav0xhrdylhl") (l "gpg-error")))

(define-public crate-libgpg-error-sys-0.5.1 (c (n "libgpg-error-sys") (v "0.5.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13wjwlkwrcy6f7jkx5fcp6jblvyfl5a9iajdz80dhl7x9w98h44s") (l "gpg-error")))

(define-public crate-libgpg-error-sys-0.5.2 (c (n "libgpg-error-sys") (v "0.5.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winreg") (r "^0.7") (d #t) (t "cfg(windows)") (k 1)))) (h "15w8a8wi48cmdswpj67xfyn047f7b43jpdcfs3yjapgw1vgsxcgz") (l "gpg-error")))

(define-public crate-libgpg-error-sys-0.6.0 (c (n "libgpg-error-sys") (v "0.6.0") (d (list (d (n "build-rs") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 1)) (d (n "system-deps") (r "^6.0.2") (d #t) (k 1)) (d (n "winreg") (r "^0.10.1") (d #t) (t "cfg(windows)") (k 1)))) (h "14028bkgwc36pgzwfcdk9dhpkxs0s1wq6mvkpp9kbj1r1wqpjw69") (l "gpg-error") (r "1.64")))

(define-public crate-libgpg-error-sys-0.6.1 (c (n "libgpg-error-sys") (v "0.6.1") (d (list (d (n "build-rs") (r "^0.1.2") (d #t) (t "cfg(windows)") (k 1)) (d (n "system-deps") (r "^6.2.2") (d #t) (k 1)) (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(windows)") (k 1)))) (h "1jfs9zpy4va4kwjxaj0pcskl65b5rjv3dzi610rmc7xqx59wq8yc") (l "gpg-error")))

