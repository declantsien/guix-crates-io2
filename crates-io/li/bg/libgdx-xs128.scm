(define-module (crates-io li bg libgdx-xs128) #:use-module (crates-io))

(define-public crate-libgdx-xs128-0.1.0 (c (n "libgdx-xs128") (v "0.1.0") (h "02vxplhnp2vckqqh7rjphqhw5blx0zlwis8i6a99xxfkrna66zxi") (f (quote (("std") ("reroll") ("default" "reroll" "check_zero_seed") ("check_zero_seed"))))))

(define-public crate-libgdx-xs128-0.1.1 (c (n "libgdx-xs128") (v "0.1.1") (h "0v1brsmyfhwsy4mrlhgpsyynyxbjvdpgg47y33rmsngpg5bv5jxs") (f (quote (("std") ("reroll") ("default" "reroll" "check_zero_seed") ("check_zero_seed"))))))

(define-public crate-libgdx-xs128-0.1.2 (c (n "libgdx-xs128") (v "0.1.2") (h "1pkyl1fm9wrb2jq694zjzyaz0wgz7fcp321dgljsp30wim2w6vi0") (f (quote (("std") ("reroll") ("default" "reroll" "check_zero_seed") ("check_zero_seed"))))))

(define-public crate-libgdx-xs128-0.1.3 (c (n "libgdx-xs128") (v "0.1.3") (h "0rawri22py92pq3h799c56w5pv0fixdr98lffc26nywpparddsz3") (f (quote (("std") ("reroll") ("default" "reroll" "check_zero_seed") ("check_zero_seed"))))))

