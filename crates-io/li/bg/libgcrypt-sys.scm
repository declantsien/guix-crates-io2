(define-module (crates-io li bg libgcrypt-sys) #:use-module (crates-io))

(define-public crate-libgcrypt-sys-0.1.0 (c (n "libgcrypt-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "*") (d #t) (k 0)))) (h "1cq0ckmb8w4nwr964n69ym7kz219pmr5dppq3ys5c5r11xjyna8n")))

(define-public crate-libgcrypt-sys-0.2.0 (c (n "libgcrypt-sys") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.1") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "*") (d #t) (k 0)))) (h "128wv7vyl0wvpzagp9q5yg9s3w6578bifpw8bvlz319j5hwnl46i")))

(define-public crate-libgcrypt-sys-0.3.0 (c (n "libgcrypt-sys") (v "0.3.0") (d (list (d (n "gcc") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.1") (d #t) (k 0)))) (h "17hixgisphqvdjg39bbx7vnml5i75gxyci1jd49piqc7f95b2bql") (f (quote (("shim" "gcc"))))))

(define-public crate-libgcrypt-sys-0.4.0 (c (n "libgcrypt-sys") (v "0.4.0") (d (list (d (n "gcc") (r "^0.3") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.1") (d #t) (k 0)))) (h "1ig72h110giry3bd8xdp1k22qj0ig20s6lf0sc00fagzwnkldx33") (f (quote (("shim" "gcc"))))))

(define-public crate-libgcrypt-sys-0.5.0 (c (n "libgcrypt-sys") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.2") (d #t) (k 0)))) (h "0cip2q8db73xbi4k1a54bf3y5ab5q32llxqh897bavg32z6iv2n5") (f (quote (("v1_7_0" "v1_6_0") ("v1_6_0") ("shim") ("default" "v1_7_0")))) (y #t)))

(define-public crate-libgcrypt-sys-0.5.1 (c (n "libgcrypt-sys") (v "0.5.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.2") (d #t) (k 0)))) (h "0z6ij05bgpgswi0k0al47awldbb92400q83gssz5j4bicik4sdfk") (f (quote (("v1_7_0" "v1_6_0") ("v1_6_0") ("shim") ("default" "v1_7_0")))) (y #t)))

(define-public crate-libgcrypt-sys-0.5.2 (c (n "libgcrypt-sys") (v "0.5.2") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.2") (d #t) (k 0)))) (h "04d24l16621mfwi7jq3nn2y7hxbqnd35bqga10sdcsxrgjxwcc19") (f (quote (("v1_7_0" "v1_6_0") ("v1_6_0") ("shim") ("default" "v1_7_0")))) (y #t)))

(define-public crate-libgcrypt-sys-0.5.3 (c (n "libgcrypt-sys") (v "0.5.3") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.2") (d #t) (k 0)))) (h "1a5v272l9z3rmplj15xb3xalkvz31hpil42zf7zqydk2fr6cp1xi") (f (quote (("v1_8_0" "v1_7_0") ("v1_7_0" "v1_6_0") ("v1_6_0") ("shim") ("default" "v1_8_0")))) (y #t)))

(define-public crate-libgcrypt-sys-0.5.4 (c (n "libgcrypt-sys") (v "0.5.4") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.2") (d #t) (k 0)))) (h "1n3gj4pj1xmk299mqhav3abmp42lcs82lwqri11i2c5ic291gn38") (f (quote (("v1_8_0" "v1_7_0") ("v1_7_0" "v1_6_0") ("v1_6_0") ("shim") ("default" "v1_8_0")))) (y #t)))

(define-public crate-libgcrypt-sys-0.6.0 (c (n "libgcrypt-sys") (v "0.6.0") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.4") (k 0)))) (h "0f5sz9kfjfxwkdgfk8ghb792gqk4590ssx7xc1y92zhiai37mcvr") (f (quote (("shim") ("default" "bundled") ("bundled" "libgpg-error-sys/bundled")))) (y #t) (l "gcrypt")))

(define-public crate-libgcrypt-sys-0.6.1 (c (n "libgcrypt-sys") (v "0.6.1") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.4") (k 0)))) (h "0367msq70fp3xqzawj616703a87ky9aldrvx0wrpz6qb04cvwsh3") (f (quote (("shim") ("default" "bundled") ("bundled" "libgpg-error-sys/bundled")))) (y #t) (l "gcrypt")))

(define-public crate-libgcrypt-sys-0.7.0 (c (n "libgcrypt-sys") (v "0.7.0") (d (list (d (n "cc") (r "^1.0.2") (d #t) (k 1)) (d (n "cfg-if") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libgpg-error-sys") (r "^0.5") (d #t) (k 0)))) (h "1r0l0a4g62wgs3i85h41g9xg057b4fr9a734z6nwin6nkinmvsv2") (f (quote (("shim")))) (l "gcrypt")))

