(define-module (crates-io li bg libgmssl) #:use-module (crates-io))

(define-public crate-libgmssl-0.1.0 (c (n "libgmssl") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libgmssl-sys") (r "^0.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1v0fc9ssvvfi390nhw68j1s9fhh1znss693pmiai2sirq8gs56zs") (f (quote (("default" "tracing")))) (y #t)))

(define-public crate-libgmssl-0.2.0 (c (n "libgmssl") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2") (d #t) (k 2)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libgmssl-sys") (r "^3.1") (d #t) (k 0)) (d (n "tracing") (r "^0.1") (o #t) (d #t) (k 0)))) (h "1yqzgn35hqnlcvay4cfnv7nbg27xrcrjd5fi5l3m3lkg0p311khp") (f (quote (("default" "tracing"))))))

