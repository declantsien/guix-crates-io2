(define-module (crates-io li bg libgweather) #:use-module (crates-io))

(define-public crate-libgweather-4.0.0 (c (n "libgweather") (v "4.0.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "ffi") (r "^4.0.0") (d #t) (k 0) (p "gweather-sys")) (d (n "gio") (r "^0.15") (d #t) (k 0)) (d (n "glib") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1cl4b55cnfj5fk4fm526ifkzfbwjaaqgcrmhk9zbdk2v1pyd79w4") (f (quote (("dox"))))))

(define-public crate-libgweather-4.2.0 (c (n "libgweather") (v "4.2.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "ffi") (r "^4.2.0") (d #t) (k 0) (p "gweather-sys")) (d (n "gio") (r "^0.17") (d #t) (k 0)) (d (n "glib") (r "^0.17") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0yc36fafpixkdgqi43347sl86c66knr1hy4gjyda9q4c7y78kng0") (f (quote (("dox" "glib/dox" "gio/dox"))))))

(define-public crate-libgweather-4.3.0 (c (n "libgweather") (v "4.3.0") (d (list (d (n "ffi") (r "^4.3.0") (d #t) (k 0) (p "gweather-sys")) (d (n "gio") (r "^0.18") (d #t) (k 0)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0hwsiaqn98gfqqh164h342hv1djfgdgzvp8i8n1ydd3fij8w7sih")))

(define-public crate-libgweather-4.3.1 (c (n "libgweather") (v "4.3.1") (d (list (d (n "ffi") (r "^4.3.0") (d #t) (k 0) (p "gweather-sys")) (d (n "gio") (r "^0.18") (d #t) (k 0)) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0p7acrpyagqx1czfj9f3jgds3jyjhrqbpg6npfg96y8xclyr4xsv")))

(define-public crate-libgweather-4.4.0 (c (n "libgweather") (v "4.4.0") (d (list (d (n "ffi") (r "^4.4.0") (d #t) (k 0) (p "gweather-sys")) (d (n "gio") (r "^0.19") (d #t) (k 0)) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "18rv9s6z7jgfja0b4czds6g5jpc9nm2rkq9hbjjfr346vxsggf1c")))

