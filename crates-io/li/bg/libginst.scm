(define-module (crates-io li bg libginst) #:use-module (crates-io))

(define-public crate-libginst-0.1.0 (c (n "libginst") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "1ca3qb22a15hyv0wa3cxkaa5iy9xzgh34c7pb0a797h7dasxisyj")))

(define-public crate-libginst-1.0.0 (c (n "libginst") (v "1.0.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.91") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.17") (d #t) (k 0)))) (h "01863lg0sxns415rm5askcsw197l6fvdblrq6g2495j820zx9spd")))

