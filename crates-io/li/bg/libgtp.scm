(define-module (crates-io li bg libgtp) #:use-module (crates-io))

(define-public crate-libgtp-0.1.1 (c (n "libgtp") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scrub_log") (r "^0.2.1") (d #t) (k 0)))) (h "1bm819kbgfrg5dnnw2ay9p8bcyfvpg45q44s5mvb7ln8kcp1d44v") (f (quote (("default" "controller") ("controller"))))))

(define-public crate-libgtp-0.1.2 (c (n "libgtp") (v "0.1.2") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "scrub_log") (r "^0.2.1") (d #t) (k 0)))) (h "1vmv1cr423kjkr06503l389g7r4kya7bbd4ml60l4bsh21k7azkp") (f (quote (("default" "controller") ("controller"))))))

