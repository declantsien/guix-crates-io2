(define-module (crates-io li bg libgssapi) #:use-module (crates-io))

(define-public crate-libgssapi-0.1.0 (c (n "libgssapi") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1f03xgjqamxv5binxs5cfnz1k7hqfmp4dikwgypfc40n7yqamxi9")))

(define-public crate-libgssapi-0.1.1 (c (n "libgssapi") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0a053d8vpdbj8aqgbcgzpzh7w7cvli8xd69z86xdqgbadnl2jrq4")))

(define-public crate-libgssapi-0.2.0 (c (n "libgssapi") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0cmnlbkasmpg1q6f4m4mwxsybmc1sl8dsr4kq89rp7vmhfcd7rph")))

(define-public crate-libgssapi-0.2.1 (c (n "libgssapi") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1ih51hjzfy0y56zx6vbdjr25hk3c7mr08sybgfbdpigb1b7q1ihy")))

(define-public crate-libgssapi-0.2.2 (c (n "libgssapi") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0wx0pv7rz7vhjd4cqi4i3bk7pkf1jf7swpm0mzdlpnbjl50233l0") (y #t)))

(define-public crate-libgssapi-0.3.0 (c (n "libgssapi") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "0sgpiryx1xr2c7rcpsd675zscnz5dnfd234wdn1a0y65iq8fw090")))

(define-public crate-libgssapi-0.4.0 (c (n "libgssapi") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "000n65ziwsbk2rfd1m2kl7l7knfn3b18c8l5382amc3zc3r8js0g")))

(define-public crate-libgssapi-0.4.1 (c (n "libgssapi") (v "0.4.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.10") (d #t) (k 0)))) (h "1cx2fgrmmar94bc5wx3d0lyd2l7cg0508m3wyszrprim4lxy63vl")))

(define-public crate-libgssapi-0.4.2 (c (n "libgssapi") (v "0.4.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "142aah1kv4gxrcj9y2mgq9q4wjak4ajqvwanln7a9wknca5niz0j") (f (quote (("iov") ("default" "iov"))))))

(define-public crate-libgssapi-0.4.4 (c (n "libgssapi") (v "0.4.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "031qnsjs3pqkpi5pg1q4hzdd5rv3k1kb5fc09ysaw17z0jv3h0bv") (f (quote (("iov") ("default" "iov"))))))

(define-public crate-libgssapi-0.4.5 (c (n "libgssapi") (v "0.4.5") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "05b2k0m8w1ify1ni5s4cf8yywmaarwkx80i2gzhj5fb2n24xd0sj") (f (quote (("iov") ("default" "iov"))))))

(define-public crate-libgssapi-0.4.6 (c (n "libgssapi") (v "0.4.6") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)))) (h "1cjl1rr54ykwss5434k3dxjpihhhqmxa8drmk1krrnkiz38vqkbj") (f (quote (("iov") ("default" "iov"))))))

(define-public crate-libgssapi-0.5.0 (c (n "libgssapi") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.4") (d #t) (k 0)))) (h "1qrmkqbaymc7gv2gkg9xqzkw3s2c0ccjj6hx1rk35273yil2vlwk") (f (quote (("iov") ("default" "iov"))))))

(define-public crate-libgssapi-0.5.1 (c (n "libgssapi") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.4") (d #t) (k 0)))) (h "1f62gmdkvypfr8fikqifbik2ml2xsn06pf2l0g1wvhq0h9gih6f6") (f (quote (("iov") ("default" "iov"))))))

(define-public crate-libgssapi-0.6.0 (c (n "libgssapi") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.4") (d #t) (k 0)))) (h "0sqsaifcvn6crxcv5knzsxhx9b8kv6p69zzw66z1qphxn21g9xv8") (f (quote (("iov") ("default" "iov"))))))

(define-public crate-libgssapi-0.6.1 (c (n "libgssapi") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.4") (d #t) (k 0)))) (h "0p0vv8pknh3lvz6kmrwwzaxws6i9dzry7nhqz0hlid0aqw06fc21") (f (quote (("iov") ("default" "iov"))))))

(define-public crate-libgssapi-0.6.2 (c (n "libgssapi") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.4") (d #t) (k 0)))) (h "1kibdknsb5xaamnd4ks2vqk6wq491zp8a8mq1bkcz78626j0pgxd") (f (quote (("iov") ("default" "iov"))))))

(define-public crate-libgssapi-0.6.3 (c (n "libgssapi") (v "0.6.3") (d (list (d (n "bitflags") (r "^1.3") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.2.4") (d #t) (k 0)))) (h "1xcj6mg26m364j9jy172f4dkvgh9lnpklyw1vn4bqy0zpcw115s2") (f (quote (("localname") ("iov") ("default" "iov" "localname"))))))

(define-public crate-libgssapi-0.6.4 (c (n "libgssapi") (v "0.6.4") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.3.0") (d #t) (k 0)))) (h "0pqfm5hv5wfk4f33dkb3yl99f4piqj8p8rpa8qm29z5ygkvvgkwx") (f (quote (("localname") ("iov") ("default" "iov" "localname"))))))

(define-public crate-libgssapi-0.7.0 (c (n "libgssapi") (v "0.7.0") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.3.1") (d #t) (k 0)))) (h "0fig4a2frjvysacb1wc0dipwmyj8xppa7329fwhlnllyjqq08by2") (f (quote (("s4u") ("localname") ("iov") ("default" "iov" "localname"))))))

(define-public crate-libgssapi-0.7.1 (c (n "libgssapi") (v "0.7.1") (d (list (d (n "bitflags") (r "^2.0") (d #t) (k 0)) (d (n "bytes") (r "^1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libgssapi-sys") (r "^0.3.1") (d #t) (k 0)))) (h "059a35qi8nkcwjlbv3ji9rx7vyhi45rqp4zsp6lhfmw4r4y3c0n5") (f (quote (("s4u") ("localname") ("iov") ("default" "iov" "localname"))))))

