(define-module (crates-io li bg libgpiod-sys) #:use-module (crates-io))

(define-public crate-libgpiod-sys-0.1.0 (c (n "libgpiod-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "system-deps") (r "^2.0") (d #t) (k 1)))) (h "12g43ihv8l2w6fia7h7537dax29n8inz6vkgfznsxpjjlqfjwa5a") (r "1.56")))

(define-public crate-libgpiod-sys-0.1.1 (c (n "libgpiod-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.63") (d #t) (k 1)) (d (n "system-deps") (r "^2.0") (d #t) (k 1)))) (h "1pqmz5007606nmjykgz2hgwr1my8rixxfcggfbcxggcpb5fqrshz") (f (quote (("v2_1")))) (r "1.60")))

