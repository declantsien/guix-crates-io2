(define-module (crates-io li bg libgitdit) #:use-module (crates-io))

(define-public crate-libgitdit-0.3.0 (c (n "libgitdit") (v "0.3.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0blmzk7rxkgcfs11j3w5bbn450nqjhplnrb04fn040f3z7xk35k2")))

(define-public crate-libgitdit-0.4.0 (c (n "libgitdit") (v "0.4.0") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "git2") (r "^0.6") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "08vnbiqxq4avls8swfddnjggb118hdrw1pf9hpxi6qii9fkmzk70")))

(define-public crate-libgitdit-0.5.0 (c (n "libgitdit") (v "0.5.0") (d (list (d (n "error-chain") (r "^0.12") (d #t) (k 0)) (d (n "git2") (r "^0.17") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0f28dnlmkbc6r0gqmq8f4j6vgcl0xjzcliw8xrp3yvwx4vs12dnd")))

