(define-module (crates-io li bg libghost) #:use-module (crates-io))

(define-public crate-libghost-0.1.0 (c (n "libghost") (v "0.1.0") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "pros") (r "^0.8.0") (d #t) (k 0)))) (h "1c8qh5y5abgs5xak1p304y01nkm34wpqm69g0x8m37ps0w5x86h9")))

(define-public crate-libghost-0.1.1 (c (n "libghost") (v "0.1.1") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "pros") (r "^0.8.0") (d #t) (k 0)))) (h "0ab1vy5vsy2ifc44qkkq6h231lhcqwwhi9m8d9dpb8kc40vrnd6k")))

(define-public crate-libghost-0.2.1 (c (n "libghost") (v "0.2.1") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "pros") (r "^0.8.0") (d #t) (k 0)))) (h "1sa7ypaai8aphp93iiqkwcwx8kvafas5ffbcnpbcv0lhr66659rj")))

(define-public crate-libghost-0.2.2 (c (n "libghost") (v "0.2.2") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "pros") (r "^0.8.0") (d #t) (k 0)))) (h "05f4k3kca6fk8zsid6k5d01pa5pxz3q5m3lsqyr3akfkzxz5n819")))

(define-public crate-libghost-0.2.3 (c (n "libghost") (v "0.2.3") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "pros") (r "^0.8.0") (d #t) (k 0)))) (h "05vz574bg80xgd9mz1vrcs5j37h7bms0rz9rgkz4wk511hrkwff1")))

(define-public crate-libghost-0.3.3 (c (n "libghost") (v "0.3.3") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "pros") (r "^0.8.0") (d #t) (k 0)))) (h "1a9b1j8kgib67sgc5959rq43360rw140aiap908s0y06j6qphjfv")))

(define-public crate-libghost-0.4.3 (c (n "libghost") (v "0.4.3") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "pros") (r "^0.8.0") (d #t) (k 0)))) (h "1rcc8rv354c2acn54kcv284hnzywfsbjg9bwm0wyq0yigvv498gr")))

(define-public crate-libghost-0.4.4 (c (n "libghost") (v "0.4.4") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "pros") (r "^0.8.0") (d #t) (k 0)))) (h "15d2fg6x63zhi361zr7yjlgrbjy7z4cl65zb0qihs88vqq515kni")))

(define-public crate-libghost-0.4.5 (c (n "libghost") (v "0.4.5") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "pros") (r "^0.8.0") (d #t) (k 0)))) (h "0gnkr7kjbd6vnjkjs645l0wjjaz4dvhr9035br5mx1wqxjma1kqp")))

(define-public crate-libghost-0.4.6 (c (n "libghost") (v "0.4.6") (d (list (d (n "libm") (r "^0.2.8") (d #t) (k 0)) (d (n "pros") (r "^0.8.0") (d #t) (k 0)))) (h "16w8v0fjxalrhp9iv4pwd1bazw1skrpj5hwpqm5wzknbzjzjbb3x")))

