(define-module (crates-io li bg libgo) #:use-module (crates-io))

(define-public crate-libgo-0.1.0 (c (n "libgo") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1kz001rj5vvnm8sdkppvkfxjd109xl6gpqvrnz6b5j6mnxhld9lr")))

(define-public crate-libgo-0.2.0 (c (n "libgo") (v "0.2.0") (d (list (d (n "clap") (r "^2.19") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0g2fdsqrm5zcnp865w2fgcx7vi0vs8w4d1pk6xrzvgz8c1mg3rqk")))

(define-public crate-libgo-0.4.0 (c (n "libgo") (v "0.4.0") (d (list (d (n "clap") (r "^2.19") (d #t) (k 2)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1ia68zjcmcyapc4iyl3cqspmc4gjsc30myz29m4awfyy85af1m6v")))

