(define-module (crates-io li bg libgmssl-sys) #:use-module (crates-io))

(define-public crate-libgmssl-sys-0.1.0 (c (n "libgmssl-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "11064miz10k6cafyqgq4jnrqm5aar0g16f650mciyx40d7visizv") (y #t)))

(define-public crate-libgmssl-sys-0.2.0 (c (n "libgmssl-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0iwzhvgp97zkgfp57yrzq62xpcvvibmk5pwrwz6q7kwrwz7w4gq1") (y #t)))

(define-public crate-libgmssl-sys-0.2.1 (c (n "libgmssl-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "11cidh9v89k87qg26qxwzhxg3s4q9x28ciyxbywxcqrviijcfl21") (y #t)))

(define-public crate-libgmssl-sys-3.1.0 (c (n "libgmssl-sys") (v "3.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0gxsrlahlfshn770qv8dcrplffvm3581vnvrsa9yw4x0i52d30nq") (y #t)))

(define-public crate-libgmssl-sys-3.1.0-alpha (c (n "libgmssl-sys") (v "3.1.0-alpha") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "08wvq45z8063vxnk3asc57s4svb36rhsy08mjf4z03a1zbz06pg2") (f (quote (("ENABLE_TLS_DEBUG") ("ENABLE_SM4_AESNI_AVX") ("ENABLE_SM3_AVX_BMI2") ("ENABLE_SM2_PRIVATE_KEY_EXPORT") ("ENABLE_SM2_EXTS") ("ENABLE_SM2_ALGOR_ID_ENCODE_NULL") ("ENABLE_RDRND") ("ENABLE_HTTP_TESTS") ("ENABLE_GMT_0105_RNG") ("ENABLE_BROKEN_CRYPTO"))))))

