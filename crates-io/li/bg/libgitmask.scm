(define-module (crates-io li bg libgitmask) #:use-module (crates-io))

(define-public crate-libgitmask-0.1.0 (c (n "libgitmask") (v "0.1.0") (d (list (d (n "barnacl") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rusqlite") (r "*") (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "tempdir") (r "*") (d #t) (k 2)) (d (n "vergen") (r "*") (d #t) (k 1)))) (h "1yc4055n5dijl021jmbnmwwh1w99nsq0akq6zpw4wmhi4d1hfrzs")))

