(define-module (crates-io li bg libgphoto2_sys) #:use-module (crates-io))

(define-public crate-libgphoto2_sys-2.5.10 (c (n "libgphoto2_sys") (v "2.5.10") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "0ir6dn8li6bb5dscq4k4rw80c4731iy85pz8fznp92lj8n20rid6") (y #t) (l "gphoto2")))

(define-public crate-libgphoto2_sys-1.0.0 (c (n "libgphoto2_sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "18q5ygvm4knlf15b8k8qh6kkpd8v43giw8jq69q6pm4cghlgjcga") (l "gphoto2")))

(define-public crate-libgphoto2_sys-1.0.1 (c (n "libgphoto2_sys") (v "1.0.1") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "165h4in6293j1lw9l1xh9bbjg5fn5z769lbv29i7l7a11jizd3qh") (l "gphoto2")))

(define-public crate-libgphoto2_sys-1.1.0 (c (n "libgphoto2_sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "1cq9vj6syswhwdqv9m2df630rmz7jkz9xy12qq578p14p1g1kkln") (l "gphoto2")))

(define-public crate-libgphoto2_sys-1.2.0 (c (n "libgphoto2_sys") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)) (d (n "gphoto2_test") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "gphoto2_test") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "1alj5b8k8jkqa84jpddsfpbsb1d0krjx3hcv52wfy1fn7csy4saj") (f (quote (("test" "gphoto2_test")))) (l "gphoto2")))

(define-public crate-libgphoto2_sys-1.2.2 (c (n "libgphoto2_sys") (v "1.2.2") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "gphoto2_test") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "gphoto2_test") (r "^1.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.25") (d #t) (k 1)))) (h "0g0gy4p5400x5yrkyx5sbm80b8kz4f4y8hw8gl2jz6myrv64zmid") (f (quote (("test" "gphoto2_test")))) (l "gphoto2")))

