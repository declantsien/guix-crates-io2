(define-module (crates-io li bg libgatekeeper-sys) #:use-module (crates-io))

(define-public crate-libgatekeeper-sys-0.1.0 (c (n "libgatekeeper-sys") (v "0.1.0") (h "0gmw90m7bn9psbsjb9g207m0d4b7kpnwsp871fiw0c6a7xl0828k")))

(define-public crate-libgatekeeper-sys-0.2.0 (c (n "libgatekeeper-sys") (v "0.2.0") (h "0jda46101kkk3i68qh6v4gxyhl0g1cy3d8ghqxx94pnccsd9f21l")))

(define-public crate-libgatekeeper-sys-0.2.1 (c (n "libgatekeeper-sys") (v "0.2.1") (h "0d4l0wp47aj39wxzh0nxc2f5zr8zx5kzkbgs49n8d9bbsfcaxihy")))

(define-public crate-libgatekeeper-sys-0.3.0 (c (n "libgatekeeper-sys") (v "0.3.0") (h "1hld0krb42zpp7y82nxj0lzalwnv7r5byywacsr0jrivxha9z83i")))

(define-public crate-libgatekeeper-sys-0.4.0 (c (n "libgatekeeper-sys") (v "0.4.0") (d (list (d (n "apdu-core") (r "^0.3.0") (f (quote ("longer_payloads"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "088z0c6i1vi803rrfbzs2h8v35vaj9wfb1qhcjhqd7bqw24h1d7n")))

(define-public crate-libgatekeeper-sys-0.5.0 (c (n "libgatekeeper-sys") (v "0.5.0") (d (list (d (n "apdu-core") (r "^0.3.0") (f (quote ("longer_payloads"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1zr8q4pl4g14s9qa3h0pj035ifz02fabc2pwvjaaqyyyqbgksdmc")))

(define-public crate-libgatekeeper-sys-0.6.0 (c (n "libgatekeeper-sys") (v "0.6.0") (d (list (d (n "apdu-core") (r "^0.3.0") (f (quote ("longer_payloads"))) (d #t) (k 0)) (d (n "openssl") (r "^0.10.41") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "0w8c4b17cdq65aa4285nm8yj7xpw9piwl77c9xd8ry5glffadsf7")))

