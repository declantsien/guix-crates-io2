(define-module (crates-io li bg libgrep-rs) #:use-module (crates-io))

(define-public crate-libgrep-rs-0.1.0 (c (n "libgrep-rs") (v "0.1.0") (h "0sqa1gr35bnplz4mxaxpry6b8ywhjl8kq5bik338iscghshjxw5k")))

(define-public crate-libgrep-rs-0.1.1 (c (n "libgrep-rs") (v "0.1.1") (h "15q7dqldkv7bhgkk107kmnlg835pcvf9072qydkphzslxxnnig4g")))

(define-public crate-libgrep-rs-0.1.2 (c (n "libgrep-rs") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0lx9jy67dafmlklj85mwqs9l1fnfb9vv1bpdwdvbrlpp89gzbfa0")))

(define-public crate-libgrep-rs-0.1.3 (c (n "libgrep-rs") (v "0.1.3") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0b2gb98a9xf2q66mcb08ay7rh4vw191a3ccssbsq8pkmvcdqcrkh")))

(define-public crate-libgrep-rs-0.1.4 (c (n "libgrep-rs") (v "0.1.4") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "16na3qs2bm2sk5499zinsrq9ma1820j37x86nnd3ykrgm8ai5njz")))

