(define-module (crates-io li bg libgitbox) #:use-module (crates-io))

(define-public crate-libgitbox-0.1.0 (c (n "libgitbox") (v "0.1.0") (d (list (d (n "curl") (r "*") (o #t) (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "mush") (r "*") (o #t) (d #t) (k 0)) (d (n "rustc-serialize") (r "*") (d #t) (k 0)) (d (n "sha1") (r "*") (d #t) (k 0)) (d (n "vergen") (r "*") (d #t) (k 1)))) (h "1n8fkn511d9pyc5qs6s7ni8a88zpraz1m0nwyf2wly7sxikr48ii") (f (quote (("ssh" "mush") ("file") ("default" "file" "ssh") ("artifactory" "curl"))))))

