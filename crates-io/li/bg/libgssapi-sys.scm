(define-module (crates-io li bg libgssapi-sys) #:use-module (crates-io))

(define-public crate-libgssapi-sys-0.1.0 (c (n "libgssapi-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "133qki6xqxdz3rxzvsg3y91jsywxg64j7b1h3l65iv1ggbgpkhdk") (l "gssapi")))

(define-public crate-libgssapi-sys-0.1.1 (c (n "libgssapi-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "1ildrjxgx54z4nf30r67xldrs0bziiw90jd18s5wkq0mix9k3z5q") (l "gssapi_krb5")))

(define-public crate-libgssapi-sys-0.1.2 (c (n "libgssapi-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "0nirg2zarlh3ixqmf423ghdk59kjj7s65y4b4jwwla9d40m0xnqw") (l "gssapi_krb5")))

(define-public crate-libgssapi-sys-0.2.0 (c (n "libgssapi-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "0x1l9x3c95rf2w3n2vc252h7blg213dv7wvx5nxq7fq7g7dp9lcd") (l "gssapi_krb5")))

(define-public crate-libgssapi-sys-0.2.1 (c (n "libgssapi-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)))) (h "1fsfgyl5nf61cab4pq5ish40i5345br8p3ii2a4l98yvgyvw0frk") (l "gssapi_krb5")))

(define-public crate-libgssapi-sys-0.2.2 (c (n "libgssapi-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "15iy0yja68lwymyb86bsdy1hvlfcd4wrd6mc9lmj5h5viw85nlx9") (l "gssapi_krb5")))

(define-public crate-libgssapi-sys-0.2.3 (c (n "libgssapi-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.55") (d #t) (k 1)))) (h "108qypgiv6vcvcisachdvfr26dmgk9mrnjc6358fisdn64b8insh") (l "gssapi_krb5")))

(define-public crate-libgssapi-sys-0.2.4 (c (n "libgssapi-sys") (v "0.2.4") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "1dqg8nq6zyby33zh59ywsy4gvn6hf69hgzw1kmn9z24w81gddmqx") (l "gssapi_krb5")))

(define-public crate-libgssapi-sys-0.3.0 (c (n "libgssapi-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.64") (d #t) (k 1)))) (h "1yybg12d5a6ihx4b50srw87wd351jgykc9pz846ji9isj8qxvp7g") (l "gssapi_krb5")))

(define-public crate-libgssapi-sys-0.3.1 (c (n "libgssapi-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0wjkahghah0647s0zp0d3y1hjllvb36bp78ip6qm7v3lqxqrlzdm") (l "gssapi_krb5")))

