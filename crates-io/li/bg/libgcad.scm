(define-module (crates-io li bg libgcad) #:use-module (crates-io))

(define-public crate-libgcad-0.2.0 (c (n "libgcad") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.62") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gcad_proc_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)) (d (n "pest") (r "^2.3.0") (f (quote ("const_prec_climber"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.0") (d #t) (k 0)))) (h "0j5xnglx1p0nab4r4mg8h8x8crcfsk4k8c0190j8yx5r3xf58qb5")))

(define-public crate-libgcad-0.2.1 (c (n "libgcad") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.62") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gcad_proc_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)) (d (n "pest") (r "^2.3.0") (f (quote ("const_prec_climber"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.0") (d #t) (k 0)))) (h "0zbgq45hjb0gl23bdz0k8ws3zgnl6rbbm1a0xshifh5pyxjs9vrl")))

(define-public crate-libgcad-0.2.2 (c (n "libgcad") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.62") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "clap") (r "^3.2.17") (f (quote ("derive"))) (d #t) (k 0)) (d (n "gcad_proc_macros") (r "^0.2.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.31.1") (d #t) (k 0)) (d (n "pest") (r "^2.3.0") (f (quote ("const_prec_climber"))) (d #t) (k 0)) (d (n "pest_derive") (r "^2.3.0") (d #t) (k 0)))) (h "1kjads8disk8gzdvl2k9b464prpb0k7vm7yxhpq1r94njj44yrvg")))

(define-public crate-libgcad-0.3.0 (c (n "libgcad") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.70") (f (quote ("backtrace"))) (d #t) (k 0)) (d (n "gcad_proc_macros") (r "^0.3.0") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "pest") (r "^2.5.7") (d #t) (k 0)) (d (n "pest_derive") (r "^2.5.7") (d #t) (k 0)))) (h "1bs26x2q450l98qkqpw8yk61yfn8pbpc17vazcq96la1s9x09cy7")))

