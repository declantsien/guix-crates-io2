(define-module (crates-io li bg libgunslinger-sys) #:use-module (crates-io))

(define-public crate-libgunslinger-sys-0.1.0 (c (n "libgunslinger-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "07cpr2am5ifq02ykm0kjsnmvidx2alwhnkkr3f50hllyjadl1m11") (y #t)))

(define-public crate-libgunslinger-sys-0.1.1 (c (n "libgunslinger-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "1fkymsldwgcp9lhqnllqxhw7vdmvxrm614gzh3qcwsfrj9jj0m58") (y #t)))

(define-public crate-libgunslinger-sys-0.1.2 (c (n "libgunslinger-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0lpqx3r1sf4lvl8v3l2832nq7x9jgfjq3hszjfxx8inyi93paxsw") (y #t)))

(define-public crate-libgunslinger-sys-0.1.3 (c (n "libgunslinger-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)))) (h "0qsgmqzb7p4259ilf7m2r69v2ds1fiw0sq58l6vzwq20qjai4abw") (y #t)))

(define-public crate-libgunslinger-sys-0.1.4 (c (n "libgunslinger-sys") (v "0.1.4") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "05i8j75j7pgyv3nw4d9pj5xhc2jrqs342l28y2qlylai832dkxhx") (y #t)))

(define-public crate-libgunslinger-sys-0.1.5 (c (n "libgunslinger-sys") (v "0.1.5") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "06z1vfp96mc6qhy5m4hwljhr10gg19bln6248i9158c031kdsk82") (y #t)))

(define-public crate-libgunslinger-sys-0.1.5-alpha (c (n "libgunslinger-sys") (v "0.1.5-alpha") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "1zqhf7zf9c7i92k1l8dggxwmakqa6yr5zgrimpgra1zz7gjmixi9") (y #t)))

(define-public crate-libgunslinger-sys-0.1.6 (c (n "libgunslinger-sys") (v "0.1.6") (d (list (d (n "bindgen") (r "^0.60.1") (d #t) (k 1)))) (h "073am9vkwh2x22jlj26r7gxapg9pq518fpkmcif8dvjcgqk879j0") (y #t)))

