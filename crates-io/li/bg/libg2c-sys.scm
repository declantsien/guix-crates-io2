(define-module (crates-io li bg libg2c-sys) #:use-module (crates-io))

(define-public crate-libg2c-sys-0.1.0 (c (n "libg2c-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "007m1n0ksb8rclm3jqbjmki3ic2sql3c59z4njfajlzs4qbvlah4")))

