(define-module (crates-io li ti liting) #:use-module (crates-io))

(define-public crate-liting-0.1.0 (c (n "liting") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "inquire") (r "^0.7.4") (d #t) (k 0)))) (h "0idi6vc7vlklgba7kagxc8ha7mac7arcj1ap9bqw0f2xqjgxv0kf")))

(define-public crate-liting-0.2.0 (c (n "liting") (v "0.2.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.8") (d #t) (k 0)) (d (n "dirs") (r "^5.0.1") (d #t) (k 0)) (d (n "inquire") (r "^0.7.4") (d #t) (k 0)) (d (n "toml_edit") (r "^0.22.9") (d #t) (k 0)))) (h "1kq5cz3qycskydx9csms8932qfqkwx91xa29735hxd0n8azr9rci")))

