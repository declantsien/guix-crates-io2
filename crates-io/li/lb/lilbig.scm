(define-module (crates-io li lb lilbig) #:use-module (crates-io))

(define-public crate-lilbig-0.1.0-alpha (c (n "lilbig") (v "0.1.0-alpha") (h "1p27vrwvfq2gpq1a0i6ixxbfraaihq6jbqsdcay9hn5las298dlr")))

(define-public crate-lilbig-0.1.1-alpha (c (n "lilbig") (v "0.1.1-alpha") (h "0a80pcjaiz1szyxi3f34ks3qf9xq1rqlgpm2kxbr6238ddfvb9g8")))

(define-public crate-lilbig-0.1.2-alpha (c (n "lilbig") (v "0.1.2-alpha") (h "0qxh1dzazagxfk6ar7s97ldhjvvskzz1n7fxdy9wbdxj1nyqvidq")))

(define-public crate-lilbig-0.2.0-alpha (c (n "lilbig") (v "0.2.0-alpha") (h "1a5qzihb487sqaff2mwrhm9b9448rrakvs25z6yhqb7ramr9r4sg")))

(define-public crate-lilbig-0.2.0-alpha.0 (c (n "lilbig") (v "0.2.0-alpha.0") (h "16781zxxcjksgrvshjgfbxjv41lg1phhgvhjclg5b72ba18bwbfs")))

(define-public crate-lilbig-0.2.0 (c (n "lilbig") (v "0.2.0") (h "0n0lv0wcafmwfhry8j5r37jdcx62p1ajaiai3hh688y5amsjzxld")))

(define-public crate-lilbig-0.2.1 (c (n "lilbig") (v "0.2.1") (h "1c22afwvha9snlmj50myvihqqz5jcbb3zsvgacdvhcpnr0clflk4")))

