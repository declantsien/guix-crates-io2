(define-module (crates-io li ni linicon-ffi) #:use-module (crates-io))

(define-public crate-linicon-ffi-0.1.1 (c (n "linicon-ffi") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "cbindgen") (r "^0.13") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "linicon") (r "^0.1") (d #t) (k 0)))) (h "10bipysyvjwh0kgy3a83ny1n7jxqxy4b0rv71y9m4i7mykvgr1gm") (f (quote (("default" "cbindgen"))))))

