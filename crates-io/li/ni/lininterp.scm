(define-module (crates-io li ni lininterp) #:use-module (crates-io))

(define-public crate-lininterp-0.1.0 (c (n "lininterp") (v "0.1.0") (h "1nif4qxzr5bq6mkn0s21xpnihvyscsvlix1s768p7pdxhlmzafqg")))

(define-public crate-lininterp-0.1.1 (c (n "lininterp") (v "0.1.1") (h "1q0j54cn87mpwj464n9vngxpmv0cwqhjaikffcv15aqa6l1xwa70")))

(define-public crate-lininterp-0.1.2 (c (n "lininterp") (v "0.1.2") (h "1zvns60d2x70wiqqacxad4wnvl891vc3bdxlv1kzw0fmzshn17yn")))

(define-public crate-lininterp-0.1.3 (c (n "lininterp") (v "0.1.3") (h "1i0pcmjba57nrqjy2jhpb7xidb36i4jfkgybx4l1w5br1irdnmca")))

