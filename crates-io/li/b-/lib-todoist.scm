(define-module (crates-io li b- lib-todoist) #:use-module (crates-io))

(define-public crate-lib-todoist-0.1.0 (c (n "lib-todoist") (v "0.1.0") (d (list (d (n "chrono_elapsed") (r "^0.1.0") (d #t) (k 0)) (d (n "korero") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)))) (h "1i7qj8ylwzj792qlc5m1hrnxjhql0hvkj66jp462kyprak4psqsh")))

(define-public crate-lib-todoist-0.1.1 (c (n "lib-todoist") (v "0.1.1") (d (list (d (n "chrono_elapsed") (r "^0.1.0") (d #t) (k 0)) (d (n "korero") (r "^0.1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.7") (d #t) (k 0)))) (h "1r5fga82czl1p9brg0wvrnjpvq4zai4p2dk5m90bndhyq6c5yshl")))

