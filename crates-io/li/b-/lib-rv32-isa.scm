(define-module (crates-io li b- lib-rv32-isa) #:use-module (crates-io))

(define-public crate-lib-rv32-isa-0.1.1 (c (n "lib-rv32-isa") (v "0.1.1") (d (list (d (n "lib-rv32-common") (r "0.2.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "003n8czca3k12drhnsljc9cnmp0misq807grrybf4y6rp8hrcaf9")))

(define-public crate-lib-rv32-isa-0.2.0 (c (n "lib-rv32-isa") (v "0.2.0") (d (list (d (n "lib-rv32-common") (r "0.2.*") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "0gk470j8ra4hcqp2j7s82c26fsa4fnxj15ghynpnb6a8c870jzy5")))

