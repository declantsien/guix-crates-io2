(define-module (crates-io li b- lib-test) #:use-module (crates-io))

(define-public crate-lib-test-0.1.0 (c (n "lib-test") (v "0.1.0") (h "1zrp6khs6vxk09pl6v252mhlm1kh316qdlq4k3ljb1rw4403riv5")))

(define-public crate-lib-test-0.2.0 (c (n "lib-test") (v "0.2.0") (h "0ag554qyh7vlwwvc11b6hnb37xyjbw4y795mjg7hk8100si3vynq")))

