(define-module (crates-io li b- lib-g29) #:use-module (crates-io))

(define-public crate-lib-g29-1.0.0 (c (n "lib-g29") (v "1.0.0") (d (list (d (n "hidapi") (r "^2.6.1") (f (quote ("macos-shared-device" "windows-native"))) (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "0zr5sp6i8m1awwxhg8fd5a8n0a50c7mcmvndq0g36yysmhh1h5gr")))

(define-public crate-lib-g29-1.0.1 (c (n "lib-g29") (v "1.0.1") (d (list (d (n "hidapi") (r "^2.6.1") (f (quote ("macos-shared-device" "windows-native"))) (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)))) (h "162xfpgyrn8nkafgpc6kynx2xblvgn5av98x0lp6jb26slazfdld")))

