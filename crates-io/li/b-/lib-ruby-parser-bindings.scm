(define-module (crates-io li b- lib-ruby-parser-bindings) #:use-module (crates-io))

(define-public crate-lib-ruby-parser-bindings-0.1.0 (c (n "lib-ruby-parser-bindings") (v "0.1.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">0.0.1") (d #t) (k 0)))) (h "1qjzga4cak1nizz7ipgzr1rxgmr4f8igkv8fybb52g30slhxy8gn")))

(define-public crate-lib-ruby-parser-bindings-0.2.0 (c (n "lib-ruby-parser-bindings") (v "0.2.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">0.0.1") (d #t) (k 0)))) (h "143a97q8idwdhs3408qdqv8ffb1cml7j2wyssbprrhzy2m202cc2")))

(define-public crate-lib-ruby-parser-bindings-0.3.0 (c (n "lib-ruby-parser-bindings") (v "0.3.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">0.0.1") (d #t) (k 0)))) (h "0fkpkp9q8c7cj3d8bbl05fv0jr7bgpnl0gjyw96c7h4mghc6hi68")))

(define-public crate-lib-ruby-parser-bindings-0.4.0 (c (n "lib-ruby-parser-bindings") (v "0.4.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">0.0.1") (d #t) (k 0)))) (h "0rc0pn93gvnswb7x7s1fah2s60c4vbgrhd9bvaz0ax3npn88gc20")))

(define-public crate-lib-ruby-parser-bindings-0.5.0 (c (n "lib-ruby-parser-bindings") (v "0.5.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">0.0.1") (d #t) (k 0)))) (h "0w75pnicrjd93s6hwmhkzn2p48b19qqwkpi4gq9jij0pv1synj82")))

(define-public crate-lib-ruby-parser-bindings-0.6.0 (c (n "lib-ruby-parser-bindings") (v "0.6.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">0.0.1") (d #t) (k 0)))) (h "0m6yn5y0hky71g75v5ffs8jva6qqiydrbf2cghq5lagal71kk81f")))

(define-public crate-lib-ruby-parser-bindings-0.7.0 (c (n "lib-ruby-parser-bindings") (v "0.7.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">0.0.1") (d #t) (k 0)))) (h "13wdcxam287yx5sx4x8cyxnhj8c4x7ifk81c81h00jnvx9xh1xp5")))

(define-public crate-lib-ruby-parser-bindings-0.8.0 (c (n "lib-ruby-parser-bindings") (v "0.8.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">0.0.1") (d #t) (k 0)))) (h "1lnzfdwb9ng3k08l6nkkjjqigngy4s9v7046jkxymq09qz48633w")))

(define-public crate-lib-ruby-parser-bindings-0.9.0 (c (n "lib-ruby-parser-bindings") (v "0.9.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">0.0.1") (d #t) (k 0)))) (h "0mjv18yc5bfb7wkf0wiy1mdnn6yk4qvmbdwscd1alnb5gkxf9dgq")))

(define-public crate-lib-ruby-parser-bindings-0.10.0 (c (n "lib-ruby-parser-bindings") (v "0.10.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">0.0.1") (d #t) (k 0)))) (h "1w5clzzvw90nsq0zppnz19zp1rnqx5kdajk4migsmmbn40925v35")))

(define-public crate-lib-ruby-parser-bindings-0.11.0 (c (n "lib-ruby-parser-bindings") (v "0.11.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">=0.30.0") (d #t) (k 0)))) (h "1qfchmaxir68ica8gnkfzj54di6csjfbkl5mllzg2ky17sn9c5sv")))

(define-public crate-lib-ruby-parser-bindings-0.12.0 (c (n "lib-ruby-parser-bindings") (v "0.12.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">=0.35.0") (d #t) (k 0)))) (h "0afyqcxfk0wiwcjs3syas058spzsw6aczjhlsi9643irmmnyh2fc")))

(define-public crate-lib-ruby-parser-bindings-0.13.0 (c (n "lib-ruby-parser-bindings") (v "0.13.0") (d (list (d (n "lib-ruby-parser-nodes") (r ">=0.36.0") (d #t) (k 0)))) (h "1nr5ffppq9sb0mblq3468whpyins8xp19sgqvhwbf8h3j60v6inx")))

