(define-module (crates-io li b- lib-base64) #:use-module (crates-io))

(define-public crate-lib-base64-1.0.0 (c (n "lib-base64") (v "1.0.0") (h "0d8m4frkpvdk7kng0f9jsvs3127x1c4h2bf2w63k3hjz437wrykm")))

(define-public crate-lib-base64-1.0.1 (c (n "lib-base64") (v "1.0.1") (h "058qc1l616z5yh22kgj1jqikswq5cr1bc92miic3n4j0dmv23mdf")))

(define-public crate-lib-base64-1.0.2 (c (n "lib-base64") (v "1.0.2") (h "12br8d4ipv8nhg1jq0kkfz7dsc5c18wvl24gqb757xhm8rdaaacg")))

(define-public crate-lib-base64-1.0.3 (c (n "lib-base64") (v "1.0.3") (h "1v217fsnsgv4l8ks7lhc50n4s54ga2aqzsckyip7cd4dyx4071rc")))

(define-public crate-lib-base64-2.0.0 (c (n "lib-base64") (v "2.0.0") (h "17j40agrj9404zanmgcbb727992fjgnc33nwqbh1dwasc6yil55g")))

(define-public crate-lib-base64-2.0.2 (c (n "lib-base64") (v "2.0.2") (h "1zw15abbsax9h1yg3v9p760pb2mspx3yw39dh646ixki56yax27q")))

(define-public crate-lib-base64-2.0.3 (c (n "lib-base64") (v "2.0.3") (h "174ksfgn1vq4l996xpx6lq7n71iavw29c6ircrlb744hzsyf7aia")))

(define-public crate-lib-base64-2.0.4 (c (n "lib-base64") (v "2.0.4") (h "13csnrmn3ix8gx87w5phkaynrqmys5wb293525qkp32wqzs0wa77")))

(define-public crate-lib-base64-2.1.0 (c (n "lib-base64") (v "2.1.0") (h "0bkb9mwpdb0b50y3cmz7m4d5d5vb7cg41mah2dggaigv4177w41c")))

