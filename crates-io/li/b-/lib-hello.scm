(define-module (crates-io li b- lib-hello) #:use-module (crates-io))

(define-public crate-lib-hello-0.1.0 (c (n "lib-hello") (v "0.1.0") (h "11ag4kwvcyz8hxdddn7slzpw73dc2dc6idwwg55pij1nan6d1scq")))

(define-public crate-lib-hello-0.2.0 (c (n "lib-hello") (v "0.2.0") (h "0nmabrnf1qf2pl3jkmyigs35cpla0zqw39z6ykqsl662mw128a5s")))

(define-public crate-lib-hello-0.3.0 (c (n "lib-hello") (v "0.3.0") (h "0p6gi8ffds4c78nj3vmsp4wl45il3jgcn9109wm76a67dk2k9yxh") (f (quote (("greeting") ("farewell") ("default" "communication") ("communication" "greeting" "farewell"))))))

