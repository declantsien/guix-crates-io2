(define-module (crates-io li b- lib-rust-sasl) #:use-module (crates-io))

(define-public crate-lib-rust-sasl-0.1.0 (c (n "lib-rust-sasl") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1gw0bnc1i4nb71rqcym4mhq0fc333dwgyhsdgcn3jiyva1y9kibw") (y #t)))

