(define-module (crates-io li b- lib-did) #:use-module (crates-io))

(define-public crate-lib-did-0.1.0 (c (n "lib-did") (v "0.1.0") (d (list (d (n "bip39") (r "^1.0.1") (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "libsm") (r "^0.4.1") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "rand") (r "^0.6.5") (d #t) (k 0)))) (h "0k30lb34b067zr5kg65rsjvkvbdprj3v7vcapbny9yxb5rysrk2s")))

