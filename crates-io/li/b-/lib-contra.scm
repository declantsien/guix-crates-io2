(define-module (crates-io li b- lib-contra) #:use-module (crates-io))

(define-public crate-lib-contra-1.0.0 (c (n "lib-contra") (v "1.0.0") (h "0wc21rjvnkw3rbg523xpxj8inlcbamfhcbqn61fqa95lznnsv14a")))

(define-public crate-lib-contra-2.0.1 (c (n "lib-contra") (v "2.0.1") (h "116g0r6k3xadxr8lqdkcivm6k4gcqp0hy283lx7wcz53ard5bp8x")))

(define-public crate-lib-contra-2.0.3 (c (n "lib-contra") (v "2.0.3") (h "1pg78zg8x06w78di1km8p819xzm4ilrg50h9ragp9pd1b22mj8dp")))

(define-public crate-lib-contra-2.0.4 (c (n "lib-contra") (v "2.0.4") (h "0j2pqhzi4ydbqyfznpcxv6gawlvz20afspky0m7adaf8dynk32v1")))

(define-public crate-lib-contra-2.0.5 (c (n "lib-contra") (v "2.0.5") (h "0g6k6gp7vv7fhddsny8rkppixvh6app2cbdhaqqbgacpipjj41g9")))

(define-public crate-lib-contra-2.0.6 (c (n "lib-contra") (v "2.0.6") (h "1yzhj5n1a7v51wxr5lfpiqmk874wk2nbj4d6ha4zgdl34kyk0rhm")))

(define-public crate-lib-contra-2.0.7 (c (n "lib-contra") (v "2.0.7") (h "0xvpp06rzd00kni807zlj558ghyw7x6bgngnln47d5p7hc5vxiy5")))

(define-public crate-lib-contra-2.0.8 (c (n "lib-contra") (v "2.0.8") (h "1n77chn4lhas57ryrs5fr8pb6q2gda6yfgcccw275x0f491xi5gv")))

(define-public crate-lib-contra-2.0.9 (c (n "lib-contra") (v "2.0.9") (h "14w8vwh8arr583qg9n4isc6vaa47vpcsphk2lliph7r1b7a165hn")))

(define-public crate-lib-contra-2.0.11 (c (n "lib-contra") (v "2.0.11") (h "06x2x01wqbaki72gypzigpi3xhjwq8n5v9l9x6qnfnkdik426gwh")))

(define-public crate-lib-contra-2.1.1 (c (n "lib-contra") (v "2.1.1") (h "14wqpzcg13navibgj7hz7xrxgh4s0kaqc6jahqwrjddwwcmlk6n1")))

(define-public crate-lib-contra-4.0.0 (c (n "lib-contra") (v "4.0.0") (h "0clj6zsbxyjn95isnamhhrq48dky7h9vmxn84rg4wnk8mfryjsjb")))

(define-public crate-lib-contra-4.2.0 (c (n "lib-contra") (v "4.2.0") (h "165gvxzaf7mw98c30y91bgaqpxbgn1bczzrz6b9fhlhkj7bk8n4y")))

(define-public crate-lib-contra-4.8.0 (c (n "lib-contra") (v "4.8.0") (h "1nir79kzqfzazsiwgfzanmhl58j1n41vvcs1mazmyjj9d1wlrwha")))

(define-public crate-lib-contra-4.9.0 (c (n "lib-contra") (v "4.9.0") (h "1a5w5xyk75wcwyjqyq40533jr7gpkm6z8av4hqzqhs88v945ij3l")))

(define-public crate-lib-contra-4.10.0 (c (n "lib-contra") (v "4.10.0") (h "01lxj5wxdqvbzq7cglj4qq5nlmahzy8c9pnz0pp1xdcgpsw0xzd6")))

(define-public crate-lib-contra-4.11.0 (c (n "lib-contra") (v "4.11.0") (h "0hqckxq3dxk356fvv7p27zd4v8s3wnv03wxh7lfxjv07357wmrp1")))

(define-public crate-lib-contra-4.12.0 (c (n "lib-contra") (v "4.12.0") (h "1jwral8jd0sy72bxfnf6nc6ysq5144iz23spjakgx7s985a2pdpb")))

(define-public crate-lib-contra-4.13.0 (c (n "lib-contra") (v "4.13.0") (h "1szlwli3rvx30dhssvclb493ar0czjq2vpf00iwfswhb4jl95i3z")))

(define-public crate-lib-contra-4.13.1 (c (n "lib-contra") (v "4.13.1") (h "1f8wa0545f5jjskv33i2m1pwr3vvyi40abwzq7qpq5ii4m8zrdld")))

(define-public crate-lib-contra-4.13.2 (c (n "lib-contra") (v "4.13.2") (h "1bym50d6bf8mang5nxpd23rrca6jsksb414hv67p3blpk02ada4m")))

(define-public crate-lib-contra-4.13.3 (c (n "lib-contra") (v "4.13.3") (h "1nfdhr73vxm7lpy1rzi3bpc801f91z5c38mjqx4kmzqb33r09sbl")))

(define-public crate-lib-contra-4.13.4 (c (n "lib-contra") (v "4.13.4") (h "0xnn3gm7q6whwnd0hm1krvhlmhqgqzikp8mh1gdii6wm17s48ymb")))

(define-public crate-lib-contra-4.13.5 (c (n "lib-contra") (v "4.13.5") (h "0xfhmj732ribrkj44b3h28r7qv094w04llq70c54hn4pw4p9ss1l")))

(define-public crate-lib-contra-4.14.0 (c (n "lib-contra") (v "4.14.0") (h "09ak5vwknxrmvlsiya6g4l9q2ryxbfn9d9b9cmc607schqg955rw")))

(define-public crate-lib-contra-4.15.0 (c (n "lib-contra") (v "4.15.0") (h "18k7iflszznp73p4ba6nfyz6cfiv2rkbjva3gp1svdbxdxnp7clk")))

(define-public crate-lib-contra-5.0.0 (c (n "lib-contra") (v "5.0.0") (h "1lfj5a65d0xbk2mwyxnb6rrd2dx7hy5j3w2k0si7sf35vc55izrg")))

(define-public crate-lib-contra-5.0.1 (c (n "lib-contra") (v "5.0.1") (h "0237fpy4pmz5pfw3nqwyazijkifqf0gvsja3594hrvr9c7i4bav8")))

(define-public crate-lib-contra-5.0.2 (c (n "lib-contra") (v "5.0.2") (h "0s2mc9q7nwnq58d6xminc757809jdf4hk1ibcllrwz9c5xfh1c2j")))

