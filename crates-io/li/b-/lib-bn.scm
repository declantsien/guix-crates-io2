(define-module (crates-io li b- lib-bn) #:use-module (crates-io))

(define-public crate-lib-bn-0.4.4 (c (n "lib-bn") (v "0.4.4") (d (list (d (n "bincode") (r "^0.6") (f (quote ("rustc-serialize"))) (k 2)) (d (n "byteorder") (r "^1.0") (f (quote ("i128"))) (d #t) (k 0)) (d (n "crunchy") (r "^0.2.1") (d #t) (k 0)) (d (n "rand") (r "^0.5") (f (quote ("i128_support"))) (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "166gplljzpfv894grvhk60lyhkdfrsjdkmav5kfra0my9wg3wqhm") (f (quote (("default" "rustc-serialize"))))))

