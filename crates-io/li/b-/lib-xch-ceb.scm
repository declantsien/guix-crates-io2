(define-module (crates-io li b- lib-xch-ceb) #:use-module (crates-io))

(define-public crate-lib-xch-ceb-0.1.0 (c (n "lib-xch-ceb") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1d7zl4dv7hflrx34199kzm87dk94jjncpy9l39i3wnfwzk23bjsa") (y #t)))

(define-public crate-lib-xch-ceb-0.1.1 (c (n "lib-xch-ceb") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1nwmb6slrfska1maik46k6hz9dsjhbg2v6s9230imvy23460km9y") (y #t)))

