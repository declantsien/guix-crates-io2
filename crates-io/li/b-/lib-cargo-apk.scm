(define-module (crates-io li b- lib-cargo-apk) #:use-module (crates-io))

(define-public crate-lib-cargo-apk-0.5.0 (c (n "lib-cargo-apk") (v "0.5.0") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "cargo") (r "^0.41.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "multimap") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "1w0c1589dhysyysnmyck179p8pyk6sp68sqqh582fsjv1qj9f0v3")))

(define-public crate-lib-cargo-apk-0.5.1 (c (n "lib-cargo-apk") (v "0.5.1") (d (list (d (n "assert_cmd") (r "^0.12.0") (d #t) (k 2)) (d (n "cargo") (r "^0.41.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "itertools") (r "^0.8.2") (d #t) (k 0)) (d (n "multimap") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.104") (d #t) (k 0)) (d (n "toml") (r "^0.5.5") (d #t) (k 0)))) (h "057v9lvvgg0l7gdjxchk5yzkcbnbjaznlcgqp3hq7gb4ia3xbnsm")))

