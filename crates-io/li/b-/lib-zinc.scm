(define-module (crates-io li b- lib-zinc) #:use-module (crates-io))

(define-public crate-lib-zinc-0.1.0 (c (n "lib-zinc") (v "0.1.0") (h "0km39xbw68kxdadx03sy42n5ghglx3f6h49x1qbq30p3xbzlrhrg")))

(define-public crate-lib-zinc-0.1.1 (c (n "lib-zinc") (v "0.1.1") (h "1kibvvfrkrlwamiy32dlfpsvkzj85s2sxdxb1hpf2hqrin7y3sjb")))

