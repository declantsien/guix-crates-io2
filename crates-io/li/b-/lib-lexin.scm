(define-module (crates-io li b- lib-lexin) #:use-module (crates-io))

(define-public crate-lib-lexin-0.1.0 (c (n "lib-lexin") (v "0.1.0") (h "1s20xkspmb74amprwwbsjpimyyzfwszl26ffwrjg6w1hjczcvm6g")))

(define-public crate-lib-lexin-0.1.1 (c (n "lib-lexin") (v "0.1.1") (h "0f7xgs3q6f5qyqjxdsyppm03dcfw3p8blicia0hggjycal14as1q")))

(define-public crate-lib-lexin-0.1.2 (c (n "lib-lexin") (v "0.1.2") (h "1ys98gxbs4qrsf6mv5ckbiibzpnn8xx6s6fxxj6gs7fyydwacja1")))

(define-public crate-lib-lexin-0.2.0 (c (n "lib-lexin") (v "0.2.0") (h "1iayk77vy8awahdgs6drm5h039pqgzwmfg1hfcmrf2jfbwkiznjr")))

(define-public crate-lib-lexin-0.2.1 (c (n "lib-lexin") (v "0.2.1") (h "04qfs9ygfrh4jxrrbaq3lqdz7v5l85m6c44n1i7xysmg95ziz857")))

(define-public crate-lib-lexin-0.2.2 (c (n "lib-lexin") (v "0.2.2") (h "1z1fpz42vs4wc0srw4ml649wiwfvgdb0ccv76jlqf2xl4q0pij96")))

(define-public crate-lib-lexin-0.2.3 (c (n "lib-lexin") (v "0.2.3") (h "0asvs4hv2j6vnv22sx029gmf3jbj2vjawhhjk59sxky1gqbxvvgc")))

(define-public crate-lib-lexin-0.3.0 (c (n "lib-lexin") (v "0.3.0") (h "1bjwddj4na3b0yprj3jln87xkvwnkck21ngl0vs601gazl4snwm8")))

(define-public crate-lib-lexin-0.3.1 (c (n "lib-lexin") (v "0.3.1") (h "1gsb2ymcb5ha3gv9lnxci2x6fi5y1cj48zbx0nybr25fh1ri8lkl")))

(define-public crate-lib-lexin-0.3.2 (c (n "lib-lexin") (v "0.3.2") (h "0744glvvqahyhn8szlsqd1b2bxskf0l13zh9pnkvj4w6dlillqc9")))

(define-public crate-lib-lexin-0.3.3 (c (n "lib-lexin") (v "0.3.3") (h "1c6ay9lhr0a3iqzmg40r2z7c3b0dpq1daf8lvl6z0xvsl8wxr38y")))

(define-public crate-lib-lexin-0.3.4 (c (n "lib-lexin") (v "0.3.4") (h "1758dysjc3dfld33n20l0vi770m40999zyazkcpx1zr4ziggzfc0")))

(define-public crate-lib-lexin-0.3.5 (c (n "lib-lexin") (v "0.3.5") (h "1vhgkz3hinf10zkzpyfkfx49nn04g9jrmar6a8wsfz6g3s2821rk")))

(define-public crate-lib-lexin-0.3.6 (c (n "lib-lexin") (v "0.3.6") (h "1zj4mcpa50jkmjy65yf1jy2crkkclq7sjc0zmkww3ydjlrm0k1si")))

