(define-module (crates-io li b- lib-ruby-parser-nodes) #:use-module (crates-io))

(define-public crate-lib-ruby-parser-nodes-0.1.0 (c (n "lib-ruby-parser-nodes") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "1zlz91r1hl33xpb11fvpwrc0ss2gkdbm1c2xr02sgbj4ca6n9aia")))

(define-public crate-lib-ruby-parser-nodes-0.2.0 (c (n "lib-ruby-parser-nodes") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "13r605dz9gj73k3hp3hbzdyrd3lrpdylqm8ndid2mij7ck5r3gjk")))

(define-public crate-lib-ruby-parser-nodes-0.3.0 (c (n "lib-ruby-parser-nodes") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "18q6m75f9pgkgw789a9ycljh5kfzy1r3hcgn3r9rrsv3gckbf1r2")))

(define-public crate-lib-ruby-parser-nodes-0.4.0 (c (n "lib-ruby-parser-nodes") (v "0.4.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "1y2vka02lbbykdjdcknx4zd5l6yp035sxv2kkm9mnyssckxgg7c7")))

(define-public crate-lib-ruby-parser-nodes-0.5.0 (c (n "lib-ruby-parser-nodes") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "03x5zgdfj5rcj4a22rsjs56xlsca0a5hpcsi1plskcpzsn9l35v2")))

(define-public crate-lib-ruby-parser-nodes-0.6.0 (c (n "lib-ruby-parser-nodes") (v "0.6.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "19pdk08iqh4cw46g7ccfp4x5klwgk3n6s9r422b6nvp0zjx5q42r")))

(define-public crate-lib-ruby-parser-nodes-0.7.0 (c (n "lib-ruby-parser-nodes") (v "0.7.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "0mbaxziv11z5yy1xdjq3i0cd76nqx55pxf67chaxnk4pf3yfan7y")))

(define-public crate-lib-ruby-parser-nodes-0.8.0 (c (n "lib-ruby-parser-nodes") (v "0.8.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "0hai17j9v4hcj6hllgaqq5852xc7jlgd9557ri7xpdahx9hgpsr7")))

(define-public crate-lib-ruby-parser-nodes-0.9.0 (c (n "lib-ruby-parser-nodes") (v "0.9.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "1xqv6fs8rl64cfijf5ydm8qcv2yn4anmc8xwlfqags453srikrfb")))

(define-public crate-lib-ruby-parser-nodes-0.10.0 (c (n "lib-ruby-parser-nodes") (v "0.10.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "0afjrj89ix18bqc5gsmlii9jahi1py9p5k6fmix2gh06q8lckkgh")))

(define-public crate-lib-ruby-parser-nodes-0.11.0 (c (n "lib-ruby-parser-nodes") (v "0.11.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "0xqnx5x4l435w7d88agx1bshdry21xznrxij0y8xbxbqak8zvv76")))

(define-public crate-lib-ruby-parser-nodes-0.12.0 (c (n "lib-ruby-parser-nodes") (v "0.12.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "1kwdx4vkwra09ixliydbnzzb1r5ff12ph0vwp1s6lzw1cxl3nv9x")))

(define-public crate-lib-ruby-parser-nodes-0.13.0 (c (n "lib-ruby-parser-nodes") (v "0.13.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.13") (d #t) (k 0)))) (h "0a5fn9ps2grfilxsdbbkwa6pxjrgggm2x8j8icdnml5h57lsln9f")))

(define-public crate-lib-ruby-parser-nodes-0.14.0 (c (n "lib-ruby-parser-nodes") (v "0.14.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1gcc5n4c4bpmzy8ka20i8zvq6kjgynq1hkpxlcfw80llv0294y7x")))

(define-public crate-lib-ruby-parser-nodes-0.15.0 (c (n "lib-ruby-parser-nodes") (v "0.15.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1kwqi79ggpkps5vf8vcl8kcpmz2nkmjf4z96pdq7bkwiik05iy33")))

(define-public crate-lib-ruby-parser-nodes-0.16.0 (c (n "lib-ruby-parser-nodes") (v "0.16.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "00kn0107w4qg7yn0yh9wc84232w621zczh203n460qn6q7gq3hv9")))

(define-public crate-lib-ruby-parser-nodes-0.17.0 (c (n "lib-ruby-parser-nodes") (v "0.17.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0xi93biyzmr8mmrynyfinbdz6bh39zb03bbd766mnvv0f6if3s30")))

(define-public crate-lib-ruby-parser-nodes-0.18.0 (c (n "lib-ruby-parser-nodes") (v "0.18.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "1m9g6mnkqv1vnx366d334fd8krvjxp4d64r46i6kbr07yx3jlcs3")))

(define-public crate-lib-ruby-parser-nodes-0.19.0 (c (n "lib-ruby-parser-nodes") (v "0.19.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)))) (h "0l1yfpdp9zq8yxnxpca7rwyc1bpqjpqhc8lkhn8ga18qd47hwlpy")))

(define-public crate-lib-ruby-parser-nodes-0.20.0 (c (n "lib-ruby-parser-nodes") (v "0.20.0") (h "0h5m8y5mf59ic1pkfcr8vpri4xvfz51zk2y3q2r34n2z5mms8cm5")))

(define-public crate-lib-ruby-parser-nodes-0.21.0 (c (n "lib-ruby-parser-nodes") (v "0.21.0") (h "1zi4jyflsrd30wafv939w7ficfbv0gm69vahpzay454ccbxcrvi8")))

(define-public crate-lib-ruby-parser-nodes-0.22.0 (c (n "lib-ruby-parser-nodes") (v "0.22.0") (h "1whzjkjgb6wcvfa4sm9rp0hvh46708di9l9zkkyhwn96j3c1idfd")))

(define-public crate-lib-ruby-parser-nodes-0.23.0 (c (n "lib-ruby-parser-nodes") (v "0.23.0") (h "06vgc56gwpylf86df6vy6dr916mf8s9fcf94qn4q73v79pqyk8mb")))

(define-public crate-lib-ruby-parser-nodes-0.24.0 (c (n "lib-ruby-parser-nodes") (v "0.24.0") (h "0nqvp7638lh80nhfw1h75gvh973ib0b43xsclqc74v135pxiaxlz")))

(define-public crate-lib-ruby-parser-nodes-0.25.0 (c (n "lib-ruby-parser-nodes") (v "0.25.0") (h "0qs5lhh8lkbfpylkdhsa3pizvb2n9irw47l5y70q5jddcw8yry5d")))

(define-public crate-lib-ruby-parser-nodes-0.26.0 (c (n "lib-ruby-parser-nodes") (v "0.26.0") (h "0xpjmbcjacl4dzvm73cwpfp061fmg94vqqlabpwr0zh4gvjai217")))

(define-public crate-lib-ruby-parser-nodes-0.27.0 (c (n "lib-ruby-parser-nodes") (v "0.27.0") (h "0gv3wfifj5s1p9fmr55cb77yy621zc0kjyw94s5f1phc5v6cr8cb")))

(define-public crate-lib-ruby-parser-nodes-0.28.0 (c (n "lib-ruby-parser-nodes") (v "0.28.0") (h "07516zsw7csn5ps51sipqdx5987fdb15ylxv57nf4615hncs8b6g")))

(define-public crate-lib-ruby-parser-nodes-0.29.0 (c (n "lib-ruby-parser-nodes") (v "0.29.0") (h "0wyhycg14j18s5yx157p42f4hrr41hq2vlyy34p8dwq5v9mizs9s")))

(define-public crate-lib-ruby-parser-nodes-0.30.0 (c (n "lib-ruby-parser-nodes") (v "0.30.0") (h "1h7ykb71294fjddr3gjbcp8qbimskl40hlq3q8pvd30729my9a5h")))

(define-public crate-lib-ruby-parser-nodes-0.31.0 (c (n "lib-ruby-parser-nodes") (v "0.31.0") (h "0ij52fdx51l302b597fpjkbysdkkihm6jng9jgjqmzklag6asfp7")))

(define-public crate-lib-ruby-parser-nodes-0.32.0 (c (n "lib-ruby-parser-nodes") (v "0.32.0") (h "1jcin5dc2m10nc6qiagp7djlyzhjp8jpzliqsr73jrp60xxxjcb8")))

(define-public crate-lib-ruby-parser-nodes-0.33.0 (c (n "lib-ruby-parser-nodes") (v "0.33.0") (h "11xnw96m5ckjfj150nmrs5yxhbxw953rv8xi7kgnq3qn7yvj0ad0")))

(define-public crate-lib-ruby-parser-nodes-0.34.0 (c (n "lib-ruby-parser-nodes") (v "0.34.0") (h "146a4v9zajhyhhnwk7piyh6b2papf0zq59vn12gfqpdip5rj552m")))

(define-public crate-lib-ruby-parser-nodes-0.35.0 (c (n "lib-ruby-parser-nodes") (v "0.35.0") (h "1k2l9nsk1fzbpl9z91kb7ljjx1vbz2xszp66h0v9xr2hbp63749j")))

(define-public crate-lib-ruby-parser-nodes-0.36.0 (c (n "lib-ruby-parser-nodes") (v "0.36.0") (h "0m3nhgkfc9ihaxnad7x8ipr2aivspiwf2zpcjpn7nc99c1bifrbg")))

(define-public crate-lib-ruby-parser-nodes-0.37.0 (c (n "lib-ruby-parser-nodes") (v "0.37.0") (h "0bnq05sfvkiww9faf5mzlid30dzbc644wnjiir97fmdmdskhnc2l")))

(define-public crate-lib-ruby-parser-nodes-0.38.0 (c (n "lib-ruby-parser-nodes") (v "0.38.0") (h "08nrjnjqvhxdk2y7p876b6x8cfcqm68iz44z1vm3gm15jfr4rf9b")))

(define-public crate-lib-ruby-parser-nodes-0.39.0 (c (n "lib-ruby-parser-nodes") (v "0.39.0") (h "1gxiig5vkx9wa6h2pwv7nc05cm978kdv349nsxy9dwb7sc80j4ga")))

(define-public crate-lib-ruby-parser-nodes-0.40.0 (c (n "lib-ruby-parser-nodes") (v "0.40.0") (h "1x3bpw3av38i1aw3wfazxg4fjhqm49v0w36fc4dpk34wmsdby01j")))

(define-public crate-lib-ruby-parser-nodes-0.41.0 (c (n "lib-ruby-parser-nodes") (v "0.41.0") (h "0hgk4d4vqvc5whwjjjq5pl9r9q5sk6sg3zfcpg69mbsmsgr891fg")))

(define-public crate-lib-ruby-parser-nodes-0.42.0 (c (n "lib-ruby-parser-nodes") (v "0.42.0") (h "16b8b7j2smh81lzh8h2iw4qxypp61lwm85yjd4gr4vgm813ihrkx")))

(define-public crate-lib-ruby-parser-nodes-0.43.0 (c (n "lib-ruby-parser-nodes") (v "0.43.0") (h "1bjgjshd2gq2cjg3l3zndpbdg6p6lwy0vlpa1r14kjw3v00gfxay")))

(define-public crate-lib-ruby-parser-nodes-0.44.0 (c (n "lib-ruby-parser-nodes") (v "0.44.0") (h "0xrhnl70b253jgxg64r3mrzfisdz97039nwpbg4nragasd47fsj2")))

(define-public crate-lib-ruby-parser-nodes-0.45.0 (c (n "lib-ruby-parser-nodes") (v "0.45.0") (h "1z84dvygq4w9n8dn7n5w585xkbll20m2xrbmksr9y990fwr5mv5n")))

(define-public crate-lib-ruby-parser-nodes-0.46.0 (c (n "lib-ruby-parser-nodes") (v "0.46.0") (h "1sgyrcvmiigiadzi2mffxjp6dip4xbnpr7dm8g6lpdk2y58cc8f0")))

(define-public crate-lib-ruby-parser-nodes-0.47.0 (c (n "lib-ruby-parser-nodes") (v "0.47.0") (h "0is024sym8k4888d16vfnnkla159xr3bqbi0n5hsfyw5zwn7fgka")))

(define-public crate-lib-ruby-parser-nodes-0.48.0 (c (n "lib-ruby-parser-nodes") (v "0.48.0") (d (list (d (n "liquid") (r "^0.26.0") (d #t) (k 0)) (d (n "liquid-core") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "07xb44qyy4q0nq7a4484g4al7sz011wcjfkq67l74zblhx6g96l0")))

(define-public crate-lib-ruby-parser-nodes-0.49.0 (c (n "lib-ruby-parser-nodes") (v "0.49.0") (d (list (d (n "liquid") (r "^0.26.0") (d #t) (k 0)) (d (n "liquid-core") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "10hrwjp6ffm4fdxhiqzzzjbrl8vwlznxw2cq51a6a6f4s2k87k8g")))

(define-public crate-lib-ruby-parser-nodes-0.50.0 (c (n "lib-ruby-parser-nodes") (v "0.50.0") (d (list (d (n "liquid") (r "^0.26.0") (d #t) (k 0)) (d (n "liquid-core") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1db960wvjc6c7qiz6cpyy14b21x5szqi3mqlyjswkqj4855p4lzh")))

(define-public crate-lib-ruby-parser-nodes-0.52.0 (c (n "lib-ruby-parser-nodes") (v "0.52.0") (d (list (d (n "liquid") (r "^0.26.0") (d #t) (k 0)) (d (n "liquid-core") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0b6n3i4h7ak09xdfg3cgpaxgh2arrcw61xf7c70qr3w6ccmva4wr")))

(define-public crate-lib-ruby-parser-nodes-0.53.0 (c (n "lib-ruby-parser-nodes") (v "0.53.0") (d (list (d (n "liquid") (r "^0.26.0") (d #t) (k 0)) (d (n "liquid-core") (r "^0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1clivda4fl6znrdszccjzykjfn6wn2nz4k8vpf63zaai00jzj1h0")))

(define-public crate-lib-ruby-parser-nodes-0.54.0 (c (n "lib-ruby-parser-nodes") (v "0.54.0") (d (list (d (n "liquid") (r "^0.26.1") (d #t) (k 0)) (d (n "liquid-core") (r "^0.26.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xxjhqi66hsp3l3g888h2n6vx1dry1vih6s2qp9z030zqxlfiy51")))

(define-public crate-lib-ruby-parser-nodes-0.55.0 (c (n "lib-ruby-parser-nodes") (v "0.55.0") (d (list (d (n "liquid") (r "=0.26.0") (d #t) (k 0)) (d (n "liquid-core") (r "=0.26.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0a1j7ri6402w07znzwaaynipxipw7cgfn14vfjjzj06r4k5hh6l7")))

(define-public crate-lib-ruby-parser-nodes-0.57.0 (c (n "lib-ruby-parser-nodes") (v "0.57.0") (d (list (d (n "liquid") (r "=0.26") (d #t) (k 0)) (d (n "liquid-core") (r "=0.26") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0vvhwrs5sqsc6wnjm1xpgda00bdri14sz88pprzb52cjjgvah1n8")))

