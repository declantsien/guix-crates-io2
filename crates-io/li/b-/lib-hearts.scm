(define-module (crates-io li b- lib-hearts) #:use-module (crates-io))

(define-public crate-lib-hearts-0.1.0 (c (n "lib-hearts") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0zq0wdjgl8ihyhqrd0z1ww9dcdycv2xs5jwc7rxx4syhvp5bjgpm")))

(define-public crate-lib-hearts-0.1.2 (c (n "lib-hearts") (v "0.1.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1inm8wvlzkzdwmjcxqqnqfzkl2aww6k0mpzh5kna9rlf6vi3r22n")))

(define-public crate-lib-hearts-0.1.3 (c (n "lib-hearts") (v "0.1.3") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)))) (h "1kgg3cb99ng9m96lcwl8zixi1iad2qqyxn9x1yby5k8vwmynbsk2")))

(define-public crate-lib-hearts-0.1.5 (c (n "lib-hearts") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "05jc6m6f1l6l24zgs7x23yxr8pbw900a4frbsfijrqdny6q1lfn4")))

(define-public crate-lib-hearts-0.1.6 (c (n "lib-hearts") (v "0.1.6") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "1610s3yq1mfsmhc73rcz7i6040add06x6ipx780j17n1xs8sivhf")))

(define-public crate-lib-hearts-0.1.7 (c (n "lib-hearts") (v "0.1.7") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "0282hzspajnx3kw2spnmgss4gvbkfnqgcannyng5zvxa9pbif1q9")))

(define-public crate-lib-hearts-0.1.8 (c (n "lib-hearts") (v "0.1.8") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "1bx7ds5kpq24ym98qhyp0jmy8gk2y4clmirxh1sfp7a6jn7f08fq")))

(define-public crate-lib-hearts-0.1.9 (c (n "lib-hearts") (v "0.1.9") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "0wnr3s3vhfn5w8q4pscmy1g0lf7rvkzc6gdrmcss4nij0ha0yick")))

(define-public crate-lib-hearts-0.1.10 (c (n "lib-hearts") (v "0.1.10") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "1vjk83dy4hxv3cm6jcw78kcrb1y85ywd14cs699np2mzhn5nhm1b")))

(define-public crate-lib-hearts-0.1.11 (c (n "lib-hearts") (v "0.1.11") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "13vnjlag89d9c0mh9ypixpvhwwf206811y19mzd41qyl9kbn8w1q")))

(define-public crate-lib-hearts-0.1.12 (c (n "lib-hearts") (v "0.1.12") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "1v0izr1y8l02rqkmmsnsjc8s7m9ffchp5qc5abwfd0w6nbrp136k")))

(define-public crate-lib-hearts-0.1.13 (c (n "lib-hearts") (v "0.1.13") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "0i5ixipy9szcpc9fqz7zh0rrl14zp9ycpcsfj9lkdqciwhmjnjx9")))

(define-public crate-lib-hearts-0.1.14 (c (n "lib-hearts") (v "0.1.14") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.174") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (d #t) (k 2)))) (h "1dp93vb5v4s8hkhghlsmqlanawcpkw75c68q1lgwvrra7b1ckf9a")))

(define-public crate-lib-hearts-0.1.15 (c (n "lib-hearts") (v "0.1.15") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "1da8x70dw3zxjys3qqlci4nz7qwi9h7hc5ygdawik8y9bwzbnpcs")))

(define-public crate-lib-hearts-0.1.16 (c (n "lib-hearts") (v "0.1.16") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "15ddz2ix16px4cs2xvc90db1yn42r36jkfphwqyj1vdgb4fhsip8")))

(define-public crate-lib-hearts-0.1.17 (c (n "lib-hearts") (v "0.1.17") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "0rxarq024b1s1xvr1njvd2sp1rg4138bpzvxkmbzaxdykgk3ij2p")))

(define-public crate-lib-hearts-0.1.18 (c (n "lib-hearts") (v "0.1.18") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)))) (h "0019iyq5zcz2h4j16zljr0rlfsfcwa43qmjbsd8qsyz748pf07i8")))

(define-public crate-lib-hearts-0.1.19 (c (n "lib-hearts") (v "0.1.19") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.177") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1nj7z362y7yc2xjzcwpwnsv58l4vlq9vf8yv7yzb3j0d8ic0w3v7")))

(define-public crate-lib-hearts-0.1.20 (c (n "lib-hearts") (v "0.1.20") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1g6zlfx11845028gw0036n4zffmmqq38pj9rzk502nih005fypn2")))

(define-public crate-lib-hearts-0.1.21 (c (n "lib-hearts") (v "0.1.21") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.181") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.104") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0g6qqkza01xzv9sw77lg0w8drygy0kpdziq3mgx0hjxv2fmmckkx")))

(define-public crate-lib-hearts-0.1.22 (c (n "lib-hearts") (v "0.1.22") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.183") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "0d1s8cyarazdn0a0i0s12p2n4rpkaznjvbxk5v6qin1q9gsj8dxf")))

(define-public crate-lib-hearts-0.1.23 (c (n "lib-hearts") (v "0.1.23") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1siy6vkf998mjvkd60jf6idvjggm03pvkv96xdgx83ky9lf84cpz")))

(define-public crate-lib-hearts-0.1.24 (c (n "lib-hearts") (v "0.1.24") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1idnlh0lff4d5fz2fj7axi7mg7hnnagw6q2yni8sw4yksgz69hy7")))

(define-public crate-lib-hearts-0.1.25 (c (n "lib-hearts") (v "0.1.25") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "00xz5rjcwyhi4szl4pm7n7fp0inwi6ir6h2v32vvfb11srvs6ccl")))

(define-public crate-lib-hearts-0.1.26 (c (n "lib-hearts") (v "0.1.26") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1nzjp4zzvaj0n4x95lwxkhs090acpgfagmb7kp02850drqzi5h0r")))

(define-public crate-lib-hearts-0.1.27 (c (n "lib-hearts") (v "0.1.27") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1yfz1xzafbq6dfd9dqq2h94xhz55gf77j7k8m2rqs916nra55z3n")))

(define-public crate-lib-hearts-0.1.28 (c (n "lib-hearts") (v "0.1.28") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "serde") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.185") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 2)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("v4"))) (d #t) (k 2)))) (h "1bzds11hcw3xdgvk9yq2qqx2qh6m8f95y6pbggzjin7n6vibzq5z")))

