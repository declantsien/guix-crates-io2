(define-module (crates-io li b- lib-bspwm) #:use-module (crates-io))

(define-public crate-lib-bspwm-0.1.0 (c (n "lib-bspwm") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "1nf426ihgjqa4i5rr8wcq6j6vhxn6qsaz6dfyb71szp4fk9pybqh")))

(define-public crate-lib-bspwm-0.1.1 (c (n "lib-bspwm") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0h0d952spszdmbfz0sq8pqhbzjl4qlz8vk7fjrhx3hy5qnxhrgb8")))

