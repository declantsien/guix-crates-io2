(define-module (crates-io li b- lib-rv32-mcu) #:use-module (crates-io))

(define-public crate-lib-rv32-mcu-0.2.0 (c (n "lib-rv32-mcu") (v "0.2.0") (d (list (d (n "glob") (r "0.3.*") (d #t) (k 2)) (d (n "lib-rv32-isa") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1rmc4rwvadg4fn1ig22fbs9hkyc2lrp4bx0qfq7nrjpxj3fxhw4r")))

(define-public crate-lib-rv32-mcu-0.2.1 (c (n "lib-rv32-mcu") (v "0.2.1") (d (list (d (n "glob") (r "0.3.*") (d #t) (k 2)) (d (n "lib-rv32-isa") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "0.4.*") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "1.0.*") (d #t) (k 2)) (d (n "wasm-bindgen") (r "^0.2") (f (quote ("serde-serialize"))) (d #t) (k 0)))) (h "1x7flyc2gmbb087awnbkiivfwzjvmq7gybx5bk30y2zawn4p35vq")))

