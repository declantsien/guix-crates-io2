(define-module (crates-io li b- lib-ruby-parser-ast) #:use-module (crates-io))

(define-public crate-lib-ruby-parser-ast-0.55.0 (c (n "lib-ruby-parser-ast") (v "0.55.0") (d (list (d (n "lib-ruby-parser-nodes") (r "^0.55.0") (o #t) (d #t) (k 1)))) (h "13158pfqwcgjw08b863gk0p7gf197r2lq53yz6h0gdbj5r3lh6a6") (f (quote (("default") ("codegen" "lib-ruby-parser-nodes"))))))

