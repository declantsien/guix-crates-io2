(define-module (crates-io li b- lib-messenger-archive) #:use-module (crates-io))

(define-public crate-lib-messenger-archive-0.1.0 (c (n "lib-messenger-archive") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "html-escape") (r "^0.2.13") (d #t) (k 0)) (d (n "xml-rs") (r "^0.8.20") (d #t) (k 0)))) (h "1zwkjm1lyi5w5879fc8gvpcbpiax6rjxbsq6c9xwaxvrb113vgs8")))

