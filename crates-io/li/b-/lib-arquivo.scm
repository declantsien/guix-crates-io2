(define-module (crates-io li b- lib-arquivo) #:use-module (crates-io))

(define-public crate-lib-arquivo-0.1.0 (c (n "lib-arquivo") (v "0.1.0") (h "1z19h6p58nm2yw41jxkg0jv9nh6x6f570kavkc9s7dgzpnbnh4yq") (r "1.59.0")))

(define-public crate-lib-arquivo-0.1.1 (c (n "lib-arquivo") (v "0.1.1") (h "0qqvvpzk2x1vsc2dkvvlv68wl482cdcqqaamaymyhm1ccdmvp18k") (r "1.59.0")))

