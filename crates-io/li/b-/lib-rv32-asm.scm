(define-module (crates-io li b- lib-rv32-asm) #:use-module (crates-io))

(define-public crate-lib-rv32-asm-0.2.0 (c (n "lib-rv32-asm") (v "0.2.0") (d (list (d (n "lib-rv32-common") (r "0.2.*") (d #t) (k 0)) (d (n "lib-rv32-isa") (r "0.2.*") (d #t) (k 2)) (d (n "log") (r "0.4.*") (d #t) (k 0)))) (h "0bki5iijswvdj546wf34q0sg536fij23slkw2vkrb8lz4yk03yfc")))

