(define-module (crates-io li b- lib-resp) #:use-module (crates-io))

(define-public crate-lib-resp-0.1.0 (c (n "lib-resp") (v "0.1.0") (d (list (d (n "nom") (r "^4.0.0-beta2") (d #t) (k 0)))) (h "0d4v5xnki8mld85m5y1n2kq8wd5gmsgcnxg6dpbni1w2n5k92anh")))

(define-public crate-lib-resp-0.1.1 (c (n "lib-resp") (v "0.1.1") (d (list (d (n "nom") (r "^4") (d #t) (k 0)))) (h "1y6y6466vqq20pmk4dfk38gdyfy94c1w1s3nbj1m202q18jkay18")))

