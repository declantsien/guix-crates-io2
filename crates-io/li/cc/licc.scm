(define-module (crates-io li cc licc) #:use-module (crates-io))

(define-public crate-licc-0.1.0 (c (n "licc") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "1z4jd3qrwqwl751qb297aqvjwl1mmpdrkih21pcpfqklgrxfszi2") (f (quote (("readonly") ("default"))))))

(define-public crate-licc-0.2.0 (c (n "licc") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.23") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.112") (d #t) (k 0)) (d (n "tokio") (r "^1.11.0") (f (quote ("macros"))) (d #t) (k 2)))) (h "1p1ig0ngqwlapm32mnfsfnql94318g161p064ddkbvdjjfqr8am9") (f (quote (("write") ("default"))))))

