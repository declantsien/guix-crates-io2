(define-module (crates-io li _s li_stephens_py_hkhan) #:use-module (crates-io))

(define-public crate-li_stephens_py_hkhan-0.1.0 (c (n "li_stephens_py_hkhan") (v "0.1.0") (d (list (d (n "li-stephens") (r "^0.1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.15") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0qf9nvb6y1vx2kbnp6s310pakrqml9nxpsxm0fij5pmc3yaasmz7")))

(define-public crate-li_stephens_py_hkhan-0.1.1 (c (n "li_stephens_py_hkhan") (v "0.1.1") (d (list (d (n "li-stephens") (r "^0.1.0") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.4") (d #t) (k 0)) (d (n "numpy") (r "^0.15") (d #t) (k 0)) (d (n "pyo3") (r "^0.15.1") (f (quote ("extension-module"))) (d #t) (k 0)))) (h "0wmijhklbrh4qy1935c2jds4lx3n6c6i28zx56khpxm1md42876d")))

