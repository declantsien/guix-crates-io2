(define-module (crates-io li bt libtempest) #:use-module (crates-io))

(define-public crate-libtempest-0.1.0 (c (n "libtempest") (v "0.1.0") (d (list (d (n "clap") (r "^3") (f (quote ("derive" "wrap_help"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1md72gvq1x72r92rsrgz818cp8d2bncdlg4scxwr1xq9dhkxr150")))

