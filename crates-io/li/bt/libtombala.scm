(define-module (crates-io li bt libtombala) #:use-module (crates-io))

(define-public crate-libtombala-2.0.0 (c (n "libtombala") (v "2.0.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "12an3yh1zgbqb2ih5080r13xijzwcd2cy8zrng8f40phcw0ns7vs")))

(define-public crate-libtombala-2.1.0 (c (n "libtombala") (v "2.1.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ikrjm6xwawf8vkrrb2lj1ficypln93mzvgj4fg70zdmza66m2sl")))

(define-public crate-libtombala-2.1.1 (c (n "libtombala") (v "2.1.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "167akfhfjrg6vjlwrkchwwa21i0nq1c2j8n2gpq77ww8yg21wxa4")))

(define-public crate-libtombala-2.2.0 (c (n "libtombala") (v "2.2.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xbbdragldmng61i4qamxwbgaq59lqi5646r80ycc0hfq603ska6")))

(define-public crate-libtombala-2.3.0 (c (n "libtombala") (v "2.3.0") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1nhwqs9xgpaqwkargy957pbxnz8adn5g7hcxipvc7rx8z2sv0w3v")))

(define-public crate-libtombala-2.3.1 (c (n "libtombala") (v "2.3.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0ncjnz8xg9dqcs3blybicm2sv03xqmpmkmflqab7d7yg1jxpbl4j")))

(define-public crate-libtombala-3.0.0 (c (n "libtombala") (v "3.0.0") (d (list (d (n "fastrand") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1b1sh147wp9vfbiw2kgz5b0ml4q93vmfbvyfk3rifg3sl0x381g7") (f (quote (("default") ("card_generator" "fastrand"))))))

(define-public crate-libtombala-3.1.0 (c (n "libtombala") (v "3.1.0") (d (list (d (n "fastrand") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "05apzbwgqpbkgl3b3izc41rq5pdh5ai5r9bqm6wb35xp0k663axs") (f (quote (("default") ("card_generator" "fastrand"))))))

(define-public crate-libtombala-3.1.1 (c (n "libtombala") (v "3.1.1") (d (list (d (n "fastrand") (r "^2.0.0") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1rh9nkkh9bp9n9jfy2ly4c4176a21pbqyxl0llbl3a4zqf6dvp2d") (f (quote (("default") ("card_generator" "fastrand"))))))

(define-public crate-libtombala-3.2.0 (c (n "libtombala") (v "3.2.0") (d (list (d (n "fastrand") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1vc3wc9wi7pvhh3ssrgc97n0hswra6g9ghsrxlyrccys0zxfsqbb") (f (quote (("default") ("card_generator" "fastrand"))))))

(define-public crate-libtombala-3.2.1 (c (n "libtombala") (v "3.2.1") (d (list (d (n "fastrand") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.197") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "18inxmvqhlziypildh889bh40h7h7s9jac6n7md72nl5dg4syx17") (f (quote (("default") ("card_generator" "fastrand"))))))

(define-public crate-libtombala-3.2.2 (c (n "libtombala") (v "3.2.2") (d (list (d (n "fastrand") (r "^2.0.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "14smk5ixqiwlw82rzfysi7lai4nxs5fxsl887bdwh9xbcfibd7lw") (f (quote (("default") ("card_generator" "fastrand"))))))

