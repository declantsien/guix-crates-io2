(define-module (crates-io li bt libtectonic) #:use-module (crates-io))

(define-public crate-libtectonic-0.3.0 (c (n "libtectonic") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.3.2") (d #t) (k 0)) (d (n "chrono") (r "^0.4.9") (d #t) (k 0)) (d (n "csv") (r "^1.1.1") (d #t) (k 0)) (d (n "fern") (r "^0.5.8") (d #t) (k 0)) (d (n "indexmap") (r "^1.2.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.62") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "serde") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.101") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.41") (d #t) (k 0)) (d (n "uuid") (r "^0.7.4") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "02ndjl7w0q7pzkfzlmphzasxiasgvba8hkapfcn247x8zd8qfkk3") (y #t)))

