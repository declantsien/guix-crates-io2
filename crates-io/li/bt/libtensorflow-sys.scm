(define-module (crates-io li bt libtensorflow-sys) #:use-module (crates-io))

(define-public crate-libtensorflow-sys-0.0.1 (c (n "libtensorflow-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.16") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k8r49zkykjbd144gkgrxjh8kp2jwcb673q76vvifxx13fvwh45z")))

(define-public crate-libtensorflow-sys-0.0.2 (c (n "libtensorflow-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.16") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03gpfnvgpxfzd4px1jgh11jgcgrhf8b3yvxhs4md6qxfzhmcw4aj")))

