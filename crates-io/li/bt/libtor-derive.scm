(define-module (crates-io li bt libtor-derive) #:use-module (crates-io))

(define-public crate-libtor-derive-0.1.0 (c (n "libtor-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0scbrql5xhn5xa7m63hd0bb1b6i0gwpgzdjiaw7lbapsmc7pyijs")))

(define-public crate-libtor-derive-0.1.1 (c (n "libtor-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0jl4hmkb557sb1yyj7j9ii30bjq4k3l38xg7rk8aw36szrh0sich")))

(define-public crate-libtor-derive-0.1.2 (c (n "libtor-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0hrhajsl8xcwrmpl4cymnvhmdr7mx8634rmgqlqki1c3bsr82xqp") (f (quote (("debug" "syn/extra-traits"))))))

