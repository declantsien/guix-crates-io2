(define-module (crates-io li bt libtimew) #:use-module (crates-io))

(define-public crate-libtimew-0.1.0 (c (n "libtimew") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "1lh12bmblviwxrx5sxf39g684mfb3q4c5wjw1javgwfncn0ska96")))

(define-public crate-libtimew-0.1.1 (c (n "libtimew") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "1jhw5drc52hb2hxw8pipyd80fk6ly3dc3zsid0xlsml4kidix1b4")))

(define-public crate-libtimew-0.1.2 (c (n "libtimew") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "1rpzmf4kvcbhlk545czdlbq3l2b8yf6in9b0kmxhpja9d5xzz9m2")))

(define-public crate-libtimew-0.1.3 (c (n "libtimew") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "10vi5hrbvcn2w3lnhw9rqnwjjl0mqv34pj3bdqrhp17g5wj5qq9z")))

(define-public crate-libtimew-0.1.4 (c (n "libtimew") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.10") (d #t) (k 0)))) (h "08knsnvc405wvc44n4wx73n5dfpbqhxayj6wy7m9bpp2qjlw11ml")))

