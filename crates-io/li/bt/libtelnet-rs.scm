(define-module (crates-io li bt libtelnet-rs) #:use-module (crates-io))

(define-public crate-libtelnet-rs-0.1.0 (c (n "libtelnet-rs") (v "0.1.0") (h "0bhv725jnqfd5vb9vmx3j3rv5g1snn0xh92866hggw24ndamw8mi")))

(define-public crate-libtelnet-rs-0.1.1 (c (n "libtelnet-rs") (v "0.1.1") (h "0zv72ryczj6j6v9gynpvbvwwvf8kw5l21q37x5101ixcc2avq7wj")))

(define-public crate-libtelnet-rs-0.1.2 (c (n "libtelnet-rs") (v "0.1.2") (h "1vs15qzbkyhqf2x03hwaxmwcwb65dyfp83isc05i8hs9psdd159p")))

(define-public crate-libtelnet-rs-0.2.0 (c (n "libtelnet-rs") (v "0.2.0") (h "0fmrwdf5bba6khmgkphg17x22j4z80c3di40vflhpcgjqchyc478")))

(define-public crate-libtelnet-rs-0.2.1 (c (n "libtelnet-rs") (v "0.2.1") (h "1f2jkr0q0mm4qnywc1g7yq5ppmzlmimmlm7ric1bi08fx4ysfnkv")))

(define-public crate-libtelnet-rs-0.2.2 (c (n "libtelnet-rs") (v "0.2.2") (h "03hzmdfkm6fha763gq9hqi1a0ysgs3sa7wa0bdbfg0qiv81qcj4w")))

(define-public crate-libtelnet-rs-0.3.0 (c (n "libtelnet-rs") (v "0.3.0") (h "0559lr0k7h38hx6l9iii47gi7q02bpfh2gyh5fnbhp3w4s4ffhda")))

(define-public crate-libtelnet-rs-0.3.2 (c (n "libtelnet-rs") (v "0.3.2") (h "13azhn0p8mw44iiwwhqpcwhwv7yjdwzf5cvqxyfzcsa7gfl25khy")))

(define-public crate-libtelnet-rs-0.3.3 (c (n "libtelnet-rs") (v "0.3.3") (h "1q1yv8mq7zm998n2lsn75pbh83y7xnfbr6gwrgc7hfwzx45yi9a6")))

(define-public crate-libtelnet-rs-0.3.5 (c (n "libtelnet-rs") (v "0.3.5") (h "0rd4iniizhk07aad4bphxzdd6ljzbfp5jh2h2qvsyclhfdmzypmd")))

(define-public crate-libtelnet-rs-0.3.7 (c (n "libtelnet-rs") (v "0.3.7") (h "1dl0py3gnlpibfxjkj9dy3v20lm34vk5kk10wkzi7ky3h1i22xh3")))

(define-public crate-libtelnet-rs-0.3.8 (c (n "libtelnet-rs") (v "0.3.8") (h "0mmirfxj7dsgh6qyzc1h3nfzfisanf4bbadjxrkfshyr3v5dkb5z")))

(define-public crate-libtelnet-rs-0.3.9 (c (n "libtelnet-rs") (v "0.3.9") (h "1543s74vnp1m1l0kjjmfhzd1yibzbs6g9ndkw6sk1l0wycna3hi0")))

(define-public crate-libtelnet-rs-1.0.0 (c (n "libtelnet-rs") (v "1.0.0") (h "1c4gzr6mw8r4idb89dnzbhlm35ba8h0mql109w7wg5y4lvg4sfay")))

(define-public crate-libtelnet-rs-1.1.0 (c (n "libtelnet-rs") (v "1.1.0") (h "12n546n8hzspm74829mqavgzbvc9wk1kv1shxharcz4jixni0rjb")))

(define-public crate-libtelnet-rs-1.1.1 (c (n "libtelnet-rs") (v "1.1.1") (h "0604lnvkfpsgh4b171dfammzykgiq0dh1mvsz9amn469agqn21nc")))

(define-public crate-libtelnet-rs-1.1.2 (c (n "libtelnet-rs") (v "1.1.2") (h "0n3jwzf1wvd1qg9xvi7iyphbj30h2wjq10pl6s4phibnlh3scawr")))

(define-public crate-libtelnet-rs-2.0.0 (c (n "libtelnet-rs") (v "2.0.0") (d (list (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "1895vqiw5z682lxxnjp6im1jbf01dwfsiim4qbcvn53bj17z34cb")))

