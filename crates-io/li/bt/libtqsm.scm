(define-module (crates-io li bt libtqsm) #:use-module (crates-io))

(define-public crate-libtqsm-0.6.1 (c (n "libtqsm") (v "0.6.1") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "fancy-regex") (r "^0.11.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10.1") (d #t) (k 0)))) (h "1f5zj596wx4ywxj3hjfgc8kc45vj9axz24r6xn6qzjgy29zr4x8x")))

