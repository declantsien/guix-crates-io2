(define-module (crates-io li bt libtuntap) #:use-module (crates-io))

(define-public crate-libtuntap-0.0.1 (c (n "libtuntap") (v "0.0.1") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ioctl") (r "^0.6") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\"))") (k 0) (p "ioctl-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "0w3z8a9c76b25kkzd6lzcixdx36prw00qmjb9bgvwc0i7i6rvr98")))

(define-public crate-libtuntap-0.0.2 (c (n "libtuntap") (v "0.0.2") (d (list (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "ioctl") (r "^0.6") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"macos\"))") (k 0) (p "ioctl-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1kvpl2maa8ggkn2xpjxjblic62pnf09a405sfdnawn3sm47l8b1l")))

