(define-module (crates-io li bt libtracecmd) #:use-module (crates-io))

(define-public crate-libtracecmd-0.1.0 (c (n "libtracecmd") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12vpgk6y9nl5jwviml042q2nzdd4ba73mvi9y01n2fj6pss3lb9i")))

(define-public crate-libtracecmd-0.1.1 (c (n "libtracecmd") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1gnd6m4smv785v3k4g70jyw1cv5a2k2f97ssffxikwhsl5yhgvq0")))

(define-public crate-libtracecmd-0.2.0 (c (n "libtracecmd") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "09fp5hjh8jc73c7pb86lr1sh3dvlazg9xqwrsf39l9np5zi7rc24")))

(define-public crate-libtracecmd-0.2.1 (c (n "libtracecmd") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 1)) (d (n "argh") (r "^0.1") (d #t) (k 2)) (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "once_cell") (r "^1.0") (d #t) (k 2)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1yqh956gdwmph07mzazdw3dmhg7chx5qv37nrrj8l77zj9pm4yk0")))

