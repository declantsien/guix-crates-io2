(define-module (crates-io li bt libtugraph-sys) #:use-module (crates-io))

(define-public crate-libtugraph-sys-0.1.0+3.5.0 (c (n "libtugraph-sys") (v "0.1.0+3.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)))) (h "0rsh8g61apjxkchn4w0l31qh0229ilcic9v6dxfy5qcngaf11s8i") (l "lgraph") (r "1.65.0")))

(define-public crate-libtugraph-sys-0.1.1+3.5.0 (c (n "libtugraph-sys") (v "0.1.1+3.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)))) (h "0aa4ad66wnrbccb08sjks24a6f00glzwkn25rjf4p28hfwh587s8") (l "lgraph") (r "1.65.0")))

(define-public crate-libtugraph-sys-0.1.2+3.5.0 (c (n "libtugraph-sys") (v "0.1.2+3.5.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "tempfile") (r "^3.4") (d #t) (k 2)))) (h "06dxbx0lr56mfagypjyawvjb8y275nhm45cxl72xw4c5agaia75c") (l "lgraph") (r "1.65.0")))

