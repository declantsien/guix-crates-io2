(define-module (crates-io li bt libtzfile) #:use-module (crates-io))

(define-public crate-libtzfile-0.3.0 (c (n "libtzfile") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "16cm5w6phm2pk0rzjfyzr7y20sv3lq2qa88kykan0mfdp2yja65f")))

(define-public crate-libtzfile-0.4.0 (c (n "libtzfile") (v "0.4.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "0p4b3z4yqalk7c74mr7p8h2329qh9sgcx0lcdz1z9f7dnciyj2fs")))

(define-public crate-libtzfile-0.5.0 (c (n "libtzfile") (v "0.5.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "0vpi0r2li98yclxq85q8yr3ylvhcazld10rm3vr5p1ddmmy76s0z") (f (quote (("with-chrono" "chrono"))))))

(define-public crate-libtzfile-0.5.1 (c (n "libtzfile") (v "0.5.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "0giwylyixpjm8bd1dc00874j9cldvfr29d3liwlxb19zsxk5amnm") (f (quote (("with-chrono" "chrono"))))))

(define-public crate-libtzfile-1.0.0 (c (n "libtzfile") (v "1.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "1prg7mmfk55baf296pk141zbr13yzmqqdznd1p0s6xsr46agvza3") (f (quote (("with-chrono" "chrono"))))))

(define-public crate-libtzfile-1.0.1 (c (n "libtzfile") (v "1.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "1x4qj8kvziss4pgm6msdjslzs1hvnzcw4a2q43fln2ijmbbn8yrl") (f (quote (("with-chrono" "chrono"))))))

(define-public crate-libtzfile-1.0.2 (c (n "libtzfile") (v "1.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)))) (h "0d0d264j4wb1li6lv32f0iyd9mljf8c04i5913r18wwy1gql4hvk") (f (quote (("with-chrono" "chrono"))))))

(define-public crate-libtzfile-1.0.3 (c (n "libtzfile") (v "1.0.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "0j35l6nfjlziz17vgvzfbdyw3kfa45m0yfgjq3r9hslq8gk1f7dz") (f (quote (("with-chrono" "chrono"))))))

(define-public crate-libtzfile-1.0.4 (c (n "libtzfile") (v "1.0.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1ripazjwgqsrqivsgxypdm363aq107hxgjib5jqakzjb810y0jh0") (f (quote (("with-chrono" "chrono"))))))

(define-public crate-libtzfile-1.0.5 (c (n "libtzfile") (v "1.0.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "11xwl2yqp4z8fxsa78h0dz40hri07knd2jka8qamkc717x6qbx4d") (f (quote (("with-chrono" "chrono"))))))

(define-public crate-libtzfile-1.0.6 (c (n "libtzfile") (v "1.0.6") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "1al4hhgs7n8pia490hr6gzhppbv1bs0x7x87hx59ypw5zzv5idv0") (f (quote (("with-chrono" "chrono"))))))

(define-public crate-libtzfile-1.0.7 (c (n "libtzfile") (v "1.0.7") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (t "cfg(windows)") (k 0)))) (h "191bmpwlkyb8fcjk79ysnwlym5m91gpvv3gp5sq3adq6d2v4yl5r") (f (quote (("with-chrono" "chrono"))))))

(define-public crate-libtzfile-1.1.0 (c (n "libtzfile") (v "1.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "084ny761frbwc3i6nlq80fqyyb3i93x247wkjxk0xbpnp8y4d29l")))

(define-public crate-libtzfile-1.1.1 (c (n "libtzfile") (v "1.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "04rq7gfvs4wb0sgfa4k9smlgrjbna6py6sj1347pddn767ihl6ir")))

(define-public crate-libtzfile-1.1.2 (c (n "libtzfile") (v "1.1.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)))) (h "17wppspjlb6dfqcjb8dhkvz6q2qxclbq9s9zk7dyzdxkg871csx9")))

(define-public crate-libtzfile-2.0.0 (c (n "libtzfile") (v "2.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0b49cxxvssjrk5qc571i9qz2ri15rjknc8shfrs9rfr9gzny44n4") (f (quote (("parse" "chrono") ("json" "serde" "serde_json" "chrono/serde" "chrono"))))))

(define-public crate-libtzfile-2.0.1 (c (n "libtzfile") (v "2.0.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0scywxx7m3b2kpgjgz7bm4krz0fvy6i9vqs34gbrdwx2dj82yxgy") (f (quote (("parse" "chrono") ("json" "serde" "serde_json" "chrono/serde" "chrono"))))))

(define-public crate-libtzfile-2.0.2 (c (n "libtzfile") (v "2.0.2") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "15ih80j3gibm398l9cslq9g91nb9rbkbkk5cf88rbf7pf5l48x68") (f (quote (("parse" "chrono") ("json" "serde" "serde_json" "chrono/serde" "chrono"))))))

(define-public crate-libtzfile-2.0.3 (c (n "libtzfile") (v "2.0.3") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0zkah5l30rj09n8r3plw546f64cbg3cifccx4dqxkyyv6kdm1ivz") (f (quote (("parse" "chrono") ("json" "serde" "serde_json" "chrono/serde" "chrono"))))))

(define-public crate-libtzfile-2.0.4 (c (n "libtzfile") (v "2.0.4") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1v08lg2qxhamvzr2y2vp1qkvkqr5x0jg302c6c9zlivsdpa95vp0") (f (quote (("parse" "chrono") ("json" "serde" "serde_json" "chrono/serde" "chrono"))))))

(define-public crate-libtzfile-2.0.5 (c (n "libtzfile") (v "2.0.5") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0prlh68qy46754cp3blrbd5z8vdwakplf8637mazavdl5hzacbcx") (f (quote (("parse" "chrono") ("json" "serde" "serde_json" "chrono/serde" "chrono"))))))

(define-public crate-libtzfile-3.0.0 (c (n "libtzfile") (v "3.0.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "1fj2fw1rs6nks9px395127dls1gp2da6bb4wandld8klys2grbal") (f (quote (("parse" "chrono") ("json" "serde" "serde_json" "chrono/serde" "chrono"))))))

(define-public crate-libtzfile-3.1.0 (c (n "libtzfile") (v "3.1.0") (d (list (d (n "byteorder") (r "^1") (k 0)) (d (n "chrono") (r "^0.4.37") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "0rl8apks7aav7i0z4abva3xp6rbvwiwibdcmjdz4wa1vw4xxanq6") (f (quote (("std") ("parse" "chrono") ("json" "serde" "serde_json" "chrono/serde" "chrono") ("default" "std"))))))

