(define-module (crates-io li bt libtool) #:use-module (crates-io))

(define-public crate-libtool-0.1.0 (c (n "libtool") (v "0.1.0") (h "0kgpkqnsd20nd3xyq2l72yzz2jjv4qb4ln4hdlyw11lr4x7i80rl")))

(define-public crate-libtool-0.1.1 (c (n "libtool") (v "0.1.1") (h "1lmqjcgvxirgki7sqiqil6r4v63k80534r49aqk8p172sl554xpm")))

