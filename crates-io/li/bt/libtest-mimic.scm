(define-module (crates-io li bt libtest-mimic) #:use-module (crates-io))

(define-public crate-libtest-mimic-0.1.0 (c (n "libtest-mimic") (v "0.1.0") (d (list (d (n "structopt") (r "^0.2") (d #t) (k 0)) (d (n "termcolor") (r "^0.3") (d #t) (k 0)))) (h "1vy2clf0w0ir5ldxrnvzs64bs5qh0326xm14hf2m9a85bqq6zshc")))

(define-public crate-libtest-mimic-0.2.0 (c (n "libtest-mimic") (v "0.2.0") (d (list (d (n "structopt") (r "^0.3.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "18pvixpy7hagsi5avaqncqrn18xcx2bm3iy218d01dpnn15rlcqa")))

(define-public crate-libtest-mimic-0.3.0 (c (n "libtest-mimic") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.2") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "1bp2jllwpciljr14g6s9bk4835g46kszgrjwi66vxxsk3ynbi9q8")))

(define-public crate-libtest-mimic-0.4.0 (c (n "libtest-mimic") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4.2") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "18v2cfj35b8l6j70hvay5q1s4fcl1w3aip6a0nv3qm1yaf83hnsb")))

(define-public crate-libtest-mimic-0.4.1 (c (n "libtest-mimic") (v "0.4.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.4") (d #t) (k 0)) (d (n "rayon") (r "^1.3.0") (d #t) (k 0)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)))) (h "17xs3xk26afdgajqqzafsv4fdg0sfijlfnjac6znad40bfmml6fc")))

(define-public crate-libtest-mimic-0.5.0 (c (n "libtest-mimic") (v "0.5.0") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "10cyhwfpzykyzl9y3msmhscl3bq3nga5wgw9x2vgk8i2hkl9q7va") (r "1.58")))

(define-public crate-libtest-mimic-0.5.1 (c (n "libtest-mimic") (v "0.5.1") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1mcs3p3rajnhmwpd6pwny45h7hz7vczal9iy1gh9zcrj7wd22w5y") (r "1.58")))

(define-public crate-libtest-mimic-0.5.2 (c (n "libtest-mimic") (v "0.5.2") (d (list (d (n "clap") (r "^3.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1v103c90ibs35ffr9mww9h5la3b7xxvil32v6x0szxcqq9wr8lkr") (r "1.58")))

(define-public crate-libtest-mimic-0.6.0 (c (n "libtest-mimic") (v "0.6.0") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1yipifm9fgfg6my2jj3i5wrc6ri9wqix02fys0isplb7cx8h7dnp") (r "1.60")))

(define-public crate-libtest-mimic-0.6.1 (c (n "libtest-mimic") (v "0.6.1") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1rwwdrzc5hmfisc9c3zcvnab14rgh0zfa636c2jbhv4az5qf73bd") (r "1.60")))

(define-public crate-libtest-mimic-0.7.0 (c (n "libtest-mimic") (v "0.7.0") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "escape8259") (r "^0.5.2") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "02y7l5bcwli37hl625bp6l0m95mry8cavwj3nkl55zgc8iplq3vz") (r "1.60")))

(define-public crate-libtest-mimic-0.7.1 (c (n "libtest-mimic") (v "0.7.1") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "escape8259") (r "^0.5.2") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1vsq8x2k7zcqxc5gimhsjfs964c8i0aqs7ly2dm5w29qijpilmdg") (r "1.60")))

(define-public crate-libtest-mimic-0.7.2 (c (n "libtest-mimic") (v "0.7.2") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "escape8259") (r "^0.5.2") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "1ydzxr097zp3r9vwfhv4sninhaqfjcyyxnwal9v3856n609g5zgy") (r "1.60")))

(define-public crate-libtest-mimic-0.7.3 (c (n "libtest-mimic") (v "0.7.3") (d (list (d (n "clap") (r "^4.0.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "escape8259") (r "^0.5.2") (d #t) (k 0)) (d (n "fastrand") (r "^1.8.0") (d #t) (k 2)) (d (n "pretty_assertions") (r "^1.2.1") (d #t) (k 2)) (d (n "termcolor") (r "^1.0.5") (d #t) (k 0)) (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0n4vdf4wz4zglammhdzgwxqal9v1a8gbj6rc4q22jfjvxm2xl2yc") (r "1.60")))

