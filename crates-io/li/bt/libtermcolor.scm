(define-module (crates-io li bt libtermcolor) #:use-module (crates-io))

(define-public crate-libtermcolor-1.0.0 (c (n "libtermcolor") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0qkzyzmdc258y16c1vch4jm33zfn66qs8vvcrqn7dj26py8dglz2")))

(define-public crate-libtermcolor-1.1.0 (c (n "libtermcolor") (v "1.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0i9aci74jfk7yibhb2vfkkvghc5fvfjrj4lbmxnim5w30qz5m8r4")))

