(define-module (crates-io li bt libtailscale-sys) #:use-module (crates-io))

(define-public crate-libtailscale-sys-0.1.0 (c (n "libtailscale-sys") (v "0.1.0") (h "0a24vbkfw57fg2lajna19i4rrj20h4ry03slbaisr6iyzagj9zj3")))

(define-public crate-libtailscale-sys-0.1.1 (c (n "libtailscale-sys") (v "0.1.1") (h "1qlh3k0n2v66arpyr05k649a1v0nqc1fb80f00szzq8kfl7fzssd")))

(define-public crate-libtailscale-sys-0.1.2 (c (n "libtailscale-sys") (v "0.1.2") (h "0mjij9sd31by60kjg4b1d3rd2y9vnwcs20ilhdmv0zvyfrz3h7wm")))

(define-public crate-libtailscale-sys-0.1.3 (c (n "libtailscale-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0b0561hg41a1likhdx4b7zx9sqx7xz6nqp9ia6h5mrn6d6mkfjd3")))

(define-public crate-libtailscale-sys-0.1.4 (c (n "libtailscale-sys") (v "0.1.4") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "0xxc1w6v3m5knkj5g60fqnr2zarjfys04vgbk21akhmhpdrd13r0")))

(define-public crate-libtailscale-sys-0.1.5 (c (n "libtailscale-sys") (v "0.1.5") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "05qv2av66pdnfz6mf8nj92fb6ri5w917ds8hyp0iackqybr4dv9g")))

(define-public crate-libtailscale-sys-0.1.6 (c (n "libtailscale-sys") (v "0.1.6") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1qva2lzwshcdkwbn2xpwhjzcki0ihkjbsg1nd3l2mkd8ilakxakb") (l "tailscale")))

(define-public crate-libtailscale-sys-0.2.0 (c (n "libtailscale-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.79") (d #t) (k 1)))) (h "1zs5isp9a5gg3jp06j7dgvqgl8sdvx7mfcxmqq4pnifx91r7z0wb") (l "tailscale")))

