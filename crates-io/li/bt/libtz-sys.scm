(define-module (crates-io li bt libtz-sys) #:use-module (crates-io))

(define-public crate-libtz-sys-0.0.0 (c (n "libtz-sys") (v "0.0.0") (h "0xpw5wz4jsbiqxy1myv5i5i2888izs1rrn3phsfxlqh4mrr1kacp")))

(define-public crate-libtz-sys-0.1.0 (c (n "libtz-sys") (v "0.1.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0zjb86xyxkl1f01n891p1j7qymvv5ck4l3n7gpgzc1x4k4ms1r5r") (l "tz")))

(define-public crate-libtz-sys-0.1.1 (c (n "libtz-sys") (v "0.1.1") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "06dcckl48fncmsaqqgxdjz7xj7llwxlpp3sm1pbzjwj5b7a3h1c9") (l "tz")))

(define-public crate-libtz-sys-0.2.0 (c (n "libtz-sys") (v "0.2.0") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 1)))) (h "0z3kzdw895j0dvff7xgr9yrq6crkmj889x0bs8kk61wq8w6rrh8k") (l "tz")))

(define-public crate-libtz-sys-0.2.1 (c (n "libtz-sys") (v "0.2.1") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1hk9y88hsa1qgb6qj3ihaxyg1kqw4yp9d2l72l7hxv5wjjiw36p0") (l "tz")))

(define-public crate-libtz-sys-0.2.2 (c (n "libtz-sys") (v "0.2.2") (d (list (d (n "version-sync") (r "^0.9") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0v0q6qpk3gwqvyrx65zzgmia7bdn00n9zsg7q7a6iqhlf4yk0ynb") (l "tz")))

