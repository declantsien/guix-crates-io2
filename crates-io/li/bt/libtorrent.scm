(define-module (crates-io li bt libtorrent) #:use-module (crates-io))

(define-public crate-libtorrent-0.1.0 (c (n "libtorrent") (v "0.1.0") (d (list (d (n "libtorrent-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0zkcyr2w9c4rq8i5l2w3jwasrnbfh54x47p81qg8dy3a4vskhxvn")))

(define-public crate-libtorrent-0.1.1 (c (n "libtorrent") (v "0.1.1") (d (list (d (n "libtorrent-sys") (r "^1.0.0") (d #t) (k 0)))) (h "07qk48fxv8zaykpwsjvx4b3sxdzhqjk93ckrgy512nybxrinkir9")))

