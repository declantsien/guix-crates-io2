(define-module (crates-io li bt libtaskrs) #:use-module (crates-io))

(define-public crate-libtaskrs-0.0.1 (c (n "libtaskrs") (v "0.0.1") (d (list (d (n "boilermates") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "utoipa") (r "^3.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0yvv8knb5xz282s825j5xb6fcc3fpgv0b4paavd773kyrfm0z71d")))

(define-public crate-libtaskrs-0.1.0 (c (n "libtaskrs") (v "0.1.0") (d (list (d (n "boilermates") (r "^0.3.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.188") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sqlx") (r "^0.7.2") (f (quote ("macros"))) (d #t) (k 0)) (d (n "utoipa") (r "^3.5.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4.1") (f (quote ("serde"))) (d #t) (k 0)))) (h "0ffcrwf3408zsv69j3nsigi9ivzlh0k4qqh8i9l36718d0jf40lz")))

