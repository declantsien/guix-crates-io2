(define-module (crates-io li bt libtcc) #:use-module (crates-io))

(define-public crate-libtcc-0.1.0 (c (n "libtcc") (v "0.1.0") (h "0gdwvhlwcgx0hswdjnz46skvcr57gavwmv7x94krixk44dwaz1z1") (y #t) (l "tcc")))

(define-public crate-libtcc-0.1.1 (c (n "libtcc") (v "0.1.1") (h "115v3rrlhsdy80d3iasfwy5ljzhii970y09zc56dslaqhi6rc20z") (l "tcc")))

(define-public crate-libtcc-0.2.0 (c (n "libtcc") (v "0.2.0") (d (list (d (n "cc") (r "^1.0.52") (d #t) (k 1)))) (h "0nrkvspxy0vwf7dbn2ndh4vf11g7hzlphkp1qb5z83dqbvsvqvpz") (l "tcc")))

