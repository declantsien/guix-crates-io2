(define-module (crates-io li bt libtls-sys) #:use-module (crates-io))

(define-public crate-libtls-sys-0.0.1 (c (n "libtls-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.11") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.8") (d #t) (k 1)))) (h "1r2zmimcdcchhpm00rq68dkn2xpnmw9s6a1if8zv5mlqb6zqf2z7") (f (quote (("system") ("fallback") ("default" "system" "fallback")))) (y #t)))

(define-public crate-libtls-sys-1.0.0-alpha1 (c (n "libtls-sys") (v "1.0.0-alpha1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0rg7rwsp65bd5qmh3p6nspwgv65s1sc83sny8v4xhdi18y5flyq4") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.0.0-alpha2 (c (n "libtls-sys") (v "1.0.0-alpha2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "193c71ik6hbp32aw7lkjxcqkhbb795ad1md85y63bdacal1nwv6i") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.0.0-alpha3 (c (n "libtls-sys") (v "1.0.0-alpha3") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0zb9wmx62axdabjny5i3km3zjklwchhjcgga9r87hyik7dkmp8l5") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.0.0-alpha4 (c (n "libtls-sys") (v "1.0.0-alpha4") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "18b53jmk97a3amgi9fs7s6c2sgzkpbn3p3qxv2ashdb6583dmwc3") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.0.0-alpha5 (c (n "libtls-sys") (v "1.0.0-alpha5") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0qkbiscd396wsj8g3km3yi493zsky2f279ld52163angn9mpwjzx") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.0.0-alpha6 (c (n "libtls-sys") (v "1.0.0-alpha6") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0mnrifky5wpc0c066l8wh16nh19sn2wyy1sbl2lzxzi39bdiphiz") (l "libtls")))

(define-public crate-libtls-sys-1.0.0-alpha7 (c (n "libtls-sys") (v "1.0.0-alpha7") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0ymdisq76qr649pihywwd34b3f9mm90b03q030yabmya64vilhdd") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.0.0-alpha8 (c (n "libtls-sys") (v "1.0.0-alpha8") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "15a9mq906lr11vj0zadk5gicmiri359jjhf2mvpz9nsgs4wrs7ck") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.0.0 (c (n "libtls-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0bszmbg3fih91mc391aybggga56xdaijbx6qmskbbqbb398ijl87") (l "libtls")))

(define-public crate-libtls-sys-1.1.0-alpha1 (c (n "libtls-sys") (v "1.1.0-alpha1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "09h80vn203spgqx7mrdcpmwxfg977mpnvpgpdv42ilf89m2xd2q3") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.1.0-alpha.2 (c (n "libtls-sys") (v "1.1.0-alpha.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "19da7z26vyknap2j0v8n4ashk3bz7f0fffmrdbmdwjblpinjifc8") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.1.0-alpha.3 (c (n "libtls-sys") (v "1.1.0-alpha.3") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0p7p11vrdk0l7qzp8dyf1kcsdkpr0hbxkw5370x4gys03s6m2vp9") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.1.0-beta.1 (c (n "libtls-sys") (v "1.1.0-beta.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0mdq0bprb06wddbkywfr596mcn3y0mia5fgbcklb0a2549i4kcnv") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.1.0-beta.2 (c (n "libtls-sys") (v "1.1.0-beta.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1vlk6lwnp5fqqh3sz0ydw394cq5ylbw3wrxqhbspcykrps995jl5") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.1.0-beta.3 (c (n "libtls-sys") (v "1.1.0-beta.3") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0fmj4kr0p5ncdiyld8x4f6jkrq0m5w6730r6zs1b5k00h1f2p44f") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.1.0-beta.4 (c (n "libtls-sys") (v "1.1.0-beta.4") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "0a9k2qmj1w31ww71pnl44c3vn727i8ah9prw35s0zp8n9ily3qgw") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.1.0-beta.5 (c (n "libtls-sys") (v "1.1.0-beta.5") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1nqxj6ff2jknj0sb5ab0l9kblfkwwm9bnr7wmmg2psk3z76by06z") (y #t) (l "libtls")))

(define-public crate-libtls-sys-1.1.0-beta.6 (c (n "libtls-sys") (v "1.1.0-beta.6") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1sj2icgdnkwm6qjhv0pj074wrniy3cbxvdwqxqsbzlk7vvisjk6i") (l "libtls")))

(define-public crate-libtls-sys-1.1.0 (c (n "libtls-sys") (v "1.1.0") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1kri8xpzfk52lsxqn0yl2lph4lgd40x3qf135gkqd195lmd8b1sv") (l "libtls")))

(define-public crate-libtls-sys-1.1.1 (c (n "libtls-sys") (v "1.1.1") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1xp2b9prbkd51dk7q4h7xfhvnwl2fj82ggc7x7knbdbj53l01n5g") (l "libtls")))

(define-public crate-libtls-sys-1.1.2 (c (n "libtls-sys") (v "1.1.2") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "num_cpus") (r "^1.11.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.16") (d #t) (k 1)))) (h "1ihmlb4i3wn4k3qfxb78b3biw96371llzsc8xwsf1fblrrbsb74s") (l "libtls")))

(define-public crate-libtls-sys-1.2.0 (c (n "libtls-sys") (v "1.2.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "num_cpus") (r "^1.12.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.17") (d #t) (k 1)))) (h "0sdhca3jx8hb0szwrnwqjd9spglnq4ldasnlr9sw2rzvwi5pwr7q") (l "libtls")))

