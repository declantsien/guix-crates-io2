(define-module (crates-io li bt libtad-models) #:use-module (crates-io))

(define-public crate-libtad-models-0.1.0 (c (n "libtad-models") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0spsjirwc6wkf6504y5gx15b3kx0q22s3f4bq1krbsirghf02p79")))

(define-public crate-libtad-models-0.2.0 (c (n "libtad-models") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "url") (r "^2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "1x6nv5ghqvg722ihvhg2sgpmwq0mdidnsfk43l39zd11ylln3kac")))

