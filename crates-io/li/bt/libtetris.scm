(define-module (crates-io li bt libtetris) #:use-module (crates-io))

(define-public crate-libtetris-0.0.1 (c (n "libtetris") (v "0.0.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "19496klqwx74pwra62z2i535dd52la3ckq4xn7klqdk3dda2hwyx")))

(define-public crate-libtetris-0.0.2 (c (n "libtetris") (v "0.0.2") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0cqq6a67l2i6jdfiy9dmxmyrwmniwnmm30dgmvhk170w67py9b4h")))

(define-public crate-libtetris-0.0.3 (c (n "libtetris") (v "0.0.3") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "11k116l9x2pz1b2q0bqzk9v0kbrba3z51f3haapjya8hbz3rbh68") (s 2) (e (quote (("wasm" "dep:wasm-bindgen" "dep:console_error_panic_hook"))))))

(define-public crate-libtetris-0.0.4 (c (n "libtetris") (v "0.0.4") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0vp7lbhicflx3v5j1l3vg5grfr95idssdhzfv6plnkq1kfqjna6i") (s 2) (e (quote (("wasm" "dep:wasm-bindgen" "dep:console_error_panic_hook"))))))

(define-public crate-libtetris-0.0.5 (c (n "libtetris") (v "0.0.5") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1k54k3i4sia267wdxpgvlllhp21642gb7lcp0i04srdid1wdzv1h") (s 2) (e (quote (("wasm" "dep:wasm-bindgen" "dep:console_error_panic_hook"))))))

(define-public crate-libtetris-0.0.6 (c (n "libtetris") (v "0.0.6") (d (list (d (n "console_error_panic_hook") (r "^0.1.7") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0wr79kqahr79qhlsfiqhs7gj00x0bm4dvz40wqhd5iqxl191dh4y") (s 2) (e (quote (("wasm" "dep:wasm-bindgen" "dep:console_error_panic_hook") ("native" "dep:rand"))))))

