(define-module (crates-io li bt libtodoist) #:use-module (crates-io))

(define-public crate-libtodoist-0.2.0 (c (n "libtodoist") (v "0.2.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)))) (h "0vys3pvh7v490zv91n90xfvgzwm9rrhm3011z3k80xwr5wj6ncq1")))

(define-public crate-libtodoist-0.3.0 (c (n "libtodoist") (v "0.3.0") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)))) (h "02630nxpy9x21ib2hslp2sdd47j3yv4z4x7kd8av2q60a5q6c3s8")))

(define-public crate-libtodoist-0.3.5 (c (n "libtodoist") (v "0.3.5") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)))) (h "0ji8vb02ann0wm65sa8515bjj211y1hzcw605xwdpvl4swy5g6qf")))

(define-public crate-libtodoist-0.3.6 (c (n "libtodoist") (v "0.3.6") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)))) (h "069drzkc33in6abx1yrvgyiv3jyk62raj1wn9s0sd916hpa3bbmc")))

(define-public crate-libtodoist-0.3.7 (c (n "libtodoist") (v "0.3.7") (d (list (d (n "reqwest") (r "^0.11.12") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)))) (h "16ixpzzsc8rhz0d6764z194yq39i2fmhphvkxg1nspx6cliihppd")))

(define-public crate-libtodoist-0.4.0 (c (n "libtodoist") (v "0.4.0") (d (list (d (n "mockito") (r "^1.2.0") (d #t) (k 2)) (d (n "reqwest") (r "^0.11.24") (f (quote ("json"))) (d #t) (k 0)) (d (n "rust-embed") (r "^8.2.0") (d #t) (k 2)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.87") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.9") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "1rpmmq8rbl2r4siipcf7c6c7ygkv1nr35p2fzkfbz4syyzlwpjyj")))

