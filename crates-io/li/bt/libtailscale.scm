(define-module (crates-io li bt libtailscale) #:use-module (crates-io))

(define-public crate-libtailscale-0.1.0 (c (n "libtailscale") (v "0.1.0") (d (list (d (n "libtailscale-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1m4lnd7rinf93wj5axflvm1v9mc9divrxn70if7mwb922zmvaimb")))

(define-public crate-libtailscale-0.1.1 (c (n "libtailscale") (v "0.1.1") (d (list (d (n "libtailscale-sys") (r "^0.1.1") (d #t) (k 0)))) (h "0pmxvx8qkmbkwlsn51ywvbg419pmwaqdaynmbi7sg9qn4rrr589f")))

(define-public crate-libtailscale-0.1.2 (c (n "libtailscale") (v "0.1.2") (d (list (d (n "libtailscale-sys") (r "^0.1.2") (d #t) (k 0)))) (h "1l6sqs0xm6fs7lipfp7xdz7qvdmvh3ab7jzyxbjbhw1zammbqrnc")))

(define-public crate-libtailscale-0.1.3 (c (n "libtailscale") (v "0.1.3") (d (list (d (n "libtailscale-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1xz95gdlpkx3fb0llviyjigy8rzgdq117fv8s1qrlb5j188cskh8")))

(define-public crate-libtailscale-0.1.4 (c (n "libtailscale") (v "0.1.4") (d (list (d (n "libtailscale-sys") (r "^0.1.4") (d #t) (k 0)))) (h "18g6f8sbgj80j1h8lsh4rag6xj8wj9zd57j4vwxp0981i9m8b83y")))

(define-public crate-libtailscale-0.1.5 (c (n "libtailscale") (v "0.1.5") (d (list (d (n "libtailscale-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0nfnq1j8yfpp7bd89lv6hxl0ca7w5xq43da4sswvnf2315xl0da7")))

(define-public crate-libtailscale-0.1.6 (c (n "libtailscale") (v "0.1.6") (d (list (d (n "libtailscale-sys") (r "^0.1.6") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 2)) (d (n "http-body-util") (r "^0.1.0-rc.2") (d #t) (k 2)) (d (n "hyper") (r "^1.0.0-rc.3") (f (quote ("http1" "server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "0aq8qnzvgcgcma5ld6hx3b8xf1i1z7kp3mg2gkcpxa47qnkya1c4")))

(define-public crate-libtailscale-0.2.0 (c (n "libtailscale") (v "0.2.0") (d (list (d (n "libtailscale-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "bytes") (r "^1.4.0") (d #t) (k 2)) (d (n "http-body-util") (r "^0.1.0-rc.2") (d #t) (k 2)) (d (n "hyper") (r "^1.0.0-rc.3") (f (quote ("http1" "server"))) (d #t) (k 2)) (d (n "tokio") (r "^1.26.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1l5hnw5vyszhc33801vla8c64vrclxfk4v74j1y9gicg5pswfn89")))

