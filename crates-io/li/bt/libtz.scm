(define-module (crates-io li bt libtz) #:use-module (crates-io))

(define-public crate-libtz-0.0.0 (c (n "libtz") (v "0.0.0") (h "1x3qskz51pc17856ykg6k9zwdys2cbczxr94sj32wiiirjci60br")))

(define-public crate-libtz-0.1.0 (c (n "libtz") (v "0.1.0") (d (list (d (n "libtz-sys") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "09i5v6dpqza4vc1y48hchfgzky4lfdhvx2y4hg4xcf6p6xh6ij9s")))

(define-public crate-libtz-0.1.1 (c (n "libtz") (v "0.1.1") (d (list (d (n "libtz-sys") (r "^0.2") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "038w8rf0xxiyg7hy4gjpykrc2zc73vjv47vr82alc2z0ybi72s3s")))

