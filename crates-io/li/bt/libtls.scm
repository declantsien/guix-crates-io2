(define-module (crates-io li bt libtls) #:use-module (crates-io))

(define-public crate-libtls-0.0.1 (c (n "libtls") (v "0.0.1") (d (list (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libtls-sys") (r "^0.0.1") (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)))) (h "0l1jgizg0sfcw5nh5b9kcimdd4hkjrghhfrk2ky8f9ijs0p6i5km") (f (quote (("system" "libtls-sys/system") ("fallback" "libtls-sys/fallback") ("default" "system" "fallback")))) (y #t)))

(define-public crate-libtls-1.0.0-alpha1 (c (n "libtls") (v "1.0.0-alpha1") (d (list (d (n "libtls-sys") (r "^1.0.0-alpha1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "08zxpami4ddjkaijl70xg0ijliyzbncsc6f4cqgmzwvzc631qizc") (y #t)))

(define-public crate-libtls-1.0.0-alpha2 (c (n "libtls") (v "1.0.0-alpha2") (d (list (d (n "libtls-sys") (r "^1.0.0-alpha2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "049k8iy42rd3n4zm8llla9pym1g1mjfxg884k431rs8bv3mqy6bs") (y #t)))

(define-public crate-libtls-1.0.0-alpha3 (c (n "libtls") (v "1.0.0-alpha3") (d (list (d (n "libtls-sys") (r "^1.0.0-alpha3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "01j73whiw3q9nhbikw0v9pqv20gljbws392s23vbm3d0wfykf4jc") (y #t)))

(define-public crate-libtls-1.0.0-alpha4 (c (n "libtls") (v "1.0.0-alpha4") (d (list (d (n "libtls-sys") (r "^1.0.0-alpha4") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1c4h8zg7qjlj4gqzj3qxs5ajpqqqngqv16va1b1gkwrx8yp536q4") (y #t)))

(define-public crate-libtls-1.0.0-alpha5 (c (n "libtls") (v "1.0.0-alpha5") (d (list (d (n "libtls-sys") (r "^1.0.0-alpha5") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1l0z700ldy7imj9jxazrkk8482kk6fnjz4ybavi1387gx42q51j4") (y #t)))

(define-public crate-libtls-1.0.0-alpha6 (c (n "libtls") (v "1.0.0-alpha6") (d (list (d (n "libtls-sys") (r "^1.0.0-alpha6") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "06mrrdcnc4mpis45ndsmvkzkcm0cg9ybdwv236lk2jgl3riw0avj")))

(define-public crate-libtls-1.0.0-alpha7 (c (n "libtls") (v "1.0.0-alpha7") (d (list (d (n "libtls-sys") (r "^1.0.0-alpha7") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1pymiw6ccm0bb84wvi9qn5072z3bwwqm01ykw31xszlwkg88vyrk") (y #t)))

(define-public crate-libtls-1.0.0-alpha8 (c (n "libtls") (v "1.0.0-alpha8") (d (list (d (n "libtls-sys") (r "^1.0.0-alpha8") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "155lpaw4v4439cpa3dsw2xkrln0gjl5fmc6p3cb5xs58kgx6fyh4") (y #t)))

(define-public crate-libtls-1.0.0 (c (n "libtls") (v "1.0.0") (d (list (d (n "libtls-sys") (r "^1.0.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0zym42gy1lc4a19dn00lvqwbmj2ig6zx56hvwiv465kss4648159")))

(define-public crate-libtls-1.1.0-alpha1 (c (n "libtls") (v "1.1.0-alpha1") (d (list (d (n "libtls-sys") (r "^1.1.0-alpha1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0hq300i8cgc2j8y8hyqrcsiigcjmxkfmc605rrcjxrclwwply0ms") (y #t)))

(define-public crate-libtls-1.1.0-alpha.2 (c (n "libtls") (v "1.1.0-alpha.2") (d (list (d (n "libtls-sys") (r "^1.1.0-alpha.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0a0izr61krgz4c0xpvwafg2fvlx5izy02qhny2pzi7s24s07wf4p") (y #t)))

(define-public crate-libtls-1.1.0-alpha.3 (c (n "libtls") (v "1.1.0-alpha.3") (d (list (d (n "libtls-sys") (r "^1.1.0-alpha.3") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "11jrn750jz0nxjfl724y57aiwdcymgh4cvimh7c0kbmpy467zy5l") (y #t)))

(define-public crate-libtls-1.1.0-beta.1 (c (n "libtls") (v "1.1.0-beta.1") (d (list (d (n "libtls-sys") (r "^1.1.0-beta.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0g9c05y165syh3dk08j5isva4n4ixisbhg2605fl12awj16mc58i") (y #t)))

(define-public crate-libtls-1.1.0-beta.2 (c (n "libtls") (v "1.1.0-beta.2") (d (list (d (n "libtls-sys") (r "^1.1.0-beta.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0nr2gbm5x3sd58gr53g4a47dkf6ppb2izpji4jlblwdlr484zrg9") (y #t)))

(define-public crate-libtls-1.1.0-beta.4 (c (n "libtls") (v "1.1.0-beta.4") (d (list (d (n "libtls-sys") (r "^1.1.0-beta.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1m66j2m1nnj0wzz03cy6r30qnqsylqlrczjplybih54qmr8kkp21") (y #t)))

(define-public crate-libtls-1.1.0-beta.5 (c (n "libtls") (v "1.1.0-beta.5") (d (list (d (n "libtls-sys") (r "^1.1.0-beta.5") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1dkd8n12kfm79w8q0ybszfz558a8w8h0zwasacyrjxfkksi2f8km") (y #t)))

(define-public crate-libtls-1.1.0-beta.6 (c (n "libtls") (v "1.1.0-beta.6") (d (list (d (n "libtls-sys") (r "^1.1.0-beta.6") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0pqxcs6cqjj49n5x7hdg7jxjprqzij6rajbc2khsd5vrapabna6l")))

(define-public crate-libtls-1.1.0 (c (n "libtls") (v "1.1.0") (d (list (d (n "libtls-sys") (r "^1.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "1xh58548j24d0g6ch9xygmw3mmn6cbqj1zf020h40yfn2jr43pdx")))

(define-public crate-libtls-1.1.1 (c (n "libtls") (v "1.1.1") (d (list (d (n "libtls-sys") (r "^1.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "09j4lycfkf55lbmy6a9q5hnkwr3siswj6aps51d2730rb44pvhql")))

(define-public crate-libtls-1.1.2 (c (n "libtls") (v "1.1.2") (d (list (d (n "libtls-sys") (r "^1.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.7.2") (d #t) (k 2)))) (h "0fwxnd64k5h3pr7hil931jirh9ddbffhsla2x4k6dnl6p9jdd7xx")))

(define-public crate-libtls-1.2.0 (c (n "libtls") (v "1.2.0") (d (list (d (n "libtls-sys") (r "^1.2.0") (d #t) (k 0)) (d (n "libtls-sys") (r "^1.2.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)))) (h "0wlqpfziwlykj4dr5jqv6jz4g544jcb2zpmzw8j53khvpg86h4rj")))

