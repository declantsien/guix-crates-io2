(define-module (crates-io li bt libtun) #:use-module (crates-io))

(define-public crate-libtun-0.1.0 (c (n "libtun") (v "0.1.0") (d (list (d (n "async-channel") (r "^1.1.0") (d #t) (k 0)) (d (n "async-std") (r "^1.6.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.2.7") (d #t) (k 0)) (d (n "cc") (r "^1.0.25") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "ipnet") (r "^2") (f (quote ("serde"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.44") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "smol") (r "^0.1.11") (d #t) (k 0)) (d (n "snafu") (r "^0.6.8") (f (quote ("backtraces"))) (d #t) (k 0)) (d (n "socket2") (r "^0.3.12") (d #t) (k 0)))) (h "15qb9kn77hpr795c5dfa4q9mqnwp7smjhbhh19llls6g0c5p88za")))

