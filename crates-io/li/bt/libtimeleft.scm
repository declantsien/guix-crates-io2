(define-module (crates-io li bt libtimeleft) #:use-module (crates-io))

(define-public crate-libtimeleft-0.1.0 (c (n "libtimeleft") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "08kq1lbzsncqg13698m60s3ah6bpp1l8hnbi45yaj685a48qgy5j")))

(define-public crate-libtimeleft-0.1.1 (c (n "libtimeleft") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "13d0ycvnfwv0xnf4mqnyj55n21b2hxgxnyyc8navl02j3wya60br") (y #t)))

(define-public crate-libtimeleft-0.1.2 (c (n "libtimeleft") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "189njc6kk4pfpv4bgdi57gkc4fa8j6hp6kvj7is8xwpgnrjd49qx")))

(define-public crate-libtimeleft-0.1.3 (c (n "libtimeleft") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ylg2n2v0cigpspyn1djiibpw0477x8zla6ascd1nwiqr06dws8r")))

(define-public crate-libtimeleft-0.1.4 (c (n "libtimeleft") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "0faspa0hmx27l9djin433kqviajsigv0sly0gay1n037x8hw48qx")))

(define-public crate-libtimeleft-0.1.5 (c (n "libtimeleft") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.22") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h607zxdcix9vz9wwfwiwvzbppajm5cxjbjns9m3mj01zp9074d6")))

