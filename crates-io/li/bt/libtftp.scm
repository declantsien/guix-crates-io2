(define-module (crates-io li bt libtftp) #:use-module (crates-io))

(define-public crate-libtftp-0.1.1 (c (n "libtftp") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "0lzm7xd6wiydiwbb4hv4b90gvgp7k7kil35yprfnlzp3j2p5s2cv") (f (quote (("pedantic"))))))

(define-public crate-libtftp-0.1.2 (c (n "libtftp") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 0)) (d (n "tokio-proto") (r "^0.1") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1") (d #t) (k 0)))) (h "15z2yfbylgf9dx01b7xmlb73nisgw9c1igm2l3jv8nl7ykmyhcvz") (f (quote (("pedantic"))))))

