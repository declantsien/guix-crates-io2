(define-module (crates-io li bt libtorrent-sys) #:use-module (crates-io))

(define-public crate-libtorrent-sys-0.1.0 (c (n "libtorrent-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.54") (d #t) (k 1)) (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wxd13ij445izbkpdsp8c4bli2n3rlc8b3j7la23n5lmcs2gdwvx") (l "torrent-rasterbar")))

(define-public crate-libtorrent-sys-1.0.0 (c (n "libtorrent-sys") (v "1.0.0") (d (list (d (n "cxx") (r "^1.0") (d #t) (k 0)) (d (n "cxx-build") (r "^1.0.0") (d #t) (k 1)))) (h "1wpwcjxangyxrkab5jiwp4y87p60rncxvzwxrk9mmc8lsbxjha26") (l "torrent-rasterbar")))

