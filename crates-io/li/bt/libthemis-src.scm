(define-module (crates-io li bt libthemis-src) #:use-module (crates-io))

(define-public crate-libthemis-src-0.0.2-1 (c (n "libthemis-src") (v "0.0.2-1") (d (list (d (n "copy_dir") (r "^0.1.2") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.4") (d #t) (k 2)))) (h "0j7j60j0avy7al4997yxfwh5wi8fwy8pv52a57c1x6z1q92cxndv") (y #t)))

(define-public crate-libthemis-src-0.0.3 (c (n "libthemis-src") (v "0.0.3") (d (list (d (n "copy_dir") (r "^0.1.2") (d #t) (k 0)) (d (n "make-cmd") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "0vlxwrxkksgip1304lvxgfmvik70ci2an1rawmgmcl5nq552zkx9")))

(define-public crate-libthemis-src-0.11.0 (c (n "libthemis-src") (v "0.11.0") (d (list (d (n "make-cmd") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "17kz2hd6d5jcw6yiqzdh9flslphcdavsmjyz434xdx816j5fqxx2")))

(define-public crate-libthemis-src-0.12.0 (c (n "libthemis-src") (v "0.12.0") (d (list (d (n "make-cmd") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "1dnvvxgrd2pcn95k8w5547q5bgr2r1y5z818dbq5ris06gy3g4y4")))

(define-public crate-libthemis-src-0.13.0 (c (n "libthemis-src") (v "0.13.0") (d (list (d (n "make-cmd") (r "^0.1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.5") (d #t) (k 2)))) (h "174dj59gx4mvaac97ixpgms09g9kyi1x29li1237pw8rhg75004x") (y #t)))

