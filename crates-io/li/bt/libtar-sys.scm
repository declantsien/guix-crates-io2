(define-module (crates-io li bt libtar-sys) #:use-module (crates-io))

(define-public crate-libtar-sys-0.0.1 (c (n "libtar-sys") (v "0.0.1") (h "00h2fwmkbsaz95mklqzrz2dym2i3m1a69a6p3lir447s4jf40j1n")))

(define-public crate-libtar-sys-0.0.2 (c (n "libtar-sys") (v "0.0.2") (h "0mw8i5d4i7g58744acpf61iab1ghcwahc35cmndw694z7jim82ng")))

(define-public crate-libtar-sys-0.0.3 (c (n "libtar-sys") (v "0.0.3") (h "0yv941jj494m710la03p0h1p645i31lrl74sngv4hfy21r75zvcw")))

(define-public crate-libtar-sys-0.0.4 (c (n "libtar-sys") (v "0.0.4") (h "1qcd5fsqajr1n0kwspcnr50kxmjkdklhj0rnpibncyrs43n41la0")))

(define-public crate-libtar-sys-0.0.5 (c (n "libtar-sys") (v "0.0.5") (h "1a9wc4xz5d01jb5mwyrnvy3zp67x6x89dl4q8m9b7l4kzdia7wfl")))

(define-public crate-libtar-sys-0.0.6 (c (n "libtar-sys") (v "0.0.6") (h "016a4i3p3zgkrhbp7fnp273n5mjnig9nc73f7pj1qg96ypc9h9g6")))

(define-public crate-libtar-sys-0.0.7 (c (n "libtar-sys") (v "0.0.7") (h "1c512p3fbfl8jmhcpasiq9ik48jmdhcfvfwr1x7cr1h1l8k5qmlf")))

(define-public crate-libtar-sys-0.0.8 (c (n "libtar-sys") (v "0.0.8") (h "1aq29xmjaqrzx8inmq57mf83g0dcc6llgmwaldh50i90wr0yzk9c")))

(define-public crate-libtar-sys-0.0.9 (c (n "libtar-sys") (v "0.0.9") (h "0790cqn5fyn5l1wa4d6lab1vgi3wzrfrbw8n1r4pmbk1lhhslc9n")))

(define-public crate-libtar-sys-0.1.0 (c (n "libtar-sys") (v "0.1.0") (h "1b6qib2kzmmih94ypiyvzr8rn657acmc2ik3k3chypdsfc9gfzj1")))

(define-public crate-libtar-sys-0.1.1 (c (n "libtar-sys") (v "0.1.1") (h "0r44hgsggvw51w4szxwbyjgk4g7a8pxn7pwd6mfr321hmsgb0q2p")))

(define-public crate-libtar-sys-0.1.2 (c (n "libtar-sys") (v "0.1.2") (h "10m3bnvy9qxnd2cl0g08lgcb445izzspw4cbny7k8j5l9cylvx4a")))

(define-public crate-libtar-sys-0.1.3 (c (n "libtar-sys") (v "0.1.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ph2cgrk2dbsgnlwmcihpjn1n55dpfw9r4rw3w95jxwqrjqphb5d")))

(define-public crate-libtar-sys-0.1.4 (c (n "libtar-sys") (v "0.1.4") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1298fsghgpvgjj0x4rk48dih0gm3ph4278rdaly4cm6hzsr2gkk8")))

(define-public crate-libtar-sys-0.1.5 (c (n "libtar-sys") (v "0.1.5") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "19pbhr6g6kyigkc9mzf7gqza27dyq0x9cyhqbilgfnzdp89rdhk3")))

(define-public crate-libtar-sys-0.1.6 (c (n "libtar-sys") (v "0.1.6") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ivvjlx1yf18i7nfizg97d4547n9g51ssy2m2lphlyxsm971bsf6")))

(define-public crate-libtar-sys-0.1.7 (c (n "libtar-sys") (v "0.1.7") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0zx8n8mv1i1zhlrm2yp1vry8713ip0hhan9r0xybdn1rbgjq9m99")))

(define-public crate-libtar-sys-0.1.8 (c (n "libtar-sys") (v "0.1.8") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "03hhln6v037dr3s62nmwfm9813wihxpm0y3hisidgkjpkhghzsxy")))

(define-public crate-libtar-sys-0.1.9 (c (n "libtar-sys") (v "0.1.9") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "13awb3nypphdxlvk2v7g91mva82x1ac8lrm77nfpsnf6i5ppba5w")))

