(define-module (crates-io li bt libtif) #:use-module (crates-io))

(define-public crate-libtif-0.1.0 (c (n "libtif") (v "0.1.0") (h "1fxivq06aj9y2v85pfm1jqkc66xsz4nnvxbmm0lm7gjynxwz39sm")))

(define-public crate-libtif-0.1.1 (c (n "libtif") (v "0.1.1") (h "04zvbfl8msjaz5w5adhlz4l74x4j22hrpxnhs8sl9986kirsjszb")))

(define-public crate-libtif-0.1.2 (c (n "libtif") (v "0.1.2") (h "1l09jb6544xic71pfm5v1fcr40q2dn97d6jdx7y745hf5wiz9lqs")))

(define-public crate-libtif-0.1.3 (c (n "libtif") (v "0.1.3") (h "1ijw7f4wp7k15zzpw7bcg8aprrpl23q66i86iv76zsr057hq14nn")))

(define-public crate-libtif-0.1.4 (c (n "libtif") (v "0.1.4") (h "1h8w0kwnn9l4w1i0a9x6n98pl65xvw9s300fsbwgkzczyyqwgz4v")))

