(define-module (crates-io li bt libthemis-sys) #:use-module (crates-io))

(define-public crate-libthemis-sys-0.0.2 (c (n "libthemis-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.42.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1hbfg6dybgy4bfcbzy60ifmbi3964n27141n7vcjq309slnifbik") (l "themis")))

(define-public crate-libthemis-sys-0.0.2-1 (c (n "libthemis-sys") (v "0.0.2-1") (d (list (d (n "bindgen") (r "^0.42.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libthemis-src") (r "= 0.0.2-1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1g43f481hwn7px2d5g0rzv2j5dhlykwqyfa5g77z00bxvz72lqb7") (f (quote (("vendored" "libthemis-src")))) (y #t) (l "themis")))

(define-public crate-libthemis-sys-0.0.2-2 (c (n "libthemis-sys") (v "0.0.2-2") (d (list (d (n "bindgen") (r "^0.42.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libthemis-src") (r "= 0.0.2-1") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1jc6k4263yf6qlpf11zjzisx5qifkrybdkkgpqakf2qgy0y4j4n6") (f (quote (("vendored" "libthemis-src")))) (y #t) (l "themis")))

(define-public crate-libthemis-sys-0.0.3 (c (n "libthemis-sys") (v "0.0.3") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libthemis-src") (r "= 0.0.3") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "01aqk1wr97mkgr2f75845gj6sgn3pksx2banmc333w6diyx4qrns") (f (quote (("vendored" "libthemis-src")))) (l "themis")))

(define-public crate-libthemis-sys-0.11.0 (c (n "libthemis-sys") (v "0.11.0") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libthemis-src") (r "= 0.11.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0z6b3qydhff0ppqrgm58xlqdr3viqv50a71qs47g3wvnnxkknipn") (f (quote (("vendored" "libthemis-src")))) (l "themis")))

(define-public crate-libthemis-sys-0.11.1 (c (n "libthemis-sys") (v "0.11.1") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libthemis-src") (r "^0.11.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "09jkcqwbymhffi642r9iix63cfknxzjz187rpr9p7byfarymi09w") (f (quote (("vendored" "libthemis-src")))) (l "themis")))

(define-public crate-libthemis-sys-0.12.0 (c (n "libthemis-sys") (v "0.12.0") (d (list (d (n "bindgen") (r "^0.46.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libthemis-src") (r "^0.12.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1v849jnbp023jmkw924wqb47q84zfaap5a2plqbx42h970c517jc") (f (quote (("vendored" "libthemis-src")))) (l "themis")))

(define-public crate-libthemis-sys-0.13.0 (c (n "libthemis-sys") (v "0.13.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "libthemis-src") (r "^0.13.0") (o #t) (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "024bwg0xy2gm36gzgwa2vihj46xjrxvwdjyjwrf8m764gwh6lv6c") (f (quote (("vendored" "libthemis-src")))) (l "themis")))

(define-public crate-libthemis-sys-0.14.0 (c (n "libthemis-sys") (v "0.14.0") (d (list (d (n "cc") (r "^1.0.28") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1mgz56b7p5lk6fgpwimfy9680zyd6f7q29ipk78x1x0ddarydn1p") (l "themis")))

