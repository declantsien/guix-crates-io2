(define-module (crates-io li sn lisnake) #:use-module (crates-io))

(define-public crate-lisnake-1.0.1 (c (n "lisnake") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.3") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 0)) (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "lighthouse-client") (r "^3.0.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tokio") (r "^1.36.0") (f (quote ("rt" "macros" "time"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter" "std"))) (d #t) (k 0)))) (h "0klcd9prvvkbb7n9f9r9vd77b2hnlmqyr3mzz4kiljvm4ccm2hrr")))

