(define-module (crates-io li ch lichess-bot) #:use-module (crates-io))

(define-public crate-lichess-bot-0.1.0 (c (n "lichess-bot") (v "0.1.0") (h "168g1gcxcqf1ibnvskr63fcg661ws94fcyb0kd7a0x1x4yw299jw")))

(define-public crate-lichess-bot-0.1.1 (c (n "lichess-bot") (v "0.1.1") (h "182a79a19nxz3ldmzc7akbq8l6w0w2bk5h987v31m7m4daksnq6i")))

