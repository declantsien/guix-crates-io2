(define-module (crates-io li ch lichen_core) #:use-module (crates-io))

(define-public crate-lichen_core-0.1.6 (c (n "lichen_core") (v "0.1.6") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "lol_html") (r "^1.2.1") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.4.0") (d #t) (k 0)) (d (n "url") (r "^2.5.0") (d #t) (k 0)))) (h "1hnf521hyq5pq65y0zn0jyfzflvpasy7frd0qmw0b70mli6a3pzr")))

