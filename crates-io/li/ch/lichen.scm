(define-module (crates-io li ch lichen) #:use-module (crates-io))

(define-public crate-lichen-0.1.0 (c (n "lichen") (v "0.1.0") (h "1ikvxqny1djfjyyj8y3s9cy65sjc2py6m0g86v17ipqs3hbr188d")))

(define-public crate-lichen-0.2.0 (c (n "lichen") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1z5mqls8ixf2vnb3g2sgscvcml0nd1gh1ry2rs3rn3nslz81smm6")))

(define-public crate-lichen-0.2.1 (c (n "lichen") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1k4a64adkapbj3y6lk5dfd318rn82hm9dwyhavrqyjb1xl8b5xsf")))

(define-public crate-lichen-0.2.2 (c (n "lichen") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "069wawz3k4n9s93hpy132r59ijx8igqv27wqjbkq4q7aghnwrvcs")))

(define-public crate-lichen-0.2.3 (c (n "lichen") (v "0.2.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "17x13jdg05i6lgm748063pybds5z30nsfq7dlpl94d7mahkzg567")))

(define-public crate-lichen-0.2.4 (c (n "lichen") (v "0.2.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "16z9c9fkf99cw1sdygsvy5xpgscp88j20z21kk5gpv3nzjhjlc5m")))

(define-public crate-lichen-0.2.5 (c (n "lichen") (v "0.2.5") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0bv5amvqas6n2p2bn8df0s7iigadd21g0xqk2mqbb9cy1q1plknw")))

(define-public crate-lichen-0.2.6 (c (n "lichen") (v "0.2.6") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0j17m8pkmkry98vjn06dzj436vkvf76yhrzflvq8j5c3g5qg2ywx")))

(define-public crate-lichen-0.2.7 (c (n "lichen") (v "0.2.7") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "13m3h14iic9d71g4f9mg5x46b965z4j7p1wcyywj6mfrfyfbqd1q")))

(define-public crate-lichen-0.2.8 (c (n "lichen") (v "0.2.8") (h "0skzwszr2rgbjzn5cabaizhjvvkm09krvyz7pb7c44hscgfcsw6b")))

(define-public crate-lichen-0.2.9 (c (n "lichen") (v "0.2.9") (h "0vaj13fy077a7f0hfj4vdqhwd3flq9yf4m7jd5xhxbhprp0y2b5l")))

(define-public crate-lichen-0.2.10 (c (n "lichen") (v "0.2.10") (h "0lfkpgpqb3xzifam6y2vz31nsv917kjkq13mpq4kj090qrqhsj1z")))

(define-public crate-lichen-0.2.11 (c (n "lichen") (v "0.2.11") (h "17sba5ccd7bsam819rg579v5hx7xa9b2q4zlfiblm2mr5v20npjp")))

(define-public crate-lichen-0.2.12 (c (n "lichen") (v "0.2.12") (h "0x0gqav6slphv9hjn23xr06na138120yc91mla3w4s4r0v6h9mxw")))

(define-public crate-lichen-0.2.13 (c (n "lichen") (v "0.2.13") (h "0qx0fyd5g7wc12n0nq6192zmjwbn41naz0y4h6qd10xnb01ws31i")))

(define-public crate-lichen-0.2.14 (c (n "lichen") (v "0.2.14") (h "0bisbcx4lpig8ckvygg3rq62wz9yjnprhi9hb437by53zj5cc28h")))

(define-public crate-lichen-0.2.15 (c (n "lichen") (v "0.2.15") (h "1qgs16ab5lwfh8md85cap6bzr96rjyq123pqr3hgf59a2zgz8m00")))

(define-public crate-lichen-0.2.16 (c (n "lichen") (v "0.2.16") (h "1cmhbsbxws0i64ygs596ipcay315175fczq4knjvg2893yr49l4n")))

(define-public crate-lichen-0.2.17 (c (n "lichen") (v "0.2.17") (h "04kxpl8yhndpx71a7vh3syrl6742jvhpn3simaddkb2hcrl7xsjx")))

(define-public crate-lichen-0.2.18 (c (n "lichen") (v "0.2.18") (h "1w2kpi859vapvgk9045d00ynaq5rm3yqqc6ihpfn3jxfk6bwgavx")))

(define-public crate-lichen-0.2.20 (c (n "lichen") (v "0.2.20") (h "1gcfrg30grbkmp04z4r7rxnq72v1i3mn1fgv33qbs8zk0nsd3l6v")))

(define-public crate-lichen-0.2.21 (c (n "lichen") (v "0.2.21") (h "1scpxdsp09f98sbziqqllaq869rhwdbwg628rbg94zwpyssca1s0")))

(define-public crate-lichen-0.2.22 (c (n "lichen") (v "0.2.22") (h "14zpgdh9qph79hsnhvi9gaavx2klq1sq19z5m873pmhyy31pvdvi")))

(define-public crate-lichen-0.2.23 (c (n "lichen") (v "0.2.23") (h "0w586nblm7qdwa8ca3rl8fpx1wjlsvwh62nlialjsh0ss42vvdab")))

(define-public crate-lichen-0.2.24 (c (n "lichen") (v "0.2.24") (h "0v0jprksann02pcy6587srpmx4s4z5x554h76nx7y3prnkxjg560") (y #t)))

(define-public crate-lichen-0.2.25 (c (n "lichen") (v "0.2.25") (h "13jf3w0myjma9vcw2yr5fbhaasfig67g3iafljjn63gd8f5ibyyj")))

(define-public crate-lichen-0.2.26 (c (n "lichen") (v "0.2.26") (h "1s7jybxczcxm24581b6vrbkbmg3lddyxqkzwrqi3kc8vb63axv08")))

(define-public crate-lichen-0.2.27 (c (n "lichen") (v "0.2.27") (h "0060wc8x16qf09ndyzymnw694j56zsvg0lm56s0vw613lqs89nhv")))

(define-public crate-lichen-0.2.28 (c (n "lichen") (v "0.2.28") (h "1qvch3xdqz8aymrc5wvy57nbwy0pziphxgqcz9ynzmh9bbswiwwl")))

(define-public crate-lichen-0.2.29 (c (n "lichen") (v "0.2.29") (h "0wqbdvw2g33i77cggr43kk0jvvcc3k5vigcjcswhfg0wn9bnmzw8")))

(define-public crate-lichen-0.2.30 (c (n "lichen") (v "0.2.30") (h "12bpfhv1di4lasylw4i0c17079dmqdq49kmzkn1fl7m4s17yq73b") (y #t)))

(define-public crate-lichen-0.2.31 (c (n "lichen") (v "0.2.31") (h "01vgn4hjzg0hhh6205avw4868f5l0w70fwbynb4q3rmhay7w8f8l")))

(define-public crate-lichen-0.2.32 (c (n "lichen") (v "0.2.32") (h "14r61yyds3qvdp3dwh2g6c5bc2crqihy579bhjjvmil5cwxy5lvd")))

(define-public crate-lichen-0.2.33 (c (n "lichen") (v "0.2.33") (h "0fqa5602hsvhcnnnq64chc4nfn8kngpc03sfiiiihx782dldv95w")))

(define-public crate-lichen-0.3.0 (c (n "lichen") (v "0.3.0") (h "1rlwjbq1js3bc6z1i3n3g90pnrvb3li5gw1ihi627bmgwg8dsh8x")))

(define-public crate-lichen-0.3.1 (c (n "lichen") (v "0.3.1") (h "0xy9f1ysziwxdnvglzfrp5wrjvqi8rw6ypgnrb59xs6k3g5g6x13")))

(define-public crate-lichen-0.3.2 (c (n "lichen") (v "0.3.2") (h "08xynvkikbrhjw11zfr4rx4h5l2vbw6zbyq11198ifk4nn3pxvpa")))

(define-public crate-lichen-0.3.3 (c (n "lichen") (v "0.3.3") (h "19b7np5mcg3igi1x9rkjk48n35pnb6spnfkyc9z10gdw8b26xdnq")))

(define-public crate-lichen-0.3.4 (c (n "lichen") (v "0.3.4") (h "1559z4ia5f1kqf8zhydn87ayzmph1h0r14jjwh19hyzqc3qilsiw")))

(define-public crate-lichen-0.3.5 (c (n "lichen") (v "0.3.5") (h "1rqhm29yngz4p87spfblr29xff28van3v3bxh80kg122vskmflqm")))

(define-public crate-lichen-0.3.6 (c (n "lichen") (v "0.3.6") (h "1przzy9fn7qc4fpxa50s9438rynv56mih5r4zm8wgrq0d4lwa3v1")))

(define-public crate-lichen-0.3.7 (c (n "lichen") (v "0.3.7") (h "09awiak898km0g8xhw9rbalkmll046nx21iyyl5vadrv4c98qxg5")))

(define-public crate-lichen-0.3.8 (c (n "lichen") (v "0.3.8") (h "1xhccnrwnn0l4w0dxjxw2mxqvfyyp485as4j8gvs5zys3x6mp02g")))

