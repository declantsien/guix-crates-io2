(define-module (crates-io li fi lifi_client) #:use-module (crates-io))

(define-public crate-lifi_client-0.0.1 (c (n "lifi_client") (v "0.0.1") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)))) (h "1kwpnddngsh9zaxfbfr0a3m5qzl79frgk9vd2kdrnbbcbddfr1c7")))

(define-public crate-lifi_client-0.0.2 (c (n "lifi_client") (v "0.0.2") (d (list (d (n "assert_cmd") (r "^0.10") (d #t) (k 2)) (d (n "exitfailure") (r "^0.5.1") (d #t) (k 0)) (d (n "failure") (r "^0.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "ra_common") (r "^0.0.2") (d #t) (k 0)))) (h "1qgza17i8j0yj02bidfiq064l8gq06bkfpfpkli54bnmp93vgv0f")))

