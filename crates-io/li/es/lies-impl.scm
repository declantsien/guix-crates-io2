(define-module (crates-io li es lies-impl) #:use-module (crates-io))

(define-public crate-lies-impl-0.0.0-rc1 (c (n "lies-impl") (v "0.0.0-rc1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0ca2vijwakb7psksmfrpizj97s05kn8xp8s3nwvq8lad5wwrhfvx")))

(define-public crate-lies-impl-0.0.1 (c (n "lies-impl") (v "0.0.1") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1rqmprvlsczjb8n1fqndnpwf6qil3f0gbp8yrrvir7dgi9xz4s9q")))

(define-public crate-lies-impl-0.0.2 (c (n "lies-impl") (v "0.0.2") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1pwy9vbhm2f1sf0r7mpldvnq6m1k96nzs9mbzivkalq10d6qdbrb")))

(define-public crate-lies-impl-0.0.3 (c (n "lies-impl") (v "0.0.3") (d (list (d (n "cargo_metadata") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1s18qvyajidrw0hhfkcwnyvk2kv10gkckvg8xjwkrh90pj5gmjvs") (f (quote (("about-per-workspace") ("about-per-crate"))))))

