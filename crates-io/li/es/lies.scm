(define-module (crates-io li es lies) #:use-module (crates-io))

(define-public crate-lies-0.0.0-rc1 (c (n "lies") (v "0.0.0-rc1") (d (list (d (n "lies-impl") (r "^0.0.0-rc1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0g93hrgzxnnl2m4md615k6bhdsanb1lp3lb91x25q35wwd56qrhw")))

(define-public crate-lies-0.0.1 (c (n "lies") (v "0.0.1") (d (list (d (n "lies-impl") (r "^0.0.1") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "10bbyzvb0bd6vz1xd3rh9863zvm1hipq925mj8dbvjnyf5vwh28j")))

(define-public crate-lies-0.0.2 (c (n "lies") (v "0.0.2") (d (list (d (n "lies-impl") (r "^0.0.2") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "1334nyynxbnfgd600jxpr3qi9p3fw5b2krlfkwvqnxf268cpzk14")))

(define-public crate-lies-0.0.3 (c (n "lies") (v "0.0.3") (d (list (d (n "lies-impl") (r "^0.0.3") (d #t) (k 0)) (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)))) (h "0ha5763lm8p21s9c9a8ndnhvrq7jfbr32vgw5f2f1sdrls1cxjkd") (f (quote (("about-per-workspace" "lies-impl/about-per-workspace") ("about-per-crate" "lies-impl/about-per-crate"))))))

