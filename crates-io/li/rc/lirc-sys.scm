(define-module (crates-io li rc lirc-sys) #:use-module (crates-io))

(define-public crate-lirc-sys-0.0.0 (c (n "lirc-sys") (v "0.0.0") (d (list (d (n "nix") (r "^0.12.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8.0") (d #t) (k 2)))) (h "08fa3kyq2d1lyvxmm49j9vgjpl361q615pyf17i9hc6sck6zkajd")))

(define-public crate-lirc-sys-0.0.1 (c (n "lirc-sys") (v "0.0.1") (d (list (d (n "nix") (r "^0.17.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.9.2") (d #t) (k 2)))) (h "0qxbsfwg7njpr9xnsl6ds6f8w4gjxf7fn1xl36qm5yr82fyi37bi")))

