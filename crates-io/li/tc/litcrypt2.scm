(define-module (crates-io li tc litcrypt2) #:use-module (crates-io))

(define-public crate-litcrypt2-0.1.0 (c (n "litcrypt2") (v "0.1.0") (d (list (d (n "expectest") (r "^0.12.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0c1ffiyv7n19759giqwq7554m6pyqkbfhpq42l081mpmcy4qhd3k")))

(define-public crate-litcrypt2-0.1.1 (c (n "litcrypt2") (v "0.1.1") (d (list (d (n "expectest") (r "^0.12.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1vrj3aa5n5s73kn0ssrw3549h07pgfwgdjr299x76g7gn8fnnhgk")))

(define-public crate-litcrypt2-0.1.2 (c (n "litcrypt2") (v "0.1.2") (d (list (d (n "expectest") (r "^0.12.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "03f6bfg2h234cmh5dbnnlhcwcx6z5jbs49y8bc5d4g8vmibsl9j1")))

