(define-module (crates-io li tc litcrypt) #:use-module (crates-io))

(define-public crate-litcrypt-0.2.0 (c (n "litcrypt") (v "0.2.0") (d (list (d (n "expectest") (r "^0.12.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)))) (h "1aakydd3hbfjwny5z8cvh6qhdqzq9shh086kgcrnbalvh8507725")))

(define-public crate-litcrypt-0.2.1 (c (n "litcrypt") (v "0.2.1") (d (list (d (n "expectest") (r "^0.12.0") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)))) (h "0hfn1dvddbm3jxg9yzimgak4v7zc5pr2yix6lxg36sk8947yjljl")))

(define-public crate-litcrypt-0.3.0 (c (n "litcrypt") (v "0.3.0") (d (list (d (n "expectest") (r "^0.12.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.24") (d #t) (k 0)) (d (n "quote") (r "^1.0.8") (d #t) (k 0)))) (h "1ir0qrb9zsj2s5xkpa07an1ynzz6fiw9nicvaqx1pm6rcqhgk0jz")))

