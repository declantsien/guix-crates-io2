(define-module (crates-io li bm libmodbus-sys) #:use-module (crates-io))

(define-public crate-libmodbus-sys-0.4.0 (c (n "libmodbus-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "07r2iv74y8mk8bwrfrf2zc0zns338cvadhhx293q1ikr9bjip5m8")))

(define-public crate-libmodbus-sys-0.4.1 (c (n "libmodbus-sys") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "04libjiw6j89mqf2rjsbib5zqih6shzzs0vb3bdh1j8jpb6dvh3y")))

(define-public crate-libmodbus-sys-0.4.2 (c (n "libmodbus-sys") (v "0.4.2") (d (list (d (n "bindgen") (r "^0.25") (d #t) (k 1)) (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1bhm95l7rkypd14iijw4vqbamamzxijs5m6iyzzdfkjal9hcfrl0")))

(define-public crate-libmodbus-sys-0.5.0 (c (n "libmodbus-sys") (v "0.5.0") (d (list (d (n "bindgen") (r "^0.30.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0pplzf7yp9kavgcca4n94zrw5cpqd1a9q8giqz78fab09z947ijs")))

(define-public crate-libmodbus-sys-0.7.0 (c (n "libmodbus-sys") (v "0.7.0") (d (list (d (n "bindgen") (r "^0.31.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "14ajjcr3y723pi0ai4hfqxw13w7l2pzv6k7pk5c115w47hcf13mn")))

(define-public crate-libmodbus-sys-0.8.0 (c (n "libmodbus-sys") (v "0.8.0") (d (list (d (n "bindgen") (r "^0.31.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1aaqmwn520mh318vk3030gbva773v1d30i2rcb4rp6hp95hbf3yn")))

(define-public crate-libmodbus-sys-1.0.0 (c (n "libmodbus-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.19") (d #t) (k 1)))) (h "00bs4b96ybq19f3mqgk7xx5bxmh1ygggq8j00d42gv2krgmbzwzd")))

