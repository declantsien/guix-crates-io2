(define-module (crates-io li bm libmatrix-client) #:use-module (crates-io))

(define-public crate-libmatrix-client-0.0.1 (c (n "libmatrix-client") (v "0.0.1") (d (list (d (n "mockito") (r "^0.20") (d #t) (k 2)) (d (n "olm-rs") (r "^0.2") (d #t) (k 0)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.1") (d #t) (k 0)))) (h "0q99m796mzifzd9l08vwmgchfmkfi6f3nwkx1ggawwvz4a6bb15q") (y #t)))

