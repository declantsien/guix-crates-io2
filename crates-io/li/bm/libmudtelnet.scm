(define-module (crates-io li bm libmudtelnet) #:use-module (crates-io))

(define-public crate-libmudtelnet-2.0.1 (c (n "libmudtelnet") (v "2.0.1") (d (list (d (n "arbitrary") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)))) (h "0nv6apww46kldbrv29krmdx6v6sb290brqj05fvw553nlhpqx2m5") (f (quote (("std") ("default" "std"))))))

