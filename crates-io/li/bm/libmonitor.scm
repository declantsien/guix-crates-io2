(define-module (crates-io li bm libmonitor) #:use-module (crates-io))

(define-public crate-libmonitor-0.1.0 (c (n "libmonitor") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "i2cdev") (r "^0.6.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "udev") (r "^0.8.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "0z3f2k4nf4prf0nzcpgnb0a4vd9750m5918wbrb2mdfpac5xzzlq")))

(define-public crate-libmonitor-0.1.1 (c (n "libmonitor") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.83") (d #t) (k 0)) (d (n "i2cdev") (r "^0.6.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.203") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.60") (d #t) (k 0)) (d (n "udev") (r "^0.8.0") (d #t) (t "cfg(target_os = \"linux\")") (k 0)))) (h "17zzga2dki1gkiijqbdkxsd2qdpcxaq87vn7fz111y75wc31li75") (f (quote (("default")))) (s 2) (e (quote (("serde" "dep:serde"))))))

