(define-module (crates-io li bm libmdb-sys) #:use-module (crates-io))

(define-public crate-libmdb-sys-0.1.0 (c (n "libmdb-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0p5k3piigm51v4y2gmdw4l8f97k7r3y23pfnw1hf6l0r0qxrnvw2")))

(define-public crate-libmdb-sys-0.1.1 (c (n "libmdb-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)) (d (n "glib-sys") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1srbjx6156hhmriyjbjxm867fm659g6hwc9f5w0b8dx0fvyr9lgf")))

(define-public crate-libmdb-sys-0.1.2 (c (n "libmdb-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "glib-sys") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1c6cyllsd5vgw6769xgijas9rhvinjn0s4bsrlfffzzgp2qg5n2l")))

