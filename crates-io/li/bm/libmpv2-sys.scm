(define-module (crates-io li bm libmpv2-sys) #:use-module (crates-io))

(define-public crate-libmpv2-sys-4.0.0 (c (n "libmpv2-sys") (v "4.0.0") (d (list (d (n "bindgen") (r "^0.69.4") (o #t) (d #t) (k 1)))) (h "0mnvvkvr8whms4ypw8c3ryai7z33xx7gcvjy3pjfvgk10nlln4j2") (f (quote (("use-bindgen" "bindgen"))))))

