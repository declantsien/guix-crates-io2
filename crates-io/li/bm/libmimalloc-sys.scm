(define-module (crates-io li bm libmimalloc-sys) #:use-module (crates-io))

(define-public crate-libmimalloc-sys-0.1.0 (c (n "libmimalloc-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0l0kbzmr15fh14h3jnvc5f7v6kaqqcd40nbrnmkx3afh7npsf493") (f (quote (("no_secure"))))))

(define-public crate-libmimalloc-sys-0.1.1 (c (n "libmimalloc-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "09h9zlq6x20vkmdzbin893727d7j2w1yz8zd4gdbg7djxrwn56k0") (f (quote (("no_secure"))))))

(define-public crate-libmimalloc-sys-0.1.2 (c (n "libmimalloc-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13pbq1647wcy2riypn58k96l4y24f1km873s99gss3wj9xzmkwfq") (f (quote (("secure"))))))

(define-public crate-libmimalloc-sys-0.1.3 (c (n "libmimalloc-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jimrlmxx4n5s4kiqmf85az8vaqvmhz9pmyabwvr2v7idi4mrg2d") (f (quote (("secure"))))))

(define-public crate-libmimalloc-sys-0.1.4 (c (n "libmimalloc-sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0swir0fzwmxs8kknjjxv0r4zcjapmlk66532b1l95gp5wd1b7qmx") (f (quote (("secure"))))))

(define-public crate-libmimalloc-sys-0.1.5 (c (n "libmimalloc-sys") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d4lpf7wlqzcray3x87hmw5a4c74y2zyl5ggxap2n2l4l3riyd80") (f (quote (("secure"))))))

(define-public crate-libmimalloc-sys-0.1.6 (c (n "libmimalloc-sys") (v "0.1.6") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "05spggnwwx8ydz042ml71l6p681psfgwblrk7d3k6wq3rysw5p77") (f (quote (("secure"))))))

(define-public crate-libmimalloc-sys-0.1.7 (c (n "libmimalloc-sys") (v "0.1.7") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m132rli6kvipp96j3fnvadlr6mm62lln3r248d2yp5aabrvafk3") (f (quote (("secure"))))))

(define-public crate-libmimalloc-sys-0.1.8 (c (n "libmimalloc-sys") (v "0.1.8") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1bp7ji5y5qywh6hh40wkrggpy38xl1fabi4z67bbmglpf3s9zjm0") (f (quote (("secure-full") ("secure"))))))

(define-public crate-libmimalloc-sys-0.1.9 (c (n "libmimalloc-sys") (v "0.1.9") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sx9iqcvzzsg6zjhipf3cw2vc7q3m5r01rpi85k7dwzcik0cdh2w") (f (quote (("secure-full") ("secure"))))))

(define-public crate-libmimalloc-sys-0.1.10 (c (n "libmimalloc-sys") (v "0.1.10") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g5wlzrmwvalb9bprbph10h8ikwwy583cwkk98g6kvd0fp7p3nnj") (f (quote (("secure-full") ("secure"))))))

(define-public crate-libmimalloc-sys-0.1.11 (c (n "libmimalloc-sys") (v "0.1.11") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0vw2fgcyvifjqgjbkbpf309cf73gpz8c60i9w1ny004zc1n3xlq4") (f (quote (("secure-full") ("secure"))))))

(define-public crate-libmimalloc-sys-0.1.12 (c (n "libmimalloc-sys") (v "0.1.12") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1r3778qa3w2x5iqz4r3ywz52n31gwcpykz3njqy0773ncxsl8bvd") (f (quote (("secure-full") ("secure"))))))

(define-public crate-libmimalloc-sys-0.1.13 (c (n "libmimalloc-sys") (v "0.1.13") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0nbl0yqy4abl962sppdcl3bir4982wr4zz474hhiy7fdqcphj06y") (f (quote (("secure-full") ("secure"))))))

(define-public crate-libmimalloc-sys-0.1.14 (c (n "libmimalloc-sys") (v "0.1.14") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1gd0065imijr24zbpcz3bgn18hrdanni9gn6xfz726sal9xglzxq") (f (quote (("secure"))))))

(define-public crate-libmimalloc-sys-0.1.15 (c (n "libmimalloc-sys") (v "0.1.15") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0x2fwar9bwmdiymgfd63dyskldhbllnmgg1c2kb0skhc3pn54wm2") (f (quote (("secure"))))))

(define-public crate-libmimalloc-sys-0.1.16 (c (n "libmimalloc-sys") (v "0.1.16") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "127x7v82y227x2gkcnbay6lx2b505dq9ny47dd4p5g8lkgklnz37") (f (quote (("secure") ("override") ("extended" "cty")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.17 (c (n "libmimalloc-sys") (v "0.1.17") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0s1k8dq1szpqfgqmcyv3p294v5iq9nzlkzbrhdz80jmpvx45gmib") (f (quote (("secure") ("override") ("extended" "cty")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.18 (c (n "libmimalloc-sys") (v "0.1.18") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0bbm03687j9fspvk6nqspmjlvchlvbxydl0mrc1x9i1k6kqiy5c2") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.19 (c (n "libmimalloc-sys") (v "0.1.19") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "12l8i7fwdi2allc004my8gzd1imr1kf1bh3phmbff6gkca1xg0j7") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.20 (c (n "libmimalloc-sys") (v "0.1.20") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0r0fwkpfj33w9wl1w9c6rx5k9d34rnbzsrcccwvda3ja8av453z5") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.21 (c (n "libmimalloc-sys") (v "0.1.21") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0p76cyjkl5i9a18k7m5yc5xjqbixmzjfxvzhd76i31pmsacwz5i3") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.22 (c (n "libmimalloc-sys") (v "0.1.22") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "008wbcdcqk14kyblsl5xw2xspnqk5swm1z214y6ainwkqmwq86qx") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.23 (c (n "libmimalloc-sys") (v "0.1.23") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0c1whqvlzpxlgzbbn8nj9f89za8ia2k9jbyz196kyj6vz6ac2dln") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.24 (c (n "libmimalloc-sys") (v "0.1.24") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0s8ab4nc33qgk9jybpv0zxcb75jgwwjb7fsab1rkyjgdyr0gq1bp") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.25 (c (n "libmimalloc-sys") (v "0.1.25") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0b1fi84giac7lsg0v2p39xbnn735prnqr2gpbpslh12ma9h17jhi") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.26 (c (n "libmimalloc-sys") (v "0.1.26") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0j7bi0sq3jgqfyzgqp1qxz3jkgj2jnfappqvmaizs2wv52mr7h4g") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.27 (c (n "libmimalloc-sys") (v "0.1.27") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0q9kv6j25cxjprh27ij06iw56xj6whgdyg9h9f92bby1h2qnfxf3") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.28 (c (n "libmimalloc-sys") (v "0.1.28") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1jb0x8a3h3ahmwi57n91fwpf3bgykkh0jczyligvgrl3xdywdl84") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.29 (c (n "libmimalloc-sys") (v "0.1.29") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0q3hxbv7mgrqyfggkkiqf4jcgg3vmrhl88mvg95v52pshf644yrl") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (y #t) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.30 (c (n "libmimalloc-sys") (v "0.1.30") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x5iczg0q6wnd77c9c1nxbvpsnnmwrr7ad3ycs1rc0c9ifzpr36x") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.31 (c (n "libmimalloc-sys") (v "0.1.31") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0xzqymz323pgcvif672ccwacp6kn2pbyzn5y85yz625i3w04ab7g") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (y #t) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.32 (c (n "libmimalloc-sys") (v "0.1.32") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h6zn14lpabnw5bgdyyfq58nwka01dcbqy4czixkrg0iv7imi9a3") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.33 (c (n "libmimalloc-sys") (v "0.1.33") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0pjix8fbm8q1fcb3zgx7ylcihnyw31b6js93x4svgwcf5j8hxb7l") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.34 (c (n "libmimalloc-sys") (v "0.1.34") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gai5w982v5x99b3a3rwmzvp7mp6prv8b50wg8nw5lgh3al5il15") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.35 (c (n "libmimalloc-sys") (v "0.1.35") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r4nrd9xbmhmipw4bvh4xlbzbc7xf74frrsibqglysffgv1vay9r") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.36 (c (n "libmimalloc-sys") (v "0.1.36") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wsbp22f7rfninzvfa480qy4gcwyinba6brxgzf1803wk3fyzvbv") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug")))) (y #t) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.37 (c (n "libmimalloc-sys") (v "0.1.37") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19qblgsdphb5kv4x2f825scdc39h80igyymw3kmfsbaqq1hl1sw1") (f (quote (("secure") ("override") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug") ("arena")))) (l "mimalloc")))

(define-public crate-libmimalloc-sys-0.1.38 (c (n "libmimalloc-sys") (v "0.1.38") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "cty") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1din0dyf8qg5hlhp6i6h21vrc3ig4fzvfy1acnpwiz1xfcyv4yqf") (f (quote (("secure") ("override") ("no_thp") ("local_dynamic_tls") ("extended" "cty") ("debug_in_debug") ("debug") ("arena")))) (l "mimalloc")))

