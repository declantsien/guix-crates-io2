(define-module (crates-io li bm libmat) #:use-module (crates-io))

(define-public crate-libmat-0.0.1 (c (n "libmat") (v "0.0.1") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "05j4ypcviz0ls7nwy2d51llrbc05hwvhhkqa16hr6ky4a92cm4fd") (y #t)))

(define-public crate-libmat-0.1.0 (c (n "libmat") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)))) (h "0z2nisv113jpii2w30fsbpmnqiy1jkppra7z9s9gyzrdgdsfdd56")))

(define-public crate-libmat-0.2.0 (c (n "libmat") (v "0.2.0") (d (list (d (n "num") (r "^0.4.0") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)))) (h "12awbx6kl3zqfjykdv94cb8l8s5zi8aspnch3bxjmbnch0kwkmk0")))

