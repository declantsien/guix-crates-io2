(define-module (crates-io li bm libmotor-sys) #:use-module (crates-io))

(define-public crate-libmotor-sys-0.1.0 (c (n "libmotor-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0xj541mjzlp1zj6nincaggc50gvl0di5b1y9fvh9zynj9hh4kxb5") (l "cryptoc")))

