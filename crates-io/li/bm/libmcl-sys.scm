(define-module (crates-io li bm libmcl-sys) #:use-module (crates-io))

(define-public crate-libmcl-sys-0.1.0 (c (n "libmcl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "08k0gmh0hxpk8mm5zgdqlnv5hx8qr2iyvlhf0n6yphq2smn8a2pw")))

(define-public crate-libmcl-sys-0.1.1 (c (n "libmcl-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0ixl5kc25wr0mhh28y5gsm5764j7dbhvi4ircsh2z4gk040kvw1z") (f (quote (("docs-rs"))))))

(define-public crate-libmcl-sys-0.1.2-rc (c (n "libmcl-sys") (v "0.1.2-rc") (d (list (d (n "autotools") (r "^0.2.5") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "0hn6h7yd5bva3r44ry48gpvsy0hfr5ybxil97rp3ljvxiknp8235") (f (quote (("docs-rs"))))))

(define-public crate-libmcl-sys-0.1.2-rc1 (c (n "libmcl-sys") (v "0.1.2-rc1") (d (list (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1kslwyn4zrnwvic5mn3hzp0a9yiypzn0lqcrcqxmgkl2k0h3dabv") (f (quote (("shared_mem") ("pocl_extensions" "shared_mem") ("mcl_debug") ("docs-rs"))))))

(define-public crate-libmcl-sys-0.1.2-rc2 (c (n "libmcl-sys") (v "0.1.2-rc2") (d (list (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1lp00pa13bmj6fzz529vm9spl7ld1ymn4d6masv1wlf4z7n5bjav") (f (quote (("shared_mem") ("pocl_extensions" "shared_mem") ("mcl_debug") ("docs-rs"))))))

(define-public crate-libmcl-sys-0.1.2-rc3 (c (n "libmcl-sys") (v "0.1.2-rc3") (d (list (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "17jbaff7g75s33gj9f799mfip7ff8f6bkq7bmk3h1afb957plkzl") (f (quote (("shared_mem") ("pocl_extensions" "shared_mem") ("mcl_debug") ("docs-rs"))))))

(define-public crate-libmcl-sys-0.1.2 (c (n "libmcl-sys") (v "0.1.2") (d (list (d (n "autotools") (r "^0.2.6") (d #t) (k 1)) (d (n "bindgen") (r "^0.59.2") (d #t) (k 1)) (d (n "cc") (r "^1.0.77") (d #t) (k 1)) (d (n "glob") (r "^0.3.1") (d #t) (k 1)) (d (n "rand") (r "^0.8.3") (d #t) (k 2)))) (h "1bwm6qhmq23nr382nnn4yqbbxmr4m51317j27pvdkgnn5swjs0xv") (f (quote (("shared_mem") ("pocl_extensions" "shared_mem") ("mcl_debug") ("docs-rs"))))))

