(define-module (crates-io li bm libmf) #:use-module (crates-io))

(define-public crate-libmf-0.1.0 (c (n "libmf") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bfp5jp4ixlih6i25vcr13a9dzimmb8vfdyjcb89wl5phimk9lrd") (l "mf")))

(define-public crate-libmf-0.1.1 (c (n "libmf") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19hby3jksc4h5fysyzfgrz7qcix23xrlmdwy0midhj1w10y1ngkb") (l "mf")))

(define-public crate-libmf-0.2.0 (c (n "libmf") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0f1a42kqi6dpk7smcvhkhvjgwfzkd2l4j8w9sxamf4af8wwvb4s4") (l "mf")))

(define-public crate-libmf-0.2.1 (c (n "libmf") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n192556kiy0lvbx9mpr0jn7b2xniv58pgl7kks8n89ir26f3x4h") (l "mf")))

(define-public crate-libmf-0.2.2 (c (n "libmf") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xkflds1g8wz4gv45w0n3crqhr12lzzjis5mhw3l87qb2wv6p6z7") (l "mf")))

