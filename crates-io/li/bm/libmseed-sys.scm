(define-module (crates-io li bm libmseed-sys) #:use-module (crates-io))

(define-public crate-libmseed-sys-0.1.0+3.0.15 (c (n "libmseed-sys") (v "0.1.0+3.0.15") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0ivlsh2jlimj42yg3wkfmv9lgyhv09c4yycry69yxj7yzcqb4w72") (l "mseed")))

(define-public crate-libmseed-sys-0.1.1+3.0.15 (c (n "libmseed-sys") (v "0.1.1+3.0.15") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "0fncbfxb9sigm9qfy8y3aaihdlklfvkbg8j1gs8cq95p4bcfi9xb") (l "mseed")))

(define-public crate-libmseed-sys-0.1.2+3.0.15 (c (n "libmseed-sys") (v "0.1.2+3.0.15") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1v48d7r8jn4fijk7h32sr63jvvz5rd4w7zzlg6npa8j9bl53xv28") (l "mseed")))

(define-public crate-libmseed-sys-0.2.0+3.0.17 (c (n "libmseed-sys") (v "0.2.0+3.0.17") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1grmcd1psw2290bh4ry39l6l2vxficmbf474q56xmvkf8j0ga3qx") (y #t) (l "mseed")))

(define-public crate-libmseed-sys-0.2.1+3.0.17 (c (n "libmseed-sys") (v "0.2.1+3.0.17") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "19diw7wv2vs170p48z5cqxgq0sd5j2vrjpnp87bwd4h0wcsvzg0m") (l "mseed")))

(define-public crate-libmseed-sys-0.3.0+3.1.1 (c (n "libmseed-sys") (v "0.3.0+3.1.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)))) (h "1mi03nby8sbadmsjfywsivxm2z1d2nzbcdrxlg7mhqnnhicjysi2") (l "mseed")))

