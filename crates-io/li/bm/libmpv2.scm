(define-module (crates-io li bm libmpv2) #:use-module (crates-io))

(define-public crate-libmpv2-3.0.1 (c (n "libmpv2") (v "3.0.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "libmpv2-sys") (r "^4.0.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (d #t) (k 2)))) (h "18l2ff8j49x64bh36f64d6y27h8q3sk71p62sqw2y9842lai7w23") (f (quote (("render") ("protocols") ("default" "protocols" "render") ("build_libmpv")))) (y #t)))

(define-public crate-libmpv2-3.0.0 (c (n "libmpv2") (v "3.0.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "libmpv2-sys") (r "^4.0.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (d #t) (k 2)))) (h "1556c5vyh3d3wpkj93wl39qyy5hrxm2cnv05wiknn4ln0w971d18") (f (quote (("render") ("protocols") ("default" "protocols" "render") ("build_libmpv"))))))

(define-public crate-libmpv2-4.0.0 (c (n "libmpv2") (v "4.0.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "libmpv2-sys") (r "^4.0.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.36.0") (d #t) (k 2)))) (h "0zrfaavggm7i6i8myialsppnmg0mbxwsv8bdygayx2m55r0s3vvv") (f (quote (("render") ("protocols") ("default" "protocols" "render") ("build_libmpv"))))))

