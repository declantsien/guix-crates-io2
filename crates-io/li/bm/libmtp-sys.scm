(define-module (crates-io li bm libmtp-sys) #:use-module (crates-io))

(define-public crate-libmtp-sys-1.1.17-1 (c (n "libmtp-sys") (v "1.1.17-1") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1pxikwi4qd2hv2nqm9qql6rfddchfbgrc7d5a5pqs0xvn1spinrc")))

(define-public crate-libmtp-sys-1.1.17-2 (c (n "libmtp-sys") (v "1.1.17-2") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "047zlgg14afcfnsyv1dpd84qjbjly2fgax55747ixzdi5ymvldga")))

(define-public crate-libmtp-sys-1.1.17-3 (c (n "libmtp-sys") (v "1.1.17-3") (d (list (d (n "bindgen") (r "^0.55.1") (d #t) (k 1)) (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0dsqipz3jl364j7b6c79b8w3i0mw0hmmizh7fpv1f23k86ivj43l")))

(define-public crate-libmtp-sys-1.1.17-4 (c (n "libmtp-sys") (v "1.1.17-4") (d (list (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "0lw7s8hnxyzvm6g36lh8gisc680qr64076lazrfr36af3vv9kbkx")))

(define-public crate-libmtp-sys-1.1.17-5 (c (n "libmtp-sys") (v "1.1.17-5") (d (list (d (n "libc") (r "^0.2.77") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1d527zqr4ki2q9z9zfarvwx41jrkbqmdisanq5pbcqy2h9ssvpih")))

