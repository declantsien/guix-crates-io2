(define-module (crates-io li bm libmem-sys) #:use-module (crates-io))

(define-public crate-libmem-sys-5.0.0-pre5 (c (n "libmem-sys") (v "5.0.0-pre5") (d (list (d (n "flate2") (r "^1") (o #t) (d #t) (k 1)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "rustls-tls"))) (o #t) (k 1)) (d (n "tar") (r "^0.4") (o #t) (d #t) (k 1)))) (h "174dj11xfc4fxkrzspy443cbx4k8lvxs33x3373b8g1idmqsh9j1") (f (quote (("static") ("default" "fetch")))) (s 2) (e (quote (("fetch" "static" "dep:reqwest" "dep:flate2" "dep:tar"))))))

