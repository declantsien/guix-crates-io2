(define-module (crates-io li bm libm) #:use-module (crates-io))

(define-public crate-libm-0.1.0 (c (n "libm") (v "0.1.0") (h "0pn1f0vsz2g6s21x7hpk87zm9ndr1p7253jav88qmlrwyn7cf0qr")))

(define-public crate-libm-0.1.1 (c (n "libm") (v "0.1.1") (h "15m32qaym8f8nnx5nvqlhffy6ji9zflg4r6zdl4kknp474gmpgr5")))

(define-public crate-libm-0.1.2 (c (n "libm") (v "0.1.2") (h "0688d8k5w5y6sv0wg9cinnsqi7ccq75w33jps1pwrdg1binvph03")))

(define-public crate-libm-0.1.3 (c (n "libm") (v "0.1.3") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "1qwrgsc5x1qp6m4qhs853dmkik5yj61c4l515xaw1m5am39zpkng") (f (quote (("stable") ("musl-reference-tests" "rand") ("default" "stable") ("checked"))))))

(define-public crate-libm-0.1.4 (c (n "libm") (v "0.1.4") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "16pc0gx4gkg0q2s1ssq8268brn14j8344623vwhadmivc4lsmivz") (f (quote (("stable") ("musl-reference-tests" "rand") ("default" "stable") ("checked"))))))

(define-public crate-libm-0.2.0 (c (n "libm") (v "0.2.0") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "19zsawx58lnydwclc8va33cxw3xp18a2nqb6kvyyd62bil2ga4dx") (f (quote (("unstable") ("musl-reference-tests" "rand") ("default"))))))

(define-public crate-libm-0.2.1 (c (n "libm") (v "0.2.1") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "0akh56sh51adhagmk9l84dyrlz60gv8ri05xhr13i1b18czkpmy7") (f (quote (("unstable") ("musl-reference-tests" "rand") ("default"))))))

(define-public crate-libm-0.2.2 (c (n "libm") (v "0.2.2") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "1nrrd12x9fm0gaw93gdvsgkklmzcm96bj255qq77d2725hv3m8rk") (f (quote (("unstable") ("musl-reference-tests" "rand") ("default"))))))

(define-public crate-libm-0.2.3 (c (n "libm") (v "0.2.3") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "0ic13gksqa6m5safhhlx505vr5y2dy0bqg5aa04ni8sv7xzsb0ys") (f (quote (("unstable") ("musl-reference-tests" "rand") ("default"))))))

(define-public crate-libm-0.2.4 (c (n "libm") (v "0.2.4") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "0nnnrrk9p68ky4f72w22080rly17i74q4l2gimaw18wzi7a3bkn7") (f (quote (("unstable") ("musl-reference-tests" "rand") ("default")))) (y #t)))

(define-public crate-libm-0.2.5 (c (n "libm") (v "0.2.5") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "0rdmkvj4920ga2wz3d0qqq9xg08hgawya7slfp7pdqwiv6698ai9") (f (quote (("unstable") ("musl-reference-tests" "rand") ("default"))))))

(define-public crate-libm-0.2.6 (c (n "libm") (v "0.2.6") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "1ywg7jfcgfv4jypxi3f6rpf7n9509ky695bfzy1fqhms7ymhi09l") (f (quote (("unstable") ("musl-reference-tests" "rand") ("default"))))))

(define-public crate-libm-0.2.7 (c (n "libm") (v "0.2.7") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "1x1z4gayv1v0dzb78bkwb5nm932nk0w1sqa7gh4y2687pcdjn0gp") (f (quote (("unstable") ("musl-reference-tests" "rand") ("default"))))))

(define-public crate-libm-0.2.8 (c (n "libm") (v "0.2.8") (d (list (d (n "no-panic") (r "^0.1.8") (d #t) (k 2)) (d (n "rand") (r "^0.6.5") (o #t) (d #t) (k 1)))) (h "0n4hk1rs8pzw8hdfmwn96c4568s93kfxqgcqswr7sajd2diaihjf") (f (quote (("unstable") ("musl-reference-tests" "rand") ("default"))))))

