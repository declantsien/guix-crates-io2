(define-module (crates-io li bm libmpv-sys) #:use-module (crates-io))

(define-public crate-libmpv-sys-3.0.0 (c (n "libmpv-sys") (v "3.0.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)))) (h "0kcki0k1bjjxicgpckjw97923368qm9d1bndfng5la96d3lq0n1z") (f (quote (("use-bindgen" "bindgen"))))))

(define-public crate-libmpv-sys-3.1.0 (c (n "libmpv-sys") (v "3.1.0") (d (list (d (n "bindgen") (r "^0.54") (o #t) (d #t) (k 1)))) (h "0jq7dpwn4w87snhk1bi63jygjl9sz9xa4897awsg3n2w2k9kiy8d") (f (quote (("use-bindgen" "bindgen"))))))

