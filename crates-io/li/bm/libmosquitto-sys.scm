(define-module (crates-io li bm libmosquitto-sys) #:use-module (crates-io))

(define-public crate-libmosquitto-sys-0.1.0 (c (n "libmosquitto-sys") (v "0.1.0") (d (list (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1hn190s50gznwp4hiifr1a1ifgysvsln4nbvamfwwsjq0r1gjkxf") (l "mosquitto")))

(define-public crate-libmosquitto-sys-0.1.1 (c (n "libmosquitto-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "112jfyk436awl49pmlzdhmig6qw2jznbs324mq2mwwnjm3ipx49s") (l "mosquitto")))

(define-public crate-libmosquitto-sys-0.1.2 (c (n "libmosquitto-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0dp24rz4df9jaqq1m8662ri31qps5klfjf8521nynrgifzx0vghg") (l "mosquitto")))

(define-public crate-libmosquitto-sys-0.2.0 (c (n "libmosquitto-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1g37201kbr69jiw2rr56fbh387lps74saafjgfwvxd1fl1x5va6b") (f (quote (("vendored-openssl" "openssl-sys/vendored" "openssl-sys") ("vendored-mosquitto") ("default" "vendored-mosquitto" "openssl-sys")))) (l "mosquitto")))

(define-public crate-libmosquitto-sys-0.2.1 (c (n "libmosquitto-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "175kj4ib5n800vhpa5v2y71yc92kz4wvsrk2pw00kf8r5h5342lk") (f (quote (("vendored-openssl" "openssl-sys/vendored" "openssl-sys") ("vendored-mosquitto") ("default" "vendored-mosquitto" "openssl-sys")))) (l "mosquitto")))

(define-public crate-libmosquitto-sys-0.2.2 (c (n "libmosquitto-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1n2mwb37hb6fxjhf7m7vcfwi7sj1a2bp8xvidi4jr8f5lkkqghbs") (f (quote (("vendored-openssl" "openssl-sys/vendored" "openssl-sys") ("vendored-mosquitto") ("default" "vendored-mosquitto" "openssl-sys")))) (l "mosquitto")))

(define-public crate-libmosquitto-sys-0.2.3 (c (n "libmosquitto-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "openssl-sys") (r "^0.9") (o #t) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0hgk0jqxzd0dx7rcqqvjr1pwag72p8vcibvw0hyrd79zgdbvkcky") (f (quote (("vendored-openssl" "openssl-sys/vendored" "openssl-sys") ("vendored-mosquitto") ("default" "vendored-mosquitto" "openssl-sys")))) (l "mosquitto")))

