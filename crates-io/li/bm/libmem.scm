(define-module (crates-io li bm libmem) #:use-module (crates-io))

(define-public crate-libmem-0.1.0 (c (n "libmem") (v "0.1.0") (h "1mpiw5dv6fj9ay35g6sxrr7p4zl63dk66iwh7igfl1rm7cb4mnzp")))

(define-public crate-libmem-0.1.1 (c (n "libmem") (v "0.1.1") (h "1pqcccn1k8nfbbfqfz7jvhqhv7lkpdp6jdqmkc1b9zhsd9ahqhs5")))

(define-public crate-libmem-0.1.2 (c (n "libmem") (v "0.1.2") (h "1ln9y1y0h5ddbb7di3z37vxgqjan9gpvmn3iyc3mn5df8sc2749s")))

(define-public crate-libmem-4.0.0 (c (n "libmem") (v "4.0.0") (h "1p137vfsb845xa6y3qg9xayjdvfgr0x5dgmapij61gj8q5h077s8")))

(define-public crate-libmem-4.1.0 (c (n "libmem") (v "4.1.0") (h "0f53kh2z104cc4ykdw1a4hsm310blg04frf412z98y59gjljnzgh")))

(define-public crate-libmem-4.2.0 (c (n "libmem") (v "4.2.0") (h "02qjxabsynamsy5gc7z1lyfdin72lyjw0mh27vhs0nlqcpw12ckz")))

(define-public crate-libmem-4.2.1 (c (n "libmem") (v "4.2.1") (h "0mnijaapgadbvqpr48880jpfank7yb6bkqx8xn1y7ngwb65a287q") (f (quote (("static"))))))

(define-public crate-libmem-4.3.0 (c (n "libmem") (v "4.3.0") (h "1ik7vyvsvjrx59hxi1l952j5i7m6rn0cb5b3fjfm1iqbyqnb466w") (f (quote (("static"))))))

(define-public crate-libmem-4.4.0 (c (n "libmem") (v "4.4.0") (h "0np0sivmhybf86q3n0lw7jfrmzkfkh99bdrwgjadpp5k7xkjliba") (f (quote (("static"))))))

(define-public crate-libmem-5.0.0-pre5 (c (n "libmem") (v "5.0.0-pre5") (d (list (d (n "bitflags") (r "^2.5.0") (d #t) (k 0)) (d (n "libmem-sys") (r "^5.0.0-pre5") (k 0)))) (h "1f8mhn99fdn3brr8jpghbgxv6qh6395kfqyfrs798mr7xf0nc2aw") (f (quote (("static" "libmem-sys/static") ("fetch" "libmem-sys/fetch") ("default" "fetch"))))))

