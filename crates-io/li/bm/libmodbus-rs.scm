(define-module (crates-io li bm libmodbus-rs) #:use-module (crates-io))

(define-public crate-libmodbus-rs-0.4.1 (c (n "libmodbus-rs") (v "0.4.1") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmodbus-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1z7gl15j8qb3qqnl4dn2d3hw494xsc1qjx0flaa2f5bi4dqr0l5v")))

(define-public crate-libmodbus-rs-0.4.2 (c (n "libmodbus-rs") (v "0.4.2") (d (list (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmodbus-sys") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "08qpn7py1q129b5pfsdbrmwb1dk8y9j6iqvrnnnadyhbpdis7lpc")))

(define-public crate-libmodbus-rs-0.4.3 (c (n "libmodbus-rs") (v "0.4.3") (d (list (d (n "clap") (r "~2.24") (f (quote ("color"))) (k 0)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmodbus-sys") (r "^0.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0dvjv3cxc29y8g70qgg6znh42cdfvrn8z1lrk3nc9gl1dixhy9wm")))

(define-public crate-libmodbus-rs-0.5.8 (c (n "libmodbus-rs") (v "0.5.8") (d (list (d (n "clap") (r "^2.24.2") (f (quote ("color"))) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmodbus-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1wxzjjrjbch053lkhk8zbvd2hd214zw0p4yvl6hw2yzrivfqbzn6")))

(define-public crate-libmodbus-rs-0.6.1 (c (n "libmodbus-rs") (v "0.6.1") (d (list (d (n "clap") (r "^2.24.2") (f (quote ("color"))) (k 0)) (d (n "error-chain") (r "^0.11") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmodbus-sys") (r "^0.5") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0jcipya9r1vdg169nx85sb4yy3flq1glq65bvis788lg5pjrgbqn")))

(define-public crate-libmodbus-rs-0.8.2 (c (n "libmodbus-rs") (v "0.8.2") (d (list (d (n "clap") (r "^2.24.2") (f (quote ("color"))) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmodbus-sys") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0zkc727l0akwz0x44y3aibqdxljknssspj8ab5iv24j9w28755p9")))

(define-public crate-libmodbus-rs-0.8.3 (c (n "libmodbus-rs") (v "0.8.3") (d (list (d (n "clap") (r "^2.24.2") (f (quote ("color"))) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libmodbus-sys") (r "^0.8") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1pqgz1izkw6nv0qnj3v4bfj88c67s26kplrx709cf5pj1sbhag69")))

