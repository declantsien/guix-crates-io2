(define-module (crates-io li bm libmw-macro) #:use-module (crates-io))

(define-public crate-libmw-macro-0.1.0 (c (n "libmw-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1bd08dxbq6s5izcagmcg23514y3n9mb6yn4xvhpflc2idgcfgdw3")))

