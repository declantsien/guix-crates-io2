(define-module (crates-io li bm libmancala) #:use-module (crates-io))

(define-public crate-libmancala-1.0.0 (c (n "libmancala") (v "1.0.0") (h "06wks2j5acg28z00izg7dlnzya87da48awz82x5l4m8vzm34i69f")))

(define-public crate-libmancala-1.1.0 (c (n "libmancala") (v "1.1.0") (h "0h60k4csiyc4amiyyr2gsw8a8wgfgfsapdc2q6ci2dzcadwbp4dk")))

