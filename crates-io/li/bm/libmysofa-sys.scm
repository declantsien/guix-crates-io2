(define-module (crates-io li bm libmysofa-sys) #:use-module (crates-io))

(define-public crate-libmysofa-sys-0.1.0 (c (n "libmysofa-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libz-sys") (r "^1.1") (f (quote ("static" "libc"))) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "08cj9w8vrjv0clmh73zrl0zprnzfifsdjb3cs243hzw4lp3admzy") (l "mysofa")))

(define-public crate-libmysofa-sys-0.1.2 (c (n "libmysofa-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libz-sys") (r "^1.1") (f (quote ("static" "libc"))) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0ri16y3dk1r2m8jiyz73p30qgqbmpjwbj55v0spm157720f28wn1") (l "mysofa")))

(define-public crate-libmysofa-sys-0.1.3 (c (n "libmysofa-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libz-sys") (r "^1.1") (f (quote ("static" "libc"))) (k 0)) (d (n "system-deps") (r "^6.1") (d #t) (k 1)))) (h "01w9khkf1r8sbrpx01ib80gw554w70ifk65v7rdsaz4fxb8f7jmz") (l "mysofa")))

(define-public crate-libmysofa-sys-0.1.4 (c (n "libmysofa-sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "libz-sys") (r "^1.1") (f (quote ("static" "libc"))) (k 0)) (d (n "system-deps") (r "^6.1") (d #t) (k 1)))) (h "10vr6qnz79bpsh8260xg4mkafkayljcwb53d7kzz21mc6xc52jvn") (l "mysofa")))

