(define-module (crates-io li bm libmangrove) #:use-module (crates-io))

(define-public crate-libmangrove-0.1.2 (c (n "libmangrove") (v "0.1.2") (d (list (d (n "mcrypt") (r "^0.1.4") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.68") (d #t) (k 1)) (d (n "vergen") (r "^7.5.0") (f (quote ("build" "rustc"))) (k 1)))) (h "0dqh3dkzih2dv8g5xx8hlhswmla0lf987plsczv9hiv5d9rqy7ac")))

