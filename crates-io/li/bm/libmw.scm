(define-module (crates-io li bm libmw) #:use-module (crates-io))

(define-public crate-libmw-0.1.0 (c (n "libmw") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "068nvw3q4hwf8d1j58zj428i488cbganyir77sijpncxbay82l0p")))

(define-public crate-libmw-0.1.2 (c (n "libmw") (v "0.1.2") (d (list (d (n "libmw-macro") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0q4xcpa5lnhxp754qnc4cy4lm336f8h0acl4cl6d344p2y0rsiyv")))

