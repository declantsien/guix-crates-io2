(define-module (crates-io li bm libmount-sys) #:use-module (crates-io))

(define-public crate-libmount-sys-0.1.0 (c (n "libmount-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.49.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "0as734lj5v1bs9xpz5yfpzww147qnw5wwvz8ij5lblkia5xiqnwg")))

