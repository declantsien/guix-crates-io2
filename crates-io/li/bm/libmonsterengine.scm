(define-module (crates-io li bm libmonsterengine) #:use-module (crates-io))

(define-public crate-libmonsterengine-0.1.0 (c (n "libmonsterengine") (v "0.1.0") (d (list (d (n "hyper") (r "^0.13.1") (d #t) (k 0)) (d (n "plamo") (r "^0.3.0") (d #t) (k 0)) (d (n "tokio") (r "^0.2.5") (d #t) (k 0)))) (h "1cp97vsyrj8hln3fkrxmvvyrjcv36d9ynhdlv01n6n8zfpfawqq9")))

