(define-module (crates-io li bm libmodbus) #:use-module (crates-io))

(define-public crate-libmodbus-1.0.0 (c (n "libmodbus") (v "1.0.0") (d (list (d (n "clap") (r "^2.33") (f (quote ("color"))) (k 2)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "libmodbus-sys") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "time") (r "^0.2.22") (d #t) (k 0)))) (h "15qf6rxchvd2gbrfg7jczv4snz6q35v3ynb1qwaydmhy8ip8h3cd")))

(define-public crate-libmodbus-1.0.1 (c (n "libmodbus") (v "1.0.1") (d (list (d (n "clap") (r "^2.33") (f (quote ("color"))) (k 2)) (d (n "libc") (r "^0.2.80") (d #t) (k 0)) (d (n "libmodbus-sys") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "time") (r "^0.2.22") (d #t) (k 0)))) (h "1s8vxjvcc3a4jjwyifimlnywqc2a8yizfihd8lmffn36k74yy4j2")))

