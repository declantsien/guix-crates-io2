(define-module (crates-io li bm libmath) #:use-module (crates-io))

(define-public crate-libmath-0.1.0 (c (n "libmath") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1ahjyq9ccgi1hka1lby0ghfym961vsgb88bgssv0p4235qh49kxk")))

(define-public crate-libmath-0.1.1 (c (n "libmath") (v "0.1.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0qybxkpmc4i93vzjq89kppg7c7x2345xvgfw5jhpdj7zhaizhqw6")))

(define-public crate-libmath-0.1.2 (c (n "libmath") (v "0.1.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1xk2qnd7nxmsj364xj991n872rqnbgd7lmnpqv7qbmz8fc6zk575")))

(define-public crate-libmath-0.1.3 (c (n "libmath") (v "0.1.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "0rpa0dwlys04q2bwbir6bwync54hkx7b48rrxf98cf98axcm51gk")))

(define-public crate-libmath-0.1.4 (c (n "libmath") (v "0.1.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1kr4s53i89m5inj5fnn5n8x4j3mpii11s1rjz934k32dznnxmyc1")))

(define-public crate-libmath-0.2.0 (c (n "libmath") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "1p6pmdd6vb5gvvza9i75lcnny60gc8j0hiqqsl967ix380kqc1ki")))

(define-public crate-libmath-0.2.1 (c (n "libmath") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 0)))) (h "00j546nfjsd4i1z1di800crazjyz6bv0dc63sn0awlx86ill3lyz")))

