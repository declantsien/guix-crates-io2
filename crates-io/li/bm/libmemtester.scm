(define-module (crates-io li bm libmemtester) #:use-module (crates-io))

(define-public crate-libmemtester-1.0.0 (c (n "libmemtester") (v "1.0.0") (d (list (d (n "libc") (r "^0.2.127") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winbase" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "126afsdp0s03agq3m62c04i692dpfms8q5688r4fqazxv4g66ddj")))

(define-public crate-libmemtester-1.1.0 (c (n "libmemtester") (v "1.1.0") (d (list (d (n "libc") (r "^0.2.127") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winbase" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1xh2y49v4kgrrkr0p25s94m8miqkys1amb19gsbflpx7kgnnm0vs")))

(define-public crate-libmemtester-1.1.1 (c (n "libmemtester") (v "1.1.1") (d (list (d (n "libc") (r "^0.2.127") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winbase" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1c2x68lqs4n6x3c7g489ffv78cv12skv2lyd45q4l548q6539hvr")))

(define-public crate-libmemtester-1.2.0 (c (n "libmemtester") (v "1.2.0") (d (list (d (n "libc") (r "^0.2.127") (d #t) (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winbase" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lwlq4l5x3sqy5jfdq4f2wvhlhxcfpz5nk4y6sp71b15cf4965s4")))

(define-public crate-libmemtester-1.2.1 (c (n "libmemtester") (v "1.2.1") (d (list (d (n "libc") (r "^0.2.127") (d #t) (t "cfg(any(target_os = \"linux\"))") (k 0)) (d (n "nix") (r "^0.24.2") (d #t) (t "cfg(any(target_os = \"linux\"))") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("memoryapi" "winbase" "processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1kwbpd9wnmj56yph0k9dq0zyk4qafpyv1w0ihli8ja8wd9c3v46i")))

