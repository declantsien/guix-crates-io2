(define-module (crates-io li bm libmpv) #:use-module (crates-io))

(define-public crate-libmpv-1.0.0 (c (n "libmpv") (v "1.0.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "libmpv-sys") (r "^3") (d #t) (k 0)))) (h "1ramhc4kkx74xq1ipv7cd25i5pab86h8rms8agsi90h7jdg3j265") (f (quote (("render") ("protocols") ("default" "protocols" "render") ("build_libmpv"))))))

(define-public crate-libmpv-1.0.1 (c (n "libmpv") (v "1.0.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "libmpv-sys") (r "^3") (d #t) (k 0)))) (h "1qnchdfi1zn7v5bqnm8c1ggd0c738cr0pcxvjx3gzjjjknvx9vm9") (f (quote (("render") ("protocols") ("default" "protocols" "render") ("build_libmpv"))))))

(define-public crate-libmpv-1.1.0 (c (n "libmpv") (v "1.1.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "libmpv-sys") (r "^3") (d #t) (k 0)))) (h "1azpkzsqqjr8h9rfygbsa6db1h9rljgf9nzgzghqmv0k7bkaqmar") (f (quote (("render") ("protocols") ("default" "protocols" "render") ("build_libmpv"))))))

(define-public crate-libmpv-2.0.0 (c (n "libmpv") (v "2.0.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "libmpv-sys") (r "^3.1.0") (d #t) (k 0)))) (h "15v5x91nkg8vz4295fc85rwzr4lg9jz5657w074263kx2wad0d0n") (f (quote (("render") ("protocols") ("default" "protocols" "render") ("build_libmpv"))))))

(define-public crate-libmpv-2.0.1 (c (n "libmpv") (v "2.0.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "libmpv-sys") (r "^3.1.0") (d #t) (k 0)))) (h "1rr2ksbl6qs15dcfg4mhfglqvvmqnfaa3p0g3vl7aixk34nqx9b1") (f (quote (("render") ("protocols") ("default" "protocols" "render") ("build_libmpv"))))))

