(define-module (crates-io li bm libmedium) #:use-module (crates-io))

(define-public crate-libmedium-0.1.0 (c (n "libmedium") (v "0.1.0") (d (list (d (n "snafu") (r "^0.6.0") (d #t) (k 0)))) (h "0yvhwnc2xk983vj22sr4bgdsw0alkdr109vmhd3g4sg388gxglkb") (f (quote (("writable") ("unrestricted_parsing") ("default" "writable"))))))

(define-public crate-libmedium-0.1.1 (c (n "libmedium") (v "0.1.1") (d (list (d (n "snafu") (r "^0.6.0") (d #t) (k 0)))) (h "01asfmamjr4pw77qx0cd92jsyzynqysclkf49d16q928i990j94r") (f (quote (("writable") ("unrestricted_parsing") ("default" "writable"))))))

(define-public crate-libmedium-0.2.0 (c (n "libmedium") (v "0.2.0") (d (list (d (n "measurements") (r "^0.10") (o #t) (d #t) (k 0)))) (h "019psx3mjmnnif330sysdb8glb562qlr4hv0izk9y8bxdhlgnqa0") (f (quote (("writable") ("unrestricted_parsing") ("measurements_units" "measurements") ("default" "writable"))))))

(define-public crate-libmedium-0.2.1 (c (n "libmedium") (v "0.2.1") (d (list (d (n "measurements") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0b3j7h8y7647qrx9akb3vnra7i0471bk4ask44pji1fvpdxv2vjp") (f (quote (("writable") ("unrestricted_parsing") ("measurements_units" "measurements") ("default" "writable"))))))

(define-public crate-libmedium-0.2.2 (c (n "libmedium") (v "0.2.2") (d (list (d (n "measurements") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0n6akjcr4ndhg13wc6cqx9haf00fayj6adafc6yj9rdm3k1952rj") (f (quote (("writable") ("unrestricted_parsing") ("measurements_units" "measurements") ("default" "writable"))))))

(define-public crate-libmedium-0.2.3 (c (n "libmedium") (v "0.2.3") (d (list (d (n "measurements") (r "^0.10") (o #t) (d #t) (k 0)))) (h "09i9fgz9a046zqn0za5lp8y88w20wyhipi7qw6zfhj2clv863367") (f (quote (("writable") ("unrestricted_parsing") ("measurements_units" "measurements") ("default" "writable"))))))

(define-public crate-libmedium-0.3.0 (c (n "libmedium") (v "0.3.0") (d (list (d (n "measurements") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1f96dgsy6hbajrvzhapghnlhgvvmdn7vjjcyxx1qb0xxn8vnrwwr") (f (quote (("writable") ("unrestricted_parsing") ("measurements_units" "measurements") ("default" "writable"))))))

(define-public crate-libmedium-0.3.1 (c (n "libmedium") (v "0.3.1") (d (list (d (n "measurements") (r "^0.10") (o #t) (d #t) (k 0)))) (h "1bqj2c8kmglnq63w9qh1dp7k1ddpcci2374r89nxkipzklnq7mc7") (f (quote (("writable") ("unrestricted_parsing") ("measurements_units" "measurements") ("default" "writable"))))))

(define-public crate-libmedium-0.3.2 (c (n "libmedium") (v "0.3.2") (d (list (d (n "measurements") (r "^0.10") (o #t) (d #t) (k 0)))) (h "0x1vnrv0sy6zq3dba2bg49371wz1syzav608hmdrpw3z7bhyrpgr") (f (quote (("writable") ("unrestricted_parsing") ("measurements_units" "measurements") ("default" "writable"))))))

(define-public crate-libmedium-0.3.3 (c (n "libmedium") (v "0.3.3") (d (list (d (n "measurements") (r "^0.10") (o #t) (d #t) (k 0)))) (h "11pik0c7dim4gd5ss0w01gqarh7136s8h05y1kyiwka88ns87rs3") (f (quote (("writable") ("unrestricted_parsing") ("measurements_units" "measurements") ("default" "writable"))))))

(define-public crate-libmedium-0.3.4 (c (n "libmedium") (v "0.3.4") (d (list (d (n "measurements") (r "^0.10") (o #t) (d #t) (k 0)))) (h "01f1m6jbj3wc7v88zi3xc6jim44f79k33r81gcxb2cgsi8y6xdcr") (f (quote (("writable") ("unrestricted_parsing") ("measurements_units" "measurements") ("default" "writable"))))))

(define-public crate-libmedium-0.4.0 (c (n "libmedium") (v "0.4.0") (d (list (d (n "uom") (r ">=0.30.0, <0.31.0") (o #t) (d #t) (k 0)))) (h "04lavjw0iz1as707djd1z5mwxhq0hglc81vmlppd671xcakvls7c") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.4.1 (c (n "libmedium") (v "0.4.1") (d (list (d (n "uom") (r ">=0.30.0, <0.31.0") (o #t) (d #t) (k 0)))) (h "1n1583q0kjf3py1db47p0x8rb32jrlayshrsy2995pfg0kxsvgkb") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.5.0 (c (n "libmedium") (v "0.5.0") (d (list (d (n "uom") (r ">=0.30.0, <0.31.0") (o #t) (d #t) (k 0)))) (h "11nqjih198pnmqy8ainyx1kj6zpbny82w9n3xxkaab2hi0bqa0zi") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.5.1 (c (n "libmedium") (v "0.5.1") (d (list (d (n "uom") (r "^0.30") (o #t) (d #t) (k 0)))) (h "19vwkv24c5kjq4iccsrv4vz96q87b78fgzvq46sl1wmwplwdwmdz") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.5.2 (c (n "libmedium") (v "0.5.2") (d (list (d (n "uom") (r "^0.30") (o #t) (d #t) (k 0)))) (h "10hp01zr6vah90iaxzxqkyfvmjm7gy1m8a1n3najl6ks80mabzr3") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.5.3 (c (n "libmedium") (v "0.5.3") (d (list (d (n "uom") (r "^0.30") (o #t) (d #t) (k 0)))) (h "0sjvjzp2znnfvayllkw09l1zm0259h5009lwfdf5043iyn7lr5rz") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.5.4 (c (n "libmedium") (v "0.5.4") (d (list (d (n "temp-dir") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.30") (o #t) (d #t) (k 0)))) (h "1z35fkgg0hw78vpkn1ycb6x1fwph48h88vw886gjbkhdvlwqdy7f") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.5.5 (c (n "libmedium") (v "0.5.5") (d (list (d (n "temp-dir") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.31") (o #t) (d #t) (k 0)))) (h "1h91354vzbcdvr07b1xmhw36a755xlhrah80lk34grgb07rsixi7") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.6.0 (c (n "libmedium") (v "0.6.0") (d (list (d (n "temp-dir") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.31") (o #t) (d #t) (k 0)))) (h "1kid1bq9n4p4fxsrbw1cgshkgxfnyzgg458y2wd5y55dl52da7kh") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.6.1 (c (n "libmedium") (v "0.6.1") (d (list (d (n "temp-dir") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.32") (o #t) (d #t) (k 0)))) (h "1b6h9fiizw2v44fh73y7k174ax8ypm13gqryvn62bmfy4hrsv8za") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.6.2 (c (n "libmedium") (v "0.6.2") (d (list (d (n "temp-dir") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.32") (o #t) (d #t) (k 0)))) (h "1zlw0bh197a6sjj82m6hg4b9l5nyxb488g78062c647z8p1cfihf") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.6.3 (c (n "libmedium") (v "0.6.3") (d (list (d (n "temp-dir") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.32") (o #t) (d #t) (k 0)))) (h "04nfhfv3hahca70alvisb48rg99hymja1d399sg7wz1znbgkcy5a") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.7.0 (c (n "libmedium") (v "0.7.0") (d (list (d (n "temp-dir") (r "^0.1") (d #t) (k 2)) (d (n "uom") (r "^0.33") (o #t) (d #t) (k 0)))) (h "1xy3nhnjajpq3m9f8fjc01hfp1v3kavm2y0h2g24kgxrg6im77m7") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("default" "writeable"))))))

(define-public crate-libmedium-0.8.0 (c (n "libmedium") (v "0.8.0") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "temp-dir") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "macros" "rt"))) (d #t) (k 2)) (d (n "uom") (r "^0.36") (o #t) (d #t) (k 0)))) (h "0ykvrmr5pdn01f52s60s3011ry7gk625fv846n4pb61sn7jbrnjg") (f (quote (("writeable") ("uom_units" "uom") ("unrestricted_parsing") ("sync") ("default" "sync" "writeable") ("async" "tokio" "async-trait"))))))

(define-public crate-libmedium-0.8.1 (c (n "libmedium") (v "0.8.1") (d (list (d (n "async-trait") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "temp-dir") (r "^0.1") (d #t) (k 2)) (d (n "tokio") (r "^1") (f (quote ("fs"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("fs" "macros" "rt"))) (d #t) (k 2)) (d (n "uom") (r "^0.36") (o #t) (d #t) (k 0)))) (h "03nbz735jpr6jn28pnpivx1lps165jjhk04pwwyd06zc9gp7h4wi") (f (quote (("writeable") ("virtual_sensors") ("uom_units" "uom") ("unrestricted_parsing") ("sync") ("default" "sync" "virtual_sensors" "writeable") ("async" "tokio" "async-trait"))))))

