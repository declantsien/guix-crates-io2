(define-module (crates-io li bm libmpv-sys-sirno) #:use-module (crates-io))

(define-public crate-libmpv-sys-sirno-4.0.0 (c (n "libmpv-sys-sirno") (v "4.0.0") (d (list (d (n "bindgen") (r "^0.62") (o #t) (d #t) (k 1)))) (h "0jg6v3w0d2y2l28rxcq5d5bnm80665ndyc717ijq7x7b23anhbq8") (f (quote (("use-bindgen" "bindgen")))) (y #t)))

(define-public crate-libmpv-sys-sirno-2.0.0-fork.1 (c (n "libmpv-sys-sirno") (v "2.0.0-fork.1") (d (list (d (n "bindgen") (r "^0.62") (o #t) (d #t) (k 1)))) (h "0l1s3pzn4ndhlz9bzmcqw1mwi06d8q1971nfhyfddw9ykwxgxbc8") (f (quote (("use-bindgen" "bindgen"))))))

