(define-module (crates-io li bm libmount) #:use-module (crates-io))

(define-public crate-libmount-0.1.0 (c (n "libmount") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "1hrphaym3m131ld4bl7s29jzlg5a4628s5dgdgv5xcdc404n5bv6")))

(define-public crate-libmount-0.1.1 (c (n "libmount") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "1a8157kgqrr903d0g0ncznv8g9q7bhx3s2xv5krgxf851cfbd0v4")))

(define-public crate-libmount-0.1.2 (c (n "libmount") (v "0.1.2") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "0kya0fs4bin5ihhhyd0bh92f1xqv5gmb77hq0ll4xdgkrm29qqhg")))

(define-public crate-libmount-0.1.3 (c (n "libmount") (v "0.1.3") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.9") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nix") (r "^0.5.0") (d #t) (k 0)))) (h "05qr9iw2gbixy83jqg2zdvxnn6fngpa7igz98nkfz8ginhq76xib")))

(define-public crate-libmount-0.1.4 (c (n "libmount") (v "0.1.4") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "1dn2vvfasx8cgsw50dcv1whdcwgas2bvw3kgicjxfz1041xd2050")))

(define-public crate-libmount-0.1.5 (c (n "libmount") (v "0.1.5") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 2)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "1nq8yjlkfx77qryg2k1bg12jy8sayvj7r7d3cmq0xcdg93jls49q")))

(define-public crate-libmount-0.1.6 (c (n "libmount") (v "0.1.6") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.16") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 2)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)))) (h "1h3xr89f5kg8g4gqmc8aif71ngvc3s461pc30n9yma396sifz74j")))

(define-public crate-libmount-0.1.7 (c (n "libmount") (v "0.1.7") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 2)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "0dlz93ppmxadp12x0h3c2azjp2pyh9vrmb13dd2y1rd9p7hi94cn")))

(define-public crate-libmount-0.1.8 (c (n "libmount") (v "0.1.8") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 2)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "1y7ddph4vj09k8dbhp7a90di0wwmishhy4sjlw9mlwm116bq6jy8")))

(define-public crate-libmount-0.1.9 (c (n "libmount") (v "0.1.9") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.3") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 2)) (d (n "nix") (r "^0.9.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "0q5aphg441ql31a6i8srf4w6xd646cpvkc8mzzzqdmkkynx2wgp9")))

(define-public crate-libmount-0.1.10 (c (n "libmount") (v "0.1.10") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 2)) (d (n "nix") (r "^0.10.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "0rf8xb0x2fya0mmn80ga2qmy91a7qxfbwdjrmyklvc1lvlhd3mv3")))

(define-public crate-libmount-0.1.11 (c (n "libmount") (v "0.1.11") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 2)) (d (n "nix") (r "^0.11") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "16jmagd51bx6rp5jd3nmgawsl0679m0l3nlad3rynmrcyf45zm6r")))

(define-public crate-libmount-0.1.12 (c (n "libmount") (v "0.1.12") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 2)) (d (n "nix") (r "^0.13") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "1v1chg5wp5h85k2khm2hlhlicpwn2c3fdq2ynnqk7i07p3z6d4mx")))

(define-public crate-libmount-0.1.13 (c (n "libmount") (v "0.1.13") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 2)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "1hp0f449nwy0vgh7k9r55jz6b6bxpmbsh8rcln3a10wrqzfl6mgv")))

(define-public crate-libmount-0.1.14 (c (n "libmount") (v "0.1.14") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 2)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "17f2n8kjnqfv5nz2idgygr2igrrak7j4pgz8fydd4pgdipsjxk95") (y #t)))

(define-public crate-libmount-0.1.15 (c (n "libmount") (v "0.1.15") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.5.10") (d #t) (k 2)) (d (n "libc") (r "^0.2.28") (d #t) (k 0)) (d (n "log") (r "^0.4.1") (d #t) (k 2)) (d (n "nix") (r "^0.14") (d #t) (k 0)) (d (n "quick-error") (r "^1.2.0") (d #t) (k 0)))) (h "0py3kl473jgfwnfajzr0xi9xs2lk8npks3320md2zgaw5nnw5i13")))

