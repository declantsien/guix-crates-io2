(define-module (crates-io li bm libmarpa-sys) #:use-module (crates-io))

(define-public crate-libmarpa-sys-0.1.0 (c (n "libmarpa-sys") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1agfz65cz7qa4j9i4hjvhx5hwybin3k469f1z1iiylj1jkx9rmpy")))

(define-public crate-libmarpa-sys-0.1.1 (c (n "libmarpa-sys") (v "0.1.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ai8x1z2da1k7vjha6cqq135mhalsbq4bhnx7ydzh6vq5bb6fhb7")))

(define-public crate-libmarpa-sys-0.1.2 (c (n "libmarpa-sys") (v "0.1.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0vr13xj4ivsyfi5a07zhpjkagh0fgjsf4386vnsmhjhb9y2973yk")))

(define-public crate-libmarpa-sys-0.1.3 (c (n "libmarpa-sys") (v "0.1.3") (d (list (d (n "autotools") (r "^0.1.3") (d #t) (k 1)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)))) (h "1lvvhizy8cn6xxvm245ryp63aks2kynyv1za3bng5sg30lxysi0c")))

(define-public crate-libmarpa-sys-0.2.0 (c (n "libmarpa-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.47.1") (d #t) (k 1)) (d (n "flate2") (r "^1.0.6") (d #t) (k 1)) (d (n "lazy_static") (r "^1.2.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.47") (d #t) (k 0)) (d (n "tar") (r "^0.4.20") (d #t) (k 1)))) (h "1lqnwyif6v7gldqlw4hfxrprfijqf95x5x5bnbr0jk72dppjwf2j")))

