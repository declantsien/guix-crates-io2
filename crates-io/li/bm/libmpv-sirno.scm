(define-module (crates-io li bm libmpv-sirno) #:use-module (crates-io))

(define-public crate-libmpv-sirno-2.0.2-fork.1 (c (n "libmpv-sirno") (v "2.0.2-fork.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 2)) (d (n "libmpv-sys-sirno") (r "^2.0.0-fork.1") (d #t) (k 0)))) (h "0ag2f5d0ih59jqypgpfzpkk0syr6im6xn4lsjklh9n02972zsiy3") (f (quote (("render") ("protocols") ("default" "protocols" "render") ("build_libmpv"))))))

