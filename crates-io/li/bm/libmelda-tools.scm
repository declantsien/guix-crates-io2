(define-module (crates-io li bm libmelda-tools) #:use-module (crates-io))

(define-public crate-libmelda-tools-0.1.1 (c (n "libmelda-tools") (v "0.1.1") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "melda") (r "^0.1.7") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "1n33ygbxxnjd0cz6shq9qfgqdrxnpmgj0rhv90zh39nhf52n7kbp")))

(define-public crate-libmelda-tools-0.1.2 (c (n "libmelda-tools") (v "0.1.2") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "melda") (r "^0.1.13") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (d #t) (k 0)))) (h "18dkvfsjpp3ycdw1kbaqcfab23bqw7px1kp6rhw35i58iljh2ig6")))

(define-public crate-libmelda-tools-0.1.4 (c (n "libmelda-tools") (v "0.1.4") (d (list (d (n "clap") (r "^3.1.6") (f (quote ("derive"))) (d #t) (k 0)) (d (n "melda") (r "^0.1.15") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0wkkqjlr6nrjj2azbsf8mz82c5yzrzpv65ww3j2yg8bz4dg7cs6p")))

