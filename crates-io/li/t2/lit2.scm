(define-module (crates-io li t2 lit2) #:use-module (crates-io))

(define-public crate-lit2-1.0.6 (c (n "lit2") (v "1.0.6") (h "02j2ba6sa8k7pm0sf176j2vg0n9q098vsack8m4pmci7m0lxk74n")))

(define-public crate-lit2-1.0.7 (c (n "lit2") (v "1.0.7") (h "1siy19zvzdzn91cwypx9d0h7q8lpd8ypq9l1sp46xx8d4givr27p")))

(define-public crate-lit2-1.0.8 (c (n "lit2") (v "1.0.8") (h "07f5zjlvm1zav5q8mqq2hf67rgiw49yzq4aqigizvkb6z56rzjys")))

(define-public crate-lit2-1.0.9 (c (n "lit2") (v "1.0.9") (h "1pl9vx6daabvmf695y2pb6akfyqcb24j5crvk6n4x84vfn4w5l6w")))

