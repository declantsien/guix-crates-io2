(define-module (crates-io li bx libxivdat) #:use-module (crates-io))

(define-public crate-libxivdat-0.1.0 (c (n "libxivdat") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "1xminfan07n3p273lks7ldkrpc99sfqh2vv7a487qw941p3dwb84")))

(define-public crate-libxivdat-0.2.0 (c (n "libxivdat") (v "0.2.0") (d (list (d (n "tempfile") (r "^3.2.0") (d #t) (k 2)))) (h "0hsk3kn3gzj2sy0xcj4rh4hp1n1iyiir4rs2wifiif50q6941sil") (f (quote (("macro" "high-level") ("high-level"))))))

