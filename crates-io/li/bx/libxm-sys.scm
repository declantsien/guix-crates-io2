(define-module (crates-io li bx libxm-sys) #:use-module (crates-io))

(define-public crate-libxm-sys-0.0.1 (c (n "libxm-sys") (v "0.0.1") (d (list (d (n "gcc") (r "0.*") (d #t) (k 1)))) (h "0vnabyqfv88d8gjr3xarhl8l963gdcwlkzmw9zvrq2ca7mw4r7lh") (y #t)))

(define-public crate-libxm-sys-0.0.2 (c (n "libxm-sys") (v "0.0.2") (d (list (d (n "gcc") (r "^0.2") (d #t) (k 1)))) (h "0g0gzf0m1n60vg81nhs38h8a672jvzwi689j090hkqprk2yngmlp") (y #t)))

