(define-module (crates-io li bx libxlsxwriter-sys-cs) #:use-module (crates-io))

(define-public crate-libxlsxwriter-sys-cs-1.1.4-2 (c (n "libxlsxwriter-sys-cs") (v "1.1.4-2") (d (list (d (n "bindgen") (r ">=0.58, <0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0kpskgf7kabc7ylzd2jaa825q75qgb2v1zjpfz92rcxy6si3hjqb") (f (quote (("use-openssl-md5") ("system-zlib") ("no-md5"))))))

