(define-module (crates-io li bx libxid) #:use-module (crates-io))

(define-public crate-libxid-0.1.0 (c (n "libxid") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0i3q48g0g9hh0jdqmdji4ialgv96i3ylgnqlal6n9q6vanzd2jcy")))

(define-public crate-libxid-0.1.1 (c (n "libxid") (v "0.1.1") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "197m6zx7m7p10czihx9nrvw71rx41bgjrm1qyyg78ryrysaqz24x")))

(define-public crate-libxid-0.1.2 (c (n "libxid") (v "0.1.2") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "data-encoding") (r "^2.1.2") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)))) (h "0czdrx6wd0p56iaqys9svqn7b111ynvacg0n1dxgh78rljqhbwa9")))

(define-public crate-libxid-0.1.3 (c (n "libxid") (v "0.1.3") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14ilyl6l8a27ij2isbpy01fiqiwkphdbb7hs0v0gs029zpyvqa2x")))

(define-public crate-libxid-0.1.4 (c (n "libxid") (v "0.1.4") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v9nklx642b7jazd84k4wq9cx614vfcsr5ylq1w8vcwq0zqq5x37")))

(define-public crate-libxid-0.1.5 (c (n "libxid") (v "0.1.5") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "crc32fast") (r "^1.1.2") (d #t) (k 0)) (d (n "gethostname") (r "^0.2.0") (d #t) (k 0)) (d (n "md5") (r "^0.6.1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0z3wscasq497ifdlh34gg3qmjk1ggjlfkhxc20s9b7cgqjsa40nl")))

