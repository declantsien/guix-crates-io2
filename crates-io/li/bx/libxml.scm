(define-module (crates-io li bx libxml) #:use-module (crates-io))

(define-public crate-libxml-0.0.2 (c (n "libxml") (v "0.0.2") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "07s71lmj3mab8zp89ljnrp42sw8y5ciprgrmyf5faxzddf29f13c")))

(define-public crate-libxml-0.0.3 (c (n "libxml") (v "0.0.3") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "0h6apwk7jl6rnpfl9ivsdzhw3355wxfh06yx255w5w0wh2968jap")))

(define-public crate-libxml-0.0.4 (c (n "libxml") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "0jhx2zqlailr7zz04r4pwm4v82b6zmy166457rx6rzz6n6sa4pmq")))

(define-public crate-libxml-0.0.5 (c (n "libxml") (v "0.0.5") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "198k91b4h7f5wg5fdkf1l2jr3bb55xkwr59p79chl6hm89ksb9xb")))

(define-public crate-libxml-0.0.6 (c (n "libxml") (v "0.0.6") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1mh3zjgn9jjgzkykwz4y1xwmzkc69b3vmbihc9sm92yy08d8pi6d")))

(define-public crate-libxml-0.0.61 (c (n "libxml") (v "0.0.61") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "15i7r4fp4jkmqsdq34nlkff40nmyrj4mvqp4ah9lhqb7nbqj5lnj")))

(define-public crate-libxml-0.0.62 (c (n "libxml") (v "0.0.62") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1g7h1x1478pj1yxlrvq665vavph49wx7z40lxd2zcgirqffs21h8")))

(define-public crate-libxml-0.0.63 (c (n "libxml") (v "0.0.63") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "0mdffz4wv6kbrcq0hsda8c8v5ack1i44gmr5r2yf5lgz1j1xjmr8")))

(define-public crate-libxml-0.0.7 (c (n "libxml") (v "0.0.7") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1ygi2radg034mxgbynigifq631pr6prjiwlxww7fdfp1gdgbm14g")))

(define-public crate-libxml-0.0.71 (c (n "libxml") (v "0.0.71") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1byp9ribdjga2cn9f1kgsj5yq2nal2azrhnafx061xahgr8y6djx")))

(define-public crate-libxml-0.0.72 (c (n "libxml") (v "0.0.72") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "06psyd62aig50940817b4wl7vhqlh5z3zlvq845r0hnphazc1c2c")))

(define-public crate-libxml-0.0.73 (c (n "libxml") (v "0.0.73") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "04a8vq51fvm9ic8ymjmdvrbzni20q9x203x7wcdxfx4agi3n97qv")))

(define-public crate-libxml-0.0.74 (c (n "libxml") (v "0.0.74") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "18bdk23n6rjkxkabr623qg3dff904czby4cwyqqr810al2ma1rm6")))

(define-public crate-libxml-0.0.741 (c (n "libxml") (v "0.0.741") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1q3h0kp8jb19hfrgvl0ad051ia409lbf6zfrz8a1pd5xh4sza0hn")))

(define-public crate-libxml-0.0.742 (c (n "libxml") (v "0.0.742") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "0lwnjjcqywnc96pj137iq56gj676j75lazgcalgr2ilcywvkzsxv")))

(define-public crate-libxml-0.0.743 (c (n "libxml") (v "0.0.743") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "04xfd41nad08mc0vrsbbj4h9dmrc7py1lqig83gnhqi8vg4gkaa4")))

(define-public crate-libxml-0.0.744 (c (n "libxml") (v "0.0.744") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "0sa5zsnpmcnajr1w54axgslhnphkid6f3kic2fnzdirfslycs0qf")))

(define-public crate-libxml-0.0.745 (c (n "libxml") (v "0.0.745") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1n86yyrcpxnhx0dhz336s9g7svsbnkw40c6zcz2qpagqc2b46c20")))

(define-public crate-libxml-0.0.746 (c (n "libxml") (v "0.0.746") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "0v1j5vklx01irn3wka9vma0cll58nw7wfcx0dpz6zv3dpn5vq8kf")))

(define-public crate-libxml-0.0.747 (c (n "libxml") (v "0.0.747") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "07afwkchl0af9cfwx7r1lvnmjaqs8n6i7x63srsl725vzrllhb5n")))

(define-public crate-libxml-0.0.748 (c (n "libxml") (v "0.0.748") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "08qqp6xbdzjyyzycvwyfhpd239jfrvqrp5i4p85ay1wm2bzrsbv4")))

(define-public crate-libxml-0.0.7481 (c (n "libxml") (v "0.0.7481") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "03zrlq8w9iz0bba1bn8vf759zm1lkvb6vqxvsb8s196b3sk7pd4x") (y #t)))

(define-public crate-libxml-0.0.749 (c (n "libxml") (v "0.0.749") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "0a5raq077zcdv5z1m9w8h0pa7hgbvxa90rlka7yh78a9iiapia5m")))

(define-public crate-libxml-0.0.75 (c (n "libxml") (v "0.0.75") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "0mwb0ngxxh6l4vl0f6m9g40sfwfxp0qfdf62vp9kcrzlsxbf7lcx")))

(define-public crate-libxml-0.0.751 (c (n "libxml") (v "0.0.751") (d (list (d (n "gcc") (r "^0.3.21") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "13cq9vlyjn263q9lqgj0ci27g3qgwk2ima6av2fb4z4rkgk0rs86")))

(define-public crate-libxml-0.1.0 (c (n "libxml") (v "0.1.0") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "0fvxlclx6k0zbc826m5l2h3gqmd4biqcwjdwllfl4hagj30n8rfg")))

(define-public crate-libxml-0.1.1 (c (n "libxml") (v "0.1.1") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "~0.1") (d #t) (k 0)))) (h "1b5f51x75wxbxz7qsixvi3nc9kgfvxgmv859wx0pnfv217kisq1q")))

(define-public crate-libxml-0.1.2 (c (n "libxml") (v "0.1.2") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19aq2vwi5baqiggsz5idpz3wgqxkv4by8rkvi25ms4ysn7xlyy8c")))

(define-public crate-libxml-0.2.0 (c (n "libxml") (v "0.2.0") (d (list (d (n "gcc") (r "^0.3.54") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0wa5dihwxyjafpnwm32m6fdvzv16wj7xvhw5273pwxp4g0mkk8wl")))

(define-public crate-libxml-0.2.1 (c (n "libxml") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1504smqq7b0s5lamh9bzv5ksfnnmkpv4cgwlxxahwqvd3plhrhir")))

(define-public crate-libxml-0.2.2 (c (n "libxml") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0b0wjvz9qkbi0sig5l7q4b3v1vg68vv4gq0n4qmzy39yhil2svwq")))

(define-public crate-libxml-0.2.3 (c (n "libxml") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0prhq74nlahbyqpkvhyngix57qdip3d2n8k96ds4nrg9hqsm60b0")))

(define-public crate-libxml-0.2.4 (c (n "libxml") (v "0.2.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "16d0hf93p33qxr6kcqpg1slc5zfp6wybyhx52n8wvs30xsikwp58")))

(define-public crate-libxml-0.2.5 (c (n "libxml") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1pk0rq79lmsmhbz6is6x8xdbp32km5c5ffhzzi4i5zvgfb2xa5ia")))

(define-public crate-libxml-0.2.6 (c (n "libxml") (v "0.2.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "02f6wxjy2j76jy9l4b27rgmvbjhn0xqdl10vl32clx8fwd89644k")))

(define-public crate-libxml-0.2.7 (c (n "libxml") (v "0.2.7") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1x4v2gz0c13sldcvcq96zkp5ddxcwc1w0bl66bn7p1jxxpqkpswi")))

(define-public crate-libxml-0.2.8 (c (n "libxml") (v "0.2.8") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "02bd9fz009v81xahl91ny5in2n90iv1kw8k411n5caczwl00bh8b")))

(define-public crate-libxml-0.2.9 (c (n "libxml") (v "0.2.9") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "1g3xna751pzn86l3bm3bx1g4b17rk4cksssw4q343pcyhf2vvxzi")))

(define-public crate-libxml-0.2.10 (c (n "libxml") (v "0.2.10") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)))) (h "05rvxjcb45ka8r4dv6marrnkvvin4g8xvbgkv3zndirbm3n6l0dd")))

(define-public crate-libxml-0.2.11 (c (n "libxml") (v "0.2.11") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)))) (h "1i1jp3x7splvs358nkisb5p67bfwgzx1jr4iibvddyw1a8crmhkl")))

(define-public crate-libxml-0.2.12 (c (n "libxml") (v "0.2.12") (d (list (d (n "criterion") (r "^0.2.10") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)))) (h "1q7zr3a08bxmq8zamp1aq6v2p4s88jycyc23a7plgd3mcjnsa9gd")))

(define-public crate-libxml-0.2.13 (c (n "libxml") (v "0.2.13") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)))) (h "1d7ipn6g0xb8012ik9pcc4nswjnfyp9pmq419l6pivga4syn3gpv")))

(define-public crate-libxml-0.2.14 (c (n "libxml") (v "0.2.14") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)))) (h "0rlm5jxk5psixva60z3w9iakwlmia3aasiq6pfgnis4q3aiprajn")))

(define-public crate-libxml-0.2.15 (c (n "libxml") (v "0.2.15") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(macos)") (k 1)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)))) (h "06s65w08ycwlrwcbrvy01kkfc9vc12izil33ygb4n5q50hl2km8k")))

(define-public crate-libxml-0.2.16 (c (n "libxml") (v "0.2.16") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(macos)") (k 1)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)))) (h "1xx1bsay6c0jy9vi7qcvliidya33lvbkxwpdfz9qmzl23f11xxxh")))

(define-public crate-libxml-0.2.17 (c (n "libxml") (v "0.2.17") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(macos)") (k 1)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)))) (h "1z567rqb55hxn0g7hpvcsh3nfvz9mdzlwk4mk179z9bkf7v10r29") (y #t)))

(define-public crate-libxml-0.3.0 (c (n "libxml") (v "0.3.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(macos)") (k 1)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)))) (h "1f42qrc1mim85ivh2d4bn2wbqrn7dfg1i3l1q32yajcr835pxh02")))

(define-public crate-libxml-0.3.1 (c (n "libxml") (v "0.3.1") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(macos)") (k 1)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)))) (h "1b4pvmbg5bpphbdq2zszhz5csanfl8zgxh35538callhjdw5lzv8")))

(define-public crate-libxml-0.3.2 (c (n "libxml") (v "0.3.2") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(macos)") (k 1)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)))) (h "03pfl4nq54764psrh2n5ddpvc4n9mdgrfd07mn3776syn16hwcfa")))

(define-public crate-libxml-0.3.3 (c (n "libxml") (v "0.3.3") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(macos)") (k 1)) (d (n "pkg-config") (r "^0.3.2") (d #t) (t "cfg(unix)") (k 1)) (d (n "rayon") (r "^1.0.0") (d #t) (k 2)) (d (n "vcpkg") (r "^0.2") (d #t) (t "cfg(windows)") (k 1)))) (h "1v3v3qnk9anik100q450gj8bhjylzizn0gzym4jnvcxwqbg3rrsz")))

