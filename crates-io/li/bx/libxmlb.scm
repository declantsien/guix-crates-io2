(define-module (crates-io li bx libxmlb) #:use-module (crates-io))

(define-public crate-libxmlb-0.3.0 (c (n "libxmlb") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.3.0") (d #t) (k 0) (p "libxmlb-sys")) (d (n "gio") (r "^0.17") (d #t) (k 0)) (d (n "glib") (r "^0.17") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "11sxid23p1b8zg61aa713jgcpx3pckj8zip7694ryn0l6sans2y8") (f (quote (("dox" "ffi/dox"))))))

