(define-module (crates-io li bx libx11) #:use-module (crates-io))

(define-public crate-libx11-0.1.0 (c (n "libx11") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0hpsy5qd1c3i299s20svpvxa7iw9gmjyxm23mrry3p6gp9pgkwjg") (y #t)))

(define-public crate-libx11-0.2.1 (c (n "libx11") (v "0.2.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1zpzf5xgr9k7mvk07jy685gq2iscvzi73mqjlvhp725xyg86nlfl") (f (quote (("xinerama") ("keysym_extra")))) (y #t)))

