(define-module (crates-io li bx libxlsxwriter-sys) #:use-module (crates-io))

(define-public crate-libxlsxwriter-sys-0.8.7 (c (n "libxlsxwriter-sys") (v "0.8.7") (d (list (d (n "bindgen") (r "^0.51.1") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "02hrakxssk4y072vg5msmzb35zdqga3s9vga1imb02fhvw177jfz")))

(define-public crate-libxlsxwriter-sys-0.9.4 (c (n "libxlsxwriter-sys") (v "0.9.4") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "04rywjs4l5cc5rjy8ryb2a7rh9zgb739aj7zfc43bysa9j4wk4gj")))

(define-public crate-libxlsxwriter-sys-1.0.0 (c (n "libxlsxwriter-sys") (v "1.0.0") (d (list (d (n "bindgen") (r "^0.54") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0pph7phzvd2yd3djpskjxihjxcx305cm6yshllk4x1ij764f9l73")))

(define-public crate-libxlsxwriter-sys-1.0.2 (c (n "libxlsxwriter-sys") (v "1.0.2") (d (list (d (n "bindgen") (r ">=0.54, <0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "045rd3cmgmpd9rz9q8r5ci1qavmpijhb2qgljqr6ycqzyswdvd7i")))

(define-public crate-libxlsxwriter-sys-1.0.6 (c (n "libxlsxwriter-sys") (v "1.0.6") (d (list (d (n "bindgen") (r ">=0.54, <0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0g84zvln8cbqia46j938ydcykx97xxfpchl614lni1jnmdmsfcbn")))

(define-public crate-libxlsxwriter-sys-1.1.1 (c (n "libxlsxwriter-sys") (v "1.1.1") (d (list (d (n "bindgen") (r ">=0.58, <0.59") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0z73a47mcy6a68bn1xp9lhs9xlgxamvnzhr7zyvcvhk0xvlrzy29") (f (quote (("use-openssl-md5") ("system-zlib") ("no-md5"))))))

(define-public crate-libxlsxwriter-sys-1.1.4 (c (n "libxlsxwriter-sys") (v "1.1.4") (d (list (d (n "bindgen") (r ">=0.58, <0.60") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1fpghabxgi9drv0dgnmd6422c2lbsmbhvzpfsb3s3f4rfqbgbalf") (f (quote (("use-openssl-md5") ("system-zlib") ("no-md5"))))))

(define-public crate-libxlsxwriter-sys-1.1.5 (c (n "libxlsxwriter-sys") (v "1.1.5") (d (list (d (n "bindgen") (r "^0.65") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "196qcj3dh74q0zfi59ii41mvx9d35by76h5y7a3zhl520dx4i6jc") (f (quote (("use-openssl-md5") ("system-zlib") ("no-md5"))))))

