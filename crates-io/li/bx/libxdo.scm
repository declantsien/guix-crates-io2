(define-module (crates-io li bx libxdo) #:use-module (crates-io))

(define-public crate-libxdo-0.0.1 (c (n "libxdo") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libxdo-sys") (r "*") (d #t) (k 0)))) (h "1kx2vgfq53wvmvgss5r958dgnxv18hfdm0m0s0klam6jgiphyxq4")))

(define-public crate-libxdo-0.0.2 (c (n "libxdo") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libxdo-sys") (r "*") (d #t) (k 0)))) (h "0gvxnzc207s89wr2jz92gmz9anfqivbqzihki2z7fmc2y10v3z8b")))

(define-public crate-libxdo-0.0.3 (c (n "libxdo") (v "0.0.3") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libxdo-sys") (r "*") (d #t) (k 0)))) (h "1dc98j13y4fpr2rnjsdf5s2xairvpk08hkkpiidjy36isf937wvr")))

(define-public crate-libxdo-0.1.0 (c (n "libxdo") (v "0.1.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libxdo-sys") (r "*") (d #t) (k 0)))) (h "1cx66q72hi6kfvl95mqwqvzxy2d54rsnm9pnzi0wkpi8yck3nfnf")))

(define-public crate-libxdo-0.2.0 (c (n "libxdo") (v "0.2.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libxdo-sys") (r "^0.1.0") (d #t) (k 0)))) (h "13ifb972nyfvgwz58zawvi615f1c2f272dr2lhbbcrikq9mbi6rb")))

(define-public crate-libxdo-0.3.0 (c (n "libxdo") (v "0.3.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "libxdo-sys") (r "^0.9.0") (d #t) (k 0)))) (h "1ck7wfaw5ah1671492ya2rk1vb4swp00fzdw6rzy23k689ranih0")))

(define-public crate-libxdo-0.4.0 (c (n "libxdo") (v "0.4.0") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "libxdo-sys") (r "^0.10.0") (d #t) (k 0)))) (h "1vh2x5gs32chvw9zzspcczc8vn1mnl0akq2cl42aj1afv9m6f9lk")))

(define-public crate-libxdo-0.4.1 (c (n "libxdo") (v "0.4.1") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "libxdo-sys") (r "^0.10.0") (d #t) (k 0)))) (h "1252vxr0rg8lpfhj15m3s55rivbinvrcpxg24pklvb2z9qkany67")))

(define-public crate-libxdo-0.5.0 (c (n "libxdo") (v "0.5.0") (d (list (d (n "libxdo-sys") (r "^0.11.0") (d #t) (k 0)))) (h "0q6hvjdjvdcwhaqqs13z2w985zxvmsi7w33m877rv9kc183nagy5")))

(define-public crate-libxdo-0.5.1 (c (n "libxdo") (v "0.5.1") (d (list (d (n "libxdo-sys") (r "^0.11.0") (d #t) (k 0)))) (h "0m54zc0krz739clmjmjzgxxbsvalf23njpi7qjccns2sdwnwgyha")))

(define-public crate-libxdo-0.5.2 (c (n "libxdo") (v "0.5.2") (d (list (d (n "libxdo-sys") (r "^0.11.0") (d #t) (k 0)))) (h "1qbsgsqnqszcnayq3wkvmn8yk7awma80dls06cvvhmvmy5nfh9ic")))

(define-public crate-libxdo-0.6.0 (c (n "libxdo") (v "0.6.0") (d (list (d (n "libxdo-sys") (r "^0.11.0") (d #t) (k 0)))) (h "1nqlina6li1bmap0144h4hdsczyyfyinf87qvrw8xlm3as3kncq0")))

