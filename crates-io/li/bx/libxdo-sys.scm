(define-module (crates-io li bx libxdo-sys) #:use-module (crates-io))

(define-public crate-libxdo-sys-0.0.1 (c (n "libxdo-sys") (v "0.0.1") (d (list (d (n "libc") (r ">= 0.1.3") (d #t) (k 0)) (d (n "x11") (r ">= 0.0.14") (d #t) (k 0)))) (h "03dln9w0d5sd806qi9hd44w8mw5hm1i7ckfzwdmmxadqvjkpbnn4")))

(define-public crate-libxdo-sys-0.1.0 (c (n "libxdo-sys") (v "0.1.0") (d (list (d (n "libc") (r ">= 0.1.3") (d #t) (k 0)) (d (n "x11") (r ">= 0.0.14") (d #t) (k 0)))) (h "0jg6yjbpcr2ympfa4nr9qydyxvgxgqn3m6rhmi6rr211zx317lyw")))

(define-public crate-libxdo-sys-0.9.0 (c (n "libxdo-sys") (v "0.9.0") (d (list (d (n "libc") (r "*") (d #t) (k 0)) (d (n "x11") (r "*") (d #t) (k 0)))) (h "0zkiav21sr0bwf9wav9pmxy4n0bwnc4bjn6xbik3d84xmvbg00gg")))

(define-public crate-libxdo-sys-0.10.0 (c (n "libxdo-sys") (v "0.10.0") (d (list (d (n "libc") (r "^0.1.10") (d #t) (k 0)) (d (n "x11") (r "^2.1.0") (d #t) (k 0)))) (h "1vx1fr68qvl4zp6mv7zal48248pzf3wjskrrl7qvwgdm03yly28y")))

(define-public crate-libxdo-sys-0.11.0 (c (n "libxdo-sys") (v "0.11.0") (d (list (d (n "libc") (r "^0.2.20") (d #t) (k 0)) (d (n "x11") (r "^2.12.1") (d #t) (k 0)))) (h "04ljl0lmirg8a9q7w8ib2sybx35nnzpbw2xciayip0xpwbkvj8yv")))

