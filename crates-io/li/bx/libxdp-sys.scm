(define-module (crates-io li bx libxdp-sys) #:use-module (crates-io))

(define-public crate-libxdp-sys-0.0.1 (c (n "libxdp-sys") (v "0.0.1") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "1iapr5mc7wbrzmpwksw869p9zgg1i0prkfsbh4ca8ikbyg4h324d") (y #t)))

(define-public crate-libxdp-sys-0.0.2 (c (n "libxdp-sys") (v "0.0.2") (d (list (d (n "bindgen") (r "^0.58.1") (d #t) (k 1)))) (h "09d3m9qa15v5zds8fk365znk4c4sf8yqg86kann27q0a74b31ich") (y #t)))

(define-public crate-libxdp-sys-0.1.0 (c (n "libxdp-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "1m8lhryj5dhjwm09w69m0z0yfa9fsffsrjzl9ikvh23468sraqcv") (l "libxdp")))

(define-public crate-libxdp-sys-0.2.0 (c (n "libxdp-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.68.1") (d #t) (k 1)))) (h "1pcfsh6118h2gzi6f1554pg7jz67qz615hwnjnwvcpb4nkn7jlcp") (l "libxdp")))

