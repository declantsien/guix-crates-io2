(define-module (crates-io li bx libxbku-common) #:use-module (crates-io))

(define-public crate-libxbku-common-0.1.0 (c (n "libxbku-common") (v "0.1.0") (h "14ws09dqpl1dk8qm2y576iazw8yj0vblybl7ki8kd19zbhvmnnbr")))

(define-public crate-libxbku-common-0.2.0 (c (n "libxbku-common") (v "0.2.0") (h "0kci0x87jmm3vp66vpigs9qrmx2lmlsmkqnaxcwmzs7bsycwgkqy")))

(define-public crate-libxbku-common-0.3.0 (c (n "libxbku-common") (v "0.3.0") (h "04ya003rm5w6281hi7yi6lx2svnbckgv2w0rkdls3gnzbywz6i1w")))

