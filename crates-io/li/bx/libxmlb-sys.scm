(define-module (crates-io li bx libxmlb-sys) #:use-module (crates-io))

(define-public crate-libxmlb-sys-0.3.0 (c (n "libxmlb-sys") (v "0.3.0") (d (list (d (n "gio") (r "^0.17") (d #t) (k 0) (p "gio-sys")) (d (n "glib") (r "^0.17") (d #t) (k 0) (p "glib-sys")) (d (n "gobject") (r "^0.17") (d #t) (k 0) (p "gobject-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)))) (h "1i1h7cj3k3i28bijglp4q28h9dbacfkpi8c8s34v8bfsg76ps5ln") (f (quote (("dox" "glib/dox" "gobject/dox")))) (l "libxmlb-1")))

