(define-module (crates-io li bx libxslt) #:use-module (crates-io))

(define-public crate-libxslt-0.1.0 (c (n "libxslt") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libxml") (r "^0.2.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0vg5l31g5gnrc3xfixc12s4azmx9pybrx5cp9m2l9zk52377mg6i")))

(define-public crate-libxslt-0.1.1 (c (n "libxslt") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libxml") (r "^0.2.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0zxvrix5dwlzi3l3lnx15x4yphfgzklql1i0xx9sqzvpwjjr71ds")))

(define-public crate-libxslt-0.1.2 (c (n "libxslt") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libxml") (r "^0.3.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.2") (d #t) (k 1)))) (h "0898x8894lww4zjmvfys0k3vvhjaci54d28fp5f1jswxv1h2y6f7")))

