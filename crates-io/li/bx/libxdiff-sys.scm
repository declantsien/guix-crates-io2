(define-module (crates-io li bx libxdiff-sys) #:use-module (crates-io))

(define-public crate-libxdiff-sys-0.1.0 (c (n "libxdiff-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)))) (h "1im615y1bmwf7n1w3nf7xkr351ig8aqqwyi8iv46drfbsp1b5wk6")))

