(define-module (crates-io li bx libxdiff) #:use-module (crates-io))

(define-public crate-libxdiff-0.1.0 (c (n "libxdiff") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "libxdiff-sys") (r "^0.1.0") (d #t) (k 0)))) (h "13nf89k79i4csj7dkyghpn72jyvsvqj6wqwsn74kiyylknv0rs2s")))

(define-public crate-libxdiff-0.2.0 (c (n "libxdiff") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "libxdiff-sys") (r "^0.1.0") (d #t) (k 0)))) (h "11lxjabjsi6ksgj4i1pp8cvk3r6r348lfdrhnc2s4irr7qjb7llq") (f (quote (("std") ("default" "std"))))))

