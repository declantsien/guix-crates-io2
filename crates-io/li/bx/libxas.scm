(define-module (crates-io li bx libxas) #:use-module (crates-io))

(define-public crate-libxas-0.1.0 (c (n "libxas") (v "0.1.0") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)))) (h "0v9syskn8v4n9yb7ddf7g0jh4nvi4j4jvlf3hqffcdwn6jsgxciq") (f (quote (("rawbin") ("default" "rawbin" "chip8" "chip8-raw") ("chip8-raw") ("chip8" "chip8-raw"))))))

