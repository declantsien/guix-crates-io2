(define-module (crates-io li bx libxc-sys) #:use-module (crates-io))

(define-public crate-libxc-sys-0.1.0 (c (n "libxc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "09492v52d6sjcab7y7b8rjf3x3n21gyb6kb9c565ppdj3vwpibq5") (y #t)))

(define-public crate-libxc-sys-0.1.1 (c (n "libxc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.59") (d #t) (k 1)))) (h "1gaf1n1m6rmcrfqrmfw7b1ylg1z2zch390w4ns3q8cz2zg6d61r9")))

