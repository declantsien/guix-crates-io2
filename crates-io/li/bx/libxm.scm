(define-module (crates-io li bx libxm) #:use-module (crates-io))

(define-public crate-libxm-0.0.1 (c (n "libxm") (v "0.0.1") (d (list (d (n "libxm-sys") (r "^0.0.1") (d #t) (k 0)))) (h "0668rn9qjj4jm9kd9y3xcaqc101p3pd8sa9i94rsz63ia5a31vsc")))

(define-public crate-libxm-0.0.2 (c (n "libxm") (v "0.0.2") (d (list (d (n "libxm-sys") (r "^0.0.2") (d #t) (k 0)))) (h "0z09dwqkmllypwwv0b82ldm3g9nyn3pc3rrpzmmhg1lyfmak3fbf")))

(define-public crate-libxm-0.0.3 (c (n "libxm") (v "0.0.3") (d (list (d (n "gcc") (r "^0.2") (d #t) (k 1)))) (h "09wlr16x9bz3130mvfyz5lfkvwyvvyjgjfyskpnacfqaxa7vpbpx")))

(define-public crate-libxm-0.0.4 (c (n "libxm") (v "0.0.4") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "0nd16np0w5jjw5b1855fyafy1i03zzf6gk75a2jxhf67g3vb7rzb")))

(define-public crate-libxm-0.0.5 (c (n "libxm") (v "0.0.5") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)))) (h "1yc0i7zcdvk8pnqi269p3nryz7bjq3s32jbs2hriicavvmj8vdzg")))

(define-public crate-libxm-0.0.6 (c (n "libxm") (v "0.0.6") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "libc") (r "*") (d #t) (k 0)))) (h "0ba54ql458pajflwrhkxad1dkrghfaqfcbccabjap1pmywbak4jc")))

(define-public crate-libxm-1.0.0 (c (n "libxm") (v "1.0.0") (d (list (d (n "gcc") (r "^0.3") (d #t) (k 1)) (d (n "getopts") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "sdl2") (r "^0.3.0") (d #t) (k 2)))) (h "00b41g2v2ahlyz5w6x903p1qjmxgbq4vcfm5yzw4gqhhnq9f6i0a")))

