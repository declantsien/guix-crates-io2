(define-module (crates-io li bx libxm-soundboard) #:use-module (crates-io))

(define-public crate-libxm-soundboard-0.0.1 (c (n "libxm-soundboard") (v "0.0.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03bakxr1lpfl5ppasb2r84v3vxrav33xairf5l3baghbhpvarbv1")))

(define-public crate-libxm-soundboard-0.0.3 (c (n "libxm-soundboard") (v "0.0.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0044x7fz9gxiavghcx2lil97lwgkwyib274cnnsqlvbpd48g9a68")))

