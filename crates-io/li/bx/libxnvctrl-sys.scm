(define-module (crates-io li bx libxnvctrl-sys) #:use-module (crates-io))

(define-public crate-libXNVCtrl-sys-0.1.0 (c (n "libXNVCtrl-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)))) (h "0ammlyi5nwwzb9z3mj2zf6vq0x4nws9ds39rcsxci7lnq7pj46z0") (y #t)))

(define-public crate-libXNVCtrl-sys-1.29.0 (c (n "libXNVCtrl-sys") (v "1.29.0") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "x11") (r "^2.18.2") (d #t) (k 0)))) (h "1dr4h56vy2wqaacvl0c92i331319zibb1nh2ysh7j38wr36ww896")))

(define-public crate-libXNVCtrl-sys-1.29.1 (c (n "libXNVCtrl-sys") (v "1.29.1") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "x11") (r "^2.18.2") (d #t) (k 0)))) (h "0qy8w3hxx6sc5r4ysjimrnsdj8dzhhflx1zg7ih13qvdsc4mxfwf")))

(define-public crate-libXNVCtrl-sys-1.29.2 (c (n "libXNVCtrl-sys") (v "1.29.2") (d (list (d (n "bindgen") (r "^0.53.2") (d #t) (k 1)) (d (n "x11") (r "^2.18.2") (d #t) (k 0)))) (h "0qyv97drg4n4al95k4wxn0av014zmfmvb0vw7y6vx0kg3lhnvg6w")))

