(define-module (crates-io li te litex) #:use-module (crates-io))

(define-public crate-litex-0.1.0 (c (n "litex") (v "0.1.0") (d (list (d (n "linked_list_allocator") (r "^0.6") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6") (d #t) (k 0)))) (h "1jqz9isigaplm54q2xp1drislc616l9xy028c5kwfhwwd6r3y0h3")))

(define-public crate-litex-0.1.1 (c (n "litex") (v "0.1.1") (d (list (d (n "linked_list_allocator") (r "^0.6") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6") (d #t) (k 0)))) (h "0rq74b3di2npajdimixsh7brd541nxd6fgxfx1bqh1bynbgnvczc")))

(define-public crate-litex-0.1.2 (c (n "litex") (v "0.1.2") (d (list (d (n "linked_list_allocator") (r "^0.6") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6") (d #t) (k 0)))) (h "1r4fsjmxpaq2582cnyya7yir723iqh3i6c5kinf47ck3flbmjkmf")))

(define-public crate-litex-0.1.3 (c (n "litex") (v "0.1.3") (d (list (d (n "linked_list_allocator") (r "^0.6") (d #t) (k 0)) (d (n "riscv") (r "^0.5.3") (d #t) (k 0)) (d (n "riscv-rt") (r "^0.6") (d #t) (k 0)))) (h "1hbqxz9qnhkcdgl27mrdkd1mkxpnlys86zh4hpmmh6s4s7izmkcg")))

