(define-module (crates-io li te liter_derive) #:use-module (crates-io))

(define-public crate-liter_derive-0.0.1 (c (n "liter_derive") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "16076lplv3hiqlhfszw7fq7b3lx6mfii4m35nm2bwzbqi09bc8s7")))

(define-public crate-liter_derive-0.0.2 (c (n "liter_derive") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "1zcy873s2am544347bc9k5zf97qms4ayi5gvlldyycpdwvjpnhfk")))

(define-public crate-liter_derive-0.0.3 (c (n "liter_derive") (v "0.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0zmxsx3z7a8dllflm20af238j4nqj56fivac9zmag9ya33h0xikq")))

(define-public crate-liter_derive-0.0.4 (c (n "liter_derive") (v "0.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0sadlk1g433ysz8xrk0spvqfym5wa5pvzd156d4y47vwwra5fkyy")))

(define-public crate-liter_derive-0.0.5 (c (n "liter_derive") (v "0.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "100iijmvbzdm98h51fvrx3pk0xbymjvqrykl85l41k05xqmgz9ii")))

(define-public crate-liter_derive-0.0.6 (c (n "liter_derive") (v "0.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits" "full" "visit-mut"))) (d #t) (k 0)))) (h "0brdc9mbm4m8424jrxav605fbxkj78wfgvs9fqgixnmqw6ih4yly")))

