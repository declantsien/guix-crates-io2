(define-module (crates-io li te literate-crypto) #:use-module (crates-io))

(define-public crate-literate-crypto-0.0.1 (c (n "literate-crypto") (v "0.0.1") (h "0yk3cmr2jiqd5x14zdikw7k886bjgxdk9w3nd82a94q83r2v28v8")))

(define-public crate-literate-crypto-0.0.2 (c (n "literate-crypto") (v "0.0.2") (d (list (d (n "docext") (r "^0.0.10") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)))) (h "1kk57a369pcjablps6584zdrqwb5da7zf4pmxfi52jhxarjmbp25")))

