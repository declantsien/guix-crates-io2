(define-module (crates-io li te litebin) #:use-module (crates-io))

(define-public crate-litebin-0.1.0 (c (n "litebin") (v "0.1.0") (d (list (d (n "dict") (r "^0.1.5") (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "rocket-basicauth") (r "^2") (d #t) (k 0)))) (h "04lvzwpb0dx4b1qd3mp3al2d386n398fcqn5603dxh7x6pslz1x6") (r "1.62.0")))

(define-public crate-litebin-0.1.1 (c (n "litebin") (v "0.1.1") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "rocket-basicauth") (r "^2") (d #t) (k 0)))) (h "089pc7wys8i0jp484yp2f9wyl1nr75hnvbrjwr9cdp8mndm1zgzg") (r "1.62.0")))

(define-public crate-litebin-0.1.2 (c (n "litebin") (v "0.1.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "rocket") (r "^0.5.0-rc.1") (d #t) (k 0)) (d (n "rocket-basicauth") (r "^2") (d #t) (k 0)))) (h "1682s7jc9c3byz1kh95sl6i55nm21r468iyx7fwjcq7nix46k6ij") (r "1.60.0")))

