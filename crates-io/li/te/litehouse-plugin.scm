(define-module (crates-io li te litehouse-plugin) #:use-module (crates-io))

(define-public crate-litehouse-plugin-0.1.0 (c (n "litehouse-plugin") (v "0.1.0") (d (list (d (n "litehouse-plugin-macro") (r "^0.1.0") (d #t) (k 0)) (d (n "schemars") (r "^0.8.16") (f (quote ("derive"))) (k 0)) (d (n "serde") (r "^1.0.193") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.14") (d #t) (k 0)) (d (n "tracing") (r "^0.1.40") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.18") (f (quote ("env-filter"))) (d #t) (k 0)) (d (n "wit-bindgen") (r "^0.22.0") (d #t) (k 0)))) (h "10g0f0dmqkdjqjznfkn9kl0d73prmy35rrzd11pa67qbiy5aj1wn")))

