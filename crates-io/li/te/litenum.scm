(define-module (crates-io li te litenum) #:use-module (crates-io))

(define-public crate-litenum-0.1.0 (c (n "litenum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (k 0)))) (h "0b5brw33r7bw9w41nnagrs050hhy0lr70cw9wkl7z57bhnbg7xq3")))

(define-public crate-litenum-0.1.1 (c (n "litenum") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (k 0)))) (h "11n3z0740v1rjbmrvknmxjk8vpiqyddrb5r0rr2sqi48zlkjlkq0")))

(define-public crate-litenum-0.1.2 (c (n "litenum") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (k 0)))) (h "0jfglzcrisswxw09amy70338l0qb57wq9mvid0sdfzdb3qbr2l5q")))

(define-public crate-litenum-0.1.3 (c (n "litenum") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing"))) (k 0)))) (h "04zh84qi9b3c1ahp0kzivg36mll4khnk1bnpjv0z6n3arv1jnqwi")))

