(define-module (crates-io li te lite-json) #:use-module (crates-io))

(define-public crate-lite-json-0.1.0 (c (n "lite-json") (v "0.1.0") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1padijrly1daxz82ka2wwbpd4bdgdjhpsj8k80haaaxi7dqkba7s") (f (quote (("std") ("default" "std"))))))

(define-public crate-lite-json-0.1.1 (c (n "lite-json") (v "0.1.1") (d (list (d (n "lite-parser") (r "^0.1.1") (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "0vip0r3wj52q544dd3sqv3dvb83bp6xx7vl2yr9jfvn4sw1s5b2k") (f (quote (("std" "lite-parser/std") ("float" "num-traits") ("default" "std"))))))

(define-public crate-lite-json-0.1.2 (c (n "lite-json") (v "0.1.2") (d (list (d (n "lite-parser") (r "^0.1.2") (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "08z5w04l4hiqpn9w0i4jrgh222bpzcnyz5ly9h3i4vmc4cx72gn7") (f (quote (("std" "lite-parser/std") ("float" "num-traits") ("default" "std"))))))

(define-public crate-lite-json-0.1.3 (c (n "lite-json") (v "0.1.3") (d (list (d (n "lite-parser") (r "^0.1.2") (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "058pls39clh7npwz68k38xpn1kqvxmp7r2l2kd6nn0is8a2xjq04") (f (quote (("std" "lite-parser/std") ("float" "num-traits") ("default" "std"))))))

(define-public crate-lite-json-0.2.0 (c (n "lite-json") (v "0.2.0") (d (list (d (n "assert_float_eq") (r "^1.1.3") (d #t) (k 2)) (d (n "lite-parser") (r "^0.2.0") (k 0)) (d (n "num-traits") (r "^0.2") (o #t) (k 0)))) (h "18ncll9d8bw14d2l92704q94phqwvygpavbg1wd18lqizrzph3nd") (f (quote (("std" "lite-parser/std") ("float" "num-traits") ("default" "std"))))))

