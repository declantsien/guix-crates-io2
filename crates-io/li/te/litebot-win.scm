(define-module (crates-io li te litebot-win) #:use-module (crates-io))

(define-public crate-litebot-win-0.1.0 (c (n "litebot-win") (v "0.1.0") (h "1gn9awamd4qikkhk5mrmvrwvn7jmnd56qbbl7swb8pjbrhkqvq5d")))

(define-public crate-litebot-win-0.1.1 (c (n "litebot-win") (v "0.1.1") (h "1589mdjqnsmjyxlhcapf0857qpwywpkn5qacg2qnc46nnv1ml4ki")))

(define-public crate-litebot-win-0.1.2 (c (n "litebot-win") (v "0.1.2") (h "1afv1y71rdz2pa2pygwkqqxwgn5p87kz9f5idaqfx4ld0jz0sxhs")))

