(define-module (crates-io li te literalext) #:use-module (crates-io))

(define-public crate-literalext-0.1.0 (c (n "literalext") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0cany961xb4pbwv1s8ydy7id437r41pqxdppxn3914h3hfjvx53r") (f (quote (("proc-macro") ("i128") ("dummy") ("default" "proc-macro2"))))))

(define-public crate-literalext-0.1.1 (c (n "literalext") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.1") (o #t) (d #t) (k 0)))) (h "10aa300w4bqr5lfhbwi50651hwc6k43hfbkj1lg5m5r7jmlxshig") (f (quote (("proc-macro") ("i128") ("dummy") ("default" "proc-macro2"))))))

(define-public crate-literalext-0.2.0 (c (n "literalext") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0h1lahg8l883v02niwm2zrqyb2v2chkgvd8pzknmc93iazp87ngr") (f (quote (("proc-macro") ("i128") ("dummy") ("default" "proc-macro2"))))))

