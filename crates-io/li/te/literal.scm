(define-module (crates-io li te literal) #:use-module (crates-io))

(define-public crate-literal-0.1.0 (c (n "literal") (v "0.1.0") (h "0l458anw0jfxlb5hqa866dgjxhys1pzcaz3kr7h4kch2byzdpxmc")))

(define-public crate-literal-0.2.0 (c (n "literal") (v "0.2.0") (h "0scndi71h2q0nhj41hacq4xkj3c9zfsx9iq3233camd7v50cp60h")))

