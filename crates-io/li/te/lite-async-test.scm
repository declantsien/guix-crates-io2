(define-module (crates-io li te lite-async-test) #:use-module (crates-io))

(define-public crate-lite-async-test-0.1.0 (c (n "lite-async-test") (v "0.1.0") (d (list (d (n "futures-lite") (r "^1.13.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "0laxjr63c01asaxkn63525jkz775fdymic7bcmzkgqz0jbl3hqym")))

(define-public crate-lite-async-test-0.1.1 (c (n "lite-async-test") (v "0.1.1") (d (list (d (n "futures-lite") (r "^1.13.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1b6qahm6xmzlmswfy68g3iwyldcb3jv1wwq35s7xw2fb2vmxb04i")))

(define-public crate-lite-async-test-0.1.2 (c (n "lite-async-test") (v "0.1.2") (d (list (d (n "futures-lite") (r "^1.13.0") (d #t) (k 2)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.38") (f (quote ("full"))) (d #t) (k 0)))) (h "1h0nzzf002kp52mvccx9inhn31wzr0kimd2zkv86qcxyqffn4h4r")))

(define-public crate-lite-async-test-0.1.3 (c (n "lite-async-test") (v "0.1.3") (d (list (d (n "futures-lite") (r "^2.2.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.48") (f (quote ("full"))) (d #t) (k 0)))) (h "15v7c5libv62whg2qh0pr5dy9vh90gj9imr747hn1y7kq6rllpj8")))

