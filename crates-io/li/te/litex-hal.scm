(define-module (crates-io li te litex-hal) #:use-module (crates-io))

(define-public crate-litex-hal-0.1.0 (c (n "litex-hal") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)))) (h "0445dxjgcp24j30ljw5fhb1ygrdzjpax1ca15y9sw55safbds1iq")))

(define-public crate-litex-hal-0.2.0 (c (n "litex-hal") (v "0.2.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)))) (h "1qkm9ri6y1z71smja52f25xk160ncbqknn76mzgi1fskhxgvzs34") (f (quote (("gpio" "embedded-hal/unproven"))))))

(define-public crate-litex-hal-0.3.0 (c (n "litex-hal") (v "0.3.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)))) (h "1fn8xclr49g94pa11p8yp6amb0hllyq8zh67hy6dkslhll86brdz") (f (quote (("gpio" "embedded-hal/unproven"))))))

(define-public crate-litex-hal-0.4.0 (c (n "litex-hal") (v "0.4.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)))) (h "1q1wx6sn7hdfia9jp18ay0vcppfvair5lvsr47xgiflqbbrhhvrk") (f (quote (("gpio" "embedded-hal/unproven"))))))

