(define-module (crates-io li te litecoins) #:use-module (crates-io))

(define-public crate-litecoins-0.2.0 (c (n "litecoins") (v "0.2.0") (d (list (d (n "bitcoins") (r "^0.2.0") (d #t) (k 0)) (d (n "coins-core") (r "^0.2.0") (d #t) (k 0)))) (h "1k3mmh8gjn41r9girz4d77vh4962fr53c7x3mj7114srbhr69hm0")))

(define-public crate-litecoins-0.2.2 (c (n "litecoins") (v "0.2.2") (d (list (d (n "bitcoins") (r "^0.2.0") (d #t) (k 0)) (d (n "coins-core") (r "^0.2.0") (d #t) (k 0)))) (h "0ssvjs9z8mhijjay3xfr1rkshiqs1zzhz31m9r41lbia61mq04ri")))

(define-public crate-litecoins-0.6.0 (c (n "litecoins") (v "0.6.0") (d (list (d (n "bitcoins") (r "^0.6.0") (d #t) (k 0)) (d (n "coins-core") (r "^0.6.0") (d #t) (k 0)))) (h "1ynfzpdbsikasb5wyxg99nc4fb401x4ssc07pwmggkg29l5j5fg1")))

(define-public crate-litecoins-0.7.0 (c (n "litecoins") (v "0.7.0") (d (list (d (n "bitcoins") (r "^0.7.0") (d #t) (k 0)) (d (n "coins-core") (r "^0.7.0") (d #t) (k 0)))) (h "1j6q5pak35yfq557331n1ksjy1vay407kkh0ib7yxpw0d65z15sj")))

