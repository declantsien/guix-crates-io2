(define-module (crates-io li te litemint-royalty-contract) #:use-module (crates-io))

(define-public crate-litemint-royalty-contract-0.1.6 (c (n "litemint-royalty-contract") (v "0.1.6") (d (list (d (n "soroban-kit") (r "^0.1.9") (f (quote ("storage" "oracle"))) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.0.0") (f (quote ("testutils"))) (d #t) (k 2)))) (h "00r5m9228z31nr3lx8vycs2nm60y49qxvp3rz18b3arps496dana") (f (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-litemint-royalty-contract-0.1.7 (c (n "litemint-royalty-contract") (v "0.1.7") (d (list (d (n "soroban-kit") (r "^0.1.10") (f (quote ("storage" "oracle"))) (k 0)) (d (n "soroban-sdk") (r "^20.3.1") (d #t) (k 0)) (d (n "soroban-sdk") (r "^20.3.1") (f (quote ("testutils"))) (d #t) (k 2)))) (h "1k3dbj49zsc89z2rn6hjkpl918wi988jh26syfj9k8c6cqgs0lnm") (f (quote (("testutils" "soroban-sdk/testutils"))))))

