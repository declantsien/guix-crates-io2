(define-module (crates-io li te litecoin) #:use-module (crates-io))

(define-public crate-litecoin-0.0.0 (c (n "litecoin") (v "0.0.0") (h "05d1n3hbz6f71xjs75ik2dq4hk9267dvn3mdqg3bav2r7hrf23ks")))

(define-public crate-litecoin-0.1.3 (c (n "litecoin") (v "0.1.3") (h "1d1mv4y03w3fdsbqr6djgvixn37knxdh6v9f5vj1lfawz6a0796g")))

