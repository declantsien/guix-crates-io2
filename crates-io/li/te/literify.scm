(define-module (crates-io li te literify) #:use-module (crates-io))

(define-public crate-literify-0.1.0 (c (n "literify") (v "0.1.0") (d (list (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "logos") (r "^0.13") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "0x8igdvhrz8b9d6cx61sivx7mbifz231h1p19j5xfkkrvxjx7r2l")))

(define-public crate-literify-0.2.0 (c (n "literify") (v "0.2.0") (d (list (d (n "litrs") (r "^0.4") (d #t) (k 0)) (d (n "logos") (r "^0.13") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)))) (h "1x5b6km5ibvh60kva9wj5avpilqfrhfkfjnk82qkv6qy0lr26mgx")))

