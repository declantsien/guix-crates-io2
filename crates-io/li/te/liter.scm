(define-module (crates-io li te liter) #:use-module (crates-io))

(define-public crate-liter-0.0.1 (c (n "liter") (v "0.0.1") (d (list (d (n "liter_derive") (r "^0.0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.28") (d #t) (k 0)))) (h "1qgbbfb0g486h9imficyqm7924m3i1qm6pbv91z4d6p2g79yjrq4")))

(define-public crate-liter-0.0.2 (c (n "liter") (v "0.0.2") (d (list (d (n "const_format") (r "^0.2") (d #t) (k 0)) (d (n "liter_derive") (r "^0.0.2") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)))) (h "16f34fpn8fb4x7p9cm12jjrz7z8smdqyqwf5h09pdmn1lh29y0k6")))

(define-public crate-liter-0.0.3 (c (n "liter") (v "0.0.3") (d (list (d (n "construe") (r "^0.0.3") (d #t) (k 0)) (d (n "liter_derive") (r "^0.0.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.29") (d #t) (k 0)))) (h "04bva5d3mbq2szb7jg65b4x3jp6sajwpq6zcgr9r3j6jrw5kj4dw")))

(define-public crate-liter-0.0.4 (c (n "liter") (v "0.0.4") (d (list (d (n "construe") (r "^0.0.3") (d #t) (k 0)) (d (n "liter_derive") (r "^0.0.4") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (d #t) (k 0)))) (h "1hvcanxnia2caim6jhph8fqbwlbgyni0z6wq968ry7fqbi96skdi")))

(define-public crate-liter-0.0.5 (c (n "liter") (v "0.0.5") (d (list (d (n "construe") (r "^0.0.3") (d #t) (k 0)) (d (n "liter_derive") (r "^0.0.5") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (d #t) (k 0)))) (h "1qj0njlqz5q0d75jbswjaz78h24nsacvbmqhs0nczr21yhyc93rx")))

(define-public crate-liter-0.0.6 (c (n "liter") (v "0.0.6") (d (list (d (n "construe") (r "^0.0.3") (d #t) (k 0)) (d (n "liter_derive") (r "^0.0.6") (d #t) (k 0)) (d (n "rusqlite") (r "^0.30") (d #t) (k 0)))) (h "1yrw0wnk4q6qjgyx93d7klmc4za99nsy7xlrajwchijnsqi4vqdq")))

