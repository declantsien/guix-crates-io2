(define-module (crates-io li te litegrep) #:use-module (crates-io))

(define-public crate-litegrep-0.1.0 (c (n "litegrep") (v "0.1.0") (h "0vqf0bdg0y7fgmka7x2p5i1n6gv1w9h9cc071jknk01f8bwi6gra") (y #t)))

(define-public crate-litegrep-0.1.1 (c (n "litegrep") (v "0.1.1") (h "0n50s4i29z2ndvd0wld8yxl5vh5mh3dnpdla9fd064ibjp9jgkpw")))

(define-public crate-litegrep-0.1.2 (c (n "litegrep") (v "0.1.2") (h "0qvpr4wfcgkvmp57dsv5wjdlmj9qr5fjw85qinz2iq8mwzlby5y2")))

