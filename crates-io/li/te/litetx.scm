(define-module (crates-io li te litetx) #:use-module (crates-io))

(define-public crate-litetx-0.1.0 (c (n "litetx") (v "0.1.0") (d (list (d (n "bitflags") (r "^2.3") (d #t) (k 0)) (d (n "crc") (r "^3.0") (d #t) (k 0)) (d (n "lz4_flex") (r "^0.11") (f (quote ("frame"))) (d #t) (k 0)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "rusqlite") (r "^0.29") (f (quote ("bundled"))) (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "uuid") (r "^1.4") (f (quote ("v4"))) (d #t) (k 2)))) (h "1nkcwfd3a31wd996jdr3mqs579z8f58jd7snl7ilsjbyj2h720yz") (f (quote (("compat"))))))

