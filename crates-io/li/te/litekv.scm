(define-module (crates-io li te litekv) #:use-module (crates-io))

(define-public crate-litekv-0.1.0 (c (n "litekv") (v "0.1.0") (d (list (d (n "actix-web") (r "^3.3.2") (d #t) (k 0)) (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "argh") (r "^0.1.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (f (quote ("serde"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "indoc") (r "^1.0.3") (d #t) (k 0)) (d (n "rusqlite") (r "^0.25.3") (f (quote ("bundled" "chrono" "serde_json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "1rxvrw8y3062fwyl3mcl1xnl44gcwmi1vgfa1p53z13l64qikddi")))

