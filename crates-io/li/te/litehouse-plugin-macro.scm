(define-module (crates-io li te litehouse-plugin-macro) #:use-module (crates-io))

(define-public crate-litehouse-plugin-macro-0.1.0 (c (n "litehouse-plugin-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.76") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (f (quote ("full"))) (d #t) (k 0)))) (h "10k47z20rl7al8q49kpff8vk98m174gp2zglvgxd1146n7x594ah")))

