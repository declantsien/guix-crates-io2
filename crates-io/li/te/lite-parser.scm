(define-module (crates-io li te lite-parser) #:use-module (crates-io))

(define-public crate-lite-parser-0.1.1 (c (n "lite-parser") (v "0.1.1") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "070rxwsjgs6y3k7i940flpg637vy7dlrw82iv7s91m6gyyi2n3gh") (f (quote (("std") ("default" "std"))))))

(define-public crate-lite-parser-0.1.2 (c (n "lite-parser") (v "0.1.2") (d (list (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1xjjdjndg80wvjahhaw3bgwjyl70zlrh788m42zx3k7080p0jl0c") (f (quote (("std") ("default" "std"))))))

(define-public crate-lite-parser-0.2.0 (c (n "lite-parser") (v "0.2.0") (d (list (d (n "paste") (r "^1.0.7") (d #t) (k 0)))) (h "17n2gmzmbx0fr2k40l9chm7sidibs21ijw7x46d8hbf56zfgkmf3") (f (quote (("std") ("default" "std"))))))

