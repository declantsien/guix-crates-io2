(define-module (crates-io li te litebot-osx) #:use-module (crates-io))

(define-public crate-litebot-osx-0.1.0 (c (n "litebot-osx") (v "0.1.0") (h "1dsgbx6a5z9nia8h8p62ignc243ka2z866cs0ggxqlbbr1962p78")))

(define-public crate-litebot-osx-0.1.2 (c (n "litebot-osx") (v "0.1.2") (h "1yvgcdpnmlkf758f5d25vry0374lsfyiikkxj3x8wzghr86bf161")))

