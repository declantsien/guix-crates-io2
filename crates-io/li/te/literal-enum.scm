(define-module (crates-io li te literal-enum) #:use-module (crates-io))

(define-public crate-literal-enum-0.1.0 (c (n "literal-enum") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "18qyhc4xb3l1vkl0nj6b528qfd2y437nr0bdfk6kh8d8wwhnp7q0")))

(define-public crate-literal-enum-0.1.1 (c (n "literal-enum") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0likdfbyh03hxcdm80xwpqax9g6faja3gpv1mnn0fcq2gyhry3wh")))

(define-public crate-literal-enum-0.1.2 (c (n "literal-enum") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "1k5mssv0xi45jgy82724rbhlv3s4ysf7zzn32b4gjq9h4vwh1n2h")))

(define-public crate-literal-enum-0.1.3 (c (n "literal-enum") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "0mck7dv4n5rqf54qqz2a47lr90j94n3736l8daw3jwf7b2qyrm7i")))

(define-public crate-literal-enum-0.1.4 (c (n "literal-enum") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "03vps5py8yszj6vysbc8bx5r3j26kdh9m59mml53083lq70wdaxn")))

(define-public crate-literal-enum-0.1.5 (c (n "literal-enum") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.64") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.25") (f (quote ("full"))) (d #t) (k 0)))) (h "149zdz2sfdgh59wccp9ykirywynzg5d9zlnxl0dfhwwcczwfi9fw")))

