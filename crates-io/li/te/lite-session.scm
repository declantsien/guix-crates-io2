(define-module (crates-io li te lite-session) #:use-module (crates-io))

(define-public crate-lite-session-1.0.0 (c (n "lite-session") (v "1.0.0") (d (list (d (n "arrayvec") (r "^0.5.2") (d #t) (k 0)) (d (n "blake3") (r "^0.3.7") (d #t) (k 0)) (d (n "chacha20") (r "^0.6.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (k 0)) (d (n "nanorand") (r "^0.5.2") (f (quote ("chacha"))) (d #t) (k 0)) (d (n "secrecy") (r "^0.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "tai64") (r "^3.1.0") (d #t) (k 0)) (d (n "timelite") (r "^1.0.4") (d #t) (k 0)))) (h "0v1xiarwrkyn09hw1xyklbbvisd8znamqpjpinjkkbhj068dpc0r")))

