(define-module (crates-io li te literally) #:use-module (crates-io))

(define-public crate-literally-0.1.0 (c (n "literally") (v "0.1.0") (h "0w5qrmfk1fmr01nv1iqh0qlk3ccmqbm05dqgcgkhg9jszbr3321k")))

(define-public crate-literally-0.1.1 (c (n "literally") (v "0.1.1") (h "0vzrzhay9wfd01p2b86xwbl87njk9gs8bv8bh3d37pgw0in9v14m")))

(define-public crate-literally-0.1.2 (c (n "literally") (v "0.1.2") (h "1xgq1g9vsyxbll0argy7y4y95dg9q3vra8wxgrhh7km382jhd47d") (y #t)))

(define-public crate-literally-0.1.3 (c (n "literally") (v "0.1.3") (h "0d9w5wz7qpv7y3mrxssgl5shhsk7hwmcr3hz7nc5qk8db8zvxlph")))

