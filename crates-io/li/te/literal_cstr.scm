(define-module (crates-io li te literal_cstr) #:use-module (crates-io))

(define-public crate-literal_cstr-0.1.0 (c (n "literal_cstr") (v "0.1.0") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "1hm2lcfyc7b597qzkvq0p46chd0iwc0xkzjb8c85cij39yi06325")))

(define-public crate-literal_cstr-0.1.1 (c (n "literal_cstr") (v "0.1.1") (d (list (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (d #t) (k 0)))) (h "14grj01bn07a4xqz280xhhrnbj65vhb7532bj5vmh4cmfjwzs3r1")))

