(define-module (crates-io li te litequad) #:use-module (crates-io))

(define-public crate-litequad-0.1.0 (c (n "litequad") (v "0.1.0") (d (list (d (n "backtrace") (r "^0.3.60") (f (quote ("std" "libbacktrace"))) (o #t) (k 0)) (d (n "bumpalo") (r "^3.4") (d #t) (k 0)) (d (n "fontdue") (r "^0.5.0") (d #t) (k 0)) (d (n "glam") (r "^0.14") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "macroquad_macro") (r "^0.1.7") (d #t) (k 0)) (d (n "miniquad") (r "=0.3.0-alpha.45") (f (quote ("log-impl"))) (d #t) (k 0)) (d (n "quad-rand") (r "^0.2.1") (d #t) (k 0)))) (h "1v6i3prs89s5qhzli7hmhjd3nmdlmzsizgsq6jvdwfaidga6jxxk") (y #t)))

(define-public crate-litequad-0.1.1 (c (n "litequad") (v "0.1.1") (d (list (d (n "backtrace") (r "^0.3.60") (f (quote ("std" "libbacktrace"))) (o #t) (k 0)) (d (n "bumpalo") (r "^3.4") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.0") (d #t) (k 0)) (d (n "glam") (r "^0.20.5") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "macroquad_macro") (r "^0.1.7") (d #t) (k 0)) (d (n "miniquad") (r "=0.3.0-alpha.45") (f (quote ("log-impl"))) (d #t) (k 0)) (d (n "quad-rand") (r "^0.2.1") (d #t) (k 0)))) (h "0l7ag6kbhjbvvxn6rflfranyd1xahzi75wgk8yqhdk0v0zvpzkvq") (y #t)))

(define-public crate-litequad-0.1.2 (c (n "litequad") (v "0.1.2") (d (list (d (n "backtrace") (r "^0.3.60") (f (quote ("std" "libbacktrace"))) (o #t) (k 0)) (d (n "bumpalo") (r "^3.4") (d #t) (k 0)) (d (n "fontdue") (r "^0.7.0") (d #t) (k 0)) (d (n "glam") (r "^0.20.5") (f (quote ("scalar-math"))) (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "macroquad_macro") (r "^0.1.7") (d #t) (k 0)) (d (n "miniquad") (r "=0.3.0-alpha.45") (f (quote ("log-impl"))) (d #t) (k 0)) (d (n "quad-rand") (r "^0.2.1") (d #t) (k 0)))) (h "023vfvybbrqngjcbqqy4057lvpx0k3z9lrrqwyv0zvr1hw70rn0c")))

