(define-module (crates-io li te lite-log) #:use-module (crates-io))

(define-public crate-lite-log-0.1.0 (c (n "lite-log") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)))) (h "1yygk1d6ram4j3dp2b01pfrr7s6f756rjvcv8vyki8kdf2ixfm6w") (f (quote (("timestamps" "chrono") ("threads") ("stderr") ("nightly") ("default" "colors" "timestamps") ("colors" "colored"))))))

(define-public crate-lite-log-0.1.1 (c (n "lite-log") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (o #t) (d #t) (k 0)) (d (n "colored") (r "^2.0.4") (o #t) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (f (quote ("std"))) (d #t) (k 0)))) (h "0a8a0p8vc1k92ydggbdb2in9iin9g70lmfrzy6vmnkwii3i3bg4y") (f (quote (("timestamps" "chrono") ("threads") ("stderr") ("nightly") ("default" "colors" "timestamps") ("colors" "colored"))))))

