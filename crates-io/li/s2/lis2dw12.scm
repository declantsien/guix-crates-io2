(define-module (crates-io li s2 lis2dw12) #:use-module (crates-io))

(define-public crate-lis2dw12-0.1.0 (c (n "lis2dw12") (v "0.1.0") (d (list (d (n "accelerometer") (r "^0.12.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.5") (o #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)))) (h "0zwrf4lbl2jzsswhni7b08vjq0b5h46nhaymxyv5ky983am62agm") (f (quote (("out_f32" "num-traits" "num-derive" "cast") ("default" "out_f32"))))))

(define-public crate-lis2dw12-0.1.1 (c (n "lis2dw12") (v "0.1.1") (d (list (d (n "accelerometer") (r "^0.12.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.5") (o #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)))) (h "1ybq8rcyrdc7xvz3fkp3hn1kqz3wlay2q3nl55c0nknabc5cjaf2") (f (quote (("out_f32" "num-traits" "num-derive" "cast") ("default"))))))

(define-public crate-lis2dw12-0.1.2 (c (n "lis2dw12") (v "0.1.2") (d (list (d (n "accelerometer") (r "^0.12.0") (d #t) (k 0)) (d (n "cast") (r "^0.2.5") (o #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)))) (h "0g84ipz43hv2i0a48h12n6q4c920h6qb0m8p0w3p2lmh17mmg5f5") (f (quote (("out_f32" "num-traits" "num-derive" "cast") ("default"))))))

(define-public crate-lis2dw12-0.1.3 (c (n "lis2dw12") (v "0.1.3") (d (list (d (n "accelerometer") (r "^0.12.0") (d #t) (k 0)) (d (n "embassy-traits") (r "^0.0.2") (f (quote ("defmt"))) (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.5") (d #t) (k 0)) (d (n "nb") (r "^1.0") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (o #t) (k 0)))) (h "10hqjakhavqpqwp96jxhvx6bs70j5ih83cdh6wgwgmfhfsk7iray") (f (quote (("out_f32" "num-traits" "num-derive") ("non_blocking" "embassy-traits") ("default"))))))

