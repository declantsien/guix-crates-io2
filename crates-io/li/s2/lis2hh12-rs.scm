(define-module (crates-io li s2 lis2hh12-rs) #:use-module (crates-io))

(define-public crate-lis2hh12-rs-0.1.0 (c (n "lis2hh12-rs") (v "0.1.0") (d (list (d (n "accelerometer") (r "~0.6") (f (quote ("orientation"))) (d #t) (k 0)) (d (n "embedded-hal") (r "^0.2.3") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "^0.3.0") (d #t) (k 0)))) (h "0cr2dplbjyrikmf2rxjcy92r3jckx92iijrpjgamyn8z6ifhkv03")))

