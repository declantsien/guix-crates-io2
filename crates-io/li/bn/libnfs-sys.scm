(define-module (crates-io li bn libnfs-sys) #:use-module (crates-io))

(define-public crate-libnfs-sys-0.1.0 (c (n "libnfs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.26.3") (d #t) (k 1)))) (h "0aj2s8byz2s115rql7ja37f00g6ikhx2mdjm26lq59gf3q0cqvgd")))

(define-public crate-libnfs-sys-0.2.0 (c (n "libnfs-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.40.0") (d #t) (k 1)))) (h "0kfms9rz2893lns02a3crdgzbiq5f0r7iwzp41jg2wwp1w660l52")))

(define-public crate-libnfs-sys-0.2.1 (c (n "libnfs-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "~0.43") (d #t) (k 1)))) (h "1l9v2nd5l3r6rvq7vdjrcnp7lcgsidaqq32cc36mvh9zqmzhgng8")))

(define-public crate-libnfs-sys-0.2.2 (c (n "libnfs-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "~0.46") (d #t) (k 1)))) (h "03amdmzgqxvi1pmcy3whzd5fp8g1f93rilyi1rgrp5r3f4l9187i")))

(define-public crate-libnfs-sys-0.2.3 (c (n "libnfs-sys") (v "0.2.3") (d (list (d (n "bindgen") (r "^0.60") (d #t) (k 1)))) (h "1vixcw532frcyp2f4y0kwz0dgq7bgkdj4yj2n7svnv80pcx8x7ks")))

