(define-module (crates-io li bn libnfs) #:use-module (crates-io))

(define-public crate-libnfs-0.1.0 (c (n "libnfs") (v "0.1.0") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "libnfs-sys") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.11") (d #t) (k 0)))) (h "1f9q3mx2bmx36kck76hqhb7wqpyxz2a8vglywg9pblv80wxv1a94")))

(define-public crate-libnfs-0.1.1 (c (n "libnfs") (v "0.1.1") (d (list (d (n "libc") (r "~0.2") (d #t) (k 0)) (d (n "libnfs-sys") (r "~0.2") (d #t) (k 0)) (d (n "nix") (r "~0.25") (d #t) (k 0)))) (h "1h4rz4f5rwq9b2fsidrr7k99w5lbca5n0zwa5jbbq92jhfnnw5dw")))

