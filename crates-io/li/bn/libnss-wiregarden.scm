(define-module (crates-io li bn libnss-wiregarden) #:use-module (crates-io))

(define-public crate-libnss-wiregarden-0.1.0 (c (n "libnss-wiregarden") (v "0.1.0") (d (list (d (n "debug_print") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnss") (r "^0.4") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)) (d (n "rusqlite") (r "^0.24") (d #t) (k 0)))) (h "1m75xjkc3xb1ny11fxriiqa2cdp9b14mw92k8pppl6byyxrrir4g")))

