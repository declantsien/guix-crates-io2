(define-module (crates-io li bn libnetfilter_queue) #:use-module (crates-io))

(define-public crate-libnetfilter_queue-0.0.2 (c (n "libnetfilter_queue") (v "0.0.2") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "0.1.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "1cw6mm7ficssf9w9cdv750vyvn659krdhpzdlmysbfc3f0fh9cw9")))

(define-public crate-libnetfilter_queue-0.0.3 (c (n "libnetfilter_queue") (v "0.0.3") (d (list (d (n "lazy_static") (r "0.1.*") (d #t) (k 0)) (d (n "libc") (r "0.1.*") (d #t) (k 0)) (d (n "num") (r "0.1.*") (d #t) (k 0)))) (h "11dncd0ifi38kh4zaciqnmyqlv4j21nq8kam6bc46m1ymj2p5jvk")))

