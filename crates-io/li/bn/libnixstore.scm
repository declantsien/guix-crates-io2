(define-module (crates-io li bn libnixstore) #:use-module (crates-io))

(define-public crate-libnixstore-0.1.0 (c (n "libnixstore") (v "0.1.0") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12f7nr3l4d9id7n0zyl4yhhckcpadw6wdfgm4xg9kcqfaxzr13q5")))

(define-public crate-libnixstore-0.1.1 (c (n "libnixstore") (v "0.1.1") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1ijy986dsprr8svidy9bg8ji5bg5098yixwlwncp2sskcnxm0qy4")))

(define-public crate-libnixstore-0.2.0 (c (n "libnixstore") (v "0.2.0") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1fxs5jlrdbgi7h2771y8rvgryx49rm8q6yym2acgbb1l29dzw35r")))

(define-public crate-libnixstore-0.3.0 (c (n "libnixstore") (v "0.3.0") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "01z63cdgy0jpfrdns9xnhzn100c8l8pmdhdn32q41sgvsrlpw582")))

(define-public crate-libnixstore-0.4.0 (c (n "libnixstore") (v "0.4.0") (d (list (d (n "cxx") (r "^1") (d #t) (k 0)) (d (n "cxx-build") (r "^1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1r736iaivp7z4hf81jgyc7rjyabrhzfizjd9wcsbvq7hwn1j944w")))

