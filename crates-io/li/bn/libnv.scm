(define-module (crates-io li bn libnv) #:use-module (crates-io))

(define-public crate-libnv-0.1.0 (c (n "libnv") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0396g0ryisz2bh0nafj1ls4h1hhrlnb4i27svy6f2rxa3sxxvxp8") (y #t)))

(define-public crate-libnv-0.1.1 (c (n "libnv") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0bashqcdybhpns5y2plvh6rvzz7jp4gmbdzr57k328j2rz55h2kk")))

(define-public crate-libnv-0.1.2 (c (n "libnv") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "03bz3nsksxj1zc16p1h1ynngl562nvc6i36gqjzq12a7dk6rcglw")))

(define-public crate-libnv-0.1.3 (c (n "libnv") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1") (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0hr7yirl90dg7y46ypgg1iw0i4arxqnxkwvkh48gywf1v5srqf06")))

(define-public crate-libnv-0.2.0 (c (n "libnv") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "1lqas83jj5qx4z280p2nkrh5ll0zrz8dykdfpkb5v648jgyjvsfl") (f (quote (("nvpair" "nvpair-sys") ("libnv" "libnv-sys") ("default" "libnv" "nvpair"))))))

(define-public crate-libnv-0.2.1 (c (n "libnv") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "0ibrhwm6gvxp4wpzszv20n9y4kckw79nrpis0sbgbz4c8wzay67g") (f (quote (("nvpair" "nvpair-sys") ("libnv" "libnv-sys") ("default" "libnv" "nvpair"))))))

(define-public crate-libnv-0.2.2 (c (n "libnv") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^1.2") (d #t) (k 0)))) (h "1sbdmdhqd6040xx7qvxafhmgqsn0h8vjcvj045anwr4c3aqnc574") (f (quote (("nvpair" "nvpair-sys") ("libnv" "libnv-sys") ("default" "libnv" "nvpair"))))))

(define-public crate-libnv-0.2.3 (c (n "libnv") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "0mwv8ivgmpam7ps753q43lb2lgbq3a18pxdhsqiq8r10bfxn8wji") (f (quote (("nvpair" "nvpair-sys") ("libnv" "libnv-sys") ("default" "libnv" "nvpair"))))))

(define-public crate-libnv-0.3.0 (c (n "libnv") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "1kp3fppss2j4iq32m3snv85mz0srnqc7gk7b7shvpsswx6zacbg2") (f (quote (("nvpair" "nvpair-sys") ("libnv" "libnv-sys") ("default" "libnv" "nvpair"))))))

(define-public crate-libnv-0.4.0 (c (n "libnv") (v "0.4.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "1kydj60wn01937lxpbh6jkgij3wp04jcbj46ck2vpickwl9vb3br") (f (quote (("nvpair" "nvpair-sys") ("libnv" "libnv-sys") ("default" "libnv" "nvpair")))) (y #t)))

(define-public crate-libnv-0.4.1 (c (n "libnv") (v "0.4.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "1d75pb4hp2xfvdymbrpnwq455zdwidri3mswmq3rjlsh312ciz71") (f (quote (("nvpair" "nvpair-sys") ("libnv" "libnv-sys") ("default" "libnv" "nvpair"))))))

(define-public crate-libnv-0.4.2 (c (n "libnv") (v "0.4.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "0f6nhrmnczpxq6v7yn7sw885nia0c1vxf5kvb1v2bfchdajnw15x") (f (quote (("nvpair" "nvpair-sys") ("libnv" "libnv-sys") ("default" "libnv" "nvpair"))))))

(define-public crate-libnv-0.4.3 (c (n "libnv") (v "0.4.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libnv-sys") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "nvpair-sys") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "quick-error") (r "^2.0") (d #t) (k 0)))) (h "0l7qbmq0kfl45p05ah78310ak3rd6gyfc8r1r0vj30xs4kvczzp4") (f (quote (("nvpair" "nvpair-sys") ("libnv" "libnv-sys") ("default" "libnv" "nvpair"))))))

