(define-module (crates-io li bn libnfd) #:use-module (crates-io))

(define-public crate-libnfd-0.1.1 (c (n "libnfd") (v "0.1.1") (d (list (d (n "cocoa") (r "^0.2.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "gtk") (r "^0.0.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0kqwx3sahx1al3zgr40zcx4c0f19cx66p80jr4f1zg6f42k033w0")))

(define-public crate-libnfd-0.1.2 (c (n "libnfd") (v "0.1.2") (d (list (d (n "cocoa") (r "^0.2.5") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "gtk") (r "^0.0.7") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)))) (h "0aynm11y5pq8m9y59fd1xn24cc5k1d5y20i33qm7id5r4gf34c6x")))

(define-public crate-libnfd-0.2.0 (c (n "libnfd") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfd-sys") (r "^0.1.3") (d #t) (k 0)))) (h "1nb0bhvw8sgfjqyksy37phs5nvxgd3syfzj5sn4fxdyygibckcgg")))

(define-public crate-libnfd-0.2.1 (c (n "libnfd") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfd-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0s2rwdrkwwcqb3989m3hh1m2jjjr19b43jf5nf9zrhnwrighw2rr")))

(define-public crate-libnfd-0.2.2 (c (n "libnfd") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfd-sys") (r "^0.1.3") (d #t) (k 0)))) (h "0w6mslc4zss9k7ij4yb1zq16cbkghj1lmn6c2frc0vy42sfq79c1")))

(define-public crate-libnfd-0.2.3 (c (n "libnfd") (v "0.2.3") (d (list (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "nfd-sys") (r "^0.1.5") (d #t) (k 0)))) (h "0b01j93h5iq48jmhm0aqa1947sh9nnwb7f39ap0gahw38h4qhmb6")))

