(define-module (crates-io li bn libnet-sys) #:use-module (crates-io))

(define-public crate-libnet-sys-0.1.0 (c (n "libnet-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "172q6gwswzibflgmqa6xcw7a739qp52i6p3iv97h99z6azm4igww")))

