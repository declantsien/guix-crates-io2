(define-module (crates-io li bn libnm-sys) #:use-module (crates-io))

(define-public crate-libnm-sys-0.1.0 (c (n "libnm-sys") (v "0.1.0") (d (list (d (n "gio-sys") (r "^0.10.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.10.1") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1akzs6qpjaikpmik4rv5lg5dmi8r02l8pfpdnba7k5d42v9qkcxz")))

(define-public crate-libnm-sys-0.2.0 (c (n "libnm-sys") (v "0.2.0") (d (list (d (n "gio-sys") (r "^0.10.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.10.1") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.10.0") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.18") (d #t) (k 1)))) (h "1gi64bnnqbn6f31as7nkk7krhanczmwf717cbhzgarfm4fqq4qjh")))

