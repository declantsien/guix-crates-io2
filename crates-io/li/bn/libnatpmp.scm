(define-module (crates-io li bn libnatpmp) #:use-module (crates-io))

(define-public crate-libnatpmp-0.1.1 (c (n "libnatpmp") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "176gywcqzi3yhxyjhhpapxd8gm2pb16f13qbfafsrbg5ffl2kp7n") (y #t)))

(define-public crate-libnatpmp-0.1.2 (c (n "libnatpmp") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "102vzs6s1wyldbs0vf1f51yw3071scag2ak3dq7afr78jh6wwi7d") (y #t)))

(define-public crate-libnatpmp-0.1.3 (c (n "libnatpmp") (v "0.1.3") (d (list (d (n "natpmp") (r "^0.1") (d #t) (k 0)))) (h "1jkym6rgmrj5xmkbpyd7ilm93zgfmmfyqaj1asgs2b8nc8vkq9bc")))

