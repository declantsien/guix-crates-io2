(define-module (crates-io li bn libnum) #:use-module (crates-io))

(define-public crate-libnum-0.1.0 (c (n "libnum") (v "0.1.0") (d (list (d (n "num") (r "^0.4.0") (f (quote ("num-bigint"))) (d #t) (k 0)) (d (n "num-bigfloat") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0v46m5c3g6bixzi6iqizd2fhzahzysy8nzz12mm2r6rp1m5f99b4")))

(define-public crate-libnum-0.1.1 (c (n "libnum") (v "0.1.1") (d (list (d (n "num") (r "^0.4.0") (f (quote ("num-bigint"))) (d #t) (k 0)) (d (n "num-bigfloat") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1zm0bbg7npi2b9p4ac46112j5v34s6b9s9iprqahkgxmjaz45zs3")))

(define-public crate-libnum-0.1.2 (c (n "libnum") (v "0.1.2") (d (list (d (n "num") (r "^0.4.0") (f (quote ("num-bigint"))) (d #t) (k 0)) (d (n "num-bigfloat") (r "^1.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0v6iga3w12wpn1j0nysixra45v6cavxbfyc8xr9b280ixsnhdzs8")))

