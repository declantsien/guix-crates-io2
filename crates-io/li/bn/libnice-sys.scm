(define-module (crates-io li bn libnice-sys) #:use-module (crates-io))

(define-public crate-libnice-sys-0.1.0 (c (n "libnice-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1kgpacz1rrfxh549nfafyg9zbb65sp39nq75hqabd4flf5nzh52c") (l "nice")))

(define-public crate-libnice-sys-0.2.0 (c (n "libnice-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "gio-sys") (r "^0.7") (d #t) (k 0)) (d (n "glib-sys") (r "^0.7") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.7") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jdirhl48zdvd0vzjiqfpg5y0n5vf4a3bpak2br9qia4c4hx8xj0") (l "nice")))

(define-public crate-libnice-sys-0.3.0 (c (n "libnice-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.53") (d #t) (k 1)) (d (n "gio-sys") (r "^0.9") (d #t) (k 0)) (d (n "glib-sys") (r "^0.9") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.9") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1n3sq642mmh5j5q1b25dgydbf7p512vrdjs5a6q0ksm79qdvp8wl") (l "nice")))

(define-public crate-libnice-sys-0.4.0 (c (n "libnice-sys") (v "0.4.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "gio-sys") (r "^0.10") (d #t) (k 0)) (d (n "glib-sys") (r "^0.10") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.10") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0yb5d05vbx86bbhqy5zdwbasqk1b8khk2d4sgfglnzms42195yh7") (l "nice")))

