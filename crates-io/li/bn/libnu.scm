(define-module (crates-io li bn libnu) #:use-module (crates-io))

(define-public crate-libnu-0.1.0 (c (n "libnu") (v "0.1.0") (d (list (d (n "novusk_syscalls") (r "^0.1.0") (f (quote ("novusk_uefi"))) (d #t) (k 0)) (d (n "uefi") (r "^0.8.1") (d #t) (k 0)))) (h "1d2zk5vi65771vdcm0ci5y8w4zzf9bdff9m26yr9svmsk3l9jfqf") (y #t)))

(define-public crate-libnu-0.1.1 (c (n "libnu") (v "0.1.1") (d (list (d (n "novusk_syscalls") (r "^0.1.0") (f (quote ("novusk_uefi"))) (d #t) (k 0)) (d (n "uefi") (r "^0.8.1") (d #t) (k 0)))) (h "0r8h5xb962raabn8jmkrd6fisrw927c00qc903h96ryss66fx132") (y #t)))

(define-public crate-libnu-0.1.2 (c (n "libnu") (v "0.1.2") (d (list (d (n "novusk_syscalls") (r "^0.1.0") (f (quote ("novusk_uefi"))) (d #t) (k 0)) (d (n "uefi") (r "^0.8.1") (d #t) (k 0)))) (h "0gana3m82rbhmz4ywgkcv1cqwb72v7zwdg9y62l6mzlz98g4lccx") (y #t)))

(define-public crate-libnu-0.1.3 (c (n "libnu") (v "0.1.3") (h "0arn6s2b0216r0s0ph76mdiqidp3vvh6pa0j0sz4cl90xx4dwccl") (y #t)))

