(define-module (crates-io li bn libnet-rs) #:use-module (crates-io))

(define-public crate-libnet-rs-0.1.1 (c (n "libnet-rs") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "106736bqy7s3fqmnriqydgxccfwmbplicms9lnwhjh23kxxdwnvr") (y #t)))

(define-public crate-libnet-rs-0.1.2 (c (n "libnet-rs") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1nv1ivxa21bancgmdgh1dmfh3mkl5vs9vhp14wpb0dsj7dw0z46r") (y #t)))

(define-public crate-libnet-rs-0.1.3 (c (n "libnet-rs") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "17qnk8x5wxhqnx6173qjbz1z6jfmqa4nd3gq54vch44di4w2mpq1") (y #t)))

(define-public crate-libnet-rs-0.1.4 (c (n "libnet-rs") (v "0.1.4") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.14") (d #t) (k 1)))) (h "1qc0sj9d0a4ihxlgx46j3fzgxxj63ap5jnjj15fvdm8z5pca6zi7") (y #t)))

