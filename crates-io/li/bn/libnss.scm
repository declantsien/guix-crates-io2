(define-module (crates-io li bn libnss) #:use-module (crates-io))

(define-public crate-libnss-0.1.0 (c (n "libnss") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1ml0lxzk4z34qy96l2dk8cp3vzxaglsm7jgkyajmf6qlfhdva6c6")))

(define-public crate-libnss-0.2.0 (c (n "libnss") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.3.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "11hgjyn35zbknafxb12s6a80zzc8c1148dynik91qjzglabbs6kb")))

(define-public crate-libnss-0.3.0 (c (n "libnss") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "1l9jmgbq3bcqlgjcngh9v1d7nz4h6nd8ypq9agm9h2742wsr5sd6")))

(define-public crate-libnss-0.4.0 (c (n "libnss") (v "0.4.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^0.1") (d #t) (k 0)))) (h "0k5kang9305vjkpg83sq5az4cqkb2xqrnv0hr1d06apgxgspxdj8")))

(define-public crate-libnss-0.5.0 (c (n "libnss") (v "0.5.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "0hm4xzlzi10ilfdnd946889xv34nc7fchg2m350rs8nw23fbvg21")))

(define-public crate-libnss-0.6.0 (c (n "libnss") (v "0.6.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "00wkjsai067mgxqcvsznnffhlb65am6446sa7pfkf3dx46ymr8v0")))

(define-public crate-libnss-0.7.0 (c (n "libnss") (v "0.7.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "1cr4794npwmgcma3p5jq0ql9jbxn8pz1glcp8jbl3gqvn02dcm8r")))

(define-public crate-libnss-0.8.0 (c (n "libnss") (v "0.8.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)))) (h "06q2q776drhy61zifwz1kgxkprc00yw7kvg9arydd1y73wlw0jrw")))

