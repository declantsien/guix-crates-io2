(define-module (crates-io li bn libnetkeeper) #:use-module (crates-io))

(define-public crate-libnetkeeper-0.1.0 (c (n "libnetkeeper") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.1.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clippy") (r "^0.0.160") (o #t) (d #t) (k 0)) (d (n "linked-hash-map") (r "^0.3.0") (d #t) (k 0)) (d (n "md4") (r "^0.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.3.15") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "07pki74g8v3s29hlpr1nbgpivzhkh4p512f5j6nq2pq3wg5h1vsp") (f (quote (("srun3k") ("singlenet") ("netkeeper") ("ipclient") ("ghca") ("drcom") ("dev" "default" "clippy") ("default" "netkeeper" "singlenet" "drcom" "ghca" "srun3k" "ipclient"))))))

