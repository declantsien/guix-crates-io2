(define-module (crates-io li bn libnixstore-sys) #:use-module (crates-io))

(define-public crate-libnixstore-sys-0.1.0 (c (n "libnixstore-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.42") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0scmhvbbx9fnax84amrxqzk91h8gias94kxa5bhi6j3878a81m1j") (l "nixstore-c")))

