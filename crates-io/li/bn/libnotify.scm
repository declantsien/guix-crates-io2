(define-module (crates-io li bn libnotify) #:use-module (crates-io))

(define-public crate-libnotify-0.0.1 (c (n "libnotify") (v "0.0.1") (d (list (d (n "glib-2-0-sys") (r "*") (d #t) (k 0)) (d (n "libnotify-sys") (r "*") (d #t) (k 0)))) (h "167bcrf7nnlq6jcjcw8ak8mg5nzlqd06mjz6rfmhwaxx00yl0d0q")))

(define-public crate-libnotify-0.0.2 (c (n "libnotify") (v "0.0.2") (d (list (d (n "glib-2-0-sys") (r "*") (d #t) (k 0)) (d (n "libnotify-sys") (r "*") (d #t) (k 0)))) (h "10i9cw8fxjd6cwc4wdr01fk8nkvjmymglcrm49qnw11akhqzx0yr")))

(define-public crate-libnotify-0.0.3 (c (n "libnotify") (v "0.0.3") (d (list (d (n "glib-2-0-sys") (r "*") (d #t) (k 0)) (d (n "libnotify-sys") (r "*") (d #t) (k 0)))) (h "1lapnpfwnqfghcdbrpl0393mw7j07rns3rkv0dxijih0kqn84ccc")))

(define-public crate-libnotify-0.0.4 (c (n "libnotify") (v "0.0.4") (d (list (d (n "glib-2-0-sys") (r "*") (d #t) (k 0)) (d (n "gtypes") (r "*") (d #t) (k 0)) (d (n "libnotify-sys") (r "*") (d #t) (k 0)))) (h "0p7sf8j901c37w0vsnzdw16092yvyv11rq53w1y0z0k0jlxva04a")))

(define-public crate-libnotify-0.1.0 (c (n "libnotify") (v "0.1.0") (d (list (d (n "glib-2-0-sys") (r "^0.46.0") (d #t) (k 0)) (d (n "gtypes") (r "^0.1.2") (d #t) (k 0)) (d (n "libnotify-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0ipr6plsz48ab61xpj67lrkyy2qgfvgmvnfqnfzqa6ajyyv6lmjj")))

(define-public crate-libnotify-0.2.0 (c (n "libnotify") (v "0.2.0") (d (list (d (n "glib-2-0-sys") (r "^0.46.0") (d #t) (k 0)) (d (n "gtypes") (r "^0.1.2") (d #t) (k 0)) (d (n "libnotify-sys") (r "^0.2.0") (d #t) (k 0)))) (h "1pmbadskf1czgccvz3h7pzi5d0ajrvmidq886ak7gk2b458q89hh")))

(define-public crate-libnotify-0.3.0 (c (n "libnotify") (v "0.3.0") (d (list (d (n "glib-2-0-sys") (r "^0.46.0") (d #t) (k 0)) (d (n "gtypes") (r "^0.2") (d #t) (k 0)) (d (n "libnotify-sys") (r "^0.3") (d #t) (k 0)))) (h "0kwak8511x9q7n501iwn243kf96znqv3bbdl5pkjd0db39hy2ygx")))

(define-public crate-libnotify-0.4.0 (c (n "libnotify") (v "0.4.0") (d (list (d (n "glib-2-0-sys") (r "^0.46.0") (d #t) (k 0)) (d (n "gtypes") (r "^0.2") (d #t) (k 0)) (d (n "libnotify-sys") (r "^0.3") (d #t) (k 0)))) (h "026k8dfh5810hrgpji513b3p91wvmix0h4v8vwfnw968h7q6r8kg")))

(define-public crate-libnotify-1.0.0 (c (n "libnotify") (v "1.0.0") (d (list (d (n "error-chain") (r "^0.10.0") (d #t) (k 0)) (d (n "gdk-pixbuf") (r "^0.1.3") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "glib") (r "^0.1.3") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "libnotify-sys") (r "^1.0.0") (d #t) (k 0)))) (h "000c9nc9pb08h0hhdk1slni3f2hlg3id5xh3fqnj7lmlbq7ag4bw")))

(define-public crate-libnotify-1.0.1 (c (n "libnotify") (v "1.0.1") (d (list (d (n "gdk-pixbuf") (r "^0.1.3") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "glib") (r "^0.1.3") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "libnotify-sys") (r "^1.0.0") (d #t) (k 0)))) (h "1sa6ygry1adwmwc989ns809jb3j3hjk8k16nz6yrz916b8dxb1qc")))

(define-public crate-libnotify-1.0.2 (c (n "libnotify") (v "1.0.2") (d (list (d (n "gdk-pixbuf") (r "^0.2.0") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "glib") (r "^0.3.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "libnotify-sys") (r "^1.0.1") (d #t) (k 0)))) (h "0s8g2m3rp90mh3i1hhvj90ggaci8z5xkpz4r7z4mc9iq0c7rjd49")))

(define-public crate-libnotify-1.0.3 (c (n "libnotify") (v "1.0.3") (d (list (d (n "gdk-pixbuf") (r "^0.3.0") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "glib") (r "^0.4.1") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libnotify-sys") (r "^1.0.2") (d #t) (k 0)))) (h "0bfc83hzzqkkd9hclvd2s79sxmvq0clsiqzxqv6ggy66id7nll0h")))

