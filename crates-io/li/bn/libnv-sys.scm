(define-module (crates-io li bn libnv-sys) #:use-module (crates-io))

(define-public crate-libnv-sys-0.1.0 (c (n "libnv-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0a06cmj6vrszjqy43rr0c764kq3mcwxr9w5mfi0lsr673cyhwxmr")))

(define-public crate-libnv-sys-0.1.1 (c (n "libnv-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "180d9s18wpbysh8p7ydab48mk0snay1a15kc3lqp04ifamgjhhi9")))

(define-public crate-libnv-sys-0.1.2 (c (n "libnv-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1j9jkwckwgypin3n714nh1c6j7yn90y9gkb7mf6bnchmam4fl38d")))

(define-public crate-libnv-sys-0.1.3 (c (n "libnv-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vy4v5lilzcx0xd7vc52x6nayyiv070kipcli6lnw83hmf62jn8v")))

(define-public crate-libnv-sys-0.1.4 (c (n "libnv-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ym687qfvdi2kfsnmc5b8g36gkgsw11xwaf77m53bxl04yq4lw4n")))

(define-public crate-libnv-sys-0.1.5 (c (n "libnv-sys") (v "0.1.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0g10fg2i6jdwy3dk3cp79ii5pdzjik11rf6sj0c37p90ncisriid") (y #t)))

(define-public crate-libnv-sys-0.2.0 (c (n "libnv-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.63.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "095vwy1rm40894g0v3yi0z4hvkfi318qb18k0r31kykwk2lvpw7d") (l "libnv")))

(define-public crate-libnv-sys-0.2.1 (c (n "libnv-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.64.0") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "0lr6cgfs8hd5adfpsbv95im2j0m1x69rzqsiy0kwf643wyd215ba") (l "libnv")))

(define-public crate-libnv-sys-0.2.2 (c (n "libnv-sys") (v "0.2.2") (d (list (d (n "bindgen") (r "^0.66.1") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.6.0") (d #t) (k 1)))) (h "04vcfh8v8az55qx9gjs69x49acmxr9a6ash3n4ybqi09li3qmp12") (l "libnv")))

