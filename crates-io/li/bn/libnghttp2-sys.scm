(define-module (crates-io li bn libnghttp2-sys) #:use-module (crates-io))

(define-public crate-libnghttp2-sys-0.1.0 (c (n "libnghttp2-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)))) (h "1641wc4w0gl5j7xc4z0wmqakh5zpkj5qyfjgwhjzdjaw8y0zpysg") (l "nghttp2")))

(define-public crate-libnghttp2-sys-0.1.1 (c (n "libnghttp2-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x74vfrhmp78q6gswjrr4j53ciglimk8xaqy5mr0nwx4pmk7jpfp") (l "nghttp2")))

(define-public crate-libnghttp2-sys-0.1.2 (c (n "libnghttp2-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qr4lyh7righx9n22c7amlcpk906rn1jnb2zd6gdfpa3yi24s982") (l "nghttp2")))

(define-public crate-libnghttp2-sys-0.1.3 (c (n "libnghttp2-sys") (v "0.1.3") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "04ab2jm4b6142bf6iz552zyjrghjwfn659f9jiv2kg06h7ngandk") (l "nghttp2")))

(define-public crate-libnghttp2-sys-0.1.4+1.41.0 (c (n "libnghttp2-sys") (v "0.1.4+1.41.0") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1wcd93a8cw1h9y25834160y6ng982fi0qcd277hpjvhnvz34wqh3") (l "nghttp2")))

(define-public crate-libnghttp2-sys-0.1.5+1.42.0 (c (n "libnghttp2-sys") (v "0.1.5+1.42.0") (d (list (d (n "cc") (r ">=1.0.24, <2.0.0") (d #t) (k 1)) (d (n "libc") (r ">=0.2.0, <0.3.0") (d #t) (k 0)))) (h "0yac3ii7040lbwl3lagkz78kzlndx0cf3hrpzl7vg2bqyiglamwn") (l "nghttp2")))

(define-public crate-libnghttp2-sys-0.1.6+1.43.0 (c (n "libnghttp2-sys") (v "0.1.6+1.43.0") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0m984syly6s9wx2xmx5n7zkfr5b0zdvmi7pcb66i6zl2m10mbx8a") (l "nghttp2")))

(define-public crate-libnghttp2-sys-0.1.7+1.45.0 (c (n "libnghttp2-sys") (v "0.1.7+1.45.0") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gy9blf4142kjjs46plj41m36zk2zz5p149by1gqvcwml6mjivap") (l "nghttp2")))

(define-public crate-libnghttp2-sys-0.1.8+1.55.1 (c (n "libnghttp2-sys") (v "0.1.8+1.55.1") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0h53v0jg0ihlqy6v7iz7rhrp70hbz9qxp5nfvaswvb9d35n9bbjg") (l "nghttp2")))

(define-public crate-libnghttp2-sys-0.1.9+1.58.0 (c (n "libnghttp2-sys") (v "0.1.9+1.58.0") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0r2ahmvgjm3x9m4vrz1j0baykn5n4n9rd7cvf1z1d3krya58azmm") (l "nghttp2")))

(define-public crate-libnghttp2-sys-0.1.10+1.61.0 (c (n "libnghttp2-sys") (v "0.1.10+1.61.0") (d (list (d (n "cc") (r "^1.0.24") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0df1gm26md0xs1xim9vvh9p3iz04xi42brghfbxf3li745ajb74m") (l "nghttp2")))

