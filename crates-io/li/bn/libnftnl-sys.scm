(define-module (crates-io li bn libnftnl-sys) #:use-module (crates-io))

(define-public crate-libnftnl-sys-1.0.7 (c (n "libnftnl-sys") (v "1.0.7") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "regex") (r "^0.2.2") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "1m90ilcxkmipw50vbq4wm6qdjwf162gskpsh370dnw46jih8bcnm")))

(define-public crate-libnftnl-sys-1.0.8 (c (n "libnftnl-sys") (v "1.0.8") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "regex") (r "^0.2.2") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "0ywbfhgw59xh0pzipfwiahkxc6zaa24mci5ki4mpaka6lsvs4lal")))

(define-public crate-libnftnl-sys-1.0.9 (c (n "libnftnl-sys") (v "1.0.9") (d (list (d (n "bindgen") (r "^0.31.3") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)) (d (n "regex") (r "^0.2.2") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "02iqwbbcrgi5p8v9xsgwsi0fppfr52q1gwk7llsn6c1pcsq0nbvp")))

(define-public crate-libnftnl-sys-1.0.10 (c (n "libnftnl-sys") (v "1.0.10") (d (list (d (n "bindgen") (r "^0.33.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0jwdysf5r1ds657dg02qy2024xcdpfsmhyr3aqrmklwsrcikq3mh")))

(define-public crate-libnftnl-sys-1.1.1 (c (n "libnftnl-sys") (v "1.1.1") (d (list (d (n "bindgen") (r "^0.37") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0v0im7vfqmpghrdrszpqj5kj4yxs63v8p3gzrda7nqrx08pfyv1n")))

(define-public crate-libnftnl-sys-1.1.2 (c (n "libnftnl-sys") (v "1.1.2") (d (list (d (n "bindgen") (r "^0.40") (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rvqa0xh6qhm1xqcwpw5w9wi9r1vw3hhrqabyq8jqfmhaxs71qm0")))

