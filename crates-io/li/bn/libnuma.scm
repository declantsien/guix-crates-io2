(define-module (crates-io li bn libnuma) #:use-module (crates-io))

(define-public crate-libnuma-0.0.4 (c (n "libnuma") (v "0.0.4") (d (list (d (n "bitflags") (r "^0.6") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "errno") (r "^0.1") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "libnuma-sys") (r "^0.0.4") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)))) (h "1xxpxyk9hk3g17iv3q39y96qfzzcrws6hq0dlx4f75fpggwcw4xk")))

