(define-module (crates-io li bn libnuma-sys) #:use-module (crates-io))

(define-public crate-libnuma-sys-0.0.1 (c (n "libnuma-sys") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.11") (k 0)))) (h "0g7q8v9p12040m1dcj82yksjsk7rx064pspz2sy0k1dcmpac753d")))

(define-public crate-libnuma-sys-0.0.3 (c (n "libnuma-sys") (v "0.0.3") (d (list (d (n "bitflags") (r "^0.6") (d #t) (k 0)) (d (n "errno") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.11") (d #t) (k 0)))) (h "05c1kb9y2ysscmgl3sc036h0gay92hqs8bf9lnx0kwpdw7j3fdns")))

(define-public crate-libnuma-sys-0.0.4 (c (n "libnuma-sys") (v "0.0.4") (d (list (d (n "bitflags") (r "^0.6") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)) (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)))) (h "1pjak0cjk4frhc270bg5rgg8w609y134hkvfracnkasrszlh8g6g")))

(define-public crate-libnuma-sys-0.0.6 (c (n "libnuma-sys") (v "0.0.6") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)))) (h "06z71lhgfpdkfcyq0gfy5nhyj475gsxcl89mzbmbb9nbzs7k5226")))

(define-public crate-libnuma-sys-0.0.7 (c (n "libnuma-sys") (v "0.0.7") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)))) (h "0b015364d3xhpk9d97xq8w9w1ir76xydaxpk04ivkpf17yywm1l2")))

(define-public crate-libnuma-sys-0.0.8 (c (n "libnuma-sys") (v "0.0.8") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)))) (h "16q9d4ppm20ndgdihwc2zzjrhcj0g2x0nv9vxfww4gjqlz6mqcf0")))

(define-public crate-libnuma-sys-0.0.9 (c (n "libnuma-sys") (v "0.0.9") (d (list (d (n "libc") (r "^0.2") (d #t) (t "cfg(any(target_os = \"linux\", target_os = \"android\"))") (k 0)))) (h "046bhd1irdp9i9nmzgq22k7bygwbn1x3zdyxxmjk2dry3ay7j42i")))

