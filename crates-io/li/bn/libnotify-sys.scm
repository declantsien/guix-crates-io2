(define-module (crates-io li bn libnotify-sys) #:use-module (crates-io))

(define-public crate-libnotify-sys-0.0.2 (c (n "libnotify-sys") (v "0.0.2") (d (list (d (n "glib-2-0-sys") (r "*") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0rckzi8kp737nsfvc15vg8mb6fq5biw5qdamr30r9drnchmlcgsg")))

(define-public crate-libnotify-sys-0.0.3 (c (n "libnotify-sys") (v "0.0.3") (d (list (d (n "glib-2-0-sys") (r "*") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "*") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "18lsb4i9d7v40rmnwniiqkgws47cf6drg2n9vn1dqhpwmc7g1sbd")))

(define-public crate-libnotify-sys-0.1.0 (c (n "libnotify-sys") (v "0.1.0") (d (list (d (n "glib-2-0-sys") (r "*") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "*") (d #t) (k 0)) (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1z2dsf6bqngnpirwpp421fdvmwjky0x1dn7gjagkjnvchfm1440s")))

(define-public crate-libnotify-sys-0.1.1 (c (n "libnotify-sys") (v "0.1.1") (d (list (d (n "glib-2-0-sys") (r "*") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "*") (d #t) (k 0)) (d (n "gtypes") (r "*") (d #t) (k 0)) (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "14570b8ppsbkr7j5q1dhl91a1b9y26pd8wjqfsffs9chhvcgz0bj")))

(define-public crate-libnotify-sys-0.2.0 (c (n "libnotify-sys") (v "0.2.0") (d (list (d (n "glib-2-0-sys") (r "^0.46.0") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.46.1") (d #t) (k 0)) (d (n "gtypes") (r "^0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.1.5") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0c8786a6gbrx3v754sf3lzs1ki8q27fgkrdcifspr31mxl3bgr49")))

(define-public crate-libnotify-sys-0.3.0 (c (n "libnotify-sys") (v "0.3.0") (d (list (d (n "glib-2-0-sys") (r "^0.46.0") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.46.1") (d #t) (k 0)) (d (n "gtypes") (r "^0.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0fm69cm7fry681nby6hfy3jv2chdbqm69ihfaskgwsm77k83kw99")))

(define-public crate-libnotify-sys-0.4.0 (c (n "libnotify-sys") (v "0.4.0") (d (list (d (n "glib-2-0-sys") (r "^0.46.0") (d #t) (k 0)) (d (n "gobject-2-0-sys") (r "^0.46.1") (d #t) (k 0)) (d (n "gtypes") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "10fciqnbb2lqfggqx165k7z4rhjc9spkk5s1qi4ypwjlcacfm5a8")))

(define-public crate-libnotify-sys-1.0.0 (c (n "libnotify-sys") (v "1.0.0") (d (list (d (n "bitflags") (r "^0.9.0") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "glib-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.0") (d #t) (k 0)) (d (n "pkg-config") (r ">= 0.3.7") (d #t) (k 1)))) (h "0pdhbj67yvm089h0l2a9bbw1x3hih3w5gp17a21i43fjc53rkvz3")))

(define-public crate-libnotify-sys-1.0.1 (c (n "libnotify-sys") (v "1.0.1") (d (list (d (n "bitflags") (r "^0.9.1") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.4.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.30") (d #t) (k 0)) (d (n "pkg-config") (r ">= 0.3.9") (d #t) (k 1)))) (h "16a32pyglfp1fnwicihrsp0zrf72h38xmhcwll7s82f79w8cpi6k")))

(define-public crate-libnotify-sys-1.0.2 (c (n "libnotify-sys") (v "1.0.2") (d (list (d (n "bitflags") (r "^1.0.1") (d #t) (k 0)) (d (n "gdk-pixbuf-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "glib-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.36") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.9") (d #t) (k 1)))) (h "0g1q8w3ivnjl7sxniynly95rlg51gx9f2cdl3q7x2knjnywid9x0")))

