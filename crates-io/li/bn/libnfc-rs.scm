(define-module (crates-io li bn libnfc-rs) #:use-module (crates-io))

(define-public crate-libnfc-rs-0.0.1 (c (n "libnfc-rs") (v "0.0.1") (h "1qyyw45r7rkbkb1jq482whcv7f8npjqlca5hcyiwpp97smkql4vb") (y #t)))

(define-public crate-libnfc-rs-0.0.1-alpha (c (n "libnfc-rs") (v "0.0.1-alpha") (h "0xj817hgk9rbflcv2yiywn2c3f8p4r4ygqxidsjdr7v1mv3hhan1") (y #t)))

