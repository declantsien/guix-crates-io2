(define-module (crates-io li bn libnspire-sys) #:use-module (crates-io))

(define-public crate-libnspire-sys-0.1.0 (c (n "libnspire-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0v6lzkl35kxrymfar45lilhv4jsl0wdj92nbwbgnpf6adhay9yrk") (l "nspire")))

(define-public crate-libnspire-sys-0.1.1 (c (n "libnspire-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "071ivvdrh0l30pvp4fw0qygiz4w15mhn29s462cw8hab3l3wp06i") (l "nspire")))

(define-public crate-libnspire-sys-0.1.2 (c (n "libnspire-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "03bpcfdzihh65zagi96cdd83yl8d3xa7mgpp6l38phvgk3r50hfi") (l "nspire")))

(define-public crate-libnspire-sys-0.2.0 (c (n "libnspire-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "13kf6q52ima6mm3c0gq7iyxy6lwk6x8vnp95pmbjdgliwsv19jf8") (l "nspire")))

(define-public crate-libnspire-sys-0.2.1 (c (n "libnspire-sys") (v "0.2.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1gsk79c10qiivy3rikhnd3b61vhifplcd1iqj3wzl96j5h47597d") (l "nspire")))

(define-public crate-libnspire-sys-0.2.2 (c (n "libnspire-sys") (v "0.2.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1j0vjgfamw0nxj8qzsa3jwcbwgnc80qr1z0qam35izb3fij048fv") (l "nspire")))

(define-public crate-libnspire-sys-0.2.3 (c (n "libnspire-sys") (v "0.2.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1dg9xmffd41smblhqp34gd55lw6ha29gn02f4miz38b9d1dh5zsd") (l "nspire")))

(define-public crate-libnspire-sys-0.2.4 (c (n "libnspire-sys") (v "0.2.4") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1x3vir9ingxw2mrvbh5r4vb5bgbh9p41l51b3r3rp3m3k0dmj5x5") (l "nspire")))

(define-public crate-libnspire-sys-0.2.5 (c (n "libnspire-sys") (v "0.2.5") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "16qwvqc2vjg4r3p7gwrzhfkl0a5nfysb2j1syaqi42rnxhj22blb") (l "nspire")))

(define-public crate-libnspire-sys-0.2.6 (c (n "libnspire-sys") (v "0.2.6") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "084b7yg9s4j58xnrrdz8qfw44rj4xizhkw76hzc6wdpsrm1bf9sv") (l "nspire")))

(define-public crate-libnspire-sys-0.2.7 (c (n "libnspire-sys") (v "0.2.7") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0sadglr8ncrjlxvhz3zik3psw6p3gb1ahb6azcyqh88jflq1vqm4") (l "nspire")))

(define-public crate-libnspire-sys-0.2.8 (c (n "libnspire-sys") (v "0.2.8") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1q72qd4q2vivbrcr472bc2igiz3wkwinih0rbwwnrcqzjsjfpzhc") (l "nspire")))

(define-public crate-libnspire-sys-0.2.9 (c (n "libnspire-sys") (v "0.2.9") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0diln51sr8aqvji8fp8i89gd9wnanmfrafvv8864yjvrzr011bzk") (l "nspire")))

(define-public crate-libnspire-sys-0.3.0 (c (n "libnspire-sys") (v "0.3.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0290mf47ra13460xgpzavqpwlm124pcqbffhp3g0g1592jx3rm36") (l "nspire")))

(define-public crate-libnspire-sys-0.3.1 (c (n "libnspire-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "1q8y9qa33kkclk0dvzas5jxm1nfpybgzx0yjihjs4ircbazc4rni") (l "nspire")))

(define-public crate-libnspire-sys-0.3.2 (c (n "libnspire-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "0qlrjp70lxa8fqdyd2d6gbrmn1646xwn2mrsd0ikybfdpx2b3dh9") (l "nspire")))

(define-public crate-libnspire-sys-0.3.3 (c (n "libnspire-sys") (v "0.3.3") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r "^0.7") (d #t) (k 1)) (d (n "libusb1-sys") (r "^0.4.1") (d #t) (k 0)))) (h "08n7r66lgz9jngc8pv2cph92hmm7hqq4v0d47fjj79qi5pc6j69n") (l "nspire")))

(define-public crate-libnspire-sys-0.3.4 (c (n "libnspire-sys") (v "0.3.4") (d (list (d (n "cc") (r ">=1.0.0, <2.0.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "globwalk") (r ">=0.7.0, <0.8.0") (d #t) (k 1)) (d (n "libusb1-sys") (r ">=0.4.1, <0.5.0") (d #t) (k 0)))) (h "1m535j6zdd9v774cxsfcynmqfvfssvgxi5mwdhjhlrnk47l7nprz") (l "nspire")))

