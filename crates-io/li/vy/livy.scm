(define-module (crates-io li vy livy) #:use-module (crates-io))

(define-public crate-livy-0.0.1 (c (n "livy") (v "0.0.1") (h "04vb07z4q0h17y8w5xqlzjcrnabd2vdmpz8r8f3s1afjicqisbrn")))

(define-public crate-livy-0.0.2 (c (n "livy") (v "0.0.2") (h "0yafwxvs33rl7l0g0zyj2nr7zf4m6zlmp6vg2lyd3rqnqvp38kss")))

(define-public crate-livy-0.0.3 (c (n "livy") (v "0.0.3") (h "04gr6jx57i7gwghrw8j313vramdm2srhfxbn8lvdjs2i2556ja8d")))

(define-public crate-livy-0.0.4 (c (n "livy") (v "0.0.4") (h "0rfimvxr32vmpxhcx1sy16vxmrixmd8xf3b6gz30bqzq2zin5snw")))

(define-public crate-livy-0.0.5 (c (n "livy") (v "0.0.5") (h "0x88lif60wqm9m89bjnjwm14frq2fzf8kldnk0am1as8nlkl4xia")))

(define-public crate-livy-0.0.6 (c (n "livy") (v "0.0.6") (h "1lvc5pxjcpq6mbrxhczisg28z4p7a4ldbd9ibr2184qvi4nqamyi")))

(define-public crate-livy-0.0.7 (c (n "livy") (v "0.0.7") (h "0kywa79hsrkhdrzkxxlk9jy3v2kmvp0qgilcwhkqrhi0mxkfiphp")))

(define-public crate-livy-0.0.8 (c (n "livy") (v "0.0.8") (d (list (d (n "hyper") (r "^0.11") (d #t) (k 0)) (d (n "reqwest") (r "^0.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "14wmr2wmkya4wmrzqwv7v3a5p92lgi2kg122v62xpk5flx66017m")))

(define-public crate-livy-0.0.9 (c (n "livy") (v "0.0.9") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1593nf9n1jw9xr2g5kk59azybrfc0rsz895bliafyyz7hkr4vxm2")))

(define-public crate-livy-0.0.10 (c (n "livy") (v "0.0.10") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1mnv114pbgj5akzfgz2mw049njsf8vqy7x2s1nlxkyr5qhdnsi0h")))

(define-public crate-livy-0.0.11 (c (n "livy") (v "0.0.11") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0naicyhg4qfc88gs38y9azp53ilky0h2yh7ssyrrzkix5nzfa01r")))

(define-public crate-livy-0.0.12 (c (n "livy") (v "0.0.12") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0j744k2gmcisf769zsq3sdvh14fci39szbrgrb799yqlnxaafgrn")))

(define-public crate-livy-0.1.0 (c (n "livy") (v "0.1.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0npfcnacww318j042057qpwssgrrgz7kcqklw41k4wn2fvqf9m5x")))

(define-public crate-livy-0.2.0 (c (n "livy") (v "0.2.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1djajkf4g9pcspcyvvndzxai36q364v4cj591rw08apn2j6mqxz9")))

(define-public crate-livy-0.3.0 (c (n "livy") (v "0.3.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0pmdbnaiic37pc5zqgwhsary3flfxbhw6wlwg4bp7yx568avar6c")))

(define-public crate-livy-0.4.0 (c (n "livy") (v "0.4.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1xiin89aqlmjfq875ns5v1hmfgvcr1y82vcqb4msa5h7ssgyy0pd")))

(define-public crate-livy-0.5.0 (c (n "livy") (v "0.5.0") (d (list (d (n "curl") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0zvagz18l4bdhrbv734fk6djh6173djpj8i8j89ad7bhz3hj4gsp")))

