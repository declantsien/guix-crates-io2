(define-module (crates-io li th lithe) #:use-module (crates-io))

(define-public crate-lithe-0.0.1 (c (n "lithe") (v "0.0.1") (d (list (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1zb8v9caq4dwzsbms6j2crypk15hb7rbn29vzwn2smvhnpq5lzmg")))

(define-public crate-lithe-0.0.2 (c (n "lithe") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0z4dvv12sqh3fx8z9gnp20ask00gcfw5z8bdnppn3b9yxr12q0y5")))

(define-public crate-lithe-0.0.3 (c (n "lithe") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "pest") (r "^2.4.1") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "0d9n1ckgahcac5drvd8mnck2z12gkmd13b4ls334iwbmi17lsr0q")))

