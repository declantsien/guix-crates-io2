(define-module (crates-io li th lithe-cli) #:use-module (crates-io))

(define-public crate-lithe-cli-0.0.1 (c (n "lithe-cli") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "lithe") (r "^0.0.1") (d #t) (k 0)))) (h "1jpk8r8c7ylwxhmjdizazr9yd99gbadvx4z84vmxc76bqh8n9glf")))

(define-public crate-lithe-cli-0.0.2 (c (n "lithe-cli") (v "0.0.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "lithe") (r "^0.0.2") (d #t) (k 0)))) (h "13hi0fdim13dii7sav7iikphxacfd9n38lk9y4x7q20k3xij7sh4")))

(define-public crate-lithe-cli-0.0.3 (c (n "lithe-cli") (v "0.0.3") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lithe") (r "^0.0.3") (d #t) (k 0)) (d (n "predicates") (r "^2.0") (d #t) (k 2)))) (h "06q8yd2ygsmfxy05iil37spskjv84yl37shl7w9g1gs77fwf6xgc")))

