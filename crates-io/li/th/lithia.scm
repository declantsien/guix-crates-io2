(define-module (crates-io li th lithia) #:use-module (crates-io))

(define-public crate-lithia-1.0.0 (c (n "lithia") (v "1.0.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "09sxxk6h57cdznxpj9a5fx0bn1rdbc0pbsdzq2dbfbis4vm4d73w")))

(define-public crate-lithia-1.0.1 (c (n "lithia") (v "1.0.1") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "088g8pkypbs11z5p8azhihgv34fldh1nlp595mmw11c2y5hg6y5b")))

(define-public crate-lithia-1.0.2 (c (n "lithia") (v "1.0.2") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1gpm47nic6w9x1534y9rka49dmn4y637wsdp4sz10vcpgcb4d12x") (f (quote (("system") ("default" "system"))))))

(define-public crate-lithia-1.0.3 (c (n "lithia") (v "1.0.3") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1mzk0nh26v858vc0rvch3znxg0cgp69mx737577gqmdw5qsjap2f") (f (quote (("system") ("default" "system"))))))

(define-public crate-lithia-1.0.4 (c (n "lithia") (v "1.0.4") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1wvicc1k38hapqmw5z3d3fz7rgla0k083kmq3w45rac8yx3q8c9f") (f (quote (("system") ("default" "system"))))))

(define-public crate-lithia-1.0.5 (c (n "lithia") (v "1.0.5") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "058q4lbmakcc58zh15i5hb7kg5qc39vhsw04i4cy2zb78532mig4") (f (quote (("system") ("default" "system"))))))

(define-public crate-lithia-1.0.6 (c (n "lithia") (v "1.0.6") (d (list (d (n "regex") (r "^1.5.6") (d #t) (k 0)))) (h "1b659sbc5q98s8ygny0h0bvm7b3wjq7agarbnibb32654fwmr9i0") (f (quote (("system") ("default" "system"))))))

