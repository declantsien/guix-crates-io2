(define-module (crates-io li tr litrs) #:use-module (crates-io))

(define-public crate-litrs-0.1.0 (c (n "litrs") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)))) (h "05dg3bchkbl105yi7jh87z8p2byqsxck6vvs32yz8azncflv9sf9") (f (quote (("proc-macro") ("default" "proc-macro" "proc-macro2"))))))

(define-public crate-litrs-0.1.1 (c (n "litrs") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)))) (h "0f278c477541d303gj8s12s0wcqg05vn5rsa24l4rhszjz6zm469") (f (quote (("proc-macro") ("default" "proc-macro" "proc-macro2"))))))

(define-public crate-litrs-0.2.0 (c (n "litrs") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)))) (h "041j7lx0f66gdsnpy8rl66gx1x91rgwjjbbc2c6k1ngjdn574mr8") (f (quote (("default" "proc-macro2"))))))

(define-public crate-litrs-0.2.1 (c (n "litrs") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)))) (h "1zgrkn9ksvxpf48d40alvzml4hsmxzvrpfwc704k2hlb1pg3536w") (f (quote (("default" "proc-macro2"))))))

(define-public crate-litrs-0.2.2 (c (n "litrs") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)))) (h "17pjigrys9q29q48z6321djlij1jggl89jbd0b5f22n783hvbyy5") (f (quote (("default" "proc-macro2"))))))

(define-public crate-litrs-0.2.3 (c (n "litrs") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)))) (h "1akrxglqv6dz41jrjr409pjjysd00z5w0949007v52yg6c4mw9zr") (f (quote (("default" "proc-macro2"))))))

(define-public crate-litrs-0.3.0 (c (n "litrs") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)))) (h "1w9n12z7lnyz50sr8bd1mwcp1434wcj7p8wmg3w5sijb7wxd31xl") (f (quote (("default" "proc-macro2")))) (r "1.54")))

(define-public crate-litrs-0.4.0 (c (n "litrs") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "0w2adv1k1142chzqv5065xcjvr85mcywkbfd6wj17h9wixkc65sg") (f (quote (("default" "proc-macro2") ("check_suffix" "unicode-xid")))) (r "1.54")))

(define-public crate-litrs-0.4.1 (c (n "litrs") (v "0.4.1") (d (list (d (n "proc-macro2") (r "^1") (o #t) (d #t) (k 0)) (d (n "unicode-xid") (r "^0.2.4") (o #t) (d #t) (k 0)))) (h "19cssch9gc0x2snd9089nvwzz79zx6nzsi3icffpx25p4hck1kml") (f (quote (("default" "proc-macro2") ("check_suffix" "unicode-xid")))) (r "1.54")))

