(define-module (crates-io li ft liftoff) #:use-module (crates-io))

(define-public crate-liftoff-0.1.0 (c (n "liftoff") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "leg") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "sane") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1g51069850id1l03gjh1asv8kx07gfjpirqixv4dparqh9mg36fy")))

(define-public crate-liftoff-0.1.1 (c (n "liftoff") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "git2") (r "^0.8") (d #t) (k 0)) (d (n "leg") (r "^0.1.3") (d #t) (k 0)) (d (n "regex") (r "^1.1.7") (d #t) (k 0)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 0)) (d (n "sane") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1v307z833rc4p6qgfzr6z8y9pvcw1r4v4kwm9qb5hjnvdrm43mim")))

