(define-module (crates-io li ft liftor) #:use-module (crates-io))

(define-public crate-liftor-0.1.0 (c (n "liftor") (v "0.1.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1j7yyvd3d1j7v0xjx06zg4aqdamx1a21qp7y7g87lzd3fw607zk9")))

(define-public crate-liftor-0.2.0 (c (n "liftor") (v "0.2.0") (d (list (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "0vc0rj01hw8r4qqmf6cqs2qbm54m2cpd026fby73hp6wqcjsmwmx")))

