(define-module (crates-io li ft lift_result) #:use-module (crates-io))

(define-public crate-lift_result-1.0.0 (c (n "lift_result") (v "1.0.0") (h "17schvvhx394zjwigmhl343i2fi854ihgvx9l0zpbndpfqz7gl8v") (y #t)))

(define-public crate-lift_result-1.0.1 (c (n "lift_result") (v "1.0.1") (h "1a8cjnljpl7w1n8mkwh457l8sjac9pzl7b5ky02hbv5gsqjmclmd")))

