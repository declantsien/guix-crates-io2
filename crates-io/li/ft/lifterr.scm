(define-module (crates-io li ft lifterr) #:use-module (crates-io))

(define-public crate-lifterr-0.1.0 (c (n "lifterr") (v "0.1.0") (h "0df3ww3gqmshpjsj4d8zsdqr7sv16mf9sghk1q0x87g8xcnfvw6d")))

(define-public crate-lifterr-0.2.0 (c (n "lifterr") (v "0.2.0") (h "12rxdaadc2b75dflsvwb3pkpgqb7bl4hgm83yi72y6ajxbqw9crm")))

(define-public crate-lifterr-0.3.0 (c (n "lifterr") (v "0.3.0") (h "006p6rf4c28x3rvba3ng1liqr150pwdb779jx0fm8z06rxhl4gmv")))

(define-public crate-lifterr-0.3.1 (c (n "lifterr") (v "0.3.1") (h "04fx7nfhb4j95jvm543fswvcpzcd24k0fcj4bqlz4iv1wqazjm56")))

(define-public crate-lifterr-0.4.0 (c (n "lifterr") (v "0.4.0") (h "1amkmpc6d1abs4wr885bn60v9fwy7zv0qzx1p6qm5kfhbig7yxjg")))

(define-public crate-lifterr-0.4.1 (c (n "lifterr") (v "0.4.1") (h "0lviw3bmhy5pap0xp3pc92dg5pnddncajdk458rx3rh1xh8rylwx")))

