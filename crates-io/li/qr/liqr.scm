(define-module (crates-io li qr liqr) #:use-module (crates-io))

(define-public crate-liqr-0.1.0 (c (n "liqr") (v "0.1.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "1yxiqg87zsbxp2g10zfcyxznc8cm28xmglln6sqlsqmgv7d47lda") (y #t)))

(define-public crate-liqr-0.1.1 (c (n "liqr") (v "0.1.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "02cb8k9wcrnaf84nfbk5rxc5dpykikr38gyrwxg7caa6d9wscsah") (y #t)))

(define-public crate-liqr-0.1.2 (c (n "liqr") (v "0.1.2") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "03hjyjbp1rww9lgnjwgac43zmq8v28c5103habh51zqmkxv6z75l") (y #t)))

(define-public crate-liqr-0.1.3 (c (n "liqr") (v "0.1.3") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "0czggqw2wgj3x00rhmizjz8lzw0q0qnchipwfvrd1qc1if3xqx54")))

(define-public crate-liqr-0.1.4 (c (n "liqr") (v "0.1.4") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "1k21ar7201ql1k58k49qpna6h73kzdr9k5lpgzyivy915mj8d7mn")))

(define-public crate-liqr-0.1.5 (c (n "liqr") (v "0.1.5") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "1j436w9c87ymfpw6ibxd1asfg78h8qki7xcw7hg1k8k01r4kjwfi") (y #t)))

(define-public crate-liqr-0.1.6 (c (n "liqr") (v "0.1.6") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "1kh7gpl2v9d7lyvg1x1r9dn33zfcg2nlvpl7bxkaxyzh3k04bqbl")))

(define-public crate-liqr-0.1.7 (c (n "liqr") (v "0.1.7") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "09k1dpjc35xvqb1v6b92zg8sggs5610qc3hb5dv983rp7ml000sx")))

(define-public crate-liqr-1.0.0 (c (n "liqr") (v "1.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "0j0ljc953hcw8cggnfj6n18a5yximhskz78dr74ff4vb2n9i4wsh")))

(define-public crate-liqr-1.0.1 (c (n "liqr") (v "1.0.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "local-ip-address") (r "^0.4.9") (d #t) (k 0)) (d (n "qr2term") (r "^0.3.1") (d #t) (k 0)))) (h "0s50p2yg590r983rjy84zp5lbhlnysll06zxfqrazlb9lnybjg6s")))

