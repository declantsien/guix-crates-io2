(define-module (crates-io li sp lispify) #:use-module (crates-io))

(define-public crate-lispify-0.0.0 (c (n "lispify") (v "0.0.0") (d (list (d (n "pretty") (r "^0.12.1") (f (quote ("termcolor"))) (d #t) (k 0)))) (h "0mwyzvyvfg27nnyz8zg3hd96jwn4drwzc6c1l6hifwsf41k410lq") (f (quote (("default"))))))

(define-public crate-lispify-0.0.1 (c (n "lispify") (v "0.0.1") (d (list (d (n "pretty") (r "^0.12.1") (f (quote ("termcolor"))) (d #t) (k 0)))) (h "0k2jcy92vbrv1i482f00qyb48m0958akw2p0462l6krigyb3n1zx") (f (quote (("default"))))))

(define-public crate-lispify-0.0.2 (c (n "lispify") (v "0.0.2") (d (list (d (n "pretty-print") (r "^0.1.7") (d #t) (k 0)))) (h "0qi3l8wmynkld9ksxggz3k3l6z1dvxg7w2p25z924snxbqgq96nb") (f (quote (("default"))))))

(define-public crate-lispify-0.0.3 (c (n "lispify") (v "0.0.3") (d (list (d (n "pretty-print") (r "^0.1.7") (d #t) (k 0)))) (h "1fj3vpc233c7ln8haxyb1c0na34nac3qips7zy34vlqva12cxggr") (f (quote (("default"))))))

(define-public crate-lispify-0.1.0 (c (n "lispify") (v "0.1.0") (d (list (d (n "pretty-print") (r "^0.1.9") (d #t) (k 0)))) (h "0r88g26g0x4v2bka618698z15v7v3yky2c6hcix9jqr1a7a0kppg") (f (quote (("default"))))))

