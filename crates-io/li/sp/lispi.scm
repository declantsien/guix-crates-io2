(define-module (crates-io li sp lispi) #:use-module (crates-io))

(define-public crate-lispi-0.1.0 (c (n "lispi") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("derive" "custom-bindings"))) (d #t) (k 0)))) (h "1gqjp4dxa8c6prq38jmsw4523h1swdwyq12m3lqbcypqij8i5wlp")))

(define-public crate-lispi-0.2.0 (c (n "lispi") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("derive" "custom-bindings"))) (d #t) (k 0)))) (h "0c0rwfwh16llsga02ywf1kg4dlnb9qhcgnmw5j3ryfzwsj8f2r79")))

(define-public crate-lispi-0.2.1 (c (n "lispi") (v "0.2.1") (d (list (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("derive" "custom-bindings"))) (d #t) (k 0)))) (h "1i91afga9ymdi4ax1hv50393razzjy450rpiia0zqzgc5r6g1lkv")))

(define-public crate-lispi-0.3.0 (c (n "lispi") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("derive" "custom-bindings"))) (d #t) (k 0)))) (h "1sx62wkx5a3jb43whcccaqk76mnb66mlad115iiwh85pskm4mpls")))

(define-public crate-lispi-0.3.1 (c (n "lispi") (v "0.3.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("derive" "custom-bindings"))) (d #t) (k 0)))) (h "0hpvkxqg2a846rgnv4zazx449wm9q21wf8r663pqi1ns65xlrwwa")))

(define-public crate-lispi-0.3.2 (c (n "lispi") (v "0.3.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("derive" "custom-bindings"))) (d #t) (k 0)))) (h "17l4cjbnnkmkaic01whd8nmymcl01j9d8mxbj6mf4w1mwzc1yxbb")))

(define-public crate-lispi-0.3.2-1 (c (n "lispi") (v "0.3.2-1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("derive" "custom-bindings"))) (d #t) (k 0)))) (h "192fgc32202k3kcg2p6sj01g0v65l0rn9vikvsf5ji31dd3w8pqw") (y #t)))

(define-public crate-lispi-0.3.3 (c (n "lispi") (v "0.3.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "rustyline") (r "^12.0.0") (f (quote ("derive" "custom-bindings"))) (d #t) (k 0)))) (h "0v91960s3911i9wrd76z6h0i98q8la1x5r54r00jp4vzd8w7xgfk")))

