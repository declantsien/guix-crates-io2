(define-module (crates-io li sp lisp-rs) #:use-module (crates-io))

(define-public crate-lisp-rs-0.0.1 (c (n "lisp-rs") (v "0.0.1") (d (list (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)))) (h "0516hcdi3mqhyppwkawxc495nrxmk177x1150nmlz5i90wp243xl")))

(define-public crate-lisp-rs-0.3.0 (c (n "lisp-rs") (v "0.3.0") (d (list (d (n "linefeed") (r "^0.6.0") (d #t) (k 0)))) (h "0g1rcm4l2z5qzqxp2winp09zlha7f1w38v4d8b13yccg482kikal")))

(define-public crate-lisp-rs-0.3.1 (c (n "lisp-rs") (v "0.3.1") (d (list (d (n "linefeed") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0dqcgk1h09n8igha7n3s80m0mldqrlvg05q30zhya6473292wfl5") (f (quote (("build-binary" "linefeed"))))))

(define-public crate-lisp-rs-0.3.2 (c (n "lisp-rs") (v "0.3.2") (d (list (d (n "linefeed") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "0isl75q1l86840kvd5b2hcds1fj9nf5blp96gmajna9ysc0gzp8m") (f (quote (("build-binary" "linefeed"))))))

(define-public crate-lisp-rs-0.3.3 (c (n "lisp-rs") (v "0.3.3") (d (list (d (n "linefeed") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "153bngy8sz66ri3038v911xv0psgknaj6qyvhdps128xf98j19d4") (f (quote (("build-binary" "linefeed"))))))

(define-public crate-lisp-rs-0.3.4 (c (n "lisp-rs") (v "0.3.4") (d (list (d (n "linefeed") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1zgd38p9w4hn68sng7hdiqqqyg844i2vcjqc043h8844592pfxdf") (f (quote (("build-binary" "linefeed"))))))

(define-public crate-lisp-rs-0.3.5 (c (n "lisp-rs") (v "0.3.5") (d (list (d (n "linefeed") (r "^0.6.0") (o #t) (d #t) (k 0)))) (h "1xwl028kp4a9qdkswvwgsmycwb2lk4z00dc3x4xqg5b4bcspz2l0") (f (quote (("build-binary" "linefeed"))))))

