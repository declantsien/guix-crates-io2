(define-module (crates-io li sp lisp_parser) #:use-module (crates-io))

(define-public crate-lisp_parser-0.1.0 (c (n "lisp_parser") (v "0.1.0") (h "0hl0lak9krjkm1qk8gfqv3dgxb28a187m3qn1j5xlbn65q8hv39j")))

(define-public crate-lisp_parser-0.1.1 (c (n "lisp_parser") (v "0.1.1") (h "1mjyw921bswj8ivi5d0ikrd55g1mfl47d4fddp5y6v0qfrcb6g7h")))

(define-public crate-lisp_parser-0.1.2 (c (n "lisp_parser") (v "0.1.2") (h "1m4vdnpg9mbqidrqkv5w8nzfkx0amf9vw1fhrxad8mw1gg9m8yxf")))

(define-public crate-lisp_parser-0.1.3 (c (n "lisp_parser") (v "0.1.3") (h "15s6zn79acmyjm3m0sr5gpqsbw6cra6j1clj8i7kkv53yi3gkymx")))

