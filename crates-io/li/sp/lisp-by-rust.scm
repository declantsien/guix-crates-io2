(define-module (crates-io li sp lisp-by-rust) #:use-module (crates-io))

(define-public crate-lisp-by-rust-0.1.0 (c (n "lisp-by-rust") (v "0.1.0") (d (list (d (n "codespan") (r "^0.3.0") (d #t) (k 0)))) (h "00kwywp9rk01hsxz2c7yzrzz8kj9zkxp49lwgsd3qx1ibxzsvvr6")))

(define-public crate-lisp-by-rust-0.0.9 (c (n "lisp-by-rust") (v "0.0.9") (d (list (d (n "codespan") (r "^0.3.0") (d #t) (k 0)))) (h "1pr0xh2hkgidi6ndqsv58qyccmn09lnpana6p2zfbsxpxzml9a0q")))

