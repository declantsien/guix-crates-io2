(define-module (crates-io li nq linq) #:use-module (crates-io))

(define-public crate-linq-0.0.1-init (c (n "linq") (v "0.0.1-init") (h "07gjzhgxai05wjaabb0jdzhwf2pm2g1d7zsvf60icgg37cwk4nms")))

(define-public crate-linq-0.0.1-preview (c (n "linq") (v "0.0.1-preview") (h "1pg5s47sl0cj2v8xkim647my84c3sqcw6jwdwk7shxjv89f47383")))

(define-public crate-linq-0.0.1-preview2 (c (n "linq") (v "0.0.1-preview2") (h "1xhhql972hnbnf9xpp49jc5d1aww0h84w6rsrkd8n50gxzca5ih2")))

(define-public crate-linq-0.0.1-release (c (n "linq") (v "0.0.1-release") (h "11xk8d0gpw3dqq83pd9m99fna2whsyjwbkdj7qnjplfxd64mgpjd")))

