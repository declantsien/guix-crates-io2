(define-module (crates-io li nc lincoln_ir) #:use-module (crates-io))

(define-public crate-lincoln_ir-0.1.0 (c (n "lincoln_ir") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lincoln_common") (r "^0.1") (d #t) (k 0)) (d (n "lincoln_compiled") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)))) (h "11h6fi3z51sccmplai9arxccfczg694l2f8ksnis3396lm1yjbm5")))

