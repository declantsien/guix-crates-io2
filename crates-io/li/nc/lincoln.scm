(define-module (crates-io li nc lincoln) #:use-module (crates-io))

(define-public crate-lincoln-0.1.0 (c (n "lincoln") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.6") (d #t) (k 0)) (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "failure_derive") (r "^0.1") (d #t) (k 0)) (d (n "lincoln_common") (r "^0.1") (d #t) (k 0)) (d (n "lincoln_compiled") (r "^0.1") (d #t) (k 0)) (d (n "lincoln_ir") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "rustyline") (r "^3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0k9afkl7pidq0z7aib2ykmzhhwjlipss0rvbcb0a564fsx4svqz0")))

