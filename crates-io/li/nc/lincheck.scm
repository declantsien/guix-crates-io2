(define-module (crates-io li nc lincheck) #:use-module (crates-io))

(define-public crate-lincheck-0.1.0 (c (n "lincheck") (v "0.1.0") (d (list (d (n "loom") (r "^0.5") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 0)))) (h "0x92g9bpb9xd9a052ldmrgxfdkp84z867s09sgappx9p6p6wj54q")))

(define-public crate-lincheck-0.1.1 (c (n "lincheck") (v "0.1.1") (d (list (d (n "loom") (r "^0.5") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 0)))) (h "0fcr4km58ialyicv2slfnyyy8sv9c3yngl3q6562ym0yijk3m5kd")))

(define-public crate-lincheck-0.2.0 (c (n "lincheck") (v "0.2.0") (d (list (d (n "loom") (r "^0.6") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 0)))) (h "0dh4a5pfld3vrrp79i7ma49ybsj8arh8pa41h726rmgz49fij8zb")))

(define-public crate-lincheck-0.2.1 (c (n "lincheck") (v "0.2.1") (d (list (d (n "loom") (r "^0.6") (d #t) (k 0)) (d (n "proptest") (r "^1.1") (d #t) (k 0)))) (h "14nbv52kpxrdwwcn4762cfxdjvgpalcg0m2jcq78hv34432k4axj")))

