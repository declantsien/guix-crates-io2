(define-module (crates-io li nc lincolns) #:use-module (crates-io))

(define-public crate-lincolns-0.0.0 (c (n "lincolns") (v "0.0.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0lqy2gqla3xqwrzlrw535ni5ysijp9yggklglnh0byaz0glbp4br")))

(define-public crate-lincolns-0.1.0 (c (n "lincolns") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0ys27ifqvka70l68l6w33ia2mqzy5nh3khd7zpca452jsy4m2hk3")))

(define-public crate-lincolns-0.1.1 (c (n "lincolns") (v "0.1.1") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)) (d (n "yaml-rust") (r "^0.4") (d #t) (k 0)))) (h "0q5z1qg0nm97ss5jrj9b9i1s7r10792ng5x3840jnqq7m4vzb48w")))

