(define-module (crates-io li nc lincoln_compiled) #:use-module (crates-io))

(define-public crate-lincoln_compiled-0.1.0 (c (n "lincoln_compiled") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lincoln_common") (r "^0.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (f (quote ("serde"))) (d #t) (k 0)))) (h "164l1cn13c45dysbv3j3y6r24mix03k4grb2yxvcdczvnv939rrl")))

