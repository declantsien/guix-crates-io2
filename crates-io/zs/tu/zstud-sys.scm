(define-module (crates-io zs tu zstud-sys) #:use-module (crates-io))

(define-public crate-zstud-sys-0.1.0 (c (n "zstud-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.66.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)))) (h "1bbclqrbzn235niggcwd9n96sm8h2qsbida6xwzp2rbhfscfmf81")))

(define-public crate-zstud-sys-0.1.1 (c (n "zstud-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.66.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)))) (h "0knzgyh7q5sxhilsqym09rzp0988hhjlwrnrr3iv466km449qv3k")))

(define-public crate-zstud-sys-0.1.2 (c (n "zstud-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.66.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)))) (h "0dlkna47a88dp9an5y1k4fw4bbjkcfcmawscczb7bjd9a71l81hn")))

(define-public crate-zstud-sys-0.1.3 (c (n "zstud-sys") (v "0.1.3") (d (list (d (n "bindgen") (r "^0.66.0") (d #t) (k 1)) (d (n "cc") (r "^1.0.79") (d #t) (k 1)) (d (n "libc") (r "^0.2.146") (d #t) (k 0)))) (h "0fklhr3rn5qa39h0p2drrkvkzx5g1pbmmpl8p8g7af20ljb82cz0")))

