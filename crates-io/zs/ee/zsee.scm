(define-module (crates-io zs ee zsee) #:use-module (crates-io))

(define-public crate-zsee-0.0.1 (c (n "zsee") (v "0.0.1") (d (list (d (n "bson") (r "^2.9.0") (d #t) (k 0)) (d (n "dioxus") (r "^0.4.3") (d #t) (k 0)) (d (n "gloo") (r "^0.11.0") (d #t) (k 0)) (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)))) (h "1bp5gw6q0dqdbajrz1dp569pz1kv4lckzb27s8p9243q6mdm3k8l")))

(define-public crate-zsee-0.0.2 (c (n "zsee") (v "0.0.2") (d (list (d (n "bson") (r "^2.9.0") (d #t) (k 0)) (d (n "dioxus") (r "^0.4.3") (d #t) (k 0)) (d (n "gloo") (r "^0.11.0") (d #t) (k 0)) (d (n "gloo-timers") (r "^0.3.0") (f (quote ("futures"))) (d #t) (k 0)))) (h "1vxfxscgz00cfps6g8mh3xnb8hrf4my7l2rc80yragaqy0gxhl3w")))

