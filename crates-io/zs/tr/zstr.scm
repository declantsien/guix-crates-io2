(define-module (crates-io zs tr zstr) #:use-module (crates-io))

(define-public crate-zstr-0.1.0 (c (n "zstr") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.69") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "1rnd3mdxfkwk87l8hgc0fyqa9fk3saxchs4g0wxk87n6hg2m4g4d")))

(define-public crate-zstr-0.1.1 (c (n "zstr") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.37") (d #t) (k 0)) (d (n "quote") (r "^1.0.18") (d #t) (k 0)) (d (n "syn") (r "^1.0.91") (f (quote ("proc-macro" "parsing" "printing"))) (k 0)))) (h "0gj5acsdl042zr1sifpdc0h9pz21gypd4mxxv153mjmlaa5z6cdz")))

