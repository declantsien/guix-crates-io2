(define-module (crates-io zs tr zstring) #:use-module (crates-io))

(define-public crate-zstring-0.0.1 (c (n "zstring") (v "0.0.1") (h "01a3lmpqkdya2d99cljbj3j1g0sky38mf9as0j4djg321dbj3fxb") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.0.2 (c (n "zstring") (v "0.0.2") (h "12zv650zda8hgqr82502wvf3wm31cbzcpsxzvbg0d05bzcxz3rq2") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.0.3 (c (n "zstring") (v "0.0.3") (h "027wbya7xvxl291630db805db675c8xkw60l30x6qxnxk3mq31fq") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.1.0 (c (n "zstring") (v "0.1.0") (h "19ay4fkbhjwzgwn2kljizbl3jjd6kpjpccyn39jgjf67ihmalsp0") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.1.1 (c (n "zstring") (v "0.1.1") (h "0pja4w8i8blg2384y33qv1z3ws5q6d2imn38rkai35w6yfc8yaf9") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.1.2 (c (n "zstring") (v "0.1.2") (h "1jcnbg0pf0yjjs4yfwzj6a8d60xy7gbr5sgl3bb19i05a6kiiynk") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.1.3 (c (n "zstring") (v "0.1.3") (h "1aw996d1w99xm1bmnb5jfs789h8ds7dzvvvqg3sr8rjv8qhrsdbk") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.2.0 (c (n "zstring") (v "0.2.0") (d (list (d (n "ptr_iter") (r "^0.1.0") (d #t) (k 0)))) (h "1jfk1qbjh5mjpqzqqwxrxgmrl6rc6lb6swqgcqn2kp4fw87r3mpv") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.2.1 (c (n "zstring") (v "0.2.1") (d (list (d (n "ptr_iter") (r "^0.1.0") (d #t) (k 0)))) (h "1y0spvcmlqw56yag880ldhc51qhcdlyfhahby77rvnk86bsjng4j") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.2.2 (c (n "zstring") (v "0.2.2") (d (list (d (n "ptr_iter") (r "^0.1.0") (d #t) (k 0)) (d (n "bstr") (r "^1") (d #t) (k 2)))) (h "1kn7phd9h1kh9mj9khn0mh360sxgmbawv8dizr0x1hcmjd1c8gyd") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.2.3 (c (n "zstring") (v "0.2.3") (d (list (d (n "ptr_iter") (r "^0.1.0") (d #t) (k 0)) (d (n "bstr") (r "^1") (d #t) (k 2)))) (h "1771lwdic15ixkdmkvirbf1gc3sjrhag5jfpmqcrryp2r3jw9l24") (f (quote (("default") ("alloc"))))))

(define-public crate-zstring-0.2.4 (c (n "zstring") (v "0.2.4") (d (list (d (n "ptr_iter") (r "^0.1.0") (d #t) (k 0)) (d (n "bstr") (r "^1") (d #t) (k 2)))) (h "0s5w4mg357wkdxzk6ff3qq7r97yw11qm52n98ncv5kizg1gywr1d") (f (quote (("default") ("alloc"))))))

