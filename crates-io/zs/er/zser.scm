(define-module (crates-io zs er zser) #:use-module (crates-io))

(define-public crate-zser-0.0.0 (c (n "zser") (v "0.0.0") (h "04zwazszgaz303cf2awn187rqf1vig9rwcnk3gz3bd43kvr0gjxc")))

(define-public crate-zser-0.0.1 (c (n "zser") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5") (k 0)) (d (n "data-encoding") (r "^2.0.0-rc.1") (d #t) (k 2)) (d (n "error-chain") (r "^0.10") (d #t) (k 0)) (d (n "leb128") (r "^0") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1wnlrvyygq431v1wxim95lzqrk2k8ydj2ja29dl86b3229yygwfz") (f (quote (("std") ("default" "std") ("bench"))))))

