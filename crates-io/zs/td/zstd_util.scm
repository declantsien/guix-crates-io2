(define-module (crates-io zs td zstd_util) #:use-module (crates-io))

(define-public crate-zstd_util-0.1.0 (c (n "zstd_util") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "zstd-safe") (r "^3") (d #t) (k 0)))) (h "0jcjbyiglz59czji5ck183cqn0bsj6f759qfmf3na71sp663hrbp")))

(define-public crate-zstd_util-0.1.1 (c (n "zstd_util") (v "0.1.1") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "zstd-safe") (r "^4.1.1") (d #t) (k 0)))) (h "0cc8xyv4yfzf9lya6ybq46lcwknz7x0r342nbq19yf6r54nk3xd4")))

(define-public crate-zstd_util-0.1.2 (c (n "zstd_util") (v "0.1.2") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zstd-safe") (r "^5.0.2") (d #t) (k 0)))) (h "04iyrqyziyx2jmyxjfc77y28gl4xv2n914i9dk2688vrk46syv1i")))

(define-public crate-zstd_util-0.1.3 (c (n "zstd_util") (v "0.1.3") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "zstd-safe") (r "^6.0.2") (d #t) (k 0)))) (h "1q8s6c91whq4na2i0pqa6wmsgzdcl74s62igc3q1vrd1dgff4yqi")))

(define-public crate-zstd_util-0.1.4 (c (n "zstd_util") (v "0.1.4") (d (list (d (n "env_logger") (r "^0.8") (d #t) (k 2)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)) (d (n "zstd-safe") (r "^6.0.6") (d #t) (k 0)))) (h "03g5065k5amyi923sjmfin38al8hhrymqm3xgypv105knh5ng9bn")))

