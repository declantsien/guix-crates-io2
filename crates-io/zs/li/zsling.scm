(define-module (crates-io zs li zsling) #:use-module (crates-io))

(define-public crate-zsling-0.0.1 (c (n "zsling") (v "0.0.1") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "zigc") (r "^0.0.0-alpha0.2") (d #t) (k 1)))) (h "0wq7g1gvnb0r7rdvygnnk3m6q405v8ichi7jgs7y2cjb6i9rdj43")))

(define-public crate-zsling-0.1.0 (c (n "zsling") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "zigc") (r "^0.0.0-alpha0.2") (d #t) (k 1)))) (h "0l8y8cwpxx3zm3a2k8d85d6gsv800bkwzzny8az63yrhay7qw4ff")))

(define-public crate-zsling-0.1.1 (c (n "zsling") (v "0.1.1") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "zigc") (r "^0.0.3") (d #t) (k 1)))) (h "1plk4vqc441cbwjq9ajhn2d8r24xdszipspirwyrvd4004k9xdxb")))

(define-public crate-zsling-0.1.2 (c (n "zsling") (v "0.1.2") (d (list (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "zigc") (r "^0.0.3") (d #t) (k 1)))) (h "133yy5ba6svcy94rfsnx68sy91z00y9kkpx370rqm796gy8zf09s")))

