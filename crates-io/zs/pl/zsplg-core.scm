(define-module (crates-io zs pl zsplg-core) #:use-module (crates-io))

(define-public crate-zsplg-core-0.0.0-alpha1 (c (n "zsplg-core") (v "0.0.0-alpha1") (d (list (d (n "dyn_sized") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0pjy5wjwbp45w310l66amzx5pc7x1hkzijd0pm39yb6kc1zlbzn9") (f (quote (("std") ("default" "std"))))))

(define-public crate-zsplg-core-0.0.0-alpha2 (c (n "zsplg-core") (v "0.0.0-alpha2") (h "0w05wlaxl00f5d55cnvhr4hvj5j20df2hhhqk6wkxswhf52l5cbd")))

