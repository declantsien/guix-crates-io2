(define-module (crates-io zs pl zsplit) #:use-module (crates-io))

(define-public crate-zsplit-0.1.0 (c (n "zsplit") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "18vjclgxcdgv2f8k7z18qnljqbb9fqi6nzqwd4raaks2hrkp43nv")))

(define-public crate-zsplit-0.1.1 (c (n "zsplit") (v "0.1.1") (d (list (d (n "clap") (r "^3.0.0-beta.5") (d #t) (k 0)))) (h "1j1n4l52mksj4rm9246asx75lcqmyd0s7ab2jirzah2jycc933f0")))

(define-public crate-zsplit-0.2.0 (c (n "zsplit") (v "0.2.0") (d (list (d (n "bool_ext") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0-rc.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1b8axxjhxid1lk521lba7sk707kh7wmhs2ra2jqbs7vh833j5n00")))

(define-public crate-zsplit-0.2.1 (c (n "zsplit") (v "0.2.1") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "assert_fs") (r "^1.0") (d #t) (k 2)) (d (n "bool_ext") (r "^0.5") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1") (d #t) (k 0)) (d (n "human-panic") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04c3i7rqvhc850xbm82s53b1zri8ipap2kfkn86hi8cg4absg3w8")))

(define-public crate-zsplit-0.3.0-rc1 (c (n "zsplit") (v "0.3.0-rc1") (h "112wi40897jvrl3110vi8zhf74w1hh8dk0k0jjc4a975k080fi4d") (f (quote (("test_mock") ("default")))) (y #t)))

(define-public crate-zsplit-0.3.0-rc2 (c (n "zsplit") (v "0.3.0-rc2") (h "1nhwzkm2j2ibycjkf6bl06mzc0p2fyr96qh9lsjx9zcjl6f161cy") (f (quote (("test_mock") ("default")))) (y #t)))

(define-public crate-zsplit-0.3.0-rc3 (c (n "zsplit") (v "0.3.0-rc3") (h "1hvgdnycxgs3w74xz4ik9rgpdgcb9k51k0bd5k43ar1x2jmc1bmi") (f (quote (("test_mock") ("default")))) (y #t)))

(define-public crate-zsplit-0.3.0 (c (n "zsplit") (v "0.3.0") (h "1y64rd1shdhz725dwsaylsjgci5w8gzifayjip2dnl329dxfq8wv") (f (quote (("test_mock") ("default"))))))

(define-public crate-zsplit-0.4.0 (c (n "zsplit") (v "0.4.0") (h "0dk83g6c7wc9ca3y9fmq0zdhz2svwbw6z5bj0zjg4min4hgl9sp4") (f (quote (("test_mock") ("default")))) (r "1.64")))

