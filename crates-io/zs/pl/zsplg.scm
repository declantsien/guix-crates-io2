(define-module (crates-io zs pl zsplg) #:use-module (crates-io))

(define-public crate-zsplg-0.0.0-alpha1 (c (n "zsplg") (v "0.0.0-alpha1") (d (list (d (n "libloading") (r "^0.5") (d #t) (k 0)) (d (n "os_str_bytes") (r "^0.1.3") (d #t) (k 0)) (d (n "zsplg-core") (r "^0.0.0-alpha1") (d #t) (k 0)))) (h "0h44c73d45c0c9ychqlh0hmw28xfylc3mr4hsq2k8cjmwip0kcbf")))

(define-public crate-zsplg-0.0.0-alpha2 (c (n "zsplg") (v "0.0.0-alpha2") (d (list (d (n "libloading") (r "^0.5") (o #t) (d #t) (k 0)) (d (n "os_str_bytes") (r "^0.2") (d #t) (k 0)) (d (n "try-block") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "zsplg-core") (r "^0.0.0-alpha2") (d #t) (k 0)))) (h "10cb19lmgcpb5147bvqm3737li2ch4jlsbnis73qizbfjl86liqj") (f (quote (("loader" "libloading" "try-block") ("default" "loader"))))))

