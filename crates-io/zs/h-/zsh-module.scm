(define-module (crates-io zs h- zsh-module) #:use-module (crates-io))

(define-public crate-zsh-module-0.1.0 (c (n "zsh-module") (v "0.1.0") (d (list (d (n "zsh-sys") (r "^0.1") (d #t) (k 0)))) (h "1wxb9rn3hdhcqr6nlki76k8rpqqss8chv5d2siizyc7k2wn62kjg")))

(define-public crate-zsh-module-0.1.1 (c (n "zsh-module") (v "0.1.1") (d (list (d (n "zsh-sys") (r "^0.1") (d #t) (k 0)))) (h "0nk8akc40qd1v8znpm1naqi1wz30ibjl8chrkywgx5721sm69n8g")))

(define-public crate-zsh-module-0.2.0 (c (n "zsh-module") (v "0.2.0") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "zsh-sys") (r "^0.1") (d #t) (k 0)))) (h "1j4iyksjkjcksw8jbysv3j2mrgibgm45xcvnkhwlv7sls093mrrh") (f (quote (("export_module") ("default" "export_module"))))))

(define-public crate-zsh-module-0.2.1 (c (n "zsh-module") (v "0.2.1") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "zsh-sys") (r "^0.1") (d #t) (k 0)))) (h "1j5mqnyd653yaykvsfcf2m92951a201zp821azqr64r18wdjb6w7") (f (quote (("export_module") ("default" "export_module"))))))

(define-public crate-zsh-module-0.2.2 (c (n "zsh-module") (v "0.2.2") (d (list (d (n "downcast-rs") (r "^1.2.0") (d #t) (k 0)) (d (n "zsh-sys") (r "^0.1") (d #t) (k 0)))) (h "131rs4l4vw76prpkkkqcg5qrv498ffd28lbxrfkq4b3a92nqsp44") (f (quote (("export_module") ("default" "export_module"))))))

(define-public crate-zsh-module-0.3.0 (c (n "zsh-module") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "paste") (r "^1.0.11") (d #t) (k 0)) (d (n "zsh-sys") (r "^0.1") (d #t) (k 0)))) (h "1xcqysmyh8hdz8z7h67adygcgz9arbpwpwd8i4c170bh6y4a9n66") (f (quote (("export_module") ("default" "export_module"))))))

