(define-module (crates-io zs pe zspell-index) #:use-module (crates-io))

(define-public crate-zspell-index-0.1.0 (c (n "zspell-index") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde" "v7"))) (d #t) (k 0)))) (h "06qz0acp6mjawdlv2fbq96b29b0vza4kk66y4rwkrymlnc9sa5v9")))

(define-public crate-zspell-index-0.2.0 (c (n "zspell-index") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.79") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "ureq") (r "^2.9.1") (f (quote ("json"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde" "v7"))) (d #t) (k 0)))) (h "0nkl70lfk2j8fi55s4rgwwddagw908f1zfa3pd0cs1qbi5g8gh9j")))

(define-public crate-zspell-index-0.2.1 (c (n "zspell-index") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde" "v7"))) (d #t) (k 0)))) (h "0xqc2x0xhn0j6kswr3bfps4nly4mb7fma73ngk2lp588kc0hbwp2")))

(define-public crate-zspell-index-0.3.0 (c (n "zspell-index") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde" "v7"))) (d #t) (k 0)))) (h "0gcnzg1dfwhsf0qpn4i1l9hq8a4z2zx5n62yhrgl2ns4f79z3h7s")))

(define-public crate-zspell-index-0.4.0 (c (n "zspell-index") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde" "v7"))) (d #t) (k 0)))) (h "0k8asp71h6am6mc720q158hr4zdawfbw7x4ny97nmz360jwcwrdw")))

(define-public crate-zspell-index-0.5.0 (c (n "zspell-index") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "uuid") (r "^1.6.1") (f (quote ("serde" "v7"))) (d #t) (k 0)))) (h "0w6lnsb4s2nzw35r7msbgpj5qbbfdqaxlxc521sjrziyq723vc87")))

