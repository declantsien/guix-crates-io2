(define-module (crates-io wl -t wl-tools) #:use-module (crates-io))

(define-public crate-wl-tools-0.0.0-alpha.1 (c (n "wl-tools") (v "0.0.0-alpha.1") (d (list (d (n "test-case") (r "^2") (k 2)))) (h "0kqn34x0dpll18fwwmm6xz24rcihs1s1cfg8sizkymjlyh9j0cqg") (f (quote (("default"))))))

(define-public crate-wl-tools-0.0.0-alpha.2 (c (n "wl-tools") (v "0.0.0-alpha.2") (d (list (d (n "test-case") (r "^2") (k 2)))) (h "0ls9c551g3yrqxcdqzhm1wpcgg63l781xnq07r96ajabkbdpklzy") (f (quote (("default"))))))

(define-public crate-wl-tools-0.0.0-alpha.3 (c (n "wl-tools") (v "0.0.0-alpha.3") (d (list (d (n "test-case") (r "^2") (k 2)))) (h "1nzgjyz9nji7ij02fcw87absc1fnpzxwkqbaasczpv9ffs8iv1ri") (f (quote (("default"))))))

