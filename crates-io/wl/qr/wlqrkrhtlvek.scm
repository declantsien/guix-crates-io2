(define-module (crates-io wl qr wlqrkrhtlvek) #:use-module (crates-io))

(define-public crate-wlqrkrhtlvek-0.1.0 (c (n "wlqrkrhtlvek") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1h9chi68wf0fy3mxq1jvxvad0ppfdrkh2z9p0zabrphajjc6chva")))

(define-public crate-wlqrkrhtlvek-0.1.1 (c (n "wlqrkrhtlvek") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "004x4niwx87ymcv2wrwkidqp8vmjj7k5p8ijj7lxcnl5lamvp7c8")))

(define-public crate-wlqrkrhtlvek-0.1.2 (c (n "wlqrkrhtlvek") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)))) (h "1hjzgnzc18smgilf0s1yxdv3239r3px5caq14v1hd591val02skn")))

