(define-module (crates-io wl -r wl-realtime-ogd) #:use-module (crates-io))

(define-public crate-wl-realtime-ogd-0.1.0 (c (n "wl-realtime-ogd") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1qjyd96ff9qxqm1w1a4fvsd0wl4p0ql4vf0g9wikc3fz68alvzv0")))

