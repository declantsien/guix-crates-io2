(define-module (crates-io wl in wlinflate) #:use-module (crates-io))

(define-public crate-wlinflate-0.1.0 (c (n "wlinflate") (v "0.1.0") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (k 0)))) (h "0scv6z1jwcvb772jahm1yzdk493p90rhmfpy930fihxag8zjsb1x")))

(define-public crate-wlinflate-0.1.1 (c (n "wlinflate") (v "0.1.1") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (k 0)))) (h "1ap8xw2wznyzk2nf1arp1h48248p4lw7h5kv9pv2ws2ky7jv3l3x")))

(define-public crate-wlinflate-0.1.2 (c (n "wlinflate") (v "0.1.2") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (k 0)))) (h "1nxc7w9cxzdlv06zj6lw2igivl4h2ps3a1ny7zqc0jr3a5ps0a6s")))

(define-public crate-wlinflate-0.1.3 (c (n "wlinflate") (v "0.1.3") (d (list (d (n "ctor") (r "^0.1.21") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (k 0)))) (h "1yksgzagpnmla6qrgyhh9dqf4mpzdxfwjjcsdbml466ay77xf970")))

