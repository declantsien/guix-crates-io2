(define-module (crates-io wl ro wlroots-dehandle) #:use-module (crates-io))

(define-public crate-wlroots-dehandle-1.0.0 (c (n "wlroots-dehandle") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1q8mnvicfxppjyvq2ff5cl79djjyvpiwwb30i617ak8an6ikf4kh")))

(define-public crate-wlroots-dehandle-2.0.0 (c (n "wlroots-dehandle") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^0.4") (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.15") (f (quote ("full" "fold"))) (d #t) (k 0)))) (h "1n5i3rq1511zmsjjdzhvy15d3c898v1syja7ap1nspd095liv2bx")))

