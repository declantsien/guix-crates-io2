(define-module (crates-io wl l- wll-macros) #:use-module (crates-io))

(define-public crate-wll-macros-0.1.0 (c (n "wll-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0w8fmgajv2ndx6fk5hjc9kykkbmk456m8wgmbh2x5fjbxqn5bdfh")))

