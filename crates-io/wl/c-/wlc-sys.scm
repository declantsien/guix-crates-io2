(define-module (crates-io wl c- wlc-sys) #:use-module (crates-io))

(define-public crate-wlc-sys-0.0.7 (c (n "wlc-sys") (v "0.0.7") (d (list (d (n "bindgen") (r "^0.20") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "06fyjqjzk0s6iabp328dn4vrpqj9lhnki2wsr9d00ycnx2rxri9s") (f (quote (("static" "cmake"))))))

(define-public crate-wlc-sys-0.0.8 (c (n "wlc-sys") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1djfhzav45bqx9xiz1z74lc05ym51k7bq5n6l9kzfl34ykxlylqf") (f (quote (("static" "cmake"))))))

