(define-module (crates-io wl c- wlc-sys-with_elogind) #:use-module (crates-io))

(define-public crate-wlc-sys-with_elogind-0.0.8 (c (n "wlc-sys-with_elogind") (v "0.0.8") (d (list (d (n "bindgen") (r "^0.22") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (o #t) (d #t) (k 1)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0c58pjr97smb50a4l92z147m9jii6bbaj6vl19c737vayg0976vk") (f (quote (("static" "cmake"))))))

