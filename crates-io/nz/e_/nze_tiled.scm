(define-module (crates-io nz e_ nze_tiled) #:use-module (crates-io))

(define-public crate-nze_tiled-0.1.0 (c (n "nze_tiled") (v "0.1.0") (d (list (d (n "nze_geometry") (r "^0.1.0") (d #t) (k 0)) (d (n "quick-xml") (r "^0.24.0") (d #t) (k 0)))) (h "0agr7jprbcydqc9ydwxrs5d0cmrhnacj1qwsf4ghrmmh2fx4d2k7")))

