(define-module (crates-io nz e_ nze_game_sdl) #:use-module (crates-io))

(define-public crate-nze_game_sdl-0.1.0 (c (n "nze_game_sdl") (v "0.1.0") (d (list (d (n "nze_geometry") (r "^0.1.0") (d #t) (k 0)) (d (n "nze_tiled") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("ttf" "image" "mixer"))) (d #t) (k 0)))) (h "0mqn89k3a380fm8xzl9n5qk2pkfw01x5wjhplh1z347x6iy7rysx")))

(define-public crate-nze_game_sdl-0.1.1 (c (n "nze_game_sdl") (v "0.1.1") (d (list (d (n "nze_geometry") (r "^0.1.0") (d #t) (k 0)) (d (n "nze_tiled") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("ttf" "image" "mixer"))) (d #t) (k 0)))) (h "048nz0wcv4iyzsqq8r5whfh9jg0cl9apy534zjzvfsp5n670126r")))

(define-public crate-nze_game_sdl-0.1.2 (c (n "nze_game_sdl") (v "0.1.2") (d (list (d (n "nze_geometry") (r "^0.1.0") (d #t) (k 0)) (d (n "nze_tiled") (r "^0.1.0") (d #t) (k 0)) (d (n "sdl2") (r "^0.35") (f (quote ("ttf" "image" "mixer"))) (d #t) (k 0)))) (h "1d3wa7bk5xrpzs9nkq42csjqc4arzrs3s52w8yh4i68kgxfyikpd")))

