(define-module (crates-io nz sc nzsc_single_player_text_interface) #:use-module (crates-io))

(define-public crate-nzsc_single_player_text_interface-0.1.0 (c (n "nzsc_single_player_text_interface") (v "0.1.0") (d (list (d (n "nzsc_single_player") (r "^0.5.0") (d #t) (k 0)))) (h "1brs5ffk2q2s8yfx1wr9k8av13k0n7x7rkkp12p6651qacr6gca8")))

(define-public crate-nzsc_single_player_text_interface-0.2.0 (c (n "nzsc_single_player_text_interface") (v "0.2.0") (d (list (d (n "nzsc_single_player") (r "^0.5.0") (d #t) (k 0)))) (h "0glm7r4hj7ahwp6k77bkbxx5haaqb1saj6pl36b48jayj8aw2lrn")))

(define-public crate-nzsc_single_player_text_interface-0.2.1 (c (n "nzsc_single_player_text_interface") (v "0.2.1") (d (list (d (n "nzsc_single_player") (r "^0.5.0") (d #t) (k 0)))) (h "1hbc7qs74d66w44yn3pmzsf4mxrsyynl83rzpciwn0v5v1drvpy0")))

(define-public crate-nzsc_single_player_text_interface-0.2.2 (c (n "nzsc_single_player_text_interface") (v "0.2.2") (d (list (d (n "nzsc_single_player") (r "^0.5.0") (d #t) (k 0)))) (h "0xgsmiwamg6amryhzkz7h4l9ncvi0asl45mxwwif8zq83fkvn25x")))

