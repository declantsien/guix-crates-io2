(define-module (crates-io nz sc nzsc2p) #:use-module (crates-io))

(define-public crate-nzsc2p-0.1.0 (c (n "nzsc2p") (v "0.1.0") (d (list (d (n "nzsc_core") (r "^0.2.0") (d #t) (k 0)))) (h "0l0bvaacljhzzakqqhnj19a9ajd7m1fdiyr0zy8842gdarlr2lsa")))

(define-public crate-nzsc2p-0.2.0 (c (n "nzsc2p") (v "0.2.0") (d (list (d (n "nzsc_core") (r "^0.2.0") (d #t) (k 0)))) (h "0wwdlyc6bjkp0msqywmfmla6hwka4kwnrjq4ld8jbnpn75gz64kk")))

(define-public crate-nzsc2p-0.3.0 (c (n "nzsc2p") (v "0.3.0") (d (list (d (n "nzsc_core") (r "^0.2.0") (d #t) (k 0)))) (h "13dihx8n9ipdb9ajfb79haivfym2h3njq2aglzb3ivkr9yzl0zgv")))

