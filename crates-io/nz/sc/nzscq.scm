(define-module (crates-io nz sc nzscq) #:use-module (crates-io))

(define-public crate-nzscq-0.1.0 (c (n "nzscq") (v "0.1.0") (h "0yhnaf7jsnp8hid8dvsj9abzbfbw26ydirzaf9lrpr0c51961kwq")))

(define-public crate-nzscq-0.2.0 (c (n "nzscq") (v "0.2.0") (h "1qmk9jfd70x6mbddhh18hmb74pbpanpchmix9jv51zf8ns7fxzhz")))

(define-public crate-nzscq-0.3.0 (c (n "nzscq") (v "0.3.0") (h "08cd1qj5jg6bpd5nlb2sd0xy8jk7wl5l1man85yl134swsbdg074")))

(define-public crate-nzscq-0.4.0 (c (n "nzscq") (v "0.4.0") (h "05sqvi620f0bqcwiasxlkg6lkjn3hi27dc55lh332ch8rnhqmgbn")))

(define-public crate-nzscq-0.5.0 (c (n "nzscq") (v "0.5.0") (h "0aqs012gyh03492yxr9i32qsg2xrjqdfsb8lm7m7y90jjs2dsz7h")))

(define-public crate-nzscq-0.6.0 (c (n "nzscq") (v "0.6.0") (h "09q37ip5vjwxxpyxddi8xnpnz4vn2gk7sfh95ap5s9yrw2pi1qq4")))

(define-public crate-nzscq-0.7.0 (c (n "nzscq") (v "0.7.0") (h "0d3qb9as60wspqcpjqa5a776x22jfvs4bzq0b7xpwzg9v4sqa0dd")))

(define-public crate-nzscq-0.8.0 (c (n "nzscq") (v "0.8.0") (h "1zbncp925s819kal46zskjq9v9xqwg7245higijsbv8yxixwwllj")))

(define-public crate-nzscq-0.9.0 (c (n "nzscq") (v "0.9.0") (h "1rwgfi2pm671zqwqypngj2k0a28njhyk9q7yf289q795xx0c1mc1")))

(define-public crate-nzscq-0.10.0 (c (n "nzscq") (v "0.10.0") (h "0f5kycnk8i1fap8l7msxab88b06ab485mz6d6xz5d73y5jayfsqy")))

