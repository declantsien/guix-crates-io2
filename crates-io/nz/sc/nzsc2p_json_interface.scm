(define-module (crates-io nz sc nzsc2p_json_interface) #:use-module (crates-io))

(define-public crate-nzsc2p_json_interface-0.1.0 (c (n "nzsc2p_json_interface") (v "0.1.0") (d (list (d (n "nzsc2p") (r "^0.2.0") (d #t) (k 0)))) (h "00d9bqc59vh41klslbs9gr17mj4ra7cscmbmnxya8jlzlna8kw6y")))

(define-public crate-nzsc2p_json_interface-0.2.0 (c (n "nzsc2p_json_interface") (v "0.2.0") (d (list (d (n "nzsc2p") (r "^0.2.0") (d #t) (k 0)))) (h "0im3abn8sv17lw3n4w7c8972xw4rjdrzxgdlnpzqimdybi411f37")))

(define-public crate-nzsc2p_json_interface-0.3.0 (c (n "nzsc2p_json_interface") (v "0.3.0") (d (list (d (n "nzsc2p") (r "^0.3.0") (d #t) (k 0)))) (h "1acxm0pgh9lk78y4yg1rrzqd51ixcvw9nrdkf4p776vqcl5w5ibx")))

