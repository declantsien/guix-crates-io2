(define-module (crates-io nz sc nzsc_single_player) #:use-module (crates-io))

(define-public crate-nzsc_single_player-0.1.0 (c (n "nzsc_single_player") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.14") (d #t) (k 0)))) (h "0hc4snbn3nch9xa8kli2mbgbll8lypq4hvvv824299jwfm7cqk1p")))

(define-public crate-nzsc_single_player-0.2.0 (c (n "nzsc_single_player") (v "0.2.0") (h "126gy0wcmw9cjghyr5dk1c56737ws3khd3y2229gng4g45vvpj3j")))

(define-public crate-nzsc_single_player-0.3.0 (c (n "nzsc_single_player") (v "0.3.0") (h "1d65ijbggfmdck67pd2hbiikkd6jq41s1dg4inmf2afsi0qwqxji")))

(define-public crate-nzsc_single_player-0.3.1 (c (n "nzsc_single_player") (v "0.3.1") (h "10fm8y8xqpgirafiw95z30gz04icr6vaxacs4w8chahcqhqxw6vj")))

(define-public crate-nzsc_single_player-0.3.2 (c (n "nzsc_single_player") (v "0.3.2") (h "0qwk7zpig54jm9hqasklkv28ymqjnqaq4wddayv2f5ra7ml5zqns")))

(define-public crate-nzsc_single_player-0.4.0 (c (n "nzsc_single_player") (v "0.4.0") (h "03qa692zaihcsghmn1s8hz2na76iaywxdjshhyr8kq3ahkc81nzv")))

(define-public crate-nzsc_single_player-0.4.1 (c (n "nzsc_single_player") (v "0.4.1") (h "1h5rcb28cr2iyv5989hw2bvc9vsm6cbhngyhh61sbkqicxv1hd4g")))

(define-public crate-nzsc_single_player-0.4.2 (c (n "nzsc_single_player") (v "0.4.2") (h "0s8zy958f3gr033rkpp3nalpvnlxs2pl3d09hwg1898p5fax7hh2")))

(define-public crate-nzsc_single_player-0.4.3 (c (n "nzsc_single_player") (v "0.4.3") (h "11mndhsnhkzbwddk1lbcbww1wqf68jp1hmxx88nigvzaqkpsck9h")))

(define-public crate-nzsc_single_player-0.4.4 (c (n "nzsc_single_player") (v "0.4.4") (h "0qf3pqc19g8vqrp1mrb6rchypayzi0kby68cfm5gp8d1pzsk0kqn")))

(define-public crate-nzsc_single_player-0.5.0 (c (n "nzsc_single_player") (v "0.5.0") (h "0v7242lds9x8s1ypdjxiliy0w0234xsfdm8g2x1wix1ymb8sa4i0")))

(define-public crate-nzsc_single_player-0.5.1 (c (n "nzsc_single_player") (v "0.5.1") (h "137yqswfddvyg1ymgnj8d3005fr3zpqbqwlzsi64fgq2r5hjqdpa")))

