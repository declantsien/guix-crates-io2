(define-module (crates-io nz sc nzsc_core) #:use-module (crates-io))

(define-public crate-nzsc_core-0.1.0 (c (n "nzsc_core") (v "0.1.0") (h "1ksrg3d917plqwgfwkkw637rqz8vpazay8yyhlqg3f4r5pjfflbg")))

(define-public crate-nzsc_core-0.2.0 (c (n "nzsc_core") (v "0.2.0") (h "1ml2rf44dmbzildw7h1j0swg5if2srq4524mdcp79a2s27rl1zsi")))

