(define-module (crates-io m2 sy m2sync) #:use-module (crates-io))

(define-public crate-m2sync-0.11.0 (c (n "m2sync") (v "0.11.0") (d (list (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ctrlc") (r "^3.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.0") (d #t) (k 0)) (d (n "vomit-config") (r "^0.2.0") (d #t) (k 0)) (d (n "vomit-m2sync") (r "^0") (d #t) (k 0)))) (h "0h2db9mzhbs63qf86vhyf784vkijjrnfj0g11h5w6niqcmxnfpmg")))

