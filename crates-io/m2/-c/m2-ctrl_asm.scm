(define-module (crates-io m2 -c m2-ctrl_asm) #:use-module (crates-io))

(define-public crate-m2-ctrl_asm-0.1.0 (c (n "m2-ctrl_asm") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)))) (h "0cmdnscw2zhrq66gwv5x0qn1dh8zfmj7adrzvdw4z07c8gmpxk1g")))

