(define-module (crates-io m2 -c m2-ctrl) #:use-module (crates-io))

(define-public crate-m2-ctrl-0.1.0 (c (n "m2-ctrl") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)))) (h "1k63cxwiddbdw9p43b0qd6n8kp9zjpmmdz6nxsy72p12ic0irw7z")))

(define-public crate-m2-ctrl-1.0.0 (c (n "m2-ctrl") (v "1.0.0") (d (list (d (n "asm") (r "^0.1.0") (o #t) (d #t) (k 0) (p "m2-ctrl_asm")) (d (n "fsm") (r "^0.1.0") (o #t) (d #t) (k 0) (p "m2-ctrl_fsm")))) (h "08kpj9qmjiqqksnhvq31jr3368528m1hxlqarw7ix4yari4yabfa")))

