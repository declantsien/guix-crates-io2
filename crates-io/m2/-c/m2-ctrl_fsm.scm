(define-module (crates-io m2 -c m2-ctrl_fsm) #:use-module (crates-io))

(define-public crate-m2-ctrl_fsm-0.1.0 (c (n "m2-ctrl_fsm") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "paste") (r "^1.0.6") (d #t) (k 0)) (d (n "simulink-binder") (r "^0.1.1") (d #t) (k 0)))) (h "16n086wik0jwp8n669ynpzkg2qz9zqn0q0bpqfl2lykmkm4m63jy")))

