(define-module (crates-io m2 #{4c}# m24c64) #:use-module (crates-io))

(define-public crate-m24c64-0.1.0 (c (n "m24c64") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1sjiiikkikbjdf9f8amx2xsfx2knq4989gfdq8kmsiqn26h4dmgg")))

(define-public crate-m24c64-0.1.1 (c (n "m24c64") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1njc9rqh150pvyv4rm2kbrri0w8h6lnv5s5q54jpsgswar6dgyjc")))

(define-public crate-m24c64-0.1.2 (c (n "m24c64") (v "0.1.2") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "1y74hpbaajj473zz156gkw279268pg6qmdkc6dd2zijnr8a3dhm1")))

(define-public crate-m24c64-0.1.3 (c (n "m24c64") (v "0.1.3") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "0p87dmg5agfvzx5nfrpv3lf6hgx438fn70ij0rbg8ag5ddczfjhl")))

(define-public crate-m24c64-0.1.4 (c (n "m24c64") (v "0.1.4") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "0r9r6p53ydy8d2q7pv7v2v75wkvig6p43qwizxmh8412jd5r0wnk")))

(define-public crate-m24c64-0.1.5 (c (n "m24c64") (v "0.1.5") (d (list (d (n "embedded-hal") (r "^0.2.7") (d #t) (k 0)) (d (n "embedded-hal-mock") (r "^0.8.0") (d #t) (k 2)))) (h "00n8jh8gsnxckqgf7783a5907cxid80r9d5hqqqg1sarc5r1iqi9")))

