(define-module (crates-io m2 di m2dir) #:use-module (crates-io))

(define-public crate-m2dir-0.0.1 (c (n "m2dir") (v "0.0.1") (h "1vj1fbbkf6wzfkan776jqhvyx67wyvqxgzf6l49fs5yb453bvkci") (f (quote (("default"))))))

(define-public crate-m2dir-0.1.0 (c (n "m2dir") (v "0.1.0") (h "0q182n27s7x7kh2wisa69smjyprcl538s0lpm0s6262l2h2wlh22")))

