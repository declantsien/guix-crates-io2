(define-module (crates-io m2 #{7q}# m27q-kvm) #:use-module (crates-io))

(define-public crate-m27q-kvm-0.1.0 (c (n "m27q-kvm") (v "0.1.0") (d (list (d (n "pico-args") (r "^0.4.2") (d #t) (k 0)) (d (n "rusb") (r "^0.8.1") (d #t) (k 0)))) (h "0in2chcbv7kp0xch16y5vh2mlyyg11wjdkn12csav1kg4h05vcfr")))

