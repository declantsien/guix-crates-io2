(define-module (crates-io hp ro hprof) #:use-module (crates-io))

(define-public crate-hprof-0.1.0 (c (n "hprof") (v "0.1.0") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "05dfpdx3sdyg2asfnljx3n1vabs0xi246zhf9vgr7gabgvmvdqs9") (f (quote (("unstable"))))))

(define-public crate-hprof-0.1.1 (c (n "hprof") (v "0.1.1") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0272f7nlk46z75dn5dxljqdhh7alv7020j6ifxy3s2ybjjz6v682") (f (quote (("unstable"))))))

(define-public crate-hprof-0.1.2 (c (n "hprof") (v "0.1.2") (d (list (d (n "clock_ticks") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "02dvp9xcrv7pm9bpxairpk07jv4r6sl9mbyzvml7hz8a50g8cp41") (f (quote (("unstable"))))))

(define-public crate-hprof-0.1.3 (c (n "hprof") (v "0.1.3") (d (list (d (n "clock_ticks") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.3.4") (d #t) (k 0)))) (h "05l5zmfvz2rzbjmqyhskrlg74idwl0w3ay14whvp5whyq1kjxd0p") (f (quote (("unstable"))))))

