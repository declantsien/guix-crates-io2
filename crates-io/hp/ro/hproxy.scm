(define-module (crates-io hp ro hproxy) #:use-module (crates-io))

(define-public crate-hproxy-0.1.0 (c (n "hproxy") (v "0.1.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "http") (r "^0.2.6") (d #t) (k 0)) (d (n "httparse") (r "^1.4") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "01h166dgq4y1q821pqkazwi2497pyl6km64bb6cifx3p1ypmw6cm") (f (quote (("sync") ("default") ("async" "tokio"))))))

(define-public crate-hproxy-0.2.0 (c (n "hproxy") (v "0.2.0") (d (list (d (n "base64") (r "^0.13") (d #t) (k 0)) (d (n "bytes") (r "^1.6.0") (d #t) (k 0)) (d (n "http") (r "^1.1.0") (d #t) (k 0)) (d (n "httparse") (r "^1.8.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("net" "io-util"))) (o #t) (d #t) (k 0)) (d (n "tracing") (r "^0.1") (d #t) (k 0)))) (h "124s4spccsj2nvflhfpdpk4awn2w20wblbqzrv141idpjhlbql35") (f (quote (("sync") ("default") ("async" "tokio"))))))

