(define-module (crates-io hp ro hprose) #:use-module (crates-io))

(define-public crate-hprose-0.1.0 (c (n "hprose") (v "0.1.0") (d (list (d (n "custom_derive") (r "^0.1") (d #t) (k 0)) (d (n "dtoa") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.9") (k 0)) (d (n "num") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "uuid") (r "^0.3") (d #t) (k 0)))) (h "171hwx3hfxi2gjjlb31vs8mv77gpg8dbgyv5zmyc0x1blqlky5qy")))

