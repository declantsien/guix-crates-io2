(define-module (crates-io hp ro hprof-slurp) #:use-module (crates-io))

(define-public crate-hprof-slurp-0.5.2 (c (n "hprof-slurp") (v "0.5.2") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "clap") (r "^4.5.2") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "01r0bb5jrlrvv3nwb7m3wwx3pj8msrks1s25yc3q1dnnl3wllich")))

(define-public crate-hprof-slurp-0.5.3 (c (n "hprof-slurp") (v "0.5.3") (d (list (d (n "ahash") (r "^0.8.11") (d #t) (k 0)) (d (n "clap") (r "^4.5.3") (f (quote ("cargo"))) (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.12") (d #t) (k 0)) (d (n "indicatif") (r "^0.17.8") (d #t) (k 0)) (d (n "indoc") (r "^2.0.4") (d #t) (k 0)) (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0n951bd50yglcjr1r21k6kbwg9gg95nq73rbx23xq37bgiy1v0rv")))

