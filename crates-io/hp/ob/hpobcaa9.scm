(define-module (crates-io hp ob hpobcaa9) #:use-module (crates-io))

(define-public crate-hpobcaa9-0.1.0 (c (n "hpobcaa9") (v "0.1.0") (h "0y12gvwcad9hdfgzd59bdki186l4a9rvh8ll407anxyhfd86waya")))

(define-public crate-hpobcaa9-0.1.1 (c (n "hpobcaa9") (v "0.1.1") (h "17a8s6kg4aj4b6mwjnw7zww0yzh3dmxylxg8xxzcc5cab3bi6lcq")))

(define-public crate-hpobcaa9-0.1.2 (c (n "hpobcaa9") (v "0.1.2") (h "02n24c38fn23dcjscrdk45x4gl2wjb7vx2yc15zljfs363mb3gzh")))

(define-public crate-hpobcaa9-0.1.3 (c (n "hpobcaa9") (v "0.1.3") (h "1rr2264dlbm751lrsn0c0ns4y8zdnb03a82frkm4cg31zj0bbwb8")))

(define-public crate-hpobcaa9-0.1.4 (c (n "hpobcaa9") (v "0.1.4") (h "1wkrcpvcx3swl6293hmwsw6vf3nq4hhd7xr4dmqcvc42731rx9sq")))

(define-public crate-hpobcaa9-0.1.5 (c (n "hpobcaa9") (v "0.1.5") (h "10sxppnn0zpi6za16lns43jd1f7996vmqm9f137z8ws3060l567y")))

(define-public crate-hpobcaa9-0.1.6 (c (n "hpobcaa9") (v "0.1.6") (h "0p98ink4bilz72bcs7jzh8gyanwkn7km099d6mcp2gnhxi5gx2wn")))

(define-public crate-hpobcaa9-0.1.7 (c (n "hpobcaa9") (v "0.1.7") (h "12090g0mqj7bmkbcdzfg2dlwfihbvq5bl177wgd9d496syar744f")))

(define-public crate-hpobcaa9-0.1.8 (c (n "hpobcaa9") (v "0.1.8") (h "0dwyd4gr5bqq1lc4326ig2xk61hga66l3myp18brjnrgxwhdiw28")))

(define-public crate-hpobcaa9-0.1.9 (c (n "hpobcaa9") (v "0.1.9") (h "051q8jz790cbwhvlc9l25a9dv4dgvhccbg9q6xricsmi3k9n7vq3")))

(define-public crate-hpobcaa9-0.1.10 (c (n "hpobcaa9") (v "0.1.10") (h "0yc2f8r7xhs0xvvjx7089whzdd95vchiqyp4zmjsjispca94v8xv")))

