(define-module (crates-io hp #{20}# hp203b) #:use-module (crates-io))

(define-public crate-hp203b-0.1.0-pre.1 (c (n "hp203b") (v "0.1.0-pre.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "17l3bbx5f2jyirx6zn7j99krlf8i37q4lz2nmc47sf5li2ccdpzn")))

(define-public crate-hp203b-0.1.0-pre.2 (c (n "hp203b") (v "0.1.0-pre.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "0dbr66dhjiy53xdxnzgcx9bwawxvygblcgz2di8a8h47h8ai2i4p")))

(define-public crate-hp203b-0.1.0-pre.3 (c (n "hp203b") (v "0.1.0-pre.3") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "0dizp3g0iqpiwd2fd578516w1qp4666nwks4pkw6xaxjnvcgfdjn")))

(define-public crate-hp203b-0.1.0-pre.4 (c (n "hp203b") (v "0.1.0-pre.4") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1yqghwr4lhp78b7ys71ar00a8gfkvi3y7np21py11nxs9kwahi4v")))

(define-public crate-hp203b-0.1.0 (c (n "hp203b") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "0naiq2gqbj6xj9n4kkcr8j1nlfn0wfmzqz3bggrpknw7kpfz448d")))

(define-public crate-hp203b-0.2.0 (c (n "hp203b") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "037cvgdn6v2almvi29kh60y11zargvm36s52yzqh9d7kw3fy6gdx")))

(define-public crate-hp203b-0.3.0-pre.1 (c (n "hp203b") (v "0.3.0-pre.1") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "15prfq3890iznb72b0ixhvy4xbv1rcq0srq5vib6fgrwajilk6pg") (f (quote (("default" "defmt")))) (s 2) (e (quote (("defmt" "dep:defmt"))))))

(define-public crate-hp203b-0.3.0-pre.2 (c (n "hp203b") (v "0.3.0-pre.2") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "fugit") (r "^0.3.6") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "05ssqp0akw8k9q3s11llqnkk79b066vkh9ggqxhf7znxkqs7h5li") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt" "fugit/defmt"))))))

(define-public crate-hp203b-0.3.0 (c (n "hp203b") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytemuck") (r "^1.12.1") (d #t) (k 0)) (d (n "defmt") (r "^0.3.2") (o #t) (d #t) (k 0)) (d (n "embedded-hal") (r "=1.0.0-alpha.8") (d #t) (k 0)) (d (n "fugit") (r "^0.3.6") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.14.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "test-case") (r "^2.2.1") (d #t) (k 2)))) (h "1xh0cl4lcjw76lfzj50r0fp2s7z66bf8pfy0529vc6pjammlbfn6") (f (quote (("default")))) (s 2) (e (quote (("defmt" "dep:defmt" "fugit/defmt"))))))

