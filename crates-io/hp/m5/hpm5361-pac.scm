(define-module (crates-io hp m5 hpm5361-pac) #:use-module (crates-io))

(define-public crate-hpm5361-pac-0.0.1 (c (n "hpm5361-pac") (v "0.0.1") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "03j1fqliw3j4frxyick25y0kgfnbxcb4rfnbdqrg1hjjjsm23zhp") (f (quote (("rt" "critical-section"))))))

(define-public crate-hpm5361-pac-0.0.2 (c (n "hpm5361-pac") (v "0.0.2") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "1ns8b6zlflyp2483x93xb0ml5ia0qrr6f4riqqkmkw0nf6ilbqv1") (f (quote (("rt" "critical-section"))))))

(define-public crate-hpm5361-pac-0.0.3 (c (n "hpm5361-pac") (v "0.0.3") (d (list (d (n "critical-section") (r "^1.1.2") (o #t) (d #t) (k 0)) (d (n "vcell") (r "^0.1.3") (d #t) (k 0)))) (h "0838fr7rczvc8lra28zyg05k07khyql4s8iszg8sqpa8z1mxrlyb") (f (quote (("rt" "critical-section"))))))

