(define-module (crates-io hp kg hpkg) #:use-module (crates-io))

(define-public crate-hpkg-0.0.1 (c (n "hpkg") (v "0.0.1") (h "195bk3dq6z5jxiz95ygharq85wdfjikk2j07r3x529m8ga58bh1v")))

(define-public crate-hpkg-0.0.2 (c (n "hpkg") (v "0.0.2") (h "0bw43p46k640iir3s1i9ld06scy0gppwz8470hmf2zfbsppbqxra")))

