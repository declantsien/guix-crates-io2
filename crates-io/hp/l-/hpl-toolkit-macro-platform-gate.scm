(define-module (crates-io hp l- hpl-toolkit-macro-platform-gate) #:use-module (crates-io))

(define-public crate-hpl-toolkit-macro-platform-gate-0.0.1-beta.1 (c (n "hpl-toolkit-macro-platform-gate") (v "0.0.1-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0zkwkj7m8yxf8v2ac0ykrmyj5mpzwl1ass4lsq09g204k25baw0k")))

(define-public crate-hpl-toolkit-macro-platform-gate-0.0.1 (c (n "hpl-toolkit-macro-platform-gate") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0kcgcmk62ijz2lna3d1mszpwnm0j41aa77nqdsp8x6i6ii4b1q3c")))

