(define-module (crates-io hp l- hpl-compression-macro-merkle-tree-utils) #:use-module (crates-io))

(define-public crate-hpl-compression-macro-merkle-tree-utils-0.0.1 (c (n "hpl-compression-macro-merkle-tree-utils") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1gal3zvqmk1pk1ym6v6vb6kclypxfsfj6ryvk7lf2qi6scbn1pky")))

(define-public crate-hpl-compression-macro-merkle-tree-utils-0.0.2 (c (n "hpl-compression-macro-merkle-tree-utils") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "092h7h11yq6dycn7ij94j2jjm1haq28xqnyvvardd2avf5id73kr")))

(define-public crate-hpl-compression-macro-merkle-tree-utils-0.0.3-beta.1 (c (n "hpl-compression-macro-merkle-tree-utils") (v "0.0.3-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0vp6nbhlxfd088miy1alh12hs1qm9skgg07k75ghyah1bwb930ra")))

(define-public crate-hpl-compression-macro-merkle-tree-utils-0.0.3-beta.2 (c (n "hpl-compression-macro-merkle-tree-utils") (v "0.0.3-beta.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "07ia5n1awvpqf4h9989zf8lxwnj3m3hjr1xidj2x7ab9kmzzimr9")))

(define-public crate-hpl-compression-macro-merkle-tree-utils-0.0.3-beta.3 (c (n "hpl-compression-macro-merkle-tree-utils") (v "0.0.3-beta.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0j0pnidmlrrpswwbcrdzqbmzcnd73aqwl2sx59bnrvwjwmmkj2rr")))

(define-public crate-hpl-compression-macro-merkle-tree-utils-0.0.3-beta.4 (c (n "hpl-compression-macro-merkle-tree-utils") (v "0.0.3-beta.4") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0hnln8ln09b62rgc9hdkqp4dxv6y51a2a719flw74846ha57qcgd")))

