(define-module (crates-io hp l- hpl-toolkit-macro-account-schemas-instruction) #:use-module (crates-io))

(define-public crate-hpl-toolkit-macro-account-schemas-instruction-0.0.1-beta.1 (c (n "hpl-toolkit-macro-account-schemas-instruction") (v "0.0.1-beta.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1l94rn71djhrxjpk0ag8g8b7g2w9agwcbcc36md80b7xa0cxayxs")))

(define-public crate-hpl-toolkit-macro-account-schemas-instruction-0.0.1-beta.2 (c (n "hpl-toolkit-macro-account-schemas-instruction") (v "0.0.1-beta.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1gi5hnszmi4jxvc8ql4r4alxm7fbbjk6fd9jfc775689c189fksw")))

(define-public crate-hpl-toolkit-macro-account-schemas-instruction-0.0.1 (c (n "hpl-toolkit-macro-account-schemas-instruction") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1xdrl96dfjmzr94gz8hs976z9s3nq80qd3gs5dinhkcbxnbz70wj")))

(define-public crate-hpl-toolkit-macro-account-schemas-instruction-0.0.2 (c (n "hpl-toolkit-macro-account-schemas-instruction") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.4.2") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "16w3izkjclighfixmbjlyycjjvmkyvkkyhbkzkvc8hy3lcrdlg7l")))

(define-public crate-hpl-toolkit-macro-account-schemas-instruction-0.0.3-beta.1 (c (n "hpl-toolkit-macro-account-schemas-instruction") (v "0.0.3-beta.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "regex") (r "^1.7.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1n9w0fs25drp4iasi99x5ajx6sd25qv82gshxy0xrbm82b43684k")))

