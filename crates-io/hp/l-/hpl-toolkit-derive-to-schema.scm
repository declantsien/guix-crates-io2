(define-module (crates-io hp l- hpl-toolkit-derive-to-schema) #:use-module (crates-io))

(define-public crate-hpl-toolkit-derive-to-schema-0.0.1-beta.1 (c (n "hpl-toolkit-derive-to-schema") (v "0.0.1-beta.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1sinylrm6l30hq741niqq50pnczw90z04cx7kkd9phb5mzxyif4i")))

(define-public crate-hpl-toolkit-derive-to-schema-0.0.1-beta.2 (c (n "hpl-toolkit-derive-to-schema") (v "0.0.1-beta.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0vcysx38983kgi08lqsh63lpglv4gnmi6ar5b15da8x0dnv82xil")))

(define-public crate-hpl-toolkit-derive-to-schema-0.0.1 (c (n "hpl-toolkit-derive-to-schema") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "09sj3nf12cj9ky5cnc2za7965phvljlr2x14bq98n0rszamij8qb")))

(define-public crate-hpl-toolkit-derive-to-schema-0.0.2 (c (n "hpl-toolkit-derive-to-schema") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "055ynrs930hmbndzd595m6wm8rsv5fw317g4b70wys5ycpaic6hc")))

(define-public crate-hpl-toolkit-derive-to-schema-0.0.3 (c (n "hpl-toolkit-derive-to-schema") (v "0.0.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "14gjkwvzg45pyj7zisa6gdbldhz04xz5jzcm2si3qqn8cw2gjzi3")))

