(define-module (crates-io hp l- hpl-toolkit-derive-to-node) #:use-module (crates-io))

(define-public crate-hpl-toolkit-derive-to-node-0.0.1-beta.1 (c (n "hpl-toolkit-derive-to-node") (v "0.0.1-beta.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0aa712vhjzf3xm5fpjqnlyhkl0yis6vw6rbv365bbky0ldch888g")))

(define-public crate-hpl-toolkit-derive-to-node-0.0.1-beta.2 (c (n "hpl-toolkit-derive-to-node") (v "0.0.1-beta.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1wi7nscn3ff7vr8zhcdsg7hpkmqblkib52ifa7px0hnmb5v08cbv")))

(define-public crate-hpl-toolkit-derive-to-node-0.0.1 (c (n "hpl-toolkit-derive-to-node") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0ndkz3prnq6vnj0vrj6bmzfv7bac3p25z8irxdrg85dly4p5x1jn")))

