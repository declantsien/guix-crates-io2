(define-module (crates-io hp l- hpl-toolkit-attribute-compressed-account) #:use-module (crates-io))

(define-public crate-hpl-toolkit-attribute-compressed-account-0.0.1-beta.1 (c (n "hpl-toolkit-attribute-compressed-account") (v "0.0.1-beta.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "189hd35kc1vsz12c6dsb5an57kgly22k28qddqffqinicq491abc")))

(define-public crate-hpl-toolkit-attribute-compressed-account-0.0.1-beta.2 (c (n "hpl-toolkit-attribute-compressed-account") (v "0.0.1-beta.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1bjja66jrklxn6dykw4d09vbxl8pls8fzn5gxrg1834zayw44vfm")))

(define-public crate-hpl-toolkit-attribute-compressed-account-0.0.1 (c (n "hpl-toolkit-attribute-compressed-account") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0jwlf09g8707s8r386f01lb48bcn3bwmygn5sbwv79nhqy8kii0v")))

