(define-module (crates-io hp l- hpl-toolkit-macro-merkle-tree-utils) #:use-module (crates-io))

(define-public crate-hpl-toolkit-macro-merkle-tree-utils-0.0.1-beta.1 (c (n "hpl-toolkit-macro-merkle-tree-utils") (v "0.0.1-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "09zwaya8g68zgs27qhf09icaibhnhxj9frazni43agpqwf9vik9g")))

(define-public crate-hpl-toolkit-macro-merkle-tree-utils-0.0.1 (c (n "hpl-toolkit-macro-merkle-tree-utils") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0rvw64ad3pangfvhcf5bs73vgn0mpmfpf7bfii3pmazyh54n5dil")))

