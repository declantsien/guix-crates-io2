(define-module (crates-io hp l- hpl-compression-macro-merkle-tree-apply-fn-deep) #:use-module (crates-io))

(define-public crate-hpl-compression-macro-merkle-tree-apply-fn-deep-0.0.1 (c (n "hpl-compression-macro-merkle-tree-apply-fn-deep") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0l02ih9spizi1gjlvi7v2fj2yxk1kgm0rdil04957dw3d2bapgkd")))

(define-public crate-hpl-compression-macro-merkle-tree-apply-fn-deep-0.0.2 (c (n "hpl-compression-macro-merkle-tree-apply-fn-deep") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "078v3mrj2w1mw46fbfcyr3b8nhxv50k4zac0ykavqj1bn5fvzpl5")))

(define-public crate-hpl-compression-macro-merkle-tree-apply-fn-deep-0.0.3-beta.1 (c (n "hpl-compression-macro-merkle-tree-apply-fn-deep") (v "0.0.3-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "13dn8mc4dqhk272nxsgmvqvnnchigvp2vs0h2ws6495gj39bk4iw")))

(define-public crate-hpl-compression-macro-merkle-tree-apply-fn-deep-0.0.3-beta.2 (c (n "hpl-compression-macro-merkle-tree-apply-fn-deep") (v "0.0.3-beta.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "124r94qf3bj8pd34d65db9cw0yahp4dl4mf8fkn1rk37liqgb3qg")))

