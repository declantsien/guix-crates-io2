(define-module (crates-io hp l- hpl-toolkit-macro-merkle-tree-apply-fn) #:use-module (crates-io))

(define-public crate-hpl-toolkit-macro-merkle-tree-apply-fn-0.0.1-beta.1 (c (n "hpl-toolkit-macro-merkle-tree-apply-fn") (v "0.0.1-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "130qqld9xb9ir6nvigw4y259a96hdmhwy05w84iv7a4hr0kwys86")))

(define-public crate-hpl-toolkit-macro-merkle-tree-apply-fn-0.0.1 (c (n "hpl-toolkit-macro-merkle-tree-apply-fn") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "12qn6d39r20sdpnldh322dfw9wi6l0mpf7gl27cgwb89xk89k9qb")))

