(define-module (crates-io hp l- hpl-events) #:use-module (crates-io))

(define-public crate-hpl-events-0.1.10 (c (n "hpl-events") (v "0.1.10") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.10") (d #t) (k 0)))) (h "097q7c8lyj47hny20gcp5dxi8xg7iakihmbr5wj9kym81ijpkpdy") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.11 (c (n "hpl-events") (v "0.1.11") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.11") (d #t) (k 0)))) (h "0ambmiq5rzwyrxp1xmcg02kwyf9w9x0hnxmd6dkslh9bwwx8s0kq") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.12 (c (n "hpl-events") (v "0.1.12") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.12") (d #t) (k 0)))) (h "1xdrqyziss7p8mlfscd152z0y8ap0nj7cn2f14408cm494ffsr3n") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.13 (c (n "hpl-events") (v "0.1.13") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.13") (d #t) (k 0)))) (h "176nyyzc4cx373z91yww0sn4xa5qdnpwnk3nv8kq3b1wyybg44fm") (f (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.14 (c (n "hpl-events") (v "0.1.14") (d (list (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.13") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)))) (h "0mlclqysvypj3mffj5xl0zld4zcylr48fzkqnqyh4j1nphzry89l") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.16 (c (n "hpl-events") (v "0.1.16") (d (list (d (n "anchor-lang") (r "=0.26.0") (d #t) (k 0)) (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.13") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)))) (h "01vak1bwb1y8vz2gww717d21d568jhsfz43fjfhgspwvzbwlzg0d") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.17 (c (n "hpl-events") (v "0.1.17") (d (list (d (n "anchor-lang") (r "=0.26.0") (d #t) (k 0)) (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.17") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)))) (h "1czp87sjqg8v61kp1nischpcyrh9akgv3qc99244ybl0i2ld89ax") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.18 (c (n "hpl-events") (v "0.1.18") (d (list (d (n "anchor-lang") (r "=0.26.0") (d #t) (k 0)) (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.18") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)))) (h "1j7l58rn1rc0kir6asp6ds33npqy2zn6x5afk4g3ws21v4mzf361") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.19 (c (n "hpl-events") (v "0.1.19") (d (list (d (n "anchor-lang") (r "=0.26.0") (d #t) (k 0)) (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.19") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)))) (h "1yrcg5sv17qq04qgrl0qcfmrzysp15yipgv2hdj5gi747mnw8jls") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.20 (c (n "hpl-events") (v "0.1.20") (d (list (d (n "anchor-lang") (r "=0.26.0") (d #t) (k 0)) (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.20") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)))) (h "13l3ng0w4d2r6b3igvk3vxjmyjps9fvvzjd6s4px21bwf61v3yx8") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.21 (c (n "hpl-events") (v "0.1.21") (d (list (d (n "anchor-lang") (r "=0.26.0") (d #t) (k 0)) (d (n "borsh") (r "=0.9.3") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.20") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)))) (h "04pldyys7rz3a3lh31d8bx2wbg9mzfkd60vgcg2kii0vc6cvl1f2") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.22 (c (n "hpl-events") (v "0.1.22") (d (list (d (n "anchor-lang") (r ">=0.28.0") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.21") (d #t) (k 0)) (d (n "spl-account-compression") (r ">=0.1.8") (f (quote ("cpi"))) (d #t) (k 0)))) (h "1r9lwh75d9lgncnq1xv6c3hw4vqzn8x2clwifnx1aznzbxhrkain") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.23 (c (n "hpl-events") (v "0.1.23") (d (list (d (n "anchor-lang") (r ">=0.28.0") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.23") (d #t) (k 0)))) (h "0q2nwqd02vyfjlir5abbzsz3wcw2hjdvsqw1c758a16k95p2xahy") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.24 (c (n "hpl-events") (v "0.1.24") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.23") (d #t) (k 0)))) (h "0s4n6f0z61r21x6rl4g55m40hxfximka4ri8rsp2v15w945d9sf7") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hpl-events-0.1.25 (c (n "hpl-events") (v "0.1.25") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "hpl-attribute-event") (r "^0.1.23") (d #t) (k 0)))) (h "1vb0a3dqc65v3sm17xsl2rg2jn36vg7kr764hm033iqcffz04nnn") (f (quote (("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

