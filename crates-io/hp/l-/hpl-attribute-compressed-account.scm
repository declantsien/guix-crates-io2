(define-module (crates-io hp l- hpl-attribute-compressed-account) #:use-module (crates-io))

(define-public crate-hpl-attribute-compressed-account-0.0.1-beta.1 (c (n "hpl-attribute-compressed-account") (v "0.0.1-beta.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1a52f7w7lzb7yd4x3gbqbfbm4mld23w755zpd26imh8i72cnjwnf")))

(define-public crate-hpl-attribute-compressed-account-0.0.1-beta.2 (c (n "hpl-attribute-compressed-account") (v "0.0.1-beta.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1qsx7sc8a9wl8fgrxhrddvrydj9y2xj356ixkljs7pnyyx24y0mz")))

(define-public crate-hpl-attribute-compressed-account-0.0.1-beta.3 (c (n "hpl-attribute-compressed-account") (v "0.0.1-beta.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1kwz2ngzmbgiz7yh7hsw4pqjfzqnyq4hjffpprrwd138kpfj2sr3")))

