(define-module (crates-io hp l- hpl-compression-macro-merkle-tree-apply-fn) #:use-module (crates-io))

(define-public crate-hpl-compression-macro-merkle-tree-apply-fn-0.0.1 (c (n "hpl-compression-macro-merkle-tree-apply-fn") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0ywpwi2n1pn3llmy8v2j577k54yzb1b26nsf7h680cf3g4741nh9")))

(define-public crate-hpl-compression-macro-merkle-tree-apply-fn-0.0.2 (c (n "hpl-compression-macro-merkle-tree-apply-fn") (v "0.0.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0piqx5r5ji7pdxidiyf9dwxbigg24lslfcxwiv5d5xp7yykbwkzs")))

(define-public crate-hpl-compression-macro-merkle-tree-apply-fn-0.0.3-beta.1 (c (n "hpl-compression-macro-merkle-tree-apply-fn") (v "0.0.3-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1sdkjhdm0qd9bk1a7z94qinvrpk04wf7dpkcxdvk8afsavjsczrg")))

(define-public crate-hpl-compression-macro-merkle-tree-apply-fn-0.0.3-beta.2 (c (n "hpl-compression-macro-merkle-tree-apply-fn") (v "0.0.3-beta.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0kyf0pm240kmv6l36ik2ma1k776dw904gd57qgd5aj22f2l2m3m6")))

