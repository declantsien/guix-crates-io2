(define-module (crates-io hp l- hpl-utils) #:use-module (crates-io))

(define-public crate-hpl-utils-0.1.0 (c (n "hpl-utils") (v "0.1.0") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.8.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "07i38asssbybh5w98k6bhq8gl30v5sswipl8adjc9j9dnyd44lkb")))

(define-public crate-hpl-utils-0.1.1 (c (n "hpl-utils") (v "0.1.1") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.8.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0zggxc1ic56k3np36qy27dlkjly79hk8czcsx48lay518yk4sj00")))

(define-public crate-hpl-utils-0.1.2 (c (n "hpl-utils") (v "0.1.2") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.9.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1y52c67wijmzfynsphklz83gy7qm7w7i03pad8lzinf24x47ygjx")))

(define-public crate-hpl-utils-0.1.3 (c (n "hpl-utils") (v "0.1.3") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.9.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1xgygzffpnd5nw2z4x5bir4yd016n4yaa2x2vl24cx333pf015p9")))

(define-public crate-hpl-utils-0.1.4 (c (n "hpl-utils") (v "0.1.4") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.9.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "1zf9dsgbzjgz419r0pavn9dpf5ncvmgr4d8xj5sl7ci7i76hikr4")))

(define-public crate-hpl-utils-0.1.5 (c (n "hpl-utils") (v "0.1.5") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.9.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0aari0syh9ip8pi7czvgm4ipx0lsskj1229wzrq8b1c1pbplm58z")))

(define-public crate-hpl-utils-0.1.6 (c (n "hpl-utils") (v "0.1.6") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.9.0") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0zir6wd25dcizq0m0i3pqbx8jyprzqvjdlsgn1kjvd2raja19iqc")))

(define-public crate-hpl-utils-0.1.7 (c (n "hpl-utils") (v "0.1.7") (d (list (d (n "anchor-lang") (r "^0.26.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.26.0") (d #t) (k 0)) (d (n "getrandom") (r "^0.2.9") (f (quote ("custom"))) (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.12.0") (d #t) (k 0)) (d (n "solana-program") (r "=1.14.17") (d #t) (k 0)) (d (n "toml_datetime") (r "=0.6.1") (d #t) (k 0)) (d (n "winnow") (r "=0.4.1") (d #t) (k 0)))) (h "0z7yih7sdz1vliz5m3775lxzjqakmipwra7pp876994wkxqw0l3q")))

(define-public crate-hpl-utils-0.1.8 (c (n "hpl-utils") (v "0.1.8") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^2.0.0-beta.1") (d #t) (k 0)))) (h "0j8by30z86nbmy5wmk2sqwwzdvybpgmyn6gw0q5kcrqq2rds5vz0")))

(define-public crate-hpl-utils-0.2.0-beta.2 (c (n "hpl-utils") (v "0.2.0-beta.2") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "^1.13.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0w9khm3xyhdab6a1ln27q1gn7hc2zbmkhzibcsdgswzr1vdivla1")))

(define-public crate-hpl-utils-0.1.9 (c (n "hpl-utils") (v "0.1.9") (d (list (d (n "anchor-lang") (r ">=0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r ">=0.28.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "=1.13.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0jjy28svj6a9kjcr6z8jcps38m2j3ijipvslnq8jrwbd08h6lhxi")))

(define-public crate-hpl-utils-0.1.10 (c (n "hpl-utils") (v "0.1.10") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "=1.13.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "13lmggi6kka23lmrxrhy1q6jdycs8xzg1r3pqngpqv2yh27wpxby")))

(define-public crate-hpl-utils-0.1.11 (c (n "hpl-utils") (v "0.1.11") (d (list (d (n "anchor-lang") (r "^0.28.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.28.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "=1.13.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "0yaz5cnlcjjd3b4i13407hp84vhkgl2ql82hdapbh49rj5b8pg37")))

(define-public crate-hpl-utils-0.1.12 (c (n "hpl-utils") (v "0.1.12") (d (list (d (n "anchor-lang") (r "^0.29.0") (d #t) (k 0)) (d (n "anchor-spl") (r "^0.29.0") (d #t) (k 0)) (d (n "mpl-token-metadata") (r "=1.13.2") (f (quote ("no-entrypoint"))) (d #t) (k 0)))) (h "14xbzqbxanacri3k9y3anvycf25yl0xjw6p6n98xgrr704lhj5f8")))

