(define-module (crates-io hp l- hpl-macro-add-service) #:use-module (crates-io))

(define-public crate-hpl-macro-add-service-0.0.1 (c (n "hpl-macro-add-service") (v "0.0.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "17j3zq9w961qxq6j4dqh8vqa0szwfiji4jzpynrhc0vlhyk3gwy6")))

(define-public crate-hpl-macro-add-service-0.0.2 (c (n "hpl-macro-add-service") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1cmfas6frha2x91lqx2jfwzv7ylykacblls82hp04g2rjz6ka6c7")))

