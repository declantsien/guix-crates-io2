(define-module (crates-io hp l- hpl-attribute-event) #:use-module (crates-io))

(define-public crate-hpl-attribute-event-0.1.0 (c (n "hpl-attribute-event") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1yn6bxa72a05i2ij6kj2wlfx0zhc0w296d1c9kirhpv2l8pfhpdr")))

(define-public crate-hpl-attribute-event-0.1.1 (c (n "hpl-attribute-event") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "07w7x0f5qrpic5lm04h7wllq3cgyp59h6lasfv7n65habh8pm27i")))

(define-public crate-hpl-attribute-event-0.1.2 (c (n "hpl-attribute-event") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0bqwlc7ch22vp1fca6v95hlqsj9jsvmbkgwg1gmg2v049h770yq3")))

(define-public crate-hpl-attribute-event-0.1.3 (c (n "hpl-attribute-event") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0v4p2g8qk2xgmpkrzk1sxdxgbsah1mkg62jniaxxaszwgy37lcvk")))

(define-public crate-hpl-attribute-event-0.1.6 (c (n "hpl-attribute-event") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0w28sxwwyxdqlgbgwd6x0f0lvc42symfz78wvlzr43g14r2aqv36")))

(define-public crate-hpl-attribute-event-0.1.10 (c (n "hpl-attribute-event") (v "0.1.10") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1bjsdv2qs4zqpr2ipziqqcmr6ddk9kcvanzj677bb5in6xwzcn60")))

(define-public crate-hpl-attribute-event-0.1.11 (c (n "hpl-attribute-event") (v "0.1.11") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0ic3s5w5q0x0zibqnbriwv3n6kyznfidrgflsss87i8ca61nka0v")))

(define-public crate-hpl-attribute-event-0.1.12 (c (n "hpl-attribute-event") (v "0.1.12") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1qcy9xwik256v529rlz61rlfp70xxfwfg94fds8cw8wmashnyv5z")))

(define-public crate-hpl-attribute-event-0.1.13 (c (n "hpl-attribute-event") (v "0.1.13") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "167smz381xw5wylkfwrp61npfjj3d4im90cf64yfwn630c92vlm5")))

(define-public crate-hpl-attribute-event-0.1.16 (c (n "hpl-attribute-event") (v "0.1.16") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "181wcqyrrj03065g9zc4gjjsyvfhp3bj2vimh2rxrgfq3d0g4v6k")))

(define-public crate-hpl-attribute-event-0.1.17 (c (n "hpl-attribute-event") (v "0.1.17") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "16by2yib362z9xvd77p7idbj0i4cnvf5g0ladbhi9gzrizlqv0vg")))

(define-public crate-hpl-attribute-event-0.1.18 (c (n "hpl-attribute-event") (v "0.1.18") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1lii2zj2gg0dqqa0f9l1knsh2xjdsghpc99b8akai347c9hpd4fs")))

(define-public crate-hpl-attribute-event-0.1.19 (c (n "hpl-attribute-event") (v "0.1.19") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0241dyz503azq3mx79wnmj0dj9zs0cn3x48pbrx5k6awqlij95sb")))

(define-public crate-hpl-attribute-event-0.1.20 (c (n "hpl-attribute-event") (v "0.1.20") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "0nzvd6zi7pryz2sc7r3aspxs04j21y81ylx3v7kazvqjnyxfg991")))

(define-public crate-hpl-attribute-event-0.1.21 (c (n "hpl-attribute-event") (v "0.1.21") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1dqy8a50a3afikhj89fg2xmcbg1jgk4ns98y4qs4gd6xmrk2d4cv")))

(define-public crate-hpl-attribute-event-0.1.23 (c (n "hpl-attribute-event") (v "0.1.23") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "11jafbf832yhib88zs6h0mw19swqd1qgf5raq44bbp7fbia0nbl7")))

