(define-module (crates-io hp l- hpl-toolkit-macro-merkle-tree-apply-fn-deep) #:use-module (crates-io))

(define-public crate-hpl-toolkit-macro-merkle-tree-apply-fn-deep-0.0.1-beta.1 (c (n "hpl-toolkit-macro-merkle-tree-apply-fn-deep") (v "0.0.1-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1pw4gznz7366ilfgsm8rch9vz3iv4y4x2n1d5vhzb0g6hzs8205d")))

(define-public crate-hpl-toolkit-macro-merkle-tree-apply-fn-deep-0.0.1 (c (n "hpl-toolkit-macro-merkle-tree-apply-fn-deep") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1cgq1jss57a8q3dyw1ms2vjqz62m8m5na3yjffpilr3fg1w2gj1m")))

