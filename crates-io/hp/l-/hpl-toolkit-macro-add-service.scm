(define-module (crates-io hp l- hpl-toolkit-macro-add-service) #:use-module (crates-io))

(define-public crate-hpl-toolkit-macro-add-service-0.0.1-beta.1 (c (n "hpl-toolkit-macro-add-service") (v "0.0.1-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1anm3fg9h7v4naf4ryp111pkfpdh65q8xkgi7a056aadbdj4xvz3")))

(define-public crate-hpl-toolkit-macro-add-service-0.0.1 (c (n "hpl-toolkit-macro-add-service") (v "0.0.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1qi5xgixqpjz2d1frml1wsxxqqaf384wpbbjrqibvnh8ar3b1fc6")))

(define-public crate-hpl-toolkit-macro-add-service-0.0.2-beta.1 (c (n "hpl-toolkit-macro-add-service") (v "0.0.2-beta.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "14n1qsgrj4n161w6ys408h4k27qwlsw9i3hybjfwgpzd6jgs7m0b")))

