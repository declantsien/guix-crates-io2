(define-module (crates-io hp l- hpl-macro-platform-gate) #:use-module (crates-io))

(define-public crate-hpl-macro-platform-gate-0.0.2 (c (n "hpl-macro-platform-gate") (v "0.0.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.28") (f (quote ("full"))) (d #t) (k 0)))) (h "1l456kzyvclnhh2f1ailipi11vd09pmpl4sz1x59x12mzz1g77wh")))

