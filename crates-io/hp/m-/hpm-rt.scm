(define-module (crates-io hp m- hpm-rt) #:use-module (crates-io))

(define-public crate-hpm-rt-0.1.0 (c (n "hpm-rt") (v "0.1.0") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "panic-halt") (r "^0.2.0") (d #t) (k 2)) (d (n "r0") (r "^1.0.0") (d #t) (k 0)) (d (n "riscv") (r "^0.10") (d #t) (k 0)) (d (n "riscv-rt-macros") (r "^0.2.0") (d #t) (k 0)))) (h "14i0qmka2ic65pqbyk9hg4ak8xj7gx2dvfsqfyxck7d5fsdxlphr") (r "1.59")))

