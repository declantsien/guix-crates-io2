(define-module (crates-io hp go hpgo) #:use-module (crates-io))

(define-public crate-HPGO-0.9.2 (c (n "HPGO") (v "0.9.2") (d (list (d (n "ansi_term") (r "^0.12") (d #t) (k 0)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.7.8") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "prettytable-rs") (r "^0.8") (d #t) (k 0)) (d (n "pyo3") (r "^0.8.4") (d #t) (k 0)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0rgg6gaxw8xxnxzis1xl60wbnrppfx24jzqyxgc4dc7jxf7fayc4") (f (quote (("extension-module" "pyo3/extension-module") ("default" "extension-module"))))))

