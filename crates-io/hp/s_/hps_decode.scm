(define-module (crates-io hp s_ hps_decode) #:use-module (crates-io))

(define-public crate-hps_decode-0.1.0 (c (n "hps_decode") (v "0.1.0") (d (list (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "193ranx0pf3x58mcnbfgrn66nskmk7rbnw5fmsh30sj3xni707yr")))

(define-public crate-hps_decode-0.1.1 (c (n "hps_decode") (v "0.1.1") (d (list (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0ivjbj7axir2bcwssbmfdybxvcqs5syrw9vmk3kax8qav15my9is")))

(define-public crate-hps_decode-0.2.0 (c (n "hps_decode") (v "0.2.0") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1v16svy97ld22f7mhqx8ivxysijdlkrx1m0c558sqcczjz50s1b4") (s 2) (e (quote (("rodio-source" "dep:rodio"))))))

(define-public crate-hps_decode-0.2.1 (c (n "hps_decode") (v "0.2.1") (d (list (d (n "nom") (r "^7.1.3") (d #t) (k 0)) (d (n "rayon") (r "^1.7.0") (d #t) (k 0)) (d (n "rodio") (r "^0.17.1") (o #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1wa3k4skihk2kq0n6z2402j7p2rlgkqsyhbrj9c46ccmj3i4vr9v") (s 2) (e (quote (("rodio-source" "dep:rodio"))))))

