(define-module (crates-io hp ts hpts) #:use-module (crates-io))

(define-public crate-hpts-0.1.0 (c (n "hpts") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1faj6f59nda5njqbpkck9566hfy0j5hp2jl6iznpzrnfcwj90f4d")))

(define-public crate-hpts-0.1.1 (c (n "hpts") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0q17gnljcjrm5ip3qv1h3m2plqjsw3c15b20ha0wxbm1qj8m44la")))

(define-public crate-hpts-0.1.2 (c (n "hpts") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "1vv6cp5kf8by2y20pppz522525g8l5ar93mn2rz1livsln72k7yx")))

(define-public crate-hpts-0.1.3 (c (n "hpts") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "17910jyfwjf8qaap9nkjch6wcx12ri6xfcgbia2fvnkack39i171")))

(define-public crate-hpts-0.1.4 (c (n "hpts") (v "0.1.4") (d (list (d (n "clap") (r "^2.33.1") (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "futures") (r "^0.3.0") (d #t) (k 0)) (d (n "httparse") (r "^1.3.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0iv6239849rjv827phgpya1zz4s1xspvnjv3d4gkja7gkcby2bz5")))

