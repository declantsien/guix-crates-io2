(define-module (crates-io hp n- hpn-client) #:use-module (crates-io))

(define-public crate-hpn-client-0.1.0 (c (n "hpn-client") (v "0.1.0") (d (list (d (n "hpn") (r "^0.6.1") (d #t) (k 0)))) (h "1zbh0j3jrvcmyq82a0hqfhanc5lqnp62w013xc3mw19jndxg9y3l")))

(define-public crate-hpn-client-0.1.1 (c (n "hpn-client") (v "0.1.1") (d (list (d (n "hpn") (r "^0.6.1") (d #t) (k 0)))) (h "1cxjfqmfhr9z5iskgc09v3kjpqbj7ixzkfir7ir82rb2kcdrhb1f")))

(define-public crate-hpn-client-0.1.2 (c (n "hpn-client") (v "0.1.2") (h "1vdmavznlgfhh9jrbk9xmr59jlw678ypixgz4zfv32fw2mhz6vfq")))

(define-public crate-hpn-client-0.1.3 (c (n "hpn-client") (v "0.1.3") (h "1kn2kl66s8jfffmz9mpl60k13ngcp0g01fq5mzh16h3zhvxaxnb0")))

