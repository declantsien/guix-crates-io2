(define-module (crates-io hp ac hpack) #:use-module (crates-io))

(define-public crate-hpack-0.0.1 (c (n "hpack") (v "0.0.1") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1p0qf62llk8lb6j1vpn27pxgyiyv8s3wsnbmarl1c4jdapnigyrb") (f (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-0.0.2 (c (n "hpack") (v "0.0.2") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0n84hzw2klksrzi1r7qxcw695ipsppq9s9y754kxdqzmv7l041kn") (f (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-0.0.3 (c (n "hpack") (v "0.0.3") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1bn7xskgi2zgn37ypfqdqd0gvd62lz30j65485ika6c5najp1s93") (f (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-0.1.0 (c (n "hpack") (v "0.1.0") (d (list (d (n "log") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0afxg4fg0mdz4x6lklhc97v8qa806kwh6rsw93bzxqhlrg66i6cn") (f (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-0.2.0 (c (n "hpack") (v "0.2.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "0n1v6icq4x2jh0fyf6p43ljrmzpsxaw120bhkmnl1xjclg9sfb9x") (f (quote (("interop_tests" "rustc-serialize"))))))

(define-public crate-hpack-0.3.0 (c (n "hpack") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1njdhpcax2k20vwrd7pnmrgar4cd4nskbc07s9yq28xda09zcs0w") (f (quote (("interop_tests" "rustc-serialize"))))))

