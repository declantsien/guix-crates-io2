(define-module (crates-io hp ac hpack_codec) #:use-module (crates-io))

(define-public crate-hpack_codec-0.1.0 (c (n "hpack_codec") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0gblnx7sh6h50ac94vkz50yyx5gx8nmr4fxp43x22l6sjkji59m7")))

(define-public crate-hpack_codec-0.1.1 (c (n "hpack_codec") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "00il50dswihl8sfgnj8k9x2ixb7mhb1jdfysw7mi80fciq0zqq81")))

