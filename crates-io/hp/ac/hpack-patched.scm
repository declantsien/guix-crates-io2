(define-module (crates-io hp ac hpack-patched) #:use-module (crates-io))

(define-public crate-hpack-patched-0.3.0 (c (n "hpack-patched") (v "0.3.0") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (o #t) (d #t) (k 0)))) (h "1s582f2bws8b7bzzjag87qbnvkqdqjr6l3wn5bviqnbgmklnipc3") (f (quote (("interop_tests" "rustc-serialize"))))))

