(define-module (crates-io hp rt hprtree) #:use-module (crates-io))

(define-public crate-hprtree-0.1.0 (c (n "hprtree") (v "0.1.0") (h "12xaggz1qwd8lazs989qgsr0srgmrqz0iasbcjpvyl69l1r7cbpy")))

(define-public crate-hprtree-0.2.0 (c (n "hprtree") (v "0.2.0") (h "0ib9km4mgrldyfrk0fdm8gnmcraph62j8vg6h5hcmm2rzgp1ll34")))

(define-public crate-hprtree-0.2.1 (c (n "hprtree") (v "0.2.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "114cqqrsvdpq1f21kl84hcvybkfjp7z2179ql3lizprz30f6f39m")))

