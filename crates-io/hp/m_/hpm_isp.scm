(define-module (crates-io hp m_ hpm_isp) #:use-module (crates-io))

(define-public crate-hpm_isp-0.1.0 (c (n "hpm_isp") (v "0.1.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "1n0pcsbqi63c1bg4fyranqj1dldgkajr0n21g0lngck9aily0zfs") (y #t)))

(define-public crate-hpm_isp-0.1.1 (c (n "hpm_isp") (v "0.1.1") (d (list (d (n "clap") (r "^3.1") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "hidapi") (r "^1.4.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.16.2") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "zerocopy") (r "^0.6.1") (d #t) (k 0)))) (h "0689lgfmll4rnvg5s6vmn18k27823f4nqgdxwgg8ydi0rrk2mzrj")))

(define-public crate-hpm_isp-0.2.0 (c (n "hpm_isp") (v "0.2.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "hidapi") (r "^2.4") (d #t) (k 0)) (d (n "hpm-rt") (r "^0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "1xnzg5g7i117y2kd1kapgmbzk3xydx3vcvxq86zhizi0i366iwnf")))

(define-public crate-hpm_isp-0.3.0 (c (n "hpm_isp") (v "0.3.0") (d (list (d (n "clap") (r "^3.1") (f (quote ("color" "derive"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10") (d #t) (k 0)) (d (n "hidapi") (r "^2.4") (d #t) (k 0)) (d (n "hpm-rt") (r "^0.1") (d #t) (k 0)) (d (n "indicatif") (r "^0.17") (d #t) (k 0)) (d (n "num_enum") (r "^0.7") (d #t) (k 0)) (d (n "strum") (r "^0.25") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "0sn5nmv84ljgm7ghzivy4d13rm5vg9jrr6arka3jpx9vcsjd7ayr")))

