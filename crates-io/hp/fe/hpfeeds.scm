(define-module (crates-io hp fe hpfeeds) #:use-module (crates-io))

(define-public crate-hpfeeds-0.1.0 (c (n "hpfeeds") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "sha-1") (r "^0.8.1") (d #t) (k 0)) (d (n "simple-error") (r "^0.1.13") (d #t) (k 0)))) (h "1dgrbhsj1l2izmcybm8ccb30df4xj33db8czgd156lxcnja6jk65")))

