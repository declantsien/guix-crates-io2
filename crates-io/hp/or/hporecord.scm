(define-module (crates-io hp or hporecord) #:use-module (crates-io))

(define-public crate-hporecord-0.0.1 (c (n "hporecord") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1c87jxpzib1wrrq15fs2z706y30a9p5m7ks2nilz8vb1cw96a5zr")))

(define-public crate-hporecord-0.0.2 (c (n "hporecord") (v "0.0.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "10krg5sn0cpzvscd445l8mzyw96zv1p2p3bsmn5p3ymf5saryd74")))

(define-public crate-hporecord-0.0.3 (c (n "hporecord") (v "0.0.3") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0grwvljaybz3rxj4w40kgis7gap2rid7ir9zvbic70l029pr80z2")))

