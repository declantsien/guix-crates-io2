(define-module (crates-io um l_ uml_parser) #:use-module (crates-io))

(define-public crate-uml_parser-0.1.0 (c (n "uml_parser") (v "0.1.0") (d (list (d (n "bodyparser") (r "^0.7.0") (d #t) (k 0)) (d (n "iron") (r "^0.5.1") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nom") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)))) (h "07ydsab93db36b4f5ip89k460hijqllvj116p83rqxdl9x55cpy8")))

(define-public crate-uml_parser-0.1.1 (c (n "uml_parser") (v "0.1.1") (d (list (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "nom") (r "^2.0.0") (d #t) (k 0)) (d (n "regex") (r "^0.1.77") (d #t) (k 0)))) (h "1k2wr3k32g1qqg9blra7sbc2ckmqzcgmay9z95vqhpxds44m3ifc")))

