(define-module (crates-io um la umlauts) #:use-module (crates-io))

(define-public crate-umlauts-0.1.0 (c (n "umlauts") (v "0.1.0") (h "0pqi8s316c6ajqzdidkpkiy9k2aj9anwfkb8bqjkk6ygjhs0q9mg")))

(define-public crate-umlauts-0.2.0-alpha.1 (c (n "umlauts") (v "0.2.0-alpha.1") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "1ahr87xgykkgsmbjhqp22ha7y3isld5q6j6qsmw53d97l3fyg73j") (f (quote (("unsafe"))))))

(define-public crate-umlauts-0.2.0-alpha.2 (c (n "umlauts") (v "0.2.0-alpha.2") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "05zqlr9d03jrsw8b5czn124sw0j65v7sdqj3f5k7lh274lrnd1np") (f (quote (("unsafe"))))))

(define-public crate-umlauts-0.1.1 (c (n "umlauts") (v "0.1.1") (h "04mf8napvx37ghf1a69i561l16gl0j4lnsvijj9fm9jdrd2jry2n")))

(define-public crate-umlauts-0.2.0-alpha.3 (c (n "umlauts") (v "0.2.0-alpha.3") (d (list (d (n "memchr") (r "^2.3") (d #t) (k 0)))) (h "1qf6bn4syyfk74w142fp623bhdw5dajz7slskghhbmj6wdg5pglf") (f (quote (("unsafe"))))))

