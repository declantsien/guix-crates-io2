(define-module (crates-io um fp umfpack-rs) #:use-module (crates-io))

(define-public crate-umfpack-rs-0.0.1 (c (n "umfpack-rs") (v "0.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "array-init") (r "^2.1.0") (d #t) (k 0)) (d (n "cc") (r "^1.0.83") (f (quote ("parallel"))) (o #t) (d #t) (k 1)) (d (n "git2") (r "^0.18.0") (d #t) (k 1)) (d (n "libc") (r "^0.2.147") (d #t) (k 0)) (d (n "num-complex") (r "^0.4.4") (d #t) (k 0)))) (h "1kxprpqycgvpz241b3yngy6vaz78fwzrsdgvnmgpbh3zd4mcvlz9") (f (quote (("default" "cc"))))))

