(define-module (crates-io um ux umux) #:use-module (crates-io))

(define-public crate-umux-1.0.0 (c (n "umux") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0.95") (d #t) (k 0)) (d (n "error-chain") (r "^0.5.0") (d #t) (k 0)) (d (n "ncurses") (r "^5.83.0") (d #t) (k 0)) (d (n "nix") (r "^0.7.0") (d #t) (k 0)))) (h "187v61623jbb5k6fcih9qj5la8zna5paymm2anyhi1wnjrl2faw8")))

