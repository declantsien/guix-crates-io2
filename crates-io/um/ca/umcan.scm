(define-module (crates-io um ca umcan) #:use-module (crates-io))

(define-public crate-umcan-0.1.0 (c (n "umcan") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)) (d (n "socketcan") (r "^2.0.0") (d #t) (k 2)))) (h "1ymjhynz0knxmqmcx1qj9k39hsxshmfqsr691s2ccdyddag3jkix")))

(define-public crate-umcan-0.1.1 (c (n "umcan") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "binrw") (r "^0.11.2") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)) (d (n "socketcan") (r "^2.0.0") (d #t) (k 2)))) (h "0wpmsbgpln9rsz0449ds9yigiyxj26il11p4s9j73k1cx53y22nk")))

(define-public crate-umcan-0.2.0 (c (n "umcan") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.72") (d #t) (k 2)) (d (n "binrw") (r "^0.11.2") (d #t) (k 0)) (d (n "embedded-can") (r "^0.4.1") (d #t) (k 0)) (d (n "socketcan") (r "^2.0.0") (d #t) (k 2)))) (h "00xahydb30szpy4s528nrci5ip48jbf2yd0k2kl20ywqx8xcn1py")))

