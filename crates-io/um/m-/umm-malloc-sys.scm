(define-module (crates-io um m- umm-malloc-sys) #:use-module (crates-io))

(define-public crate-umm-malloc-sys-0.1.0 (c (n "umm-malloc-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "05w19fx4a3sc7a42d69my49ka8adybfl2lb64xfzj2iyxrp4zwza") (f (quote (("sync") ("first-fit"))))))

(define-public crate-umm-malloc-sys-0.1.1 (c (n "umm-malloc-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.56") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "18z08bmbnnq5fcb36rab7jq5y88kd95jpfyhvlar60wkvj5m9aiw") (f (quote (("sync") ("first-fit"))))))

(define-public crate-umm-malloc-sys-0.2.0 (c (n "umm-malloc-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0jvsqi7y8garrf0rhwh5k72vpl10k2ry4jw8ayx5mp51yx5pckpl") (f (quote (("init-if-uninitialized") ("hang-if-uninitialized") ("first-fit") ("extern-critical-section")))) (l "umm_malloc")))

(define-public crate-umm-malloc-sys-0.3.0 (c (n "umm-malloc-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.59.1") (d #t) (k 1)) (d (n "cc") (r "^1") (d #t) (k 1)))) (h "15qxisq35w7xhvxwnmwwi1xs86rkncvphhwihr85y8mv5rsqmylq") (f (quote (("init-if-uninitialized") ("hang-if-uninitialized") ("first-fit") ("extern-critical-section") ("cortex-m-interrupt-critical-section")))) (l "umm_malloc")))

(define-public crate-umm-malloc-sys-0.3.1 (c (n "umm-malloc-sys") (v "0.3.1") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "13ssag1am7wd1qyagr89ihpmhf22sp2pzw63q7qqha3byy26l02m") (f (quote (("init-if-uninitialized") ("hang-if-uninitialized") ("first-fit") ("extern-critical-section") ("cortex-m-interrupt-critical-section")))) (l "umm_malloc")))

(define-public crate-umm-malloc-sys-0.3.2 (c (n "umm-malloc-sys") (v "0.3.2") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "1kl17gpxbjqlrg5indzw01rwm1r96p6bmgydv0sr2ll375bz2jxk") (f (quote (("init-if-uninitialized") ("hang-if-uninitialized") ("first-fit") ("extern-critical-section") ("enable-pie") ("cortex-m-interrupt-critical-section")))) (l "umm_malloc")))

