(define-module (crates-io um br umbra-tui) #:use-module (crates-io))

(define-public crate-umbra-tui-0.1.0 (c (n "umbra-tui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "018v50bw4hcwq0j1n5vf0jmkvsygfcibya0zppfz9vd962lvq5h3")))

