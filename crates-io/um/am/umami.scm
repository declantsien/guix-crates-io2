(define-module (crates-io um am umami) #:use-module (crates-io))

(define-public crate-umami-0.1.0 (c (n "umami") (v "0.1.0") (h "0hn15vzq1p227clpfsp2iwaql180hz85lbh3pk1dn9v8df9xc251")))

(define-public crate-umami-0.1.1 (c (n "umami") (v "0.1.1") (h "1lw25ayny9sz29ga41af9vv4x30yjhfxfah74nj7j4dpc6cz51a7")))

