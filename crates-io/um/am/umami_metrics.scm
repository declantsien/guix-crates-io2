(define-module (crates-io um am umami_metrics) #:use-module (crates-io))

(define-public crate-umami_metrics-0.1.0 (c (n "umami_metrics") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.58") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 2)) (d (n "version-sync") (r "^0.9.4") (d #t) (k 2)))) (h "0fb8pjz2n2igw0ynx137s45cf9h1dvj6y1zvr8r4wl5h3d2yrjfb")))

