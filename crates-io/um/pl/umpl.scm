(define-module (crates-io um pl umpl) #:use-module (crates-io))

(define-public crate-umpl-0.9.9 (c (n "umpl") (v "0.9.9") (d (list (d (n "hexponent") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 0)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "1ym98fkjsx4f2g5kn2k1sgdlikm1d7finkfhlx0q58gq1lapcpmn")))

(define-public crate-umpl-1.0.0 (c (n "umpl") (v "1.0.0") (d (list (d (n "hexponent") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 0)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "06jsdxvi8pn5y0n8a5ibqadgfa9x72arz8l1blm6rw2rmrd9lqly")))

(define-public crate-umpl-1.0.1 (c (n "umpl") (v "1.0.1") (d (list (d (n "hexponent") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 0)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "0rwc9lvb6vficby6j68rajx9z19z19hbsk13zr61dnc3xp08nnis")))

(define-public crate-umpl-1.0.2 (c (n "umpl") (v "1.0.2") (d (list (d (n "hexponent") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "log4rs") (r "^1.1.1") (d #t) (k 0)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "02c71xm7asg0rfqmsnwmc26kq7nxmbiprbzhzgjyih742yxwhna3")))

(define-public crate-umpl-1.1.0 (c (n "umpl") (v "1.1.0") (d (list (d (n "hexponent") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_file_logger") (r "^0.3.1") (d #t) (k 0)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "03djyn41myqjjif2s9xbjxypgqcx5s26k0r54yk33lb5sdy9in44") (r "1.59.0")))

(define-public crate-umpl-1.1.1 (c (n "umpl") (v "1.1.1") (d (list (d (n "hexponent") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "simple_file_logger") (r "^0.3.1") (d #t) (k 0)) (d (n "unic-emoji-char") (r "^0.9.0") (d #t) (k 0)))) (h "14nhi82vk0yr3a5l1nm407n3l53x4vzjkglrbn7g3n8fafgvni2x") (r "1.59.0")))

