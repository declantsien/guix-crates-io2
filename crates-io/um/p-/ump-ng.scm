(define-module (crates-io um p- ump-ng) #:use-module (crates-io))

(define-public crate-ump-ng-0.1.0 (c (n "ump-ng") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("async_tokio"))) (d #t) (k 2)) (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)) (d (n "sigq") (r "^0.13.4") (d #t) (k 0)) (d (n "swctx") (r "^0.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.32.0") (f (quote ("rt-multi-thread"))) (d #t) (k 2)))) (h "1nk2n9c70ki00234yln5bndxmyjql5yf5d2qqq8vsl442b8vxkgz")))

