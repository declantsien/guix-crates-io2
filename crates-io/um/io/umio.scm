(define-module (crates-io um io umio) #:use-module (crates-io))

(define-public crate-umio-0.1.0 (c (n "umio") (v "0.1.0") (d (list (d (n "mio") (r "^0.5.0") (d #t) (k 0)))) (h "0fqc8raw13mvgrr38gxvy6p9bwl6zzcvgklrszng5bfy8nyg5287") (f (quote (("unstable"))))))

(define-public crate-umio-0.2.0 (c (n "umio") (v "0.2.0") (d (list (d (n "mio") (r "^0.5.0") (d #t) (k 0)))) (h "0xq4qvid5hs43dbc4w2z82ird8gjbbsd2adrfi3ps8m5q1icivj3") (f (quote (("unstable"))))))

(define-public crate-umio-0.3.0 (c (n "umio") (v "0.3.0") (d (list (d (n "mio") (r "^0.5.0") (d #t) (k 0)))) (h "1db78yddda82mp8n2mb517nycqpb9bzzaihis1gbyg8qwysy9ql6") (f (quote (("unstable"))))))

