(define-module (crates-io um ik umiko) #:use-module (crates-io))

(define-public crate-umiko-0.1.0 (c (n "umiko") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1wlxwahny8vgynpyzkzqbk3k9nng7mbjkysp2mml417nv8qabrrd") (f (quote (("keys") ("hotkeys") ("default" "hotkeys" "keys"))))))

(define-public crate-umiko-0.1.1 (c (n "umiko") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1fx84msp1dld72qwa51ai67w4378bw42sz733ifgg81mcndn0h7a") (f (quote (("keys") ("hotkeys") ("default" "hotkeys" "keys"))))))

(define-public crate-umiko-0.1.2 (c (n "umiko") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1iqda48pq2d9zc8p5l3213l7ar8y65srvs5mm1cys521dxdpmxlz") (f (quote (("keys") ("hotkeys") ("default" "hotkeys" "keys"))))))

(define-public crate-umiko-0.1.3 (c (n "umiko") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0ds16xrh7qx4xzy1iijjp4770ll18gf2hq560csf0qfzlkvyhbcb") (f (quote (("keys") ("hotkeys") ("default" "hotkeys" "keys"))))))

(define-public crate-umiko-0.1.4 (c (n "umiko") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "winapi") (r "^0.3.9") (f (quote ("winuser"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02p4d1vlm1dxhyqcncq98hf4xl97z5qn9ixxxdik4c8n5hvcsvg4") (f (quote (("keys") ("hotkeys") ("default" "hotkeys" "keys"))))))

