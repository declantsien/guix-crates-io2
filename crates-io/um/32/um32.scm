(define-module (crates-io um #{32}# um32) #:use-module (crates-io))

(define-public crate-um32-1.0.0 (c (n "um32") (v "1.0.0") (d (list (d (n "byteorder") (r "1.0.*") (d #t) (k 0)))) (h "0n2d60f77l6amyx6bn1s05xvh8h8ciwqpzlrvq6jxj2qw77dz7l3") (y #t)))

(define-public crate-um32-1.0.1 (c (n "um32") (v "1.0.1") (d (list (d (n "byteorder") (r "1.0.*") (d #t) (k 0)))) (h "0d0vfpgghjwmm8n2x52qwvwy81xpbbnbk25nf3gmlv8dpyasvirl") (y #t)))

(define-public crate-um32-1.0.2 (c (n "um32") (v "1.0.2") (d (list (d (n "byteorder") (r "1.0.*") (d #t) (k 0)))) (h "1vcs80cs1b2l5dmj66fbi4hfaw32dr9dwz7la6kr4xr33gazm3cd") (y #t)))

