(define-module (crates-io um an umanux) #:use-module (crates-io))

(define-public crate-umanux-0.1.1 (c (n "umanux") (v "0.1.1") (d (list (d (n "chrono") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "clap") (r ">=3.0.0-beta, <4.0.0") (d #t) (k 0)) (d (n "derive_builder") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "env_logger") (r ">=0.8.0, <0.9.0") (d #t) (k 0)) (d (n "lazy_static") (r ">=1.4.0, <2.0.0") (d #t) (k 0)) (d (n "log") (r ">=0.4.0, <0.5.0") (d #t) (k 0)) (d (n "regex") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "simplelog") (r ">=0.8.0, <0.9.0") (d #t) (k 0)) (d (n "tempfile") (r ">=3.1.0, <4.0.0") (d #t) (k 2)) (d (n "test_bin") (r ">=0.3.0, <0.4.0") (d #t) (k 2)))) (h "1m3bsbhy58jm8n5nqf9pbgxm76ghf1xyr71dlk2ipm531028j6iw")))

