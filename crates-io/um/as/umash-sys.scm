(define-module (crates-io um as umash-sys) #:use-module (crates-io))

(define-public crate-umash-sys-0.1.1 (c (n "umash-sys") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "119a1h4cgn8mps4w6kzqs0k987vyybl3x19ani0av0bvjlxvxrni")))

(define-public crate-umash-sys-0.1.2 (c (n "umash-sys") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "14xrdh2146kpmf3x87h5w2b2nrkhi227c8v3xv2gl0hxjx793fja")))

(define-public crate-umash-sys-0.2.0 (c (n "umash-sys") (v "0.2.0") (d (list (d (n "cc") (r "^1") (d #t) (k 1)))) (h "0h1wxkf7cmql5inr601hahw7hk1k5b22hqgg33qn943gd4y4j1b5")))

