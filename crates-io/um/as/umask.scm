(define-module (crates-io um as umask) #:use-module (crates-io))

(define-public crate-umask-0.1.0 (c (n "umask") (v "0.1.0") (h "1lyb0r3s1brfl2cb66rwf6zii81ramxkjqkq2ak93z7v3r5gxpr4")))

(define-public crate-umask-0.1.1 (c (n "umask") (v "0.1.1") (h "1qihrky1wk1mp0sj45jixiyr7i3wlr4229z2cgx32myf5y0s29xd")))

(define-public crate-umask-0.1.2 (c (n "umask") (v "0.1.2") (h "0ra9vmlhcx9yzbhim72axn1fzbs44v0rhbh8z2j5lj7x0lgiigsc")))

(define-public crate-umask-0.1.3 (c (n "umask") (v "0.1.3") (h "0cqzandlz6m2l9mc7wldip0qf834728m0bibnmiwmp5rl2lzzx9q")))

(define-public crate-umask-0.1.4 (c (n "umask") (v "0.1.4") (h "15bb6fjmqzdrri99yrv4ap1gm5h5dbcvlzxmwvs0fzmfkps1q8z8")))

(define-public crate-umask-0.1.5 (c (n "umask") (v "0.1.5") (h "025ysm69rylr8kv776csng1n0ab7lia64bh3204p0zcri0ny5mlg")))

(define-public crate-umask-0.1.6 (c (n "umask") (v "0.1.6") (h "1av94zv88r4wqc4lf2036f16sf4dffy2mxilis2m3c1hj50rbfx0")))

(define-public crate-umask-0.1.7 (c (n "umask") (v "0.1.7") (h "1bwjiizlffd8x0w775mr06jqsw4jf8lnmrfjmwxx0i9df6gcm7x2")))

(define-public crate-umask-0.1.8 (c (n "umask") (v "0.1.8") (h "0rvhf2rxlh4la6m5fzqv6cx9qjpcgwx5294ivc8dbbaaxdd2xv6k")))

(define-public crate-umask-1.0.0 (c (n "umask") (v "1.0.0") (h "0ipyyv82lpy5xpqzmq3ra0d61vsd3bfh6b06c9w8zln41vvznblq")))

(define-public crate-umask-1.0.1 (c (n "umask") (v "1.0.1") (h "1f5nkxmbq5dj0qwzx1fqzhfxlksscg5l6p11xdivb4s1965g7czg")))

(define-public crate-umask-2.0.0 (c (n "umask") (v "2.0.0") (h "0rzswv5956m05kvjyjki6ssjw5gks6bzqw39rmx2qcgvmmpc3c26")))

(define-public crate-umask-2.1.0 (c (n "umask") (v "2.1.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "071xszsd6znk0ik11pxl7mwhf07clsiq3qpzw1ac0dcyak14d6pc")))

