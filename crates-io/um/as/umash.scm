(define-module (crates-io um as umash) #:use-module (crates-io))

(define-public crate-umash-0.1.2 (c (n "umash") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "umash-sys") (r "^0.1.2") (d #t) (k 0)))) (h "0b1s96dfk4ypbf0cdnllh92xisddmawhk0z7ybcavclii9s8kh98")))

(define-public crate-umash-0.2.0 (c (n "umash") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "umash-sys") (r "^0.2.0") (d #t) (k 0)))) (h "0ij6cm1a7v95sv6wfzs38cpbmrybq16d2dzl02j2w2yv2vfpji14")))

(define-public crate-umash-0.3.0 (c (n "umash") (v "0.3.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "umash-sys") (r "^0.2") (d #t) (k 0)))) (h "13530a881ydp7pprg7f3lkqnhy7wg2523xyd4b4g47zz4dqnscbi")))

(define-public crate-umash-0.4.0 (c (n "umash") (v "0.4.0") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "umash-sys") (r "^0.2") (d #t) (k 0)))) (h "09wmlpikc990b45wicikg67vk5vcik4b68ivijszgk65pd0slhgi")))

(define-public crate-umash-0.4.1 (c (n "umash") (v "0.4.1") (d (list (d (n "getrandom") (r "^0.2") (d #t) (k 0)) (d (n "umash-sys") (r "^0.2") (d #t) (k 0)))) (h "02n6rn86hi0zqr67rdgi9gkamkwxd6ymrhwmhzhvmy84275j09sm")))

