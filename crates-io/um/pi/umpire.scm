(define-module (crates-io um pi umpire) #:use-module (crates-io))

(define-public crate-umpire-0.4.0 (c (n "umpire") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "cpal") (r "^0.10.0") (d #t) (k 0)) (d (n "criterion") (r "^0.3.1") (d #t) (k 2)) (d (n "crossterm") (r "^0.16.0") (d #t) (k 0)) (d (n "csv") (r "^1.1.3") (d #t) (k 0)) (d (n "failure") (r "^0.1.6") (d #t) (k 0)) (d (n "flate2") (r "^1.0.13") (d #t) (k 0)) (d (n "pastel") (r "^0.7.0") (d #t) (k 0)) (d (n "pitch_calc") (r "^0.11.1") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "sample") (r "^0.10.0") (d #t) (k 0)) (d (n "synth") (r "^0.11.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0v2bnhkl578i873h8f75rpjqs83ksrv7x8cnwaqvy9x4a4rb4qf8")))

