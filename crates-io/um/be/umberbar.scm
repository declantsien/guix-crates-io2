(define-module (crates-io um be umberbar) #:use-module (crates-io))

(define-public crate-umberbar-0.7.0 (c (n "umberbar") (v "0.7.0") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "systemstat") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1gsyvp5jkniaz5yd6jkiccm3n42cv1rb2cfg4avna2xc7rym433v")))

(define-public crate-umberbar-0.7.1 (c (n "umberbar") (v "0.7.1") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "systemstat") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "088h53b1flr0n2hpqa5f8njxp86d44szk6fv7v2cbbkm1ma4f6yh")))

(define-public crate-umberbar-0.7.2 (c (n "umberbar") (v "0.7.2") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "systemstat") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0sip4dhp8gcxwbk2rfcy2imh46ks6y4fxbn781hvfjfh5zgdq974")))

(define-public crate-umberbar-0.7.3 (c (n "umberbar") (v "0.7.3") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "systemstat") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "049hvg33s1xl95bi47pg1akbcdv0b0b3m5ck2np2l5w13xvacni6")))

(define-public crate-umberbar-0.7.4 (c (n "umberbar") (v "0.7.4") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "systemstat") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0ikn93466k68jh8l2p8hk503lwkchrd734ias6241mhdpqxm35ah")))

(define-public crate-umberbar-0.7.5 (c (n "umberbar") (v "0.7.5") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "systemstat") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1npk601rlhsq6qwkqp0g1xccrckbrppl22lrb19vww6jbdravdff")))

(define-public crate-umberbar-0.7.6 (c (n "umberbar") (v "0.7.6") (d (list (d (n "regex") (r "^1.4") (d #t) (k 0)) (d (n "systemstat") (r "^0.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "092wpq9rdnaj40q4lahz5gi4xyydlhccwi2a3apakvcb1yf712dl")))

