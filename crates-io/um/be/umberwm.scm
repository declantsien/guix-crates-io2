(define-module (crates-io um be umberwm) #:use-module (crates-io))

(define-public crate-umberwm-0.0.1 (c (n "umberwm") (v "0.0.1") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (d #t) (k 0)))) (h "1x70mbl06b3wmqqkqyx1y4dbrlq9v4sz1w0w41vjysrrl2kg1mpz")))

(define-public crate-umberwm-0.0.2 (c (n "umberwm") (v "0.0.2") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (d #t) (k 0)))) (h "132cbk016y2hmznnk5lsmp43q3h23k15j74rd08fzdh8jrhj8bgm")))

(define-public crate-umberwm-0.0.3 (c (n "umberwm") (v "0.0.3") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (d #t) (k 0)))) (h "0wmjyx642r35zl019ijpymdanismpwv24g17pf75bbdz2c3scvns")))

(define-public crate-umberwm-0.0.4 (c (n "umberwm") (v "0.0.4") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (d #t) (k 0)))) (h "0qq0kyqdgw0135ww6bdl6kc6qzxvkviw99x9vdq1nwyjjgysyciv")))

(define-public crate-umberwm-0.0.5 (c (n "umberwm") (v "0.0.5") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "0121rafziy4cpj74s8wd58igrhlk16w47n9yalfrhbph5vd1qpqq")))

(define-public crate-umberwm-0.0.6 (c (n "umberwm") (v "0.0.6") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "18jmiz2ff5m84s9b80d0cf8lxw4b0sdiv7ll5vcn5kny02jlccfa")))

(define-public crate-umberwm-0.0.7 (c (n "umberwm") (v "0.0.7") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "1hhy2sb0v7zlv7mwq770bwx4bqba2b0rhlhk02zgn5pfzrw4pzcr")))

(define-public crate-umberwm-0.0.8 (c (n "umberwm") (v "0.0.8") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "0r0cx5s4cgvi8rigfy5x1n43k3myl5iwsffplrk29fz8p2in6rj5")))

(define-public crate-umberwm-0.0.9 (c (n "umberwm") (v "0.0.9") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "0n2ds59a8hj89a7867xvld73ac8qqyz93hm4zlgsy8pla79573g0")))

(define-public crate-umberwm-0.0.10 (c (n "umberwm") (v "0.0.10") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "000q0gmkgj6rvigqs1flcbfl88ml815nrk11babqgk82v0292xgv")))

(define-public crate-umberwm-0.0.11 (c (n "umberwm") (v "0.0.11") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "0i3r16k2d33308ma5k4iwshp7jf9mlw1xf24zcf11w9myq8q74qq")))

(define-public crate-umberwm-0.0.12 (c (n "umberwm") (v "0.0.12") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "1ilr977vm7bklrx89a7j2h1n1q5ac6lw2q7iwa5wd348xnmcnk21")))

(define-public crate-umberwm-0.0.13 (c (n "umberwm") (v "0.0.13") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "18hjm4q3s37jv3pq8756pnlg3nnp2jznf1yrzab9gx6v5q3nc915")))

(define-public crate-umberwm-0.0.14 (c (n "umberwm") (v "0.0.14") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "0q9jx6faiixf9nxgnzc28gjjps12csrpfadb3xzfr521dv97ragm")))

(define-public crate-umberwm-0.0.15 (c (n "umberwm") (v "0.0.15") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "0r189g7bs4ds8cq5d4vpm438mh3y4jznaqd29yr70y0gkkjx7wrk")))

(define-public crate-umberwm-0.0.16 (c (n "umberwm") (v "0.0.16") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "06mv67x1immrc5v0cvfa5z1hx2vcgxq5a5rlxjy559w54r04f6px")))

(define-public crate-umberwm-0.0.17 (c (n "umberwm") (v "0.0.17") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "0d0j82v4qaksajpnv879rdsj7420q0f3ayy5kk0l1j4417qbsj2f")))

(define-public crate-umberwm-0.0.18 (c (n "umberwm") (v "0.0.18") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "0va7hllxizwwza3zkjnzn40aw1v1fn67wvmxix51gs52rbaxf88f")))

(define-public crate-umberwm-0.0.19 (c (n "umberwm") (v "0.0.19") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "0m4rvgrchx653nnml87apghsxva6fkmmz5g8m6bw7xkydignrr82")))

(define-public crate-umberwm-0.0.20 (c (n "umberwm") (v "0.0.20") (d (list (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "043kw2zi5bybsaf0hwy10xp0rkdwi3bz82qv6x8d94150gk5k8mp")))

(define-public crate-umberwm-0.0.21 (c (n "umberwm") (v "0.0.21") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "1fff88fkbalgcywavgd3mck4wy1nm74wjk7p6m45cx212g4ifhml")))

(define-public crate-umberwm-0.0.22 (c (n "umberwm") (v "0.0.22") (d (list (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "14x88rjaswhd7rh9lv6hxqzflsqy0fsh3rh77n4xdqc62dqrbq1i")))

(define-public crate-umberwm-0.0.24 (c (n "umberwm") (v "0.0.24") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "regex") (r "^1.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)))) (h "057gswnkkqn9jy8iyws775w0w53pyxbkqdymkw6bwygn58qqqka7")))

(define-public crate-umberwm-0.0.25 (c (n "umberwm") (v "0.0.25") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "miniserde") (r "^0.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)) (d (n "xmodmap-pke-umberwm") (r "^0.0.1") (d #t) (k 0)))) (h "19wfw9fdp3lv9ijizapzc4a06lgjqx5n4sjx9g4s0ic2bwmp0g44")))

(define-public crate-umberwm-0.0.26 (c (n "umberwm") (v "0.0.26") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.9") (f (quote ("macros" "json"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)) (d (n "xmodmap-pke-umberwm") (r "^0.0.1") (d #t) (k 0)))) (h "15p7snymwn2pyds9z1nf66nv72kwyi16845gnklldh1v7lqbkfa6")))

(define-public crate-umberwm-0.0.27 (c (n "umberwm") (v "0.0.27") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)) (d (n "xmodmap-pke-umberwm") (r "^0.0.1") (d #t) (k 0)))) (h "0nhxg3amlzwn3qc1a6n1yzmn831x8g2xnvgdfk0wipzdcgc6nlgr")))

(define-public crate-umberwm-0.0.28 (c (n "umberwm") (v "0.0.28") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)) (d (n "xmodmap-pke-umberwm") (r "^0.0.1") (d #t) (k 0)))) (h "0z3f6amgpsjvn6y921zy76iz4158yplmdp72s86m5gyxav1gbkcd")))

(define-public crate-umberwm-0.0.29 (c (n "umberwm") (v "0.0.29") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)) (d (n "xmodmap-pke-umberwm") (r "^0.0.1") (d #t) (k 0)))) (h "024s99mb4y2fz241xmlzc5rqlad0z5d1ykid8lg4wcasn3j34w40")))

(define-public crate-umberwm-0.0.30 (c (n "umberwm") (v "0.0.30") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)) (d (n "xmodmap-pke-umberwm") (r "^0.0.1") (d #t) (k 0)))) (h "0p1l86amkiz36xwh763hg36ggf2bxwca4id57ap3ikxc5a0f6xs4")))

(define-public crate-umberwm-0.0.31 (c (n "umberwm") (v "0.0.31") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)) (d (n "xmodmap-pke-umberwm") (r "^0.0.1") (d #t) (k 0)))) (h "0v26i0lljrnyjdnwib1mksg394vsd1kaz7ff9mr1v3gmxzxs7w68")))

(define-public crate-umberwm-0.0.32 (c (n "umberwm") (v "0.0.32") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)) (d (n "xmodmap-pke-umberwm") (r "^0.0.1") (d #t) (k 0)))) (h "00jkvmqppir4ilgyw5583kdpwf6c7nd4j326ywxgl1v0dc3qkppp")))

(define-public crate-umberwm-0.0.33 (c (n "umberwm") (v "0.0.33") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)) (d (n "xmodmap-pke-umberwm") (r "^0.0.1") (d #t) (k 0)))) (h "02br8hpcbjmprmyja3hjk85pq3rvk60rnrayqm76cmyb2j1z800g")))

(define-public crate-umberwm-0.0.34 (c (n "umberwm") (v "0.0.34") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "dirs") (r "^3.0") (d #t) (k 0)) (d (n "ron") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_with") (r "^1.9") (f (quote ("macros"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)) (d (n "xcb") (r "^0.9") (f (quote ("randr"))) (d #t) (k 0)) (d (n "xmodmap-pke-umberwm") (r "^0.0.1") (d #t) (k 0)))) (h "1jbcr7k3vbqzv7pxw106lj0kyabvxihk4m0qqlbrnc8yywbj6bhq")))

