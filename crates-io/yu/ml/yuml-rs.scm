(define-module (crates-io yu ml yuml-rs) #:use-module (crates-io))

(define-public crate-yuml-rs-0.1.0 (c (n "yuml-rs") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0lk73annggzgfqksc3mkshi9vrbk5543my3jpf973lrxmx8ddqnd")))

(define-public crate-yuml-rs-0.1.1 (c (n "yuml-rs") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)) (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "0fq1y7n3bq3drgv8h9m9f1yw2sh20y4qdp2j0gn2qzcpgwkdjbcy")))

(define-public crate-yuml-rs-0.1.2 (c (n "yuml-rs") (v "0.1.2") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "1zyf73sd45rwjs5as9l29qgfp46sv2x9ch62v49hqkvr7ll7zmd1")))

(define-public crate-yuml-rs-0.1.3 (c (n "yuml-rs") (v "0.1.3") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "07pfdn7ixx2s37rqcrs5i2s30ha1s5xnqkhip1g5y65gvqdfpdla")))

(define-public crate-yuml-rs-0.1.4 (c (n "yuml-rs") (v "0.1.4") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0pda54ijwbmqcvw3m0yklp1ha77qaw1x3xp7xl1ykphibian0hsm")))

(define-public crate-yuml-rs-0.1.5 (c (n "yuml-rs") (v "0.1.5") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0x3ly00h4v2jvvj48w5xg1mn9fz05icz3kk25ckrhmq40sigygl0")))

(define-public crate-yuml-rs-0.1.6 (c (n "yuml-rs") (v "0.1.6") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "derive_more") (r "^0.99") (d #t) (k 0)) (d (n "itertools") (r "^0.10") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "nom") (r "^7") (d #t) (k 0)))) (h "055jjpbwl79bb96c2alhm3cxx3a9b4q2i1whjqc8lbydk9s6pwqj")))

