(define-module (crates-io yu li yulid) #:use-module (crates-io))

(define-public crate-yulid-0.1.0 (c (n "yulid") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (f (quote ("i128"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_cbor") (r "^0.9") (d #t) (k 2)) (d (n "serde_derive") (r "^1") (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^0.7") (o #t) (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("v4"))) (d #t) (k 2)))) (h "0dmns2w820fjxw9vp2mqidmlll5amfaqrx585yp2lnxcq3smckah")))

