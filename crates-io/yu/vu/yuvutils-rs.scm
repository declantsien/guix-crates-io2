(define-module (crates-io yu vu yuvutils-rs) #:use-module (crates-io))

(define-public crate-yuvutils-rs-0.1.0 (c (n "yuvutils-rs") (v "0.1.0") (h "0md35xylnfg88nsk76wjhb5ffvg5v9aj342h3y226iy8srxsqyv7")))

(define-public crate-yuvutils-rs-0.1.1 (c (n "yuvutils-rs") (v "0.1.1") (h "1q3kbsc409r6j7sjbm3l5mi8p9idny2cdlykbwk4s0b406w66gqq")))

(define-public crate-yuvutils-rs-0.1.2 (c (n "yuvutils-rs") (v "0.1.2") (h "1fns4y567nzqhv20bmyl5x67sfgcnapijjknqmxbzk9xjcm8gk60")))

(define-public crate-yuvutils-rs-0.1.3 (c (n "yuvutils-rs") (v "0.1.3") (h "18a3ypgzkb8si5hckcz00ckli9yl8babhb2nq69s8kd52qs5fdk8")))

(define-public crate-yuvutils-rs-0.1.4 (c (n "yuvutils-rs") (v "0.1.4") (h "19hsq6vrarycfgrb508fgwl2jbkz05k8qlqs843q4dhc7m59iwdx")))

(define-public crate-yuvutils-rs-0.1.5 (c (n "yuvutils-rs") (v "0.1.5") (h "17p5yh82pb056jhprg8qb0bp9qbsdj7jlxp3jnj2lhjp4jksp88f")))

(define-public crate-yuvutils-rs-0.1.6 (c (n "yuvutils-rs") (v "0.1.6") (h "12bynarkwzk1146z7kpn2h767cqnidh42pq7niv0m0pmgy8zksxi")))

(define-public crate-yuvutils-rs-0.1.7 (c (n "yuvutils-rs") (v "0.1.7") (h "16vmh8qnl7kysx01lzpj4pizdfab5aj7j7pxjhnsb49jby49x4s1")))

(define-public crate-yuvutils-rs-0.1.8 (c (n "yuvutils-rs") (v "0.1.8") (h "16aqs2ki6k1sksrbfkcmn05micw9c2pvzvxwdr4181dl2fz0d0js")))

(define-public crate-yuvutils-rs-0.1.9 (c (n "yuvutils-rs") (v "0.1.9") (h "1rgnl9kkd42ddarzsnnryxp4pw2ywriz23sz0cmqzwb0qwfd10x0")))

(define-public crate-yuvutils-rs-0.1.10 (c (n "yuvutils-rs") (v "0.1.10") (h "00kbsihkwz1kiy3zx2kkj7mqh6gml7h0q423hz9aad8h8gbpy14j")))

(define-public crate-yuvutils-rs-0.1.11 (c (n "yuvutils-rs") (v "0.1.11") (h "155z3hhfvanj7pm8wx5cw2aa2xa5l45hvbwwrccjazapkyw1pwn7")))

(define-public crate-yuvutils-rs-0.1.12 (c (n "yuvutils-rs") (v "0.1.12") (h "0ifpb8s2l1qk6lylqjn7qgzx01i4nb4wsvwqi2dnwngn0j011qa5")))

(define-public crate-yuvutils-rs-0.1.13 (c (n "yuvutils-rs") (v "0.1.13") (h "1fibfx04jl3w91vm54a8avy27bh6szqzkhhgca6xv086kk7vh4pj")))

(define-public crate-yuvutils-rs-0.2.0 (c (n "yuvutils-rs") (v "0.2.0") (h "1i24ca5jfmvrb0zigm2jhs5gbp2cmr5qyjnxk6500967rx13xhhf") (f (quote (("nightly_avx512") ("default"))))))

(define-public crate-yuvutils-rs-0.2.1 (c (n "yuvutils-rs") (v "0.2.1") (h "0n8nldd2xfhilgnxkg82ja8aqyppsrcrk41npfxyl09xrhjhqkwv") (f (quote (("nightly_avx512") ("default"))))))

(define-public crate-yuvutils-rs-0.2.2 (c (n "yuvutils-rs") (v "0.2.2") (h "1m99v5yp7hkvhlxqafv0phf7ffz4dbgh69vcx4gi0s1bd50i78vs") (f (quote (("nightly_avx512") ("default"))))))

