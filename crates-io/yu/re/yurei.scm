(define-module (crates-io yu re yurei) #:use-module (crates-io))

(define-public crate-yurei-0.1.0 (c (n "yurei") (v "0.1.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "14nkqsnmmfyj4db9j0vbfvinvv038l17krl1bbz7z50kaadfkz2r")))

(define-public crate-yurei-0.2.0 (c (n "yurei") (v "0.2.0") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "136zmls6mffm0lalw4cvpcqdqq4qw84175605pr4xgcafzrxbv3b")))

(define-public crate-yurei-0.2.1 (c (n "yurei") (v "0.2.1") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "02bvvd7zfk0r6y76ckybdcv06i09y2726nznkm0pcwhg7dm7nl5c")))

(define-public crate-yurei-0.2.2 (c (n "yurei") (v "0.2.2") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "1sk0saxp8m1y71sg7y392afac57ldh0q5j96yr6lllvpga86v0pz")))

(define-public crate-yurei-0.2.21 (c (n "yurei") (v "0.2.21") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "0ay348javy4crvd31z6hzksbabngba9m17mq72vpk7bzd3cch5rp")))

(define-public crate-yurei-0.2.22 (c (n "yurei") (v "0.2.22") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "0vknn7xp0w0mx05fk9vd74yga3d3vjrpcd2s8574vfzn74hpf9l6")))

(define-public crate-yurei-0.2.23 (c (n "yurei") (v "0.2.23") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "1ib3395x7dyaq2xlvc9dllcra6c7hv9zri0wahrdjv26f9f7jck1")))

(define-public crate-yurei-0.2.24 (c (n "yurei") (v "0.2.24") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "1cbwf0nhxwxnachcgnqfjypd341rrqrz3f7c6lsyqssvgg1x4iy3")))

(define-public crate-yurei-0.2.25 (c (n "yurei") (v "0.2.25") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "1dg1lyphqam43n448fs8fpkb7h49yfv3vs43jkldzhip00vha33p")))

(define-public crate-yurei-0.2.26 (c (n "yurei") (v "0.2.26") (d (list (d (n "bevy") (r "^0.9.1") (d #t) (k 0)) (d (n "bevy_rapier3d") (r "^0.19.0") (d #t) (k 0)))) (h "1qyhpy1kcx6c5rxiri9027i4hv9dng76a7rcnsrvg2if58nbcxrr")))

