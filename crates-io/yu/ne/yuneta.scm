(define-module (crates-io yu ne yuneta) #:use-module (crates-io))

(define-public crate-yuneta-0.1.0 (c (n "yuneta") (v "0.1.0") (h "0hypfx02h4bmhajlyfpa8xni0mqajmpc8idpjxk620g2zqqn7waz")))

(define-public crate-yuneta-0.1.1 (c (n "yuneta") (v "0.1.1") (h "02b796vd03w3730kanxc4iaqgpad3k6ippv74ky0ljl0gyjg0i0c")))

