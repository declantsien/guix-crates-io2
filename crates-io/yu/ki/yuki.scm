(define-module (crates-io yu ki yuki) #:use-module (crates-io))

(define-public crate-yuki-0.1.0 (c (n "yuki") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 0)))) (h "0bjhcara79k0jy2k0irskz0nv5l1g27rslyj4774qd1l7wd76432")))

(define-public crate-yuki-0.1.1 (c (n "yuki") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 0)))) (h "0n4nnk0armp5na1cypamhxlfipbdz03f45lw0jfxyq2ngpyy2avr") (y #t)))

(define-public crate-yuki-0.1.2 (c (n "yuki") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 0)))) (h "0pgm1iqdmbifaw0nmjqvlkq9prq8agl1zgfar7nj443b5aflhizl")))

(define-public crate-yuki-0.1.3 (c (n "yuki") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking"))) (d #t) (k 0)) (d (n "simple_logger") (r "^4.1.0") (d #t) (k 0)))) (h "1a608zxc9wx2bbk8iw19k3adclzp89dajmbgffl25lakd96drsv5")))

