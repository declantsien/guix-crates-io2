(define-module (crates-io yu xi yuxii_macros) #:use-module (crates-io))

(define-public crate-yuxii_macros-0.1.0 (c (n "yuxii_macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1qcc7ydcfz7vv8x5iqzgnl0wdp1bv4rin1ggygjjqxarnzx6x54m")))

(define-public crate-yuxii_macros-0.1.1 (c (n "yuxii_macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.51") (d #t) (k 0)) (d (n "quote") (r "^1.0.26") (d #t) (k 0)) (d (n "syn") (r "^1.0.107") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "08cglp5aylgpvxk9z67r2myl713a90rybjn36zv7w9v5i4v0clz5")))

