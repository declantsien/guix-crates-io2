(define-module (crates-io yu i_ yui_derive) #:use-module (crates-io))

(define-public crate-yui_derive-0.1.0 (c (n "yui_derive") (v "0.1.0") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "yui_internal") (r "^0.1") (d #t) (k 0)))) (h "1kl8sywzhs5w14rwc7lmbz818psiqlp7w0a2j163x6xpyyq5ln8c") (y #t)))

(define-public crate-yui_derive-0.1.1 (c (n "yui_derive") (v "0.1.1") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "yui_internal") (r "^0.1") (d #t) (k 0)))) (h "19pgjikaas9sxax2hz21sxg5g1gn2wg4015ya1nbv964nx01ya16") (y #t)))

(define-public crate-yui_derive-0.1.2 (c (n "yui_derive") (v "0.1.2") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "yui_internal") (r "^0.1") (d #t) (k 0)))) (h "0597hhjykn4qxiwq5va994i25g2hw2fa20l86hf1nnml6d0648h3") (y #t)))

(define-public crate-yui_derive-0.1.3 (c (n "yui_derive") (v "0.1.3") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "yui_internal") (r "^0.1") (d #t) (k 0)))) (h "05fkd561r1bdchypqcmarwvj0qsq3l3r72g1gqv6lm322nkb6x6q")))

(define-public crate-yui_derive-0.1.4 (c (n "yui_derive") (v "0.1.4") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "yui_internal") (r "^0.1.4") (d #t) (k 0)))) (h "1bds5n5ss3b3s8ykips62zj5yivvx00d1d0ms7cfgmc3c52hxsd0")))

(define-public crate-yui_derive-0.1.5 (c (n "yui_derive") (v "0.1.5") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "yui_internal") (r "^0.1.5") (d #t) (k 0)))) (h "1mq397glvik2imsqbxjfff0czxcm1m8qmh693sx0r3n8f6h7spnp")))

(define-public crate-yui_derive-0.1.6 (c (n "yui_derive") (v "0.1.6") (d (list (d (n "heck") (r "^0.3.1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("fold" "full"))) (d #t) (k 0)) (d (n "yui_internal") (r "^0.1.5") (d #t) (k 0)))) (h "0g3k1vvsd6x0jsbhfrfjmwh5v4n5nas71i9y7g2g4rpj8mql5fmn")))

