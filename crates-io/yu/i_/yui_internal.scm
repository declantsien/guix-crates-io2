(define-module (crates-io yu i_ yui_internal) #:use-module (crates-io))

(define-public crate-yui_internal-0.1.0 (c (n "yui_internal") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rdl10bzkbssag29nrzz05fqgm1l6zyyggnby26lr31c353ia0aj") (y #t)))

(define-public crate-yui_internal-0.1.1 (c (n "yui_internal") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "02di054y3ycmsya0xbwhfincsl0drbjpzbdffpb5l00g9w80vr6v") (y #t)))

(define-public crate-yui_internal-0.1.2 (c (n "yui_internal") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1pq07lnvjx0hcwv093a8pqj79hg6vc0yi4b32i37cgma6azfgzvr") (y #t)))

(define-public crate-yui_internal-0.1.3 (c (n "yui_internal") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1wmvdnw8k7c3c2yfadnh96vdj6w3psmyw78fsndb1504pn7j904w")))

(define-public crate-yui_internal-0.1.4 (c (n "yui_internal") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0kw23p0aam3p5lli2igbcs3md6cccb4kmd2gkg2q5d3q9zlk8q6b")))

(define-public crate-yui_internal-0.1.5 (c (n "yui_internal") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "082cs04qag1sdsa01parjhak33ndpp7g6vwvr37lrn5nz5h6x32r")))

