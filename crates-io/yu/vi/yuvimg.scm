(define-module (crates-io yu vi yuvimg) #:use-module (crates-io))

(define-public crate-yuvimg-0.1.0 (c (n "yuvimg") (v "0.1.0") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 2)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 2)))) (h "0x7vmgyz5jgqv2d3im43a7ym6ry3rcrvxlf8hprynxz0wpfsg7k1")))

(define-public crate-yuvimg-0.1.1 (c (n "yuvimg") (v "0.1.1") (d (list (d (n "conv") (r "^0.3.3") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "image") (r "^0.24.4") (d #t) (k 0)) (d (n "imageproc") (r "^0.23.0") (d #t) (k 2)) (d (n "rusttype") (r "^0.9.2") (d #t) (k 2)))) (h "1c2f2pvxqmvan2xz6yx8zv2fdy77vpx1bcr2hqmgv32jbzdg00hm")))

