(define-module (crates-io yu lc yulc) #:use-module (crates-io))

(define-public crate-yulc-0.1.1 (c (n "yulc") (v "0.1.1") (d (list (d (n "cli-batteries") (r "^0.1.1") (f (quote ("rand" "rayon"))) (d #t) (k 0)) (d (n "cli-batteries") (r "^0.1.1") (d #t) (k 1)) (d (n "eyre") (r "^0.6") (d #t) (k 0)) (d (n "proptest") (r "^1.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^1.18") (f (quote ("fs" "io-util"))) (d #t) (k 0)) (d (n "tracing-test") (r "^0.2") (d #t) (k 2)) (d (n "yul") (r "^0.1.0") (d #t) (k 0)))) (h "0df9cmmrshhzq5fqdgvs524hh2rp0zpnfal1dxg7bjj520zaj110")))

