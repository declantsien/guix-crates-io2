(define-module (crates-io yu l- yul-parser) #:use-module (crates-io))

(define-public crate-yul-parser-0.1.1 (c (n "yul-parser") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3.3") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "logos") (r "^0.11.4") (d #t) (k 0)) (d (n "num-derive") (r "^0.3.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.14") (d #t) (k 0)) (d (n "rowan") (r "^0.10.0") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1rqw2jkjxxb746sl6mhk9d0r66kwjck9a0mwfl81k5qs5pcy9iss")))

