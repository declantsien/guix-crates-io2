(define-module (crates-io yu np yunpian-sdk) #:use-module (crates-io))

(define-public crate-yunpian-sdk-0.1.0 (c (n "yunpian-sdk") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "02ln9hpx8x3m9hn6lhc5qd2cfkws4q69zrv3i8l16igv40gqpdh7")))

(define-public crate-yunpian-sdk-0.1.1 (c (n "yunpian-sdk") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "1z9svysq5yf7j903rm6saf9fa4r6y36igp0san4c4j1f48f222nj")))

