(define-module (crates-io yu ns yunser-hello) #:use-module (crates-io))

(define-public crate-yunser-hello-0.0.1 (c (n "yunser-hello") (v "0.0.1") (h "19yfy0jdkc9asnd7q5nn1wzxk372c7npky3dn2mf233k2j8prqx9")))

(define-public crate-yunser-hello-0.0.2 (c (n "yunser-hello") (v "0.0.2") (h "1j8r8vghr4694n8dczaizzxdbrb4vnjn1i55fklwgcv8b7k0bg3z")))

