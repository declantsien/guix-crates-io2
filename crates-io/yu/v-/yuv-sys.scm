(define-module (crates-io yu v- yuv-sys) #:use-module (crates-io))

(define-public crate-yuv-sys-0.1.0 (c (n "yuv-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cmake") (r "^0.1") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "06yji71raly07liraq6nh53k0djvkwj00szyv6p5aavdchs3df4h")))

(define-public crate-yuv-sys-0.2.0 (c (n "yuv-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1p5narrhisc9ms1fqw7lwyd1ahyyx86pv5328w250wg6ppmdxi54")))

(define-public crate-yuv-sys-0.2.1 (c (n "yuv-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1m00dm4v7k2fspn0zcp3hyl1i8bicml61j4hl7jzmk1gn76sj1p3")))

(define-public crate-yuv-sys-0.3.0 (c (n "yuv-sys") (v "0.3.0") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0q4sj1avinlygy7ngiv2qv3g74hdilq2lzlvpnfjklq87cx1y747")))

(define-public crate-yuv-sys-0.3.1 (c (n "yuv-sys") (v "0.3.1") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1zk540q3zbrdl4hfi43l72b80gf01nmfmzr5lw0ig7s7jcd8m6hv")))

(define-public crate-yuv-sys-0.3.2 (c (n "yuv-sys") (v "0.3.2") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "16hcx0sy0p14f0v38w43nb15g75wcg6gcxby08hhr7yc16amw6dh")))

(define-public crate-yuv-sys-0.3.3 (c (n "yuv-sys") (v "0.3.3") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0ppdmsgnk70yjq785w78hby5s9kn86qcs5d742mcy6w282v6bmbi")))

(define-public crate-yuv-sys-0.3.4 (c (n "yuv-sys") (v "0.3.4") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1gd1zgjky7x2zg66q96rp4k0aq633k4ddfnn9ifzwpss3602c04r")))

(define-public crate-yuv-sys-0.3.5 (c (n "yuv-sys") (v "0.3.5") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0bn0h524n92k6iq3zf21f0cgndk1bxlcj18y4bv52b6ss0yw3pwl")))

(define-public crate-yuv-sys-0.3.6 (c (n "yuv-sys") (v "0.3.6") (d (list (d (n "bindgen") (r "^0.69") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "rayon") (r "^1.8") (d #t) (k 1)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0l3in0wzchn21hh3kr08527p81k01lg6xvldrlsm353cfy4zb8w4")))

