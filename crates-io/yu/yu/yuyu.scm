(define-module (crates-io yu yu yuyu) #:use-module (crates-io))

(define-public crate-yuyu-0.1.0 (c (n "yuyu") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "indicatif") (r "^0.15.0") (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "yuyu_core") (r "^0.1.0") (d #t) (k 0)))) (h "05bpb5sac93i846kn4srinhlzj6b57fc8z4j6afv8lxldfhyrhpi") (y #t)))

(define-public crate-yuyu-0.2.0 (c (n "yuyu") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.40") (d #t) (k 0)) (d (n "clap") (r "^2.33.3") (f (quote ("suggestions" "wrap_help"))) (k 0)) (d (n "indicatif") (r "^0.15.0") (f (quote ("improved_unicode" "rayon"))) (d #t) (k 0)) (d (n "tokio") (r "^1.5.0") (f (quote ("fs" "macros" "parking_lot" "rt-multi-thread"))) (d #t) (k 0)) (d (n "yuyu_core") (r "^0.2.0") (f (quote ("vendored-tls"))) (d #t) (k 0)))) (h "0y2k22c49r8yv35lzi98m2kdv9gg9ikp61n34x7w5rr95z746m11") (y #t)))

