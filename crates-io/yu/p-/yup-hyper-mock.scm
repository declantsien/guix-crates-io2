(define-module (crates-io yu p- yup-hyper-mock) #:use-module (crates-io))

(define-public crate-yup-hyper-mock-0.1.0 (c (n "yup-hyper-mock") (v "0.1.0") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1kfxh8fam4x14l9fvr848fg9cwz20fw38allcizlxmj65zvhp32x")))

(define-public crate-yup-hyper-mock-0.1.1 (c (n "yup-hyper-mock") (v "0.1.1") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1nb9z1719f0ma67fj7fhg0rpn21klpn2794hsrj5gbwbjk9d817x")))

(define-public crate-yup-hyper-mock-0.1.2 (c (n "yup-hyper-mock") (v "0.1.2") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1ijqkic0yvh19kax1i1g5glv4r96jsqdrriic9jhj8mdwj5ymsja")))

(define-public crate-yup-hyper-mock-0.1.3 (c (n "yup-hyper-mock") (v "0.1.3") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0fsc3q6ha4rln80c6jr9m0h537sjlr1zar5pjnb7a6ja1lnjmh39")))

(define-public crate-yup-hyper-mock-0.1.4 (c (n "yup-hyper-mock") (v "0.1.4") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0209jx2l445ckhgsqr50i2bijg37s6pcrvkjmsm6hs5w0mx9xhw7")))

(define-public crate-yup-hyper-mock-0.1.5 (c (n "yup-hyper-mock") (v "0.1.5") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1hdi8is7xl9pwywxc604jb8sxw905spmls461lc358g8lkxmsg7i")))

(define-public crate-yup-hyper-mock-0.1.6 (c (n "yup-hyper-mock") (v "0.1.6") (d (list (d (n "hyper") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0bkfr205chscxhvhxnsdijaw328yg34j6rjy1fs4wfzml3zafzzb")))

(define-public crate-yup-hyper-mock-0.1.8 (c (n "yup-hyper-mock") (v "0.1.8") (d (list (d (n "hyper") (r ">= 0.4.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "13dnx4y78g7f6fc9vsqd9689rk5vxh9qb18r1l7a8yv7x2l3bqj6")))

(define-public crate-yup-hyper-mock-1.0.0 (c (n "yup-hyper-mock") (v "1.0.0") (d (list (d (n "hyper") (r ">= 0.5.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1cfgwxvk16zsgqvcs9m90cjc4gswviqjdh3l2dj07cka6349jhga")))

(define-public crate-yup-hyper-mock-1.0.1 (c (n "yup-hyper-mock") (v "1.0.1") (d (list (d (n "hyper") (r ">= 0.5.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0xxfja07cdrb8zvlwndw0v3n7vjqhhpnhwvabgq4b6sa8lyqqkid")))

(define-public crate-yup-hyper-mock-1.1.0 (c (n "yup-hyper-mock") (v "1.1.0") (d (list (d (n "hyper") (r ">= 0.5.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1bhs8d0vykwzgazb3b5m9hfxhrmm8qsv5k387agrl0z8n92q8cha")))

(define-public crate-yup-hyper-mock-1.2.0 (c (n "yup-hyper-mock") (v "1.2.0") (d (list (d (n "hyper") (r ">= 0.5.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0jbgy0p5injq78ibki620vzzp88sgk23jpavr1cgzlxm0i2xwdf3")))

(define-public crate-yup-hyper-mock-1.3.0 (c (n "yup-hyper-mock") (v "1.3.0") (d (list (d (n "hyper") (r ">= 0.6.0") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "0qflc5077gdl48gk4rb1fb9dad1r13nl3fsnwkl8icmbdgzwahap")))

(define-public crate-yup-hyper-mock-1.3.1 (c (n "yup-hyper-mock") (v "1.3.1") (d (list (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)))) (h "1y8d822zril7qsws0kxp7znx7yl3305i531whsixmvxqs51bi45q")))

(define-public crate-yup-hyper-mock-1.3.2 (c (n "yup-hyper-mock") (v "1.3.2") (d (list (d (n "hyper") (r ">= 0.7") (d #t) (k 0)) (d (n "log") (r ">= 0.1") (d #t) (k 0)))) (h "1zr34fabzyzspwm9nxanr32l12kmk6a7cyipp35m74x77816vcgf")))

(define-public crate-yup-hyper-mock-1.3.3 (c (n "yup-hyper-mock") (v "1.3.3") (d (list (d (n "hyper") (r "^0.9.5") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)))) (h "0fmnf6ipdi205n86jnwhdknl3ycysfs2abnq959x4qmk5hhbayir")))

(define-public crate-yup-hyper-mock-2.0.0 (c (n "yup-hyper-mock") (v "2.0.0") (d (list (d (n "hyper") (r "^0.10") (d #t) (k 0)) (d (n "log") (r ">= 0.1") (d #t) (k 0)))) (h "1zj7xkis6wzaygx66v3rqlbgx5sxbawayz3iqnx0156v32dbw68v")))

(define-public crate-yup-hyper-mock-3.12.0 (c (n "yup-hyper-mock") (v "3.12.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r ">= 0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1.1") (d #t) (k 0)))) (h "0ky3cp77kwxfs3iqrhv3svip4jja1wxajg63gvicgmps21ljhvwj")))

(define-public crate-yup-hyper-mock-3.13.0 (c (n "yup-hyper-mock") (v "3.13.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r ">= 0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1.1") (d #t) (k 0)))) (h "1wxc0b48bmmx23vdj0w33l0qd7nk6q8gav7vww2fbq80b7dwfzc9")))

(define-public crate-yup-hyper-mock-3.14.0 (c (n "yup-hyper-mock") (v "3.14.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r ">= 0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1.1") (d #t) (k 0)))) (h "05nsccl6ii4h9ach18n1gjr7cz3ps5hzazyx8xs1xhy8fnfjf0n8")))

(define-public crate-yup-hyper-mock-3.15.0 (c (n "yup-hyper-mock") (v "3.15.0") (d (list (d (n "env_logger") (r "^0.5") (d #t) (k 2)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "hyper") (r "^0.12") (d #t) (k 0)) (d (n "log") (r ">= 0.2") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.11") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)) (d (n "tokio-reactor") (r "^0.1.1") (d #t) (k 0)))) (h "1iwajnkq50pg325ywm5gdl4s6v32cs6pq0qajs3783ijma8rs62w")))

(define-public crate-yup-hyper-mock-4.0.0 (c (n "yup-hyper-mock") (v "4.0.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.13.1") (d #t) (k 0)) (d (n "log") (r ">= 0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("io-driver" "io-util" "stream"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)))) (h "18ggyp9vn4y5pgj1dzs1zrrwlbhls3vaz5jalbvd99pw2zhhiarj")))

(define-public crate-yup-hyper-mock-5.14.0 (c (n "yup-hyper-mock") (v "5.14.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1" "runtime" "stream"))) (d #t) (k 0)) (d (n "log") (r ">=0.4") (d #t) (k 0)) (d (n "mio") (r "^0.6") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "15hxv4v67jbmsz8af69f0yb4r95x4karvss3ajls1w1vccq7pnh1")))

(define-public crate-yup-hyper-mock-5.14.1 (c (n "yup-hyper-mock") (v "5.14.1") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1" "runtime" "stream"))) (d #t) (k 0)) (d (n "log") (r ">=0.4") (d #t) (k 0)) (d (n "mio") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "10b5blsis8h66j44z9az0x7dm3mj8nvigd2j726njwzx2cxl1frh") (y #t)))

(define-public crate-yup-hyper-mock-6.0.0 (c (n "yup-hyper-mock") (v "6.0.0") (d (list (d (n "env_logger") (r "^0.7") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "hyper") (r "^0.14") (f (quote ("client" "http1" "runtime" "stream"))) (d #t) (k 0)) (d (n "log") (r ">=0.4") (d #t) (k 0)) (d (n "mio") (r "^0.8") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "1kjx5h0p8yxr7f3vwjmknxiw9kgl0h4299kiaq827wl3phcd79xx")))

(define-public crate-yup-hyper-mock-8.0.0 (c (n "yup-hyper-mock") (v "8.0.0") (d (list (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "http-body-util") (r "^0.1.0") (d #t) (k 0)) (d (n "hyper") (r "^1.1") (f (quote ("client" "http1"))) (d #t) (k 0)) (d (n "hyper-util") (r "^0.1.2") (f (quote ("tokio" "http1" "client-legacy"))) (d #t) (k 0)) (d (n "log") (r ">=0.4") (d #t) (k 0)) (d (n "tokio") (r "^1.35") (f (quote ("io-util"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("rt" "macros"))) (d #t) (k 2)) (d (n "tower-service") (r "^0.3") (d #t) (k 0)))) (h "0nyjwnjgs6l1q6kfymirvi30vv93xkf9dqvc2c2wx93v735bnnww")))

