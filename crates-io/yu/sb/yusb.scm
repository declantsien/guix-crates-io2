(define-module (crates-io yu sb yusb) #:use-module (crates-io))

(define-public crate-yusb-0.1.0 (c (n "yusb") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "usb-ids") (r "^1.2023.0") (d #t) (k 2)))) (h "1iv2imz5mvm335rliy0l328s72v1xkshm10a3zkljm4kx4makqls") (f (quote (("vendored" "libusb1-sys/vendored")))) (r "1.60")))

(define-public crate-yusb-0.1.1 (c (n "yusb") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.6.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "usb-ids") (r "^1.2023.0") (d #t) (k 2)))) (h "1j1zs50j9rcjmsmk2yviawahqcaid3phdrq7nm1wh4lh09ikjd5b") (f (quote (("vendored" "libusb1-sys/vendored")))) (r "1.60")))

(define-public crate-yusb-0.1.2 (c (n "yusb") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb1-sys") (r "^0.6") (d #t) (k 0)) (d (n "once_cell") (r "^1.18") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "usb-ids") (r "^1.2023.0") (d #t) (k 2)))) (h "1dv6zp55r7394saxrydxpxlz576hik6mjks47a1k4in2wwb9p7db") (f (quote (("vendored" "libusb1-sys/vendored")))) (r "1.60")))

