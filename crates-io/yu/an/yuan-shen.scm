(define-module (crates-io yu an yuan-shen) #:use-module (crates-io))

(define-public crate-yuan-shen-0.0.0 (c (n "yuan-shen") (v "0.0.0") (h "17xab90dfzla0y0lpxrkcmwmr20x30p10wcnn9k6ps9lhqmwa6xp") (f (quote (("default"))))))

(define-public crate-yuan-shen-0.0.1 (c (n "yuan-shen") (v "0.0.1") (d (list (d (n "clap") (r "^4.2.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "clap_builder") (r "^4.5.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.115") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "ys-core") (r "0.0.*") (d #t) (k 0)))) (h "0pry1b3a6dzgvjf3n3gqchbpnmqrgfvjfypbyyk5mxpjrhi6f4pv") (f (quote (("default"))))))

