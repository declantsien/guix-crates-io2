(define-module (crates-io yu bi yubibomb) #:use-module (crates-io))

(define-public crate-yubibomb-0.0.0 (c (n "yubibomb") (v "0.0.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0wh9g9s96kxwc93an4s1g5l7vcvvsvkfs8syc7sp8zwfwlz8inb2")))

(define-public crate-yubibomb-0.1.0 (c (n "yubibomb") (v "0.1.0") (d (list (d (n "rand") (r "^0.3.0") (d #t) (k 0)))) (h "0pwbjmpbqbrjhn6j21r2rca9m960g6l7zjxjc0kajykca2f7km23")))

(define-public crate-yubibomb-0.2.0 (c (n "yubibomb") (v "0.2.0") (d (list (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "0zpx0gkv9grvahq24zvk5qfsx8ggp22bjwwgkpi62lnb0992c0vh")))

(define-public crate-yubibomb-0.2.1 (c (n "yubibomb") (v "0.2.1") (d (list (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0ij5higmhc5wxf3imk86655lzz87psshbivqic7rny2y7w0gbpss")))

(define-public crate-yubibomb-0.2.2 (c (n "yubibomb") (v "0.2.2") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0c7ilrrsp1bx45qz25z0v67fnbwmz7frf2kfabgcid0q6crw3ghz")))

(define-public crate-yubibomb-0.2.3 (c (n "yubibomb") (v "0.2.3") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1xz44ag986q35x6z7mx39n0axn8yspva2vnr738kn99lsi5khk14")))

(define-public crate-yubibomb-0.2.4 (c (n "yubibomb") (v "0.2.4") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "092cdwcyzp80sg91di0byz83mximqhyiq5bap3phldrsyl0fyhnb")))

(define-public crate-yubibomb-0.2.5 (c (n "yubibomb") (v "0.2.5") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0ndslsjdcfiy5l5s6b07sprfk23pw6jx4sj2i8fgxk0w8wkisjjm")))

(define-public crate-yubibomb-0.2.6 (c (n "yubibomb") (v "0.2.6") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0xg7kl99y7q679cnb71klwxh2icma7xd6n9m7awh8n631xyji24g")))

(define-public crate-yubibomb-0.2.7 (c (n "yubibomb") (v "0.2.7") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0mx8rdx1k4z65636a1rs3b2vpngf9yk7rn2f53p28db7c5sga2i4")))

(define-public crate-yubibomb-0.2.8 (c (n "yubibomb") (v "0.2.8") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1hiij96c77xbmq5dka51kn753vanky5q0bj7ibd5qrvmsyn3j3zy")))

(define-public crate-yubibomb-0.2.9 (c (n "yubibomb") (v "0.2.9") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0mdrp9dhqnmgxw0yn58a70vsz5nnschfvbbl8065nhhpb9p7wqd9")))

(define-public crate-yubibomb-0.2.10 (c (n "yubibomb") (v "0.2.10") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1xg54wznwndb1flpxbhljfhs4p7wp494323iqd4ymg932c4z7g53")))

(define-public crate-yubibomb-0.2.11 (c (n "yubibomb") (v "0.2.11") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "03mn1qsbffxksw996axapk20ylm556a3s13wb6yzh3p1fiph9shp")))

(define-public crate-yubibomb-0.2.12 (c (n "yubibomb") (v "0.2.12") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "10nj6j6n4n36wp8anl378ah0xxj8lr226ngsvax7vyy2z8y3gr2v")))

(define-public crate-yubibomb-0.2.13 (c (n "yubibomb") (v "0.2.13") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "0jjansc49i6sw9yyh89xyfvgv8a24x9llaapjpda9aqf8z4k1z23") (r "1.65")))

(define-public crate-yubibomb-0.2.14 (c (n "yubibomb") (v "0.2.14") (d (list (d (n "rand") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 2)))) (h "1d3314rc6yhmk4yqlgr6cf5y44czqz1r1c5y7z9b03d96s64r1lx") (r "1.65")))

