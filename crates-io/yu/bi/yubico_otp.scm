(define-module (crates-io yu bi yubico_otp) #:use-module (crates-io))

(define-public crate-yubico_otp-0.1.0 (c (n "yubico_otp") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.2") (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (o #t) (d #t) (k 0)) (d (n "dotenvy") (r "^0.15.7") (d #t) (k 2)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)) (d (n "tokio") (r "^1.29.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.29.0") (f (quote ("rt" "macros" "rt-multi-thread"))) (d #t) (k 2)))) (h "0x720b5s3kaqsnnl4r48a6v6y38c045syn5pwz28848bi548kv46") (f (quote (("bin" "tokio" "dotenvy"))))))

