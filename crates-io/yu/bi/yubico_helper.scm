(define-module (crates-io yu bi yubico_helper) #:use-module (crates-io))

(define-public crate-yubico_helper-1.0.1 (c (n "yubico_helper") (v "1.0.1") (d (list (d (n "clap") (r "^4.1.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libusb") (r "^0.3.0") (d #t) (k 0)) (d (n "passwd-rs") (r "^0.2.0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yx2v1hbsjkqk2da23dxbqayvg4rbkz85ms8y9l3dhlgskbynr5y")))

