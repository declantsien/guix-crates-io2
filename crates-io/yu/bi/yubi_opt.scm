(define-module (crates-io yu bi yubi_opt) #:use-module (crates-io))

(define-public crate-yubi_opt-0.1.0 (c (n "yubi_opt") (v "0.1.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "1whby9h9y1g9g045rpgyx29s4n9h0d6yvlr7v0vxipa4ag3xf8am")))

(define-public crate-yubi_opt-0.2.0 (c (n "yubi_opt") (v "0.2.0") (d (list (d (n "base64") (r "^0.21.0") (d #t) (k 0)) (d (n "derive_builder") (r "^0.12.0") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "sha1") (r "^0.10.5") (d #t) (k 0)) (d (n "ureq") (r "^2.6.2") (d #t) (k 0)))) (h "09xvpxy2ddk3rc27l77n18npmg40fldp3sxshw3zmngq2acmaa9b")))

