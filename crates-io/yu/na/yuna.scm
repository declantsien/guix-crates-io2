(define-module (crates-io yu na yuna) #:use-module (crates-io))

(define-public crate-yuna-0.1.0 (c (n "yuna") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "comfy-table") (r "^5.0.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.0") (d #t) (k 0)) (d (n "tempera") (r "^0.1.0") (d #t) (k 0)))) (h "07fjjc1l96rvr3pk3wl3c147j7l90xzd16pybbbjdxv144jzx3xz")))

(define-public crate-yuna-0.2.0 (c (n "yuna") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "comfy-table") (r "^5.0.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.0") (d #t) (k 0)) (d (n "tempera") (r "^0.1.0") (d #t) (k 0)))) (h "0fydlyk86b38ppv32bpswpqf71nkn7j46m727723ga6k3jlj1ish")))

(define-public crate-yuna-0.2.1 (c (n "yuna") (v "0.2.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "comfy-table") (r "^5.0.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.0") (d #t) (k 0)) (d (n "tempera") (r "^0.1.0") (d #t) (k 0)))) (h "15kz97d71drcrgxk8h1b8wgfyan3822xkr1gbs3ay1wsrrnsqkz3")))

(define-public crate-yuna-0.2.2 (c (n "yuna") (v "0.2.2") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "comfy-table") (r "^5.0.0") (d #t) (k 0)) (d (n "handlebars") (r "^4.1.4") (d #t) (k 0)) (d (n "serde") (r "^1.0.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.0") (d #t) (k 0)) (d (n "tempera") (r "^0.1.0") (d #t) (k 0)))) (h "125jy1yjxz6mq2s5711acfh5nlli3yx4k6dvc3ckywlzcdwkrnc1")))

