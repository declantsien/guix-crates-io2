(define-module (crates-io yu mr yumrepo) #:use-module (crates-io))

(define-public crate-yumrepo-0.0.1 (c (n "yumrepo") (v "0.0.1") (d (list (d (n "serde") (r "^1") (f (quote ("derive"))) (k 0)) (d (n "syaml") (r "^0.8") (k 0) (p "serde_yaml")) (d (n "xml") (r "^0.17.1") (f (quote ("serialize"))) (k 0) (p "quick-xml")))) (h "1jcac95s9id1m3i1aikm4i0xjl5xn34mgc9bfdjcqwd2nyy83s8l")))

