(define-module (crates-io yu ex yuexclusive_foo) #:use-module (crates-io))

(define-public crate-yuexclusive_foo-0.1.0 (c (n "yuexclusive_foo") (v "0.1.0") (h "0ql396hr0spv4xbbx3y0cvbc4n7f10mvk662kxfp98xfgxf9pz2a") (y #t)))

(define-public crate-yuexclusive_foo-0.1.1 (c (n "yuexclusive_foo") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "04ka7c5riznmk8kchs3awklsrg85m0szc9axiv96skdgji0b7263") (y #t)))

(define-public crate-yuexclusive_foo-0.1.2 (c (n "yuexclusive_foo") (v "0.1.2") (h "1kld921yix8lqa636zajhg02sbg86k5y1ighkb3j7pg0yc94094m") (y #t)))

(define-public crate-yuexclusive_foo-0.1.3 (c (n "yuexclusive_foo") (v "0.1.3") (d (list (d (n "regex") (r "~1.6") (d #t) (k 0)))) (h "1k6yi348lrnzznf3i9z45ixrai7y2wlxh8fxdprqnpznf4xqa0v7") (y #t)))

(define-public crate-yuexclusive_foo-0.1.4 (c (n "yuexclusive_foo") (v "0.1.4") (d (list (d (n "regex") (r "~1.6") (d #t) (k 0)))) (h "1pjpcrg67hpr6kh6hmapvp3lw96l3m7qihmqm7ycwmhiq3iqbi16") (y #t)))

(define-public crate-yuexclusive_foo-0.1.5 (c (n "yuexclusive_foo") (v "0.1.5") (d (list (d (n "regex") (r "~1.6") (d #t) (k 0)))) (h "0xq011alrkq4vh9zhl525n6snqsivzx9kr4rqf4cnx886gnc0ls7")))

(define-public crate-yuexclusive_foo-0.1.6 (c (n "yuexclusive_foo") (v "0.1.6") (d (list (d (n "regex") (r "~1.6") (d #t) (k 0)))) (h "107sayv48xsq377jam74dvnr0c9lgsg4bsx61ssphd9qp3dwbxp5") (r "1.56")))

(define-public crate-yuexclusive_foo-0.1.7 (c (n "yuexclusive_foo") (v "0.1.7") (d (list (d (n "regex") (r "~1.6") (d #t) (k 0)))) (h "1nsnk7p5h0b6hpdl6hkvwfqp2n512zc665a734dndqca2ihlwwn6") (r "1.56")))

(define-public crate-yuexclusive_foo-0.1.8 (c (n "yuexclusive_foo") (v "0.1.8") (h "19ygnihm4f57ydy0mp77927xpx28fl95g8d6bvzx8znck5vdf5mv")))

