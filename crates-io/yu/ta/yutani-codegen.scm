(define-module (crates-io yu ta yutani-codegen) #:use-module (crates-io))

(define-public crate-yutani-codegen-0.0.0 (c (n "yutani-codegen") (v "0.0.0") (d (list (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "1sqbyv3x11qk258l9vp63nnq74xsbasa4c711j3v473pcn4f5gk7")))

