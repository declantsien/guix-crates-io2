(define-module (crates-io yu ge yugen) #:use-module (crates-io))

(define-public crate-yugen-0.1.0 (c (n "yugen") (v "0.1.0") (h "0m2jbkxk2zk7f78b5ncl2f0i16y8m9qvvsfipycccdz7hm0gaqx0")))

(define-public crate-yugen-0.1.1 (c (n "yugen") (v "0.1.1") (h "0v069aln63f8vhc0x7w2l416hnffwdczn7x0a7lm73bbm708h7k2")))

(define-public crate-yugen-0.1.2 (c (n "yugen") (v "0.1.2") (h "0qpq2dd81dwsja69m46d2anv6b287lkagh7cilnafn3s2l4kmrqs")))

(define-public crate-yugen-0.1.3 (c (n "yugen") (v "0.1.3") (h "0mgvk9msf65vk45jwz5y976sp9mb89d09yfl9d0rfjh39pd2n43m")))

(define-public crate-yugen-0.1.4 (c (n "yugen") (v "0.1.4") (h "1k4pjgpqb417qycncxr8nq9fyc37849dwbgxygrf9cisl7jsy90f")))

(define-public crate-yugen-0.1.5 (c (n "yugen") (v "0.1.5") (h "0pxx6v57np7afkvy5bj9w47nqliy6gjl4d4fw3m5lkjri4bzshw2")))

(define-public crate-yugen-0.1.6 (c (n "yugen") (v "0.1.6") (h "0nbdrh0a4p9shqixzp3z79p3n2hfqj13wn4b714si17jk4zm14dz")))

(define-public crate-yugen-0.1.7 (c (n "yugen") (v "0.1.7") (h "1m365cnkv0887jwxq7simqmgg946x23g1r5jsmi4zjrkb3x86060")))

(define-public crate-yugen-0.1.8 (c (n "yugen") (v "0.1.8") (h "0qvnljxw8p6671i0ig1y6fxgan3zcyjvqnky97fnqiv09654710l")))

(define-public crate-yugen-0.1.9 (c (n "yugen") (v "0.1.9") (h "0p68jjki9c0khll0xz12jv46kcq3gm8ldyyqcl8kh2ckh8022mnb")))

