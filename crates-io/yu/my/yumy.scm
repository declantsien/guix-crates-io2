(define-module (crates-io yu my yumy) #:use-module (crates-io))

(define-public crate-yumy-0.1.0 (c (n "yumy") (v "0.1.0") (d (list (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "nonmax") (r "^0.5") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1bfk0q2g4klalj0hjg5vv95lbzb6jwsq85g810045if1f0ar4yfa")))

(define-public crate-yumy-0.1.1 (c (n "yumy") (v "0.1.1") (d (list (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "nonmax") (r "^0.5") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "036xdwy7mx2q7b03va5ah5sbdz2rfbc0j244s7i43zbschl63bb4")))

(define-public crate-yumy-0.2.0 (c (n "yumy") (v "0.2.0") (d (list (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "nonmax") (r "^0.5") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1f71svb31d1yfigvv6xrvg1lsyf8g4wkmg5m8pilq32ixqqfbqzi")))

(define-public crate-yumy-0.2.1 (c (n "yumy") (v "0.2.1") (d (list (d (n "either") (r "^1.8") (d #t) (k 0)) (d (n "nonmax") (r "^0.5") (d #t) (k 0)) (d (n "owo-colors") (r "^3.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "1l666jbpjbqqxanprv0mz1n45yk0mllqb2iirqmvw8v1hbc9wlfg")))

(define-public crate-yumy-0.3.0 (c (n "yumy") (v "0.3.0") (d (list (d (n "ansi-str") (r "^0.8") (d #t) (k 0)) (d (n "anstyle-roff") (r "^0.3") (d #t) (k 0)) (d (n "insta") (r "^1.34") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "nonmax") (r "^0.5") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "0s4hf31p0nlp80nr9npy1ywz3ljsnzdkayiy0mcpzxvw0dpy8vfb")))

(define-public crate-yumy-0.3.1 (c (n "yumy") (v "0.3.1") (d (list (d (n "ansi-str") (r "^0.8") (d #t) (k 0)) (d (n "insta") (r "^1.34") (f (quote ("yaml"))) (d #t) (k 2)) (d (n "nonmax") (r "^0.5") (d #t) (k 0)) (d (n "owo-colors") (r "^4.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.10") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1") (d #t) (k 0)))) (h "02zjs6fs9a098mxiq49vqi1ydvh4sfcvqn4kpjq33kff3p73mh5z")))

