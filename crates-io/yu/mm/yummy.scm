(define-module (crates-io yu mm yummy) #:use-module (crates-io))

(define-public crate-yummy-0.0.8 (c (n "yummy") (v "0.0.8") (d (list (d (n "clap") (r "^4.2.1") (d #t) (k 0)) (d (n "tokio") (r "^1.20.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "yummy-delta") (r "^0.0.8") (o #t) (d #t) (k 0)))) (h "0pmkgq0x09xs29mk1cgykh1ipawr6ya1qlz7x6rzjcjzqgwdwj1b") (s 2) (e (quote (("yummy-delta" "dep:yummy-delta"))))))

