(define-module (crates-io yu v2 yuv2rgb) #:use-module (crates-io))

(define-public crate-yuv2rgb-0.0.1 (c (n "yuv2rgb") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1yxprpbp5zgjps73qiwnagwxxxl5yw9xi0dgqlbph4xs3gm3mjh4")))

(define-public crate-yuv2rgb-0.0.2 (c (n "yuv2rgb") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (k 0)) (d (n "num-traits") (r "^0.2") (d #t) (k 0)))) (h "1vf67p043db4pg85mfjif8nvgjl6acnag9arbw79gzwaw0szp6ad")))

