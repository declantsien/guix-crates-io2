(define-module (crates-io zk p- zkp-error-utils) #:use-module (crates-io))

(define-public crate-zkp-error-utils-0.1.0 (c (n "zkp-error-utils") (v "0.1.0") (h "1z5qs8hk05j2azca5p215jsmj3glvkvqkrf3zr1y8dv3mria20jy") (f (quote (("std") ("default" "std"))))))

(define-public crate-zkp-error-utils-0.2.0 (c (n "zkp-error-utils") (v "0.2.0") (h "0kcfhrw4msdn2n9nld2dn40mlms8w5ikn5niw3vg4g5xgmr301dm") (f (quote (("std") ("default" "std"))))))

