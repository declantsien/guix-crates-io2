(define-module (crates-io zk p- zkp-macros-impl) #:use-module (crates-io))

(define-public crate-zkp-macros-impl-0.1.0 (c (n "zkp-macros-impl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "zkp-macros-lib") (r "^0.1.0") (d #t) (k 0)))) (h "1gglrrl3ciizh061s316239giyrhai255vc90y59qg0sjr36wzdd") (f (quote (("std") ("default" "std"))))))

(define-public crate-zkp-macros-impl-0.2.0 (c (n "zkp-macros-impl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5.8") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "zkp-macros-lib") (r "^0.2.0") (d #t) (k 0)))) (h "09qpasm0qgwh4dz4pgihc7604p9q2kd40p7a8nmi0qcw0r8lgidp") (f (quote (("std") ("default" "std"))))))

