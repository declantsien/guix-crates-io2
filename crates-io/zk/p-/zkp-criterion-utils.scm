(define-module (crates-io zk p- zkp-criterion-utils) #:use-module (crates-io))

(define-public crate-zkp-criterion-utils-0.1.0 (c (n "zkp-criterion-utils") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1fd8aaz9s6qh92ancd81nhqav6qv3x8glab67nb3nkp3mi0jd3q0") (f (quote (("std") ("default" "std"))))))

(define-public crate-zkp-criterion-utils-0.2.0 (c (n "zkp-criterion-utils") (v "0.2.0") (d (list (d (n "criterion") (r "^0.3.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "num_cpus") (r "^1.10.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "0qjqqg0b41sgxl2rslaj802h55l91k5r15jx3myxl5813cbb8xf4") (f (quote (("std") ("default" "std"))))))

