(define-module (crates-io zk p- zkp-macros-decl) #:use-module (crates-io))

(define-public crate-zkp-macros-decl-0.1.0 (c (n "zkp-macros-decl") (v "0.1.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "zkp-macros-impl") (r "^0.1.0") (d #t) (k 0)))) (h "0m3a2404zd6hdgp5v0s26cm0h6igyg8cgcankgny4akiv80ki6d3") (f (quote (("std") ("default" "std"))))))

(define-public crate-zkp-macros-decl-0.2.0 (c (n "zkp-macros-decl") (v "0.2.0") (d (list (d (n "proc-macro-hack") (r "^0.5") (d #t) (k 0)) (d (n "zkp-macros-impl") (r "^0.2.0") (d #t) (k 0)))) (h "0bgq73g4lj0wc3cjz0bpj341iamylp10dc7gdrr6gm9p6ncillsd") (f (quote (("std") ("default" "std"))))))

