(define-module (crates-io zk p- zkp-mmap-vec) #:use-module (crates-io))

(define-public crate-zkp-mmap-vec-0.1.0 (c (n "zkp-mmap-vec") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "no-std-compat") (r "^0.1.0") (f (quote ("alloc"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (o #t) (d #t) (k 0)))) (h "1ml2hdagn4xrv69kdyxgbxcs9v5l0281xkxkk35xwv50dfk32nrh") (f (quote (("std" "no-std-compat/std" "log/std" "memmap" "tempfile") ("default" "std"))))))

(define-public crate-zkp-mmap-vec-0.2.0 (c (n "zkp-mmap-vec") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (k 0)) (d (n "memmap") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "no-std-compat") (r "^0.4.0") (f (quote ("alloc"))) (d #t) (k 0)))) (h "1ix1xbih0z4x1654r61d3rmwvgasj6qa960xajyiic7dlw86jn2h") (f (quote (("std" "no-std-compat/std" "log/std" "memmap") ("default" "std"))))))

