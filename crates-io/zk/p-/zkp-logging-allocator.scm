(define-module (crates-io zk p- zkp-logging-allocator) #:use-module (crates-io))

(define-public crate-zkp-logging-allocator-0.1.0 (c (n "zkp-logging-allocator") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (k 0)))) (h "0qb5qxk2ph4kdwsnmdvz508711lc4af30brgfjxf5m0r89x26bd3") (f (quote (("std" "log/std") ("enable") ("default" "std" "enable"))))))

(define-public crate-zkp-logging-allocator-0.2.0 (c (n "zkp-logging-allocator") (v "0.2.0") (d (list (d (n "log") (r "^0.4.8") (k 0)))) (h "1vqd6fgw1531d9c8dfrmrdh1xq4n87nvwbfa6ay26l4fhp0y2yhx") (f (quote (("std" "log/std") ("enable") ("default" "std" "enable"))))))

