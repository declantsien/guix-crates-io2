(define-module (crates-io zk p- zkp-macros-lib) #:use-module (crates-io))

(define-public crate-zkp-macros-lib-0.1.0 (c (n "zkp-macros-lib") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0cw5ammwyrkwswxh2wagynlsr3yl1jjplhhqgfym1vxgi042aydb") (f (quote (("std") ("default" "std"))))))

(define-public crate-zkp-macros-lib-0.2.0 (c (n "zkp-macros-lib") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.0") (k 0)) (d (n "proc-macro2") (r "^1.0.9") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02s0xh0jchm0fhg61bdjqy8f8zsjvb096pwla2sifp6lpdkvfr55") (f (quote (("std") ("default" "std"))))))

