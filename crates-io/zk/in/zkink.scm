(define-module (crates-io zk in zkink) #:use-module (crates-io))

(define-public crate-zkink-0.0.1 (c (n "zkink") (v "0.0.1") (d (list (d (n "ink_env") (r "^3.0.0-rc8") (k 0)) (d (n "ink_lang") (r "^3.0.0-rc8") (k 0)) (d (n "ink_metadata") (r "^3.0.0-rc8") (f (quote ("derive"))) (o #t) (k 0)) (d (n "ink_primitives") (r "^3.0.0-rc8") (k 0)) (d (n "ink_storage") (r "^3.0.0-rc8") (k 0)) (d (n "scale") (r "^3") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "scale-info") (r "^2") (f (quote ("derive"))) (o #t) (k 0)))) (h "10qxinny3jnd1h2q7m32mdks9z8ykqzd5vrhw1rna46iy9zr6sps") (f (quote (("std" "ink_metadata/std" "ink_env/std" "ink_storage/std" "ink_primitives/std" "scale/std" "scale-info/std") ("ink-as-dependency") ("default" "std")))) (r "1.56.1")))

