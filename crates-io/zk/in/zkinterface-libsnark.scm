(define-module (crates-io zk in zkinterface-libsnark) #:use-module (crates-io))

(define-public crate-zkinterface-libsnark-1.1.2 (c (n "zkinterface-libsnark") (v "1.1.2") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "zkinterface") (r "^1.1.2") (d #t) (k 0)))) (h "0j76991kmni13f0a51ka2zrqxmkrz60rrgv57h2r23m6c8qxjzqw") (l "zkif_gadgetlib")))

(define-public crate-zkinterface-libsnark-1.1.3 (c (n "zkinterface-libsnark") (v "1.1.3") (d (list (d (n "cmake") (r "^0.1.44") (d #t) (k 1)) (d (n "zkinterface") (r "^1.1.3") (d #t) (k 0)))) (h "00i0s6l2ri719hvphka5974qm18s6vpf289z2myswc2rfdmly5q1") (l "zkif_gadgetlib")))

