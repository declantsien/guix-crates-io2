(define-module (crates-io zk -l zk-loader) #:use-module (crates-io))

(define-public crate-zk-loader-0.1.0 (c (n "zk-loader") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "zookeeper") (r "^0.5") (d #t) (k 0)))) (h "0z1k3gzwkvbas728inaa6c4rg4cnxgr7wh429axkjnkfxzlszhid")))

(define-public crate-zk-loader-0.1.1 (c (n "zk-loader") (v "0.1.1") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "zookeeper") (r "^0.5") (d #t) (k 0)))) (h "1igh7ravzyp3mmdf115a6m3p497szzil3ic4xgw0n1qbdbdi8cv4")))

(define-public crate-zk-loader-0.1.2 (c (n "zk-loader") (v "0.1.2") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "zookeeper") (r "^0.5") (d #t) (k 0)))) (h "19yxbhkl1mzlpxzm0ljwi2vaym1sssrs71c9qq5f5lwcq7a7nydg")))

(define-public crate-zk-loader-0.2.0 (c (n "zk-loader") (v "0.2.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "serial_test") (r "^0.5.1") (d #t) (k 0)) (d (n "tar") (r "^0.4") (d #t) (k 0)) (d (n "zookeeper") (r "^0.5") (d #t) (k 0)))) (h "0dadfnh44dxkakixm7480sm8gmf06a34srcf908d82f739hr4iy1")))

