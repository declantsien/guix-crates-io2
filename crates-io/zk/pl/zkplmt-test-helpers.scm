(define-module (crates-io zk pl zkplmt-test-helpers) #:use-module (crates-io))

(define-public crate-zkplmt-test-helpers-0.4.5 (c (n "zkplmt-test-helpers") (v "0.4.5") (d (list (d (n "proptest") (r "^0.10") (d #t) (k 0)) (d (n "rand") (r "^0.7") (f (quote ("getrandom"))) (k 0)) (d (n "zkplmt") (r "^0.4.3") (f (quote ("test-helpers"))) (d #t) (k 0)))) (h "01j31ff1j1zrnf6p9cxfzgxz2mwrv1xq0mky307qkl4454wrgql8")))

