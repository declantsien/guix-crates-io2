(define-module (crates-io zk ra zkraken-lib) #:use-module (crates-io))

(define-public crate-zkraken-lib-0.1.0 (c (n "zkraken-lib") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "hidapi") (r "^1.3.4") (d #t) (k 0)))) (h "01dfd5fwnj926spi7p0xjikq6rambyds34821yhxy39n495cvr08")))

(define-public crate-zkraken-lib-0.2.0-alpha (c (n "zkraken-lib") (v "0.2.0-alpha") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "hidapi-rusb") (r "^1.3.0") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "16iih0sxal25wp5z1mmmds8z3q13a0cm1dxyz1605znc09751qnq")))

(define-public crate-zkraken-lib-0.2.0 (c (n "zkraken-lib") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "rusb") (r "^0.9") (d #t) (k 0)))) (h "1mppj375mm4sm75jfa4npma28qwsfhbglj5xg563snh96sn8lkj6")))

