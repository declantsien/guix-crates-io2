(define-module (crates-io zk me zkmemory) #:use-module (crates-io))

(define-public crate-zkmemory-0.1.0 (c (n "zkmemory") (v "0.1.0") (h "1c982nwn3nc5kdxv6133521d11k8kpc2hk1w9sfl34rqqgi7gc96")))

(define-public crate-zkmemory-0.1.1 (c (n "zkmemory") (v "0.1.1") (d (list (d (n "cargo-llvm-cov") (r "^0.5.27") (d #t) (k 0)) (d (n "ethnum") (r "^1.3.2") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "rbtree") (r "^0.1.5") (d #t) (k 0)))) (h "1mq0gm9bj2wm7qs6jw8720pvbpz2xc5knqzw224yh42d9s6xv3ra")))

