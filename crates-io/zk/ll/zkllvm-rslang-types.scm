(define-module (crates-io zk ll zkllvm-rslang-types) #:use-module (crates-io))

(define-public crate-zkllvm-rslang-types-0.1.0 (c (n "zkllvm-rslang-types") (v "0.1.0") (d (list (d (n "ark-ff") (r "^0.4.0-alpha") (o #t) (k 0)) (d (n "ark-serialize") (r "^0.4.0-alpha") (o #t) (k 0)) (d (n "ark-std") (r "^0.4.0-alpha") (o #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (o #t) (d #t) (k 0)) (d (n "zeroize") (r "^1") (o #t) (d #t) (k 0)))) (h "0lyw2sbi205hpmw2ab5n04bh1db2828npang1f5j9mz8n1namp86") (f (quote (("iter") ("int-conversions") ("hash") ("default")))) (y #t) (s 2) (e (quote (("zeroize" "dep:zeroize") ("num-traits" "dep:num-traits") ("arkworks" "dep:ark-std" "dep:ark-ff" "dep:ark-serialize" "hash" "int-conversions" "num-traits" "iter" "zeroize"))))))

