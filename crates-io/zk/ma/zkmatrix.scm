(define-module (crates-io zk ma zkmatrix) #:use-module (crates-io))

(define-public crate-zkmatrix-0.1.0 (c (n "zkmatrix") (v "0.1.0") (h "1j044rjixg27vd57v6q5c9hc17bl9rpxhyrsr2x0yc3hg7kvmrcd")))

(define-public crate-zkmatrix-0.1.1 (c (n "zkmatrix") (v "0.1.1") (d (list (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "bls12_381") (r "^0.8.0") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "curv-kzen") (r "^0.10.0") (f (quote ("num-bigint"))) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "ndarray") (r "^0.15.3") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "sha2") (r "^0.10.8") (d #t) (k 0)))) (h "11649br8pl8m0rc6jlbn939pamyqpm6m9d9qz78rxl96qdyjcgq2")))

