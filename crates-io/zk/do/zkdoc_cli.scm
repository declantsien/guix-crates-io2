(define-module (crates-io zk do zkdoc_cli) #:use-module (crates-io))

(define-public crate-zkdoc_cli-0.0.0 (c (n "zkdoc_cli") (v "0.0.0") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "zkdoc_sdk") (r "^0.0.0") (d #t) (k 0)))) (h "1gkbiqx8lny879qih21zcilqyrc9dpx0p7b9q0c686pmp4y4bis3")))

(define-public crate-zkdoc_cli-0.0.1 (c (n "zkdoc_cli") (v "0.0.1") (d (list (d (n "clap") (r "^4.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.117") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.59") (d #t) (k 0)) (d (n "spinners") (r "^4.1.0") (d #t) (k 0)) (d (n "zkdoc_sdk") (r "^0.0.1") (d #t) (k 0)))) (h "167j7dcigy20ndxv1mgq9v11clz3cs1lpfkzywh2mz7i1i8mwxig")))

