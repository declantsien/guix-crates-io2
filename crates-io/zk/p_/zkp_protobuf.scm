(define-module (crates-io zk p_ zkp_protobuf) #:use-module (crates-io))

(define-public crate-zkp_protobuf-0.1.0 (c (n "zkp_protobuf") (v "0.1.0") (d (list (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "tonic-build") (r "^0.10.2") (d #t) (k 1)))) (h "1487l9sv7abqkd331m957ws32m9pdz9jzdc2yc50dnv0i1zy2083")))

