(define-module (crates-io zk p_ zkp_grpc_client) #:use-module (crates-io))

(define-public crate-zkp_grpc_client-0.1.0 (c (n "zkp_grpc_client") (v "0.1.0") (d (list (d (n "chaum_pedersen_auth") (r "^0.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "zkp_protobuf") (r "^0.1.0") (d #t) (k 0)))) (h "0h1q2y29g7q7yls0k4vcpnlfgc8gvalbn8m5rr52mg8s62a4f261")))

