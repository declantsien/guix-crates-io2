(define-module (crates-io zk p_ zkp_grpc_server) #:use-module (crates-io))

(define-public crate-zkp_grpc_server-0.1.0 (c (n "zkp_grpc_server") (v "0.1.0") (d (list (d (n "chaum_pedersen_auth") (r "^0.1.0") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4") (f (quote ("rand"))) (d #t) (k 0)) (d (n "prost") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)) (d (n "tonic") (r "^0.10.2") (d #t) (k 0)) (d (n "zkp_protobuf") (r "^0.1.0") (d #t) (k 0)))) (h "1vzdnhh6dg2pz1h023d4rvpxb3sms989xzr44lfa5w4i2pnwrm06")))

