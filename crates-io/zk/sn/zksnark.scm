(define-module (crates-io zk sn zksnark) #:use-module (crates-io))

(define-public crate-zksnark-0.0.1 (c (n "zksnark") (v "0.0.1") (d (list (d (n "bigint") (r "^4.4.0") (d #t) (k 0)) (d (n "bn") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)))) (h "0h83kpg8y5plpy49rrplbc11ib71sq4d6c9lzrz6wrhlpmkryl46")))

(define-public crate-zksnark-0.0.2 (c (n "zksnark") (v "0.0.2") (d (list (d (n "bigint") (r "^4.4.0") (d #t) (k 0)) (d (n "bn") (r "^0.4.3") (d #t) (k 0)) (d (n "itertools") (r "^0.6.0") (d #t) (k 0)) (d (n "quickcheck") (r "^0.7.1") (d #t) (k 2)) (d (n "rand") (r "~0.3.14") (d #t) (k 0)))) (h "1fg8aqqk7q8qjnm3xqnkag33q2gx0mnqfm4fh6fx7ycbn14swfng")))

