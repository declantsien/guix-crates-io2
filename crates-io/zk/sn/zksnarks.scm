(define-module (crates-io zk sn zksnarks) #:use-module (crates-io))

(define-public crate-zksnarks-0.0.9 (c (n "zksnarks") (v "0.0.9") (h "1i3rl3b08nx4xi9k5gn5pb10b4g9qpg565yjampxb0zcizcdcyk0")))

(define-public crate-zksnarks-0.0.1 (c (n "zksnarks") (v "0.0.1") (d (list (d (n "bls-12-381") (r "^0.0.23") (k 0)) (d (n "ec-pairing") (r "^0.0.14") (k 0)) (d (n "jub-jub") (r "^0.0.20") (k 0)) (d (n "merlin") (r "^3.0") (k 0)) (d (n "parity-scale-codec") (r "^2.0.0") (f (quote ("derive"))) (k 0)) (d (n "poly-commit") (r "^0.0.13") (k 0)) (d (n "rand") (r "^0.8.5") (f (quote ("getrandom"))) (k 0)) (d (n "zkstd") (r "^0.0.22") (k 0)))) (h "0d94ab621qcz8ginj0l6vy7ssdw4lrlg7h1d7n6jqpmq8gfdis4w")))

