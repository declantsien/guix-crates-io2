(define-module (crates-io ja ru jaru) #:use-module (crates-io))

(define-public crate-jaru-0.2.0 (c (n "jaru") (v "0.2.0") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.15") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "1qvjxqkp3n031rg5sh753720vcvmjd6d4d339x6g7pnm7rps2kfz") (y #t)))

(define-public crate-jaru-0.1.1 (c (n "jaru") (v "0.1.1") (d (list (d (n "owo-colors") (r "^3.5.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.15") (d #t) (k 0)) (d (n "tokio") (r "^1.26.0") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 0)))) (h "0yr6xigykndsrp468zhwfx2935y75d2pqh0d2p0ih9iw5c4p6ahx")))

