(define-module (crates-io ja ru jarust_make_plugin) #:use-module (crates-io))

(define-public crate-jarust_make_plugin-0.1.0 (c (n "jarust_make_plugin") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1lky13hn5vxq3cz6yf7aq55jqjwc7jccqb1n5k2b5zdb5i62dqff") (f (quote (("echotest") ("default" "echotest"))))))

(define-public crate-jarust_make_plugin-0.1.1 (c (n "jarust_make_plugin") (v "0.1.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1hmsc38nir74wwkxyxl3i5v3xxk1x2y7yh0bykdp27rvgwq2d1i2") (f (quote (("echotest") ("default" "echotest"))))))

(define-public crate-jarust_make_plugin-0.1.2 (c (n "jarust_make_plugin") (v "0.1.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1p7j188gbn3y5hs04nnrrzwvkvcjqr8yxsjhiqgy3c3n2cc9lv1v") (f (quote (("echotest") ("default" "echotest"))))))

(define-public crate-jarust_make_plugin-0.1.3 (c (n "jarust_make_plugin") (v "0.1.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "18nvdsarj9dh5cj9pbbmr2fjkbwn36jwz8cy4g9v526a0n1ybpy7") (f (quote (("echotest") ("default" "echotest"))))))

(define-public crate-jarust_make_plugin-0.1.4 (c (n "jarust_make_plugin") (v "0.1.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "049yxafsnzdpbgjm20ar9jpwprp9mxc4j7asnilf9cggn81l2pwb") (f (quote (("echotest") ("default" "echotest"))))))

(define-public crate-jarust_make_plugin-0.1.5 (c (n "jarust_make_plugin") (v "0.1.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1m4yqmcppdj6a3h6wvgwciy6782fkq8w6041gdkg3xgsl2z4ncsf") (f (quote (("echotest") ("default" "echotest"))))))

(define-public crate-jarust_make_plugin-0.2.0 (c (n "jarust_make_plugin") (v "0.2.0") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "127lqgfbscvg64fnbc6h12cbnf8pla6xkmz3ki05k3iwryrfdld8") (f (quote (("echotest") ("default" "echotest"))))))

(define-public crate-jarust_make_plugin-0.2.1 (c (n "jarust_make_plugin") (v "0.2.1") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1sbh1gcrw2qq8bgmlz9rhsfc89b0ijrmaf9qjmk5swljnwvpcvrh")))

(define-public crate-jarust_make_plugin-0.2.2 (c (n "jarust_make_plugin") (v "0.2.2") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1rqpad2xm3khdxk3n252djcagcl5s39glq5rxayxxyl1yd1cwm2k")))

(define-public crate-jarust_make_plugin-0.2.3 (c (n "jarust_make_plugin") (v "0.2.3") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1gi73n4q2l9pl797vhxpjhl7ci5sxyiigkylfyg25mkprx2jz3sl")))

(define-public crate-jarust_make_plugin-0.2.4 (c (n "jarust_make_plugin") (v "0.2.4") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0vajdmd9a36g53my3fnih4lk1vls6lnyzb4k1gm342p017x79ng1")))

(define-public crate-jarust_make_plugin-0.2.5 (c (n "jarust_make_plugin") (v "0.2.5") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0syb36ah2lfhx038zmb1da4401zvy5v7wwkxzdb8qspfk9rmm89a")))

(define-public crate-jarust_make_plugin-0.2.6 (c (n "jarust_make_plugin") (v "0.2.6") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "06q4zwpp4m6mibxhcgvla6bp10y9jsg937vjwh4jsvf1273iw8ks")))

(define-public crate-jarust_make_plugin-0.2.7 (c (n "jarust_make_plugin") (v "0.2.7") (d (list (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0njrjcbh99cng65s0bc633l4dhy917qs0vdi7hiasqx3bk9fmh7n")))

