(define-module (crates-io ja ss jassbot) #:use-module (crates-io))

(define-public crate-jassbot-0.1.0 (c (n "jassbot") (v "0.1.0") (d (list (d (n "matrix-sdk") (r "^0.5.0") (f (quote ("markdown"))) (d #t) (k 0)) (d (n "oops") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0amlxdsi1c564wsnxwwfmhby5xqm60hmn9vy1sv8bb0j12lc0m0z")))

(define-public crate-jassbot-0.2.0 (c (n "jassbot") (v "0.2.0") (d (list (d (n "matrix-sdk") (r "^0.5.0") (f (quote ("markdown"))) (d #t) (k 0)) (d (n "oops") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "18vg1h1p3ld08ihivilnxldr4i3bdbkabwv98fw2wgp0jz95nwh0")))

(define-public crate-jassbot-0.2.1 (c (n "jassbot") (v "0.2.1") (d (list (d (n "matrix-sdk") (r "^0.5.0") (f (quote ("markdown"))) (d #t) (k 0)) (d (n "oops") (r "^0.2.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.11") (d #t) (k 0)) (d (n "serde") (r "^1.0.143") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.83") (d #t) (k 0)) (d (n "tokio") (r "^1.20.1") (f (quote ("full"))) (d #t) (k 0)) (d (n "urlencoding") (r "^2.1.0") (d #t) (k 0)))) (h "0bgz8pmf6pbchx12vbsy0xqg9bj64nr4lnbcddphbdrciglzl9h6")))

