(define-module (crates-io ja ne jane-eyre) #:use-module (crates-io))

(define-public crate-jane-eyre-0.1.0 (c (n "jane-eyre") (v "0.1.0") (d (list (d (n "eyre") (r "^0.3.0") (d #t) (k 0)) (d (n "indenter") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.13") (d #t) (k 2)) (d (n "tracing-error") (r "^0.1.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.2") (f (quote ("registry" "fmt"))) (k 2)))) (h "0wr79fz425wy3c37ixz1wmvzxm8s23zlks3cj41ri394yc4j3yr9")))

(define-public crate-jane-eyre-0.1.1 (c (n "jane-eyre") (v "0.1.1") (d (list (d (n "eyre") (r "^0.3.0") (d #t) (k 0)) (d (n "indenter") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.13") (d #t) (k 2)) (d (n "tracing-error") (r "^0.1.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.2") (f (quote ("registry" "fmt"))) (k 2)))) (h "1vj2x3ln9m79scqjaqg14n5arg9rbvbg1yg1a5f0xyqhc36vxs8q")))

(define-public crate-jane-eyre-0.1.2 (c (n "jane-eyre") (v "0.1.2") (d (list (d (n "eyre") (r "^0.3.5") (d #t) (k 0)) (d (n "indenter") (r "^0.1.3") (d #t) (k 0)) (d (n "tracing") (r "^0.1.12") (d #t) (k 2)) (d (n "tracing-error") (r "^0.1.2") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.2") (f (quote ("registry" "fmt"))) (k 2)))) (h "140ffkhcz4hxx970h64lx0hpms27zqdjmzbql2apfd69njh5r98v")))

(define-public crate-jane-eyre-0.2.0 (c (n "jane-eyre") (v "0.2.0") (d (list (d (n "color-eyre") (r "^0.3.1") (d #t) (k 0)))) (h "162mzlq4y9744qsxz5wqxddq7yzhsfcqvi324n958gg3457s6yy5")))

(define-public crate-jane-eyre-0.3.0 (c (n "jane-eyre") (v "0.3.0") (d (list (d (n "color-eyre") (r "^0.5") (d #t) (k 0)))) (h "1q0b5lx5ch4wfwhghdj8dsk2klszz3y5hnz7qv5ls9kyhdy9bkbw")))

