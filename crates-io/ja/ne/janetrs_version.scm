(define-module (crates-io ja ne janetrs_version) #:use-module (crates-io))

(define-public crate-janetrs_version-0.1.0 (c (n "janetrs_version") (v "0.1.0") (d (list (d (n "const_fn") (r "^0.4") (d #t) (k 0)) (d (n "evil-janet") (r "^1") (d #t) (k 0)))) (h "0zrsiw8zjj1bwssk1npxccp9cv3cd1121fk6lpflis90jgvpzlf9") (f (quote (("system" "evil-janet/system") ("default"))))))

