(define-module (crates-io ja ne janet-rs-sys) #:use-module (crates-io))

(define-public crate-janet-rs-sys-0.1.0 (c (n "janet-rs-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.65.1") (d #t) (k 1)) (d (n "meson") (r "^1.0.0") (d #t) (k 1)) (d (n "which") (r "^4.4.2") (d #t) (t "cfg(windows)") (k 1)))) (h "0hln2v4riligclw8dgg4f7lzd1vb833027f02s0v4lpslzdsg9g4")))

