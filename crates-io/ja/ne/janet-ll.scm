(define-module (crates-io ja ne janet-ll) #:use-module (crates-io))

(define-public crate-janet-ll-0.4.1 (c (n "janet-ll") (v "0.4.1") (d (list (d (n "bindgen") (r "^0.49.0") (d #t) (k 1)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "shlex") (r "^0.1.1") (d #t) (k 1)))) (h "17xai7k03n4w68ynmcpx1fqmz1pcm6pkz41mvkncdmv9fimklpg7") (f (quote (("link-amalg") ("default"))))))

