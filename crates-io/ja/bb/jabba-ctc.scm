(define-module (crates-io ja bb jabba-ctc) #:use-module (crates-io))

(define-public crate-jabba-ctc-0.1.0 (c (n "jabba-ctc") (v "0.1.0") (d (list (d (n "jabba-lib") (r "^0.1.6") (d #t) (k 0)))) (h "13n8zlcrgldwr8ns9014hkmglr31bhim8861lk93mrvgvz56jhcr")))

(define-public crate-jabba-ctc-0.1.1 (c (n "jabba-ctc") (v "0.1.1") (d (list (d (n "jabba-lib") (r "^0.1.6") (d #t) (k 0)))) (h "1cmgr6n9y1arckdbq35i1nclgjvkkqh8lyg6cylg4w8d5fq3aaac")))

