(define-module (crates-io ja bb jabba-lib) #:use-module (crates-io))

(define-public crate-jabba-lib-0.1.0 (c (n "jabba-lib") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "18ldnkznmk204jfcsyyhh69m7iwih04cpb8fwarr6jhlwymqvf6z")))

(define-public crate-jabba-lib-0.1.1 (c (n "jabba-lib") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "077g3qdg2akqjvmgb06izvkbniaxpkmm1d53qzg6bag3hpy9bbbw")))

(define-public crate-jabba-lib-0.1.2 (c (n "jabba-lib") (v "0.1.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clipboard-win") (r "^4.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0as1drl26r01yiiqnkdn0xf2241akd42cn567l2ncmb05dmdg780")))

(define-public crate-jabba-lib-0.1.3 (c (n "jabba-lib") (v "0.1.3") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clipboard-win") (r "^4.4.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "0c346qivrplwkvhrr04a96kralvmsyy8pjp41xnbhvvcpc3cmyp4")))

(define-public crate-jabba-lib-0.1.4 (c (n "jabba-lib") (v "0.1.4") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clipboard-win") (r "^4.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1pmd25pn5lpqgkip7lniakfpq951067blcpki0ikm22mn8fslvab")))

(define-public crate-jabba-lib-0.1.5 (c (n "jabba-lib") (v "0.1.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clipboard-win") (r "^4.4.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1rz3ssfygdnrln3zyl4p5z880gm3kmayb87rfd0mdp32y7740f3p")))

(define-public crate-jabba-lib-0.1.6 (c (n "jabba-lib") (v "0.1.6") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clipboard-win") (r "^4.4.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1bbfix1dxj8w6dlp7jk2hvv0na5rsm75kad5vwh9qkfhg2yshrln")))

(define-public crate-jabba-lib-0.1.7 (c (n "jabba-lib") (v "0.1.7") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clipboard-win") (r "^4.4.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1s70hlq2sg8qywkaxkhy0ymrhc9ykvbd8khx9jh2w9wwx3cs3v2r")))

(define-public crate-jabba-lib-0.1.8 (c (n "jabba-lib") (v "0.1.8") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "clipboard-win") (r "^4.4.2") (d #t) (k 0)) (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "once_cell") (r "^1.13.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "shlex") (r "^1.1.0") (d #t) (k 0)) (d (n "which") (r "^4.2.5") (d #t) (k 0)))) (h "1l5xc2vwrv9ln9irqpgdknkr39qfyn5djy3mf87qwvf186213fkl")))

