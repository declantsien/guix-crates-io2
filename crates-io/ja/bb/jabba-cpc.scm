(define-module (crates-io ja bb jabba-cpc) #:use-module (crates-io))

(define-public crate-jabba-cpc-0.1.0 (c (n "jabba-cpc") (v "0.1.0") (d (list (d (n "jabba-lib") (r "^0.1.3") (d #t) (k 0)))) (h "1pza9q1cp8y4cfy6f9cjyrglv9xryahfqwl7z656aw82hvwis0r1")))

(define-public crate-jabba-cpc-0.1.1 (c (n "jabba-cpc") (v "0.1.1") (d (list (d (n "jabba-lib") (r "^0.1.3") (d #t) (k 0)))) (h "1r4jircgqvy90qjx5rbwi09skdxgs8vfbad4mwc7064w42aps2qc")))

(define-public crate-jabba-cpc-0.1.2 (c (n "jabba-cpc") (v "0.1.2") (d (list (d (n "jabba-lib") (r "^0.1.4") (d #t) (k 0)))) (h "066kps23qcjilsqjwwh1jgdsk1957ziqalhs9pcl4r9x1rhpsm4q")))

