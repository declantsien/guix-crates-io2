(define-module (crates-io ja bb jabba) #:use-module (crates-io))

(define-public crate-jabba-0.1.0 (c (n "jabba") (v "0.1.0") (d (list (d (n "async-lock") (r "^2.6.0") (d #t) (k 0)) (d (n "async-trait") (r "^0.1.59") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("full"))) (d #t) (k 2)))) (h "1fj8x099rlbs3vsb8cmlw41xl55gq6cz63bl8xcz87m991qj8q7m")))

