(define-module (crates-io ja bb jabberwock) #:use-module (crates-io))

(define-public crate-jabberwock-0.1.0 (c (n "jabberwock") (v "0.1.0") (d (list (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "hatter") (r "^0.1.4") (d #t) (k 0)) (d (n "markdown") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.9") (o #t) (d #t) (k 0)))) (h "0sz3fzhrpvls9iyzy0xvff545vakfgf9qs7ipqp026lqq7r4x43a") (f (quote (("transpile") ("templates") ("default" "copy" "markdown" "templates" "toml" "transpile") ("copy")))) (s 2) (e (quote (("toml" "dep:toml") ("markdown" "dep:markdown"))))))

