(define-module (crates-io ja ck jack-pet) #:use-module (crates-io))

(define-public crate-jack-pet-0.1.0 (c (n "jack-pet") (v "0.1.0") (d (list (d (n "async-std") (r "^1.2.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "1c1dyp2xg49nk040lxlwhgs374b8wajn105jlh4yj2gl957n4d47")))

(define-public crate-jack-pet-0.1.0-beta.0.0.1 (c (n "jack-pet") (v "0.1.0-beta.0.0.1") (d (list (d (n "async-std") (r "^1.2.0") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "chrono") (r "^0.4.19") (d #t) (k 0)))) (h "0k2ss8jb61li5j6yhbsc1293bs0489ap85sj87vnimkga3lvmz75")))

