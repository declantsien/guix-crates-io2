(define-module (crates-io ja ck jack-client) #:use-module (crates-io))

(define-public crate-jack-client-0.1.0 (c (n "jack-client") (v "0.1.0") (d (list (d (n "jack-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1x3y3m72kidm73xxram4288a6k7ky4ky7hmwfncjf26a3lljsw49")))

(define-public crate-jack-client-0.1.1 (c (n "jack-client") (v "0.1.1") (d (list (d (n "jack-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0k0qyshlisshspjc8l5dl98hmz5fidvyp99gs4jp79wfn5dbv5p9")))

(define-public crate-jack-client-0.1.2 (c (n "jack-client") (v "0.1.2") (d (list (d (n "jack-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "00d77snsg8g850mbjmykla3y51a2d5781a7zahqs7psahcnghg45")))

