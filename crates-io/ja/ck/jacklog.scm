(define-module (crates-io ja ck jacklog) #:use-module (crates-io))

(define-public crate-jacklog-0.0.3 (c (n "jacklog") (v "0.0.3") (d (list (d (n "tracing") (r "^0.1.24") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.16") (d #t) (k 0)))) (h "0wwjh7w17621hs0wa9s48lizh7v7i9a04q84cw8idxig8mva95r5")))

(define-public crate-jacklog-0.0.4 (c (n "jacklog") (v "0.0.4") (d (list (d (n "tracing") (r "^0.1.24") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.16") (d #t) (k 0)))) (h "0c4ckkq390kasv9mkrhghp40wn519lngg10gfxsyyr7c4xmj4agl")))

(define-public crate-jacklog-0.0.6 (c (n "jacklog") (v "0.0.6") (d (list (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)))) (h "0r3iz7656vg7v6vyia5r6lwwps8q47kqr3pkfwl2nlyy9ca6q0kp")))

(define-public crate-jacklog-0.0.7 (c (n "jacklog") (v "0.0.7") (d (list (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)))) (h "1jijzpda6b42h8y3hm970dc7ww6pk38x4navdpcw8qhf5flkd4zp")))

(define-public crate-jacklog-0.0.8 (c (n "jacklog") (v "0.0.8") (d (list (d (n "atty") (r "^0.2.14") (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (d #t) (k 0)))) (h "17hr26z0z9am4nlnwv2ca7i0x96sw42z18h9fkbracd72wmc9dp7")))

(define-public crate-jacklog-0.0.9 (c (n "jacklog") (v "0.0.9") (d (list (d (n "atty") (r "^0.2.14") (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (f (quote ("ansi" "env-filter" "fmt" "json" "registry"))) (k 0)))) (h "08vjfzbzzk3swqbm272xrfj8z6sx1gsl7yn10kk4zydfn1n1ak8b")))

(define-public crate-jacklog-0.1.0 (c (n "jacklog") (v "0.1.0") (d (list (d (n "atty") (r "^0.2.14") (k 0)) (d (n "tracing") (r "^0.1.26") (k 0)) (d (n "tracing-subscriber") (r "^0.3.11") (f (quote ("ansi" "env-filter" "fmt" "json" "registry"))) (k 0)))) (h "1l7rs7hv0m26q0iy0ndr2p5ymlx8ypnfqi78byqdx5z5ax5c8jkd")))

(define-public crate-jacklog-0.0.10 (c (n "jacklog") (v "0.0.10") (d (list (d (n "atty") (r "^0.2.14") (k 0)) (d (n "tracing") (r "~0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "~0") (f (quote ("ansi" "env-filter" "fmt" "json" "registry"))) (k 0)))) (h "0inxchy38nw79y4k8m9b11jbw995rkfd7hapvddbgx7wfgvqnh9y") (y #t)))

(define-public crate-jacklog-0.0.11 (c (n "jacklog") (v "0.0.11") (d (list (d (n "atty") (r "^0.2.14") (k 0)) (d (n "tracing") (r "~0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "~0") (f (quote ("ansi" "env-filter" "fmt" "json" "registry"))) (k 0)))) (h "17xb7gmcp942p9067nl79bzwy742rgvaqinw2j0s1fymwswmgclz") (y #t)))

(define-public crate-jacklog-0.2.0 (c (n "jacklog") (v "0.2.0") (d (list (d (n "atty") (r "^0.2.14") (k 0)) (d (n "tracing") (r "~0") (d #t) (k 0)) (d (n "tracing-subscriber") (r "~0") (f (quote ("ansi" "env-filter" "fmt" "json" "registry"))) (k 0)))) (h "03davf4xxbnlsvxbkb6fs38kxpi96gx4284k1x654s38s75gy76d")))

