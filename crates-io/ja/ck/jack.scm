(define-module (crates-io ja ck jack) #:use-module (crates-io))

(define-public crate-jack-0.1.1 (c (n "jack") (v "0.1.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "10z35pd1wlwisyfp5dczr51d7kavha7pwym1j17s3cas87fiysc9")))

(define-public crate-jack-0.1.2 (c (n "jack") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17m1qnpz2ig7apvjjkzdv6m8dadgvcm8ji4gq3vx27dhfnq11f77")))

(define-public crate-jack-0.1.3 (c (n "jack") (v "0.1.3") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.2") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "13yj8d28cdq79yf7178nh811yf7kd9n0b8kjbn7hzzv616zi7y15")))

(define-public crate-jack-0.1.4 (c (n "jack") (v "0.1.4") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ii94myk6gq0rrrds2h3q5x6ir3qgq4z32d4z7rhizrpiva44qcb")))

(define-public crate-jack-0.2.0 (c (n "jack") (v "0.2.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "062yjjkrkbdy3sjjj3njrf3k1b2yfmsk43d7alyprrvvyf1pzrb1")))

(define-public crate-jack-0.2.1 (c (n "jack") (v "0.2.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.3") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1n5sx9r457fb0w8bvnkbiaf22gv9qymxkq3pak0snlda6nvcqh7n")))

(define-public crate-jack-0.2.2 (c (n "jack") (v "0.2.2") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0mxk4inz60ccvm29aqpr37asfb80ds240fnhll8fdn26hg46rkv3")))

(define-public crate-jack-0.3.0 (c (n "jack") (v "0.3.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "03asw3jji09j6x4imjy1pxh83q81xp6fihn011mn9m03z22gdigd")))

(define-public crate-jack-0.4.0 (c (n "jack") (v "0.4.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qnaxwl5zjpvz0jjx1bk5vi4qn2622cz2ygwwamyfrga90g0wiwl")))

(define-public crate-jack-0.5.0 (c (n "jack") (v "0.5.0") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0cc6pc1mbcdg4cbs0ayds9a6g43lr63qnkyp7qafkhk90qakd797")))

(define-public crate-jack-0.5.1 (c (n "jack") (v "0.5.1") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1sgm8zn4db8b6jjm40wz63029agdyjxv04r95jn0r7fyj561zdcc")))

(define-public crate-jack-0.5.2 (c (n "jack") (v "0.5.2") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.4") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0jdab1dp16xpj5phhf7qw66pi44qa6iyz6b66q6wvx46dnpfqh5w")))

(define-public crate-jack-0.5.3 (c (n "jack") (v "0.5.3") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r3l11rfl78bb1x606ikd29yf884xfmdp1vr1qnwm6z0f9z4kyxp")))

(define-public crate-jack-0.5.4 (c (n "jack") (v "0.5.4") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0x3wnsa4qjazd37zjgb7zylps99knpcjfhpprkx8v55dzz9hvbgw")))

(define-public crate-jack-0.5.5 (c (n "jack") (v "0.5.5") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ngidij25wbdadjakkgalz4gr0j2hirki7j2xxz3rx1djnr6bjny")))

(define-public crate-jack-0.5.6 (c (n "jack") (v "0.5.6") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.1.5") (d #t) (k 0)) (d (n "lazy_static") (r "^0.2.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17x9k41hs8yb8xb6q2n28ydkn6qbbz79ka8dpill8a7psb8yg3q2")))

(define-public crate-jack-0.5.7 (c (n "jack") (v "0.5.7") (d (list (d (n "bitflags") (r "^0.7.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pr2fkjh181b6qjx940vp8hamcadq21p0l7z0nhp8nif5rczq58y")))

(define-public crate-jack-0.6.0 (c (n "jack") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ki76fy4ng0pjyp7gbss24rghb7mfqrr5ig8j7wxigxv074ncjq6")))

(define-public crate-jack-0.6.1 (c (n "jack") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "jack-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1vl91zpcm3k0fqjakiahwxflsbqzh6g6jncm7s94g9h09k5ja9in")))

(define-public crate-jack-0.6.2 (c (n "jack") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0dpp53w2ylah04d5m5zbgqxh5z16iq58l0gsc6svvl97aj83jmrq")))

(define-public crate-jack-0.6.3 (c (n "jack") (v "0.6.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "182kgxlqpg9q97rsdi23hd2jlvsiy9xdrdzn35wf2rrk5h8w59n8")))

(define-public crate-jack-0.6.4 (c (n "jack") (v "0.6.4") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1r06fj6ccm822c6pmf18h1rwzp28v8cdp55r7m1vhinvwb83765k")))

(define-public crate-jack-0.6.5 (c (n "jack") (v "0.6.5") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1xd67a9vl25p7qalqs34j80dxlg5lraamnxyycla6nm63z4p263w")))

(define-public crate-jack-0.6.6 (c (n "jack") (v "0.6.6") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0sk8rxiq3h2y33hdq15hnf915l8rv09zl9sgg2vjysvypms4ksrd") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.7.0 (c (n "jack") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "17cxds0dh4zmjhwqf6sk90kxxx8755hvya339nlq851zjjmx03ip") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.7.1 (c (n "jack") (v "0.7.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1nz2d69ddbaz83v0ans03m0z59cr999cldd3rcriygjakcjj1rs9") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.7.2 (c (n "jack") (v "0.7.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0d4yglr21if712ayb6y5rdzqjwpy16q1m6x9gnbccmxfjnbjnwir") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.7.3 (c (n "jack") (v "0.7.3") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1r7bgfpbph3fl9xyp4i9qffcc4h923dcs7d967mpir13lxg216yp") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.8.0 (c (n "jack") (v "0.8.0") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0k0a2hl6wx822j0cbffa856qvhrpbp0x0i2b7chfz0qayj58xs3d") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.8.1 (c (n "jack") (v "0.8.1") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1a6wwnzh4ab8540k2ccdqiy367qm6sx2424q3b1xn2565p3i0r5b") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.8.2 (c (n "jack") (v "0.8.2") (d (list (d (n "bitflags") (r "^1.2") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1xlha6y4m7glkcy0z005b273y609m2c08zqvz3vxhgy8q6h7fhd3") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.8.3 (c (n "jack") (v "0.8.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "08sa18yrk6pa8acdsxzylmilnxw7yk2yrlgdhkbwlgbw5012m45k") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.8.4 (c (n "jack") (v "0.8.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.2.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0lz10s0n2gy128m65pf96is9ip00vfgvnkfja0y9ydmv24pw2ajx") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.9.0 (c (n "jack") (v "0.9.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "jack-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0ghsqkraxlarlq7xyxsrr26hxl48pifmcrydpipff5xr2mkbga81") (f (quote (("metadata") ("dlopen" "jack-sys/dlopen") ("default" "dlopen"))))))

(define-public crate-jack-0.9.1 (c (n "jack") (v "0.9.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "jack-sys") (r "^0.3.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1jpdkpjda6zgnnihrz74zm41qdcn02b6567hlifr9l8yqk5ivxhc") (f (quote (("metadata") ("dlopen" "jack-sys/dlopen") ("default" "dlopen"))))))

(define-public crate-jack-0.9.2 (c (n "jack") (v "0.9.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "jack-sys") (r "^0.3.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0q10pq0zq10rnxv1ay2ffylxg4q363g9x6j5lcmvay9szg92did1") (f (quote (("metadata") ("dlopen" "jack-sys/dlopen") ("default" "dlopen"))))))

(define-public crate-jack-0.10.0 (c (n "jack") (v "0.10.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0djs3j0icxbzbivhj73vgjrvjw6ncpfak2vyxjcbn4wvl9ajcwnf") (f (quote (("metadata") ("default"))))))

(define-public crate-jack-0.11.0 (c (n "jack") (v "0.11.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "1niqyyp6x7bsai4iwdlbs4cdqzq3l627gk4fpydjc4brq2rhxvhs") (f (quote (("metadata") ("dynamic_loading" "jack-sys/dynamic_loading") ("default" "dynamic_loading"))))))

(define-public crate-jack-0.11.1 (c (n "jack") (v "0.11.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0705jvywbyznk71134in703xq5xrz8dh1i3hvrwxmkj42xxzlfnj") (f (quote (("metadata") ("dynamic_loading" "jack-sys/dynamic_loading") ("default" "dynamic_loading"))))))

(define-public crate-jack-0.11.2 (c (n "jack") (v "0.11.2") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "0rhirvvs6a5gc2fxif204lrh9alpyxnph1ssb3b6srk1qz5rk1gh") (f (quote (("metadata") ("dynamic_loading" "jack-sys/dynamic_loading") ("default" "dynamic_loading"))))))

(define-public crate-jack-0.11.3 (c (n "jack") (v "0.11.3") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)) (d (n "jack-sys") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "03hlf9815f846731km2l480fmz9i1n51ibv0sydnpijykk9f4ida") (f (quote (("metadata") ("dynamic_loading" "jack-sys/dynamic_loading") ("default" "dynamic_loading"))))))

(define-public crate-jack-0.11.4 (c (n "jack") (v "0.11.4") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "jack-sys") (r "^0.5") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 2)))) (h "1kd6p6bfmxyclkkq9pkrqyynf0mj53ias4binx7kbyxfqaiihnhf") (f (quote (("metadata") ("dynamic_loading" "jack-sys/dynamic_loading") ("default" "dynamic_loading"))))))

