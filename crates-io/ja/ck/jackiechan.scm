(define-module (crates-io ja ck jackiechan) #:use-module (crates-io))

(define-public crate-jackiechan-0.0.1 (c (n "jackiechan") (v "0.0.1") (h "1889q6s63z0hj5zaf62nw9z91w9mx2rxnpkvl8ql4lfmdhp2vn8s")))

(define-public crate-jackiechan-0.0.2 (c (n "jackiechan") (v "0.0.2") (d (list (d (n "blocking") (r "^0.6.0") (d #t) (k 2)) (d (n "concurrent-queue") (r "^1.2.2") (d #t) (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "11sdxfaqr5is6grbj4984mgb1p0hq4f8rqrgipbsj76133bfhs3q")))

(define-public crate-jackiechan-0.0.4 (c (n "jackiechan") (v "0.0.4") (d (list (d (n "blocking") (r "^0.6.0") (d #t) (k 2)) (d (n "concurrent-queue") (r "^1.2.2") (d #t) (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "futures-core") (r "^0.3.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "1w40icaj7qwvrc0pibrx89h82p095xh9amvnr7d8y6arm6fgl4gi")))

(define-public crate-jackiechan-1.0.0 (c (n "jackiechan") (v "1.0.0") (d (list (d (n "blocking") (r "^0.6.0") (d #t) (k 2)) (d (n "concurrent-queue") (r "^1.2.2") (d #t) (k 0)) (d (n "easy-parallel") (r "^3.1.0") (d #t) (k 2)) (d (n "event-listener") (r "^2.4.0") (d #t) (k 0)) (d (n "futures-lite") (r "^1.0.0") (d #t) (k 2)))) (h "1lh51rr7rjfxpiys467z1lswkqkxl2x23prbk9plxqrk3xb6dn0k")))

