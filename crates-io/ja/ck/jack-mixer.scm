(define-module (crates-io ja ck jack-mixer) #:use-module (crates-io))

(define-public crate-jack-mixer-0.1.0 (c (n "jack-mixer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.28") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.3.8") (d #t) (k 0)) (d (n "druid") (r "^0.5") (d #t) (k 0)) (d (n "jack") (r "^0.6.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "novation_launch_control") (r "^0.1") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.3.0") (d #t) (k 0)) (d (n "structopt") (r "^0.2.18") (d #t) (k 0)))) (h "00zjgvaqawv2xvwyasbzxlpi97l9jlqqdqgpl81dycgw1dpdw7kf")))

