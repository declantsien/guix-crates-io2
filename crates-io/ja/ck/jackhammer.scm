(define-module (crates-io ja ck jackhammer) #:use-module (crates-io))

(define-public crate-jackhammer-0.1.0 (c (n "jackhammer") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "110z6511kf3yl5g8k2pw4s87jv4nrpw58wb3cz3i3d7framn336y")))

(define-public crate-jackhammer-0.2.0 (c (n "jackhammer") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.32") (d #t) (k 0)) (d (n "metrix") (r "^0.13.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (d #t) (k 2)) (d (n "structopt") (r "^0.3.17") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 0)))) (h "1fwp94xv6r80ws2c02mr0m3y44crywphzp4p4id7w11f2bm6902k")))

(define-public crate-jackhammer-0.3.0 (c (n "jackhammer") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.38") (d #t) (k 0)) (d (n "metrix") (r "^0.13.12") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.0") (d #t) (k 2)) (d (n "structopt") (r "^0.3.17") (d #t) (k 2)) (d (n "termion") (r "^1.5.5") (d #t) (k 2)) (d (n "tokio") (r "^1.0.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1rdl8q2xqbfmf956vfh7pznx9y7w7nzrlgcx0zw506rrgz3d8jy1")))

