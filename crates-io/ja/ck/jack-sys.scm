(define-module (crates-io ja ck jack-sys) #:use-module (crates-io))

(define-public crate-jack-sys-0.0.1 (c (n "jack-sys") (v "0.0.1") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "0yq474x7vikldg12b3n2vakhma3vqb5q6cqx5a62hbdgj08y5gk6")))

(define-public crate-jack-sys-0.0.2 (c (n "jack-sys") (v "0.0.2") (d (list (d (n "libc") (r "*") (d #t) (k 0)))) (h "1l9nvgpyszbnn68y7g816fcdyfnkaxacrm543c9m9gn1fr3nl5mm")))

(define-public crate-jack-sys-0.1.0 (c (n "jack-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1gdr5m6ir3v14j5s8q563c19hp80mn632mk78a8g7b29zq6lq6zs")))

(define-public crate-jack-sys-0.1.1 (c (n "jack-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0wn9ipa00f1kyq6a545c9fynbvb9y95vh8apipyl87mpp3df4fnd")))

(define-public crate-jack-sys-0.1.2 (c (n "jack-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1awi452jrsf0ck5njnf3a81zm9w305h9f819vdvjlx36szrq0c8w")))

(define-public crate-jack-sys-0.1.3 (c (n "jack-sys") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "14pnpfcrc42cgdgjz9si14qhs395awii81ibsq6c2k4i5ynz0ymh")))

(define-public crate-jack-sys-0.1.4 (c (n "jack-sys") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "11yh4ibjb34qxc9ayr89p9ad760f1q699jnaklhvspgsq22q8dxb")))

(define-public crate-jack-sys-0.1.5 (c (n "jack-sys") (v "0.1.5") (d (list (d (n "lazy_static") (r "^0.2.8") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4.0") (d #t) (k 0)))) (h "13ii9ip1i89rcn88rk35aq0qvwrcbqazkh4a0ha3nyis6vfi77z6")))

(define-public crate-jack-sys-0.2.0 (c (n "jack-sys") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.4.0") (d #t) (k 0)))) (h "1hvc3r9069ihindpxkr6v2737mp58q85iprn7bckrzbp2i8cmm60")))

(define-public crate-jack-sys-0.2.1 (c (n "jack-sys") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)))) (h "1mxlmhkr219awxpqhg1ib26a8qmy0ijvi8hjg6ibc0m4mnvnl7cf")))

(define-public crate-jack-sys-0.2.2 (c (n "jack-sys") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)))) (h "0hggdy2zkvq9c0agkcscm7hr8lfqmjfbqfgd36vz5v6zf86kz62p")))

(define-public crate-jack-sys-0.2.3 (c (n "jack-sys") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.6") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1h9c9za19nyr1prx77gkia18ia93f73lpyjdiyrvmhhbs79g54bv") (l "jack")))

(define-public crate-jack-sys-0.3.0 (c (n "jack-sys") (v "0.3.0") (d (list (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "dlopen") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1s21zqhcx9wmy1sdfz4pv8d16iaikdxbxd5fb7brv9kdlmjzbybl") (f (quote (("default")))) (y #t) (l "jack")))

(define-public crate-jack-sys-0.3.1 (c (n "jack-sys") (v "0.3.1") (d (list (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "dlopen") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0padvfs47m1q8wkpvpbfd5ghk0dwvyjnc8zb8a28fzk3ibpxqq5j") (f (quote (("default")))) (y #t) (l "jack")))

(define-public crate-jack-sys-0.3.2 (c (n "jack-sys") (v "0.3.2") (d (list (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "dlopen") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "11bm2n22dc7habhq4lw1i6xggc78mkwwnmf0yjc072nslx94z5v9") (f (quote (("default")))) (l "jack")))

(define-public crate-jack-sys-0.3.3 (c (n "jack-sys") (v "0.3.3") (d (list (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "dlopen") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "12fb4zp234rk9zd7s7h2swwxq7dzya90migh1psc2scjk5qg6khn") (f (quote (("default")))) (l "jack")))

(define-public crate-jack-sys-0.3.4 (c (n "jack-sys") (v "0.3.4") (d (list (d (n "dlib") (r "^0.5") (d #t) (k 0)) (d (n "dlopen") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1m2ci6p44ssaipfqf9hgwl3mhq3637magqj88xir2ddyc8zj77ig") (f (quote (("default")))) (l "jack")))

(define-public crate-jack-sys-0.4.0 (c (n "jack-sys") (v "0.4.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "17vaq4i8q5nx39rjqx9sixqn1xraf1vxs3bmrf618v8nzxchbmz9") (l "jack")))

(define-public crate-jack-sys-0.5.0 (c (n "jack-sys") (v "0.5.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0jpd70xa12m6l6k03zb3q2ckpjvvh8zylzgd0irs34g4x7xsnzjf") (f (quote (("dynamic_loading") ("default_features" "dynamic_loading")))) (l "jack")))

(define-public crate-jack-sys-0.5.1 (c (n "jack-sys") (v "0.5.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 1)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libloading") (r "^0.7") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "1aw6zishflmd5v9dz5yvpc5f9jsfm9pjjhzvdmbjp8lmkdhvf4v0") (f (quote (("dynamic_loading") ("default_features" "dynamic_loading")))) (l "jack")))

