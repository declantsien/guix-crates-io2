(define-module (crates-io ja ck jackdauer) #:use-module (crates-io))

(define-public crate-jackdauer-0.1.0 (c (n "jackdauer") (v "0.1.0") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1c0q3cjihfh431ggbn9n8wkjdnwd0zqsp96y5l08qp70zgk87743")))

(define-public crate-jackdauer-0.1.1 (c (n "jackdauer") (v "0.1.1") (d (list (d (n "nom") (r "^6") (d #t) (k 0)))) (h "07xbb37244fzh6cgyzzvi5amp1bx4pz5m0sk7dhn7cidbj7ys7sm")))

(define-public crate-jackdauer-0.1.2 (c (n "jackdauer") (v "0.1.2") (d (list (d (n "nom") (r "^7") (d #t) (k 0)))) (h "0x7ivwkgzv544visnn38s1hcwxpr7dd97qn2wd9s7i8137c0hdlx")))

