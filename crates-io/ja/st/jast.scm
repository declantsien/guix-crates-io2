(define-module (crates-io ja st jast) #:use-module (crates-io))

(define-public crate-jast-0.1.2 (c (n "jast") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0cm0n4zsag0lwsnbsgcws459nrgpijk79631bdj2w0q9vx77nmp5")))

