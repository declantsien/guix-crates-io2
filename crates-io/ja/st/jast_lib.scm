(define-module (crates-io ja st jast_lib) #:use-module (crates-io))

(define-public crate-jast_lib-0.1.0 (c (n "jast_lib") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "0709dj0ha5chv8z95hqiyr4zmmg4g12kd4v3asj5qb1x4niqmih7")))

(define-public crate-jast_lib-0.1.1 (c (n "jast_lib") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "num_cpus") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.93") (d #t) (k 0)))) (h "04pn9g4yampj1rp6pib4rv0amjwsmdigkkr2qgwvbdykd3r8zjdw")))

