(define-module (crates-io ja rs jars) #:use-module (crates-io))

(define-public crate-jars-0.1.0 (c (n "jars") (v "0.1.0") (d (list (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "01qsy75kcv0ww5ryxfsn94p0jl290y8mkbfclz99ji7zsj1sk59g")))

(define-public crate-jars-0.1.1 (c (n "jars") (v "0.1.1") (d (list (d (n "zip") (r "^0.6.4") (d #t) (k 0)))) (h "019rgdq944cjvf8y69ar5x9v5jih72nq7yr0dp6rlmiw1gzidlfr")))

