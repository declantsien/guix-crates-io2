(define-module (crates-io ja de jadep) #:use-module (crates-io))

(define-public crate-jadep-0.1.0 (c (n "jadep") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.18") (f (quote ("derive"))) (d #t) (k 0)) (d (n "dashmap") (r "^5.5.3") (d #t) (k 0)) (d (n "rayon") (r "^1.8.1") (d #t) (k 0)) (d (n "regex") (r "^1.10.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)))) (h "1l52f3isyyaa8zccv8kpyqwjqzsj3gxxbmdlq4avzff36qycak7p")))

