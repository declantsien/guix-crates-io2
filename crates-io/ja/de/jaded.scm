(define-module (crates-io ja de jaded) #:use-module (crates-io))

(define-public crate-jaded-0.1.0 (c (n "jaded") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0yl8mimjpy4mk3bfh2z0wa935mad5ypjc560r10rcddic6yb5da6")))

(define-public crate-jaded-0.1.1 (c (n "jaded") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0x9rayk0c9xyvjwpipnys15w321b8yfz8p6a922iw1pdmixbl6pd")))

(define-public crate-jaded-0.2.0 (c (n "jaded") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1hddpygdp50wm0zspdgcnaj6ral3a36s0nnjbincmqw0ay0zz4gv")))

(define-public crate-jaded-0.3.0 (c (n "jaded") (v "0.3.0") (d (list (d (n "jaded-derive") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "0xs4pcs71zycsn0ahrzjr8c22nvfy7k8xwy5n31yyzh1m5281smx") (f (quote (("renaming" "jaded-derive/renaming") ("derive" "jaded-derive"))))))

(define-public crate-jaded-0.4.0 (c (n "jaded") (v "0.4.0") (d (list (d (n "jaded-derive") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "1zwa9ag9rcbz988shhwjprxlsm6x5k9wmgcb5fxwpxgrm21gn5ym") (f (quote (("renaming" "jaded-derive/renaming") ("derive" "jaded-derive"))))))

