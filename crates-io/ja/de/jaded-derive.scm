(define-module (crates-io ja de jaded-derive) #:use-module (crates-io))

(define-public crate-jaded-derive-0.1.0 (c (n "jaded-derive") (v "0.1.0") (d (list (d (n "convert_case") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "07qn01w398k2bqbmkadbsi29zd25515f0ry5sha3j0fi5bn116l4") (f (quote (("renaming" "convert_case"))))))

(define-public crate-jaded-derive-0.2.0 (c (n "jaded-derive") (v "0.2.0") (d (list (d (n "proc-macro-error") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits"))) (d #t) (k 2)) (d (n "trybuild") (r "^1.0.52") (d #t) (k 2)))) (h "005rhrbl4wl39x5ylbbbrnv3n8z5s54bfn3hjz30dam3ih8y3b01") (f (quote (("renaming"))))))

