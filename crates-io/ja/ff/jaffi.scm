(define-module (crates-io ja ff jaffi) #:use-module (crates-io))

(define-public crate-jaffi-0.1.0 (c (n "jaffi") (v "0.1.0") (h "1ralbp87zqwqiqd33gzaql0aszph3264vhl2aid5k10q8sh3ncaj") (y #t)))

(define-public crate-jaffi-0.2.0 (c (n "jaffi") (v "0.2.0") (d (list (d (n "cafebabe") (r "^0.6.0") (d #t) (k 0)) (d (n "enum-as-inner") (r "^0.5") (d #t) (k 0)) (d (n "heck") (r "^0.4.0") (d #t) (k 0)) (d (n "jaffi_support") (r "^0.2.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.40") (d #t) (k 0)) (d (n "quote") (r "^1.0.20") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.20") (d #t) (k 0)) (d (n "typed-builder") (r "^0.10.0") (d #t) (k 0)))) (h "1445w5hb594ni42ys6301g7ci414r7zzwbzpmsa3z7j2wyw2xh9r")))

