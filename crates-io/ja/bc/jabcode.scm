(define-module (crates-io ja bc jabcode) #:use-module (crates-io))

(define-public crate-jabcode-0.1.0 (c (n "jabcode") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)))) (h "0l2lglfp6ha03w8md7p0i04wirrbz68x6gzqzymhzs3aic82qa0w")))

(define-public crate-jabcode-1.0.0 (c (n "jabcode") (v "1.0.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "02d3wfc77cwzp9byp0fb11g8xikbhbx893xw4g5vs8sxwpvgmisd")))

(define-public crate-jabcode-1.1.0 (c (n "jabcode") (v "1.1.0") (d (list (d (n "cc") (r "^1.0.73") (d #t) (k 1)) (d (n "image") (r "^0.24.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.126") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "03ws1j75m3l88xhdlr667vx00mp4yc84nn7rpwzxmxhkp595hqkk")))

