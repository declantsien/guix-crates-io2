(define-module (crates-io ja mj jamjam) #:use-module (crates-io))

(define-public crate-jamjam-0.1.0 (c (n "jamjam") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "0fgbcawb0f62b27lks4d06awp6rd2g6mfm5db07if6hkljg2bjs5")))

(define-public crate-jamjam-0.2.0 (c (n "jamjam") (v "0.2.0") (d (list (d (n "bstr") (r "^1.9.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1") (d #t) (k 2)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "rayon") (r "^1.10.0") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.58") (d #t) (k 0)))) (h "1x5vzi6i9ncan6p4ky9q9dx43k90m8sn88qi5j0q72my8w0fq3ky")))

