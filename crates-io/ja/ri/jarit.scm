(define-module (crates-io ja ri jarit) #:use-module (crates-io))

(define-public crate-jarit-0.0.0 (c (n "jarit") (v "0.0.0") (d (list (d (n "goolog") (r "^0.8.1") (d #t) (k 0)) (d (n "jni") (r "^0.21.1") (d #t) (k 0)))) (h "1477yaxgwb654na92l8gvyyp7hxjwrmfvz1c0mflss12dv9skgkq") (y #t)))

(define-public crate-jarit-0.0.1 (c (n "jarit") (v "0.0.1") (h "0m30k38d892vxagp5sgdp90gll08sfk86yxvh9imwx2ihf905244") (y #t)))

