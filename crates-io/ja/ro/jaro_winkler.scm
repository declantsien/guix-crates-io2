(define-module (crates-io ja ro jaro_winkler) #:use-module (crates-io))

(define-public crate-jaro_winkler-0.1.0 (c (n "jaro_winkler") (v "0.1.0") (d (list (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "eddie") (r "^0.4.2") (d #t) (k 2)) (d (n "strsim") (r "^0.10.0") (d #t) (k 2)))) (h "0pvbydv3yqn9x4ajy56w19j451g6cqp2gvsyshwpa82wnzb06scn")))

