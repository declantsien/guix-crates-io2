(define-module (crates-io ja rn jarn) #:use-module (crates-io))

(define-public crate-jarn-0.1.0 (c (n "jarn") (v "0.1.0") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.4") (f (quote ("history"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.10.0") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "toml") (r "^0.7.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.3") (d #t) (k 0)) (d (n "zip") (r "^0.6.5") (d #t) (k 0)))) (h "1xf9nr7f1ir04fq92ab28z20cxr2hgb2way4xrxdavvh2yq5kbcy")))

