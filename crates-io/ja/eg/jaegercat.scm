(define-module (crates-io ja eg jaegercat) #:use-module (crates-io))

(define-public crate-jaegercat-0.1.0 (c (n "jaegercat") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serdeconv") (r "^0.3") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "sloggers") (r "^0.2") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "110136dbmwkdm1n1myskxqyd4gq1rv23krksvb8vx3fn9kdh0gvf")))

(define-public crate-jaegercat-0.1.1 (c (n "jaegercat") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serdeconv") (r "^0.3") (d #t) (k 0)) (d (n "slog") (r "^2") (f (quote ("release_max_level_debug"))) (d #t) (k 0)) (d (n "sloggers") (r "^0.2") (d #t) (k 0)) (d (n "thrift_codec") (r "^0.1") (d #t) (k 0)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "0vqpbinqrxw1p0wp3fwv8gkfp4rp503w3iiifr9rlvazq0cfcs5z")))

