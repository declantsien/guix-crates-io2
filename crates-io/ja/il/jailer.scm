(define-module (crates-io ja il jailer) #:use-module (crates-io))

(define-public crate-jailer-0.1.0 (c (n "jailer") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (f (quote ("arc_lock"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "0x7f923s84yjxi7msh44bjdah8qf6c4xfpw5qhbdj1abn5v85291")))

(define-public crate-jailer-0.1.1 (c (n "jailer") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12.1") (f (quote ("arc_lock"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.9.0") (d #t) (k 0)))) (h "1bjr7m5qw44q7x9jrll3gf0cpwzbsbxxi94zlq2dj8mkz48wak9a")))

(define-public crate-jailer-0.2.0 (c (n "jailer") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12.1") (f (quote ("arc_lock"))) (d #t) (k 0)) (d (n "tempfile") (r "^3.10.1") (d #t) (k 0)))) (h "1iq6p5gmnj17wpy3zdwiail2d9wndmsaqfy2i8whq9b4wil95mr5")))

