(define-module (crates-io ja nu janus) #:use-module (crates-io))

(define-public crate-janus-0.1.0 (c (n "janus") (v "0.1.0") (d (list (d (n "futures-channel") (r "^0.3") (f (quote ("sink"))) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (d #t) (k 0)))) (h "0j8j4y2r8gjl48kdqkbf3qvhndflzmkk1sab15w9bzdwmq1dx1p1")))

(define-public crate-janus-0.2.0 (c (n "janus") (v "0.2.0") (d (list (d (n "futures-channel") (r "^0.3") (f (quote ("sink"))) (d #t) (k 0)) (d (n "futures-core") (r "^0.3") (d #t) (k 0)) (d (n "futures-sink") (r "^0.3") (d #t) (k 0)) (d (n "futures-util") (r "^0.3") (f (quote ("sink"))) (d #t) (k 0)))) (h "02jvacs41xfs7lg36xzyx48afvbdypxivvqcyahkcygjn5zy7j2g")))

