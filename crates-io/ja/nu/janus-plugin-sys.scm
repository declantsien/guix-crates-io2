(define-module (crates-io ja nu janus-plugin-sys) #:use-module (crates-io))

(define-public crate-janus-plugin-sys-0.1.0 (c (n "janus-plugin-sys") (v "0.1.0") (d (list (d (n "glib-sys") (r "^0.4") (d #t) (k 0)) (d (n "jansson-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0sc5qh2m3mf97mvr2hn9bv87bq1gnjh0pzvj1fwz9j3y9ir4p1js")))

(define-public crate-janus-plugin-sys-0.2.0 (c (n "janus-plugin-sys") (v "0.2.0") (d (list (d (n "glib-sys") (r "^0.4") (d #t) (k 0)) (d (n "jansson-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0p1bbg5blqarlbfniwmw3f52j2bzhfrmkq3bf88wjjqj3zmqdh0c")))

(define-public crate-janus-plugin-sys-0.3.0 (c (n "janus-plugin-sys") (v "0.3.0") (d (list (d (n "glib-sys") (r "^0.4") (d #t) (k 0)) (d (n "jansson-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1s9fhamvmwx39xfynmd8jcg3n3wbm6py8g8fi1p1xrb5cvna45vw") (f (quote (("refcount") ("default"))))))

(define-public crate-janus-plugin-sys-0.4.0 (c (n "janus-plugin-sys") (v "0.4.0") (d (list (d (n "glib-sys") (r "^0.4") (d #t) (k 0)) (d (n "jansson-sys") (r "^0.1.0") (d #t) (k 0)))) (h "0hj7l61a3995s70qi1jpz4xrh0nfw9yv9spgd8sanbhv2al8k0hz") (f (quote (("refcount") ("default"))))))

(define-public crate-janus-plugin-sys-0.5.0 (c (n "janus-plugin-sys") (v "0.5.0") (d (list (d (n "glib-sys") (r "^0.4") (d #t) (k 0)) (d (n "jansson-sys") (r "^0.1.0") (d #t) (k 0)))) (h "19ii1w2zr9rmjzlxzvxz03yha8farfvbv6mgp1q23amnsx5hsdpk") (f (quote (("refcount") ("default"))))))

(define-public crate-janus-plugin-sys-0.6.0 (c (n "janus-plugin-sys") (v "0.6.0") (d (list (d (n "glib-sys") (r "^0.4") (d #t) (k 0)) (d (n "jansson-sys") (r "^0.1.0") (d #t) (k 0)))) (h "19vqcvzb1bc1cn5y6fsh6dmz3kq6mra8q6a8jw46ggigjgw58cjd") (f (quote (("refcount") ("default"))))))

(define-public crate-janus-plugin-sys-0.7.0 (c (n "janus-plugin-sys") (v "0.7.0") (d (list (d (n "glib-sys") (r "^0.9") (d #t) (k 0)) (d (n "jansson-sys") (r "^0.1.0") (d #t) (k 0)))) (h "00m56aizbyg95igf9iw3mp54bvrp7l9frq3djfm7yvghc0567iqz")))

(define-public crate-janus-plugin-sys-0.8.0 (c (n "janus-plugin-sys") (v "0.8.0") (d (list (d (n "glib-sys") (r "^0.10") (d #t) (k 0)) (d (n "jansson-sys") (r "^0.1.0") (d #t) (k 0)))) (h "1jzdsfp3ybm7lsaci2vdn8b08ng3iygwy2k9w32fcdwykp08w03d")))

