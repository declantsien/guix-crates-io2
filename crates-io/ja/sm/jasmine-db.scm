(define-module (crates-io ja sm jasmine-db) #:use-module (crates-io))

(define-public crate-jasmine-db-0.1.1 (c (n "jasmine-db") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.8.1") (d #t) (k 2)) (d (n "libc") (r "^0.2.126") (o #t) (d #t) (k 0)) (d (n "shuttle") (r "^0.1.0") (d #t) (k 2)) (d (n "spin") (r "^0.9.3") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 2)))) (h "1y2fr78h9w82b8nbzd5svh2vwxsqqc57d1hcg63417yy1r60fxhf") (f (quote (("shuttle") ("mmap" "libc"))))))

