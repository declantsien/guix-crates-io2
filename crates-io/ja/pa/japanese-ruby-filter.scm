(define-module (crates-io ja pa japanese-ruby-filter) #:use-module (crates-io))

(define-public crate-japanese-ruby-filter-0.1.0 (c (n "japanese-ruby-filter") (v "0.1.0") (d (list (d (n "pulldown-cmark") (r "^0.8.0") (o #t) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "05pkk11py4fsipj3b52lzsvm9ln16hsrly7ffpl6sv9vfx3scs0p") (f (quote (("pulldown-cmark-filter" "pulldown-cmark"))))))

