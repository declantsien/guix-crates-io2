(define-module (crates-io ja pa japanese) #:use-module (crates-io))

(define-public crate-japanese-0.1.0 (c (n "japanese") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)))) (h "0dij16m9jxbb7r6pglby94rh3pf5hpqfg4ckyfpycsvj3xa07q36")))

(define-public crate-japanese-0.1.1 (c (n "japanese") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "rstest") (r "^0.12.0") (d #t) (k 2)))) (h "0vzwpnfg5pvainfd81lcfpnq33z6lyhmhxyr53rd71z6184k9alp")))

(define-public crate-japanese-0.1.2 (c (n "japanese") (v "0.1.2") (d (list (d (n "maplit") (r "^1.0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.15.0") (d #t) (k 0)) (d (n "rstest") (r "^0.17.0") (d #t) (k 2)))) (h "1f4x5ni25xyzq8v96bxkcls51i7gqqb4zfl4hyww7k2ifk14qs87")))

