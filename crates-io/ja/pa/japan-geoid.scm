(define-module (crates-io ja pa japan-geoid) #:use-module (crates-io))

(define-public crate-japan-geoid-0.2.1 (c (n "japan-geoid") (v "0.2.1") (d (list (d (n "lz4_flex") (r "^0.11.1") (d #t) (k 0)))) (h "0vqk7jb9lj027hlzq35wm0vmbmpkfl1x9hw0p7jrqri0vn8mdmi6") (y #t)))

(define-public crate-japan-geoid-0.3.0 (c (n "japan-geoid") (v "0.3.0") (d (list (d (n "lz4_flex") (r "^0.11.1") (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "1hpcmjy3n6r67d5kwzsnjb8hah1xlvw0hp9bxvv334zqmcv41c7b") (y #t)))

(define-public crate-japan-geoid-0.3.1 (c (n "japan-geoid") (v "0.3.1") (d (list (d (n "lz4_flex") (r "^0.11.1") (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "0z8a5ry8pfp1g6i8p3a96zdmia17969czwd15ppp7jmba4v5sp44") (y #t)))

(define-public crate-japan-geoid-0.3.2 (c (n "japan-geoid") (v "0.3.2") (d (list (d (n "lz4_flex") (r "^0.11.1") (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "07y5f8lgjf17bayw37zmr4dpajp7nwg711hdpcq1nnh18sxw0w8l") (y #t)))

(define-public crate-japan-geoid-0.4.0 (c (n "japan-geoid") (v "0.4.0") (d (list (d (n "lz4_flex") (r "^0.11.1") (k 0)) (d (n "wasm-bindgen") (r "^0.2.84") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.34") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 2)))) (h "157w6z7ml3lf5mr1q4pvzpfg31g6r98b4x63m9dimyc88kpl5xny")))

