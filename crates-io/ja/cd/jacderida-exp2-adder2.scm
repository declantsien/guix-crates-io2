(define-module (crates-io ja cd jacderida-exp2-adder2) #:use-module (crates-io))

(define-public crate-jacderida-exp2-adder2-0.1.0 (c (n "jacderida-exp2-adder2") (v "0.1.0") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.6") (d #t) (k 0)))) (h "1b3bg3b7q2wr8v2vp4d6k1j2b4ccsn7m2ad26sg3a4npsjmzwsxg")))

(define-public crate-jacderida-exp2-adder2-0.1.1 (c (n "jacderida-exp2-adder2") (v "0.1.1") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.7") (d #t) (k 0)))) (h "1cflsd1gs20d0lwarmqgckdniqi58kgjjxpqa652jwvbsk2ch93w")))

(define-public crate-jacderida-exp2-adder2-0.1.2 (c (n "jacderida-exp2-adder2") (v "0.1.2") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.8") (d #t) (k 0)))) (h "0a93sl71flk84aai5vh1ffvhzp2pg20jds1hvcl65lchdwkxicnk")))

(define-public crate-jacderida-exp2-adder2-0.1.3 (c (n "jacderida-exp2-adder2") (v "0.1.3") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.9") (d #t) (k 0)))) (h "0hbhlgk0vvplp9xd3h6z47b7hl3jx2w839wfha7sf6242ax45s9k")))

(define-public crate-jacderida-exp2-adder2-0.1.4 (c (n "jacderida-exp2-adder2") (v "0.1.4") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.10") (d #t) (k 0)))) (h "12hbgck21ivi3i2ghc5azn099d92xnikbcc6xpyhirsdbc8gglk4")))

(define-public crate-jacderida-exp2-adder2-0.1.5 (c (n "jacderida-exp2-adder2") (v "0.1.5") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.11") (d #t) (k 0)))) (h "0ry1hgz83yxg5mhhz9qr7xs4rzpp8645lvs57is22k4q5798b4ds")))

(define-public crate-jacderida-exp2-adder2-0.1.6 (c (n "jacderida-exp2-adder2") (v "0.1.6") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.12") (d #t) (k 0)))) (h "1dwz9bjw34rr9299dfq2cwbc1w7ipdnvd5lwzpry02pdsyap3sqk")))

(define-public crate-jacderida-exp2-adder2-0.1.7 (c (n "jacderida-exp2-adder2") (v "0.1.7") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.13") (d #t) (k 0)))) (h "1hpspc3nrafnmrn992vizml5c9nh94sb2qg94al080pb1xll8frg")))

(define-public crate-jacderida-exp2-adder2-0.1.8 (c (n "jacderida-exp2-adder2") (v "0.1.8") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.14") (d #t) (k 0)))) (h "18n7z8wqrvkcbjj08q3vm3040cmzk8jp9540b30pjp41hf4rp6nc")))

(define-public crate-jacderida-exp2-adder2-0.1.9 (c (n "jacderida-exp2-adder2") (v "0.1.9") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.15") (d #t) (k 0)))) (h "0ry6gym38l5y69x50ldh1sm04ygnkp9c4xzszlfnyz4wyn339r5x")))

(define-public crate-jacderida-exp2-adder2-0.1.10 (c (n "jacderida-exp2-adder2") (v "0.1.10") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.16") (d #t) (k 0)))) (h "0478nnkcl85jy9wk428x14bdigj7rrdc1dv862j5jlym5sxvb7kb")))

(define-public crate-jacderida-exp2-adder2-0.1.11 (c (n "jacderida-exp2-adder2") (v "0.1.11") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.17") (d #t) (k 0)))) (h "0bwgyrsi84ppn8amldhqq8s8b37hbkmd672ccdjk0s40r4h38xb5")))

(define-public crate-jacderida-exp2-adder2-0.1.12 (c (n "jacderida-exp2-adder2") (v "0.1.12") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.18") (d #t) (k 0)))) (h "1z8anr83gw76idnjc001ny3jckf8jyyf4rdf5f5x3hms2f8ghwh9")))

(define-public crate-jacderida-exp2-adder2-0.1.13 (c (n "jacderida-exp2-adder2") (v "0.1.13") (d (list (d (n "jacderida-exp2-add-lib") (r "^0.1.19") (d #t) (k 0)))) (h "08mcxi2lzf7lq48k0ghbnmgk6x74axzqyg6fxq9v6v5b0dvadzrf")))

