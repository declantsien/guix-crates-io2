(define-module (crates-io ja cd jacderida-exp2-add-lib) #:use-module (crates-io))

(define-public crate-jacderida-exp2-add-lib-0.1.0 (c (n "jacderida-exp2-add-lib") (v "0.1.0") (h "0i3gqpr2vry493by4bi3ha9z9rzqa94ig8q814n228vfaarh4jlf")))

(define-public crate-jacderida-exp2-add-lib-0.1.1 (c (n "jacderida-exp2-add-lib") (v "0.1.1") (h "0c1fl345fvh4ss2xfc2ckl8l8llpsx1p4c0xh3f3hw1wkzfz9c24")))

(define-public crate-jacderida-exp2-add-lib-0.1.2 (c (n "jacderida-exp2-add-lib") (v "0.1.2") (h "15pdf4rxbgh186snihhrw1gj925p1n1fsacxpd7hwmq591c2rwb5")))

(define-public crate-jacderida-exp2-add-lib-0.1.3 (c (n "jacderida-exp2-add-lib") (v "0.1.3") (h "183cx64jd3h6hi2msxpmfxv03axk43k1p52af6vxniw4fk1rbidl")))

(define-public crate-jacderida-exp2-add-lib-0.1.4 (c (n "jacderida-exp2-add-lib") (v "0.1.4") (h "1dma14p9vkyy5x00q2673zx3c7xpc4akvvk38fbd5pckkys4mqqp")))

(define-public crate-jacderida-exp2-add-lib-0.1.5 (c (n "jacderida-exp2-add-lib") (v "0.1.5") (h "10fkzsygg6bq5jcc98yhyp29yq218pi98h109b8bv1764wkbslqm")))

(define-public crate-jacderida-exp2-add-lib-0.1.6 (c (n "jacderida-exp2-add-lib") (v "0.1.6") (h "1y938gvcnh5vwm5lp2gm72l1alcaj7rrn036aas9xd3yl2syq15l")))

(define-public crate-jacderida-exp2-add-lib-0.1.7 (c (n "jacderida-exp2-add-lib") (v "0.1.7") (h "0f4avzcl3iw76ancg4cs9675pbk11n9346iq572h7h7aax9hw8av")))

(define-public crate-jacderida-exp2-add-lib-0.1.8 (c (n "jacderida-exp2-add-lib") (v "0.1.8") (h "1vc46h0wmqpqfkzpn2bcgn50x7is3qz0nw0qpzplf8x9q6fr5xhw")))

(define-public crate-jacderida-exp2-add-lib-0.1.9 (c (n "jacderida-exp2-add-lib") (v "0.1.9") (h "03pl9m7r5xblv9wbbgsw0c145h9q01bv4vb4qvd3jz1va6pi6ihv")))

(define-public crate-jacderida-exp2-add-lib-0.1.10 (c (n "jacderida-exp2-add-lib") (v "0.1.10") (h "18vbg0zk2bzkjj1fv5dbllsxwf4kg6rassdkvwb35jfppd865k42")))

(define-public crate-jacderida-exp2-add-lib-0.1.11 (c (n "jacderida-exp2-add-lib") (v "0.1.11") (h "1qk975v5ahgbc3a9khqfjc1nlp2703jyzv3k4xiyzqbxvl1z36zr")))

(define-public crate-jacderida-exp2-add-lib-0.1.12 (c (n "jacderida-exp2-add-lib") (v "0.1.12") (h "0gh9d9d4v6d82gswrkx5l4qg4yzph3c85in5bz092jkvgxh7b487")))

(define-public crate-jacderida-exp2-add-lib-0.1.13 (c (n "jacderida-exp2-add-lib") (v "0.1.13") (h "0n96v1vwmvdv168xmd4wihxzlf2hbzgzhgbv10bsmihbxsf23dr2")))

(define-public crate-jacderida-exp2-add-lib-0.1.14 (c (n "jacderida-exp2-add-lib") (v "0.1.14") (h "0m3qvs64jvks237vf5hlpwqv1m7yskvrqp44fmvffz82rzf469wx")))

(define-public crate-jacderida-exp2-add-lib-0.1.15 (c (n "jacderida-exp2-add-lib") (v "0.1.15") (h "0d9ynmk6gczhz4bg32wnasa739fgzyyk2w19z2aqcpfzrsvhv76w")))

(define-public crate-jacderida-exp2-add-lib-0.1.16 (c (n "jacderida-exp2-add-lib") (v "0.1.16") (h "0ma6f9f9q11571vkzzrhb3kdvfj164ih549q92m19kga8q9nyyx3")))

(define-public crate-jacderida-exp2-add-lib-0.1.17 (c (n "jacderida-exp2-add-lib") (v "0.1.17") (h "0mq68y8millgymya9bagm8vfskcchdmc8zd73lm61jk6m56dp3g1")))

(define-public crate-jacderida-exp2-add-lib-0.1.18 (c (n "jacderida-exp2-add-lib") (v "0.1.18") (h "1yv6b485iy199zs3qjjpi5w4y17xwah8vb1z9wk8f99bikgigc0j")))

(define-public crate-jacderida-exp2-add-lib-0.1.19 (c (n "jacderida-exp2-add-lib") (v "0.1.19") (h "08hclv9cmz8m129mmyn3d9zd2zw71j50dxq0mnbc04c1yvmrz2nr")))

