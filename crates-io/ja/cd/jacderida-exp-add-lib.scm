(define-module (crates-io ja cd jacderida-exp-add-lib) #:use-module (crates-io))

(define-public crate-jacderida-exp-add-lib-0.1.0 (c (n "jacderida-exp-add-lib") (v "0.1.0") (h "14hi995yghvla6fl2yzwqpal3hccc913aq2kl50wm4dlkkimf2r9")))

(define-public crate-jacderida-exp-add-lib-0.1.1 (c (n "jacderida-exp-add-lib") (v "0.1.1") (h "1binzlzjzalwsnqbp2jw0kspi8g9qf3qjz9bd2yazs4452xljndf")))

