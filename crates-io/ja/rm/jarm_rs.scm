(define-module (crates-io ja rm jarm_rs) #:use-module (crates-io))

(define-public crate-jarm_rs-0.1.0 (c (n "jarm_rs") (v "0.1.0") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)))) (h "0wigsl8vikyd0lgryp91g0hfy9a28gvr9lxw2ywl7aqxl6zvfi0r") (y #t)))

(define-public crate-jarm_rs-0.1.1 (c (n "jarm_rs") (v "0.1.1") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)))) (h "0wc26wx1msbz8219f45anzyrrf7q24jdxcsgc91w4wjhljafrjig") (y #t)))

(define-public crate-jarm_rs-0.1.2 (c (n "jarm_rs") (v "0.1.2") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)))) (h "1la80b187pgjizlnkyk4qb8xppbwz7pwsdxp4hqdljvbmlz8kk6a") (y #t)))

(define-public crate-jarm_rs-0.1.3 (c (n "jarm_rs") (v "0.1.3") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)))) (h "16kgg69lqr782s37b114fm18yivqqx1j2670rjnqf670vadyf06i") (y #t)))

(define-public crate-jarm_rs-0.1.4 (c (n "jarm_rs") (v "0.1.4") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)))) (h "09a706r3hwb2k6lv3vchlc766l9j9lyw7wqa8qrh47m63mdyq56y") (y #t)))

(define-public crate-jarm_rs-0.1.5 (c (n "jarm_rs") (v "0.1.5") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)))) (h "06gzslwjd9dj8f8ik8xw7ixblgih9f7sqs9n7v7cfwm5srwi3x48") (y #t)))

(define-public crate-jarm_rs-0.1.6 (c (n "jarm_rs") (v "0.1.6") (d (list (d (n "error-chain") (r "^0.12.4") (d #t) (k 0)))) (h "1wmd1r4vlhsd29a2xcd7ync4ihgzaw7wqa0l8wm19aqw3iqragd3")))

