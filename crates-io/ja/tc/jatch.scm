(define-module (crates-io ja tc jatch) #:use-module (crates-io))

(define-public crate-jatch-0.1.0 (c (n "jatch") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0ppy48hl8wca5z5fhmv9h1rkdykxqbfhl72zmd4mfm1m8kvvgbv3")))

(define-public crate-jatch-0.1.1 (c (n "jatch") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0jnllr8bnfrmngj8jhgi1bcbmszmshd3a8pyzvczb113g1iskdyh")))

