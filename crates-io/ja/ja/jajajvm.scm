(define-module (crates-io ja ja jajajvm) #:use-module (crates-io))

(define-public crate-jajajvm-0.1.0 (c (n "jajajvm") (v "0.1.0") (h "0dsxqbbznjxg347wdc6k26v8c10b7p378011n5h5ikfxqgw4f6rp")))

(define-public crate-jajajvm-0.1.1 (c (n "jajajvm") (v "0.1.1") (h "1gbapzib6i268z6rv23mx9lkhn8r7i5hb39hd6gxyg7lilr0zcis")))

