(define-module (crates-io ja cc jacc) #:use-module (crates-io))

(define-public crate-jacc-0.1.0 (c (n "jacc") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "clap") (r "^4.4.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ii89b6b1j6psybf7vhvzxrhh6ad456a7wp7k4iws51c5xw7f9pi")))

