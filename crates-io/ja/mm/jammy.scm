(define-module (crates-io ja mm jammy) #:use-module (crates-io))

(define-public crate-jammy-0.0.1 (c (n "jammy") (v "0.0.1") (d (list (d (n "bevy") (r "^0.10.1") (d #t) (k 2)) (d (n "iyes_progress") (r "^0.8") (d #t) (k 2)) (d (n "jammy_splash_screen") (r "^0.0.1") (d #t) (k 0)))) (h "0zc50wyrrqwwj2wpk7r3azaf95grd9ngxprcr8qg992z9vnarki5")))

