(define-module (crates-io ja mm jamml) #:use-module (crates-io))

(define-public crate-jamml-0.1.0 (c (n "jamml") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "1nkhxw7qcjb1wh9wzyl9p7f5h7sx8nldrkhh3ffbhjir4hhhlgnw")))

(define-public crate-jamml-0.1.1 (c (n "jamml") (v "0.1.1") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 2)))) (h "01w0vl4fn4m7by9dh6srqg991226l21g9gbdji2c909ki25as4sz")))

(define-public crate-jamml-0.1.2 (c (n "jamml") (v "0.1.2") (d (list (d (n "num-traits") (r "^0.2.6") (d #t) (k 0)) (d (n "rand") (r "^0.6.1") (d #t) (k 0)))) (h "0s93syldfs0ylxcwk6ab16azkx9r1q1rv184110kawvahvp8ylhm")))

