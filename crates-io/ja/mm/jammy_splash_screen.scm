(define-module (crates-io ja mm jammy_splash_screen) #:use-module (crates-io))

(define-public crate-jammy_splash_screen-0.0.1 (c (n "jammy_splash_screen") (v "0.0.1") (d (list (d (n "bevy") (r "^0.10.1") (f (quote ("bevy_core_pipeline" "bevy_render" "bevy_sprite"))) (k 0)) (d (n "iyes_progress") (r "^0.8") (d #t) (k 0)))) (h "0gc59x8cajcndqk6a68c3zacblwczyhmc5niyk8qjsxls0hvm833")))

