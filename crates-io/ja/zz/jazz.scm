(define-module (crates-io ja zz jazz) #:use-module (crates-io))

(define-public crate-jazz-0.1.0 (c (n "jazz") (v "0.1.0") (d (list (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1kx0hc9yp158dfqv7vqvc41d9xgw5gj3iy9ibv3xkf77kzgvhmac")))

(define-public crate-jazz-0.1.5 (c (n "jazz") (v "0.1.5") (d (list (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "14ywkxzj8ipfbcbj8gvczfp5sqks4737fsv32n2cf7mbisl6i37g")))

(define-public crate-jazz-0.1.6 (c (n "jazz") (v "0.1.6") (d (list (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0f3pd0m1lvw34b31417vrh2pczz26amid0fgfcs94w5h0ffvl5v3")))

(define-public crate-jazz-0.1.7 (c (n "jazz") (v "0.1.7") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1vzx30xmi24kwr5x2vi1yjnx0pa4fiisim1ikqld76vsx19yydmf")))

(define-public crate-jazz-0.1.8 (c (n "jazz") (v "0.1.8") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0z1nymvviqgw0jrjkngd3392aj1a3ggdc35b5zamg7i84m5909my")))

(define-public crate-jazz-0.1.89 (c (n "jazz") (v "0.1.89") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "00y8brnsgpvk0vg9rhcpwxgi7b430q01m3dp9m3rqm6mk2d5v850")))

(define-public crate-jazz-0.1.90 (c (n "jazz") (v "0.1.90") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0fziysyksvhvqqqv45zix8l7hlc77jbwsl9l75vnhz9z3j6990z4")))

(define-public crate-jazz-0.1.91 (c (n "jazz") (v "0.1.91") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "simple_jazz") (r "^0.1.0") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "0h93ljpryy86dwgaxjm4iyn7xfkf2a22yr8q6j1vrm44dy7vxiwx")))

(define-public crate-jazz-0.2.0 (c (n "jazz") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "09aqljcn739wa3jynzcv6ixv9ihd9mwzjxlxhgzmp4zyrnkck0ih")))

(define-public crate-jazz-0.2.1 (c (n "jazz") (v "0.2.1") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "1b37x7qdnradapzgw89acv1rqw4y67x8d4zvxsf4vfsw551ymbq0")))

(define-public crate-jazz-0.2.2 (c (n "jazz") (v "0.2.2") (d (list (d (n "libc") (r "^0.2.43") (d #t) (k 0)) (d (n "time") (r "^0.1.40") (d #t) (k 0)))) (h "018v97gbn6abw32lzp6lgzd6l297brflc186jzd8wi9k28ifh7ww")))

