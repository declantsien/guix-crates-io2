(define-module (crates-io ja zz jazz_vm) #:use-module (crates-io))

(define-public crate-jazz_vm-0.1.0 (c (n "jazz_vm") (v "0.1.0") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "1gxfzxz6f1jg6pwk3p4qagk249fh8ccrraing87c28gzgjii1ji3")))

(define-public crate-jazz_vm-0.1.5 (c (n "jazz_vm") (v "0.1.5") (d (list (d (n "bincode") (r "^0.9") (d #t) (k 0)) (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "smallvec") (r "^0.6") (d #t) (k 0)))) (h "0md5z9dx9kmi6ndcwzwikd4i9crdca8l6lh0gsnkfl18v6z1ib6j")))

