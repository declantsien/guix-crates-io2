(define-module (crates-io ja ph japhonex) #:use-module (crates-io))

(define-public crate-japhonex-0.1.0 (c (n "japhonex") (v "0.1.0") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "05k03c084cj058mbqny2r3qcj5m3yb4jsyzp7m0v660yc1l0cdax")))

(define-public crate-japhonex-0.1.1 (c (n "japhonex") (v "0.1.1") (d (list (d (n "regex") (r "^1.5") (d #t) (k 0)))) (h "1myh7khzfhkh2k54zrcn43clq3ng7a81pdcn17d5yz9rf45jjig0")))

