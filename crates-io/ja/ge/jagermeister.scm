(define-module (crates-io ja ge jagermeister) #:use-module (crates-io))

(define-public crate-jagermeister-0.1.0 (c (n "jagermeister") (v "0.1.0") (d (list (d (n "async-std") (r "^1.6.3") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "prost") (r "^0.6") (d #t) (k 0)) (d (n "prost-build") (r "^0.6") (d #t) (k 1)) (d (n "surf") (r "^1.0.3") (d #t) (k 0)) (d (n "tide") (r "^0.13.0") (d #t) (k 0)))) (h "1z4s3gcrsfzra8mr8rs08hr3sxzqpzgdfnh82b05kr34xjhy33rk")))

