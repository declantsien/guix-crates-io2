(define-module (crates-io ja ap jaaptools) #:use-module (crates-io))

(define-public crate-jaaptools-0.1.0 (c (n "jaaptools") (v "0.1.0") (h "18f3b4yihxsk40ksc1fp0l03mrfwxf34xda6nf6fc6lpkls3hmqy")))

(define-public crate-jaaptools-0.1.1 (c (n "jaaptools") (v "0.1.1") (h "0wfbqwvvqvqg3sic13j1ywn0x6hdnvly7nn07n9d66sfybjc30q5")))

(define-public crate-jaaptools-0.1.2 (c (n "jaaptools") (v "0.1.2") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0qqgyjqa1529p7hszhchsh9c7bjhbakz4h5xil90qcxx3m3hbb3x")))

(define-public crate-jaaptools-0.1.3 (c (n "jaaptools") (v "0.1.3") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "04qad0gswyaflhajwcf6a73rmima7jwzlp6sapqp47mg84r4wi0n")))

(define-public crate-jaaptools-0.1.4 (c (n "jaaptools") (v "0.1.4") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "15wmvf924pvy2vrcfqkcr4xw7ljzdkjvfgp1zris08x8wr50yh27")))

(define-public crate-jaaptools-0.1.5 (c (n "jaaptools") (v "0.1.5") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "0xdifa5sl9xprv50pjqcsg5h3lrlzywn4d4kixfj6rbc4h9bfghs")))

(define-public crate-jaaptools-0.1.6 (c (n "jaaptools") (v "0.1.6") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "08ai036j1n9yyl3mmdg3i92khvfjhfplkw697mpnr3jdf35azmv0")))

(define-public crate-jaaptools-0.1.7 (c (n "jaaptools") (v "0.1.7") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "15jc10jwg4x3r58imcf0pmj6ic60jlxryg0hfdg5qa8qhmzfjcc9")))

(define-public crate-jaaptools-0.1.8 (c (n "jaaptools") (v "0.1.8") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1qw2m2qy1qrljdhz1r4piip3jq53l4hnwmj771pdb3g69j7z7brp")))

(define-public crate-jaaptools-0.1.9 (c (n "jaaptools") (v "0.1.9") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "03x0yp7vhc8bjxdkml9dm6cljrmmnfm07r5pil8392ryib70p87d")))

(define-public crate-jaaptools-0.1.10 (c (n "jaaptools") (v "0.1.10") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1savpazp3h3bis35bfrz6nzla44mxj6m7g12y57lzh0srn2mg0bg")))

(define-public crate-jaaptools-0.1.11 (c (n "jaaptools") (v "0.1.11") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "151894bwkwk503n5w3819kyyslp92n95y1kalm52a2vcl6cq90dj")))

(define-public crate-jaaptools-0.1.12 (c (n "jaaptools") (v "0.1.12") (d (list (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "num") (r "^0.4.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "09rc85c5vkn08vsv0lzi526rmmyi8k3mnrkkslk96xq7r4x1zgbm")))

