(define-module (crates-io ja la jalali-date) #:use-module (crates-io))

(define-public crate-jalali-date-0.1.0 (c (n "jalali-date") (v "0.1.0") (h "03r8azvim9493cn0li3bffzrdfc2r9vgfsg3p2i0jpp6isxyqbwk")))

(define-public crate-jalali-date-0.2.0 (c (n "jalali-date") (v "0.2.0") (h "01zc7nw2y0wp8b9k9af2ygmzhvhk0k2r6b3hbciw7zzc5jyqkpnl")))

