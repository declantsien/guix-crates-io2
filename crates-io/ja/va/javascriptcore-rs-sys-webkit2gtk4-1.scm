(define-module (crates-io ja va javascriptcore-rs-sys-webkit2gtk4-1) #:use-module (crates-io))

(define-public crate-javascriptcore-rs-sys-webkit2gtk4-1-0.4.0 (c (n "javascriptcore-rs-sys-webkit2gtk4-1") (v "0.4.0") (d (list (d (n "glib-sys") (r "^0.16") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "153plwwq0mwsvzs9ddsvgsn0ai9jzccfvkqlc65sfi3ah832h8hm") (f (quote (("v2_28") ("dox")))) (y #t)))

