(define-module (crates-io ja va java-locator) #:use-module (crates-io))

(define-public crate-java-locator-0.1.0 (c (n "java-locator") (v "0.1.0") (d (list (d (n "docopt") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)))) (h "02g0wrkgmzdmrgwh94dgvklkrn099xqx03axyv8fiilkh1x81r6h") (f (quote (("build-binary" "docopt"))))))

(define-public crate-java-locator-0.1.1 (c (n "java-locator") (v "0.1.1") (d (list (d (n "docopt") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)))) (h "1ay0lxsnbafh612fxhmmghmhz2lvjjjric0dwg4f48kj8sa35cip") (f (quote (("build-binary" "docopt"))))))

(define-public crate-java-locator-0.1.2 (c (n "java-locator") (v "0.1.2") (d (list (d (n "docopt") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.2") (d #t) (k 0)) (d (n "lazy_static") (r "^1.2") (d #t) (k 0)))) (h "1pzrl07j41ahlsylbj1ppxfd3rpciarn8vdb7dnizrbmyy9jjh32") (f (quote (("build-binary" "docopt"))))))

(define-public crate-java-locator-0.1.3 (c (n "java-locator") (v "0.1.3") (d (list (d (n "docopt") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1nlka33smw5xpvnn7f7qsxrcxa8dgnimryggxjflchm602fprjvj") (f (quote (("build-binary" "docopt"))))))

(define-public crate-java-locator-0.1.5 (c (n "java-locator") (v "0.1.5") (d (list (d (n "docopt") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1hl06w62yzvfzrl6pwgzssx8006s508hylnq44n22by5v4pky04h") (f (quote (("build-binary" "docopt"))))))

(define-public crate-java-locator-0.1.6 (c (n "java-locator") (v "0.1.6") (d (list (d (n "docopt") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "14r1d33g514a362f6zkp74v57nq9q6z8rylmswngi8vyxwa8wrbc") (f (quote (("build-binary" "docopt"))))))

(define-public crate-java-locator-0.1.7 (c (n "java-locator") (v "0.1.7") (d (list (d (n "docopt") (r "^1.1") (o #t) (d #t) (k 0)) (d (n "glob") (r "^0.3") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "1zckkd0sgh9w9iz169wr4r5dj6yggxl2cr2s800mw74nv6myrayj") (f (quote (("build-binary" "docopt"))))))

