(define-module (crates-io ja va java_random) #:use-module (crates-io))

(define-public crate-java_random-0.1.0 (c (n "java_random") (v "0.1.0") (h "18mzh2lg6zy1ivhm4irqsvxf8nyfpvwnrc1snz0vfn0g82fzyhbx")))

(define-public crate-java_random-0.1.1 (c (n "java_random") (v "0.1.1") (h "0ck3hxk7bnlnmwsp9lziw2q0j80kynqnn1isxjclqv55p3nzsb36")))

(define-public crate-java_random-0.1.2 (c (n "java_random") (v "0.1.2") (h "1gyjvjq71ina4a7xzbb7d8a05i6mr9d2gyav99m2xgj9yg9h08ck")))

(define-public crate-java_random-0.1.3 (c (n "java_random") (v "0.1.3") (h "1c6i7nm9wvz8xcfdn6zxf3dm80sp7wy3balfhzb6rar7a9bq3v86")))

(define-public crate-java_random-0.1.4 (c (n "java_random") (v "0.1.4") (h "0bpsg9bkmn55al49fp5q1sah7j92ygiwv0nxxbbfrqz3yxvgjv6i")))

(define-public crate-java_random-0.1.5 (c (n "java_random") (v "0.1.5") (h "0jfily3ar4krfsaw3lmz94jpg8m1fg2pj3dp1y2301srw3qf98sq")))

(define-public crate-java_random-0.1.6 (c (n "java_random") (v "0.1.6") (h "10c8m5v12qx9b6l0b1kxzb3s0pj4zk5ac9wm4qs0abxq4vynjdfm")))

(define-public crate-java_random-0.1.7 (c (n "java_random") (v "0.1.7") (h "0wl2rckh201kbg529zsgq4anr9d51nkckn0jhnd64qwfk5vvrmvr") (f (quote (("default" "const_fn") ("const_fn"))))))

