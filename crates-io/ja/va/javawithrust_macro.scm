(define-module (crates-io ja va javawithrust_macro) #:use-module (crates-io))

(define-public crate-javawithrust_macro-0.1.0 (c (n "javawithrust_macro") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02gpkjwcffgxcpr81v0whfp4q37vl78jfc7m4k9fbf08g2as8dm7")))

(define-public crate-javawithrust_macro-0.2.0 (c (n "javawithrust_macro") (v "0.2.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0a5zscvvmcbpailild6ffs8hcgx86mbk6bq58plpawc8dr4mky40")))

