(define-module (crates-io ja va java_classpaths) #:use-module (crates-io))

(define-public crate-java_classpaths-0.0.2 (c (n "java_classpaths") (v "0.0.2") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)) (d (n "url") (r "^2.3.1") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "1yf18h4b78jk6mjwpyjlf56xia0dsvcn64jm2saz6mx15gyxwyvm")))

