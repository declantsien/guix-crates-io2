(define-module (crates-io ja va javarandom) #:use-module (crates-io))

(define-public crate-javarandom-0.1.0 (c (n "javarandom") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13hl5krg79rjhq0lywr5c4iby4nwpbhl1rjxfybvrs1snrzb1ja5")))

(define-public crate-javarandom-0.2.0 (c (n "javarandom") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1q6nqrx7ylmdc8aiwpdjjh08zi3b5p88j8q2zpyx8hwjrg3mdm4v")))

(define-public crate-javarandom-0.2.1 (c (n "javarandom") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0p248wm891mpv2ilv8jf95ls26iyxdhsx5ynh4r3br3hmlizv6wz")))

