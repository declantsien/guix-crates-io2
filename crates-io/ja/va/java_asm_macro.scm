(define-module (crates-io ja va java_asm_macro) #:use-module (crates-io))

(define-public crate-java_asm_macro-0.0.1 (c (n "java_asm_macro") (v "0.0.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "12xby5s74rfjn6bp5gd7pxah93gsax87v6bzsxnrafm124nblk15")))

(define-public crate-java_asm_macro-0.0.2 (c (n "java_asm_macro") (v "0.0.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w64nkbb3fm90csd6wgb4chss3p2qd9fs2w6351yhiw9fwfsnz7k")))

(define-public crate-java_asm_macro-0.0.3 (c (n "java_asm_macro") (v "0.0.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ia6bp0q0mykgryxjm3703ppjh9kg6wnppjvgfbv22jaxh85hdcm")))

(define-public crate-java_asm_macro-0.0.4 (c (n "java_asm_macro") (v "0.0.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0qnffkijf7ahbqgk974bkq1lhxnh7fz3w5vbkl4dsmj9xp3vns6p")))

