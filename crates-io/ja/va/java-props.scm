(define-module (crates-io ja va java-props) #:use-module (crates-io))

(define-public crate-java-props-0.1.0 (c (n "java-props") (v "0.1.0") (h "039dll7pdq6vahzl263z16xhwa6swm744cqwc8m2gd13k54c78mp")))

(define-public crate-java-props-0.1.1 (c (n "java-props") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "0k7ii3llvw54cpw8ngghhwnjirwwjzl1bbm6wm1k5gfvd7bg1p7r")))

(define-public crate-java-props-0.1.2 (c (n "java-props") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)))) (h "1z0jwidr3d36b6qxs8gkm2h464j3ssz7c9063zc4py2sb992hbs5")))

