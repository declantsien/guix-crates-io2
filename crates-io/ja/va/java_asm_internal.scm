(define-module (crates-io ja va java_asm_internal) #:use-module (crates-io))

(define-public crate-java_asm_internal-0.0.1 (c (n "java_asm_internal") (v "0.0.1") (d (list (d (n "java_asm_macro") (r "^0.0.1") (d #t) (k 0)))) (h "03k4jc2pk9gj0mx98q3a3il3jk1rp7liy1wdzs0q91acdvsqsjl9")))

(define-public crate-java_asm_internal-0.0.2 (c (n "java_asm_internal") (v "0.0.2") (d (list (d (n "java_asm_macro") (r "^0.0.2") (d #t) (k 0)))) (h "1am4wd3wbx0r5x783pfznr9zqimwc2f6im2wi2gq22n0cand8sbq")))

(define-public crate-java_asm_internal-0.0.3 (c (n "java_asm_internal") (v "0.0.3") (d (list (d (n "java_asm_macro") (r "^0.0.3") (d #t) (k 0)))) (h "18dmp988f0kbabqckrjz942vs6a4v1qnnai1cv4kk55n1m0jm5f4")))

(define-public crate-java_asm_internal-0.0.4 (c (n "java_asm_internal") (v "0.0.4") (d (list (d (n "java_asm_macro") (r "^0.0.4") (d #t) (k 0)))) (h "0kd0ndx7z210x360rrjl1gsgszxbr0sxxnfj0706z3smy5w0rbp0")))

