(define-module (crates-io ja va javascriptcore5-rs) #:use-module (crates-io))

(define-public crate-javascriptcore5-rs-0.15.3 (c (n "javascriptcore5-rs") (v "0.15.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.3.0") (d #t) (k 0) (p "javascriptcore5-rs-sys")) (d (n "glib") (r "^0.14.0") (d #t) (k 0)))) (h "128wh15w7zrkgpa4fsg3iq8b2igw7jskw3rfbxhshp6sczndmv9p") (f (quote (("v2_28" "ffi/v2_28") ("dox" "ffi/dox")))) (y #t)))

(define-public crate-javascriptcore5-rs-0.1.0 (c (n "javascriptcore5-rs") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1.0") (d #t) (k 0) (p "javascriptcore5-rs-sys")) (d (n "glib") (r "^0.14.0") (d #t) (k 0)))) (h "01qphvhc9qwzr5zmxcqz6rprvclfvf5rz0ira98c0b1y6566fips") (f (quote (("v2_28" "ffi/v2_28") ("dox" "ffi/dox"))))))

(define-public crate-javascriptcore5-rs-0.1.1 (c (n "javascriptcore5-rs") (v "0.1.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1.1") (d #t) (k 0) (p "javascriptcore5-rs-sys")) (d (n "glib") (r "^0.14.0") (d #t) (k 0)))) (h "1pb2h93r8hg4qqyb4h6bkcmp45bxparlb02r5235g7zflwwg57mq") (f (quote (("v2_28" "ffi/v2_28") ("dox" "ffi/dox"))))))

(define-public crate-javascriptcore5-rs-0.1.2 (c (n "javascriptcore5-rs") (v "0.1.2") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1.2") (d #t) (k 0) (p "javascriptcore5-rs-sys")) (d (n "glib") (r "^0.14.0") (d #t) (k 0)))) (h "0smfaxhw34yail30ig833x0p8vk6ha57sychmwkchlzh3n6gjp1x") (f (quote (("v2_28" "ffi/v2_28") ("dox" "ffi/dox"))))))

(define-public crate-javascriptcore5-rs-0.1.3 (c (n "javascriptcore5-rs") (v "0.1.3") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1.3") (d #t) (k 0) (p "javascriptcore5-rs-sys")) (d (n "glib") (r "^0.15") (d #t) (k 0)))) (h "1dyn37nyqay099m5i2avcbwlkw562jqcqd39fm9sb1whfz92950r") (f (quote (("v2_28" "ffi/v2_28") ("dox" "ffi/dox"))))))

(define-public crate-javascriptcore5-rs-0.1.4 (c (n "javascriptcore5-rs") (v "0.1.4") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1.3") (d #t) (k 0) (p "javascriptcore5-rs-sys")) (d (n "glib") (r "^0.15") (d #t) (k 0)))) (h "09pqjyan0yf5c6xyd4p8j58h2m8hw8gvrkqgiimwv585z1nc9052") (f (quote (("v2_28" "ffi/v2_28") ("dox" "ffi/dox"))))))

(define-public crate-javascriptcore5-rs-0.2.0 (c (n "javascriptcore5-rs") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.2") (d #t) (k 0) (p "javascriptcore5-rs-sys")) (d (n "glib") (r "^0.16") (d #t) (k 0)))) (h "04ghra7hlwvcv4n68sxdznflia0jrr9d8br6mhr7kbgzpp7170yx") (f (quote (("v2_38" "v2_28" "ffi/v2_38") ("v2_28" "ffi/v2_28") ("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-javascriptcore5-rs-0.3.1 (c (n "javascriptcore5-rs") (v "0.3.1") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.3") (d #t) (k 0) (p "javascriptcore5-rs-sys")) (d (n "glib") (r "^0.17") (d #t) (k 0)))) (h "0x7a4azi51h6hqpihvs6z2lw3vzlh88rl4qawp5zy57k68szk2wc") (f (quote (("v2_38" "v2_28" "ffi/v2_38") ("v2_28" "ffi/v2_28") ("dox" "ffi/dox" "glib/dox"))))))

