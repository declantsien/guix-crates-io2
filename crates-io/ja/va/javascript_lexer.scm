(define-module (crates-io ja va javascript_lexer) #:use-module (crates-io))

(define-public crate-javascript_lexer-0.1.0 (c (n "javascript_lexer") (v "0.1.0") (d (list (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "1giim23lmi8iipav6j0fzimvqvci07q6mmjyx6f4bywnck2idlgm") (f (quote (("default"))))))

(define-public crate-javascript_lexer-0.1.1 (c (n "javascript_lexer") (v "0.1.1") (d (list (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "0n0wr4slh99yhgvz2fh43m0gmb73kmyv8gkswxrc22rs716rgf97") (f (quote (("default")))) (y #t)))

(define-public crate-javascript_lexer-0.1.2 (c (n "javascript_lexer") (v "0.1.2") (d (list (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "1qp18c55kbs7sy3fyglxmp8d05pam5iz027kmiys83d8v2jmyj3x") (f (quote (("default"))))))

(define-public crate-javascript_lexer-0.1.3 (c (n "javascript_lexer") (v "0.1.3") (d (list (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "06m6ksil6n03r63is5xhzfg2f2gvmak26nvi2q78c5p7wgshb8f2") (f (quote (("default"))))))

(define-public crate-javascript_lexer-0.1.4 (c (n "javascript_lexer") (v "0.1.4") (d (list (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "1mk5r87hdwswix1ri4favkw4nd27v595bhf6702mrj6hi850q5c7") (f (quote (("default"))))))

(define-public crate-javascript_lexer-0.1.5 (c (n "javascript_lexer") (v "0.1.5") (d (list (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "1ymxz8ypgy87cgbxc4j9bc9d556mzdhqbrlz4parjdkpq5dc2097") (f (quote (("default"))))))

(define-public crate-javascript_lexer-0.1.6 (c (n "javascript_lexer") (v "0.1.6") (d (list (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "1gl6fffffj5mzas6kbvmczn1jkqnklv9r0lfig5hnggdgp6bc72a") (f (quote (("default"))))))

(define-public crate-javascript_lexer-0.1.7 (c (n "javascript_lexer") (v "0.1.7") (d (list (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "0ak8dyyasdpi96qb0rz26cb09aj7wx6r2b201jk7087z51q9k7bp") (f (quote (("default"))))))

(define-public crate-javascript_lexer-0.1.8 (c (n "javascript_lexer") (v "0.1.8") (d (list (d (n "internship") (r "^0.6.0") (d #t) (k 0)) (d (n "phf") (r "^0.7.24") (f (quote ("macros"))) (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.5") (d #t) (k 2)))) (h "0zwd5yb1044gadinf8fzcfpbrx6hz23h4il684vyjwwxl50hfkhl") (f (quote (("default"))))))

