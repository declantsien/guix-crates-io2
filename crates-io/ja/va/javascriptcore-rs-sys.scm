(define-module (crates-io ja va javascriptcore-rs-sys) #:use-module (crates-io))

(define-public crate-javascriptcore-rs-sys-0.1.0 (c (n "javascriptcore-rs-sys") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1355hsfv68hivqqg068w9hgfalfrm3rw4x3vk8kj2gyyjy0kpbiz")))

(define-public crate-javascriptcore-rs-sys-0.1.1 (c (n "javascriptcore-rs-sys") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1kr6kd9if8znwbw13zxnb4r9r972zmk4iy9hghs0g8v2sl3qkq5y")))

(define-public crate-javascriptcore-rs-sys-0.1.2 (c (n "javascriptcore-rs-sys") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "19cxza087ak3sqcwdbfc8rmyzspjfqy96dqrsm7dhbnwxwjxnir7")))

(define-public crate-javascriptcore-rs-sys-0.2.0 (c (n "javascriptcore-rs-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1619vl48yw3wrjw4c3dfm3s5s9sizgxp5s7s1ahpbkcdl2lasiiz")))

(define-public crate-javascriptcore-rs-sys-0.3.0 (c (n "javascriptcore-rs-sys") (v "0.3.0") (d (list (d (n "glib-sys") (r "^0.14") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.14") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^5") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1wnapb271d9zky3phbasqlgnxq27m30c2lb6hl1kjzx8clgjcxbc") (f (quote (("v2_28") ("dox"))))))

(define-public crate-javascriptcore-rs-sys-0.3.1 (c (n "javascriptcore-rs-sys") (v "0.3.1") (d (list (d (n "glib-sys") (r "^0.14") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.14") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^5") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0phnhzbq68w9xfynby9ljs79pmazzyy6sb8l5nlikd8ab0zzb93r") (f (quote (("v2_28") ("dox"))))))

(define-public crate-javascriptcore-rs-sys-0.3.3 (c (n "javascriptcore-rs-sys") (v "0.3.3") (d (list (d (n "glib-sys") (r "^0.14") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.14") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^5") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1qp9vk1sc8bqsiclqn3bfsdf8pg9wzs5knhpc1ndfy5i4kl2vpra") (f (quote (("v2_28") ("dox"))))))

(define-public crate-javascriptcore-rs-sys-0.4.0 (c (n "javascriptcore-rs-sys") (v "0.4.0") (d (list (d (n "glib-sys") (r "^0.15") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.15") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^5") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0g3rwwaz3wsyj5z0apcc03rk2r6llx77wlv969pdwp4w863vnpwh") (f (quote (("v2_28") ("dox"))))))

(define-public crate-javascriptcore-rs-sys-0.5.0 (c (n "javascriptcore-rs-sys") (v "0.5.0") (d (list (d (n "glib-sys") (r "^0.16") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "1kkv1d8bkkxmq15p5sgxljhpgc0ypkfgfyb3dlyy08spm6r3a2p1") (f (quote (("v2_28") ("dox"))))))

(define-public crate-javascriptcore-rs-sys-0.5.1 (c (n "javascriptcore-rs-sys") (v "0.5.1") (d (list (d (n "glib-sys") (r "^0.16") (d #t) (k 0)) (d (n "gobject-sys") (r "^0.16") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "0v4iixanzm9yhs75x5gk8z6v3z9w0bqx62isfcd99kajk98id8lq") (f (quote (("v2_28") ("dox"))))))

(define-public crate-javascriptcore-rs-sys-1.0.0 (c (n "javascriptcore-rs-sys") (v "1.0.0") (d (list (d (n "glib") (r "^0.16") (d #t) (k 0) (p "glib-sys")) (d (n "gobject") (r "^0.16") (d #t) (k 0) (p "gobject-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "14dswkgdz7yfk25gi4l0w1nmsy8zqwnw80ny304hr6mbnfjkp65h") (f (quote (("v2_38" "v2_28") ("v2_28") ("dox"))))))

(define-public crate-javascriptcore-rs-sys-1.1.0 (c (n "javascriptcore-rs-sys") (v "1.1.0") (d (list (d (n "glib") (r "^0.18") (d #t) (k 0) (p "glib-sys")) (d (n "gobject") (r "^0.18") (d #t) (k 0) (p "gobject-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "162jwhcn422swzq4mw1vaw22p14jglygiyigxhynbx71sirv3qpk") (f (quote (("v2_38" "v2_28") ("v2_28") ("dox"))))))

(define-public crate-javascriptcore-rs-sys-1.1.1 (c (n "javascriptcore-rs-sys") (v "1.1.1") (d (list (d (n "glib") (r "^0.18") (d #t) (k 0) (p "glib-sys")) (d (n "gobject") (r "^0.18") (d #t) (k 0) (p "gobject-sys")) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "shell-words") (r "^1.0.0") (d #t) (k 2)) (d (n "system-deps") (r "^6") (d #t) (k 1)) (d (n "tempfile") (r "^3") (d #t) (k 2)))) (h "092igagxm561lx65sin2z18jpxzyg0288cfzcrdvg97z2j6yf6xg") (f (quote (("v2_38" "v2_28") ("v2_28"))))))

