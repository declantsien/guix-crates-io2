(define-module (crates-io ja va javascriptcore6) #:use-module (crates-io))

(define-public crate-javascriptcore6-0.1.0 (c (n "javascriptcore6") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.1") (d #t) (k 0) (p "javascriptcore6-sys")) (d (n "glib") (r "^0.17") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "once_cell") (r "^1.8") (d #t) (k 0)))) (h "16vs3ppfmsv0dw5yhhwgbj86q0dngndxy3xd30jh9a2jam70ajm8") (f (quote (("dox" "ffi/dox" "glib/dox"))))))

(define-public crate-javascriptcore6-0.2.0 (c (n "javascriptcore6") (v "0.2.0") (d (list (d (n "ffi") (r "^0.2") (d #t) (k 0) (p "javascriptcore6-sys")) (d (n "glib") (r "^0.18") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0ybys4zgf8i9y9xx8ngpy6sfcn2hzspg0qxzwwiwy4iipakfgcls")))

(define-public crate-javascriptcore6-0.3.0 (c (n "javascriptcore6") (v "0.3.0") (d (list (d (n "ffi") (r "^0.3") (d #t) (k 0) (p "javascriptcore6-sys")) (d (n "glib") (r "^0.19") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y11a9y1bh3h637kd1i0p25frh801i9gmipjcqhgbfs01hk6jnnr")))

