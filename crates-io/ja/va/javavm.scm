(define-module (crates-io ja va javavm) #:use-module (crates-io))

(define-public crate-javavm-0.1.0 (c (n "javavm") (v "0.1.0") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "084q2nvjwcscyq9dvxyxqc02bldlwrdfyqy280r5rcssxyx77ri0")))

(define-public crate-javavm-0.1.1 (c (n "javavm") (v "0.1.1") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "1lfnfba81aqawgl5ydv4yj6dxcrxvjlsygzb7h69nxdkrmc1mzvx")))

(define-public crate-javavm-0.1.2 (c (n "javavm") (v "0.1.2") (d (list (d (n "jni") (r "^0.19.0") (d #t) (k 0)))) (h "0wf55mrnafxqw9qvgazcfyn0yy9m4k8kdxfia0kmqivhsdpbx237")))

