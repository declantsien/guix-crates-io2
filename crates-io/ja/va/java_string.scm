(define-module (crates-io ja va java_string) #:use-module (crates-io))

(define-public crate-java_string-0.0.1 (c (n "java_string") (v "0.0.1") (h "1izn93x25xf22p8h66r87ncjzpsmh4zpq84vc21ax5vc9xnrsk2z")))

(define-public crate-java_string-0.1.0 (c (n "java_string") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)))) (h "1afvf2l752d2fj7i8lyqiazjypns92wxbr71z4f3srvh5rhpybjk") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-java_string-0.1.1 (c (n "java_string") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)))) (h "17kyqkl9mn47a5m2kwq60j1pzrpi2j9b73j5hi8yp2xddj33cqk7") (s 2) (e (quote (("serde" "dep:serde"))))))

(define-public crate-java_string-0.1.2 (c (n "java_string") (v "0.1.2") (d (list (d (n "serde") (r "^1.0.160") (o #t) (d #t) (k 0)))) (h "0w8vil2zvgx9vhkwqgwxwc3bh6kjizgg8wfqnz1gfd9xvpfn210d") (s 2) (e (quote (("serde" "dep:serde"))))))

