(define-module (crates-io ja va java-properties) #:use-module (crates-io))

(define-public crate-java-properties-0.1.0 (c (n "java-properties") (v "0.1.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "0hs52jdgpl3199wg4ndqnqkmrfbydv9ym8b6m761d9av2vla87mz")))

(define-public crate-java-properties-0.1.1 (c (n "java-properties") (v "0.1.1") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "1nkmvq2jcy0jvclzxk5i84lrn52rzn9ffmyzhi7kqip4kb8cfzml")))

(define-public crate-java-properties-0.2.0 (c (n "java-properties") (v "0.2.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "0hn15wbpb191zk070jhm72ib7pvgh2x0aprv1qbi5a8j8m0ximpc")))

(define-public crate-java-properties-1.0.0 (c (n "java-properties") (v "1.0.0") (d (list (d (n "encoding") (r "^0.2.32") (d #t) (k 0)) (d (n "regex") (r "^0.1.47") (d #t) (k 0)))) (h "1aym4l4j7048win20hb7rkb0arib42qzbgv0kmwjwpci8rhahhyd")))

(define-public crate-java-properties-1.1.0 (c (n "java-properties") (v "1.1.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1014i0hs6afvijc8zmxf6w0hmhhjqfny55m5al3qrsgydhwchc03")))

(define-public crate-java-properties-1.1.1 (c (n "java-properties") (v "1.1.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "regex") (r "^0.2.1") (d #t) (k 0)))) (h "1gd8z6p6wp9xxpjqjsijr6h1x87w96p6yynnxgadggs59jrnljym")))

(define-public crate-java-properties-1.2.0 (c (n "java-properties") (v "1.2.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "regex") (r "^1.1.0") (d #t) (k 0)))) (h "1yl9nfjyf507cgr5kwhq4w8fr5x3gr9zpwm7hfi25pjvvs543x6a")))

(define-public crate-java-properties-1.3.0 (c (n "java-properties") (v "1.3.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)))) (h "1dy1j1am0gg1vnmz47z4xhxqlwq4ki6mm3flh10grrj245lw6yfi")))

(define-public crate-java-properties-1.4.0 (c (n "java-properties") (v "1.4.0") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.1") (d #t) (k 0)))) (h "01wwq475wxgg90afap19wjaif4wc13wx7bc4cjwwwi6ixlfv3s90")))

(define-public crate-java-properties-1.4.1 (c (n "java-properties") (v "1.4.1") (d (list (d (n "encoding") (r "^0.2.33") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0qv2jwji778pmr2bc2jvl6z93gsh3d0skm829l1m3vx1aj34v471")))

(define-public crate-java-properties-2.0.0 (c (n "java-properties") (v "2.0.0") (d (list (d (n "encoding_rs") (r "^0.8.32") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0zqi8l4q8w307mn4pv65a12jg9rzdgkdkaqynpr53i3i8i46zgrp")))

