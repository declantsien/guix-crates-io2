(define-module (crates-io ja va javascriptcore-sys) #:use-module (crates-io))

(define-public crate-javascriptcore-sys-0.0.1 (c (n "javascriptcore-sys") (v "0.0.1") (h "0837506ng94rmdw52g27iqp6nm5q8ndlma2288n2fizbzarjxgmn")))

(define-public crate-javascriptcore-sys-0.0.2 (c (n "javascriptcore-sys") (v "0.0.2") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "19ziav5hh32hvdlbwkbrz9q31vqsdxghf537605pfzr3s569hjlq")))

(define-public crate-javascriptcore-sys-0.0.3 (c (n "javascriptcore-sys") (v "0.0.3") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "1c81m7dgzhnmxwd4xld07ig5ckqybm7zqrair5azkp4cy2ly9inq")))

(define-public crate-javascriptcore-sys-0.0.4 (c (n "javascriptcore-sys") (v "0.0.4") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "1ahw4r6sp452rgrv9lnsp3pa6hc1g31lc9szm6svinf9lnykkmnf")))

(define-public crate-javascriptcore-sys-0.0.5 (c (n "javascriptcore-sys") (v "0.0.5") (d (list (d (n "pkg-config") (r "^0.3.9") (d #t) (t "cfg(target_os = \"linux\")") (k 1)))) (h "1px02pg5jdbxh1xcdazy4r17vhw8sg8lsizdldjp4dm6s940pm9d")))

