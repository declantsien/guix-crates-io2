(define-module (crates-io ja va javabc) #:use-module (crates-io))

(define-public crate-javabc-0.1.0 (c (n "javabc") (v "0.1.0") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "09v80pa0cb043dmw372zxjvk82crhvgmrq3s0b0a11p4slkaqg0z")))

(define-public crate-javabc-0.1.2 (c (n "javabc") (v "0.1.2") (d (list (d (n "bitflags") (r "^0.7") (d #t) (k 0)) (d (n "byteorder") (r "^0.5") (d #t) (k 0)) (d (n "error-chain") (r "^0.7") (d #t) (k 0)))) (h "0vkqig6shm45rn0zwv75mbj2sp937hfq60kp2k4i66rbk9imsna1")))

