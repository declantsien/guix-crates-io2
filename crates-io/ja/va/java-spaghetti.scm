(define-module (crates-io ja va java-spaghetti) #:use-module (crates-io))

(define-public crate-java-spaghetti-0.1.0 (c (n "java-spaghetti") (v "0.1.0") (d (list (d (n "jni-sys") (r "^0.4.0") (d #t) (k 0)))) (h "07iwh40c56gbykgsdzb58mrl23mm48zf6wscbvka9awllr36896y")))

(define-public crate-java-spaghetti-0.1.1 (c (n "java-spaghetti") (v "0.1.1") (d (list (d (n "jni-sys") (r "^0.4.0") (d #t) (k 0)))) (h "1xcn3pzhgb46v2s9qzqssimjfrd674wixn928dy4g9g424iw4n0r")))

(define-public crate-java-spaghetti-0.2.0 (c (n "java-spaghetti") (v "0.2.0") (d (list (d (n "jni-sys") (r "^0.4.0") (d #t) (k 0)))) (h "10hv4n7phiwn2n2k650zgacgwyy7a4nvcg8032jp6727j6k81cdn")))

