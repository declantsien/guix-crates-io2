(define-module (crates-io ja va javascriptcore-rs-webkitgtk4-1) #:use-module (crates-io))

(define-public crate-javascriptcore-rs-webkitgtk4-1-0.4.999 (c (n "javascriptcore-rs-webkitgtk4-1") (v "0.4.999") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.4") (d #t) (k 0) (p "javascriptcore-rs-sys-webkit2gtk4-1")) (d (n "glib") (r "^0.16.0") (d #t) (k 0)))) (h "0q50nrzq8zbbfiws7g4bwzif9v8136rlgzvgkzzm037mc8yhyvf6") (f (quote (("v2_28" "ffi/v2_28") ("dox" "ffi/dox")))) (y #t)))

(define-public crate-javascriptcore-rs-webkitgtk4-1-0.4.1000 (c (n "javascriptcore-rs-webkitgtk4-1") (v "0.4.1000") (d (list (d (n "bitflags") (r "^1.0") (d #t) (k 0)) (d (n "ffi") (r "^0.4") (d #t) (k 0) (p "javascriptcore-rs-sys-webkit2gtk4-1")) (d (n "glib") (r "^0.16.0") (d #t) (k 0)))) (h "0g8580z88sdgrwzvcnz6c97g3zzq4l1kag1cjprhkmdyn0am73ah") (f (quote (("v2_28" "ffi/v2_28") ("dox" "ffi/dox")))) (y #t)))

