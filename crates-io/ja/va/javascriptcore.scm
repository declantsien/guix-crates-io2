(define-module (crates-io ja va javascriptcore) #:use-module (crates-io))

(define-public crate-javascriptcore-0.0.1 (c (n "javascriptcore") (v "0.0.1") (d (list (d (n "javascriptcore-sys") (r "^0.0.1") (d #t) (k 0)))) (h "1i7j2wznpxxvxmn8hi8c1zsrxvfhpw7asa6b07jihyjbqnq9dfpx")))

(define-public crate-javascriptcore-0.0.2 (c (n "javascriptcore") (v "0.0.2") (d (list (d (n "javascriptcore-sys") (r "^0.0.2") (d #t) (k 0)))) (h "195cg6rcp6lj9f2l5jal4ym4kjdsgy8slxpl6zvh0kbnd0h0pfdq")))

(define-public crate-javascriptcore-0.0.3 (c (n "javascriptcore") (v "0.0.3") (d (list (d (n "javascriptcore-sys") (r "^0.0.3") (d #t) (k 0)))) (h "0lfl1s2p8dlvvvdplwfdlpndd42x45rxhq1vvhgnrlwjsj56wa1v")))

(define-public crate-javascriptcore-0.0.4 (c (n "javascriptcore") (v "0.0.4") (d (list (d (n "javascriptcore-sys") (r "^0.0.4") (d #t) (k 0)))) (h "0p97359lg87dbrnbcq66s6jpk0f0m4fd4wawmqvp10j89y0lykjc")))

(define-public crate-javascriptcore-0.0.5 (c (n "javascriptcore") (v "0.0.5") (d (list (d (n "javascriptcore-sys") (r "^0.0.5") (d #t) (k 0)))) (h "1fvf4xj7jx71rs1fgjd49053k549gqbq0c1qza8xi781f883kbrh")))

