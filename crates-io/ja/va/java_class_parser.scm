(define-module (crates-io ja va java_class_parser) #:use-module (crates-io))

(define-public crate-java_class_parser-0.0.0 (c (n "java_class_parser") (v "0.0.0") (d (list (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 1)))) (h "1m5n98g8j28np8pb4ks3c8zpwza2v0vwrfrd9arrvkpv1wdbiwm9")))

(define-public crate-java_class_parser-0.0.2 (c (n "java_class_parser") (v "0.0.2") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "java-locator") (r "^0.1.2") (d #t) (k 0)) (d (n "nom") (r "^7.1.1") (d #t) (k 0)) (d (n "petgraph") (r "^0.6.2") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "zip") (r "^0.6.3") (d #t) (k 0)))) (h "1crc5h5820zg2y89aiyazpqcb03cqf3x9nw6c4l2qcpq9fv2kjz9") (r "1.65")))

