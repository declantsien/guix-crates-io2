(define-module (crates-io ja va java-rand) #:use-module (crates-io))

(define-public crate-java-rand-0.1.0 (c (n "java-rand") (v "0.1.0") (h "0wxb8dww2dm9815hv120g2kbp16hl3qarc4qhq3w84c7vy1qk68d")))

(define-public crate-java-rand-0.2.0 (c (n "java-rand") (v "0.2.0") (h "106x1azij4nhw5wjywz7w2y63gbs3psbinhphv08n1ywhcdva1r0")))

