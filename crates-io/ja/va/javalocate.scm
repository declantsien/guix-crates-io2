(define-module (crates-io ja va javalocate) #:use-module (crates-io))

(define-public crate-javalocate-0.1.0 (c (n "javalocate") (v "0.1.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^1.4.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)))) (h "0qi90vq0i5l78gg3p0as0wqhxmpjznslyx7ry1gg7j1pfgz91pmn") (y #t)))

(define-public crate-javalocate-0.2.0 (c (n "javalocate") (v "0.2.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^1.4.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)))) (h "0s03fq9lg376wvl27z8kc358bjasqkzmv5pzjp8k2dizv5vnjwd5") (y #t)))

(define-public crate-javalocate-0.3.0 (c (n "javalocate") (v "0.3.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^1.4.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)))) (h "19nalr11p79hf0yk6f309lz073wgxqsprl1kg98lrv952gp8r9cv") (y #t)))

(define-public crate-javalocate-0.3.1 (c (n "javalocate") (v "0.3.1") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^1.4.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)))) (h "11mnpwia43d7kvb542gfyyan6bs67ga0ls50drhd0f3a6mg3a1ld") (y #t)))

(define-public crate-javalocate-0.3.2 (c (n "javalocate") (v "0.3.2") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^1.4.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)))) (h "0mysc7r80qgv2ydxbbgb4pvjq33dyv9qcqw170p6rg0km4xbglmz") (y #t)))

(define-public crate-javalocate-0.4.0 (c (n "javalocate") (v "0.4.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^1.4.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)))) (h "1w4szjnk5060asc8d7qnaya66aw42wwd8n32m1ca8cg6fxbcl8s5")))

(define-public crate-javalocate-0.5.0 (c (n "javalocate") (v "0.5.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^1.4.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "09cmsbl1syswijlfdvl2hw7rcacnz5417nvyjw67yazg5b2knncz")))

(define-public crate-javalocate-0.5.1 (c (n "javalocate") (v "0.5.1") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^1.4.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "1zyl5ssifwrzapxbna3w1vg6prkmhnvhl5v7y71a8ac6yl4w4pxh")))

(define-public crate-javalocate-0.6.0 (c (n "javalocate") (v "0.6.0") (d (list (d (n "clap") (r "^3.1.5") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^1.4.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winreg") (r "^0.10") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "06wggdk241c0w8y91daw6ckdwzf4a7zbmw70pq3shxjnb3a77n41")))

(define-public crate-javalocate-0.7.0 (c (n "javalocate") (v "0.7.0") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^2.0.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0c8ixmw2qx025h4wh1yqfl65ni49c221130lspr6slkf54b2v3z2")))

(define-public crate-javalocate-0.7.1 (c (n "javalocate") (v "0.7.1") (d (list (d (n "clap") (r "^4.4.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "confy") (r "^0.4.0") (d #t) (k 0)) (d (n "exitcode") (r "^1.1.2") (d #t) (k 0)) (d (n "java-properties") (r "^2.0.0") (d #t) (k 0)) (d (n "plist") (r "^1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "winreg") (r "^0.52.0") (d #t) (t "cfg(target_os = \"windows\")") (k 0)))) (h "0sxycxc695li9r0x3450vq9ysszfvdkqw9qhl350f64isqzz48hd")))

