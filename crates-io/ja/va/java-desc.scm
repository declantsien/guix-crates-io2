(define-module (crates-io ja va java-desc) #:use-module (crates-io))

(define-public crate-java-desc-0.1.0 (c (n "java-desc") (v "0.1.0") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "0lqxyan2d7rk66cwy0g7z2570yvy4m8md8nssn3qhdwl23f7piid")))

(define-public crate-java-desc-0.1.1 (c (n "java-desc") (v "0.1.1") (d (list (d (n "nom") (r "^5") (d #t) (k 0)))) (h "1r3f7vmzsqszcl6b0i014zwh5cwlvqwjsg8z5wbd8596gz3i5f6k")))

