(define-module (crates-io ja va javawithrust) #:use-module (crates-io))

(define-public crate-javawithrust-0.1.0 (c (n "javawithrust") (v "0.1.0") (d (list (d (n "j4rs") (r "^0.13") (d #t) (k 0)) (d (n "j4rs_derive") (r "^0.1") (d #t) (k 0)) (d (n "javawithrust_macro") (r "^0.1") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0j787q3vkqkl3an8ffgw5a4a22qdbamsj5dsxka887r0yhlawvp4")))

(define-public crate-javawithrust-0.2.0 (c (n "javawithrust") (v "0.2.0") (d (list (d (n "j4rs") (r "^0.13") (d #t) (k 0)) (d (n "j4rs_derive") (r "^0.1") (d #t) (k 0)) (d (n "javawithrust_macro") (r "^0.2") (d #t) (k 0)) (d (n "paste") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sawjyacx8dxq354gwvjfvfy89id61j4isw0bqyvwas3law36bmp")))

