(define-module (crates-io ja va java_asm) #:use-module (crates-io))

(define-public crate-java_asm-0.0.1 (c (n "java_asm") (v "0.0.1") (d (list (d (n "java_asm_internal") (r "^0.0.1") (d #t) (k 0)))) (h "05d4g5nk0qiql6xydy5vaxh9yfjjbdx1q7x20hlmqdbgsk2cd4g2")))

(define-public crate-java_asm-0.0.2 (c (n "java_asm") (v "0.0.2") (d (list (d (n "java_asm_internal") (r "^0.0.2") (d #t) (k 0)))) (h "08pwm97m6ihrri4dlb44nw2zgq3bypaxlijs1zja22m0lr82abqh")))

(define-public crate-java_asm-0.0.3 (c (n "java_asm") (v "0.0.3") (d (list (d (n "java_asm_internal") (r "^0.0.3") (d #t) (k 0)))) (h "0naaz5afhjjiz320nfjyiy4gaqmqb8601i8ljiqwadg2s3kk248h")))

(define-public crate-java_asm-0.0.4 (c (n "java_asm") (v "0.0.4") (d (list (d (n "java_asm_internal") (r "^0.0.4") (d #t) (k 0)))) (h "0f8r53y319h8gnxcgz1q27kch2bhjq70ikhr2v5r1x0irb1d3wmy")))

