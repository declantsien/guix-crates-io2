(define-module (crates-io ja va java_runtime) #:use-module (crates-io))

(define-public crate-java_runtime-0.0.1 (c (n "java_runtime") (v "0.0.1") (d (list (d (n "java-locator") (r "~0.1") (d #t) (k 0)) (d (n "sha1") (r "~0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)) (d (n "zip") (r "~0.6") (d #t) (k 0)))) (h "0crl113d7pkjj6xddl93wmi95hzb27qxhszxk2wbdyq99lwn26qk")))

(define-public crate-java_runtime-0.0.2 (c (n "java_runtime") (v "0.0.2") (d (list (d (n "java-locator") (r "~0.1") (d #t) (k 0)) (d (n "sha1") (r "~0.10") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "ureq") (r "^2") (d #t) (k 0)) (d (n "zip") (r "~0.6") (d #t) (k 0)))) (h "17cycmpdlf5sc1fsvc6s5p157gzancplq39v1sys2q9n0cavxfn9")))

