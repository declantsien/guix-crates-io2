(define-module (crates-io ja y- jay-algorithms) #:use-module (crates-io))

(define-public crate-jay-algorithms-0.1.0 (c (n "jay-algorithms") (v "0.1.0") (d (list (d (n "smallvec") (r "^1.8.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "1m01sy4jwkzfb8w043yqqqva19v6bydl3ay989kw0lni4yzvv2jh")))

(define-public crate-jay-algorithms-0.2.0 (c (n "jay-algorithms") (v "0.2.0") (d (list (d (n "smallvec") (r "^1.8.0") (f (quote ("const_generics" "const_new" "union"))) (d #t) (k 0)))) (h "0jnxa9g6c3gxapkgxyl2q5pfl5d7pivpd9cw9r4cibnsqy6zckhy")))

