(define-module (crates-io ja vy javy-apis) #:use-module (crates-io))

(define-public crate-javy-apis-1.0.0 (c (n "javy-apis") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "javy") (r "^1.0.0") (d #t) (k 0)))) (h "0c3945n9nm3zksvx3i5akjfk7lw3y6prac6qlz0xyx3y9xgmw8vd") (f (quote (("text_encoding") ("stream_io") ("console"))))))

(define-public crate-javy-apis-2.0.0 (c (n "javy-apis") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "javy") (r "^2.0.0") (d #t) (k 0)))) (h "1ylqr017s8v3z7as4f189ciign70a9frbx3zvswrwj7qdx2w2yrd") (f (quote (("text_encoding") ("stream_io") ("console")))) (s 2) (e (quote (("random" "dep:fastrand"))))))

(define-public crate-javy-apis-2.1.0 (c (n "javy-apis") (v "2.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fastrand") (r "^1.9.0") (o #t) (d #t) (k 0)) (d (n "javy") (r "^2.1.0") (d #t) (k 0)))) (h "0f4826yk584l8r3a621d6mpw1vnmra4kzcy1yv3lnlm0rb143xlm") (f (quote (("text_encoding") ("stream_io") ("console")))) (s 2) (e (quote (("random" "dep:fastrand"))))))

(define-public crate-javy-apis-2.2.0 (c (n "javy-apis") (v "2.2.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "fastrand") (r "^2.0.1") (o #t) (d #t) (k 0)) (d (n "javy") (r "^2.2.0") (d #t) (k 0)))) (h "0cglv6wgv7y9cx06rp3hgzm60pn9sz86xjkrjbc5mfh81vzj3gqy") (f (quote (("text_encoding") ("stream_io") ("console")))) (s 2) (e (quote (("random" "dep:fastrand"))))))

(define-public crate-javy-apis-3.0.0 (c (n "javy-apis") (v "3.0.0") (h "0g0mf0h24hxyqf0w22i6bmb9h677d1nsshgi6g3vgfw41ydl2198")))

