(define-module (crates-io ja q- jaq-syn) #:use-module (crates-io))

(define-public crate-jaq-syn-1.0.0-beta (c (n "jaq-syn") (v "1.0.0-beta") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "152h25yf9pr2zlc8knmv7fz53scl9ly8bndh78icwh5h8dg03r1s") (f (quote (("default" "serde"))))))

(define-public crate-jaq-syn-1.0.0 (c (n "jaq-syn") (v "1.0.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1yg9cr8ka5sgrm0lcnk2xcs6lqjh2hyywzk8yy1hkl41ywh0psxs") (f (quote (("default" "serde"))))))

(define-public crate-jaq-syn-1.1.0 (c (n "jaq-syn") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1mlqhgc8pdlk41y695v4cmim7lp7ss7887bkhb4j06vrzc0h3mm4") (f (quote (("default" "serde"))))))

