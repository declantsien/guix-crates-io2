(define-module (crates-io ja q- jaq-parse) #:use-module (crates-io))

(define-public crate-jaq-parse-0.5.0 (c (n "jaq-parse") (v "0.5.0") (d (list (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "08b4xpd5gxb0qyqp6i1ymvj4j5xhxm79cifh5f5xbkrfgcnn5rwg") (f (quote (("default" "serde"))))))

(define-public crate-jaq-parse-0.6.0 (c (n "jaq-parse") (v "0.6.0") (d (list (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0x0rgbrs6cm82yv1gywvyv1gwqkj76dgdrax1zrqsd1wj1sswk9g") (f (quote (("default" "serde"))))))

(define-public crate-jaq-parse-0.7.0 (c (n "jaq-parse") (v "0.7.0") (d (list (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "14w7xr4a78rg5jadmrzid2xvypr480g57l1sxv839w28zrb6xc8q") (f (quote (("default" "serde"))))))

(define-public crate-jaq-parse-0.8.0 (c (n "jaq-parse") (v "0.8.0") (d (list (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0z8vgm667j110larywpskxn6fsy4aksc8h0vi0qmmm12n68ac5a9") (f (quote (("default" "serde"))))))

(define-public crate-jaq-parse-0.8.1 (c (n "jaq-parse") (v "0.8.1") (d (list (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0xs9k8n6k74sfxa2msdnpm124cryng37s10vjdwp50xh2mb2mfpp") (f (quote (("default" "serde"))))))

(define-public crate-jaq-parse-0.9.0 (c (n "jaq-parse") (v "0.9.0") (d (list (d (n "chumsky") (r "^0.8.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1zajj3j97ahbyms926xwv02yylmyikjbgdpfdxplg9b4knjk088a") (f (quote (("default" "serde"))))))

(define-public crate-jaq-parse-0.10.0 (c (n "jaq-parse") (v "0.10.0") (d (list (d (n "chumsky") (r "^0.9.0") (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "1c1651lgrn8dk343s2drlaqccgv9zq6kp16crgrpms5r3vq9f3rg") (f (quote (("default" "serde"))))))

(define-public crate-jaq-parse-1.0.0-alpha (c (n "jaq-parse") (v "1.0.0-alpha") (d (list (d (n "chumsky") (r "^0.9.0") (k 0)) (d (n "serde") (r "^1.0.137") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "078ria4mzdh7bf5h8jwikrad77appskywlad7w6l5zcb7xjpdanq") (f (quote (("default" "serde"))))))

(define-public crate-jaq-parse-1.0.0-beta (c (n "jaq-parse") (v "1.0.0-beta") (d (list (d (n "chumsky") (r "^0.9.0") (k 0)) (d (n "jaq-syn") (r "^1.0.0-beta") (d #t) (k 0)))) (h "02hyk25ykrfz4bxv0wh1y8bk2hsx3vnd6c8yya8pxsxvgqxw2p9j")))

(define-public crate-jaq-parse-1.0.0 (c (n "jaq-parse") (v "1.0.0") (d (list (d (n "chumsky") (r "^0.9.0") (k 0)) (d (n "jaq-syn") (r "^1.0.0") (d #t) (k 0)))) (h "03h941iybs9sxs45hwvbwh5nlw14ns7schsw37n0scxrca8yk68j")))

(define-public crate-jaq-parse-1.0.1 (c (n "jaq-parse") (v "1.0.1") (d (list (d (n "chumsky") (r "^0.9.0") (k 0)) (d (n "jaq-syn") (r "^1.0.0") (d #t) (k 0)))) (h "1hv1qfc6pcn6s1clbx1wjqcphisf7y7kqfl2b4wl22xway9m4nvg") (r "1.64")))

(define-public crate-jaq-parse-1.0.2 (c (n "jaq-parse") (v "1.0.2") (d (list (d (n "chumsky") (r "^0.9.0") (k 0)) (d (n "jaq-syn") (r "^1.0.0") (d #t) (k 0)))) (h "056f38rg6glg7hra59b39ksniag8k50y4x7735j588lrkzmqnvzg") (r "1.64")))

