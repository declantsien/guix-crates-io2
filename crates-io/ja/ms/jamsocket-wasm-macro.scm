(define-module (crates-io ja ms jamsocket-wasm-macro) #:use-module (crates-io))

(define-public crate-jamsocket-wasm-macro-0.1.0 (c (n "jamsocket-wasm-macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "1kdk052zg0qd5qw73b0zlzac3656mnw6kcnsr2kdg9ss96qbrsgh")))

(define-public crate-jamsocket-wasm-macro-0.1.5 (c (n "jamsocket-wasm-macro") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^1.0.27") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.73") (f (quote ("full"))) (d #t) (k 0)))) (h "01pkpc5cmbibrlaz790k0ca0kk6k9fjp7m18kjwsz4jfbhmmg56q")))

(define-public crate-jamsocket-wasm-macro-0.1.7 (c (n "jamsocket-wasm-macro") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^1.0.36") (d #t) (k 0)) (d (n "quote") (r "^1.0.14") (d #t) (k 0)) (d (n "syn") (r "^1.0.85") (f (quote ("full"))) (d #t) (k 0)))) (h "0qv79k6wdnsjn4aljkgspzdjc7jk95kdv06h22q0y915j6dl1qk3")))

(define-public crate-jamsocket-wasm-macro-0.2.0 (c (n "jamsocket-wasm-macro") (v "0.2.0") (h "0s6n8qxmvh7a1pxx9q01nwsc7wzr9b4p6qhzx0szb1lfakrqinry")))

