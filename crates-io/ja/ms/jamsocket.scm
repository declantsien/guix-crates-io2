(define-module (crates-io ja ms jamsocket) #:use-module (crates-io))

(define-public crate-jamsocket-0.1.0 (c (n "jamsocket") (v "0.1.0") (h "0m6zbhcjn1zmpxxya8h6k32xvjl96f0fgjb1cy2b4x4a6jfsrva9")))

(define-public crate-jamsocket-0.1.1 (c (n "jamsocket") (v "0.1.1") (h "0zcfjaq68n9p10phhkc92wdj5qzjz4gyrhng0g12z89sg729qc0q")))

(define-public crate-jamsocket-0.1.3 (c (n "jamsocket") (v "0.1.3") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wyi6yzbillk7h9ynmsd1v3v7075238vbr2ahb0gan1dnsvp4cbx") (f (quote (("default"))))))

(define-public crate-jamsocket-0.1.5 (c (n "jamsocket") (v "0.1.5") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0fn7g5c9x9760n44g7f8q4pzm3bzbyig6z0qwqrdy1xzkcm2y6y6") (f (quote (("default"))))))

(define-public crate-jamsocket-0.1.6 (c (n "jamsocket") (v "0.1.6") (d (list (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "008i5jd71zggbmmrlvd7d2lk02qqs62nkl878i7p7h5gn7vib3nj") (f (quote (("default"))))))

(define-public crate-jamsocket-0.1.7 (c (n "jamsocket") (v "0.1.7") (d (list (d (n "serde") (r "^1.0.133") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "13ggkfwgv54vqsbapg7h5w9vczwza8g94zjzmi1ah20g89711fqi") (f (quote (("default"))))))

(define-public crate-jamsocket-0.2.0 (c (n "jamsocket") (v "0.2.0") (h "1nqhk6n4dwla3x42g52cny798927j531kdyl3spq0masvaiqsml8")))

