(define-module (crates-io ja ms jamsocket-stdio) #:use-module (crates-io))

(define-public crate-jamsocket-stdio-0.1.3 (c (n "jamsocket-stdio") (v "0.1.3") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "jamsocket") (r "^0.1.3") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.68") (d #t) (k 0)))) (h "107acn7dax96hmwl3ks71pz1cyn524vpn9icp3yfcahj7i71vgac")))

(define-public crate-jamsocket-stdio-0.1.5 (c (n "jamsocket-stdio") (v "0.1.5") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "jamsocket") (r "^0.1.5") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.70") (d #t) (k 0)))) (h "1z47jdifg0agjm4x9d8sfq1xcvmbkbjp115b4k8kyh521ksfzfch")))

(define-public crate-jamsocket-stdio-0.1.7 (c (n "jamsocket-stdio") (v "0.1.7") (d (list (d (n "interactive_process") (r "^0.1.2") (d #t) (k 0)) (d (n "jamsocket") (r "^0.1.7") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.74") (d #t) (k 0)))) (h "19nsxczl1p3gqnsx9cygbhcyhf2276dbi1bw1a0lw5rnhr2cjjf0")))

(define-public crate-jamsocket-stdio-0.2.0 (c (n "jamsocket-stdio") (v "0.2.0") (h "1ph0avffkjvg4s1swf9c1y1cspzkf966n6nhpvg2pdfc1z6i64b5")))

