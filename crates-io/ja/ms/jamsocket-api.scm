(define-module (crates-io ja ms jamsocket-api) #:use-module (crates-io))

(define-public crate-jamsocket-api-0.1.0 (c (n "jamsocket-api") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0909frix4jzw1bjmnwzbxdjkkkff28fvjmjd7cmjz3i7cyapz31r")))

(define-public crate-jamsocket-api-0.1.1 (c (n "jamsocket-api") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.42") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0yhfy9pjiw5qr0ydy788lxvrnhxk8s62v8qx9jhm498lbj990iwx") (f (quote (("default" "client") ("client" "reqwest" "anyhow"))))))

(define-public crate-jamsocket-api-0.1.2 (c (n "jamsocket-api") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.42") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lqmsxl7hr0bsxrv76m3gsl5vrjz2hvgssbrjwrvr5wnn8dkbj9b") (f (quote (("default" "client") ("client" "reqwest" "anyhow"))))))

(define-public crate-jamsocket-api-0.1.3 (c (n "jamsocket-api") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.42") (o #t) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("blocking" "json"))) (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0cjfksca2hy2kri2vxk8hbs1xi14g9vjyyg4pfa33wvnlbsijkyl") (f (quote (("default" "client") ("client" "reqwest" "anyhow"))))))

