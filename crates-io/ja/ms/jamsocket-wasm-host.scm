(define-module (crates-io ja ms jamsocket-wasm-host) #:use-module (crates-io))

(define-public crate-jamsocket-wasm-host-0.1.0 (c (n "jamsocket-wasm-host") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "jamsocket") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasmtime") (r "^0.28.0") (f (quote ("async" "wat" "jitdump" "parallel-compilation"))) (k 0)) (d (n "wasmtime-wasi") (r "^0.28.0") (d #t) (k 0)))) (h "1brbfgayxyqyi8sh1l296qjs9xrqscq6rlgvwjscxp2vc3dflim7")))

(define-public crate-jamsocket-wasm-host-0.1.1 (c (n "jamsocket-wasm-host") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "jamsocket") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "wasmtime") (r "^0.30.0") (f (quote ("async" "wat" "jitdump" "parallel-compilation" "cranelift"))) (k 0)) (d (n "wasmtime-wasi") (r "^0.30.0") (d #t) (k 0)))) (h "1fa13pd3m6684niv1l5qshnxx3qal7nnb2b2phiixy2whjzgzwcz")))

(define-public crate-jamsocket-wasm-host-0.1.3 (c (n "jamsocket-wasm-host") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "jamsocket") (r "^0.1.0") (d #t) (k 0)) (d (n "tracing") (r "^0.1.28") (d #t) (k 0)) (d (n "wasmtime") (r "^0.30.0") (f (quote ("async" "wat" "jitdump" "parallel-compilation" "cranelift"))) (k 0)) (d (n "wasmtime-wasi") (r "^0.30.0") (d #t) (k 0)))) (h "0w1jjnalklli1bcxxvysy7nl5zggmk1zcfp0chngv3y8s39amgfl")))

(define-public crate-jamsocket-wasm-host-0.1.5 (c (n "jamsocket-wasm-host") (v "0.1.5") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "jamsocket") (r "^0.1.5") (d #t) (k 0)) (d (n "tracing") (r "^0.1.28") (d #t) (k 0)) (d (n "wasmtime") (r "^0.31.0") (f (quote ("async" "wat" "jitdump" "parallel-compilation" "cranelift"))) (k 0)) (d (n "wasmtime-wasi") (r "^0.31.0") (d #t) (k 0)))) (h "0pqi03293bbjdwd2qbyrz3rsi2p0zdpwrhj4wyzq7r75vj1aac3k")))

(define-public crate-jamsocket-wasm-host-0.1.7 (c (n "jamsocket-wasm-host") (v "0.1.7") (d (list (d (n "anyhow") (r "^1.0.45") (d #t) (k 0)) (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)) (d (n "jamsocket") (r "^0.1.7") (d #t) (k 0)) (d (n "tracing") (r "^0.1.28") (d #t) (k 0)) (d (n "wasmtime") (r "^0.33.0") (f (quote ("async" "wat" "jitdump" "parallel-compilation" "cranelift"))) (k 0)) (d (n "wasmtime-wasi") (r "^0.33.0") (d #t) (k 0)))) (h "1zh1wnjsyrslxnp4sqfd7275aypxvg8qp8xhm995p75nlmjgnc5q")))

(define-public crate-jamsocket-wasm-host-0.2.0 (c (n "jamsocket-wasm-host") (v "0.2.0") (h "07slwjlpxjgpl1s8rpflsbs97mh26w3x75xxwpl1kav0rz18mx0m")))

