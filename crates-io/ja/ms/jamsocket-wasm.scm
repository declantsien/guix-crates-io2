(define-module (crates-io ja ms jamsocket-wasm) #:use-module (crates-io))

(define-public crate-jamsocket-wasm-0.1.0 (c (n "jamsocket-wasm") (v "0.1.0") (d (list (d (n "jamsocket") (r "^0.1.0") (d #t) (k 0)) (d (n "jamsocket-wasm-macro") (r "^0.1.0") (d #t) (k 0)))) (h "0x4fy7nrlinmsp0ll5r330g0crd9igpy55qbm4vmqcv3yrk8k1rl")))

(define-public crate-jamsocket-wasm-0.1.3 (c (n "jamsocket-wasm") (v "0.1.3") (d (list (d (n "jamsocket") (r "^0.1.3") (d #t) (k 0)) (d (n "jamsocket-wasm-macro") (r "^0.1.0") (d #t) (k 0)))) (h "10nh98ixxvp8iqiv18kd8vbsbngcvvl31mgn7qk7dcpk3f5vjn5g")))

(define-public crate-jamsocket-wasm-0.1.5 (c (n "jamsocket-wasm") (v "0.1.5") (d (list (d (n "jamsocket") (r "^0.1.5") (d #t) (k 0)) (d (n "jamsocket-wasm-macro") (r "^0.1.5") (d #t) (k 0)))) (h "14jna0i2p55sgw7mhc2b2lyy0jwda73akqqf830ajdxdjghpxxls")))

(define-public crate-jamsocket-wasm-0.1.7 (c (n "jamsocket-wasm") (v "0.1.7") (d (list (d (n "jamsocket") (r "^0.1.7") (d #t) (k 0)) (d (n "jamsocket-wasm-macro") (r "^0.1.7") (d #t) (k 0)))) (h "0ll1bzcwv7a8xrh3k0jr5xss96wp1k552xbw3z7nqs8czj429bsq")))

(define-public crate-jamsocket-wasm-0.2.0 (c (n "jamsocket-wasm") (v "0.2.0") (h "1g5n77yly1cbh9bzzjp57j1598gpzrbzszjvx2z47i7r2d3x284l")))

