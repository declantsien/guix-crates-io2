(define-module (crates-io ja ni janitor-bot) #:use-module (crates-io))

(define-public crate-janitor-bot-0.1.0 (c (n "janitor-bot") (v "0.1.0") (d (list (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "teloxide") (r "^0.3") (d #t) (k 0)) (d (n "teloxide-macros") (r "^0.3") (d #t) (k 0)) (d (n "tokio") (r "^0.2.11") (f (quote ("rt-threaded" "macros"))) (d #t) (k 0)))) (h "0v8zmvhx1dxf8pnhf28vnw6v411j7r9kpxxy30xvdwx2nxbck3d5")))

