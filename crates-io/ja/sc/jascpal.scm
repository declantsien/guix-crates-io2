(define-module (crates-io ja sc jascpal) #:use-module (crates-io))

(define-public crate-jascpal-0.1.0 (c (n "jascpal") (v "0.1.0") (d (list (d (n "nom") (r "^5.0.0") (f (quote ("std"))) (k 0)) (d (n "rgb") (r "^0.8.13") (d #t) (k 0)))) (h "1r913k3ya2rdi58s7i6796mmkpiyy3rg3nary1a4qikfclcirrr1")))

(define-public crate-jascpal-0.1.1 (c (n "jascpal") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.27") (d #t) (k 2)) (d (n "nom") (r "^5.0.0") (f (quote ("std"))) (k 0)) (d (n "rgb") (r "^0.8.13") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.13") (d #t) (k 0)))) (h "0zsyhnjig0mwdccsv7la53sh5spzj1xpvvpksc5qbslfmqd54h49")))

