(define-module (crates-io ja #{3-}# ja3-livecapture) #:use-module (crates-io))

(define-public crate-ja3-livecapture-0.5.0 (c (n "ja3-livecapture") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "failure") (r "^0.1.7") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.8") (d #t) (k 0)) (d (n "md5") (r "^0.7.0") (d #t) (k 0)) (d (n "nix") (r "^0.17.0") (d #t) (k 2)) (d (n "pcap") (r "^0.7.0") (d #t) (k 0)) (d (n "pcap-parser") (r "^0.9.2") (d #t) (k 0)) (d (n "pnet") (r "^0.28.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^0.4.0") (d #t) (k 2)) (d (n "rusty-fork") (r "^0.2.2") (d #t) (k 2)) (d (n "tls-parser") (r "^0.9.2") (d #t) (k 0)))) (h "18k8i8bpsi1znhzw7p0fmxrkaagswxvsqhiizs8sr7rcm5bf691w")))

