(define-module (crates-io ja #{3-}# ja3-rustls) #:use-module (crates-io))

(define-public crate-ja3-rustls-0.0.0 (c (n "ja3-rustls") (v "0.0.0") (d (list (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)))) (h "1jry61z145zkbfx8z0sjhh3692il4pd2l1sjwp81vi2xn77ajckb") (f (quote (("md5-string" "md-5" "hex") ("md5" "md-5"))))))

(define-public crate-ja3-rustls-0.0.1 (c (n "ja3-rustls") (v "0.0.1") (d (list (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "itertools") (r "^0.10.4") (d #t) (k 0)) (d (n "md-5") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)))) (h "09i8rw70hs8yr4ijlwnmafiiy5wfsy6cazyasy2k94m5p91jv7mn") (f (quote (("md5-string" "md-5" "hex") ("md5" "md-5"))))))

(define-public crate-ja3-rustls-0.0.2 (c (n "ja3-rustls") (v "0.0.2") (d (list (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)))) (h "0jhl3410awzlvznh9lbjpi0wmd04amfii0bwplkwda8frbs21ric") (f (quote (("md5-string" "md-5" "hex") ("md5" "md-5"))))))

(define-public crate-ja3-rustls-0.0.3 (c (n "ja3-rustls") (v "0.0.3") (d (list (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)))) (h "0qnam0lmig6z8ch517ia5jga43jp17i5m943k1vl7dr3rjvl9jwz") (f (quote (("md5-string" "md-5" "hex") ("md5" "md-5")))) (s 2) (e (quote (("rand" "dep:rand"))))))

(define-public crate-ja3-rustls-0.0.5 (c (n "ja3-rustls") (v "0.0.5") (d (list (d (n "hex") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)) (d (n "md-5") (r "^0.10") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "rustls") (r "^0.20") (d #t) (k 0)))) (h "0cblcrkrfgbqvnjw4plpslnshsgyp27ay2qg0apxyrglg612n8i4") (f (quote (("md5-string" "md-5" "hex") ("md5" "md-5")))) (s 2) (e (quote (("rand" "dep:rand"))))))

