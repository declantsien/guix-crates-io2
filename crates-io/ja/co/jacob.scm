(define-module (crates-io ja co jacob) #:use-module (crates-io))

(define-public crate-jacob-0.1.0 (c (n "jacob") (v "0.1.0") (d (list (d (n "bitreader") (r "^0.3.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1kj4cp1hm85lliiqz5myw5r0qfva0lf7x3bk9cm7gq5zas5fai71")))

(define-public crate-jacob-0.1.1 (c (n "jacob") (v "0.1.1") (d (list (d (n "bitreader") (r "^0.3.4") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "0gxpgfxdnl1cz66y4z18fc4wlaq2ysyiryzzh7a61715vl65qw54")))

(define-public crate-jacob-0.2.0 (c (n "jacob") (v "0.2.0") (d (list (d (n "bitreader") (r "^0.3.4") (d #t) (k 0)) (d (n "bitstream-io") (r "^1.2.0") (d #t) (k 0)) (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "13q07yi0b00iwkz5sb3pqx0ry0a84xp3d0kndkxkq3kk302h649b")))

