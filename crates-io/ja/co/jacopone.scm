(define-module (crates-io ja co jacopone) #:use-module (crates-io))

(define-public crate-jacopone-0.1.0 (c (n "jacopone") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "1swk7v23w91g46zx5d4njhlpiwk7r670j3c1b0hi0gna3aiwxisq")))

(define-public crate-jacopone-0.1.1 (c (n "jacopone") (v "0.1.1") (d (list (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "027f5jls5z3mr09gpd7bcjq3s0ivhhb6y1al40prmlhm3mcm4nnq")))

(define-public crate-jacopone-0.2.0 (c (n "jacopone") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.4.1") (d #t) (k 0)) (d (n "crunchy") (r "^0.2.1") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)))) (h "0033kvny629grjbs45ww9wvn4f913kih1yfhlhxdwz3ab8pbifa9")))

