(define-module (crates-io ja wo jawohl) #:use-module (crates-io))

(define-public crate-jawohl-0.1.0-dev (c (n "jawohl") (v "0.1.0-dev") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0hpp8cbxlr8vvjd3vxbcbg6134ipb54bkggycjqmysszmfijqb3l")))

(define-public crate-jawohl-0.1.0 (c (n "jawohl") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.96") (d #t) (k 2)))) (h "0z7ci49qflki333sgas71igfslqpai61qcd5y7y8yydm023ym8hd")))

