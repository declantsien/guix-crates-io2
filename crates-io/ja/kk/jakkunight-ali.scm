(define-module (crates-io ja kk jakkunight-ali) #:use-module (crates-io))

(define-public crate-jakkunight-ali-0.0.1 (c (n "jakkunight-ali") (v "0.0.1") (h "0gc5n56zlx9r3ymmhv82hnnsfzg5v1hg0r1k1sxwyvgpv8bhw5mi")))

(define-public crate-jakkunight-ali-0.0.2 (c (n "jakkunight-ali") (v "0.0.2") (h "0vddkh6snhwj8hz2p2hx3ka18hnqd8ar06lc95209f68a7x58wnq")))

(define-public crate-jakkunight-ali-0.0.3 (c (n "jakkunight-ali") (v "0.0.3") (h "1vyc4mbbjkvimngw9wpnmnhny298iq70aizq2mfb3d3w70wbf7sl")))

(define-public crate-jakkunight-ali-0.0.4 (c (n "jakkunight-ali") (v "0.0.4") (h "18rgaydjslj4nm6rw76d99g3l0qbcbl6jdzacqqg2yl15nqd87c6")))

