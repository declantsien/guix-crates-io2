(define-module (crates-io ja me jamendo) #:use-module (crates-io))

(define-public crate-jamendo-0.1.0 (c (n "jamendo") (v "0.1.0") (d (list (d (n "hyper") (r "^0.9") (f (quote ("serde-serialization"))) (d #t) (k 0)) (d (n "serde") (r "^0.7") (d #t) (k 0)) (d (n "serde_json") (r "^0.7") (d #t) (k 0)) (d (n "serde_macros") (r "^0.7") (d #t) (k 0)) (d (n "url") (r "^1.0") (d #t) (k 0)))) (h "1s8a4r0hl2da9dfiv6yizjdmvcmami9dd2z08gc4jwd1viwg8qa8")))

