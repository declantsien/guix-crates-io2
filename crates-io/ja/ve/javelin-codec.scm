(define-module (crates-io ja ve javelin-codec) #:use-module (crates-io))

(define-public crate-javelin-codec-0.3.3 (c (n "javelin-codec") (v "0.3.3") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "17cm059q4pm8g85smxw40q2i6scdvryscwri5ljg8i7d2k3lf67j")))

(define-public crate-javelin-codec-0.3.4 (c (n "javelin-codec") (v "0.3.4") (d (list (d (n "byteorder") (r "^1.2") (d #t) (k 0)) (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7") (d #t) (k 0)))) (h "1zgv7kz162gagvrv63y9fm3bw9j43ccl26524avqd4gk4kbs581f")))

