(define-module (crates-io ja ry jarylo) #:use-module (crates-io))

(define-public crate-jarylo-0.1.0 (c (n "jarylo") (v "0.1.0") (d (list (d (n "glium") (r "^0.32.1") (d #t) (k 0)))) (h "0rhy9lbbhs9qa23qxfa9311np0q7zf8l96yqq3k9z15jan1180j0") (y #t) (r "1.68")))

(define-public crate-jarylo-0.1.1 (c (n "jarylo") (v "0.1.1") (d (list (d (n "glium") (r "^0.32.1") (d #t) (k 0)))) (h "0iy6rzh4zyd5y903xcrsndxzk4fkbxsvy277y094f2j5k9gn6rhk") (y #t) (r "1.68")))

(define-public crate-jarylo-0.1.2 (c (n "jarylo") (v "0.1.2") (d (list (d (n "glium") (r "^0.32.1") (d #t) (k 0)))) (h "1za9789flvhxbysclyk1yjc74r8k584gwvh9inlqi2wak75jsqs4") (y #t) (r "1.68")))

(define-public crate-jarylo-0.1.3 (c (n "jarylo") (v "0.1.3") (d (list (d (n "glium") (r "^0.32.1") (d #t) (k 0)))) (h "1rh6dx3h47n4bx9224grvvvxg1wcdv8ln34hx9nrb11s1ql154r1") (y #t) (r "1.68")))

