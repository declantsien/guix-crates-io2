(define-module (crates-io ja rv jarvis) #:use-module (crates-io))

(define-public crate-jarvis-0.1.0 (c (n "jarvis") (v "0.1.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1n8fnzn0k79byjy421gh6lxpnkfhiv5a6bd3pvp1vhmz1fkw5qwd")))

(define-public crate-jarvis-0.2.0 (c (n "jarvis") (v "0.2.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "1szin1rk0g8cdirzaarl9fxhms5c3nqq2nvrs0waabx1x0rgi1zm")))

(define-public crate-jarvis-0.3.0 (c (n "jarvis") (v "0.3.0") (d (list (d (n "config") (r "^0.9") (d #t) (k 0)) (d (n "walkdir") (r "^2") (d #t) (k 0)))) (h "19yg9wls0dmpyz8c1ab6s7qksyikwbvhqkpfzi4gb799f08lk9bi")))

