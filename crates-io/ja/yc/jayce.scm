(define-module (crates-io ja yc jayce) #:use-module (crates-io))

(define-public crate-jayce-0.1.0 (c (n "jayce") (v "0.1.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "13dab6ky0cbhi7lp25giybxd8ab9n6flwjfzf4cf04rhfk39ah6p")))

(define-public crate-jayce-0.1.1 (c (n "jayce") (v "0.1.1") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "1dwa5blg240vbndvvnx06v67fmp59sm0l4gyf4xlrcxhnkmq30f0")))

(define-public crate-jayce-0.1.2 (c (n "jayce") (v "0.1.2") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "09nd34npwgv8bkdd081hzqfw6i1b8i9ni789x5zyvd5brm5n642w")))

(define-public crate-jayce-0.1.3 (c (n "jayce") (v "0.1.3") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0lym56n1hdvwxw0ab5i7nrg8hp1gd309s6gq5k5a59vdijn5ck4h")))

(define-public crate-jayce-0.2.0 (c (n "jayce") (v "0.2.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "030fdm7y7py66pk8mfr4f6ypcijh844c0vijy6v5r57bcp323rjm")))

(define-public crate-jayce-0.2.1 (c (n "jayce") (v "0.2.1") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0bl3ghg86xivg6gl96syw5bfc3v5d48cvqbsz6iwzf5875vc27l1")))

(define-public crate-jayce-0.2.2 (c (n "jayce") (v "0.2.2") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "16rgmiknvz74cmrjbcbxkhmlnb9s3d2lk6iqzzq5al07yfnfmg53")))

(define-public crate-jayce-0.3.0 (c (n "jayce") (v "0.3.0") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0crv5s1zdsgsfj09ja6ys1kmqazgachy2h5balnl4fb6vskg074k")))

(define-public crate-jayce-0.3.1 (c (n "jayce") (v "0.3.1") (d (list (d (n "regex") (r "^1.5.5") (d #t) (k 0)))) (h "0immhxdyvch4pvzmiyq2zsxaramrzn23m2wv1zda6z5sfgp33ssk")))

(define-public crate-jayce-1.0.0 (c (n "jayce") (v "1.0.0") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1dmi6q7jzb01z8sxmbcnbb5yhmxg9w3f3mxy2fwqddfdy411rnzw")))

(define-public crate-jayce-2.0.0 (c (n "jayce") (v "2.0.0") (d (list (d (n "regex") (r "^1.7.0") (d #t) (k 0)))) (h "1fywl4hhx8x0f37mc112y2qrnadmh8hpq3f5yvh2d79fh2s0m297")))

(define-public crate-jayce-3.0.0 (c (n "jayce") (v "3.0.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1ibnf00ixq5w4187nfvidmrln6hcghdmbyv1zir9qx19xqk5plgm")))

(define-public crate-jayce-3.1.0 (c (n "jayce") (v "3.1.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "19jrmka65qb719ki9zgzgbgq6y9dl6mn29b9g464nbikmybgrrr5")))

(define-public crate-jayce-4.0.0 (c (n "jayce") (v "4.0.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1mvs3v4sql1mrhjny0bd9ifn99syp02pjk3mzyjrx5ppxl6r0dl9")))

(define-public crate-jayce-4.0.1 (c (n "jayce") (v "4.0.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1v5y0z5ri2dz34yr64zvg073l00brlb1i23k6s491rl2cg1xigqv")))

(define-public crate-jayce-6.0.0 (c (n "jayce") (v "6.0.0") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0jdaa9pjqpsb2c53mbmnkrydansn3ksd2w8rjp232j007yfb8glw")))

(define-public crate-jayce-6.0.1 (c (n "jayce") (v "6.0.1") (d (list (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1wv7shnp5w2i57zfp38v31s1cihkj41yra143q8spw91d1p9mrsv")))

(define-public crate-jayce-6.0.2 (c (n "jayce") (v "6.0.2") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "056f3y8xs2pqiqsqdnpw5wqx7bhi5rhdc9cqvs1kqq3nqjvn1cqw") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

(define-public crate-jayce-6.0.4 (c (n "jayce") (v "6.0.4") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0pb4n65wm11yf22fz9ysz08v7b75651p3k81fdzia3p7l4c3xif0") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

(define-public crate-jayce-6.0.5 (c (n "jayce") (v "6.0.5") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1bab023ia2v0pzxx42sw9qm3djsl08r7404m6prv9vxq76xj9nh4") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

(define-public crate-jayce-7.0.0 (c (n "jayce") (v "7.0.0") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "1q2mm6ni5szarrrdyl1w3y5rvan3bizmx3mzspzs23q8ragsbv8z") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

(define-public crate-jayce-7.0.1 (c (n "jayce") (v "7.0.1") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0n31zdvb1jgl80c3hz4bpc1g31yyl5vz94cyhh6qr10nr0xrcfd2") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

(define-public crate-jayce-8.0.0 (c (n "jayce") (v "8.0.0") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "06p9mx64w2crjdvwm6q27lbdbzdqwbkap2y2c5m98m01xx12h9wj") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

(define-public crate-jayce-8.1.0 (c (n "jayce") (v "8.1.0") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0yl3wrrffsb51qlmv1rn13gxr5bsc1px8klhx2ak6v4a2j5mx7jj") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

(define-public crate-jayce-9.0.0 (c (n "jayce") (v "9.0.0") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0i0p4ik4izirkpd65mw2cl5bymfndb3vfqp5ywgh3gig7j0k8wq2") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

(define-public crate-jayce-9.0.1 (c (n "jayce") (v "9.0.1") (d (list (d (n "bytecount") (r "^0.6.3") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.8.4") (d #t) (k 0)))) (h "0z15jqgn782xqx3mrfjm03k8jmw4xbl6blc7g8yqk77p5fzr24js") (f (quote (("runtime-dispatch-simd" "bytecount/runtime-dispatch-simd") ("generic-simd" "bytecount/generic-simd"))))))

(define-public crate-jayce-9.1.0 (c (n "jayce") (v "9.1.0") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "16xwb1ijl3xqk1ggzi01lq5c1bgzkfhj7dqn4zlzyh6vlj6i42ds") (f (quote (("serialization" "serde") ("generic-simd" "bytecount/generic-simd") ("default" "bytecount/runtime-dispatch-simd"))))))

(define-public crate-jayce-9.1.1 (c (n "jayce") (v "9.1.1") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "1lx8idaia5pwfx29h62gmw9d6r36bnb2layxhq9a0f5gh0plxssk") (f (quote (("serialization" "serde") ("generic-simd" "bytecount/generic-simd") ("default" "bytecount/runtime-dispatch-simd"))))))

(define-public crate-jayce-9.1.2 (c (n "jayce") (v "9.1.2") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "03q8hp4rr28bhy4ic5609m4c9i6sdk5dckvmw197zfzb4lsx4bd1") (f (quote (("serialization" "serde") ("generic-simd" "bytecount/generic-simd") ("default" "bytecount/runtime-dispatch-simd"))))))

(define-public crate-jayce-10.0.0 (c (n "jayce") (v "10.0.0") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "1qvx8rvjb8420za20pl6lli7xaj4q63r60sikawpy2gjjsls039v") (f (quote (("serialization" "serde") ("generic-simd" "bytecount/generic-simd") ("default" "bytecount/runtime-dispatch-simd"))))))

(define-public crate-jayce-10.0.1 (c (n "jayce") (v "10.0.1") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 2)))) (h "0dab75a3vjlp0xbvimdl4dx1yps85b28gsnbwxypznvwl93l8ghc") (f (quote (("serialization" "serde") ("generic-simd" "bytecount/generic-simd") ("default" "bytecount/runtime-dispatch-simd"))))))

(define-public crate-jayce-11.0.0 (c (n "jayce") (v "11.0.0") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "0nkngz9z1lz27f38mzghf0vq8zh2bzb1ig5cqdkz2kw0pym515q7") (f (quote (("generic-simd" "bytecount/generic-simd") ("default" "bytecount/runtime-dispatch-simd"))))))

(define-public crate-jayce-12.0.0 (c (n "jayce") (v "12.0.0") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "1kr1cdn7rq1hm8bm8ylyaxja7dv39bdbm4vxcpslw8d06xs9jbgw") (f (quote (("generic-simd" "bytecount/generic-simd") ("default" "bytecount/runtime-dispatch-simd"))))))

(define-public crate-jayce-12.1.0 (c (n "jayce") (v "12.1.0") (d (list (d (n "bytecount") (r "^0.6.7") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "regex") (r "^1.10.3") (d #t) (k 0)))) (h "18mp0z5my921a28ijl60yab9npy9sm0d1jldkm68nq68av8ch51f") (f (quote (("generic-simd" "bytecount/generic-simd") ("default" "bytecount/runtime-dispatch-simd"))))))

