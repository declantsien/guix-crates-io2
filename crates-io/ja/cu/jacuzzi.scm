(define-module (crates-io ja cu jacuzzi) #:use-module (crates-io))

(define-public crate-jacuzzi-0.1.1 (c (n "jacuzzi") (v "0.1.1") (d (list (d (n "actix-files") (r "^0.3.0") (d #t) (k 0)) (d (n "actix-web") (r "^3.0.2") (f (quote ("openssl"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "openssl") (r "^0.10.30") (d #t) (k 0)))) (h "1krjwn6zlfyy5swbn84dc723ykwacmynk7fspsa3l2xir7m4pd01")))

(define-public crate-jacuzzi-0.2.1 (c (n "jacuzzi") (v "0.2.1") (d (list (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.3") (f (quote ("tls"))) (d #t) (k 0)))) (h "07r40r80dcji4skv0jv1xg7pv0yn1ml22li2wz09bwm7zf5g5iw8")))

