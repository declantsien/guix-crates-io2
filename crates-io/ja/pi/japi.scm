(define-module (crates-io ja pi japi) #:use-module (crates-io))

(define-public crate-japi-0.1.1 (c (n "japi") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1p9lj45qrm4dikd8xh5aqlhnngkqaq5jgvclkl04x93nv4zfa7sy")))

(define-public crate-japi-0.2.0 (c (n "japi") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1sh8240rwn2ar6yjjvifk7i8d8gfz1qbg7i75in6z4h174bcgj9w")))

(define-public crate-japi-0.3.0 (c (n "japi") (v "0.3.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "04ivhkagzih1009a7hiap6cay2vai6618ph26vbs7b89avgglbma")))

(define-public crate-japi-0.3.1 (c (n "japi") (v "0.3.1") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "129sgx73wpmnwajwf8zn4zx1rr1vc888lcy42bwx9q3qi7h7p7fk")))

(define-public crate-japi-0.3.2 (c (n "japi") (v "0.3.2") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1hnv5g8v0ngik7by5v9xg6n8lxcasnn51p3by3pr61f9hsh82ik7")))

