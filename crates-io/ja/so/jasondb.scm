(define-module (crates-io ja so jasondb) #:use-module (crates-io))

(define-public crate-jasondb-0.1.0 (c (n "jasondb") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "tar") (r "^0.4.33") (k 0)))) (h "1v8riklf07h624qpjw8r1c9icbss2x2psijvk4l15xv5v4a2c40i") (f (quote (("validation" "serde" "serde_json") ("default" "validation"))))))

(define-public crate-jasondb-0.1.1 (c (n "jasondb") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "08fc6832akmnp9kgh0lf95h9q79kayz4x1vm3yzp60zszlqsw45b") (f (quote (("validation" "serde" "serde_json") ("default" "validation"))))))

(define-public crate-jasondb-0.1.2 (c (n "jasondb") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (o #t) (d #t) (k 0)))) (h "11w709vy70vkh6yfwj7brzzjbk01c996dskbaw3984rx9ml1dibv") (f (quote (("validation" "serde" "serde_json") ("default" "validation"))))))

(define-public crate-jasondb-0.2.0 (c (n "jasondb") (v "0.2.0") (d (list (d (n "humphrey_json") (r "^0.1.1") (d #t) (k 0)))) (h "1sin7bh5m7lig164pfrrrch6bv3amqdjdf2gdc61v8fbjsydzh1i")))

(define-public crate-jasondb-0.2.1 (c (n "jasondb") (v "0.2.1") (d (list (d (n "humphrey_json") (r "^0.1.1") (d #t) (k 0)))) (h "00b1rkgkx0shhk5j05q9axxkskfcc1qmp95mjsdrc24r4snd3ffc")))

(define-public crate-jasondb-0.2.2 (c (n "jasondb") (v "0.2.2") (d (list (d (n "humphrey_json") (r "^0.1.1") (d #t) (k 0)))) (h "1lw6d66gsa4zal76vqmnaxsc83gwraqzav5yjrc1szxh9lfirp4d")))

(define-public crate-jasondb-0.2.3 (c (n "jasondb") (v "0.2.3") (d (list (d (n "humphrey_json") (r "^0.1.1") (d #t) (k 0)))) (h "1l18k1rfjnd1cprnml6svmlsyn6xs837xxmjpf50r2sj8asrpv4a")))

(define-public crate-jasondb-0.2.4 (c (n "jasondb") (v "0.2.4") (d (list (d (n "humphrey_json") (r "^0.1.1") (d #t) (k 0)))) (h "0dqnrxwm4k8yzhij76w3dzy59i9z4xkrakw13yh9qirb0vxcl9kr")))

(define-public crate-jasondb-0.2.5 (c (n "jasondb") (v "0.2.5") (d (list (d (n "humphrey_json") (r "^0.1.1") (d #t) (k 0)))) (h "0i8ayixnzy70z065pjs161ck9qmmx1gm6ivmsa2jgfbwh1gw6fih")))

(define-public crate-jasondb-0.2.6 (c (n "jasondb") (v "0.2.6") (d (list (d (n "humphrey_json") (r "^0.2.2") (d #t) (k 0)))) (h "13wsyyfcjjw6f3yalsb0pf7jyw43szclab68njcwl2bnwb9rm2l9")))

(define-public crate-jasondb-0.2.7 (c (n "jasondb") (v "0.2.7") (d (list (d (n "humphrey_json") (r "^0.2.2") (d #t) (k 0)))) (h "119ccjwp9bp30ha0d0i36il14jn8j53kdnvivn68p5013lqmi9h1")))

