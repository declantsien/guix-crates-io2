(define-module (crates-io ja so jason-minigrep) #:use-module (crates-io))

(define-public crate-jason-minigrep-0.1.0 (c (n "jason-minigrep") (v "0.1.0") (h "1phhm0anw01p13i31wkqvmfrk6jns2xj37n3zx9xvh77kal06qj9")))

(define-public crate-jason-minigrep-0.1.1 (c (n "jason-minigrep") (v "0.1.1") (h "09b63a6by0dx304nr6nrqwpayxkm3p4hsda57rrs0ha3cln3x2g5")))

