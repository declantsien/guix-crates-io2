(define-module (crates-io ja so jaso) #:use-module (crates-io))

(define-public crate-jaso-1.0.0 (c (n "jaso") (v "1.0.0") (d (list (d (n "async-recursion") (r "^1.0.0") (d #t) (k 0)) (d (n "clap") (r "^4.0.32") (f (quote ("derive"))) (d #t) (k 0)) (d (n "rlimit") (r "^0.9.1") (d #t) (k 0)) (d (n "tokio") (r "^1.23.0") (f (quote ("sync" "rt" "rt-multi-thread" "parking_lot" "macros" "time" "fs"))) (d #t) (k 0)) (d (n "unicode-normalization") (r "^0.1.22") (d #t) (k 0)))) (h "1qal6wpqhqdics7qc3w0yrsjkm0mbxkmqmr2rswd4sf0fjqmmc6g")))

