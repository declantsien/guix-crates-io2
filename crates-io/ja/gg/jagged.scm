(define-module (crates-io ja gg jagged) #:use-module (crates-io))

(define-public crate-jagged-0.1.0 (c (n "jagged") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)))) (h "0czlnzss4c5gc2bwcynxifwwqa5672vwfxqzfcvfii87d4a5n74n")))

(define-public crate-jagged-0.2.0 (c (n "jagged") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.31") (d #t) (k 0)) (d (n "bytes") (r "^0.5") (d #t) (k 0)) (d (n "bzip2") (r "^0.3.3") (d #t) (k 0)))) (h "0z7dd7rq6y8g5fflfn6yh5ki2nfaycw269g09a036cpm7yjwnhnf")))

