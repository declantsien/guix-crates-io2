(define-module (crates-io ja gg jaggedarray) #:use-module (crates-io))

(define-public crate-jaggedarray-0.1.0 (c (n "jaggedarray") (v "0.1.0") (d (list (d (n "generic-array") (r "^1.0.0") (d #t) (k 0)) (d (n "typenum") (r "^1.17.0") (d #t) (k 0)))) (h "1z06alnq95f8y6n0y0d9vd16wxbvr49w3v9qyigx186wh62dx8gm")))

(define-public crate-jaggedarray-0.1.1 (c (n "jaggedarray") (v "0.1.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "generic-array") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)) (d (n "typenum") (r "^1.17.0") (d #t) (k 0)))) (h "0jck6cd1b2cdjk99knp4drcb34hav5089h1gd6xm2n0finq5bn08")))

(define-public crate-jaggedarray-0.2.0 (c (n "jaggedarray") (v "0.2.0") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "generic-array") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)) (d (n "typenum") (r "^1.17.0") (d #t) (k 0)))) (h "1jbf2xrzmsrdbzlbwvapdn61n19029y8m9a0q1sasrr3swi3ddbd")))

(define-public crate-jaggedarray-0.2.1 (c (n "jaggedarray") (v "0.2.1") (d (list (d (n "criterion") (r "^0.4") (f (quote ("html_reports"))) (d #t) (k 2)) (d (n "generic-array") (r "^1.0.0") (d #t) (k 0)) (d (n "num") (r "^0.4.3") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (d #t) (k 0)) (d (n "typenum") (r "^1.17.0") (d #t) (k 0)))) (h "1ss2jamn9ddhvm43wgb34lha24wabfr420ncjx0zpwawkrwwrmic")))

