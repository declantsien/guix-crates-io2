(define-module (crates-io ja gg jagged_array) #:use-module (crates-io))

(define-public crate-jagged_array-0.1.0 (c (n "jagged_array") (v "0.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "try_opt") (r "^0.1") (d #t) (k 0)))) (h "11j4sbj9hsqfksp24g0pnck4w9j59zli6qhl3mgwxhh4ymh9b029")))

(define-public crate-jagged_array-0.2.0 (c (n "jagged_array") (v "0.2.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "try_opt") (r "^0.1") (d #t) (k 0)))) (h "0p01xd6yp1w1ajxqikqmaf26id3sf1j2vriwlpr9basaxamnji5n")))

(define-public crate-jagged_array-0.2.1 (c (n "jagged_array") (v "0.2.1") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "try_opt") (r "^0.1") (d #t) (k 0)))) (h "1xijv818dhysqnrbwsn2ma51ppz6pkv4x1rd67bs0kw98j6i3pck")))

(define-public crate-jagged_array-0.2.2 (c (n "jagged_array") (v "0.2.2") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "try_opt") (r "^0.1") (d #t) (k 0)))) (h "1ksbkc6phinldxf13pd15y7qyaqc3dmabv24lvpvvnxna614rv8m")))

(define-public crate-jagged_array-0.2.3 (c (n "jagged_array") (v "0.2.3") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "try_opt") (r "^0.1") (d #t) (k 0)))) (h "05c6j6qpzbys75nskjcypw5flgawlzc7ni0v68xly1vva0pim5am")))

(define-public crate-jagged_array-0.2.4 (c (n "jagged_array") (v "0.2.4") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "streaming-iterator") (r "^0.1") (d #t) (k 0)) (d (n "try_opt") (r "^0.1") (d #t) (k 0)))) (h "1l7qcqjvfqm44iy9dim89idggxs4wqra0g82br6zyj5wg69688mc")))

