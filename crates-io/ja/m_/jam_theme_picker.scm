(define-module (crates-io ja m_ jam_theme_picker) #:use-module (crates-io))

(define-public crate-jam_theme_picker-0.1.0 (c (n "jam_theme_picker") (v "0.1.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "bevy") (r "^0.6") (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "randomorg") (r "^1.0.4") (d #t) (k 0)))) (h "1kwma93w3ys6ydpfm5gqy5fbf13217iji2qxm6py74ivpvpibgx1")))

(define-public crate-jam_theme_picker-0.2.0 (c (n "jam_theme_picker") (v "0.2.0") (d (list (d (n "bevy") (r "^0.6") (k 0)) (d (n "bevy") (r "^0.6") (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "randomorg") (r "^1.0.4") (d #t) (k 0)))) (h "1hfk6axxqj38fm3s55i1i9mjvahvs5xfi7p9f9aq0c1k4y28fpyv")))

