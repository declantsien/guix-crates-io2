(define-module (crates-io ja mk jamkit) #:use-module (crates-io))

(define-public crate-jamkit-0.1.0 (c (n "jamkit") (v "0.1.0") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (d #t) (k 0)))) (h "1zvhd34bvp7ln19gg6hn8m3y6hdh66rjwd7cg9y83yp9hpmp4dix")))

(define-public crate-jamkit-0.1.1 (c (n "jamkit") (v "0.1.1") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (d #t) (k 0)))) (h "0wbkwwakjarl0wdf4gqwvjd03kgz03mrnr0i9cplf3dxs3y6fzdn")))

(define-public crate-jamkit-0.1.2 (c (n "jamkit") (v "0.1.2") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (d #t) (k 0)))) (h "09z9pjm3zicfxlnh8awkyrvgpm4vg2lznjpkm62p7qmcbsn4sc96")))

(define-public crate-jamkit-0.1.3 (c (n "jamkit") (v "0.1.3") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (d #t) (k 0)))) (h "1s7wy8kah3bwmn5lyjbpa4fi8r2wa82122vj64338k9bfbrlam3z")))

(define-public crate-jamkit-0.2.0 (c (n "jamkit") (v "0.2.0") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (d #t) (k 0)))) (h "0rgpr9aslwz25dghlwp8kp3ri4gvai2jwl7m3swkqr5vc7gs3zhh")))

(define-public crate-jamkit-0.2.1 (c (n "jamkit") (v "0.2.1") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (d #t) (k 0)))) (h "09q96n7dmfa8q79f90vbjpikap1yhm004sm4125wy5yk14b2rnki")))

(define-public crate-jamkit-0.2.2 (c (n "jamkit") (v "0.2.2") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "12gsrzsyw0qw6w02clk8y5fqkq00jf5w7ra5f7dxxd92g5q3608h")))

(define-public crate-jamkit-0.2.3 (c (n "jamkit") (v "0.2.3") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "1mlzk4jg5hqazidw2j0byg5q5rfw551abqgb5f00sngilxi2lc1f")))

(define-public crate-jamkit-0.2.4 (c (n "jamkit") (v "0.2.4") (d (list (d (n "cgmath") (r "^0.2") (d #t) (k 0)) (d (n "glium") (r "^0.6") (d #t) (k 0)) (d (n "image") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1.30") (d #t) (k 0)))) (h "0pv8l7azq08fcqpgm6qqy9w4wbgm4mvbqv1pk1irg0ks80l1kvax")))

(define-public crate-jamkit-0.2.5 (c (n "jamkit") (v "0.2.5") (d (list (d (n "cgmath") (r "^0.4.0") (d #t) (k 0)) (d (n "glium") (r "^0.10.0") (d #t) (k 0)) (d (n "image") (r "^0.3.9") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "0i6dyvd0bn28pq6ahgrvgszff40afmx1rll4r0v1f2wpmi4d7xfa")))

(define-public crate-jamkit-0.2.6 (c (n "jamkit") (v "0.2.6") (d (list (d (n "cgmath") (r "^0.4.0") (d #t) (k 0)) (d (n "glium") (r "^0.13.2") (d #t) (k 0)) (d (n "image") (r "^0.3.9") (d #t) (k 0)) (d (n "time") (r "^0.1.33") (d #t) (k 0)))) (h "1skx9ycbxpy6rcd8avlg05lq7hmqqppznzzz3q7s9h9pmpcga8wz")))

(define-public crate-jamkit-0.3.0 (c (n "jamkit") (v "0.3.0") (d (list (d (n "cgmath") (r "^0.7") (d #t) (k 0)) (d (n "glium") (r "^0.13") (d #t) (k 0)) (d (n "image") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "00cbfymnihfym0pg0ivm4gk9d2nhv02m4822sdc7d0ai6rvvxmdw")))

(define-public crate-jamkit-0.3.1 (c (n "jamkit") (v "0.3.1") (d (list (d (n "cgmath") (r "^0.7") (d #t) (k 0)) (d (n "glium") (r "^0.13") (d #t) (k 0)) (d (n "image") (r "^0.6") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "00935hnvax2j68pd7i0g20ijcbifndq3677np17afj2d4qfjlw4k")))

