(define-module (crates-io ja rg jarg) #:use-module (crates-io))

(define-public crate-jarg-0.1.1 (c (n "jarg") (v "0.1.1") (d (list (d (n "aerr") (r "^0.1.8") (d #t) (k 0)) (d (n "sonic-rs") (r "^0.2.6") (d #t) (k 0)))) (h "01s39kgj3mqlax1dah8xyjf1g02bf8qkcwyysfch9lkdswpywfbs")))

(define-public crate-jarg-0.1.2 (c (n "jarg") (v "0.1.2") (d (list (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "sonic-rs") (r "^0.2.6") (d #t) (k 0)))) (h "0x57c47hry3jyc0sj91x4fgb8wmf156nv262dhcrn90diwmviqqf")))

(define-public crate-jarg-0.1.3 (c (n "jarg") (v "0.1.3") (d (list (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "sonic-rs") (r "^0.2.6") (d #t) (k 0)))) (h "1z0chvc98fa3rrjpyy5pxvf3bbqwk5s79bp62pmn2qjyibcj6czb")))

(define-public crate-jarg-0.1.4 (c (n "jarg") (v "0.1.4") (d (list (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "sonic-rs") (r "^0.2.6") (d #t) (k 0)))) (h "1qmgvjglspz6i0szhnfnp6rfmacmy2p8jp97vgwcz2vpvli5m3kc")))

(define-public crate-jarg-0.1.5 (c (n "jarg") (v "0.1.5") (d (list (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "sonic-rs") (r "^0.2.6") (d #t) (k 0)))) (h "1dippw5nvnw4vgg7frgk5855m2amichaxsm805hk2ry6c72b737c")))

(define-public crate-jarg-0.1.6 (c (n "jarg") (v "0.1.6") (d (list (d (n "axum") (r "^0.7.2") (d #t) (k 0)) (d (n "sonic-rs") (r "^0.2.6") (d #t) (k 0)))) (h "19i6dnavr6w6x2pc3iw0sgcbxqvzjqv7s4il7bll654vz85wxwlg")))

(define-public crate-jarg-0.1.7 (c (n "jarg") (v "0.1.7") (d (list (d (n "axum") (r "^0.7.3") (d #t) (k 0)) (d (n "sonic-rs") (r "^0.3.1") (d #t) (k 0)))) (h "1zz62w87q1mhbm4867cxng5g6bqkbdbmmnszmli9vpm0wnr6g74p")))

