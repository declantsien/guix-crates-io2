(define-module (crates-io ja rg jargon) #:use-module (crates-io))

(define-public crate-jargon-0.2.0 (c (n "jargon") (v "0.2.0") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)))) (h "046408k32i4qxx763yn8ynm1parg23mghdmq9lds0yq0z9rchkzc")))

(define-public crate-jargon-0.2.1 (c (n "jargon") (v "0.2.1") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)))) (h "0gsvlavzxqdlzisp5pn3gcla8y1kkyypihz61ya9xmzj1nfw9k7r")))

(define-public crate-jargon-0.2.3 (c (n "jargon") (v "0.2.3") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)))) (h "12p9x03p4m3h09fxwy4j52dlknpz5pa6grkfjdqy4gmpa6djx19s")))

(define-public crate-jargon-0.2.4 (c (n "jargon") (v "0.2.4") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)))) (h "0x32b0p63cz1z8c1bql005gycb8xrvf4bm2zg7biblcypr095qaj")))

(define-public crate-jargon-0.2.5 (c (n "jargon") (v "0.2.5") (d (list (d (n "colored") (r "^1.9") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 1)) (d (n "rand") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 1)))) (h "13abbhn527z3fc1vyq93agvnvxlrk5g87pz3bmpps8f9mmqkynx1")))

(define-public crate-jargon-0.3.0 (c (n "jargon") (v "0.3.0") (d (list (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0dvz6rxcdvq631kyfmmnjs0dj8910y1lhldy11dzpn7s2n0z6ysj")))

(define-public crate-jargon-0.3.1 (c (n "jargon") (v "0.3.1") (d (list (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0y907np7dqxfc4p5g5xrhj8n03ipq4yafv5b8hgi5qmbs663ghzi")))

(define-public crate-jargon-0.3.2 (c (n "jargon") (v "0.3.2") (d (list (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0c3qky2yrmviqrj46v7rdq227vxpzyhiy7l6nyj75g0lv1fc0v8l")))

(define-public crate-jargon-0.3.3 (c (n "jargon") (v "0.3.3") (d (list (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "00hbsa9d753ns7nhjj2v3fd94mjhxxg67c1i1jhng802lk5r4dl6")))

(define-public crate-jargon-0.3.4 (c (n "jargon") (v "0.3.4") (d (list (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "0b8zjlzckci3ya5aknha83mn6w1vyn8a43lnacc2kskvv1q8ax8j")))

(define-public crate-jargon-0.3.5 (c (n "jargon") (v "0.3.5") (d (list (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "flate2") (r "^1") (d #t) (k 1)) (d (n "rand") (r "^0") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 1)))) (h "1fq2blxmmlckx1sqqyqnlzlmnz0y2adjhamz85hgcx2ws1mm19pg")))

