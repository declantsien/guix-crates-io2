(define-module (crates-io ja wn jawn) #:use-module (crates-io))

(define-public crate-jawn-0.1.0 (c (n "jawn") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)))) (h "057xfimwly51p9kjcskwpsykwv94wa1sx272c2h24x9hwvyxd102")))

(define-public crate-jawn-0.1.1 (c (n "jawn") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "indicatif") (r "^0.13.0") (d #t) (k 0)))) (h "184b8qzi7q27wvqwl0395wiydqyl4zh7cgvfysnxdp6451msbyaj")))

