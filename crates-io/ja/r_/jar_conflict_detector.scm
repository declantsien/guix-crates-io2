(define-module (crates-io ja r_ jar_conflict_detector) #:use-module (crates-io))

(define-public crate-jar_conflict_detector-0.0.1 (c (n "jar_conflict_detector") (v "0.0.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.5") (d #t) (k 0)))) (h "0jhh8n7vd0ypd94ip6gj1rm1g7zqczj1bmz440c726kadj2khm2h")))

