(define-module (crates-io ja r_ jar_conflict_detecter) #:use-module (crates-io))

(define-public crate-jar_conflict_detecter-0.0.1 (c (n "jar_conflict_detecter") (v "0.0.1") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.5") (d #t) (k 0)))) (h "0npf4gvfwkqi3ac33prmbwv1n5ivsx3nqymsnxivw9l9ka3v7zzm") (y #t)))

(define-public crate-jar_conflict_detecter-0.0.2 (c (n "jar_conflict_detecter") (v "0.0.2") (d (list (d (n "clap") (r "^4.2.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "zip") (r "^0.6.5") (d #t) (k 0)))) (h "08gs4jc01996zz391nyjga39a9bkw4m13f8hnr20rxym9q8annfn") (y #t)))

