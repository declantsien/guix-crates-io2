(define-module (crates-io bx ca bxcan) #:use-module (crates-io))

(define-public crate-bxcan-0.1.0 (c (n "bxcan") (v "0.1.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.1.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "04q161v31ls933m3jvdv8vwc8ssb08rl6ppdsz9dg9myymqcqhnh")))

(define-public crate-bxcan-0.2.0 (c (n "bxcan") (v "0.2.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.1.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0nscwf3qaszsx31i5gsxsv26fj51jildz9wnyg6xx87s9c8zadd6") (y #t)))

(define-public crate-bxcan-0.2.1 (c (n "bxcan") (v "0.2.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.1.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1hg1grshnrawgppw03055qmkjsflw0gs4gidvr2yg5gzv69ss4jz")))

(define-public crate-bxcan-0.2.2 (c (n "bxcan") (v "0.2.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.1.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0f5pm4m1x9awnp9npzr3j8mxa9dgfarv5cha13wllbkmykj24f0x")))

(define-public crate-bxcan-0.2.3 (c (n "bxcan") (v "0.2.3") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.1.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0y4lxvg2srrw8cvkyar3r5pz89zri39krnfzmd4cadjkd6q7j264")))

(define-public crate-bxcan-0.3.0 (c (n "bxcan") (v "0.3.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.1.3") (d #t) (k 0)) (d (n "embedded-can") (r "^0.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "063fqlf3dhiq6wqhgq11lwknqlr0fcwgjkpr8v93vcw8kpa4b9pd")))

(define-public crate-bxcan-0.4.0 (c (n "bxcan") (v "0.4.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.1.3") (d #t) (k 0)) (d (n "embedded-can") (r "^0.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "15hcw8apn06hvlr0bmjg1wzp7dzdj7j3iq9p39p22721ry8r478c")))

(define-public crate-bxcan-0.5.0 (c (n "bxcan") (v "0.5.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-can") (r "^0.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "179q6w7d7ibglkdirmask8nl7x2g1l173qx7dhalylny23qry3g0")))

(define-public crate-bxcan-0.5.1 (c (n "bxcan") (v "0.5.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.2.0") (d #t) (k 0)) (d (n "embedded-can") (r "^0.3") (d #t) (k 0)) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1zqzaqximm7jb03zwrhdwiwx0v809521qym7qgiqfazcp1wcdsvn")))

(define-public crate-bxcan-0.6.0 (c (n "bxcan") (v "0.6.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r "^0.2.3") (o #t) (d #t) (k 0)) (d (n "embedded-can-03") (r "^0.3") (o #t) (d #t) (k 0) (p "embedded-can")) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "0r5v4q4f3sh6lnzvfd605yvqb5202p0hzppkhnx0aq438nsw67ca") (f (quote (("unstable-defmt" "defmt"))))))

(define-public crate-bxcan-0.6.1 (c (n "bxcan") (v "0.6.1") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r ">=0.2.3, <0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-can-03") (r "^0.3") (o #t) (d #t) (k 0) (p "embedded-can")) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1mykbvyzdgaivxr36pr15iyhl9pdxwc1967qyjghp9r14h1l0nzs") (f (quote (("unstable-defmt" "defmt"))))))

(define-public crate-bxcan-0.6.2 (c (n "bxcan") (v "0.6.2") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r ">=0.2.3, <0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-can-03") (r "^0.3") (o #t) (d #t) (k 0) (p "embedded-can")) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "1y9hbcrhrclyjhkhzycvlzs7kjwmv1mbisv3829bmclsxarb84sb") (f (quote (("unstable-defmt" "defmt"))))))

(define-public crate-bxcan-0.7.0 (c (n "bxcan") (v "0.7.0") (d (list (d (n "bitflags") (r "^1.2.1") (d #t) (k 0)) (d (n "defmt") (r ">=0.2.3, <0.4.0") (o #t) (d #t) (k 0)) (d (n "embedded-hal-02") (r "^0.2.7") (d #t) (k 0) (p "embedded-hal")) (d (n "nb") (r "^1.0.0") (d #t) (k 0)) (d (n "vcell") (r "^0.1.2") (d #t) (k 0)))) (h "106x85d7wd3dcxpnf5gldl9sf1i7yrrzh48jaashlbal1863vb20") (f (quote (("unstable-defmt" "defmt"))))))

