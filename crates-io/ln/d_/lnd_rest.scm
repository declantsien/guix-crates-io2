(define-module (crates-io ln d_ lnd_rest) #:use-module (crates-io))

(define-public crate-lnd_rest-0.1.0 (c (n "lnd_rest") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "0p3l6hann3pkh9576c0nvap3ffzyy63nl8akscyvlgrzchzbj8pm")))

(define-public crate-lnd_rest-0.1.1 (c (n "lnd_rest") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1z56xa8sq129a3f82j222g0f3kz2v3364ypdzr5w9wfyccwnz6fl")))

(define-public crate-lnd_rest-0.2.0 (c (n "lnd_rest") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1gprg6032yrf02gjl3gfvc02bcr7mr0sdxxgw6wh517lfrk88can")))

(define-public crate-lnd_rest-0.3.0 (c (n "lnd_rest") (v "0.3.0") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1ak48azhkdik3k9rgsg58dzig6sjs36a3w7k9hnq7shqbp81fkwr")))

(define-public crate-lnd_rest-0.3.1 (c (n "lnd_rest") (v "0.3.1") (d (list (d (n "async-trait") (r "^0.1.71") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.18") (f (quote ("json" "native-tls"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.166") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.100") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "08ivz4lk0rw20z8hhphlz4alm8ln9mljcj46jcr8dp91i989j0xl")))

