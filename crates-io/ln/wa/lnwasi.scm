(define-module (crates-io ln wa lnwasi) #:use-module (crates-io))

(define-public crate-lnwasi-0.0.1 (c (n "lnwasi") (v "0.0.1") (h "13a7nlk5nln5wq9pl1gm7c21gf0nbss7p3b8b5yxnjg89h3zzk0z")))

(define-public crate-lnwasi-0.0.2 (c (n "lnwasi") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.8") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "04b50ycxviqnmh09d80y92yznxfp2ak9i4c9v1l3vis5vzn3d6fl")))

(define-public crate-lnwasi-0.0.3 (c (n "lnwasi") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "bincode") (r "^1.3.3") (d #t) (k 0)) (d (n "ipnet") (r "^2.7.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "libc") (r "^0.2.139") (d #t) (k 0)) (d (n "nix") (r "^0.26.2") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_bytes") (r "^0.11.8") (d #t) (k 0)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)))) (h "1c2lyhhf18147xwapmjd0888c48mrqigyqg5inyc16pxr9i8x0dz")))

