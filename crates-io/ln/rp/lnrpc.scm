(define-module (crates-io ln rp lnrpc) #:use-module (crates-io))

(define-public crate-lnrpc-0.0.1 (c (n "lnrpc") (v "0.0.1") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "prost-build") (r "^0.6.1") (d #t) (k 1)))) (h "1v53kacj3pjsk3pvkx5wvvy2a3zgjamf97jpidvlaqfvn17mdizn")))

(define-public crate-lnrpc-0.1.0 (c (n "lnrpc") (v "0.1.0") (d (list (d (n "bytes") (r "^0.5.4") (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 0)) (d (n "prost") (r "^0.6.1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "macros"))) (d #t) (k 2)) (d (n "tokio-rustls") (r "^0.14.0") (f (quote ("dangerous_configuration"))) (d #t) (k 2)) (d (n "tonic") (r "^0.3.0") (f (quote ("transport" "tls"))) (d #t) (k 0)) (d (n "tonic-build") (r "^0.3.0") (d #t) (k 1)) (d (n "webpki") (r "^0.21.3") (d #t) (k 2)))) (h "0kc5alzq3yn8lhmgbprpgyfnf0ihx47x92rv1mgsd6cm8iy6d9s1")))

