(define-module (crates-io ln gc lngcnv) #:use-module (crates-io))

(define-public crate-lngcnv-1.0.0-alpha (c (n "lngcnv") (v "1.0.0-alpha") (h "1d557iyj23i4fijr776vzvp90kayyy1py24kvl3p4qcadlgvq5qc") (y #t)))

(define-public crate-lngcnv-1.0.0-alpha.1 (c (n "lngcnv") (v "1.0.0-alpha.1") (h "06ia2bbnq5a78sm99mampwm0p9064n92i48f0c38vjxdhpsfhbl7") (y #t)))

(define-public crate-lngcnv-1.0.0-beta (c (n "lngcnv") (v "1.0.0-beta") (h "144hpgg5k1yghnqlwzg8vsn8r82qgwd0fsnabmqwvc6dakprb7f7") (y #t)))

(define-public crate-lngcnv-1.0.0 (c (n "lngcnv") (v "1.0.0") (h "0hxaq9ji25ip05qcz6p7h6qnfwqj4qabg5101kq6c0p7l3al5r40")))

(define-public crate-lngcnv-1.1.0 (c (n "lngcnv") (v "1.1.0") (h "1mjh6bqf5liicg3m3gn1bvam8r44v63f4jbck7w2439w6d05x8a6")))

(define-public crate-lngcnv-1.2.0 (c (n "lngcnv") (v "1.2.0") (h "1m1d0g7y1igz9p43cqamq61sdxzss57pwhwk7by23zvq4zqiyvh2")))

(define-public crate-lngcnv-1.3.0 (c (n "lngcnv") (v "1.3.0") (h "09j30bkpryhnz48dk83nh1c53nf8gp52q3rixzwbr2v8ny3rw41z")))

(define-public crate-lngcnv-1.4.0-alpha (c (n "lngcnv") (v "1.4.0-alpha") (h "0w2c7fzh8iibgkcsn6h4q1dpg50b6nbci0qijvlh42q0k885p5bc") (y #t)))

(define-public crate-lngcnv-1.4.0-beta (c (n "lngcnv") (v "1.4.0-beta") (h "0d50n5sc2ln62yqlfh6lgr2fkhzwi5gdfm918vd5z3cjwvvndd6r") (y #t)))

(define-public crate-lngcnv-1.4.0 (c (n "lngcnv") (v "1.4.0") (h "09ysvk3ljwb2issxypk5ykkysqaym0bs8dmi75ajywz62sx4lp4n")))

(define-public crate-lngcnv-1.4.1 (c (n "lngcnv") (v "1.4.1") (h "0np26wirb7zbwch72wdgrqrgriqw5q72wwlb245qs11lqld9bwkw")))

(define-public crate-lngcnv-1.5.0 (c (n "lngcnv") (v "1.5.0") (h "1r0q3184vggsmw007zlq8ynkzxdkkzzs73hg6rc7kb5lfpa8d8kq")))

(define-public crate-lngcnv-1.5.1 (c (n "lngcnv") (v "1.5.1") (h "0snyssx4fzpgih0s84rsjkv29ai92b0v8xddjfd1y0di6843hvh0")))

(define-public crate-lngcnv-1.6.0-alpha.1 (c (n "lngcnv") (v "1.6.0-alpha.1") (h "1fnwgccm3chwlqg9g7l4av9j8s9k2a3vvsa1hb9kh2b9zmfgabk2") (y #t)))

(define-public crate-lngcnv-1.6.0-alpha.2 (c (n "lngcnv") (v "1.6.0-alpha.2") (h "153sw05wa2niiiys9l0a064vclpdz03rg78ky865gcn97sfc3fps") (y #t)))

(define-public crate-lngcnv-1.6.0-alpha.3 (c (n "lngcnv") (v "1.6.0-alpha.3") (h "1wiyqdxv9z2sx5ww2184psqv8547gqf8k0b93rcynjy3mzpvqkqx") (y #t)))

(define-public crate-lngcnv-1.6.0-alpha.4 (c (n "lngcnv") (v "1.6.0-alpha.4") (h "14s8nbx9ll82vc4pzqq23i7xira32l3jiihldg4zazqh5w08bzpy") (y #t)))

(define-public crate-lngcnv-1.6.0-alpha.5 (c (n "lngcnv") (v "1.6.0-alpha.5") (h "0mpkwpa007bgychlbrmcfbf7sgrrdpcglnpnnw5kphd29jmp8800") (y #t)))

(define-public crate-lngcnv-1.6.0-alpha.6 (c (n "lngcnv") (v "1.6.0-alpha.6") (h "01di366b72jny7x9rpdsra8rhx2kwjdg9h3lcj0bxpnq7jfyngca") (y #t)))

(define-public crate-lngcnv-1.6.0-alpha.7 (c (n "lngcnv") (v "1.6.0-alpha.7") (h "1dfl9i0yfg3yhr3scq7kniwg7nw52ryqq66zh4mklqr3h9d5v3xc") (y #t)))

(define-public crate-lngcnv-1.6.0-alpha.8 (c (n "lngcnv") (v "1.6.0-alpha.8") (h "06v9yyv3haw3a5xxy2yh2bvms5v3iyhy95dsrslglddmb8x97hss") (y #t)))

(define-public crate-lngcnv-1.6.0-alpha.9 (c (n "lngcnv") (v "1.6.0-alpha.9") (h "0219dpbxhwx60pmjh8r7pva6gnmxng50jk14h8fqr6snhwyarlav") (y #t)))

(define-public crate-lngcnv-1.6.0-alpha.10 (c (n "lngcnv") (v "1.6.0-alpha.10") (h "1pd3h9v391x3b65w5m3nxhqzxjb9kr55275vx8cdcy8j1npl64dm") (y #t)))

(define-public crate-lngcnv-1.6.0-alpha.11 (c (n "lngcnv") (v "1.6.0-alpha.11") (h "05kyl0nqphfpx8pkzv9791nyvb69wjmkj69chyqshr50mz5258zb") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.1 (c (n "lngcnv") (v "1.6.0-beta.1") (h "0ik7k7lr8xq18wjq6znc6v37dc4iv9wph5nbgcpsvm9k6yn3lvfi") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.2 (c (n "lngcnv") (v "1.6.0-beta.2") (h "027c3vx2i0zr1b39hqvz5dlnq171sjrmc8hmarjhlv87pwi7xynj") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.3 (c (n "lngcnv") (v "1.6.0-beta.3") (h "19dlyc3cxjqjn3x55ir0lffywq5v5z363qyw6zcg60cjjmp81fly") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.4 (c (n "lngcnv") (v "1.6.0-beta.4") (h "1s4yswwdzwc7wdz60nw5kkc3hxm29fignac939ksxyd84fv6w4dz") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.5 (c (n "lngcnv") (v "1.6.0-beta.5") (h "1gd17q3jvszjv1dv03gx7hhbhms0lzxkqwidrc96q28lymxzk2xi") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.6 (c (n "lngcnv") (v "1.6.0-beta.6") (h "1vrr92scwhq0a9lxz5cb5m1b2g4pk63vpwhpd4l01y7jixlksrsa") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.7 (c (n "lngcnv") (v "1.6.0-beta.7") (h "0gb0gwf4y1cxpa4c20rp36xjv3fq9gpa447dz4ggkm3gqcqfvraq") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.8 (c (n "lngcnv") (v "1.6.0-beta.8") (h "1myaq9jgdcf2jkh959hq67v5nqw3zy5s5ydlsg4kpf9s3xq9m3cv") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.9 (c (n "lngcnv") (v "1.6.0-beta.9") (h "0v8dcjswkvi1l9q28qa77fkalvp9wgr4fgkq3jb34s08w4f9dbd0") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.10 (c (n "lngcnv") (v "1.6.0-beta.10") (h "1xvr4b3rj5mgi23xkflvqqzikd3rcvhjpcq3ipzzls6qfaccyqqk") (y #t)))

(define-public crate-lngcnv-1.6.0-beta.11 (c (n "lngcnv") (v "1.6.0-beta.11") (h "1aymywg1rk25g7fqlrkrwqqmcn1bjnr6vsvikcacyzz450micf92") (y #t)))

(define-public crate-lngcnv-1.6.0 (c (n "lngcnv") (v "1.6.0") (h "0h86343iqz3jxa0v13bpgc3q699iy82x5mir8yk5s0nm4gi2lsr4")))

(define-public crate-lngcnv-1.6.1 (c (n "lngcnv") (v "1.6.1") (h "00mvfwrc5xfxassvk114sahcxws0jgnvci1fazckaayvjzcrv2xi")))

(define-public crate-lngcnv-1.7.0 (c (n "lngcnv") (v "1.7.0") (h "0niplqnxjyzzp3gwcsxgpjn7mnlddkwhp84g47hqmmzn6p4lri24")))

(define-public crate-lngcnv-1.8.0 (c (n "lngcnv") (v "1.8.0") (h "07wig2rbwyq97ynjaihzq9pba1ij31ysmcbvzkh9h1gywkj41af7")))

(define-public crate-lngcnv-1.8.1 (c (n "lngcnv") (v "1.8.1") (h "0kacxman4rx0wzbb4mwyaljpivb85h2ypnsa9pabxganlii9qcig")))

(define-public crate-lngcnv-1.8.2 (c (n "lngcnv") (v "1.8.2") (h "0948k7kmjprvrnk3m4an1sn587kpadbxmxi03b6qn7swapl3ydp9")))

(define-public crate-lngcnv-1.8.3 (c (n "lngcnv") (v "1.8.3") (h "13w8j82z49j3b3qddqv33axrnki9zzb4zfhh2k4k2y6l2fg0i6vc")))

(define-public crate-lngcnv-1.8.4 (c (n "lngcnv") (v "1.8.4") (h "1nm642pjc4cayq9xrp2snxgiapp5dvh20algsr02kyypk568darz")))

(define-public crate-lngcnv-1.8.5 (c (n "lngcnv") (v "1.8.5") (h "0ahw407r3hi0dddwdnni46m1128avh7462ack12glpcwmw58bp46")))

(define-public crate-lngcnv-1.8.6 (c (n "lngcnv") (v "1.8.6") (h "12i9j78p88b6gilrhnxlzl2yjdy0z1f51arb09ami57b4jp465c3")))

(define-public crate-lngcnv-1.8.7 (c (n "lngcnv") (v "1.8.7") (h "02s0591np1mpz61hnna0xdsbl4hgiq3xlqf5z1pp7l7d8s5nzdbv")))

(define-public crate-lngcnv-1.8.8 (c (n "lngcnv") (v "1.8.8") (h "1nq7sf4lnsfb5fph9vqndcx84g5adhn8i0470xs6krhi0jcqj7j3")))

(define-public crate-lngcnv-1.8.9 (c (n "lngcnv") (v "1.8.9") (h "00g8s09jdbxhbmdbqmgmd3p5igs2qh42arfhsgas6rhwxpxqjgb8")))

(define-public crate-lngcnv-1.8.10 (c (n "lngcnv") (v "1.8.10") (h "1qi7cfx6pw2kcbk6knx97asn46i4zc5ni15lfq7i4r5x4skpsyxj")))

(define-public crate-lngcnv-1.8.11 (c (n "lngcnv") (v "1.8.11") (h "091rwapdfz2mm9znfn5crb94vb5q7ygfh0r81xk0913giiq3mbsq")))

(define-public crate-lngcnv-1.8.12 (c (n "lngcnv") (v "1.8.12") (h "0zz8qfymgwk4nfnik52yacrilpp719xig8xq3c3r75f3ahyr4hgy")))

(define-public crate-lngcnv-1.9.0 (c (n "lngcnv") (v "1.9.0") (h "0m246h950inr2a504sr0ifbsipdx9rmqwkzvaqxyg36vssizp4qs")))

(define-public crate-lngcnv-1.9.1 (c (n "lngcnv") (v "1.9.1") (h "1v30cix1h8xc6wxhxffwdvxj3zkj8qg7h9bgybnvsdpr8mlwh6xr")))

(define-public crate-lngcnv-1.9.2 (c (n "lngcnv") (v "1.9.2") (h "04bl113skp02akh7ylgkma5zw98f4q1il92m5mvw99r32sp1aqgr")))

