(define-module (crates-io ln pb lnpbp_derive) #:use-module (crates-io))

(define-public crate-lnpbp_derive-0.1.0 (c (n "lnpbp_derive") (v "0.1.0") (d (list (d (n "amplify") (r "~2.0.6") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0ldrnlqjbyj2f8q7bhs857llrcww5jy5sd4bsfbgjisdkiy9g9y8")))

(define-public crate-lnpbp_derive-0.2.0-alpha.1 (c (n "lnpbp_derive") (v "0.2.0-alpha.1") (d (list (d (n "amplify") (r "~2.0.6") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1pw3ws6ivgbxyk0rc4ypx1s3xqrk5qm0cn43hqg40i67r0ns13am")))

(define-public crate-lnpbp_derive-0.2.0-alpha.2 (c (n "lnpbp_derive") (v "0.2.0-alpha.2") (d (list (d (n "amplify") (r "~2.0.6") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1wvsj0c6xkkdddiqnz82vz87fhkkvc3zc44czmcwsmwi2wk5lqi5")))

(define-public crate-lnpbp_derive-0.2.0-alpha.3 (c (n "lnpbp_derive") (v "0.2.0-alpha.3") (d (list (d (n "amplify") (r "~2.0.6") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "14w8zs71bkhf6gw49cls740gi56k5l2gsrmsp3r2aq0fd8svnfb7")))

(define-public crate-lnpbp_derive-0.2.0-beta.1 (c (n "lnpbp_derive") (v "0.2.0-beta.1") (d (list (d (n "amplify") (r "~2.2.1") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0wvw3pqlzxmpal7crb3j3mpkl6w6gvclf1iq0y1nz8nj3vn92398")))

(define-public crate-lnpbp_derive-0.2.0-beta.2 (c (n "lnpbp_derive") (v "0.2.0-beta.2") (d (list (d (n "amplify") (r "~2.3.1") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "016hxnwgkyg049hf4fy0smr7kanrdfrbq662lkxgyik6h5cb0cv6")))

(define-public crate-lnpbp_derive-0.1.2 (c (n "lnpbp_derive") (v "0.1.2") (d (list (d (n "amplify") (r "~2.3.1") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0d990q0686infh6fdafngqlh0id6fxl6h8nc9zsah5v1ji1h3dv4")))

(define-public crate-lnpbp_derive-0.2.0-beta.3 (c (n "lnpbp_derive") (v "0.2.0-beta.3") (d (list (d (n "amplify") (r "~2.3.1") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "07b2mrb0c2syp2x268sx7spz24hy6nlnd55z6xay0gb1xiqk7g0x")))

(define-public crate-lnpbp_derive-0.2.0-beta.4 (c (n "lnpbp_derive") (v "0.2.0-beta.4") (d (list (d (n "amplify") (r "~2.3.1") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0lx0dj70nj6x5g7m7jb9ymkimwpcc1fm9wy8m154jvh8nxkd8bh1")))

(define-public crate-lnpbp_derive-0.2.0-rc.1 (c (n "lnpbp_derive") (v "0.2.0-rc.1") (d (list (d (n "amplify") (r "~2.3.1") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1fppfhjda1wmwnfd63hn4l2i3qvgvhxp5xav2m8q04narl0dw7zx") (y #t)))

(define-public crate-lnpbp_derive-0.2.0-rc.1+1 (c (n "lnpbp_derive") (v "0.2.0-rc.1+1") (d (list (d (n "amplify") (r "~2.3.1") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "01adjrm0ygifyvlhi8qmnmn70i8l6ps4y8q54zqz61rf8lh0m301") (y #t)))

(define-public crate-lnpbp_derive-0.2.0-rc.1+2 (c (n "lnpbp_derive") (v "0.2.0-rc.1+2") (d (list (d (n "amplify") (r "~2.3.1") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "1gskw700426gr1n328d9qr9fs3bxc6xxka1a6s8v9pcnq09df3pk")))

(define-public crate-lnpbp_derive-0.2.0-rc.2 (c (n "lnpbp_derive") (v "0.2.0-rc.2") (d (list (d (n "amplify") (r "^2.4") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "13lyhsrbrzcx95blrxhjsggp7d5nl94cqfl6s9365cj5xcvr4mjd")))

(define-public crate-lnpbp_derive-0.2.0 (c (n "lnpbp_derive") (v "0.2.0") (d (list (d (n "amplify") (r "^2.4") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.31") (d #t) (k 0)))) (h "0lza9apj85rm8diak5bj9v4562b4ppirpb3kdiyfm681hgf8d4dd")))

(define-public crate-lnpbp_derive-0.3.0-alpha.1 (c (n "lnpbp_derive") (v "0.3.0-alpha.1") (d (list (d (n "amplify") (r "^2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.24") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.58") (d #t) (k 0)))) (h "0qjvb7ld55ykify4mnkbiwslgxdrmawgrrxmnvzi0q8c1f7fj7x4")))

(define-public crate-lnpbp_derive-0.3.0-alpha.2 (c (n "lnpbp_derive") (v "0.3.0-alpha.2") (d (list (d (n "amplify") (r "^2.4") (d #t) (k 0)) (d (n "proc-macro2") (r "~1.0.24") (d #t) (k 0)) (d (n "quote") (r "~1.0.7") (d #t) (k 0)) (d (n "syn") (r "~1.0.58") (d #t) (k 0)))) (h "0x9f8paf9vv0ir34d7l7vgygrq4hsz3a0s9nyjc798w75zd7sc1d")))

