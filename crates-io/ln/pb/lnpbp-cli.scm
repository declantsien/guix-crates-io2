(define-module (crates-io ln pb lnpbp-cli) #:use-module (crates-io))

(define-public crate-lnpbp-cli-0.8.0-rc.1 (c (n "lnpbp-cli") (v "0.8.0-rc.1") (d (list (d (n "bp-cli") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "bp-core") (r "^0.8.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "descriptor-wallet") (r "^0.8.2") (f (quote ("all"))) (d #t) (k 0)) (d (n "lnp-cli") (r "^0.8.0") (d #t) (k 0)) (d (n "rgb-cli") (r "^0.8.0-rc.1") (d #t) (k 0)) (d (n "rgb-std") (r "^0.8.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "rgb20") (r "^0.8.0-rc.3") (f (quote ("all"))) (d #t) (k 0)) (d (n "store-cli") (r "^0.8.0") (d #t) (k 0)) (d (n "storm-cli") (r "^0.8.0-beta.1") (d #t) (k 0)))) (h "1vayzvw7kdjx8jq0iwdqakrsijapjq5nh7hf2g8y92741ipj9c5v") (r "1.59.0")))

