(define-module (crates-io ln pb lnpbp_elgamal) #:use-module (crates-io))

(define-public crate-lnpbp_elgamal-0.5.0-beta.1 (c (n "lnpbp_elgamal") (v "0.5.0-beta.1") (d (list (d (n "amplify") (r "^3.8.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.10.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (f (quote ("rand-std" "global-context-less-secure"))) (d #t) (k 2)))) (h "1bahm8hc01ldkxlcs5y73wgys40mg4mwfmy1ddzkpgw7dscwbjw4")))

(define-public crate-lnpbp_elgamal-0.5.0 (c (n "lnpbp_elgamal") (v "0.5.0") (d (list (d (n "amplify") (r "^3.9.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.10.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (d #t) (k 0)) (d (n "secp256k1") (r "^0.20.3") (f (quote ("rand-std" "global-context-less-secure"))) (d #t) (k 2)))) (h "10w5z489gm8mdhgk6i137f9skrblciwmxyd0isq7v1cpk0zcmhkv")))

(define-public crate-lnpbp_elgamal-0.6.0 (c (n "lnpbp_elgamal") (v "0.6.0") (d (list (d (n "amplify") (r "^3.12.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.10.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.22.1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.22.1") (f (quote ("rand-std" "global-context"))) (d #t) (k 2)))) (h "1fpdd1mdlx9flkmwsz1hq1wchgfnqn0v6v4cnixkc7bz9pljl1f1")))

(define-public crate-lnpbp_elgamal-0.7.0 (c (n "lnpbp_elgamal") (v "0.7.0") (d (list (d (n "amplify") (r "^3.12.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.10.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.22.1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.22.1") (f (quote ("rand-std" "global-context"))) (d #t) (k 2)))) (h "0wsc4f6f68xmafb5bfcnsfjmhd5pg7fz4hj0gn6129vqbz2s74df")))

(define-public crate-lnpbp_elgamal-0.8.0 (c (n "lnpbp_elgamal") (v "0.8.0") (d (list (d (n "amplify") (r "^3.12.1") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.10.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.22.1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.22.1") (f (quote ("rand-std" "global-context"))) (d #t) (k 2)))) (h "0idai4xd7dbjfp0hmg5xj1c410dars4f0fmn6bfsrdzld1bn96x7")))

(define-public crate-lnpbp_elgamal-0.9.0 (c (n "lnpbp_elgamal") (v "0.9.0") (d (list (d (n "amplify") (r "^3.13.0") (d #t) (k 0)) (d (n "bitcoin_hashes") (r "^0.11.0") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24.1") (d #t) (k 0)) (d (n "secp256k1") (r "^0.24.1") (f (quote ("rand-std" "global-context"))) (d #t) (k 2)))) (h "1ykv4vc1rapsvm4pqjj4vlbk4cagbh9z24anxz3m5sa48xmn1dwg")))

