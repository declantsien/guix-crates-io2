(define-module (crates-io ln pb lnpbp_rpc) #:use-module (crates-io))

(define-public crate-lnpbp_rpc-0.8.0-alpha.1 (c (n "lnpbp_rpc") (v "0.8.0-alpha.1") (d (list (d (n "bp_rpc") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "lnp_rpc") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "rgb_rpc") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "store_rpc") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "storm_rpc") (r "^0.8.0-alpha.1") (d #t) (k 0)))) (h "0yyizidqnqdq8j70w02mqfdg37pcq8myf4fgjhhb54f3jxkj1gp6") (r "1.59.0")))

