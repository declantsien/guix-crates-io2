(define-module (crates-io ln pb lnpbp_nodes) #:use-module (crates-io))

(define-public crate-lnpbp_nodes-0.8.0-alpha.1 (c (n "lnpbp_nodes") (v "0.8.0-alpha.1") (d (list (d (n "bp_node") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "lnp_node") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "rgb_node") (r "^0.8.0-alpha.1") (d #t) (k 0)) (d (n "store_daemon") (r "^0.8.0-beta.2") (d #t) (k 0)) (d (n "storm_node") (r "^0.8.0-alpha.1") (d #t) (k 0)))) (h "0ipfipn0gwmadsf81ll4rm0h9wzz6bs6394vdzqfrqdimvdz3v40") (r "1.59.0")))

(define-public crate-lnpbp_nodes-0.8.0-rc.1 (c (n "lnpbp_nodes") (v "0.8.0-rc.1") (d (list (d (n "bp_node") (r "^0.8.0-alpha.2") (d #t) (k 0)) (d (n "lnp_node") (r "^0.8.0") (f (quote ("all"))) (d #t) (k 0)) (d (n "rgb_node") (r "^0.8.0-rc.1") (d #t) (k 0)) (d (n "store_daemon") (r "^0.8.0") (d #t) (k 0)) (d (n "storm_node") (r "^0.8.0-beta.1") (d #t) (k 0)))) (h "16ywl0wpkp2zb45jx99xf36lcg30h290qg8nbqxnfjxhcqhnhx1s") (r "1.59.0")))

