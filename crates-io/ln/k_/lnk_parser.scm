(define-module (crates-io ln k_ lnk_parser) #:use-module (crates-io))

(define-public crate-lnk_parser-0.1.0 (c (n "lnk_parser") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "winparsingtools") (r "^1.1.0") (d #t) (k 0)))) (h "16zw7xf4d87ck1i6ln737iwdyappyz77zvag730qbsrznvczm4qj")))

(define-public crate-lnk_parser-0.4.0 (c (n "lnk_parser") (v "0.4.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.15") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "getset") (r "^0.1.2") (d #t) (k 0)) (d (n "glob") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.123") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "winparsingtools") (r "^2") (d #t) (k 0)))) (h "05p5cjnaj4v1v5zf13h3rx0plh319hm2k5lnfnmccfqs3p54g32p")))

