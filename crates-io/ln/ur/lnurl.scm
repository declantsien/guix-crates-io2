(define-module (crates-io ln ur lnurl) #:use-module (crates-io))

(define-public crate-lnurl-0.1.0 (c (n "lnurl") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "0mha6vq92vfd7w72cvj1sym3mj2c2wsip4m8lfv3mfi43lbxvf6l")))

(define-public crate-lnurl-0.1.1 (c (n "lnurl") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)))) (h "1jxl5gnfdlqf6ah8336z35i4qhfblmlzyyxz9iq2iy6v9s8hh90a")))

(define-public crate-lnurl-0.2.0 (c (n "lnurl") (v "0.2.0") (d (list (d (n "bech32") (r "^0.7.1") (d #t) (k 2)) (d (n "hex") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "image") (r "^0.22.3") (d #t) (k 2)) (d (n "qrcode") (r "^0.11.0") (d #t) (k 2)) (d (n "rand") (r "^0.7.3") (d #t) (k 2)) (d (n "secp256k1") (r "^0.17.2") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0.93") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("macros"))) (d #t) (k 2)) (d (n "tracing") (r "^0.1") (d #t) (k 2)) (d (n "tracing-subscriber") (r "^0.2") (d #t) (k 2)) (d (n "warp") (r "^0.2.4") (d #t) (k 2)))) (h "15pcvycdmmmf5ysj7hwcdsijkhszvyd038zwbfgh4ahiay7rlkiv") (f (quote (("default") ("auth" "hex" "secp256k1"))))))

