(define-module (crates-io ln ur lnurl-tool) #:use-module (crates-io))

(define-public crate-lnurl-tool-0.1.0 (c (n "lnurl-tool") (v "0.1.0") (d (list (d (n "bech32") (r "^0.9.1") (d #t) (k 0)) (d (n "clap") (r "^4.3.6") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ch42n1af9vnli4qcddlcyp2k2hz6ygbvga5yfm752gikfh2grky")))

