(define-module (crates-io ln ur lnurl-cli) #:use-module (crates-io))

(define-public crate-lnurl-cli-0.1.0 (c (n "lnurl-cli") (v "0.1.0") (d (list (d (n "bitcoin") (r "^0.29.2") (f (quote ("rand"))) (d #t) (k 0)) (d (n "clap") (r "^4.4.7") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lnurl-rs") (r "^0.3.0") (d #t) (k 0)))) (h "18ma4jiww0r0z75yfygpj3d1yd6by5d645l64ampsijiyglfvl1k")))

