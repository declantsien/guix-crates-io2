(define-module (crates-io ln kr lnkr) #:use-module (crates-io))

(define-public crate-lnkr-0.1.0 (c (n "lnkr") (v "0.1.0") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1z602im3chwnix5iyb40dx9kg2grrz3i3ppj9zxbzfq3fg86mz94")))

(define-public crate-lnkr-0.1.1 (c (n "lnkr") (v "0.1.1") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1k08rp2g1ah26pqs6k90186ypk1yvij2yrnbrf7lvd5f0981ld4g")))

(define-public crate-lnkr-0.1.2 (c (n "lnkr") (v "0.1.2") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "18cfi3nvg8gd9q355i65by6wgjaawar4527z3c8xmaj8kyn1gjq9")))

(define-public crate-lnkr-0.1.3 (c (n "lnkr") (v "0.1.3") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "1izcakn6g1bqd0k97bn3gzpd46csr9w5kbar2chbg6bd8lnna4yx")))

(define-public crate-lnkr-0.1.4 (c (n "lnkr") (v "0.1.4") (d (list (d (n "directories") (r "^5.0.1") (d #t) (k 0)) (d (n "eyre") (r "^0.6.8") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.41") (d #t) (k 0)))) (h "07yab1k268f5zvgwcx57dad59bpyqkpvvvs8n1fnd8hy44h1lzfm")))

