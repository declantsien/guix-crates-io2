(define-module (crates-io ln k- lnk-thrussh-encoding) #:use-module (crates-io))

(define-public crate-lnk-thrussh-encoding-0.1.0 (c (n "lnk-thrussh-encoding") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4") (d #t) (k 0)) (d (n "lnk-cryptovec") (r "^0.6.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "19piys14jw2jlfd7wdkhz83znsgh502fpvmphwcnkib12d2l0g4b")))

