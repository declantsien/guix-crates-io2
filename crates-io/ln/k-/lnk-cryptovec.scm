(define-module (crates-io ln k- lnk-cryptovec) #:use-module (crates-io))

(define-public crate-lnk-cryptovec-0.6.1 (c (n "lnk-cryptovec") (v "0.6.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("basetsd" "minwindef" "memoryapi"))) (d #t) (k 0)))) (h "0nzzhz3dp1f6sh2bvdr5a63qc516spjv8qvggp2x8amaxzcjlcdf")))

