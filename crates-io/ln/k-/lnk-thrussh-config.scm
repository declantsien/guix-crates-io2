(define-module (crates-io ln k- lnk-thrussh-config) #:use-module (crates-io))

(define-public crate-lnk-thrussh-config-0.6.0 (c (n "lnk-thrussh-config") (v "0.6.0") (d (list (d (n "dirs-next") (r "^2.0") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.0") (f (quote ("io-util" "net" "macros" "process"))) (d #t) (k 0)) (d (n "whoami") (r "^1.0") (d #t) (k 0)))) (h "0z5412l0rbb9wskypd7x3wcdcm7dbzv10p26s66zgzz9ji65y8n2")))

