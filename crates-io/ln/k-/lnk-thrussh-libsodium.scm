(define-module (crates-io ln k- lnk-thrussh-libsodium) #:use-module (crates-io))

(define-public crate-lnk-thrussh-libsodium-0.2.1 (c (n "lnk-thrussh-libsodium") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "0x6w97jhgwd6s2lvsyjzx4py28kvq4p2ma12a1zki01m3wjd6z2x")))

