(define-module (crates-io ln ex lnexp) #:use-module (crates-io))

(define-public crate-lnexp-0.1.0 (c (n "lnexp") (v "0.1.0") (h "13hz40mglrzw9ga2gjiak1q7cxysfrjhwcdvyswmcbwlcw8749wp")))

(define-public crate-lnexp-0.2.0 (c (n "lnexp") (v "0.2.0") (h "1v4dynnhzqgvljr8sp2ffrbx6hg7y4x66hkxng3ba45fjbrlamgm")))

(define-public crate-lnexp-0.2.1 (c (n "lnexp") (v "0.2.1") (h "0j5rdnyr0g1v2dgb8x5fdifddrbp368szcdyyvf36fqhg33j92gr")))

