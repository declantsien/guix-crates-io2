(define-module (crates-io ln gh lngh_strings) #:use-module (crates-io))

(define-public crate-lngh_strings-0.1.0 (c (n "lngh_strings") (v "0.1.0") (h "05r3b4mwhlvmqh27yp0j4b4r9xj4gabr8jmyx3dy1cpjpwrm242l")))

(define-public crate-lngh_strings-0.2.0 (c (n "lngh_strings") (v "0.2.0") (h "0yw8ni65km6rxabyphj4565l1jvl7fgyd8fib1jk1dbzv00bc82x")))

