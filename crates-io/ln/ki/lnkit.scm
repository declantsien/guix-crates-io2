(define-module (crates-io ln ki lnkit) #:use-module (crates-io))

(define-public crate-lnkit-0.1.0 (c (n "lnkit") (v "0.1.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "paperplane") (r "^0.4") (d #t) (k 0)) (d (n "rapier3d") (r "^0.8") (f (quote ("simd-nightly" "parallel" "serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1vi5mdvpys7ld6iqzz70pjmrlrhl8fm71bs58hyvisdkxmqprkvl")))

(define-public crate-lnkit-0.2.0 (c (n "lnkit") (v "0.2.0") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "paperplane") (r "^0.4") (d #t) (k 0)) (d (n "rapier3d") (r "^0.8") (f (quote ("simd-nightly" "parallel" "serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1nx7pbnw187vdprwgkdqnvgzjkvnwh2d784wlaqx09iz13b2jgf4")))

(define-public crate-lnkit-0.2.1 (c (n "lnkit") (v "0.2.1") (d (list (d (n "async-std") (r "^1") (f (quote ("attributes"))) (d #t) (k 0)) (d (n "paperplane") (r "^0.4") (d #t) (k 0)) (d (n "rapier3d") (r "^0.8") (f (quote ("simd-nightly" "parallel" "serde-serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "173zsszyfi2rdzhwj1xqafphxd586vyd6c0gkzxrszrb3x9g50jq")))

