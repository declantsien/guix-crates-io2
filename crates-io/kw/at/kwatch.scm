(define-module (crates-io kw at kwatch) #:use-module (crates-io))

(define-public crate-kwatch-0.1.0 (c (n "kwatch") (v "0.1.0") (d (list (d (n "colored") (r "^1.4") (d #t) (k 0)) (d (n "docopt") (r "^0.7.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.4.0") (d #t) (k 0)) (d (n "kqueue") (r "^0.2.0") (d #t) (k 0)) (d (n "log") (r "^0.3.6") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.22") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "walkdir") (r "^1.0.7") (d #t) (k 0)))) (h "0spzb8wg4r47ni43r7amj2myprdphikskdzilvyssg3hs0kivmv2")))

