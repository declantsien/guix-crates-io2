(define-module (crates-io kw r1 kwr103) #:use-module (crates-io))

(define-public crate-kwr103-0.1.0 (c (n "kwr103") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "clap") (r "^4.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serialport") (r "^4.3") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1y98lh9yv01qsch6acwl3cdvicfixl7bqzg5845dbhc9sqh9p4br")))

