(define-module (crates-io kw iw kwiwk-experimental) #:use-module (crates-io))

(define-public crate-kwiwk-experimental-0.1.0 (c (n "kwiwk-experimental") (v "0.1.0") (h "0vf9gm4248hrizyvhf9qzl8friz6p21l7hxwi1j7q7j4855xg43i")))

(define-public crate-kwiwk-experimental-0.2.0 (c (n "kwiwk-experimental") (v "0.2.0") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "1zq57cqdyydrd5vq97skfav0ck27zrw067a3kz2nz9jrrdvllkpa")))

(define-public crate-kwiwk-experimental-0.2.2 (c (n "kwiwk-experimental") (v "0.2.2") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "04fz2dc7vidmr5mab6q99lyp81kp8fcmp51ar65xnh0630d5g4jr")))

(define-public crate-kwiwk-experimental-0.2.3 (c (n "kwiwk-experimental") (v "0.2.3") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "0g2dg4vylvip973dmhi4dwxaas08rakc5nmc1hsphflk49h6k6dx")))

(define-public crate-kwiwk-experimental-0.2.4 (c (n "kwiwk-experimental") (v "0.2.4") (d (list (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "03zhj6ya65b7xrqjf60ryykf368cg36mbmdvxccx6pa50r1q0p3d")))

(define-public crate-kwiwk-experimental-0.3.0 (c (n "kwiwk-experimental") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.6") (d #t) (k 0)) (d (n "env_logger") (r "^0.5.13") (d #t) (k 0)) (d (n "log") (r "^0.4.5") (d #t) (k 0)))) (h "127sf9hjpfh8ha2ixf5bkk8nh4q7pff5brk5dlkbz6knk1hqkwp4")))

