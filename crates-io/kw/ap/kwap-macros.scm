(define-module (crates-io kw ap kwap-macros) #:use-module (crates-io))

(define-public crate-kwap-macros-0.1.1 (c (n "kwap-macros") (v "0.1.1") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19k4ncl29njdvsdxpn9n5c1mipz0255pqlkfc93265fd9qw021ws")))

(define-public crate-kwap-macros-0.1.2 (c (n "kwap-macros") (v "0.1.2") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0c8wd72jrzkrz3sv0n3nqmh061a1lh82frhi3fvmkykwzgf5443s")))

(define-public crate-kwap-macros-0.1.3 (c (n "kwap-macros") (v "0.1.3") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0p9c8b5mdb6iiin02lxc1kf3lbl2nklljhkbmmdsicahr454j1fv")))

(define-public crate-kwap-macros-0.1.4 (c (n "kwap-macros") (v "0.1.4") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0rgwvsfz9ng9d143p3g8g8q9j67jyl011li258aaq66cm9d8qkm5")))

(define-public crate-kwap-macros-0.1.5 (c (n "kwap-macros") (v "0.1.5") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "018hy9v9z8ld98af9i0zayyca4qfrps3xqpj6qlf7sw67w89gqai")))

(define-public crate-kwap-macros-0.1.6 (c (n "kwap-macros") (v "0.1.6") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "05b8x7x0kmhfxdyj5f86nxl682anxgy24v7r5lpz4b0s037b2v9f")))

(define-public crate-kwap-macros-0.1.7 (c (n "kwap-macros") (v "0.1.7") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "regex") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "11ijr5l21bh8svvvj228q6lhc32d9ck7wvwi1wfxw4qaa8lcqbzq")))

