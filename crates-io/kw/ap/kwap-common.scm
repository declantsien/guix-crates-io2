(define-module (crates-io kw ap kwap-common) #:use-module (crates-io))

(define-public crate-kwap-common-0.2.6 (c (n "kwap-common") (v "0.2.6") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1hhclmww04alhgia3j9c68mmgnj0jhk7w4d8msz7yail6b1d74yw")))

(define-public crate-kwap-common-0.3.0 (c (n "kwap-common") (v "0.3.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1pvg64c91xw2hc0bc6wngxsvkc1hq7rr5dssg4d2r5ahf7x0k6r2")))

(define-public crate-kwap-common-0.4.0 (c (n "kwap-common") (v "0.4.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1qidym589j8k5p7jgmz0farj5p1vxkdkcmidwm4cfmcmabh5m47b")))

(define-public crate-kwap-common-0.5.0 (c (n "kwap-common") (v "0.5.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1i5zmxam4wixd8zkrrysh975aqmm7fl56zd35bn1bqh25gdi6pnd")))

(define-public crate-kwap-common-0.6.0 (c (n "kwap-common") (v "0.6.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1lfcbyl39z7fg56dg5s48m1kzj6m8l79g81zqvncw0xpa9v7bxck")))

(define-public crate-kwap-common-0.6.1 (c (n "kwap-common") (v "0.6.1") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "1kybnhlwigb00ifgf88gplhgzfnmzfb72wqwljhiw7ja9xnxkkci")))

(define-public crate-kwap-common-0.6.2 (c (n "kwap-common") (v "0.6.2") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "14r8rym23b55f3x7j8r8mkkfbn6paq565pshyib1hi4gcwphax88")))

(define-public crate-kwap-common-0.6.3 (c (n "kwap-common") (v "0.6.3") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "088rwixyljx157bxkrn1fm5dg9zsi5ymhsklvpkzpcnb651571mv")))

(define-public crate-kwap-common-0.6.4 (c (n "kwap-common") (v "0.6.4") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "19p20wpj9s6n20qf5wab8mxgibb8ihkydggy7alqa7c2iaim3dfy")))

(define-public crate-kwap-common-0.7.0 (c (n "kwap-common") (v "0.7.0") (d (list (d (n "tinyvec") (r "^1.5") (f (quote ("rustc_1_55"))) (k 0)))) (h "0qinwgj9j73rw4l66jbanmvvbllhmbll2rrx66bcspad52pyr4zr")))

