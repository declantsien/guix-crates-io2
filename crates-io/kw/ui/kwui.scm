(define-module (crates-io kw ui kwui) #:use-module (crates-io))

(define-public crate-kwui-0.1.0 (c (n "kwui") (v "0.1.0") (d (list (d (n "embed-resource") (r "^2.4.1") (d #t) (k 1)) (d (n "kwui-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "windows_dpi") (r "^0.3.0") (d #t) (k 2)))) (h "0h7knvw2gpd7976gayv7hzr4jr2d2gpkhi1rs9qsq6sma4gx6bqn")))

(define-public crate-kwui-0.1.1 (c (n "kwui") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 2)) (d (n "embed-resource") (r "^2.4.1") (d #t) (k 1)) (d (n "kwui-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.24") (d #t) (k 2)) (d (n "rss") (r "^2.0.7") (d #t) (k 2)) (d (n "tokio") (r "^1.36.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "windows_dpi") (r "^0.3.0") (d #t) (k 2)))) (h "1slyq8rhf26q8g3ykfrpx58lfcf93sycgr06v8kadnqknpwc1l2r")))

