(define-module (crates-io kw ui kwui-cli) #:use-module (crates-io))

(define-public crate-kwui-cli-0.1.2 (c (n "kwui-cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "byteorder") (r "^1.5.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.11") (f (quote ("derive"))) (d #t) (k 0)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)) (d (n "itertools") (r "^0.12.0") (d #t) (k 0)) (d (n "lzf") (r "^1.0.0") (d #t) (k 0)) (d (n "num_enum") (r "^0.7.1") (d #t) (k 0)) (d (n "path-clean") (r "^1.0.1") (d #t) (k 0)) (d (n "sha1") (r "^0.10.6") (d #t) (k 0)) (d (n "size") (r "^0.4.1") (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)))) (h "0pp6j01ahd1633489kcpspc6wckvr33wz88vv6w5l87lgbjl8lc1")))

