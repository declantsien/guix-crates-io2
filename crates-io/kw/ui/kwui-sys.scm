(define-module (crates-io kw ui kwui-sys) #:use-module (crates-io))

(define-public crate-kwui-sys-0.1.0 (c (n "kwui-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "119am391lkl67wpxf87k6wvjfa86h082pnr4ibsczvi7xs7k5mcp") (l "kwui_static")))

(define-public crate-kwui-sys-0.1.1 (c (n "kwui-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.4") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1s84cs0mqz8mszkn2dzz386rd82xr8vwp482q143kb69qyxnips7") (l "kwui_static")))

