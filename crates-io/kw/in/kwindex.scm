(define-module (crates-io kw in kwindex) #:use-module (crates-io))

(define-public crate-kwindex-0.1.0 (c (n "kwindex") (v "0.1.0") (h "1srlhsvhvsf31ainqarry4314haw1py6mdapwzf9h926bylsla1l")))

(define-public crate-kwindex-0.1.1 (c (n "kwindex") (v "0.1.1") (h "1arpr5ql1mzsw1bdcfarjm2s2gar6i8yp8cm1z4sbcc1wwwjyr25")))

(define-public crate-kwindex-0.1.2 (c (n "kwindex") (v "0.1.2") (h "1zh2xd1bsq4jpnlmz1gd6jdfdinzmdkw95qwcnjfj8766a9d70qq")))

