(define-module (crates-io kw ai kwai_interactive_live_bevy_plugin) #:use-module (crates-io))

(define-public crate-kwai_interactive_live_bevy_plugin-0.1.0 (c (n "kwai_interactive_live_bevy_plugin") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "async-compat") (r "^0.2") (d #t) (k 0)) (d (n "bevy") (r "^0.8") (d #t) (k 2)) (d (n "bevy_app") (r "^0.8") (d #t) (k 0)) (d (n "bevy_ecs") (r "^0.8") (d #t) (k 0)) (d (n "bevy_tasks") (r "^0.8") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "kwai_interactive_live") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)))) (h "05pnipl2v5aj5jngx5wq5akj8hi8xkw0fn6jlrifc1qrisy3k59p") (y #t)))

