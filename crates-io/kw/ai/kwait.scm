(define-module (crates-io kw ai kwait) #:use-module (crates-io))

(define-public crate-kwait-0.0.1 (c (n "kwait") (v "0.0.1") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)) (d (n "rand") (r "^0.7") (d #t) (k 0)) (d (n "scopeguard") (r "^1.1.0") (d #t) (k 0)))) (h "1y9rqns0sx06mg7fb3qz6z602izfp4b3d295a7x58942hb6w8c3i")))

