(define-module (crates-io ed gy edgy) #:use-module (crates-io))

(define-public crate-edgy-0.1.0 (c (n "edgy") (v "0.1.0") (d (list (d (n "clap") (r "^2.32") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)))) (h "086aj3m3c3fl4237wzj18gqqvfd830ssdhky0zv71ly7k57126md")))

(define-public crate-edgy-0.1.1 (c (n "edgy") (v "0.1.1") (d (list (d (n "clap") (r "^2.32") (f (quote ("yaml"))) (d #t) (k 0)) (d (n "image") (r "^0.21.0") (d #t) (k 0)) (d (n "pbr") (r "^1.0.1") (d #t) (k 0)))) (h "1xkw966hjfzz6i9w4d404bpihga4wr9d1vz05bna7hrpmbj1wcxs")))

