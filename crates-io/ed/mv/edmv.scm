(define-module (crates-io ed mv edmv) #:use-module (crates-io))

(define-public crate-edmv-0.1.0 (c (n "edmv") (v "0.1.0") (h "13v7addk51fsjgr3diy1w64gppzcgd65kkqzx7i6844rq8kmgh8a") (y #t)))

(define-public crate-edmv-0.0.1 (c (n "edmv") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "executable-path") (r "^1.0.0") (d #t) (k 2)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "unindent") (r "^0.2.1") (d #t) (k 2)))) (h "1i1fgdy6wjrmymc3g68j18ca2s9g92h02wnl9hmdnvs9rcibr5z8") (y #t)))

(define-public crate-edmv-1.0.0 (c (n "edmv") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 0)) (d (n "clap") (r "^4.3.10") (f (quote ("derive"))) (d #t) (k 0)) (d (n "executable-path") (r "^1.0.0") (d #t) (k 2)) (d (n "path-absolutize") (r "^3.1.0") (d #t) (k 0)) (d (n "pretty_assertions") (r "^1.3.0") (d #t) (k 2)) (d (n "tempfile") (r "^3.6.0") (d #t) (k 0)) (d (n "unindent") (r "^0.2.1") (d #t) (k 2)))) (h "03x563nvyc3mfmvv22fl0x5xqcmc09zs4sxxcgpmgz17ib22fw6q")))

