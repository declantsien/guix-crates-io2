(define-module (crates-io ed b- edb-derive) #:use-module (crates-io))

(define-public crate-edb-derive-0.1.0 (c (n "edb-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1c176pzqvm0zh756lhvfvf1f7qhsa6jz5xmhcv3gc7s5x4w7if3c")))

(define-public crate-edb-derive-0.1.1 (c (n "edb-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1v598664pqzyq1w012vphsxsa72vd9yrmjgja1idj2mzzx5x64wp")))

(define-public crate-edb-derive-0.1.2 (c (n "edb-derive") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17y457n5263d0aqj1bjig3a7jsrjvb5lx26xpq3lqcjphax6ya28")))

