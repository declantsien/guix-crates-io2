(define-module (crates-io ed it editres_cli) #:use-module (crates-io))

(define-public crate-editres_cli-0.1.1 (c (n "editres_cli") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "editres") (r "^0.1") (f (quote ("injector"))) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.9") (d #t) (k 2)))) (h "0ka1viryhnajh13hbda4wdq1vff3yfmri44q1pg7dzz630y49ni0")))

(define-public crate-editres_cli-0.1.2 (c (n "editres_cli") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "editres") (r "^0.1") (f (quote ("injector"))) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.9") (d #t) (k 2)))) (h "0kw06ggkkrxmyw28984cfj172zgp7j4nnpx2qaayywdgkkgr57f1")))

(define-public crate-editres_cli-0.1.4 (c (n "editres_cli") (v "0.1.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "editres") (r "^0.1") (f (quote ("injector"))) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.9") (d #t) (k 2)))) (h "12q7zh070rpc1vjc79fx2wr2265hihfwp5b8zfv12vjp747mj0qb")))

(define-public crate-editres_cli-0.1.5 (c (n "editres_cli") (v "0.1.5") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "editres") (r "^0.1") (f (quote ("injector"))) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.9") (d #t) (k 2)))) (h "1awdvc6wfqsgh8y3dcbxly7i9iwsq6fdjzakrhm1clq3hbgdr026")))

(define-public crate-editres_cli-0.1.6 (c (n "editres_cli") (v "0.1.6") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "bincode") (r "^1") (d #t) (k 2)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "editres") (r "^0.1") (f (quote ("injector"))) (k 0)) (d (n "fastrand") (r "^2.0.1") (d #t) (k 2)) (d (n "tempfile") (r "^3.9") (d #t) (k 2)))) (h "1pgnc713ph8hpgnsdsrsp0xllm0m6y1bbwvv86bx39wdrz0rzpxl")))

