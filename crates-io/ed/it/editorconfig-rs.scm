(define-module (crates-io ed it editorconfig-rs) #:use-module (crates-io))

(define-public crate-editorconfig-rs-0.1.0 (c (n "editorconfig-rs") (v "0.1.0") (d (list (d (n "editorconfig-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0clczlwl38ivr24kyhpyaxanlq1czdfk6jsk8ngpizlvrpgpifq0")))

(define-public crate-editorconfig-rs-0.2.0 (c (n "editorconfig-rs") (v "0.2.0") (d (list (d (n "editorconfig-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0zy4i5r8gvpsn125rgzkk7z8b0r6vf0aky60km9bgm3j4d5hlvx4")))

(define-public crate-editorconfig-rs-0.2.1 (c (n "editorconfig-rs") (v "0.2.1") (d (list (d (n "editorconfig-sys") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lridzkvlcn8qk3g7a7d6y00xmjs8mprfmpwi4n9bm19cig2cx1d")))

