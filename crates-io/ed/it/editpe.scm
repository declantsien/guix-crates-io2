(define-module (crates-io ed it editpe) #:use-module (crates-io))

(define-public crate-editpe-0.1.0 (c (n "editpe") (v "0.1.0") (d (list (d (n "debug-ignore") (r "^1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.10") (d #t) (k 2)) (d (n "image") (r "^0.24") (f (quote ("ico" "jpeg" "png" "tga" "tiff" "webp" "bmp"))) (o #t) (k 0)) (d (n "indexmap") (r "^2.1") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "zerocopy") (r "^0.7") (f (quote ("derive"))) (d #t) (k 0)))) (h "048jn277l7g0sqmlafvdd29cvq235iwp7yb9sacdazdhn4mxxkj8") (f (quote (("default" "image")))) (s 2) (e (quote (("image" "dep:image"))))))

