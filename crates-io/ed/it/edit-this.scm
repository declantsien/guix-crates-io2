(define-module (crates-io ed it edit-this) #:use-module (crates-io))

(define-public crate-edit-this-1.0.0 (c (n "edit-this") (v "1.0.0") (h "065yz7mfkv1rqzmhwkfn8k9bzy5qjaarpxn960gfwg7gh7zhm69x") (y #t)))

(define-public crate-edit-this-1.0.1 (c (n "edit-this") (v "1.0.1") (h "1mqlvklqblqfnr146ks1g5f6b0x10vw3vapi1y3r499bh102f3fg") (y #t)))

