(define-module (crates-io ed it edit-xlsx) #:use-module (crates-io))

(define-public crate-edit-xlsx-0.1.0 (c (n "edit-xlsx") (v "0.1.0") (h "0aj5fd7s1w5b4fgw50gjqw7vdi845kyyi429b9jz5p9y050p22kr")))

(define-public crate-edit-xlsx-0.2.0 (c (n "edit-xlsx") (v "0.2.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1imvx5mrlhbcb70wgxxmias2n68qgrsjx0srsix6n47brkbz86lm")))

(define-public crate-edit-xlsx-0.3.0 (c (n "edit-xlsx") (v "0.3.0") (d (list (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1ikppj3cidhmhg93v2pzzfxjmpfjkyrh50rkh3b37hzb6hqmbfr3") (y #t)))

(define-public crate-edit-xlsx-0.3.1 (c (n "edit-xlsx") (v "0.3.1") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1m9g9q4n19v2sxmkdd6knwaf4ac9dx531fvb7cbdm7ficjmycgak")))

(define-public crate-edit-xlsx-0.3.3 (c (n "edit-xlsx") (v "0.3.3") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "14y7vd2nk9lyci4y75mamb1j31wl7xs9cf9a6zf7i3m7amxjd2kp")))

(define-public crate-edit-xlsx-0.3.4 (c (n "edit-xlsx") (v "0.3.4") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0665bypvxjqbw8xpginj42i9dba44k7p83nypl4f8zmsma6ik3gz")))

(define-public crate-edit-xlsx-0.3.5 (c (n "edit-xlsx") (v "0.3.5") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "17rsbzfar7yr84yk54n1g13fki4dk9cvawxxjhfykc31xynm87ln")))

(define-public crate-edit-xlsx-0.3.6 (c (n "edit-xlsx") (v "0.3.6") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1q2kxmjpjz283ws614c5ys37pwsvf1g8jwn733axmqvnddj1mb88") (y #t)))

(define-public crate-edit-xlsx-0.3.7 (c (n "edit-xlsx") (v "0.3.7") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "05lqglqmyc7wkfmm0ad7ry2y1gyfqwglvicxwwqj7f51s520rm7j")))

(define-public crate-edit-xlsx-0.3.8 (c (n "edit-xlsx") (v "0.3.8") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1x9px5hyx6am85zkmh8wcdrs3362gj8r14xw1scxkmkgvk468pz7")))

(define-public crate-edit-xlsx-0.3.9 (c (n "edit-xlsx") (v "0.3.9") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "09md71589wjkkll83pc2y07hbdk6wcldmff6grs5n92jwfjkdxh9")))

(define-public crate-edit-xlsx-0.4.0 (c (n "edit-xlsx") (v "0.4.0") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0989k4y22ydy103r5nay3f8k1vihd3bi79v7r7pb2y78krpcpirz") (y #t)))

(define-public crate-edit-xlsx-0.4.1 (c (n "edit-xlsx") (v "0.4.1") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "04ijgsk77pyygxj02749kln87xqxsp526rd91vii9jymz5wxha4g")))

(define-public crate-edit-xlsx-0.4.2 (c (n "edit-xlsx") (v "0.4.2") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1a8w17mhavx3lqjc8dsbkxq2c2yy9fxv10h949jws93k4133bh61")))

(define-public crate-edit-xlsx-0.4.3 (c (n "edit-xlsx") (v "0.4.3") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "1cpr0zjpvh0wam5c6bzmcmmdjhlvxm9a5g52xd4hbvfslb655a41")))

(define-public crate-edit-xlsx-0.4.4 (c (n "edit-xlsx") (v "0.4.4") (d (list (d (n "futures") (r "^0.3.30") (d #t) (k 0)) (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "walkdir") (r "^2.4.0") (d #t) (k 0)) (d (n "zip") (r "^0.6.6") (d #t) (k 0)))) (h "0glv83qdz0gfha0yjyxd9zw3rp55ap7na4cymm2mx739qj045x8i")))

