(define-module (crates-io ed it edit_tree) #:use-module (crates-io))

(define-public crate-edit_tree-0.1.0 (c (n "edit_tree") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "seqalign") (r "^0.2.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1yxc6plw0zy5b0d3vv22svlhlwygzk1780w9bzcqxh9z44hfi125")))

(define-public crate-edit_tree-0.1.1 (c (n "edit_tree") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "seqalign") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "07c93c12d7qdv1928mdcm755v69cmcdqynqpsgwhzdah6flckzpq")))

(define-public crate-edit_tree-0.1.2 (c (n "edit_tree") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "seqalign") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "1rlcfsi07mi1xkrcj2n61y8mcz6scw3fxsvsxjmnj7h9sfl5pdf2")))

(define-public crate-edit_tree-0.2.0 (c (n "edit_tree") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.1") (d #t) (k 0)) (d (n "seqalign") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "04m8qmjmfwx1mll3pb10k6vj1aiz87advm399iprp53fnv4nwnfl")))

(define-public crate-edit_tree-0.2.1 (c (n "edit_tree") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "seqalign") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)))) (h "1lgynj479dqc55h2hfjppl46iddlc9yn6dqk0dcv80mx7y1av50f")))

