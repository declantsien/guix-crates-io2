(define-module (crates-io ed it editor-input) #:use-module (crates-io))

(define-public crate-editor-input-0.1.0 (c (n "editor-input") (v "0.1.0") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "008k5d5hirznc57w943bmdg24kfv6xvadk86acrr6ig9yv9sp0d7")))

(define-public crate-editor-input-0.1.1 (c (n "editor-input") (v "0.1.1") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "0lfqmswxvazgjfnk861c1rbqh91123daxn2pwgzg1qrlw00ign4l")))

(define-public crate-editor-input-0.1.2 (c (n "editor-input") (v "0.1.2") (d (list (d (n "tempfile") (r "^3") (d #t) (k 0)))) (h "1rskpbxws3xi8ryj2vz2afz724wj5xjjsn5jiafk38a6vqsy49pq")))

