(define-module (crates-io ed it editorconfig-sys) #:use-module (crates-io))

(define-public crate-editorconfig-sys-0.1.0 (c (n "editorconfig-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "0lkqzkzn4ajvmwxv651py6bwwdk012nw4k2h95lrmj6hrc378p22") (l "editorconfig")))

(define-public crate-editorconfig-sys-0.1.1 (c (n "editorconfig-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.2") (d #t) (k 1)) (d (n "pkg-config") (r "^0.3.29") (d #t) (k 1)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1vd2r43ks9ssf390v3wgw01ky3h2kkksmjagsgffcw8x2v5gjg78") (f (quote (("buildtime-bindgen")))) (l "editorconfig")))

