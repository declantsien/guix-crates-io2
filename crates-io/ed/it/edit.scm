(define-module (crates-io ed it edit) #:use-module (crates-io))

(define-public crate-edit-0.1.0 (c (n "edit") (v "0.1.0") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^3.1.0") (o #t) (k 0)))) (h "0cgrgnh1rnsbwfdj4yz74af0wpqiazqw2x872kmz5m9zz4g4xhz4") (f (quote (("default" "better-path") ("better-path" "which"))))))

(define-public crate-edit-0.1.1 (c (n "edit") (v "0.1.1") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^3.1.0") (o #t) (k 0)))) (h "143nhrmzd91cvvhrx4a4lzp2wji8c7j5bf8szmjkgd6ph016l47c") (f (quote (("default" "better-path") ("better-path" "which"))))))

(define-public crate-edit-0.1.2 (c (n "edit") (v "0.1.2") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^3.1.0") (o #t) (k 0)))) (h "1z1vvd07i7f042a5ca217v2ic70m2mw6wvdli355lvxsgr234c1j") (f (quote (("default" "better-path") ("better-path" "which"))))))

(define-public crate-edit-0.1.3 (c (n "edit") (v "0.1.3") (d (list (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.0") (o #t) (k 0)))) (h "1q0f7mhsv2z8wyrqnx755jcli1fdpx9ziviji7i855xxz0v6kp9w") (f (quote (("default" "better-path") ("better-path" "which"))))))

(define-public crate-edit-0.1.4 (c (n "edit") (v "0.1.4") (d (list (d (n "shell-words") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.0") (o #t) (k 0)))) (h "1d7v2yd2jckqr1v4rd1fgihqnad5wlxgkxbb9kg1ysdwyxqslqn5") (f (quote (("quoted-env" "shell-words") ("default" "better-path") ("better-path" "which"))))))

(define-public crate-edit-0.1.5 (c (n "edit") (v "0.1.5") (d (list (d (n "shell-words") (r "^1.1.0") (o #t) (d #t) (k 0)) (d (n "tempfile") (r "^3.1.0") (d #t) (k 0)) (d (n "which") (r "^4.0") (o #t) (k 0)))) (h "02dan6bg9pcj42ny48g8fq9f76w30c826n4gihy1d1s7fq78cr7k") (f (quote (("quoted-env" "shell-words") ("default" "better-path") ("better-path" "which"))))))

