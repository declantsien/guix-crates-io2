(define-module (crates-io ed it edita-core) #:use-module (crates-io))

(define-public crate-edita-core-0.1.0 (c (n "edita-core") (v "0.1.0") (h "02cr4r2iqnsqqzn1il0k4avipclzbp8xcalc25fbjfzy5klg3m7d")))

(define-public crate-edita-core-0.2.0 (c (n "edita-core") (v "0.2.0") (h "0ah9y1y9awqhzjbvl5l79npbhfaw6x5c5llj410c28q1i6w345yk")))

(define-public crate-edita-core-0.2.1 (c (n "edita-core") (v "0.2.1") (h "1n8iavdp7wlaw6i2380hgg837596j27g396cbcqcnbcszpdzp9m6")))

