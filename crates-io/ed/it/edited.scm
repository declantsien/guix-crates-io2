(define-module (crates-io ed it edited) #:use-module (crates-io))

(define-public crate-edited-0.0.0 (c (n "edited") (v "0.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "11nzcagq1fl68y01h6j6j200i70ng8wr7rcrym1982gzyi7klihf")))

