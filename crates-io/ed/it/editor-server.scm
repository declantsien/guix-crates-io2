(define-module (crates-io ed it editor-server) #:use-module (crates-io))

(define-public crate-editor-server-0.0.1 (c (n "editor-server") (v "0.0.1") (d (list (d (n "clap") (r "^3.0.0-beta.2") (f (quote ("regex"))) (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "warp") (r "^0.3.1") (d #t) (k 0)))) (h "12p0bijizckh9psfcnm3alchghp11x2595kihcgmh5l2ylxipqg5")))

