(define-module (crates-io ed it editorjs-to-html-converter) #:use-module (crates-io))

(define-public crate-editorjs-to-html-converter-0.1.0 (c (n "editorjs-to-html-converter") (v "0.1.0") (d (list (d (n "html_parser") (r "^0.6.3") (d #t) (k 0)) (d (n "regex") (r "1.*") (d #t) (k 0)) (d (n "serde") (r "^1.0.147") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 0)) (d (n "typetag") (r "0.*") (d #t) (k 0)))) (h "19pmd2f80ick54q5dijrx8z23clifr6imfgayafg9qr7l584kcbi")))

