(define-module (crates-io ed it edit-chunks) #:use-module (crates-io))

(define-public crate-edit-chunks-0.1.0 (c (n "edit-chunks") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.1") (d #t) (k 0)) (d (n "structopt-derive") (r "^0.1") (d #t) (k 0)))) (h "19l0j1ykdsxj7kz7s7cg1qcyn5ghvvw4bzs7sw0agh37g4rfy2as")))

(define-public crate-edit-chunks-0.1.1 (c (n "edit-chunks") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.14") (d #t) (k 0)))) (h "1a25qnc6yjpa1cffpsin2zx56g94msp79h5c133w48y19axnlfxq")))

