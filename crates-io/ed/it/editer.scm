(define-module (crates-io ed it editer) #:use-module (crates-io))

(define-public crate-editer-0.1.0 (c (n "editer") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (k 0)) (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 2)) (d (n "tinyvec") (r "^1.6") (f (quote ("rustc_1_55"))) (o #t) (d #t) (k 0)) (d (n "tinyvec") (r "^1.6") (f (quote ("alloc"))) (d #t) (k 2)))) (h "1xcrw5r391lq9lgsacymxqc9bmxsnrmafqi8nalrw60pr79zwnqw") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t) (r "1.57")))

(define-public crate-editer-0.1.1 (c (n "editer") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (k 0)) (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 2)) (d (n "tinyvec") (r "^1.6") (f (quote ("rustc_1_55"))) (o #t) (d #t) (k 0)) (d (n "tinyvec") (r "^1.6") (f (quote ("alloc"))) (d #t) (k 2)))) (h "1v2gciyzxh6y3gdkmf4vpa6mv32dmpbhjg5nl8wj8mkh901yx406") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t) (r "1.57")))

(define-public crate-editer-0.2.0 (c (n "editer") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.7") (o #t) (k 0)) (d (n "arrayvec") (r "^0.7") (d #t) (k 2)) (d (n "smallvec") (r "^1.10") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.10") (d #t) (k 2)) (d (n "tinyvec") (r "^1.6") (f (quote ("rustc_1_55"))) (o #t) (d #t) (k 0)) (d (n "tinyvec") (r "^1.6") (f (quote ("alloc"))) (d #t) (k 2)))) (h "04aqjixdvja2gpvrn5cqsf62sb5v8ax5qwcpsry6m0m3zm3q1g05") (f (quote (("std" "alloc") ("default" "std") ("alloc")))) (y #t) (r "1.57")))

(define-public crate-editer-0.3.0 (c (n "editer") (v "0.3.0") (h "0xw41sh7vldl2fjxbyvnm59zz2mdgbzqmf8kz5sgpg4h7s7nffw7")))

