(define-module (crates-io ed it editdistancek) #:use-module (crates-io))

(define-public crate-editdistancek-0.1.0 (c (n "editdistancek") (v "0.1.0") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0v8fx30111i6g006xwc09jn4x5qmlrq0mym3h3a6dmzb50xq0gp2") (r "1.62")))

(define-public crate-editdistancek-0.1.1 (c (n "editdistancek") (v "0.1.1") (d (list (d (n "levenshtein") (r "^1.0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)))) (h "0076y539zbxp6b666hnlpp99s0s7gxb9fakzd1zfasxgh31pr5rh") (r "1.62")))

(define-public crate-editdistancek-1.0.0 (c (n "editdistancek") (v "1.0.0") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "levenshtein") (r "^1.0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "triple_accel") (r "^0.4.0") (d #t) (k 2)))) (h "0k6qq4a6myqfb6hkj06z86ng2mp0s8sd9g9d85k200vkfpmi0m17") (r "1.62")))

(define-public crate-editdistancek-1.0.1 (c (n "editdistancek") (v "1.0.1") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "levenshtein") (r "^1.0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "triple_accel") (r "^0.4.0") (d #t) (k 2)))) (h "1ly53gy2zd3aqcckhnwfif0x9mydfx3qkgjbxl1bwgkh06f6p7gj") (r "1.62")))

(define-public crate-editdistancek-1.0.2 (c (n "editdistancek") (v "1.0.2") (d (list (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "levenshtein") (r "^1.0.5") (d #t) (k 2)) (d (n "quickcheck") (r "^1.0.3") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "triple_accel") (r "^0.4.0") (d #t) (k 2)))) (h "04r6lfq9sfz3wqhqm6fzfcqbj8w16y8bh0x6kzkgkimislixy0iy") (r "1.62")))

