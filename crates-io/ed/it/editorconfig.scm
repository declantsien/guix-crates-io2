(define-module (crates-io ed it editorconfig) #:use-module (crates-io))

(define-public crate-editorconfig-0.1.0 (c (n "editorconfig") (v "0.1.0") (d (list (d (n "rust-ini") (r "^0.10.0") (d #t) (k 0)))) (h "1vqwdxjb4gkm2j8dn9q181qrs4ax7dskj2m3jkxxmpragr08pg9s")))

(define-public crate-editorconfig-0.2.0 (c (n "editorconfig") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "ordermap") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0mw90nib3aan0j6lqmd62mrfdsvfpr042ywx251gpvdiza467r6p")))

(define-public crate-editorconfig-1.0.0 (c (n "editorconfig") (v "1.0.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "ordermap") (r "^0.2.10") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "0wibszywwmvbs0cspgx40j2vi24awkpqhakyngac2r3rr8igm3ag")))

