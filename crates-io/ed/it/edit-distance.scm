(define-module (crates-io ed it edit-distance) #:use-module (crates-io))

(define-public crate-edit-distance-1.0.0 (c (n "edit-distance") (v "1.0.0") (d (list (d (n "quickcheck") (r "*") (d #t) (k 2)))) (h "07d6h3nb8kixw01kji98wmmwpz1cm1jcrb6bz7yk54f00q9acl6d")))

(define-public crate-edit-distance-2.0.0 (c (n "edit-distance") (v "2.0.0") (d (list (d (n "quickcheck") (r "^0.4.1") (d #t) (k 2)))) (h "1jfrp432r9vvb4qvd98jsg30gpfwlzriqq8qwhnmh4xw9whgad3a")))

(define-public crate-edit-distance-2.0.1 (c (n "edit-distance") (v "2.0.1") (d (list (d (n "quickcheck") (r "^0.6") (d #t) (k 2)))) (h "0zkv55ni5npzlksvcswlyslrky8r35qs3qd4jybzh8frqdw6iliv")))

(define-public crate-edit-distance-2.1.0 (c (n "edit-distance") (v "2.1.0") (d (list (d (n "quickcheck") (r "^0") (d #t) (k 2)))) (h "0yq3wlmd7ly22qxhfysi77qp31yvpx2ll9waa75bkpiih7rsmfmv")))

