(define-module (crates-io ed fs edfsm) #:use-module (crates-io))

(define-public crate-edfsm-0.5.0 (c (n "edfsm") (v "0.5.0") (d (list (d (n "event-driven-macros") (r "^0.5.0") (d #t) (k 0)))) (h "1vqp0qjdavnyp45mmzivwfdj8652rghy9al16c46s627irpiwmm8") (y #t)))

(define-public crate-edfsm-0.5.1 (c (n "edfsm") (v "0.5.1") (d (list (d (n "event-driven-macros") (r "^0.5.0") (d #t) (k 0)))) (h "0fr2rh0khjxgnnijd6xnbqpfvvvpw1dajbzrjzpfxcgv6y03d18d")))

(define-public crate-edfsm-0.6.0 (c (n "edfsm") (v "0.6.0") (d (list (d (n "event-driven-macros") (r "^0.6.0") (d #t) (k 0)))) (h "0br1l5621pxamd2isgv6lfsg3kbd723c9fqnv0ghr998sia6bvqc")))

(define-public crate-edfsm-0.7.0 (c (n "edfsm") (v "0.7.0") (d (list (d (n "event-driven-macros") (r "^0.7.0") (d #t) (k 0)))) (h "1na1fx7ala3r2143xw4nam9ycyvapmn237kny2fpd2rw73w6232c")))

