(define-module (crates-io ed n- edn-rs) #:use-module (crates-io))

(define-public crate-edn-rs-0.1.0 (c (n "edn-rs") (v "0.1.0") (h "1zrzcmy2mk000hs4v87qi1il3mima4a4f70vyxxj52dx53b29rni")))

(define-public crate-edn-rs-0.1.1 (c (n "edn-rs") (v "0.1.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1bfm6ih8b1lgjxl1hxabc6ilrjg7a685i0if4hrz5g9396di1rv6")))

(define-public crate-edn-rs-0.2.0 (c (n "edn-rs") (v "0.2.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1474qg4xx4zz0bgfjwb1886i1v1qh8h8h9xchvjlvlrd4c6nzg57")))

(define-public crate-edn-rs-0.2.1 (c (n "edn-rs") (v "0.2.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1l27kzk8gdfa5z1pns7sgm41x9x2yjp7anb1h5xqbmb865r6zg8y")))

(define-public crate-edn-rs-0.2.2 (c (n "edn-rs") (v "0.2.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0gi0s0f6i42a540rc23cf193ygi3vis8l9djp6b1841sl7svi9h3")))

(define-public crate-edn-rs-0.3.0 (c (n "edn-rs") (v "0.3.0") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "103fbg7s2rpag23031yi5wd5dxp5v2x9k82gz7n6an3kswiiik7j")))

(define-public crate-edn-rs-0.3.1 (c (n "edn-rs") (v "0.3.1") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1pqg3m797f54pd2f3lb2dqy0c84rdvdj6wqpazw2f2k99gsyk4b4")))

(define-public crate-edn-rs-0.3.2 (c (n "edn-rs") (v "0.3.2") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12nq50cldw5izrgcjyi4vw5z6bi859lqzzsp9pa3v0s40ia53sma")))

(define-public crate-edn-rs-0.3.3 (c (n "edn-rs") (v "0.3.3") (d (list (d (n "itertools") (r "^0.8") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wcq8c3z9xg8rd9g0f6zrhykdgli3pn25nc6dcnkbr8gc5xhm2jq")))

(define-public crate-edn-rs-0.3.4 (c (n "edn-rs") (v "0.3.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0xld1cnqa3h3bfwzf8z4lkpd715azv60dh5ghjj03ynsi7c0vp3w")))

(define-public crate-edn-rs-0.4.0 (c (n "edn-rs") (v "0.4.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0m8mlli07y1j4250rd6rcdm8s2bzmb72w9cin8cavh4yjsppk4vm")))

(define-public crate-edn-rs-0.4.1 (c (n "edn-rs") (v "0.4.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1nzqa4s0xwhw0225zi0p6sg7l8wli9q5p8ji1n7xv9pk16pvsxjc")))

(define-public crate-edn-rs-0.4.2 (c (n "edn-rs") (v "0.4.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p6jawvxpc0hp1ba34n3y0zqcpdlib3ma9xqwkxxly7q9qrwdq20")))

(define-public crate-edn-rs-0.4.3 (c (n "edn-rs") (v "0.4.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "157gs2xmdl9gr91siqzvbqi6ialawgg2irb1sadwym74r6hy1fva")))

(define-public crate-edn-rs-0.4.4 (c (n "edn-rs") (v "0.4.4") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ywiq97h20d56xaqafbppjxxrnc38isysx169y31ps7cyrxxbliz")))

(define-public crate-edn-rs-0.4.5 (c (n "edn-rs") (v "0.4.5") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0da9i25azgc15laigclxkznl7npz716vh4rw4cnlhy453n2m4hfw")))

(define-public crate-edn-rs-0.5.0 (c (n "edn-rs") (v "0.5.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vkbv9azc6xs7lcxld2dmqb4jq8j2fqs1120lya66a1hjyx67025")))

(define-public crate-edn-rs-0.5.1 (c (n "edn-rs") (v "0.5.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "07j8x7rzw1hpvnb6xlhdirhc1jrzw73y7kpf63sx866mgjdkywbn")))

(define-public crate-edn-rs-0.5.2 (c (n "edn-rs") (v "0.5.2") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06wl7xibmqq9r0sy9fzl2bgck5dmwvwqsnw93fyks99r92y7lmdn")))

(define-public crate-edn-rs-0.5.3 (c (n "edn-rs") (v "0.5.3") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0z3ccpwwz1ljcgmwjaj3fbapsvpmqla9fymxadkjj57if316q87f")))

(define-public crate-edn-rs-0.6.0 (c (n "edn-rs") (v "0.6.0") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1ycfglm9372p3j86zsivq5v5pjca0f3p5i0ncdqmpjrhr19mhn4i")))

(define-public crate-edn-rs-0.6.1 (c (n "edn-rs") (v "0.6.1") (d (list (d (n "regex") (r "^1") (d #t) (k 0)))) (h "13p5vjx31jkph1fq88xi8bdzx7is7xsv49ibnvy5h53zvywsb31y")))

(define-public crate-edn-rs-0.6.2 (c (n "edn-rs") (v "0.6.2") (d (list (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1p7mx0z351bpn9g2k7dx35c6w7m9rra1shm8sbzdxzqvrdbwrd5j")))

(define-public crate-edn-rs-0.7.0 (c (n "edn-rs") (v "0.7.0") (d (list (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "04j7gshcmgnr005mnxss269h0l3s60cf0dzbhyi7200k9nfzb11x")))

(define-public crate-edn-rs-0.8.0 (c (n "edn-rs") (v "0.8.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "11izdlykzjy1bfyfr096ak5c8kp2sl6nvd899yqx2ggy80al8a6s")))

(define-public crate-edn-rs-0.8.1 (c (n "edn-rs") (v "0.8.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0jnhlh6b4vvr7rna2kpmzls49hsk6gvbbh25r0jnv8mcnqm9zjyv")))

(define-public crate-edn-rs-0.8.2 (c (n "edn-rs") (v "0.8.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1yaisa6kxq53xsr02rk3k1sm6g4zxmmplf5h0m38ixlpkjaa46fc")))

(define-public crate-edn-rs-0.9.0 (c (n "edn-rs") (v "0.9.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1qz4jcrxb2d9cdgrwzwvpd3kwn1dv418jhbk8636vr800y1d6sbw")))

(define-public crate-edn-rs-0.9.1 (c (n "edn-rs") (v "0.9.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1s7l1ay5sn89pawyh0ysxffs0kzpqyrg55lhrqvj0z7s3ldpfd6c")))

(define-public crate-edn-rs-0.9.2 (c (n "edn-rs") (v "0.9.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1anr3j6aibsw9g669c1852z9pxn7jrysvm3jy8hg078w5lmh2f94")))

(define-public crate-edn-rs-0.9.3 (c (n "edn-rs") (v "0.9.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1q8j4v2k2rh40279sxl0381p2wzb4wkl46bdp090f56i6ghyx10x")))

(define-public crate-edn-rs-0.9.4 (c (n "edn-rs") (v "0.9.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "17d4sgzi1yasnyp24d9lnx7v9k8yb27k3mkgix4xnvv00z5jbk2m")))

(define-public crate-edn-rs-0.10.0 (c (n "edn-rs") (v "0.10.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1jlimymjffcyh8nh0fd4v7idgibzid6fhvsr05p7zd0l03xlv0v3")))

(define-public crate-edn-rs-0.10.1 (c (n "edn-rs") (v "0.10.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "ordered-float") (r "^1.1") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1lqdy8p05q462qd13djsc4xzjrrns313srp7cy62vn9i7bl7jpfq")))

(define-public crate-edn-rs-0.10.2 (c (n "edn-rs") (v "0.10.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "12fzbajln0ivqidfn0jvpyfmrx0l8wzscxr34kgdj0278nzlgvm5")))

(define-public crate-edn-rs-0.10.3 (c (n "edn-rs") (v "0.10.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "09f42dwwb0pkbqxnmcdzjr30chypzy79zb20bx7c38dp6apg44sd")))

(define-public crate-edn-rs-0.10.4 (c (n "edn-rs") (v "0.10.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1wa9lqa8sjjp1r56pssrnbyhvkg5j7d5rklcmjsaazadgjzr85nb")))

(define-public crate-edn-rs-0.10.5 (c (n "edn-rs") (v "0.10.5") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "0vp3jnfj2x731vp3r5a4m626zm8vwiqm6y66pb7y61mzb277hnsl")))

(define-public crate-edn-rs-0.11.0 (c (n "edn-rs") (v "0.11.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0yddrsf52is9a2nsdsh33dqv2wprl84gmpaf7jsk2apphq9jki5h") (f (quote (("async" "futures"))))))

(define-public crate-edn-rs-0.11.1 (c (n "edn-rs") (v "0.11.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1y0575k76wwsdir8jzgsxm03yz5wp5bnlfl3wv9sckla5lp104vw") (f (quote (("async" "futures"))))))

(define-public crate-edn-rs-0.11.2 (c (n "edn-rs") (v "0.11.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0kdgyds59jsxl9vara611x8vif144m4ifrwqxjv3fn901h0w1wrz") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.11.3 (c (n "edn-rs") (v "0.11.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0rvzpxj5a60s53jiincidc9dbgmasz6cvcd6sykp3bn3c4sd62ga") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.12.0 (c (n "edn-rs") (v "0.12.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1y1hl87yqbzyjfxvamn56zm9qida1j1zlds8qqz2zplwcbmx8s2r") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.12.1 (c (n "edn-rs") (v "0.12.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0jd7znz2a4j3nnxwymmxq9sn180v9nnlp1pji18cb98p421kiirv") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.13.0 (c (n "edn-rs") (v "0.13.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1iqd4ln634fm9v56h29h3rd8riyiiy4q0k4jnd2pzg4mn1233vwm") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.13.1 (c (n "edn-rs") (v "0.13.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1m6vp8skvq7v6dalrc7x4j5spnca6xv77ld9ffnpb6pabjbgp3gj") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.13.2 (c (n "edn-rs") (v "0.13.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0sc1z045q2qpj15n4kyxl5w2bii99jyj6j2cqz9xwyv3kk7ap7l2") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.13.3 (c (n "edn-rs") (v "0.13.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "16aqg6vgwx2djdp313l7bj35xg0157manf5782z95i2ag049ycz5") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.13.4 (c (n "edn-rs") (v "0.13.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "19l9arlrl3fakf97y7hmq4z6g9dv2wl1fgj874n35g72849qdaay") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.13.5 (c (n "edn-rs") (v "0.13.5") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "17idhjgnqfz4vqynh3ni1yr22c96xddqfmnvpijlbxzhdll5ykqz") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.14.0 (c (n "edn-rs") (v "0.14.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "04bnrv109i8rljz0f288xq9gyw85zzajsa67m0sfcjpxjyh6i3kk") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.14.1 (c (n "edn-rs") (v "0.14.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0z4bkv7a231kyxf4i07p6v1zn44ld0cwb0gwm40vksg7b2iwy55c") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.14.2 (c (n "edn-rs") (v "0.14.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1yllabxcm7ljy7xps60scxd5hkaa3rgaa3zxhaplhw4nscgv5d50") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.15.0 (c (n "edn-rs") (v "0.15.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0ls3963gh7mp3wxb0lx7jdj3yn93asgya8ajkklrmawlxc93g505") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.15.1 (c (n "edn-rs") (v "0.15.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.3.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "08z2whhfmycwymxszhcba0rk2dv3rk4f6fwi0yci48bhnw82xqzn") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.0 (c (n "edn-rs") (v "0.16.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0q18y22slg7bls35qmc7y6xwcn1qls63l9gapngwfiwa5zv8hkhi") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.1 (c (n "edn-rs") (v "0.16.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0xjl44a8jkvryk3hszmk524ihx4qzngzzi1kssl89hwwyh2jin4d") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.2 (c (n "edn-rs") (v "0.16.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1m23ym740mg1jdpib78s81y6lpwgrg5qja3i7zxah45x26rzj4dh") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.3 (c (n "edn-rs") (v "0.16.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "08y0xnf3l0mn81dkbslakzcjxrnsm72ljcrr1m8q7i5v9v0g7r0y") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.4 (c (n "edn-rs") (v "0.16.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "17sn9ym71y9nfysnb93nvjyqvk7d6yp0xp99bj48c8lzc3j982sh") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.5 (c (n "edn-rs") (v "0.16.5") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1j9vj6sqh1pragn3a4phmfm05wzyvx7b24815mjkdbk02q8ra3y5") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.6 (c (n "edn-rs") (v "0.16.6") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.4.2") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0g38rlk7xxbzfas077ki81x9jbgp2w5g30350ciwbb3n4wpfvglm") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.7 (c (n "edn-rs") (v "0.16.7") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1v73xcnlvdpvjrchjgqav3nb7swqvdihmg5wb4azrylkdvv001ch") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.9 (c (n "edn-rs") (v "0.16.9") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1bbjxw59wlyym1rk25rwr0dv32472bsrv3ljv77ks3k4v4vqp3p0") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.10 (c (n "edn-rs") (v "0.16.10") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0jym756j056ampw1pw60d5zcnlawczr1jl0z01zmaqlflh3j8vj4") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.11 (c (n "edn-rs") (v "0.16.11") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0d08z94lp6b3dsqaqq2hiqg2zp4vq68lfnhnlkc73dr408vhppp3") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.12 (c (n "edn-rs") (v "0.16.12") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0l0ymklsxq26syj0igxi83jr5bvllcsy97dfi0i6y6bjdq3xhc7l") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.13 (c (n "edn-rs") (v "0.16.13") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1gaq0dgnzc9qya2yvfifxrm0vimic2mgdks69djyn2mi4i7mj90w") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.16.14 (c (n "edn-rs") (v "0.16.14") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "09vsav561h2s8jc3b5hxmcrfqx5cw4jvnlvivs93kbhii4sm1lxw") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.17.0 (c (n "edn-rs") (v "0.17.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "1y7avcd4v6f6gj56z27fgbns0z9dna134zm2kc3zr48bdbyf64w1") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.17.1 (c (n "edn-rs") (v "0.17.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0w306c7aqc51i99pmajaqsn2l7vg208nga9vx78gnl5z50yn8i38") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.17.2 (c (n "edn-rs") (v "0.17.2") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "150fwpxh2w22b76ixzm28l03k2hgrd0an6kwcwxq1184x9fbg3x3") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.17.3 (c (n "edn-rs") (v "0.17.3") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "0viyycb08fc12nv2vqc2rgs3v6fvndqkkp5j7ak68js5a7j3l28w") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.17.4 (c (n "edn-rs") (v "0.17.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "futures") (r "^0.3.5") (o #t) (d #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("macros"))) (d #t) (k 2)))) (h "030svza2hgl35jg08bnd2mdp6xq4bkb69m0aacriamgvl2160bhx") (f (quote (("json" "regex") ("async" "futures"))))))

(define-public crate-edn-rs-0.17.5 (c (n "edn-rs") (v "0.17.5") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "edn-derive") (r "^0.5.0") (d #t) (k 2)) (d (n "ordered-float") (r "^4.1") (o #t) (k 0)) (d (n "regex") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "tokio") (r "^1.33") (f (quote ("full"))) (d #t) (k 2)))) (h "1g8dhrvvk7jza8gpz7fgj31k5r7p3zaii0mzn9xga5ki751jziwb") (f (quote (("sets" "ordered-float") ("json" "regex") ("default" "sets"))))))

