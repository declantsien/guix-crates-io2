(define-module (crates-io ed c- edc-connector-client) #:use-module (crates-io))

(define-public crate-edc-connector-client-0.1.0 (c (n "edc-connector-client") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^3.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "full"))) (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4" "v4"))) (d #t) (k 2)))) (h "03pmaawqac1g1vb25fs2rvfq1d752cwhvd5hrjydx5cmlqk85053")))

(define-public crate-edc-connector-client-0.1.1 (c (n "edc-connector-client") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.12.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "serde_with") (r "^3.7.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full" "full"))) (d #t) (k 2)) (d (n "uuid") (r "^1") (f (quote ("v4" "v4"))) (d #t) (k 2)))) (h "1kzk6wj8ac7zm41djw891cjgpyj84295w9i7g387j64pwwv1vm3f")))

