(define-module (crates-io ed #{2k}# ed2k) #:use-module (crates-io))

(define-public crate-ed2k-1.0.0 (c (n "ed2k") (v "1.0.0") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "md4") (r "^0.10") (d #t) (k 0)))) (h "1hz54fj7kfcxx0q88xjgjhf59sa78x5pcyz044dkvcckjmz4qwcz")))

(define-public crate-ed2k-1.0.1 (c (n "ed2k") (v "1.0.1") (d (list (d (n "digest") (r "^0.10") (d #t) (k 0)) (d (n "md4") (r "^0.10") (d #t) (k 0)))) (h "0y53x3ivy9wdi32zdjn6w9kqz67s9pga51kab4q653m4nk2dz57r")))

