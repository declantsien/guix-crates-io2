(define-module (crates-io ed ge edgeware-primitives) #:use-module (crates-io))

(define-public crate-edgeware-primitives-3.1.0 (c (n "edgeware-primitives") (v "3.1.0") (d (list (d (n "codec") (r "^1.3.0") (f (quote ("derive"))) (k 0) (p "parity-scale-codec")) (d (n "frame-system") (r "^2.0.0") (k 0)) (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "sp-application-crypto") (r "^2.0.0") (k 0)) (d (n "sp-core") (r "^2.0.0") (k 0)) (d (n "sp-runtime") (r "^2.0.0") (k 0)) (d (n "sp-serializer") (r "^2.0.0") (d #t) (k 2)))) (h "15a3zscnbmgvqaaa005sa8mbnq71iq7b1m2dz4shr1dhj0n3bq2v") (f (quote (("std" "codec/std" "frame-system/std" "sp-application-crypto/std" "sp-core/std" "sp-runtime/std") ("default" "std"))))))

