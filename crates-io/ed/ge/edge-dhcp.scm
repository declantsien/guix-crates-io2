(define-module (crates-io ed ge edge-dhcp) #:use-module (crates-io))

(define-public crate-edge-dhcp-0.0.0 (c (n "edge-dhcp") (v "0.0.0") (h "0xhhrqw83fhmmwdwxdvnwlw44xil1pr6fbhh4hyxvyijckmx5vl8") (r "1.71")))

(define-public crate-edge-dhcp-0.1.0 (c (n "edge-dhcp") (v "0.1.0") (d (list (d (n "edge-raw") (r "^0.1.0") (k 0)) (d (n "embassy-futures") (r "^0.1") (o #t) (k 0)) (d (n "embassy-time") (r "^0.3") (k 0)) (d (n "embedded-nal-async") (r "^0.7") (o #t) (k 0)) (d (n "embedded-nal-async-xtra") (r "^0.1.0") (o #t) (k 0)) (d (n "heapless") (r "^0.8") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "no-std-net") (r "^0.6") (k 0)) (d (n "num_enum") (r "^0.7") (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "0cyw0g3cm3r4fz8p0bws6swwarcdjjsz9cm05jml4a9n9ww0hn3g") (f (quote (("std" "io") ("io" "embassy-futures" "embedded-nal-async" "embedded-nal-async-xtra") ("default" "io")))) (r "1.75")))

(define-public crate-edge-dhcp-0.2.0 (c (n "edge-dhcp") (v "0.2.0") (d (list (d (n "edge-raw") (r "^0.2.0") (k 0)) (d (n "embassy-futures") (r "^0.1") (o #t) (k 0)) (d (n "embassy-time") (r "^0.3") (k 0)) (d (n "embedded-nal-async") (r "^0.7") (o #t) (k 0)) (d (n "embedded-nal-async-xtra") (r "^0.2.0") (o #t) (k 0)) (d (n "heapless") (r "^0.8") (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "no-std-net") (r "^0.6") (k 0)) (d (n "num_enum") (r "^0.7") (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)))) (h "1v35gq4qdbncvygmjbvm072lk7wqcwfy99rr5m7zmnn8ypfvg6r1") (f (quote (("std" "io") ("io" "embassy-futures" "embedded-nal-async" "embedded-nal-async-xtra") ("default" "io")))) (r "1.75")))

