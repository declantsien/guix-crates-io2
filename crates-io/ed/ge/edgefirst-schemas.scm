(define-module (crates-io ed ge edgefirst-schemas) #:use-module (crates-io))

(define-public crate-edgefirst-schemas-1.0.1 (c (n "edgefirst-schemas") (v "1.0.1") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "0243q305iqw15i06sm6vqsimi7r3f4pc6mkc0j0mfsrpfn6lips1")))

(define-public crate-edgefirst-schemas-1.0.2 (c (n "edgefirst-schemas") (v "1.0.2") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "0frrii5b52x75w3kgf6sm2cvr55848c45gk5mdbh5k3ikyxx8c1w")))

(define-public crate-edgefirst-schemas-1.0.3 (c (n "edgefirst-schemas") (v "1.0.3") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "00w4pmxa18bqfwfb9h6912qgnv11dmk8wsm0bqnksrafgzpnx6jh")))

(define-public crate-edgefirst-schemas-1.0.4 (c (n "edgefirst-schemas") (v "1.0.4") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "116x41gxwivdah651m6b5pd2xh85qqfm74zmnfrbfhddmy0z277d")))

(define-public crate-edgefirst-schemas-1.1.0 (c (n "edgefirst-schemas") (v "1.1.0") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "1p9gdfirrpxq52k9p7ylws5v67mabais3l8fpniah6zcdv3ivz8a")))

(define-public crate-edgefirst-schemas-1.1.1 (c (n "edgefirst-schemas") (v "1.1.1") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "1jxivklp4zd8wpf9n0pnhgnk3iaj2db52vkdgirm75fw30gkp8rj")))

(define-public crate-edgefirst-schemas-1.2.0 (c (n "edgefirst-schemas") (v "1.2.0") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "0f8g0afma4izapwrw2sxp2x6x4w6lz28z65y59bbi2d58iqrw02p")))

(define-public crate-edgefirst-schemas-1.2.1 (c (n "edgefirst-schemas") (v "1.2.1") (d (list (d (n "serde") (r "^1.0.147") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.147") (d #t) (k 0)))) (h "0h7jgfhsvnanjhlkc02sgpk0v6ra4fmdxqnc2j565kf04h30vk9m")))

