(define-module (crates-io ed ge edge-raw) #:use-module (crates-io))

(define-public crate-edge-raw-0.1.0 (c (n "edge-raw") (v "0.1.0") (d (list (d (n "embedded-io-async") (r "^0.6") (o #t) (k 0)) (d (n "embedded-nal-async") (r "^0.7") (o #t) (k 0)) (d (n "embedded-nal-async-xtra") (r "^0.1.0") (o #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "no-std-net") (r "^0.6") (k 0)))) (h "17xpp0lb9yfflvlmhbm2r1iifr8mm3l8dg91wlj7hlk3b7zslcx1") (f (quote (("std" "io") ("io" "embedded-io-async" "embedded-nal-async" "embedded-nal-async-xtra") ("default" "io")))) (r "1.75")))

(define-public crate-edge-raw-0.2.0 (c (n "edge-raw") (v "0.2.0") (d (list (d (n "embedded-io-async") (r "^0.6") (o #t) (k 0)) (d (n "embedded-nal-async") (r "^0.7") (o #t) (k 0)) (d (n "embedded-nal-async-xtra") (r "^0.2.0") (o #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "no-std-net") (r "^0.6") (k 0)))) (h "0n8vz15zghw6w9dnfgxwiq5bqgs7r0b9nilr7irjkw52xi7gxc3c") (f (quote (("std" "io") ("io" "embedded-io-async" "embedded-nal-async" "embedded-nal-async-xtra") ("default" "io")))) (r "1.75")))

