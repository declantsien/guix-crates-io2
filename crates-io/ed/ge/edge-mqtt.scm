(define-module (crates-io ed ge edge-mqtt) #:use-module (crates-io))

(define-public crate-edge-mqtt-0.0.0 (c (n "edge-mqtt") (v "0.0.0") (h "0jqfagw7h08489ch346yj016jwv2mf0h3wqrc4zadfvqjh76rrgb") (r "1.71")))

(define-public crate-edge-mqtt-0.1.0 (c (n "edge-mqtt") (v "0.1.0") (d (list (d (n "embedded-svc") (r "^0.27") (f (quote ("std"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "rumqttc") (r "^0.23") (d #t) (k 0)))) (h "1x7s7mww3vbshp95fg0bb2nsigmrwci0hzicvxsl3d49pbml04d1") (r "1.75")))

(define-public crate-edge-mqtt-0.2.0 (c (n "edge-mqtt") (v "0.2.0") (d (list (d (n "embedded-svc") (r "^0.27") (f (quote ("std"))) (o #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "rumqttc") (r "^0.23") (d #t) (k 0)))) (h "0aph250248lsdgqn4yfy8ydxclg748nm9whnz03rzxk71sisb5c4") (r "1.75")))

