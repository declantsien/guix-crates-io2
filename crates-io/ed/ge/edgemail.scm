(define-module (crates-io ed ge edgemail) #:use-module (crates-io))

(define-public crate-edgemail-0.2.1 (c (n "edgemail") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libsql-client") (r "^0.24.3") (f (quote ("local_backend" "reqwest_backend"))) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0hywabflsl3yaf741c03dhgdfw923pz5grsms2ghjpmn8qq1wa4g")))

(define-public crate-edgemail-0.2.2 (c (n "edgemail") (v "0.2.2") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libsql-client") (r "^0.24.3") (f (quote ("local_backend" "reqwest_backend"))) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "1159nw8szfhmlgxyl50zdspisq88xcp5h5hk7pw8ir68r5ifqkzi")))

(define-public crate-edgemail-0.2.3 (c (n "edgemail") (v "0.2.3") (d (list (d (n "anyhow") (r "^1.0.69") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (d #t) (k 0)) (d (n "libsql-client") (r "^0.24.3") (f (quote ("local_backend" "reqwest_backend"))) (k 0)) (d (n "tokio") (r "^1.25.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.3.16") (d #t) (k 0)))) (h "0p6i9gz5yj81i39zip0nlwi1bbb96w6gqlnics1lylpqmnxlhkrv")))

