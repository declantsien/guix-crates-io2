(define-module (crates-io ed ge edge) #:use-module (crates-io))

(define-public crate-edge-0.0.1 (c (n "edge") (v "0.0.1") (d (list (d (n "handlebars") (r "^0.16.0") (f (quote ("serde_type"))) (k 0)) (d (n "hyper") (r "^0.8.0") (f (quote ("cookie"))) (k 0)) (d (n "mime") (r "^0.2.0") (d #t) (k 0)) (d (n "serde") (r "^0.7.0") (d #t) (k 0)) (d (n "serde_json") (r "^0.7.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)) (d (n "url") (r "^0.5.7") (d #t) (k 0)))) (h "1qbgpcbblab6h80vyim9w1qygf89n231as996fz02f34v4nqdjp8")))

