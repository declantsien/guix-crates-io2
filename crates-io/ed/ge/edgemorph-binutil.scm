(define-module (crates-io ed ge edgemorph-binutil) #:use-module (crates-io))

(define-public crate-edgemorph-binutil-0.1.0-alpha (c (n "edgemorph-binutil") (v "0.1.0-alpha") (d (list (d (n "binrw") (r "^0.8.0") (d #t) (k 0)))) (h "1q0wpm6a235im7580j534w77xcna4m1x0qzw76gxw3axw3dffsvc") (f (quote (("tokio") ("std") ("pyo3") ("no_std") ("default") ("async_std"))))))

