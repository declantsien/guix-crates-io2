(define-module (crates-io ed ge edges) #:use-module (crates-io))

(define-public crate-edges-0.1.0 (c (n "edges") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.0") (o #t) (d #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0fv6hqkfwzi2y9cpgn1ys1l9bz0z506rjg71c4ncr109yy72447c") (f (quote (("default")))) (s 2) (e (quote (("bevy" "dep:bevy"))))))

(define-public crate-edges-0.2.0 (c (n "edges") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_render"))) (o #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0md28dqavza1744mikqmhhp1x1y1g41nbhb7ww8249sx8agsa169") (f (quote (("default")))) (s 2) (e (quote (("bevy" "dep:bevy"))))))

(define-public crate-edges-0.3.0 (c (n "edges") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_render"))) (o #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0mb0p4738zq7phrirdfzl5lzh8fn1pwpk1cfz49c07wgqji68ybn") (f (quote (("default")))) (s 2) (e (quote (("bevy" "dep:bevy"))))))

(define-public crate-edges-0.3.1 (c (n "edges") (v "0.3.1") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_render"))) (o #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0vk04aqsd555vxlqawj92dmh4d1jhvdsd7c5pmmd1rjjm0a78xl1") (f (quote (("default")))) (s 2) (e (quote (("bevy" "dep:bevy"))))))

(define-public crate-edges-0.3.2 (c (n "edges") (v "0.3.2") (d (list (d (n "bevy") (r "^0.13.0") (f (quote ("bevy_render"))) (o #t) (k 0)) (d (n "glam") (r "^0.25.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.14.5") (d #t) (k 0)) (d (n "image") (r "^0.24.9") (d #t) (k 0)) (d (n "mashmap") (r "^0.1.1") (d #t) (k 0)) (d (n "open") (r "^5.1.2") (d #t) (k 2)) (d (n "ordered-float") (r "^4.2.0") (d #t) (k 0)) (d (n "raqote") (r "^0.8.4") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1p5an3mrqv601rvz0nr771wb36lzbc2c47nrjmliliq8ay8aw5c4") (f (quote (("default")))) (s 2) (e (quote (("bevy" "dep:bevy"))))))

