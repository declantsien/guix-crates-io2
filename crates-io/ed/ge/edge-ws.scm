(define-module (crates-io ed ge edge-ws) #:use-module (crates-io))

(define-public crate-edge-ws-0.0.0 (c (n "edge-ws") (v "0.0.0") (h "02k4949cfg7z5a245g50aaik6gf4glbfn9vy7miw44lba753f8py") (r "1.71")))

(define-public crate-edge-ws-0.1.0 (c (n "edge-ws") (v "0.1.0") (d (list (d (n "embedded-io-async") (r "^0.6") (o #t) (k 0)) (d (n "embedded-svc") (r "^0.27") (o #t) (k 0)))) (h "099b1d0gfg1m18gqgcjvs9by3pj9ffw8vm0xsasy3fr7743c89y1") (f (quote (("std" "io") ("io" "embedded-io-async") ("default" "io")))) (r "1.75")))

(define-public crate-edge-ws-0.2.0 (c (n "edge-ws") (v "0.2.0") (d (list (d (n "embedded-io-async") (r "^0.6") (o #t) (k 0)) (d (n "embedded-svc") (r "^0.27") (o #t) (k 0)))) (h "1320n4blx8rgg7iffc24kg5y7d67r96cl1x4j10ih1b5vf21mvfy") (f (quote (("std" "io") ("io" "embedded-io-async") ("default" "io")))) (r "1.75")))

