(define-module (crates-io ed ge edge-util) #:use-module (crates-io))

(define-public crate-edge-util-0.0.1 (c (n "edge-util") (v "0.0.1") (d (list (d (n "edge-schema") (r "^0.0.1") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmparser") (r "^0.95") (d #t) (k 0)))) (h "0ffjbraz699j56wkgjyqg4n3z1fmqiz00pc1aypnyknkizfmqrjx")))

(define-public crate-edge-util-0.0.2 (c (n "edge-util") (v "0.0.2") (d (list (d (n "edge-schema") (r "^0.0.3") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmparser") (r "^0.121.0") (d #t) (k 0)))) (h "0f9gxfzbb8lf7wfy1cga8sm4cvzsa5kj9v82f3kr4kalscprrlj7")))

(define-public crate-edge-util-0.1.0 (c (n "edge-util") (v "0.1.0") (d (list (d (n "edge-schema") (r "^0.0.3") (d #t) (k 0)) (d (n "http") (r "^0.2.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.89") (d #t) (k 2)) (d (n "wasmparser") (r "^0.121.0") (d #t) (k 0)))) (h "0dvkrxjs9rhfvbz0rcgmgqajxlm6s7vpab5cm7h6jqv7ms06gl30")))

