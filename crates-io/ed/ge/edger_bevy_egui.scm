(define-module (crates-io ed ge edger_bevy_egui) #:use-module (crates-io))

(define-public crate-edger_bevy_egui-0.1.0 (c (n "edger_bevy_egui") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy_egui") (r "^0.25.0") (d #t) (k 0)))) (h "10fcbxqg9miwdwkizrmk4954wh05scrrfiqg1pjfmxf2rs17s3xl")))

(define-public crate-edger_bevy_egui-0.2.0 (c (n "edger_bevy_egui") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy_egui") (r "^0.25.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)))) (h "0sl0pdgn9wg6ach5nf74lbd83f49a87s5pd4prdhway9bpikjw20") (f (quote (("default" "bevy/bevy_text"))))))

