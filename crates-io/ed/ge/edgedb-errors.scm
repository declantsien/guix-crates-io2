(define-module (crates-io ed ge edgedb-errors) #:use-module (crates-io))

(define-public crate-edgedb-errors-0.0.0 (c (n "edgedb-errors") (v "0.0.0") (h "0xzfg568yxgwv596wpbxlkrmzhxjllgs8safqjx8nyp9aaip9wh9")))

(define-public crate-edgedb-errors-0.1.0 (c (n "edgedb-errors") (v "0.1.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "05mb90r69a46zz6shf3xnibbcsm0cn097x0qfb6mvv25s11vr9sc")))

(define-public crate-edgedb-errors-0.2.0 (c (n "edgedb-errors") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "0336lgfay5ji7656lass1i3jjqb6pqczvshc4fr751ms9psyfjmw")))

(define-public crate-edgedb-errors-0.2.1 (c (n "edgedb-errors") (v "0.2.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "100w7aplcb0hzq0r1yp2yzllaclgy3bnlryzssjcm5c3c8vlskzw")))

(define-public crate-edgedb-errors-0.3.0 (c (n "edgedb-errors") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)))) (h "0brpaj6bniv4zsr6pll4s0hknzz3rcpd7xcmy6m9xc2fh993mz1l")))

(define-public crate-edgedb-errors-0.4.0 (c (n "edgedb-errors") (v "0.4.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "miette") (r "^5.3.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 2)))) (h "1qjzxs50b97ahv5q8yr3aqlwy1ii6f9g2bbgn6m5ql0bh1dl0i90")))

(define-public crate-edgedb-errors-0.4.1 (c (n "edgedb-errors") (v "0.4.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "miette") (r "^5.3.0") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.0") (d #t) (k 2)))) (h "03linwl76gdj38qkjsza9c2rys5gcp0kjp62zbllqlfr5j2ccyzq")))

(define-public crate-edgedb-errors-0.4.2 (c (n "edgedb-errors") (v "0.4.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 0)) (d (n "miette") (r "^7.2.0") (o #t) (d #t) (k 0)))) (h "1xbd8m2bh3yhlvvjqsr35bqp6v80vd5hd348rlbxi76f8lc1m1ny") (r "1.72")))

