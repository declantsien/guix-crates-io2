(define-module (crates-io ed ge edger_bevy_util) #:use-module (crates-io))

(define-public crate-edger_bevy_util-0.1.0 (c (n "edger_bevy_util") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.80") (d #t) (k 0)) (d (n "bevy") (r "^0.13.0") (k 0)))) (h "0khj54g6ad0ww4b8r4xg0g9g0jjlr5j80kiaihxi0r72y7w8cdfm") (f (quote (("default" "bevy/bevy_render" "bevy/bevy_sprite" "bevy/bevy_asset" "bevy/bevy_text"))))))

