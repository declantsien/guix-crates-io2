(define-module (crates-io ed ge edge-captive) #:use-module (crates-io))

(define-public crate-edge-captive-0.0.0 (c (n "edge-captive") (v "0.0.0") (h "1nkvisjx8kzayl9j6z52dqh7wq7hna3v4s27ww7aiw5izs6cykdf") (r "1.71")))

(define-public crate-edge-captive-0.1.0 (c (n "edge-captive") (v "0.1.0") (d (list (d (n "domain") (r "^0.9.3") (f (quote ("heapless"))) (k 0)) (d (n "embedded-nal-async") (r "^0.7") (o #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "octseq") (r "^0.3.2") (k 0)))) (h "0csjpdnralmkma2fg4jz488qbvhnfacln72q1pvj1bflgi9lp1ik") (f (quote (("std" "io") ("io" "embedded-nal-async") ("default" "io")))) (r "1.75")))

(define-public crate-edge-captive-0.2.0 (c (n "edge-captive") (v "0.2.0") (d (list (d (n "domain") (r "^0.9.3") (f (quote ("heapless"))) (k 0)) (d (n "embedded-nal-async") (r "^0.7") (o #t) (k 0)) (d (n "log") (r "^0.4") (k 0)) (d (n "octseq") (r "^0.3.2") (k 0)))) (h "04a5n7jwsl0vxqqdhp0vv3bscbncp70q680sr4mpc93xwwfrwyv9") (f (quote (("std" "io") ("io" "embedded-nal-async") ("default" "io")))) (r "1.75")))

