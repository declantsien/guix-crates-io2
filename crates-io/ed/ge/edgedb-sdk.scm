(define-module (crates-io ed ge edgedb-sdk) #:use-module (crates-io))

(define-public crate-edgedb-sdk-0.0.0 (c (n "edgedb-sdk") (v "0.0.0") (h "1wh39wai3ndifhp910q0hwgm89jcdbndc8fbp8rmvykhjg63lrrz")))

(define-public crate-edgedb-sdk-0.1.0 (c (n "edgedb-sdk") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.52") (d #t) (k 0)) (d (n "bitflags") (r "^1.3.2") (d #t) (k 0)) (d (n "bytes") (r "^1.1.0") (d #t) (k 0)) (d (n "edgedb-errors") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "edgedb-protocol") (r "^0.3.0") (o #t) (d #t) (k 0)) (d (n "edgedb-sdk-macros") (r "^0.1.0") (d #t) (k 0)) (d (n "http") (r "^0.2.6") (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "uuid") (r "^0.8.1") (d #t) (k 0)))) (h "0bvssfxhjv7v6l01pnf6jq65nxbr2bprkgvaiya7mv7albjgm8b2") (f (quote (("host") ("default" "client") ("client" "edgedb-protocol" "edgedb-errors"))))))

