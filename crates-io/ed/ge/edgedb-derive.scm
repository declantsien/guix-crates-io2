(define-module (crates-io ed ge edgedb-derive) #:use-module (crates-io))

(define-public crate-edgedb-derive-0.1.0 (c (n "edgedb-derive") (v "0.1.0") (h "08c3a0q1pbxq0gvqg9yvhd52ci1nkpbqv6prgr8yrdrrbrln12f4")))

(define-public crate-edgedb-derive-0.2.0 (c (n "edgedb-derive") (v "0.2.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (d #t) (k 0)))) (h "1xwvgj2slfk33kyikzcz8jf0jn4zz0hdh86h4c9pbnv6d3fl710r")))

(define-public crate-edgedb-derive-0.3.0 (c (n "edgedb-derive") (v "0.3.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (d #t) (k 0)))) (h "0z4053m4s9virqh3hymi4p6vp4992f5syfbyppi1pry40h46n2h9")))

(define-public crate-edgedb-derive-0.4.0 (c (n "edgedb-derive") (v "0.4.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (d #t) (k 0)))) (h "1xc9xj761g6ifyjypxkiv7ql3kd159vqdnrknxgfabw5l9by241c")))

(define-public crate-edgedb-derive-0.5.0 (c (n "edgedb-derive") (v "0.5.0") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (d #t) (k 0)))) (h "0xg2mnvaap8iddsq6igx8dm3hgyi9rsd1bias1jqz41vw3wsjiv7")))

(define-public crate-edgedb-derive-0.5.1 (c (n "edgedb-derive") (v "0.5.1") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (d #t) (k 0)))) (h "1dpmlbckxyfqdyfnbj1axnxr1xxxpxk663na4i8s7kk83wgplqgl")))

(define-public crate-edgedb-derive-0.5.2 (c (n "edgedb-derive") (v "0.5.2") (d (list (d (n "bytes") (r "^1.0.1") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.19") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (d #t) (k 0)))) (h "1lijwqilpxhzyplj90k9ak48zs5cqvzm7br91jdz9rgn7jiz3k8s") (r "1.72")))

