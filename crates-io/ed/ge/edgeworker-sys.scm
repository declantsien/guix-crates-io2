(define-module (crates-io ed ge edgeworker-sys) #:use-module (crates-io))

(define-public crate-edgeworker-sys-0.0.1 (c (n "edgeworker-sys") (v "0.0.1") (d (list (d (n "cfg-if") (r "^0.1.2") (d #t) (k 0)) (d (n "js-sys") (r "^0.3.53") (d #t) (k 0)) (d (n "wasm-bindgen") (r "=0.2.76") (d #t) (k 0)) (d (n "web-sys") (r "^0.3.53") (f (quote ("ReadableStream" "RequestRedirect" "RequestInit" "ResponseInit" "FormData" "Blob"))) (d #t) (k 0)))) (h "0ks42n75nwhv04ac4cqywhdi5bsm1zvf87ic7fg28haylidlizpr") (y #t)))

