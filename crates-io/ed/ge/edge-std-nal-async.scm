(define-module (crates-io ed ge edge-std-nal-async) #:use-module (crates-io))

(define-public crate-edge-std-nal-async-0.1.0 (c (n "edge-std-nal-async") (v "0.1.0") (d (list (d (n "async-io") (r "^2") (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "embedded-nal-async") (r "^0.7") (k 0)) (d (n "embedded-nal-async-xtra") (r "^0.1.0") (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "heapless") (r "^0.8") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "071iv90awk0ql0bvy7ggp2mrpkbjw7zbd189x72n07my68i22pm4") (r "1.75")))

(define-public crate-edge-std-nal-async-0.2.0 (c (n "edge-std-nal-async") (v "0.2.0") (d (list (d (n "async-io") (r "^2") (d #t) (k 0)) (d (n "embedded-io-async") (r "^0.6") (f (quote ("std"))) (k 0)) (d (n "embedded-nal-async") (r "^0.7") (k 0)) (d (n "embedded-nal-async-xtra") (r "^0.2.0") (k 0)) (d (n "futures-lite") (r "^1") (d #t) (k 0)) (d (n "heapless") (r "^0.8") (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1d7vfmgm7agpgr8xham7nsins9jvbknwfjr38ysc8z798pr3mwml") (r "1.75")))

