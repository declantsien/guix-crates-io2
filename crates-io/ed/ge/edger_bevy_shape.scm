(define-module (crates-io ed ge edger_bevy_shape) #:use-module (crates-io))

(define-public crate-edger_bevy_shape-0.1.0 (c (n "edger_bevy_shape") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy_prototype_lyon") (r "^0.11.0") (d #t) (k 0)))) (h "0sqrc9624ifjpj6cr23nryjp0c8h24a60nklj8nq2gwrqmjqyp05") (f (quote (("default" "bevy/bevy_gltf" "bevy/bevy_render" "bevy/png"))))))

(define-public crate-edger_bevy_shape-0.2.0 (c (n "edger_bevy_shape") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "bevy_prototype_lyon") (r "^0.11.0") (d #t) (k 0)))) (h "1mpypxbrqfwzd8xci89q8fybk0pc3v8h4swww1whr6dgfdimz0j3") (f (quote (("default" "bevy/bevy_gltf" "bevy/bevy_render" "bevy/png"))))))

