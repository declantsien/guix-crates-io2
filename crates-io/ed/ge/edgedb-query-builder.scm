(define-module (crates-io ed ge edgedb-query-builder) #:use-module (crates-io))

(define-public crate-edgedb-query-builder-0.1.0 (c (n "edgedb-query-builder") (v "0.1.0") (h "106pihkdyr5s0x3zlk0zw8cqila207p08vpjnqwpndviayvna01k")))

(define-public crate-edgedb-query-builder-0.1.1 (c (n "edgedb-query-builder") (v "0.1.1") (h "070bahp0qscl0mz17mpllna8jgnymq3fnk2b3lqha3bnj5jp7mv6")))

(define-public crate-edgedb-query-builder-0.1.2 (c (n "edgedb-query-builder") (v "0.1.2") (h "10236pcmy70kb2j008fjl6fzhh94hvi3lpa90izg0m2w624nr60f")))

