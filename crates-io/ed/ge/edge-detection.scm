(define-module (crates-io ed ge edge-detection) #:use-module (crates-io))

(define-public crate-edge-detection-0.0.1 (c (n "edge-detection") (v "0.0.1") (d (list (d (n "image") (r "~0.17.0") (d #t) (k 0)))) (h "0m76130nhd9czfqyyrjw8hms35zgs37rg8c0wj228wqglml6hfl0")))

(define-public crate-edge-detection-0.0.2 (c (n "edge-detection") (v "0.0.2") (d (list (d (n "image") (r "~0.17.0") (d #t) (k 0)))) (h "1nhsbkafdpprv7dihl3c0kr21av059aymmihvjxx5m074cvml23j")))

(define-public crate-edge-detection-0.0.3 (c (n "edge-detection") (v "0.0.3") (d (list (d (n "image") (r "~0.17.0") (d #t) (k 0)))) (h "1npbxp4pyq8k6xb6s4xp4c3i9fh4cvczyx5pz3fnpafkl2lc2jyj")))

(define-public crate-edge-detection-0.0.4 (c (n "edge-detection") (v "0.0.4") (d (list (d (n "image") (r "~0.17.0") (d #t) (k 0)) (d (n "rayon") (r "~0.8.2") (d #t) (k 0)))) (h "1ihkv4pp5jv3yl48iqks203fqcbdz8cvgz1l4xhdnwjqaar4rfm8") (f (quote (("unstable"))))))

(define-public crate-edge-detection-0.1.0 (c (n "edge-detection") (v "0.1.0") (d (list (d (n "image") (r "~0.17.0") (d #t) (k 0)) (d (n "rayon") (r "~0.8.2") (d #t) (k 0)))) (h "1xas10r1rkbqm8ygwsds842vkcxrna78mcpy0zvkkfk09crarb2p") (f (quote (("unstable"))))))

(define-public crate-edge-detection-0.1.1 (c (n "edge-detection") (v "0.1.1") (d (list (d (n "image") (r "~0.17.0") (d #t) (k 0)) (d (n "rayon") (r "~0.8.2") (d #t) (k 0)))) (h "0dk8g2wdf4cji8z1cpla44r3rlk3lp5fi2vfac9s8bqxrm5hjh5w") (f (quote (("unstable"))))))

(define-public crate-edge-detection-0.2.0 (c (n "edge-detection") (v "0.2.0") (d (list (d (n "image") (r "~0.17.0") (d #t) (k 0)) (d (n "rayon") (r "~0.8.2") (d #t) (k 0)))) (h "1yaaipr948ddxv1wc4pkjb7calsfvav5kdhq2xr2g840wl5knbmd") (f (quote (("unstable"))))))

(define-public crate-edge-detection-0.2.1 (c (n "edge-detection") (v "0.2.1") (d (list (d (n "image") (r "^0.18.0") (d #t) (k 0)) (d (n "rayon") (r "^0.9.0") (d #t) (k 0)))) (h "05xd6a47kqj7ybi3c31d86ik77qlscyq7700xkwz009hznrmqsvg") (f (quote (("unstable"))))))

(define-public crate-edge-detection-0.2.2 (c (n "edge-detection") (v "0.2.2") (d (list (d (n "image") (r "^0.20.1") (d #t) (k 0)) (d (n "rayon") (r "^1.0.3") (d #t) (k 0)))) (h "1fz7yx7w68cjjczc3d5gl3mb99d94vn20sy17zjylwb3s6ij9hbl") (f (quote (("unstable"))))))

(define-public crate-edge-detection-0.2.3 (c (n "edge-detection") (v "0.2.3") (d (list (d (n "image") (r "^0.21") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "19nq6382jc42k8jd6j8gpmvs5yglmg62q64gam3wbwfyr9qf3s5y") (f (quote (("unstable"))))))

(define-public crate-edge-detection-0.2.4 (c (n "edge-detection") (v "0.2.4") (d (list (d (n "image") (r "^0.22") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "057xdxdlcac2k8gm038pkhmr64cbf3685r2j2adx15dwkww6b0pr") (f (quote (("unstable"))))))

(define-public crate-edge-detection-0.2.5 (c (n "edge-detection") (v "0.2.5") (d (list (d (n "image") (r "^0.23") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0qbsra0f6h7r4vld8c6m7qn49n40v4s2kkb1dg0h7hkpg3g87w2q") (f (quote (("unstable"))))))

(define-public crate-edge-detection-0.2.6 (c (n "edge-detection") (v "0.2.6") (d (list (d (n "image") (r "^0.24") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "06kznmqjmq6d0cwln4737c5hil6k7p6h523k27rcp2aixzshvzbc") (f (quote (("unstable"))))))

