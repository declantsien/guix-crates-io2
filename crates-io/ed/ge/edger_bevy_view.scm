(define-module (crates-io ed ge edger_bevy_view) #:use-module (crates-io))

(define-public crate-edger_bevy_view-0.1.0 (c (n "edger_bevy_view") (v "0.1.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "edger_bevy_shape") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "edger_bevy_util") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.8.1") (d #t) (k 0)))) (h "1zfh7lbl1c6mylm1wgphlf7li5rj9d2nk1aclziy37dplz910s0c") (f (quote (("shape" "edger_bevy_shape" "rand") ("default" "bevy/bevy_gltf" "bevy/bevy_render" "bevy/png"))))))

(define-public crate-edger_bevy_view-0.2.0 (c (n "edger_bevy_view") (v "0.2.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "edger_bevy_shape") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "edger_bevy_util") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.8.1") (d #t) (k 0)))) (h "1rjrq3d461avvfvy4v4pzqkzy6wzrzydl9i884jxnc6mnsagsfrl") (f (quote (("shape" "edger_bevy_shape" "rand") ("default" "bevy/bevy_gltf" "bevy/bevy_render" "bevy/png"))))))

(define-public crate-edger_bevy_view-0.3.0 (c (n "edger_bevy_view") (v "0.3.0") (d (list (d (n "bevy") (r "^0.13.0") (k 0)) (d (n "edger_bevy_shape") (r "^0.2.0") (o #t) (d #t) (k 0)) (d (n "edger_bevy_util") (r "^0.1.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (o #t) (d #t) (k 0)) (d (n "snafu") (r "^0.8.1") (d #t) (k 0)))) (h "03456nr97ky1x3ihf41kqk2hagawf911gfpfr5lg09x3cf9479ap") (f (quote (("shape" "edger_bevy_shape" "rand") ("default" "bevy/bevy_gltf" "bevy/bevy_render" "bevy/png"))))))

