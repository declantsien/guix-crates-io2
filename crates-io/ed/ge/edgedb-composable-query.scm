(define-module (crates-io ed ge edgedb-composable-query) #:use-module (crates-io))

(define-public crate-edgedb-composable-query-0.0.1 (c (n "edgedb-composable-query") (v "0.0.1") (d (list (d (n "edgedb-protocol") (r "^0.6.0") (d #t) (k 0)) (d (n "itertools") (r "^0.11") (d #t) (k 0)))) (h "0zwrj93fb7whffvzvxn4snbv94r8qcj0vzmdav54y287l48n4ql1")))

(define-public crate-edgedb-composable-query-0.0.4 (c (n "edgedb-composable-query") (v "0.0.4") (d (list (d (n "anyhow") (r "^1") (d #t) (k 0)) (d (n "edgedb-composable-query-derive") (r "^0.0.4") (d #t) (k 0)) (d (n "edgedb-protocol") (r "^0.6") (d #t) (k 0)) (d (n "edgedb-tokio") (r "^0.5") (d #t) (k 0)) (d (n "insta") (r "^1.34.0") (d #t) (k 2)) (d (n "itertools") (r "^0.11") (d #t) (k 0)) (d (n "nonempty") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "tokio") (r "^1.34.0") (f (quote ("full"))) (d #t) (k 2)) (d (n "tokio-test") (r "^0.4") (d #t) (k 2)))) (h "0gfdhcb993k018siqscim0ldyk4dwkb0aq3zqda4d0m1dghrgm2n")))

