(define-module (crates-io ed ge edgehog-device-forwarder-proto) #:use-module (crates-io))

(define-public crate-edgehog-device-forwarder-proto-0.1.0-alpha.0 (c (n "edgehog-device-forwarder-proto") (v "0.1.0-alpha.0") (d (list (d (n "prost") (r "^0.12.1") (d #t) (k 0)))) (h "1lq445jm5i59vrdna22k6hspcvnrqabn2g5zdrz52yc8zzs15dig") (r "1.66.1")))

