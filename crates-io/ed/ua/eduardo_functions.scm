(define-module (crates-io ed ua eduardo_functions) #:use-module (crates-io))

(define-public crate-eduardo_functions-0.1.0 (c (n "eduardo_functions") (v "0.1.0") (h "1pa729fyh4xs3k2b28fsa2vyfji00ihaiw0lmz43am5kz5mqa1r6")))

(define-public crate-eduardo_functions-0.1.1 (c (n "eduardo_functions") (v "0.1.1") (h "1ybyr51wmbcl9yd6pfw7l17wji5a7jlanhzf9db9xr4gwamzcp9r")))

