(define-module (crates-io ed ua eduapi-port) #:use-module (crates-io))

(define-public crate-eduapi-port-0.1.0 (c (n "eduapi-port") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.75") (d #t) (k 0)) (d (n "assert_cmd") (r "^2.0.12") (f (quote ("color-auto"))) (d #t) (k 0)) (d (n "async-curl") (r "^0.1.6") (f (quote ("multi"))) (d #t) (k 0)) (d (n "nix") (r "^0.27.1") (f (quote ("fs" "aio"))) (d #t) (k 0)))) (h "0haj7cc3758j0pywqi4yxb5yflrc39wvz0iilpx9kpafj8hd2kvw")))

