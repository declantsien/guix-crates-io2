(define-module (crates-io ed ua eduardo_more_cargo) #:use-module (crates-io))

(define-public crate-eduardo_more_cargo-0.1.0 (c (n "eduardo_more_cargo") (v "0.1.0") (h "0a6ldg5yha57qlxihg79ndwg9s4341igxhb4s38w42bq0xh26jvx") (y #t)))

(define-public crate-eduardo_more_cargo-0.1.1 (c (n "eduardo_more_cargo") (v "0.1.1") (h "08my1qzikl8vmn4j3c4d0ddk0zv4q8yi5a7h5b2pzp2j6by8craj")))

