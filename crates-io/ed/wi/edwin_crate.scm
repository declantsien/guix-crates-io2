(define-module (crates-io ed wi edwin_crate) #:use-module (crates-io))

(define-public crate-edwin_crate-0.1.0 (c (n "edwin_crate") (v "0.1.0") (h "0c70i5inh6f149kd1jzf4gjs9fv22iwdyp4k0ywyf0bfw6n9xif3")))

(define-public crate-edwin_crate-0.1.1 (c (n "edwin_crate") (v "0.1.1") (h "0d2acqgihbapn6fdchismi2wl44ndx5l3ccha1pznhwdm0aidcl7")))

