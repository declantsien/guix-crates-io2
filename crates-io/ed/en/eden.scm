(define-module (crates-io ed en eden) #:use-module (crates-io))

(define-public crate-eden-0.1.0 (c (n "eden") (v "0.1.0") (h "0k8rq8syy0lrkh3dw9fg97phcpbzklvrgywdbl1k6vk9900rj086")))

(define-public crate-eden-0.1.1 (c (n "eden") (v "0.1.1") (d (list (d (n "nom") (r "^5.1.2") (d #t) (k 0)))) (h "0f7pqsr9hgn97gz8mnr3z3r42bcs15225rgcp7d7k7bwy62yhrcg")))

