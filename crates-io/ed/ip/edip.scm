(define-module (crates-io ed ip edip) #:use-module (crates-io))

(define-public crate-edip-0.1.0 (c (n "edip") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "gchemol") (r "^0.0.42") (f (quote ("adhoc"))) (d #t) (k 0)) (d (n "gut") (r "^0.1.3") (d #t) (k 0) (p "gchemol-gut")) (d (n "vecfx") (r "^0.1") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "0ljridizc01azvzaax6wz1k9w2pp0mbp74zr035v88k6x11f292c") (f (quote (("adhoc"))))))

(define-public crate-edip-0.1.1 (c (n "edip") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "gchemol") (r "^0.0.42") (f (quote ("adhoc"))) (d #t) (k 2)) (d (n "gut") (r "^0.3") (d #t) (k 0) (p "gchemol-gut")) (d (n "vecfx") (r "^0.1") (f (quote ("nalgebra"))) (d #t) (k 0)))) (h "1kyr0h6swd5h4c4z2mmckhnf6ff7vgxx34q1vw5q345xyfgj9hd6") (f (quote (("adhoc"))))))

