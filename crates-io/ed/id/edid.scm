(define-module (crates-io ed id edid) #:use-module (crates-io))

(define-public crate-edid-0.1.0 (c (n "edid") (v "0.1.0") (d (list (d (n "nom") (r "^3.2.0") (d #t) (k 0)))) (h "11z3qcmrgalx8j3j77gvx7mx562qpl5yak25q701a330rxjbh3mn")))

(define-public crate-edid-0.1.1 (c (n "edid") (v "0.1.1") (d (list (d (n "nom") (r "^3.2.0") (d #t) (k 0)))) (h "18bzr90dq5bzjwphhx1glmyqyz7n75pj7mnv80zivmqi61f7fpwk")))

(define-public crate-edid-0.2.0 (c (n "edid") (v "0.2.0") (d (list (d (n "nom") (r "^3.2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1jpk6hz6vl9fscrzlviipf7vbfm03ha25fclk4whdiyz9kd17iwv")))

(define-public crate-edid-0.3.0 (c (n "edid") (v "0.3.0") (d (list (d (n "nom") (r "^3.2.0") (f (quote ("verbose-errors"))) (d #t) (k 0)))) (h "1zi4md5sy60nlcy45d3w9r48ki6ywzh7rdivzvf39n4k119pbki4")))

