(define-module (crates-io ed if edifact-types-macros) #:use-module (crates-io))

(define-public crate-edifact-types-macros-0.1.0 (c (n "edifact-types-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0ml22630v0s4ns4yyadl3h6g2gi1h1y0fvrapyhhz6pkfsk3a3mh")))

(define-public crate-edifact-types-macros-0.2.0 (c (n "edifact-types-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("parsing" "extra-traits"))) (d #t) (k 0)))) (h "0mhqqb40fp51y3x20ry551vj8c2j2511pm4ai1vah7rsz0z28r2f")))

(define-public crate-edifact-types-macros-0.2.2 (c (n "edifact-types-macros") (v "0.2.2") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0bcyfgdiwj7gi16nrg60fbdai78myx46fkbphsja5cwd5bzixicr")))

(define-public crate-edifact-types-macros-0.2.1 (c (n "edifact-types-macros") (v "0.2.1") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "11hir6m15xddbsrngqxj2fvpyr1118aqnn8vszdi29magdfsnz7k")))

(define-public crate-edifact-types-macros-0.2.3 (c (n "edifact-types-macros") (v "0.2.3") (d (list (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "01cz2ph8kx764s0i3a8d4v5cfapzahbnpfv6z8kcbamk49ifl0yj")))

(define-public crate-edifact-types-macros-0.3.0 (c (n "edifact-types-macros") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1q5kz3b0j19gy6jw3lc59kdwisqczs6v4srgk66bxvjhgiiqglfh") (f (quote (("default") ("debug"))))))

(define-public crate-edifact-types-macros-0.3.1 (c (n "edifact-types-macros") (v "0.3.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0vm1l889n0vszriq6x7mbslllvffpy8ad3r3i40h9p47nxkx9las") (f (quote (("default") ("debug"))))))

(define-public crate-edifact-types-macros-0.3.2 (c (n "edifact-types-macros") (v "0.3.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "rustversion") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "0fsyb1yxkgqswr6hcidbjpv07mgqvihn79xrx5ky3ykkahrv0x22") (f (quote (("default") ("debug"))))))

