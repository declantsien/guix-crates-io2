(define-module (crates-io ed tf edtf) #:use-module (crates-io))

(define-public crate-edtf-0.1.0 (c (n "edtf") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "no-panic") (r "^0.1.15") (d #t) (k 2)) (d (n "nom") (r "^7.0.0") (k 0)) (d (n "num-integer") (r "^0.1.36") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1ppvwqwqcj81jxky2zm60a09qrar03iargchh0kd9wiblc525yil") (f (quote (("default"))))))

(define-public crate-edtf-0.1.1 (c (n "edtf") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "no-panic") (r "^0.1.15") (d #t) (k 2)) (d (n "nom") (r "^7.0.0") (k 0)) (d (n "num-integer") (r "^0.1.36") (k 0)) (d (n "version-sync") (r "^0.9") (d #t) (k 2)))) (h "1v2pd51m9f5xvwirbpnmk0w8r6vnd8c52ygg9wvsyqjad4b3djs7") (f (quote (("default"))))))

(define-public crate-edtf-0.2.0 (c (n "edtf") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (o #t) (k 0)) (d (n "no-panic") (r "^0.1.15") (d #t) (k 2)) (d (n "nom") (r "^7.0.0") (k 0)) (d (n "num-integer") (r "^0.1.36") (k 0)) (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1") (d #t) (k 2)))) (h "06khgrfmk4i6ippm2r5lx884i80xifz6ajcqy3jprh11mzdpv33w") (f (quote (("default"))))))

