(define-module (crates-io ed -d ed-derive) #:use-module (crates-io))

(define-public crate-ed-derive-0.1.0 (c (n "ed-derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1qrr8i4xgrd9n1ixnsv6gxhrc6gj0531ik30i6gickjbsmh038yc")))

(define-public crate-ed-derive-0.1.1 (c (n "ed-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0fcv9kd3i5cgbfd9dwx0lip9g3wh752pc4snfqn3clw8v28mdjgn")))

(define-public crate-ed-derive-0.2.0 (c (n "ed-derive") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0aglh7hsx2yhxlgnw06x0cycilz15ljn71xqa2yc7qy2wysym9m5")))

(define-public crate-ed-derive-0.2.1 (c (n "ed-derive") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0b1p8n7s15vdp4b4vk4v50d3pq46z00kj1j8b1sk7x2kfxhbjqz1")))

(define-public crate-ed-derive-0.2.2 (c (n "ed-derive") (v "0.2.2") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1dk4nnwvlcwhaf9px55cn61qmlw7qw70v1i5n2mk66286jhgdybr")))

(define-public crate-ed-derive-0.2.3 (c (n "ed-derive") (v "0.2.3") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "0q9ihq7mixan166gr1das6jx6wkdwrjn3g4ilz51m1jb9xviva86")))

(define-public crate-ed-derive-0.3.0 (c (n "ed-derive") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1jz2vpnk1m94j9xrpnrjx5qqcqchx2m70a69rz49zd6n0wgpx479")))

