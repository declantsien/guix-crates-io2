(define-module (crates-io ed el edeltraud) #:use-module (crates-io))

(define-public crate-edeltraud-0.16.4 (c (n "edeltraud") (v "0.16.4") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt"))) (d #t) (k 2)))) (h "07g691dwd66abw6hcip33q2j5xyj7zycp369vaqlbakvrk38ngph")))

