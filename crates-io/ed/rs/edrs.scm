(define-module (crates-io ed rs edrs) #:use-module (crates-io))

(define-public crate-edrs-0.1.0 (c (n "edrs") (v "0.1.0") (d (list (d (n "clap") (r ">=4.3.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "syntect") (r "^5.0.0") (d #t) (k 0)))) (h "0azakjg22sbziwf6d5f5yl5577691zf9fpzd5w3s0ir80kggik5y")))

