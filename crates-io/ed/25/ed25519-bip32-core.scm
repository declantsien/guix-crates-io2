(define-module (crates-io ed #{25}# ed25519-bip32-core) #:use-module (crates-io))

(define-public crate-ed25519-bip32-core-0.1.0 (c (n "ed25519-bip32-core") (v "0.1.0") (d (list (d (n "cryptoxide") (r "^0.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "1xdq0v2fmkz2390nlx4zz2307fv8v1ringmlck457pprrzqg115v") (f (quote (("with-bench") ("std") ("default" "std"))))))

(define-public crate-ed25519-bip32-core-0.1.1 (c (n "ed25519-bip32-core") (v "0.1.1") (d (list (d (n "cryptoxide") (r "^0.4") (d #t) (k 0)) (d (n "zeroize") (r "^1.6.0") (d #t) (k 0)))) (h "0sfxmqwyzaqkgxzyd4916gr378pqh85f8l76n7niyxf377564nfk") (f (quote (("with-bench") ("std") ("default" "std"))))))

