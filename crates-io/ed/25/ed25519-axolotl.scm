(define-module (crates-io ed #{25}# ed25519-axolotl) #:use-module (crates-io))

(define-public crate-ed25519-axolotl-0.1.0 (c (n "ed25519-axolotl") (v "0.1.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1pkhgmwbi9llr288p3mla1ln769qhxxmqsycxlzi4png5j6i4wdf")))

(define-public crate-ed25519-axolotl-0.1.1 (c (n "ed25519-axolotl") (v "0.1.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1pqc5h25c8i7cig3pj8z153sgr45w9bajb8p8sq3gcbbq7s8x9d1")))

(define-public crate-ed25519-axolotl-0.1.4 (c (n "ed25519-axolotl") (v "0.1.4") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "17d9ndhi9j3n5zqphcj1hf5l8yijg0v3yhzmv5vbr2val1jj60rj")))

(define-public crate-ed25519-axolotl-1.0.0 (c (n "ed25519-axolotl") (v "1.0.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "01n1klcpr8rqrl4w4y2vpgqabjzglfa8a8g0rmcqk4jrlr1v30na")))

(define-public crate-ed25519-axolotl-0.1.5 (c (n "ed25519-axolotl") (v "0.1.5") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0fzs1zkg7fmmz5jc6v3365pakf3kwbjm91vhr7vpy7pfcy0ppnky")))

(define-public crate-ed25519-axolotl-1.0.1 (c (n "ed25519-axolotl") (v "1.0.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "06qxz9ax4bxp325wi5xs5g70k3mzqpn25n6p8bxpclmhrn3m5b9l")))

(define-public crate-ed25519-axolotl-1.1.4 (c (n "ed25519-axolotl") (v "1.1.4") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1hlh6bn4wv1jdfpd27ldfqcqcpscblf7vn50hdcllzhr2ygzsfzc")))

(define-public crate-ed25519-axolotl-1.2.1 (c (n "ed25519-axolotl") (v "1.2.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0dvycbqi0sbq58wlrag2x2b5jlnhklkbvswkvnds7kh23yy2iz5d")))

(define-public crate-ed25519-axolotl-1.3.1 (c (n "ed25519-axolotl") (v "1.3.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.56") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 0)))) (h "0das7qp45xjmqxf6wlb594x825a8gm8k104ah4cqcvhn1x3087ih")))

(define-public crate-ed25519-axolotl-1.5.0 (c (n "ed25519-axolotl") (v "1.5.0") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 0)))) (h "1mih6mi9w1mn8j5zm4gdpf4dqk1ygdn0chnbjszbp4z1sma7dwnl")))

(define-public crate-ed25519-axolotl-1.6.1 (c (n "ed25519-axolotl") (v "1.6.1") (d (list (d (n "getrandom") (r "^0.2") (f (quote ("js"))) (d #t) (k 0)) (d (n "js-sys") (r "^0.3.56") (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "wasm-bindgen") (r "^0.2.78") (d #t) (k 0)) (d (n "wasm-bindgen-test") (r "^0.3.29") (d #t) (k 0)))) (h "1lxdhqqm1clg1yynw0gv6aryi5jk7z7ldfh7myqq22cc98q0647l")))

(define-public crate-ed25519-axolotl-1.7.0 (c (n "ed25519-axolotl") (v "1.7.0") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "0whq41zwy3nnnfxcv19p6cpvhji2szbn6xyc5r1jnh46zk54b4nv") (y #t)))

(define-public crate-ed25519-axolotl-1.7.1 (c (n "ed25519-axolotl") (v "1.7.1") (d (list (d (n "rand") (r "^0.8.4") (d #t) (k 0)))) (h "1xlr7bklqz2knz68lzz892gxxbqg158h95qmryzxirx42hy6q2a8")))

