(define-module (crates-io ed #{25}# ed25519-hd-key) #:use-module (crates-io))

(define-public crate-ed25519-hd-key-0.3.0 (c (n "ed25519-hd-key") (v "0.3.0") (d (list (d (n "hmac") (r "^0.12") (d #t) (k 0)) (d (n "regex") (r "^1.7.0") (d #t) (k 0)) (d (n "sha2") (r "^0.10") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.2") (d #t) (k 0)))) (h "1vxy7zi21v2wirpykpah6hgf2cn0vx9wy4gai6jwhws18vf3c2xd")))

