(define-module (crates-io ed #{25}# ed25519_to_curve25519) #:use-module (crates-io))

(define-public crate-ed25519_to_curve25519-0.1.0 (c (n "ed25519_to_curve25519") (v "0.1.0") (h "10bz6y9cs64irv5sh7wrq389l1zp6af3raw0m58ax3sa14y6rxzy")))

(define-public crate-ed25519_to_curve25519-0.2.0 (c (n "ed25519_to_curve25519") (v "0.2.0") (h "1dlnhm9hxqf2nkj20xi7gy7bmlf91875y6cjg4bvppqcy3k391q4")))

(define-public crate-ed25519_to_curve25519-0.2.1 (c (n "ed25519_to_curve25519") (v "0.2.1") (h "1bnlsrppks1jdi2j9mhzfzlgqlmrj1h93k1vq3rs7wj0yygnrnsk")))

(define-public crate-ed25519_to_curve25519-0.2.2 (c (n "ed25519_to_curve25519") (v "0.2.2") (h "0685x6p02pjci5hn2h80dpjrnr91rp4bh8gmdq08l4c0r5phhypf")))

(define-public crate-ed25519_to_curve25519-0.2.3 (c (n "ed25519_to_curve25519") (v "0.2.3") (h "1dkbs2vcw2hh8ph36mnfafy3kzaa24xr3wld19rpkpaa8wjn15sa")))

