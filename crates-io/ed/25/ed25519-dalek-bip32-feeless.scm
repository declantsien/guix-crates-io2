(define-module (crates-io ed #{25}# ed25519-dalek-bip32-feeless) #:use-module (crates-io))

(define-public crate-ed25519-dalek-bip32-feeless-0.1.1 (c (n "ed25519-dalek-bip32-feeless") (v "0.1.1") (d (list (d (n "derivation-path") (r "^0.1.3") (k 0)) (d (n "ed25519-dalek") (r "^1.0.1") (f (quote ("u64_backend" "rand"))) (k 0)) (d (n "hex") (r "^0.4.2") (d #t) (k 2)) (d (n "hmac") (r "^0.9.0") (k 0)) (d (n "sha2") (r "^0.9.1") (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)))) (h "1v8hxgxn9zy4wnyyr5gjjylh29kpgbnk8nd6kzzmgcc0hnx1na0r") (f (quote (("std" "derivation-path/std" "sha2/std" "ed25519-dalek/std") ("default" "std"))))))

