(define-module (crates-io ed #{25}# ed25519-bip32) #:use-module (crates-io))

(define-public crate-ed25519-bip32-0.1.0 (c (n "ed25519-bip32") (v "0.1.0") (d (list (d (n "cryptoxide") (r "^0.1") (d #t) (k 0)))) (h "15h3phj4na7q9vgzic247ps11qxfd029l907ljx3h7lcqzh4ibs9") (f (quote (("with-bench") ("default"))))))

(define-public crate-ed25519-bip32-0.1.1 (c (n "ed25519-bip32") (v "0.1.1") (d (list (d (n "cryptoxide") (r "^0.1") (d #t) (k 0)))) (h "0lgr6zdxxmrd9yjdrz44ak7crybf3nfprjw5csfnarf7lw8d8jnq") (f (quote (("with-bench") ("default"))))))

(define-public crate-ed25519-bip32-0.1.2 (c (n "ed25519-bip32") (v "0.1.2") (d (list (d (n "cryptoxide") (r "^0.1") (d #t) (k 0)))) (h "1q6lnlwdcgjagz20iiy2gafkqqpgagqhj5hk27p9y3a3bs0c3prn") (f (quote (("with-bench") ("default"))))))

(define-public crate-ed25519-bip32-0.1.3 (c (n "ed25519-bip32") (v "0.1.3") (d (list (d (n "cryptoxide") (r "^0.1") (d #t) (k 0)))) (h "07w9bbr55dwilz2rp40pisayyvzkl74w57h3sbsrx5w9jwiqky1s") (f (quote (("with-bench") ("default")))) (y #t)))

(define-public crate-ed25519-bip32-0.1.4 (c (n "ed25519-bip32") (v "0.1.4") (d (list (d (n "cryptoxide") (r "^0.1") (d #t) (k 0)))) (h "0p07y9hwqpkkzd33wchsg8l8v50lzgr4vvv9xqcz2hxksci9088b") (f (quote (("with-bench") ("default"))))))

(define-public crate-ed25519-bip32-0.1.5 (c (n "ed25519-bip32") (v "0.1.5") (d (list (d (n "cryptoxide") (r "^0.1") (d #t) (k 0)))) (h "0gasdzqsnsq4k9yl42dqnd7xa86zcps0nm6fsg9vfhddz0k0xjcx") (f (quote (("with-bench") ("default"))))))

(define-public crate-ed25519-bip32-0.2.0 (c (n "ed25519-bip32") (v "0.2.0") (d (list (d (n "cryptoxide") (r "^0.1") (d #t) (k 0)))) (h "1cs1d1vy1a3j9xjwdd9jcapbdbikxwacjjqnldrdakigpiyhyipg") (f (quote (("with-bench") ("default"))))))

(define-public crate-ed25519-bip32-0.3.0 (c (n "ed25519-bip32") (v "0.3.0") (d (list (d (n "cryptoxide") (r "^0.1") (d #t) (k 0)))) (h "1vz6lvrir5szcx5sv1r8bx2pvkgd6zz1npkhjs6v2bwxmvv7hlbv") (f (quote (("with-bench") ("default"))))))

(define-public crate-ed25519-bip32-0.3.1 (c (n "ed25519-bip32") (v "0.3.1") (d (list (d (n "cryptoxide") (r "^0.2") (d #t) (k 0)))) (h "0vzw0rbyna8dyvn5aawvn8msssj92bnqr632fh2dh5n0dzhpqwp4") (f (quote (("with-bench") ("default"))))))

(define-public crate-ed25519-bip32-0.3.2 (c (n "ed25519-bip32") (v "0.3.2") (d (list (d (n "cryptoxide") (r "^0.3") (d #t) (k 0)))) (h "14pyvlmfmzin3zw75d8gbr345md8645yahciwkxl24ai5c51h9w8") (f (quote (("with-bench") ("default"))))))

(define-public crate-ed25519-bip32-0.4.0 (c (n "ed25519-bip32") (v "0.4.0") (d (list (d (n "cryptoxide") (r "^0.3") (d #t) (k 0)))) (h "1k63xv2bmxn34gnsjgrgipwr2ngxi1n1v1bm1j1mgwpwhkbiw2bc") (f (quote (("with-bench") ("default"))))))

(define-public crate-ed25519-bip32-0.4.1 (c (n "ed25519-bip32") (v "0.4.1") (d (list (d (n "cryptoxide") (r "^0.4") (d #t) (k 0)))) (h "089a97gsr9cx32z66n8h3ziv5c6x1h1nvza9i1k2y6yrq29qyn6b") (f (quote (("with-bench") ("default"))))))

