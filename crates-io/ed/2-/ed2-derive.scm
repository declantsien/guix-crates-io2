(define-module (crates-io ed #{2-}# ed2-derive) #:use-module (crates-io))

(define-public crate-ed2-derive-0.1.1 (c (n "ed2-derive") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.10") (d #t) (k 0)) (d (n "quote") (r "^1.0.3") (d #t) (k 0)) (d (n "syn") (r "^1.0.17") (d #t) (k 0)))) (h "1cbqvlw56hajdmyyvvjarn6ycl0qqgn83c0795iv2kslqfryz461")))

