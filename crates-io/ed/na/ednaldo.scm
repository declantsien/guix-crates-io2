(define-module (crates-io ed na ednaldo) #:use-module (crates-io))

(define-public crate-ednaldo-0.1.0 (c (n "ednaldo") (v "0.1.0") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "term_size") (r "^1.0.0-beta1") (d #t) (k 0)))) (h "1a1rr0dwb7d75jm1ash4qxwgnmba61dpxn5sw8zy9ihgz55jkr5s") (y #t)))

(define-public crate-ednaldo-0.1.1 (c (n "ednaldo") (v "0.1.1") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "term_size") (r "^1.0.0-beta1") (d #t) (k 0)))) (h "1ika6sx6wh15iix7dbn6y20hm2xksipz88k5qpaq58zhavjcnb0x") (y #t)))

(define-public crate-ednaldo-0.1.2 (c (n "ednaldo") (v "0.1.2") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "term_size") (r "^1.0.0-beta1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "macros"))) (d #t) (k 0)))) (h "13b10zgl5q10p8vah24qaqz0hhp7wg5admb3nm6fn7b0xj0fbbcq")))

(define-public crate-ednaldo-0.1.3 (c (n "ednaldo") (v "0.1.3") (d (list (d (n "colorful") (r "^0.2.1") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "term_size") (r "^1.0.0-beta1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "macros"))) (d #t) (k 0)))) (h "1gd1hc18ribwnd12v2knmzh73x9a7cjgw4a6f0kpclbja83j1pdb")))

(define-public crate-ednaldo-0.1.4 (c (n "ednaldo") (v "0.1.4") (d (list (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "image") (r "^0.23.4") (d #t) (k 0)) (d (n "rand") (r "^0.7.3") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (d #t) (k 0)) (d (n "term_size") (r "^1.0.0-beta1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("fs" "macros"))) (d #t) (k 0)))) (h "1kkqlvvy9d799sfgwji7fmmfz4av447vsj8l4bmxaqs1n2h1rknd")))

