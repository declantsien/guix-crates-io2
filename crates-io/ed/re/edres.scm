(define-module (crates-io ed re edres) #:use-module (crates-io))

(define-public crate-edres-0.5.0 (c (n "edres") (v "0.5.0") (d (list (d (n "case") (r "~1.0.0") (o #t) (d #t) (k 0)) (d (n "failure") (r "~0.1.1") (d #t) (k 0)) (d (n "linear-map") (r "~1.2.0") (f (quote ("serde_impl"))) (d #t) (k 0)) (d (n "quote") (r "~1.0.3") (d #t) (k 0)) (d (n "ron") (r "~0.3.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "~1.0.24") (o #t) (d #t) (k 0)) (d (n "serde_yaml") (r "~0.7.5") (o #t) (d #t) (k 0)) (d (n "toml") (r "~0.4.6") (o #t) (d #t) (k 0)))) (h "14kwppybh7cnmsmd3d20zcrc500qky7hdv9cp8ibxnpb9l5y4x86") (f (quote (("yaml-parsing" "serde_yaml") ("toml-parsing" "toml") ("ron-parsing" "ron") ("json-parsing" "serde_json") ("experimental-files-enum" "case") ("default" "toml-parsing"))))))

(define-public crate-edres-0.6.0 (c (n "edres") (v "0.6.0") (d (list (d (n "edres_core") (r "=0.6.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 2)) (d (n "quote") (r "^1.0") (d #t) (k 2)))) (h "1j3w1aw599ib7q41q1c5889x5r8j16sv7swxns8bmq3j3cvxmd8l") (f (quote (("yaml" "edres_core/yaml") ("toml" "edres_core/toml") ("json" "edres_core/json") ("default" "toml"))))))

