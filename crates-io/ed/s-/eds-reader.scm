(define-module (crates-io ed s- eds-reader) #:use-module (crates-io))

(define-public crate-eds-reader-0.1.0 (c (n "eds-reader") (v "0.1.0") (d (list (d (n "eds") (r "^0.5") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.4") (d #t) (k 0)))) (h "13naj2ma4rr62391zwcpb1yyfb7qpn6vb898nxhjwvjhq0x9q97l")))

(define-public crate-eds-reader-0.1.1 (c (n "eds-reader") (v "0.1.1") (d (list (d (n "eds") (r "^0.5") (d #t) (k 0)) (d (n "fixed-queue") (r "^0.5") (d #t) (k 0)))) (h "0yzg5cxl5ank1dyxq7f4wm5hx8ryyng62c4h40r3khqbh4rsiygy")))

(define-public crate-eds-reader-0.1.2 (c (n "eds-reader") (v "0.1.2") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "eds") (r "^0.5") (d #t) (k 0)))) (h "0ks6sgzfwpk3dax2bni6vggiks14sjha2pp9md9rxr1zd11684n5")))

(define-public crate-eds-reader-0.1.3 (c (n "eds-reader") (v "0.1.3") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "eds-core") (r "^0.5") (d #t) (k 0)))) (h "1jy0gfsq35qk9lvwrbb5r3sy99gnp5zc767f2gp336qxrjkkp1i3")))

(define-public crate-eds-reader-0.1.4 (c (n "eds-reader") (v "0.1.4") (d (list (d (n "bytes") (r "^1.1") (k 0)) (d (n "eds-core") (r "^0.5") (d #t) (k 0)))) (h "1ci60m17v67dsbmhi162kq25bq8crmmx1c9i7n7hxgbk6w92iqa6")))

