(define-module (crates-io ed s- eds-writer) #:use-module (crates-io))

(define-public crate-eds-writer-0.1.0 (c (n "eds-writer") (v "0.1.0") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "eds") (r "^0.5") (d #t) (k 0)))) (h "19ska4cs06r6aqdc3w0639vfb7kj4rcn3874w7pxwv3mwxk14d8j")))

(define-public crate-eds-writer-0.1.1 (c (n "eds-writer") (v "0.1.1") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "eds") (r "^0.5") (d #t) (k 0)))) (h "1friba8njw7d7xiky2dz5vx4va4lcas5qf0ma1fblqp7yp1ri57b")))

(define-public crate-eds-writer-0.1.2 (c (n "eds-writer") (v "0.1.2") (d (list (d (n "bytes") (r "^1.1") (d #t) (k 0)) (d (n "eds-core") (r "^0.5") (d #t) (k 0)))) (h "0h8xvr4lk9vx2fv23sgczjwpsybi0qg2bpmayxx0hhn0s6sgfia3")))

(define-public crate-eds-writer-0.1.3 (c (n "eds-writer") (v "0.1.3") (d (list (d (n "bytes") (r "^1.1") (k 0)) (d (n "eds-core") (r "^0.5") (d #t) (k 0)))) (h "18ffdy8x8wg8lsm3brapsvpr83x18mj5c5dnwzhlp8hc0ra0vldg")))

