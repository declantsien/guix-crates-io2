(define-module (crates-io ed #{44}# ed448) #:use-module (crates-io))

(define-public crate-ed448-0.1.0 (c (n "ed448") (v "0.1.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "1y1q20mx3y4686cpd8g2aacar3lz01n8c0d4yws1k3q34i5p2n7b") (f (quote (("u32_backend") ("default" "u32_backend"))))))

(define-public crate-ed448-0.2.0 (c (n "ed448") (v "0.2.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "14mv4g8l3k9qsqgyqswv45qfd8370w29x5jz81m56mfvgac1g0vn") (f (quote (("u32_backend") ("default" "u32_backend"))))))

(define-public crate-ed448-0.3.0 (c (n "ed448") (v "0.3.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "1mkh50kb33ih5vicqapjl7c25j3s6p33mrj5yj9p7p2pvalsv6s3") (f (quote (("u32_backend") ("default" "u32_backend"))))))

(define-public crate-ed448-0.4.0 (c (n "ed448") (v "0.4.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "1gd2xv20x4g0njj5h18k6wvfs4ryyn0iggik5b9b6zsf8sc76vgw") (f (quote (("u32_backend") ("default" "u32_backend"))))))

