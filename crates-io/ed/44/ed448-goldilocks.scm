(define-module (crates-io ed #{44}# ed448-goldilocks) #:use-module (crates-io))

(define-public crate-ed448-goldilocks-0.4.0 (c (n "ed448-goldilocks") (v "0.4.0") (d (list (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "1bjihx2rd9ggcwabsi477bwnb9d9is7p8x89mmlmxfd0zlksr9q0") (f (quote (("u32_backend") ("default" "u32_backend"))))))

(define-public crate-ed448-goldilocks-0.5.0 (c (n "ed448-goldilocks") (v "0.5.0") (d (list (d (n "fiat-crypto") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "040kh7sq70mnp7qmhj4pgwzyh75vn3ip5sgc4xbkllwqmdn5yyr5") (f (quote (("u32_backend") ("fiat_u64_backend" "fiat-crypto") ("default" "fiat_u64_backend"))))))

(define-public crate-ed448-goldilocks-0.6.0 (c (n "ed448-goldilocks") (v "0.6.0") (d (list (d (n "fiat-crypto") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "0zfchyrk238x9cjiv7zcs32j661frgd8v6jhcvq3hk258zd3pyzd") (f (quote (("u32_backend") ("fiat_u64_backend" "fiat-crypto") ("default" "fiat_u64_backend"))))))

(define-public crate-ed448-goldilocks-0.7.0 (c (n "ed448-goldilocks") (v "0.7.0") (d (list (d (n "fiat-crypto") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "007axyyg2l8f542iv3k97d7igh2q93qjrjizgzl0i8aswzn3kkcg") (f (quote (("u32_backend") ("fiat_u64_backend" "fiat-crypto") ("default" "fiat_u64_backend"))))))

(define-public crate-ed448-goldilocks-0.7.1 (c (n "ed448-goldilocks") (v "0.7.1") (d (list (d (n "fiat-crypto") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "1ksjhlgqm7d775p2jh5ggmlh4rc3cdx00i9x7p43a0aafpligskn") (f (quote (("u32_backend") ("fiat_u64_backend" "fiat-crypto") ("default" "fiat_u64_backend"))))))

(define-public crate-ed448-goldilocks-0.7.2 (c (n "ed448-goldilocks") (v "0.7.2") (d (list (d (n "fiat-crypto") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "0ycflyk3h3mliw25pkd1s3dnd9yzvk9sr07kd49zxm9xksggmdc7") (f (quote (("u32_backend") ("fiat_u64_backend" "fiat-crypto") ("default" "fiat_u64_backend"))))))

(define-public crate-ed448-goldilocks-0.8.0 (c (n "ed448-goldilocks") (v "0.8.0") (d (list (d (n "fiat-crypto") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "0gww4gw51j53k3sy62vazxa1l4cqrw78xphgwpf4vi7bpgadpda0") (f (quote (("u32_backend") ("fiat_u64_backend" "fiat-crypto") ("default" "fiat_u64_backend"))))))

(define-public crate-ed448-goldilocks-0.8.1 (c (n "ed448-goldilocks") (v "0.8.1") (d (list (d (n "fiat-crypto") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "1dfhhawydzn4v32ixk5jxs9ip6mkn198zcckmpk2jr8cw4hb0z7q") (f (quote (("u32_backend") ("fiat_u64_backend" "fiat-crypto") ("default" "fiat_u64_backend"))))))

(define-public crate-ed448-goldilocks-0.8.2 (c (n "ed448-goldilocks") (v "0.8.2") (d (list (d (n "fiat-crypto") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)))) (h "04gzyjxs8p4hhad4x7nc946x987fpvmb2gdmx81vx9ina1h8iqg1") (f (quote (("u32_backend") ("fiat_u64_backend" "fiat-crypto") ("default" "fiat_u64_backend"))))))

(define-public crate-ed448-goldilocks-0.8.3 (c (n "ed448-goldilocks") (v "0.8.3") (d (list (d (n "fiat-crypto") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)) (d (n "zeroize") (r "^1") (f (quote ("zeroize_derive"))) (o #t) (k 0)))) (h "14p0xc0hpdwcps9803wapihr3bxxhmzl8bh15f2300g9x4qlb4h6") (f (quote (("u32_backend") ("fiat_u64_backend" "fiat-crypto") ("default" "fiat_u64_backend"))))))

(define-public crate-ed448-goldilocks-0.9.0 (c (n "ed448-goldilocks") (v "0.9.0") (d (list (d (n "fiat-crypto") (r "^0.1.4") (o #t) (d #t) (k 0)) (d (n "hex") (r "^0.4") (d #t) (k 0)) (d (n "rand_core") (r "^0.6") (d #t) (k 0)) (d (n "subtle") (r "^2.2.2") (d #t) (k 0)) (d (n "zeroize") (r "^1") (f (quote ("zeroize_derive"))) (o #t) (k 0)))) (h "1d078d5glxdpig83cd358wam9fkzhz5k032vnrxgmindpj124cl8") (f (quote (("u32_backend") ("fiat_u64_backend" "fiat-crypto") ("default" "fiat_u64_backend"))))))

