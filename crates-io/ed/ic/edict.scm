(define-module (crates-io ed ic edict) #:use-module (crates-io))

(define-public crate-edict-0.0.0 (c (n "edict") (v "0.0.0") (h "0wam84wlcg749vnv3cir64bmw6zn660pj7mgh85ph41gnplr26q9")))

(define-public crate-edict-0.0.1 (c (n "edict") (v "0.0.1") (d (list (d (n "hashbrown") (r "^0.11") (d #t) (k 0)))) (h "0k8hpix4zkj97cbl9ng1nvwnvv3yalx0c191x21fs7xbkfpkvmwv")))

(define-public crate-edict-0.0.2 (c (n "edict") (v "0.0.2") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)))) (h "0fyh4sykdvwk63ah6j1sd0x5zjzrbkax78igpnqd4wi4h66wnnih")))

(define-public crate-edict-0.0.3 (c (n "edict") (v "0.0.3") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)))) (h "07dl33gi2nlmiwbbv492g7y1wc8ij8zrx97bj4iaks54p6s8d1yf") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.0.4 (c (n "edict") (v "0.0.4") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)))) (h "08ghn1f58kc2p77yi64qqpdxwr7na2fidgv5p5c2060y2vd4iz8f") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.0.5 (c (n "edict") (v "0.0.5") (d (list (d (n "hashbrown") (r "^0.12") (d #t) (k 0)))) (h "0sizzcjs1gyqq6yl8qj7a5pg8z79m07b0qnpa5156c4qlvj2z90j") (f (quote (("std") ("rc") ("default" "std" "rc"))))))

(define-public crate-edict-0.1.0 (c (n "edict") (v "0.1.0") (d (list (d (n "hashbrown") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)))) (h "018kgcpr51h0jrcxfd1j4jzdj22mfd2zc290dpissxgwcdp4qnv4") (f (quote (("std") ("rc") ("default" "std" "rc"))))))

(define-public crate-edict-0.1.1 (c (n "edict") (v "0.1.1") (d (list (d (n "hashbrown") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)))) (h "1xrch95ax71h5wiidax6jyh3fx4mb5xbj9jxnxhndbq7s7blfx3s") (f (quote (("std") ("rc") ("default" "std" "rc"))))))

(define-public crate-edict-0.1.2 (c (n "edict") (v "0.1.2") (d (list (d (n "hashbrown") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)))) (h "0m43bn8nrg7lnpzd1fvid5dr29qqqxsl6prykd4lkvsi67gy5p56") (f (quote (("std") ("rc") ("default" "std" "rc"))))))

(define-public crate-edict-0.1.3 (c (n "edict") (v "0.1.3") (d (list (d (n "hashbrown") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)))) (h "17fr7xbi9k1x3wy140nva1bgsc0jcr4v3k82wg62hcgbgdw8apyv") (f (quote (("std") ("rc") ("default" "std" "rc"))))))

(define-public crate-edict-0.1.4 (c (n "edict") (v "0.1.4") (d (list (d (n "hashbrown") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)))) (h "1bs9525jrhlq8wr4ybfg8cz8h91j2f8dnbw3r0qq6ba5krgphvkx") (f (quote (("std") ("rc") ("default" "std" "rc"))))))

(define-public crate-edict-0.1.5 (c (n "edict") (v "0.1.5") (d (list (d (n "hashbrown") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)))) (h "13g1ii3lkq6im8z33cbw62nnf9l5jss4fcbnjjkap4abjafm05z1") (f (quote (("std") ("rc") ("default" "std" "rc"))))))

(define-public crate-edict-0.1.6 (c (n "edict") (v "0.1.6") (d (list (d (n "hashbrown") (r "^0.12") (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)))) (h "189s9pg67xdb93ycr8ydjd7532a788xlaxx45fy6jd4hv6l5p3yw") (f (quote (("std") ("rc") ("default" "std" "rc"))))))

(define-public crate-edict-0.2.0-rc.1 (c (n "edict") (v "0.2.0-rc.1") (d (list (d (n "atomicell") (r "^0.1.4") (d #t) (k 0)) (d (n "edict-proc") (r "=0.2.0-rc.1") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (f (quote ("ahash"))) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.3") (d #t) (k 0)))) (h "0v1fi3pzp93m49ackcwy6aab97g2yppbv4jh4jwrpic2h2nl0alz") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.2.0-rc.2 (c (n "edict") (v "0.2.0-rc.2") (d (list (d (n "atomicell") (r "^0.1.4") (d #t) (k 0)) (d (n "edict-proc") (r "=0.2.0-rc.2") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (f (quote ("ahash"))) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "1blz0qk9dj78iff8ihlgj69jpiyrk2nnba7cgb3ckqiqdfvsdnaf") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.2.0-rc.3 (c (n "edict") (v "0.2.0-rc.3") (d (list (d (n "atomicell") (r "^0.1.4") (d #t) (k 0)) (d (n "edict-proc") (r "=0.2.0-rc.3") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.12") (f (quote ("ahash"))) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "01f1n6mj5ln2v005x5s12jyy92hi17hrs496rb3l0a0h1kcldfc4") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.3.0 (c (n "edict") (v "0.3.0") (d (list (d (n "atomicell") (r "^0.1.8") (d #t) (k 0)) (d (n "edict-proc") (r "=0.3.0") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("ahash"))) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "12f8m5g4pq7sznncnd4wadaq5c76pzvvp4as3jmsxvsqjjq4qgc3") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.3.1 (c (n "edict") (v "0.3.1") (d (list (d (n "atomicell") (r "^0.1.8") (d #t) (k 0)) (d (n "edict-proc") (r "=0.3.0") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("ahash"))) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "061kssspdfs1xnb5mi4pjdvwldsjqa3d2c66y6d8swn5545isppw") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.3.2 (c (n "edict") (v "0.3.2") (d (list (d (n "atomicell") (r "^0.1.8") (d #t) (k 0)) (d (n "edict-proc") (r "=0.3.0") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("ahash"))) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "1g1rcvh84zlbdgxcdpi1n2nwc1h865fxn2nxi52x7h9pdm9g769z") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.3.3 (c (n "edict") (v "0.3.3") (d (list (d (n "atomicell") (r "^0.1.8") (d #t) (k 0)) (d (n "edict-proc") (r "=0.3.0") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("ahash"))) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "1vh1niiykph7yy5z2b6j5z42cc40ibhhq0x6xvia6dvb5i00gbv9") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.4.0-rc.1 (c (n "edict") (v "0.4.0-rc.1") (d (list (d (n "atomicell") (r "^0.1.8") (d #t) (k 0)) (d (n "edict-proc") (r "=0.3.0") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("ahash"))) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "1qm2xs5yrci4crv1x9m3ly2qh39v5ry2f6hcdpg9s937jwdkkx55") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.4.0-rc.2 (c (n "edict") (v "0.4.0-rc.2") (d (list (d (n "atomicell") (r "^0.1.8") (d #t) (k 0)) (d (n "edict-proc") (r "=0.3.0") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("ahash"))) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "13kziiyzfiqyq6c7va5iwh4qvr3nwmsik890g8pwd099cjdzx71z") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.4.0-rc.4 (c (n "edict") (v "0.4.0-rc.4") (d (list (d (n "alkahest") (r "^0.2.0") (f (quote ("alloc" "fixed32"))) (o #t) (k 0)) (d (n "alkahest-proc") (r "^0.2.0-rc.6") (d #t) (k 2)) (d (n "atomicell") (r "^0.1.9") (d #t) (k 0)) (d (n "edict-proc") (r "=0.4.0-rc.4") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("ahash"))) (k 0)) (d (n "nanoserde") (r "^0.1.0") (o #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "06jnb898x9zv9x3bin7hx9prfp11dvlhddrxhc4nsfff0mcalpv5") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.4.0-rc.5 (c (n "edict") (v "0.4.0-rc.5") (d (list (d (n "alkahest") (r "^0.2.0") (f (quote ("alloc" "fixed32"))) (o #t) (k 0)) (d (n "alkahest-proc") (r "^0.2.0-rc.6") (d #t) (k 2)) (d (n "atomicell") (r "^0.1.9") (d #t) (k 0)) (d (n "edict-proc") (r "=0.4.0-rc.4") (d #t) (k 0)) (d (n "flume") (r "^0.10") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("ahash"))) (k 0)) (d (n "nanoserde") (r "^0.1.0") (o #t) (k 0)) (d (n "rayon") (r "^1.5") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "smallvec") (r "^1.8") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "0za644j216dpsyblh334cg6gbww0408z5mlwi481xmalgz9k6smm") (f (quote (("std") ("default" "std"))))))

(define-public crate-edict-0.5.0 (c (n "edict") (v "0.5.0") (d (list (d (n "alkahest") (r "^0.3.0") (f (quote ("alloc" "fixed32"))) (o #t) (k 0)) (d (n "alkahest-proc") (r "^0.3.0") (d #t) (k 2)) (d (n "atomicell") (r "^0.1.9") (d #t) (k 0)) (d (n "edict-proc") (r "=0.5.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.13") (f (quote ("ahash"))) (k 0)) (d (n "nanoserde") (r "^0.1.32") (o #t) (k 0)) (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "rayon") (r "^1.7") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "smallvec") (r "^1.10") (f (quote ("union"))) (k 0)) (d (n "tiny-fn") (r "^0.1.5") (d #t) (k 0)))) (h "0xqxfqhxdsv209x5dlnzddmrj4psacs1iziv155y0z38wp6zfnyq") (f (quote (("std") ("default" "std"))))))

