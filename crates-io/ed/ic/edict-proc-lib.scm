(define-module (crates-io ed ic edict-proc-lib) #:use-module (crates-io))

(define-public crate-edict-proc-lib-0.2.0-rc.1 (c (n "edict-proc-lib") (v "0.2.0-rc.1") (d (list (d (n "proc-easy") (r "^0.2.2") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1ysp8gv99jla7fxi5igsyrcg971p7hhvqgi95z1w4mcn7mqb6iln")))

(define-public crate-edict-proc-lib-0.2.0-rc.2 (c (n "edict-proc-lib") (v "0.2.0-rc.2") (d (list (d (n "proc-easy") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1g01pi1yqpvfq03spq3cchzzp7mkigzfhin8fnchz9kc2qd4wg2g")))

(define-public crate-edict-proc-lib-0.2.0-rc.3 (c (n "edict-proc-lib") (v "0.2.0-rc.3") (d (list (d (n "proc-easy") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pdivsl6hpp5n22mwip3f8j6imx2mk6764z0fqicxh07dis6fl0j")))

(define-public crate-edict-proc-lib-0.3.0 (c (n "edict-proc-lib") (v "0.3.0") (d (list (d (n "proc-easy") (r "^0.2.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zhnlj0i2mw387910kq4ccd1pz86sxh45qgsd4p5ng2l3ksp1v4r")))

(define-public crate-edict-proc-lib-0.4.0-rc.4 (c (n "edict-proc-lib") (v "0.4.0-rc.4") (d (list (d (n "proc-easy") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1f9drszzssn43ad7x0rzc663nky4cbwviz9mi5n1q02lziq2nrrc")))

(define-public crate-edict-proc-lib-0.5.0 (c (n "edict-proc-lib") (v "0.5.0") (d (list (d (n "proc-easy") (r "^0.3.0") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1sps9yn10b55fr9sd1awc8a71cki2bpz8p6qnxz1rxx466cqznaj")))

