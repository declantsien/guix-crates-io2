(define-module (crates-io ed ic edict-proc) #:use-module (crates-io))

(define-public crate-edict-proc-0.2.0-rc.1 (c (n "edict-proc") (v "0.2.0-rc.1") (d (list (d (n "edict-proc-lib") (r "=0.2.0-rc.1") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1bdavahckl8rzm8g5z4jpk2cha840qmmmgxfqw7jfrc8xw9x43l1")))

(define-public crate-edict-proc-0.2.0-rc.2 (c (n "edict-proc") (v "0.2.0-rc.2") (d (list (d (n "edict-proc-lib") (r "=0.2.0-rc.2") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0ic6a780i0rawkflvac6rdj8dj4bbgcdfpc501rzwzjnngidzfpb")))

(define-public crate-edict-proc-0.2.0-rc.3 (c (n "edict-proc") (v "0.2.0-rc.3") (d (list (d (n "edict-proc-lib") (r "=0.2.0-rc.3") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0dg1cksks3fzpnlxzqzm0rvfcs1z392aaiiq3hab171x3p764016")))

(define-public crate-edict-proc-0.3.0 (c (n "edict-proc") (v "0.3.0") (d (list (d (n "edict-proc-lib") (r "=0.3.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1rzbanwmbglzzkxpn9a0qizvxaz3aja418sabpzlwzw64n0cjmgv")))

(define-public crate-edict-proc-0.4.0-rc.4 (c (n "edict-proc") (v "0.4.0-rc.4") (d (list (d (n "edict-proc-lib") (r "=0.4.0-rc.4") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1lri5glik3h3h0syxn5q7kjww0pw2knfhhvk4lgmp091pawvi6r6")))

(define-public crate-edict-proc-0.5.0 (c (n "edict-proc") (v "0.5.0") (d (list (d (n "edict-proc-lib") (r "=0.5.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "1wdrm6sczssaj7gn1c48ncfwbinnyfxmb55vha80l9851zf80kf9")))

