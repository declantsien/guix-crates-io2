(define-module (crates-io ed m- edm-orm) #:use-module (crates-io))

(define-public crate-edm-orm-0.1.0 (c (n "edm-orm") (v "0.1.0") (d (list (d (n "apache-avro") (r "^0.16.0") (d #t) (k 0)) (d (n "edm-core") (r "^0.1.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.107") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "00h0kqwp293gj10crap8bv750q36cppxb5db6mryww4dmbrlwc1i")))

