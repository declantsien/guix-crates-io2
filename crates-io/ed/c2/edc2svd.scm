(define-module (crates-io ed c2 edc2svd) #:use-module (crates-io))

(define-public crate-edc2svd-0.1.0 (c (n "edc2svd") (v "0.1.0") (d (list (d (n "fern") (r "^0.5.7") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "0zfs5x8xgpmh9mlz7dbmc15qmzf4h1bq8k34f5vxjfvjgnpp0rks")))

(define-public crate-edc2svd-0.2.0 (c (n "edc2svd") (v "0.2.0") (d (list (d (n "fern") (r "^0.5.7") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "0cg2wl8rkmawc8ygdsm7ixs0qw83qyyxi5av11jbgkwa623964ma")))

(define-public crate-edc2svd-0.3.0 (c (n "edc2svd") (v "0.3.0") (d (list (d (n "fern") (r "^0.5.7") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "00zqnhpfqhb3daklbbymgvii8sx235n1sjzcvpkp9zg2p5gj7b4k")))

(define-public crate-edc2svd-0.3.1 (c (n "edc2svd") (v "0.3.1") (d (list (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "0ng0r9633l4g8dnpkv915rldm7qlbwz6g0kkj6g8ycx7cvxx168q")))

(define-public crate-edc2svd-0.4.0 (c (n "edc2svd") (v "0.4.0") (d (list (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "xmltree") (r "^0.8.0") (d #t) (k 0)))) (h "0h2nnh31yj8chqgm3cc707fk59a4wm9rjgyqr64pwihhw2rfjb7c")))

(define-public crate-edc2svd-0.5.0 (c (n "edc2svd") (v "0.5.0") (d (list (d (n "fern") (r "^0.6.1") (d #t) (k 0)) (d (n "getopts") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4.17") (d #t) (k 0)) (d (n "xmltree") (r "^0.10.3") (d #t) (k 0)))) (h "0g4siyv60mzf5d1cpkxg2287q2vbb6sh1dh9bgzw3y6q5csl33xf")))

