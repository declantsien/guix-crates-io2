(define-module (crates-io ed tu edtui-jagged) #:use-module (crates-io))

(define-public crate-edtui-jagged-0.1.0 (c (n "edtui-jagged") (v "0.1.0") (h "1rpsz4kkzx5l5l2z939mykga87ks259n20brqwm33zpynp9jbflz")))

(define-public crate-edtui-jagged-0.1.1 (c (n "edtui-jagged") (v "0.1.1") (h "0rhybcy374d3jyw5wrilckcvvw4iaxv15q7xh4n3l0x3icxss5i2")))

(define-public crate-edtui-jagged-0.1.2 (c (n "edtui-jagged") (v "0.1.2") (h "01ffilajd96q3n3sdbwvrph5k328167m8rydm48kgbd7dra64m9x")))

(define-public crate-edtui-jagged-0.1.3 (c (n "edtui-jagged") (v "0.1.3") (h "14npxjnchlsymb2pqqyn3wcbscf7xlp27ywbk4n4w6l82k46v8p7")))

