(define-module (crates-io ed ce edcert-letter) #:use-module (crates-io))

(define-public crate-edcert-letter-0.1.0 (c (n "edcert-letter") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^4.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1p316cw6ymrq4bl2cw1c1c3cmab5qjmmpsap9gj3xnls5gds9lhr")))

(define-public crate-edcert-letter-0.1.1 (c (n "edcert-letter") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^4.1") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "119s0xjv90isb7xfd3b6galsbmq242kmyjdzsxhzcz41bcf6ilga")))

(define-public crate-edcert-letter-1.0.0 (c (n "edcert-letter") (v "1.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^5.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0sg7ki429cdcmz8rwssrwbp74a0p44c7f2sqmfakl3npkpn4kp7n")))

(define-public crate-edcert-letter-1.0.1 (c (n "edcert-letter") (v "1.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^6.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "11flvmz0bmv9hj7vd64b61c7kwv3jasz76gw2xl3b3yv5zlwgcc2")))

(define-public crate-edcert-letter-1.0.2 (c (n "edcert-letter") (v "1.0.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^7.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0nkrms2l7mnhz4whnk7nrby4h45gnk0if70fir3ggl53ry0yx2w1")))

(define-public crate-edcert-letter-1.0.3 (c (n "edcert-letter") (v "1.0.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^8.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "04jwi2awdlcwjzrw5dnzmyc8y2chg25la4c8f3xza92lswwgm6ix")))

(define-public crate-edcert-letter-1.0.4 (c (n "edcert-letter") (v "1.0.4") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^8.0") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0w58sb0pf51whw6bdmyviq5p8lsx3zmnfbl8hk05z06wazgbn5lp")))

(define-public crate-edcert-letter-1.1.0 (c (n "edcert-letter") (v "1.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^8.0") (d #t) (k 0)))) (h "1k9yl4i3if3ik6m0llphx703prpcarjrvfszrs74fnwc3g8yywgr")))

(define-public crate-edcert-letter-2.0.0 (c (n "edcert-letter") (v "2.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^9.0") (d #t) (k 0)))) (h "0f94n28fx4ngipji21ddzf4gbxm17y8hrdwvwghq9br7jgc6p808")))

