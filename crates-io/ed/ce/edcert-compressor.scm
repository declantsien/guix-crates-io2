(define-module (crates-io ed ce edcert-compressor) #:use-module (crates-io))

(define-public crate-edcert-compressor-0.1.0 (c (n "edcert-compressor") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^4.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "19ca0w8lg3rdl0ky9cg1x6yi2frk51alq3llq9yd6d74xx056rm3")))

(define-public crate-edcert-compressor-0.1.1 (c (n "edcert-compressor") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^4.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "04izvs28mlg9xsk5c0qqnrnbwymxl1ncpys2vmb9qhgzl52wrh9c")))

(define-public crate-edcert-compressor-1.0.0 (c (n "edcert-compressor") (v "1.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^5.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1xyl9v92mwzi1lay5bn0spnfyrkyq5w3m0s63wqkyb7n92f4w7bd")))

(define-public crate-edcert-compressor-1.0.1 (c (n "edcert-compressor") (v "1.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^6.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1frxsa2wg2pw16f9fppsj0b1wbynk4lcglgq2kccb3gxpm9fnrph")))

(define-public crate-edcert-compressor-1.1.0 (c (n "edcert-compressor") (v "1.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^6.1") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "038m3a2f8b7sy3ra6dbzyisj38f6lxcfql3ic5yid43m8bwy4vd1")))

(define-public crate-edcert-compressor-1.1.1 (c (n "edcert-compressor") (v "1.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^6.1") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0nbr54qrqbk3cv6grnflpwmjy6v3cydai9nkj05j2y626m6w7xq8")))

(define-public crate-edcert-compressor-1.1.2 (c (n "edcert-compressor") (v "1.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^7.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0hyfd6b9k22wwilzhr1fgilzjapw2rckvckdmkc1wba90kqh853v")))

(define-public crate-edcert-compressor-1.1.3 (c (n "edcert-compressor") (v "1.1.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^7.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "01i9kcyhw76rb2cri31lbff6d8rkrrqqfxvk79w0f1gdbdgv3brl")))

(define-public crate-edcert-compressor-1.1.4 (c (n "edcert-compressor") (v "1.1.4") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^8.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1sf356dk6addrfygjr2kqqmcbadnzmmaklgd3pdrjiln9vxrjf21")))

(define-public crate-edcert-compressor-1.1.5 (c (n "edcert-compressor") (v "1.1.5") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^8.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.2") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1zcr3dr6sffnhhxcvz4n4dp3qhv4ja81vxrrrqjp1686ygmrhrxp")))

(define-public crate-edcert-compressor-1.1.6 (c (n "edcert-compressor") (v "1.1.6") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^9.0") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "semver") (r "^0.5.1") (d #t) (k 0)))) (h "1x4xxakr7sq6rl51jixqk72la6795fg7dawbavckng8xarc5fwyx")))

