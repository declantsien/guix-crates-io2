(define-module (crates-io ed ce edcert-restrevoke) #:use-module (crates-io))

(define-public crate-edcert-restrevoke-0.1.0 (c (n "edcert-restrevoke") (v "0.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^4.0") (d #t) (k 0)) (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0nfmmg53l60mda8ag100vvp8sxbf8fqj9mscj6kq1va3dqph9nww")))

(define-public crate-edcert-restrevoke-0.1.1 (c (n "edcert-restrevoke") (v "0.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^4.1") (d #t) (k 0)) (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "166sy969b4swjf18blhkyr5kyc60jm9xhlmnjirab60h7fw72gil")))

(define-public crate-edcert-restrevoke-0.1.2 (c (n "edcert-restrevoke") (v "0.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^4.1") (d #t) (k 0)) (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "185c9v6zjkzk659dil5f4cp8rrfiz1bxdxa59hr1jsgq20ngxpaj")))

(define-public crate-edcert-restrevoke-1.0.0 (c (n "edcert-restrevoke") (v "1.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^5.0") (d #t) (k 0)) (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1qzw04ddrpya2wdf3ls0vfsq1d897p2fs4sf3iv6jl7qd3kxng2v")))

(define-public crate-edcert-restrevoke-1.0.1 (c (n "edcert-restrevoke") (v "1.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^6.0") (d #t) (k 0)) (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0adh9apjl97lkplpry5wr4rxydq0a7dab43i3lq9fhyzmdvaybjk")))

(define-public crate-edcert-restrevoke-1.0.2 (c (n "edcert-restrevoke") (v "1.0.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^7.0") (d #t) (k 0)) (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "03yjkjg8krk830kk8nfz5gdxg81a1n3mz4n4i6a45zbyzwpbryph")))

(define-public crate-edcert-restrevoke-1.0.3 (c (n "edcert-restrevoke") (v "1.0.3") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^8.0") (d #t) (k 0)) (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "0zfiz5cf2gpv3c6grlv66svg4a1bghf2r4wpj9x91m3kk25dkff3")))

(define-public crate-edcert-restrevoke-1.0.4 (c (n "edcert-restrevoke") (v "1.0.4") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^8.0") (d #t) (k 0)) (d (n "hyper") (r "~0.8") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1vkd3m0nmhgv72slk0w6fhdwziifack1pb1q77bbkk97dsf8bk7l")))

(define-public crate-edcert-restrevoke-1.0.5 (c (n "edcert-restrevoke") (v "1.0.5") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^9.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1bmix0vwpqdjn81q7xf6l0wzhpimg59wcr7q03k0vmda87wyzpgb") (y #t)))

(define-public crate-edcert-restrevoke-1.0.6 (c (n "edcert-restrevoke") (v "1.0.6") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "edcert") (r "^9.0") (d #t) (k 0)) (d (n "hyper") (r "^0.9.10") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)))) (h "1f01fszdlskfl8csigcz4dhb0zpp7fnz7ic057spavq5p8y1ghqr")))

