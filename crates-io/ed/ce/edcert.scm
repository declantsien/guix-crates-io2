(define-module (crates-io ed ce edcert) #:use-module (crates-io))

(define-public crate-edcert-2.0.1 (c (n "edcert") (v "2.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1wsc4y1aw37d5i34mir70fgkq88w1af7y01lyf0clxrrv88xmdwa")))

(define-public crate-edcert-2.0.2 (c (n "edcert") (v "2.0.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "12nm1y3nmzx9g10lqy72h7cfsl4mq77kyp974szjaws3ncfibcjz")))

(define-public crate-edcert-2.1.1 (c (n "edcert") (v "2.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1n3gmn2zwja4s0ihh5y54sgpgyay6dj3p3n2ilwfikbxpjjkjglr")))

(define-public crate-edcert-2.1.2 (c (n "edcert") (v "2.1.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1asaala891r66cg50baqvfyq75dv5syp795753qdkldigsqi3cag")))

(define-public crate-edcert-3.0.0 (c (n "edcert") (v "3.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "07lj3zff67vzrhkq0fni24kwsyp8n5qbfsj91nd3cl755380j1nf")))

(define-public crate-edcert-3.0.1 (c (n "edcert") (v "3.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "hyper") (r "^0.7") (d #t) (k 0)) (d (n "rust-lzma") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "19n2d3afyn2i7y90y514h97dlraf52zmvpv6rlw1a25hcwkm3jjl")))

(define-public crate-edcert-4.0.0 (c (n "edcert") (v "4.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "11sizf4c2cgw9yd7l6rp2fb1f9by8y0a25k6cj792cd02gml43da")))

(define-public crate-edcert-4.1.0 (c (n "edcert") (v "4.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1fw305fp2clr72wqzv2lmdkc2ax17vri50g80cpav39hw6ggkm41")))

(define-public crate-edcert-4.1.1 (c (n "edcert") (v "4.1.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1n35v9cv1vzkm5i23brf31cl6ak9zbp5m4slacff7jbxcdbknb30")))

(define-public crate-edcert-5.0.0 (c (n "edcert") (v "5.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "1x1irg21aigsrxs7ig7s2h3jdf3x13xlq66823vmiyk9zg59vq6g")))

(define-public crate-edcert-6.0.0 (c (n "edcert") (v "6.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0za24bh5hwmsx9n0w5n8klqypga6mbb3sdqibqaq528n9is7y9bg")))

(define-public crate-edcert-6.0.1 (c (n "edcert") (v "6.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "105rm4h3wf434j11fvw0qm9ld31dgz89v4bqq4jqfxwzqxfpg5m2")))

(define-public crate-edcert-6.0.2 (c (n "edcert") (v "6.0.2") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0pvhfcr0xpxc3v24i6fl2vf9h0m4s8kskvhbfgaksnqhgckrb8rd")))

(define-public crate-edcert-6.1.0 (c (n "edcert") (v "6.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0gbmr1gwy7symrsq4cpxa68f8g5gwxxflld0kvm5ln1zzdxs307p")))

(define-public crate-edcert-7.0.0 (c (n "edcert") (v "7.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0i648s1y9blgq9aaw1ydc4rhld9vwvlgxgbaqx0xagghx6g23s9x")))

(define-public crate-edcert-8.0.0 (c (n "edcert") (v "8.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "0l0dlvcjd3wzl42zqza54hbq7a7fbp26h6kcgbv5x77y55yhbrsi")))

(define-public crate-edcert-8.0.1 (c (n "edcert") (v "8.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "12yfy38xkr1xcphk0rgqy8cx1ic7dfsvx41rp1z41cmy8xv270jr")))

(define-public crate-edcert-8.1.0 (c (n "edcert") (v "8.1.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)) (d (n "time") (r "^0.1") (d #t) (k 0)))) (h "16yxamsvpa87kpvsafydpavhbvbbfj06j2gikysb88bz9s48xbm7")))

(define-public crate-edcert-8.2.0 (c (n "edcert") (v "8.2.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.9") (d #t) (k 0)))) (h "0xk4bbi04bxz8hwxzb609qyi6n9vn8myyhv6n2r468r6mh4p5ckz")))

(define-public crate-edcert-9.0.0 (c (n "edcert") (v "9.0.0") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "secrets") (r "^0.11.1") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.12") (d #t) (k 0)))) (h "01i10xnx9xldnkwcl5wznanp0hg5yh9f0fxibhdgr0fcwdph3ms5")))

(define-public crate-edcert-9.0.1 (c (n "edcert") (v "9.0.1") (d (list (d (n "chrono") (r "^0.2") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3") (d #t) (k 0)) (d (n "secrets") (r "^0.11.1") (d #t) (k 0)) (d (n "sodiumoxide") (r "^0.0.12") (d #t) (k 0)))) (h "1lc3dcc2jxjyfn3kpmv697lxgirccarwiz3xp4l36vjm4q29cpq4")))

