(define-module (crates-io ed mu edmunge) #:use-module (crates-io))

(define-public crate-edmunge-1.0.0 (c (n "edmunge") (v "1.0.0") (d (list (d (n "clap") (r "^2.31.2") (d #t) (k 0)) (d (n "executable-path") (r "^1.0.0") (d #t) (k 2)) (d (n "regex") (r "^1.0.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.0.2") (d #t) (k 0)))) (h "162lx21k7r4823yb9p7h3n3ffiyavz7j8w0plrxhxackzjsbwwgd")))

