(define-module (crates-io ed he edhex) #:use-module (crates-io))

(define-public crate-edhex-0.1.0 (c (n "edhex") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "19d6spp8xzb2a21n9gi8wy9vlhw7nja9g1qbic7ci2rqjxs5clr6")))

(define-public crate-edhex-0.2.0 (c (n "edhex") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1m0qygc0lh7z2xalkq3whvr0dzj8wwdw6cp7ma5y9cn5b1c7mmfi")))

(define-public crate-edhex-0.2.1 (c (n "edhex") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "14band56yaljczyjjsjhkpdnclvqjfprbrq9g92yrpf1106wh7nh")))

(define-public crate-edhex-0.2.2 (c (n "edhex") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1m4kkc7idfva5w294m45w5m3wb9mn06y9rfpms3rv2rdjmscrgq5")))

(define-public crate-edhex-0.2.3 (c (n "edhex") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "19irjw1qnc5mfsbvfzskywihwp669isjdn6n0pxl3hwnrb890ymy")))

(define-public crate-edhex-0.2.4 (c (n "edhex") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "18hzwjkwzsss9y2saww063mgr7h13wfsa1kxb1b0p6c8r84rp3is")))

(define-public crate-edhex-0.2.5 (c (n "edhex") (v "0.2.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0zkjs8ikk382wkqd7d3kscz3zh6l1dch13agfgm57pmp9pzcfd4k")))

(define-public crate-edhex-0.2.6 (c (n "edhex") (v "0.2.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "121zy5cni8x14c4r44qc63qj79ggrakyz5vaa44p6d7s962q3k4x")))

(define-public crate-edhex-0.2.7 (c (n "edhex") (v "0.2.7") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0vf3pffksr4213z4kc18b3wvhvvhn42gqsr71cw6sgddq9zw29gv")))

(define-public crate-edhex-0.2.8 (c (n "edhex") (v "0.2.8") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0c1wb4rvjza4vzk9b99v88626xnxqijc07pljc693cp49akrw2gi")))

(define-public crate-edhex-0.2.9 (c (n "edhex") (v "0.2.9") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0944qp86zill6j6rz9hd0dyl4vjzqlzg5rc7q91v0lswzf5hyz2q")))

(define-public crate-edhex-0.2.10 (c (n "edhex") (v "0.2.10") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "010ar4a6qhp67vbfydanb21x34xbvb9qsssxrr4dxjzsxmrsqncc")))

(define-public crate-edhex-0.2.11 (c (n "edhex") (v "0.2.11") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0s5g0jainq4129vhjh2mac0kk5kk3zzm52dcn9qw36p1iq5zshrj")))

(define-public crate-edhex-0.2.12 (c (n "edhex") (v "0.2.12") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "edhex_core") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1qz3ril9vj79ahiw7bk9i61gdzkv5bmkh3q3nzk4nxk7jhx7n6q8")))

(define-public crate-edhex-0.2.13 (c (n "edhex") (v "0.2.13") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "edhex_core") (r "^0.1.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0yyw7k3xsp3w6lnpsk9b78sw2hlmyhd06q1ici0fhhsdxvbz77ln")))

(define-public crate-edhex-0.3.0 (c (n "edhex") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "edhex_core") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1wqs9xxj4xnjl091jfyj67q4f7gx1cjy926kjnkrqzlvgk0mwgjw")))

(define-public crate-edhex-0.3.1 (c (n "edhex") (v "0.3.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "edhex_core") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "00xygi7y6z1ymy384nkzjpnbldp0p0j02fpaainizb5f7l0wb1y3")))

(define-public crate-edhex-0.3.2 (c (n "edhex") (v "0.3.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "edhex_core") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0bxy6kp6r70cag9744fmd0gc6vwpkdjm6fq8hd4fgbsbrdpgyhza")))

(define-public crate-edhex-0.3.3 (c (n "edhex") (v "0.3.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "edhex_core") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "12mnwp570k0gdqn5ffdsqj5p33q83pr1b81x306y30jcs4362ikp")))

(define-public crate-edhex-0.3.4 (c (n "edhex") (v "0.3.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "edhex_core") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1x1s93qj4jvmi9mvcmddr58j4axqy2ysxsfghnmc81c9w0nrq689")))

(define-public crate-edhex-0.3.5 (c (n "edhex") (v "0.3.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "edhex_core") (r "^0.2.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0jjk36lh27hw083s9rx93x0dvyg3049yj7gxb64dfzgc5h7aj4ml")))

(define-public crate-edhex-0.3.6 (c (n "edhex") (v "0.3.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "edhex_core") (r "^0.2.6") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0k199n8rprx0jvm2i0cvfbsi884g9fiij7dwjshjk4m6xpyq9dyp")))

(define-public crate-edhex-0.3.7 (c (n "edhex") (v "0.3.7") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^0.3.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1pj6959x6s339vs8d7aymk6f537v28wc7bh7is3n8md3qdlihvy6")))

(define-public crate-edhex-0.3.8 (c (n "edhex") (v "0.3.8") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^0.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0385ad8x4afa30wvpsy2r6xmvgs0msp52misfh9mi6llgqp1j3k7")))

(define-public crate-edhex-0.3.9 (c (n "edhex") (v "0.3.9") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1z4za0s9dilsnli9i2igdx5ghd8c6a1d2abfqrzanmhqry6022qq")))

(define-public crate-edhex-0.3.10 (c (n "edhex") (v "0.3.10") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^0.5.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1ypp45sh8jiqyq4nvz5w402qvadszjshmb6h8kbhrlhk9cf1k1bh")))

(define-public crate-edhex-0.3.11 (c (n "edhex") (v "0.3.11") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^0.5.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0r0g7rlldbxava65aan7m8y0ad9kkjcfkvm87q3cnn388j78hsia")))

(define-public crate-edhex-0.3.12 (c (n "edhex") (v "0.3.12") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^0.6.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1684sabg5zxjlg02pq6882z07px9n4fyj7smgpnsa3l56nlgs5aq")))

(define-public crate-edhex-0.3.13 (c (n "edhex") (v "0.3.13") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^0.6.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1magqv7cw1zla7490n8wnla28d2iwc8q4kzr6b1fncn4gw07zwmz")))

(define-public crate-edhex-0.4.0 (c (n "edhex") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^0.7.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "16xqp2v77ypmv93017pik0cd7xc5gr5z157jyq48wk14j62vhvry")))

(define-public crate-edhex-0.4.1 (c (n "edhex") (v "0.4.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^0.7.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "1bmzsmd1bfb2fs9sfzvyg8446zxs1sm6d37mv4dkjwqncwjfiw14")))

(define-public crate-edhex-0.4.2 (c (n "edhex") (v "0.4.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^0.7.2") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "06c5ibsv592mxl79s0r1vf9hxk2cv7lidjkyf9nyl8425imxxz2r")))

(define-public crate-edhex-1.0.0 (c (n "edhex") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "clap") (r "^2.27.0") (d #t) (k 0)) (d (n "edhex_core") (r "^1.0.0") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)))) (h "0r58hp7m93ni8bj4sl8l5r3hfhp73rcz0jrggxwkjcmxihmnsyzl")))

