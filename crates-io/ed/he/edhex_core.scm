(define-module (crates-io ed he edhex_core) #:use-module (crates-io))

(define-public crate-edhex_core-0.1.0 (c (n "edhex_core") (v "0.1.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)))) (h "08pmcxs35c5pqmnv2h2ih4aprh72qpvhlv4p4r5376s4cxga2hih")))

(define-public crate-edhex_core-0.1.1 (c (n "edhex_core") (v "0.1.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1ssrjaq5c7zbr5aj8k80k06i0zwhvw4rkx21md5qk9ydlnm9cwy4")))

(define-public crate-edhex_core-0.2.0 (c (n "edhex_core") (v "0.2.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1a24hpxbbhpnds36w0lwz9hplz9p4231jybl71blcc7ncgnkycmm")))

(define-public crate-edhex_core-0.2.1 (c (n "edhex_core") (v "0.2.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1sln46infh420nyivxqw4rar6d8z3i4ai6z5rymw627skyvvllqf")))

(define-public crate-edhex_core-0.2.2 (c (n "edhex_core") (v "0.2.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "06iij5ah5wsbss9iq1npfx3dqyzjbz83y8y0yngjdsi0d8h48222")))

(define-public crate-edhex_core-0.2.3 (c (n "edhex_core") (v "0.2.3") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0kxs1k3zmzssz48vi0ziw15fccc849dm8hy2l50q65ny1zshkw0i")))

(define-public crate-edhex_core-0.2.4 (c (n "edhex_core") (v "0.2.4") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0gvj4rn7z5d8mkcmiys4yx6jz7qz43q540jdvm77sjccbhy85gw6")))

(define-public crate-edhex_core-0.2.5 (c (n "edhex_core") (v "0.2.5") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0cyvf6bdp0w7kf8c054imlj9khdm3gd4dngpxzbp9gi4q57dl8k3")))

(define-public crate-edhex_core-0.2.6 (c (n "edhex_core") (v "0.2.6") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1rh7cp10lgwnfsl8qhpr52xcmx6i2k468600dbfclq4f9622j24h")))

(define-public crate-edhex_core-0.3.0 (c (n "edhex_core") (v "0.3.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0yrkmhib0xazn0yn9hh58jjya9gz9ijyvqbyqsi750gazy4a30gi")))

(define-public crate-edhex_core-0.4.0 (c (n "edhex_core") (v "0.4.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "165y95mnm2mcmz5vs9mbv8w95j2cza6flxs1xj19kpp8w5ivx22g")))

(define-public crate-edhex_core-0.5.0 (c (n "edhex_core") (v "0.5.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0043smlczk4y23p8ks2pr5sznbxmbqzkyhv2b1nd79gjdb1n2imj")))

(define-public crate-edhex_core-0.5.1 (c (n "edhex_core") (v "0.5.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1zxa0bdifdb4md2bzcygq0y4vqv1y5l9sckbs56h1pmvny1qsxxp")))

(define-public crate-edhex_core-0.5.2 (c (n "edhex_core") (v "0.5.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "08jla4cm8d4x3gfd432ql8f2xpm0576mkh7swl1m4qvq90dqsqgq")))

(define-public crate-edhex_core-0.6.0 (c (n "edhex_core") (v "0.6.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0m7gfci21qyjzy4qa1mhw3gfb9mwan2lxpxi34gb8byxbl8mfciq")))

(define-public crate-edhex_core-0.6.1 (c (n "edhex_core") (v "0.6.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0y1rdkhykzdla48g8pznzblbz0r7cww2fli7q60iy9dvycsvdzlj")))

(define-public crate-edhex_core-0.7.0 (c (n "edhex_core") (v "0.7.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1zgh5zv427krslwmrxr9d890s62fygz7h4cmlhymnxj61cprb9s2")))

(define-public crate-edhex_core-0.7.1 (c (n "edhex_core") (v "0.7.1") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "0wcr1fxwcjwq0fl6abaz8cahka7nalnwjmv1cgh1k6ack5s1rgsd")))

(define-public crate-edhex_core-0.7.2 (c (n "edhex_core") (v "0.7.2") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "1n86jl5bfr7abivsvrldapx11v62jdbfa6xd0yfqadlfwksxx59f")))

(define-public crate-edhex_core-1.0.0 (c (n "edhex_core") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.12.1") (d #t) (k 0)) (d (n "regex") (r "^1.4.5") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "unicode-segmentation") (r "^1.6.0") (d #t) (k 0)))) (h "04hv928axi638ki215dn1cw5z3kyghqpgh0fg09xsl7zd20q2668")))

