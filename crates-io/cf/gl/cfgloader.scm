(define-module (crates-io cf gl cfgloader) #:use-module (crates-io))

(define-public crate-cfgloader-0.1.0 (c (n "cfgloader") (v "0.1.0") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "polyglot") (r "^0.2.1") (f (quote ("json_fmt" "toml_fmt" "yaml_fmt"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "0gj4hw8zh90aniwhh58w7ipr0yczs4zjzl8xjymzlxwf0gwv8hbr")))

(define-public crate-cfgloader-0.1.1 (c (n "cfgloader") (v "0.1.1") (d (list (d (n "dirs") (r "^4") (d #t) (k 0)) (d (n "polyglot") (r "^0.2.1") (f (quote ("json_fmt" "toml_fmt" "yaml_fmt"))) (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "snafu") (r "^0.6") (d #t) (k 0)))) (h "1lmfzs4ffy8hf6l6zvqlvlb80w2m5wr16p6wf2vwmk7sgkzjxf00")))

