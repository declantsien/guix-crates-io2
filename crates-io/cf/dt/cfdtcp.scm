(define-module (crates-io cf dt cfdtcp) #:use-module (crates-io))

(define-public crate-CFDTCP-0.1.0 (c (n "CFDTCP") (v "0.1.0") (d (list (d (n "aho-corasick") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "roaring") (r "^0.10.1") (d #t) (k 0)))) (h "15nznbfxjzazd5d8x0wcclhxqdxcmyry5ad5xx05lhzra0z7g2fa")))

(define-public crate-CFDTCP-0.1.1 (c (n "CFDTCP") (v "0.1.1") (d (list (d (n "aho-corasick") (r "^1.0.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "roaring") (r "^0.10.1") (d #t) (k 0)))) (h "06m1klc478mkwlhvv1ma8zf9cyik5g8jfgmnrnmljqnghznadb8w")))

