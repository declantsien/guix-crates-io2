(define-module (crates-io cf ap cfap_screen) #:use-module (crates-io))

(define-public crate-cfap_screen-1.0.0 (c (n "cfap_screen") (v "1.0.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "05jh371j7rabzs4w2g3dan78ah168s9c3ixxcpkl1rpdxkgbqj2q")))

(define-public crate-cfap_screen-2.0.0 (c (n "cfap_screen") (v "2.0.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "1qwg32jc7q6jczyf12i5hriiyichiq47ca4pskvvcl6yzxf7zv7l")))

(define-public crate-cfap_screen-2.0.1 (c (n "cfap_screen") (v "2.0.1") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "1zh33plwp33hqdncssimgspzv4zyk15vn7s0k8zsphbzypdkf9k3")))

(define-public crate-cfap_screen-3.0.0 (c (n "cfap_screen") (v "3.0.0") (d (list (d (n "embedded-hal") (r "^0.2.1") (f (quote ("unproven"))) (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "volatile-register") (r "^0.2") (d #t) (k 0)))) (h "0an2c2cac33fmfj408isqybzafd27wp1wl5svalyn7pg7n1w4k3d")))

