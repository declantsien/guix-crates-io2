(define-module (crates-io cf g- cfg-match) #:use-module (crates-io))

(define-public crate-cfg-match-0.2.0 (c (n "cfg-match") (v "0.2.0") (h "1sv2w0r3jrms8lb1i98cgs4gfs2g2j5yzkq93dqc2am1c9ibz64z")))

(define-public crate-cfg-match-0.2.1 (c (n "cfg-match") (v "0.2.1") (h "0d3v9qsin7jh7mml9ac47hwpcwag58zwfc19vkv5pf1fz5py8041")))

