(define-module (crates-io cf g- cfg-expr) #:use-module (crates-io))

(define-public crate-cfg-expr-0.1.0 (c (n "cfg-expr") (v "0.1.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.1.0") (d #t) (k 0)))) (h "0ya5s06s0awy4mg99n70jphns76h688d603cggc9m59pxsxd7rix")))

(define-public crate-cfg-expr-0.2.0 (c (n "cfg-expr") (v "0.2.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.1.0") (d #t) (k 0)))) (h "0fx8azkdq2kn6252kvsw4xgzv4wbzv0va2ny0413v7rmyky2478r")))

(define-public crate-cfg-expr-0.2.1 (c (n "cfg-expr") (v "0.2.1") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.1.0") (d #t) (k 0)))) (h "1b2pwapi153s2ysfy6fzxd2r6cylbisvbjnzxcpsjnv7qs1yg5r2")))

(define-public crate-cfg-expr-0.3.0 (c (n "cfg-expr") (v "0.3.0") (d (list (d (n "difference") (r "^2.0.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.1.0") (d #t) (k 0)))) (h "1sckbwjr84lfvasm187l9fxp1kdv1dvvq82b3xnqs1ify62y7zkd")))

(define-public crate-cfg-expr-0.4.0 (c (n "cfg-expr") (v "0.4.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1v69hc4fjzf2i798fvi2g2v512bmh6bgp1py7qx3sv7wi6x95qsc") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.4.1 (c (n "cfg-expr") (v "0.4.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1zy58aj0ca1gs3zbg3vf97j8yswc6al5kbkpk9k000l20rpyfavc") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.5.0 (c (n "cfg-expr") (v "0.5.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0wk5kg1yw0c79xbky2scngb9ql9rqx9q8yaplf29r8pq6411036d") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.5.1 (c (n "cfg-expr") (v "0.5.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (o #t) (d #t) (k 0)))) (h "0pbbz5qb0f0yd6vpq55jyxccjl5mqjyvn7dmgxxdfg6v5rljjc6g") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.6.0 (c (n "cfg-expr") (v "0.6.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.4") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (o #t) (d #t) (k 0)))) (h "01dwdxy1r83s6v0mdfjpv6dh13kq8aml00nsvif1yrjqrgv9qkyb") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.7.0 (c (n "cfg-expr") (v "0.7.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11") (o #t) (d #t) (k 0)))) (h "1ajb02mrfairqwbw5wkxm1w0v4a428gcd0q9ng1zblzc0av9awhf") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.7.1 (c (n "cfg-expr") (v "0.7.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "0npxb030pf61i6awh39bbzaigkxdmjpjngqdpw7h8mn61xjbk09b") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.7.2 (c (n "cfg-expr") (v "0.7.2") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "163mv9bi39qgn6hx30nm7hjdxxgkb9rjgbqjzijsyb9q2icimjwh") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.7.3 (c (n "cfg-expr") (v "0.7.3") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "1bqa10s2419yxdpa92sfbrnxanybcqz8p1z86412dm8d17hnyazj") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.7.4 (c (n "cfg-expr") (v "0.7.4") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.11.2") (o #t) (d #t) (k 0)))) (h "0sn2mw7s2i9qr5mrbyarz0mn5vb3iv6z656va6sccf5qzcprxaih") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.8.0 (c (n "cfg-expr") (v "0.8.0") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "152z106dfranvqalpd51apc9rv7zkd5g0pdbr925vpdvcqkx38bh") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.8.1 (c (n "cfg-expr") (v "0.8.1") (d (list (d (n "difference") (r "^2.0") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.1") (o #t) (d #t) (k 0)))) (h "03lgv8psc2qrr93hxgdfmfwbj1crpzghxd7qh6w2nz0l4qryh4ml") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.9.0 (c (n "cfg-expr") (v "0.9.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.2") (o #t) (d #t) (k 0)))) (h "0wbmpnf6mwda8iz4370sxilq9gxiv621gdv4glpy7z6i4nb0pbpd") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.9.1 (c (n "cfg-expr") (v "0.9.1") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "1xra4gpp8h825zf6rajk6wy3zskbqsnakm7fni6azk4ay9cxyc9l") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.10.0 (c (n "cfg-expr") (v "0.10.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "1c594ydkmhmxdl06gignx111kpabnxd2psc8823nh07ybl9i6bga") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.10.1 (c (n "cfg-expr") (v "0.10.1") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.6") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "1kdmljn6dhqhbbk12i5hgfpxpzbl7rimwfmjq3z2a2m632wnwnr9") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.10.2 (c (n "cfg-expr") (v "0.10.2") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "1idny2rgjf4x8ka76k9y5jgmky49bxnc2vc476s1bg3bh2r8q1jy") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.10.3 (c (n "cfg-expr") (v "0.10.3") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.3") (o #t) (d #t) (k 0)))) (h "1nw50j1sl6q96067399r1c6ppwp483q6vvmqdsnv493cv7sarb0a") (f (quote (("targets" "target-lexicon") ("default"))))))

(define-public crate-cfg-expr-0.11.0 (c (n "cfg-expr") (v "0.11.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.4") (o #t) (d #t) (k 0)))) (h "1ai8cgpmf9b6v9miivqzyi0jxh26vy28w55whqxcm5dj09j7lddh") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.58.0")))

(define-public crate-cfg-expr-0.12.0 (c (n "cfg-expr") (v "0.12.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.5") (o #t) (d #t) (k 0)))) (h "1vxbyvni78skh06665a2cf09lwrbxkvkdyryrh1b59lhcazi7g0b") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.58.0")))

(define-public crate-cfg-expr-0.13.0 (c (n "cfg-expr") (v "0.13.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.5") (o #t) (d #t) (k 0)))) (h "1g92hrai07lbs19ciy4vw00d7kcawfgnf6jkd5rw9v4rfhynh9x3") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.58.0")))

(define-public crate-cfg-expr-0.14.0 (c (n "cfg-expr") (v "0.14.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.5") (o #t) (d #t) (k 0)))) (h "1im4f8zrd0gaflv4sxjr2vdn2z4600lyhz32bjc342llc5a2anx3") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.58.0")))

(define-public crate-cfg-expr-0.15.0 (c (n "cfg-expr") (v "0.15.0") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.5") (o #t) (d #t) (k 0)))) (h "1w6c1z3krd1gllblkb31jf82w0zlxbrmb61l72axb7gq20mzlbvk") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.58.0")))

(define-public crate-cfg-expr-0.15.1 (c (n "cfg-expr") (v "0.15.1") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.5") (o #t) (d #t) (k 0)))) (h "1jb8fa0c892r8a4pvikf0c08hhq8lgmpmz7m5k3qb93d53qhqyf8") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.58.0")))

(define-public crate-cfg-expr-0.15.2 (c (n "cfg-expr") (v "0.15.2") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.5") (o #t) (d #t) (k 0)))) (h "0fj5vpm65icq0dywfrx3c551zz3bzq82gwk20n5md84qhv83l3g7") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.58.0")))

(define-public crate-cfg-expr-0.15.3 (c (n "cfg-expr") (v "0.15.3") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.5") (o #t) (d #t) (k 0)))) (h "0744z7sky6kgchfk2ilchb16bjyz7nk8p8zfn3p953y2xir00p11") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.58.0")))

(define-public crate-cfg-expr-0.15.4 (c (n "cfg-expr") (v "0.15.4") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.5") (o #t) (d #t) (k 0)))) (h "1ja914wbplch23cc0z4gxkwgdkivlg9ffdwgvs6c2xai7ghcw35l") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.58.0")))

(define-public crate-cfg-expr-0.15.5 (c (n "cfg-expr") (v "0.15.5") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.11") (o #t) (d #t) (k 0)))) (h "1cqicd9qi8mzzgh63dw03zhbdihqfl3lbiklrkynyzkq67s5m483") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.70.0")))

(define-public crate-cfg-expr-0.15.6 (c (n "cfg-expr") (v "0.15.6") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.11") (o #t) (d #t) (k 0)))) (h "06m4hbpzgjn8m2r8sy5xywrd6ja8d1sjgjwmimwl1610nrbvq031") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.70.0")))

(define-public crate-cfg-expr-0.15.7 (c (n "cfg-expr") (v "0.15.7") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.11") (o #t) (d #t) (k 0)))) (h "07cny7rg58mlzmzw2b1x5b6ib1za9647gklksnlzv9m9cj5qcl7s") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.70.0")))

(define-public crate-cfg-expr-0.15.8 (c (n "cfg-expr") (v "0.15.8") (d (list (d (n "similar-asserts") (r "^1.1") (d #t) (k 2)) (d (n "smallvec") (r "^1.8") (d #t) (k 0)) (d (n "target-lexicon") (r "^0.12.11") (o #t) (d #t) (k 0)))) (h "00lgf717pmf5qd2qsxxzs815v6baqg38d6m5i6wlh235p14asryh") (f (quote (("targets" "target-lexicon") ("default")))) (r "1.70.0")))

