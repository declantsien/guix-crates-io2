(define-module (crates-io cf g- cfg-iif) #:use-module (crates-io))

(define-public crate-cfg-iif-0.2.0 (c (n "cfg-iif") (v "0.2.0") (h "0mab6yvjxn6dqhvpp5ma7461lkxp1i658qazsd9xgllidvah0d9j")))

(define-public crate-cfg-iif-0.2.1 (c (n "cfg-iif") (v "0.2.1") (h "1016jf4ds4apnwf97r2lk39s3ziwd16n0hc1yb320sqz44vqf6y4")))

(define-public crate-cfg-iif-0.2.2 (c (n "cfg-iif") (v "0.2.2") (h "11rwrvk147ahh25k72490kaga6sjm0zhwsf3jx2mjlpmnlw86y5r")))

(define-public crate-cfg-iif-0.2.3 (c (n "cfg-iif") (v "0.2.3") (h "0j127pf024khb6h61dhgjq0wx2ap7qr06ddy3in8pi5l03f8cimj")))

(define-public crate-cfg-iif-0.2.4 (c (n "cfg-iif") (v "0.2.4") (h "12bk7xb4p5zcn9ijk3apk7vf9a041i0b41hrcyc8jlgri6ihsdqp") (r "1.56.0")))

(define-public crate-cfg-iif-0.2.5 (c (n "cfg-iif") (v "0.2.5") (h "0kazh9rasxagmr7zwv4vz8v69f2qckmmdksr9nyqjwhp42h11b78") (r "1.56.0")))

(define-public crate-cfg-iif-0.2.6 (c (n "cfg-iif") (v "0.2.6") (h "0b8ghf46xpgiwyad3i45n97g6bp9lp6yhlypwl6pbr8125hr9106") (r "1.56.0")))

