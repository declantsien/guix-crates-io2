(define-module (crates-io cf g- cfg-if-edc) #:use-module (crates-io))

(define-public crate-cfg-if-edc-1.0.0 (c (n "cfg-if-edc") (v "1.0.0") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "08s31qd7a0syy4j1b7a0sjszr9dl6k9agpk9ivrlyq0w8clwa7mb") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

