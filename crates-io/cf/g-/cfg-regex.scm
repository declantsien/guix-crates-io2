(define-module (crates-io cf g- cfg-regex) #:use-module (crates-io))

(define-public crate-cfg-regex-0.0.0 (c (n "cfg-regex") (v "0.0.0") (d (list (d (n "cfg") (r "^0.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.3") (d #t) (k 0)) (d (n "regex_dfa") (r "^0.4") (d #t) (k 0)))) (h "19kg271y7qdvskj4brkwa3pycva86rvcpn9d6alx95il70vbaf00")))

(define-public crate-cfg-regex-0.1.0 (c (n "cfg-regex") (v "0.1.0") (d (list (d (n "cfg") (r "^0.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)) (d (n "regex_dfa") (r "^0.5") (d #t) (k 0)))) (h "18gyph11r70iifbnfwxxljb0qxv5isnpljdp3qpl6nnnbm0bqv9r")))

(define-public crate-cfg-regex-0.1.1 (c (n "cfg-regex") (v "0.1.1") (d (list (d (n "cfg") (r "^0.3") (d #t) (k 0)) (d (n "regex-syntax") (r "^0.6") (d #t) (k 0)))) (h "0w0w656bxl80x9zwjj65lv8xxcrqgbs0izfjmpj8x9nhgnwhygn9")))

