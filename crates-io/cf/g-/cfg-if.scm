(define-module (crates-io cf g- cfg-if) #:use-module (crates-io))

(define-public crate-cfg-if-0.1.0 (c (n "cfg-if") (v "0.1.0") (h "137qikjcal4h75frzcn6mknygqk8vy5bva7w851aydb5gc6pc7ny")))

(define-public crate-cfg-if-0.1.1 (c (n "cfg-if") (v "0.1.1") (h "0aca4rwz816ivsk0bnf1zdar5jxv3hych1b7d99z1srnd92pvi6h")))

(define-public crate-cfg-if-0.1.2 (c (n "cfg-if") (v "0.1.2") (h "1pj067njlrq5sr86x2l8knhnpin4qmrp2r6c8zgiidky52hikj6l")))

(define-public crate-cfg-if-0.1.3 (c (n "cfg-if") (v "0.1.3") (h "066ya9an8qrlzpxrhabl1kj6qswlia0fl0kima6p2pz6izyiclj0")))

(define-public crate-cfg-if-0.1.4 (c (n "cfg-if") (v "0.1.4") (h "0zmiphlysvcj2a7wqxa69qn208zp14kknqgk1dx7373sw5vwirgg")))

(define-public crate-cfg-if-0.1.5 (c (n "cfg-if") (v "0.1.5") (h "1lzxhzz84b0ssn85lqgmqn1zh8ilxa1fdqc3cj2hvfwf9av7nkhc")))

(define-public crate-cfg-if-0.1.6 (c (n "cfg-if") (v "0.1.6") (h "1x5x48nlf260rgq97r7xnfwin3za9k768gn0kk9wklq0isrbjaq8")))

(define-public crate-cfg-if-0.1.7 (c (n "cfg-if") (v "0.1.7") (h "1x6i0lyf5minisrd20m5ng17pvbl8cp39rjwnkpjx1vf75ak7m0i")))

(define-public crate-cfg-if-0.1.8 (c (n "cfg-if") (v "0.1.8") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1ck67y9z9888vpffdwdj4v65av2rrbvac06dbzxr4w3b9sx1nhw9") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-cfg-if-0.1.9 (c (n "cfg-if") (v "0.1.9") (h "0csygklgz3ybpr0670rkip49zh76m43ar3k7xgypkzbzrwycx1ml")))

(define-public crate-cfg-if-0.1.10 (c (n "cfg-if") (v "0.1.10") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "08h80ihs74jcyp24cd75wwabygbbdgl05k6p5dmq8akbr78vv1a7") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

(define-public crate-cfg-if-1.0.0 (c (n "cfg-if") (v "1.0.0") (d (list (d (n "compiler_builtins") (r "^0.1.2") (o #t) (d #t) (k 0)) (d (n "core") (r "^1.0.0") (o #t) (d #t) (k 0) (p "rustc-std-workspace-core")))) (h "1za0vb97n4brpzpv8lsbnzmq5r8f2b0cpqqr0sy8h5bn751xxwds") (f (quote (("rustc-dep-of-std" "core" "compiler_builtins"))))))

