(define-module (crates-io cf g- cfg-lib) #:use-module (crates-io))

(define-public crate-cfg-lib-0.1.0 (c (n "cfg-lib") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "rustyline") (r "^6") (d #t) (k 0)))) (h "0h3lh692df1sm2rvdgf5ds4yiizf999qkk65m035njqk36192grw")))

(define-public crate-cfg-lib-0.1.1 (c (n "cfg-lib") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "lazy_static") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "06ag0nwlf91xm2dpxb8lpgb1yzm7qrlg81mxj03yfwjwc8hm87ij")))

