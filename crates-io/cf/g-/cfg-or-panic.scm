(define-module (crates-io cf g- cfg-or-panic) #:use-module (crates-io))

(define-public crate-cfg-or-panic-0.1.0 (c (n "cfg-or-panic") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1xzpah0m90r22fc1nsn3nzl5p88f4nqwsgx0j4is4ndgw8fn0nhm") (y #t)))

(define-public crate-cfg-or-panic-0.1.1 (c (n "cfg-or-panic") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "19liav36l0qgcm262kakwak1k68xbn4x34cxg1sq4m8m90wdb1fg")))

(define-public crate-cfg-or-panic-0.2.0 (c (n "cfg-or-panic") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0n3kh3mgs2as0k6ii3a6fi78jscc9l4q6ymmqfv45k2fim9v4z5w")))

(define-public crate-cfg-or-panic-0.2.1 (c (n "cfg-or-panic") (v "0.2.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0bzp6as179jlz41a0vb74w2mbjl0llqx3izkbnbjlzk91j2jrzd2")))

