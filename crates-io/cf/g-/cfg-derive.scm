(define-module (crates-io cf g- cfg-derive) #:use-module (crates-io))

(define-public crate-cfg-derive-0.1.0 (c (n "cfg-derive") (v "0.1.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "131yiyamg0vca3xlcqpgw8h4s8k5pksbbc5a85l463rhx3sj1av4")))

(define-public crate-cfg-derive-0.2.0 (c (n "cfg-derive") (v "0.2.0") (d (list (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("extra-traits"))) (d #t) (k 0)))) (h "1r59ndr6vrd31d88gx53zkwvnmzwznn1rw14rjhav7c45cpv7kfv")))

