(define-module (crates-io cf -r cf-rustracing) #:use-module (crates-io))

(define-public crate-cf-rustracing-1.0.0 (c (n "cf-rustracing") (v "1.0.0") (d (list (d (n "backtrace") (r "^0.3") (o #t) (d #t) (k 0)) (d (n "rand") (r "^0.8.1") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("sync"))) (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("rt" "rt-multi-thread" "macros"))) (d #t) (k 2)) (d (n "trackable") (r "^1.2") (d #t) (k 0)))) (h "1f2my5x3gp1igqd5zchlgha9khhcarmx660305vn0s088yc62dgn") (f (quote (("stacktrace" "backtrace") ("default" "stacktrace"))))))

