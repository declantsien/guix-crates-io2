(define-module (crates-io cf s_ cfs_openpgp) #:use-module (crates-io))

(define-public crate-cfs_openpgp-0.1.0 (c (n "cfs_openpgp") (v "0.1.0") (d (list (d (n "clap") (r "^3.0.0-beta.2") (d #t) (k 0)) (d (n "nettle") (r "^7.0.1") (d #t) (k 0)) (d (n "sequoia-openpgp") (r "^1.1.0") (d #t) (k 0)) (d (n "sha2") (r "^0.9") (d #t) (k 0)))) (h "0pwrwya336i78kikr298bjzpjxn32xqq4nxggbiw488k7k37hnkl")))

