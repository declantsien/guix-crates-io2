(define-module (crates-io cf b8 cfb8) #:use-module (crates-io))

(define-public crate-cfb8-0.1.0 (c (n "cfb8") (v "0.1.0") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "blobby") (r "^0.1") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "1b26s4xwd8lryn051zz3w8jgxbzr52h6hfrm2zb761c1k3zxyhf3") (f (quote (("std"))))))

(define-public crate-cfb8-0.3.0 (c (n "cfb8") (v "0.3.0") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "1mpc5k3fycjk1y87k3dlv6yany7bxbvk1bxhlawgnf1snc6yhzy8")))

(define-public crate-cfb8-0.3.1 (c (n "cfb8") (v "0.3.1") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "0jhnz8f0k97r4q1yy0gq8wivkkcbxzzc2frpscs2rl6xhvzn4qi4")))

(define-public crate-cfb8-0.3.2 (c (n "cfb8") (v "0.3.2") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "0396f5wbijl9kmhmamycicyxrj0q8jqzbjpai58qsnm2czx0lc8v")))

(define-public crate-cfb8-0.4.0 (c (n "cfb8") (v "0.4.0") (d (list (d (n "aes") (r "^0.4") (d #t) (k 2)) (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.4") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "1mj5yhcg2ndhnyg1mpmj0f61kxj0xasb8614k7fjrqddbg2alaxs")))

(define-public crate-cfb8-0.5.0 (c (n "cfb8") (v "0.5.0") (d (list (d (n "aes") (r "^0.5") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.7") (f (quote ("block-cipher"))) (d #t) (k 0)) (d (n "stream-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)))) (h "17lsx1ihk0afna797xi1ilwkrzscg1damql8d70x0nhc1pm1vsfz")))

(define-public crate-cfb8-0.6.0 (c (n "cfb8") (v "0.6.0") (d (list (d (n "aes") (r "^0.6") (d #t) (k 2)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0ix0wni8al7kvszapkx3v996mb10yirchh9q5bk7wdl7j1zc316z")))

(define-public crate-cfb8-0.7.0 (c (n "cfb8") (v "0.7.0") (d (list (d (n "aes") (r "^0.7") (f (quote ("force-soft"))) (d #t) (k 2)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1rmnq0209lmiwxq3d9g0vlszmz6gqqywixhpxgsa75wnn2mvrqj9")))

(define-public crate-cfb8-0.7.1 (c (n "cfb8") (v "0.7.1") (d (list (d (n "aes") (r "^0.7") (f (quote ("force-soft"))) (d #t) (k 2)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "1792bgnsklbqnav9njis199q0rk7970mvkjrlqbyd17j7g2bd963")))

(define-public crate-cfb8-0.8.0 (c (n "cfb8") (v "0.8.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "18abmyzbry5pv7vzgds97bbmn6q0wiyjhy8am43wakd66kwhxjlg") (f (quote (("zeroize" "cipher/zeroize") ("block-padding" "cipher/block-padding")))) (r "1.56")))

(define-public crate-cfb8-0.8.1 (c (n "cfb8") (v "0.8.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "1qd1jjha7m5lv384fw126k3w5s3zppwv4b88djlfdnnh3870lk01") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

