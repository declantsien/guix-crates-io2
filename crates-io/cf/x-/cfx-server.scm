(define-module (crates-io cf x- cfx-server) #:use-module (crates-io))

(define-public crate-cfx-server-0.1.0 (c (n "cfx-server") (v "0.1.0") (d (list (d (n "cfx-core") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1499zndikcv24hh7skd0yy8mhr10yawdfkxingw3crr8ibj2b78m")))

