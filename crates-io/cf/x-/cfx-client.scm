(define-module (crates-io cf x- cfx-client) #:use-module (crates-io))

(define-public crate-cfx-client-0.1.0 (c (n "cfx-client") (v "0.1.0") (d (list (d (n "cfx-core") (r "^0.1.0") (d #t) (k 0)) (d (n "futures") (r "^0.3.14") (d #t) (k 0)) (d (n "rmp-serde") (r "^0.15.4") (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h1scx29nlc96sa28465r1nzj2z13vz2a839ricpq04dh3lhliln")))

