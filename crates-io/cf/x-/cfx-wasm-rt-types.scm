(define-module (crates-io cf x- cfx-wasm-rt-types) #:use-module (crates-io))

(define-public crate-cfx-wasm-rt-types-0.1.0 (c (n "cfx-wasm-rt-types") (v "0.1.0") (d (list (d (n "rmp-serde") (r "^0.15.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "0wdvgpgxb2xs2w9sskd7f7k4ahbsi40ksrh5xbpraxr98xxlvhqb") (f (quote (("in-module" "rmp-serde" "serde") ("default" "in-module"))))))

(define-public crate-cfx-wasm-rt-types-0.2.0 (c (n "cfx-wasm-rt-types") (v "0.2.0") (d (list (d (n "rmp-serde") (r "^0.15.4") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)))) (h "16244j6sqpn1qk54m9mn391vnhmlxwz6kzq7szzgbvivqnxmxpmi") (f (quote (("in-module" "rmp-serde" "serde") ("default" "in-module"))))))

