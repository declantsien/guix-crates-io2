(define-module (crates-io cf x- cfx-addr) #:use-module (crates-io))

(define-public crate-cfx-addr-0.1.0 (c (n "cfx-addr") (v "0.1.0") (d (list (d (n "cfx-types") (r "^0.1.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "rustc-hex") (r "^2.1") (d #t) (k 0)))) (h "1wmaix3nd9ly2dfwaw780lh297d25jb9msrj9bc3g1rfbfxf9471")))

