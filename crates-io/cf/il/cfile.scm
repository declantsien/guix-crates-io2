(define-module (crates-io cf il cfile) #:use-module (crates-io))

(define-public crate-cfile-0.1.0 (c (n "cfile") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "175cd9v01f7zlml5bd0j2z3j6xnknmw3jvyiqwk8wbn6j000mdl3")))

(define-public crate-cfile-0.1.1 (c (n "cfile") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1g0iwa7z3793k02zkgbxmv9rwk77yq1lph7vlyjbfiqq16288nvr")))

(define-public crate-cfile-0.2.0 (c (n "cfile") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0gx11aw8k7k8ggcxphhmx0z7bmmal7nh2y44ad7xjx0550i1cnbr")))

(define-public crate-cfile-0.2.1 (c (n "cfile") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0avbkn395swrs4y1jdajy9g4kx77nm07j5wz0lapwddgpbry0ma2")))

(define-public crate-cfile-0.2.2 (c (n "cfile") (v "0.2.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1y3x0m87n3r02iavpzqdciw1h3802316qm439nw089xak7wlwlr4")))

(define-public crate-cfile-0.2.3 (c (n "cfile") (v "0.2.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0qfkikkl6d5kc94dyvgp71hg8sqykk73p3x6q9w8dgcyps6scw62")))

(define-public crate-cfile-0.4.0 (c (n "cfile") (v "0.4.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("impl-debug" "impl-default" "minwindef" "minwinbase" "winbase" "winnt" "fileapi" "handleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0583gv7pzb6f75vimr9d6nxbv7pqmg5h5syajs65aw68xsgb2a8m") (f (quote (("doc") ("default"))))))

(define-public crate-cfile-0.5.0 (c (n "cfile") (v "0.5.0") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("impl-debug" "impl-default" "minwindef" "minwinbase" "winbase" "winnt" "fileapi" "handleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1mzmblin4m4pxx8s4ffdkh9vg2q9ra9bzahzwf1gsiw4vyjnay7k") (f (quote (("doc") ("default"))))))

(define-public crate-cfile-0.5.1 (c (n "cfile") (v "0.5.1") (d (list (d (n "cfg-if") (r "^0.1") (d #t) (k 0)) (d (n "foreign-types") (r "^0.5") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("impl-debug" "impl-default" "minwindef" "minwinbase" "winbase" "winnt" "fileapi" "handleapi" "processenv"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1lp5w3s3mbha7s4i1851a4jkm66ax5j7daymsljr4aqlhssm3il4") (f (quote (("doc") ("default"))))))

