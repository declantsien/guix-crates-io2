(define-module (crates-io cf il cfile-rs) #:use-module (crates-io))

(define-public crate-cfile-rs-0.1.0 (c (n "cfile-rs") (v "0.1.0") (h "00c5pvhakq1k8g0zl098bfv6win55whcqd2zcp0z5fy206pg9icj") (y #t)))

(define-public crate-cfile-rs-0.2.0 (c (n "cfile-rs") (v "0.2.0") (h "03i947c2c3sa3in0ihc7kx5w3f7jvx6y3ykw2l7kh6f97qykx6n7") (y #t)))

(define-public crate-cfile-rs-0.3.0 (c (n "cfile-rs") (v "0.3.0") (h "1wb7kr4kqrn819az1ni3dfr22d4w2wz9xwjrs8cshx78hvxqxxyr") (y #t)))

(define-public crate-cfile-rs-0.3.1 (c (n "cfile-rs") (v "0.3.1") (h "0d1dzg6y6lml9h9j4n84ky0zkgxv9abbarfdvz20fxxby3byx4ws") (y #t)))

(define-public crate-cfile-rs-0.3.2 (c (n "cfile-rs") (v "0.3.2") (h "1zx6nwmscbzlpf0lmam91dqqw047c7shw931r8x7n7a92zjynj86") (y #t)))

(define-public crate-cfile-rs-0.3.3 (c (n "cfile-rs") (v "0.3.3") (h "1zm9v5qgx39q560xfqhr2qishm2qra0aj4gyx3ds1f17qsrpgf5b") (y #t)))

