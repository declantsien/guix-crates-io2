(define-module (crates-io cf g_ cfg_log) #:use-module (crates-io))

(define-public crate-cfg_log-0.1.0 (c (n "cfg_log") (v "0.1.0") (h "1wxhddjf16z1ghm48kkixrrzdn8m3h393bdjhfzqxsibb55v4470")))

(define-public crate-cfg_log-0.1.1 (c (n "cfg_log") (v "0.1.1") (h "0z90qqy96zim8ifjcw4xfg8yajdrsxx4bjf1l87jw4cmajk770gr")))

