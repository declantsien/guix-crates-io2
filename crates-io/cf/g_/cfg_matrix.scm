(define-module (crates-io cf g_ cfg_matrix) #:use-module (crates-io))

(define-public crate-cfg_matrix-0.1.0 (c (n "cfg_matrix") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "0xb2bm5p6xwm9ws7994fqhlj17nmbc30vd2iij5pa3f69qvspq99") (y #t)))

(define-public crate-cfg_matrix-0.1.1 (c (n "cfg_matrix") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "parsing" "proc-macro"))) (d #t) (k 0)))) (h "1n3lwn6ihk4cbircfxl0h8qsr0jcc7nh1kh0hh373arw9vhcsnqr")))

