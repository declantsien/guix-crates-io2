(define-module (crates-io cf g_ cfg_me) #:use-module (crates-io))

(define-public crate-cfg_me-0.1.0 (c (n "cfg_me") (v "0.1.0") (d (list (d (n "configure_me") (r "^0.3.3") (d #t) (k 0)) (d (n "configure_me_codegen") (r "^0.3.12") (f (quote ("man"))) (d #t) (k 0)) (d (n "configure_me_codegen") (r "^0.3.12") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "0qz01xd527klaf7l1vdg9004y72272wggvwgwc9wm25ginkjam35")))

(define-public crate-cfg_me-0.1.1 (c (n "cfg_me") (v "0.1.1") (d (list (d (n "configure_me") (r "^0.4.0") (d #t) (k 0)) (d (n "configure_me_codegen") (r "^0.4.1") (f (quote ("man"))) (d #t) (k 0)) (d (n "configure_me_codegen") (r "^0.4.1") (d #t) (k 1)) (d (n "serde") (r "^1") (d #t) (k 0)))) (h "1mw4vbbykh432ky7kdjycdsd6j34ax4xqmfls7kwrsjrq3p2jspv")))

