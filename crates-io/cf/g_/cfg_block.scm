(define-module (crates-io cf g_ cfg_block) #:use-module (crates-io))

(define-public crate-cfg_block-0.1.0 (c (n "cfg_block") (v "0.1.0") (h "0pj0166lkpym8dvxcqdy16dl5449imn6fj08yxqknfhwznj6fb7g")))

(define-public crate-cfg_block-0.1.1 (c (n "cfg_block") (v "0.1.1") (h "11z47bfb5qylcp9ryqbbni6139kdzksqd0vw9wkc6r11jxa80x8q")))

(define-public crate-cfg_block-0.2.0 (c (n "cfg_block") (v "0.2.0") (h "023im9v3nqhh2pap4cmfcjlk76ypj2v1cyafrl6q1zdlprd8j5lg")))

