(define-module (crates-io cf g_ cfg_rust_features) #:use-module (crates-io))

(define-public crate-cfg_rust_features-0.1.0 (c (n "cfg_rust_features") (v "0.1.0") (d (list (d (n "autocfg") (r "^1") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 0)))) (h "1p3f64sy4xzk2mcnvgx93ymar0ip3k1jdb4q3ywi9l3wfn1w9yaw") (r "1.0.0")))

(define-public crate-cfg_rust_features-0.1.1 (c (n "cfg_rust_features") (v "0.1.1") (d (list (d (n "autocfg") (r "^1") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 0)))) (h "1nq6pvw6na1a39jszvndmjkqv52182rn4rkzaw73hxik1f1kaz5b") (r "1.0.0")))

(define-public crate-cfg_rust_features-0.1.2 (c (n "cfg_rust_features") (v "0.1.2") (d (list (d (n "autocfg") (r "^1") (d #t) (k 0)) (d (n "version_check") (r "^0.9") (d #t) (k 0)))) (h "0aypjybdq7niz18i3n11y3v25q7wrak6273q1gw1p29lwc9pjahf") (r "1.0.0")))

