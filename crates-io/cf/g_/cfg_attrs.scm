(define-module (crates-io cf g_ cfg_attrs) #:use-module (crates-io))

(define-public crate-cfg_attrs-1.0.0 (c (n "cfg_attrs") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1mwfzz9j6fadfvimnq8pzpqsj0v3b2aaxxw3blfcv0mwwy51mg7l")))

(define-public crate-cfg_attrs-1.0.1 (c (n "cfg_attrs") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "1b855bkkv8znynyyf21pgyzxr9z706qkfxwl2x4c15wa396yhpfg")))

(define-public crate-cfg_attrs-1.0.2 (c (n "cfg_attrs") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2.0.39") (d #t) (k 0)))) (h "0xp62hfkc2iyzxgayzii8ga9bnhj6a8c353pslh7yjnb3xvkm7v4")))

(define-public crate-cfg_attrs-2.0.0 (c (n "cfg_attrs") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0qfh8xm2xaw5n31pz9i9kk1aryg3xk20cq0b7f9945lsgy7lm5i3")))

(define-public crate-cfg_attrs-2.1.0 (c (n "cfg_attrs") (v "2.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0ga838sxkyyb3asfdzznwl5daghv87m6pa9mrxqm8y0lfrmri8vg")))

(define-public crate-cfg_attrs-2.1.1 (c (n "cfg_attrs") (v "2.1.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "0lvsrzicmp1qbchlzzxp2j8l1lhg7rl4mrzmfzi37nc3hjy6iv6i")))

(define-public crate-cfg_attrs-2.1.2 (c (n "cfg_attrs") (v "2.1.2") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (d #t) (k 0)))) (h "16xs3d4nfsahxick59b2wxx1w676n6by76c3am8rijpykz33grmx")))

(define-public crate-cfg_attrs-3.0.0 (c (n "cfg_attrs") (v "3.0.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "1qnsrnpg1szs8xrjhcxcyygac98hm2hix6c79i0pq708jxhh9sxc")))

