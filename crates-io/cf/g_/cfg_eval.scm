(define-module (crates-io cf g_ cfg_eval) #:use-module (crates-io))

(define-public crate-cfg_eval-0.0.0 (c (n "cfg_eval") (v "0.0.0") (h "04dpmmqdai33w9dgdjczmd1h2brzjw55ncqpszinlhf3r3g1syfw")))

(define-public crate-cfg_eval-0.1.0-rc1 (c (n "cfg_eval") (v "0.1.0-rc1") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "=2.0.0") (f (quote ("parsing" "printing"))) (k 0)))) (h "03z6275dvrpffaj0v4n2dr0vs0ncwj1ivdzs546xqq6fddyj1wgv") (f (quote (("ui-tests" "better-docs") ("docs-rs" "better-docs") ("default") ("better-docs")))) (r "1.61.0")))

(define-public crate-cfg_eval-0.1.0-rc2 (c (n "cfg_eval") (v "0.1.0-rc2") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "=2.0.0") (f (quote ("parsing" "printing"))) (k 0)))) (h "1783yv1czgzri1m89ldl7q7b62mkbjcag5bv1c0hyvdrpg221r71") (f (quote (("ui-tests" "better-docs") ("docs-rs" "better-docs") ("default") ("better-docs")))) (r "1.61.0")))

(define-public crate-cfg_eval-0.1.0 (c (n "cfg_eval") (v "0.1.0") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "=2.0.0") (f (quote ("parsing" "printing"))) (k 0)))) (h "1ambczc2h69sn272z5cl2a2wdbi53mnkzdr644l9pxbynca1rmx8") (f (quote (("ui-tests" "better-docs") ("docs-rs" "better-docs") ("default") ("better-docs")))) (y #t) (r "1.61.0")))

(define-public crate-cfg_eval-0.2.0-rc1 (c (n "cfg_eval") (v "0.2.0-rc1") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "=2.0.0") (f (quote ("parsing" "printing"))) (k 0)))) (h "11an6cq7i1gpq7k4nzaxbd656fqlbwpgbrxgamf1si21jx7sh9zh") (f (quote (("ui-tests" "better-docs") ("items") ("docs-rs" "better-docs") ("default") ("better-docs")))) (y #t) (r "1.61.0")))

(define-public crate-cfg_eval-0.1.1 (c (n "cfg_eval") (v "0.1.1") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "printing"))) (k 0)))) (h "0jz3w3psw1i6hdwv4j5nw8axi9qh650p63qgm52plnk152mqlvm0") (f (quote (("ui-tests" "better-docs") ("docs-rs" "better-docs") ("default") ("better-docs")))) (r "1.61.0")))

(define-public crate-cfg_eval-0.1.2 (c (n "cfg_eval") (v "0.1.2") (d (list (d (n "macro_rules_attribute") (r "^0.2.0") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.0") (d #t) (k 0)) (d (n "syn") (r "^2.0.0") (f (quote ("parsing" "printing"))) (k 0)))) (h "13zqm1jc0ghc0bj1f33kcqxfa3l1dxvsqcjp9w0rd63b874mymj5") (f (quote (("ui-tests" "better-docs") ("items") ("docs-rs" "better-docs") ("default") ("better-docs")))) (r "1.61.0")))

