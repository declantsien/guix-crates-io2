(define-module (crates-io cf g_ cfg_table) #:use-module (crates-io))

(define-public crate-cfg_table-0.1.0 (c (n "cfg_table") (v "0.1.0") (h "0pq0ibskxwxa9vyazmlbxqnyxfxvzd311q5hi3h30s3rx6rg53k0") (y #t)))

(define-public crate-cfg_table-0.1.1 (c (n "cfg_table") (v "0.1.1") (h "07hjdbfqjkpvxrp4gzb12fdz50hm5y0813pbkjl82j73j416kws9")))

(define-public crate-cfg_table-1.0.0 (c (n "cfg_table") (v "1.0.0") (h "18ihbjac021j2w7pr79c97vv4cw09xcha6dxl1lhmisw9vpqnlbq")))

