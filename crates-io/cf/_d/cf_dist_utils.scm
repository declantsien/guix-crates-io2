(define-module (crates-io cf _d cf_dist_utils) #:use-module (crates-io))

(define-public crate-cf_dist_utils-0.1.0 (c (n "cf_dist_utils") (v "0.1.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 0)) (d (n "fang_oost") (r "^0.6.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootfind") (r "^0.7.0") (d #t) (k 0)))) (h "1jrq290g1h60bjxi4lqwbgvwcmvswf9x2win3j8ljjm0rvwbigw6")))

(define-public crate-cf_dist_utils-0.2.0 (c (n "cf_dist_utils") (v "0.2.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootfind") (r "^0.7.0") (d #t) (k 0)))) (h "07ph48i5rcas9q35i9xjrcwbklzv6a35d5jvah23i79nbxxy4r82")))

(define-public crate-cf_dist_utils-0.2.1 (c (n "cf_dist_utils") (v "0.2.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootfind") (r "^0.7.0") (d #t) (k 0)))) (h "0srhdncxjgd876wxc2bm96k7wzfaryrri8njakq6d6m733in2rn2")))

(define-public crate-cf_dist_utils-0.2.2 (c (n "cf_dist_utils") (v "0.2.2") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootfind") (r "^0.7.0") (d #t) (k 0)))) (h "12wifiyczlpwm7s9kmg73ix6qnhn0h1vdh3396lz6nms9qbx1xh5")))

(define-public crate-cf_dist_utils-0.3.0 (c (n "cf_dist_utils") (v "0.3.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootfind") (r "^0.7.0") (d #t) (k 0)))) (h "17ag2riki96dlvrghz6gyyy73n8cl1k63kzm3hjr4jm5gb586mya")))

(define-public crate-cf_dist_utils-0.3.1 (c (n "cf_dist_utils") (v "0.3.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootfind") (r "^0.7.0") (d #t) (k 0)))) (h "1f16y3hmkf94xwjg74bvib94hq6gms8jbkmbk0w2a3kzs2z67a0w")))

(define-public crate-cf_dist_utils-0.3.2 (c (n "cf_dist_utils") (v "0.3.2") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootfind") (r "^0.7.0") (d #t) (k 0)))) (h "058xg9w09xnl846ax9206jzllyd79s5l5g2pka2qzy7y5cykpj3h")))

(define-public crate-cf_dist_utils-0.3.3 (c (n "cf_dist_utils") (v "0.3.3") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootfind") (r "^0.7.0") (d #t) (k 0)))) (h "0shx0fl1zpigm36g81gx475kbi1ypc18j86x6ms7y3byxc3zcb72")))

(define-public crate-cf_dist_utils-0.4.0 (c (n "cf_dist_utils") (v "0.4.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootfind") (r "^0.7.0") (d #t) (k 0)))) (h "1lg8nz63q270w30zyzs9mr7f1paimlkqm5pc1l7i8bznghxjbis2")))

(define-public crate-cf_dist_utils-0.5.0 (c (n "cf_dist_utils") (v "0.5.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "rootfind") (r "^0.7.0") (d #t) (k 0)))) (h "19ysvvk3w59fvw96s2qqv3mlz2w0lbx8k0r0d64fx3jgj2msa6vz")))

(define-public crate-cf_dist_utils-0.6.0 (c (n "cf_dist_utils") (v "0.6.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.7.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "roots") (r "^0.0.4") (d #t) (k 0)))) (h "01mb4gxfmm44q259wm13wj43x4rjrbr6aifc32l2qqi7k43fbvb9")))

(define-public crate-cf_dist_utils-0.6.1 (c (n "cf_dist_utils") (v "0.6.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.13.7") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "roots") (r "^0.0.4") (d #t) (k 0)))) (h "0nfwcj6qnizclldvbvzyfgqj4z3rvya6psp62l086n1zsmakmbjq")))

(define-public crate-cf_dist_utils-0.7.0 (c (n "cf_dist_utils") (v "0.7.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.13.7") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "roots") (r "^0.0.4") (d #t) (k 0)))) (h "00n7qd65r4bbsbdbmw24vmhkhc2b99f8nckvs9qbfw8w3klgcic9")))

(define-public crate-cf_dist_utils-0.7.1 (c (n "cf_dist_utils") (v "0.7.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.13.7") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "roots") (r "^0.0.4") (d #t) (k 0)))) (h "0h6wf0am138p5j18pssn2rj0sd4lvpv2d9gc8pdyg8w1yriz6n26")))

(define-public crate-cf_dist_utils-0.7.2 (c (n "cf_dist_utils") (v "0.7.2") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "fang_oost") (r "^0.13.7") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "roots") (r "^0.0.4") (d #t) (k 0)))) (h "00kh7h1zz0ffcjssqzgx4sh0jay4fj37pcvz8dw2cfhrrpzxrlj4")))

(define-public crate-cf_dist_utils-0.8.0 (c (n "cf_dist_utils") (v "0.8.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "fang_oost") (r "^0.13.7") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nw7hpbvyf8qv9g8i37qqjy3jdnndrvs9rl7ysdgj274vvrm2a9r")))

(define-public crate-cf_dist_utils-0.8.1 (c (n "cf_dist_utils") (v "0.8.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "fang_oost") (r "^0.13.7") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)))) (h "0nqrcfvgbnfsna9368s697wdhjcs422rc4q1zag448280pmhfwjp")))

(define-public crate-cf_dist_utils-0.9.0 (c (n "cf_dist_utils") (v "0.9.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "fang_oost") (r "^0.14.0") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0rg3ywg65976aqghbphmlshbnz7zx6ni2d2ijg0ia8ir9x3bjizd")))

(define-public crate-cf_dist_utils-0.9.1 (c (n "cf_dist_utils") (v "0.9.1") (d (list (d (n "approx") (r ">=0.2.0, <0.3.0") (d #t) (k 2)) (d (n "criterion") (r ">=0.3.0, <0.4.0") (d #t) (k 2)) (d (n "fang_oost") (r ">=0.14.0, <0.15.0") (d #t) (k 0)) (d (n "num-complex") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "rayon") (r ">=1.0.1, <2.0.0") (d #t) (k 0)) (d (n "roots") (r ">=0.0.5, <0.0.6") (d #t) (k 0)) (d (n "serde") (r ">=1.0.0, <2.0.0") (d #t) (k 0)) (d (n "serde_derive") (r ">=1.0.0, <2.0.0") (d #t) (k 0)))) (h "149ynh7ks0q3hbni4ll430ch1xjwmqdq81ql66ajkp73zralslnn")))

(define-public crate-cf_dist_utils-0.10.0 (c (n "cf_dist_utils") (v "0.10.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "fang_oost") (r "^0.15.1") (d #t) (k 0)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 0)) (d (n "roots") (r "^0.0.5") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)))) (h "0js4aa5g74kzsyhx0ag43hpk6yah1iabkg10c2dhr89hq3x8h6vb")))

