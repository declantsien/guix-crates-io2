(define-module (crates-io cf -e cf-env) #:use-module (crates-io))

(define-public crate-cf-env-0.1.0 (c (n "cf-env") (v "0.1.0") (d (list (d (n "guid-create") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "locale-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "0q6kyms93cj93kmrnli1zca2vqwchrd5caniqd0d7jwxwmdvfpdk") (y #t)))

(define-public crate-cf-env-0.1.1 (c (n "cf-env") (v "0.1.1") (d (list (d (n "guid-create") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "locale-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "1qcs8asn5ndv6qs6l823sqn59d1q2wk64d9jbk1d9dcyh04gsyzm") (y #t)))

(define-public crate-cf-env-0.1.2 (c (n "cf-env") (v "0.1.2") (d (list (d (n "guid-create") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "locale-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "0cmjxjmdii5apm6ap5zfg422j1lp09vbw5bd2n3mjbga0l40ww5r")))

(define-public crate-cf-env-0.1.3 (c (n "cf-env") (v "0.1.3") (d (list (d (n "guid-create") (r "^0.1.1") (d #t) (k 0)) (d (n "http") (r "^0.2.5") (d #t) (k 0)) (d (n "locale-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.131") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.72") (d #t) (k 0)))) (h "0llwb37ynk48zc1z6jhs0if65zd8dyzxbx5530g6g613vbivgwdd")))

(define-public crate-cf-env-0.1.4 (c (n "cf-env") (v "0.1.4") (d (list (d (n "guid-create") (r "=0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "http") (r "=0.2.6") (d #t) (k 0)) (d (n "locale-types") (r "=0.4.0") (d #t) (k 0)) (d (n "serde") (r "=1.0.133") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "=1.0.74") (d #t) (k 0)))) (h "0z5llvmdxybvqkr53s83lplyj2hh0hdrl95lwzya2ga24na23dwc")))

(define-public crate-cf-env-0.1.5 (c (n "cf-env") (v "0.1.5") (d (list (d (n "guid-create") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "http") (r "^0.2.6") (d #t) (k 0)) (d (n "locale-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.78") (d #t) (k 0)))) (h "0l7ch8afbcfzxsmcj0x39bc9gphj4ff1igiclw423fcm3ngv8id1")))

(define-public crate-cf-env-0.1.6 (c (n "cf-env") (v "0.1.6") (d (list (d (n "guid-create") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "http") (r "^0.2.6") (d #t) (k 0)) (d (n "locale-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.136") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "15azjdnn5qyjr4ry5xqwrd597zkw7y2h39kwf7jm3cn667ybwby7")))

(define-public crate-cf-env-0.1.7 (c (n "cf-env") (v "0.1.7") (d (list (d (n "guid-create") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "locale-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.140") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.79") (d #t) (k 0)))) (h "10d24ssyrj0j2y6hqwyfrzig7lkma79llihzv6l3jyzvf0fan9f3")))

(define-public crate-cf-env-0.1.8 (c (n "cf-env") (v "0.1.8") (d (list (d (n "guid-create") (r "^0.2.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "http") (r "^0.2.8") (d #t) (k 0)) (d (n "locale-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.141") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.82") (d #t) (k 0)))) (h "12m91w12lwql6hvmvgl0dbscwmqqdfzw8xlc1581gfdff7gcg61x")))

(define-public crate-cf-env-0.1.9 (c (n "cf-env") (v "0.1.9") (d (list (d (n "guid-create") (r "^0.3.1") (f (quote ("serde"))) (d #t) (k 0)) (d (n "http") (r "^1.0.0") (d #t) (k 0)) (d (n "locale-types") (r "^0.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1a37snbac1gdgmpx7ckkpg77bllzx7s387nqjq41xgmv9gk85m1y")))

