(define-module (crates-io cf e_ cfe_progmacro) #:use-module (crates-io))

(define-public crate-cfe_progmacro-0.1.0 (c (n "cfe_progmacro") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (d #t) (k 0)))) (h "17ijqpzp2p98bnwwrqldmbss2i6ix5dhnnjs9xwd7nz919klk8wp")))

(define-public crate-cfe_progmacro-0.1.1 (c (n "cfe_progmacro") (v "0.1.1") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "quote") (r "^1.0.21") (d #t) (k 0)) (d (n "syn") (r "^1.0.105") (d #t) (k 0)))) (h "1vnyp0chs663nw1kgzfvwliphbhhlh1iwxhg50w6pz3is9s7qbj6")))

