(define-module (crates-io cf or cfor) #:use-module (crates-io))

(define-public crate-cfor-0.1.0 (c (n "cfor") (v "0.1.0") (h "0f02v4wm3fdrcxpmngspaz2w6xjlyfy0i111xpckdq7ay90md2vf")))

(define-public crate-cfor-0.1.1 (c (n "cfor") (v "0.1.1") (h "1x24d47v2pjldyvsysqhg534w938j6xh356hixpxqgx7n4kjyf45")))

(define-public crate-cfor-0.2.0 (c (n "cfor") (v "0.2.0") (h "1iigxs1zhiba81ch85gnk6rqp72p5gqxdlf5q609ah6jr1hn45yf")))

(define-public crate-cfor-0.2.1 (c (n "cfor") (v "0.2.1") (h "07dkwvhmyv2k3g6rsmmf07qc149yih129xilbjc5fz7mc8zxdvsv")))

(define-public crate-cfor-1.0.0 (c (n "cfor") (v "1.0.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "0grp6w091dk9waaa5mpzb7qmxxmgddxrzyg6h4zzd63cd38jc1gi") (f (quote (("unstable"))))))

(define-public crate-cfor-1.1.0 (c (n "cfor") (v "1.1.0") (d (list (d (n "rand") (r "^0.3") (d #t) (k 2)))) (h "018j63r5sr2x5qdw8y5c9p66mpnlsa182h5sx1hk49bnryf02dg0") (f (quote (("unstable"))))))

