(define-module (crates-io cf lp cflp) #:use-module (crates-io))

(define-public crate-cflp-0.1.0 (c (n "cflp") (v "0.1.0") (d (list (d (n "cflp_macros") (r "^0.1.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0b6h34w3kgl3j6zr1hsjp1vlw7i5xydd8xjq7khr2mq1klj500i4") (y #t)))

(define-public crate-cflp-1.0.0 (c (n "cflp") (v "1.0.0") (d (list (d (n "cflp_macros") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "12rnsmr4yz9vsrdplcfipimnl7kd5sm5l3x0jss2a8z2cl422mn2")))

(define-public crate-cflp-1.0.1 (c (n "cflp") (v "1.0.1") (d (list (d (n "cflp_macros") (r "^1.0.1") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0df35n64xaym9rigcrhrjdpkvh4h5x9gaav1g8x1mpv7a6hsx0yz")))

(define-public crate-cflp-1.0.2 (c (n "cflp") (v "1.0.2") (d (list (d (n "cflp_macros") (r "^1.0.2") (d #t) (k 0)) (d (n "criterion") (r "^0.5") (f (quote ("html_reports"))) (d #t) (k 2)))) (h "0rrq8lym9cz2cr0ybc4z88bn2z727n6vbx2d3h5npr9mdm26lxc3")))

