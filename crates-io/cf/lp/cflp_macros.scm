(define-module (crates-io cf lp cflp_macros) #:use-module (crates-io))

(define-public crate-cflp_macros-0.1.0 (c (n "cflp_macros") (v "0.1.0") (d (list (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1dad63a0g2hrcbvz44g13652df7ywadif3fg1v22idc1xcv6hnhi") (y #t)))

(define-public crate-cflp_macros-1.0.0 (c (n "cflp_macros") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0kf7lxcmxafmilqcf09c4qqvfvl8xn390z6r8pnkc4y4xj1xb5z9")))

(define-public crate-cflp_macros-1.0.1 (c (n "cflp_macros") (v "1.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0330mnx5djls7vh2p7g2xsj23wlpg2zg9i9hi16zys2p91pwjhfh")))

(define-public crate-cflp_macros-1.0.2 (c (n "cflp_macros") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ydz9fwz146xv2lvaqjwg8fzj5zdgf4frn962avs9j0jaz302icv")))

