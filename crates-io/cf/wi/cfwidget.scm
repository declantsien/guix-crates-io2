(define-module (crates-io cf wi cfwidget) #:use-module (crates-io))

(define-public crate-cfwidget-1.0.0 (c (n "cfwidget") (v "1.0.0") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("rustls" "json"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0cvxbnd7kxydlx82919apy406lvvqsdpr89cys91fdpkkkb6sjl5")))

(define-public crate-cfwidget-1.0.1 (c (n "cfwidget") (v "1.0.1") (d (list (d (n "failure") (r "^0.1.8") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("rustls" "json"))) (d #t) (k 0)) (d (n "semver") (r "^0.11.0") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)) (d (n "url") (r "^2.2.2") (f (quote ("serde"))) (d #t) (k 0)))) (h "0z3fzgrzlfr3mic6h1aghl3cmiajpvk0kj535j2ldk10pwly0gp1")))

