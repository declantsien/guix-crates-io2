(define-module (crates-io cf ro cfront-definition-lexer) #:use-module (crates-io))

(define-public crate-cfront-definition-lexer-0.1.0 (c (n "cfront-definition-lexer") (v "0.1.0") (d (list (d (n "cfront-definition") (r "0.1.*") (d #t) (k 0)) (d (n "cfront-definition-keyword") (r "0.1.*") (d #t) (k 0)))) (h "0fhhj5qn3zpybryfmpfrpl33hp5bcgixs4gjfay11qqjwid435c4")))

