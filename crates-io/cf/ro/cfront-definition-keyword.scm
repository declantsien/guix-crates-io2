(define-module (crates-io cf ro cfront-definition-keyword) #:use-module (crates-io))

(define-public crate-cfront-definition-keyword-0.1.0 (c (n "cfront-definition-keyword") (v "0.1.0") (h "1mrb9d8xcwsxvah5pwww4i8q5dr43vkj9rhz9aj3xcyrlz6q0s2p")))

(define-public crate-cfront-definition-keyword-0.1.1 (c (n "cfront-definition-keyword") (v "0.1.1") (h "0n6wrf3v595z45gm51fps942ksbpm3hr7idzqcpaj1cg5amf5ggk")))

