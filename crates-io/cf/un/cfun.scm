(define-module (crates-io cf un cfun) #:use-module (crates-io))

(define-public crate-cfun-0.1.0 (c (n "cfun") (v "0.1.0") (d (list (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)))) (h "0gmiwha510y7z1vl07gqfzdfhs3cfricg5vcwibiajq0kmrx1531") (y #t)))

(define-public crate-cfun-0.1.10 (c (n "cfun") (v "0.1.10") (d (list (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "0jdvy519nx0h9vf4xrrfm1l0b5ar34fxb5z3xkjwammyf9nh8m30") (y #t)))

(define-public crate-cfun-0.1.11 (c (n "cfun") (v "0.1.11") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "0wjd979fcly6an39sawd8b04czr0y5ra0dbhipy4pfn96bb88gb6") (y #t)))

(define-public crate-cfun-0.1.12 (c (n "cfun") (v "0.1.12") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "091nh6s58v3hw9qm1fzfxhi0ah9cas2nzql9vm6sxwnnimrbp8jg") (y #t)))

(define-public crate-cfun-0.1.13 (c (n "cfun") (v "0.1.13") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "01ki373j8hghy7j532mwwmrm78cciv1a8dhz5mkyr74dl599r2r5") (y #t)))

(define-public crate-cfun-0.1.14 (c (n "cfun") (v "0.1.14") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)))) (h "1cxgv4pjad4wnm1qcvgj1l63zwq1vh7zrxk99pvk1bfb4ra3cnv9") (y #t)))

(define-public crate-cfun-0.1.15 (c (n "cfun") (v "0.1.15") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)))) (h "0rjjs0jy38fmn9sh4gk61ma26l7kjb5sgbxzf4g4f55kna6pkrvq") (y #t)))

(define-public crate-cfun-0.1.16 (c (n "cfun") (v "0.1.16") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)))) (h "0fqz3s6p7a86c3nf19x477l1vgb94vh64ffplwxccr5aklvj8nyi") (y #t)))

(define-public crate-cfun-0.1.17 (c (n "cfun") (v "0.1.17") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "simple-log") (r "^1.6.0") (d #t) (k 0)))) (h "097rhy0mnckr71rm33nbxizcmg3k4jy468l3nv7pwdavy8ik2l5d") (y #t)))

(define-public crate-cfun-0.1.18 (c (n "cfun") (v "0.1.18") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "simple-log") (r "^1.6.0") (d #t) (k 0)))) (h "18qnh8pfjsfwik31ij28v2sll5p67cknnmqc3bsvkbbn3si6gv8y") (y #t)))

(define-public crate-cfun-0.1.19 (c (n "cfun") (v "0.1.19") (d (list (d (n "bzip2") (r "^0.4") (d #t) (k 0)) (d (n "chardetng") (r "^0.1.17") (d #t) (k 0)) (d (n "chrono") (r "^0.4.24") (d #t) (k 0)) (d (n "dirs") (r "^5.0") (d #t) (k 0)) (d (n "encoding_rs") (r "^0.8") (d #t) (k 0)) (d (n "log") (r "^0.4.18") (d #t) (k 0)) (d (n "md-5") (r "^0.10.5") (d #t) (k 0)) (d (n "simple-log") (r "^1.6.0") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("winuser"))) (d #t) (k 0)))) (h "11fckybp00bqjnbh3zqh8nkcy877wmpsdp7lf0f357q7wpan61p3") (y #t)))

