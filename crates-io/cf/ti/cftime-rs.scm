(define-module (crates-io cf ti cftime-rs) #:use-module (crates-io))

(define-public crate-cftime-rs-0.1.0 (c (n "cftime-rs") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "0r3di1qabj02ll5fy0665w0x2jd9slhy30hbfnz7ixr7rhk8fv5x")))

(define-public crate-cftime-rs-0.1.1 (c (n "cftime-rs") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "10gfxn7m7klwcjsp55ds71wgyp5vds1drnn3s7hy86jlqmgq48ng")))

(define-public crate-cftime-rs-0.1.2-ci (c (n "cftime-rs") (v "0.1.2-ci") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "12n4w1b5y5bwha3labqcsl4grwk56kwmwdy2mdj65msa1kidsvhb")))

(define-public crate-cftime-rs-0.1.2 (c (n "cftime-rs") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1yyaxiscbyn3fm6dq82zw90psqdhxjgp5h990rrkl93a301xj684")))

(define-public crate-cftime-rs-0.1.3 (c (n "cftime-rs") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1y574i1m095c5laz9b9lprdpdi2p5ijlha3y3fand8ivlqc0d1ja")))

(define-public crate-cftime-rs-0.1.4 (c (n "cftime-rs") (v "0.1.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "1g4ys1n2pfxrdps5bhkn5vkrakx07p3ghnwn79jkgy9fyijyxvn7")))

(define-public crate-cftime-rs-0.1.5 (c (n "cftime-rs") (v "0.1.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "15p5q202s2rczr4kby6hfqh7cm9z7y73sh99ggd820s9ylgr85x9")))

(define-public crate-cftime-rs-0.1.6 (c (n "cftime-rs") (v "0.1.6") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "pyo3") (r "^0.19.0") (f (quote ("extension-module"))) (d #t) (k 0)) (d (n "rand") (r "^0.8.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.49") (d #t) (k 0)))) (h "11vg8qblkv7bgcqbrigpziq0n1b2a6km67q7qmpg1wssdqsfyada")))

