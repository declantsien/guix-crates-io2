(define-module (crates-io cf p- cfp-rs) #:use-module (crates-io))

(define-public crate-cfp-rs-0.1.0 (c (n "cfp-rs") (v "0.1.0") (d (list (d (n "isahc") (r "^0.8.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "1fmppclb60mg7yk8rbac4wh0660h5yk8yld1p5vg3vgak0kxmraf")))

(define-public crate-cfp-rs-0.2.0 (c (n "cfp-rs") (v "0.2.0") (d (list (d (n "isahc") (r "^0.8.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.103") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.44") (d #t) (k 0)))) (h "07hhsmsmayv128bmvn3x955ax0rkrmaq7iqhj3yki5ll7pa1fivv")))

