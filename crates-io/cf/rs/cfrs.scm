(define-module (crates-io cf rs cfrs) #:use-module (crates-io))

(define-public crate-cfrs-1.0.0 (c (n "cfrs") (v "1.0.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "string"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (o #t) (d #t) (k 0)))) (h "19wi8mn210m8ghvl6k36nf8dhbx922has85q91sski2pw9lglqin") (f (quote (("default" "image")))) (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-cfrs-1.0.1 (c (n "cfrs") (v "1.0.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "string"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (o #t) (d #t) (k 0)))) (h "1as4pk1c2i905j54indb3y12vk9npd9sf26jlj2nzxhaahkg6g24") (f (quote (("default" "image")))) (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-cfrs-1.0.2 (c (n "cfrs") (v "1.0.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "string"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (o #t) (d #t) (k 0)))) (h "11zi3bfldqf8l2kyvqmbzxr2dhw1jk4ivasc7jjmi3aca0iiqgv2") (f (quote (("default" "image")))) (s 2) (e (quote (("image" "dep:image"))))))

(define-public crate-cfrs-1.1.0 (c (n "cfrs") (v "1.1.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "string"))) (o #t) (d #t) (k 0)) (d (n "image") (r "^0.25.1") (o #t) (d #t) (k 0)))) (h "0bs3vac7cbzmpbbbb60sm7w7d2bz1ryzhsv35xkj5zd16q3w1dcj") (f (quote (("default" "image")))) (s 2) (e (quote (("image" "dep:image"))))))

