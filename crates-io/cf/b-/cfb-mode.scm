(define-module (crates-io cf b- cfb-mode) #:use-module (crates-io))

(define-public crate-cfb-mode-0.1.0 (c (n "cfb-mode") (v "0.1.0") (d (list (d (n "aes") (r "^0.1") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.5") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "0c09rpi16zsb67xzhfgln13jfqw24askkqlnn220vdr7x4ijna9i") (f (quote (("std"))))))

(define-public crate-cfb-mode-0.2.0 (c (n "cfb-mode") (v "0.2.0") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "blobby") (r "^0.1") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "00z8345gn55g20vhha44qiqkyz3wdhvhcziza166qpkaqfrmrga8") (f (quote (("std"))))))

(define-public crate-cfb-mode-0.3.0 (c (n "cfb-mode") (v "0.3.0") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "0rqaym8vh40rzrak0lg6f0j3z3phj8k68ffjwf0fnqwn41hk54cc")))

(define-public crate-cfb-mode-0.3.1 (c (n "cfb-mode") (v "0.3.1") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "0b2j3wqgybdl1a4zp98k8ppxp0cl5mv31xv4xxv1k7l4mfcp33g9")))

(define-public crate-cfb-mode-0.3.2 (c (n "cfb-mode") (v "0.3.2") (d (list (d (n "aes") (r "^0.3") (d #t) (k 2)) (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.3") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "0i8kk6gp21hpdp8l32i5yf1wgjs1l4si1xpckf3zhz52sdapn3hr")))

(define-public crate-cfb-mode-0.4.0 (c (n "cfb-mode") (v "0.4.0") (d (list (d (n "aes") (r "^0.4") (d #t) (k 2)) (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.4") (d #t) (k 0)) (d (n "stream-cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)))) (h "0pvjvpvkpc0zfpn7bz72y876cxkx950i5wla8s0bdfi2qj13mz2i")))

(define-public crate-cfb-mode-0.5.0 (c (n "cfb-mode") (v "0.5.0") (d (list (d (n "aes") (r "^0.5") (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)) (d (n "stream-cipher") (r "^0.7") (f (quote ("block-cipher"))) (d #t) (k 0)) (d (n "stream-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)))) (h "14i58slld9n0y6mvq7dj6jnpg066klb7s197ilvk95zqjdr6p9wg")))

(define-public crate-cfb-mode-0.6.0 (c (n "cfb-mode") (v "0.6.0") (d (list (d (n "aes") (r "^0.6") (d #t) (k 2)) (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0cln25dy93j3gja7zzhpd37yqzbd0lah3xc5bwr8syal23lpas8x")))

(define-public crate-cfb-mode-0.7.0 (c (n "cfb-mode") (v "0.7.0") (d (list (d (n "aes") (r "^0.7") (f (quote ("force-soft"))) (d #t) (k 2)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0pdj5grkfzsd5hbgliq53ahw5g97mmj81s7ndr5djs398gzgwj1r")))

(define-public crate-cfb-mode-0.7.1 (c (n "cfb-mode") (v "0.7.1") (d (list (d (n "aes") (r "^0.7") (f (quote ("force-soft"))) (d #t) (k 2)) (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0q08x22aqp3w2dgj59fdq2ni3jvnbvx11v9gm70pai7qn6qzn3bm")))

(define-public crate-cfb-mode-0.8.0 (c (n "cfb-mode") (v "0.8.0") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "11bqr1fd9fxkcik2lrxvj9n1x7j2mcx48qpy40si25dgk98x8810") (f (quote (("zeroize" "cipher/zeroize") ("block-padding" "cipher/block-padding")))) (r "1.56")))

(define-public crate-cfb-mode-0.8.1 (c (n "cfb-mode") (v "0.8.1") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "03mxk2m6flhqsxnybra0sjic880wsk812ddm4dln0vrianfwhn5j") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

(define-public crate-cfb-mode-0.8.2 (c (n "cfb-mode") (v "0.8.2") (d (list (d (n "aes") (r "^0.8") (d #t) (k 2)) (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0c6kd34jk4p52vr0qgn9slj6zdgmc42gfcqr6mqhmy37g138v2vk") (f (quote (("zeroize" "cipher/zeroize") ("std" "cipher/std" "alloc") ("block-padding" "cipher/block-padding") ("alloc" "cipher/alloc")))) (r "1.56")))

