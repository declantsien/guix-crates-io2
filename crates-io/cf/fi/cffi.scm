(define-module (crates-io cf fi cffi) #:use-module (crates-io))

(define-public crate-cffi-0.0.0 (c (n "cffi") (v "0.0.0") (h "0scaych66b5dr09kk9y7h4hi264crfs51kz7sh5cv2ajh9kg22d0") (y #t)))

(define-public crate-cffi-0.1.0 (c (n "cffi") (v "0.1.0") (d (list (d (n "cffi-impl") (r "=0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (o #t) (d #t) (k 0)))) (h "194b7xsaqrngxkn2jvv0yvch55290b3178vfgp7n39vwfd2cn5vp") (f (quote (("default"))))))

(define-public crate-cffi-0.1.1 (c (n "cffi") (v "0.1.1") (d (list (d (n "cffi-impl") (r "=0.1.0") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (o #t) (d #t) (k 0)))) (h "0my2mjvx50ahqy1bvv2pz3fwbxs7p8kgm5f9lr90zbb3mmkcv9k6") (f (quote (("default"))))))

(define-public crate-cffi-0.1.2 (c (n "cffi") (v "0.1.2") (d (list (d (n "cffi-impl") (r "=0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (o #t) (d #t) (k 0)))) (h "0xq5rsscnynnayjc2lnbvf3cn6jy9p9d3g2gd1x6w80nhkbvxr6y") (f (quote (("default"))))))

(define-public crate-cffi-0.1.3 (c (n "cffi") (v "0.1.3") (d (list (d (n "cffi-impl") (r "=0.1.2") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (o #t) (d #t) (k 0)))) (h "0hfcdr55ka0x7g6n41nwk59iri5gflyqqgmz2xz0i7k85q0mvnv1") (f (quote (("default"))))))

(define-public crate-cffi-0.1.4 (c (n "cffi") (v "0.1.4") (d (list (d (n "cffi-impl") (r "=0.1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (o #t) (d #t) (k 0)))) (h "0vgg4vz6zdnmb19yawz52lb48p4nmc86fh3z3f27227bps6286zm") (f (quote (("default")))) (y #t)))

(define-public crate-cffi-0.1.5 (c (n "cffi") (v "0.1.5") (d (list (d (n "cffi-impl") (r "=0.1.5") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (o #t) (d #t) (k 0)))) (h "0dm282nyvf338whb739vx3x0941j70pshkm20b7jxs5nxrk98jmw") (f (quote (("default"))))))

(define-public crate-cffi-0.1.6 (c (n "cffi") (v "0.1.6") (d (list (d (n "cffi-impl") (r "=0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (o #t) (d #t) (k 0)))) (h "0b9fzb7x5217sij9zk7l3dvrhgi345mc3141d9gl18s7lz7d4fy6") (f (quote (("default"))))))

(define-public crate-cffi-0.1.7 (c (n "cffi") (v "0.1.7") (d (list (d (n "cffi-impl") (r "=0.1.6") (d #t) (k 0)) (d (n "libc") (r "^0.2.79") (d #t) (k 0)) (d (n "log") (r "^0.4.11") (d #t) (k 0)) (d (n "url") (r "^2.1.1") (o #t) (d #t) (k 0)))) (h "1vaj5gy6ykp19lhcw5f8cjgnb2aq6k60h3yn8pba5j5a75ifzr75") (f (quote (("default"))))))

