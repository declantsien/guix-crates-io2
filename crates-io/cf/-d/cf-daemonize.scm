(define-module (crates-io cf -d cf-daemonize) #:use-module (crates-io))

(define-public crate-cf-daemonize-0.3.0 (c (n "cf-daemonize") (v "0.3.0") (d (list (d (n "clippy") (r "^0.0") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2.4") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 2)))) (h "1f15d60kgjpnjfvsj3pcv2xgfn6mlf01cyh2l6qg6xf5i1bl4c5m") (f (quote (("default"))))))

