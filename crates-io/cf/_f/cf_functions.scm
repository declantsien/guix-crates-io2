(define-module (crates-io cf _f cf_functions) #:use-module (crates-io))

(define-public crate-cf_functions-0.1.0 (c (n "cf_functions") (v "0.1.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "1v6m3z19pvjz5cj5xb6mhw5ayngv0jwyjvfil2ayrwwqmznb7jf8")))

(define-public crate-cf_functions-0.2.0 (c (n "cf_functions") (v "0.2.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)))) (h "0rds8fgvbf921bj2gzb2vpzhvm8n9yj2p1cz610k03m04p73x19p")))

(define-public crate-cf_functions-0.4.0 (c (n "cf_functions") (v "0.4.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "09109wdy2f6j7sfb9mx5dp63dayr9dpmswbq7x628i2hwna10lcr")))

(define-public crate-cf_functions-0.5.0 (c (n "cf_functions") (v "0.5.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "0acmkrniwqgdpsd2m2vyxpin7n1hf2bzpvlq9mrjk9v9jdvik0dj")))

(define-public crate-cf_functions-0.6.0 (c (n "cf_functions") (v "0.6.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "1lgq6f4ai004amqspzs4j6by8cx5sl97bx0n4ynh8rjgdfi1z83p")))

(define-public crate-cf_functions-0.6.1 (c (n "cf_functions") (v "0.6.1") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "0n7wpcks548kacs318955vch0dqqi1n0dia1d1avgzklrh4h5qn6")))

(define-public crate-cf_functions-0.6.2 (c (n "cf_functions") (v "0.6.2") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "1jki4yk8rn189648p74ip7b0mi69965fnwvkwd9f60zgbcfnf4qq")))

(define-public crate-cf_functions-0.7.0 (c (n "cf_functions") (v "0.7.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "0f6kidcbd7qvp31kvr4c8x3mmfmiml0kfn3m30v4hmjz8s4d1hq2")))

(define-public crate-cf_functions-0.8.0 (c (n "cf_functions") (v "0.8.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "1vjbm1q88yc23v5hm8p9f4d4k65rrv3xp7f8bzkll8hbgdzfh6y8")))

(define-public crate-cf_functions-0.9.0 (c (n "cf_functions") (v "0.9.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "048x80jphc08pxnw0wk18v1z7zwmpgjs5fmvxv7ss0f79ak3hrbg")))

(define-public crate-cf_functions-0.10.0 (c (n "cf_functions") (v "0.10.0") (d (list (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "1kswyax1gv07b759aw9dydzk56xgf1v3l0llkp5xblms3iz3r10p")))

(define-public crate-cf_functions-0.11.0 (c (n "cf_functions") (v "0.11.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "1fskc65g14sr02viz475wnkr9qryi2gxr1grnkiqjhs02i35dmd4")))

(define-public crate-cf_functions-0.12.0 (c (n "cf_functions") (v "0.12.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "cf_dist_utils") (r "^0.8.1") (d #t) (k 2)) (d (n "fang_oost") (r "^0.13.8") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "1yv1r90m37a357bljyn5jdnc0ijjw5fpq2wsp8pw2ncpa13i5wyq")))

(define-public crate-cf_functions-0.12.1 (c (n "cf_functions") (v "0.12.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "cf_dist_utils") (r "^0.8.1") (d #t) (k 2)) (d (n "fang_oost") (r "^0.13.8") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "0snxnc1n51jp0p86cgjph684v91jl9h2azi2a2lq532xa5spn43w")))

(define-public crate-cf_functions-0.13.0 (c (n "cf_functions") (v "0.13.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "cf_dist_utils") (r "^0.8.1") (d #t) (k 2)) (d (n "fang_oost") (r "^0.13.8") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "0n12rhxqyvvkbackq9j9na47hhg4wjyav3zydqp3jy4ph24xrb18")))

(define-public crate-cf_functions-0.14.0 (c (n "cf_functions") (v "0.14.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "cf_dist_utils") (r "^0.8.1") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fang_oost") (r "^0.13.8") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "0c57zch1bgfif1qrgc97ip0nf406wg5jkl35y16736dw5qi29i4i")))

(define-public crate-cf_functions-0.15.0 (c (n "cf_functions") (v "0.15.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "cf_dist_utils") (r "^0.10") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fang_oost") (r "^0.15.1") (d #t) (k 2)) (d (n "fang_oost_option") (r "^0.31") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 2)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "0j1ldjll4qkclyckk0gl2lxgpg9xjx7w3s7zdz2gky272ghraip4")))

(define-public crate-cf_functions-0.15.1 (c (n "cf_functions") (v "0.15.1") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "cf_dist_utils") (r "^0.10") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fang_oost") (r "^0.15.1") (d #t) (k 2)) (d (n "fang_oost_option") (r "^0.31") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 2)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "12m0vj8z0ab3ca0qcwbr9pvz048rcmgs0f1wi53n300fygmvac9b")))

(define-public crate-cf_functions-0.15.2 (c (n "cf_functions") (v "0.15.2") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "cf_dist_utils") (r "^0.10") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fang_oost") (r "^0.15.1") (d #t) (k 2)) (d (n "fang_oost_option") (r "^0.31") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 2)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "0xbwl4b7m6hagf8yvz284z726rjyk8d3q3hapblhhp2j523xgqxv")))

(define-public crate-cf_functions-0.16.0 (c (n "cf_functions") (v "0.16.0") (d (list (d (n "approx") (r "^0.2.0") (d #t) (k 2)) (d (n "cf_dist_utils") (r "^0.10") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "fang_oost") (r "^0.15.1") (d #t) (k 2)) (d (n "fang_oost_option") (r "^0.31") (d #t) (k 2)) (d (n "num-complex") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1.0.1") (d #t) (k 2)) (d (n "special") (r "^0.7") (d #t) (k 0)))) (h "01pbx9iaravxg1m0b1binl04d5w7fjfqhsd54lpm4jwimdx71xvn")))

