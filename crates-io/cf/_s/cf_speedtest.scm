(define-module (crates-io cf _s cf_speedtest) #:use-module (crates-io))

(define-public crate-cf_speedtest-0.4.5 (c (n "cf_speedtest") (v "0.4.5") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "14w4m2x2k2qrz3fxk3vszaznw95dhl54fn5cbyar156xy99hmnch")))

(define-public crate-cf_speedtest-0.4.6 (c (n "cf_speedtest") (v "0.4.6") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "0k501f9csifj7dhm97lif3clnpnrqx9x5vqrp2ixnyh32v5vyhp5")))

(define-public crate-cf_speedtest-0.4.7 (c (n "cf_speedtest") (v "0.4.7") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.21") (d #t) (k 0)) (d (n "ureq") (r "^2.8.0") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.25") (d #t) (k 0)))) (h "1jk4n763jblr0wgppfwly6g5ncqhvhwzjvbihmpsxb3lqhdhjkz0")))

(define-public crate-cf_speedtest-0.4.8 (c (n "cf_speedtest") (v "0.4.8") (d (list (d (n "argh") (r "^0.1.12") (d #t) (k 0)) (d (n "chrono") (r "^0.4.37") (d #t) (k 0)) (d (n "comfy-table") (r "^7.1.0") (d #t) (k 0)) (d (n "rustls") (r "^0.22.0") (d #t) (k 0)) (d (n "ureq") (r "^2.9.6") (d #t) (k 0)) (d (n "webpki-roots") (r "^0.26") (d #t) (k 0)))) (h "1q4h8xd3rhdyijwaqjnb5q1c0bb258l3zizgpv1ngxz2gdjnrsrj")))

