(define-module (crates-io cf di cfdi_use_mx) #:use-module (crates-io))

(define-public crate-cfdi_use_mx-0.1.0 (c (n "cfdi_use_mx") (v "0.1.0") (h "1vkpy99qmm8hrma5zb473j4jrwx2kl5hzy29w96fml8dkivxyn8z")))

(define-public crate-cfdi_use_mx-0.1.1 (c (n "cfdi_use_mx") (v "0.1.1") (h "0arrky1sbc80f63in2jd7riyzrsby8yb33xj3bjm5k2scf7j4848")))

(define-public crate-cfdi_use_mx-0.2.0 (c (n "cfdi_use_mx") (v "0.2.0") (h "1zxvmy00j4blq84pyh23nvh1qd4lrbd3pbif69yx2g62mlhqmqwz")))

(define-public crate-cfdi_use_mx-1.0.0 (c (n "cfdi_use_mx") (v "1.0.0") (d (list (d (n "serde") (r "^1") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1") (d #t) (k 2)))) (h "1javp9ijmgxbnadpf7yfw73q4c6iy6b1hf4a5as33yilkvypj2w0") (f (quote (("default" "serde")))) (s 2) (e (quote (("serde" "dep:serde"))))))

