(define-module (crates-io cf as cfasttext-sys) #:use-module (crates-io))

(define-public crate-cfasttext-sys-0.1.0 (c (n "cfasttext-sys") (v "0.1.0") (d (list (d (n "cc") (r "^1.0.3") (d #t) (k 1)) (d (n "cmake") (r "^0.1.29") (d #t) (k 1)))) (h "1vb2jfbf3iyf3v280higmb1h5ggqzrzzxmzygcilxkwyv72p8zxy")))

(define-public crate-cfasttext-sys-0.1.1 (c (n "cfasttext-sys") (v "0.1.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1zmcaazrccv9dkyqah1lrxdz6q7rqps4dwyzdy79v9qbf04s8glw") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.1.2 (c (n "cfasttext-sys") (v "0.1.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1id8b508xgzvn3avnn8sk99y5ka4ri35ybzm6pgddzkpplhyk11m") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.1.3 (c (n "cfasttext-sys") (v "0.1.3") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1p87lpx3scgjqmm1bms3ipns4lmcjr2wf8g9lyjf34gw5nkvd9wd") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.1.4 (c (n "cfasttext-sys") (v "0.1.4") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "16782fs05vsax03fv3lshp77ahzx3bigpm46ndbsbbp2f0nrfib4") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.1.5 (c (n "cfasttext-sys") (v "0.1.5") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0l9nirz7hi0m194r8yjc8df3v5jp24k8n2amh2kz5jd4kyl4rbac") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.2.0 (c (n "cfasttext-sys") (v "0.2.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0vk535zkv83h8bhgk96gig27y42a60pcjq75wpfi6afgmz27cxlf") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.3.0 (c (n "cfasttext-sys") (v "0.3.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0vnc73irpvnf6gw1a41kj89iqajgnyyy43lc3n4kc8n0ls9mlyw7") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.4.0 (c (n "cfasttext-sys") (v "0.4.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "07q5752fgzzb4dqzy78skw6wwzrfd5s4nqyjc754yxhdib5yai15") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.4.1 (c (n "cfasttext-sys") (v "0.4.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1gvhbxkms90swjs0q95qwdc1a7rknyldgakifafqxw8zi7kv8anv") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.5.0 (c (n "cfasttext-sys") (v "0.5.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1vn9rd6gjswdlia50iijkqmnkq7f8kvxkimc55piw4p1r7kvjc0w") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.5.1 (c (n "cfasttext-sys") (v "0.5.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1ggkl81bqpk5w61jg3mq2503i5cq2sqdv5mrzc0qf9jih7shxl2v") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.5.2 (c (n "cfasttext-sys") (v "0.5.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "09y7kglk966q2xs19zv71dqq5w1rc52fz7ib51dpln8wmf8jqkm3") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.6.0 (c (n "cfasttext-sys") (v "0.6.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "1q2jh20hk25sfd1jalc6fj1sriwiyzwrimvy1ahj1qdivbacff39") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7.0 (c (n "cfasttext-sys") (v "0.7.0") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0pd3w7dzjbln8xbrwyxcicfrk6zjc60w1cw433imrmqa3gpmacq2") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7.1 (c (n "cfasttext-sys") (v "0.7.1") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "0rdw86wsl6j2f8n902jl3gild2fwfsyx0im0n3wlqpwp0fkkcbhp") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7.2 (c (n "cfasttext-sys") (v "0.7.2") (d (list (d (n "cmake") (r "^0.1") (d #t) (k 1)))) (h "14knm2njdkdwgkyjmq9l3nj278j8298plrhjim0zp0ljx7r9zp0l") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7.3 (c (n "cfasttext-sys") (v "0.7.3") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0p5rdnkmzain2mz85qkgw96asw9zgdg0d4nna139xjmmf4nsa4dh") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7.4 (c (n "cfasttext-sys") (v "0.7.4") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "1kj20hf9z8p3ibsa5yfvc8s7vm901cwpgdzcmlg2mx53pipicd26") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7.5 (c (n "cfasttext-sys") (v "0.7.5") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0nkbwslz65snd3rcx9inz0kfq2b3n04m6890jd2fvaxavalb39df") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7.6 (c (n "cfasttext-sys") (v "0.7.6") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0annnxpcd9fbp2zha334gmmii1f6if325cjqgp3ffakrxr95j8dk") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7.7 (c (n "cfasttext-sys") (v "0.7.7") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0sfjlvhpiqbpfgskpyy5h66rcvlzbf97zkvlaffjm9y7d6qkm9zp") (l "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7.8 (c (n "cfasttext-sys") (v "0.7.8") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)))) (h "0sdkzlbvz7qw6k8j0g2m1vbd4xz06rmy89cwcdhki8nv9spqa8cq") (l "cfasttext_static")))

