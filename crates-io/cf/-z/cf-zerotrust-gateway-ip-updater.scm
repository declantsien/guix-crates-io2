(define-module (crates-io cf -z cf-zerotrust-gateway-ip-updater) #:use-module (crates-io))

(define-public crate-cf-zerotrust-gateway-ip-updater-0.1.0 (c (n "cf-zerotrust-gateway-ip-updater") (v "0.1.0") (d (list (d (n "native-tls") (r "^0.2") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "ureq") (r "^2.6") (f (quote ("native-tls" "json"))) (k 0)))) (h "0008j6vnji1svlrvi75hh4764qn8ybj3z8r6pb8sx6ymc5jy3hdl")))

