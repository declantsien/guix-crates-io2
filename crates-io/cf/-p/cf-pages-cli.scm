(define-module (crates-io cf -p cf-pages-cli) #:use-module (crates-io))

(define-public crate-cf-pages-cli-0.1.0 (c (n "cf-pages-cli") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "1qdxacmcqham2qxkkdd1yccsfcm5kbf97b6apz89fy2xdjkskx5q")))

(define-public crate-cf-pages-cli-0.2.0 (c (n "cf-pages-cli") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "14fddazf9wk7z7gg1r4lp92zgrqinpszgcgma8l5g8y0xyyxmpi7") (y #t)))

(define-public crate-cf-pages-cli-0.2.1 (c (n "cf-pages-cli") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.70") (d #t) (k 0)) (d (n "clap") (r "^4.2.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.16") (f (quote ("blocking" "rustls-tls" "json"))) (k 0)) (d (n "serde") (r "^1.0.160") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)))) (h "0m80sivdfwcm9qzy26cmdkhsj88lz752xhsccmn5j99szffc1kwc")))

