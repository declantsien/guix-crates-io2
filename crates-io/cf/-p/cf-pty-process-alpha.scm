(define-module (crates-io cf -p cf-pty-process-alpha) #:use-module (crates-io))

(define-public crate-cf-pty-process-alpha-0.2.0 (c (n "cf-pty-process-alpha") (v "0.2.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "errno") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.21.2") (f (quote ("full"))) (d #t) (k 0)))) (h "0jwvzazf4z0da5h7cpcyjr64ynlw4nh1dgbdbfhx56zxl078g82n")))

