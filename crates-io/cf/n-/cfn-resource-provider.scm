(define-module (crates-io cf n- cfn-resource-provider) #:use-module (crates-io))

(define-public crate-cfn-resource-provider-0.1.0 (c (n "cfn-resource-provider") (v "0.1.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "mockito") (r "^0.14") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "0z70j444iyyrr6xf1hgxxz4if407dk4x42nkg65i2kq5z74p6gn6") (y #t)))

(define-public crate-cfn-resource-provider-0.1.1 (c (n "cfn-resource-provider") (v "0.1.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 2)) (d (n "mockito") (r "^0.14") (d #t) (k 2)) (d (n "reqwest") (r "^0.9") (d #t) (k 0)) (d (n "serde") (r "^1") (d #t) (k 0)) (d (n "serde_derive") (r "^1") (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1") (d #t) (k 2)))) (h "06x38jkchwh0hlx23n6cwbyz6im67ryp89vzjrz4drnjky4cq7s0")))

