(define-module (crates-io cf n- cfn-custom-resource) #:use-module (crates-io))

(define-public crate-cfn-custom-resource-0.1.0 (c (n "cfn-custom-resource") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0366lcwrwy7yxlqqnfp5ghkj8qm1yxnvpv5g6m7k9b7yfpcv6a0n") (y #t)))

(define-public crate-cfn-custom-resource-0.1.1 (c (n "cfn-custom-resource") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.6.1") (d #t) (k 2)) (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1") (d #t) (k 2)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1g4g5wgdp05656fwmmw4k0nk17fynlyz3p5yjjj1f56g7hr62qli")))

