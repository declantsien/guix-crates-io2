(define-module (crates-io cf n- cfn-guard-rulegen-lambda) #:use-module (crates-io))

(define-public crate-cfn-guard-rulegen-lambda-0.0.0 (c (n "cfn-guard-rulegen-lambda") (v "0.0.0") (h "0afpyrgmfsgqclg4xr378n75wnic54w0zcrqx921ilvl1yg0shdk") (y #t)))

(define-public crate-cfn-guard-rulegen-lambda-1.0.0 (c (n "cfn-guard-rulegen-lambda") (v "1.0.0") (d (list (d (n "cfn-guard-rulegen") (r "^1.0.0") (d #t) (k 0)) (d (n "lambda_runtime") (r "^0.2.1") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.92") (d #t) (k 0)) (d (n "simple-error") (r "^0.2.0") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1m7ix5c3y78k93avmkxf47l0h2i8pkanzc2ygfig70rckzvywgx1")))

