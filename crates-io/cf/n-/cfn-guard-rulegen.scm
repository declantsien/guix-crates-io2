(define-module (crates-io cf n- cfn-guard-rulegen) #:use-module (crates-io))

(define-public crate-cfn-guard-rulegen-0.0.0 (c (n "cfn-guard-rulegen") (v "0.0.0") (h "15zxgl5wdxi7xhw9krqn41hlksqayh1y3835l0bx01f879dsvvaq") (y #t)))

(define-public crate-cfn-guard-rulegen-1.0.0 (c (n "cfn-guard-rulegen") (v "1.0.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1dz79adw3rjihjjn1fghs1zhyyvzkzybf620nl30jch9x3j3j9ys")))

