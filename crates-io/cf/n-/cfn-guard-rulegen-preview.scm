(define-module (crates-io cf n- cfn-guard-rulegen-preview) #:use-module (crates-io))

(define-public crate-cfn-guard-rulegen-preview-0.7.0 (c (n "cfn-guard-rulegen-preview") (v "0.7.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1zp3yjz8f12syjzq5sk5kp7wzhl8705xsl398y1z0mrh957xqihk")))

