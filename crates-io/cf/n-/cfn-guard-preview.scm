(define-module (crates-io cf n- cfn-guard-preview) #:use-module (crates-io))

(define-public crate-cfn-guard-preview-0.7.0 (c (n "cfn-guard-preview") (v "0.7.0") (d (list (d (n "cfn-guard-rulegen-preview") (r "^0.7.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "jni") (r "^0.14") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.6") (d #t) (k 0)) (d (n "regex") (r "^1.1.9") (d #t) (k 0)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8.9") (d #t) (k 0)) (d (n "simple_logger") (r "^1.3.0") (d #t) (k 0)))) (h "1z1g38l09hanzrr7k25hjrf185nq3c5a99wsra67izjgv89hrszk")))

