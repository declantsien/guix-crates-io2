(define-module (crates-io cf n- cfn-guard-ffi) #:use-module (crates-io))

(define-public crate-cfn-guard-ffi-2.1.1 (c (n "cfn-guard-ffi") (v "2.1.1") (d (list (d (n "cfn-guard") (r "^2.1.1") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4.4") (d #t) (k 0)))) (h "1cqdf1cvz2jrf0aaswnqks5143gfn2ld8nlfmvwclc8idykil9xs")))

(define-public crate-cfn-guard-ffi-2.1.2 (c (n "cfn-guard-ffi") (v "2.1.2") (d (list (d (n "cfn-guard") (r "^2.1.2") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4.4") (d #t) (k 0)))) (h "1bc8xi36qjdmir1hwpc2dhxhvff5x6sj74q0nlkarpfhnvx6jh9c")))

(define-public crate-cfn-guard-ffi-2.1.4 (c (n "cfn-guard-ffi") (v "2.1.4") (d (list (d (n "cfn-guard") (r "^2.1.4") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4.4") (d #t) (k 0)))) (h "1b3047g459xsg0gp8igypc0lrn8k7c2xd6mcbisxqkmkk9g8hsjy")))

(define-public crate-cfn-guard-ffi-3.0.0 (c (n "cfn-guard-ffi") (v "3.0.0") (d (list (d (n "cfn-guard") (r "^3.0.0") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4.4") (d #t) (k 0)))) (h "111m8wscbzqk6n0ggfnxd9jzxwrfmaprdv0gqik3zad5sa5gb9af")))

(define-public crate-cfn-guard-ffi-3.0.1 (c (n "cfn-guard-ffi") (v "3.0.1") (d (list (d (n "cfn-guard") (r "^3.0.1") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4.4") (d #t) (k 0)))) (h "0vp608lisxdlch7grr1zwdlm23jwqvc19lp07zv8ss072km29545")))

(define-public crate-cfn-guard-ffi-3.0.2 (c (n "cfn-guard-ffi") (v "3.0.2") (d (list (d (n "cfn-guard") (r "^3.0.2") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4.4") (d #t) (k 0)))) (h "0lvl4g4kv3ydhl3mxl65jwjpdcy7h37vxgki59azkd34l763kyg5")))

(define-public crate-cfn-guard-ffi-3.0.3 (c (n "cfn-guard-ffi") (v "3.0.3") (d (list (d (n "cfn-guard") (r "^3.0.3") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4.4") (d #t) (k 0)))) (h "1pbq6fzh0f6cfnlg6yddc0y6wy14byd1ig76jppi80396rj4rdkl")))

(define-public crate-cfn-guard-ffi-3.1.0 (c (n "cfn-guard-ffi") (v "3.1.0") (d (list (d (n "cfn-guard") (r "^3.1.0") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4.4") (d #t) (k 0)))) (h "1ilnwpkyb12ifkza93k9l01f87r1d4plbwld3jp9ypd86k3gssf8")))

(define-public crate-cfn-guard-ffi-3.1.1 (c (n "cfn-guard-ffi") (v "3.1.1") (d (list (d (n "cfn-guard") (r "^3.1.1") (d #t) (k 0)) (d (n "ffi-support") (r "^0.4.4") (d #t) (k 0)))) (h "0r8p48yjzmkhaa9ccy73d0shl0mnf1f2awp9jd1f8z6kypw52h3g")))

