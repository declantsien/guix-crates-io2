(define-module (crates-io cf -s cf-services) #:use-module (crates-io))

(define-public crate-cf-services-0.1.0 (c (n "cf-services") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "0lbi1na3dp1gp9nvvj7zfiiabn2v1cia1j0zpwmkbhg6zdpf0pcx")))

(define-public crate-cf-services-0.1.1 (c (n "cf-services") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)))) (h "1spfkc5iadj19jp71mwpzanw6qap46w0ymd0bsa6kl1ay3phhfbd")))

