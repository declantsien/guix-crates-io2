(define-module (crates-io cf rp cfrp) #:use-module (crates-io))

(define-public crate-cfrp-0.0.1 (c (n "cfrp") (v "0.0.1") (d (list (d (n "log") (r "*") (d #t) (k 0)))) (h "0fc9qpdwk5bkvs2wfrar5pp9agpwbs2w23g93ka78g98f17z7q9c")))

(define-public crate-cfrp-0.0.2 (c (n "cfrp") (v "0.0.2") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "^0.3.0") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1phl6l9lxr4j2rpwrk55plgqi762mbfq08vslinz164wlschha82")))

(define-public crate-cfrp-0.0.3 (c (n "cfrp") (v "0.0.3") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1gjsg0alylhl47pqx4ny0bhqa8la0qmy16xady4g3n0x1i6w76rx")))

(define-public crate-cfrp-0.0.4 (c (n "cfrp") (v "0.0.4") (d (list (d (n "env_logger") (r "*") (d #t) (k 0)) (d (n "log") (r "*") (d #t) (k 0)) (d (n "rand") (r "*") (d #t) (k 0)) (d (n "time") (r "*") (d #t) (k 0)))) (h "1z7r59c594fci8864f7vkr9rckgyw891dnn1583chcqibrz1mm3j")))

