(define-module (crates-io cf -w cf-worker-http-client) #:use-module (crates-io))

(define-public crate-cf-worker-http-client-0.1.0 (c (n "cf-worker-http-client") (v "0.1.0") (d (list (d (n "cookie") (r "^0.16.1") (k 0)) (d (n "cookie_store") (r "^0.18.0") (f (quote ("wasm-bindgen"))) (k 0)) (d (n "parking_lot") (r "^0.12.1") (k 0)) (d (n "url") (r "^2.3.1") (k 0)) (d (n "worker") (r "^0.0.11") (k 0)))) (h "14hydfs1i7j5bgxj53xmf5hhc3g2p8xwdnv9imyl8939qzqbp1v9") (r "1.63")))

