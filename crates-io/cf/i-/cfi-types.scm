(define-module (crates-io cf i- cfi-types) #:use-module (crates-io))

(define-public crate-cfi-types-0.0.1 (c (n "cfi-types") (v "0.0.1") (h "06fpkda9zk319pcscs3dkyn6w5alzqmnsdryx3ds7k4k4gdgk262")))

(define-public crate-cfi-types-0.0.2 (c (n "cfi-types") (v "0.0.2") (h "1zkcxzvyqp6g3rdc4gmgf1xhr94rgwlvi6ikl2qwm98nw41h5vrd")))

