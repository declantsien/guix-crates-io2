(define-module (crates-io cf gm cfgmap) #:use-module (crates-io))

(define-public crate-cfgmap-0.1.0 (c (n "cfgmap") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.48") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "1qivp2cw6xjjpcig22a419r7vjxv50g1mmb1yn1hdmhdha4aqrqa") (f (quote (("from_toml" "toml") ("from_json" "serde_json") ("default" "from_json" "from_toml"))))))

(define-public crate-cfgmap-0.2.0 (c (n "cfgmap") (v "0.2.0") (d (list (d (n "serde_json") (r "^1.0.48") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "0414b1sdhqdg0gs0pb4k9rh71cbymd0ym9hc748mwq379xy7mafx") (f (quote (("from_toml" "toml") ("from_json" "serde_json") ("default" "from_json" "from_toml"))))))

(define-public crate-cfgmap-0.2.1 (c (n "cfgmap") (v "0.2.1") (d (list (d (n "rand") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "0az8kjklv48fjpwadydrnm11yifbphwjvc3avh9sqzxypin6x2i2") (f (quote (("generator" "rand") ("from_toml" "toml") ("from_json" "serde_json") ("default"))))))

(define-public crate-cfgmap-0.2.2 (c (n "cfgmap") (v "0.2.2") (d (list (d (n "rand") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "16dlxr2dcj3piwjlxnf1dydxhzp4f0qqcg67isk66jssjz703rfx") (f (quote (("generator" "rand") ("from_toml" "toml") ("from_json" "serde_json") ("default"))))))

(define-public crate-cfgmap-0.2.3 (c (n "cfgmap") (v "0.2.3") (d (list (d (n "rand") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "1iwz74j2pilpbqnp7qr80fvmf8xmw6gdi77rlvhs3rf32b6hdysq") (f (quote (("generator" "rand") ("from_toml" "toml") ("from_json" "serde_json") ("default"))))))

(define-public crate-cfgmap-0.3.0 (c (n "cfgmap") (v "0.3.0") (d (list (d (n "rand") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "1naqqkvzjd372ifd916x6z8l7zrabscg5nnzir6y6r82hkqhpp3v") (f (quote (("generator" "rand") ("from_toml" "toml") ("from_json" "serde_json") ("default"))))))

(define-public crate-cfgmap-0.3.1 (c (n "cfgmap") (v "0.3.1") (d (list (d (n "rand") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (o #t) (d #t) (k 0)))) (h "19bq2nzggm1sjldaykwn4nff1md2jdrb3awwi1dsfwvdq8ac0zfq") (f (quote (("generator" "rand") ("from_toml" "toml") ("from_json" "serde_json") ("default"))))))

(define-public crate-cfgmap-0.4.0 (c (n "cfgmap") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.48") (o #t) (d #t) (k 0)) (d (n "toml") (r "^0.5.6") (o #t) (d #t) (k 0)) (d (n "yaml-rust") (r "^0.4.4") (o #t) (d #t) (k 0)))) (h "16dbcx9zii266zjr97i932zsby58w4z1j9d8029lavwfl4dpns3h") (f (quote (("generator" "rand") ("from_yaml" "yaml-rust") ("from_toml" "toml") ("from_json" "serde_json") ("default"))))))

