(define-module (crates-io cf dy cfdydns) #:use-module (crates-io))

(define-public crate-cfdydns-0.1.0 (c (n "cfdydns") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.82") (d #t) (k 0)) (d (n "clap") (r "^4.5.4") (f (quote ("derive" "env"))) (d #t) (k 0)) (d (n "colored") (r "^2.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.11.3") (d #t) (k 0)) (d (n "log") (r "^0.4.21") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("blocking" "json" "rustls-tls"))) (k 0)) (d (n "serde") (r "^1.0.200") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ngn6fzkyybflh2nw7ndxpffkyqd3amq8y0iqzg6kmh8c864g7kf")))

