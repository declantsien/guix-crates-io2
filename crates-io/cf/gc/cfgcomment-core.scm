(define-module (crates-io cf gc cfgcomment-core) #:use-module (crates-io))

(define-public crate-cfgcomment-core-0.1.0 (c (n "cfgcomment-core") (v "0.1.0") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "070jzhsvsrsgdaqcvy9g7yzjpg7da4j54wl1kvmiimi7544cfrpw")))

(define-public crate-cfgcomment-core-0.2.0 (c (n "cfgcomment-core") (v "0.2.0") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "1dr5107n3rp4ypy5pkwml7yg9sxsxb3hd6ysymkh41yiyiszg8gj")))

(define-public crate-cfgcomment-core-0.3.0 (c (n "cfgcomment-core") (v "0.3.0") (d (list (d (n "ignore") (r "^0.4.18") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "peg") (r "^0.7.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)))) (h "00p6fpwls9skvjc9kq7zw2i6xjkwmsjdcbvrmpj9dbfvxkjw9g76")))

