(define-module (crates-io cf gc cfgcomment-buildscript) #:use-module (crates-io))

(define-public crate-cfgcomment-buildscript-0.1.0 (c (n "cfgcomment-buildscript") (v "0.1.0") (d (list (d (n "cfgcomment-core") (r "^0.2.0") (d #t) (k 0)))) (h "1ibjqv045g83kcsxn61z70z199mkb806l9m1fkzwqnnlsi7mmmm4") (f (quote (("taa-fff"))))))

(define-public crate-cfgcomment-buildscript-0.3.0 (c (n "cfgcomment-buildscript") (v "0.3.0") (d (list (d (n "cfgcomment-core") (r "^0.3.0") (d #t) (k 0)))) (h "0iq00v0gjnghsm9rn1fncl333pdr0cifxb191jk00df3lbd7838j") (f (quote (("taa-fff"))))))

