(define-module (crates-io cf gc cfgcomment) #:use-module (crates-io))

(define-public crate-cfgcomment-0.1.0 (c (n "cfgcomment") (v "0.1.0") (d (list (d (n "cfgcomment-core") (r "^0.1.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.9.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (f (quote ("color" "suggestions" "wrap_help"))) (d #t) (k 0)))) (h "0lpxppd97w8b8qy1far2mw04795ivp69nykckn5mrczl0yac2fhh")))

(define-public crate-cfgcomment-0.2.0 (c (n "cfgcomment") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.42") (d #t) (k 0)) (d (n "cfgcomment-core") (r "^0.2.0") (d #t) (k 0)) (d (n "git-filter-server") (r "^0.1.0") (d #t) (k 0)) (d (n "structopt") (r "^0.3.22") (f (quote ("color" "suggestions" "wrap_help"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.19") (f (quote ("tracing-log"))) (d #t) (k 0)))) (h "0yi46hcwi33ck9x9z9da0pnzxglna35v62mfgrad3r0xswa783s8")))

(define-public crate-cfgcomment-0.3.0 (c (n "cfgcomment") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0.43") (d #t) (k 0)) (d (n "cfgcomment-core") (r "^0.3.0") (d #t) (k 0)) (d (n "git-filter-server") (r "^0.1.3") (d #t) (k 0)) (d (n "structopt") (r "^0.3.23") (f (quote ("color" "suggestions" "wrap_help"))) (d #t) (k 0)) (d (n "tracing") (r "^0.1.26") (d #t) (k 0)) (d (n "tracing-subscriber") (r "^0.2.20") (f (quote ("tracing-log"))) (d #t) (k 0)))) (h "146l0mjdm7qiz2kmmqvl3rxhb6h1wgajcqgza1pn4xk05wn5a736")))

