(define-module (crates-io tk lo tklog) #:use-module (crates-io))

(define-public crate-tklog-0.0.1 (c (n "tklog") (v "0.0.1") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5.13") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "02z1iygq3m6698w4n7hxvzcrp01ir3v29qxa1pgd7d9ifkfwahr9")))

(define-public crate-tklog-0.0.2 (c (n "tklog") (v "0.0.2") (d (list (d (n "chrono") (r "^0.4.38") (d #t) (k 0)) (d (n "crossbeam-channel") (r "^0.5.13") (d #t) (k 0)) (d (n "flate2") (r "^1.0") (d #t) (k 0)) (d (n "once_cell") (r "^1.19.0") (d #t) (k 0)) (d (n "regex") (r "^1.10.4") (d #t) (k 0)) (d (n "tokio") (r "^1.37.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0axs3bxnsryix1glhwd0a2xnaf2s30s25fbwdbbl66ris9hpwdbv")))

