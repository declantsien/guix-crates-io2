(define-module (crates-io tk -b tk-bufstream) #:use-module (crates-io))

(define-public crate-tk-bufstream-0.1.0 (c (n "tk-bufstream") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "futures") (r "^0.1.2") (d #t) (k 0)) (d (n "netbuf") (r "^0.3.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "1v0hprpz7qnhdn28cm4w6m3xb325l5w4sj3xywgxkhs9liv2jbw1")))

(define-public crate-tk-bufstream-0.2.0 (c (n "tk-bufstream") (v "0.2.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "futures") (r "^0.1.4") (d #t) (k 0)) (d (n "netbuf") (r "^0.3.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "1f7mai85wxz7mxxfiq5ffbjzkvgvbwzkfdyfrm46pq44rd0j3y26")))

(define-public crate-tk-bufstream-0.2.1 (c (n "tk-bufstream") (v "0.2.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "futures") (r "^0.1.4") (d #t) (k 0)) (d (n "netbuf") (r "^0.3.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "09nx65vvn1f1gf0aqf518zqh4i472zd6qh55nyzqna7la7ib7arr")))

(define-public crate-tk-bufstream-0.2.2 (c (n "tk-bufstream") (v "0.2.2") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "futures") (r "^0.1.4") (d #t) (k 0)) (d (n "netbuf") (r "^0.3.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "168h6lflja8risjnq616dqdmycw83g6dahhaaysmx51i8d5ji3jw")))

(define-public crate-tk-bufstream-0.2.3 (c (n "tk-bufstream") (v "0.2.3") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "futures") (r "^0.1.4") (d #t) (k 0)) (d (n "netbuf") (r "^0.3.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "1pxqc2lzv3m9zv1nqd3m81i22lwnkdg7bm4hby283cswm5f0vk01")))

(define-public crate-tk-bufstream-0.2.4 (c (n "tk-bufstream") (v "0.2.4") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "futures") (r "^0.1.4") (d #t) (k 0)) (d (n "netbuf") (r "^0.3.5") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.0") (d #t) (k 0)))) (h "1jib8n3pdjkwssafdnhd2b3r5p02z91xkk077x4qg7qvnn8qp7hd")))

(define-public crate-tk-bufstream-0.3.0 (c (n "tk-bufstream") (v "0.3.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 2)) (d (n "env_logger") (r "^0.3.5") (d #t) (k 2)) (d (n "futures") (r "^0.1.11") (d #t) (k 0)) (d (n "netbuf") (r "^0.4.0") (d #t) (k 0)) (d (n "quick-error") (r "^1.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.6") (d #t) (k 2)) (d (n "tokio-io") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-service") (r "^0.1.0") (d #t) (k 2)))) (h "0drjp0s4ad88sglfc8qvpmlv7m9vzx07xyg38lh3dcxygh37pf4v")))

