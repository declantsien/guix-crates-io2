(define-module (crates-io tk -o tk-opc) #:use-module (crates-io))

(define-public crate-tk-opc-0.1.0 (c (n "tk-opc") (v "0.1.0") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "125k1h0d2n29hbi8zvbzpb5p93r0r330mly9yy5rv5hqv9884sm8")))

(define-public crate-tk-opc-0.1.1 (c (n "tk-opc") (v "0.1.1") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "1jaawd8sm416fn2rji9jfpx3ww1agvnxrh22wzcy4gxyxl5h2xpp")))

(define-public crate-tk-opc-0.1.2 (c (n "tk-opc") (v "0.1.2") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "0qk5cv8m1mhhpav5gsadvaiscrsplgkv9i25clhm7qcy6bz2375q")))

(define-public crate-tk-opc-0.1.3 (c (n "tk-opc") (v "0.1.3") (d (list (d (n "bytes") (r "^0.4") (d #t) (k 0)) (d (n "tokio-io") (r "^0.1") (d #t) (k 0)))) (h "02l60a6cihs0b6n7ff76l5hx3frhpkgsqr9lsrz0a1vl9k5xyj0g")))

