(define-module (crates-io tk -e tk-easyloop) #:use-module (crates-io))

(define-public crate-tk-easyloop-0.1.0 (c (n "tk-easyloop") (v "0.1.0") (d (list (d (n "futures") (r "^0.1.7") (d #t) (k 0)) (d (n "scoped-tls") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.2") (d #t) (k 0)))) (h "040z8adkmsndn5prrhfq4yfyv8p2cqdr3dksxcqcj9lyhr14jgha")))

(define-public crate-tk-easyloop-0.1.1 (c (n "tk-easyloop") (v "0.1.1") (d (list (d (n "futures") (r "^0.1.7") (d #t) (k 0)) (d (n "scoped-tls") (r "^0.1.0") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.2") (d #t) (k 0)))) (h "1ccaqkxjav5digd53hhm5bgiyj7pjvqan8k1yhpyrh3g907106i9")))

