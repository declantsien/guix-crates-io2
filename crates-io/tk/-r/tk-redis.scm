(define-module (crates-io tk -r tk-redis) #:use-module (crates-io))

(define-public crate-tk-redis-0.1.0 (c (n "tk-redis") (v "0.1.0") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (f (quote ("use_needs_drop" "use_union"))) (d #t) (k 0)) (d (n "spin") (r "^0.4.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.1") (d #t) (k 0)))) (h "0qb0cpj3nfqlk02yhmw4y79ah0avg6wjd7jcwpwi3q2n7860190l") (y #t)))

(define-public crate-tk-redis-0.1.1 (c (n "tk-redis") (v "0.1.1") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (f (quote ("use_needs_drop" "use_union"))) (d #t) (k 0)) (d (n "spin") (r "^0.4.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "1rfvncapvwjdsdf600sxhylwciz177j431g670c75rq6ggz86cm9") (y #t)))

(define-public crate-tk-redis-0.1.2 (c (n "tk-redis") (v "0.1.2") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (f (quote ("use_needs_drop" "use_union"))) (d #t) (k 0)) (d (n "spin") (r "^0.4.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0a0cz0k2ps0ag5868zyqq1gkdmvxil7yxp6x2gypd006hp02mhw4") (y #t)))

(define-public crate-tk-redis-0.1.3 (c (n "tk-redis") (v "0.1.3") (d (list (d (n "either") (r "^1.0") (d #t) (k 0)) (d (n "futures") (r "^0.1.7") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "nodrop") (r "^0.1.8") (f (quote ("use_needs_drop" "use_union"))) (d #t) (k 0)) (d (n "spin") (r "^0.4.5") (d #t) (k 0)) (d (n "tokio-core") (r "^0.1.1") (d #t) (k 0)) (d (n "url") (r "^1.2") (d #t) (k 0)))) (h "0zg2snkz9hs89xj80a4wjchyy7qk1pz53nljyzb6f4di840z1j19")))

