(define-module (crates-io tk -s tk-sys) #:use-module (crates-io))

(define-public crate-tk-sys-0.1.0 (c (n "tk-sys") (v "0.1.0") (d (list (d (n "bindgen") (r "^0.63.0") (f (quote ("runtime"))) (k 1)) (d (n "libc") (r "^0.2.137") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)) (d (n "tcl-sys") (r "^0.1.0") (d #t) (k 0)) (d (n "x11") (r "^2.20.1") (d #t) (k 0)))) (h "17hinqk7kda20mdad3m0ba1m6q36gfi4pnw730y7n9x2x77109ig")))

(define-public crate-tk-sys-0.2.0 (c (n "tk-sys") (v "0.2.0") (d (list (d (n "libc") (r "^0.2.140") (d #t) (k 0)) (d (n "tcl-sys") (r "^0.2.0") (d #t) (k 0)) (d (n "x11") (r "^2.21.0") (d #t) (k 0)) (d (n "bindgen") (r "^0.64.0") (f (quote ("runtime"))) (k 1)) (d (n "pkg-config") (r "^0.3.26") (d #t) (k 1)))) (h "1dzqm2lbka5zrysz1gbk1zr21c34w94v0jjs5fsxmb2bfhbalgwn") (l "tk")))

