(define-module (crates-io p_ ru p_rust) #:use-module (crates-io))

(define-public crate-p_rust-0.1.0 (c (n "p_rust") (v "0.1.0") (d (list (d (n "num-bigint") (r "^0.4.3") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.15") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1gyjbw4h4w0vmnisbq251l0567xw3qa39sszaba2yr0101ij3h53")))

