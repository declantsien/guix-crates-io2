(define-module (crates-io p_ do p_documenting) #:use-module (crates-io))

(define-public crate-p_documenting-0.1.0 (c (n "p_documenting") (v "0.1.0") (h "13x11sfzmy8g7xx1bwsllpqxip30c9dm0y3f7hdrs9d062dx5gvq")))

(define-public crate-p_documenting-0.1.1 (c (n "p_documenting") (v "0.1.1") (h "04x4jm5dmspc53d4nx6rnicl7imm41wlrnch446vkklll46whak3")))

