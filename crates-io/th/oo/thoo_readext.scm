(define-module (crates-io th oo thoo_readext) #:use-module (crates-io))

(define-public crate-thoo_readext-0.1.0 (c (n "thoo_readext") (v "0.1.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "1q4qzldnaam8293giw6qlw4njayx1dpqbr9a7rrdf5lx42m9wb6v")))

(define-public crate-thoo_readext-1.0.0 (c (n "thoo_readext") (v "1.0.0") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0xvyzfykd7qgmnmf3l7himcjflmz0bjamz36kbziqhn7md35p27m")))

(define-public crate-thoo_readext-1.0.1 (c (n "thoo_readext") (v "1.0.1") (d (list (d (n "byteorder") (r "^1.4.3") (d #t) (k 0)))) (h "0cbn9nmz9hb79pdxabgmy0a50sxmj5cnfllx8n4ylia1f6nw57r6")))

