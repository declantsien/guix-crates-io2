(define-module (crates-io th om thomas) #:use-module (crates-io))

(define-public crate-thomas-0.2.0 (c (n "thomas") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "thomas_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1v5y6835ax3djljga5q651fv36dh1ipb6c3zdvnfzgw15navbhya")))

(define-public crate-thomas-0.2.1 (c (n "thomas") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "thomas_derive") (r "^0.2.0") (d #t) (k 0)))) (h "176cdfd6z8jyg23gsrnsvzlj4szmfvvcfb6qh9kcv3jiia0sspll")))

(define-public crate-thomas-0.2.2 (c (n "thomas") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "thomas_derive") (r "^0.2.0") (d #t) (k 0)))) (h "16l7c0nba6zjsbvykqdjv3imxm3lwnjjzccp7rdcw6ps69jvmygj")))

(define-public crate-thomas-0.2.3 (c (n "thomas") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "thomas_derive") (r "^0.2.0") (d #t) (k 0)))) (h "1dbrmms6r098x28fmri80mv03a49h0r00bb6q5pkqnws0a9bh83k")))

(define-public crate-thomas-0.2.4 (c (n "thomas") (v "0.2.4") (d (list (d (n "crossterm") (r "^0.26.1") (d #t) (k 0)) (d (n "device_query") (r "^1.1.2") (d #t) (k 0)) (d (n "thomas_derive") (r "^0.2.0") (d #t) (k 0)))) (h "09ir8m5w3s79js8nzalcp0zy0k7gik6njpnvmpm0n1iyxw36w048")))

