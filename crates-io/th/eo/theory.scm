(define-module (crates-io th eo theory) #:use-module (crates-io))

(define-public crate-theory-0.0.1 (c (n "theory") (v "0.0.1") (d (list (d (n "endiannezz") (r "^0.6.5") (d #t) (k 0)) (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)))) (h "09rfsfy9facafl0zapss96kmvicj4ci4g2nbwzlqdkl84sxch4n9")))

(define-public crate-theory-0.1.0 (c (n "theory") (v "0.1.0") (d (list (d (n "clap") (r "^4.0.10") (f (quote ("derive" "std" "usage" "help"))) (k 2)) (d (n "endiannezz") (r "^0.6.5") (d #t) (k 0)) (d (n "flate2") (r "^1.0.24") (o #t) (d #t) (k 0)) (d (n "leb128") (r "^0.2.5") (d #t) (k 0)) (d (n "lru") (r "^0.8.1") (k 0)) (d (n "lz4_flex") (r "^0.9.5") (o #t) (d #t) (k 0)) (d (n "num_enum") (r "^0.5.7") (k 0)) (d (n "thiserror") (r "^1.0.36") (d #t) (k 0)) (d (n "tinyvec") (r "^1.6.0") (f (quote ("rustc_1_57" "alloc"))) (d #t) (k 0)))) (h "0f94r9f5r5cvkqsn9adyn2pxxzang4pfdmcmm4axamhhdgs2nvwm") (f (quote (("lz4" "lz4_flex") ("deflate" "flate2") ("default" "deflate" "lz4"))))))

