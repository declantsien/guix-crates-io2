(define-module (crates-io th eo theorafile-rs) #:use-module (crates-io))

(define-public crate-theorafile-rs-0.1.0 (c (n "theorafile-rs") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "1mcvsy7rhbyjizlhirzp33x5bdkxb4ca5f1vsyqxj0y17mx93s75")))

(define-public crate-theorafile-rs-0.1.1 (c (n "theorafile-rs") (v "0.1.1") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0y5xzdfmp382k11s2366gap8c9pfji1j5vc667h3h72vl7q7gd3r")))

(define-public crate-theorafile-rs-0.1.2 (c (n "theorafile-rs") (v "0.1.2") (d (list (d (n "cc") (r "^1.0") (f (quote ("parallel"))) (d #t) (k 1)) (d (n "walkdir") (r "^2.3") (d #t) (k 1)))) (h "0zacgn79gdks9s7dgg0vdg34ajggf9m4fhdl60rwdsgafnd12li5")))

