(define-module (crates-io th eo theobserver) #:use-module (crates-io))

(define-public crate-theobserver-0.1.0 (c (n "theobserver") (v "0.1.0") (d (list (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "0k73cjzh42z8cynj6jv3nz3xa8nnjcwlmd3s5m6c32k5a6q0jlaa") (f (quote (("tilt") ("position"))))))

(define-public crate-theobserver-0.2.0 (c (n "theobserver") (v "0.2.0") (d (list (d (n "error-utils-derive") (r "^0.1.3") (d #t) (k 0)) (d (n "evdev") (r "^0.12.1") (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)))) (h "11ibqzrzmqr07wnrdjs738kvh4kk0csfzvxhmqd1my75m44ra3l4")))

