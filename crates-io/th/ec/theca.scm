(define-module (crates-io th ec theca) #:use-module (crates-io))

(define-public crate-theca-1.0.0 (c (n "theca") (v "1.0.0") (d (list (d (n "clippy") (r "^0.0.80") (o #t) (d #t) (k 0)) (d (n "docopt") (r "^0.6.81") (d #t) (k 0)) (d (n "libc") (r "^0.2.13") (d #t) (k 0)) (d (n "rand") (r "^0.3.14") (d #t) (k 0)) (d (n "regex") (r "^0.1.71") (d #t) (k 0)) (d (n "rust-crypto") (r "^0.2.36") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.19") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.4") (d #t) (k 0)) (d (n "term") (r "^0.4.4") (d #t) (k 0)) (d (n "time") (r "^0.1.35") (d #t) (k 0)))) (h "0q6gzvql1i4icbwgm291bvd1402mv9nzag7273pm8c27r68zx402") (f (quote (("unstable" "clippy") ("default"))))))

