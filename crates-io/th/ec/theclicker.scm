(define-module (crates-io th ec theclicker) #:use-module (crates-io))

(define-public crate-theclicker-0.1.2 (c (n "theclicker") (v "0.1.2") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "input-linux") (r "^0.6.0") (d #t) (k 0)) (d (n "input-linux-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0g39aa62gqjshhmnhvdh325b2g5ybv94nf27mq2jfdq8yazb336v")))

(define-public crate-theclicker-0.1.3 (c (n "theclicker") (v "0.1.3") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "input-linux") (r "^0.6.0") (d #t) (k 0)) (d (n "input-linux-sys") (r "^0.8.0") (d #t) (k 0)))) (h "1ab1mvws4m4741s2z8xvlimzhmgb6c57bnjl6an3h6fbs7vjjanw")))

(define-public crate-theclicker-0.1.4 (c (n "theclicker") (v "0.1.4") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "input-linux") (r "^0.6.0") (d #t) (k 0)) (d (n "input-linux-sys") (r "^0.8.0") (d #t) (k 0)))) (h "03yi5szp7ba6dkw97pf6fx102wdqplr9bd1kchsn2jh974w3qzcn")))

(define-public crate-theclicker-0.1.5 (c (n "theclicker") (v "0.1.5") (d (list (d (n "base64") (r "^0.21.4") (d #t) (k 0)) (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "input-linux") (r "^0.6.0") (d #t) (k 0)) (d (n "input-linux-sys") (r "^0.8.0") (d #t) (k 0)))) (h "0kvlm1g78p73fs6lwx7sxh7dsvrmlk39dh3km76f5cv6d34ijnf1")))

(define-public crate-theclicker-0.1.6 (c (n "theclicker") (v "0.1.6") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "input-linux") (r "^0.6.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "0krs38qif8blz8cz6gam2fx44vpvf81z4cq1knr2hjymfv4v01ig")))

(define-public crate-theclicker-0.1.7 (c (n "theclicker") (v "0.1.7") (d (list (d (n "clap") (r "^4.4.3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "input-linux") (r "^0.6.0") (d #t) (k 0)) (d (n "ron") (r "^0.8.1") (d #t) (k 0)) (d (n "serde") (r "^1.0.190") (f (quote ("derive"))) (d #t) (k 0)))) (h "0if3akdwrbwx6q3hfxv4lkyg2cc9pmgqwnj7xaf13aakd9bc8aan")))

