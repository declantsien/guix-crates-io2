(define-module (crates-io th em themelio-bootstrap) #:use-module (crates-io))

(define-public crate-themelio-bootstrap-0.1.0 (c (n "themelio-bootstrap") (v "0.1.0") (d (list (d (n "once_cell") (r "^1.8.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.5.0") (d #t) (k 0)) (d (n "themelio-stf") (r "^0.6.3") (d #t) (k 0)))) (h "1kba23lhy1dwbcv52mw84zczfrvillqzjpjwzvjw4h3xm1nx9l7l")))

(define-public crate-themelio-bootstrap-0.2.0 (c (n "themelio-bootstrap") (v "0.2.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.6.0") (d #t) (k 0)) (d (n "themelio-stf") (r "^0.6.11") (d #t) (k 0)))) (h "023c7fk4jq0aafhg2sdib7lynkynbli8sc4wgrbyg93xf3n9dzgd")))

(define-public crate-themelio-bootstrap-0.3.0 (c (n "themelio-bootstrap") (v "0.3.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.7.0") (d #t) (k 0)) (d (n "themelio-stf") (r "^0.7.1") (d #t) (k 0)))) (h "0wr7d666gxm5isva1i5ah277lif1x0fq99bbgpq33k20ks4bx4li")))

(define-public crate-themelio-bootstrap-0.4.0 (c (n "themelio-bootstrap") (v "0.4.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.9.0") (d #t) (k 0)) (d (n "themelio-structs") (r "^0.1.2") (d #t) (k 0)))) (h "0raswqfh8aa807svhhyjp5mx3d7b7dl3bm870ps3niy0p1nzn7p2")))

(define-public crate-themelio-bootstrap-0.5.0 (c (n "themelio-bootstrap") (v "0.5.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.10.0") (d #t) (k 0)) (d (n "themelio-structs") (r "^0.2") (d #t) (k 0)))) (h "0asgvi9dym39wv67jxpm2lx7biir8hpjvfp2jh3mj9340cgajivn")))

(define-public crate-themelio-bootstrap-0.5.1 (c (n "themelio-bootstrap") (v "0.5.1") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.10.0") (d #t) (k 0)) (d (n "themelio-structs") (r "^0.2") (d #t) (k 0)))) (h "1cqwm7y9sbb6ql6d6l7qkh1z1s0z5y6rgmqi0jzlcpxl15mz9xvy")))

(define-public crate-themelio-bootstrap-0.5.2 (c (n "themelio-bootstrap") (v "0.5.2") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.10.0") (d #t) (k 0)) (d (n "themelio-structs") (r "^0.2") (d #t) (k 0)))) (h "1ki4x3w6k88qajwmlfs8zcmkp3i72gcr8vy2czy0qr0bacssasb5")))

(define-public crate-themelio-bootstrap-0.6.0 (c (n "themelio-bootstrap") (v "0.6.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.11.0") (d #t) (k 0)) (d (n "themelio-structs") (r "^0.2") (d #t) (k 0)))) (h "1p5i7w0v57f8k1qqk7q4qgnlfcjcpmmzsa45x8fy9why8p7y226h")))

(define-public crate-themelio-bootstrap-0.6.1 (c (n "themelio-bootstrap") (v "0.6.1") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.11.0") (d #t) (k 0)) (d (n "themelio-structs") (r "^0.2") (d #t) (k 0)))) (h "1n9m81m99kka8rbxs5lj3mr8rwip2h3mlnknqid3zhsfspw9xzxd")))

(define-public crate-themelio-bootstrap-0.7.0 (c (n "themelio-bootstrap") (v "0.7.0") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.12.0") (d #t) (k 0)) (d (n "themelio-structs") (r "^0.2") (d #t) (k 0)))) (h "0i32r2cl3kcvxpm6aydmpzvp0500129cqyl4gihvxjsqh0x4wx1y")))

(define-public crate-themelio-bootstrap-0.7.1 (c (n "themelio-bootstrap") (v "0.7.1") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.12.0") (d #t) (k 0)) (d (n "themelio-structs") (r "^0.2") (d #t) (k 0)))) (h "01r5chwwga996qsjw3aspvrj6msgs5d4nxmfgp7c5p02c4lsrd95")))

(define-public crate-themelio-bootstrap-0.7.2 (c (n "themelio-bootstrap") (v "0.7.2") (d (list (d (n "once_cell") (r "^1.9.0") (d #t) (k 0)) (d (n "themelio-nodeprot") (r "^0.12.0") (d #t) (k 0)) (d (n "themelio-structs") (r "^0.2") (d #t) (k 0)))) (h "1qdigm14y6ji98b224hpkz2flrymadfpkqn1w2q4pdcn28jwgkzp")))

