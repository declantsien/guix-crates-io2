(define-module (crates-io th em themeparks) #:use-module (crates-io))

(define-public crate-themeparks-0.1.0 (c (n "themeparks") (v "0.1.0") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "1qbp3cp66ifjj7ln767hcz9rvxib9nxja1sk1mqsw303838arhzm") (y #t)))

(define-public crate-themeparks-0.1.1 (c (n "themeparks") (v "0.1.1") (d (list (d (n "serde") (r "^1.0.145") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.85") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.37") (d #t) (k 0)) (d (n "ureq") (r "^2.5.0") (f (quote ("json" "charset"))) (d #t) (k 0)))) (h "18s5p9mlyjfdl34jyncbkj5mc5g5jgi7h6alh647hjvnsggybspq")))

