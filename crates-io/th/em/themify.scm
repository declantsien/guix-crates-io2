(define-module (crates-io th em themify) #:use-module (crates-io))

(define-public crate-themify-0.1.0 (c (n "themify") (v "0.1.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "1cilzsj0y13kqz9ljshcrfxc0mj6dgsaiq6x4j9176wnrk6v8ngs")))

(define-public crate-themify-0.1.1 (c (n "themify") (v "0.1.1") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)))) (h "0q1d2mcn7qd176zi73sksx41b2ak3yh0555rl0zgwdqczcshsyam")))

(define-public crate-themify-0.1.2 (c (n "themify") (v "0.1.2") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "10ryfd1qg0pzmrhk9s7lm0p7xrjdibr5r2735vppkq4vapx33cqv")))

(define-public crate-themify-0.1.3 (c (n "themify") (v "0.1.3") (d (list (d (n "dirs") (r "^4.0") (d #t) (k 0)) (d (n "hex") (r "^0.4.3") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "structopt") (r "^0.3.25") (d #t) (k 0)))) (h "03pj2b61bvil3qwvpl6g7zfh97vp72dhcx9gm531c5c3w2h4lllm")))

