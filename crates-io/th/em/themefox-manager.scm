(define-module (crates-io th em themefox-manager) #:use-module (crates-io))

(define-public crate-themefox-manager-0.9.9 (c (n "themefox-manager") (v "0.9.9") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "0w0f08n7987rqqhp4glnwd3kcj1vcd399r38rc0dr7za92lkf87m")))

(define-public crate-themefox-manager-0.9.10 (c (n "themefox-manager") (v "0.9.10") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "0jjimm5j9sh8lrcfdjwihic9k2ljhq91frhd3zm93gs129yv09hy")))

(define-public crate-themefox-manager-0.9.11 (c (n "themefox-manager") (v "0.9.11") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "colored") (r "^1.9.3") (d #t) (k 0)) (d (n "dialoguer") (r "^0.6.2") (d #t) (k 0)) (d (n "dirs") (r "^2.0.2") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "zip") (r "^0.5.5") (d #t) (k 0)))) (h "09nllsa04hna7qs8ivyh1k1v5bqk8xaq9j7q59dkjg9y27pnhwrp")))

