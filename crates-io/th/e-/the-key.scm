(define-module (crates-io th e- the-key) #:use-module (crates-io))

(define-public crate-the-key-0.0.1 (c (n "the-key") (v "0.0.1") (h "0qiv2kjawmlzlbvq2505dbrgbqiigh94y1f4pn6xvrrrzcabj5bx") (y #t)))

(define-public crate-the-key-0.0.2 (c (n "the-key") (v "0.0.2") (h "11v4whig0mgnaj4qn4052zrmphv4y0mgsw65znnzxiyxa2gwjshh") (y #t)))

(define-public crate-the-key-0.0.3 (c (n "the-key") (v "0.0.3") (h "09bn48lvx77injbn9ghqd59aral2gwkkm2css1sz9dblcgadd5z0") (y #t)))

(define-public crate-the-key-0.0.4 (c (n "the-key") (v "0.0.4") (h "1w3a74fv0qgznxgf9q8cgawkjpi8llfb7iqs983vvpzc9dxnlaim") (y #t)))

(define-public crate-the-key-0.0.4-beta.1 (c (n "the-key") (v "0.0.4-beta.1") (h "10k39zkqpd3hq7c5p84kl44dn080hajw3bklwc1v4xhc13gwhp0q") (y #t)))

(define-public crate-the-key-0.0.5-beta.1 (c (n "the-key") (v "0.0.5-beta.1") (h "1q50dciri141vcgxh3z7pxck97jfisbfxfn7671wp06zc06s4452") (y #t)))

(define-public crate-the-key-0.0.5-beta.2 (c (n "the-key") (v "0.0.5-beta.2") (h "0h8wp8bixa1w7q7xg2724ccws217870gxjq77mpgy0i8g63ykwyf") (y #t)))

(define-public crate-the-key-0.0.5-beta.3 (c (n "the-key") (v "0.0.5-beta.3") (h "1spqm3zg4hl3s6dyy02naahp4prnhqi9lnb4hv6c265qjy0wbr3q") (y #t)))

(define-public crate-the-key-0.0.5-beta.4 (c (n "the-key") (v "0.0.5-beta.4") (h "0q88rqxhv0zw0adcxwphd7njaq3rynsg7r4am8iqclisk27m9bwg") (y #t)))

(define-public crate-the-key-0.0.5-beta.5 (c (n "the-key") (v "0.0.5-beta.5") (h "104zgcwraw9g4c9s5azgx95yc436jxwawgxsdns217lpxk3xb3cb") (y #t)))

(define-public crate-the-key-0.1.0-beta.1 (c (n "the-key") (v "0.1.0-beta.1") (h "1c8mya111hrl81c5w9vwv72842ipzpxlb2a41px9i7ri2g2g8gjl")))

(define-public crate-the-key-0.1.0-beta.2 (c (n "the-key") (v "0.1.0-beta.2") (h "0fxg10hjzwmci7zk09d9h649myzf351g2gl456yby2s16q35mdyl")))

(define-public crate-the-key-0.1.0 (c (n "the-key") (v "0.1.0") (h "1gkn16r95cbzg2fvzbdzb8fa085fiqc2wl0cvn8v87rxip5v9frf")))

(define-public crate-the-key-0.1.1 (c (n "the-key") (v "0.1.1") (h "1mcqzpw3p222yjm38vaxk9xbdwrgxwn42q2j28d6x7ffyrajjnbl")))

