(define-module (crates-io th e- the-media-organizer) #:use-module (crates-io))

(define-public crate-the-media-organizer-1.0.0 (c (n "the-media-organizer") (v "1.0.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "viperus") (r "^0.1.10") (d #t) (k 0)))) (h "0xsddyz966c3klsykhf73354n9ia41nbkxaqxa7sy2hjfrvv35dr")))

(define-public crate-the-media-organizer-1.1.0 (c (n "the-media-organizer") (v "1.1.0") (d (list (d (n "assert_cmd") (r "^2.0.2") (d #t) (k 2)) (d (n "clap") (r "^2.34.0") (d #t) (k 0)) (d (n "color-eyre") (r "^0.5.11") (d #t) (k 0)) (d (n "directories") (r "^4.0.1") (d #t) (k 0)) (d (n "kamadak-exif") (r "^0.5.4") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "tempfile") (r "^3") (d #t) (k 2)) (d (n "viperus") (r "^0.1.10") (d #t) (k 0)))) (h "0ym4ppbcqwq1hgqpayyrrppndzqqzmy31zclsif4nra8kb3dndmy")))

