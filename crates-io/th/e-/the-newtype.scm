(define-module (crates-io th e- the-newtype) #:use-module (crates-io))

(define-public crate-the-newtype-0.1.0 (c (n "the-newtype") (v "0.1.0") (d (list (d (n "the-newtype-core") (r "^0.1.0") (d #t) (k 0)) (d (n "the-newtype-macros") (r "^0.1.0") (d #t) (k 0)))) (h "00y4smxhlsfdriyp3fbazv52r3qh1g8zapl98a83sa6g6ipi1lvy")))

(define-public crate-the-newtype-0.1.1 (c (n "the-newtype") (v "0.1.1") (d (list (d (n "the-newtype-core") (r "^0.1.1") (d #t) (k 0)) (d (n "the-newtype-macros") (r "^0.1.1") (d #t) (k 0)) (d (n "derive_more") (r "^0.99") (d #t) (k 2)))) (h "15w8wpf681cf6g3nmmb2qbf0cssjcs5qcqwmhgy9c3h60nmcmxcq")))

