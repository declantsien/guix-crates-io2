(define-module (crates-io th e- the-newtype-macros) #:use-module (crates-io))

(define-public crate-the-newtype-macros-0.1.0 (c (n "the-newtype-macros") (v "0.1.0") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "182a30l1b22hf0h60gvzjrzrq6p58qjifxkxlvcikhx714fkfy7d")))

(define-public crate-the-newtype-macros-0.1.1 (c (n "the-newtype-macros") (v "0.1.1") (d (list (d (n "proc-macro-crate") (r "^1") (d #t) (k 0)) (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "15k3r7j04b9ys22xx0q1y4xp0dyj45j61ir64ppwzhm9hz6a0s4j")))

