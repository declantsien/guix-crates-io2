(define-module (crates-io th e- the-newtype-core) #:use-module (crates-io))

(define-public crate-the-newtype-core-0.1.0 (c (n "the-newtype-core") (v "0.1.0") (h "0hqvk6w6imrg08fcrx6ig52zsp5li4c3wi2s6drp39bp3m077c7w")))

(define-public crate-the-newtype-core-0.1.1 (c (n "the-newtype-core") (v "0.1.1") (h "0f45l0ngiwmn2b7yxhl6gmcggnk92akvwnx5gp8idqcbb3pzh23l")))

