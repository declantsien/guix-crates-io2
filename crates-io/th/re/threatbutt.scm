(define-module (crates-io th re threatbutt) #:use-module (crates-io))

(define-public crate-threatbutt-0.0.1 (c (n "threatbutt") (v "0.0.1") (d (list (d (n "hyper") (r "0.3.*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1yclbj41mp78kjxqbszq1a7z32k2fkzjv8yqhggv45isd430x5na")))

(define-public crate-threatbutt-0.0.2 (c (n "threatbutt") (v "0.0.2") (d (list (d (n "hyper") (r "0.3.*") (d #t) (k 0)) (d (n "url") (r "*") (d #t) (k 0)))) (h "1354hpbsr7898cr5xsg2hk7s2h65gfxjhgmjfb53qm28y2skzpw5")))

