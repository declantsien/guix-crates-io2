(define-module (crates-io th re threadstack) #:use-module (crates-io))

(define-public crate-threadstack-0.1.0 (c (n "threadstack") (v "0.1.0") (h "109a7dh0irbbbg75g7i7sg8wds2mwswz4h9af1pnfvxwl62bs2ik")))

(define-public crate-threadstack-0.2.0 (c (n "threadstack") (v "0.2.0") (h "01ialqasb0xhivwmyxc6rqllh9x82fr0d2lnn96gsn9q360g6s2f")))

(define-public crate-threadstack-0.3.0 (c (n "threadstack") (v "0.3.0") (h "1nairs35wn2gqgskzjcbqrybwfpa488r59vhcqshbx5aggy6k88q")))

(define-public crate-threadstack-0.4.0 (c (n "threadstack") (v "0.4.0") (d (list (d (n "rel-ptr") (r "^0.2.3") (d #t) (k 0)))) (h "0s2nq1h9b0asbs53jdsqd8h6avxmm8zr8q6j3vggy0whbg8pbbk2")))

(define-public crate-threadstack-0.4.1 (c (n "threadstack") (v "0.4.1") (d (list (d (n "rel-ptr") (r "^0.2.3") (d #t) (k 0)))) (h "1yy5lf1v27l1zwdvwydzvr3022d6n95b6cpj06knhg663fgrvgw4")))

