(define-module (crates-io th re threadinfo) #:use-module (crates-io))

(define-public crate-threadinfo-0.1.0 (c (n "threadinfo") (v "0.1.0") (d (list (d (n "libc") (r "^0.2.45") (d #t) (t "cfg(target_os = \"linux\")") (k 0)) (d (n "mach") (r "^0.2.3") (d #t) (t "cfg(target_os = \"macos\")") (k 0)) (d (n "serde") (r "^1.0.69") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.69") (d #t) (k 0)))) (h "17dixvhdm6w9xwl2xb0ym8g1hk7psphjazmb2d4jfj5ym7p47zwa")))

