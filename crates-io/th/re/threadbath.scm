(define-module (crates-io th re threadbath) #:use-module (crates-io))

(define-public crate-threadbath-0.1.0 (c (n "threadbath") (v "0.1.0") (h "04jlk7gbjya6n3j3ycfr5k0i5j851c2fh18pi5wrkk2rwssmq1r2")))

(define-public crate-threadbath-0.1.1 (c (n "threadbath") (v "0.1.1") (h "004w7da1lrc8i4v8n7pq0z9kfhfg3cv45as4nc032fs3y1s5f01x")))

