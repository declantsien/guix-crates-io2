(define-module (crates-io th re three-set-compare) #:use-module (crates-io))

(define-public crate-three-set-compare-0.1.0 (c (n "three-set-compare") (v "0.1.0") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "unidecode") (r "^0.3") (d #t) (k 0)))) (h "0mk90z1gb536xylr5qb4w6c28q1frq1yc73kcg8z2kn0cyzp4gba")))

(define-public crate-three-set-compare-0.1.1 (c (n "three-set-compare") (v "0.1.1") (d (list (d (n "assert_approx_eq") (r "^1.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1") (d #t) (k 0)) (d (n "unidecode") (r "^0.3") (d #t) (k 0)))) (h "1rrjjvqkbs5f9mrhg7pb6zrx6q4v554zs2pgwj7zv8ibyfmnp262")))

