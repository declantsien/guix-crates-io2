(define-module (crates-io th re thread-scoped-ref) #:use-module (crates-io))

(define-public crate-thread-scoped-ref-0.1.0 (c (n "thread-scoped-ref") (v "0.1.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "11bzr8rh0l0xs8zrllh6lgx76r20ypgjfhk84lc6p23k0bfij3g5")))

(define-public crate-thread-scoped-ref-0.1.1 (c (n "thread-scoped-ref") (v "0.1.1") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1lhbwq2glx423b008qa584phmf0gz85spk3b84lik663fkraj0v1")))

(define-public crate-thread-scoped-ref-0.1.2 (c (n "thread-scoped-ref") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0dhfxhsf0fkgcmc7hyp5lgm1aafpsp32pxs7ih4yqw8fhzfxllhf")))

(define-public crate-thread-scoped-ref-0.1.3 (c (n "thread-scoped-ref") (v "0.1.3") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0nfvbgmirfs215h9fp715bfiiksn3r1lqq11shmxh2438p9p3afn")))

(define-public crate-thread-scoped-ref-0.1.4 (c (n "thread-scoped-ref") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0j3p89b1zda799d5rn61q7pjjqdaqdaw6pw1s19wzzisya3wxa8r")))

