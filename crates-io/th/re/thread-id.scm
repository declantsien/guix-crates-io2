(define-module (crates-io th re thread-id) #:use-module (crates-io))

(define-public crate-thread-id-1.0.0 (c (n "thread-id") (v "1.0.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "00x9w8pc2kwwfvbas9rap5lxf01hv8y5s9lh00hf61nf7q97x4dw")))

(define-public crate-thread-id-2.0.0 (c (n "thread-id") (v "2.0.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (k 0)))) (h "00zzs2bx1xw8aqm5plqqgr7bc2zz6zkqrdxq8vpiqb8hc2srslx9")))

(define-public crate-thread-id-3.0.0 (c (n "thread-id") (v "3.0.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 0)))) (h "0jn13q459lz46l9r8qqy62ngzc9zbcw5p6k282g143f7b1swjds4")))

(define-public crate-thread-id-3.1.0 (c (n "thread-id") (v "3.1.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 0)))) (h "0wzpy6652xqhjd44526l0zkyvr9424xymplnzpgzmpbgcxdqgxwd")))

(define-public crate-thread-id-3.2.0 (c (n "thread-id") (v "3.2.0") (d (list (d (n "kernel32-sys") (r "^0.2.1") (d #t) (t "cfg(windows)") (k 0)) (d (n "libc") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)))) (h "17ccsriml5hkfyd12a1c2a22fpv877fplwza796mr8v9k8lddx1a")))

(define-public crate-thread-id-3.3.0 (c (n "thread-id") (v "3.3.0") (d (list (d (n "libc") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.1") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1h90v19fjz3x9b25ywh68z5yf2zsmm6h5zb4rl302ckbsp4z9yy7")))

(define-public crate-thread-id-4.0.0 (c (n "thread-id") (v "4.0.0") (d (list (d (n "libc") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0zvikdngp0950hi0jgiipr8l36rskk1wk7pc8cd43xr3g5if1psz")))

(define-public crate-thread-id-4.1.0 (c (n "thread-id") (v "4.1.0") (d (list (d (n "libc") (r "^0.2.6") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0wwx6h4xxyc3nyips18l31bij069y91qhm4ij3n0y71kp2i3ms9y")))

(define-public crate-thread-id-4.2.0 (c (n "thread-id") (v "4.2.0") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "redox_syscall") (r "^0.2") (d #t) (t "cfg(target_os = \"redox\")") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0sb6fpdnpyf9p6szn4wi0db5lzrgr58lmqqd3a3w9kb16mblyivr")))

(define-public crate-thread-id-4.2.1 (c (n "thread-id") (v "4.2.1") (d (list (d (n "libc") (r "^0.2.147") (d #t) (t "cfg(unix)") (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("processthreadsapi"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02xph1955cpsriazs73hnwgjs1fzi8a2zgjplbm0vdcydv283v7h")))

