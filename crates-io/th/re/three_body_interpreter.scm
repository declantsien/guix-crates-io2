(define-module (crates-io th re three_body_interpreter) #:use-module (crates-io))

(define-public crate-three_body_interpreter-0.4.0 (c (n "three_body_interpreter") (v "0.4.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "0nc731nw00pa1prvz9kf5dv0ydjv0f5am7n5kbyhi6841cr7k3az")))

(define-public crate-three_body_interpreter-0.4.1 (c (n "three_body_interpreter") (v "0.4.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "06i6b3rrmqkykggmazg0l7nxlh9kxkwcv2fh1jbv17xhbmwh5kkf")))

(define-public crate-three_body_interpreter-0.4.3 (c (n "three_body_interpreter") (v "0.4.3") (d (list (d (n "rand") (r "^0.7.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "1jgg7jzq7q8pdrl4bk5fzrfwcvwyp3b0mw00v2nww63sx8kipbq8")))

(define-public crate-three_body_interpreter-0.4.5 (c (n "three_body_interpreter") (v "0.4.5") (d (list (d (n "rand") (r "^0.7.3") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "1fhw0rsamxh66l5ym9ckwykps9ibvxrg95qlg8dhbgda5lvjd3ma")))

(define-public crate-three_body_interpreter-0.6.0 (c (n "three_body_interpreter") (v "0.6.0") (d (list (d (n "llm") (r "^0.1.1") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "llm-base") (r "^0.1.1") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "spinoff") (r "^0.7.0") (f (quote ("dots" "arc" "line"))) (o #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "03i4r8kv0vyybqz4bl6g1dkkv310y1qm1ldqx3lgwzq8gh61lgy2") (f (quote (("sophon" "llm" "llm-base" "spinoff") ("default"))))))

(define-public crate-three_body_interpreter-0.6.1 (c (n "three_body_interpreter") (v "0.6.1") (d (list (d (n "llm") (r "^0.1.1") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "llm-base") (r "^0.1.1") (o #t) (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "spinoff") (r "^0.7.0") (f (quote ("dots" "arc" "line"))) (o #t) (t "cfg(not(target_arch = \"wasm32\"))") (k 0)) (d (n "unicode-xid") (r "^0.2.1") (d #t) (k 0)))) (h "072rbwx17jvgs4ic3qjjrpp5cg446qix40ya9hradri7dk1sx6kz") (f (quote (("sophon" "llm" "llm-base" "spinoff") ("default"))))))

