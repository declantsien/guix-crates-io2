(define-module (crates-io th re thread-control) #:use-module (crates-io))

(define-public crate-thread-control-0.1.0 (c (n "thread-control") (v "0.1.0") (h "1hmlcvd0mlpl521dwb3f28wx15n8yh54m71fq0hw4bc33zwq7v2f")))

(define-public crate-thread-control-0.1.1 (c (n "thread-control") (v "0.1.1") (h "1978lqiimwyciq35q025cckb7js1b84k39a0xqn2hayvx264gmz6")))

(define-public crate-thread-control-0.1.2 (c (n "thread-control") (v "0.1.2") (h "1sif3ij5ikkr0lpn14571rwzyx8vpzapgznndzky9k2j2d1h5q12")))

