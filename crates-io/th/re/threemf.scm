(define-module (crates-io th re threemf) #:use-module (crates-io))

(define-public crate-threemf-0.1.0 (c (n "threemf") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1ni1rca4f7sqvrzgkbwdvrr3434yj5igqnkg2cd07abxbdnx7a7z") (r "1.56")))

(define-public crate-threemf-0.2.0 (c (n "threemf") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "14ycwr1ydm878dvb1kxpcga2shra3ryf85cm6n4gli40r808ijf0") (r "1.56")))

(define-public crate-threemf-0.3.0 (c (n "threemf") (v "0.3.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "zip") (r "^0.5.13") (d #t) (k 0)))) (h "1wzz3k46ljwijwhg0860jjsqaqq34bidh6zbip1lmibpshc2i882") (r "1.56")))

(define-public crate-threemf-0.3.1 (c (n "threemf") (v "0.3.1") (d (list (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (f (quote ("deflate"))) (k 0)))) (h "1k2fc2s4d399fik7h6f84n6zpnv8kkfsykqccgql6mxaxq3lj283") (r "1.56")))

(define-public crate-threemf-0.4.0 (c (n "threemf") (v "0.4.0") (d (list (d (n "quick-xml") (r "^0.27.1") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (f (quote ("deflate"))) (k 0)))) (h "13l08ck1984bjfjvg9r20m18rgyr7m7k7idkk4030rwqfv68l45z") (r "1.56")))

(define-public crate-threemf-0.5.0 (c (n "threemf") (v "0.5.0") (d (list (d (n "quick-xml") (r "^0.31.0") (f (quote ("serialize"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)) (d (n "zip") (r "^0.6.2") (f (quote ("deflate"))) (k 0)))) (h "08q1amp0k8v7rv0gjh9slvvnfwxra95dl9b39fspkiwdgni6wbjl") (r "1.56")))

