(define-module (crates-io th re threadx-sys) #:use-module (crates-io))

(define-public crate-threadx-sys-0.1.0 (c (n "threadx-sys") (v "0.1.0") (d (list (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0jiqkp9cfp5n3k15azj0jk5gj3cxs7j8i39p8vzn6hkp1cgqnqdb") (l "threadx")))

(define-public crate-threadx-sys-0.1.1 (c (n "threadx-sys") (v "0.1.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1c9qzmvsnilqfrs4kf8sgdfnw6q0n10pjmqnfg9c71chyk46i1zh") (l "threadx")))

(define-public crate-threadx-sys-0.1.2 (c (n "threadx-sys") (v "0.1.2") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "0ryi8xnzmm3d3h9x1pwvvhfn5i7w0157wisc12swsciy06zjijhx") (l "threadx")))

(define-public crate-threadx-sys-0.2.0 (c (n "threadx-sys") (v "0.2.0") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "1w6kngvfxvbi21mxbj75zfzbrajv870hwwffhy8zhw5kxgdcja5y") (l "threadx")))

(define-public crate-threadx-sys-0.2.1 (c (n "threadx-sys") (v "0.2.1") (d (list (d (n "bindgen") (r "^0.69.1") (d #t) (k 1)) (d (n "cmake") (r "^0.1.50") (d #t) (k 1)))) (h "01d43phd6hqqnl2pzzlfgq028fgdqmkz6111zy26a40r9a66hjc8") (l "threadx")))

