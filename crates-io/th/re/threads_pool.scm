(define-module (crates-io th re threads_pool) #:use-module (crates-io))

(define-public crate-threads_pool-0.1.0 (c (n "threads_pool") (v "0.1.0") (h "1kw27gzcflq958gp3jfs5bpz5pghcw6v9rpili1f4n0bdb39g9f6") (y #t)))

(define-public crate-threads_pool-0.1.1 (c (n "threads_pool") (v "0.1.1") (h "187q6dqa5d1zr5smqqz5cs0l4p6g9m0gli3xf1nk1lfaxfx7ml0y")))

(define-public crate-threads_pool-0.1.2 (c (n "threads_pool") (v "0.1.2") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0hy2cshn69xh3kaax3jsnlgcpxm6fllm3j7s1v4b5sr31s72w9ff")))

(define-public crate-threads_pool-0.1.3 (c (n "threads_pool") (v "0.1.3") (h "00scz0ljvd3drjqaalfmcrwr8cvp4vm500brx2zllzl0qvbi58jc")))

(define-public crate-threads_pool-0.1.4 (c (n "threads_pool") (v "0.1.4") (h "015ngq8nagw0n6j018f3favyivb80wpc57qbfifch6m6l9m1j6v5")))

(define-public crate-threads_pool-0.1.5 (c (n "threads_pool") (v "0.1.5") (h "1ycx7621hwqrsn031hrkcy16hzs614vz1hi35w50kkk753pzrl2n")))

(define-public crate-threads_pool-0.1.6 (c (n "threads_pool") (v "0.1.6") (d (list (d (n "crossbeam-channel") (r "^0.2.0") (d #t) (k 0)))) (h "1fjfdwr7r8hl232jhqz8wkylhij5y57c5qcls2cjaprdmf6p04m3")))

(define-public crate-threads_pool-0.1.7 (c (n "threads_pool") (v "0.1.7") (d (list (d (n "crossbeam-channel") (r "^0.2.0") (d #t) (k 0)))) (h "0gdgani4vgkdl7lfbxkzaz00k94sy7bq25qmds0dlqm2c2ylcjai")))

(define-public crate-threads_pool-0.1.8 (c (n "threads_pool") (v "0.1.8") (d (list (d (n "crossbeam-channel") (r "^0.2.0") (d #t) (k 0)))) (h "0bqc5hdqxhyxm1q81cgpf5lym4xyp969asxqmwgrmknqmqgk989v")))

(define-public crate-threads_pool-0.1.9 (c (n "threads_pool") (v "0.1.9") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "1hn18slzd4npbpzbv5pgkhlg6rr1bd88zkqk9bzpbx3662kmji37")))

(define-public crate-threads_pool-0.1.10 (c (n "threads_pool") (v "0.1.10") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "1caqrsd1bki29v1c5qfz1004v4bprcy553gylh0xwspjhp941a6n")))

(define-public crate-threads_pool-0.1.11 (c (n "threads_pool") (v "0.1.11") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "08b1x6qm796kz0xamyhxd0prx13v1rlyp2sfh4anf0ffd9k2g7pn") (y #t)))

(define-public crate-threads_pool-0.1.12 (c (n "threads_pool") (v "0.1.12") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "0a89gbhdl1fdr9y63jnqm5h3rzsrz29viskl4891lkrgs2rqysbz")))

(define-public crate-threads_pool-0.1.13 (c (n "threads_pool") (v "0.1.13") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "0ybqgzv3cs83scfg775ws7mip5gzbdx1l6h5h7hwc4gpm6g245v4") (y #t)))

(define-public crate-threads_pool-0.1.14 (c (n "threads_pool") (v "0.1.14") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "1qzskfkd780idni3vl39xw1qd00y5pnrsvr225h6igibfhaxlpzk")))

(define-public crate-threads_pool-0.1.15 (c (n "threads_pool") (v "0.1.15") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)))) (h "0mmf1wjbbqw138icnp5h5rvvb3c2pls4yb0bc47cdp4c85hqbrpq")))

(define-public crate-threads_pool-0.1.16 (c (n "threads_pool") (v "0.1.16") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)))) (h "1jhkf0c94lvi984k63wvd8bisydbjp05z5583bpa4cdirrwzrld9")))

(define-public crate-threads_pool-0.1.17 (c (n "threads_pool") (v "0.1.17") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)))) (h "1hl0m35i9siwiw4naxz4kycwmmjgbxyq5zl1h5ln6zrmyc5ykgpg")))

(define-public crate-threads_pool-0.1.18 (c (n "threads_pool") (v "0.1.18") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "01ihsa4x3a7zx91h403w8hsmhyw9kccay459v3v72w5890jwjf3v")))

(define-public crate-threads_pool-0.1.19 (c (n "threads_pool") (v "0.1.19") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "0psywdik84sp02fazm69khc10ijb47r9807wdnwhs6g9yjhn1xgn")))

(define-public crate-threads_pool-0.2.0 (c (n "threads_pool") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "00f31pgb14f03zrp3x7f3nzwwbkasgmayz96wlrr718ksq3d1i5k")))

(define-public crate-threads_pool-0.2.1 (c (n "threads_pool") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "11f8lvly7cfsg9r8kxp7jhpyk394l83cbfygkz6cw0d1rklnc83c")))

(define-public crate-threads_pool-0.2.2 (c (n "threads_pool") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "0am9kgym07g6rvqhx294dzdl6504vmr5c0f7qfa25mfs8nls6dxz")))

(define-public crate-threads_pool-0.2.4 (c (n "threads_pool") (v "0.2.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "09jvb2l7xc15scfliv15aw96nv5i8j4p8d5n42xvv7aq3k7xgd6x")))

(define-public crate-threads_pool-0.2.5 (c (n "threads_pool") (v "0.2.5") (d (list (d (n "async-task") (r "^1.0.0") (d #t) (k 2)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "futures-channel") (r "^0.3.1") (d #t) (k 2)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "0kn8p2rc8b83w3gxhf525hy1m5xdvvy27aqfyr88yma9v1y4plai")))

(define-public crate-threads_pool-0.2.6 (c (n "threads_pool") (v "0.2.6") (d (list (d (n "async-task") (r "^3.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.3.0") (d #t) (k 0)) (d (n "crossbeam-deque") (r "^0.7.0") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.7.0") (d #t) (k 0)) (d (n "hashbrown") (r "^0.1.8") (d #t) (k 0)) (d (n "parking_lot") (r "^0.7.1") (d #t) (k 0)))) (h "1rfic4vaf9g3x4kl9dk4qz3mfhjvqblfl16l0gjpx951vyd2nvpi")))

