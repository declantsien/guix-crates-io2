(define-module (crates-io th re three_em_macro) #:use-module (crates-io))

(define-public crate-three_em_macro-0.1.0 (c (n "three_em_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "syn") (r "^1") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1gw8nkdk11flrql2zhz6r5hnzbyvpj8c6sa1mv4mn8sk5mkac6fb")))

