(define-module (crates-io th re threadcell) #:use-module (crates-io))

(define-public crate-threadcell-0.1.0 (c (n "threadcell") (v "0.1.0") (h "1vi706jczzlhwxvd3c7mq0qqkhh8q87xc87w47yqxshp9xfky6xy")))

(define-public crate-threadcell-0.2.0 (c (n "threadcell") (v "0.2.0") (h "1dx6f4brv50c19mjrw2558xr32mffv5ibzcp8l76r995gikpfbz2")))

(define-public crate-threadcell-0.3.0 (c (n "threadcell") (v "0.3.0") (h "1hvkzn30rxi9ja10j9aas9pmcg6allcrqz14fqs45i6ygrps438y")))

(define-public crate-threadcell-0.4.0 (c (n "threadcell") (v "0.4.0") (h "1si36m75s6aa3jyyhlrmnl18zib724mxj24nyw0irgwxc43scylg")))

(define-public crate-threadcell-0.5.0 (c (n "threadcell") (v "0.5.0") (h "0pkh5msy5araji7d31bkj6zimgp936fzb187sv6sl40rb50icb0m")))

(define-public crate-threadcell-0.6.0 (c (n "threadcell") (v "0.6.0") (h "0kzq90kcwz29k3f4lj07z1lqd70x92fb0f159sxvb0fwq2gw5290")))

(define-public crate-threadcell-0.7.0 (c (n "threadcell") (v "0.7.0") (h "14ccfafgzirbb9szaf44f6d4ab337ncsv5fs754q3q693xbiyaww")))

(define-public crate-threadcell-0.8.0 (c (n "threadcell") (v "0.8.0") (h "0fdmi56d2qyxf3x0d81b0sr16l3pv4gr79s1khz562w00ndq9552")))

(define-public crate-threadcell-0.8.1 (c (n "threadcell") (v "0.8.1") (h "01jy2sbq0935pllw9g9l57n1wshar6d3f6jmfk0zksiviv8zpww4")))

(define-public crate-threadcell-0.8.2 (c (n "threadcell") (v "0.8.2") (h "03hgl9pdxl4nddmzsr6q4l57prlml3h120v3r106iv9cfmmp453f")))

(define-public crate-threadcell-0.9.0 (c (n "threadcell") (v "0.9.0") (d (list (d (n "mutants") (r "^0.0.3") (d #t) (k 0)))) (h "1jk7vnsk48da3wixvm7ai9v21r6xpz6diriyyrp4k4v8x0yya85m")))

(define-public crate-threadcell-0.10.0 (c (n "threadcell") (v "0.10.0") (d (list (d (n "mutants") (r "^0.0.3") (d #t) (k 0)))) (h "0jslkjgbwbfc57zkaqcwsmkpcpc2w1caiqnwp6gpls3bac1xl81p")))

(define-public crate-threadcell-0.11.0 (c (n "threadcell") (v "0.11.0") (d (list (d (n "mutants") (r "^0.0.3") (d #t) (k 0)))) (h "1lm177krkkfl9v6m53zbr39wwf8ybl9l751h0h3n52yhcqjixpgj")))

(define-public crate-threadcell-0.12.0 (c (n "threadcell") (v "0.12.0") (d (list (d (n "mutants") (r "^0.0.3") (d #t) (k 0)))) (h "1wa4a8i7iwr08ilp2lrcllmy9nj2nr4kn5335rj7s4g9cndxsxd7")))

(define-public crate-threadcell-1.0.0 (c (n "threadcell") (v "1.0.0") (d (list (d (n "conf_test") (r "^0.3") (d #t) (k 1)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)))) (h "17b7hj1sjaj8q0mx21jlpdff2a6rf58i2jfblfws532p550g9kkj") (f (quote (("nightly_thread_id_value") ("default"))))))

(define-public crate-threadcell-1.0.1 (c (n "threadcell") (v "1.0.1") (d (list (d (n "conf_test") (r "^0.4") (d #t) (k 1)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)))) (h "0phjw6jv0s7jrm9h1sw19zh3r70b5bzcjwrd8vr8hhpg7bzw42b8") (f (quote (("nightly_thread_id_value") ("default"))))))

(define-public crate-threadcell-1.0.2 (c (n "threadcell") (v "1.0.2") (d (list (d (n "conf_test") (r "^0.5") (d #t) (k 1)) (d (n "mutants") (r "^0.0.3") (d #t) (k 0)))) (h "14qdmfk01qlqi279ygcz1asb72gb0c3vng975xklbzpypsdbz4qk") (f (quote (("nightly_thread_id_value") ("default"))))))

(define-public crate-threadcell-1.0.3 (c (n "threadcell") (v "1.0.3") (d (list (d (n "mutants") (r "^0.0.3") (d #t) (k 0)))) (h "0z39vs35a0d2cgqp603mq1f25m0lmwyx2rhgh0d2x7hf97y7s65a") (f (quote (("nightly_thread_id_value") ("nightly" "nightly_thread_id_value") ("default"))))))

