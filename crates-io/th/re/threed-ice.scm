(define-module (crates-io th re threed-ice) #:use-module (crates-io))

(define-public crate-threed-ice-0.0.1 (c (n "threed-ice") (v "0.0.1") (h "12f0vkhhcb3mjx2g35f2xxczm47d3clb5mbsqd6ir34k8s7f8755")))

(define-public crate-threed-ice-0.1.0 (c (n "threed-ice") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "0ci3b6kh7qkaqp0wxqzajm097rpah52006w7zzx56nbym8y74qmc")))

(define-public crate-threed-ice-0.2.0 (c (n "threed-ice") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "0x80ni30bb43k998hmz1xzlv9ph76syvm9b6shsx9a8p3wn5v6ss")))

(define-public crate-threed-ice-0.2.1 (c (n "threed-ice") (v "0.2.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "07swh3a34iyqi45kysfmy5rbrc3awfrvqw340xqkfnvgys636hba")))

(define-public crate-threed-ice-0.3.0 (c (n "threed-ice") (v "0.3.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "03c400r9371yhldwwb9773q2lzi555fr5rmfl96h07wzc0s9m7p9")))

(define-public crate-threed-ice-0.4.0 (c (n "threed-ice") (v "0.4.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "1fj10zlspc8g1bcdc9mqn2gl66j25vrwjl7n837ybw8vi9n0lwd6")))

(define-public crate-threed-ice-0.5.0 (c (n "threed-ice") (v "0.5.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "0skf69vd9dp33hbgkxpx35x7frls4b8hrq94n11nrf5v2k8vx7nd")))

(define-public crate-threed-ice-0.6.0 (c (n "threed-ice") (v "0.6.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "08zbr2z8ma4chagm4klzv8570q0q2kar6lzi5rss1kphh55lyi3z")))

(define-public crate-threed-ice-0.6.1 (c (n "threed-ice") (v "0.6.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "0i0zms73nnqf11bn3q2s4x97zdywpr39yfq8l7zn7k7v3mcd1xvb")))

(define-public crate-threed-ice-0.7.0 (c (n "threed-ice") (v "0.7.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "15zfpz8yzi5dwc2qa024f6zgqk32w02xxzbjmirc4v9kwkafwvc8")))

(define-public crate-threed-ice-0.7.1 (c (n "threed-ice") (v "0.7.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.10") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "0g3w55sddmrqbvdlxcvhl0g67qabb8iijbvl20avlhb777dl395i")))

(define-public crate-threed-ice-0.7.2 (c (n "threed-ice") (v "0.7.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.11") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "0j5iyjpvldmp8js2il3l8w80vr091iafc7sflg58lr6djl7prdic")))

(define-public crate-threed-ice-0.8.0 (c (n "threed-ice") (v "0.8.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.11") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "1scqr3128l2v87bhkvp200ac830zwyjrbv4w5l2gkmx8q5i60qb5")))

(define-public crate-threed-ice-0.9.0 (c (n "threed-ice") (v "0.9.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.13") (d #t) (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "1lmxrdfzhx8bmdi40jn335fm052rrqhljgmj9ibzlnqkhs8k0win")))

(define-public crate-threed-ice-0.9.1 (c (n "threed-ice") (v "0.9.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.14") (d #t) (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "04sxappdhcmiqjqb0pf922z9gz9gr4080vsbhhqzkba2wqrq7di2")))

(define-public crate-threed-ice-0.9.2 (c (n "threed-ice") (v "0.9.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.15") (d #t) (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "1sbkqmnhifdwn8gwqmcxbxh0vnjdzvvjnkccq94lnmm40y60n6p8")))

(define-public crate-threed-ice-0.9.3 (c (n "threed-ice") (v "0.9.3") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.17") (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "12qxrmhyfxfn5z69p311dcqs18610p8ml42ck4as03h839bj9mh3")))

(define-public crate-threed-ice-0.9.4 (c (n "threed-ice") (v "0.9.4") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.18") (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "1az07x49mhg7bjfsnmsnwa5s0pvq9rdi0dsh7n1ywcrcs8y7qw3i")))

(define-public crate-threed-ice-0.9.5 (c (n "threed-ice") (v "0.9.5") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.19") (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "0sg6wvkyr243hxphksxr1rhg0pn32w6i983yjg08dr3b94wx7grz")))

(define-public crate-threed-ice-0.10.0 (c (n "threed-ice") (v "0.10.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.19") (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "02rq9qlwy91sl17kirb1dw2n3yhp804msd1q2k0sx13kzs5dwqil")))

(define-public crate-threed-ice-0.10.1 (c (n "threed-ice") (v "0.10.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.19") (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "03pa6qr2f60l521y4yg0g782kp86dcszbrk7ai19aif6l3lsg5p7")))

(define-public crate-threed-ice-0.10.2 (c (n "threed-ice") (v "0.10.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "matrix") (r "^0.20") (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)) (d (n "threed-ice-sys") (r "*") (d #t) (k 0)))) (h "10qmrzng7qnv7x8iig0gjlrz1gdn06z9a3szvq898iyfyxkfirpd")))

(define-public crate-threed-ice-0.11.0 (c (n "threed-ice") (v "0.11.0") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "fixture") (r "^0.2") (d #t) (k 2)) (d (n "matrix") (r "^0.20") (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)) (d (n "threed-ice-sys") (r "^0.2") (d #t) (k 0)))) (h "15z8a7wlgyxpxl0hpjvgzpfyvrsn756na64lm98hyzh14wsnd57c")))

(define-public crate-threed-ice-0.11.1 (c (n "threed-ice") (v "0.11.1") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "fixture") (r "^0.2") (d #t) (k 2)) (d (n "matrix") (r "^0.21") (k 0)) (d (n "superlu") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)) (d (n "threed-ice-sys") (r "^0.2") (d #t) (k 0)))) (h "0lp1n3xrzcfn6nqpk1i2cdwc453kiibnk4255sai1i8lxb3iwnsa")))

(define-public crate-threed-ice-0.12.0 (c (n "threed-ice") (v "0.12.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "fixture") (r "^0.3") (d #t) (k 2)) (d (n "matrix") (r "^0.21") (k 0)) (d (n "superlu") (r "^0.3") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)) (d (n "threed-ice-sys") (r "^0.3") (d #t) (k 0)))) (h "1dmzfrwnm45a176idn01qf7bdw591l8fyyl3z2aw2i4qfbw1sz5s")))

