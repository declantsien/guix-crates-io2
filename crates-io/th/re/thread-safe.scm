(define-module (crates-io th re thread-safe) #:use-module (crates-io))

(define-public crate-thread-safe-0.1.0 (c (n "thread-safe") (v "0.1.0") (h "1y126xydnczvbix3162c0y40pha2h53961w36ik82pq3v1nxpvj2")))

(define-public crate-thread-safe-0.1.1 (c (n "thread-safe") (v "0.1.1") (h "13p29xh1ss0li02za4v1rkxb7lhphiiy560dl7x6ca1gl1c3wgvc")))

(define-public crate-thread-safe-0.1.2 (c (n "thread-safe") (v "0.1.2") (h "127yg0ycvzjx4lg6qqi2b4anwamgi3d6ynqyl666ncyb5ddfffkl")))

