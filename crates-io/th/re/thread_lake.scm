(define-module (crates-io th re thread_lake) #:use-module (crates-io))

(define-public crate-thread_lake-0.1.2 (c (n "thread_lake") (v "0.1.2") (h "0gh4j5ykggv8izr2mss9c6xaln648xdg84v6m1rmrh0k2hfs5pw1")))

(define-public crate-thread_lake-0.1.3 (c (n "thread_lake") (v "0.1.3") (h "1b85dikirmig8r7kkp11s628hhrwyypzjnw9mz9na850p5ijjib9")))

(define-public crate-thread_lake-0.1.4 (c (n "thread_lake") (v "0.1.4") (h "0163l76qf46kf52sr755k2vr5cyiyg9a780qjj41ksj7pah35w2n")))

