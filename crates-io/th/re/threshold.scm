(define-module (crates-io th re threshold) #:use-module (crates-io))

(define-public crate-threshold-0.1.0 (c (n "threshold") (v "0.1.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.6") (d #t) (k 2)))) (h "0nhndpp42cnfznc3kp5c9c34ijf548bwzzyrw1h5a9cjl0ysn4rl")))

(define-public crate-threshold-0.2.0 (c (n "threshold") (v "0.2.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1a6lsc9d485bbfyzkmmj3r6s7p2mi8a3m3iwszz7csn6sm672w3d")))

(define-public crate-threshold-0.2.1 (c (n "threshold") (v "0.2.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1glmmw8wg5qjzh55z2l5ld5a9vpsgdvrvq3bvzmv0xnbcbcpnhjx")))

(define-public crate-threshold-0.2.2 (c (n "threshold") (v "0.2.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1zy4554vrxvc4g5f8m7nynrdcfg27bhy2k74f05di6p0ix11b8xx")))

(define-public crate-threshold-0.2.3 (c (n "threshold") (v "0.2.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "00ylijjjqfrzmk8h12zk02sfi6kcr1rcgcwgrrr1p4h4qb1wzr5m")))

(define-public crate-threshold-0.2.4 (c (n "threshold") (v "0.2.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1lqk95gcjzgvc5ly4xnwgxk42g4bb00wld71f2syrpc87ii1rlms")))

(define-public crate-threshold-0.2.5 (c (n "threshold") (v "0.2.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "035580wqmcfy0fwnqan3wvh5gily0bcxq32lzwh5sbrf5va87p28")))

(define-public crate-threshold-0.2.6 (c (n "threshold") (v "0.2.6") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0nbmbh0gm2ipmrychlkcidpq3vz1wmraczpfwypk479np4ypixvx")))

(define-public crate-threshold-0.3.0 (c (n "threshold") (v "0.3.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "06s32mpximrzr5q9k2vag5zfimkfy2fmlv9gv9caj55r9lsi9xc3")))

(define-public crate-threshold-0.3.1 (c (n "threshold") (v "0.3.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0xpasj80mydwmgnxf0wlhpyhkry8k059bkyd088gick648gx2vmw")))

(define-public crate-threshold-0.3.2 (c (n "threshold") (v "0.3.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0anpxx1kcix0x7ggmbg7k6xhbppr6f0rmfxhr1rahkdzlsr03s37")))

(define-public crate-threshold-0.4.0 (c (n "threshold") (v "0.4.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1wmp52gm21lx2d8vhcifihn26frs6wkkqsxg4fnsdkdmx2mcsrdl")))

(define-public crate-threshold-0.4.1 (c (n "threshold") (v "0.4.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1gghd9n8cjq0zf0fw0xqnrzav8l6h7j9zbcsrnpsipswdmnsgbhz")))

(define-public crate-threshold-0.4.2 (c (n "threshold") (v "0.4.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "128hsnv9hxkcwnmr2iqnrv2jrxg08lp7yy06lj8336ipxvllb263")))

(define-public crate-threshold-0.4.3 (c (n "threshold") (v "0.4.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "11j12n1ifvh3yy48j4lm9l4n3a99bgq2bxhmn8wasqi5dkac6hkn")))

(define-public crate-threshold-0.4.4 (c (n "threshold") (v "0.4.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "0davqcdvnirhdmxlkl5abw3sizqb9lgvzrfmv8kys3np2nf35604")))

(define-public crate-threshold-0.4.5 (c (n "threshold") (v "0.4.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "14jxscgnzyaqx6g3nxdqnf2c2zjfg6ffrih9ydyj9mr34a3hg0sm")))

(define-public crate-threshold-0.5.0 (c (n "threshold") (v "0.5.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "18a43bj8bk810yysz0c5pxqsy97gwwbszcm0d2iwrzqa39j9jzch")))

(define-public crate-threshold-0.6.0 (c (n "threshold") (v "0.6.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "09mrnw0yapiw74n8faf6nd2hshxgrdb29pgahqxszd194chv42vi")))

(define-public crate-threshold-0.6.1 (c (n "threshold") (v "0.6.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "03cx277q5v2njgl1ch0clrfrgxalqjml6p54nzb6ysmcz6sjzl1h")))

(define-public crate-threshold-0.6.2 (c (n "threshold") (v "0.6.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "1np1rvylix8hz442qvv6wqsw4gwyh4iv9w4mdczpkwsc0j22jd6k")))

(define-public crate-threshold-0.6.3 (c (n "threshold") (v "0.6.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)))) (h "19ddyky9vv6q6rfsx2g23lsw9dw8gy2r21a0yw2ngkxjda79dazj")))

(define-public crate-threshold-0.6.4 (c (n "threshold") (v "0.6.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1h71s9v8p6qb9mdjnpyyi2ciw1yp9msk3raam6j7f1lhasx0qfik")))

(define-public crate-threshold-0.6.5 (c (n "threshold") (v "0.6.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1vws5g65vki97srs0n6x9rd22ndnxd8z9fs7fkby9id6r1wwjg2h")))

(define-public crate-threshold-0.6.6 (c (n "threshold") (v "0.6.6") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0rvy2kc4kmxqn6ha0sv7bw36qsiwh4h89prpfxy55gra4zv026pv")))

(define-public crate-threshold-0.6.7 (c (n "threshold") (v "0.6.7") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "00y7lid7f2h8i2w1878xr69zxfpwdsn6l132r8k7a49y15k13i4f")))

(define-public crate-threshold-0.6.8 (c (n "threshold") (v "0.6.8") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0l2gy3a17cnkg5z0sdjqs9kbffxbwbzci2ra456vqm6anj1gpp93")))

(define-public crate-threshold-0.7.0 (c (n "threshold") (v "0.7.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jbhbgppby08cgdm10gkhpsx8blvi1xk9j39kpm7y0jjxhsw9rh2")))

(define-public crate-threshold-0.7.1 (c (n "threshold") (v "0.7.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n8s8ipxa4qqszzxgjq9smf83gbpcsc4l9p6b3qdhygrh70rc0v8")))

(define-public crate-threshold-0.8.0 (c (n "threshold") (v "0.8.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09lj2fkqyx5irp6ljj2l6lhckzi87ayykfinryf5ah1ybrvp43aj")))

(define-public crate-threshold-0.8.1 (c (n "threshold") (v "0.8.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pa5cj1pxd5pwjp7i2v9n7dyp18sjwipkw51hypdrkqgq6s3k2fd")))

(define-public crate-threshold-0.8.2 (c (n "threshold") (v "0.8.2") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iyv2xkm7jwzrlnkp1p13jfl3xcj7x7jif20xj678zl8aaijbzps")))

(define-public crate-threshold-0.8.3 (c (n "threshold") (v "0.8.3") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "12xazbkcnp7mdbk02kj1fp3ql2h21w733yl2vpa5krbv2ixl0q86")))

(define-public crate-threshold-0.8.4 (c (n "threshold") (v "0.8.4") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1nzwa2zhci3rvq87q3spkq7zjfwfy7yiqn8l7whhlvfw5kyl8nvb")))

(define-public crate-threshold-0.8.5 (c (n "threshold") (v "0.8.5") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "01r49h7mp6w0wd5kivkbcbdvyhdy4sz35wlqiisvajkp625fib88")))

(define-public crate-threshold-0.8.6 (c (n "threshold") (v "0.8.6") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0xk5hph2rank6kv078ks45nyl3dfarrz2pjcajndwlby2qfix6qs")))

(define-public crate-threshold-0.8.7 (c (n "threshold") (v "0.8.7") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zpmqf2xjbcvw9ln5lcav1sggls0qzdqw8i9sl95vlnc2mhqp5hz")))

(define-public crate-threshold-0.8.8 (c (n "threshold") (v "0.8.8") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "04758wzgqfh5gvac5mwk39wz4yfmzhzsf28d6g9i524hdfs9krwh")))

(define-public crate-threshold-0.8.9 (c (n "threshold") (v "0.8.9") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0iqrzg9dkzkyw4limlmw5pg6x5qka2fqnrzxn5xrzs0jy2bmp92y")))

(define-public crate-threshold-0.8.10 (c (n "threshold") (v "0.8.10") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "019b0b0a7h9kfp6b3py2z6bjjmlnxdvl21qipvimpsf0nkb2xrd7")))

(define-public crate-threshold-0.8.11 (c (n "threshold") (v "0.8.11") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1sd7ri78awicy76fxmxwfss80zk2p5297kvwg8maj4z4phalpbq6")))

(define-public crate-threshold-0.8.12 (c (n "threshold") (v "0.8.12") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02lya7c0fs39dcg7zxri0d7xmry3b7kj0ix8js5jvp57dszm3vyv")))

(define-public crate-threshold-0.8.13 (c (n "threshold") (v "0.8.13") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0pf91hs02ira6lgfln6jpay5fpwh52wlxb518d6nmz4hdwmzc73m")))

(define-public crate-threshold-0.8.14 (c (n "threshold") (v "0.8.14") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "02vdvyz36gaisqng2rln9jxm0aqbmrxcgdmf4ya1nnfg378dwk8h")))

(define-public crate-threshold-0.8.15 (c (n "threshold") (v "0.8.15") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1jcwv43la71qgbx1384d3907pmr2zz6rng36r0ima0klkzvk0zwk")))

(define-public crate-threshold-0.8.16 (c (n "threshold") (v "0.8.16") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "0n8d03kgph7dk8p5ny0kvlz3rbsk40cmd3n4yv6m9dg730hrbnvz")))

(define-public crate-threshold-0.9.0 (c (n "threshold") (v "0.9.0") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "09wqhnj8gm9i0jdcs937ngrymc87l7kz3n102w74nc9whc6zq9b4")))

(define-public crate-threshold-0.9.1 (c (n "threshold") (v "0.9.1") (d (list (d (n "criterion") (r "^0.2") (d #t) (k 2)) (d (n "quickcheck") (r "^0.8") (d #t) (k 2)) (d (n "quickcheck_macros") (r "^0.8") (d #t) (k 2)) (d (n "rand") (r "^0.7") (d #t) (k 2)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)))) (h "1n8almdxcv5gbxm4d6kr46hd73fcjlik7hd7bigva5mgzm08v7mx")))

