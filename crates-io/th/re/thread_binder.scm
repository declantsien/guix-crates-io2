(define-module (crates-io th re thread_binder) #:use-module (crates-io))

(define-public crate-thread_binder-0.1.0 (c (n "thread_binder") (v "0.1.0") (d (list (d (n "hwloc") (r "^0.5.0") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rayon") (r "^1") (d #t) (k 0)))) (h "0mnz6hfrhq1vvycy1ks7927iw7ydn4sd4iizhikq38m19l2y53s2")))

