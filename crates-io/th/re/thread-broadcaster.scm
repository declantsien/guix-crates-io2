(define-module (crates-io th re thread-broadcaster) #:use-module (crates-io))

(define-public crate-thread-broadcaster-0.1.0 (c (n "thread-broadcaster") (v "0.1.0") (d (list (d (n "crossbeam-channel") (r "^0.5.8") (d #t) (k 0)) (d (n "tracing") (r "^0.1.37") (d #t) (k 0)))) (h "0phgainj91sg74fqcqm9yaikbb4s799ib7qdpaqyv0byj127hxd3")))

