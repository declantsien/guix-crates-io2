(define-module (crates-io th re threadbeam) #:use-module (crates-io))

(define-public crate-threadbeam-0.1.0 (c (n "threadbeam") (v "0.1.0") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0") (o #t) (d #t) (k 0)))) (h "084sgp5ynypknl6qmbkfjbplgxj76ma40wc4vg714gwcrivciikp") (f (quote (("no_std" "spin")))) (s 2) (e (quote (("parking_lot" "dep:parking_lot"))))))

(define-public crate-threadbeam-0.1.1 (c (n "threadbeam") (v "0.1.1") (d (list (d (n "bitflags") (r "^1") (d #t) (k 0)) (d (n "parking_lot") (r "^0.12") (o #t) (d #t) (k 0)) (d (n "spin") (r "^0.9") (o #t) (d #t) (k 0)))) (h "1811vf7dfvds76a55cb3xj8w1mbnb79z4bp7hppvk3qf2lchz81i") (f (quote (("no_std" "spin")))) (s 2) (e (quote (("parking_lot" "dep:parking_lot"))))))

