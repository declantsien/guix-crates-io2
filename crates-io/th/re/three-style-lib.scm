(define-module (crates-io th re three-style-lib) #:use-module (crates-io))

(define-public crate-three-style-lib-0.1.0 (c (n "three-style-lib") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1jw7w39jd7xv50xmnpwyrckk3564vjrqcaw1pmvvz0zgfjbd119r")))

(define-public crate-three-style-lib-0.1.1 (c (n "three-style-lib") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "0pmmc9pfy2a4hyffzn791jzgkqwjhzh8hqydd78p04z1rfi49sxh")))

(define-public crate-three-style-lib-0.1.2 (c (n "three-style-lib") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.57") (d #t) (k 0)))) (h "1sg2dj87r2dzkf1cifv3z0ajaqkqn12k6cnnni3lqzxgp4adp84v")))

