(define-module (crates-io th re threadpool) #:use-module (crates-io))

(define-public crate-threadpool-0.1.0 (c (n "threadpool") (v "0.1.0") (h "1isg9brhkxfcp07fkll7xypgrhahwrbybnly9xyb55vk1lybgvcf")))

(define-public crate-threadpool-0.1.1 (c (n "threadpool") (v "0.1.1") (h "135pnn4bka1hrsafm1g122pan0pirhx30m8sah7pwc0kp8ag569p")))

(define-public crate-threadpool-0.1.2 (c (n "threadpool") (v "0.1.2") (h "1q63y8b7a9a6zgsx0hw5yslvxjx1v828sypfa76s657fxmmwzswc")))

(define-public crate-threadpool-0.1.3 (c (n "threadpool") (v "0.1.3") (h "0p5iqs1lp06im07356vcdxzw0bryqyln6g4h056pdjcpmmivjw1q")))

(define-public crate-threadpool-0.1.4 (c (n "threadpool") (v "0.1.4") (h "0w0cbglb5lzqy5pxiwn88yyqd3pk9cyc54clb6gkmnvh00iiafm6") (f (quote (("scoped-pool"))))))

(define-public crate-threadpool-0.2.0 (c (n "threadpool") (v "0.2.0") (h "01fv3c8j2nd0n7ih3qw2kqzm5p7fwcyayqcq5khlijrbqfaqdd2a")))

(define-public crate-threadpool-0.2.1 (c (n "threadpool") (v "0.2.1") (h "06v6kccbhjfzszi3rsvy305472xrqmw2yjiar00cpd9c24gs8jdf")))

(define-public crate-threadpool-1.0.0 (c (n "threadpool") (v "1.0.0") (h "1d8l1f0n7zqi19x5dzi6q58nvmj5jwkq3jb9ckj1cknxsq4ll28n")))

(define-public crate-threadpool-1.0.1 (c (n "threadpool") (v "1.0.1") (h "0s3ii0d2d5cvwd3q9svqzdy232103z2xjzdlf8xgzmq2rfqidxnp")))

(define-public crate-threadpool-1.0.2 (c (n "threadpool") (v "1.0.2") (h "10nkhzbnpkv25n2h2pbr2kmzwv1pvq8s6igp93v3hm8l2h4b7cg8")))

(define-public crate-threadpool-1.1.0 (c (n "threadpool") (v "1.1.0") (h "0bp35375sh44ignlihyyplh4pbxs34fg9zbxrarj8acnw525hnic")))

(define-public crate-threadpool-1.1.1 (c (n "threadpool") (v "1.1.1") (h "11vrqkhy2lqbjaw016b0skd9q3z7c659v9wyb02c1nv8h42ly5bq")))

(define-public crate-threadpool-1.2.0 (c (n "threadpool") (v "1.2.0") (h "1s57ijbaqm5zjqyqf2aab13s0kzh9y4m07awj175jd97wf9zlzmy")))

(define-public crate-threadpool-1.3.0 (c (n "threadpool") (v "1.3.0") (h "016hvvqgcrvrx412pp0xxy4ar3g6grsnppfcpg02mb0vz9msl8ns")))

(define-public crate-threadpool-1.3.1 (c (n "threadpool") (v "1.3.1") (h "0x4wpna84ajxh99wdg9i7qhrp7wy8iizzv86msbni7ddc17bx1c3")))

(define-public crate-threadpool-1.3.2 (c (n "threadpool") (v "1.3.2") (h "1qbzblmw6rh4i8jppdd5i0gd0w8x8vg4vd4xmhyi284rz3px7xjr")))

(define-public crate-threadpool-1.4.0 (c (n "threadpool") (v "1.4.0") (h "0fhc8jwb7f9rm0d0iibgxv933pp6msbbccq55pscil5a05hnd59g")))

(define-public crate-threadpool-1.4.1 (c (n "threadpool") (v "1.4.1") (h "144xayaynqrg6amfa3fw97fs0mp07d6fxs4wayb16jirhf3ymz9r")))

(define-public crate-threadpool-1.5.0 (c (n "threadpool") (v "1.5.0") (d (list (d (n "num_cpus") (r "^1.6") (d #t) (k 0)))) (h "154jilxpxrrvfz4nvibcn5qin3zb7y0i4khbkz760v2bikprncnd")))

(define-public crate-threadpool-1.6.0 (c (n "threadpool") (v "1.6.0") (d (list (d (n "num_cpus") (r "^1.6") (d #t) (k 0)))) (h "177a66ijmhn3481vvwx1j4zll33ad1srjsjfd7fa1ja3l0hvfv06")))

(define-public crate-threadpool-1.7.0 (c (n "threadpool") (v "1.7.0") (d (list (d (n "num_cpus") (r "^1.6") (d #t) (k 0)))) (h "1k1nznha259yddahgzxca86803x71f6r3jjhvjz9bb67s6xhhxn3")))

(define-public crate-threadpool-1.7.1 (c (n "threadpool") (v "1.7.1") (d (list (d (n "num_cpus") (r "^1.6") (d #t) (k 0)))) (h "0rd89n1q7vy47w4c32cnynibffv9kj3jy3dwr0536n9lbw5ckw72")))

(define-public crate-threadpool-1.8.0 (c (n "threadpool") (v "1.8.0") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "0rkx0wzaw9v958ckiliwl42m2j7c59j3r5vdj6kda5bw8j2f3np8")))

(define-public crate-threadpool-1.8.1 (c (n "threadpool") (v "1.8.1") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)))) (h "1amgfyzvynbm8pacniivzq9r0fh3chhs7kijic81j76l6c5ycl6h")))

