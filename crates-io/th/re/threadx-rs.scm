(define-module (crates-io th re threadx-rs) #:use-module (crates-io))

(define-public crate-threadx-rs-0.1.0 (c (n "threadx-rs") (v "0.1.0") (d (list (d (n "threadx-sys") (r "^0.1.1") (d #t) (k 0)))) (h "1mh6wrh1wcam7avbhbg5j3hylkykcmpj1708cipxinxb1ylrknld")))

(define-public crate-threadx-rs-0.1.1 (c (n "threadx-rs") (v "0.1.1") (d (list (d (n "defmt") (r "^0.3") (d #t) (k 0)) (d (n "num-derive") (r "^0.4.1") (d #t) (k 0)) (d (n "num-traits") (r "^0.2.17") (k 0)) (d (n "thiserror-no-std") (r "^2.0.2") (d #t) (k 0)) (d (n "threadx-sys") (r "^0.2") (d #t) (k 0)))) (h "07gb8vwbf89pm6kdh1f5y83vims47wn9xc9c6mrgdyzni03a48x6")))

