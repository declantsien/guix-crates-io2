(define-module (crates-io th re threatpool) #:use-module (crates-io))

(define-public crate-threatpool-0.1.0 (c (n "threatpool") (v "0.1.0") (h "16kyk9k6sp054x0vf579vxcb0yjsrrmycw9i5frq9zg2k2rrlaqk")))

(define-public crate-threatpool-0.2.0 (c (n "threatpool") (v "0.2.0") (h "118r8j3n82v3asjycv1z2zc4vcwsafwfwfadzwk04p90gp5zb788") (r "1.75.0")))

