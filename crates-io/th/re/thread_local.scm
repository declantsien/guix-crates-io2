(define-module (crates-io th re thread_local) #:use-module (crates-io))

(define-public crate-thread_local-0.1.0 (c (n "thread_local") (v "0.1.0") (d (list (d (n "thread-id") (r "^1") (d #t) (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "1i8ay1xlm5p7dgyzvj14h32f06zdfibk1s67459yc56zkljw2mxg")))

(define-public crate-thread_local-0.2.0 (c (n "thread_local") (v "0.2.0") (d (list (d (n "thread-id") (r "^1") (d #t) (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "1kfwqvqs3k0va5sim8k5brggrxkfjb8jpfg41yx5lcpf7yphpz5b")))

(define-public crate-thread_local-0.2.1 (c (n "thread_local") (v "0.2.1") (d (list (d (n "thread-id") (r "^2") (d #t) (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "1d0mxqfb5sykdh3q8wn9lsq8cjrrvjlgnhx6l0d8ir5gi9anf5ns")))

(define-public crate-thread_local-0.2.2 (c (n "thread_local") (v "0.2.2") (d (list (d (n "thread-id") (r "^2.0") (d #t) (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "12ijv2n4m1c5yn2wkzpvh9rh0r94fm22569dizlcb1vrwlzlk9wl")))

(define-public crate-thread_local-0.2.3 (c (n "thread_local") (v "0.2.3") (d (list (d (n "thread-id") (r "^2.0") (d #t) (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "1ij6lf8hgq20j8w5y8d1xwvvfgbrvyb7xh6wd2xnrqgjabp4vqnq")))

(define-public crate-thread_local-0.2.4 (c (n "thread_local") (v "0.2.4") (d (list (d (n "thread-id") (r "^2.0") (d #t) (k 0)))) (h "0qkp08wrww5i4ab1z12iwf74a0rps5lawynh6grd0bhpnx3zlxpy")))

(define-public crate-thread_local-0.2.5 (c (n "thread_local") (v "0.2.5") (d (list (d (n "thread-id") (r "^2.0") (d #t) (k 0)))) (h "1cfa70rr6w8fqiics6slfrp3br9fc3qyf7dql73zlz7g20bgb506")))

(define-public crate-thread_local-0.2.6 (c (n "thread_local") (v "0.2.6") (d (list (d (n "thread-id") (r "^2.0") (d #t) (k 0)))) (h "0kblnkrjx7bn1f14pf9bwc2phjhmqf8pzgv6fam0ip7ap8yrdpam")))

(define-public crate-thread_local-0.2.7 (c (n "thread_local") (v "0.2.7") (d (list (d (n "thread-id") (r "^2.0") (d #t) (k 0)))) (h "1mgxikqvhpsic6xk7pan95lvgsky1sdxzw2w5m2l35pgrazxnxl5")))

(define-public crate-thread_local-0.3.0 (c (n "thread_local") (v "0.3.0") (d (list (d (n "thread-id") (r "^3.0") (d #t) (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "0z62a0xsrw252i5prz56by3jgdqch19v53idsnp3k6k25jjpq1ah")))

(define-public crate-thread_local-0.3.1 (c (n "thread_local") (v "0.3.1") (d (list (d (n "thread-id") (r "^3.0") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "180c1paqqv6sn3g20m81c3hikfz76775snv1rmv82cfps7h538mf")))

(define-public crate-thread_local-0.3.2 (c (n "thread_local") (v "0.3.2") (d (list (d (n "thread-id") (r "^3.0") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "0pg07fla8i4z4v2bmzq49pr2z8ww6lbg9b7iwwbffz7py0ibg4vp")))

(define-public crate-thread_local-0.3.3 (c (n "thread_local") (v "0.3.3") (d (list (d (n "thread-id") (r "^3.0") (d #t) (t "cfg(not(target_os = \"emscripten\"))") (k 0)) (d (n "unreachable") (r "^0.1") (d #t) (k 0)))) (h "1izh57y5yj7m0322hnxz43i0pfdnkwnjiqzadi4cy5qd4v34hl68")))

(define-public crate-thread_local-0.3.4 (c (n "thread_local") (v "0.3.4") (d (list (d (n "lazy_static") (r "^0.2") (d #t) (k 0)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "055vj0ddb6vh0zaqdlxssfqzzpgs4ll5l5j7nqvabdzfgasw95qn")))

(define-public crate-thread_local-0.3.5 (c (n "thread_local") (v "0.3.5") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)) (d (n "unreachable") (r "^1.0") (d #t) (k 0)))) (h "0qw9sgni1x8gcz6j8smh6pv222h453kzlbfipxvzbvgd34fg77i7")))

(define-public crate-thread_local-0.3.6 (c (n "thread_local") (v "0.3.6") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "06rzik99p8c5js8238yhc8rk6np543ylb1dy9nrw5v80j0r3xdf6")))

(define-public crate-thread_local-1.0.0 (c (n "thread_local") (v "1.0.0") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "1ayfi1f9hc4n4md7l25rararjgy9pirdjxzqzwg3szhcb2nz3pc8")))

(define-public crate-thread_local-1.0.1 (c (n "thread_local") (v "1.0.1") (d (list (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "054vlrr1vsdy1h4b7n99mr24pnj8928ig9qwzg36wnkld4dns36l")))

(define-public crate-thread_local-1.1.0 (c (n "thread_local") (v "1.1.0") (d (list (d (n "criterion") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "lazy_static") (r "^1.0") (d #t) (k 0)))) (h "0iyl5jby55q6mv6j69csjpdw57pw9x9qb7cxn6r7c7nms29c16xv")))

(define-public crate-thread_local-1.1.1 (c (n "thread_local") (v "1.1.1") (d (list (d (n "criterion") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "16cdyc7rk9ys027c245ll0xls9rdi4q12idy4qlnfj9ws89xs6rh")))

(define-public crate-thread_local-1.1.2 (c (n "thread_local") (v "1.1.2") (d (list (d (n "criterion") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "05b94g3giadny554wpwzlqwclj7wp3952sfpbgfiicqw3qrql86q")))

(define-public crate-thread_local-1.1.3 (c (n "thread_local") (v "1.1.3") (d (list (d (n "criterion") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1gccp3grndpi6dyhzylz4hkqnkzc1xyri98n0xwwhnn90i7d4640")))

(define-public crate-thread_local-1.1.4 (c (n "thread_local") (v "1.1.4") (d (list (d (n "criterion") (r "^0.3.3") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "1001bvz6a688wf3izcrh3jqrkiqaarf44wf08azm071ig1xw45jm")))

(define-public crate-thread_local-1.1.5 (c (n "thread_local") (v "1.1.5") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (o #t) (d #t) (k 0)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0j50zi5zlgljgiijvzzg702v3xmj7qx22bhlixrfiz31grmzj1fq") (f (quote (("nightly")))) (y #t)))

(define-public crate-thread_local-1.1.6 (c (n "thread_local") (v "1.1.6") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "01qard9k9srkrdzjp6bildgwgadv65b3s56zh3kfzm7r1w99gwjh") (f (quote (("nightly")))) (y #t)))

(define-public crate-thread_local-1.1.7 (c (n "thread_local") (v "1.1.7") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.4.0") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "0lp19jdgvp5m4l60cgxdnl00yw1hlqy8gcywg9bddwng9h36zp9z") (f (quote (("nightly"))))))

(define-public crate-thread_local-1.1.8 (c (n "thread_local") (v "1.1.8") (d (list (d (n "cfg-if") (r "^1.0.0") (d #t) (k 0)) (d (n "criterion") (r "^0.5.1") (d #t) (k 2)) (d (n "once_cell") (r "^1.5.2") (d #t) (k 0)))) (h "173i5lyjh011gsimk21np9jn8al18rxsrkjli20a7b8ks2xgk7lb") (f (quote (("nightly")))) (r "1.59")))

