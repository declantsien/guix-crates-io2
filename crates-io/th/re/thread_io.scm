(define-module (crates-io th re thread_io) #:use-module (crates-io))

(define-public crate-thread_io-0.2.0 (c (n "thread_io") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.6") (d #t) (k 0)))) (h "0v6rzr04hxyfpicycxns0gy11rkv6s04vzvq3v54dgfrqxfg3rck") (f (quote (("crossbeam_channel"))))))

(define-public crate-thread_io-0.3.0 (c (n "thread_io") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.7") (d #t) (k 0)))) (h "14d1fmsf4fy6b7vh4z943hpj1wxi6jmm7q6lw99aqy74q5s6whm4") (f (quote (("crossbeam_channel"))))))

(define-public crate-thread_io-0.3.1 (c (n "thread_io") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "crossbeam-utils") (r "^0.8") (d #t) (k 0)))) (h "1hr9wmj3zligir32qabdn7xld6xdhyv5snf4i7dn3gqwlqilf62i") (f (quote (("crossbeam_channel")))) (r "1.38")))

