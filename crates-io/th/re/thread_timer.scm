(define-module (crates-io th re thread_timer) #:use-module (crates-io))

(define-public crate-thread_timer-0.1.0 (c (n "thread_timer") (v "0.1.0") (h "1yasn7327l7986676sgijx2x39ha4qbj3j8766dz500ldyn5lw9h")))

(define-public crate-thread_timer-0.2.0 (c (n "thread_timer") (v "0.2.0") (h "1z0iqljn6ahpsm46gdxg0h4nmg7y2mc9cgyrj97d2lrnf021jz4w")))

(define-public crate-thread_timer-0.3.0 (c (n "thread_timer") (v "0.3.0") (h "0qkm28y6jkzcclsvy8w0xqyb5qzwpy39n2l08gd6dgikrdnv2sqf")))

