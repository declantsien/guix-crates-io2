(define-module (crates-io th re thread_clock) #:use-module (crates-io))

(define-public crate-thread_clock-0.1.0 (c (n "thread_clock") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("sync" "rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 0)))) (h "02glqmi819c1nwha8x14ynzmbnc3p5z4yzk4k5yp7vhfi8lr4wc1") (y #t)))

(define-public crate-thread_clock-0.2.0 (c (n "thread_clock") (v "0.2.0") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("sync" "rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 0)))) (h "1d07p7xxa0dsr03ldhwz5xavxs2fpzmjxzk8iygm9zy6s30712qn") (y #t)))

(define-public crate-thread_clock-0.2.1 (c (n "thread_clock") (v "0.2.1") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 0)) (d (n "tokio") (r "^1.21.1") (f (quote ("sync" "rt" "rt-multi-thread" "macros" "time"))) (d #t) (k 0)))) (h "1i1jfrk3qj0llwqcl3q5rd4dp386wa9i8bnfan6wm82n5ksr9igr") (y #t)))

