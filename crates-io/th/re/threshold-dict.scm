(define-module (crates-io th re threshold-dict) #:use-module (crates-io))

(define-public crate-threshold-dict-0.1.2 (c (n "threshold-dict") (v "0.1.2") (h "0pjq7as69xrvkc2iwb64hb8zpx3xb49mkdmdvsbdmywp9djvbgvv") (y #t)))

(define-public crate-threshold-dict-0.1.3 (c (n "threshold-dict") (v "0.1.3") (h "10rrxys2dc77ibd0sf5vbsqg1k76nkj6asq7190nlf9wn6j0gx2r")))

(define-public crate-threshold-dict-0.1.4 (c (n "threshold-dict") (v "0.1.4") (h "19r4kqbpq8bvwff5l56mh6prnlmmfrl05apzkvfgldiqs9vrpzyx")))

(define-public crate-threshold-dict-0.2.0 (c (n "threshold-dict") (v "0.2.0") (h "0iqrikdzl807qxn47vkg352vl99nsqsxx0c9qwja1bmfsczf6j4h")))

(define-public crate-threshold-dict-0.3.0 (c (n "threshold-dict") (v "0.3.0") (h "094r3a9ad3wz4va0swmxdwkpx5hz41pkpbsx16nlqqw6y6a4jq3d")))

(define-public crate-threshold-dict-0.4.0 (c (n "threshold-dict") (v "0.4.0") (h "15b39f8g0mayic6q2nnfhf0xvzibpl45w5qh39wajxrfl0mx5vi4") (y #t)))

(define-public crate-threshold-dict-0.4.1 (c (n "threshold-dict") (v "0.4.1") (h "1s0p8bglbpxx919881vinhkllm0j2s9pcbqd73mhvq29ryzsiv5l")))

(define-public crate-threshold-dict-0.5.0 (c (n "threshold-dict") (v "0.5.0") (h "1gdf33jd7rvns9aydphg1hqmbrv7h0nmzw0vck2h9da87n97iirv")))

(define-public crate-threshold-dict-0.6.0 (c (n "threshold-dict") (v "0.6.0") (h "1gwambm9pq250v42qi9ixbpi3f052709613x8wi817w06dzav39v")))

(define-public crate-threshold-dict-0.7.0 (c (n "threshold-dict") (v "0.7.0") (h "1kgzy164s0cdfcdc01g9rj9gf1dj3b6n08y31xjbra0vaivsj9hf")))

