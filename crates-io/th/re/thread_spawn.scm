(define-module (crates-io th re thread_spawn) #:use-module (crates-io))

(define-public crate-thread_spawn-0.1.0 (c (n "thread_spawn") (v "0.1.0") (d (list (d (n "compiletest_rs") (r "^0.3.5") (d #t) (k 2)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "0knp6y5z7zgr3v0dgflxy2c3gmqhdxf8nzv3brb3790v276zkr63")))

(define-public crate-thread_spawn-0.2.0 (c (n "thread_spawn") (v "0.2.0") (d (list (d (n "compiletest_rs") (r "^0.3.5") (d #t) (k 2)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06pk2lndhdw4ycf8nmki8iawl4jc9ck4baxx89g2mf7qzk69vyld")))

(define-public crate-thread_spawn-0.3.0 (c (n "thread_spawn") (v "0.3.0") (d (list (d (n "compiletest_rs") (r "^0.3.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.3") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.5") (d #t) (k 0)) (d (n "syn") (r "^0.13") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "06szbirrx31pkya769qsd65ar86xd2s8q18wiwzc9lb2y5lh62gy")))

(define-public crate-thread_spawn-0.4.0 (c (n "thread_spawn") (v "0.4.0") (d (list (d (n "compiletest_rs") (r "^0.3.5") (d #t) (k 2)) (d (n "proc-macro2") (r "^0.4") (f (quote ("nightly"))) (d #t) (k 0)) (d (n "quote") (r "^0.6") (d #t) (k 0)) (d (n "syn") (r "^0.14") (f (quote ("full" "extra-traits"))) (d #t) (k 0)))) (h "1g8jhcmqx4kkw3wzl1mry23lj0b4si9s4asilv5nw44qwvi2h0ly")))

