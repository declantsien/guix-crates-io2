(define-module (crates-io th re thread-owned-lock) #:use-module (crates-io))

(define-public crate-thread-owned-lock-0.1.0 (c (n "thread-owned-lock") (v "0.1.0") (d (list (d (n "criterion") (r "^0.5") (d #t) (k 2)) (d (n "parking_lot") (r "^0.12") (d #t) (k 2)))) (h "1cyxymfg7f2gqvprpsdzyj49578m9hf1g6s67x6f974fplkpc1nm") (f (quote (("no-std"))))))

