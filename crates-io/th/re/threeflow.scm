(define-module (crates-io th re threeflow) #:use-module (crates-io))

(define-public crate-threeflow-0.1.0 (c (n "threeflow") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "0bgp8fqmpldpqqj5nsiir07l6cr1x0479dav59bvdpz9bck1c6ys")))

(define-public crate-threeflow-0.2.0 (c (n "threeflow") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "16rgwachrak815bkrlr7ji19axvgxj49sjhp47wsa533v8rbb894")))

(define-public crate-threeflow-0.3.0 (c (n "threeflow") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.18.1") (d #t) (k 0)))) (h "0kxl1qhrvsa49w0l4hmivhrv49m53q9qc9h2rn2mv5c8rkqwi5xw")))

(define-public crate-threeflow-0.3.1 (c (n "threeflow") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "clap") (r "^3.0.0") (d #t) (k 0)))) (h "1c41i3wwhl4rxkalrd9wgwgdnisfqy57zcy48h73k7nq9gcb3j9f")))

