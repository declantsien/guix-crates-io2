(define-module (crates-io th re thread-manager) #:use-module (crates-io))

(define-public crate-thread-manager-0.1.0 (c (n "thread-manager") (v "0.1.0") (h "1z47nw77krn9p8q5qgn2hjb7as9ws2dwifhngpl74gkw0m6wqvq5")))

(define-public crate-thread-manager-0.1.1 (c (n "thread-manager") (v "0.1.1") (h "097iz2xprf2wwgakq7k7zmvjl29hp0d59vai23kizrb407rfk85g")))

(define-public crate-thread-manager-0.2.0 (c (n "thread-manager") (v "0.2.0") (h "1j5s4396c94j08j9plnb37zrrfz756c5vv5d46rp5xb1nm7856x8")))

(define-public crate-thread-manager-0.3.0 (c (n "thread-manager") (v "0.3.0") (h "01qz3dmfxmqlp8qhxvk2cxxpy5gd7ys4c192glpa1zgavgbkhxva")))

(define-public crate-thread-manager-0.4.0 (c (n "thread-manager") (v "0.4.0") (h "1l7ii5k94ibwq4cyzb2j7fxrw79l71dffw0ja2qbk823r77fg674")))

(define-public crate-thread-manager-0.4.1 (c (n "thread-manager") (v "0.4.1") (h "036c45q5rkp93c555whvlph44kdfg7lmqj1v0fjah46wwa0vihq8")))

(define-public crate-thread-manager-0.5.0 (c (n "thread-manager") (v "0.5.0") (h "0rs9f2zvnk0p8l5vhqrbj1c4gyy3ckxq6mi1m26dpxzbhzi67h6s")))

(define-public crate-thread-manager-0.6.0 (c (n "thread-manager") (v "0.6.0") (h "18p50j8crhsppxv6f01lbn1jw67aybhhvfag77rrvcrvyixhng46")))

(define-public crate-thread-manager-0.7.0 (c (n "thread-manager") (v "0.7.0") (h "0mg0y40gs7g3ahfqfzz5z2snyqnz1w6i2ps34p5k14797bwnwmil")))

(define-public crate-thread-manager-0.8.0 (c (n "thread-manager") (v "0.8.0") (h "0kb0vm7s096h8z1pbd2mc6vmwx4sd94pxlnr3nsy63f60pdviyhy")))

(define-public crate-thread-manager-0.8.1 (c (n "thread-manager") (v "0.8.1") (h "16a0d4ygzzvqhcv4ha14xqm1apwx5y55c1a9ijb2rvljf3hdpsfj")))

(define-public crate-thread-manager-0.9.0 (c (n "thread-manager") (v "0.9.0") (h "1a1x4x9vafc0f8nhb0yj87k33rx1iyhm0zs4j8jj4r67fvfq7vms")))

(define-public crate-thread-manager-0.9.1 (c (n "thread-manager") (v "0.9.1") (h "1bsw5b623s5y9kyx3xfvvz4xy13hyb84nk07b5c0h0balxs2pnvf")))

(define-public crate-thread-manager-0.9.2 (c (n "thread-manager") (v "0.9.2") (h "1mi70883rw2n7cl45q0flvglklcd070b888cmzwbamcv0gddww9h")))

(define-public crate-thread-manager-0.9.3 (c (n "thread-manager") (v "0.9.3") (h "0hhvdi0jpw6kfsbwqqhdya2i40fgzrzxyzycsaq5c8jacinl91sw")))

(define-public crate-thread-manager-0.9.4 (c (n "thread-manager") (v "0.9.4") (h "0rprmw783wp19qa2wq431zyyj5l53wfqlgja481nlxiy0dp5wvvy")))

(define-public crate-thread-manager-0.9.5 (c (n "thread-manager") (v "0.9.5") (h "1k3blbw9ac2lmb553qfcpparfgx5zagd0cpl9adqgjhv6x6l7zsn")))

(define-public crate-thread-manager-0.9.6 (c (n "thread-manager") (v "0.9.6") (h "0bsh08k1z2gd7bqwdwxbig2kraks8iifgwkn5f697bzks2837knc")))

(define-public crate-thread-manager-0.9.7 (c (n "thread-manager") (v "0.9.7") (h "0hc5zw5fryq4paf0b5dhsj9pzfrsak9szia5grm8c5vn90gihjqy")))

(define-public crate-thread-manager-0.9.8 (c (n "thread-manager") (v "0.9.8") (h "1qqlnxrrni9vy2m647vri29ry9bclz44dmlwk4jzcjwpqyicjxzl")))

(define-public crate-thread-manager-0.9.9 (c (n "thread-manager") (v "0.9.9") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)))) (h "08xsjrmzk5603k68p5hb3vhqzbv08ciapazikccpqkn7mixlnji0")))

(define-public crate-thread-manager-0.9.10 (c (n "thread-manager") (v "0.9.10") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)))) (h "0yd7samhnf5vj290c0y466b0nhnnmin06qqzfx7zsm6wql15qg8j")))

(define-public crate-thread-manager-0.9.11 (c (n "thread-manager") (v "0.9.11") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)))) (h "0lwb15yp7afrsp88ib7ijgvds8m4hvii2i62c4j8darypnm326yy")))

(define-public crate-thread-manager-0.9.12 (c (n "thread-manager") (v "0.9.12") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)))) (h "0rvqprwzf3yw9m4c1wbxhb1bhkl6k8mca1zk1hjd82aky8yx3zyx")))

(define-public crate-thread-manager-0.9.13 (c (n "thread-manager") (v "0.9.13") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)))) (h "0s2y3rfm24bpqpk6vm9734yd2f4yfi016dyqwr1da5ibvazh6a4z")))

(define-public crate-thread-manager-0.10.0 (c (n "thread-manager") (v "0.10.0") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)))) (h "0dyrpl2m05jqgyv73gbmh8sl5y95hm0lg44pligcnpxj8gqw9rqy")))

(define-public crate-thread-manager-1.0.0 (c (n "thread-manager") (v "1.0.0") (d (list (d (n "crossbeam-channel") (r "^0.5.11") (d #t) (k 0)))) (h "1y68zssxh55qmpad6fkn5xjm4bnc8jbdfrjzbjvxfdkp6jw2qsgd")))

