(define-module (crates-io th re three_em_evm) #:use-module (crates-io))

(define-public crate-three_em_evm-0.5.0 (c (n "three_em_evm") (v "0.5.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "primitive-types") (r "^0.10.1") (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "1mi9p7hl03dga2a8615b6aa9qc6x1fkliylgikbsz6j4y8i7p5dh") (y #t)))

(define-public crate-three_em_evm-0.2.0 (c (n "three_em_evm") (v "0.2.0") (d (list (d (n "hex") (r "^0.4.3") (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.4") (d #t) (k 2)) (d (n "primitive-types") (r "^0.10.1") (k 0)) (d (n "tiny-keccak") (r "^2.0") (f (quote ("keccak"))) (d #t) (k 0)))) (h "0mx1w8gzz8lrkp046mydf7j7bsx6qqyr6ybabmc2n4vj6gwiqqbg")))

