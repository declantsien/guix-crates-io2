(define-module (crates-io th re threaded_logger) #:use-module (crates-io))

(define-public crate-threaded_logger-0.1.0 (c (n "threaded_logger") (v "0.1.0") (d (list (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("rt"))) (d #t) (k 0)))) (h "1m0nl9h1ry3374mn4bq9fx6jpsfnabdmgjdvhffqxazxxpkrdypv")))

(define-public crate-threaded_logger-0.2.0 (c (n "threaded_logger") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "env_logger") (r "^0.8.3") (d #t) (k 2)) (d (n "log") (r "^0.4.14") (f (quote ("std"))) (d #t) (k 0)) (d (n "once_cell") (r "^1.7.2") (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("rt"))) (d #t) (k 0)) (d (n "tokio") (r "^1.6.0") (f (quote ("rt" "macros"))) (d #t) (k 2)))) (h "0fpsmf49j8rsz8n6nivnksiy2442i0wsw25iaw1nac7l2xsdsq04")))

