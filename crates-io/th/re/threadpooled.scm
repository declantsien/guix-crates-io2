(define-module (crates-io th re threadpooled) #:use-module (crates-io))

(define-public crate-threadpooled-0.1.0 (c (n "threadpooled") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "0l023dlh237797am7v3npi1qlld4szbbsw17hlvygbjqdjwsrhz1")))

(define-public crate-threadpooled-0.1.1 (c (n "threadpooled") (v "0.1.1") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "020a13qjahr5mv6r3zjvzgk0l8vnxvsn7cb26mcmxkah8y3hph7v")))

(define-public crate-threadpooled-0.1.2 (c (n "threadpooled") (v "0.1.2") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1bmas1178gfkdzqmvlx65d8x3lxn7w0mq22hcdshkc89r4a88aki")))

(define-public crate-threadpooled-0.1.3 (c (n "threadpooled") (v "0.1.3") (d (list (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1rwas6gzwz1wx7d7bl54wb1215gnblpkbl61yaaas2rnka37yvjh")))

