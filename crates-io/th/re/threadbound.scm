(define-module (crates-io th re threadbound) #:use-module (crates-io))

(define-public crate-threadbound-0.1.0 (c (n "threadbound") (v "0.1.0") (h "18r7lhylmpnx9nv7wsiww7p2pq7nc3rlzwnf70nvvx8ygbp8fgnj")))

(define-public crate-threadbound-0.1.1 (c (n "threadbound") (v "0.1.1") (h "19v2l38q9xbxms1ms2f8hk8v7sn1i6da0g4c1d2h0q7h2shzy8n9")))

(define-public crate-threadbound-0.1.2 (c (n "threadbound") (v "0.1.2") (h "10xv6nfawharx1787nw4szrnksrr0rqz10lqf8jv8p3pjzc2nnrb")))

(define-public crate-threadbound-0.1.3 (c (n "threadbound") (v "0.1.3") (h "1qvd38ywvzgfsbiq2222cmbmy8c31cv1nkpp45zs47a8nphlarpg") (r "1.31")))

(define-public crate-threadbound-0.1.4 (c (n "threadbound") (v "0.1.4") (h "1pxwxcjiq4cwwkclimppp86x04fgchbj8sq4h7nn2xxlgjmk792l") (r "1.31")))

(define-public crate-threadbound-0.1.5 (c (n "threadbound") (v "0.1.5") (h "1c6xniv2gz7xqsclrflfrsz8il35ys9fgq4jkmjcsjbckq1qyqq6") (r "1.31")))

(define-public crate-threadbound-0.1.6 (c (n "threadbound") (v "0.1.6") (h "06h6zc6abqh0ri90yqysmvmqd5pxfwymmgqs4bhqldws26mxhq8v") (r "1.31")))

(define-public crate-threadbound-0.1.7 (c (n "threadbound") (v "0.1.7") (h "0rqwyjf2c82ya48kbmj7954aniwz9rkb834zhfkwrm89xg7kmz90") (r "1.31")))

