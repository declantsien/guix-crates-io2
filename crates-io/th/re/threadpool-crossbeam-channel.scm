(define-module (crates-io th re threadpool-crossbeam-channel) #:use-module (crates-io))

(define-public crate-threadpool-crossbeam-channel-1.8.0 (c (n "threadpool-crossbeam-channel") (v "1.8.0") (d (list (d (n "crossbeam-channel") (r "^0.4") (d #t) (k 0)) (d (n "num_cpus") (r "^1.6") (d #t) (k 0)))) (h "1cganvwi1q0l8aiwx9zvbqnh1sz7g90kvj2kh6xwky53gziqll55")))

