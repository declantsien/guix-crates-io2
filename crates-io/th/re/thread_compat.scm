(define-module (crates-io th re thread_compat) #:use-module (crates-io))

(define-public crate-thread_compat-1.0.0 (c (n "thread_compat") (v "1.0.0") (h "0fg3k4dzb85zqww4nps64hcmnfzlxzksmnbay6p9hyihm2i7zm0f")))

(define-public crate-thread_compat-1.0.1 (c (n "thread_compat") (v "1.0.1") (h "15a5b0yy8h2ryllwihx5n6xqkcsrq4ca3m9qcl8bzcbq4llpzs75")))

