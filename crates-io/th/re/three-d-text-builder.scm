(define-module (crates-io th re three-d-text-builder) #:use-module (crates-io))

(define-public crate-three-d-text-builder-0.6.0 (c (n "three-d-text-builder") (v "0.6.0") (d (list (d (n "fontdue") (r "^0.8.0") (d #t) (k 0)) (d (n "rectangle-pack") (r "^0.4") (d #t) (k 0)) (d (n "three-d") (r "^0.16") (d #t) (k 0)) (d (n "three-d-asset") (r "^0.6") (d #t) (k 0)))) (h "0azh3wvglv0iw3w8gsxz8mlvbqq0vk6lgfc9ydld27dxp8rv2nij")))

(define-public crate-three-d-text-builder-0.7.0 (c (n "three-d-text-builder") (v "0.7.0") (d (list (d (n "fontdue") (r "^0.8.0") (d #t) (k 0)) (d (n "rectangle-pack") (r "^0.4") (d #t) (k 0)) (d (n "three-d") (r "^0.17") (d #t) (k 0)) (d (n "three-d-asset") (r "^0.7") (d #t) (k 0)))) (h "0yvn93imljlg301yrr18hw2xq0g0bv991d959s6wq6ddwa9qhlzw")))

