(define-module (crates-io th re threaded-map) #:use-module (crates-io))

(define-public crate-threaded-map-0.1.0 (c (n "threaded-map") (v "0.1.0") (d (list (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "044z62nck4gvfsd3fhh4jjgnarxdaml0kxcbglmk01gdh6ig9yj0") (y #t)))

(define-public crate-threaded-map-0.1.1 (c (n "threaded-map") (v "0.1.1") (d (list (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0k4izhdgb7c8r8dayaywzsixy04alwfalf5ksvi8jpmmzq7pz81n") (y #t)))

(define-public crate-threaded-map-0.1.2 (c (n "threaded-map") (v "0.1.2") (d (list (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "180hc4l2xa46473c0m3xih1ag4a8lwdr5ac8x3kl3x9qz0687kpj") (y #t)))

(define-public crate-threaded-map-0.1.3 (c (n "threaded-map") (v "0.1.3") (d (list (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "03w61ynd3vl1x1sq9fq6i2xfar19p93ln6qrcvy21fbr7icry1zj")))

(define-public crate-threaded-map-0.1.4 (c (n "threaded-map") (v "0.1.4") (d (list (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "030jyghfj1z2xplr68sza803gljksgsp13p03bk3v1f0jbmvc5b5")))

(define-public crate-threaded-map-0.2.0 (c (n "threaded-map") (v "0.2.0") (d (list (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0y63nd8lmy6d3pgwgjd3hkbcxv6bc3iviwlbfgkc7w1hk6mlxjjj")))

(define-public crate-threaded-map-0.1.5 (c (n "threaded-map") (v "0.1.5") (d (list (d (n "threadpool") (r "^1.8.1") (d #t) (k 0)))) (h "0j4h6fpsa7xqas5az7829f9zsg78b5mrvz4iqv0rg4jcv9i12fkq")))

