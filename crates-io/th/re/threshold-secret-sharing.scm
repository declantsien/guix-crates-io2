(define-module (crates-io th re threshold-secret-sharing) #:use-module (crates-io))

(define-public crate-threshold-secret-sharing-0.1.0 (c (n "threshold-secret-sharing") (v "0.1.0") (d (list (d (n "primal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0zj1fmfmjlmawv0ngzrxp5awwqlpixzzin0nnpdnbrq9q10wm9av") (f (quote (("paramgen" "primal"))))))

(define-public crate-threshold-secret-sharing-0.1.1 (c (n "threshold-secret-sharing") (v "0.1.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "primal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0h93jkagrivxbgf56r38a3g66cgjjwhaag55jq9h7m7aii0lqian") (f (quote (("paramgen" "primal"))))))

(define-public crate-threshold-secret-sharing-0.2.0 (c (n "threshold-secret-sharing") (v "0.2.0") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "primal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "00spfi0i4k4xgqzmqf0ji40z71lrp8c9vcvd6n8iy1wpc5sd7ixh") (f (quote (("paramgen" "primal"))))))

(define-public crate-threshold-secret-sharing-0.2.1 (c (n "threshold-secret-sharing") (v "0.2.1") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "primal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "02knvz19jky8npga2gw4pph2fngv1fvw1snrb9chifrbxim628jr") (f (quote (("paramgen" "primal"))))))

(define-public crate-threshold-secret-sharing-0.2.2 (c (n "threshold-secret-sharing") (v "0.2.2") (d (list (d (n "bencher") (r "^0.1") (d #t) (k 2)) (d (n "primal") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)))) (h "0dpsqcs7vk64ygc8a4jpcyv5i7ilgbw91pa73qykn7b6vqcmrwif") (f (quote (("paramgen" "primal"))))))

