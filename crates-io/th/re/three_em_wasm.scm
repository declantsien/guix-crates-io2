(define-module (crates-io th re three_em_wasm) #:use-module (crates-io))

(define-public crate-three_em_wasm-0.2.0 (c (n "three_em_wasm") (v "0.2.0") (d (list (d (n "deno_core") (r "^0.116.0") (d #t) (k 0)) (d (n "serde") (r "^1") (f (quote ("derive"))) (d #t) (k 0)) (d (n "three_em_js") (r "^0.2.0") (d #t) (k 0)) (d (n "three_em_smartweave") (r "^0.2.0") (d #t) (k 0)) (d (n "tokio") (r "^1.10.1") (f (quote ("full"))) (d #t) (k 2)) (d (n "v8") (r "^0.38.1") (d #t) (k 0)))) (h "1d60zhrl7b0i52q09xz1rlrpw9pikldwb1pxl53mma542y30ym34")))

