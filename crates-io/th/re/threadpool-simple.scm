(define-module (crates-io th re threadpool-simple) #:use-module (crates-io))

(define-public crate-threadpool-simple-0.1.0 (c (n "threadpool-simple") (v "0.1.0") (h "0knrpm38b7ms59lsl4fh9zsjavql6ni72ylfdff935xk54yhzq0n") (y #t)))

(define-public crate-threadpool-simple-0.1.1 (c (n "threadpool-simple") (v "0.1.1") (h "1b6d5wsf2v6d8hdi9ybnrbk12xxn9zhcs85q9ilqp2ymy518z91y")))

(define-public crate-threadpool-simple-0.1.11 (c (n "threadpool-simple") (v "0.1.11") (h "0vn89g6xy1higivx2rgb5vn1xhw5615xylqinj3ma8s11j3863kn")))

(define-public crate-threadpool-simple-0.1.12 (c (n "threadpool-simple") (v "0.1.12") (h "0alp9d7nrp12lq9ry658wqb12564sx7l3xrf1p6h5830ngr6vf0c")))

