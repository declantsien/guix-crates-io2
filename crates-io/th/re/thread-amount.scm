(define-module (crates-io th re thread-amount) #:use-module (crates-io))

(define-public crate-thread-amount-0.1.0 (c (n "thread-amount") (v "0.1.0") (d (list (d (n "field-offset") (r "^0.3.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_System_Diagnostics_ToolHelp" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1s23iya1glymqjagb1jfcv7rmx22haknd9ydgxf20vgnwdrmn0rc")))

(define-public crate-thread-amount-0.1.1 (c (n "thread-amount") (v "0.1.1") (d (list (d (n "field-offset") (r "^0.3.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_System_Diagnostics_ToolHelp" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "1s0hl2w9y0xqq8a4z7lskfz2d0qngjaggyzzhzs2il38xydz7dxj")))

(define-public crate-thread-amount-0.1.2 (c (n "thread-amount") (v "0.1.2") (d (list (d (n "field-offset") (r "^0.3.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.37.0") (f (quote ("alloc" "Win32_System_Diagnostics_ToolHelp" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "0qf9nrsabqyvz1jqajls1qf87b1pagxhgkcrh3va84hpfk0ap269")))

(define-public crate-thread-amount-0.1.3 (c (n "thread-amount") (v "0.1.3") (d (list (d (n "field-offset") (r "^0.3.4") (d #t) (t "cfg(windows)") (k 0)) (d (n "windows") (r "^0.38.0") (f (quote ("alloc" "Win32_System_Diagnostics_ToolHelp" "Win32_Foundation"))) (d #t) (t "cfg(windows)") (k 0)))) (h "02141a76fwnzhxkpk8habp65ppvgflrl9bcz8215zkchvwrxk1nc")))

