(define-module (crates-io th re thread-object) #:use-module (crates-io))

(define-public crate-thread-object-0.1.0 (c (n "thread-object") (v "0.1.0") (h "04yd3y43z5fvsn2h0f67d35as03lmf178bahdbbi7hm0yq4jv48y")))

(define-public crate-thread-object-0.2.0 (c (n "thread-object") (v "0.2.0") (h "1qhk5nls0cwmamzz9dy2rnar9lr3mn2s3aw4mc2pbcqym3343afj")))

