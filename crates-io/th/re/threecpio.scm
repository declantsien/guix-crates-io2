(define-module (crates-io th re threecpio) #:use-module (crates-io))

(define-public crate-threecpio-0.1.0 (c (n "threecpio") (v "0.1.0") (d (list (d (n "assert_cmd") (r "^2.0") (d #t) (k 2)) (d (n "gumdrop") (r "^0.8") (d #t) (k 0)) (d (n "predicates") (r "^3.0") (d #t) (k 2)))) (h "04w6kaxyz7klm1gbkksc1q1my5lxhnv5ybi7w77ln9914qvaab5c")))

