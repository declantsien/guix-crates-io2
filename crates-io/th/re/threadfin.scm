(define-module (crates-io th re threadfin) #:use-module (crates-io))

(define-public crate-threadfin-0.1.0 (c (n "threadfin") (v "0.1.0") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "futures-timer") (r "^3") (d #t) (k 2)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.6") (d #t) (k 2)) (d (n "threadpool") (r "^1") (d #t) (k 2)))) (h "0knxpccg68klba2c0v0znrld3xiqgnz9xsx21j4av624mqczf9sf")))

(define-public crate-threadfin-0.1.1 (c (n "threadfin") (v "0.1.1") (d (list (d (n "criterion") (r "^0.3") (d #t) (k 2)) (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "futures-timer") (r "^3") (d #t) (k 2)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r "^1") (d #t) (k 0)) (d (n "rusty_pool") (r "^0.6") (d #t) (k 2)) (d (n "threadpool") (r "^1") (d #t) (k 2)) (d (n "waker-fn") (r "^1") (d #t) (k 0)))) (h "18a53c4qb0zlrkg17p2r7wkznsa4graqkv132x8i9zpy81x68jry")))

(define-public crate-threadfin-0.1.2 (c (n "threadfin") (v "0.1.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "num_cpus") (r "^1") (d #t) (k 0)) (d (n "once_cell") (r ">=1.0, <=1.14") (d #t) (k 0)) (d (n "waker-fn") (r "^1") (d #t) (k 0)) (d (n "futures-timer") (r "^3") (d #t) (k 2)))) (h "0n352g46waz80c1xjg0kxyx2w4gnlsmw0mw9sswj5hk40lmcnfxl")))

