(define-module (crates-io th re three-edge-connected) #:use-module (crates-io))

(define-public crate-three-edge-connected-0.1.0 (c (n "three-edge-connected") (v "0.1.0") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "gfa") (r "^0.8") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "0d5v1zih3519scsa5y1nb0f0knx3l0gwn75ayjmlzwjjz8agbm61")))

(define-public crate-three-edge-connected-0.2.0 (c (n "three-edge-connected") (v "0.2.0") (d (list (d (n "bstr") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "fxhash") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "gfa") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 0)))) (h "19ck3f6nqn3mkfx59ba89rd634d55484703ym88nacwfl9fla4s3")))

(define-public crate-three-edge-connected-0.2.1 (c (n "three-edge-connected") (v "0.2.1") (d (list (d (n "bstr") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "fxhash") (r ">=0.2.0, <0.3.0") (d #t) (k 0)) (d (n "gfa") (r ">=0.9.0, <0.10.0") (d #t) (k 0)) (d (n "structopt") (r ">=0.3.0, <0.4.0") (d #t) (k 2)))) (h "0w5hs0ykvd2js5mv5hdaja6951787chm990z34bc0w1f79r8srkh")))

(define-public crate-three-edge-connected-0.2.2 (c (n "three-edge-connected") (v "0.2.2") (d (list (d (n "bstr") (r "^0.2") (d #t) (k 0)) (d (n "fxhash") (r "^0.2") (d #t) (k 0)) (d (n "gfa") (r "^0.10") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 2)))) (h "0z9mqq013dqfgdj740sgkahrm6navrbd948w8rwax6sp0059bkqg")))

