(define-module (crates-io th re threefish) #:use-module (crates-io))

(define-public crate-threefish-0.0.0 (c (n "threefish") (v "0.0.0") (h "01sg8lcrgsk1y7b2lpwn34f92wakf32d9rcbxgxzp8414fzy3gy3") (y #t)))

(define-public crate-threefish-0.0.1 (c (n "threefish") (v "0.0.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "byte-tools") (r "^0.1") (d #t) (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)))) (h "0pv4005cqizr3xy1lqabk7gffxm0igal9yp1q077sv9qckhvqvb4")))

(define-public crate-threefish-0.1.0 (c (n "threefish") (v "0.1.0") (d (list (d (n "block-cipher") (r "^0.7") (d #t) (k 0)) (d (n "block-cipher") (r "^0.7") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byte-tools") (r "^0.1") (d #t) (k 0)))) (h "14zccdqc94x1dcdsc71nsdda8ckhsqf5lj32cgaw58br311fwd1c")))

(define-public crate-threefish-0.2.0 (c (n "threefish") (v "0.2.0") (d (list (d (n "block-cipher") (r "^0.8") (d #t) (k 0)) (d (n "block-cipher") (r "^0.8") (f (quote ("dev"))) (d #t) (k 2)) (d (n "byte-tools") (r "^0.1") (d #t) (k 0)))) (h "1vn307pc915h7sa6pf89qx1rg1cja35gz4bdy4664yj9z74ggkqz")))

(define-public crate-threefish-0.3.0 (c (n "threefish") (v "0.3.0") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "cipher") (r "^0.2") (f (quote ("dev"))) (d #t) (k 2)))) (h "1kgvdxiqfb6kz5wq79knf0yjk37j946hbcapnpknvq6bd3b7k5hr")))

(define-public crate-threefish-0.4.0 (c (n "threefish") (v "0.4.0") (d (list (d (n "cipher") (r "^0.3") (d #t) (k 0)) (d (n "cipher") (r "^0.3") (f (quote ("dev"))) (d #t) (k 2)))) (h "1cyk9n1ysyn25jzmisfhka3d4q17c5sdfh6wqip4cvcx7ra4n5yx")))

(define-public crate-threefish-0.5.0 (c (n "threefish") (v "0.5.0") (d (list (d (n "cipher") (r "^0.4") (d #t) (k 0)) (d (n "cipher") (r "^0.4") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3") (d #t) (k 2)))) (h "0m0mwfq0hpww8pr0xi7jpir5z87pd3r3jg6abxpb1indcs9b02jc") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-threefish-0.5.1 (c (n "threefish") (v "0.5.1") (d (list (d (n "cipher") (r "^0.4.2") (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)))) (h "062nhlrlcz5hbb4axxcxyq96036c4i9pymi22v58a1kxz4mhbm8f") (f (quote (("zeroize" "cipher/zeroize")))) (r "1.56")))

(define-public crate-threefish-0.5.2 (c (n "threefish") (v "0.5.2") (d (list (d (n "cipher") (r "^0.4.2") (o #t) (d #t) (k 0)) (d (n "cipher") (r "^0.4.2") (f (quote ("dev"))) (d #t) (k 2)) (d (n "hex-literal") (r "^0.3.3") (d #t) (k 2)) (d (n "zeroize") (r "^1.6") (o #t) (k 0)))) (h "0bp6fsff663s7ngw9jjggl4yflk4iixy9ywkban3z5qnrz4d14x6") (f (quote (("default" "cipher")))) (r "1.56")))

