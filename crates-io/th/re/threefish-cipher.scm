(define-module (crates-io th re threefish-cipher) #:use-module (crates-io))

(define-public crate-threefish-cipher-0.3.0 (c (n "threefish-cipher") (v "0.3.0") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "1w2qp1jmp1bs24ajdk9fa5v40y7ilcgazb1kiz6hygqqd5zl090j")))

(define-public crate-threefish-cipher-0.3.1 (c (n "threefish-cipher") (v "0.3.1") (d (list (d (n "block-cipher-trait") (r "^0.6") (d #t) (k 0)) (d (n "byteorder") (r "^1") (k 0)) (d (n "generic-array") (r "^0.12") (d #t) (k 0)) (d (n "hex-literal") (r "^0.1") (d #t) (k 2)))) (h "09cnqyw48nc09353rdcs11y8n826c6j803x5qd2j9klc3nxh5sy6") (f (quote (("no_unroll"))))))

(define-public crate-threefish-cipher-0.4.0 (c (n "threefish-cipher") (v "0.4.0") (d (list (d (n "cipher") (r "^0.2") (d #t) (k 0)) (d (n "hex-literal") (r "^0.2") (d #t) (k 2)))) (h "0iza2b9r7lzlrf82j7pl5fqj04m6cl3ycinx67yn1vhzjgxi3m25") (f (quote (("no_unroll"))))))

