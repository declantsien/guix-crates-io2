(define-module (crates-io th re thread_isolated) #:use-module (crates-io))

(define-public crate-thread_isolated-0.1.0 (c (n "thread_isolated") (v "0.1.0") (h "0q73wh0yzr3dy43d8w0v7rhbkhwdwwayvrllv5sy9bki6ikcpy25")))

(define-public crate-thread_isolated-0.1.1 (c (n "thread_isolated") (v "0.1.1") (h "0lakgf40mfv1djybwifaikfaw083y9727z3bjmnf0cwn9hvixjnv")))

