(define-module (crates-io th re thread-executor) #:use-module (crates-io))

(define-public crate-thread-executor-0.1.0 (c (n "thread-executor") (v "0.1.0") (d (list (d (n "pin-utils") (r "^0.1.0") (d #t) (k 0)) (d (n "static_assertions") (r "^1.1.0") (d #t) (k 0)))) (h "1wm2khb4v2pxjrp50whflaz5g2njwxqsnr1qmgin7nkw7ibrf38b")))

