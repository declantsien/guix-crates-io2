(define-module (crates-io th re threading) #:use-module (crates-io))

(define-public crate-threading-0.1.0 (c (n "threading") (v "0.1.0") (h "01w5k20ya3dr7zap13kwz9hn3hza2syz3lrjwiawh979whc2h06w")))

(define-public crate-threading-0.1.1 (c (n "threading") (v "0.1.1") (d (list (d (n "num_cpus") (r "^1.3") (d #t) (k 0)))) (h "0nxsk160bxsxqf8d8pf9dqbqdj3mwrdqwppza3hyglwdihxbjh17")))

(define-public crate-threading-0.1.2 (c (n "threading") (v "0.1.2") (d (list (d (n "num_cpus") (r "^1.3") (d #t) (k 0)))) (h "0s5l6rpk22vmb5lmqpqbm9fw9hldf425hacfyjpvvh38wi5z2365")))

