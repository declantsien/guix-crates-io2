(define-module (crates-io th re thread_tryjoin) #:use-module (crates-io))

(define-public crate-thread_tryjoin-0.2.0 (c (n "thread_tryjoin") (v "0.2.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0bjaqdpm3vamhxh0sa4x7ry7liks9c8929wqj5s3ml9sclpssrd5")))

(define-public crate-thread_tryjoin-0.2.1 (c (n "thread_tryjoin") (v "0.2.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0slh6d9nwz6z9nzk6gv3lcvj2jqw6z51m44nwwk8fi08v4pna8pz")))

(define-public crate-thread_tryjoin-0.3.0 (c (n "thread_tryjoin") (v "0.3.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "176j86iya40bzvbd98qpcgyi2vdd1zhfqp3vqkv02jfiphsxgziw")))

