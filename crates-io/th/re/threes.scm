(define-module (crates-io th re threes) #:use-module (crates-io))

(define-public crate-threes-0.1.0 (c (n "threes") (v "0.1.0") (h "0clg4am0l5g3ia56ypx0gfga2s6z14mpplnzkrii7h2014wjyppb")))

(define-public crate-threes-0.2.0 (c (n "threes") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "1zavihjs7f1gck142ma9h0v8wh3jasp8lqqqdykc677xf7xm2hij")))

(define-public crate-threes-0.3.0 (c (n "threes") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "rand_pcg") (r "^0.3.1") (f (quote ("serde1"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.196") (f (quote ("derive" "rc"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.113") (d #t) (k 0)))) (h "0fzckjb4g6179mfwvin672r15bywgnkwc7fcv821rklfl23cx4f0")))

