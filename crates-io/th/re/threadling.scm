(define-module (crates-io th re threadling) #:use-module (crates-io))

(define-public crate-threadling-0.1.0 (c (n "threadling") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "console_log") (r "^1.0") (f (quote ("color"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm_thread") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "1npy3wqn8vlmgprmca3xywaqgibigi13a35zk77wqi5rqxaz26lx") (r "1.75")))

(define-public crate-threadling-0.1.1 (c (n "threadling") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "bytes") (r "^1.6") (d #t) (k 0)) (d (n "console_error_panic_hook") (r "^0.1") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "console_log") (r "^1.0") (f (quote ("color"))) (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)) (d (n "wasm_thread") (r "^0.3") (d #t) (t "cfg(target_arch = \"wasm32\")") (k 0)))) (h "13azcibc4jyylwhl0rrr1ayf8pcnxiqsrbdsayqw05m7rw0b9bvs") (r "1.75")))

