(define-module (crates-io th re threadpool_scope) #:use-module (crates-io))

(define-public crate-threadpool_scope-0.1.0 (c (n "threadpool_scope") (v "0.1.0") (d (list (d (n "num_cpus") (r "^1.13") (d #t) (k 0)) (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "rayon") (r "^1.3") (d #t) (k 0)) (d (n "scoped_threadpool") (r "^0.1") (d #t) (k 0)) (d (n "threadpool") (r "^1.8") (d #t) (k 0)))) (h "0x1g2k6ri9x73lc2fj3fp32z74sc6jrhna645hhjqavkhapiivi3")))

