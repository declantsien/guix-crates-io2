(define-module (crates-io th re thread_reviver) #:use-module (crates-io))

(define-public crate-thread_reviver-0.1.0 (c (n "thread_reviver") (v "0.1.0") (d (list (d (n "serenity") (r "^0.10") (f (quote ("builder" "cache" "client" "gateway" "http" "model" "utils" "rustls_backend"))) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "0rqa4q80szj3ywvlk52h94n1cgb7c6arpgbxlg0j6p3r5rcrvmmh")))

(define-public crate-thread_reviver-0.2.0 (c (n "thread_reviver") (v "0.2.0") (d (list (d (n "serenity") (r "^0.10") (f (quote ("builder" "cache" "client" "gateway" "http" "model" "utils" "rustls_backend"))) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "11ig468l54845mb3v8blpipdmwvmxx842a2h19klxqkqaymcjjgm")))

(define-public crate-thread_reviver-0.2.1 (c (n "thread_reviver") (v "0.2.1") (d (list (d (n "serenity") (r "^0.10") (f (quote ("builder" "cache" "client" "gateway" "http" "model" "utils" "rustls_backend"))) (k 0)) (d (n "tokio") (r "^1.17") (f (quote ("macros" "rt-multi-thread"))) (d #t) (k 0)))) (h "1hnd1p29g86k556f685kykpiwjw4v4r6070ln0vja4c9nmvcjcj2")))

