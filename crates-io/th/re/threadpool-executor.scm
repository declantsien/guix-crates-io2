(define-module (crates-io th re threadpool-executor) #:use-module (crates-io))

(define-public crate-threadpool-executor-0.1.0 (c (n "threadpool-executor") (v "0.1.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 2)))) (h "0n2afgq77gcpqhny4xzd2nn9d6i4xd55wd59c25facg2gwk52r5x") (y #t)))

(define-public crate-threadpool-executor-0.2.0 (c (n "threadpool-executor") (v "0.2.0") (d (list (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 2)))) (h "11cgcxq47sc05xkvrwn2d9pa6f1rb25gvj47grxbc94845bfn548") (y #t)))

(define-public crate-threadpool-executor-0.2.1 (c (n "threadpool-executor") (v "0.2.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 2)))) (h "0g8c0cnl06kkdlqad6rprlalsgy0rcyh8ivr3314d12jz9f1s9py") (y #t)))

(define-public crate-threadpool-executor-0.2.2 (c (n "threadpool-executor") (v "0.2.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 2)))) (h "1c3lwxgg47a2g1j93im0d8b5pr88zi3gvvg7dzdj8xbgrsxzajra")))

(define-public crate-threadpool-executor-0.3.0 (c (n "threadpool-executor") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 2)))) (h "16hrkdpn7qyak8jclxba453lxlirng6nxwi5yvbgr4cy0krpvjvj") (y #t)))

(define-public crate-threadpool-executor-0.3.1 (c (n "threadpool-executor") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 2)))) (h "0c9g53xi8qlgaxadn0mpd7v500jb5ic36gqqp28q7h232hal0mq6")))

(define-public crate-threadpool-executor-0.3.2 (c (n "threadpool-executor") (v "0.3.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "simple_logger") (r "^4") (d #t) (k 2)))) (h "1vgrkm59jxnc9hl035d7gwwjqx2gsriq89wgdsxxvys14igsvpag")))

