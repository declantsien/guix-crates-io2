(define-module (crates-io th re thread-scoped) #:use-module (crates-io))

(define-public crate-thread-scoped-0.0.1 (c (n "thread-scoped") (v "0.0.1") (h "0krnj7d0i741y5h64wl2s6dsfzkwf928p65dk9j6g6w4bw2bfdjc") (f (quote (("unstable"))))))

(define-public crate-thread-scoped-1.0.0 (c (n "thread-scoped") (v "1.0.0") (h "00fgsywlb1fsbib1nj1xkwdj050c8r9z8m766ffhfbmxzaw7qzh4") (f (quote (("unstable"))))))

(define-public crate-thread-scoped-1.0.1 (c (n "thread-scoped") (v "1.0.1") (h "0s5fsl9swn9kqppm0bwbj0m6gh5mkm6w7sy8hkhyy2bd4k77sf0l") (f (quote (("unstable"))))))

(define-public crate-thread-scoped-1.0.2 (c (n "thread-scoped") (v "1.0.2") (h "16dxl8grpii4vh20qikv2x7r871ggsf9m733xysv1lz506inmfxw")))

