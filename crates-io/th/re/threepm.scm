(define-module (crates-io th re threepm) #:use-module (crates-io))

(define-public crate-threepm-0.1.0 (c (n "threepm") (v "0.1.0") (d (list (d (n "byte-slice-cast") (r "^1.2.2") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hound") (r "^3.5") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "16dkzkg57da7zhvljwxv2zr70zrx6mml9ggpi29rl9qkbhg5a8qj") (f (quote (("code-in-ram"))))))

(define-public crate-threepm-0.1.1 (c (n "threepm") (v "0.1.1") (d (list (d (n "byte-slice-cast") (r "^1.2.2") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hound") (r "^3.5") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1ba1jwwxx8ax0fw5irydhnxkj4fn9m42l73yfkyhxkidhvvb92yy") (f (quote (("code-in-ram"))))))

(define-public crate-threepm-0.1.2 (c (n "threepm") (v "0.1.2") (d (list (d (n "byte-slice-cast") (r "^1.2.2") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hound") (r "^3.5") (o #t) (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "024vykfzfg3n6ic9n0nk3gingwaxgi2dm28kiq1qi3878ndmm4rb") (f (quote (("code-in-ram"))))))

(define-public crate-threepm-0.1.3 (c (n "threepm") (v "0.1.3") (d (list (d (n "byte-slice-cast") (r "^1.2.2") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hound") (r "^3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0kdgzplqibnv7mb7gr7p4p7d1z8nqp6k111zpk92jmv5kif040ka") (f (quote (("code-in-ram"))))))

(define-public crate-threepm-0.2.0 (c (n "threepm") (v "0.2.0") (d (list (d (n "byte-slice-cast") (r "^1.2.2") (d #t) (k 2)) (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "hound") (r "^3.5") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1pwz505abw5xqxl6flbgkylhn4xai0hcbm4j9hs897q3xs8xi5sp") (f (quote (("code-in-ram"))))))

