(define-module (crates-io th re thread-tree) #:use-module (crates-io))

(define-public crate-thread-tree-0.2.0 (c (n "thread-tree") (v "0.2.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "1789bj154jrjqs9bj6brlalxrwrnpzpn4276xvvpwrf1msjkwdmn") (f (quote (("unstable-thread-sea"))))))

(define-public crate-thread-tree-0.3.0 (c (n "thread-tree") (v "0.3.0") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "18i01i2j9xdx6acvvbfp41lv22d76jfcvyawxwqc0bjnvnjsxyzn") (f (quote (("unstable-thread-sea"))))))

(define-public crate-thread-tree-0.3.1 (c (n "thread-tree") (v "0.3.1") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "1pgi9y9l98sfn5aapih1ss89y6kxlha7v8a8r13737misz77l72j") (f (quote (("unstable-thread-sea"))))))

(define-public crate-thread-tree-0.3.2 (c (n "thread-tree") (v "0.3.2") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "0mqsmabyf4vd18ci1xxwdyd77xv3b1dc24qx52ywq3nwch4125fx") (f (quote (("unstable-thread-sea"))))))

(define-public crate-thread-tree-0.3.3 (c (n "thread-tree") (v "0.3.3") (d (list (d (n "crossbeam-channel") (r "^0.5") (d #t) (k 0)) (d (n "once_cell") (r "^1.5") (d #t) (k 2)))) (h "0c6n8m5xrxffxkvfqbn7z09n38r493hn77sdjljkm5a7p063gggz")))

