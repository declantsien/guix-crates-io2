(define-module (crates-io th re threed-ice-sys) #:use-module (crates-io))

(define-public crate-threed-ice-sys-0.0.1 (c (n "threed-ice-sys") (v "0.0.1") (h "0pvx3dzhm02lzjf20vjpckr9nnnqnzfad4395c4xkync9r5rmds3")))

(define-public crate-threed-ice-sys-0.1.0 (c (n "threed-ice-sys") (v "0.1.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "177f6sqx78432x52rni8gg5mpbprd0sjid94z7if39fm84hg2di0")))

(define-public crate-threed-ice-sys-0.1.1 (c (n "threed-ice-sys") (v "0.1.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1lwklygxnbinn4yqmknhpgcqnjla8kfspirvbspvarp6ppmw1cia")))

(define-public crate-threed-ice-sys-0.1.2 (c (n "threed-ice-sys") (v "0.1.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "13iiqqf1p5kz329asnh35x6v1nik60snm8ciba63w62599irf1aw")))

(define-public crate-threed-ice-sys-0.2.0 (c (n "threed-ice-sys") (v "0.2.0") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "014m27wl68mg07hysp1f4gbhib0adaq1ccrqg8kf41m15scnlish")))

(define-public crate-threed-ice-sys-0.2.1 (c (n "threed-ice-sys") (v "0.2.1") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1bn833yhrvilfv02ny6cpvagbyqznybdzl2nmx3sfaf5400bmbsb")))

(define-public crate-threed-ice-sys-0.2.2 (c (n "threed-ice-sys") (v "0.2.2") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "14wy8fhrw00dfnj34hif0f2mad5g229927ldl4fafgx45c73xbaw")))

(define-public crate-threed-ice-sys-0.2.3 (c (n "threed-ice-sys") (v "0.2.3") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "110762w21dmpbcppjric8ia899cvrpzji5mkw52r562p1mzcdgxg")))

(define-public crate-threed-ice-sys-0.2.4 (c (n "threed-ice-sys") (v "0.2.4") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1zgwxcssipl0q1lg8dpachl5b88mlzvlv6wdkdhh6jb2lnxcwmhl")))

(define-public crate-threed-ice-sys-0.2.5 (c (n "threed-ice-sys") (v "0.2.5") (d (list (d (n "assert") (r "*") (d #t) (k 2)) (d (n "fixture") (r "*") (d #t) (k 2)) (d (n "libc") (r "*") (d #t) (k 0)) (d (n "superlu-sys") (r "*") (d #t) (k 0)) (d (n "temporary") (r "*") (d #t) (k 2)))) (h "1gz62178ad0z66rhcwi6mqg2whgziq6g1samhrsqpfsxwpd0ap3h")))

(define-public crate-threed-ice-sys-0.2.6 (c (n "threed-ice-sys") (v "0.2.6") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "fixture") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "superlu-sys") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "1shyy32gbxm6hlgk4sarnvz06l72qw52mgc722b0w8y4gj3rvjg9")))

(define-public crate-threed-ice-sys-0.2.7 (c (n "threed-ice-sys") (v "0.2.7") (d (list (d (n "assert") (r "^0.6") (d #t) (k 2)) (d (n "fixture") (r "^0.2") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "superlu-sys") (r "^0.2") (d #t) (k 0)) (d (n "temporary") (r "^0.5") (d #t) (k 2)))) (h "03fmh9p68n9wm954dnp4p37g87d1nl9w3k1a77hwrpxydfag6xly")))

(define-public crate-threed-ice-sys-0.3.0 (c (n "threed-ice-sys") (v "0.3.0") (d (list (d (n "assert") (r "^0.7") (d #t) (k 2)) (d (n "fixture") (r "^0.3") (d #t) (k 2)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "superlu-sys") (r "^0.3") (d #t) (k 0)) (d (n "temporary") (r "^0.6") (d #t) (k 2)))) (h "0r8vjqh6la68r2h9ryx8hxa1m4pigxvq4136zk3n0av2rqg1nj51")))

