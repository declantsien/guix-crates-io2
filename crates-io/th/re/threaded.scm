(define-module (crates-io th re threaded) #:use-module (crates-io))

(define-public crate-threaded-0.1.0 (c (n "threaded") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)))) (h "0wpyvjaa2v6qq019c8s17bcs7ybzfnbq8yi102jaix5157xi4x1j")))

(define-public crate-threaded-0.2.0 (c (n "threaded") (v "0.2.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)))) (h "0jh4w7l286jlmdcvxbgiwlia4pnm4ghigk1xpi4kzlql5mrcndwm")))

(define-public crate-threaded-0.3.0 (c (n "threaded") (v "0.3.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "0ia6dz3skal9mkl1jx3836578yzqr5hsihcv6d7v8976mdhq3znj")))

(define-public crate-threaded-0.3.1 (c (n "threaded") (v "0.3.1") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "1dl4hx6nhg40kjmgkckdzlaqqlpp0dcql982qyq5mwbhcfw7p38d")))

(define-public crate-threaded-0.4.0 (c (n "threaded") (v "0.4.0") (d (list (d (n "crossbeam") (r "^0.8.0") (d #t) (k 0)) (d (n "uuid") (r "^0.8.2") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "19yr5khb5qhb01q7n3wz6ljz031sipv2m57003px7z8mxqwhcyzk")))

