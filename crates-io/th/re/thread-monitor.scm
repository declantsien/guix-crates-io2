(define-module (crates-io th re thread-monitor) #:use-module (crates-io))

(define-public crate-thread-monitor-0.1.0 (c (n "thread-monitor") (v "0.1.0") (d (list (d (n "crossbeam") (r "^0.8") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "oneshot") (r "^0.2") (d #t) (k 0) (p "transmitter")) (d (n "time") (r "^0.3") (d #t) (k 0)))) (h "0kx1lrnjzq3ah0zz0b98by84aqphf7bfn0a8djwz4kdyh9i0076h")))

