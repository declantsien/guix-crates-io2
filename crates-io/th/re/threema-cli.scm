(define-module (crates-io th re threema-cli) #:use-module (crates-io))

(define-public crate-threema-cli-0.1.0 (c (n "threema-cli") (v "0.1.0") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "threema") (r "^0.1") (d #t) (k 0)))) (h "1bx6wlmd44amy0n984az1bzdrnqygf5v3hv0jcdjkpxpv5cnclqk")))

(define-public crate-threema-cli-0.1.4 (c (n "threema-cli") (v "0.1.4") (d (list (d (n "clap") (r "^2.33") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "threema") (r "^0.1") (d #t) (k 0)))) (h "0yj3vkv9100cvffhh8sa7r119kfzm69ji8y4kxh3vxb6jsc166rb")))

(define-public crate-threema-cli-0.1.5 (c (n "threema-cli") (v "0.1.5") (d (list (d (n "clap") (r "^4.0.29") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "pretty_env_logger") (r "^0.4") (d #t) (k 0)) (d (n "threema") (r "^0.2") (d #t) (k 0)))) (h "1g6crapxvzsxxrjc12q4mv4s9kly51y6q9ximxxm10wv5md8hanm")))

