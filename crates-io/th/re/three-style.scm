(define-module (crates-io th re three-style) #:use-module (crates-io))

(define-public crate-three-style-0.1.0 (c (n "three-style") (v "0.1.0") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "three-style-lib") (r "^0.1.0") (d #t) (k 0)))) (h "0kmaav95a9sg8p9nqin536fp0pyb6awndc07n8a7d7k1l14sw6yy")))

(define-public crate-three-style-0.1.1 (c (n "three-style") (v "0.1.1") (d (list (d (n "clap") (r "^4.5.1") (f (quote ("derive" "color"))) (d #t) (k 0)) (d (n "three-style-lib") (r "^0.1.1") (d #t) (k 0)))) (h "0c0ly9636m13pm7minf9v56j2l1ps7n4qny6zfb48db807kzxh8x")))

