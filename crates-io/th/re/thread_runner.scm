(define-module (crates-io th re thread_runner) #:use-module (crates-io))

(define-public crate-thread_runner-0.1.0 (c (n "thread_runner") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12.1") (d #t) (k 0)))) (h "0vzzqdv7c1wbbnn6k7q43bvkikrp08329fx0skmyxd1v8wk8v6fv")))

(define-public crate-thread_runner-0.2.0 (c (n "thread_runner") (v "0.2.0") (d (list (d (n "tokio") (r "^1.27.0") (f (quote ("rt" "rt-multi-thread"))) (d #t) (k 0)))) (h "1iqll5p2hpcvn1psp3gx6gk7996cdq6lblfgsn0vrdckwr6ngvmd")))

