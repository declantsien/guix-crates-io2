(define-module (crates-io th hp thhp) #:use-module (crates-io))

(define-public crate-thhp-0.1.0 (c (n "thhp") (v "0.1.0") (d (list (d (n "arrayvec") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "httparse") (r "^1.2") (d #t) (k 2)) (d (n "picohttpparser-sys") (r "^1.0") (k 2)))) (h "1y7nk1g1spjdy2f8f8arz0h0h85w3c49cbxcraydvx3nws73ki2v") (f (quote (("std") ("nightly" "picohttpparser-sys/sse4") ("default" "std"))))))

(define-public crate-thhp-0.1.1 (c (n "thhp") (v "0.1.1") (d (list (d (n "arrayvec") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "httparse") (r "^1.2") (d #t) (k 2)) (d (n "picohttpparser-sys") (r "^1.0") (k 2)))) (h "01qvbk3ghcanvk60hhr84jj1gi4qdd3a8kxbzlw4xjjvaksqdrnk") (f (quote (("std") ("nightly" "picohttpparser-sys/sse4") ("default" "std"))))))

(define-public crate-thhp-0.2.0 (c (n "thhp") (v "0.2.0") (d (list (d (n "arrayvec") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "httparse") (r "^1.3") (d #t) (k 2)) (d (n "picohttpparser-sys") (r "^1.0") (f (quote ("sse4"))) (k 2)) (d (n "version_check") (r "^0.1") (d #t) (k 1)))) (h "0fcx9z1mbjnspmyli8q150kvhn8chcwqmi9zzzzz8j5n79yb29pb") (f (quote (("thhp_simd") ("std") ("default" "std"))))))

