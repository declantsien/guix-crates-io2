(define-module (crates-io th ue thue) #:use-module (crates-io))

(define-public crate-thue-0.1.0 (c (n "thue") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "15s0pg0hjzq7r3vd5ly71mpx79wcpm30gh9rz4cccmxj03g8895w")))

(define-public crate-thue-0.1.1 (c (n "thue") (v "0.1.1") (d (list (d (n "clap") (r "^4.4.8") (f (quote ("derive"))) (d #t) (k 0)))) (h "1rb3n2x3nqxl0zfq9gr3lhlkx3jgri98hyrfjmp56lm8sjpnal1j")))

