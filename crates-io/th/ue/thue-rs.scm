(define-module (crates-io th ue thue-rs) #:use-module (crates-io))

(define-public crate-thue-rs-0.1.0 (c (n "thue-rs") (v "0.1.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1hd3j93b8s4qa88c2lzdyh2cf8x2rahpy60aygmlgy64b7117rpv")))

(define-public crate-thue-rs-0.1.1 (c (n "thue-rs") (v "0.1.1") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0bghw71l2p97dbslflr1s2ksc551izm65cacracmnq1hnzid9wxf")))

