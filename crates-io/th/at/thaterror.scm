(define-module (crates-io th at thaterror) #:use-module (crates-io))

(define-public crate-thaterror-0.0.0 (c (n "thaterror") (v "0.0.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "07l8gqi0q98mc4j4s5whsqlz8ffnkzifbn81r8pfpb9ax8d162x3")))

(define-public crate-thaterror-0.1.0 (c (n "thaterror") (v "0.1.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "169lgc67v5wvc76z6yfgiyigfibi814ad2i7bib5ksnki2d9sn5p")))

(define-public crate-thaterror-0.2.0 (c (n "thaterror") (v "0.2.0") (d (list (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "1zfadzpfwahh6bd9vdxhxnpacv0nh11w3jikp79lf149a2p7p27h")))

