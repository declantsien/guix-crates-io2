(define-module (crates-io th at that) #:use-module (crates-io))

(define-public crate-that-0.1.0 (c (n "that") (v "0.1.0") (h "1mxfzm1d2385878mmcdrgaxs3pgaixf658k4iz26gzzsh53c9j27")))

(define-public crate-that-0.1.1 (c (n "that") (v "0.1.1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nothing") (r "^0.1") (d #t) (k 0)))) (h "1z4ih6nlaac4x2iy6z5vdj30jh5z177s9gjwd99wdf7r22jv2kgc")))

(define-public crate-that-0.1.1-1 (c (n "that") (v "0.1.1-1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nothing") (r "^0.1") (d #t) (k 0)))) (h "0grspx6wvv7746hdbsibmpsc2ars38gz3y0msal2cwwm6ih35lji")))

(define-public crate-that-0.1.2 (c (n "that") (v "0.1.2") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nothing") (r "^0.1") (d #t) (k 0)))) (h "1ix1jb8w7qirl4zbdwdnny78c2x02rihsvxm5jn5w8w5x5aagryy")))

(define-public crate-that-0.1.2-1 (c (n "that") (v "0.1.2-1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nothing") (r "^0.1") (d #t) (k 0)))) (h "0fww0ajzjzch9rgka2yz535qsm81qdzh41snjqbzrglapsivk51g")))

(define-public crate-that-0.1.3 (c (n "that") (v "0.1.3") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nothing") (r "^0.1") (d #t) (k 0)))) (h "1ypr03vwwhdjqqhd5rl53s961yg0d4z78qa1m1kxd5pbq4s8gqaz")))

(define-public crate-that-0.1.3-1 (c (n "that") (v "0.1.3-1") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nothing") (r "^0.1") (d #t) (k 0)))) (h "1766h25hb66marf8sms1z102lr0z7faz7im1ij5aqlbkn4wbcfjf")))

(define-public crate-that-0.1.3-2 (c (n "that") (v "0.1.3-2") (d (list (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 0)) (d (n "nothing") (r "^0.1") (d #t) (k 0)))) (h "188w7f49gr5rnmnfmq2mdwaz4zi4aqjp4wpfl3gspv2907z8ymqm")))

