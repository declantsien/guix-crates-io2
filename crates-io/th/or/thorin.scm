(define-module (crates-io th or thorin) #:use-module (crates-io))

(define-public crate-thorin-0.1.0 (c (n "thorin") (v "0.1.0") (d (list (d (n "cc") (r "^1.0") (d #t) (k 1)) (d (n "fallible-iterator") (r "^0.1.6") (d #t) (k 0)) (d (n "gimli") (r "^0.17.0") (d #t) (k 0)) (d (n "hex") (r "^0.3") (d #t) (k 0)) (d (n "libc") (r "^0.2.50") (d #t) (k 0)) (d (n "memmap") (r "^0.7") (d #t) (k 0)) (d (n "object") (r "^0.11") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "text_io") (r "^0.1") (d #t) (k 0)))) (h "0s6xlkssh45pxdrv99hscijnrzkh5vya4qzi50qk1hzicpjiyss5") (l "thorin")))

