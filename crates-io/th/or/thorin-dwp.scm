(define-module (crates-io th or thorin-dwp) #:use-module (crates-io))

(define-public crate-thorin-dwp-0.1.0 (c (n "thorin-dwp") (v "0.1.0") (d (list (d (n "gimli") (r "^0.26.1") (f (quote ("read" "write" "std"))) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "object") (r "^0.27.1") (f (quote ("archive" "read" "write" "compression"))) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "1lzyhj1b7r1x8a8f0w4pxdqd0ad4jqrkckvbxdd7c3cxbl6wvi1m")))

(define-public crate-thorin-dwp-0.1.1 (c (n "thorin-dwp") (v "0.1.1") (d (list (d (n "gimli") (r "^0.26.1") (f (quote ("read" "write" "std"))) (k 0)) (d (n "indexmap") (r "^1.7.0") (d #t) (k 0)) (d (n "object") (r "^0.27.1") (f (quote ("archive" "read" "write" "compression"))) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "0flm5mps6f6xip4pp63syj9c2f0fb0akb2824w692wyvpz01z783")))

(define-public crate-thorin-dwp-0.2.0 (c (n "thorin-dwp") (v "0.2.0") (d (list (d (n "gimli") (r "^0.26.1") (f (quote ("read" "write" "std"))) (k 0)) (d (n "hashbrown") (r "^0.11.2") (d #t) (k 0)) (d (n "object") (r "^0.28.1") (f (quote ("archive" "read" "write" "compression"))) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "1gj350ab7jfvlb9g9bgr8hl6qyacib818khv8p48fs8rkiav95fx")))

(define-public crate-thorin-dwp-0.3.0 (c (n "thorin-dwp") (v "0.3.0") (d (list (d (n "gimli") (r "^0.26.1") (f (quote ("read" "write" "std"))) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "object") (r "^0.29.0") (f (quote ("archive" "read" "write" "compression"))) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "0m52986s8hskcy3xsz0vh6l5551x4q1sn20iac3h9yfpd1w0rjz6")))

(define-public crate-thorin-dwp-0.4.0 (c (n "thorin-dwp") (v "0.4.0") (d (list (d (n "gimli") (r "^0.26.1") (f (quote ("read" "write" "std"))) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "object") (r "^0.30.0") (f (quote ("archive" "read" "write" "compression"))) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "0wcnc43m3skwsxk6lfr9rjcr1ak42h264mx93vqnp6q119kbz3ys")))

(define-public crate-thorin-dwp-0.5.0 (c (n "thorin-dwp") (v "0.5.0") (d (list (d (n "gimli") (r "^0.26.1") (f (quote ("read" "write" "std"))) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "object") (r "^0.30.0") (f (quote ("archive" "read" "write" "compression"))) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "1myv5cfjpr2l5ldkm7z3qplkkw4fsmyh5ygv3r8v5wb0w0xqdz0q")))

(define-public crate-thorin-dwp-0.6.0 (c (n "thorin-dwp") (v "0.6.0") (d (list (d (n "gimli") (r "^0.26.1") (f (quote ("read" "write" "std"))) (k 0)) (d (n "hashbrown") (r "^0.12.0") (d #t) (k 0)) (d (n "object") (r "^0.31.0") (f (quote ("archive" "read" "write" "compression"))) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "09yrn0w15dl02h87wx8p2v5k577sgrw1skn6h10rv20b6khl1h4q")))

(define-public crate-thorin-dwp-0.7.0 (c (n "thorin-dwp") (v "0.7.0") (d (list (d (n "gimli") (r "^0.28.0") (f (quote ("read" "write" "std"))) (k 0)) (d (n "hashbrown") (r "^0.14.0") (d #t) (k 0)) (d (n "object") (r "^0.32.0") (f (quote ("archive" "read" "write" "compression"))) (k 0)) (d (n "tracing") (r "^0.1.29") (d #t) (k 0)))) (h "16sv618d03l70xsc9f610j9aj8gn9hndvwrfd6di2vn0zvl2xdad")))

