(define-module (crates-io th or thoraudit) #:use-module (crates-io))

(define-public crate-thoraudit-0.1.0 (c (n "thoraudit") (v "0.1.0") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1q0rvakviia0wx2sagp3g6rcpn3glqh3lkgx681gx94zznyw4v1i")))

(define-public crate-thoraudit-0.1.1 (c (n "thoraudit") (v "0.1.1") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "10xyspwh4gczj4bwba6ygjvwjgwjka12cjfah7flh8cijhv8d2gq")))

(define-public crate-thoraudit-0.1.2 (c (n "thoraudit") (v "0.1.2") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "1qbj7lkq0xx6l0c76wnj8d6v58451s2w1q22qw76qy8s48yma5m2")))

(define-public crate-thoraudit-0.1.3 (c (n "thoraudit") (v "0.1.3") (d (list (d (n "anyhow") (r "^1.0.48") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.130") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.71") (d #t) (k 0)) (d (n "tokio") (r "^1.14.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0f7881f638xg4vx70wa7jrzvd5hscwy082s8gmmzlzls5cbjz7n9")))

