(define-module (crates-io th or thorium) #:use-module (crates-io))

(define-public crate-thorium-0.0.1 (c (n "thorium") (v "0.0.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2") (d #t) (k 0)))) (h "1lrl40ln3dj81ng4lahvvjic83y2l034l9nq8zsz9w4smvvar4w4")))

(define-public crate-thorium-0.0.2 (c (n "thorium") (v "0.0.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "winuser"))) (d #t) (k 0)) (d (n "wio") (r "^0.2") (d #t) (k 0)))) (h "0j1vmv3xh4jclrzv72qfn4l234wp752b7qsgi2swjvf6gr0ms9wa")))

(define-public crate-thorium-0.0.3 (c (n "thorium") (v "0.0.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi"))) (d #t) (k 0)))) (h "0vs5icg6zgvz1179fgcqp265yjglhyz29nvysfmkvvq1gl60h2kk")))

(define-public crate-thorium-0.0.4 (c (n "thorium") (v "0.0.4") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi"))) (d #t) (k 0)))) (h "1q5jriqdma4fh2na72qrawc41g8vzmznz0khl6kzyq05xfg3k6vv")))

(define-public crate-thorium-0.0.5 (c (n "thorium") (v "0.0.5") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi"))) (d #t) (k 0)))) (h "1q2schq3njfva58xad0hn04sgkdn4ynjma4ik7as2ydbmappr13j")))

(define-public crate-thorium-0.0.6 (c (n "thorium") (v "0.0.6") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput"))) (d #t) (k 0)))) (h "0z5wii69s6vr4wcjjxihx16j1cwvw4z6ancdjc09nnnjl8c1xfid")))

(define-public crate-thorium-0.0.7 (c (n "thorium") (v "0.0.7") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "0v3idwfa360c2d0yn3qh34rn0hhpfls7m4s6alxwvnlac2f1lfic")))

(define-public crate-thorium-0.0.8 (c (n "thorium") (v "0.0.8") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "0x1dv2715jqpzi3xx6crhh236242y0bmrh94bvq6qlawdxrkr28p")))

(define-public crate-thorium-0.0.9 (c (n "thorium") (v "0.0.9") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "0vfvslhizf1rwfvnp9gv0qg46mi9ij8r3xfbhv2wg941x4hvblsm")))

(define-public crate-thorium-0.0.10 (c (n "thorium") (v "0.0.10") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "1b207x1fd3v5m13dn6kvawkgvr0jjxhfjqvr691h83b4n8p8xiv3")))

(define-public crate-thorium-0.0.11 (c (n "thorium") (v "0.0.11") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "1fb0x0sa1i695zm6q3j5b1qjgvabln2d18gh9fc4iz1hii1yyw5p")))

(define-public crate-thorium-0.0.12 (c (n "thorium") (v "0.0.12") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "0l238j5nq23vs7n9lpnfz59101n3lpmwjai8gr0qkdbgpsy04mxy")))

(define-public crate-thorium-0.0.13 (c (n "thorium") (v "0.0.13") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "0rpm926sf8cp1cscy4sibqa4szcmwhf0dqkw3ya9g1l5cdyfp279")))

(define-public crate-thorium-0.0.14 (c (n "thorium") (v "0.0.14") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "0spm7fcrfqhyzgz77agai7bgwx8g20wh0ha136c02r4nsajdrzj4")))

(define-public crate-thorium-0.0.15 (c (n "thorium") (v "0.0.15") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "1lc9b0hhz73h0x1xm434yaznbmrdnkckqwbaqzpg3j1kn233fd7r")))

(define-public crate-thorium-0.0.16 (c (n "thorium") (v "0.0.16") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "1h9qajx5w716qyxdccfq1wix0fzmg4sh8jillikk0zkc19pvp4x4")))

(define-public crate-thorium-0.0.17 (c (n "thorium") (v "0.0.17") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound"))) (d #t) (k 0)))) (h "105q3rm1jpslypsgkx0yn8dgkr9zgcws6cxddg365ykmfbfwlfs2")))

(define-public crate-thorium-0.0.18 (c (n "thorium") (v "0.0.18") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi"))) (d #t) (k 0)))) (h "1wbx5509djc1rd9vyfqmc48qhc2rsz1dn78s5w7mjaq6x6rm4652")))

(define-public crate-thorium-0.0.19 (c (n "thorium") (v "0.0.19") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi"))) (d #t) (k 0)))) (h "1qab5mhfgxx4vdd1kr67m5vd052ki0fykljsbqifm6wwhmwmwg9l")))

(define-public crate-thorium-0.0.20 (c (n "thorium") (v "0.0.20") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "1yna0w6zl23sq3ikzk45grlrw15iaxrl3ki9gng44sqpkphsa5y0")))

(define-public crate-thorium-0.0.21 (c (n "thorium") (v "0.0.21") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "1w9g6q37q9j37gs5j3gb5g7cq21yn8hai1hhrj38pasv26zz3myw")))

(define-public crate-thorium-0.0.22 (c (n "thorium") (v "0.0.22") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "1m51a7spph8amx7hj3fq43ywf2wa9vjarm3ifgj9mmliyh9n6rjm") (f (quote (("hotloading") ("default" "hotloading"))))))

(define-public crate-thorium-0.0.23 (c (n "thorium") (v "0.0.23") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "0yqb19ami6fcgf23g4mb31dif0m3fai27pzvdgl7qjgjxzf8b56m") (f (quote (("hotloading") ("default" "hotloading"))))))

(define-public crate-thorium-0.1.1 (c (n "thorium") (v "0.1.1") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "0976y57hpkm5fr37vgi4vcn97c6wdpg37nxn3wzga1h9sn0sxl0f") (f (quote (("hotloading") ("default" "hotloading"))))))

(define-public crate-thorium-0.1.2 (c (n "thorium") (v "0.1.2") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "0v812hvfd39mhl3c6af8k1g0l07kkxbx38pxcspgizbrcn015wkk") (f (quote (("win32sound_debug") ("hotloading") ("default" "hotloading"))))))

(define-public crate-thorium-0.1.3 (c (n "thorium") (v "0.1.3") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "0xgdv9b8v918yigv96551nhzrd50vdq0k096wwnk3401cxfqrwaz") (f (quote (("win32sound_debug") ("hotloading") ("default" "hotloading"))))))

(define-public crate-thorium-0.1.4 (c (n "thorium") (v "0.1.4") (d (list (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "16w7zfn7y2f3gyyngq1j2k6z760wmgkjkl5bqisp7gpwmb80knqh") (f (quote (("win32sound_debug") ("hotloading") ("default" "hotloading"))))))

(define-public crate-thorium-0.2.0 (c (n "thorium") (v "0.2.0") (d (list (d (n "sdl2-sys") (r "^0.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "1j4gd8022rp3lr2rfblav8khigdw6sy5ixq5g47cgi09fsmfk9v1") (f (quote (("win32sound_debug") ("hotloading") ("default" "hotloading"))))))

(define-public crate-thorium-0.2.1 (c (n "thorium") (v "0.2.1") (d (list (d (n "sdl2-sys") (r "^0.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "1q26cgk4nyaa27am3azf8726gl92gk8w71bvraqmjjdynys9pwjf") (f (quote (("win32sound_debug") ("hotloading") ("default" "hotloading"))))))

(define-public crate-thorium-0.2.2 (c (n "thorium") (v "0.2.2") (d (list (d (n "sdl2-sys") (r "^0.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "15728xg5s5gv4gambd53hd2hpmd8z9p2akapb6cnxrl22g0xs0s0") (f (quote (("win32sound_debug") ("hotloading") ("default" "hotloading"))))))

(define-public crate-thorium-0.2.3 (c (n "thorium") (v "0.2.3") (d (list (d (n "sdl2-sys") (r "^0.32") (d #t) (k 0)) (d (n "winapi") (r "^0.3") (f (quote ("std" "debug" "winuser" "winerror" "errhandlingapi" "libloaderapi" "wingdi" "xinput" "dsound" "timeapi" "profileapi" "synchapi"))) (d #t) (k 0)))) (h "0mxv8436mdj023sff009hwmppbvl87hgmmnglfx8g3w720llc5qq") (f (quote (("win32sound_debug") ("hotloading") ("default" "hotloading"))))))

(define-public crate-thorium-0.2.4 (c (n "thorium") (v "0.2.4") (d (list (d (n "sdl2-sys") (r "^0.32") (d #t) (k 0)))) (h "0a3rayvypv82rysjgzz4yn7z3pli3xf6038xlkwkz00dz2bak5ba") (f (quote (("sdl2_alloc") ("hotloading") ("default" "hotloading" "avoid_std") ("avoid_std" "sdl2_alloc"))))))

(define-public crate-thorium-0.2.5 (c (n "thorium") (v "0.2.5") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.32") (d #t) (k 0)))) (h "1slgqm8h863dmjxxk197rc1gs8yklpgxpd811xwxap25mhyiwiiz") (f (quote (("sdl2_alloc") ("just_a_no_std_lib" "avoid_std") ("hotloading" "dynamic_link") ("dynamic_link") ("default" "hotloading" "avoid_std") ("avoid_std" "sdl2_alloc"))))))

(define-public crate-thorium-0.2.6 (c (n "thorium") (v "0.2.6") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "sdl2-sys") (r "^0.32") (d #t) (k 0)))) (h "04lsx4rngz3iywmj5il7286d2zrvrbj7j90vhiyh4qwhbnafwcma") (f (quote (("sdl2_alloc") ("just_a_no_std_lib" "avoid_std") ("hotloading" "dynamic_link") ("dynamic_link") ("default" "hotloading" "avoid_std") ("avoid_std" "sdl2_alloc"))))))

(define-public crate-thorium-0.3.0 (c (n "thorium") (v "0.3.0") (d (list (d (n "fermium") (r "^0.0.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "1dm90drmf8fs19pqy2l1bfwfwz3d2rg4iv9j9i4zk1mg4qivwk2j") (f (quote (("sdl2_alloc") ("just_a_no_std_lib" "avoid_std") ("hotloading" "dynamic_link") ("dynamic_link") ("default" "hotloading" "avoid_std") ("avoid_std" "sdl2_alloc"))))))

(define-public crate-thorium-0.4.0 (c (n "thorium") (v "0.4.0") (d (list (d (n "beryllium") (r "^0.0.10") (d #t) (k 0)) (d (n "gba-proc-macro") (r "^0.5") (d #t) (k 0)))) (h "1vlj7k9s3lh46v0jyzxpg9rbcgx43qbzhk23d700c77zjyginp4p") (f (quote (("sdl2_alloc") ("just_a_no_std_lib" "avoid_std") ("hotloading" "dynamic_link") ("dynamic_link") ("default" "hotloading" "avoid_std") ("avoid_std" "sdl2_alloc"))))))

