(define-module (crates-io th in things3) #:use-module (crates-io))

(define-public crate-things3-0.0.1 (c (n "things3") (v "0.0.1") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "osascript") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0ycihqq5sb45026b41139yhx0wj5g1v4wrhpls2yd6xs20dyzxq4")))

(define-public crate-things3-0.0.2 (c (n "things3") (v "0.0.2") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "osascript") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "085z5qpxgr5kg41l73kkqasx0dwmc421b2whm6zw8jca1alzr5hc")))

(define-public crate-things3-0.0.3 (c (n "things3") (v "0.0.3") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "osascript") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "0fgssxcgdn1c3zaxclmrqbqsgmsbm8l3jvp3vkgfa9mjidmf8bgi")))

(define-public crate-things3-0.0.4 (c (n "things3") (v "0.0.4") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "osascript") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1ixcvnml10n5yff8p95n3il3wbai27qyq1xajvr4qrw5krhkp1sh")))

(define-public crate-things3-0.0.5 (c (n "things3") (v "0.0.5") (d (list (d (n "anyhow") (r "^1.0.68") (d #t) (k 0)) (d (n "chrono") (r "^0.4.23") (f (quote ("serde"))) (d #t) (k 0)) (d (n "osascript") (r "^0.3.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (f (quote ("derive"))) (d #t) (k 0)))) (h "1hj80qrbdq9zja1a24cmqiiq51ifwyfnc0i14xfa1nd7vd3yd69d")))

