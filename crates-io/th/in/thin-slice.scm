(define-module (crates-io th in thin-slice) #:use-module (crates-io))

(define-public crate-thin-slice-0.1.0 (c (n "thin-slice") (v "0.1.0") (h "1y4f2mxwpj7drm6ygy7pw23riqdxkbbarlsaxjkq27pk5hff9cw5")))

(define-public crate-thin-slice-0.1.1 (c (n "thin-slice") (v "0.1.1") (h "0g4z51g3yarah89ijpakbwhrrknw6d7k3ry0m1zqcn3hbhiq3alf")))

