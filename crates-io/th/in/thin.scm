(define-module (crates-io th in thin) #:use-module (crates-io))

(define-public crate-thin-0.1.0 (c (n "thin") (v "0.1.0") (d (list (d (n "dyn_sized") (r "^0.1") (d #t) (k 0)) (d (n "fn_move") (r "^0.1") (d #t) (k 0)))) (h "0m5hgrvscyvbaz5a2n0ri3fh54b4f6fsj6rz6mb32fz5p3ln4mdk")))

(define-public crate-thin-0.2.0 (c (n "thin") (v "0.2.0") (d (list (d (n "dyn_sized") (r "^0.2") (d #t) (k 0)) (d (n "fn_move") (r "^0.1") (d #t) (k 0)))) (h "1fw41x0b3m6wcbp8z4bcr5bq9c408x401ivailjnd1wl7q239yjm")))

