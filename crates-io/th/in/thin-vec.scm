(define-module (crates-io th in thin-vec) #:use-module (crates-io))

(define-public crate-thin-vec-0.1.0 (c (n "thin-vec") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0i40f18pzsl96di29fkz934nnwg0k32bczvv8xl53a359jwg9zbk") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.0 (c (n "thin-vec") (v "0.2.0") (h "1603jskzczglnfw275xxsvm4ygvwq07lf72f7czvrnw4gihxh54y") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.1 (c (n "thin-vec") (v "0.2.1") (h "1h2g76kkqqgsl8d8k9kmp2qppg2fk11x26z8s336zxd9ljnn1iyw") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.1.1 (c (n "thin-vec") (v "0.1.1") (h "0cb5inr46vdwbvywcvbgpd5pfkf191b469gycx7ssrphry8mqpbd") (f (quote (("unstable") ("gecko-ffi") ("default")))) (y #t)))

(define-public crate-thin-vec-0.1.2 (c (n "thin-vec") (v "0.1.2") (h "10gd3b9w3gdr3v8glhil377zyinvcf2809br4mlpm0kawfnwk4rs") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.2 (c (n "thin-vec") (v "0.2.2") (h "05w4b85l4688vyqjb42kfhhvzr3ayanz1vagwlz8nhfs4sbwccrs") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.3 (c (n "thin-vec") (v "0.2.3") (h "0xfarf2yk3ggfn9zrm7qbzipiphylrwxni8jll0w4qvp7bhqhvxh") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.4 (c (n "thin-vec") (v "0.2.4") (h "1si7lqg7lwbzffsg44rgkd69vsdvg7rl8992hyh2zrilfd6s0y85") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.5 (c (n "thin-vec") (v "0.2.5") (h "1x2xd92ln640sixh7625lqcp4vpxi8vc7s2ly0a7wn91n6s4pfff") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.6 (c (n "thin-vec") (v "0.2.6") (h "0czphs5fm58md8zydm0qd2gxi9hfkjxlqrxpsj2sn35alcpzfhdq") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.7 (c (n "thin-vec") (v "0.2.7") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0cm6cw58ij2yg66h6cwf2r9gssx5nqkwqr7kyvp1hs6ym4g1p76h") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.8 (c (n "thin-vec") (v "0.2.8") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "11zdsx08ls16s3lac7kmp8r8smwbp3lni1r7nbavcvqb32rjqk0h") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.9 (c (n "thin-vec") (v "0.2.9") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "06v7dr9z0pvwl39xgs83ncwx7qhvnk6h2fcg1yb6yf8dfdqmxc6f") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.10 (c (n "thin-vec") (v "0.2.10") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "09lw3zz7qg64wb251lqqbhaqqv25pqcqhpqaqi9il2b71dkync66") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.11 (c (n "thin-vec") (v "0.2.11") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1jnf2wwri93xysl3jqhyjhafq2829d2iyd0dyaid3wlk95qh7pwq") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.12 (c (n "thin-vec") (v "0.2.12") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "1n1dzsraik7x9i0rzj3grdzd9rg62y0incpk1i5qiddysrpipj5a") (f (quote (("unstable") ("gecko-ffi") ("default"))))))

(define-public crate-thin-vec-0.2.13 (c (n "thin-vec") (v "0.2.13") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_test") (r "^1.0") (d #t) (k 2)))) (h "0jsyprh9gjyp2ivnpbfpqmwdcqxfyjj1s9srmfikdhjjh7a91353") (f (quote (("unstable") ("std") ("gecko-ffi") ("default" "std"))))))

