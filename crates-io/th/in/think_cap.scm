(define-module (crates-io th in think_cap) #:use-module (crates-io))

(define-public crate-think_cap-0.0.1 (c (n "think_cap") (v "0.0.1") (d (list (d (n "rand") (r "^0.3.7") (d #t) (k 0)) (d (n "rustc-serialize") (r "^0.3.12") (d #t) (k 0)) (d (n "time") (r "^0.1.24") (d #t) (k 0)))) (h "0557k1cqzvjr9qhl57rzb02vb3yb3sn3frgw7xvaz25141kcll2w")))

