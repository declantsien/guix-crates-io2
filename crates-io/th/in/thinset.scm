(define-module (crates-io th in thinset) #:use-module (crates-io))

(define-public crate-thinset-0.1.0 (c (n "thinset") (v "0.1.0") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)))) (h "0gbyfmj8mm9s2i6b26lmw70cqc2f7n70v0ccwl7sjrib5my1f2c9")))

(define-public crate-thinset-0.2.0 (c (n "thinset") (v "0.2.0") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1zf3yprq01q9z8a1swyidm3qwyp0997j6bw7d5h7r93x2gy0sqyp")))

(define-public crate-thinset-0.3.0 (c (n "thinset") (v "0.3.0") (d (list (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "09nd91m1ww95m4gk0qzm5zig9209hp2a6v1nrapgjykk6gyz9mnb")))

(define-public crate-thinset-0.4.0 (c (n "thinset") (v "0.4.0") (d (list (d (n "bencher") (r "^0.1.5") (d #t) (k 2)) (d (n "bit-set") (r "^0.5.3") (d #t) (k 2)) (d (n "num-traits") (r "^0.2.17") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)))) (h "1v47dpya1q5jkli2prq20rma6gh1zbvx3dpj3gdsalym731xqbq5")))

