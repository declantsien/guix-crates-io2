(define-module (crates-io th in thinnable) #:use-module (crates-io))

(define-public crate-thinnable-0.0.0 (c (n "thinnable") (v "0.0.0") (h "107f2vjzyg79997mibmlmszz3swwfsm4ldn9bmd08k4rjc9dznp0")))

(define-public crate-thinnable-0.1.0 (c (n "thinnable") (v "0.1.0") (h "13ln3ld1062jwb1l12ajak2ri5kyg93wnayakcvnd0ghlr6abaqk") (f (quote (("std"))))))

(define-public crate-thinnable-0.1.1 (c (n "thinnable") (v "0.1.1") (h "034g0wq5y43m5gjal3g9qkgi1jq5dmjf69904vq2ii0cpzwd3cif") (f (quote (("std") ("fn-refs"))))))

(define-public crate-thinnable-0.2.0 (c (n "thinnable") (v "0.2.0") (h "17grkjswb3iq3d36jby15xyvkwzzp8llk23w43c1wahxyv1qzwxd") (f (quote (("std") ("fn-refs"))))))

(define-public crate-thinnable-0.3.0 (c (n "thinnable") (v "0.3.0") (h "1bwqrim25fnpgj90vi40g9yaqv52csryvkyai41s3iam3c7pdj2v") (f (quote (("std") ("fn-refs"))))))

(define-public crate-thinnable-0.3.1 (c (n "thinnable") (v "0.3.1") (h "1n86s9qa1xdxyx562hyxv546gnmcarqw35h29ln6a45d67zlb7dx") (f (quote (("std") ("fn-refs")))) (y #t)))

(define-public crate-thinnable-0.3.2 (c (n "thinnable") (v "0.3.2") (h "0yka1wp4hlz96xlyifdlyv59cvxfzs7mj511rl545kma0iks7l5d") (f (quote (("std") ("fn-refs"))))))

(define-public crate-thinnable-0.3.3 (c (n "thinnable") (v "0.3.3") (h "1ykvv1w0pndc7yv2gnf11a58k092jsgqqrcj4fpf8pmv5qilnjix") (f (quote (("std") ("fn-refs"))))))

