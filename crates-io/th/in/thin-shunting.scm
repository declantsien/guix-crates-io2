(define-module (crates-io th in thin-shunting) #:use-module (crates-io))

(define-public crate-thin-shunting-0.0.7 (c (n "thin-shunting") (v "0.0.7") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "1i5xkfgra2xdh1a7azf95cs7jkllc3c08z879s8b87wmmpsr7nkd")))

(define-public crate-thin-shunting-1.0.0 (c (n "thin-shunting") (v "1.0.0") (d (list (d (n "rand") (r "^0.7.3") (d #t) (k 0)))) (h "0ajwgpr783njmnsn9z7apfajq7dcgkddrzcfndlk8aspqmrrhlwv")))

