(define-module (crates-io th in thincollections) #:use-module (crates-io))

(define-public crate-thincollections-0.5.0 (c (n "thincollections") (v "0.5.0") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "xoshiro") (r "^0.0.4") (d #t) (k 2)))) (h "1y0cbfdggmbypf9bfxhwmc86ap088na2wjqn0w6hsw9kvdviblsy")))

(define-public crate-thincollections-0.5.1 (c (n "thincollections") (v "0.5.1") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "xoshiro") (r "^0.0.4") (d #t) (k 2)))) (h "1cyccbp06i6swhji8dng6j5f9j5xm0r9kms8x5dsv79mhpwl92yb")))

(define-public crate-thincollections-0.5.2 (c (n "thincollections") (v "0.5.2") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "xoshiro") (r "^0.0.4") (d #t) (k 2)))) (h "1ir3rcpipawiq0d6k2p9x74q92fv4dy2dmcbaphsl5p6fjfpv66m")))

(define-public crate-thincollections-0.5.3 (c (n "thincollections") (v "0.5.3") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "xoshiro") (r "^0.0.4") (d #t) (k 2)))) (h "0hgxv8ymdyfdd6jd2w412776j92hspyasrbmf58zbgw0a10ic65h")))

(define-public crate-thincollections-0.5.4 (c (n "thincollections") (v "0.5.4") (d (list (d (n "num") (r "^0.2.0") (d #t) (k 2)) (d (n "ordered-float") (r "^1.0") (d #t) (k 2)) (d (n "rand") (r "^0.5.5") (d #t) (k 2)) (d (n "xoshiro") (r "^0.0.4") (d #t) (k 2)))) (h "09fs33sybsl5wyqaixqxddk7xl4w0fd1lqjrkbcys59n4q4wgfvg")))

