(define-module (crates-io th in thinker) #:use-module (crates-io))

(define-public crate-thinker-0.0.0 (c (n "thinker") (v "0.0.0") (h "1hvgchkxkkbmlllgmkbclpcyr118pz9yr3s8xkr34vvinggqli8z") (y #t)))

(define-public crate-thinker-0.0.1 (c (n "thinker") (v "0.0.1") (d (list (d (n "bufstream") (r "^0.1.1") (d #t) (k 0)) (d (n "byteorder") (r "^0.5.1") (d #t) (k 0)) (d (n "ql2") (r "^1.0.0") (d #t) (k 0)) (d (n "r2d2") (r "^0.7.0") (d #t) (k 0)) (d (n "reql") (r "^0.0") (d #t) (k 0)))) (h "1vh4x5gv5wq2d5mln6av1yb01j557jxnmnb0c163kimxhz0l5qr3") (y #t)))

(define-public crate-thinker-0.0.2 (c (n "thinker") (v "0.0.2") (h "0n6nwp1mbz2l7ds788jylkar6p147c1ygwbr83h5z58ipsmim3b5") (y #t)))

(define-public crate-thinker-0.1.0-beta.1+20230330.b2545c0 (c (n "thinker") (v "0.1.0-beta.1+20230330.b2545c0") (h "1b4xjfiqxxc1dizzl8xbza4zyhw1ccz6c8fvj0wpkzq3kbb6cqls") (y #t)))

(define-public crate-thinker-0.1.0-beta.1+20230401.b2545c0 (c (n "thinker") (v "0.1.0-beta.1+20230401.b2545c0") (h "0dcj6z5f5ysmsvd4iffa5yvk53d0c9r879jzcdsx70ix8yz469qb") (y #t)))

(define-public crate-thinker-0.1.0-beta.1 (c (n "thinker") (v "0.1.0-beta.1") (h "0xb098qsp5v6cjld6iy1kanvs3kr9k9m4v0ay36xi3ci9fcrgjq1") (y #t)))

(define-public crate-thinker-0.1.0-beta.1+20230402.f83f50e (c (n "thinker") (v "0.1.0-beta.1+20230402.f83f50e") (h "1db911ch4mvmqwpswfflicaxzgwy0ad0rv3zhyxf5a67dpxannz6") (y #t)))

(define-public crate-thinker-0.1.0-beta.1+20230402.d7f66e4 (c (n "thinker") (v "0.1.0-beta.1+20230402.d7f66e4") (h "12f94hk0kdwibk5vh9a2x9h41g0gwhm0l66krqs3rk35cn4jg7cv") (y #t)))

(define-public crate-thinker-0.1.0-beta.1+2023040300 (c (n "thinker") (v "0.1.0-beta.1+2023040300") (h "09kxk2bmypzlypm0f06lpph11zzxdsdc4j9nlj7cb4cnrzc1s6jh") (y #t)))

(define-public crate-thinker-0.1.0-beta.1+2023040301 (c (n "thinker") (v "0.1.0-beta.1+2023040301") (h "0zigiv2x3d58c1ydg4hjancxh05bnizkpmdl4qhr3jz1vb8ll5il") (y #t)))

(define-public crate-thinker-0.1.0-beta.1+2023040303 (c (n "thinker") (v "0.1.0-beta.1+2023040303") (h "1j5x2pz27qpvmhc564g0m4pq8ab2935flxpgpm7mzkyi8qlgfnnd") (y #t)))

(define-public crate-thinker-0.1.0-beta.5+20230402 (c (n "thinker") (v "0.1.0-beta.5+20230402") (h "1zdnvvfqrm2qg7sz77wmhgkyjdz66p6dpwp4dh8jf46dl4qfylrr") (y #t)))

(define-public crate-thinker-0.1.0-beta.5+2023040201 (c (n "thinker") (v "0.1.0-beta.5+2023040201") (h "076y8dplp95a991hyi7732zfz2azw7b8w0j46shngi72akqxyicv") (y #t)))

(define-public crate-thinker-0.1.0-beta.5+20230403 (c (n "thinker") (v "0.1.0-beta.5+20230403") (h "0n2xgy28lh0v42y5sykbzp0mf63fv0w90a42jf5vdkf4m20ah5pv") (y #t)))

(define-public crate-thinker-0.2.0-beta.1+2023041101 (c (n "thinker") (v "0.2.0-beta.1+2023041101") (h "10y9f3cmyz0gccazgj9yn323z35xfq2jfvglwjh159iw9p7akzfh") (y #t)))

(define-public crate-thinker-0.2.0-beta.1+2023041102 (c (n "thinker") (v "0.2.0-beta.1+2023041102") (h "1wmy5zp7g70m0mqnsan00rw9yxqyllrr77bi15s5kp0fjra9pz21") (y #t)))

(define-public crate-thinker-0.3.0-beta.1 (c (n "thinker") (v "0.3.0-beta.1") (h "02x1iw4md6xnjhqbn77lms5rh42c3xp63355yyqq0wchjpx3srdw") (y #t)))

(define-public crate-thinker-0.3.0-beta.1+2023041101 (c (n "thinker") (v "0.3.0-beta.1+2023041101") (h "1c0yym0l9amqvbirxj28p95m70fbiiqgdibb5aangcqb5ll63rg4") (y #t)))

(define-public crate-thinker-1.0.0 (c (n "thinker") (v "1.0.0") (h "1k7a7r843rkykyn53ng1fb91azvzm9r36asls4c9cr0q7f7l5idl") (y #t)))

(define-public crate-thinker-1.0.1 (c (n "thinker") (v "1.0.1") (h "1r9j7wldi2dgqc67q8vwalpb8fafqibd1xrlh9f6ry38apbzdk9f") (y #t)))

(define-public crate-thinker-1.0.2-beta (c (n "thinker") (v "1.0.2-beta") (h "0pr4x5xjzdg4sw6gavnbb2iqp4s61swawh97i0c2d8vgbgf2jzz6") (y #t)))

(define-public crate-thinker-1.0.2-beta.1 (c (n "thinker") (v "1.0.2-beta.1") (h "0smqwl2j3ykcwv75qqf6bpq72zghc9lm10agaxqypk87fwl5f6xs") (y #t)))

