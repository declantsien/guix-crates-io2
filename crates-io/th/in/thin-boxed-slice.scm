(define-module (crates-io th in thin-boxed-slice) #:use-module (crates-io))

(define-public crate-thin-boxed-slice-0.1.1 (c (n "thin-boxed-slice") (v "0.1.1") (d (list (d (n "allocator-api2") (r "^0.2.18") (d #t) (k 0)))) (h "1iwzfb9c0wncl08xwlhkd4jvzpwr7rxn8n8szmsn7lh1kp8idrym")))

(define-public crate-thin-boxed-slice-0.2.0 (c (n "thin-boxed-slice") (v "0.2.0") (d (list (d (n "allocator-api2") (r "^0.2.18") (d #t) (k 0)))) (h "1njr2j6aixy4vw5j65ayrhf6b8pks6vms0vm28bahvfzwzbys8kr")))

(define-public crate-thin-boxed-slice-0.2.1 (c (n "thin-boxed-slice") (v "0.2.1") (d (list (d (n "allocator-api2") (r "^0.2.18") (d #t) (k 0)))) (h "0k1ascl5hrdcraj8jjn9xrraxvxy7al61bk314389aa9m727lb46")))

(define-public crate-thin-boxed-slice-0.2.2 (c (n "thin-boxed-slice") (v "0.2.2") (d (list (d (n "allocator-api2") (r "^0.2.18") (d #t) (k 0)))) (h "1q9q6s2jp1a74i0if7p9jdlc5jxfblv7455n6vcyahqjrvqhircq")))

(define-public crate-thin-boxed-slice-0.2.3 (c (n "thin-boxed-slice") (v "0.2.3") (d (list (d (n "allocator-api2") (r "^0.2.18") (d #t) (k 0)))) (h "1ik6sibf2ah43hla9s1kf4zfwqbfpl74br66b6n1m79v4jm50xlc")))

(define-public crate-thin-boxed-slice-0.2.4 (c (n "thin-boxed-slice") (v "0.2.4") (d (list (d (n "allocator-api2") (r "^0.2.18") (d #t) (k 0)))) (h "0kj4lmpnblgr7jxqdd7xiqczr1kp1zb5lnpp6zcpdzzbvg4yy3hr")))

(define-public crate-thin-boxed-slice-0.2.5 (c (n "thin-boxed-slice") (v "0.2.5") (d (list (d (n "allocator-api2") (r "^0.2.18") (d #t) (k 0)))) (h "0470nzvz01gbiiqxcqa5f68pmizaaqb62x6m16rrar6y9d7pkqqc")))

