(define-module (crates-io th in thingy-91-nrf9160-bsp) #:use-module (crates-io))

(define-public crate-thingy-91-nrf9160-bsp-0.1.0 (c (n "thingy-91-nrf9160-bsp") (v "0.1.0") (d (list (d (n "cortex-m") (r "^0.7") (d #t) (k 2)) (d (n "cortex-m-rt") (r "^0.6") (d #t) (k 2)) (d (n "nb") (r "^1.0.0") (d #t) (k 2)) (d (n "nrf9160-hal") (r "^0.12.2") (d #t) (k 0)))) (h "1py5gfqca9dz7k7xr6443i1pvw5wqyk0hb9h442s42ky3pfg9zx9")))

