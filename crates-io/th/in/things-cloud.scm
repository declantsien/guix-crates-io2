(define-module (crates-io th in things-cloud) #:use-module (crates-io))

(define-public crate-things-cloud-0.1.0 (c (n "things-cloud") (v "0.1.0") (d (list (d (n "async-recursion") (r "^1.0.5") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.23") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.111") (d #t) (k 0)) (d (n "serde_path_to_error") (r "^0.1.15") (d #t) (k 0)) (d (n "serde_repr") (r "^0.1.18") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.56") (d #t) (k 0)) (d (n "tokio") (r "^1.35.1") (f (quote ("rt-multi-thread" "rt" "macros"))) (d #t) (k 2)))) (h "1bmq545092rd655mcwdfdwf0l1v48l601gqqvllrcqyqv9wn1cg5")))

