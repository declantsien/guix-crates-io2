(define-module (crates-io th in thin-dst) #:use-module (crates-io))

(define-public crate-thin-dst-1.0.0-beta.1 (c (n "thin-dst") (v "1.0.0-beta.1") (h "06r2vznpmc2ikb2bffdipgbg7vbp4fzcr8ds35d18gms4a0csjc8")))

(define-public crate-thin-dst-1.0.0-beta.2 (c (n "thin-dst") (v "1.0.0-beta.2") (h "1zdj5332kry9pqxlz62373bnj17s24q7f6xraxw9wxm7xa0idxap")))

(define-public crate-thin-dst-1.0.0-beta.3 (c (n "thin-dst") (v "1.0.0-beta.3") (h "0r5m5y2hlxw751s6shbrl4akagax9fjz50ncdqm5axr3zdlwn865")))

(define-public crate-thin-dst-1.0.0-beta.4 (c (n "thin-dst") (v "1.0.0-beta.4") (h "1flc6srbhpjn2xnqsa485fim3vghzr57g9imsvla89wr0w6bkv51")))

(define-public crate-thin-dst-1.0.0 (c (n "thin-dst") (v "1.0.0") (h "1qcnmbc4p5kzlpy12ip0yg97a6398n9ab09kv1kc84s9ks5djby5")))

(define-public crate-thin-dst-1.1.0 (c (n "thin-dst") (v "1.1.0") (h "03jgc3mlsqifpc6zxcv5d7pn2vrr6l9bq9xv7rkzj6hg32z4cg6v")))

