(define-module (crates-io th in thingpark-client-rs) #:use-module (crates-io))

(define-public crate-thingpark-client-rs-0.1.0 (c (n "thingpark-client-rs") (v "0.1.0") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "06pb9d93wdnvxggmh6vkqbzlyjm55m8ypddaaylaq22sx5fs779f")))

(define-public crate-thingpark-client-rs-0.1.1 (c (n "thingpark-client-rs") (v "0.1.1") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "1zcyabbm2j14wnghd4509y2mavs06l5bj0c7i5wz9d03gglb3szf")))

(define-public crate-thingpark-client-rs-0.1.2 (c (n "thingpark-client-rs") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11.3") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.116") (f (quote ("derive"))) (d #t) (k 0)))) (h "00pax15a68jj38ykic5qd8i1njqzx126d2yxw0jmy05ljw1g6aqi")))

