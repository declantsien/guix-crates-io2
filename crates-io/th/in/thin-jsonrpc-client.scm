(define-module (crates-io th in thin-jsonrpc-client) #:use-module (crates-io))

(define-public crate-thin-jsonrpc-client-0.0.1 (c (n "thin-jsonrpc-client") (v "0.0.1") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("from" "display"))) (k 0)) (d (n "futures") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (f (quote ("raw_value"))) (d #t) (k 0)))) (h "17cyrbb01rf20y40qalvcysr655yj9wwm7qhadi6vg09qr9zb01s")))

(define-public crate-thin-jsonrpc-client-0.1.0 (c (n "thin-jsonrpc-client") (v "0.1.0") (d (list (d (n "derive_more") (r "^0.99.17") (f (quote ("from" "display"))) (k 0)) (d (n "futures-core") (r "^0.3.28") (d #t) (k 0)) (d (n "futures-util") (r "^0.3.28") (d #t) (k 0)) (d (n "serde") (r "^1.0.175") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.103") (f (quote ("raw_value"))) (d #t) (k 0)) (d (n "tokio") (r "^1.29.1") (f (quote ("full"))) (d #t) (k 2)))) (h "1f5jlr08q2hmvyx9sznx7z64qqrld0xd8sl427w3y9xp6jmg3pw5")))

