(define-module (crates-io th in thin-string) #:use-module (crates-io))

(define-public crate-thin-string-0.1.0 (c (n "thin-string") (v "0.1.0") (d (list (d (n "thin-vec") (r "^0.2.3") (d #t) (k 0)))) (h "14jja23l2yn060hmg0sf3ckj81xjnhh7xv8prq4g6glwkyzrkkca")))

(define-public crate-thin-string-0.1.1 (c (n "thin-string") (v "0.1.1") (d (list (d (n "thin-vec") (r "^0.2.3") (d #t) (k 0)))) (h "0bkm8fdlwmld45p9h5dlssirsxn1ndhb4bqmkm8wm4wghdgcl4qs")))

(define-public crate-thin-string-0.1.2 (c (n "thin-string") (v "0.1.2") (d (list (d (n "thin-vec") (r "^0.2.3") (d #t) (k 0)))) (h "1pqaprza4gxbhj3j51sl4ahkdac0m823jzg29xmmp87y8db3rabh")))

(define-public crate-thin-string-0.1.3 (c (n "thin-string") (v "0.1.3") (d (list (d (n "thin-vec") (r "^0.2.3") (d #t) (k 0)))) (h "1cqsfbii9dr91ixyhwfxmxwcdckb2ni28qmrv9n00a8d8glfqnvp")))

(define-public crate-thin-string-0.2.0 (c (n "thin-string") (v "0.2.0") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.12") (d #t) (k 0)))) (h "1vhlm839wnxkrq9f5nnivmbbschb7nn2gmigv3g55wr5vrsiiagp")))

(define-public crate-thin-string-0.2.1 (c (n "thin-string") (v "0.2.1") (d (list (d (n "serde") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "thin-vec") (r "^0.2.12") (d #t) (k 0)))) (h "06s86xa36kkwmx8qni63ikdw0ra9i1ighil3yfb62m4y7gg11dkc")))

