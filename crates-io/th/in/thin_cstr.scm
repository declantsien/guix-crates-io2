(define-module (crates-io th in thin_cstr) #:use-module (crates-io))

(define-public crate-thin_cstr-0.1.0 (c (n "thin_cstr") (v "0.1.0") (h "1q909hkin50yx3hfc7wjz8lwi069zds6qzjs4vapy8szpzizd0s1")))

(define-public crate-thin_cstr-0.1.1 (c (n "thin_cstr") (v "0.1.1") (h "0yxhlkg8f0ify0bbwqfgbswgvxhqkc90a6afhn6y62ydnh8k87k1")))

