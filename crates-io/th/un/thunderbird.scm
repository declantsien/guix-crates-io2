(define-module (crates-io th un thunderbird) #:use-module (crates-io))

(define-public crate-thunderbird-0.1.0 (c (n "thunderbird") (v "0.1.0") (h "0s9zbp1id665yyppx71glflw5x0gmvw785gjy3lf7srh73dfznix")))

(define-public crate-thunderbird-0.2.0 (c (n "thunderbird") (v "0.2.0") (h "03gsg4cg4zcbc0aglv67gzyfl0llq0yciikrwn1zwg68p7l0hl0p")))

(define-public crate-thunderbird-0.2.1 (c (n "thunderbird") (v "0.2.1") (h "0038jvy9mcp42zc2x30s01lm43z92m00fycm8zbzk1n4ndamlhaz")))

(define-public crate-thunderbird-0.2.2 (c (n "thunderbird") (v "0.2.2") (h "1k93vm8vl1hvngchypp2g6ds4alax64zsl632ijdcy504r1i53s9")))

(define-public crate-thunderbird-0.2.3 (c (n "thunderbird") (v "0.2.3") (h "1q360ccqli366cjhc0jnczzdvda1i5wdqyxlgfd9vf6jq6nnknym")))

(define-public crate-thunderbird-0.2.4 (c (n "thunderbird") (v "0.2.4") (h "0gg886g4d88qvfvj71sfh10w7azdckbx888fbi9zlwi2v1x3jml8")))

