(define-module (crates-io th un thunderdome) #:use-module (crates-io))

(define-public crate-thunderdome-0.1.0 (c (n "thunderdome") (v "0.1.0") (h "1lgsp76233kff2lxw0w4mgv70zl0jrz8x25z86v03v6m783ws2vf")))

(define-public crate-thunderdome-0.1.1 (c (n "thunderdome") (v "0.1.1") (h "0px683jsdbgazgkxhyzp07nhhj8pvpy97fz2iw4ydsyhpkp6kbqc")))

(define-public crate-thunderdome-0.2.0 (c (n "thunderdome") (v "0.2.0") (h "0jj5gw3qrbaqh1f974ap3isk9r1bn53qcqjxjsqcddgfd0w08skq")))

(define-public crate-thunderdome-0.2.1 (c (n "thunderdome") (v "0.2.1") (h "08vd0pn041xwf15l55ymaqfxh88kwyvm30fhiszbf0cxxwzkw712")))

(define-public crate-thunderdome-0.3.0 (c (n "thunderdome") (v "0.3.0") (h "1bmcn0fm6pc3zawh74v148baxf85x34g8dkf5z2h3m48srdl2wkm")))

(define-public crate-thunderdome-0.4.0 (c (n "thunderdome") (v "0.4.0") (h "1wd5pbx4jww66yidkksk0lk5pbvrfyd7bpr1g7cncjwdkn6fb8yf")))

(define-public crate-thunderdome-0.4.1 (c (n "thunderdome") (v "0.4.1") (h "1q3khv82pp99a3imfjk95rsm7n53rbcl2883l0jcwgn989vr9d47")))

(define-public crate-thunderdome-0.4.2 (c (n "thunderdome") (v "0.4.2") (h "0hm6pgqh7q0biqhmmpwz6zr1ih41br2i4d1nygkbvl1c2x7n51gn")))

(define-public crate-thunderdome-0.5.0 (c (n "thunderdome") (v "0.5.0") (h "0sy5jp3wxwgxdpg09m8k8sm32wqnvf84lmr6k1nw98b6m9ws3acn")))

(define-public crate-thunderdome-0.5.1 (c (n "thunderdome") (v "0.5.1") (h "1x9i9h4jjh12cr0irb2rmfxsfnnv7bbvprp41i9mpdr3gphryswd")))

(define-public crate-thunderdome-0.6.0 (c (n "thunderdome") (v "0.6.0") (h "08anibamzhsjbb4xzlcir1vrylrsgi8amd4sw1fhyi5v5rda48k6") (f (quote (("std") ("default" "std"))))))

(define-public crate-thunderdome-0.6.1 (c (n "thunderdome") (v "0.6.1") (h "02wdv7r38zl5sz1nppffak8b3b97pxmi2c9wzvk9mgv06gwp1qcj") (f (quote (("std") ("default" "std"))))))

