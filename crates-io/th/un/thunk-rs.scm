(define-module (crates-io th un thunk-rs) #:use-module (crates-io))

(define-public crate-thunk-rs-0.1.0 (c (n "thunk-rs") (v "0.1.0") (h "08dxdgzd7j6z0r8yai0vh9zwz6q8f2h72ji97vhshgs3i80c71p7")))

(define-public crate-thunk-rs-0.3.0 (c (n "thunk-rs") (v "0.3.0") (h "0zhbnr9f7vp45j5abhfqs050n1hflfllg78fchs6d0wpgryn12ir") (f (quote (("windows_xp") ("windows_vista") ("vc_ltl_only") ("subsystem_windows") ("lib") ("default" "windows_xp"))))))

(define-public crate-thunk-rs-0.3.1 (c (n "thunk-rs") (v "0.3.1") (h "07gpkmcwykf4kgdnyzvxxj5b76s5fb6drl5dd8sifcxdlx6p4xxz") (f (quote (("windows_xp") ("windows_vista") ("vc_ltl_only") ("subsystem_windows") ("lib") ("default" "windows_xp"))))))

