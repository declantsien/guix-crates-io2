(define-module (crates-io th un thunder) #:use-module (crates-io))

(define-public crate-thunder-0.1.0 (c (n "thunder") (v "0.1.0") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0dswyx5jdk6xn4swd9zs9falx96dxakmyq1zri2wlwdxq0q6ivv7")))

(define-public crate-thunder-0.1.1 (c (n "thunder") (v "0.1.1") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0pg13yrhk7g0f6fpdyng33yhpbhrdnnsvn8gmyq6ncg551wwfl90")))

(define-public crate-thunder-0.1.2 (c (n "thunder") (v "0.1.2") (d (list (d (n "clap") (r "2.*") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1xbal62n27vv4438yrl7c9iyhbgw88p6zr6zxj00pgkxhmfav2z0")))

(define-public crate-thunder-0.2.0 (c (n "thunder") (v "0.2.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (d #t) (k 0)))) (h "1ysr0b6azbp06pzxlhj7ix8vhwkmjrpsm4xpjl06gk5srb9cvd7z")))

(define-public crate-thunder-0.3.0 (c (n "thunder") (v "0.3.0") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (d #t) (k 0)))) (h "0d1rd189c5jcvaaq13p6dc4rsi3ssixr777j0rqy8as8q8ycxw6g")))

(define-public crate-thunder-0.3.1 (c (n "thunder") (v "0.3.1") (d (list (d (n "clap") (r "^2.0") (d #t) (k 0)) (d (n "quote") (r "^0.4") (d #t) (k 0)) (d (n "syn") (r "^0.12") (f (quote ("extra-traits" "full" "fold" "visit" "visit-mut"))) (d #t) (k 0)))) (h "18j3pil2sa5798l1cgxggq0r48niladqd2ljjb7bq74nnxqwcyi7")))

