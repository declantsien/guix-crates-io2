(define-module (crates-io th un thunky) #:use-module (crates-io))

(define-public crate-thunky-0.8.0 (c (n "thunky") (v "0.8.0") (d (list (d (n "tokio") (r "^0.1.21") (d #t) (k 0)))) (h "1mqkpprmfr7j717paikyg098r3lacv9gnlidp2q778wvcsbj7yd1") (y #t)))

(define-public crate-thunky-0.9.0 (c (n "thunky") (v "0.9.0") (d (list (d (n "tokio") (r "^0.1.21") (d #t) (k 0)))) (h "13xx5fawywpw1nnw0wm8rx1qb7wyi7ym02lmxxblp6ygwzg9shl4")))

