(define-module (crates-io th un thunks) #:use-module (crates-io))

(define-public crate-thunks-0.0.1 (c (n "thunks") (v "0.0.1") (h "04rw0phkm3653bkwbbxkks57ydylpnh0snh4c7yz9l8n0l8pxz2w")))

(define-public crate-thunks-0.0.2 (c (n "thunks") (v "0.0.2") (h "0jza15pw4zfzg322fk9gvjc1b782ixx1imfwyi3f0k3qxkbn7wz3")))

(define-public crate-thunks-0.0.3 (c (n "thunks") (v "0.0.3") (h "1znmbld5zay2ri4iifaxqzlvnp72hnid8xr7427qd0gk9chi4xc3")))

(define-public crate-thunks-0.1.0 (c (n "thunks") (v "0.1.0") (h "0h5x5qrvmb3h3sxxcymmwkmhvab3qmhwq3s7wzmrp6cz5mfh2drz")))

