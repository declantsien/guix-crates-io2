(define-module (crates-io th un thunk) #:use-module (crates-io))

(define-public crate-thunk-0.1.0 (c (n "thunk") (v "0.1.0") (d (list (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "1xrrnvznn2jwcimynf5vibxk154zkarrxhs0sdrqy4va4q4lalbi")))

(define-public crate-thunk-0.1.1 (c (n "thunk") (v "0.1.1") (d (list (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "1ivbh8xqpls5c05cab1z31nabnpngdl40lgj9iqf6v69p3dig9xj")))

(define-public crate-thunk-0.1.2 (c (n "thunk") (v "0.1.2") (d (list (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "0ck5877kfx5wgdrlc5fx34lkh88x8abc5i1m57fyw5qnvn4zs9dy")))

(define-public crate-thunk-0.2.0 (c (n "thunk") (v "0.2.0") (d (list (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "1xpxzkgx84dl330fc2jfg8zdsgfbpif6726v6qmg7kdq3anh2qa1")))

(define-public crate-thunk-0.2.1 (c (n "thunk") (v "0.2.1") (d (list (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "03rbv9qrkyggslx6zjn27y4cmy5102nfbqbrr3ya27i29kpsk48j")))

(define-public crate-thunk-0.3.0 (c (n "thunk") (v "0.3.0") (d (list (d (n "unreachable") (r "^1.0.0") (d #t) (k 0)))) (h "100gk1p7mc2i915l0habzlcbl69qd85nn3aw37lbddv6lyybznfq")))

