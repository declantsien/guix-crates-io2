(define-module (crates-io th un thunderborg_driver) #:use-module (crates-io))

(define-public crate-thunderborg_driver-0.1.0 (c (n "thunderborg_driver") (v "0.1.0") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "17skf4k35zp9qlw7vk77nkcnmfh534xfk1av7800b0v91zgcfwrb")))

(define-public crate-thunderborg_driver-0.2.0 (c (n "thunderborg_driver") (v "0.2.0") (d (list (d (n "rppal") (r "^0.11.3") (d #t) (k 0)))) (h "08f7ksg3gidhkb419xan73mvs4nci9igmqkxxh89pl15n5rl8kan")))

