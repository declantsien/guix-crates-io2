(define-module (crates-io th un thunk_simple) #:use-module (crates-io))

(define-public crate-thunk_simple-0.1.0 (c (n "thunk_simple") (v "0.1.0") (h "1v24xad3xarj18qrd2q9nwnyr9afsp4hkdgxywgmj4c4096y0gj7")))

(define-public crate-thunk_simple-0.1.1 (c (n "thunk_simple") (v "0.1.1") (h "0slvncswhwfip9d02nn1h6jw8hark7l4n3lic77n5krbkw5rhq26")))

