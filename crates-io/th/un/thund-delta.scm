(define-module (crates-io th un thund-delta) #:use-module (crates-io))

(define-public crate-thund-delta-0.0.1 (c (n "thund-delta") (v "0.0.1") (d (list (d (n "arrow2") (r "^0.14.2") (d #t) (k 0)) (d (n "arrowalloy") (r "^0.0.3") (d #t) (k 0)) (d (n "chrono") (r "^0.4.22") (d #t) (k 0)))) (h "0shsybc2gpprjgliny47pnmgprakwr92vr3z763cl4744acsk3qg")))

