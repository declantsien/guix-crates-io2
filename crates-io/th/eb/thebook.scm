(define-module (crates-io th eb thebook) #:use-module (crates-io))

(define-public crate-thebook-0.1.1 (c (n "thebook") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "git2") (r "^0.15.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.2") (d #t) (k 0)))) (h "18hi7ffshg83r4k3awlpv8jb0h979psbbg6ll15x0hyc2qz3f21h")))

(define-public crate-thebook-0.1.2 (c (n "thebook") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.2") (d #t) (k 0)))) (h "0w4z6pszxw53gnsy9qpscyrc3sn3wsnvjbfds63s5wavfmkryp4w")))

(define-public crate-thebook-0.1.3 (c (n "thebook") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.2") (d #t) (k 0)))) (h "00fccvxksbj2n5lm29ydghig7jjca835kgh5zp54v7zmjd8cg5hg")))

(define-public crate-thebook-0.1.4 (c (n "thebook") (v "0.1.4") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.2") (d #t) (k 0)))) (h "02d75gqa90gv28l1lxm4wpi6fmv7149lqc87xlm8ammk0glfbx21")))

(define-public crate-thebook-0.1.5 (c (n "thebook") (v "0.1.5") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.2") (d #t) (k 0)))) (h "1wv7lmg9yrfpcfx892nz6p72hlwf6wan43vzi4mwr5jx3vh3345i")))

(define-public crate-thebook-0.1.6 (c (n "thebook") (v "0.1.6") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.2") (d #t) (k 0)))) (h "03gjcwiash4m17kjfwvvz8yl2c7akq2awclw69sldrhcb7c3f6d7")))

(define-public crate-thebook-0.2.1 (c (n "thebook") (v "0.2.1") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.2") (d #t) (k 0)))) (h "12dihqbcjc73lxh4lrfamjdlq7qmar6gn0hgczi0r21p2qq6gdxf")))

(define-public crate-thebook-0.2.2 (c (n "thebook") (v "0.2.2") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.3") (d #t) (k 0)))) (h "1x1vk3vw83rwzh23i9a9ih44rnly7gml9krhcl5n62j346mygpjy")))

(define-public crate-thebook-0.2.3 (c (n "thebook") (v "0.2.3") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.3") (d #t) (k 0)))) (h "1zjil542b70sxfvxl71zqay7abrihi8asndrnx43j6m30p9gx6ml")))

(define-public crate-thebook-0.3.0 (c (n "thebook") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.23.1") (d #t) (k 0)) (d (n "dirs") (r "^4.0.0") (d #t) (k 0)) (d (n "termimad") (r "^0.20.3") (d #t) (k 0)) (d (n "walkdir") (r "^2.3.2") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.3") (d #t) (k 0)))) (h "123klb1badl0j9amx8mnpzhnb6r3iddpic4dk79b3cdbp97zjnam")))

