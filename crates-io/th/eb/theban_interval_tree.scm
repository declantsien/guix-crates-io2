(define-module (crates-io th eb theban_interval_tree) #:use-module (crates-io))

(define-public crate-theban_interval_tree-0.6.2 (c (n "theban_interval_tree") (v "0.6.2") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "time") (r "0.1.*") (d #t) (k 0)))) (h "1wc9kgv41zzdhprvmzavw6nad9bzyimax32lm5j3lq5iw6a2xm9z")))

(define-public crate-theban_interval_tree-0.7.0 (c (n "theban_interval_tree") (v "0.7.0") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "time") (r "0.1.*") (d #t) (k 0)))) (h "0m6zf5p2l6kl54mycl7rxnlxn8hg52rfh7fhljvflg3r4a2hjlwg")))

(define-public crate-theban_interval_tree-0.7.1 (c (n "theban_interval_tree") (v "0.7.1") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "rand") (r "0.3.*") (d #t) (k 0)) (d (n "time") (r "0.1.*") (d #t) (k 0)))) (h "1sp4vxdy7y4vlb98qz9jj8fcpjcasp8xq7h950b6b6nvhm9jmd57")))

