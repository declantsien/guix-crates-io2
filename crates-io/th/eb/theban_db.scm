(define-module (crates-io th eb theban_db) #:use-module (crates-io))

(define-public crate-theban_db-0.4.0 (c (n "theban_db") (v "0.4.0") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "rmp") (r "^0.7.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "0.6.*") (d #t) (k 0)))) (h "0pmjzn7kpii6k1lhjjjwqx9hckyhzl9x43qb17z58qzp3j5ssvv3")))

(define-public crate-theban_db-0.4.1 (c (n "theban_db") (v "0.4.1") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "rmp") (r "^0.7.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "0.6.*") (d #t) (k 0)))) (h "1kd81sdx8cz4bi3kj1bj8m65ijkgpyhv183dki9nhyyz5nvp62rw")))

(define-public crate-theban_db-0.4.2 (c (n "theban_db") (v "0.4.2") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "rmp") (r "^0.7.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "0.6.*") (d #t) (k 0)))) (h "1gprw1nnpaz2rjpfbmx50b152pzpzmrwdkczmh651rksp4435gig")))

(define-public crate-theban_db-0.5.0 (c (n "theban_db") (v "0.5.0") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "rmp") (r "^0.7.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "0.6.*") (d #t) (k 0)))) (h "0hfaccphc47bjp1719y7az924md8ish2win53lzc2rmgy1qlp2s4")))

(define-public crate-theban_db-0.5.1 (c (n "theban_db") (v "0.5.1") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "rmp") (r "^0.7.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "0.6.*") (d #t) (k 0)))) (h "1rhbskqzpzvr6j0ppws4y3vycgjbdbk3ih8w91zkh8r0hm73ffbp")))

(define-public crate-theban_db-0.5.2 (c (n "theban_db") (v "0.5.2") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "rmp") (r "^0.7.3") (d #t) (k 0)) (d (n "theban_interval_tree") (r "0.7.*") (d #t) (k 0)))) (h "1sjcbywqyb2qi4415p49ipqhyjcn845syyn2izbjw75dc6jjdf7g")))

(define-public crate-theban_db-0.6.0 (c (n "theban_db") (v "0.6.0") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rmp") (r "^0.7.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "theban_interval_tree") (r "0.7.*") (d #t) (k 0)))) (h "1dx9p30s7hbk5r3vnsba7hx012sv8h3qk7mkg7dvbr7wla5j2b52")))

(define-public crate-theban_db-0.7.0 (c (n "theban_db") (v "0.7.0") (d (list (d (n "memrange") (r "0.1.*") (d #t) (k 0)) (d (n "quick-error") (r "^1.0.0") (d #t) (k 0)) (d (n "rmp") (r "^0.7.3") (d #t) (k 0)) (d (n "rustc-serialize") (r "0.3.*") (d #t) (k 0)) (d (n "theban_interval_tree") (r "0.7.*") (d #t) (k 0)))) (h "1d8qvymb5qk40pq64nynxk862a28xk17d9hpldvcqq3kspy9b6kd")))

