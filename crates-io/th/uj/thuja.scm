(define-module (crates-io th uj thuja) #:use-module (crates-io))

(define-public crate-thuja-0.1.0 (c (n "thuja") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.20") (d #t) (k 0)) (d (n "tui") (r "^0.16.0") (f (quote ("crossterm"))) (k 0)))) (h "15rdvcx7ybfxg5bpmjfmgqv16c4r0hkzbkdgli2g5w412arsh2bv")))

(define-public crate-thuja-0.2.0 (c (n "thuja") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.22.1") (d #t) (k 0)) (d (n "tui") (r "^0.17.0") (d #t) (k 0)))) (h "1zlrxw64p109rhh00kmq09fk0pvbg4iigzixn40rhzr2j4w11ccq")))

(define-public crate-thuja-0.2.1 (c (n "thuja") (v "0.2.1") (d (list (d (n "crossterm") (r "^0") (d #t) (k 0)) (d (n "tui") (r "^0.18.0") (d #t) (k 0)))) (h "0lq6yhxq29c0nsrv8izmv7cm2r5qw0zz1gx2c0r0grf7kvcilqg1")))

