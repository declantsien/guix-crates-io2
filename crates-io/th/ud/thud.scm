(define-module (crates-io th ud thud) #:use-module (crates-io))

(define-public crate-thud-0.1.0 (c (n "thud") (v "0.1.0") (d (list (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "1hcfqwarxrc889w2wj5cplksxs37nxg6k0d7jw5mipnbwv1vdc8r")))

(define-public crate-thud-0.1.1 (c (n "thud") (v "0.1.1") (d (list (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "1g4snmi3sgb5g44xd9xjfz5a23ysj5p78yng8ajjcfffkyirynis")))

(define-public crate-thud-0.1.2 (c (n "thud") (v "0.1.2") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 0)))) (h "0k3z086ll9mn5ybqlbd6fch4p59kdvvhsghy0wgby09kgrx4r0xv") (f (quote (("serialize" "serde"))))))

(define-public crate-thud-0.1.3 (c (n "thud") (v "0.1.3") (d (list (d (n "libc") (r "^0.2.68") (o #t) (d #t) (k 0)) (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "1nsx7gr0hcgim2gj9qv0s40jqgmpn20nhf51ykjiybvjljpljw3b") (f (quote (("serialize" "serde") ("ffi" "libc"))))))

(define-public crate-thud-0.1.4 (c (n "thud") (v "0.1.4") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "1xj8rj9vl02mfkrxw37ijbcb8jdzkp042raa7rm33v6w0rjxxnf2") (f (quote (("serialize" "serde"))))))

(define-public crate-thud-0.1.5 (c (n "thud") (v "0.1.5") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)))) (h "0za4d320hpywkkcr5ls1hdk73mna9xhlyr7ai7x5gycykx137n86") (f (quote (("serialize" "serde"))))))

(define-public crate-thud-0.1.6 (c (n "thud") (v "0.1.6") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "test-case") (r "^1.0.0") (d #t) (k 2)) (d (n "thiserror") (r "^1.0.14") (d #t) (k 0)))) (h "0frpwpy3731jpw0cnih1dkv5dfjv2qvcsrzdvv7cbfdldd98bgii") (f (quote (("serialize" "serde"))))))

