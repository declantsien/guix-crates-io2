(define-module (crates-io th gh thghosting-data-centers) #:use-module (crates-io))

(define-public crate-thghosting-data-centers-0.1.0 (c (n "thghosting-data-centers") (v "0.1.0") (d (list (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "scraper") (r "^0.12") (k 0)) (d (n "thiserror") (r "^1.0") (k 0)))) (h "1kgidkdgh3y878b4hkk48rlxz0ilf233fpxyqfqplmcji9nrnmwl")))

(define-public crate-thghosting-data-centers-0.1.1 (c (n "thghosting-data-centers") (v "0.1.1") (d (list (d (n "http-api-client-endpoint") (r "^0.2") (k 0)) (d (n "scraper") (r "^0.13") (k 0)) (d (n "thiserror") (r "^1") (k 0)))) (h "0n5gjchrc4ppak78rmnx96bhmxzg4n8h0893w68xz78cwjmk3apq")))

