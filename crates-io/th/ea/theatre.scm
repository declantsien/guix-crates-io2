(define-module (crates-io th ea theatre) #:use-module (crates-io))

(define-public crate-theatre-0.1.0 (c (n "theatre") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0ri0cs3x2p3yl4kmx4k9p9dk60rgljfqn4d045izd4s68y97a5fl")))

(define-public crate-theatre-0.1.1 (c (n "theatre") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0cmhncc1019ii8kw4c7k9l9566a7lpccw9hgs1hc63rw406l1gkx")))

(define-public crate-theatre-0.1.2 (c (n "theatre") (v "0.1.2") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "sync"))) (d #t) (k 0)) (d (n "tokio") (r "^0.2") (f (quote ("rt-threaded" "macros"))) (d #t) (k 2)))) (h "0lszx0a4j7w9dwfkdda334mhzvjfqbfvf38y8r6dyh5ln0m42v01")))

(define-public crate-theatre-0.1.3 (c (n "theatre") (v "0.1.3") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("rt-multi-thread" "sync" "parking_lot"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "18vaizp0jvsv6kd2ciy56sljmmgncq6rimljpkgmmcfrjrsfqfh8")))

(define-public crate-theatre-0.1.4 (c (n "theatre") (v "0.1.4") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("rt-multi-thread" "sync" "parking_lot"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1lsz0fjy4829hyzqwb46mzwj9mg5dkzwrasdcpdk9sdz8j3558dd")))

(define-public crate-theatre-0.1.5 (c (n "theatre") (v "0.1.5") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("rt-multi-thread" "sync" "parking_lot"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0q8n07brf3d48fdpxx7br9spsph84vabax28742jr3kd4rplyjgh")))

(define-public crate-theatre-0.1.6 (c (n "theatre") (v "0.1.6") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync" "parking_lot"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "1036jbm0a1k1fzc6dph2frmqa0k74y154nkvnfkv4a6xa0v09vmp")))

(define-public crate-theatre-0.1.7 (c (n "theatre") (v "0.1.7") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync" "parking_lot"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0q4x171sfw283bv61mrg5m7fvx4vdg4iafqn3xf6kvnmjddhicqm")))

(define-public crate-theatre-0.1.8 (c (n "theatre") (v "0.1.8") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.3") (d #t) (k 0)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "tokio") (r "^1.2") (f (quote ("sync" "parking_lot"))) (d #t) (k 0)) (d (n "tokio") (r "^1.0.2") (f (quote ("rt-multi-thread" "macros"))) (d #t) (k 2)))) (h "0npn7ikc8p5bif3z43j7mll8zbx4cn3h9aad0bkh699pnnd9izzw")))

