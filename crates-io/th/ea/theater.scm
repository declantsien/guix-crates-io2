(define-module (crates-io th ea theater) #:use-module (crates-io))

(define-public crate-theater-0.1.0 (c (n "theater") (v "0.1.0") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1n71q93hbi808zw1flkmf97l7dpbdi1lx7dgbmyd8nv16srrw685")))

(define-public crate-theater-0.1.1 (c (n "theater") (v "0.1.1") (d (list (d (n "rustc-serialize") (r "*") (d #t) (k 0)))) (h "1jbxh2y3xcs6xz6ksf8yp6zf69xr551ag0sj3cy2isa04avsi5lw")))

