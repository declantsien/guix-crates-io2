(define-module (crates-io th e_ the_rust_book_in_a_nutshell_lessons) #:use-module (crates-io))

(define-public crate-the_rust_book_in_a_nutshell_lessons-0.1.0 (c (n "the_rust_book_in_a_nutshell_lessons") (v "0.1.0") (d (list (d (n "color-eyre") (r "^0.6.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "1wbsvrmapz9rbv2cyyjx1jrzwlvc7gdhkg5afs44x41qvkph4rvf") (y #t)))

