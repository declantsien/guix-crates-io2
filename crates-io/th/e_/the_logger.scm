(define-module (crates-io th e_ the_logger) #:use-module (crates-io))

(define-public crate-the_logger-0.5.0 (c (n "the_logger") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "0j53d6gnvr0xyjs7hrkm0q2jnhqmim8b6n384gzj0p6bh5fmi64a") (f (quote (("json_config" "serde" "serde_json"))))))

(define-public crate-the_logger-0.5.1 (c (n "the_logger") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "0mky2w8sq6z6jldzqpln1s5yknzhqx5hq73xw7cmwa0nn0k90rks") (f (quote (("json_config" "serde" "serde_json"))))))

(define-public crate-the_logger-0.5.2 (c (n "the_logger") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "04sg89554q7h6w3qwp4w5ky3jq65ndl9dmcmd0k0x9qw6k1sb992") (f (quote (("json_config" "serde" "serde_json"))))))

(define-public crate-the_logger-0.5.3 (c (n "the_logger") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.19") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (o #t) (d #t) (k 0)) (d (n "tokio") (r "^1.35.0") (f (quote ("sync"))) (d #t) (k 0)))) (h "1bvdncdbj3n3hy3cwz89xvpq1abwm1j04wlb32k9wb0d54440s7w") (f (quote (("json_config" "serde" "serde_json"))))))

