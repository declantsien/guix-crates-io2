(define-module (crates-io th e_ the_heck) #:use-module (crates-io))

(define-public crate-the_heck-0.1.0 (c (n "the_heck") (v "0.1.0") (d (list (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "fst") (r "^0.4") (f (quote ("levenshtein"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "11lvbnrxvwmi740vyvp1x5dacx1dgih188f886d2x9idga1whqbv") (y #t)))

(define-public crate-the_heck-0.1.1 (c (n "the_heck") (v "0.1.1") (d (list (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "fst") (r "^0.4") (f (quote ("levenshtein"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1cvigpz52y7qyg7l6l63s2bnawsbai7zwhlcbnabffq6d0dnx4gp") (y #t)))

(define-public crate-the_heck-0.1.2 (c (n "the_heck") (v "0.1.2") (d (list (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "fst") (r "^0.4") (f (quote ("levenshtein"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "05673zl057vwcswf5i1dn010s5niqhwprzw36bki7wq62xjxa2k2")))

(define-public crate-the_heck-0.1.3 (c (n "the_heck") (v "0.1.3") (d (list (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "fst") (r "^0.4") (f (quote ("levenshtein"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "0xjfqlpsrpa0z08px12m4j67hn240wyd7hcqb78gpqhm5gbmcfs9")))

(define-public crate-the_heck-0.1.4 (c (n "the_heck") (v "0.1.4") (d (list (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "fst") (r "^0.4") (f (quote ("levenshtein"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1i1l0dzh9y4f8b967hy8wcx157whxgg64l10adsvaia9vdmv157c")))

(define-public crate-the_heck-0.1.5 (c (n "the_heck") (v "0.1.5") (d (list (d (n "dialoguer") (r "^0.10.3") (d #t) (k 0)) (d (n "fst") (r "^0.4") (f (quote ("levenshtein"))) (d #t) (k 0)) (d (n "home") (r "^0.5.4") (d #t) (k 0)) (d (n "rev_buf_reader") (r "^0.3.0") (d #t) (k 0)))) (h "1s75dbv7qaylghyzlllla4c29rj5l0w8pvily6r522l4bgk53rn4")))

