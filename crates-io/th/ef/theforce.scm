(define-module (crates-io th ef theforce) #:use-module (crates-io))

(define-public crate-theforce-0.1.0 (c (n "theforce") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "inkwell") (r "^0.1.0-beta.4") (o #t) (d #t) (k 0)) (d (n "pest") (r "^2.1.3") (d #t) (k 0)) (d (n "pest_derive") (r "^2.1.0") (d #t) (k 0)))) (h "1wmshli8pjl77kggnysh7g16kx35rj4281fhgp615ai26wgap349") (f (quote (("llvm" "inkwell"))))))

