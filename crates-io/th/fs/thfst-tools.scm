(define-module (crates-io th fs thfst-tools) #:use-module (crates-io))

(define-public crate-thfst-tools-1.0.0-beta.1 (c (n "thfst-tools") (v "1.0.0-beta.1") (d (list (d (n "box-format") (r "^0.3") (d #t) (k 0)) (d (n "divvunspell") (r "^1.0.0-beta.1") (f (quote ("internal_convert" "compression"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.57") (d #t) (k 0)) (d (n "structopt") (r "^0.3.17") (d #t) (k 0)) (d (n "tempdir") (r "^0.3.7") (d #t) (k 0)))) (h "1fr1lz7m99z4arnl4hjk0rw280yv6d9a2c22hj2h6gvg6w32ak29")))

