(define-module (crates-io th an thanix) #:use-module (crates-io))

(define-public crate-thanix-0.1.0 (c (n "thanix") (v "0.1.0") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "0qiryq6787pnj7gpwd2wf81y0k0729wmjw2n8invs8i8qgh7pkpc") (y #t)))

(define-public crate-thanix-0.1.0-alpha.1 (c (n "thanix") (v "0.1.0-alpha.1") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "032r3g8ip241j95zw58m3h52sfc4vfkd3csp37x2dplmzhwgpdzs") (y #t)))

(define-public crate-thanix-0.1.0-alpha.2 (c (n "thanix") (v "0.1.0-alpha.2") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "0c832viix09g8f6b854ylycbhn6b5d2v9dnj2jhra6wyj4dj918c") (y #t)))

(define-public crate-thanix-0.1.0-alpha.3 (c (n "thanix") (v "0.1.0-alpha.3") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "03mmyff5jc75xc1fy4r65cl7pwp0zxqpl8j308bg80als3ki35i8") (y #t)))

(define-public crate-thanix-0.1.0-alpha.4 (c (n "thanix") (v "0.1.0-alpha.4") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "0iprj6bs3a1kl6b9czn3lq9lzh2p2rllrvkqxpic10sx135m6naq") (y #t)))

(define-public crate-thanix-0.1.0-alpha.5 (c (n "thanix") (v "0.1.0-alpha.5") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "0rj18jl83k6n42dn31c3cfdx076apbblz121hwvmyc0i27rlfbsh") (y #t)))

(define-public crate-thanix-0.1.0-alpha.6 (c (n "thanix") (v "0.1.0-alpha.6") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "0mf5b1379ii7nia0fil9y93qadrbpr2iswhsm4lw69p9m2d3d4pi") (y #t)))

(define-public crate-thanix-0.1.0-alpha.7 (c (n "thanix") (v "0.1.0-alpha.7") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "08540hzflsaixfr8zxz89b1ds0cbj210b6y9im1b682hg1cn9n9j") (y #t)))

(define-public crate-thanix-0.1.0-alpha.9 (c (n "thanix") (v "0.1.0-alpha.9") (d (list (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "14yagfymr51z5p9yi5zb3zbyplhmg4mkdhzpwjczmfm85hvg5hpn") (y #t)))

(define-public crate-thanix-0.1.0-alpha.10 (c (n "thanix") (v "0.1.0-alpha.10") (d (list (d (n "check_keyword") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "openapiv3") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "001zi9lrrslnip26a45ax7vwq36bjfnh4p3vdamqv2ky43pi8i3x") (y #t)))

(define-public crate-thanix-0.1.0-alpha.12 (c (n "thanix") (v "0.1.0-alpha.12") (d (list (d (n "check_keyword") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "openapiv3") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "1zlhnp98jbk304fm815572d7yq2xm0l2mvah2hcqx3plxllc94i5") (y #t)))

(define-public crate-thanix-0.1.0-beta.1 (c (n "thanix") (v "0.1.0-beta.1") (d (list (d (n "check_keyword") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "openapiv3") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "0s496z2dyrwlnfgkfr6nnafgmhginc2w6bph1bwwh3v6a8mqk3yn")))

(define-public crate-thanix-0.1.0-beta.2 (c (n "thanix") (v "0.1.0-beta.2") (d (list (d (n "check_keyword") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "openapiv3") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "1c8bnvayc47z3d8wrjn4w30ns63kjxwff4v38f28h6f0wv263mv0")))

(define-public crate-thanix-0.1.0-beta.3 (c (n "thanix") (v "0.1.0-beta.3") (d (list (d (n "check_keyword") (r "^0.2.0") (d #t) (k 0)) (d (n "clap") (r "^4.4.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "convert_case") (r "^0.6.0") (d #t) (k 0)) (d (n "openapiv3") (r "^2.0.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.9.30") (d #t) (k 0)))) (h "04gjay1f05xbpzzgqg833awxvbwl8mh1skj2a5lggha7k6szdcqb")))

