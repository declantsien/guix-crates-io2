(define-module (crates-io th an thankyoulist) #:use-module (crates-io))

(define-public crate-thankyoulist-0.1.0 (c (n "thankyoulist") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1") (f (quote ("full"))) (d #t) (k 0)) (d (n "toml") (r "^0.5") (d #t) (k 0)))) (h "15fyzj1198zdwsm0jlkpl1igdgdk71644n8kq4p88cydsndyzgxl")))

