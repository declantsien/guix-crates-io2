(define-module (crates-io th an thanix_client) #:use-module (crates-io))

(define-public crate-thanix_client-0.1.0 (c (n "thanix_client") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "19zmh4zfp82l8ljd65jsjf7g74nm1aripxb8f8137zz07clj6iz6") (y #t)))

(define-public crate-thanix_client-0.2.0 (c (n "thanix_client") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "0b9497ssxqalq3bwlk8qk7fkfaxqmk0bq21b9467k77kg9px8lp7") (y #t)))

(define-public crate-thanix_client-0.3.0 (c (n "thanix_client") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "1c6902zc81484j5nig1msisji1956qjm6c4v392v5v3xl65kbz84") (y #t)))

(define-public crate-thanix_client-0.4.0 (c (n "thanix_client") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "0k8vkh6994y04c33x2p66g35647xhlm66bvzxl0kvv6nh5bb3z7g") (y #t)))

(define-public crate-thanix_client-0.5.0 (c (n "thanix_client") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "08720l0aq89i2scy234ggjlf2f6l2821wq9l0ppndncavh0sf6mn") (y #t)))

(define-public crate-thanix_client-0.6.0 (c (n "thanix_client") (v "0.6.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "0h5v582ixqm9rjd9sabmyvg9xx6ln2da0hrs0n73nv7zb4sv7xs4") (y #t)))

(define-public crate-thanix_client-0.7.0 (c (n "thanix_client") (v "0.7.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "1vlk40x74pjx6iijqiqc816qmplm6bmwsg5k9w8hdlszaw0lmnc1") (y #t)))

(define-public crate-thanix_client-0.8.0 (c (n "thanix_client") (v "0.8.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "0690vg0034r9a65gxpg3v9xydjf7apgqnasr12z3jj1jzv3c8vrw") (y #t)))

(define-public crate-thanix_client-0.9.0 (c (n "thanix_client") (v "0.9.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "0dvg73wnx32f3niy8fwhicxf0j97rzz9jdh7mclq627q7q63m0lp") (y #t)))

(define-public crate-thanix_client-1.0.0 (c (n "thanix_client") (v "1.0.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "1vi6f04szdrkmbq4z7zdzj4cjr5rcrd4ii294chla5vvg4f1rwkm")))

(define-public crate-thanix_client-1.1.0 (c (n "thanix_client") (v "1.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("blocking" "json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.108") (d #t) (k 0)) (d (n "serde_qs") (r "^0.12.0") (d #t) (k 0)))) (h "0imy6k40v0z14p93is7nhwqzl2nipv1420v605hcl6cfrccq88zs")))

