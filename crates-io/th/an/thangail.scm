(define-module (crates-io th an thangail) #:use-module (crates-io))

(define-public crate-thangail-0.1.2 (c (n "thangail") (v "0.1.2") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("rustls-tls"))) (k 0)) (d (n "rocket") (r "^0.5.0") (d #t) (k 0)) (d (n "rocket_async_compression") (r "^0.5") (d #t) (k 0)))) (h "0lkvcqbw39s0jgs783zbkljq1vbwvczr380kya2jqf2w9h5xx6a1")))

