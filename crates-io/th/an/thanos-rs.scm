(define-module (crates-io th an thanos-rs) #:use-module (crates-io))

(define-public crate-thanos-rs-1.0.0 (c (n "thanos-rs") (v "1.0.0") (d (list (d (n "argh") (r "^0") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "dialoguer") (r "^0") (d #t) (k 0)) (d (n "indicatif") (r "^0") (d #t) (k 0)) (d (n "rand") (r "^0") (d #t) (k 0)))) (h "19kd4vpj6p6wa00fpafb08xk9scmp0m3lz2hmxnf71dglqcgj2nq")))

