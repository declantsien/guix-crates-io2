(define-module (crates-io th et thetadb) #:use-module (crates-io))

(define-public crate-thetadb-0.0.1 (c (n "thetadb") (v "0.0.1") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1l2a05wank8pcbw1h1zppj81dn9swn57cd2bv93s8ixgak21yba6")))

(define-public crate-thetadb-0.0.2 (c (n "thetadb") (v "0.0.2") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1d7d6py79lfq24b0nk6gc1491rlfzr5zk0npr7di5bx6k5m9aqc3")))

(define-public crate-thetadb-0.0.3 (c (n "thetadb") (v "0.0.3") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0agzprnqw69mplpkfb03yx2gq549i9hq3fn9as7dhrvj5m1nz7ck")))

(define-public crate-thetadb-0.0.4 (c (n "thetadb") (v "0.0.4") (d (list (d (n "crc32fast") (r "^1.3.2") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j9dzp8ak7cqp30ajybn210rvdpb65a6s3n12q839iaqdssjx6d5")))

