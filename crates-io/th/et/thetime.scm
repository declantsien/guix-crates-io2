(define-module (crates-io th et thetime) #:use-module (crates-io))

(define-public crate-thetime-0.1.0 (c (n "thetime") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0rl361nmgp31z8avvq2aqqj4rgmjv6br896n6g6j0n1gmr2yfil9")))

(define-public crate-thetime-0.1.1 (c (n "thetime") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "1vbzlq8958v9pbm8my1qfx4k0zqc0xw1wz3nicb28bap2rnjxhxn")))

(define-public crate-thetime-0.1.2 (c (n "thetime") (v "0.1.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)))) (h "0vkxn1azzvb49s1dgj0p456l3ijbgka864mgcg7n550gjncv5khi")))

(define-public crate-thetime-0.2.0 (c (n "thetime") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "1v2cz0ibs3wm4g9g44zypmcb1wj7nhj5nbh7bw4bx50rylwjxfi6")))

(define-public crate-thetime-0.2.1 (c (n "thetime") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "1g3azhqwzd10hh4k590lliiyb7ink6n837dfwv942c8pazhgbv4f")))

(define-public crate-thetime-0.3.0 (c (n "thetime") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "1m7mh167016q6yw683skk828f28s9zk576rnxr9jxdzbp3waz0bl")))

(define-public crate-thetime-0.4.0 (c (n "thetime") (v "0.4.0") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "16ifk3d1a4fmkyj8ggif169sfbilr56q5pjf15982kwqnqm3r0ip")))

(define-public crate-thetime-0.4.1 (c (n "thetime") (v "0.4.1") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "10zccfpazpy97sdzlz9v1qhxr0qpi1y159g7s30fqwh8mqx0fwjz")))

(define-public crate-thetime-0.4.2 (c (n "thetime") (v "0.4.2") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "1g9zc34bwah4km0ry5qxmc7z07m4rrl8dk51n79j66q4zk0k65m9")))

(define-public crate-thetime-0.4.3 (c (n "thetime") (v "0.4.3") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "045ml771bhn7i3mzn10k6pxl4q67d7wfv04v8ya2dbckibrznwz2")))

(define-public crate-thetime-0.4.4 (c (n "thetime") (v "0.4.4") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "1l0ki0sij99whbm85bbzprw012i3xn7nc1yjg9qp94xjgx405dxx")))

(define-public crate-thetime-0.4.5 (c (n "thetime") (v "0.4.5") (d (list (d (n "chrono") (r "^0.4.31") (d #t) (k 0)) (d (n "time") (r "^0.3.31") (d #t) (k 0)))) (h "0hrg1dlfbyr7g6226h3wn7a0g90iax1cgcxawc14zs74a8j5riwf")))

(define-public crate-thetime-0.5.0 (c (n "thetime") (v "0.5.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)))) (h "05g5qqgrybpr5swfrv99sm07k9fk5di3p4i80ra08dixjbkcshm4") (f (quote (("ntp") ("default" "ntp"))))))

(define-public crate-thetime-0.5.1 (c (n "thetime") (v "0.5.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)))) (h "0kvqz4f0fd5fczwqbdlp1knqag3skg0r5a2j6xl5qkpdymqb9v7b") (f (quote (("ntp") ("default" "ntp"))))))

(define-public crate-thetime-0.5.2 (c (n "thetime") (v "0.5.2") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)))) (h "1w4hh827g8v5lm43kdgwg2hlzlhmrpy7pawll9acsqxr4adpi5n3") (f (quote (("ntp") ("default" "ntp"))))))

(define-public crate-thetime-0.5.3 (c (n "thetime") (v "0.5.3") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)))) (h "1klgaia3sj3afi3rssrriwpsnjamds6y2v6v3fhm11pia1dv44g8") (f (quote (("ntp") ("default" "ntp"))))))

(define-public crate-thetime-0.5.4 (c (n "thetime") (v "0.5.4") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)))) (h "1xv7ir86fq4had6xhgvif2ax5bmivi98f99d9y2yi9k94l8hk2lw")))

(define-public crate-thetime-0.5.5 (c (n "thetime") (v "0.5.5") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "0za78ba13nx2mmmi4y5gdag7yppmkgh76d8zra1hcz46r9bghay3")))

(define-public crate-thetime-0.5.6 (c (n "thetime") (v "0.5.6") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("clock"))) (k 0)) (d (n "serde") (r "^1.0.130") (f (quote ("derive"))) (d #t) (k 0)))) (h "135kdb879wlvjx2ycxb78iiz5j0aapm7hgp4bd8dhpnm6d58cr18")))

