(define-module (crates-io th et theta-chart) #:use-module (crates-io))

(define-public crate-theta-chart-0.0.1 (c (n "theta-chart") (v "0.0.1") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)))) (h "0xk7mn9cka612x2c6dy8yxvn3889wpkaxagzq736l9696bcfbpp6")))

(define-public crate-theta-chart-0.0.2 (c (n "theta-chart") (v "0.0.2") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)))) (h "0adb2v89mh82i18jf87w7bp8kf9shbp35ha4scnmr3v47imwyv9s")))

(define-public crate-theta-chart-0.0.3 (c (n "theta-chart") (v "0.0.3") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)))) (h "0ci5rc84f4rkkj6xmvpqw60121bgl17mmy6v3fbpifm6r4y02msc")))

(define-public crate-theta-chart-0.0.4 (c (n "theta-chart") (v "0.0.4") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)))) (h "06blps3gm6zi9g37wh3i8c7lb1dfas2i766gmmdnrvjnhh0mp53j")))

(define-public crate-theta-chart-0.0.5 (c (n "theta-chart") (v "0.0.5") (d (list (d (n "approx") (r "^0.5.1") (d #t) (k 0)) (d (n "chrono") (r "^0.4.26") (d #t) (k 0)) (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "palette") (r "^0.7.2") (d #t) (k 0)))) (h "1lnyzrjhafvyb29gy6iybbaapm0zv2an2anych5h5sbj3yqk999x")))

(define-public crate-theta-chart-0.0.6 (c (n "theta-chart") (v "0.0.6") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)))) (h "1ddmkz2wknkknjnhr8k5cwf3chcr7ccbmm4hyk5k3hrwsn7cbh9b")))

(define-public crate-theta-chart-0.0.7 (c (n "theta-chart") (v "0.0.7") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)))) (h "1asy90gsib2c38bsz7nfj1m862h90zajcz3106rhiwpqn60z9nmb")))

(define-public crate-theta-chart-0.0.8 (c (n "theta-chart") (v "0.0.8") (d (list (d (n "approx") (r "^0.5") (d #t) (k 0)) (d (n "chrono") (r "^0.4") (f (quote ("alloc"))) (k 0)) (d (n "nalgebra") (r "^0.32") (d #t) (k 0)) (d (n "palette") (r "^0.7") (d #t) (k 0)) (d (n "robust") (r "^1.1.0") (d #t) (k 0)))) (h "0ji81lccm81gshv9hx42l5ad040x5phvn62w63a776z3hj3bjg83")))

