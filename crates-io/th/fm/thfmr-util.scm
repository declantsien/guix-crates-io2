(define-module (crates-io th fm thfmr-util) #:use-module (crates-io))

(define-public crate-thfmr-util-0.1.0 (c (n "thfmr-util") (v "0.1.0") (h "1dxmnb5yacymxb3v7vlac74vqczb2j0a24jd4q9m2pf9lf4qsc6v")))

(define-public crate-thfmr-util-0.1.1 (c (n "thfmr-util") (v "0.1.1") (h "1d1q0p53h78c8cblgi7c7vzgbjhzf5xd536n7y4zk8j4bxw14rlg")))

(define-public crate-thfmr-util-0.1.2 (c (n "thfmr-util") (v "0.1.2") (h "0qymkr9dh4700dsg08z58vj0nis5ivr47wsyvqmbgd5kfkffsdl4")))

(define-public crate-thfmr-util-0.2.0 (c (n "thfmr-util") (v "0.2.0") (h "0r56szpxskszcp969lxj8gr8hmbl7gljvv21rwpci3dwmb7cajc5")))

(define-public crate-thfmr-util-0.2.1 (c (n "thfmr-util") (v "0.2.1") (h "1nf5c9vsj70qixlncskxd3kszh0hgjcv1k80khb9y82s0l5kv310")))

(define-public crate-thfmr-util-0.2.2 (c (n "thfmr-util") (v "0.2.2") (d (list (d (n "async-graphql") (r "^2.8.0") (d #t) (k 0)) (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "13qhcazds82mvxddqmh3xg9gdg8yh0svd8yh9vz913r41pnn99yw")))

