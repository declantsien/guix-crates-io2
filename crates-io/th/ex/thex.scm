(define-module (crates-io th ex thex) #:use-module (crates-io))

(define-public crate-thex-0.1.0 (c (n "thex") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.0.0") (d #t) (k 0)) (d (n "parking_lot") (r "^0.5.3") (d #t) (k 0)) (d (n "stable_deref_trait") (r "^1.0.0") (d #t) (k 0)))) (h "12y132dkcbblj227y9hgyprkxdwg0afax4jrhv0nvsz8zdzvv80w") (y #t)))

