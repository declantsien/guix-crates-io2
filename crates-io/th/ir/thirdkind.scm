(define-module (crates-io th ir thirdkind) #:use-module (crates-io))

(define-public crate-thirdkind-1.0.0 (c (n "thirdkind") (v "1.0.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1l7c3hkzz13awkin4fk49zfcnnw2lcixhsdz5c32zk7paajczjrx")))

(define-public crate-thirdkind-1.0.1 (c (n "thirdkind") (v "1.0.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.22.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "13i9zkzg8a1d57djmprwd7ah6d0r3w9sfvb0c76mg5flxknl1hbs")))

(define-public crate-thirdkind-1.0.2 (c (n "thirdkind") (v "1.0.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.22.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1gc3lfnchydcvwzxbspl1gvsfy2dkxq9s7nc510qhzhxrr92qli5")))

(define-public crate-thirdkind-1.0.4 (c (n "thirdkind") (v "1.0.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.22.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0vn6cv2d17mwk4q4fia4ks20njxy7p40r9lc2ay4jiv7pvcdng46")))

(define-public crate-thirdkind-1.0.5 (c (n "thirdkind") (v "1.0.5") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.22.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1vc87vgvi91l7yw324mv20vwxr41qrqry59mvvzq0fps14svqd6w")))

(define-public crate-thirdkind-1.0.6 (c (n "thirdkind") (v "1.0.6") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.22.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1ddpvccbarmcdbm4ryqqkn5285w53xvzp5kbc20a0i44fxrdfncg")))

(define-public crate-thirdkind-1.0.7 (c (n "thirdkind") (v "1.0.7") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.22.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0r8mbf8bllnxb4y99c6j24hj8l9931i2yxqxcsgna0za9y0yw3wd")))

(define-public crate-thirdkind-1.1.0 (c (n "thirdkind") (v "1.1.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.23.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0m40g2sbpgajaws1b1hw2wlr3kkarq65xxrr4dlymhknx4k3k8mc")))

(define-public crate-thirdkind-1.2.0 (c (n "thirdkind") (v "1.2.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.24.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1w5vwx4a56n3wqhymipgr09zi3vi0k6m1v450qijf1yry9fkgyd3")))

(define-public crate-thirdkind-1.2.1 (c (n "thirdkind") (v "1.2.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.24.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0b3d0wjd5sgnhl5i8hlzwf1dbn72mlg8jw6b80caxsslqax2h39x")))

(define-public crate-thirdkind-1.3.0 (c (n "thirdkind") (v "1.3.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.24.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "18794jnqvp3kp1kv20imf9xasprz5z6w7gjd72hd1zqdv0xipb3g")))

(define-public crate-thirdkind-1.3.1 (c (n "thirdkind") (v "1.3.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.24.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "073dapgp20zzv1libqy2mhgfvhbgqayz79zwf3z6h6rrg6dsipmh")))

(define-public crate-thirdkind-1.3.2 (c (n "thirdkind") (v "1.3.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.25.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1qw0ixdzb6j075agdg17000xh0rabjchiyxd7lfwjycag5wklrkm")))

(define-public crate-thirdkind-1.4.0 (c (n "thirdkind") (v "1.4.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.26.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "087429rlrg25437222sgc1nqw1md7kv6bmksn6lwacc1rlnqliph")))

(define-public crate-thirdkind-1.4.1 (c (n "thirdkind") (v "1.4.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.26.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "170h0fi3h5j0cnsawarig8apf564fgp92crlbzmw61n3xvxxj4zz")))

(define-public crate-thirdkind-1.5.0 (c (n "thirdkind") (v "1.5.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.27.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1y1qa04cbwqj0379x8j0ng1kbmrp11yk841vzrpnv4zjnh5f0n9h")))

(define-public crate-thirdkind-1.5.1 (c (n "thirdkind") (v "1.5.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.27.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1c9fgk2xz2vb4m7pgkjj8siqqnxhqg4r3vydmqgg7j723ii3vv39")))

(define-public crate-thirdkind-1.6.0 (c (n "thirdkind") (v "1.6.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.27.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1vxgppwpg2wyls06fzr9hnixpccxfzsz2r33cf2pcvk815ww1jhn")))

(define-public crate-thirdkind-1.6.1 (c (n "thirdkind") (v "1.6.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.27.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0zs6n19arxgikakgq4xb57am118bnxnrvf0220nk3lm19p0ljdha")))

(define-public crate-thirdkind-1.7.0 (c (n "thirdkind") (v "1.7.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.27.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "057df9ll4yi9hd8wycp6krh5kdd1vlvlvdw9p09jssvwxfgiw0mk")))

(define-public crate-thirdkind-1.8.0 (c (n "thirdkind") (v "1.8.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.28.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1aagi6dw1b8mci080mrq2z5xa2sd7dk2ki8fgrnkyp7584npgabb")))

(define-public crate-thirdkind-2.0.0 (c (n "thirdkind") (v "2.0.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.29.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "05zn45509c02a9fjpqvcqq97gxm5rmq7qn6vx86rncqm8l00w7sl")))

(define-public crate-thirdkind-2.0.1 (c (n "thirdkind") (v "2.0.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.29.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "15q0wsqh4zx5g4mh2z3r4hyz8rjl39408s65snnihrxn1pa6fp88")))

(define-public crate-thirdkind-2.1.0 (c (n "thirdkind") (v "2.1.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.30.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "04w32p9jknb7rcdc6rl20z0k8pnb84kg63xdsnjwivdn94q9znax")))

(define-public crate-thirdkind-2.2.0 (c (n "thirdkind") (v "2.2.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.31.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "12ajx67dfynbng61pkv6a4w99bgb54qv77yg86s8fms0z9hvk7jl")))

(define-public crate-thirdkind-2.2.1 (c (n "thirdkind") (v "2.2.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.31.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "01x7sq386c8kk00p2zsfqjpvdssjfw520rjcxy3daddgy01a9jj4")))

(define-public crate-thirdkind-2.2.2 (c (n "thirdkind") (v "2.2.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.31.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1ihf9vwi8p1bivc8ic3cc7fxbjb5l1zmjkphpb6zcw1hjmp3aflq")))

(define-public crate-thirdkind-2.2.3 (c (n "thirdkind") (v "2.2.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.31.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0vyzxwnwwd39n5l9x0ma1l40ab1jyjss6v953hmgzjfc6pcgzbfy")))

(define-public crate-thirdkind-2.2.4 (c (n "thirdkind") (v "2.2.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.31.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "114kjn4jbrgwhglsg6n51zxkyij9a5bkq8z881m3z5432b2d18px")))

(define-public crate-thirdkind-2.3.0 (c (n "thirdkind") (v "2.3.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.32.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1mgapliyzk1s9lxk7hp88k6kvygdsqzns9v7wdq5ph6kn6fipv2w")))

(define-public crate-thirdkind-2.3.1 (c (n "thirdkind") (v "2.3.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.32.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0vmzil9cl5lwjjgarq5s99xqh0acqka0z0z9dffapgj55y9mzlm2")))

(define-public crate-thirdkind-2.3.2 (c (n "thirdkind") (v "2.3.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.32.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0189pnbs3qp0xasbbll2v10yn9llpkpw8s5rkkig8b716lbmf7mk")))

(define-public crate-thirdkind-2.3.3 (c (n "thirdkind") (v "2.3.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.32.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0qzznzqfzwjrlrdg1vm0mjvxmis1hzv4cqm1z1ckwl0p3bw2lwxq")))

(define-public crate-thirdkind-2.3.4 (c (n "thirdkind") (v "2.3.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.32.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "10ksjrxng6pz15mv394q1b4d2q4bh9lfdv1a3akszbbyvf56w1k8")))

(define-public crate-thirdkind-2.4.0 (c (n "thirdkind") (v "2.4.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "141wywvam88nb5krsxdfsr434bf0rz12n0xm4s6n7grn49a19ysl")))

(define-public crate-thirdkind-2.4.1 (c (n "thirdkind") (v "2.4.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.33.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "10qg52far1h5kplb69a7yxvzijb7yabbpsic2l7qmk1s0q7cy8jy")))

(define-public crate-thirdkind-2.4.2 (c (n "thirdkind") (v "2.4.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.34.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1kx3xbdlwvsypgkbqi7p30p0vc4k1rpl2895bny0wrwvsg4mvd51")))

(define-public crate-thirdkind-2.4.3 (c (n "thirdkind") (v "2.4.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.34.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1qgvzx3mssz4xkvcxcnfsixh95jxca1rp67l9pwnmjk9xvy8rrpx")))

(define-public crate-thirdkind-2.4.4 (c (n "thirdkind") (v "2.4.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.34.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1iaaqjs218y4q3q8infqspicxvml68blq6dpwjr11iij2cbz0s1i")))

(define-public crate-thirdkind-2.4.5 (c (n "thirdkind") (v "2.4.5") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.34.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0jz2i2pkgq5h8d5qdknrn2azhvp3vrd6s55cnpdl92nqc6826aqd")))

(define-public crate-thirdkind-2.4.6 (c (n "thirdkind") (v "2.4.6") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.34.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1rfzbrq01k9l8bnsw3n7frlv79v6kqsgnrmzh2b7xk91lvi36wq5")))

(define-public crate-thirdkind-2.4.8 (c (n "thirdkind") (v "2.4.8") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.34.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1pwd0mrdg60d15bdyxjwl327ajlwi791l5s71z3wr8v6qamvvb23")))

(define-public crate-thirdkind-2.5.0 (c (n "thirdkind") (v "2.5.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.34.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0jfmxrvgfzapymi665sxi30cxlxkz25s23m61n8ikhla54bnnsvi")))

(define-public crate-thirdkind-2.5.1 (c (n "thirdkind") (v "2.5.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^0.34.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1xlnfavk5m4nm9snp2dnrnsgnr7nfy31r70nid29ia8p9jqhlcq8")))

(define-public crate-thirdkind-2.6.0 (c (n "thirdkind") (v "2.6.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.0.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0jwsd4m4v3nz99vj1nmxb4b3pqcnrxmvafky5ixg0sn6r1bmgxbl")))

(define-public crate-thirdkind-2.7.0 (c (n "thirdkind") (v "2.7.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.1.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "126vnxa099jrxgd34vrkm6sn7q5xjjk3pz1i31wfsrkm5gqrw34a")))

(define-public crate-thirdkind-2.7.1 (c (n "thirdkind") (v "2.7.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0vn1plwngr6kglbfn8vr36mgdgdl8pm6h3n2i4gs0a945nasz5di")))

(define-public crate-thirdkind-2.7.2 (c (n "thirdkind") (v "2.7.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0dmaihi7934d05il0y1xpq8h13ywcsgdgacl9svns7qy2ln3lxnz")))

(define-public crate-thirdkind-2.8.0 (c (n "thirdkind") (v "2.8.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0wl6h778swfmfvgvkcy1jv5vil95f26zckmcmp24vk8yz0cp0z5a")))

(define-public crate-thirdkind-2.9.0 (c (n "thirdkind") (v "2.9.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1ib20j4qaqh4p76jnwfgmpcii6kvfgmkn18ji6rawm93f3gyc92l")))

(define-public crate-thirdkind-2.9.1 (c (n "thirdkind") (v "2.9.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "195iwxky415z4q4pc856fy4s8rbvbpnczcj253hg1sfibqiwl9qq")))

(define-public crate-thirdkind-2.9.2 (c (n "thirdkind") (v "2.9.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "08sdfr3j2400vb5hnhprkypy9xwps3dc0hlmyapy5nd1n3ka6dd7")))

(define-public crate-thirdkind-2.9.3 (c (n "thirdkind") (v "2.9.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0497g5qfi5xq74w5qzw5lpj4bp45ld9yk0p71k9mryl5fjh1hwkx")))

(define-public crate-thirdkind-2.9.4 (c (n "thirdkind") (v "2.9.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.2.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1w9wzg59ggas49pbbrq3ylxc5law43lvwlv6px8qsr7ay2n2fsn9")))

(define-public crate-thirdkind-3.0.0 (c (n "thirdkind") (v "3.0.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "12i3s938j2pnn4abyfgrn3ln7d2yvp4f1jywwsx6ag6qpcicy1zl")))

(define-public crate-thirdkind-3.0.1 (c (n "thirdkind") (v "3.0.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0z18cys9mc9qbhcma2q6ysksz5j76gci8amkkhgqmrm4m7lq4kkx")))

(define-public crate-thirdkind-3.0.2 (c (n "thirdkind") (v "3.0.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.4.0") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0pi7i3i7jbk6s527nrk1ih3hn26l2jfnhz77ja5288l6y3rva1p2")))

(define-public crate-thirdkind-3.0.3 (c (n "thirdkind") (v "3.0.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.4.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "12g8ypzb8g5ffnq568wld1nynsfyb7zq4ghi92z9csj3nwgapqq8")))

(define-public crate-thirdkind-3.0.4 (c (n "thirdkind") (v "3.0.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.4.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1wf2xmx3x33mjyvqi0cx0kgwm66zzq6ggr09id5mhinx4dprqrw3")))

(define-public crate-thirdkind-3.0.5 (c (n "thirdkind") (v "3.0.5") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^1.4.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0cw3zpvylw6d9n99fji8qnsafja7vmhqhf5ph70ax68qi2mdzlib")))

(define-public crate-thirdkind-3.2.0 (c (n "thirdkind") (v "3.2.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "1k0ny54hzl9pn5n1840zn3m3mjq8q3y5xny82692sdm4sl37xxln")))

(define-public crate-thirdkind-3.2.1 (c (n "thirdkind") (v "3.2.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0c5rd73yxdmg4fqbjk4h06j4vnz3i85f9s1d4ma6jnvs69jxbpr5")))

(define-public crate-thirdkind-3.2.2 (c (n "thirdkind") (v "3.2.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "06igk60383l9lxgbmyg821fiyb7zwgjnp4xpqq0l6yv0513wba4f")))

(define-public crate-thirdkind-3.3.0 (c (n "thirdkind") (v "3.3.0") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.1") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0d7m4m9zym6sfa4j3bpa9f88idmd21jmjhaffil7wv9aygl4dpfl")))

(define-public crate-thirdkind-3.3.1 (c (n "thirdkind") (v "3.3.1") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.5.5") (d #t) (k 0)))) (h "0hmszhp14khnjxfpc7d0svjs2927aww0lv847030lplbcy3pgrci")))

(define-public crate-thirdkind-3.3.2 (c (n "thirdkind") (v "3.3.2") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "1wyq5a4811pb6plkcdw9m63ghj0plj8bd9rx3vv6z3shqhjp87sx")))

(define-public crate-thirdkind-3.3.3 (c (n "thirdkind") (v "3.3.3") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "0z8wigch6m1bam39m13dgfan27vwafjsh7m6dxhi025smqchqf0i")))

(define-public crate-thirdkind-3.3.4 (c (n "thirdkind") (v "3.3.4") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "0qwn5hb0pxmj52hi7h2y4rwvvbcn82l7gm2v1wzs5pxzw7hpjggr")))

(define-public crate-thirdkind-3.3.5 (c (n "thirdkind") (v "3.3.5") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "0jmb53yhs0620211dyi09bkp2s4ywhkillzdw656zjwbflx8yia7")))

(define-public crate-thirdkind-3.3.6 (c (n "thirdkind") (v "3.3.6") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "1ssl58izplfkifai5251cabcqq4f1npncfxfijpp563dj0pq68wp")))

(define-public crate-thirdkind-3.3.7 (c (n "thirdkind") (v "3.3.7") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "0k3b4s3fcbbqwi7fnrkmrm1d2hndvdshsf0pfkh8196h6djpi85k")))

(define-public crate-thirdkind-3.3.8 (c (n "thirdkind") (v "3.3.8") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "00lqck0k2pa36nlbwbd4s7y3cddf8z3k9lj1rx0ncg2ny3px7q1d")))

(define-public crate-thirdkind-3.3.9 (c (n "thirdkind") (v "3.3.9") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "06n4arb24scz3frpf74nys5iizz1xbfrp81rdjd55xlrbq4kvrqa")))

(define-public crate-thirdkind-3.3.10 (c (n "thirdkind") (v "3.3.10") (d (list (d (n "env_logger") (r "^0.8.3") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "0w7svgsy3g005jdxgzx646zr6sinynnrkkpgdkckxk28wskvzdaz")))

(define-public crate-thirdkind-3.3.11 (c (n "thirdkind") (v "3.3.11") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "0k9cfc4iym5yvxkfvsxpdh65lwriiilbv0sgy76z8zfwg3k280bh")))

(define-public crate-thirdkind-3.4.0 (c (n "thirdkind") (v "3.4.0") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "0bpr59gy5mp71ap1rgcdqbywl9xq52nsv9441mj4b2krk2vmbndd")))

(define-public crate-thirdkind-3.5.0 (c (n "thirdkind") (v "3.5.0") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.8") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "1nkcwc7vj4cg9n8zcllqglm39fqjkngr3ml5dzzg92b4my362hih")))

(define-public crate-thirdkind-3.5.1 (c (n "thirdkind") (v "3.5.1") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.9") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "0m1pa64xjv3vlwdwkxj9gwdyx8n1kfka864s4021rk29s5bc1178")))

(define-public crate-thirdkind-3.5.2 (c (n "thirdkind") (v "3.5.2") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.1.10") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "02mlax07v5685qdqmc79avxb007n1f3gkqphdx4x6bssl896ms3g")))

(define-public crate-thirdkind-3.5.3 (c (n "thirdkind") (v "3.5.3") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.2.2") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "0813pava5lkfvrndwhvyqlhb0nsfzxhirnwvrh9qb6fj43h0z5wv")))

(define-public crate-thirdkind-3.5.4 (c (n "thirdkind") (v "3.5.4") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.2.3") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "04qq6c8asj6hmsdsqwj4d1p61h2n0b059pgww4wr7a9fflca8cds")))

(define-public crate-thirdkind-3.5.5 (c (n "thirdkind") (v "3.5.5") (d (list (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "17i994m8ya8k36znmwv83h5fhb7imisd85hp3wfrpcavxwgkcp28")))

(define-public crate-thirdkind-3.6.0 (c (n "thirdkind") (v "3.6.0") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "1w176b2yvbg2x1r550phjfwsnjv89qk3fh1ld4i3kzsdnsws6d12")))

(define-public crate-thirdkind-3.6.1 (c (n "thirdkind") (v "3.6.1") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.2.4") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "09w3l5r1yya4zhgslzba6x0cy98m1x0iig842vdclawl174hxaa5")))

(define-public crate-thirdkind-3.6.2 (c (n "thirdkind") (v "3.6.2") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.2.5") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "1pjvl72prnj92qkdkc9c8249j2rfrlqza0awhy5h6xfrbxl3jp6r")))

(define-public crate-thirdkind-3.6.5 (c (n "thirdkind") (v "3.6.5") (d (list (d (n "clap") (r "^4.5.4") (f (quote ("derive" "wrap_help"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.11.2") (d #t) (k 0)) (d (n "getopt") (r "^1.1.0") (d #t) (k 0)) (d (n "light_phylogeny") (r "^2.2.6") (d #t) (k 0)) (d (n "log") (r "^0.4.14") (d #t) (k 0)) (d (n "webbrowser") (r "^0.8.10") (d #t) (k 0)))) (h "00qkpk3qkxcl08ijpy33svz8yi3nv6cr6kv34w6ag90rdjwgx3g1")))

