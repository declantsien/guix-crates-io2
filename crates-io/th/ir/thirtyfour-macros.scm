(define-module (crates-io th ir thirtyfour-macros) #:use-module (crates-io))

(define-public crate-thirtyfour-macros-0.1.0 (c (n "thirtyfour-macros") (v "0.1.0") (d (list (d (n "itertools") (r "^0.10.3") (d #t) (k 0)) (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1f3if1d2wz10ka6i9rqc92dvfjfzyhrjp1wwypp8iz6h1mwa2vl4")))

(define-public crate-thirtyfour-macros-0.1.1 (c (n "thirtyfour-macros") (v "0.1.1") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1c7vdyflilmxl7wqym761ajm1qqfcia4j1pi2xccc7n6qz8r3blw")))

(define-public crate-thirtyfour-macros-0.1.2 (c (n "thirtyfour-macros") (v "0.1.2") (d (list (d (n "proc-macro-error") (r "^1.0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^1.0.109") (f (quote ("full" "parsing" "extra-traits"))) (d #t) (k 0)))) (h "1psrh3x5a9g39618bhf0izz6sg2wpmg2zyzjgv700nh4yxpqzjb7")))

