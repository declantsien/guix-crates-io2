(define-module (crates-io th ir third-pact) #:use-module (crates-io))

(define-public crate-third-pact-0.1.0 (c (n "third-pact") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "1lja2a16y7p20jcf2n2764chwhsy36grqsdywxcwi42gdd758sfv")))

(define-public crate-third-pact-0.1.1 (c (n "third-pact") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "04xm6jn4bmnnack7fa6n8ay08wzvcn0inbydi9q32v4kjyw4rlpk")))

(define-public crate-third-pact-0.1.2 (c (n "third-pact") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^1.0.29") (d #t) (k 0)) (d (n "quote") (r "^1.0.9") (d #t) (k 0)) (d (n "syn") (r "^1.0.76") (f (quote ("full" "printing"))) (d #t) (k 0)))) (h "0wfbsq1z2qnrykab57pfyq0icyxll0b4kfqya9wc8283zwxmhikh")))

