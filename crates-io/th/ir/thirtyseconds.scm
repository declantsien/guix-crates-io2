(define-module (crates-io th ir thirtyseconds) #:use-module (crates-io))

(define-public crate-thirtyseconds-0.1.0 (c (n "thirtyseconds") (v "0.1.0") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "14nmbf1myzh5708dl80p6v92vh5lj4vfln89w7q6p2s47cwxnzng")))

(define-public crate-thirtyseconds-0.1.1 (c (n "thirtyseconds") (v "0.1.1") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)))) (h "16as0f6y04aiyv160i8hg3pwd7my88s2qfsaz2knxx5h2ri4shdp")))

(define-public crate-thirtyseconds-0.1.4 (c (n "thirtyseconds") (v "0.1.4") (d (list (d (n "cargo-husky") (r "^1") (f (quote ("user-hooks"))) (k 2)) (d (n "proptest") (r "^1.0.0") (d #t) (k 2)))) (h "1rmwvzlfir5rkypbsyfx4887hpj25zc66yc2kwa928bd2b5a4dh2")))

