(define-module (crates-io th ou thoughts) #:use-module (crates-io))

(define-public crate-thoughts-2.0.0 (c (n "thoughts") (v "2.0.0") (d (list (d (n "clap") (r "^4.5.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "home") (r "^0.5.9") (d #t) (k 0)) (d (n "lazy-db") (r "^1.0.2") (d #t) (k 0)) (d (n "rustyline") (r "^13.0.0") (d #t) (k 0)) (d (n "stack-db") (r "^0.3.2") (d #t) (k 0)))) (h "11b1k64lm3pka5f32y93ljq6m7gmkj6xphakn44qbpzpzhqvyhd1")))

(define-public crate-thoughts-1.0.0 (c (n "thoughts") (v "1.0.0") (d (list (d (n "clap") (r "^4.3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "lazy-db") (r "^1.0.2") (d #t) (k 0)))) (h "1gdj91g7a2wpwswknll3cd2hyy1zjmmq7x562cvcsw2b7ab5y18f")))

