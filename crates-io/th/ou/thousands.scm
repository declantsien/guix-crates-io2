(define-module (crates-io th ou thousands) #:use-module (crates-io))

(define-public crate-thousands-0.1.0 (c (n "thousands") (v "0.1.0") (h "0v9svvspvr3c0v72klamz4gb690mr75kqiqdxf9bnzinskaylbp9")))

(define-public crate-thousands-0.1.1 (c (n "thousands") (v "0.1.1") (h "1d5h82kbhxlf95y4cfc1yhsgkzcgzcqj5b092hlywg5hmahhgyw0")))

(define-public crate-thousands-0.1.2 (c (n "thousands") (v "0.1.2") (h "1b4wscjqyx54nx7qrvyjrchx5armw6plcr1dkjczfk5i2i2gdxir")))

(define-public crate-thousands-0.1.3 (c (n "thousands") (v "0.1.3") (h "05ddinl99y3by1yh4zf05qg1vrgv5dyh0wp97p161h8krmvh95kf")))

(define-public crate-thousands-0.1.4 (c (n "thousands") (v "0.1.4") (h "0vhms2mcxcgqdab52bm0pr1mmcpffa9q6q3a0q5crcqwq2b2fyak")))

(define-public crate-thousands-0.2.0 (c (n "thousands") (v "0.2.0") (h "0848gnkn7ah51lrx15z9zmn701ipn6gc4xbk4kfdlfahkypkpxiv")))

