(define-module (crates-io th ou thound) #:use-module (crates-io))

(define-public crate-thound-0.1.0 (c (n "thound") (v "0.1.0") (h "05yfb60lq8a4dhpppf1s1yj60qbzv7lkx25wbwi1p9x7jlp23zlc")))

(define-public crate-thound-0.1.1 (c (n "thound") (v "0.1.1") (h "1kl4z335hcnm4ki72i2sn3r42zm7ya9jwrcjs0452bx8b4rs0gig")))

