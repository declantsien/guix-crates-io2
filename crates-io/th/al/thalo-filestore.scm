(define-module (crates-io th al thalo-filestore) #:use-module (crates-io))

(define-public crate-thalo-filestore-0.5.0 (c (n "thalo-filestore") (v "0.5.0") (d (list (d (n "async-trait") (r "^0.1") (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "tempfile") (r "^3.3") (d #t) (k 2)) (d (n "thalo") (r "^0.5.0") (f (quote ("event-store" "tests-cfg"))) (d #t) (k 0)) (d (n "thalo-inmemory") (r "^0.5.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)) (d (n "tokio") (r "^1.15") (f (quote ("macros" "rt"))) (d #t) (k 2)))) (h "1azp5icrjv07v2b6qk4rhha4yq9y116k0axqh6d7i6b4k69qpd3g")))

