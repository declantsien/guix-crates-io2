(define-module (crates-io th al thalo-macros) #:use-module (crates-io))

(define-public crate-thalo-macros-0.1.0 (c (n "thalo-macros") (v "0.1.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "0yi94c0hyvkm8645sypj1ysravcv6hc0l23z4p2c6bvl139yvmqv")))

(define-public crate-thalo-macros-0.1.1 (c (n "thalo-macros") (v "0.1.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "0287jnxylyjcq56ypm246zcwcppyiz0s0y94lgyf0f07izg956cc")))

(define-public crate-thalo-macros-0.1.2 (c (n "thalo-macros") (v "0.1.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "1a7awz1mfvscbjjfi3pw82mlrj4jbh4s3rrrxpym8rhcx7wmnjmc")))

(define-public crate-thalo-macros-0.1.3 (c (n "thalo-macros") (v "0.1.3") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "1gf3vxmczab1mb6igzpgnr5ywcvjh2zrzcm5fpv38i66glldjj3f")))

(define-public crate-thalo-macros-0.2.0 (c (n "thalo-macros") (v "0.2.0") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "0fjirzq3ybdp02lws3kjpkp0x3xqs1amp2v61gjjm0zvpiwwjljl")))

(define-public crate-thalo-macros-0.2.1 (c (n "thalo-macros") (v "0.2.1") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "19r8cmhzyp934b3wzi17blyjqgznmx810xv7hnk143bwyc0rik5n")))

(define-public crate-thalo-macros-0.2.2 (c (n "thalo-macros") (v "0.2.2") (d (list (d (n "heck") (r "^0.3") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "0p1r9nvkzkspbignyzyq8p01ica6603qd5x58giivh5hzl8m97d2")))

(define-public crate-thalo-macros-0.3.0 (c (n "thalo-macros") (v "0.3.0") (d (list (d (n "better-bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0x76hlahh98varpni4ganbiglf3375v6kd2f343868jkby30dfjm")))

(define-public crate-thalo-macros-0.3.1 (c (n "thalo-macros") (v "0.3.1") (d (list (d (n "better-bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08bp63yq5i1di6h3z3719qarj3hf83r2haw8vfmgvia2y3zzhfvx")))

(define-public crate-thalo-macros-0.3.2 (c (n "thalo-macros") (v "0.3.2") (d (list (d (n "better-bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1zjxvmw690fx4g79hlshmszb7mksyhb96mryfk4p2da6zdlcql5a")))

(define-public crate-thalo-macros-0.3.3 (c (n "thalo-macros") (v "0.3.3") (d (list (d (n "better-bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0lfj7g8hcicl88pim1c0pqqnygkrcb5vzmzdsjbifyvw095raj06")))

(define-public crate-thalo-macros-0.4.0 (c (n "thalo-macros") (v "0.4.0") (d (list (d (n "better-bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0pfqggicip42kspyk7rhs0dsgkxvgrvnhpm3lrdfw8z0ljhgxvci")))

(define-public crate-thalo-macros-0.5.0 (c (n "thalo-macros") (v "0.5.0") (d (list (d (n "better-bae") (r "^0.1") (d #t) (k 0)) (d (n "heck") (r "^0.4") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "157d19bnqsprlh8j1h9mcaqkcfbvr66g71yd2240nsfk9xpqyzrp")))

