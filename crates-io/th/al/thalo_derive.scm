(define-module (crates-io th al thalo_derive) #:use-module (crates-io))

(define-public crate-thalo_derive-0.7.0 (c (n "thalo_derive") (v "0.7.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "0wscsqq0g119x29ij2301aswanivnghjwfdrf5rhj0rrywb0v4ps")))

(define-public crate-thalo_derive-0.8.0 (c (n "thalo_derive") (v "0.8.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full" "extra-traits" "parsing" "printing"))) (d #t) (k 0)))) (h "00lscwwsrvcdaxji48v8mmg9l026b4jx4cmynvkh1hhqgp2rdh2n")))

