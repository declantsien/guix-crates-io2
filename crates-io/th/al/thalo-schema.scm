(define-module (crates-io th al thalo-schema) #:use-module (crates-io))

(define-public crate-thalo-schema-0.5.0 (c (n "thalo-schema") (v "0.5.0") (d (list (d (n "serde") (r "^1.0") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_yaml") (r "^0.8") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "12djrbp2lgril0q2ch4fdp0r78k5c0l9a7spkw7clbsnz944p9a1")))

