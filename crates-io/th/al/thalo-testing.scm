(define-module (crates-io th al thalo-testing) #:use-module (crates-io))

(define-public crate-thalo-testing-0.3.3 (c (n "thalo-testing") (v "0.3.3") (d (list (d (n "thalo") (r "^0.3.3") (d #t) (k 0)) (d (n "thalo") (r "^0.3.3") (f (quote ("macros"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1xd8y56hajwq1ikmcn0qpkny5ddcjml0na0iskvlri0jsrc1c92l")))

(define-public crate-thalo-testing-0.4.0 (c (n "thalo-testing") (v "0.4.0") (d (list (d (n "thalo") (r "^0.4.0") (d #t) (k 0)) (d (n "thalo") (r "^0.4.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1cipxiyyl87f77i1jkyvxrhj8i588vz7rk6ymkdy580yp71aj284")))

(define-public crate-thalo-testing-0.5.0 (c (n "thalo-testing") (v "0.5.0") (d (list (d (n "thalo") (r "^0.5.0") (d #t) (k 0)) (d (n "thalo") (r "^0.5.0") (f (quote ("macros"))) (d #t) (k 2)) (d (n "thiserror") (r "^1.0") (d #t) (k 2)))) (h "1yy11nxi3xjbmy6jrsq821aryh9gkfjxpmj3sfjpqlswqmf5mrnn")))

