(define-module (crates-io th al thallium_derive) #:use-module (crates-io))

(define-public crate-thallium_derive-0.1.0 (c (n "thallium_derive") (v "0.1.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "0c0q21bphp4y0z9nic6vd5j91c7yyj6nsk3j7lai5q3fpca8c4v6")))

(define-public crate-thallium_derive-0.1.1 (c (n "thallium_derive") (v "0.1.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)) (d (n "thallium_ecs") (r "^0.4.2") (d #t) (k 0)))) (h "04yvbkbaskwap4jxslqrdvgzb6q8njyyxpkzrmz6c116kb98xiz5")))

(define-public crate-thallium_derive-0.1.2 (c (n "thallium_derive") (v "0.1.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)) (d (n "thallium_ecs") (r "^0.5.0") (d #t) (k 0)))) (h "187mi2g52xrfcjlbpp5yqgk12kxvidqd4ji8h01qlrygh1mxhfsd")))

(define-public crate-thallium_derive-0.2.0 (c (n "thallium_derive") (v "0.2.0") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)) (d (n "thallium_ecs") (r "^0.6.0") (d #t) (k 0)))) (h "0pjc2qqzp3nw3a9g3cddxb4r83dx3gg4a12cr8hinj8v8rw1mqg0")))

(define-public crate-thallium_derive-0.2.1 (c (n "thallium_derive") (v "0.2.1") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "178pj4xx3kxg8dx30qqw654kqvsd9p088gfw5clg25ahfa60kwq8")))

(define-public crate-thallium_derive-0.2.2 (c (n "thallium_derive") (v "0.2.2") (d (list (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.53") (d #t) (k 0)))) (h "10ji5fkjrijvk9hmvcxfnlbaq3qy1wmdz99jmnlmdypnghr9znsr")))

