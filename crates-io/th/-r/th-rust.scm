(define-module (crates-io th -r th-rust) #:use-module (crates-io))

(define-public crate-th-rust-0.1.0 (c (n "th-rust") (v "0.1.0") (d (list (d (n "nalgebra") (r "^0.32.2") (d #t) (k 0)) (d (n "plotters") (r "^0.3.4") (d #t) (k 0)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)))) (h "0x3ygrvivr81d0b9knivxyn5wgl4867wpdm8f1jwwjnlwz5hrrim")))

