(define-module (crates-io th ig thiggle-client) #:use-module (crates-io))

(define-public crate-thiggle-client-0.0.1 (c (n "thiggle-client") (v "0.0.1") (d (list (d (n "reqwest") (r "^0.11") (f (quote ("json" "multipart"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "serde_derive") (r "^1.0") (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "url") (r "^2.2") (d #t) (k 0)) (d (n "uuid") (r "^1.0") (f (quote ("serde"))) (d #t) (k 0)))) (h "1dv39nqz779l7rg8lcwsac4kk3xbc9s1fyr26jlvv2gq9bw532i4")))

