(define-module (crates-io th ig thigpen) #:use-module (crates-io))

(define-public crate-thigpen-0.1.0 (c (n "thigpen") (v "0.1.0") (d (list (d (n "cargo_toml") (r "^0.17.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.47") (f (quote ("full"))) (d #t) (k 0)))) (h "030dwqk4vwipq0fv205pmrqilx34mffc2aw81ccvv8rpvsl16jkv")))

(define-public crate-thigpen-0.2.1 (c (n "thigpen") (v "0.2.1") (d (list (d (n "cargo_toml") (r "^0.17.2") (d #t) (k 0)) (d (n "clap") (r "^4.4.12") (f (quote ("derive" "string"))) (d #t) (k 0)) (d (n "log") (r "^0.4.20") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0.75") (d #t) (k 0)) (d (n "simplelog") (r "^0.12.1") (d #t) (k 0)) (d (n "syn") (r "^2.0.47") (f (quote ("full"))) (d #t) (k 0)))) (h "1rq6v7ws4q4l74pai1n45px1qh2r9j0x904l90hs98j8gkb6yvrp") (f (quote (("default" "cargo") ("cargo"))))))

