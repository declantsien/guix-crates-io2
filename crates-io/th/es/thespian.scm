(define-module (crates-io th es thespian) #:use-module (crates-io))

(define-public crate-thespian-0.1.0 (c (n "thespian") (v "0.1.0") (h "07ib0yy19b9fvy48z645bs10hdx4yql59457jq4nifgiq6kjv1ic")))

(define-public crate-thespian-0.2.0 (c (n "thespian") (v "0.2.0") (d (list (d (n "tokio") (r "^0.3.5") (f (quote ("full"))) (d #t) (k 0)))) (h "0ywgrr88ikbyx4msgf3580k6c58kysyxpnrasm7bffijsr93zk05")))

