(define-module (crates-io th es thesaurus-wordnet) #:use-module (crates-io))

(define-public crate-thesaurus-wordnet-0.1.0 (c (n "thesaurus-wordnet") (v "0.1.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "18m1g0rgqfmxrvgvhfadshmc7nlgwrbbqpsm5fx8hy4nxa3can82")))

(define-public crate-thesaurus-wordnet-0.1.1 (c (n "thesaurus-wordnet") (v "0.1.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 1)) (d (n "serde_json") (r "^1.0") (d #t) (k 1)))) (h "1y9a0knasg0dqc6d7sjw3db4mycfsqzan5mjxhi30zlf51dsl66w")))

(define-public crate-thesaurus-wordnet-0.2.0 (c (n "thesaurus-wordnet") (v "0.2.0") (d (list (d (n "libflate") (r "^1") (d #t) (k 0)) (d (n "libflate") (r "^1") (d #t) (k 1)) (d (n "serde_json") (r "^1") (d #t) (k 1)))) (h "0d9ynjx5kis597jb8ldgqznaw72b4q1lrbgd1i55biwz8yc5n2qy")))

