(define-module (crates-io th es thespis) #:use-module (crates-io))

(define-public crate-thespis-0.0.0 (c (n "thespis") (v "0.0.0") (h "0rl2fwgxphijj086ragcbg74nr9gia0gaijviidws6f4wiv3j5a1")))

(define-public crate-thespis-0.1.0-alpha.1 (c (n "thespis") (v "0.1.0-alpha.1") (d (list (d (n "futures-sink") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "thespis_derive") (r "^0.1.0-alpha") (o #t) (d #t) (k 0)))) (h "1hxz6z8y89rfsj8hh7h29i1pqzvrvkxgx5wx98rg5fdypn82bkqc") (f (quote (("external_doc") ("derive" "thespis_derive") ("default" "derive"))))))

(define-public crate-thespis-0.1.0-alpha.2 (c (n "thespis") (v "0.1.0-alpha.2") (d (list (d (n "futures-sink") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "thespis_derive") (r "^0.1.0-alpha") (o #t) (d #t) (k 0)))) (h "02jgnwgjjj7247502qnb061jy3sm6gbm052ds4k543r3imxpw3lz") (f (quote (("external_doc") ("derive" "thespis_derive") ("default" "derive"))))))

(define-public crate-thespis-0.1.0-alpha.3 (c (n "thespis") (v "0.1.0-alpha.3") (d (list (d (n "futures-sink") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "rustc_version") (r "^0.3") (d #t) (k 1)) (d (n "thespis_derive") (r "^0.1.0-alpha") (o #t) (d #t) (k 0)))) (h "1y9ig8z3vcidy7rm1b58kiixm47bz0h3njqfxss1ih3f8c4iim6f") (f (quote (("external_doc") ("derive" "thespis_derive") ("default" "derive"))))))

(define-public crate-thespis-0.1.0 (c (n "thespis") (v "0.1.0") (d (list (d (n "futures-sink") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "thespis_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0083mv7x2d0bw4z4kg1d5fpffcq99566ha174zlj52siavdld4mj") (f (quote (("derive" "thespis_derive") ("default" "derive"))))))

(define-public crate-thespis-0.1.1 (c (n "thespis") (v "0.1.1") (d (list (d (n "futures-sink") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "thespis_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0y6dqvr1cx6h1rsls1v2fn6vydf60c0biwdkz16z32dxsfr25516") (f (quote (("derive" "thespis_derive") ("default" "derive"))))))

(define-public crate-thespis-0.2.0 (c (n "thespis") (v "0.2.0") (d (list (d (n "futures-sink") (r "^0.3") (f (quote ("std"))) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "thespis_derive") (r "^0.1") (o #t) (d #t) (k 0)))) (h "10gnkdv33xrsjhaia2cvxrilmcra8cyzpwd6a3xw25drpq6r8dc2") (f (quote (("derive" "thespis_derive") ("default" "derive"))))))

