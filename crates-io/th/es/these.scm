(define-module (crates-io th es these) #:use-module (crates-io))

(define-public crate-these-0.1.0 (c (n "these") (v "0.1.0") (h "1xg6icjflai8fx6wvq4cfmffpj249lq2cvvlvwxl0qyc8g629fpi")))

(define-public crate-these-0.1.1 (c (n "these") (v "0.1.1") (h "1w6k8d7kmda1sf23sxf2kdzlavcc0gf12i5qnh7c47x50cmnd010")))

(define-public crate-these-1.0.0 (c (n "these") (v "1.0.0") (h "181wbsb303km8xgp9pfk41648b8zr47129vm14zdrrmcz87hqci5")))

(define-public crate-these-1.1.0 (c (n "these") (v "1.1.0") (h "180m35qfvvpndqs4mhivw8y3ybaavhrb6x9i4mlmlc4q04qirmqp")))

(define-public crate-these-2.0.0 (c (n "these") (v "2.0.0") (h "0hyya42yzxm6lc13fygxzqpqr099v39zcl8fis9lxi7qyjzssqki")))

