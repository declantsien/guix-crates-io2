(define-module (crates-io th es theshy) #:use-module (crates-io))

(define-public crate-theshy-0.1.0 (c (n "theshy") (v "0.1.0") (h "1hd4ycy6ih67p10gxayy4xhmjj82im8zhnf0rr57h6lhzacsvw6c")))

(define-public crate-theshy-0.1.1 (c (n "theshy") (v "0.1.1") (d (list (d (n "lucian") (r "^0.1.0") (d #t) (k 0)))) (h "1mwzi0jxk6q3db08sdz17vin8yzj8gi37wfais3mr8rph34m0sgs")))

