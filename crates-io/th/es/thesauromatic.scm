(define-module (crates-io th es thesauromatic) #:use-module (crates-io))

(define-public crate-thesauromatic-0.0.10 (c (n "thesauromatic") (v "0.0.10") (d (list (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10.0") (d #t) (k 1)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "13m5v1kjjycgandyc40cmaczkh50dqrz1vsicpnc1mnxxnr6q5gm")))

(define-public crate-thesauromatic-0.0.11 (c (n "thesauromatic") (v "0.0.11") (d (list (d (n "paw") (r "^1.0.0") (d #t) (k 0)) (d (n "phf") (r "^0.10.0") (d #t) (k 0)) (d (n "phf_codegen") (r "^0.10.0") (d #t) (k 1)) (d (n "toml") (r "^0.5.8") (d #t) (k 0)))) (h "09va5ilyzqqk8fn4hn8m59z40jfnj6272l6k0wl1lh002i6a97wl")))

