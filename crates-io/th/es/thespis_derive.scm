(define-module (crates-io th es thespis_derive) #:use-module (crates-io))

(define-public crate-thespis_derive-0.1.0-alpha.1 (c (n "thespis_derive") (v "0.1.0-alpha.1") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.2") (d #t) (k 1)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "00ar3n3bqfbmi9xsk87fwa5j6ckq8ph0hi9h7hbqzaar833pkl0c")))

(define-public crate-thespis_derive-0.1.0 (c (n "thespis_derive") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1") (d #t) (k 0)) (d (n "quote") (r "^1") (d #t) (k 0)) (d (n "rustc_version") (r "^0.4") (d #t) (k 1)) (d (n "syn") (r "^1") (f (quote ("full"))) (d #t) (k 0)))) (h "0zwqnppl7cmm19b9i9ypswrv2xqzpaqgfsf3vs65j4ryymgi5xwv")))

