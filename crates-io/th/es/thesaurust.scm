(define-module (crates-io th es thesaurust) #:use-module (crates-io))

(define-public crate-thesaurust-0.1.0 (c (n "thesaurust") (v "0.1.0") (d (list (d (n "clap") (r "^4.1.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "console") (r "^0.15.5") (f (quote ("windows-console-colors"))) (d #t) (k 0)) (d (n "dialoguer") (r "^0.10.3") (f (quote ("fuzzy-select" "completion" "history"))) (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json" "blocking"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.154") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_derive") (r "^1.0.154") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.94") (d #t) (k 0)) (d (n "textwrap") (r "^0.16.0") (f (quote ("hyphenation" "terminal_size"))) (d #t) (k 0)))) (h "0jj0ic7mymbxiw0aki52kmhzj8ahv0axiwidafpxyxwvpbcn1f4b")))

