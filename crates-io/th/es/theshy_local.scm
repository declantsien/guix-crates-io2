(define-module (crates-io th es theshy_local) #:use-module (crates-io))

(define-public crate-theshy_local-0.1.0 (c (n "theshy_local") (v "0.1.0") (d (list (d (n "lucian") (r "^0.1") (d #t) (k 0)))) (h "0gj9n80avv6y1dk5s2lkhp15368x07wlm61iwq5yam1x3ayb5whp")))

(define-public crate-theshy_local-0.1.1 (c (n "theshy_local") (v "0.1.1") (d (list (d (n "embed-resource") (r "^1.7") (d #t) (k 1)) (d (n "lucian") (r "^0.2.1") (d #t) (k 0)) (d (n "native-windows-derive") (r "^1.0.5") (d #t) (k 0)) (d (n "native-windows-gui") (r "^1.0.12") (d #t) (k 0)) (d (n "omg-cool") (r "^0.1.1") (d #t) (k 0)))) (h "18siiggqshaf2c4qyc3dlzn66ngp9jh213izyp3gqnf8gl26xs0s")))

