(define-module (crates-io th es thesaurus) #:use-module (crates-io))

(define-public crate-thesaurus-0.1.0 (c (n "thesaurus") (v "0.1.0") (d (list (d (n "serde_json") (r "^1.0.61") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.23") (d #t) (k 0)))) (h "122lm6pxdy3ppi7agcwfg4h59jh4h1pa6c7fdjdf9ag7lxw24iha")))

(define-public crate-thesaurus-0.1.2 (c (n "thesaurus") (v "0.1.2") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "1dyj8wjbrfpn8bzgy50c1i35wghpa0yag1jxx1w6yy7g9lwfipf8")))

(define-public crate-thesaurus-0.1.3 (c (n "thesaurus") (v "0.1.3") (d (list (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "0ipqz0bc58b8fziva0da93mgfrpc28z3c5n5m68205wq5m48409a")))

(define-public crate-thesaurus-0.2.0 (c (n "thesaurus") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "libflate") (r "^1.1.0") (d #t) (k 0)) (d (n "libflate") (r "^1.1.0") (d #t) (k 1)) (d (n "serde_json") (r "^1.0.64") (d #t) (k 0)))) (h "19g34vf0by0vl60whnksa443v99l2ssrax3r24533v9kxrrh516i")))

(define-public crate-thesaurus-0.3.0 (c (n "thesaurus") (v "0.3.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 1)))) (h "04qz9xxvj0kylzqiaxr38iks0wdgd870pnnbfbrx47lwkc32m727")))

(define-public crate-thesaurus-0.3.1 (c (n "thesaurus") (v "0.3.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 0)) (d (n "libflate") (r "^1.1") (d #t) (k 1)))) (h "01mxwxb6fjpqlg30w7cwv8kw20xy41zygv7vvfvcr9kx8wdpkvn0")))

(define-public crate-thesaurus-0.4.0 (c (n "thesaurus") (v "0.4.0") (d (list (d (n "thesaurus-moby") (r "^0.1.0") (o #t) (d #t) (k 0)) (d (n "thesaurus-wordnet") (r "^0.1.0") (o #t) (d #t) (k 0)))) (h "14fv9s2gaml8yns3l11wz5zndvyl3qsj1dfpnvyiqgpdml8pqapk") (f (quote (("wordnet" "thesaurus-wordnet") ("moby" "thesaurus-moby") ("default" "wordnet"))))))

(define-public crate-thesaurus-0.5.0 (c (n "thesaurus") (v "0.5.0") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)) (d (n "thesaurus-moby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "thesaurus-wordnet") (r "^0.1") (o #t) (d #t) (k 0)))) (h "03if334dspb77lc6n8g91brqq7dzkjhplfy8wzw40pz61yvxzvwr") (f (quote (("wordnet" "thesaurus-wordnet") ("moby" "thesaurus-moby") ("default" "wordnet"))))))

(define-public crate-thesaurus-0.5.1 (c (n "thesaurus") (v "0.5.1") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)) (d (n "thesaurus-moby") (r "^0.1") (o #t) (d #t) (k 0)) (d (n "thesaurus-wordnet") (r "^0.1") (o #t) (d #t) (k 0)))) (h "0asf0ql4npksfqlwr9aik53a85lqgp8mamg69sagd1gwy3081igc") (f (quote (("wordnet" "thesaurus-wordnet") ("moby" "thesaurus-moby") ("default" "wordnet"))))))

(define-public crate-thesaurus-0.5.2 (c (n "thesaurus") (v "0.5.2") (d (list (d (n "cfg-if") (r "^1") (d #t) (k 2)) (d (n "lazy_static") (r "^1.4") (o #t) (d #t) (k 0)) (d (n "thesaurus-moby") (r "^0.2") (o #t) (d #t) (k 0)) (d (n "thesaurus-wordnet") (r "^0.2") (o #t) (d #t) (k 0)))) (h "0sb3z6j8kyp0r5gacczaa9h3skvk7gzh9723scy6injk3qkylcry") (f (quote (("wordnet" "thesaurus-wordnet") ("static" "lazy_static") ("moby" "thesaurus-moby") ("default" "wordnet" "static"))))))

