(define-module (crates-io th es theshy_server) #:use-module (crates-io))

(define-public crate-theshy_server-0.1.0 (c (n "theshy_server") (v "0.1.0") (h "05hmc6kqfcsrggj9vrjj7lhplrgdmsmxsir47p8nkz2zb4wq69sk")))

(define-public crate-theshy_server-0.1.1 (c (n "theshy_server") (v "0.1.1") (d (list (d (n "lucian") (r "^0.2") (d #t) (k 0)) (d (n "omg-cool") (r "^0.1") (d #t) (k 0)))) (h "09f9l0jwdmg40r2a2xxj4k9ld6mhf4bwrljza5rk981d4c3sdli5")))

