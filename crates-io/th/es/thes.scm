(define-module (crates-io th es thes) #:use-module (crates-io))

(define-public crate-thes-0.1.0 (c (n "thes") (v "0.1.0") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "thesaurus") (r "^0.1.0") (d #t) (k 0)))) (h "0d942v4bcji1i9j29bmj3chhgm2i24bvlk5dlb1zcwf2n164xa12")))

(define-public crate-thes-0.1.1 (c (n "thes") (v "0.1.1") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "thesaurus") (r "^0.1.0") (d #t) (k 0)))) (h "1f7knhvfkwqw2b7n9xk6dqssxbd5m5dzhi7zl47jmg758kql08sk")))

(define-public crate-thes-0.1.2 (c (n "thes") (v "0.1.2") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "shrust") (r "^0.0.7") (d #t) (k 0)) (d (n "thesaurus") (r "^0.1.3") (d #t) (k 0)))) (h "1w3baqp8dxfdrh36gih9q1lhdz8bf2bkqnynylv35ram0iblmzib")))

(define-public crate-thes-0.1.3 (c (n "thes") (v "0.1.3") (d (list (d (n "clap") (r "^2.33.3") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "thesaurus") (r "^0.1.3") (d #t) (k 0)))) (h "0cl7wpy46v5f5w22bf06y7cjh7zanr28zpd0hqlrqijkjwl6pwiw")))

