(define-module (crates-io th um thumbs) #:use-module (crates-io))

(define-public crate-thumbs-0.3.0 (c (n "thumbs") (v "0.3.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "rustbox") (r "^0.11.0") (d #t) (k 0)))) (h "0imrbrjc754hjar9bfhpldn601llyimd6sibk592na45pa261ysy")))

(define-public crate-thumbs-0.4.0 (c (n "thumbs") (v "0.4.0") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "0gfnww0c334i3i5h0yxmraqh4j6a2pzyw8smw6h12w62rlih9ynf")))

(define-public crate-thumbs-0.4.1 (c (n "thumbs") (v "0.4.1") (d (list (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "07g3lvj2f7r1p35r1jf8p3qns98vbghdc76w3ih838jslbbnl576")))

(define-public crate-thumbs-0.4.3 (c (n "thumbs") (v "0.4.3") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "1614i4w7gsdchfkpw0lkf0vjgffgq3ambdbgy1dkrn59djh3pij8")))

(define-public crate-thumbs-0.4.4 (c (n "thumbs") (v "0.4.4") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)))) (h "00gr8a8hp3jrv0q9f8w7jhvy858phq05mwp0zsx6vmcivjqr0xpp")))

(define-public crate-thumbs-0.5.0 (c (n "thumbs") (v "0.5.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "18vh2n3il221vp68a1iqsbvvhl372rq2kiy2v3fzjday981j45dv")))

(define-public crate-thumbs-0.5.1 (c (n "thumbs") (v "0.5.1") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "19bdkcx22kr0fhzn55bbrzgks0rix712ykbgjibf15csa8c1b7qs")))

(define-public crate-thumbs-0.6.0 (c (n "thumbs") (v "0.6.0") (d (list (d (n "base64") (r "^0.11.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "0w1ccz2zhi0aswi7v6a3zizqhwx2h5i8nf9p96r7vf0lpl79kiiz")))

(define-public crate-thumbs-0.6.1 (c (n "thumbs") (v "0.6.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "1pyp4zvvyzzar415flz02kx41dscck3lxs20ga7mkp476q6b5226")))

(define-public crate-thumbs-0.7.1 (c (n "thumbs") (v "0.7.1") (d (list (d (n "base64") (r "^0.13.0") (d #t) (k 0)) (d (n "clap") (r "^2.33.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "regex") (r "^1.3.1") (d #t) (k 0)) (d (n "termion") (r "^1.5") (d #t) (k 0)) (d (n "unicode-width") (r "^0.1.7") (d #t) (k 0)))) (h "14f76fyy2pwpf64ysd6nvjws4q30mmn0cyy7bif23aws1y3d2nx2")))

