(define-module (crates-io th um thumbnailer) #:use-module (crates-io))

(define-public crate-thumbnailer-0.1.0 (c (n "thumbnailer") (v "0.1.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "webp") (r "^0.2.0") (d #t) (k 0)))) (h "04fh1vg2y36kad1vqag6v8xq9nw52f2pi2zrdixjxihbh1pisd0h")))

(define-public crate-thumbnailer-0.2.0 (c (n "thumbnailer") (v "0.2.0") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)) (d (n "webp") (r "^0.2.0") (d #t) (k 0)))) (h "1h92gq6kn2m5668hy2jb7p5qzg78m4qnx2a6izqcjmnx8aq7nnjg")))

(define-public crate-thumbnailer-0.2.1 (c (n "thumbnailer") (v "0.2.1") (d (list (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "vid2img") (r "^0.1.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.0") (d #t) (k 0)))) (h "076xg6qar7mg28nzczlmsnl05qn67dcy3imbh5s9wzq5b7phx570")))

(define-public crate-thumbnailer-0.2.2 (c (n "thumbnailer") (v "0.2.2") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.0") (d #t) (k 0)))) (h "1vqkf0wih6minb09g0n3f8brpqrdjzkcr1awkgy1dwrg113gsi93")))

(define-public crate-thumbnailer-0.2.3 (c (n "thumbnailer") (v "0.2.3") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.0") (d #t) (k 0)))) (h "1y8ad4ypcrf74sny6f3fm4gzf2afnrnbfh2cl0xw4cn283sa5rbm") (f (quote (("ffmpeg" "ffmpeg-next") ("default" "ffmpeg"))))))

(define-public crate-thumbnailer-0.2.4 (c (n "thumbnailer") (v "0.2.4") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.0") (d #t) (k 0)))) (h "0fk4v1acy0fvlc74aq64bwagn2wxx9dps2rl574170lvw87vd0c9") (f (quote (("ffmpeg" "ffmpeg-next") ("default" "ffmpeg"))))))

(define-public crate-thumbnailer-0.2.5 (c (n "thumbnailer") (v "0.2.5") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.23.14") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.2.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.0") (d #t) (k 0)))) (h "0fwcnjhy87gwz0nall0fzx2x0rqbvmkry481i3ihdi50i4f385v0") (f (quote (("ffmpeg" "ffmpeg-next") ("default" "ffmpeg"))))))

(define-public crate-thumbnailer-0.3.0 (c (n "thumbnailer") (v "0.3.0") (d (list (d (n "ffmpeg-next") (r "^4.4.0") (o #t) (d #t) (k 0)) (d (n "image") (r "^0.24.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.1") (d #t) (k 0)))) (h "1gjlv7r9k412s57h2qyj67l9dy7frqlds02zy27xbwha4a1qwb5y") (f (quote (("ffmpeg" "ffmpeg-next") ("default" "ffmpeg"))))))

(define-public crate-thumbnailer-0.4.0 (c (n "thumbnailer") (v "0.4.0") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.1") (d #t) (k 0)))) (h "12rk5b2p3m2vik8gq1ar06yjspnw4yszlqs05003agqs0sq55ihn")))

(define-public crate-thumbnailer-0.5.0 (c (n "thumbnailer") (v "0.5.0") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.1") (d #t) (k 0)))) (h "09lyv083q72i66v7y6zz41v4n6dzam3avv199bp2nd3havzbd3zw")))

(define-public crate-thumbnailer-0.5.1 (c (n "thumbnailer") (v "0.5.1") (d (list (d (n "image") (r "^0.24.0") (d #t) (k 0)) (d (n "lazy_static") (r "^1.4.0") (d #t) (k 0)) (d (n "mime") (r "^0.3.16") (d #t) (k 0)) (d (n "rayon") (r "^1.5.1") (d #t) (k 0)) (d (n "tempfile") (r "^3.3.0") (d #t) (k 0)) (d (n "webp") (r "^0.2.1") (d #t) (k 0)))) (h "0m5kz3hinvl1083p63f330fqazs0ay66f72rdikiixd38hk56szh")))

