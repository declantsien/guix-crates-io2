(define-module (crates-io th um thumbsdbkit) #:use-module (crates-io))

(define-public crate-thumbsdbkit-1.0.0 (c (n "thumbsdbkit") (v "1.0.0") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "thumbsdb") (r "^0.1.0") (d #t) (k 0)))) (h "0z249f5nidixq41igr1g9gx8g5ydizk4am7v9qx8akmyppxv7p0f")))

(define-public crate-thumbsdbkit-1.0.1 (c (n "thumbsdbkit") (v "1.0.1") (d (list (d (n "ansi_term") (r "^0.11.0") (d #t) (k 0)) (d (n "chrono") (r "^0.4.0") (d #t) (k 0)) (d (n "clap") (r "^2.31.1") (d #t) (k 0)) (d (n "thumbsdb") (r "^0.1.0") (d #t) (k 0)))) (h "0fm377qr6sa6gj18fzx9naqyz7aj7fn7xsxdv04227fqhwf9d1b1")))

