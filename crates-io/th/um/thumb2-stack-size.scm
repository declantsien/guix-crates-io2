(define-module (crates-io th um thumb2-stack-size) #:use-module (crates-io))

(define-public crate-thumb2-stack-size-0.1.0 (c (n "thumb2-stack-size") (v "0.1.0") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.7") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)))) (h "05drkczq4q8cw44qrpwgf45c0iz0c6fcjdcay73j72j5vl09dr5p")))

(define-public crate-thumb2-stack-size-0.1.1 (c (n "thumb2-stack-size") (v "0.1.1") (d (list (d (n "argparse") (r "^0.2.1") (d #t) (k 0)) (d (n "rustc-demangle") (r "^0.1.7") (d #t) (k 0)) (d (n "xmas-elf") (r "^0.6.2") (d #t) (k 0)))) (h "0iggd05jnwvhvs74vkgj7f5k569ka080d0d2w2zkgp2x9d0crmmh")))

