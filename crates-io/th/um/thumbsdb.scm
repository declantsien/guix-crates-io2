(define-module (crates-io th um thumbsdb) #:use-module (crates-io))

(define-public crate-thumbsdb-0.1.0 (c (n "thumbsdb") (v "0.1.0") (d (list (d (n "ole") (r "^0.1.14") (d #t) (k 0)))) (h "0ia2kij7by6p0ywrxpjbdpny7gyy5zh0a6zb27kmiszwd6x1pjgi")))

(define-public crate-thumbsdb-0.1.1 (c (n "thumbsdb") (v "0.1.1") (d (list (d (n "ole") (r "^0.1.14") (d #t) (k 0)))) (h "08d3074biji2dywz638w5ljca801m79p52jb8vgkw2wpg3riq5ia")))

(define-public crate-thumbsdb-0.1.2 (c (n "thumbsdb") (v "0.1.2") (d (list (d (n "ole") (r "^0.1.14") (d #t) (k 0)))) (h "1habjgcqcq1hbkh6a2l3cszp8dxpf1cvpg0r5ihg65wdfz2xx14a")))

