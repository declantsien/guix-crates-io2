(define-module (crates-io th is thiserror-nostd-notrait) #:use-module (crates-io))

(define-public crate-thiserror-nostd-notrait-1.0.57 (c (n "thiserror-nostd-notrait") (v "1.0.57") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.57") (k 0) (p "thiserror-nostd-notrait-impl")) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0qcb80k6n8yw9rjsqlmzgz62nwybibp1s0rpjcm4vi12h1ilwi58") (f (quote (("std" "thiserror-impl/std") ("default" "std")))) (r "1.56")))

