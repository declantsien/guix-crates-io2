(define-module (crates-io th is thiserror_core2-impl) #:use-module (crates-io))

(define-public crate-thiserror_core2-impl-2.0.0 (c (n "thiserror_core2-impl") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "08z5ca9pab3sb0brr99n07m8arpas96493zr98s5kxfxx8x1hr4w") (r "1.31")))

