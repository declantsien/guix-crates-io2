(define-module (crates-io th is thiserror-no-std) #:use-module (crates-io))

(define-public crate-thiserror-no-std-2.0.0 (c (n "thiserror-no-std") (v "2.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl-no-std") (r "=2.0.0") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0287lyqjldg4w7gi44wmkrg8cpa35224f5x7cc7h08pscf70mxds") (f (quote (("std" "thiserror-impl-no-std/std")))) (y #t) (r "1.31")))

(define-public crate-thiserror-no-std-2.0.1 (c (n "thiserror-no-std") (v "2.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl-no-std") (r "=2.0.1") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1iy150zclx1ajd986xcyr4r4qvl1789l2zbj0bahx6mb9cilj22y") (f (quote (("std" "thiserror-impl-no-std/std")))) (r "1.31")))

(define-public crate-thiserror-no-std-2.0.2 (c (n "thiserror-no-std") (v "2.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl-no-std") (r "=2.0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "1smgmzgrgzfshbf6qvmv24065vlh66jdibcnribp4lfxjjflbbd3") (f (quote (("std" "thiserror-impl-no-std/std")))) (r "1.31")))

