(define-module (crates-io th is thiserror-core-impl) #:use-module (crates-io))

(define-public crate-thiserror-core-impl-1.0.37 (c (n "thiserror-core-impl") (v "1.0.37") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1zhw3vdqfdnhaglhwhkydhnc0n66n5mxlsa4aj0rwzv1svdb81br") (f (quote (("std")))) (r "1.31")))

(define-public crate-thiserror-core-impl-1.0.38 (c (n "thiserror-core-impl") (v "1.0.38") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1b32vakaixn8z54813dkv3ymzrkrrv9d151gdg8i8c74a181rb0h") (f (quote (("std")))) (r "1.31")))

(define-public crate-thiserror-core-impl-1.0.50 (c (n "thiserror-core-impl") (v "1.0.50") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "014xs0ajjzrc7pxafn1ys8i5f9s2iv5vjqvnrivs05b6ydlhvip4") (f (quote (("std")))) (r "1.56")))

