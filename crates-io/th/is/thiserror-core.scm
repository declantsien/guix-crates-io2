(define-module (crates-io th is thiserror-core) #:use-module (crates-io))

(define-public crate-thiserror-core-1.0.37 (c (n "thiserror-core") (v "1.0.37") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.37") (d #t) (k 0) (p "thiserror-core-impl")) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1lw3rf13vyhd88msvkhnhm1ca4g2liv0xbq4vdba8lr3p2fpd3l0") (f (quote (("std" "thiserror-impl/std") ("default" "std")))) (r "1.31")))

(define-public crate-thiserror-core-1.0.38 (c (n "thiserror-core") (v "1.0.38") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.38") (d #t) (k 0) (p "thiserror-core-impl")) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "15zlv507q2nypg2aphk1fvfnrqzqks59v0aqrl221frpcigk95qd") (f (quote (("std" "thiserror-impl/std") ("default" "std")))) (r "1.31")))

(define-public crate-thiserror-core-1.0.50 (c (n "thiserror-core") (v "1.0.50") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.50") (d #t) (k 0) (p "thiserror-core-impl")) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "16g9j00g7bn8q1wk2i5p5f88vrhr04igxisqpwngdqz5nwcfw0f0") (f (quote (("std" "thiserror-impl/std") ("default" "std")))) (r "1.56")))

