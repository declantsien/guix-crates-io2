(define-module (crates-io th is this_crate_is_here_forever) #:use-module (crates-io))

(define-public crate-this_crate_is_here_forever-0.1.0 (c (n "this_crate_is_here_forever") (v "0.1.0") (h "0gdslcbvvdv5lzprappfq98n5p6l9g02b6nwn2873ybf75r338ql")))

(define-public crate-this_crate_is_here_forever-0.2.0 (c (n "this_crate_is_here_forever") (v "0.2.0") (h "15gmkyzfmd5x3l0xj770bfnbv62ng5amgag8m4b9f6hm31509d8n")))

(define-public crate-this_crate_is_here_forever-0.3.0 (c (n "this_crate_is_here_forever") (v "0.3.0") (h "1vw175l6qp945igl383mkl4c2qgw18v2041acdl8ab9l3rrlahny")))

(define-public crate-this_crate_is_here_forever-0.3.1 (c (n "this_crate_is_here_forever") (v "0.3.1") (h "0q0nqh5kxf30rf1rcf35422qdxdhms5ci0d91xl4b8qdvx7an062")))

