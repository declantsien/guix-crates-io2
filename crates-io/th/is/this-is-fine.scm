(define-module (crates-io th is this-is-fine) #:use-module (crates-io))

(define-public crate-this-is-fine-0.0.1 (c (n "this-is-fine") (v "0.0.1") (d (list (d (n "cargo-husky") (r "^1.5.0") (d #t) (k 2)) (d (n "doc-comment") (r "^0.3.3") (d #t) (k 2)) (d (n "git_info") (r "^0.1.2") (d #t) (k 2)) (d (n "version-sync") (r "^0.9.3") (d #t) (k 2)) (d (n "wasm-bindgen-test") (r "^0.3.28") (d #t) (k 2)))) (h "0y62w4651bap6w9fyzjsc17swdbiysniww4skqclg2sfswkjr4if")))

