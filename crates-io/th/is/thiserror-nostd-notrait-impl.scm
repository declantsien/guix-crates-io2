(define-module (crates-io th is thiserror-nostd-notrait-impl) #:use-module (crates-io))

(define-public crate-thiserror-nostd-notrait-impl-1.0.57 (c (n "thiserror-nostd-notrait-impl") (v "1.0.57") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "03jrck7kai90psbf0fax74fj33b820hpczf6945yck3q1bs5wpjq") (f (quote (("std") ("default" "std")))) (r "1.56")))

