(define-module (crates-io th is thisisplural) #:use-module (crates-io))

(define-public crate-thisisplural-0.1.0 (c (n "thisisplural") (v "0.1.0") (d (list (d (n "thisisplural_derive") (r "^0.1.0") (d #t) (k 0) (p "thisisplural_derive")))) (h "1flw1di168si9mb850p700057mklagwkzaknzmlk5bslk6xlzc1r")))

(define-public crate-thisisplural-0.1.1 (c (n "thisisplural") (v "0.1.1") (d (list (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0h4v1fqd08aw3ilvnkbxa5597829bhx0yb18nzrpv4rqyklxdbgy")))

(define-public crate-thisisplural-0.1.2 (c (n "thisisplural") (v "0.1.2") (d (list (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "08lic3qd1269y5ac81mb9jn3l7n4ss09p4x7m0s115rraxln55v5")))

(define-public crate-thisisplural-0.2.0 (c (n "thisisplural") (v "0.2.0") (d (list (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0xxiac6nxl7rni6jnrxgjzl980mcfig1ns8k2fd2an5jsdsivjdc")))

(define-public crate-thisisplural-0.3.0 (c (n "thisisplural") (v "0.3.0") (d (list (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0qqw4a2zf8z9cihkx93c8ncwhqiwq99j3brr1bqx67581b8akpnn")))

(define-public crate-thisisplural-0.4.0 (c (n "thisisplural") (v "0.4.0") (d (list (d (n "pretty_assertions") (r "^1.4") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2") (f (quote ("full"))) (d #t) (k 0)))) (h "0495v7l57s4gy951ribsqij415s2f87b82yxp792c5wjzihmq9vl")))

