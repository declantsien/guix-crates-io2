(define-module (crates-io th is thisdiagnostic) #:use-module (crates-io))

(define-public crate-thisdiagnostic-0.1.0 (c (n "thisdiagnostic") (v "0.1.0") (d (list (d (n "colored") (r "^2.0.0") (d #t) (k 0)) (d (n "thisdiagnostic-derive") (r "^0.1.0") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "1ncc6asg262fjscs409qsi5xd30mpicr94x2v38fbxwvmrqx0pn2") (y #t)))

