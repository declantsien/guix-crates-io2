(define-module (crates-io th is thistermination) #:use-module (crates-io))

(define-public crate-thistermination-1.0.0 (c (n "thistermination") (v "1.0.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 2)))) (h "1yhb6bbbcrc09m6r1w1vcp782cl8l9c80pwfscbgazxvr6bx9zzx")))

(define-public crate-thistermination-1.1.0 (c (n "thistermination") (v "1.1.0") (d (list (d (n "image") (r "^0.24.6") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "regex") (r "^1.9.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11") (f (quote ("blocking" "json"))) (d #t) (k 2)) (d (n "syn") (r "^2.0.25") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.43") (d #t) (k 2)))) (h "097gqsm46gxix9fbf873kqfhkdr7j9qw2hdcb7gjy3v02s88brb4")))

