(define-module (crates-io th is thisctx_impl) #:use-module (crates-io))

(define-public crate-thisctx_impl-0.1.0 (c (n "thisctx_impl") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "13pf8pdl55657i5l62dni2qv9hvb96hxjg452z12l4xyxqs575k4")))

(define-public crate-thisctx_impl-0.2.0 (c (n "thisctx_impl") (v "0.2.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0w85gycxhgapm30rqil2yjkz5s918bx1w0xnrrvgw51ypgabk3w1") (r "1.33")))

(define-public crate-thisctx_impl-0.3.0 (c (n "thisctx_impl") (v "0.3.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "19asnazmhdv8f3lxjm4pa2nk92v16jzliz60k59kar12nypmhmd5") (r "1.33")))

(define-public crate-thisctx_impl-0.4.0 (c (n "thisctx_impl") (v "0.4.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "18kah4zlxlph9kj089ynrr3v69smphqsybsjhfigvjj9vsnirmdi") (r "1.33")))

