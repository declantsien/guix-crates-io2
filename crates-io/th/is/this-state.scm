(define-module (crates-io th is this-state) #:use-module (crates-io))

(define-public crate-this-state-0.1.0 (c (n "this-state") (v "0.1.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("full"))) (d #t) (k 2)))) (h "1p5zkr5yhmchpwak0rggzgcl1i4ld4ahmafbjnlwac4h3lnbmhpm") (r "1.56.1")))

(define-public crate-this-state-0.1.1 (c (n "this-state") (v "0.1.1") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("full"))) (d #t) (k 2)))) (h "0h5dnp01ndl8caymrssbbd60vfibw6irjcvd6ar69vhw7cf29vpz") (r "1.56.1")))

(define-public crate-this-state-0.2.0 (c (n "this-state") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("full"))) (d #t) (k 2)))) (h "116h70g2h2xqzxsmxyfjba27zds99g5sv9bc859nc5p0ryqjfcdx") (r "1.56.1")))

(define-public crate-this-state-0.3.0 (c (n "this-state") (v "0.3.0") (d (list (d (n "parking_lot") (r "^0.12") (d #t) (k 0)) (d (n "tokio") (r "^1.13") (f (quote ("full"))) (d #t) (k 2)))) (h "0ns5zndwpawkj30iy9dkh7434aa8q5ys3drm3y8vd06in67dgpdf") (r "1.56.1")))

