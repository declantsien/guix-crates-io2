(define-module (crates-io th is thiserror-ext) #:use-module (crates-io))

(define-public crate-thiserror-ext-0.0.1 (c (n "thiserror-ext") (v "0.0.1") (d (list (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.1") (d #t) (k 0)))) (h "133d2rg6hjdq0wpwrqvc809sdljxp0k7h9gwmxi6cj8z6nks8a2z")))

(define-public crate-thiserror-ext-0.0.2 (c (n "thiserror-ext") (v "0.0.2") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.2") (d #t) (k 0)))) (h "0sf6xgvg3j83sz0jkly4kbbb94nfndivkp0waivnxy5c545qdh4h")))

(define-public crate-thiserror-ext-0.0.3 (c (n "thiserror-ext") (v "0.0.3") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.3") (d #t) (k 0)))) (h "0gxq5r40lj23qfmskq7v9p05a2c62hvk0236h0nl8fqissqr2bdz")))

(define-public crate-thiserror-ext-0.0.4 (c (n "thiserror-ext") (v "0.0.4") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.4") (d #t) (k 0)))) (h "0m8s0190rx0j875cr8d5fvz8hzm7mbmjgikdfdi94mifa34v1d9q")))

(define-public crate-thiserror-ext-0.0.5 (c (n "thiserror-ext") (v "0.0.5") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.5") (d #t) (k 0)))) (h "0s4a4q9yi2g4c3y63709nicma68i9pf3h072fchzwd56c1g8qjkn")))

(define-public crate-thiserror-ext-0.0.6 (c (n "thiserror-ext") (v "0.0.6") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.6") (d #t) (k 0)))) (h "1x8bwk02rimcbd2kx2rvnckpxx10v4l4xzdrw70p6ny3dkkz4lxj")))

(define-public crate-thiserror-ext-0.0.7 (c (n "thiserror-ext") (v "0.0.7") (d (list (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "sealed_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.7") (d #t) (k 0)))) (h "1xljbw2shhqyd2m6sz6d0cg37qmnw97v59dwy1jipzwaijpgxq01")))

(define-public crate-thiserror-ext-0.0.8 (c (n "thiserror-ext") (v "0.0.8") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "sealed_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.8") (d #t) (k 0)))) (h "1kv7syzq2ggfy4ndsj6ilh4gb0xmip0nvm4sgz9dv059y6jjxcsi")))

(define-public crate-thiserror-ext-0.0.9 (c (n "thiserror-ext") (v "0.0.9") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "sealed_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.9") (d #t) (k 0)))) (h "1qwy88xk5nyp0kkhl2lz84ww5jij2i4yxb9gmf2qiqvc9qplr3s3")))

(define-public crate-thiserror-ext-0.0.10 (c (n "thiserror-ext") (v "0.0.10") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "sealed_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.10") (d #t) (k 0)))) (h "01x87w7zbgaj8449sagyal1l6750jprs1hxrj0dlmk059f40k13p")))

(define-public crate-thiserror-ext-0.0.11 (c (n "thiserror-ext") (v "0.0.11") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "sealed_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.0.11") (d #t) (k 0)))) (h "0l1a0wfykzmfy5j6d7z2c8h86q07p47lmiifjrsp37rx07ds39jl")))

(define-public crate-thiserror-ext-0.1.0 (c (n "thiserror-ext") (v "0.1.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "sealed_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.1.0") (d #t) (k 0)))) (h "12sx1d9kxqzzhw3rhl7jc9x1790wlkfd39fijwkzc3azxdrxv3rn")))

(define-public crate-thiserror-ext-0.1.1 (c (n "thiserror-ext") (v "0.1.1") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "sealed_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.1.1") (d #t) (k 0)))) (h "0mcnxlrb99kx0ffxqgyjk1xygk4cjfpikzp9gmli6s61i1my71k1")))

(define-public crate-thiserror-ext-0.1.2 (c (n "thiserror-ext") (v "0.1.2") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "sealed_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.1.2") (d #t) (k 0)))) (h "1lmsqsphh6xi1mk4gb2i5n09670i6aq9jdhvbk52n1j7vih9ghd7")))

(define-public crate-thiserror-ext-0.2.0 (c (n "thiserror-ext") (v "0.2.0") (d (list (d (n "anyhow") (r "^1") (d #t) (k 2)) (d (n "expect-test") (r "^1") (d #t) (k 2)) (d (n "sealed_test") (r "^1") (d #t) (k 2)) (d (n "thiserror") (r "^1") (d #t) (k 0)) (d (n "thiserror-ext-derive") (r "=0.2.0") (d #t) (k 0)))) (h "1pviklpk6azcnv700j8s6y94rvyj7v8016s7j4d6wwasnq4gsdda") (f (quote (("backtrace" "thiserror-ext-derive/backtrace"))))))

