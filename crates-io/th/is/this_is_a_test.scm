(define-module (crates-io th is this_is_a_test) #:use-module (crates-io))

(define-public crate-this_is_a_test-0.0.0 (c (n "this_is_a_test") (v "0.0.0") (h "1vpa7gmawdxiz0xa91pwzcx0m6a2xgsnc3iivxixzj2ry3fcfzvg") (y #t)))

(define-public crate-this_is_a_test-0.0.1 (c (n "this_is_a_test") (v "0.0.1") (h "0kiw24m212xg9k3js55p5gxiddn4y0g5qwy4nk9xnyl742aypv7s") (y #t)))

(define-public crate-this_is_a_test-0.0.2 (c (n "this_is_a_test") (v "0.0.2") (h "16qag9gp60w8561csldxdz9ynygk0d4q9asp0bflsrlgwrgplk8f") (y #t)))

(define-public crate-this_is_a_test-0.0.3 (c (n "this_is_a_test") (v "0.0.3") (h "0ldaqqy4l82rkm09aba70svsknnbbq2fx4kxrrfp0gjyvzj2bpfw")))

