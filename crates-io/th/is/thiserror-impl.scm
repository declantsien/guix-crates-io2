(define-module (crates-io th is thiserror-impl) #:use-module (crates-io))

(define-public crate-thiserror-impl-1.0.0 (c (n "thiserror-impl") (v "1.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1jgagdpgfy25ac6gyn7incb648janyza8s8yhhfnvmib1md3zmg9")))

(define-public crate-thiserror-impl-1.0.2 (c (n "thiserror-impl") (v "1.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "1aarcl0alfmxig20giffw1s5kfsmkhd61038v1gk8yx06xc23iid")))

(define-public crate-thiserror-impl-1.0.3 (c (n "thiserror-impl") (v "1.0.3") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "06ydvkxzyhdkpdji7j27b6ac78xwxwx2s2xc8my63gmrsjj6hvc3")))

(define-public crate-thiserror-impl-1.0.4 (c (n "thiserror-impl") (v "1.0.4") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "08frnvk8vsvda5mq9dpg1kl26g9ickknpb21ll7nb475k5zad395")))

(define-public crate-thiserror-impl-1.0.5 (c (n "thiserror-impl") (v "1.0.5") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "0z82r50z27ahwlmc9frq2lgcdbwldl1za9bakll59awal05rq1i4")))

(define-public crate-thiserror-impl-1.0.6 (c (n "thiserror-impl") (v "1.0.6") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (d #t) (k 0)))) (h "15w5ksaxg189d9p7mbapia8wgdxzx1s4bmbsdd2zqj4w1n0qvfj5")))

(define-public crate-thiserror-impl-1.0.7 (c (n "thiserror-impl") (v "1.0.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1nzl8ik0kbp6jspkjm9v1irz5v5ga0wffmxhpnar0qspzszhhcqg")))

(define-public crate-thiserror-impl-1.0.8 (c (n "thiserror-impl") (v "1.0.8") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "01zwadcm0vhavkc2rzv1prhqim6j1bmjspf7kj7fmjxlr2w3vhbm")))

(define-public crate-thiserror-impl-1.0.9 (c (n "thiserror-impl") (v "1.0.9") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1vxzhb98pm5yrq9mmcz50bgpml35iyx7lwjgi4v89sq7ag92abpb")))

(define-public crate-thiserror-impl-1.0.10 (c (n "thiserror-impl") (v "1.0.10") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "19z5vkmmxpkq36nr76agz149k9zgwgf9nc2qzd2fsl501kjx5r2p")))

(define-public crate-thiserror-impl-1.0.11 (c (n "thiserror-impl") (v "1.0.11") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1x1yrnbyyfx4d8gfc5r61w2hkgk0z70gp5751f2a1da4phgixdd7")))

(define-public crate-thiserror-impl-1.0.12 (c (n "thiserror-impl") (v "1.0.12") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1z9f3n24c1cg41gfy1vxfb03791z8r9ay82as5rml7bsjamcngls")))

(define-public crate-thiserror-impl-1.0.13 (c (n "thiserror-impl") (v "1.0.13") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0cxjad9qhdpj0wwyl5wcd2vm96rfsaw0xn1v6k9jv8ws9jx8aaxf")))

(define-public crate-thiserror-impl-1.0.14 (c (n "thiserror-impl") (v "1.0.14") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1zd883wmdqnql22rfjy145y1ahx00wg40r4gl90vwrnm87gn4wr2")))

(define-public crate-thiserror-impl-1.0.15 (c (n "thiserror-impl") (v "1.0.15") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1p8hjdj4lhsprh7zl4d0c8y06387k3dhs9cidwk1axrv2642k5ya")))

(define-public crate-thiserror-impl-1.0.16 (c (n "thiserror-impl") (v "1.0.16") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0s8jaa7i6ixqsllzx50dc917h7pa8s4pcszc83c2yimarb0y0d1z")))

(define-public crate-thiserror-impl-1.0.17 (c (n "thiserror-impl") (v "1.0.17") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1namrg2d3s8955c93s436474f8qjw2ic96s2kc43962v4a8i0g76")))

(define-public crate-thiserror-impl-1.0.18 (c (n "thiserror-impl") (v "1.0.18") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0gapi5nz91f0qs640j866ljvld1knyyx7gfg5vi2rkb9rp8xp0db")))

(define-public crate-thiserror-impl-1.0.19 (c (n "thiserror-impl") (v "1.0.19") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "0ygllwcnbglqx156fv04n5sa0c2hbfk6aqlhizhwv61gdh484dc9")))

(define-public crate-thiserror-impl-1.0.20 (c (n "thiserror-impl") (v "1.0.20") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "14qphjwa68sfjk3404iwv6hh8kvk6vmcwan9589sqqrhyw9gr05x")))

(define-public crate-thiserror-impl-1.0.21 (c (n "thiserror-impl") (v "1.0.21") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.11") (d #t) (k 0)))) (h "1ay06mahcnmz1l58n9mxp06d4siapsll17wrjcs6qy42c9xl9qna")))

(define-public crate-thiserror-impl-1.0.22 (c (n "thiserror-impl") (v "1.0.22") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0mnx51374c69l1w7gh98prn2wzm2yvmlll4ms567a42vx0ihz8lv")))

(define-public crate-thiserror-impl-1.0.23 (c (n "thiserror-impl") (v "1.0.23") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1law4wqpc36hxs4vcgc1pjjniy4l6xn7kwvf0k886xf2mqn3mrwv")))

(define-public crate-thiserror-impl-1.0.24 (c (n "thiserror-impl") (v "1.0.24") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1h7kh6rr4vsm79dmv8qk8drhh2if3zyxc1lqa921l96q22b1hrbp")))

(define-public crate-thiserror-impl-1.0.25 (c (n "thiserror-impl") (v "1.0.25") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "139i3bxidyncjd1sphkn4c577nkba8lzmphhr9gb26xz1y67cdla")))

(define-public crate-thiserror-impl-1.0.26 (c (n "thiserror-impl") (v "1.0.26") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0ia72qiynlws5avb8f1xqlazp4g6bqgzjbwy5vs6nyg7myh6j386")))

(define-public crate-thiserror-impl-1.0.27 (c (n "thiserror-impl") (v "1.0.27") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0kr2j03ak7w0n6m5y2i2ga7scbpsi0j3341fsczmfcyymzxmbycb")))

(define-public crate-thiserror-impl-1.0.28 (c (n "thiserror-impl") (v "1.0.28") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0wcfsqdy9myvml8kyphknd72x0yfgnzhkm7210vcvx8ihqi88f7s")))

(define-public crate-thiserror-impl-1.0.29 (c (n "thiserror-impl") (v "1.0.29") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0k290k93cs3gfmfjfdz23srx3rl0grj3lxh0hhjxxs3q5k657mds")))

(define-public crate-thiserror-impl-1.0.30 (c (n "thiserror-impl") (v "1.0.30") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0jviwmvx6wzawsj6c9msic7h419wmsbjagl9dzhpydkzc8zzscma") (r "1.31")))

(define-public crate-thiserror-impl-1.0.31 (c (n "thiserror-impl") (v "1.0.31") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "16npm1s1cv9kxkk7is7blnayfnf41hny46gqprc4c916ws4vr5h3") (r "1.31")))

(define-public crate-thiserror-impl-1.0.32 (c (n "thiserror-impl") (v "1.0.32") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "08bc1x36izsg7ps5s9yrpj80mf0av2jlbcgirm4h2zjhaidzrfhj") (r "1.31")))

(define-public crate-thiserror-impl-1.0.33 (c (n "thiserror-impl") (v "1.0.33") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "028cmrmfb2n5yyvrrypdf8mj4x8y2g17v4gl2sdc85lff07yjlf2") (r "1.31")))

(define-public crate-thiserror-impl-1.0.34 (c (n "thiserror-impl") (v "1.0.34") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "11vl82hy65b18fpigr39xnbs2rf6f3qg658hj9ffhbb4hccmkwp8") (r "1.31")))

(define-public crate-thiserror-impl-1.0.35 (c (n "thiserror-impl") (v "1.0.35") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "10zpsdnqwakckqa9piwf1rjmq6f4bql74hbj3s02vasf3fcn7d7q") (r "1.31")))

(define-public crate-thiserror-impl-1.0.36 (c (n "thiserror-impl") (v "1.0.36") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0hzblzqr8jlhgd037a5avds2624hbxvb7pbkivn6zmn8sdh1i29s") (r "1.31")))

(define-public crate-thiserror-impl-1.0.37 (c (n "thiserror-impl") (v "1.0.37") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1fydmpksd14x1mkc24zas01qjssz8q43sbn2ywl6n527dda1fbcq") (r "1.31")))

(define-public crate-thiserror-impl-1.0.38 (c (n "thiserror-impl") (v "1.0.38") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0vzkcjqkzzgrwwby92xvnbp11a8d70b1gkybm0zx1r458spjgcqz") (r "1.31")))

(define-public crate-thiserror-impl-1.0.39 (c (n "thiserror-impl") (v "1.0.39") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0zi6kaf9s9isyllcamczziwz7gdkp4jwmg0a571kh35gj0pd882l") (r "1.31")))

(define-public crate-thiserror-impl-1.0.40 (c (n "thiserror-impl") (v "1.0.40") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (d #t) (k 0)))) (h "17sn41kyimc6s983aypkk6a45pcyrkbkvrw6rp407n5hqm16ligr") (r "1.56")))

(define-public crate-thiserror-impl-1.0.41 (c (n "thiserror-impl") (v "1.0.41") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0nbc6jjhg2jfm767qp4rwy2a56gkvsd0cjg5y2jddi019csjhjfi") (r "1.56")))

(define-public crate-thiserror-impl-1.0.42 (c (n "thiserror-impl") (v "1.0.42") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "14g96zpfc0mygppzy4iv2gy6winjcaqcsb9va0clyv6i8b60sppq") (r "1.56")))

(define-public crate-thiserror-impl-1.0.43 (c (n "thiserror-impl") (v "1.0.43") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "13qyfz23vq6dydg1jazw6bg5i86zsj6wwdvmg0kv7lwkg4ny2gs6") (r "1.56")))

(define-public crate-thiserror-impl-1.0.44 (c (n "thiserror-impl") (v "1.0.44") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "15nwh6qfwxlwimgij1p6ajb377p4rlvvc6sx7amiz11h959rh089") (r "1.56")))

(define-public crate-thiserror-impl-1.0.45 (c (n "thiserror-impl") (v "1.0.45") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1czl3jy42v8aya2n9mk0q83x1k4dkngn4227lsgf3ixvrjniyyvx") (r "1.56")))

(define-public crate-thiserror-impl-1.0.46 (c (n "thiserror-impl") (v "1.0.46") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0idlkyhy544nf6dxca3xhrkpq6myg5fc3y0l9zqy8k94scb84wpi") (r "1.56")))

(define-public crate-thiserror-impl-1.0.47 (c (n "thiserror-impl") (v "1.0.47") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "16z1irxb45l011af53diap97x44dixnbp60v9g6pvarrdssj7dkb") (r "1.56")))

(define-public crate-thiserror-impl-1.0.48 (c (n "thiserror-impl") (v "1.0.48") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "0dcx46hn5gb8viyc4q009x8jq6rwcb8d2s3ynx4s5j3cwv52x4j9") (r "1.56")))

(define-public crate-thiserror-impl-1.0.49 (c (n "thiserror-impl") (v "1.0.49") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1k643ga9sbqpcb873n299n3zjzc4srfakyv98xwqi4ly0412yw8h") (r "1.56")))

(define-public crate-thiserror-impl-1.0.50 (c (n "thiserror-impl") (v "1.0.50") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1f0lmam4765sfnwr4b1n00y14vxh10g0311mkk0adr80pi02wsr6") (r "1.56")))

(define-public crate-thiserror-impl-1.0.51 (c (n "thiserror-impl") (v "1.0.51") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1ps9ylhlk2vn19fv3cxp40j3wcg1xmb117g2z2fbf4vmg2bj4x01") (r "1.56")))

(define-public crate-thiserror-impl-1.0.52 (c (n "thiserror-impl") (v "1.0.52") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1lwiha4c2772w4aifvbgx7hp82yqcxz0l9a32im8lmnnjjsykyz7") (r "1.56")))

(define-public crate-thiserror-impl-1.0.53 (c (n "thiserror-impl") (v "1.0.53") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "06cywbwmsfs4l6x8wcrjhvb813jc4cj6zbiqdz6yl2nf9j14mkrx") (r "1.56")))

(define-public crate-thiserror-impl-1.0.54 (c (n "thiserror-impl") (v "1.0.54") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "02s1vmdcwfldr42mslh0fflq8cmdc71h7saaa087pjmpkdd14psr") (r "1.56")))

(define-public crate-thiserror-impl-1.0.55 (c (n "thiserror-impl") (v "1.0.55") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "10fmnqvvy5xnf1wfh5gbwvbv7z51734hgrg90dqqvqrbbdl2d016") (r "1.56")))

(define-public crate-thiserror-impl-1.0.56 (c (n "thiserror-impl") (v "1.0.56") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0w9ldp8fa574ilz4dn7y7scpcq66vdjy59qal8qdpwsh7faal3zs") (r "1.56")))

(define-public crate-thiserror-impl-1.0.57 (c (n "thiserror-impl") (v "1.0.57") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "108wacybfa95akidi07ahabfwkl0sfj3srp67np5sdzgbckcnlx9") (r "1.56")))

(define-public crate-thiserror-impl-1.0.58 (c (n "thiserror-impl") (v "1.0.58") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "1xylyqcb8rv5yh2yf97hg4n4kg27qccc0ijafr1zqklrhahkn7y6") (r "1.56")))

(define-public crate-thiserror-impl-1.0.59 (c (n "thiserror-impl") (v "1.0.59") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0rlb2jvpxx5xkpk52yh6gixlw0d5dx5343k8yddlr2smblxl3kfi") (r "1.56")))

(define-public crate-thiserror-impl-1.0.60 (c (n "thiserror-impl") (v "1.0.60") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0945q2hk1rqdzjz2zqakxbddwm4h26k5c0wdncdarhvfq10h0iz2") (r "1.56")))

(define-public crate-thiserror-impl-1.0.61 (c (n "thiserror-impl") (v "1.0.61") (d (list (d (n "proc-macro2") (r "^1.0.74") (d #t) (k 0)) (d (n "quote") (r "^1.0.35") (d #t) (k 0)) (d (n "syn") (r "^2.0.46") (d #t) (k 0)))) (h "0cvm37hp0kbcyk1xac1z0chpbd9pbn2g456iyid6sah0a113ihs6") (r "1.56")))

