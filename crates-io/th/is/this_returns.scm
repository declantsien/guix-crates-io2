(define-module (crates-io th is this_returns) #:use-module (crates-io))

(define-public crate-this_returns-0.1.0 (c (n "this_returns") (v "0.1.0") (d (list (d (n "fake") (r "^2.8") (f (quote ("derive"))) (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "serde") (r "^1.0.186") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.105") (d #t) (k 0)) (d (n "syn") (r "^2.0.29") (d #t) (k 0)))) (h "1klcz3npv51z9n8cxmpf6pi10as76mizay5pghync495y5khmdnv") (y #t)))

