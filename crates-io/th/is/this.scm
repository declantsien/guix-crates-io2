(define-module (crates-io th is this) #:use-module (crates-io))

(define-public crate-this-0.1.0 (c (n "this") (v "0.1.0") (h "15bclqnd8abi31jsglix4cal56g8qf32xq3z54mjv5sn6aw9b0gf")))

(define-public crate-this-0.2.0 (c (n "this") (v "0.2.0") (h "18xhpgsp0awfmbz08xmywqsg1x5d5cwx68kclyb1pc2f71c0m0zf")))

(define-public crate-this-0.3.0 (c (n "this") (v "0.3.0") (h "066m53ai7fib908q948bf0sj4ym72p8w9xz84r22zz6dy9p52i70")))

