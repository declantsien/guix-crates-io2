(define-module (crates-io th is thiserror) #:use-module (crates-io))

(define-public crate-thiserror-1.0.0 (c (n "thiserror") (v "1.0.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "^1.0") (d #t) (k 0)))) (h "07cmlfqwa65dqfgmxf00zfj67lfrmifal5nhlpggz5j12q0xncnh")))

(define-public crate-thiserror-1.0.1 (c (n "thiserror") (v "1.0.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.0") (d #t) (k 0)))) (h "0c67zyi4khqfnfcvplypl6d888wnlf788fbyxklmk5gr563p59kn")))

(define-public crate-thiserror-1.0.2 (c (n "thiserror") (v "1.0.2") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.2") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "08x7i1pqw9z1xcg96w33dnm93vind5ilhgfkj41xzdv9ld1ph1kr")))

(define-public crate-thiserror-1.0.3 (c (n "thiserror") (v "1.0.3") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.3") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "023ni965930n7gjjq1q158s7w5myxcbsrvdiz65dyzswhwvwjaad")))

(define-public crate-thiserror-1.0.4 (c (n "thiserror") (v "1.0.4") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.4") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1g119czg5sg2sifmsg32p1dx3pk3fdwgfj1d14kkldn31zx4iqcz")))

(define-public crate-thiserror-1.0.5 (c (n "thiserror") (v "1.0.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.5") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0928apqg4gnz7hnj9hv8x3bdag02znkbwnblcwg3nmvyfgzn5yzr")))

(define-public crate-thiserror-1.0.6 (c (n "thiserror") (v "1.0.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.6") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "16h9r9yj3458c23dihxp1cyhdq0n4ni9hq7zryvcf8z3q1g30syc")))

(define-public crate-thiserror-1.0.7 (c (n "thiserror") (v "1.0.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.7") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "1dg6qd4gmim9p81d5am705dd2m8vr59z9rwv0ymg618gpa97kqx6")))

(define-public crate-thiserror-1.0.8 (c (n "thiserror") (v "1.0.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.8") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0cmpdrbpzf9hknhndmhpi44jsxaymf66hndylqsw7jmilqsx0cpl")))

(define-public crate-thiserror-1.0.9 (c (n "thiserror") (v "1.0.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.9") (d #t) (k 0)) (d (n "trybuild") (r "^1.0") (d #t) (k 2)))) (h "0zlp3kzjssl1ndqmn7cipqnyggq4851ghhqj4bfc4fxk2hc7sdbg")))

(define-public crate-thiserror-1.0.10 (c (n "thiserror") (v "1.0.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.10") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1b8srynh2gxsf572jg8wawd32qv7sjinxkicn4r498cc07yq8mi0")))

(define-public crate-thiserror-1.0.11 (c (n "thiserror") (v "1.0.11") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.11") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1nzlpqv6bnza6icvm8v7znb2l14yhw1w12wygil4rav7cy7by57f")))

(define-public crate-thiserror-1.0.12 (c (n "thiserror") (v "1.0.12") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.12") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1bggklq8giwx8wh6w7dkmpnv8wm3b2qqf5x9j36b1f15fqb0z316")))

(define-public crate-thiserror-1.0.13 (c (n "thiserror") (v "1.0.13") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.13") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1gs91h374iv63961gs3naifbddk2lzdl0p5s2bzkwnz7qk8iywg3")))

(define-public crate-thiserror-1.0.14 (c (n "thiserror") (v "1.0.14") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.14") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1bjlxj8m06by19mh5clk8bhjahz7y8wwg5fvmc4mja912b30smzh")))

(define-public crate-thiserror-1.0.15 (c (n "thiserror") (v "1.0.15") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.15") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "04j0j9kzhfqzjqavmm2bpqd90v5j1jxvcyr5mc042438zz9d7csl")))

(define-public crate-thiserror-1.0.16 (c (n "thiserror") (v "1.0.16") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.16") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0q0v144z9w3nbhczpjj44nma2nql5wabzizbirb0s3yx9ap1sani")))

(define-public crate-thiserror-1.0.17 (c (n "thiserror") (v "1.0.17") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.17") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1h92kcgfy047bpq66gbzi4sg855bf8kxdq5435jr32k18zs5yzj6")))

(define-public crate-thiserror-1.0.18 (c (n "thiserror") (v "1.0.18") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.18") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "19wc9vjp7xb38cbrwvs6c5crakd6bag5n2w5fy2gdd2hd4fqjxjr")))

(define-public crate-thiserror-1.0.19 (c (n "thiserror") (v "1.0.19") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "= 1.0.19") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0i63pw7qanxhyajv0nh8x5kgifq47f115yi9s5fmj05dcmlr4gxi")))

(define-public crate-thiserror-1.0.20 (c (n "thiserror") (v "1.0.20") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.20") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "020d7pfq7vg2iw1bbilnyq764zy35ncg2syn9a7vgk6qriqd1zbx")))

(define-public crate-thiserror-1.0.21 (c (n "thiserror") (v "1.0.21") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.21") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hlvhsa1bf8yrynpmkw0154rsr2zkcvbims0kbz2029flbzk90ii")))

(define-public crate-thiserror-1.0.22 (c (n "thiserror") (v "1.0.22") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.22") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0gp5wp7izpv9rdvq035ajbxcl3g0vck61pg9y6mfsvk1hi5y76hf")))

(define-public crate-thiserror-1.0.23 (c (n "thiserror") (v "1.0.23") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.23") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0imiv97kdlba0r0ld6mlizfmw5rpmfzhvk7xw8l8k35zd9n63k3n")))

(define-public crate-thiserror-1.0.24 (c (n "thiserror") (v "1.0.24") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.24") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "13m99wjikivkkwd209fgxhdprjxj17s39ldfvn1l8k89jxasdx70")))

(define-public crate-thiserror-1.0.25 (c (n "thiserror") (v "1.0.25") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.25") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ip9j8riar3xffp261yls4phpasz768xhnafxdz4qlargx2pcvzs")))

(define-public crate-thiserror-1.0.26 (c (n "thiserror") (v "1.0.26") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.26") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1qmz542pq4wmz3p0s4kavsqv09h0x99klkf3k33ydjy1x97rw4ck")))

(define-public crate-thiserror-1.0.27 (c (n "thiserror") (v "1.0.27") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.27") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "0w0wvcg79znjhd4c992hyfmc23lasby16y0xgvhk9vlqfkwikhz1")))

(define-public crate-thiserror-1.0.28 (c (n "thiserror") (v "1.0.28") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.28") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "1v7n0id0lzws1d6vcvj34l970vpv3pf1lsfrqy561y9xwqq54g98")))

(define-public crate-thiserror-1.0.29 (c (n "thiserror") (v "1.0.29") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.29") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.19") (f (quote ("diff"))) (d #t) (k 2)))) (h "124v54fr3b3i230naz4j04l401dnf2fb0d1g5fg3d0rd9c3clbk0")))

(define-public crate-thiserror-1.0.30 (c (n "thiserror") (v "1.0.30") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.30") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "05y4wm29ck8flwq5k1q6nhwh00a3b30cz3xr0qvnbwad5vjsnjw5") (r "1.31")))

(define-public crate-thiserror-1.0.31 (c (n "thiserror") (v "1.0.31") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.31") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "16h6d602kmjilbfw28zma22wnh03klqba82n4rv7zlkk4girz0mx") (r "1.31")))

(define-public crate-thiserror-1.0.32 (c (n "thiserror") (v "1.0.32") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.32") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "154ry4l3h9nv3ikb6l9y5yxndrr0p7krpizl641dqjkngxmmixpm") (r "1.31")))

(define-public crate-thiserror-1.0.33 (c (n "thiserror") (v "1.0.33") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.33") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0mwa7kvqp0lzk5bcv53yrna9ln31hyhvfzdc6la6aic7j6d562ix") (r "1.31")))

(define-public crate-thiserror-1.0.34 (c (n "thiserror") (v "1.0.34") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.34") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0lp2ask4jb0kldp9c8ak8772rgp7ci5ap78swg9afsqhkp50a6wc") (r "1.31")))

(define-public crate-thiserror-1.0.35 (c (n "thiserror") (v "1.0.35") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.35") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "11bcx8xp6wdl10yya60xa216f4gnv0galrbp21la5bhm8s3rhgy5") (r "1.31")))

(define-public crate-thiserror-1.0.36 (c (n "thiserror") (v "1.0.36") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.36") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "08mi1cbar01x82pchwsf3iblvh4drhbvdlvwj3kz13ls9f6cp68a") (r "1.31")))

(define-public crate-thiserror-1.0.37 (c (n "thiserror") (v "1.0.37") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.37") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.49") (f (quote ("diff"))) (d #t) (k 2)))) (h "0gky83x4i87gd87w3fknnp920wvk9yycp7dgkf5h3jg364vb7phh") (r "1.31")))

(define-public crate-thiserror-1.0.38 (c (n "thiserror") (v "1.0.38") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.38") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1l7yh18iqcr2jnl6qjx3ywvhny98cvda3biwc334ap3xm65d373a") (r "1.31")))

(define-public crate-thiserror-1.0.39 (c (n "thiserror") (v "1.0.39") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.39") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "171dbc6ln19hig5h2hcczcb6m9ldvw3ji24pca2nsm0hnmnh3ax5") (r "1.31")))

(define-public crate-thiserror-1.0.40 (c (n "thiserror") (v "1.0.40") (d (list (d (n "anyhow") (r "^1.0.65") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0") (d #t) (k 2)) (d (n "rustversion") (r "^1.0") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.40") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.66") (f (quote ("diff"))) (d #t) (k 2)))) (h "1b7bdhriasdsr99y39d50jz995xaz9sw3hsbb6z9kp6q9cqrm34p") (r "1.56")))

(define-public crate-thiserror-1.0.41 (c (n "thiserror") (v "1.0.41") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.41") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "00kqnfrj466w3mpk7h4537lzr5q9gbqaghgrwkd3zvw7jfx68sn1") (r "1.56")))

(define-public crate-thiserror-1.0.42 (c (n "thiserror") (v "1.0.42") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.42") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0iwrz1ccizw7m5ck5ggp3998ar90x6vcisvnrii0iv7g536izvdq") (r "1.56")))

(define-public crate-thiserror-1.0.43 (c (n "thiserror") (v "1.0.43") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.43") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0hlcsnjb0rgkqb8imiv86i7jcmad9l14zpx6iwsclhqijywcapx3") (r "1.56")))

(define-public crate-thiserror-1.0.44 (c (n "thiserror") (v "1.0.44") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.44") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "143zzmardcq447va2pw09iq9rajvr48v340riljghf84iah40431") (r "1.56")))

(define-public crate-thiserror-1.0.45 (c (n "thiserror") (v "1.0.45") (d (list (d (n "anyhow") (r "^1.0.71") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.45") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "183ypcgy1ba40p1fv430x9rbdha4dlbryb7yxnz8jah9jxj29pfy") (r "1.56")))

(define-public crate-thiserror-1.0.46 (c (n "thiserror") (v "1.0.46") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.46") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0p77ad1cpdd2j5m9miy0dam45fn6qgd95s6msd13y00smr97j86r") (r "1.56")))

(define-public crate-thiserror-1.0.47 (c (n "thiserror") (v "1.0.47") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.47") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "13wdsrdyrq6x3rcydvxlx4mxck0c5v3mz1dj8zp7xhdg63n05a4p") (r "1.56")))

(define-public crate-thiserror-1.0.48 (c (n "thiserror") (v "1.0.48") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.48") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1dw90plisa09q5xafkhmagzhsafwq2lhvl4dh9z6lrla1ds7lvcx") (r "1.56")))

(define-public crate-thiserror-1.0.49 (c (n "thiserror") (v "1.0.49") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.49") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1m3yalbcx89bx397igc1ckypw8hiwq9jbzc56pgazrzdsz3fhxqi") (r "1.56")))

(define-public crate-thiserror-1.0.50 (c (n "thiserror") (v "1.0.50") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.50") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ll2sfbrxks8jja161zh1pgm3yssr7aawdmaa2xmcwcsbh7j39zr") (r "1.56")))

(define-public crate-thiserror-1.0.51 (c (n "thiserror") (v "1.0.51") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.51") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1drvyim21w5sga3izvnvivrdp06l2c24xwbhp0vg1mhn2iz2277i") (r "1.56")))

(define-public crate-thiserror-1.0.52 (c (n "thiserror") (v "1.0.52") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.52") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0kb1lkilc107qj34l5irf3j5axx1wb48351fdd90lb5h8vcqz943") (r "1.56")))

(define-public crate-thiserror-1.0.53 (c (n "thiserror") (v "1.0.53") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.53") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "02fczinyqdp28gl6i7zsag577qiarw9bpp8kannhib9vfq25kkdj") (r "1.56")))

(define-public crate-thiserror-1.0.54 (c (n "thiserror") (v "1.0.54") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.54") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0x6pv0yl7pfwwsvj5di9znilzfv7pfxjl4d6vbv5sj4zvd24rmb4") (r "1.56")))

(define-public crate-thiserror-1.0.55 (c (n "thiserror") (v "1.0.55") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.55") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ljk14y8kbd89srx577y01wq71mszhqz0zr8nhcj4ab515my4gbf") (r "1.56")))

(define-public crate-thiserror-1.0.56 (c (n "thiserror") (v "1.0.56") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.56") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1b9hnzngjan4d89zjs16i01bcpcnvdwklyh73lj16xk28p37hhym") (r "1.56")))

(define-public crate-thiserror-1.0.57 (c (n "thiserror") (v "1.0.57") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.57") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0jxj8y5kxgq6lcizzssnyz6a9xvssqnayp0953r7b5yjiszbqi8y") (r "1.56")))

(define-public crate-thiserror-1.0.58 (c (n "thiserror") (v "1.0.58") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.58") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "15rjgd1abi2mzjgzfhrvmsxf9h65n95h6sp8f4s52q4i00wqhih3") (r "1.56")))

(define-public crate-thiserror-1.0.59 (c (n "thiserror") (v "1.0.59") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.59") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "1ai39ga7d50jny5zzqv1zzgmc81mfb65asmfqfgz4ygzig86l4ph") (r "1.56")))

(define-public crate-thiserror-1.0.60 (c (n "thiserror") (v "1.0.60") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.60") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "067wi7pb1zn9jhhk82w0ppmvjwa00nwkp4m9j77rvpaqra1r17jp") (r "1.56")))

(define-public crate-thiserror-1.0.61 (c (n "thiserror") (v "1.0.61") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "thiserror-impl") (r "=1.0.61") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "028prh962l16cmjivwb1g9xalbpqip0305zhq006mg74dc6whin5") (r "1.56")))

