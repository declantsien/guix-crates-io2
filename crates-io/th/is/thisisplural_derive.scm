(define-module (crates-io th is thisisplural_derive) #:use-module (crates-io))

(define-public crate-thisisplural_derive-0.1.0 (c (n "thisisplural_derive") (v "0.1.0") (d (list (d (n "pretty_assertions") (r "^0.7") (d #t) (k 2)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "12j2lcvxwjjp4q1bpgi7rf3jr33sg2iq6yh7bsc2kifmfls2xagr") (y #t)))

