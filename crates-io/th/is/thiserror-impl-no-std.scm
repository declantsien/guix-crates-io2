(define-module (crates-io th is thiserror-impl-no-std) #:use-module (crates-io))

(define-public crate-thiserror-impl-no-std-2.0.0 (c (n "thiserror-impl-no-std") (v "2.0.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0ycfq8gsia7k54mb563vy9svkkz38km9kn1svxvy8xj0lfzncyvw") (f (quote (("std")))) (y #t) (r "1.31")))

(define-public crate-thiserror-impl-no-std-2.0.1 (c (n "thiserror-impl-no-std") (v "2.0.1") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "1j40783rvc6zxqq6w120ffcyi4c4dz6h2k1n3ln0jngh4rqc4161") (f (quote (("std")))) (r "1.31")))

(define-public crate-thiserror-impl-no-std-2.0.2 (c (n "thiserror-impl-no-std") (v "2.0.2") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0.45") (d #t) (k 0)))) (h "0n4p4ir5lqa3g3hd6fhs866zpsq4p78achmlq9nvl6dm924k3rjq") (f (quote (("std")))) (r "1.31")))

