(define-module (crates-io th ro throttle-timer) #:use-module (crates-io))

(define-public crate-throttle-timer-0.0.1 (c (n "throttle-timer") (v "0.0.1") (h "0gp44qpkd8fkcwx0i11rfqz7s8m20phm6wplj74m5fdwzfzkbk8x")))

(define-public crate-throttle-timer-0.0.2 (c (n "throttle-timer") (v "0.0.2") (h "0gb1rhmpbjsrjpdgsjg6xscadblcn018wlvqjnml1zy04x1agbzw")))

(define-public crate-throttle-timer-0.0.4 (c (n "throttle-timer") (v "0.0.4") (h "1a061c041rpn94443ks32609nzd8cla2zvmysjxiiaqa5nn8aqjz")))

(define-public crate-throttle-timer-0.0.5 (c (n "throttle-timer") (v "0.0.5") (h "1yz61rin7zi6m93py0dhzgdx3avvmmr17vy6i86syfhghyh4xlg2")))

(define-public crate-throttle-timer-0.0.6 (c (n "throttle-timer") (v "0.0.6") (h "1j0h28jjalv6d7blnar1ic6hqc9vv8jby7wv8rm60hil5b00r90r")))

(define-public crate-throttle-timer-1.0.0 (c (n "throttle-timer") (v "1.0.0") (h "1das4ad1sa6cys5zg82012by606ymrav265r4kgfdxl9166ais8l")))

