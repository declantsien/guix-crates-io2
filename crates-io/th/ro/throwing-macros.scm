(define-module (crates-io th ro throwing-macros) #:use-module (crates-io))

(define-public crate-throwing-macros-0.1.0 (c (n "throwing-macros") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (f (quote ("full"))) (d #t) (k 0)))) (h "0jg1rrwk93yrzkyaaihz3hz6sa969yb00brbrzqpmbijvv7spbc4") (r "1.65")))

(define-public crate-throwing-macros-0.1.1 (c (n "throwing-macros") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^1.0.66") (d #t) (k 0)) (d (n "quote") (r "^1.0.33") (d #t) (k 0)) (d (n "syn") (r "^2.0.32") (f (quote ("full"))) (d #t) (k 0)))) (h "0jp6fjymwdrafpn6v1rh14zgd0b4a5aw5jcklz17cc03spjizjz4") (r "1.65")))

