(define-module (crates-io th ro throttler) #:use-module (crates-io))

(define-public crate-throttler-0.1.0 (c (n "throttler") (v "0.1.0") (h "0fi8qs676ws5aaikx23ig93jkqc83hh836p6mhq5cn7ddc1npv3f")))

(define-public crate-throttler-0.1.1 (c (n "throttler") (v "0.1.1") (h "0lmb04gxbwjfjpqkphg7vvfk76pxrnnnbmg6p4qjakchk83h95jd")))

(define-public crate-throttler-0.1.2 (c (n "throttler") (v "0.1.2") (h "15f8mhrk4qw0n18jx9xkjlhm84xq2slnhl2l915khjbng75kvwh2")))

(define-public crate-throttler-0.1.3 (c (n "throttler") (v "0.1.3") (h "06n44m59lc3qd9j1xf0s0glw1py85191ns092filfj2kf7irwva6")))

