(define-module (crates-io th ro throttle2) #:use-module (crates-io))

(define-public crate-throttle2-0.1.0 (c (n "throttle2") (v "0.1.0") (h "1da1qv8mi8x2ix6z1dd8wk4ssrj0zpmg3c8qmc4qjwjldy19qrav")))

(define-public crate-throttle2-0.1.1 (c (n "throttle2") (v "0.1.1") (h "1lcn6zgl732ipblk49ix8sa8dgha2pammjcyvsmh0y9mnb9yyf68")))

