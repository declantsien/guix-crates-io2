(define-module (crates-io th ro throbber) #:use-module (crates-io))

(define-public crate-throbber-0.1.0 (c (n "throbber") (v "0.1.0") (h "1pdbm0fmrdza6vjrpfl16p7n2s7c3qybr7nc8ndb1g7mwwaimd58")))

(define-public crate-throbber-0.1.1 (c (n "throbber") (v "0.1.1") (h "0a6k6fwslpnwkfx1vjyh444k5iv3r8hg1v89vgn5p1zpsznyv8br")))

(define-public crate-throbber-0.1.2 (c (n "throbber") (v "0.1.2") (h "0q4n00vryjby21hx7whg8pjm4qwsd46052pb9sv2jmwxlrj4j1z4")))

(define-public crate-throbber-0.1.3 (c (n "throbber") (v "0.1.3") (h "1psxqs728038czkpq0x02hlns4wqd7c8rxa66vnbym5dqcc4vlfj")))

(define-public crate-throbber-0.1.4 (c (n "throbber") (v "0.1.4") (h "018g2z98d9lvfjarbzdn949s4qdj8qnx4r77y8d46yf01gf9qrzs")))

