(define-module (crates-io th ro throw) #:use-module (crates-io))

(define-public crate-throw-0.1.0 (c (n "throw") (v "0.1.0") (h "1kcagx5qxms7qhr97vgp05s0r2gjpba5lsx0hbkd793vqa8r1pfm")))

(define-public crate-throw-0.1.1 (c (n "throw") (v "0.1.1") (h "0i4w0n0lbjca5ms0p777180rqwcljirhzchpdazkiqj3la8pkipy")))

(define-public crate-throw-0.1.2 (c (n "throw") (v "0.1.2") (d (list (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "0p0q57cgf61b2b4zc9xnjidnpzyyyjihbr6p2a0c0dbcanldssm2")))

(define-public crate-throw-0.1.3 (c (n "throw") (v "0.1.3") (d (list (d (n "regex") (r "^0.2") (d #t) (k 2)))) (h "01qz3c47q5qji7d98avzwssi9ksc79yaimmwrz37mvv5fkn8py77") (f (quote (("unlimited-points") ("std") ("default" "std" "unlimited-points"))))))

(define-public crate-throw-0.1.4 (c (n "throw") (v "0.1.4") (d (list (d (n "regex") (r "^0.2") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "087azphzilycfrwl94isbx3l8jr22mn6cn712dsbrfb6z7wdlpid") (f (quote (("unlimited-points") ("std") ("serde-1-std" "serde" "serde_derive" "serde/std") ("serde-1" "serde" "serde_derive" "serde/alloc") ("default" "std" "unlimited-points"))))))

(define-public crate-throw-0.1.5 (c (n "throw") (v "0.1.5") (d (list (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "1c0q16mjinknr8qg5qywmfx82vwg6hhcyggm6iby1mhq71i6sb8j") (f (quote (("unlimited-points") ("std") ("serde-1-std" "serde" "serde_derive" "serde/std") ("serde-1" "serde" "serde_derive" "serde/alloc") ("default" "std" "unlimited-points"))))))

(define-public crate-throw-0.1.6 (c (n "throw") (v "0.1.6") (d (list (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "01wp6icxfpq05b5zq0ja8pb8cq6ipqa6r2wnzyfnm6a48lpkmbhp") (f (quote (("unlimited-points") ("std") ("serde-1-std" "serde" "serde_derive" "serde/std") ("serde-1" "serde" "serde_derive" "serde/alloc") ("default" "std" "unlimited-points"))))))

(define-public crate-throw-0.1.7 (c (n "throw") (v "0.1.7") (d (list (d (n "regex") (r "^1.0") (d #t) (k 2)) (d (n "serde") (r "^1.0") (o #t) (k 0)) (d (n "serde_derive") (r "^1.0") (o #t) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 2)))) (h "0x8gw6hv6yzz9f3l0qswz53hp3rl1n6f9w9lw75x30hiafi13l2y") (f (quote (("unlimited-points") ("std") ("serde-1-std" "serde" "serde_derive" "serde/std") ("serde-1" "serde" "serde_derive" "serde/alloc") ("default" "std" "unlimited-points"))))))

