(define-module (crates-io th ro throttled-reader) #:use-module (crates-io))

(define-public crate-throttled-reader-1.0.0 (c (n "throttled-reader") (v "1.0.0") (h "02clms4sqa21pmzfc74kw551fy514qv2l0lnqjpsfhdpf2fjqynm")))

(define-public crate-throttled-reader-1.0.1 (c (n "throttled-reader") (v "1.0.1") (h "1j4biyci03sl49lm1y69jmvfa22sckwjsl88wj8wfxbqr865g1a7")))

