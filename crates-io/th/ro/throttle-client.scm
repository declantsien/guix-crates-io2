(define-module (crates-io th ro throttle-client) #:use-module (crates-io))

(define-public crate-throttle-client-0.3.9 (c (n "throttle-client") (v "0.3.9") (d (list (d (n "humantime") (r "^2.0.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "0bfgixbydpbydyywscdwl5pkwg9fgaf38gylcryp1y3gwmyydvxp")))

(define-public crate-throttle-client-0.3.11 (c (n "throttle-client") (v "0.3.11") (d (list (d (n "humantime") (r "^2.0.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "1v8pl7kj141j35pvrsbin50ap2xj2zljns449c45frppii8qa2ya")))

(define-public crate-throttle-client-0.3.12 (c (n "throttle-client") (v "0.3.12") (d (list (d (n "humantime") (r "^2.0.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.6") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.111") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.19") (d #t) (k 0)))) (h "160h6a9fp5zdxybiiwaw269nynn053xdb3cmynspn4956f11r0h3")))

(define-public crate-throttle-client-0.3.13 (c (n "throttle-client") (v "0.3.13") (d (list (d (n "humantime") (r "^2.0.1") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.10.8") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.117") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.22") (d #t) (k 0)))) (h "14kg53ray41f4gv6p0mc74y0jhwxh00nq7ys4wvpfm2pps76h08b")))

(define-public crate-throttle-client-0.3.14 (c (n "throttle-client") (v "0.3.14") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.2") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.125") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1lrsafq286falksj0f2ps7lrl1qmbhy4f48a9qmfjspvh8jl2454")))

(define-public crate-throttle-client-0.3.15 (c (n "throttle-client") (v "0.3.15") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.3") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.126") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.24") (d #t) (k 0)))) (h "1vy6jjdml2bqryc82ipxqh3iiimhl9pqmhkhcpwlay9cj6ylrd80")))

(define-public crate-throttle-client-0.3.16 (c (n "throttle-client") (v "0.3.16") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.127") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.26") (d #t) (k 0)))) (h "022wk8371ap5j9z2rkh1d6jnq0zfvz76x4qmcb7qablk8ca8kl18")))

(define-public crate-throttle-client-0.3.17 (c (n "throttle-client") (v "0.3.17") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.0.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.9") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.133") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.30") (d #t) (k 0)))) (h "11rmmyf9rwdnkmzfsxnwr27gfrw3g400xhr64qaybln5mil92ifr")))

(define-public crate-throttle-client-0.4.0 (c (n "throttle-client") (v "0.4.0") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.137") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.31") (d #t) (k 0)))) (h "0ydfx4bbarky997yh0pam3ixxismblx2vh7cjrlyqp7dagpk7c25")))

(define-public crate-throttle-client-0.4.1 (c (n "throttle-client") (v "0.4.1") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "16f0s6dbi699l1gws4p3cn4knl0z20390fdzzx3npqbkp02ahbf1")))

(define-public crate-throttle-client-0.4.2 (c (n "throttle-client") (v "0.4.2") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.14") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.152") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.38") (d #t) (k 0)))) (h "0mf7cm7j7f0zc55d0rr6glmgxhwkzmd6vfylcpv7gv4vvzmi958s")))

(define-public crate-throttle-client-0.4.3 (c (n "throttle-client") (v "0.4.3") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.15") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.158") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.40") (d #t) (k 0)))) (h "1jd4l2yhai5brp4l83q998zncj4f7wx5yvlr6kwx9n36lxfv7yj1")))

(define-public crate-throttle-client-0.4.4 (c (n "throttle-client") (v "0.4.4") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "1mjd5baf4pkmzcdnq5ghwqj0nvqkfr6aslhcf7f12mmh24qm98m3")))

(define-public crate-throttle-client-0.4.5 (c (n "throttle-client") (v "0.4.5") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.11.22") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.193") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.50") (d #t) (k 0)))) (h "08lnks7z4lspig33hdpz04rzfsr3pdwnd12ml2b0kp79mnrx76jv")))

(define-public crate-throttle-client-0.5.3 (c (n "throttle-client") (v "0.5.3") (d (list (d (n "humantime") (r "^2.1.0") (d #t) (k 0)) (d (n "humantime-serde") (r "^1.1.1") (d #t) (k 0)) (d (n "reqwest") (r "^0.12.4") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.198") (d #t) (k 0)) (d (n "thiserror") (r "^1.0.59") (d #t) (k 0)))) (h "1726jihy2l6av6acvm343lc6df997qx5m1b64vf05n3v8wvcixk6")))

