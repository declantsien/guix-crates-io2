(define-module (crates-io th ro throttle_my_fn) #:use-module (crates-io))

(define-public crate-throttle_my_fn-0.1.0 (c (n "throttle_my_fn") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "146dmgfnps936pmndd8022bdnxfmfpbwm75d7n14xpgq249nqvfb") (y #t)))

(define-public crate-throttle_my_fn-0.2.0 (c (n "throttle_my_fn") (v "0.2.0") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "01zgdjr790hs8szayn4rn7kx0mhs3q146cpk1cf0fj3ch4lv9fs7")))

(define-public crate-throttle_my_fn-0.2.1 (c (n "throttle_my_fn") (v "0.2.1") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "05ak3m4gbx6fn7clqlfqq4myr0854k8bj3iki6kb2w3c7fb1l451")))

(define-public crate-throttle_my_fn-0.2.2 (c (n "throttle_my_fn") (v "0.2.2") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1ax4hvyry6ja4hjdhfdjlfflp3svqg58ndh732i1xfyvzg1gp08f")))

(define-public crate-throttle_my_fn-0.2.3 (c (n "throttle_my_fn") (v "0.2.3") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1506pyfi9rhrk9fpaygl5bqdjj44p6ka8b0yxvf2v9znriz9r7vi")))

(define-public crate-throttle_my_fn-0.2.4 (c (n "throttle_my_fn") (v "0.2.4") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "0dxn0pn2hr7xqznqc6vpqmcjxayc7dqaz9rvj1cvnxd8iv7s7bl5")))

(define-public crate-throttle_my_fn-0.2.5 (c (n "throttle_my_fn") (v "0.2.5") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1bm1n0zr8dkixrf2kkniac0ndrm9cafkj38bxvw4dy250wya6w9f")))

(define-public crate-throttle_my_fn-0.2.6 (c (n "throttle_my_fn") (v "0.2.6") (d (list (d (n "parking_lot") (r "^0.11") (d #t) (k 0)) (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("extra-traits" "full"))) (d #t) (k 0)))) (h "1w47p4w5ia6rl52zg0n8729j6aij5hv8yfhb2fd6qqkmarg1hb28")))

