(define-module (crates-io th ro throbber-widgets-tui) #:use-module (crates-io))

(define-public crate-throbber-widgets-tui-0.1.0 (c (n "throbber-widgets-tui") (v "0.1.0") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1k4b99by55yw8j9lry6dbs9gmyr6wpghb2zka7yfdv9jvbcj03dp")))

(define-public crate-throbber-widgets-tui-0.1.1 (c (n "throbber-widgets-tui") (v "0.1.1") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "01mvw6jk13ch0070wqs8jbi4baq6g94h82v6khsnc16zd31l9jyb")))

(define-public crate-throbber-widgets-tui-0.1.2 (c (n "throbber-widgets-tui") (v "0.1.2") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "14p0111kzsm1qvvynfah17xpd3dmz79hsrfz31cwz3hjahx80dxj")))

(define-public crate-throbber-widgets-tui-0.1.3 (c (n "throbber-widgets-tui") (v "0.1.3") (d (list (d (n "crossterm") (r "^0.25.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (d #t) (k 0)))) (h "1zx21krp1sf832gb6v18zpbb79jvkqb5yw17gaar4659p0xbhpw8") (r "1.56.1")))

(define-public crate-throbber-widgets-tui-0.2.0 (c (n "throbber-widgets-tui") (v "0.2.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.22.0") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1b2z1x4f4anzjv0q7s8jcga3dp18ivfy0himaznh6n3fa2169j04") (f (quote (("default" "ratatui")))) (r "1.67.0")))

(define-public crate-throbber-widgets-tui-0.3.0 (c (n "throbber-widgets-tui") (v "0.3.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.24.0") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1dgm210df1g1215wpiya5r0i533b8zax2h3hkmbnmyj82l2wcwna") (f (quote (("default" "ratatui")))) (r "1.70.0")))

(define-public crate-throbber-widgets-tui-0.4.0 (c (n "throbber-widgets-tui") (v "0.4.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "1xjjn1ddzmgp8asr6rpsjm8cdldc5fjsqswacjisj0v3lmxlab4v") (f (quote (("default" "ratatui")))) (r "1.72.0")))

(define-public crate-throbber-widgets-tui-0.4.1 (c (n "throbber-widgets-tui") (v "0.4.1") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.0") (o #t) (d #t) (k 0)) (d (n "tui") (r "^0.19.0") (o #t) (d #t) (k 0)))) (h "0jaa5p4qfybzm7359jlpyqvwnj3r1snvy97ggr65j52kc9gd5759") (f (quote (("default" "ratatui")))) (r "1.72.0")))

(define-public crate-throbber-widgets-tui-0.5.0 (c (n "throbber-widgets-tui") (v "0.5.0") (d (list (d (n "crossterm") (r "^0.27.0") (d #t) (k 2)) (d (n "rand") (r "^0.8.5") (d #t) (k 0)) (d (n "ratatui") (r "^0.26.1") (d #t) (k 0)))) (h "18kpqn8qhpjh720b0cdl2c0bn9vhvlycn6sv9bwisyzwc1x886s3") (r "1.72.0")))

