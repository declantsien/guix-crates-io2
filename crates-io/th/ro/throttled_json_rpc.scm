(define-module (crates-io th ro throttled_json_rpc) #:use-module (crates-io))

(define-public crate-throttled_json_rpc-0.0.1 (c (n "throttled_json_rpc") (v "0.0.1") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 2)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0wyb76aaz4jl9g5mbry8bs2ywl8n8lhwi4kdn6d9k6rmi98hx25j")))

(define-public crate-throttled_json_rpc-0.0.2 (c (n "throttled_json_rpc") (v "0.0.2") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 2)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1zl53hv3l9s32r6abx3wc3z5syyl84bb8yvsz14i01jfh5vdiqk3")))

(define-public crate-throttled_json_rpc-0.0.3 (c (n "throttled_json_rpc") (v "0.0.3") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 2)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "0hrjmy4afbnlgvg88vq1r0qnhaqycjsl88mpl6yfpvkpbc94d2k8")))

(define-public crate-throttled_json_rpc-0.0.4 (c (n "throttled_json_rpc") (v "0.0.4") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 2)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "1bfg78wmn1n0phnzk1wsyq90ny99iqn46bl0p6qs70hvjzzkqyx6")))

(define-public crate-throttled_json_rpc-0.0.5 (c (n "throttled_json_rpc") (v "0.0.5") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 2)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "01bd1mk41vc8ymv6dzn0biw57bhh0vhxzsl5r40d6ns0zp8ndx1d")))

(define-public crate-throttled_json_rpc-0.0.6 (c (n "throttled_json_rpc") (v "0.0.6") (d (list (d (n "failure") (r "^0.1.5") (d #t) (k 2)) (d (n "reqwest") (r "^0.9.18") (d #t) (k 2)) (d (n "serde") (r "^1.0.92") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde_json") (r "^1.0.39") (d #t) (k 2)))) (h "11c9wdxy47ln3bn8cjl3biwbwpzx6fh48lraaska69sgql2v1fhy")))

