(define-module (crates-io th ro throwing) #:use-module (crates-io))

(define-public crate-throwing-0.1.0 (c (n "throwing") (v "0.1.0") (d (list (d (n "throwing-macros") (r "^0.1.0") (d #t) (k 0)))) (h "1z0k9kgb4c2lk0472l0bhgkhryrgn26clj9ip7hdbidaa2gcmi7k") (r "1.65")))

(define-public crate-throwing-0.1.1 (c (n "throwing") (v "0.1.1") (d (list (d (n "throwing-macros") (r "^0.1.1") (d #t) (k 0)))) (h "1dhql3xy5sz9mi4xf80x357galn4pn14xqk1h4nlcxz7hcrqikwg") (r "1.65")))

