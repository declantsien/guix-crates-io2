(define-module (crates-io th ie thieves-cant) #:use-module (crates-io))

(define-public crate-thieves-cant-0.1.0 (c (n "thieves-cant") (v "0.1.0") (d (list (d (n "clap") (r "^2.31") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2") (d #t) (k 0)))) (h "1wrzji9wjgkzbn0x0lha4s7wi6mxsvaaglbr599psm1q99273ddr")))

(define-public crate-thieves-cant-1.0.0 (c (n "thieves-cant") (v "1.0.0") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "1vy5cgh9lwbii1k87pmr5xg4gdnh1ha4j8768gv1d21qp1v1acg8")))

(define-public crate-thieves-cant-1.0.1 (c (n "thieves-cant") (v "1.0.1") (d (list (d (n "clap") (r "^2") (d #t) (k 0)) (d (n "lazy_static") (r "^1") (d #t) (k 0)) (d (n "rand") (r "^0.6") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)))) (h "121il84ab7dzra4v1w041d6av8bb3gcxrg0kn1f1sgi337hd7apg")))

