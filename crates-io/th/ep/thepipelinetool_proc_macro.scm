(define-module (crates-io th ep thepipelinetool_proc_macro) #:use-module (crates-io))

(define-public crate-thepipelinetool_proc_macro-0.1.0 (c (n "thepipelinetool_proc_macro") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^1.0") (f (quote ("full"))) (d #t) (k 0)))) (h "0ckxvhb295i9g4w57db50wbx05wc06byih65978d733hmhh18gdr")))

(define-public crate-thepipelinetool_proc_macro-0.2.7 (c (n "thepipelinetool_proc_macro") (v "0.2.7") (d (list (d (n "proc-macro2") (r "^1.0") (d #t) (k 0)) (d (n "quote") (r "^1.0") (d #t) (k 0)) (d (n "syn") (r "^2.0") (f (quote ("full"))) (d #t) (k 0)))) (h "00bdrdjf3np6bd0lznbws7igbgba3kqz36zl61bmihmycp3380id")))

