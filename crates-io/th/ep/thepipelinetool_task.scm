(define-module (crates-io th ep thepipelinetool_task) #:use-module (crates-io))

(define-public crate-thepipelinetool_task-0.1.0 (c (n "thepipelinetool_task") (v "0.1.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.1.0") (d #t) (k 0)))) (h "1462n7cnnznjn1pir2vznzrlv0m4a0dwig3skmyv8lc0vic1sr3w")))

(define-public crate-thepipelinetool_task-0.1.1 (c (n "thepipelinetool_task") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.1.1") (d #t) (k 0)))) (h "1s7c8fdv1b99v3fdpymmnanlx5jbyvywihy19fnawcybh0h152ib")))

(define-public crate-thepipelinetool_task-0.1.11 (c (n "thepipelinetool_task") (v "0.1.11") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.1.2") (d #t) (k 0)))) (h "13mq1wk32cwcrs97p5x9xh0fqflffb5773caidm9qx0l51vfzjxq") (y #t)))

(define-public crate-thepipelinetool_task-0.1.3 (c (n "thepipelinetool_task") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.1.3") (d #t) (k 0)))) (h "0193g5xzj34km968qi0kfxvyqdqcxw6x4mp4krgiggy337iqqcxi")))

(define-public crate-thepipelinetool_task-0.2.0 (c (n "thepipelinetool_task") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.2.0") (d #t) (k 0)))) (h "0llbv2my3s16rn1fmj9afwxwvdg62hfp2ap2gg1lrrj86msm1s1j")))

(define-public crate-thepipelinetool_task-0.2.1 (c (n "thepipelinetool_task") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.2.1") (d #t) (k 0)))) (h "02ni4bpax7b4f1r9zavrs4p16xwnk2m64m27qklklp8mwa8pzv2v")))

(define-public crate-thepipelinetool_task-0.2.2 (c (n "thepipelinetool_task") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.2.2") (d #t) (k 0)))) (h "1xkni0grd1vx75nhvi89m2s26mpy3w35qhg5m2sh5rprrhalwnzm")))

(define-public crate-thepipelinetool_task-0.2.3 (c (n "thepipelinetool_task") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.2.3") (d #t) (k 0)))) (h "09lwacn49zn78vmfpg22sh1kq1qp7v7fl9mg0ag5zr3wdw14bkmk")))

(define-public crate-thepipelinetool_task-0.2.4 (c (n "thepipelinetool_task") (v "0.2.4") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.2.4") (d #t) (k 0)))) (h "0v5zwinvsxlq3x1iycwiybf2cng4r1vxjs5w6ff3q9gi5yr1h350")))

(define-public crate-thepipelinetool_task-0.2.5 (c (n "thepipelinetool_task") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.2.5") (d #t) (k 0)))) (h "01dg8lcsf3385mg9a2gmwxvr462scdkfgc19pfgd4kdpgn4brdgl")))

(define-public crate-thepipelinetool_task-0.2.6 (c (n "thepipelinetool_task") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.2.6") (d #t) (k 0)))) (h "14rkz3n9ymh720dna7gasarr04qkbnbipbrf93xnnrmpx00w7mfv")))

(define-public crate-thepipelinetool_task-0.2.7 (c (n "thepipelinetool_task") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0.81") (d #t) (k 0)) (d (n "chrono") (r "^0.4.31") (f (quote ("serde"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.189") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0") (d #t) (k 0)) (d (n "thepipelinetool_utils") (r "^0.2.7") (d #t) (k 0)))) (h "0ql2vvghgs29vxmw2i8jhjz31gk8qks3mwzihrfmv5qfq7wa1b41")))

