(define-module (crates-io th er therror) #:use-module (crates-io))

(define-public crate-therror-0.0.48 (c (n "therror") (v "0.0.48") (d (list (d (n "anyhow") (r "^1.0.73") (d #t) (k 2)) (d (n "ref-cast") (r "^1.0.18") (d #t) (k 2)) (d (n "rustversion") (r "^1.0.13") (d #t) (k 2)) (d (n "therror-impl") (r "^0.0.48") (d #t) (k 0)) (d (n "trybuild") (r "^1.0.81") (f (quote ("diff"))) (d #t) (k 2)))) (h "0a7vgqvc95xb1vg4pg4pq4gahza7npi4v049zh1jvl5q90i9ms26") (r "1.56")))

