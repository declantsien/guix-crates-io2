(define-module (crates-io th er thereandbackagain) #:use-module (crates-io))

(define-public crate-thereandbackagain-0.1.0 (c (n "thereandbackagain") (v "0.1.0") (h "175cd3919qffylzhcqi5wyw9zw9bdgzmf2s3np1kshgvwl6kxdlf")))

(define-public crate-thereandbackagain-0.1.1 (c (n "thereandbackagain") (v "0.1.1") (h "03jg6x0j8g0xq9dsllqfgbjmz6s5k9znzyh8cjy6s3ay9yfdb9cr")))

