(define-module (crates-io th er thermal-camera) #:use-module (crates-io))

(define-public crate-thermal-camera-0.1.1 (c (n "thermal-camera") (v "0.1.1") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "=0.4.0-alpha.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15.1") (d #t) (k 0)))) (h "1bbnljfc8afzfvg7maa4y4zcvipvvzgbqbv4ldjnlwxhmv6akhgv")))

(define-public crate-thermal-camera-0.2.0 (c (n "thermal-camera") (v "0.2.0") (d (list (d (n "embedded-hal") (r "=1.0.0-alpha.4") (d #t) (k 0)) (d (n "linux-embedded-hal") (r "=0.4.0-alpha.0") (d #t) (k 2)) (d (n "ndarray") (r "^0.15") (k 0)) (d (n "num_enum") (r "^0.5") (k 0)))) (h "15bh7ggdccp9bca5nscnyqfz5shsm7kvw81bvyidbmssb83qbpxy") (f (quote (("std" "ndarray/std" "num_enum/std") ("default" "std"))))))

