(define-module (crates-io th er thermocouple) #:use-module (crates-io))

(define-public crate-thermocouple-0.1.0 (c (n "thermocouple") (v "0.1.0") (d (list (d (n "libm") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0g6nfabndar206pdvn4d43njqzgi7ljhmna926cycbhh2wrcax76") (f (quote (("use_serde") ("k-type" "libm") ("f64") ("f32") ("extrapolate") ("default" "f64" "k-type"))))))

(define-public crate-thermocouple-0.1.1 (c (n "thermocouple") (v "0.1.1") (d (list (d (n "libm") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0123k6yy5si0s9qxay4wbxfz7sn5ch7vgk78cdjhhg8k5bkljqw6") (f (quote (("use_serde") ("k-type" "libm") ("f64") ("f32") ("extrapolate") ("default" "f64" "k-type"))))))

(define-public crate-thermocouple-0.1.2 (c (n "thermocouple") (v "0.1.2") (d (list (d (n "libm") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "0v9pdayg79q35fpw2qhwzy7rzlgqa152vf059j8fr3rwjzmpxc9h") (f (quote (("use_serde") ("k-type" "libm") ("f64") ("f32") ("extrapolate") ("default" "f64" "k-type"))))))

(define-public crate-thermocouple-0.1.3 (c (n "thermocouple") (v "0.1.3") (d (list (d (n "libm") (r "^0.1.4") (o #t) (d #t) (k 0)))) (h "06cc0h98p2nkl5msdxlhiwwn5rs92gypcizqmr04z5rxlfzqbys5") (f (quote (("use_serde") ("k-type" "libm") ("f64") ("f32") ("extrapolate") ("default" "f64" "k-type"))))))

