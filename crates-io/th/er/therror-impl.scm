(define-module (crates-io th er therror-impl) #:use-module (crates-io))

(define-public crate-therror-impl-0.0.48 (c (n "therror-impl") (v "0.0.48") (d (list (d (n "proc-macro2") (r "^1.0.63") (d #t) (k 0)) (d (n "quote") (r "^1.0.29") (d #t) (k 0)) (d (n "syn") (r "^2.0.23") (d #t) (k 0)))) (h "1q7abapg90x7rpr1nw0hhm3vwjpk3ljbf8lfz86ajqhav3q5g313") (r "1.56")))

