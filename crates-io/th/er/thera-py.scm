(define-module (crates-io th er thera-py) #:use-module (crates-io))

(define-public crate-thera-py-0.1.0 (c (n "thera-py") (v "0.1.0") (d (list (d (n "clap-verbosity-flag") (r "^0.3.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "1dgg0wl7wrrjfdj0k5ki7j7p1wmx7hv9y799ngd7k5wj0bb00hfb")))

(define-public crate-thera-py-0.1.1 (c (n "thera-py") (v "0.1.1") (d (list (d (n "clap-verbosity-flag") (r "^0.3.1") (d #t) (k 0)) (d (n "colored") (r "^2") (d #t) (k 0)) (d (n "structopt") (r "^0.3.13") (d #t) (k 0)))) (h "10pj3567a7dbylx48h249wa3kpa34n0dj4abr6gcg4v6mmbq4krf")))

