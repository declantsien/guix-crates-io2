(define-module (crates-io th er thermostat) #:use-module (crates-io))

(define-public crate-thermostat-0.0.1 (c (n "thermostat") (v "0.0.1") (h "1vkdlrbap027v5s0ixpyvp08rq99mdy7jz0x3cbz8xrd9l7bzqaz")))

(define-public crate-thermostat-0.0.2 (c (n "thermostat") (v "0.0.2") (h "1kvjmv1q26522vrzn18dkdfdih7rrsslwyna3cqxcl9s4pvlxa6f")))

