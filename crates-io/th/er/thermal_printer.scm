(define-module (crates-io th er thermal_printer) #:use-module (crates-io))

(define-public crate-thermal_printer-0.0.1 (c (n "thermal_printer") (v "0.0.1") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serial") (r "^0.3") (d #t) (k 0)))) (h "1az5zr1gkq10bqagiazw6834s44v19f80l4sw5nfr41gp8ayv5gk") (y #t)))

(define-public crate-thermal_printer-0.0.2 (c (n "thermal_printer") (v "0.0.2") (d (list (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "serial") (r "^0.3") (d #t) (k 0)))) (h "0hfc8rb915j610bn1rd9a1dgrc5b3b0rhphpkg3ribscdbziykgk")))

(define-public crate-thermal_printer-0.1.0 (c (n "thermal_printer") (v "0.1.0") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)) (d (n "serial-embedded-hal") (r "^0.1.2") (d #t) (k 2)))) (h "0jpf4dgk8mn5hwpxmq7d3bxhajkpkppnhda5xz55kgvxfbvc79za")))

(define-public crate-thermal_printer-0.1.1 (c (n "thermal_printer") (v "0.1.1") (d (list (d (n "embedded-hal") (r "^0.2") (d #t) (k 0)) (d (n "nb") (r "^0.1.1") (d #t) (k 0)) (d (n "serial") (r "^0.4.0") (d #t) (k 2)) (d (n "serial-embedded-hal") (r "^0.1.2") (d #t) (k 2)))) (h "0a4laxax7cp5jw1q9yvhlhzn4ign1s8ly2cnjxj0jnkb3rkd9li3")))

