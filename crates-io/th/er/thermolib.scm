(define-module (crates-io th er thermolib) #:use-module (crates-io))

(define-public crate-thermolib-0.1.0 (c (n "thermolib") (v "0.1.0") (h "028v3hbjnbdmhw8w84dkgfli0vcjihvy4dh8sx4wb8hbdmzrmvm8")))

(define-public crate-thermolib-0.1.1 (c (n "thermolib") (v "0.1.1") (h "0x90kb8x0d8ywrkn2qg9q1804m9i9a9m4bjjyflfd72jy5vjbnsi")))

(define-public crate-thermolib-0.1.2 (c (n "thermolib") (v "0.1.2") (h "0pdbngackmfjca0raamvk3ibzk1gm1zg1773qdxi7lx2ph4jvawf")))

(define-public crate-thermolib-0.1.3 (c (n "thermolib") (v "0.1.3") (h "0fpcryfc5xr8p1q9qxlr9dn680nm21fw3cf23lszwx8azbm8y5wa")))

(define-public crate-thermolib-0.2.0 (c (n "thermolib") (v "0.2.0") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "18av9gyrjih35x2ai5zd41rhmnm4vdb1s1iwp3hf7lyp9lnkabkq")))

(define-public crate-thermolib-0.2.1 (c (n "thermolib") (v "0.2.1") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "0ybyynzqh2iwgqasix81j2x6j5s40afkf1krs3ql1cdfgql3y8am")))

(define-public crate-thermolib-0.2.2 (c (n "thermolib") (v "0.2.2") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "1w612phklxyvpcb6410c602x1lk85m7s5nj4x3gdmwz7s30xk1g9")))

(define-public crate-thermolib-0.2.3 (c (n "thermolib") (v "0.2.3") (d (list (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "02pbaickhji6sqfbhj08019cvawjih95z5g4fbp5f05ik63m632l")))

(define-public crate-thermolib-0.2.4 (c (n "thermolib") (v "0.2.4") (d (list (d (n "pyo3") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)))) (h "0mz9pla8a00fwvl6ymc8vpaparv9zid31rp4mi4wn657ww4sc908")))

(define-public crate-thermolib-0.2.5 (c (n "thermolib") (v "0.2.5") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "14hvgqs65y8lz1nhijhrxxaxgakiaf1gppyl7wf2cr50iz2pjlp5")))

(define-public crate-thermolib-0.2.6 (c (n "thermolib") (v "0.2.6") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("anyhow"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0j03a7p2939r3f4kqnrchqwlkxlkc3q4gmnyrwzzlzhznx6gx0di")))

(define-public crate-thermolib-0.2.7 (c (n "thermolib") (v "0.2.7") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("anyhow"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "1rxny3adr0jhqcsfxnbzq76dfh0j78znm6zs5x04d2ly683crf5m")))

(define-public crate-thermolib-0.2.8 (c (n "thermolib") (v "0.2.8") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("anyhow"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "045jpjgmnmqwn2gyw5228av2ibjs7jgqdv5icy7nwqx3mgs5kz2q")))

(define-public crate-thermolib-0.2.9 (c (n "thermolib") (v "0.2.9") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("anyhow"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0fbbvyqjdx1awjgn1dgmldvgh649fvrq7yaiabh4a4mh8ncshs6k")))

(define-public crate-thermolib-0.2.10 (c (n "thermolib") (v "0.2.10") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("anyhow"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "04z4rzm7vbx4l2kb2r1jjk8jvj1b0svqaapm2xs5rnxdbbl3sl8r")))

(define-public crate-thermolib-0.3.0 (c (n "thermolib") (v "0.3.0") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("anyhow"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "slint") (r "^1.5.0") (d #t) (k 0)) (d (n "slint-build") (r "^1.5.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "0pmkm28x32yzlglx2fqnfm9xbavcysx8xz157fq1vsdqhamdgmjd")))

(define-public crate-thermolib-0.3.1 (c (n "thermolib") (v "0.3.1") (d (list (d (n "anyhow") (r "^1.0") (d #t) (k 0)) (d (n "pyo3") (r "^0.20.0") (f (quote ("anyhow"))) (d #t) (k 0)) (d (n "serde") (r "^1.0.195") (f (quote ("derive"))) (d #t) (k 0)) (d (n "serde_json") (r "^1.0.110") (d #t) (k 0)) (d (n "slint") (r "^1.5.0") (d #t) (k 0)) (d (n "slint-build") (r "^1.5.0") (d #t) (k 1)) (d (n "thiserror") (r "^1.0") (d #t) (k 0)))) (h "149iv7jr8ldrm8pz4vncayyi23c2n024fcd85pg69c76z3fzbw5i")))

