(define-module (crates-io th ot thot-data) #:use-module (crates-io))

(define-public crate-thot-data-0.10.0-init (c (n "thot-data") (v "0.10.0-init") (h "0cp105hplhz4a7dcxbkiw3p70b8p79bng6lg5dn2djr7dwfmh9g6")))

(define-public crate-thot-data-0.10.0-intermediate (c (n "thot-data") (v "0.10.0-intermediate") (h "03xdwvkc5y22922zq6ylc7y00pwwalcbcf65yfa72phxap4qjq9w")))

