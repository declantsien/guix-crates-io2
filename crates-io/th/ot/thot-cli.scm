(define-module (crates-io th ot thot-cli) #:use-module (crates-io))

(define-public crate-thot-cli-0.10.0-init (c (n "thot-cli") (v "0.10.0-init") (h "097qlkqjb7nbsz4bsbw8ipjdyxi2aaznwq45lv0hb2yn2mdlj7wi")))

(define-public crate-thot-cli-0.10.0-intermediate (c (n "thot-cli") (v "0.10.0-intermediate") (d (list (d (n "clap") (r "^3.2") (f (quote ("derive"))) (d #t) (k 0)) (d (n "env_logger") (r "^0.9") (d #t) (k 0)) (d (n "fake") (r "^2.4") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "mockall") (r "^0.11") (d #t) (k 2)) (d (n "rand") (r "^0.8") (d #t) (k 2)) (d (n "settings_manager") (r "^0.0.2") (d #t) (k 0)) (d (n "thot-core") (r "^0.10.0-intermediate") (f (quote ("clap"))) (d #t) (k 0)) (d (n "thot-local") (r "^0.10.0-intermediate") (d #t) (k 0)) (d (n "uuid") (r "^0.8") (f (quote ("serde" "v4"))) (d #t) (k 0)))) (h "03g2q4kls6nsrln5mrsqb33q1c807b8ki10grsj0r2rs00x5ajyl")))

