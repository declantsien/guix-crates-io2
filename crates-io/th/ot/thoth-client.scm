(define-module (crates-io th ot thoth-client) #:use-module (crates-io))

(define-public crate-thoth-client-0.2.1 (c (n "thoth-client") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (f (quote ("serde"))) (d #t) (k 0)) (d (n "graphql_client") (r "^0.9.0") (d #t) (k 0)) (d (n "reqwest") (r "^0.10") (f (quote ("json"))) (d #t) (k 0)) (d (n "serde") (r "^1.0") (d #t) (k 0)) (d (n "thoth-api") (r "^0.2.1") (d #t) (k 0)) (d (n "uuid") (r "^0.7") (f (quote ("serde"))) (d #t) (k 0)))) (h "1y5q8id9lkjm2spxbkkil7qzayymcxfgwwzkh4m4z8d9x4pdrwzl")))

