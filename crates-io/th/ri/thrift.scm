(define-module (crates-io th ri thrift) #:use-module (crates-io))

(define-public crate-thrift-0.0.1 (c (n "thrift") (v "0.0.1") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "try_from") (r "^0.2.0") (d #t) (k 0)))) (h "16rd7b7x69z7gg7qlhdvhd62phg4iiwj43bg5yqllp5lk6xmz8ph")))

(define-public crate-thrift-0.0.2 (c (n "thrift") (v "0.0.2") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "try_from") (r "^0.2.0") (d #t) (k 0)))) (h "0hq78bd9zfq050m2m4r20vagxqviv86385i823dqbi44gr4jjc3h")))

(define-public crate-thrift-0.0.3 (c (n "thrift") (v "0.0.3") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "try_from") (r "^0.2.0") (d #t) (k 0)))) (h "0f1d1g83qvd4ccz1g14a49is8bxjvdi3s9277xmqv5s1ndsvdvb2") (y #t)))

(define-public crate-thrift-0.0.4 (c (n "thrift") (v "0.0.4") (d (list (d (n "byteorder") (r "^0.5.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0.3") (d #t) (k 0)) (d (n "log") (r "~0.3.6") (d #t) (k 0)) (d (n "threadpool") (r "^1.0") (d #t) (k 0)) (d (n "try_from") (r "^0.2.0") (d #t) (k 0)))) (h "1828ls24b33im5xzzwp56nl6p0i01fbal6rbcq8rc9zx1ri0z4hb")))

(define-public crate-thrift-0.12.0 (c (n "thrift") (v "0.12.0") (d (list (d (n "byteorder") (r "~1.2.1") (d #t) (k 0)) (d (n "integer-encoding") (r "~1.0.4") (d #t) (k 0)) (d (n "log") (r "~0.3.8") (d #t) (k 0)) (d (n "threadpool") (r "~1.7.1") (d #t) (k 0)) (d (n "try_from") (r "~0.2.2") (d #t) (k 0)))) (h "0mf6r5fn0ny0fbsrq9pkmgd3gpnywc6z7hh8s0qkpwwrjxbalir8")))

(define-public crate-thrift-0.13.0 (c (n "thrift") (v "0.13.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^1.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0nvbrflb5fw6hmayqppkp97dh63vs2znww92dlkcfz4laia9cv8c")))

(define-public crate-thrift-0.14.0 (c (n "thrift") (v "0.14.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "integer-encoding") (r ">= 1.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0b1kv5wx4ji7qxiw8a2dzj7fr4n8567d33hcb9x1rybgsj99fkjp")))

(define-public crate-thrift-0.14.1 (c (n "thrift") (v "0.14.1") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "integer-encoding") (r ">= 1.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "00vb1bxdzb18n6vijzpicdyiiqip8ignqggvkridcpfnkp4v3y64")))

(define-public crate-thrift-0.14.2 (c (n "thrift") (v "0.14.2") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "integer-encoding") (r ">= 1.1.4") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "1g9wza0zfxld02znhjggxahkmi0fzf15gy8wjwm20c3wps3zr0zf")))

(define-public crate-thrift-0.15.0 (c (n "thrift") (v "0.15.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (d #t) (k 0)))) (h "0bk9mqmbdlamaa975fslq9qakphg2s4xvqqz12bcxcwmdzsahb5q")))

(define-public crate-thrift-0.16.0 (c (n "thrift") (v "0.16.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^1.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (o #t) (d #t) (k 0)))) (h "1w0gb6qf5hrz10nwdfpw2vnjb666549szhmp30pdgvmlvd68qrq9") (f (quote (("server" "threadpool" "log") ("default" "server"))))))

(define-public crate-thrift-0.17.0 (c (n "thrift") (v "0.17.0") (d (list (d (n "byteorder") (r "^1.3") (d #t) (k 0)) (d (n "integer-encoding") (r "^3.0.3") (d #t) (k 0)) (d (n "log") (r "^0.4") (o #t) (d #t) (k 0)) (d (n "ordered-float") (r "^2.0") (d #t) (k 0)) (d (n "threadpool") (r "^1.7") (o #t) (d #t) (k 0)))) (h "02cydaqqlp25ri19y3ixi77a7nd85fwvbfn4fp0qpakzzj2vqm3y") (f (quote (("server" "threadpool" "log") ("default" "server"))))))

