(define-module (crates-io th ri thrift_codec) #:use-module (crates-io))

(define-public crate-thrift_codec-0.1.0 (c (n "thrift_codec") (v "0.1.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serdeconv") (r "^0.3") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "06f4mzbh3lkpfc5cp3vdlhlfidcf7i8b6xak9asnc2g3s7p4zxj9")))

(define-public crate-thrift_codec-0.1.1 (c (n "thrift_codec") (v "0.1.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^2") (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (k 0)) (d (n "serdeconv") (r "^0.3") (d #t) (k 2)) (d (n "trackable") (r "^0.2") (d #t) (k 0)))) (h "1a5iz4ria22d9y114wb44qrf24licfr4js9skya19bx0s2rizdlg")))

(define-public crate-thrift_codec-0.2.0 (c (n "thrift_codec") (v "0.2.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^3") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serdeconv") (r "^0.4") (d #t) (k 2)) (d (n "trackable") (r "^1.2") (d #t) (k 0)))) (h "0ybj2i6i94s8nmyvcrhwp9hh7an8ambj68mvn8z77m4z305j1qzw")))

(define-public crate-thrift_codec-0.3.0 (c (n "thrift_codec") (v "0.3.0") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serdeconv") (r "^0.4") (d #t) (k 2)) (d (n "trackable") (r "^1.2") (d #t) (k 0)))) (h "1h95xgjwwymwd09pmigbmrsq16gdp0hbszx25zpkpqj8qhrzbrda")))

(define-public crate-thrift_codec-0.3.1 (c (n "thrift_codec") (v "0.3.1") (d (list (d (n "byteorder") (r "^1") (d #t) (k 0)) (d (n "clap") (r "^4") (f (quote ("derive"))) (d #t) (k 2)) (d (n "serde") (r "^1") (f (quote ("derive"))) (o #t) (d #t) (k 0)) (d (n "serdeconv") (r "^0.4") (d #t) (k 2)) (d (n "trackable") (r "^1.2") (d #t) (k 0)))) (h "0dsncck1q6xk4gqqwm9604dnwcl9gfacrd1rfddfsrns2q9kwj8n")))

