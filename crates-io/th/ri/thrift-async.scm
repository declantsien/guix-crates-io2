(define-module (crates-io th ri thrift-async) #:use-module (crates-io))

(define-public crate-thrift-async-0.1.0 (c (n "thrift-async") (v "0.1.0") (d (list (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "thrift") (r "^0.0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "062l3gp18m2f8l520fziyv85ss9hdhlc7gklbs03gl83ibqp7anb")))

(define-public crate-thrift-async-0.2.0 (c (n "thrift-async") (v "0.2.0") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "thrift") (r "^0.0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1d0kv2rya3c07qbl3a93fjl9rxlpmfbb3vg1qprq3cdgimsxxja7")))

(define-public crate-thrift-async-0.2.1 (c (n "thrift-async") (v "0.2.1") (d (list (d (n "failure") (r "^0.1") (d #t) (k 0)) (d (n "futures") (r "^0.1") (d #t) (k 0)) (d (n "thrift") (r "^0.0.4") (d #t) (k 0)) (d (n "tokio") (r "^0.1") (d #t) (k 0)))) (h "1gagyva7kk7c20hny9kiykh06mfwc4klr2jwssdrcir7wrsmdi41")))

