(define-module (crates-io th ri thrift-parser) #:use-module (crates-io))

(define-public crate-thrift-parser-0.0.1 (c (n "thrift-parser") (v "0.0.1") (d (list (d (n "derive-newtype") (r "^0.2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1mqsknybkaj6byxrh2fcwvy9lyq72vadrbdy1ibqzms7c0j8n9i2")))

(define-public crate-thrift-parser-0.0.2 (c (n "thrift-parser") (v "0.0.2") (d (list (d (n "derive-newtype") (r "^0.2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1dhnjx9rgxx0r839gnyj87xx7kwswcp13qxkzdjgw006sph3hdw7")))

(define-public crate-thrift-parser-0.0.3 (c (n "thrift-parser") (v "0.0.3") (d (list (d (n "derive-newtype") (r "^0.2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "1k4c6lz84n4znrhg4gdxshh2ppm255mvh2kg1dnk4mhq2bazf1sm")))

(define-public crate-thrift-parser-0.0.4 (c (n "thrift-parser") (v "0.0.4") (d (list (d (n "derive-newtype") (r "^0.2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "10yp8acv6v9cv8ha28wy6b8f4f4zm6y63cnql46ja9ia7l79sq8q")))

(define-public crate-thrift-parser-0.0.5 (c (n "thrift-parser") (v "0.0.5") (d (list (d (n "derive-newtype") (r "^0.2") (d #t) (k 0)) (d (n "float-cmp") (r "^0.8") (d #t) (k 0)) (d (n "nom") (r "^6") (d #t) (k 0)))) (h "15kpri482z8smfnr6n6hi0607cazskfl35cvqx6nvmv6jqrfrrxq")))

