(define-module (crates-io th ri thrify-core) #:use-module (crates-io))

(define-public crate-thrify-core-0.0.1 (c (n "thrify-core") (v "0.0.1") (d (list (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (d #t) (k 0)) (d (n "diesel") (r "^1.4.4") (f (quote ("postgres"))) (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "juniper") (r "^0.11") (d #t) (k 0)))) (h "0vhiaflmdcfslm78biim6ydkdcfvyijyrdkydci5x1d04i38kzj7") (y #t)))

