(define-module (crates-io th tt thttp) #:use-module (crates-io))

(define-public crate-thttp-0.1.1 (c (n "thttp") (v "0.1.1") (d (list (d (n "actix-files") (r "^0.2.1") (d #t) (k 0)) (d (n "actix-rt") (r "^1.0") (d #t) (k 0)) (d (n "actix-web") (r "^2.0") (d #t) (k 0)) (d (n "dotenv") (r "^0.15.0") (d #t) (k 0)) (d (n "listenfd") (r "^0.3.3") (d #t) (k 0)) (d (n "regex") (r "^1") (d #t) (k 0)) (d (n "structopt") (r "^0.3") (d #t) (k 0)))) (h "12bgaph0ff7mv36s5l1p606h4qz4hiqp5v93d3j0ily35kxwchia")))

