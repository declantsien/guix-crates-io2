(define-module (crates-io th ru thruster-session) #:use-module (crates-io))

(define-public crate-thruster-session-0.1.0 (c (n "thruster-session") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "hmac") (r "^0.12.1") (d #t) (k 0)) (d (n "jwt") (r "^0.16.0") (d #t) (k 0)) (d (n "serde") (r "^1.0.163") (d #t) (k 0)) (d (n "serde_json") (r "^1.0.96") (d #t) (k 0)) (d (n "sha2") (r "^0.10.6") (d #t) (k 0)) (d (n "thruster") (r "^1.3.3") (f (quote ("hyper_server"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)))) (h "0bab9mak64g335zj5b6xnwaxfcdznxlmis4n72a1j305fbcqi9hm")))

