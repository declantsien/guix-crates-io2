(define-module (crates-io th ru thruster-rate-limit) #:use-module (crates-io))

(define-public crate-thruster-rate-limit-0.1.0 (c (n "thruster-rate-limit") (v "0.1.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (f (quote ("connection-manager" "tokio-comp"))) (o #t) (d #t) (k 0)) (d (n "thruster") (r "^1.3.3") (f (quote ("hyper_server"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)))) (h "1d5zs8anrw9fdagxmg7bdzv90didvaf7l0n9qpprfv2nv3irbicx") (s 2) (e (quote (("redis_store" "dep:redis"))))))

(define-public crate-thruster-rate-limit-0.1.1 (c (n "thruster-rate-limit") (v "0.1.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (f (quote ("connection-manager" "tokio-comp"))) (o #t) (d #t) (k 0)) (d (n "thruster") (r "^1.3.3") (f (quote ("hyper_server"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)))) (h "0pi5h7r4vrp4crh2hs2raz4bqzvkl18638dgjhmb515cq9jddhrz") (s 2) (e (quote (("redis_store" "dep:redis"))))))

(define-public crate-thruster-rate-limit-1.0.0 (c (n "thruster-rate-limit") (v "1.0.0") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (f (quote ("connection-manager" "tokio-comp"))) (o #t) (d #t) (k 0)) (d (n "thruster") (r "^1.3.3") (f (quote ("hyper_server"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)))) (h "0gnwwnqg87944ipjrrkfr7n6ml9p7fw49h4s37mhng43bq0kn29h") (s 2) (e (quote (("redis_store" "dep:redis"))))))

(define-public crate-thruster-rate-limit-1.0.1 (c (n "thruster-rate-limit") (v "1.0.1") (d (list (d (n "async-trait") (r "^0.1.68") (d #t) (k 0)) (d (n "redis") (r "^0.23.0") (f (quote ("connection-manager" "tokio-comp"))) (o #t) (d #t) (k 0)) (d (n "thruster") (r "^1.3.3") (f (quote ("hyper_server"))) (d #t) (k 0)) (d (n "tokio") (r "^1.28.2") (d #t) (k 0)))) (h "0i079isv7vlwcdbya2d3002kvj3agxpnkq0l0770rrwx5mwhgvwg") (s 2) (e (quote (("redis_store" "dep:redis"))))))

