(define-module (crates-io th ru thruster-jab) #:use-module (crates-io))

(define-public crate-thruster-jab-0.1.0 (c (n "thruster-jab") (v "0.1.0") (d (list (d (n "dotenv") (r "^0.15.0") (d #t) (k 2)) (d (n "env_logger") (r "^0.7.1") (d #t) (k 2)) (d (n "hyper") (r "^0.14.8") (d #t) (k 2)) (d (n "log") (r "^0.4") (d #t) (k 2)) (d (n "thruster") (r "^1.1.10") (f (quote ("hyper_server"))) (d #t) (k 2)) (d (n "tokio") (r "^0.2.22") (f (quote ("full"))) (d #t) (k 2)))) (h "0kd32ra4cy06a06nqrq2akvzniqbp4i10jbagv771mjfk4nchmrh")))

