(define-module (crates-io th ru thrussh_client) #:use-module (crates-io))

(define-public crate-thrussh_client-0.1.0 (c (n "thrussh_client") (v "0.1.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5.1") (d #t) (k 0)) (d (n "net2") (r "= 0.2.19") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (k 0)) (d (n "thrussh") (r "^0.1") (d #t) (k 0)))) (h "0xns2iasj1fm21phr5h47i3h40vimj9qx3r3j3vvlv9qhvkfq5db") (y #t)))

(define-public crate-thrussh_client-0.2.0 (c (n "thrussh_client") (v "0.2.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5.1") (d #t) (k 0)) (d (n "net2") (r "= 0.2.19") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (k 0)) (d (n "thrussh") (r "^0.2") (d #t) (k 0)))) (h "037yz2s82pnfyvw17s8qq9xxaflr3z141nf2il9smz4g17w7fq6r") (y #t)))

(define-public crate-thrussh_client-0.3.0 (c (n "thrussh_client") (v "0.3.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5.1") (d #t) (k 0)) (d (n "net2") (r "= 0.2.19") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (k 0)) (d (n "thrussh") (r "^0.3") (d #t) (k 0)))) (h "1zglrsh2y7mzpkf41vqgm4bg71ccvm2bj1kkg1rq4pwbvwa2lnhj") (y #t)))

(define-public crate-thrussh_client-0.3.3 (c (n "thrussh_client") (v "0.3.3") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5.1") (d #t) (k 0)) (d (n "net2") (r "= 0.2.19") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (k 0)) (d (n "thrussh") (r "^0.3.3") (d #t) (k 0)))) (h "1xmp9lcjgpvb7i7820h48jqz1zkblfi0sp8fxcvkpkp0nsbcq6wq") (y #t)))

(define-public crate-thrussh_client-0.4.0 (c (n "thrussh_client") (v "0.4.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (k 0)) (d (n "thrussh") (r "^0.4") (d #t) (k 0)))) (h "1igggs98i0dzskm70x6g40x6g08ld1y8wnfipr7rq7lijz1mh1w3") (y #t)))

(define-public crate-thrussh_client-0.5.0 (c (n "thrussh_client") (v "0.5.0") (d (list (d (n "env_logger") (r "^0.3") (d #t) (k 0)) (d (n "log") (r "^0.3") (d #t) (k 0)) (d (n "mio") (r "^0.5") (d #t) (k 0)) (d (n "regex") (r "^0.1") (d #t) (k 0)) (d (n "termios") (r "^0.2.2") (d #t) (k 0)) (d (n "thrussh") (r "^0.5") (d #t) (k 0)) (d (n "user") (r "^0.1") (d #t) (k 0)))) (h "0y6m2casrz8rslnkni9l9znszz82kna254ahw46wix5lc4dcx0j9") (y #t)))

