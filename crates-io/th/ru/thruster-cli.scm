(define-module (crates-io th ru thruster-cli) #:use-module (crates-io))

(define-public crate-thruster-cli-0.1.1 (c (n "thruster-cli") (v "0.1.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line") (r "^0.1") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (f (quote ("pattern"))) (d #t) (k 0)))) (h "1wzhs5si42363pnw9l7ppycag8qzvkhrkh1wyn7j185179qz9ycw")))

(define-public crate-thruster-cli-0.1.3 (c (n "thruster-cli") (v "0.1.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line") (r "^0.1") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (f (quote ("pattern"))) (d #t) (k 0)))) (h "06dmm18kmxi7wv2zl0lss9qsgcaayrhcxyqm25ss0ccwadrsnwx7")))

(define-public crate-thruster-cli-0.2.0 (c (n "thruster-cli") (v "0.2.0") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line") (r "^0.1") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (f (quote ("pattern"))) (d #t) (k 0)))) (h "0q3q77kysr03mi21pi09zglsb4jkvawlzz0b23h9bjq6d81na2y8")))

(define-public crate-thruster-cli-0.2.1 (c (n "thruster-cli") (v "0.2.1") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line") (r "^0.1") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1.1") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (f (quote ("pattern"))) (d #t) (k 0)))) (h "13zdnz35wkr7q21qhhs2442wh034sm5zza01amsqiv6c13ap3yag")))

(define-public crate-thruster-cli-0.2.2 (c (n "thruster-cli") (v "0.2.2") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line") (r "^0.1.2") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (f (quote ("pattern"))) (d #t) (k 0)))) (h "13xlc3dxx6d0an0pbdpkmh92dbxp7ffivqa5pydfp789scxyn95j")))

(define-public crate-thruster-cli-0.2.3 (c (n "thruster-cli") (v "0.2.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line") (r "^0.1.2") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "1cxd4xp9kpdv3999nnpnjb940x37hjq8hcyxv93p7h5c1m66j30d")))

(define-public crate-thruster-cli-0.6.3 (c (n "thruster-cli") (v "0.6.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line") (r "^0.1.2") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "08b2s4vva7ybhldbllk7873rrfjlrf4vf5wdjihbfl8m7l2saq2z")))

(define-public crate-thruster-cli-0.7.3 (c (n "thruster-cli") (v "0.7.3") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line") (r "^0.1.2") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "1raybnn0cink29c8qswknpigcyrxk1kanmwg8nfilz2v9gpv530p")))

(define-public crate-thruster-cli-0.7.4 (c (n "thruster-cli") (v "0.7.4") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line") (r "^0.1.2") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "0c8kfnycfba7sj65y7hddinm41288pzj263vmcrld19wvmkk6zzw")))

(define-public crate-thruster-cli-0.7.5 (c (n "thruster-cli") (v "0.7.5") (d (list (d (n "chrono") (r "^0.4") (d #t) (k 0)) (d (n "fuel_line") (r "^0.1.2") (d #t) (k 0)) (d (n "fuel_line_derive") (r "^0.1.2") (d #t) (k 0)) (d (n "rand") (r "^0.4") (d #t) (k 0)) (d (n "regex") (r "^0.2.5") (d #t) (k 0)))) (h "0b8sv8yj93ppnh32f47xh22xr2y9x6jh4rwngfchjpwyy1vmj78f")))

