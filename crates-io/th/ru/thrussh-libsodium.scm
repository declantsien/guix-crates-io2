(define-module (crates-io th ru thrussh-libsodium) #:use-module (crates-io))

(define-public crate-thrussh-libsodium-0.1.0 (c (n "thrussh-libsodium") (v "0.1.0") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "0w89gqqgv4ks9s1644ii28r8qxmy223k5y43njmjmjcc4b254fg0")))

(define-public crate-thrussh-libsodium-0.1.1 (c (n "thrussh-libsodium") (v "0.1.1") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)))) (h "15pp9jb9148d4jdrasd4fnnv3sgan65mb5xa04zscn3fhrvb7p3w")))

(define-public crate-thrussh-libsodium-0.1.2 (c (n "thrussh-libsodium") (v "0.1.2") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0imk6dqyc9li2dz1rarxns2h9h2hvrisq63il1jbs8n2i7j925sk")))

(define-public crate-thrussh-libsodium-0.1.3 (c (n "thrussh-libsodium") (v "0.1.3") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "10vq42ym67id3yhn3pf2k4a71wx8dihsjzg9b9xx4r2hkqxff3xk")))

(define-public crate-thrussh-libsodium-0.1.4 (c (n "thrussh-libsodium") (v "0.1.4") (d (list (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)))) (h "0fjssjiwnmbxjvajk37l7k0fcw1ys97j7n8bpn3q3bbnz2qfrphv")))

(define-public crate-thrussh-libsodium-0.2.0 (c (n "thrussh-libsodium") (v "0.2.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "02kyc5hs805hmr90xd503hklwidvzqia76l45ac2v6a3j6fh8x46")))

(define-public crate-thrussh-libsodium-0.2.1 (c (n "thrussh-libsodium") (v "0.2.1") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2") (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "004mxns64gr3507dzr6s8vgd1478jcsgd2mw2cpbj73vs9q9rs6g")))

(define-public crate-thrussh-libsodium-0.2.2 (c (n "thrussh-libsodium") (v "0.2.2") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.7") (f (quote ("use-pkg-config"))) (d #t) (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "1r0nwlq601xv5a9vabrwkvj1mffc03k1c9yqh4rdhdarwqvxd14r")))

(define-public crate-thrussh-libsodium-0.3.0 (c (n "thrussh-libsodium") (v "0.3.0") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.7") (f (quote ("use-pkg-config"))) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "libsodium-sys") (r "^0.2.7") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "18vf8zpvyhbcdkn3cl6rdc2s57676jj6j4m2ykszc3fyi2xh1vaq")))

(define-public crate-thrussh-libsodium-0.2.3 (c (n "thrussh-libsodium") (v "0.2.3") (d (list (d (n "lazy_static") (r "^1.4") (d #t) (k 0)) (d (n "libc") (r "^0.2") (d #t) (k 0)) (d (n "libsodium-sys") (r "^0.2.7") (f (quote ("use-pkg-config"))) (d #t) (t "cfg(not(target_os = \"windows\"))") (k 0)) (d (n "libsodium-sys") (r "^0.2.7") (d #t) (t "cfg(target_os = \"windows\")") (k 0)) (d (n "pkg-config") (r "^0.3") (d #t) (k 1)) (d (n "vcpkg") (r "^0.2") (d #t) (k 1)))) (h "07lscwsfrh02hqnjw62wyknwmydiqwq4jz1gg1g25xygnh79qksz")))

