(define-module (crates-io th ru thruster-socketio-proc) #:use-module (crates-io))

(define-public crate-thruster-socketio-proc-0.1.0 (c (n "thruster-socketio-proc") (v "0.1.0") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1avhf91hzhx9hc68yh6sc3vdrw553zwph3p91wsm4wzcmcwndrgj")))

(define-public crate-thruster-socketio-proc-0.1.1 (c (n "thruster-socketio-proc") (v "0.1.1") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1i458ygnqdg6gzp623n9cxm1h8pkjbqb3wwqdizxv1sif7cfiinl")))

(define-public crate-thruster-socketio-proc-0.1.2 (c (n "thruster-socketio-proc") (v "0.1.2") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1mk22j0kp4vknmxi67niz0nz59a28s7ydm6s47wyz9djd40nw5rz")))

(define-public crate-thruster-socketio-proc-0.1.3 (c (n "thruster-socketio-proc") (v "0.1.3") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "0d9jzl6g97abwxsbznmag9qxzbf39xc2byf536smix0pw3jfgplg")))

(define-public crate-thruster-socketio-proc-0.1.4 (c (n "thruster-socketio-proc") (v "0.1.4") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1vnpxgsgwffmjgi71gz1ibqrzgd1ix72dbavqp0bbcnpk5xqdsqd")))

(define-public crate-thruster-socketio-proc-0.1.5 (c (n "thruster-socketio-proc") (v "0.1.5") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "082sqskfmqpj64idp8f13r1g1cf815fj9mkq0g8ixfvlrgg9qqlz")))

(define-public crate-thruster-socketio-proc-0.1.6 (c (n "thruster-socketio-proc") (v "0.1.6") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1ry3ji1dvys12z8q68a5slks276j6xd5rpqqqd5g7qprlkxig0zc")))

(define-public crate-thruster-socketio-proc-0.1.7 (c (n "thruster-socketio-proc") (v "0.1.7") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "07dkfnk3h7yvsk5sgznl8wwm81skjqpvvgqbbg8nvnjcnh3hzrch")))

(define-public crate-thruster-socketio-proc-0.1.8 (c (n "thruster-socketio-proc") (v "0.1.8") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "10bhbcznr5wr8m9vs2164s8p21f61csbj9rknhz403nrjmvdf8mp")))

(define-public crate-thruster-socketio-proc-0.1.9 (c (n "thruster-socketio-proc") (v "0.1.9") (d (list (d (n "proc-macro2") (r "^0.4.27") (d #t) (k 0)) (d (n "quote") (r "^0.6.3") (d #t) (k 0)) (d (n "syn") (r "^0.15.27") (f (quote ("full"))) (d #t) (k 0)))) (h "1h0v4s48wrr6vzwq56s6yf1iifcn1ffxi7335my99z7k9z4ysvbk")))

